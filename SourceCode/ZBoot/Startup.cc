//--Base
#include "Startup.h"

//--Classes
#include "AdventureLevel.h"
#include "AdventureInventory.h"
#include "AudioPackage.h"
#include "AdvCombat.h"
#include "Chapter0LoadInterrupt.h"
#include "StarCamera2D.h"
#include "FlexMenu.h"
#include "PlayerPony.h"
#include "WADFile.h"
#include "WorldDialogue.h"

//--CoreClases
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightPerlin.h"
#include "DataList.h"
#include "StarFileSystem.h"
#include "StarLinkedList.h"
#include "ModParser.h"
#include "VirtualFile.h"

//--Definitions
#include "Definitions.h"
#include "DebugDefinitions.h"
#include "GameGear.h"
#include "Global.h"
#include "GlDfn.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"
#include "MapManager.h"
#include "MemoryManager.h"
#include "NetworkManager.h"
#include "ResetManager.h"
#include "SaveManager.h"
#include "SteamManager.h"
#include "StarLumpManager.h"
#include "TranslationManager.h"

///--[Conditional Includes]
#if defined _TARGET_OS_WINDOWS_
    #include <windows.h>
#endif
#if defined _TARGET_OS_LINUX_
    #include <unistd.h>
#endif
#if defined _TARGET_OS_MAC_
    #include <unistd.h>
#endif

///========================================== Globals ==============================================
void InitializeGlobals()
{
    ///--[System Statics]
    //--Definitions.h, used for malloc interception. Can be disabled with defines.
    strcpy(gxMemoryFile, "Not Set");
    gxMemoryLine = 0;

    //--Error logging.
    strcpy(gxLastIssue, "No issue.");

    //--Mark the error log that a new execution is occurring.
    time_t tRawTime;
    struct tm *tTimeInfo;
    time (&tRawTime);
    tTimeInfo = localtime(&tRawTime);
    FILE *fOutfile = fopen("ErrorLog.txt", "a");
    fprintf(fOutfile, "%s - Begin program execution.\n", asctime(tTimeInfo));
    fclose(fOutfile);

    //--Resolve the memory page size.
    #if defined _TARGET_OS_WINDOWS_
        SYSTEM_INFO si;
        GetSystemInfo(&si);
        Memory::xPageSize = si.dwPageSize;
    #endif
    #if defined _TARGET_OS_LINUX_
        Memory::xPageSize = sysconf(_SC_PAGESIZE);
    #endif
    #if defined _TARGET_OS_MAC_
        Memory::xPageSize = sysconf(_SC_PAGESIZE);
    #endif


    ///--[Global Variable]
    //--Sets the Global variables to their pre-commandline modes.
    GLOBAL *rGlobal = Global::Shared();

    ///--[Allegro Variables]
    #if defined _ALLEGRO_PROJECT_
        rGlobal->gDisplay = NULL;
        rGlobal->gEventQueue = NULL;
        rGlobal->gSpeedTimer = NULL;
    #endif

    ///--[Generation Pointers]
    rGlobal->rCreateTitleMenu = &FlexMenu::GenerateFlexMenu;

    ///--[Static Classes]
    rGlobal->gLoadInterrupt = NULL;
    rGlobal->gFileSystemStack = new StarLinkedList(true);

    ///--[Managers]
    //--External
    rGlobal->gLuaManager = NULL;
    rGlobal->gNetworkManager = NULL;
    rGlobal->gStarLumpManager = NULL;
    rGlobal->gSaveManager = NULL;

    //--Game
    rGlobal->gCutsceneManager = NULL;
    rGlobal->gEntityManager = NULL;
    rGlobal->gMapManager = NULL;
    rGlobal->gResetManager = NULL;

    //--System
    rGlobal->gAudioManager = NULL;
    rGlobal->gCameraManager = NULL;
    rGlobal->gControlManager = NULL;
    rGlobal->gDisplayManager = NULL;
    rGlobal->gOptionsManager = NULL;

    ///--[GUI]
    rGlobal->gSystemFont = NULL;
    rGlobal->gBitmapFont = NULL;

    ///--[Libraries]
    rGlobal->gDataLibrary = NULL;

    ///--[Primitives]
    //--Display Variables
    rGlobal->gWindowWidth = 1024;
    rGlobal->gWindowHeight = 768;
    rGlobal->gScreenWidthPixels = VIRTUAL_CANVAS_X;
    rGlobal->gScreenHeightPixels = VIRTUAL_CANVAS_Y;
    rGlobal->gRenderedPixelsW = 1024;
    rGlobal->gRenderedPixelsH = 768;
    rGlobal->gProgramPath = NULL;

    //--Singleshot variables
    rGlobal->gQuit = false;
    rGlobal->gReset = false;
    rGlobal->gFastTitle = false;
    rGlobal->gExpectedFPS = STANDARD_FPS;
    rGlobal->gActualFPS = 0.0f;
    rGlobal->gBootWithJoystick = false;
    rGlobal->gWriteFPS = false;
    rGlobal->gTicksElapsed = 0;
    strcpy(rGlobal->gVersionString, "Not Set");

    //--Debug.
    rGlobal->mResetKeyPresses = 0;
    rGlobal->mResetKeyCooldown = 0;

    //--Game Options
    rGlobal->gDisallowPerlinNoise = false;
    rGlobal->gSkipPairanormalDisclaimer = false;

    ///--[Static Noise Generator]
    StarlightPerlin::xgStatic = new StarlightPerlin();
    StarlightPerlin::xgStatic->SetSeed(rand());
    StarlightPerlin::xgStatic->SetFrequency(16.0f);
    StarlightPerlin::xgStatic->SetOctaves(4);
    StarlightPerlin::xgStatic->Boot();

    ///--[Debug Variables]
    rGlobal->gShowStartupDebug = false;

    ///--[Debug Prints]
    SetMemoryData(__FILE__, __LINE__);
    rGlobal->gDebugPrintStrings = (char **)starmemoryalloc(sizeof(char *) * DEBUG_LINES_TOTAL);
    for(int i = 0; i < DEBUG_LINES_TOTAL; i ++) rGlobal->gDebugPrintStrings[i] = NULL;

    ///--[Function Pointers for Gears]
    rGlobal->rCurrentEventHandler  = &GameGear::EventHandler;
    rGlobal->rCurrentLogicHandler  = &GameGear::LogicHandler;
    rGlobal->rCurrentRenderHandler = &GameGear::RenderHandler;

    ///--[Game Registry]
    rGlobal->gGameRegistry = new StarLinkedList(false);

    ///--[Other Stuff]
    srand(time(NULL));
}
void RegisterGames()
{
    ///--[Setup]
    int cDummyPtr = 1;
    StarLinkedList *rRegistry = Global::Shared()->gGameRegistry;

    ///--[Register]
    rRegistry->AddElement("Pandemonium", &cDummyPtr);
    rRegistry->AddElement("Pandemonium Classic", &cDummyPtr);
    rRegistry->AddElement("String Tyrant", &cDummyPtr);
    rRegistry->AddElement("Slitted Eye", &cDummyPtr);
}

///======================================= Command Line ===========================================
void HandleCommandLine(int pArgsTotal, char **pArgs)
{
    //--Take care of the command line.
    if(pArgsTotal < 1 || !pArgs) return;

    //--0th arg is the execution path.
    GLOBAL *rGlobal = Global::Shared();
    ResetString(rGlobal->gProgramPath, pArgs[0]);

    //--Iterate across arguments.
    for(int i = 1; i < pArgsTotal; i ++)
    {
        //--Shows startup diagnostics.
        if(!strcmp(pArgs[i], "-startupdebug"))
        {
            rGlobal->gShowStartupDebug = true;
        }
        //--Redirects stderr to the specified file.
        if(!strcmp(pArgs[i],"-redirect") && i+1 < pArgsTotal)
        {
            freopen(pArgs[i+1], "w", stderr);
        }
        //--Disables all audio, both music and sound.
        if(!strcmp(pArgs[i],"-blockaudio"))
        {
            AudioManager::xBlockAllAudio = true;
        }
        //--Disables all music.
        if(!strcmp(pArgs[i],"-blockmusic"))
        {
            AudioManager::xBlockMusic = true;
        }
        //--Disables all sound, including voice ticks.
        if(!strcmp(pArgs[i],"-blocksound"))
        {
            AudioManager::xBlockSound = true;
        }
        //--Deprecated.
        if(!strcmp(pArgs[i],"-bootjoystick"))
        {
            rGlobal->gBootWithJoystick = true;
        }
        //--Jumps immediately into a given game mode. Used for testing.
        if(!strcmp(pArgs[i],"-autorun") && i+1 < pArgsTotal)
        {
            ResetString(OptionsManager::xAutorun, pArgs[i+1]);
        }
        //--Writes the FPS to a logfile.
        if(!strcmp(pArgs[i],"-logfps"))
        {
            rGlobal->gWriteFPS = true;
        }
        //--For 3D Classic, specifies a rendering profile, describing system strength.
        if(!strcmp(pArgs[i],"-renderprofile") && i+1 < pArgsTotal)
        {
            if(!strcasecmp(pArgs[i+1], "Standard"))
            {
                //fprintf(stdout, "[Activating Standard Rendering]\n");
                WADFile::xRenderWithVAOs = false;
                WADFile::xRenderTextures = true;
                WADFile::xRenderLightmaps = false;
                WADFile::xDisallowTextureAtlas = false;
            }
            else if(!strcasecmp(pArgs[i+1], "Potato"))
            {
                //fprintf(stdout, "[Activating Potato Rendering]\n");
                WADFile::xRenderWithVAOs = false;
                WADFile::xRenderTextures = true;
                WADFile::xRenderLightmaps = false;
                WADFile::xDisallowTextureAtlas = true;
            }
            else if(!strcasecmp(pArgs[i+1], "OnlyLightmap"))
            {
                //fprintf(stdout, "[Activating Lightmaps-only Rendering]\n");
                WADFile::xRenderWithVAOs = false;
                WADFile::xRenderTextures = false;
                WADFile::xRenderLightmaps = true;
                WADFile::xDisallowTextureAtlas = true;
            }
            else if(!strcasecmp(pArgs[i+1], "FullNoVAO"))
            {
                //fprintf(stdout, "[Activating Texture/Lightmap Rendering]\n");
                WADFile::xRenderWithVAOs = false;
                WADFile::xRenderTextures = true;
                WADFile::xRenderLightmaps = false;
                WADFile::xDisallowTextureAtlas = false;
            }
            else if(!strcasecmp(pArgs[i+1], "FullVAO"))
            {
                //fprintf(stdout, "[Activating High-End Rendering]\n");
                WADFile::xRenderWithVAOs = true;
                WADFile::xRenderTextures = true;
                WADFile::xRenderLightmaps = true;
                WADFile::xDisallowTextureAtlas = false;
            }
        }
        //--Forces wireframe rendering for wadfiles.
        if(!strcmp(pArgs[i],"-wireframe"))
        {
            WADFile::xAlwaysRenderWireframes = true;
        }
        //--Non-power-of-two texture atlas size.
        if(!strcmp(pArgs[i],"-atlasNPOT"))
        {
            WADFile::xForceNPOTAtlas = true;
        }
        //--Title does not fade.
        if(!strcmp(pArgs[i],"-fasttitle"))
        {
            rGlobal->gFastTitle = true;
        }
        //--Skip the splash screen.
        if(!strcmp(pArgs[i],"-nosplash"))
        {
            FlexMenu::xShowSplash = false;
        }
        //--Set the visual trace flags.
        if(!strcmp(pArgs[i],"-vistrace") && i+1 < pArgsTotal)
        {
            DebugManager::xVisTraceGlobalFlag = atoi(pArgs[i+1]);
            fprintf(stderr, "Set visual trace flag to %i\n", DebugManager::xVisTraceGlobalFlag);
        }
        /*
        //--Allocates a large amount of heap, fills it with garbage, and then deallocates it.
        //  Used for diagnostics.
        if(!strcmp(pArgs[i],"-memscrew"))
        {
            //--Debug.
            fprintf(stderr, "Memory screw initializing.\n");
            fprintf(stderr, "Please note: Memory screw is a debug feature. It is meant to help diagnose heap errors.\n");
            fprintf(stderr, "It will take a while to run. It is not meant for ordinary player use.\n");

            //--Setup.
            uint64_t cSizePerBlock = 1204; //1 kB per block
            uint64_t cNumBlocks = 1024 * 512;

            //--Debug.
            fprintf(stderr, "Memory screw will allocate %lli blocks of %lli bytes.\n", cNumBlocks, cSizePerBlock);
            fprintf(stderr, " Total Bytes: %lli\n", cNumBlocks * cSizePerBlock);
            fprintf(stderr, " %lli kB - %lli mB\n", (cNumBlocks * cSizePerBlock) / 1024, (cNumBlocks * cSizePerBlock) / 1024 / 1024);

            //--Allocate, iterate.
            uint8_t **tBlocks = (uint8_t **)starmemoryalloc(sizeof(uint8_t *) * cNumBlocks);
            for(int64_t p = 0; p < cNumBlocks; p ++)
            {
                //--Allocate.
                tBlocks[p] = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * cSizePerBlock);
                if(!tBlocks[p])
                {
                    fprintf(stderr, "Failed to allocated block %i\n", p);
                }

                //--Fill with garbage.
                for(int64_t c = 0; c < cSizePerBlock; c ++)
                {
                    tBlocks[p][c] = 0xEE;
                }
            }

            //--Deallocate.
            if(true)
            {
                for(int64_t p = 0; p < cNumBlocks; p ++)
                {
                    free(tBlocks[p]);
                }
                free(tBlocks);
            }

            //--Debug.
            fprintf(stderr, "Memory screw completed.\n");
        }*/
    }
}

///========================================= Libraries ============================================
bool InitializeLibraries()
{
    ///--[Documentation]
    //--Boot Allegro/SDL. If any step fails, the program will be ordered to quit. If all steps are
    //  successful, the gQuit variable is flipped back to false.
    GLOBAL *rGlobal = Global::Shared();
    DebugManager::PushPrint(rGlobal->gShowStartupDebug, "[Initializing Libraries]\n");
    rGlobal->gQuit = true;

    ///--[Allegro Classes]
    #if defined _ALLEGRO_PROJECT_
    {
        if(!al_init())
        {
            DebugManager::ForcePrint("Unable to boot Allegro, exiting.\n");
            return false;
        }
        DebugManager::Print("Booted Allegro\n");

        //--Init the addons
        #ifndef _TARGET_OS_LINUX_
            al_init_image_addon();
            DebugManager::Print("Booted Addons\n");
        #endif

        if(!al_install_keyboard())
        {
            DebugManager::ForcePrint("Unable to install Allegro keyboard, exiting.\n");
            return false;
        }
        DebugManager::Print("Booted Keyboard\n");

        if(!al_install_mouse())
        {
            DebugManager::ForcePrint("Unable to install Allegro mouse, exiting.\n");
            return false;
        }
        DebugManager::Print("Booted Mouse\n");

        if(!al_install_joystick())
        {
            DebugManager::ForcePrint("Unable to install Allegro joystick, exiting.\n");
            return false;
        }
        DebugManager::Print("Booted Joystick\n");

        rGlobal->gEventQueue = al_create_event_queue();
        if(!rGlobal->gEventQueue)
        {
            DebugManager::ForcePrint("Event queue did not initialize, exiting.\n");
            return false;
        }
        DebugManager::Print("Booted Event Queue\n");

        //--Boot the custom event source.
        al_init_user_event_source(&rGlobal->gCustomEventSource);
        al_register_event_source(rGlobal->gEventQueue, &rGlobal->gCustomEventSource);

        rGlobal->gSpeedTimer = al_create_timer(1.0f / rGlobal->gExpectedFPS);
        if(!rGlobal->gSpeedTimer)
        {
            DebugManager::ForcePrint("Speed timer failed to initialize, exiting.\n");
            return false;
        }
        DebugManager::Print("Booted Timer with FPS %f\n", rGlobal->gExpectedFPS);

        //--Set up some global settings on Allegro
        al_init_timeout(&rGlobal->gSpeedCounter, 0.06);
    }
    #endif
    ///--[SDL Classes]
    #if defined _SDL_PROJECT_

        //--Request OpenGL 4.5.
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);

        //--Boot SDL.
        uint32_t tBootFlags = SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_EVENTS;
        if(SDL_Init(tBootFlags))
        {
            DebugManager::ForcePrint("Error: SDL was unable to boot.\n");
            DebugManager::ForcePrint(" Error Code: %s\n", SDL_GetError());
            return false;
        }

        //--Initialize the timer object and register it.
        uint32_t tDelay = 16;
        SDL_AddTimer(tDelay, Timer_Callback, NULL);

        //--Boot SDL Image addon. This is currently causing odd crashing bugs, so we worked around it.
        //int tInittedTypes = IMG_Init(IMG_INIT_PNG);
        //if(tInittedTypes != IMG_INIT_PNG)
        //{
        //    DebugManager::ForcePrint("Error: SDLImage was unable to boot.\n");
        //    DebugManager::ForcePrint(" Error Code: %s\n", IMG_GetError());
        //    return false;
        //}

    #endif


    //--All checks passed!
    rGlobal->gQuit = false;
    DebugManager::PopPrint("[Libraries] Finished init\n");
    return true;
}

///========================================= Managers =============================================
bool InitializeManagers()
{
    ///--[Documentation]
    //--Create managers and other important system objects. Retruns true if it succeeded, false if it
    //  failed. If it fails, the engine has encountered a critical error and cannot run.


    //--Boot the Managers (and static objects).
    GLOBAL *rGlobal = Global::Shared();
    DebugManager::PushPrint(true, "[Begin Manager Boot]\n");

    //--StarLumpsManager, LuaManager will need this if in Tarball mode.
    rGlobal->gStarLumpManager = new StarLumpManager();

    //--Lua
    rGlobal->gLuaManager = new LuaManager();
    RegisterCoreLuaFunctions(rGlobal->gLuaManager->GetLuaState());
    RegisterEngineLuaFunctions(rGlobal->gLuaManager->GetLuaState());
    Global::Shared()->gLuaManager->ExecuteLuaFile("Bootstrap.lua");
    DebugManager::Print("Lua Manager\n");

    //--Translatable Strings
    rGlobal->gTranslationManager = new TranslationManager();
    RegisterCoreStrings();
    RegisterEngineStrings();

    //--Control Manager must be booted before OptionsManager to get its keys configured.
    rGlobal->gControlManager = new ControlManager();
    rGlobal->gControlManager->SetupControls();
    DebugManager::Print("Control Manager\n");

    rGlobal->gOptionsManager = new OptionsManager();
    rGlobal->gOptionsManager->SetupOptionList();
    DebugManager::Print("Options Manager\n");

    //--Run the Configuration file and the LoadCounter file.
    rGlobal->gLuaManager->mBypassTarballOnce = true;
    rGlobal->gLuaManager->ExecuteLuaFile("Data/Scripts/000 ConfigList.lua");
    rGlobal->gOptionsManager->RunConfigFiles();

    //--Run the override. This is only used on the developer's computer. It will fail silently if it does not exist.
    bool tOldFlag = rGlobal->gLuaManager->xFailSilently;
    rGlobal->gLuaManager->xFailSilently = true;
    rGlobal->gLuaManager->ExecuteLuaFile("DeveloperLocal/Override.lua");
    rGlobal->gLuaManager->xFailSilently = tOldFlag;

    //--RAM Loading flag for Virtual Files.
    VirtualFile::xUseRAMLoading = OptionsManager::Fetch()->GetOptionB("UseRAMLoading");

    //--If the option to disable the splash screen was set, do that here.
    if(rGlobal->gOptionsManager->GetOptionB("Disable Splash Screen"))
    {
        FlexMenu::xShowSplash = false;
    }

    #if defined _ALLEGRO_PROJECT_
    rGlobal->gLuaManager->ExecuteLuaFile("LoadCountersAL.lua");
    #else
    rGlobal->gLuaManager->ExecuteLuaFile("LoadCountersSDL.lua");
    #endif
    DebugManager::Print(" Configuration files executed.\n");
    rGlobal->gOptionsManager->PostProcess();
    DebugManager::Print(" Post Processing\n");

    //--Display must be created for a valid OpenGL context.
    rGlobal->gDisplayManager = new DisplayManager();
    if(rGlobal->gQuit) return false;
    DebugManager::Print("Display Created\n");

    //--Wrangle any unsupported extensions.
    DisplayManager::WrangleExec();
    rGlobal->gDisplayManager->ShaderExec();

    //--Swap buffer interval. Must be done after the wrangle is complete, since it's platform dependent.
    //  If you don't call this, the display may tear.
    DebugManager::Print("Setting swap interval %p\n", swglSwapIntervalEXT);
    if(swglSwapIntervalEXT) swglSwapIntervalEXT(1);
    DebugManager::Print("Wrangle completed\n");

    //--Create the StarLoadInterrupt.
    rGlobal->gLoadInterrupt = new Chapter0LoadInterrupt();
    rGlobal->gLoadInterrupt->mIsSuppressed = true;
    rGlobal->gLoadInterrupt->Construct();
    rGlobal->gLoadInterrupt->mIsSuppressed = true;
    DebugManager::Print("Created StarLoadInterrupt\n");

    //--Set the StarLoadInterrupt for the boot sequence.
    int tExpectedBootCount = rGlobal->gOptionsManager->GetOptionI("ExpectedLoad_BootSequence");
    rGlobal->gLoadInterrupt->Reset(tExpectedBootCount);

    //--DataLibrary, resources can now organize themselves.
    rGlobal->gDataLibrary = new DataLibrary();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print("Created DataLibrary\n");

    ///--[Diagnostic and System Bitmaps]
    //--Path.
    rGlobal->gDataLibrary->AddPath("Root/Images/System/System/");

    //--Create the system White Pixel. This is used for shaders or missing images.
    uint8_t cWhitePixel[4];
    cWhitePixel[0] = 255;
    cWhitePixel[1] = 255;
    cWhitePixel[2] = 255;
    cWhitePixel[3] = 255;
    uint32_t nWhitePixelHandle = StarBitmap::UploadBitmap((uint8_t *)&cWhitePixel, 1, 1, 0, StarBitmap::cNoCompression, sizeof(uint8_t) * 4);
    StarBitmap *nWhitePixel = new StarBitmap(nWhitePixelHandle);
    rGlobal->gDataLibrary->RegisterPointer("Root/Images/System/System/WhitePixel", nWhitePixel, &StarBitmap::DeleteThis);

    //--Create the system Dummy Pixel. Same as White Pixel, used for some errors.
    uint32_t nDummyPixelHandle = StarBitmap::UploadBitmap((uint8_t *)&cWhitePixel, 1, 1, 0, StarBitmap::cNoCompression, sizeof(uint8_t) * 4);
    StarBitmap *nDummyPixel = new StarBitmap(nDummyPixelHandle);
    rGlobal->gDataLibrary->RegisterPointer("Root/Images/System/System/DummyPixel", nDummyPixel, &StarBitmap::DeleteThis);

    //--Pass the white pixel to the control manager.
    rGlobal->gControlManager->SetErrorImage("Root/Images/System/System/WhitePixel");

    //--Create the system bitmap used for dummy reporting. If this image is bound or rendered, it will
    //  print a warning. Used to save space for games that need UI objects to be overloaded.
    //--This is the "Adventure" version. Each major game nominally has one to help narrow down the source
    //  of an incorrect bitmap render. Note that this refers to the object's game (AdvUIEquip, MonoUIEquip, CarnUIEquip)
    //  and not the active game (the one the user is playing). That is, autoloaders should put the adventure
    //  dummy into the AdvUI objects that are using dummies.
    StarBitmap *nDummyBitmapAdv = new StarBitmap();
    nDummyBitmapAdv->SetDummyMode("System Dummy Bitmap Adventure");
    rGlobal->gDataLibrary->RegisterPointer("Root/Images/System/System/DummyBitmapAdv", nDummyBitmapAdv, &StarBitmap::DeleteThis);

    //--Monoceros Version.
    StarBitmap *nDummyBitmapMono = new StarBitmap();
    nDummyBitmapMono->SetDummyMode("System Dummy Bitmap Monoceros");
    rGlobal->gDataLibrary->RegisterPointer("Root/Images/System/System/DummyBitmapMono", nDummyBitmapMono, &StarBitmap::DeleteThis);

    //--Carnation Version.
    StarBitmap *nDummyBitmapCarn = new StarBitmap();
    nDummyBitmapCarn->SetDummyMode("System Dummy Bitmap Carnation");
    rGlobal->gDataLibrary->RegisterPointer("Root/Images/System/System/DummyBitmapCarn", nDummyBitmapCarn, &StarBitmap::DeleteThis);

    ///--[Other Managers]
    //--Managers that can be booted in any order.
    rGlobal->gAudioManager = new AudioManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Audio Manager.\n");
    rGlobal->gCameraManager = new CameraManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Camera Manager.\n");
    rGlobal->gEntityManager = new EntityManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Entity Manager.\n");
    rGlobal->gCutsceneManager = new CutsceneManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Cutscene Manager.\n");
    rGlobal->gEntityManager->BootSpecialLists();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Booted special lists.\n");
    rGlobal->gMapManager = new MapManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Map Manager.\n");
    rGlobal->gNetworkManager = new NetworkManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Network Manager.\n");
    rGlobal->gResetManager = new ResetManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Reset Manager.\n");
    rGlobal->gSaveManager = new SaveManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Save Manager.\n");
    rGlobal->gSteamManager = new SteamManager();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print(" Created Steam Manager.\n");
    DebugManager::Print("Created Misc Managers\n");

    //--Set audio volumes from configuration.
    int cMusicVolume = rGlobal->gOptionsManager->GetOptionI("MusicVolume");
    int cSoundVolume = rGlobal->gOptionsManager->GetOptionI("SoundVolume");
    rGlobal->gAudioManager->ChangeMusicVolumeTo(cMusicVolume / 100.0f);
    rGlobal->gAudioManager->ChangeSoundVolumeTo(cSoundVolume / 100.0f);
    for(int i = 0; i < AM_ABSTRACT_VOLUME_TOTAL; i ++)
    {
        char tBuf[128];
        sprintf(tBuf, "AbstractVolume%02i", i);
        int tAbstractVolume = rGlobal->gOptionsManager->GetOptionI(tBuf);
        rGlobal->gAudioManager->ChangeAbstractVolumeTo(i, tAbstractVolume / 100.0f);
    }

    //--<DEBUG>
    DebugManager::PopPrint("[Complete Manager Boot]\n");

    ///--[Debug Flags]
    //--Set all debug flags to their default existence. They can be modified later.
    DebugManager::RegisterDebugFlag("KPopDance", false);

    return true;
}

///===================================== Events, Resources ========================================
#if defined _ALLEGRO_PROJECT_
void RegisterEvents(ALLEGRO_EVENT_QUEUE *EQ)
{
    //--Register the Events that Allegro will handle. SDL does not need this.
    GLOBAL *rGlobal = Global::Shared();
    al_register_event_source(EQ, al_get_display_event_source(rGlobal->gDisplay));
    al_register_event_source(EQ, al_get_timer_event_source(rGlobal->gSpeedTimer));
    al_register_event_source(EQ, al_get_keyboard_event_source());
    al_register_event_source(EQ, al_get_mouse_event_source());
    al_register_event_source(EQ, al_get_joystick_event_source());
}
#endif
void LoadResources()
{
    //--Any resources which need to be loaded for program duration are booted here.  Some are
    //  on-the-fly resources which may only be defined here.
    GLOBAL *rGlobal = Global::Shared();
    DebugManager::PushPrint(true, "[Loading Resources]\n");
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    rGlobal->gLuaManager->ExecuteLuaFile("Data/Scripts/100 Startup.lua");
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::PopPrint("[Done Loading Resources]\n");
}

///==================================== Post-Setup Process ========================================
void PostProcess()
{
    //--After all the Managers have been created, and all the resources have been loaded, some objects
    //  need to be created which rely on both.  Do that here.
    GLOBAL *rGlobal = Global::Shared();
    DebugManager::PushPrint(true, "[Post Process] Begin\n");

    //--Construct objects that need it.
    PlayerPony::Fetch()->Construct();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();

    //--Boot Freetype.
    FT_Init_FreeType(&StarFont::xFTLibrary);
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    DebugManager::Print("Booted freetype\n");

    //--Fonts.
    ProgramFlexibleResources();
    DebugManager::Print("Finished flexible resource load.\n");
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();

    //--Give the ControlManager initial mouse information.
    ControlManager::Fetch()->RefreshMousePosition();
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();

    //--Options Manager report
    rGlobal->gOptionsManager->PrintReport();

    //--StarLoadInterrupt prints finishing report.
    //StarLoadInterrupt::Fetch()->Finish();
    DebugManager::Print("Printed loading report\n");

    //--Debug
    DebugManager::PopPrint("[Post Process] Complete\n");
}

///======================================= Program Boot ===========================================
void ProgramBoot()
{
    ///--[Documenation]
    //--ProgramBoot() is a program-specific startup routine. It is at this point that your game
    //  should call any initializers before the title or gameplay.

    ///--[Allegro Code]
    //--Allegro can load the image into an ALLEGRO_BITMAP.
    #if defined _ALLEGRO_PROJECT_

        //--Disable this for the benefit of Linux.
        #if defined _TARGET_OS_WINDOWS_
        {
            //--Open the UI.slf file.
            StarLumpManager *rSLM = StarLumpManager::Fetch();
            rSLM->Open("Data/UISystem.slf");
            if(rSLM->IsFileOpen())
            {
                //--Get the raw image data out. We want it raw, NOT in StarBitmap format.
                StarBitmapPrecache *tPrecache = rSLM->GetImageData("Icon");
                if(tPrecache)
                {
                    //--Create a memory-sided bitmap. Upload the data.
                    ALLEGRO_BITMAP *tNewBitmap = al_create_bitmap(tPrecache->mWidth, tPrecache->mHeight);
                    //ALLEGRO_LOCKED_REGION *rRegionLock = al_lock_bitmap(tNewBitmap, ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_WRITEONLY);
                    al_lock_bitmap(tNewBitmap, ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_WRITEONLY);
                    al_set_target_bitmap(tNewBitmap);
                    int tCursor = 0;
                    for(int y = 0; y < tPrecache->mHeight; y ++)
                    {
                        for(int x = 0; x < tPrecache->mWidth; x ++)
                        {
                            al_put_pixel(x, y, al_map_rgba(tPrecache->mArray[tCursor+0], tPrecache->mArray[tCursor+1], tPrecache->mArray[tCursor+2], tPrecache->mArray[tCursor+3]));
                            tCursor += 4;
                        }
                    }
                    al_unlock_bitmap(tNewBitmap);
                    al_set_target_bitmap(NULL);

                    //--Turn it into the icon.
                    al_set_display_icon(Global::Shared()->gDisplay, tNewBitmap);

                    //--Clean.
                    al_destroy_bitmap(tNewBitmap);
                    free(tPrecache->mArray);
                    free(tPrecache->mHitboxes);
                    free(tPrecache);
                    al_set_target_backbuffer(Global::Shared()->gDisplay);
                }
                //--Error report.
                else
                {
                    DebugManager::ForcePrint("SDL Error: Couldn't find Icon in UI.slf.\n");
                }
            }

            //--Finish up.
            rSLM->Close();
            if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
        }
        #endif
    #endif

    ///--[SDL Code]
    //--In SDL mode, use the non-standard program icon. Allegro doesn't need to do this at all.
    #if defined _SDL_PROJECT_

        //--Open the UI.slf file.
        StarLumpManager *rSLM = StarLumpManager::Fetch();
        rSLM->Open("Data/UISystem.slf");
        if(rSLM->IsFileOpen())
        {
            //--Get the raw image data out. We want it raw, NOT in StarBitmap format.
            StarBitmapPrecache *tPrecache = rSLM->GetImageData("Icon");
            if(tPrecache)
            {
                //--Create a memory-sided bitmap.
                SDL_Surface *tSurface = SDL_CreateRGBSurfaceFrom(tPrecache->mArray, tPrecache->mWidth, tPrecache->mHeight, 32, tPrecache->mWidth*4, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);

                //--Turn it into the icon.
                if(tSurface)
                {
                    SDL_SetWindowIcon(DisplayManager::Fetch()->GetWindow(), tSurface);
                    SDL_FreeSurface(tSurface);
                }
                //--Error.
                else
                {
                    DebugManager::ForcePrint("SDL Error: Couldn't build an SDL_Surface from the Icon bitmap.\n");
                    DebugManager::ForcePrint(" Compression Flag: %i.\n", tPrecache->mCompressionType);
                    DebugManager::ForcePrint(" Sizes: %ix%i.\n", tPrecache->mWidth, tPrecache->mHeight);
                    DebugManager::ForcePrint(" Data Length: %i.\n", tPrecache->mDataSize);
                }

                //--Clean.
                free(tPrecache->mArray);
                free(tPrecache->mHitboxes);
                free(tPrecache);
            }
            //--Error report.
            else
            {
                DebugManager::ForcePrint("SDL Error: Couldn't find Icon in UI.slf.\n");
            }
        }

        //--Finish up.
        rSLM->Close();
        if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    #endif

    //--Basic reset back to the main menu.
    ResetManager *rResetManager = ResetManager::Fetch();
    ResetList *rFullReset = rResetManager->AllocateReset("Full Game Reset", 5);
    rFullReset->mResetList[0] = EntityManager::Fetch();
    rFullReset->mResetList[1] = MapManager::Fetch();
    rFullReset->mResetList[2] = EntityManager::Fetch();
    rFullReset->mResetList[3] = CameraManager::Fetch();
    rFullReset->mResetList[4] = CutsceneManager::Fetch();

    //--Combat reset. Wipes all data from the AdventureCombatUI. Obviously, don't use it while that's active, idiot.
    ResetList *rCombatReset = rResetManager->AllocateReset("Combat", 1);
    rCombatReset->mResetList[0] = MapManager::Fetch();

    //--Run the main menu to allow the player to select what to do.
    fprintf(stderr, "Flexible boot.\n");
    ProgramFlexibleBoot();

    ///--[Load Complete]
    //--Store how many interrupt calls there were for future executions.
    GLOBAL *rGlobal = Global::Shared();
    rGlobal->gLoadInterrupt->Finish();
    int tCallsSoFar = rGlobal->gLoadInterrupt->GetCallsSoFar();
    rGlobal->gOptionsManager->SetOptionI("ExpectedLoad_BootSequence", tCallsSoFar);

    //--Write it to the matching file, based on the core library.
    #if defined _ALLEGRO_PROJECT_
    rGlobal->gOptionsManager->WriteLoadFile("LoadCountersAL.lua");
    #else
    rGlobal->gOptionsManager->WriteLoadFile("LoadCountersSDL.lua");
    #endif
}
#include "FlexMenu.h"
void ProgramFlexibleBoot()
{
    ///--[Documentation and Setup]
    //--End of the startup chain. Can be re-used by the ResetManager to start the program. Presently
    //  builds the main menu for execution.
    //--Store if we're doing a splash screen.
    bool tIsDoingSplash = FlexMenu::xShowSplash;

    //--Create a main menu and pass it to the MapManager.
    fprintf(stderr, "Starting flexible boot.\n");
    FlexMenu *nMainMenu = Global::Shared()->rCreateTitleMenu();
    MapManager::Fetch()->PushMenuStack(nMainMenu);
    LuaManager::Fetch()->PushExecPop(nMainMenu, "Data/Scripts/MainMenu/000 PopulateMainMenu.lua");

    //--If we're not doing a splash screen, cause the MapManager to fade in here.
    if(!tIsDoingSplash)
    {
        MapManager::Fetch()->BeginFade();
    }

    //--Finish up.
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    fprintf(stderr, "Completed flexible boot.\n");
}
void ProgramFlexibleResources()
{
    ///--[Documentation and Setup]
    //--Used when the program has switched display modes, loads default resources to where they are
    //  supposed to go. An important example is the system font(s).
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    GLOBAL *rGlobal = Global::Shared();

    ///--[Create System Fonts]
    //--Boot a new font.
    rGlobal->gSystemFont = new StarFont();
    rGlobal->gSystemFont->ConstructWith("Data/UIFont.ttf", 0, SUGARFONT_PRECACHE_WITH_NEAREST);

    //--Bitmap font.
    rGlobal->gBitmapFont = new StarFont();
    rGlobal->gBitmapFont->SetInternalBitmap((StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/UIFontSmall"));
    rGlobal->gBitmapFont->RunVariableWidthScript("Data/Graphics/UIFontSmall.lua");
    rGlobal->gBitmapFont->SetInternalName("System Bitmap Font");
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();

    //--Alternate, smaller UI font.
    StarFont *nSmallFont = new StarFont();
    nSmallFont->SetInternalBitmap((StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/UIFontVSmall"));
    nSmallFont->RunVariableWidthScript("Data/Graphics/UIFontVSmall.lua");
    nSmallFont->SetInternalName("System Bitmap Font Small");
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();

    //--Register these fonts. The EngineFonts file can register aliases.
    rDataLibrary->RegisterFont("System Font",       rGlobal->gSystemFont, "Null");
    rDataLibrary->RegisterFont("Bitmap Font",       rGlobal->gBitmapFont, "Null");
    rDataLibrary->RegisterFont("Bitmap Font Small", nSmallFont,           "Null");

    //--Debug.
    DebugManager::Print("Set system fonts.\n");

    ///--[Engine Font Script]
    //--Boot other fonts that may be used by the main screen, and build aliases.
    LuaManager::Fetch()->ExecuteLuaFile("Data/Scripts/Fonts/EngineFonts.lua");
    DebugManager::Print("Built engine fonts.\n");

    ///--[Defaults]
    //--Reset all font defaults.
    StarFont::SetDownfade(-1.0f, -1.0f);
    StarFont::SetOutlineWidth(-1);

    ///--[Edging Utility]
    if(false)
    {
        StarBitmap *nInternalBitmap = new StarBitmap("Data/Graphics/UIFontVSmall.png");
        StarFont *nFont = new StarFont();
        nFont->SetInternalBitmap(nInternalBitmap);
        nFont->RunVariableWidthScript("Data/Graphics/UIFontVSmall.lua");
        rGlobal->gQuit = true;
    }

    //--Debug: Auto Folder Maker.
    //StarFileSystem::AutoFolderMaker();

    //--Debug: Mod Parser
    if(false)
    {
        ModParser *tModParser = new ModParser();
        tModParser->ParseFile("Modlist.txt");
        delete tModParser;
    }
}

///--[SDL Projects]
//--SDL does not automatically have a timer event, so this callback creates one. This callback
//  should be attached to an SDL timer object.
#if defined _SDL_PROJECT_
#ifndef _TIMER_CALLBACK_
#define _TIMER_CALLBACK_
uint32_t Timer_Callback(uint32_t pInterval, void *pParameters)
{
    //--Setup.
    SDL_Event nEvent;
    SDL_UserEvent nUserEvent;

    //--Set up the userevent.
    nUserEvent.type = SDL_USEREVENT;
    nUserEvent.code = 0;
    nUserEvent.data1 = NULL;
    nUserEvent.data2 = NULL;

    //--Associate the event with the userevent.
    nEvent.type = SDL_USEREVENT;
    nEvent.user = nUserEvent;

    //--Push it onto the stack.
    SDL_PushEvent(&nEvent);
    return(pInterval);
}
#endif // _TIMER_CALLBACK_
#endif
