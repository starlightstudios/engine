//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatJob.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

bool AdvCombatEntity::DoesSecondPageHaveAbilities()
{
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rSecondaryGrid[x][y]) return true;
        }
    }
    return false;
}
bool AdvCombatEntity::IsAbilityEquipped(AdvCombatAbility *pCheckAbility)
{
    if(!pCheckAbility) return false;
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y] == pCheckAbility) return true;
        }
    }
    return false;
}
bool AdvCombatEntity::IsAbilityEquippedS(const char *pName)
{
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(!rAbilityGrid[x][y]) continue;
            if(!strcasecmp(rAbilityGrid[x][y]->GetInternalName(), pName))
            {
                return true;
            }
        }
    }
    return false;
}
bool AdvCombatEntity::IsAbilityEquippedI(uint32_t pID)
{
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(!rAbilityGrid[x][y]) continue;
            if(rAbilityGrid[x][y]->GetID() == pID) return true;
        }
    }
    return false;
}
const char *AdvCombatEntity::GetAbilityNameInSlot(int pX, int pY)
{
    //--Returns "Null" if out of range or slot is empty.
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return "Null";
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return "Null";
    if(!rAbilityGrid[pX][pY]) return "Null";
    return rAbilityGrid[pX][pY]->GetInternalName();
}
int AdvCombatEntity::GetProbabilityInSlot(int pX, int pY)
{
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return 0;
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return 0;
    if(!rAbilityGrid[pX][pY]) return 0;
    return mAbilityProbabilityGrid[pX][pY];
}
void AdvCombatEntity::RegisterAbility(const char *pAbilityName, AdvCombatAbility *pAbility)
{
    //--Registers a new ability and takes explicit ownership of it.
    if(!pAbilityName || !pAbility) return;
    pAbility->SetOwner(this);
    pAbility->SetInternalName(pAbilityName);
    mAbilityList->AddElement(pAbilityName, pAbility, &RootObject::DeleteThis);
}
void AdvCombatEntity::RegisterAbilityToJob(const char *pAbilityName, const char *pJobName, int pJPCost)
{
    //--Registers the ability to the given job, allowing it to be purchased in the skills UI.
    if(!pAbilityName || !pJobName) return;

    //--Get and check.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->GetElementByName(pJobName);
    if(!rAbility || !rJob)
    {
        fprintf(stderr, "AdvCombatEntity::RegisterAbilityToJob() - Warning, either the job %s %p or ability %s %p was not found. Failing.\n", pJobName, rJob, pAbilityName, rAbility);
        return;
    }

    //--Register.
    rJob->RegisterAbility(rAbility);
}
void AdvCombatEntity::UnregisterAbilityFromJob(const char *pAbilityName, const char *pJobName)
{
    ///--[Documentation]
    //--Removes the given ability from the given job, preventing it from being seen on the skills UI.
    if(!pAbilityName || !pJobName) return;

    //--Get the ability pointer.
    AdvCombatAbility *rCheckAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    if(!rCheckAbility) return;

    //--Check the job.
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->GetElementByName(pJobName);
    if(!rJob || !rCheckAbility)
    {
        fprintf(stderr, "AdvCombatEntity::UnregisterAbilityFromJob() - Warning, job %s %p or ability %s %p not found. Failing.\n", pJobName, rJob, pAbilityName, rCheckAbility);
        return;
    }

    //--Unregister.
    rJob->UnregisterAbility(rCheckAbility);
}
void AdvCombatEntity::RemoveAbility(const char *pAbilityName)
{
    //--Locate the ability.
    if(!pAbilityName) return;
    AdvCombatAbility *rCheckAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    if(!rCheckAbility) return;

    //--Remove it from any equipped slots.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y] == rCheckAbility) rAbilityGrid[x][y] = NULL;
        }
    }

    //--Unregister it from any jobs that reference it.
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->PushIterator();
    while(rJob)
    {
        rJob->UnregisterAbility(rCheckAbility);
        rJob = (AdvCombatJob *)mJobList->AutoIterate();
    }

    //--Remove it from the list.
    mAbilityList->RemoveElementP(rCheckAbility);
    mrPassiveAbilityList->RemoveElementP(rCheckAbility);
}
void AdvCombatEntity::SetAbilitySlot(int pSlotX, int pSlotY, const char *pAbilityName)
{
    ///--[Documentation]
    //--Sets which ability goes in this slot. Fails if the slot is out of range, but not if the ability
    //  name is NULL. Passing NULL or "Null" for an ability name will clear the slot.

    //--Range check.
    if(pSlotX < 0 || pSlotX >= ACE_ABILITY_GRID_SIZE_X)
    {
        fprintf(stderr, "AdvCombatEntity:SetAbilitySlot: Warning, slot X was out of range (%i vs %i to %i)\n", pSlotX, 0, ACE_ABILITY_GRID_SIZE_X);
        fprintf(stderr, " %s %s\n", mLocalName, pAbilityName);
        return;
    }
    if(pSlotY < 0 || pSlotY >= ACE_ABILITY_GRID_SIZE_Y)
    {
        fprintf(stderr, "AdvCombatEntity:SetAbilitySlot: Warning, slot Y was out of range (%i vs %i to %i)\n", pSlotY, 0, ACE_ABILITY_GRID_SIZE_Y);
        fprintf(stderr, " %s %s\n", mLocalName, pAbilityName);
        return;
    }

    //--Store the player's HP percent, in case the ability affects the HP total.
    float tHPPercent = GetHealthPercent();

    //--If an ability was already in the slot, mark it as not equipped.
    if(rAbilityGrid[pSlotX][pSlotY])
    {
        rAbilityGrid[pSlotX][pSlotY]->SetEquipped(false);
    }

    //--Special: If the ability is "System|Retreat" or "System|Surrender", those are acquired from the AdvCombat class.
    if(pAbilityName && !strcasecmp(pAbilityName, "System|Retreat"))
    {
        rAbilityGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemRetreat();
        return;
    }
    else if(pAbilityName && !strcasecmp(pAbilityName, "System|Surrender"))
    {
        rAbilityGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemSurrender();
        return;
    }
    else if(pAbilityName && !strcasecmp(pAbilityName, "System|Volunteer"))
    {
        rAbilityGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemVolunteer();
        return;
    }

    //--Get the ability. If it comes back NULL, or "Null" is passed in, we can still set the slot to null
    //  to indicate it is empty.
    AdvCombatAbility *rAbility = NULL;
    if(pAbilityName && strcasecmp(pAbilityName, "Null")) rAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    rAbilityGrid[pSlotX][pSlotY] = rAbility;
    if(rAbilityGrid[pSlotX][pSlotY]) rAbilityGrid[pSlotX][pSlotY]->SetEquipped(true);

    //--Set the HP.
    SetHealthPercent(tHPPercent, false);
}
void AdvCombatEntity::SaveCurrentAbilitiesToActiveProfile()
{
    //--Takes the active profile and synchronizes the abilities currently equipped with it. If no active
    //  profile is found, uses the zeroth profile.
    if(!rActiveProfile) rActiveProfile = (AbilityProfile *)mProfilesList->GetHead();
    if(!rActiveProfile) return;

    //--Iterate, sync.
    for(int x = 0; x < ACE_ABILITY_GRID_PROFILE_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_PROFILE_SIZE_Y; y ++)
        {
            //--Ability.
            AdvCombatAbility *rAbility = rAbilityGrid[x+ACE_ABILITY_GRID_PROFILE_OFFSET_X][y+ACE_ABILITY_GRID_PROFILE_OFFSET_Y];

            //--No ability in slot:
            if(!rAbility)
            {
                ResetString(rActiveProfile->mAbilityNames[x][y], NULL);
                rActiveProfile->rAbilityPtrs[x][y] = NULL;
            }
            //--Ability in slot.
            else
            {
                ResetString(rActiveProfile->mAbilityNames[x][y], rAbility->GetInternalName());
                rActiveProfile->rAbilityPtrs[x][y] = rAbility;
            }
        }
    }
}
void AdvCombatEntity::SetAbilitySlotProbability(int pSlotX, int pSlotY, int pProbability)
{
    //--Range check.
    if(pSlotX < 0 || pSlotX >= ACE_ABILITY_GRID_SIZE_X) return;
    if(pSlotY < 0 || pSlotY >= ACE_ABILITY_GRID_SIZE_Y) return;

    //--Set.
    mAbilityProbabilityGrid[pSlotX][pSlotY] = pProbability;
}
void AdvCombatEntity::SetSecondarySlot(int pSlotX, int pSlotY, const char *pAbilityName)
{
    //--Range check.
    if(pSlotX < 0 || pSlotX >= ACE_ABILITY_GRID_SIZE_X) return;
    if(pSlotY < 0 || pSlotY >= ACE_ABILITY_GRID_SIZE_Y) return;

    //--If an ability was already in the slot, mark it as not equipped.
    if(rSecondaryGrid[pSlotX][pSlotY])
    {
        rSecondaryGrid[pSlotX][pSlotY]->SetEquipped(false);
    }

    //--Special: If the ability is "System|Retreat" or "System|Surrender", those are acquired from the AdvCombat class.
    if(!strcasecmp(pAbilityName, "System|Retreat"))
    {
        rSecondaryGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemRetreat();
        return;
    }
    else if(!strcasecmp(pAbilityName, "System|Surrender"))
    {
        rSecondaryGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemSurrender();
        return;
    }

    //--Get the ability. If it comes back NULL, or "Null" is passed in, we can still set the slot to null
    //  to indicate it is empty.
    AdvCombatAbility *rAbility = NULL;
    if(strcasecmp(pAbilityName, "Null")) rAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    rSecondaryGrid[pSlotX][pSlotY] = rAbility;
    if(rSecondaryGrid[pSlotX][pSlotY]) rSecondaryGrid[pSlotX][pSlotY]->SetEquipped(true);
}
void AdvCombatEntity::AddPassiveAbility(const char *pAbilityName)
{
    AdvCombatAbility *rCheckPtr = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    if(rCheckPtr) mrPassiveAbilityList->AddElementAsTail(pAbilityName, rCheckPtr);
}
void AdvCombatEntity::RunAbilityScripts(int pCode)
{
    ///--[Docoumentation and Setup]
    //--All abilities not on the ability grid decrement their cooldown if applicable. Those on the ability
    //  grid run more advanced logic.
    AdvCombatAbility::xLastCallNumber ++;

    ///--[Equipped Grid]
    //--Run. All responding abilities will update their last call number to match xLastCallNumber.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y])   rAbilityGrid[x][y]->CallCode(pCode);
            if(rSecondaryGrid[x][y]) rSecondaryGrid[x][y]->CallCode(pCode);
        }
    }

    ///--[Unequipped]
    //--Iterate across all abilities. Any which has a lower last-call is not equipped. It just decrements its
    //  cooldown if applicable.
    if(pCode == ACA_SCRIPT_CODE_BEGINACTION && AdvCombat::Fetch()->GetActingEntity() == this)
    {
        AdvCombatAbility *rAbility = (AdvCombatAbility *)mAbilityList->PushIterator();
        while(rAbility)
        {
            //--Ability has a call number lower than the static. It's not equipped, decrement.
            if(rAbility->GetLastCallNumber() < AdvCombatAbility::xLastCallNumber)
            {
                //--If this flag is set, the ability runs its full logic even if not equipped. It still won't
                //  run again if it already ran when equipped.
                if(rAbility->RunsIfNotEquipped())
                {
                    //--If this flag is true, the ability must at least be unlocked.
                    if(rAbility->RunsOnlyIfUnlocked())
                    {
                        if(rAbility->IsUnlocked()) rAbility->CallCode(pCode);
                    }
                    //--Flag is not true, run it.
                    else
                    {
                        rAbility->CallCode(pCode);
                    }
                }
                //--Just run the cooldown.
                else
                {
                    int tCurrentCooldown = rAbility->GetCooldown();
                    if(tCurrentCooldown > 0) rAbility->SetCooldown(tCurrentCooldown - 1);
                }
            }

            //--Next.
            rAbility = (AdvCombatAbility *)mAbilityList->AutoIterate();
        }
    }
    //--If the code is something else and the ability runs when not equipped, run it.
    else
    {
        AdvCombatAbility *rAbility = (AdvCombatAbility *)mAbilityList->PushIterator();
        while(rAbility)
        {
            //--Ability has a call number lower than the static. It's not equipped, decrement.
            if(rAbility->GetLastCallNumber() < AdvCombatAbility::xLastCallNumber)
            {
                //--If this flag is set, the ability runs its full logic even if not equipped. It still won't
                //  run again if it already ran when equipped.
                if(rAbility->RunsIfNotEquipped())
                {
                    //--If this flag is true, the ability must at least be unlocked.
                    if(rAbility->RunsOnlyIfUnlocked())
                    {
                        if(rAbility->IsUnlocked()) rAbility->CallCode(pCode);
                    }
                    //--Flag is not true, run it.
                    else
                    {
                        rAbility->CallCode(pCode);
                    }
                }
            }

            //--Next.
            rAbility = (AdvCombatAbility *)mAbilityList->AutoIterate();
        }
    }
}
void AdvCombatEntity::PrintAbilitiesToConsole()
{
    ///--[Documentation]
    //--Prints a list of abilities currently held by this entity to the console. Used for debug.
    fprintf(stderr, "Listing abilities for %s\n", mLocalName);
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mAbilityList->PushIterator();
    while(rAbility)
    {
        //--List.
        fprintf(stderr, " %s - %s\n", mAbilityList->GetIteratorName(), rAbility->GetInternalName());

        //--Next.
        rAbility = (AdvCombatAbility *)mAbilityList->AutoIterate();
    }
}
