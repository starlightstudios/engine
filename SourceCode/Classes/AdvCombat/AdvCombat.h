///========================================= AdvCombat ============================================
//--Adventure Mode's combat engine.

//--Party Notes:
//--The player's party is split into the Roster, the Active Party, and the Combat Party.
//--The Roster is all of the player's party members, including ones not in the party. This is
//  used when switching party members later in the game.
//--The Active Party is the characters in the player's party out of combat.
//--The Combat Party is the characters under the player's control in combat. This starts as a
//  copy of the Active Party, but enemies can change sides and the player's party can change sides.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "DisplayManager.h"
#include "AdvCombatDefStruct.h"
#include "AudioManager.h"

///===================================== Local Structures =========================================
///--[AdvVictoryItemPack]
//--Represents an item the player gets in combat. These have a timer associated so they pop up over
//  time, adding flair.
typedef struct AdvVictoryItemPack
{
    const char *rItemNameRef;
    StarBitmap *rItemImg;
    int mTimer;
    void Initialize()
    {
        rItemNameRef = NULL;
        rItemImg = NULL;
        mTimer = 0;
    }
}AdvVictoryItemPack;

///--[Turn Order Rendering Pack]
//--Represents an entry on the turn order list, along with image and mixing values.
typedef struct TurnOrderRenderPack
{
    float mXPos;
    float mYPos;
    float mScale;
    float mMixer;
    float mAlpha;
    StarBitmap *rImage;
    StarBitmap *rBacking;
}TurnOrderRenderPack;

///--[Turn Order Sorting Pack]
//--Represents an entry in turn order when it is being rolled. Contains an entity and their INI score.
typedef struct TurnOrderRollPack
{
    int mInitiative;
    AdvCombatEntity *rEntity;
}TurnOrderRollPack;

///===================================== Local Definitions ========================================
//--Timers
#define ADVCOMBAT_HIGHLIGHT_MOVE_TICKS 10
#define ADVCOMBAT_CONFIRMATION_TICKS 15
#define ADVCOMBAT_STD_MOVE_TICKS 25
#define ADVCOMBAT_MOVE_TRANSITION_TICKS 25
#define ADVCOMBAT_MOVE_TARGET_TICKS 12
#define ADVCOMBAT_TARGET_FLASH_PERIOD 30
#define ADVCOMBAT_TITLE_TICKS_STANDARD 120
#define ADVCOMBAT_INFIELD_TEXT_TICKS 30
#define ADVCOMBAT_EVENT_TICKS_MINIMUM 25
#define ADVCOMBAT_VICTORY_PACK_TICKS 15
#define ADVCOMBAT_PAGE_CHANGE_TICKS 5
#define ADVCOMBAT_PLAYER_INTERFACE_TICKS 7
#define ADVCOMBAT_ARROW_OSCILLATE_TICKS 120
#define ADVCOMBAT_MIN_EVENT_ENDING_TICKS 30
#define ADVCOMBAT_DESCRIPTION_TICKS 15

//--Rendering
#define ADVCOMBAT_STD_BACKING_OPACITY 0.75f
#define ADVCOMBAT_ARROW_OSCILLATE_DISTANCE 3.0f
#define ADVCOMBAT_ADVANCED_CP_FRAMES 8
#define ADVCOMBAT_ADVANCED_CP_BUFFER 20
#define ADVCOMBAT_ADVANCED_CPFRAME_TOTAL 4
#define ADVCOMBAT_DESCRIPTION_OFFSET 400.0f

//--Party
#define ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE 4

//--Combat Resolution Types
#define ADVCOMBAT_END_NONE 0
#define ADVCOMBAT_END_VICTORY 1
#define ADVCOMBAT_END_DEFEAT 2
#define ADVCOMBAT_END_RETREAT 3
#define ADVCOMBAT_END_SURRENDER 4
#define ADVCOMBAT_END_TOTAL 5

//--Victory
#define ADVCOMBAT_VICTORY_OVERLAY 0
#define ADVCOMBAT_VICTORY_ZOOM 1
#define ADVCOMBAT_VICTORY_SLIDE 2
#define ADVCOMBAT_VICTORY_COUNT_OFF 3
#define ADVCOMBAT_VICTORY_DOCTOR 4
#define ADVCOMBAT_VICTORY_HIDE 5

#define ADVCOMBAT_VICTORY_OVERLAY_TICKS 42
#define ADVCOMBAT_VICTORY_OVERLAY_TICKS_PER_LETTER 4
#define ADVCOMBAT_VICTORY_ZOOM_TICKS 30
#define ADVCBOMAT_VICTORY_SLIDE_TICKS 30
#define ADVCBOMAT_VICTORY_AWARD_TICKS 45
#define ADVCBOMAT_VICTORY_DOCTOR_TICKS 45
#define ADVCBOMAT_VICTORY_HIDE_TICKS 30

#define ADVCOMBAT_VICTORY_MASK_START 1

//--Defeat
#define ADVCOMBAT_DEFEAT_FRAMES_TOTAL 6
#define ADVCOMBAT_DEFEAT_TICKS_PER_LETTER 15
#define ADVCOMBAT_DEFEAT_TICKS_LETTERS (ADVCOMBAT_DEFEAT_TICKS_PER_LETTER * 6)
#define ADVCOMBAT_DEFEAT_TICKS_HOLD 45
#define ADVCOMBAT_DEFEAT_TICKS_FADEOUT 45

//--Retreat
#define ADVCOMBAT_RETREAT_TICKS 30

//--Positions
#define ADVCOMBAT_POSITION_OFFSCREEN_LFT -100.0f
#define ADVCOMBAT_POSITION_INTRO_OFFSCREEN (VIRTUAL_CANVAS_X + 10.0f)
#define ADVCOMBAT_POSITION_STD_Y -50.0f
#define ADVCOMBAT_POSITION_ABILITY_X 269.0f
#define ADVCOMBAT_POSITION_ABILITY_Y 526.0f
#define ADVCOMBAT_POSITION_ABILITY_W 54.0f
#define ADVCOMBAT_POSITION_ABILITY_H 70.0f

//--Position Types
#define ADVCOMBAT_POSTYPE_PRIMARY_TEXT 0
#define ADVCOMBAT_POSTYPE_SECONDARY_TEXT 1
#define ADVCOMBAT_POSTYPE_ANIMATION 2

//--Responses
#define ADVCOMBAT_RESPONSE_GENERAL_BEGINCOMBAT 0
#define ADVCOMBAT_RESPONSE_GENERAL_BEGINTURN_PREINITIATIVE 1
#define ADVCOMBAT_RESPONSE_GENERAL_BEGINTURN_POSTINITIATIVE 2
#define ADVCOMBAT_RESPONSE_GENERAL_BEGINTURN_POSTRESPONSES 3
#define ADVCOMBAT_RESPONSE_GENERAL_BEGINACTION 4

#define ADVCOMBAT_RESPONSE_TYPE_ENTITY 0
#define ADVCOMBAT_RESPONSE_TYPE_AI 1
#define ADVCOMBAT_RESPONSE_TYPE_JOB 2
#define ADVCOMBAT_RESPONSE_TYPE_ABILITY 3
#define ADVCOMBAT_RESPONSE_TYPE_EFFECT 4
#define ADVCOMBAT_RESPONSE_TYPE_TOTAL 5

#define ADVCOMBAT_RESPONSE_BEGINCOMBAT 0
#define ADVCOMBAT_RESPONSE_BEGINTURN 1
#define ADVCOMBAT_RESPONSE_BEGINTURNPOSTINITIATIVE 1
#define ADVCOMBAT_RESPONSE_BEGINTURNPOSTRESPONSES 100
#define ADVCOMBAT_RESPONSE_BEGINACTION 2
#define ADVCOMBAT_RESPONSE_BEGINFREEACTION 3
#define ADVCOMBAT_RESPONSE_ENDACTION 4
#define ADVCOMBAT_RESPONSE_ENDTURN 5
#define ADVCOMBAT_RESPONSE_ENDCOMBAT 6
#define ADVCOMBAT_RESPONSE_QUEUEEVENT 7
#define ADVCOMBAT_RESPONSE_AIKNOCKOUT 8
#define ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS 9
#define ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE 10
#define ADVCOMBAT_RESPONSE_ABILITY_EXECUTE 11
#define ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT 12
#define ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX 13
#define ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS 14
#define ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS 15
#define ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY 16
#define ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY 17
#define ADVCOMBAT_RESPONSE_TOTAL 18

//--Field Abilities
#define ADVCOMBAT_FIELD_ABILITY_SLOTS 5

//--Catalyst
#define ADVCOMBAT_CATALYST_MAX 6

//--Paragons
#define ADVCOMBAT_PARAGON_NO 0
#define ADVCOMBAT_PARAGON_YES 1
#define ADVCOMBAT_PARAGON_DESPAWN 2

//--Ability Block Types
#define ADVCOMBAT_ABILITY_BLOCK_JOB 0
#define ADVCOMBAT_ABILITY_BLOCK_MEMORIZED 1
#define ADVCOMBAT_ABILITY_BLOCK_TACTICS 2

//--Descriptions
#define ADVCOMBAT_DESCRIPTIONS_SIMPLE 0
#define ADVCOMBAT_DESCRIPTIONS_COMPLEX 1
#define ADVCOMBAT_DESCRIPTIONS_HIDDEN 2

///--[Player Input]
//--Input Modes
#define ADVCOMBAT_INPUT_CARDS 0
#define ADVCOMBAT_INPUT_JOB 1
#define ADVCOMBAT_INPUT_MEMORIZED 2
#define ADVCOMBAT_INPUT_TACTICS 3

//--Input Cards
#define ADVCOMBAT_INPUT_CARD_ATTACK 0
#define ADVCOMBAT_INPUT_CARD_JOB 1
#define ADVCOMBAT_INPUT_CARD_MEMORIZED 2
#define ADVCOMBAT_INPUT_CARD_TACTICS 3
#define ADVCOMBAT_INPUT_CARD_TOTAL 4

///--[Combat Inspector Definitions]
//--Combat Inspector Timers
#define ADVCOMBAT_INSPECTOR_TICKS 15
#define ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS 7
#define ADVCOMBAT_INSPECTOR_ABILITY_SWITCH_TICKS 7

//--Combat Inspector Sizes
#define ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE 11
#define ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE 13
#define ADVCOMBAT_INSPECTOR_MAINLINE_H 22.0f

//--Combat Inspector Modes
#define ADVCOMBAT_INSPECTOR_CHARSELECT 0
#define ADVCOMBAT_INSPECTOR_EFFECTS 1
#define ADVCOMBAT_INSPECTOR_ABILITIES 2

//--Combat Inspector Abilities Types
#define ADVCOMBAT_INSPECTOR_ABILITIES_JOB 0
#define ADVCOMBAT_INSPECTOR_ABILITIES_MEMORIZED 1
#define ADVCOMBAT_INSPECTOR_ABILITIES_TACTICS 2
#define ADVCOMBAT_INSPECTOR_ABILITIES_TOTAL 3

//--Icons
#define ADVCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL 13

//--Combat Inspector Statistic Slots
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_HPMAX 0
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_MPMAX 1
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_MPREGEN 2
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_ATTACK 3
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_INITIATIVE 4
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_ACCURACY 5
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_EVADE 6
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_THREAT_MULTIPLIER 7
#define ADVCOMBAT_INSPECTOR_STATUS_ICON_TOTAL 8

///--[Forward Declarations]
struct TargetCluster;
struct CombatEventPack;
struct ApplicationPack;

///--[Function Pointers]
typedef void(*ConfirmationFnPtr)();

///========================================= Classes ==============================================
class AdvCombat : public RootObject
{
    private:

    protected:
    ///--[System]
    bool mHasInitializedAdvCombat;
    bool mCombatReinitialized;
    bool mIsActive;
    bool mWasLastDefeatSurrender;
    bool mAIHandledAction;
    bool mPlayerGainedInitiative;
    int mShowDetailedAbilityDescriptions;
    char *mDoctorResolvePath;
    int mDoctorResolveValue;
    char *mStatisticsPath;

    ///--[Changeable Constants]
    //--Constants that can be updated by derived classes.
    int cIntroMoveTicks;
    int cStdMoveTicks;
    int cMoveTargetTicks;

    ///--[Colors]
    StarlightColor mHeadingStatic;

    ///--[Field Abilities]
    StarLinkedList *mExtraScripts; //ExtraScriptPack *, master
    FieldAbility *rActiveFieldAbilities[ADVCOMBAT_FIELD_ABILITY_SLOTS];

    ///--[Introduction]
    bool mIsIntroduction;
    int mIntroTimer;

    ///--[Title]
    bool mShowTitle;
    int mTitleTicks;
    int mTitleTicksMax;
    char *mTitleText;

    ///--[Turn State]
    bool mIsTurnActive;
    int mCurrentTurn;
    int mTurnDisplayTimer;
    int mTurnBarMoveTimer;
    int mTurnBarMoveTimerMax;
    int mTurnBarNewTurnTimer;
    int mTurnBarNewTurnTimerMax;
    int mTurnWidStart;
    int mTurnWidCurrent;
    int mTurnWidTarget;
    StarLinkedList *mrTurnOrder; //AdvCombatEntity *, ref
    StarBitmap *rTurnDiscardImg;

    ///--[Action Handling]
    bool mNoEventsEndsAction;
    bool mWasLastActionFree;
    bool mWasLastActionEffortless;
    bool mActionFirstTick;
    bool mIsActionOccurring;
    bool mIsActionConcluding;
    bool mIsPerformingPlayerUpdate;
    bool mEventFirstTick;
    bool mEventCanRun;
    bool mEntitiesHaveRepositioned;
    int mEventTimer;
    CombatEventPack *rEventZeroPackage;
    CombatEventPack *rFiringEventPackage;
    StarLinkedList *mEventQueue; //CombatEventPack *, master
    int mTextScatterCounter;
    StarLinkedList *mApplicationQueue; //ApplicationPack *, master
    StarlightString *mToggleDescriptionString;

    ///--[Player Input]
    bool mHideCombatUI;
    int mPlayerInterfaceTimer;
    int mPlayerCardTimer;
    int mPlayerJobTimer;
    int mPlayerMemorizedTimer;
    int mPlayerTacticsTimer;
    int mPlayerMode;
    int mPlayerCPFrameTimer;
    int mPlayerDescriptionTimer;
    int mPlayerCPTimer;
    int mAbilitySelectionX;
    int mAbilitySelectionY;
    EasingPack2D mAbilityHighlightPos;
    EasingPack2D mAbilityHighlightSize;
    StarlightString *mHideUIString;
    StarlightString *mShowUIString;
    StarlightString *mToggleDescriptionsString;
    StarlightString *mChangeCardString;

    ///--[Expert Mode]
    bool mExpertSecondPage;
    int mExpertPageSwapTime;
    int mExpertArrowTimer;

    ///--[Target Selection]
    bool mLockTargetClusters;
    int mTargetClusterCounter;
    bool mIsSelectingTargets;
    int mTargetClusterCursor;
    int mTargetFlashTimer;
    char *mLastUsedTargetType;
    StarLinkedList *mTargetClusters; //TargetCluster *, master

    ///--[Prediction Boxes]
    bool mIsClearingPredictionBoxes;
    TargetCluster *rPredictionCluster;
    StarLinkedList *mPredictionBoxes; //AdvCombatPrediction *, master

    ///--[Confirmation Window]
    bool mIsShowingConfirmationWindow;
    int mConfirmationTimer;
    StarlightString *mConfirmationString;
    StarlightString *mAcceptString;
    StarlightString *mCancelString;
    ConfirmationFnPtr rConfirmationFunction;

    ///--[Combat Inspector]
    //--System
    bool mIsShowingCombatInspector;
    bool mInspectorShowStatistics;
    int mCombatInspectorMode;
    int mCombatInspectorAbilitiesType;
    StarLinkedList *mrInspectorEffectListing;
    int mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_TOTAL];

    //--Timers
    int mCombatInspectorTimer;
    int mCombatInspectorJobTimer;
    int mCombatInspectorMemorizedTimer;
    int mCombatInspectorTacticsTimer;

    //--Cursors
    int mInspectorEntityCursor;
    int mInspectorEntitySkip;
    int mInspectorEffectCursor;
    int mInspectorEffectSkip;
    int mInspectorAbilityCursorX;
    int mInspectorAbilityCursorY;
    StarLinkedList *mrInspectorEntityList; //AdvCombatEntity *, reference

    //--Highlight
    EasingPack2D mInspectorHighlightPos;
    EasingPack2D mInspectorHighlightSize;

    //--Colors
    StarlightColor mInspectorBonus;
    StarlightColor mInspectorMalus;

    //--Strings
    StarlightString *mCombatInspectorShow;
    StarlightString *mCombatInspectorHelp;
    StarlightString *mCombatInspectorScrollFast;
    StarlightString *mCombatInspectorEffects;
    StarlightString *mCombatInspectorAbilities;
    StarlightString *mCombatInspectorToggleResistances;
    StarlightString *mCombatInspectorToggleSkills;

    ///--[Ability Execution]
    int mSkillCatalystExtension;
    ApplicationPack *rCurrentApplicationPack; //Populated for ACE_AI_SCRIPT_CODE_APPLICATION_START and ACE_AI_SCRIPT_CODE_APPLICATION_END
    AdvCombatAbility *rActiveAbility;
    TargetCluster *rActiveTargetCluster;

    ///--[System Abilities]
    char *mVolunteerScript;
    AdvCombatAbility *mSysAbilityPassTurn;
    AdvCombatAbility *mSysAbilityRetreat;
    AdvCombatAbility *mSysAbilitySurrender;
    AdvCombatAbility *mSysAbilityVolunteer;

    ///--[Animation Storage]
    StarLinkedList *mCombatAnimations; //AdvCombatAnimation *, master
    StarLinkedList *mTextPackages; //CombatTextPack *, master

    ///--[Option Flags]
    bool mAutoUseDoctorBag;
    bool mTouristMode;
    bool mMemoryCursor;
    bool mRestoreMainWeaponOnCombatEnd;

    ///--[Combat State Flags]
    bool mIsUnwinnable;
    bool mIsUnloseable;
    bool mIsUnretreatable;
    bool mIsUnsurrenderable;
    int mResolutionTimer;
    int mResolutionState;
    int mCombatResolution;
    int mResolutionDoctorStart;
    int mResolutionDoctorEnd;
    char *mPostScriptHandler;

    ///--[Enemy Storage]
    int mUniqueEnemyIDCounter;
    char *mParagonScript;
    char *mEnemyAutoScript;
    char *mDefeatedUnitScript;
    int mLastParagonValue;
    AliasStorage *mEnemyAliases;
    AdvCombatEntity *rKnockoutEntity;
    AdvCombatEntity *rLastRegisteredEnemy;
    StarLinkedList *mEnemyRoster; //AdvCombatEntity *, master
    StarLinkedList *mrEnemyCombatParty; //AdvCombatEntity *, ref
    StarLinkedList *mrEnemyReinforcements; //AdvCombatEntity *, ref
    StarLinkedList *mrEnemyGraveyard; //AdvCombatEntity *, ref
    StarLinkedList *mWorldReferences; //WorldRefPack *, master

    ///--[Party Storage]
    StarLinkedList *mPartyRoster; //AdvCombatEntity *, master
    StarLinkedList *mrActiveParty; //AdvCombatEntity *, ref
    StarLinkedList *mrCombatParty; //AdvCombatEntity *, ref

    ///--[Stat Storage for Level Ups]
    CombatStatistics mJobTempStatistics;

    ///--[Victory Storage]
    int mXPToAward;
    int mJPToAward;
    int mPlatinaToAward;
    int mDoctorToAward;
    int mXPBonus;
    int mJPBonus;
    int mPlatinaBonus;
    int mDoctorBonus;
    int mXPToAwardStorage;
    int mJPToAwardStorage;
    int mPlatinaToAwardStorage;
    int mDoctorToAwardStorage;
    StarLinkedList *mItemsToAwardList; //char *, master
    StarLinkedList *mItemsAwardedList; //AdvVictoryItemPack *, ref

    ///--[Effect Storage]
    bool mMustRebuildEffectReferences;
    StarlightString *mSpecialShieldString;
    StarlightString *mSpecialAdrenalineString;
    StarLinkedList *mGlobalEffectList; //AdvCombatEffect *, master
    StarLinkedList *mEffectGraveyard; //AdvCombatEffect *, master
    StarLinkedList *mrTempEffectList; //AdvCombatEffect *, reference

    ///--[Response Codes]
    int cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_TOTAL][ADVCOMBAT_RESPONSE_TOTAL];

    ///--[Cutscene/Script Handlers]
    StarLinkedList *mCutsceneExecList; //char *, master
    char *mStandardRetreatScript;
    char *mStandardDefeatScript;
    char *mStandardVictoryScript;
    char *mCurrentRetreatScript;
    char *mCurrentDefeatScript;
    char *mCurrentVictoryScript;
    char *mCombatResponseScript;

    ///--[Audio]
    StarLinkedList *mSoundList; //PlaySoundPackage*, master
    char *mDefaultCombatMusic;
    char *mDefaultVictoryMusic;
    float mCombatMusicStart;
    char *mNextCombatMusic;
    float mNextCombatMusicStart;
    int mEndCombatTracksTotal;
    float *mEndCombatMusicResume;
    bool mQuietBackgroundMusicForVictory;
    AudioPackage *rPreviousMusicWhenReplaying;
    float mPreviousTimeWhenReplaying;

    ///--[Auto-Win Handler]
    bool mAutoWinFlag;
    char *mAutoWinPath;

    ///--[Strings]
    #define COMBAT_TRANSLATABLE_STRINGS_TOTAL 13
    union AdvString
    {
        struct
        {
            char *mPtr[COMBAT_TRANSLATABLE_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mShowUI;
            char *mHideUI;
            char *mToggleDescriptions;
            char *mCycleSkills;
            char *mAccept;
            char *mCancel;
            char *mToggleAbilityDetails;
            char *mOpenCombatInspector;
            char *mShowHelp;
            char *mScrollx10;
            char *mInspect;
            char *mToggleResistances;
            char *mToggleSkillBlock;
        }str;
    }AdvString;

    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            ///--Fonts
            //--Victory
            StarFont *rVictoryFontHeader;
            StarFont *rVictoryFontExp;
            StarFont *rVictoryFontPlatina;
            StarFont *rVictoryFontDoctor;
            StarFont *rVictoryFontItem;

            //--Ally Bar
            StarFont *rAllyHPFont;

            //--Player Bar
            StarFont *rPlayerNameFont;
            StarFont *rPlayerCardFont;
            StarFont *rPlayerHPFont;

            //--Enemy Bar
            StarFont *rEnemyHPFont;

            //--Misc.
            StarFont *rAbilityTitleFont;  //--Shows ability names when used
            StarFont *rNewTurnFont;       //--Shows turn count
            StarFont *rTargetClusterFont; //--Name of target clusters (eg. "All Enemies")
            StarFont *rPredictionFont;    //--Prediction box
            StarFont *rCombatTextFont;    //--Effects that apply

            //--Descriptions
            StarFont *rAbilityHeaderFont;
            StarFont *rAbilityDescriptionFont;
            StarFont *rAbilitySimpleDescriptionFont;

            //--Confirmation UI
            StarFont *rConfirmationFontBig;
            StarFont *rConfirmationFontSmall;

            //--Combat Inspector
            StarFont *rInspectorHeading;
            StarFont *rInspectorMainline;
            StarFont *rInspectorStatistic;

            //--Help Strings
            StarFont *rHelpFont;

            ///--Base Interface
            StarBitmap *rBGGradient;
            StarBitmap *rCardAttack;
            StarBitmap *rCardJob;
            StarBitmap *rCardMemorized;
            StarBitmap *rCardTactics;
            StarBitmap *rCharacterBacking;
            StarBitmap *rCharacterBanner;
            StarBitmap *rCharacterCPBarPip;
            StarBitmap *rCharacterCPFrame;
            StarBitmap *rCharacterHPFill;
            StarBitmap *rCharacterHPFrame;
            StarBitmap *rCharacterMPFill;
            StarBitmap *rCharacterMPFrame;
            StarBitmap *rDescription;
            StarBitmap *rJobSkillsFrame;
            StarBitmap *rMemorizedSkillsFrame;
            StarBitmap *rTacticsSkillsFrame;

            ///--Ally Bar
            StarBitmap *rAllyCPPip;
            StarBitmap *rAllyFrame;
            StarBitmap *rAllyHPFill;
            StarBitmap *rAllyMPFill;
            StarBitmap *rAllyPortraitMask;

            ///--Defeat Overlay
            StarBitmap *rDefeatFrames[ADVCOMBAT_DEFEAT_FRAMES_TOTAL];

            ///--Enemy Health Bar
            StarBitmap *rEnemyHPFill;
            StarBitmap *rEnemyHPFrame;
            StarBitmap *rEnemyStunBarFill;
            StarBitmap *rEnemyStunFill;
            StarBitmap *rEnemyStunFrame;

            ///--Combat Inspector
            //--Base parts
            StarBitmap *rInspectorAllyCPPip;
            StarBitmap *rInspectorAllyHPFill;
            StarBitmap *rInspectorAllyMPFill;
            StarBitmap *rInspectorEffectsDetail;
            StarBitmap *rInspectorEffectsFrame;
            StarBitmap *rInspectorEffectsScrollbar;
            StarBitmap *rInspectorEntitiesFrame;
            StarBitmap *rInspectorEntitiesScrollbar;
            StarBitmap *rInspectorHeader;
            StarBitmap *rInspectorResistancesDetail;
            StarBitmap *rInspectorResistancesFrame;
            StarBitmap *rInspectorSummaryFrame;
            StarBitmap *rScrollbarFront;

            //--Icons
            StarBitmap *rInspectorStatusIcons[ADVCOMBAT_INSPECTOR_STATUS_ICON_TOTAL];
            StarBitmap *rInspectorResistIcons[ADVCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL];

            ///--Expert UI
            StarBitmap *rExpertCPFill;
            StarBitmap *rExpertHPFill;
            StarBitmap *rExpertMPFill;
            StarBitmap *rExpertPageArrowD;
            StarBitmap *rExpertPageArrowU;
            StarBitmap *rExpertPageFrame;
            StarBitmap *rExpertSkillsFrame;
            StarBitmap *rExpertStatisticFramesBack;
            StarBitmap *rExpertStatisticFrames;

            //--CP Animation
            StarBitmap *rExpertCPAnim[ADVCOMBAT_ADVANCED_CP_FRAMES];
            StarBitmap *rExpertCPFrameAnim[ADVCOMBAT_ADVANCED_CPFRAME_TOTAL];

            ///--Icons
            StarBitmap *rHealthIcon;
            StarBitmap *rShieldsIcon;
            StarBitmap *rAdrenalineIcon;
            StarBitmap *rManaIcon;
            StarBitmap *rComboIcon;
            StarBitmap *rClockIcon;
            StarBitmap *rClockLgIcon;

            ///--Player Interface
            StarBitmap *rArrowLft;
            StarBitmap *rArrowRgt;
            StarBitmap *rPredictionBox;

            ///--Turn Order
            StarBitmap *rTurnOrderEnemy;
            StarBitmap *rTurnOrderFriendly;
            StarBitmap *rTurnOrderNext;

            ///--Victory
            StarBitmap *rVictoryBannerBack;
            StarBitmap *rVictoryBannerBig;
            StarBitmap *rVictoryDoctorBack;
            StarBitmap *rVictoryDoctorFill;
            StarBitmap *rVictoryDoctorFrame;
            StarBitmap *rVictoryDropFrame;
            StarBitmap *rVictoryExpFrameBack;
            StarBitmap *rVictoryExpFrameFill;
            StarBitmap *rVictoryExpFrameFront;
            StarBitmap *rVictoryExpFrameMask;

            ///--Standard
            StarBitmap *rHighlight;
        }Data;
    }Images;

    public:
    ///--System
    AdvCombat();
    virtual ~AdvCombat();

    //--Public Variables
    bool mIsRunningEventQuery;
    static bool xIsAISpawningEvents;
    static char *xDebugCalloutPath;

    //--Property Queries
    virtual bool IsOfType(int pType);
    bool IsActive();
    bool IsStoppingWorldUpdate();
    bool DoesPlayerHaveInitiative();
    bool IsAnythingAnimating();
    bool IsAnyoneMoving();
    int GetSkillCatalystSlots();
    int GetPartyGroupingID(uint32_t pUniqueID);
    FieldAbility *GetFieldAbility(int pSlot);
    bool IsAbilityUnlocked(const char *pCharacter, const char *pJob, const char *pAbility);
    bool IsUnretreatable();
    bool IsUnsurrenderable();
    bool IsUnloseable();
    int IsShowingDetailedDescriptions();
    bool IsEntityInActiveTargetCluster(void *pPtr);
    bool IsIntroduction();

    //--Manipulators
    void Activate();
    void Deactivate();
    void SetDefaultCombatMusic(const char *pMusicName, float pStartPoint);
    void SetDefaultVictoryMusic(const char *pMusicName);
    void SetNextCombatMusic(const char *pMusicName, float pStartPoint);
    void SetUnloseable(bool pFlag);
    void SetUnwinnable(bool pFlag);
    void SetUnretreatable(bool pFlag);
    void SetUnsurrenderable(bool pFlag);
    void SpawnTitle(int pTicks, const char *pText);
    void MarkEventCanRun(bool pFlag);
    void ResumeMusic();
    void SetFieldAbility(int pSlot, FieldAbility *pAbility);
    void RegisterExtraScriptPack(ExtraScriptPack *pPackage);
    void SetSkillCatalystSlots(int pSlots);
    void SetPostCombatHandler(const char *pScript);
    void SetDetailedDescriptionFlag(int pFlag);
    void SetAutoWinPath(const char *pPath);
    void AddScriptToExec(const char *pPath);
    void SetStatisticsPath(const char *pPath);
    void SetUnitDefeatScript(const char *pPath);

    //--Core Methods
    void HealFromDoctorBag(int pPartyIndex);
    bool IsEntityInvolvedInEvent(AdvCombatEntity *pEntity, CombatEventPack *pEvent);
    bool CheckKnockouts();
    void CleanDataLibrary();
    void CallStatisticsScript(int pCode);
    virtual void EnqueueSound(PlaySoundPackage *pPackage);
    virtual void PlayMonsterKnockout();
    void SwitchCombatPartyWithActivePartyMember(int pCombatSlot, const char *pActiveMemberName);
    virtual AdvCombatEntity *PushAbilityOwnerWhenNull();

    ///--AIs
    void MarkHandledAction();
    virtual void SetAbilityAsActiveI(int pSlot);
    virtual void SetAbilityAsActiveS(const char *pName);
    void RunActiveAbilityTargetScript();
    void SetTargetClusterAsActive(int pSlot);

    ///--Animations
    void RegisterAnimation(const char *pName, AdvCombatAnimation *pAnimation);
    void CreateAnimationInstance(const char *pAnimationName, const char *pRefName, int pStartingTicks, float pX, float pY);
    AdvCombatAnimation *GetAnimation(const char *pName);

    ///--Applications
    void ScatterPosition(int &sIndex, float &sXPos, float &sYPos);
    virtual void ResolveBattlePosition(AdvCombatEntity *pEntity, int pTypeFlag, float &sXPos, float &sYPos);
    virtual void HandleApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString);

    ///--Confirmation
    void ActivateConfirmationWindow();
    void DeactivateConfirmationWindow();
    StarlightString *GetConfirmationString();
    void SetConfirmationText(const char *pString);
    void SetConfirmationFunc(ConfirmationFnPtr pFuncPtr);
    static void ConfirmExecuteAbility();
    void UpdateConfirmation();
    virtual void RenderConfirmation();

    ///--Construction
    virtual void Initialize();
    virtual void Reinitialize(bool pHandleMusic);
    virtual void HandleMusic();
    virtual void Construct();
    virtual void Disassemble();

    ///--Debug
    void Debug_RestoreParty();
    void Debug_AwardXP(int pAmount);
    void Debug_AwardJP(int pAmount);
    void Debug_AwardPlatina(int pAmount);
    void Debug_AwardCrafting();

    ///--Effects
    bool DoesEffectExist(uint32_t pID);
    void RegisterEffect(AdvCombatEffect *pEffect);
    void RebuildEffectReferences();
    StarLinkedList *GetEffectsReferencing(AdvCombatEntity *pEntity);
    void StoreEffectsReferencing(uint32_t pID);
    void PushEffectByID(uint32_t pID);
    void ClearTemporaryEffectList();
    void MarkEffectForRemoval(uint32_t pID);
    void PulseEffectsForRemoval();
    void SortEffects();
    void ClearEffects();
    static int EffectCompare(const void *pEntryA, const void *pEntryB);
    StarLinkedList *GetTemporaryEffectList();

    ///--Enemy Handlers
    int GetNextEnemyID();
    const char *GetParagonScript();
    const char *GetEnemyAutoScript();
    int GetLastParagonValue();
    AdvCombatEntity *GetLastRegisteredEnemy();
    void RegisterEnemy(const char *pUniqueName, AdvCombatEntity *pEntity, int pReinforcementTurns);
    void SetParagonScript(const char *pScript);
    void SetEnemyAutoScript(const char *pScript);
    void SetParagonFlag(int pFlag);

    ///--Inspector
    void ActivateInspector();
    void DeactivateInspector();
    void RecomputeInspectorHighlight();
    StarLinkedList *ConstructEffectListReferencing(AdvCombatEntity *pEntity);
    void UpdateInspector();
    virtual void UpdateInspectorEntitySelect();
    virtual void UpdateInspectorEffectSelect();
    void UpdateInspectorAbilityQuery();
    virtual void RenderInspector();
    void RenderInspectorEntities();
    virtual void RenderInspectorResistStats();
    void RenderInspectorEffects();
    virtual void RenderInspectorAbilities();
    void RenderInspectorDescription();

    ///--Introduction
    void UpdateIntroduction();
    void RenderIntroduction();

    ///--Options
    bool IsAutoUseDoctorBag();
    bool IsTouristMode();
    bool IsMemoryCursor();
    void SetAutoUseDoctorBag(bool pFlag);
    void SetTouristMode(bool pFlag);
    void SetMemoryCursor(bool pFlag);

    ///--Paths
    void SetStandardRetreatScript(const char *pPath);
    void SetStandardDefeatScript(const char *pPath);
    void SetStandardVictoryScript(const char *pPath);
    void SetCombatResponseScript(const char *pPath);
    void SetRetreatScript(const char *pPath);
    void SetDefeatScript(const char *pPath);
    void SetVictoryScript(const char *pPath);
    void SetDoctorResolvePath(const char *pPath);
    void SetVolunteerScript(const char *pPath);

    ///--Player Input
    void RecomputePlayerInputHighlight();
    virtual void HandlePlayerControls(bool pAnyEntitiesMoving);
    void HandleCardControls();
    void HandleJobControls();
    void HandleMemorizedControls();
    void HandleTacticsControls();
    void HandleExpertControls();
    void BeginTargetingAbility(AdvCombatAbility *pAbility);
    void BeginTargetingAbilityBySlot(int pX, int pY);
    void ExecuteAbility(AdvCombatAbility *pAbility, TargetCluster *pTargetCluster);
    void ExecuteActiveAbility();

    ///--Positioning
    virtual float ComputeIdealX(bool pIsLeftSide, bool pIsOffscreen, int pPosition, int pTotalEntries, StarBitmap *pCombatPortrait);
    float ComputeOnscreenX(bool pIsLeftSide, int pPosition, int pTotalEntries);
    float ComputeOffscreenX(bool pIsLeftSide, StarBitmap *pImage);
    virtual float ComputeIdealY(AdvCombatEntity *pEntity);
    void PositionCharactersOnScreenForEvent(CombatEventPack *pEvent);
    void RestageCharactersWithNewID(uint32_t pID);
    void PositionCharactersNormally();
    void PositionCharactersOffScreen();
    void PositionEnemies();
    virtual void PositionEnemiesByTarget(StarLinkedList *pTargetList);
    void PositionListOnscreen(bool pIsLeftSide, int pTicks, StarLinkedList *pList);
    void PositionListOffscreen(bool pIsLeftSide, int pTicks, StarLinkedList *pList);

    ///--Prediction Boxes
    virtual void CreatePredictionBox(const char *pBoxName);
    void AllocatePredictionBoxStrings(const char *pBoxName, int pStringsTotal);
    void SetPredictionBoxOffsets(const char *pBoxName, float pXOffset, float pYOffset);
    virtual void SetPredictionBoxHostI(const char *pBoxName, uint32_t pHostID);
    void SetPredictionBoxStringText(const char *pBoxName, int pStringIndex, const char *pText);
    void SetPredictionBoxStringImagesTotal(const char *pBoxName, int pStringIndex, int pImagesTotal);
    virtual void SetPredictionBoxStringImage(const char *pBoxName, int pStringIndex, int pSlot, float pYOffset, const char *pPath);
    void BuildPredictionBoxes();
    void RunPredictionBoxCalculations();
    void SetClusterAsActive(int pIndex);
    void ClearPredictionBoxes();
    void ClearPredictionBoxesNow();
    void UpdatePredictionBoxes();
    virtual void RenderPredictionBoxes();
    TargetCluster *GetPredictionTargetCluster();

    ///--Reinforcements
    void RegisterWorldReference(TilemapActor *pActor, int pReinforcementTurns);

    ///--Resolution
    int GetResolutionCode();
    void AwardXP(int pAmount);
    void AwardJP(int pAmount);
    void AwardPlatina(int pAmount);
    void AwardDoctorBag(int pAmount);
    bool AwardItems(int pStart, int pEnd);
    void SetDoctorResolvedValue(int pValue);
    void RunCombatEndScripts();
    int GetVictoryXP();
    int GetVictoryPlatina();
    int GetVictoryJP();
    int GetVictoryDoctor();
    int GetBonusXP();
    int GetBonusPlatina();
    int GetBonusJP();
    int GetBonusDoctor();
    virtual bool IsPlayerDefeated();
    bool IsEnemyDefeated();
    void SetBonusXP(int pAmount);
    void SetBonusPlatina(int pAmount);
    void SetBonusJP(int pAmount);
    void SetBonusDoctor(int pAmount);
    void BeginResolutionSequence(int pSequence);
    virtual void UpdateResolution();
    virtual void RenderResolution();
    virtual void RenderVictory();
    virtual void RenderVictoryBanner(float pPercent);
    virtual void RenderVictoryDrops(float pPercent);
    virtual void RenderVictoryDoctor(float pPercent);
    virtual void RenderExpBars(float pPercent);
    virtual void RenderDefeat();
    void RenderRetreat();

    ///--Responses
    void RunGeneralResponse(int pCode);
    void RunAllResponseScripts(int pCode);
    void RunPartyResponseScripts(int pCode);
    void RunPartyAIScripts(int pCode);
    void RunPartyJobScripts(int pCode);
    void RunPartyAbilityScripts(int pCode);
    void RunEnemyResponseScripts(int pCode);
    void RunEnemyAIScripts(int pCode);
    void RunEnemyJobScripts(int pCode);
    void RunEnemyAbilityScripts(int pCode);
    void RunEffects(int pCode);
    void RunGraveyardAIResponseScripts(int pCode);

    ///--Roster Handling
    int GetRosterCount();
    int GetActivePartyCount();
    int GetCombatPartyCount();
    bool IsEntityInPlayerParty(void *pPtr);
    bool DoesPartyMemberExist(const char *pInternalName);
    bool IsPartyMemberActive(const char *pInternalName);
    bool IsPartyMemberActiveI(uint32_t pID);
    void RegisterPartyMember(const char *pInternalName, AdvCombatEntity *pEntity);
    void PushPartyMemberS(const char *pInternalName);
    void PushPartyMemberI(int pSlot);
    void PushActivePartyMemberS(const char *pInternalName);
    void PushActivePartyMemberI(int pSlot);
    void PushCombatPartyMemberS(const char *pInternalName);
    void PushCombatPartyMemberI(int pSlot);
    void SetPartyBySlot(int pSlot, const char *pPartyMemberName);
    void AddCombatMember(const char *pPartyMemberName);
    void ClearParty();
    void FullRestoreParty();
    void FullRestoreRoster();
    void MoveEntityToPartyGroup(uint32_t pID, int pPartyCode);
    void RemoveActiveMember(uint32_t pID);

    ///--Save/Load
    virtual void WriteLoadInfo(StarAutoBuffer *pBuffer);

    ///--Targeting
    //--System
    void ClearTargetClusters();
    void MovePartyByActiveCluster();

    //--Property Queries
    const char *GetLastTargetCode();
    int GetTargetClusterTotal();
    const char *GetNameOfTargetCluster(int pSlot);
    TargetCluster *GetTargetClusterI(int pSlot);
    TargetCluster *GetTargetClusterS(const char *pClusterName);
    AdvCombatEntity *GetEntityInCluster(const char *pClusterName, int pSlot);
    TargetCluster *GetActiveCluster();

    //--Manipulators
    void ResetTargetCode();
    TargetCluster *CreateTargetCluster(const char *pClusterName, const char *pDisplayName);
    void RemoveTargetClusterI(int pIndex);
    void RemoveTargetClusterS(const char *pClusterName);
    void RegisterElementToCluster(const char *pClusterName, void *pElement);
    void RegisterEntityToClusterByID(const char *pClusterName, uint32_t pUniqueID);
    void RemoveEntityFromClusterI(const char *pClusterName, int pSlot);
    void RemoveEntityFromClusterP(const char *pClusterName, void *pPtr);

    //--Addition Macros
    void AddPartyTargetToClusterI(const char *pClusterName, int pSlot);
    void AddPartyTargetToClusterS(const char *pClusterName, const char *pPartyMemberName);
    void AddEnemyTargetToClusterI(const char *pClusterName, int pSlot);
    void AddEnemyTargetToClusterS(const char *pClusterName, const char *pEnemyName);

    //--Autobuild Macros
    virtual void PopulateTargetsByCode(const char *pCodeName, void *pCaller);

    ///--Turn Order
    int GetTurn();
    bool IsEntityOnTurnList(uint32_t pUniqueID);
    void InsertEntityInTurnOrder(uint32_t pUniqueID, int pSlot);
    void GivePlayerInitiative();
    void RemovePlayerInitiative();
    void SetEventTimer(int pTicks);
    void SetAsFreeAction();
    void SetAsEffortlessAction();
    void EnqueueEvent(CombatEventPack *pPack);
    virtual void EnqueueApplicationPack(ApplicationPack *pPack);
    void RollTurnOrder();
    static int SortByInitiative(const void *pEntryA, const void *pEntryB);
    void RecomputeTurnBarLength();
    void BeginTurnDisplay();
    virtual void BeginTurn();
    void BeginAction();
    void CompleteAction();
    void EndTurn();

    ///--World
    void PulseWorld(bool pDontAnimate, bool pDontCheckEnemies, bool pIsPolling);
    bool DoAnyEnemiesBlockAutoWin();
    bool CheckAutoWin(TilemapActor *pCaller);
    void TripAutoWin();

    private:
    //--Private Core Methods
    public:
    ///--Update
    virtual void Update();
    void PlayQueuedSounds();

    //--File I/O

    ///--Drawing
    virtual void Render(bool pBypassResolution, float pAlpha);
    void RenderEntity(AdvCombatEntity *pEntity, float pAlpha);
    virtual void RenderEntityUI(AdvCombatEntity *pEntity, float pAlpha);
    virtual void RenderAllyBars(float pAlpha);
    virtual void RenderTurnOrder(float pAlpha);
    void RenderPlayerBar(float pAlpha);
    void RenderExpertBar(float pAlpha);
    void RenderPlayerCards(float pAlpha);
    void RenderPlayerJobSkills(float pAlpha);
    void RenderPlayerMemorizedSkills(float pAlpha);
    void RenderPlayerTacticsSkills(float pAlpha);
    void RenderSkillDescription(AdvCombatAbility *pAbility);
    virtual void RenderNewTurn(float pAlpha);
    void RenderTargetCluster();
    virtual void RenderTitle();
    void RenderAbilityBlock(float pX, float pY, int pType, AdvCombatEntity *pEntity);

    //--Pointer Routing
    StarLinkedList *GetActivePartyList();
    StarLinkedList *GetCombatPartyList();
    StarLinkedList *GetEnemyPartyList();
    CombatEventPack *GetFiringEventPack();
    CombatEventPack *GetEventQueryPack();
    AdvCombatEntity *GetEntityByID(uint32_t pUniqueID);
    AdvCombatEntity *GetRosterMemberI(int pIndex);
    AdvCombatEntity *GetRosterMemberS(const char *pName);
    AdvCombatEntity *GetActiveMemberI(int pIndex);
    AdvCombatEntity *GetActiveMemberS(const char *pName);
    AdvCombatEntity *GetCombatMemberI(int pIndex);
    AdvCombatEntity *GetCombatMemberS(const char *pName);
    AdvCombatEntity *GetKnockoutEntity();
    StarLinkedList *GetGraveyard();
    AliasStorage *GetEnemyAliasStorage();
    AdvCombatEntity *GetActingEntity();
    AdvCombatEntity *GetEventOriginator();
    AdvCombatAbility *GetSystemPassTurn();
    AdvCombatAbility *GetSystemRetreat();
    AdvCombatAbility *GetSystemSurrender();
    AdvCombatAbility *GetSystemVolunteer();
    ApplicationPack *GetApplicationPack();
    CombatStatistics *GetJobLevelUpStorage();

    //--Static Functions
    static AdvCombat *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombat_GetProperty(lua_State *L);
int Hook_AdvCombat_SetProperty(lua_State *L);
