//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "FlexMenu.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"
#include "MapManager.h"

void PandemoniumLevel::FlagForClose()
{
    mHasPendingClose = true;
}
void PandemoniumLevel::PushSelfMenu()
{
    //--Pushes the self-action menu atop the menu stack. Also populates the menu.
    if(!mSelfMenuPopulationScript)
    {
        DebugManager::ForcePrint("Error: No script for self-menu population.\n");
        return;
    }

    //--Work.
    FlexMenu *nNewMenu = new FlexMenu();
    LuaManager::Fetch()->PushExecPop(nNewMenu, mSelfMenuPopulationScript);
    MapManager::Fetch()->PushMenuStack(nNewMenu);
}
