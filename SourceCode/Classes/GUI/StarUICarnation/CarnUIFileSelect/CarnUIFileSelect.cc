//--Base
#include "CarnUIFileSelect.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StringEntry.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "SaveManager.h"

///========================================== System ==============================================
CarnUIFileSelect::CarnUIFileSelect()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CFLS");

    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== CarnUIFileSelect ======= ]
    ///--[System]
    mShowTutorialIndicator = false;
    mAllowNewSaves = false;

    ///--[Cursor]
    mSelectedSaveFile = -1;

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    ///--[Hitboxes]
    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 633.0f,  77.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollBd", 633.0f, 135.0f, 36.0f, 515.0f);
    RegisterHitboxWH("ScrollDn", 633.0f, 650.0f, 36.0f,  58.0f);

    //--Savefiles.
    float cLft = 212.0f;
    float cTop =  84.0f;
    float cWid = 416.0f;
    float cHei =  62.0f;
    float cSpY =  65.0f;
    for(int i = 0; i < cxCarnEntriesPerPage; i ++)
    {
        char tBuf[32];
        sprintf(tBuf, "Save%02i", i);
        RegisterHitboxWH(tBuf, cLft, cTop, cWid, cHei);
        cTop = cTop + cSpY;
    }

    //--Fixed buttons.
    RegisterHitboxWH("NewSaveBtn",   325.0f, 653.0f, 228.0f, 61.0f);
    RegisterHitboxWH("OverwriteBtn", 688.0f, 653.0f, 228.0f, 61.0f);
    RegisterHitboxWH("EditNoteBtn",  935.0f, 653.0f, 228.0f, 61.0f);

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mSelectFileOffset;
    nScrollbarPack->mPerPage = cxCarnEntriesPerPage;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxHitbox_ScrollUp;
    nScrollbarPack->mIndexBd = cxHitbox_ScrollMd;
    nScrollbarPack->mIndexDn = cxHitbox_ScrollDn;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;
}
CarnUIFileSelect::~CarnUIFileSelect()
{
}
void CarnUIFileSelect::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation File Select UI",           &CarnImages, sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI",                &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure File Select UI Carnation", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",                  &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CarnUIFileSelect::TakeForeground()
{
    ///--[Documentation]
    //--Called when this object is selected. Resets variables and recomputes maximums.
    mShowTutorialIndicator = false;
    mAllowNewSaves = false;
    mSelectedSaveFile = -1;
    ScanExistingFiles();

    //--Reset max scrollbar value.
    ScrollbarPack *rScrollbarPack = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
    if(rScrollbarPack) rScrollbarPack->mMaxSkip = ADVUIFILE_SCAN_TOTAL - cxCarnEntriesPerPage;

    //--Sort files by date.
    SortFilesByDateRev();

    ///--[Allow New Saves]
    //--Check this variable. If it's 1, allow new saves. The game dynmically disallows saving
    //  when in dungeons.
    SysVar *rSaveCheckVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Carnation/System/iAllowSaves");
    if(rSaveCheckVar && rSaveCheckVar->mNumeric > 0)
    {
        mAllowNewSaves = true;
    }

    ///--[Tutorial Indicator]
    //--Variable used to reassure the player that they can indeed save, after the tutorial is complete, SUCKEEEEEEEEEEEERS!
    SysVar *rTutorialVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Carnation/System/iShowSaveTutorial");
    if(rTutorialVar && rTutorialVar->mNumeric > 0)
    {
        mShowTutorialIndicator = true;
    }
}

///======================================= Core Methods ===========================================
void CarnUIFileSelect::RefreshMenuHelp()
{

}
void CarnUIFileSelect::RecomputeCursorPositions()
{

}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIFileSelect::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Updates save menu mode. Allows creation of new save files or overwriting of old ones.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, true);

    ///--[Mode Handlers]
    //--If a mode function returns true, it blocks the rest of the update.
    if(UpdateNotes()) return true;

    ///--[ ===== Scrollbars and Mouseover ===== ]
    //--Get mouse information.
    GetMouseInfo();
    RunHitboxCheck();

    //--Scrollbars update before anything else.
    if(UpdateScrollbars()) return true;

    ///--[ ========== Click Handling ========== ]
    //--Player left-clicks.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        HandleSaveLeftClick();
        return true;
    }

    //--Player right-clicks.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        FlagExit();
        return true;
    }

    //--Finish.
    return true;
}
bool CarnUIFileSelect::UpdateNotes()
{
    ///--[Documentation]
    //--Identical to the base version except pressing F1 to edit notes is disallowed.

    ///--[Is Entering Notes]
    //--Accept the player's keypresses to edit the save note.
    if(mIsEnteringNotes)
    {
        //--Run the update.
        mStringEntryForm->Update();

        //--Check for completion.
        if(mStringEntryForm->IsComplete())
        {
            //--Flag.
            mIsEnteringNotes = false;

            //--Get the note from the string entry form. It can legally be empty, in which case
            //  we generate a standard note.
            const char *rEnteredString = mStringEntryForm->GetString();

            //--Generate a note if the player left it empty.
            bool tDeallocateNote = false;
            char *rNewNote = SaveManager::GenerateNote(rEnteredString, tDeallocateNote);

            ///--[New Save]
            //--If this is a new saving case:
            if(mEditingFileCursor != -1)
            {
                //--Set the current savegame name.
                SaveManager::Fetch()->SetSavegameName(rNewNote);

                //--Game Directory:
                const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

                //--New savegame.
                if(mIsEditingNoteForNewSave)
                {
                    //--Set the current savegame name.
                    SaveManager::Fetch()->SetSavegameName(rNewNote);

                    //--Write to the file.
                    char *tFileBuf = InitializeString("%s/../../Saves/File%03i.slf", rAdventureDir, mEditingFileCursor);
                    SaveManager::Fetch()->SaveAdventureFile(tFileBuf);
                    free(tFileBuf);

                    //--Decrement the count of available save slots.
                    mEmptySaveSlotsTotal --;
                }
                //--Saving over an old save with a new note.
                else
                {
                    //--Write to the file. Remember the files are listed backwards.
                    char *tFileBuf = InitializeString("%s/../../Saves/File%03i.slf", rAdventureDir, mEditingFileCursor);
                    SaveManager::Fetch()->SaveAdventureFile(tFileBuf);
                    free(tFileBuf);
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Save");

                //--Exit this mode.
                FlagExit();
            }
            ///--[Note Modification]
            //--If the file edit cursor is -1, then we're just saving over the note for that pack.
            //  This code cannot create new saves, just edit existing ones.
            else
            {
                //--Get the loading pack in question. If the loading pack exists, write over it.
                LoadingPack *rPack = mLoadingPacks[mNoteSlot];
                if(!rPack) return true;

                //--Modify the string.
                ResetString(rPack->mFileName, rNewNote);

                //--Run the SaveManager's subroutine to switch the note out.
                SaveManager::ModifyFileNote(rNewNote, rPack->mFilePath);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--If the note was generated, deallocate it here.
            if(tDeallocateNote) free(rNewNote);
        }
        else if(mStringEntryForm->IsCancelled())
        {
            mIsEnteringNotes = false;
            mEditingFileCursor = -1;
        }

        //--Block the rest of the update.
        return true;
    }

    ///--[No Input]
    return false;
}
void CarnUIFileSelect::UpdateBackground()
{
    ///--[Timers]
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
}
void CarnUIFileSelect::HandleSaveLeftClick()
{
    ///--[Documentation]
    //--Does the bulk of the work of a left click. Returns out when it hits anything.

    //--Common functions.
    if(CommonTabClickUpdate()) return;

    ///--[Save File Selection]
    //--Check if the mouse is within range.
    int tReply = CheckRepliesWithin(cxHitbox_SavefileLo, cxHitbox_SavefileHi);
    if(tReply != -1)
    {
        mSelectedSaveFile = tReply - cxHitbox_SavefileLo;
        return;
    }

    ///--[New Save Button]
    //--Starts saving a new game.
    tReply = CheckRepliesWithin(cxHitbox_NewGame, cxHitbox_NewGame);
    if(tReply != -1 && mAllowNewSaves)
    {
        //--Scan along the list of available spots. First open one gets saved to.
        for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
        {
            //--If this spot is occupied, ignore it.
            if(mExistingFileListing[i]) continue;

            //--Mark this for saving and activate note entry.
            mIsEditingNoteForNewSave = true;
            mIsEnteringNotes = true;
            mNoteSlot = i;
            mEditingFileCursor = i;

            //--Upload info.
            mStringEntryForm->SetCompleteFlag(false);
            mStringEntryForm->SetPromptString("Enter a note for this save file.");
            mStringEntryForm->SetString(NULL);
            mStringEntryForm->SetEmptyString("Enter Name");
            break;
        }
        return;
    }

    ///--[Overwrite Save]
    tReply = CheckRepliesWithin(cxHitbox_Ovewrite, cxHitbox_Ovewrite);
    if(tReply != -1 && mAllowNewSaves)
    {
        //--Figure out which slot we're on.
        int tFileSlot = GetArraySlotFromLookups(mSelectedSaveFile);
        if(tFileSlot == -1) return;

        //--Flags.
        mIsEditingNoteForNewSave = false;
        mIsEnteringNotes = true;
        mNoteSlot = tFileSlot;
        mEditingFileCursor = tFileSlot;

        //--Upload info.
        mStringEntryForm->SetCompleteFlag(false);
        mStringEntryForm->SetPromptString("Enter a note for this save file.");
        mStringEntryForm->SetEmptyString("Enter Name");

        //--If the loading pack in question does not exist, put a default string in.
        if(!mLoadingPacks[mNoteSlot])
        {
            mStringEntryForm->SetString(NULL);
        }
        //--Otherwise, populate with the file name.
        else
        {
            mStringEntryForm->SetString(mLoadingPacks[mNoteSlot]->mFileName);
        }
        return;
    }

    ///--[Edit Note]
    tReply = CheckRepliesWithin(cxHitbox_EditNote, cxHitbox_EditNote);
    if(tReply != -1)
    {
        //--The given file may be different from the grid position based on page and sorting. Verify.
        int tFileSlot = GetArraySlotFromLookups(mSelectedSaveFile);
        fprintf(stderr, "Savefile slot: %i\n", mSelectedSaveFile);
        fprintf(stderr, "File slot: %i\n", tFileSlot);
        if(tFileSlot == -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Make sure there's actually a file in the given slot.
        else if(!mLoadingPacks[tFileSlot])
        {
            fprintf(stderr, "No loading pack.\n");
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Checks passed.
        else
        {
            //--Verify.
            mEditingFileCursor = -1;
            mNoteSlot = tFileSlot;
            if(mNoteSlot >= 0 && mLoadingPacks[mNoteSlot])
            {
                //--Flags.
                mIsEnteringNotes = true;

                //--Upload info.
                mStringEntryForm->SetCompleteFlag(false);
                mStringEntryForm->SetPromptString("Enter a note for this save file.");
                mStringEntryForm->SetString(mLoadingPacks[mNoteSlot]->mFileName);
                mStringEntryForm->SetEmptyString("Enter Name");
            }
        }
        return;
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIFileSelect::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the entire UI, Carnation does not use piece rendering.

    ///--[Static Parts]
    RenderTopTabs(mCodeBackward);
    CarnImages.rFrame_Base->Draw();

    ///--[Save Information]
    for(int i = 0; i < cxCarnEntriesPerPage; i ++)
    {
        //--If this is not the 0th entry, render a divider above.
        if(i > 0) CarnImages.rOverlay_Divider->Draw(0.0f, (i-1) * 65.0f);

        //--Block.
        RenderFileBlock(i, 212.0f, 84.0f + (65.0f * i), (i == mSelectedSaveFile), 1.0f);
        StarlightColor::ClearMixer();
    }

    //--Scrollbar.
    if(true)
    {
        StandardRenderScrollbar(mSelectFileOffset, cxCarnEntriesPerPage, ADVUIFILE_SCAN_TOTAL-cxCarnEntriesPerPage, 135.0f, 515.0f, false, CarnImages.rScrollbar_Scroller, CarnImages.rScrollbar_Static);
    }

    //--New Save button.
    TwoDimensionReal *rNewSaveHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("NewSaveBtn");
    if(rNewSaveHitbox && mAllowNewSaves)
    {
        CarnImages.rFrame_Button->Draw(rNewSaveHitbox->mLft, rNewSaveHitbox->mTop);
        CarnImages.rFont_Button->DrawText(rNewSaveHitbox->mXCenter, rNewSaveHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "New Save");
    }

    //--Save tutorial text. Appears in place of the new save button.
    if(!mAllowNewSaves && mShowTutorialIndicator)
    {
        CarnImages.rFont_File->DrawText(212.0f, 663.0f, 0, 1.0f, "You will be able to save when the");
        CarnImages.rFont_File->DrawText(212.0f, 683.0f, 0, 1.0f, "tutorial is complete.");
    }

    //--Overwrite button. Only appears when a save is selected.
    TwoDimensionReal *rOverwriteHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("OverwriteBtn");
    if(rOverwriteHitbox && mSelectedSaveFile != -1 && mAllowNewSaves)
    {
        CarnImages.rFrame_Button->Draw(rOverwriteHitbox->mLft, rOverwriteHitbox->mTop);
        CarnImages.rFont_Button->DrawText(rOverwriteHitbox->mXCenter, rOverwriteHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Overwrite Save");
    }

    //--Edit Note button. Only appears when a save is selected.
    TwoDimensionReal *rEditNotHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("EditNoteBtn");
    if(rEditNotHitbox && mSelectedSaveFile != -1)
    {
        CarnImages.rFrame_Button->Draw(rEditNotHitbox->mLft, rEditNotHitbox->mTop);
        CarnImages.rFont_Button->DrawText(rEditNotHitbox->mXCenter, rEditNotHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Edit Note");
    }

    ///--[Entering Notes]
    //--Notes appear over everything else.
    if(mIsEnteringNotes)
    {
        mStringEntryForm->Render();
        return;
    }
}
void CarnUIFileSelect::RenderFileBlock(int pIndex, float pLft, float pTop, bool pIsHighlighted, float pOpacityPct)
{
    ///--[Documentation and Setup]
    //--Given an index, renders the file information for that load information pack. Note that this is the RAW file
    //  position in the original array, NOT the lookup index. Depending on the sorting, the lookup index may not
    //  be the same as the array position.
    //--Note: This may modify the color mixer. Clear the mixer when you are done.
    int tFileSlot = GetArraySlotFromLookups(pIndex);
    if(tFileSlot < 0 || tFileSlot >= ADVUIFILE_SCAN_TOTAL) return;

    //--Fast-access pointer.
    StarFont *rUseFont = CarnImages.rFont_File;

    //--If highlighted draw a darkened backing.
    if(pIsHighlighted)
    {
        StarBitmap::DrawRectFill(pLft, pTop, pLft + 451.0f, pTop + 62.0f, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.50f));
    }

    ///--[System]
    //--Empty spots now get represented as blank cards with "Empty Slot" on them.
    if(!mLoadingPacks[tFileSlot])
    {
        //--Empty slot indicator.
        rUseFont->DrawTextArgs(pLft + 150.0f, pTop + 7.0f, 0, 1.0f, "Empty Slot");

        //--File number if this slot were to be occupied.
        rUseFont->DrawTextArgs(pLft + 13.0f, pTop + 7.0f, 0, 1.0f, "File%03i.slf", tFileSlot);
        return;
    }

    ///--[Text]
    //--File number.
    rUseFont->DrawTextArgs(pLft + 13.0f, pTop + 7.0f, 0, 1.0f, mLoadingPacks[tFileSlot]->mFilePathShort);

    //--File note. First, figure out if it needs to be a two-line now.
    char tTimestampA[32];
    char tTimestampB[32];
    float tNoteLen = rUseFont->GetTextWidth(mLoadingPacks[tFileSlot]->mFileName) * 1.0f;
    if(tNoteLen > 220.0f)
    {
        //--Setup.
        int tCharsParsed = 0;
        int tCharsParsedTotal = 0;
        float tCurY = pTop + 34.0f;
        int tStringLen = (int)strlen(mLoadingPacks[tFileSlot]->mFileName);

        //--Loop until the whole note has been parsed out.
        while(tCharsParsedTotal < tStringLen)
        {
            //--Run the subdivide.
            char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &mLoadingPacks[tFileSlot]->mFileName[tCharsParsedTotal], -1, 220.0f, rUseFont, 1.0f);
            rUseFont->DrawText(pLft + 13.0f, tCurY, 0, 1.0f, tDescriptionLine);
            tCurY = tCurY + 13.0f;

            //--Move to the next line.
            tCharsParsedTotal += tCharsParsed;

            //--Clean up.
            free(tDescriptionLine);
        }
    }
    //--One-line.
    else
    {
        rUseFont->DrawText(pLft + 13.0f, pTop + 34.0f, 0, 1.0f, mLoadingPacks[tFileSlot]->mFileName);
    }

    ///--[Right-Align Timestamp]
    //--Split the timestamp into two parts.
    int tLen = (int)strlen(mLoadingPacks[tFileSlot]->mTimestamp);
    for(int p = 0; p < tLen; p ++)
    {
        if(mLoadingPacks[tFileSlot]->mTimestamp[p] == ' ')
        {
            strcpy(tTimestampB, &mLoadingPacks[tFileSlot]->mTimestamp[p+1]);
            break;
        }
        else
        {
            tTimestampA[p+0] = mLoadingPacks[tFileSlot]->mTimestamp[p];
            tTimestampA[p+1] = '\0';
        }
    }

    //--Timestamp.
    rUseFont->DrawText(pLft + 412.0f, pTop +  7.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, tTimestampA);
    rUseFont->DrawText(pLft + 412.0f, pTop + 34.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, tTimestampB);

    ///--[Party]
    //--The maximum party size for a Carnation save is 3. This only occurs if this is the selected block.
    if(!pIsHighlighted) return;

    //--Constants.
    float cFrameX      = 700.0f;
    float cFrameY      =  80.0f;
    float cFrameSpcY   = 185.0f;
    float cPorScale    =   0.65;
    float cPorScaleInv =   1.0f / cPorScale;

    //--Render members.
    for(int i = 0; i < 3; i ++)
    {
        //--Skip empty slots.
        if(!mLoadingPacks[tFileSlot]->rRenderImg[i]) continue;

        //--Position.
        float cXPos = cFrameX;
        float cYPos = cFrameY + (cFrameSpcY * i);

        //--Render portrait.
        glTranslatef(cXPos, cYPos, 0.0f);
        glScalef(cPorScale, cPorScale, 1.0f);
        mLoadingPacks[tFileSlot]->rRenderImg[i]->Draw();
        glScalef(cPorScaleInv, cPorScaleInv, 1.0f);
        glTranslatef(-cXPos, -cYPos, 0.0f);

        //--Render overlay.
        CarnImages.rOverlay_PortraitFrame->Draw(0.0f, cFrameSpcY * i);

        //--Resolve the name.
        StarLinkedList *tStringList = Subdivide::SubdivideStringToList(mLoadingPacks[tFileSlot]->mPartyNames[i], "|");
        char *rCharacterName = (char *)tStringList->GetElementBySlot(1);

        //--Information. Name, level, etc.
        float cTxtLft = cXPos + 209.0f;
        rUseFont->DrawText    (cTxtLft, cYPos +  0.0f, 0, 1.0f, rCharacterName);
        rUseFont->DrawTextArgs(cTxtLft, cYPos + 20.0f, 0, 1.0f, "Level: %i", mLoadingPacks[tFileSlot]->mPartyLevels[i] + 1);

        //--Clean.
        delete tStringList;
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
