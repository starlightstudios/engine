//--[StarlightSocket]
//--Wrapper around a winsock/unix socket object. Is platform-independent using the _TARGET_OS_[X]_ flags.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--If no network flag has been set, define _NETWORK_NONE_
#ifndef _NETWORK_NONE_
    #ifdef _NETWORK_WINSOCK_


    #else
        #define _NETWORK_NONE_
    #endif
#endif

//--[Local Definitions]
//--Blocking states
#define STARLIGHTSOCKET_BLOCKING 0
#define STARLIGHTSOCKET_NONBLOCKING 1

//--Error codes
#define STARLIGHTSOCKET_ERROR_NOTDEFINED -1
#define STARLIGHTSOCKET_ERROR_WOULDBLOCK 0

//--Packet Types
#define STARLIGHTPACKET_STRING 0
#define STARLIGHTPACKET_FORCEDISCONNECT 1
#define STARLIGHTPACKET_REQUESTNAME 2
#define STARLIGHTPACKET_ANSWERNAME 3
#define STARLIGHTPACKET_STANDARDPACKETS 4

//--Local Structures
typedef struct StarlightPacket
{
    uint32_t mLength;
    uint16_t mType;
    char mMessage[STD_MAX_LETTERS];
}StarlightPacket;

#if defined _TARGET_OS_MAC_
struct in_addr
{
    unsigned long s_addr;  // load with inet_aton()
};

struct sockaddr_in
{
    short            sin_family;   // e.g. AF_INET
    unsigned short   sin_port;     // e.g. htons(3490)
    struct in_addr   sin_addr;     // see struct in_addr, below
    char             sin_zero[8];  // zero this if you want to
};
#endif

//--Local Typdefs
typedef void(*StarlightPacketHandler)(StarlightSocket *, StarlightPacket &);

class StarlightSocket
{
    private:
    #if defined _NETWORK_WINSOCK_
    //--System
    bool mSocketIsStarted;
    static bool xHasBootedWinsock;
    static long unsigned int xBlockingFlags;

    //--Self-Data
    int mSocketHandle;

    //--Server
    bool mIsServerMode;
    sockaddr_in mLocalSocketInfo;

    //--Client
    bool mIsClientMode;
    int mClientHandle;
    int mClientLen;
    sockaddr_in mClientData;
    bool mClientCheckConnect;

    //--Meta-Information
    bool mHasOtherIdentified;
    int mCounterForIdentify;
    char *mOtherIdentifier;

    //--Packet controllers
    StarlightPacketHandler rPacketHandler;
    #endif

    protected:

    public:
    //--System
    StarlightSocket();
    ~StarlightSocket();
    void StartSocket();
    void DisconnectSocket();
    static void DeleteThis(void *pPtr);
    static void TakeDownNetwork();

    //--Public Variables
    //--Property Queries
    bool IsSocketOpen();
    int GetConnectionsTotal();

    //--Manipulators
    void StartServerOn(int pPort);
    void ConnectTo(const char *pAddress, int pPort);
    void ForgetClient();
    void SetConnecteeName(const char *pName);
    void SetIdentifyTimer(int pTimer);
    void SetPacketHandler(StarlightPacketHandler pNewHandler);

    //--Core Methods
    static StarlightPacket PrefabPacket(int pType, void *pDataBuffer, int pLength);
    void SendData(StarlightPacket pPacket);
    void SendData(const char *pString);
    static void HandlePacket(StarlightSocket *rCaller, StarlightPacket &sPacket);

    //--Update
    void Update();
    void ClientUpdate();
    void ServerUpdate();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static int GetSocketErrorCode();
    static void PrintError();

    //--Lua Hooking
};

//--Hooking Functions

