///======================================== GridHandler ===========================================
//--Routines used by the UI to build a grid and populate it with priorities. Exactly how the grid is displayed
//  and what the priorities represent is up to the caller.

#pragma once
#include <string.h>
#include "AdvMenuDefStruct.h"

///========================================= Includes =============================================
class StarLinkedList;

///===================================== Local Definitions ========================================
//--Grid Direction Constants
#define GRID_LFT 0
#define GRID_TOP 1
#define GRID_RGT 2
#define GRID_BOT 3
#define GRID_TOTAL 4

///===================================== Local Structures =========================================
//--When using the grid menus, a grid entry stores its ideal position and the entry code it should
//  switch to when the matching key is pressed.
typedef struct GridPackage
{
    //--Members
    int mCode;
    int mPulseValue;
    int mTimer;
    int mControlLookups[GRID_TOTAL];
    float mXPos;
    float mYPos;
    char mLocalName[64];
    AdvMenuStandardAlignments mAlignments;

    //--Functions
    void Initialize()
    {
        mCode = -1;
        mPulseValue = 0;
        mTimer = 0;
        memset(mControlLookups, -1, sizeof(int) * GRID_TOTAL);
        mXPos = 0.0f;
        mYPos = 0.0f;
        memset(mLocalName,  0, sizeof(char) * 64);
        memset(&mAlignments, 0, sizeof(AdvMenuStandardAlignments));
    }
}GridPackage;

///========================================== Classes =============================================
class GridHandler
{
    private:
    //--System
    GridHandler();

    protected:

    public:
    //--Splitting
    static int **BuildGrid(int pXSize, int pYSize, int pXStart, int pYStart, int pMaxEntries);
    static void BuildGridList(StarLinkedList *pUseList, int **pGrid, int pXSize, int pYSize, int pEntries, float pCenterX, float pCenterY, float pSpacingX, float pSpacingY);
    static void DeallocateGrid(int **pGrid, int pXSize);
};

//--Hooking Functions

