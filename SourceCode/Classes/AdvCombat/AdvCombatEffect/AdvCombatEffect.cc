//--Base
#include "AdvCombatEffect.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
AdvCombatEffect::AdvCombatEffect()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATEFFECT;

    ///--[AdvCombatEffect]
    //--System
    mLocalName = InitializeString("Effect");
    mDisplayName = InitializeString("Effect");
    mRemoveNow = false;
    mDontShowOnUI = false;
    mDontShowOnQuickUI = false;
    mDontShowShortText = false;

    //--Sort Priority
    mEffectPriority = 0;
    mEffectTimeIndex = Global::Shared()->gTicksElapsed;

    //--Display
    rBackImg = NULL;
    rFrameImg = NULL;
    rFrontImg = NULL;

    //--Script Handler
    memset(&mArguments, 0, sizeof(float) * ADVCE_MAX_ARGS);
    mApplicationPriority = 0;
    mScriptPath = NULL;
    mEffectPrototypeName = NULL;

    //--Targets
    rOriginator = NULL;
    mTargetList = new StarLinkedList(true);

    //--Tags
    mTagList = new StarLinkedList(true);
}
AdvCombatEffect::~AdvCombatEffect()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    free(mEffectPrototypeName);
    delete mTargetList;
    delete mTagList;
}

///--[Public Variables]
//--Global effect name. In order to make it possible for an ability to handle all their effect code
//  inside a single script, an ability can create one or more global effect prototypes and create
//  effects using that prototype, rather than defining a unique script. The effect prototypes are
//  stored in Lua, and the xGlobalEffectName is searched on that list to find the prototype.
char *AdvCombatEffect::xGlobalEffectName = NULL;

///================================= AdvCombatEffectTargetPack ====================================
void AdvCombatEffectTargetPack::Initialize()
{
    //--Variables.
    mTargetID = 0;
    rTargetPtr = NULL;
    mDisplayString = new StarlightString();
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
}
void AdvCombatEffectTargetPack::AllocateDescriptionLines(int pTotal)
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        delete mDescriptionLines[i];
    }
    free(mDescriptionLines);
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    if(pTotal < 1) return;
    mDescriptionLinesTotal = pTotal;
    mDescriptionLines = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mDescriptionLinesTotal);
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        mDescriptionLines[i] = new StarlightString();
    }
}
void AdvCombatEffectTargetPack::SetDescriptionText(int pSlot, const char *pText)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->SetString(pText);
}
void AdvCombatEffectTargetPack::AllocateDescriptionImages(int pSlot, int pImageTotal)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->AllocateImages(pImageTotal);
}
void AdvCombatEffectTargetPack::SetDescriptionImage(int pSlot, int pImageIndex, float pYOffset, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->SetImageS(pImageIndex, pYOffset, pDLPath);
}
void AdvCombatEffectTargetPack::CrossloadImages(int pSlot)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->CrossreferenceImages();
}
void AdvCombatEffectTargetPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    AdvCombatEffectTargetPack *rPtr = (AdvCombatEffectTargetPack *)pPtr;
    delete rPtr->mDisplayString;
    for(int i = 0; i < rPtr->mDescriptionLinesTotal; i ++)
    {
        delete rPtr->mDescriptionLines[i];
    }
    free(rPtr->mDescriptionLines);
    free(rPtr);
}

///--[Public Variables]
///===================================== Property Queries =========================================
const char *AdvCombatEffect::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatEffect::GetScriptPath()
{
    return mScriptPath;
}
bool AdvCombatEffect::IsRemovedNow()
{
    return mRemoveNow;
}
bool AdvCombatEffect::IsIDOnTargetList(uint32_t pID)
{
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pID)
        {
            mTargetList->PopIterator();
            return true;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
    return false;
}
int AdvCombatEffect::GetTagCount(const char *pTag)
{
    ///--[Documentation]
    //--Returns how many times the given tag appears in the list. Returns 0 if none are found.
    if(!pTag) return 0;

    //--Check if there are any on the list. If NULL comes back, the tag wasn't found.
    int *rCheckPtr = (int *)mTagList->GetElementByName(pTag);
    if(!rCheckPtr)
    {
        return 0;
    }
    return *rCheckPtr;
}
int AdvCombatEffect::GetTagCountPartial(const char *pTag)
{
    ///--[Documentation]
    //--Returns how many times the given tag appears in the list. Returns 0 if none are found. Partial matches
    //  are honored, so the results can be added together.
    if(!pTag) return 0;

    //--Setup.
    int tRunningTotal = 0;
    int tLen = (int)strlen(pTag);

    //--Iterate.
    int *rCheckPtr = (int *)mTagList->PushIterator();
    while(rCheckPtr)
    {
        //--If there is a match, add the count.
        const char *rName = mTagList->GetIteratorName();
        if(!strncasecmp(rName, pTag, tLen))
        {
            tRunningTotal = tRunningTotal + (*rCheckPtr);
        }

        //--Auto-iterate.
        rCheckPtr = (int *)mTagList->AutoIterate();
    }

    //--Finish up.
    return tRunningTotal;
}
const char *AdvCombatEffect::GetFirstTagNameMatching(const char *pTag)
{
    ///--[Documentation]
    //--Returns the full name of the first tag found matching the given partial tag. Returns NULL if no matches were found.
    if(!pTag) return NULL;
    int tLen = (int)strlen(pTag);

    //--Iterate.
    int *rCheckPtr = (int *)mTagList->PushIterator();
    while(rCheckPtr)
    {
        //--If there is a match, add the count.
        const char *rName = mTagList->GetIteratorName();
        if(!strncasecmp(rName, pTag, tLen))
        {
            mTagList->PopIterator();
            return rName;
        }

        //--Auto-iterate.
        rCheckPtr = (int *)mTagList->AutoIterate();
    }

    //--No matches.
    return NULL;
}
bool AdvCombatEffect::IsVisibleOnUI()
{
    return !mDontShowOnUI;
}
bool AdvCombatEffect::IsVisibleOnQuickUI()
{
    return !mDontShowOnQuickUI;
}
bool AdvCombatEffect::HasShortText()
{
    return !mDontShowShortText;
}
int AdvCombatEffect::GetEffectPriority()
{
    return mEffectPriority;
}
uint32_t AdvCombatEffect::GetTimeIndex()
{
    return mEffectTimeIndex;
}
float AdvCombatEffect::GetArgument(int pIndex)
{
    if(pIndex < 0 || pIndex >= ADVCE_MAX_ARGS) return 0.0f;
    return mArguments[pIndex];
}
const char *AdvCombatEffect::GetGlobalPrototypeName()
{
    return mEffectPrototypeName;
}

///======================================== Manipulators ==========================================
void AdvCombatEffect::SetDisplayName(const char *pName)
{
    ResetString(mDisplayName, pName);
}
void AdvCombatEffect::SetOriginatorByID(uint32_t pID)
{
    //--Note: Entity must exist within the AdvCombat class or it will come back NULL. Pass 0 to NULL
    //  off the originator.
    rOriginator = AdvCombat::Fetch()->GetEntityByID(pID);
}
void AdvCombatEffect::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void AdvCombatEffect::SetRemoveNow(bool pFlag)
{
    mRemoveNow = pFlag;
}
void AdvCombatEffect::SetHideOnUIFlag(bool pFlag)
{
    mDontShowOnUI = pFlag;
}
void AdvCombatEffect::SetHideOnQuickUIFlag(bool pFlag)
{
    mDontShowOnQuickUI = pFlag;
}
void AdvCombatEffect::SetHideShortText(bool pFlag)
{
    mDontShowShortText = pFlag;
}
void AdvCombatEffect::AddTargetByID(uint32_t pID)
{
    AdvCombatEntity *rCheckEntity = AdvCombat::Fetch()->GetEntityByID(pID);
    if(!rCheckEntity) return;

    //--Scan the list to see if an entry already exists with it.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pID)
        {
            mTargetList->PopIterator();
            return;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }

    //--New package needs to be created.
    SetMemoryData(__FILE__, __LINE__);
    AdvCombatEffectTargetPack *nPackage = (AdvCombatEffectTargetPack *)starmemoryalloc(sizeof(AdvCombatEffectTargetPack));
    nPackage->Initialize();
    nPackage->mTargetID = pID;
    nPackage->rTargetPtr = rCheckEntity;
    mTargetList->AddElement("X", nPackage, &AdvCombatEffectTargetPack::DeleteThis);
}
void AdvCombatEffect::SetFrontImage(const char *pPath)
{
    rFrontImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdvCombatEffect::SetFrameImage(const char *pPath)
{
    rFrameImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdvCombatEffect::SetBackImage(const char *pPath)
{
    rBackImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdvCombatEffect::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdvCombatEffect::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}
void AdvCombatEffect::SetEffectPriority(int pPriority)
{
    mEffectPriority = pPriority;
}
void AdvCombatEffect::SetGlobalPrototypeName(const char *pName)
{
    ResetString(mEffectPrototypeName, pName);
}
void AdvCombatEffect::SetArgument(int pIndex, float pValue)
{
    if(pIndex < 0 || pIndex >= ADVCE_MAX_ARGS) return;
    mArguments[pIndex] = pValue;
}

///======================================== Core Methods ==========================================
void AdvCombatEffect::ApplyStatsToTargets()
{
    //--Error check.
    if(!mScriptPath) return;

    //--Push this effect on the activity stack. It stays there for all function calls.
    LuaManager *rLuaManager = LuaManager::Fetch();
    DataLibrary::Fetch()->PushActiveEntity(this);

    //--Iterate across the entities.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        //--Set the static path. This is done each time as the path is cleared after each use.
        SetLocalPrototypeAsStatic();

        //--Apply to the target.
        rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_APPLYSTATS, "N", (float)rPackage->mTargetID);

        //--Order them to add their stats back up.
        if(rPackage->rTargetPtr) rPackage->rTargetPtr->ComputeStatistics();

        //--Next.
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }

    //--Pop activity stack.
    DataLibrary::Fetch()->PopActiveEntity();
}
void AdvCombatEffect::UnApplyStatsToTargets()
{
    ///--[Documentation]
    //--When an Effect expires, if it modifies the stats of a target it should un-apply the modification as to
    //  return the stats to normal.

    //--Error check.
    if(!mScriptPath) return;

    ///--[Push]
    //--Push this effect on the activity stack. It stays there for all function calls.
    LuaManager *rLuaManager = LuaManager::Fetch();
    DataLibrary::Fetch()->PushActiveEntity(this);

    ///--[Iterate]
    //--Iterate across the entities.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        //--Set the static path. This is done each time as the path is cleared after each use.
        SetLocalPrototypeAsStatic();

        //--Un-apply.
        rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_UNAPPLYSTATS, "N", (float)rPackage->mTargetID);

        //--Order them to add their stats back up.
        if(rPackage->rTargetPtr) rPackage->rTargetPtr->ComputeStatistics();

        //--Next.
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }

    ///--[Clean]
    //--Pop activity stack.
    DataLibrary::Fetch()->PopActiveEntity();
}
void AdvCombatEffect::ApplyStatsToTarget(uint32_t pTargetID)
{
    //--Runs the stat-modify algorithm only on the target provided. If the target is not on the target list,
    //  does nothing.
    LuaManager *rLuaManager = LuaManager::Fetch();
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pTargetID)
        {
            //--Set the static path. This is done each time as the path is cleared after each use.
            SetLocalPrototypeAsStatic();

            //--Call.
            DataLibrary::Fetch()->PushActiveEntity(this);
            rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_APPLYSTATS, "N", (float)rPackage->mTargetID);
            DataLibrary::Fetch()->PopActiveEntity();
            mTargetList->PopIterator();
            break;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
}
void AdvCombatEffect::UnApplyStatsToTarget(uint32_t pTargetID)
{
    //--Runs the stat-modify algorithm only on the target provided. If the target is not on the target list,
    //  does nothing.
    LuaManager *rLuaManager = LuaManager::Fetch();
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pTargetID)
        {
            //--Set the static path. This is done each time as the path is cleared after each use.
            SetLocalPrototypeAsStatic();

            //--Call.
            DataLibrary::Fetch()->PushActiveEntity(this);
            rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_UNAPPLYSTATS, "N", (float)rPackage->mTargetID);
            DataLibrary::Fetch()->PopActiveEntity();
            mTargetList->PopIterator();
            break;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
}
void AdvCombatEffect::SetLocalPrototypeAsStatic()
{
    //--Orders the object to set its local prototype name as the global one.
    if(!mEffectPrototypeName) return;
    ResetString(AdvCombatEffect::xGlobalEffectName, mEffectPrototypeName);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void AdvCombatEffect::RenderAt(float pX, float pY)
{
    if(rBackImg)  rBackImg->Draw(pX, pY);
    if(rFrameImg) rFrameImg->Draw(pX, pY);
    if(rFrontImg) rFrontImg->Draw(pX, pY);
}

///====================================== Pointer Routing =========================================
StarBitmap *AdvCombatEffect::GetBackImage()
{
    return rBackImg;
}
StarBitmap *AdvCombatEffect::GetFrameImage()
{
    return rFrameImg;
}
StarBitmap *AdvCombatEffect::GetFrontImage()
{
    return rFrontImg;
}
AdvCombatEntity *AdvCombatEffect::GetOriginator()
{
    return rOriginator;
}
StarLinkedList *AdvCombatEffect::GetTargetList()
{
    return mTargetList;
}
StarlightString *AdvCombatEffect::GetStringByTargetID(uint32_t pTargetID)
{
    //--Returns the StarlightString associated with the given target ID. If the ID is not on the
    //  target list, returns NULL.
    AdvCombatEffectTargetPack *rPackage = GetTargetPackByID(pTargetID);
    if(rPackage) return rPackage->mDisplayString;

    //--Not found.
    return NULL;
}
AdvCombatEffectTargetPack *AdvCombatEffect::GetTargetPackByID(uint32_t pTargetID)
{
    //--Returns the target package associated with the ID, or NULL if not found.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pTargetID)
        {
            mTargetList->PopIterator();
            return rPackage;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
    return NULL;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
