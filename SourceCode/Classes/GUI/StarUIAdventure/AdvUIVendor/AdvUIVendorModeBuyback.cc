//--Base
#include "AdvUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "ControlManager.h"
#include "LuaManager.h"

///======================================== Activation ============================================
void AdvUIVendor::ActivateModeBuyback()
{
    ///--[Documentation]
    //--Switches to Buyback mode.

    ///--[Variables]
    //--Common subroutines.
    ActivateMode(mIsBuybackMode);
    ResetCursor();

    ///--[Other]
    //--Reset comparison on zeroth item.
    RecheckComparisonCharacter(GetConsideredItem());
}

///===================================== Property Queries =========================================
float AdvUIVendor::GetBuybackCursorNameWidth()
{
    ///--[Documentation]
    //--In buyback mode, resolves the highlighted item and returns its width. Used for cursor size computations.
    float tNameWid = 100.0f;

    //--Get name.
    StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();
    ShopInventoryPack *rPackage = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mCursor);
    if(rPackage && rPackage->mItem)
    {
        tNameWid = AdvImages.rFont_Mainline->GetTextWidth(rPackage->mItem->GetName());
    }

    //--Name length clamp.
    if(tNameWid > cxAdvMaxNameWid) tNameWid = cxAdvMaxNameWid;

    //--Finish.
    return tNameWid;
}

///========================================== Update ==============================================
void AdvUIVendor::UpdateModeBuyback()
{
    ///--[Documentation]
    //--Update handler for Buyback mode. Always called, runs timers and stops if not in Buyback mode or marked
    //  to not handle the update.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Fast-access pointers.
    StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();

    ///--[Timers]
    StandardTimer(mBuyTimer,        0, cxAdvBuyTicks,      mIsBuyingItem);
    StandardTimer(mAskToEquipTimer, 0, cxAdvAskEquipTicks, mAskToEquip);

    //--If these times are not zeroed or capped, block the update.
    if(mBuyTimer     > 0 && mBuyTimer     < cxAdvBuyTicks)     return;
    if(mDetailsTimer > 0 && mDetailsTimer < cxAdvDetailsTicks) return;

    ///--[Mode Handlers]
    //--Buying an item.
    if(mIsBuyingItem) { UpdateBuybackItem(); return; }

    //--Asked to equip an item the player just bought.
    if(mAskToEquip) { UpdateBuybackEquipItem(); return; }

    //--If in Details mode, pressing cancel exits details mode. All other controls are ignored.
    if(CommonDetailsUpdate()) return;

    ///--[Shoulder Buttons]
    //--Shoulder-left. Decrements the comparison character. Does nothing if there is only one comparison character.
    if(rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel") && mShowSwapArrows)
    {
        ScrollComparisonCharacter(-1);
        return;
    }
    //--Shoulder-right. Increments the comparison character. Does nothing if there is only one comparison character.
    if(rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel") && mShowSwapArrows)
    {
        ScrollComparisonCharacter(1);
        return;
    }

    ///--[Run Button]
    //--Toggles which comparison slot is in use.
    if(rControlManager->IsFirstPress("Run") && mCanSwapCompareSlot)
    {
        ToggleComparisonSlot();
        return;
    }

    ///--[Left and Right, Change Mode]
    //--Changes modes to the left, which is Buy.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && !mIsShowingDetails)
    {
        ScrollMode(-1);
        return;
    }
    //--Changes modes to the right, which is Buyback.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && !mIsShowingDetails)
    {
        ScrollMode(1);
        return;
    }

    ///--[Up and Down]
    //--Up decrements, down increments. Wrapping is enabled.
    if(AutoListUpDn(mCursor, mSkip, rBuybackList->GetListSize(), cScrollBuf, cScrollPage, true))
    {
        //--Modify cursor.
        RecomputeCursorPositions();

        //--The comparison character may have changed. Recheck that here.
        RecheckComparisonCharacter(GetConsideredItem());
        return;
    }

    ///--[Activate]
    //--Begins purchasing the highlighted item. If you don't have enough money to buy even one, fails.
    if(rControlManager->IsFirstPress("Activate") && !mIsShowingDetails)
    {
        //--Get the item in question.
        StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();
        ShopInventoryPack *rPackage = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mCursor);
        if(!rPackage)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Make sure the item exists.
        AdventureItem *rItem = rPackage->mItem;
        if(!rItem) return;

        //--Get how much the item will cost. If we don't have enough money to buy one, fail.
        int tCost = rItem->GetValue();
        if(rPackage->mPriceOverride > -1) tCost = rPackage->mPriceOverride;
        if(tCost > AdventureInventory::Fetch()->GetPlatina())
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Otherwise, activate buying mode.
        mIsBuyingItem = true;
        mBuyQuantity = 1;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel") && !mIsShowingDetails)
    {
        HandleExit();
        return;
    }
}
void AdvUIVendor::UpdateBuybackItem()
{
    ///--[Documentation and Setup]
    //--When the player has selected an item they want to buy, this handles the input. The player
    //  can adjust the quantity, purchase, or cancel out.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the item package.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rBuybackList = rInventory->GetBuybackList();
    ShopInventoryPack *rPack = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mCursor);
    if(!rPack) return;

    //--Fast-access pointer.
    AdventureItem *rItem = rPack->mItem;
    if(!rItem) return;

    ///--[Left and Right]
    //--Adjust quantity down. Wraps to maximum.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        AdjustQuantity(mSellQuantity, -1);
        return;
    }

    //--Adjust quantity up. Wraps to minimum.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        AdjustQuantity(mSellQuantity, 1);
        return;
    }

    ///--[Activate]
    //--Buy the item in question in the given quantity.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Flag.
        bool tNeedsToDeallocate = false;
        mIsBuyingItem = false;

        //--Buying the whole stack:
        if(!rItem->IsStackable() || mBuyQuantity >= rPack->mQuantity)
        {
            //--Remove the buyback package from the list. It is not deallocated yet.
            tNeedsToDeallocate = true;
            rBuybackList->SetRandomPointerToThis(rPack);
            rBuybackList->LiberateRandomPointerEntry();

            //--If the cursor was at the end of the list, decrement.
            if(mCursor >= rBuybackList->GetListSize()) mCursor --;
            if(mCursor < 0) mCursor = 0;

            //--Put the item in the player's inventory if it was not adamantite.
            if(!rItem->IsAdamantite())
            {
                //--Simply run the construction script again.
                const char *rItemName = rItem->GetName();
                for(int i = 0; i < mBuyQuantity; i ++) LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", rItemName, "N", 1.0f);
            }
            //--Adamantite. Has a special registering algorithm. If the item was flagged as "UNLIMITED" then we won't deallocate it.
            else
            {
                rInventory->RegisterAdamantite(rItem, false);
            }
        }
        //--Buying part of a stack:
        else
        {
            //--Decrement the quantity.
            rPack->mQuantity -= mBuyQuantity;

            //--Run the construction script again.
            const char *rItemName = rItem->GetName();
            for(int i = 0; i < mBuyQuantity; i ++) LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", rItemName, "N", 1.0f);
        }

        //--Get how much the item costs.
        int tItemCost = rItem->GetValue();
        if(rPack->mPriceOverride > -1) tItemCost = rPack->mPriceOverride;
        tItemCost = tItemCost * mBuyQuantity;

        //--Decrement the player's case.
        rInventory->SetPlatina(rInventory->GetPlatina() - tItemCost);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

        //--Buying the whole stack:
        if(tNeedsToDeallocate)
        {
            //--Deallocate the inventory package.
            ShopInventoryPack::DeleteThis(rPack);
        }

        //--Scan the active characters. See how many can equip the item.
        mAskToEquipSlot = -1;
        for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
        {
            //--Get character.
            AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
            if(!rCharacter) continue;

            //--Can the character equip it?
            if(rItem->IsEquippableBy(rCharacter->GetName()))
            {
                //--If the equip slot is -1, this is the first character that can equip it.
                if(mAskToEquipSlot == -1)
                {
                    mAskToEquipSlot = i;
                }
                //--If it's not -1, then at least two characters can equip the item. Don't show the equip dialogue.
                else
                {
                    mAskToEquipSlot = -1;
                    return;
                }
            }
        }

        //--If we exited this loop with the mAskToEquipSlot being -1, nobody can equip it.
        if(mAskToEquipSlot == -1)
        {
            StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();
            ShopInventoryPack *rPackage = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mCursor);
            if(rPackage) RecheckComparisonCharacter(rPackage->mItem);
            return;
        }

        //--If we got this far, we should show the equip prompt.
        mAskToEquip = true;
        return;
    }

    ///--[Cancel]
    //--Exit without buying anything.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsBuyingItem = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        if(mShopTeardownPath && strcasecmp(mShopTeardownPath, "Null"))
        {
            LuaManager::Fetch()->ExecuteLuaFile(mShopTeardownPath);
            ResetString(mShopTeardownPath, NULL);
        }
    }
}
void AdvUIVendor::UpdateBuybackEquipItem()
{
    ///--[Documentation]
    //--Handles the update during the small menu which asks the player to equip the item they just purchased.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Activate]
    //--Equips the item.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Common.
        mAskToEquip = false;
        AudioManager::Fetch()->PlaySound("World|TakeItem");

        //--Get the character in question.
        AdvCombatEntity *rEntity = AdvCombat::Fetch()->GetActiveMemberI(mAskToEquipSlot);
        if(!rEntity) return;

        //--Get the item from the inventory. It was just purchased so it should be the last-purchased item.
        AdventureItem *rLastReggedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
        if(!rLastReggedItem) return;

        //--Dummy variable.
        int tDummy = 5;

        //--Make a list of all the slots that can accept this item. Weapon alternates are ignored.
        int tEquipSlotsTotal = rEntity->GetEquipmentSlotsTotal();
        StarLinkedList *tSlotList = new StarLinkedList(false);
        for(int i = 0; i < tEquipSlotsTotal; i ++)
        {
            //--Ignore weapon alts.
            if(rEntity->IsWeaponAlternateSlot(i)) continue;

            //--If it can be equipped in this slot, store it.
            const char *rSlotName = rEntity->GetNameOfEquipmentSlot(i);
            if(rLastReggedItem->IsEquippableIn(rSlotName))
            {
                tSlotList->AddElement(rSlotName, &tDummy);
            }
        }

        //--We now have a list of slots that can accept the item. If the list somehow came back zero-length, do nothing.
        if(tSlotList->GetListSize() < 1)
        {
            delete tSlotList;
            return;
        }

        //--If there is exactly one entry, equip to that slot. Done!
        int tUseSlot = -1;
        if(tSlotList->GetListSize() == 1)
        {
        }
        //--Otherwise, check if any of the slots in question are empty. If so, use those.
        else
        {
            for(int i = 0; i < tSlotList->GetListSize(); i ++)
            {
                //--Name.
                const char *rSlotName = tSlotList->GetNameOfElementBySlot(i);

                //--Check if the slot is empty.
                AdventureItem *rCheckItem = rEntity->GetEquipmentBySlotS(rSlotName);
                if(!rCheckItem)
                {
                    tUseSlot = i;
                    break;
                }
            }
        }

        //--If no slot was selected, use the slot that is being compared against.
        bool tAcceptedItem = false;
        if(tUseSlot == -1)
        {
            tAcceptedItem = rEntity->EquipItemToSlot(mCompareSlot, rLastReggedItem);
        }
        //--Otherwise, equip to the slot.
        else
        {
            const char *rSlotName = tSlotList->GetNameOfElementBySlot(tUseSlot);
            tAcceptedItem = rEntity->EquipItemToSlot(rSlotName, rLastReggedItem);
        }

        //--If flagged, the entity accepted the item.
        if(tAcceptedItem) AdventureInventory::Fetch()->LiberateItemP(rLastReggedItem);

        //--Check the last registered item, which will be the item unequipped. If it exists, remove any gems from it.
        //  Gems get passed across by the equip routine, but if the new item has fewer slots, some may be left over.
        AdventureItem *rUnequippedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
        if(rUnequippedItem)
        {
            for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
            {
                AdventureItem *rPrevGem = rUnequippedItem->RemoveGemFromSlot(i);
                if(rPrevGem)
                {
                    AdventureInventory::Fetch()->RegisterItem(rPrevGem);
                }
            }
        }

        //--Recompute statistics.
        rLastReggedItem->ComputeStatistics();
        rEntity->ComputeStatistics();

        //--If a script is specified, call it to handle any post-menu changes.
        SetCloseCode("Change Equipment");

        //--Recheck comparison character.
        StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();
        ShopInventoryPack *rPackage = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mCursor);
        if(rPackage) RecheckComparisonCharacter(rPackage->mItem);

        //--Clean.
        delete tSlotList;
        return;
    }

    ///--[Cancel]
    //--Doesn't.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Recheck comparison.
        RecheckComparisonCharacter(GetConsideredItem());

        //--Etc.
        mAskToEquip = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}

///========================================= Rendering ============================================
void AdvUIVendor::RenderLftBuyback(float pVisAlpha)
{
    RenderCommonLft(pVisAlpha);
}
void AdvUIVendor::RenderTopBuyback(float pVisAlpha)
{
    RenderCommonTop(pVisAlpha);
}
void AdvUIVendor::RenderRgtBuyback(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    //--Comparison Block
    AdvImages.rFrame_Comparison->Draw();
    RenderBuybackProperties(pVisAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderBotBuyback(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Fast-access pointers.
    StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();

    ///--[Render]
    //--Inventory Listing.
    AdvImages.rFrame_Vendor->Draw();

    //--Tabs and titles.
    RenderModeTabs(cColorAlpha);
    RenderModeTitles(cColorAlpha);

    //--Render entries.
    RenderBuybackItemListing(mSkip, cScrollPage, cColorAlpha);

    //--Note: Scrollbars are not nominally possible since the buyback list has a hard size cap.

    //--If there are no entries on the item list, render this instead.
    if(rBuybackList->GetListSize() < 1)
    {
        AdvImages.rFont_Mainline->DrawTextFixed(267.0f, 189.0f, cxAdvMaxNameWid, 0, "There is nothing to buy back.");
    }

    //--Detail. Goes over the entries since the columns overwrite the black backing.
    AdvImages.rOverlay_DetailBuyback->Draw();

    //--Highlight.
    if(rBuybackList->GetListSize() > 0)
        RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Help Strings.
    //mShowHelpString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only renders if character swapping is possible.
    if(mShowSwapArrows)
        mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only renders if multiple compare slots are present.
    if(mCanSwapCompareSlot)
        mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);


    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderUnalignedBuyback(float pVisAlpha)
{
    ///--[Modes]
    AdventureItem *rDetailsItem = GetConsideredItem();
    RenderItemDetails(mDetailsTimer, cxAdvDetailsTicks, rDetailsItem);
    RenderBuybackItem(pVisAlpha);
    RenderBuybackEquipItem(pVisAlpha);
}

///===================================== Common Rendering =========================================
void AdvUIVendor::RenderBuybackProperties(float pVisAlpha)
{
    //--Handles rendering the properties of the item currently highlighted, as well as comparison to the
    //  comparison character if applicable.
    StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();
    ShopInventoryPack *rShopPackage = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mCursor);
    if(!rShopPackage) return;

    //--Pass this off to a subroutine.
    RenderComparisonPane(rShopPackage->mItem, pVisAlpha);
}
void AdvUIVendor::RenderBuybackItem(float pVisAlpha)
{
    ///--[Documentation]
    //--When the player chooses an item to purchase, this UI appears over the rest of the UI. It shows
    //  what they are purchasing and the cost.
    ShopInventoryPack *rConsiderationPack = GetConsideredItemPack();
    AdventureItem *rConsiderationItem = GetConsideredItem();
    if(!rConsiderationPack || !rConsiderationItem) return;

    ///--[Darken Background]
    //--Local offset.
    float cPurchasePct = EasingFunction::QuadraticInOut(mBuyTimer, cxAdvBuyTicks);

    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cPurchasePct * 0.7f));

    ///--[Positioning]
    //--Multiply by parent offset.
    float tXOffset, tYOffset, tColorAlpha;
    StandardPieceOpen(DIR_UP, pVisAlpha * cPurchasePct, tXOffset, tYOffset, tColorAlpha);
    HandlePositioning(tXOffset * 1.0f, tYOffset * 1.0f, tColorAlpha);

    ///--[Rendering]
    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(tColorAlpha);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Purchase");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tColorAlpha);

    //--Centering computations.
    char *tTextBuf = InitializeString("%s x %i", rConsiderationItem->GetName(), mBuyQuantity);
    float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tTextBuf);
    float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

    //--Item Icon
    StarBitmap *rItemIcon = rConsiderationItem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 205.0f);

    //--Compute the item cost.
    int tItemCost = rConsiderationItem->GetValue();
    if(rConsiderationPack->mPriceOverride > -1) tItemCost = rConsiderationPack->mPriceOverride;
    tItemCost = tItemCost * mBuyQuantity;

    //--Item name.
    AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, tTextBuf);
    AdvImages.rFont_Mainline->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 230.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Costs %i Platina (You have %i)", tItemCost, AdventureInventory::Fetch()->GetPlatina());

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(757.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Finish Up]
    //--Clean.
    HandlePositioning(tXOffset * -1.0f, tYOffset * -1.0f, tColorAlpha);
    free(tTextBuf);
}
void AdvUIVendor::RenderBuybackEquipItem(float pVisAlpha)
{
    ///--[Documentation]
    //--This display pops up when the player purchases an item and asks them to equip it.

    //--Get the character who should equip the consideration item.
    AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(mAskToEquipSlot);
    if(!rCharacter) return;

    //--Get their name.
    const char *rEquipName = rCharacter->GetDisplayName();
    if(!rEquipName) return;

    ///--[Darken Background]
    //--Local offset.
    float cPurchasePct = EasingFunction::QuadraticInOut(mAskToEquipTimer, cxAdvAskEquipTicks);

    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cPurchasePct * 0.7f));

    ///--[Positioning]
    //--Multiply by parent offset.
    float tXOffset, tYOffset, tColorAlpha;
    StandardPieceOpen(DIR_UP, pVisAlpha * cPurchasePct, tXOffset, tYOffset, tColorAlpha);
    HandlePositioning(tXOffset * 1.0f, tYOffset * 1.0f, tColorAlpha);

    ///--[Rendering]
    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--Text.
    AdvImages.rFont_Heading->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equip this item to %s?", rEquipName);

    ///--[Finish Up]
    //--Clean.
    HandlePositioning(tXOffset * -1.0f, tYOffset * -1.0f, tColorAlpha);
}
