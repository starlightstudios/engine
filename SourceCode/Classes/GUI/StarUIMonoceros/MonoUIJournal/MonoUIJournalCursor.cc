//--Base
#include "MonoUIJournal.h"

//--Classes
//--CoreClasses
#include "StarFont.h"

//--Definitions
//--Libraries
//--Managers

void MonoUIJournal::ComputeBestiaryCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mBestiaryCursor - mBestiaryOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mBestiaryEntries->GetElementBySlot(mBestiaryCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = MonoImages.rFontMainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}
void MonoUIJournal::ComputeCombatCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mCombatCursor - mCombatOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mCombatEntries->GetElementBySlot(mCombatCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = MonoImages.rFontMainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}
void MonoUIJournal::ComputeLocationCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mLocationCursor - mLocationOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mLocationEntries->GetElementBySlot(mLocationCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = MonoImages.rFontMainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}
void MonoUIJournal::ComputeParagonCursorPosition()
{

}
void MonoUIJournal::ComputeProfileCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mProfileCursor - mProfileOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mProfileEntries->GetElementBySlot(mProfileCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = MonoImages.rFontMainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}
void MonoUIJournal::ComputeQuestCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mQuestCursor - mQuestOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mQuestEntries->GetElementBySlot(mQuestCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = MonoImages.rFontMainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}
