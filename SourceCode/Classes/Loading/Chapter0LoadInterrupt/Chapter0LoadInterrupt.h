///=================================== Chapter0LoadInterrupt ======================================
//--Version of the StarLoadInterrupt, does not have any particular chapter in mind and thus can be
//  used for mods or games that don't have specific interrupts.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLoadInterrupt.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define LI_CH0_DOTS 3

///========================================== Classes =============================================
class Chapter0LoadInterrupt : public StarLoadInterrupt
{
    private:
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    bool mIsReady;
    StarBitmap *mLoading;
    StarBitmap *mLoadingPeriod[LI_CH0_DOTS];

    protected:

    public:
    //--System
    Chapter0LoadInterrupt();
    virtual ~Chapter0LoadInterrupt();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators

    //--Core Methods
    virtual void DeallocateImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Call(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_BootChapter0(lua_State *L);
