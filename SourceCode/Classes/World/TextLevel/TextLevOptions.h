///====================================== TextLevOptions ==========================================
//--Options menu that appears when the player clicks the gear icon on the main game screen.

#pragma once
///==========================================
#include "Definitions.h"
#include "Structures.h"
#include "StringTyrantTitleStructures.h"

///===================================== Local Structures =========================================
typedef struct TLO_Option_Pack
{
    float mWheelScaler;
    char mDisplay[STD_MAX_LETTERS];
    TwoDimensionReal mClickDim;
    STTMenuOptionPack mOptionPack;
    void Initialize()
    {
        mWheelScaler = 0.0f;
        memset(mDisplay, 0, sizeof(mDisplay));
        mClickDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
        mOptionPack.Initialize();
    }
    void SetI(const char *pDisplay, float pLft, float pTop, float pWid, float pHei, int pValue)
    {
        strncpy(mDisplay, pDisplay, sizeof(mDisplay));
        mClickDim.SetWH(pLft, pTop, pWid, pHei);
        mOptionPack.mType = POINTER_TYPE_INT;
        mOptionPack.mValue.i = pValue;
    }
}TLO_Option_Pack;

///===================================== Local Definitions ========================================
//--Timers
#define TLO_VISIBLITY_TICKS 30
#define TLO_CONFIRM_TICKS 5

//--Button Identification
#define TLO_BTN_SAVE 0
#define TLO_BTN_CANCEL 1
#define TLO_BTN_ENDINGS 2
#define TLO_BTN_ENDINGBACK 3
#define TLO_BTN_TOTAL 4

//--Options
#define TLO_OPTION_MUSICVOL 0
#define TLO_OPTION_SOUNDVOL 1
#define TLO_OPTION_COLORTHEME 2
#define TLO_OPTION_TEXTSIZE 3
#define TLO_OPTION_RESOLUTION 4
#define TLO_OPTION_FULLSCREEN 5
#define TLO_OPTION_DOLLTYPE 6
#define TLO_OPTION_DOLLCOLOR 7
#define TLO_OPTION_BACKTOTITLE 8
#define TLO_OPTION_EXITGAME 9
#define TLO_OPTION_TOTAL 10

//--Endings
#define TLO_ENDING_PLAYTHING 0
#define TLO_ENDING_FACEINACROWD 1
#define TLO_ENDING_GALLERY 2
#define TLO_ENDING_MALLEABLE 3
#define TLO_ENDING_GOODDOLLY 4
#define TLO_ENDING_MISTRESSDOLLS 5
#define TLO_ENDING_TRUEHERO 6
#define TLO_ENDING_JESSIE 7
#define TLO_ENDING_LAUREN 8
#define TLO_ENDING_SARAH 9
#define TLO_ENDING_MARIONETTE 10
#define TLO_ENDING_RUBBER 11
#define TLO_ENDING_ICE 12
#define TLO_ENDING_CLAYHOUSE 13
#define TLO_ENDING_GLASS 14
#define TLO_ENDINGS_TOTAL 15

///========================================== Classes =============================================
class TextLevOptions : public RootObject
{
    private:
    //--System
    TextLevel *rCaller;
    bool mIsVisible;
    int mVisibilityTimer;

    //--Positions
    TwoDimensionReal mTxtHeading;
    TwoDimensionReal mTxtConfirm;

    //--Buttons
    TwoDimensionReal mBtnDim[TLO_BTN_TOTAL];

    //--Options
    float mOldMouseZ;
    int mHighlightedOption;
    TLO_Option_Pack mOptionPacks[TLO_OPTION_TOTAL];

    //--Endings Mode
    bool mIsEndingsMode;
    TLO_Option_Pack mEndingPacks[TLO_ENDINGS_TOTAL];

    //--String Storage
    char mColorThemeBuf[STD_MAX_LETTERS];
    char mTextSizeBuf[STD_MAX_LETTERS];
    char mDisplayModeBuf[STD_MAX_LETTERS];
    char mDollTypeBuf[STD_MAX_LETTERS];
    char mDollColorBuf[STD_MAX_LETTERS];

    //--Confirmation.
    bool mIsConfirmation;
    int mConfirmationTimer;
    char mConfirmationText[STD_MAX_LETTERS];
    int mConfirmationOptionHandler;
    TwoDimensionReal mConfirmBtnNo;
    TwoDimensionReal mConfirmBtnYes;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontHeader;
            StarFont *rFontMain;

            //--Images
            StarBitmap *rBacking;
            StarBitmap *rBtnBack;
            StarBitmap *rBtnSave;
            StarBitmap *rBtnCancel;
            StarBitmap *rBtnEndings;
            StarBitmap *rConfirm;
        }Data;
    }Images;

    protected:

    public:
    //--System
    TextLevOptions();
    virtual ~TextLevOptions();
    void Construct(TextLevel *pCaller);
    void ConstructOptions(TextLevel *pCaller);
    void ConstructEndings(TextLevel *pCaller);

    //--Public Variables
    static char *xResetExec;
    static char *xPostExecPath;
    static char *xEndingExec;
    static int xEndingOverride;

    //--Property Queries
    bool IsActive();

    //--Manipulators
    void Activate();
    void Deactivate();
    void SetPostExec(const char *pPath);

    //--Core Methods
    void RefreshValues(TextLevel *pCaller);
    void SaveChanges();
    void HandleConfirmClick();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update(bool pAllowControls);
    void UpdateConfirmation();
    void UpdateOptions();
    void UpdateEndings();

    //--File I/O
    //--Drawing
    void Render();
    void RenderOptions();
    void RenderEndings();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

