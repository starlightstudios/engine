//--Base
#include "AdventureItem.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
AdventureItem::AdventureItem()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_ADVENTUREITEM;

    ///--[ ===== AdventureItem ====== ]
    //--System
    mIsAdamantite = false;
    mIsStackable = false;
    mStackSize = 1;
    mItemUniqueID = xItemUniqueIDCounter;
    xItemUniqueIDCounter ++;
    mLocalName = InitializeString("AdventureItem");
    mDisplayName = InitializeString("AdventureItem");
    mDescription = NULL;
    mDescriptionType = NULL;
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++) mAdvancedDescription[i] = new StarlightString();
    mHasOverrideQuality = false;
    mOverrideQuality = 0.0f;

    //--Value
    mValue = 0;

    //--Consumable
    mIsConsumable = false;

    //--Key Item
    mIsUnique = false;
    mIsKeyItem = false;

    //--Equipment
    mIsEquipment = false;
    mEquipmentAttackSound = NULL;
    mEquipmentCriticalSound = NULL;
    mEquipmentAttackAnimation = NULL;
    mEquipSlotList = NULL;
    mEquipEntityList = NULL;
    mEquipmentBonus.Zero();
    mFinalEquipBonus.Zero();
    for(int i = 0; i < ADITEM_DAMAGETYPE_TOTAL; i ++)
    {
        mEquipmentDamageTypes[i].Initialize();
    }

    //--Gem Slots
    mGemSlotsTotal = 0;
    mOriginalGemName = NULL;
    rOriginalGemIcon = NULL;
    mOriginalGemDescription = NULL;
    memset(mGemSlots, 0, sizeof(AdventureItem *) * ADITEM_MAX_GEMS);

    //--Gems
    mIsGem = false;
    mGemColorCode = 0;

    //--Combat Items
    mAbilityPath = NULL;

    //--Tags
    mTagList = new StarLinkedList(true);

    //--Rendering
    rIconImg = NULL;

    //--Public Variables
    mGemErrorFlag = false;
    rGemParent = NULL;
    rEquippingEntity = NULL;

    ///--[Special]
    //--Clear the global flag for the last-registered item icon.
    AdventureInventory::xrLastRegisteredIcon = NULL;
}
AdventureItem::~AdventureItem()
{
    free(mLocalName);
    free(mDisplayName);
    free(mDescription);
    free(mDescriptionType);
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++) delete mAdvancedDescription[i];
    free(mEquipmentAttackSound);
    free(mEquipmentCriticalSound);
    free(mEquipmentAttackAnimation);
    delete mEquipSlotList;
    delete mEquipEntityList;
    free(mOriginalGemName);
    free(mOriginalGemDescription);
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++) delete mGemSlots[i];
    free(mAbilityPath);
    delete mTagList;
}

///--[Public Statics]
//--Unique ID counter for items. All items in the game get an ID unique to them, which is used to
//  track their upgrade state. The ID values can never be 0.
int AdventureItem::xItemUniqueIDCounter = 1;

//--Comparison Lines. These lines are used when displaying comparisons between two items in the equipment
//  or shop screen. The names provided are the names of the comparing items, and the lines are not rebuilt
//  if both of the items are identical. Otherwise, building them is similar to building property lines.
char *AdventureItem::xComparisonItemA = NULL;
char *AdventureItem::xComparisonItemB = NULL;
int AdventureItem::xComparisonLinesTotal = 0;
char **AdventureItem::xComparisonLines = NULL;

//--Stores translation data. Should point to a data block registered to the TranslationManager.
AdvItem_Strings *AdventureItem::xrStringData = NULL;

///--[Static Listings]
//--List lookups.
bool AdventureItem::xHasBuiltStatIcoList = false;
bool AdventureItem::xHasBuiltDamageNameList = false;
StarBitmap *AdventureItem::xStaticStatIcoList[STATS_TOTAL];

//--Functions that build these.
void AdventureItem::InitializeStatIcoList()
{
    //--Setup.
    xHasBuiltStatIcoList = true;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Listing.
    xStaticStatIcoList[STATS_HPMAX]             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Health");
    xStaticStatIcoList[STATS_MPMAX]             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Mana");
    xStaticStatIcoList[STATS_MPREGEN]           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Mana");
    xStaticStatIcoList[STATS_CPMAX]             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Health");
    xStaticStatIcoList[STATS_FREEACTIONMAX]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Health");
    xStaticStatIcoList[STATS_FREEACTIONGEN]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Health");
    xStaticStatIcoList[STATS_ATTACK]            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Attack");
    xStaticStatIcoList[STATS_INITIATIVE]        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Initiative");
    xStaticStatIcoList[STATS_ACCURACY]          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Accuracy");
    xStaticStatIcoList[STATS_EVADE]             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Evade");
    xStaticStatIcoList[STATS_RESIST_PROTECTION] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Protection");
    xStaticStatIcoList[STATS_RESIST_SLASH]      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Slashing");
    xStaticStatIcoList[STATS_RESIST_STRIKE]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Striking");
    xStaticStatIcoList[STATS_RESIST_PIERCE]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Piercing");
    xStaticStatIcoList[STATS_RESIST_FLAME]      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Flaming");
    xStaticStatIcoList[STATS_RESIST_FREEZE]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Freezing");
    xStaticStatIcoList[STATS_RESIST_SHOCK]      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Shocking");
    xStaticStatIcoList[STATS_RESIST_CRUSADE]    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Crusading");
    xStaticStatIcoList[STATS_RESIST_OBSCURE]    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Obscuring");
    xStaticStatIcoList[STATS_RESIST_BLEED]      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Bleeding");
    xStaticStatIcoList[STATS_RESIST_POISON]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Poisoning");
    xStaticStatIcoList[STATS_RESIST_CORRODE]    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Corroding");
    xStaticStatIcoList[STATS_RESIST_TERRIFY]    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Terrifying");
    xStaticStatIcoList[STATS_STUN_CAP]          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Health");
    xStaticStatIcoList[STATS_THREAT_MULTIPLIER] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Health");
}

///==================================== Subclass Functions ========================================
AdvItem_Strings::AdvItem_Strings()
{
    //--Local name: "AdventureItem"
    str.mCraftItemType   = InitializeString("AdventureItem_mCraftItemType");        //"Crafting Item"
    str.mNoTypeSet       = InitializeString("AdventureItem_mNoTypeSet");            //"Type: No Type Set"
    str.mType            = InitializeString("AdventureItem_mType");                 //"Type: "
    str.mIsKeyItem       = InitializeString("AdventureItem_mIsKeyItem");            //"[Key Item]"
    str.mValueless       = InitializeString("AdventureItem_mValueless");            //"Value: Worthless"
    str.mValue           = InitializeString("AdventureItem_mValue");                //"Value: [iValue] [sImageCnt]"
    str.mGemSlots        = InitializeString("AdventureItem_mGemSlots");             //"Gem Slots: "
    str.mDamageType      = InitializeString("AdventureItem_mDamageType");           //"Damage Type: [sIconStr] ([sDamageStr])"
    str.mType_Protection = InitializeString("AdventureItem_mProtection");           //"Protection"
    str.mType_Slashing   = InitializeString("AdventureItem_mSlashing");             //"Slashing"
    str.mType_Striking   = InitializeString("AdventureItem_mStriking");             //"Striking"
    str.mType_Piercing   = InitializeString("AdventureItem_mPiercing");             //"Piercing"
    str.mType_Flaming    = InitializeString("AdventureItem_mFlaming");              //"Flaming"
    str.mType_Freezing   = InitializeString("AdventureItem_mFreezing");             //"Freezing"
    str.mType_Shocking   = InitializeString("AdventureItem_mShocking");             //"Shocking"
    str.mType_Crusading  = InitializeString("AdventureItem_mCrusading");            //"Crusading"
    str.mType_Obscuring  = InitializeString("AdventureItem_mObscuring");            //"Obscuring"
    str.mType_Bleeding   = InitializeString("AdventureItem_mBleeding");             //"Bleeding"
    str.mType_Poisoning  = InitializeString("AdventureItem_mPoisoning");            //"Poisoning"
    str.mType_Corroding  = InitializeString("AdventureItem_mCorroding");            //"Corroding"
    str.mType_Terrifying = InitializeString("AdventureItem_mTerrifying");           //"Terrifying"
    AdventureItem::xrStringData = this;
}
AdvItem_Strings::~AdvItem_Strings()
{
    for(int i = 0; i < ADVITEM_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int AdvItem_Strings::GetSize()
{
    return ADVITEM_STRINGS_TOTAL;
}
void AdvItem_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= ADVITEM_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
bool AdventureItem::IsAdamantite()
{
    return mIsAdamantite;
}
bool AdventureItem::IsConsumable()
{
    return mIsConsumable;
}
bool AdventureItem::IsStackable()
{
    return mIsStackable;
}
int AdventureItem::GetStackSize()
{
    if(!mIsStackable) return 1;
    return mStackSize;
}
const char *AdventureItem::GetAbilityPath()
{
    return mAbilityPath;
}
uint32_t AdventureItem::GetItemUniqueID()
{
    return mItemUniqueID;
}
const char *AdventureItem::GetName()
{
    return (const char *)mLocalName;
}
const char *AdventureItem::GetDisplayName()
{
    return (const char *)mDisplayName;
}
const char *AdventureItem::GetDescription()
{
    return (const char *)mDescription;
}
const char *AdventureItem::GetDescriptionType()
{
    return (const char *)mDescriptionType;
}
int AdventureItem::GetValue()
{
    ///--[Documentation]
    //--Returns the value of the item. This includes adding all the gems merged into it if it is a gem
    //  that can be upgraded.
    if(!mIsGem) return mValue;

    //--Sum up the value of all gems inside this one if it's a gem.
    int tValue = mValue;
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        if(mGemSlots[i]) tValue = tValue + mGemSlots[i]->GetValue();
    }
    return tValue;
}
int AdventureItem::GetSlottedGems()
{
    //--Counts and returns how many of the gem slots are filled with gems.
    int tCount = 0;
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        if(mGemSlots[i]) tCount ++;
    }

    //--Finish.
    return tCount;
}
int AdventureItem::GetGemSlots()
{
    return mGemSlotsTotal;
}
bool AdventureItem::IsUnique()
{
    return mIsUnique;
}
bool AdventureItem::IsKeyItem()
{
    return mIsKeyItem;
}
StarBitmap *AdventureItem::GetIconImage()
{
    return rIconImg;
}
int AdventureItem::GetTagCount(const char *pTag)
{
    ///--[Documentation]
    //--Given a tag, returns how many times that tag appears both in this piece of equipment, and
    //  in any and all gems socketed into the equipment.
    if(!pTag) return 0;

    //--Base value.
    int tTagCount = 0;
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(rTag) tTagCount += (*rTag);

    //--Gems.
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        if(!mGemSlots[i]) continue;
        tTagCount += mGemSlots[i]->GetTagCount(pTag);
    }

    //--Finish up.
    return tTagCount;
}
StarlightString *AdventureItem::GetAdvancedDescription(int pSlot)
{
    if(pSlot < 0 || pSlot >= ADITEM_DESCRIPTION_MAX) return NULL;
    return mAdvancedDescription[pSlot];
}

///======================================== Manipulators ==========================================
void AdventureItem::SetAdamantite()
{
    mIsAdamantite = true;
    ResetString(mDescriptionType, xrStringData->str.mCraftItemType);
}
void AdventureItem::SetConsumable()
{
    mIsConsumable = true;
}
void AdventureItem::SetStackable(bool pIsStackable)
{
    mIsStackable = pIsStackable;
}
void AdventureItem::SetStackSize(int pSize)
{
    mStackSize = pSize;
    if(mStackSize < 1) mStackSize = 1;
}
void AdventureItem::SetAbilityPath(const char *pPath)
{
    ResetString(mAbilityPath, pPath);
}
void AdventureItem::OverrideID(uint32_t pID)
{
    //--Note: Should only be used when loading a save file!
    mItemUniqueID = pID;
}
void AdventureItem::SetName(const char *pName)
{
    ///--[Documentation]
    //--Sets the internal name of the item, and implicitly sets the display name either to the
    //  same string, or uses a translation if one is active.
    if(!pName) return;

    //--Set local string.
    ResetString(mLocalName, pName);

    ///--[Display Name]
    //--Check for an active translation.
    StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_ITEMS);
    if(rTranslation)
    {
        //--Check for a translated string. If found, use that.
        const char *rTranslatedString = rTranslation->GetTranslationFor(pName);
        if(rTranslatedString)
        {
            ResetString(mDisplayName, rTranslatedString);
        }
        //--Not found, use base.
        else
        {
            ResetString(mDisplayName, pName);
        }
    }
    //--No translation, use the local name.
    else
    {
        ResetString(mDisplayName, pName);
    }
}
void AdventureItem::SetDisplayName(const char *pName, bool pDisallowTranslation)
{
    ///--[Documentation]
    //--Manually sets the display name. Can optionally be flagged to ignore translation calls.
    if(!pName) return;

    //--Set local string.
    ResetString(mDisplayName, pName);

    ///--[Translation]
    //--If this flag is true, ignore translation.
    if(pDisallowTranslation) return;

    //--Check for an active translation.
    StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_ITEMS);
    if(!rTranslation) return;

    //--Check for a translated string. If found, use that.
    const char *rTranslatedString = rTranslation->GetTranslationFor(pName);
    if(rTranslatedString)
    {
        ResetString(mDisplayName, rTranslatedString);
    }
    //--Not found, use base.
    else
    {
        ResetString(mDisplayName, pName);
    }
}
void AdventureItem::SetDescription(const char *pDescription)
{
    ResetString(mDescription, pDescription);
}
void AdventureItem::SetType(const char *pType)
{
    ResetString(mDescriptionType, pType);
}
void AdventureItem::SetValue(int pValue)
{
    mValue = pValue;
}
void AdventureItem::SetUnique(bool pFlag)
{
    mIsUnique = pFlag;
}
void AdventureItem::SetKeyItem(bool pFlag)
{
    mIsKeyItem = pFlag;
}
void AdventureItem::SetConsumable(bool pFlag)
{
    mIsConsumable = pFlag;
}
void AdventureItem::SetEquipmentFlag(bool pFlag)
{
    ///--[Documentation]
    //--Marks the item as a piece of equipment which can be worn. If needed, initializes equipment storage
    //  variables, or deallocates them if unsetting the equipment flag.

    //--Store the old flag.
    bool tWasEquipment = mIsEquipment;

    //--Set the new flag.
    mIsEquipment = pFlag;

    ///--[Do Nothing Cases]
    //--Already was equipment, no need to allocate again.
    if(mIsEquipment && tWasEquipment) return;

    //--Wasn't equipment and isn't becoming it. Do nothing.
    if(!mIsEquipment && !tWasEquipment) return;

    ///--[Initialize]
    //--Initialization case:
    if(!tWasEquipment && mIsEquipment)
    {
        mEquipEntityList = new StarLinkedList(false);
        mEquipSlotList = new StarLinkedList(false);
        return;
    }

    ///--[Deallocate]
    //--Item was flagged as equipment, and is losing that property.
    if(tWasEquipment && !mIsEquipment)
    {
        delete mEquipEntityList;
        mEquipEntityList = NULL;
        delete mEquipSlotList;
        mEquipSlotList = NULL;
        return;
    }

    //--Should not be logically possible to reach this point.
}
void AdventureItem::SetIconImageS(const char *pPath)
{
    rIconImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    AdventureInventory::xrLastRegisteredIcon = rIconImg;
}
void AdventureItem::SetIconImageP(StarBitmap *pImage)
{
    rIconImg = pImage;
    AdventureInventory::xrLastRegisteredIcon = rIconImg;
}
AdventureItem *AdventureItem::PlaceGemInSlot(int pSlot, AdventureItem *pGem)
{
    ///--[Documentation]
    //--Removes the existing gem from the slot and passes it back, replacing it with the new
    //  gem. Takes ownership of the new gem, and the caller takes ownership of the returned gem.
    //--If the mGemErrorFlag is set to true, something went wrong and *ownership does not change*.
    //--It is legal to pass NULL in to remove a gem. It is also legal to receive NULL back
    //  if the gem slot was empty.
    mGemErrorFlag = false;

    //--Range check.
    if(pSlot < 0 || pSlot >= ADITEM_MAX_GEMS) { mGemErrorFlag = true; return NULL; }

    //--If this is not a gem, it has a gem slot cap. Gems can "equip" other gems via merging.
    //  When merging, use the function MergeWithGem(). When removing, use this function.
    if(pSlot >= mGemSlotsTotal && !mIsGem) { mGemErrorFlag = true; return NULL; }

    //--Take the current gem in the slot out. It can be NULL.
    AdventureItem *rPreviousGem = mGemSlots[pSlot];
    mGemSlots[pSlot] = pGem;

    //--If the previous gem exists, remove its parent pointer.
    if(rPreviousGem)
    {
        rPreviousGem->rGemParent = NULL;
    }

    //--If the new gem exists, set us as its parent.
    if(mGemSlots[pSlot])
    {
        mGemSlots[pSlot]->rGemParent = this;
    }

    //--Pass back the previous gem. It can be NULL, in which case the slot was empty.
    return rPreviousGem;
}
AdventureItem *AdventureItem::RemoveGemFromSlot(int pSlot)
{
    //--Removes the gem from the slot. Just calls PlaceGemInSlot() with NULL. Error flag rules
    //  are the same as before.
    return PlaceGemInSlot(pSlot, NULL);
}
void AdventureItem::SetOverrideQuality(float pQuality)
{
    mHasOverrideQuality = pQuality;
    mOverrideQuality = pQuality;
}
void AdventureItem::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdventureItem::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}
void AdventureItem::SetStaticImage(int pSlot, const char *pPath)
{
    if(pSlot < 0 || pSlot >= STATS_TOTAL) return;
    if(!pPath) return;
    xStaticStatIcoList[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}

///======================================== Core Methods ==========================================
AdventureItem *AdventureItem::Clone()
{
    //--Clones the item, creating an exact duplicate.
    AdventureItem *nClone = new AdventureItem();

    //--System
    nClone->mIsAdamantite = mIsAdamantite;
    nClone->mIsStackable = mIsStackable;
    nClone->mStackSize = 1;
    ResetString(nClone->mLocalName,       mLocalName);
    ResetString(nClone->mDisplayName,     mDisplayName);
    ResetString(nClone->mDescription,     mDescription);
    ResetString(nClone->mDescriptionType, mDescriptionType);
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        nClone->mAdvancedDescription[i] = mAdvancedDescription[i]->Clone();
    }
    nClone->mHasOverrideQuality = mHasOverrideQuality;
    nClone->mOverrideQuality = mOverrideQuality;

    //--Value
    nClone->mValue = mValue;

    //--Consumable
    nClone->mIsConsumable = mIsConsumable;

    //--Key Item
    nClone->mIsUnique = mIsUnique;
    nClone->mIsKeyItem = mIsKeyItem;

    //--Equipment
    nClone->mIsEquipment = mIsEquipment;
    if(mEquipEntityList)
    {
        nClone->mEquipEntityList = new StarLinkedList(false);
        void *rDummyPtr = mEquipEntityList->PushIterator();
        while(rDummyPtr)
        {
            nClone->mEquipEntityList->AddElement(mEquipEntityList->GetIteratorName(), rDummyPtr);
            mEquipEntityList->AutoIterate();
        }
    }
    if(mEquipSlotList)
    {
        nClone->mEquipSlotList = new StarLinkedList(false);
        void *rDummyPtr = mEquipSlotList->PushIterator();
        while(rDummyPtr)
        {
            nClone->mEquipSlotList->AddElement(mEquipSlotList->GetIteratorName(), rDummyPtr);
            mEquipSlotList->AutoIterate();
        }
    }
    memcpy(&nClone->mEquipmentBonus,  &mEquipmentBonus,  sizeof(mEquipmentBonus));
    memcpy(&nClone->mFinalEquipBonus, &mFinalEquipBonus, sizeof(mFinalEquipBonus));
    for(int i = 0; i < ADITEM_DAMAGETYPE_TOTAL; i ++)
    {
        memcpy(&nClone->mEquipmentDamageTypes[i], &mEquipmentDamageTypes[i], sizeof(mEquipmentDamageTypes[i]));
    }

    //--Gem Slots. Clones don't get their own copies of the gems.
    nClone->mGemSlotsTotal = mGemSlotsTotal;
    ResetString(nClone->mOriginalGemName, mOriginalGemName);
    nClone->rOriginalGemIcon = rOriginalGemIcon;
    ResetString(nClone->mOriginalGemDescription, mOriginalGemDescription);
    memset(mGemSlots, 0, sizeof(AdventureItem *) * ADITEM_MAX_GEMS);

    //--Gems
    nClone->mIsGem = mIsGem;
    nClone->mGemColorCode = mGemColorCode;

    //--Combat Items
    ResetString(nClone->mAbilityPath, mAbilityPath);

    //--Tags
    int *rTagCount = (int *)mTagList->PushIterator();
    while(rTagCount)
    {
        //--Duplicate value.
        int *nCloneTag = (int *)starmemoryalloc(sizeof(int));
        *nCloneTag = *rTagCount;

        //--Duplicate name.
        const char *rEntryName = mTagList->GetIteratorName();
        nClone->mTagList->AddElementAsTail(rEntryName, nCloneTag, &FreeThis);

        //--Next.
        rTagCount = (int *)mTagList->AutoIterate();
    }

    //--Rendering
    nClone->rIconImg = rIconImg;

    //--All done.
    return nClone;
}
void AdventureItem::ProcessDescription()
{
    ///--[Documentation]
    //--When creating an item description, the description may have tags in it. These tags get replaced
    //  by properties derived from the item.
    //--The description and properties should all be in the item at this point. For items that can
    //  be equipped, gems are NOT taken into account when computing statistics.
    if(!mDescription) return;

    //--Setup.
    int tAdvancedIndex = 0;
    StarAutoBuffer *tAutoBuffer = new StarAutoBuffer();
    StarLinkedList *tImageListing = new StarLinkedList(true); //StarlightStringImagePack *, master

    ///--[Clear]
    //--Reset all advanced description lines to be empty, in case the final doesn't use them.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        mAdvancedDescription[i]->SetString(" ");
        mAdvancedDescription[i]->AllocateImages(0);
    }

    ///--[Copy Iteration]
    //--Iterate across the string and begin copying letters.
    bool tRemainingLetters = false;
    int tLength = (int)strlen(mDescription);
    for(int i = 0; i < tLength; i ++)
    {
        //--If the letter is a '[' and the tag is '[BR]' then it's the same as a newline. This bypasses
        //  tags for most purposes.
        if(i < tLength - 3 && mDescription[i] == '[' && mDescription[i+1] == 'B' && mDescription[i+2] == 'R' && mDescription[i+3] == ']')
        {
            //--Cap with a null character.
            tAutoBuffer->AppendNull();

            //--Move autobuffer contents.
            char *rAutobufferContents = (char *)tAutoBuffer->GetRawData();
            mAdvancedDescription[tAdvancedIndex]->SetString(rAutobufferContents);
            delete tAutoBuffer;

            //--Upload image data.
            int o = 0;
            mAdvancedDescription[tAdvancedIndex]->AllocateImages(tImageListing->GetListSize());
            StarlightStringImagePack *rPackage = (StarlightStringImagePack *)tImageListing->PushIterator();
            while(rPackage)
            {
                //--Set.
                mAdvancedDescription[tAdvancedIndex]->SetImageP(o, rPackage->mYOffset, rPackage->rImage);

                //--Next.
                o ++;
                rPackage = (StarlightStringImagePack *)tImageListing->AutoIterate();
            }

            //--Crossreference images to tags.
            mAdvancedDescription[tAdvancedIndex]->CrossreferenceImages();

            //--Clear everything.
            tAutoBuffer = new StarAutoBuffer();
            tImageListing->ClearList();

            //--Next. If we exceed the max description lines, stop.
            tAdvancedIndex ++;
            tRemainingLetters = false;
            if(tAdvancedIndex >= ADITEM_DESCRIPTION_MAX) break;

            //--Advance a few characters.
            i = i + 3;
        }
        //--If the letter is a '[' left brace, it's a tag.
        else if(mDescription[i] == '[')
        {
            //--Figure out where the ending is.
            int tStart = i;
            int tEnd = i-1;
            for(int x = i+1; x < tLength; x ++)
            {
                if(mDescription[x] == ']')
                {
                    tEnd = x;
                    break;
                }
            }

            //--If the ending value is below the start value, the tag is malformed.
            if(tEnd < tStart)
            {
                fprintf(stderr, "AdventureItem::ProcessDescription: Error, tag is malformed. %s\n", mDescription);
                delete tAutoBuffer;
                delete tImageListing;
                return;
            }

            //--Subroutine handles the tags.
            HandleDescriptionTag(&mDescription[i], tAutoBuffer, tImageListing);

            //--Advance the cursor to the ending location to effectively skip the tag.
            i = tEnd;
            tRemainingLetters = true;
        }
        //--EOL case. Move to the next line.
        else if(mDescription[i] == 10 || mDescription[i] == 13)
        {
            //--Cap with a null character.
            tAutoBuffer->AppendNull();

            //--Move autobuffer contents.
            char *rAutobufferContents = (char *)tAutoBuffer->GetRawData();
            mAdvancedDescription[tAdvancedIndex]->SetString(rAutobufferContents);
            delete tAutoBuffer;

            //--Upload image data.
            int o = 0;
            mAdvancedDescription[tAdvancedIndex]->AllocateImages(tImageListing->GetListSize());
            StarlightStringImagePack *rPackage = (StarlightStringImagePack *)tImageListing->PushIterator();
            while(rPackage)
            {
                //--Set.
                mAdvancedDescription[tAdvancedIndex]->SetImageP(o, rPackage->mYOffset, rPackage->rImage);

                //--Next.
                o ++;
                rPackage = (StarlightStringImagePack *)tImageListing->AutoIterate();
            }

            //--Crossreference images to tags.
            mAdvancedDescription[tAdvancedIndex]->CrossreferenceImages();

            //--Clear everything.
            tAutoBuffer = new StarAutoBuffer();
            tImageListing->ClearList();

            //--Next. If we exceed the max description lines, stop.
            tAdvancedIndex ++;
            tRemainingLetters = false;
            if(tAdvancedIndex >= ADITEM_DESCRIPTION_MAX) break;
        }
        //--Otherwise, copy across.
        else
        {
            tAutoBuffer->AppendCharacter(mDescription[i]);
            tRemainingLetters = true;
        }
    }

    ///--[Leftovers]
    //--If the string didn't end with an EOL, and there are leftover letters, append them to the last string.
    if(tRemainingLetters)
    {
        //--Cap with a null character.
        tAutoBuffer->AppendNull();

        //--Move autobuffer contents.
        char *rAutobufferContents = (char *)tAutoBuffer->GetRawData();
        mAdvancedDescription[tAdvancedIndex]->SetString(rAutobufferContents);

        //--Upload image data.
        int o = 0;
        mAdvancedDescription[tAdvancedIndex]->AllocateImages(tImageListing->GetListSize());
        StarlightStringImagePack *rPackage = (StarlightStringImagePack *)tImageListing->PushIterator();
        while(rPackage)
        {
            //--Set.
            mAdvancedDescription[tAdvancedIndex]->SetImageP(o, rPackage->mYOffset, rPackage->rImage);

            //--Next.
            o ++;
            rPackage = (StarlightStringImagePack *)tImageListing->AutoIterate();
        }

        //--Crossreference images to tags.
        mAdvancedDescription[tAdvancedIndex]->CrossreferenceImages();
    }

    ///--[Clean Up]
    delete tAutoBuffer;
    delete tImageListing;
}
void AdventureItem::HandleDescriptionTag(const char *pString, StarAutoBuffer *pBuffer, StarLinkedList *pImageListing)
{
    ///--[Documentation]
    //--Given a string which starts with (but does not necessarily exclusively contain) a tag, handles adding the
    //  required information to the provided autobuffer, image listing, etc.
    //--These are used when turning a raw description string into a set of advanced description lines.
    if(!pString) return;

    ///--[INV]
    //--[INV] is a placeholder. It does not render any text.
    if(!strncasecmp(pString, "[INV]", (int)strlen("[INV]")))
    {
    }
    ///--[ICOATK]
    //--Shows the attack power icon.
    else if(!strncasecmp(pString, "[ICOATK]", (int)strlen("[ICOATK]")))
    {
        //--Put the image index into a buffer.
        int tImageIndex = pImageListing->GetListSize();
        char tNumBuffer[256];
        sprintf(tNumBuffer, "[IMG%i]", tImageIndex);

        //--Add an element to the image storage list representing the attack icon.
        StarlightStringImagePack *nPack = (StarlightStringImagePack *)starmemoryalloc(sizeof(StarlightStringImagePack));
        nPack->Initialize();
        nPack->mYOffset = 3.0f;
        nPack->rImage = GetStatIconBySlot(STATS_ATTACK);
        pImageListing->AddElementAsTail("X", nPack, &FreeThis);

        //--Append.
        pBuffer->AppendStringWithoutNull(tNumBuffer);
    }
    ///--[ICO----]
    //--Uses the large icons set. The part of the string after ICO is mapped onto the "Root/Images/AdventureUI/DamageTypeIconsLg/XXX"
    //  pattern. A lookup table can be found in the system 000 Variables.lua.
    else if(!strncasecmp(pString, "[ICO", (int)strlen("[ICO")))
    {
        //--Scan ahead and find the ']' which ends the string.
        int tEnd = 0;
        for(int i = 0; i < (int)strlen(pString); i ++)
        {
            tEnd = i;
            if(pString[i] == ']') break;
        }

        //--Locate the icon, make sure it exists.
        int tNeededLen = (int)strlen("Root/Images/AdventureUI/DamageTypeIconsLg/") + (tEnd - 4) + 1;
        char *tBuffer = (char *)malloc(sizeof(char) * tNeededLen);
        strcpy(tBuffer, "Root/Images/AdventureUI/DamageTypeIconsLg/");
        strncat(tBuffer, &pString[4], tEnd-4);
        StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(tBuffer);
        if(!rImage)
        {
            pBuffer->AppendStringWithoutNull("[IcoError]");
            fprintf(stderr, "Warning: Icon %s not found.\n", tBuffer);
            free(tBuffer);
            return;
        }

        //--Put the image index into a buffer.
        int tImageIndex = pImageListing->GetListSize();
        char tNumBuffer[256];
        sprintf(tNumBuffer, "[IMG%i]", tImageIndex);

        //--Add an element to the image storage list representing the attack icon.
        StarlightStringImagePack *nPack = (StarlightStringImagePack *)starmemoryalloc(sizeof(StarlightStringImagePack));
        nPack->Initialize();
        nPack->mYOffset = 3.0f;
        nPack->rImage = rImage;
        pImageListing->AddElementAsTail("X", nPack, &FreeThis);

        //--Append.
        pBuffer->AppendStringWithoutNull(tNumBuffer);

        //--Clean up.
        free(tBuffer);
    }
    ///--[ATK]
    //--[ATK] shows the attack power icon and the associated attack power.
    else if(!strncasecmp(pString, "[ATK]", (int)strlen("[ATK]")))
    {
        //--Put the image index into a buffer.
        int tImageIndex = pImageListing->GetListSize();
        char tNumBuffer[256];
        sprintf(tNumBuffer, "[IMG%i] +%i ", tImageIndex, mFinalEquipBonus.GetStatByIndex(STATS_ATTACK));

        //--Add an element to the image storage list representing the attack icon.
        StarlightStringImagePack *nPack = (StarlightStringImagePack *)starmemoryalloc(sizeof(StarlightStringImagePack));
        nPack->Initialize();
        nPack->mYOffset = 3.0f;
        nPack->rImage = GetStatIconBySlot(STATS_ATTACK);
        pImageListing->AddElementAsTail("X", nPack, &FreeThis);

        //--Append.
        pBuffer->AppendStringWithoutNull(tNumBuffer);
    }
    ///--[STATS]
    //--[STATS] shows the icon and value of all non-zero stat bonuses.
    else if(!strncasecmp(pString, "[STATS]", (int)strlen("[STATS]")))
    {
        //--Setup.
        char tNumBuffer[256];

        //--Iterate across the stat values.
        for(int i = 0; i < STATS_TOTAL; i ++)
        {
            //--If the value is zero, don't render anything.
            int tValue = mFinalEquipBonus.GetStatByIndex(i);
            if(tValue == 0) continue;

            //--Put the image index into a buffer.
            int tImageIndex = pImageListing->GetListSize();
            sprintf(tNumBuffer, "[IMG%i] %+i ", tImageIndex, tValue);

            //--Add an element to the image storage list representing the attack icon.
            StarlightStringImagePack *nPack = (StarlightStringImagePack *)starmemoryalloc(sizeof(StarlightStringImagePack));
            nPack->Initialize();
            nPack->mYOffset = 3.0f;
            nPack->rImage = GetStatIconBySlot(i);
            pImageListing->AddElementAsTail("X", nPack, &FreeThis);

            //--Append.
            pBuffer->AppendStringWithoutNull(tNumBuffer);
        }
    }
    ///--[TYPE]
    //--[TYPE] shows what kind of item something is. Weapon, armor, accessory, etc.
    else if(!strncasecmp(pString, "[TYPE]", (int)strlen("[TYPE]")))
    {
        //--No type is set.
        if(!mDescriptionType)
        {
            pBuffer->AppendStringWithoutNull(xrStringData->str.mNoTypeSet);
        }
        //--Type.
        else
        {
            pBuffer->AppendStringWithoutNull(xrStringData->str.mType);
            pBuffer->AppendStringWithoutNull(mDescriptionType);
        }
    }
    ///--[VALUE]
    //--[VALUE] shows what this item can be sold for. Key items cannot be sold and thus display "[Key Item]".
    else if(!strncasecmp(pString, "[VALUE]", (int)strlen("[VALUE]")))
    {
        //--Is a key item:
        if(mIsKeyItem)
        {
            pBuffer->AppendStringWithoutNull(xrStringData->str.mIsKeyItem);
        }
        //--Value is zero.
        else if(mValue < 1)
        {
            pBuffer->AppendStringWithoutNull(xrStringData->str.mValueless);
        }
        //--Otherwise, platina value.
        else
        {
            //--Put the image index into a buffer, then construct a string around it.
            char tImageBuf[32];
            sprintf(tImageBuf, "[IMG%i]", pImageListing->GetListSize());
            char *tAppendString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mValue, 2, "[iValue]", mValue, "[sImageCnt]", tImageBuf);

            //--Add an element to the image storage list representing the attack icon.
            StarlightStringImagePack *nPack = (StarlightStringImagePack *)starmemoryalloc(sizeof(StarlightStringImagePack));
            nPack->Initialize();
            nPack->mYOffset = 3.0f;
            nPack->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Platina");
            pImageListing->AddElementAsTail("X", nPack, &FreeThis);

            //--Append.
            pBuffer->AppendStringWithoutNull(tAppendString);
            free(tAppendString);
        }
    }
    ///--[GEMSLOTS]
    //--[GEMSLOTS] list how many gem slots the item has. Does nothing if the value is zero.
    else if(!strncasecmp(pString, "[GEMSLOTS]", (int)strlen("[GEMSLOTS]")))
    {
        //--Must have gems.
        if(mGemSlotsTotal < 1) return;

        //--Display.
        char tNumBuffer[32];
        sprintf(tNumBuffer, "%i", mGemSlotsTotal);
        pBuffer->AppendStringWithoutNull(xrStringData->str.mGemSlots);
        pBuffer->AppendStringWithoutNull(tNumBuffer);
    }
    ///--[DAMAGETYPE]
    //--[DAMAGETYPE] shows the damage icon and type in text. If there's more than one, shows the primary one.
    else if(!strncasecmp(pString, "[DAMAGETYPE]", (int)strlen("[DAMAGETYPE]")))
    {
        //--Resolve the highest damage type.
        int tHighestSlot = -1;
        float tHighestValue = 0.0f;
        for(int i = 0; i < ADVC_DAMAGE_TOTAL; i ++)
        {
            if(mEquipmentDamageTypes[ADITEM_DAMAGETYPE_BASE].mValueList[i] > tHighestValue)
            {
                tHighestSlot = i;
                tHighestValue = mEquipmentDamageTypes[ADITEM_DAMAGETYPE_BASE].mValueList[i];
            }
        }

        //--No highest damage, so this might not be a weapon. Discard.
        if(tHighestSlot == -1) return;

        //--Get the icon.
        StarBitmap *rIcon = GetStatIconBySlot(tHighestSlot + STATS_RESIST_START);

        //--Get the associated name.
        const char *rDamageString = GetDamageStringBySlot(tHighestSlot);

        //--Put the image index into a buffer.
        char tIconBuf[32];
        sprintf(tIconBuf, "[IMG%i]", pImageListing->GetListSize());
        char *tAppendString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mDamageType, 2, "[sIconStr]", tIconBuf, "[sDamageStr]", rDamageString);

        //--Add an element to the image storage list representing the attack icon.
        StarlightStringImagePack *nPack = (StarlightStringImagePack *)starmemoryalloc(sizeof(StarlightStringImagePack));
        nPack->Initialize();
        nPack->mYOffset = 3.0f;
        nPack->rImage = rIcon;
        pImageListing->AddElementAsTail("X", nPack, &FreeThis);

        //--Append.
        pBuffer->AppendStringWithoutNull(tAppendString);
        free(tAppendString);
    }
    ///--[GemDamage]
    //--For gems that change the damage type, renders the associated symbol and type.
    else if(!strncasecmp(pString, "[GemDamage]", (int)strlen("[GemDamage]")))
    {
        //--Resolve the highest damage type.
        int tHighestSlot = -1;
        float tHighestValue = 0.0f;
        for(int i = 0; i < ADVC_DAMAGE_TOTAL; i ++)
        {
            if(mEquipmentDamageTypes[ADITEM_DAMAGETYPE_OVERRIDE].mValueList[i] > tHighestValue)
            {
                tHighestSlot = i;
                tHighestValue = mEquipmentDamageTypes[ADITEM_DAMAGETYPE_OVERRIDE].mValueList[i];
            }
        }

        //--No highest damage, so this might not be a weapon. Discard.
        if(tHighestSlot == -1) return;

        //--Get the icon.
        StarBitmap *rIcon = GetStatIconBySlot(tHighestSlot + STATS_RESIST_START);

        //--Get the associated name.
        const char *rDamageString = GetDamageStringBySlot(tHighestSlot);

        //--Put the image index into a buffer.
        char tNumBuffer[256];
        int tImageIndex = pImageListing->GetListSize();
        sprintf(tNumBuffer, "[IMG%i] (%s)", tImageIndex, rDamageString);

        //--Add an element to the image storage list representing the attack icon.
        StarlightStringImagePack *nPack = (StarlightStringImagePack *)starmemoryalloc(sizeof(StarlightStringImagePack));
        nPack->Initialize();
        nPack->mYOffset = 3.0f;
        nPack->rImage = rIcon;
        pImageListing->AddElementAsTail("X", nPack, &FreeThis);

        //--Append.
        pBuffer->AppendStringWithoutNull(tNumBuffer);
    }
    ///--[TAB1]
    //--[TAB1] creates a moving breakpoint to align text.
    else if(!strncasecmp(pString, "[TAB1]", (int)strlen("[TAB1]")))
    {
        pBuffer->AppendStringWithoutNull(" [MOV150] ");
    }
    ///--[TAB2]
    //--[TAB2] creates a moving breakpoint to align text.
    else if(!strncasecmp(pString, "[TAB2]", (int)strlen("[TAB2]")))
    {
        pBuffer->AppendStringWithoutNull(" [MOV600] ");
    }
    ///--[Error]
    //--Error case. Print a warning.
    else
    {
        pBuffer->AppendStringWithoutNull("[Unhandled Tag]");
    }
}
char *AdventureItem::GetDisplayNameOfItemWithModCount()
{
    ///--[Documentation]
    //--Returns a heap-allocated string equivalent to the item's display name with a parenthesis
    //  indicator of how many gems are slotted into it. Ex: "Dragon Face (+3)" for a "Dragon Face" with 3 gems.
    //--The caller is responsible for deallocating the string. Cannot logically return a NULL.

    ///--[Execution]
    //--Get the base string. If there is no display name, use the internal name.
    const char *rBaseString = mDisplayName;
    if(!rBaseString) rBaseString = mLocalName;

    //--Count how many gems are present.
    int tGemCount = 0;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i] != NULL) tGemCount ++;
    }

    ///--[No Gems]
    //--Return a string that is just a copy of the display name.
    if(tGemCount < 1)
    {
        char *nString = InitializeString("%s", rBaseString);
        return nString;
    }

    ///--[Has Gems]
    //--Format!
    char *nString = InitializeString("%s (+%i)", rBaseString, tGemCount);
    return nString;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
AdventureItem *AdventureItem::GetGemInSlot(int pSlot)
{
    //--Note: It is legal to receive NULL back on error, OR empty slot. Attempting to get a gem
    //  past the allowable gem slot always returns NULL since it's logically impossible to cram
    //  a gem in there.
    if(pSlot < 0 || pSlot >= ADITEM_MAX_GEMS) return NULL;
    return mGemSlots[pSlot];
}

///===================================== Static Functions =========================================
StarBitmap *AdventureItem::GetStatIconBySlot(int pSlot)
{
    ///--[Documentation]
    //--Given a statistic's index, returns the icon associated with it. This is routed through
    //  the datalibrary. Returns NULL if out of range or not found.
    if(pSlot < 0 || pSlot >= STATS_TOTAL) return NULL;

    //--Static list. Gets built the first time this is called.
    if(!xHasBuiltStatIcoList) InitializeStatIcoList();

    //--Return the requested value.
    return xStaticStatIcoList[pSlot];
}
const char *AdventureItem::GetDamageStringBySlot(int pSlot)
{
    ///--[Documentation]
    //--Given a damage type index, returns a string used to represent it. Returns NULL on error.
    if(pSlot < 0 || pSlot >= ADVC_DAMAGE_TOTAL) return NULL;

    //--Return string from package.
    return (const char *)xrStringData->mem.mPtr[ADVITEM_STRINGS_TYPE_START + pSlot];
}

///======================================== Lua Hooking ===========================================
void AdventureItem::HookToLuaState(lua_State *pLuaState)
{
    /* //--[System]
       AdItem_GetProperty("Name") (1 String)
       AdItem_GetProperty("Quantity") (1 Integer)

       //--[Equipment Properties]
       AdItem_GetProperty("Damage Type", iCategory, iSlot) (1 Float)
       AdItem_GetProperty("Attack Animation") (1 String)
       AdItem_GetProperty("Attack Sound") (1 String)
       AdItem_GetProperty("Critical Sound") (1 String)
       AdItem_GetProperty("Ability Path") (1 String)

       //--[Gem Properties]
       AdItem_GetProperty("Is Gem") (1 Boolean)
       AdItem_GetProperty("Base Gem Name") (1 String)
       AdItem_GetProperty("Gem Colors") (1 Integer)
       AdItem_GetProperty("Subgem Name", iSlot) (1 String)
       Returns the requested property from the active AdventureItem. */
    lua_register(pLuaState, "AdItem_GetProperty", &Hook_AdItem_GetProperty);

    /* //--[Static]
       AdItem_SetProperty("Init Stat Icon List") (Static)
       AdItem_SetProperty("Init Damage Name List") (Static)
       AdItem_SetProperty("Set Stat Icon", iIndex, sDLPath) (Static)
       AdItem_SetProperty("Set Damage Name", iIndex, sName) (Static)

       //--[General]
       AdItem_SetProperty("Name", sName)
       AdItem_SetProperty("Is Adamantite")
       AdItem_SetProperty("Description", sDescription)
       AdItem_SetProperty("Is Consumable", bFlag)
       AdItem_SetProperty("Is Gem", uiGemColor)
       AdItem_SetProperty("Is Key Item", bFlag)
       AdItem_SetProperty("Is Unique", bFlag)
       AdItem_SetProperty("Is Equipment", bFlag)
       AdItem_SetProperty("Rendering Image", sDLPath)

       //--[Equippable]
       AdItem_SetProperty("Add Equippable Character", sName)
       AdItem_SetProperty("Add Equippable Slot", sSlotName)
       AdItem_SetProperty("Gem Slots", iTotal)
       AdItem_SetProperty("Equipment Attack Animation", sAnimation)
       AdItem_SetProperty("Equipment Attack Sound", sSound)
       AdItem_SetProperty("Statistic", iSlot, iValue)
       AdItem_SetProperty("Damage Type", iCategory, iSlot, fValue)

       //--[Tags]
       AdItem_SetProperty("Add Tag", sTagName, iCount)
       AdItem_SetProperty("Remove Tag", sTagName, iCount)

       //--[Combat Item]
       AdItem_SetProperty("Create Action") (Pushes Activity Stack)

       //--[Gem]
       AdItem_SetProperty("Autogenerate Gem Description")

       Sets the requested property in the active AdventureItem. */
    lua_register(pLuaState, "AdItem_SetProperty", &Hook_AdItem_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AdItem_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdItem_GetProperty("Name") (1 String)
    //AdItem_GetProperty("Quantity") (1 Integer)
    //AdItem_GetProperty("Description") (1 String)

    //--[Equipment Properties]
    //AdItem_GetProperty("Gem Slots") (1 Integer)
    //AdItem_GetProperty("Has Gem In Slot", iSlot) (1 Boolean)
    //AdItem_GetProperty("Base Statistic", iSlot) (1 Integer)
    //AdItem_GetProperty("Final Statistic", iSlot) (1 Integer)
    //AdItem_GetProperty("Damage Type", iCategory, iSlot) (1 Float)
    //AdItem_GetProperty("Attack Animation") (1 String)
    //AdItem_GetProperty("Attack Sound") (1 String)
    //AdItem_GetProperty("Critical Sound") (1 String)
    //AdItem_GetProperty("Ability Path") (1 String)

    //--[Gem Properties]
    //AdItem_GetProperty("Is Gem") (1 Boolean)
    //AdItem_GetProperty("Base Gem Name") (1 String)
    //AdItem_GetProperty("Gem Colors") (1 Integer)
    //AdItem_GetProperty("Subgem Name", iSlot) (1 String)

    //--[Tags]
    //AdItem_GetProperty("Get Tag Count", sTagName) (1 Integer)
    //AdItem_GetProperty("Get Tag Count Partial", sTagName, iLetters) (1 Integer)

    ///--[Argument Check]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdItem_GetProperty");

    //--Active object.
    AdventureItem *rItem = (AdventureItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || !rItem->IsOfType(POINTER_TYPE_ADVENTUREITEM)) return LuaTypeError("AdItem_GetProperty", L);

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[System]
    //--Name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetName());
        tReturns = 1;
    }
    //--Quantity.
    else if(!strcasecmp(rSwitchType, "Quantity") && tArgs == 1)
    {
        lua_pushinteger(L, rItem->GetStackSize());
        tReturns = 1;
    }
    //--Raw description.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetDescription());
        tReturns = 1;
    }
    ///--[Equipment Properties]
    //--Number of gem slots in the item.
    else if(!strcasecmp(rSwitchType, "Gem Slots") && tArgs >= 1)
    {
        lua_pushinteger(L, rItem->GetGemSlots());
        tReturns = 1;
    }
    //--Whether or not there is a gem in the given slot.
    else if(!strcasecmp(rSwitchType, "Has Gem In Slot") && tArgs == 2)
    {
        AdventureItem *rCheckGem = rItem->GetGemInSlot(lua_tointeger(L, 2));
        if(rCheckGem)
            lua_pushboolean(L, true);
        else
            lua_pushboolean(L, false);
        tReturns = 1;
    }
    //--Returns a statistic for the piece of equipment before gems are added.
    else if(!strcasecmp(rSwitchType, "Base Statistic") && tArgs == 2)
    {
        CombatStatistics *rStatPack = rItem->GetEquipStatistics();
        lua_pushinteger(L, rStatPack->GetStatByIndex(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Returns a statistic for the piece of equipment after gems are added.
    else if(!strcasecmp(rSwitchType, "Final Statistic") && tArgs == 2)
    {
        CombatStatistics *rStatPack = rItem->GetFinalStatistics();
        lua_pushinteger(L, rStatPack->GetStatByIndex(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Damage factor for the given type and category.
    else if(!strcasecmp(rSwitchType, "Damage Type") && tArgs == 3)
    {
        lua_pushnumber(L, rItem->GetDamageType(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    //--String representing the attack animation to use.
    else if(!strcasecmp(rSwitchType, "Attack Animation") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetEquipmentAttackAnimation());
        tReturns = 1;
    }
    //--String representing the attack sound to use.
    else if(!strcasecmp(rSwitchType, "Attack Sound") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetEquipmentAttackSound());
        tReturns = 1;
    }
    //--String representing the critical strike sound to use.
    else if(!strcasecmp(rSwitchType, "Critical Sound") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetEquipmentCriticalSound());
        tReturns = 1;
    }
    //--Path to the on-equip ability.
    else if(!strcasecmp(rSwitchType, "Ability Path") && tArgs == 1)
    {
        const char *rPath = rItem->GetAbilityPath();
        if(rPath)
            lua_pushstring(L, rPath);
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    ///--[Gem Properties]
    //--Returns true if the item is a gem, false if not.
    else if(!strcasecmp(rSwitchType, "Is Gem") && tArgs == 1)
    {
        lua_pushboolean(L, rItem->IsGem());
        tReturns = 1;
    }
    //--Returns the gem's original name before merging.
    else if(!strcasecmp(rSwitchType, "Base Gem Name") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetGemBaseName());
        tReturns = 1;
    }
    //--Returns the colors of the gem and all its subtypes.
    else if(!strcasecmp(rSwitchType, "Gem Colors") && tArgs == 1)
    {
        lua_pushinteger(L, rItem->GetGemColors());
        tReturns = 1;
    }
    //--Name of the gem slotted in the given slot. Can be used for equipment as well.
    else if(!strcasecmp(rSwitchType, "Subgem Name") && tArgs == 2)
    {
        AdventureItem *rSubgem = rItem->GetGemInSlot(lua_tointeger(L, 2));
        if(rSubgem)
            lua_pushstring(L, rSubgem->GetGemBaseName());
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    ///--[Tags]
    //--How many times the given tag appears on the item.
    else if(!strcasecmp(rSwitchType, "Get Tag Count") && tArgs == 2)
    {
        lua_pushinteger(L, rItem->GetTagCount(lua_tostring(L, 2)));
        tReturns = 1;
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AdItem_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AdItem_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AdItem_SetProperty("Init Stat Icon List") (Static)
    //AdItem_SetProperty("Init Damage Name List") (Static)
    //AdItem_SetProperty("Set Stat Icon", iIndex, sDLPath) (Static)
    //AdItem_SetProperty("Set Damage Name", iIndex, sName) (Static)

    //--[General]
    //AdItem_SetProperty("Name", sName)
    //AdItem_SetProperty("Display Name", sName)
    //AdItem_SetProperty("Display Name", sName, bDisallowTranslation)
    //AdItem_SetProperty("Name Lines", sTopName, sBotName)
    //AdItem_SetProperty("Is Adamantite")
    //AdItem_SetProperty("Is Consumable")
    //AdItem_SetProperty("Is Stackable")
    //AdItem_SetProperty("Description", sDescription)
    //AdItem_SetProperty("Type", sType)
    //AdItem_SetProperty("Process Description")
    //AdItem_SetProperty("Value", iValue)
    //AdItem_SetProperty("Is Consumable", bFlag)
    //AdItem_SetProperty("Is Gem", uiGemColor)
    //AdItem_SetProperty("Is Key Item", bFlag)
    //AdItem_SetProperty("Is Unique", bFlag)
    //AdItem_SetProperty("Rendering Image", sDLPath)

    //--[Equippable]
    //AdItem_SetProperty("Is Equipment", bFlag)
    //AdItem_SetProperty("Add Equippable Character", sName)
    //AdItem_SetProperty("Add Equippable Slot", sSlotName)
    //AdItem_SetProperty("Gem Slots", iTotal)
    //AdItem_SetProperty("Equipment Attack Animation", sAnimation)
    //AdItem_SetProperty("Equipment Attack Sound", sSound)
    //AdItem_SetProperty("Equipment Critical Sound", sSound)
    //AdItem_SetProperty("Statistic", iSlot, iValue)
    //AdItem_SetProperty("Damage Type", iCategory, iSlot, fValue)
    //AdItem_SetProperty("Push Gem In Slot", iSlot) (Pushes Activity Stack)

    //--[Tags]
    //AdItem_SetProperty("Add Tag", sTagName, iCount)
    //AdItem_SetProperty("Remove Tag", sTagName, iCount)

    //--[Combat Item]
    //AdItem_SetProperty("Ability Path", sPath)

    //--[Gem]
    //AdItem_SetProperty("Autogenerate Gem Description")

    ///--[Argument Check]
    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdItem_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Types]
    //--Initializes the static icon list. Refs must be in the DataLibrary already.
    if(!strcasecmp(rSwitchType, "Init Stat Icon List") && tArgs == 1)
    {
        AdventureItem::InitializeStatIcoList();
        return 0;
    }
    //--Overrides a static icon reference.
    else if(!strcasecmp(rSwitchType, "Set Stat Icon") && tArgs == 3)
    {
        AdventureItem::SetStaticImage(lua_tointeger(L, 2), lua_tostring(L, 3));
        return 0;
    }

    ///--[Dynamic Types]
    //--Active object.
    AdventureItem *rItem = (AdventureItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || !rItem->IsOfType(POINTER_TYPE_ADVENTUREITEM))
    {
        return LuaTypeError("AdItem_SetProperty", L);
    }

    ///--[General Properties]
    //--Internal/search name of the item.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rItem->SetName(lua_tostring(L, 2));
    }
    //--Overrides the display name.
    else if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rItem->SetDisplayName(lua_tostring(L, 2), false);
    }
    //--Overrides the display name, and can disallow translating that name. Use if the code already translates the display name.
    else if(!strcasecmp(rSwitchType, "Display Name") && tArgs >= 3)
    {
        rItem->SetDisplayName(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Causes the item to become Adamantite. Can only exist in shops. When purchased, becomes a crafting item.
    else if(!strcasecmp(rSwitchType, "Is Adamantite") && tArgs == 1)
    {
        rItem->SetAdamantite();
    }
    //--Causes the item to become consumable. Certain games allow usage of items in the inventory.
    else if(!strcasecmp(rSwitchType, "Is Consumable") && tArgs == 1)
    {
        rItem->SetConsumable();
    }
    //--Items can stack together.
    else if(!strcasecmp(rSwitchType, "Is Stackable") && tArgs == 1)
    {
        rItem->SetStackable(true);
    }
    //--Description of the item.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rItem->SetDescription(lua_tostring(L, 2));
    }
    //--Type. Only used for descriptions, does not affect properties or who can equip it.
    else if(!strcasecmp(rSwitchType, "Type") && tArgs == 2)
    {
        rItem->SetType(lua_tostring(L, 2));
    }
    //--Use tags to automatically modify the description.
    else if(!strcasecmp(rSwitchType, "Process Description") && tArgs == 1)
    {
        rItem->ProcessDescription();
    }
    //--How much the item is worth at shops.
    else if(!strcasecmp(rSwitchType, "Value") && tArgs == 2)
    {
        rItem->SetValue(lua_tointeger(L, 2));
    }
    //--Consumable, can be equipped in combat. Will always have at least one max charge.
    else if(!strcasecmp(rSwitchType, "Is Consumable") && tArgs == 2)
    {
        rItem->SetConsumable(lua_toboolean(L, 2));
    }
    //--Gem, item can be socketed and has a color. Can be merged based on its color.
    else if(!strcasecmp(rSwitchType, "Is Gem") && tArgs == 2)
    {
        rItem->SetIsGem(lua_tointeger(L, 2));
    }
    //--Key item. Cannot be discarded, does not preclude usage or equipping.
    else if(!strcasecmp(rSwitchType, "Is Key Item") && tArgs == 2)
    {
        rItem->SetKeyItem(lua_toboolean(L, 2));
    }
    //--Unique. Will not appear in shops if the player already has one.
    else if(!strcasecmp(rSwitchType, "Is Unique") && tArgs == 2)
    {
        rItem->SetUnique(lua_toboolean(L, 2));
    }
    //--Rendering image. Which icon to use when seeing this item on the UI.
    else if(!strcasecmp(rSwitchType, "Rendering Image") && tArgs == 2)
    {
        rItem->SetIconImageS(lua_tostring(L, 2));
    }
    ///--[Equipment Subsection]
    //--Marks this item as a piece of equipment which is equipped in a slot to provide stat changes.
    else if(!strcasecmp(rSwitchType, "Is Equipment") && tArgs == 2)
    {
        rItem->SetEquipmentFlag(lua_toboolean(L, 2));
    }
    //--Adds a character who can equip the item.
    else if(!strcasecmp(rSwitchType, "Add Equippable Character") && tArgs == 2)
    {
        rItem->AddEquippableCharacter(lua_tostring(L, 2));
    }
    //--Adds a slot the item can be equipped in.
    else if(!strcasecmp(rSwitchType, "Add Equippable Slot") && tArgs == 2)
    {
        rItem->AddEquippableSlot(lua_tostring(L, 2));
    }
    //--Sets how many gems this item can hold.
    else if(!strcasecmp(rSwitchType, "Gem Slots") && tArgs == 2)
    {
        rItem->SetGemCap(lua_tointeger(L, 2));
    }
    //--Which animation the weapon plays when used to attack in combat.
    else if(!strcasecmp(rSwitchType, "Equipment Attack Animation") && tArgs == 2)
    {
        rItem->SetEquipmentAttackAnimation(lua_tostring(L, 2));
    }
    //--Which sound the weapon plays when used to attack in combat.
    else if(!strcasecmp(rSwitchType, "Equipment Attack Sound") && tArgs == 2)
    {
        rItem->SetEquipmentAttackSound(lua_tostring(L, 2));
    }
    //--Which sound the weapon plays when it strikes critically.
    else if(!strcasecmp(rSwitchType, "Equipment Critical Sound") && tArgs == 2)
    {
        rItem->SetEquipmentCriticalSound(lua_tostring(L, 2));
    }
    //--Sets a statistic by its index, used for comparison and combat stats when equipped.
    else if(!strcasecmp(rSwitchType, "Statistic") && tArgs == 3)
    {
        rItem->SetStatistic(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets a damage type. Only used by weapons.
    else if(!strcasecmp(rSwitchType, "Damage Type") && tArgs == 4)
    {
        rItem->SetDamageType(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tonumber(L, 4));
    }
    //--Pushes the gem in the given slot, or NULL.
    else if(!strcasecmp(rSwitchType, "Push Gem In Slot") && tArgs == 2)
    {
        DataLibrary::Fetch()->PushActiveEntity(rItem->GetGemInSlot(lua_tointeger(L, 2)));
    }
    ///--[Tags]
    //--Adds the given tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Add Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rItem->AddTag(lua_tostring(L, 2));
    }
    //--Removes the given tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Remove Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rItem->RemoveTag(lua_tostring(L, 2));
    }
    ///--[Combat Item]
    //--Sets the path to the ability for this item.
    else if(!strcasecmp(rSwitchType, "Ability Path") && tArgs == 2)
    {
        rItem->SetAbilityPath(lua_tostring(L, 2));
    }
    ///--[Gems]
    //--Causes hybrid gems to generate their hybrid descriptions.
    else if(!strcasecmp(rSwitchType, "Autogenerate Gem Description") && tArgs == 1)
    {
        rItem->AutogenerateGemDescription();
    }
    ///--[Misc]
    //--Error.
    else
    {
        LuaPropertyError("AdItem_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
