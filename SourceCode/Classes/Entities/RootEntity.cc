//--Base
#include "RootEntity.h"

//--Classes
//--CoreClasses
#include "DataList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

///========================================== System ==============================================
RootEntity::RootEntity()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ROOTENTITY;

    //--[RootEntity]
    //--System
    //--Nameable
    mLocalName = InitializeString("Unnamed Entity");

    //--Renderable
    mDepth = 0.0f;

    //--Data Storage
    mDataList = new DataList();
    mDataList->SetStrictFlag(true);

    //--Public Variables
    mSelfDestruct = false;
}
RootEntity::~RootEntity()
{
    free(mLocalName);
    delete mDataList;
}

///===================================== Property Queries =========================================
const char *RootEntity::GetName()
{
    return mLocalName;
}
float RootEntity::GetDepth()
{
    return mDepth;
}
bool RootEntity::HasHandledTurn()
{
    return true;
}

///======================================== Manipulators ==========================================
void RootEntity::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void RootEntity::SetDepth(float pValue)
{
    mDepth = pValue;
}

///======================================== Core Methods ==========================================
void RootEntity::RespondToSelfDestruct()
{
    //--Overridable call, used right before the entity self-destructs for any reason.  Default does
    //  nothing, but can be overridden by daughter classes.
}

///=========================================== Update =============================================
void RootEntity::Update()
{
    //--Function called by the EntityManager every tick, the class should update itself here.
}
void RootEntity::PostUpdate()
{
    //--In some programs, an entity may run another update after all other entities (including itself)
    //  have had a chance to update. This is usually used for 'spawner' type entities.
}
void RootEntity::HandleTurnUpdate()
{
    //--Called by the EntityManager during turn handling. The base class does nothing.
}
void RootEntity::HandleEndOfTurn()
{
    //--Called by the EntityManager after all entities have acted during a turn. Base class does nothing.
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
DataList *RootEntity::GetDataList()
{
    return mDataList;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void RootEntity::HookToLuaState(lua_State *pLuaState)
{
    /* RE_GetID()
       Synonym of RO_GetID(). */
    lua_register(pLuaState, "RE_GetID", &Hook_RO_GetID);

    /* RE_SetDestruct(bFlag)
       Sets the self-destruct flag on the active entity. */
    lua_register(pLuaState, "RE_SetDestruct", &Hook_RE_SetDestruct);

    /* RE_GetDestruct() (1 boolean)
       Returns whether or not the RootEntity is flagged for self-destruct. */
    lua_register(pLuaState, "RE_GetDestruct", &Hook_RE_GetDestruct);

    /* RE_GetType()
       Returns the internal type code of the active entity. Synonym of RO_GetType().*/
    lua_register(pLuaState, "RE_GetType", &Hook_RO_GetType);

    /* RE_DefineData(sName, "N", fValue)
       RE_DefineData(sName, "S", sString)
       Macro, pushes the RootEntity's DataList and then calls the DataList function of the same name.
       Auto-pops the DataList when done with it. */
    lua_register(pLuaState, "RE_DefineData", &Hook_RE_DefineData);

    /* RE_GetData(sName, "N")
       RE_GetData(sName, "S")
       Macro, pushes the RootEntity's DataList and then calls the DataList function of the same name.
       Auto-pops the DataList when done with it. */
    lua_register(pLuaState, "RE_GetData", &Hook_RE_GetData);

    /* RE_SetData(sName, "N", fValue)
       RE_SetData(sName, "S", sString)
       Macro, pushes the RootEntity's DataList and then calls the DataList function of the same name.
       Auto-pops the DataList when done with it. */
    lua_register(pLuaState, "RE_SetData", &Hook_RE_SetData);

    /* RE_SetDepth(fDepth)
       Sets the depth override.  Not all entities use it, but the ones that do will change how
       close/far they are to the camera, orthographically. In 3D programs, this may override the
       Z coordinate if that sort of rendering is used, or it may do nothing. */
    lua_register(pLuaState, "RE_SetDepth", &Hook_RE_SetDepth);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
#include "DataLibrary.h"
int Hook_RE_SetDestruct(lua_State *L)
{
    //RE_SetDestruct(bFlag)
    if(lua_gettop(L) != 1) return LuaArgError("RE_SetDestruct");

    RootEntity *rEntity = (RootEntity *)DataLibrary::Fetch()->rActiveObject;
    if(!rEntity) return 0;

    rEntity->mSelfDestruct = lua_toboolean(L, 1);

    return 0;
}
int Hook_RE_GetDestruct(lua_State *L)
{
    //RE_GetDestruct() (1 boolean)
    RootEntity *rEntity = (RootEntity *)DataLibrary::Fetch()->rActiveObject;
    if(!rEntity)
    {
        lua_pushboolean(L, false);
        return 1;
    }

    lua_pushboolean(L, rEntity->mSelfDestruct);
    return 1;
}
int Hook_RE_DefineData(lua_State *L)
{
    //--See DataList's version
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEntity *rEntity = (RootEntity *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Check the Entity
    if(!rEntity) return LuaTypeError("RE_DefineData", L);

    rDataLibrary->rActiveObject = rEntity->GetDataList();
        Hook_DataList_Define(L);
    rDataLibrary->PopActiveEntity();
    return 0;
}
int Hook_RE_GetData(lua_State *L)
{
    //--See DataList's version
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEntity *rEntity = (RootEntity *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Check the Entity
    if(!rEntity) return LuaTypeError("RE_GetData", L);

    rDataLibrary->rActiveObject = rEntity->GetDataList();
        Hook_DataList_GetData(L);
    rDataLibrary->PopActiveEntity();
    return 1;
}
int Hook_RE_SetData(lua_State *L)
{
    //--See DataList's version
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEntity *rEntity = (RootEntity *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Check the Entity
    if(!rEntity) return LuaTypeError("RE_SetData", L);

    rDataLibrary->rActiveObject = rEntity->GetDataList();
        Hook_DataList_SetData(L);
    rDataLibrary->PopActiveEntity();
    return 0;
}
int Hook_RE_SetDepth(lua_State *L)
{
    //RE_SetDepth(fDepth)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("RE_SetDepth", L);

    //--Check Type.  Fails silently.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid()) return 0;

    //--Get and apply.
    RootEntity *rEntity = (RootEntity *)rDataLibrary->rActiveObject;
    rEntity->SetDepth(lua_tonumber(L, 1));

    return 0;
}
