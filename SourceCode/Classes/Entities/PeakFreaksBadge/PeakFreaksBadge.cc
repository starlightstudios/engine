//--Base
#include "PeakFreaksBadge.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

///========================================== System ==============================================
PeakFreaksBadge::PeakFreaksBadge()
{
    ///--[ ==== PeakFreaksBadge ===== ]
    ///--[Timing]
    mTimer = 0;

    ///--[Position]
    mXPosition = 0.0f;
    mYPositionLag = 0.0f;

    ///--[Rendering]
    rMedallion = NULL;
    rBadge = NULL;
    rTitle = NULL;
}
PeakFreaksBadge::~PeakFreaksBadge()
{

}
void PeakFreaksBadge::DeleteThis(void *pPtr)
{
    PeakFreaksBadge *rBadge = (PeakFreaksBadge *)pPtr;
    delete rBadge;
}

///===================================== Property Queries =========================================
bool PeakFreaksBadge::IsComplete()
{
    return (mTimer > PFB_MAX_TICKS + PFB_LAG_TICKS);
}

///======================================= Manipulators ===========================================
void PeakFreaksBadge::SetXPosition(float pXPosition)
{
    mXPosition = pXPosition;
}
void PeakFreaksBadge::SetMedallion(SugarBitmap *pImage)
{
    rMedallion = pImage;
}
void PeakFreaksBadge::SetBadge(SugarBitmap *pImage)
{
    rBadge = pImage;
}
void PeakFreaksBadge::SetTitle(SugarBitmap *pImage)
{
    rTitle = pImage;
}

///======================================= Core Methods ===========================================
float PeakFreaksBadge::ComputeYPosition(float pTimer)
{
    //--Computes and returns the expected Y position based on the provided timer.
    if(pTimer < PFB_SEQ_COMP_A)
    {
        float tPercent = EasingFunction::QuadraticOut(pTimer, PFB_SEQ_COMP_A);
        return PFB_Y_MIDDLE * tPercent;
    }
    //--Hold in middle.
    else if(pTimer < PFB_SEQ_COMP_B)
    {
        return PFB_Y_MIDDLE;
    }
    //--Slide out.
    else if(pTimer < PFB_SEQ_COMP_C)
    {
        float tPercent = EasingFunction::QuadraticOut(pTimer-PFB_SEQ_COMP_B, PFB_SEQ_COMP_C-PFB_SEQ_COMP_B);
        return PFB_Y_MIDDLE + ((VIRTUAL_CANVAS_Y + 100 - PFB_Y_MIDDLE) * tPercent);
    }

    //--Hold offscreen.
    return VIRTUAL_CANVAS_Y + 100;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksBadge::Update()
{
    //--Advance timer.
    mTimer ++;

    //--Set positions.
    mYPosition = ComputeYPosition(mTimer);
    mYPositionLag = ComputeYPosition(mTimer - PFB_LAG_TICKS);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void PeakFreaksBadge::Render()
{
    //--The medallion renders behind everything else.
    if(rMedallion)
    {
        float cMedalX = rMedallion->GetTrueWidthSafe()  * 0.50f;
        float cMedalY = rMedallion->GetTrueHeightSafe() * 0.50f;
        rMedallion->Draw(mXPosition - cMedalX, mYPosition - cMedalY);
    }

    //--The badge renders slightly behind the medallion in Y position, but in front of it in Z position.
    if(rBadge)
    {
        float cBadgeX = rBadge->GetTrueWidthSafe()  * 0.50f;
        float cBadgeY = rBadge->GetTrueHeightSafe() * 0.50f;
        rBadge->Draw(mXPosition - cBadgeX, mYPositionLag - cBadgeY);
    }

    //--The title renders in front of everything, and reverses the Y position so it flies in from
    //  the bottom of the screen.
    if(rTitle)
    {
        float cTitleW = rTitle->GetTrueWidthSafe()  * 0.50f;
        float cTitleH = rTitle->GetTrueHeightSafe() * 0.50f;
        float cTitleY = VIRTUAL_CANVAS_Y - mYPosition;
        rTitle->Draw(mXPosition - cTitleW, cTitleY - cTitleH);
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
