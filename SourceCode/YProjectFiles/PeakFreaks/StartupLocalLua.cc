///--[ ============ Base ============ ]
#include "PeakFreaksLevel.h"
#include "PeakFreaksTitle.h"
#include "PeakFreaksLoadInterrupt.h"

///======================================= Registration ===========================================
void RegisterLocalLuaFunctions(lua_State *pLuaState)
{
    ///--[Documentation]
    //--Called after the LuaManager is created, registers every function in the program to its
    //  Lua state. Lua scripts should not execute before this.

    ///--[ ========== Classes =========== ]
    ///--[World]
    PeakFreaksLevel::HookToLuaState(pLuaState);
    PeakFreaksTitle::HookToLuaState(pLuaState);

    ///--[ ======== CoreClasses ========= ]
    PeakFreaksLoadInterrupt::HookToLuaState(pLuaState);
}
