//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureLevel.h"
#include "RootEntity.h"
#include "TilemapActor.h"

//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "EntityManager.h"

void AdventureMenu::ExecuteRest()
{
    ///--[Documentation]
    //--Causes the player's party to be restored to full, all enemies to respawn, and the destroyed enemy
    //  list to be cleared.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();

    ///--[Enemy Respawning]
    //--Clear all entities marked as enemies.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->SetToHeadAndReturn();
    while(rCheckEntity)
    {
        //--Entity must be of the right type.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            if(rActor->IsEnemy("Any"))
            {
                rEntityList->RemoveRandomPointerEntry();
            }
        }
        rCheckEntity = (RootEntity *)rEntityList->IncrementAndGetRandomPointerEntry();
    }

    //--Randomize the entity spawn seed.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rSpawnSeedPtr = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Enemies/Random/iRandomSpawnSeed");
    if(rSpawnSeedPtr)
    {
        rSpawnSeedPtr->mNumeric = (int)rand();
    }

    //--Clear, respawn.
    rActiveLevel->WipeDestroyedEnemies();
    rActiveLevel->SpawnEnemies();
    rActiveLevel->PulseIgnore(AdventureLevel::xLastIgnorePulse);

    ///--[Party Restore]
    //--Restore party.
    AdvCombat::Fetch()->FullRestoreParty();

    ///--[Inventory Restore]
    //--Inventory resets all charges.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    rInventory->SetDoctorBagCharges(rInventory->GetDoctorBagChargesMax());

    ///--[Variable Restore]
    //--Go through the DataLibrary's special section and reset the variables within to 0.
    StarLinkedList *rHeading = rDataLibrary->GetHeading("Root/Variables/Chapter1/RestReset/");
    if(rHeading)
    {
        //--Iterate.
        SysVar *rVariable = (SysVar *)rHeading->PushIterator();
        while(rVariable)
        {
            rVariable->mNumeric = 0.0f;
            rVariable = (SysVar *)rHeading->AutoIterate();
        }
    }

    //--Reset the random chest opened variable.
    SysVar *rVariable = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Chests/Random/iTotalGreensOpened");
    if(rVariable) rVariable->mNumeric = 0.0f;

    //--Go through the chests section of the DataLibrary. Reset any respawning chests.
    StarLinkedList *rChestsCatalogue = rDataLibrary->GetCatalogue("Root/Variables/Chests/");
    if(rChestsCatalogue)
    {
        StarLinkedList *rSubHeading = (StarLinkedList *)rChestsCatalogue->PushIterator();
        while(rSubHeading)
        {
            //--Iterate across the variables.
            SysVar *rVariable = (SysVar *)rSubHeading->PushIterator();
            while(rVariable)
            {
                //--A chest set to AL_CHEST_OPEN_NORMAL_PENDING or AL_CHEST_OPEN_RESPAWN is set back to AL_CHEST_CLOSED_RESPAWN,
                //  which will respawn the contents.
                if(rVariable->mNumeric == AL_CHEST_OPEN_NORMAL_PENDING || rVariable->mNumeric == AL_CHEST_OPEN_RESPAWN)
                {
                    rVariable->mNumeric = AL_CHEST_CLOSED_RESPAWN;
                }

                //--Next variable.
                rVariable = (SysVar *)rSubHeading->AutoIterate();
            }

            //--Next heading.
            rSubHeading = (StarLinkedList *)rChestsCatalogue->AutoIterate();
        }
    }
}
