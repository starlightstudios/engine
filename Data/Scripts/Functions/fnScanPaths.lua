-- |[ ====================================== fnScanPaths() ===================================== ]|
--Scans available paths, returns lowest index of a usable path. Returns 0 if no paths match.
-- The path checks for the existince of the launching file, but not the rest of the datafiles.
function fnScanPaths(psaPathList)
    
    --Deprecated Warning
    if(gbPrintDeprecatedCallWarnings == true) then
        io.write("Warning: Calling fnScanPaths() is deprecated. Call from: " .. LM_GetCallStack(0) .. "\n")
    end
    
    -- |[Argument Check]|
    if(psaPathList == nil) then return 0 end
    
    -- |[Scan]|
    for i = 1, #psaPathList, 1 do
        if(FS_Exists(psaPathList[i])) then
            return i
        end
    end
    
    --Not found.
    return 0
end