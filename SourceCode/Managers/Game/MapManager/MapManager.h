///========================================= MapManager ===========================================
//--Controls the Map, whatever type it may be, and does routing between the different types when
//  requests are made by entities.  Since entities cannot and should not handle the many types of
//  maps themselves, the generic Property Queries should be used instead of getting the map directly.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "IRenderable.h"
#include "Interfaces/IResettable.h"

///===================================== Local Structures =========================================
///--[MapCatalystPack]
//--Storage for a catalyst, used to track catalyst progress in the UI.
typedef struct MapCatalystPack
{
    char mChapterAssociated[32];
    char mLevelName[128];
    char mChestName[32];
    char mCatalystType[32];
}MapCatalystPack;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class MapManager : public IRenderable, public IResettable
{
    private:
    //--System
    //--Activity
    RootLevel *mActiveLevel;

    //--Menu Activity
    bool mHasPendingClose;
    StarLinkedList *mMenuStack;
    bool mIsShowingGallery;
    GalleryMenu *mGalleryMenu;

    //--Overlay
    bool mShowOverlay;
    bool mIsOverlayFadeIn;
    int mOverlayFadeTimer;
    StarlightColor mOverlayColor;

    //--Class Switching
    int mUseDialogueType;
    int mUseMenuType;
    int mUseCombatType;

    //--Global GUI Pieces
    AdvCombat *mGlobalAdvCombat;
    AdventureInventory *mAdventureInventory;
    WorldDialogue *mWorldDialogue;

    //--Shaking
    bool mIsShaking;
    bool mShakeHorizontal;
    float mShakeRadius;
    int mShakeTicksRemaining;
    int mShakeTicksInitial;

    //--Catalyst Storage
    StarLinkedList *mCatalystInfoList; //MapCatalystPack *, master

    public:
    //--System
    MapManager();
    ~MapManager();

    //--Public Variables
    static bool xOnlyOneGame;
    static bool xDontRenderExtras;
    static int xIncrementor;
    static bool xHasRenderedMenus;
    static bool xHasUpdatedMenus;
    static float xCandidateAlpha;
    char *mReloadAtEndOfTick;
    bool mBackToTitle;

    //--Property Queries
    char *GetMapName();
    virtual float GetDepth();
    bool IsMapReady();
    float GetScaleFactor();
    void GetShakeFactors(float &sX, float &sY);
    bool MenuStackHasContents();
    const char *IsCatalystRegistered(const char *pLevelName, const char *pChestName, const char *pContents);

    //--Manipulators
    void ReceiveLevel(RootLevel *pLevel);
    void SetScaleFactor(float pFactor);
    void SetOverlayColor(float pRed, float pGreen, float pBlue, float pAlpha);
    void BeginFade();
    void BeginShake(float pRadius, int pTicks);
    void PushMenuStack(FlexMenu *pMenu);
    void PopMenuStack();
    void ShowGallery();
    void HideGallery();
    void ChangeDialogueType(int pType);
    void ChangeWorldDialogueToWorldDialogue();
    void SetMenuType(int pType);
    void SetCombatType(int pType);
    void TemporarilySwitchWorldDialogue(WorldDialogue *pNewDialogue);
    void ClearCatalystList();
    void RegisterCatalyst(const char *mChapter, const char *mLevelName, const char *mChestName, const char *mContents);

    //--Core Methods
    void Reset(const char *pResetType);
    WorldDialogue *GenerateNewDialogue();
    AdventureMenu *GenerateNewMenu();
    AdvCombat *GenerateNewCombat();

    //--Clip Interface
    bool GetClipAt(int pXPos, int pYPos, int pZPos);
    bool GetClipAt(int pXPos, int pYPos);
    bool CanMoveToH(int pXPosition, int pTop, int pBot);
    bool CanMoveToV(int pYPosition, int pLft, int pRgt);
    bool IsTouchingGround(int pYPosition, int pLft, int pRgt);
    bool IsTouchingGround(ThreeDimensionReal pDimensions);
    bool IsLftBlocked(ThreeDimensionReal pDimensions);
    bool IsTopBlocked(ThreeDimensionReal pDimensions);
    bool IsRgtBlocked(ThreeDimensionReal pDimensions);
    bool IsBotBlocked(ThreeDimensionReal pDimensions);

    //--File I/O
    //--Update
    void Update();
    void UpdatePaused(uint8_t pPauseFlags);
    bool UpdateMenuStack(bool pPendingStackClose);

    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();
    void RenderMenuStack();

    //--Pointer Routing
    RootLevel *LiberateLevel();
    RootLevel *GetActiveLevel();
    FlexMenu *GetMenuStackHead();
    GalleryMenu *GetGalleryMenu();
    AdventureInventory *GetAdventureInventory();
    AdvCombat *GetAdventureCombat();
    WorldDialogue *GetWorldDialogue();
    StarLinkedList *GetCatalystListing();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);

    //--Static Functions
    static MapManager *Fetch();
};

//--System
int Hook_MapM_IssueReset(lua_State *L);
int Hook_MapM_SetOneGame(lua_State *L);
int Hook_MapM_SetOverlayColor(lua_State *L);
int Hook_MapM_Shake(lua_State *L);
int Hook_MapM_PushMenuStackHead(lua_State *L);
int Hook_MapM_BackToTitle(lua_State *L);
int Hook_MapM_ReplaceDialogueWithWorldDialogue(lua_State *L);
int Hook_MapM_ReplaceDialogueWithMonocerosDialogue(lua_State *L);
int Hook_MapM_ChangeMenuHandler(lua_State *L);
int Hook_MapM_ChangeCombatHandler(lua_State *L);
int Hook_MapM_ClearCatalystList(lua_State *L);
int Hook_MapM_RegisterCatalyst(lua_State *L);
