///===================================== StarPointerSeries ========================================
//--A StarPointerSeries is an ordered list of strings. Each string represents a DataLibrary path,
//  and an object can use the strings to resolve its image data during starting. This means that
//  the autoloader from StarLumps can compress images meant for the UI without them needing to be
//  manually loaded in the C++ code.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "StarLinkedList.h"

///===================================== Local Structures =========================================
///--[PointerSeriesEntry]
//--Individual entry on the master list.
typedef struct PointerSeriesEntry
{
    //--Members
    int mSubtype;
    char *mPath;

    //--Functions
    void Initialize()
    {
        mSubtype = 0;
        mPath = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        //--Cast and check.
        PointerSeriesEntry *rPtr = (PointerSeriesEntry *)pPtr;
        if(!rPtr) return;

        //--Deallocate.
        free(rPtr->mPath);
        free(rPtr);
    }
}PointerSeriesEntry;

///--[PurgeDatafile]
//--Stored on the xAllExtractOrders list, tracks which file a bitmap was loaded from.
typedef struct PurgeDatafile
{
    //--Members
    char *mInternalName;
    StarLinkedList *mEntryList;  //char *, master
    StarLinkedList *mrValidList; //char *, reference

    //--Methods
    void Initialize();
    static void DeleteThis(void *pPtr);
    bool IsNamePathRegistered(const char *pInfileName, const char *pPath);
    const char *IsPathInThisDatafile(const char *pPath);
    void SetName(const char *pName);
    void StoreInfileNamePath(const char *pInfileName, const char *pPath);
    void StoreValidInfileName(const char *pName);
    void ClearValidList();
    void WriteToFile(FILE *fOutfile);
}PurgeDatafile;

///===================================== Local Definitions ========================================
///--[Forward Declarations]
class StarLinkedList;

///--[Diagnostics]
//--If this is defined, then a StarLinkedList will be created that tracks all Dummy/ image registrations.
//  This list can be exported to create a clearing listing for a build file to trim unused images
//  and datafiles.
//#define SPS_TRACKS_DUMMIES

///--[Subtypes]
#define SPS_SUBTYPE_NONE 0
#define SPS_SUBTYPE_IMAGE 1
#define SPS_SUBTYPE_FONT 2
#define SPS_SUBTYPE_MAX 3

///========================================== Classes =============================================
class StarPointerSeries
{
    private:
    ///--[System]
    StarLinkedList *mMembers; //PointerSeriesEntry *, master

    ///--[Etc]

    protected:

    public:
    //--System
    StarPointerSeries();
    ~StarPointerSeries();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    #if defined SPS_TRACKS_DUMMIES
    static StarLinkedList *xAllExtractOrders; //PurgeDatafile *, master
    static StarLinkedList *xSafeRemovalList; //dummyptr
    #endif

    //--Property Queries
    //--Manipulators
    void RegisterEntry(int pSubtype, const char *pPath);

    //--Core Methods
    void Crossload(void *pPtr, int pEntriesExpected);
    void CrossloadDiagnostics();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static void RegisterExtractOrder(const char *pInfileName, const char *pDLPathName);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_StarPointerSeries_SetProperty(lua_State *L);

