-- |[ ================================= Save Back to Main Menu ================================= ]|
--Closes the existing menu and returns to the main menu. Also saves out the config file.
MapM_PushMenuStackHead()

-- |[Functions]|
--Default boolean resolver. Handy!
local fnHandleBoolean = function(sButtonName, sOptionName)
	
	FlexMenu_PushButton(sButtonName)
		local iCurrentMipmapFlag = FlexButton_GetProperty("Current Option")
		if(iCurrentMipmapFlag ~= nil and iCurrentMipmapFlag == 0) then
			OM_SetOption(sOptionName, true)
		elseif(iCurrentMipmapFlag ~= nil and iCurrentMipmapFlag == 1) then
			OM_SetOption(sOptionName, false)
		end
	DL_PopActiveObject()
	
end

-- |[ ===================================== Resolve Values ===================================== ]|
--Figure out which options the buttons were pointing to, and upload those values to the Options Manager.

--Animation flags.
fnHandleBoolean("Actors Animate Move", "ActorsAnimateMove")
fnHandleBoolean("Show Damage Animations", "ShowDamageAnimations")
fnHandleBoolean("Show 3D Damage Overlays", "AllowDamageOverlay")
fnHandleBoolean("Show 2D Underlays", "Show2DUnderlays")

--Radar indicators.
fnHandleBoolean("No Fog of War Overlay", "DisableDynamicFog")
fnHandleBoolean("No Fog of War for Actors", "Always Show Radar Indicators")
fnHandleBoolean("No Fog of War for Items", "No Fog of War for Items")

--Display flags.
fnHandleBoolean("Show HPs", "Show Entity HP")
fnHandleBoolean("Show WPs", "Show Entity WP")
fnHandleBoolean("Show Weapon Names", "Show Entity Weapon")
fnHandleBoolean("Show Icons", "Show Entity Icons")

--Map Zoom
FlexMenu_PushButton("Map Zoom")
	OM_SetOption("Map Zoom", FlexButton_GetProperty("Current Slider"))
DL_PopActiveObject()

-- |[ ======================================== Finish Up ======================================= ]|
-- |[Write]|
--Save the data to the hard drive.
OM_WriteConfigFiles()

-- |[Reset]|
--Clear the old menu, and return to the main menu.
FlexMenu_Clear()

--Run the setup script. This is elsewhere.
LM_ExecuteScript("Data/Scripts/MainMenu/000 PopulateMainMenu.lua")

--Clean Up.
DL_PopActiveObject()