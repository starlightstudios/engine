//--Base
#include "TranslationManager.h"

//--Classes
#include "TilemapActor.h"

///======================================= Registration ===========================================
void RegisterEngineStrings()
{
    ///--[Documentation]
    //--Called at startup, creates translation classes for all classes that have them and registers them
    //  to the TranslationManager. This will make them available to be modified by translations or
    //  by mods/derived games.
    TranslationManager *rTranslationManager = TranslationManager::Fetch();

    ///--[Classes]
    rTranslationManager->RegisterEntry("TilemapActor", new TA_Strings());
}
