//--Base
#include "WADFile.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--The Binary Mesh is a technique which works independent of the complexity of floor and ceiling polygons. The
//  mesh is created by cycling across the level and identifying zones of a fixed size. Each zone is subdivided if
//  a linedef bisects it until none do, at which point it can be rendered as a quad.

//--[Forward Declarations]
#define INDENT 0.500000f

//--[Entry Function]
StarLinkedList *WADFile::BuildBinaryMesh(StarLinkedList *pSectorPolyList)
{
    //--Entry point of the subroutine. Scans across the level and calls the recursive routines to build the mesh.
    //  Returns a list containing all the polygons in the mesh.
    if(mVerticesTotal < 1 || !mVertices || !pSectorPolyList || pSectorPolyList->GetListSize() < 1) return NULL;

    //--Figure out the bounding box of the level.
    TwoDimensionReal tLevelHitbox;
    tLevelHitbox.mLft = mVertices[0].mX;
    tLevelHitbox.mTop = mVertices[0].mY;
    tLevelHitbox.mRgt = mVertices[0].mX;
    tLevelHitbox.mBot = mVertices[0].mY;
    for(int i = 1; i < mVerticesTotal; i ++)
    {
        if(mVertices[i].mX < tLevelHitbox.mLft) tLevelHitbox.mLft = mVertices[i].mX;
        if(mVertices[i].mY < tLevelHitbox.mTop) tLevelHitbox.mTop = mVertices[i].mY;
        if(mVertices[i].mX > tLevelHitbox.mRgt) tLevelHitbox.mRgt = mVertices[i].mX;
        if(mVertices[i].mY > tLevelHitbox.mBot) tLevelHitbox.mBot = mVertices[i].mY;
    }

    //--Polygon storage.
    StarLinkedList *nPolygonList = new StarLinkedList(true);

    //--Sizing.
    float cBoxSize = 256.0f;
    if(true)
    {
        for(float mXPos = tLevelHitbox.mLft; mXPos < tLevelHitbox.mRgt; mXPos = mXPos + cBoxSize)
        {
            for(float mYPos = tLevelHitbox.mTop; mYPos < tLevelHitbox.mBot; mYPos = mYPos + cBoxSize)
            {
                //--Run the subdivide algorithm.
                Subdivide(mXPos, mYPos, mXPos + cBoxSize, mYPos + cBoxSize, nPolygonList, mGlobalTexturePolyList);
            }
        }
    }
    else if(false)
    {
        cBoxSize = 256.0f;
        for(float mXPos = 1472.0f; mXPos < 1472.0f + 640.0f; mXPos = mXPos + cBoxSize)
        {
            for(float mYPos = 3072.0f; mYPos < 3072.0f + 640.0f; mYPos = mYPos + cBoxSize)
            {
                //--Run the subdivide algorithm.
                Subdivide(mXPos, mYPos, mXPos + cBoxSize, mYPos + cBoxSize, nPolygonList, mGlobalTexturePolyList);
            }
        }
    }
    else
    {
        FindContainingFloorPoly(1788, 2236, mGlobalTexturePolyList, false);
    }

    //--Remove all polygons which have a NULL floor or ceiling texture.
    WAD_FloorPoly *rPoly = (WAD_FloorPoly *)nPolygonList->SetToHeadAndReturn();
    while(rPoly)
    {
        if(rPoly->rFloorTexture == NULL || rPoly->rCeilingTexture == NULL)
        {
            nPolygonList->RemoveRandomPointerEntry();
        }
        rPoly = (WAD_FloorPoly *)nPolygonList->IncrementAndGetRandomPointerEntry();
    }

    //--Store the polygon list.
    mFloorPolysTotal = nPolygonList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mFloorPolys = (WAD_FloorPoly *)starmemoryalloc(sizeof(WAD_FloorPoly) * mFloorPolysTotal);
    for(int i = 0; i < mFloorPolysTotal; i ++)
    {
        WAD_FloorPoly *rPoly = (WAD_FloorPoly *)nPolygonList->GetElementBySlot(i);
        mFloorPolys[i].Initialize();
        mFloorPolys[i].mAssociatedSector = rPoly->mAssociatedSector;
        mFloorPolys[i].mVertexesTotal = rPoly->mVertexesTotal;
        mFloorPolys[i].mFloorHeight = rPoly->mFloorHeight;
        mFloorPolys[i].mCeilingHeight = rPoly->mCeilingHeight;
        mFloorPolys[i].AllocVertices(mFloorPolys[i].mVertexesTotal);
        for(int p = 0; p < mFloorPolys[i].mVertexesTotal; p ++)
        {
            memcpy(&mFloorPolys[i].mVertices[p], &rPoly->mVertices[p], sizeof(UDMFVertex));
        }

        mFloorPolys[i].rFloorTexture = rPoly->rFloorTexture;
        mFloorPolys[i].rCeilingTexture = rPoly->rCeilingTexture;
    }

    //--Finish up.
    return nPolygonList;
}

//--[Main Worker]
#define SMALLEST 0.25f
void WADFile::Subdivide(float pLft, float pTop, float pRgt, float pBot, StarLinkedList *pPolygonList, StarLinkedList *pSectorPolyList)
{
    //--Given a zone of territory, checks if there are any linedefs in it.
    //--If no lines split this territory then we just find what sector it belongs to and create a new polygon.
    //--If one line splits it, we split it into two triangles and find their sectors.
    //--If more than one line splits it, then we recursively call the function again on a smaller area.
    if(pRgt - pLft < SMALLEST || pBot - pTop < SMALLEST) return;

    //--Storage.
    bool tSubdivideImmediately = false;
    StarLinkedList *tInterceptList = new StarLinkedList(true);

    //--Scan through the linedefs.
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        //--Get the two vertices associated. If either is invalid, ignore it.
        if(mLinedefs[i].mStartVertex < 0 || mLinedefs[i].mEndVertex < 0) continue;
        if(mLinedefs[i].mStartVertex >= mVerticesTotal || mLinedefs[i].mEndVertex >= mVerticesTotal) continue;

        //--Linedef cannot be two-sided and have both sectors be the same on both sides.
        if(mLinedefs[i].mSidedefL != -1 && mLinedefs[i].mSidedefR != -1)
        {
            int tSectorLeft = mSidedefs[mLinedefs[i].mSidedefL].mSector;
            int tSectorRight = mSidedefs[mLinedefs[i].mSidedefR].mSector;
            if(tSectorLeft == tSectorRight) continue;
        }

        //--We have a linedef that can legally hit our rectangle. First, check if they have a bounding box
        //  in common. This allows us to reject some linedefs immediately.
        //--Note that we use the exclusive version of IsCollision. We do not care about cases where a linedef
        //  touches the edge but doesn't actually bisect the polygon. Walls do, not floors.
        int s = mLinedefs[i].mStartVertex;
        int t = mLinedefs[i].mEndVertex;
        if(!IsCollision(pLft, pTop, pRgt, pBot, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY))
        {
            continue;
        }

        //--If a linedef is wholly within the area under consideration, a subdivide is needed immediately.
        if(IsPointWithin(mVertices[s].mX, mVertices[s].mY, pLft, pTop, pRgt, pBot) &&
           IsPointWithin(mVertices[t].mX, mVertices[t].mY, pLft, pTop, pRgt, pBot))
        {
            tSubdivideImmediately = true;
            break;
        }

        //--Beginning subdivision.
        DebugManager::PushPrint(false, "Subdividing checks linedef %i.\n", i);

        //--There is at least some overlap. Now we need to determine where that overlap is.
        bool tReject = true;
        float tInterceptXL, tInterceptYL;
        float tInterceptXT, tInterceptYT;
        float tInterceptXR, tInterceptYR;
        float tInterceptXB, tInterceptYB;

        //--Get the four edges.
        #define CNST 0.0f
        DebugManager::Print("Checking Left intersection\n");
        int tHitLft = GetLineIntersection(pLft, pTop-CNST, pLft, pBot+CNST, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptXL, &tInterceptYL);
        DebugManager::Print("Checking Top intersection\n");
        int tHitTop = GetLineIntersection(pLft-CNST, pTop, pRgt+CNST, pTop, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptXT, &tInterceptYT);
        DebugManager::Print("Checking Right intersection\n");
        int tHitRgt = GetLineIntersection(pRgt, pTop-CNST, pRgt, pBot+CNST, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptXR, &tInterceptYR);
        DebugManager::Print("Checking Bottom intersection\n");
        int tHitBot = GetLineIntersection(pLft-CNST, pBot, pRgt+CNST, pBot, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptXB, &tInterceptYB);

        //--[All Four]
        //--In the event of all four hits, the linedef is guaranteed to have bisected the polygon.
        DebugManager::Print("Impact results for %i - %i %i %i %i.\n", i, tHitLft, tHitTop, tHitRgt, tHitBot);
        if(tHitLft == 1 && tHitTop == 1 && tHitRgt == 1 && tHitBot == 1)
        {
            tReject = false;
            DebugManager::Print("Hit all four.\n");
        }
        //--[Hit Three]
        //--In the event of hitting all sides except the bottom, a bisection must occur.
        else if(tHitLft == 1 && tHitTop == 1 && tHitRgt == 1 && tHitBot == 0)
        {
            tReject = false;
            DebugManager::Print("Hit three, missed bottom.\n");
        }
        //--In the event of hitting all sides except the right, a bisection must occur.
        else if(tHitLft == 1 && tHitTop == 1 && tHitRgt == 0 && tHitBot == 1)
        {
            tReject = false;
            DebugManager::Print("Hit three, missed right.\n");
        }
        //--In the event of hitting all sides except the top, a bisection must occur.
        else if(tHitLft == 1 && tHitTop == 0 && tHitRgt == 1 && tHitBot == 1)
        {
            tReject = false;
            DebugManager::Print("Hit three, missed top.\n");
        }
        //--In the event of hitting all sides except the left, a bisection must occur.
        else if(tHitLft == 0 && tHitTop == 1 && tHitRgt == 1 && tHitBot == 1)
        {
            tReject = false;
            DebugManager::Print("Hit three, missed left.\n");
        }
        //--[Hit Left and One Other]
        //--In the event of hitting the left and top, but missing the right and bottom...
        else if(tHitLft == 1 && tHitTop == 1 && tHitRgt == 0 && tHitBot == 0)
        {
            //--If the hit is on the corner, that's a miss.
            if(tInterceptXL != pLft || tInterceptYL != pTop) tReject = false;
            DebugManager::Print("Hit left and top. %i.\n", tReject);
        }
        //--In the event of hitting the left and bottom, but missing the right and top...
        else if(tHitLft == 1 && tHitTop == 0 && tHitRgt == 0 && tHitBot == 1)
        {
            //--If the hit is on the corner, that's a miss.
            if(tInterceptXL != pLft || tInterceptYL != pBot) tReject = false;
            DebugManager::Print("Hit left and bottom. %i.\n", tReject);
        }
        //--In the event of hitting the left and right, but missing the bottom and top...
        else if(tHitLft == 1 && tHitTop == 0 && tHitRgt == 1 && tHitBot == 0)
        {
            tReject = false;
            DebugManager::Print("Hit left and right.\n");
        }
        //--[Hit Top and One Other]
        //--We hit the top and the right.
        else if(tHitLft == 0 && tHitTop == 1 && tHitRgt == 1 && tHitBot == 0)
        {
            //--If the hit is on the corner, that's a miss.
            if(tInterceptXT != pRgt || tInterceptYT != pTop) tReject = false;
            DebugManager::Print("Hit right and top. %i.\n", tReject);
        }
        //--Hit the top and bottom.
        else if(tHitLft == 0 && tHitTop == 1 && tHitRgt == 0 && tHitBot == 1)
        {
            tReject = false;
            DebugManager::Print("Hit top and bottom.\n");
        }
        //--[Hit Right and One Other]
        //--Logically guaranteed to be right and bottom.
        else if(tHitLft == 0 && tHitTop == 0 && tHitRgt == 1 && tHitBot == 1)
        {
            //--If the hit is on the corner, that's a miss.
            if(tInterceptXB != pRgt || tInterceptYB != pBot) tReject = false;
            DebugManager::Print("Hit right and bottom. %i.\n", tReject);
        }
        //--[Hit a Single Side]
        //--Left.
        else if(tHitLft == 1 && tHitTop == 0 && tHitRgt == 0 && tHitBot == 0)
        {
            //--Reject if and only if one of the vertices is exactly equal to the hit point.
            tReject = false;
            if(tInterceptXL == mVertices[s].mX && tInterceptYL == mVertices[s].mY) tReject = true;
            if(tInterceptXL == mVertices[t].mX && tInterceptYL == mVertices[t].mY) tReject = true;
            DebugManager::Print("Hit left only %i.\n", tReject);
        }
        //--Top.
        else if(tHitLft == 0 && tHitTop == 1 && tHitRgt == 0 && tHitBot == 0)
        {
            //--Reject if and only if one of the vertices is exactly equal to the hit point.
            tReject = false;
            if(tInterceptXT == mVertices[s].mX && tInterceptYT == mVertices[s].mY) tReject = true;
            if(tInterceptXT == mVertices[t].mX && tInterceptYT == mVertices[t].mY) tReject = true;
            DebugManager::Print("Hit top only %i.\n", tReject);
        }
        //--Right.
        else if(tHitLft == 0 && tHitTop == 0 && tHitRgt == 1 && tHitBot == 0)
        {
            //--Reject if and only if one of the vertices is exactly equal to the hit point.
            tReject = false;
            if(tInterceptXR == mVertices[s].mX && tInterceptYR == mVertices[s].mY) tReject = true;
            if(tInterceptXR == mVertices[t].mX && tInterceptYR == mVertices[t].mY) tReject = true;
            DebugManager::Print("Hit right only %i.\n", tReject);
        }
        //--Bottom.
        else if(tHitLft == 0 && tHitTop == 0 && tHitRgt == 0 && tHitBot == 1)
        {
            //--Reject if and only if one of the vertices is exactly equal to the hit point.
            tReject = false;
            if(tInterceptXB == mVertices[s].mX && tInterceptYB == mVertices[s].mY) tReject = true;
            if(tInterceptXB == mVertices[t].mX && tInterceptYB == mVertices[t].mY) tReject = true;
            DebugManager::Print("Hit bottom only %i.\n", tReject);
        }
        //--[No Hits]
        else
        {
            //--Always reject, duh.
            DebugManager::Print("Missed everything.\n");
        }

        //--Hit was legal, store it.
        if(!tReject)
        {
            DebugManager::Print("Linedef %i intercepts.\n", i);
            SetMemoryData(__FILE__, __LINE__);
            int *nInterceptPtr = (int *)starmemoryalloc(sizeof(int));
            *nInterceptPtr = i;
            tInterceptList->AddElementAsTail("X", nInterceptPtr, &FreeThis);
        }
        DebugManager::PopPrint("Printed impact results.\n");
    }

    //--This area has no linedefs splitting it. Mark it as a single sector.
    if(tInterceptList->GetListSize() == 0 && !tSubdivideImmediately)
    {
        //--Create a new polygon.
        WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft, pTop, pSectorPolyList);
        pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

        //--Positions.
        nNewPolygon->AllocVertices(4);

        //--Set.
        nNewPolygon->mVertices[0].mX = pLft;
        nNewPolygon->mVertices[0].mY = pTop;
        nNewPolygon->mVertices[1].mX = pRgt;
        nNewPolygon->mVertices[1].mY = pTop;
        nNewPolygon->mVertices[2].mX = pRgt;
        nNewPolygon->mVertices[2].mY = pBot;
        nNewPolygon->mVertices[3].mX = pLft;
        nNewPolygon->mVertices[3].mY = pBot;
    }
    //--The area has exactly one linedef splitting it. This can easily be marked into two sectors
    //  by using the linedef's intercept points. We are now interested in corners.
    else if(tInterceptList->GetListSize() == 1 && !tSubdivideImmediately)
    {
        //--We need to find out where the linedef intercepts. It is guaranteed to continue through the sector
        //  since a linedef must be connected to another linedef to be legal, it can't go "nowhere".
        int tActiveSlot = 0;
        float tInterceptsX[4];
        float tInterceptsY[4];

        //--Debug.
        DebugManager::PushPrint(false, "Got a single linedef interception. Processing.\n");

        //--Get vertices to shorten things.
        int *rZerothIntercept = (int *)tInterceptList->GetElementBySlot(0);
        int s = mLinedefs[*rZerothIntercept].mStartVertex;
        int t = mLinedefs[*rZerothIntercept].mEndVertex;

        //--Check the top line.
        if(GetLineIntersection(pLft - 1.0f, pTop, pRgt + 1.0f, pTop, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptsX[tActiveSlot], &tInterceptsY[tActiveSlot]) == 1)
        {
            //--Intercept must be on a legal part of the line, not the extension.
            if(tInterceptsX[tActiveSlot] >= pLft && tInterceptsX[tActiveSlot] <= pRgt)
            {
                tActiveSlot ++;
            }
        }
        //--Check the TL corner.
        else if((mVertices[s].mX == pLft && mVertices[s].mY == pTop) || (mVertices[t].mX == pLft && mVertices[t].mY == pTop))
        {
            tInterceptsX[tActiveSlot] = pLft;
            tInterceptsY[tActiveSlot] = pTop;
            tActiveSlot ++;
        }
        //--Check the TR corner.
        else if((mVertices[s].mX == pRgt && mVertices[s].mY == pTop) || (mVertices[t].mX == pRgt && mVertices[t].mY == pTop))
        {
            tInterceptsX[tActiveSlot] = pRgt;
            tInterceptsY[tActiveSlot] = pTop;
            tActiveSlot ++;
        }

        //--Check the bottom line.
        if(GetLineIntersection(pLft - 1.0f, pBot, pRgt + 1.0f, pBot, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptsX[tActiveSlot], &tInterceptsY[tActiveSlot]) == 1)
        {
            //--Intercept must be on a legal part of the line, not the extension.
            if(tInterceptsX[tActiveSlot] >= pLft && tInterceptsX[tActiveSlot] <= pRgt)
            {
                tActiveSlot ++;
            }
        }
        //--Check the BL corner.
        else if((mVertices[s].mX == pLft && mVertices[s].mY == pBot) || (mVertices[t].mX == pLft && mVertices[t].mY == pBot))
        {
            tInterceptsX[tActiveSlot] = pLft;
            tInterceptsY[tActiveSlot] = pBot;
            tActiveSlot ++;
        }
        //--Check the BR corner.
        else if((mVertices[s].mX == pRgt && mVertices[s].mY == pBot) || (mVertices[t].mX == pRgt && mVertices[t].mY == pBot))
        {
            tInterceptsX[tActiveSlot] = pRgt;
            tInterceptsY[tActiveSlot] = pBot;
            tActiveSlot ++;
        }

        //--Check the left line. Auto-fails if the top/bottom were the intercepts.
        if(GetLineIntersection(pLft, pTop - 1.0f, pLft, pBot + 1.0f, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptsX[tActiveSlot], &tInterceptsY[tActiveSlot]) == 1)
        {
            //--Intercept must be on a legal part of the line, not the extension.
            if(tInterceptsY[tActiveSlot] >= pTop && tInterceptsY[tActiveSlot] <= pBot)
            {
                tActiveSlot ++;
            }
        }
        //--Check the TL corner.
        else if((mVertices[s].mX == pLft && mVertices[s].mY == pTop) || (mVertices[t].mX == pLft && mVertices[t].mY == pTop))
        {
            tInterceptsX[tActiveSlot] = pLft;
            tInterceptsY[tActiveSlot] = pTop;
            tActiveSlot ++;
        }
        //--Check the BL corner.
        else if((mVertices[s].mX == pLft && mVertices[s].mY == pBot) || (mVertices[t].mX == pLft && mVertices[t].mY == pBot))
        {
            tInterceptsX[tActiveSlot] = pLft;
            tInterceptsY[tActiveSlot] = pBot;
            tActiveSlot ++;
        }

        //--Check the right line. Auto-fails if we already found two intercepts.
        if(GetLineIntersection(pRgt, pTop - 1.0f, pRgt, pBot + 1.0f, mVertices[s].mX, mVertices[s].mY, mVertices[t].mX, mVertices[t].mY, &tInterceptsX[tActiveSlot], &tInterceptsY[tActiveSlot]) == 1)
        {
            //--Hit must be along the line.
            if(tInterceptsY[tActiveSlot] >= pTop && tInterceptsY[tActiveSlot] <= pBot)
            {
                tActiveSlot ++;
            }
        }
        //--Check the TR corner.
        else if((mVertices[s].mX == pRgt && mVertices[s].mY == pTop) || (mVertices[t].mX == pRgt && mVertices[t].mY == pTop))
        {
            tInterceptsX[tActiveSlot] = pRgt;
            tInterceptsY[tActiveSlot] = pTop;
            tActiveSlot ++;
        }
        //--Check the BR corner.
        else if((mVertices[s].mX == pRgt && mVertices[s].mY == pBot) || (mVertices[t].mX == pRgt && mVertices[t].mY == pBot))
        {
            tInterceptsX[tActiveSlot] = pRgt;
            tInterceptsY[tActiveSlot] = pBot;
            tActiveSlot ++;
        }

        //--If we have 4 hits on a single linedef, it means those linedefs hit the corners.
        if(tActiveSlot == 4)
        {
            //--Top-left to Bottom-right case.
            DebugManager::Print("Got 4 slot case:\n");
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[0], (int)tInterceptsY[0]);
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[1], (int)tInterceptsY[1]);
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[2], (int)tInterceptsY[2]);
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[3], (int)tInterceptsY[3]);
            if(tInterceptsX[0] == pLft && tInterceptsY[0] == pTop && tInterceptsX[1] == pRgt && tInterceptsY[1] == pBot)
            {
                //--Bottomleft-half of the polygon.
                WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pBot - INDENT, pSectorPolyList);
                pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                //--Positions.
                nNewPolygon->AllocVertices(3);
                nNewPolygon->mVertices[0].mX = pLft;
                nNewPolygon->mVertices[0].mY = pTop;
                nNewPolygon->mVertices[1].mX = pRgt;
                nNewPolygon->mVertices[1].mY = pBot;
                nNewPolygon->mVertices[2].mX = pLft;
                nNewPolygon->mVertices[2].mY = pBot;

                //--Topright-half of the polygon.
                nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                //--Positions.
                nNewPolygon->AllocVertices(3);
                nNewPolygon->mVertices[0].mX = pRgt;
                nNewPolygon->mVertices[0].mY = pTop;
                nNewPolygon->mVertices[1].mX = pRgt;
                nNewPolygon->mVertices[1].mY = pBot;
                nNewPolygon->mVertices[2].mX = pLft;
                nNewPolygon->mVertices[2].mY = pTop;
            }
            //--Top-right to Bottom-left case.
            else if(tInterceptsX[0] == pRgt && tInterceptsY[0] == pTop && tInterceptsX[1] == pLft && tInterceptsY[1] == pBot)
            {
                //--Topleft-half of the polygon.
                WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                //--Positions.
                nNewPolygon->AllocVertices(3);
                nNewPolygon->mVertices[0].mX = pLft;
                nNewPolygon->mVertices[0].mY = pTop;
                nNewPolygon->mVertices[1].mX = pRgt;
                nNewPolygon->mVertices[1].mY = pTop;
                nNewPolygon->mVertices[2].mX = pLft;
                nNewPolygon->mVertices[2].mY = pBot;

                //--Bottomright-half of the polygon.
                nNewPolygon = BuildPolyFrom(pRgt - INDENT, pBot - INDENT, pSectorPolyList);
                pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                //--Positions.
                nNewPolygon->AllocVertices(3);
                nNewPolygon->mVertices[0].mX = pRgt;
                nNewPolygon->mVertices[0].mY = pTop;
                nNewPolygon->mVertices[1].mX = pRgt;
                nNewPolygon->mVertices[1].mY = pBot;
                nNewPolygon->mVertices[2].mX = pLft;
                nNewPolygon->mVertices[2].mY = pBot;
            }

            //--Remove from consideration.
            tActiveSlot = 0;
        }
        //--If we have 3 hits on a single linedef, it means that at least one hit a corner. This means both the top
        //  and bottom are guaranteed to be hit, and slot 2 will be either the left or right edge.
        else if(tActiveSlot == 3)
        {
            //--Debug.
            DebugManager::Print("Got 3 slot case:\n");
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[0], (int)tInterceptsY[0]);
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[1], (int)tInterceptsY[1]);
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[2], (int)tInterceptsY[2]);

            //--If the 0th node is the top node...
            if(tInterceptsY[0] == pTop)
            {
                //--If the 1th node is the bottom...
                if(tInterceptsY[1] == pBot)
                {
                    //--If the 2nd node is the left...
                    if(tInterceptsX[2] == pLft)
                    {
                        //--Check if we hit the topleft.
                        if(tInterceptsY[2] == pTop)
                        {
                            //--Debug.
                            DebugManager::Print("3 Slot: Hit top-left and bottom.\n");

                            //--Left-half of the polygon.
                            WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pBot - INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(3);
                            nNewPolygon->mVertices[0].mX = pLft;
                            nNewPolygon->mVertices[0].mY = pTop;
                            nNewPolygon->mVertices[1].mX = tInterceptsX[1];
                            nNewPolygon->mVertices[1].mY = tInterceptsY[1];
                            nNewPolygon->mVertices[2].mX = pLft;
                            nNewPolygon->mVertices[2].mY = pBot;

                            //--Right-half of the polygon.
                            nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(4);
                            nNewPolygon->mVertices[0].mX = pLft;
                            nNewPolygon->mVertices[0].mY = pTop;
                            nNewPolygon->mVertices[1].mX = pRgt;
                            nNewPolygon->mVertices[1].mY = pTop;
                            nNewPolygon->mVertices[2].mX = pRgt;
                            nNewPolygon->mVertices[2].mY = pBot;
                            nNewPolygon->mVertices[3].mX = tInterceptsX[1];
                            nNewPolygon->mVertices[3].mY = tInterceptsY[1];
                        }
                        //--Otherwise, we hit the bottom left and the top.
                        else if(tInterceptsY[2] == pBot)
                        {
                            //--Debug.
                            DebugManager::Print("3 Slot: Hit bottom-left and top.\n");

                            //--Left-half of the polygon.
                            WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(3);
                            nNewPolygon->mVertices[0].mX = pLft;
                            nNewPolygon->mVertices[0].mY = pTop;
                            nNewPolygon->mVertices[1].mX = tInterceptsX[0];
                            nNewPolygon->mVertices[1].mY = tInterceptsY[0];
                            nNewPolygon->mVertices[2].mX = pLft;
                            nNewPolygon->mVertices[2].mY = pBot;

                            //--Right-half of the polygon.
                            nNewPolygon = BuildPolyFrom(pRgt - INDENT, pBot - INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(4);
                            nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                            nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                            nNewPolygon->mVertices[1].mX = pRgt;
                            nNewPolygon->mVertices[1].mY = pTop;
                            nNewPolygon->mVertices[2].mX = pRgt;
                            nNewPolygon->mVertices[2].mY = pBot;
                            nNewPolygon->mVertices[3].mX = pLft;
                            nNewPolygon->mVertices[3].mY = pBot;
                        }
                    }
                    //--If the 2nd node is the right...
                    else if(tInterceptsX[2] == pRgt)
                    {
                        //--Check if we hit the topright.
                        if(tInterceptsY[2] == pTop)
                        {
                            //--Debug.
                            DebugManager::Print("3 Slot: Hit top-right and bottom.\n");

                            //--Left-half of the polygon.
                            WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pBot - INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(4);
                            nNewPolygon->mVertices[0].mX = pLft;
                            nNewPolygon->mVertices[0].mY = pTop;
                            nNewPolygon->mVertices[1].mX = pRgt;
                            nNewPolygon->mVertices[1].mY = pTop;
                            nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                            nNewPolygon->mVertices[2].mY = tInterceptsY[1];
                            nNewPolygon->mVertices[3].mX = pLft;
                            nNewPolygon->mVertices[3].mY = pBot;

                            //--Right-half of the polygon.
                            nNewPolygon = BuildPolyFrom(pRgt - INDENT, pBot - INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(3);
                            nNewPolygon->mVertices[0].mX = pRgt;
                            nNewPolygon->mVertices[0].mY = pTop;
                            nNewPolygon->mVertices[1].mX = pRgt;
                            nNewPolygon->mVertices[1].mY = pBot;
                            nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                            nNewPolygon->mVertices[2].mY = tInterceptsY[1];
                        }
                        //--Otherwise, we hit the bottomright and the top.
                        else if(tInterceptsY[2] == pBot)
                        {
                            //--Debug.
                            DebugManager::Print("3 Slot: Hit bottom-right and top.\n");

                            //--Left-half of the polygon.
                            WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pBot - INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(4);
                            nNewPolygon->mVertices[0].mX = pLft;
                            nNewPolygon->mVertices[0].mY = pTop;
                            nNewPolygon->mVertices[1].mX = tInterceptsX[0];
                            nNewPolygon->mVertices[1].mY = tInterceptsY[0];
                            nNewPolygon->mVertices[2].mX = pRgt;
                            nNewPolygon->mVertices[2].mY = pBot;
                            nNewPolygon->mVertices[3].mX = pLft;
                            nNewPolygon->mVertices[3].mY = pBot;

                            //--Right-half of the polygon.
                            nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                            //--Positions.
                            nNewPolygon->AllocVertices(3);
                            nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                            nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                            nNewPolygon->mVertices[1].mX = pRgt;
                            nNewPolygon->mVertices[1].mY = pTop;
                            nNewPolygon->mVertices[2].mX = pRgt;
                            nNewPolygon->mVertices[2].mY = pBot;
                        }
                    }
                }
                //--Otherwise, we hit the top, left, and right sides.
                else
                {
                    //--Did we hit the top-left corner?
                    if(tInterceptsX[0] == pLft)
                    {
                        //--Debug.
                        DebugManager::Print("3 Slot: Hit top-left and right.\n");

                        //--Left-half of the polygon.
                        WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pBot - INDENT, pSectorPolyList);
                        pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                        //--Positions.
                        nNewPolygon->AllocVertices(4);
                        nNewPolygon->mVertices[0].mX = pLft;
                        nNewPolygon->mVertices[0].mY = pTop;
                        nNewPolygon->mVertices[1].mX = tInterceptsX[2];
                        nNewPolygon->mVertices[1].mY = tInterceptsY[2];
                        nNewPolygon->mVertices[2].mX = pRgt;
                        nNewPolygon->mVertices[2].mY = pBot;
                        nNewPolygon->mVertices[3].mX = pLft;
                        nNewPolygon->mVertices[3].mY = pBot;

                        //--Right-half of the polygon.
                        nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                        pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                        //--Positions.
                        nNewPolygon->AllocVertices(3);
                        nNewPolygon->mVertices[0].mX = pLft;
                        nNewPolygon->mVertices[0].mY = pTop;
                        nNewPolygon->mVertices[1].mX = pRgt;
                        nNewPolygon->mVertices[1].mY = pTop;
                        nNewPolygon->mVertices[2].mX = tInterceptsX[2];
                        nNewPolygon->mVertices[2].mY = tInterceptsY[2];
                    }
                    //--We hit the top-right corner.
                    else
                    {
                        //--Debug.
                        DebugManager::Print("3 Slot: Hit top-right and left.\n");

                        //--Left-half of the polygon.
                        WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                        pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                        //--Positions.
                        nNewPolygon->AllocVertices(3);
                        nNewPolygon->mVertices[0].mX = pLft;
                        nNewPolygon->mVertices[0].mY = pTop;
                        nNewPolygon->mVertices[1].mX = pRgt;
                        nNewPolygon->mVertices[1].mY = pTop;
                        nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                        nNewPolygon->mVertices[2].mY = tInterceptsY[1];

                        //--Right-half of the polygon.
                        nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                        pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                        //--Positions.
                        nNewPolygon->AllocVertices(4);
                        nNewPolygon->mVertices[0].mX = pRgt;
                        nNewPolygon->mVertices[0].mY = pTop;
                        nNewPolygon->mVertices[1].mX = pRgt;
                        nNewPolygon->mVertices[1].mY = pBot;
                        nNewPolygon->mVertices[2].mX = pLft;
                        nNewPolygon->mVertices[2].mY = pBot;
                        nNewPolygon->mVertices[3].mX = tInterceptsX[1];
                        nNewPolygon->mVertices[3].mY = tInterceptsY[1];
                    }
                }
            }
            //--Otherwise, the 0th node has to be the bottom node. 1 will be left, 2 will be right.
            else if(tInterceptsY[0] == pBot)
            {
                //--Did we hit the bottom-left and the right?
                if(tInterceptsX[0] == pLft)
                {
                    //--Debug.
                    DebugManager::Print("3 Slot: Hit bottom-left and right.\n");

                    //--Left-half of the polygon.
                    WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(4);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pTop;
                    nNewPolygon->mVertices[1].mX = pRgt;
                    nNewPolygon->mVertices[1].mY = pTop;
                    nNewPolygon->mVertices[2].mX = tInterceptsX[2];
                    nNewPolygon->mVertices[2].mY = tInterceptsY[2];
                    nNewPolygon->mVertices[3].mX = pLft;
                    nNewPolygon->mVertices[3].mY = pBot;

                    //--Right-half of the polygon.
                    nNewPolygon = BuildPolyFrom(pRgt - INDENT, pBot - INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(3);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pBot;
                    nNewPolygon->mVertices[1].mX = tInterceptsX[2];
                    nNewPolygon->mVertices[1].mY = tInterceptsY[2];
                    nNewPolygon->mVertices[2].mX = pRgt;
                    nNewPolygon->mVertices[2].mY = pBot;
                }
                //--We hit the bottom-right corner.
                else
                {
                    //--Debug.
                    DebugManager::Print("3 Slot: Hit bottom-right and left.\n");

                    //--Left-half of the polygon.
                    WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pBot - INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(3);
                    nNewPolygon->mVertices[0].mX = pRgt;
                    nNewPolygon->mVertices[0].mY = pBot;
                    nNewPolygon->mVertices[1].mX = pLft;
                    nNewPolygon->mVertices[1].mY = pBot;
                    nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[2].mY = tInterceptsY[1];

                    //--Right-half of the polygon.
                    nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(4);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pTop;
                    nNewPolygon->mVertices[1].mX = pRgt;
                    nNewPolygon->mVertices[1].mY = pTop;
                    nNewPolygon->mVertices[2].mX = pRgt;
                    nNewPolygon->mVertices[2].mY = pBot;
                    nNewPolygon->mVertices[3].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[3].mY = tInterceptsY[1];
                }
            }

            //--Remove from consideration.
            tActiveSlot = 0;
        }

        //--If the active slot is somehow 1, then this polygon somehow had a linedef cut only one side. If that's the case, then
        //  it was in error. Behave as if there were no cuts.
        if(tActiveSlot == 1)
        {
            //--Debug.
            DebugManager::Print("Got 1 slot case:\n");
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[0], (int)tInterceptsY[0]);

            //--Create a new polygon.
            WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
            pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

            //--Positions.
            nNewPolygon->AllocVertices(4);
            nNewPolygon->mVertices[0].mX = pLft;
            nNewPolygon->mVertices[0].mY = pTop;
            nNewPolygon->mVertices[1].mX = pRgt;
            nNewPolygon->mVertices[1].mY = pTop;
            nNewPolygon->mVertices[2].mX = pRgt;
            nNewPolygon->mVertices[2].mY = pBot;
            nNewPolygon->mVertices[3].mX = pLft;
            nNewPolygon->mVertices[3].mY = pBot;
        }
        //--Otherwise, we now have two points which bisect the square. Convert to two quads.
        else if(tActiveSlot == 2)
        {
            //--Debug.
            DebugManager::Print("Got 2 slot case:\n");
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[0], (int)tInterceptsY[0]);
            DebugManager::Print(" %i %i\n", (int)tInterceptsX[1], (int)tInterceptsY[1]);

            //--Check if the two cleaves were the same point. If so, it hit a corner and we can ignore it.
            if(tInterceptsX[0] == tInterceptsX[1] && tInterceptsY[0] == tInterceptsY[1])
            {
                //--Create a new polygon.
                WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                //--Positions.
                nNewPolygon->AllocVertices(4);
                nNewPolygon->mVertices[0].mX = pLft;
                nNewPolygon->mVertices[0].mY = pTop;
                nNewPolygon->mVertices[1].mX = pRgt;
                nNewPolygon->mVertices[1].mY = pTop;
                nNewPolygon->mVertices[2].mX = pRgt;
                nNewPolygon->mVertices[2].mY = pBot;
                nNewPolygon->mVertices[3].mX = pLft;
                nNewPolygon->mVertices[3].mY = pBot;
            }
            //--First cleave was the top.
            else if(tInterceptsY[0] == pTop)
            {
                //--Second cleave was the bottom, each poly has 4 sides.
                if(tInterceptsY[1] == pBot)
                {
                    //--Left-half of the polygon.
                    WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(4);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pTop;
                    nNewPolygon->mVertices[1].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[1].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[2].mY = tInterceptsY[1];
                    nNewPolygon->mVertices[3].mX = pLft;
                    nNewPolygon->mVertices[3].mY = pBot;

                    //--Right-half of the polygon.
                    nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(4);
                    nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[1].mX = pRgt;
                    nNewPolygon->mVertices[1].mY = pTop;
                    nNewPolygon->mVertices[2].mX = pRgt;
                    nNewPolygon->mVertices[2].mY = pBot;
                    nNewPolygon->mVertices[3].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[3].mY = tInterceptsY[1];
                }
                //--Second cleave was the left. Left poly has 3 points, right poly has 5.
                else if(tInterceptsX[1] == pLft)
                {
                    //--Left-half of the polygon.
                    WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(3);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pTop;
                    nNewPolygon->mVertices[1].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[1].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[2].mY = tInterceptsY[1];

                    //--Right half of the polygon.
                    nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(5);
                    nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[1].mX = pRgt;
                    nNewPolygon->mVertices[1].mY = pTop;
                    nNewPolygon->mVertices[2].mX = pRgt;
                    nNewPolygon->mVertices[2].mY = pBot;
                    nNewPolygon->mVertices[3].mX = pLft;
                    nNewPolygon->mVertices[3].mY = pBot;
                    nNewPolygon->mVertices[4].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[4].mY = tInterceptsY[1];
                }
                //--Second cleave was the right. Left poly has 5 points, right poly has 3.
                else if(tInterceptsX[1] == pRgt)
                {
                    //--Left-half of the polygon.
                    WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(5);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pTop;
                    nNewPolygon->mVertices[1].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[1].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[2].mY = tInterceptsY[1];
                    nNewPolygon->mVertices[3].mX = pRgt;
                    nNewPolygon->mVertices[3].mY = pBot;
                    nNewPolygon->mVertices[4].mX = pLft;
                    nNewPolygon->mVertices[4].mY = pBot;

                    //--Right half of the polygon.
                    nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(3);
                    nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[1].mX = pRgt;
                    nNewPolygon->mVertices[1].mY = pTop;
                    nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[2].mY = tInterceptsY[1];
                }
            }
            //--First cleave was along the bottom.
            else if(tInterceptsY[0] == pBot)
            {
                //--Second cleave was along the left. Left poly has 3 sides, right has 5.
                if(tInterceptsX[1] == pLft)
                {
                    //--Left-half of the polygon.
                    WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pBot - INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(3);
                    nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[1].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[1].mY = tInterceptsY[1];
                    nNewPolygon->mVertices[2].mX = pLft;
                    nNewPolygon->mVertices[2].mY = pBot;

                    //--Right half of the polygon.
                    nNewPolygon = BuildPolyFrom(pRgt - INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(5);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pTop;
                    nNewPolygon->mVertices[1].mX = pRgt;
                    nNewPolygon->mVertices[1].mY = pTop;
                    nNewPolygon->mVertices[2].mX = pRgt;
                    nNewPolygon->mVertices[2].mY = pBot;
                    nNewPolygon->mVertices[3].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[3].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[4].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[4].mY = tInterceptsY[1];
                }
                //--Second cleave was along the right. Left poly has 5 sides, right has 3.
                else if(tInterceptsX[1] == pRgt)
                {
                    //--Left-half of the polygon.
                    WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(5);
                    nNewPolygon->mVertices[0].mX = pLft;
                    nNewPolygon->mVertices[0].mY = pTop;
                    nNewPolygon->mVertices[1].mX = pRgt;
                    nNewPolygon->mVertices[1].mY = pTop;
                    nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[2].mY = tInterceptsY[1];
                    nNewPolygon->mVertices[3].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[3].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[4].mX = pLft;
                    nNewPolygon->mVertices[4].mY = pBot;

                    //--Right half of the polygon.
                    nNewPolygon = BuildPolyFrom(pRgt - INDENT, pBot - INDENT, pSectorPolyList);
                    pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                    //--Positions.
                    nNewPolygon->AllocVertices(3);
                    nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                    nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                    nNewPolygon->mVertices[1].mX = tInterceptsX[1];
                    nNewPolygon->mVertices[1].mY = tInterceptsY[1];
                    nNewPolygon->mVertices[2].mX = pRgt;
                    nNewPolygon->mVertices[2].mY = pBot;
                }
            }
            //--Remaining case: Left-right cleave. Both polys will have 4 sides.
            else
            {
                //--North-half of the polygon.
                WAD_FloorPoly *nNewPolygon = BuildPolyFrom(pLft + INDENT, pTop + INDENT, pSectorPolyList);
                pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                //--Positions.
                nNewPolygon->AllocVertices(4);
                nNewPolygon->mVertices[0].mX = pLft;
                nNewPolygon->mVertices[0].mY = pTop;
                nNewPolygon->mVertices[1].mX = pRgt;
                nNewPolygon->mVertices[1].mY = pTop;
                nNewPolygon->mVertices[2].mX = tInterceptsX[1];
                nNewPolygon->mVertices[2].mY = tInterceptsY[1];
                nNewPolygon->mVertices[3].mX = tInterceptsX[0];
                nNewPolygon->mVertices[3].mY = tInterceptsY[0];

                //--South-half of the polygon.
                nNewPolygon = BuildPolyFrom(pRgt - INDENT, pBot - INDENT, pSectorPolyList);
                pPolygonList->AddElement("X", nNewPolygon, &WAD_FloorPoly::DeleteThis);

                //--Positions.
                nNewPolygon->AllocVertices(4);
                nNewPolygon->mVertices[0].mX = tInterceptsX[0];
                nNewPolygon->mVertices[0].mY = tInterceptsY[0];
                nNewPolygon->mVertices[1].mX = tInterceptsX[1];
                nNewPolygon->mVertices[1].mY = tInterceptsY[1];
                nNewPolygon->mVertices[2].mX = pRgt;
                nNewPolygon->mVertices[2].mY = pBot;
                nNewPolygon->mVertices[3].mX = pLft;
                nNewPolygon->mVertices[3].mY = pBot;
            }
        }

        DebugManager::PopPrint("Done.\n");
    }
    //--Two or more non-corner linedefs. Recurse.
    else if(tInterceptList->GetListSize() >= 2 || tSubdivideImmediately)
    {
        float tXMd = (pLft+pRgt)/2.0f;
        float tYMd = (pTop+pBot)/2.0f;
        //fprintf(stderr, "==> Calling new subdivide TL.\n");
        Subdivide(pLft, pTop, tXMd, tYMd, pPolygonList, pSectorPolyList);
        //fprintf(stderr, "==> Calling new subdivide TR.\n");
        Subdivide(tXMd, pTop, pRgt, tYMd, pPolygonList, pSectorPolyList);
        //fprintf(stderr, "==> Calling new subdivide BL.\n");
        Subdivide(pLft, tYMd, tXMd, pBot, pPolygonList, pSectorPolyList);
        //fprintf(stderr, "==> Calling new subdivide BR.\n");
        Subdivide(tXMd, tYMd, pRgt, pBot, pPolygonList, pSectorPolyList);
        //fprintf(stderr, "==> Done subdivide.\n");
    }

    //--Clean up.
    delete tInterceptList;
}
WAD_FloorPoly *WADFile::BuildPolyFrom(float pCheckX, float pCheckY, StarLinkedList *pSectorPolyList)
{
    //--Returns a new polygon based on the one provided. The node list is deliberately left NULL since we're presumably
    //  going to be inserting new points.
    //--This algorithm will search the given polygon list for the best polygon to clone texture data from.
    //--Cannot return NULL under any circumstances.
    SetMemoryData(__FILE__, __LINE__);
    WAD_FloorPoly *nNewPolygon = (WAD_FloorPoly *)starmemoryalloc(sizeof(WAD_FloorPoly));
    nNewPolygon->Initialize();

    //--Find the appropriate properties.
    WAD_FloorPoly *rContainingPoly = FindContainingFloorPoly(pCheckX, pCheckY, mGlobalTexturePolyList, false);
    if(rContainingPoly)
    {
        memcpy(nNewPolygon, rContainingPoly, sizeof(WAD_FloorPoly));
    }

    //--Positions. This needs to be nulled off, not freed, since we may have borrowed data without
    //  taking ownership of it.
    nNewPolygon->mVertexesTotal = 0;
    nNewPolygon->mVertices = NULL;
    nNewPolygon->mVertexColorsC = NULL;
    nNewPolygon->mVertexColorsF = NULL;

    //--All done.
    return nNewPolygon;
}
