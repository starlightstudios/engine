//--Base
#include "AdvUISkills.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== Update ==============================================
bool AdvUISkills::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ =============== Documentation ================ ]
    //--Handles timers, controls, and other update components for the UI object.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer,     0, mVisibilityTimerMax,                  true);
    StandardTimer(mPurchaseTimer,       0, ADVMENU_SKILLS_PURCHASE_TICKS,       (mMode == ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE));
    StandardTimer(mDetailsTimer,        0, ADVMENU_SKILLS_DETAILS_SWITCH_TICKS, (mIsShowingDetails));
    StandardTimer(mStringEntryTimer,    0, ADVMENU_SKILLS_STRING_VIS_TICKS,     (mMode == ADVMENU_SKILLS_MODE_PROFILES_NEW || mMode == ADVMENU_SKILLS_MODE_PROFILES_RENAME));
    StandardTimer(mTacticsGridTimer,    0, ADVMENU_SKILLS_TACTICS_VIS_TICKS,    (mIsUsingTacticsGrid));
    StandardTimer(mProfileConfirmTimer, 0, ADVMENU_SKILLS_VIS_TICKS,            (mIsProfileConfirm));
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks,                       (mIsShowingHelp));

    //--Arrow timer. Always runs.
    mArrowTimer ++;

    //--Character swapping timer.
    if(mIsSwappingCharacter)
    {
        if(mCharacterSwapTimer < ADVMENU_SKILLS_CHARACTER_SWAP_TICKS)
        {
            mCharacterSwapTimer ++;
        }
        else
        {
            mIsSwappingCharacter = false;
        }
    }

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[ ================= Sub-Modes ================== ]
    ///--[Submenu Update]
    bool tProfileHandledUpdate = UpdateProfilesMode(pCannotHandleUpdate || !mIsProfilesMode);

    ///--[Error Checking]
    //--If this menu is not the main object, stop the update here.
    //if(mCurrentMode != AM_MODE_SKILLS || pCannotHandleUpdate) return false;
    if(pCannotHandleUpdate) return false;

    //--If this timer is not zeroed, stop.
    if(mPurchaseTimer > 0 && mPurchaseTimer < ADVMENU_SKILLS_PURCHASE_TICKS)       return false;
    if(mDetailsTimer  > 0 && mDetailsTimer  < ADVMENU_SKILLS_DETAILS_SWITCH_TICKS) return false;

    //--In profiles mode, stop control input here.
    if(mIsProfilesMode) return true;
    if(tProfileHandledUpdate) return true;

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(mIsShowingHelp)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingHelp = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsShowingHelp = true;
        RefreshMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Sub-Updates]
    //--Details mode.
    if(mIsShowingDetails)
    {
        UpdateSkillsDetails();
    }
    //--Job selection:
    else if(mMode == ADVMENU_SKILLS_MODE_JOB_SELECT)
    {
        UpdateSkillsJobSelect();
    }
    //--Skill selection:
    else if(mMode == ADVMENU_SKILLS_MODE_SKILL_SELECT)
    {
        UpdateSkillsSkillSelect();
    }
    //--Skill query:
    else if(mMode == ADVMENU_SKILLS_MODE_SKILL_QUERY)
    {
        UpdateSkillsSkillQuery();
    }
    //--Placing skills:
    else if(mMode == ADVMENU_SKILLS_MODE_SKILL_REPLACE)
    {
        UpdateSkillsPlacement();
    }
    //--Purchasing.
    else if(mMode == ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE)
    {
        UpdateSkillsPurchase();
    }

    //--Handled update.
    return true;
}
void AdvUISkills::UpdateBackground()
{
    ///--[Documentation]
    //--Runs timers when this is not the active object.
    StandardTimer(mVisibilityTimer, 0, ADVMENU_SKILLS_VIS_TICKS, false);
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
}
void AdvUISkills::UpdateSkillsJobSelect()
{
    ///--[Documentation]
    //--Player is selecting a job.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get active character and job list.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;
    StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();
    if(!rJobList) return;

    ///--[Toggle Descriptions]
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowAdvancedDescriptions = !mShowAdvancedDescriptions;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Profiles Mode]
    if(rControlManager->IsFirstPress("F4"))
    {
        SetToProfilesMode();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Toggle Tactics]
    if(rControlManager->IsFirstPress("Jump"))
    {
        mIsUsingTacticsGrid = !mIsUsingTacticsGrid;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Cursor Handling]
    if(AutoListUpDn(mJobCursor, mJobSkip, rJobList->GetListSize(), cxScrollbarJobsScrollBuf, cxScrollbarJobsPerPage, true))
    {
        RecomputeCursorPositions();
        return;
    }

    ///--[Switch Characters]
    //--Decrement the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Left")))
    {
        //--Does nothing if there is exactly one party member.
        if(AdvCombat::Fetch()->GetActivePartyCount() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }
        //--Change it. Disables other controls while scrolling.
        else
        {
            //--Set.
            mIsSwappingCharacter = true;
            mCharacterSwapTimer = 0;
            mPrevCharacterSlot = mCharacterCursor;
            mCharacterCursor --;

            //--Reset flags.
            mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mJobCursor = 0;
            mSkillCursor = 0;

            //--Wrap.
            if(mCharacterCursor < 0)
            {
                mCharacterCursor = AdvCombat::Fetch()->GetActivePartyCount() - 1;
            }

            //--Recompute cursor.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }
    }
    if((rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Right")))
    {
        //--Does nothing if there is exactly one party member.
        if(AdvCombat::Fetch()->GetActivePartyCount() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }
        //--Change it. Disables other controls while scrolling.
        else
        {
            //--Set.
            mIsSwappingCharacter = true;
            mCharacterSwapTimer = 0;
            mPrevCharacterSlot = mCharacterCursor;
            mCharacterCursor ++;

            //--Reset flags.
            mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mJobCursor = 0;
            mSkillCursor = 0;

            //--Wrap.
            if(mCharacterCursor >= AdvCombat::Fetch()->GetActivePartyCount())
            {
                mCharacterCursor = 0;
            }

            //--Recompute cursor.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }
    }

    ///--[Switch to Ability Inspection]
    //--Press Run to switch to ability query mode.
    if(rControlManager->IsFirstPress("Run"))
    {
        //--Store previous mode.
        mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;

        //--Set new mode.
        mMode = ADVMENU_SKILLS_MODE_SKILL_QUERY;

        //--Recompute cursor.
        RecomputeCursorPositions();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Activate, Cancel]
    //--Activate. Selects the job, changes to skill select mode.
    if(rControlManager->IsFirstPress("Activate") || rControlManager->IsFirstPress("Right"))
    {
        //--Check if this job has at least one ability. If not, fail.
        AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
        if(!rHighlightedJob) return;
        StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();
        if(rAbilityList->GetListSize() < 1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Change mode.
        mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
        mSkillCursor = 0;
        mSkillSkip = 0;

        //--Store the old cursor.
        memcpy(&mJobHighlightPos,  &mHighlightPos,  sizeof(EasingPack2D));
        memcpy(&mJobHighlightSize, &mHighlightSize, sizeof(EasingPack2D));

        //--Recompute.
        RecomputeCursorPositions();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    //--Cancel. Exits the skills menu.
    if(rControlManager->IsFirstPress("Cancel"))// && !mStartedHidingThisTick)
    {
        FlagExit();
        //SetToMainMenu(AM_MAIN_SKILLS);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvUISkills::UpdateSkillsSkillSelect()
{
    ///--[Documentation]
    //--Player has selected a job, and is now selecting a skill.
    ControlManager *rControlManager = ControlManager::Fetch();
    bool tRecomputeCursorPosition = false;

    //--Resolve selected character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Get the highlighted job.
    AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
    if(!rHighlightedJob) return;

    //--Get the ability list for that job.
    StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();

    ///--[Toggle Descriptions]
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowAdvancedDescriptions = !mShowAdvancedDescriptions;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Profiles Mode]
    if(rControlManager->IsFirstPress("F4"))
    {
        SetToProfilesMode();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Toggle Tactics]
    if(rControlManager->IsFirstPress("Jump"))
    {
        mIsUsingTacticsGrid = !mIsUsingTacticsGrid;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Cursor Handling]
    if(AutoListUpDn(mSkillCursor, mSkillSkip, rAbilityList->GetListSize(), cxScrollbarSkillsScrollBuf, cxScrollbarSkillsPerPage, true))
    {
        RecomputeCursorPositions();
    }
    /*
    //--Up. Decrements job cursor. Wraps.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursor(mSkillCursor, mSkillSkip, tIterations, rAbilityList->GetListSize(), ADVMENU_SKILLS_SKILL_SCROLL_RANGE, ADVMENU_SKILLS_MAX_SKILLS_PER_PAGE, true);
    }
    //--Down. Increments cursor. Wraps.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursor(mSkillCursor, mSkillSkip, tIterations, rAbilityList->GetListSize(), ADVMENU_SKILLS_SKILL_SCROLL_RANGE, ADVMENU_SKILLS_MAX_SKILLS_PER_PAGE, true);
    }*/

    ///--[Switch Characters]
    //--Decrement the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Left")))
    {
        //--Does nothing if there is exactly one party member.
        if(AdvCombat::Fetch()->GetActivePartyCount() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }
        //--Change it. Disables other controls while scrolling.
        else
        {
            //--Set.
            mIsSwappingCharacter = true;
            mCharacterSwapTimer = 0;
            mPrevCharacterSlot = mCharacterCursor;
            mCharacterCursor --;

            //--Reset flags.
            mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mJobCursor = 0;
            mSkillCursor = 0;

            //--Wrap.
            if(mCharacterCursor < 0)
            {
                mCharacterCursor = AdvCombat::Fetch()->GetActivePartyCount() - 1;
            }

            //--Recompute cursor.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }
    }
    if((rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Right")))
    {
        //--Does nothing if there is exactly one party member.
        if(AdvCombat::Fetch()->GetActivePartyCount() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }
        //--Change it. Disables other controls while scrolling.
        else
        {
            //--Set.
            mIsSwappingCharacter = true;
            mCharacterSwapTimer = 0;
            mPrevCharacterSlot = mCharacterCursor;
            mCharacterCursor ++;

            //--Reset flags.
            mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mJobCursor = 0;
            mSkillCursor = 0;

            //--Wrap.
            if(mCharacterCursor >= AdvCombat::Fetch()->GetActivePartyCount())
            {
                mCharacterCursor = 0;
            }

            //--Recompute cursor.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }
    }

    //--Cursor.
    if(tRecomputeCursorPosition)
    {
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Switch to Ability Inspection]
    //--Press Run to switch to ability query mode.
    if(rControlManager->IsFirstPress("Run"))
    {
        //--Store previous mode.
        mPrevMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;

        //--Set new mode.
        mMode = ADVMENU_SKILLS_MODE_SKILL_QUERY;

        //--Recompute cursor.
        RecomputeCursorPositions();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Activate, Cancel]
    //--Activate. Attempts to begin skill purchasing, or skill placement.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get the ability in question.
        AdvCombatAbility *rHighlightedAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillCursor);
        if(!rHighlightedAbility) return;

        //--Ability is already purchased. Begin placement.
        if(rHighlightedAbility->IsUnlocked())
        {
            //--Ability is not equippable.
            if(!rHighlightedAbility->CanBeEquipped())
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Mode.
            mMode = ADVMENU_SKILLS_MODE_SKILL_REPLACE;
            mIsUsingTacticsGrid = false;

            //--Cursor.
            SetAbilityCursorToFirstEmptySlot();
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Ability needs to be purchased.
        else
        {
            //--Get the JP cost.
            int tJPCost = rHighlightedAbility->GetJPCost();

            //--If the JP cost is -1, that means the ability must be unlocked via scripts.
            if(tJPCost == -1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Player has enough JP?
            else if(tJPCost <= rCurrentCharacter->GetGlobalJP() + rHighlightedJob->GetCurrentJP())
            {
                mMode = ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Not enough JP.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
        return;
    }

    //--Cancel. Exits the skills menu.
    if((rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Left")))// && !mStartedHidingThisTick)
    {
        mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
        RecomputeCursorPositions();
        mSkillCursor = 0;
        mSkillSkip = 0;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvUISkills::UpdateSkillsSkillQuery()
{
    ///--[Documentation]
    //--Player is inspecting the currently equipped abilities.
    ControlManager *rControlManager = ControlManager::Fetch();
    bool tRecomputeCursorPosition = false;

    ///--[Toggle Descriptions]
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowAdvancedDescriptions = !mShowAdvancedDescriptions;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Profiles Mode]
    if(rControlManager->IsFirstPress("F4"))
    {
        SetToProfilesMode();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Toggle Tactics]
    if(rControlManager->IsFirstPress("Jump"))
    {
        mIsUsingTacticsGrid = !mIsUsingTacticsGrid;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Cursor Handling]
    //--Y scrolling always has the fixed maximum of 3.
    if(AutoListUpDnNoScroll(mAbilityCursorY, 3, true))
    {
        RecomputeCursorPositions();
    }

    //--X scrolling has a variable maximum.
    int tCursorMax = ACE_ABILITY_TACTICS_GRID_X0;
    if(mIsUsingTacticsGrid) tCursorMax = (ACE_ABILITY_TACTICS_GRID_X1 - ACE_ABILITY_TACTICS_GRID_X0) + 1;
    if(AutoListLfRtNoScroll(mAbilityCursorX, tCursorMax, true))
    {
        RecomputeCursorPositions();
    }

    /*
    //--Up. Decrements Y cursor. Wraps.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorY, tIterations, 3, true);
    }
    //--Down. Increments Y cursor. Wraps.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorY, tIterations, 3, true);
    }*/

    //--Left. Decrements X cursor. Wraps.
    /*if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Speed increase.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Resolve max cursor.
        int tCursorMax = ACE_ABILITY_TACTICS_GRID_X0;
        if(mIsUsingTacticsGrid) tCursorMax = (ACE_ABILITY_TACTICS_GRID_X1 - ACE_ABILITY_TACTICS_GRID_X0) + 1;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorX, tIterations, tCursorMax, true);
    }
    //--Right. Increments X cursor. Wraps.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Speed increase.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Resolve max cursor.
        int tCursorMax = ACE_ABILITY_TACTICS_GRID_X0;
        if(mIsUsingTacticsGrid) tCursorMax = (ACE_ABILITY_TACTICS_GRID_X1 - ACE_ABILITY_TACTICS_GRID_X0) + 1;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorX, tIterations, tCursorMax, true);
    }*/

    //--Cursor.
    if(tRecomputeCursorPosition)
    {
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Activate, Cancel]
    //--Activate clears the current slot.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Active character.
        AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
        if(!rCurrentCharacter) return;

        //--Must be an editable slot.
        if(!IsCustomizableSlot(mAbilityCursorX, mAbilityCursorY))
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Clear.
        rCurrentCharacter->SetAbilitySlot(mAbilityCursorX, mAbilityCursorY, "Null");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    //--Cancel. Reset to previous mode.
    if(rControlManager->IsFirstPress("Cancel"))// && !mStartedHidingThisTick)
    {
        mMode = mPrevMode;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvUISkills::UpdateSkillsPlacement()
{
    ///--[Documentation]
    //--Player has selected an ability and is placing it in the ability grid.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Cursor Handling]
    //--Y scrolling always has the fixed maximum of 3.
    if(AutoListUpDnNoScroll(mAbilityCursorY, 3, true))
    {
        RecomputeCursorPositions();
    }

    //--X scrolling has a variable maximum.
    int tCursorMax = ACE_ABILITY_TACTICS_GRID_X0;
    if(mIsUsingTacticsGrid) tCursorMax = (ACE_ABILITY_TACTICS_GRID_X1 - ACE_ABILITY_TACTICS_GRID_X0) + 1;
    if(AutoListLfRtNoScroll(mAbilityCursorX, tCursorMax, true))
    {
        RecomputeCursorPositions();
    }

    /*
    //--Up. Decrements Y cursor. Wraps.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorY, tIterations, 3, true);
    }
    //--Down. Increments Y cursor. Wraps.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorY, tIterations, 3, true);
    }

    //--Left. Decrements X cursor. Wraps.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Speed increase.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Resolve max cursor.
        int tCursorMax = ACE_ABILITY_TACTICS_GRID_X0;
        if(mIsUsingTacticsGrid) tCursorMax = (ACE_ABILITY_TACTICS_GRID_X1 - ACE_ABILITY_TACTICS_GRID_X0) + 1;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorX, tIterations, tCursorMax, true);
    }
    //--Right. Increments X cursor. Wraps.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Speed increase.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Resolve max cursor.
        int tCursorMax = ACE_ABILITY_TACTICS_GRID_X0;
        if(mIsUsingTacticsGrid) tCursorMax = (ACE_ABILITY_TACTICS_GRID_X1 - ACE_ABILITY_TACTICS_GRID_X0) + 1;

        //--Handler.
        tRecomputeCursorPosition = HandleCursorNoScroll(mAbilityCursorX, tIterations, tCursorMax, true);
    }

    //--Cursor.
    if(tRecomputeCursorPosition)
    {
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }*/

    ///--[Activate, Cancel]
    //--Activate. Place the skill.
    if(rControlManager->IsFirstPress("Activate"))
    {
        ///--[Resolve]
        //--Active character.
        AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
        if(!rCurrentCharacter) return;

        //--Get highlighted job.
        AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
        if(!rHighlightedJob) return;

        //--Ability List.
        StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();
        if(!rAbilityList) return;

        //--Make sure the ability exists.
        AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillCursor);
        if(!rAbility) return;

        ///--[Slot Check]
        //--If the slot was illegal, fail.
        if(!IsCustomizableSlot(mAbilityCursorX, mAbilityCursorY))
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Availability lookup tables.
        int tSlotsTotal = 15;
        int cSlotsX[] = {3, 4, 5, 6, 7, 3, 4, 5, 6, 7, 3, 4, 5, 6, 7};
        int cSlotsY[] = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2};

        //--Ability is already equipped somewhere:
        if(rCurrentCharacter->IsAbilityEquipped(rAbility))
        {
            //--Unequip the ability.
            for(int i = 0; i < tSlotsTotal; i ++)
            {
                AdvCombatAbility *rCheckAbility = rCurrentCharacter->GetAbilityBySlot(cSlotsX[i], cSlotsY[i]);
                if(rCheckAbility == rAbility)
                {
                    rCurrentCharacter->SetAbilitySlot(cSlotsX[i], cSlotsY[i], "Null");
                    break;
                }
            }
        }

        ///--[Equip]
        //--Get internal name.
        const char *rInternalName = rAbility->GetInternalName();
        rCurrentCharacter->SetAbilitySlot(mAbilityCursorX, mAbilityCursorY, rInternalName);

        //--Resync abilities with active profile.
        rCurrentCharacter->SaveCurrentAbilitiesToActiveProfile();

        //--Switch back to skill select mode.
        mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    //--Cancel. Return to skill selection.
    if(rControlManager->IsFirstPress("Cancel"))// && !mStartedHidingThisTick)
    {
        mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvUISkills::UpdateSkillsPurchase()
{
    ///--[Documentation]
    //--Player is opting to purchase an ability.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Activate, Cancel]
    //--Activate. Memorize the skill.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Active character.
        AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
        if(!rCurrentCharacter) return;

        //--Get highlighted job.
        AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
        if(!rHighlightedJob) return;

        //--Ability List.
        StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();
        if(!rAbilityList) return;

        //--Get ability.
        AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillCursor);
        if(!rAbility) return;

        //--Get variables.
        int tJPCost = rAbility->GetJPCost();
        int tJobJP = rHighlightedJob->GetCurrentJP();
        int tGlobalJP = rCurrentCharacter->GetGlobalJP();

        //--Spend JP out of the job's JP. If we have enough just for that, we're done.
        if(tJobJP >= tJPCost)
        {
            rHighlightedJob->SetCurrentJP(tJobJP - tJPCost);
        }
        //--Otherwise, spend the rest out of the global.
        else
        {
            int tDifference = tJPCost - tJobJP;
            rHighlightedJob->SetCurrentJP(0);
            rCurrentCharacter->SetGlobalJP(tGlobalJP - tDifference);
        }

        //--Mark the ability as purchased.
        rAbility->SetUnlocked(true);
        rHighlightedJob->RecheckMastery();

        //--Reset.
        mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
        RecomputeCursorPositions();

        //--Special: The ability may optionally have an attached script that runs when it is purchased. If so,
        //  run that here. It may change some or all of its properties. The code ACA_SCRIPT_CODE_UI_PURCHASED
        //  is passed alongside in case it is using the ability script.
        const char *rExecScript = rAbility->GetUnlockExecScript();
        if(rExecScript)
        {
            LuaManager::Fetch()->PushExecPop(rAbility, rExecScript, 1, "N", (float)ACA_SCRIPT_CODE_UI_PURCHASED);
        }

        //--Order the job script to run the "Master" code. This is to help check if the job should unlock its
        //  master skill.
        rHighlightedJob->CallScript(ADVCJOB_CODE_MASTER);

        //--Play the purchase sound.
        AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
    }

    //--Cancel. Reset to previous mode.
    if(rControlManager->IsFirstPress("Cancel"))// && !mStartedHidingThisTick)
    {
        mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvUISkills::UpdateSkillsDetails()
{
    ///--[Documentation]
    //--The skills details only really accepts pressing the cancel button to exit details mode. Sure
    //  is an exciting bit of code, though!
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsShowingDetails = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
