//--Base
#include "AdvUIInventory.h"

//--Classes
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIInventory::AdvUIInventory()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_ADVUIINVENTORY;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    mVisibilityTimer = 0;
    mVisibilityTimerMax = cxVisTicks;

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ===== AdvUIInventory ===== ]
    ///--[Constants]
    cItemsPerPage = 14;

    ///--[System]
    //--Cursor.
    mItemCursor = 0;
    mItemSkip = 0;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Deconstruction
    mStoredItemName = NULL;
    mIsDeconstructMode = false;
    mDeconstructTimer = 0;
    mDeconstructErrorEquipped = false;
    mDeconstructErrorNoResults = false;
    mDeconstructResults = new StarLinkedList(false);

    ///--[Instructions]
    //--Strings
    mShowHelpString = new StarlightString();
    mChangeSortString = new StarlightString();
    mDeconstructString = new StarlightString();
    mScrollFastString = new StarlightString();
    mConfirmString = new StarlightString();
    mCancelString = new StarlightString();
    AllocateHelpStrings(cxHelpStringsTotal);

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the AdvImages structure to the verify list.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIInventory::~AdvUIInventory()
{
    free(mStoredItemName);
    delete mDeconstructResults;
    delete mShowHelpString;
    delete mChangeSortString;
    delete mDeconstructString;
    delete mScrollFastString;
    delete mConfirmString;
    delete mCancelString;
}
void AdvUIInventory::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Inventory UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",      &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
bool AdvUIInventory::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    if(pType == POINTER_TYPE_ADVUIINVENTORY) return true;
    return false;
}

///======================================= Manipulators ===========================================
void AdvUIInventory::TakeForeground()
{
    ///--[Variables]
    //--Reset variables.
    mItemCursor = 0;
    mItemSkip = 0;

    //--Inventory commands.
    AdventureInventory::Fetch()->SortItemListByInternalCriteria();
    AdventureInventory::Fetch()->BuildExtendedItemList();

    ///--[Construction]
    //--Set cursor to 0th position.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--Normal cases.
    mShowHelpString->   AutoSetControlString("[IMG0] Show Help",         1, "F1");
    mScrollFastString-> AutoSetControlString("[IMG0] (Hold) Scroll x10", 1, "Ctrl");
    mDeconstructString->AutoSetControlString("[IMG0] Deconstruct item",  1, "OpenFieldAbilityMenu");
    mConfirmString->    AutoSetControlString("[IMG0] Confirm",           1, "Activate");
    mCancelString->     AutoSetControlString("[IMG0] Cancel",            1, "Cancel");

    //--"Change Sorting" string. The string changes depending on what the current sort is.
    SetSortingString(AdventureInventory::Fetch()->GetInternalSortFlag());
}

///======================================= Core Methods ===========================================
void AdvUIInventory::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Set Lines]
    //--Common lines.
    SetHelpString(i, "This menu shows all the items you currently have. Press [IMG0] to change the sorting.", 1, "F2");
    SetHelpString(i, "If a character has an item equipped, it will appear with their icon next to it.");
    SetHelpString(i, "You can see the basic properties, like value and stats, here. If you'd like a more complete");
    SetHelpString(i, "breakdown of an item's properties, highlight it and read the full description.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "Icon Legend: Statistics");

    //--Legend string.
    mHelpMenuStrings[i]->SetString("[IMG0] Health [MOV200][IMG1] Power [MOV400][IMG2] Accuracy [MOV600][IMG3] Evade [MOV800][IMG4] Initiative [MOV1000][IMG5] Protection");
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Health"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Attack"));
    mHelpMenuStrings[i]->SetImageP(2, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Accuracy"));
    mHelpMenuStrings[i]->SetImageP(3, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Evade"));
    mHelpMenuStrings[i]->SetImageP(4, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Initiative"));
    mHelpMenuStrings[i]->SetImageP(5, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Protection"));
    i ++;

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void AdvUIInventory::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Determines where the cursor should be given the inventory cursor and scrollbar offsets.
    float cEntryY = 147.0f;
    float cEntryH =  26.0f;
    float cBuf = 5.0f;

    //--Positions.
    float cLft = 67.0f - cBuf;
    float cTop = cEntryY + (cEntryH * (mItemCursor - mItemSkip)) - cBuf + 2.0f;
    float cRgt = cLft + 100.0f + cBuf + 11.0f;
    float cBot = cTop + cEntryH + cBuf + 1.0f;

    //--Get entry.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetExtendedItemList();
    AdventureItem *rEntry = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);
    if(rEntry)
    {
        cRgt = cLft + 52.0f + AdvImages.rFont_Mainline->GetTextWidth(rEntry->GetName()) + cBuf;
    }

    //--Set.
    mHighlightPos.MoveTo(cLft, cTop, cxCursorTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxCursorTicks);
}
void AdvUIInventory::SetSortingString(int pFlag)
{
    ///--[Documentation]
    //--The mChangeSortString changes its text based on the current sorting type.
    if(pFlag == AINV_SORT_CRITERIA_NAME)
    {
        mChangeSortString->AutoSetControlString("[IMG0] Change Sort (Current: By Name)",  1, "F2");
    }
    else if(pFlag == AINV_SORT_CRITERIA_VALUE)
    {
        mChangeSortString->AutoSetControlString("[IMG0] Change Sort (Current: By Value)", 1, "F2");
    }
    else if(pFlag == AINV_SORT_CRITERIA_TYPE)
    {
        mChangeSortString->AutoSetControlString("[IMG0] Change Sort (Current: By Type)",  1, "F2");
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIInventory::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ =============== Documentation ================ ]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--Standard Timers.
    StandardTimer(mVisibilityTimer,     0, cxVisTicks,         true);
    StandardTimer(mHelpVisibilityTimer, 0, cHlpTicks,          mIsShowingHelp);
    StandardTimer(mDeconstructTimer,    0, cxDeconstructTicks, mIsDeconstructMode);

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Deconstruction]
    //--In deconstruct mode, call the subroutine instead.
    if(mIsDeconstructMode)
    {
        UpdateDeconstruct();
        return true;
    }

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(mIsShowingHelp)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingHelp = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsShowingHelp = true;
        RefreshMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[ ============== Control Handling ============== ]
    //--This object has no sub-modes and very little control handling.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetExtendedItemList();

    ///--[Sorting]
    //--Change sorting.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--Modify.
        rInventory->IncrementInternalSort();
        rInventory->SortItemListByInternalCriteria();

        //--Rebuild the item list in order.
        AdventureInventory::Fetch()->BuildExtendedItemList();

        //--Change Sorting string.
        SetSortingString(AdventureInventory::Fetch()->GetInternalSortFlag());

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Up and Down]
    //--Automatic routine takes care of standardized scrolling.
    if(AutoListUpDn(mItemCursor, mItemSkip, rItemList->GetListSize(), cxScrollEdge, cItemsPerPage, true)) return true;

    ///--[Deconstruction]
    //--OpenFieldAbilityMenu. This begins deconstruction.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--No deconstruct script is set, fail.
        if(!AdventureMenu::xDeconstructScript || !strcasecmp(AdventureMenu::xDeconstructScript, "NULL")) return true;

        //--Deconstruct script exists. Set flags and clear any lingering data.
        mIsDeconstructMode = true;
        mDeconstructErrorEquipped = false;
        mDeconstructErrorNoResults = false;
        mDeconstructResults->ClearList();

        //--Check to see if the item is equipped. We can't deconstruct equipped items.
        AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);
        const char *rOwnerName = rItemList->GetNameOfElementBySlot(mItemCursor);

        //--Item somehow doesn't exist. Do nothing.
        if(!rItem)
        {
            mIsDeconstructMode = false;
        }
        //--Item exists, and it has an owner:
        else if(rOwnerName && strcasecmp(rOwnerName, "Null"))
        {
            mDeconstructErrorEquipped = true;
        }
        //--Item exists and has no owner. Run the script.
        else
        {
            //--Store the item name.
            ResetString(mStoredItemName, rItem->GetDisplayName());

            //--Call.
            LuaManager::Fetch()->ExecuteLuaFile(AdventureMenu::xDeconstructScript, 1, "S", mStoredItemName);
            if(mDeconstructResults->GetListSize() < 1) mDeconstructErrorNoResults = true;
        }

        //--Exit out.
        return true;
    }

    ///--[Cancel]
    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[ ================= Finish Up ================== ]
    //--Exited without any controls pressed.
    return true;
}
void AdvUIInventory::UpdateBackground()
{
    ///--[Timers]
    StandardTimer(mVisibilityTimer,     0, cxVisTicks,         false);
    StandardTimer(mHelpVisibilityTimer, 0, cHlpTicks,          false);
    StandardTimer(mDeconstructTimer,    0, cxDeconstructTicks, false);
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *AdvUIInventory::GetDeconstructList()
{
    return mDeconstructResults;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
