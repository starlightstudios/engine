///======================================== RootEffect ============================================
//--Root for the Effect heirarchy. Effects are owned by a room and have some impact on the entities
//  within them. Exactly what is up to the scripts called. All Effects are RootEntities.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootEntity.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define EFFECT_INITIALIZE 0
#define EFFECT_UPDATE 1
#define EFFECT_DESTRUCTOR 2

///========================================== Classes =============================================
class RootEffect : public RootEntity
{
    private:
    //--System
    int mTurnsLeft;
    char *mDisplayName;

    //--Calling
    char *mScriptPath;

    //--Owning Room
    char *mOwningRoomName;

    protected:

    public:
    //--System
    RootEffect();
    virtual ~RootEffect();

    //--Public Variables
    //--Property Queries
    const char *GetDisplayName();
    const char *GetOwningRoomName();

    //--Manipulators
    void SetDisplayName(const char *pName);
    void SetTurnsLeft(int pTurns);
    void SetScriptPath(const char *pPath);
    void SetOwningRoom(const char *pRoomName);

    //--Core Methods
    void RespondToBoot();
    void RespondToSelfDestruct();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Effect_Create(lua_State *L);
int Hook_Effect_SetProperty(lua_State *L);
int Hook_Effect_PushOwner(lua_State *L);
