//--Base
#include "imagelump.h"

//--System
#include <stdio.h>
#include <stdint.h>
#include <QString>
#include <QImage>

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

//--System
ImageLump::ImageLump()
{
    //--[ImageLump]
    //--System
    mLocalCompression = cInvalidCompression;

    //--Storage
    mDataWidth = 0;
    mDataHeight = 0;
    mTrueWidth = 0;
    mTrueHeight = 0;
    mXOffset = 0;
    mYOffset = 0;
    mDataArraySize = 0;
    mDataArray = NULL;
}
ImageLump::~ImageLump()
{
    free(mDataArray);
}

//--Property Queries
//--Manipulators
//--Core Methods
void ImageLump::RipFromFile(const char *pPath)
{
    //--Loads the given image and extracts its RGBA data to be placed into the local storage.
    if(!pPath) return;

    //--Modify the path. We need to remove "file:///" from the front since we receive a URL and not a direct path.
    const char *rModdedPath = &pPath[8];

    //--Load it into a QImage.
    QImage *tImage = new QImage();
    tImage->load(QString::fromLocal8Bit(rModdedPath));
    fprintf(stderr, "  Image loaded from file %s\n", rModdedPath);

    //--Compression type is always Uncompressed.
    mLocalCompression = cNoCompression;

    //--Basic data.
    mDataWidth = tImage->width();
    mDataHeight = tImage->height();
    mTrueWidth = mDataWidth;
    mTrueHeight = mDataHeight;
    mXOffset = 0;
    mYOffset = 0;
    fprintf(stderr, "  Data: %i %i\n", mDataWidth, mDataHeight);

    //--Allocate space.
    mDataArraySize = (mDataWidth * mDataHeight * 4 * sizeof(uint8_t));
    mDataArray = (uint8_t *)malloc(sizeof(uint8_t) * mDataArraySize);

    //--Copy data. QImage is ARGB, so it needs a swap. Image is NOT flipped like it is under GL.
    int tCursor = 0;
    for(int y = 0; y < mDataHeight; y ++)
    {
        for(int x = 0; x < mDataWidth; x ++)
        {
            QRgb tPixel = tImage->pixel(x, y);

            mDataArray[tCursor+0] = qRed(tPixel);
            mDataArray[tCursor+1] = qGreen(tPixel);
            mDataArray[tCursor+2] = qBlue(tPixel);
            mDataArray[tCursor+3] = qAlpha(tPixel);
            tCursor += 4;
        }
    }

    //--Clean up.
    delete tImage;
}

//--Update
//--File I/O
void ImageLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Overloaded member. Write the local data to the file.
    if(!pOutfile) return;

    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "IMAGE00000";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    //--Make sure the compression type is valid.  If it isn't, write a FAIL compression code, and
    //  no data.
    if(mLocalCompression >= cSupportedCompressions || mLocalCompression == ImageLump::cInvalidCompression)
    {
        uint8_t tVal = ImageLump::cInvalidCompression;
        fwrite(&tVal, sizeof(uint8_t), 1, pOutfile);
        sCounter += (sizeof(uint8_t) * 1);
        return;
    }

    //--Write the compression type
    fwrite(&mLocalCompression, sizeof(uint8_t), 1, pOutfile);
    sCounter += (sizeof(uint8_t) * 1);

    //--Write the offset data.
    fwrite(&mTrueWidth,  sizeof(int32_t), 1, pOutfile);
    fwrite(&mTrueHeight, sizeof(int32_t), 1, pOutfile);
    fwrite(&mXOffset,    sizeof(int32_t), 1, pOutfile);
    fwrite(&mYOffset,    sizeof(int32_t), 1, pOutfile);
    fwrite(&mDataWidth,  sizeof(int32_t), 1, pOutfile);
    fwrite(&mDataHeight, sizeof(int32_t), 1, pOutfile);
    sCounter += (sizeof(int32_t) * 6);

    //--Write the length of the data
    fwrite(&mDataArraySize, sizeof(uint32_t), 1, pOutfile);
    sCounter += (sizeof(uint32_t) * 1);

    //--Write the data itself.
    fwrite(mDataArray, sizeof(uint8_t), mDataArraySize, pOutfile);
    sCounter += (sizeof(uint8_t) * mDataArraySize);

    //--Write the number of hitboxes.  Can be zero.
    //  NOTE: In this program, hitboxes are always zero and are never written.
    uint8_t tHitboxesTotal = 0;
    fwrite(&tHitboxesTotal, sizeof(uint8_t), 1, pOutfile);
    sCounter += (sizeof(uint8_t) * 1);
}

//--Drawing
//--Pointer Routing
//--Static Functions
//--Lua Hooking




