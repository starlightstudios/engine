///======================================== MonoUIEquip ===========================================
//--UI module for Monoceros, inherits from the Adventure version with some changes for Monoceros'
//  slightly different layout.

#pragma once

///========================================= Includes =============================================
#include "AdvUIEquip.h"
#include "MonoUICommon.h"

#ifdef _TARGET_OS_WINDOWS_
    #ifndef _STEAM_API_
        #define constexpr const
    #endif
#endif
#ifdef _TARGET_OS_MAC_
#define constexpr const
#endif

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Icons
#define MONOUIEQP_ICONS_TOTAL 10
#define MONOUIEQP_RESISTANCE_BEGIN 6

///======================================== Translation ===========================================
///--[MonoUIBase_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIEQUIP_STRINGS_TOTAL 32
class MonoUIEquip_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIEQUIP_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextEquipment;
            char *mTextSlots;
            char *mTextGems;
            char *mTextStatistics;
            char *mTextHelpMenu;
            char *mTextImmune;
            char *mTextResists;
            char *mTextDamage;
            char *mTextEmpty;
            char *mTextEmptyUnequip;
            char *mTextHelp[17];
            char *mTextControlShowHelp;
            char *mTextControlItemDetails;
            char *mTextControlHoldScroll;
            char *mTextControlPrevChar;
            char *mTextControlNextChar;
        }str;
    };

    //--Functions
    MonoUIEquip_Strings();
    virtual ~MonoUIEquip_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIEquip : virtual public AdvUIEquip, virtual public MonoUICommon
{
    private:
    ///--[Constants]
    static const int   cxMonoEqpScrollBuf    = 2;
    static const int   cxMonoEqpScrollPage   = 7;
    static constexpr float cxMonoEqpSizePerEntry = 71.0f;

    ///--[System]
    ///--[Scrollbar]
    int mEqpSkip;

    ///--[Colors]
    StarlightColor mMonoTitleColor;
    StarlightColor mMonoSubtitleColor;
    StarlightColor mMonoParagraphColor;

    ///--[Images]
    struct
    {
        //--Fonts.
        StarFont *rFont_Control;
        StarFont *rFont_Description;

        //--Images
        StarBitmap *rFrame_Clean;
        StarBitmap *rFrame_Equipment;
        StarBitmap *rFrame_Resistances;
        StarBitmap *rFrame_Statistics;
        StarBitmap *rOverlay_DescriptionName;
        StarBitmap *rOverlay_Replacement;
        StarBitmap *rOverlay_Selection;
        StarBitmap *rOverlay_Titleshade;
        StarBitmap *rScrollbar_EqpStatic;
        StarBitmap *rPropertyIcons[MONOUIEQP_ICONS_TOTAL];
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIEquip();
    virtual ~MonoUIEquip();
    virtual void Construct();

    //--Public Variables
    static MonoUIEquip_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void UpdateSelection();

    //--File I/O
    //--Drawing
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    virtual void RenderEquipDetails(float pColorAlpha);
    virtual void RenderEquipEntry(int pSlot, float pColorAlpha);
    virtual void RenderEquipReplacements();
    virtual void RenderEquipProperties(float pColorAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


