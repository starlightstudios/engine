///======================================= CarnUIStatus ===========================================
//--Description

#pragma once

///========================================= Includes =============================================
#include "AdvUIStatus.h"
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CarnUIStatus : public CarnUICommon, virtual public AdvUIStatus
{
    protected:
    ///--[System]
    static const int cxHitbox_OrderForward  = 0;
    static const int cxHitbox_OrderBackward = 1;

    ///--[Character Storage]
    CarnationUIClass mClassData[CARNUI_CLASS_TOTAL];

    ///--[Images]
    struct
    {
        //--Fonts.
        StarFont *rFont_Header;
        StarFont *rFont_Mainline;
        StarFont *rFont_Statistic;

        //--Images.
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_BtnBackward;
        StarBitmap *rFrame_BtnForward;
        StarBitmap *rOverlay_HPFill;
        StarBitmap *rOverlay_SPFill;
        StarBitmap *rOverlay_MeterFrames;
        StarBitmap *rOverlay_StatDetail;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIStatus();
    virtual ~CarnUIStatus();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void ChangeCharacterCursor(int pNewValue);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();
    void HandleLeftClick();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


