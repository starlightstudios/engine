//--Base
#include "SlittedEyeLevel.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "MapManager.h"

///========================================== Drawing =============================================
void SlittedEyeLevel::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void SlittedEyeLevel::Render()
{
    ///--[Documentation]
    //--Handles the rendering cycle for a Slitted Eye level. These are basically pre-rendered images
    //  layered over one another. There is no fixed "backgrounds" or even any special effects, just
    //  the images in a layering order.
    //--Any UI renders over top of the images. Fading is also handled.
    if(!Images.mIsReady) return;

    ///--[Setup]
    //--Toggle this flag so the MapManager knows we took over rendering.
    MapManager::xHasRenderedMenus = true;

    ///--[Layer Rendering]
    //--If scrolling is disallowed, lock it to zero.
    float tScroll = mXOffset;
    if(mDisallowScrolling) tScroll = 0.0f;

    //--Offset.
    glTranslatef(-tScroll, 0.0f, 0.0f);

    //--Render all layers in order.
    for(int i = 0; i < mImagesTotal; i ++)
    {
        //--Skip empty slots.
        if(!mrImages[i]) continue;

        //--Render.
        mrImages[i]->Draw();
    }

    //--Clean.
    glTranslatef(tScroll, 0.0f, 0.0f);

    ///--[GUI]
    //--Setup.
    float tGUILft = 0.0f;
    float tGUITop = 0.0f;
    float cGUIHei = Images.GUI.rGUIDefaultFont->GetTextHeight();

    //--Time rendering. Compute what time it is, based on the stored value. Time starts at 10:00pm, and is capable
    //  of resolution to 1 "minute", which is not actually a minute of real time.
    //--If the value is over 180, it rolls to 1:00am instead of 12:00am.
    char tBuffer[128];
    int tMinutes = mGameTime % 60;
    int tHours = mGameTime / 60;
    if(tHours < 3)
    {
        sprintf(tBuffer, "%i:%02i", tHours+10, tMinutes);
    }
    else
    {
        sprintf(tBuffer, "%i:%02i", tHours-2, tMinutes);
    }

    //--Render.
    Images.GUI.rGUIDefaultFont->DrawTextArgs(tGUILft, tGUITop + (cGUIHei * 0.0f), 0, 1.0f, "Time: %s", tBuffer);
    Images.GUI.rGUIDefaultFont->DrawTextArgs(tGUILft, tGUITop + (cGUIHei * 1.0f), 0, 1.0f, "Stamina: %i", (int)round(mStamina));
    Images.GUI.rGUIDefaultFont->DrawTextArgs(tGUILft, tGUITop + (cGUIHei * 2.0f), 0, 1.0f, "Noise: %i", (int)round(mNoise));

    ///--[Fade Rendering]
    //--If a fade is in place, render it here.
    if(mIsFading)
    {
        StarBitmap::DrawFullColor(mFadeColCurrent);
    }
    //--No fade is in place, but the last one persists. Render that.
    else if(!mIsFading && mFadePersists)
    {
        StarBitmap::DrawFullColor(mFadeColFinish);
    }

    ///--[Hissing Hitbox Debug]
    //--Render the positions of the hitboxes for debug.
    if(false)
    {
        //--Setup.
        glDisable(GL_TEXTURE_2D);
        glColor3f(1.0f, 0.0f, 0.0f);
        glTranslatef(-tScroll, 0.0f, 0.0f);

        //--Iterate.
        HissingHitbox *rHitbox = (HissingHitbox *)mHissHitboxes->PushIterator();
        while(rHitbox)
        {
            //--Render.
            glBegin(GL_LINE_LOOP);
                glVertex2f(rHitbox->mHitbox.mLft, rHitbox->mHitbox.mTop);
                glVertex2f(rHitbox->mHitbox.mRgt, rHitbox->mHitbox.mTop);
                glVertex2f(rHitbox->mHitbox.mRgt, rHitbox->mHitbox.mBot);
                glVertex2f(rHitbox->mHitbox.mLft, rHitbox->mHitbox.mBot);
            glEnd();

            //--Next.
            rHitbox = (HissingHitbox *)mHissHitboxes->AutoIterate();
        }

        //--Clean.
        glTranslatef(tScroll, 0.0f, 0.0f);
        StarlightColor::ClearMixer();
        glEnable(GL_TEXTURE_2D);
    }

    ///--[Mouse Debug]
    //--Renders where the program thinks the mouse cursor is.
    if(false)
    {
        //--Setup.
        glTranslatef(mMouseX, mMouseY, 0.0f);
        glDisable(GL_TEXTURE_2D);
        glPointSize(3.0f);
        glColor3f(1.0f, 0.0f, 0.0f);
        glEnable(GL_POINT_SMOOTH);

        //--Render.
        glBegin(GL_POINTS);
            glVertex2f(0.0f, 0.0f);
        glEnd();

        //--Clean.
        glDisable(GL_POINT_SMOOTH);
        StarlightColor::ClearMixer();
        glPointSize(1.0f);
        glEnable(GL_TEXTURE_2D);
        glTranslatef(-mMouseX, -mMouseY, 0.0f);
    }
}
