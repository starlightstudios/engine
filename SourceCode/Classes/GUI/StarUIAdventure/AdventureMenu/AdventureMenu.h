///======================================= AdventureMenu ==========================================
//--Menu used in Adventure Mode. Has sub-menus for things like status, party arrangement, equipment, and inventory.
#pragma once

///========================================== Includes ============================================
//--Raw
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "GridHandler.h"
#include "AdvCombatDefStruct.h"
#include "AdventureInventory.h"
#include "AdvUIBase.h"
#include "AdvUICampfire.h"

///===================================== Local Definitions ========================================
//--Submodes
#define AM_SUBMODE_NONE 0
#define AM_SUBMODE_CAMPFIRE 1
#define AM_SUBMODE_FIELDABILITIES 2
#define AM_SUBMODE_TRAINER 3
#define AM_SUBMODE_VENDOR 4

//--Timers
#define AM_MAIN_VIS_TICKS 15

//--Spacing
#define AM_MAINLINE_H 22.0f
#define AM_HELP_SYMBOL_TEXT_H 22.0f
#define AM_MAINLINE_INDENT 12.0f

///===================================== Local Structures =========================================
//--Forward Declaration
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Structure Declarations
struct LoadingPack;
struct AdvMenuJournalAchievementEntry;

///--[Classes]
class AdventureMenu : public RootObject
{
    private:
    protected:
    ///--[Constants]
    //--Error Codes
    static const int cxAdvError_None = 0;
    static const int cxAdvError_NoMenu = 1;
    static const int cxAdvError_NotFound = 2;

    ///--[System]
    //--System
    bool mOpenedThisTick;
    bool mStartedHidingThisTick;
    bool mHandledUpdate;
    int mMainOpacityTimer;
    bool mIsVisible;
    uint8_t mCurrentMode;

    //--Closing Handler
    char *mClosingScript;

    //--Sub-modes.
    int mSubmodeCode;

    //--Colors
    StarlightColor mGreyStatic;
    StarlightColor mHeadingStatic;

    ///--[Component UI]
    //--Individual Components.
    AdvUIBase *rUIBase;                       //Held as a reference to the object on the mrMainMenuComponents list.
    AdvUICampfire *rCampBase;                 //Held as a reference to the object on the mrCampfireComponents list.

    //--List of components.
    StarLinkedList *mrCampfireComponents;     //StarUIPiece *, reference
    StarLinkedList *mrVendorComponents;       //StarUIPiece *, reference
    StarLinkedList *mrFieldAbilityComponents; //StarUIPiece *, reference
    StarLinkedList *mrTrainerComponents;      //StarUIPiece *, reference
    StarLinkedList *mrMainMenuComponents;     //StarUIPiece *, reference
    StarLinkedList *mAllComponents;           //StarUIPiece *, master

    ///--[Help Menus]
    AdvHelp *mMainMenuHelp;

    public:
    ///========================== Class Functions =============================
    //--System
    AdventureMenu();
    virtual ~AdventureMenu();

    //--Public Variables
    static bool xSuppressLoadingErrors;
    static char *xRestResolveScript;
    static char *xFieldAbilityResolveScript;
    static char *xWarpResolveScript;
    static char *xWarpExecuteScript;
    static int xCatalystTotal;
    static int xLastSavefileSortingType;
    static char *xCostumeIconPath;
    static char *xTransformIconPath;
    static char *xReliveIconPath;
    static bool xUseAntchievement;
    static bool xDisallowEquippingFromResponse;
    static char *xDeconstructScript;

    //--Text Display Remaps
    static int xTextImgRemapsTotal;
    static char **xTextImgRemap; //Name that appears in the text
    static char **xTextImgPath;  //Path that the name maps to

    //--Property Queries
    bool StartedHidingThisTick();
    bool IsVisible();
    bool NeedsToBlockControls();
    bool NeedsToRender();

    //--Manipulators
    virtual void Show();
    void Hide();
    void ZeroVisibility();
    void SetCloseScript(const char *pPath);
    void AppendDeconstructItem(const char *pString);

    //--Core Methods
    void OpenUIByBackwardsCode(int pCode);
    void OpenCampUIByBackwardsCode(int pCode);

    ///--[Submoves]
    void OpenCampfire();
    void SetBenchMode(bool pFlag);
    void OpenFieldAbilities();
    void CloseFieldAbilities();
    void OpenTrainer();
    void CloseTrainer();
    void InitializeVendorMode(const char *pShopName, const char *pPath, const char *pTeardownPath);
    void CloseVendor();

    ///--[Lists]
    void RegisterUI(const char *pName, StarUIPiece *pElement, int pBackwardCode, StarLinkedList *pList);
    void UnregisterUI(const char *pName, StarLinkedList *pList);
    StarUIPiece *RetrieveUI(const char *pName, int pType, StarLinkedList *pList, const char *pErrorCall);
    StarUIPiece *LocateComponentInList(StarLinkedList *pList, const char *pCode);
    void ActivateComponentInListName(StarLinkedList *pList, const char *pName);
    void ActivateComponentInListCode(StarLinkedList *pList, const char *pCode);
    void ListUpdateBackground(StarLinkedList *pList);
    int ListUpdateForeground(bool &sHandledUpdate, bool pIgnoreModes, StarLinkedList *pList);
    void ListRenderForeground(StarLinkedList *pList, float pOpacity);

    //--Nowhere Mode
    void InitializeNowhereMode();
    void UpdateNowhereMode();
    void RenderNowhereMode();

    ///--[Rest]
    void ExecuteRest();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void Render();
    void RenderCursor(float pLft, float pTop);
    static void RenderBox(float pLft, float pTop, float pRgt, float pBot);

    //--Pointer Routing
    StarLinkedList *GetComponentList();
    static StarUIPiece *ResolveUIPiece(int &sIsFailErroneous, const char *pName, int pType);
    StarLinkedList *GetGemAdjectiveList();
    AdvUIVendor *GetVendor();
    AdvUIWarp *GetWarpUI();
    AdvUIJournal *GetJournalUI();
    AdvUISkills *GetSkillsUI();

    //--Static Functions
    static AdventureMenu *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
    static void HookToLuaStateShop(lua_State *pLuaState);
    static void HookToLuaStateJournal(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AM_GetProperty(lua_State *L);
int Hook_AM_SetProperty(lua_State *L);
int Hook_AM_SetMapInfo(lua_State *L);

//--Shop Functions
int Hook_AM_SetShopProperty(lua_State *L);
