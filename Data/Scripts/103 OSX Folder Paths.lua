-- |[ ==================================== OSX Folder Paths ==================================== ]|
--Until I can get PhysFS to compile on OSX, or find some other filesystem that works, files are
-- checked manually here using specified filenames. You're welcome.
local iSlot = 0

-- |[Path Listings for Starlight Games]|
--Adventure Mode
if(LM_IsGameRegistered("Pandemonium") == true) then
    fnAddGameEntry("Adventure Mode", "Root/Paths/System/Startup/sAdventurePath", "Play Runes of Pandemonium", 100, "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Adventure Mode")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/AdventureMode/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../adventure/Games/AdventureMode/ZLaunch.lua")
        VM_SetVar("Root/Paths/System/Startup/sAdventurePath", "S", string.sub(gsaGameEntries[iSlot].sActivePath, 1, -12))
    end
end

--String Tyrant and Demo
if(LM_IsGameRegistered("String Tyrant") == true) then
    
    --Main Game
    fnAddGameEntry("Doll Manor", "Root/Paths/System/Startup/sDollManorPath", "Play String Tyrant", 101, "YMenuLaunch.lua", -23)
    iSlot = fnGetGameIndex("Doll Manor")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/DollManor/000 Launcher Script.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../dollmanor/Games/DollManor/000 Launcher Script.lua")
    end

    --Demo
    fnAddGameEntry("Doll Manor Demo", "Root/Paths/System/Startup/sDollManorDemoPath", "Play String Tyrant Demo", 102, "YMenuLaunch.lua", -23)
    iSlot = fnGetGameIndex("Doll Manor Demo")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/DollManorDemo/000 Launcher Script.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../dollmanor/Games/DollManorDemo/000 Launcher Script.lua")
    end
end

--Slitted Eye
if(LM_IsGameRegistered("Slitted Eye") == true) then
    fnAddGameEntry("Slitted Eye", "Root/Paths/System/Startup/sSlittedEyePath", "Play Slitted Eye", 103,  "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Slitted Eye")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/Slitted Eye/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../slittedeye/Games/Slitted Eye/ZLaunch.lua")
    end
end

--Electrosprite Text Adventure: Only appears if the player unlocked it in Adventure Mode.
local sDoesOptionExist = OM_OptionExists("Unlock Electrosprites")
if(sDoesOptionExist == "N" and LM_IsGameRegistered("Pandemonium") == true) then

    --Get variable.
    local fUnlockElectrosprites = OM_GetOption("Unlock Electrosprites")
    
    --Unlock it.
    if(fUnlockElectrosprites == 1.0) then
        fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "Play Electrosprite Adventure", 104, "YMenuLaunch.lua", -23)
        iSlot = fnGetGameIndex("Electrosprite Adventure")
        if(iSlot ~= 0) then
            fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
            fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
        end
    
    --Mark it, but don't add it.
    else
        fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "DO NOT ADD", 104, "YMenuLaunch.lua", -23)
        iSlot = fnGetGameIndex("Electrosprite Adventure")
        if(iSlot ~= 0) then
            fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
            fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
        end
    end

--Option did not exist. Mark it, do not add it.
else
    fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "DO NOT ADD", 104, "YMenuLaunch.lua", -23)
    iSlot = fnGetGameIndex("Electrosprite Adventure")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
    end
end

-- |[Non-Bottled-Starlight Games]|
--Monsters of the Forest
if(LM_IsGameRegistered("Pairanormal") == true) then
    fnAddGameEntry("MOTF", "Root/Paths/System/Startup/sMOTFPath", "Play Monsters of the Forest", 105, "YMenuLaunch.lua", -23)
    iSlot = fnGetGameIndex("MOTF")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/MonstersOfTheForest/000 Launcher Script.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../monstersoftheforest/Games/MonstersOfTheForest/000 Launcher Script.lua")
    end
end

--Pairanormal
if(LM_IsGameRegistered("Pairanormal") == true) then
    fnAddGameEntry("Pairanormal", "Root/Paths/System/Startup/sPairanormalPath", "Play Pairanormal", 106,  "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Pairanormal")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/Pairanormal/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../pairanormal/Games/Pairanormal/ZLaunch.lua")
    end
end

--Our Own Wonderland
if(LM_IsGameRegistered("Our Own Wonderland") == true) then
    fnAddGameEntry("Our Own Wonderland", "Root/Paths/System/Startup/sWonderlandPath", "Play Our Own Wonderland", 107,  "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Our Own Wonderland")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/Wonderland/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../oow/Games/Wonderland/ZLaunch.lua")
    end
end

--Peak Freaks
if(LM_IsGameRegistered("Peak Freaks") == true) then
    fnAddGameEntry("Peak Freaks", "Root/Paths/System/Startup/sPeakFreaksPath", "Play Peak Freaks", 108,  "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Peak Freaks")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/PeakFreaks/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../peakfreaks/Games/PeakFreaks/ZLaunch.lua")
    end
end

-- |[Path Listing for Classic Mode Games]|
if(LM_IsGameRegistered("Pandemonium Classic") == true) then
    --Classic Mode
    fnAddGameEntry("Classic Mode", "Root/Paths/System/Startup/sClassicModePath", "Play Classic Mode", 109, "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Classic Mode")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/ClassicMode/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../classic/Games/ClassicMode/ZLaunch.lua")
    end

    --Classic Mode 3D
    fnAddGameEntry("Classic Mode 3D", "Root/Paths/System/Startup/sClassicMode3DPath", "Play Classic Mode 3D", 110, "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Classic Mode 3D")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/ClassicMode3D/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../classic/Games/ClassicMode3D/ZLaunch.lua")
    end

    --Corrupter Mode
    fnAddGameEntry("Corrupter Mode", "Root/Paths/System/Startup/sCorrupterModePath", "Play Corrupter Mode", 111,  "YMenuLaunch.lua", -12)
    iSlot = fnGetGameIndex("Corrupter Mode")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/CorrupterMode/ZLaunch.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../classic/Games/CorrupterMode/ZLaunch.lua")
    end
end

-- |[Tools]|
--Level Generator
if(LM_IsGameRegistered("Pandemonium") == true) then
    fnAddGameEntry("Level Generator", "Root/Paths/System/Startup/sLevelGeneratorPath", "Show Level Generator", 112, "YMenuLaunch.lua", -10)
    iSlot = fnGetGameIndex("Level Generator")
    if(iSlot ~= 0) then
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "Games/AdventureLevelGenerator/Grids.lua")
        fnAddGamePath(gsaGameEntries[iSlot].sSearchPaths, "../adventure/Games/AdventureLevelGenerator/Grids.lua")
    end
end
