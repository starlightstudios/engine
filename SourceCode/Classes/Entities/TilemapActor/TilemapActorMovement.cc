//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"
#include "AdventureInventory.h"
#include "TileLayer.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

bool TilemapActor::EmulateMovement(bool pLft, bool pTop, bool pRgt, bool pBot)
{
    //--Uses the entity's movement speed to emulate movement.
    return EmulateMovement(pLft, pTop, pRgt, pBot, mMoveSpeed, mRunSpeed);
}
bool TilemapActor::EmulateMovement(bool pLft, bool pTop, bool pRgt, bool pBot, float pWalkSpeed, float pRunSpeed)
{
    ///--[Documentation and Setup]
    //--Given a set of controls, attempts to move according to the direction implied by those controls.
    bool tAllowDiagonals = true;
    float cMoveSpeed = pWalkSpeed;
    float cCos45 = 0.707107f;
    float tMoveX = 0.0f;
    float tMoveY = 0.0f;

    //--Store current position and remainders.
    float tOldPosX = mTrueX;
    float tOldPosY = mTrueY;
    float tOldRemX = mRemainderX;
    float tOldRemY = mRemainderY;

    //--Speed increase!
    mRanLastTick = false;
    if(mIsRunning)
    {
        //--Basic flags.
        mRanLastTick = true;
        cMoveSpeed = pRunSpeed;
    }

    ///--[Resolve Speeds]
    //--Player is pressing left and not right...
    if(pLft && !pRgt)
    {
        //--Player is pressing up and not down...
        if(pTop && !pBot)
        {
            mFacing = TA_DIR_NW;
            tMoveX = cMoveSpeed * -cCos45;
            tMoveY = cMoveSpeed * -cCos45;
            tAllowDiagonals = false;
        }
        //--Player is pressing down and not up...
        else if(!pTop && pBot)
        {
            mFacing = TA_DIR_SW;
            tMoveX = cMoveSpeed * -cCos45;
            tMoveY = cMoveSpeed *  cCos45;
            tAllowDiagonals = false;
        }
        //--Player is not pressing up or down, move at full speed.
        else
        {
            mFacing = TA_DIR_WEST;
            tMoveX = cMoveSpeed * -1.0f;
            mRemainderY = 0.0f;
        }
    }
    //--Player is pressing right and not left...
    else if(!pLft && pRgt)
    {
        //--Player is pressing up and not down...
        if(pTop && !pBot)
        {
            mFacing = TA_DIR_NE;
            tMoveX = cMoveSpeed *  cCos45;
            tMoveY = cMoveSpeed * -cCos45;
            tAllowDiagonals = false;
        }
        //--Player is pressing down and not up...
        else if(!pTop && pBot)
        {
            mFacing = TA_DIR_SE;
            tMoveX = cMoveSpeed *  cCos45;
            tMoveY = cMoveSpeed *  cCos45;
            tAllowDiagonals = false;
        }
        //--Player is not pressing up or down, move at full speed.
        else
        {
            mFacing = TA_DIR_EAST;
            tMoveX = cMoveSpeed *  1.0f;
            mRemainderY = 0.0f;
        }
    }
    //--Player is not pressing left or right.
    else if(!pLft && !pRgt)
    {
        //--Player is pressing up and not down...
        if(pTop && !pBot)
        {
            mRemainderX = 0.0f;
            mFacing = TA_DIR_NORTH;
            tMoveY = cMoveSpeed * -1.0f;
        }
        //--Player is pressing down and not up...
        else if(!pTop && pBot)
        {
            mRemainderX = 0.0f;
            mFacing = TA_DIR_SOUTH;
            tMoveY = cMoveSpeed * 1.0f;
        }
        //--No keypresses.
        else
        {
            mRemainderX = 0.0f;
            mRemainderY = 0.0f;
        }
    }

    //--Store facing if this is the player:
    if(xIsPlayerCollisionCheck) xPlayerFacing = mFacing;

    //--Factor for wall movement.
    const float cFactor = 0.35f;

    //--Add remainders.
    float tStoreMoveX = tMoveX + mRemainderX;
    float tStoreMoveY = tMoveY + mRemainderY;
    mRemainderX = fmodf(tStoreMoveX, 1.0f);
    mRemainderY = fmodf(tStoreMoveY, 1.0f);
    if(mRemainderX < -cFactor || mRemainderX > cFactor || mRemainderY < -cFactor || mRemainderY > cFactor) mHasSlopeMoveLeft = true;

    //--Final move is the integer value, no half-pixel movements.
    int tFinalMoveX = (int)tStoreMoveX;
    int tFinalMoveY = (int)tStoreMoveY;
    if(tFinalMoveX != 0 || tFinalMoveY != 0) mHasSlopeMoveLeft = true;

    ///--[Movement Loop]
    //--Movement loop.
    bool tMovedAlongSlope = false;
    bool tOldSlopeFlag = mHasSlopeMoveLeft;
    bool tMovedAtAll = HandleMovement(tFinalMoveX, tFinalMoveY, tAllowDiagonals);
    if(mHasSlopeMoveLeft == false && tOldSlopeFlag == true && tAllowDiagonals == true)
    {
        //--X.
        if(mRemainderX < -cFactor)
            mRemainderX = mRemainderX + cFactor;
        else if(mRemainderX > cFactor)
            mRemainderX = mRemainderX - cFactor;
        else
            mRemainderX = 0.0f;

        //--Y.
        if(mRemainderY < -cFactor)
            mRemainderY = mRemainderY + cFactor;
        else if(mRemainderY > cFactor)
            mRemainderY = mRemainderY - cFactor;
        else
            mRemainderY = 0.0f;

        //--Flag.
        tMovedAlongSlope = true;
    }

    //--We are considered to be moving if any of the positions or remainders changed.
    if(mTrueX != tOldPosX || mTrueY != tOldPosY || (tMovedAlongSlope && tAllowDiagonals))
    {
        mIsMoving = true;
        mMoveTimer ++;
        mLastMoveTick = 0;
    }
    //--If the remainders changed, we need some extra checks.
    else if((mRemainderX != tOldRemX || mRemainderY != tOldRemY))
    {
        //--If the player is moving in a cardinal direction...
        if(tAllowDiagonals)
        {
            //--Setup.
            bool tAnyCheckPassed = false;

            //--If the value became further from 0, it's a move.
            if(mRemainderX > 0.0f && tOldRemX > 0.0f && !IsRgtClippedNarrow())
            {
                tAnyCheckPassed = true;
                mIsMoving = true;
                mMoveTimer ++;
                mLastMoveTick = 0;
            }
            //--Negative version.
            else if(mRemainderX < 0.0f && tOldRemX < 0.0f && !IsLftClippedNarrow())
            {
                tAnyCheckPassed = true;
                mIsMoving = true;
                mMoveTimer ++;
                mLastMoveTick = 0;
            }
            //--Y version positive.
            else if(mRemainderY > 0.0f && tOldRemY > 0.0f && !IsBotClippedNarrow())
            {
                tAnyCheckPassed = true;
                mIsMoving = true;
                mMoveTimer ++;
                mLastMoveTick = 0;
            }
            //--Negative version.
            else if(mRemainderY < 0.0f && tOldRemY < 0.0f && !IsTopClippedNarrow())
            {
                tAnyCheckPassed = true;
                mIsMoving = true;
                mMoveTimer ++;
                mLastMoveTick = 0;
            }

            //--No check passed, stop moving.
            if(!tAnyCheckPassed && !mAutoAnimates)
            {
                mIsMoving = false;
                mMoveTimer = -1;
            }
        }
        //--If moving diagonally...
        else
        {
            //--Only one remainder is allowed to change. Doesn't matter which.
            if(mHasSlopeMoveLeft)
            {
                mIsMoving = true;
                mMoveTimer ++;
                mLastMoveTick = 0;
            }
            //--Stop moving.
            else
            {
                if(!mAutoAnimates)
                {
                    mIsMoving = false;
                    mMoveTimer = -1;
                }
            }
        }
    }
    //--Not moving.
    else
    {
        //--Flag.
        if(!mAutoAnimates) mIsMoving = false;

        //--Move timer doesn't get unset if mAutoAnimates is true. Some flying entities do this.
        if(!mAutoAnimates) mMoveTimer = 0;
    }

    //--Recheck inflection based on our dimensions.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel)
    {
        int tCode = rActiveLevel->GetEffectiveDepth(mTrueX + TA_SIZE * 0.5f, mTrueY + TA_SIZE * 0.5f);
        if(tCode != INFLECTION_NO_CHANGE) mCollisionDepth = tCode;
    }

    //--Once movement is finalized, position the activation rectangle by facing.
    ResolveActivationDimensions();
    return tMovedAtAll;
}
bool TilemapActor::HandleMovement(int pX, int pY, bool pAllowDiagonals)
{
    ///--[Documentation]
    //--Given two movement amounts, attempts to move along the given distance. This respects collisions
    //  if the Actor is flagged to use those.
    //--This is used both by the player and NPCs when they move, and is used by cutscene movement handlers.
    //--Returns true if at least one pixel move was completed, false otherwise.
    bool tMovedAtAll = false;
    while(pX != 0 || pY != 0)
    {
        //--Collision data.
        bool tIsLftClipped = IsLftClipped();
        bool tIsRgtClipped = IsRgtClipped();
        bool tIsTopClipped = IsTopClipped();
        bool tIsBotClipped = IsBotClipped();

        //--Left movement.
        if(pX < 0)
        {
            //--Add.
            pX ++;

            //--We can move left if the space is open...
            if(!tIsLftClipped)
            {
                //--Modify.
                tMovedAtAll = true;
                mTrueX = mTrueX - 1.0f;
                mTileX = mTrueX / TileLayer::cxSizePerTile;
                mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
            }
            //--Check slopes.
            else if(mHasSlopeMoveLeft)
            {
                //--Up slope. Check if we can move left if we moved up 1 pixel.
                bool tCheckPassed = false;
                if(!tIsTopClipped)
                {
                    //--Move up.
                    mTrueY = mTrueY - 1.0f;

                    //--Check passed.
                    if(!IsLftClipped())
                    {
                        //--Flag.
                        mHasSlopeMoveLeft = false;
                        tCheckPassed = true;

                        //--Can't actually move unless this flag was true.
                        if(pAllowDiagonals)
                        {
                            tMovedAtAll = true;
                            mTrueX = mTrueX - 1.0f;
                            mTileX = mTrueX / TileLayer::cxSizePerTile;
                            mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                        }
                        else
                        {
                            mTrueY = mTrueY + 1.0f;
                        }
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueY = mTrueY + 1.0f;
                    }
                }

                //--Down slope. Check if we can move left if we moved down 1 pixel. Fails if we already did the up slope check.
                if(!tCheckPassed && !tIsBotClipped)
                {
                    //--Move up.
                    mTrueY = mTrueY + 1.0f;

                    //--Check passed.
                    if(!IsLftClipped())
                    {
                        //--Flag.
                        mHasSlopeMoveLeft = false;

                        //--Can't actually move unless this flag was true.
                        if(pAllowDiagonals)
                        {
                            tMovedAtAll = true;
                            mTrueX = mTrueX - 1.0f;
                            mTileX = mTrueX / TileLayer::cxSizePerTile;
                            mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                        }
                        else
                        {
                            mTrueY = mTrueY - 1.0f;
                        }
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueY = mTrueY - 1.0f;
                    }
                }
            }
        }
        //--Right movement.
        else if(pX > 0)
        {
            //--Add.
            pX --;

            //--We can move left if the space is open...
            if(!tIsRgtClipped)
            {
                //--Modify.
                tMovedAtAll = true;
                mTrueX = mTrueX + 1.0f;
                mTileX = mTrueX / TileLayer::cxSizePerTile;
                mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
            }
            //--Check slopes.
            else if(mHasSlopeMoveLeft)
            {
                //--Up slope. Check if we can move right if we moved up 1 pixel.
                bool tCheckPassed = false;
                if(!tIsTopClipped)
                {
                    //--Move up.
                    mTrueY = mTrueY - 1.0f;

                    //--Check passed.
                    if(!IsRgtClipped())
                    {
                        //--Flag.
                        mHasSlopeMoveLeft = false;
                        tCheckPassed = true;

                        //--Can't actually move unless this was true.
                        if(pAllowDiagonals)
                        {
                            tMovedAtAll = true;
                            mTrueX = mTrueX + 1.0f;
                            mTileX = mTrueX / TileLayer::cxSizePerTile;
                            mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                        }
                        else
                        {
                            mTrueY = mTrueY + 1.0f;
                        }
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueY = mTrueY + 1.0f;
                    }
                }

                //--Down slope. Check if we can move left if we moved down 1 pixel. Fails if we already did the up slope check.
                if(!tCheckPassed && !tIsBotClipped)
                {
                    //--Move up.
                    mTrueY = mTrueY + 1.0f;

                    //--Check passed.
                    if(!IsRgtClipped())
                    {
                        //--Flag.
                        mHasSlopeMoveLeft = false;

                        //--Can't actually move unless this was true.
                        if(pAllowDiagonals)
                        {
                            tMovedAtAll = true;
                            mTrueX = mTrueX + 1.0f;
                            mTileX = mTrueX / TileLayer::cxSizePerTile;
                            mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                        }
                        else
                        {
                            mTrueY = mTrueY - 1.0f;
                        }
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueY = mTrueY - 1.0f;
                    }
                }
            }
        }

        //--Collision data.
        tIsLftClipped = IsLftClipped();
        tIsRgtClipped = IsRgtClipped();
        tIsTopClipped = IsTopClipped();
        tIsBotClipped = IsBotClipped();

        //--Upwards movement.
        if(pY < 0)
        {
            //--Add.
            pY ++;

            //--We can move left if the space is open...
            if(!tIsTopClipped)
            {
                //--Modify.
                tMovedAtAll = true;
                mTrueY = mTrueY - 1.0f;
                mTileY = mTrueY / TileLayer::cxSizePerTile;
                mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
            }
            //--Check slopes.
            else if(mHasSlopeMoveLeft && pAllowDiagonals)
            {
                //--Left slope. Check if we can move up if we move one pixel to the left.
                bool tCheckPassed = false;
                if(!tIsLftClipped)
                {
                    //--Move up.
                    mTrueX = mTrueX - 1.0f;

                    //--Check passed.
                    if(!IsTopClipped())
                    {
                        mHasSlopeMoveLeft = false;
                        tCheckPassed = true;
                        tMovedAtAll = true;
                        mTrueY = mTrueY - 1.0f;
                        mTileY = mTrueY / TileLayer::cxSizePerTile;
                        mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueX = mTrueX + 1.0f;
                    }
                }

                //--Right slope. As above, one pixel to the right.
                if(!tCheckPassed && !tIsRgtClipped)
                {
                    //--Move up.
                    mTrueX = mTrueX + 1.0f;

                    //--Check passed.
                    if(!IsTopClipped())
                    {
                        mHasSlopeMoveLeft = false;
                        tMovedAtAll = true;
                        mTrueY = mTrueY - 1.0f;
                        mTileY = mTrueY / TileLayer::cxSizePerTile;
                        mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueX = mTrueX - 1.0f;
                    }
                }
            }
        }
        //--Downwards movement.
        else if(pY > 0)
        {
            //--Add.
            pY --;

            //--We can move left if the space is open...
            if(!tIsBotClipped)
            {
                //--Modify.
                tMovedAtAll = true;
                mTrueY = mTrueY + 1.0f;
                mTileY = mTrueY / TileLayer::cxSizePerTile;
                mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
            }
            //--Check slopes.
            else if(mHasSlopeMoveLeft && pAllowDiagonals)
            {
                //--Left slope. Check if we can move down if we move one pixel to the left.
                bool tCheckPassed = false;
                if(!tIsLftClipped)
                {
                    //--Move up.
                    mTrueX = mTrueX - 1.0f;

                    //--Check passed.
                    if(!IsBotClipped())
                    {
                        mHasSlopeMoveLeft = false;
                        tCheckPassed = true;
                        tMovedAtAll = true;
                        mTrueY = mTrueY + 1.0f;
                        mTileY = mTrueY / TileLayer::cxSizePerTile;
                        mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueX = mTrueX + 1.0f;
                    }
                }

                //--Right slope. As above, one pixel to the right.
                if(!tCheckPassed && !tIsRgtClipped)
                {
                    //--Move up.
                    mTrueX = mTrueX + 1.0f;

                    //--Check passed.
                    if(!IsBotClipped())
                    {
                        mHasSlopeMoveLeft = false;
                        tMovedAtAll = true;
                        mTrueY = mTrueY + 1.0f;
                        mTileY = mTrueY / TileLayer::cxSizePerTile;
                        mAmountMovedLastTick = mAmountMovedLastTick + 1.0f;
                    }
                    //--Check failed, reset the Y position.
                    else
                    {
                        mTrueX = mTrueX - 1.0f;
                    }
                }
            }
        }
    }

    //--Report whether or not any moves took place.
    return tMovedAtAll;
}
