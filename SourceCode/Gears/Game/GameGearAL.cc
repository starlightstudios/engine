///--[Allegro Version]
//--The GameGear makes extensive use of the event procedures, so it's split up into versions for
//  each of the handlers available for events. This is the Allegro version.
#if defined _ALLEGRO_PROJECT_

//--Base
#include "GameGear.h"

//--Classes
#include "PlayerPony.h"
#include "VisualLevel.h"
#include "TilemapActor.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarCamera2D.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarLoadInterrupt.h"

//--Definitions
#include "DebugDefinitions.h"
#include "Global.h"
#include "Program.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "MapManager.h"
#include "NetworkManager.h"
#include "OptionsManager.h"

///--[Forward Defs]
void RenderDebugStrings();
void RenderFPS();
void RenderJoypadDepression();

///--[Debug]
//#define AL_GEAR_DEBUG
#ifdef AL_GEAR_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--Definitions for which handlers are printing.
#define AL_GEAR_EVENTS false
#define AL_GEAR_LOGIC true
#define AL_GEAR_RENDER true

///======================================= Event Handler ==========================================
void GameGear::EventHandler(DriveShaft &sDriveShaft)
{
    //--Event handler, any event other than FORM_TIMER_EXPIRED should be handled here.
    GLOBAL *rGlobal = Global::Shared();
    ALLEGRO_EVENT tEvent = rGlobal->gEvent;
    DebugPush(AL_GEAR_EVENTS, "[Event Handler] Begin %i\n", tEvent.type);

    //--Logic tick is passed to the function pointed at.
    if(tEvent.type == ALLEGRO_EVENT_TIMER)
    {
        if(sDriveShaft.GameTime >= 1.0f) Global::Shared()->gTicksElapsed ++;
        rGlobal->rCurrentLogicHandler(sDriveShaft);
    }
    //--User clicks the big fat X button on their window.
    else if(tEvent.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
    {
        rGlobal->gQuit = true;
    }
    //--Order the ControlManager to update key states on the next pass.
    else if(tEvent.type == ALLEGRO_EVENT_KEY_DOWN)
    {
        al_get_keyboard_state(&sDriveShaft.KeyboardState);
        ControlManager::Fetch()->rKeyboardStatePtr = &sDriveShaft.KeyboardState;
        ControlManager::Fetch()->InformOfPress(tEvent.keyboard.keycode, -1, -1);
    }
    //--Order the ControlManager to update key states on the next pass.
    else if(tEvent.type == ALLEGRO_EVENT_KEY_UP)
    {
        al_get_keyboard_state(&sDriveShaft.KeyboardState);
        ControlManager::Fetch()->SetShiftFlag(tEvent.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT);
        ControlManager::Fetch()->rKeyboardStatePtr = &sDriveShaft.KeyboardState;
    }
    //--Specifies the position of the mouse to the ControlManager, rather than polling it.
    else if(tEvent.type == ALLEGRO_EVENT_MOUSE_AXES || tEvent.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY)
    {
        ControlManager::Fetch()->SetMouseCoordsByDisplay(tEvent.mouse.x, tEvent.mouse.y, tEvent.mouse.z);
    }
    //--Order the ControlManager to update mouse button states on the next pass. Likewise, the event
    //  is checked against the GUI.  If it is not caught by the UI, it may be passed down to the
    //  game entities to check mouse position for clicks.
    else if(tEvent.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
    {
        //--Controls
        al_get_mouse_state(&sDriveShaft.MouseState);
        ControlManager::Fetch()->rMouseStatePtr = &sDriveShaft.MouseState;
        ControlManager::Fetch()->InformOfPress(-1, tEvent.mouse.button, -1);
    }
    //--Order the ControlManager to update mouse button states on the next pass. Likewise, the event
    //  is checked against the GUI.  If it is not caught by the UI, it may be passed down to the
    //  game entities to check mouse position for clicks.
    else if(tEvent.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
    {
        //--Controls
        al_get_mouse_state(&sDriveShaft.MouseState);
        ControlManager::Fetch()->rMouseStatePtr = &sDriveShaft.MouseState;
    }
    //--Order the ControlManager to update Joystick button states on the next pass. These event
    //  types are not checked if the gJoystick was never booted.
    else if(tEvent.type == ALLEGRO_EVENT_JOYSTICK_AXIS || tEvent.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN || tEvent.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP)
    {
        //--Tell Allegro it needs to check the joystick states.
        ControlManager::Fetch()->InformAllegroOfJoyAxis();

        //--Joypad button down.
        if(tEvent.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
        {
            ControlManager::Fetch()->InformOfPress(-2, -2, tEvent.joystick.button);
        }
        //--Release case.
        else if(tEvent.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP)
        {
            ControlManager::Fetch()->InformOfRelease(-2, -2, tEvent.joystick.button);
        }
        //--Axis. Passes all -1's but still informs of a keypress.
        else if(tEvent.type == ALLEGRO_EVENT_JOYSTICK_AXIS)
        {
            //--Resolve the code based on the stick/axis/position. Note that a press event for the negative
            //  axis will trigger a release event for the positive axis.
            int tNegativeCode = (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * tEvent.joystick.stick) + (JOYSTICK_AXIS_CONSTANT * tEvent.joystick.axis) + 0);
            int tPositiveCode = (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * tEvent.joystick.stick) + (JOYSTICK_AXIS_CONSTANT * tEvent.joystick.axis) + 1);

            //--Press event for the negative code.
            if(tEvent.joystick.pos < -0.30f)
            {
                ControlManager::Fetch()->InformOfPress(-2, -2, tNegativeCode);
                ControlManager::Fetch()->InformOfRelease(-2, -2, tPositiveCode);
            }
            //--Press event for the positive code.
            else if(tEvent.joystick.pos > 0.30f)
            {
                ControlManager::Fetch()->InformOfPress(-2, -2, tPositiveCode);
                ControlManager::Fetch()->InformOfRelease(-2, -2, tNegativeCode);
            }
            //--Release for both.
            else if(tEvent.joystick.pos < 0.05 && tEvent.joystick.pos > -0.05)
            {
                ControlManager::Fetch()->InformOfRelease(-2, -2, tPositiveCode);
                ControlManager::Fetch()->InformOfRelease(-2, -2, tNegativeCode);
            }

            //--Debug.
            //fprintf(stderr, "Axis event %i %i %f.\n", tEvent.joystick.stick, tEvent.joystick.axis, tEvent.joystick.pos);
        }
    }

    //--Debug
    DebugPop("[Event Handler] Complete\n");
}

///==================================== Logic Tick Handler ========================================
void GameGear::LogicHandler(DriveShaft &sDriveShaft)
{
    ///--[Setup]
    //--FORM_TIMER_EXPIRED occupies such a large amount of game logic that it gets its own function.
    GLOBAL *rGlobal = Global::Shared();
    DebugPush(AL_GEAR_LOGIC, "[Timer] Begin\n");

    //--Fast-access pointers.
    ControlManager *rControlManager = rGlobal->gControlManager;
    MapManager *rMapManager = rGlobal->gMapManager;
    NetworkManager *rNetworkManager = rGlobal->gNetworkManager;

    //--Store when this logic tick started. Logic ticks have 1/60 seconds to operate.
    rGlobal->gTickStartTime = GetGameTime();

    //--Store the "End time" for the tick. This is not actually the end time, it's a warning to subroutines that they need to pack it
    //  in and stop doing stuff because they're hitting the outer edge of what they should do.
    //--For example, enemy pathing will add random offsets to make the AI stop running pathing if it gets too close to the end of the
    //  tick when running pathing.
    rGlobal->gTickWarnTime = rGlobal->gTickStartTime + (1.0f / rGlobal->gExpectedFPS * 0.85);

    //--Zero this global.
    TilemapActor::xActorsPathingThisTick = 0;

    ///--[Network]
    //--Network logic goes first.
    DebugPrint("Network\n");
    rNetworkManager->Update();

    ///--[Control Input]
    //--Controls are only actually updated here, instead of during keypress logics.  If no keys
    //  were pressed since the last logic tick, nothing happens.
    DebugPrint("Controls\n");
    rControlManager->Update(true, false);
    rControlManager->CheckHotkeys();

    ///--[Load Interrupt Keypress]
    //--If the load interrupt is waiting for keypresses, that handles here.
    StarLoadInterrupt *rStarLoadInterrupt = StarLoadInterrupt::Fetch();
    if(rStarLoadInterrupt && rStarLoadInterrupt->IsAwaitingKeypress() && StarBitmap::xInterruptCall)
    {
        StarBitmap::xInterruptCall();
        if(rControlManager->IsAnyKeyPressed()) rStarLoadInterrupt->SetKeypressFlag(false);
        DebugPop("[Timer] Complete by Interrupt\n");
        return;
    }

    ///--[Maps]
    DebugPrint("Map Manager\n");
    rMapManager->Update();
    if(rGlobal->gQuit) return;

    ///--[Entities]
    DebugPrint("Entity Manager\n");
    EntityManager::Fetch()->Update();

    ///--[Camera]
    DebugPrint("Camera Manager\n");
    CameraManager::Fetch()->Update();

    ///--[Finish Up]
    sDriveShaft.ScreenNeedsUpdate = true;

    //--Post-tick updates.
    DebugPrint("Post-tick.\n");
    PostLogicHandler();
    DebugManager::PopPrint("[Timer] Complete\n");
}

///==================================== Render Pass Handler =======================================
void GameGear::RenderHandler(DriveShaft &sDriveShaft)
{
    ///--[Documentation and Setup]
    //--Rendering handler, exactly what it says on the tin.
    GLOBAL *rGlobal = Global::Shared();

    //--Debug
    DebugPush(AL_GEAR_RENDER, "[GameGear Render Begin]\n");

    ///--[No-Render Checks]
    //--If the load interrupt is waiting for keypresses, no rendering occurs.
    StarLoadInterrupt *rStarLoadInterrupt = StarLoadInterrupt::Fetch();
    if(rStarLoadInterrupt && rStarLoadInterrupt->IsAwaitingKeypress())
    {
        DebugManager::PopPrint("[GameGear Render Unnecessary]\n");
        return;
    }

    //--Only renders if the event queue has no pending events, and at least one logic tick passed.
    if(!sDriveShaft.ScreenNeedsUpdate || !al_is_event_queue_empty(rGlobal->gEventQueue))
    {
        DebugManager::PopPrint("[GameGear Render Unnecessary]\n");
        return;
    }
    DebugPrint("Clearing\n");
    sDriveShaft.ScreenNeedsUpdate = false;

    ///--[GL Setup]
    //--Is there a VisualLevel? If so, the DisplayManager does not use hard letterboxes, since widescreen
    //  will render more 3D space.
    DisplayManager::xUseHardLetterbox = (VisualLevel::Fetch() == NULL);

    //--Setup GL
    DebugManager::ResetVisualTrace();
    DisplayManager::LetterboxOffsets();
    DisplayManager::StdOrtho();

    //--Draw to the front buffer directly. Bypass Allegro's backbuffer options.
    glDrawBuffer(GL_BACK);

    //--Clear the screen off
    glClearDepth(1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearStencil(0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    //--Variable reset.
    TilemapActor::xViewconeCountLastTick = TilemapActor::xViewconeCount;
    TilemapActor::xViewconeCount = 0;

    ///--[Immediate Rendering]
    if(!rGlobal->gDisplayManager->IsUsingSortedRendering())
    {
        //--Screen shaking.
        float tShakeX, tShakeY;
        MapManager::Fetch()->GetShakeFactors(tShakeX, tShakeY);
        glTranslatef(tShakeX, tShakeY, 0.0f);

        //--Render the Entities.
        DebugPrint("Rendering Entities\n");
        EntityManager::Fetch()->Render();
        DebugPrint("Rendered Entities\n");

        //--Render the foreground.
        MapManager::Fetch()->Render();

        //--Debug render calls.
        RenderDebugStrings();
        RenderFPS();
        RenderJoypadDepression();
    }
    ///--[Delayed Rendering]
    //--Rendering mode which uses a sorted depth list to render all objects in the depth order,
    //  which is back-to-front to allow proper alpha layering. This is only really useful in a
    //  2D application, since 3D graphics don't necessarily have an easily sorted depth list.
    else
    {
        //--Screen shaking.
        float tShakeX, tShakeY;
        MapManager::Fetch()->GetShakeFactors(tShakeX, tShakeY);
        glTranslatef(tShakeX, tShakeY, 0.0f);

        //--Reset the rendering list.
        StarLinkedList *rRenderingList = DisplayManager::FetchRenderingList();
        rRenderingList->ClearList();

        //--Render the Background.
        MapManager::Fetch()->AddToRenderList(rRenderingList);
        DebugPrint("Queued Map\n");

        //--Render the Entities.
        EntityManager::Fetch()->AddToRenderingList(rRenderingList);
        DebugPrint("Queued Entities\n");

        //--Sort the rendering list.
        rRenderingList->SortListUsing(&IRenderable::CompareRenderable);
        DebugPrint("Sorted Entities\n");

        //--Render all the objects on the rendering list.
        IRenderable *rRenderObject = (IRenderable *)rRenderingList->PushIterator();
        while(rRenderObject)
        {
            rRenderObject->Render();
            rRenderObject = (IRenderable *)rRenderingList->AutoIterate();
        }
        DebugPrint("Rendered all objects.\n");

        //--Debug render calls. These are technically part of the GUI.
        RenderDebugStrings();
        RenderFPS();
        RenderJoypadDepression();
    }

    ///--[Diagnostics]
    //--These render nothing if their flags aren't set.
    CutsceneManager::Fetch()->RenderDiagnostics();

    /*
    if(StarBitmap::xrActiveAtlas)
    {
        StarBitmap::xrActiveAtlas->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.00f, 0.0000f); glVertex2f(   0.0f,   0.0f);
            glTexCoord2f(0.25f, 0.0000f); glVertex2f(1024.0f,   0.0f);
            glTexCoord2f(0.25f, 0.1875f); glVertex2f(1024.0f, 768.0f);
            glTexCoord2f(0.00f, 0.1875f); glVertex2f(   0.0f, 768.0f);
        glEnd();
    }*/

    ///--[Send to Screen]
    //--In either case, send the data to the screen.
    al_flip_display();

    //--Clean
    DisplayManager::CleanOrtho();

    //--Toggle this flag.
    DisplayManager::xHasHadRenderPassSinceFullscreen = true;

    ///--[Debug]
    LogFPS(sDriveShaft);
    DebugPop("[GameGear Render Complete]\n");
}

///====================================== Worker Functions ========================================
void GameGear::ShiftTo()
{
    //--Shifts the program into GameGear
    GLOBAL *rGlobal = Global::Shared();
    rGlobal->rCurrentEventHandler  = &GameGear::EventHandler;
    rGlobal->rCurrentLogicHandler  = &GameGear::LogicHandler;
    rGlobal->rCurrentRenderHandler = &GameGear::RenderHandler;
}
void RenderDebugStrings()
{
    //--Error in the fonts.
    GLOBAL *rGlobal = Global::Shared();
    if(!rGlobal->gBitmapFont) return;

    //--Setup
    const float cScale = 0.25f;

    //--For each debug line, independent of what's on the line, render...
    for(int i = 0; i < DEBUG_LINES_TOTAL; i ++)
    {
        //--Make sure there's something to render.
        if(!rGlobal->gDebugPrintStrings[i]) continue;

        //--Position/Scale
        glTranslatef(0.0f, 40.0f * cScale * (i+1), 0.0f);
        glScalef(cScale, -cScale, 1.0f);

        //--Render
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, rGlobal->gDebugPrintStrings[i]);

        //--Clean
        glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
        glTranslatef(0.0f * -1.0f, -40.0f * cScale * (i+1), 0.0f);
    }
}
void RenderFPS()
{
    //--Render the current FPS in the top right, averaged over a few seconds by the logger. This can
    //  be disabled with a define change.
    #ifndef DBG_SHOW_FPS
        return;
    #endif
    return;

    //--Error in the fonts.
    GLOBAL *rGlobal = Global::Shared();
    if(!rGlobal->gBitmapFont) return;

    //--Setup
    float tRenderX = VIRTUAL_CANVAS_X - 40.0f;
    const float cScale = 0.25f;
    char tBufferFPS[32];
    sprintf(tBufferFPS, "%04.1f", rGlobal->gActualFPS);

    //--Position/Scale
    glTranslatef(tRenderX, 40.0f * cScale, 0.0f);
    glScalef(cScale, -cScale, 1.0f);

    //--Render
    rGlobal->gBitmapFont->DrawText(0.0f,   0.0f, SUGARFONT_AUTOCENTER_X, tBufferFPS);

    //--Clean
    glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
    glTranslatef(tRenderX * -1.0f, -40.0f * cScale, 0.0f);
}
void RenderJoypadDepression()
{
    //--Renders text in the bottom left of the canvas indicating which joypad keys are currently down.
    //  This can be disabled with a define change.
    #ifndef DBG_SHOW_JOYPAD_DEPRESSION
        return;
    #endif

    //--Setup
    GLOBAL *rGlobal = Global::Shared();
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Buffering
    char tNumBuf[8];
    char tNameBuf[32];
    char tBuffer[STD_MAX_LETTERS];
    strcpy(tBuffer, "Joypad: ");

    //--Iterate across. If no joypad is hooked up, nothing will print.
    ALLEGRO_JOYSTICK *rJoystick = al_get_joystick(0);
    if(rJoystick)
    {
        //--If a key is down, it gets appended. If it's not down, it is ignored.
        for(int i = 0; i < al_get_joystick_num_buttons(rJoystick); i ++)
        {
            sprintf(tNameBuf, "DBGJoy%02i", i);
            if(rControlManager->GetControlState(tNameBuf)->mIsDown)
            {
                sprintf(tNumBuf, "%i, ", i);
                strcat(tBuffer, tNumBuf);
            }
        }

        //--Check sticks as well.
        for(int i = 0; i < al_get_joystick_num_sticks(rJoystick); i ++)
        {
            for(int p = 0; p < 4; p ++)
            {
                int tIndex = JOYSTICK_OFFSET_STICK + (i * JOYSTICK_STICK_CONSTANT)  + p;
                sprintf(tNameBuf, "DBGStk%03i", tIndex);
                if(rControlManager->GetControlState(tNameBuf)->mIsDown)
                {
                    sprintf(tNumBuf, "%i, ", tIndex);
                    strcat(tBuffer, tNumBuf);
                }
            }
        }
    }

    //--Remove the last ','
    if(tBuffer[8] != '\0')
    {
        uint32_t tLen = strlen(tBuffer);
        tBuffer[tLen-2] = '\0';
    }

    //--Rendering
    const float cScale = 0.25f;
    float tRenderY = VIRTUAL_CANVAS_Y - 5.0f;
    glColor3f(1.0f, 0.0f, 0.0f);
    glTranslatef(0.0f, tRenderY, 0.0f);
    glScalef(cScale, -cScale, 1.0f);
    rGlobal->gBitmapFont->DrawText( 0.0f, 0.0f, 0, tBuffer);

    //--Clean
    glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
    glTranslatef(0.0f, tRenderY, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
}
#endif
