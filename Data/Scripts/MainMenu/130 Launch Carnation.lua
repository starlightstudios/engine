-- |[ ================================ Launch Project Carnation ================================ ]|
--Hotboot of Pandemonium then into project Carnation immediately.
local zAdventureGameEntry = SysPaths:fnGetGameEntryValid("Adventure Mode")
if(zAdventureGameEntry == nil) then return end

--Variables.
local sModInternalName = "Carnation"
local sModAutorunName  = "Carnation"

-- |[ ==================================== Mod Registration ==================================== ]|
--Run this subroutine to scan for mods. The global gsModDirectories/gsModNames will be populated
-- with mod information.
fnScanMods("../adventure/Games/AdventureMode/")
    
-- |[ ==================================== Mod Check / Boot ==================================== ]|
-- |[Mod Boot]|
--Find mod slot.
local iGameSlot = -1
for i = 1, #gsModNames, 1 do
    if(gsModNames[i] == sModInternalName) then
        fnMarkAutorunAsFound(sModAutorunName)
        iGameSlot = i
        break
    end
end

--Mod not found.
if(iGameSlot == -1) then 
    return
end

--Run Pandemonium.
LM_ExecuteScript(zAdventureGameEntry.sActivePath)

--Assemble the boot script name.
local sModBoothPath = gsModDirectories[iGameSlot] .. "System/000 Mod Setup.lua"

--Call the mod boot script.
gbIsStartingChapter = true
LM_ExecuteScript(sModBoothPath)
gbIsStartingChapter = false

-- |[ ==================================== Additional Setup ==================================== ]|
--Re-run combat animations.
LM_ExecuteScript(gsRoot .. "Combat/Animations/ZRouting.lua")
