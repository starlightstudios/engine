///======================================== CarnUIEquip ===========================================
//--Instance of the equip UI for Carnation. Has extra mouse features and uses new rendering handlers.

#pragma once

///========================================= Includes =============================================
#include "AdvUIEquip.h"
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define CARNUIEQP_PROFICIENCY_SLOTS 12

///========================================== Classes =============================================
class CarnUIEquip : public AdvUIEquip, public CarnUICommon
{
    private:
    ///--[Constants]
    //--Scrollbar.
    static const int cxCarnScrollBuf = 3;
    static const int cxCarnScrollPage = 12;

    //--Timers
    static const int cxUnequipCursedTicks = 15;

    //--Hitboxes.
    static const int cxSelectBoxMin  = 0;
    static const int cxSelectBoxMax  = 11;
    static const int cxReplaceBoxMin = 12;
    static const int cxReplaceBoxMax = 23;
    static const int cxScrollMin     = 24;
    static const int cxScrollBod     = 25;
    static const int cxScrollMax     = 26;
    static const int cxConfirm       = 27;
    static const int cxCancel        = 28;
    static const int cxModMin        = 29;
    static const int cxModMax        = 40;
    static const int cxBtnToggleDesc = 41;

    ///--[System]
    bool mConfirmUnequipCursed;
    int mConfirmUnequipCursedTimer;
    int mConfirmStoredSlot;

    ///--[Proficiency Data]
    int mRecheckProficienciesAfterOpen;
    StarBitmap *rStoredProficiencyImages[CARNUIEQP_PROFICIENCY_SLOTS];
    StarLinkedList *mrReplacementProficiencyImages; //StarBitmap *, reference

    ///--[Description]
    bool mIsDescriptionMode;
    char *mHighlightName;
    StarLinkedList *mDescription; //char *, master

    ///--[Hitboxes]
    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Name;
        StarFont *rFont_ConfirmHeader;
        StarFont *rFont_Header;
        StarFont *rFont_Description;
        StarFont *rFont_Statistics;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Button;
        StarBitmap *rFrame_Confirm;
        StarBitmap *rFrame_Modify;
        StarBitmap *rFrame_Proficiency_Untrained;
        StarBitmap *rFrame_Proficiency_Trained;
        StarBitmap *rFrame_Proficiency_Skilled;
        StarBitmap *rFrame_Proficiency_Master;
        StarBitmap *rFrame_Proficiency_Grandmaster;
        StarBitmap *rOverlay_StatBase;
        StarBitmap *rOverlay_DetailHeading;
        StarBitmap *rOverlay_DetailItem;
        StarBitmap *rOverlay_StatDetail;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIEquip();
    virtual ~CarnUIEquip();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    //--Selection
    void UpdateSelection();
    void HandleSelectLeftClick();
    void RenderSelectMode();
    void RenderEquipmentSlot(float pX, float pY, const char *pSlotName);
    void RenderEquipmentEntry(float pX, float pY, const char *pString, StarBitmap *pIcon);

    //--Replacement
    virtual bool ActivateReplacement();
    void UpdateReplacement();
    void HandleReplaceLeftClick();
    void RenderReplaceMode();
    void RenderStatistic(float pY, const char *pString, int pStatisticCode);

    //--Core Methods
    void RecheckProficienciesForCharacter(int pCharacterSlot);
    StarBitmap *ResolveProficiencyIconForItem(AdvCombatEntity *pCharacter, AdventureItem *pItem);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pAlpha);
    void RenderConfirmWindow(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
