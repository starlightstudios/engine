///======================================= CarnUICommon ===========================================
//--Common UI object to all Carnation UI sub-components. Note that unlike the AdvUICommon object, this
//  one is not part of the StarUIPiece inheritance series. It just contains extra variables and functions,
//  it should be inherited as an addition to an existing piece, not a replacement for it.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "StarUIMouse.h"

///===================================== Local Structures =========================================
///--[CarnationUIClass]
//--Represents data needed by a class, such as the jobs Medic or Apprentice. This data is stored in the
//  lua state but is needed to be displayed on the UI. Characters can have multiple jobs, 3 in the base
//  but this is theoretically moddable so a generic structure is used.
typedef struct CarnationUIClass
{
    //--Members
    char mName[64];
    int mClassLevel;
    int mClassXP;
    int mClassXPNext;

    //--Functions
    void Clear()
    {
        mName[0] = '\0';
        mClassLevel = 0;
        mClassXP = 0;
        mClassXPNext = 0;
    }
}CarnationUIClass;

///===================================== Local Definitions ========================================
///--[Class Data]
#define CARNUI_CLASS_TOTAL 3

///--[Menu Mode Constants]
#ifndef CARNMENU_MODE_CONSTANTS
#define CARNMENU_MODE_CONSTANTS

//--Menu modes themselves.
#define CARNMENU_MODE_STATUS 0
#define CARNMENU_MODE_EQUIP 1
#define CARNMENU_MODE_INVENTORY 2
#define CARNMENU_MODE_SKILLS 3
#define CARNMENU_MODE_OPTIONS 4
#define CARNMENU_MODE_SAVE 5
#define CARNMENU_MODE_REGEM 6
#define CARNMENU_MODE_HEALPARTY 7
#define CARNMENU_MODE_TOTAL 8

#define CARNMENU_TOP_RENDER 6

//--Backwards codes used to change menu modes.
#define CARNMENU_CHANGEMODE_OFFSET 20
#define CARNMENU_CHANGEMODE_STATUS    (CARNMENU_MODE_STATUS    + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_EQUIP     (CARNMENU_MODE_EQUIP     + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_INVENTORY (CARNMENU_MODE_INVENTORY + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_SKILLS    (CARNMENU_MODE_SKILLS    + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_OPTIONS   (CARNMENU_MODE_OPTIONS   + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_SAVE      (CARNMENU_MODE_SAVE      + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_REGEM     (CARNMENU_MODE_REGEM     + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_HEALPARTY (CARNMENU_MODE_HEALPARTY + CARNMENU_CHANGEMODE_OFFSET)

#endif

///========================================== Classes =============================================
class CarnUICommon : virtual public AdvUICommon, virtual public StarUIMouse
{
    protected:
    ///--[System]
    ///--[Constants]
    ///--[Colors]
    ///--[Mode Tabs]
    bool mAllowSaveMode;
    TwoDimensionReal mModeTabs[CARNMENU_MODE_TOTAL];

    ///--[Images]
    struct CarnImagesCommon
    {
        //--Fonts
        StarFont *rFont_Hexes;
        StarFont *rFont_Name;
        StarFont *rFont_Tabs;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Hexes;
        StarBitmap *rTab_CharGrey;
        StarBitmap *rTab_CharPink;
        StarBitmap *rTab_CharViolet;
        StarBitmap *rTab_ModeActive;
        StarBitmap *rTab_ModeInactive;
    }CarnImagesCommon;

    protected:

    public:
    //--System
    CarnUICommon();
    virtual ~CarnUICommon();

    //--Public Variables
    static int xPersistentCharacterCursor;

    //--Property Queries
    //--Manipulators
    void ZeroVisTimer();
    void FinishVisTimer();

    //--Core Methods
    int CheckTabsLeftClick(float pMouseX, float pMouseY);
    int CheckCharsLeftClick(float pMouseX, float pMouseY);
    static StarLinkedList *CreateDescriptionListFromString(const char *pString, StarFont *pFont, float pMaxWidth);
    static void ReplaceCommonTagsInList(StarLinkedList *pList);

    ///--[Class Properties]
    int CallQueryScript(const char *pSwitchType, int pArgs, ...);
    bool CheckQueryReplies(const char *pCallerName, int pNeededReplies, int pReceivedReplies);
    int GetIntSafe(const char *pVariablePath);
    const char *GetStringSafe(const char *pVariablePath);
    void QueryClassDataInto(int pIndex, const char *pCharacterName, CarnationUIClass &sCallStructure);
    const char *QueryClassNameFor(int pIndex, const char *pCharacterName);
    int QueryClassXPFor(int pIndex, const char *pCharacterName);
    int QueryClassXPNextFor(int pIndex, const char *pCharacterName);

    private:
    //--Private Core Methods
    public:
    //--Update
    bool CommonTabClickUpdate();
    bool CommonCharClickUpdate(int &sCharacterCursor);

    //--File I/O
    //--Drawing
    StarBitmap *GetTabByActivity(int pActiveMode, int pFlaggedMode);
    void RenderTopTabs(int pActiveMode);
    void RenderCharacterTabs(int pCursor);
    void RenderHexes();
    static void RenderDescription(float pX, float pY, float pH, StarLinkedList *pDescription, StarFont *pFont);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


