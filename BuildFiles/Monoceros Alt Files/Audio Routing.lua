-- |[ =================================== Music Routing File =================================== ]|
-- |[Variables]|
local sBasePath = fnResolvePath()

-- |[Registration Function]|
--Loads music with set loops.
local function fnRegisterMusic(psMusicName, psPath, pfLoopStart, pfLoopEnd)
	
	--Arg check.
	if(psMusicName == nil) then return end
	if(psPath == nil) then return end
	
	--If the pfLoopStart and/or pfLoopEnd are nil, then this doesn't loop.
	if(pfLoopStart == nil or pfLoopEnd == nil) then
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath)
		
	--Otherwise, register with looping.
	else
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath, pfLoopStart, pfLoopEnd)
	end

end

-- |[Ambient Tracks]|
local sAmbientPath = sBasePath .. "Ambient Tracks/"
fnRegisterMusic("Windy Mountain",  sAmbientPath .. "Windy Mountain.ogg",  2.000,       50.562)
fnRegisterMusic("Cave Quiet",      sAmbientPath .. "Cave Quiet.ogg",      3.000, 120 +  0.743)

-- |[Battle Themes]|
local sBattlePath = sBasePath .. "Battle Themes/"
fnRegisterMusic("CombatVictory",        sBattlePath .. "CombatVictory.ogg")
