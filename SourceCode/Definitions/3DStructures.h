///======================================== 3DStructures ===========================================
//--File defining things like Vertex and Face structures which are used in 3D. These can be used
//  in 2D as well, if you ignore the Z coordinate.
#include "StarlightColor.h"

#pragma once

///--[Vector]
//--For scanning purposes, Z is located third, even though texture coordinates use W as the third
//  value. If scanning using memcpy, make sure you take this into account.
typedef struct Vector
{
    union{ float x; float X; float U; };
    union{ float y; float Y; float V; };
    union{ float z; float Z; };
    float W;
}Vector;
typedef struct Vector Vertex;

///--[Material]
//--Materials definitions. Used for texturing.
typedef struct Material
{
    StarlightColor mAmbientColor;
    StarlightColor mDiffuseColor;
    StarlightColor mSpecularColor;
    StarlightColor mTransmissionFilter;
    float mSpecialCoefficient;
    float mAlpha;
    int mIlluminationFlag;
    float mOpticalDensity;
}Material;

///--[Face]
//--Faces construct the majority of the geometry.
typedef struct
{
    int mVerticesTotal;
    int *mVertices;
    int *mTexCoords;
    int *mNormals;
    Material *rMaterial;
}Face;

