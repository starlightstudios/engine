//--Base
#include "StarCamera.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
StarCamera::StarCamera()
{
}
StarCamera::~StarCamera()
{
}
void StarCamera::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((StarCamera *)pPtr);
}

///===================================== Property Queries =========================================
bool StarCamera::Is2DCamera()
{
    return false;
}
bool StarCamera::Is3DCamera()
{
    return false;
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void StarCamera::Clear()
{

}
void StarCamera::PositionInWorld()
{
    //--Call this to translate/rotate/scale the camera as necessary, before rendering objects.
}
void StarCamera::UnpositionInWorld()
{
    //--Call this to undo PositionInWorld(), for example, to render the UI.
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void StarCamera::Update()
{

}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
