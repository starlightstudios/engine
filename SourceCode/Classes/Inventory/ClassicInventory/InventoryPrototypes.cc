//--Base
#include "InventoryPrototypes.h"

//--Classes
#include "InventoryItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
InventoryPrototypes::InventoryPrototypes()
{
    ///--[InventoryPrototypes]
    //--System
}
InventoryPrototypes::~InventoryPrototypes()
{
}

///--[Public Statics]
StarLinkedList *InventoryPrototypes::xStorageList = new StarLinkedList(true);

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
InventoryItem *InventoryPrototypes::Fetch(const char *pName)
{
    //--Returns an InventoryItem as a clone of the original, assuming it exists. NULL if not.
    if(!pName) return NULL;

    //--Find the item.
    InventoryItem *rItem = (InventoryItem *)xStorageList->GetElementByName(pName);
    if(!rItem) return NULL;

    //--Return a clone. Heap-allocated.
    return rItem->Clone();
}
InventoryItem *InventoryPrototypes::FetchPrototype(const char *pName)
{
    //--Returns the InventoryItem prototype that others are cloned from. This should be considered
    //  read-only for most purposes. NULL is returned if it didn't exist.
    if(!pName) return NULL;

    //--Find the item.
    InventoryItem *rItem = (InventoryItem *)xStorageList->GetElementByName(pName);
    if(!rItem) return NULL;

    //--Return the prototype.
    return rItem;
}
void InventoryPrototypes::Upload(const char *pName, InventoryItem *pPrototype)
{
    //--Uploads an InventoryItem to the static list. Takes ownership of the pointer.
    if(!pName || !pPrototype) return;
    xStorageList->AddElement(pName, pPrototype, &InventoryItem::DeleteThis);
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
