//--Base
#include "AdvUIGrid.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
AdvUIGrid::AdvUIGrid()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ====== AdvUIGrid ======= ]
    ///--[External Constants]
    cCursorMoveTicks = 15;
    cGridTimerMin = 0;
    cGridTimerMax = 15;
    cGridSizeBoost = 0.50f;
    cGridIconScale = 1.0f;
    cGridNameColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

    ///--[System]
    mPulseValue = 0;

    ///--[Cursor]
    mGridCurrentX = 0;
    mGridCurrentY = 0;
    mGridCenterX = 0;
    mGridCenterY = 0;

    ///--[Positioning]
    mGridRenderCenterX = 0.0f;
    mGridRenderCenterY = 0.0f;
    mGridRenderSpacingX = 0.0f;
    mGridRenderSpacingY = 0.0f;

    ///--[Grid Storage]
    //--Active Grid.
    mGridSizeX = 0;
    mGridSizeY = 0;
    mGridEntries = NULL;

    //--List of stored grids, excluding active grid.
    mActiveGridName = InitializeString("Base");
    mStoredGridList = new StarLinkedList(true);

    ///--[Rendering]
    rFont_Name = NULL;

    ///--[ ================ Construction ================ ]
}
AdvUIGrid::~AdvUIGrid()
{
    for(int x = 0; x < mGridSizeX; x ++)
    {
        for(int y = 0; y < mGridSizeY; y ++)
        {
            free(mGridEntries[x][y]);
        }
        free(mGridEntries[x]);
    }
    free(mGridEntries);
    free(mActiveGridName);
    delete mStoredGridList;
}

///===================================== Property Queries =========================================
int AdvUIGrid::GetGridCodeAt(int pX, int pY)
{
    ///--[Documentation]
    //--Returns the code of the grid entry at the given X/Y coordinates, or -1 on error.
    if(pX < 0 || pY < 0) return GRID_CODE_NONE;
    if(pX >= mGridSizeX || pY >= mGridSizeY) return GRID_CODE_NONE;

    //--Return entry.
    return mGridEntries[pX][pY]->mCode;
}
int AdvUIGrid::GetGridCodeAtCursor()
{
    ///--[Documentation]
    //--Uses the internal cursor values to quickly return the grid code.
    return GetGridCodeAt(mGridCurrentX, mGridCurrentY);
}
GridPackage *AdvUIGrid::GetNextPriorityEntry()
{
    ///--[Documentation]
    //--Returns the next entry, in order of priority, from a newly allocated grid. This entry can then
    //  be populated with data, in particular its ->mCode value. If mCode is not -1 then the entry
    //  is populated and will not be returned by a future call.
    //--This routine must be called after ProcessGrid() has set positions, else the distance values
    //  will be incorrect and entries will be placed in the wrong order.
    //--If there are no further entries available, returns NULL.

    ///--[List Creation]
    //--Get the X/Y position of the center entry.
    GridPackage *rCenterPack = GetPackageAt(mGridCenterX, mGridCenterY);
    if(!rCenterPack) return NULL;

    //--Store.
    float cCenterX = rCenterPack->mXPos;
    float cCenterY = rCenterPack->mYPos;

    //--Create a linked list that stores all the grid entries.
    float tShortestDistance = 10000.0f;
    GridPackage *rClosestPack = NULL;

    //--Iterate across all entries. If the entry has a code of -1, its distance can be checked.
    for(int x = 0; x < mGridSizeX; x ++)
    {
        for(int y = 0; y < mGridSizeY; y ++)
        {
            if(mGridEntries[x][y]->mCode == -1)
            {
                float tCheckDistance = GetPlanarDistance(cCenterX, cCenterY, mGridEntries[x][y]->mXPos, mGridEntries[x][y]->mYPos);
                if(tCheckDistance < tShortestDistance)
                {
                    tShortestDistance = tCheckDistance;
                    rClosestPack = mGridEntries[x][y];
                }
            }
        }
    }

    ///--[Return]
    //--Whatever package was closest to the center is used. If no packages were available, this will be NULL.
    return rClosestPack;
}

///======================================= Manipulators ===========================================
void AdvUIGrid::AllocateGrid(int pSizeX, int pSizeY)
{
    ///--[Documentation]
    //--Given a number of entries, deallocates, clears, and reallocates the class to handle the new
    //  grid size. To clear and deallocate the class, pass 0 for either size.
    //--Always implicitly resets the cursor position to 0 as well. If allocating, the program will
    //  attempt to set it to the "middle" slot, rounded down.
    mGridCurrentX = 0;
    mGridCurrentY = 0;

    ///--[Deallocate]
    for(int x = 0; x < mGridSizeX; x ++)
    {
        for(int y = 0; y < mGridSizeY; y ++)
        {
            free(mGridEntries[x][y]);
        }
        free(mGridEntries[x]);
    }
    free(mGridEntries);

    ///--[Clear]
    mGridSizeX = 0;
    mGridSizeY = 0;
    mGridEntries = NULL;
    if(pSizeX < 1 || pSizeY < 1) return;

    ///--[Allocate]
    //--Allocate space.
    mGridSizeX = pSizeX;
    mGridSizeY = pSizeY;
    mGridEntries = (GridPackage ***)starmemoryalloc(sizeof(GridPackage **) * mGridSizeX);
    for(int x = 0; x < mGridSizeX; x ++)
    {
        mGridEntries[x] = (GridPackage **)starmemoryalloc(sizeof(GridPackage *) * mGridSizeY);
        for(int y = 0; y < mGridSizeY; y ++)
        {
            mGridEntries[x][y] = (GridPackage *)starmemoryalloc(sizeof(GridPackage));
            mGridEntries[x][y]->Initialize();
        }
    }

    //--Set the cursor to the center. This rounds down.
    mGridCenterX = mGridSizeX / 2;
    mGridCenterY = mGridSizeY / 2;
    mGridCurrentX = mGridCenterX;
    mGridCurrentY = mGridCenterY;
}
void AdvUIGrid::ProcessGrid()
{
    ///--[Documentation]
    //--After AllocateGrid() is called, the programmer should set the grid positions either by manually
    //  entering in the grid information (such as for the base UI) or auto-generating it (costumes, warp).
    //  Once that is done, call this function to create the control lookups and sizings for each grid element.
    for(int x = 0; x < mGridSizeX; x ++)
    {
        for(int y = 0; y < mGridSizeY; y ++)
        {
            //--Determine where this entry is relative to the center. mGridCenterX/Y can be manually adjusted
            //  to change the centering of the grid if it is not an odd number of entries.
            int tRelativeX = x - mGridCenterX;
            int tRelativeY = y - mGridCenterY;

            //--Determine position.
            mGridEntries[x][y]->mXPos = mGridRenderCenterX + (tRelativeX * mGridRenderSpacingX * 1.00f);
            mGridEntries[x][y]->mYPos = mGridRenderCenterY + (tRelativeY * mGridRenderSpacingY * 1.00f);
        }
    }
}
void AdvUIGrid::QuicksetGridEntry(int pX, int pY, int pCode, const char *pName, StarBitmap *pImagePtr)
{
    ///--[Documentation]
    //--Function that allows the coder to quickly set what is in a grid entry. The most common properties
    //  are its calling code, name, and image. Image sizes are resolved from the image, if it exists.

    ///--[Resolve]
    //--Get and check the package. If it doesn't exist, print a warning.
    GridPackage *rPackage = GetPackageAt(pX, pY);
    if(!rPackage)
    {
        fprintf(stderr, "AdvUIGrid:QuicksetGridEntry() - Warning. Attempted to set package %i %i to %s, but package was not found.\n", pX, pY, pName);
        return;
    }

    ///--[Set]
    //--Basic variables.
    rPackage->mCode = pCode;
    strcpy(rPackage->mLocalName, pName);
    rPackage->mAlignments.Initialize();
    rPackage->mAlignments.rImage = pImagePtr;

    //--The image sizes default to the image's width and height, assuming it exists.
    if(pImagePtr)
    {
        rPackage->mAlignments.mImageW = pImagePtr->GetWidth();
        rPackage->mAlignments.mImageH = pImagePtr->GetHeight();
    }
}
void AdvUIGrid::SetCursorByCode(int pCode)
{
    ///--[Documentation]
    //--Scans the grid for the given code. If found, sets the cursor variables to that entry, otherwise
    //  does nothing. Does not print warnings or play sounds.
    for(int x = 0; x < mGridSizeX; x ++)
    {
        for(int y = 0; y < mGridSizeY; y ++)
        {
            if(mGridEntries[x][y]->mCode == pCode)
            {
                mGridCurrentX = x;
                mGridCurrentY = y;
                return;
            }
        }
    }
}
void AdvUIGrid::CreateStoredGrid(const char *pName)
{
    ///--[Documentation]
    //--Creates a storage grid which starts empty. This allows a single object to have multiple grids
    //  at the same time.
    //--Disallows the creation of a grid named "Base", as this is the name that the base grid will take
    //  when another grid is activated.
    if(!pName || !strcasecmp(pName, "Base")) return;

    ///--[Execution]
    StoredGrid *nGrid = (StoredGrid *)starmemoryalloc(sizeof(StoredGrid));
    nGrid->Initialize();
    mStoredGridList->AddElement(pName, nGrid, &StoredGrid::DeleteThis);
}
void AdvUIGrid::ActivateStoredGrid(const char *pName)
{
    ///--[Documentation]
    //--Switches the currently active grid with the one at the provided name, allowing multiple grids
    //  to be used within the same object. If the active grid is the name provided, does nothing.
    //--The grid is removed from the list and placed in storage. The base grid that the object starts
    //  with always has the name "Base".
    if(!pName) return;

    ///--[Error Checks]
    //--If the activation and current name are the same, do nothing.
    if(!strcasecmp(pName, mActiveGridName)) return;

    //--Attempt to locate the stored grid. If it's not found, fail.
    StoredGrid *rArchivedGrid = (StoredGrid *)mStoredGridList->GetElementByName(pName);
    if(!rArchivedGrid) return;

    ///--[Activate]
    //--Create a new grid entry and load the current one into that object.
    StoredGrid *nGrid = (StoredGrid *)starmemoryalloc(sizeof(StoredGrid));
    nGrid->Initialize();
    nGrid->mCursorX = mGridCurrentX;
    nGrid->mCursorY = mGridCurrentY;
    nGrid->mGridSizeX = mGridSizeX;
    nGrid->mGridSizeY = mGridSizeY;
    nGrid->mGridEntries = mGridEntries;
    mStoredGridList->AddElement(mActiveGridName, nGrid, &StoredGrid::DeleteThis);

    //--Reset the active name.
    ResetString(mActiveGridName, pName);

    //--Liberate the requested entry and fill in the grid data.
    mStoredGridList->SetRandomPointerToThis(rArchivedGrid);
    mStoredGridList->LiberateRandomPointerEntry();
    mGridCurrentX = rArchivedGrid->mCursorX;
    mGridCurrentY = rArchivedGrid->mCursorY;
    mGridSizeX = rArchivedGrid->mGridSizeX;
    mGridSizeY = rArchivedGrid->mGridSizeY;
    mGridEntries = rArchivedGrid->mGridEntries;
}
void AdvUIGrid::ClearStoredGrids()
{
    ///--[Documentation]
    //--Clears all stored grid data.
    mStoredGridList->ClearList();
}

///======================================= Core Methods ===========================================
void AdvUIGrid::AutoAllocateSquareGrid(int pElements)
{
    ///--[Documentation]
    //--Given a number of elements, attempts to create a square grid to house the elements. A square
    //  grid places the highest priority element in the middle and lowest priority in the corners.

    ///--[Allocation]
    //--If no elements are provided, clear.
    if(pElements < 1)
    {
        AllocateGrid(0, 0);
    }
    //--Single element, create a 1x1 grid.
    else if(pElements == 1)
    {
        AllocateGrid(1, 1);
    }
    //--6 entries, 3x2 grid.
    else if(pElements <= 6)
    {
        AllocateGrid(3, 2);
    }
    //--3x3 grid. The ideal access points are orthogonal from the middle.
    else if(pElements <= 9)
    {
        AllocateGrid(3, 3);
    }
    //--To 15 entries, the grid pays extra heed to left/right as there is more screen space horizontally than vertically.
    else if(pElements <= 15)
    {
        AllocateGrid(5, 3);
    }
    //--Engine will probably need new code to handle larger grids or to switch to a conventional list format.
    else
    {
        fprintf(stderr, "AdvUIGrid:AutoAllocateSquareGrid() - Warning, grid sizes over 15 are not handled.\n");
        AllocateGrid(7, 3);
    }
}
void AdvUIGrid::RecomputeGridCursorPosition(EasingPack2D &sHighlightPos, EasingPack2D &sHighlightSize, float pVisPercent)
{
    ///--[Documentation]
    //--Using local values, determines where the cursor should be on the grid and sets the provided
    //  easing packs to that value. Note that this does not take into account external vis alphas,
    //  as grid rendering squishes and resizes things as they enter and exit. This provides the final
    //  positions and sizes.

    ///--[Constants]
    float cBuffer   =  0.0f;
    float cIconSize = 96.0f;

    ///--[Get Grid Package]
    //--Figure out the render position.
    GridPackage *rUsePackage = GetPackageAtCursor();
    if(!rUsePackage) return;

    ///--[Get Render Position]
    //--Variables.
    float cScale = 1.0f + (1.0f * cGridSizeBoost);

    //--Starting position.
    float tXPos = rUsePackage->mXPos;
    float tYPos = rUsePackage->mYPos;

    //--The actual render position is based on the opacity percent as the object slides out of the middle.
    tXPos = mGridRenderCenterX + ((tXPos - mGridRenderCenterX) * pVisPercent);
    tYPos = mGridRenderCenterY + ((tYPos - mGridRenderCenterY) * pVisPercent);

    //--Move the render position off by the scale of the object.
    tXPos = tXPos - (cIconSize * cScale * 0.50f);
    tYPos = tYPos - (cIconSize * cScale * 0.50f);

    //--Positions.
    float cLft = tXPos - cBuffer;
    float cTop = tYPos - cBuffer;
    float cRgt = tXPos + (cIconSize * cScale) + (cBuffer * 2.0f);
    float cBot = tYPos + (cIconSize * cScale) + (cBuffer * 2.0f);

    ///--[Set Cursor]
    //--Resize the cursor.
    sHighlightPos. MoveTo(cLft,        cTop,        cCursorMoveTicks);
    sHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cCursorMoveTicks);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIGrid::UpdateGridControls()
{
    ///--[Documentation]
    //--Allows the grid cursor to move based on pressing up/left/right/down. Returns true if it handled
    //  an input, false otherwise.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Keys]
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Check the destination. If the result's code is a -1, fail.
        if(GetGridCodeAt(mGridCurrentX, mGridCurrentY-1) == GRID_CODE_NONE) return true;

        //--Move.
        mGridCurrentY --;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        //--Check the destination. If the result's code is a -1, fail.
        if(GetGridCodeAt(mGridCurrentX, mGridCurrentY+1) == GRID_CODE_NONE) return true;

        //--Move.
        mGridCurrentY ++;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Check the destination. If the result's code is a -1, fail.
        if(GetGridCodeAt(mGridCurrentX-1, mGridCurrentY) == GRID_CODE_NONE) return true;

        //--Move.
        mGridCurrentX --;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        //--Check the destination. If the result's code is a -1, fail.
        if(GetGridCodeAt(mGridCurrentX+1, mGridCurrentY) == GRID_CODE_NONE) return true;

        //--Move.
        mGridCurrentX ++;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[No Input]
    return false;
}
void AdvUIGrid::UpdateGridTimers()
{
    ///--[Documentation]
    //--For each grid element, decrements the unselected timers and increments the selected timers.
    for(int x = 0; x < mGridSizeX; x ++)
    {
        for(int y = 0; y < mGridSizeY; y ++)
        {
            //--Selected.
            if(mGridCurrentX == x && mGridCurrentY == y)
            {
                if(mGridEntries[x][y]->mTimer < cGridTimerMax) mGridEntries[x][y]->mTimer ++;
            }
            //--Unselected.
            else
            {
                if(mGridEntries[x][y]->mTimer > cGridTimerMin) mGridEntries[x][y]->mTimer --;
            }
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
bool AdvUIGrid::ResolveEntryRenderData(GridPackage *pEntry, float &sXPos, float &sYPos, float &sScale, float &sBoostPct, float pVisAlpha)
{
    ///--[Documentation]
    //--Resolves and returns, via shared variables, the render information that an entry uses. This
    //  allows different routines to share render properties.
    //--Returns true if everything worked, false on error. Specifically, returning false means that
    //  the variables are not initialized and may cause divide by zero errors.
    if(!pEntry) return false;

    //--Constants.
    float cIconSize = 96.0f;

    //--Determine the rendering size. This also affects color blending.
    sBoostPct = EasingFunction::QuadraticInOut(pEntry->mTimer, cGridTimerMax);
    sScale = cGridIconScale + (sBoostPct * cGridSizeBoost);

    //--Figure out the render position.
    sXPos = pEntry->mXPos;
    sYPos = pEntry->mYPos;

    //--The actual render position is based on the opacity percent as the object slides out of the middle.
    sXPos = mGridRenderCenterX + ((sXPos - mGridRenderCenterX) * pVisAlpha);
    sYPos = mGridRenderCenterY + ((sYPos - mGridRenderCenterY) * pVisAlpha);

    //--Move the render position off by the scale of the object.
    sXPos = sXPos - (cIconSize * sScale * 0.50f);
    sYPos = sYPos - (cIconSize * sScale * 0.50f);

    //--Normal exit.
    return true;
}
void AdvUIGrid::RenderGrid(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the entries on the grid. This is the icons first followed by the associated names if
    //  present. Names are rendered later as the cursor typically underlays the name.
    for(int x = 0; x < mGridSizeX; x ++)
    {
        for(int y = 0; y < mGridSizeY; y ++)
        {
            RenderGridEntryIcon(mGridEntries[x][y], pVisAlpha);
        }
    }
}
void AdvUIGrid::RenderGridEntryIcon(GridPackage *pEntry, float pVisAlpha)
{
    ///--[Documentation]
    //--Individual renderer for a grid element. Renders the associated icon.
    if(!pEntry || !pEntry->mAlignments.rImage) return;

    ///--[Computations]
    //--Constants.
    float cCenterSize = 96.0f;

    //--Subroutine handles most of the work.
    float tXPos, tYPos, cScale, cBoostPct;
    if(!ResolveEntryRenderData(pEntry, tXPos, tYPos, cScale, cBoostPct, pVisAlpha)) return;

    //--Derived values.
    float cScaleInv = 1.0f / cScale;

    ///--[Position and Color]
    //--Color blending.
    float cColVal = 0.75f + (0.25f * cBoostPct);
    StarlightColor::SetMixer(cColVal, cColVal, cColVal, pVisAlpha);

    //--Position, scale.
    glTranslatef(tXPos, tYPos, 0.0f);
    glScalef(cScale, cScale, 1.0f);

    ///--[Rendering]
    //--Render icon. The alignment code is designed to allow larger images to partially render so as to meet
    //  sizing requirements on the grid.
    StarBitmap *rImage = pEntry->mAlignments.rImage;

    //--Resolve percentages.
    float cWid = rImage->GetWidthSafe();
    float cHei = rImage->GetHeightSafe();
    float cLft = pEntry->mAlignments.mImageX / cWid;
    float cTop = pEntry->mAlignments.mImageY / cHei;
    float cRgt = cLft + (pEntry->mAlignments.mImageW / cWid);
    float cBot = cTop + (pEntry->mAlignments.mImageH / cHei);

    //--Scale down.
    float cNeededScale = cCenterSize / pEntry->mAlignments.mImageW;
    float cNeededScaleInv = 1.0f / cNeededScale;

    //--X/Y Position.
    float cXPosition = pEntry->mAlignments.mOffsetX;
    float cYPosition = pEntry->mAlignments.mOffsetY;

    //--Bind the image.
    glTranslatef(cXPosition, cYPosition, 0.0f);
    glScalef(cNeededScale, cNeededScale, 1.0f);
    rImage->RenderPercent(rImage->GetXOffset() * -1.0f, rImage->GetYOffset() * -1.0f, cLft, cTop, cRgt, cBot);
    glScalef(cNeededScaleInv, cNeededScaleInv, 1.0f);
    glTranslatef(cXPosition * -1.0f, cYPosition * -1.0f, 0.0f);

    ///--[Clean]
    //--Unscale, unposition.
    glScalef(cScaleInv, cScaleInv, 1.0f);
    glTranslatef(-tXPos, -tYPos, 0.0f);

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
}
void AdvUIGrid::RenderGridEntryName(GridPackage *pEntry, float pVisAlpha)
{
    ///--[Documentation]
    //--Individual renderer for a grid element.
    if(!pEntry || !rFont_Name) return;

    ///--[Computations]
    //--Subroutine handles most of the work.
    float tXPos, tYPos, cScale, cBoostPct;
    if(!ResolveEntryRenderData(pEntry, tXPos, tYPos, cScale, cBoostPct, pVisAlpha)) return;

    //--Derived values.
    float cScaleInv = 1.0f / cScale;

    ///--[Position and Color]
    //--Position, scale.
    glTranslatef(tXPos, tYPos, 0.0f);
    glScalef(cScale, cScale, 1.0f);

    ///--[Rendering]
    //--Compute position.
    float cNameXOff = 48.0f;
    float cLowerYOff = 78.0f;
    float cTxtScale = 0.80f;
    float cTxtScaleInv = 1.0f / cTxtScale;

    //--Color.
    cGridNameColor.SetAsMixerAlpha(pVisAlpha);

    //--Position, scale.
    glTranslatef(cNameXOff, cLowerYOff, 0.0f);
    glScalef(cTxtScale, cTxtScale, 1.0f);

    //--Render.
    rFont_Name->DrawText(0.0f, 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, pEntry->mLocalName);

    //--Unscale, unposition.
    glScalef(cTxtScaleInv, cTxtScaleInv, 1.0f);
    glTranslatef(-cNameXOff, -cLowerYOff, 0.0f);

    ///--[Clean]
    //--Unscale, unposition.
    glScalef(cScaleInv, cScaleInv, 1.0f);
    glTranslatef(-tXPos, -tYPos, 0.0f);

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
}

///====================================== Pointer Routing =========================================
GridPackage *AdvUIGrid::GetPackageAt(int pX, int pY)
{
    ///--[Documentation]
    //--Returns the grid entry at the given X/Y coordinates, or NULL on error.
    if(pX <           0 || pY <           0) return NULL;
    if(pX >= mGridSizeX || pY >= mGridSizeY) return NULL;

    //--Return entry.
    return mGridEntries[pX][pY];
}
GridPackage *AdvUIGrid::GetPackageAtCursor()
{
    ///--[Documentation]
    //--Returns the grid entry at the cursor.
    return GetPackageAt(mGridCurrentX, mGridCurrentY);
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
