-- |[ =============================== ObjectPrototype Class File =============================== ]|
--Class file for ObjectPrototype. Call this once at initialization.

--This is a namespacing class with a few global variables and functions. It is not designed to be
-- instantiated.

-- |[ =========== Globals/Statics ============ ]|
--Execution constants for prototype scripts.
ObjectPrototype = {}
ObjectPrototype.ciScriptCreate = 0
ObjectPrototype.ciScriptExecute = 1

--Export directory.
ObjectPrototype.csExportDir = "Data/Object Prototypes/Output/"

-- |[ ============ Class Members ============= ]|
-- |[System]|
ObjectPrototype.__index = ObjectPrototype
ObjectPrototype.iTotalFilesCreated = 0   --Variable that tracks how many files have been created to make it easier to give output files unique names.

-- |[ ============ Class Methods ============= ]|
--System
--Enumerations
function ObjectPrototype:fnStatisticGenEnums(psFieldName, piDefault) end

--File I/O
function ObjectPrototype:fnGeneratePathname()                                     end
function ObjectPrototype:fnGetLastLineStartingWith(psaStrings, psToFind, psError) end

--Utility
function ObjectPrototype:fnCreateBalancedHeader(psString, piLength) end
function ObjectPrototype:fnGetJPEnumeration(piJPAmount)             end
function ObjectPrototype:fnGetDamageEnumeration(piDamageFlag)       end
function ObjectPrototype:fnGetDamageAnimFromType(piDamageFlag)      end
function ObjectPrototype:fnNospace(psString)                        end
function ObjectPrototype:fnGenerateDotStrings(piDamageFlag)         end
function ObjectPrototype:fnGetFloatWithPrecision(pfFloat)           end
function ObjectPrototype:fnGetStatEnumeration(piStatFlag)           end
function ObjectPrototype:fnGetStatModifierEnumeration(piStatFlag)   end
function ObjectPrototype:fnRespaceLine(psString, piaLengths)        end
 
-- |[ ========== Class Constructor =========== ]|
--Namepsacing class, not meant to be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "ObjectPrototypeEnums.lua")
LM_ExecuteScript(fnResolvePath() .. "ObjectPrototypeFileIO.lua")
LM_ExecuteScript(fnResolvePath() .. "ObjectPrototypeUtility.lua")