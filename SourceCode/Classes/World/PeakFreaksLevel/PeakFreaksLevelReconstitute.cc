//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "PeakFreaksPlayer.h"
#include "TileLayer.h"
#include "Tiled/TiledRawData.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers

///--[Local Definitions]

///========================================== System ==============================================
void PeakFreaksLevel::Reconstitute()
{
    ///--[Documentation]
    //--Once the level is finished loading and parsing, creates a new level using the first one as
    //  a series of block pieces. This involves reallocation and moving stuff around.
    //--For now, we use a fixed-size construction.
    if(mTileLayers->GetListSize() < 1) return;

    ///--[Run Generator]
    PF_LevelLayout *tLayout = NULL;
    if(!mIsTutorial)
        tLayout = GenerateLevel();
    else
        tLayout = GenerateTutorial();
    if(!tLayout) return;

    ///--[New Level Allocation]
    //--Set expected sizes.
    int tNewTileXSize = tLayout->mXSize * CHUNK_W;
    int tNewTileYSize = tLayout->mYSize * CHUNK_H;

    //--We need to create a new set of tile layers, using the existing properties of those layers.
    //  Copy them.
    int tTotalLayers = mTileLayers->GetListSize();

    //--Collision layer.
    CollisionPack *tCollisionLayer = (CollisionPack *)starmemoryalloc(sizeof(CollisionPack));
    tCollisionLayer->Initialize();
    tCollisionLayer->AllocateSizes(tNewTileXSize, tNewTileYSize);
    tCollisionLayer->ClearTo(CLIP_NONE);

    //--AI collision layer. Used for pathing.
    CollisionPack *tCollisionLayerAI = (CollisionPack *)starmemoryalloc(sizeof(CollisionPack));
    tCollisionLayerAI->Initialize();
    tCollisionLayerAI->AllocateSizes(tNewTileXSize, tNewTileYSize);
    tCollisionLayerAI->ClearTo(CLIP_ALL);

    //--Powerups collision layer. Spawns goodies.
    CollisionPack *tPowerupLayer = (CollisionPack *)starmemoryalloc(sizeof(CollisionPack));
    tPowerupLayer->Initialize();
    tPowerupLayer->AllocateSizes(tNewTileXSize, tNewTileYSize);
    tPowerupLayer->ClearTo(CLIP_NONE);

    //--Create a set of layers.
    TileLayer **tLayerList = (TileLayer **)starmemoryalloc(sizeof(TileLayer *) * tTotalLayers);
    for(int i = 0; i < tTotalLayers; i ++)
    {
        //--Get reference layer.
        TileLayer *rRefLayer = (TileLayer *)mTileLayers->GetElementBySlot(i);

        //--Collision layer. Do nothing.
        if(rRefLayer->IsCollisionLayer())
        {
            tLayerList[i] = new TileLayer();
        }
        //--Tile layer.
        else
        {
            tLayerList[i] = new TileLayer();
            tLayerList[i]->SetDepth(rRefLayer->GetDepth());
            tLayerList[i]->ProvideTileReferences(mTilesetsTotal, mTilesetPacks);
            tLayerList[i]->ProvideNoData(tNewTileXSize, tNewTileYSize);
        }
    }

    //--Create a set of object lists.
    mReconPlayerStartZones = new SugarLinkedList(true);
    mReconDamageZones      = new SugarLinkedList(true);
    mReconVictoryZones     = new SugarLinkedList(true);
    mReconCoinZones        = new SugarLinkedList(true);
    mReconTreasureZones    = new SugarLinkedList(true);
    mReconLayerDropZones   = new SugarLinkedList(true);
    mReconCheckpointZones  = new SugarLinkedList(true);
    mReconSlipZones        = new SugarLinkedList(true);
    mReconClimbZones       = new SugarLinkedList(true);
    mReconTriggerZones     = new SugarLinkedList(true);
    mReconSliderList       = new SugarLinkedList(true);
    mReconSpinnerList      = new SugarLinkedList(true);
    mReconWaterfallList    = new SugarLinkedList(true);
    mReconBombSpikeList    = new SugarLinkedList(true);

    ///--[Construction]
    //--Size the progress tracker array.
    mProgressSizeX = mCollisionLayers[0].mCollisionSizeX;
    mProgressSizeY = mCollisionLayers[0].mCollisionSizeY;
    mTileProgressValues = (float **)starmemoryalloc(sizeof(float *) * mProgressSizeX);
    for(int x = 0; x < mProgressSizeX; x ++)
    {
        mTileProgressValues[x] = (float *)starmemoryalloc(sizeof(float) * mProgressSizeY);
        for(int y = 0; y < mProgressSizeY; y ++)
        {
            mTileProgressValues[x][y] = 0.0f;
        }
    }

    //--Iterate across the grid and populate the new tilesets with data.
    int tSearchCode = 0;
    while(true)
    {
        //--Search the array for the required priority. Chunks are therefore created in the order
        //  the player is supposed to move across them. This makes sure things like checkpoints are
        //  in a nominal order.
        bool tFoundPriority = false;
        for(int tMajorX = 0; tMajorX < tLayout->mXSize; tMajorX ++)
        {
            for(int tMajorY = 0; tMajorY < tLayout->mYSize; tMajorY ++)
            {
                //--Skip empty grid slots.
                int tGridCode = tLayout->mArray[tMajorX][tMajorY];
                if(tGridCode == RECON_EMPTY) continue;

                //--Priority match.
                if(tSearchCode == tLayout->mPriorities[tMajorX][tMajorY])
                {
                    //--Reconstitute.
                    tFoundPriority = true;
                    ReconstituteGrid(tGridCode, tMajorX * CHUNK_W, tMajorY * CHUNK_H, tTotalLayers, tLayerList, tCollisionLayer, tCollisionLayerAI, tPowerupLayer);

                    //--Specify the progress value of all parts of this section of the grid.
                    float tProgressVal = 1.0f - ((float)(tSearchCode / (float)(LEVEL_LENGTH-1)));
                    for(int x = 0; x < CHUNK_W; x ++)
                    {
                        for(int y = 0; y < CHUNK_H; y ++)
                        {
                            int tXPos = (tMajorX * CHUNK_W) + x;
                            int tYPos = (tMajorY * CHUNK_H) + y;
                            mTileProgressValues[tXPos][tYPos] = tProgressVal;
                        }
                    }

                    //--Create an entry in the opening list.
                    Point2DF *nPoint = (Point2DF *)starmemoryalloc(sizeof(Point2DF));
                    nPoint->mX = ((tMajorX * CHUNK_W) + (CHUNK_W / 2)) * TileLayer::cxSizePerTile;
                    nPoint->mY = ((tMajorY * CHUNK_H) + (CHUNK_H / 2)) * TileLayer::cxSizePerTile;
                    mOpeningPositions->AddElementAsTail("X", nPoint, &FreeThis);
                    break;
                }
            }

            //--Break if the priority was found.
            if(tFoundPriority) break;
        }

        //--If we didn't find a priority, we're done.
        if(!tFoundPriority) break;

        //--Otherwise, increment and run the loop again.
        tSearchCode ++;
    }

    ///--[Reprocess Tiles]
    //--This informs the tile layers of the rendering coordinates of the tiles.
    for(int i = 0; i < tTotalLayers; i ++)
    {
        tLayerList[i]->ResaveTiles();
    }

    ///--[Print]
    if(false)
    {
        FILE *fOutfile = fopen("Clips.txt", "wb");
        for(int y = 0; y < tNewTileYSize; y ++)
        {
            for(int x = 0; x < tNewTileXSize; x ++)
            {
                fprintf(fOutfile, "%03i ", tCollisionLayer[0].GetClipAt(x, y));
            }
            fprintf(fOutfile, "\n");
        }
        fclose(fOutfile);
    }

    ///--[Compile]
    //--Take the existing values out, put the new ones in.
    mTileLayers->ClearList();
    for(int i = 0; i < tTotalLayers; i ++)
    {
        mTileLayers->AddElementAsTail("X", tLayerList[i], &TileLayer::DeleteThis);
    }

    //--Collision layer.
    CollisionPack::FreeThis(&mCollisionLayers[0]);
    memcpy(&mCollisionLayers[0], tCollisionLayer, sizeof(CollisionPack));
    CollisionPack::FreeThis(&mCollisionLayers[1]);
    memcpy(&mCollisionLayers[1], tCollisionLayerAI, sizeof(CollisionPack));
    CollisionPack::FreeThis(&mCollisionLayers[2]);
    memcpy(&mCollisionLayers[2], tPowerupLayer, sizeof(CollisionPack));

    //--Clear previous object lists.
    delete mPlayerStartZones;
    delete mDamageZones;
    delete mVictoryZones;
    delete mCoinZones;
    delete mTreasureZones;
    delete mLayerDropZones;
    delete mCheckpointZones;
    delete mSlipZones;
    delete mClimbZones;
    delete mTriggerZones;
    delete mSliderList;
    delete mSpinnerList;
    delete mWaterfallList;
    delete mBombSpikeList;

    //--Replace them.
    mPlayerStartZones = mReconPlayerStartZones;
    mDamageZones      = mReconDamageZones;
    mVictoryZones     = mReconVictoryZones;
    mCoinZones        = mReconCoinZones;
    mTreasureZones    = mReconTreasureZones;
    mLayerDropZones   = mReconLayerDropZones;
    mCheckpointZones  = mReconCheckpointZones;
    mSlipZones        = mReconSlipZones;
    mClimbZones       = mReconClimbZones;
    mTriggerZones     = mReconTriggerZones;
    mSliderList       = mReconSliderList;
    mSpinnerList      = mReconSpinnerList;
    mWaterfallList    = mReconWaterfallList;
    mBombSpikeList    = mReconBombSpikeList;

    //--Clean pointers.
    mReconPlayerStartZones = NULL;
    mReconDamageZones      = NULL;
    mReconVictoryZones     = NULL;
    mReconCoinZones        = NULL;
    mReconTreasureZones    = NULL;
    mReconLayerDropZones   = NULL;
    mReconCheckpointZones  = NULL;
    mReconSlipZones        = NULL;
    mReconClimbZones       = NULL;
    mReconTriggerZones     = NULL;
    mReconSliderList       = NULL;
    mReconSpinnerList      = NULL;
    mReconWaterfallList    = NULL;
    mReconBombSpikeList    = NULL;

    ///--[Player Reposition]
    ZonePack *rPlayerStartZone = (ZonePack *)mPlayerStartZones->PushIterator();
    while(rPlayerStartZone)
    {
        //--Zero depth, player start position.
        if(rPlayerStartZone->mLayer == 0)
        {
            //--Zeroth checkpoint.
            mCheckpointX = rPlayerStartZone->mDim.mLft;
            mCheckpointY = rPlayerStartZone->mDim.mTop;
            mCheckpointZ = 0;

            //--Set player position.
            if(mPlayer)
            {
                mPlayer->SetPosition(mCheckpointX / 16.0f, mCheckpointY / 16.0f);
            }
        }

        //--Next.
        rPlayerStartZone = (ZonePack *)mPlayerStartZones->AutoIterate();
    }

    ///--[Death Zones]
    //--All unoccupied grid locations get a hole placed over them.
    for(int tMajorX = 0; tMajorX < tLayout->mXSize; tMajorX ++)
    {
        for(int tMajorY = 0; tMajorY < tLayout->mYSize; tMajorY ++)
        {
            //--Skip filed grid slots.
            int tGridCode = tLayout->mArray[tMajorX][tMajorY];
            if(tGridCode != RECON_EMPTY) continue;

            //--Make a big hole.
            ZonePack *nPackage = new ZonePack();
            nPackage->mDim.SetWH(tMajorX*8*16, tMajorY*8*16, 8*16, 8*16);
            mDamageZones->AddElement("X", nPackage, &ZonePack::DeleteThis);
        }
    }

    ///--[Clean]
    free(tLayerList);
    tLayout->Deallocate();
    free(tLayout);

    ///--[Checkpoint Scanner]
    //--Scan the checkpoints list to spot checkpoints that are immediately adjacent to another checkpoint.
    //  Delete this kind of checkpoint until all are removed. This removes duplicates so two checkpoints
    //  don't spawn right next to each other.
    bool tDidAnything = true;
    while(tDidAnything)
    {
        //--Reset this flag.
        tDidAnything = false;

        //--Store the pointer to the checkpoint to be removed.
        void *rRemovePtr = NULL;

        //--Iterate across all checkpoints.
        ZonePack *rCheckpointPackA = (ZonePack *)mCheckpointZones->PushIterator();
        while(rCheckpointPackA)
        {
            //--Check against all other checkpoints.
            ZonePack *rCheckpointPackB = (ZonePack *)mCheckpointZones->PushIterator();
            while(rCheckpointPackB)
            {
                //--Ignore comparisons against self:
                if(rCheckpointPackA == rCheckpointPackB)
                {

                }
                //--Otherwise, check if these are immediately adjacent.
                else
                {
                    if(IsCollision2DReal(rCheckpointPackA->mDim, rCheckpointPackB->mDim))
                    {
                        rRemovePtr = rCheckpointPackA;
                    }
                }

                //--If there's a removal, break out.
                if(rRemovePtr)
                {
                    mCheckpointZones->PopIterator();
                    break;
                }

                //--Next.
                rCheckpointPackB = (ZonePack *)mCheckpointZones->AutoIterate();
            }

            //--If there's a removal, break out.
            if(rRemovePtr)
            {
                mCheckpointZones->PopIterator();
                break;
            }
            //--Next.
            rCheckpointPackA = (ZonePack *)mCheckpointZones->AutoIterate();
        }

        //--If flagged for removal, mark that we did something and remove the pointer once all iterators are cleared.
        if(rRemovePtr)
        {
            tDidAnything = true;
            mCheckpointZones->RemoveElementP(rRemovePtr);
        }
    }

    ///--[Debug]
    if(false)
    {
        fprintf(stderr, "Writing checkpoints.\n");
        ZonePack *rCheckpointPack = (ZonePack *)mCheckpointZones->PushIterator();
        while(rCheckpointPack)
        {
            fprintf(stderr, " %s %i %i\n", mCheckpointZones->GetIteratorName(), (int)(rCheckpointPack->mDim.mLft) / 16, (int)(rCheckpointPack->mDim.mTop) / 16);
            rCheckpointPack = (ZonePack *)mCheckpointZones->AutoIterate();
        }
    }
}
void PeakFreaksLevel::ReconstituteGrid(int pCode, int pX, int pY, int pLayersTotal, TileLayer **pLayerList, CollisionPack *pCollisions, CollisionPack *pAICollisions, CollisionPack *pPowerups)
{
    ///--[Documentation]
    //--Given a section of map (16x16 Tiles), takes from the existing level's layers and copies
    //  into the provided arrays of tiles.
    if(pCode == RECON_EMPTY || pLayersTotal < 1 || !pLayerList || !pCollisions || !pAICollisions || !pPowerups) return;

    ///--[Resolve Query Positions]
    //--Resolve positions to use for query. Some may have multiple rolls possible.
    int tQueryX = 0;
    int tQueryY = 0;

    //--Constants
    int cChunkSpcX = CHUNK_W+1;
    int cChunkSpcY = CHUNK_H+1;

    //--Starting positions.
    if(pCode == RECON_START_E)
    {
        tQueryX = 0;
        tQueryY = 0;
    }
    else if(pCode == RECON_START_N)
    {
        tQueryX = (cChunkSpcX) * 1;
        tQueryY = 0;
    }
    else if(pCode == RECON_START_W)
    {
        tQueryX = (cChunkSpcX) * 2;
        tQueryY = 0;
    }
    else if(pCode == RECON_START_S)
    {
        tQueryX = (cChunkSpcX) * 3;
        tQueryY = 0;
    }
    //--Ending Positions
    else if(pCode == RECON_END_E)
    {
        tQueryX = (cChunkSpcX) * 4;
        tQueryY = 0;
    }
    else if(pCode == RECON_END_N)
    {
        tQueryX = (cChunkSpcX) * 5;
        tQueryY = 0;
    }
    else if(pCode == RECON_END_W)
    {
        tQueryX = (cChunkSpcX) * 6;
        tQueryY = 0;
    }
    else if(pCode == RECON_END_S)
    {
        tQueryX = (cChunkSpcX) * 7;
        tQueryY = 0;
    }
    //--Horizontal and Vertical
    else if(pCode == RECON_HORIZONTAL)
    {
        int tRoll = rand() % 3;
        tQueryX = 0 + (cChunkSpcX * tRoll);
        tQueryY = cChunkSpcY;
    }
    else if(pCode == RECON_VERTICAL)
    {
        int tRoll = rand() % 3;
        tQueryX = (cChunkSpcX * 3) + (cChunkSpcX * tRoll);
        tQueryY = cChunkSpcY;
    }
    //--Corner R/U
    else if(pCode == RECON_ELBOW_RU)
    {
        int tRoll = rand() % 2;
        tQueryX = (cChunkSpcX * 0) + (cChunkSpcX * tRoll);
        tQueryY = (cChunkSpcY * 2);
    }
    //--Corner L/U
    else if(pCode == RECON_ELBOW_LU)
    {
        int tRoll = rand() % 2;
        tQueryX = (cChunkSpcX * 2) + (cChunkSpcX * tRoll);
        tQueryY = (cChunkSpcY * 2);
    }
    //--Corner R/D
    else if(pCode == RECON_ELBOW_RD)
    {
        int tRoll = rand() % 2;
        tQueryX = (cChunkSpcX * 4) + (cChunkSpcX * tRoll);
        tQueryY = (cChunkSpcY * 2);
    }
    //--Corner L/D
    else if(pCode == RECON_ELBOW_LD)
    {
        int tRoll = rand() % 2;
        tQueryX = (cChunkSpcX * 0) + (cChunkSpcX * tRoll);
        tQueryY = (cChunkSpcY * 3);
    }

    ///--[Area Resolve]
    //--Query area.
    float tLft = tQueryX * TileLayer::cxSizePerTile;
    float tTop = tQueryY * TileLayer::cxSizePerTile;
    float tRgt = tLft + (CHUNK_W * TileLayer::cxSizePerTile);
    float tBot = tTop + (CHUNK_H * TileLayer::cxSizePerTile);

    //--Result area.
    float tNewLft = (pX * TileLayer::cxSizePerTile);
    float tNewTop = (pY * TileLayer::cxSizePerTile);

    ///--[Copy Tiles]
    //--Copy to all layers.
    for(int i = 0; i < pLayersTotal; i ++)
    {
        //--Get reference layer.
        TileLayer *rRefLayer = (TileLayer *)mTileLayers->GetElementBySlot(i);

        //--Iterate.
        for(int x = 0; x < CHUNK_W; x ++)
        {
            for(int y = 0; y < CHUNK_H; y ++)
            {
                //--Tile.
                int tTileVal = rRefLayer->GetTileIndexAt(tQueryX + x, tQueryY + y);
                pLayerList[i]->SetTile(pX + x, pY + y, tTileVal);

                //--Flips.
                uint8_t tFlipVal = rRefLayer->GetFlipFlagsAt(tQueryX + x, tQueryY + y);
                pLayerList[i]->SetFlipFlag(pX + x, pY + y, tFlipVal);
            }
        }
    }

    //--Copy collisions.
    for(int x = 0; x < CHUNK_W; x ++)
    {
        for(int y = 0; y < CHUNK_H; y ++)
        {
            pCollisions->  mCollisionData[pX + x][pY + y] = mCollisionLayers[0].mCollisionData[tQueryX + x][tQueryY + y];
            pAICollisions->mCollisionData[pX + x][pY + y] = mCollisionLayers[1].mCollisionData[tQueryX + x][tQueryY + y];
            pPowerups->    mCollisionData[pX + x][pY + y] = mCollisionLayers[2].mCollisionData[tQueryX + x][tQueryY + y];
        }
    }

    ///--[Copy Objects]
    CloneListInArea(mPlayerStartZones,       mReconPlayerStartZones, tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mDamageZones,            mReconDamageZones,      tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mVictoryZones,           mReconVictoryZones,     tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mCoinZones,              mReconCoinZones,        tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mTreasureZones,          mReconTreasureZones,    tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mLayerDropZones,         mReconLayerDropZones,   tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mSlipZones,              mReconSlipZones,        tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mClimbZones,             mReconClimbZones,       tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneListInArea(mTriggerZones,           mReconTriggerZones,     tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneCheckpointsInArea(mCheckpointZones, mReconCheckpointZones,  tLft, tTop, tRgt, tBot, tNewLft, tNewTop);

    //--Hazards.
    CloneSlidersInArea   (tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneSpinnersInArea  (tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneWaterfallsInArea(tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
    CloneBombSpikesInArea(tLft, tTop, tRgt, tBot, tNewLft, tNewTop);
}
void PeakFreaksLevel::CloneListInArea(SugarLinkedList *pList, SugarLinkedList *pNewList, float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop)
{
    ///--[Documentation]
    //--Takes all entities that fall within the provided area that are on the provided list, and clones them into
    //  pNewList. Easy!
    if(!pList || !pNewList) return;

    ///--[Iterate]
    ZonePack *rZonePack = (ZonePack *)pList->PushIterator();
    while(rZonePack)
    {
        //--Check for hit:
        if(IsCollision2DReal(rZonePack->mDim, pLft, pTop, pRgt, pBot))
        {
            //--Clone.
            ZonePack *nNewPack = new ZonePack();
            nNewPack->mLayer = rZonePack->mLayer;
            memcpy(&nNewPack->mDim, &rZonePack->mDim, sizeof(TwoDimensionReal));

            //--Reposition.
            nNewPack->mDim.mXCenter = (nNewPack->mDim.mXCenter - pLft) + (pNewLft);
            nNewPack->mDim.mYCenter = (nNewPack->mDim.mYCenter - pTop) + (pNewTop);
            nNewPack->mDim.mLft = (nNewPack->mDim.mLft - pLft) + (pNewLft);
            nNewPack->mDim.mTop = (nNewPack->mDim.mTop - pTop) + (pNewTop);
            nNewPack->mDim.mRgt = (nNewPack->mDim.mRgt - pLft) + (pNewLft);
            nNewPack->mDim.mBot = (nNewPack->mDim.mBot - pTop) + (pNewTop);

            //--Register.
            pNewList->AddElementAsHead(pList->GetIteratorName(), nNewPack, &ZonePack::DeleteThis);
        }

        //--Next.
        rZonePack = (ZonePack *)pList->AutoIterate();
    }
}
void PeakFreaksLevel::CloneCheckpointsInArea(SugarLinkedList *pList, SugarLinkedList *pNewList, float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop)
{
    ///--[Documentation]
    //--Identical to the above function, except gives each checkpoint a unique name.
    if(!pList || !pNewList) return;

    ///--[Iterate]
    ZonePack *rZonePack = (ZonePack *)pList->PushIterator();
    while(rZonePack)
    {
        //--Check for hit:
        if(IsCollision2DReal(rZonePack->mDim, pLft, pTop, pRgt, pBot))
        {
            //--Clone.
            ZonePack *nNewPack = new ZonePack();
            nNewPack->mLayer = rZonePack->mLayer;
            memcpy(&nNewPack->mDim, &rZonePack->mDim, sizeof(TwoDimensionReal));

            //--Reposition.
            nNewPack->mDim.mXCenter = (nNewPack->mDim.mXCenter - pLft) + (pNewLft);
            nNewPack->mDim.mYCenter = (nNewPack->mDim.mYCenter - pTop) + (pNewTop);
            nNewPack->mDim.mLft = (nNewPack->mDim.mLft - pLft) + (pNewLft);
            nNewPack->mDim.mTop = (nNewPack->mDim.mTop - pTop) + (pNewTop);
            nNewPack->mDim.mRgt = (nNewPack->mDim.mRgt - pLft) + (pNewLft);
            nNewPack->mDim.mBot = (nNewPack->mDim.mBot - pTop) + (pNewTop);

            //--Generate a name.
            char tBuf[64];
            sprintf(tBuf, "CP%i", mCheckpointCount);

            //--Register.
            pNewList->AddElementAsHead(tBuf, nNewPack, &ZonePack::DeleteThis);
            mCheckpointCount ++;
        }

        //--Next.
        rZonePack = (ZonePack *)pList->AutoIterate();
    }
}
void PeakFreaksLevel::CloneSlidersInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop)
{
    ///--[Documentation]
    //--As above, copies the PF_Slider's to the destination list. The lists are implied.

    ///--[Iterate]
    PF_Slider *rSlider = (PF_Slider *)mSliderList->PushIterator();
    while(rSlider)
    {
        //--Check for hit:
        if(IsPointWithin(rSlider->mXPos, rSlider->mYPos, pLft, pTop, pRgt, pBot))
        {
            //--Clone.
            PF_Slider *nNewPack = (PF_Slider *)starmemoryalloc(sizeof(PF_Slider));
            memcpy(nNewPack, rSlider, sizeof(PF_Slider));

            //--Reposition.
            nNewPack->mXPos   = (nNewPack->mXPos   - pLft) + (pNewLft);
            nNewPack->mYPos   = (nNewPack->mYPos   - pTop) + (pNewTop);
            nNewPack->mXStart = (nNewPack->mXStart - pLft) + (pNewLft);
            nNewPack->mYStart = (nNewPack->mYStart - pTop) + (pNewTop);
            nNewPack->mXEnd   = (nNewPack->mXEnd   - pLft) + (pNewLft);
            nNewPack->mYEnd   = (nNewPack->mYEnd   - pTop) + (pNewTop);

            //--Register.
            mReconSliderList->AddElementAsHead("X", nNewPack, &FreeThis);
        }

        //--Next.
        rSlider = (PF_Slider *)mSliderList->AutoIterate();
    }
}
void PeakFreaksLevel::CloneSpinnersInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop)
{
    ///--[Documentation]
    //--As above, copies the PF_Spinner's to the destination list. The lists are implied.

    ///--[Iterate]
    PF_Spinner *rSpinner = (PF_Spinner *)mSpinnerList->PushIterator();
    while(rSpinner)
    {
        //--Check for hit:
        if(IsPointWithin(rSpinner->mXPos, rSpinner->mYPos, pLft, pTop, pRgt, pBot))
        {
            //--Clone.
            PF_Spinner *nNewPack = (PF_Spinner *)starmemoryalloc(sizeof(PF_Spinner));
            memcpy(nNewPack, rSpinner, sizeof(PF_Spinner));

            //--Reposition.
            nNewPack->mXPos = (nNewPack->mXPos - pLft) + (pNewLft);
            nNewPack->mYPos = (nNewPack->mYPos - pTop) + (pNewTop);

            //--Register.
            mReconSpinnerList->AddElementAsHead("X", nNewPack, &FreeThis);
        }

        //--Next.
        rSpinner = (PF_Spinner *)mSpinnerList->AutoIterate();
    }
}
void PeakFreaksLevel::CloneWaterfallsInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop)
{
    ///--[Documentation]
    //--As above, copies the PF_Waterfall's to the destination list. The lists are implied.

    ///--[Iterate]
    PF_Waterfall *rWaterfall = (PF_Waterfall *)mWaterfallList->PushIterator();
    while(rWaterfall)
    {
        //--Check for hit:
        if(IsPointWithin(rWaterfall->mXPos, rWaterfall->mYPos, pLft, pTop, pRgt, pBot))
        {
            //--Clone.
            PF_Waterfall *nNewPack = (PF_Waterfall *)starmemoryalloc(sizeof(PF_Waterfall));
            memcpy(nNewPack, rWaterfall, sizeof(PF_Waterfall));

            //--Reposition.
            nNewPack->mXPos = (nNewPack->mXPos - pLft) + (pNewLft);
            nNewPack->mYPos = (nNewPack->mYPos - pTop) + (pNewTop);

            //--Register.
            mReconWaterfallList->AddElementAsHead("X", nNewPack, &FreeThis);
        }

        //--Next.
        rWaterfall = (PF_Waterfall *)mWaterfallList->AutoIterate();
    }
}
void PeakFreaksLevel::CloneBombSpikesInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop)
{
    ///--[Documentation]
    //--As above, copies the PF_BombSpike's to the destination list. The lists are implied.

    ///--[Iterate]
    PF_BombSpike *rBombSpike = (PF_BombSpike *)mBombSpikeList->PushIterator();
    while(rBombSpike)
    {
        //--Check for hit:
        if(IsPointWithin(rBombSpike->mXPos, rBombSpike->mYPos, pLft, pTop, pRgt, pBot))
        {
            //--Clone.
            PF_BombSpike *nNewPack = (PF_BombSpike *)starmemoryalloc(sizeof(PF_BombSpike));
            memcpy(nNewPack, rBombSpike, sizeof(PF_BombSpike));

            //--Reposition.
            nNewPack->mXPos = (nNewPack->mXPos - pLft) + (pNewLft);
            nNewPack->mYPos = (nNewPack->mYPos - pTop) + (pNewTop);

            //--Register.
            mReconBombSpikeList->AddElementAsHead("X", nNewPack, &FreeThis);
        }

        //--Next.
        rBombSpike = (PF_BombSpike *)mBombSpikeList->AutoIterate();
    }
}
