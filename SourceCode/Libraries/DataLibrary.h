///======================================== DataLibrary ===========================================
//--General purpose archiver of pointers, by name.  Everything is stored in StarLinkedLists as
//  void pointers, and the type is expected to be known by the caller.  If the object is known to
//  be a RootObject then the type can be resolved from there.
//--Supports up to four levels of indexing.  These are called, in order, Section Catalogue Heading
//  and then the pointer itself is in the Index.
//--Example:  Section/Catalogue/Heading/Pointer
//            Graphics/Ponies/Applejack/AJSmiling
//--Additional functionality is provided for storing variables within the library as Sysvars.  These
//  are Lua-ready variables which can be strings or floating-point.
//--The DL can also store pointers by activity in a stack.  Lua can push/pull to and from this stack
//  which allows for complex object manipulation in decentralized Lua scripts.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[SysVar]
typedef struct SysVar
{
    float mNumeric;
    char *mAlpha;
    bool mIsSaved;
}SysVar;

///--[ResolvePack]
typedef struct ResolvePack
{
    char mSection[STD_MAX_LETTERS];
    char mCatalogue[STD_MAX_LETTERS];
    char mHeading[STD_MAX_LETTERS];
    char mName[STD_MAX_LETTERS];

    StarLinkedList *rSection;
    StarLinkedList *rCatalogue;
    StarLinkedList *rHeading;
    void *rEntry;
}ResolvePack;

///--[UnloadEntry]
//--When unloading bitmaps after they have been used, this package stores all queued bitmaps along
//  with a DLPath to be checked to make sure the bitmaps didn't get deleted. Bitmaps are typically
//  cleared during level transitions.
typedef struct UnloadEntry
{
    char mCheckPath[STD_MAX_LETTERS];
    StarBitmap *rUnloadBitmap;
}UnloadEntry;

///===================================== Local Definitions ========================================
#define DL_ROOT -1
#define DL_SECTION 0
#define DL_CATALOGUE 1
#define DL_HEADING 2
#define DL_NAME 3

///========================================== Classes =============================================
class DataLibrary
{
    private:
    //--System
    bool mDebugMode;
    bool mErrorMode;

    //--Storage
    StarLinkedList *mSectionList;
    StarLinkedList *mActiveEntityStack;

    //--Counters
    int mRunningIndexCounter;

    //--Dummies
    RootObject *mDummyPtr;

    //--Font Storage
    StarLinkedList *mMasterFontList; //StarFont *, master
    StarLinkedList *mFontKerningPathList; //char *, master, parallel with mMasterFontList
    StarLinkedList *mrFontRefList;   //StarFont *, ref

    //--Path Pool Storage
    StarLinkedList *mPathPool; //char *, master

    //--Unload Storage
    StarLinkedList *mUnloadList; //UnloadEntry *, master

    //--Pointer Series Listing
    StarLinkedList *mPointerSeriesList; //StarPointerSeries *, master

    protected:

    public:
    //--System
    DataLibrary();
    ~DataLibrary();
    static void DeleteSysVar(void *pPtr);

    //--Public Variables
    bool mFailSilently;
    bool mSuppressFontErrors;
    StarLinkedList *rCurrentSection;
    StarLinkedList *rCurrentCatalogue;
    StarLinkedList *rCurrentHeading;
    void *rActiveObject;
    int mErrorCode;
    static bool xBlockSpecialDeletion;
    static bool xGetEntryDebug;
    static int xLastPixelCount;

    //--Property Queries
    char *GetPathOfEntry(void *pPtr);
    StarLinkedList *GetSection(const char *pResolveString);
    StarLinkedList *GetCatalogue(const char *pResolveString);
    StarLinkedList *GetHeading(const char *pResolveString);
    void *GetEntry(const char *pResolveString);
    bool DoesEntryExist(const char *pResolveString);
    bool IsActiveValid();
    bool IsActiveValid(int pTypeFlag);
    bool IsActiveValid(int pTypeStart, int pTypeEnd);
    int GetActiveObjectType();
    int GetSizeOfActivityStack();
    int GetSizeOfList(const char *pResolveString);
    static int CountElementsInList(StarLinkedList *pList, int pLevel);
    StarPointerSeries *GetStarPointerSeries(const char *pName);

    //--Manipulators
    void SetErrorFlag(bool pFlag);
    void SetErrorCode(int pCode);
    void RegisterStarPointerSeries(const char *pName, StarPointerSeries *pEntry);
    void AddPath(const char *pDLPath);
    void RegisterPointer(const char *pDLPath, void *pPointer);
    void RegisterPointer(const char *pDLPath, void *pPointer, DeletionFunctionPtr pDeletionFunction);
    void *RemovePointer(const char *pDLPath);

    void SetActiveObject(const char *pDLPath);
    void PushActiveEntity();
    void PushActiveEntity(void *pReplacer);
    void *PopActiveEntity();

    //--Fonts
    void HandleFontAddition(const char *pName, const char *pFontPath, const char *pKerningPath, int pSize, int pFlags);
    void HandleDummyFontAddition(const char *pName);
    bool FontExists(const char *pName);
    bool FontAliasExists(const char *pName);
    void RegisterFont(const char *pName, StarFont *pFont, const char *pKerningPath);
    void RegisterFontAlias(const char *pFontName, const char *pAliasName);
    void RebuildAllKerning();
    void RebuildFontKerning(const char *pName);
    StarFont *GetFont(const char *pName);

    //--Core Methods
    static void PrintDiagnostics();
    void EnqueueBitmapUnloading(const char *pCheckPath, StarBitmap *pBitmap);
    void ClearUnloadQueueForBitmap(StarBitmap *pBitmap);
    void UnloadPendingBitmaps();

    //--Private Core Methods
    private:
    void AddSection(const char *pName);
    void AddCatalogue(const char *pName);
    void AddHeading(const char *pName);
    public:

    //--Purging (DataLibraryPurging.cc)
    void *Purge(const char *pDLPath);
    void *Purge(const char *pDLPath, bool pBlockDeallocation);

    //--Worker Functions
    void ErrorNotFound(const char *pCallerName, const char *pResolveString);
    ResolvePack *CreateResolvePack(const char *pResolveString);
    ResolvePack *CreateResolvePack(const char *pResolveString, bool pSkipLists);
    static void GetNames(const char *pResolveString, ResolvePack &sPack);
    void GetLists(ResolvePack &sResolvePack);
    static char *GetPathPart(const char *pResolveString, int pPartFlag);
    const char *GetTranslatedString(const char *pTranslationPath, const char *pString);

    //--Variables
    void WriteToBuffer(StarAutoBuffer *pBuffer, bool pUsePathCompression);
    void ReadFromFile(VirtualFile *fInfile, bool pUsePathPool);

    //--Update
    //--File I/O
    void PrintToFile(const char *pFilePath);

    //--Drawing
    //--Pointer Routing
    StarLinkedList *GetMasterFontList();
    StarLinkedList *GetPointerSeriesList();

    //--Static Functions
    static DataLibrary *Fetch();
    static const char *GetGamePath(const char *pVariablePath);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

///--[Hooking Functions]
//--Standard
int Hook_DL_AddPath(lua_State *L);
int Hook_DL_SetActiveObject(lua_State *L);
int Hook_DL_Exists(lua_State *L);
int Hook_DL_SizeOfPath(lua_State *L);

//--Purging
int Hook_DL_Purge(lua_State *L);

//--Activity Stack
int Hook_DL_GetActivityStackSize(lua_State *L);
int Hook_DL_PushActiveEntity(lua_State *L);
int Hook_DL_PopActiveEntity(lua_State *L);
int Hook_DL_ActiveIsValid(lua_State *L);
int Hook_DL_GetActiveObjectType(lua_State *L);
int Hook_DL_ClearActiveEntity(lua_State *L);

//--CoreClass Macros
int Hook_DL_LoadBitmap(lua_State *L);
int Hook_DL_ExtractDelayedBitmap(lua_State *L);
int Hook_DL_ExtractBitmap(lua_State *L);
int Hook_DL_ExtractDummyBitmap(lua_State *L);
int Hook_DL_ExtractPalette(lua_State *L);
int Hook_DL_LoadDelayedBitmap(lua_State *L);
int Hook_DL_UnloadBitmap(lua_State *L);
int Hook_DL_ReportBitmap(lua_State *L);
int Hook_DL_PrintBitmapStatistics(lua_State *L);
int Hook_DL_SaveBitmapToDrive(lua_State *L);

//--Variable Manager Handling
int Hook_VM_Exists(lua_State *L);
int Hook_VM_SetVar(lua_State *L);
int Hook_VM_RemVar(lua_State *L);
int Hook_VM_GetVar(lua_State *L);
int Hook_VM_SetSaveFlag(lua_State *L);

//--Fonts
int Hook_Font_Register(lua_State *L);
int Hook_Font_RegisterAsDummy(lua_State *L);
int Hook_Font_GetProperty(lua_State *L);
int Hook_Font_SetProperty(lua_State *L);

//--Translations
int Hook_Translation_QueryString(lua_State *L);
