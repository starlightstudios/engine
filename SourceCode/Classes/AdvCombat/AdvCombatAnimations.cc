//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAnimation.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

void AdvCombat::RegisterAnimation(const char *pName, AdvCombatAnimation *pAnimation)
{
    if(!pName || !pAnimation) return;
    mCombatAnimations->AddElement(pName, pAnimation, &RootObject::DeleteThis);
}
void AdvCombat::CreateAnimationInstance(const char *pAnimationName, const char *pRefName, int pStartingTicks, float pX, float pY)
{
    //--Creates a new animation instance inside an existing animation.
    if(!pAnimationName || !pRefName) return;

    //--Locate the animation.
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->GetElementByName(pAnimationName);
    if(!rAnimation) return;

    //--Spawn the instance.
    rAnimation->CreateInstance(pRefName, pStartingTicks, pX, pY);
}
