//--Base
#include "TextLevel.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

void TextLevel::SetAutomapTileset(const char *pDLPath, float pXSizePerTile, float pYSizePerTile)
{
    ///--[Documentation]
    //--Set from datalibrary.
    rAutomapTileset = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mUseBlankAutomap = (rAutomapTileset == NULL);
    if(!rAutomapTileset) return;

    //--Sizes.
    mTilemapXSize = rAutomapTileset->GetTrueWidth();
    mTilemapYSize = rAutomapTileset->GetTrueHeight();
    mTilemapXSizePerTile = pXSizePerTile;
    mTilemapYSizePerTile = pYSizePerTile;

    //--Reset constants.
    mSizePerRoomX = 63.99f;
    mSizePerRoomY = 63.99f;
    mHfX = mSizePerRoomX * 0.50f;
    mHfY = mSizePerRoomY * 0.50f;
    mTexXPerTile = mTilemapXSizePerTile / mTilemapXSize;
    mTexYPerTile = mTilemapYSizePerTile / mTilemapYSize;
    cDepthTerrain = -0.000005f;
    cDepthFriendly = -0.000003f;
    cDepthHostile = -0.000002f;
    cDepthPlayer = -0.000001f;

    ///--[Door World Sizes]
    //--Sizings
    float cDoorHWid = (21.0f) / mSizePerRoomX * 0.50f;
    float cDoorHHei = ( 8.0f) / mSizePerRoomY * 0.50f;
    float cDoorVWid = ( 8.0f) / mSizePerRoomX * 0.50f;
    float cDoorVHei = (21.0f) / mSizePerRoomY * 0.50f;

    //--Door Positions.
    cDoorNorth.SetWH(0.50f - cDoorHWid, 0.00f,             cDoorHWid, cDoorHHei);
    cDoorEast .SetWH(1.00f - cDoorVWid, 0.50f - cDoorVHei, cDoorVWid, cDoorVHei);
    cDoorSouth.SetWH(0.50f - cDoorHWid, 1.00f - cDoorHHei, cDoorHWid, cDoorHHei);
    cDoorWest .SetWH(0.00f,             0.50f - cDoorVHei, cDoorVWid, cDoorVHei);

    //--Detect positions, where the mouse needs to be to be over the door.
    float cFat = 8.0f;
    cDoorHWid = (21.0f + cFat) / mSizePerRoomX * 0.50f;
    cDoorHHei = ( 8.0f + cFat) / mSizePerRoomY * 0.50f;
    cDoorVWid = ( 8.0f + cFat) / mSizePerRoomX * 0.50f;
    cDoorVHei = (21.0f + cFat) / mSizePerRoomY * 0.50f;
    cDoorDetectNorth.SetWH(0.50f - cDoorHWid, 0.00f,             cDoorHWid * 2.00f, cDoorHHei);
    cDoorDetectEast .SetWH(1.00f - cDoorVWid, 0.50f - cDoorVHei, cDoorVWid,         cDoorVHei * 2.00f);
    cDoorDetectSouth.SetWH(0.50f - cDoorHWid, 1.00f - cDoorHHei, cDoorHWid * 2.00f, cDoorHHei);
    cDoorDetectWest .SetWH(0.00f,             0.50f - cDoorVHei, cDoorVWid,         cDoorVHei * 2.00f);
}
void TextLevel::SetTilesetFadePositions(float pLineX, float pLineY, float pCornX, float pCornY, float pAlleyX, float pAlleyY, float pBoxX, float pBoxY, float pInsetX, float pInsetY, float pFullblackX, float pFullblackY)
{
    //--Sets positions on the tilemap used by fading. The tileset should be set before calling this!
    if(!rAutomapTileset) return;
    mLineShadeX       = (mTilemapXSizePerTile * pLineX)      / mTilemapXSize;
    mLineShadeY       = (mTilemapYSizePerTile * pLineY)      / mTilemapYSize;
    mLineCornerShadeX = (mTilemapXSizePerTile * pCornX)      / mTilemapXSize;
    mLineCornerShadeY = (mTilemapYSizePerTile * pCornY)      / mTilemapYSize;
    mLineAlleyShadeX  = (mTilemapXSizePerTile * pAlleyX)     / mTilemapXSize;
    mLineAlleyShadeY  = (mTilemapYSizePerTile * pAlleyY)     / mTilemapYSize;
    mLineBoxShadeX    = (mTilemapXSizePerTile * pBoxX)       / mTilemapXSize;
    mLineBoxShadeY    = (mTilemapYSizePerTile * pBoxY)       / mTilemapYSize;
    mLineInsetShadeX  = (mTilemapXSizePerTile * pInsetX)     / mTilemapXSize;
    mLineInsetShadeY  = (mTilemapYSizePerTile * pInsetY)     / mTilemapYSize;
    mFullblackShadeX  = (mTilemapXSizePerTile * pFullblackX) / mTilemapXSize;
    mFullblackShadeY  = (mTilemapYSizePerTile * pFullblackY) / mTilemapYSize;
}
void TextLevel::SetTilesetAlarmPositions(float pExclamationX, float pExclamationY, float pQuestionX, float pQuestionY)
{
    //--Indicators used by entities to show if they are chasing the player.
    if(!rAutomapTileset) return;
    mExclamationX = (mTilemapXSizePerTile * pExclamationX) / mTilemapXSize;
    mExclamationY = (mTilemapXSizePerTile * pExclamationY) / mTilemapYSize;
    mQuestionX    = (mTilemapXSizePerTile * pQuestionX)    / mTilemapXSize;
    mQuestionY    = (mTilemapXSizePerTile * pQuestionY)    / mTilemapYSize;
}
void TextLevel::RegisterAutomapRoom(const char *pName, float pX, float pY, float pZ)
{
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    AutomapPack *nPack = (AutomapPack *)starmemoryalloc(sizeof(AutomapPack));
    nPack->Initialize();
    strncpy(nPack->mInternalName, pName, sizeof(nPack->mInternalName));
    nPack->mX = pX;
    nPack->mY = pY;
    nPack->mZ = pZ;
    mAutomapList->AddElementAsTail(pName, nPack, &AutomapPack::DeleteThis);
}
void TextLevel::RegisterRoomConnection(const char *pNameA, const char *pNameB)
{
    //--Error check.
    if(!pNameA || !pNameB) return;

    //--Get the rooms in question.
    AutomapPack *rRoomA = (AutomapPack *)mAutomapList->GetElementByName(pNameA);
    AutomapPack *rRoomB = (AutomapPack *)mAutomapList->GetElementByName(pNameB);
    if(!rRoomA || !rRoomB) return;

    //--Check for duplicates. Ignore them.
    AutomapPack *rCheckPack = (AutomapPack *)mConnectionList->PushIterator();
    while(rCheckPack)
    {
        if((rCheckPack->rReferenceA == rRoomA && rCheckPack->rReferenceB == rRoomB) ||
           (rCheckPack->rReferenceA == rRoomB && rCheckPack->rReferenceB == rRoomA))
        {
            mConnectionList->PopIterator();
            return;
        }
        rCheckPack = (AutomapPack *)mConnectionList->AutoIterate();
    }

    //--Checks passed, register.
    SetMemoryData(__FILE__, __LINE__);
    AutomapPack *nPack = (AutomapPack *)starmemoryalloc(sizeof(AutomapPack));
    nPack->Initialize();
    nPack->mX = rRoomA->mX;
    nPack->mY = rRoomA->mY;
    nPack->mXEnd = rRoomB->mX;
    nPack->mYEnd = rRoomB->mY;
    nPack->mZ = rRoomA->mZ;
    nPack->rReferenceA = rRoomA;
    nPack->rReferenceB = rRoomB;
    mConnectionList->AddElement("X", nPack, &AutomapPack::DeleteThis);

    //--Tell these rooms they should render connectivity. First, A is west of B.
    if(rRoomA->mX < rRoomB->mX)
    {
        //--A is northwest of B.
        if(rRoomA->mY < rRoomB->mY)
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_SE;
            rRoomB->mConnectionFlags |= TL_CONNECT_NW;
        }
        //--A is southwest of B.
        else if(rRoomA->mY > rRoomB->mY)
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_NE;
            rRoomB->mConnectionFlags |= TL_CONNECT_SW;
        }
        //--A is directly west of B.
        else
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_E;
            rRoomB->mConnectionFlags |= TL_CONNECT_W;
        }
    }
    //--A is east of B.
    else if(rRoomA->mX > rRoomB->mX)
    {
        //--A is northeast of B.
        if(rRoomA->mY < rRoomB->mY)
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_SW;
            rRoomB->mConnectionFlags |= TL_CONNECT_NE;
        }
        //--A is southeast of B.
        else if(rRoomA->mY > rRoomB->mY)
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_NW;
            rRoomB->mConnectionFlags |= TL_CONNECT_SE;
        }

        //--A is directly east of B.
        else
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_W;
            rRoomB->mConnectionFlags |= TL_CONNECT_E;
        }
    }
    //--A and B are vertically aligned.
    else
    {
        //--A is north of B.
        if(rRoomA->mY < rRoomB->mY)
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_S;
            rRoomB->mConnectionFlags |= TL_CONNECT_N;
        }
        //--A is south of B.
        else if(rRoomA->mY > rRoomB->mY)
        {
            rRoomA->mConnectionFlags |= TL_CONNECT_N;
            rRoomB->mConnectionFlags |= TL_CONNECT_S;
        }
        //--A and B occupy the same space, somehow.
        else
        {
        }
    }
}
void TextLevel::SetRoomExplored(float pX, float pY, float pZ, bool pIsExplored)
{
    //--Set a room as explored or not. Unexplored rooms don't render at all.
    //--Search for packs that match the X/Y/Z of the provided area. There can be multiple matches
    //  theoretically, though this is rare in practice.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Room was not previously discovered? Set its fade timers to zero.
            if(!rCheckPack->mIsDiscovered && pIsExplored)
            {
                rCheckPack->mDiscoveryTimer = 0;
                rCheckPack->mFadeCode = AM_FADE_NONE;
                rCheckPack->mInsetCode = AM_FADE_NONE;
                rCheckPack->mPrevFadeTimer = 0;
                rCheckPack->mPrevFadeCode = AM_FADE_NONE;
                rCheckPack->mFadeTimer = AM_FADE_TICKS;
                rCheckPack->mPrevInsetTimer = 0;
                rCheckPack->mPrevInsetCode = AM_FADE_NONE;
                rCheckPack->mInsetTimer = AM_FADE_TICKS;
            }

            //--Flag.
            rCheckPack->mIsDiscovered = pIsExplored;
        }
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::SetRoomVisible(float pX, float pY, float pZ, bool pIsVisible)
{
    //--Sets a room as visible or not. Rooms that are visible show their entity/object contents.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            rCheckPack->mIsVisibleNow = pIsVisible;
        }
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::SetRoomFlags(float pX, float pY, float pZ, bool pHasHostile, bool pHasFriendly, bool pHasExaminable, bool pHasItem)
{
    //--Marks a given room as having up to four symbols. Room marking is always positive, negative flags are ignored.
    //  To clear a room's flags, use ClearRoomFlags().
    if(!pHasExaminable && !pHasFriendly && !pHasHostile && !pHasItem) return;

    //--Iterate.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            if(pHasHostile) rCheckPack->mHasEnemyIndicator = true;
            if(pHasFriendly) rCheckPack->mHasFriendlyIndicator = true;
            if(pHasExaminable) rCheckPack->mHasExaminableIndicator = true;
            if(pHasItem) rCheckPack->mHasItemIndicator = true;
        }
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::AddRoomTilemapLayer(float pX, float pY, float pZ, int pTileX, int pTileY, int pSlot, uint8_t pRotationFlags)
{
    //--Adds a visibility layer to the given room.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Range check.
            int i = rCheckPack->mTilemapLayersTotal;
            if(i >= AMP_MAX_RENDER_OBJECTS)
            {
                mAutomapList->PopIterator();
                return;
            }

            //--Add a new layer.
            rCheckPack->mTilemapLayers[i].mX = pTileX;
            rCheckPack->mTilemapLayers[i].mY = pTileY;
            rCheckPack->mTilemapLayers[i].mSlotOffset = pSlot;
            rCheckPack->mTilemapLayers[i].mRotationFlags = pRotationFlags;
            rCheckPack->mTilemapLayersTotal ++;

            //--Done.
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::SetDoorState(float pX, float pY, float pZ, bool pIsSouthDoor, bool pIsOpen)
{
    //--Given a room, attempts to find the matching door and change its open/closed state.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Try to find the matching door.
            for(int i = 0; i < rCheckPack->mTilemapLayersTotal; i ++)
            {
                if(pIsSouthDoor && (rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_S_CLOSED || rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_S_OPEN))
                {
                    if(pIsOpen)
                    {
                        rCheckPack->mTilemapLayers[i].mSlotOffset = AM_SLOT_DOOR_S_OPEN;
                    }
                    else
                    {
                        rCheckPack->mTilemapLayers[i].mSlotOffset = AM_SLOT_DOOR_S_CLOSED;
                    }
                }
                else if(!pIsSouthDoor && (rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_E_CLOSED || rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_E_OPEN))
                {
                    if(pIsOpen)
                    {
                        rCheckPack->mTilemapLayers[i].mSlotOffset = AM_SLOT_DOOR_E_OPEN;
                    }
                    else
                    {
                        rCheckPack->mTilemapLayers[i].mSlotOffset = AM_SLOT_DOOR_E_CLOSED;
                    }
                }
            }

            //--Finish up.
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::SetDoorIndicator(float pX, float pY, float pZ, bool pIsSouthDoor, int pTileX, int pTileY)
{
    //--Given a room, changes the key indicator the door uses. -1 indicates there is no indicator.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Try to find the matching door.
            for(int i = 0; i < rCheckPack->mTilemapLayersTotal; i ++)
            {
                if(pIsSouthDoor && (rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_S_CLOSED || rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_S_OPEN))
                {
                    rCheckPack->mDoorSXTile = pTileX;
                    rCheckPack->mDoorSYTile = pTileY;
                }
                else if(!pIsSouthDoor && (rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_E_CLOSED || rCheckPack->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_E_OPEN))
                {
                    rCheckPack->mDoorEXTile = pTileX;
                    rCheckPack->mDoorEYTile = pTileY;
                }
            }

            //--Finish up.
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::RegisterTilemapIndicator(float pX, float pY, float pZ, const char *pName, int pTileX, int pTileY, int pPriority)
{
    //--Registers a new tilemap indicator for the requested room.
    if(!pName) return;
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--If the pack already exists, do nothing.
            if(rCheckPack->mEntityIndicators->GetElementByName(pName))
            {

            }
            //--Create and register a new pack.
            else
            {
                //--Allocate.
                SetMemoryData(__FILE__, __LINE__);
                AutomapPackTilemapLayer *nPack = (AutomapPackTilemapLayer *)starmemoryalloc(sizeof(AutomapPackTilemapLayer));
                nPack->Initialize();
                nPack->mX = pTileX;
                nPack->mY = pTileY;
                nPack->mSlotOffset = 0;
                nPack->mRotationFlags = AM_NO_MOD;
                nPack->mPriority = pPriority;
                rCheckPack->mEntityIndicators->AddElementAsTail(pName, nPack, &FreeThis);

                //--Flag this to instantly move to destination, otherwise it looks odd as it slides across the map.
                nPack->mInstantMoveToDest = true;

                //--Resolve texture X/Y.
                float cTxL = mTexXPerTile * nPack->mX;
                float cTxT = mTexYPerTile * nPack->mY;
                float cTxR = cTxL + mTexXPerTile;
                float cTxB = cTxT + mTexYPerTile;
                cTxT = 1.0f - cTxT;
                cTxB = 1.0f - cTxB;
                nPack->mTxDim.Set(cTxL, cTxT, cTxR, cTxB);
            }

            //--Rebuild indicator positions for the new indicator.
            mRebuildIndicators = true;

            //--Finish up.
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::UnregisterTilemapIndicator(float pX, float pY, float pZ, const char *pName)
{
    //--Removes the named package from the room.
    if(!pName) return;
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Rebuild indicator positions, as stacked indicators may need to move up.
            mRebuildIndicators = true;

            //--Remove.
            rCheckPack->mEntityIndicators->RemoveElementS(pName);
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::UnregisterAllTilemapIndicators()
{
    //--Removes all packages from all rooms. Used during the loading sequence, leaves no loose ends.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        rCheckPack->mEntityIndicators->ClearList();
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Rebuild indicator positions, since new ones are likely to populate.
    mRebuildIndicators = true;
}
void TextLevel::ModifyTilemapIndicator(float pX, float pY, float pZ, const char *pName, int pTileX, int pTileY, int pPriority)
{
    //--Modifies the named indicator. This is usually to change which tile it is using.
    if(!pName) return;

    //--Iterate.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Locate.
            AutomapPackTilemapLayer *rModLayer = (AutomapPackTilemapLayer *)rCheckPack->mEntityIndicators->GetElementByName(pName);
            if(rModLayer)
            {
                //--Set.
                rModLayer->mX = pTileX;
                rModLayer->mY = pTileY;
                rModLayer->mPriority = pPriority;

                //--Resolve texture X/Y.
                float cTxL = mTexXPerTile * rModLayer->mX;
                float cTxT = mTexYPerTile * rModLayer->mY;
                float cTxR = cTxL + mTexXPerTile;
                float cTxB = cTxT + mTexYPerTile;
                cTxT = 1.0f - cTxT;
                cTxB = 1.0f - cTxB;
                rModLayer->mTxDim.Set(cTxL, cTxT, cTxR, cTxB);
            }

            //--Because the priority can change the position of the indicator, we need to rebuild positions.
            mRebuildIndicators = true;

            //--Finish up.
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::ModifyTilemapIndicatorHostility(float pX, float pY, float pZ, const char *pName, int pHostileX, int pHostileY)
{
    //--Modifies the named indicator's hostility. Pass -1 for either to disable the hostility indicator.
    if(!pName) return;

    //--Iterate.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Locate.
            AutomapPackTilemapLayer *rModLayer = (AutomapPackTilemapLayer *)rCheckPack->mEntityIndicators->GetElementByName(pName);
            if(rModLayer)
            {
                if(pHostileX < 0 || pHostileY < 0)
                {
                    rModLayer->mHasHostilityMarker = false;
                }
                else
                {
                    rModLayer->mHasHostilityMarker = true;
                    rModLayer->mHostilityX = pHostileX;
                    rModLayer->mHostilityY = pHostileY;
                }

                //--Compute, store.
                float cHosTxL = mTexXPerTile * rModLayer->mHostilityX;
                float cHosTxT = mTexYPerTile * rModLayer->mHostilityY;
                float cHosTxR = cHosTxL + mTexXPerTile;
                float cHosTxB = cHosTxT + mTexYPerTile;
                cHosTxT = 1.0f - cHosTxT;
                cHosTxB = 1.0f - cHosTxB;
                rModLayer->mHsDim.Set(cHosTxL, cHosTxT, cHosTxR, cHosTxB);
            }

            //--Finish up.
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::MoveTilemapIndicator(float pX, float pY, float pZ, const char *pName, float pNewX, float pNewY, float pNewZ)
{
    //--Moves the package to a different room. If the other room doesn't exist, it gets scrapped.
    if(!pName) return;

    //--Storage.
    AutomapPack *rPreviousOwnerPack = NULL;
    AutomapPackTilemapLayer *tStoragePack = NULL;

    //--Try to find the room that owns the indicator in question.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            //--Check if it exists. If not, fail.
            void *rLocatePtr = rCheckPack->mEntityIndicators->GetElementByName(pName);
            if(!rLocatePtr)
            {
                mAutomapList->PopIterator();
                return;
            }

            //--Liberate it and move it to the storage pack.
            rPreviousOwnerPack = rCheckPack;
            rCheckPack->mEntityIndicators->SetRandomPointerToThis(rLocatePtr);
            tStoragePack = (AutomapPackTilemapLayer *)rCheckPack->mEntityIndicators->LiberateRandomPointerEntry();
            mAutomapList->PopIterator();
            break;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--If there's no storage pack, the room was never found. Exit.
    if(!tStoragePack) return;

    //--Regardless of whether a new destination is found, we need to rebuild indicator data.
    mRebuildIndicators = true;

    //--Register the pack to the new room.
    rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Match.
        if(rCheckPack->mX == pNewX && rCheckPack->mY == pNewY && rCheckPack->mZ == pNewZ)
        {
            //--If this was the player's pack, set the screen translation.
            if(tStoragePack->mPriority == 0)
            {
                mPlayerMoveTimer = 0;
                mPlayerMoveTimerMax = 15;
                mScreenTranslationX = (pX - pNewX) * mTilemapXSizePerTile;
                mScreenTranslationY = (pY - pNewY) * mTilemapYSizePerTile;
            }
            //--Otherwise, the indicator moves.
            else
            {
                tStoragePack->rPreviousRoom = rPreviousOwnerPack;
                tStoragePack->mMoveTimer = 0;
                tStoragePack->mMoveTimerMax = 150;
                tStoragePack->mXStart = pX * mTilemapXSizePerTile;
                tStoragePack->mYStart = pY * mTilemapYSizePerTile;
                tStoragePack->mXTarget = pNewX * mTilemapXSizePerTile;
                tStoragePack->mYTarget = pNewY * mTilemapYSizePerTile;
            }

            //--If the pack already exists, do nothing.
            if(rCheckPack->mEntityIndicators->GetElementByName(pName))
            {
                delete tStoragePack;
            }
            //--Create and register a new pack.
            else
            {
                rCheckPack->mEntityIndicators->AddElementAsTail(pName, tStoragePack, &FreeThis);
            }

            //--Finish up.
            mAutomapList->PopIterator();
            return;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Didn't find the destination room. Delete the pack.
    delete tStoragePack;
}
void TextLevel::ClearRoomFlags(float pX, float pY, float pZ)
{
    //--Clears the display flags for a given room.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            rCheckPack->mHasEnemyIndicator = false;
            rCheckPack->mHasFriendlyIndicator = false;
            rCheckPack->mHasExaminableIndicator = false;
            rCheckPack->mHasItemIndicator = false;
        }
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::RegisterAnimationToRoom(float pX, float pY, float pZ, const char *pAnimationName)
{
    //--Registers an animation to the requested room.
    void *rAnimation = mTilesetAnimations->GetElementByName(pAnimationName);
    if(!rAnimation) return;

    //--Find the room and register it.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        if(rCheckPack->mX == pX && rCheckPack->mY == pY && rCheckPack->mZ == pZ)
        {
            rCheckPack->mrAnimationsList->AddElement(pAnimationName, rAnimation);
        }
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::FinalizeWorld()
{
    //--Once all rooms have been registered and set up, generates a fixed-size 3D array. This
    //  allows for extremely fast lookups when building visibility.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    if(!rCheckPack) return;

    //--Set sizes to the first pack's variables.
    int tLoX = (int)rCheckPack->mX;
    int tHiX = (int)rCheckPack->mX;
    int tLoY = (int)rCheckPack->mY;
    int tHiY = (int)rCheckPack->mY;
    int tLoZ = (int)rCheckPack->mZ;
    int tHiZ = (int)rCheckPack->mZ;

    //--Iterate across all packs.
    rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    while(rCheckPack)
    {
        //--Store lowest/highest.
        if(rCheckPack->mX < tLoX) tLoX = rCheckPack->mX;
        if(rCheckPack->mX > tHiX) tHiX = rCheckPack->mX;
        if(rCheckPack->mY < tLoY) tLoY = rCheckPack->mY;
        if(rCheckPack->mY > tHiY) tHiY = rCheckPack->mY;
        if(rCheckPack->mZ < tLoZ) tLoZ = rCheckPack->mZ;
        if(rCheckPack->mZ > tHiZ) tHiZ = rCheckPack->mZ;

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Pad the X and Y values. This makes edge-checking safer.
    tLoX --;
    tLoY --;
    tHiX ++;
    tHiY ++;

    //--Compute sizes.
    mWorldOffsetX = tLoX;
    mWorldOffsetY = tLoY;
    mWorldOffsetZ = tLoZ;
    mWorldSizeX = (tHiX - tLoX) + 1;
    mWorldSizeY = (tHiY - tLoY) + 1;
    mWorldSizeZ = (tHiZ - tLoZ) + 1;

    //--Allocate space.
    SetMemoryData(__FILE__, __LINE__);
    mrFixedWorld = (AutomapPack ****)starmemoryalloc(sizeof(AutomapPack ***) * mWorldSizeX);
    for(int x = 0; x < mWorldSizeX; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mrFixedWorld[x] = (AutomapPack ***)starmemoryalloc(sizeof(AutomapPack **) * mWorldSizeY);
        for(int y = 0; y < mWorldSizeY; y ++)
        {
            SetMemoryData(__FILE__, __LINE__);
            mrFixedWorld[x][y] = (AutomapPack **)starmemoryalloc(sizeof(AutomapPack *) * mWorldSizeZ);
            for(int z = 0; z < mWorldSizeZ; z ++)
            {
                mrFixedWorld[x][y][z] = NULL;
            }
        }
    }

    //--Iterate again, this time placing rooms where they belong.
    rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Resolve positions and place.
        int tX = ((int)rCheckPack->mX) - mWorldOffsetX;
        int tY = ((int)rCheckPack->mY) - mWorldOffsetY;
        int tZ = ((int)rCheckPack->mZ) - mWorldOffsetZ;
        mrFixedWorld[tX][tY][tZ] = rCheckPack;

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Once all packs are in place, set door flags.
    rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Resolve positions and place.
        int tX = ((int)rCheckPack->mX) - mWorldOffsetX;
        int tY = ((int)rCheckPack->mY) - mWorldOffsetY;
        int tZ = ((int)rCheckPack->mZ) - mWorldOffsetZ;

        //--North door check.
        if(tY > 0 && mrFixedWorld[tX][tY-1][tZ])
        {
            AutomapPack *rNorthPack = mrFixedWorld[tX][tY-1][tZ];
            for(int i = 0; i < rNorthPack->mTilemapLayersTotal; i ++)
            {
                //--Skip unflagged cases.
                int tSlot = rNorthPack->mTilemapLayers[i].mSlotOffset;
                if(tSlot == AM_SLOT_DOOR_S_CLOSED || tSlot == AM_SLOT_DOOR_S_OPEN)
                {
                    rCheckPack->mDoorHighlightPossible |= AMP_NORTH;
                    //fprintf(stderr, "North door at %p %i %i %i - %i\n", rCheckPack, tX, tY, tZ, rCheckPack->mDoorHighlightPossible);
                }
            }
        }
        //--West.
        if(tX > 0 && mrFixedWorld[tX-1][tY][tZ])
        {
            AutomapPack *rWestPack = mrFixedWorld[tX-1][tY][tZ];
            for(int i = 0; i < rWestPack->mTilemapLayersTotal; i ++)
            {
                //--Skip unflagged cases.
                int tSlot = rWestPack->mTilemapLayers[i].mSlotOffset;
                if(tSlot == AM_SLOT_DOOR_E_CLOSED || tSlot == AM_SLOT_DOOR_E_OPEN)
                {
                    rCheckPack->mDoorHighlightPossible |= AMP_WEST;
                    //fprintf(stderr, "West door at %p %i %i %i - %i\n", rCheckPack, tX, tY, tZ, rCheckPack->mDoorHighlightPossible);
                }
            }
        }

        //--South/East door check.
        for(int i = 0; i < rCheckPack->mTilemapLayersTotal; i ++)
        {
            //--Skip unflagged cases.
            int tSlot = rCheckPack->mTilemapLayers[i].mSlotOffset;
            if(tSlot == AM_SLOT_DOOR_S_CLOSED || tSlot == AM_SLOT_DOOR_S_OPEN)
            {
                rCheckPack->mDoorHighlightPossible |= AMP_SOUTH;
                //fprintf(stderr, "South door at %p %i %i %i - %i\n", rCheckPack, tX, tY, tZ, rCheckPack->mDoorHighlightPossible);
            }
            else if(tSlot == AM_SLOT_DOOR_E_CLOSED || tSlot == AM_SLOT_DOOR_E_OPEN)
            {
                rCheckPack->mDoorHighlightPossible |= AMP_EAST;
                //fprintf(stderr, "East door at %p %i %i %i - %i\n", rCheckPack, tX, tY, tZ, rCheckPack->mDoorHighlightPossible);
            }
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::CleanUpTempStructures()
{
    mTempDoorList->ClearList();
    mTempAudioList->ClearList();
    mTempPatrolList->ClearList();
    mTempLocationList->ClearList();
}
void TextLevel::RecomputeFadeVisibility()
{
    //--Scans across all world addresses. Figures out which fade components should be applied to
    //  which rooms. Rooms that are not visible or explored don't fade.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Store.
        int tOldCode = rCheckPack->mFadeCode;
        int tOldInsetCode = rCheckPack->mInsetCode;

        //--Room is not visible or explored.
        if(!rCheckPack->mIsDiscovered || !rCheckPack->mIsVisibleNow)
        {
            //--Set.
            rCheckPack->mFadeCode = AM_FADE_OVER;
            rCheckPack->mInsetCode = AM_FADE_NONE;

            //--Fade properties changed? Reset timer, store changes.
            if(rCheckPack->mFadeCode != tOldCode)
            {
                rCheckPack->mPrevFadeTimer = rCheckPack->mFadeTimer;
                rCheckPack->mPrevFadeCode = tOldCode;
                rCheckPack->mFadeTimer = 0;
            }

            //--Inset properties changed.
            if(rCheckPack->mInsetCode != tOldInsetCode)
            {
                rCheckPack->mPrevInsetTimer = rCheckPack->mInsetTimer;
                rCheckPack->mPrevInsetCode = tOldInsetCode;
                rCheckPack->mInsetTimer = 0;
            }

            rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
            continue;
        }

        //--Get the room's coordinates in the fixed map.
        int tX = ((int)rCheckPack->mX) - mWorldOffsetX;
        int tY = ((int)rCheckPack->mY) - mWorldOffsetY;
        int tZ = ((int)rCheckPack->mZ) - mWorldOffsetZ;

        //--Set flags.
        bool tIsNVisible = false;
        bool tIsEVisible = false;
        bool tIsSVisible = false;
        bool tIsWVisible = false;

        //--Resolve values.
        if(mrFixedWorld[tX][tY-1][tZ] && mrFixedWorld[tX][tY-1][tZ]->mIsVisibleNow) tIsNVisible = true;
        if(mrFixedWorld[tX+1][tY][tZ] && mrFixedWorld[tX+1][tY][tZ]->mIsVisibleNow) tIsEVisible = true;
        if(mrFixedWorld[tX][tY+1][tZ] && mrFixedWorld[tX][tY+1][tZ]->mIsVisibleNow) tIsSVisible = true;
        if(mrFixedWorld[tX-1][tY][tZ] && mrFixedWorld[tX-1][tY][tZ]->mIsVisibleNow) tIsWVisible = true;

        //--North is invisible:
        if(!tIsNVisible)
        {
            //--East is invisible:
            if(!tIsEVisible)
            {
                //--South is invisible:
                if(!tIsSVisible)
                {
                    //--West is invisible. None are visible, full square.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_FULL;
                    }
                    //--West is visible. Alley closing east.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_ALLEY_E;
                    }
                }
                //--South is visible.
                else
                {
                    //--West is invisible. Alley closing north.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_ALLEY_N;
                    }
                    //--South+West is visible. NE Corner.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_CORN_NE;
                    }
                }
            }
            //--East is visible.
            else
            {
                //--South is invisible:
                if(!tIsSVisible)
                {
                    //--West is invisible. East is only visible.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_ALLEY_W;
                    }
                    //--East + West are visible. Horizontal lines.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_TWOLINE_H;
                    }
                }
                //--South is visible.
                else
                {
                    //--West is invisible. East + South are visible, NW corner.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_CORN_NW;
                    }
                    //--ESW are visible. North line.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_LINE_N;
                    }
                }
            }
        }
        //--North is visible.
        else
        {
            //--East is invisible:
            if(!tIsEVisible)
            {
                //--South is invisible:
                if(!tIsSVisible)
                {
                    //--West is invisible. North is only visible, alley closing S.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_ALLEY_S;
                    }
                    //--West is visible. North and West are visible, corner SE.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_CORN_SE;
                    }
                }
                //--South is visible.
                else
                {
                    //--West is invisible. North and south visible, vertical lines.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_TWOLINE_V;
                    }
                    //--South, West, and North are visible. East line.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_LINE_E;
                    }
                }
            }
            //--East is visible.
            else
            {
                //--South is invisible:
                if(!tIsSVisible)
                {
                    //--West is invisible. East/North visible, corner SW.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_CORN_SW;
                    }
                    //--East, North, and West are visible. Line south.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_LINE_S;
                    }
                }
                //--South is visible.
                else
                {
                    //--West is invisible. West is only invisible, line west.
                    if(!tIsWVisible)
                    {
                        rCheckPack->mFadeCode = AM_FADE_LINE_W;
                    }
                    //--All visible. No fade.
                    else
                    {
                        rCheckPack->mFadeCode = AM_FADE_NONE;
                    }
                }
            }
        }

        //--Fade properties changed? Reset timer, store changes.
        if(rCheckPack->mFadeCode != tOldCode)
        {
            rCheckPack->mPrevFadeTimer = rCheckPack->mFadeTimer;
            rCheckPack->mPrevFadeCode = tOldCode;
            rCheckPack->mPrevInsetCode = rCheckPack->mInsetCode;
            rCheckPack->mFadeTimer = 0;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Insets. These compute after all other packs have computed since they
    //  need to complement one another.
    rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Get the room's coordinates in the fixed map.
        int tX = ((int)rCheckPack->mX) - mWorldOffsetX;
        int tY = ((int)rCheckPack->mY) - mWorldOffsetY;
        int tZ = ((int)rCheckPack->mZ) - mWorldOffsetZ;

        //--Set flags.
        bool tIsNVisible = false;
        bool tIsEVisible = false;
        bool tIsSVisible = false;
        bool tIsWVisible = false;
        int tOldInsetCode = rCheckPack->mInsetCode;
        rCheckPack->mInsetCode = AM_FADE_INSET_NONE;

        //--Resolve values.
        if(mrFixedWorld[tX][tY-1][tZ] && mrFixedWorld[tX][tY-1][tZ]->mIsVisibleNow) tIsNVisible = true;
        if(mrFixedWorld[tX+1][tY][tZ] && mrFixedWorld[tX+1][tY][tZ]->mIsVisibleNow) tIsEVisible = true;
        if(mrFixedWorld[tX][tY+1][tZ] && mrFixedWorld[tX][tY+1][tZ]->mIsVisibleNow) tIsSVisible = true;
        if(mrFixedWorld[tX-1][tY][tZ] && mrFixedWorld[tX-1][tY][tZ]->mIsVisibleNow) tIsWVisible = true;

        //--Northwest inset:
        if(tIsNVisible && tIsWVisible)
        {
            int tCodeN = mrFixedWorld[tX][tY-1][tZ]->mFadeCode;
            int tCodeW = mrFixedWorld[tX-1][tY][tZ]->mFadeCode;
            if((tCodeN == AM_FADE_LINE_W || tCodeN == AM_FADE_CORN_NW || tCodeN == AM_FADE_TWOLINE_V || tCodeN == AM_FADE_ALLEY_N) &&
               (tCodeW == AM_FADE_LINE_N || tCodeW == AM_FADE_CORN_NW || tCodeW == AM_FADE_TWOLINE_H || tCodeW == AM_FADE_ALLEY_W))
            {
                rCheckPack->mInsetCode |= AM_FADE_INSET_NW;
            }
        }

        //--Northeast inset:
        if(tIsNVisible && tIsEVisible)
        {
            int tCodeN = mrFixedWorld[tX][tY-1][tZ]->mFadeCode;
            int tCodeE = mrFixedWorld[tX+1][tY][tZ]->mFadeCode;
            if((tCodeN == AM_FADE_LINE_E || tCodeN == AM_FADE_CORN_NE || tCodeN == AM_FADE_TWOLINE_V || tCodeN == AM_FADE_ALLEY_N) &&
               (tCodeE == AM_FADE_LINE_N || tCodeE == AM_FADE_CORN_NE || tCodeE == AM_FADE_TWOLINE_H || tCodeE == AM_FADE_ALLEY_E))
            {
                rCheckPack->mInsetCode |= AM_FADE_INSET_NE;
            }
        }

        //--Southeast inset:
        if(tIsSVisible && tIsEVisible)
        {
            int tCodeS = mrFixedWorld[tX][tY+1][tZ]->mFadeCode;
            int tCodeE = mrFixedWorld[tX+1][tY][tZ]->mFadeCode;
            if((tCodeS == AM_FADE_LINE_E || tCodeS == AM_FADE_CORN_SE || tCodeS == AM_FADE_TWOLINE_V || tCodeS == AM_FADE_ALLEY_S) &&
               (tCodeE == AM_FADE_LINE_S || tCodeE == AM_FADE_CORN_SE || tCodeE == AM_FADE_TWOLINE_H || tCodeE == AM_FADE_ALLEY_E))
            {
                rCheckPack->mInsetCode |= AM_FADE_INSET_SE;
            }
        }

        //--Southwest inset:
        if(tIsSVisible && tIsWVisible)
        {
            int tCodeS = mrFixedWorld[tX][tY+1][tZ]->mFadeCode;
            int tCodeW = mrFixedWorld[tX-1][tY][tZ]->mFadeCode;
            if((tCodeS == AM_FADE_LINE_W || tCodeS == AM_FADE_CORN_SW || tCodeS == AM_FADE_TWOLINE_V || tCodeS == AM_FADE_ALLEY_S) &&
               (tCodeW == AM_FADE_LINE_S || tCodeW == AM_FADE_CORN_SW || tCodeW == AM_FADE_TWOLINE_H || tCodeW == AM_FADE_ALLEY_W))
            {
                rCheckPack->mInsetCode |= AM_FADE_INSET_SW;
            }
        }

        //--Inset properties changed.
        if(rCheckPack->mInsetCode != tOldInsetCode)
        {
            rCheckPack->mPrevInsetTimer = rCheckPack->mInsetTimer;
            rCheckPack->mPrevInsetCode = tOldInsetCode;
            rCheckPack->mInsetTimer = 0;
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::SetRenderingZ(int pZLevel)
{
    mCurrentRenderingZ = pZLevel;
}
void TextLevel::SetUnderRenderingZ(int pZLevelLo, int pZLevelHi)
{
    mUnderRenderingZLo = pZLevelLo;
    mUnderRenderingZHi = pZLevelHi;
}
