//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureLevel.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioPackage.h"
#include "AudioManager.h"
#include "DebugManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///========================================== System ==============================================
AdvCombat::AdvCombat()
{
    mHasInitializedAdvCombat = false;
    Initialize();
}
AdvCombat::~AdvCombat()
{
    Disassemble();
}

///===================================== Property Queries =========================================
bool AdvCombat::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVCOMBAT) return true;
    return false;
}
bool AdvCombat::IsActive()
{
    return mIsActive;
}
bool AdvCombat::IsStoppingWorldUpdate()
{
    return mIsActive;
}
bool AdvCombat::DoesPlayerHaveInitiative()
{
    return mPlayerGainedInitiative;
}
bool AdvCombat::IsAnythingAnimating()
{
    //--If any of the animations have an instance on the field, return true.
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->PushIterator();
    while(rAnimation)
    {
        if(rAnimation->HasAnyInstances())
        {
            mCombatAnimations->PopIterator();
            return true;
        }
        rAnimation = (AdvCombatAnimation *)mCombatAnimations->AutoIterate();
    }

    //--All checks failed, return false.
    return false;
}
bool AdvCombat::IsAnyoneMoving()
{
    //--If any player party members are moving, return true.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        if(rEntity->IsAnimating() || rEntity->IsKnockingOut())
        {
            mrCombatParty->PopIterator();
            return true;
        }
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--If any enemy party members are moving, return true.
    rEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rEntity)
    {
        if(rEntity->IsAnimating() || rEntity->IsKnockingOut())
        {
            mEnemyRoster->PopIterator();
            return true;
        }
        rEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--All checks failed, return false.
    return false;
}
int AdvCombat::GetSkillCatalystSlots()
{
    return mSkillCatalystExtension;
}
int AdvCombat::GetPartyGroupingID(uint32_t pUniqueID)
{
    ///--[Documentation]
    //--Returns from series AC_PARTY_GROUP_[X] based on which party the given ID is found in.
    //  Entities *can* switch parties during combat.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        if(rEntity->GetID() == pUniqueID) {mrCombatParty->PopIterator(); return AC_PARTY_GROUP_PARTY; }
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Active enemy party.
    rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEntity)
    {
        if(rEntity->GetID() == pUniqueID) {mrEnemyCombatParty->PopIterator(); return AC_PARTY_GROUP_ENEMY; }
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    //--Enemy reinforcements.
    rEntity = (AdvCombatEntity *)mrEnemyReinforcements->PushIterator();
    while(rEntity)
    {
        if(rEntity->GetID() == pUniqueID) {mrEnemyReinforcements->PopIterator(); return AC_PARTY_GROUP_REINFORCEMENTS; }
        rEntity = (AdvCombatEntity *)mrEnemyReinforcements->AutoIterate();
    }

    //--Enemy graveyard.
    rEntity = (AdvCombatEntity *)mrEnemyGraveyard->PushIterator();
    while(rEntity)
    {
        if(rEntity->GetID() == pUniqueID) {mrEnemyGraveyard->PopIterator(); return AC_PARTY_GROUP_GRAVEYARD; }
        rEntity = (AdvCombatEntity *)mrEnemyGraveyard->AutoIterate();
    }

    //--ID not found.
    return AC_PARTY_GROUP_NONE;
}
FieldAbility *AdvCombat::GetFieldAbility(int pSlot)
{
    if(pSlot < 0 || pSlot >= ADVCOMBAT_FIELD_ABILITY_SLOTS) return NULL;
    return rActiveFieldAbilities[pSlot];
}
bool AdvCombat::IsAbilityUnlocked(const char *pCharacter, const char *pJob, const char *pAbility)
{
    //--Check the character roster.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->GetElementByName(pCharacter);
    if(!rEntity) return false;

    //--Find the job.
    StarLinkedList *rJobList = rEntity->GetJobList();
    AdvCombatJob *rJob = (AdvCombatJob *)rJobList->GetElementByName(pJob);
    if(!rJob) return false;

    //--Find the ability.
    StarLinkedList *rAbilityList = rJob->GetAbilityList();
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementByName(pAbility);
    if(!rAbility) return false;

    //--Finale.
    return rAbility->IsUnlocked();
}
bool AdvCombat::IsUnretreatable()
{
    return mIsUnretreatable;
}
bool AdvCombat::IsUnsurrenderable()
{
    return mIsUnsurrenderable;
}
bool AdvCombat::IsUnloseable()
{
    return mIsUnloseable;
}
int AdvCombat::IsShowingDetailedDescriptions()
{
    return mShowDetailedAbilityDescriptions;
}
bool AdvCombat::IsEntityInActiveTargetCluster(void *pPtr)
{
    //--Returns true if the requested pointer is in the active target cluster. If no target cluster
    //  is active for any reason, or the entity is not present, returns false.
    if(!pPtr) return false;

    //--No target cluster list.
    if(!mTargetClusters) return false;

    //--Target cluster is out of range or does not exist.
    TargetCluster *rActiveCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
    if(!rActiveCluster) return false;

    //--Cluster exists, return whether or not the entity is in it.
    return rActiveCluster->IsElementInCluster(pPtr);
}
bool AdvCombat::IsIntroduction()
{
    return mIsIntroduction;
}

///======================================== Manipulators ==========================================
void AdvCombat::Activate()
{
    mIsActive = true;
}
void AdvCombat::Deactivate()
{
    //--Flag.
    mIsActive = false;
}
void AdvCombat::SetDefaultCombatMusic(const char *pMusicName, float pStartPoint)
{
    ResetString(mDefaultCombatMusic, pMusicName);
    mCombatMusicStart = pStartPoint;
}
void AdvCombat::SetDefaultVictoryMusic(const char *pMusicName)
{
    ResetString(mDefaultVictoryMusic, pMusicName);
}
void AdvCombat::SetNextCombatMusic(const char *pMusicName, float pStartPoint)
{
    ResetString(mNextCombatMusic, pMusicName);
    mNextCombatMusicStart = pStartPoint;
}
void AdvCombat::SetUnloseable(bool pFlag)
{
    mIsUnloseable = pFlag;
}
void AdvCombat::SetUnwinnable(bool pFlag)
{
    mIsUnwinnable = pFlag;
}
void AdvCombat::SetUnretreatable(bool pFlag)
{
    mIsUnretreatable = pFlag;
}
void AdvCombat::SetUnsurrenderable(bool pFlag)
{
    mIsUnsurrenderable = pFlag;
}
void AdvCombat::SpawnTitle(int pTicks, const char *pText)
{
    mShowTitle = false;
    mTitleTicks = 1;
    mTitleTicksMax = 1;
    free(mTitleText);
    mTitleText = NULL;
    if(pTicks < 1 || !pText) return;

    mShowTitle = true;
    mTitleTicks = 0;
    mTitleTicksMax = pTicks;
    ResetString(mTitleText, pText);
}
void AdvCombat::MarkEventCanRun(bool pFlag)
{
    mEventCanRun = pFlag;
}
void AdvCombat::ResumeMusic()
{
    ///--[Documentation and Setup]
    //--Restore music to where it was before combat began.
    AudioManager *rAudioManager = AudioManager::Fetch();

    ///--[Overworld Flag]
    //--If this flag is set, this was a play-through where the combat music never starts and the overworld theme
    //  keeps playing.
    if(mQuietBackgroundMusicForVictory)
    {
        //--Player.
        if(rPreviousMusicWhenReplaying)
        {
            const char *rMusicName = rAudioManager->GetNameOfMusicPack(rPreviousMusicWhenReplaying);
            rAudioManager->PlayMusicStartingAt(rMusicName, mPreviousTimeWhenReplaying);
        }

        //--Clear.
        mQuietBackgroundMusicForVictory = false;
        rPreviousMusicWhenReplaying = NULL;
        mPreviousTimeWhenReplaying = 0.0f;
    }
    ///--[Non-layered Music]
    //--If music is not layering, start playback wherever it left off.
    else if(!AdventureLevel::IsLayeringMusic())
    {
        //--No music:
        if(!AdventureLevel::xLevelMusic)
        {
            rAudioManager->FadeMusic(-30);
        }
        //--Has music:
        else
        {
            if(mEndCombatTracksTotal > 0)
                rAudioManager->PlayMusicStartingAt(AdventureLevel::xLevelMusic, mEndCombatMusicResume[0]);
        }
    }
    ///--[Layered Music, Mandated Max Intensity]
    //--If layering, each of the tracks needs to restart where it was. If the combat music is just the max-intensity then
    //  nothing happens here, the music is already playing.
    else if(AdventureLevel::IsLayeringMusic() && !AdventureLevel::xIsCombatMaxIntensity)
    {
        //--Setup.
        AudioManager *rAudioManager = AudioManager::Fetch();
        for(int i = 0; i < mEndCombatTracksTotal; i ++)
        {
            //--Check the package. If it exists, order it to play and seek.
            AudioPackage *rLayer = rAudioManager->GetMusicPack(AdventureLevel::xLayerNames[i]);
            if(rLayer)
            {
                rLayer->Stop();
                rLayer->Play();
                rLayer->SeekTo(mEndCombatMusicResume[i]);
            }
        }
    }
    ///--[Layered Music]
    //--Layering, but mandating intensity. Set to -1.0f to disable, then restart all the layer packs
    //  which were stopped for the victory music.
    else
    {
        //--Level must exist.
        AdventureLevel *rCheckLevel = AdventureLevel::Fetch();

        //--Flag.
        AdventureLevel::xCombatMandatedIntensity = -1.0f;

        //--Restart all music packs.
        for(int i = 0; i < mEndCombatTracksTotal; i ++)
        {
            //--Check the package. If it exists, order it to play and seek.
            AudioPackage *rLayer = rAudioManager->GetMusicPack(AdventureLevel::xLayerNames[i]);
            if(rLayer)
            {
                rLayer->Stop();
                rLayer->Play();
                rLayer->SeekTo(mEndCombatMusicResume[i]);
            }
        }

        //--Reduce the intensity since the enemy is defeated.
        AdventureLevel::xClosestEnemy = 10000.0f;
        AdventureLevel::xZeroOffIntensityTicks = 1;
        if(rCheckLevel) rCheckLevel->UpdateMusicLayering();
    }
}
void AdvCombat::SetFieldAbility(int pSlot, FieldAbility *pAbility)
{
    if(pSlot < 0 || pSlot >= ADVCOMBAT_FIELD_ABILITY_SLOTS) return;
    rActiveFieldAbilities[pSlot] = pAbility;
}
void AdvCombat::RegisterExtraScriptPack(ExtraScriptPack *pPackage)
{
    if(!pPackage) return;
    mExtraScripts->AddElement("X", pPackage, &ExtraScriptPack::DeleteThis);
}
void AdvCombat::SetSkillCatalystSlots(int pSlots)
{
    mSkillCatalystExtension = pSlots;
}
void AdvCombat::SetPostCombatHandler(const char *pScript)
{
    ResetString(mPostScriptHandler, pScript);
}
void AdvCombat::SetDetailedDescriptionFlag(int pFlag)
{
    mShowDetailedAbilityDescriptions = pFlag;
}
void AdvCombat::SetAutoWinPath(const char *pPath)
{
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mAutoWinPath, NULL);
    }
    else
    {
        ResetString(mAutoWinPath, pPath);
    }
}
void AdvCombat::AddScriptToExec(const char *pPath)
{
    if(!pPath || !strcasecmp(pPath, "Null")) return;
    mCutsceneExecList->AddElementAsTail("X", InitializeString(pPath), &FreeThis);
}
void AdvCombat::SetStatisticsPath(const char *pPath)
{
    if(!pPath) return;
    ResetString(mStatisticsPath, pPath);
}
void AdvCombat::SetUnitDefeatScript(const char *pPath)
{
    ResetString(mDefeatedUnitScript, pPath);
}

///======================================== Core Methods ==========================================
//#define COMBAT_HEAL_DEBUG
#ifdef COMBAT_HEAL_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

void AdvCombat::HealFromDoctorBag(int pPartyIndex)
{
    ///--[Documentation and Setup]
    //--Heals the given entity from the doctor bag. If -1 is passed, then all entities are healed
    //  equally as much as the bag can support, with remainders going to the lower entities.
    DebugPush(false, "Running Heal From Doctor Bag.\n");
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    int tDoctorBagCharges = rInventory->GetDoctorBagCharges();

    //--No charges, fail.
    if(tDoctorBagCharges < 1) return;

    //--Get potency.
    float tDoctorBagPotency = rInventory->GetDoctorBagPotency();
    if(tDoctorBagPotency <= 0.0f) return;

    //--Effective charges is charges times potency. This is the total percent that can be healed.
    int tEffectiveCharges = tDoctorBagCharges * tDoctorBagPotency;

    ///--[All-Heal]
    //--All-heal case.
    if(pPartyIndex == -1)
    {
        //--If there is exactly one party member, just heal them.
        if(mrActiveParty->GetListSize() == 1)
        {
            pPartyIndex = 0;
        }
        //--Otherwise, we need to split healing.
        else
        {
            //--Create an array to hold all healing values. Each charges is 1 unit of doctor bag
            //  and 1 percent HP. Potency modifies this amount.
            float tHealingSum = 0.0f;
            int tHealingArray[ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE];
            memset(tHealingArray, 0, sizeof(int) * ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE);

            //--Debug.
            DebugPrint("Doctor Bag Report, All Heal:\n");

            //--Run across all entities in the party. Check how much healing they need.
            for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
            {
                //--Verify the entity exists.
                AdvCombatEntity *rSlotEntity = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(i);
                if(!rSlotEntity) continue;

                //--Store value.
                int cHPPercent = rSlotEntity->GetHealthPercent() * 100;
                tHealingArray[i] = cHPPercent;
                tHealingSum += (100 - cHPPercent);

                //--Debug.
                DebugPrint(" %i: %i\n", i, tHealingArray[i]);
            }

            //--If we have enough to heal the entire party, do that here.
            if(tEffectiveCharges >= tHealingSum)
            {
                //--Storage.
                int tOriginalCharges = tEffectiveCharges;

                //--Debug.
                DebugPrint(" Able to fully heal all entities.\n");
                DebugPrint(" Charges present: %i\n", tDoctorBagCharges);
                DebugPrint(" Potency: %f\n", tDoctorBagPotency);
                DebugPrint(" Effective charges: %i\n", tEffectiveCharges);
                DebugPrint(" Charges needed: %i\n", tHealingSum);

                //--Loop across the party and apply healing.
                for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
                {
                    //--Verify the entity exists.
                    AdvCombatEntity *rSlotEntity = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(i);
                    if(!rSlotEntity) continue;

                    //--Store value.
                    int tHealingPercentNeeded = (100 - tHealingArray[i]);
                    tEffectiveCharges -= tHealingPercentNeeded;
                    rInventory->SetDoctorBagCharges(tDoctorBagCharges);
                    rSlotEntity->FullRestore();
                }

                //--Check how many effective charges are present.
                int tChargesSpent = tOriginalCharges - tEffectiveCharges;
                int tActualChargesSpent = ((float)tChargesSpent / tDoctorBagPotency);
                DebugPrint(" Effective charges after heal: %i\n", tEffectiveCharges);
                DebugPrint(" Charges spent: %i\n", tChargesSpent);
                DebugPrint(" Actual charges spent: %i\n", tActualChargesSpent);
                rInventory->SetDoctorBagCharges(tDoctorBagCharges - tActualChargesSpent);
                DebugPrint(" Bag charges after deduction: %i\n", rInventory->GetDoctorBagCharges());
                DebugPop("Done heal from doctorbag.\n");
                return;
            }

            //--Not enough to heal the whole party. In that case, heal everyone with priority to those who are weakest.
            DebugPrint(" Splitting healing.\n");
            memset(tHealingArray, 0, sizeof(int) * ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE);

            //--For each charge:
            for(int i = 0; i < tEffectiveCharges; i ++)
            {
                //--Iterate and find the entity with the lowest HP.
                int tLowestSlot = -1;
                int tLowestVal = 100;
                for(int p = 0; p < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; p ++)
                {
                    //--Verify the entity exists.
                    AdvCombatEntity *rSlotEntity = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(p);
                    if(!rSlotEntity) continue;

                    //--Get this entity's HP.
                    int cHPPercent = rSlotEntity->GetHealthPercent() * 100;

                    //--Check if it's the lowest.
                    if(tLowestSlot == -1 || cHPPercent + tHealingArray[p] < tLowestVal)
                    {
                        tLowestSlot = p;
                        tLowestVal = cHPPercent + tHealingArray[p];
                    }
                }

                //--Once the lowest slot is resolved, put a point of healing into the array. We can't heal the entity yet
                //  or they will lose HP due to rounding errors.
                if(tLowestSlot != -1) tHealingArray[tLowestSlot] ++;
            }

            //--Once all charges are spent, allocate them. Doctor bag is zeroed.
            rInventory->SetDoctorBagCharges(0);
            for(int p = 0; p < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; p ++)
            {
                //--Verify the entity exists.
                AdvCombatEntity *rSlotEntity = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(p);
                if(!rSlotEntity) continue;

                //--Get this entity's HP.
                float tHPPercent = rSlotEntity->GetHealthPercent();
                tHPPercent = tHPPercent + (float)(tHealingArray[p] / 100.0f);

                //--Set.
                rSlotEntity->SetHealthPercent(tHPPercent, true);
                DebugPrint(" Entity %i: %i\n", p, tHealingArray[p]);
            }

            DebugPop("Done heal from doctorbag.\n");
            return;
        }
    }

    //--Heal an individual character.
    AdvCombatEntity *rSlotEntity = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(pPartyIndex);
    if(!rSlotEntity)
    {
        DebugPop("Done heal from doctorbag, entity did not exist.\n");
        return;
    }

    //--Debug.
    DebugPrint("Doctor Bag Report:\n");

    //--Get how much, percentage-wise, the entity is missing.
    float cHPPercent = rSlotEntity->GetHealthPercent();
    int cChargesNeeded = 100 - (int)(cHPPercent * 100.0f);
    DebugPrint(" HP Percent: %f\n", cHPPercent);
    DebugPrint(" Charges Needed: %i\n", cChargesNeeded);

    //--Modify by potency.
    DebugPrint(" Potency: %f\n", tDoctorBagPotency);

    //--If we have enough charges in the doctor bag, fullheal the character.
    if(tEffectiveCharges >= cChargesNeeded)
    {
        //--Fullheal.
        rSlotEntity->FullRestore();

        //--Compute how many charges we spent.
        int tActualChargesSpent = ((float)cChargesNeeded / tDoctorBagPotency);
        DebugPrint(" Actual charges needed: %i\n", tActualChargesSpent);

        //--Subtract charges.
        rInventory->SetDoctorBagCharges(tDoctorBagCharges - tActualChargesSpent);
        DebugPrint(" Set charges from full heal: %i\n", rInventory->GetDoctorBagCharges());
    }
    //--We do not have enough charges to fullheal, so heal as much as possible.
    else
    {
        float tNewHPPercent = cHPPercent + ((float)tEffectiveCharges / 100.0f);
        rSlotEntity->SetHealthPercent(tNewHPPercent, true);
        rInventory->SetDoctorBagCharges(0);
        DebugPrint(" Set charges from partial heal: %i\n", rInventory->GetDoctorBagCharges());
        DebugPrint(" New HP percentage: %f\n", tNewHPPercent);
        DebugPrint(" Actual HP: %i / %i\n", rSlotEntity->GetHealth(), rSlotEntity->GetStatistic(STATS_HPMAX));
    }
    DebugPop("Done heal from doctorbag.\n");
}
bool AdvCombat::IsEntityInvolvedInEvent(AdvCombatEntity *pEntity, CombatEventPack *pEvent)
{
    //--Resolves if the entity provided should be involved in any way with the current action.
    if(!pEntity || !pEvent) return false;

    //--If the entity is acting, it's involved.
    void *rActingEntity = mrTurnOrder->GetElementBySlot(0);
    if(rActingEntity == pEntity) return true;

    //--If the entity is the originator of the event, it's involved.
    if(pEvent->rOriginator == pEntity) return true;

    //--If the entity is on the target list for the event, it's involved.
    if(pEvent->mTargetCluster)
    {
        if(pEvent->mTargetCluster->IsElementInCluster(pEntity))
        {
            return true;
        }
    }

    //--If the entity is on the "Additional Entities" list, it is involved.
    if(pEvent->mrAdditionalEntities)
    {
        void *rCheckPtr = pEvent->mrAdditionalEntities->PushIterator();
        while(rCheckPtr)
        {
            if(rCheckPtr == pEntity)
            {
                pEvent->mrAdditionalEntities->PopIterator();
                return true;
            }
            rCheckPtr = pEvent->mrAdditionalEntities->AutoIterate();
        }
    }

    //--All checks failed.
    return false;
}
bool AdvCombat::CheckKnockouts()
{
    //--If any party members got KO'd, handle that here. This is called after each event concludes.
    bool tAtLeastOneKnockout = false;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        //--Party member was knocked out this action. Begin animating.
        if(rEntity->CheckKnockout())
        {
            //--Flags.
            rEntity->BeginKnockout();
            tAtLeastOneKnockout = true;

            //--If this script is set, call it.
            if(mDefeatedUnitScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mDefeatedUnitScript, 1, "N", (float)rEntity->GetID());
            }
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
    rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEntity)
    {
        //--Party member was knocked out this action. Begin animating.
        if(rEntity->CheckKnockout())
        {
            //--Flags.
            rEntity->BeginKnockout();
            tAtLeastOneKnockout = true;

            //--If this script is set, call it.
            if(mDefeatedUnitScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mDefeatedUnitScript, 1, "N", (float)rEntity->GetID());
            }
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
    return tAtLeastOneKnockout;
}
void AdvCombat::CleanDataLibrary()
{
    //--When combat is over, cleans up temporary variables from the DataLibrary.
    DataLibrary::Fetch()->Purge("Root/Variables/Combat/");
}
void AdvCombat::CallStatisticsScript(int pCode)
{
    if(!mStatisticsPath) return;
    LuaManager::Fetch()->ExecuteLuaFile(mStatisticsPath, 1, "N", (float)pCode);
}
void AdvCombat::EnqueueSound(PlaySoundPackage *pPackage)
{
    ///--[Documentation]
    //--Puts the given sound name in a list. If that sound is already in the list, doesn't put it
    //  in the list. This is to prevent the same sound from playing too frequently.
    //--Takes ownership of the package. The name in the package should be the sound name, and will
    //  be duplicated as the name of the pack to check for duplicate plays.
    if(!pPackage) return;

    ///--[Error Check]
    //--Name must contain at least one character.
    if(pPackage->mSoundName[0] == '\0')
    {
        fprintf(stderr, "Attempted to enqueue sound with a blank name.\n");
        PlaySoundPackage::DeleteThis(pPackage);
        return;
    }

    ///--[Duplication Check]
    //--Check if the sound is already in the list.
    void *rCheckPtr = mSoundList->GetElementByName(pPackage->mSoundName);
    if(rCheckPtr)
    {
        PlaySoundPackage::DeleteThis(pPackage);
        return;
    }

    ///--[Register]
    //--Enqueue the sound with its sound name as its package name.
    mSoundList->AddElementAsTail(pPackage->mSoundName, pPackage, &PlaySoundPackage::DeleteThis);
}
void AdvCombat::PlayMonsterKnockout()
{
    //--Plays the monster knockout sound. Used so derived classes can remove that sound effect without having
    //  to override a lot of other functions.
    AudioManager::Fetch()->PlaySound("Combat|MonsterDie");
}
void AdvCombat::SwitchCombatPartyWithActivePartyMember(int pCombatSlot, const char *pActiveMemberName)
{
    ///--[Documentation]
    //--Given a combat slot, switches the character in that slot with the member of the active party
    //  provided, assuming it exists. The member originally in that slot is removed. This can also be
    //  used to append a new entry to the combat party if an empty slot was provided.
    if(!pActiveMemberName) return;

    //--Get the active member. Stop if it doesn't exist.
    AdvCombatEntity *rNewEntity = (AdvCombatEntity *)mrActiveParty->GetElementByName(pActiveMemberName);
    if(!rNewEntity) return;

    //--Order the combat party to remove the element.
    mrCombatParty->RemoveElementI(pCombatSlot);
    mrCombatParty->AddElementInSlot(pActiveMemberName, rNewEntity, pCombatSlot);
}
AdvCombatEntity *AdvCombat::PushAbilityOwnerWhenNull()
{
    ///--[Documentation]
    //--Abilities typically have an owner, the character who can use the ability. Some abilities do
    //  not have an owner, in which case, when they are asked to push their owner, this function is
    //  called and will tell them what to return.
    //--In the base class, push NULL. Adventure Combat does not support abilities without owners.
    return NULL;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *AdvCombat::GetActivePartyList()
{
    return mrActiveParty;
}
StarLinkedList *AdvCombat::GetCombatPartyList()
{
    return mrCombatParty;
}
StarLinkedList *AdvCombat::GetEnemyPartyList()
{
    return mrEnemyCombatParty;
}
CombatEventPack *AdvCombat::GetFiringEventPack()
{
    //--Is NULL unless ACA_SCRIPT_CODE_EXECUTE is running.
    return rFiringEventPackage;
}
CombatEventPack *AdvCombat::GetEventQueryPack()
{
    //--Is NULL unless ACA_SCRIPT_CODE_EVENT_QUEUED is running.
    return rEventZeroPackage;
}
AdvCombatEntity *AdvCombat::GetEntityByID(uint32_t pUniqueID)
{
    //--Scans entities for their unique ID. Can return NULL if the entity is not found. The entity
    //  can be KOd and will still return. This includes the party's full roster, including entities
    //  that aren't even in the active party.
    if(pUniqueID == 0) return NULL;

    //--Party list.
    AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pUniqueID)
        {
            mPartyRoster->PopIterator();
            return rCheckEntity;
        }
        rCheckEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }

    //--Enemy list.
    rCheckEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pUniqueID)
        {
            mEnemyRoster->PopIterator();
            return rCheckEntity;
        }
        rCheckEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--Couldn't find the entity.
    return NULL;
}
AdvCombatEntity *AdvCombat::GetRosterMemberI(int pIndex)
{
    return (AdvCombatEntity *)mPartyRoster->GetElementBySlot(pIndex);
}
AdvCombatEntity *AdvCombat::GetRosterMemberS(const char *pName)
{
    return (AdvCombatEntity *)mPartyRoster->GetElementByName(pName);
}
AdvCombatEntity *AdvCombat::GetActiveMemberI(int pIndex)
{
    return (AdvCombatEntity *)mrActiveParty->GetElementBySlot(pIndex);
}
AdvCombatEntity *AdvCombat::GetActiveMemberS(const char *pName)
{
    return (AdvCombatEntity *)mrActiveParty->GetElementByName(pName);
}
AdvCombatEntity *AdvCombat::GetCombatMemberI(int pIndex)
{
    return (AdvCombatEntity *)mrCombatParty->GetElementBySlot(pIndex);
}
AdvCombatEntity *AdvCombat::GetCombatMemberS(const char *pName)
{
    return (AdvCombatEntity *)mrCombatParty->GetElementByName(pName);
}
AdvCombatEntity *AdvCombat::GetKnockoutEntity()
{
    //--Populated only when ADVCOMBAT_RESPONSE_AIKNOCKOUT is active, otherwise NULL.
    return rKnockoutEntity;
}
StarLinkedList *AdvCombat::GetGraveyard()
{
    return mrEnemyGraveyard;
}
AliasStorage *AdvCombat::GetEnemyAliasStorage()
{
    return mEnemyAliases;
}
AdvCombatEntity *AdvCombat::GetActingEntity()
{
    //--Can return NULL if no entity is currently acting. Note that for ability execution, this may
    //  not be the originator of the event, in case a response is firing.
    return (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
}
AdvCombatEntity *AdvCombat::GetEventOriginator()
{
    //--Can return NULL if no event is firing.
    if(!rFiringEventPackage) return NULL;
    return rFiringEventPackage->rOriginator;
}
AdvCombatAbility *AdvCombat::GetSystemPassTurn()
{
    return mSysAbilityPassTurn;
}
AdvCombatAbility *AdvCombat::GetSystemRetreat()
{
    return mSysAbilityRetreat;
}
AdvCombatAbility *AdvCombat::GetSystemSurrender()
{
    return mSysAbilitySurrender;
}
AdvCombatAbility *AdvCombat::GetSystemVolunteer()
{
    return mSysAbilityVolunteer;
}
ApplicationPack *AdvCombat::GetApplicationPack()
{
    return rCurrentApplicationPack;
}
CombatStatistics *AdvCombat::GetJobLevelUpStorage()
{
    return &mJobTempStatistics;
}

///===================================== Static Functions =========================================
AdvCombat *AdvCombat::Fetch()
{
    return MapManager::Fetch()->GetAdventureCombat();
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
