//--Base
#include "CutsceneManager.h"

//--Classes
#include "RootEvent.h"
#include "ActorEvent.h"
#include "AudioEvent.h"
#include "CameraEvent.h"
#include "TilemapActor.h"
#include "TimerEvent.h"
#include "PairanormalDialogue.h"
#include "WorldDialogue.h"
#include "CarnationLevel.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

///--[Debug]
//#define CS_DEBUG
#ifdef CS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
CutsceneManager::CutsceneManager()
{
    ///--[Variable Initialization]
    //--[CutsceneManager]
    //--System

    //--Storage
    mEventList = new StarLinkedList(true);

    //--Bucketed Addition
    mIsBucketedAddition = false;
    mBypassBucket = false;
    mBucketedList = new StarLinkedList(true);

    //--Drop List.
    mDropList = NULL;

    //--Parallel Simultaneous Scripts
    mClearAllParallelLists = false;
    mSuspendActiveScript = false;
    mRemoveActiveScript = false;
    rActivePackage = NULL;
    mParallelScriptList = new StarLinkedList(true);
    mSuspendedParallelScripts = new StarLinkedList(true);
    mParallelClearList = new StarLinkedList(true);

    //--Parallel Bucketing
    mParallelAppendList = new StarLinkedList(true);
}
CutsceneManager::~CutsceneManager()
{
    delete mEventList;
    delete mBucketedList;
    delete mDropList;
    delete mParallelScriptList;
    delete mSuspendedParallelScripts;
    delete mParallelClearList;
    delete mParallelAppendList;
}

///--[Public Statics]
//--Used to turn on and off flags related to printing debug text.
bool CutsceneManager::xFlag = true;

//--Used to track how many times an entity can update per tick. Normally 1, but can be increased
//  deliberately to accelerate cutscenes.
//--Gets reset before each tick back to 1.
int CutsceneManager::xUpdatesPerEntityThisTick = CM_DEFAULT_CUTSCENE_CYCLES;
int CutsceneManager::xTimerUpdatesThisTick = CM_DEFAULT_CUTSCENE_CYCLES;

//--Scattering tracker. Is true when scattering parallel events, otherwies false.
bool CutsceneManager::xIsScatteringScripts = false;

//--Diagnostic level. Each flag prints different things to the console/screen.
int CutsceneManager::xDiagnosticLevel = 0;

///===================================== Property Queries =========================================
bool CutsceneManager::HasAnyEvents()
{
    //--Note: Ignores parallel script events. Only the lockout-list has this.
    return (mEventList->GetListSize() > 0);
}
const char *CutsceneManager::GetActiveParallelPackName()
{
    if(!rActivePackage) return "NULL";
    int tSlot = mParallelScriptList->GetSlotOfElementByPtr(rActivePackage);
    return mParallelScriptList->GetNameOfElementBySlot(tSlot);
}

///======================================== Manipulators ==========================================
void CutsceneManager::CreateParallelScript(const char *pName, const char *pPath, bool pReportTicksOnRefresh)
{
    //--Creates a new parallel script. Also immediately executes it to populate its event list.
    //  Note that a script which has been created cannot be accessed until after the tick ends.
    if(!pName || !pPath) return;
    SetMemoryData(__FILE__, __LINE__);
    ParallelScriptPack *nNewScript = (ParallelScriptPack *)starmemoryalloc(sizeof(ParallelScriptPack));
    nNewScript->Initialize();
    nNewScript->mPrintExecTimeOnReset = pReportTicksOnRefresh;

    //--Give it its name and register it.
    ResetString(nNewScript->mRefresherScript, pPath);
    mParallelAppendList->AddElement(pName, nNewScript, &ParallelScriptPack::DeleteThis);

    //--Run it for the first time. It will also re-run when its event list empties.
    ParallelScriptPack *rOldPackage = rActivePackage;
    rActivePackage = nNewScript;
    LuaManager::Fetch()->ExecuteLuaFile(pPath);
    rActivePackage = rOldPackage;
}
void CutsceneManager::SuspendParallelScript(const char *pName)
{
    //--Check the main parallel list.
    void *rCheckStruct = mParallelScriptList->GetElementByName(pName);
    if(rCheckStruct)
    {
        //--Liberate the entry.
        mParallelScriptList->SetRandomPointerToThis(rCheckStruct);
        mParallelScriptList->LiberateRandomPointerEntry();

        //--Re-register it to the suspended list.
        mSuspendedParallelScripts->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
        return;
    }

    //--If that failed, check the additions bucket.
    rCheckStruct = mParallelAppendList->GetElementByName(pName);
    if(rCheckStruct)
    {
        //--Liberate the entry.
        mParallelAppendList->SetRandomPointerToThis(rCheckStruct);
        mParallelAppendList->LiberateRandomPointerEntry();

        //--Re-register it to the suspended list.
        mSuspendedParallelScripts->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
        return;
    }
}
void CutsceneManager::SuspendActiveScript()
{
    mSuspendActiveScript = true;
}
void CutsceneManager::UnsuspendParallelScript(const char *pName)
{
    //--Find the named script and unsuspend it.
    void *rCheckStruct = mSuspendedParallelScripts->GetElementByName(pName);
    if(!rCheckStruct) return;

    //--Liberate the entry.
    mSuspendedParallelScripts->SetRandomPointerToThis(rCheckStruct);
    mSuspendedParallelScripts->LiberateRandomPointerEntry();

    //--Re-register it to the parallel list.
    mParallelScriptList->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
}
void CutsceneManager::RemoveParallelScript(const char *pName)
{
    //--Moves the named script onto the removal list, which gets dumped at the end of the tick.
    while(true)
    {
        //--Check the main list.
        void *rCheckStruct = mParallelScriptList->GetElementByName(pName);
        if(rCheckStruct)
        {
            mParallelScriptList->SetRandomPointerToThis(rCheckStruct);
            mParallelScriptList->LiberateRandomPointerEntry();
            mParallelClearList->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
            continue;
        }

        //--Check the suspend list.
        rCheckStruct = mSuspendedParallelScripts->GetElementByName(pName);
        if(rCheckStruct)
        {
            mSuspendedParallelScripts->SetRandomPointerToThis(rCheckStruct);
            mSuspendedParallelScripts->LiberateRandomPointerEntry();
            mParallelClearList->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
            continue;
        }

        //--No matches, exit.
        break;
    }
}
void CutsceneManager::RemoveActiveScript()
{
    mRemoveActiveScript = true;
}
void CutsceneManager::ClearParallelScripts()
{
    mClearAllParallelLists = true;
}
void CutsceneManager::ApplyScatterToAppendingPackage(const char *pName, int pMinTicks, int pMaxTicks)
{
    ///--[Note]
    //--The package in question MUST be on the append bucket. This function will then mark the package
    //  to run its scattering code when it is placed on the main list.
    ParallelScriptPack *rPackage = (ParallelScriptPack *)mParallelAppendList->GetElementByName(pName);
    if(!rPackage) return;

    //--Set.
    rPackage->mHasPendingScatter = true;
    rPackage->mScatterLo = pMinTicks;
    rPackage->mScatterHi = pMaxTicks;
}

///======================================== Core Methods ==========================================
void CutsceneManager::RegisterEvent(RootEvent *pEvent)
{
    //--Registers the event to the end of the event list. Names can be duplicated on the list.
    if(!pEvent) return;

    //--Parallel List: Add to that event list, if it exists.
    if(rActivePackage)
    {
        //fprintf(stderr, " Intercepted, event registered to parallel package %p\n", rActivePackage);
        rActivePackage->mEventList->AddElementAsTail(pEvent->GetName(), pEvent, &RootObject::DeleteThis);
        return;
    }

    //--Normal case: Add this to the list.
    if(!mIsBucketedAddition || (mIsBucketedAddition && mBypassBucket))
    {
        mEventList->AddElementAsTail(pEvent->GetName(), pEvent, &RootObject::DeleteThis);
    }
    //--Otherwise, add this to the bucket.
    else
    {
        mBucketedList->AddElementAsTail(pEvent->GetName(), pEvent, &RootObject::DeleteThis);
    }
}
RootEvent *CutsceneManager::CreateBlocker()
{
    //--Quick-access subroutine: Creates a basic blocker with no other properties.
    RootEvent *nEvent = new RootEvent();
    nEvent->SetName("Blocker");
    nEvent->SetBlockingFlag(true);
    RegisterEvent(nEvent);
    return nEvent;
}
void CutsceneManager::DropAllEvents()
{
    //--Drops all events. Events get moved to a non-executing list and will be purged at the start
    //  of the next tick.
    if(mDropList)
    {
        //--Liberate the events and move them to the drop list.
        while(mEventList->GetListSize() > 0)
        {
            mEventList->SetRandomPointerToHead();
            void *rPtr = mEventList->LiberateRandomPointerEntry();
            mDropList->AddElementAsTail("X", rPtr, &RootObject::DeleteThis);
        }
    }
    //--Just reposition the event list.
    else
    {
        mDropList = mEventList;
        mEventList = new StarLinkedList(true);
    }
}
void CutsceneManager::Reset(const char *pResetType)
{
    ///--[Documentation]
    //--Resets the program using the provided code. Only called at the end of a tick.
    if(!pResetType) return;

    ///--[Full Reset]
    //--The Full Reset is only called at the end of a tick.
    if(!strcasecmp(pResetType, "Full Game Reset"))
    {
        DropAllEvents();
    }
    ///--[Combat]
    //--Combat Reset.
    else if(!strcasecmp(pResetType, "Combat"))
    {
    }
    //--Unknown reset type.
    else
    {
    }
}
bool CutsceneManager::IsAnythingStoppingEvents()
{
    ///--[Documentation]
    //--Used when the CutsceneManager is checking if anything is stopping events from updating. This includes
    //  things like dialogue, animations, and whatever other things this local project has. If anything
    //  is stopping events, returns true. Otherwise, false.

    ///--[Dialogue Check]
    //--Dialogue handler, often used by the AdventureLevel during cutscenes.
    WorldDialogue *rDialogue = WorldDialogue::Fetch();
    if(rDialogue->IsStoppingEvents()) return true;

    ///--[Carnation Dialogue]
    //--If Carnation is active, check if its internal level dialogue is blocking anything.
    CarnationLevel *rActiveLevel = CarnationLevel::Fetch();
    if(rActiveLevel)
    {
        WorldDialogue *rLevelDialogue = rActiveLevel->GetInternalDialogue();
        if(rLevelDialogue && rLevelDialogue->IsStoppingEvents()) return true;
    }

    ///--[All Checks Passed]
    return false;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void CutsceneManager::Update()
{
    ///--[Documentation and Setup]
    //--Updates the event cycle currently in the manager. Does nothing if there are no events.
    bool tHasIncompleteEvent = false;
    mIsBucketedAddition = true;
    mBypassBucket = false;

    //--Debug.
    DebugPush(true, "CutsceneManager:Update - Begin.\n");
    DebugPrint("Event count: %i\n", mEventList->GetListSize());

    ///--[Derived Check]
    //--This function is implemented in the "local" project. It should return true if anything local
    //  to a game that doesn't have a class in the Core/Engine is interrupting the cutscene manager's update.
    //--If something is blocking, then events start off as having an incomplete event.
    if(IsAnythingStoppingEvents()) tHasIncompleteEvent = true;

    ///--[Timer Acceleration Scan]
    //--Check if any events are able to update before a blocker that are not timer events. If so, all timer
    //  events tick once. If acceleration is taking place, and only timer events are running, timer events can
    //  tick multiple times.
    xUpdatesPerEntityThisTick = 1;
    if(xUpdatesPerEntityThisTick > 1)
    {
        //--Storage.
        bool tStartingIncomplete = tHasIncompleteEvent;

        //--Iterate.
        RootEvent *rEvent = (RootEvent *)mEventList->SetToHeadAndReturn();
        while(rEvent)
        {
            //--Ignore complete events.
            if(rEvent->IsComplete())
            {
            }
            //--Ignore blockers that will get removed later.
            else if(rEvent->IsBlocker() && !tHasIncompleteEvent)
            {
            }
            //--If this event is a blocker and the incomplete flag is set, stop here.
            else if(rEvent->IsBlocker() && tHasIncompleteEvent)
            {
                break;
            }
            //--If this event was not complete yet, set this flag.
            else
            {
                tHasIncompleteEvent = true;
            }

            //--After each event, the WorldDialogue can stop proceedings if it hit a blocker.
            if(IsAnythingStoppingEvents())
            {
                fprintf(stderr, "Dialogue is stopping events boosted.\n");
                tHasIncompleteEvent = true;
            }

            //--If this event is a type that stops the timer events, set that here.
            if(rEvent->DisablesTimerAcceleration())
            {
                CutsceneManager::xTimerUpdatesThisTick = CM_DEFAULT_CUTSCENE_CYCLES;
                break;
            }

            //--Next event.
            rEvent = (RootEvent *)mEventList->IncrementAndGetRandomPointerEntry();
        }

        //--Reset.
        tHasIncompleteEvent = tStartingIncomplete;
    }

    ///--[Main Iteration]
    //--Iterate.
    RootEvent *rEvent = (RootEvent *)mEventList->SetToHeadAndReturn();
    while(rEvent)
    {
        //--Debug.
        DebugPrint(" Update event: %s %p\n", rEvent->GetName(), rEvent);

        //--Visual diagnostics.
        if(xDiagnosticLevel & CM_DIAGNOSTIC_VISUAL)
        {
            rEvent->HandleDiagnostics();
        }

        //--Run the update.
        rEvent->Update();

        //--Ask this event if it is complete.
        if(rEvent->IsComplete())
        {
            mEventList->RemoveRandomPointerEntry();
            DebugPrint("  Removed from completion.\n");
        }
        //--If this event is a blocker and the incomplete flag is not set, remove it and keep going.
        else if(rEvent->IsBlocker() && !tHasIncompleteEvent)
        {
            mEventList->RemoveRandomPointerEntry();
            DebugPrint("  Removed from cleared blocker.\n");
        }
        //--If this event is a blocker and the incomplete flag is set, stop here.
        else if(rEvent->IsBlocker() && tHasIncompleteEvent)
        {
            DebugPrint("  Stopping due to blocker.\n");
            break;
        }
        //--If this event was not complete yet, set this flag.
        else
        {
            tHasIncompleteEvent = true;
            DebugPrint("  Event is incomplete.\n");
        }

        //--After each event, the WorldDialogue can stop proceedings if it hit a blocker.
        if(IsAnythingStoppingEvents())
        {
            tHasIncompleteEvent = true;
            DebugPrint("  Dialogue is stopping event iteration.\n");
        }

        //--Next event.
        DebugPrint("  Getting next event.\n");
        rEvent = (RootEvent *)mEventList->IncrementAndGetRandomPointerEntry();
    }

    //--Flags.
    mIsBucketedAddition = false;
    DebugPrint("Completed iterations.\n");

    //--After iteration, if any events were put in the bucket, add them to the end of the list. This allows events to create the next
    //  set of events ala enclosures or repeated usages of the same script.
    RootEvent *rAppendEvent = (RootEvent *)mBucketedList->SetToHeadAndReturn();
    while(rAppendEvent)
    {
        mBucketedList->LiberateRandomPointerEntry();
        RegisterEvent(rAppendEvent);
        rAppendEvent = (RootEvent *)mBucketedList->SetToHeadAndReturn();
    }

    //--Debug.
    DebugPop("CutsceneManager: Update - Complete.\n");
}
void CutsceneManager::ScatterParallelPackage(const char *pName, int pMinTicks, int pMaxTicks)
{
    ///--[Documentation]
    //--Given a parallel event package name, finds the package and then runs its update between the given number of ticks.
    //  This creates randomization to the otherwise clockwork speeds of the scripts.
    ParallelScriptPack *rPackage = (ParallelScriptPack *)mParallelScriptList->GetElementByName(pName);
    if(!rPackage) return;

    ///--[Tick Resolving]
    //--If the min and max ticks are invalid, fail here.
    if(pMinTicks < 0) pMinTicks = 0;
    if(pMaxTicks < pMinTicks) pMaxTicks = pMinTicks;

    //--If the min and max ticks are the same, no scattering.
    int tRuns = 0;
    if(pMinTicks == pMaxTicks)
    {
        tRuns = pMinTicks;
    }
    //--Otherwise, roll a value and run that.
    else
    {
        tRuns = (rand() % (pMaxTicks - pMinTicks)) + pMinTicks;
    }

    ///--[Entity Setup]
    StarLinkedList *tEntityList = EntityManager::Fetch()->GetListContaining(POINTER_TYPE_TILEMAPACTOR);

    ///--[Run Scattering]
    //--Set these static flag to true, allowing events to run multiple times per tick.
    TimerEvent::xAlwaysRunEvent = true;
    TilemapActor::xAlwaysRunEvents = true;
    CutsceneManager::xIsScatteringScripts = true;

    //--Debug.
    //fprintf(stderr, "Scattering %s for %i ticks. %i.\n", pName, tRuns, xIsScatteringScripts);

    //--Execute.
    ParallelScriptPack *rOldPackage = rActivePackage;
    rActivePackage = rPackage;
    for(int i = 0; i < tRuns; i ++)
    {
        //--Run the update.
        UpdateParallelPackage(pName, rPackage);

        //--Order all actors to reset their last instruction handle timer, allowing them to run another instruction.
        TilemapActor *rActor = (TilemapActor *)tEntityList->PushIterator();
        while(rActor)
        {
            rActor->OverrideInstructionHandle();
            rActor = (TilemapActor *)tEntityList->AutoIterate();
        }
    }
    rActivePackage = rOldPackage;

    ///--[Clean]
    TimerEvent::xAlwaysRunEvent = false;
    TilemapActor::xAlwaysRunEvents = false;
    CutsceneManager::xIsScatteringScripts = false;
    delete tEntityList;
}
void CutsceneManager::UpdateParallelEvents()
{
    ///--[Documentation]
    //--Updates parallel event lists. Unlike the main lists, these can run even if there are no events on the main list.
    //  This inherently means the player still has control over the game, and the event still runs!
    //--Pausing the game, however, still stops this update. Obviously.
    ParallelScriptPack *rParallelPack = (ParallelScriptPack *)mParallelScriptList->SetToHeadAndReturn();
    while(rParallelPack)
    {
        //--Clear these flags.
        mRemoveActiveScript = false;
        mSuspendActiveScript = false;

        //--Run the update.
        rActivePackage = rParallelPack;
        UpdateParallelPackage(mParallelScriptList->GetRandomPointerName(), rParallelPack);
        rActivePackage = NULL;

        //--If the order was received to remove this script, do it now.
        if(mRemoveActiveScript)
        {
            mParallelScriptList->RemoveRandomPointerEntry();
        }
        //--If the order was received to suspend the script, do it now.
        else if(mSuspendActiveScript)
        {
            mSuspendedParallelScripts->AddElement(mParallelScriptList->GetRandomPointerName(), rParallelPack, &ParallelScriptPack::DeleteThis);
            mParallelScriptList->LiberateRandomPointerEntry();
        }

        //--Next.
        rParallelPack = (ParallelScriptPack *)mParallelScriptList->IncrementAndGetRandomPointerEntry();
    }
}
void CutsceneManager::UpdateParallelPackage(const char *pName, ParallelScriptPack *pPack)
{
    ///--[Documentation]
    //--Updates a parallel event list package. This always occurs right after normal event updates occur, so
    //  a parallel package created by an event will execute for the first time on the same tick.
    //  Note that parallel packages have full permission sets, including the ability to spawn their own
    //  parallel scripts. Try not to break things, please.
    //--When a parallel package runs out of events, it re-runs its refresher script automatically to
    //  rebuild the event list.
    //--The special event "SUSPENDTHIS" will suspend the calling package and only the calling package.
    //--The name is only used for diagnostics.
    //fprintf(stderr, "Updating %i\n", Global::Shared()->gTicksElapsed);
    if(!pPack) return;

    ///--[Tick]
    //--Increment this counter, which tracks how many ticks the package executed for. This is useful for diagnostics
    //  and for randomizing execution time for fixed-length scripts.
    pPack->mExecTime ++;

    //--Setup.
    bool tExecutedAnyEvents = false;
    bool tHasIncompleteEvent = false;

    //--Iterate.
    RootEvent *rEvent = (RootEvent *)pPack->mEventList->SetToHeadAndReturn();
    while(rEvent)
    {
        //--Debug.
        //DebugPrint(" Update event: %s %p\n", rEvent->GetName(), rEvent);

        //--Run the update.
        tExecutedAnyEvents = true;
        rEvent->Update();

        //--Ask this event if it is complete.
        if(rEvent->IsComplete())
        {
            pPack->mEventList->RemoveRandomPointerEntry();
            //DebugPrint("  Removed from completion.\n");
        }
        //--If this event is a blocker and the incomplete flag is not set, remove it and keep going.
        else if(rEvent->IsBlocker() && !tHasIncompleteEvent)
        {
            pPack->mEventList->RemoveRandomPointerEntry();
            //DebugPrint("  Removed from cleared blocker.\n");
        }
        //--If this event is a blocker and the incomplete flag is set, stop here.
        else if(rEvent->IsBlocker() && tHasIncompleteEvent)
        {
            //DebugPrint("  Stopping due to blocker.\n");
            break;
        }
        //--If this event was not complete yet, set this flag.
        else
        {
            tHasIncompleteEvent = true;
            //DebugPrint("  Event is incomplete.\n");
        }

        //--Next event.
        //DebugPrint("  Getting next event.\n");
        rEvent = (RootEvent *)pPack->mEventList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Re-populate]
    //--If the event list was completely empty, re-execute the populating script. Don't do this if
    //  the order to suspend or delete this script was received.
    if(!tExecutedAnyEvents && !mSuspendActiveScript && !mRemoveActiveScript)
    {
        //--Debug.
        //fprintf(stderr, " Out of events: Refreshing.\n");
        if(pPack->mPrintExecTimeOnReset && pName)
        {
            fprintf(stderr, "Script %s refreshes on tick %i\n", pName, pPack->mExecTime);
        }

        //--Reset, run the refresher.
        pPack->mExecTime = 0;
        LuaManager::Fetch()->ExecuteLuaFile(pPack->mRefresherScript);
    }
}
void CutsceneManager::DropListAtEndOfTick()
{
    ///--[Documentation]
    //--At the end of an update tick, this routine gets called. It is meant to make sure that parallel
    //  scripts don't delete themselves while still running, which would cause crashes.
    //--In addition, if anything is in the appendation bucket, it gets moved to the main update list.
    //  This prevents scripts from being created in a constructor and cleared on the same tick.

    ///--[Primary List Clear]
    //--This list always drops. Scripts which have finished execution are moved here.
    mParallelClearList->ClearList();

    ///--[Clearing Flag]
    //--If this flag is set, clear all parallel lists. This usually happens when the room changes and
    //  all executing scripts are considered deprecated.
    if(mClearAllParallelLists)
    {
        mClearAllParallelLists = false;
        mParallelScriptList->ClearList();
        mSuspendedParallelScripts->ClearList();
    }

    ///--[Drop List]
    //--This list only contains mainline events. It drops if it exists.
    if(mDropList)
    {
        delete mDropList;
        mDropList = NULL;
    }

    ///--[Bucket Append]
    //--Anything created during this tick gets put into a bucket of scripts. Those scripts move to the
    //  main list after the list has been cleared, if it was going to be cleared.
    ParallelScriptPack *rParallelPack = (ParallelScriptPack *)mParallelAppendList->SetToHeadAndReturn();
    while(rParallelPack)
    {
        //--Register the element using its existing name.
        mParallelScriptList->AddElement(mParallelAppendList->GetRandomPointerName(), rParallelPack, &ParallelScriptPack::DeleteThis);

        //--Liberate the element from the bucket list.
        mParallelAppendList->LiberateRandomPointerEntry();

        //--Next.
        rParallelPack = (ParallelScriptPack *)mParallelAppendList->SetToHeadAndReturn();
    }

    ///--[Scattering]
    //--Scan across the packages that just got appended to the list. If any are marked for scattering, do that now.
    ParallelScriptPack *rScatterPack = (ParallelScriptPack *)mParallelScriptList->SetToHeadAndReturn();
    while(rScatterPack)
    {
        //--If set, run.
        if(rScatterPack->mHasPendingScatter)
        {
            rScatterPack->mHasPendingScatter = false;
            ScatterParallelPackage(mParallelScriptList->GetRandomPointerName(), rScatterPack->mScatterLo, rScatterPack->mScatterHi);
        }

        //--Next.
        rScatterPack = (ParallelScriptPack *)mParallelScriptList->IncrementAndGetRandomPointerEntry();
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void CutsceneManager::RenderDiagnostics()
{
    ///--[Documentation]
    //--Renders cutscene diagnostics to the screen.
    if(!(xDiagnosticLevel & CM_DIAGNOSTIC_VISUAL)) return;
    DebugManager::RenderVisualTrace("CTN");

    ///--[Dialogue]
    if(IsAnythingStoppingEvents())
    {
        WorldDialogue *rDialogue = WorldDialogue::Fetch();
        if(rDialogue) rDialogue->RenderDiagnostics();
    }

    ///--[Main Iteration]
    //--Iterate.
    bool tHasIncompleteEvent = false;
    RootEvent *rEvent = (RootEvent *)mEventList->PushIterator();
    while(rEvent)
    {
        //--Event handles diagnostics.
        rEvent->HandleDiagnostics();

        //--If this event is a blocker and the incomplete flag is set, stop here.
        if(rEvent->IsBlocker() && tHasIncompleteEvent)
        {
            mEventList->PopIterator();
            break;
        }
        //--If this event was not complete yet, set this flag.
        else
        {
            tHasIncompleteEvent = true;
        }

        //--Next event.
        rEvent = (RootEvent *)mEventList->AutoIterate();
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
CutsceneManager *CutsceneManager::Fetch()
{
    return Global::Shared()->gCutsceneManager;
}

///======================================== Lua Hooking ===========================================
void CutsceneManager::HookToLuaState(lua_State *pLuaState)
{
    /* Cutscene_CreateEvent("DROPALLEVENTS") (Does not push)
       Cutscene_CreateEvent(sEventName)
       Cutscene_CreateEvent("Blocker")
       Cutscene_CreateEvent(sEventName, "Timer")
       Cutscene_CreateEvent(sEventName, "Actor")
       Cutscene_CreateEvent(sEventName, "Camera")
       Cutscene_CreateEvent(sEventName, "Audio")
       Creates, registers, and pushes a new event to the default CutsceneManager. */
    lua_register(pLuaState, "Cutscene_CreateEvent", &Hook_Cutscene_CreateEvent);

    /* Cutscene_HandleParallel("Create", sName, sPath)
       Cutscene_HandleParallel("Suspend", sName)
       Cutscene_HandleParallel("Unsuspend", sName)
       Cutscene_HandleParallel("Remove", sName)
       Cutscene_HandleParallel("Scatter", sName, iMinTicks, iMaxTicks)
       Cutscene_HandleParallel("ClearAll")
       Cutscene_HandleParallel("SUSPENDTHIS") --Special, suspends the executing parallel list only.
       Cutscene_HandleParallel("DELETETHIS") --Special, deletes the executing parallel list only.
       Master function to handle parallel execution lists. */
    lua_register(pLuaState, "Cutscene_HandleParallel", &Hook_Cutscene_HandleParallel);

    /* Cutscene_GetProperty("Is Scattering") (1 Boolean)
       Returns the given property from the CutsceneManager. */
    lua_register(pLuaState, "Cutscene_GetProperty", &Hook_Cutscene_GetProperty);

    /* Cutscene_SetProperty("Diagnostic Level", iFlag) (Static)
       Sets the given property in the CutsceneManager. */
    lua_register(pLuaState, "Cutscene_SetProperty", &Hook_Cutscene_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Cutscene_CreateEvent(lua_State *L)
{
    //Cutscene_CreateEvent("DROPALLEVENTS") (Does not push)
    //Cutscene_CreateEvent(sEventName)
    //Cutscene_CreateEvent("Blocker")
    //Cutscene_CreateEvent(sEventName, "Timer")
    //Cutscene_CreateEvent(sEventName, "Actor")
    //Cutscene_CreateEvent(sEventName, "Camera")
    //Cutscene_CreateEvent(sEventName, "Audio")
    DataLibrary::Fetch()->PushActiveEntity();

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Cutscene_CreateEvent");

    //--Switching types.
    const char *rName = lua_tostring(L, 1);
    const char *rType = NULL;
    if(tArgs == 2) rType = lua_tostring(L, 2);

    //--Special case: Drops all events. Doesn't push an event.
    if(!strcasecmp(rName, "DROPALLEVENTS"))
    {
        CutsceneManager::Fetch()->DropAllEvents();
    }
    //--Special case: Create a blocker immediately.
    else if(!strcasecmp(rName, "Blocker"))
    {
        DataLibrary::Fetch()->rActiveObject = CutsceneManager::Fetch()->CreateBlocker();
    }
    //--Special case: Timer type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Timer"))
    {
        TimerEvent *nEvent = new TimerEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Special case: Actor type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Actor"))
    {
        ActorEvent *nEvent = new ActorEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Special case: Camera type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Camera"))
    {
        CameraEvent *nEvent = new CameraEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Special case: Audio type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Audio"))
    {
        AudioEvent *nEvent = new AudioEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Normal case.
    else
    {
        RootEvent *nEvent = new RootEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    return 0;
}
int Hook_Cutscene_HandleParallel(lua_State *L)
{
    //Cutscene_HandleParallel("Create", sName, sPath)
    //Cutscene_HandleParallel("Create", sName, sPath, bPrintExecTime)
    //Cutscene_HandleParallel("Suspend", sName)
    //Cutscene_HandleParallel("Unsuspend", sName)
    //Cutscene_HandleParallel("Remove", sName)
    //Cutscene_HandleParallel("ClearAll")
    //Cutscene_HandleParallel("Scatter", sName, iMinTicks, iMaxTicks)
    //Cutscene_HandleParallel("SUSPENDTHIS") --Special, suspends the executing parallel list only.
    //Cutscene_HandleParallel("DELETETHIS") --Special, deletes the executing parallel list only.

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Cutscene_HandleParallel");

    //--Switching types.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Creates a new parallel execution list.
    if(!strcasecmp(rSwitchType, "Create") && tArgs == 3)
    {
        CutsceneManager::Fetch()->CreateParallelScript(lua_tostring(L, 2), lua_tostring(L, 3), false);
    }
    //--Creates a new parallel execution list, optionally specifies if we should print the exec time on reset.
    else if(!strcasecmp(rSwitchType, "Create") && tArgs == 4)
    {
        CutsceneManager::Fetch()->CreateParallelScript(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4));
    }
    //--Suspends the named script.
    else if(!strcasecmp(rSwitchType, "Suspend") && tArgs == 2)
    {
        CutsceneManager::Fetch()->SuspendParallelScript(lua_tostring(L, 2));
    }
    //--Unsuspends the named script.
    else if(!strcasecmp(rSwitchType, "Unsuspend") && tArgs == 2)
    {
        CutsceneManager::Fetch()->UnsuspendParallelScript(lua_tostring(L, 2));
    }
    //--Removes the named script.
    else if(!strcasecmp(rSwitchType, "Remove") && tArgs == 2)
    {
        CutsceneManager::Fetch()->RemoveParallelScript(lua_tostring(L, 2));
    }
    //--Clears all parallel scripts.
    else if(!strcasecmp(rSwitchType, "ClearAll") && tArgs == 1)
    {
        CutsceneManager::Fetch()->ClearParallelScripts();
    }
    //--Runs the script between the given two values to "Scatter" it, randomizing the locations of the NPCs in question.
    else if(!strcasecmp(rSwitchType, "Scatter") && tArgs == 4)
    {
        CutsceneManager::Fetch()->ApplyScatterToAppendingPackage(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Suspends the currently active parallel script. Does nothing if none is active.
    else if(!strcasecmp(rSwitchType, "SUSPENDTHIS") && tArgs == 1)
    {
        CutsceneManager::Fetch()->SuspendActiveScript();
    }
    //--Deletes the currently active parallel script. Does nothing if none is active.
    else if(!strcasecmp(rSwitchType, "DELETETHIS") && tArgs == 1)
    {
        CutsceneManager::Fetch()->RemoveActiveScript();
    }
    return 0;
}
int Hook_Cutscene_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //Cutscene_GetProperty("Is Scattering") (1 Boolean) (Static)
    //Cutscene_GetProperty("Parallel Script Name") (1 String)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Cutscene_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--True if scattering a parallel event, false otherwise.
    if(!strcasecmp(rSwitchType, "Is Scattering"))
    {
        lua_pushboolean(L, CutsceneManager::xIsScatteringScripts);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Parallel Script Name") && tArgs == 1)
    {
        lua_pushstring(L, rCutsceneManager->GetActiveParallelPackName());
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("Cutscene_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_Cutscene_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //Cutscene_SetProperty("Diagnostic Level", iFlag) (Static)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Cutscene_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--True if scattering a parallel event, false otherwise.
    if(!strcasecmp(rSwitchType, "Diagnostic Level") && tArgs == 2)
    {
        CutsceneManager::xDiagnosticLevel = lua_tointeger(L, 2);
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    //CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
    }
    //--Error case.
    else
    {
        LuaPropertyError("Cutscene_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
