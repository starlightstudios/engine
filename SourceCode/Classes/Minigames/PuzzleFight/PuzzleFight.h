///======================================== PuzzleFight ===========================================
//--A puzzle-fight minigame that takes place on an AdventureLevel. This object intercepts player inputs
//  and issues instructions to move tiles each tick.
//--Actors moving during the fight are commanded by lua scripts and cutscenes as usual.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[DislocationPack]
//--Stores a "dislocation", which is a part of the map that is not rendering where it was placed in the
//  original tile. Each dislocation is meant to represent a tile on the board, with a couple extras for
//  spots where the tile scrolls over the edge.
typedef struct DislocationPack
{
    //--Members
    int mDebugNum;
    char *mDislocationName;
    char *mAltDislocationName;
    float mCurrentX;
    float mCurrentY;
    float mDestinationX;
    float mDestinationY;

    //--Functions
    void Initialize();
    void Deallocate();
}DislocationPack;

///--[BoardObjectPack]
//--Stores a tile object, which is basically anything the player character will interact with as they
//  move across the board. These are arrows, power ups, or actions.
typedef struct BoardObjectPack
{
    //--Members
    bool mIsDisabled;
    int mObjectType;
    int mEasingType;
    EasingPack2D mPosition;
    EasingPack2D mDupePosition;

    //--Functions
    void Initialize();
}BoardObjectPack;

///--[BoardActionPack]
//--Stores a board action. Used to handle the undo stack by playing back the actions in opposite order.
typedef struct BoardActionPack
{
    //--Members
    int mCode;
    int mCursor;

    //--Functions
    void Initialize();
}BoardActionPack;

///--[BoardNotifierPack]
//--Stores a notifier. This is a world object that appears briefly.
typedef struct BoardNotifierPack
{
    //--Members
    int mType;
    int mTimer;
    int mTimerMax;
    float mWorldX;
    float mWorldY;
    StarBitmap *rImage;

    //--Functions
    void Initialize();
    bool IsComplete();
    void SetToType(int pType);
    void Update();
    void Render();
}BoardNotifierPack;

///===================================== Local Definitions ========================================
//--Board Object Codes
#define BOARD_OBJECT_EMPTY -1
#define BOARD_OBJECT_ARROW_UP 0
#define BOARD_OBJECT_ARROW_RT 1
#define BOARD_OBJECT_ARROW_DN 2
#define BOARD_OBJECT_ARROW_LF 3
#define BOARD_OBJECT_BOOST 4
#define BOARD_OBJECT_GUARD 5
#define BOARD_OBJECT_MELEE 6
#define BOARD_OBJECT_RANGED 7
#define BOARD_OBJECT_TOTAL 8

//--Board Actions
#define BOARD_MOVE_ERROR 0
#define BOARD_MOVE_UP 1
#define BOARD_MOVE_RT 2
#define BOARD_MOVE_DN 3
#define BOARD_MOVE_LF 4

//--Timings
#define UI_SLIDE_TICKS 45
#define CURSOR_MOVE_TICKS 5
#define BOARD_OBJECT_INITIAL_MOVE_TICKS 30
#define UI_CONFIRM_TICKS 15
#define TITLE_PRINT_TICKS_PER_LETTER 3
#define TITLE_PRINT_TICKS_TO_HOLD 45
#define TITLE_PRINT_TICKS_TO_FADE 15
#define HEALTH_SLIDE_TICKS 30

//--Title Print
#define PF_TITLE_PRINT_NONE 0
#define PF_TITLE_PRINT_PRINT 1
#define PF_TITLE_PRINT_HOLD 2
#define PF_TITLE_PRINT_FADE 3

//--Health
#define MAX_HEALTH 100

//--Game States
#define MINIGAME_STATE_PLAYERSTURNINIT 0
#define MINIGAME_STATE_PLAYERSTURNRUN 1
#define MINIGAME_STATE_PLAYERSTURNACT 2
#define MINIGAME_STATE_ENEMYTURN 3

//--Firing Codes
#define HANDLER_CODE_NONE -1
#define HANDLER_CODE_BEGIN_TURN 0
#define HANDLER_CODE_SPAWNED_OBJECTS_MOVED 1
#define HANDLER_CODE_MOVED 2
#define HANDLER_CODE_CONFIRMACTIONDONE 3

//--Notifier Types
#define PF_NOTIFIER_TYPE_NONE -1
#define PF_NOTIFIER_TYPE_FLOATFADE 0

//--Notifier Timings
#define PF_NOTIFIER_FLOATFADE_TIME_FLOAT 45
#define PF_NOTIFIER_FLOATFADE_DISTANCE -10.0f
#define PF_NOTIFIER_FLOATFADE_TIME_FADE 20
#define PF_NOTIFIER_FLOATFADE_TIME_TOTAL (PF_NOTIFIER_FLOATFADE_TIME_FLOAT + PF_NOTIFIER_FLOATFADE_TIME_FADE)

//--Debug Quit Sequence
#define PF_DEBUGQUIT_BACKSPACE 0
#define PF_DEBUGQUIT_F5 1

///========================================== Classes =============================================
class PuzzleFight : public RootObject
{
    private:
    //--System
    bool mIsTakingDown;
    bool mIsHandlingUpdates;
    bool mIsComplete;
    char *mUpdateScript;
    int mGameState;
    int mQuitSequence;

    //--UI
    int mUISlideTimer;

    //--Title Printing
    int mTitlePrintPhase;
    int mTitlePrintTimer;
    int mTitlePrintTimerMax;
    char *mTitlePrintString;

    //--Dislocation Storage
    int mDislocationsTotal;
    DislocationPack *mDislocations;

    //--Board
    float mBoardScrollSpeed;
    int mBoardStartX;
    int mBoardStartY;
    int mBoardW;
    int mBoardH;
    int mBoardTileSizeX;
    int mBoardTileSizeY;
    BoardObjectPack **mBoardObjects;
    DislocationPack ***rDislocationBoard;//Master list is mDislocations.
    StarLinkedList *mNotifierList; //BoardNotifierPack *, master

    //--Player
    int mPlayerTicksLeft;
    int mPlayerMovesLeft;
    char *mPlayerEntityName;
    int mPlayerHealth;
    int mBoardSlideTimer;
    StarLinkedList *mBoardActionList; //BoardActionPack *, master
    EasingPack1D mPlayerHealthEasing;
    bool mShowConfirmDialogue;
    int mConfirmTimer;
    StarlightString *mConfirmAccept;
    StarlightString *mConfirmCancel;

    //--Challenger
    int mChallengerHealth;
    EasingPack1D mChallengerHealthEasing;

    //--Cursor
    bool mUpdateCursorImmediately;
    bool mIsCursorHorizontal;
    int mCursorSlot;
    int mCursorMoveTimer;
    TwoDimensionReal mCursorStart;
    TwoDimensionReal mCursorCurrent;
    TwoDimensionReal mCursorFinish;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            ///--Fonts
            StarFont *rMovesFont;
            StarFont *rTimeFont;
            StarFont *rConfirmFont;
            StarFont *rConfirmSmallFont;
            StarFont *rCenterTitleText;

            ///--World Pieces
            StarBitmap *rHighlight;
            StarBitmap *rArrowDn;
            StarBitmap *rArrowRt;
            StarBitmap *rArrowUp;
            StarBitmap *rArrowLf;
            StarBitmap *rBonusBoost;
            StarBitmap *rBonusGuard;
            StarBitmap *rActionMelee;
            StarBitmap *rActionRanged;
            StarBitmap *rBoardObjects[BOARD_OBJECT_TOTAL];
            StarBitmap *rPowerUpNotifier;
            StarBitmap *rPerfectGuardNotifier;

            ///--UI Pieces
            StarBitmap *rHealthFillLft;
            StarBitmap *rHealthFillRgt;
            StarBitmap *rHealthFrameLft;
            StarBitmap *rHealthFrameRgt;
            StarBitmap *rMovesBox;
            StarBitmap *rTimeHeader;
            StarBitmap *rConfirmBox;
            StarBitmap *rTextTop;
            StarBitmap *rTextBot;
        }Data;
    }Images;

    protected:

    public:
    //--System
    PuzzleFight();
    virtual ~PuzzleFight();
    void Construct();

    //--Public Variables
    //--Property Queries
    bool IsHandlingUpdate();
    bool IsComplete();
    int GetObjectAtBoardPosition(int pX, int pY);
    int GetPlayerHealth();
    int GetEnemyHealth();
    int GetMovesLeft();
    int GetTicksLeft();

    //--Manipulators
    void BeginTakedown();
    void SetHandlingUpdates(bool pFlag);
    void SetComplete(bool pFlag);
    void SetUpdateHandlerScript(const char *pPath);
    void SetGameStateFlag(int pFlag);
    void AllocateDislocations(int pTotal);
    void SetDislocationName(int pSlot, const char *pName);
    void SetDislocationAltName(int pSlot, const char *pName);
    void SetDislocationPos(int pSlot, float pX, float pY);
    void SetDislocationDest(int pSlot, float pX, float pY);
    void SetBoardTilePosition(int pX, int pY);
    void SetBoardSize(int pW, int pH);
    void RegisterDislocationPackToBoardSlot(int pPackSlot, int pBoardX, int pBoardY);
    void SetPlayerEntityName(const char *pName);
    void SetPlayerTicks(int pTicks);
    void SetPlayerMoves(int pMoves);
    void SetPlayerHealth(int pHealth);
    void SetEnemyHealth(int pHealth);
    void SetTitlePrint(const char *pString);
    void RegisterNotifier(int pType, float pWorldX, float pWorldY, const char *pImagePath);

    //--Core Methods
    void RunLoaderScript();
    void RunInitializerScript();
    void SpawnBoardObject(int pType, int pBoardX, int pBoardY, float pSpawnX, float pSpawnY);
    void DisableBoardObjectAt(int pBoardX, int pBoardY);
    void ClearBoardObjectAt(int pBoardX, int pBoardY);
    void ClearAllBoardObjects();

    //--Puzzle Fight Movers
    bool MoveRowLeft(int pRow);
    bool MoveRowRight(int pRow);
    bool MoveColumnUp(int pCol);
    bool MoveColumnDown(int pCol);

    private:
    //--Private Core Methods
    void CreateActionPack(int pAction, int pCursor);

    public:
    //--Update
    void Update();
    void HandlePlayerControls();

    //--File I/O
    //--Drawing
    void RenderWorld();
    void RenderUI();

    //--Pointer Routing
    //--Static Functions
    static PuzzleFight *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PuzzleFight_GetProperty(lua_State *L);
int Hook_PuzzleFight_SetProperty(lua_State *L);

