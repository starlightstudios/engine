-- |[ ===================================== fnResolveName() ==================================== ]|
--Function that resolves the name of the currently executing script.
-- If a script is in C:/Dogs/Cats/Script.lua the result will be "Script.lua"
function fnResolveName()
	
    -- |[Setup]|
	local sStartPath = LM_GetCallStack(0)
	local iLen = string.len(sStartPath)

    -- |[Seek Backwards]|
	--Determine where the last slash is.
	local sLetter = ""
	for i = iLen, 1, -1 do
		sLetter = string.sub(sStartPath, i, i)
		if(sLetter == "/" or sLetter == "\\") then
			iLen = i
			break
		end
	end

    -- |[Finish Up]|
	local sFinishPath = string.sub(sStartPath, iLen+1)
	return sFinishPath
end