//--Base
#include "TiledLevel.h"

//--Classes
#include "AdventureLevel.h"
#include "TileLayer.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "DebugManager.h"
#include "StarLumpManager.h"

void TiledLevel::ParseFile(StarLumpManager *pSLM)
{
    ///--[Documentation and Setup]
    //--Given a StarLumpManager which should have the given file open, reads and returns a usable
    //  and precached Tiled2DLevel.
    if(!pSLM) return;

    ///--[File Integrity Check]
    //--Basics.
    DebugManager::PushPrint(false, "==> Orthogonal Tiled Level - Begin parse\n");
    if(!pSLM->StandardSeek("AMapInfo", "MAPINFO"))
    {
        DebugManager::PopPrint("Error: No AMapInfo lump was located.\n");
        return;
    }
    VirtualFile *rSLFFile = pSLM->GetVirtualFile();

    //--Make sure that the MapInfoLump has 4 as its type. Fail if it doesn't.
    uint8_t tMapType;
    rSLFFile->Read(&tMapType, sizeof(uint8_t), 1);
    if(tMapType != 4)
    {
        DebugManager::PopPrint("Error: AMapInfo lump lists type as %i, not 4.\n", tMapType);
        return;
    }

    ///--[Tilesets]
    //--Get the total number of tilesets and their names.
    mTilesetsTotal = 0;
    rSLFFile->Read(&mTilesetsTotal, sizeof(int16_t), 1);
    DebugManager::Print("Tilesets: %i\n", mTilesetsTotal);

    //--Allocate and read.
    SetMemoryData(__FILE__, __LINE__);
    mTilesetPacks = (TilesetInfoPack *)starmemoryalloc(sizeof(TilesetInfoPack) * mTilesetsTotal);
    for(int i = 0; i < mTilesetsTotal; i ++)
    {
        //--Init.
        mTilesetPacks[i].Initialize();

        //--Name.
        mTilesetPacks[i].mName = rSLFFile->ReadLenString();
        DebugManager::Print(" Name: %s\n", mTilesetPacks[i].mName);

        //--Other data.
        mTilesetPacks[i].mTilesToThisPoint = 0;
        mTilesetPacks[i].mTilesTotal = 0;
        rSLFFile->Read(&mTilesetPacks[i].mTilesToThisPoint, sizeof(int16_t), 1);
        rSLFFile->Read(&mTilesetPacks[i].mTilesTotal, sizeof(int16_t), 1);
        DebugManager::Print("  Data: %i %i\n", mTilesetPacks[i].mTilesToThisPoint, mTilesetPacks[i].mTilesTotal);
    }

    ///--[Image Creation]
    //--With the tileset data read out, create StarBitmaps and locate the tileset lumps.
    for(int i = 0; i < mTilesetsTotal; i ++)
    {
        //--Set flags.
        StarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
        StarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;

        //--Name of the image's lump.
        char *tBuffer = InitializeString("Tileset|%s", mTilesetPacks[i].mName);
        mTilesetPacks[i].mTileset = NULL;

        //--Edges. These get padded normally to prevent tiles that size up from bleeding into each other.
        //  If the name of the tileset starts with "Animation_" then padding never occurs, these are not normal tiles.
        if(PAD_TILE_EDGES && strncasecmp(mTilesetPacks[i].mName, "Animation_", 10))
        {
            mTilesetPacks[i].mTileset = pSLM->GetPaddedTileImage(tBuffer);
            mTilesetPacks[i].mOwnsTileset = true;
        }
        //--Unpadded images are extracted using GL_LINEAR. This is only the case for animations.
        else
        {
            mTilesetPacks[i].mTileset = pSLM->GetImage(tBuffer);
            mTilesetPacks[i].mOwnsTileset = true;
        }

        //--Clean flags.
        StarBitmap::RestoreDefaultTextureFlags();

        //--Clean memory.
        free(tBuffer);
    }

    ///--[Tile Layers]
    //--Begin work on tile layers. There can be up to 100 (!) and are always in order.
    int tLumpCount = 0;
    char *tLumpNameBuffer = InitializeString("Layer %02i", tLumpCount);
    while(pSLM->StandardSeek(tLumpNameBuffer, "LAYERDATA"))
    {
        //--Does this lump have flipping data?
        bool tHasFlipData = false;
        char *tHeaderString = pSLM->GetHeaderOf(tLumpNameBuffer);
        if(tHeaderString[9] >= '1') tHasFlipData = true;

        //--Clean up.
        free(tHeaderString);

        //--Let the subroutine handle it.
        ParseLayerLump(pSLM, tHasFlipData);

        //--Seek to the next object.
        tLumpCount ++;
        free(tLumpNameBuffer);
        tLumpNameBuffer = InitializeString("Layer %02i", tLumpCount);
    }

    //--Clean up.
    free(tLumpNameBuffer);

    ///--[Objects]
    //--Build objects.
    DebugManager::Print(" Scanning object data.\n");
    ParseObjectData();

    ///--[Compilation]
    //--Post-parse compilation.
    DebugManager::Print(" Compiling.\n");
    CompileAfterParse();

    ///--[Debug]
    DebugManager::PopPrint("Parsing completed.\n");
}
void TiledLevel::ParseLayerLump(StarLumpManager *pSLM, bool pHasFlipData)
{
    //--Parses out a layer lump. The StarLumpManager is assumed to be in place already.
    if(!pSLM) return;
    VirtualFile *rSLFFile = pSLM->GetVirtualFile();
    DebugManager::Print("Parsing Layer\n");

    //--Get the layer's type. We handle "Tile" right now.
    char *tType = rSLFFile->ReadLenString();
    if(!strcasecmp(tType, "Tile"))
    {
        //--Read the name of the layer.
        char *nName = rSLFFile->ReadLenString();
        DebugManager::Print(" Layer name: %s\n", nName);

        //--Custom properties. Uses a 16 bit integer.
        int tCustomPropertiesTotal = 0;
        rSLFFile->Read(&tCustomPropertiesTotal, sizeof(int16_t), 1);

        //--Read out the properties as key-value strings. We don't store them yet.
        bool tIsMidground = false;
        bool tIsCollisionLayer = false;
        bool tIsAnimatedLayer = false;
        bool tIsAnimatedSlowlyLayer = false;
        bool tIsOscillating = false;
        bool tNoDepthMask = false;
        bool tIsLyingDepth = false;
        int tAnimationFrames = 0;
        int tAnimationOffset = 0;
        int tAnimationTPF = 0;
        float tUseDepth = 0.0f;
        float tLieDepth = 0.0f;
        int tCollisionDepth = 0;
        for(int i = 0; i < tCustomPropertiesTotal; i ++)
        {
            //--Get the key/value pair.
            char *tKey = rSLFFile->ReadLenString();
            char *tVal = rSLFFile->ReadLenString();

            //--Check property types.
            if(!strcasecmp(tKey, "Depth"))
            {
                tUseDepth = atof(tVal);
            }
            //--Midground layer.
            else if(!strcasecmp(tKey, "Midground"))
            {
                tIsMidground = true;
            }
            //--Lying Depth. Changes lighting depth.
            else if(!strcasecmp(tKey, "LieDepth"))
            {
                tIsLyingDepth = true;
                tLieDepth = atof(tVal);
            }
            //--Is a collision layer.
            else if(!strcasecmp(tKey, "Collision"))
            {
                DebugManager::Print("  Collision layer.\n");
                tIsCollisionLayer = true;
            }
            //--Depth of the collision layer.
            else if(!strcasecmp(tKey, "CollisionDepth"))
            {
                DebugManager::Print("  Collision depth %i\n", atoi(tVal));
                tCollisionDepth = atoi(tVal);
            }
            //--Is animated layer.
            else if(!strcasecmp(tKey, "Animated"))
            {
                tIsAnimatedLayer = true;
            }
            //--Is animated layer.
            else if(!strcasecmp(tKey, "AnimatedSlowly"))
            {
                tIsAnimatedSlowlyLayer = true;
            }
            //--Is animated with X frames.
            else if(!strcasecmp(tKey, "AnimatedFrames"))
            {
                tAnimationFrames = atoi(tVal);
            }
            //--Is animated with X offset for each frame.
            else if(!strcasecmp(tKey, "AnimatedOffset"))
            {
                tAnimationOffset = atoi(tVal);
            }
            //--Ticks Per Frame for animations.
            else if(!strcasecmp(tKey, "AnimatedTPF"))
            {
                tAnimationTPF = atoi(tVal);
            }
            //--Layer oscillates.
            else if(!strcasecmp(tKey, "Oscillates"))
            {
                tIsOscillating = true;
            }
            //--Layer does not write to the depth buffer.
            else if(!strcasecmp(tKey, "NoDepth"))
            {
                tNoDepthMask = true;
            }

            //--Clean.
            free(tKey);
            free(tVal);
        }
        DebugManager::Print(" Layer has %i properties\n", tCustomPropertiesTotal);

        //--Sizing and offset information.
        int16_t tSizingData[4];
        memset(tSizingData, 0, sizeof(int16_t) * 4);
        rSLFFile->Read(tSizingData, sizeof(int16_t), 4);

        //--Tile data.
        SetMemoryData(__FILE__, __LINE__);
        int16_t *tTileData = (int16_t *)starmemoryalloc(sizeof(int16_t) * tSizingData[2] * tSizingData[3]);
        memset(tTileData, 0, sizeof(int16_t) * tSizingData[2] * tSizingData[3]);
        rSLFFile->Read(tTileData, sizeof(int16_t), tSizingData[2] * tSizingData[3]);

        //--Flip data. Only gets read if the flag is on. Otherwise, is the same size as tile data.
        int16_t *tFlipData = NULL;
        if(pHasFlipData)
        {
            SetMemoryData(__FILE__, __LINE__);
            tFlipData = (int16_t *)starmemoryalloc(sizeof(int16_t) * tSizingData[2] * tSizingData[3]);
            memset(tFlipData, 0, sizeof(int16_t) * tSizingData[2] * tSizingData[3]);
            rSLFFile->Read(tFlipData, sizeof(int16_t), tSizingData[2] * tSizingData[3]);
        }

        //--Store the X/YSize for the level, if it's bigger. Largest layer = levels's size.
        if(tSizingData[2] > mXSize) mXSize = tSizingData[2];
        if(tSizingData[3] > mYSize) mYSize = tSizingData[3];

        //--Create a TileLayer and upload the information.
        TileLayer *nLayer = new TileLayer();
        nLayer->SetDepth(tUseDepth);
        nLayer->SetMidgroundFlag(tIsMidground);
        nLayer->SetAnimatedFlag(tIsAnimatedLayer);
        nLayer->SetAnimatedSlowlyFlag(tIsAnimatedSlowlyLayer);
        nLayer->SetOscillationFlag(tIsOscillating);
        if(tNoDepthMask) nLayer->SetDepthMaskFlag(true);
        nLayer->ProvideTileReferences(mTilesetsTotal, mTilesetPacks);
        nLayer->ProvideTileData(tSizingData[2], tSizingData[3], tTileData, false);
        if(pHasFlipData) nLayer->ProvideFlipData(tSizingData[2], tSizingData[3], tFlipData);

        //--Lying depth.
        if(tIsLyingDepth)
        {
            nLayer->SetLyingDepth(tLieDepth);
        }

        //--Add animation properties if specified:
        if(tAnimationFrames > 1 && tAnimationOffset > 0 && tAnimationTPF > 0)
        {
            nLayer->AnimatedManual(tAnimationFrames, tAnimationOffset, tAnimationTPF);
        }

        //--If it's a collision layer, get the collision data out.
        if(tIsCollisionLayer)
        {
            //--If this index is higher than the current collision size, allocate space.
            if(tCollisionDepth+1 > mCollisionLayersTotal)
            {
                int tOldSize = mCollisionLayersTotal;
                mCollisionLayersTotal = tCollisionDepth + 1;
                mCollisionLayers = (CollisionPack *)realloc(mCollisionLayers, sizeof(CollisionPack) * mCollisionLayersTotal);

                //--Initialize all new slots.
                for(int i = tOldSize; i < mCollisionLayersTotal; i ++)
                {
                    mCollisionLayers[i].Initialize();
                }
            }

            //--Process the collisions.
            mCollisionLayers[tCollisionDepth].mCollisionData = nLayer->ProcessCollision(mCollisionLayers[tCollisionDepth].mCollisionSizeX, mCollisionLayers[tCollisionDepth].mCollisionSizeY);
            mCollisionLayers[tCollisionDepth].mCollisionFlipData = nLayer->ProcessCollisionFlips(mCollisionLayers[tCollisionDepth].mCollisionSizeX, mCollisionLayers[tCollisionDepth].mCollisionSizeY);
            nLayer->SetDoNotRenderFlag(true);
        }

        //--Add it.
        mTileLayers->AddElement(nName, nLayer, &TileLayer::DeleteThis);

        //--Clean up.
        free(nName);
        free(tTileData);
        free(tFlipData);
    }
    //--Okay, so it's an Object type layer. Scan it for enemies, cameras, slip zones, etc.
    else if(!strcasecmp(tType, "Object"))
    {
        //--Handled by one easy subroutine!
        ReadObjectData(rSLFFile);
    }
    //--It's an image. These are used for backgrounds.
    else if(!strcasecmp(tType, "Image"))
    {
        //ReadImageData(pSLM, rSLFFile);
    }
    //--Other types are unhandled.
    else
    {
        DebugManager::ForcePrint("Error parsing layer lump: Unknown type %s\n", tType);
    }

    DebugManager::Print("Layer parsed.\n");

    //--Clean up.
    free(tType);
}
