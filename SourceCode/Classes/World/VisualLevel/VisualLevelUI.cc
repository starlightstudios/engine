//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "ContextMenu.h"
#include "FlexMenu.h"
#include "InventoryItem.h"
#include "PandemoniumRoom.h"
#include "VisualRoom.h"
#include "WorldContainer.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarButton.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "MapManager.h"

//--[Construction]
void VisualLevel::SetUIConstants()
{
    //--[Documenation]
    //--Called during class initialization, sets up constants used by the below functions. Requires the images to have
    //  been referenced or does nothing.
    if(!Images.mIsReady) return;

    //--[Redundancy Check]
    //--If the screen doesn't need resizing, don't do anything here.
    GLOBAL *rGlobal = Global::Shared();
    //if(mPreviousScreenW == rGlobal->gWindowWidth && mPreviousScreenH == rGlobal->gWindowHeight) return;

    //--Store these values.
    mPreviousScreenW = rGlobal->gWindowWidth;
    mPreviousScreenH = rGlobal->gWindowHeight;

    //--[Radar and Console]
    //--Radar. Has a fixed size since the update cycle needs to handle mousewheel.
    float cRadarDim = ConsolePane.cHei;
    mRadarSize.Set(VIRTUAL_CANVAS_X - cRadarDim, VIRTUAL_CANVAS_Y - cRadarDim, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);

    //--Modify the console to reach the radar.
    ConsolePane.cLft = 0.0f;
    ConsolePane.cWid = mRadarSize.mLft - ConsolePane.cLft;

    //--Modify the positions of the console and radar dynamically. The radar is always in the bottom right, and the
    //  console in the bottom left, and they meet. In fullscreen, their positions change.
    {
        //--Vertical reposition.
        ConsolePane.cTop = (VIRTUAL_CANVAS_Y + DisplayManager::xScaledOffsetY) - ConsolePane.cHei;
        mRadarSize.mTop  = (VIRTUAL_CANVAS_Y + DisplayManager::xScaledOffsetY) - ConsolePane.cHei;
        mRadarSize.mBot = mRadarSize.mTop + ConsolePane.cHei;

        //--Radar.
        mRadarSize.mLft = (VIRTUAL_CANVAS_X + DisplayManager::xScaledOffsetX) - cRadarDim;
        mRadarSize.mRgt = (VIRTUAL_CANVAS_X + DisplayManager::xScaledOffsetX);

        //--Console, moves left and stretches.
        ConsolePane.cLft = -DisplayManager::xScaledOffsetX;
        ConsolePane.cWid = mRadarSize.mLft - ConsolePane.cLft;
    }
}
void VisualLevel::CreateRemapPack(const char *pDLPath, float pLft, float pTop, float fWid, float fHei)
{
    //--Creates a new remap pack, assuming the image exists.
    if(!pDLPath) return;

    //--Get the image and check it.
    StarBitmap *rRefPtr = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    if(!rRefPtr) return;

    //--Create and register the pack.
    SetMemoryData(__FILE__, __LINE__);
    CharacterCardRemapPack *nPack = (CharacterCardRemapPack *)starmemoryalloc(sizeof(CharacterCardRemapPack));
    nPack->rCheckPtr = rRefPtr;
    nPack->mLft = pLft / rRefPtr->GetTrueWidth();
    nPack->mTop = pTop / rRefPtr->GetTrueHeight();
    nPack->mRgt = nPack->mLft + (fWid / rRefPtr->GetWidth());
    nPack->mBot = nPack->mTop + (fHei / rRefPtr->GetHeight());
    mPortraitRemapList->AddElement("X", nPack, &FreeThis);

    //--Flip the remap coordinates since textures are flipped in our Y coordinate system.
    nPack->mTop = 1.0f - nPack->mTop;
    nPack->mBot = 1.0f - nPack->mBot;
}

//--[Update]
bool VisualLevel::HandleUIUpdate()
{
    //--Handles the UI update sequence which was normally the entire update sequence for the parent class.
    //  Returns true if the update was handled, in which case hotkeys and controls should be ignored.
    //--Does not handle the ZoneEditor, that's in the main update cycle.
    if(!Images.mIsReady || mMoveToNodeID) return false;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--New turn timer.
    mNewTurnTimer ++;

    //--If there's something on the menu stack, let that handle the update.
    if(MapManager::Fetch()->UpdateMenuStack(mHasPendingClose))
    {
        mHasPendingClose = false;
        return true;
    }

    //--Get mouse position.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Update the context menu if it's visible.
    if(mShowContextMenu)
    {
        //--Run the update.
        bool tHandledClicks = false;
        mContextMenu->Update(tMouseX, tMouseY);

        //--If the update was not handled, check if a left-click occurred. If it did, hide the menu.
        if(!mContextMenu->HandledUpdate() && rControlManager->IsFirstPress("MouseLft"))
        {
            HideContextMenu();
            tHandledClicks = true;
        }

        //--If the menu handled the update but still wants to close, do that now.
        if(mContextMenu->HasPendingClose())
        {
            HideContextMenu();
            tHandledClicks = true;
        }

        //--In any case, ignore the rest of the update.
        return tHandledClicks;
    }

    //--If the level is currently waiting on a keypress, handle that.
    if(mIsConsoleWaitingOnKeypress)
    {
        //--Wipe player controls!
        Actor::xAreControlsEnabled = false;
        Actor::xInstruction = INSTRUCTION_NOENTRY;

        //--Upon pressing a key, unflip the flag.
        if(rControlManager->IsAnyKeyPressed())
        {
            //--Flag.
            mIsConsoleWaitingOnKeypress = false;

            //--If we have a pending instruction, run that.
            if(mPostExecScript)
            {
                //--Free the old copy. We don't do this after the script runs, since it might replace
                //  the path (or not).
                char *tOldScriptPtr = mPostExecScript;
                mPostExecScript = NULL;

                //--Execute.
                LuaManager::Fetch()->ExecuteLuaFile(tOldScriptPtr, 1, "N", (float)mPostExecFiringCode);

                //--Clean up.
                free(tOldScriptPtr);
            }
        }

        return true;
    }

    //--Check if the player clicked something on the character pane. If this function returns true, stop the
    //  rest of the update.
    if(HandleCharacterCardClick(tMouseX, tMouseY)) return true;

    //--Radar click. Same as above.
    if(HandleRadarClick(tMouseX, tMouseY)) return true;

    //--Check if the Enter key has been pushed. This skips the turn.
    if(rControlManager->IsFirstPress("Enter") && !mIsFreeMove)
    {
        //fprintf(stderr, "Skipping turn!\n");
        mRefreshClickPoints = 2;
        Actor::xInstruction = INSTRUCTION_SKIPTURN;
        return true;
    }

    //--If the mouse is over the skip turn button, handle that.
    if(IsPointWithin2DReal(tMouseX, tMouseY, mSkipTurnBtn))
    {
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            mRefreshClickPoints = 2;
            Actor::xInstruction = INSTRUCTION_SKIPTURN;
            return true;
        }
    }

    //--If the mouse is over the system menu button, handle that.
    if(tMouseX >= ConsolePane.cLft + 4.0f && tMouseX <= ConsolePane.cLft + 4.0f + Images.Data.rSystemMenuBtn->GetWidth() && tMouseY >= 4.0f && tMouseY <= 4.0f + Images.Data.rSystemMenuBtn->GetHeight())
    {
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            FlexMenu *nNewMenu = new FlexMenu();
            LuaManager::Fetch()->PushExecPop(nNewMenu, mSystemMenuPopulationScript);
            MapManager::Fetch()->PushMenuStack(nNewMenu);
            return true;
        }
    }

    //--If the mouse is over the radar display, handle that.
    if(tMouseX >= mRadarSize.mLft && tMouseX <= mRadarSize.mRgt && tMouseY >= mRadarSize.mTop && tMouseY <= mRadarSize.mBot)
    {
        //--Mouse scroll wheel zooms the radar display in and out.
        if(tMouseZ != mPreviousZ)
        {
            //--Zoom amount. As the map zooms out, it zooms faster.
            float cFactor = 0.10f;
            if(xMapZoom > 1.0f) cFactor = 0.25f;
            if(xMapZoom > 2.0f) cFactor = 0.50f;

            //--Modify and clamp.
            xMapZoom = xMapZoom + ((tMouseZ - mPreviousZ) * cFactor);
            if(xMapZoom < 0.70f) xMapZoom = 0.70f;
            if(xMapZoom > 5.00f) xMapZoom = 5.00f;

            //--Recompute the distance scaler.
            VisualRoom::xDistanceScale = 0.10f * xMapZoom;
        }
    }
    mPreviousZ = tMouseZ;

    //--Check if we need to reconstitute the inventory. This may happen if any of the above updates
    //  added items or removed them.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerCharacter && rPlayerCharacter->GetInventoryReconstituteFlag() == true) ReconstituteInventory();

    //--The update was not handled, return false and allow hotkeys to execute.
    return false;
}
bool VisualLevel::HandleCharacterCardClick(int pMouseX, int pMouseY)
{
    //--Checks if the player clicked something on the character card. Returns true if it handled the update,
    //  false if not.
    //--This function checks both left and right clicks, since both do something. However, if the mouse isn't
    //  even over the card, it never handles the update.
    ControlManager *rControlManager = ControlManager::Fetch();
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerActor) return false;

    //--If neither left or right click occurred, then do nothing.
    bool tIsLftClick = rControlManager->IsFirstPress("MouseLft");
    bool tIsRgtClick = rControlManager->IsFirstPress("MouseRgt");
    if(!tIsLftClick && !tIsRgtClick) return false;

    //--Is this a left-click over a self-menu button? Execute that.
    if(tIsLftClick && mStoredSelfMenu && IsPointWithin2DReal(pMouseX, pMouseY, mSelfMenuDimensions))
    {
        //--Get the slot of the button in question.
        float cVIndent = 8.0f;
        float cSpacing = 20.0f;
        int tIndex = ((pMouseY - mSelfMenuDimensions.mTop) - cVIndent) / cSpacing;

        //--Make sure the button exists.
        StarButton *rButton = mStoredSelfMenu->GetButtonI(tIndex);
        if(!rButton) return false;

        //--Execute.
        rButton->Execute();
        delete mStoredSelfMenu;
        mStoredSelfMenu = NULL;
        return true;
    }

    //--Is this a left-click over the character portrait? That's an "Examine-self" action.
    if(tIsLftClick && IsPointWithin2DReal(pMouseX, pMouseY, mCharacterPortraitDim))
    {
        ContextMenu::ExamineTargetActor(rPlayerActor);
        return true;
    }

    //--Is this a left-click over the inventory? That will be an attempt to use the item, or examine it
    //  if it's a weapon/armor.
    if(tIsLftClick && IsPointWithin2DReal(pMouseX, pMouseY, mInventoryDim))
    {
        //--Takes priority if this happens to be over one of the scroll buttons.
        if(IsPointWithin2DReal(pMouseX, pMouseY, mItemScrollUpBtn))
        {
            mItemOffset --;
            if(mItemOffset < 0) mItemOffset = 0;
        }
        //--Down.
        else if(IsPointWithin2DReal(pMouseX, pMouseY, mItemScrollDnBtn))
        {
            int tInventoryEntries = 2 + mInventoryList->GetListSize();
            mItemOffset ++;
            if(mItemOffset >= tInventoryEntries - mItemsRendered + 1) mItemOffset = tInventoryEntries - mItemsRendered;
        }
        //--Must have been over an item.
        else
        {
            //--Figure out the slot of the item in question.
            float cSpacing = 20.0f;
            int tSlot = ((pMouseY - mInventoryDim.mTop) / cSpacing) + mItemOffset;
            int tInventoryEntries = 2 + mInventoryList->GetListSize();
            if(tSlot < 0 || tSlot >= tInventoryEntries) return false;

            //--If the slot is off the rendering edge, ignore it.
            if(xUseAlternateUI && tSlot > mItemOffset + mItemsRendered) return false;

            //--If this is slot 0, that's *always* the weapon (no flags!), so examine it. Do nothing if the
            //  player has no weapon.
            if(tSlot == 0)
            {
                InventoryItem *rPlayerWeapon = rPlayerActor->GetWeapon();
                if(rPlayerWeapon) ContextMenu::ExamineTargetItem(rPlayerWeapon);
            }
            //--If this is slot 1, that's *always* the armor. As above.
            else if(tSlot == 1)
            {
                InventoryItem *rPlayerArmor = rPlayerActor->GetArmor();
                if(rPlayerArmor) ContextMenu::ExamineTargetItem(rPlayerArmor);
            }
            //--Otherwise, this is something from the (compressed) inventory. A left-click attempts to use it.
            else
            {
                //--Fail if the character is unable to, you know, use items. Duh.
                if(rPlayerActor->IsStunned() || rPlayerActor->IsControlled()) return true;

                //--Use the item.
                InventoryPack *rPack = (InventoryPack *)mInventoryList->GetElementBySlot(tSlot - 2);
                if(rPack) rPlayerActor->UseItemByPtr(rPack->rItem);
            }
        }

        return true;
    }

    //--Is this a right-click over the inventory? This will drop whatever the item in question was.
    if(tIsRgtClick && IsPointWithin2DReal(pMouseX, pMouseY, mInventoryDim))
    {
        //--If the player is unable to do anything, ignore this press.
        if(rPlayerActor->IsStunned() || rPlayerActor->IsControlled()) return true;

        //--Takes priority if this happens to be over one of the scroll buttons. Right-click is caught but does nothing.
        if(IsPointWithin2DReal(pMouseX, pMouseY, mItemScrollUpBtn))
        {
        }
        //--Down.
        else if(IsPointWithin2DReal(pMouseX, pMouseY, mItemScrollDnBtn))
        {
        }
        //--Must have been over an item.
        else
        {
            //--Figure out the slot of the item in question.
            float cSpacing = 20.0f;
            int tSlot = ((pMouseY - mInventoryDim.mTop) / cSpacing) + mItemOffset;
            int tInventoryEntries = 2 + mInventoryList->GetListSize();
            if(tSlot < 0 || tSlot >= tInventoryEntries) return false;

            //--If the slot is off the rendering edge, ignore it.
            if(xUseAlternateUI && tSlot > mItemOffset + mItemsRendered) return false;

            //--Try to drop the weapon, if we have one.
            if(tSlot == 0)
            {
                rPlayerActor->UnequipWeapon();
            }
            //--If this is slot 1, that's *always* the armor. As above.
            else if(tSlot == 1)
            {
                rPlayerActor->UnequipArmor();
            }
            //--Otherwise, this is something from the (compressed) inventory. Drop it.
            else
            {
                InventoryPack *rPack = (InventoryPack *)mInventoryList->GetElementBySlot(tSlot - 2);
                if(rPack) rPlayerActor->DropItemByPtr(rPack->rItem);
            }

            //--Turn ends. If the player actually did drop something, this flag will be set to true.
            if(rPlayerActor->GetInventoryReconstituteFlag())
            {
                mRefreshClickPoints = 2;
                Actor::xInstruction = INSTRUCTION_SKIPTURN;
            }
        }
        return true;
    }

    //--All checks failed, this update didn't handle anything.
    return false;
}

//--[Worker Functions]
void VisualLevel::RenderBar(StarlightColor pColor, float pX, float pY, float pW, float pH, float pPercentFull)
{
    //--Renders a health bar (or any other kind of bar) according to the given properties.
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);

        //--Render the backing bar.
        glColor3f(pColor.r * 0.5f, pColor.g * 0.5f, pColor.b * 0.5f);
        glVertex2f(pX      - 2.0f, pY      - 2.0f);
        glVertex2f(pX + pW + 2.0f, pY      - 2.0f);
        glVertex2f(pX + pW + 2.0f, pY + pH + 2.0f);
        glVertex2f(pX      - 2.0f, pY + pH + 2.0f);

        //--Render the front bar.
        glColor3f(pColor.r, pColor.g, pColor.b);
        glVertex2f(pX,                     pY     );
        glVertex2f(pX + pW * pPercentFull, pY     );
        glVertex2f(pX + pW * pPercentFull, pY + pH);
        glVertex2f(pX,                     pY + pH);

    //--Clean up.
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
}

//--[Rendering]
void VisualLevel::RenderNavigation()
{
    //--Does nothing, removes navigation rendering from the base class.
}
void VisualLevel::RenderCharacterPane()
{
    //--Renders the character pane in the top-right corner of the screen. Shows the player's portrait, status, and inventory.
    //  The inventory expands dynamically as it grows, but everything else is fixed-size.
    if(xUseAlternateUI) { RenderAltCharacterPane(); return; }
    if(!Images.mIsReady) return;

    //--Scaling actions only affect the width. Heights are fixed.
    StarFont::xScaleAffectsX = true;
    StarFont::xScaleAffectsY = false;

    //--Font.
    StarFont *rCharactersFont = PandemoniumLevel::Images.Data.rUIFontMedium;

    //--[Initial Values]
    //--Determine the initial position of the character pane. These values change as the objects render.
    float cIns = 8.0f;
    float cLft = VIRTUAL_CANVAS_X - 174.0f;
    float cTop = 4.0f;
    float cWid = 170.0f;
    float cHei = 80.0f;
    float cRgt = cLft + cWid;
    float cBot = cTop + cHei;

    //--Recompute the borders. We need to make the overlay cover the whole screen, accounting for no letterbox in 3D mode.
    //if(rGlobal->gWindowWidth > VIRTUAL_CANVAS_X)
    {
        //--Horizontal.
        cLft = cLft + (DisplayManager::xScaledOffsetX);
        cRgt = cLft + cWid;

        //--Vertical.
        cTop = cTop - (DisplayManager::xScaledOffsetY);
        cBot = cBot - (DisplayManager::xScaledOffsetY);
    }

    //--[Self-Menu]
    //--The Self-Menu is already expanded and placed next to the character pane. It periodically updates, specifically whenever the player gains or loses
    //  abilities (such as during a TF).
    if(!mStoredSelfMenu && mSelfMenuPopulationScript)
    {
        mStoredSelfMenu = new FlexMenu();
        LuaManager::Fetch()->PushExecPop(mStoredSelfMenu, mSelfMenuPopulationScript);
    }

    //--Render the self-menu.
    if(mStoredSelfMenu)
    {
        //--Get the button list. All except the first will render here.
        StarLinkedList *rButtonList = mStoredSelfMenu->GetButtonList();

        //--Size the border.
        float cHIndent = 8.0f;
        float cVIndent = 8.0f;
        float cSpacing = 20.0f;
        float cSelfMenuWid = 180.0f;
        mSelfMenuDimensions.SetWH(cLft - cSelfMenuWid, cTop, cSelfMenuWid, (rButtonList->GetListSize() * cSpacing) + (cVIndent * 2.0f));
        RenderBorderCardOver(mSelfMenuDimensions.mLft, mSelfMenuDimensions.mTop, mSelfMenuDimensions.mRgt, mSelfMenuDimensions.mBot, 0x01FF);

        //--Render the buttons.
        for(int i = 0; i < rButtonList->GetListSize(); i ++)
        {
            //--Get and check.
            StarButton *rButton = (StarButton *)rButtonList->GetElementBySlot(i);
            if(!rButton) continue;

            //--Get the text width, and downscale it to make it narrower if it's too long.
            float tScale = ResolveLargestScale(rCharactersFont, mSelfMenuDimensions.GetWidth() - (cHIndent * 2.0f), rButton->GetName());

            //--Render the text.
            float cLft = mSelfMenuDimensions.mLft + cHIndent;
            float cTop = mSelfMenuDimensions.mTop + (i * cSpacing) + cVIndent;
            rCharactersFont->DrawText(cLft, cTop, 0, tScale, rButton->GetName());
        }
    }

    //--Button, renders next to the portrait card.
    //Images.Data.rSelfMenuBtn->Draw(cLft - Images.Data.rSelfMenuBtn->GetWidth(), cTop);
    //mSelfMenuBtnSize.SetWH(cLft - Images.Data.rSelfMenuBtn->GetWidth(), cTop, Images.Data.rSelfMenuBtn->GetWidth(), Images.Data.rSelfMenuBtn->GetHeight());

    //--[Portrait Card]
    //--Card background.
    RenderBorderCardOver(cLft, cTop, cRgt, cBot, 0x003F);

    //--Store this position. It's not dynamic.
    mCharacterPortraitDim.Set(cLft + cIns, cTop + cIns, cRgt - cIns, cBot - cIns);

    //--Get the current character portrait.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerCharacter && rPlayerCharacter->GetActiveImage())
    {
        //--Get and bind.
        StarBitmap *rPlayerPortrait = rPlayerCharacter->GetActiveImage();
        rPlayerPortrait->Bind();

        //--Static remap pack. Since portraits don't change very often, we store the last one and
        //  refresh it only if the portrait changes. This is automatic.
        static CharacterCardRemapPack *xrLastPack = NULL;
        if(xrLastPack && xrLastPack->rCheckPtr != rPlayerPortrait) xrLastPack = NULL;

        //--If there was no xrLastPack, then either the portrait changed or this is the first pass. In
        //  either case, find the current portrait and set its pack.
        if(!xrLastPack)
        {
            CharacterCardRemapPack *rPack = (CharacterCardRemapPack *)mPortraitRemapList->PushIterator();
            while(rPack)
            {
                if(rPack->rCheckPtr == rPlayerPortrait)
                {
                    xrLastPack = rPack;
                    mPortraitRemapList->PopIterator();
                    break;
                }
                rPack = (CharacterCardRemapPack *)mPortraitRemapList->AutoIterate();
            }
        }

        //--If no portrait pack was found, use the defaults. This will not be pretty!
        float tTxL =   0.0f / rPlayerPortrait->GetTrueWidth();
        float tTxR = 162.0f / rPlayerPortrait->GetTrueWidth();
        float tTxT = 1.0f - ( 49.0f / rPlayerPortrait->GetTrueHeight());
        float tTxB = 1.0f - (125.0f / rPlayerPortrait->GetTrueHeight());
        if(xrLastPack)
        {
            tTxL = xrLastPack->mLft;
            tTxR = xrLastPack->mRgt;
            tTxT = xrLastPack->mTop;
            tTxB = xrLastPack->mBot;
        }

        //--Render the small version of the image. It covers the center of the pane.
        glBegin(GL_QUADS);
            glTexCoord2f(tTxL, tTxT); glVertex2f(cLft + cIns, cTop + cIns);
            glTexCoord2f(tTxR, tTxT); glVertex2f(cRgt - cIns, cTop + cIns);
            glTexCoord2f(tTxR, tTxB); glVertex2f(cRgt - cIns, cBot - cIns);
            glTexCoord2f(tTxL, tTxB); glVertex2f(cLft + cIns, cBot - cIns);
        glEnd();
    }

    //--[Player Status Card]
    //--Recompute the position. Horizontal does not change.
    cTop = cBot - cIns;
    cBot = cTop + cHei + 8.0f;
    RenderBorderCardOver(cLft, cTop, cRgt, cBot, 0x003F);

    //--Render the values and bars.
    glColor3f(1.0f, 1.0f, 1.0f);
    if(rPlayerCharacter)
    {
        //--Get values.
        CombatStats tPlayerStats = rPlayerCharacter->GetCombatStatistics();

        //--Health.
        RenderBar(StarlightColor::MapRGBF(1.0f, 0.0f, 0.0f), cLft + 28.0f, cTop + 22.0f, 132.0f, 4.0f, (float)tPlayerStats.mHP / (float)tPlayerStats.mHPMax);
        rCharactersFont->DrawText(cLft + 10.0f, cTop +  8.0f, 0, "H");
        rCharactersFont->DrawTextArgs(cLft + 28.0f, cTop +  8.0f, 0, 1.0f, "%i/%i", tPlayerStats.mHP, tPlayerStats.mHPMax);
        if(rPlayerCharacter->IsStunned())
        {
            StarFont::xScaleAffectsY = false;
            rCharactersFont->DrawTextArgs(cLft + 80.0f, cTop +  8.0f, 0, 0.6f, "Stunned! (%i)", rPlayerCharacter->GetTurnsToRecovery());
        }

        //--Willpower.
        RenderBar(StarlightColor::MapRGBF(0.0f, 0.0f, 1.0f), cLft + 28.0f, cTop + 46.0f, 132.0f, 4.0f, (float)tPlayerStats.mWillPower / (float)tPlayerStats.mWillPowerMax);
        rCharactersFont->DrawText(cLft +  6.0f, cTop + 32.0f, 0, "W");
        rCharactersFont->DrawTextArgs(cLft + 28.0f, cTop + 32.0f, 0, 1.0f, "%i/%i", tPlayerStats.mWillPower, tPlayerStats.mWillPowerMax);
        if(tPlayerStats.mWillPower < 1)
        {
            StarFont::xScaleAffectsY = false;
            rCharactersFont->DrawTextArgs(cLft + 80.0f, cTop + 32.0f, 0, 0.6f, "Confounded!");
        }

        //--Stamina.
        RenderBar(StarlightColor::MapRGBF(0.0f, 1.0f, 0.0f), cLft + 28.0f, cTop + 70.0f, 132.0f, 4.0f, (float)tPlayerStats.mStamina / (float)tPlayerStats.mStaminaMax);
        rCharactersFont->DrawText(cLft + 11.0f, cTop + 56.0f, 0, "S");
        rCharactersFont->DrawTextArgs(cLft + 28.0f, cTop + 56.0f, 0, 1.0f, "%i/%i", tPlayerStats.mStamina,   tPlayerStats.mStaminaMax);
        StarFont::xScaleAffectsY = true;
    }

    //--[Status Card]
    //--Game status display. Renders under the system menu button. Length is determined by the longest string.
    float cStatusCardScale = 0.75f;
    float cFontSpacing = 22.0f * cStatusCardScale;
    float tCursor = Images.Data.rSystemMenuBtn->GetHeight() + 6.0f;
    StarFont::xScaleAffectsX = true;
    StarFont::xScaleAffectsY = true;

    //--Render the game status. These strings are set by scripts externally.
    if(mStatusStrings)
    {
        //--Initial loop, determine the longest string.
        float tLongest = 0.0f;
        for(int i = 0; i < mStatusStringsTotal; i ++)
        {
            //--Skip blank lines.
            if(!mStatusStrings[i]) continue;

            //--Check length, store.
            float tLength = rCharactersFont->GetTextWidth(mStatusStrings[i]) * cStatusCardScale * 1.05f;
            if(tLength > tLongest) tLongest = tLength;
        }

        //--Render a card border for this.
        RenderBorderCardOver(ConsolePane.cLft + 4.0f, tCursor, ConsolePane.cLft + tLongest + 160.0f, tCursor + (cFontSpacing * mStatusStringsTotal) + 140.0f, 0x01FF);
        tCursor = tCursor + 8.0f;

        //--Loop, render.
        for(int i = 0; i < mStatusStringsTotal; i ++)
        {
            //--Skip blank lines.
            if(!mStatusStrings[i]) continue;

            //--Render.
            rCharactersFont->DrawText(ConsolePane.cLft + 12.0f, tCursor, 0, cStatusCardScale, mStatusStrings[i]);
            tCursor = tCursor + cFontSpacing;
        }
    }

    //--[Inventory Card]
    //--If the inventory needs to reconstitute, do that now. The parent class handles this.
    if(rPlayerCharacter && rPlayerCharacter->GetInventoryReconstituteFlag()) ReconstituteInventory();

    //--Recompute sizes. This thing resizes dynamically. It always contains at least 2 members to indicate the weapon/armor status.
    float cSpacing = 20.0f;
    int tInventoryEntries = 2 + mInventoryList->GetListSize();
    cTop = cBot - cIns;
    cBot = cTop + (tInventoryEntries * cSpacing) + 16.0f;

    //--Border.
    RenderBorderCardOver(cLft, cTop, cRgt, cBot, 0x00FF);

    //--Store this position. This is dynamic, and can (and will) change through gameplay.
    mInventoryDim.Set(cLft + cIns, cTop + cIns, cRgt - cIns, cBot - cIns);

    //--Render the weapon/armor info.
    if(rPlayerCharacter)
    {
        //--Weapon.
        InventoryItem *rWeapon = rPlayerCharacter->GetWeapon();
        if(rWeapon)
        {
            float tScale = ResolveLargestScale(rCharactersFont, mInventoryDim.GetWidth() - 32.0f, rWeapon->GetName());
            Images.Data.rWeaponIcon->Draw(cLft + 8, cTop + 8);
            rCharactersFont->DrawText(cLft + 28.0f, cTop + 8.0f, 0, tScale, rWeapon->GetName());
        }
        //--No weapon, render no icon and the word "Unarmed".
        else
        {
            Images.Data.rWeaponIcon->Draw(cLft + 8, cTop + 8);
            rCharactersFont->DrawText(cLft + 28.0f, cTop + 8.0f, 0, "No Weapon");
        }

        //--Armor.
        InventoryItem *rArmor = rPlayerCharacter->GetArmor();
        if(rArmor)
        {
            float tScale = ResolveLargestScale(rCharactersFont, mInventoryDim.GetWidth() - 32.0f, rArmor->GetName());
            Images.Data.rArmorIcon->Draw(cLft + 8, cTop + 30);
            rCharactersFont->DrawText(cLft + 28.0f, cTop + 8.0f, 0, tScale, rArmor->GetName());
        }
        //--No weapon, render no icon and the word "Unarmed".
        else
        {
            Images.Data.rArmorIcon->Draw(cLft + 8, cTop + 30);
            rCharactersFont->DrawText(cLft + 28.0f, cTop + 8.0f + cSpacing, 0, "No Armor");
        }
    }

    //--Render the names of each item, and how many we're carrying.
    int tYCursor = cTop + 12.0f + (cSpacing * 2.0f);
    InventoryPack *rPack = (InventoryPack *)mInventoryList->PushIterator();
    while(rPack)
    {
        //--Buffering.
        char tBuffer[128];
        if(rPack->mQuantity > 1)
            sprintf(tBuffer, "%s x%i", rPack->rItem->GetName(), rPack->mQuantity);
        else
            sprintf(tBuffer, "%s", rPack->rItem->GetName());

        //--Render it.
        float tScale = ResolveLargestScale(rCharactersFont, mInventoryDim.GetWidth() - 32.0f, tBuffer);
        rCharactersFont->DrawText(cLft + 28.0f, tYCursor, 0, tScale, tBuffer);

        //--Next.
        tYCursor = tYCursor + cSpacing;
        rPack = (InventoryPack *)mInventoryList->AutoIterate();
    }

    //--[Clean Up]
    //--Reset the scale flags.
    StarFont::xScaleAffectsX = true;
    StarFont::xScaleAffectsY = true;
}

//--[Class Workers]
//--Class call. Passes in the border card implicitly to the static worker.
void VisualLevel::RenderBorderCardOver(float pLft, float pTop, float pRgt, float pBot, uint16_t pFlags)
{
    RenderBorderCardOver(Images.Data.rBorderCard, pLft, pTop, pRgt, pBot, pFlags);
}

//--Overload for RenderBorderCardOver(), uses TwoDimensionReal.
void VisualLevel::RenderBorderCardOver(StarBitmap *pBorderCard, TwoDimensionReal pDimensions, uint16_t pFlags)
{
    VisualLevel::RenderBorderCardOver(pBorderCard, pDimensions.mLft, pDimensions.mTop, pDimensions.mRgt, pDimensions.mBot, pFlags);
}
void VisualLevel::RenderBorderCardOver(StarBitmap *pBorderCard, TwoDimensionReal pDimensions, uint16_t pFlags, StarlightColor pBackingColor)
{
    VisualLevel::RenderBorderCardOver(pBorderCard, pDimensions.mLft, pDimensions.mTop, pDimensions.mRgt, pDimensions.mBot, pFlags, pBackingColor);
}

//--Overload for RenderBorderCardOver(), uses the standard backing color.
void VisualLevel::RenderBorderCardOver(StarBitmap *pBorderCard, float pLft, float pTop, float pRgt, float pBot, uint16_t pFlags)
{
    RenderBorderCardOver(pBorderCard, pLft, pTop, pRgt, pBot, pFlags, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.85f));
}

//--Static call. Uses the static private variable mBorderSizes.
TwoDimensionReal VisualLevel::mBorderSizes[9];
void VisualLevel::RenderBorderCardOver(StarBitmap *pBorderCard, float pLft, float pTop, float pRgt, float pBot, uint16_t pFlags, StarlightColor pBackingColor)
{
    //--Renders the border card over the given area. Only the parts specified by pFlags are rendered, meaning
    //  that if the 1 bit is set, the TL corner is rendered, if the 2 bit is set, the top bar is rendered,
    //  and so on. Note that the bar assumes that un-rendered areas are still "occupied" and therefore will
    //  leave areas blank if all the flags aren't set.
    //--Does nothing if there's just not enough space to render. You must render at least 1 pixel.
    if(!pFlags || !pBorderCard) return;

    //--Atlas constants.
    float cTxL = pBorderCard->GetAtlasLft();
    //float cTxT = pBorderCard->GetAtlasTop();
    //float cTxR = pBorderCard->GetAtlasRgt();
    float cTxB = pBorderCard->GetAtlasBot();

    //--Build the border sizes. This should be called once.
    if(mBorderSizes[0].GetWidth() == 0.0f)
    {
        //--Sizing.
        float cDenomX = pBorderCard->GetWidth();
        float cDenomY = pBorderCard->GetHeight();
        float cTrueDenomX = StarBitmap::xAtlasMaxSize;
        float cTrueDenomY = StarBitmap::xAtlasMaxSize;
        if(!pBorderCard->IsSubBitmap() || true)
        {
            cTrueDenomX = pBorderCard->GetWidth();
            cTrueDenomY = pBorderCard->GetHeight();
        }

        //--Non-constant sizing. Modifying the border images demands modification of these.
        float cWid = 8.0f;
        float cWdP = cWid * 2.0f; //Right edge of central border, stretches.
        float cWdR = cDenomX - cWid; //Right edge minus width.
        float cHei = 8.0f;
        float cHeP = cHei * 2.0f; //Right edge of central border, stretches.
        float cHeB = cDenomY - cHei; //Bottom edge minus height.

        //--Set border texture constants.
        mBorderSizes[0].Set(0.0f, 0.0f,    cWid,    cHei);
        mBorderSizes[1].Set(cWid, 0.0f,    cWdP,    cHei);
        mBorderSizes[2].Set(cWdR, 0.0f, cDenomX,    cHei);
        mBorderSizes[3].Set(0.0f, cHei,    cWid,    cHeP);
        mBorderSizes[4].Set(cWid, cHei,    cWdP,    cHeP);
        mBorderSizes[5].Set(cWdR, cHei, cDenomX,    cHeP);
        mBorderSizes[6].Set(0.0f, cHeB,    cWid, cDenomY);
        mBorderSizes[7].Set(cWid, cHeB,    cWdP, cDenomY);
        mBorderSizes[8].Set(cWdR, cHeB, cDenomX, cDenomY);

        //--Divide by the size of the bitmap to get texture coordinates. Also, flip the Y coordinate.
        for(int i = 0; i < 9; i ++)
        {
            mBorderSizes[i].mLft = cTxL + (mBorderSizes[i].mLft / cTrueDenomX);
            mBorderSizes[i].mTop = cTxB - (mBorderSizes[i].mTop / cTrueDenomY);
            mBorderSizes[i].mRgt = cTxL + (mBorderSizes[i].mRgt / cTrueDenomX);
            mBorderSizes[i].mBot = cTxB - (mBorderSizes[i].mBot / cTrueDenomY);
        }
    }

    //--We need at least this much space to render: cIns * 3.0f. Less and we do nothing.
    float cIns = 8.0f;
    if(pRgt - pLft < cIns * 3.0f) return;
    if(pBot - pTop < cIns * 3.0f) return;

    //--Compute rendering positions.
    float cLMd = pLft + cIns;
    float cTMd = pTop + cIns;
    float cRMd = pRgt - cIns;
    float cBMd = pBot - cIns;

    //--Begin rendering.
    pBorderCard->Bind();
    glBegin(GL_QUADS);

        //--TL Corner.
        if(pFlags & 0x0001)
        {
            glTexCoord2f(mBorderSizes[0].mLft, mBorderSizes[0].mTop); glVertex2f(pLft, pTop);
            glTexCoord2f(mBorderSizes[0].mRgt, mBorderSizes[0].mTop); glVertex2f(cLMd, pTop);
            glTexCoord2f(mBorderSizes[0].mRgt, mBorderSizes[0].mBot); glVertex2f(cLMd, cTMd);
            glTexCoord2f(mBorderSizes[0].mLft, mBorderSizes[0].mBot); glVertex2f(pLft, cTMd);
        }

        //--Top Bar.
        if(pFlags & 0x0002)
        {
            glTexCoord2f(mBorderSizes[1].mLft, mBorderSizes[1].mTop); glVertex2f(cLMd, pTop);
            glTexCoord2f(mBorderSizes[1].mRgt, mBorderSizes[1].mTop); glVertex2f(cRMd, pTop);
            glTexCoord2f(mBorderSizes[1].mRgt, mBorderSizes[1].mBot); glVertex2f(cRMd, cTMd);
            glTexCoord2f(mBorderSizes[1].mLft, mBorderSizes[1].mBot); glVertex2f(cLMd, cTMd);
        }

        //--TR Corner
        if(pFlags & 0x0004)
        {
            glTexCoord2f(mBorderSizes[2].mLft, mBorderSizes[2].mTop); glVertex2f(cRMd, pTop);
            glTexCoord2f(mBorderSizes[2].mRgt, mBorderSizes[2].mTop); glVertex2f(pRgt, pTop);
            glTexCoord2f(mBorderSizes[2].mRgt, mBorderSizes[2].mBot); glVertex2f(pRgt, cTMd);
            glTexCoord2f(mBorderSizes[2].mLft, mBorderSizes[2].mBot); glVertex2f(cRMd, cTMd);
        }

        //--Mid-left Bar.
        if(pFlags & 0x0008)
        {
            glTexCoord2f(mBorderSizes[3].mLft, mBorderSizes[3].mTop); glVertex2f(pLft, cTMd);
            glTexCoord2f(mBorderSizes[3].mRgt, mBorderSizes[3].mTop); glVertex2f(cLMd, cTMd);
            glTexCoord2f(mBorderSizes[3].mRgt, mBorderSizes[3].mBot); glVertex2f(cLMd, cBMd);
            glTexCoord2f(mBorderSizes[3].mLft, mBorderSizes[3].mBot); glVertex2f(pLft, cBMd);
        }

        //--Middle. does nothing.
        if(pFlags & 0x0010)
        {
        }

        //--Mid-right bar.
        if(pFlags & 0x0020)
        {
            glTexCoord2f(mBorderSizes[5].mLft, mBorderSizes[5].mTop); glVertex2f(cRMd, cTMd);
            glTexCoord2f(mBorderSizes[5].mRgt, mBorderSizes[5].mTop); glVertex2f(pRgt, cTMd);
            glTexCoord2f(mBorderSizes[5].mRgt, mBorderSizes[5].mBot); glVertex2f(pRgt, cBMd);
            glTexCoord2f(mBorderSizes[5].mLft, mBorderSizes[5].mBot); glVertex2f(cRMd, cBMd);
        }

        //--BL Corner.
        if(pFlags & 0x0040)
        {
            glTexCoord2f(mBorderSizes[6].mLft, mBorderSizes[6].mTop); glVertex2f(pLft, cBMd);
            glTexCoord2f(mBorderSizes[6].mRgt, mBorderSizes[6].mTop); glVertex2f(cLMd, cBMd);
            glTexCoord2f(mBorderSizes[6].mRgt, mBorderSizes[6].mBot); glVertex2f(cLMd, pBot);
            glTexCoord2f(mBorderSizes[6].mLft, mBorderSizes[6].mBot); glVertex2f(pLft, pBot);
        }

        //--Bottom Bar.
        if(pFlags & 0x0080)
        {
            glTexCoord2f(mBorderSizes[7].mLft, mBorderSizes[7].mTop); glVertex2f(cLMd, cBMd);
            glTexCoord2f(mBorderSizes[7].mRgt, mBorderSizes[7].mTop); glVertex2f(cRMd, cBMd);
            glTexCoord2f(mBorderSizes[7].mRgt, mBorderSizes[7].mBot); glVertex2f(cRMd, pBot);
            glTexCoord2f(mBorderSizes[7].mLft, mBorderSizes[7].mBot); glVertex2f(cLMd, pBot);
        }

        //--BR Corner
        if(pFlags & 0x0100)
        {
            glTexCoord2f(mBorderSizes[8].mLft, mBorderSizes[8].mTop); glVertex2f(cRMd, cBMd);
            glTexCoord2f(mBorderSizes[8].mRgt, mBorderSizes[8].mTop); glVertex2f(pRgt, cBMd);
            glTexCoord2f(mBorderSizes[8].mRgt, mBorderSizes[8].mBot); glVertex2f(pRgt, pBot);
            glTexCoord2f(mBorderSizes[8].mLft, mBorderSizes[8].mBot); glVertex2f(cRMd, pBot);
        }
    glEnd();

    //--If the middle is flagged, print a transparent background.
    if(pFlags & 0x0010)
    {
        //--Alpha is determined by the Alt-UI flag. The Alt-UI uses a lot more screen space, so
        //  becomes more transparent.
        pBackingColor.SetAsMixer();

        //--Render.
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glVertex2f(cLMd-2.0f, cTMd-2.0f);
            glVertex2f(cRMd+2.0f, cTMd-2.0f);
            glVertex2f(cRMd+2.0f, cBMd+2.0f);
            glVertex2f(cLMd-2.0f, cBMd+2.0f);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
    }
}

//--[Public Statics]
StarlightColor VisualLevel::xTextColor = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

//--Static call, renders a button with text on it. This is a border and some bitmap font.
void VisualLevel::RenderTextButton(StarBitmap *pBorderCard, StarFont *pRenderFont, TwoDimensionReal pDimensions, float pIndent, const char *pText)
{
    //--Render the border card. This is done by another routine.
    RenderBorderCardOver(pBorderCard, pDimensions, 0x01EF);

    //--Render the text if it exists.
    if(!pRenderFont || !pText) return;
    glColor4f(xTextColor.r, xTextColor.g, xTextColor.b, xTextColor.a);
    pRenderFont->DrawText(pDimensions.mLft + pIndent, pDimensions.mTop + pIndent, pText);
    glColor3f(1.0f, 1.0f, 1.0f);
}
