//--[StarlightTextureLoader]
//--Notes

#pragma once

//--[Includes]
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"
#include "spine/TextureLoader.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class StarlightTextureLoader : public spine::TextureLoader
{
    private:

    protected:

    public:
    //--System
    StarlightTextureLoader();
    virtual ~StarlightTextureLoader();
    virtual void load(spine::AtlasPage &page, const spine::String &path);
    virtual void unload(void* texture);
};

//--Hooking Functions

