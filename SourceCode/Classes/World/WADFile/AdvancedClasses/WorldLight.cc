//--Base
#include "WorldLight.h"

//--Classes
#include "WorldPolygon.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//#define WL_DEBUG

///========================================== System ==============================================
WorldLight::WorldLight()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_WORLDLIGHT;

    ///--[WorldLight]
    //--System
    //--Position
    mPosition.Set(0.0f, 0.0f, 0.0f);

    //--Properties
    mIntensity = 0.0f;

    //--Shadow Lookups
    mShadowCasterList = new StarLinkedList(false);
}
WorldLight::~WorldLight()
{
    delete mShadowCasterList;
}

///--[Public Statics]
//--Flag, blocks shadow computations. Useful in debug to speed up lighting when you don't care
//  about how purdy everything looks.
bool WorldLight::xSkipShadows = false;

//--Once the distance to a given luxel is computed, if the intensity is less than this amount it
//  is not added (and shadow computations are skipped). This is an speed-hack. Set to 0.0f and the
//  optimization never occurs.
float WorldLight::cxMinIntensity = 2.0f;

//--When deciding which faces should compute shadows, any face that has an intensity lower than this
//  amount is considered to be too far away to cast a shadow. This reduces the number of objects that
//  need shadow computations. Setting this value to -1 means no faces are skipped.
float WorldLight::cxShadowThreshold = 5.0f;

//--Because light intensity falls off geometrically, the light intensity values tend to get unreal
//  pretty quickly. This scaler is applied to make the values a bit more readable by humans.
float WorldLight::cxLightDistanceScaler = 10000.0f;

//--In the real world, light bounces around rooms and also exhibits wave-diffraction, causing it to
//  "round" a corner even if it doesn't have line-of-sight on that face. Under these circumstances,
//  the shadow still exists but becomes considerably less "hard". This factor increases the distance
//  to the light by multiplication, and then allows the intensity to be applied anyway. This softens
//  shadows at all ranges, but more so up close.
//--Set to zero to make all shadows perfectly hard. Negative values will cause the intensity to be
//  divided by the absolute value of the scaler, which produces shadows that look very different.
float WorldLight::cxShadowDistanceScaler = 3.5f;

//--As above, light bounces around rooms and exhibits wave-diffraction. Faces that are facing away
//  from a light tend to still be lit by it due to this bouncing, but less. This factor determines
//  how much a backfacing face has multiplied onto its effective distance.
//--Set this to 0.0f and a backfacing face will always be skipped. Negatives are ignored.
float WorldLight::cxBackfaceDistanceScaler = 3.5f;

///===================================== Property Queries =========================================
Point3D WorldLight::GetPosition()
{
    return mPosition;
}
float WorldLight::GetIntensity()
{
    return mIntensity;
}
float WorldLight::GetIntensityByDistance(float pDistance)
{
    return mIntensity * cxLightDistanceScaler / (pDistance * pDistance);
}
float WorldLight::GetIntensityAtPoint(float pX, float pY, float pZ, float pScaler)
{
    //--Inlined for speed reasons. The distance is multiplied by the scaler, which can be 1.0f.
    float tDistance = Get3DDistance(mPosition, pX, pY, pZ) * pScaler;
    return mIntensity * cxLightDistanceScaler / (tDistance * tDistance);
}
float WorldLight::GetIntensityAtPoint(Point3D pPoint, float pScaler)
{
    //--Inlined for speed reasons.
    float tDistance = Get3DDistance(mPosition, pPoint) * pScaler;
    return mIntensity * cxLightDistanceScaler / (tDistance * tDistance);
}

///======================================= Manipulators ===========================================
void WorldLight::SetPosition(float pX, float pY, float pZ)
{
    mPosition.Set(pX, pY, pZ);
}
void WorldLight::SetIntensity(float pIntensity)
{
    if(pIntensity < 0.0f) pIntensity = 0.0f;
    mIntensity = pIntensity;
}

///======================================= Core Methods ===========================================
void WorldLight::StoreShadowRefs(StarLinkedList *pFaceList)
{
    //--Stores references to external WorldPolygons that are close enough to the object in question.
    //  When computing shadows, only objects that are close enough should cast a shadow, since
    //  far away objects might cast a shadow, but it'd likely be too small to see.
    mShadowCasterList->ClearList();
    if(!pFaceList) return;

    //--Go through every WorldPolygon present. If any of the points on that polygon are close enough
    //  to our position, store that polygon.
    //--We don't subdivide here, so if a polygon is gigantic it might theoretically be close enough
    //  to cast a shadow but have no points that are close to us. In these cases, the mapper should
    //  subdivide the polygon, or the engine, but not us.
    WorldPolygon *rPolygon = (WorldPolygon *)pFaceList->PushIterator();
    while(rPolygon)
    {
        //--The polygon must be "facing" this point, which is determined by its Normal vector.
        float tDistanceToMiddle = Get3DDistance(mPosition, rPolygon->mAverage);
        float tDistanceToNormal = Get3DDistance(mPosition, rPolygon->mAverage.mX + rPolygon->mNormal.mX, rPolygon->mAverage.mY + rPolygon->mNormal.mY, rPolygon->mAverage.mZ + rPolygon->mNormal.mZ);
        if(tDistanceToMiddle >= tDistanceToNormal)
        {

        }
        //--Polygon is facing us, so check the distance.
        else
        {
            //--We already have the distance to the average point. If the intensity at that distance is too low,
            //  this object will not cast a shadow.
            float tIntensityAtDistance = GetIntensityByDistance(tDistanceToMiddle);
            if(cxShadowThreshold != -1.0f && tIntensityAtDistance < cxShadowThreshold)
            {

            }
            //--Object is close enough to cast a shadow.
            else
            {
                mShadowCasterList->AddElement("X", rPolygon);
            }
        }

        //--Next polygon.
        rPolygon = (WorldPolygon *)pFaceList->AutoIterate();
    }

    //--Report.
    #ifdef WL_DEBUG
        DebugManager::ForcePrint("World Light stored %i shadow-casting faces.\n", mShadowCasterList->GetListSize());
    #endif
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *WorldLight::GetShadowCasterList()
{
    return mShadowCasterList;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
