-- |[ ================================== fnResolveDirectory() ================================== ]|
--Function that resolves the directory name of the currently executing script.
-- If a script is in C:/Dogs/Cats/Script.lua the result will be "Cats"
function fnResolveDirectory()
	
    -- |[Setup]|
    --Variables.
    local bFoundSlash = false
	local sStartPath = LM_GetCallStack(0)
    local iLen = string.len(sStartPath)
	local iBegin = 1
	local iEnd = 1

    -- |[First Slash]|
	--Determine where the last slash is.
	local sLetter = ""
	for i = iLen, 1, -1 do
		sLetter = string.sub(sStartPath, i, i)
		if(sLetter == "/" or sLetter == "\\") then
            bFoundSlash = true
			iEnd = i
			break
		end
	end

    --If no slash was found, return "Null".
    if(bFoundSlash == false) then return "Null" end
    
    -- |[Second Slash]|
    --Now keep going back and find the next slash.
    bFoundSlash = false
	for i = iEnd-1, 1, -1 do
		sLetter = string.sub(sStartPath, i, i)
		if(sLetter == "/" or sLetter == "\\") then
            bFoundSlash = true
			iBegin = i
			break
		end
	end

    --If no slash was found, return "Null". Again.
    if(bFoundSlash == false) then return "Null" end

    -- |[Assembly]|
    --Get the name and return it.
	local sFinishPath = string.sub(sStartPath, iBegin+1, iEnd-1)

	return sFinishPath
end