///===================================== AudioPackageSample ========================================
//--An AudioPackage that is explicitly a Stream type. Streams are nominally loaded from the hard drive
//  when they are played. Bass allows certain effects and properties to only affect streams, so be sure
//  not to make something a sample if it needs these effects.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "AudioPackage.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AudioPackageSample : public AudioPackage
{
    private:
    protected:

    public:
    //--System
    AudioPackageSample();
    virtual ~AudioPackageSample();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    virtual bool IsPlaying();
    virtual float GetPosition();
    virtual float GetDuration();

    //--Manipulators
    virtual void Load(const char *pPath);
    virtual void Play();
    virtual void FadeIn(int pTicks);
    virtual void FadeOut(int pTicks);
    virtual void FadeOutToStop(int pTicks);
    virtual void Pause();
    virtual void Stop();
    virtual void SetVolume(float pVolume);
    virtual void SetGlobalVolume(float pVolume);
    virtual void SlideVolume(float pVolume, int pTicks);
    virtual void SeekTo(float pSeconds);
    virtual void SetTempo(float pTempo);

    //--Core Methods
    virtual void AddLoop(float pStartLoop, float pEndLoop);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
