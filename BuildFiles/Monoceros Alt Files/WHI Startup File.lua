-- |[ ======================================== Startup ========================================= ]|
--Script that executes during program startup.  Load all resources here.
Debug_PushPrint(false, "Executing Startup.lua\n")

-- |[Version Setting]|
--Set version string.
Debug_SetVersion("Starlight Eng v3.2 Adv v4.33")

--Witch Hunter Izana's Version
MT_SetProperty("Set Version", "Witch Hunter Izana v1.25")

--Used to check Lua version if you need to.
--io.write("Lua reports it is ",_VERSION,"!\n")

-- |[Diagnostic Flags]|
--If true, all deprecated function calls will bark a warning. Useful for updating a mod or old
-- code that might use deprecated calls. In addition, you can search for instances where it is
-- used to find deprecated code and remove it after modders have had time to update.
gbPrintDeprecatedCallWarnings = false

-- |[Title Screen Behavior]|
--This flag mandates the use of a specific title screen. If set to "Null", the game selector
-- menu will appear. Game selection is considered Adventure Mode's "title screen".
gsMandateTitle = "Witch Hunter Izana"

--Other accepted title mandates.
--gsMandateTitle = "Null"
--gsMandateTitle = "Witch Hunter Izana"
--gsMandateTitle = "Witches of Ravenbrook"
--gsMandateTitle = "String Tyrant"
--gsMandateTitle = "String Tyrant Demo"

--In "One Game" mode, pressing exit on the title screen quits the game immediately. Otherwise it jumps
-- back to the game selection screen. This flag is used for games designed to have standalone title
-- screens like String Tyrant.
gbIsOneGameMode = false

--Flag that tracks if the intro has been displayed. It should only appear once.
gbHasRunIntro = false

-- |[Translations and Localizations]|
--Call this file to build a list of what translations exist, and if needed, activates a translation
-- set by a previous program execution.
giTranslationCount = 0
LM_ExecuteScript("Data/Translations/Translation Builder.lua")

-- |[Global Lua Functions and Variables]|
Debug_Print("Executing System and Pathing scripts.\n")
LM_ExecuteScript("Data/Scripts/Functions/000 Global Lua Variables.lua")
LM_ExecuteScript("Data/Scripts/Functions/001 Basic Math.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnAddGameEntry.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnAddGamePath.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnAppendTables.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnCheckAutorun.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnCompleteLoadSequence.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnCopy.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnExecAll.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnExecAutoload.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnGameExists.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnGetGameIndex.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnGetStringAfterLastSlash.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnIssueLoadReset.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnMarkAutorunAsFound.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnResolveDirectory.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnResolveFolderName.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnResolveName.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnResolvePath.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnResolvePathFrom.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnScanPaths.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnScanMods.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnSetCutsceneDiagnostics.lua")
LM_ExecuteScript("Data/Scripts/Functions/fnTrimLastDirectory.lua")
LM_ExecuteScript("Data/Scripts/Functions/io.printf.lua")
local sBasePath = fnResolvePath()

-- |[ ===================================== Engine Classes ===================================== ]|
--Execute these files to build class objects in Lua.
LM_ExecuteScript("Data/Classes/ZClassRouting.lua")

-- |[ ================================== Diagnostic Variables ================================== ]|
--SysPath Flags
Debug_RegisterFlag("SysPaths: All", false)
Debug_RegisterFlag("SysPaths: Scanning", false)
Debug_RegisterFlag("SysPaths: Activation", false)
Debug_RegisterFlag("SysPaths: Populate Main Menu", false)

-- |[Auto-Checker Function]|
--Assuming an entry is formatted in format "NameOfThing: SubEntry" for diagnostic flags, checks if
-- the entry "NameOfThing: All" is true. If it is, returns the all flag, otherwise returns true if the
-- subflag is true.
function fnAutoCheckDebugFlag(psFlag)

    -- |[Argument Check]|
    if(psFlag == nil) then return false end
    
    -- |[All Check]|
    --Check for the location of the colon.
    local iColonLocation = string.find(psFlag, ":")
    if(iColonLocation ~= nil) then
        local sAllFlag = string.sub(psFlag, 1, iColonLocation-1) .. ": All"
        if(Debug_GetFlag(sAllFlag) == true) then return true end
    end
    
    -- |[Flag Check]|
    return Debug_GetFlag(psFlag)
end

-- |[ ==================================== Engine Constants ==================================== ]|
--Math Constants
cfPi = 3.1415926
cfToRadians = (cfPi)  / (180.0) --Converts from Degrees to Radians
cfToDegrees = (180.0) / (cfPi)  --Converts from Radians to Degrees

-- |[ ================================= Path and Game Handling ================================= ]|
--Now that functions and classes are built, scan the directories around the engine looking for games
-- it can play and storing their paths.
Debug_Print("Building path listings.\n")
SysPaths:fnScanDirectories()

--Build Script Hunter paths. These are used for diagnostics.
Debug_Print("Building Script Hunter paths.\n")
SysPaths:fnBuildScriptHunterPaths()

--Now set active paths. This is done by ordering all games to check if they actually have the
-- requisite files to be played. This isn't actually needed yet (the main menu re-calls this)
-- but is instead used for one-game mode setting or overrides.
DL_AddPath("Root/Paths/System/Startup/")
SysPaths:fnActivateAllGames()

--Set suppression flag.
LI_SetSuppression(false)

-- |[ ============================= Autoboot Load Interrupt Reboot ============================= ]|
--If String Tyrant is the mandated game, and it has a game entry, switch to using its title screen.
-- If it's mandated but has no entry, unset the mandate.
Debug_Print("Checking autoboot to String Tyrant.\n")
if(gsMandateTitle == "String Tyrant" or gsMandateTitle == "String Tyrant Demo") then
	
	--Check if String Tyrant, or the demo, are available.
	local zStringTyrantEntry     = SysPaths:fnGetGameEntry("Doll Manor")
	local zStringTyrantDemoEntry = SysPaths:fnGetGameEntry("Doll Manor Demo")
	
	--If the main entry is not found but the demo is, use that.
	local zUseEntry = zStringTyrantEntry
	if(zUseEntry == nil or zUseEntry.sActivePath == "Null") then zUseEntry = zStringTyrantDemoEntry end
	
	--If both came back nil, unset the mandate.
	if(zUseEntry == nil or zUseEntry.sActivePath == "Null") then
		gsMandateTitle = "Null"
	
	--One of them was available, switch to the String Tyrant title screen.
	else
    
		--Mark the loader so it does not re-boot in the launcher.
		gbHasBootedLoader = true
		gbIsOneGameMode = true
    
		--Get path.
		local sPath = string.sub(zUseEntry.sActivePath, 1, zUseEntry.iNegativeLen-1)
	
		--Reboot the loader.
		LI_BootStringTyrant(sPath .. "Datafiles/STLoad.slf")
		--if(bOnlyStringTyrant) then
			LI_Reset(36 + 316)
		--else
		--	LI_Reset(36 + 217)
		--end
		
		--Flag.
		MapM_SetOneGame(true)
	end
end

-- |[ ====================================== Mod Checking ====================================== ]|
LM_ExecuteScript("Data/Scripts/110 Mod Paths.lua")

-- |[ ==================================== Resource Loading ==================================== ]|
-- |[Graphics Loading]|
--Setup.
Debug_Print("Loading boot graphics.\n")
LM_ExecuteScript("Data/Scripts/200 Graphics.lua")

-- |[Audio Loading]|
Debug_Print("Loading audio.\n")
LM_SetSilenceFlag(true)
	LM_ExecuteScript("Data/Audio/ZRouting.lua")
LM_SetSilenceFlag(false)

-- |[Control Manager Loading]|
Debug_Print("Loading control images.\n")
LM_SetSilenceFlag(true)
	LM_ExecuteScript("Data/Scripts/201 Control Manager UI.lua")
LM_SetSilenceFlag(false)

-- |[Debug]|
Debug_PopPrint("Completed Startup.lua\n")

-- |[ ======================================= Finish Up ======================================== ]|
--Tell the load interrupt we're done loading.
LI_Finish()
