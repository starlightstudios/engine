//--Base
#include "AdvUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarPointerSeries.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Debug]
//#define ADVUIEQUIP_DEBUG
#ifdef ADVUIEQUIP_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
AdvUIEquip::AdvUIEquip()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AEQP");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIEquip ======= ]
    ///--[Constants]
    cReplaceEntriesPerPage = 20;

    ///--[Selection]
    //--Cursor Integers.
    mCharacterCursor = 0;
    mEquipmentCursor = 0;
    mGemCursor = -1;

    //--Cursor Positions
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Other timers.
    mArrowTimer = 0;

    ///--[Character Swap]
    mIsSwappingCharacter = false;
    mCharacterSwapTimer = 0;
    mPrevCharacterSlot = 0;

    ///--[Replacement]
    mIsReplacing = false;
    mReplacementTimer = 0;
    mReplacementFlashTimer = 0;
    mReplacementSkip = 0;
    mReplacementCursor = 0;
    mValidReplacementsList = new StarLinkedList(false);

    ///--[Details]
    mIsShowingDetails = false;
    mDetailsTimer = 0;
    rDetailsItem = NULL;

    ///--[Statistic Lookups]
    //--Build statistic lookups.
    mStatisticIndexes[ 0] = STATS_HPMAX;
    mStatisticIndexes[ 1] = STATS_ATTACK;
    mStatisticIndexes[ 2] = STATS_ACCURACY;
    mStatisticIndexes[ 3] = STATS_EVADE;
    mStatisticIndexes[ 4] = STATS_INITIATIVE;
    mStatisticIndexes[ 5] = STATS_RESIST_PROTECTION;
    mStatisticIndexes[ 6] = STATS_RESIST_SLASH;
    mStatisticIndexes[ 7] = STATS_RESIST_STRIKE;
    mStatisticIndexes[ 8] = STATS_RESIST_PIERCE;
    mStatisticIndexes[ 9] = STATS_RESIST_FLAME;
    mStatisticIndexes[10] = STATS_RESIST_FREEZE;
    mStatisticIndexes[11] = STATS_RESIST_SHOCK;
    mStatisticIndexes[12] = STATS_RESIST_CRUSADE;
    mStatisticIndexes[13] = STATS_RESIST_OBSCURE;
    mStatisticIndexes[14] = STATS_RESIST_BLEED;
    mStatisticIndexes[15] = STATS_RESIST_POISON;
    mStatisticIndexes[16] = STATS_RESIST_CORRODE;
    mStatisticIndexes[17] = STATS_RESIST_TERRIFY;

    //--Bonus Tag Names
    strcpy(mBonusTagNames[ 0], "Null");
    strcpy(mBonusTagNames[ 1], "Null");
    strcpy(mBonusTagNames[ 2], "Null");
    strcpy(mBonusTagNames[ 3], "Null");
    strcpy(mBonusTagNames[ 4], "Null");
    strcpy(mBonusTagNames[ 5], "Null");
    strcpy(mBonusTagNames[ 6], "Slash Damage Dealt +");
    strcpy(mBonusTagNames[ 7], "Strike Damage Dealt +");
    strcpy(mBonusTagNames[ 8], "Pierce Damage Dealt +");
    strcpy(mBonusTagNames[ 9], "Flame Damage Dealt +");
    strcpy(mBonusTagNames[10], "Freeze Damage Dealt +");
    strcpy(mBonusTagNames[11], "Shock Damage Dealt +");
    strcpy(mBonusTagNames[12], "Holy Damage Dealt +");
    strcpy(mBonusTagNames[13], "Shadow Damage Dealt +");
    strcpy(mBonusTagNames[14], "Bleed Damage Dealt +");
    strcpy(mBonusTagNames[15], "Poison Damage Dealt +");
    strcpy(mBonusTagNames[16], "Corrode Damage Dealt +");
    strcpy(mBonusTagNames[17], "Terrify Damage Dealt +");

    //--Malus Tag Names
    strcpy(mMalusTagNames[ 0], "Null");
    strcpy(mMalusTagNames[ 1], "Null");
    strcpy(mMalusTagNames[ 2], "Null");
    strcpy(mMalusTagNames[ 3], "Null");
    strcpy(mMalusTagNames[ 4], "Null");
    strcpy(mMalusTagNames[ 5], "Null");
    strcpy(mMalusTagNames[ 6], "Slash Damage Dealt -");
    strcpy(mMalusTagNames[ 7], "Strike Damage Dealt -");
    strcpy(mMalusTagNames[ 8], "Pierce Damage Dealt -");
    strcpy(mMalusTagNames[ 9], "Flame Damage Dealt -");
    strcpy(mMalusTagNames[10], "Freeze Damage Dealt -");
    strcpy(mMalusTagNames[11], "Shock Damage Dealt -");
    strcpy(mMalusTagNames[12], "Holy Damage Dealt -");
    strcpy(mMalusTagNames[13], "Shadow Damage Dealt -");
    strcpy(mMalusTagNames[14], "Bleed Damage Dealt -");
    strcpy(mMalusTagNames[15], "Poison Damage Dealt -");
    strcpy(mMalusTagNames[16], "Corrode Damage Dealt -");
    strcpy(mMalusTagNames[17], "Terrify Damage Dealt -");

    ///--[Rendering Constants]
    cSlotNameX       = 482.0f;
    cSlotNameY       = 192.0f;
    cSlotH           =  56.0f;
    cEquipIconX      = 486.0f;
    cEquipIconY      = 214.0f;
    cEquipNameX      = 507.0f;
    cEquipNameY      = 214.0f;
    cGemIconX        = 767.0f;
    cGemIconY        = 212.0f;
    cGemIconW        =  24.0f;
    cReplaceStartY   = 241.0f;
    cReplaceStartH   =  22.0f;
    cReplaceDividerH = 438.0f;

    ///--[Help Strings]
    //--Control Strings
    mShowHelpString = new StarlightString();
    mInspectString = new StarlightString();
    mScrollFastString = new StarlightString();
    mCharacterLftString = new StarlightString();
    mCharacterRgtString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Strings
    AllocateHelpStrings(cHelpStringsTotal);

    //--Add the AdvImages structure to the verify list.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIEquip::~AdvUIEquip()
{
    delete mValidReplacementsList;
    delete mShowHelpString;
    delete mInspectString;
    delete mScrollFastString;
    delete mCharacterLftString;
    delete mCharacterRgtString;
}
void AdvUIEquip::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Equipment UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",      &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
bool AdvUIEquip::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    return false;
}

///======================================= Manipulators ===========================================
void AdvUIEquip::TakeForeground()
{
    ///--[Documentation]
    //--Called when this object is shown.

    ///--[Variables]
    //--Reset cursors.
    mCharacterCursor = 0;
    mEquipmentCursor = 0;
    mGemCursor = -1;

    //--Submode flags.
    mIsReplacing = false;
    mReplacementTimer = 0;
    mReplacementCursor = 0;
    mIsShowingDetails = false;
    mDetailsTimer = 0;

    ///--[Construction]
    //--Set cursor to 0th position, instantly complete to snap the cursor to position.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--Control strings.
    mShowHelpString->    AutoSetControlString("[IMG0] Show Help",                           1, "F1");
    mInspectString->     AutoSetControlString("[IMG0] Item Details",                        1, "F2");
    mScrollFastString->  AutoSetControlString("[IMG0] (Hold) Scroll x10",                   1, "Ctrl");
    mCharacterLftString->AutoSetControlString("[IMG0] or [IMG1]+[IMG2] Previous Character", 3, "UpLevel", "Ctrl", "Left");
    mCharacterRgtString->AutoSetControlString("[IMG0] or [IMG1]+[IMG2] Next Character",     3, "DnLevel", "Ctrl", "Right");

    ///--[Character Refresh]
    //--Get the character and refresh their stats.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(0);
    if(rActiveEntity) rActiveEntity->RefreshStatsForUI();

    ///--[Inventory Refresh]
    //--Order the inventory to sort internally.
    AdventureInventory::Fetch()->SortItemListByInternalCriteria();
}

///======================================= Core Methods ===========================================
void AdvUIEquip::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Set Lines]
    //--Common lines.
    SetHelpString(i, "This UI allows you to change your equipment. This allows you to increase your ");
    SetHelpString(i, "statistics, improve resistances, and use items in battle.");
    SetHelpString(i, "");
    SetHelpString(i, "Select an equipment slot to see a lit of items you can equip to it. You can also unequip");
    SetHelpString(i, "the item in that slot, except for weapons.");
    SetHelpString(i, "");
    SetHelpString(i, "To equip gems, select the empty gem slot with the arrow keys and activate it the same");
    SetHelpString(i, "way you equip other items.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "");

    //--Damage Icon legend.
    SetHelpString(i, "Icon Legend: Damage Types");
    mHelpMenuStrings[i]->SetString("[IMG0] Slashing [MOV200][IMG1] Striking [MOV400][IMG2] Piercing [MOV600][IMG3] Flaming [MOV800][IMG4] Freezing [MOV1000][IMG5] Shocking");
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Slashing"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Striking"));
    mHelpMenuStrings[i]->SetImageP(2, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Piercing"));
    mHelpMenuStrings[i]->SetImageP(3, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Flaming"));
    mHelpMenuStrings[i]->SetImageP(4, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Freezing"));
    mHelpMenuStrings[i]->SetImageP(5, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Shocking"));
    i ++;

    mHelpMenuStrings[i]->SetString("[IMG0] Crusading [MOV200][IMG1] Obscuring [MOV400][IMG2] Bleeding [MOV600][IMG3] Poisoning [MOV800][IMG4] Corroding [MOV1000][IMG5] Terrifying");
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Crusading"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Obscuring"));
    mHelpMenuStrings[i]->SetImageP(2, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Bleeding"));
    mHelpMenuStrings[i]->SetImageP(3, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Poisoning"));
    mHelpMenuStrings[i]->SetImageP(4, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Corroding"));
    mHelpMenuStrings[i]->SetImageP(5, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Terrifying"));
    i ++;
    SetHelpString(i, "");

    //--Statistic Icon legend.
    SetHelpString(i, "Icon Legend: Statistics");
    mHelpMenuStrings[i]->SetString("[IMG0] Health [MOV200][IMG1] Power [MOV400][IMG2] Accuracy [MOV600][IMG3] Evade [MOV800][IMG4] Initiative [MOV1000][IMG5] Protection");
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Health"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Attack"));
    mHelpMenuStrings[i]->SetImageP(2, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Accuracy"));
    mHelpMenuStrings[i]->SetImageP(3, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Evade"));
    mHelpMenuStrings[i]->SetImageP(4, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Initiative"));
    mHelpMenuStrings[i]->SetImageP(5, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Protection"));
    i ++;

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void AdvUIEquip::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Depending on what mode is currently active and where the cursor is, modifies the cursor's
    //  target position and size.

    //--Selecting Equipment slot or Gem slot
    if(!mIsReplacing)
    {
        //--Not a gem slot:
        if(mGemCursor == -1)
        {
            mHighlightPos.MoveTo(cSlotNameX - 4.0f, cSlotNameY + (mEquipmentCursor * cSlotH), cxAdvCurTicks);
            mHighlightSize.MoveTo(cGemIconX - cSlotNameX, cSlotH - 9.0f, cxAdvCurTicks);
        }
        //--Gem slot:
        else
        {
            mHighlightPos.MoveTo(cGemIconX + (cGemIconW * mGemCursor) - 4.0f, cGemIconY + (mEquipmentCursor * cSlotH) - 4.0f, cxAdvCurTicks);
            mHighlightSize.MoveTo(30.0f, 30.0f, cxAdvCurTicks);
        }
    }
    //--Replacing.
    else
    {
        //--Modify replacement cursor by offset.
        int tUseCursor = mReplacementCursor - mReplacementSkip;
        mHighlightPos.MoveTo(cSlotNameX, cReplaceStartY + (cReplaceStartH * tUseCursor), cxAdvCurTicks);
        mHighlightSize.MoveTo(cGemIconX - cSlotNameX, cReplaceStartH + 3.0f, cxAdvCurTicks);
    }
}

///=================================== Private Core Methods =======================================
void AdvUIEquip::BuildSlotList(int pCharacterSlot, const char *pSlotType)
{
    ///--[Documentation]
    //--Given a character slot and an equipment slot, creates a list of all items the player currently
    //  has which can be equipped in that slot.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Setup]
    //--Clear existing list.
    mValidReplacementsList->ClearList();

    //--Get the character.
    AdvCombatEntity *rEquipEntity = rAdvCombat->GetActiveMemberI(pCharacterSlot);
    if(!rEquipEntity) return;

    ///--[Normal Equipment]
    //--Pass the list to the AdventureInventory which will do the heavy lifting.
    if(mGemCursor == -1)
    {
        rInventory->BuildEquippableList(rEquipEntity, pSlotType, mValidReplacementsList);
        return;
    }

    ///--[Gem Replacement]
    //--For gem replacement, we order the inventory to build a list of gems not equipped to anyone,
    //  then retrive and copy that list.
    rInventory->BuildGemListNoEquip();
    StarLinkedList *rGemList = rInventory->GetGemList();

    //--Copy the list into the replacement list.
    void *rEntry = rGemList->PushIterator();
    while(rEntry)
    {
        mValidReplacementsList->AddElementAsTail("X", rEntry);
        rEntry = rGemList->AutoIterate();
    }
}

///========================================== Update ==============================================
bool AdvUIEquip::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--Arrow oscillation.
    mArrowTimer = (mArrowTimer + 1) % cxAdvOscTicks;

    //--Replacement timer.
    if(!mIsReplacing)
    {
        if(mReplacementTimer > 0) mReplacementTimer --;
    }
    else
    {
        mReplacementFlashTimer ++;
        if(mReplacementTimer < cReplaceTicks) mReplacementTimer ++;
    }

    //--Character swapping timer.
    if(mIsSwappingCharacter)
    {
        if(mCharacterSwapTimer < cxAdvCharSwapTicks)
        {
            mCharacterSwapTimer ++;
        }
        else
        {
            mIsSwappingCharacter = false;
        }
    }

    //--Standard timers.
    StandardTimer(mVisibilityTimer,     0, mVisibilityTimerMax, true);
    StandardTimer(mDetailsTimer,        0, cDetailTicks,        mIsShowingDetails);
    StandardTimer(mHelpVisibilityTimer, 0, cHlpTicks,           mIsShowingHelp);

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Update Blockers]
    //--If the replacement timer is not zeroed or capped, block.
    if(mReplacementTimer > 0 && mReplacementTimer < cReplaceTicks) return true;

    //--If the details timer is not zeroed or capped, block.
    if(mDetailsTimer > 0 && mDetailsTimer < cDetailTicks) return true;

    //--If characters are swapping, block.
    if(mIsSwappingCharacter) return true;

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[Mode: Selecting Equipment]
    //--When the player is selecting which piece of equipment or gem they want to change or inspect.
    if(!mIsReplacing)
    {
        UpdateSelection();
    }
    ///--[Mode: Replacing Equipment or Gems]
    //--When the player is selecting which piece of equipment or gem they want to swap.
    else
    {
        UpdateReplacement();
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}
void AdvUIEquip::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;
    mVisibilityTimer --;
}

///========================================= File I/O =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
