///========================================= FlexMenu =============================================
//--A Flexible Menu (duh) based off the StarMenu class. Stored in a stack by the Level, it
//  allows Lua to have a greater degree of control over the player's actions. Implements
//  rendering behaviors that the StarMenu does not have by default.

#pragma once

///========================================== Includes ============================================
#include "AdvCombat.h"
#include "Definitions.h"
#include "Structures.h"
#include "StarMenu.h"
#include "FileDef.h"
#include "SteamManager.h"

///===================================== Local Structures =========================================
///--[HeaderPack]
//--Contains a size and a string. Used when constructing lists of strings/buttons/etc.
typedef struct HeaderPack
{
    char *mText;
    float cFontSize;
}HeaderPack;

///--[FM_RebindGroup]
//--Represents a rebindable keycode from the options menu on the title screen.
typedef struct
{
    //--Members
    char *mLocalName;
    int mControlsTotal;
    char **mControlNames;
    char **mControlShow;
    int **mCurrentScancodes;
    TwoDimensionReal *mControlCoords;

    //--Functions
    void Initialize();
    void SetName(const char *pName);
    void Allocate(int pTotal);
    void SetControl(int pSlot, const char *pName, const char *pDisplay, TwoDimensionReal pPosition);
    static void DeallocateThis(void *pPtr);
}FM_RebindGroup;

///--[FM_ModInfo]
//--Represents a mod in the mod load order list.
typedef struct FM_ModInfo
{
    //--Members
    bool mIsEnabled;
    char *mPath;
    char *mInternalName;
    char *mDisplayName;

    //--Upload Data.
    bool mIsUploadReady;

    //--Function
    void Initialize()
    {
        //--Members.
        mPath = NULL;
        mInternalName = NULL;
        mDisplayName = NULL;

        //--Upload Data.
        mIsUploadReady = false;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        FM_ModInfo *rModInfo = (FM_ModInfo *)pPtr;
        free(rModInfo->mPath);
        free(rModInfo->mInternalName);
        free(rModInfo->mDisplayName);
        free(rModInfo);
    }
}FM_ModInfo;

///===================================== Local Definitions ========================================
//--Audio timing
#define FM_SILENCE_TICKS 15

//--Splash Screen States
#define FM_SPLASH_STATE_NONE 0
#define FM_SPLASH_STATE_FADEIN 1
#define FM_SPLASH_STATE_HOLD 2
#define FM_SPLASH_STATE_FADEOUT 3

//--Splash Screen Timers
#define FM_SPLASH_FRAMES_TOTAL 24
#define FM_SPLASH_TPF 4.0f
#define FM_SPLASH_FADEIN_TICKS 60
#define FM_SPLASH_HOLD_TICKS 180
#define FM_SPLASH_FADEOUT_TICKS 60

//--Text Sizing
#define FM_DESCRIPTION_LINES 8
#define FM_DESCRIPTION_CHARS 80

//--Loading Constants
#define FM_FILES_PER_PAGE 4

//--Cheat Codes
#define COUNT_TREASURES_WORD "glitteringprizes"
#define COUNT_TREASURES_LEN 16

//--Button Codes
#define FM_LOAD_NEWGAME 0
#define FM_LOAD_BACK -2
#define FM_LOAD_CONTROLS -3
#define FM_LOAD_SCROLLUP -4
#define FM_LOAD_SCROLLDN -5
#define FM_LOAD_TOGGLEAUTOSAVE -6

///========================================== Classes =============================================
class FlexMenu : public StarMenu
{
    protected:
    //--System
    int mCountTreasureProgress;
    bool mClearedSelfThisTick;
    bool mHasSortedRecently;

    //--Splash Screen
    bool mAccelerateSplashState;
    int mSplashState;
    int mSplashStateTimer;
    int mSplashTimer;
    StarBitmap *rSplashImg[FM_SPLASH_FRAMES_TOTAL];

    //--Selection
    int mSelectedOption;
    int mPreviousMouseX, mPreviousMouseY, mPreviousMouseZ;

    //--Introduction Mode
    bool mIsIntroMode;
    int mIntroTimer;

    //--Loading Mode
    bool mIsLoadingMode;
    int mLoadingTimer;
    int mLoadingCursor;
    int mLoadingOffset;
    bool mLoadingShowAutosaves;
    TwoDimensionReal mBackBtn; //Shared with Rebinding mode
    TwoDimensionReal mControlsBtn;
    TwoDimensionReal mScrollUp;
    TwoDimensionReal mScrollDn;
    TwoDimensionReal mToggleAutosave;
    StarLinkedList *mLoadingPackList; //LoadingPack *, master
    StarLinkedList *mAutosavePackList; //LoadingPack *, master
    bool mIsEnteringString;
    int mFileStringEntering;
    StringEntry *mStringEntryForm;

    //--Scrollbar Click
    bool mIsClickingScrollbar;
    float mScrollbarClickStartY;

    //--Rebinding Mode
    bool mIsRebindingMode;
    int mRebindingCursor;
    bool mIsRebindSecondary;
    int mCurrentRebindKey;
    int mActiveRebindPack;
    int mRebindPacksTotal;
    FM_RebindGroup *mRebindPacks;
    TwoDimensionReal mRebindPackCurButton;
    TwoDimensionReal mRebindPackLftButton;
    TwoDimensionReal mRebindPackRgtButton;
    TwoDimensionReal mSaveBtn;
    TwoDimensionReal mDefaultsBtn;

    //--Header
    bool mHasHeader;
    int mHeaderLinesTotal;
    HeaderPack *mHeaderLines;
    float mHeaderWid, mHeaderHei;
    float cHeaderPadding;

    //--Descriptions
    int mLinesOccupied;
    bool mShowDescriptions;
    void *rLastHighlightedBtn;
    StarBitmap *rDescriptionImg;
    TwoDimensionReal mDescriptionDim;
    TwoDimensionReal mDescriptionImgDim;
    char mDescriptionLines[FM_DESCRIPTION_LINES][FM_DESCRIPTION_CHARS];

    //--Rendering Constants
    bool mCanRender;
    float cBorder;
    float cButtonIndentX, cButtonIndentY;
    float cFontSize;
    float cStepRate;

    //--Rendering Variables
    StarFont *rOverrideFont;

    //--Location
    TwoDimensionReal mCoordinates;

    //--Storage
    StarLinkedList *mButtonStorage;

    //--Mod Load Order
    StarLinkedList *mModLoadOrderList; //FM_ModInfo *, master

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts.
            StarFont *rUIFont;

            //--Images.
            StarBitmap *rBorderCard;
            StarBitmap *rUpArrow;
            StarBitmap *rDnArrow;
            StarBitmap *rChapterComplete[LOADINGPACK_MAX_CHAPTER];
        }Data;
    }Images;

    protected:

    public:
    //--System
    FlexMenu();
    virtual ~FlexMenu();
    virtual void Construct();
    void CountTreasures();

    //--Public Variables
    static bool xShowSplash;
    static bool xUseDummySprites;
    static bool xUseClassicUI;
    static int xNoHighlightTimer;
    static bool xNeedsToLoadControls;
    static bool xObjectScanDebug;
    static FILE *xfDebugFileOut;

    //--Property Queries
    virtual bool IsOfType(int pType);
    StarFont *ResolveFont();
    bool DoesButtonExist(const char *pButtonName);

    //--Manipulators
    virtual void Clear();
    void SetDescriptionFlag(bool pFlag);
    void ResetHeader(int pHeaderLines);
    void SetHeaderLine(int pIndex, const char *pString, float pFontSize);
    virtual void RegisterButton(StarButton *pButton);
    void ActivateIntroMode();

    //--Core Methods
    void RefreshDescriptionFrom(FlexButton *pButton);

    ///--[Loading]
    void ActivateLoadingMode();
    void DeactivateLoadingMode();
    void CrossloadSaveImages(LoadingPack *pPack);
    virtual void UpdateLoadingMode();
    virtual void RenderLoadingMode();

    ///--[Mods]
    void RegisterMod(const char *pInternal, const char *pDisplay, bool pIsEnabled);
    void SetPathOfMod(const char *pInternal, const char *pPath);
    void UploadModToWorkshop(const char *pInternalName);
    void ClearModList();
    void ReadLoadOrderFromFile(const char *pFilePath);
    void WriteLoadOrderToFile(const char *pFilePath);

    ///--[Rebinding]
    bool IsRebindingMode();
    void ActivateRebindingMode();
    void DeactivateRebindingMode(bool pSave);
    void DeactivateRebindingModeDefaults();
    void UpdateRebindingMode();
    void RenderRebindingMode();

    ///--[Splash Screen]
    bool IsSplashMode();
    void UpdateSplashMode();
    void RenderSplashMode();

    protected:
    //--Private Core Methods
    virtual void AutoSortButtons();
    static int CompareButtonPriorities(const void *pEntryA, const void *pEntryB);
    bool HandleCheatCode(const char *pPressedKey, int &sProgressVar, int pMaxLength, const char *pCheatCode);

    public:
    //--Update
    virtual void Update();
    void UpdateInactive();

    //--File I/O
    //--Drawing
    virtual void Render();

    //--Pointer Routing
    StarLinkedList *GetButtonList();
    StarButton *GetButtonI(int pIndex);
    StarButton *GetButtonS(const char *pName);

    //--Static Functions
    static FlexMenu *GenerateFlexMenu();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_FlexMenu_Open(lua_State *L);
int Hook_FlexMenu_FlagClose(lua_State *L);
int Hook_FlexMenu_Clear(lua_State *L);
int Hook_FlexMenu_RegisterButton(lua_State *L);
int Hook_FlexMenu_DoesButtonExist(lua_State *L);
int Hook_FlexMenu_PushButton(lua_State *L);
int Hook_FlexMenu_SetHeaderSize(lua_State *L);
int Hook_FlexMenu_SetHeaderLine(lua_State *L);
int Hook_FlexMenu_ActivateIntro(lua_State *L);
int Hook_FlexMenu_ActivateLoading(lua_State *L);
int Hook_FlexMenu_SetDummySpriteFlag(lua_State *L);
int Hook_FlexMenu_GetProperty(lua_State *L);
int Hook_FlexMenu_SetProperty(lua_State *L);
