//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///==================================== Collision Functions =======================================
//--These four functions check if any collision along the given edge is true. They use the actor's
//  local positions, and are specific to the actor.
bool TilemapActor::IsLftClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX - 1.0f;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 0; y < TA_SIZE; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, (cTop + y), 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int y = 0; y < TA_SIZE; y ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft, (cTop + y)))
            {
                return true;
            }
        }
    }

    return false;
}
bool TilemapActor::IsTopClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY - 1.0f;

    //--Iterate.
    for(int x = 0; x < TA_SIZE; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int x = 0; x < TA_SIZE; x ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft + x, cTop))
            {
                return true;
            }
        }
    }

    return false;
}
bool TilemapActor::IsRgtClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX + TA_SIZE;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 0; y < TA_SIZE; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, cTop + y, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int y = 0; y < TA_SIZE; y ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft, cTop + y))
            {
                return true;
            }
        }
    }

    return false;
}
bool TilemapActor::IsBotClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY + TA_SIZE;

    //--Iterate.
    for(int x = 0; x < TA_SIZE; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int x = 0; x < TA_SIZE; x ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft + x, cTop))
            {
                return true;
            }
        }
    }

    return false;
}

///================================= Narrow Collision Functions ===================================
//--These are the same as their above equivalents but they do next check the edge of the edge (hah!), instead
//  narrowing the hitbox by 1 pixel on both sides. This is used for slope checks.
bool TilemapActor::IsLftClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX - 1.0f;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 1; y < TA_SIZE - 1; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, (cTop + y), 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}
bool TilemapActor::IsTopClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY - 1.0f;

    //--Iterate.
    for(int x = 1; x < TA_SIZE - 1; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}
bool TilemapActor::IsRgtClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX + TA_SIZE;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 1; y < TA_SIZE - 1; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, cTop + y, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}
bool TilemapActor::IsBotClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY + TA_SIZE;

    //--Iterate.
    for(int x = 1; x < TA_SIZE - 1; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}

///=============================== Abstract Collision Functions ===================================
//--"Abstract" collision functions that are static. They do not care about the existing entity, and
//  instead use values passed in to resolve if a theoretical entity could perform that move.
bool TilemapActor::AbsIsLftClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth)
{
    //--Error check.
    if(!pLevel) return false;

    //--Position.
    float cLft = pLft - 1.0f;
    float cTop = pTop;

    //--Iterate.
    for(int y = 0; y < TA_SIZE; y ++)
    {
        if(pLevel->GetClipAt(cLft, (cTop + y), 0.0f, pDepth))
        {
            return true;
        }
    }

    //--All checks passed.
    return false;
}
bool TilemapActor::AbsIsTopClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth)
{
    //--Error check.
    if(!pLevel) return false;

    //--Position.
    float cLft = pLft;
    float cTop = pTop - 1.0f;

    //--Iterate.
    for(int x = 0; x < TA_SIZE; x ++)
    {
        if(pLevel->GetClipAt(cLft + x, cTop, 0.0f, pDepth))
        {
            return true;
        }
    }

    //--All checks passed.
    return false;
}
bool TilemapActor::AbsIsRgtClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth)
{
    //--Error check.
    if(!pLevel) return false;

    //--Position.
    float cLft = pLft + TA_SIZE;
    float cTop = pTop;

    //--Iterate.
    for(int y = 0; y < TA_SIZE; y ++)
    {
        if(pLevel->GetClipAt(cLft, cTop + y, 0.0f, pDepth))
        {
            return true;
        }
    }

    //--All checks passed.
    return false;
}
bool TilemapActor::AbsIsBotClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth)
{
    //--Error check.
    if(!pLevel) return false;

    //--Position.
    float cLft = pLft;
    float cTop = pTop + TA_SIZE;

    //--Iterate.
    for(int x = 0; x < TA_SIZE; x ++)
    {
        if(pLevel->GetClipAt(cLft + x, cTop, 0.0f, pDepth))
        {
            return true;
        }
    }

    //--All checks passed.
    return false;
}
