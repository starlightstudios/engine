//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "PeakFreaksBadge.h"
#include "PeakFreaksBomb.h"
#include "PeakFreaksFlyby.h"
#include "PeakFreaksFragment.h"
#include "PeakFreaksPlayer.h"
#include "PeakFreaksPuzzlePiece.h"
#include "TileLayer.h"
#include "WorldDialogue.h"
#include "PeakFreaksTitle.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

///========================================== System ==============================================
PeakFreaksLevel::PeakFreaksLevel()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_PEAKFREAKSLEVEL;

    ///--[ ====== IRenderable ======= ]
    ///--[System]

    ///--[ ======== RootLevel ======= ]
    ///--[System]

    ///--[ ======= TiledLevel ======= ]
    ///--[System]
    ///--[Sizing]
    ///--[Tileset Packs]
    ///--[Raw Lists]
    ///--[Collision Storage]

    ///--[ ===== PeakFreaksLevel ==== ]
    ///--[System]
    mReturnToTitle = false;
    mRestartLevel = false;
    mShowPersonalBest = false;
    mSkipResultsScreen = false;
    mTriggerScript = NULL;
    mIsTutorial = false;
    mLastCameraX = 0.0f;
    mLastCameraY = 0.0f;
    mSelectionOffsetTick = 1;
    mSelectionOffsetTimer = 0;
    mMusicVolumeNormal = AudioManager::xMusicVolume;

    ///--[Dialogue / Tutorial]
    //--Main Dialogue
    mIsDialogueVisible = false;
    mDialogueVisTimer = 0;
    mDialogueLinesTotal = 0;
    mDialogueLines = NULL;
    mDialogueColors = NULL;
    mTutorialClearState = 0;

    //--Display
    mDialogueFloatTimer = 0;

    //--Current Goal
    mCurrentGoalTimer = 0;
    mCurrentGoalTimerMax = 1;
    mCurrentGoalWid = 1.0f;
    mDialogueCurrentGoal = NULL;

    //--Tutorial State
    mDirectionMoveTutorialActive = false;
    mDirectionTutorialUp = false;
    mDirectionTutorialRt = false;
    mDirectionTutorialDn = false;
    mDirectionTutorialLf = false;
    mBigMoveTutorialActive = false;
    mBigMoveCount = 0;
    mFillTutorialActive = false;
    mFillCount = 0;
    mPowerJumpTutorialActive = false;
    mPowerJumpCount = 0;

    ///--[Track Minimap]
    mPlayerTrackProgress = 0.0f;
    mRivalTrackProgress = 0.0f;
    mProgressSizeX = 0;
    mProgressSizeY = 0;
    mTileProgressValues = NULL;

    ///--[Finale Fireworks]
    mFireworksTimer = 60;
    mFireworks = new SugarLinkedList(true);

    ///--[Victory/Defeat]
    mPlayerReachedVictoryFirst = true;
    rVictoriousEntity = NULL;
    mVictoryTimer = PF_VICTORY_TICKS;
    mReturnToTitleVictory = false;
    mReturnToTitleDefeat = false;

    ///--[Opening]
    mIsOpening = true;
    mOpeningTimer = 0;
    mOverallOpenTimer = -225;
    mOpeningCameraX = 0.0f;
    mOpeningCameraY = 0.0f;
    mPrevOpeningX = -1.0f;
    mPrevOpeningY = -1.0f;
    mOpeningPositions = new SugarLinkedList(true);

    ///--[Pause Menu]
    mIsPaused = false;
    mPauseToTitleTimer = 0;
    mPauseCursor = PFLEVEL_PAUSE_RESUME;
    mPauseTimerCursor = 0;
    mPauseTimerBG = 0;
    mPauseTimerMenu = PFLEVEL_PAUSE_SLIDE_TICKS_CURSOR_OFF;
    mPauseTimerAlienFloat = 0;
    mPauseTimerAlienAnim = 0;

    ///--[Puzzle Handling]
    mFragmentList = new SugarLinkedList(true);
    mPuzzleGrid = NULL;

    ///--[Clouds]
    mCloudSpawnTimer = 0;
    mCloudList = new SugarLinkedList(true);

    ///--[Player Handling]
    mPlayer = NULL;
    mCursorTimer = 0;
    mSpeeds[0] = (1.0f / 4.0f) * TICKSTOSECONDS;
    mSpeeds[1] = (1.0f / 2.0f) * TICKSTOSECONDS;
    mSpeeds[2] = (1.0f / 1.0f) * TICKSTOSECONDS;
    mSpeeds[3] = (2.0f / 1.0f) * TICKSTOSECONDS;
    mSpeeds[4] = (2.0f / 1.0f) * TICKSTOSECONDS;

    ///--[Rival]
    mRival = NULL;

    ///--[UI Badges]
    mBadgeList = new SugarLinkedList(true);

    ///--[World Particles]
    mWorldFragmentList = new SugarLinkedList(true);

    ///--[Checkpoints]
    mActiveCheckpointName = NULL;
    mCheckpointX = 0.0f;
    mCheckpointY = 0.0f;
    mCheckpointZ = 0;
    mRivalActiveCheckpointName = NULL;
    mRivalCheckpointX = 0.0f;
    mRivalCheckpointY = 0.0f;
    mRivalCheckpointZ = 0;

    ///--[UI Particles]
    mCanyonBarParticles = new SugarLinkedList(true);
    mIceBeamParticles = new SugarLinkedList(true);
    mSpeedBoostParticles = new SugarLinkedList(true);

    ///--[Damage Zones]
    mPlayerStartZones = new SugarLinkedList(true);
    mDamageZones      = new SugarLinkedList(true);
    mVictoryZones     = new SugarLinkedList(true);
    mCoinZones        = new SugarLinkedList(true);
    mTreasureZones    = new SugarLinkedList(true);
    mLayerDropZones   = new SugarLinkedList(true);
    mCheckpointZones  = new SugarLinkedList(true);
    mSlipZones        = new SugarLinkedList(true);
    mClimbZones       = new SugarLinkedList(true);
    mTriggerZones     = new SugarLinkedList(true);

    ///--[Hazard Entities]
    mSliderList = new SugarLinkedList(true);
    mSpinnerList = new SugarLinkedList(true);
    mWaterfallList = new SugarLinkedList(true);
    mBombSpikeList = new SugarLinkedList(true);

    ///--[Reconstituting Zones]
    mCheckpointCount = 0;
    mReconPlayerStartZones = NULL;
    mReconDamageZones      = NULL;
    mReconVictoryZones     = NULL;
    mReconCoinZones        = NULL;
    mReconTreasureZones    = NULL;
    mReconLayerDropZones   = NULL;
    mReconCheckpointZones  = NULL;
    mReconSlipZones        = NULL;
    mReconClimbZones       = NULL;
    mReconTriggerZones     = NULL;
    mReconSliderList       = NULL;
    mReconSpinnerList      = NULL;
    mReconWaterfallList    = NULL;
    mReconBombSpikeList    = NULL;

    ///--[Powerups]
    //--Coin timer.
    mCoinTimer = 0;

    //--Ice beam.
    mIceBeamCountdown = -1;
    mIceBeamTimer = 0;

    //--Flybys.
    mFlybyList = new SugarLinkedList(true);
    mBombList = new SugarLinkedList(true);

    ///--[Goal Flags]
    mResolveGoalPosition = true;
    mGoalX = 0;
    mGoalY = 0;
    mGoalFlagTimer = 0;

    ///--[Images]
    memset(&Images, 0, sizeof(Images));

    ///--[Public Variables]
    //--Reset the match timer.
    xTimeElapsed = 0;

    ///--[ ====== Image Resolve ===== ]
    //--Setup.
    char tBuf[128];
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts
    Images.Data.rUIFont           = rDataLibrary->GetFont("PeakFreaks Level UI");
    Images.Data.rTutorialFont     = rDataLibrary->GetFont("PeakFreaks Tutorial UI");
    Images.Data.rTutorialGoalFont = rDataLibrary->GetFont("PeakFreaks Goal UI");

    //--Objects
    Images.Data.rCoin[0]             = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Coin/Coin0");
    Images.Data.rCoin[1]             = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Coin/Coin1");
    Images.Data.rCoin[2]             = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Coin/Coin2");
    Images.Data.rCoin[3]             = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Coin/Coin3");
    Images.Data.rTreasureClosed      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Treasure/Treasure0");
    Images.Data.rPowerup_Strike      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Powerup/PU_Strike0");
    Images.Data.rPowerup_SuperStrike = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Powerup/PU_SuperStrike0");
    Images.Data.rPowerup_IceBeam     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Powerup/PU_Ice0");
    Images.Data.rPowerup_CanyonBar   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Powerup/PU_Canyon0");
    Images.Data.rPowerup_HeliLift    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Powerup/PU_Heli0");
    Images.Data.rPowerup_GuanoBomb   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Powerup/PU_Guano0");
    Images.Data.rCheckpointIH4       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointIH4");
    Images.Data.rCheckpointAH4       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointAH4");
    Images.Data.rCheckpointIH3       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointIH3");
    Images.Data.rCheckpointAH3       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointAH3");
    Images.Data.rCheckpointIH2       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointIH2");
    Images.Data.rCheckpointAH2       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointAH2");
    Images.Data.rCheckpointIV4       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointIV4");
    Images.Data.rCheckpointAV4       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointAV4");
    Images.Data.rCheckpointIV3       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointIV3");
    Images.Data.rCheckpointAV3       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointAV3");
    Images.Data.rCheckpointIV2       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointIV2");
    Images.Data.rCheckpointAV2       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Checkpoint/CheckpointAV2");
    Images.Data.rHazardSlider        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Slider");
    Images.Data.rHazardSpinner[0]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner0");
    Images.Data.rHazardSpinner[1]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner1");
    Images.Data.rHazardSpinner[2]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner2");
    Images.Data.rHazardSpinner[3]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner3");
    Images.Data.rHazardSpinner[4]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner4");
    Images.Data.rHazardSpinner[5]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner5");
    Images.Data.rHazardSpinner[6]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner6");
    Images.Data.rHazardSpinner[7]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Spinner7");
    Images.Data.rHazardSpinnerBig[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/SpinnerBig0");
    Images.Data.rHazardSpinnerBig[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/SpinnerBig1");
    Images.Data.rHazardSpinnerBig[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/SpinnerBig2");
    Images.Data.rHazardSpinnerBig[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/SpinnerBig3");
    Images.Data.rHazardWaterfallFrames[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate0");
    Images.Data.rHazardWaterfallFrames[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate1");
    Images.Data.rHazardWaterfallFrames[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate2");
    Images.Data.rHazardWaterfallFrames[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate3");
    Images.Data.rHazardWaterfallFrames[4] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate4");
    Images.Data.rHazardWaterfallFrames[5] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate5");
    Images.Data.rHazardWaterfallFrames[6] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate6");
    Images.Data.rHazardWaterfallFrames[7] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/WaterGate7");
    Images.Data.rHazardWaterfallTop       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/StreamH");
    Images.Data.rHazardWaterfallMid       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/StreamM");
    Images.Data.rHazardWaterfallBot       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/StreamB");
    Images.Data.rHazardBombSpikeNeutral   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/CrystalBomb");
    Images.Data.rHazardBombSpikeExplosion[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Explosion0");
    Images.Data.rHazardBombSpikeExplosion[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Explosion1");
    Images.Data.rHazardBombSpikeExplosion[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Hazards/Explosion2");
    Images.Data.rDustParticle = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Dust/Dust0");
    for(int i = 0; i < 30; i ++)
    {
        sprintf(tBuf, "Root/Images/UI/World/EnFlag|%02i", i);
        Images.Data.rGoalFlags[i] =(SugarBitmap *)rDataLibrary->GetEntry(tBuf);
    }

    //--Tutorial
    Images.Data.rTutorialDialogue = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TutorialTextBox");
    Images.Data.rTutorialAlien    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TutorialAlien");
    Images.Data.rTutorialPrompt   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TutorialPrompt");
    Images.Data.rTutorialGoal     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TutorialGoal");
    Images.Data.rTutorialHand     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TutorialHand");
    Images.Data.rTutorialGoalMask = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TutorialGoalMask");

    //--Powerup: Drone Strike
    Images.Data.rPowerupDroneImg0    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/Drone0");
    Images.Data.rPowerupDroneImg1    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/Drone1");
    Images.Data.rPowerupDroneTarget  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/DroneTarget");
    Images.Data.rPowerupBombNormal   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/BombNormal");
    Images.Data.rPowerupBombSuper    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/BombSuper");
    Images.Data.rPowerupExplosion[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/Explosion0");
    Images.Data.rPowerupExplosion[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/Explosion1");
    Images.Data.rPowerupExplosion[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/Explosion2");
    Images.Data.rPowerupExplosion[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/DroneBomb/Explosion3");

    //--Powerup: Guano Bomb
    Images.Data.rPowerupGuanoBird    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/GuanoBomb/GuanoBird");
    Images.Data.rPowerupGuanoFall    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/GuanoBomb/GuanoFall");
    Images.Data.rPowerupGuanoTrap    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/GuanoBomb/GuanoTrap");
    Images.Data.rPowerupGuanoOverlay = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/GuanoBomb/GuanoOver");

    //--Powerup: Ice Beam
    Images.Data.rPowerupIceBeamOver = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/IceBeam/IceBeamOver");
    Images.Data.rPowerupIceBeamRain = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/IceBeam/IceBeamRain");

    //--UI
    Images.Data.rCursor[0]              = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Cursor0");
    Images.Data.rCursor[1]              = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Cursor1");
    Images.Data.rCursor[2]              = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Cursor2");
    Images.Data.rCrushIndicator         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowCrush");
    Images.Data.rUIBackground           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Background");
    Images.Data.rUIMenuButton           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Menu Button");
    Images.Data.rUIPlayerBacking        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Player Backing");
    Images.Data.rUIPlayerFrame          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Player Frame");
    Images.Data.rUIPlayerGuanoOverlay   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Player Guano Overlay");
    Images.Data.rUIRivalBacking         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Rival Backing");
    Images.Data.rUIRivalFrame           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Rival Frame");
    Images.Data.rUIRivalGuanoOverlay    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Rival Guano Overlay");
    Images.Data.rUIPlayerPuzzleAreaMask = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Puzzle Area Mask");
    Images.Data.rUIPlayerPuzzleArea     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Puzzle Area");
    Images.Data.rUIPlayerSpeedFill0     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Speed Fill 0");
    Images.Data.rUIPlayerSpeedFill1     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Speed Fill 1");
    Images.Data.rUIPlayerSpeedFill2     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Speed Fill 2");
    Images.Data.rUIPlayerSpeedFill3     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Speed Fill 3");
    Images.Data.rUIPlayerSpeedFill4     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Speed Fill 4");
    Images.Data.rUIPlayerSpeedFrame     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Speed Frame");
    Images.Data.rTutorialA              = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Tutorial A");
    Images.Data.rTutorialB              = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Tutorial B");
    Images.Data.rCanyonBarFlame         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Canyon Bar Flame");
    Images.Data.rSpeedParticle          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/SpeedParticles");
    Images.Data.rTrackBack              = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TrackBack");
    Images.Data.rTrackFill              = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TrackFill");

    //--Medallions/Badges
    Images.Data.rBadgeMedallion   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/Common/BadgeBacking");
    Images.Data.rBadgeAirstrike   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/Airstrike/Badge");
    Images.Data.rTitleAirstrike   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/Airstrike/Title");
    Images.Data.rBadgeCanyonBar   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/CanyonBar/Badge");
    Images.Data.rTitleCanyonBar   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/CanyonBar/Title");
    Images.Data.rBadgeGuano       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/Guano/Badge");
    Images.Data.rTitleGuano       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/Guano/Title");
    Images.Data.rBadgeHelilift    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/HeliLift/Badge");
    Images.Data.rTitleHelilift    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/HeliLift/Title");
    Images.Data.rBadgeIcebeam     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/IceBeam/Badge");
    Images.Data.rTitleIcebeam     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/IceBeam/Title");
    Images.Data.rBadgeSuperstrike = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/Airstrike/Badge");
    Images.Data.rTitleSuperstrike = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/PowerupUI/Airstrike/Title");

    //--Pause Menu
    Images.Data.rPauseAlien[0]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Alien0");
    Images.Data.rPauseAlien[1]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Alien1");
    Images.Data.rPauseAlien[2]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Alien2");
    Images.Data.rPauseCursor      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Cursor");
    Images.Data.rPauseBG          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/PauseBG");
    Images.Data.rPauseSelQuit     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Sel_Quit");
    Images.Data.rPauseSelResume   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Sel_Resume");
    Images.Data.rPauseSelRestart  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Sel_Restart");
    Images.Data.rPauseTextQuit    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Text_Quit");
    Images.Data.rPauseTextResume  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Text_Resume");
    Images.Data.rPauseTextRestart = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Pause/Text_Restart");

    //--Opening
    Images.Data.rOpeningAlien[0]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Alien0");
    Images.Data.rOpeningAlien[1]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Alien1");
    Images.Data.rOpeningAlien[2]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Alien2");
    Images.Data.rOpeningAlien[3]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Alien3");
    Images.Data.rOpeningBanner[0]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Banner0");
    Images.Data.rOpeningBanner[1]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Banner1");
    Images.Data.rOpeningBanner[2]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Banner2");
    Images.Data.rOpeningBanner[3]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Banner3");
    Images.Data.rOpeningCountdown[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Countdown0");
    Images.Data.rOpeningCountdown[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Countdown1");
    Images.Data.rOpeningCountdown[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Countdown2");
    Images.Data.rOpeningCountdown[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Countdown3");
    Images.Data.rOpeningCountdown[4] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/Countdown4");

    //--Background
    Images.Data.rBackgroundSky = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Background_All");
    Images.Data.rClouds[0]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Cloud0");
    Images.Data.rClouds[1]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Cloud1");
    Images.Data.rClouds[2]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Cloud2");

    //--Puzzle Pieces
    Images.Data.mPuzzleArray[PEAKFREAK_PUZZLE_TYPE_ARROW_DN] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowDn");
    Images.Data.mPuzzleArray[PEAKFREAK_PUZZLE_TYPE_ARROW_UP] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowUp");
    Images.Data.mPuzzleArray[PEAKFREAK_PUZZLE_TYPE_ARROW_LF] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowLf");
    Images.Data.mPuzzleArray[PEAKFREAK_PUZZLE_TYPE_ARROW_RT] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowRt");
    Images.Data.mPuzzleFragments[PEAKFREAK_PUZZLE_TYPE_ARROW_DN] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowDnFrag");
    Images.Data.mPuzzleFragments[PEAKFREAK_PUZZLE_TYPE_ARROW_UP] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowUpFrag");
    Images.Data.mPuzzleFragments[PEAKFREAK_PUZZLE_TYPE_ARROW_LF] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowLfFrag");
    Images.Data.mPuzzleFragments[PEAKFREAK_PUZZLE_TYPE_ARROW_RT] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowRtFrag");

    //--Fireworks
    const char *cFireworkCols[] = {"Blue", "Green", "Orange", "Pink"};
    for(int i = 0; i < PFTITLE_FIREWORKS_COLS; i ++)
    {
        for(int p = 0; p < PFTITLE_FIREWORKS_FRAMES; p ++)
        {
            sprintf(tBuf, "Root/Images/UI/Fireworks/%s|%i", cFireworkCols[i], p);
            Images.Finale.rFireworks[i][p] = (SugarBitmap *)rDataLibrary->GetEntry(tBuf);
        }
    }

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *)) && VerifyStructure(&Images.Finale, sizeof(Images.Finale), sizeof(void *));

    ///--[Puzzle Grid Setup]
    //--Allocate the puzzle grid.
    mPuzzleGrid = (PeakFreaksPuzzlePiece ***)starmemoryalloc(sizeof(PeakFreaksPuzzlePiece **) * PLL_PUZZLE_SIZE_X);
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        mPuzzleGrid[x] = (PeakFreaksPuzzlePiece **)starmemoryalloc(sizeof(PeakFreaksPuzzlePiece *) * PLL_PUZZLE_SIZE_Y);
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            mPuzzleGrid[x][y] = new PeakFreaksPuzzlePiece();
        }
    }

    //--Initial.
    RandomizePuzzleGrid();

    ///--[World Dialogue]
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue)
    {
        rWorldDialogue->SetNormalTextTick("PF Voice Tick");
    }
}
PeakFreaksLevel::~PeakFreaksLevel()
{
    //--System.
    free(mTriggerScript);
    delete mFireworks;

    //--Dialogue.
    for(int i = 0; i < mDialogueLinesTotal; i ++)
    {
        free(mDialogueLines[i]);
        free(mDialogueColors[i]);
    }
    free(mDialogueLines);
    free(mDialogueColors);
    free(mDialogueCurrentGoal);

    //--Progress tracking.
    for(int x = 0; x < mProgressSizeX; x ++) free(mTileProgressValues[x]);
    free(mTileProgressValues);

    //--Opening.
    delete mOpeningPositions;

    //--Clouds.
    delete mCloudList;

    //--Grid.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            delete mPuzzleGrid[x][y];
        }
        free(mPuzzleGrid[x]);
    }
    free(mPuzzleGrid);

    //--Puzzle Fragments.
    delete mFragmentList;

    //--Player.
    delete mPlayer;
    free(mActiveCheckpointName);
    free(mRivalActiveCheckpointName);

    //--Rival.
    delete mRival;

    //--Badges.
    delete mBadgeList;

    //--World Particles
    delete mWorldFragmentList;

    //--Particles.
    delete mCanyonBarParticles;
    delete mIceBeamParticles;
    delete mSpeedBoostParticles;

    //--Objects.
    delete mPlayerStartZones;
    delete mDamageZones;
    delete mVictoryZones;
    delete mCoinZones;
    delete mTreasureZones;
    delete mLayerDropZones;
    delete mCheckpointZones;
    delete mSlipZones;
    delete mClimbZones;
    delete mTriggerZones;

    //--Hazard Entities
    delete mSliderList;
    delete mSpinnerList;
    delete mWaterfallList;
    delete mBombSpikeList;

    //--Reconstituting Zones
    delete mReconPlayerStartZones;
    delete mReconDamageZones;
    delete mReconVictoryZones;
    delete mReconCoinZones;
    delete mReconTreasureZones;
    delete mReconLayerDropZones;
    delete mReconCheckpointZones;
    delete mReconSlipZones;
    delete mReconClimbZones;
    delete mReconTriggerZones;
    delete mReconSliderList;
    delete mReconSpinnerList;
    delete mReconWaterfallList;
    delete mReconBombSpikeList;

    //--Powerups.
    delete mFlybyList;
    delete mBombList;
}

///--[Public Statics]
//--Edited by typing SEEDXXXX where the X's are the four-digit seed number on the title screen.
//  If the value is -1, uses a timer-based seed. Otherwise, uses this seed, for testing.
int PeakFreaksLevel::xMandatedSeed = -1;

//--Used by the title screen to pass the difficulty to this object.
int PeakFreaksLevel::xDifficulty = 0;

//--Tracks the amount of time elapsed since the match began. In ticks.
int PeakFreaksLevel::xTimeElapsed = 0;

//--Used by the PeakFreaksTitle to determine who won the match.
bool PeakFreaksLevel::xPlayerVictory = false;

//--Used during level generation, tracks the "direction" of movement across a tile so one
//  of the checkpoints can be deleted.
int PeakFreaksLevel::xStartType = RECON_START_N;

///===================================== Property Queries =========================================
bool PeakFreaksLevel::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_PEAKFREAKSLEVEL) return true;
    if(pType == POINTER_TYPE_TILEDLEVEL) return true;
    if(pType == POINTER_TYPE_ROOTLEVEL) return true;
    return false;
}
bool PeakFreaksLevel::GetClipAt(float pX, float pY, float pZ, int pLayer)
{
    //--Compute the tile position to check.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    //--Edge check.
    if(pLayer < 0 || pLayer >= mCollisionLayersTotal) return false;
    if(tTileX < 0.0f || tTileY < 0.0f || tTileX >= mCollisionLayers[pLayer].mCollisionSizeX || tTileY >= mCollisionLayers[pLayer].mCollisionSizeY) return true;

    //--Collision hull.
    uint8_t tClipVal = mCollisionLayers[pLayer].mCollisionData[tTileX][tTileY];
    if(tClipVal == CLIP_NONE) return false;
    return true;
}
bool PeakFreaksLevel::GetClipAtTile(int pX, int pY)
{
    //--Edge check.
    if(mCollisionLayersTotal < 1) return false;
    if(pX < 0 || pY < 0 || pX >= mCollisionLayers[0].mCollisionSizeX || pY >= mCollisionLayers[0].mCollisionSizeY) return true;

    //--Collision hull.
    uint8_t tClipVal = mCollisionLayers[0].mCollisionData[pX][pY];
    if(tClipVal == CLIP_NONE) return false;
    return true;
}
bool PeakFreaksLevel::GetClipAt(float pX, float pY, float pZ)
{
    return GetClipAt(pX, pY, pZ, 0);
}
bool PeakFreaksLevel::IsOutOfBounds(int pX, int pY)
{
    if(pX < 0 || pY < 0) return true;
    if(mCollisionLayersTotal < 1) return false;
    if(pX >= mCollisionLayers[0].mCollisionSizeX) return true;
    if(pY >= mCollisionLayers[0].mCollisionSizeY) return true;
    return false;
}
int PeakFreaksLevel::IsTouchingZone(SugarLinkedList *pZoneList, float pLft, float pTop, float pRgt, float pBot, int pLayer, bool pRemoveOnTrue)
{
    ///--[Documentation]
    //--Scans the requested list of ZonePack *'s or derived classes. Returns the subtype on collision, -1
    //  otherwise. Optionally can remove the offending zone on contact.
    if(!pZoneList) return -1;

    //--Iterate.
    ZonePack *rCheckPack = (ZonePack *)pZoneList->PushIterator();
    while(rCheckPack)
    {
        //--Hit.
        if(IsCollision2DReal(rCheckPack->mDim, pLft, pTop, pRgt, pBot) && rCheckPack->mLayer == pLayer)
        {
            //--Clean up list.
            pZoneList->PopIterator();

            //--Optional: Remove the hit entry.
            if(pRemoveOnTrue) pZoneList->RemoveElementP(rCheckPack);
            return rCheckPack->mSubtype;
        }

        //--Next.
        rCheckPack = (ZonePack *)pZoneList->AutoIterate();
    }

    //--No hits.
    return -1;
}
bool PeakFreaksLevel::IsTouchingDamageZone(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    return (IsTouchingZone(mDamageZones, pLft, pTop, pRgt, pBot, pLayer, false) != -1);
}
bool PeakFreaksLevel::IsTouchingVictoryZone(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    return (IsTouchingZone(mVictoryZones, pLft, pTop, pRgt, pBot, pLayer, false) != -1);
}
int PeakFreaksLevel::IsTouchingCoinZone(float pLft, float pTop, float pRgt, float pBot, int pLayer, bool pRemoveOnTrue)
{
    return IsTouchingZone(mCoinZones, pLft, pTop, pRgt, pBot, pLayer, pRemoveOnTrue);
}
bool PeakFreaksLevel::IsTouchingTreasureZone(float pLft, float pTop, float pRgt, float pBot, int pLayer, bool pRemoveOnTrue)
{
    return (IsTouchingZone(mTreasureZones, pLft, pTop, pRgt, pBot, pLayer, pRemoveOnTrue) != -1);
}
int PeakFreaksLevel::IsTouchingLayerDropZone(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    ///--[Documentation]
    //--Unlike other IsTouching functions, this one returns the layer that the touched zone specifies.
    //  If no zone is touched, returns -1.

    //--Iterate.
    DropZonePack *rCheckPack = (DropZonePack *)mLayerDropZones->PushIterator();
    while(rCheckPack)
    {
        //--Hit.
        if(IsCollisionStrict(rCheckPack->mDim, pLft, pTop, pRgt, pBot) && rCheckPack->mLayer == pLayer)
        {
            mLayerDropZones->PopIterator();
            return rCheckPack->mDestination;
        }

        //--Next.
        rCheckPack = (DropZonePack *)mLayerDropZones->AutoIterate();
    }

    //--No hits.
    return -1;
}
const char *PeakFreaksLevel::IsTouchingCheckpoint(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    ///--[Documentation]
    //--Returns the name of the touched checkpoint, or NULL if none are being touched.

    //--Iterate.
    ZonePack *rCheckPack = (ZonePack *)mCheckpointZones->PushIterator();
    while(rCheckPack)
    {
        //--Hit.
        if(IsCollision2DReal(rCheckPack->mDim, pLft, pTop, pRgt, pBot) && rCheckPack->mLayer == pLayer)
        {
            const char *rIteratorName = mCheckpointZones->GetIteratorName();
            mCheckpointZones->PopIterator();
            return rIteratorName;
        }

        //--Next.
        rCheckPack = (ZonePack *)mCheckpointZones->AutoIterate();
    }

    //--No hits.
    return NULL;
}
bool PeakFreaksLevel::IsTouchingSlipZone(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    return (IsTouchingZone(mSlipZones, pLft, pTop, pRgt, pBot, pLayer, false) != -1);
}
bool PeakFreaksLevel::IsWithinSlipZone(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    ///--[Documentation]
    //--Slip zones require a full "esconcement". That is, the player must be entirely within the slip zone.

    //--Iterate.
    ZonePack *rCheckPack = (ZonePack *)mSlipZones->PushIterator();
    while(rCheckPack)
    {
        //--Hit.
        if(IsCollisionStrict(rCheckPack->mDim, pLft, pTop, pRgt, pBot) && rCheckPack->mLayer == pLayer)
        {
            //--Clean up list.
            mSlipZones->PopIterator();
            return true;
        }

        //--Next.
        rCheckPack = (ZonePack *)mSlipZones->AutoIterate();
    }

    //--No hits.
    return false;
}
bool PeakFreaksLevel::IsTouchingClimbZone(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    return (IsTouchingZone(mClimbZones, pLft, pTop, pRgt, pBot, pLayer, false) != -1);
}
bool PeakFreaksLevel::IsWithinClimbZone(float pLft, float pTop, float pRgt, float pBot, int pLayer)
{
    ///--[Documentation]
    //--As slip zones above, except for climb zones.

    //--Iterate.
    ZonePack *rCheckPack = (ZonePack *)mClimbZones->PushIterator();
    while(rCheckPack)
    {
        //--Hit.
        if(IsCollisionStrict(rCheckPack->mDim, pLft, pTop, pRgt, pBot) && rCheckPack->mLayer == pLayer)
        {
            //--Clean up list.
            mClimbZones->PopIterator();
            return true;
        }

        //--Next.
        rCheckPack = (ZonePack *)mClimbZones->AutoIterate();
    }

    //--No hits.
    return false;
}
float PeakFreaksLevel::GetCheckpointX(void *pCaller)
{
    if(pCaller == mRival) return mRivalCheckpointX;
    return mCheckpointX;
}
float PeakFreaksLevel::GetCheckpointY(void *pCaller)
{
    if(pCaller == mRival) return mRivalCheckpointY;
    return mCheckpointY;
}
int PeakFreaksLevel::GetCheckpointZ(void *pCaller)
{
    if(pCaller == mRival) return mRivalCheckpointZ;
    return mCheckpointZ;
}
bool PeakFreaksLevel::IsTrampoline(int pTileX, int pTileY)
{
    //--Sliders.
    PF_Slider *rSlider = (PF_Slider *)mSliderList->PushIterator();
    while(rSlider)
    {
        if(pTileX == rSlider->mXPos/TileLayer::cxSizePerTile && pTileY == rSlider->mYPos/TileLayer::cxSizePerTile)
        {
            mSliderList->PopIterator();
            return true;
        }

        rSlider = (PF_Slider *)mSliderList->AutoIterate();
    }

    //--Spinners.
    PF_Spinner *rSpinner = (PF_Spinner *)mSpinnerList->PushIterator();
    while(rSpinner)
    {
        if(pTileX == rSpinner->mXPos/TileLayer::cxSizePerTile && pTileY == rSpinner->mYPos/TileLayer::cxSizePerTile)
        {
            mSpinnerList->PopIterator();
            return true;
        }

        rSpinner = (PF_Spinner *)mSpinnerList->AutoIterate();
    }

    //--All checks passed.
    return false;
}

///======================================= Manipulators ===========================================
void PeakFreaksLevel::SetTutorial()
{
    mIsTutorial = true;
    mSkipResultsScreen = true;
}
void PeakFreaksLevel::SetTriggerScript(const char *pPath)
{
    ResetString(mTriggerScript, pPath);
}
void PeakFreaksLevel::SpawnPlayer()
{
    //--Only works if the player does not already exist.
    if(mPlayer) return;
    mPlayer = new PeakFreaksPlayer();

    //--Place them at the player start.
    mPlayer->SetPosition(mCheckpointX / 16.0f, mCheckpointY / 16.0f);
    SetPlayerLayer(mCheckpointZ);

    //--AI:
    //mPlayer->SetAIControl();

    //--Reset the player's checkpoint based on the initial direction of the starting block.
    //  This is to make them spawn on the starting line instead of behind it if they die.
    //--The AI is deliberately not afforded this.
    if(PeakFreaksLevel::xStartType == RECON_START_N)
    {
        mCheckpointY = mCheckpointY - 64.0f;
    }
    else if(PeakFreaksLevel::xStartType == RECON_START_E)
    {
        mCheckpointX = mCheckpointX - 48.0f;
    }
    else if(PeakFreaksLevel::xStartType == RECON_START_S)
    {
        mCheckpointY = mCheckpointY + 48.0f;
    }
    else if(PeakFreaksLevel::xStartType == RECON_START_W)
    {
        mCheckpointX = mCheckpointX - 64.0f;
    }
}
void PeakFreaksLevel::SpawnRival()
{
    //--Only works if the rival does not already exist.
    if(mRival) return;
    mRival = new PeakFreaksPlayer();

    //--Locate the 1-layer player start and put them there.
    ZonePack *rPlayerStart = (ZonePack *)mPlayerStartZones->PushIterator();
    while(rPlayerStart)
    {
        //--Position.
        if(rPlayerStart->mLayer == 1)
        {
            mRival->SetPosition(rPlayerStart->mDim.mLft / 16.0f, rPlayerStart->mDim.mTop / 16.0f);
            mRivalCheckpointX = rPlayerStart->mDim.mLft;
            mRivalCheckpointY = rPlayerStart->mDim.mTop;
            mPlayerStartZones->PopIterator();
            break;
        }

        //--Next.
        rPlayerStart = (ZonePack *)mPlayerStartZones->AutoIterate();
    }

    //--AI:
    mRival->SetAIControl();
    mRival->SetAIDifficulty(xDifficulty);

    //--Graphics is based on difficulty:
    if(xDifficulty == PTTITLE_DIFFICULTY_CHALLENGING)
        mRival->SetMimiGraphics();
    else if(xDifficulty == PTTITLE_DIFFICULTY_IMPOSSIBLE)
        mRival->SetAllenGraphics();
    else
        mRival->SetScooterGraphics();


}
void PeakFreaksLevel::PositionPlayer(float pX, float pY)
{
    //--Note: In world coordinates. Each whole integer is 1 tile. Decimals are allowed to indicate partial
    //  tile positions.
    if(!mPlayer) return;
    mPlayer->SetPosition(pX, pY);
}
void PeakFreaksLevel::SetPlayerLayer(int pZ)
{
    //--Sets the player's layer, which is an integer. The "Lowest" layer is 0. All layers beneath
    //  the active layer continue to render.
    mPlayer->SetLayer(pZ);

    if(pZ == 0)
    {
        SetRenderingDisabled("Floor00", false);
        SetRenderingDisabled("Floor01", false);
        SetRenderingDisabled("Floor02", false);
        SetRenderingDisabled("Floor03", false);
        SetRenderingDisabled("Floor0S", true);
        SetRenderingDisabled("Floor10", true);
        SetRenderingDisabled("Floor11", true);
    }
    else if(pZ == 1)
    {
        SetRenderingDisabled("Floor00", false);
        SetRenderingDisabled("Floor01", false);
        SetRenderingDisabled("Floor02", false);
        SetRenderingDisabled("Floor03", false);
        SetRenderingDisabled("Floor0S", false);
        SetRenderingDisabled("Floor10", false);
        SetRenderingDisabled("Floor11", false);
    }
}
void PeakFreaksLevel::SetRenderingDisabled(const char *pLayerName, bool pFlag)
{
    TileLayer *rLayer = (TileLayer *)mTileLayers->GetElementByName(pLayerName);
    if(!rLayer) return;
    rLayer->SetDoNotRenderFlag(pFlag);
}
void PeakFreaksLevel::ActivateCheckpoint(void *pCaller, const char *pCheckpointName)
{
    ///--[Documentation]
    //--Given a checkpoint name, locates it and sets the current checkpoint to its values.
    if(!pCheckpointName) return;
    if(pCaller == mRival) { ActivateRivalCheckpoint(pCheckpointName); return; }

    //--Locate.
    ZonePack *rCheckpoint = (ZonePack *)mCheckpointZones->GetElementByName(pCheckpointName);
    if(!rCheckpoint) return;

    //--This is already the checkpoint in question.
    if(!strcasecmp(mActiveCheckpointName, pCheckpointName)) return;

    //--Set.
    ResetString(mActiveCheckpointName, pCheckpointName);
    mCheckpointX = rCheckpoint->mDim.mLft;
    mCheckpointY = rCheckpoint->mDim.mTop;
    mCheckpointZ = rCheckpoint->mLayer;

    //--Set the player's progress bar. Each checkpoint represents 0.20 of the bar.
    int tCheckpointSlot = mCheckpointZones->GetSlotOfElementByName(pCheckpointName);
    //mPlayerTrackProgress = (tCheckpointSlot * 0.167f) + 0.167f;

    //--SFX.
    AudioManager::Fetch()->PlaySound("PF Entity Checkpoint");
}
void PeakFreaksLevel::ActivateRivalCheckpoint(const char *pCheckpointName)
{
    ///--[Documentation]
    //--Same as above but for the Rival character.
    if(!pCheckpointName) return;

    //--Locate.
    ZonePack *rCheckpoint = (ZonePack *)mCheckpointZones->GetElementByName(pCheckpointName);
    if(!rCheckpoint) return;

    //--Set.
    ResetString(mRivalActiveCheckpointName, pCheckpointName);
    mRivalCheckpointX = rCheckpoint->mDim.mLft;
    mRivalCheckpointY = rCheckpoint->mDim.mTop;
    mRivalCheckpointZ = rCheckpoint->mLayer;

    //--Set the player's progress bar. Each checkpoint represents 0.20 of the bar.
    int tCheckpointSlot = mCheckpointZones->GetSlotOfElementByName(pCheckpointName);
    //mRivalTrackProgress = (tCheckpointSlot * 0.167f) + 0.167f;
}
void PeakFreaksLevel::SpawnBadge(bool pIsRival, int pCode)
{
    ///--[Documentation]
    //--Spawns a badge using the given code. If flagged for the rival, changes the X position.
    float tXPosition = 100.0f;
    if(pIsRival) tXPosition = VIRTUAL_CANVAS_X - 100.0f;

    //--Range check.
    if(pCode < PFLEVEL_COIN_SUBTYPE_ROLL_LOW || pCode > PFLEVEL_COIN_SUBTYPE_ROLL_HI) return;

    //--SFX.
    AudioManager::Fetch()->PlaySound("PF Powerup Activation");

    //--Spawn.
    PeakFreaksBadge *nBadge = new PeakFreaksBadge();
    mBadgeList->AddElement("X", nBadge, &PeakFreaksBadge::DeleteThis);

    //--Set X position.
    nBadge->SetXPosition(tXPosition);

    //--Medallion always uses the same image.
    nBadge->SetMedallion(Images.Data.rBadgeMedallion);

    //--Set images.
    if(pCode == PFLEVEL_COIN_SUBTYPE_GUANOBOMB)
    {
        nBadge->SetBadge(Images.Data.rBadgeGuano);
        nBadge->SetTitle(Images.Data.rTitleGuano);
    }
    else if(pCode == PFLEVEL_COIN_SUBTYPE_DRONESTRIKE)
    {
        nBadge->SetBadge(Images.Data.rBadgeAirstrike);
        nBadge->SetTitle(Images.Data.rTitleAirstrike);
    }
    else if(pCode == PFLEVEL_COIN_SUBTYPE_SUPERSTRIKE)
    {
        nBadge->SetBadge(Images.Data.rBadgeSuperstrike);
        nBadge->SetTitle(Images.Data.rTitleSuperstrike);
    }
    else if(pCode == PFLEVEL_COIN_SUBTYPE_CANYONBAR)
    {
        nBadge->SetBadge(Images.Data.rBadgeCanyonBar);
        nBadge->SetTitle(Images.Data.rTitleCanyonBar);
    }
    else if(pCode == PFLEVEL_COIN_SUBTYPE_HELILIFT)
    {
        nBadge->SetBadge(Images.Data.rBadgeHelilift);
        nBadge->SetTitle(Images.Data.rTitleHelilift);
    }
    else if(pCode == PFLEVEL_COIN_SUBTYPE_ICEBEAM)
    {
        nBadge->SetBadge(Images.Data.rBadgeIcebeam);
        nBadge->SetTitle(Images.Data.rTitleIcebeam);
    }
}
void PeakFreaksLevel::ActivateIceBeam()
{
    mIceBeamCountdown = PFLEVEL_ICEBEAM_ACTIVATION_COOLDOWN;
}
void PeakFreaksLevel::SpawnGuanoProjectile(int pX, int pY, float pHeight)
{
    PeakFreaksBomb *nBomb = new PeakFreaksBomb();
    nBomb->SetSubtype(PFBOMB_SUBTYPE_GUANO);
    nBomb->SetTilePosition(pX, pY);
    nBomb->SetHeight(pHeight);
    nBomb->SetFlightImg(Images.Data.rPowerupGuanoFall);
    nBomb->SetTargetImg(NULL);
    nBomb->SetExplosionImg(0, Images.Data.rPowerupGuanoTrap);
    nBomb->SetExplosionImg(1, Images.Data.rPowerupGuanoTrap);
    nBomb->SetExplosionImg(2, Images.Data.rPowerupGuanoTrap);
    nBomb->SetExplosionImg(3, Images.Data.rPowerupGuanoTrap);
    mBombList->AddElement("X", nBomb, &PeakFreaksBomb::DeleteThis);
}
void PeakFreaksLevel::SpawnDroneProjectile(int pX, int pY, float pHeight)
{
    PeakFreaksBomb *nBomb = new PeakFreaksBomb();
    nBomb->SetSubtype(PFFB_SUBTYPE_BOMB);
    nBomb->SetTilePosition(pX, pY);
    nBomb->SetHeight(pHeight);
    nBomb->SetFlightImg(Images.Data.rPowerupBombNormal);
    nBomb->SetTargetImg(Images.Data.rPowerupDroneTarget);
    nBomb->SetExplosionImg(0, Images.Data.rPowerupExplosion[0]);
    nBomb->SetExplosionImg(1, Images.Data.rPowerupExplosion[1]);
    nBomb->SetExplosionImg(2, Images.Data.rPowerupExplosion[2]);
    nBomb->SetExplosionImg(3, Images.Data.rPowerupExplosion[3]);
    mBombList->AddElement("X", nBomb, &PeakFreaksBomb::DeleteThis);
}
void PeakFreaksLevel::SpawnSuperProjectile(int pX, int pY, float pHeight)
{
    PeakFreaksBomb *nBomb = new PeakFreaksBomb();
    nBomb->SetSubtype(PFFB_SUBTYPE_SUPERBOMB);
    nBomb->SetTilePosition(pX, pY);
    nBomb->SetHeight(pHeight);
    nBomb->SetFlightImg(Images.Data.rPowerupBombSuper);
    nBomb->SetTargetImg(Images.Data.rPowerupDroneTarget);
    nBomb->SetExplosionImg(0, Images.Data.rPowerupExplosion[0]);
    nBomb->SetExplosionImg(1, Images.Data.rPowerupExplosion[1]);
    nBomb->SetExplosionImg(2, Images.Data.rPowerupExplosion[2]);
    nBomb->SetExplosionImg(3, Images.Data.rPowerupExplosion[3]);
    mBombList->AddElement("X", nBomb, &PeakFreaksBomb::DeleteThis);
}
void PeakFreaksLevel::SpawnIceBeamParticles()
{
    ///--[Documentation]
    //--Spawns ice beam projectiles at the top of the map. They will then rain down.
    if(mCollisionLayersTotal < 1) return;
    for(int x = -16; x < mCollisionLayers[0].mCollisionSizeX + 16; x ++)
    {
        //--Spawn three particles at different Y positions.
        float tXPos = x * TileLayer::cxSizePerTile;
        for(int y = 0; y < 3; y ++)
        {
            //--Y position.
            float tYPos = (y * 16.0f) - 150.0f;

            //--Spawn particle.
            PF_Particle *nParticle = (PF_Particle *)starmemoryalloc(sizeof(PF_Particle));
            nParticle->Initialize();
            nParticle->mXPos = tXPos;
            nParticle->mYPos = tYPos;
            nParticle->mXSpeed = 0.0f;
            nParticle->mYSpeed = 0.0f;

            //--Register.
            mIceBeamParticles->AddElement("X", nParticle, &FreeThis);
        }
    }
}
void PeakFreaksLevel::SpawnSpeedParticle(float pX, float pY, int pTimer)
{
    PF_Particle *nParticle = (PF_Particle *)starmemoryalloc(sizeof(PF_Particle));
    nParticle->Initialize();
    nParticle->mTimer = pTimer;
    nParticle->mXPos = pX;
    nParticle->mXSpeed = pX;
    nParticle->mYPos = pY;
    nParticle->mYSpeed = pY;
    mSpeedBoostParticles->AddElement("X", nParticle, &FreeThis);
}
void PeakFreaksLevel::StartVictory(void *pEntity)
{
    //--Called when a player has won the game. If someone else already has, is ignored.
    if(rVictoriousEntity) return;

    //--If it's the rival, set this flag.
    if(pEntity == mRival)
    {
        mPlayerReachedVictoryFirst = false;
        return;
    }

    //--Only the player can be "victorious" to finish a level.
    rVictoriousEntity = pEntity;
    AudioManager::Fetch()->PlaySound("PF Announce StageClear");

    //--Fade the music out over two seconds.
    AudioManager::Fetch()->FadeMusic(120);

    //--Fast time handling. First, get a structure containing the current best time data.
    PFTitle_Highscore *tBestTimeData = PeakFreaksTitle::ReadHighscores();
    if(tBestTimeData)
    {
        //--Easy: Flag the win, store the best time.
        if(xDifficulty == PTTITLE_DIFFICULTY_SIMPLE)
        {
            //--New best time!
            if(xTimeElapsed < tBestTimeData->mEasyTimes[0] || tBestTimeData->mEasyTimes[0] == 0)
            {
                mShowPersonalBest = true;
                tBestTimeData->mEasyTimes[2] = tBestTimeData->mEasyTimes[1];
                tBestTimeData->mEasyTimes[1] = tBestTimeData->mEasyTimes[0];
                tBestTimeData->mEasyTimes[0] = xTimeElapsed;
            }
            //--New second best time!
            else if(xTimeElapsed < tBestTimeData->mEasyTimes[1] || tBestTimeData->mEasyTimes[1] == 0)
            {
                tBestTimeData->mEasyTimes[2] = tBestTimeData->mEasyTimes[1];
                tBestTimeData->mEasyTimes[1] = xTimeElapsed;
            }
            //--New third best time!
            else if(xTimeElapsed < tBestTimeData->mEasyTimes[2] || tBestTimeData->mEasyTimes[2] == 0)
            {
                tBestTimeData->mEasyTimes[2] = xTimeElapsed;
            }
        }
        //--Medium:
        else if(xDifficulty == PTTITLE_DIFFICULTY_CHALLENGING)
        {
            //--New best time!
            if(xTimeElapsed < tBestTimeData->mMediumTimes[0] || tBestTimeData->mMediumTimes[0] == 0)
            {
                mShowPersonalBest = true;
                tBestTimeData->mMediumTimes[2] = tBestTimeData->mMediumTimes[1];
                tBestTimeData->mMediumTimes[1] = tBestTimeData->mMediumTimes[0];
                tBestTimeData->mMediumTimes[0] = xTimeElapsed;
            }
            //--New second best time!
            else if(xTimeElapsed < tBestTimeData->mMediumTimes[1] || tBestTimeData->mMediumTimes[1] == 0)
            {
                tBestTimeData->mMediumTimes[2] = tBestTimeData->mMediumTimes[1];
                tBestTimeData->mMediumTimes[1] = xTimeElapsed;
            }
            //--New third best time!
            else if(xTimeElapsed < tBestTimeData->mMediumTimes[2] || tBestTimeData->mMediumTimes[2] == 0)
            {
                tBestTimeData->mMediumTimes[2] = xTimeElapsed;
            }
        }
        //--Hard:
        else if(xDifficulty == PTTITLE_DIFFICULTY_IMPOSSIBLE)
        {
            //--New best time!
            if(xTimeElapsed < tBestTimeData->mHardTimes[0] || tBestTimeData->mHardTimes[0] == 0)
            {
                mShowPersonalBest = true;
                tBestTimeData->mHardTimes[2] = tBestTimeData->mHardTimes[1];
                tBestTimeData->mHardTimes[1] = tBestTimeData->mHardTimes[0];
                tBestTimeData->mHardTimes[0] = xTimeElapsed;
            }
            //--New second best time!
            else if(xTimeElapsed < tBestTimeData->mHardTimes[1] || tBestTimeData->mHardTimes[1] == 0)
            {
                tBestTimeData->mHardTimes[2] = tBestTimeData->mHardTimes[1];
                tBestTimeData->mHardTimes[1] = xTimeElapsed;
            }
            //--New third best time!
            else if(xTimeElapsed < tBestTimeData->mHardTimes[2] || tBestTimeData->mHardTimes[2] == 0)
            {
                tBestTimeData->mHardTimes[2] = xTimeElapsed;
            }
        }

        //--Write results to the hard drive.
        PeakFreaksTitle::WriteHighscores(tBestTimeData);

        //--Clean.
        free(tBestTimeData);
    }
}
void PeakFreaksLevel::SpawnDustParticles(int pTileX, int pTileY)
{
    ///--[Documentation]
    //--Spawns dust particles at the given tile location. Used to indicate a trampoline bounce.
    int cLifetime = 30;
    float cPosOff = 4.0f;
    float cPosSct = 2.0f;
    float cSpdOff = 1.0f;
    float cSpdSct = 0.25f;

    //--Position.
    float cBlockX = (pTileX * TileLayer::cxSizePerTile) + (TileLayer::cxSizePerTile * 0.50f);
    float cBlockY = (pTileY * TileLayer::cxSizePerTile) + (TileLayer::cxSizePerTile * 0.50f);

    //--NW Fragment
    PeakFreaksFragment *nNWFragment = new PeakFreaksFragment();
    nNWFragment->SetLifetime(cLifetime);
    nNWFragment->SetImage(Images.Data.rDustParticle);
    nNWFragment->SetPosition(cBlockX - ScatterWithinRange(cPosOff, cPosSct), cBlockY - ScatterWithinRange(cPosOff, cPosSct));
    nNWFragment->SetSpeeds(ScatterWithinRange(-cSpdOff, cSpdSct), ScatterWithinRange(-cSpdOff, cSpdSct));
    mWorldFragmentList->AddElement("X", nNWFragment, PeakFreaksFragment::DeleteThis);

    //--NE Fragment.
    PeakFreaksFragment *nNEFragment = new PeakFreaksFragment();
    nNEFragment->SetLifetime(cLifetime);
    nNEFragment->SetImage(Images.Data.rDustParticle);
    nNEFragment->SetPosition(cBlockX + ScatterWithinRange(cPosOff, cPosSct), cBlockY - ScatterWithinRange(cPosOff, cPosSct));
    nNEFragment->SetSpeeds(ScatterWithinRange(cSpdOff, cSpdSct), ScatterWithinRange(-cSpdOff, cSpdSct));
    mWorldFragmentList->AddElement("X", nNEFragment, PeakFreaksFragment::DeleteThis);

    //--SE Fragment.
    PeakFreaksFragment *nSEFragment = new PeakFreaksFragment();
    nSEFragment->SetLifetime(cLifetime);
    nSEFragment->SetImage(Images.Data.rDustParticle);
    nSEFragment->SetPosition(cBlockX + ScatterWithinRange(cPosOff, cPosSct), cBlockY + ScatterWithinRange(cPosOff, cPosSct));
    nSEFragment->SetSpeeds(ScatterWithinRange(cSpdOff, cSpdSct), ScatterWithinRange(cSpdOff, cSpdSct));
    mWorldFragmentList->AddElement("X", nSEFragment, PeakFreaksFragment::DeleteThis);

    //--SW Fragment.
    PeakFreaksFragment *nSWFragment = new PeakFreaksFragment();
    nSWFragment->SetLifetime(cLifetime);
    nSWFragment->SetImage(Images.Data.rDustParticle);
    nSWFragment->SetPosition(cBlockX - ScatterWithinRange(cPosOff, cPosSct), cBlockY + ScatterWithinRange(cPosOff, cPosSct));
    nSWFragment->SetSpeeds(ScatterWithinRange(-cSpdOff, cSpdSct), ScatterWithinRange(cSpdOff, cSpdSct));
    mWorldFragmentList->AddElement("X", nSWFragment, PeakFreaksFragment::DeleteThis);
}

///======================================= Core Methods ===========================================
void PeakFreaksLevel::SpawnCloud()
{
    ///--[Documentation]
    //--Spawns a cloud in a random spot on the map. This is used when the object initializes to give
    //  the impression that clouds were already flying in the background.
    PFTitle_Cloud *nCloud = (PFTitle_Cloud *)starmemoryalloc(sizeof(PFTitle_Cloud));
    nCloud->Initialize();

    //--Roll a random Y position.
    nCloud->mYPos = (rand() % (PFTITLE_CLOUD_Y_HI - PFTITLE_CLOUD_Y_LO)) + PFTITLE_CLOUD_Y_LO;

    //--Select a cloud image.
    int tCloudSlot = rand() % PFTITLE_CLOUD_FRAMES;
    nCloud->rImage = Images.Data.rClouds[tCloudSlot];

    //--Randomize the X position.
    nCloud->mXPos = rand() % (int)VIRTUAL_CANVAS_X;

    //--Roll a speed.
    nCloud->mXSpeed = ((rand() % (PFTITLE_CLOUD_SPEED_HI - PFTITLE_CLOUD_SPEED_LO)) + PFTITLE_CLOUD_SPEED_LO) / 10.0f;

    //--Register.
    mCloudList->AddElement("X", nCloud, &FreeThis);
}
void PeakFreaksLevel::SpawnCloudEdge()
{
    ///--[Documentation]
    //--Spawns a cloud off the left edge of the map. This allows it to scroll naturally onto the field.
    PFTitle_Cloud *nCloud = (PFTitle_Cloud *)starmemoryalloc(sizeof(PFTitle_Cloud));
    nCloud->Initialize();

    //--Roll a random Y position.
    nCloud->mYPos = (rand() % (PFTITLE_CLOUD_Y_HI - PFTITLE_CLOUD_Y_LO)) + PFTITLE_CLOUD_Y_LO;

    //--Select a cloud image.
    int tCloudSlot = rand() % PFTITLE_CLOUD_FRAMES;
    nCloud->rImage = Images.Data.rClouds[tCloudSlot];

    //--Resolve the X position. The cloud needs to be just off frame to the left.
    nCloud->mXPos = nCloud->rImage->GetWidth() * -1;

    //--Roll a speed.
    nCloud->mXSpeed = ((rand() % (PFTITLE_CLOUD_SPEED_HI - PFTITLE_CLOUD_SPEED_LO)) + PFTITLE_CLOUD_SPEED_LO) / 10.0f;

    //--Register.
    mCloudList->AddElement("X", nCloud, &FreeThis);
}
void PeakFreaksLevel::RandomizePuzzleGrid()
{
    ///--[Documentation]
    //--Sets all puzzle pieces in the grid to random values immediately. There must always be at least
    //  three of each direction on the grid.
    //--To make this happen, we first place the mandatory elements at random, and then fill the rest
    //  of the grid with random tiles.
    //--First, clear the grid.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            mPuzzleGrid[x][y]->SetPuzzleType(PEAKFREAK_PUZZLE_TYPE_NONE);
            mPuzzleGrid[x][y]->SetPower(0);
        }
    }

    ///--[Mandated Pieces]
    //--Next, iterate across the piece types.
    for(int i = 0; i < PEAKFREAK_PUZZLE_TYPE_MAX; i ++)
    {
        //--Place three of each type.
        for(int p = 0; p < 3; p ++)
        {
            //--Select a random X/Y position to start.
            int tX = rand() % PLL_PUZZLE_SIZE_X;
            int tY = rand() % PLL_PUZZLE_SIZE_Y;

            //--Other variables.
            int tXIterations = PLL_PUZZLE_SIZE_X;
            int tYIterations = PLL_PUZZLE_SIZE_Y;

            //--As long as the given spot is occupied, attempt to place in adjacent spots.
            while(mPuzzleGrid[tX][tY]->GetPuzzleType() != PEAKFREAK_PUZZLE_TYPE_NONE)
            {
                //--If there are X increments left, do that:
                if(tXIterations > 0)
                {
                    tXIterations --;
                    tX = (tX + 1) % PLL_PUZZLE_SIZE_X;
                    continue;
                }

                //--If we ran out of X iterations without finding an open slot, move a row.
                if(tYIterations > 0)
                {
                    tYIterations --;
                    tXIterations = PLL_PUZZLE_SIZE_X;
                    tY = (tY + 1) % PLL_PUZZLE_SIZE_Y;
                    continue;
                }

                //--If we somehow ran out of X and Y iterations, the grid is too small to handle
                //  all the pieces. Stop.
                break;
            }

            //--We have found an unused spot.
            mPuzzleGrid[tX][tY]->SetPuzzleType(i);
            mPuzzleGrid[tX][tY]->SetPosition(PLL_PUZZLE_X + (tX * PLL_PUZZLE_W), PLL_PUZZLE_Y + (tY * PLL_PUZZLE_H));
        }
    }

    ///--[Randomized Pieces]
    //--Now place pieces in all unoccupied slots, at random.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            //--Slot is occupied:
            if(mPuzzleGrid[x][y]->GetPuzzleType() != PEAKFREAK_PUZZLE_TYPE_NONE) continue;

            //--Roll.
            int tPuzzleType = rand() % PEAKFREAK_PUZZLE_TYPE_MAX;

            //--Set.
            mPuzzleGrid[x][y]->SetPuzzleType(tPuzzleType);
            mPuzzleGrid[x][y]->SetPosition(PLL_PUZZLE_X + (x * PLL_PUZZLE_W), PLL_PUZZLE_Y + (y * PLL_PUZZLE_H));
        }
    }
}
void PeakFreaksLevel::MovePuzzleGridUp()
{
    ///--[Documentation]
    //--Orders all puzzle pieces to move up by one.
    float cYBot = PLL_PUZZLE_Y + (PLL_PUZZLE_H * PLL_PUZZLE_SIZE_Y);

    //--The zeroth piece is held in storage.
    PeakFreaksPuzzlePiece *rZeroPieces[PLL_PUZZLE_SIZE_X];

    //--Move all pieces down one.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        //--Store the zero piece.
        rZeroPieces[x] = mPuzzleGrid[x][0];

        //--Compute X position.
        float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);

        //--Shift.
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y-1; y ++)
        {
            //--Compute positions.
            float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);

            //--Change values.
            mPuzzleGrid[x][y] = mPuzzleGrid[x][y+1];
            mPuzzleGrid[x][y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
        }

        //--Replace the last piece. It snaps in the bottom.
        mPuzzleGrid[x][PLL_PUZZLE_SIZE_Y-1] = rZeroPieces[x];
        rZeroPieces[x]->SetPosition(cXPos, cYBot);
        rZeroPieces[x]->MoveTo(cXPos, cYBot - PLL_PUZZLE_H, PLL_PUZZLE_MOVE_TICKS);
    }
}
void PeakFreaksLevel::MovePuzzleGridDn()
{
    ///--[Documentation]
    //--Orders all puzzle pieces to move down by one.
    float cYTop = PLL_PUZZLE_Y;

    //--The last piece is held in storage.
    PeakFreaksPuzzlePiece *rLastPieces[PLL_PUZZLE_SIZE_Y];

    //--Move all pieces up one.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        //--Store the last piece.
        rLastPieces[x] = mPuzzleGrid[x][PLL_PUZZLE_SIZE_Y-1];

        //--Compute X position.
        float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);

        //--Shift.
        for(int y = PLL_PUZZLE_SIZE_Y-1; y >= 1; y --)
        {
            mPuzzleGrid[x][y] = mPuzzleGrid[x][y-1];
            mPuzzleGrid[x][y]->MoveTo(cXPos, PLL_PUZZLE_Y + (y * PLL_PUZZLE_H), PLL_PUZZLE_MOVE_TICKS);
        }

        //--Replace the last piece.
        mPuzzleGrid[x][0] = rLastPieces[x];
        rLastPieces[x]->SetPosition(cXPos, cYTop - PLL_PUZZLE_H);
        rLastPieces[x]->MoveTo(cXPos, cYTop, PLL_PUZZLE_MOVE_TICKS);
    }
}
void PeakFreaksLevel::MovePuzzleGridLf()
{
    ///--[Documentation]
    //--Orders all puzzle pieces in the movement column to move left one slot.
    float cXRgt = PLL_PUZZLE_X + (PLL_PUZZLE_W * PLL_PUZZLE_SIZE_X);

    //--Storage for zeroth pieces.
    PeakFreaksPuzzlePiece *rZeroPieces[PLL_PUZZLE_SIZE_Y];

    //--Move all pieces down one.
    for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
    {
        //--Store the zero piece.
        rZeroPieces[y] = mPuzzleGrid[0][y];

        //--Compute Y position.
        float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);

        //--Shift.
        for(int x = 0; x < PLL_PUZZLE_SIZE_X-1; x ++)
        {
            mPuzzleGrid[x][y] = mPuzzleGrid[x+1][y];
            mPuzzleGrid[x][y]->MoveTo(PLL_PUZZLE_X + (x * PLL_PUZZLE_W), cYPos, PLL_PUZZLE_MOVE_TICKS);
        }

        //--Replace the last piece. It snaps in the bottom.
        mPuzzleGrid[PLL_PUZZLE_SIZE_X-1][y] = rZeroPieces[y];
        rZeroPieces[y]->SetPosition(cXRgt, cYPos);
        rZeroPieces[y]->MoveTo(cXRgt - PLL_PUZZLE_W, cYPos, PLL_PUZZLE_MOVE_TICKS);
    }
}
void PeakFreaksLevel::MovePuzzleGridRt()
{
    ///--[Documentation]
    //--Orders all puzzle pieces to move one slot to the right.
    float cXLft = PLL_PUZZLE_X;

    //--The last piece is held in storage.
    PeakFreaksPuzzlePiece *rLastPieces[PLL_PUZZLE_SIZE_Y];

    //--Move all pieces right one.
    for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
    {
        //--Store the last piece.
        rLastPieces[y] = mPuzzleGrid[PLL_PUZZLE_SIZE_X-1][y];

        //--Compute Y position.
        float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);

        //--Shift.
        for(int x = PLL_PUZZLE_SIZE_X-1; x >= 1; x --)
        {
            mPuzzleGrid[x][y] = mPuzzleGrid[x-1][y];
            mPuzzleGrid[x][y]->MoveTo(PLL_PUZZLE_X + (x * PLL_PUZZLE_W), cYPos, PLL_PUZZLE_MOVE_TICKS);
        }

        //--Replace the last piece. It snaps to the top.
        mPuzzleGrid[0][y] = rLastPieces[y];
        rLastPieces[y]->SetPosition(cXLft - PLL_PUZZLE_W, cYPos);
        rLastPieces[y]->MoveTo(cXLft, cYPos, PLL_PUZZLE_MOVE_TICKS);
    }
}
void PeakFreaksLevel::RebalanceList(SugarLinkedList *pList)
{
    ///--[Documentation]
    //--Given a linked list containing a series of (PeakFreaksPuzzlePiece *), causes the board to
    //  have at least 3 of each available type of block. The entries in the list will have their
    //  members modified as needed to make sure the minimums are present.
    if(!pList) return;

    //--Initial counts.
    int tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_MAX];
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_DN] = 3;
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_UP] = 3;
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_LF] = 3;
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_RT] = 3;

    //--Count.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            int tType = mPuzzleGrid[x][y]->GetPuzzleType();
            tCountsNeeded[tType] --;
        }
    }

    //--Run across the counts array. If a count needs increasing, change a block in the rebalance list.
    for(int i = 0; i < PEAKFREAK_PUZZLE_TYPE_MAX; i ++)
    {
        for(int p = 0; p < tCountsNeeded[i]; p ++)
        {
            //--If the rebalance list has run out of elements, stop.
            int tListSize = pList->GetListSize();
            if(tListSize < 1) return;

            //--Extract an element at random.
            int tSlot = rand() % tListSize;
            PeakFreaksPuzzlePiece *rPiece = (PeakFreaksPuzzlePiece *)pList->GetElementBySlot(tSlot);
            rPiece->SetPuzzleType(i);

            //--Remove the piece from the list.
            pList->RemoveElementI(tSlot);
        }
    }
}
void PeakFreaksLevel::RebalanceGrid()
{
    ///--[Documentation]
    //--Similar to above, except scans the grid for PEAKFREAK_PUZZLE_TYPE_NONE and replaces them
    //  with new puzzle pieces while meeting required minimums.

    //--Initial counts.
    int tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_MAX];
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_DN] = 3;
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_UP] = 3;
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_LF] = 3;
    tCountsNeeded[PEAKFREAK_PUZZLE_TYPE_ARROW_RT] = 3;

    //--Count.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            int tType = mPuzzleGrid[x][y]->GetPuzzleType();
            if(tType == PEAKFREAK_PUZZLE_TYPE_NONE) continue;
            tCountsNeeded[tType] --;
        }
    }

    //--Now run across the grid again. Replace the NONE types with needed types.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            //--Get and check type.
            int tType = mPuzzleGrid[x][y]->GetPuzzleType();
            if(tType != PEAKFREAK_PUZZLE_TYPE_NONE) continue;

            //--Scan across the puzzle types. If any are over zero, set this block to that type.
            bool tSetType = false;
            for(int i = 0; i < PEAKFREAK_PUZZLE_TYPE_MAX; i ++)
            {
                if(tCountsNeeded[i] > 0)
                {
                    tSetType = true;
                    tCountsNeeded[i] = 0;
                    mPuzzleGrid[x][y]->SetPuzzleType(i);
                }
            }

            //--If the type was not set by mandate, randomize.
            if(!tSetType)
            {
                mPuzzleGrid[x][y]->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
            }
        }
    }
}
void PeakFreaksLevel::SpawnPowerups()
{
    ///--[Documentation]
    //--Spawns powerups once the level has finished reconstituting. The level is broken into grid
    //  pieces, similar to when it is reconstituting, and these grid pieces each will spawn up to 6
    //  coins and 2 powerups per piece.
    //--To determine where these can spawn, the collision depth of 2 is tested. Valid spots must have
    //  a collision on this depth. If not enough spots to spawn are found, then nothing spawns there.
    if(mCollisionLayersTotal < 2) return;

    ///--[Setup]
    int tXSize = mCollisionLayers[2].mCollisionSizeX;
    int tYSize = mCollisionLayers[2].mCollisionSizeY;

    ///--[Chunk Iterate]
    //--Iterate across the grid chunks. Each chunk spawns its own items.
    for(int x = 0; x < tXSize; x += CHUNK_W)
    {
        for(int y = 0; y < tYSize; y += CHUNK_H)
        {
            SpawnPowerupsInChunk(x, y);
        }
    }

    ///--[Debug]
    //--Powerups that spawn next to the player for testing.
    ZonePack *rSpawnPosition = (ZonePack *)mPlayerStartZones->GetElementBySlot(0);
    if(rSpawnPosition && false)
    {
        //--Drone Strike.
        ZonePack *nPackage = new ZonePack();
        nPackage->mLayer = 0;
        nPackage->mSubtype = PFLEVEL_COIN_SUBTYPE_DRONESTRIKE;
        nPackage->mDim.SetWH(rSpawnPosition->mDim.mLft - 16.0f, rSpawnPosition->mDim.mTop-48.0f, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);

        //--Guano Bomb.
        nPackage = new ZonePack();
        nPackage->mLayer = 0;
        nPackage->mSubtype = PFLEVEL_COIN_SUBTYPE_GUANOBOMB;
        nPackage->mDim.SetWH(rSpawnPosition->mDim.mLft - 32.0f, rSpawnPosition->mDim.mTop-48.0f, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);

        //--Super Drone Strike.
        nPackage = new ZonePack();
        nPackage->mLayer = 0;
        nPackage->mSubtype = PFLEVEL_COIN_SUBTYPE_SUPERSTRIKE;
        nPackage->mDim.SetWH(rSpawnPosition->mDim.mLft - 48.0f, rSpawnPosition->mDim.mTop-48.0f, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);

        //--Ice Beam.
        nPackage = new ZonePack();
        nPackage->mLayer = 0;
        nPackage->mSubtype = PFLEVEL_COIN_SUBTYPE_ICEBEAM;
        nPackage->mDim.SetWH(rSpawnPosition->mDim.mLft - 16.0f, rSpawnPosition->mDim.mTop - 32.0f, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);

        //--Canyon Bar.
        nPackage = new ZonePack();
        nPackage->mLayer = 0;
        nPackage->mSubtype = PFLEVEL_COIN_SUBTYPE_CANYONBAR;
        nPackage->mDim.SetWH(rSpawnPosition->mDim.mLft - 32.0f, rSpawnPosition->mDim.mTop - 32.0f, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);

        //--Heli Lift.
        nPackage = new ZonePack();
        nPackage->mLayer = 0;
        nPackage->mSubtype = PFLEVEL_COIN_SUBTYPE_HELILIFT;
        nPackage->mDim.SetWH(rSpawnPosition->mDim.mLft - 48.0f, rSpawnPosition->mDim.mTop - 32.0f, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);

        //--Coin.
        nPackage = new ZonePack();
        nPackage->mLayer = 0;
        nPackage->mSubtype = PFLEVEL_COIN_SUBTYPE_COIN;
        nPackage->mDim.SetWH(rSpawnPosition->mDim.mLft - 16.0f, rSpawnPosition->mDim.mTop - 16.0f, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);
    }
}
void PeakFreaksLevel::SpawnPowerupsInChunk(int pStartX, int pStartY)
{
    ///--[Documentation]
    //--For a given chunk of the level, spawns powerups.
    if(mCollisionLayersTotal < 2) return;

    ///--[List Assembly]
    //--Create a list of legal locations to spawn a powerup in this chunk.
    SugarLinkedList *tLocationList = new SugarLinkedList(true); //TwoDimensionRealPoint *, master

    //--Populate.
    for(int x = 0; x < CHUNK_W; x ++)
    {
        for(int y = 0; y < CHUNK_H; y ++)
        {
            //--Position is clipped: Add it.
            if(mCollisionLayers[2].GetClipAt(pStartX + x, pStartY + y))
            {
                TwoDimensionRealPoint *nPoint = (TwoDimensionRealPoint *)starmemoryalloc(sizeof(TwoDimensionRealPoint));
                nPoint->mXCenter = x;
                nPoint->mYCenter = y;
                tLocationList->AddElement("X", nPoint, &FreeThis);
            }
        }
    }

    //--If the list has no locations in it, we can stop here. This is an empty chunk.
    if(tLocationList->GetListSize() < 1)
    {
        delete tLocationList;
        return;
    }

    ///--[Spawn Coins]
    //--Spawn six coins in the available slots.
    int tCoinsToSpawn = 6;
    while(tCoinsToSpawn > 0)
    {
        //--Subtract one from the count even if there are no spots available.
        tCoinsToSpawn --;

        //--If there are no spots in the list, stop.
        int tListLen = tLocationList->GetListSize();
        if(tListLen < 1)
        {
            break;
        }

        //--Select a location from the list. Spawn a coin there.
        int tSlot = rand() % tListLen;
        TwoDimensionRealPoint *rSpawnPosition = (TwoDimensionRealPoint *)tLocationList->GetElementBySlot(tSlot);
        if(rSpawnPosition)
        {
            //--Create.
            ZonePack *nPackage = new ZonePack();
            nPackage->mLayer = 0;

            //--Compute position.
            nPackage->mDim.mLft = ((pStartX + rSpawnPosition->mXCenter) * TileLayer::cxSizePerTile);
            nPackage->mDim.mTop = ((pStartY + rSpawnPosition->mYCenter) * TileLayer::cxSizePerTile);
            nPackage->mDim.mRgt = nPackage->mDim.mLft + TileLayer::cxSizePerTile;
            nPackage->mDim.mBot = nPackage->mDim.mTop + TileLayer::cxSizePerTile;
            nPackage->mDim.mXCenter = (nPackage->mDim.mLft + nPackage->mDim.mRgt) / 2.0f;
            nPackage->mDim.mYCenter = (nPackage->mDim.mTop + nPackage->mDim.mBot) / 2.0f;

            //--Register.
            mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);
        }

        //--Remove that element from the list.
        tLocationList->RemoveElementI(tSlot);
    }

    ///--[Spawn Powerups]
    //--Same as above, but spawns powerups. No particular powerup is favoured. Note that powerups go on the same
    //  list as the coins do. They function the same for the most part.
    int tPowerupsToSpawn = 2;
    while(tPowerupsToSpawn > 0)
    {
        //--Subtract one from the count even if there are no spots available.
        tPowerupsToSpawn --;

        //--If there are no spots in the list, stop.
        int tListLen = tLocationList->GetListSize();
        if(tListLen < 1)
        {
            break;
        }

        //--Select a location from the list. Spawn a powerup there.
        int tSlot = rand() % tListLen;
        TwoDimensionRealPoint *rSpawnPosition = (TwoDimensionRealPoint *)tLocationList->GetElementBySlot(tSlot);
        if(rSpawnPosition)
        {
            //--Create.
            ZonePack *nPackage = new ZonePack();
            nPackage->mLayer = 0;

            //--Roll the subtype.
            nPackage->mSubtype = rand() % (PFLEVEL_COIN_SUBTYPE_ROLL_HI - PFLEVEL_COIN_SUBTYPE_ROLL_LOW) + PFLEVEL_COIN_SUBTYPE_ROLL_LOW;

            //--Compute position.
            nPackage->mDim.mLft = ((pStartX + rSpawnPosition->mXCenter) * TileLayer::cxSizePerTile);
            nPackage->mDim.mTop = ((pStartY + rSpawnPosition->mYCenter) * TileLayer::cxSizePerTile);
            nPackage->mDim.mRgt = nPackage->mDim.mLft + TileLayer::cxSizePerTile;
            nPackage->mDim.mBot = nPackage->mDim.mTop + TileLayer::cxSizePerTile;
            nPackage->mDim.mXCenter = (nPackage->mDim.mLft + nPackage->mDim.mRgt) / 2.0f;
            nPackage->mDim.mYCenter = (nPackage->mDim.mTop + nPackage->mDim.mBot) / 2.0f;

            //--Register.
            mCoinZones->AddElement("X", nPackage, &ZonePack::DeleteThis);
        }

        //--Remove that element from the list.
        tLocationList->RemoveElementI(tSlot);
    }

    ///--[Clean]
    delete tLocationList;
}
void PeakFreaksLevel::BumpPlayerAbsolute(int pSrcX, int pSrcY, int pDstX, int pDstY)
{
    ///--[Documentation]
    //--If the player or rival is in the given tile, bump them to the destination tile.
    CollisionPack *rZeroLayer = GetCollisionPack(0);
    if(!rZeroLayer) return;

    //--Player.
    PeakFreaksPlayer *rPlayer = GetPlayer();
    if(rPlayer && !rPlayer->IsIgnoringHazards())
    {
        //--Get position.
        int tPlayerWorldX = (int)rPlayer->GetWorldX();
        int tPlayerWorldY = (int)rPlayer->GetWorldY();

        //--Match. Bump them.
        if(tPlayerWorldX == pSrcX && tPlayerWorldY == pSrcY)
        {
            //--Compute the difference.
            int tXDif = pDstX - tPlayerWorldX;
            int tYDif = pDstY - tPlayerWorldY;

            //--Bump the player.
            rPlayer->QueueBump(tXDif, tYDif);
        }
    }

    //--Rival.
    PeakFreaksPlayer *rRival = GetRival();
    if(rRival && !rRival->IsIgnoringHazards())
    {
        //--Rival's position:
        int tRivalWorldX = (int)rRival->GetWorldX();
        int tRivalWorldY = (int)rRival->GetWorldY();

        //--Match. Bump them.
        if(tRivalWorldX == pSrcX && tRivalWorldY == pSrcY)
        {
            //--Compute the difference.
            int tXDif = pDstX - tRivalWorldX;
            int tYDif = pDstY - tRivalWorldY;

            //--Bump the player.
            rRival->QueueBump(tXDif, tYDif);
        }
    }
}
void PeakFreaksLevel::BumpPlayerRelative(int pSrcX, int pSrcY, int pDstX, int pDstY)
{

}
void PeakFreaksLevel::HandlePlayerPowerup(void *rCallingEntity, int pCode)
{
    ///--[Documentation]
    //--Spawns a powerup indicator and sets any powerup information needed. The calling entity provides
    //  its pointer so we can check if it was the player or rival that activated a powerup.
    SpawnBadge((rCallingEntity == mRival), pCode);

    //--Cast.
    PeakFreaksPlayer *rCallingCast = (PeakFreaksPlayer *)rCallingEntity;

    ///--[Coin]
    //--Spawns particles.
    if(pCode == PFLEVEL_COIN_SUBTYPE_COIN)
    {
        //--Must be the player.
        if(rCallingEntity == mRival) return;

        //--Player must exist.
        if(!mPlayer) return;

        //--Get the player's position, subtract the camera offset.
        float tWorldToUIX = 635.0f;
        float tWorldToUIY = 386.0f;
        SpawnSpeedParticle(tWorldToUIX, tWorldToUIY,  0);
        SpawnSpeedParticle(tWorldToUIX, tWorldToUIY, -1);
        SpawnSpeedParticle(tWorldToUIX, tWorldToUIY, -2);
        SpawnSpeedParticle(tWorldToUIX, tWorldToUIY, -3);

        return;
    }

    //--Ice Beam. Stops hazards.
    if(pCode == PFLEVEL_COIN_SUBTYPE_ICEBEAM)
    {
        //--Call.
        ActivateIceBeam();

        //--Caller is happy.
        if(rCallingCast) rCallingCast->MakeHappy();
    }
    //--Canyon Bar, temporary invincibility.
    else if(pCode == PFLEVEL_COIN_SUBTYPE_CANYONBAR)
    {
        if(rCallingCast) rCallingCast->ActivateCanyonBar();
    }
    //--Heli-Lift. Instantly jumps the player to the next checkpoint.
    else if(pCode == PFLEVEL_COIN_SUBTYPE_HELILIFT)
    {
        //--Get which string to check.
        const char *rCheckString = mActiveCheckpointName;
        if(rCallingEntity == mRival) rCheckString = mRivalActiveCheckpointName;

        //--Get the slot of the checkpoint on the checkpoint list. If it came back invalid, use the
        //  lowest slot. This happens if the player hasn't tripped any checkpoints yet.
        int tCheckSlot = mCheckpointZones->GetSlotOfElementByName(rCheckString);
        if(tCheckSlot == -1) tCheckSlot = -1;
        //fprintf(stderr, "Checkpoint was in slot %i out of %i\n", tCheckSlot, mCheckpointZones->GetListSize());

        //--Subtract one from the check slot, and get that checkpoint. If it's out of range, there are no
        //  more checkpoints between the player and the goal, so do nothing.
        ZonePack *rPackage = (ZonePack *)mCheckpointZones->GetElementBySlot(tCheckSlot + 1);
        if(!rPackage) return;

        //--Debug.
        //fprintf(stderr, "Jump target: %i %i\n", (int)(rPackage->mDim.mLft / TileLayer::cxSizePerTile), (int)(rPackage->mDim.mTop / TileLayer::cxSizePerTile));

        //--Order the player/rival to immediately clear their queue and instead perform a jump to the
        //  checkpoint from wherever they are.
        if(rCallingEntity == mPlayer)
        {
            int tXDif = (int)(rPackage->mDim.mLft / TileLayer::cxSizePerTile - mPlayer->GetWorldX());
            int tYDif = (int)(rPackage->mDim.mTop / TileLayer::cxSizePerTile - mPlayer->GetWorldY());
            //fprintf(stderr, "Player Pos: %i %i\n", (int)mPlayer->GetWorldX(), (int)mPlayer->GetWorldY());
            //fprintf(stderr, "Dif: %i %i\n", tXDif, tYDif);
            mPlayer->QueueBump(tXDif, tYDif);
            mPlayer->ActivateHelicopter();
        }
        else if(rCallingEntity == mRival)
        {
            int tXDif = mRival->GetWorldX() - (int)(rPackage->mDim.mLft / TileLayer::cxSizePerTile);
            int tYDif = mRival->GetWorldY() - (int)(rPackage->mDim.mTop / TileLayer::cxSizePerTile);
            mRival->QueueBump(tXDif, tYDif);
            mRival->ActivateHelicopter();
        }
    }
    //--Drone Strikes. Drops a bomb on the enemy. Handles Guano too.
    else if(pCode == PFLEVEL_COIN_SUBTYPE_DRONESTRIKE || pCode == PFLEVEL_COIN_SUBTYPE_GUANOBOMB || pCode == PFLEVEL_COIN_SUBTYPE_SUPERSTRIKE)
    {
        //--Resolve target position.
        float tTargetX = mPlayer->GetWorldX() * TileLayer::cxSizePerTile;
        float tTargetY = mPlayer->GetWorldY() * TileLayer::cxSizePerTile;
        if(rCallingEntity == mPlayer)
        {
            //--Happy time.
            if(mPlayer) mPlayer->MakeHappy();

            //--Target rival.
            if(mRival)
            {
                tTargetX = mRival->GetWorldX() * TileLayer::cxSizePerTile;
                tTargetY = mRival->GetWorldY() * TileLayer::cxSizePerTile;
                mRival->Shock();
            }
            //--No rival. Aim at the spot next to the player, for testing.
            else
            {
                tTargetX = tTargetX + 32.0f;
            }
        }
        else
        {
            //--Happy time.
            if(mRival) mRival->MakeHappy();

            //--Target player.
            if(mPlayer)
            {
                tTargetX = mPlayer->GetWorldX() * TileLayer::cxSizePerTile;
                tTargetY = mPlayer->GetWorldY() * TileLayer::cxSizePerTile;
                mPlayer->Shock();
            }
            //--No rival. Aim at the spot next to the player, for testing.
            else
            {
                tTargetX = tTargetX + 32.0f;
            }
        }

        //--Create.
        PeakFreaksFlyby *nFlyby = new PeakFreaksFlyby();
        nFlyby->SetTargetPosition(tTargetX, tTargetY);
        nFlyby->SetEndingPosition(tTargetX + 320.0f);

        //--Graphics.
        if(pCode == PFLEVEL_COIN_SUBTYPE_DRONESTRIKE)
        {
            nFlyby->SetSubtype(PFFB_SUBTYPE_BOMB);
            nFlyby->SetImages(Images.Data.rPowerupDroneImg0, Images.Data.rPowerupDroneImg1);
        }
        else if(pCode == PFLEVEL_COIN_SUBTYPE_GUANOBOMB)
        {
            nFlyby->SetSubtype(PFFB_SUBTYPE_GUANO);
            nFlyby->SetImages(Images.Data.rPowerupGuanoBird, Images.Data.rPowerupGuanoBird);
        }
        else if(pCode == PFLEVEL_COIN_SUBTYPE_SUPERSTRIKE)
        {
            nFlyby->SetSubtype(PFFB_SUBTYPE_SUPERBOMB);
            nFlyby->SetImages(Images.Data.rPowerupDroneImg0, Images.Data.rPowerupDroneImg1);
        }

        //--Register.
        mFlybyList->AddElement("X", nFlyby, PeakFreaksFlyby::DeleteThis);
    }
    //--Unhandled case, stop here to prevent a sound effect from playing.
    else
    {
        return;
    }

    ///--[SFX]
    if(rCallingEntity == mRival)
    {
        AudioManager::Fetch()->PlaySound("PF Announce LookOut");
    }
    else
    {
        AudioManager::Fetch()->PlaySound("PF Announce Powerup");
    }
}
void PeakFreaksLevel::HandlePlayerProgress(void *rCallingEntity, int pX, int pY)
{
    ///--[Documentation]
    //--Handles updating the player/rival's progress values based on where they are in the level. The
    //  entity calls this during it's update if it is in need of an update, that is, when it's not getting
    //  hit by something.
    if(!mTileProgressValues) return;

    //--Range-check the progress value.
    if(pX < 0 || pX >= mProgressSizeX) return;
    if(pY < 0 || pY >= mProgressSizeY) return;
    float tProgressValue = mTileProgressValues[pX][pY];

    //--Check the entity.
    if(rCallingEntity == mPlayer)
    {
        mPlayerTrackProgress = tProgressValue;
    }
    else
    {
        mRivalTrackProgress = tProgressValue;
    }
}

///=================================== Private Core Methods =======================================
///========================================= File I/O =============================================
///======================================= Pointer Routing ========================================
PeakFreaksPlayer *PeakFreaksLevel::GetPlayer()
{
    return mPlayer;
}
PeakFreaksPlayer *PeakFreaksLevel::GetRival()
{
    return mRival;
}
ZonePack *PeakFreaksLevel::GetPlayerStart(int pLayer)
{
    //--Checks the player starts for a specific layer. Returns that zone if found.
    ZonePack *rPlayerStartZone = (ZonePack *)mPlayerStartZones->PushIterator();
    while(rPlayerStartZone)
    {
        //--Zero depth, player start position.
        if(rPlayerStartZone->mLayer == pLayer)
        {
            mPlayerStartZones->PopIterator();
            return rPlayerStartZone;
        }

        //--Next.
        rPlayerStartZone = (ZonePack *)mPlayerStartZones->AutoIterate();
    }

    //--Not found.
    return NULL;
}

///===================================== Static Functions =========================================
PeakFreaksLevel *PeakFreaksLevel::Fetch()
{
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_PEAKFREAKSLEVEL)) return NULL;
    return (PeakFreaksLevel *)rCheckLevel;
}
