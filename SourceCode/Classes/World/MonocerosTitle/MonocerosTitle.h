///====================================== MonocerosTitle ==========================================
//--Title screen for Project Monoceros (Witch Hunter Izana). Replaces the existing game selector
//  at startup. The game is technically a mod so it can still be accessed via the mod interface
//  but using this class allows more direct access.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "FlexMenu.h"
#include "StarBitmap.h"
struct RootLump;

///===================================== Local Structures =========================================
///--[LanguagePack]
//--Represents a language on the language selection screen. Contains a hitbox to click it, the language,
//  and a flag to represent it.
typedef struct LanguagePack
{
    //--Members
    TwoDimensionReal mPosition;
    char *mLanguageCode;
    char *mFolderPath;
    char *mExtension;
    StarBitmap *mFlag;
    StarBitmap *mRestartProgram;

    //--Functions
    void Initialize()
    {
        mPosition.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
        mLanguageCode = NULL;
        mFolderPath = NULL;
        mExtension = NULL;
        mFlag = NULL;
        mRestartProgram = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        //--Cast and check.
        LanguagePack *rPtr = (LanguagePack *)pPtr;
        if(!pPtr) return;

        //--Deallocate.
        free(rPtr->mLanguageCode);
        free(rPtr->mFolderPath);
        free(rPtr->mExtension);
        free(rPtr);
    }
}LanguagePack;

///===================================== Local Definitions ========================================
#define MONOTITLE_OSCILLATE_TICKS 120
#define MONOTITLE_OSCILLATE_DISTANCE 4.0f
#define MONOTITLE_SHADE_TICKS 600
#define MONOTITLE_LANGUAGE_TICKS 15

#define MONOTITLE_MODS_PER_PAGE 11
#define MONOTITLE_BUTTONS_REMOVE_WHEN_LANGUAGE_CHANGE 4

///======================================== Translation ===========================================
///--[MonoTitle_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOTITLE_STRINGS_TOTAL 23
class MonoTitle_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOTITLE_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextSelectASaveFile;
            char *mTextNewGame;
            char *mTextRightClickAFile;
            char *mTextBack;
            char *mTextAutoSaves;
            char *mTextNormalSaves;
            char *mTextSaveMod;
            char *mTextChangesPending;
            char *mTextChangesModInstruct;
            char *mTextToggleAutosaves;
            char *mTextDeveloperMenu;
            char *mTextCreateNewPack;
            char *mTextLoadPack;
            char *mTextWorking;
            char *mTextSelectLanguage;
            char *mTextAskCreateNewPack;
            char *mTextNewPackagePath;
            char *mTextBackupPack;
            char *mTextConfirm;
            char *mTextCancel;
            char *mTextDeployPackage;
            char *mTextWarningOverwrite;
            char *mTextBackupFiles;
        }str;
    };

    //--Functions
    MonoTitle_Strings();
    virtual ~MonoTitle_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonocerosTitle : public FlexMenu
{
    protected:
    ///--[Constants]
    static const int cxLanguage_Nothing = 0;
    static const int cxLanguage_Create  = 1;
    static const int cxLanguage_Deploy  = 2;

    ///--[Variables]
    //--System
    int mTitleOverTimer;
    int mTitleShadeTimer;
    int mTitleOscillateTimer;
    StarlightColor mMonoTitle;
    StarlightColor mMonoSubtitle;
    StarlightColor mMonoParagraph;

    //--Music
    bool mHasStartedMusic;

    //--Options Menu
    bool mIsShowingOptions;
    MonocerosMenu *mOptionsMenu;

    //--Mod Load Order Menu
    bool mIsShowingModLoadOrder;
    bool mModChangesPending;
    int mModOffset;

    //--Other
    StarlightString *mToggleAutosaveKey;

    ///--[Languages]
    //--System
    bool mIsLanguageMenuActive;
    bool mIsLanguageSubmenuActive;

    //--Language Selection
    int mLanguageTimer;
    int mStartingLanguage;
    int mSelectedLanguage;
    TwoDimensionReal mHitbox_LanguageSubmenu;

    //--Language Pack Creation/Deployment
    int mLanguageHighlight;
    int mLanguageUseForPacks;
    bool mIsSelectingLanguagePack;
    bool mConfirmCreate;
    bool mConfirmDeploy;
    bool mCreateBackups;
    TwoDimensionReal mHitbox_Create;
    TwoDimensionReal mHitbox_Deploy;
    TwoDimensionReal mHitbox_Confirm;
    TwoDimensionReal mHitbox_Cancel;
    TwoDimensionReal mHitbox_Toggle;

    //--Execution
    bool mIsLanguageExecuting;
    int mExecutionTimer;

    //--Storage
    StarLinkedList *mLanguageList;//LanguagePack *, master

    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts.
            StarFont *rFontDoubleHeader;
            StarFont *rFontMenu;
            StarFont *rFontSaveStat;

            //--Images.
            StarBitmap *rBacking;
            StarBitmap *rButton_LanguageShowMore;
            StarBitmap *rFrameAutosavesButton;
            StarBitmap *rFrameBackButton;
            StarBitmap *rFrame_Confirm;
            StarBitmap *rFrameLanguage;
            StarBitmap *rFrame_LanguageCheck;
            StarBitmap *rFrame_LanguageEx;
            StarBitmap *rFrame_LanguageX;
            StarBitmap *rFrameModLoadButtons;
            StarBitmap *rFrameModLoadCheck;
            StarBitmap *rFrameModLoadX;
            StarBitmap *rFrameModLoadOrder;
            StarBitmap *rFrameModUploadButton;
            StarBitmap *rFrameNewGame;
            StarBitmap *rFrameNewGameSelect;
            StarBitmap *rFrameSaveCell;
            StarBitmap *rFrameSaveCellSelect;
            StarBitmap *rFrameSaveModButton;
            StarBitmap *rNotification;
            StarBitmap *rOverlayFlagSelect;
            StarBitmap *rOverlayFlagUK;
            StarBitmap *rOverlayRestart;
            StarBitmap *rOverlaySaveShade;
            StarBitmap *rOverlaySelection;
            StarBitmap *rOverlayShading;
            StarBitmap *rOverlayTitle;
            StarBitmap *rScrollbarBack;
            StarBitmap *rScrollbarFront;
            StarBitmap *rScrollbarModsBack;
        }Data;
    }MonoImg;

    protected:

    public:
    //--System
    MonocerosTitle();
    virtual ~MonocerosTitle();
    virtual void Construct();

    //--Public Variables
    static bool xAllowLanguages;
    static char *xVersionString;
    static bool xIsDeveloperMode;
    static MonoTitle_Strings *xrStringData;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    void ShowOptions();

    //--Core Methods
    virtual void Clear();
    virtual void AutoSortButtons();

    ///--[Languages]
    void SetStartingLanguage(const char *pString);
    void ChangeLanguage(const char *pString);
    void RegisterLanguage(const char *pLanguageName, const char *pExtension, const char *pFolderPath, const char *pFlagFilePath, const char *pRestartPath);
    bool UpdateLanguageMode();
    void RenderLanguageMode();

    ///--[Language Workers]
    void CreateLanguageDirectory(const char *pLanguageName, const char *pFolderPath, const char *pLuaSuffix);
    void CreateLanguagePack(const char *pLanguageName, const char *pFolderPath, const char *pLuaSuffix);
    void DeployLanguagePack(const char *pLanguageName, const char *pFolderPath);
    RootLump *CreateRootLumpFromFile(const char *pPath);
    void DeployFileFromLump(int pSlot);

    ///--[Mod Load Order]
    void ShowModLoadOrder();
    void MoveModDown(int pIndex);
    void MoveModUp(int pIndex);
    void ToggleModEnable(int pIndex);
    void UpdateModMode();
    void RenderModMode();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    virtual void UpdateLoadingMode();

    //--File I/O
    //--Drawing
    virtual void Render();
    virtual void RenderLoadingMode();

    //--Pointer Routing
    //--Static Functions
    static FlexMenu *GenerateMonocerosTitle();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_MT_SetProperty(lua_State *L);
