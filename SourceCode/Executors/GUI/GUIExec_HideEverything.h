///================================== GUIExec_HideEverything ======================================
//--Hides everything on the UI.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "GUI.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class GUIExec_HideEverything : public RootExecutor
{
    private:

    protected:

    public:
    //--System
    GUIExec_HideEverything()
    {

    }
    virtual ~GUIExec_HideEverything()
    {

    }
    virtual void Execute()
    {
        GraphicalUserInterface::Fetch()->HideEverything();
    }
};
