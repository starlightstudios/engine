//--Base
#include "AdvCombatDefStruct.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///--[System]
void CombatStatistics::Initialize()
{
    //--Just clear the values off, all defaults are zeroes. Identical to the Zero() call but this
    //  may change in the future.
    memset(mValueList, 0, sizeof(int) * STATS_TOTAL);
}
void CombatStatistics::Zero()
{
    //--Note: Ignores clamps. Be sure to clamp after setups if this is the final version of a stat.
    memset(mValueList, 0, sizeof(int) * STATS_TOTAL);
}

///--[Property Queries]
int CombatStatistics::GetStatByIndex(int pIndex)
{
    if(pIndex < 0 || pIndex >= STATS_TOTAL) return 0;
    return mValueList[pIndex];
}

///--[General Manipulators]
void CombatStatistics::SetStatByIndex(int pIndex, int pValue)
{
    if(pIndex < 0 || pIndex >= STATS_TOTAL) return;
    mValueList[pIndex] = pValue;
}

///--[Specific Manipulators]
void CombatStatistics::SetHPMax(int pValue)
{
    mValueList[STATS_HPMAX] = pValue;
}
void CombatStatistics::SetInitiative(int pValue)
{
    mValueList[STATS_INITIATIVE] = pValue;
}

///--[Core Methods]
void CombatStatistics::AddStatistics(CombatStatistics pPackage)
{
    for(int i = 0; i < STATS_TOTAL; i ++)
    {
        mValueList[i] += pPackage.mValueList[i];
    }
}
void CombatStatistics::ClampStatistics()
{
    //--Clamps all statistics. Should be used after adding together with other stat packs.
    if(mValueList[STATS_HPMAX] < 1) mValueList[STATS_HPMAX] = 1;
}
