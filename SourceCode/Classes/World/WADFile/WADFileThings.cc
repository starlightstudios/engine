//--Base
#include "WADFile.h"

//--Classes
#include "WorldLight.h"
#include "WorldPolygon.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "GlDfn.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CameraManager.h"
#include "DebugManager.h"

//--This file contains all the functions related to "Things". While necessarily vague, a "Thing" is anything
//  in Doom which is not considered part of the level geometry. This includes player spawns, objects, items,
//  guns, monsters, and anything else that is game-specific.
//--Things are not used that extensively by the rendering engine. Most items are ignored, we only need player
//  positions for setting the initial camera, and some of the lights are used. That's about it.

//#define THING_DEBUG

//--[Types]
//--These are specified in Doom's specifications, we can't modify them.
#define TYPE_PLAYER_START_1 1

#define TYPE_BLOOD_POOL_A 24
#define TYPE_ELECTRIC_POLE 48
#define TYPE_BLOOD_POOL_B 79
#define TYPE_BLOOD_POOL_C 80
#define TYPE_BRAIN_POOL 81
#define TYPE_FLOOR_LAMP 2028
#define TYPE_TALL_LAMP 85
#define TYPE_SHORT_LAMP 86
#define TYPE_CANDLE 34
#define TYPE_CANDELABRA 35
#define TYPE_SHORT_GREY_TREE 43
#define TYPE_STALAGMITE 47
#define TYPE_TALL_BLUE_FIRESTICK 44
#define TYPE_TALL_GREEN_FIRESTICK 45
#define TYPE_TALL_RED_FIRESTICK 46
#define TYPE_SHORT_BLUE_FIRESTICK 55
#define TYPE_SHORT_GREEN_FIRESTICK 56
#define TYPE_SHORT_RED_FIRESTICK 57
#define TYPE_BURNING_BARREL 70
#define TYPE_HEALTH_PICKUP 2014
#define TYPES_TOTAL 20

//--[System]
void WADFile::BuildThingLookups()
{
    //--Builds or rebuilds the lookups used to place things from the THINGS lump. Note that one entry, the player
    //  start, is not on this list. That's handled specially.
    mThingLookupsTotal = TYPES_TOTAL;
    SetMemoryData(__FILE__, __LINE__);
    mThingLookups = (WAD_ThingLookup *)starmemoryalloc(sizeof(WAD_ThingLookup) * mThingLookupsTotal);

    //--Set.
    int i = 0;
    mThingLookups[i++].Set(TYPE_BLOOD_POOL_A,          "POL5", 0x00);
    mThingLookups[i++].Set(TYPE_ELECTRIC_POLE,         "ELEC", 0x00);
    mThingLookups[i++].Set(TYPE_BLOOD_POOL_B,          "POB1", 0x00);
    mThingLookups[i++].Set(TYPE_BLOOD_POOL_C,          "POB2", 0x00);
    mThingLookups[i++].Set(TYPE_BRAIN_POOL,            "BRS1", 0x00);
    mThingLookups[i++].Set(TYPE_FLOOR_LAMP,            "COLU", 0x00);
    mThingLookups[i++].Set(TYPE_TALL_LAMP,             "TLMP", 0x00);
    mThingLookups[i++].Set(TYPE_SHORT_LAMP,            "TLP2", 0x00);
    mThingLookups[i++].Set(TYPE_CANDLE,                "CAND", 0x00);
    mThingLookups[i++].Set(TYPE_CANDELABRA,            "CBRA", 0x00);
    mThingLookups[i++].Set(TYPE_SHORT_GREY_TREE,       "TRE1", 0x00);
    mThingLookups[i++].Set(TYPE_STALAGMITE,            "SMIT", 0x00);
    mThingLookups[i++].Set(TYPE_TALL_BLUE_FIRESTICK,   "TBLU", 0x00);
    mThingLookups[i++].Set(TYPE_TALL_GREEN_FIRESTICK,  "TGRE", 0x00);
    mThingLookups[i++].Set(TYPE_TALL_RED_FIRESTICK,    "TRED", 0x00);
    mThingLookups[i++].Set(TYPE_SHORT_BLUE_FIRESTICK,  "SMBT", 0x00);
    mThingLookups[i++].Set(TYPE_SHORT_GREEN_FIRESTICK, "SMGT", 0x00);
    mThingLookups[i++].Set(TYPE_SHORT_RED_FIRESTICK,   "SMRT", 0x00);
    mThingLookups[i++].Set(TYPE_BURNING_BARREL,        "FCAN", 0x00);
    mThingLookups[i++].Set(TYPE_HEALTH_PICKUP,         "BON1", 0x00);
}

//--[Core Methods]
void WADFile::HandleUDMFThings(StarLinkedList *pThingList)
{
    //--Given a list of WAD_WorldSprites, goes through the list and decomposes them to either WorldSprites or other
    //  objects of more specialized types. The number of things that will become world sprites is not necessarily
    //  the same as the list size, some objects get pruned first.
    //--Any object which survives the initial pruning becomes a world sprite though.
    if(!pThingList) return;

    //--Debug.
    #ifdef THING_DEBUG
        DebugManager::PushPrint(true, "Handling UDMF Things: %i\n", pThingList->GetListSize());
    #endif

    //--Iterate.
    WAD_WorldSprite *rSprite = (WAD_WorldSprite *)pThingList->SetToHeadAndReturn();
    while(rSprite)
    {
        //--Universal type is that of a Vavoom light. Create a light at this location, remove the sprite.
        if(rSprite->mUniversalType == 9825)
        {
            //--Debug.
            #ifdef THING_DEBUG
                DebugManager::Print("Creating a new world light.\n");
            #endif

            //--Create a new light.
            WorldLight *nLight = new WorldLight();
            nLight->SetPosition(rSprite->mX, rSprite->mY, rSprite->mZ);
            nLight->SetIntensity(rSprite->mArgs[0]);

            //--Debug.
            #ifdef THING_DEBUG
                DebugManager::Print(" Light is at %.0f %.0f %.0f.\n", rSprite->mX, rSprite->mY, rSprite->mZ);
                DebugManager::Print(" Intensity is %.0f\n", rSprite->mArgs[0]);
            #endif

            //--Allocate more space for the light.
            mLightsTotal ++;
            mLightList = (WorldLight **)realloc(mLightList, sizeof(WorldLight *) * mLightsTotal);
            mLightList[mLightsTotal-1] = nLight;

            //--This entry is not going to be a world sprite.
            pThingList->RemoveRandomPointerEntry();
        }
        //--Candles. These only create a light if their Arg0 is nonzero and present. These do not get removed, they
        //  go on to spawn World Lights.
        if(rSprite->mUniversalType == TYPE_CANDLE && rSprite->mArgs[0] != 0.0f)
        {
            //--Create a new light.
            WorldLight *nLight = new WorldLight();
            nLight->SetPosition(rSprite->mX, rSprite->mY, rSprite->mZ + 10.0f);
            nLight->SetIntensity(rSprite->mArgs[0]);

            //--Allocate more space for the light.
            mLightsTotal ++;
            mLightList = (WorldLight **)realloc(mLightList, sizeof(WorldLight *) * mLightsTotal);
            mLightList[mLightsTotal-1] = nLight;
        }
        //--Firesticks. Only create a light source if their Arg0 is nonzero and present.
        if(rSprite->mUniversalType == TYPE_SHORT_RED_FIRESTICK && rSprite->mArgs[0] != 0.0f)
        {
            //--Create a new light.
            WorldLight *nLight = new WorldLight();
            nLight->SetPosition(rSprite->mX, rSprite->mY, rSprite->mZ + 10.0f);
            nLight->SetIntensity(rSprite->mArgs[0]);

            //--Allocate more space for the light.
            mLightsTotal ++;
            mLightList = (WorldLight **)realloc(mLightList, sizeof(WorldLight *) * mLightsTotal);
            mLightList[mLightsTotal-1] = nLight;
        }

        //--Next.
        rSprite = (WAD_WorldSprite *)pThingList->IncrementAndGetRandomPointerEntry();
    }

    //--Debug.
    #ifdef THING_DEBUG
        DebugManager::Print("Dumping things to statically sized list.\n");
    #endif

    //--Anything still on the list becomes a proper world sprite.
    mWorldSpritesTotal = pThingList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mWorldSprites = (WAD_WorldSprite *)starmemoryalloc(sizeof(WAD_WorldSprite) * mWorldSpritesTotal);
    for(int i = 0; i < mWorldSpritesTotal; i ++)
    {
        //--Get the sprite.
        WAD_WorldSprite *rSpriteData = (WAD_WorldSprite *)pThingList->GetElementBySlot(i);
        memcpy(&mWorldSprites[i], rSpriteData, sizeof(WAD_WorldSprite));
    }

    //--Debug.
    #ifdef THING_DEBUG
        DebugManager::PopPrint("Finished UDMF thing handling.\n");
    #endif
}
void WADFile::ProcessThingsLump(WAD_Directory_Entry *pThingEntry)
{
    //--Entry point for handling Things. Requires a valid THINGS lump, and will do the rest for you. Note that
    //  the level pointers need to have been set up first, as THINGS are map-specific.
    if(!pThingEntry) return;

    //--Create the WorldSpriteConstructionList. We don't know exactly how many sprites we're doing to need, so
    //  this is a linked-list we'll reconstitute later.
    mWorldSpriteConstructionList = new StarLinkedList(true);

    //--Build lookups if they haven't been built yet.
    if(mThingLookupsTotal < 1) BuildThingLookups();

    //--Create a fast-access copy array of the things. We can freely modify this without damaging the originals.
    int tExpectedThings = pThingEntry->mSize / sizeof(WAD_Thing);
    SetMemoryData(__FILE__, __LINE__);
    WAD_Thing *tThingListing = (WAD_Thing *)starmemoryalloc(sizeof(WAD_Thing) * tExpectedThings);
    memcpy(tThingListing, pThingEntry->mData, sizeof(WAD_Thing) * tExpectedThings);

    //--Scan through the things and upload their data to the level based on what type each thing is. Most things
    //  get ignored since this engine doesn't need them!
    for(int i = 0; i < tExpectedThings; i ++)
    {
        //--Type: Player 1 Start. This positions the 3DCamera.
        if(tThingListing[i].mType == TYPE_PLAYER_START_1)
        {
            //--Resolve the height.
            float tHeight = GetHeightAtPoint(tThingListing[i].mXPosition, -tThingListing[i].mYPosition);

            //--Move the camera up a bit, since most people don't have their heads on the floor.
            tHeight = tHeight - 48.0f;

            //--Set the camera's XYZ position.
            StarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
            rCamera->SetPosition(-tThingListing[i].mXPosition, tThingListing[i].mYPosition, tHeight);
            //fprintf(stderr, "Set position %f %f %f\n", (float)tThingListing[i].mXPosition, (float)-tThingListing[i].mYPosition, tHeight);

            //--Get the facing angle. 0 is east, 90 is north, 180 is west, and so on.
            rCamera->SetRotation(90.0f, 0.0f, (tThingListing[i].mAngle - 90) * -1.0f);
            continue;
        }

        //--If it wasn't a player start, scan the lookups.
        for(int p = 0; p < mThingLookupsTotal; p ++)
        {
            //--Compare.
            if(tThingListing[i].mType == mThingLookups[p].mTypeCode)
            {
                //--Get the height from the level geometry.
                float tHeight = GetHeightAtPoint(tThingListing[i].mXPosition, -tThingListing[i].mYPosition);

                //--Place the object.
                WAD_WorldSprite *nSprite = WAD_WorldSprite::Construct(tThingListing[i].mXPosition, -tThingListing[i].mYPosition, tHeight, mThingLookups[p].mNameCode);
                mWorldSpriteConstructionList->AddElement("X", nSprite, &FreeThis);

                //--Lighting values.
                WAD_FloorPoly *rContainingPoly = FindContainingFloorPoly(tThingListing[i].mXPosition, -tThingListing[i].mYPosition, mGlobalTexturePolyList, false);
                if(rContainingPoly && rContainingPoly->mAssociatedSector >= 0 && rContainingPoly->mAssociatedSector < mSectorsTotal)
                {
                    nSprite->mLightValue = (float)mSectors[rContainingPoly->mAssociatedSector].mLightLevel / 255.0f;
                }

                //--Ignore further lookups.
                break;
            }
        }
    }

    //--Deallocate existing.
    mWorldSpritesTotal = 0;
    free(mWorldSprites);
    mWorldSprites = NULL;

    //--Reconstitute the world sprite list.
    if(mWorldSpriteConstructionList->GetListSize() > 0)
    {
        //--Allocate.
        mWorldSpritesTotal = mWorldSpriteConstructionList->GetListSize();
        SetMemoryData(__FILE__, __LINE__);
        mWorldSprites = (WAD_WorldSprite *)starmemoryalloc(sizeof(WAD_WorldSprite) * mWorldSpritesTotal);

        //--Copy data.
        for(int i = 0; i < mWorldSpritesTotal; i ++)
        {
            WAD_WorldSprite *rSpriteData = (WAD_WorldSprite *)mWorldSpriteConstructionList->GetElementBySlot(i);
            if(!rSpriteData) continue;
            memcpy(&mWorldSprites[i], rSpriteData, sizeof(WAD_WorldSprite));
        }
    }

    //--Clean up.
    delete mWorldSpriteConstructionList;
    mWorldSpriteConstructionList = NULL;

    //--Crossload all the sprites.
    LoadThingSprites();
}

//--[File I/O]
void WADFile::LoadThingSprites()
{
    //--One all the WorldSprites have been loaded and placed, we need to load all their sprites and give
    //  them reference copies. Since we don't want to overdo it, we create a DataLibrary entry for the sprites.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->Purge("Root/Images/WorldSprites/");
    rDataLibrary->AddPath("Root/Images/WorldSprites/");

    //--Setup.
    char tDLBuffer[512];
    char tSpriteDirectoryBuf[9];

    //--Go through the sprite listing.
    for(int i = 0; i < mWorldSpritesTotal; i ++)
    {
        //--Skip if the first letter is a dash.
        if(mWorldSprites[i].mName[0] == '-') continue;

        //--Check if we've already loaded the sprite.
        sprintf(tDLBuffer, "Root/Images/WorldSprites/%s/00", mWorldSprites[i].mName);
        if(rDataLibrary->DoesEntryExist(tDLBuffer))
        {
            //DebugManager::ForcePrint("WADFile:LoadThingSprites - Picture %s already exists.\n", tDLBuffer);
            continue;
        }

        //--This is a new sprite. Get the expected lump name.
        memset(tSpriteDirectoryBuf, 0, sizeof(char) * 9);
        strncpy(tSpriteDirectoryBuf, mWorldSprites[i].mName, sizeof(char) * 4);
        strcat(tSpriteDirectoryBuf, "A0");

        //--Load it.
        StarBitmap *nNewbitmap = LoadSprite(FindLumpNonFlat(tSpriteDirectoryBuf));

        //--If it didn't exist, bark an error.
        if(!nNewbitmap)
        {
            DebugManager::ForcePrint("WADFile:LoadThingSprites - Error, could not find lump %s.\n", tSpriteDirectoryBuf);
        }
        //--Otherwise, save it in the DataLibrary.
        else
        {
            rDataLibrary->AddPath(tDLBuffer);
            rDataLibrary->RegisterPointer(tDLBuffer, nNewbitmap, &StarBitmap::DeleteThis);
            //DebugManager::ForcePrint("WADFile:LoadThingSprites - Registered picture %s.\n", tSpriteDirectoryBuf);
        }
    }

    //--Give the sprite the associated info.
    for(int i = 0; i < mWorldSpritesTotal; i ++)
    {
        //--Check if we've already loaded the sprite.
        sprintf(tDLBuffer, "Root/Images/WorldSprites/%s/00", mWorldSprites[i].mName);
        mWorldSprites[i].rImage = (StarBitmap *)rDataLibrary->GetEntry(tDLBuffer);
    }
}
StarBitmap *WADFile::LoadSprite(WAD_Directory_Entry *pSpriteEntry)
{
    //--Given a directory, loads and returns the requested sprite in StarBitmap format.
    if(!pSpriteEntry) return NULL;

    //--Header.
    int16_t tWidth, tHeight, tXOffset, tYOffset;
    memcpy(&tWidth,   &pSpriteEntry->mData[0], sizeof(int16_t));
    memcpy(&tHeight,  &pSpriteEntry->mData[2], sizeof(int16_t));
    memcpy(&tXOffset, &pSpriteEntry->mData[4], sizeof(int16_t));
    memcpy(&tYOffset, &pSpriteEntry->mData[6], sizeof(int16_t));

    //--Allocate space.
    int tSizeOfData = sizeof(uint8_t) * tWidth * tHeight * 4;
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tPixelData = (uint8_t *)starmemoryalloc(tSizeOfData);
    for(int i = 0; i < tSizeOfData; i += 4)
    {
        tPixelData[i+0] = 255;
        tPixelData[i+1] = 255;
        tPixelData[i+2] = 255;
        tPixelData[i+3] = 0;
    }

    //--Debug.
    //fprintf(stderr, "Image: %i %i - %i %i\n", tWidth, tHeight, tXOffset, tYOffset);

    //--Columns data.
    for(int c = 0; c < tWidth; c ++)
    {
        //--Get the location of the column data, relative to the 0 in the picture lump. Each column has a pointer
        //  elsewhere in the data, so we need to hop around a bit.
        int32_t tColumnCursor = 0;
        int32_t tDataPosition = (sizeof(int16_t) * 4) + (sizeof(int32_t) * c);
        memcpy(&tColumnCursor, &pSpriteEntry->mData[tDataPosition], sizeof(int32_t));

        //--Debug.
        //fprintf(stderr, " Reading column %i %i\n", c, tDataPosition);

        //--Columns are stored sequentially. They end on a 255 bytes.
        bool tIsDone = false;
        while(!tIsDone)
        {
            //--Header for the column.
            uint8_t tColumnYOffset = 0;
            uint8_t tColumnHeight = 0;
            memcpy(&tColumnYOffset, &pSpriteEntry->mData[tColumnCursor+0], sizeof(uint8_t));
            memcpy(&tColumnHeight,  &pSpriteEntry->mData[tColumnCursor+1], sizeof(uint8_t));
            tColumnCursor = tColumnCursor + 2;

            //--Skip the first byte, it's not used.
            tColumnCursor = tColumnCursor + 1;

            //--Begin cross-loading.
            for(int y = 0; y < tColumnHeight; y ++)
            {
                //--Calculate where the color data goes.
                int tXPos = c;
                int tYPos = tHeight - (tColumnYOffset + y) - 1;
                int tColorCursor = (tXPos + (tYPos * tWidth)) * 4;

                //--Edge check.
                if(tXPos < 0 || tYPos < 0 || tXPos >= tWidth || tYPos >= tHeight)
                {
                }
                //--Don't place anything outside the buffer's edges.
                else if(tColorCursor < 0 || tColorCursor >= tSizeOfData)
                {
                }
                //--Place the pixel.
                else
                {
                    //--Get the palette pixel.
                    uint8_t tPalettePosition;
                    memcpy(&tPalettePosition, &pSpriteEntry->mData[tColumnCursor], sizeof(uint8_t));
                    StarlightColor *rColor = &mPaletteData[tPalettePosition];

                    //--Send it over.
                    tPixelData[tColorCursor+0] = rColor->r * 255.0f;
                    tPixelData[tColorCursor+1] = rColor->g * 255.0f;
                    tPixelData[tColorCursor+2] = rColor->b * 255.0f;
                    tPixelData[tColorCursor+3] = rColor->a * 255.0f;
                    //fprintf(stderr, "  Pixel in %i %i is %i %i %i\n", tXPos, tYPos, tPixelData[tColorCursor+0], tPixelData[tColorCursor+1], tPixelData[tColorCursor+2]);
                }

                //--Iterate up.
                tColumnCursor ++;
            }

            //--One extra byte at the end.
            tColumnCursor ++;

            //--Check if that's the end of the column data.
            uint8_t tCheckByte = 0;
            memcpy(&tCheckByte, &pSpriteEntry->mData[tColumnCursor], sizeof(uint8_t));
            if(tCheckByte == 0xFF) tIsDone = true;
        }
    }

    //--Upload the data.
    //DebugManager::Print("Uploading data. %i legal pixels, %i illegal pixels.\n", tLegalCount, tIllegalCounter);
    StarBitmap::xStaticFlags.mUplWrapS = GL_CLAMP_TO_EDGE;
    StarBitmap::xStaticFlags.mUplWrapT = GL_CLAMP_TO_EDGE;
    StarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
    StarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
    StarBitmap *nBitmap = new StarBitmap(tPixelData, tWidth, tHeight);
    //DebugManager::Print("Upload complete.\n");

    //--Debug.
    //DebugManager::PopPrint("Completed upload of texture.\n");

    //--Clean up.
    free(tPixelData);
    return nBitmap;
}

//--[Rendering]
#define INTO_FLOOR_OFFSET -0.0f
void WADFile::RenderWorldSprites()
{
    //--Renders all the WAD_WorldSprites that were loaded. This is often done outside of the geometry list since
    //  many world sprites tend to be animated in some way.
    glColor3f(1.0f, 1.0f, 1.0f);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_TEXTURE_2D);

    //--Camera must be 3D to render correctly.
    float tXRot, tYRot, tZRot;
    StarCamera3D *r3DCamera = CameraManager::FetchActiveCamera3D();
    r3DCamera->Get3DRotation(tXRot, tYRot, tZRot);

    //--Render.
    for(int i = 0; i < mWorldSpritesTotal; i ++)
    {
        //--Skip if it has no image.
        if(!mWorldSprites[i].rImage) continue;

        //--Reposition.
        glTranslatef(mWorldSprites[i].mX, mWorldSprites[i].mY, mWorldSprites[i].mZ + INTO_FLOOR_OFFSET);
        glRotatef(tZRot, 0, 0, -1);
        //glRotatef(tYRot, 0, -1, 0);
        //glRotatef(tXRot, -1, 0, 0);

        //--Sizing.
        float tWid = mWorldSprites[i].rImage->GetWidth() / 2.0f;
        float tHei = mWorldSprites[i].rImage->GetHeight();

        //--Lighting.
        glColor3f(mWorldSprites[i].mLightValue, mWorldSprites[i].mLightValue, mWorldSprites[i].mLightValue);

        //--Render a quad at this location in the world.
        mWorldSprites[i].rImage->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 1.0f); glVertex3f(-tWid, 0.0f, tHei);
            glTexCoord2f(1.0f, 1.0f); glVertex3f( tWid, 0.0f, tHei);
            glTexCoord2f(1.0f, 0.0f); glVertex3f( tWid, 0.0f, 0.0f);
            glTexCoord2f(0.0f, 0.0f); glVertex3f(-tWid, 0.0f, 0.0f);
        glEnd();

        //--Clean.
        //glRotatef(tXRot, 1, 0, 0);
        //glRotatef(tYRot, 0, 1, 0);
        glRotatef(tZRot, 0, 0, 1);
        glTranslatef(-mWorldSprites[i].mX, -mWorldSprites[i].mY, -mWorldSprites[i].mZ - INTO_FLOOR_OFFSET);
    }

    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
}
void WADFile::BuildEnvironmentGeometry()
{
    //--During lights building, more advanced environmental geometry, such as trees and bushes, is constructed. These replace
    //  existing sprites and are generally bigger and more detailed.

    //--Get the associated textures.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    StarBitmap *rTreeTexture   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/3DSprites/Trees/Tree00");
    StarBitmap *rBushTexture   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/3DSprites/Bushes/Bush00");
    StarBitmap *rBottleTexture = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/3DSprites/Brewery/WineBottle");
    if(!rTreeTexture || !rBushTexture) return;

    //--Iterate.
    for(int i = 0; i < mWorldSpritesTotal; i ++)
    {
        //--Tree: Replaced with some simple tree geometry.
        if(mWorldSprites[i].mUniversalType == TYPE_SHORT_GREY_TREE)
        {
            float tUseZ = GetHeightAtPoint(mWorldSprites[i].mX, mWorldSprites[i].mY);
            CreateEnvironmentPolyAt(mWorldSprites[i].mX, mWorldSprites[i].mY, tUseZ, rTreeTexture);
            //fprintf(stderr, "Created a tree at %f %f %f\n", mWorldSprites[i].mX, -mWorldSprites[i].mY, mWorldSprites[i].mZ);
        }
        //--Bush. Also simple, slightly off the ground.
        else if(mWorldSprites[i].mUniversalType == TYPE_STALAGMITE)
        {
            float tUseZ = GetHeightAtPoint(mWorldSprites[i].mX, mWorldSprites[i].mY);
            CreateEnvironmentPolyAt(mWorldSprites[i].mX, mWorldSprites[i].mY, tUseZ - 12.0f, rBushTexture);
            //fprintf(stderr, "Created a bush at %f %f %f\n", mWorldSprites[i].mX, -mWorldSprites[i].mY, mWorldSprites[i].mZ - 12.0f);
        }
        //--Health potion turns into a wine bottle.
        else if(mWorldSprites[i].mUniversalType == TYPE_HEALTH_PICKUP)
        {
            float tUseZ = GetHeightAtPoint(mWorldSprites[i].mX, mWorldSprites[i].mY);
            CreateEnvironmentPolyAt(mWorldSprites[i].mX, mWorldSprites[i].mY, tUseZ, rBottleTexture);
        }
    }
}
void WADFile::CreateEnvironmentPolyAt(float pX, float pY, float pZ, StarBitmap *pImage)
{
    //--Worker routine, creates four environmental polygons at the given location. Each of the four is facing
    //  a different direction, giving an illusion of solidity.
    if(!pImage || !mArbitraryPolysConstructionList) return;

    //--Resolve sizes.
    float cWidth = pImage->GetWidth() * 0.5f;
    float cHeight = pImage->GetHeight();

    //--Slight angle randomization.
    float cAngle = (float)(rand() % 360) * TORADIAN;

    //--Create.
    for(int i = 0; i < 4; i ++)
    {
        //--Create and allocate.
        SetMemoryData(__FILE__, __LINE__);
        WorldPolygon *nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));

        //--Setup.
        nNewPolygon->Initialize();
        nNewPolygon->rTexture = pImage;
        mArbitraryPolysConstructionList->AddElement("X", nNewPolygon, &WorldPolygon::DeleteThis);

        //--Calculate the offsets.
        float tXOff = cWidth * cosf(cAngle);
        float tYOff = cWidth * sinf(cAngle);

        //--Position it.
        nNewPolygon->Allocate(4);
        nNewPolygon->SetPoint(0, pX-tXOff, pY-tYOff, pZ+cHeight);
        nNewPolygon->SetPoint(1, pX+tXOff, pY+tYOff, pZ+cHeight);
        nNewPolygon->SetPoint(2, pX+tXOff, pY+tYOff, pZ);
        nNewPolygon->SetPoint(3, pX-tXOff, pY-tYOff, pZ);

        //--Texture coordinates.
        nNewPolygon->SetTexCoord(0, 0.0f, 1.0f);
        nNewPolygon->SetTexCoord(1, 1.0f, 1.0f);
        nNewPolygon->SetTexCoord(2, 1.0f, 0.0f);
        nNewPolygon->SetTexCoord(3, 0.0f, 0.0f);

        //--Next.
        cAngle = cAngle + (90.0f * TORADIAN);
    }
}
