//--Base
#include "AdvCombatJob.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
AdvCombatJob::AdvCombatJob()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATJOB;

    //--[AdvCombatJob]
    //--System
    mIsMastered = false;
    mIsAlwaysMastered = false;
    mAppearsOnSkillUI = true;
    mLocalName = InitializeString("Job");
    mDisplayName = InitializeString("Job");
    mScriptPath = NULL;
    rOwner = NULL;
    memset(mScriptResponses, 0, sizeof(mScriptResponses));

    //--Ability Listing
    mAssociatedProfileName = InitializeString("Default");
    mCurrentJP = 0;
    mJPNeededToMaster = 0;
    mrAbilityList = new StarLinkedList(false);

    //--Display.
    mFaceTableDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    rFaceTableImg = NULL;

    //--Tags
    mTagList = new StarLinkedList(true);
}
AdvCombatJob::~AdvCombatJob()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    free(mAssociatedProfileName);
    delete mrAbilityList;
    delete mTagList;
}

///===================================== Property Queries =========================================
bool AdvCombatJob::IsMastered()
{
    return (mIsMastered || mIsAlwaysMastered);
}
bool AdvCombatJob::AppearsOnSkillUI()
{
    return mAppearsOnSkillUI;
}
const char *AdvCombatJob::GetInternalName()
{
    return mLocalName;
}
const char *AdvCombatJob::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatJob::GetScriptPath()
{
    return mScriptPath;
}
const char *AdvCombatJob::GetAssociatedProfileName()
{
    return mAssociatedProfileName;
}
int AdvCombatJob::GetCurrentJP()
{
    return mCurrentJP;
}
int AdvCombatJob::GetTotalJP()
{
    //--Run across all skills. Sum cost of unlocked skills.
    int tTotalJP = mCurrentJP;
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mrAbilityList->PushIterator();
    while(rAbility)
    {
        if(rAbility->IsUnlocked())
        {
            tTotalJP = tTotalJP + rAbility->GetJPCost();
        }
        rAbility = (AdvCombatAbility *)mrAbilityList->AutoIterate();
    }

    return tTotalJP;
}
int AdvCombatJob::GetJPToMaster()
{
    //--If this flag is set, always return 0.
    if(mIsAlwaysMastered) return 0;

    //--Otherwise, compute.
    int tTotalJP = 0;
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mrAbilityList->PushIterator();
    while(rAbility)
    {
        if(!rAbility->IsUnlocked())
        {
            tTotalJP = tTotalJP + rAbility->GetJPCost();
        }
        rAbility = (AdvCombatAbility *)mrAbilityList->AutoIterate();
    }

    return tTotalJP;
}
int AdvCombatJob::GetTagCount(const char *pTag)
{
    int *rTagPtr = (int *)mTagList->GetElementByName(pTag);
    if(!rTagPtr) return 0;
    return *rTagPtr;
}
bool AdvCombatJob::HasInternalVersionOfAbility(AdvCombatAbility *pAbility)
{
    //--Purchasable abilities are JOBNAME|ABILITYNAME, while internal versions of an ability are
    //  JOBNAMEINTERNAL|ABILITYNAME and are automatically equipped to the ability bar. This function
    //  returns true if an internal version of the ability exists in this job.
    if(!pAbility) return false;
    int tAbilityflag = pAbility->HasInternalVersion();

    //--Already checked and computed this, return cases:
    if(tAbilityflag == ADVCA_HAS_NO_INTERNAL) return false;
    if(tAbilityflag == ADVCA_HAS_INTERNAL) return true;

    //--If the value was not one of the above, we need to resolve the answer now.
    if(!mrAbilityList->IsElementOnList(pAbility) || !rOwner)
    {
        pAbility->SetInternalVersionFlag(ADVCA_HAS_NO_INTERNAL);
        return false;
    }

    //--Get the internal name of the ability.
    const char *rAbilityInternalName = pAbility->GetInternalName();

    //--Locate the bar in the name.
    int tBarSlot = -1;
    int tLen = (int)strlen(rAbilityInternalName);
    for(int i = 0; i < tLen; i ++)
    {
        if(rAbilityInternalName[i] == '|')
        {
            tBarSlot = i;
            break;
        }
    }

    //--No bar, fail.
    if(tBarSlot == -1 || tBarSlot == 0)
    {
        pAbility->SetInternalVersionFlag(ADVCA_HAS_NO_INTERNAL);
        return false;
    }

    //--Create a buffer for the internal version.
    char tBuffer[256];
    strncpy(tBuffer, rAbilityInternalName, sizeof(char) * tBarSlot);
    tBuffer[tBarSlot] = '\0';
    strcat(tBuffer, "Internal");
    strcat(tBuffer, &rAbilityInternalName[tBarSlot]);

    //--Create a second buffer for the case where there is a space.
    char tBuffer2[256];
    strncpy(tBuffer2, rAbilityInternalName, sizeof(char) * tBarSlot);
    tBuffer2[tBarSlot] = '\0';
    strcat(tBuffer2, " Internal");
    strcat(tBuffer2, &rAbilityInternalName[tBarSlot]);

    //--See if the owner has the ability in question. If so, it's on the bar.
    if(rOwner->GetAbilityList()->GetElementByName(tBuffer) != NULL || rOwner->GetAbilityList()->GetElementByName(tBuffer2) != NULL)
    {
        pAbility->SetInternalVersionFlag(ADVCA_HAS_INTERNAL);
        return true;
    }

    //--Not on the bar.
    pAbility->SetInternalVersionFlag(ADVCA_HAS_NO_INTERNAL);
    return false;
}
StarBitmap *AdvCombatJob::GetFaceProperties(TwoDimensionReal &sDimensions)
{
    memcpy(&sDimensions, &mFaceTableDim, sizeof(TwoDimensionReal));
    return rFaceTableImg;
}

///======================================== Manipulators ==========================================
void AdvCombatJob::SetOwner(AdvCombatEntity *pOwner)
{
    rOwner = pOwner;
}
void AdvCombatJob::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void AdvCombatJob::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
void AdvCombatJob::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void AdvCombatJob::SetIsAlwaysMastered(bool pFlag)
{
    mIsAlwaysMastered = pFlag;
}
void AdvCombatJob::SetAppearsOnSkillUI(bool pFlag)
{
    mAppearsOnSkillUI = pFlag;
}
void AdvCombatJob::SetScriptResponse(int pIndex, bool pFlag)
{
    if(pIndex < 0 || pIndex >= ADVCJOB_CODE_TOTAL) return;
    mScriptResponses[pIndex] = pFlag;
}
void AdvCombatJob::RegisterAbility(AdvCombatAbility *pAbility)
{
    if(!pAbility || mrAbilityList->IsElementOnList(pAbility)) return;
    mrAbilityList->AddElementAsTail(pAbility->GetInternalName(), pAbility);

    //--Add the JP needed to unlock this ability to the total needed for the job.
    mJPNeededToMaster += pAbility->GetJPCost();
}
void AdvCombatJob::UnregisterAbility(AdvCombatAbility *pAbility)
{
    if(pAbility && mrAbilityList->IsElementOnList(pAbility))
    {
        mJPNeededToMaster -= pAbility->GetJPCost();
    }

    while(mrAbilityList->RemoveElementP(pAbility))
    {

    }
}
void AdvCombatJob::SetCurrentJP(int pAmount)
{
    mCurrentJP = pAmount;
}
void AdvCombatJob::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdvCombatJob::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}
void AdvCombatJob::SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath)
{
    //--Sets the coordinates for the face that appears in the Profiles UI.
    if(!pPath) return;
    rFaceTableImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    mFaceTableDim.Set(pLft, pTop, pRgt, pBot);

    //--If the image exists, recompute face table positions.
    if(!rFaceTableImg) return;
    float cWid = rFaceTableImg->GetWidth();
    float cHei = rFaceTableImg->GetHeight();
    mFaceTableDim.mLft = mFaceTableDim.mLft / cWid;
    mFaceTableDim.mTop = mFaceTableDim.mTop / cHei;
    mFaceTableDim.mRgt = mFaceTableDim.mRgt / cWid;
    mFaceTableDim.mBot = mFaceTableDim.mBot / cHei;
    mFaceTableDim.mXCenter = mFaceTableDim.mXCenter / cWid;
    mFaceTableDim.mYCenter = mFaceTableDim.mYCenter / cHei;
}
void AdvCombatJob::SetAssociatedProfileName(const char *pName)
{
    //--If the result was "NULL" or NULL, then set it to "Default".
    if(!pName || !strcasecmp(pName, "NULL"))
    {
        ResetString(mAssociatedProfileName, "Default");
        return;
    }

    //--Set.
    ResetString(mAssociatedProfileName, pName);
}

///======================================== Core Methods ==========================================
void AdvCombatJob::AssumeJob(AdvCombatEntity *pCaller)
{
    //--The given caller will assume this job, having their stats and graphics modified as needed.
    //  The caller is placed on the activity stack, but the job can be retrieved with lua commands.
    if(!mScriptPath || !pCaller) return;

    //--Reset the job stats.
    CombatStatistics *rJobStatGroup = pCaller->GetStatisticsGroup(ADVCE_STATS_JOB);
    rJobStatGroup->Zero();

    //--Call the lua script to do the heavy lifting.
    CallScript(ADVCJOB_CODE_SWITCHTO);
}
void AdvCombatJob::CallScript(int pCode)
{
    if(!mScriptPath) return;
    if(pCode < 0 || pCode >= ADVCJOB_CODE_TOTAL) return;
    if(!mScriptResponses[pCode]) return;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)pCode);
}
void AdvCombatJob::CallScriptLevelUp(int pLevel)
{
    //--Used for level-up cases, passes the requested level to the script. Doesn't necessarily need
    //  to be the current level for a character.
    if(!mScriptPath) return;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 2, "N", (float)ADVCJOB_CODE_LEVEL, "N", (float)pLevel);
}
void AdvCombatJob::RecheckMastery()
{
    //--Reset flag.
    mIsMastered = true;

    //--Scan. If any ability is not unlocked, not mastered.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mrAbilityList->PushIterator();
    while(rAbility)
    {
        if(!rAbility->IsUnlocked())
        {
            mIsMastered = false;
            mrAbilityList->PopIterator();
            return;
        }

        rAbility = (AdvCombatAbility *)mrAbilityList->AutoIterate();
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
AdvCombatEntity *AdvCombatJob::GetOwner()
{
    return rOwner;
}
AdvCombatAbility *AdvCombatJob::GetAbility(int pSlot)
{
    return (AdvCombatAbility *)mrAbilityList->GetElementBySlot(pSlot);
}
StarLinkedList *AdvCombatJob::GetAbilityList()
{
    return mrAbilityList;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
