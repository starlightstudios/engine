//--Base
#include "AbyssCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///--[Local Definitions]
#define ACI_EFFECTS_PER_PAGE 16

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
///======================================= Core Methods ===========================================
///=========================================== Update =============================================
///========================================== Drawing =============================================
///=================================== Rendering Subhandlers ======================================
void AbyssCombat::RenderInspectorResistStats()
{
    ///--[Documentation]
    //--Renders the Resistances/Statistics window, depending on which flag is active. Identical to
    //  the base version, except renders only the relevant resistances if applicable.

    ///--[Common]
    //--Backing and detail.
    Images.Data.rInspectorResistancesFrame->Draw();
    Images.Data.rInspectorResistancesDetail->Draw();

    //--Get entity.
    AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
    if(!rInspectEntity) return;

    ///--[Resistances]
    if(!mInspectorShowStatistics)
    {
        ///--[Common]
        //--Heading.
        mHeadingStatic.SetAsMixer();
        Images.Data.rInspectorHeading->DrawTextFixed(624.0f, 91.0f, 220.0f, 0, "Resistances");
        StarlightColor::ClearMixer();

        ///--[Do Not Show]
        //--Enemies need to be defeated a certain number of times to show their resistances. Allies never do this.
        if(!IsEntityInPlayerParty(rInspectEntity) && rInspectEntity->GetInspectorShowResists() > 0)
        {
            float cTxtHei =  18.0f;
            float cMaxWid = 237.0f;
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 0.0f), cMaxWid, 0, "Unknown.");
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 2.0f), cMaxWid, 0, "Defeat this enemy");
            Images.Data.rInspectorStatistic->DrawTextFixedArgs(614.0f, 134.0f + (cTxtHei * 3.0f), cMaxWid, 0, "%i more times to", rInspectEntity->GetInspectorShowResists());
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 4.0f), cMaxWid, 0, "reveal their resistances.");
        }
        ///--[Show All Resistances]
        else
        {
            //--Icon/Value setup.
            float cIconX = 622.0f;
            float cIconY = 136.0f;
            float cIconH =  25.0f;
            float cValueX = cIconX + 50.0f;
            float cValueY = cIconY -  3.0f;
            float cReductionX = 844.0f;
            float cReductionY = cIconY - 3.0f;

            //--Icons, resist values, reduction percentage.
            for(int i = 0; i < ABYCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL; i ++)
            {
                //--Backing.
                if(i % 2 == 1)
                {
                    StarBitmap::DrawRectFill(666.0f, cValueY + (cIconH * i), 851.0f, cValueY + (cIconH * i) + cIconH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
                }

                //--Setup.
                int tResistValue = rInspectEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_RESIST_START + i);

                //--Render icon.
                Images.Data.rInspectorResistIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

                //--Value is 1000 or higher. Render "Immune".
                if(tResistValue >= 1000)
                {
                    Images.Data.rInspectorStatistic->DrawText(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
                }
                //--Otherwise, render the value and the percentage reduction.
                else
                {
                    //--If a bonus is applied, render the statistic in purple. If a malus is applied, red.
                    int tBonusAbi = rInspectEntity->GetStatistic(ADVCE_STATS_TEMPEFFECT, STATS_RESIST_START + i);
                    if(tBonusAbi > 0)
                    {
                        mInspectorBonus.SetAsMixer();
                    }
                    else if(tBonusAbi < 0)
                    {
                        mInspectorMalus.SetAsMixer();
                    }

                    //--Resistance amount.
                    Images.Data.rInspectorStatistic->DrawTextArgs(cValueX, cValueY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistValue);

                    //--Computed reduction.
                    float cReduction = 1.0f - AdvCombatEntity::ComputeResistance(tResistValue);
                    Images.Data.rInspectorStatistic->DrawTextArgs(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%0.0f%%", cReduction * 100.0f);

                    //--Clean.
                    StarlightColor::ClearMixer();
                }
            }
        }
    }
    ///--[Statistics]
    else
    {
        ///--[Do Not Show]
        //--Enemies need to be defeated a certain number of times to show their resistances. Allies never do this.
        if(!IsEntityInPlayerParty(rInspectEntity) && rInspectEntity->GetInspectorShowStats() > 0)
        {
            //--Heading.
            mHeadingStatic.SetAsMixer();
            Images.Data.rInspectorHeading->DrawTextFixed(679.0f, 91.0f, 220.0f, 0, "Statistics");
            StarlightColor::ClearMixer();

            float cTxtHei =  18.0f;
            float cMaxWid = 237.0f;
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 0.0f), cMaxWid, 0, "Unknown.");
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 2.0f), cMaxWid, 0, "Defeat this enemy");
            Images.Data.rInspectorStatistic->DrawTextFixedArgs(614.0f, 134.0f + (cTxtHei * 3.0f), cMaxWid, 0, "%i more times to", rInspectEntity->GetInspectorShowStats());
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 4.0f), cMaxWid, 0, "reveal their statistics.");
        }
        ///--[Show All Resistances]
        else
        {
            //--Heading.
            mHeadingStatic.SetAsMixer();
            Images.Data.rInspectorHeading->DrawTextFixed(624.0f, 91.0f, 165.0f, 0, "Statistics");
            StarlightColor::ClearMixer();

            //--Icon/Value setup.
            float cIconX  = 622.0f;
            float cIconY  = 136.0f;
            float cIconH  =  25.0f;
            float cValueX = 841.0f;
            float cValueY = cIconY -  3.0f;

            //--Lookup table.
            for(int i = 0; i < ADVCOMBAT_INSPECTOR_STATUS_ICON_TOTAL; i ++)
            {
                //--Backing.
                if(i % 2 == 1)
                {
                    StarBitmap::DrawRectFill(611.0f, cValueY + (cIconH * i), 851.0f, cValueY + (cIconH * i) + cIconH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
                }

                //--Image.
                Images.Data.rInspectorStatusIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

                //--Statistic.
                int tValue = rInspectEntity->GetStatistic(mInspectorStatLookups[i]);

                //--If a bonus is applied, render the statistic in purple. If a malus is applied, red.
                int tBonusAbi = rInspectEntity->GetStatistic(ADVCE_STATS_TEMPEFFECT, mInspectorStatLookups[i]);
                if(tBonusAbi > 0)
                {
                    mInspectorBonus.SetAsMixer();
                }
                else if(tBonusAbi < 0)
                {
                    mInspectorMalus.SetAsMixer();
                }

                //--Render.
                Images.Data.rInspectorStatistic->DrawTextArgs(cValueX, cValueY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
                StarlightColor::ClearMixer();
            }
        }
    }
}
