///=================================== CarnationCombatEntity ======================================
//--Modified version of the AdvCombatEntity, has extra portrait handling code.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "AdvCombatEntity.h"

///===================================== Local Structures =========================================
///--[CCELayer]
//--Represents a layer when animating a character portrait. A layer is a single image, but may animate
//  according to a timer.
//--For simplicity, the hardcap of CCELAYER_MAX_FRAMES frames is used.
#define CCELAYER_MAX_FRAMES 10
typedef struct CCELayer
{
    //--Variables
    bool mIsDisabled;
    bool mRendersInWorld;
    int mPriority;
    float mTicksPerFrame;
    int mMaxFrames;
    float mXOffset, mYOffset;
    StarBitmap *rImages[CCELAYER_MAX_FRAMES];

    //--Functions
    void Initialize()
    {
        mIsDisabled = false;
        mRendersInWorld = false;
        mPriority = 0;
        mTicksPerFrame = 1.0f;
        mMaxFrames = 0;
        mXOffset = 0.0f;
        mYOffset = 0.0f;
        memset(rImages, 0, sizeof(StarBitmap *) * CCELAYER_MAX_FRAMES);
    }
}CCELayer;

///===================================== Local Definitions ========================================
//--Timers
#define CCE_INJURY_TICKS 11
#define CCE_ATTACK_TICKS 8
#define CCE_TICKS_PER_HP 1.0f
#define CCE_TICKS_HP_CAP 30
#define CCE_TICKS_PER_SP 0.2f
#define CCE_TICKS_SP_CAP 30
#define CCE_TICKS_PER_INF 0.2f
#define CCE_TICKS_INF_CAP 30

//--Script Calls
#define CCE_SCRIPT_CALL_RECOMPILE_PORTRAIT 100
#define CCE_SCRIPT_CALL_FAILED_TO_ACT 101
#define CCE_SCRIPT_CALL_CHECK_STATUS_INTERCEPT 102

///========================================== Classes =============================================
class CarnationCombatEntity : public AdvCombatEntity
{
    private:
    ///--[System]
    bool mIsPlayerControlled;
    bool mStopKnockout;

    ///--[Combat]
    bool mIgnoresProficiency;
    int mFlatAccuracy;
    int mFlatCritRate;

    ///--[Switchable Layers]
    int mInjuryTimer;
    char *mSwitchableLayerName;
    StarBitmap *rInjuredImg;
    StarBitmap *rKnockoutImg;

    ///--[Sounds]
    char *mSoundHit;
    char *mSoundMiss;
    char *mSoundSwing;
    char *mSoundHeal;
    char *mSoundSpell;

    ///--[HP/SP/Inf Handling]
    bool mHPScrollHold;
    bool mCompleteTimersImmediately;
    EasingPack1D mHPPack;
    EasingPack1D mSPPack;
    EasingPack1D mInfPack;

    ///--[Rendering]
    bool mEnemyUsesComplexRendering;
    bool mIgnoreRenderOffset;
    char *mRenderRefreshScript;
    char *mSavePortraitPath;
    int mAnimationTimer;
    int mAttackTimer;
    StarLinkedList *mRenderingLayers; //CCELayer *, master

    ///--[Injury Array]
    static const int xInjuryYOffsetCap;
    static const float xInjuryYOffsetTPF;
    static const int xInjuryYOffsets[];

    protected:

    public:
    //--System
    CarnationCombatEntity();
    virtual ~CarnationCombatEntity();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    bool IsPlayerControlled();
    const char *GetSoundHit();
    const char *GetSoundMiss();
    const char *GetSoundSwing();
    const char *GetSoundHeal();
    const char *GetSoundSpell();
    virtual bool IsKnockingOut();
    bool IsKnockoutPending();
    bool HasKnockoutBlock();
    float GetDisplayInfluence();
    bool IgnoresProficiency();
    int GetFlatAccuracy();
    int GetFlatCritRate();

    //--Manipulators
    void SetPlayerControlThisTurn(bool pFlag);
    void SetRenderRefreshScript(const char *pPath);
    void SetComplexRendering(bool pFlag);
    void SetIgnoreXPosition(bool pFlag);
    void AddRenderLayer(const char *pName, int pPriority, float pTPF, int pFrames);
    void RemoveRenderLayer(const char *pName);
    void ClearRenderLayers();
    void SetRenderLayerFrame(const char *pName, int pSlot, const char *pImagePath);
    void SetRenderLayerWorld(const char *pName, bool pWorldRender);
    void SetRenderLayerDisabled(const char *pName, bool pIsDisabled);
    void SetRenderLayerOffsets(const char *pName, float pXOffset, float pYOffset);
    void SortRenderLayers();
    void SetSwitchableLayerName(const char *pName);
    void SetInjuredImage(const char *pImagePath);
    void SetKnockoutImage(const char *pImagePath);
    void Injure();
    void Heal();
    void Attack();
    void SetHPScrollHold(bool pFlag);
    void CompleteTimers();
    void ClearKnockoutBlock();
    void EditFlag(const char *pString);
    char *GetSavePortraitPath();
    void SetSavePortraitPath(const char *pString);
    void SetIgnoresProficiency(bool pFlag);
    void SetFlatAccuracy(int pAccuracy);
    void SetFlatCritRate(int pCritRate);

    //--Core Methods
    static int LayerCompare(const void *pEntryA, const void *pEntryB);
    void TripRenderRefresh();
    virtual void FullRestore();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void UpdateTimers();

    //--File I/O
    //--Drawing
    void RenderPortraitAt(float pX, float pY, bool pShowOnlyWorld);
    void RenderSwitchableAt(float pX, float pY);
    void RenderLayersAt(float pX, float pY);
    void RenderAsEnemy(float pX, float pY, int pTargetTimer);
    static void RenderThreeDigitNumber(int pNumber, float pX, float pY, StarBitmap *pImage);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnationCombatEntity_GetProperty(lua_State *L);
int Hook_CarnationCombatEntity_SetProperty(lua_State *L);
