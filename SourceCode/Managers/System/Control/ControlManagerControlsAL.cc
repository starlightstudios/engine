//--Base
#include "ControlManager.h"

//--Classes
//--Definitions
#include "Global.h"

//--Generics
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///--[ ======================================== Allegro ========================================= ]
#if defined _ALLEGRO_PROJECT_
void ControlManager::SetupControls()
{
    ///--[Standard keys]
    //--Mappings for directional movements.
    RegisterControl("Up"   ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_UP,    ALLEGRO_KEY_W));
    RegisterControl("Down" ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_DOWN,  ALLEGRO_KEY_S));
    RegisterControl("Left" ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_LEFT,  ALLEGRO_KEY_A));
    RegisterControl("Right",   CreateControlWatchingKeyboard(ALLEGRO_KEY_RIGHT, ALLEGRO_KEY_D));
    RegisterControl("UpLevel", CreateControlWatchingKeyboard(ALLEGRO_KEY_Q,     ALLEGRO_KEY_PGUP));
    RegisterControl("DnLevel", CreateControlWatchingKeyboard(ALLEGRO_KEY_E,     ALLEGRO_KEY_PGDN));

    //--Action mappings.
    RegisterControl("Activate", CreateControlWatchingKeyboard(ALLEGRO_KEY_Z,      -1));
    RegisterControl("Cancel",   CreateControlWatchingKeyboard(ALLEGRO_KEY_X,      -1));
    RegisterControl("Run",      CreateControlWatchingKeyboard(ALLEGRO_KEY_LSHIFT, -1));
    RegisterControl("Jump",     CreateControlWatchingKeyboard(ALLEGRO_KEY_SPACE,  -1));

    //--Field Abilities
    RegisterControl("OpenFieldAbilityMenu", CreateControlWatchingKeyboard(ALLEGRO_KEY_C,      -1));
    RegisterControl("FieldAbility0", CreateControlWatchingKeyboard(ALLEGRO_KEY_1, -1));
    RegisterControl("FieldAbility1", CreateControlWatchingKeyboard(ALLEGRO_KEY_2, -1));
    RegisterControl("FieldAbility2", CreateControlWatchingKeyboard(ALLEGRO_KEY_3, -1));
    RegisterControl("FieldAbility3", CreateControlWatchingKeyboard(ALLEGRO_KEY_4, -1));
    RegisterControl("FieldAbility4", CreateControlWatchingKeyboard(ALLEGRO_KEY_5, -1));

    ///--[Mouse Buttons]
    RegisterControl("MouseLft", CreateControlWatchingMouse(1, -1));
    RegisterControl("MouseRgt", CreateControlWatchingMouse(2, -1));

    ///--[Witch Hunter Izana]
    RegisterControl("WHI Emergency Escape", CreateControlWatchingKeyboard(ALLEGRO_KEY_END, -1));

    ///--[GUI Keys]
    RegisterControl("Shift",              CreateControlWatchingKeyboard(ALLEGRO_KEY_LSHIFT, ALLEGRO_KEY_RSHIFT));
    RegisterControl("Backspace",          CreateControlWatchingKeyboard(ALLEGRO_KEY_BACKSPACE,              -1));
    RegisterControl("F1",                 CreateControlWatchingKeyboard(ALLEGRO_KEY_F1,                     -1));
    RegisterControl("F2",                 CreateControlWatchingKeyboard(ALLEGRO_KEY_F2,                     -1));
    RegisterControl("F3",                 CreateControlWatchingKeyboard(ALLEGRO_KEY_F3,                     -1));
    RegisterControl("F4",                 CreateControlWatchingKeyboard(ALLEGRO_KEY_F4,                     -1));
    RegisterControl("F5",                 CreateControlWatchingKeyboard(ALLEGRO_KEY_F5,                     -1));
    RegisterControl("Enter",              CreateControlWatchingKeyboard(ALLEGRO_KEY_ENTER,   ALLEGRO_KEY_PAD_ENTER));
    RegisterControl("Ctrl",               CreateControlWatchingKeyboard(ALLEGRO_KEY_LCTRL,   ALLEGRO_KEY_RCTRL));
    RegisterControl("Delete",             CreateControlWatchingKeyboard(ALLEGRO_KEY_DELETE,                 -1));
    RegisterControl("Escape",             CreateControlWatchingKeyboard(ALLEGRO_KEY_ESCAPE,                 -1));
    RegisterControl("ToggleFreeMovement", CreateControlWatchingKeyboard(ALLEGRO_KEY_TAB,                    -1));
    RegisterControl("Quick Node",         CreateControlWatchingKeyboard(ALLEGRO_KEY_F1,                     -1));
    RegisterControl("Reload Level",       CreateControlWatchingKeyboard(ALLEGRO_KEY_F2,                     -1));
    RegisterControl("Build Lighting",     CreateControlWatchingKeyboard(ALLEGRO_KEY_F3,                     -1));
    RegisterControl("Save Level Data",    CreateControlWatchingKeyboard(ALLEGRO_KEY_F4,                     -1));
    RegisterControl("Load Level Data",    CreateControlWatchingKeyboard(ALLEGRO_KEY_F10,                    -1));
    RegisterControl("ToggleFullscreen",   CreateControlWatchingKeyboard(ALLEGRO_KEY_F11,                    -1));
    RegisterControl("PageUp",             CreateControlWatchingKeyboard(ALLEGRO_KEY_PGUP,                   -1));
    RegisterControl("PageDn",             CreateControlWatchingKeyboard(ALLEGRO_KEY_PGDN,                   -1));
    RegisterControl("GUI|Up",             CreateControlWatchingKeyboard(ALLEGRO_KEY_UP,                     -1));
    RegisterControl("GUI|Dn",             CreateControlWatchingKeyboard(ALLEGRO_KEY_DOWN,                   -1));
    RegisterControl("GUI|Home",           CreateControlWatchingKeyboard(ALLEGRO_KEY_HOME,                   -1));
    RegisterControl("GUI|End",            CreateControlWatchingKeyboard(ALLEGRO_KEY_END,                    -1));
    RegisterControl("GUI|BackToTitle",    CreateControlWatchingKeyboard(ALLEGRO_KEY_F8,                     -1));

    ///--[Numeric Keys]
    RegisterControl("Key_1", CreateControlWatchingKeyboard(ALLEGRO_KEY_1, -1));
    RegisterControl("Key_2", CreateControlWatchingKeyboard(ALLEGRO_KEY_2, -1));
    RegisterControl("Key_3", CreateControlWatchingKeyboard(ALLEGRO_KEY_3, -1));
    RegisterControl("Key_4", CreateControlWatchingKeyboard(ALLEGRO_KEY_4, -1));
    RegisterControl("Key_5", CreateControlWatchingKeyboard(ALLEGRO_KEY_5, -1));
    RegisterControl("Key_6", CreateControlWatchingKeyboard(ALLEGRO_KEY_6, -1));
    RegisterControl("Key_7", CreateControlWatchingKeyboard(ALLEGRO_KEY_7, -1));
    RegisterControl("Key_8", CreateControlWatchingKeyboard(ALLEGRO_KEY_8, -1));
    RegisterControl("Key_9", CreateControlWatchingKeyboard(ALLEGRO_KEY_9, -1));
    RegisterControl("Key_0", CreateControlWatchingKeyboard(ALLEGRO_KEY_0, -1));

    ///--[Debug Keys]
    RegisterControl("OpenDebug",    CreateControlWatchingKeyboard(ALLEGRO_KEY_I,     -1));
    RegisterControl("CameraUnlock", CreateControlWatchingKeyboard(ALLEGRO_KEY_LCTRL, -1));
    RegisterControl("WalkSpeedDn",  CreateControlWatchingKeyboard(ALLEGRO_KEY_O, -1));
    RegisterControl("WalkSpeedUp",  CreateControlWatchingKeyboard(ALLEGRO_KEY_P, -1));
    RegisterControl("FrameSpeedUp", CreateControlWatchingKeyboard(ALLEGRO_KEY_K, -1));
    RegisterControl("FrameSpeedDn", CreateControlWatchingKeyboard(ALLEGRO_KEY_L, -1));

    //--Shader Toggles
    RegisterControl("Shader0", CreateControlWatchingKeyboard(ALLEGRO_KEY_0, -1));
    RegisterControl("Shader1", CreateControlWatchingKeyboard(ALLEGRO_KEY_1, -1));
    RegisterControl("Shader2", CreateControlWatchingKeyboard(ALLEGRO_KEY_2, -1));
    RegisterControl("RecompileShaders", CreateControlWatchingKeyboard(ALLEGRO_KEY_F9, -1));

    //--Kerning
    RegisterControl("RebuildKerning", CreateControlWatchingKeyboard(ALLEGRO_KEY_F10, -1));

    //--Diagnostics
    RegisterControl("Diagnostics", CreateControlWatchingKeyboard(ALLEGRO_KEY_F7, -1));
    RegisterControl("ToggleVisTrace", CreateControlWatchingKeyboard(ALLEGRO_KEY_F6, -1));

    //--Loop Controls
    //RegisterControl("LoopBack", CreateControlWatchingKeyboard(ALLEGRO_KEY_LEFTBRACKET, -1));
    //RegisterControl("LoopForw", CreateControlWatchingKeyboard(ALLEGRO_KEY_RIGHTBRACKET, -1));

    ///--[Peak Freaks]
    RegisterControl("PF Left",        CreateControlWatchingKeyboard(ALLEGRO_KEY_LEFT,  -1));
    RegisterControl("PF Right",       CreateControlWatchingKeyboard(ALLEGRO_KEY_RIGHT, -1));
    RegisterControl("PF Up",          CreateControlWatchingKeyboard(ALLEGRO_KEY_UP,    -1));
    RegisterControl("PF Down",        CreateControlWatchingKeyboard(ALLEGRO_KEY_DOWN,  -1));
    RegisterControl("PF Activate",    CreateControlWatchingKeyboard(ALLEGRO_KEY_Z,     -1));
    RegisterControl("PF Crush",       CreateControlWatchingKeyboard(ALLEGRO_KEY_X,     -1));
    RegisterControl("PF Cancel",      CreateControlWatchingKeyboard(ALLEGRO_KEY_C,     -1));
    RegisterControl("PF Debug Win",   CreateControlWatchingKeyboard(ALLEGRO_KEY_F1,    -1));
    RegisterControl("PF Debug Lose",  CreateControlWatchingKeyboard(ALLEGRO_KEY_F2,    -1));
    RegisterControl("PF Kill Rival",  CreateControlWatchingKeyboard(ALLEGRO_KEY_F3,    -1));

    ///--[Slitted Eye]
    //--Game controls.
    RegisterControl("SE Manipulate", CreateControlWatchingMouse(1, -1));
    RegisterControl("SE Hiss", CreateControlWatchingMouse(2, -1));
    RegisterControl("SE Game Over", CreateControlWatchingKeyboard(ALLEGRO_KEY_SPACE, -1));

    ///--[Witches of Ravenbrook]
    RegisterControl("ScndUp"   ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_W, -1));
    RegisterControl("ScndDown" ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_S, -1));
    RegisterControl("ScndLeft" ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_A, -1));
    RegisterControl("ScndRight",   CreateControlWatchingKeyboard(ALLEGRO_KEY_D, -1));

    ///--[Script Object Builder]
    RegisterControl("SOB Escape",    CreateControlWatchingKeyboard(ALLEGRO_KEY_ESCAPE,                 -1));
    RegisterControl("SOB Up",        CreateControlWatchingKeyboard(ALLEGRO_KEY_UP,                     -1));
    RegisterControl("SOB Down",      CreateControlWatchingKeyboard(ALLEGRO_KEY_DOWN,                   -1));
    RegisterControl("SOB Left",      CreateControlWatchingKeyboard(ALLEGRO_KEY_LEFT,                   -1));
    RegisterControl("SOB Right",     CreateControlWatchingKeyboard(ALLEGRO_KEY_RIGHT,                  -1));
    RegisterControl("SOB Accel",     CreateControlWatchingKeyboard(ALLEGRO_KEY_LCTRL,  ALLEGRO_KEY_RCTRL));
    RegisterControl("SOB Select",    CreateControlWatchingKeyboard(ALLEGRO_KEY_ENTER,                  -1));
    RegisterControl("SOB Backspace", CreateControlWatchingKeyboard(ALLEGRO_KEY_BACKSPACE,              -1));
    RegisterControl("SOB Shift",     CreateControlWatchingKeyboard(ALLEGRO_KEY_LSHIFT, ALLEGRO_KEY_RSHIFT));
    RegisterControl("SOB Export",    CreateControlWatchingKeyboard(ALLEGRO_KEY_F2,                     -1));

    ///--[Joypad Depression]
    //--Creates controls watching all the keys on the joypad. These are for display purposes only
    //  and are not needed if the #define is not set.
    #ifdef DBG_SHOW_JOYPAD_DEPRESSION
    {
        ALLEGRO_JOYSTICK *rJoystick = al_get_joystick(0);
        if(rJoystick)
        {
            fprintf(stderr, "A joystick is attached.\n");
            char tBuffer[32];
            for(int i = 0; i < al_get_joystick_num_buttons(rJoystick); i ++)
            {
                sprintf(tBuffer, "DBGJoy%02i", i);
                RegisterControl(tBuffer, CreateControlWatchingJoystick(i, -1));
            }
            for(int i = 0; i < al_get_joystick_num_sticks(rJoystick); i ++)
            {
                for(int p = 0; p < 4; p ++)
                {
                    int tIndex = JOYSTICK_OFFSET_STICK + (i * JOYSTICK_STICK_CONSTANT)  + p;
                    sprintf(tBuffer, "DBGStk%03i", tIndex);
                    RegisterControl(tBuffer, CreateControlWatchingJoystick(tIndex, -1));
                }
            }
        }
        else
        {
            fprintf(stderr, "No joysticks attached.\n");
        }
    }
    #endif
}
#endif
