//--Base
//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers

///===================================== Game Registration ========================================
//--At program startup, the scripts will typically search for valid games to populate the starting list
//  with, which the user can then select from. However, various versions of the engine may not support
//  certain games, as those may be part of dedicated projects and need their own compilation.
//--Therefore, this function is called which creates a global list of supported games with this implementation.
//--This is also incidentally a list of games that this project supports.
void RegisterGames()
{
    ///--[Setup]
    int cDummyPtr = 1;
    SugarLinkedList *rRegistry = Global::Shared()->gGameRegistry;

    ///--[Register]
    rRegistry->AddElement("Peak Freaks", &cDummyPtr);
}
