///========================================== TakeDown =============================================
//--Called at program exit. Frees all outstanding memory and deletes outstanding objects. While
//  the OS will do this on its own, deleting handles to the graphics and audio cards speeds the
//  process up considerably.

#pragma once

void Free_All_Memory();

