///--[Starlight Core PCH]
//--Starlight Engine Core Pre-compiled Header. This is used by the Starlight Core project and only contains
//  includes relevant to the managers/definitions/coreclasses/etc of the Starlight engine.

//--If you don't know what a pre-compiled header is:
//  A PCH contains the header information from all the headers within and compiles them. So long
//  as they do not change, the compiler will not need to recompile these headers when they are
//  included in other files. This speeds up compilation times *considerably* but the pch files
//  are 1) large and 2) take a while to recompile. Only system libraries should be in the PCH.
//--Since the file is precompiled, the pragma directive spits an error. Leave the header guards.

///--[Header Guards]
#ifndef _STARLIGHTCORE_AL_PCH_H_
#define _STARLIGHTCORE_AL_PCH_H_

#ifndef __cpp_attributes
#define __cpp_attributes
#endif
#ifndef _stdcall
#define _stdcall
#endif

///--[Allegro Headers]
#if defined _ALLEGRO_PROJECT_
    #include <allegro5/allegro.h>
    #ifndef _TARGET_OS_LINUX_
        #include <allegro5/allegro_image.h>
    #else
        #include "GL/gl.h"
    #endif
    #include <allegro5/allegro_opengl.h>
#endif
#if defined _FMOD_AUDIO_
    #include "fmod.hpp"
#endif

///--[Stdlib Headers]
#include <stdlib.h>
#include <stdio.h>
#include "string.h"
#ifdef __cplusplus
    #include <vector>
    #include <fstream>
    #include <iostream>
#endif
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <time.h>

///--[Other Library Headers]
#if defined _BASS_AUDIO_
    #include "bass.h"
    #include "bass_fx.h"
#endif

///--[Lua]
extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

///--[Starlight Color]
//--The StarlightColor is considered a primitive type in the engine.
#include "StarlightColor.h"

///--[Memory Manager]
//--Static class, can be accessed from anywhere.
#include "MemoryManager.h"

#endif
