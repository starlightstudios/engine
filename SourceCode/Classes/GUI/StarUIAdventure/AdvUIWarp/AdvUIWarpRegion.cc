//--Base
#include "AdvUIWarp.h"
#include "AdvUIWarpStructures.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== Update ==============================================
void AdvUIWarp::UpdateRegionMode()
{
    ///--[Documentation and Setup]
    //--When the player is selecting a warp region, this appears.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Warp variables.
        mIsSelectingLocation = true;
        mLocationCursor = 0;
        mLocationSkip = 0;
        FocusOnWarpLocation();
        mLocationPos.Complete();

        //--Cursor.
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");

        //--The region index in the linked list is the code of the selected entry.
        int tSelectedRegion = GetGridCodeAtCursor();

        //--Locate the region package.
        WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementBySlot(tSelectedRegion);
        if(!rRegionPack) return;

        //--Make sure all the images in the advanced maps have loaded, if any.
        AdvancedMapPack *rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->PushIterator();
        while(rMapPack)
        {
            if(rMapPack->rImage) rMapPack->rImage->LoadDataFromSLF();
            rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->AutoIterate();
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

///========================================== Drawing =============================================
void AdvUIWarp::RenderRegionMode()
{
    ///--[Documentation]
    //--Renders the warp regions grid.

    //--Position the objects slide away from.
    float tSlidePercent = EasingFunction::QuadraticOut(mVisibilityTimer, mVisibilityTimerMax);

    ///--[Grid Entries]
    //--Render all grid entries. This renders the images.
    RenderGrid(tSlidePercent);

    //--Render the cursor.
    if(!mIsSelectingLocation) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Render the name of the selected entry.
    RenderGridEntryName(GetPackageAtCursor(), tSlidePercent);

    //--Clean.
    StarlightColor::ClearMixer();
}
