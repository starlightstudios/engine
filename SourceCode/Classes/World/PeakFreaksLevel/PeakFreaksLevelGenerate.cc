//--Base
#include "PeakFreaksLevel.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
PF_LevelLayout *PeakFreaksLevel::GenerateLevel()
{
    ///--[Documentation]
    //--Generates a PeakFreaks level given the required length and passes it back. This version of the
    //  level is simplified grid indices which can then be extrapolated with a map file to make tiles.

    //--Mandated seed. If the level needs to generate with a specific 4-digit number, that gets handled here.
    //  Otherwise, the last-used seed is stored.
    if(xMandatedSeed != -1)
    {
        srand(xMandatedSeed);
    }
    else
    {
        xMandatedSeed = rand() % 10000;
        srand(xMandatedSeed);
    }

    ///--[Setup]
    //--Create a set of movement instructions. Each level is 6 pieces plus an end piece. The first piece
    //  can move any direction.
    int tInstructions[LEVEL_LENGTH];
    int tPlaceCodes[LEVEL_LENGTH];
    int tXPositions[LEVEL_LENGTH];
    int tYPositions[LEVEL_LENGTH];
    for(int i = 0; i < LEVEL_LENGTH; i ++)
    {
        tInstructions[i] = DIR_NONE;
        tPlaceCodes[i] = RECON_EMPTY;
        tXPositions[i] = LEVEL_INIT_X;
        tYPositions[i] = LEVEL_INIT_Y;
    }

    ///--[Generation]
    //--Variables.
    int tXPos = LEVEL_INIT_X;
    int tYPos = LEVEL_INIT_Y;

    //--The zero instruction is always a downwards ending piece.
    tInstructions[0] = ROLL_S;
    tPlaceCodes[0] = RECON_END_N;
    tXPositions[0] = tXPos;
    tYPositions[0] = tYPos;

    //--Adjust grid downwards.
    tYPos ++;

    //--Place the rest of the instructions.
    for(int i = 1; i < LEVEL_LENGTH; i ++)
    {
        //--Special: If this is the last piece, it becomes a starting piece. This does not require rolling
        //  a new direction, it is based on the previous position.
        if(i == LEVEL_LENGTH-1)
        {
            //--Get positions.
            int tCode = RECON_START_N;
            int tPrevX = tXPositions[i-1];
            int tPrevY = tYPositions[i-1];

            //--Resolve ending piece.
            if(tPrevY < tYPos)
                tCode = RECON_START_N;
            else if(tPrevY > tYPos)
                tCode = RECON_START_S;
            else if(tPrevX < tXPos)
                tCode = RECON_START_W;
            else
                tCode = RECON_START_E;

            //--Store this code for later.
            PeakFreaksLevel::xStartType = tCode;

            //--Set.
            tInstructions[i] = 0;
            tPlaceCodes[i] = tCode;
            tXPositions[i] = tXPos;
            tYPositions[i] = tYPos;
            break;
        }

        //--Roll a new instruction.
        int tNewInstruction = (rand() % ROLL_CAP);

        //--Emulate moving to the new position.
        int tNewX = tXPos;
        int tNewY = tYPos;
        if(tNewInstruction == ROLL_N)
            tNewY --;
        else if(tNewInstruction == ROLL_E)
            tNewX ++;
        else if(tNewInstruction == ROLL_S)
            tNewY ++;
        else if(tNewInstruction == ROLL_W)
            tNewX --;

        //--Scan the previous positions to see if any match. If so, this move was invalid.
        bool tInvalid = false;
        for(int p = 0; p < i; p ++)
        {
            if(tXPositions[p] == tNewX && tYPositions[p] == tNewY)
            {
                tInvalid = true;
                break;
            }
        }
        if(tInvalid)
        {
            i --;
            continue;
        }

        //--Position was valid. Place.
        tInstructions[i] = (rand() % ROLL_CAP);
        tXPositions[i] = tXPos;
        tYPositions[i] = tYPos;

        //--Determine the place code based on how the positions changed.
        int tPrevX = tXPositions[i-1];
        int tPrevY = tYPositions[i-1];

        //--Y positions are the same. Horizontal.
        if(tPrevY == tNewY)
        {
            tPlaceCodes[i] = RECON_HORIZONTAL;
        }
        //--X positions are the same. Vertical.
        else if(tPrevX == tNewX)
        {
            tPlaceCodes[i] = RECON_VERTICAL;
        }
        //--Above Previous:
        else if(tYPos < tPrevY)
        {
            //--Left:
            if(tNewX < tXPos)
            {
                tPlaceCodes[i] = RECON_ELBOW_LD;
            }
            //--Right:
            else
            {
                tPlaceCodes[i] = RECON_ELBOW_RD;
            }
        }
        //--Below Previous:
        else if(tYPos > tPrevY)
        {
            //--Left:
            if(tNewX < tXPos)
            {
                tPlaceCodes[i] = RECON_ELBOW_LU;
            }
            //--Right:
            else
            {
                tPlaceCodes[i] = RECON_ELBOW_RU;
            }
        }
        //--Left of Previous:
        else if(tXPos < tPrevX)
        {
            //--Above:
            if(tNewY < tYPos)
            {
                tPlaceCodes[i] = RECON_ELBOW_RU;
            }
            //--Below:
            else
            {
                tPlaceCodes[i] = RECON_ELBOW_RD;
            }
        }
        //--Right of Previous:
        else if(tXPos > tPrevX)
        {
            //--Above:
            if(tNewY < tYPos)
            {
                tPlaceCodes[i] = RECON_ELBOW_LU;
            }
            //--Below:
            else
            {
                tPlaceCodes[i] = RECON_ELBOW_LD;
            }
        }

        //--Next spot.
        tXPos = tNewX;
        tYPos = tNewY;
    }

    ///--[Trimming]
    //--Find the lowest X/Y number, and the highest. These are the dimensions.
    int tLowestX = 1000;
    int tLowestY = 1000;
    int tHighestX = -1000;
    int tHighestY = -1000;
    for(int i = 0; i < LEVEL_LENGTH; i ++)
    {
        if(tXPositions[i] < tLowestX)  tLowestX  = tXPositions[i];
        if(tXPositions[i] > tHighestX) tHighestX = tXPositions[i];
        if(tYPositions[i] < tLowestY)  tLowestY  = tYPositions[i];
        if(tYPositions[i] > tHighestY) tHighestY = tYPositions[i];
    }

    //--Subtract all positions by the lowest values.
    for(int i = 0; i < LEVEL_LENGTH; i ++)
    {
        tXPositions[i] = tXPositions[i] - tLowestX;
        tYPositions[i] = tYPositions[i] - tLowestY;
    }

    ///--[Set Structure]
    //--Allocate.
    PF_LevelLayout *nLayout = (PF_LevelLayout *)starmemoryalloc(sizeof(PF_LevelLayout));
    nLayout->Initialize();

    //--Sizes for the arrays.
    nLayout->mXSize = (tHighestX - tLowestX) + 1;
    nLayout->mYSize = (tHighestY - tLowestY) + 1;

    //--Allocate.
    nLayout->mArray = (int **)starmemoryalloc(sizeof(int *) * nLayout->mXSize);
    for(int x = 0; x < nLayout->mXSize; x ++)
    {
        nLayout->mArray[x] = (int *)starmemoryalloc(sizeof(int) * nLayout->mYSize);
        for(int y = 0; y < nLayout->mYSize; y ++)
        {
            nLayout->mArray[x][y] = RECON_EMPTY;
        }
    }
    nLayout->mPriorities = (int **)starmemoryalloc(sizeof(int *) * nLayout->mXSize);
    for(int x = 0; x < nLayout->mXSize; x ++)
    {
        nLayout->mPriorities[x] = (int *)starmemoryalloc(sizeof(int) * nLayout->mYSize);
        for(int y = 0; y < nLayout->mYSize; y ++)
        {
            nLayout->mPriorities[x][y] = -1;
        }
    }

    //--Place the array on the piece grid.
    for(int i = 0; i < LEVEL_LENGTH; i ++)
    {
        int x = tXPositions[i];
        int y = tYPositions[i];
        nLayout->mArray[x][y] = tPlaceCodes[i];
        nLayout->mPriorities[x][y] = i;
    }

    ///--[Debug]
    if(false)
    {
        fprintf(stderr, "Sizes: %i x %i\n", nLayout->mXSize, nLayout->mYSize);
        for(int y = 0; y < nLayout->mYSize; y ++)
        {
            for(int x = 0; x < nLayout->mXSize; x ++)
            {
                fprintf(stderr, "%2i ", nLayout->mArray[x][y]);
            }
            fprintf(stderr, "\n");
        }
    }

    ///--[Finish Up]
    //--Pass back the grid.
    return nLayout;
}
PF_LevelLayout *PeakFreaksLevel::GenerateTutorial()
{
    ///--[Documentation]
    //--Generates a PeakFreaks level using a fixed layout, specifically for a tutorial.

    ///--[Set Structure]
    //--Allocate.
    PF_LevelLayout *nLayout = (PF_LevelLayout *)starmemoryalloc(sizeof(PF_LevelLayout));
    nLayout->Initialize();

    //--Sizes for the arrays.
    nLayout->mXSize = 4;
    nLayout->mYSize = 1;

    //--Allocate.
    nLayout->mArray = (int **)starmemoryalloc(sizeof(int *) * nLayout->mXSize);
    for(int x = 0; x < nLayout->mXSize; x ++)
    {
        nLayout->mArray[x] = (int *)starmemoryalloc(sizeof(int) * nLayout->mYSize);
        for(int y = 0; y < nLayout->mYSize; y ++)
        {
            nLayout->mArray[x][y] = RECON_EMPTY;
        }
    }
    nLayout->mPriorities = (int **)starmemoryalloc(sizeof(int *) * nLayout->mXSize);
    for(int x = 0; x < nLayout->mXSize; x ++)
    {
        nLayout->mPriorities[x] = (int *)starmemoryalloc(sizeof(int) * nLayout->mYSize);
        for(int y = 0; y < nLayout->mYSize; y ++)
        {
            nLayout->mPriorities[x][y] = -1;
        }
    }

    //--Place the array on the piece grid.
    nLayout->mArray[0][0] = RECON_START_E;
    nLayout->mArray[1][0] = RECON_START_N;
    nLayout->mArray[2][0] = RECON_START_W;
    nLayout->mArray[3][0] = RECON_START_S;

    nLayout->mPriorities[0][0] = 3;
    nLayout->mPriorities[1][0] = 2;
    nLayout->mPriorities[2][0] = 1;
    nLayout->mPriorities[3][0] = 0;


    ///--[Finish Up]
    //--Pass back the grid.
    return nLayout;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
