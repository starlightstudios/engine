//--Base
#include "MonocerosMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"
#include "MonoUIBase.h"
#include "MonoUICampfire.h"
#include "MonoUIEquip.h"
#include "MonoUIFileSelect.h"
#include "MonoUIInventory.h"
#include "MonoUIJournal.h"
#include "MonoUIMap.h"
#include "MonoUIOptions.h"
#include "MonoUIQuit.h"
#include "MonoUISkills.h"
#include "MonoUIStatus.h"
#include "MonoUIVendor.h"
#include "MonoUIWarp.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarTranslation.h"
#include "StarlightString.h"
#include "StarPointerSeries.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
MonocerosMenu::MonocerosMenu()
{
    ///--[ ====== Variable Initialization ===== ]
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_MONOCEROSMENU;

    ///--[AdventureMenu]
    //--System
    //--Component UI
    //--Closing Handler
    //--Colors
    //--Help Menus
    //--Images

    ///--[MonocerosMenu]
    //--System
    mOptionsOnly = false;
    mEnableMouse = true;

    //--Strings
    mTextCampfireMenu = InitializeString("Campfire Menu");
    mTextSave         = InitializeString("Save");
    mTextWarp         = InitializeString("Warp");
    mTextPassword     = InitializeString("Password");

    //--Resolutions.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    mResolutionsTotal = rDisplayManager->GetTotalDisplayModes();
    mResolutionStrings = (char **)starmemoryalloc(sizeof(char *) * mResolutionsTotal);
    for(int i = 0; i < mResolutionsTotal; i ++)
    {
        DisplayInfo tInfo = rDisplayManager->GetDisplayMode(i);
        mResolutionStrings[i] = InitializeString("%i x %i : %i", tInfo.mWidth, tInfo.mHeight, tInfo.mRefreshRate);
    }

    //--Images
    mMonoTitle.    SetRGBAI(236, 211, 160, 255);
    mMonoSubtitle. SetRGBAI(181, 112,  41, 255);
    mMonoParagraph.SetRGBAI(214, 188, 219, 255);
    memset(&MonoImg, 0, sizeof(MonoImg));

    ///--[ =========== Construction =========== ]
    ///--[Construction]
    //--Standard pieces.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    MonoImg.Standard.rFontControl        = rDataLibrary->GetFont("Monoceros Standard Control");
    MonoImg.Standard.rFontHeading        = rDataLibrary->GetFont("Monoceros Menu Inventory Header");
    MonoImg.Standard.rFontMainline       = rDataLibrary->GetFont("Monoceros Menu Inventory Description");
    MonoImg.Standard.rFrameDescription   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoMenu/Inventory/Frame_Description");
    MonoImg.Standard.rScrollbarFront     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoMenu/Inventory/Scrollbar_Scroller");
    MonoImg.Standard.rFrameHelp          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoMenu/Options/Frame_Help");
    MonoImg.Standard.rOverlayDescription = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoMenu/Inventory/Overlay_DescriptionName");

    ///--[Components]
    //--Create the Base UI. It is held on mrMainMenuComponents which will handle deallocation.
    rUIBase   = new MonoUIBase();
    rCampBase = new MonoUICampfire();

    //--Build components.
    RegisterUI("Campfire",    rCampBase,              AM_CAMPFIRE_MODE_BASE,       mrCampfireComponents);
    RegisterUI("Warp",        new MonoUIWarp(),       AM_CAMPFIRE_MODE_WARP,       mrCampfireComponents);
    RegisterUI("File Select", new MonoUIFileSelect(), AM_CAMPFIRE_MODE_FILESELECT, mrCampfireComponents);
    RegisterUI("Vendor",      new MonoUIVendor(),     AM_MODE_NONE,                mrVendorComponents);
    RegisterUI("Base",        rUIBase,                AM_MODE_BASE,                mrMainMenuComponents);
    RegisterUI("Equip",       new MonoUIEquip(),      AM_MODE_EQUIP,               mrMainMenuComponents);
    RegisterUI("Inventory",   new MonoUIInventory(),  AM_MODE_ITEMS,               mrMainMenuComponents);
    RegisterUI("Map",         new MonoUIMap(),        AM_MODE_MAP,                 mrMainMenuComponents);
    RegisterUI("Options",     new MonoUIOptions(),    AM_MODE_OPTIONS,             mrMainMenuComponents);
    RegisterUI("Quit",        new MonoUIQuit(),       AM_MODE_QUIT,                mrMainMenuComponents);
    RegisterUI("Skills",      new MonoUISkills(),     AM_MODE_SKILLS,              mrMainMenuComponents);
    RegisterUI("Status",      new MonoUIStatus(),     AM_MODE_STATUS,              mrMainMenuComponents);
    RegisterUI("Journal",     new MonoUIJournal(),    AM_MODE_JOURNAL,             mrMainMenuComponents);

    ///--[Verify Images]
    //--Make sure everything resolved.
    MonoImg.mIsReady = VerifyStructure(&MonoImg.Standard,   sizeof(MonoImg.Standard),   sizeof(void *), false);

    //--List checking. Run construction then check that images resolved.
    StarUIPiece *rUIPiece = (StarUIPiece *)mAllComponents->PushIterator();
    while(rUIPiece)
    {
        rUIPiece->Construct();
        rUIPiece->RunTranslation();
        MonoImg.mIsReady = (MonoImg.mIsReady && rUIPiece->DidImagesResolve());
        rUIPiece = (StarUIPiece *)mAllComponents->AutoIterate();
    }

    ///--[Debug]
    //--If the images failed to resolve, try to establish why by re-running the verifications.
    if(!MonoImg.mIsReady && !AdventureMenu::xSuppressLoadingErrors)
    {
        //--Heading.
        fprintf(stderr, "=== Mono UI Images Failed to Resolve ===\n");

        //--Component UI.
        StarUIPiece *rUIPiece = (StarUIPiece *)mAllComponents->PushIterator();
        while(rUIPiece)
        {
            rUIPiece->ImageDiagnostics();
            rUIPiece = (StarUIPiece *)mAllComponents->AutoIterate();
        }
    }

    ///--[String Reinitialization]
    //--Base Class
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_UI);
    if(rTranslation)
    {
        StarTranslation::StringTranslateWorker(mTextCampfireMenu, rTranslation);
        StarTranslation::StringTranslateWorker(mTextSave,         rTranslation);
        StarTranslation::StringTranslateWorker(mTextWarp,         rTranslation);
        StarTranslation::StringTranslateWorker(mTextPassword,     rTranslation);
    }

    //--In addition, run this override function that changes the text in the campfire menu.
    SetCampfireOverrides();

    ///--[ ======= Special Diagnostics ======== ]
    //--If and only if SPS_TRACKS_DUMMIES from the StarPointerSeries header is defined, we also perform
    //  a series of "dummy" crossloads to build a purge file. This is used to specify which images do
    //  not need to be loaded, but this only happens if a crossload is actually called, and the dummy
    //  SPS entries are never used since there is no equivalent UI in Monoceros. Examples are Doctor,
    //  Forms, Chat, etc.
    #if defined SPS_TRACKS_DUMMIES

    //--Fast-access pointers.
    StarPointerSeries *rSeries = NULL;

    //--Calls.
    rSeries = rDataLibrary->GetStarPointerSeries("Adventure Chat UI Monoceros");
    if(rSeries) rSeries->CrossloadDiagnostics();
    rSeries = rDataLibrary->GetStarPointerSeries("Adventure Costumes UI Monoceros");
    if(rSeries) rSeries->CrossloadDiagnostics();
    rSeries = rDataLibrary->GetStarPointerSeries("Adventure Doctor UI Monoceros");
    if(rSeries) rSeries->CrossloadDiagnostics();
    rSeries = rDataLibrary->GetStarPointerSeries("Adventure Field Ability UI Monoceros");
    if(rSeries) rSeries->CrossloadDiagnostics();
    rSeries = rDataLibrary->GetStarPointerSeries("Adventure Forms UI Monoceros");
    if(rSeries) rSeries->CrossloadDiagnostics();
    rSeries = rDataLibrary->GetStarPointerSeries("Adventure Relive UI Monoceros");
    if(rSeries) rSeries->CrossloadDiagnostics();
    rSeries = rDataLibrary->GetStarPointerSeries("Adventure Trainer UI Monoceros");
    if(rSeries) rSeries->CrossloadDiagnostics();

    #endif
}
MonocerosMenu::~MonocerosMenu()
{
    //--Base Members
    for(int i = 0; i < mResolutionsTotal; i ++) free(mResolutionStrings[i]);
    free(mResolutionStrings);
    free(mTextCampfireMenu);
    free(mTextSave);
    free(mTextWarp);
    free(mTextPassword);
}
void MonocerosMenu::ReverifyOptions()
{
    ///--[Documentation]
    //--When the menu is held on the title screen, the options menu can be used in standalone. This function
    //  preps the menu to be used in that fashion.
    mOptionsOnly = true;

    //--Run construct to verify images.
    StarUIPiece *rUIPiece = RetrieveUI("Options", POINTER_TYPE_STARUIPIECE, NULL, "MonocerosMenu:ReverifyOptions()");
    if(rUIPiece)
    {
        MonoUIOptions *rOptionsUI = dynamic_cast<MonoUIOptions *>(rUIPiece);
        rOptionsUI->ConstructTitle();
    }
}
void MonocerosMenu::SetCampfireOverrides()
{
    /*
    ///--[Documentation]
    //--The campfire menu is mostly unchanged in Monoceros, but the help text needs to use a new set of fonts
    //  and have the text changed to reflect the locked-out options.

    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Fonts]
    Images.CampfireUI.rDoubleHeadingFont = rDataLibrary->GetFont("Monoceros Menu Base Double Header");
    Images.CampfireUI.rMainlineFont      = rDataLibrary->GetFont("Monoceros Menu Base Mainline");

    ///--[Strings]
    //--Clear the help strings.
    for(int i = 0; i < ADVMENU_CAMPFIRE_HELP_MENU_STRINGS; i ++)
    {
        free(Images.CampfireUI.mHelpStrings[i]);
    }

    //--Help Strings
    Images.CampfireUI.mHelpStrings[0] = InitializeString("The campfire menu allows you to warp between locations you have unlocked or to");
    Images.CampfireUI.mHelpStrings[1] = InitializeString("save your game. You can also enter a password here, most of which are used");
    Images.CampfireUI.mHelpStrings[2] = InitializeString("for debug so don't worry about that.");
    Images.CampfireUI.mHelpStrings[3] = InitializeString("");
    Images.CampfireUI.mHelpStrings[4] = InitializeString("");
    Images.CampfireUI.mHelpStrings[5] = InitializeString("");
    Images.CampfireUI.mHelpStrings[6] = InitializeString("");
    Images.CampfireUI.mHelpStrings[7] = InitializeString("");
    Images.CampfireUI.mHelpStrings[8] = InitializeString("");
    Images.CampfireUI.mHelpStrings[9] = InitializeString("Press [IMG0] or [IMG1] to exit this help menu.");

    //--Run translation.
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_UI);
    if(rTranslation)
    {
        for(int i = 0; i < ADVMENU_CAMPFIRE_HELP_MENU_STRINGS; i ++)
        {
            StarTranslation::StringTranslateWorker(Images.CampfireUI.mHelpStrings[i], rTranslation);
        }
    }
    */
}

///--[Public Statics]
bool MonocerosMenu::xIsMenuExecution = false;
char *MonocerosMenu::xSwitchToLevelupPath = NULL;

///===================================== Property Queries =========================================
bool MonocerosMenu::IsOfType(int pType)
{
    if(mType == POINTER_TYPE_FAIL) return false;
    if(mType == POINTER_TYPE_ADVENTUREMENU) return true;
    return (pType == mType);
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void MonocerosMenu::SetLevelupMode()
{
    ///--[Documentation]
    //--Levelup mode allows the player to purchase items that give experience to their party members.
    //  It must be activated by an external Lua call.

    ///--[Cast and Check]
    //--Locate.
    StarUIPiece *rStarUIPiece = RetrieveUI("Vendor", POINTER_TYPE_MONOUIVENDOR, mrVendorComponents, "MonocerosMenu:SetLevelupMode()");
    if(!rStarUIPiece) return;

    //--Dynamic cast, set.
    MonoUIVendor *rVendorUI = dynamic_cast<MonoUIVendor *>(rStarUIPiece);
    rVendorUI->ActivateModeLevelup();
}
void MonocerosMenu::ActivateGemsMode()
{
    ///--[Documentation]
    //--Gemcutters are activated via their own topics, unlike the base version which access them via
    //  a tab in the vendor interface. This function is called from Lua to switch into gems mode.

    ///--[Cast and Check]
    //--Locate.
    StarUIPiece *rStarUIPiece = RetrieveUI("Vendor", POINTER_TYPE_MONOUIVENDOR, mrVendorComponents, "MonocerosMenu:ActivateGemsMode()");
    if(!rStarUIPiece) return;

    //--Dynamic cast, set.
    MonoUIVendor *rVendorUI = dynamic_cast<MonoUIVendor *>(rStarUIPiece);
    rVendorUI->ActivateModeGems();
}
void MonocerosMenu::SetBuyOnly()
{
    //mMonoVendorVars.mNoSellBuyback = true;
}
void MonocerosMenu::SetToOptionsMode()
{
    ///--[Documentation]
    //--Switches to options mode, called from the title screen to directly set to options mode only
    //  since the rest of the UI is not loaded.
    mCurrentMode = AM_MODE_OPTIONS;
    ActivateComponentInListName(mrMainMenuComponents, "Options");
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void MonocerosMenu::UpdateBackground()
{
    ///--[Documentation]
    //--Update called when in the background. Just handles hiding. If in options-mode-only, then only
    //  update options mode.
    if(mOptionsOnly)
    {
        //--Flags.
        mHandledUpdate = true;
        mStartedHidingThisTick = false;
        if(mMainOpacityTimer > 0) mMainOpacityTimer --;

        //--Background update.
        StarUIPiece *rUIPiece = RetrieveUI("Options", POINTER_TYPE_STARUIPIECE, NULL, "MonocerosMenu:Update()");
        if(rUIPiece)
        {
            rUIPiece->UpdateBackground();
        }
        return;
    }

    ///--[Normal Call]
    AdventureMenu::UpdateBackground();
}
void MonocerosMenu::Update()
{
    ///--[Documentation and Setup]
    //--Update called when in the foreground. If options-mode-only is active, bypass the other updates.
    if(mOptionsOnly)
    {
        //--Reset this flag.
        mStartedHidingThisTick = false;
        mHandledUpdate = false;

        //--[Timers]
        //--If this object is visible, opacity increases.
        if(mIsVisible)
        {
            if(mMainOpacityTimer < AM_MAIN_VIS_TICKS) mMainOpacityTimer ++;
        }
        //--Object is not visible. Stop update, decrease opacity timer.
        else
        {
            if(mMainOpacityTimer > 0) mMainOpacityTimer --;
        }

        //--[Opening Tick]
        //--To prevent idiots from opening the menu and then closing it in the same keypress, the first tick is skipped.
        if(mOpenedThisTick)
        {
            mOpenedThisTick = false;
            return;
        }

        //--Update.
        StarUIPiece *rUIPiece = RetrieveUI("Options", POINTER_TYPE_STARUIPIECE, NULL, "MonocerosMenu:Update()");
        if(rUIPiece)
        {
            rUIPiece->UpdateForeground(false);
            if(rUIPiece->IsFlaggingExit())
            {
                rUIPiece->UnflagExit();
                mStartedHidingThisTick = true;
            }
        }

        //--If not in options mode, hide the object.
        return;
    }

    ///--[Base Call]
    AdventureMenu::Update();
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonocerosMenu::Render()
{
    ///--[Options-Only]
    //--If this flag is true, only render options mode. Used on the title screen.
    if(mOptionsOnly)
    {
        StarUIPiece *rUIPiece = RetrieveUI("Options", POINTER_TYPE_STARUIPIECE, NULL, "MonocerosMenu:Render()");
        if(rUIPiece)
        {
            rUIPiece->RenderPieces(1.0f);
        }
        return;
    }

    ///--[Standard]
    AdventureMenu::Render();
}
void MonocerosMenu::RenderItemDetails(int pTimer, int pTimerMax, AdventureItem *pItem)
{
    ///--[Documentation]
    //--Standard description handler. Descriptions appear at the bottom of the screen using a specific
    //  image and render an item's advanced description lines.
    //--It is legal for NULL to be passed as the item. This will render a blank box.
    if(pTimer < 1 || pTimerMax < 1) return;

    //--Percentage.
    float cScrollPercent = EasingFunction::QuadraticInOut(pTimer, pTimerMax);

    //--Offsets.
    float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cScrollPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cScrollPercent * 0.7f));

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    //--Frame.
    MonoImg.Standard.rFrameDescription->Draw();

    ///--[Blank Box Handler]
    //--If the item is NULL, stop.
    if(!pItem)
    {
        glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
        return;
    }

    //--Header, name.
    MonoImg.Standard.rOverlayDescription->Draw();
    MonoImg.Standard.rFontHeading->DrawText(195.0f, 496.0f, 0, 1.0f, pItem->GetDisplayName());

    ///--[Text Lines]
    //--Setup.
    float cXPosition = 129.0f;
    float cYPosition = 563.0f;
    float cYHeight   =  22.0f;

    //--Render advanced description lines.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = pItem->GetAdvancedDescription(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), SUGARFONT_NOCOLOR, 1.0f, MonoImg.Standard.rFontMainline);
    }

    ///--[Clean]
    StarlightColor::ClearMixer();

    ///--[Finish Up]
    //--Clean.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void MonocerosMenu::HookToLuaState(lua_State *pLuaState)
{
    /* MonoMenu_GetProperty("Is Menu Exec") (1 Boolean) (Static) */
    lua_register(pLuaState, "MonoMenu_GetProperty", &Hook_MonoMenu_GetProperty);

    /* MonoMenu_SetProperty("Set Status to Levelup Path", sPath) (Static)
       Sets the property in the Monoceros Menu class. */
    lua_register(pLuaState, "MonoMenu_SetProperty", &Hook_MonoMenu_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_MonoMenu_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //MonoMenu_GetProperty("Is Menu Exec") (1 Boolean) (Static)

    ///--[Setup]
    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("MonoMenu_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--True if the skills menu is currently refreshing skills.
    if(!strcasecmp(rSwitchType, "Is Menu Exec"))
    {
        lua_pushboolean(L, MonocerosMenu::xIsMenuExecution);
        return 1;
    }

    ///--[Menu Resolve]
    MonocerosMenu *rMenu = (MonocerosMenu *)AdventureMenu::Fetch();
    if(!rMenu || !rMenu->IsOfType(POINTER_TYPE_MONOCEROSMENU))
    {
        fprintf(stderr, "%s: Failed, Menu was wrong type, or NULL.\n", "MonoMenu_GetProperty");
        fprintf(stderr, " Arguments: %i\n", tArgs);
        for(int i = 0; i < tArgs; i ++)
        {
            fprintf(stderr, "  %02i: %s\n", i, lua_tostring(L, i+1));
        }
        fprintf(stderr, " Path: %s\n", LuaManager::Fetch()->GetCallStack(0));
        return 0;
    }

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    ///--[Error]
    else
    {
        LuaPropertyError("MonoMenu_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_MonoMenu_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //MonoMenu_SetProperty("Set Status to Levelup Path", sPath) (Static)

    //--[Skills]
    //MonoMenu_SetProperty("Clear Skills")
    //MonoMenu_SetProperty("Add Skill", sSkillName)

    //--[Vendor]
    //MonoMenu_SetProperty("Is Levelup")
    //MonoMenu_SetProperty("Is Gems")
    //MonoMenu_SetProperty("Is Buy Only")

    ///--[Setup]
    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("MonoMenu_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Calls]
    //--Sets the script that gets called when the player presses Activate in the status menu.
    if(!strcasecmp(rSwitchType, "Set Status to Levelup Path") && tArgs >= 2)
    {
        ResetString(MonocerosMenu::xSwitchToLevelupPath, lua_tostring(L, 2));
        return 0;
    }

    ///--[Menu Resolve]
    MonocerosMenu *rMenu = (MonocerosMenu *)AdventureMenu::Fetch();
    if(!rMenu || !rMenu->IsOfType(POINTER_TYPE_MONOCEROSMENU))
    {
        fprintf(stderr, "%s: Failed, Menu was wrong type, or NULL.\n", "MonoMenu_SetProperty");
        fprintf(stderr, " Arguments: %i\n", tArgs);
        for(int i = 0; i < tArgs; i ++)
        {
            fprintf(stderr, "  %02i: %s\n", i, lua_tostring(L, i+1));
        }
        fprintf(stderr, " Path: %s\n", LuaManager::Fetch()->GetCallStack(0));
        return 0;
    }

    ///--[System]
    ///--[Vendor]
    //--Sets to levelup mode.
    if(!strcasecmp(rSwitchType, "Is Levelup") && tArgs == 1)
    {
        rMenu->SetLevelupMode();
    }
    //--Sets to gems mode.
    else if(!strcasecmp(rSwitchType, "Is Gems") && tArgs == 1)
    {
        rMenu->ActivateGemsMode();
    }
    //--Sets to buy only. Used for the skills menus.
    else if(!strcasecmp(rSwitchType, "Is Buy Only") && tArgs == 1)
    {
        rMenu->SetBuyOnly();
    }
    ///--[Error]
    else
    {
        LuaPropertyError("MonoMenu_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
