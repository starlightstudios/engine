install_name_tool -change /usr/local/opt/sdl2/lib/libSDL2-2.0.0.dylib @loader_path/osxbin/libSDL2-2.0.0.dylib PandemoniumSDLOSX
install_name_tool -change /usr/local/opt/lua/lib/liblua.dylib @loader_path/osxbin/liblua.5.4.4.dylib PandemoniumSDLOSX
install_name_tool -change /usr/local/opt/boost/lib/libboost_filesystem.dylib @loader_path/osxbin/libboost_filesystem.dylib PandemoniumSDLOSX
install_name_tool -change @loader_path/libbass.dylib @loader_path/osxbin/libbass.dylib PandemoniumSDLOSX
install_name_tool -change @loader_path/libbass_fx.dylib @loader_path/osxbin/libbass_fx.dylib PandemoniumSDLOSX
otool -L PandemoniumSDLOSX
