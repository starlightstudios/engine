//--Base
#include "CarnUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "CarnUIVendor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarPointerSeries.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Debug]
//#define CARNUIEQUIP_DEBUG
#ifdef CARNUIEQUIP_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
CarnUIEquip::CarnUIEquip()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CEQP");

    ///--[Visiblity]
    mVisibilityTimerMax = 1;

    ///--[Images]

    ///--[ ======= AdvUIEquip ======= ]
    ///--[Cursor]
    ///--[Help Strings]
    ///--[Colors]
    ///--[Images]

    ///--[ ====== CarnUIEquip ======= ]
    ///--[System]
    mConfirmUnequipCursed = false;
    mConfirmUnequipCursedTimer = 0;
    mConfirmStoredSlot = -1;

    ///--[Proficiency Data]
    mRecheckProficienciesAfterOpen = 0;
    memset(rStoredProficiencyImages, 0, sizeof(StarBitmap *) * CARNUIEQP_PROFICIENCY_SLOTS);
    mrReplacementProficiencyImages = new StarLinkedList(false);

    ///--[Description]
    mIsDescriptionMode = false;
    mHighlightName = NULL;
    mDescription = NULL;

    ///--[Hitboxes]
    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Hitboxes. Construct at program start, the selection hitboxes are always the same.
    float cEntryX = 289.0f;
    float cEntryY = 184.0f;
    float cEntryW = 307.0f;
    float cEntryH =  31.0f;
    float cEntryS =  37.0f;
    for(int i = 0; i < cxCarnScrollPage; i ++)
    {
        RegisterHitboxWH("X", cEntryX, cEntryY + (cEntryS * i), cEntryW, cEntryH);
    }

    //--Second hitbox set, the replacement hitboxes are the same as the selection ones but offset slightly.
    for(int i = 0; i < cxCarnScrollPage; i ++)
    {
        RegisterHitboxWH("X", cEntryX - 11, cEntryY + (cEntryS * i) + 16, cEntryW, cEntryH);
    }

    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 563.0f, 185.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollBd", 563.0f, 242.0f, 36.0f, 336.0f);
    RegisterHitboxWH("ScrollDn", 563.0f, 578.0f, 36.0f,  58.0f);

    //--Confirm/Cancel.
    RegisterHitboxWH("Confirm", 327.0f, 366.0f, 228.0f, 61.0f);
    RegisterHitboxWH("Cancel",  815.0f, 366.0f, 228.0f, 61.0f);

    //--Mod hitboxes.
    float cModX = 551.0f;
    float cModY = 183.0f;
    float cModW =  30.0f;
    float cModH =  30.0f;
    for(int i = 0; i < cxCarnScrollPage; i ++)
    {
        RegisterHitboxWH("X", cModX, cModY + (cEntryS * i), cModW, cModH);
    }

    //--Toggle description/statistics.
    RegisterHitboxWH("ToggleDesc", 688.0f, 652.0f, 228.0f, 61.0f);

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mReplacementSkip;
    nScrollbarPack->mPerPage = cxCarnScrollPage;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxScrollMin;
    nScrollbarPack->mIndexBd = cxScrollMin+1;
    nScrollbarPack->mIndexDn = cxScrollMin+2;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;
}
CarnUIEquip::~CarnUIEquip()
{
    delete mrReplacementProficiencyImages;
    free(mHighlightName);
    delete mDescription;
}
void CarnUIEquip::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers. These can overloaded.
    ResolveSeriesInto("Carnation Equipment UI",           &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI",              &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Equipment UI Carnation", &AdvImages,        sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",                &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CarnUIEquip::TakeForeground()
{
    ///--[Documentation]
    //--Called when this object is shown.

    ///--[Variables]
    //--Reset cursor.
    mEquipmentCursor = -1;

    //--Persistent character cursor.
    mCharacterCursor = xPersistentCharacterCursor;
    if(mCharacterCursor < 0) mCharacterCursor = 0;

    //--Reset description.
    delete mDescription;
    mDescription = NULL;

    //--Call the equipment refresh.
    mRecheckProficienciesAfterOpen = 2;
    SetCloseCode("Open Equipment");
}

///======================================= Core Methods ===========================================
void CarnUIEquip::RefreshMenuHelp()
{
}
void CarnUIEquip::RecomputeCursorPositions()
{
}
void CarnUIEquip::RecheckProficienciesForCharacter(int pCharacterSlot)
{
    ///--[Documentation]
    //--Given a character slot, populates proficiency information for that character's equipped items.
    //  This is stored as a series of images, not actual values.
    //--First, clear the slots in case something fails to resolve.
    for(int i = 0; i < CARNUIEQP_PROFICIENCY_SLOTS; i ++) rStoredProficiencyImages[i] = NULL;

    //--Fast-access pointers.
    AdvCombat *rCombat = AdvCombat::Fetch();
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(pCharacterSlot);
    if(!rAdvEntity) return;

    ///--[Execution]
    for(int i = 0; i < CARNUIEQP_PROFICIENCY_SLOTS; i ++)
    {
        //--Get the item in the slot.
        AdventureItem *rEquippedItem = rAdvEntity->GetEquipmentBySlotI(i);

        //--Run routine.
        rStoredProficiencyImages[i] = ResolveProficiencyIconForItem(rAdvEntity, rEquippedItem);
    }
}
StarBitmap *CarnUIEquip::ResolveProficiencyIconForItem(AdvCombatEntity *pCharacter, AdventureItem *pItem)
{
    ///--[Documentation]
    //--Checks the given item and character and determines how proficient the character is with that item.
    //  Only weapons actually use proficiency tags, everything else is ignored.
    //--Returns the matching image for the proficiency, but can legally return NULL if the item does
    //  not exist or if it has no proficiency tags.
    if(!pCharacter || !pItem) return NULL;

    //--Get the CarnUIVendor interface, which stores proficiency information for all characters.
    CarnUIVendor *rVendor = CarnUIVendor::Fetch();
    if(!rVendor)
    {
        fprintf(stderr, "CarnUIEquip:ResolveProficiencyIconForItem() - Error. CarnUIVendor failed to resolve.\n");
        return NULL;
    }

    //--Diagnostics.
    //fprintf(stderr, "Resolving for %s %s\n", pCharacter->GetName(), pItem->GetName());

    ///--[Execution]
    //--Get lists.
    StarLinkedList *rCharacterProficiencyList = rVendor->GetCharacterProficiencyList();
    StarLinkedList *rProficiencyRenderList = rVendor->GetProficiencyRenderList();
    if(!rCharacterProficiencyList || !rProficiencyRenderList)
    {
        fprintf(stderr, "CarnUIEquip:ResolveProficiencyIconForItem() - Error. Lists failed to resolve. %p %p\n", rCharacterProficiencyList, rProficiencyRenderList);
        return NULL;
    }

    //--Get proficiency data for the character in question.
    ProficiencyPack *rProficiencyPack = (ProficiencyPack *)rCharacterProficiencyList->GetElementByName(pCharacter->GetName());
    if(!rProficiencyPack)
    {
        fprintf(stderr, "CarnUIEquip:ResolveProficiencyIconForItem() - Error. Character %s did not have any proficiency data.\n", pCharacter->GetName());
        return NULL;
    }

    //--Iterate across the proficiency render packs. These contain tag information.
    ProficiencyRenderPack *rPackage = (ProficiencyRenderPack *)rProficiencyRenderList->PushIterator();
    while(rPackage)
    {
        //--If the tag is present on this item, it has a proficiency requirement:
        int tTags = pItem->GetTagCount(rPackage->mCheckTag);
        if(tTags > 0)
        {
            //--Stop iteration.
            rProficiencyRenderList->PopIterator();

            //--Get the code and decide which image to use.
            int tCode = rProficiencyPack->mLookups[rPackage->mLookupSlot];
            if(tCode == 1) return CarnImages.rFrame_Proficiency_Untrained;
            if(tCode == 2) return CarnImages.rFrame_Proficiency_Trained;
            if(tCode == 3) return CarnImages.rFrame_Proficiency_Skilled;
            if(tCode == 4) return CarnImages.rFrame_Proficiency_Master;
            if(tCode == 5) return CarnImages.rFrame_Proficiency_Grandmaster;

            //--Unhandled code. Return NULL.
            return NULL;
        }

        //--Next.
        rPackage = (ProficiencyRenderPack *)rProficiencyRenderList->AutoIterate();
    }

    ///--[Not Found]
    //--No proficiency tags were found for the given item. No image should render.
    return NULL;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIEquip::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ========== Documentation =========== ]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    DebugPush(true, "CarnUIEquip:UpdateForeground() - Begin.\n");

    //--Route update to background mode.
    if(pCannotHandleUpdate)
    {
        UpdateBackground();
        DebugPop("Switched to background. Completed.\n");
        return false;
    }

    ///--[Proficiencies]
    //--Because proficiency data is built after opening, this flag will be true when the UI opens
    //  on the first tick, and rebuild proficiency data.
    if(mRecheckProficienciesAfterOpen > 0)
    {
        mRecheckProficienciesAfterOpen --;
        if(mRecheckProficienciesAfterOpen == 0) RecheckProficienciesForCharacter(mCharacterCursor);
    }

    ///--[ ============== Timers ============== ]
    //--Diagnostics.
    DebugPrint("Running timers.\n");

    //--Immediately cap the vis timer.
    mVisibilityTimer = mVisibilityTimerMax;

    //--Confirmation.
    StandardTimer(mConfirmUnequipCursedTimer, 0, cxUnequipCursedTicks, mConfirmUnequipCursed);

    ///--[ ========== Mouse Handling ========== ]
    //--Retrieve the mouse coordinates. Subroutines can then use them freely.
    GetMouseInfo();
    RunHitboxCheck();

    ///--[ ============= Routing ============== ]
    //--Selection mode.
    if(!mIsReplacing)
    {
        UpdateSelection();
    }
    //--Replacement mode.
    else
    {
        UpdateReplacement();
    }

    ///--[Finish Up]
    //--We handled the update.
    DebugPop("Completed normally.\n");
    return true;
}
void CarnUIEquip::UpdateBackground()
{
    ///--[Timers]
    mVisibilityTimer = 0;
    StandardTimer(mConfirmUnequipCursedTimer, 0, cxUnequipCursedTicks, false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIEquip::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Carnation does not use by-side rendering, though it still uses piece rendering for subsections.
    //  Carnation also does not use any alphas except the primary one, UI pieces instantly switch.
    DebugPush(true, "CarnUIEquip:RenderPieces() - Begin.\n");

    //--Visibility check.
    if(mVisibilityTimer < 1) { DebugPop("Not visible. Stopping.\n"); return; }

    //--Fast-access pointers.
    AdvCombat *rCombat = AdvCombat::Fetch();
    if(!rCombat) { DebugPop("Unable to resolve AdvCombat. Failing.\n"); return; }

    ///--[Character Select]
    //--Shows the current party members.
    DebugPrint("Rendering character tabs.\n");
    RenderCharacterTabs(mCharacterCursor);

    ///--[Left Page]
    DebugPrint("Rendering item select and replacements.\n");
    RenderSelectMode();
    RenderReplaceMode();

    ///--[Common Parts]
    //--Subroutines.
    RenderTopTabs(mCodeBackward);
    RenderHexes();

    ///--[Confirmation]
    RenderConfirmWindow(pVisAlpha);

    ///--[Finish Up]
    //--Diagnostics.
    DebugPop("Completed normally.\n");
}
void CarnUIEquip::RenderConfirmWindow(float pVisAlpha)
{
    ///--[Documentation]
    //--Confirmation window, lets players confirm or cancel.
    float cColorAlpha = EasingFunction::QuadraticInOut(mConfirmUnequipCursedTimer, cxUnequipCursedTicks) * pVisAlpha;
    if(mConfirmUnequipCursedTimer < 1) return;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cColorAlpha * 0.7f));

    //--Frame.
    SetColor("Clear", cColorAlpha);
    CarnImages.rFrame_Confirm->Draw();

    //--Heading.
    CarnImages.rFont_ConfirmHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, 218.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Really Unequip Cursed Item?");

    //--Item name.
    float cTextLft = 312.0f;
    CarnImages.rFont_Description->DrawText(cTextLft, 265.0f, 0, 1.0f, "Cursed items will inflict 1 point of influence damage if unequipped in a");
    CarnImages.rFont_Description->DrawText(cTextLft, 290.0f, 0, 1.0f, "dungeon. Are you sure you want to change it?");

    //--Buttons.
    TwoDimensionReal *rConfirmHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Confirm");
    if(rConfirmHitbox)
    {
        CarnImages.rFrame_Button->Draw(rConfirmHitbox->mLft, rConfirmHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rConfirmHitbox->mXCenter, rConfirmHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Confirm");
    }
    TwoDimensionReal *rCancelHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Cancel");
    if(rCancelHitbox)
    {
        CarnImages.rFrame_Button->Draw(rCancelHitbox->mLft, rCancelHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rCancelHitbox->mXCenter, rCancelHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Cancel");
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
