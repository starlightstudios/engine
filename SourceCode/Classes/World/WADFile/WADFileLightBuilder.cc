//--Base
#include "WADFile.h"

//--Classes
#include "WorldLight.h"
#include "WorldPolygon.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"

//--While the basic Doom lighting engine is adequate in some cases, it's the modern era and we can
//  do a lot better than strict sector lighting. This engine has the capability to produce precomputed
//  lights and store them in an optional file.
//--Lights are build in a grid format as opposed to a lightmap format. First, all polygons are subdivided
//  until they cover a low surface area, which then stores lighting values for each corner. This is
//  extremely fast during rendering and doesn't require shaders.
//--Once the polys have been subdivided, all the dynamic lights in the level check if they can see the
//  various corner points, and update their light values. Normals are used to speed up computations
//  but the normals are not stored in the final file.
//--Finally, lighting information is written to a lights file, which can be the same file as the nodes file.
//  Lighting information can be used instead of the geometery from the wad file.

#define LIGHTS_DEBUG

//--[Entry Function]
void WADFile::BuildLighting()
{
    //--Call this function to construct the lighting information. It will do everything by itself.
    #ifdef LIGHTS_DEBUG
        DebugManager::ForcePrint("Subdividing polygons...\n");
    #endif
    SubdividePolygons();

    //--Build normals for all faces once they've been subdivided. This also performs precomputation
    //  of values like the DotProduct.
    #ifdef LIGHTS_DEBUG
        DebugManager::ForcePrint("Computing normals...\n");
    #endif
    ComputeNormals();

    //--If this variable is false, then the engine will not build lights.
    if(xRenderLightmaps)
    {
        #ifdef LIGHTS_DEBUG
            DebugManager::ForcePrint("Computing lights...\n");
        #endif
        ComputeLights();
    }

    //--Debug.
    #ifdef LIGHTS_DEBUG
        DebugManager::ForcePrint("Light building process complete.\n");
    #endif
}

//--[Worker Functions]
float WADFile::GetCorrectZ(WAD_FloorPoly *pPolygon, int pIndex, bool pIsFloor)
{
    //--Given a WAD_FloorPoly, returns the correct Z for the floor (or ceiling, if flagged) at the numbered vertex.
    if(!pPolygon) return 0.0f;

    //--Range check.
    if(pIndex < 0 || pIndex >= pPolygon->mVertexesTotal) return 0.0f;
    if(pPolygon->mAssociatedSector < 0 || pPolygon->mAssociatedSector >= mSectorsTotal) return 0.0f;

    //--[Floor]
    if(pIsFloor)
    {
        //--Special floor: Set by manual vertex manipulation. Takes priority.
        if(pPolygon->mVertices[pIndex].mHasSpecialFloor)
        {
            return pPolygon->mVertices[pIndex].mFloorZ;
        }

        //--Scan all linedefs. Linedefs with the mPlaneAlignFloor property can modify vertex heights, if their vertex
        //  matches the provided index.
        for(int i = 0; i < mLinedefsTotal; i ++)
        {
            //--Check for a match.
            if(mLinedefs[i].mPlaneAlignFloor == PLANE_ALIGN_NONE) continue;

            //--The vertex must match one of ours exactly on the X/Y.
            if(mVertices[mLinedefs[i].mVertex0].CompareTo(pPolygon->mVertices[pIndex]) == false && mVertices[mLinedefs[i].mVertex1].CompareTo(pPolygon->mVertices[pIndex]) == false) continue;

            //--A linedef cannot be one-sided and have a plane align case.
            if(mLinedefs[i].mSidedefL == -1) continue;

            //--Match. Check the front-to-back case.
            if(mLinedefs[i].mPlaneAlignFloor == PLANE_ALIGN_FRONT_TO_BACK)
            {
                //--If we're the front sector, we use the height of the back sector.
                if(mSidedefs[mLinedefs[i].mSidedefR].mSector == pPolygon->mAssociatedSector)
                {
                    return mSectors[mSidedefs[mLinedefs[i].mSidedefL].mSector].mHeightFloor;
                }
                //--If we're the back sector, we are not modified.
                else if(mSidedefs[mLinedefs[i].mSidedefL].mSector == pPolygon->mAssociatedSector)
                {

                }
                //--We're neither, do nothing.
                else
                {

                }
            }
            //--Check the back-to-front case.
            else
            {
                //--If we're the front sector, we are not modified.
                if(mSidedefs[mLinedefs[i].mSidedefR].mSector == pPolygon->mAssociatedSector)
                {

                }
                //--If we're the back sector, use the height of the front sector.
                else if(mSidedefs[mLinedefs[i].mSidedefL].mSector == pPolygon->mAssociatedSector)
                {
                    return mSectors[mSidedefs[mLinedefs[i].mSidedefR].mSector].mHeightFloor;
                }
                //--We're neither, do nothing.
                else
                {

                }
            }
        }

        //--Default value for the matching sector.
        return mSectors[pPolygon->mAssociatedSector].mHeightFloor;
    }

    //--[Ceiling]
    //--Special ceiling: Set by manual vertex manipulation. Takes priority.
    if(pPolygon->mVertices[pIndex].mHasSpecialCeiling)
    {
        return pPolygon->mVertices[pIndex].mCeilingZ;
    }

    //--Default value for the matching sector.
    return mSectors[pPolygon->mAssociatedSector].mHeightCeiling;
}

//--[Supporting Functions]
void WADFile::SubdividePolygons()
{
    //--Given the wall and floor polygon list, creates a series of WorldTriangles out of them. These triangles
    //  are independent of wall/floor setups.
    //--Doing this resets the rendering list.
    delete mArbitraryPolysConstructionList;
    mArbitraryPolysConstructionList = new StarLinkedList(true);

    //--Go through the wall polygons.
    for(int i = 0; i < mWallPolysTotal; i ++)
    {
        //--Error check. Must have a valid texture.
        if(!mWallPolys[i].rTexture) continue;

        //--Skip wall polys that were from 3D walls.
        if(mWallPolys[i].mIs3DWall) continue;

        //--Create and register. This will be a four-sided polygon.
        SetMemoryData(__FILE__, __LINE__);
        WorldPolygon *nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));
        nNewPolygon->Initialize();
        nNewPolygon->Allocate(4);

        //--Copy the texture reference.
        nNewPolygon->rTexture = mWallPolys[i].rTexture;

        //--Heights. These can be modified by PLANE_ALIGN cases.
        float tTLH = mWallPolys[i].mVertZ0;
        float tTRH = mWallPolys[i].mVertZ0;
        float tBLH = mWallPolys[i].mVertZ1;
        float tBRH = mWallPolys[i].mVertZ1;
        for(int p = 0; p < mLinedefsTotal; p ++)
        {
            //--Ignore the non-modifying cases.
            if(mLinedefs[p].mPlaneAlignFloor == PLANE_ALIGN_NONE || mLinedefs[p].mSidedefL == -1) continue;

            //--Store heights.
            float tHeightF = mSectors[mSidedefs[mLinedefs[p].mSidedefR].mSector].mHeightFloor;
            float tHeightB = mSectors[mSidedefs[mLinedefs[p].mSidedefL].mSector].mHeightFloor;

            //--Check vertex 0 against the linedef.
            if(mVertices[mLinedefs[p].mVertex0].mX == mWallPolys[i].mVertX0 && mVertices[mLinedefs[p].mVertex0].mY == mWallPolys[i].mVertY0)
            {
                if(mLinedefs[p].mPlaneAlignFloor == PLANE_ALIGN_BACK_TO_FRONT)
                    tTLH = tHeightF;
                else
                    tTLH = tHeightB;
            }
            else if(mVertices[mLinedefs[p].mVertex1].mX == mWallPolys[i].mVertX0 && mVertices[mLinedefs[p].mVertex1].mY == mWallPolys[i].mVertY0)
            {
                if(mLinedefs[p].mPlaneAlignFloor == PLANE_ALIGN_BACK_TO_FRONT)
                    tTLH = tHeightF;
                else
                    tTLH = tHeightB;
            }

            //--Check vertex 1 against the linedef.
            if(mVertices[mLinedefs[p].mVertex0].mX == mWallPolys[i].mVertX1 && mVertices[mLinedefs[p].mVertex0].mY == mWallPolys[i].mVertY1)
            {
                if(mLinedefs[p].mPlaneAlignFloor == PLANE_ALIGN_BACK_TO_FRONT)
                    tTRH = tHeightF;
                else
                    tTRH = tHeightB;
            }
            else if(mVertices[mLinedefs[p].mVertex1].mX == mWallPolys[i].mVertX1 && mVertices[mLinedefs[p].mVertex1].mY == mWallPolys[i].mVertY1)
            {
                if(mLinedefs[p].mPlaneAlignFloor == PLANE_ALIGN_BACK_TO_FRONT)
                    tTRH = tHeightF;
                else
                    tTRH = tHeightB;
            }
        }

        //--After modification, check if the polygon still occupies any vertical space. If not, we don't need to register, the PLANE_ALIGN
        //  align'd it right out of existence.
        if(tTLH - tBLH == 0.0f && tTRH - tBRH == 0.0f)
        {
            WorldPolygon::DeleteThis(nNewPolygon);
            continue;
        }

        //--Set the polygon's positions. This should be in the same order as the usual winding.
        nNewPolygon->SetPoint(0, mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, tTLH);
        nNewPolygon->SetPoint(1, mWallPolys[i].mVertX1, mWallPolys[i].mVertY1, tTRH);
        nNewPolygon->SetPoint(2, mWallPolys[i].mVertX1, mWallPolys[i].mVertY1, tBLH);
        nNewPolygon->SetPoint(3, mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, tBRH);
        nNewPolygon->Compress();

        //--Resolve the positions used for the texture.
        float tTexLft = 0.0f;
        float tTexRgt = 1.0f;
        float tTexTopL = 0.0f;
        float tTexTopR = 0.0f;
        float tTexBotL = 1.0f;
        float tTexBotR = 1.0f;
        {
            //--Texture vertical alignment.
            if(mWallPolys[i].mTexStoreH > 0)
            {
                //--Bottom alignment (start at floor and render up)
                if(mWallPolys[i].mIsBotAligned)
                {
                    //--Base.
                    tTexTopL = 1.0f;
                    tTexTopR = 1.0f;
                    tTexBotL = tTexTopL + ((tTLH - tBLH) / mWallPolys[i].mTexStoreH);
                    tTexBotR = tTexTopR + ((tTRH - tBRH) / mWallPolys[i].mTexStoreH);

                    //--Modify the alignment by the offset.
                    tTexTopL = tTexTopL + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    tTexTopR = tTexTopR + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    tTexBotL = tTexBotL + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    tTexBotR = tTexBotR + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                }
                //--Top alignment (start at ceiling and render down)
                else
                {
                    //--Base.
                    tTexBotL = 0.0f;
                    tTexBotR = 0.0f;
                    tTexTopL = tTexBotL - ((tTLH - tBLH) / mWallPolys[i].mTexStoreH);
                    tTexTopR = tTexBotR - ((tTRH - tBRH) / mWallPolys[i].mTexStoreH);

                    //--Modify the alignment by the offset.
                    tTexTopL = tTexTopL + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    tTexTopR = tTexTopR + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    tTexBotL = tTexBotL + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    tTexBotR = tTexBotR + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                }
            }

            //--Left alignment.
            if(mWallPolys[i].mTexStoreW > 0)
            {
                //--Compute.
                float tDistance = GetPlanarDistance(mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, mWallPolys[i].mVertX1, mWallPolys[i].mVertY1);
                tTexLft = (mWallPolys[i].mXTexOffset / mWallPolys[i].mTexStoreW);
                tTexRgt = tTexLft + ((tDistance) / mWallPolys[i].mTexStoreW);
            }
        }

        //--Store.
        nNewPolygon->SetTexCoord(0, tTexLft, tTexBotL);
        nNewPolygon->SetTexCoord(1, tTexRgt, tTexBotR);
        nNewPolygon->SetTexCoord(2, tTexRgt, tTexTopR);
        nNewPolygon->SetTexCoord(3, tTexLft, tTexTopL);

        //--Register.
        mArbitraryPolysConstructionList->AddElement("X", nNewPolygon, &WorldPolygon::DeleteThis);
    }

    //--Floor polygons. A floor polygon usually has 3 points, but can have up to 5 under normal circumstances.
    //  Fortunately, they're guaranteed to be convex and monotone in that case.
    //--We also have to build version for the ceiling and any 3D floors present. We have to handle the reversed
    //  winding order for ceiling polygons.
    for(int i = 0; i < mFloorPolysTotal; i ++)
    {
        //--[Floor Code]
        //--Make sure there's a texture.
        if(mFloorPolys[i].rFloorTexture)
        {
            //--Create and register.
            SetMemoryData(__FILE__, __LINE__);
            WorldPolygon *nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));
            mArbitraryPolysConstructionList->AddElement("X", nNewPolygon, &WorldPolygon::DeleteThis);

            //--Setup.
            nNewPolygon->Initialize();
            nNewPolygon->rTexture = mFloorPolys[i].rFloorTexture;

            //--Allocate.
            nNewPolygon->Allocate(mFloorPolys[i].mVertexesTotal);
            for(int p = 0; p < nNewPolygon->mPointsTotal; p ++)
            {
                nNewPolygon->SetPoint(p, mFloorPolys[i].mVertices[p].mX, mFloorPolys[i].mVertices[p].mY, GetCorrectZ(&mFloorPolys[i], p, true));
                nNewPolygon->SetTexCoord(p, (mFloorPolys[i].mVertices[p].mX + mSectors[mFloorPolys[i].mAssociatedSector].mXOffsetF) / 64.0f, (mFloorPolys[i].mVertices[p].mY + mSectors[mFloorPolys[i].mAssociatedSector].mYOffsetF) / 64.0f);
            }
            nNewPolygon->Compress();
        }

        //--[Ceiling Code]
        //--Texture check.
        if(mFloorPolys[i].rCeilingTexture)
        {
            //--Create and register.
            SetMemoryData(__FILE__, __LINE__);
            WorldPolygon *nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));
            mArbitraryPolysConstructionList->AddElement("X", nNewPolygon, &WorldPolygon::DeleteThis);

            //--Setup.
            nNewPolygon->Initialize();
            nNewPolygon->rTexture = mFloorPolys[i].rCeilingTexture;

            //--Allocate. Make sure to set in the opposite winding order.
            nNewPolygon->Allocate(mFloorPolys[i].mVertexesTotal);
            for(int p = 0; p < nNewPolygon->mPointsTotal; p ++)
            {
                nNewPolygon->SetPoint(nNewPolygon->mPointsTotal - p - 1, mFloorPolys[i].mVertices[p].mX, mFloorPolys[i].mVertices[p].mY, GetCorrectZ(&mFloorPolys[i], p, false));
                nNewPolygon->SetTexCoord(nNewPolygon->mPointsTotal - p - 1, (mFloorPolys[i].mVertices[p].mX + mSectors[mFloorPolys[i].mAssociatedSector].mXOffsetC) / 64.0f, (mFloorPolys[i].mVertices[p].mY + mSectors[mFloorPolys[i].mAssociatedSector].mYOffsetC) / 64.0f);
            }
            nNewPolygon->Compress();
        }

        //--[3D Floors]
        //--Optional, but they do exist.
        for(int p = 0; p < mFloorPolys[i].m3DFloorsTotal; p ++)
        {
            //--[Checks]
            //--Check to make sure the sector identifier is within range.
            if(mFloorPolys[i].m3DFloorReferenceSlots[p] < 0 || mFloorPolys[i].m3DFloorReferenceSlots[p] >= mSectorsTotal) continue;

            //--Get the control sector.
            UDMFSector *rControlSector = &mSectors[mFloorPolys[i].m3DFloorReferenceSlots[p]];
            StarBitmap *rCheckFloor = GetFlatTexture(rControlSector->mTexFloor);
            StarBitmap *rCheckCeiling = GetFlatTexture(rControlSector->mTexCeiling);
            if(!rCheckCeiling || !rCheckFloor) continue;

            //--[Top]
            //--Create a polygon for the top of the 3D floor. This is the ceiling of the control sector.
            SetMemoryData(__FILE__, __LINE__);
            WorldPolygon *nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));
            mArbitraryPolysConstructionList->AddElement("X", nNewPolygon, &WorldPolygon::DeleteThis);

            //--Setup.
            nNewPolygon->Initialize();
            nNewPolygon->rTexture = rCheckCeiling;

            //--Allocate and fill.
            nNewPolygon->Allocate(mFloorPolys[i].mVertexesTotal);
            for(int o = 0; o < nNewPolygon->mPointsTotal; o ++)
            {
                nNewPolygon->SetPoint(o, mFloorPolys[i].mVertices[o].mX, mFloorPolys[i].mVertices[o].mY, rControlSector->mHeightCeiling);
                nNewPolygon->SetTexCoord(o, (mFloorPolys[i].mVertices[o].mX + mSectors[mFloorPolys[i].mAssociatedSector].mXOffsetC) / 64.0f, (mFloorPolys[i].mVertices[o].mY + mSectors[mFloorPolys[i].mAssociatedSector].mYOffsetC) / 64.0f);
            }
            nNewPolygon->Compress();

            //--[Bottom]
            //--Create a polygon for the bottom of the 3D floor. This is the floor of the control sector.
            SetMemoryData(__FILE__, __LINE__);
            nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));
            mArbitraryPolysConstructionList->AddElement("X", nNewPolygon, &WorldPolygon::DeleteThis);

            //--Setup.
            nNewPolygon->Initialize();
            nNewPolygon->rTexture = rCheckFloor;

            //--Allocate and fill.
            nNewPolygon->Allocate(mFloorPolys[i].mVertexesTotal);
            for(int o = 0; o < nNewPolygon->mPointsTotal; o ++)
            {
                nNewPolygon->SetPoint(nNewPolygon->mPointsTotal - o - 1, mFloorPolys[i].mVertices[o].mX, mFloorPolys[i].mVertices[o].mY, rControlSector->mHeightFloor);
                nNewPolygon->SetTexCoord(nNewPolygon->mPointsTotal - o - 1, (mFloorPolys[i].mVertices[o].mX + mSectors[mFloorPolys[i].mAssociatedSector].mXOffsetF) / 64.0f, (mFloorPolys[i].mVertices[o].mY + mSectors[mFloorPolys[i].mAssociatedSector].mYOffsetF) / 64.0f);
            }
            nNewPolygon->Compress();

            //--[Sides]
            //--Creates the side polygons for this polygon. The walls are always perfectly vertical quads.
            for(int o = 0; o < mFloorPolys[i].mVertexesTotal; o ++)
            {
                //--Make sure the matching linedef is legal.
                if(mFloorPolys[i].m3DFloorLinedefIndex[p] < 0 || mFloorPolys[i].m3DFloorLinedefIndex[p] >= mLinedefsTotal) continue;

                //--Create and allocate.
                SetMemoryData(__FILE__, __LINE__);
                nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));

                //--Setup.
                nNewPolygon->Initialize();
                nNewPolygon->rTexture = GetWallTexture(mSidedefs[mLinedefs[mFloorPolys[i].m3DFloorLinedefIndex[p]].mSidedefR].mTexMid);
                if(!nNewPolygon->rTexture) { WorldPolygon::DeleteThis(nNewPolygon); continue; }
                mArbitraryPolysConstructionList->AddElement("X", nNewPolygon, &WorldPolygon::DeleteThis);

                //--To make sure everything is facing the right way, the linedefs for a 3D wall are always assumed to be facing outwards in
                //  their winding order. It is up to the mapper to fix errors, not the engine, because a 3D wall cannot verify using
                //  sector heights which way it should be facing.
                int u = (o + 1) % mFloorPolys[i].mVertexesTotal;
                nNewPolygon->Allocate(4);
                nNewPolygon->SetPoint(0, mFloorPolys[i].mVertices[o].mX, mFloorPolys[i].mVertices[o].mY, rControlSector->mHeightFloor);
                nNewPolygon->SetPoint(1, mFloorPolys[i].mVertices[u].mX, mFloorPolys[i].mVertices[u].mY, rControlSector->mHeightFloor);
                nNewPolygon->SetPoint(2, mFloorPolys[i].mVertices[u].mX, mFloorPolys[i].mVertices[u].mY, rControlSector->mHeightCeiling);
                nNewPolygon->SetPoint(3, mFloorPolys[i].mVertices[o].mX, mFloorPolys[i].mVertices[o].mY, rControlSector->mHeightCeiling);
                nNewPolygon->Compress();

                //--Resolve the positions used for the texture.
                float tTexLft = 0.0f;
                float tTexRgt = 1.0f;
                float tTexTop = 0.0f;
                float tTexBot = 1.0f;
                float tTexWid = nNewPolygon->rTexture->GetWidth();
                float tTexHei = nNewPolygon->rTexture->GetHeight();
                {
                    //--Bottom alignment (start at floor and render up)
                    if(mLinedefs[mFloorPolys[i].m3DFloorLinedefIndex[p]].mIsLowerUnpegged)
                    {
                        //--Base.
                        tTexTop = 1.0f;
                        tTexBot = tTexTop + ((rControlSector->mHeightFloor- rControlSector->mHeightCeiling) / tTexHei);

                        //--Modify the alignment by the offset.
                        tTexTop = tTexTop + (mSidedefs[mLinedefs[mFloorPolys[i].m3DFloorLinedefIndex[p]].mSidedefR].mOffsetY / tTexHei);
                        tTexBot = tTexBot + (mSidedefs[mLinedefs[mFloorPolys[i].m3DFloorLinedefIndex[p]].mSidedefR].mOffsetY / tTexHei);
                    }
                    //--Top alignment (start at ceiling and render down)
                    else
                    {
                        //--Base.
                        tTexBot = 0.0f;
                        tTexTop = tTexBot - ((rControlSector->mHeightCeiling - rControlSector->mHeightFloor) / tTexHei);

                        //--Modify the alignment by the offset.
                        tTexTop = tTexTop + (mSidedefs[mLinedefs[mFloorPolys[i].m3DFloorLinedefIndex[p]].mSidedefR].mOffsetY / tTexHei);
                        tTexBot = tTexBot + (mSidedefs[mLinedefs[mFloorPolys[i].m3DFloorLinedefIndex[p]].mSidedefR].mOffsetY / tTexHei);
                    }

                    //--Compute.
                    float tDistance = GetPlanarDistance(mFloorPolys[i].mVertices[o].mX, mFloorPolys[i].mVertices[o].mY, mFloorPolys[i].mVertices[u].mX, mFloorPolys[i].mVertices[u].mY);
                    tTexLft = (mSidedefs[mLinedefs[mFloorPolys[i].m3DFloorLinedefIndex[p]].mSidedefR].mOffsetX / tTexWid);
                    tTexRgt = tTexLft + ((tDistance) / tTexWid);
                }

                //--Store.
                nNewPolygon->SetTexCoord(0, tTexRgt, tTexTop);
                nNewPolygon->SetTexCoord(1, tTexLft, tTexTop);
                nNewPolygon->SetTexCoord(2, tTexLft, tTexBot);
                nNewPolygon->SetTexCoord(3, tTexRgt, tTexBot);
            }
        }
    }

    //--Environmental geometry is handled in WADFileThings.cc
    BuildEnvironmentGeometry();

    //--We no longer need the wall polygons.
    glDeleteLists(mListHandle, 1);
    mListHandle = 0;
}
void WADFile::ComputeNormals()
{
    //--All polygons will compute and store their normal vectors when this is called. It greatly speeds
    //  up light computations, since a surface cannot be lit by a light it is facing away from.
    //--Normals can be seen if wireframes are turned on during rendering.
    WorldPolygon *rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rWorldPolygon)
    {
        rWorldPolygon->ComputeNormal();
        rWorldPolygon->ComputeDotProduct();
        rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Additionally, all WorldPolygons reposition at this stage, before lighting is built.
    rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rWorldPolygon)
    {
        for(int i = 0; i < rWorldPolygon->mPointsTotal; i ++)
        {
            //--[Main Floor]
            //--Does nothing.
            if(rWorldPolygon->mPoints[i].mX < 2830.0f)
            {

            }
            //--[Wine Cellar]
            else if(rWorldPolygon->mPoints[i].mX < 2940.0f)
            {
                rWorldPolygon->mPoints[i].mX = rWorldPolygon->mPoints[i].mX -  5808.0f +  736.0f + (2944.0f - 2832.0f);
                rWorldPolygon->mPoints[i].mY = rWorldPolygon->mPoints[i].mY - -176.0f  +  144.0f;
            }
            //--[Basement]
            //--Shifts over to be under the main floor.
            else if(rWorldPolygon->mPoints[i].mX < 6700.0f)
            {
                rWorldPolygon->mPoints[i].mX = rWorldPolygon->mPoints[i].mX -  5808.0f +  736.0f;
                rWorldPolygon->mPoints[i].mY = rWorldPolygon->mPoints[i].mY - -176.0f  +  144.0f;
            }
            //--[Top Floor]
            //--Shifts left to be over the main floor.
            else
            {
                rWorldPolygon->mPoints[i].mX = rWorldPolygon->mPoints[i].mX -  7856.0f +  -800.0f;
                rWorldPolygon->mPoints[i].mY = rWorldPolygon->mPoints[i].mY - -2784.0f + -1152.0f;
            }

        }
        rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Move things following the same basic logic as above.
    for(int i = 0; i < mWorldSpritesTotal; i ++)
    {
        //--[Main Floor]
        if(mWorldSprites[i].mX < 2830.0f)
        {

        }
        //--[Wine Cellar]
        else if(mWorldSprites[i].mX < 2940.0f)
        {
            mWorldSprites[i].mX = mWorldSprites[i].mX -  5808.0f +  736.0f + (2944.0f - 2832.0f);
            mWorldSprites[i].mY = mWorldSprites[i].mY - -176.0f  +  144.0f;
        }
        //--[Basement]
        else if(mWorldSprites[i].mX < 6700.0f)
        {
            mWorldSprites[i].mX = mWorldSprites[i].mX -  5808.0f +  736.0f;
            mWorldSprites[i].mY = mWorldSprites[i].mY - -176.0f  +  144.0f;
        }
        //--[Top Floor]
        else
        {
            mWorldSprites[i].mX = mWorldSprites[i].mX -  7856.0f +  -800.0f;
            mWorldSprites[i].mY = mWorldSprites[i].mY - -2784.0f + -1152.0f;
        }
    }
}
void WADFile::ComputeLights()
{
    //--Given a set of wall and floor polygons, goes through all the vertex points on them and computes
    //  how much light is hitting them, setting their light values accordingly. A list of lights is constructed
    //  for quick-access before this.
    //--Note to self: I'm using m's for this. Probably want to build a permanent light list later.
    if(mLightsTotal < 1) return;
    for(int i = 0; i < mLightsTotal; i ++)
    {
        mLightList[i]->StoreShadowRefs(mArbitraryPolysConstructionList);
    }

    //--Counter.
    int tCount = 0;
    int tTotalPoly = mArbitraryPolysConstructionList->GetListSize();

    //--Timing.
    DebugManager::ForcePrint("Building lights.\n");
    float tStartTime = GetGameTime();

    //--Iterate across the polygons.
    WorldPolygon *rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rWorldPolygon)
    {
        //--Debug.
        if(tCount % 100 == 0) DebugManager::ForcePrint("Building lights on polygon %3i/%3i. %.1f %% complete.\n", tCount, tTotalPoly, ((float)tCount / (float)tTotalPoly) * 100.0f);
        tCount ++;

        //--If there's an error, skip this polygon.
        rWorldPolygon->SetupLightmap();
        if(rWorldPolygon->mPointsTotal < 3 || rWorldPolygon->mLightmapXSize == 0 || rWorldPolygon->mLightmapYSize == 0)
        {
            rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Step across the lightmap.
        for(int x = 0; x < rWorldPolygon->mLightmapXSize; x ++)
        {
            for(int y = 0; y < rWorldPolygon->mLightmapYSize; y ++)
            {
                //--Figure out the world position.
                float tWorldX = rWorldPolygon->mLightmapTL.mX + (rWorldPolygon->mLightmapXStep.mX * ((float)x+0.5f)) + (rWorldPolygon->mLightmapYStep.mX * ((float)y+0.5f));
                float tWorldY = rWorldPolygon->mLightmapTL.mY + (rWorldPolygon->mLightmapXStep.mY * ((float)x+0.5f)) + (rWorldPolygon->mLightmapYStep.mY * ((float)y+0.5f));
                float tWorldZ = rWorldPolygon->mLightmapTL.mZ + (rWorldPolygon->mLightmapXStep.mZ * ((float)x+0.5f)) + (rWorldPolygon->mLightmapYStep.mZ * ((float)y+0.5f));

                for(int i = 0; i < rWorldPolygon->mPointsTotal; i ++)
                {
                    float tDistance = Get3DDistance(rWorldPolygon->mPoints[i].mX, rWorldPolygon->mPoints[i].mY, rWorldPolygon->mPoints[i].mZ, tWorldX, tWorldY, tWorldZ);
                    if(tDistance < rWorldPolygon->mLightmapCoords[i].mZ || rWorldPolygon->mLightmapCoords[i].mZ == -100.0f)
                    {
                        rWorldPolygon->mLightmapCoords[i].mX = (float)x / (float)rWorldPolygon->mLightmapXSize;
                        rWorldPolygon->mLightmapCoords[i].mY = (float)y / (float)rWorldPolygon->mLightmapYSize;
                        rWorldPolygon->mLightmapCoords[i].mZ = tDistance;
                        if(rWorldPolygon->mPointsTotal == 4 && rWorldPolygon->mPoints[0].mZ == 16.0f && rWorldPolygon->mPoints[1].mZ == 32.0f && rWorldPolygon->mPoints[2].mZ == 32.0f && rWorldPolygon->mPoints[3].mZ == 16.0f)
                        {
                            //fprintf(stderr, "Set lightmap on polygon point %i to %f %f\n", i, rWorldPolygon->mLightmapCoords[i].mX, rWorldPolygon->mLightmapCoords[i].mY);
                        }
                    }
                }

                //--If the step rate is 0 in a direction, move towards the light slightly. This prevents surfaces
                //  that are parallel and adjacent from shadowing each other.
                float cIndent = 0.5f;
                if(rWorldPolygon->mNormal.mX == 0.0f && rWorldPolygon->mNormal.mY == 0.0f)
                {
                    tWorldZ = tWorldZ + (rWorldPolygon->mNormal.mZ * cIndent);
                }

                //--Check the distance and set the lightmap accordingly.
                for(int i = 0; i < mLightsTotal; i ++)
                {
                    //--Setup.
                    float cDistanceScale = 1.0f;
                    Point3D tLightPosition = mLightList[i]->GetPosition();

                    //--Skip the light if this polygon is facing away from it, or, if flagged, increase the distance to emulate
                    //  light bounces.
                    bool tForceSkipShadows = false;
                    if(!rWorldPolygon->IsFacingPoint(tLightPosition))
                    {
                        //--If this is greater than zero, we light the face but at a much increased distance. Backfaces also never
                        //  check shadows, since they're nominally already in the shade.
                        if(WorldLight::cxBackfaceDistanceScaler > 0.0f)
                        {
                            tForceSkipShadows = true;
                            cDistanceScale = WorldLight::cxBackfaceDistanceScaler;
                        }
                        //--If the value was zero, we don't light the backface. This is much faster but looks uglier.
                        //  You can use ambient lighting to offset the negative effects of this, and that's a lot faster.
                        else
                        {
                            continue;
                        }
                    }

                    //--Get the data from the light.
                    float tIntensityAtDistance = mLightList[i]->GetIntensityAtPoint(tWorldX, tWorldY, tWorldZ, cDistanceScale);

                    //--If the intensity is very low, we don't bother to add it. This saves time computing shadows.
                    if(tIntensityAtDistance < WorldLight::cxMinIntensity) continue;

                    //--Don't compute shadows? Just go by distance.
                    if(WorldLight::xSkipShadows || tForceSkipShadows)
                    {
                        rWorldPolygon->AddLightmap(x, y, tIntensityAtDistance);
                    }
                    //--Compute shadows.
                    else
                    {
                        //--Set this light's list as the active one. Only faces fairly close can cast shadows.
                        rActiveShadowList = mLightList[i]->GetShadowCasterList();
                        if(!DoesLineCrossFace(tWorldX, tWorldY, tWorldZ, tLightPosition.mX, tLightPosition.mY, tLightPosition.mZ, rWorldPolygon))
                        {
                            rWorldPolygon->AddLightmap(x, y, tIntensityAtDistance);
                        }
                        //--If the line crosses a shadow, greatly increase its effective distance and apply it anyway.
                        //  At short ranges, this causes shadows to become less "hard".
                        else if(WorldLight::cxShadowDistanceScaler > 0.0f)
                        {
                            tIntensityAtDistance = mLightList[i]->GetIntensityAtPoint(tWorldX, tWorldY, tWorldZ, cDistanceScale + WorldLight::cxShadowDistanceScaler);
                            rWorldPolygon->AddLightmap(x, y, tIntensityAtDistance);
                        }
                        //--If the constant is negative, we divide the intensity by the absolute value of the scaler. This
                        //  produces a different shadow effect.
                        else if(WorldLight::cxShadowDistanceScaler < 0.0f)
                        {
                            rWorldPolygon->AddLightmap(x, y, tIntensityAtDistance / fabsf(WorldLight::cxShadowDistanceScaler));
                        }
                        //--If the scaler value is exactly zero, shadows are perfectly hard.
                        else
                        {

                        }

                        //--Clear the shadow caster list.
                        rActiveShadowList = NULL;
                    }
                }
            }
        }

        //--Send the information to the graphics card.
        rWorldPolygon->FinalizeLightmap();

        //--Next.
        rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Timing.
    float tEndTime = GetGameTime();
    DebugManager::ForcePrint("Lights complete, took %f seconds.\n", tEndTime - tStartTime);
}
