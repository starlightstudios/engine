//--Base
//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions

//--Libraries
//--Managers

void IncrementWithSkipAndPages(int &sCursor, int &sSkip, int pMoves, int pPageSize, int pListSize, int pLoBuf, int pHiBuf, bool pAllowWrap)
{
    ///--[Documentation and Setup]
    //--Standardized list cursor handling. Typically a player presses up or down to move a cursor up or down on a list, and can
    //  optionally hold a button to move several entries at once. The list can be longer than what fits on the screen at once so
    //  a "skip" value indicates how many entries to not display off the top. The cursor and skip need to be adjusted at the
    //  same time and to dynamically clamp to page scroll limits.
    //--Moves is how many entries to move and in which direction, negative being up. Zero is invalid and does nothing.
    //--pLoBuf is how many entries should be "above" the cursor and still allow scrolling, can be zero.
    //--pHiBuf is how many entries should be "below" the cursor and still allow scrolling, can be zero.
    if(pMoves == 0) return;
    if(pListSize <= 1 || pPageSize < 2) return;

    ///--[Increment, Move Cursor Down]
    if(pMoves > 0)
    {
        //--For each move:
        for(int i = 0; i < pMoves; i ++)
        {
            //--Set.
            sCursor ++;

            //--Wrap if needed. Can only occur on the first iteration, otherwise, the cursor clamps to the end of the list.
            if(sCursor >= pListSize && i == 0 && pAllowWrap)
            {
                sCursor = 0;
                sSkip = 0;
                break;
            }
            //--Clamp to end of list.
            else if(sCursor >= pListSize)
            {
                sCursor = pListSize - 1;
                break;
            }
            //--Check scrolling.
            else
            {
                //--Resolve the cursor relative to where it is on the page.
                int tEffectiveCursor = sCursor - sSkip;

                //--Cursor goes over the buffer.
                if(tEffectiveCursor >= (pPageSize - pHiBuf))
                {
                    //--Increment.
                    sSkip ++;

                    //--Clamp against the end of the list.
                    if(sSkip + pPageSize > pListSize)
                    {
                        sSkip --;
                    }
                }
            }
        }
    }
    ///--[Decrement, Move Cursor Up]
    else
    {
        //--Switch the move counter to its negative.
        pMoves = pMoves * -1;

        //--For each move:
        for(int i = 0; i < pMoves; i ++)
        {
            //--Set.
            sCursor --;

            //--Wrap if needed. Can only occur on the first iteration, otherwise, the cursor clamps to the end of the list.
            if(sCursor < 0 && i == 0)
            {
                sCursor = pListSize - 1;
                sSkip = sCursor - pPageSize + 1;
                if(sSkip < 0) sSkip = 0;
                break;
            }
            //--Just clamp on successive passes.
            else if(sCursor < 0)
            {
                sCursor = 0;
                break;
            }
            //--Offset scrolling.
            else
            {
                //--Resolve the cursor relative to where it is on the page.
                int tEffectiveCursor = sCursor - sSkip;

                //--Cursor goes under the buffer.
                if(tEffectiveCursor < pLoBuf)
                {
                    //--Set.
                    sSkip --;

                    //--Clamp against zero.
                    if(sSkip < 0)
                    {
                        sSkip ++;
                    }
                }
            }
        }
    }
}
void RenderScrollbarFront(float pLft, float pTop, float pBot, int pLowest, int pHighest, int pMaximum, float pIndent, StarBitmap *pImage)
{
    //--Renders the front of a scrollbar to the given position, given where it is in the list and how much of the list it occupies.
    //--Do not use this for StarUIPiece objects, there is a standard function in the class.
    if(!pImage || pMaximum < 1) return;

    //--Percentages.
    float cPercentTop = pLowest  / (float)pMaximum;
    float cPercentBot = pHighest / (float)pMaximum;

    //--Compute the positions of the top and bottom of the scrollbar.
    float cPositionTop = pTop + (cPercentTop * (pBot - pTop));
    float cPositionBot = pTop + (cPercentBot * (pBot - pTop));

    //--If the percentage between the top and bottom is less than the height of the scrollbar,
    //  just render the scrollbar normally.
    if(cPositionBot - cPositionTop < pImage->GetHeight())
    {
        pImage->Draw(pLft, cPositionTop);
    }
    //--Render a top, bottom, and middle.
    else
    {
        //--Setup.
        pImage->Bind();
        glBegin(GL_QUADS);

        //--Texture percent.
        float cTxInd = pIndent / (pImage->GetHeightSafe());

        //--Common.
        float cTxL = 0.0f;
        float cTxR = 1.0f;
        float cLft = pLft;
        float cRgt = cLft + pImage->GetWidth();

        //--Top section.
        float cTxT = 1.0f;
        float cTxB = 1.0f - cTxInd;
        float cTop = cPositionTop;
        float cBot = cTop + pIndent;
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);

        //--Bottom section.
        cTxT = 0.0f + cTxInd;
        cTxB = 0.0f;
        cTop = cPositionBot - pIndent;
        cBot = cPositionBot;
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);

        //--Middle section.
        cTxT = 1.0f - cTxInd;
        cTxB = cTxInd;
        cTop = cPositionTop + pIndent;
        cBot = cPositionBot - pIndent;
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);

        glEnd();
    }
}
