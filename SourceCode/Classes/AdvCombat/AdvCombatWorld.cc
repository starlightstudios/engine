//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

///========================================= Functions ============================================
void AdvCombat::PulseWorld(bool pDontAnimate, bool pDontCheckEnemies, bool pIsPolling)
{
    ///--[Documentation and Setup]
    //--When combat is active, the map file will animate an overlay causing combat to crossfade. It optionally
    //  checks for nearby enemies and adds them as reinforcements. This occurs after the reinitialization of
    //  the AdvCombat class and typically right after the Activate() call.

    ///--[UI Change]
    //--Activate the reinforcement pulse sequence. This renders the overlay.
    if(!pDontAnimate)
    {
        AdventureLevel *rLevel = AdventureLevel::Fetch();
        if(rLevel) rLevel->SetReinforcementPulseMode(true);
    }

    ///--[Enemy Reinforcements]
    //--Run the world pulse to add enemies. This does not occur if flagged. The flag is often false for
    //  script-activated combats. It defaults to true for most world encounters.
    if(!pDontCheckEnemies)
    {
        //--Get a list of entities.
        StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

        //--Run through the entity list.
        RootEntity *rEntity = (RootEntity *)rEntityList->PushIterator();
        while(rEntity)
        {
            //--The entity must be a TilemapActor. We are not interested in anything else.
            if(rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                //--Cast.
                TilemapActor *rTilemapActor = (TilemapActor *)rEntity;

                //--Run the internal routine.
                rTilemapActor->ComputeReinforcement(pIsPolling);
            }

            //--Next.
            rEntity = (RootEntity *)rEntityList->AutoIterate();
        }
    }
}
bool AdvCombat::DoAnyEnemiesBlockAutoWin()
{
    //--Returns true if any of the enemies currently on the world reference list block auto-wins.
    WorldRefPack *rPackage = (WorldRefPack *)mWorldReferences->PushIterator();
    while(rPackage)
    {
        //--Flag set.
        if(rPackage->mBlocksMugAutoWin)
        {
            mWorldReferences->PopIterator();
            return true;
        }

        //--Next.
        rPackage = (WorldRefPack *)mWorldReferences->AutoIterate();
    }

    //--None found.
    return false;
}
bool AdvCombat::CheckAutoWin(TilemapActor *pCaller)
{
    ///--[Documentation and Setup]
    //--When an enemy is mugged and auto-win is possible, this subroutine collects all adjacent enemy cases,
    //  calls a lua script to check if an auto-win has occurred, and handles cleaning up any defeated enemies
    //  if it does.
    //--Returns true if the auto-win took place. The calling entity should stop executing at that point.
    if(!mAutoWinPath) return false;

    ///--[Variable Assembly]
    //--Clear and reinialize the combat handler as if prepping for a battle.
    Reinitialize(false);

    //--Pulse the world to assemble the enemies that will be participating.
    TilemapActor::xPulseIsNotCombat = true;
    PulseWorld(true, false, true);
    TilemapActor::xPulseIsNotCombat = false;

    //--Trim off all enemies that are on the list as reinforcements.
    WorldRefPack *rPackage = (WorldRefPack *)mWorldReferences->SetToHeadAndReturn();
    while(rPackage)
    {
        //--Remove if over zero.
        if(rPackage->mTurns > 0)
        {
            mWorldReferences->RemoveRandomPointerEntry();
        }

        //--Next.
        rPackage = (WorldRefPack *)mWorldReferences->IncrementAndGetRandomPointerEntry();
    }

    ///--[Run Subroutine]
    //--We now have in memory a list of all enemies participating immediately in the battle. Run the script
    //  file. If the file trips the flag, then it determined an auto-win should take place.
    mAutoWinFlag = false;
    LuaManager::Fetch()->ExecuteLuaFile(mAutoWinPath);

    ///--[Subroutine Fail]
    //--Flag was not tripped, so nothing needs to be done.
    if(!mAutoWinFlag) return false;

    ///--[KO Enemies]
    //--Flag was tripped. All enemies on the world references list are defeated, player is awarded requisite items
    //  XP JP etc. First, order all defeated enemies to do their defeat routine.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    rPackage = (WorldRefPack *)mWorldReferences->PushIterator();
    while(rPackage)
    {
        //--Exec.
        rPackage->rActor->BeginDying();
        rActiveLevel->KillEnemy(rPackage->rActor->GetUniqueEnemyName());

        //--Next.
        rPackage = (WorldRefPack *)mWorldReferences->AutoIterate();
    }

    ///--[Goodies Awarding]
    //--Give the player EXP/JP/Platina etc. First, setup.
    mXPToAwardStorage = 0;
    mJPToAwardStorage = 0;
    mPlatinaToAwardStorage = 0;
    mDoctorToAwardStorage = 0;
    mItemsToAwardList->ClearList();

    //--Run across all enemies and tally values.
    AdvCombatEntity *rDefeatedEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rDefeatedEntity)
    {
        //--Totals.
        mXPToAwardStorage      += rDefeatedEntity->GetRewardXP();
        mJPToAwardStorage      += rDefeatedEntity->GetRewardJP();
        mPlatinaToAwardStorage += rDefeatedEntity->GetRewardPlatina();

        //--Doctor bag is not handled.

        //--Add any items to the list.
        StarLinkedList *rItemsList = rDefeatedEntity->GetRewardList();
        char *rItemName = (char *)rItemsList->SetToHeadAndReturn();
        while(rItemName)
        {
            rItemsList->LiberateRandomPointerEntry();
            mItemsToAwardList->AddElement("X", rItemName, &FreeThis);
            rItemName = (char *)rItemsList->SetToHeadAndReturn();
        }

        //--Next.
        rDefeatedEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--Debug.
    if(false)
    {
        fprintf(stderr, "After scan, rewards:\n");
        fprintf(stderr, " XP: %i\n", mXPToAwardStorage);
        fprintf(stderr, " JP: %i\n", mJPToAwardStorage);
        fprintf(stderr, " PL: %i\n", mPlatinaToAwardStorage);
        fprintf(stderr, " Items: %i\n", mItemsToAwardList->GetListSize());
    }

    //--Provide.
    AwardXP(mXPToAwardStorage);
    AwardJP(mJPToAwardStorage);
    AwardPlatina(mPlatinaToAwardStorage);

    //--Add items.
    const char *rItemName = (const char *)mItemsToAwardList->PushIterator();
    while(rItemName)
    {
        //--Add the item to the inventory.
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rItemName);

        //--Next.
        rItemName = (const char *)mItemsToAwardList->AutoIterate();
    }

    ///--[SFX]
    AudioManager::Fetch()->PlaySound("Combat|MugVictory");

    ///--[Display]
    //--Order the calling actor, if it exists, to display the results.
    if(pCaller)
    {
        pCaller->SpawnAutoWinNotices(mXPToAwardStorage, mJPToAwardStorage, mPlatinaToAwardStorage, mItemsToAwardList);
    }

    return true;
}
void AdvCombat::TripAutoWin()
{
    mAutoWinFlag = true;
}
