//--Base
#include "AdventureInventory.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "MapManager.h"

///========================================== System ==============================================
///===================================== Property Queries =========================================
int AdventureInventory::GetCatalystCount(int pType)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return 0;
    return mCatalystCounts[pType];
}
int AdventureInventory::GetCatalystLocalCur(int pType)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return 0;
    return mCatalystCountsLocalCur[pType];
}
int AdventureInventory::GetCatalystLocalMax(int pType)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return 0;
    return mCatalystCountsLocalTotal[pType];
}

///======================================= Manipulators ===========================================
void AdventureInventory::AddCatalyst(int pType)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return;
    mCatalystCounts[pType] ++;;
}
void AdventureInventory::SetCatalystCount(int pType, int pAmount)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return;
    if(pAmount < 0) pAmount = 0;
    mCatalystCounts[pType] = pAmount;
}
void AdventureInventory::SetCatalystLocalCur(int pType, int pAmount)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return;
    if(pAmount < 0) pAmount = 0;
    mCatalystCountsLocalCur[pType] = pAmount;
}
void AdventureInventory::SetCatalystLocalMax(int pType, int pAmount)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return;
    if(pAmount < 0) pAmount = 0;
    mCatalystCountsLocalTotal[pType] = pAmount;
}

///======================================= Core Methods ===========================================
int AdventureInventory::ComputeCatalystBonus(int pType)
{
    //--Computes and returns the catalyst bonus for the given type. Can return zero.

    //--Health.
    if(pType == CATALYST_HEALTH)
    {
        return (mCatalystCounts[CATALYST_HEALTH] / CATALYST_NEEDED_HEALTH) * CATALYST_BUFF_HEALTH;
    }

    //--Attack power.
    if(pType == CATALYST_ATTACK)
    {
        return (mCatalystCounts[CATALYST_ATTACK] / CATALYST_NEEDED_ATTACK) * CATALYST_BUFF_ATTACK;
    }

    //--Initiative.
    if(pType == CATALYST_INITIATIVE)
    {
        return (mCatalystCounts[CATALYST_INITIATIVE] / CATALYST_NEEDED_INITIATIVE) * CATALYST_BUFF_INITIATIVE;
    }

    //--Dodge.
    if(pType == CATALYST_DODGE)
    {
        return (mCatalystCounts[CATALYST_DODGE] / CATALYST_NEEDED_DODGE) * CATALYST_BUFF_DODGE;
    }

    //--Accuracy.
    if(pType == CATALYST_ACCURACY)
    {
        return (mCatalystCounts[CATALYST_ACCURACY] / CATALYST_NEEDED_ACCURACY) * CATALYST_BUFF_ACCURACY;
    }

    //--Skill.
    if(pType == CATALYST_SKILL)
    {
        return (mCatalystCounts[CATALYST_SKILL] / CATALYST_NEEDED_SKILL);
    }

    //--Inaccessable code, here for safety.
    return 0;
}
void AdventureInventory::SetCatalystCountsByGlobalString(const char *pString)
{
    ///--[ ====== Documentation ===== ]
    //--When loading a game or starting a new chapter, the program builds a list of all catalyst chests
    //  and what room they are in. This function uses the given string to run across this list, checking
    //  which chests are opened and counting the number of catalysts based on the results.
    //--Note that this could lead to desyncs in the number of available catalysts if those catalysts
    //  get awarded via other means. This is the scripter's problem. This function checks what chests are open.
    bool tPrintDiagnostics = DebugManager::GetDebugFlag("Catalysts: All") || DebugManager::GetDebugFlag("Catalysts: Loading");
    DebugManager::PushPrint(tPrintDiagnostics, "AdventureInventory::SetCatalystCountsByGlobalString() - Setting catalyst counts using string %s\n", pString);

    //--Lookup table used for diagnostics.
    char tNameLookups[CATALYST_TOTAL][32];
    strcpy(tNameLookups[CATALYST_HEALTH],     "Health");
    strcpy(tNameLookups[CATALYST_ATTACK],     "Attack");
    strcpy(tNameLookups[CATALYST_INITIATIVE], "Initiative");
    strcpy(tNameLookups[CATALYST_DODGE],      "Evade");
    strcpy(tNameLookups[CATALYST_ACCURACY],   "Accuracy");
    strcpy(tNameLookups[CATALYST_SKILL],      "Skill");

    //--Reset.
    for(int i = 0; i < CATALYST_TOTAL; i ++)
    {
        mCatalystCounts[i] = 0;
        mCatalystCountsLocalCur[i] = 0;
        mCatalystCountsLocalTotal[i] = 0;
    }

    //--If no string is specified, clear.
    if(!pString || !strcasecmp(pString, "Null"))
    {
        DebugManager::PopPrint("Completed, string was Null.\n");
        return;
    }

    ///--[ ========== Scan ========== ]
    //--Fast-access pointers.
    StarLinkedList *rCatalystInfoList = MapManager::Fetch()->GetCatalystListing();

    //--Debug.
    DebugManager::Print("Total catalyst blocks in listing: %i\n", rCatalystInfoList->GetListSize());

    //--Iterate.
    MapCatalystPack *rPackage = (MapCatalystPack *)rCatalystInfoList->PushIterator();
    while(rPackage)
    {
        ///--[Relevance]
        //--Check if this package is relevant to the given string. If it is, then the count gets added to local totals.
        //  If not, the count only affects the global total.
        bool tIsLocalCount = false;
        if(strcmp(pString, rPackage->mChapterAssociated))
        {
            DebugManager::Print(" Block %s %s is not relevant, is chapter %s. ", rPackage->mLevelName, rPackage->mChestName, rPackage->mChapterAssociated);
        }
        else
        {
            tIsLocalCount = true;
            DebugManager::Print(" Block %s %s is relevant. ", rPackage->mLevelName, rPackage->mChestName);
        }

        ///--[Catalyst Type]
        //--Get the catalyst type.
        int tCatalystType = -1;
        if(     !strcmp(rPackage->mCatalystType, "CATALYST|Health"))     tCatalystType = CATALYST_HEALTH;
        else if(!strcmp(rPackage->mCatalystType, "CATALYST|Attack"))     tCatalystType = CATALYST_ATTACK;
        else if(!strcmp(rPackage->mCatalystType, "CATALYST|Initiative")) tCatalystType = CATALYST_INITIATIVE;
        else if(!strcmp(rPackage->mCatalystType, "CATALYST|Evade"))      tCatalystType = CATALYST_DODGE;
        else if(!strcmp(rPackage->mCatalystType, "CATALYST|Accuracy"))   tCatalystType = CATALYST_ACCURACY;
        else if(!strcmp(rPackage->mCatalystType, "CATALYST|Skill"))      tCatalystType = CATALYST_SKILL;

        //--If the catalyst type is -1, an error occurred. Stop.
        if(tCatalystType == -1)
        {
            fprintf(stderr, "AdventureInventory::SetCatalystCountsByGlobalString(): Warning, no catalyst type %s found.\n", rPackage->mCatalystType);
            rPackage = (MapCatalystPack *)rCatalystInfoList->AutoIterate();
            continue;
        }

        //--Increase the maximum by one.
        if(tIsLocalCount) mCatalystCountsLocalTotal[tCatalystType] ++;

        //--Debug.
        DebugManager::Print(" Catalyst is of type: %s\n", tNameLookups[tCatalystType]);

        ///--[Chest Open Status]
        //--Create a DL Path buffer to check if the chest is opened.
        char tBuffer[256];
        sprintf(tBuffer, "Root/Variables/Chests/%s/%s", rPackage->mLevelName, rPackage->mChestName);
        SysVar *rCheckPtr = (SysVar *)DataLibrary::Fetch()->GetEntry(tBuffer);

        //--If this variable exists, the chest has been opened at least once.
        if(rCheckPtr)
        {
            //--Global total always increments by one.
            mCatalystCounts[tCatalystType] ++;

            //--Local total.
            if(tIsLocalCount) mCatalystCountsLocalCur[tCatalystType] ++;

            //--Debug.
            DebugManager::Print(" Catalyst has been retrieved by player. Count is now: %i / Local: %i\n", mCatalystCounts[tCatalystType], mCatalystCountsLocalCur[tCatalystType]);
        }

        //--Next.
        rPackage = (MapCatalystPack *)rCatalystInfoList->AutoIterate();
    }

    ///--[Set Internal Totals]
    DebugManager::Print("Final catalyst counts. Local / Cap   / Total\n");
    DebugManager::Print("Health:              %3i     %3i     %3i\n", mCatalystCountsLocalCur[CATALYST_HEALTH],     mCatalystCountsLocalTotal[CATALYST_HEALTH],     mCatalystCounts[CATALYST_HEALTH]);
    DebugManager::Print("Attack:              %3i     %3i     %3i\n", mCatalystCountsLocalCur[CATALYST_ATTACK],     mCatalystCountsLocalTotal[CATALYST_ATTACK],     mCatalystCounts[CATALYST_ATTACK]);
    DebugManager::Print("Initiative:          %3i     %3i     %3i\n", mCatalystCountsLocalCur[CATALYST_INITIATIVE], mCatalystCountsLocalTotal[CATALYST_INITIATIVE], mCatalystCounts[CATALYST_INITIATIVE]);
    DebugManager::Print("Evade:               %3i     %3i     %3i\n", mCatalystCountsLocalCur[CATALYST_DODGE],      mCatalystCountsLocalTotal[CATALYST_DODGE],      mCatalystCounts[CATALYST_DODGE]);
    DebugManager::Print("Accuracy:            %3i     %3i     %3i\n", mCatalystCountsLocalCur[CATALYST_ACCURACY],   mCatalystCountsLocalTotal[CATALYST_ACCURACY],   mCatalystCounts[CATALYST_ACCURACY]);
    DebugManager::Print("Skill:               %3i     %3i     %3i\n", mCatalystCountsLocalCur[CATALYST_SKILL],      mCatalystCountsLocalTotal[CATALYST_SKILL],      mCatalystCounts[CATALYST_SKILL]);

    ///--[Debug]
    DebugManager::PopPrint("Completed catalyst count operations normally.\n");
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
