//--Base
#include "CarnationCombat.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
void CarnationCombat::RegisterHitbox(const char *pString, float pX, float pY, float pW, float pH)
{
    ///--[Documentation]
    //--Registers a hitbox onto the end of the hitbox list.
    if(!pString) return;
    CarnButton *nButton = (CarnButton *)starmemoryalloc(sizeof(CarnButton));
    nButton->Initialize();
    nButton->mIndex = mButtonList->GetListSize();
    ResetString(nButton->mDisplayString, pString);
    nButton->mPosition.SetWH(pX, pY, pW, pH);
    mButtonList->AddElementAsTail("X", nButton, &CarnButton::DeleteThis);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void CarnationCombat::SetHitboxesResolve()
{
    ///--[Documentation]
    //--Determines which set of hitboxes should be in use based on other variables. Target mode, create
    //  target hitboxes, otherwise, if on primary action list, primary action, or sublist creation.
    if(mIsSelectingTargets)
    {
        SetHitboxesForTargetSelection();
        return;
    }

    //--Check if there is a sublist on the stack. If no sublist, use the primary action.
    StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();
    if(rPlayerStackList == mCommandList)
    {
        SetHitboxesForPrimaryAction();
        return;
    }

    //--Otherwise, set for the secondary list on the command list.
    SetHitboxesForSecondaryAction();
}
void CarnationCombat::SetHitboxesForPrimaryAction()
{
    ///--[Documentation]
    //--Primary action, this is the part where the player selects Attack/Defend/Abilities etc.
    //  These hitboxes are in fixed position.
    //--There are a max of six abilities. In order, these are Attack, Defend, Skills, Specials,
    //  Items, Retreat.
    float cButtonLft = 240.0f;
    float cButtonTop =  64.0f;
    float cButtonWid = 140.0f;
    float cButtonHei =  41.0f;
    float cButtonXSp = 149.0f;
    mButtonList->ClearList();
    RegisterHitbox("Attack",   cButtonLft + (0 * cButtonXSp), cButtonTop, cButtonWid, cButtonHei);
    RegisterHitbox("Defend",   cButtonLft + (1 * cButtonXSp), cButtonTop, cButtonWid, cButtonHei);
    RegisterHitbox("Skills",   cButtonLft + (2 * cButtonXSp), cButtonTop, cButtonWid, cButtonHei);
    RegisterHitbox("Specials", cButtonLft + (3 * cButtonXSp), cButtonTop, cButtonWid, cButtonHei);
    RegisterHitbox("Items",    cButtonLft + (4 * cButtonXSp), cButtonTop, cButtonWid, cButtonHei);
    RegisterHitbox("Retreat",  cButtonLft + (5 * cButtonXSp), cButtonTop, cButtonWid, cButtonHei);
}
void CarnationCombat::SetHitboxesForSecondaryAction()
{
    ///--[Documentation]
    //--Resolves hitboxes for secondary actions, such as the skills menu or items menu.
    mButtonList->ClearList();

    //--Constants.
    int cMaxPerRow = 6;
    float cButtonLft = 240.0f;
    float cButtonTop =  64.0f;
    float cButtonWid = 140.0f;
    float cButtonHei =  41.0f;
    float cButtonXSp = 149.0f;
    float cButtonYSp =  50.0f;

    //--Variables.
    float tCurX = cButtonLft;
    float tCurY = cButtonTop;
    StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();

    //--Iterate.
    int i = 0;
    CommandListEntry *rEntry = (CommandListEntry *)rPlayerStackList->PushIterator();
    while(rEntry)
    {
        //--Create button.
        RegisterHitbox(rEntry->mDisplayName, tCurX, tCurY, cButtonWid, cButtonHei);

        //--Adjust X/Y Position.
        i ++;
        if(i >= cMaxPerRow)
        {
            i = 0;
            tCurX = cButtonLft;
            tCurY = tCurY + cButtonYSp;
        }
        else
        {
            tCurX = tCurX + cButtonXSp;
        }

        //--Next.
        rEntry = (CommandListEntry *)rPlayerStackList->AutoIterate();
    }
}
void CarnationCombat::SetHitboxesForTargetSelection()
{
    ///--[Documentation]
    //--Resolves hitboxes for all enemies currently on the field. A list is created below the primary
    //  action list and populated with hitboxes.
    mButtonList->ClearList();

    //--Constants.
    int cMaxPerRow = 6;
    float cButtonLft = 240.0f;
    float cButtonTop =  64.0f;
    float cButtonWid = 140.0f;
    float cButtonHei =  41.0f;
    float cButtonXSp = 149.0f;
    float cButtonYSp =  50.0f;

    //--Variables.
    float tCurX = cButtonLft;
    float tCurY = cButtonTop;
    //StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();

    //--Iterate.
    int i = 0;
    TargetCluster *rTargetCluster = (TargetCluster *)mTargetClusters->PushIterator();
    while(rTargetCluster)
    {
        //--Create button.
        RegisterHitbox(rTargetCluster->mDisplayName, tCurX, tCurY, cButtonWid, cButtonHei);

        //--Adjust X/Y Position.
        i ++;
        if(i >= cMaxPerRow)
        {
            i = 0;
            tCurX = cButtonLft;
            tCurY = tCurY + cButtonYSp;
        }
        else
        {
            tCurX = tCurX + cButtonXSp;
        }

        //--Next.
        rTargetCluster = (TargetCluster *)mTargetClusters->AutoIterate();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
