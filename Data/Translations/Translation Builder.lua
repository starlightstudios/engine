-- |[ ================================== Translation Builder =================================== ]|
--At program startup, checks translation folders for translations. Because the filesystem doesn't
-- work on OSX, this is done with specific names.

-- |[Globals]|
--The global variable gsLocalization is used to capture the current translation. If none is in use,
-- the default is "English"
gsLocalization = "English"

--Global Translation Variables
gsTranslationUI           = "Null"
gsTranslationItems        = "Null"
gsTranslationSkills       = "Null"
gsTranslationCombat       = "Null"
gsTranslationJournal      = "Null"
gsTranslationDialogue     = "Null"
gsTranslationCallouts     = "Null"
gsTranslationAchievements = "Null"

--Font File Replacements
gsEngineFontPathReplace = nil
gsAdventureFontPathReplace = nil
gsMonocerosFontPathReplace = nil

-- |[Scan]|
--The Data/Translations/ folder contains the translations. It must contain a flag and Info.txt,
-- which contains the name of the translation.
local saPossibleTranslations = {"JP"}
gzaFoundTranslations = {}

--Translation Package Format:
--sPathLetters          --Usually two letters, name of the folder that has the translation eg. "JP"
--sInternalName         --Full name of the language or translation eg. "Japanese"
--sExtension            --Extension to look for when building packages. Eg: ".luajp"

--Look in the possible translation folders:
for i = 1, #saPossibleTranslations, 1 do
    
    --Setup.
    local sPath = "Data/Translations/" .. saPossibleTranslations[i] .. "/"
    
    --If the info and the flag datafile exist, this is valid.
    if(FS_Exists(sPath .. "Info.txt") == true and FS_Exists(sPath .. "Images.slf") == true) then
        
        --New entry
        local zEntry = {}
        zEntry.sPathLetters  = saPossibleTranslations[i]
        zEntry.sInternalName = "Null"
        zEntry.sExtension    = "Null"
        
        --Read Info.txt to get the name of this translation.
        local fInfile = io.open(sPath .. "Info.txt", "r")
        zEntry.sInternalName = fInfile:read()
        zEntry.sExtension = fInfile:read()
        fInfile:close()
        
        --Add it to the list.
        table.insert(gzaFoundTranslations, zEntry)
        
        --Debug.
        io.write("The translation " .. zEntry.sPathLetters .. " exists! Is named " .. zEntry.sInternalName .. ". Extension " .. zEntry.sExtension .. "\n")
    end
end

-- |[Read File]|
--Read the file "Data/Language.txt", which should contain a single string indicating what language we should use.
-- If the file doesn't exist, the language will default to "English".
local fInfile = io.open("Data/Language.txt", "r")
local sLanguage = "English"
if(fInfile ~= nil) then
    sLanguage = fInfile:read()
    fInfile:close()
end

-- |[Compare]|
--If the language is English, we're done.
if(sLanguage == "English") then return end

--Otherwise, check the translations available. If any of them match, use their files. If a translation is not found
-- but was specified by the file, it may be malformed. Use English.
local zEntry = nil
for i = 1, #gzaFoundTranslations, 1 do
    if(gzaFoundTranslations[i].sInternalName == sLanguage) then
        zEntry = gzaFoundTranslations[i]
        break
    end
end

--Not found. Fail.
if(zEntry == nil) then return end

-- |[ ==== Activate Translation ==== ]|
--A translation has been found, activate it. In addition to setting the global variable, we also need to 
-- create translations for various UI pieces and set global paths.
gsLocalization = zEntry.sInternalName

--Set font file replacements. These are local fonts that allow alternate character sets and kernings. If
-- a replacement doesn't exist, it stays nil and no change occurs.
local sTranslationPath = "Data/Translations/" .. zEntry.sPathLetters .. "/"
if(FS_Exists(sTranslationPath .. "EngineFonts.lua")) then
    gsEngineFontPathReplace = sTranslationPath .. "EngineFonts.lua"
end
if(FS_Exists(sTranslationPath .. "AdventureFonts.lua")) then
    gsAdventureFontPathReplace = sTranslationPath .. "AdventureFonts.lua"
end
if(FS_Exists(sTranslationPath .. "MonocerosFonts.lua")) then
    gsMonocerosFontPathReplace = sTranslationPath .. "MonocerosFonts.lua"
end

--List of all possible translation files.
local saTranslationFileList = {"UI", "Items", "Skills", "Combat", "Journal", "Dialogue", "Callouts", "Achievements"}
for i = 1, #saTranslationFileList, 1 do

    --Setup.
    local sFilePath = "Data/Translations/" .. zEntry.sPathLetters .. "/" .. saTranslationFileList[i] .. ".csv"
    
    --If the file exists, create a translation and activate it.
    if(FS_Exists(sFilePath)) then
            
        --Create translation.
        STrans_CreateTranslation(gsLocalization, saTranslationFileList[i], sFilePath)
        STrans_ActivateTranslation(gsLocalization, saTranslationFileList[i])
        
        --Set globals. Not all translations use all possible files. Files not in use remain "Null".
        if(i == 1) then
            gsTranslationUI = "Root/Translations/Game/Active/UI"
        elseif(i == 2) then
            gsTranslationItems = "Root/Translations/Game/Active/Items"
        elseif(i == 3) then
            gsTranslationSkills = "Root/Translations/Game/Active/Skills"
        elseif(i == 4) then
            gsTranslationCombat = "Root/Translations/Game/Active/Combat"
        elseif(i == 5) then
            gsTranslationJournal = "Root/Translations/Game/Active/Journal"
        elseif(i == 6) then
            gsTranslationDialogue = "Root/Translations/Game/Active/Dialogue"
        elseif(i == 7) then
            gsTranslationCallouts = "Root/Translations/Game/Active/Callouts"
        elseif(i == 8) then
            gsTranslationAchievements = "Root/Translations/Game/Active/Achievements"
        end
    end
end

--Additional system exec.
if(FS_Exists(sTranslationPath .. "Exec.lua")) then
    LM_ExecuteScript(sTranslationPath .. "Exec.lua")
end
