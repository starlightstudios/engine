///======================================= WorldPolygon ===========================================
//--A structure which is a polygon located in the world. Unlike the requirements of the WAD file, polygons
//  are under no obligation to align with the floor or walls. They are totally arbitrary.
//--A WorldPolygon can consist of three or four sides. It can't be more than that, or it would need to subdivide.
//--This is a structure, so don't use new/delete!

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--This constant fixes a bug introduced on newer graphics cards. The bug causes the texture value to be a non-integer
//  for some inscrutable reason. Adding this offset causes the int() function on the shader to behave. I don't know why.
#define WP_GFX_CARD_OFFSET 0.100000f

///========================================== Classes =============================================
class WorldPolygon
{
    public:
    //--System
    bool mIsSkyPolygon;

    //--Points
    int mPointsTotal;
    Point3D *mPoints;
    Point3D mNormal;
    Point3D mAverage;
    float mDotProduct;

    //--Texturing
    Point3D *mTexCoords;
    Point3D *mLightmapCoords;
    StarBitmap *rTexture;
    StarBitmap *mLightmap;

    //--Lightmap Storage
    Point3D mLightmapTL;
    Point3D mLightmapBR;
    Point3D mLightmapXStep;
    Point3D mLightmapYStep;
    int mLightmapXSize;
    int mLightmapYSize;
    uint8_t *mLightmapData;

    //--Temporary Data
    int16_t mTemporaryTextureIndex;
    int mLightmapLoadX, mLightmapLoadY;

    public:
    //--System
    void Initialize();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    static float xLightmapRatioX;
    static float xLightmapRatioY;
    static int xLightmapSizeMinX;
    static int xLightmapSizeMinY;
    static int xLightmapSizeMaxX;
    static int xLightmapSizeMaxY;
    static int xLightmapMaxArea;
    static int xLightmapAmbient;

    //--Property Queries
    int GetTriangleCount();
    bool IsFacingPoint(Point3D pPoint);
    bool IsPointInPolygon(float pX, float pY, float pZ);
    bool IsPointInPlanarPolygon(float pX, float pY);
    float GetHeightAt(float pX, float pY);

    //--Manipulators
    void Allocate(int pPointsTotal);
    void SetPoint(int pSlot, float pX, float pY, float pZ);
    void SetTexCoord(int pSlot, float pX, float pY);
    void ComputeNormal();
    void ComputeDotProduct();

    //--Core Methods
    void Compress();
    void SetupLightmap();
    void AddLightmap(int pX, int pY, int pIntensity);
    void FinalizeLightmap();
    void PopulateTriangleData(int &sCursor, float *pData);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void WriteToBuffer(WADFile *pCaller, StarAutoBuffer *pBuffer, bool pWriteTextureName);
    void WriteLightmapToBuffer(StarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *fInfile);
    void ReadLightmapFromFile(VirtualFile *fInfile, bool pPreserveRawData);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

