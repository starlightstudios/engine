//--Base
#include "WordCombat.h"

//--Classes
#include "IndicatorPack.h"
#include "WordCard.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///--[Local Definitions]
#define WC_ACE_POS_X  95.0f
#define WC_ACE_POS_Y 700.0f

///========================================== System ==============================================
WordCombat::WordCombat()
{
    ///--[Word Combat]
    //--System
    mCombatResolution = TL_COMBAT_UNRESOLVED;
    mDamageScreenScatterTimer = 0;
    mDamageScreenScatterX = 0.0f;
    mDamageScreenScatterY = 0.0f;

    //--Wait Flags
    mIsWaitMode = true;
    mTicksAllowed = 0;
    mTicksReverse = 0;
    mTicksPerCardMove = 15;
    mTicksPerCardDraw = 15;
    mTicksPerAct = 30;
    mTicksPerShuffle = 120;
    mTicksPerPotion = 30;
    mFreeDrawsLeft = 0;

    //--String Handler
    mTextIndicatorList = new StarLinkedList(true);

    //--Options
    mAutoDraw = false;
    mAutoPlace = true;

    //--Timers
    mUITransitionTimer = 0;
    mCombatTransitionTimer = 0;
    mCombatTransitionMode = TL_COMBAT_MODE_NONE;

    //--Enemy List
    mIsAnEnemyDying = false;
    mEnemyTarget = 0;
    mDyingEnemyTimer = 0;
    mDyingEnemySlot = 0;
    memset(cEnemyRenderPos, 0, sizeof(float) * 5 * 5);
    mCombatEnemyList = new StarLinkedList(true);

    //--Combat Ending
    mEndingPhase = TL_COMBAT_ENDING_NONE;
    mTargetDyingTimer = 0;

    //--Player Properties
    mPlayerHP = 10;
    mPlayerHPMax = 10;
    mPotions = 0;
    mHealingTimer = 0;
    rLastHeart = NULL;
    mLastObjectWasTail = true;
    mPlayerShieldList = new StarLinkedList(true);
    mPlayerHeartList = new StarLinkedList(true);
    mDiscardedUIObjects = new StarLinkedList(true);

    //--Player's Deck
    mDeckPower = 0.50f;
    mDeckPowerGainPerTick = FULL_CHARGE_POWER / (SECONDS_FOR_FULL_CHARGE * (float)TICKS_PER_SECOND);
    mDeckPowerMax = 1.00f;
    mDeckPowerPerCard = 0.25f;
    mPlayerMaxHandSize = 9; //Cannot be more than WC_MAX_CARDS_IN_HAND
    if(mPlayerMaxHandSize >= WC_MAX_CARDS_IN_HAND) mPlayerMaxHandSize = WC_MAX_CARDS_IN_HAND;
    mAceTimer = WC_ACE_TICKS;
    mAceCard = NULL;
    mGlobalCardDeck = new StarLinkedList(true);
    mrPlayerHand = new StarLinkedList(false);
    mrPlayerDeck = new StarLinkedList(false);
    mrPlayerGraveyard = new StarLinkedList(false);

    //--Shuffle Handling
    mIsShuffling = false;
    mShuffleTimer = 1;
    mShuffleTimerMax = 1;
    mShuffleHighlightTimer = 0;
    mShuffleRotateTimer = 0;

    //--Click Handling
    mClickState = WC_CLICK_STATE_NONE;
    mClickTimer = 0;
    mSwapCard = -1;
    mSwapCardTimer = 0;
    mDisplayPower = 0;
    rClickedCard = NULL;
    mAceButton.SetWH( 20.0f, 690.0f, 159.0f, 56.0f);
    mActButton.SetWH(600.0f, 400.0f, 159.0f, 56.0f);
    mDrawButton.SetWH(1194.0f, 627.0f, 159.0f, 56.0f);
    mShuffleButton.SetWH(1194.0f, 690.0f, 159.0f, 56.0f);
    mSurrenderButton.SetWH(1195.0f, 13.0f, 159.0f, 56.0f);
    mPotionButton.SetWH(185.0f, 674.0f, 42.0f, 75.0f);

    //--Sentence Handler
    memset(mDestinationActivities, 0, sizeof(bool) * WC_DESTINATIONS_MAX);
    memset(mDestinationButtons, 0, sizeof(TwoDimensionReal) * WC_DESTINATIONS_MAX);
    memset(mDestinationPositions, 0, sizeof(float) * WC_DESTINATIONS_MAX);
    memset(rSentenceHand, 0, sizeof(WordCard *) * WC_DESTINATIONS_MAX);
    mSentenceResultList = new StarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));

    ///--[Construction]
    //--Fast Access Pointers
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts.
    Images.Data.rFontMenuSize   = rDataLibrary->GetFont("Word Combat Medium");
    Images.Data.rFontMenuHeader = rDataLibrary->GetFont("Word Combat Header");
    Images.Data.rFontMenuCards  = rDataLibrary->GetFont("Word Combat Cards");

    //--UI Parts
    Images.Data.rAttackBarFrame     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/AttackBarFrame");
    Images.Data.rAttackBarFill      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/AttackBarFill");
    Images.Data.rBarHeart           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Heart");
    Images.Data.rBarShield          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Shield");
    Images.Data.rBarShieldOrbit     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_ShieldOrbit");
    Images.Data.rBtnAce             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Btn_Ace");
    Images.Data.rBtnAct             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Btn_Act");
    Images.Data.rBtnDraw            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Btn_Draw");
    Images.Data.rBtnShuffle         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Btn_Shuffle");
    Images.Data.rBtnShuffleHighlight= (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Btn_Shuffle_Highlight");
    Images.Data.rBtnSurrender       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Btn_Surrender");
    Images.Data.rBtnPotion          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Btn_Potion");
    Images.Data.rCardAnd            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_And");
    Images.Data.rCardOf             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Of");
    Images.Data.rCardAndOfHighlight = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_AndOfHighlight");
    Images.Data.rCardAttack         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Attack");
    Images.Data.rCardDeath          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Death");
    Images.Data.rCardDefend         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Defend");
    Images.Data.rCardEarth          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Earth");
    Images.Data.rCardFire           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Fire");
    Images.Data.rCardHighlight      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Highlight");
    Images.Data.rCardJoker          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Joker");
    Images.Data.rCardLife           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Life");
    Images.Data.rCardWater          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Water");
    Images.Data.rCardWind           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Wind");
    Images.Data.rExpandableL        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/ExpandableFrameL");
    Images.Data.rExpandableM        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/ExpandableFrameM");
    Images.Data.rExpandableR        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/ExpandableFrameR");
    Images.Data.rHealthBarFrame     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/HealthBarFrame");
    Images.Data.rHealthBarFill      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/HealthBarFill");
    Images.Data.rIconAttack         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Attack");
    Images.Data.rIconDeath          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Death");
    Images.Data.rIconDefend         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Defend");
    Images.Data.rIconEarth          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Earth");
    Images.Data.rIconFire           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Fire");
    Images.Data.rIconLife           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Life");
    Images.Data.rIconWater          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Water");
    Images.Data.rIconWind           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Icon_Wind");
    Images.Data.rStaticDivider      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Static_WordDivider");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--[Enemy Render Positions]
    cEnemyRenderPos[0][0] = VIRTUAL_CANVAS_X * 0.50f;

    cEnemyRenderPos[1][0] = VIRTUAL_CANVAS_X * 0.35f;
    cEnemyRenderPos[1][1] = VIRTUAL_CANVAS_X * 0.65f;

    cEnemyRenderPos[2][0] = VIRTUAL_CANVAS_X * 0.50f;
    cEnemyRenderPos[2][1] = VIRTUAL_CANVAS_X * 0.30f;
    cEnemyRenderPos[2][2] = VIRTUAL_CANVAS_X * 0.70f;

    cEnemyRenderPos[3][0] = VIRTUAL_CANVAS_X * 0.40f;
    cEnemyRenderPos[3][1] = VIRTUAL_CANVAS_X * 0.60f;
    cEnemyRenderPos[3][2] = VIRTUAL_CANVAS_X * 0.20f;
    cEnemyRenderPos[3][3] = VIRTUAL_CANVAS_X * 0.80f;

    cEnemyRenderPos[4][0] = VIRTUAL_CANVAS_X * 0.50f;
    cEnemyRenderPos[4][1] = VIRTUAL_CANVAS_X * 0.30f;
    cEnemyRenderPos[4][2] = VIRTUAL_CANVAS_X * 0.70f;
    cEnemyRenderPos[4][3] = VIRTUAL_CANVAS_X * 0.10f;
    cEnemyRenderPos[4][4] = VIRTUAL_CANVAS_X * 0.90f;

    ///--[Destination Buttons]
    //--Statically positioned.
    float cHOff = -60.0f + 210.0f;
    mDestinationButtons[0].SetWH(150.0f + cHOff, 458.0f, 86.0f, 84.0f);//Modifier
    mDestinationButtons[1].SetWH(236.0f + cHOff, 458.0f, 88.0f, 84.0f);//And
    mDestinationButtons[2].SetWH(324.0f + cHOff, 458.0f, 86.0f, 84.0f);//Modifier
    mDestinationButtons[3].SetWH(410.0f + cHOff, 458.0f, 88.0f, 84.0f);//And
    mDestinationButtons[4].SetWH(498.0f + cHOff, 458.0f, 86.0f, 84.0f);//Modifier
    mDestinationButtons[5].SetWH(614.0f + cHOff, 458.0f, 86.0f, 84.0f);//Action
    mDestinationButtons[6].SetWH(700.0f + cHOff, 458.0f, 88.0f, 84.0f);//Of
    mDestinationButtons[7].SetWH(788.0f + cHOff, 458.0f, 86.0f, 84.0f);//Modifier

    //--Positions are where the buttom goes to. The Y is constant.
    mDestinationPositions[0] = mDestinationButtons[0].mXCenter;
    mDestinationPositions[1] = mDestinationButtons[1].mXCenter;
    mDestinationPositions[2] = mDestinationButtons[2].mXCenter;
    mDestinationPositions[3] = mDestinationButtons[3].mXCenter;
    mDestinationPositions[4] = mDestinationButtons[4].mXCenter;
    mDestinationPositions[5] = mDestinationButtons[5].mXCenter;
    mDestinationPositions[6] = mDestinationButtons[6].mXCenter;
    mDestinationPositions[7] = mDestinationButtons[7].mXCenter;

    ///--[Elemental Colors]
    mElementalColors[TL_ELEMENT_SLOT_PHYSICAL].SetRGBAI(255, 255, 255, 255);
    mElementalColors[TL_ELEMENT_SLOT_FIRE].SetRGBAI(    231, 140,   0, 255);
    mElementalColors[TL_ELEMENT_SLOT_WATER].SetRGBAI(     0, 238,  55, 255);
    mElementalColors[TL_ELEMENT_SLOT_WIND].SetRGBAI(     76, 165,  38, 255);
    mElementalColors[TL_ELEMENT_SLOT_EARTH].SetRGBAI(   211,  70,   0, 255);
    mElementalColors[TL_ELEMENT_SLOT_LIFE].SetRGBAI(    255, 124, 255, 255);
    mElementalColors[TL_ELEMENT_SLOT_DEATH].SetRGBAI(    32,  32,  32, 255);

    ///--[Card Positions]
    //--This is where the player's hand positions itself. They are evenly spaced from the center using
    //  alternating orders. Pattern:
    //  <- Left side of the screen                    Right side of the screen ->
    //                9    7    5    3    1    2    4    6    8
    float cCenterX = VIRTUAL_CANVAS_X * 0.50f;
    float cCardSpacing = WC_WORD_CARD_SPACING;
    float tScaler = 1.0f; //Swaps between -1 and 1.
    float tFactor = 1.0f; //Increments to space the cards out.
    mHandPositionsX[0] = cCenterX;
    for(int i = 1; i < WC_MAX_CARDS_IN_HAND; i ++)
    {
        //--Set.
        mHandPositionsX[i] = cCenterX + (cCardSpacing * tFactor * tScaler);

        //--Swap to negative.
        if(tScaler > 0.0f)
        {
            tScaler = tScaler * -1.0f;
        }
        //--Swap to positive, add to factor to space the cards more.
        else
        {
            tScaler = tScaler * -1.0f;
            tFactor = tFactor + 1.1f;
        }
    }
}
WordCombat::~WordCombat()
{
    delete mTextIndicatorList;
    delete mPlayerShieldList;
    delete mPlayerHeartList;
    delete mDiscardedUIObjects;
    delete mCombatEnemyList;
    delete mAceCard;
    delete mGlobalCardDeck;
    delete mrPlayerHand;
    delete mrPlayerDeck;
    delete mrPlayerGraveyard;
    delete mSentenceResultList;
}
void WordCombat::Initialize()
{
    ///--[Documentation]
    //--Re-initializes the combat engine for a new battle. Should be called before anything else.
    //  Variables will be reset and lists will be cleared to their neutral states.

    //--System
    mCombatResolution = TL_COMBAT_UNRESOLVED;
    mDamageScreenScatterTimer = 0;
    mDamageScreenScatterX = 0.0f;
    mDamageScreenScatterY = 0.0f;

    //--Wait Flags
    mTicksAllowed = 0;
    mTicksReverse = 0;
    mFreeDrawsLeft = 0;

    //--String Handler
    mTextIndicatorList->ClearList();

    //--Timers
    mUITransitionTimer = 0;
    mCombatTransitionTimer = 0;
    mCombatTransitionMode = TL_COMBAT_MODE_NONE;

    //--Enemy List
    mIsAnEnemyDying = false;
    mEnemyTarget = 0;
    mDyingEnemyTimer = 0;
    mDyingEnemySlot = 0;
    mCombatEnemyList->ClearList();

    //--Combat Ending
    mEndingPhase = TL_COMBAT_ENDING_NONE;
    mTargetDyingTimer = 0;

    //--Player Properties
    mPlayerHP = 10;

    mPlayerHPMax = 10;
    mPotions = 0;
    mHealingTimer = 0;
    rLastHeart = NULL;
    mLastObjectWasTail = true;
    mPlayerShieldList->ClearList();
    mPlayerHeartList->ClearList();
    mDiscardedUIObjects->ClearList();

    //--Player's Deck
    mDeckPower = 0.50f;
    mDeckPowerGainPerTick = FULL_CHARGE_POWER / (SECONDS_FOR_FULL_CHARGE * (float)TICKS_PER_SECOND);
    mDeckPowerMax = 1.00f;
    mDeckPowerPerCard = 0.25f;
    mPlayerMaxHandSize = 9;
    if(mPlayerMaxHandSize >= WC_MAX_CARDS_IN_HAND) mPlayerMaxHandSize = WC_MAX_CARDS_IN_HAND;
    mAceTimer = WC_ACE_TICKS;
    delete mAceCard;
    mAceCard = NULL;
    mGlobalCardDeck->ClearList();
    mrPlayerHand->ClearList();
    mrPlayerDeck->ClearList();
    mrPlayerGraveyard->ClearList();

    //--Shuffle Handling
    mIsShuffling = false;
    mShuffleTimer = 1;
    mShuffleTimerMax = 1;

    //--Click Handling
    mClickState = WC_CLICK_STATE_NONE;
    rClickedCard = NULL;

    //--Sentence Handler
    memset(mDestinationActivities, 0, sizeof(bool) * WC_DESTINATIONS_MAX);
    memset(rSentenceHand, 0, sizeof(WordCard *) * WC_DESTINATIONS_MAX);
    mSentenceResultList->ClearList();
}
void WordCombat::Finalize()
{
    //--Once all setup is completed, begins combat.
    ShuffleDeck();

    //--Determine how many hearts are needed.
    int tNeededHearts = mPlayerHP / 2;
    if(mPlayerHP % 2 == 1) tNeededHearts ++;

    //--Spawn the player's hearts up to their HP.
    GainHearts(tNeededHearts, true);
    rLastHeart = NULL;

    //--Start the music.
    AudioManager::Fetch()->PlayMusic("Doll|BattleTheme");
}

///===================================== Property Queries =========================================
int WordCombat::GetCombatResolution()
{
    return mCombatResolution;
}
int WordCombat::GetPlayerHP()
{
    return mPlayerHP;
}
int WordCombat::GetPotions()
{
    return mPotions;
}

///======================================= Manipulators ===========================================
void WordCombat::SetActiveCombat()
{
    mIsWaitMode = false;
    mAutoDraw = true;
}
void WordCombat::SetTurnCombat()
{
    mIsWaitMode = true;
    mAutoDraw = false;
}
void WordCombat::SetPlayerProperties(int pHP, int pHPMax, int pPotions)
{
    mPlayerHP = pHP;
    mPlayerHPMax = pHPMax;
    mPotions = pPotions;
}
void WordCombat::RegisterEnemy(int pHP, int pHPMax, const char *pImagePath)
{
    //--Create and register a new enemy.
    SetMemoryData(__FILE__, __LINE__);
    CombatEnemyPack *nPack = (CombatEnemyPack *)starmemoryalloc(sizeof(CombatEnemyPack));
    nPack->Initialize();
    nPack->BootSentenceMode();
    nPack->SetHP(pHP, pHPMax);
    nPack->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    mCombatEnemyList->AddElementAsTail("X", nPack, &CombatEnemyPack::DeleteThis);
}
void WordCombat::RegisterAttackToLastEnemy(int pRollWeight, int pPower, int pWindupTicks, uint8_t pElementalFlags)
{
    //--Get the last registered enemy. Add attacks to that one, if it exists.
    CombatEnemyPack *rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->GetTail();
    if(!rEnemyPack) return;

    //--Create a new attack.
    rEnemyPack->AddSentenceAttack(pRollWeight, pPower, pWindupTicks, pElementalFlags);
}
void WordCombat::RegisterWeaknessToLastEnemy(int pWeakness, int pAmount)
{
    //--Get the last registered enemy. Set a weakness in that enemy if it is found.
    CombatEnemyPack *rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->GetTail();
    if(!rEnemyPack) return;

    //--Set.
    rEnemyPack->SetWeakness(pWeakness, pAmount);
}
void WordCombat::RegisterNewCard(int pType, int pEffect, int pSeverity, const char *pWord, uint8_t pElementalFlags, bool pIsAce)
{
    //--Error check.
    if(!pWord) return;

    //--Create.
    WordCard *nCard = NULL;
    if(!pIsAce)
    {
        nCard = new WordCard();
        mGlobalCardDeck->AddElement("X", nCard, &WordCard::DeleteThis);
    }
    else
    {
        delete mAceCard;
        mAceCard = new WordCard();
        nCard = mAceCard;
    }

    //--Set properties.
    nCard->SetCardType(pType);
    nCard->SetDisplayedWord(pWord);
    nCard->SetFont(Images.Data.rFontMenuCards);
    nCard->SetExpandableNameplates(Images.Data.rExpandableL, Images.Data.rExpandableM, Images.Data.rExpandableR);
    nCard->SetElements(pElementalFlags);
    nCard->SetEffectType(pEffect);
    nCard->SetEffectSeverity(pSeverity);

    //--This is an action word:
    if(pType == WORDCARD_TYPE_ACTION)
    {
        if(pEffect == WORDCARD_EFFECT_DAMAGE)
        {
            nCard->SetBackingImage(Images.Data.rCardAttack);
        }
        else
        {
            nCard->SetBackingImage(Images.Data.rCardDefend);
        }
    }
    //--This is a modifier word:
    else if(pType == WORDCARD_TYPE_MODIFIER)
    {
        //--Resolve the element.
        if(pElementalFlags & TL_ELEMENT_FIRE)
        {
            nCard->SetBackingImage(Images.Data.rCardFire);
        }
        else if(pElementalFlags & TL_ELEMENT_WATER)
        {
            nCard->SetBackingImage(Images.Data.rCardWater);
        }
        else if(pElementalFlags & TL_ELEMENT_WIND)
        {
            nCard->SetBackingImage(Images.Data.rCardWind);
        }
        else if(pElementalFlags & TL_ELEMENT_EARTH)
        {
            nCard->SetBackingImage(Images.Data.rCardEarth);
        }
        else if(pElementalFlags & TL_ELEMENT_LIFE)
        {
            nCard->SetBackingImage(Images.Data.rCardLife);
        }
        else if(pElementalFlags & TL_ELEMENT_DEATH)
        {
            nCard->SetBackingImage(Images.Data.rCardDeath);
        }
    }
    //--And/Of
    else
    {
        nCard->SetAndOfFlag();
        if(pType == WORDCARD_TYPE_AND)
            nCard->SetBackingImage(Images.Data.rCardAnd);
        else
            nCard->SetBackingImage(Images.Data.rCardOf);
    }

    //--Once everything is set, figure out how big the card is.
    nCard->ComputeDimensions();

    //--Ace cards need to be positioned differently.
    if(pIsAce)
    {
        nCard->MoveToInstantly(WC_ACE_POS_X, WC_ACE_POS_Y);
    }
}

///======================================= Core Methods ===========================================
void WordCombat::BuildDestinationMarkers(WordCard *pCard)
{
    ///--[Documentation]
    //--When the player clicks a card in their hand, we need to construct a list of buttons which
    //  represent the spots in the current sentence that the selected card can go.
    //--Pass NULL to clear all the information.
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++) mDestinationActivities[i] = false;
    if(!pCard) return;

    //--If this card is an action card, then activate the action slot. Simple!
    if(pCard->GetCardType() == WORDCARD_TYPE_ACTION)
    {
        mDestinationActivities[WC_DESTINATION_ACTION] = true;
        return;
    }

    //--Card is an "Of". This is always the slot after the action case.
    if(pCard->GetCardType() == WORDCARD_TYPE_OF)
    {
        mDestinationActivities[WC_DESTINATION_OFSLOT] = true;
        return;
    }

    //--Card is a modifier card. This can go before or after the action. There are multiple legal cases.
    if(pCard->GetCardType() == WORDCARD_TYPE_MODIFIER)
    {
        //--If there is an "Of" word already in place, that slot activates.
        if(rSentenceHand[WC_DESTINATION_OFSLOT]) mDestinationActivities[WC_DESTINATION_TRAILINGSLOT] = true;

        //--Iterator. Counts backwards to slots behind the action. The first slot does not need an "And" in it,
        //  afterwards, "And" cards are required to add more modifiers.
        int tSlot = WC_DESTINATION_ACTION - 1;
        while(tSlot >= 0)
        {
            //--The slot becomes active.
            mDestinationActivities[tSlot] = true;

            //--Check the slot behind this. There must be an "And" in place, and it can't be at the edge.
            //  If this fails, stop.
            if(tSlot == 0 || !rSentenceHand[tSlot-1])
            {
                break;
            }

            //--Otherwise, move down two slots.
            tSlot -= 2;
        }
    }

    //--Card is an "And", which allows more modifiers to be tacked on before the action.
    if(pCard->GetCardType() == WORDCARD_TYPE_AND)
    {
        //--There are two "And" slots. To be valid, the slot ahead of it must have a card in it.
        int tAndSlot = 3;
        while(tAndSlot >= 0)
        {
            if(!rSentenceHand[tAndSlot+1]) break;
            mDestinationActivities[tAndSlot] = true;
            tAndSlot -= 2;
        }
    }
}
int WordCombat::ComputeActionPower(bool pExecuteAction)
{
    ///--[Documentation and Setup]
    //--Computes and returns the expected damage/defense of the combo. If pExecuteAction is true,
    //  the function will damage/defend and build the sentence list. Otherwise it just returns damage/defense.
    //--First, make sure an enemy actually exists, or actions do nothing.
    CombatEnemyPack *rEnemy = (CombatEnemyPack *)mCombatEnemyList->GetElementBySlot(0);
    if(!rEnemy) return 0;

    ///--[No Action Type]
    //--Neither defending nor attacking, so always return 0.
    if(rSentenceHand[WC_DESTINATION_ACTION] == NULL) return 0;

    //--Variables.
    bool tAtLeastOneWeakness = false;
    int tTotalModifiers = 0;
    uint8_t tElementalFlags = 0x00;
    int tElementalDamages[TL_ELEMENTS_TOTAL];
    int tElementalCombos[TL_ELEMENTS_TOTAL];
    int tWeaknessBonus[TL_ELEMENTS_TOTAL];
    memset(tElementalDamages, 0, sizeof(int) * TL_ELEMENTS_TOTAL);
    memset(tElementalCombos,  0, sizeof(int) * TL_ELEMENTS_TOTAL);
    memset(tWeaknessBonus,    0, sizeof(int) * TL_ELEMENTS_TOTAL);

    //--Wait-Mode Specials
    int tWindDrawBonus = 0;
    int tWaterActionReduction = 0;
    int tFireElementBonus = 0;
    int tEarthActionReduction = 0;
    int tEarthFreeShields = 0;

    //--Wait-Mode Constants
    int cWaterActionReductionPerCard = 15;
    int cWaterActionReductionPerCombo = 10;
    int cFireBonusPerCombo = 1;
    int cFreeDrawsPerWindCard = 3;
    int cFreeDrawsPerWindCombo = 1;
    int cEarthActionReductionPerCard = 5;
    int cEarthActionReductionPerCombo = 3;
    int cEarthFreeShieldsPerCombo = 1;

    ///--[Basic Power Computation]
    //--Iterate across the cards and see how much power we have total.
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
    {
        //--Skip empty slots.
        if(!rSentenceHand[i]) continue;
        if(rSentenceHand[i]->GetCardType() == WORDCARD_TYPE_OF) continue;
        if(rSentenceHand[i]->GetCardType() == WORDCARD_TYPE_AND) continue;

        //--Flag modifiers.
        if(rSentenceHand[i]->GetCardType() == WORDCARD_TYPE_MODIFIER) tTotalModifiers ++;

        //--Storage pack.
        SentencePack *nPack = NULL;
        if(pExecuteAction)
        {
            SetMemoryData(__FILE__, __LINE__);
            nPack = (SentencePack *)starmemoryalloc(sizeof(SentencePack));
            nPack->Init();
            sprintf(nPack->mSentence, "+%i %s", rSentenceHand[i]->GetEffectSeverity(), rSentenceHand[i]->GetDisplayString());
            mSentenceResultList->AddElementAsTail("X", nPack, &FreeThis);
        }

        //--Get the elemental type. It is expected right now that cards have only one elemental type.
        uint8_t tElementalFlag = rSentenceHand[i]->GetElementalFlag();
        if(tElementalFlag == TL_ELEMENT_NONE)
        {
            if(nPack) nPack->rDisplayImg = Images.Data.rIconAttack;
            tElementalDamages[TL_ELEMENT_SLOT_PHYSICAL] += rSentenceHand[i]->GetEffectSeverity();
        }
        else if(tElementalFlag == TL_ELEMENT_FIRE)
        {
            //--Normal.
            tElementalFlags |= TL_ELEMENT_FIRE;
            if(nPack) nPack->rDisplayImg = Images.Data.rIconFire;
            tElementalDamages[TL_ELEMENT_SLOT_FIRE] += rSentenceHand[i]->GetEffectSeverity();
            tElementalCombos[TL_ELEMENT_SLOT_FIRE] ++;
            tWeaknessBonus[TL_ELEMENT_SLOT_FIRE] += rEnemy->mElementWeakness[TL_ELEMENT_SLOT_FIRE];

            //--Wait mode bonus.
            if(tElementalCombos[TL_ELEMENT_SLOT_FIRE] > 1)
            {
                tFireElementBonus += cFireBonusPerCombo;
            }
        }
        else if(tElementalFlag == TL_ELEMENT_WATER)
        {
            //--Normal.
            tElementalFlags |= TL_ELEMENT_WATER;
            if(nPack) nPack->rDisplayImg = Images.Data.rIconWater;
            tElementalDamages[TL_ELEMENT_SLOT_WATER] += rSentenceHand[i]->GetEffectSeverity();
            tElementalCombos[TL_ELEMENT_SLOT_WATER] ++;
            tWeaknessBonus[TL_ELEMENT_SLOT_WATER] += rEnemy->mElementWeakness[TL_ELEMENT_SLOT_WATER];

            //--Wait mode bonus, slows enemy actions down.
            tWaterActionReduction += cWaterActionReductionPerCard;
            if(tElementalCombos[TL_ELEMENT_SLOT_WATER] > 1)
            {
                tWaterActionReduction += cWaterActionReductionPerCombo;
            }
        }
        //--Wind cards are special, as they provide free draws.
        else if(tElementalFlag == TL_ELEMENT_WIND)
        {
            //--Normal.
            tElementalFlags |= TL_ELEMENT_WIND;
            if(nPack) nPack->rDisplayImg = Images.Data.rIconWind;
            tElementalDamages[TL_ELEMENT_SLOT_WIND] += rSentenceHand[i]->GetEffectSeverity();
            tElementalCombos[TL_ELEMENT_SLOT_WIND] ++;
            tWeaknessBonus[TL_ELEMENT_SLOT_WIND] += rEnemy->mElementWeakness[TL_ELEMENT_SLOT_WIND];

            //--Provide the draw bonus for wind cards. For each card past the first, provide 1 extra draw.
            tWindDrawBonus += cFreeDrawsPerWindCard;
            if(tElementalCombos[TL_ELEMENT_SLOT_WIND] > 1)
            {
                tWindDrawBonus += cFreeDrawsPerWindCombo;
            }
        }
        else if(tElementalFlag == TL_ELEMENT_EARTH)
        {
            //--Normal.
            tElementalFlags |= TL_ELEMENT_EARTH;
            if(nPack) nPack->rDisplayImg = Images.Data.rIconEarth;
            tElementalDamages[TL_ELEMENT_SLOT_EARTH] += rSentenceHand[i]->GetEffectSeverity();
            tElementalCombos[TL_ELEMENT_SLOT_EARTH] ++;
            tWeaknessBonus[TL_ELEMENT_SLOT_EARTH] += rEnemy->mElementWeakness[TL_ELEMENT_SLOT_EARTH];

            //--Wait mode bonus, slows enemy actions down.
            tEarthActionReduction += cEarthActionReductionPerCard;
            if(tElementalCombos[TL_ELEMENT_SLOT_EARTH] > 1)
            {
                tEarthActionReduction += cEarthActionReductionPerCombo;
                tEarthFreeShields += cEarthFreeShieldsPerCombo;
            }
        }
        else if(tElementalFlag == TL_ELEMENT_LIFE)
        {
            tElementalFlags |= TL_ELEMENT_LIFE;
            if(nPack) nPack->rDisplayImg = Images.Data.rIconLife;
            tElementalDamages[TL_ELEMENT_SLOT_LIFE] += rSentenceHand[i]->GetEffectSeverity();
            tElementalCombos[TL_ELEMENT_SLOT_LIFE] ++;
            tWeaknessBonus[TL_ELEMENT_SLOT_LIFE] += rEnemy->mElementWeakness[TL_ELEMENT_SLOT_LIFE];
        }
        else if(tElementalFlag == TL_ELEMENT_DEATH)
        {
            tElementalFlags |= TL_ELEMENT_DEATH;
            if(nPack) nPack->rDisplayImg = Images.Data.rIconDeath;
            tElementalDamages[TL_ELEMENT_SLOT_DEATH] += rSentenceHand[i]->GetEffectSeverity();
            tElementalCombos[TL_ELEMENT_SLOT_DEATH] ++;
            tWeaknessBonus[TL_ELEMENT_SLOT_DEATH] += rEnemy->mElementWeakness[TL_ELEMENT_SLOT_DEATH];
        }
    }

    ///--[Bonus Sentence Packs]
    //--If executing an action, build sentence packs to indicate the various bonuses.
    if(pExecuteAction)
    {
        ///--[Combo Bonuses]
        //--Compute bonuses. Each elemental combo adds an extra point of damage.
        if(tElementalCombos[TL_ELEMENT_SLOT_WATER] > 1)
        {
            RegisterSentencePack("+%i Water Bonus", Images.Data.rIconWater, tElementalCombos[TL_ELEMENT_SLOT_WATER] - 1);
        }
        if(tElementalCombos[TL_ELEMENT_SLOT_FIRE] > 1)
        {
            RegisterSentencePack("+%i Fire Bonus", Images.Data.rIconFire, tElementalCombos[TL_ELEMENT_SLOT_FIRE] - 1);
        }
        if(tElementalCombos[TL_ELEMENT_SLOT_WIND] > 1)
        {
            RegisterSentencePack("+%i Wind Bonus", Images.Data.rIconWind, tElementalCombos[TL_ELEMENT_SLOT_WIND] - 1);
        }
        if(tElementalCombos[TL_ELEMENT_SLOT_EARTH] > 1)
        {
            RegisterSentencePack("+%i Earth Bonus", Images.Data.rIconEarth, tElementalCombos[TL_ELEMENT_SLOT_EARTH] - 1);
        }
        if(tElementalCombos[TL_ELEMENT_SLOT_LIFE] > 1)
        {
            RegisterSentencePack("+%i Life Bonus", Images.Data.rIconLife, tElementalCombos[TL_ELEMENT_SLOT_LIFE] - 1);
        }
        if(tElementalCombos[TL_ELEMENT_SLOT_DEATH] > 1)
        {
            RegisterSentencePack("+%i Death Bonus", Images.Data.rIconDeath, tElementalCombos[TL_ELEMENT_SLOT_DEATH] - 1);
        }

        ///--[Wait Bonus]
        //--In wait mode, fire damage can deal extra. Only applies to damage.
        if(mIsWaitMode && tFireElementBonus && rSentenceHand[WC_DESTINATION_ACTION]->GetEffectType() == WORDCARD_EFFECT_DAMAGE)
        {
            RegisterSentencePack("+%i Fire Special Bonus", Images.Data.rIconFire, tFireElementBonus);
        }

        ///--[Weakness Bonus]
        //--Only applies to attacks. Enemies that take at least one point of damage of a type they are weak to
        //  take bonus damage.
        //--Note that 0 or negative weaknesses do not appear here.
        if(rSentenceHand[WC_DESTINATION_ACTION]->GetEffectType() == WORDCARD_EFFECT_DAMAGE)
        {
            if(tWeaknessBonus[TL_ELEMENT_SLOT_WATER] > 0 && rEnemy->mElementWeakness[TL_ELEMENT_SLOT_WATER] > 0)
            {
                tAtLeastOneWeakness = true;
                RegisterSentencePack("+%i Water Weakness", Images.Data.rIconWater, tWeaknessBonus[TL_ELEMENT_SLOT_WATER]);
            }
            if(tWeaknessBonus[TL_ELEMENT_SLOT_FIRE] > 0 && rEnemy->mElementWeakness[TL_ELEMENT_SLOT_FIRE] > 0)
            {
                tAtLeastOneWeakness = true;
                RegisterSentencePack("+%i Fire Weakness", Images.Data.rIconFire, tWeaknessBonus[TL_ELEMENT_SLOT_FIRE]);
            }
            if(tWeaknessBonus[TL_ELEMENT_SLOT_WIND] > 0 && rEnemy->mElementWeakness[TL_ELEMENT_SLOT_WIND] > 0)
            {
                tAtLeastOneWeakness = true;
                RegisterSentencePack("+%i Wind Weakness", Images.Data.rIconWind, tWeaknessBonus[TL_ELEMENT_SLOT_WIND]);
            }
            if(tWeaknessBonus[TL_ELEMENT_SLOT_EARTH] > 0 && rEnemy->mElementWeakness[TL_ELEMENT_SLOT_EARTH] > 0)
            {
                tAtLeastOneWeakness = true;
                RegisterSentencePack("+%i Earth Weakness", Images.Data.rIconEarth, tWeaknessBonus[TL_ELEMENT_SLOT_EARTH]);
            }
            if(tWeaknessBonus[TL_ELEMENT_SLOT_LIFE] > 0 && rEnemy->mElementWeakness[TL_ELEMENT_SLOT_LIFE] > 0)
            {
                tAtLeastOneWeakness = true;
                RegisterSentencePack("+%i Life Weakness", Images.Data.rIconLife, tWeaknessBonus[TL_ELEMENT_SLOT_LIFE]);
            }
            if(tWeaknessBonus[TL_ELEMENT_SLOT_DEATH] > 0 && rEnemy->mElementWeakness[TL_ELEMENT_SLOT_DEATH] > 0)
            {
                tAtLeastOneWeakness = true;
                RegisterSentencePack("+%i Death Weakness", Images.Data.rIconDeath, tWeaknessBonus[TL_ELEMENT_SLOT_DEATH]);
            }
        }

        ///--[Sentence Length]
        //--Sentence length bonus. +1 for each modifier past the first.
        if(tTotalModifiers > 1)
        {
            RegisterSentencePack("+%i Sentence Length", Images.Data.rIconAttack, tTotalModifiers-1);
        }
    }

    ///--[Draw Bonus]
    //--If executing the action, provide the free draws here.
    if(mIsWaitMode && pExecuteAction && tWindDrawBonus > 0)
    {
        //--Increment.
        mFreeDrawsLeft += tWindDrawBonus;

        //--Indicators.
        CreateWindDrawIndicator(tWindDrawBonus);
    }

    ///--[Offense]
    //--If the action used is an attack, damage the target enemy here.
    if(rSentenceHand[WC_DESTINATION_ACTION]->GetEffectType() == WORDCARD_EFFECT_DAMAGE)
    {
        //--Get the damage total.
        int tTotalDamage = 0;
        for(int i = 0; i < TL_ELEMENTS_TOTAL; i ++)
        {
            //--Basic damage. Can be zero.
            tTotalDamage += tElementalDamages[i];

            //--Combo damage.
            if(tElementalCombos[i] > 1)
            {
                tTotalDamage += (tElementalCombos[i] - 1);
            }

            //--Weakness damage.
            if(tElementalDamages[i] > 0 && rEnemy->mElementWeakness[i] > 0)
            {
                tTotalDamage += rEnemy->mElementWeakness[i];
            }
        }

        //--Fire damage bonus, only applies to wait mode!
        if(mIsWaitMode && tFireElementBonus > 0)
        {
            tTotalDamage += tFireElementBonus;
        }

        //--Sentence length bonus.
        if(tTotalModifiers > 1) tTotalDamage += tTotalModifiers - 1;

        //--If the death element is present, damage increases by 50%!
        if(tElementalFlags & TL_ELEMENT_DEATH)
        {
            //--Damage.
            tTotalDamage = (int)(tTotalDamage * 1.50f);

            //--Description.
            if(pExecuteAction)
            {
                SetMemoryData(__FILE__, __LINE__);
                SentencePack *nPack = (SentencePack *)starmemoryalloc(sizeof(SentencePack));
                nPack->Init();
                nPack->rDisplayImg = Images.Data.rIconDeath;
                sprintf(nPack->mSentence, "Death Element: +50%% Damage");
                mSentenceResultList->AddElementAsTail("X", nPack, &FreeThis);
            }
        }

        //--If the life element is present, the enemy is stunned! Life cards do not affect damage
        //  computations, only execution.
        if(tElementalFlags & TL_ELEMENT_LIFE && pExecuteAction)
        {
            //--Flag.
            rEnemy->mStunTimer = CEP_STUN_TICKS;

            //--Description.
            SetMemoryData(__FILE__, __LINE__);
            SentencePack *nPack = (SentencePack *)starmemoryalloc(sizeof(SentencePack));
            nPack->Init();
            nPack->rDisplayImg = Images.Data.rIconDeath;
            sprintf(nPack->mSentence, "Life Element: Enemy Stunned");
            mSentenceResultList->AddElementAsTail("X", nPack, &FreeThis);
        }

        //--In wait mode, fire cards deal even more damage on combos. This is already applied, so create the indicator.
        if(mIsWaitMode && pExecuteAction && tFireElementBonus > 0)
        {
            CreateFireDamageIndicator(tFireElementBonus);
        }

        //--In wait mode, earth cards can slow the enemy's actions.
        if(mIsWaitMode && pExecuteAction && tEarthActionReduction > 0)
        {
            //--Set.
            mTicksReverse += tEarthActionReduction;

            //--Indicator.
            CreateEnemyProgressIndicator(tEarthActionReduction * -1);
            CreateEarthSlowdownIndicator(tEarthActionReduction);
        }

        //--In wait mode, earth cards can give free shields.
        if(mIsWaitMode && pExecuteAction && tEarthFreeShields > 0)
        {
            //--Spawn the shields.
            for(int i = 0; i < tEarthFreeShields; i ++)
            {
                GainShield(TL_ELEMENT_EARTH);
            }

            //--Indicator.
            CreateEarthShieldIndicator(tEarthFreeShields);
        }

        //--In wait mode, water cards can slow the enemy's actions.
        if(mIsWaitMode && pExecuteAction && tWaterActionReduction > 0)
        {
            mTicksReverse += tEarthActionReduction;
            CreateEnemyProgressIndicator(tWaterActionReduction * -1);
            CreateWaterSlowdownIndicator(tWaterActionReduction);
        }

        ///--[Damage Enemy]
        //--If executing the action, damage the enemy here.
        if(pExecuteAction)
        {
            //--Store enemy's previous stats.
            rEnemy->mHPTimer = 0;
            rEnemy->mHPStart = rEnemy->mHPRender;

            //--Apply that damage to the enemy.
            rEnemy->mHP -= tTotalDamage;
            rEnemy->mFlashTimer = 30;
            if(rEnemy->mHPMax > 0)
            {
                int tUseHP = rEnemy->mHP;
                if(tUseHP < 0) tUseHP = 0;
                rEnemy->mHPEnd = tUseHP / (float)rEnemy->mHPMax;
            }

            //--Sentence pack displaying the total.
            SetMemoryData(__FILE__, __LINE__);
            SentencePack *nPack = (SentencePack *)starmemoryalloc(sizeof(SentencePack));
            nPack->Init();
            nPack->rDisplayImg = Images.Data.rIconAttack;
            sprintf(nPack->mSentence, "Total Damage: %i", tTotalDamage);
            mSentenceResultList->AddElementAsTail("X", nPack, &FreeThis);

            //--If the enemy is KO'd, start the death sequence.
            if(rEnemy->mHP < 1)
            {
                //--Clamp.
                rEnemy->mHP = 0;

                //--Flags.
                mIsAnEnemyDying = true;
                mDyingEnemyTimer = ENEMY_DIE_TICKS_TOTAL;
                mDyingEnemySlot = mEnemyTarget;

                //--SFX.
                AudioManager::Fetch()->PlaySound("Combat|MonsterDie");
            }

            //--SFX.
            if(!tAtLeastOneWeakness)
            {
                AudioManager::Fetch()->PlaySound("Combat|Impact_Pierce");
            }
            else
            {
                AudioManager::Fetch()->PlaySound("Combat|Impact_Pierce_Crit");
            }
        }
        //--Otherwise, just return how much damage is expected.
        else
        {
            return tTotalDamage;
        }
    }
    ///--[Defense]
    //--Defensive actions spawn shields. One shield is spawned for each point of effect severity, with
    //  elements attached for each elemental point. The combo counter spawns more shields with just
    //  that element.
    else if(rSentenceHand[WC_DESTINATION_ACTION]->GetEffectType() == WORDCARD_EFFECT_DEFEND)
    {
        //--First, get the number of shields that will spawn with all the elemental effects.
        //  Spawn them with all elements represented in the hand.
        int tShieldsToSpawn = rSentenceHand[WC_DESTINATION_ACTION]->GetEffectSeverity();
        if(tTotalModifiers > 1) tShieldsToSpawn += tTotalModifiers - 1;

        //--If not executing the action, indicate the extra shields that spawn from earth guards.
        if(!pExecuteAction)
        {
            if(mIsWaitMode && tEarthFreeShields > 0) tShieldsToSpawn += tEarthFreeShields;
        }

        //--Executing guard:
        if(pExecuteAction)
        {
            //--Spawn the shields.
            for(int i = 0; i < tShieldsToSpawn; i ++)
            {
                //--Gain.
                GainShield(tElementalFlags);

                //--All shields past the first lose the life flag. This means the life card only provides
                //  one super shield.
                if(tElementalFlags & TL_ELEMENT_LIFE) tElementalFlags = tElementalFlags ^ TL_ELEMENT_LIFE;
            }

            //--Sentence pack displaying the total.
            SetMemoryData(__FILE__, __LINE__);
            SentencePack *nPack = (SentencePack *)starmemoryalloc(sizeof(SentencePack));
            nPack->Init();
            nPack->rDisplayImg = Images.Data.rIconAttack;
            sprintf(nPack->mSentence, "Total Guard: %i", tShieldsToSpawn);
            mSentenceResultList->AddElementAsTail("X", nPack, &FreeThis);

            //--Each point of combo spawns a bonus shield with that element.
            for(int i = 1; i < tElementalCombos[TL_ELEMENT_SLOT_FIRE];  i ++) GainShield(TL_ELEMENT_FIRE);
            for(int i = 1; i < tElementalCombos[TL_ELEMENT_SLOT_WATER]; i ++) GainShield(TL_ELEMENT_WATER);
            for(int i = 1; i < tElementalCombos[TL_ELEMENT_SLOT_WIND];  i ++) GainShield(TL_ELEMENT_WIND);
            for(int i = 1; i < tElementalCombos[TL_ELEMENT_SLOT_EARTH]; i ++) GainShield(TL_ELEMENT_EARTH);
            for(int i = 1; i < tElementalCombos[TL_ELEMENT_SLOT_LIFE];  i ++) GainShield(TL_ELEMENT_LIFE);
            for(int i = 1; i < tElementalCombos[TL_ELEMENT_SLOT_DEATH]; i ++) GainShield(TL_ELEMENT_DEATH);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Combat|Impact_Buff");

            //--In wait mode, earth cards can give free shields.
            if(mIsWaitMode && pExecuteAction && tEarthFreeShields > 0)
            {
                //--Spawn the shields.
                for(int i = 0; i < tEarthFreeShields; i ++)
                {
                    GainShield(TL_ELEMENT_EARTH);
                }

                //--Indicator.
                CreateEarthShieldIndicator(tEarthFreeShields);
            }
        }
        //--Computing, just return the guard power:
        else
        {
            return tShieldsToSpawn;
        }
    }

    //--We return 0 when actually executing the action.
    return 0;
}
void WordCombat::ClickShuffleButton()
{
    ///--[Documentation and Setup]
    //--Handles the player clicking the shuffle button. Can be done at almost any point in combat.
    mSwapCard = -1;
    mSwapCardTimer = 0;
    mClickState = WC_CLICK_STATE_NONE;

    //--In wait mode, add time and shuffle the deck.
    if(mIsWaitMode)
    {
        mTicksAllowed += mTicksPerShuffle;
        CreateEnemyProgressIndicator(mTicksPerShuffle);
        BeginShuffle();
    }
    //--If not in wait mode, clicking shuffle immediately shuffles the deck.
    else
    {
        BeginShuffle();
    }
}
void WordCombat::ClickDrawButton()
{
    ///--[Documentation and Setup]
    //--Handles clicking the draw button, which only exists during wait combat, and only when the player
    //  has cards in their deck AND at least one missing card in their hand.
    mSwapCard = -1;
    mSwapCardTimer = 0;
    mClickState = WC_CLICK_STATE_NONE;

    //--In auto-draw, ignore this click.
    if(mAutoDraw) return;

    //--Player's hand is at cap size. Stop.
    if(mrPlayerHand->GetListSize() >= mPlayerMaxHandSize) return;

    //--Player's deck has no cards. Stop.
    if(mrPlayerDeck->GetListSize() < 1) return;

    //--Checks passed.
    DrawCard();
    RecomputeHighlights();
}
void WordCombat::ClickActButton()
{
    ///--[Documentation and Setup]
    //--When the player has an action card in the action slot, this will compute the properties of the
    //  created sentence and deal the needed damage/create shields.
    mSentenceResultTimer = 0;
    mSentenceResultList->ClearList();

    //--Flags.
    mSwapCard = -1;
    mSwapCardTimer = 0;
    mClickState = WC_CLICK_STATE_NONE;

    //--A sentence without an action does nothing.
    if(!rSentenceHand[WC_DESTINATION_ACTION]) return;

    //--There must be an enemy to attack.
    CombatEnemyPack *rEnemy = (CombatEnemyPack *)mCombatEnemyList->GetElementBySlot(0);
    if(!rEnemy) return;

    //--In wait mode, add time.
    if(mIsWaitMode)
    {
        mTicksAllowed += mTicksPerAct;
        CreateEnemyProgressIndicator(mTicksPerAct);
    }

    //--Perform the damage/guard computation here. Passing true causes the action to take place.
    ComputeActionPower(true);
    mDisplayPower = 0;

    //--Clean off the sentence list. Send all its cards to the graveyard.
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
    {
        //--Skip empty slots.
        if(!rSentenceHand[i]) continue;

        //--Order it to move to the bottom of the screen.
        rSentenceHand[i]->MoveToSlide(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 200.0f);

        //--Move to graveyard.
        if(rSentenceHand[i] != mAceCard) mrPlayerGraveyard->AddElement("X", rSentenceHand[i]);
        rSentenceHand[i] = NULL;
    }
}
void WordCombat::CreateWaterSlowdownIndicator(int pSlowTicks)
{
    ///--[Documentation and Setup]
    //--Creates an indicator pack showing the enemy's movement being reduced by water cards.
    if(pSlowTicks < 1 || !Images.mIsReady) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    IndicatorPack *nPack = (IndicatorPack *)starmemoryalloc(sizeof(IndicatorPack));
    nPack->Initialize();
    mTextIndicatorList->AddElementAsTail("X", nPack, &FreeThis);

    //--Set basic properties.
    nPack->SetColor(0.10f, 0.25f, 0.50f, 1.0f);
    nPack->SetFlags(SUGARFONT_AUTOCENTER_XY);
    nPack->SetFont(Images.Data.rFontMenuCards);
    nPack->SetSize(2.0f);
    nPack->SetMaxTimer(WC_SPECIAL_EFFECT_TICKS);
    nPack->SetUpdateFloatFadeout(50.0f);

    //--Set the string.
    char tBuffer[32];
    sprintf(tBuffer, "+%i Water Slowdown", pSlowTicks);
    nPack->SetString(tBuffer);

    //--Compute position.
    float tXPos = 295.0f;
    float tYPos = 200.0f;
    nPack->SetPosition(tXPos, tYPos);
}
void WordCombat::CreateFireDamageIndicator(int pFireDamage)
{
    //--Creates an indicator pack showing the extra fire damage from fire combos.
    if(pFireDamage < 1 || !Images.mIsReady) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    IndicatorPack *nPack = (IndicatorPack *)starmemoryalloc(sizeof(IndicatorPack));
    nPack->Initialize();
    mTextIndicatorList->AddElementAsTail("X", nPack, &FreeThis);

    //--Set basic properties.
    nPack->SetColor(1.00f, 0.50f, 0.25f, 1.0f);
    nPack->SetFlags(SUGARFONT_AUTOCENTER_XY);
    nPack->SetFont(Images.Data.rFontMenuCards);
    nPack->SetSize(2.0f);
    nPack->SetMaxTimer(WC_SPECIAL_EFFECT_TICKS);
    nPack->SetUpdateFloatFadeout(50.0f);

    //--Set the string.
    char tBuffer[32];
    sprintf(tBuffer, "+%i Fire Bonus", pFireDamage);
    nPack->SetString(tBuffer);

    //--Compute position.
    float tXPos = 295.0f;
    float tYPos = 400.0f;
    nPack->SetPosition(tXPos, tYPos);
}
void WordCombat::CreateWindDrawIndicator(int pFreeDraws)
{
    //--Creates an indicator pack showing movement in the enemy action bar.
    if(pFreeDraws < 1 || !Images.mIsReady) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    IndicatorPack *nPack = (IndicatorPack *)starmemoryalloc(sizeof(IndicatorPack));
    nPack->Initialize();
    mTextIndicatorList->AddElementAsTail("X", nPack, &FreeThis);

    //--Set basic properties.
    nPack->SetColor(0.0f, 1.0f, 0.0f, 1.0f);
    nPack->SetFlags(SUGARFONT_AUTOCENTER_X);
    nPack->SetFont(Images.Data.rFontMenuCards);
    nPack->SetSize(2.0f);
    nPack->SetMaxTimer(WC_SPECIAL_EFFECT_TICKS);
    nPack->SetUpdateFloatFadeout(50.0f);

    //--Set the string.
    char tBuffer[32];
    sprintf(tBuffer, "+%i Free Draws", pFreeDraws);
    nPack->SetString(tBuffer);

    //--Compute position.
    float tXPos = 1100.0f;
    float tYPos =  400.0f;
    nPack->SetPosition(tXPos, tYPos);
}
void WordCombat::CreateEarthShieldIndicator(int pShields)
{
    //--Creates an indicator pack showing the player gaining free shields from earth cards.
    if(pShields < 1 || !Images.mIsReady) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    IndicatorPack *nPack = (IndicatorPack *)starmemoryalloc(sizeof(IndicatorPack));
    nPack->Initialize();
    mTextIndicatorList->AddElementAsTail("X", nPack, &FreeThis);

    //--Set basic properties.
    nPack->SetColor(0.50f, 0.37f, 0.25f, 1.0f);
    nPack->SetFlags(SUGARFONT_AUTOCENTER_X);
    nPack->SetFont(Images.Data.rFontMenuCards);
    nPack->SetSize(2.0f);
    nPack->SetMaxTimer(WC_SPECIAL_EFFECT_TICKS);
    nPack->SetUpdateFloatFadeout(50.0f);

    //--Set the string.
    char tBuffer[32];
    sprintf(tBuffer, "+%i Earth Shields", pShields);
    nPack->SetString(tBuffer);

    //--Compute position.
    float tXPos = 1100.0f;
    float tYPos =  200.0f;
    nPack->SetPosition(tXPos, tYPos);
}
void WordCombat::CreateEarthSlowdownIndicator(int pSlowdown)
{
    //--Creates an indicator pack showing the enemy being slowed by earth cards.
    if(pSlowdown < 1 || !Images.mIsReady) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    IndicatorPack *nPack = (IndicatorPack *)starmemoryalloc(sizeof(IndicatorPack));
    nPack->Initialize();
    mTextIndicatorList->AddElementAsTail("X", nPack, &FreeThis);

    //--Set basic properties.
    nPack->SetColor(0.50f, 0.37f, 0.25f, 1.0f);
    nPack->SetFlags(SUGARFONT_AUTOCENTER_X);
    nPack->SetFont(Images.Data.rFontMenuCards);
    nPack->SetSize(2.0f);
    nPack->SetMaxTimer(WC_SPECIAL_EFFECT_TICKS);
    nPack->SetUpdateFloatFadeout(50.0f);

    //--Set the string.
    char tBuffer[32];
    sprintf(tBuffer, "+%i Earth Slowdown", pSlowdown);
    nPack->SetString(tBuffer);

    //--Compute position.
    float tXPos = 1100.0f;
    float tYPos =  300.0f;
    nPack->SetPosition(tXPos, tYPos);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void WordCombat::Update()
{
    ///--[Documentation]
    //--Update handler.

    ///--[Timers]
    //--Screen scattering.
    if(mDamageScreenScatterTimer > 1)
    {
        //--Decrement.
        mDamageScreenScatterTimer --;

        //--Reroll every 3 ticks.
        if(mDamageScreenScatterTimer % WC_SCATTER_INTERVAL == 0)
        {
            mDamageScreenScatterX = (GetRandomPercent() - 0.50f) * WC_SCATTER_SEVERITY;
            mDamageScreenScatterY = (GetRandomPercent() - 0.50f) * WC_SCATTER_SEVERITY;
        }
    }
    //--Reset the scatter.
    else
    {
        mDamageScreenScatterX = 0.0f;
        mDamageScreenScatterY = 0.0f;
    }

    //--Shuffle radial timer.
    if(mrPlayerDeck->GetListSize() >= WC_SHUFFLE_MIN_CARDS)
    {
        if(mShuffleHighlightTimer > 0) mShuffleHighlightTimer--;
    }
    else
    {
        if(mShuffleHighlightTimer < WC_SHUFFLE_HIGHLIGHT_TICKS) mShuffleHighlightTimer ++;
    }

    //--The rotation timer always runs even if the shuffle highlight is at zero.
    mShuffleRotateTimer = (mShuffleRotateTimer + 1) % WC_SHUFFLE_HIGHLIGHT_ROTATE_TICKS;

    //--When swapping cards, run this timer.
    if(mClickState == WC_CLICK_STATE_PLAYEDCARD)
    {
        mSwapCardTimer = (mSwapCardTimer + 1) % WC_WORD_FLASH_TICKS;
    }

    //--Shield Handling
    UpdateUIObjects();

    //--Run this timer.
    if(mSentenceResultList->GetListSize() > 0)
    {
        //--Increment.
        mSentenceResultTimer ++;

        //--Reset.
        if(mSentenceResultTimer > (mSentenceResultList->GetListSize() * 15) + 120)
        {
            mSentenceResultList->ClearList();
            mSentenceResultTimer = 0;
        }
    }

    //--Healing Timer
    if(mHealingTimer > 0)
    {
        //--Decrement
        mHealingTimer --;

        //--Spawn a heart every WC_HEALING_TICKS_PER_HEART ticks.
        if(mHealingTimer % WC_HEALING_TICKS_PER_HEART == 0)
        {
            //--Spawn the heart.
            GainHearts(1, false);

            //--If this exists, reposition the heart to the potion button. This makes it flow more naturally.
            if(rLastHeart)
            {
                rLastHeart->MoveToInstant(205.0f, 720.0f);
                rLastHeart = NULL;
                PositionHearts();
            }
        }
    }

    ///--[Indicators]
    //--Update indicator packs.
    IndicatorPack *rIndPack = (IndicatorPack *)mTextIndicatorList->SetToHeadAndReturn();
    while(rIndPack)
    {
        //--Update.
        rIndPack->Update();

        //--Remove if done.
        if(rIndPack->IsComplete())
        {
            mTextIndicatorList->RemoveRandomPointerEntry();
        }

        //--Next.
        rIndPack = (IndicatorPack *)mTextIndicatorList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Ending Handler]
    //--Handles the update of the combat cycle. Should not be called if not in combat, obviously.
    if(mCombatTransitionMode == TL_COMBAT_MODE_FADEIN)
    {
        mCombatTransitionTimer ++;
        if(mCombatTransitionTimer >= COMBAT_TRANSITION_TICKS)
        {
            mCombatTransitionMode = TL_COMBAT_MODE_BATTLE;
        }
        return;
    }
    //--Victory.
    else if(mCombatTransitionMode == TL_COMBAT_MODE_VICTORY)
    {
        //--Initializer.
        if(mEndingPhase == TL_COMBAT_ENDING_NONE)
        {
            mCombatTransitionTimer = 0;
            mEndingPhase = TL_COMBAT_ENDING_VICTORY_JINGLE;
            AudioManager::Fetch()->FadeMusic(2.0f);
            AudioManager::Fetch()->PlayMusicNoFade("Doll|BattleThemeOutro");
        }
        //--Jingle plays, display Victory.
        else
        {
            //--Timer.
            mCombatTransitionTimer ++;

            //--At this point, end combat.
            if(mCombatTransitionTimer >= TL_VICTORY_TIME_TOTAL)
            {
                mCombatResolution = TL_COMBAT_PLAYER_WINS;
            }
        }

        return;
    }
    //--Defeat.
    else if(mCombatTransitionMode == TL_COMBAT_MODE_DEFEAT)
    {
        //--Initializer.
        if(mEndingPhase == TL_COMBAT_ENDING_NONE)
        {
            mCombatTransitionTimer = 0;
            mEndingPhase = TL_COMBAT_ENDING_VICTORY_JINGLE;
            AudioManager::Fetch()->FadeMusic(2.0f);
            AudioManager::Fetch()->PlayMusicNoFade("Doll|BattleThemeOutro");
        }
        //--Jingle plays, display Defeat.
        else
        {
            //--Timer.
            mCombatTransitionTimer ++;

            //--At this point, end combat.
            if(mCombatTransitionTimer >= TL_VICTORY_TIME_TOTAL)
            {
                mCombatResolution = TL_COMBAT_PLAYER_DEFEATED;
            }
        }
        return;
    }

    //--If the player is defeated, end.
    if(mPlayerHP < 1)
    {
        mCombatTransitionTimer = 0;
        mCombatTransitionMode = TL_COMBAT_MODE_DEFEAT;
        return;
    }

    //--If all enemies are defeated, end.
    if(mCombatEnemyList->GetListSize() < 1)
    {
        mCombatTransitionTimer = 0;
        mCombatTransitionMode = TL_COMBAT_MODE_VICTORY;
        return;
    }

    ///--[Main Update]
    //--When not in wait mode, enemy timers always update.
    bool tAllowAceIncrement = true;
    if(!mIsWaitMode)
    {
        UpdateEnemies(true);
    }
    //--If not in wait mode, enemy timers only update if ticks are available.
    else
    {
        //--Ace does not increment unless time is passing.
        tAllowAceIncrement = false;

        //--Update enemies.
        UpdateEnemies(mTicksAllowed > 0);

        //--Pass "time".
        if(mTicksAllowed > 0)
        {
            mTicksAllowed --;
            tAllowAceIncrement = true;
        }
    }

    //--When shuffling, update the shuffle timer.
    if(mIsShuffling)
    {
        //--Timer.
        mShuffleTimer ++;

        //--SFX.
        if(mShuffleTimer % 60 == 55)
        {
            AudioManager::Fetch()->PlaySound("World|TakeItem");
        }

        //--Ending case.
        if(mShuffleTimer >= mShuffleTimerMax)
        {
            mIsShuffling = false;
            ShuffleDeck();
        }
    }

    //--Ace handling. If the Ace is respawning, run the timer.
    if(mAceTimer < WC_ACE_TICKS)
    {
        //--Check if the Ace is currently in the sentence. If so, don't increment the timer.
        bool tHasAceInSentence = false;
        for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
        {
            if(rSentenceHand[i] == mAceCard)
            {
                tHasAceInSentence = true;
                break;
            }
        }

        //--Increment.
        if(!tHasAceInSentence && tAllowAceIncrement)
        {
            mAceTimer ++;
        }

        //--Ace card respawns.
        if(mAceTimer >= WC_ACE_TICKS)
        {
            mAceCard->MoveToInstantly(WC_ACE_POS_X, WC_ACE_POS_Y + 400.0f);
            mAceCard->MoveToSlide(    WC_ACE_POS_X, WC_ACE_POS_Y);
        }
    }

    //--In all cases, update the Ace card, if it exists.
    if(mAceCard) mAceCard->Update();

    //--Player's deck power increases. The smaller the player's current hand, the faster it increases.
    //  This does nothing in wait mode.
    if(!mIsWaitMode)
    {
        //--Figure out the rate of increase.
        int tPlayerHandSize = mrPlayerHand->GetListSize();
        float tSecondsForFullCharge = SECONDS_FOR_FULL_CHARGE;
        if(tPlayerHandSize == 0) tSecondsForFullCharge = 0.5f;
        else if(tPlayerHandSize == 1) tSecondsForFullCharge = 1.0f;
        else if(tPlayerHandSize == 2) tSecondsForFullCharge = 1.5f;
        else if(tPlayerHandSize == 3) tSecondsForFullCharge = 2.2f;
        else if(tPlayerHandSize == 4) tSecondsForFullCharge = 3.6f;
        else if(tPlayerHandSize == 5) tSecondsForFullCharge = 5.0f;
        else if(tPlayerHandSize == 6) tSecondsForFullCharge = 9.0f;
        else if(tPlayerHandSize == 7) tSecondsForFullCharge = 12.0f;
        else tSecondsForFullCharge = 15.0f;

        //--Increment.
        mDeckPowerGainPerTick = FULL_CHARGE_POWER / (tSecondsForFullCharge * (float)TICKS_PER_SECOND);
        mDeckPower = mDeckPower + mDeckPowerGainPerTick;
        if(mDeckPower > mDeckPowerMax) mDeckPower = mDeckPowerMax;
    }

    //--Auto-draw. If this is on, and the player needs cards, draw them.
    if(!mIsWaitMode && mAutoDraw)
    {
        if(mDeckPower >= mDeckPowerPerCard && mrPlayerHand->GetListSize() < mPlayerMaxHandSize && mrPlayerDeck->GetListSize() > 0)
        {
            DrawCard();
            RecomputeHighlights();
        }
    }

    //--Now update the cards in the master list.
    WordCard *rUpdateCard = (WordCard *)mGlobalCardDeck->PushIterator();
    while(rUpdateCard)
    {
        rUpdateCard->Update();
        rUpdateCard = (WordCard *)mGlobalCardDeck->AutoIterate();
    }

    //--Run this timer.
    mClickTimer ++;

    ///--[Click Handling]
    //--Get mouse information.
    int tMouseX, tMouseY, tMouseZ;
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Is the mouse being clicked this tick? Shuffling locks out controls for most things.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Common: Clicking the shuffle button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mShuffleButton) && !mIsShuffling)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            ClickShuffleButton();
            return;
        }

        //--Common: Clicking the draw button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mDrawButton) && !mIsShuffling)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            ClickDrawButton();
            return;
        }

        //--Common: Clicking the act button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mActButton) && !mIsShuffling)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            ClickActButton();
            RecomputeHighlights();
            return;
        }

        //--Common: Clicking the surrender button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mSurrenderButton))
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mPlayerHP = 0;
            return;
        }

        //--Common: Clicking the potion button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mPotionButton))
        {
            //--No potions, the player's HP is full, or potions are currently ticking.
            if(mPotions < 1 || mPlayerHP >= mPlayerHPMax || mHealingTimer > 0)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Otherwise, remove a potion and restore HP.
            else
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("Combat|Heal");

                //--Set the heart spawning timer.
                int tCurrentHearts = mPlayerHeartList->GetListSize();
                int tHeartsNeeded = mPlayerHPMax / 2;
                int tHeartsToSpawn = tHeartsNeeded - tCurrentHearts;
                mHealingTimer = (tHeartsToSpawn * WC_HEALING_TICKS_PER_HEART) - 1;

                //--Time passes.
                if(mIsWaitMode)
                {
                    mTicksAllowed += mTicksPerPotion;
                    CreateEnemyProgressIndicator(mTicksPerPotion);
                }

                //--Heal.
                mPotions --;
                mPlayerHP = mPlayerHPMax;
            }
            return;
        }

        //--Cards cannot be clicked while shuffling.
        if(mIsShuffling) return;

        //--First, if there is currently nothing going on with the clicking of objects:
        if(mClickState == WC_CLICK_STATE_NONE)
        {
            //--Flag.
            bool tClickedACard = false;

            //--Run across all the words in the player's hand. Clicking one of them marks them as the clicked object.
            WordCard *rHandCard = (WordCard *)mrPlayerHand->PushIterator();
            while(rHandCard)
            {
                //--Card was clicked. Set it as the active card.
                if(rHandCard->IsPointWithin(tMouseX, tMouseY))
                {
                    //--Flag.
                    tClickedACard = true;

                    //--Manual mode:
                    if(!mAutoPlace)
                    {
                        //--Set this as the clicked card.
                        mClickState = WC_CLICK_STATE_HANDCARD;
                        rClickedCard = rHandCard;
                        mClickTimer = 0;

                        //--Build markers.
                        BuildDestinationMarkers(rClickedCard);

                        //--SFX.
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Automatic placement. Handles the rest for us.
                    else
                    {
                        //--Place the card. If it fails, play a sound.
                        mLastReplaceWasFree = false;
                        if(!AutoplaceCard(rHandCard))
                        {
                            AudioManager::Fetch()->PlaySound("Menu|Failed");
                        }
                        else
                        {
                            //--SFX.
                            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

                            //--Time passes.
                            if(mIsWaitMode && !mLastReplaceWasFree)
                            {
                                mTicksAllowed += mTicksPerCardMove;
                                CreateEnemyProgressIndicator(mTicksPerCardMove);
                            }
                        }

                        //--Recompute all highlights.
                        RecomputeHighlights();
                    }

                    //--Break out.
                    mrPlayerHand->PopIterator();
                    break;
                }

                //--Next.
                rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
            }

            //--Ace is handled the same way.
            if(mAceTimer >= WC_ACE_TICKS && mAceCard && !tClickedACard)
            {
                //--Card was clicked. Set it as the active card.
                if(mAceCard->IsPointWithin(tMouseX, tMouseY))
                {
                    //--Flag.
                    tClickedACard = true;

                    //--Manual mode:
                    if(!mAutoPlace)
                    {
                        //--Set this as the clicked card.
                        mClickState = WC_CLICK_STATE_HANDCARD;
                        rClickedCard = mAceCard;
                        mClickTimer = 0;

                        //--Build markers.
                        BuildDestinationMarkers(rClickedCard);

                        //--SFX.
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Automatic placement. Handles the rest for us.
                    else
                    {
                        //--Place the card. If it fails, play a sound.
                        mLastReplaceWasFree = false;
                        if(!AutoplaceCard(mAceCard))
                        {
                            AudioManager::Fetch()->PlaySound("Menu|Failed");
                        }
                        else
                        {
                            //--Time passes.
                            if(mIsWaitMode && !mLastReplaceWasFree)
                            {
                                mTicksAllowed += mTicksPerCardMove;
                                CreateEnemyProgressIndicator(mTicksPerCardMove);
                            }

                            //--SFX.
                            mAceTimer = 0;
                            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                        }

                        //--Recompute all highlights.
                        RecomputeHighlights();
                    }
                }
            }

            //--Run across the played cards. They can be clicked if they are element cards.
            if(!tClickedACard)
            {
                //--Iterate.
                for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
                {
                    //--Skip blank cards.
                    if(!rSentenceHand[i]) continue;

                    //--If the card was not clicked, ignore it.
                    if(!rSentenceHand[i]->IsPointWithin(tMouseX, tMouseY)) continue;

                    //--Element cards need a second click to do anything:
                    if(rSentenceHand[i]->GetCardType() == WORDCARD_TYPE_MODIFIER)
                    {
                        mSwapCard = i;
                        mSwapCardTimer = 0;
                        mClickState = WC_CLICK_STATE_PLAYEDCARD;
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                        break;
                    }
                    //--And/Of cards return to the hand with one click.
                    else if(rSentenceHand[i]->GetCardType() == WORDCARD_TYPE_OF || rSentenceHand[i]->GetCardType() == WORDCARD_TYPE_AND)
                    {
                        //--Send the selected card to the hand.
                        MoveSentenceCardToHand(i);

                        //--If this is an OF card, and the last slot has an element, dump the element.
                        if(i == WC_DESTINATION_OFSLOT)
                        {
                            MoveSentenceCardToHand(WC_DESTINATION_TRAILINGSLOT);
                        }
                        //--It's an AND card, move all cards below it to the hand.
                        else
                        {
                            for(int p = 0; p < i; p ++) MoveSentenceCardToHand(p);
                        }

                        //--SFX.
                        RecomputeHighlights();
                        AudioManager::Fetch()->PlaySound("Menu|Select");
                        break;
                    }
                }
            }
        }
        //--A card from the hand has been clicked:
        else if(mClickState == WC_CLICK_STATE_HANDCARD)
        {
            //--Check across all destinations. If a valid one is clicked, move the card to that destination.
            for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
            {
                //--Skip inactive destinations.
                if(!mDestinationActivities[i]) continue;

                //--Check if the click is in this zone.
                if(IsPointWithin2DReal(tMouseX, tMouseY, mDestinationButtons[i]))
                {
                    //--If the slot is already occupied, remove the card from it and dump it back into the hand.
                    if(rSentenceHand[i])
                    {
                        rSentenceHand[i]->SetAssignedIndex(-1);
                        mrPlayerHand->AddElementAsTail("X", rSentenceHand[i]);
                    }

                    //--Liberate the card from our hand and place it in the sentence's hand.
                    mrPlayerHand->RemoveElementP(rClickedCard);
                    rSentenceHand[i] = rClickedCard;
                    rClickedCard->SetAssignedIndex(-1);

                    //--Order the card to reposition.
                    rClickedCard->MoveToSlide(mDestinationPositions[i], mDestinationButtons[i].mYCenter);

                    //--SFX.
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

                    //--At least one card had to move, so reposition all cards in the hand.
                    PositionCardsInHand();
                }
            }

            //--No matter what happens, unset the click state and clear the active card.
            mClickState = WC_CLICK_STATE_NONE;
            rClickedCard = NULL;
            BuildDestinationMarkers(NULL);

            //--Recompute all highlights.
            RecomputeHighlights();
        }
        //--A card from the played sentence was clicked. Clicking a hand card can swap elements. Clicking anywhere
        //  else cancels this mode.
        else if(mClickState == WC_CLICK_STATE_PLAYEDCARD)
        {
            //--Flag.
            bool tClickedAnyCard = false;

            //--Run across all the words in the player's hand. Elemental cards will swap with the selected sentence card.
            WordCard *rHandCard = (WordCard *)mrPlayerHand->PushIterator();
            while(rHandCard)
            {
                //--If the card was not clicked, ignore it.
                if(!rHandCard->IsPointWithin(tMouseX, tMouseY))
                {
                    rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
                    continue;
                }

                //--Card was clicked, but is not an element card. Cancel this mode and play a sound effect.
                if(rHandCard->GetCardType() != WORDCARD_TYPE_MODIFIER)
                {
                    tClickedAnyCard = true;
                    mSwapCard = -1;
                    mSwapCardTimer = 0;
                    mClickState = WC_CLICK_STATE_NONE;
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    mrPlayerHand->PopIterator();
                    break;
                }

                //--If we got this far, it's a swap action. The existing action card should be dumped into the hand.
                if(rSentenceHand[mSwapCard])
                {
                    rSentenceHand[mSwapCard]->SetAssignedIndex(-1);
                    mrPlayerHand->AddElementAsTail("X", rSentenceHand[mSwapCard]);
                }

                //--Liberate the card from our hand and place it in the sentence's hand.
                mrPlayerHand->RemoveElementP(rHandCard);
                rSentenceHand[mSwapCard] = rHandCard;

                //--Order the card to reposition.
                rHandCard->MoveToSlide(mDestinationPositions[mSwapCard], mDestinationButtons[mSwapCard].mYCenter);

                //--At least one card had to move, so reposition all cards in the hand.
                PositionCardsInHand();
                RecomputeHighlights();
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                mDisplayPower = ComputeActionPower(false);

                //--Finish looping.
                mClickState = WC_CLICK_STATE_NONE;
                tClickedAnyCard = true;
                mrPlayerHand->PopIterator();
                break;
            }

            //--Ace is handled the same way. Ace can theoretically be an elemental card.
            if(mAceTimer >= WC_ACE_TICKS && mAceCard && mAceCard->GetCardType() == WORDCARD_TYPE_MODIFIER && !tClickedAnyCard)
            {
                //--Click must be on the Ace.
                if(mAceCard->IsPointWithin(tMouseX, tMouseY))
                {
                    //--Swap!
                    if(rSentenceHand[mSwapCard])
                    {
                        rSentenceHand[mSwapCard]->SetAssignedIndex(-1);
                        mrPlayerHand->AddElementAsTail("X", rSentenceHand[mSwapCard]);
                    }

                    //--Liberate the card from our hand and place it in the sentence's hand.
                    rSentenceHand[mSwapCard] = mAceCard;

                    //--Order the card to reposition.
                    mAceCard->MoveToSlide(mDestinationPositions[mSwapCard], mDestinationButtons[mSwapCard].mYCenter);

                    //--At least one card had to move, so reposition all cards in the hand.
                    PositionCardsInHand();
                    RecomputeHighlights();
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mDisplayPower = ComputeActionPower(false);

                    //--Finish looping.
                    mAceTimer = 0;
                    mClickState = WC_CLICK_STATE_NONE;
                    tClickedAnyCard = true;
                }
            }

            //--Run across the played cards. They can be clicked if they are element cards.
            if(!tClickedAnyCard)
            {
                for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
                {
                    //--Skip blank cards.
                    if(!rSentenceHand[i]) continue;

                    //--If the card was not clicked, ignore it.
                    if(!rSentenceHand[i]->IsPointWithin(tMouseX, tMouseY)) continue;

                    //--Card was clicked. Only element cards can be swapped, ignore all others.
                    if(rSentenceHand[i]->GetCardType() != WORDCARD_TYPE_MODIFIER) continue;

                    //--Set flag.
                    tClickedAnyCard = true;

                    //--Card is in the same slot as the swap card, so dump it back to the hand.
                    if(mSwapCard == i)
                    {
                        //--Base.
                        MoveSentenceCardToHand(i);

                        //--If this is a mod slot, dump all cards before it back to the hand.
                        if(i < WC_DESTINATION_MODSLOTS)
                        {
                            for(int p = 0; p < i; p ++) MoveSentenceCardToHand(p);
                        }

                        //--SFX.
                        RecomputeHighlights();
                        AudioManager::Fetch()->PlaySound("Menu|Select");
                    }
                    //--Otherwise, swap them.
                    else
                    {
                        //--Swap.
                        WordCard *rTempCard = rSentenceHand[i];
                        rSentenceHand[i] = rSentenceHand[mSwapCard];
                        rSentenceHand[mSwapCard] = rTempCard;

                        //--Position.
                        rSentenceHand[i]->MoveToSlide(mDestinationPositions[i], mDestinationButtons[i].mYCenter);
                        rSentenceHand[mSwapCard]->MoveToSlide(mDestinationPositions[mSwapCard], mDestinationButtons[mSwapCard].mYCenter);

                        //--SFX and Highlight.
                        AudioManager::Fetch()->PlaySound("Menu|Select");
                        PositionCardsInHand();
                        RecomputeHighlights();
                        mDisplayPower = ComputeActionPower(false);
                    }

                    //--Clear.
                    mSwapCard = -1;
                    mSwapCardTimer = 0;
                    mClickState = WC_CLICK_STATE_NONE;
                    break;
                }
            }

            //--If no card got clicked, unset swap mode.
            if(!tClickedAnyCard)
            {
                mSwapCard = -1;
                mSwapCardTimer = 0;
                mClickState = WC_CLICK_STATE_NONE;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
    }

    //--Run this timer if nothing else is happening.
    mUITransitionTimer ++;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
