///====================================== CarnationLevel ==========================================
//--Level structure used by Project Carnation. This is a dungeon-crawler interface.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "TiledLevel.h"

///===================================== Local Structures =========================================
#include "AdventureLevelStructures.h"

typedef struct CarnationRoom
{
    //--Members
    char mName[128];
    char mLayout[128];
    char mBGPath[128];
    int mX;
    int mY;
    bool mIsSafe;
    int mDanger;
    char mEnemy[128];
    char mMusic[128];
    char mParticlePath[128];
    int mParticleRate;

    //--Functions
    void Initialize()
    {
        mName[0] = '\0';
        mLayout[0] = '\0';
        mBGPath[0] = '\0';
        mX = 0;
        mY = 0;
        mIsSafe = true;
        mDanger = 0;
        mEnemy[0] = '\0';
        mMusic[0] = '\0';
        strcpy(mParticlePath, "Null");
        mParticleRate = 0;
    }
}CarnationRoom;
typedef struct CarnationConnection
{
    //--Members
    char mName[128];
    int mLft;
    int mTop;
    int mRgt;
    int mBot;

    //--Functions
    void Initialize()
    {
        mName[0] = '\0';
        mLft = 0;
        mTop = 0;
        mRgt = 1;
        mBot = 1;
    }
}CarnationConnection;

///===================================== Local Definitions ========================================
#define REGPOPUP_HEART_OFF_X 20
#define CL_COMBAT_TICKS 15
#define CL_MINIMAP_TICKS 30

///--[Transition Sequence]
//--Switch codes
#define CL_TRANSITION_FADEOUT 0
#define CL_TRANSITION_LEVELSWITCH 1
#define CL_TRANSITION_FADEIN 2

//--Timers
#define CL_TRANSITION_FADEOUT_TICKS 15
#define CL_TRANSITION_LEVELSWITCH_TICKS 5
#define CL_TRANSITION_FADEIN_TICKS 15

///--[Stencils]
#define CL_STENCIL_MINIMAP 1
#define CL_STENCIL_SHADOWING 2
#define CL_STENCIL_PARTY_START 10

///========================================== Classes =============================================
class CarnationLevel : public TiledLevel
{
    private:
    ///--[Constants]
    static const int cxTimer_ConfirmRetreat = 15;

    ///--[System]
    char *mCurrentAreaName;
    char *mTriggersPath;

    ///--[Description Window]
    WorldDialogue *mInternalDialogue;

    ///--[Examinable Objects]
    void *rHighlightedExaminable; //Do not dereference.
    StarLinkedList *mExaminablesList; //CarnExaminable *, master

    ///--[Input]
    //--Mouse Coordinates
    int mMouseX;
    int mMouseY;
    int mMouseZ;

    //--Fixed buttons.
    TwoDimensionReal mCharacterA;
    TwoDimensionReal mCharacterB;
    TwoDimensionReal mCharacterC;
    TwoDimensionReal mButtonJournal;
    TwoDimensionReal mButtonBackpack;
    TwoDimensionReal mButtonOptions;

    //--Popup Menu
    StarLinkedList *mPopupList; //CarnationPopup *, master

    ///--[Level Transition]
    bool mIsLevelTransitioning;
    char *mTransitionToScript;
    int mTransitionTimer;
    int mTransitionStage;

    ///--[Alarm, Hostility, Encounters]
    //--Alarm State
    EasingPack1D mAlarmVal;
    float mAlarmShake;
    float mAlarmShakeDelta;
    float mAlarmShakeCap;
    float mAlarmShakePush;
    int mAlarmShakeZeroTicks;

    ///--[Dungeon Retreat]
    bool mCanRetreat;
    bool mIsConfirmRetreat;
    int mConfirmRetreatTimer;
    char *mRetreatScript;
    TwoDimensionReal mButtonConfirmRetreat;
    TwoDimensionReal mButtonCancelRetreat;

    ///--[Event Notifications]
    StarLinkedList *mEventNotificationList; //CarnLevelEvent *, master

    ///--[GUI]
    //--Menu
    AdventureMenu *mMenu;

    //--Combat Handling
    int mCombatTimer;

    //--Minimap
    bool mIsMinimapMode;
    bool mIsMinimapDrag;
    int mMinimapDragX;
    int mMinimapDragY;
    int mMinimapTimer;
    float mMinimapOffsetX;
    float mMinimapOffsetY;
    CarnationMinimap *mMinimap;

    //--Auras
    int mSpawnAuraChance;
    StarBitmap *rSpawnAuraImage;
    StarLinkedList *mAuraParticleList; //AuraParticle *, master

    ///--[Queryable SLF Data]
    char *mQuerySLFName;
    char *mQuerySLFImage;
    StarLinkedList *mQuerySLFRoomInfo;       //CarnationRoom *, master
    StarLinkedList *mQuerySLFConnectionInfo; //CarnationConnection *, master

    ///--[Images]
    //--Layers
    StarLinkedList *mBackgroundLayers; //CarnMapLayer *, master

    //--UI Pieces
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontTitle;
            StarFont *rFontPopup;
            StarFont *rFont_Tabs;
            StarFont *rFont_MinimapHelp;
            StarFont *rFont_MinimapTitle;

            //--Images
            StarBitmap *rButton_CenterOnPlayer;
            StarBitmap *rFrameDialogue;
            StarBitmap *rFrameExaminableNameBack;
            StarBitmap *rFrameMinimapMask;
            StarBitmap *rFrameMouseDecision;
            StarBitmap *rFramePartyBack;
            StarBitmap *rFramePartyFront;
            StarBitmap *rFramePopupBacking;
            StarBitmap *rFrameRightBlock;
            StarBitmap *rFrameRoomName;
            StarBitmap *rOverlayAdvance;
            StarBitmap *rOverlayAlarm0;
            StarBitmap *rOverlayAlarm1;
            StarBitmap *rOverlayAlarm2;
            StarBitmap *rOverlayAlarm3;
            StarBitmap *rOverlayHeart;
            StarBitmap *rTabBackpack;
            StarBitmap *rTabJournal;
            StarBitmap *rTabOptions;
        }Data;
    }Images;

    protected:

    public:
    //--System
    CarnationLevel();
    virtual ~CarnationLevel();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    bool AreAnyPopupsAcceptingClicks();
    bool IsLayeringMusic();

    //--Manipulators
    void SetTriggersPath(const char *pPath);
    void RegisterBackgroundLayer(const char *pName, int pRenderOrder, float pXOffset, float pYOffset, const char *pDLPath);
    void SetBackgroundLayerVisible(const char *pName, bool pIsVisible, int pTicks);
    void SetLevelName(const char *pName);
    void SetAlarmTarget(int pValue, int pTicks);
    void KickAlarm(float pPower);
    void KickAlarmAuto();
    void SetAuraProperties(int pSpawnChance, const char *pParticleDLPath);
    void SetRetreatable(bool pIsRetreatable);
    void SetRetreatScript(const char *pPath);

    //--Events
    void CreateEventNotification(const char *pText);
    void UpdateEvents();
    bool UpdateEventLeftClick(float pX, float pY);
    void RenderEvents();

    //--Examinables and Popups
    bool IsExaminableAt(float pX, float pY);
    bool DoesExaminableExist(const char *pInternalName);
    void RegisterExaminable(const char *pInternalName, int pX, int pY, const char *pDisplayName, const char *pScript, const char *pArgument);
    void RemoveExaminable(const char *pInternalName);
    void RegisterPopupFromExaminable(const char *pInternalName, const char *pExaminableName, const char *pDisplayName);
    void AddEntryToPopup(const char *pPopupName, const char *pInternalName, const char *pDisplayName, const char *pScript, const char *pArgument);
    void RecomputePopupSizes(const char *pPopupName);
    void CloseAllPopups();
    void SetExaminableAlwaysVisible(const char *pInternalName, bool pFlag);
    void SetExaminableShowsDisplayName(const char *pInternalName, bool pFlag);
    void SetExaminableHighlightValue(const char *pInternalName, int pValue);

    //--Core Methods
    void SortBackgroundLayers();
    static int CompareBackgroundLayer(const void *pEntryA, const void *pEntryB);
    void BeginTransitionTo(const char *pConstructorPath);
    void ClearOldLevelData();
    void RunAutosave();

    //--Parser
    void ExecCarnationParse(const char *pPath);
    void DeallocateCarnationParse();
    virtual void ParseObjectData();
    void HandleObject(ObjectInfoPack *pPack);
    const char *GetParsedLevelName();
    const char *GetParsedImagePath();
    int GetParsedXSize();
    int GetParsedYSize();
    int GetParsedCollision(int pX, int pY);
    int GetParsedRoomCount();
    CarnationRoom *GetParsedRoomEntry(int pSlot);
    const char *GetParsedConnectionInDirectionFrom(int pSlot, int pDirection);
    const char *GetRoomOnOtherSideOfConnection(int pX, int pY, int pDirection);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    bool EmulateClickAt(float pX, float pY);

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();
    void RenderFixedUI(float pAlpha);
    void RenderExaminables(float pAlpha);
    void RenderPopups(float pAlpha);
    void RenderExpandedMinimap();

    //--Pointer Routing
    CarnationMinimap *GetMinimap();
    WorldDialogue *GetInternalDialogue();
    AdventureMenu *GetMenu();

    //--Static Functions
    static CarnationLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnationLevel_Create(lua_State *L);
int Hook_CarnationLevel_GetProperty(lua_State *L);
int Hook_CarnationLevel_SetProperty(lua_State *L);
int Hook_CarnDia_SetProperty(lua_State *L);
int Hook_CarnDia_GetProperty(lua_State *L);
int Hook_CarnAppend(lua_State *L);
