///======================================= CarnationTitle =========================================
//--Title screen for Project Carnation.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "MonocerosTitle.h"
#include "StarUIMouse.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define CT_MODE_TITLE 0
#define CT_MODE_LOADFILE 1
#define CT_MODE_OPTIONS 2

///========================================== Classes =============================================
class CarnationTitle : public MonocerosTitle, public StarUIMouse
{
    private:
    ///--[Constants]
    //--Timers
    static const int cxTimer_ClickPromptMax = 120;
    static const int cxTimer_ClickPromptFadeIn = 15;
    static const int cxTimer_ClickExitMax = 15;
    static const int cxTimer_TitleMax = 15;
    static const int cxTimer_LoadFileMax = 30;
    static const int cxTimer_TransitTicks = 30;

    //--Sizes
    static const int cxScroll_SavesPerPage = 3;
    static const int cxPos_SaveBoxYOffset = 180.0f;

    //--Hitboxes
    static const int cxHitbox_NewGame     =  0;
    static const int cxHitbox_Continue    =  1;
    static const int cxHitbox_Options     =  2;
    static const int cxHitbox_Quit        =  3;
    static const int cxHitbox_ScrollbarUp =  4;
    static const int cxHitbox_ScrollbarMd =  5;
    static const int cxHitbox_ScrollbarDn =  6;
    static const int cxHitbox_SelectFile0 =  7;
    static const int cxHitbox_SelectFile1 =  8;
    static const int cxHitbox_SelectFile2 =  9;
    static const int cxHitbox_BackToTitle = 10;

    ///--[System]
    bool mHasClickedToBegin;
    int mMode;
    int mHighlightedHitbox;

    ///--[Timers]
    int mClickPromptTimer;
    int mClickExitTimer;
    int mTitleTimer;
    int mLoadFileTimer;

    ///--[Transition Work]
    int mTransitTimer;
    bool mTransitToNewGame;

    ///--[Options Menu]
    CarnUIOptions *mCarnOptionsMenu;

    ///--[Images]
    bool mImagesReady;
    struct
    {
        //--Fonts
        StarFont *rFont_Header;
        StarFont *rFont_Mainline;
        StarFont *rFont_SaveMain;
        StarFont *rFont_Small;

        //--Images
        StarBitmap *rBackground_Main;
        StarBitmap *rFrame_Button;
        StarBitmap *rFrame_SaveInfo;
        StarBitmap *rOverlay_BlackLft;
        StarBitmap *rOverlay_BlackRgt;
        StarBitmap *rOverlay_Title;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rScrollbar_Static;
    }CarnImages;

    protected:

    public:
    //--System
    CarnationTitle();
    virtual ~CarnationTitle();
    virtual void Construct();

    //--Public Variables
    virtual bool IsOfType(int pType);

    //--Property Queries
    //--Manipulators
    //--Core Methods
    void RunTimer(int &sTimer, int pLo, int pHi, bool pShouldIncrement);
    void ActivateNoteEntry(int pLoadingCursor, LoadingPack *pLoadingPack);

    ///--[LoadFile]
    void LoadFile(LoadingPack *pLoadingPack);
    void UpdateLoadFile();
    void RenderLoadFile();
    virtual void RenderLoadingMode();
    void RenderFileBox(const char *pFileName, LoadingPack *pLoadingPack);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    void UpdateMainTitle();
    void UpdateOptions();

    //--File I/O
    //--Drawing
    virtual void Render();
    void RenderMainTitle();
    void RenderButton(const char *pHitboxName, const char *pText, uint32_t pFlags);

    //--Pointer Routing
    //--Static Functions
    static FlexMenu *GenerateCarnationTitle();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CT_SetProperty(lua_State *L);


