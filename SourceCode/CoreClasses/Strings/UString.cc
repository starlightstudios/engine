//--Base
#include "UString.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

#if defined _ALLEGRO_PROJECT_

///========================================== System ==============================================
UString::UString()
{
    //--System
    mUString = NULL;
}
UString::~UString()
{
    Clear();
}

///===================================== Property Queries =========================================
ALLEGRO_USTR *UString::GetRaw()
{
    return mUString;
}
const char *UString::GetCString()
{
    if(!mUString) return NULL;
    return al_cstr(mUString);
}
int UString::GetLength()
{
    if(!mUString) return 0;
    return al_ustr_length(mUString);
}
int UString::GetLastSpace()
{
    if(!mUString) return 0;
    int tSize = al_ustr_size(mUString);

    //--Step backwards through each code point to find a space.
    int tLength = GetLength();
    int tIndex = tSize;
    for(int i = 0; i < tLength; i ++)
    {
        if(al_ustr_prev_get(mUString, &tIndex) == ' ') return tLength - i;
    }

    return 0;
}

///======================================= Manipulators ===========================================
void UString::Set(const char *pCString)
{
    Clear();
    mUString = al_ustr_new(pCString);
}
void UString::Cat(int32_t pLetter)
{
    al_ustr_append_chr(mUString, pLetter);
}
void UString::Insert(int pPos, const char *pCString)
{
    if(!mUString || !pCString) return;
    al_ustr_insert_cstr(mUString, pPos, pCString);
}

///======================================= Core Methods ===========================================
void UString::Clear()
{
    al_ustr_free(mUString);
    mUString = NULL;
}
UString *UString::PullOffXChars(int pCharacters)
{
    //--Exracts the given number of characters from the end of the UString, and
    //  creates a new UString to hold them, and returns that.  You must clean
    //  it if you're not using it.
    if(!mUString) return NULL;
    if(GetLength() <= pCharacters) return NULL;

    //--New String
    int tSize = al_ustr_size(mUString);
    UString *nString = new UString();
    nString->Set("");

    //--Step backwards through each code point to find the specified start.
    int tIndex = tSize;
    for(int i = 0; i < pCharacters; i ++)
    {
        al_ustr_prev(mUString, &tIndex);
    }

    //--tIndex represents the start of the UString, start catting these onto
    //  the new UString.
    int tStart = tIndex;
    for(int i = 0; i < pCharacters; i ++)
    {
        int32_t tLetter = al_ustr_get_next(mUString, &tIndex);
        nString->Cat(tLetter);
    }

    //--Remove the parts we copied off.
    al_ustr_remove_range(mUString, tStart, tSize);

    //--Finish up
    return nString;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================

#endif
