//--Base
#include "Pathrunner.h"

//--Classes
#include "AdventureLevel.h"
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Local Definitions]
///--[Local Structures]
///--[Debug]
//#define PATHRUNNER_ADVANCED_PATH_DEBUG
#ifdef PATHRUNNER_ADVANCED_PATH_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
Pathrunner::Pathrunner()
{
    ///--[ ====== Variable Initialization ===== ]
    ///--[Pathrunner]
    //--System
    mFailure = false;
    mHasAnyPath = false;
    mIsInitialized = false;
    mWalkDist = ADVP_WALK_DIST;
    mIterationsToRun = ADVP_NORMAL_ITERATIONS;

    //--Collision Storage
    mXSize = 0;
    mYSize = 0;
    mPathArray = NULL;

    //--Flags
    mStartX = 0;
    mStartY = 0;
    mEndX = 1;
    mEndY = 1;
    mShortestDistance = 0.0f;
    mOriginalDistance = 0.0f;

    //--Path Runner
    mAllowGridAligning = true;
    mPulseTotal = 0;
    mCollisionDepth = 0;
    rActiveLevel = NULL;
    mPathRunnerList = NULL;
}
Pathrunner::~Pathrunner()
{
    for(int x = 0; x < mXSize; x ++) free(mPathArray[x]);
    free(mPathArray);
    delete mPathRunnerList;
}

///--[Static Variables]
StarLinkedList *Pathrunner::xStaticPathList = new StarLinkedList(true);

///===================================== Property Queries =========================================
bool Pathrunner::IsInitialized()
{
    return mIsInitialized;
}
bool Pathrunner::IsFailed()
{
    return mFailure;
}
bool Pathrunner::HasPulsesRemaining()
{
    if(!mPathRunnerList) return false;
    return (mPathRunnerList->GetListSize() > 0);
}
int Pathrunner::GetPulsesRemaining()
{
    return mPathRunnerList->GetListSize();
}

///======================================= Manipulators ===========================================
void Pathrunner::SetCollisionDepth(int pDepth)
{
    mCollisionDepth = pDepth;
}
void Pathrunner::SetIterationsPerCycle(int pIterations)
{
    mIterationsToRun = pIterations;
    if(mIterationsToRun < 1) mIterationsToRun = 1;
}
void Pathrunner::SetWalkDistance(int pDistance)
{
    mWalkDist = pDistance;
    if(mWalkDist < 1) mWalkDist = 1;
}

///======================================= Core Methods ===========================================
bool Pathrunner::IsMoveShortest(int pX, int pY, int pPulseVal)
{
    ///--[Documentation]
    //--Given a position and a pulse value, returns true if that position is one pulse value lower.
    //  This allows the routine to "walk" down the pulse values to zero.
    if(pX < 0 || pY < 0 || pX >= mXSize || pY >= mYSize) return false;

    //--Pulse values match.
    if(mPathArray[pX][pY] < pPulseVal)
    {
        return true;
    }

    //--Failed.
    return false;
}
bool Pathrunner::IsMoveValid(int pX, int pY, int pPulseVal, int pDirection)
{
    ///--[Documentation]
    //--Returns true if the given move is legal, false if it hits a collision, out of bounds, or locates a shorter pulse.
    //  The X/Y values are in pixels, not tiles.
    int tSpotCheckX = pX;
    int tSpotCheckY = pY;
    if(pDirection == DIR_UP)
    {
        tSpotCheckY -= mWalkDist;
    }
    else if(pDirection == DIR_RIGHT)
    {
        tSpotCheckX += mWalkDist;
    }
    else if(pDirection == DIR_DOWN)
    {
        tSpotCheckY += mWalkDist;
    }
    else if(pDirection == DIR_LEFT)
    {
        tSpotCheckX -= mWalkDist;
    }

    //--Range check.
    if(tSpotCheckX < 0 || tSpotCheckY < 0 || tSpotCheckX >= mXSize || tSpotCheckY >= mYSize) return false;

    //--If the pulse value is lower than the current one, fail.
    if(mPathArray[tSpotCheckX][tSpotCheckY] <= pPulseVal) return false;

    //--Based on the direction, run the appropriate collision checker.
    if(pDirection == DIR_UP)
    {
        for(int i = 0; i < mWalkDist; i ++)
        {
            if(TilemapActor::AbsIsTopClipped(rActiveLevel, pX, pY-i, 0)) return false;
        }
    }
    else if(pDirection == DIR_RIGHT)
    {
        for(int i = 0; i < mWalkDist; i ++)
        {
            if(TilemapActor::AbsIsRgtClipped(rActiveLevel, pX+i, pY, 0)) return false;
        }
    }
    else if(pDirection == DIR_DOWN)
    {
        for(int i = 0; i < mWalkDist; i ++)
        {
            if(TilemapActor::AbsIsBotClipped(rActiveLevel, pX, pY+i, 0)) return false;
        }
    }
    else if(pDirection == DIR_LEFT)
    {
        for(int i = 0; i < mWalkDist; i ++)
        {
            if(TilemapActor::AbsIsLftClipped(rActiveLevel, pX-i, pY, 0)) return false;
        }
    }

    //--All checks passed.
    return true;
}
void Pathrunner::InsertPackageOnList(PathrunnerPack *pPackage, StarLinkedList *pList, float pStartDistance)
{
    ///--[Documentation]
    //--Heuristic optimizer. Places the given package on the linked list by implicitly sorting it relative to its distance to the
    //  destination (which has already been calculated). Thus, pulses that are getting closer to the target get processed first.
    //--This obviously has a problem when a cul-de-sac is hit, but most of the time this so massively speeds up operation
    //  that this heuristic is acceptable.
    //--If the distance value of the package is higher than the original starting distance, we start counting from the end of the
    //  list instead of the start. This speeds up list sorting, as it is assumed that a further away point will likely be less
    //  useful than the closer points from the word go.
    if(!pPackage || !pList) return;

    ///--[Sort from Front]
    if(pPackage->mDistToLine <= pStartDistance)
    {
        //--Iterate.
        int i = 0;
        PathrunnerPack *rCheckPackage = (PathrunnerPack *)pList->PushIterator();
        while(rCheckPackage)
        {
            //--If the new package has a lower distance than the one being checked, insert there and be done with it.
            if(pPackage->mDistToTarget < rCheckPackage->mDistToTarget)
            {
                //--Pop iterator before placing.
                pList->PopIterator();

                //--Add it.
                pList->AddElementInSlot("X", pPackage, &FreeThis, i);
                return;
            }

            //--Next.
            i ++;
            rCheckPackage = (PathrunnerPack *)pList->AutoIterate();
        }

        //--If we got this far, every element on the list has a lower distance. Place this element on the end.
        pList->AddElementAsTail("X", pPackage, &FreeThis);
    }
    ///--[Sort from Back]
    else
    {
        //--Iterate.
        int i = pList->GetListSize()+1;
        PathrunnerPack *rCheckPackage = (PathrunnerPack *)pList->SetToTailAndReturn();
        while(rCheckPackage)
        {
            //--If the new package has a higher distance than the one being checked, insert there and be done with it.
            if(pPackage->mDistToLine >= rCheckPackage->mDistToLine)
            {
                //--Add it.
                pList->AddElementInSlot("X", pPackage, &FreeThis, i);
                return;
            }

            //--Next.
            i --;
            rCheckPackage = (PathrunnerPack *)pList->DecrementAndGetRandomPointerEntry();
        }

        //--If we got this far, every element on the list has a higher distance. Place this element on the front.
        pList->AddElementAsHead("X", pPackage, &FreeThis);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
StarLinkedList *Pathrunner::RunPathingIntoList()
{
    ///--[ ========== Documentation =========== ]
    //--Run the advanced pathing handler and put the results in the provided linked list. Note that the start
    //  and end coordinates here are in pixels, not tiles.
    //--Returns a list of (int *)'s, which represent the path instructions from start to end. If there is an
    //  error, or the path is unreachable, or the algorithm isn't done yet, returns NULL.
    if(mFailure) return NULL;

    //--If not initialized, the user needs to do that first.
    if(!mIsInitialized) return NULL;

    ///--[Path Runner]
    //--Run the pathing algorithm.
    RunIterator(mIterationsToRun, false);

    //--If the algorithm didn't find a path in the given number of pulses, stop for now.
    if(!mHasAnyPath)
    {
        //--If the algorithm exhausted all pulses and didn't find a path, there is no path!
        if(mPathRunnerList->GetListSize() < 1)
        {
            mFailure = true;
            return NULL;
        }

        //--Otherwise, the function will need to be called again.
        return NULL;
    }

    ///--[ ============== Success ============= ]
    ///--[Setup]
    //--Debug.
    DebugPush(true, "Advanced Pathing Has Path: Pulses run: %i\n", mPulseTotal);
    DebugPrint("Shortest distance: %f\n", mShortestDistance);
    DebugPrint("Pulses remaining: %i\n", mPathRunnerList->GetListSize());

    //--At least one solution has been found. First, error check the path array.
    if(mPathArray[mEndX][mEndY] == ADVP_UNUSED)
    {
        mFailure = true;
        DebugPop("Advanced Pathing Completes: Unable to find a path between start and finish.\n");
        return NULL;
    }

    //--Run this algorithm to get a path.
    StarLinkedList *nPathList = ResolvePath();
    if(!nPathList)
    {
        mFailure = true;
        DebugPop("Advanced Pathing Completes: Couldn't build a path with ResolvePath().\n");
        return NULL;
    }

    //--Debug.
    #if defined PATHRUNNER_ADVANCED_PATH_DEBUG
    DebugPrint("Path length after walking is %i.\n", nPathList->GetListSize());
    if(true)
    {
        int *rWalkPtr = (int *)nPathList->PushIterator();
        while(rWalkPtr)
        {
            if(*rWalkPtr == DIR_NORTH) DebugPrint("N");
            if(*rWalkPtr == DIR_SOUTH) DebugPrint("S");
            if(*rWalkPtr == DIR_WEST)  DebugPrint("W");
            if(*rWalkPtr == DIR_EAST)  DebugPrint("E");
            rWalkPtr = (int *)nPathList->AutoIterate();
        }
        DebugPrint("\n");
    }
    #endif

    ///--[Finish Up]
    //--Pass back the list.
    DebugPop("Advanced Pathing Completes: Finished normally.\n");
    return nPathList;
}
void Pathrunner::Initialize(int pStartX, int pStartY, int pEndX, int pEndY)
{
    ///--[Documentation]
    //--Sets up variables and stores everything needed for execution.
    if(mIsInitialized) return;

    //--Retrive the level that this will run in.
    rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel)
    {
        mFailure = true;
        return;
    }

    //--Store positions.
    mStartX = pStartX;
    mStartY = pStartY;
    mEndX = pEndX;
    mEndY = pEndY;

    ///--[Collision Setup]
    //--Debug.
    DebugPush(true, "Advanced Pathing Initializes.\n");

    //--Get collisions.
    CollisionPack *rCollisionPack = rActiveLevel->GetCollisionPack(mCollisionDepth);
    if(!rCollisionPack)
    {
        mFailure = true;
        DebugPop("Advanced Pathing Completes: Error, no collision pack on depth %i.\n", mCollisionDepth);
        return;
    }

    //--Fast-access variables.
    mXSize = rCollisionPack->mCollisionSizeX * TileLayer::cxSizePerTile;
    mYSize = rCollisionPack->mCollisionSizeY * TileLayer::cxSizePerTile;

    //--Size check.
    if(mXSize < 1 || mYSize < 1)
    {
        mFailure = true;
        DebugPop("Advanced Pathing Completes: Error, collision pack has invalid sizes %ix%i.\n", mXSize, mYSize);
        return;
    }

    //--Create a 2-D array holding the path data. Set the values to their defaults.
    mPathArray = (int16_t **)starmemoryalloc(sizeof(int16_t *) * mXSize);
    for(int x = 0; x < mXSize; x ++)
    {
        mPathArray[x] = (int16_t *)starmemoryalloc(sizeof(int16_t) * mYSize);
        for(int y = 0; y < mYSize; y ++)
        {
            mPathArray[x][y] = ADVP_UNUSED;
        }
    }

    ///--[Path Runner Setup]
    //--This linked list stores all the pulses to be checked.
    mPathRunnerList = new StarLinkedList(true);

    //--The first element on the list is the starting point.
    PathrunnerPack *nFirstPack = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
    nFirstPack->mPulseVal = 0;
    nFirstPack->mDistToLine = 0;
    nFirstPack->mDistToTarget = GetPlanarDistance(pStartX, pStartY, pEndX, pEndY);
    nFirstPack->mX = pStartX;
    nFirstPack->mY = pStartY;
    nFirstPack->mPrevDir = DIR_UP;
    mPathRunnerList->AddElementAsTail("X", nFirstPack, &FreeThis);

    ///--[Distances]
    //--Store distances.
    mShortestDistance = nFirstPack->mDistToTarget;
    mOriginalDistance = mShortestDistance;

    ///--[Finish Up]
    //--Specify that initialization is done.
    mPulseTotal = 0;
    mIsInitialized = true;

    //--Debug report.
    DebugPrint("Beginning path runner.\n");
    DebugPrint("Start position:  %ix%i\n", mStartX, mStartY);
    DebugPrint("Target position: %ix%i\n", mEndX, mEndY);
    DebugPrint("Distance: %f\n", mShortestDistance);
    DebugPop("Advanced Pathing Completes: Finished normally.\n");
}
void Pathrunner::RunIterator(int pPulseCap, bool pStopOnReachingTarget)
{
    ///--[Documentation]
    //--Runs the path iterator. The list contains all the unresolved pulses which are trying to place their
    //  values on the collision grid. While the elements are placed in such a manner that the closest ones
    //  get priority, it is very possible that the shortest possible path is not the first one arrived at.
    //--The algorithm runs until the pulse cap is reached, and then stops. Pass -1 to ignore the pulse cap
    //  and run until the algorithm is totally out of pulses.
    if(!mIsInitialized || mFailure) return;

    //--Allowable pulses. Decrements. When it reaches zero, iteration stops. If you pass a negative in, then
    //  the algorithm never reaches zero and won't stop until pulses are exhausted.
    int tAllowablePulses = pPulseCap;

    ///--[Iteration]
    PathrunnerPack *rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
    while(rIteratingPack)
    {
        ///--[Pulse Cap]
        //--If the allowable pulses value hits zero, stop iteration.
        if(tAllowablePulses == 0) break;
        tAllowablePulses --;

        ///--[Set Pulse]
        //--Remove the head pack.
        mPathRunnerList->SetRandomPointerToHead();
        mPathRunnerList->LiberateRandomPointerEntry();

        //--Check the pulse distance to make sure another pulse didn't slip in and place a shorter one here.
        if(mPathArray[rIteratingPack->mX][rIteratingPack->mY] < rIteratingPack->mPulseVal)
        {
            free(rIteratingPack);
            rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
            continue;
        }

        //--Mark the current tile's shortest distance to the start.
        mPulseTotal ++;
        mPathArray[rIteratingPack->mX][rIteratingPack->mY] = rIteratingPack->mPulseVal;

        ///--[Check Success]
        //--If this iterating pack hit the target, shift the target to match. This is done because the target
        //  is not necessarily grid-aligned to the start point. This grid-aligns it.
        //--If flagged, this also stops iteration because we reached the target.
        if(rIteratingPack->mDistToTarget <= mWalkDist)
        {
            //--Indicate at least one solution has been found.
            mHasAnyPath = true;

            //--Realign. Only needs to be done once.
            if(mAllowGridAligning)
            {
                mAllowGridAligning = false;
                mEndX = rIteratingPack->mX;
                mEndY = rIteratingPack->mY;
            }

            //--If flagged, stop iteration here.
            if(pStopOnReachingTarget)
            {
                break;
            }

            //--If we didn't stop, there is no need to spawn successors as we hit the target with this pulse.
            free(rIteratingPack);
            rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
            continue;
        }

        ///--[Spawn Successors]
        //--Check each of the cardinal directions if they can support a path move. This is done by
        //  a subroutine. If true, put the package on the end of the list.

        //--North:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_UP))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX + 0;
            nNewPackage->mY = rIteratingPack->mY - mWalkDist;
            nNewPackage->mPrevDir = DIR_UP;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        //--South:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_DOWN))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX + 0;
            nNewPackage->mY = rIteratingPack->mY + mWalkDist;
            nNewPackage->mPrevDir = DIR_DOWN;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        //--West:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_LEFT))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX - mWalkDist;
            nNewPackage->mY = rIteratingPack->mY + 0;
            nNewPackage->mPrevDir = DIR_LEFT;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        //--East:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_RIGHT))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX + mWalkDist;
            nNewPackage->mY = rIteratingPack->mY + 0;
            nNewPackage->mPrevDir = DIR_RIGHT;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        ///--[Next]
        //--Deallocate the old package.
        free(rIteratingPack);

        //--Get the new head.
        rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
    }
}
StarLinkedList *Pathrunner::ResolvePath()
{
    ///--[ =========== Documentation ========== ]
    //--Runs an algorithm from end to start, resolving the shortest possible path. This can be run at multiple
    //  times during iteration as the pulse information comes in.
    //--Returns a linked-list containing the movement instructions as int *'s. NULL on error. Caller is responsible
    //  for deallocating the list.
    if(mFailure || !mHasAnyPath) return NULL;

    //--Setup.
    StarLinkedList *nPathList = new StarLinkedList(true); //int *, master
    StarLinkedList *tTempMoveList = new StarLinkedList(true); //int *, master
    int tCurX = mEndX;
    int tCurY = mEndY;
    int tEndPathValue = mPathArray[mEndX][mEndY];

    ///--[ ============== Walking ============= ]
    //--Starting at the end position, "Walk" back to the start position. There should be an unbroken
    //  chain of values counting down to zero that denotes the shortest path.
    //--If multiple move cases exist, ties are broken at random. This gives some variation to the paths.
    //--If multiple move cases exist, but some are "longer" than others, the longer ones are trimmed.
    //fprintf(stderr, " End path value is %i\n", tEndPathValue);
    while(tEndPathValue >= 1)
    {
        ///--[Move Checking]
        //--Place all adjacent moves in the tTempMoveList.
        int tLowestMove = ADVP_UNUSED;
        tTempMoveList->ClearList();

        //--North:
        if(IsMoveShortest(tCurX, tCurY-mWalkDist, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX][tCurY-mWalkDist]) tLowestMove = mPathArray[tCurX][tCurY-mWalkDist];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_SOUTH;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
            //fprintf(stderr, " N\n");
        }

        //--South:
        if(IsMoveShortest(tCurX, tCurY+mWalkDist, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX][tCurY+mWalkDist]) tLowestMove = mPathArray[tCurX][tCurY+mWalkDist];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_NORTH;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
            //fprintf(stderr, " S\n");
        }

        //--West:
        if(IsMoveShortest(tCurX-mWalkDist, tCurY, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX-mWalkDist][tCurY]) tLowestMove = mPathArray[tCurX-mWalkDist][tCurY];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_EAST;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
            //fprintf(stderr, " W\n");
        }

        //--East:
        if(IsMoveShortest(tCurX+mWalkDist, tCurY, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX+mWalkDist][tCurY]) tLowestMove = mPathArray[tCurX+mWalkDist][tCurY];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_WEST;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
            //fprintf(stderr, " E\n");
        }

        ///--[Trimming]
        //--Error check. No moves available results in a failure.
        if(tTempMoveList->GetListSize() == 0)
        {
            break;
        }

        //--If there are more than one move available, trim all the ones that aren't the lowest move.
        if(tTempMoveList->GetListSize() > 1)
        {
            //--Iterate.
            int *rMovePtr = (int *)tTempMoveList->SetToHeadAndReturn();
            while(rMovePtr)
            {
                //--Get the value at the target.
                int tTargetVal = 0;
                if(*rMovePtr == DIR_WEST)  tTargetVal = mPathArray[tCurX+mWalkDist][tCurY];
                if(*rMovePtr == DIR_EAST)  tTargetVal = mPathArray[tCurX-mWalkDist][tCurY];
                if(*rMovePtr == DIR_NORTH) tTargetVal = mPathArray[tCurX][tCurY+mWalkDist];
                if(*rMovePtr == DIR_SOUTH) tTargetVal = mPathArray[tCurX][tCurY-mWalkDist];

                //--If the target value is not the lowest value, remove this.
                if(tTargetVal > tLowestMove) tTempMoveList->RemoveRandomPointerEntry();

                //--Next.
                rMovePtr = (int *)tTempMoveList->IncrementAndGetRandomPointerEntry();
            }
        }

        //--If the trimmer somehow removed all the elements, fail.
        if(tTempMoveList->GetListSize() == 0)
        {
            break;
        }

        ///--[Path Resolve]
        //--If there's more than one move, roll which one to use at random.
        int tUseSlot = rand() % tTempMoveList->GetListSize();

        //--Liberate that element.
        int *rMovePtr = (int *)tTempMoveList->GetElementBySlot(tUseSlot);
        tTempMoveList->SetRandomPointerToThis(rMovePtr);
        tTempMoveList->LiberateRandomPointerEntry();

        //--Register it on the return list.
        nPathList->AddElementAsHead("X", rMovePtr, &FreeThis);

        //--Next.
        tEndPathValue --;

        //--Given the direction, move the cursor.
        if(     *rMovePtr == DIR_NORTH) tCurY += mWalkDist;
        else if(*rMovePtr == DIR_SOUTH) tCurY -= mWalkDist;
        else if(*rMovePtr == DIR_WEST)  tCurX += mWalkDist;
        else if(*rMovePtr == DIR_EAST)  tCurX -= mWalkDist;
    }

    //--Clean.
    delete tTempMoveList;
    return nPathList;
}

///========================================= File I/O =============================================
void Pathrunner::WritePathToDisk(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Debug function, renders the path information to disk in the form of an image.
    if(!pPath) return;
    #if defined _ALLEGRO_PROJECT_

    ///--[Allegro Bitmap]
    //--Create a new bitmap for output to the hard drive.
    ALLEGRO_BITMAP *tOutBitmap = al_create_bitmap(mXSize, mYSize);
    al_set_target_bitmap(tOutBitmap);
    al_clear_to_color(al_map_rgba(0, 0, 0, 0));

    ///--[Transfer Pixel Data]
    //--Place the pixel data in.
    for(int y = 0; y < mYSize; y ++)
    {
        for(int x = 0; x < mXSize; x ++)
        {
            int tVal = mPathArray[x][y] * 1;
            al_put_pixel(x, y, al_map_rgba(0, tVal/200, tVal%200, 255));
        }
    }

    ///--[Write Data, Clean]
    //--Write it. The extension determines the format.
    al_save_bitmap(pPath, tOutBitmap);

    //--Clean.
    al_destroy_bitmap(tOutBitmap);

    #endif
}

///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void Pathrunner::HookToLuaState(lua_State *pLuaState)
{
    /* Path_GetPathLength() (1 Integer)
       Returns the length of the path in the static pathrunner object. */
    lua_register(pLuaState, "Path_GetPathLength", &Hook_Path_GetPathLength);

    /* Path_GetPathDirection(iSlot) (1 Integer)
       Returns the direction in the given slot of the static pathrunner object. */
    lua_register(pLuaState, "Path_GetPathDirection", &Hook_Path_GetPathDirection);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Path_GetPathLength(lua_State *L)
{
    //Hook_Path_GetPathLength() (1 Integer)
    lua_pushinteger(L, Pathrunner::xStaticPathList->GetListSize());
    return 1;
}
int Hook_Path_GetPathDirection(lua_State *L)
{
    //Path_GetPathDirection(iSlot) (1 Integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("PathTile_RunPathing");
        lua_pushinteger(L, 0);
        return 1;
    }

    //--Get and check.
    int *rDirection = (int *)Pathrunner::xStaticPathList->GetElementBySlot(lua_tointeger(L, 1));
    if(!rDirection)
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    //--Push.
    lua_pushinteger(L, *rDirection);
    return 1;
}
