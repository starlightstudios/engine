///====================================== File Definitions ========================================
//--Structures and definitions common to various save/load operations.
#pragma once

#include "StarBitmap.h"

///======================================== LoadingPack ===========================================
//--Package that represents a hard-drive savefile used by various Starlight Engine games. This contains
//  information seen on the load-game screen.

///--[Forward Declarations]
class StarLinkedList;

///--[Local Definitions]
#define LOADINGPACK_MAX_PARTY_SIZE 4
#define LOADINGPACK_MAX_CHAPTER 6

///--[Structure]
typedef struct LoadingPack
{
    ///--[Variables]
    //--System
    char *mFileName;
    char *mFilePath;
    char *mFilePathShort;

    //--Party Members
    char *mPartyNames[LOADINGPACK_MAX_PARTY_SIZE];
    int mPartyLevels[LOADINGPACK_MAX_PARTY_SIZE];
    StarBitmap *rRenderImg[LOADINGPACK_MAX_PARTY_SIZE];

    //--Location
    char *mMapLocation;

    //--Timestamp.
    char *mTimestamp;

    //--Mod Activity
    StarLinkedList *mActiveMods; //Dummy Pointer

    //--Chapter Completion
    bool mIsChapterComplete[LOADINGPACK_MAX_CHAPTER];

    ///--[Functions]
    void Initialize();
    void Deallocate();
    static void DeleteThis(void *pPtr);
}LoadingPack;
