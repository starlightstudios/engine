//--Base
#include "AdvUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///======================================== Activation ============================================
void AdvUIVendor::ActivateModeSell()
{
    ///--[Documentation]
    //--Switches to Sell mode.

    ///--[Variables]
    //--Common subroutines.
    ActivateMode(mIsSellMode);
    ResetCursor();

    ///--[Other]
    //--Rebuild inventory in case it has changed due to other actions.
    AdventureInventory::Fetch()->SortItemListByInternalCriteria();

    //--Recheck comparison character.
    RecheckComparisonCharacter(GetConsideredItem());
}

///===================================== Property Queries =========================================
float AdvUIVendor::GetSellCursorNameWidth()
{
    ///--[Documentation]
    //--In sell mode, resolves the highlighted item and returns its width. Used for cursor size computations.
    float tNameWid = 100.0f;

    //--Get name.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);
    if(rItem)
    {
        tNameWid = AdvImages.rFont_Mainline->GetTextWidth(rItem->GetName());
    }

    //--Name length clamp.
    if(tNameWid > cxAdvMaxNameWid) tNameWid = cxAdvMaxNameWid;

    //--Finish.
    return tNameWid;
}

///========================================== Update ==============================================
void AdvUIVendor::UpdateModeSell()
{
    ///--[Documentation]
    //--Update handler for Sell mode.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Fast-access pointers.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();

    ///--[Timers]
    //--Run standard timers.
    StandardTimer(mSellTimer,       0, cxAdvSellTicks,     mIsSellingItem);

    //--Update blockers. If these timers are not capped, stop the update.
    if(mSellTimer    > 0 && mSellTimer    < cxAdvSellTicks)    return;
    if(mDetailsTimer > 0 && mDetailsTimer < cxAdvDetailsTicks) return;

    ///--[Mode Handlers]
    //--Confirming sale.
    if(mIsSellingItem) { UpdateSellItem(); return; }

    //--If in Details mode, pressing cancel exits details mode. All other controls are ignored.
    if(CommonDetailsUpdate()) return;

    ///--[Shoulder Buttons]
    //--Shoulder-left. Decrements the comparison character. Does nothing if there is only one comparison character.
    if(rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel") && mShowSwapArrows)
    {
        ScrollComparisonCharacter(-1);
        return;
    }
    //--Shoulder-right. Increments the comparison character. Does nothing if there is only one comparison character.
    if(rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel") && mShowSwapArrows)
    {
        ScrollComparisonCharacter(1);
        return;
    }

    ///--[Run Button]
    //--Toggles which comparison slot is in use.
    if(rControlManager->IsFirstPress("Run") && mCanSwapCompareSlot)
    {
        ToggleComparisonSlot();
        return;
    }

    ///--[Left and Right, Change Mode]
    //--Changes modes to the left, which is Buy.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && !mIsShowingDetails)
    {
        ScrollMode(-1);
        return;
    }
    //--Changes modes to the right, which is Buyback.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && !mIsShowingDetails)
    {
        ScrollMode(1);
        return;
    }

    ///--[Up and Down]
    //--Up decrements, down increments. Wrapping is enabled.
    if(AutoListUpDn(mCursor, mSkip, rItemList->GetListSize(), cScrollBuf, cScrollPage, true))
    {
        //--Modify cursor.
        RecomputeCursorPositions();

        //--The comparison character may have changed. Recheck that here.
        RecheckComparisonCharacter(GetConsideredItem());
        return;
    }

    ///--[Activate]
    //--Begins selling the item.
    if(rControlManager->IsFirstPress("Activate") && !mIsShowingDetails)
    {
        //--Check the item.
        AdventureItem *rSellingItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);

        //--If the item is a key-item, don't allow them to sell it.
        if(rSellingItem && !rSellingItem->IsKeyItem())
        {
            mIsSellingItem = true;
            mSellQuantity = 1;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Otherwise, play a sound effect to indicate failure.
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel") && !mIsShowingDetails)
    {
        HandleExit();
        return;
    }
}
void AdvUIVendor::UpdateSellItem()
{
    ///--[Documentation]
    //--Update handler for the player selling an item.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetItemList();
    AdventureItem *rSoldItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);

    ///--[Left and Right]
    //--Adjust quantity down. Wraps to maximum.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        AdjustQuantity(mSellQuantity, -1);
        return;
    }

    //--Adjust quantity up. Wraps to minimum.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        AdjustQuantity(mSellQuantity, 1);
        return;
    }

    ///--[Activate]
    //--Accepts the sale.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Flag.
        mIsSellingItem = false;

        //--Get the buyback list.
        StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();

        //--Buyback value.
        int tBuybackValue = rSoldItem->GetValue() / 2;

        //--Selling the entire stack:
        int tStackSize = rSoldItem->GetStackSize();
        if(mSellQuantity >= tStackSize || !rSoldItem->IsStackable())
        {
            //--Remove the item from the player's inventory.
            rInventory->LiberateItemP(rSoldItem);

            //--Move the item to a buyback list for the amount it was sold for.
            SetMemoryData(__FILE__, __LINE__);
            ShopInventoryPack *nPackage = (ShopInventoryPack *)starmemoryalloc(sizeof(ShopInventoryPack));
            nPackage->Initialize();
            nPackage->mItem = rSoldItem;
            nPackage->mPriceOverride = tBuybackValue;
            nPackage->mQuantity = mSellQuantity;
            rBuybackList->AddElementAsHead("X", nPackage, &ShopInventoryPack::DeleteThis);
        }
        //--Selling less than a full stack.
        else
        {
            //--Decrement stack size.
            rSoldItem->SetStackSize(tStackSize - mSellQuantity);

            //--Create a clone and put it on the buyback list.
            AdventureItem *nClonedItem = rSoldItem->Clone();

            //--Create a package to go around it.
            SetMemoryData(__FILE__, __LINE__);
            ShopInventoryPack *nPackage = (ShopInventoryPack *)starmemoryalloc(sizeof(ShopInventoryPack));
            nPackage->Initialize();
            nPackage->mItem = nClonedItem;
            nPackage->mPriceOverride = tBuybackValue;
            nPackage->mQuantity = mSellQuantity;
            rBuybackList->AddElementAsHead("X", nPackage, &ShopInventoryPack::DeleteThis);
        }

        //--When selling, value is halved.
        int tValue = rSoldItem->GetValue() * mSellQuantity / 2;

        //--Increment the player's cash.
        rInventory->SetPlatina(rInventory->GetPlatina() + tValue);

        //--If there are more than AINV_MAX_BUYBACK elements on the buyback list, clean off the end.
        while(rBuybackList->GetListSize() >= AINV_MAX_BUYBACK)
        {
            rBuybackList->DeleteTail();
        }

        //--If the cursor was at the end of the list, decrement.
        if(mCursor >= rItemList->GetListSize()) mCursor --;
        if(mCursor < 0) mCursor = 0;

        //--Recompute cursor properties.
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();

        //--If there is at least one entry on the list, recheck comparison character.
        AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);
        RecheckComparisonCharacter(rItem);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
        return;
    }

    ///--[Cancel]
    //--Cancels the sale.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsSellingItem = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}

///===================================== Segment Rendering ========================================
void AdvUIVendor::RenderLftSell(float pVisAlpha)
{
    RenderCommonLft(pVisAlpha);
}
void AdvUIVendor::RenderTopSell(float pVisAlpha)
{
    RenderCommonTop(pVisAlpha);
}
void AdvUIVendor::RenderRgtSell(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    //--Comparison Block
    AdvImages.rFrame_Comparison->Draw();
    RenderSellProperties(cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderBotSell(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Setup.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();

    ///--[Render]
    //--Inventory Listing.
    AdvImages.rFrame_Vendor->Draw();

    //--Tabs and titles.
    RenderModeTabs(cColorAlpha);
    RenderModeTitles(cColorAlpha);

    //--If there are no entries on the item list, render this instead.
    if(rItemList->GetListSize() < 1)
    {
        AdvImages.rFont_Mainline->DrawTextFixed(267.0f, 189.0f, cxAdvMaxNameWid, 0, "You have nothing to sell.");
    }

    //--Render entries.
    RenderInventoryItemListing(mSkip, cScrollPage, cColorAlpha);

    //--Scrollbar.
    if(rItemList->GetListSize() > cScrollPage)
    {
        StandardRenderScrollbar(mSkip, cScrollPage, rItemList->GetListSize(), 219.0f, 447.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    //--Detail. Goes over the entries since the columns overwrite the black backing.
    AdvImages.rOverlay_DetailSell->Draw();

    //--Highlight.
    if(rItemList->GetListSize() >= 1) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Help Strings.
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only renders if character swapping is possible.
    if(mShowSwapArrows)
        mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only renders if multiple compare slots are present.
    if(mCanSwapCompareSlot)
        mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderUnalignedSell(float pVisAlpha)
{
    ///--[Details]
    //--Standard details renderer.
    AdventureItem *rDetailsItem = GetConsideredItem();
    RenderItemDetails(mDetailsTimer, cxAdvDetailsTicks, rDetailsItem);

    //--Other modes.
    RenderSellItem(pVisAlpha);
}

///===================================== Common Rendering =========================================
void AdvUIVendor::RenderSellProperties(float pVisAlpha)
{
    ///--[Documentation]
    //--Handles rendering the properties of the item currently highlighted, as well as comparison to the
    //  comparison character if applicable.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    AdventureItem *rComparisonItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);

    //--Pass this off to a subroutine.
    RenderComparisonPane(rComparisonItem, pVisAlpha);
}
void AdvUIVendor::RenderSellItem(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--When the player chooses an item to sell, this UI appears over the rest of the UI. It shows
    //  what they are selling and for how much.
    if(mSellTimer < 1) return;

    ///--[Position]
    //--Percentage.
    float cSalePct = EasingFunction::QuadraticInOut(mSellTimer, cxAdvSellTicks);

    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha * cSalePct);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cSalePct));

    ///--[Item Resolve]
    //--Get the item package.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);

    ///--[Rendering]
    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--All information specific to the item is not shown when hiding the UI.
    if(rItem && mIsSellingItem)
    {
        //--Heading.
        mColorHeading.SetAsMixerAlpha(cColorAlpha);
        AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Sale");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

        //--Centering computations.
        int tTotalItemValue = rItem->GetValue() * mSellQuantity / 2;
        char *tTextBuf = InitializeString("%s x %i for %i Platina", rItem->GetDisplayName(), mSellQuantity, tTotalItemValue);
        float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tTextBuf);
        float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

        //--Item Icon
        StarBitmap *rItemIcon = rItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 205.0f);

        //--Item name.
        AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, tTextBuf);
        free(tTextBuf);
    }

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(579.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
