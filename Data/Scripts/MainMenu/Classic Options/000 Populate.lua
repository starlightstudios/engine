-- |[ ==================================== Game Options Menu =================================== ]|
--Options concerning Classic/Classic3D/Corrupter game modes.
local sBasePath = fnResolvePath()

-- |[Setup]|
--Local vars.
local iAscendingPriority = 0

--Length.
local sLongestString = "Show Damage Animations"

-- |[Header]|
FlexMenu_SetHeaderSize(2)
FlexMenu_SetHeaderLine(0, "Game Options")
FlexMenu_SetHeaderLine(1, "Affects Classic and Corrupter Mode", 0.6)

-- |[ ====================================== Menu Options ====================================== ]|
-- |[Actors Animate in 2D Mode]|
--Flag for whether or not Actors will dynamically move on the map in 2D mode. Slightly slows down gameplay, but not by much.
FlexMenu_RegisterButton("Actors Animate Move")

	--Description.
    FlexButton_SetProperty("Description", "Toggles actor icons moving across the game map dynamically in 2D modes. If set to false, they will instantly appear at their destination.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bActorsAnimateMove = OM_GetOption("ActorsAnimateMove")
	if(bActorsAnimateMove) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Damage Animations]|
FlexMenu_RegisterButton("Show Damage Animations")

	--Description.
    FlexButton_SetProperty("Description", "Toggles damage text and sound effects when an Actor takes damage or avoids an attack.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bShowDamageAnimations = OM_GetOption("ShowDamageAnimations")
	if(bShowDamageAnimations) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Damage Overlays]|
FlexMenu_RegisterButton("Show 3D Damage Overlays")

	--Description.
    FlexButton_SetProperty("Description", "In 3D mode, when you are damaged a red/purple overlay will appear over the UI edges.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bShowDamageOverlay = OM_GetOption("AllowDamageOverlay")
	if(bShowDamageOverlay) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Map Underlays]|
FlexMenu_RegisterButton("Show 2D Underlays")

	--Description.
    FlexButton_SetProperty("Description", "Shows a colored map under the game world.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bShowUnderlays = OM_GetOption("Show2DUnderlays")
	if(bShowUnderlays) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[World Fog of War]|
--Shows a highlight around the player and the rooms they can see.
FlexMenu_RegisterButton("No Fog of War Overlay")

	--Description.
    FlexButton_SetProperty("Description", "If set to true, disables the lighting overlay that highlights what rooms the player can see.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bGeneralFogOfWar = OM_GetOption("DisableDynamicFog")
	if(bGeneralFogOfWar) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Radar]|
--Shows indicators for the positions of entities. If false, only the player is visible.
FlexMenu_RegisterButton("No Fog of War for Actors")

	--Description.
    FlexButton_SetProperty("Description", "If set to true, Actors will not disappear if they are not in a room adjacent to the player.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bShowIndicators = OM_GetOption("Always Show Radar Indicators")
	if(bShowIndicators) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Item Fog of War]|
--Shows indicators for the positions of items/weapons/containers and static entities.
FlexMenu_RegisterButton("No Fog of War for Items")

	--Description.
    FlexButton_SetProperty("Description", "If set to true, Items and Containers will not disappear if they are not in a room adjacent to the player.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bItemFogOfWar = OM_GetOption("No Fog of War for Items")
	if(bItemFogOfWar) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[HP Rendering]|
FlexMenu_RegisterButton("Show HPs")

	--Description.
    FlexButton_SetProperty("Description", "If set to true, the Entities Pane will show the HP of nearby Actors.")
	
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bShowHPs = OM_GetOption("Show Entity HP")
	if(bShowHPs) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[WP Rendering]|
FlexMenu_RegisterButton("Show WPs")

	--Description.
    FlexButton_SetProperty("Description", "If set to true, the Entities Pane will show the WP of nearby Actors.")
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bShowWPs = OM_GetOption("Show Entity WP")
	if(bShowWPs) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Weapon Rendering]|
FlexMenu_RegisterButton("Show Weapon Names")

	--Description.
    FlexButton_SetProperty("Description", "If set to true, the Entities Pane will show which weapon, if any, is currently held by nearby Actors.")
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bShowWeapons = OM_GetOption("Show Entity Weapon")
	if(bShowWeapons) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Icon Rendering]|
FlexMenu_RegisterButton("Show Icons")

	--Description.
    FlexButton_SetProperty("Description", "If set to true, Actors will render on the map using icons instead of circles. These icons are based on whether they are a monster, human, or other type of entity. Items and Containers will also appear above the room icon.")
	
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bShowIcons = OM_GetOption("Show Entity Icons")
	if(bShowIcons) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Zoom Factor]|
local iMapZoom = OM_GetOption("Map Zoom")
FlexMenu_RegisterButton("Map Zoom")

	--Description.
    FlexButton_SetProperty("Description", "Sets how zoomed in the map is. Higher numbers are more zoomed in.")
	
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Slider Mode", 25, 175)
	FlexButton_SetProperty("Current Slider", iMapZoom)
	FlexButton_SetProperty("Slider IncVals", 1, 10)
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[ ======================================= Save / Exit ====================================== ]|
-- |[Save]|
--Save changes and go back to the main menu.
FlexMenu_RegisterButton("Save changes")

	--Description.
	FlexButton_SetProperty("Description", "Save changes and return to the main menu.")

	StarButton_SetProperty("Priority", 10000)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Exit And Save.lua", 0)
DL_PopActiveObject()

-- |[Back]|
--Go back to the main menu.
FlexMenu_RegisterButton("Exit without saving")

	--Description.
	FlexButton_SetProperty("Description", "Return to the main menu without saving changes.")

	StarButton_SetProperty("Priority", 10001)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "101 Exit No Save.lua", 0)
DL_PopActiveObject()
