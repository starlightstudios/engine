//--Base
#include "RunningMinigame.h"

//--Classes
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///--[Local Definitions]
#define OPACITY_TICKS 30

#define HEART_SPACING_X 32

#define SLASH_TICKS 8
#define SLASH_COOLDOWN 8

#define DEFAULT_PLAYER_SPEED 4.0f

///========================================== System ==============================================
RunningMinigame::RunningMinigame()
{
    ///--[RunningMinigame]
    //--System
    mIsActive = false;
    mHoldingPattern = false;
    mOpacityTimer = 0;
    mFadeinTimer = 0;
    mBypassTutorial = false;

    //--Introduction
    mIntroductionState = RMG_INTRO_PLAYER_RUNS_IN;
    mIntroTimer = 0;

    //--Player State
    mPlayerStopTimer = 0;
    mPlayerInvincibleTimer = 0;
    mPlayerHealth = 5;
    mPlayerSpeed = DEFAULT_PLAYER_SPEED;
    mSlashTimer = 0;
    mSlashCooldown = 0;
    for(int i = 0; i < RMG_COMPANION_TICKS; i ++)
    {
        mPlayerCurrentDirection[i] = RMG_PLAYER_RUN_W;
        mRunTimer[i] = 0;
        mJumpTimer[i] = 0;
        mPlayerYPos[i] = RMG_TILE_SIZE * (RMG_ACTIVE_WORLD_WIDTH * 0.5f);
        mPlayerZPos[i] = 0.0f;
        mPlayerZSpeed[i] = 0.0f;
    }

    //--Checkpoint
    mCheckpoint = 0;

    //--Enemies
    mEnemyList = new StarLinkedList(true);

    //--Defeat
    mDefeatFadeTimer = 0;

    //--Hearts
    for(int i = 0; i < RMG_HEARTS_MAX; i ++)
    {
        mHeartX[i] = (HEART_SPACING_X * i) + (HEART_SPACING_X * 0.50f);
        mHeartY[i] = (HEART_SPACING_X * 0.50f);
        mHeartR[i] = 0;
        mHeartXSpeed[i] = 0.0f;
        mHeartYSpeed[i] = 0.0f;
        mHeartRSpeed[i] = 0.0f;
    }

    //--Player/Companion Images
    memset(rPlayerRunImg,    0, sizeof(StarBitmap *) * RMG_PLAYER_RUN_DIR_TOTAL * RMG_PLAYER_RUN_TOTAL);
    memset(rCompanionRunImg, 0, sizeof(StarBitmap *) * RMG_PLAYER_RUN_DIR_TOTAL * RMG_PLAYER_RUN_TOTAL);
    rShadowImg = NULL;

    //--World Layout (Active)
    mCurrentWorldTile = 0;
    mTotalWorldTiles = 0;
    mWorldXOffset = 0.0f;
    memset(mWorldWallHi, 0, sizeof(int) * RMG_ACTIVE_WORLD_LEN);
    memset(mWorldWallLo, 0, sizeof(int) * RMG_ACTIVE_WORLD_LEN);
    memset(mWorldFloor,  0, sizeof(int) * RMG_ACTIVE_WORLD_WIDTH * RMG_ACTIVE_WORLD_LEN);
    memset(mWorldTile,   0, sizeof(int) * RMG_ACTIVE_WORLD_WIDTH * RMG_ACTIVE_WORLD_LEN);

    //--World Layout (Passive)
    mObstacleList = new StarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));
}
RunningMinigame::~RunningMinigame()
{
    delete mEnemyList;
    delete mObstacleList;
    for(int i = 0; i < RMG_GROUND_TILES; i ++) delete Images.Data.mGroundTile[i];
    for(int i = 0; i < RMG_HIWALL_TILES; i ++) delete Images.Data.mWallHi[i];
    for(int i = 0; i < RMG_LOWALL_TILES; i ++) delete Images.Data.mWallLo[i];
    delete Images.Data.mPitWall;
    delete Images.Data.mPitMarkerTile;
    delete Images.Data.mLowObstacle;
    for(int i = 0; i < RMG_ENEMIES; i ++)
    {
        delete Images.Data.mEnemiesUp[i];
        delete Images.Data.mEnemiesDn[i];
    }
}
StarBitmap *MakeSubBitmapOf(uint32_t pHandle, int pSizeX, int pSizeY, float pAtlasLft, float pAtlasTop, float pAtlasRgt, float pAtlasBot)
{
    //--Worker Function. Creates a new StarBitmap as a sub-bitmap of another existing bitmap and gives it the provided coordinates.
    StarBitmap *nBitmap = new StarBitmap();
    nBitmap->SetSizes(pSizeX, pSizeY);
    nBitmap->AssignHandle(pHandle, false, true);
    nBitmap->SetAtlasCoordinates(pAtlasLft, pAtlasTop, pAtlasRgt, pAtlasBot);
    nBitmap->FlipAtlasCoordinates();
    return nBitmap;
}

void RunningMinigame::Construct()
{
    //--Load all the images and then check them. In this case, the images are locally held atlases.
    if(Images.mIsReady) return;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Get the base image.
    Images.Data.rBase = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/RunningMinigame/All");
    if(!Images.Data.rBase) return;

    //--Compute Sizes.
    uint32_t tGlobalHandle = Images.Data.rBase->GetGLTextureHandle();
    float cTileSizeX = 16.0f / (float)Images.Data.rBase->GetTrueWidth();
    float cTileSizeY = 16.0f / (float)Images.Data.rBase->GetTrueHeight();
    float cHfPxX = 0.5f / (float)Images.Data.rBase->GetTrueWidth();
    float cHfPxY = 0.5f / (float)Images.Data.rBase->GetTrueHeight();

    //--UI.
    Images.Data.mHeart       = MakeSubBitmapOf(tGlobalHandle, 16, 16, cTileSizeX * 3.0f + cHfPxX, cTileSizeY * 0.0f, cTileSizeX * 4.0f - cHfPxX, cTileSizeY * 1.0f);
    Images.Data.mDistanceL   = MakeSubBitmapOf(tGlobalHandle, 16, 16, cTileSizeX * 4.0f, cTileSizeY * 0.0f, cTileSizeX * 5.0f, cTileSizeY * 1.0f);
    Images.Data.mDistanceM   = MakeSubBitmapOf(tGlobalHandle, 16, 16, cTileSizeX * 5.0f, cTileSizeY * 0.0f, cTileSizeX * 6.0f, cTileSizeY * 1.0f);
    Images.Data.mDistanceR   = MakeSubBitmapOf(tGlobalHandle, 16, 16, cTileSizeX * 6.0f, cTileSizeY * 0.0f, cTileSizeX * 7.0f, cTileSizeY * 1.0f);
    Images.Data.mCheckpointL = MakeSubBitmapOf(tGlobalHandle, 16, 16, cTileSizeX * 2.0f, cTileSizeY * 1.0f, cTileSizeX * 3.0f, cTileSizeY * 2.0f);
    Images.Data.mCheckpointM = MakeSubBitmapOf(tGlobalHandle, 16, 16, cTileSizeX * 3.0f, cTileSizeY * 1.0f, cTileSizeX * 4.0f, cTileSizeY * 2.0f);
    Images.Data.mCheckpointR = MakeSubBitmapOf(tGlobalHandle, 16, 16, cTileSizeX * 4.0f, cTileSizeY * 1.0f, cTileSizeX * 5.0f, cTileSizeY * 2.0f);

    //--Ground Tiles.
    for(int i = 0; i < RMG_GROUND_TILES; i ++)
    {
        Images.Data.mGroundTile[i] = new StarBitmap();
        Images.Data.mGroundTile[i]->SetSizes(16, 16);
        Images.Data.mGroundTile[i]->AssignHandle(tGlobalHandle, false, true);
        Images.Data.mGroundTile[i]->SetAtlasCoordinates(cTileSizeX * (i+0), cTileSizeY * 0.0f, cTileSizeX * (i+1), cTileSizeY * 1.0f);
        Images.Data.mGroundTile[i]->FlipAtlasCoordinates();
        Images.Data.mGroundTile[i]->MoveAtlasInBy(cHfPxX, cHfPxY);
    }
    for(int i = 0; i < RMG_HIWALL_TILES; i ++)
    {
        Images.Data.mWallHi[i] = new StarBitmap();
        Images.Data.mWallHi[i]->SetSizes(16, 16);
        Images.Data.mWallHi[i]->AssignHandle(tGlobalHandle, false, true);
        Images.Data.mWallHi[i]->SetAtlasCoordinates(cTileSizeX * (i+0), cTileSizeY * 1.0f, cTileSizeX * (i+1), cTileSizeY * 2.0f);
        Images.Data.mWallHi[i]->FlipAtlasCoordinates();
        Images.Data.mWallHi[i]->MoveAtlasInBy(cHfPxX, cHfPxY);
    }
    for(int i = 0; i < RMG_LOWALL_TILES; i ++)
    {
        Images.Data.mWallLo[i] = new StarBitmap();
        Images.Data.mWallLo[i]->SetSizes(16, 16);
        Images.Data.mWallLo[i]->AssignHandle(tGlobalHandle, false, true);
        Images.Data.mWallLo[i]->SetAtlasCoordinates(cTileSizeX * (i+0), cTileSizeY * 2.0f, cTileSizeX * (i+1), cTileSizeY * 3.0f);
        Images.Data.mWallLo[i]->FlipAtlasCoordinates();
        Images.Data.mWallLo[i]->MoveAtlasInBy(cHfPxX, cHfPxY);
    }
    Images.Data.mPitWall = new StarBitmap();
    Images.Data.mPitWall->SetSizes(16, 32);
    Images.Data.mPitWall->AssignHandle(tGlobalHandle, false, true);
    Images.Data.mPitWall->SetAtlasCoordinates(cTileSizeX * (0), cTileSizeY * 3.0f, cTileSizeX * (1), cTileSizeY * 5.0f);
    Images.Data.mPitWall->FlipAtlasCoordinates();
    Images.Data.mPitWall->MoveAtlasInBy(cHfPxX, cHfPxY);

    Images.Data.mPitMarkerTile = new StarBitmap();
    Images.Data.mPitMarkerTile->SetSizes(16, 16);
    Images.Data.mPitMarkerTile->AssignHandle(tGlobalHandle, false, true);
    Images.Data.mPitMarkerTile->SetAtlasCoordinates(cTileSizeX * (1), cTileSizeY * 3.0f, cTileSizeX * (2), cTileSizeY * 4.0f);
    Images.Data.mPitMarkerTile->FlipAtlasCoordinates();
    Images.Data.mPitMarkerTile->MoveAtlasInBy(cHfPxX, cHfPxY);

    Images.Data.mLowObstacle = new StarBitmap();
    Images.Data.mLowObstacle->SetSizes(16, 16);
    Images.Data.mLowObstacle->AssignHandle(tGlobalHandle, false, true);
    Images.Data.mLowObstacle->SetAtlasCoordinates(cTileSizeX * (1), cTileSizeY * 4.0f, cTileSizeX * (2), cTileSizeY * 5.0f);
    Images.Data.mLowObstacle->FlipAtlasCoordinates();
    Images.Data.mLowObstacle->MoveAtlasInBy(cHfPxX, cHfPxY);

    for(int i = 0; i < RMG_ENEMIES; i ++)
    {
        Images.Data.mEnemiesUp[i] = new StarBitmap();
        Images.Data.mEnemiesUp[i]->SetSizes(16, 32);
        Images.Data.mEnemiesUp[i]->AssignHandle(tGlobalHandle, false, true);
        Images.Data.mEnemiesUp[i]->SetAtlasCoordinates(cTileSizeX * (i+0), cTileSizeY * 5.0f, cTileSizeX * (i+1), cTileSizeY * 7.0f);
        Images.Data.mEnemiesUp[i]->FlipAtlasCoordinates();
        Images.Data.mEnemiesUp[i]->MoveAtlasInBy(cHfPxX, cHfPxY);

        Images.Data.mEnemiesDn[i] = new StarBitmap();
        Images.Data.mEnemiesDn[i]->SetSizes(16, 32);
        Images.Data.mEnemiesDn[i]->AssignHandle(tGlobalHandle, false, true);
        Images.Data.mEnemiesDn[i]->SetAtlasCoordinates(cTileSizeX * (i+0), cTileSizeY * 7.0f, cTileSizeX * (i+1), cTileSizeY * 9.0f);
        Images.Data.mEnemiesDn[i]->FlipAtlasCoordinates();
        Images.Data.mEnemiesDn[i]->MoveAtlasInBy(cHfPxX, cHfPxY);
    }

    //--Slashes
    Images.Data.mSlashU = new StarBitmap();
    Images.Data.mSlashU->SetSizes(32, 40);
    Images.Data.mSlashU->AssignHandle(tGlobalHandle, false, true);
    Images.Data.mSlashU->SetAtlasCoordinates(cTileSizeX * 3.0f, cTileSizeY * 3.0f, cTileSizeX * 5.0f, cTileSizeY * 6.0f);
    Images.Data.mSlashU->FlipAtlasCoordinates();
    Images.Data.mSlashU->MoveAtlasInBy(cHfPxX, cHfPxY);
    Images.Data.mSlashD = new StarBitmap();
    Images.Data.mSlashD->SetSizes(32, 40);
    Images.Data.mSlashD->AssignHandle(tGlobalHandle, false, true);
    Images.Data.mSlashD->SetAtlasCoordinates(cTileSizeX * 5.0f, cTileSizeY * 3.0f, cTileSizeX * 7.0f, cTileSizeY * 6.0f);
    Images.Data.mSlashD->FlipAtlasCoordinates();
    Images.Data.mSlashD->MoveAtlasInBy(cHfPxX, cHfPxY);

    //--Tutorial
    Images.Data.rTutorial0 = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/RunningMinigame/Tutorial0");
    Images.Data.rTutorial1 = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/RunningMinigame/Tutorial1");
    Images.Data.rTutorial2 = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/RunningMinigame/Tutorial2");
    Images.Data.rTutorial3 = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/RunningMinigame/Tutorial3");
    Images.Data.rTutorial4 = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/RunningMinigame/Tutorial4");
    Images.Data.rTutorial5 = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/RunningMinigame/Tutorial5");

    //--Ready.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
}
void RunningMinigame::AssemblePlayerImages()
{
    //--Attempts to auto-assemble the player's movement images based on the currently active TilemapActor.
    AdventureLevel *rAdventureLevel = AdventureLevel::Fetch();
    if(!rAdventureLevel) return;

    //--Get the actor.
    TilemapActor *rActiveActor = rAdventureLevel->LocatePlayerActor();
    if(!rActiveActor) return;

    //--Get and set player images.
    for(int i = 0; i < RMG_PLAYER_RUN_TOTAL; i ++)
    {
        rPlayerRunImg[RMG_PLAYER_RUN_W ][i] = rActiveActor->GetRunImage(TA_DIR_WEST, i);
        rPlayerRunImg[RMG_PLAYER_RUN_NW][i] = rActiveActor->GetRunImage(TA_DIR_NW, i);
        rPlayerRunImg[RMG_PLAYER_RUN_SW][i] = rActiveActor->GetRunImage(TA_DIR_SW, i);
    }
    rShadowImg = rActiveActor->GetShadowImage();

    //--Get and set companion images. May not exist! Shares the player's shadow.
    TilemapActor *rFollowingActor = rAdventureLevel->LocateFollowingActor(0);
    if(!rFollowingActor) return;
    for(int i = 0; i < RMG_PLAYER_RUN_TOTAL; i ++)
    {
        rCompanionRunImg[RMG_PLAYER_RUN_W ][i] = rFollowingActor->GetRunImage(TA_DIR_WEST, i);
        rCompanionRunImg[RMG_PLAYER_RUN_NW][i] = rFollowingActor->GetRunImage(TA_DIR_NW, i);
        rCompanionRunImg[RMG_PLAYER_RUN_SW][i] = rFollowingActor->GetRunImage(TA_DIR_SW, i);
    }
}

///===================================== Property Queries =========================================
bool RunningMinigame::IsGameFinished()
{
    return !mIsActive;
}
bool RunningMinigame::IsVisible()
{
    return (mOpacityTimer > 0);
}
bool RunningMinigame::IsFullyOpaque()
{
    return (mOpacityTimer >= OPACITY_TICKS);
}

///======================================= Manipulators ===========================================
void RunningMinigame::Activate()
{
    mIsActive = true;
}
void RunningMinigame::Deactivate()
{
    mIsActive = false;
}
void RunningMinigame::SetHoldingPattern(bool pFlag)
{
    mHoldingPattern = pFlag;
}
void RunningMinigame::TakeHit()
{
    //--SFX.
    AudioManager::Fetch()->PlaySound("Combat|Impact_Ice");

    //--Invincibility for 2 seconds.
    mPlayerInvincibleTimer = 120;

    //--Activate the heart flight!
    mPlayerHealth --;
    if(mPlayerHealth >= 0 && mPlayerHealth < RMG_HEARTS_MAX)
    {
        mHeartXSpeed[mPlayerHealth] = (rand() %  5) + 3;
        mHeartYSpeed[mPlayerHealth] = (rand() %  5) - 8;
        mHeartRSpeed[mPlayerHealth] = (rand() % 10) - 3;
    }
}

///======================================= Core Methods ===========================================
void RunningMinigame::SetPlayerImg(int pDir, int pSlot, StarBitmap *pImg)
{
    if(pDir  < 0 || pDir  >= RMG_PLAYER_RUN_DIR_TOTAL) return;
    if(pSlot < 0 || pSlot >= RMG_PLAYER_RUN_TOTAL)     return;
    rPlayerRunImg[pDir][pSlot] = pImg;
}
void RunningMinigame::SetShadowImg(StarBitmap *pImg)
{
    rShadowImg = pImg;
}
void RunningMinigame::GenerateLevel()
{
    //--Generates a random level for the player to run through! This is done by storing obstacles
    //  at various points in the level.
    //--First, determine a length. This is how many tiles the player needs to run before they
    //  finish the level.
    mTotalWorldTiles = (rand() % 300) + 1000;

    //--Clear this list if there was anything on it.
    mObstacleList->ClearList();

    //--Roll Constants. Sum isn't 100!
    int cPitRoll = 20;
    int cBarrierRoll = cPitRoll + 20;
    int cEnemyRoll = cBarrierRoll + 20;

    //--Begin creating obstacles. The "Difficulty" goes up every time a tile is advanced. When the
    //  difficulty passes a certain threshold, multiple obstacles can be created in the same X position.
    //--We can't place obstacles in the first 40 tiles.
    int tTilePosition = 20;

    //--If checkpointing, start later in the level.
    if(mCheckpoint == 2)
    {
        tTilePosition = (int)(mTotalWorldTiles * 0.66f);
        mCurrentWorldTile = tTilePosition - RMG_ACTIVE_WORLD_WIDTH - 1;
    }
    else if(mCheckpoint == 1)
    {
        tTilePosition = (int)(mTotalWorldTiles * 0.33f);
        mCurrentWorldTile = tTilePosition - RMG_ACTIVE_WORLD_WIDTH - 1;
    }

    //--Run the loop.
    while(true)
    {
        //--Roll to place an obstacle. There is a chance to skip ahead a few tiles and roll again.
        int tRoll = rand() % 100;

        //--Place a pit.
        if(tRoll < cPitRoll)
        {
            //--Basics.
            SetMemoryData(__FILE__, __LINE__);
            RMGObstaclePack *nPack = (RMGObstaclePack *)starmemoryalloc(sizeof(RMGObstaclePack));
            nPack->Initialize();
            nPack->mObstacleType = RMG_OBSTACLE_TYPE_PIT;
            mObstacleList->AddElementAsTail("X", nPack, &FreeThis);

            //--Position. Pits are 4 tiles wide. They can be 2 or 3 tiles tall.
            nPack->mLft = tTilePosition;
            nPack->mRgt = tTilePosition+3;

            //--40% chance to be 2 tiles tall.
            int tHeightRoll = rand() % 100;
            if(tHeightRoll < 40)
            {
                int tHeightPos = rand() % (RMG_ACTIVE_WORLD_WIDTH-1);
                nPack->mTop = tHeightPos+0;
                nPack->mBot = tHeightPos+1;
            }
            //--40% 3 tiles tall.
            else if(tHeightRoll < 80)
            {
                int tHeightPos = rand() % (RMG_ACTIVE_WORLD_WIDTH-2);
                nPack->mTop = tHeightPos+0;
                nPack->mBot = tHeightPos+2;
            }
            //--Remainder is double-pit!
            else
            {
                nPack->mTop = 0;
                nPack->mBot = 1;

                //--Second pit.
                SetMemoryData(__FILE__, __LINE__);
                RMGObstaclePack *nSPack = (RMGObstaclePack *)starmemoryalloc(sizeof(RMGObstaclePack));
                nSPack->Initialize();
                nSPack->mObstacleType = RMG_OBSTACLE_TYPE_PIT;
                mObstacleList->AddElementAsTail("X", nSPack, &FreeThis);
                nSPack->mLft = tTilePosition;
                nSPack->mRgt = tTilePosition+3;
                nSPack->mTop = 4;
                nSPack->mBot = 5;
            }

            //--Tile position advances.
            tTilePosition += 6;
        }
        //--Place a low barrier.
        else if(tRoll < cBarrierRoll)
        {
            //--Basics.
            SetMemoryData(__FILE__, __LINE__);
            RMGObstaclePack *nPack = (RMGObstaclePack *)starmemoryalloc(sizeof(RMGObstaclePack));
            nPack->Initialize();
            nPack->mObstacleType = RMG_OBSTACLE_TYPE_LOWWALL;
            mObstacleList->AddElementAsTail("X", nPack, &FreeThis);

            //--Position. These are always 1 wide.
            nPack->mLft = tTilePosition;
            nPack->mRgt = tTilePosition;

            //--50% chance to be 4 tiles tall.
            int tHeightRoll = rand() % 100;
            if(tHeightRoll < 50)
            {
                int tHeightPos = rand() % (RMG_ACTIVE_WORLD_WIDTH-3);
                nPack->mTop = tHeightPos+0;
                nPack->mBot = tHeightPos+3;
            }
            //--6 tiles tall.
            else
            {
                nPack->mTop = 0;
                nPack->mBot = 5;
            }

            //--Tile position advances.
            tTilePosition += 2;
        }
        //--Place some enemies.
        else if(tRoll < cEnemyRoll)
        {
            //--Basics.
            SetMemoryData(__FILE__, __LINE__);
            RMGObstaclePack *nPack = (RMGObstaclePack *)starmemoryalloc(sizeof(RMGObstaclePack));
            nPack->Initialize();
            nPack->mObstacleType = RMG_OBSTACLE_TYPE_ENEMY;
            mObstacleList->AddElementAsTail("X", nPack, &FreeThis);

            int tWidthRoll = rand() % 100;
            if(tWidthRoll < 50)
            {
                nPack->mLft = tTilePosition;
                nPack->mRgt = tTilePosition;
            }
            else if(tWidthRoll < 80)
            {
                nPack->mLft = tTilePosition;
                nPack->mRgt = tTilePosition+1;
            }
            else
            {
                nPack->mLft = tTilePosition;
                nPack->mRgt = tTilePosition+3;
            }


            //--50% chance to be 4 tiles tall.
            int tHeightRoll = rand() % 100;
            if(tHeightRoll < 50)
            {
                int tHeightPos = rand() % (RMG_ACTIVE_WORLD_WIDTH-3);
                nPack->mTop = tHeightPos+0;
                nPack->mBot = tHeightPos+3;
            }
            //--6 tiles tall.
            else
            {
                nPack->mTop = 0;
                nPack->mBot = 5;
            }

            //--Tile position advances.
            tTilePosition += 5;
        }
        //--No obstacles. Move up a few tiles and roll again. This increases the difficulty faster
        //  than if an obstacle was placed!
        else
        {

            //--Tile position advances by 5.
            tTilePosition += 5;
        }

        //--Ending case. If we're within 20 tiles of the finish line, stop placing obstacles.
        if(tTilePosition >= mTotalWorldTiles - 35) break;
    }

    //--Populate the initial world.
    for(int x = 0; x < RMG_ACTIVE_WORLD_LEN; x ++)
    {
        mWorldWallHi[x] = 0;
        mWorldWallLo[x] = 0;
        for(int y = 0; y < RMG_ACTIVE_WORLD_WIDTH; y ++)
        {
            mWorldFloor[y][x] = 0;
            mWorldTile[y][x] = RMG_FLOOR_NORMAL;
        }
    }
}
StarBitmap *RunningMinigame::ResolvePlayerFrame()
{
    //--Figures out which frame the player should be showing and returns it. Can return NULL!
    //  First, get what tile the player is standing on.
    int tPlayerYPos = (int)(mPlayerYPos[0] / (float)RMG_TILE_SIZE);
    if(tPlayerYPos < 0) tPlayerYPos = 0;
    if(tPlayerYPos >= RMG_ACTIVE_WORLD_WIDTH) tPlayerYPos = RMG_ACTIVE_WORLD_WIDTH-1;
    int tPlayerTile = mWorldTile[tPlayerYPos][RMG_PLAYER_TILE];

    //--Determine if player is airborne:
    bool tIsPlayerAirborne = false;
    if(mPlayerZSpeed[0] < 0.0f) tIsPlayerAirborne = true;
    if(mPlayerZPos[0] != 0.0f || tPlayerTile == RMG_FLOOR_PIT) tIsPlayerAirborne = true;

    //--Resolve rendering frame.
    int tCurrentRunFrame = (int)(mRunTimer[0] / RMG_PLAYER_RUN_TPF) % RMG_PLAYER_RUN_TOTAL;
    StarBitmap *rPlayerFrame = rPlayerRunImg[mPlayerCurrentDirection[0]][tCurrentRunFrame];
    if(tIsPlayerAirborne)
    {
        //--Z Speed is negative, so the player is on the up:
        if(mPlayerZSpeed[0] < 0.0f)
        {
            rPlayerFrame = rPlayerRunImg[mPlayerCurrentDirection[0]][RMG_PLAYER_JUMP_UP_FRAME];
        }
        //--Player is going down.
        else
        {
            rPlayerFrame = rPlayerRunImg[mPlayerCurrentDirection[0]][RMG_PLAYER_JUMP_DN_FRAME];
        }
    }

    //--Pass it back.
    return rPlayerFrame;
}
StarBitmap *RunningMinigame::ResolveCompanionFrame()
{
    //--As above, but used for the companion who is just behind the player.
    int tSlot = RMG_COMPANION_TICKS-1;
    int tPlayerYPos = (int)(mPlayerYPos[tSlot] / (float)RMG_TILE_SIZE);
    if(tPlayerYPos < 0) tPlayerYPos = 0;
    if(tPlayerYPos >= RMG_ACTIVE_WORLD_WIDTH) tPlayerYPos = RMG_ACTIVE_WORLD_WIDTH-1;
    int tPlayerTile = mWorldTile[tPlayerYPos][RMG_PLAYER_TILE];

    //--Direction.
    int tDirection = mPlayerCurrentDirection[tSlot];

    //--Determine if player is airborne:
    bool tIsPlayerAirborne = false;
    if(mPlayerZSpeed[tSlot] < 0.0f) tIsPlayerAirborne = true;
    if(mPlayerZPos[tSlot] != 0.0f || tPlayerTile == RMG_FLOOR_PIT) tIsPlayerAirborne = true;

    //--Resolve rendering frame.
    int tCurrentRunFrame = (int)(mRunTimer[tSlot] / RMG_PLAYER_RUN_TPF) % RMG_PLAYER_RUN_TOTAL;
    StarBitmap *rCompanionFrame = rCompanionRunImg[tDirection][tCurrentRunFrame];
    if(tIsPlayerAirborne)
    {
        //--Z Speed is negative, so the player is on the up:
        if(mPlayerZSpeed[tSlot] < 0.0f)
        {
            rCompanionFrame = rCompanionRunImg[tDirection][RMG_PLAYER_JUMP_UP_FRAME];
        }
        //--Player is going down.
        else
        {
            rCompanionFrame = rCompanionRunImg[tDirection][RMG_PLAYER_JUMP_DN_FRAME];
        }
    }

    //--Pass it back.
    return rCompanionFrame;
}
void RunningMinigame::Reset()
{
    //--Resets the class to start a new game. Starts from the point where the screen is blacked out.

    //--System
    mIsActive = true;
    mHoldingPattern = true;
    mOpacityTimer = OPACITY_TICKS;
    mFadeinTimer = 0;
    mBypassTutorial = true;

    //--Introduction
    mIntroductionState = RMG_INTRO_PLAYER_RUNS_IN;
    mIntroTimer = 0;

    //--Player State
    mPlayerStopTimer = 0;
    mPlayerInvincibleTimer = 0;
    mPlayerHealth = 5;
    mPlayerSpeed = DEFAULT_PLAYER_SPEED;
    mSlashTimer = 0;
    mSlashCooldown = 0;
    for(int i = 0; i < RMG_COMPANION_TICKS; i ++)
    {
        mPlayerCurrentDirection[i] = RMG_PLAYER_RUN_W;
        mRunTimer[i] = 0;
        mJumpTimer[i] = 0;
        mPlayerYPos[i] = RMG_TILE_SIZE * (RMG_ACTIVE_WORLD_WIDTH * 0.5f);
        mPlayerZPos[i] = 0.0f;
        mPlayerZSpeed[i] = 0.0f;
    }

    //--Checkpoint does NOT reset!

    //--Enemies.
    mEnemyList->ClearList();

    //--Defeat
    mDefeatFadeTimer = 0;

    //--Hearts
    for(int i = 0; i < RMG_HEARTS_MAX; i ++)
    {
        mHeartX[i] = (HEART_SPACING_X * i) + (HEART_SPACING_X * 0.50f);
        mHeartY[i] = (HEART_SPACING_X * 0.50f);
        mHeartR[i] = 0;
        mHeartXSpeed[i] = 0.0f;
        mHeartYSpeed[i] = 0.0f;
        mHeartRSpeed[i] = 0.0f;
    }

    //--World Layout (Active)
    mCurrentWorldTile = 0;
    mTotalWorldTiles = 0;
    mWorldXOffset = 0.0f;
    memset(mWorldWallHi, 0, sizeof(int) * RMG_ACTIVE_WORLD_LEN);
    memset(mWorldWallLo, 0, sizeof(int) * RMG_ACTIVE_WORLD_LEN);
    memset(mWorldFloor,  0, sizeof(int) * RMG_ACTIVE_WORLD_WIDTH * RMG_ACTIVE_WORLD_LEN);
    memset(mWorldTile,   0, sizeof(int) * RMG_ACTIVE_WORLD_WIDTH * RMG_ACTIVE_WORLD_LEN);

    //--World Layout (Passive)
    mObstacleList->ClearList();

    //--Generate!
    GenerateLevel();
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void RunningMinigame::Update()
{
    ///--[Opacity Handler]
    //--SFX flag.
    bool tNoSFX = false;

    //--If active, increment the timer.
    if(mIsActive)
    {
        if(mOpacityTimer < OPACITY_TICKS + 60) mOpacityTimer ++;
        if(mOpacityTimer >= OPACITY_TICKS + 60) mFadeinTimer ++;
        if(mOpacityTimer < OPACITY_TICKS + 60) tNoSFX = true;
    }
    //--Inactive, decrement and stop the update.
    else
    {
        tNoSFX = true;
        if(mOpacityTimer > OPACITY_TICKS) mOpacityTimer = OPACITY_TICKS;
        if(mOpacityTimer > 0) mOpacityTimer --;
        if(mOpacityTimer == 0) return;
    }

    //--No SFX after a certain point in the outro.
    if(mIntroductionState == RMG_OUTRO_ESCAPE && mIntroTimer > 120) tNoSFX = true;

    ///--[World Movement]
    //--Every tick, the world scrolls by the player's running speed. If it exceed one "Tile", move
    //  the player's position onto the next tile.
    if(mIntroductionState != RMG_OUTRO_ESCAPE && mPlayerStopTimer == 0) mWorldXOffset = mWorldXOffset + mPlayerSpeed;
    while(mWorldXOffset >= RMG_TILE_SIZE)
    {
        //--Move forward one tile.
        mWorldXOffset = mWorldXOffset - RMG_TILE_SIZE;

        //--In "Holding Pattern", the world tile never advances.
        if(!mHoldingPattern) mCurrentWorldTile ++;

        //--Advance all the tiles by 1.
        for(int x = 0; x < RMG_ACTIVE_WORLD_LEN-1; x ++)
        {
            mWorldWallHi[x+0] = mWorldWallHi[x+1];
            mWorldWallLo[x+0] = mWorldWallLo[x+1];
            for(int y = 0; y < RMG_ACTIVE_WORLD_WIDTH; y ++)
            {
                mWorldFloor[y][x+0] = mWorldFloor[y][x+1];
                mWorldTile[y][x+0]  = mWorldTile[y][x+1];
            }
        }

        //--The last set of tiles needs to be generated, as it is now spawning just offscreen.
        int tSlot = RMG_ACTIVE_WORLD_LEN-1;

        //--On the 33rd, 66th, and 100th percents, floor tiles become finish lines.
        int cTile33 = mTotalWorldTiles / 3;
        int cTile66 = mTotalWorldTiles / 3 * 2;
        if(mCurrentWorldTile == cTile33 || mCurrentWorldTile == cTile66 || mCurrentWorldTile == mTotalWorldTiles)
        {
            //--Tiles.
            mWorldWallHi[tSlot] = 0;
            mWorldWallLo[tSlot] = 0;
            if(rand() % 100 > 80)
            {
                mWorldWallHi[tSlot] = rand() % RMG_HIWALL_TILES;
                mWorldWallLo[tSlot] = rand() % RMG_LOWALL_TILES;
            }
            for(int y = 0; y < RMG_ACTIVE_WORLD_WIDTH; y ++)
            {
                mWorldFloor[y][tSlot] = RMG_CHECKPOINT_TILE;
                mWorldTile[y][tSlot]  = RMG_FLOOR_NORMAL;
            }
        }
        //--On tiles other than every 20th tile excluding the first 30, place flat falls.
        else if(mCurrentWorldTile < 30 || mCurrentWorldTile % 20 != 0)
        {
            mWorldWallHi[tSlot] = 0;
            mWorldWallLo[tSlot] = 0;
            if(rand() % 100 > 80)
            {
                mWorldWallHi[tSlot] = rand() % RMG_HIWALL_TILES;
                mWorldWallLo[tSlot] = rand() % RMG_LOWALL_TILES;
            }
            for(int y = 0; y < RMG_ACTIVE_WORLD_WIDTH; y ++)
            {
                mWorldFloor[y][tSlot] = 0;
                mWorldTile[y][tSlot]  = RMG_FLOOR_NORMAL;
            }
        }
        //--Generate a randomly selected patch of wall to give visual variety. Place a line down the floor.
        else
        {
            mWorldWallHi[tSlot] = rand() % RMG_HIWALL_TILES;
            mWorldWallLo[tSlot] = rand() % RMG_LOWALL_TILES;
            for(int y = 0; y < RMG_ACTIVE_WORLD_WIDTH; y ++)
            {
                mWorldFloor[y][tSlot] = 1;
                mWorldTile[y][tSlot]  = RMG_FLOOR_NORMAL;
            }
        }

        //--If in a holding pattern, we never place special tile types.
        if(!mHoldingPattern)
        {
            //--Now scan the obstacle list and see if we coincide with any obstacles. If an obstacle
            //  is spawned, it gets pruned from the list.
            RMGObstaclePack *rObstaclePack = (RMGObstaclePack *)mObstacleList->SetToHeadAndReturn();
            while(rObstaclePack)
            {
                //--Check if this obstacle is on screen.
                if(mCurrentWorldTile >= rObstaclePack->mLft && mCurrentWorldTile <= rObstaclePack->mRgt)
                {
                    //--Spawn pit.
                    if(rObstaclePack->mObstacleType == RMG_OBSTACLE_TYPE_PIT)
                    {
                        for(int y = rObstaclePack->mTop; y <= rObstaclePack->mBot; y ++)
                        {
                            //--Skip if the floor is part of a checkpoint.
                            if(mWorldFloor[y][tSlot] == RMG_CHECKPOINT_TILE)
                            {

                            }
                            //--Only the top tile renders a pit tile.
                            else if(y == rObstaclePack->mTop)
                            {
                                mWorldFloor[y][tSlot] = RMG_GROUND_TILES + RMG_PIT_OFFSET;
                                mWorldTile[y][tSlot] = RMG_FLOOR_PIT;
                            }
                            //--Others render nothing.
                            else
                            {
                                mWorldFloor[y][tSlot] = -1;
                                mWorldTile[y][tSlot] = RMG_FLOOR_PIT;
                            }
                        }
                    }
                    //--Low wall.
                    else if(rObstaclePack->mObstacleType == RMG_OBSTACLE_TYPE_LOWWALL)
                    {
                        for(int y = rObstaclePack->mTop; y <= rObstaclePack->mBot; y ++)
                        {
                            //--Skip if the floor is part of a checkpoint.
                            if(mWorldFloor[y][tSlot] == RMG_CHECKPOINT_TILE)
                            {

                            }
                            //--Place.
                            else
                            {
                                mWorldFloor[y][tSlot] = RMG_GROUND_TILES + RMG_OBSTACLE_OFFSET;
                                mWorldTile[y][tSlot] = RMG_FLOOR_OBSTACLE;
                            }
                        }
                    }
                    //--Enemies!
                    else if(rObstaclePack->mObstacleType == RMG_OBSTACLE_TYPE_ENEMY)
                    {
                        //--For each tile, spawn an enemy.
                        for(int y = rObstaclePack->mTop; y <= rObstaclePack->mBot; y ++)
                        {
                            //--Skip if the floor is part of a checkpoint.
                            if(mWorldFloor[y][tSlot] == RMG_CHECKPOINT_TILE)
                            {

                            }
                            //--Spawn.
                            else
                            {
                                SetMemoryData(__FILE__, __LINE__);
                                RMGEnemy *nEnemy = (RMGEnemy *)starmemoryalloc(sizeof(RMGEnemy));
                                nEnemy->Initialize();
                                nEnemy->mX = RMG_ACTIVE_WORLD_LEN * RMG_TILE_SIZE;
                                nEnemy->mY = y * RMG_TILE_SIZE;
                                mEnemyList->AddElement("X", nEnemy, &FreeThis);
                            }
                        }
                    }

                    //--If we're on the last tile of the object, remove it.
                    if(mCurrentWorldTile == rObstaclePack->mRgt)
                    {
                        mObstacleList->RemoveRandomPointerEntry();
                    }
                }

                //--Next.
                rObstaclePack = (RMGObstaclePack *)mObstacleList->IncrementAndGetRandomPointerEntry();
            }
        }
    }

    //--Defeat!
    if(mPlayerHealth < 1)
    {
        //--Timer.
        mDefeatFadeTimer ++;

        //--Press Activate to try again!
        if(ControlManager::Fetch()->IsFirstPress("Activate") && mDefeatFadeTimer >= 25)
        {
            Reset();
        }
        return;
    }

    //--If we make it to the end of the level, the player escapes! Great work!
    int cTile33 = mTotalWorldTiles / 3;
    int cTile66 = mTotalWorldTiles / 3 * 2;
    if(mCurrentWorldTile >= mTotalWorldTiles)
    {
        if(mIntroductionState != RMG_OUTRO_ESCAPE)
        {
            mIntroTimer = 0;
            mIntroductionState = RMG_OUTRO_ESCAPE;
        }
        else
        {
            mIntroTimer ++;
            if(mIntroTimer >= 300 && mIsActive)
            {
                Deactivate();
            }
        }
    }
    //--Checkpoint 2:
    else if(mWorldFloor[0][RMG_PLAYER_TILE] == RMG_CHECKPOINT_TILE && mCurrentWorldTile >= cTile66 && mCheckpoint < 2)
    {
        //--Flag.
        mCheckpoint = 2;
        AudioManager::Fetch()->PlaySound("Menu|SpecialItem");

        //--Hearts
        mPlayerHealth = RMG_HEARTS_MAX;
        for(int i = 0; i < RMG_HEARTS_MAX; i ++)
        {
            mHeartX[i] = (HEART_SPACING_X * i) + (HEART_SPACING_X * 0.50f);
            mHeartY[i] = (HEART_SPACING_X * 0.50f);
            mHeartR[i] = 0;
            mHeartXSpeed[i] = 0.0f;
            mHeartYSpeed[i] = 0.0f;
            mHeartRSpeed[i] = 0.0f;
        }
    }
    //--Checkpoint 1:
    else if(mWorldFloor[0][RMG_PLAYER_TILE] == RMG_CHECKPOINT_TILE && mCurrentWorldTile >= cTile33 && mCheckpoint < 1)
    {
        //--Flag.
        mCheckpoint = 1;
        AudioManager::Fetch()->PlaySound("Menu|SpecialItem");

        //--Hearts
        mPlayerHealth = RMG_HEARTS_MAX;
        for(int i = 0; i < RMG_HEARTS_MAX; i ++)
        {
            mHeartX[i] = (HEART_SPACING_X * i) + (HEART_SPACING_X * 0.50f);
            mHeartY[i] = (HEART_SPACING_X * 0.50f);
            mHeartR[i] = 0;
            mHeartXSpeed[i] = 0.0f;
            mHeartYSpeed[i] = 0.0f;
            mHeartRSpeed[i] = 0.0f;
        }
    }

    ///--[Enemy Movement]
    //--Each tick, spawned enemies move closer to the player by the movement speed.
    RMGEnemy *rEnemy = (RMGEnemy *)mEnemyList->SetToHeadAndReturn();
    while(rEnemy)
    {
        //--Move.
        if(mPlayerStopTimer == 0) rEnemy->mX = rEnemy->mX - mPlayerSpeed;

        //--Enemies that are "downed" can't damage the player and instead just fly off screen.
        if(rEnemy->mIsInjured)
        {
            //--Speeds.
            rEnemy->mX = rEnemy->mX + rEnemy->mXSpeed;
            rEnemy->mY = rEnemy->mY + rEnemy->mYSpeed;

            //--Gravity.
            rEnemy->mYSpeed = rEnemy->mYSpeed + 0.350f;

            //--Air resistance.
            rEnemy->mXSpeed = rEnemy->mXSpeed * 0.99f;
        }
        //--These enemies can be hit and can hit the player.
        else
        {
            //--If the enemy is within 8 pixels radially they hit the player. They can hit airborne
            //  players, and are quite tall!
            float tRadialDistance = GetPlanarDistance(rEnemy->mX, rEnemy->mY, RMG_PLAYER_TILE * RMG_TILE_SIZE, mPlayerYPos[0]);
            if(tRadialDistance < 26.0f && mPlayerZPos[0] <= 0.0f && mPlayerZPos[0] >= -23.0f)
            {
                //--If the player is not swinging:
                if(mSlashTimer == 0 && tRadialDistance < 8.0f)
                {
                    if(mPlayerInvincibleTimer == 0)
                    {
                        TakeHit();
                        mPlayerInvincibleTimer = mPlayerInvincibleTimer / 2;

                        //--Enemy is defeated upon impact!
                        rEnemy->mIsInjured = true;

                        //--Send them flying!
                        rEnemy->mXSpeed = (rand() % 10) - 5;
                        rEnemy->mYSpeed = (rand() % 5) - 5;
                    }
                }
                //--Player is swinging, enemy gets defeated!
                else if(mSlashTimer != 0)
                {
                    //--SFX and Flag.
                    AudioManager::Fetch()->PlaySound("World|Slap");
                    rEnemy->mIsInjured = true;

                    //--Send them flying!
                    rEnemy->mXSpeed = (rand() % 10) - 5;
                    rEnemy->mYSpeed = (rand() % 5) - 5;
                }
            }
        }

        //--If the entity moves past -32, they despawn.
        if(rEnemy->mX < -32) mEnemyList->RemoveRandomPointerEntry();

        //--Next.
        rEnemy = (RMGEnemy *)mEnemyList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Player Timers]
    //--Each tick, all data gets moved back one in the array. The 0th element is the current, the trailing
    //  ones are used for following companions.
    for(int i = RMG_COMPANION_TICKS - 1; i > 0; i --)
    {
        mRunTimer[i] = mRunTimer[i-1];
        mJumpTimer[i] = mJumpTimer[i-1];
        mPlayerYPos[i] = mPlayerYPos[i-1];
        mPlayerZPos[i] = mPlayerZPos[i-1];
        mPlayerZSpeed[i] = mPlayerZSpeed[i-1];
        mPlayerCurrentDirection[i] = mPlayerCurrentDirection[i-1];
    }

    //--Heart timers. If the player has taken a hit, the hearts update.
    for(int i = mPlayerHealth; i < RMG_HEARTS_MAX; i ++)
    {
        mHeartYSpeed[i] = mHeartYSpeed[i] + 0.500f;
        mHeartX[i] = mHeartX[i] + mHeartXSpeed[i];
        mHeartY[i] = mHeartY[i] + mHeartYSpeed[i];
        mHeartR[i] = mHeartR[i] + mHeartRSpeed[i];
    }

    //--Stop timer. When the player falls off a pit, this timer stops the level from moving.
    if(mPlayerStopTimer > 0) mPlayerStopTimer --;

    //--Player invincible timer. Player cannot take damage while invincible.
    if(mPlayerInvincibleTimer > 0) mPlayerInvincibleTimer --;

    //--If cooling, slash timer decrements.
    if(mSlashTimer    > 0) mSlashTimer --;
    if(mSlashCooldown > 0) mSlashCooldown --;

    //--Get what tile the player is currently standing on. It's always the 3rd tile from the "end"
    //  but the player can move up and down.
    int tPlayerYPos = (int)(mPlayerYPos[0] / (float)RMG_TILE_SIZE);
    if(tPlayerYPos < 0) tPlayerYPos = 0;
    if(tPlayerYPos >= RMG_ACTIVE_WORLD_WIDTH) tPlayerYPos = RMG_ACTIVE_WORLD_WIDTH-1;
    int tPlayerTile = mWorldTile[tPlayerYPos][RMG_PLAYER_TILE];

    ///--[Airborne]
    //--Constants.
    int cJumpBoostTicks = 15;
    float cJumpPower = -5.0f;
    float cGravity = 0.305f;

    //--If the player is not airborne:
    bool tIsPlayerAirborne = false;
    if(mPlayerZSpeed[0] < 0.0f) tIsPlayerAirborne = true;
    if(mPlayerZPos[0] != 0.0f || tPlayerTile == RMG_FLOOR_PIT) tIsPlayerAirborne = true;

    //--Increment the run timer if the player is not airborne.
    if(!tIsPlayerAirborne)
    {
        //--Timers.
        mRunTimer[0] ++;
        mJumpTimer[0] = -1;
        mPlayerZSpeed[0] = 0.0f;

        //--SFX.
        if(mRunTimer[0] % (int)(RMG_PLAYER_RUN_TPF * RMG_PLAYER_RUN_TOTAL) == (int)(RMG_PLAYER_RUN_TPF * 1.0f))
        {
            if(!tNoSFX) AudioManager::Fetch()->PlaySound("World|FootstepL_00");
        }
        else if(mRunTimer[0] % (int)(RMG_PLAYER_RUN_TPF * RMG_PLAYER_RUN_TOTAL) == (int)(RMG_PLAYER_RUN_TPF * 3.0f))
        {
            if(!tNoSFX) AudioManager::Fetch()->PlaySound("World|FootstepR_00");
        }

        //--If the player hits an obstacle, damage them. Invincibility time is lower for this.
        if(tPlayerTile == RMG_FLOOR_OBSTACLE && mPlayerInvincibleTimer == 0)
        {
            TakeHit();
            mPlayerInvincibleTimer = mPlayerInvincibleTimer / 2;
        }
    }
    //--If airborne, the player's run timer defaults to the 3rd frame.
    else
    {
        //--Set frame.
        mRunTimer[0] = RMG_PLAYER_RUN_TPF * 3.0f + 1;

        //--Player's jump timer decrements.
        mJumpTimer[0] --;

        //--Gravity.
        mPlayerZSpeed[0] = mPlayerZSpeed[0] + cGravity;
        mPlayerZPos[0] = mPlayerZPos[0] + mPlayerZSpeed[0];

        //--Less than 6 units above the ground. Can hit low obstacles.
        if(mPlayerZPos[0] <= 0.0f && mPlayerZPos[0] >= -6.0f)
        {
            //--If the player hits an obstacle, damage them. Invincibility time is lower for this.
            if(tPlayerTile == RMG_FLOOR_OBSTACLE && mPlayerInvincibleTimer == 0)
            {
                TakeHit();
                mPlayerInvincibleTimer = mPlayerInvincibleTimer / 2;
            }
        }
        //--Hitting the ground.
        else if(mPlayerZPos[0] >= 0.0f && mPlayerZPos[0] < 8.0f && tPlayerTile != RMG_FLOOR_PIT)
        {
            tIsPlayerAirborne = false;
            mPlayerZPos[0] = 0.0f;
            if(!tNoSFX) AudioManager::Fetch()->PlaySound("World|Land");
        }
        //--Player has fallen under the world!
        else if(mPlayerZPos[0] >= 8.0f)
        {
            //--Player falls off the bottom of the level and takes a hit. The player does not take the
            //  hit until they are on normal ground again.
            //--Falling off the level also stops time so the player can reorient.
            mPlayerZSpeed[0] = mPlayerZSpeed[0] + (cGravity * 0.5f);
            if(mPlayerZPos[0] >= 170.0f && tPlayerTile != RMG_FLOOR_PIT)
            {
                TakeHit();
                mPlayerZPos[0] = 0.0f;
                mPlayerStopTimer = 120;
            }
        }
    }

    ///--[Introduction]
    //--Fast-Access Pointers
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Handles the introduction. If set to RMG_INTRO_NONE, normal gameplay.
    if(mIntroductionState == RMG_INTRO_NONE)
    {
        //--Timer still runs, used to fade stuff out.
        mIntroTimer ++;
        if(mIntroTimer == 25) mHoldingPattern = false;
    }
    //--Player runs in.
    else if(mIntroductionState == RMG_INTRO_PLAYER_RUNS_IN)
    {
        //--Timer.
        mIntroTimer ++;
        if(mIntroTimer >= RMG_INTRO_RUN_IN_END)
        {
            if(!mBypassTutorial)
            {
                mIntroTimer = 0;
                mIntroductionState = RMG_INTRO_ASK_TO_SKIP;
            }
            else
            {
                mIntroTimer = 24;
                mIntroductionState = RMG_INTRO_NONE;
            }
        }
    }
    //--Asking the player to skip the intro. Remains onscreen until the player presses a key, handled below.
    else if(mIntroductionState == RMG_INTRO_ASK_TO_SKIP)
    {
        mIntroTimer ++;
    }
    //--Learning to jump.
    else if(mIntroductionState == RMG_INTRO_LEARN_TO_JUMP)
    {
        mIntroTimer ++;
        if(rControlManager->IsFirstPress("Run"))
        {
            mIntroTimer = 0;
            mIntroductionState = RMG_INTRO_LEARN_TO_SWING;
        }
    }
    //--Learning to fight.
    else if(mIntroductionState == RMG_INTRO_LEARN_TO_SWING)
    {
        mIntroTimer ++;
        if(rControlManager->IsFirstPress("Run"))
        {
            mIntroTimer = 0;
            mIntroductionState = RMG_INTRO_SHOW_HP;
        }
    }
    //--Learning to HP.
    else if(mIntroductionState == RMG_INTRO_SHOW_HP)
    {
        mIntroTimer ++;
        if(rControlManager->IsFirstPress("Run"))
        {
            mIntroTimer = 0;
            mIntroductionState = RMG_INTRO_F1_TO_SKIP;
        }
    }
    //--Learning to cheat.
    else if(mIntroductionState == RMG_INTRO_F1_TO_SKIP)
    {
        mIntroTimer ++;
        if(rControlManager->IsFirstPress("Run"))
        {
            mIntroTimer = 0;
            mIntroductionState = RMG_INTRO_NONE;
        }
    }

    ///--[Player Input]
    //--Player defaults to running west.
    mPlayerCurrentDirection[0] = RMG_PLAYER_RUN_W;

    //--All player controls are locked out when the player is escaping.
    if(mIntroductionState == RMG_OUTRO_ESCAPE) return;

    //--Player can quit out of the minigame by pressing F1.
    if(rControlManager->IsFirstPress("F1"))
    {
        Deactivate();
        return;
    }

    ///--[Up And Down]
    int cMoveRate = 2;
    if(rControlManager->IsDown("Up") && !rControlManager->IsDown("Down"))
    {
        mPlayerCurrentDirection[0] = RMG_PLAYER_RUN_NW;
        mPlayerYPos[0] -= cMoveRate;
        if(mPlayerYPos[0] < 0) mPlayerYPos[0] = 0;
    }
    if(rControlManager->IsDown("Down") && !rControlManager->IsDown("Up"))
    {
        mPlayerCurrentDirection[0] = RMG_PLAYER_RUN_SW;
        mPlayerYPos[0] += cMoveRate;
        if(mPlayerYPos[0] >= RMG_TILE_SIZE * RMG_ACTIVE_WORLD_WIDTH) mPlayerYPos[0] = RMG_TILE_SIZE * RMG_ACTIVE_WORLD_WIDTH;
    }

    ///--[Jump]
    //--Bound! Leap!
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Asking to skip the intro. Activate skips it.
        if(mIntroductionState == RMG_INTRO_ASK_TO_SKIP)
        {
            mIntroductionState = RMG_INTRO_LEARN_TO_JUMP;
            mIntroTimer = 0;
        }
        //--Does nothing if currently airborne.
        else if(tIsPlayerAirborne)
        {
        }
        //--Player must be standing on a ground tile.
        else
        {
            //--Jump!
            mJumpTimer[0] = cJumpBoostTicks;
            mPlayerZSpeed[0] = cJumpPower;

            //--SFX.
            if(!tNoSFX) AudioManager::Fetch()->PlaySound("World|Jump");
        }

    }
    //--If not the first press, and the jump key is down, player can get some air.
    else if(rControlManager->IsDown("Activate"))
    {
        //--If the player has jump boost ticks left, reduce gravity for the tick.
        if(mJumpTimer[0] > 0)
        {
            mPlayerZSpeed[0] = mPlayerZSpeed[0] - (cGravity * 0.30f);
        }
    }

    ///--[Cancel]
    //--Slashes the player's weapon.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Asking to skip the intro. Activate skips it.
        if(mIntroductionState == RMG_INTRO_ASK_TO_SKIP)
        {
            mIntroductionState = RMG_INTRO_NONE;
            mIntroTimer = 0;
        }
        //--Otherwise, attempt to slash the weapon.
        else if(mSlashCooldown < 1)
        {
            //--Set player's slash timer and cooldown.
            mSlashTimer = SLASH_TICKS;
            mSlashCooldown = SLASH_COOLDOWN;

            //--SFX.
            if(!tNoSFX) AudioManager::Fetch()->PlaySound("Combat|AttackMiss");
        }
        //--Still on cooldown, does nothing.
        else
        {
        }
    }

    //--Debug.
    if(rControlManager->IsDown("Run") && false)
    {
        mPlayerSpeed = 0.1f;
    }
    else
    {
        mPlayerSpeed = DEFAULT_PLAYER_SPEED;
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void RunningMinigame::Render()
{
    ///--[Documentation and Setup]
    //--Renders the running minigame, which is the player and a companion running and jumping over
    //  randomly generated obstacles. The game emulates movement by scrolling the ground beneath
    //  the player.
    if(!Images.mIsReady) return;

    ///--[Constants]
    //--Z positions
    //float cBackingZ = -0.900020f;
    float cTileZStep = 0.0000010f;
    float cWorldZ = -0.900100f;
    float cWorldZBack = -0.010000f;
    float cPlayerZNormal = cWorldZ + cTileZStep;
    float cPlayerZStep = -0.0000001f;

    //--Variables.
    bool tShowPlayerShadow = true;
    bool tShowCompanionShadow = true;

    ///--[Opacity Handler]
    //--Don't render at all if the opacity is zero. Otherwise, compute opacity.
    if(mOpacityTimer < 1) return;

    //--Backing.
    float cOpacity = (float)mOpacityTimer / (float)OPACITY_TICKS;
    if(cOpacity < 1.0f)
    {
        StarBitmap::DrawFullBlack(cOpacity);
        return;
    }

    ///--[World]
    //--World Scale
    float cScale = 3.0f;

    //--Rendering position.
    float cWorldOffsetX =   0.0f + (mWorldXOffset * cScale) + (RMG_TILE_SIZE * 2.0f);
    float cWorldOffsetY = 192.0f;

    //--Offset.
    glTranslatef(cWorldOffsetX, cWorldOffsetY, cWorldZ);
    glScalef(cScale, cScale, 1.0f);

    //--Render the world tiles.
    for(int x = 0; x < RMG_ACTIVE_WORLD_LEN; x ++)
    {
        //--Rendering Position X. Tile data is stored right-to-left.
        float tRenderX = (VIRTUAL_CANVAS_X / cScale) - (x * RMG_TILE_SIZE) - (RMG_TILE_SIZE);

        //--High Wall
        if(mWorldWallHi[x] < 0 || mWorldWallHi[x] >= RMG_HIWALL_TILES) continue;
        Images.Data.mWallHi[mWorldWallHi[x]]->Draw(tRenderX, RMG_TILE_SIZE * 0.0f);

        //--Low Wall
        if(mWorldWallLo[x] < 0 || mWorldWallLo[x] >= RMG_LOWALL_TILES) continue;
        Images.Data.mWallLo[mWorldWallLo[x]]->Draw(tRenderX, RMG_TILE_SIZE * 1.0f);

        //--If the 0th floor is the checkpoint tile, and we're not past the second checkpoint, render some UI.
        if(mWorldFloor[0][x] == RMG_CHECKPOINT_TILE && mCurrentWorldTile < mTotalWorldTiles * 0.90f)
        {
            glTranslatef(0.0f, 0.0f, 0.000001f);
            Images.Data.mCheckpointL->Draw(tRenderX - RMG_TILE_SIZE, RMG_TILE_SIZE * 1.0f);
            Images.Data.mCheckpointM->Draw(tRenderX ,                RMG_TILE_SIZE * 1.0f);
            Images.Data.mCheckpointR->Draw(tRenderX + RMG_TILE_SIZE, RMG_TILE_SIZE * 1.0f);
            glTranslatef(0.0f, 0.0f, -0.000001f);
        }

        //--Floors.
        for(int y = 0; y < RMG_ACTIVE_WORLD_WIDTH; y ++)
        {
            //--Get the index.
            int tIndex = mWorldFloor[y][x];
            glTranslatef(0.0f, 0.0f, cTileZStep * y);

            //--Render nothing here:
            if(tIndex < 0)
            {
                glTranslatef(0.0f, 0.0f, cTileZStep * -y);
                continue;
            }
            //--Basic floor:
            else if(tIndex < RMG_GROUND_TILES)
            {
                Images.Data.mGroundTile[tIndex]->Draw(tRenderX, RMG_TILE_SIZE * (2.0f + y));
            }
            //--Pit:
            else if(tIndex == RMG_GROUND_TILES + RMG_PIT_OFFSET)
            {
                glTranslatef(0.0f, 0.0f, cWorldZBack);
                Images.Data.mPitWall->Draw(tRenderX, RMG_TILE_SIZE * (2.0f + y));
                glTranslatef(0.0f, 0.0f, -cWorldZBack);
            }
            //--Warning Tape:
            else if(tIndex == RMG_GROUND_TILES + RMG_WARNING_OFFSET)
            {
                Images.Data.mGroundTile[0]->Draw(tRenderX, RMG_TILE_SIZE * (2.0f + y));
                Images.Data.mPitMarkerTile->Draw(tRenderX, RMG_TILE_SIZE * (2.0f + y));
            }
            //--Obstacle:
            else if(tIndex == RMG_GROUND_TILES + RMG_OBSTACLE_OFFSET)
            {
                Images.Data.mGroundTile[0]->Draw(tRenderX, RMG_TILE_SIZE * (2.0f + y));
                Images.Data.mLowObstacle->Draw(tRenderX, RMG_TILE_SIZE * (2.0f + y));
            }
            glTranslatef(0.0f, 0.0f, cTileZStep * -y);
        }
    }

    //--Clean world scales.
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    glTranslatef(-cWorldOffsetX, -cWorldOffsetY, -cWorldZ);

    //--Render a black area over the bottom part of the screen.
    glColor3f(0.0f, 0.0f, 0.0f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f, 576.0f);
        glVertex2f(VIRTUAL_CANVAS_X, 576.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    StarlightColor::ClearMixer();

    ///--[Player Rendering]
    //--Render the player. Player uses the world scale but not the same offsets.
    float tPlayerX = (VIRTUAL_CANVAS_X) - (RMG_TILE_SIZE * (RMG_PLAYER_TILE) * cScale) + (24.0f);
    float tPlayerY = cWorldOffsetY + (RMG_TILE_SIZE * 2.0f * cScale) + (mPlayerYPos[0] * cScale);
    float cHeartOffsetX = 0.0f;

    //--During the intro, the player's X position is offset.
    if(mIntroductionState == RMG_INTRO_PLAYER_RUNS_IN)
    {
        //--Offset.
        float cTotalOffset = (RMG_TILE_SIZE * 7.0f) * cScale;

        //--Fully offscreen.
        if(mIntroTimer < RMG_INTRO_RUN_IN_OFFSCREEN)
        {
            tPlayerX = tPlayerX + cTotalOffset;
            cHeartOffsetX = -cTotalOffset;
        }
        //--Running in.
        else if(mIntroTimer < RMG_INTRO_RUN_IN_OFFSCREEN_DONE)
        {
            float tPercent = 1.0f - ((float)(mIntroTimer - RMG_INTRO_RUN_IN_OFFSCREEN) / (float)(RMG_INTRO_RUN_IN_OFFSCREEN_DONE - RMG_INTRO_RUN_IN_OFFSCREEN));
            tPlayerX = tPlayerX + (cTotalOffset * tPercent);
            cHeartOffsetX = (-cTotalOffset * tPercent);
        }
        //--Done running in.
        else
        {
        }
    }
    //--Outro, player runs offscreen.
    else if(mIntroductionState == RMG_OUTRO_ESCAPE)
    {
        tPlayerX = tPlayerX - (mIntroTimer * (mPlayerSpeed * cScale));
    }

    //--Player's Y position on screen is modified by the Z position. The shadow does not change.
    float cPlayerZOffset = mPlayerZPos[0];

    //--Player's depth depends on which tile they are on.
    int tPlayerTile = (int)(mPlayerYPos[0] / RMG_TILE_SIZE);
    float tPlayerZMod = (cTileZStep * (float)tPlayerTile);
    if(mWorldTile[tPlayerTile][RMG_PLAYER_TILE] == RMG_PLAYER_TILE) tShowPlayerShadow = false;

    //--Move to position.
    float tUsePlayerZ = cPlayerZNormal + tPlayerZMod;
    if(mPlayerZPos[0] > 0.0)
    {
        tShowPlayerShadow = false;
        tUsePlayerZ = tUsePlayerZ + (cPlayerZStep * mPlayerZPos[0]);
    }
    glTranslatef(tPlayerX, tPlayerY, tUsePlayerZ);
    glScalef(cScale, cScale, 1.0f);

    //--Render the player's current frame.
    StarBitmap *rPlayerFrame = ResolvePlayerFrame();
    if(mPlayerInvincibleTimer > 0 && mPlayerInvincibleTimer % 4 < 2)
    {

    }
    else
    {
        if(rShadowImg && tShowPlayerShadow) rShadowImg->Draw(-16, -30);
        if(rPlayerFrame) rPlayerFrame->Draw(-16, -30 + cPlayerZOffset);
        if(mSlashTimer > SLASH_TICKS / 2)
        {
            Images.Data.mSlashU->Draw(-13, -32 + cPlayerZOffset);
        }
        else if(mSlashTimer > 0)
        {
            Images.Data.mSlashD->Draw(-13, -32 + cPlayerZOffset);
        }
    }

    //--Clean up.
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    glTranslatef(-tPlayerX, -tPlayerY, -tUsePlayerZ);

    ///--[Companion Rendering]
    //--Same as the player, but may be in a different place/facing.
    int cCompSlot = RMG_COMPANION_TICKS-1;

    //--Companions's depth depends on which tile they are on.
    int tCompanionTile = (int)(mPlayerYPos[cCompSlot] / RMG_TILE_SIZE);
    float tCompanionZMod = (cTileZStep * (float)tCompanionTile);
    float tUseCompanionZ = cPlayerZNormal + tCompanionZMod;
    if(mPlayerZPos[cCompSlot] > 0.0)
    {
        tShowCompanionShadow = false;
        tUseCompanionZ = tUseCompanionZ + (cPlayerZStep * mPlayerZPos[cCompSlot]);
    }
    if(mWorldTile[tCompanionTile][RMG_PLAYER_TILE] == RMG_PLAYER_TILE) tShowCompanionShadow = false;

    //--Other positions.
    float tCompanionX = tPlayerX + 64.0f;
    float tCompanionY = cWorldOffsetY + (RMG_TILE_SIZE * 2.0f * cScale) + (mPlayerYPos[cCompSlot] * cScale);
    float cCompanionZOffset = mPlayerZPos[cCompSlot];
    glTranslatef(tCompanionX, tCompanionY, tUseCompanionZ);
    glScalef(cScale, cScale, 1.0f);

    //--Resolve, render. If it comes back NULL, there's no companion character.
    StarBitmap *rCompanionFrame = ResolveCompanionFrame();
    if(rCompanionFrame)
    {
        if(mPlayerInvincibleTimer > 0 && mPlayerInvincibleTimer % 4 < 2)
        {

        }
        else
        {
            if(rShadowImg && tShowCompanionShadow) rShadowImg->Draw(-16, -30);
            rCompanionFrame->Draw(-16, -30 + cCompanionZOffset);
        }
    }

    //--Clean.
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    glTranslatef(-tCompanionX, -tCompanionY, -tUseCompanionZ);

    ///--[Enemy Rendering]
    //--Enemies render a single image at the listed position. Not a complex enemy, phew!
    float tEnemyX = 0.0f;
    float tEnemyY = 0.0f;
    float tEnemyZ = 0.0f;
    RMGEnemy *rEnemy = (RMGEnemy *)mEnemyList->PushIterator();
    while(rEnemy)
    {
        //--On their feet.
        StarBitmap *rImage = NULL;
        if(!rEnemy->mIsInjured)
        {
            rImage = Images.Data.mEnemiesUp[rEnemy->mImage];
        }
        //--Downed frame.
        else
        {
            rImage = Images.Data.mEnemiesDn[rEnemy->mImage];
        }

        //--Setup.
        int tEnemyTile = (int)(rEnemy->mY / RMG_TILE_SIZE);
        float tEnemyZMod = (cTileZStep * (float)tEnemyTile);
        tEnemyX = VIRTUAL_CANVAS_X - (rEnemy->mX * cScale);
        tEnemyY = cWorldOffsetY + (rEnemy->mY * cScale) + (RMG_TILE_SIZE * 2.0f * cScale);
        tEnemyZ = cPlayerZNormal + tEnemyZMod;

        //--Render.
        glTranslatef(tEnemyX, tEnemyY, tEnemyZ);
        glScalef(cScale, cScale, 1.0f);
        if(rShadowImg) rShadowImg->Draw(-16, -22);
        if(rImage)     rImage->Draw(    - 8, -22);
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-tEnemyX, -tEnemyY, -tEnemyZ);

        //--Next.
        rEnemy = (RMGEnemy *)mEnemyList->AutoIterate();
    }

    ///--[Heart UI]
    //--Renders the five hearts. They can go offscreen!
    for(int i = 0; i < RMG_HEARTS_MAX; i ++)
    {
        glTranslatef((mHeartX[i] + cHeartOffsetX), mHeartY[i], 0.0f);
        glRotatef(mHeartR[i], 0.0f, 0.0f, 1.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.Data.mHeart->Draw(-8, -8);
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glRotatef(-mHeartR[i], 0.0f, 0.0f, 1.0f);
        glTranslatef(-(mHeartX[i] + cHeartOffsetX), -mHeartY[i], 0.0f);
    }

    ///--[Distance Indicator]
    //--Shows how far the player has left to go. It has 10 pieces, with a left/right piece and 8 middles.
    float cDistanceTop =   0.0f;
    float cDistanceRgt = VIRTUAL_CANVAS_X - 32.0f;
    float cDistanceLft = cDistanceRgt - 480.0f;
    float cDistanceSpc = 16.0f; //Already scaled!
    glTranslatef(cDistanceLft, cDistanceTop, 0.0f);
    glScalef(cScale, cScale, 1.0f);
        Images.Data.mDistanceL->Draw(cDistanceSpc * 0, 0);
        for(int i = 0; i < 8; i ++)
        {
            Images.Data.mDistanceM->Draw(cDistanceSpc * (i+1), 0);
        }
        Images.Data.mDistanceR->Draw(cDistanceSpc * 9, 0);
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    glTranslatef(-cDistanceLft, -cDistanceTop, 0.0f);

    //--Player distance indicator.
    float cPlayerProgress = (float)mCurrentWorldTile / (float)mTotalWorldTiles;
    if(cPlayerProgress < 0.0f) cPlayerProgress = 0.0f;
    if(cPlayerProgress > 1.0f) cPlayerProgress = 1.0f;
    if(rPlayerFrame)
    {
        float tXPos = cDistanceRgt + ((cDistanceLft - cDistanceRgt) * cPlayerProgress);
        float tYPos = cDistanceTop + 16.0f + 22.0f; //Bottom-align.
        glTranslatef(tXPos, tYPos, 0.0f);
        rPlayerFrame->Draw(-16, -30);
        glTranslatef(-tXPos, -tYPos, 0.0f);
    }

    ///--[Tutorial Overlays]
    //--No intro state. Fade out!
    if(mIntroductionState == RMG_INTRO_NONE)
    {
        //--Only do this if we actually need to fade.
        if(mIntroTimer < RMG_INTRO_FADE_TICKS)
        {
            float tPercent = mIntroTimer / (float)RMG_INTRO_FADE_TICKS;
            if(tPercent > 1.0f) tPercent = 1.0f;
            float cAlpha = (1.0f - tPercent) * 0.30f;
            StarBitmap::DrawFullBlack(cAlpha);
        }
    }
    //--Asking the player to skip the intro or not.
    else if(mIntroductionState == RMG_INTRO_ASK_TO_SKIP)
    {
        //--Darkening.
        float tPercent = mIntroTimer / (float)RMG_INTRO_FADE_TICKS;
        if(tPercent > 1.0f) tPercent = 1.0f;
        float cAlpha = tPercent * 0.30f;
        StarBitmap::DrawFullBlack(cAlpha);

        //--After the fade, render the tutorial image.
        if(mIntroTimer >= RMG_INTRO_FADE_TICKS)
        {
            float cInstructionAlpha = (mIntroTimer-RMG_INTRO_FADE_TICKS) / (float)RMG_INTRO_FADE_TICKS;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cInstructionAlpha);
            Images.Data.rTutorial0->Draw();
            StarlightColor::ClearMixer();
        }
    }
    //--Learnin to jump!
    else if(mIntroductionState == RMG_INTRO_LEARN_TO_JUMP)
    {
        //--Darkening.
        float tPercent = mIntroTimer / (float)RMG_INTRO_FADE_TICKS;
        StarBitmap::DrawFullBlack(0.30f);

        //--Render the old image, fading out, and the new image fading in.
        if(tPercent < 1.0f)
        {
            float cInstructionAlpha = (mIntroTimer) / (float)RMG_INTRO_FADE_TICKS;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, (1.0f - cInstructionAlpha));
            Images.Data.rTutorial0->Draw();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cInstructionAlpha);
            Images.Data.rTutorial1->Draw();
            StarlightColor::ClearMixer();
        }
        //--Render the new image at full opacity.
        else
        {
            Images.Data.rTutorial1->Draw();
        }
    }
    //--Learnin to fight!
    else if(mIntroductionState == RMG_INTRO_LEARN_TO_SWING)
    {
        //--Darkening.
        float tPercent = mIntroTimer / (float)RMG_INTRO_FADE_TICKS;
        StarBitmap::DrawFullBlack(0.30f);

        //--Render the old image, fading out, and the new image fading in.
        if(tPercent < 1.0f)
        {
            float cInstructionAlpha = (mIntroTimer) / (float)RMG_INTRO_FADE_TICKS;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, (1.0f - cInstructionAlpha));
            Images.Data.rTutorial1->Draw();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cInstructionAlpha);
            Images.Data.rTutorial2->Draw();
            StarlightColor::ClearMixer();
        }
        //--Render the new image at full opacity.
        else
        {
            Images.Data.rTutorial2->Draw();
        }
    }
    //--Here's your HP!
    else if(mIntroductionState == RMG_INTRO_SHOW_HP)
    {
        //--Darkening.
        float tPercent = mIntroTimer / (float)RMG_INTRO_FADE_TICKS;
        StarBitmap::DrawFullBlack(0.30f);

        //--Render the old image, fading out, and the new image fading in.
        if(tPercent < 1.0f)
        {
            float cInstructionAlpha = (mIntroTimer) / (float)RMG_INTRO_FADE_TICKS;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, (1.0f - cInstructionAlpha));
            Images.Data.rTutorial2->Draw();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cInstructionAlpha);
            Images.Data.rTutorial3->Draw();
            StarlightColor::ClearMixer();
        }
        //--Render the new image at full opacity.
        else
        {
            Images.Data.rTutorial3->Draw();
        }
    }
    //--Here's how to skip this!
    else if(mIntroductionState == RMG_INTRO_F1_TO_SKIP)
    {
        //--Darkening.
        float tPercent = mIntroTimer / (float)RMG_INTRO_FADE_TICKS;
        StarBitmap::DrawFullBlack(0.30f);

        //--Render the old image, fading out, and the new image fading in.
        if(tPercent < 1.0f)
        {
            float cInstructionAlpha = (mIntroTimer) / (float)RMG_INTRO_FADE_TICKS;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, (1.0f - cInstructionAlpha));
            Images.Data.rTutorial3->Draw();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cInstructionAlpha);
            Images.Data.rTutorial4->Draw();
            StarlightColor::ClearMixer();
        }
        //--Render the new image at full opacity.
        else
        {
            Images.Data.rTutorial4->Draw();
        }
    }

    ///--[Clean Up]
    StarlightColor::ClearMixer();

    //--Fading in. Uses the fade-in timer, which runs after the opacity finishes.
    if(mIntroductionState != RMG_OUTRO_ESCAPE)
    {
        cOpacity = (float)mFadeinTimer / (float)OPACITY_TICKS;
        if(cOpacity < 1.0f)
        {
            StarBitmap::DrawFullBlack(1.0f - cOpacity);
        }
    }
    //--During the escape, fade out.
    else if(mIntroductionState == RMG_OUTRO_ESCAPE)
    {
        cOpacity = (float)(mIntroTimer-240) / (float)OPACITY_TICKS;
        if(cOpacity > 0.0f)
        {
            StarBitmap::DrawFullBlack(cOpacity);
        }
    }

    //--Defeat fade.
    if(mPlayerHealth < 1)
    {
        cOpacity = (float)(mDefeatFadeTimer) / (float)OPACITY_TICKS;
        if(cOpacity > 0.0f)
        {
            StarBitmap::DrawFullBlack(cOpacity);
        }

        //--Render this image.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cOpacity);
        Images.Data.rTutorial5->Draw();
        StarlightColor::ClearMixer();
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
