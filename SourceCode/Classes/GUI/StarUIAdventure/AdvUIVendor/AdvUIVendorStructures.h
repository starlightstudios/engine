///================================== AdvUI Vendor Structures =====================================
//--Vendor-related structures with implementations of functions in an affiliated .cc file.
#pragma once

///--[ ========== Includes ========== ]
#include "AdvCombat.h"

///--[ ====== Shop Unlock Pack ====== ]
//--Used for the unlock UI, found inside ShopInventoryPacks. Contains an item name and a number
//  required. Also has a "Short" name for the UI.
typedef struct ShopUnlockPack
{
    //--Members
    char *mItemName;
    char *mItemDisplayName;
    char *mItemShort;
    int mNumberNeeded;

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}ShopUnlockPack;

///--[ ==== Shop Inventory Pack ===== ]
//--A package containing an item to be purchased in a shop.
typedef struct ShopInventoryPack
{
    //--Members
    AdventureItem *mItem;
    char *mDisplayName;
    int mOwnedDisplay;
    int mPriceOverride;
    int mQuantity;
    bool mOwnedBy[ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE];

    //--Unlock Data
    float mScrollMax;
    char *mAssociatedVariable;
    StarLinkedList *mUnlockRequirements; //ShopUnlockPack *, master
    bool mRemoveOnUnlock;
    char *mExecOnUnlock;

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}ShopInventoryPack;

///--[ ====== ShopCategoryPack ====== ]
//--Contains a category, which is a subheading of items. The player can switch between categories
//  to change which items they are examining. Only used for Unlocking mode right now.
typedef struct ShopCategoryPack
{
    //--Members
    StarBitmap *rIcon;
    StarLinkedList *mrMembers; //ShopInventoryPack *, ref

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}ShopCategoryPack;

///--[ ===== ShopMaterialsPage ====== ]
//--Contains a list of materials that appears on a 'page'. If there are a lot of materials, this
//  can help to keep them organized.
typedef struct ShopMaterialsPage
{
    //--Members
    StarLinkedList *mMaterials; //char *, master

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}ShopMaterialsPage;
