//--Base
#include "TilemapActor.h"

//--Classes
#include "Pathrunner.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Local Definitions]
#define MINIMUM_WAIT_TICKS 15
#define ITERATIONS_PER_CYCLE 50

///--[Debug]
//#define TA_LEASH_DEBUG
#ifdef TA_LEASH_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///===================================== Worker Functions =========================================
void TilemapActor::BeginLeashing()
{
    ///--[Documentation and Setup]
    //--Begins leashing, resetting flags as required.
    mUseDumbLeash = false;
    mSpottedPlayerTicks = 0;
    mWaitTimer = 0;
    mAIState = TA_AI_LEASH;
}
void TilemapActor::FinishLeashing()
{
    ///--[Documentation and Setup]
    //--Resets all relevant flags and cleans up memory.
    delete mLeashInstructions;
    delete mLeashPathrunner;

    //--Reset.
    mAIState = TA_AI_WANDER;
    mMustRollWanderPoint = true;
    mLeashInstructions = NULL;
    mLeashPathrunner = NULL;
}

///===================================== Primary Function =========================================
void TilemapActor::HandleLeashAI()
{
    ///--[ ====== Documentation and Setup ===== ]
    //--Handles the process of the enemy walking back to where it started chasing the player from.
    DebugPush(true, "Actor %s runs leashing.\n", mLocalName);

    ///--[ =============== Pause ============== ]
    //--Wait a few ticks. During this time, run the walk pathing back to the leash point. We always wait a minimum
    //  of MINIMUM_WAIT_TICKS, but can wait longer if no path is ready.
    if(mWaitTimer < MINIMUM_WAIT_TICKS || (!mUseDumbLeash && !mLeashInstructions))
    {
        ///--[Timer]
        mWaitTimer ++;

        ///--[Dumb Leashing]
        //--Used if the pathrunner fails to build a path, the AI will blindly walk back to the leash point
        //  and not navigate around obstacles. It's possible for this to get stuck (in fact, it's likely).
        if(mUseDumbLeash)
        {
            DebugPop("Dumb leash is waiting.\n");
        }
        ///--[Pathed Leashing]
        else
        {
            //--If the leash Pathrunner isn't created yet, do that. Set it to home in on the leash point.
            if(!mLeashPathrunner)
            {
                //--Make the leash target a multiple of ADVP_WALK_DIST.
                int tXTarget = (int)((mLeashX / ADVP_WALK_DIST) * ADVP_WALK_DIST);
                int tYTarget = (int)((mLeashY / ADVP_WALK_DIST) * ADVP_WALK_DIST);
                DebugPrint("Leashing position %f %f\n", mLeashX, mLeashY);
                DebugPrint("Target position %i %i\n", tXTarget, tYTarget);

                //--Run.
                mLeashPathrunner = new Pathrunner();
                mLeashPathrunner->SetIterationsPerCycle(ITERATIONS_PER_CYCLE);
                mLeashPathrunner->Initialize(mTrueX, mTrueY, tXTarget, tYTarget);
            }

            //--Run the pathrunner. It can legally return NULL.
            StarLinkedList *tResultList = mLeashPathrunner->RunPathingIntoList();
            //if(mWaitTimer == 30) mLeashPathrunner->WritePathToDisk("Path.png");

            //--If the path runner failed for some reason, stop here. Switch to dumb wander pathing.
            if(mLeashPathrunner->IsFailed())
            {
                mUseDumbLeash = true;
                delete mLeashPathrunner;
                mLeashPathrunner = NULL;
                DebugPop("Pathrunner failed. Switching to dumb pathing.\n");
                return;
            }

            //--If the path runner didn't have a path yet, we can run it again until it does.
            if(!tResultList)
            {
                DebugPop("Pathrunner is incomplete. %i pulses remain.\n", mLeashPathrunner->GetPulsesRemaining());
                return;
            }

            //--If the path runner finished sooner than the minimum ticks, and has pulses remaining, let it
            //  keep running those pulses. This can result in better paths.
            if(mWaitTimer < MINIMUM_WAIT_TICKS && mLeashPathrunner->HasPulsesRemaining())
            {
                delete tResultList;
                DebugPop("Pathrunner finished too soon, improving pulse. %i pulses remain.\n", mLeashPathrunner->GetPulsesRemaining());
                return;
            }

            ///--[Success, Store]
            //--The path list was passed back successfully. Store it.
            delete mLeashInstructions;
            mLeashInstructions = tResultList;

            //--Clean up the path runner.
            delete mLeashPathrunner;
            mLeashPathrunner = NULL;

            //--Flags.
            mSmartLeashWalkDist = ADVP_WALK_DIST;
            DebugPop("Pathrunner finished normally.\n");
        }
        return;
    }

    ///--[ =========== Execute Path =========== ]
    //--The AI has waited at least the minimum ticks and a path is ready, or we're using dumb leashing. Either way,
    //  start moving the entity.

    ///--[Dumb Leashing]
    //--For dumb leashing, just try to walk in a straight line back to the leash point.
    if(mUseDumbLeash)
    {
        //--Ending check.
        if(HandleMoveTo(mLeashX, mLeashY, mMoveSpeed * 0.40f))
        {
            FinishLeashing();
        }
        DebugPop("AI finished dumb leashing.\n");
        return;
    }

    ///--[Pathed Leashing]
    //--Zeroth entry. If it doesn't exist, assume the move is complete.
    int *rDirection = (int *)mLeashInstructions->GetElementBySlot(0);
    if(!rDirection)
    {
        FinishLeashing();
        DebugPop("AI finished instructions, returning to wandering.\n");
        return;
    }

    //--Setup.
    float tTargetX = mTrueX;
    float tTargetY = mTrueY;

    //--Resolve the target position.
    if(*rDirection == DIR_NORTH) tTargetY = tTargetY - mSmartLeashWalkDist;
    if(*rDirection == DIR_SOUTH) tTargetY = tTargetY + mSmartLeashWalkDist;
    if(*rDirection == DIR_WEST)  tTargetX = tTargetX - mSmartLeashWalkDist;
    if(*rDirection == DIR_EAST)  tTargetX = tTargetX + mSmartLeashWalkDist;

    ///--[Movement Finished]
    //--Move to the target. If the move completes:
    int tStoreMove = mMoveTimer;
    if(HandleMoveTo(tTargetX, tTargetY, mMoveSpeed * 0.40f))
    {
        //--Debug.
        DebugPrint("Moving to target, move completes. Popping.\n");

        //--Pop the movement list.
        mLeashInstructions->DeleteHead();

        //--Reset the distance variable.
        mSmartLeashWalkDist = ADVP_WALK_DIST;

        //--If there are no instructions left, we reached the leash point.
        if(mLeashInstructions->GetListSize() < 1)
        {
            FinishLeashing();
            DebugPop("Finished leashing, returning to wander.\n");
            return;
        }
    }
    ///--[Movement Incomplete]
    //--Not at the location yet.
    else
    {
        //--Flags.
        mIsMoving = true;
        if(mMoveTimer != tStoreMove) mMoveTimer = tStoreMove + 1;

        //--Get the distance between our position and the target. Store that as the distance remaining.
        mSmartLeashWalkDist = GetPlanarDistance(mTrueX, mTrueY, tTargetX, tTargetY);

        //--Debug.
        DebugPrint("Moved normally, move incomplete.\n");
    }

    ///--[Debug]
    DebugPop("AI has unfinished leash instructions. Function exited.\n");



    ///--[Old Code]
    /*
    //--If a TDR is on the leash list's tail, pop it and move to that position.
    TwoDimensionReal *rTail = (TwoDimensionReal *)mLeashList->GetTail();
    if(rTail)
    {
        //--Ending check.
        if(HandleMoveTo(rTail->mLft, rTail->mTop, mMoveSpeed * 0.60f))
        {
            mLeashList->DeleteTail(); //Note: rTail is now deallocated!
        }

        //--If the list ran out of points, enter wander mode.
        if(mLeashList->GetListSize() < 1)
        {
            mAIState = TA_AI_WANDER;
            mMustRollWanderPoint = true;
        }
    }
    //--Something went wrong, so try to walk back to the leash point.
    else
    {
        //--Ending check.
        if(HandleMoveTo(mLeashX, mLeashY, mMoveSpeed * 0.40f))
        {
            mAIState = TA_AI_WANDER;
            mMustRollWanderPoint = true;
        }
        //--Not there yet.
        else
        {
        }
    }

    //--Store the true facing.
    mTrueFacing = mFacing;*/
}
