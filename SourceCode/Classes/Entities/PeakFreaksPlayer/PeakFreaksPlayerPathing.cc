//--Base
#include "PeakFreaksPlayer.h"

//--Classes
#include "PeakFreaksLevel.h"
#include "Pathrunner.h"
#include "PathrunnerTile.h"
#include "TileLayer.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define PFP_SMART_PATH_DEBUG
#ifdef PFP_SMART_PATH_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
///===================================== Property Queries =========================================
bool PeakFreaksPlayer::IsAIControlled()
{
    return mIsAIControlled;
}

///======================================= Manipulators ===========================================
void PeakFreaksPlayer::SetAIControl()
{
    mIsAIControlled = true;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksPlayer::HandleAIControlUpdate()
{
    ///--[Documentation]
    //--When the player is controlled by an AI, this update fires instead to decide what to do next.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--Stall ticks:
    if(mStallTicks > 0)
    {
        mStallTicks --;
        return;
    }

    //--Debug.
    DebugPush(true, "PFP Pather - Begin.\n");

    ///--[Path Exists, Use It]
    if(mAIInstructions)
    {
        HandleAIInstructions();
        DebugPop("PFP Pather - Completed. AI handled instructions.\n");
        return;
    }

    ///--[Pathing]
    //--If no pathing handler has run, do that now. Resolve a path that the AI will generally follow
    //  to reach the goal.
    if(!mPathRunner)
    {
        //--Debug.
        DebugPrint("Initializing path runner.\n");

        //--Get the next patrol node we're pathing to.
        ZonePack *rTarget = rActiveLevel->GetPlayerStart(2);
        if(!rTarget) return;
        int tTargetTileX = rTarget->mDim.mXCenter / TileLayer::cxSizePerTile;
        int tTargetTileY = rTarget->mDim.mYCenter / TileLayer::cxSizePerTile;

        //--Create the path runner.
        mPathRunner = new PathrunnerTile();
        mPathRunner->SetCollisionDepth(1);
        mPathRunner->Initialize(mTileX, mTileY, tTargetTileX, tTargetTileY);

        //--Reset flags.
        mPathTicksSoFar = 0;

        //--Debug.
        DebugPrint("Path runner ready.\n");
    }

    //--Run. The result can legally be NULL.
    DebugPrint("Running path into list.\n");
    SugarLinkedList *tResultList = mPathRunner->RunPathingIntoList();
    mPathTicksSoFar ++;

    ///--[Error Checking]
    //--If the path runner failed for some reason, stop here. Switch to dumb wander pathing.
    if(mPathRunner->IsFailed())
    {
        delete mPathRunner;
        mPathRunner = NULL;
        DebugPop("PFP Pather - Completed. Failed.\n");
        return;
    }

    //--If the path runner didn't have a path yet, we can run it again until it does.
    if(!tResultList)
    {
        DebugPop("PFP Pather - Completed. Results not in yet.\n");
        return;
    }

    //--If the path runner finished sooner than the minimum ticks, and has pulses remaining, let it
    //  keep running those pulses. This can result in better paths.
    if(mPathTicksSoFar < 120 && mPathRunner->HasPulsesRemaining())
    {
        delete tResultList;
        DebugPop("PFP Pather - Completed. Results in, pulses remaining, time not expired.\n");
        return;
    }

    ///--[Success, Store]
    //--The path list was passed back successfully. Store it.
    delete mAIInstructions;
    mAIInstructions = tResultList;

    //--Clean up the path runner.
    delete mPathRunner;
    mPathRunner = NULL;
    DebugPop("PFP Pather - Completed. Finished building AI list.\n");
}
void PeakFreaksPlayer::HandleAIInstructions()
{
    ///--[Documentation]
    //--If the AI list currently has instructions, move!
    if(!mAIInstructions) return;

    ///--[Currently Moving]
    //--Do nothing.
    if(mJumpingQueue->GetListSize() > 0 || mActiveJump) return;

    //--If this flag is set and there's no active jump, clear the instructions and force a path rebuild.
    if(mClearQueueWhenLanding)
    {
        mClearQueueWhenLanding = false;
        mJumpingQueue->ClearList();
        delete mAIInstructions;
        mAIInstructions = NULL;
        return;
    }

    ///--[Stationary. New Move.]
    //--Zeroth entry. If it doesn't exist, assume the move is complete.
    int *rDirection = (int *)mAIInstructions->GetElementBySlot(0);
    if(!rDirection)
    {
        delete mAIInstructions;
        mAIInstructions = NULL;
        return;
    }

    //--Debug.
    //fprintf(stderr, "AI is deciding action.\n");

    //--Always-wait. Every time the AI lands, they stop this many ticks at minimum. This
    //  creates a rough speed for each difficulty.
    if(mAIAlwaysStopTicksLeft > 0)
    {
        mAIAlwaysStopTicksLeft --;
        return;
    }

    //--Waiting around. The AI will sometimes wait for a few ticks instead of moving.
    if(mAIStopTicks > 0)
    {
        //fprintf(stderr, " Stop ticks: %i\n", mAIStopTicks);
        mAIStopTicks --;
        return;
    }

    //--AI stops moving:
    if(mAIStopMovesLeft < 1)
    {
        //--Set stop tick flag.
        mAIAlwaysStopTicksLeft = mAIAlwaysStopTicks;
        mAIStopTicks = 120;

        //--Re-roll stop ticks.
        if(mAIStopHi <= mAIStopLo)
        {
            mAIStopMovesLeft = mAIStopLo;
        }
        else
        {
            mAIStopMovesLeft = (rand() % (mAIStopHi - mAIStopLo)) + mAIStopLo;
        }

        //--Debug.
        //fprintf(stderr, " AI is stopping. Next stop in %i moves.\n", mAIStopMovesLeft);
        return;
    }
    //--AI is moving, decrement the stop counter.
    else
    {
        mAIStopMovesLeft --;
    }

    //--Double speed. Modifies the speed bar up for a few turns.
    if(mAIDoubleMoveSpeedLeft >= 0)
    {
        mAIDoubleMoveSpeedLeft --;
        SetSpeedBar(2);
        //fprintf(stderr, " Double speed!\n");
    }

    //--Reroll double speed. There is a flat chance each move to activate it.
    int tDoubleSpeedRoll = rand() % 100;
    if(tDoubleSpeedRoll <= mAIDoubleMoveSpeedChance) mAIDoubleMoveSpeedLeft = 3;

    //--Decrease speed.
    int tActualSpeed = GetActualSpeedBarLevel();
    SetSpeedBar(tActualSpeed - 1);

    //--Resolve the target position.
    if(mAIMistakeMovesLeft >= 1)
    {
        mAIMistakeMovesLeft --;
        if(*rDirection == DIR_NORTH) QueueJump( 0, -1);
        if(*rDirection == DIR_SOUTH) QueueJump( 0,  1);
        if(*rDirection == DIR_WEST)  QueueJump(-1,  0);
        if(*rDirection == DIR_EAST)  QueueJump( 1,  0);
    }
    //--Make a mistake, and move in a random direction.
    else
    {
        //--Reroll the mistake threshold.
        if(mAIMistakeLo == mAIMistakeHi)
        {
            mAIMistakeMovesLeft = mAIMistakeLo;
        }
        else
        {
            mAIMistakeMovesLeft = (rand() % (mAIMistakeHi - mAIMistakeLo)) + mAIMistakeLo;
        }

        //--Pick a random direction and jump in it.
        int tRoll = rand() % 4;
        if(tRoll == 0) QueueJump( 0, -1);
        if(tRoll == 1) QueueJump( 0,  1);
        if(tRoll == 2) QueueJump(-1,  0);
        if(tRoll == 3) QueueJump( 1,  0);

        //--Mark the instruction queue to be cleared when the character next lands.
        mClearQueueWhenLanding = true;

        //--Debug.
        //fprintf(stderr, " Rolled an incorrect move.");
        return;
    }

    //--Remove the instruction.
    mAIInstructions->RemoveElementI(0);

    //--Debug.
    //fprintf(stderr, " Normal action.\n");
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
