//--Base
#include "MonocerosDialogue.h"

//--Classes
#include "DialogueActor.h"
#include "DialogueTopic.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StringEntry.h"
#include "StarTranslation.h"

//--Definitions
#include "Global.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///--[Debug]
//#define MD_SAVE_DEBUG
#ifdef MD_SAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
MonocerosDialogue::MonocerosDialogue()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ========== RootObject ========== ]
    mType = POINTER_TYPE_MONOCEROSDIALOGUE;

    ///--[ ======== WorldDialogue ========= ]
    //--Constants
    cDialogueMaxWidth = 1255.0f;

    ///--[ ====== MonocerosDialogue ======= ]
    ///--[System]
    ///--[Text]
    mTextTopics = InitializeString("Topics");

    ///--[Images]
    memset(&MonoImg, 0, sizeof(MonoImg));
}
MonocerosDialogue::~MonocerosDialogue()
{
    free(mTextTopics);
}
void MonocerosDialogue::Construct()
{
    ///--[ ================ Construction ================ ]
    //--Call base class to resolve standard images.
    WorldDialogue::Construct();

    ///--[Setup]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Fonts]
    MonoImg.Data.rFontNameplate     = rDataLibrary->GetFont("Monoceros Dialogue Nameplate");
    MonoImg.Data.rFontMajorMainline = rDataLibrary->GetFont("Monoceros Dialogue Major Mainline");
    MonoImg.Data.rFontInfoMainline  = rDataLibrary->GetFont("Monoceros Dialogue Info Mainline");
    MonoImg.Data.rFontTopicHeader   = rDataLibrary->GetFont("Monoceros Dialogue Topic Header");
    MonoImg.Data.rFontTopic         = rDataLibrary->GetFont("Monoceros Dialogue Topics");

    ///--[Images]
    MonoImg.Data.rFrameDecision          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoDialogue/Base/Frame_Decision");
    MonoImg.Data.rFrameDialogue          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoDialogue/Base/Frame_Dialogue");
    MonoImg.Data.rFrameInfo              = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoDialogue/Base/Frame_Info");
    MonoImg.Data.rOverlayNameplate       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoDialogue/Base/Overlay_Nameplate");
    MonoImg.Data.rOverlayTopicShade      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoDialogue/Base/Overlay_TopicShade");
    MonoImg.Data.rOverlayTopicSelected   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoDialogue/Base/Overlay_TopicSelected");
    MonoImg.Data.rOverlayTopicUnselected = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoDialogue/Base/Overlay_TopicUnselected");

    ///--[Verify]
    MonoImg.mIsReady = VerifyStructure(&MonoImg.Data, sizeof(MonoImg.Data), sizeof(void *), false);

    ///--[Diagnostics]
    if(!MonoImg.mIsReady)
    {
        //--Heading.
        fprintf(stderr, "=== Mono Dialogue UI Images Failed to Resolve ===\n");

        //--Data.
        VerifyStructure(&MonoImg.Data, sizeof(MonoImg.Data), sizeof(void *), true);
    }

    ///--[Translation]
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_UI);
    if(rTranslation)
    {
        StarTranslation::StringTranslateWorker(mTextTopics, rTranslation);
    }
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
float MonocerosDialogue::ComputeLengthOf(StarLinkedList *pListOfCharacters)
{
    ///--[Documentation]
    //--Modified version of the base, uses the new fonts but is otherwise the same.
    if(!pListOfCharacters || !Images.mIsReady || !MonoImg.mIsReady) return 0.0f;

    //--Setup.
    float tRunningWidth = 0.0f;

    //--Iterate.
    DialogueCharacter *rCharacter = (DialogueCharacter *)pListOfCharacters->PushIterator();
    while(rCharacter)
    {
        //--Get the next character.
        DialogueCharacter *rNextCharacter = (DialogueCharacter *)pListOfCharacters->AutoIterate();

        //--Next character exists:
        if(rNextCharacter)
        {
            tRunningWidth = tRunningWidth + MonoImg.Data.rFontInfoMainline->GetKerningBetween(rCharacter->mLetter, rNextCharacter->mLetter);
        }
        //--Next character does not exist, pass a NULL for the next character.
        else
        {
            tRunningWidth = tRunningWidth + MonoImg.Data.rFontInfoMainline->GetKerningBetween(rCharacter->mLetter, '\0');
        }

        //--Iterate.
        rCharacter = rNextCharacter;
    }

    //--All done, return.
    return tRunningWidth;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void MonocerosDialogue::UpdateTopicsMode()
{
    //--Timer.
    mTopicTimer ++;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Pressing up decrements the topic cursor by 2.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Move.
        if(mTopicCursor >= 2)
        {
            mTopicCursor -= 2;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--If the cursor is close to the top of the scroll, move the scroll. Scroll moves in increments of 2.
        if(mTopicScroll > 0 && mTopicCursor < mTopicScroll + 4)
        {
            mTopicScroll -= 2;
        }
    }
    //--Down, scrolls positively by 2.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Move.
        if(mTopicCursor < mrCurrentTopicListing->GetListSize() - 2)
        {
            mTopicCursor += 2;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--If the cursor is close to the top of the scroll, move the scroll. Scroll moves in increments of 2.
        int tMaxTopicScroll = (mrCurrentTopicListing->GetListSize() - 8);
        if(tMaxTopicScroll > 0 && tMaxTopicScroll % 2 == 1) tMaxTopicScroll ++;
        if(mTopicScroll < tMaxTopicScroll && mTopicCursor >= mTopicScroll + 6)
        {
            mTopicScroll += 2;
        }
    }
    //--Left, if the index is odd decrements.
    else if(rControlManager->IsFirstPress("Left"))
    {
        if(mTopicCursor % 2 == 1)
        {
            mTopicCursor --;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Right, if the index is even increments.
    else if(rControlManager->IsFirstPress("Right"))
    {
        if(mTopicCursor % 2 == 0 && mTopicCursor < mrCurrentTopicListing->GetListSize() - 1)
        {
            mTopicCursor ++;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Activate will execute the given topic script.
    else if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get the topic in question.
        DialogueTopic *rSelectedTopic = (DialogueTopic *)mrCurrentTopicListing->GetElementBySlot(mTopicCursor);
        if(!rSelectedTopic) return;

        //--Normal case: Execute.
        if(rSelectedTopic != mGoodbyeTopic)
        {
            //--Flags.
            mLastTopicCursor = mTopicCursor;
            mIsTopicsMode = false;

            //--Auto-update the topic.
            rSelectedTopic->SetNPCLevel(mCurrentTopicActor, rSelectedTopic->GetLevel());

            //--Call this script. It will handle the rest.
            char *tPathBuf = InitializeString("%s/Topic Launcher.lua", xTopicsDirectory);
            Clear();
            LuaManager::Fetch()->ExecuteLuaFile(tPathBuf, 2, "S", mCurrentTopicActor, "S", rSelectedTopic->GetInternalName());
            free(tPathBuf);

            //--Build path.
            //char *tPathBuffer = InitializeString("%s/%s/100 %s.lua", xTopicsDirectory, mCurrentTopicActor, rSelectedTopic->GetInternalName());

            //--Run.
            //Clear();
            //LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer, 1, "S", "Hello");

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Clean.
            //free(tPathBuffer);
        }
        //--Goodbye case: Execute the goodbye text. The dialogue closes afterwards.
        else
        {
            //--Flags.
            mLastTopicCursor = 0;
            mIsTopicsMode = false;

            //--Call this script. It will handle the rest.
            char *tPathBuf = InitializeString("%s/Topic Launcher.lua", xTopicsDirectory);
            Clear();
            LuaManager::Fetch()->ExecuteLuaFile(tPathBuf, 2, "S", mCurrentTopicActor, "S", "Goodbye");
            free(tPathBuf);

            //--Build path.
            //char *tPathBuffer = InitializeString("%s/%s/100 %s.lua", xTopicsDirectory, mCurrentTopicActor, "Goodbye");

            //--Run.
            //Clear();
            //LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Clean.
            //free(tPathBuffer);
        }
    }
    //--Cancel will move the cursor to the "Goodbye" entry.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Set.
        mTopicCursor = mrCurrentTopicListing->GetListSize() - 1;
        if(mTopicCursor < 0) mTopicCursor = 0;

        //--Handle the topic scroll.
        mTopicScroll = (mrCurrentTopicListing->GetListSize() - 8);
        if(mTopicScroll % 2 == 1 && mTopicScroll >= 0) mTopicScroll ++;
        if(mTopicScroll < 0) mTopicScroll = 0;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonocerosDialogue::Render()
{
    //--Rendering cycle. Does nothing if the object is not visible in any way, or the images did not resolve.
    if(!Images.mIsReady || !MonoImg.mIsReady || mVisibilityState == WD_INVISIBLE) return;

    //--Debug.
    DebugPush(true, "WorldDialogue:Render - Begin.\n");

    //--Subsequences.
    if(mIsCreditsSequence)
    {
        RenderCredits();
        DebugPop("WorldDialogue:Render - Ended via credits.\n");
        return;
    }
    if(mIsMajorSequenceMode)
    {
        RenderMajorSequence();
        DebugPop("WorldDialogue:Render - Ended via major sequence.\n");
        return;
    }
    if(mIsSceneMode)
    {
        RenderScenesMode();
        DebugPop("WorldDialogue:Render - Ended via scenes.\n");
        return;
    }
    if(mIsTopicsMode && !mDecisionUsesMouse)
    {
        RenderTopicsMode();
        DebugPop("WorldDialogue:Render - Ended via topics.\n");
        return;
    }

    //--Mixing.
    float tAlphaFactor = 1.0f;
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaFactor);

    //--Positioning.
    float tCursorX = 0.0f;
    float tCursorY = 0.0f;

    //--If there is a speaker:
    bool tIsSpeakerCase = (mMajorSequenceSpeaker && strcasecmp(mMajorSequenceSpeaker, "Null") && strcasecmp(mMajorSequenceSpeaker, "Narrator") && strcasecmp(mMajorSequenceSpeaker, "Thought"));
    if(tIsSpeakerCase)
    {
        //--Render.
        MonoImg.Data.rFrameDialogue->Draw();
        MonoImg.Data.rOverlayNameplate->Draw();

        //--Name of speaker. Renders in a light purple.
        StarlightColor::SetMixer(0.85f, 0.75f, 0.87f, tAlphaFactor);
        MonoImg.Data.rFontNameplate->DrawText(141.0f, 478.0f, 0, 1.0f, mMajorSequenceSpeakerDisplay);

        //--Text color becomes black.
        mWhitePack.SetRGBAF(0.0f, 0.0f, 0.0f, tAlphaFactor);

        //--Cursor position.
        tCursorX =  46.0f;
        tCursorY = 543.0f;
    }
    //--No speaker.
    else
    {
        //--Render.
        MonoImg.Data.rFrameInfo->Draw();

        //--Text color becomes a light purple.
        mWhitePack.SetRGBAF(0.85f, 0.75f, 0.87f, 1.0f);

        //--Cursor position.
        tCursorX =  46.0f;
        tCursorY = 543.0f;
    }

    //--Subroutine renders the letters themselves.
    RenderOnlyText(tCursorX, tCursorY, tAlphaFactor);

    ///--[Decisions]
    //--Renders over everything else, but only when active.
    if(mIsDecisionMode && !mIsBlocking && mIsPrintingComplete) RenderDecisions();

    ///--[String Entry]
    if(mIsStringEntryMode && mStringEntryForm && mIsPrintingComplete) mStringEntryForm->Render();

    ///--[Flash]
    RenderFlash();
    DebugPop("WorldDialogue:Render - Ended normally.\n");
}
void MonocerosDialogue::RenderOnlyText(float pX, float pY, float pAlphaFactor)
{
    ///--[ ===================== Documentation ==================== ]
    //--Subroutine that only renders the text using the provided offsets. This is used both internally and for the
    //  TextLevel handlers since they use the same inputs.

    //--Reposition the rendering cursor for the dialogue.
    glTranslatef(pX, pY, 0.0f);

    ///--[ ================= Iterate Across Lines ================= ]
    //--Now start rendering.
    StarLinkedList *rLetterList = (StarLinkedList *)mDialogueList->PushIterator();
    while(rLetterList)
    {
        //--Offset value. Resets for each line.
        float tLineCursor = 0.0f;

        ///--[ ==== Iterate Across Letters ==== ]
        DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
        while(rCharacterPack)
        {
            ///--[Color Resolve]
            //--Spooky characters change color dynamically with a timer.
            if(rCharacterPack->mIsSpooky)
            {
                if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 2)
                {
                    StarlightColor::SetMixer(1.0f, 0.4f, 0.2f, rCharacterPack->mAlpha * pAlphaFactor);
                }
                else if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 4)
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 0.1f, rCharacterPack->mAlpha * pAlphaFactor);
                }
                else
                {
                    StarlightColor::SetMixer(1.0f, 0.2f, 0.5f, rCharacterPack->mAlpha * pAlphaFactor);
                }
            }
            //--Fixed color.
            else if(rCharacterPack->rColor)
            {
                StarlightColor::SetMixer(rCharacterPack->rColor->r, rCharacterPack->rColor->g, rCharacterPack->rColor->b, rCharacterPack->mAlpha * pAlphaFactor);
            }
            //--Light purple.
            else
            {
                StarlightColor::SetMixer(0.85f, 0.75f, 0.87f, rCharacterPack->mAlpha * pAlphaFactor);
            }

            ///--[Size/Position Resolve]
            //--Store original size.
            float tOrigScale = mTextScale;

            //--Text is "Large". This makes it render at double size, and doubles the advance rate.
            if(rCharacterPack->mIsLarge)
            {
                mTextScale = mTextScale * 2.0f;
            }
            //--Text is "Small". Renders at half size, halves advance rate.
            else if(rCharacterPack->mIsSmall)
            {
                mTextScale = mTextScale * 0.5f;
            }

            ///--[Rendering]
            //--Next letter. It may be NULL. This also does the work of iterating.
            DialogueCharacter *rNextLetterPack = (DialogueCharacter *)rLetterList->AutoIterate();

            //--Render the letter.
            char tNextLetter = '\0';
            if(rNextLetterPack) tNextLetter = rNextLetterPack->mLetter;
            float tAdvance = MonoImg.Data.rFontInfoMainline->DrawLetter(tLineCursor, 0.0f, 0, mTextScale, rCharacterPack->mLetter, tNextLetter);
            tAdvance = MonoImg.Data.rFontInfoMainline->GetKerningBetween(rCharacterPack->mLetter, tNextLetter);

            ///--[Next]
            //--Move the cursor over.
            tLineCursor = tLineCursor + (tAdvance * mTextScale);

            //--Reset the scale.
            mTextScale = tOrigScale;

            //--Get the next letter.
            rCharacterPack = rNextLetterPack;
        }

        //--Move the cursor down.
        pY = pY + 44.0f;
        glTranslatef(0.0f, 44.0f, 0.0f);

        //--Next line.
        rLetterList = (StarLinkedList *)mDialogueList->AutoIterate();
    }

    //--Clean up.
    StarlightColor::ClearMixer();
    glTranslatef(pX * -1, pY * -1, 0.0f);
}

///====================================== Major Sequence ==========================================
void MonocerosDialogue::RenderMajorSequence()
{
    ///--[ ===================== Documentation ==================== ]
    //--Rendering routine during a major dialogue sequence, which usually consists of multiple characters
    //  speaking for multiple lines.
    if(!Images.mIsReady) return;

    ///--[Compute Alphas]
    float tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    if(tAlphaFactor < 0.0f) tAlphaFactor = 0.0f;
    if(tAlphaFactor > 1.0f) tAlphaFactor = 1.0f;

    ///--[Backing and Widescreen]
    //--Render a black border under everything else. This makes portraits pop out of the screen better.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.1f, 0.1f, 0.1f, 0.80f * tAlphaFactor));

    ///--[Portraits]
    //--Render the portraits with the subroutine.
    if(!mIsVisualNovel)
    {
        glTranslatef(0.0f, -60.0f, 0.0f);
        RenderDialogueActors(tAlphaFactor);
        glTranslatef(0.0f, 60.0f, 0.0f);
    }
    //--Visual novel version.
    else
    {
        RenderVisualNovelActors(tAlphaFactor);
    }

    ///--[ ==================== Dialogue Render =================== ]
    //--Positioning.
    float tCursorX =  46.0f;
    float tCursorY = 543.0f;

    //--Color mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaFactor);

    //--Text color becomes is black.
    mWhitePack.SetRGBAF(0.0f, 0.0f, 0.0f, 1.0f);

    ///--[Speaker's Name]
    //--If there is a speaker:
    bool tIsLeftSpeaker = false;
    bool tIsSpeakerCase = (mMajorSequenceSpeaker && strcasecmp(mMajorSequenceSpeaker, "Null") && strcasecmp(mMajorSequenceSpeaker, "Narrator") && strcasecmp(mMajorSequenceSpeaker, "Thought"));
    if(tIsSpeakerCase)
    {
        //--Find which side the speaker is on.
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            //--Empty slot. Skip.
            if(!mDialogueActors[i]) continue;

            //--Check aliases.
            if(mDialogueActors[i]->IsSpeaking(mMajorSequenceSpeaker))
            {
                tIsLeftSpeaker = (i < 3) || (i == 6);
                break;
            }
        }

        //--Left-sided speaker:
        if(tIsLeftSpeaker)
        {
            MonoImg.Data.rFrameDialogue->Draw();
            MonoImg.Data.rOverlayNameplate->Draw();
            StarlightColor::SetMixer(0.85f, 0.75f, 0.87f, tAlphaFactor);
            MonoImg.Data.rFontNameplate->DrawText(141.0f, 478.0f, 0, 1.0f, mMajorSequenceSpeakerDisplay);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaFactor);
        }
        //--Right-sided speaker:
        else
        {
            MonoImg.Data.rFrameDialogue->Draw();
            MonoImg.Data.rOverlayNameplate->Draw(742.0f, 0.0f);
            StarlightColor::SetMixer(0.85f, 0.75f, 0.87f, tAlphaFactor);
            MonoImg.Data.rFontNameplate->DrawText(883.0f, 478.0f, 0, 1.0f, mMajorSequenceSpeakerDisplay);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaFactor);
        }
    }
    //--No speaker.
    else
    {
        MonoImg.Data.rFrameDialogue->Draw();
    }

    ///--[ =================== Rendering Cycle ==================== ]
    //--Move the rendering cursor.
    glTranslatef(tCursorX, tCursorY, 0.0f);

    ///--[ ======= Iterate Across Lines ======= ]
    //--Now start rendering.
    StarLinkedList *rLetterList = (StarLinkedList *)mDialogueList->PushIterator();
    while(rLetterList)
    {
        //--Offset value. Resets for each line.
        float tLineCursor = 0.0f;

        ///--[ ==== Iterate Across Letters ==== ]
        DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
        while(rCharacterPack)
        {
            ///--[Color Resolve]
            //--Spooky characters change color.
            if(rCharacterPack->mIsSpooky)
            {
                if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 2)
                {
                    StarlightColor::SetMixer(1.0f, 0.4f, 0.2f, rCharacterPack->mAlpha * tAlphaFactor);
                }
                else if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 4)
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 0.1f, rCharacterPack->mAlpha * tAlphaFactor);
                }
                else
                {
                    StarlightColor::SetMixer(1.0f, 0.2f, 0.5f, rCharacterPack->mAlpha * tAlphaFactor);
                }
            }
            //--Coloring.
            else if(rCharacterPack->rColor)
            {
                StarlightColor::SetMixer(rCharacterPack->rColor->r, rCharacterPack->rColor->g, rCharacterPack->rColor->b, rCharacterPack->mAlpha * tAlphaFactor);
            }
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, rCharacterPack->mAlpha * tAlphaFactor);
            }

            ///--[Size/Position Resolve]
            //--Store original size.
            float tOrigScale = mTextScale;

            //--Text is "Large". This makes it render at double size, and doubles the advance rate.
            if(rCharacterPack->mIsLarge)
            {
                mTextScale = mTextScale * 2.0f;
            }
            //--Text is "Small". Renders at half size, halves advance rate.
            else if(rCharacterPack->mIsSmall)
            {
                mTextScale = mTextScale * 0.5f;
            }

            ///--[Rendering]
            //--Next letter. It may be NULL. This also does the work of iterating.
            DialogueCharacter *rNextLetterPack = (DialogueCharacter *)rLetterList->AutoIterate();

            //--Render the letter.
            char tNextLetter = '\0';
            if(rNextLetterPack) tNextLetter = rNextLetterPack->mLetter;
            float tAdvance = MonoImg.Data.rFontInfoMainline->DrawLetter(tLineCursor, 0.0f, 0, mTextScale, rCharacterPack->mLetter, tNextLetter);
            tAdvance = MonoImg.Data.rFontInfoMainline->GetKerningBetween(rCharacterPack->mLetter, tNextLetter);

            ///--[Next]
            //--Move the cursor over.
            tLineCursor = tLineCursor + (tAdvance * mTextScale);

            //--Reset the scale.
            mTextScale = tOrigScale;

            //--Get the next letter.
            rCharacterPack = rNextLetterPack;
        }

        //--Move the cursor down.
        tCursorY = tCursorY + 44.0f;
        glTranslatef(0.0f, 44.0f, 0.0f);

        //--Next line.
        rLetterList = (StarLinkedList *)mDialogueList->AutoIterate();
    }

    ///--[Clean Up]
    //--Reset mixer and position.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
    glTranslatef(tCursorX * -1, tCursorY * -1, 0.0f);

    ///--[ =================== Other UI Pieces ==================== ]
    ///--[Decisions]
    //--Renders over everything else, but only when active.
    if(mIsDecisionMode && !mIsBlocking && mIsPrintingComplete) RenderDecisions();

    ///--[Topics]
    //--When using the mouse, topics renders over the characters.
    if(mIsTopicsMode && mDecisionUsesMouse)
    {
        RenderTopicsMode();
    }

    ///--[String Entry]
    if(mIsStringEntryMode && mStringEntryForm && mIsPrintingComplete) mStringEntryForm->Render();

    ///--[Final Clean]
    StarlightColor::ClearMixer();

    ///--[Flash]
    RenderFlash();
}

///========================================= Decisions ============================================
void MonocerosDialogue::RenderDecisions()
{
    ///--[Documentation and Setup]
    //--Renders the decisions available to the player in the bottom left near the dialogue pane.
    if(!Images.mIsReady || !MonoImg.mIsReady) return;

    //--Compute alphas.
    float tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    if(tAlphaFactor < 0.0f) tAlphaFactor = 0.0f;
    if(tAlphaFactor > 1.0f) tAlphaFactor = 1.0f;

    //--Constants.
    float cTextSize  = 1.0f;
    float cTextHei   = MonoImg.Data.rFontMajorMainline->GetTextHeight() * cTextSize;
    float cCursorWid = 8.0f;

    //--Iterate across the decisions and figure out how wide the pane should be.
    float tWidestDecision = 0.0f;
    DecisionPack *rPack = (DecisionPack *)mDecisionList->PushIterator();
    while(rPack)
    {
        //--Get and compare length.
        float tLength = (MonoImg.Data.rFontMajorMainline->GetTextWidth(rPack->mText) * cTextSize);
        if(tLength > tWidestDecision) tWidestDecision = tLength;

        //--Next.
        rPack = (DecisionPack *)mDecisionList->AutoIterate();
    }

    //--Increase the width to accomodate the cursor.
    float cXIndent = 24.0f;
    float cXExdent = 30.0f;
    float cYIndent =  8.0f;
    float cYExdent = 17.0f;
    tWidestDecision = tWidestDecision + cCursorWid + cXIndent + cXExdent;

    //--Compute the lft/top position of the backing. Top position needs to move up as more decisions exist.
    float cTextBoxTop = 470.0f;
    float cLftPos =  20.0f;
    float cTopPos = cTextBoxTop - (mDecisionList->GetListSize() * cTextHei) - cYIndent - cYExdent;

    //--Figure out the sizing.
    TwoDimensionReal tDimensions;
    tDimensions.Set(cLftPos, cTopPos, cLftPos + (tWidestDecision), cTextBoxTop);

    //--Texture sizings.
    float cTxW = 0.333333f;
    float cTxH = 0.333333f;
    float cWid = 30.0f;
    float cHei = 30.0f;

    //--Border card.
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
    MonoImg.Data.rFrameDecision->Bind();
    glBegin(GL_QUADS);

        ///--[Top Vertically]
        //--Top left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 0.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 0.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));

        //--Top bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 0.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 0.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));

        //--Top right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 0.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 0.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));

        ///--[Middle Vertically]
        //--Middle left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));

        //--Middle bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));

        //--Middle right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));

        ///--[Bottom Vertically]
        //--Bottom left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 3.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 3.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mBot - (cHei * 0.0f));

        //--Bottom bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 3.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 3.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));

        //--Bottom right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 3.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 3.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));

    glEnd();

    ///--[Decision Rendering]
    //--Render the text options.
    int i = 0;
    rPack = (DecisionPack *)mDecisionList->PushIterator();
    while(rPack)
    {
        //--Render base text.
        MonoImg.Data.rFontMajorMainline->DrawText(tDimensions.mLft + cCursorWid + cXIndent, tDimensions.mTop + (cTextHei * i) + cYIndent, 0, cTextSize, rPack->mText);

        //--Next.
        i ++;
        rPack = (DecisionPack *)mDecisionList->AutoIterate();
    }

    //--Render the cursor.
    cCursorWid = 5.0f;
    float cCursorHei = (cTextHei * 0.80f) - 2.0f;
    float tTopicCursorX = tDimensions.mLft + cXIndent;
    float tTopicCursorY = tDimensions.mTop + (cTextHei * mHighlightedDecision) + 12.0f + cYIndent;

    //--Render.
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLES);
        glVertex2f(tTopicCursorX,              tTopicCursorY);
        glVertex2f(tTopicCursorX + cCursorWid, tTopicCursorY + cCursorHei / 2.0f);
        glVertex2f(tTopicCursorX,              tTopicCursorY + cCursorHei);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}

///=========================================== Topics =============================================
void MonocerosDialogue::RenderTopicsMode()
{
    ///--[Documentation]
    //--Rendering routine for topics mode. Shares a lot of code with Major Sequences since portraits are expected
    //  to be shown during a topic sequence. Does not have the top dialogue box, though.
    if(!Images.mIsReady || !MonoImg.mIsReady) return;

    ///--[Mixing]
    float tAlphaFactor = 1.0f;
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);

    ///--[Backing and Widescreen]
    //--Base borders.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = VIRTUAL_CANVAS_X;
    float cBot = VIRTUAL_CANVAS_Y;

    //--Render a black border under everything else. This makes portraits pop out of the screen better.
    glColor4f(0.1f, 0.1f, 0.1f, 0.80f * tAlphaFactor);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);

    ///--[Portraits]
    //--Render the portraits with the subroutine.
    glTranslatef(0.0f, -60.0f, 0.0f);
    RenderDialogueActors(1.0f);
    glTranslatef(0.0f, 60.0f, 0.0f);

    ///--[Backing]
    //--Backing image
    MonoImg.Data.rFrameInfo->Draw();

    ///--[Topic Listing]
    //--Constants.
    float cTextHeight = 53.0f;

    //--Positioning.
    float cCursorIndent = 12.0f;

    //--Render the topics.
    int i = 0;
    int tActualRenders = 0;
    DialogueTopic *rTopic = (DialogueTopic *)mrCurrentTopicListing->PushIterator();
    while(rTopic)
    {
        //--Value is less than the topic scroll, so don't render it.
        if(i < mTopicScroll)
        {
            i ++;
            rTopic = (DialogueTopic *)mrCurrentTopicListing->AutoIterate();
            continue;
        }

        //--Stop rendering if 8 topics render.
        if(tActualRenders >= 8)
        {
            mrCurrentTopicListing->PopIterator();
            break;
        }

        //--Compute position.
        float tPosX =  64.0f + ((i-mTopicScroll) % 2) * 637.0f;
        float tPosY = 522.0f + ((i-mTopicScroll) / 2) * (cTextHeight);

        //--Compute box position.
        float tBoxPosX = 32.0f;
        float tBoxPosY = 528 + (((i-mTopicScroll) / 2) * cTextHeight);
        if(((i-mTopicScroll) % 2) == 1)
        {
            tBoxPosX = 669.0f;
        }

        //--Box.
        if(mTopicCursor == i)
        {
            MonoImg.Data.rOverlayTopicSelected->Draw(tBoxPosX -11.0f, tBoxPosY);
        }
        else
        {
            MonoImg.Data.rOverlayTopicUnselected->Draw(tBoxPosX, tBoxPosY);
        }

        //--If this dialogue has been seen before, nothing but its name gets rendered.
        if(mrCurrentTopicListing->GetIteratorName()[0] == 'Y')
        {
            glColor4f(0.85f, 0.8f, 0.6f, 1.0f * tAlphaFactor);
        }
        //--Otherwise, render an indicator that there's something new by chaning the text to pink.
        else
        {
            glColor4f(0.9f, 0.0f, 0.9f, 1.0f * tAlphaFactor);
        }

        //--Render the topic, indented slightly for the cursor.
        MonoImg.Data.rFontTopic->DrawTextArgs(tPosX + cCursorIndent, tPosY, 0, mTextScale, "%s", rTopic->GetDisplayName());

        //--Next.
        i ++;
        tActualRenders ++;
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
        rTopic = (DialogueTopic *)mrCurrentTopicListing->AutoIterate();
    }

    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);

    ///--[Topic Header]
    MonoImg.Data.rOverlayTopicShade->Draw();
    glColor4f(0.85f, 0.8f, 0.6f, 1.0f * tAlphaFactor);
    MonoImg.Data.rFontTopicHeader->DrawText(120.0f, 450.0f, 0, 1.0f, mTextTopics);
    glColor3f(1.0f, 1.0f, 1.0f);

    ///--[Flash]
    RenderFlash();
}

///======================================== Scenes Mode ===========================================
void MonocerosDialogue::RenderScenesMode()
{
    ///--[Documentation]
    //--Scenes mode is different from the other modes in that it shows an entity in the middle of the screen,
    //  based on which one is currently showing. All the other rules of dialogue are the same.
    DebugPush(true, "MonocerosDialogue:Render Scenes - Begin.\n");

    ///--[Compute Alphas]
    //--Sequence fade.
    float tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(tAlphaFactor < 0.0f) tAlphaFactor = 0.0f;
    if(tAlphaFactor > 1.0f) tAlphaFactor = 1.0f;

    //--Hiding fade.
    float tHidingAlpha = 1.0f;
    if(mHidingTimer != -1) tHidingAlpha = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);

    ///--[Backing and Widescreen]
    //--Base borders.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = VIRTUAL_CANVAS_X;
    float cBot = VIRTUAL_CANVAS_Y;

    //--Render a black border under everything else. This makes portraits pop out of the screen better.
    glColor4f(0.1f, 0.1f, 0.1f, 0.65f * tAlphaFactor * tHidingAlpha);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor * tHidingAlpha);

    //--Debug.
    DebugPrint("Finished computations.\n");

    ///--[Entity]
    //--No crossfading.
    if(mSceneTransitionTimer >= mSceneFadeTimeMax)
    {
        //--Debug.
        DebugPrint("Begun no-crossfade mode.\n");

        //--If the image has been set, render it. Add the offsets. The image is expected to be centered.
        RenderSceneImage(rSceneImg);

        //--Censor bars.
        RenderCensorBars(mCensorBarList);
    }
    ///--[Crossfading]
    else
    {
        //--Debug.
        DebugPrint("Begun crossfade mode %p\n", rOldSceneImg);

        //--Compute alphas.
        float cFadeOutAlpha = (1.0f - EasingFunction::QuadraticInOut(mSceneTransitionTimer, mSceneFadeTimeMax));
        float cFadeInAlpha = 1.6f - cFadeOutAlpha;

        //--Previous scene image. May not exist.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cFadeOutAlpha);
        RenderSceneImage(rOldSceneImg);
        StarlightColor::ClearMixer();
        RenderCensorBars(mOldCensorBarList);

        //--Debug.
        DebugPrint("Rendering scene image %p\n", rSceneImg);

        //--If the image has been set, render it. Add the offsets. The image is expected to be centered.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cFadeInAlpha);
        RenderSceneImage(rSceneImg);
        StarlightColor::ClearMixer();
        RenderCensorBars(mCensorBarList);

        //--Clean.
        StarlightColor::ClearMixer();
    }

    //--Debug.
    DebugPrint("Render dialogue box.\n");

    //--Dialogue box.
    if(!mHideDialogueBoxes)
    {
        //--Positioning.
        float tCursorX =  46.0f;
        float tCursorY = 543.0f;

        //--Render.
        MonoImg.Data.rFrameDialogue->Draw();

        //--Reposition the rendering cursor for the dialogue.
        glTranslatef(tCursorX, tCursorY, 0.0f);

        //--Now start rendering.
        StarLinkedList *rLetterList = (StarLinkedList *)mDialogueList->PushIterator();
        while(rLetterList)
        {
            //--Offset value. Resets for each line.
            float tLineCursor = 0.0f;

            //--Iterate.
            DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
            while(rCharacterPack)
            {
                //--Spooky characters change color.
                if(rCharacterPack->mIsSpooky)
                {
                    if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 2)
                    {
                        StarlightColor::SetMixer(1.0f, 0.4f, 0.2f, rCharacterPack->mAlpha * tAlphaFactor * tHidingAlpha);
                    }
                    else if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 4)
                    {
                        StarlightColor::SetMixer(1.0f, 1.0f, 0.1f, rCharacterPack->mAlpha * tAlphaFactor * tHidingAlpha);
                    }
                    else
                    {
                        StarlightColor::SetMixer(1.0f, 0.2f, 0.5f, rCharacterPack->mAlpha * tAlphaFactor * tHidingAlpha);
                    }
                }
                //--Coloring.
                else if(rCharacterPack->rColor)
                {
                    StarlightColor::SetMixer(rCharacterPack->rColor->r, rCharacterPack->rColor->g, rCharacterPack->rColor->b, rCharacterPack->mAlpha * tAlphaFactor * tHidingAlpha);
                }
                else
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, rCharacterPack->mAlpha * tAlphaFactor * tHidingAlpha);
                }

                //--Next letter. It may be NULL. This also does the work of iterating.
                DialogueCharacter *rNextLetterPack = (DialogueCharacter *)rLetterList->AutoIterate();

                //--Render the letter.
                char tNextLetter = '\0';
                if(rNextLetterPack) tNextLetter = rNextLetterPack->mLetter;
                float tAdvance = MonoImg.Data.rFontInfoMainline->DrawLetter(tLineCursor, 0.0f, 0, mTextScale, rCharacterPack->mLetter, tNextLetter);
                tAdvance = MonoImg.Data.rFontInfoMainline->GetKerningBetween(rCharacterPack->mLetter, tNextLetter);

                //--Move the cursor over.
                tLineCursor = tLineCursor + (tAdvance * mTextScale);
                rCharacterPack = rNextLetterPack;
            }

            //--Move the cursor down.
            tCursorY = tCursorY + (MonoImg.Data.rFontInfoMainline->GetTextHeight() * mTextScale);
            glTranslatef(0.0f, (MonoImg.Data.rFontInfoMainline->GetTextHeight() * mTextScale), 0.0f);

            //--Next line.
            rLetterList = (StarLinkedList *)mDialogueList->AutoIterate();
        }

        //--Clean up.
        StarlightColor::ClearMixer();
        glTranslatef(tCursorX * -1, tCursorY * -1, 0.0f);
    }

    ///--[Flash]
    DebugPrint("Begun flash.\n");
    RenderFlash();

    ///--[Animation]
    //--If there's an animation, render that over the entity. It's still centered and uses the same offset.
    if(mSceneAnimFramesTotal > 0 && mSceneTimer >= 0)
    {
        //--Debug.
        DebugPrint("Begun animations.\n");

        //--Compute the frame.
        int tFrame = mSceneTimer / mSceneTicksPerFrame;
        if(tFrame >= mSceneAnimFramesTotal) tFrame = mSceneAnimFramesTotal - 1;

        //--Get the image. It can be NULL, in which case don't render it.
        StarBitmap *rRenderImage = mrSceneAnimFrames[tFrame];
        if(rRenderImage)
        {
            //--Centering info.
            float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rRenderImage->GetWidth() * 0.5f)  + mSceneOffsetX;
            float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rRenderImage->GetHeight() * 0.5f) + mSceneOffsetY;

            //--Render.
            rRenderImage->Draw(tXPos, tYPos);
        }
    }

    //--Debug.
    DebugPop("WorldDialogue:Render Scenes - Ended normally.\n");
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
