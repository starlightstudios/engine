///================================== PeakFreaksLoadInterrupt ======================================
//--Load Interrupt replacement for Peak Freaks. Replaces the global instance on program boot, but is
//  otherwise identical to the base Load Interrupt in most ways.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "LoadInterrupt.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class PeakFreaksLoadInterrupt : public LoadInterrupt
{
    private:
    ///--[System]
    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            SugarBitmap *mLoading;
            SugarBitmap *mAllen;
            SugarBitmap *mMimi;
            SugarBitmap *mScooter;
            SugarBitmap *mPlayer;
        }Data;
    }PFImages;

    protected:

    public:
    //--System
    PeakFreaksLoadInterrupt();
    virtual ~PeakFreaksLoadInterrupt();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void DeallocateImages();
    void Initialize(const char *pDatafilePath);
    static void Interrupt();

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void RenderCall(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PFLI_Boot(lua_State *L);


