//--Base
#include "StarlightString.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DebugManager.h"

///--[Debug]
//#define STARLIGHT_STRING_DEBUG
#ifdef STARLIGHT_STRING_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
StarlightString::StarlightString()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_STARLIGHTSTRING;

    //--[StarlightString]
    //--Breakpoints
    mBreakpointList = new StarLinkedList(true);

    //--Images
    mImagesTotal = 0;
    mYOffsets = NULL;
    mImgScales = NULL;
    mrImages = NULL;

    //--Colors
    mTextColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mImageColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
}
StarlightString::~StarlightString()
{
    delete mBreakpointList;
    free(mYOffsets);
    free(mImgScales);
    free(mrImages);
}

///===================================== Property Queries =========================================
float StarlightString::GetLength(StarFont *pFont)
{
    //--Error check.
    if(!pFont) return 1.0f;

    //--Setup.
    float tLongestPosition = 0.0f;
    float tCurrentPosition = 0.0f;

    //--Iterate.
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        //--Normal case:
        if(!rBreakpoint->mIsReposition)
        {
            if(rBreakpoint->mString)
            {
                tCurrentPosition = tCurrentPosition + pFont->GetTextWidth(rBreakpoint->mString);
                if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
            }
            if(rBreakpoint->rImage)
            {
                tCurrentPosition = tCurrentPosition + rBreakpoint->rImage->GetTrueWidth();
                tCurrentPosition = tCurrentPosition + STARLIGHT_STRING_BUFFER_PIXELS;
                if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
            }
        }
        //--Repositioning breakpoint. Note that this can move the position *backwards*.
        else
        {
            tCurrentPosition = rBreakpoint->mRepositionX;
            if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
        }

        //--Next breakpoint.
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    //--Done.
    return tLongestPosition;
}
int StarlightString::GetTotalChars()
{
    //--Setup.
    int tTotalChars = 0;

    //--Iterate.
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        if(rBreakpoint->mString) tTotalChars = tTotalChars + (int)strlen(rBreakpoint->mString);
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    //--Done.
    return tTotalChars;
}

///======================================== Manipulators ==========================================
void StarlightString::AllocateImages(int pTotal)
{
    //--Deallocate.
    free(mYOffsets);
    free(mImgScales);
    free(mrImages);

    //--Initialize;
    mImagesTotal = 0;
    mYOffsets = NULL;
    mImgScales = NULL;
    mrImages = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mImagesTotal = pTotal;
    mYOffsets = (float *)starmemoryalloc(sizeof(float) * mImagesTotal);
    mImgScales = (float *)starmemoryalloc(sizeof(float) * mImagesTotal);
    mrImages = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mImagesTotal);
    for(int i = 0; i < mImagesTotal; i ++)
    {
        mYOffsets[i] = 0.0f;
        mImgScales[i] = 1.0f;
        mrImages[i] = NULL;
    }
}
void StarlightString::SetImageS(int pSlot, float pYOffset, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mImagesTotal) return;
    mYOffsets[pSlot] = pYOffset;
    mrImages[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void StarlightString::SetImageP(int pSlot, float pYOffset, StarBitmap *pImage)
{
    if(pSlot < 0 || pSlot >= mImagesTotal) return;
    mYOffsets[pSlot] = pYOffset;
    mrImages[pSlot] = pImage;
}
void StarlightString::SetImageScale(int pSlot, float pScale)
{
    if(pSlot < 0 || pSlot >= mImagesTotal) return;
    mImgScales[pSlot] = pScale;
}
void StarlightString::SetTextColor(StarlightColor pColor)
{
    memcpy(&mTextColor, &pColor, sizeof(StarlightColor));
}
void StarlightString::SetImageColor(StarlightColor pColor)
{
    memcpy(&mImageColor, &pColor, sizeof(StarlightColor));
}

///======================================== Core Methods ==========================================
void StarlightString::AutoSetControlString(const char *pString, int pImages, ...)
{
    ///--[Documentation and Setup]
    //--A one-and-done setting of the string, mandating how many images there are and always resolving them
    //  from the ControlManager. This is used for a lot of help strings.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Initialize]
    //--Clear. If this is zero or a one-null string, place an "Empty" package.
    mBreakpointList->ClearList();
    if(!pString || strlen(pString) < 1)
    {
        StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
        nPack->Initialize();
        ResetString(nPack->mString, " ");
        mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);
        return;
    }

    ///--[Call Base]
    //--Calls SetString() to handle the string itself.
    SetString(pString);

    ///--[Image Handling]
    //--Allocate.
    AllocateImages(pImages);

    //--For each expected image, get a string from the va_arg listing. This is the name of a control.
    va_list tArgList;
    va_start(tArgList, pImages);
    for(int i = 0; i < pImages; i ++)
    {
        const char *rControlName = va_arg(tArgList, const char *);
        SetImageP(i, 3.0f, rControlManager->ResolveControlImage(rControlName));
    }

    //--Crossreference images once all are set.
    CrossreferenceImages();

    //--Clean up.
    va_end(tArgList);
}
void StarlightString::SetString(const char *pString, ...)
{
    ///--[Documentation and Setup]
    //--Sets the internals of the string. The string is parsed individually.
    SetMemoryData(__FILE__, __LINE__);

    ///--[Initialize]
    //--Clear. If this is zero or a one-null string, place an "Empty" package.
    mBreakpointList->ClearList();
    if(!pString || strlen(pString) < 1)
    {
        StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
        nPack->Initialize();
        ResetString(nPack->mString, " ");
        mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);
        return;
    }

    ///--[String Resolve]
    //--Get the variable arg list.
    va_list tArgList;
    va_start(tArgList, pString);

    //--Print the args into a buffer.
    char tBuffer[STARLIGHT_STRING_BUFFER_LETTERS];
    memset(tBuffer, 0, sizeof(char) * STARLIGHT_STRING_BUFFER_LETTERS);
    int tLetters = vsprintf(tBuffer, pString, tArgList);
    va_end(tArgList);

    //--Warning:
    if(tLetters >= STARLIGHT_STRING_BUFFER_LETTERS) fprintf(stderr, "== Warning, StarlightString vsprintf() printed %i letters, buffer was %i.\n", tLetters, STARLIGHT_STRING_BUFFER_LETTERS);

    ///--[First Pass: Break Up]
    //--Scan through the string and break it into pieces. Each piece contains a string and is ended with an image.
    bool tLettersRemaining = true;
    int tStart = 0;
    int tEnd = 0;
    int cLen = (int)strlen(tBuffer);
    for(int i = 0; i < cLen; i ++)
    {
        //--Found a match.
        if(!strncmp(&tBuffer[i], "[IMG", 4))
        {
            //--Create and register new breakpoint.
            StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
            nPack->Initialize();
            mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);

            //--Store the substring segment.
            tEnd = i;
            int cLetters = (tEnd - tStart) + 1;

            //--Zero-length string:
            if(cLetters <= 1)
            {
                nPack->mString = (char *)starmemoryalloc(sizeof(char) * 1);
                nPack->mString[0] = '\0';
            }
            //--Non-zero-length string:
            else
            {
                nPack->mString = (char *)starmemoryalloc(sizeof(char) * cLetters);
                strncpy(nPack->mString, &tBuffer[tStart], cLetters);
                nPack->mString[cLetters-1] = '\0';
            }

            //--Scan into a buffer until a ']' is reached.
            int n = 0;
            char tNumBuf[32];
            tNumBuf[0] = '\0';
            for(int p = i+4; p < cLen; p ++)
            {
                //--Stop on a ']'
                if(tBuffer[p] == ']')
                {
                    tStart = p+1;
                    i = p;
                    if(tStart == cLen) tLettersRemaining = false;
                    break;
                }
                //--Copy across.
                else
                {
                    tNumBuf[n+0] = tBuffer[p];
                    tNumBuf[n+1] = '\0';
                    n ++;
                }
            }

            //--Resolve the number.
            nPack->mIndex = atoi(tNumBuf);
        }
        //--This is a movement breakpoint. It moves the rendering cursor.
        else if(!strncmp(&tBuffer[i], "[MOV", 4))
        {
            ///--[Leadup]
            //--Create a breakpoint for all the text that is up to this point.
            StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
            nPack->Initialize();
            mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);

            //--Store the substring segment.
            tEnd = i;
            int cLetters = (tEnd - tStart) + 1;

            //--Zero-length string:
            if(cLetters <= 1)
            {
                nPack->mString = (char *)starmemoryalloc(sizeof(char) * 1);
                nPack->mString[0] = '\0';
            }
            //--Non-zero-length string:
            else
            {
                nPack->mString = (char *)starmemoryalloc(sizeof(char) * cLetters);
                strncpy(nPack->mString, &tBuffer[tStart], cLetters);
                nPack->mString[cLetters-1] = '\0';
            }

            ///--[Reposition]
            //--Create and register new breakpoint.
            nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
            nPack->Initialize();
            mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);

            //--Set the breakpoint to the X position in the succeeding string until a ] is found.
            nPack->mIsReposition = true;

            //--Scan into a buffer until a ']' is reached.
            int n = 0;
            char tNumBuf[32];
            tNumBuf[0] = '\0';
            for(int p = i+4; p < cLen; p ++)
            {
                //--Stop on a ']'
                if(tBuffer[p] == ']')
                {
                    tStart = p+1;
                    i = p;
                    if(tStart == cLen) tLettersRemaining = false;
                    break;
                }
                //--Copy across.
                else
                {
                    tNumBuf[n+0] = tBuffer[p];
                    tNumBuf[n+1] = '\0';
                    n ++;
                }
            }

            //--Resolve the number.
            nPack->mRepositionX = atof(tNumBuf);
        }
        //--No special string.
        else
        {
        }
    }

    ///--[Single-Line Handler]
    //--First, if there were no breakpoints, this is just a normal string. Create a single
    //  breakpoint to represent the whole string.
    if(mBreakpointList->GetListSize() < 1)
    {
        //--Set.
        StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
        nPack->Initialize();
        ResetString(nPack->mString, tBuffer);
        mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);

        //--Report.
        if(false)
        {
            fprintf(stderr, "Report:\n");
            fprintf(stderr, " Original string: %s\n", tBuffer);
            fprintf(stderr, " String has no breakpoints.\n");
            fprintf(stderr, " String Segment 0: %s\n", nPack->mString);
        }
        return;
    }

    ///--[Remaining Letters]
    //--Otherwise, we need to add a breakpoint for the remainder of the string after the last image block.
    //  if tLettersRemaining is false, then the last letter coincides with an image breakpoint.
    if(tLettersRemaining)
    {
        StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
        nPack->Initialize();
        ResetString(nPack->mString, &tBuffer[tStart]);
        mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);
    }

    ///--[Debug]
    //--Report.
    if(false)
    {
        fprintf(stderr, "Report:\n");
        fprintf(stderr, " Original string: %s\n", tBuffer);
        fprintf(stderr, " Segments: %i\n", mBreakpointList->GetListSize());
        int t = 0;
        StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
        while(rBreakpoint)
        {
            fprintf(stderr, "  %i: IMG %3i - String: |%s|\n", t, rBreakpoint->mIndex, rBreakpoint->mString);
            t ++;
            rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
        }
    }
}
void StarlightString::CrossreferenceImages()
{
    //--Once all images are set and the string is parsed, call this to provide pointers to all
    //  the breakpoints.
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        if(rBreakpoint->mIndex >= 0 && rBreakpoint->mIndex < mImagesTotal)
        {
            rBreakpoint->mYOffset = mYOffsets[rBreakpoint->mIndex];
            rBreakpoint->mImgScale = mImgScales[rBreakpoint->mIndex];
            rBreakpoint->rImage = mrImages[rBreakpoint->mIndex];
        }
        else
        {
            rBreakpoint->rImage = NULL;
        }

        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }
}
StarlightString *StarlightString::Clone()
{
    //--Create.
    StarlightString *nString = new StarlightString();

    //--Breakpoints
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        //--Create a copy breakpoint.
        StarlightStringBreakpoint *nNewBreakpoint = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
        nNewBreakpoint->Initialize();
        nNewBreakpoint->mIndex = rBreakpoint->mIndex;
        ResetString(nNewBreakpoint->mString, rBreakpoint->mString);
        nNewBreakpoint->mYOffset = rBreakpoint->mYOffset;
        nNewBreakpoint->mImgScale = rBreakpoint->mImgScale;
        nNewBreakpoint->rImage = rBreakpoint->rImage;
        nString->mBreakpointList->AddElementAsTail("X", nNewBreakpoint, &StarlightStringBreakpoint::DeleteThis);

        //--Next.
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    //--Images
    nString->AllocateImages(mImagesTotal);
    for(int i = 0; i < mImagesTotal; i ++)
    {
        nString->mYOffsets[i] = mYOffsets[i];
        nString->mImgScales[i] = mImgScales[i];
        nString->mrImages[i] = mrImages[i];
    }

    //--Colors
    memcpy(&nString->mTextColor,  &mTextColor, sizeof(StarlightColor));
    memcpy(&nString->mImageColor, &mImageColor, sizeof(StarlightColor));

    //--Finish.
    return nString;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void StarlightString::DrawText(float pX, float pY, int pFlags, float pScale, StarFont *pFont)
{
    ///--[Documentation and Setup]
    //--Simple rendering. More complicated rendering will require getting each breakpoint package
    //  and rendering those manually.
    if(!pFont) return;

    ///--[Length]
    //--Compute the total length.
    float tLongestPosition = 0.0f;
    float tCurrentPosition = 0.0f;
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        //--Normal case:
        if(!rBreakpoint->mIsReposition)
        {
            if(rBreakpoint->mString)
            {
                tCurrentPosition = tCurrentPosition + pFont->GetTextWidth(rBreakpoint->mString);
                if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
            }
            if(rBreakpoint->rImage)
            {
                tCurrentPosition = tCurrentPosition + rBreakpoint->rImage->GetTrueWidth();
                tCurrentPosition = tCurrentPosition + STARLIGHT_STRING_BUFFER_PIXELS;
                if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
            }
        }
        //--Repositioning breakpoint. Note that this can move the position *backwards*.
        else
        {
            tCurrentPosition = rBreakpoint->mRepositionX;
            if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
        }

        //--Next breakpoint.
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    ///--[Flag Handling]
    //--Horizontal centering:
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (tLongestPosition * 0.50f);
        pFlags = pFlags ^ SUGARFONT_AUTOCENTER_X;
    }

    //--Horizontal right-align:
    if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (tLongestPosition * 1.00f);
        pFlags = pFlags ^ SUGARFONT_RIGHTALIGN_X;
    }

    //--Vertical centering:
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (pFont->GetTextHeight() * 0.50f);
        pFlags = pFlags ^ SUGARFONT_AUTOCENTER_Y;
    }

    ///--[Scaling]
    if(pScale != 1.0f)
    {
        //TODO
    }

    ///--[Render]
    //--Store the old color.
    StarlightColor tOldColor;
    glGetFloatv(GL_CURRENT_COLOR, (GLfloat *)(&tOldColor));

    //--Iterate.
    float cOriginalX = pX;
    rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        //--Normal case:
        if(!rBreakpoint->mIsReposition)
        {
            //--Render the text component.
            if(rBreakpoint->mString)
            {
                //--Set the new color.
                if(!(pFlags & SUGARFONT_NOCOLOR)) mTextColor.SetAsMixer();

                //--Render.
                pFont->DrawText(pX, pY, pFlags, 1.0f, rBreakpoint->mString);
                pX = pX + pFont->GetTextWidth(rBreakpoint->mString);
            }

            //--Render the image component bottom-up.
            if(rBreakpoint->rImage)
            {
                //--Set the new color.
                if(!(pFlags & SUGARFONT_NOCOLOR)) mImageColor.SetAsMixer();

                //--Position.
                glTranslatef(pX, pY + rBreakpoint->mYOffset, 0.0f);

                //--Scale.
                if(rBreakpoint->mImgScale != 1.0f && rBreakpoint->mImgScale != 0.0f)
                {
                    glScalef(rBreakpoint->mImgScale, rBreakpoint->mImgScale, 1.0f);
                }

                //--Render.
                rBreakpoint->rImage->Draw();

                //--Clean, next.
                if(rBreakpoint->mImgScale != 1.0f && rBreakpoint->mImgScale != 0.0f)
                {
                    glScalef(1.0f / rBreakpoint->mImgScale, 1.0f / rBreakpoint->mImgScale, 1.0f);
                }
                glTranslatef(-pX, -(pY + rBreakpoint->mYOffset), 0.0f);
                pX = pX + rBreakpoint->rImage->GetTrueWidth();
                pX = pX + STARLIGHT_STRING_BUFFER_PIXELS;
            }
        }
        //--Reposition. This moves the rendering cursor on the breakpoint but doesn't render anything.
        //  Note that this may move the rendering cursor behind the start point!
        else
        {
            pX = cOriginalX + rBreakpoint->mRepositionX;
        }

        //--Next.
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    ///--[Clean]
    //--Reset color.
    tOldColor.SetAsMixer();

    //--Clean scaling.
    //TODO
}
void StarlightString::DrawTextFixed(float pX, float pY, float pMaxLen, int pFlags, StarFont *pFont)
{
    ///--[Documentation]
    //--Determines the width of the string. If it's wider than the max length, changes the horizontal scale
    //  to fit the max len instead, preventing text overruns.
    if(pMaxLen < 1.0f || !pFont) return;

    ///--[Normal Case]
    //--Length check. If less than the max length, just render normally.
    float tNormalLen = GetLength(pFont);
    if(tNormalLen <= pMaxLen)
    {
        DrawText(pX, pY, pFlags, 1.0f, pFont);
        return;
    }

    ///--[Position]
    //--Translate to the rendering position.
    glTranslatef(pX, pY, 0.0f);

    //--Compute the X Scale.
    float cXScale = pMaxLen / tNormalLen;
    glScalef(cXScale, 1.0f, 1.0f);

    //--Render.
    DrawText(0, 0, pFlags, 1.0f, pFont);

    ///--[Clean Up]
    //--Unscale.
    glScalef(1.0f / cXScale, 1.0f, 1.0f);

    //--Untranslate.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);
}
void StarlightString::DrawTextImgScale(float pX, float pY, int pFlags, float pScale, float pImgScale, StarFont *pFont)
{
    ///--[Documentation and Setup]
    //--Same as the above rendering, but the image is scaled according to pImgScale.
    if(!pFont || pImgScale < 0.01f) return;

    ///--[Length]
    //--Compute the total length.
    float tLongestPosition = 0.0f;
    float tCurrentPosition = 0.0f;
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        //--Normal case:
        if(!rBreakpoint->mIsReposition)
        {
            if(rBreakpoint->mString)
            {
                tCurrentPosition = tCurrentPosition + pFont->GetTextWidth(rBreakpoint->mString);
                if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
            }
            if(rBreakpoint->rImage)
            {
                tCurrentPosition = tCurrentPosition + rBreakpoint->rImage->GetTrueWidth();
                tCurrentPosition = tCurrentPosition + STARLIGHT_STRING_BUFFER_PIXELS;
                if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
            }
        }
        //--Repositioning breakpoint. Note that this can move the position *backwards*.
        else
        {
            tCurrentPosition = rBreakpoint->mRepositionX;
            if(tCurrentPosition > tLongestPosition) tLongestPosition = tCurrentPosition;
        }

        //--Next breakpoint.
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    ///--[Flag Handling]
    //--Horizontal centering:
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (tLongestPosition * 0.50f);
        pFlags = pFlags ^ SUGARFONT_AUTOCENTER_X;
    }

    //--Horizontal right-align:
    if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (tLongestPosition * 1.00f);
        pFlags = pFlags ^ SUGARFONT_RIGHTALIGN_X;
    }

    //--Vertical centering:
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (pFont->GetTextHeight() * 0.50f);
        pFlags = pFlags ^ SUGARFONT_AUTOCENTER_Y;
    }

    ///--[Scaling]
    if(pScale != 1.0f)
    {
        //TODO
    }

    ///--[Render]
    float cOriginalX = pX;
    rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        //--Normal:
        if(!rBreakpoint->mIsReposition)
        {
            //--Render the text component.
            if(rBreakpoint->mString)
            {
                pFont->DrawText(pX, pY, pFlags, 1.0f, rBreakpoint->mString);
                pX = pX + pFont->GetTextWidth(rBreakpoint->mString);
            }

            //--Render the image component bottom-up.
            if(rBreakpoint->rImage)
            {
                //--Compute positions.
                float tXPosition = pX;
                float tYPosition = pY + rBreakpoint->mYOffset;

                //--Translate, scale, render.
                glTranslatef(tXPosition, tYPosition, 0.0f);
                glScalef(pImgScale, pImgScale, 1.0f);
                rBreakpoint->rImage->Draw();
                glScalef(1.0f / pImgScale, 1.0f / pImgScale, 1.0f);
                glTranslatef(-tXPosition, -tYPosition, 0.0f);

                //--Increment.
                pX = pX + (rBreakpoint->rImage->GetTrueWidth() * pImgScale);
                pX = pX + STARLIGHT_STRING_BUFFER_PIXELS;
            }
        }
        //--Reposition. This moves the rendering cursor on the breakpoint but doesn't render anything.
        //  Note that this may move the rendering cursor behind the start point!
        else
        {
            pX = cOriginalX + rBreakpoint->mRepositionX;
        }

        //--Next.
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    ///--[Clean]
    //--Clean scaling.
    //TODO
}

///====================================== Pointer Routing =========================================
StarlightStringBreakpoint *StarlightString::GetBreakpoint(int pIndex)
{
    return (StarlightStringBreakpoint *)mBreakpointList->GetElementBySlot(pIndex);
}

///===================================== Static Functions =========================================
///==================================== Structure Functions =======================================
void StarlightStringBreakpoint::Initialize()
{
    //--Variables.
    mIndex = -1;
    mString = NULL;
    mYOffset = 0.0f;
    mImgScale = 1.0f;
    rImage = NULL;

    //--Special.
    mIsReposition = false;
    mRepositionX = 0.0f;
}
void StarlightStringBreakpoint::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    StarlightStringBreakpoint *rPtr = (StarlightStringBreakpoint *)pPtr;
    free(rPtr->mString);
    free(rPtr);
}
int StarlightStringBreakpoint::ComputeLength(StarFont *pFont)
{
    if(!pFont) return 0;
    int tLength = 0;
    if(mString) tLength = tLength + pFont->GetTextWidth(mString);
    if(rImage)  tLength = tLength + rImage->GetTrueWidth();
    return tLength;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
