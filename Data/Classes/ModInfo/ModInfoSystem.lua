-- |[ ===================================== ModInfo System ===================================== ]|
--Contains static functions for using ModInfo arrays.

-- |[ ================================ ModInfo:fnScanModsInto() ================================ ]|
--Scans mods and returns them as an array. This is where the Z Mod Reply.lua script should be
-- getting called from.
function ModInfo:fnScanModsInto(psFolderPath)
    
    -- |[ =================== Setup ==================== ]|
    -- |[Argument Check]|
    if(psFolderPath == nil) then return end
    
    -- |[Descriptor Check]|
    --The folder must contain Mod Info.txt to be a valid folder to scan.
    if(FS_Exists(psFolderPath .. "Mod Info.txt") == false) then return end
    
    -- |[Temp Global Setup]|
    gzaCurrentModListing = {}
    
    -- |[ ============= Manual Addition =============== ]|
    --On OSX, I couldn't get physfx to work. So known mods are manually added here.
    if(LM_GetSystemOS() == "OSX") then
        if(FS_Exists(psFolderPath .. "PaletteForms/Z Mod Reply.lua")) then
            LM_ExecuteScript(psFolderPath .. "PaletteForms/Z Mod Reply.lua", ModInfo.ciReportBasicData)
        end
        if(FS_Exists(psFolderPath .. "AltDragons/Z Mod Reply.lua")) then
            LM_ExecuteScript(psFolderPath .. "AltDragons/Z Mod Reply.lua", ModInfo.ciReportBasicData)
        end
        if(FS_Exists(psFolderPath .. "Randomizer/Z Mod Reply.lua")) then
            LM_ExecuteScript(psFolderPath .. "Randomizer/Z Mod Reply.lua", ModInfo.ciReportBasicData)
        end
    
    -- |[ ============ Scan All Subfolders ============= ]|
    else
    
        -- |[Mods Directory]|
        --Always call this.
        if(true) then
            
            --Open.
            FS_Open(psFolderPath, false)
            local sPath = FS_Iterate()

            --Iterate across the paths.
            while(sPath ~= "NULL") do

                --Setup.
                local bBypass = false
                local sFolderName = fnResolveFolderName(sPath)

                --Check the last byte for '/'. This indicates it's a directory. Ignore files.
                local iLength = string.len(sPath)
                if(string.byte(sPath, iLength) ~= string.byte("/")) then
                    bBypass = true
                end

                --If not bypassing, execute.
                if(bBypass == false) then
                    if(FS_Exists(sPath .. "Z Mod Reply.lua") == true) then
                        LM_ExecuteScript(sPath .. "Z Mod Reply.lua", ModInfo.ciReportBasicData)
                    end
                end

                --Next.
                sPath = FS_Iterate()
            end

            FS_Close()
        end
        
        -- |[Steam Workshop Directory]|
        --If any only if we're on Steam, scan the workshop directory using the game's ID. Right now this is
        -- keyed to WHI. In the future multiple scans may be necessary for some games.
        --The code is basically the same as above on a different folder.
        if(Steam_IsSteamGame() == true) then
            
            --Path of the workshop relative to the executable folder.
            local iGameID = 2482900 --WHI's game ID.
            local sScanWorkshopPath = "../../workshop/content/2482900/"
            
            --Open the path, but not in recursive mode.
            FS_Open(sScanWorkshopPath, false)
            local sFolderPath = FS_Iterate()
            while(sFolderPath ~= "NULL") do

                --Setup.
                local bBypass = false
                local sFolderName = fnResolveFolderName(sFolderPath)

                --Check the last byte for '/'. This indicates it's a directory. Ignore files.
                local iLength = string.len(sFolderPath)
                if(string.byte(sFolderPath, iLength) ~= string.byte("/")) then
                    bBypass = true
                end

                --If not bypassing, execute.
                if(bBypass == false) then
                    if(FS_Exists(sFolderPath .. "Z Mod Reply.lua") == true) then
                        LM_ExecuteScript(sFolderPath .. "Z Mod Reply.lua", ModInfo.ciReportBasicData)
                    end
                end

                --Next.
                sFolderPath = FS_Iterate()
            end

            --Finish up.
            FS_Close()
        end
    end
    
    -- |[Finish Up]|
    return gzaCurrentModListing
end

-- |[ =============================== ModInfo:fnTrimModsByFile() =============================== ]|
--After mods are scanned into a mod array, check the load order file. Any mods not in the file, or
-- in the file but deactivated, are removed from the array, and the array returned be ordered to match
-- the order in the file.
--On error, returns an empty array. Otherwise, returns an array containing ModInfo objects.
function ModInfo:fnTrimModsByFile(psLoadOrderFile, pzaModArray)
    
    -- |[Argument Check]|
    if(psLoadOrderFile == nil) then return {} end
    if(pzaModArray     == nil) then return {} end
    
    -- |[File Check]|
    local fInfile = io.open(psLoadOrderFile, "r")
    if(fInfile == nil) then return {} end
    
    -- |[Setup]|
    --New array to hold the ordered mods.
    local zaNewModListing = {}
    
    -- |[Read]|
    local sLine = fInfile:read("*line")
    while(sLine ~= nil) do
        
        --Read the enabled/disabled line.
        local sEnableLine = fInfile:read("*line")
        
        --If the mod is enabled, the sEnableLine will not be a "0". If so, it can be added. If it's
        -- "0" then ignore it.
        if(sEnableLine ~= "0") then
            
            --The read line will be the name of the mod in question. Find it in the mod array.
            for i = 1, #pzaModArray, 1 do
                if(pzaModArray[i].sLocalName == sLine) then
                    table.insert(zaNewModListing, pzaModArray[i])
                    break
                end
            end
        end
        
        --Read the next line.
        sLine = fInfile:read("*line")
    end
    
    -- |[Clean Up]|
    fInfile:close()
    return zaNewModListing
end

-- |[ =============================== ModInfo:fnRunModsAtStep() ================================ ]|
--Runs all mods in the provided mod array at the given step, which should be a constant as defined
-- in the ModInfo.lua file.
function ModInfo:fnRunModsAtStep(piStep, pzaModArray)
    
    -- |[Argument Check]|
    if(piStep      == nil) then return end
    if(pzaModArray == nil) then return end
    
    -- |[Execute]|
    for i = 1, #pzaModArray, 1 do
    
        --Mod must be active to run.
        if(pzaModArray[i].bIsActive == true and pzaModArray[i].sPath ~= "Null") then
    
            --Get reply script.
            local sModReplyPath = pzaModArray[i].sPath .. "Z Mod Reply.lua"
    
            --Execute.
            LM_ExecuteScript(sModReplyPath, piStep)
    
        end
    end
end