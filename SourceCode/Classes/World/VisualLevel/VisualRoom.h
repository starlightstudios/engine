///======================================== VisualRoom ============================================
//--Functions mostly the same as a PandemoniumRoom from which it is derived, but has extra visual components.
//  Can also show visual transitions. These are referred to as "Zones" in the Lua in some cases.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "PandemoniumRoom.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define WAD_TO_VR_SCALE 0.30f
#define VR_BODY_FACTOR 0.40f
#define VR_CONN_DEPTH -0.000006f
#define VR_BODY_DEPTH -0.000005f

///========================================== Classes =============================================
class VisualRoom : public PandemoniumRoom
{
    private:
    //--System
    uint32_t mStoredNodeID;

    //--Connection Building
    uint32_t mConnectionNodeIDs[NAV_TOTAL];

    protected:

    public:
    //--System
    VisualRoom();
    virtual ~VisualRoom();

    //--Public Variables
    bool mIsHighlighted;
    static float xDistanceScale;

    //--Property Queries
    virtual bool IsOfType(int pType);
    uint32_t GetAssociatedNodeID();

    //--Manipulators
    void SetPositionByNode(const char *pNodeName);

    //--Core Methods
    virtual void BuildConnectivity(int pRoomsTotal, PandemoniumRoom **pRoomList);
    virtual bool RecursivePather(PandemoniumRoom *pCurrentRoom, int pMovesSoFar, int *pMoveList);
    static uint32_t FollowIDTrail(uint32_t pStartNodeID, uint32_t pMoveToNodeID, VisualLevel *pLevel);
    static uint32_t FindFirstID(uint32_t pStartNodeID, uint32_t pEndNode, VisualLevel *pLevel);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void RespondToActorExit(Actor *pActor);
    virtual void RespondToActorEntry(Actor *pActor, PandemoniumRoom *pPreviousRoom);

    //--File I/O
    //--Drawing
    virtual void Render();
    virtual void RenderConnections();
    virtual void RenderBody();
    virtual void RenderEntities();
    virtual void RenderSpecialIcons();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_VR_SetProperty(lua_State *L);

