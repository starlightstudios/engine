//--Base
#include "MonoUICampfire.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
MonoUICampfire::MonoUICampfire()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MCMP");

    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUISample ======= ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUICampfire ====== ]
    ///--[System]
    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    ///--[Strings]
    //--Allocate Help Strings
    AllocateHelpStrings(cxMonoHelpStrings);

    ///--[Images]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUICampfire::~MonoUICampfire()
{
}
void MonoUICampfire::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Campfire UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Campfire UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",               &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = MonoImages.rFont_Control;
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUICampfire_Strings *MonoUICampfire::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUICampfire_Strings::MonoUICampfire_Strings()
{
    //--Local name: "MonoUICampfire"
    MonoUICampfire::xrStringData = this;

    //--Strings
    str.mTextCampfireMenu = InitializeString("MonoUICampfire_mCampfireMenu"); //"Campfire Menu"
    str.mTextWarp         = InitializeString("MonoUICampfire_mWarp");         //"Warp"
    str.mTextSave         = InitializeString("MonoUICampfire_mSave");         //"Save"
    str.mTextPassword     = InitializeString("MonoUICampfire_mPassword");     //"Password"

    //--Help Sequence
    str.mTextHelp[0] = InitializeString("MonoUICampfire_mHelpText00");
    str.mTextHelp[1] = InitializeString("MonoUICampfire_mHelpText01");
    str.mTextHelp[2] = InitializeString("MonoUICampfire_mHelpText02");
    str.mTextHelp[3] = InitializeString("MonoUICampfire_mHelpText03");
    str.mTextHelp[4] = InitializeString("MonoUICampfire_mHelpText04");

    //--Control Sequence
    str.mTextControlShowHelp = InitializeString("MonoUICampfire_mControlShowHelp"); //"[IMG0] Show Help"
}
MonoUICampfire_Strings::~MonoUICampfire_Strings()
{
    for(int i = 0; i < MONOUICAMPFIRE_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUICampfire_Strings::GetSize()
{
    return MONOUICAMPFIRE_STRINGS_TOTAL;
}
void MonoUICampfire_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUICAMPFIRE_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUICampfire::TakeForeground()
{
    ///--[Variables]
    ///--[Images]
    //--Because the icons for the campfire entries can change depending on circumstances, icon lookups are
    //  rebuilt each time the object takes the foreground.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    AdvImages.rIcons[AM_CAMPFIRE_REST]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Rest");
    AdvImages.rIcons[AM_CAMPFIRE_CHAT]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Chat");
    AdvImages.rIcons[AM_CAMPFIRE_SAVE]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Save");
    AdvImages.rIcons[AM_CAMPFIRE_COSTUMES] = (StarBitmap *)rDataLibrary->GetEntry(AdventureMenu::xCostumeIconPath);
    AdvImages.rIcons[AM_CAMPFIRE_FORM]     = (StarBitmap *)rDataLibrary->GetEntry(AdventureMenu::xTransformIconPath);
    AdvImages.rIcons[AM_CAMPFIRE_WARP]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Warp");
    AdvImages.rIcons[AM_CAMPFIRE_RELIVE]   = (StarBitmap *)rDataLibrary->GetEntry(AdventureMenu::xReliveIconPath);
    AdvImages.rIcons[AM_CAMPFIRE_PASSWORD] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Password");

    ///--[Construction]
    //--On the first build, setup the menu grid.
    if(!mHasBuiltGrid)
    {
        ///--[Basic Setup]
        //--Construct the grid and set its positions.
        mHasBuiltGrid = true;
        AllocateGrid(5, 3); //Three lines are allocated to keep the center point same as the base UI. They are not used.
        ProcessGrid();

        ///--[Components]
        //--Populate the grid.
        QuicksetGridEntry(1, 1, AM_CAMPFIRE_WARP,     xrStringData->str.mTextWarp,     AdvImages.rIcons[AM_CAMPFIRE_WARP]);
        QuicksetGridEntry(2, 1, AM_CAMPFIRE_SAVE,     xrStringData->str.mTextSave,     AdvImages.rIcons[AM_CAMPFIRE_SAVE]);
        QuicksetGridEntry(3, 1, AM_CAMPFIRE_PASSWORD, xrStringData->str.mTextPassword, AdvImages.rIcons[AM_CAMPFIRE_PASSWORD]);
    }

    //--Grid. Cursor is set to the save entry.
    SetCursor(AM_CAMPFIRE_SAVE);

    //--Cursor.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString(xrStringData->str.mTextControlShowHelp);
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();
}

///======================================= Core Methods ===========================================
void MonoUICampfire::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Execution]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Common lines.
    SetHelpString(i, xrStringData->str.mTextHelp[0]);
    SetHelpString(i, xrStringData->str.mTextHelp[1]);
    SetHelpString(i, xrStringData->str.mTextHelp[2]);
    SetHelpString(i, xrStringData->str.mTextHelp[3]);
    SetHelpString(i, xrStringData->str.mTextHelp[4], 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUICampfire::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    MonoImages.rTitleBack->Draw(22.0f, -37.0f);

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cColorAlpha);
    MonoImages.rDoubleHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextCampfireMenu);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    AutoPieceClose();
}
void MonoUICampfire::RenderBot(float pVisAlpha)
{
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
