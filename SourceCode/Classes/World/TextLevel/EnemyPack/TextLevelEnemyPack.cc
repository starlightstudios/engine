//--Base
#include "TextLevelEnemyPack.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
void CombatEnemyPack::Initialize()
{
    //--Common Variables
    mHP = 1;
    mHPMax = 1;
    mFlashTimer = 0;
    mStunTimer = 0;
    rImage = NULL;

    //--HP Falloff
    mHPTimer = CEP_HP_TICKS;
    mHPRender = 1.0f;
    mHPStart = 1.0f;
    mHPEnd = 1.0f;

    //--Typing-Mode Variables
    mAttackPower = 2;
    mDefensePower = 1;

    //--Sentence-Mode Variables
    mTotalAttackWeight = 0;
    mCurrentAttack = -1;
    mPrepTicksLeft = 0;
    mPrepTicksMax = 1;
    mAttackTicks = 0;
    mAttackTicksNeeded = 0;
    mAPRender = 0.0f;
    mAttackList = NULL;

    //--Elemental Weaknesses
    memset(mElementWeakness, 0, sizeof(int) * TL_ELEMENTS_TOTAL);
}
void CombatEnemyPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    CombatEnemyPack *rPtr = (CombatEnemyPack *)pPtr;
    delete rPtr->mAttackList;
    free(rPtr);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CombatEnemyPack::SetHP(int pHP, int pMaxHP)
{
    mHP = pHP;
    mHPMax = pMaxHP;
    if(mHPMax < 1) mHPMax = 1;
    if(mHP > mHPMax) mHP = mHPMax;
    if(mHP < 1) mHP = 1;
}
void CombatEnemyPack::SetWeakness(int pSlot, int pAmount)
{
    if(pSlot < 0 || pSlot >= TL_ELEMENTS_TOTAL) return;
    mElementWeakness[pSlot] = pAmount;
}

///======================================= Core Methods ===========================================
void CombatEnemyPack::BootSentenceMode()
{
    //--Activates variables for use in sentence-typing combat.
    mAttackList = new StarLinkedList(true);
}
void CombatEnemyPack::AddSentenceAttack(int pWeight, int pPower, int pTicks, uint8_t pElementalFlags)
{
    //--Error check.
    if(!mAttackList) return;

    //--Argument check.
    if(pWeight < 1) pWeight = 1;
    if(pPower < 1) pPower = 1;
    if(pTicks < 1) pTicks = 1;

    //--Create and add.
    SetMemoryData(__FILE__, __LINE__);
    CombatEnemyAttackPack *nAttackPack = (CombatEnemyAttackPack *)starmemoryalloc(sizeof(CombatEnemyAttackPack));
    nAttackPack->mRollWeight = pWeight;
    nAttackPack->mAttackPower = pPower;
    nAttackPack->mPrepTicks = pTicks;
    nAttackPack->mElementFlags = pElementalFlags;
    mAttackList->AddElement("X", nAttackPack, &FreeThis);

    //--Increase total weight.
    mTotalAttackWeight += pWeight;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
