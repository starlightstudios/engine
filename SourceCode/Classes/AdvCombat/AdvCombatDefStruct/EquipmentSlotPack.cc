//--Base
#include "AdvCombatDefStruct.h"

//--Classes
#include "AdventureItem.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void EquipmentSlotPack::Initialize()
{
    mDisplayName = NULL;
    mIsComputedForStats = true;
    mIsComputedForTags = true;
    mIsWeaponAlternate = false;
    mCanBeEmpty = false;
    mNoGems = false;
    mEquippedItem = NULL;
}
void EquipmentSlotPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    EquipmentSlotPack *rPtr = (EquipmentSlotPack *)pPtr;
    free(rPtr->mDisplayName);
    delete rPtr->mEquippedItem;
    free(rPtr);
}
