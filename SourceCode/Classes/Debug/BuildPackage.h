///======================================= BuildPackage ===========================================
//--Class that stores a single "Build" of a game, such as String Tyrant or Pandemonium for a specific
//  OS. These can be made and configured via Lua and accessed from various locations in the engine.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Definitions ========================================
//--Shell Types
#define BP_SHELL_DOS 0
#define BP_SHELL_BASH 1

//--Instruction Types
#define BP_INSTRUCTION_FAIL -1
#define BP_INSTRUCTION_SETUP_BUILD_DIR 0
#define BP_INSTRUCTION_CREATE_FOLDER 1
#define BP_INSTRUCTION_COPY_FILE 2
#define BP_INSTRUCTION_COPY_FOLDER 3
#define BP_INSTRUCTION_DELETE_FILE 4
#define BP_INSTRUCTION_DELETE_FOLDER 5
#define BP_INSTRUCTION_CHANGEICON 6
#define BP_INSTRUCTION_OPENSLFFILE 7
#define BP_INSTRUCTION_DUMMYIMAGEINFILE 8
#define BP_INSTRUCTION_CLOSESLFFILE 9

//--Strings Needed
#define BP_STRINGS_MAX 2
#define BP_COMMANDS_MAX 2

///===================================== Local Structures =========================================
//--Contains an OS-agnostic instruction which can be executed to make builds.
typedef struct BuildPackageInstruction
{
    //--Members
    int mInstructionCode;
    char *mStrings[BP_STRINGS_MAX];

    //--Functions
    void Initialize()
    {
        mInstructionCode = BP_INSTRUCTION_FAIL;
        memset(mStrings, 0, sizeof(char *) * BP_STRINGS_MAX);
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        BuildPackageInstruction *rPtr = (BuildPackageInstruction *)pPtr;
        for(int i = 0; i < BP_STRINGS_MAX; i ++) free(rPtr->mStrings[i]);
        free(rPtr);
    }
}BuildPackageInstruction;

///========================================== Classes =============================================
class BuildPackage : public RootObject
{
    private:
    //--System
    int mCommandsType;
    char *mCoreDirectory;
    char *mBuildDirectory;
    StarLinkedList *mInstructionList; //BuildPackageInstruction *, master

    //--Private Statics
    static StarLinkedList *xBuildList; //StarLinkedList *, master. Contains BuildPackage *, master.

    protected:

    public:
    //--System
    BuildPackage();
    virtual ~BuildPackage();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetCoreDirectory(const char *pPath);
    void SetBuildDirectory(const char *pPath);
    void RegisterSetupBuildDirectory();
    void RegisterCreateFolderCommand(const char *pFolderPath);
    void RegisterCopyFileCommand(const char *pOrigFile, const char *pDestFile);
    void RegisterCopyFolderCommand(const char *pOrigFolder, const char *pDestFolder);
    void RegisterDeleteFileCommand(const char *pFileName);
    void RegisterDeleteFolderCommand(const char *pFolderName);
    void RegisterChangeIconCommand(const char *pFilename, const char *pIconName);
    void RegisterOpenFileCommand(const char *pFilename);
    void RegisterDummyImageCommand(const char *pLumpName);
    void RegisterFinishFileCommand();

    //--Core Methods
    void FixSlashes(char *pString);
    void ExecuteCommand(BuildPackageInstruction *pCommand, bool pIsEmulation, FILE *fOutputFile);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Execute(bool pIsEmulation, FILE *fOutfile);
    void ExecuteToPath(bool pIsEmulation, const char *pPath);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static StarLinkedList *FetchBuildList();
    static void Scan();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_BP_Exists(lua_State *L);
int Hook_BP_Create(lua_State *L);
int Hook_BP_GetProperty(lua_State *L);
int Hook_BP_SetProperty(lua_State *L);
