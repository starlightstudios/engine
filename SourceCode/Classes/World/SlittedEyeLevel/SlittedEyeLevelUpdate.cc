//--Base
#include "SlittedEyeLevel.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///--[Local Definitions]
#define HISSING_TICKS 60

///========================================== Update ==============================================
void SlittedEyeLevel::Update()
{
    ///============ Documentation ===============
    //--This routine will call RunLuaUpdateFunction() several times as dictated by a counter until it
    //  receives a "true" result. If a false result is received, the update script is instead called,
    //  which is expected to create the function fnSlittedEyeLevelUpdate() in lua.
    //--That function is what is called. This is meant to bypass hard drive calls and instead call
    //  functions from the lua state directly, speeding things up. The function is expected to call
    //  SEL_SetProperty("Handle Update Call") to indicate it actually handled the update, otherwise
    //  the program will assume something went wrong and rebuild the function.
    //--This routine also handles timers that may be running and manages sound effects and queues.
    if(!mUpdateScript)
    {
        //DebugManager::ForcePrint("SlittedEyeLevelUpdate: Error, update script path was NULL.\n");
        return;
    }

    ///=========== Update Handler ===============
    //--Call the update script.
    LuaManager::Fetch()->ExecuteLuaFile(mUpdateScript);

    ///============ Fading Timers ===============
    //--If currently fading, handle color modifications.
    if(mIsFading)
    {
        //--Timer.
        mFadeTicksCur ++;

        //--Percentage.
        float tPercent = (float)mFadeTicksCur / (float)mFadeTicksMax;
        if(tPercent < 0.0f) tPercent = 0.0f;
        if(tPercent > 1.0f) tPercent = 1.0f;

        //--Set.
        mFadeColCurrent.r = mFadeColBegin.r + ((mFadeColFinish.r - mFadeColBegin.r) * tPercent);
        mFadeColCurrent.g = mFadeColBegin.g + ((mFadeColFinish.g - mFadeColBegin.g) * tPercent);
        mFadeColCurrent.b = mFadeColBegin.b + ((mFadeColFinish.b - mFadeColBegin.b) * tPercent);
        mFadeColCurrent.a = mFadeColBegin.a + ((mFadeColFinish.a - mFadeColBegin.a) * tPercent);
    }

    ///========== Hissing Sequence ==============
    //--When the player begins hissing, this reduces the stamina and prevents duplication.
    if(mIsHissing)
    {
        //--Each tick, increment the timer and decrement the hiss cost.
        mHissTimer ++;
        mStamina = mStamina - (mStaminaLossPerHiss / (float)HISSING_TICKS);

        //--Ending case.
        if(mHissTimer >= HISSING_TICKS)
        {
            mHissTimer = 0;
            mIsHissing = false;
        }
    }

    ///========== Control Handling ==============
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Update the mouse position.
    int tDummyZ = 0;
    rControlManager->GetMouseCoords(mMouseX, mMouseY, tDummyZ);

    ///--[Passive Handlers]
    //--Setup.
    int cScrollThreshold = 64;
    int cXOffsetDelta = 5;
    int cXOffsetCap = 434;

    //--If the mouse is on the left side of the screen, scroll left.
    if(mMouseX < cScrollThreshold)
    {
        if(mXOffset > 0) mXOffset -= cXOffsetDelta;
        if(mXOffset < 1) mXOffset = 0;
    }
    else if(mMouseX > VIRTUAL_CANVAS_X - cScrollThreshold)
    {
        if(mXOffset < cXOffsetCap) mXOffset += cXOffsetDelta;
        if(mXOffset >= cXOffsetCap) mXOffset = cXOffsetCap;
    }

    ///--[Active Handlers]
    //--"Manipulate" button. Left-click by default. Can activate objects.
    if(rControlManager->IsFirstPress("SE Manipulate") && !mDisableManipulate)
    {
    }

    //--"Hiss" button. Right-click by default. Scares away certain enemies.
    if(rControlManager->IsFirstPress("SE Hiss") && !mDisableHiss)
    {
        //--The player cannot already be hissing.
        if(!mIsHissing)
        {
            //--Play SFX.
            AudioManager::Fetch()->PlaySound("Player|Hiss");

            //--Set hissing timers.
            mIsHissing = true;
            mHissTimer = 0;

            //--Check the hiss hitboxes. If any got hit, fire their script.
            HissingHitbox *rHitbox = (HissingHitbox *)mHissHitboxes->PushIterator();
            while(rHitbox)
            {
                //--On hit, call the script.
                if(IsPointWithin2DReal(mMouseX, mMouseY, rHitbox->mHitbox) && rHitbox->mScriptPath)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(rHitbox->mScriptPath, 1, "N", rHitbox->mFiringCode);
                }

                //--Next.
                rHitbox = (HissingHitbox *)mHissHitboxes->AutoIterate();
            }
        }
    }

    //--Game over, returns to menu during game over sequence.
    if(rControlManager->IsFirstPress("SE Game Over") && mAllowGameOver)
    {
        LuaManager::Fetch()->ExecuteLuaFile(mGameOverScript);
    }

    ///=============== Clean Up =================
    //--Any hissing boxes flagged for deletion are cleaned up here. This is done after hitbox detection,
    //  to prevent dangling pointer problems.
    HissingHitbox *rHitbox = (HissingHitbox *)mHissHitboxes->SetToHeadAndReturn();
    while(rHitbox)
    {
        if(rHitbox->mFlagForDeletion)
        {
            mHissHitboxes->RemoveRandomPointerEntry();
        }
        rHitbox = (HissingHitbox *)mHissHitboxes->IncrementAndGetRandomPointerEntry();
    }
}
