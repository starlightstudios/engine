///======================================= AdvUITrainer ===========================================
//--UI for the Trainer interface, allows the player to spend cash to buy XP.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#ifndef ADVUITRAINER_DEFINITIONS
#define ADVUITRAINER_DEFINITIONS

//--Timers
#define ADVMENU_TRAINER_VIS_TICKS 15
#define ADVMENU_TRAINER_CASH_TICKS 15
#define ADVMENU_TRAINER_CONFIRM_TICKS 15
#define ADVMENU_TRAINER_CURSOR_MOVE_TICKS 7

//--Sizing and Positions
#define ADVMENU_TRAINER_XSPACING       328.0f
#define ADVMENU_TRAINER_CASH_MAXSLOTS 5
#define ADVMENU_TRAINER_CASH_ARROWLFT  475.0f
#define ADVMENU_TRAINER_CASH_ARROWSPX   64.0f
#define ADVMENU_TRAINER_CASH_ARROWTOP  185.0f
#define ADVMENU_TRAINER_CASH_ARROWBOT  307.0f
#define ADVMENU_TRAINER_CASH_NUMBERLFT 471.0f
#define ADVMENU_TRAINER_CASH_NUMBERTOP 215.0f
#define ADVMENU_TRAINER_CASH_NUMBERSPX  64.0f

//--Strings
#define ADVMENU_TRAINER_STRING_PURCHASE_TOTAL 3
#define ADVMENU_TRAINER_STRING_CONFIRM_TOTAL 2
#define ADVMENU_TRAINER_STRING_HELP_CHARSELECT_TOTAL 2
#define ADVMENU_TRAINER_STRING_HELP_EXPSELECT_TOTAL 1
#define ADVMENU_TRAINER_STRING_HELP_CONFIRM_TOTAL 1

#endif

///========================================== Classes =============================================
class AdvUITrainer : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    ///--[Variables]
    //--System
    //--Character Selection
    int mCharacterCursor;

    //--Cash Input
    bool mIsCashMode;
    int mCashTimer;
    int mEXPValue;
    int mEXPEditSlot;

    //--Confirmation
    bool mIsConfirmMode;
    int mConfirmTimer;

    //--Cursor
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    ///--[Strings]
    //--Strings
    char *mStringsPurchase[ADVMENU_TRAINER_STRING_PURCHASE_TOTAL];
    char *mStringsConfirm[ADVMENU_TRAINER_STRING_CONFIRM_TOTAL];
    StarlightString *mStringsCharacterHelp[ADVMENU_TRAINER_STRING_HELP_CHARSELECT_TOTAL];
    StarlightString *mStringsEXPSelectHelp[ADVMENU_TRAINER_STRING_HELP_EXPSELECT_TOTAL];
    StarlightString *mStringsConfirmHelp[ADVMENU_TRAINER_STRING_HELP_CONFIRM_TOTAL];

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFontCash;
        StarFont *rFontDoubleHeading;
        StarFont *rFontHeading;
        StarFont *rFontHelp;
        StarFont *rFontPaymentHelp;
        StarFont *rFontName;
        StarFont *rFontStats;

        //--Images
        StarBitmap *rFrameBase;
        StarBitmap *rFrameConfirm;
        StarBitmap *rFrameHeader;
        StarBitmap *rFramePayment;
        StarBitmap *rFramePlatina;
        StarBitmap *rOverlayArrowRgt;
        StarBitmap *rOverlayArrowUp;
        StarBitmap *rOverlayNameBanner;
        StarBitmap *rOverlayHighlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUITrainer();
    virtual ~AdvUITrainer();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    void RenderCharacterInfoPanel(AdvCombatEntity *pEntity);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


