///======================================= MonoUIStatus ===========================================
//--Shows HP/MP/Atk etc as well as equipment and resistances.

#pragma once

///========================================= Includes =============================================
#include "AdvUIStatus.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define MONOUISS_RESIST_ICONS_TOTAL 5

//--Backwards Code
#define MONOUISTS_CODE_LEVELUP 10

///======================================== Translation ===========================================
///--[MonoUIStatus_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUISTATUS_STRINGS_TOTAL 31
class MonoUIStatus_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUISTATUS_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextStatus;
            char *mTextHelpMenu;
            char *mTextPower;
            char *mTextProtection;
            char *mTextAccuracy;
            char *mTextEvade;
            char *mTextInitiative;
            char *mTextEquipment;
            char *mTextEmpty;
            char *mTextResistances;
            char *mTextImmune;
            char *mTextHelp[16];
            char *mTextControlShowHelp;
            char *mTextControlPrevChar;
            char *mTextControlNextChar;
            char *mTextControlPurchaseLevelUps;
        }str;
    };

    //--Functions
    MonoUIStatus_Strings();
    virtual ~MonoUIStatus_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIStatus : public MonoUICommon, virtual public AdvUIStatus
{
    protected:
    ///--[System]
    //--Array Sizes
    static const int cxMonoHelpStrings = 16;

    ///--[Help Text]
    //--Help Strings
    StarlightString *mActivateString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_MainlineLg;
        StarFont *rFont_Equipment;
        StarFont *rFont_Control;
        StarFont *rFont_Health;
        StarFont *rFont_Help;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIStatus();
    virtual ~MonoUIStatus();
    virtual void Construct();

    //--Public Variables
    static MonoUIStatus_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    virtual void RenderBasics(AdvCombatEntity *pEntity, float pColorAlpha);
    virtual void RenderEquipment(AdvCombatEntity *pEntity, float pColorAlpha);
    virtual void RenderEquipmentEntry(AdvCombatEntity *pEntity, int pSlot, float pColorAlpha);
    virtual void RenderResistances(AdvCombatEntity *pEntity, float pColorAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
