///==================================== AudioExec_StopMusic =======================================
//--Stops whatever music is currently playing.  Can optionally fade it out.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "AudioManager.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AudioExec_StopMusic : public RootExecutor
{
    private:
    //--System
    int mFadeMusicTicks;

    protected:

    public:
    //--System
    AudioExec_StopMusic()
    {
        mFadeMusicTicks = 0;
    }
    virtual ~AudioExec_StopMusic()
    {
    }

    //--Manipulators
    void SetFadingTicks(int pTicks)
    {
        mFadeMusicTicks = pTicks;
    }

    //--Executor
    virtual void Execute()
    {
        if(!mFadeMusicTicks)
        {
            AudioManager::Fetch()->StopAllMusic();
        }
        else
        {
            AudioManager::Fetch()->FadeMusic(mFadeMusicTicks);
        }
    }
};

