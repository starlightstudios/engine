//--Base
#include "TextLevel.h"

//--Classes
#include "TileLayer.h"
#include "TiledRawData.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "StarLumpManager.h"

//--Deletion function for the ZLevel package.
void TextLevZLevel::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    TextLevZLevel *rLevel = (TextLevZLevel *)pPtr;
    for(int i = 0; i < rLevel->mTilesetsTotal; i ++)
    {
        TiledRawData::DeleteTilesetPack(&rLevel->mTilesetPacks[i]);
    }
    free(rLevel->mTilesetPacks);
    delete rLevel->mTileLayers;
    delete rLevel->mObjectData;
    delete rLevel->mImageData;
    free(rLevel);
}

///--[Parser Functions]
void TextLevel::ParseSLFFile(const char *pPath, int pZLevel)
{
    ///--[Documentation and Setup]
    //--Given a path to an SLF file, parses it using the built-in TiledLevel capabilities. We then run the usual object
    //  parsing functionality to build the full TextLevel world.
    if(!pPath) return;

    //--Store which Z level we're operating on.
    mActiveZLevel = pZLevel;
    mActiveXOff = 0;
    mActiveYOff = 0;

    //--StarLumpManager needs to use 64-pixel tiles in this mode.
    TileLayer::cxSizePerTile = 64.0f;
    StarLumpManager::xTileSizeX = 64;
    StarLumpManager::xTileSizeY = 64;

    //--Open it with the StarLumpManager, which automates the data extraction.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    rSLM->Open(pPath);
    if(!rSLM->IsFileOpen()) return;

    //--This Z-level captures object data used to set offsets.
    TextLevZLevel *nZLevel = (TextLevZLevel *)starmemoryalloc(sizeof(TextLevZLevel));
    nZLevel->Initialize();
    nZLevel->mZLevel = pZLevel;
    mZLevels->AddElementAsTail("X", nZLevel, &TextLevZLevel::DeleteThis);

    //--Run the built-in parser.
    ParseFile(rSLM);

    //--Modify room depths. -1.0 to 0.0 becomes ROOM_DEPTH to ROOM_DEPTH+ROOM_RANGE
    TileLayer *rLayer = (TileLayer *)mTileLayers->PushIterator();
    while(rLayer)
    {
        float tNewDepth = (rLayer->GetDepth() * TL_DEPTH_ROOM_RANGE);
        rLayer->SetDepth(tNewDepth);

        rLayer = (TileLayer *)mTileLayers->AutoIterate();
    }

    //--Report.
    if(false)
    {

        //--Base.
        fprintf(stderr, "SLF Parse Report:\n");
        fprintf(stderr, " Tile Layers: %i\n", mTileLayers->GetListSize());
        fprintf(stderr, " Objects Total: %i\n", mObjectData->GetListSize());

        //--Layer information.
        int i = 0;
        rLayer = (TileLayer *)mTileLayers->PushIterator();
        while(rLayer)
        {
            fprintf(stderr, " Layer %i\n", i);
            fprintf(stderr, "  Sizes %i x %i\n", rLayer->GetXSize(), rLayer->GetYSize());
            fprintf(stderr, "  Depth %f\n", rLayer->GetDepth());

            i ++;
            rLayer = (TileLayer *)mTileLayers->AutoIterate();
        }
    }

    //--Finish up.
    rSLM->Close();
    StarLumpManager::xTileSizeX = DEFAULT_TILE_SIZE;
    StarLumpManager::xTileSizeY = DEFAULT_TILE_SIZE;

    ///--[Z Level Storage]
    //--The Z level now stores the tilesets and tile info local to its level.
    nZLevel->mXSize = mXSize;
    nZLevel->mYSize = mYSize;

    //--Tileset Packs
    nZLevel->mTilesetsTotal = mTilesetsTotal;
    nZLevel->mTilesetPacks = mTilesetPacks;

    //--Raw Lists
    nZLevel->mTileLayers = mTileLayers;
    nZLevel->mObjectData = mObjectData;
    nZLevel->mImageData = mImageData;

    ///--[Reset]
    //--Reset back to neutral state.
    mXSize = 0;
    mYSize = 0;
    mTilesetsTotal = 0;
    mTilesetPacks = NULL;
    mTileLayers = new StarLinkedList(true);
    mObjectData = new StarLinkedList(true);
    mImageData = new StarLinkedList(true);

    ///--[Extensions]
    //--Now that the rooms have been built, check if we need to expand the Z array.
    if(pZLevel < mZLoMost) mZLoMost = pZLevel;
    if(pZLevel > mZHiMost) mZHiMost = pZLevel;

    //--Report.
    //fprintf(stderr, "Extents: %i %i %i %i, %i %i\n", mLftMost, mTopMost, mRgtMost, mBotMost, mZLoMost, mZHiMost);
}
void TextLevel::ParseObjectData()
{
    ///--[Documentation and Setup]
    //--Override of the base TiledLevel version, parses object classifications unique to TextLevels.
    //  First, scan the objects for a room with the ZeroPos property. This tells us which room is 0,0 in the 3D array.
    //  If multiple rooms with the property exist, the last one found is used.
    ObjectInfoPack *rObjectPack = (ObjectInfoPack *)mObjectData->PushIterator();
    while(rObjectPack)
    {
        //--Type must be "Room".
        const char *rType = rObjectPack->mType;
        if(!strcasecmp(rType, "Room"))
        {
            //fprintf(stderr, "%s\n", rObjectPack->mName);
            //--Scan properties.
            for(int i = 0; i < rObjectPack->mProperties->mPropertiesTotal; i ++)
            {
                char *rKey = rObjectPack->mProperties->mKeys[i];
                if(!strcasecmp(rKey, "ZeroPos"))
                {
                    mActiveXOff = rObjectPack->mX / 64;
                    mActiveYOff = rObjectPack->mY / 64;
                }
            }
        }

        //--Next.
        rObjectPack = (ObjectInfoPack *)mObjectData->AutoIterate();
    }

    //--Now handle objects normally.
    rObjectPack = (ObjectInfoPack *)mObjectData->PushIterator();
    while(rObjectPack)
    {
        HandleObject(rObjectPack);
        rObjectPack = (ObjectInfoPack *)mObjectData->AutoIterate();
    }
}
void TextLevel::HandleObject(ObjectInfoPack *pPack)
{
    ///--[Documentation and Setup]
    //--Handles a given object info pack. These can be rooms with properties, stairwells, doors, etc.
    if(!pPack) return;

    //--Fast-access pointers.
    const char *rType = pPack->mType;

    //--Stairwell. Used to get between parts of the manor.
    if(!strcasecmp(rType, "Stairs"))
    {
    }
    //--Room. A place that entities can move to.
    else if(!strcasecmp(rType, "Room"))
    {
        //--Get the position in terms of integers, from map coordinates.
        int tXPos = (pPack->mX / 64) - mActiveXOff;
        int tYPos = (pPack->mY / 64) - mActiveYOff;
        RegisterAutomapRoom(pPack->mName, tXPos, tYPos, mActiveZLevel);

        //--If this room pushes the 3D boundary, update it here.
        if(tXPos < mLftMost) mLftMost = tXPos;
        if(tXPos > mRgtMost) mRgtMost = tXPos;
        if(tYPos < mTopMost) mTopMost = tYPos;
        if(tYPos > mBotMost) mBotMost = tYPos;

        //--Properties.
        AutomapPack *rLastPack = (AutomapPack *)mAutomapList->GetTail();
        if(rLastPack)
        {
            for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
            {
                //--Fast access.
                const char *rKey = pPack->mProperties->mKeys[i];
                const char *rVal = pPack->mProperties->mVals[i];

                //--Room cannot be entered. Used to show things like trees to fill out the map.
                if(!strcasecmp(rKey, "Unenterable"))
                {
                    rLastPack->mIsUnenterable = true;
                }
                //--Enemies cannot see the player when in this room. Flag is not absolute, it can be toggled.
                else if(!strcasecmp(rKey, "NoSeePlayer"))
                {
                    rLastPack->mNoSeePlayer = true;
                }
                //--Room Identity, used to help give broad names to rooms when using the "Think" handler.
                else if(!strcasecmp(rKey, "Identity"))
                {
                    strncpy(rLastPack->mIdentity, rVal, sizeof(rLastPack->mIdentity));
                }
                //--If present, denotes the offset relative to the zero position on the main floor.
                else if(!strcasecmp(rKey, "TileOffX"))
                {
                    TextLevZLevel *rLastLevel = (TextLevZLevel *)mZLevels->GetTail();
                    if(rLastLevel)
                    {
                        rLastLevel->mTileOffX = atoi(rVal);
                    }
                }
                //--As above, but for Y.
                else if(!strcasecmp(rKey, "TileOffY"))
                {
                    TextLevZLevel *rLastLevel = (TextLevZLevel *)mZLevels->GetTail();
                    if(rLastLevel)
                    {
                        rLastLevel->mTileOffY = atoi(rVal);
                    }
                }
                //--Stranger Grouping, dictates which group of rooms the Stranger will search when the player enters this room.
                //  Defaults to "Null". "None" indicates the Stranger will not follow the player here.
                else if(!strcasecmp(rKey, "StrangerGroup"))
                {
                    strncpy(rLastPack->mStrangerGroup, rVal, sizeof(rLastPack->mStrangerGroup));
                }
                //--Animation. Can be many animations, so suffix them as "AnimationA", "AnimationB", etc.
                else if(!strncasecmp(rKey, "Animation", strlen("Animation")))
                {
                    //--Locate the animation.
                    void *rAnimation = mTilesetAnimations->GetElementByName(rVal);
                    if(rAnimation)
                    {
                        rLastPack->mrAnimationsList->AddElement(rVal, rAnimation);
                    }
                }
                //--Audio. Name lookup that specifies which audio mix to use. The name is defined in Lua, this
                //  merely stores it until later.
                else if(!strcasecmp(rKey, "Audio"))
                {
                    SetMemoryData(__FILE__, __LINE__);
                    TextLevelTempString *nStringPack = (TextLevelTempString *)starmemoryalloc(sizeof(TextLevelTempString));
                    nStringPack->mX = tXPos;
                    nStringPack->mY = tYPos;
                    nStringPack->mZ = mActiveZLevel;
                    strncpy(nStringPack->mString, rVal, sizeof(nStringPack->mString));
                    mTempAudioList->AddElement("X", nStringPack, &FreeThis);
                }
                //--Allows manual override of visibility to the north. Can be "True"/"Always" (always visible) or "False"/"Never" (never visible),
                //  "Narrow" (acts like narrow door), or "Short" (allows visibility, ray stops. Shortens vis range like a window might.)
                else if(!strcasecmp(rKey, "VisNorth"))
                {
                    if(!strcasecmp(rVal, "True") || !strcasecmp(rVal, "Always"))
                    {
                        rLastPack->mOverrideVisN = OVERRIDE_VISIBILITY_ALWAYS;
                    }
                    else if(!strcasecmp(rVal, "False") || !strcasecmp(rVal, "Never"))
                    {
                        rLastPack->mOverrideVisN = OVERRIDE_VISIBILITY_NEVER;
                    }
                    else if(!strcasecmp(rVal, "Narrow"))
                    {
                        rLastPack->mOverrideVisN = OVERRIDE_VISIBILITY_ALWAYSNARROW;
                    }
                    else if(!strcasecmp(rVal, "Short"))
                    {
                        rLastPack->mOverrideVisN = OVERRIDE_VISIBILITY_SHORT;
                    }
                }
                //--As above, for east.
                else if(!strcasecmp(rKey, "VisEast"))
                {
                    if(!strcasecmp(rVal, "True") || !strcasecmp(rVal, "Always"))
                    {
                        rLastPack->mOverrideVisE = OVERRIDE_VISIBILITY_ALWAYS;
                    }
                    else if(!strcasecmp(rVal, "False") || !strcasecmp(rVal, "Never"))
                    {
                        rLastPack->mOverrideVisE = OVERRIDE_VISIBILITY_NEVER;
                    }
                    else if(!strcasecmp(rVal, "Narrow"))
                    {
                        rLastPack->mOverrideVisE = OVERRIDE_VISIBILITY_ALWAYSNARROW;
                    }
                    else if(!strcasecmp(rVal, "Short"))
                    {
                        rLastPack->mOverrideVisE = OVERRIDE_VISIBILITY_SHORT;
                    }
                }
                //--As above, for east.
                else if(!strcasecmp(rKey, "VisSouth"))
                {
                    if(!strcasecmp(rVal, "True") || !strcasecmp(rVal, "Always"))
                    {
                        rLastPack->mOverrideVisS = OVERRIDE_VISIBILITY_ALWAYS;
                    }
                    else if(!strcasecmp(rVal, "False") || !strcasecmp(rVal, "Never"))
                    {
                        rLastPack->mOverrideVisS = OVERRIDE_VISIBILITY_NEVER;
                    }
                    else if(!strcasecmp(rVal, "Narrow"))
                    {
                        rLastPack->mOverrideVisS = OVERRIDE_VISIBILITY_ALWAYSNARROW;
                    }
                    else if(!strcasecmp(rVal, "Short"))
                    {
                        rLastPack->mOverrideVisS = OVERRIDE_VISIBILITY_SHORT;
                    }
                }
                //--As above, for south.
                else if(!strcasecmp(rKey, "VisWest"))
                {
                    if(!strcasecmp(rVal, "True") || !strcasecmp(rVal, "Always"))
                    {
                        rLastPack->mOverrideVisW = OVERRIDE_VISIBILITY_ALWAYS;
                    }
                    else if(!strcasecmp(rVal, "False") || !strcasecmp(rVal, "Never"))
                    {
                        rLastPack->mOverrideVisW = OVERRIDE_VISIBILITY_NEVER;
                    }
                    else if(!strcasecmp(rVal, "Narrow"))
                    {
                        rLastPack->mOverrideVisW = OVERRIDE_VISIBILITY_ALWAYSNARROW;
                    }
                    else if(!strcasecmp(rVal, "Short"))
                    {
                        rLastPack->mOverrideVisW = OVERRIDE_VISIBILITY_SHORT;
                    }
                }
                //--Down. This allows the player to see the map below them when on a higher Z-level.
                else if(!strcasecmp(rKey, "VisDown"))
                {
                    rLastPack->mVisibilityDown = atoi(rVal);
                }
            }
        }
    }
    //--Door. Blocks LOS, can be locked.
    else if(!strcasecmp(rType, "Door"))
    {
        //--Register the door.
        SetMemoryData(__FILE__, __LINE__);
        TextLevelTempDoor *nDoorPack = (TextLevelTempDoor *)starmemoryalloc(sizeof(TextLevelTempDoor));
        nDoorPack->mX = ((pPack->mX / 64) - mActiveXOff);
        nDoorPack->mY = ((pPack->mY / 64) - mActiveYOff) - 1; //EntityY is bottom-up for some reason.
        nDoorPack->mZ = mActiveZLevel;

        //--Properties.
        char tLockBuffer[32];
        strcpy(tLockBuffer, "Normal");
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            if(!strcasecmp(pPack->mProperties->mKeys[i], "Lock"))
            {
                strcpy(tLockBuffer, pPack->mProperties->mVals[i]);
            }
        }

        //--North door.
        if(pPack->mTileID >= 160-1 && pPack->mTileID <= 164-1)
        {
            nDoorPack->mDoorDir = 'N';
            strcpy(nDoorPack->mDoorName, "C ");
            strcat(nDoorPack->mDoorName, tLockBuffer);
        }
        //--East door.
        else if(pPack->mTileID >= 180-1 && pPack->mTileID <= 184-1)
        {
            nDoorPack->mDoorDir = 'E';
            strcpy(nDoorPack->mDoorName, "C ");
            strcat(nDoorPack->mDoorName, tLockBuffer);
        }

        //--Register.
        mTempDoorList->AddElement("X", nDoorPack, &FreeThis);
    }
    //--PatrolNode, used to build enemy spawns and patrol paths.
    else if(!strcasecmp(rType, "PatrolNode"))
    {
        //--Scan the properties to find the path identity.
        const char *rPathIdentity = NULL;
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast-access.
            const char *rKey = pPack->mProperties->mKeys[i];
            const char *rVal = pPack->mProperties->mVals[i];

            //--Unique name of the enemy.
            if(!strcasecmp(rKey, "PathIdentity"))
            {
                rPathIdentity = rVal;
                break;
            }
        }

        //--No path identity? Ignore the node.
        if(!rPathIdentity) { fprintf(stderr, "Error, node %s has no path identity.\n", pPack->mName); return; }

        //--Locate the index based on the name. The pattern is "NodeXX". It must be a valid index or we fail here.
        if(strlen(pPack->mName) < 6) { fprintf(stderr, "Erroneous name for node %s, no index found.\n", pPack->mName); return; }
        int tNodeIndex = atoi(&pPack->mName[4]);
        if(tNodeIndex < 0 || tNodeIndex >= TLPDP_MAX_NODES) { fprintf(stderr, "Error, node index %i from %s is out of range.\n", tNodeIndex, pPack->mName); return; }

        //--Check if the path package already exists for this identity.
        TextLevelPatrolDataPack *rUseLookup = (TextLevelPatrolDataPack *)mTempPatrolList->GetElementByName(rPathIdentity);

        //--Create new if not found:
        if(!rUseLookup)
        {
            SetMemoryData(__FILE__, __LINE__);
            rUseLookup = (TextLevelPatrolDataPack *)starmemoryalloc(sizeof(TextLevelPatrolDataPack));
            rUseLookup->Initialize();
            mTempPatrolList->AddElement(rPathIdentity, rUseLookup, &FreeThis);
        }

        //--Set the X/Y/Z properties for the node index. Y position is bottom-up.
        if(tNodeIndex+1 > rUseLookup->mTotalNodes) rUseLookup->mTotalNodes = tNodeIndex+1;
        rUseLookup->mNodes[tNodeIndex].mX = ((pPack->mX / 64) - mActiveXOff);
        rUseLookup->mNodes[tNodeIndex].mY = ((pPack->mY / 64) - mActiveYOff) - 1;
        rUseLookup->mNodes[tNodeIndex].mZ = mActiveZLevel;
        //fprintf(stderr, "Got node %s - %s %i %i %i\n", rPathIdentity, pPack->mName, rUseLookup->mNodes[tNodeIndex].mX, rUseLookup->mNodes[tNodeIndex].mY, mActiveZLevel);
        //fprintf(stderr, " Orig %f %f\n", pPack->mX, pPack->mY);
        //fprintf(stderr, " Highest %i\n", rUseLookup->mTotalNodes);

        //--Properties. Only one node in the path group needs to have these, typically the zeroth node.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast-access.
            const char *rKey = pPack->mProperties->mKeys[i];
            const char *rVal = pPack->mProperties->mVals[i];

            //--Unique name of the enemy.
            if(!strcasecmp(rKey, "EnemyIdentity"))
            {
                strncpy(rUseLookup->mEnemyUniqueName, rVal, sizeof(rUseLookup->mEnemyUniqueName));
            }
            //--Type of the enemy.
            else if(!strcasecmp(rKey, "EnemyType"))
            {
                strncpy(rUseLookup->mEnemyType, rVal, sizeof(rUseLookup->mEnemyType));
            }
        }
    }
    //--Location. Used to store things like items, fleeing points, non-patrol spawns, special events, etc.
    else if(!strcasecmp(rType, "Location"))
    {
        SetMemoryData(__FILE__, __LINE__);
        TextLevelTempString *nStringPack = (TextLevelTempString *)starmemoryalloc(sizeof(TextLevelTempString));
        nStringPack->mX = (pPack->mX / 64) - mActiveXOff;
        nStringPack->mY = (pPack->mY / 64) - mActiveYOff - 1;
        nStringPack->mZ = mActiveZLevel;
        strncpy(nStringPack->mString, pPack->mName, sizeof(nStringPack->mString));
        mTempLocationList->AddElement(pPack->mName, nStringPack, &FreeThis);
    }
    //--Unhandled.
    else
    {
        fprintf(stderr, "Warning: Unhandled object type %s - %i %i %i\n", rType, (int)((pPack->mX / 64) - mActiveXOff), (int)((pPack->mY / 64) - mActiveYOff) - 1, (int)mActiveZLevel);
    }
}
