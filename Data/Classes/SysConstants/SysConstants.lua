-- |[ ================================ SysConstants Class File ================================= ]|
--Class file for SysConstants. Call this once at initialization.

--System Constants helper class. Most of the functions are just namespacings of functions related
-- to querying constant tables.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
SysConstants = {}
SysConstants.__index = SysConstants 

-- |[ ============ Class Methods ============= ]|
--System
--Directions
function SysConstants:fnGetStringFromDirCode(piCode)      end
function SysConstants:fnGetDirCodeFromString(psString)    end
function SysConstants:fnGetRevStringFromDirCode(piCode)   end
function SysConstants:fnGetRevDirCodeFromString(psString) end
function SysConstants:fnGetReverseDirFromCode(piCode)     end
function SysConstants:fnGetReverseDirFromString(psString) end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "SysConstantsDirection.lua")
