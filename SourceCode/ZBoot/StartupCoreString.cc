//--Base
#include "TranslationManager.h"

///--[Assorted Classes]
#include "AdventureItem.h"

///--[Monoceros GUI]
#include "MonoUIBase.h"
#include "MonoUICampfire.h"
#include "MonoUIEquip.h"
#include "MonoUIFileSelect.h"
#include "MonoUIInventory.h"
#include "MonoUIJournal.h"
#include "MonoUIMap.h"
#include "MonoUIOptions.h"
#include "MonoUIQuit.h"
#include "MonoUISkills.h"
#include "MonoUIStatus.h"
#include "MonoUIWarp.h"
#include "MonoUIVendor.h"
#include "MonocerosTitle.h"

///======================================= Registration ===========================================
void RegisterCoreStrings()
{
    ///--[Documentation]
    //--Called at startup, creates translation classes for all classes that have them and registers them
    //  to the TranslationManager. This will make them available to be modified by translations or
    //  by mods/derived games.
    TranslationManager *rTranslationManager = TranslationManager::Fetch();

    ///--[Classes]
    rTranslationManager->RegisterEntry("AdventureItem", new AdvItem_Strings());

    ///--[Monoceros GUI]
    rTranslationManager->RegisterEntry("MonoUIBase",       new MonoUIBase_Strings());
    rTranslationManager->RegisterEntry("MonoUICampfire",   new MonoUICampfire_Strings());
    rTranslationManager->RegisterEntry("MonoUIEquip",      new MonoUIEquip_Strings());
    rTranslationManager->RegisterEntry("MonoUIFileSelect", new MonoUIFileSelect_Strings());
    rTranslationManager->RegisterEntry("MonoUIInventory",  new MonoUIInventory_Strings());
    rTranslationManager->RegisterEntry("MonoUIJournal",    new MonoUIJournal_Strings());
    rTranslationManager->RegisterEntry("MonoUIMap",        new MonoUIMap_Strings());
    rTranslationManager->RegisterEntry("MonoUIOptions",    new MonoUIOptions_Strings());
    rTranslationManager->RegisterEntry("MonoUIQuit",       new MonoUIQuit_Strings());
    rTranslationManager->RegisterEntry("MonoUISkills",     new MonoUISkills_Strings());
    rTranslationManager->RegisterEntry("MonoUIStatus",     new MonoUIStatus_Strings());
    rTranslationManager->RegisterEntry("MonoUIWarp",       new MonoUIWarp_Strings());
    rTranslationManager->RegisterEntry("MonoUIVendor",     new MonoUIVendor_Strings());
    rTranslationManager->RegisterEntry("MonocerosTitle",   new MonoTitle_Strings());
}
