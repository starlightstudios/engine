///===================================== Global Structures========================================
//--Listing of structures which are in common usage throughout the code. Classes which use structures
//  in 1-2 files should have structures declared in their header files as "Local Structures".
//--If a structure is used in 10+ files, consider including it in this file instead.

#pragma once

#ifdef _TARGET_OS_WINDOWS_
class StarBitmap;
#include "Definitions.h"
#endif

#ifdef _TARGET_OS_LINUX_
class StarBitmap;
#include "Definitions.h"
#endif

#ifdef _TARGET_OS_MAC_
class StarBitmap;
#include "Definitions.h"
#endif

///================================= Points and Simple Shapes =====================================
//--Point2DI, two integers to represent a point in two dimensions.
typedef struct Point2DI
{
    int mX;
    int mY;
}Point2DI;

//--Point3DI, three integers to represent a point in three dimensions.
typedef struct Point3DI
{
    int mX;
    int mY;
    int mZ;
}Point3DI;

//--Point2DF, two floats to represent a point in two dimensions.
typedef struct Point2DF
{
    float mX;
    float mY;
}Point2DF;

//--Point3DF, three floats to represent a point in three dimensions.
typedef struct Point3DF
{
    float mX;
    float mY;
    float mZ;
}Point3DF;

///====================================== Easing Packages =========================================
//--Used as a package for a single dimension of an easing routine. Typically, when an object
//  is moving from point A to point B where both are known, the total number of ticks must be
//  known as well as the start, current, and end positions.
//--These packages handle 1D and 2D cases. Implementation of functions is in EasingPack.cc
#define EASING_CODE_LOWEST 0
#define EASING_CODE_LINEAR 0
#define EASING_CODE_QUADIN 1
#define EASING_CODE_QUADOUT 2
#define EASING_CODE_QUADINOUT 3
#define EASING_CODE_HIGHEST 4
typedef struct EasingPack1D
{
    int mTimer;
    int mTimerMax;
    float mXBgn;
    float mXCur;
    float mXEnd;
    void Initialize();
    void Increment(int pEasingCode);
    void IncrementLinear();
    void IncrementQuadIn();
    void IncrementQuadOut();
    void IncrementQuadInOut();
    void MoveTo(float pTargetX, int pTicks);
    void Complete();
}EasingPack1D;
typedef struct EasingPack2D
{
    int mTimer;
    int mTimerMax;
    float mXBgn;
    float mXCur;
    float mXEnd;
    float mYBgn;
    float mYCur;
    float mYEnd;
    void Initialize();
    void Increment(int pEasingCode);
    void MoveTo(float pTargetX, float pTargetY, int pTicks);
    void Complete();
}EasingPack2D;
void RenderExpandableHighlight(float pLft, float pTop, float pRgt, float pBot, float pIndent, StarBitmap *pImage); //Highlight.cc
void RenderExpandableHighlight(EasingPack2D pLocation, EasingPack2D pSize, float pIndent, StarBitmap *pImage);     //Highlight.cc
void RenderExpandableHeader(float pXCenter, float pY, float pNeededSize, float pLftDeadZone, float pRgtDeadZone, StarBitmap *pImage); //Header.cc

///================================== Dimensions and Hitboxes =====================================
//--Represents a hitbox in 2-dimensions. Note that the X/Y center can be modified directly, but
//  using the auto-set functions will make them always legally represent the center points. If
//  modified externally, they may not be accurate.
typedef struct TwoDimensionReal
{
    //--Variables
    float mLft, mTop, mRgt, mBot;
    float mXCenter, mYCenter;

    //--Property Queries
    float GetWidth()
    {
        return mRgt - mLft;
    }
    float GetHeight()
    {
        return mBot - mTop;
    }
    bool IsPointWithin(float pX, float pY)
    {
        return (pX >= mLft && pX <= mRgt && pY >= mTop && pY <= mBot);
    }

    //--Manipulators
    void Add2DReal(TwoDimensionReal pAdder)
    {
        mLft = mLft + pAdder.mLft;
        mTop = mTop + pAdder.mTop;
        mRgt = mRgt + pAdder.mRgt;
        mBot = mBot + pAdder.mBot;
    }
    void SetPosition(float pLft, float pTop)
    {
        float tWid = mRgt - mLft;
        float tHei = mBot - mTop;
        mLft = pLft;
        mTop = pTop;
        mRgt = mLft + tWid;
        mBot = mTop + tHei;
        mXCenter = (mLft + mRgt) / 2.0f;
        mYCenter = (mTop + mBot) / 2.0f;
    }
    void Set(float pLft, float pTop, float pRgt, float pBot)
    {
        mLft = pLft;
        mTop = pTop;
        mRgt = pRgt;
        mBot = pBot;
        mXCenter = (mLft + mRgt) / 2.0f;
        mYCenter = (mTop + mBot) / 2.0f;
    }
    void SetWH(float pLft, float pTop, float pWid, float pHei)
    {
        Set(pLft, pTop, pLft + pWid, pTop + pHei);
    }
    void SetCenter(float pXCenter, float pYCenter)
    {
        float tXDif = pXCenter - mXCenter;
        float tYDif = pYCenter - mYCenter;
        Set(mLft + tXDif, mTop + tYDif, mRgt + tXDif, mBot + tYDif);
    }
    void OffsetX(float pAmount)
    {
        mLft = mLft + pAmount;
        mRgt = mRgt + pAmount;
        mXCenter = mXCenter + pAmount;
    }
    void OffsetY(float pAmount)
    {
        mTop = mTop + pAmount;
        mBot = mBot + pAmount;
        mYCenter = mYCenter + pAmount;
    }
}TwoDimensionReal;

//--Represents a hitbox in 3-dimensions, with spare remainders for movement.
typedef struct ThreeDimensionReal
{
    float mLft, mTop, mRgt, mBot, mFro, mBak;
    float mXCenter, mYCenter, mZCenter;
    float mXRemainder, mYRemainder, mZRemainder;
}ThreeDimensionReal;

///========================================== Geometry ============================================
//--Represents a polygon, composed of mPointsTotal points in two paralell arrays.
typedef struct
{
    int mPointsTotal;
    float *mXVertices;
    float *mYVertices;
}PolygonalHitbox;

typedef struct
{
    float mXCenter, mYCenter;
}TwoDimensionRealPoint;

typedef struct
{
    float mX, mY, mZ;
    void Set(float pX, float pY, float pZ)
    {
        mX = pX;
        mY = pY;
        mZ = pZ;
    }
}Point3D;

///======================================= Main Packages ==========================================
//--Aka Driveshaft, shuttles information between the global and gears.
typedef struct MainPackage
{
    //--Frame Update Variables
    bool ScreenNeedsUpdate;
    int FramesDoneThisTick;

    //--Timers
    double OldTime;
    double GameTime;
    time_t StartTime, EndTime;

    //--[Allegro Parts]
    #if defined _ALLEGRO_PROJECT_
    ALLEGRO_KEYBOARD_STATE KeyboardState;
    ALLEGRO_MOUSE_STATE MouseState;
    ALLEGRO_JOYSTICK_STATE JoystickState;
    #endif

    //--[SDL Parts]
    #if defined _SDL_PROJECT_
    #endif
}MainPackage;
typedef MainPackage DriveShaft;

///========================================= Lua Types ============================================
//--Lua Tarball type.  Used by both StarLumps and LuaManager.
typedef struct
{
    char *mName;
    uint32_t mBinarySize;
    uint8_t *mBinaryData;
    uint32_t mCompiledSize;
    void *mCompiledData;
}LuaTarballEntry;

///========================================== Controls ============================================
//--Represents the state of a control (not a key). Can have two keys, two mouse buttons, and/or
//  two joystick codes.
typedef struct ControlState
{
    int mWatchKeyPri;
    int mWatchKeySec;
    int mWatchMouseBtnPri;
    int mWatchMouseBtnSec;
    int mWatchJoyPri;
    int mWatchJoySec;

    bool mIsFirstPress;
    bool mIsFirstRelease;
    bool mIsDown;
    int mTicksSincePress;

    bool mHasPendingRelease;
}ControlState;

//--A keycode for typing. If the normalchar is 'a', the shifted char is 'A'.
typedef struct
{
    char mNormalChar;
    char mShiftedChar;
}Keycode;

///===================================== Level Structures =========================================
//--Represents an InventoryItem which can be accessed from the CharacterPane.
class InventoryItem;
typedef struct InventoryPack
{
    //--Members.
    InventoryItem *rItem;
    int mQuantity;

    //--Rendering.
    int mLastRenderSlot;
    int mLastSetVisible;
    int mIsVisibleTimer;
}InventoryPack;

///===================================== Combat Structures ========================================
typedef struct CombatStats
{
    //--Variables
    int mHP, mHPMax;
    int mWillPower, mWillPowerMax;
    int mStamina, mStaminaMax;
    int mAttackPower;
    int mDefensePower;
    int mAccuracy;

    //--Functions
    void Clear()
    {
        mHP = 1;
        mHPMax = 1;
        mWillPower = 1;
        mWillPowerMax = 1;
        mStamina = 1;
        mStaminaMax = 1;
        mAttackPower = 0;
        mDefensePower = 0;
        mAccuracy = 0;
    }
    void Set(int pHPMax, int pWPMax, int pSTAMax, int pATP, int pDEF, int pACC)
    {
        if(pHPMax < 1) pHPMax = 1;
        if(pWPMax < 1) pWPMax = 1;
        if(pSTAMax < 1) pSTAMax = 1;
        mHP = pHPMax;
        mHPMax = pHPMax;
        mWillPower = pWPMax;
        mWillPowerMax = pWPMax;
        mStamina = pSTAMax;
        mStaminaMax = pSTAMax;
        mAttackPower = pATP;
        mDefensePower = pDEF;
        mAccuracy = pACC;
    }
}CombatStats;
