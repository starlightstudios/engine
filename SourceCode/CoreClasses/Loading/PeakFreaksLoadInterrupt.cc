//--Base
#include "PeakFreaksLoadInterrupt.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DisplayManager.h"
#include "SugarLumpManager.h"

///========================================== System ==============================================
PeakFreaksLoadInterrupt::PeakFreaksLoadInterrupt()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ========== LoadInterrupt =========== ]
    //--System
    //--Loading Bar
    //--Animation Data
    //--Graphics
    //--String Tyrant Mode
    //--Timing
    //--Public Variables

    ///--[ ===== PeakFreaksLoadInterrupt ====== ]
    //--Images
    memset(&PFImages, 0, sizeof(PFImages));

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
PeakFreaksLoadInterrupt::~PeakFreaksLoadInterrupt()
{
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void PeakFreaksLoadInterrupt::DeallocateImages()
{
    //--Deallocates all associated image data.
    LoadInterrupt::DeallocateImages();
    if(PFImages.Data.mLoading) delete PFImages.Data.mLoading;
    if(PFImages.Data.mAllen)   delete PFImages.Data.mAllen;
    if(PFImages.Data.mMimi)    delete PFImages.Data.mMimi;
    if(PFImages.Data.mScooter) delete PFImages.Data.mScooter;
    if(PFImages.Data.mPlayer)  delete PFImages.Data.mPlayer;
    memset(&PFImages, 0, sizeof(PFImages));
}
void PeakFreaksLoadInterrupt::Initialize(const char *pDatafilePath)
{
    ///--[Documentation]
    //--Load image data and set variables for the Peak Freaks loading screen.
    DeallocateImages();
    if(!pDatafilePath) return;

    //--Open file.
    SugarLumpManager *tSLM = new SugarLumpManager();
    tSLM->Open(pDatafilePath);

    //--Set Mode.
    mMode = LI_MODE_NONE;

    //--Suppress the load interrupt.
    bool tOldFlag = SugarBitmap::xSuppressInterrupt;
    SugarBitmap::xSuppressInterrupt = true;

    ///--[Load]
    //--Loading Text
    PFImages.Data.mLoading = tSLM->GetImage("LoadWord");
    PFImages.Data.mAllen   = tSLM->GetImage("Allen");
    PFImages.Data.mMimi    = tSLM->GetImage("Mimi");
    PFImages.Data.mScooter = tSLM->GetImage("Scooter");
    PFImages.Data.mPlayer  = tSLM->GetImage("Climber");

    //--Make sure everything resolved.
    PFImages.mIsReady = VerifyStructure(&PFImages.Data, sizeof(PFImages.Data), sizeof(void *));

    //--External setting. If anything went wrong, these won't get set, and nothing renders.
    SugarBitmap::xInterruptCall = &PeakFreaksLoadInterrupt::Interrupt;

    ///--[Clean]
    delete tSLM;
    SugarBitmap::xSuppressInterrupt = tOldFlag;
}
void PeakFreaksLoadInterrupt::Interrupt()
{
    //--Static Interrupt call, fetch the object and perform the Interrupt call.
    PeakFreaksLoadInterrupt *rInterrupt = (PeakFreaksLoadInterrupt *)LoadInterrupt::Fetch();
    if(!rInterrupt) return;
    rInterrupt->RenderCall(false);
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void PeakFreaksLoadInterrupt::RenderCall(bool pForceRender)
{
    ///--[Documentation and Setup]
    //--Called when an interrupt is mandated by the loading handlers. Doesn't do anything if it's
    //  too soon since the last interrupt.
    if(!CommonLogicUpdate(pForceRender) || !PFImages.mIsReady) return;

    //--Call the renderer setup. The screen is now ready for drawing.
    CommonRendererSetup();

    ///--[Setup]
    //--Position of loading image.
    float cLoadXPos = (VIRTUAL_CANVAS_X - PFImages.Data.mLoading->GetWidth())  * 0.50f;
    float cLoadYPos = (VIRTUAL_CANVAS_Y - PFImages.Data.mLoading->GetHeight()) * 0.50f;

    //--Fast-access pointers.
    SugarBitmap *rSprites[4];
    rSprites[0] = PFImages.Data.mPlayer;
    rSprites[1] = PFImages.Data.mScooter;
    rSprites[2] = PFImages.Data.mMimi;
    rSprites[3] = PFImages.Data.mAllen;

    //--Positions of the sprites. Fixed.
    float cXPositions[4];
    float cBaseY = 300.0f;
    float cJumpH = -48.0f;
    cXPositions[0] = 585.0f;
    cXPositions[1] = 635.0f;
    cXPositions[2] = 685.0f;
    cXPositions[3] = 735.0f;

    ///--[Render Pieces]
    //--Static.
    PFImages.Data.mLoading->Draw(cLoadXPos, cLoadYPos);

    //--Compute the X positions using a sinusoidal jump.
    for(int i = 0; i < 4; i ++)
    {
        //--Compute their position in the sequence.
        int tUseFrame = (mFrame + (i * 45)) % 100;
        float cPct = (tUseFrame / 100.0f);
        float cYPosition = cBaseY + (sinf(cPct * 3.1415926f) * cJumpH);

        //--Render.
        rSprites[i]->Draw(cXPositions[i], cYPosition);
    }

    ///--[Finish Up]
    CommonRendererClean();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void PeakFreaksLoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* PFLI_Boot(sPath)
       Boots the PFLoadInterrupt with the given datafile. Replaces the existing Load Interrupt. */
    lua_register(pLuaState, "PFLI_Boot", &Hook_PFLI_Boot);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_PFLI_Boot(lua_State *L)
{
    //PFLI_Boot(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PFLI_Boot");

    //--Create a new interrupt.
    PeakFreaksLoadInterrupt *nInterrupt = new PeakFreaksLoadInterrupt();
    nInterrupt->Initialize(lua_tostring(L, 1));

    //--Replace the global with this one.
    GLOBAL *rGlobal = Global::Shared();
    LoadInterrupt *rOldInterrupt = rGlobal->gLoadInterrupt;
    rGlobal->gLoadInterrupt = nInterrupt;

    //--Delete the existing LoadInterrupt.
    delete rOldInterrupt;
    return 0;
}
