//--Base
#include "AdvUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///======================================== Activation ============================================
void AdvUIVendor::ActivateModeUnlock()
{
    ///--[Documentation]
    //--Switches to Unlock mode.

    ///--[Variables]
    //--Common subroutines.
    ActivateMode(mIsUnlockMode);
    ResetCursor();

    //--Category cursor.
    mCategoryCursor = 0;
    RecomputeCategoryCursorPositions();
    mCategoryHighlightPos.Complete();
    mCategoryHighlightSize.Complete();

    ///--[Other]
    //--Reset comparison on zeroth item.
    RecheckComparisonCharacter(GetConsideredItem());
}

///===================================== Property Queries =========================================
float AdvUIVendor::GetUnlockCursorNameWidth()
{
    ///--[Documentation]
    //--In unlock mode, resolves the highlighted item and returns its width. Used for cursor size computations.
    float tNameWid = 100.0f;

    //--Get name.
    AdventureItem *rConsideredItem = GetConsideredItem();
    if(rConsideredItem)
    {
        tNameWid = AdvImages.rFont_Mainline->GetTextWidth(rConsideredItem->GetName());
    }

    //--Name length clamp.
    if(tNameWid > cxAdvMaxNameWid) tNameWid = cxAdvMaxNameWid;

    //--Finish.
    return tNameWid;
}
bool AdvUIVendor::HasEnoughOfUnlockItem(ShopUnlockPack *pUnlockPack, bool pIsFirstElement)
{
    ///--[Documentation]
    //--Returns true if the player has enough of the given item in their inventory, false if not.
    //  If this is the first item in a series of unlock requirements, then that item can be equipped
    //  to a player character. When this happens, an upgrade-in-place can occur.
    if(!pUnlockPack) return false;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Execution]
    //--Get how many we have.
    int tItemCount = rInventory->GetCountOf(pUnlockPack->mItemName);

    //--If and only if this is the first requirement, it can also be an equipped item. To do this,
    //  the quantity requirement must be 1.
    //--If we already had enough anyway, then skip this check.
    if(tItemCount < pUnlockPack->mNumberNeeded && pUnlockPack->mNumberNeeded == 1 && pIsFirstElement)
    {
        tItemCount = rInventory->GetCountOfEquip(pUnlockPack->mItemName);
    }

    //--Not enough: Fail.
    if(tItemCount < pUnlockPack->mNumberNeeded) return false;

    //--Checks passed.
    return true;
}
bool AdvUIVendor::MeetsUnlockRequirements(ShopInventoryPack *pPackage)
{
    ///--[Documentation]
    //--Returns true if the given package meets the unlock requirements, false if it does not.
    if(!pPackage) return false;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Requirements Check]
    //--Scan across the requirements. If the player doesn't have all of them, fail.
    bool tIsFirstCheck = true;
    ShopUnlockPack *rUnlockPack = (ShopUnlockPack *)pPackage->mUnlockRequirements->PushIterator();
    while(rUnlockPack)
    {
        //--Call subroutine to see if we have enough of the item. This handles the equipped-item
        //  case as well.
        if(!HasEnoughOfUnlockItem(rUnlockPack, tIsFirstCheck))
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            pPackage->mUnlockRequirements->PopIterator();
            return false;
        }

        //--Next.
        tIsFirstCheck = false;
        rUnlockPack = (ShopUnlockPack *)pPackage->mUnlockRequirements->AutoIterate();
    }

    ///--[All Checks Passed]
    return true;
}
void AdvUIVendor::RemoveUnlockRequirements(ShopInventoryPack *pPackage)
{
    ///--[Documentation]
    //--Given a ShopInventoryPack, iterate across the requirements list and remove them from the inventory.
    //  This also handles setting static variables for the replacement case.
    ResetString(xUnlockReplaceInfo, NULL);
    if(!pPackage) return;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Execution]
    //--Iterate across its requirement list and remove them from the inventory.
    bool tIsFirstCheck = true;
    ShopUnlockPack *rUnlockPack = (ShopUnlockPack *)pPackage->mUnlockRequirements->PushIterator();
    while(rUnlockPack)
    {
        //--If this is the first requirement item, it is possible the item is equipped to a character.
        //  Check if there are any in the inventory. If not, it's equipped to a character, which we
        //  need to store and send to the unlock script.
        if(tIsFirstCheck && rUnlockPack->mNumberNeeded == 1)
        {
            //--Check if there are any in the inventory, or any equipped. If any are in the inventory
            //  then we don't need to do any special code.
            int tItemCount = rInventory->GetCountOf(rUnlockPack->mItemName);

            //--If the count is zero, then set the static string to a special code. The unlock script
            //  will use this so it knows to check for an upgrade-in-place rather than adding the item
            //  to the inventory directly. This also doesn't remove the item.
            if(tItemCount < 1)
            {
                char tBuf[256];
                sprintf(tBuf, "UpgradeInPlace:%s|", rUnlockPack->mItemName);
                ResetString(xUnlockReplaceInfo, tBuf);
            }
            //--If the count is nonzero, remove the item as normal.
            else
            {
                rInventory->RemoveItem(rUnlockPack->mItemName);
            }
        }
        //--Normal case. Remove the number of items requested from the inventory.
        else
        {
            for(int i = 0; i < rUnlockPack->mNumberNeeded; i ++) rInventory->RemoveItem(rUnlockPack->mItemName);
        }

        //--Next.
        tIsFirstCheck = false;
        rUnlockPack = (ShopUnlockPack *)pPackage->mUnlockRequirements->AutoIterate();
    }
}

///========================================== Update ==============================================
void AdvUIVendor::UpdateModeUnlock()
{
    ///--[Documentation]
    //--Update handler for Unlock mode.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Run timers.
    StandardCycleTimer(mUnlockScrollTimer, mUnlockScrollTimerPause, 0, cxAdvUnlockCycleTicks, mUnlockScrollTimerUp, cxAdvUnlockPauseTicks);
    StandardTimer(mAskToEquipTimer,    0, cxAdvAskEquipTicks, mAskToEquip);
    StandardTimer(mUnlockConfirmTimer, 0, cxAdvBuyTicks,      mIsUnlockingItem);

    //--Update blockers. If these timers are not capped, stop the update.
    if(mDetailsTimer > 0 && mDetailsTimer < cxAdvDetailsTicks) return;

    ///--[List Resolve]
    //--Resolve which list to use. Categories can be changed.
    StarLinkedList *rActiveCategory = GetConsideredCategory();

    ///--[Mode Handlers]
    ///--[Unlocking Handler]
    //--Call mode handlers.
    if(mIsUnlockingItem) { UpdateConfirmUnlock(); return; }
    if(mAskToEquip)      { UpdateBuyEquipItem();  return; }
    if(CommonDetailsUpdate()) return;

    ///--[Shoulder Buttons]
    //--Shoulder-left. Decrements the comparison character, or changes materials page.
    if(rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel"))
    {
        //--Comparison character. The user is not holding ctrl. Attempt to scroll the character.
        if(!rControlManager->IsDown("Ctrl")) { ScrollComparisonCharacter(-1); return; }

        //--Change materials page.
        mMaterialsPage --;
        if(mMaterialsPage < 0) mMaterialsPage = mMaterialsPages->GetListSize() - 1;
        if(mMaterialsPage < 0) mMaterialsPage = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Shoulder-right. Increments the comparison character, or changes materials page.
    if(rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel"))
    {
        //--Comparison character. The user is not holding ctrl. Attempt to scroll the character.
        if(!rControlManager->IsDown("Ctrl")) { ScrollComparisonCharacter(-1); return; }

        //--Change materials page.
        mMaterialsPage ++;
        if(mMaterialsPage >= mMaterialsPages->GetListSize()) mMaterialsPage = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Run Button]
    //--Toggles which comparison slot is in use.
    if(rControlManager->IsFirstPress("Run") && mCanSwapCompareSlot)
    {
        ToggleComparisonSlot();
        return;
    }

    ///--[Jump Button]
    //--Toggles materials vs comparison mode.
    if(rControlManager->IsFirstPress("Jump"))
    {
        mComparisonShowMaterials = !mComparisonShowMaterials;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Up and Down]
    //--Up decrements, down increments. Wrapping is enabled.
    if(AutoListUpDn(mCursor, mSkip, rActiveCategory->GetListSize(), cScrollBuf, cScrollPage, true))
    {
        //--Modify cursor.
        RecomputeCursorPositions();

        //--Resets scroll timers.
        mUnlockScrollTimerUp = true;
        mUnlockScrollTimer = 0;
        mUnlockScrollTimerPause = cxAdvUnlockPauseTicks;

        //--The comparison character may have changed. Recheck that here.
        RecheckComparisonCharacter(GetConsideredItem());
        return;
    }

    ///--[Category Switching]
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && mCategoryList->GetListSize() > 0)
    {
        //--If the category list contains exactly one category, do nothing.
        if(mCategoryList->GetListSize() <= 1)
        {

        }
        //--Otherwise, switch:
        else
        {
            //--Flags.
            mCursor = 0;
            mSkip = 0;

            //--Decrement.
            mCategoryCursor --;
            if(mCategoryCursor < 0) mCategoryCursor = mCategoryList->GetListSize() - 1;
            if(mCategoryCursor < 0) mCategoryCursor = 0;

            //--Re-resolve the list in question.
            if(mCategoryList->GetListSize() > 0)
            {
                ShopCategoryPack *rCategoryPack = (ShopCategoryPack *)mCategoryList->GetElementBySlot(mCategoryCursor);
                if(rCategoryPack) rActiveCategory = rCategoryPack->mrMembers;
            }

            //--Cursor.
            RecomputeCursorPositions();
            RecomputeCategoryCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && mCategoryList->GetListSize() > 0)
    {
        //--If the category list contains exactly one category, do nothing.
        if(mCategoryList->GetListSize() <= 1)
        {

        }
        //--Otherwise, switch:
        else
        {
            //--Flags.
            mCursor = 0;
            mSkip = 0;

            //--Increment.
            mCategoryCursor ++;
            if(mCategoryCursor >= mCategoryList->GetListSize()) mCategoryCursor = 0;

            //--Re-resolve the list in question.
            if(mCategoryList->GetListSize() > 0)
            {
                ShopCategoryPack *rCategoryPack = (ShopCategoryPack *)mCategoryList->GetElementBySlot(mCategoryCursor);
                if(rCategoryPack) rActiveCategory = rCategoryPack->mrMembers;
            }

            //--Cursor.
            RecomputeCursorPositions();
            RecomputeCategoryCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Activate]
    //--Begins unlock of the highlighted item.
    if(rControlManager->IsFirstPress("Activate") && mShopInventory->GetListSize() > 0 && !mIsShowingDetails)
    {
        //--Fast-access pointers.
        AdventureInventory *rInventory = AdventureInventory::Fetch();

        //--Get the item in question.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)rActiveCategory->GetElementBySlot(mCursor);
        if(!rPackage) return;

        //--If the player doesn't have the requirements, stop:
        if(!MeetsUnlockRequirements(rPackage))
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--If we got this far, it's valid. Bring up the confirmation box.
        mIsUnlockingItem = true;
        mUnlockConfirmTimer = 0;
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        HandleExit();
        return;
    }
}
void AdvUIVendor::UpdateConfirmUnlock()
{
    ///--[Documentation]
    //--The player may press Activate to confirm the unlock, or cancel to stop.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Check category.
    StarLinkedList *rActiveCategory = GetConsideredCategory();
    if(!rActiveCategory) return;

    ///--[Timers]
    ///--[Activate]
    //--Activate handler.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Flag.
        mIsUnlockingItem = false;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

        //--Fast-access pointers.
        AdventureInventory *rInventory = AdventureInventory::Fetch();

        //--Get the item in question.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)rActiveCategory->GetElementBySlot(mCursor);
        if(!rPackage) return;

        //--Iterate across its requirement list and remove them from the inventory. This also handles
        //  setting the static string the unlock script can use.
        RemoveUnlockRequirements(rPackage);

        //--Set the variable in question to 1.0f.
        SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry(rPackage->mAssociatedVariable);
        if(rVariable) rVariable->mNumeric = 1.0f;

        //--If flagged, remove this entry from the unlock list.
        if(rPackage->mRemoveOnUnlock)
        {
            mShopInventory->RemoveElementI(mCursor);
            mCursor --;
            if(mCursor < 0) mCursor = 0;
            RecomputeCursorPositions();
        }

        //--If flagged, execute a script.
        if(rPackage->mExecOnUnlock)
        {
            LuaManager::Fetch()->ExecuteLuaFile(rPackage->mExecOnUnlock, 1, "S", rPackage->mItem->GetName());
        }

        //--If this flag is true, prompt the user to equip the item.
        if(mUnlockCanEquip)
        {
            //--Check the last-regged item.
            AdventureItem *rLastReggedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
            if(!rLastReggedItem) return;

            //--Scan the active characters. See how many can equip the item.
            mAskToEquipSlot = -1;
            for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
            {
                //--Get character.
                AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
                if(!rCharacter) continue;

                //--Can the character equip it?
                if(rLastReggedItem->IsEquippableBy(rCharacter->GetName()))
                {
                    //--If the equip slot is -1, this is the first character that can equip it.
                    if(mAskToEquipSlot == -1)
                    {
                        mAskToEquipSlot = i;
                    }
                    //--If it's not -1, then at least two characters can equip the item. Give priority to the one
                    //  being used for comparison.
                    else
                    {
                        if(i == mCompareCharacterCur) mAskToEquipSlot = mCompareCharacterCur;
                    }
                }
            }

            //--If we exited this loop with the mAskToEquipSlot being -1, nobody can equip it.
            if(mAskToEquipSlot == -1) return;

            //--If we got this far, we should show the equip prompt.
            mAskToEquip = true;
        }
    }

    //--Cancel handler.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsUnlockingItem = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    return;
}

///===================================== Segment Rendering ========================================
void AdvUIVendor::RenderLftUnlock(float pVisAlpha)
{
    RenderCommonLft(pVisAlpha);
}
void AdvUIVendor::RenderTopUnlock(float pVisAlpha)
{
    RenderCommonTop(pVisAlpha);
}
void AdvUIVendor::RenderRgtUnlock(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    //--Comparison Block
    AdvImages.rFrame_Comparison->Draw();
    if(!mComparisonShowMaterials)
    {
        RenderBuyProperties(cColorAlpha);
    }
    else
    {
        RenderMaterials(cColorAlpha);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderBotUnlock(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Render]
    //--Inventory Listing.
    AdvImages.rFrame_Unlock->Draw();

    //--Titles
    SetColor("Heading", cColorAlpha);
    AdvImages.rFont_Heading->DrawText(cxAdv_UnlHdgNameX, cxAdv_UnlHdgY, 0, 1.0f, "Name");
    SetColor("Clear", cColorAlpha);

    ///--[List Resolve]
    //--Resolve which list to use. If no categories are in place, use the mShopInventory list.
    StarLinkedList *rUseList = GetConsideredCategory();

    ///--[Normal Rendering]
    if(mShopInventory->GetListSize() > 0)
    {
        //--Render stencil but not colors.
        DisplayManager::ActivateMaskRender(cxStencil);
        AdvImages.rStencil_Unlock->Draw();

        //--Set stencils for text rendering. Note this is an inverted stencil.
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_NOTEQUAL, cxStencil, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        //--Render entries.
        RenderUnlockListing(mSkip, cScrollPage, cColorAlpha);

        //--Remove stencils.
        DisplayManager::DeactivateStencilling();
    }
    ///--[Zero Entries]
    else
    {
        AdvImages.rFont_Mainline->DrawTextFixed(289.0f, 189.0f, cxAdvMaxNameWid, 0, "Nothing to unlock.");
    }
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    ///--[Category Selection]
    //--If there is more than zero categories, render the category selection boxes.
    if(mCategoryList->GetListSize() > 0)
    {
        ///--[Frame]
        //--Constants.
        float cFrameX = 251.0f;
        float cFrameY = 85.0f;
        float cEdgeW = 18.0f;
        float cIconW = 38.0f;

        //--The frame is resizable. Compute texture positions.
        float cWid = AdvImages.rFrame_Category->GetTrueWidthSafe();
        float cHei = AdvImages.rFrame_Category->GetTrueHeightSafe();
        float cTxLft = 0.0f;
        float cTxMLf = cEdgeW / cWid;
        float cTxMRt = (cWid - cEdgeW) / cWid;
        float cTxRgt = 1.0f;

        //--Compute pixel positions.
        float cPosWid = (cIconW * mCategoryList->GetListSize()) + (cEdgeW * 2.0f);
        float cPosLft = 0.0f;
        float cPosMLf = cEdgeW;
        float cPosMRt = cPosWid - cEdgeW;
        float cPosRgt = cPosWid;

        //--Setup.
        AdvImages.rFrame_Category->Bind();
        glTranslatef(cFrameX, cFrameY, 0.0f);
        glBegin(GL_QUADS);

        //--Left edge. Fixed position.
        glTexCoord2f(cTxLft, 1.0f); glVertex2f(cPosLft, 0.0f);
        glTexCoord2f(cTxMLf, 1.0f); glVertex2f(cPosMLf, 0.0f);
        glTexCoord2f(cTxMLf, 0.0f); glVertex2f(cPosMLf, cHei);
        glTexCoord2f(cTxLft, 0.0f); glVertex2f(cPosLft, cHei);

        //--Expandable center.
        glTexCoord2f(cTxMLf, 1.0f); glVertex2f(cPosMLf, 0.0f);
        glTexCoord2f(cTxMRt, 1.0f); glVertex2f(cPosMRt, 0.0f);
        glTexCoord2f(cTxMRt, 0.0f); glVertex2f(cPosMRt, cHei);
        glTexCoord2f(cTxMLf, 0.0f); glVertex2f(cPosMLf, cHei);

        //--Right edge.
        glTexCoord2f(cTxMRt, 1.0f); glVertex2f(cPosMRt, 0.0f);
        glTexCoord2f(cTxRgt, 1.0f); glVertex2f(cPosRgt, 0.0f);
        glTexCoord2f(cTxRgt, 0.0f); glVertex2f(cPosRgt, cHei);
        glTexCoord2f(cTxMRt, 0.0f); glVertex2f(cPosMRt, cHei);

        //--Finish frame.
        glEnd();
        glTranslatef(-cFrameX, -cFrameY, 0.0f);

        ///--[Icons]
        //--Render icons across the top.
        float tIconX = cFrameX + cEdgeW;
        float cIconY = cFrameY + 5.0f;
        ShopCategoryPack *rCategory = (ShopCategoryPack *)mCategoryList->PushIterator();
        while(rCategory)
        {
            //--If the icon exists, render it.
            if(rCategory->rIcon)
            {
                rCategory->rIcon->Draw(tIconX, cIconY);
            }

            //--Next.
            tIconX = tIconX + cIconW;
            rCategory = (ShopCategoryPack *)mCategoryList->AutoIterate();
        }

        ///--[Selection]
        //--Compute highlight positions.
        RenderExpandableHighlight(mCategoryHighlightPos, mCategoryHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
    }

    //--Scrollbar.
    if(rUseList->GetListSize() > cScrollPage)
    {
        StandardRenderScrollbar(mSkip, cScrollPage, rUseList->GetListSize(), 219.0f, 447.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    //--Highlight.
    if(mShopInventory->GetListSize() > 0)
        RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Help Strings.
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mToggleCompareMaterialsString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 2.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Materials pages in use only.
    if(mMaterialsUsePages)
    {
        mToggleMaterialsPageString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }

    //--This string only renders if character swapping is possible.
    if(mShowSwapArrows)
        mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only renders if multiple compare slots are present.
    if(mCanSwapCompareSlot)
        mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 3.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderUnalignedUnlock(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders components that have no specific side alignment.

    ///--[List Resolve]
    //--Standard details renderer. Item can be NULL!
    AdventureItem *rItem = GetConsideredItem();
    if(mDetailsTimer > 0)
    {
        RenderItemDetails(mDetailsTimer, cxAdvDetailsTicks, rItem);
    }

    //--Unlock an item confirmation.
    if(mIsUnlockingItem)
    {
        RenderUnlockConfirm(pVisAlpha);
    }

    //--Asking to equip.
    if(mAskToEquip)
    {
        RenderBuyEquipItem(pVisAlpha);
    }
}

///===================================== Common Rendering =========================================
void AdvUIVendor::RenderUnlockConfirm(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders a confirmation menu to unlock something.

    //--Resolve pointers.
    StarLinkedList *rUseList = GetConsideredCategory();

    //--Get the item package.
    ShopInventoryPack *rPack = (ShopInventoryPack *)rUseList->GetElementBySlot(mCursor);
    if(!rPack) return;

    //--Fast-access pointer.
    AdventureItem *rItem = rPack->mItem;
    if(!rItem) return;

    //--Compute local alpha.
    float cLocalAlpha = EasingFunction::QuadraticInOut(mUnlockConfirmTimer, cxAdvAskMergeTicks) * pVisAlpha;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cLocalAlpha));

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, cLocalAlpha);

    ///--[Rendering]
    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Unlock");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Centering computations.
    char *tTextBuf = InitializeString("%s", rItem->GetName());
    float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tTextBuf);
    float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

    //--Item Icon
    StarBitmap *rItemIcon = rItem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 205.0f);

    //--Item name.
    AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, tTextBuf);

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(757.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
    free(tTextBuf);
}
void AdvUIVendor::RenderMaterial(const char *pName, const char *pShortname, ShopInventoryPack *pShopPackage, float pX, float pY, float pR, float pReqR, float pAlpha)
{
    ///--[Documentation]
    //--Worker routine, renders the given name at the given position. Will dynamically resolve the
    //  amount of the item needed if an entry is highlighted.
    if(!pName) return;

    //--If the name is "DIVIDER" then skip rendering.
    if(!strcmp(pName, "DIVIDER")) return;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Setup]
    //--Get the item's quantity. It can be zero.
    int tQuantity = rInventory->GetCountOf(pName);

    //--Get the materials needed for the highlighted item, if it exists. Can be zero.
    int tQuantityNeeded = 0;
    if(pShopPackage)
    {
        //--Look for the unlock package.
        ShopUnlockPack *rPackage = (ShopUnlockPack *)pShopPackage->mUnlockRequirements->PushIterator();
        while(rPackage)
        {
            //--Name match:
            if(!strcasecmp(rPackage->mItemName, pName))
            {
                tQuantityNeeded = rPackage->mNumberNeeded;
                pShopPackage->mUnlockRequirements->PopIterator();
                break;
            }

            //--Next.
            rPackage = (ShopUnlockPack *)pShopPackage->mUnlockRequirements->AutoIterate();
        }
    }

    ///--[Color Resolve]
    //--If the item is required and we have enough:
    if(tQuantityNeeded > 0 && tQuantity >= tQuantityNeeded)
    {
        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
    }
    //--If the item is required and we're short:
    else if(tQuantityNeeded > 0 && tQuantity < tQuantityNeeded)
    {
        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
    }
    //--Neutral, grey.
    else
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    ///--[Render]
    //--Render the item name.
    AdvImages.rFont_Mainline->DrawText(pX, pY, 0, 1.0f, pShortname);

    //--Render the quantity right-aligned.
    AdvImages.rFont_Mainline->DrawTextArgs(pR, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "x%i", tQuantity);

    //--Quantity required, renders right-aligned if non-zero.
    if(tQuantityNeeded > 0)
    {
        AdvImages.rFont_Mainline->DrawTextArgs(pReqR, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "x%i", tQuantityNeeded);
    }
}
void AdvUIVendor::RenderMaterials(float pAlpha)
{
    ///--[ =========== Documentation ========== ]
    //--Instead of rendering the comparison properties on the right side, instead shows a list of
    //  all materials that currently have aliases, and how many the player has. Also shows the
    //  number to be spent on the current transaction if applicable.

    ///--[Comparison Info]
    //--Resolve which list to use. If no categories are in place, use the mShopInventory list.
    StarLinkedList *rUseList = mShopInventory;

    //--In unlock mode, use categories if present.
    if(mIsUnlockMode && mCategoryList->GetListSize() > 0)
    {
        ShopCategoryPack *rCategoryPack = (ShopCategoryPack *)mCategoryList->GetElementBySlot(mCategoryCursor);
        if(rCategoryPack) rUseList = rCategoryPack->mrMembers;
    }

    //--Resolve entry. Note that the shop package doesn't actually need to exist to begin rendering,
    //  it is only used to show the costs of the highlighted item.
    ShopInventoryPack *rShopPackage = (ShopInventoryPack *)rUseList->GetElementBySlot(mCursor);

    ///--[Setup]
    //--Fast-access pointers.
    //AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Setup variables.
    float cTextY =  182.0f;
    float cTextH =   25.0f;
    float cNameX = 1020.0f;
    float cNeedR = cNameX + 200.0f;
    float cQuanR = cNameX + 278.0f;

    ///--[ ======= Render All Materials ======= ]
    //--Renders a list of all materials, not using pages.
    if(!mMaterialsUsePages)
    {
        ///--[Header]
        //--Title of materials area.
        mColorHeading.SetAsMixerAlpha(pAlpha);
        AdvImages.rFont_Heading->DrawText(1158.0f, 137.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Materials");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        ///--[Iteration]
        //--Begin iterating across the material names.
        int i = 0;
        const char *rShortname = (const char *)mMaterialsList->PushIterator();
        while(rShortname)
        {
            //--Variables.
            const char *rItemName = mMaterialsList->GetIteratorName();
            float cYPos = cTextY + (cTextH * i);

            //--Call routine.
            RenderMaterial(rItemName, rShortname, rShopPackage, cNameX, cYPos, cQuanR, cNeedR, pAlpha);

            //--Next.
            i ++;
            rShortname = (const char *)mMaterialsList->AutoIterate();
        }
    }
    ///--[ ====== Render Materials Page ======= ]
    //--In cases where there are lots of materials, they can be organized into pages. Each page is a
    //  series of strings representing item names. The list mMaterialsPages stores ShopMaterialsPage's.
    else
    {
        //--Resolve the page in use.
        ShopMaterialsPage *rCurrentPage = (ShopMaterialsPage *)mMaterialsPages->GetElementBySlot(mMaterialsPage);
        if(!rCurrentPage) return;

        //--Title of materials area.
        mColorHeading.SetAsMixerAlpha(pAlpha);
        AdvImages.rFont_Heading->DrawText(1158.0f, 137.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mMaterialsPages->GetNameOfElementBySlot(mMaterialsPage));
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        //--If there is more than one material, put these scroll indicators on.
        if(mMaterialsPages->GetListSize() > 1)
        {
            AdvImages.rOverlay_MatArrowLft->Draw();
            AdvImages.rOverlay_MatArrowRgt->Draw();
        }

        //--Begin iterating across the material names.
        int i = 0;
        const char *rShortname = (const char *)rCurrentPage->mMaterials->PushIterator();
        while(rShortname)
        {
            //--Variables.
            const char *rItemName = rCurrentPage->mMaterials->GetIteratorName();
            float cYPos = cTextY + (cTextH * i);

            //--Call routine.
            RenderMaterial(rItemName, rShortname, rShopPackage, cNameX, cYPos, cQuanR, cNeedR, pAlpha);

            //--Next.
            i ++;
            rShortname = (const char *)rCurrentPage->mMaterials->AutoIterate();
        }
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
