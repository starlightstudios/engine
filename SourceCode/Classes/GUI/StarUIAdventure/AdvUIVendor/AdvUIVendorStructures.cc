//--Base
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///--[ ====== Shop Unlock Pack ====== ]
void ShopUnlockPack::Initialize()
{
    mItemName = NULL;
    mItemDisplayName = NULL;
    mItemShort = NULL;
    mNumberNeeded = 0;
}
void ShopUnlockPack::DeleteThis(void *pPtr)
{
    //--Cast and check.
    ShopUnlockPack *rPtr = (ShopUnlockPack *)pPtr;
    if(!pPtr) return;

    //--Deallocate.
    free(rPtr->mItemName);
    free(rPtr->mItemDisplayName);
    free(rPtr->mItemShort);
    free(rPtr);
}

///--[ ==== Shop Inventory Pack ===== ]
void ShopInventoryPack::Initialize()
{
    //--Members
    mItem = NULL;
    mDisplayName = NULL;
    mOwnedDisplay = -1;
    mPriceOverride = -1;
    mQuantity = 0;
    memset(mOwnedBy, 0, sizeof(bool) * ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE);

    //--Unlock Data
    mScrollMax = 0.0f;
    mAssociatedVariable = NULL;
    mUnlockRequirements = new StarLinkedList(true);
    mRemoveOnUnlock = false;
    mExecOnUnlock = NULL;
}
void ShopInventoryPack::DeleteThis(void *pPtr)
{
    //--Cast and check.
    ShopInventoryPack *rPtr = (ShopInventoryPack *)pPtr;
    if(!pPtr) return;

    //--Deallocate.
    delete rPtr->mItem;
    free(rPtr->mDisplayName);
    free(rPtr->mAssociatedVariable);
    delete rPtr->mUnlockRequirements;
    free(rPtr->mExecOnUnlock);
    free(rPtr);
}

///--[ ====== ShopCategoryPack ====== ]
void ShopCategoryPack::Initialize()
{
    rIcon = NULL;
    mrMembers = new StarLinkedList(false);
}
void ShopCategoryPack::DeleteThis(void *pPtr)
{
    //--Cast and check.
    ShopCategoryPack *rPtr = (ShopCategoryPack *)pPtr;
    if(!pPtr) return;

    //--Deallocate.
    delete rPtr->mrMembers;
    free(pPtr);
}

///--[ ===== ShopMaterialsPage ====== ]
void ShopMaterialsPage::Initialize()
{
    mMaterials = new StarLinkedList(true);
}
void ShopMaterialsPage::DeleteThis(void *pPtr)
{
    //--Cast and check.
    ShopMaterialsPage *rPtr = (ShopMaterialsPage *)pPtr;
    if(!pPtr) return;

    //--Deallocate.
    delete rPtr->mMaterials;
    free(pPtr);
}
