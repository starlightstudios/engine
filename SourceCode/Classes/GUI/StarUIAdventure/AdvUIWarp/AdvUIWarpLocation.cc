//--Base
#include "AdvUIWarp.h"
#include "AdvUIWarpStructures.h"

//--Classes
#include "AdvUIBase.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== Update ==============================================
void AdvUIWarp::UpdateLocationMode()
{
    ///--[Documentation and Setup]
    //--When the player is selecting a warp location within a region, this appears.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Region.
    int tSelectedRegion = GetGridCodeAtCursor();
    WarpRegionPack *rCurrentRegion = (WarpRegionPack *)mRegionData->GetElementBySlot(tSelectedRegion);
    if(!rCurrentRegion)
    {
        mIsSelectingLocation = false;
        return;
    }

    ///--[Arrow Keys]
    //--Up, decrements cursor.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        if(StandardCursor(mLocationCursor, mLocationSkip, -1, rCurrentRegion->mLocations->GetListSize(), cxAdvScrollBuf, cxAdvScrollPage, true))
        {
            FocusOnWarpLocation();
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return;
    }
    //--Down, increments cursor.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        if(StandardCursor(mLocationCursor, mLocationSkip, 1, rCurrentRegion->mLocations->GetListSize(), cxAdvScrollBuf, cxAdvScrollPage, true))
        {
            FocusOnWarpLocation();
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return;
    }

    ///--[Activation Handler]
    //--Activate, warp to the requested location. This calls an execution script.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get the location under consideration.
        WarpLocationPack *rLocationPack = (WarpLocationPack *)rCurrentRegion->mLocations->GetElementBySlot(mLocationCursor);
        if(!rLocationPack)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Execute the standard file to warp there.
        LuaManager::Fetch()->ExecuteLuaFile(xWarpExecuteScript, 1, "S", rLocationPack->mRoom);

        //--Hide the object.
        FlagExit();

        //--Special backwards code, orders the calling UI to exit instead of returning to the main menu.
        SetCodeBackward(AM_MODE_EXITNOW);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsSelectingLocation = false;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
}

///========================================== Drawing =============================================
void AdvUIWarp::RenderLocationMode()
{
    ///--[Documentation]
    //--Renders a map and a set of pins indicating where the player can warp to.
    if(mLocationVisTimer < 1) return;

    //--Slide percentage.
    float cFadePct = EasingFunction::QuadraticOut(mLocationVisTimer, mVisibilityTimerMax * 2.0f);

    //--Get the image or advanced set of images for rendering.
    int tSelectedRegion = GetGridCodeAtCursor();
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementBySlot(tSelectedRegion);
    if(!rRegionPack) return;

    ///--[Fullblack]
    //--Don't render anything except a black fadeout if below half on the fade percent.
    if(cFadePct < 0.50f)
    {
        StarBitmap::DrawFullBlack(cFadePct * 2.0f);
        return;
    }

    //--When using advanced rendering, render a black underlay.
    if(rRegionPack->mIsUsingAdvancedMaps) StarBitmap::DrawFullBlack(1.0f);

    ///--[Positioning]
    //--The map scrolls around when the player selects a different area. Set that here.
    float cXOffset = mLocationPos.mXCur;
    float cYOffset = mLocationPos.mYCur;
    glTranslatef(cXOffset, cYOffset, 0.0f);

    //--Scale.
    glScalef(mWarpZoomFactor, mWarpZoomFactor, 1.0f);

    ///--[Map Background]
    //--Render the background image.
    if(!rRegionPack->mIsUsingAdvancedMaps && rRegionPack->rMapImage)
    {
        rRegionPack->rMapImage->Draw();
    }
    //--Render the advanced image set.
    else if(rRegionPack->mIsUsingAdvancedMaps)
    {
        ///--[Stencil Setup]
        glClear(GL_STENCIL_BITS);
        glEnable(GL_STENCIL_TEST);
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_ALWAYS, 0, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilMask(0xFF);

        ///--[Iteration]
        //--Iterate across the advanced map packs.
        AdvancedMapPack *rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->PushIterator();
        while(rMapPack)
        {
            ///--[Validity Check]
            //--Skip if the image is not present for some reason.
            if(!rMapPack->rImage)
            {
                rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->AutoIterate();
                continue;
            }

            ///--[Stencil Handling]
            //--If the layer does not read or write stencils:
            if(!rMapPack->mReadsStencil && !rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_ALWAYS, 0, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            }
            //--Layer reads stencils.
            else if(rMapPack->mReadsStencil && !rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_EQUAL, rMapPack->mReadStencil, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            }
            //--Layer writes stencils.
            else if(!rMapPack->mReadsStencil && rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_ALWAYS, rMapPack->mWriteStencil, 0xFF);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
            }
            //--Layer does both. Use the read value. Doesn't matter much since this doesn't really write anything different.
            else if(rMapPack->mReadsStencil && rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_EQUAL, rMapPack->mReadStencil, 0xFF);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
            }

            //--If the roll is within this pack's range, render.
            if(mAdvancedPackRoll >= rMapPack->mRenderChanceLo && mAdvancedPackRoll <= rMapPack->mRenderChanceHi)
            {
                rMapPack->rImage->Draw();
            }

            //--Next.
            rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->AutoIterate();
        }
    }

    ///--[Clean]
    //--Unset stencils.
    glDisable(GL_STENCIL_TEST);

    ///--[Map Pins]
    //--Render a pin for all warpable locations on the map.
    float cPinOffX = -9.0f;
    float cPinOffY = -30.0f;

    //--Iterate.
    int i = 0;
    WarpLocationPack *rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->PushIterator();
    while(rLocationPack)
    {
        //--If this is the active pin, render a red one:
        if(i == mLocationCursor)
        {
            //--Compute pin's render coords.
            float cRenderX = rLocationPack->mCoordX + cPinOffX;
            float cRenderY = rLocationPack->mCoordY + cPinOffY;

            //--Render.
            AdvImages.rOverlay_MapPinSelected->Draw(cRenderX, cRenderY);

            //--Modify by the scale.
            float cFinalXOffset = mLocationPos.mXEnd;
            float cFinalYOffset = mLocationPos.mYEnd;
            cRenderX = cRenderX * mWarpZoomFactor;
            cRenderY = cRenderY * mWarpZoomFactor;
            cRenderX = cRenderX + cFinalXOffset;
            cRenderY = cRenderY + cFinalYOffset;

            //--If within the given area, move the listing to the right side.
            if(cRenderX < 522.0f && cRenderY < 157.0f)
            {
                if(mLocationOffsetTimer < cxAdvWarpOffsetTicks) mLocationOffsetTimer ++;
            }
            else
            {
                if(mLocationOffsetTimer > 0) mLocationOffsetTimer --;
            }
        }
        //--Otherwise, render a blue pin.
        else
        {
            AdvImages.rOverlay_MapPinUnselected->Draw(rLocationPack->mCoordX + cPinOffX, rLocationPack->mCoordY + cPinOffY);
        }

        //--Next.
        i ++;
        rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->AutoIterate();
    }

    ///--[Unset Position]
    //--Undo scale.
    glScalef(1.0f / mWarpZoomFactor, 1.0f / mWarpZoomFactor, 1.0f);

    //--Undo area scroll.
    glTranslatef(-cXOffset, -cYOffset, 0.0f);

    ///--[Overlay]
    //--If there is an overlay, render it with a fixed position.
    if(rRegionPack->rMapOverlayImage) rRegionPack->rMapOverlayImage->Draw();

    //--No region map image, black underlay.
    if(!rRegionPack->mIsUsingAdvancedMaps && !rRegionPack->rMapImage)
    {
        StarBitmap::DrawFullBlack(1.0f);
    }

    //--When using advanced rendering, render a black underlay.
    //if(rRegionPack->mIsUsingAdvancedMaps) StarBitmap::DrawFullBlack(1.0f);

    ///--[Header]
    //--Render a header. Borrows the base menu one.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    SetColor("Heading", 1.0f);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Warp Select");
    SetColor("Clear", 1.0f);

    ///--[Listing]
    //--If the map pin is under the listing, move the listing to the right side.
    float cTranslationPct = EasingFunction::QuadraticInOut(mLocationOffsetTimer, cxAdvWarpOffsetTicks);
    float cTranslationVal = 850.0f * cTranslationPct;
    glTranslatef(cTranslationVal, 0.0f, 0.0f);

    //--Appears in the top right or top left, depending on where the map pin is. Shows the names of the warp locations.
    AdvImages.rUnderlay_MapLocationBacking->Draw();

    //--Setup.
    int tRender = 0;
    float cTxtLft =  32.0f;
    float cTxtTop =  14.0f;
    float cTxtHei =  21.0f;

    //--Entries.
    rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->PushIterator();
    while(rLocationPack)
    {
        //--Effective position.
        int tEffectiveRender = tRender - mLocationSkip;

        //--Skip when scrolling.
        if(tEffectiveRender < 0)
        {
            tRender ++;
            rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->AutoIterate();
            continue;
        }

        //--Render name.
        AdvImages.rFont_Mainline->DrawTextFixed(cTxtLft, cTxtTop + (cTxtHei * tEffectiveRender), cxAdvMaxLocationNameWid, 0, rRegionPack->mLocations->GetIteratorName());

        //--Cap.
        if(tEffectiveRender >= cxAdvScrollPage-1)
        {
            rRegionPack->mLocations->PopIterator();
            break;
        }

        //--Next.
        tRender ++;
        rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->AutoIterate();
    }

    ///--[Highlight]
    //--Only renders when selecting a location.
    if(mIsSelectingLocation) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    ///--[Scrollbar]
    //--If there are enough entries, render a scrollbar.
    if(rRegionPack->mLocations->GetListSize() > cxAdvScrollPage)
    {
        StandardRenderScrollbar(mLocationSkip, cxAdvScrollPage, rRegionPack->mLocations->GetListSize(), 25.0f, 93.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    //--Unset pin.
    glTranslatef(-cTranslationVal, 0.0f, 0.0f);

    ///--[Fading]
    //--If we got this far, render a fullblack fade in.
    if(cFadePct < 1.0f)
    {
        float cAlpha = (cFadePct - 0.50f) * 2.0f;
        StarBitmap::DrawFullBlack(1.0f - cAlpha);
    }
}
