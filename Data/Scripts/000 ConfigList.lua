-- |[Configuration File List]|
--Immediately after the OptionsManager is built, this file is executed. It builds a list configuration files
-- that may or may not exist. These files then execute in the built order.
OM_RegisterConfigPath("Config_Engine.lua")
OM_RegisterConfigPath("Config_Classic.lua")
OM_RegisterConfigPath("Config_Adventure.lua")
OM_RegisterConfigPath("Config_StringTyrant.lua")
OM_RegisterConfigPath("Config_Pairanormal.lua")
