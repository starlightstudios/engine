//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueTopic.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers

void WorldDialogue::WriteToBuffer(StarAutoBuffer *pBuffer)
{
    //--Writes topic information to the provided buffer. Called during the saving process.
    if(!pBuffer) return;

    //--Write how many topics there are.
    pBuffer->AppendInt32(mTopicListing->GetListSize());

    //--Append the information from each topic.
    DialogueTopic *rTopic = (DialogueTopic *)mTopicListing->PushIterator();
    while(rTopic)
    {
        //--Name of the topic. Reference only.
        pBuffer->AppendStringWithLen(rTopic->GetInternalName());

        //--Level of the topic. This is what the loading process actually needs.
        pBuffer->AppendInt32(rTopic->GetLevel());

        //--How many NPCs are on the topic's listing. Can be 0.
        StarLinkedList *rNPCList = rTopic->GetNPCList();
        pBuffer->AppendInt32(rNPCList->GetListSize());

        //--Iterate across the list of NPCs. NPCs store what level of the topic they were last asked at.
        DTNPCRegPack *rPackage = (DTNPCRegPack *)rNPCList->PushIterator();
        while(rPackage)
        {
            //--Write the name of the NPC. This is for reference only.
            pBuffer->AppendStringWithLen(rNPCList->GetIteratorName());

            //--Write the level of the NPC.
            pBuffer->AppendInt32(rPackage->mLastSpokeLevel);

            //--Next.
            rPackage = (DTNPCRegPack *)rNPCList->AutoIterate();
        }

        //--Next.
        rTopic = (DialogueTopic *)mTopicListing->AutoIterate();
    }
}
void WorldDialogue::ReadFromFile(VirtualFile *fInfile)
{
    //--Given a VirtualFile in position, reads topic data and stores it. Existing topic data is overwritten.
    //  Topics not found in the save file will be left unmolested. Topics in the save file but not in the
    //  current listing are created.
    if(!fInfile) return;

    //--Get how many topics we expect.
    int32_t tExpectedTopics = 0;
    fInfile->Read(&tExpectedTopics, sizeof(int32_t), 1);

    //--Read.
    for(int i = 0; i < tExpectedTopics; i ++)
    {
        //--Read the name of the topic.
        char *tTopicName = fInfile->ReadLenString();

        //--Locate the topic. If it does not exist, create it.
        DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(tTopicName);
        if(!rCheckTopic)
        {
            rCheckTopic = new DialogueTopic();
            rCheckTopic->SetName(tTopicName);
            mTopicListing->AddElement(tTopicName, rCheckTopic, &RootObject::DeleteThis);
        }

        //--Set the topic's level.
        int32_t tTopicLevel = 0;
        fInfile->Read(&tTopicLevel, sizeof(int32_t), 1);
        rCheckTopic->SetLevel(tTopicLevel);

        //--Get how many NPCs are on this topic at the moment.
        int32_t tExpectedNPCs = 0;
        fInfile->Read(&tExpectedNPCs, sizeof(int32_t), 1);
        for(int i = 0; i < tExpectedNPCs; i ++)
        {
            //--Get the name and level.
            char *tNPCName = fInfile->ReadLenString();
            int32_t tLevel = 0;
            fInfile->Read(&tLevel, sizeof(int32_t), 1);

            //--Set. This will register the NPC if it didn't already exist.
            rCheckTopic->RegisterOrOverrideNPC(tNPCName, tLevel);

            //--Clean.
            free(tNPCName);
        }

        //--Clean.
        free(tTopicName);
    }
}
