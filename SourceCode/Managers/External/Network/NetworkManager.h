///====================================== NetworkManager ==========================================
//--Handles all interprocess communications, including that taking place on another machine.  But
//  technically, it's any process that is not this one, so network is a slight misnomer.
//--It uses StarSockets to handle communications code.  The StarSockets handle a lot of the
//  platform-specific code so the NetworkManager doesn't have to.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarlightSocket.h"

///===================================== Local Definitions ========================================
#define NETWORK_PROGRAMNAME "StarCubeEngineProgram"

///===================================== Local Structures =========================================
///--[AudioPacket_SetVariable_Data]
//--Local Socket Types (Program only, NOT for StarlightSocket.h!)
#define AUDIOPACKET_SETVARIABLE (SUGARPACKET_STANDARDPACKETS + 0)
typedef struct AudioPacket_SetVariable_Data
{
    char mVariableName[32];
    int32_t mEnemyCount;
}AudioPacket_SetVariable_Data;

///========================================== Classes =============================================
class NetworkManager
{
    private:
    //--System

    //--Socket Controls
    StarLinkedList *mSocketList;

    //--Client Handling
    bool mRetryConnection;
    int mRetryTimer;
    int mRetriesLeft;

    //--Server Handling

    protected:

    public:
    //--System
    NetworkManager();
    ~NetworkManager();

    //--Public Variables
    //--Property Queries
    bool IsConnectedTo(const char *pName);

    //--Manipulators
    void AttemptConnection(const char *pNameIdentifier, const char *pIPAddress, int pPort);
    void FlagStarBeats();

    //--Core Methods
    void SendPacketTo(const char *pName, StarlightPacket pPacket);

    //--Update
    void Update();
    void UpdatePaused(uint8_t pPauseFlags);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static NetworkManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

