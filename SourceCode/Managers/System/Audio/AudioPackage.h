///======================================= AudioPackage ===========================================
//--A wrapper class around an audio piece, be it a stream or sample. The AudioManager deals
//  exclusively with these, and thus only AudioPackages need to be reprogrammed based on the API
//  in use.
//--The values mStartLoopTime and mEndLoopTime are *not* used by the looping software, they are
//  used only by property queries since getting the values once they are on the audio card is difficult.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define SAMPLE_MAX_CHANNELS 5
#define AUDIOPACKAGE_TYPE_BASE 0
#define AUDIOPACKAGE_TYPE_STREAM 1
#define AUDIOPACKAGE_TYPE_SAMPLE 2

///========================================== Classes =============================================
class AudioPackage
{
    private:
    protected:
    //--System
    uint32_t mAudioPackageType;
    bool mIsReady;
    float mLocalVolume;
    float mGlobalVolume;
    int mAbstractSlot;
    bool mIsFading;
    bool mPlaysWhenAtZeroVolume;

    //--Bass Only
    #if defined _BASS_AUDIO_
        uint32_t mHandle;
        uint32_t mTempoHandle;
    #endif

    //--FMOD Only
    #if defined _FMOD_AUDIO_
        FMOD_SOUND *mSound;
        FMOD_SOUND *rSoundToPlay;
        FMOD_CHANNEL *rChannel;
        FMOD_RESULT mFMODResult;
        int mSubSoundsTotal;

        //--Looping properties.
        float mStartLoop;
        float mEndLoop;
    #endif

    //--Sample
    bool mIsSample;

    //--Stream
    bool mIsStream;

    //--Extra Data
    float mStartLoopTime; //Note: Read-only.
    float mEndLoopTime;   //Note: Read-only.
    void *mExtraData;

    public:
    //--System
    AudioPackage();
    virtual ~AudioPackage();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    uint32_t GetAudioPackageType();
    int GetAbstractSlot();
    virtual float GetDuration();
    virtual bool IsPlaying();
    float GetVolume();
    virtual float GetPosition();
    bool IsLooping();
    float GetStartLoopTime();
    float GetEndLoopTime();
    bool PlaysWhenAtZeroVolume();

    //--Manipulators
    void SetAbstractSlot(int pSlot);
    virtual void Load(const char *pPath);
    virtual void Play();
    virtual void FadeIn(int pTicks);
    virtual void FadeOut(int pTicks);
    virtual void FadeOutToStop(int pTicks);
    virtual void Pause();
    virtual void Stop();
    void SetLoop(bool pIsLooping);
    virtual void SetVolume(float pVolume);
    virtual void SetGlobalVolume(float pVolume);
    virtual void SlideVolume(float pVolume, int pTicks);
    virtual void SeekTo(float pSeconds);
    virtual void SetTempo(float pTempo);
    void SetPlaysWhenAtZeroVolume(bool pFlag);

    //--Core Methods
    virtual void AddLoop(float pStartLoop, float pEndLoop);
    float ResolveAbstractVolume();

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AudioPackage_ActivateLoop(lua_State *L);
int Hook_AudioPackage_SetLocalVolume(lua_State *L);

