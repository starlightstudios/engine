///====================================== StarTranslation =========================================
//--Object that contains a list of translations, alphabetized. The list can be searched for matches
//  when appending strings or rendering text. This class also contains logic for reading translations
//  out of csv files.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
typedef struct StarTranslateEntry
{
    //--Members
    char *mBaseString;
    char *mTranslation;
    char *mPrefix;

    //--Functions
    void Initialize()
    {
        mBaseString = NULL;
        mTranslation = NULL;
        mPrefix = NULL;
    }
    void Deallocate()
    {
        free(mBaseString);
        free(mTranslation);
        free(mPrefix);
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        StarTranslateEntry *rPtr = (StarTranslateEntry *)pPtr;
        free(rPtr->mBaseString);
        free(rPtr->mTranslation);
        free(rPtr->mPrefix);
        free(rPtr);
    }
}StarTranslateEntry;

///===================================== Local Definitions ========================================
//--Active Translation Paths
#define TRANSPATH_GENERAL      "Root/Translations/Game/"
#define TRANSPATH_ACTIVE       "Root/Translations/Game/Active/"
#define TRANSPATH_UI           "Root/Translations/Game/Active/UI"
#define TRANSPATH_COMBAT       "Root/Translations/Game/Active/Combat"
#define TRANSPATH_JOURNAL      "Root/Translations/Game/Active/Journal"
#define TRANSPATH_DIALOGUE     "Root/Translations/Game/Active/Dialogue"
#define TRANSPATH_ITEMS        "Root/Translations/Game/Active/Items"
#define TRANSPATH_SKILLS       "Root/Translations/Game/Active/Skills"
#define TRANSPATH_ACHIEVEMENTS "Root/Translations/Game/Active/Achievements"

//--Sizing
#define STARTRANS_BUFSIZE 65536

///========================================== Classes =============================================
class StarTranslation
{
    private:
    ///--[System]
    int mLastReturnedEntry;
    int mTotalEntries;
    StarTranslateEntry *mEntries;

    protected:

    public:
    //--System
    StarTranslation();
    ~StarTranslation();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    int BinaryGetLookup(const char *pString, int pMin, int pMax);
    const char *GetTranslationFor(const char *pString);
    const char *GetLastPrefix();

    //--Manipulators
    void Translate(char *&sString);

    //--Core Methods
    void FactoryZero();
    void ParseFile(const char *pPath);
    static char *ParseLine(const char *pString, int &sPosition);

    private:
    //--Private Core Methods
    static int CompareTranslateEntries(const void *pEntryA, const void *pEntryB);

    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static void StringTranslateWorker(char *&sString, StarTranslation *pTranslation);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_STrans_CreateTranslation(lua_State *L);
int Hook_STrans_ActivateTranslation(lua_State *L);


