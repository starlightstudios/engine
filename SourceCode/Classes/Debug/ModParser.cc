//--Base
#include "ModParser.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
ModParser::ModParser()
{
    ///--[ModParser]
    //--System
    //--Name Categories
    mNameList = new StarLinkedList(true);
}
ModParser::~ModParser()
{
    delete mNameList;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
void ModParser::ParseFile(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Opens and parses a file, printing the results at the end.
    if(!pPath) return;

    //--Open it as a text file.
    FILE *fInfile = fopen(pPath, "r");
    if(!fInfile) return;

    //--Report file.
    FILE *fOutfile = fopen("ModOutput.txt", "w");
    if(!fOutfile)
    {
        fclose(fInfile);
        return;
    }

    ///--[Variables]
    //--Fast-access variables.
    char tInBuffer[1024];
    char tPreviousBuf[STD_MAX_LETTERS];
    ModParserPack *rPreviousPack = NULL;

    ///--[Parse]
    int p = 0;
    while(!feof(fInfile))
    {
        //--Get the next line.
        fgets(tInBuffer, 1024, fInfile);
        int tLen = (int)strlen(tInBuffer);

        //--Buffers
        int tNameSlot = 0;
        int tNumSlot = 0;
        char tNameBuf[32];
        char tNumBuf[32];

        //--Resolve the name.
        bool tIsOnName = true;
        for(int i = 0; i < tLen; i ++)
        {
            //--Get the letter.
            char tLetter = tInBuffer[i];

            //--Currently on the name:
            if(tIsOnName)
            {
                //--Is a letter:
                if((tLetter >= 'a' && tLetter <= 'z') || (tLetter >= 'A' && tLetter <= 'Z'))
                {
                    tNameBuf[tNameSlot+0] = tLetter;
                    tNameBuf[tNameSlot+1] = '\0';
                    tNameSlot ++;
                }
                //--Is a number:
                else if(tLetter >= '0' && tLetter <= '9')
                {
                    tIsOnName = false;
                    tNumBuf[tNumSlot+0] = tLetter;
                    tNumBuf[tNumSlot+1] = '\0';
                    tNumSlot ++;
                }
                //--Error.
                else
                {
                    break;
                }
            }
            //--Currently on the number:
            else
            {
                //--Is a letter:
                if((tLetter >= 'a' && tLetter <= 'z') || (tLetter >= 'A' && tLetter <= 'Z'))
                {
                    break;
                }
                //--Is a number:
                else if(tLetter >= '0' && tLetter <= '9')
                {
                    tIsOnName = false;
                    tNumBuf[tNumSlot+0] = tLetter;
                    tNumBuf[tNumSlot+1] = '\0';
                    tNumSlot ++;
                }
                //--Error.
                else
                {
                    break;
                }
            }
        }

        //--Get the number.
        int tNumber = atoi(tNumBuf);
        if(tNumber < 0 || tNumber >= MP_MAX) tNumber = 0;

        //--Debug.
        //fprintf(stderr, "%s %s %i\n", tNameBuf, tNumBuf, tNumber);

        //--If the previous entry does not exist, create it.
        if(!rPreviousPack)
        {
            //--Init.
            ModParserPack *nPack = (ModParserPack *)starmemoryalloc(sizeof(ModParserPack));
            nPack->Initialize();
            nPack->mHighest = tNumber;
            mNameList->AddElement(tNameBuf, nPack, &FreeThis);

            //--Store.
            strcpy(tPreviousBuf, tNameBuf);
            rPreviousPack = nPack;
            //fprintf(stderr, " Created a new pack %s\n", tNameBuf);
        }
        //--The previous pack exists, but is not the same as the current pack.
        else if(strcasecmp(tNameBuf, tPreviousBuf))
        {
            //--Locate.
            ModParserPack *rCheckPack = (ModParserPack *)mNameList->GetElementByName(tNameBuf);
            if(!rCheckPack)
            {
                rCheckPack = (ModParserPack *)starmemoryalloc(sizeof(ModParserPack));
                rCheckPack->Initialize();
                rCheckPack->mHighest = tNumber;
                mNameList->AddElement(tNameBuf, rCheckPack, &FreeThis);
                //fprintf(stderr, " Created a new pack %s\n", tNameBuf);
            }

            //--Store.
            strcpy(tPreviousBuf, tNameBuf);
            rPreviousPack = rCheckPack;
            //fprintf(stderr, " Switched packs %s\n", tNameBuf);
        }

        //--Set flags.
        rPreviousPack->mTaken[tNumber] = true;
        if(rPreviousPack->mHighest < tNumber) rPreviousPack->mHighest = tNumber;

        //--Next.
        p ++;
    }

    ///--[Report]
    fprintf(stderr, "[Beginning Mod Parser Report]\n");
    ModParserPack *rPackage = (ModParserPack *)mNameList->PushIterator();
    while(rPackage)
    {
        //--Print.
        fprintf(fOutfile, "Namespace: %s\n", mNameList->GetIteratorName());

        //--Variables.
        int tBlockStart = -1;
        int tBlockEnd = -1;

        //--Iterate.
        for(int i = 0; i < rPackage->mHighest; i ++)
        {
            //--Empty slot.
            if(!rPackage->mTaken[i])
            {
                if(tBlockStart == -1)
                {
                    tBlockStart = i;
                    tBlockEnd = i;
                }
                else
                {
                    tBlockEnd = i;
                }
            }
            //--Filled slot.
            else
            {
                if(tBlockStart != -1)
                {
                    fprintf(fOutfile, " Empty group: %i to %i\n", tBlockStart, tBlockEnd);
                    tBlockStart = -1;
                }
            }
        }

        //--Next.
        rPackage = (ModParserPack *)mNameList->AutoIterate();
    }
    fprintf(stderr, "[Conclude Mod Parser Report]\n");

    ///--[Clean]
    fclose(fInfile);
    fclose(fOutfile);
}

///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
