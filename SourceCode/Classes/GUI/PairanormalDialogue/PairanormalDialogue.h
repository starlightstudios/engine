///--[PairanormalDialogue]
//--Handles... dialogue. This includes decisions, showing text, timing, and sending instructions
//  to the owning object to change characters.

#pragma once

//--[Includes]
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"
#include "Samples/RootObject.h"
#include "Classes/World/TiledLevel/TiledLevel.h"

///--[Local Structures]
//--Represents a decision the player can make. Refers to the Pairanormal version only.
typedef struct
{
    char mScriptPath[STD_PATH_LEN];
    char mScriptArg[STD_MAX_LETTERS];
    char mDisplayText[STD_MAX_LETTERS];
    TwoDimensionReal mPosition;
}PairanormalDecisionPack;

//--Represents an object the player has found in the current investigation.
typedef struct
{
    StarBitmap *rIconImg;
    float mIconU1;
    float mIconV1;
    float mIconU2;
    float mIconV2;
    TwoDimensionReal mExaminePosition;
    char mScriptPath[STD_PATH_LEN];
    char mScriptArg[STD_MAX_LETTERS];
}InvestigationPack;

///--[Local Definitions]
#define MAX_LINES 3
#define MAX_LETTERS 512
#define MAX_ACTIVE_CHARACTERS 7
#define MIDDLE_CHARACTER 3

#define STD_SLIDE_TICKS 25

//--Text Flags for application
#define TEXTFLAG_APPLY_GLITCH 0x01
#define TEXTFLAG_APPLY_ITALICS 0x02

//--Text Flags for rendering
#define TEXTFLAG_NOTHING 0x00
#define TEXTFLAG_MIRRORX 0x01
#define TEXTFLAG_MIRRORY 0x02
#define TEXTFLAG_DOUBLERENDERX 0x04
#define TEXTFLAG_DOUBLERENDERY 0x08
#define TEXTFLAG_ITALICS 0x10

//--Glitch types
#define DIAGLITCH_LINES 0x01
#define DIAGLITCH_COLINV 0x02
#define DIAGLITCH_BGCOLINV 0x04
#define DIAGLITCH_CREEPY 0x08

#define CREEPY_OVERLAYS_TOTAL 4

//--Investigation Buttons
#define DIABTN_INV_SWITCHMODES 0
#define DIABTN_INV_GOHOME 1
#define DIABTN_INV_ARROWL 2
#define DIABTN_INV_ARROWR 3
#define DIABTN_INV_ITEM0 4
#define DIABTN_INV_ITEM1 5
#define DIABTN_INV_ITEM2 6
#define DIABTN_INV_ITEM3 7
#define DIABTN_INV_ITEM4 8
#define DIABTN_INV_ITEM5 9
#define DIABTN_INV_ITEM6 10
#define DIABTN_INV_ITEM7 11
#define DIABTN_INV_ITEM8 12
#define DIABTN_INV_ITEM9 13
#define DIABTN_INV_ITEM10 14
#define DIABTN_INV_ITEM11 15
#define DIABTN_INV_ITEM12 16
#define DIABTN_INV_TOTAL 17

//--Presentation Buttons
#define DIABTN_PRS_CHAR0 0
#define DIABTN_PRS_CHAR1 1
#define DIABTN_PRS_CHAR2 2
#define DIABTN_PRS_CHAR3 3
#define DIABTN_PRS_SCROLLLFT 4
#define DIABTN_PRS_SCROLLRGT 5
#define DIABTN_PRS_LATER 6
#define DIABTN_PRS_CHAT 7
#define DIABTN_PRS_TOTAL 8

//--Name Entry
#define DIABTN_NAME_MAXLEN 10
#define DIABTN_NAME_LETTERS 11

///--[Classes]
class PairanormalDialogue : public RootObject
{
    private:
    //--System
    bool mIsLoad;
    bool mCharacterFirstShowThisTick;
    bool mIsHardBlocking;
    bool mCharactersRecomputePositions;

    //--Tag Remaps
    StarLinkedList *mTagRemapList;

    //--Speaker
    bool mIsSpeakerPlayer;
    bool mIsSpeakerThought;
    bool mIsSpeakerInfo;
    bool mIsSpeakerCredits;
    bool mSpeakerChangedThisTick;
    bool mCharactersChangedVisibilityThisTick;
    char *mCharacterSpeaker;
    char *mSpeakerName;
    char *mFocusName;
    float mCharacterZoom;

    //--Text Ticking
    float mTextTickTimer;
    float mTextTickInterval;
    char mVoiceTick[32];

    //--Display
    float cFontScale;
    bool mIsUsingSwaggerNextPass;
    TwoDimensionReal mDialogueBoxDim;

    //--Glitch
    int mGlitchTimer;
    int mGlitchRunTimer;
    uint16_t mGlitchFlags;
    float mGlitchVHSStart;
    float mGlitchVHSRange;
    int mCreepySlot;

    //--Text
    bool mAnythingToAppend;
    bool mWaitingOnKeypress;
    char *mMainString;
    int mCurrentAppendLetter;
    int mCurrentAppendX;
    int mCurrentAppendY;
    uint8_t mCurrentTextFlags;
    char mSubstrings[MAX_LINES][MAX_LETTERS];
    bool mSubstringsDisplay[MAX_LINES][MAX_LETTERS];
    uint8_t mSubstringFlags[MAX_LINES][MAX_LETTERS];
    int mChangeTextBoxesTimer;
    StarBitmap *rCurrentTextBox;
    StarBitmap *rPreviousTextBox;

    //--Flags
    bool mReappend;

    //--Image Display Mode
    bool mIsImageDisplayMode;
    int mImageFadeTimer;
    StarBitmap *rActiveImage;
    StarBitmap *rFadingImage;

    //--Decisions
    bool mShowDecisions;
    bool mDecisionsNeedReposition;
    int mHighlightedDecision;
    bool mDecisionTwoLists;
    StarLinkedList *mDecisionList;

    //--Timings
    float mAppendSpeed;
    float mSpeedFactor;
    float mAppendNumerator;
    float mAppendDenominator;

    //--Characters
    PairanormalDialogueCharacter *rActiveCharacters[MAX_ACTIVE_CHARACTERS];
    StarLinkedList *mCharactersList;//PairanormalDialogueCharacter
    StarLinkedList *mCharNameRemapList; //char *, master

    //--Major Scenes
    StarBitmap *rMajorSceneImg;

    //--Investigation Mode
    bool mIsInvestigationMode;
    int mInvestigationTimeLeft;
    int mInvestigationTimeMax;
    char *mGoHomeScript;
    char *mGoHomeArg;
    int mBGScrollTimer;
    int mInvestigationPointsLeft;
    int mInvestigationBottomScrollTimer;
    int mInventoryOffset;
    InvestigationPack *rShowDiscoveredObject;
    StarLinkedList *mInvestigationObjectList;//InvestigationPack *, master
    StarLinkedList *mDiscoveredObjectList;//InvestigationPack *, master
    StarLinkedList *mRenderableObjectList;//InvestigationPack *, master
    TwoDimensionReal mInvestigationButtons[DIABTN_INV_TOTAL];

    //--Name-Entry Mode
    bool mIsNameEntryMode;
    bool mIsShifted;
    bool mIsCapsLocked;
    int mLetterCursor;
    int mNameEntryTimer;
    char mNameEntryString[128];
    char mCurrentPlayerName[DIABTN_NAME_LETTERS];

    //--Character Talking Mode
    bool mIsPresentationMode;
    int mCharacterTalkSwapTimer;
    int mCharacterFocusTimer;
    int mPresentationOffset;
    int mSelectedCharacter;
    int mSpeakingToCharacter;
    void *rSlidingCharacter;
    StarLinkedList *mCharacterTalkList;//DialogueCharacter Ref
    TwoDimensionReal mPresentationButtons[DIABTN_PRS_TOTAL];
    TwoDimensionReal mPresentNameBox;
    TwoDimensionReal mPresentInfoBox;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rDialogueFont;
            StarFont *rHeadingFont;

            //--Overlays
            StarBitmap *rBackgroundBarsBot;
            StarBitmap *rBackgroundBarsTop;
            StarBitmap *rBackgroundBarUnder;
            StarBitmap *rVHSOverlay;
            StarBitmap *rCreepyOverlay[CREEPY_OVERLAYS_TOTAL];

            //--Common UI Pieces
            StarBitmap *rBtnExitGame;
            StarBitmap *rBtnSettings;

            //--Dialogue Parts
            StarBitmap *rContinueIcon;
            StarBitmap *rDialogueBoxBlack;
            StarBitmap *rDialogueBoxWhite;
            StarBitmap *rNameBarLft;
            StarBitmap *rNameBarRgt;

            //--Decision Parts
            StarBitmap *rDecisionBoxBlack;
            StarBitmap *rDecisionBoxWhite;
            StarBitmap *rDecisionBoxBlackHf;
            StarBitmap *rDecisionBoxWhiteHf;

            //--Info Box
            StarBitmap *rInfoBoxBot;
            StarBitmap *rInfoBoxTop;
        }Data;

        //--Investigation UI
        struct
        {
            //--UI Parts
            StarBitmap *rInvArrows;
            StarBitmap *rCloseupBacking;
            StarBitmap *rHand;
            StarBitmap *rHouse;
            StarBitmap *rIconBacking;
            StarBitmap *rItemNameBacking;
            StarBitmap *rMagnifyingLens;
            StarBitmap *rMoreCharsLft;
            StarBitmap *rMoreCharsRgt;
            StarBitmap *rTimerBarBlack;
            StarBitmap *rTimerBarWhite;

            //--Presentation UI
            StarBitmap *rButtonBlack;
            StarBitmap *rButtonWhite;
            StarBitmap *rShowMeBox;
            StarBitmap *rShowMeNameBox;
        }Investigation;
    }Images;

    protected:

    public:
    //--System
    PairanormalDialogue();
    virtual ~PairanormalDialogue();
    void Construct();

    //--Public Variables
    static bool xAllowVoiceTicks;
    static bool xShowInvestigationHints;
    static char *xInvestigationStdImg;
    static char *xInvestigationStdPath;
    static PairanormalDialogue *xStaticCopy;

    //--Property Queries
    bool IsStoppingEvents();
    float GetAppendSpeed();
    bool IsUsingSwagger();
    const char *GetPlayerName();
    char *ComposeCharacterList();
    bool IsCharacterActive(const char *pName);

    //--Manipulators
    void SetSpeaker(const char *pName);
    void SetSpeakerName(const char *pName);
    void HighlightCharacter(const char *pName, bool pIsHighlighted);
    void FocusCharacter(const char *pName);
    void SetVoice(const char *pVoiceName);
    void AddCharacterAlias(const char *pBaseName, const char *pAlias);
    void AppendDialogue(const char *pDialogue);
    void AppendDialogueOnlyChars(const char *pDialogue);
    void RegisterCharacter(const char *pName, PairanormalDialogueCharacter *pCharacter);
    void ShowCharacter(const char *pName);
    void ShowCharacter(const char *pName, const char *pEmotion);
    void ShowCharacterBySet(const char *pSet);
    void HideCharacter(const char *pName);
    void SetCharacterEmotion(const char *pName, const char *pEmotion);
    void HideAllCharacters();
    void HideAllCharactersFast();
    void ShowMajorScene(const char *pDLPath);
    void HideMajorScene();
    void SetAppendSpeed(float pSpeed);
    void SetUseSwagger(bool pFlag);
    void SetPlayerName(const char *pName);

    //--Core Methods
    void ResolvePositionForActor(PairanormalDialogueCharacter *pActorPtr, bool pIsSpeakerChange);
    void ActivateDecisions();
    void AppendDecision(const char *pDisplayText, const char *pDecisionScript, const char *pScriptArg);
    void RepositionDecisions();
    void FactoryZero();

    //--Glitch
    float GetRenderDoubleOffset();
    StarBitmap *GetCreepyOverlay();
    bool IsBackgroundInverted();
    void RollGlitchEffects();

    //--Investigation
    void ConstructInvestigation();
    int GetInvestigationTime();
    int GetInvestigationTimeMax();
    void SetInvestigationTime(int pCurrentTime);
    void SetInvestigationTimeMax(int pMaxTime);
    void ActivateInvestigationMode();
    void DeactivateInvestigationMode();
    void ActivateTalkingMode();
    void DeactivateTalkingMode();
    void ClearInvestigationData();
    void ClearInvestigationCharacterData();
    void SetGoHomeStrings(const char *pScript, const char *pArg);
    void ClearDiscoveredObjectPtr();
    void AddInvestigationObject(const char *pName, TwoDimensionReal pDimensions, const char *pIconPath, const char *pScriptPath, const char *pScriptArg);
    void AddInvestigationObject(const char *pName, TwoDimensionReal pDimensions, const char *pIconPath, const char *pScriptPath, const char *pScriptArg, float pX1, float pY1, float pX2, float pY2);
    void ClearInvestigationObjects();
    void AddRenderableObject(const char *pName, float pX1, float pY1, float pX2, float pY2);
    void AddDiscoveredObject(const char *pName, const char *pIconPath, const char *pScriptPath, const char *pScriptArg);
    void AddDiscoveredObject(const char *pName, const char *pIconPath, const char *pScriptPath, const char *pScriptArg, float pX1, float pY1, float pX2, float pY2);
    void RemoveDiscoveredObject(const char *pName);
    void RegisterTalkableCharacter(const char *pName, const char *pScriptPath);
    void UpdateInvestigationMode();
    void RenderInvestigationMode();

    //--Name Entry
    void ActivateNameEntry();
    void DeactivateNameEntry();
    void SetNameEntryString(const char *pString);
    void UpdateNameEntry();
    void RenderNameEntry();

    //--SLF Parser
    void ParseSLFFile(const char *pPath);
    void HandleObject(ObjectInfoPack *pObject);

    //--Tags
    void RegisterTagRemap(const char *pTag, const char *pRemapTo);
    void HandleTag(const char *pTag, bool pAllowRemaps);
    void HandleTagQuick(const char *pTag, bool pAllowRemaps);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    static PairanormalDialogue *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Dia_CreateCharacter(lua_State *L);
int Hook_Dia_SetProperty(lua_State *L);
