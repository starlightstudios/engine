-- |[ ================================== SysPaths Activation =================================== ]|
--Handler for startup, activates all games after scanning is complete.

-- |[ ============================= SysPaths:fnActivateAllGames() ============================== ]|
--Activates all games that are on the scanned lists. This sets their active paths to either the
-- first available path, or "Null" if the game wasn't found.
function SysPaths:fnActivateAllGames()

    -- |[Diagnostics]|
    local bDiagnostics = fnAutoCheckDebugFlag("SysPaths: Activation")
    Debug_PushPrint(bDiagnostics, "SysPaths:fnActivateAllGames() - Begins.\n")
    Debug_Print("There are " .. #self.zaGameEntries .. " entries.\n")

    -- |[Execution]|
    for i = 1, #self.zaGameEntries, 1 do
        
        --Get the name.
        local sEntryName = self.zaGameEntries[i].sName
        
        --Call internal function. If it's false, just print a warning if flagged.
        if(self:fnGameEntryHasValidPath(sEntryName) == false) then
            Debug_Print("Game " .. sEntryName .. " does not have any valid paths.\n")
        
        --Entry exists and has a hard drive path. Print a notice.
        else
            Debug_Print("Game " .. sEntryName .. " has valid path: " .. self.zaGameEntries[i].sActivePath .. "\n")
        end
    end
    
    -- |[Diagnostics]|
    Debug_PopPrint("Finished.\n")
end