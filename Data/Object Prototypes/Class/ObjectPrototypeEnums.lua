-- |[ ============================== ObjectPrototype Enumerations ============================== ]|
--Enumerations that may be common across multiple object prototype files.

-- |[Constants]|
gciDefaultStatEnum = 4

-- |[ ========================== ObjectPrototype:fnStatisticGenEnums() ========================== ]|
--Sets enumerations associated with statistic formula generation. These range from F to A to S++.
-- Default value is optional.
function ObjectPrototype:fnStatisticGenEnums(psFieldName, piDefault)
    
    -- |[Argument Check]|
    if(psFieldName == nil) then return end
    if(piDefault == nil) then piDefault = gciDefaultStatEnum end
    
    -- |[Set]|
    SOP_SetProperty("Add Integer Field",              psFieldName, piDefault)
    SOP_SetProperty("Allocate Enumerations In Field", psFieldName, 9)
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 0, 0, "F")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 1, 1, "E")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 2, 2, "D")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 3, 3, "C")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 4, 4, "B")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 5, 5, "A")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 6, 6, "S")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 7, 7, "S+")
    SOP_SetProperty("Set Enumerations In Field",      psFieldName, 8, 8, "S++")
    SOP_SetProperty("Set Enumerations Locking Flag",  psFieldName, true)
    SOP_SetProperty("Set Field Description",          psFieldName, "The letter grade associated with this stat. B=4 is 'average', F is terrible, S and above should be reserved for specialists.")
end