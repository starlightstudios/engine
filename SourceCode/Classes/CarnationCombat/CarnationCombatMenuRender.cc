//--Base
#include "CarnationCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "CutsceneManager.h"

///====================================== Command Window ==========================================
void CarnationCombat::RenderCommandWindow(AdvCombatEntity *pEntity, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the command window for the currently acting entity, which is assumed to be the one passed in.
    //  The commands are kept globally by the CarnationCombat object, but reconstructed whenever an entity begins
    //  its turn. The window appears along the top, spread out horizontally.
    if(!pEntity || pAlpha <= 0.0f) return;

    //--If there is dialogue or pending events, don't show the window.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rWorldDialogue->IsVisible()) return;
    if(rCutsceneManager->HasAnyEvents()) return;
    if(mIsShowingTutorialOverlay) return;

    ///--[Skills Window]
    //--If the player is using a derived window, render that. Skills show more information than the base window does.
    StarLinkedList *rPlayerDerivedList = ResolvePlayerCommandList();
    if(rPlayerDerivedList != mCommandList)
    {
        RenderSkillsWindow(pEntity, pAlpha);
        return;
    }

    ///--[Resolve List]
    //--Resolve which command list is in use.
    StarLinkedList *rPlayerStackList = mCommandList;//ResolvePlayerCommandList();
    StarLinkedList *rCurrentStackHead = ResolvePlayerCommandList();

    //--Figure out where the cursor should be. If the command list isn't the current head, figure out
    //  which entry on the main list contains the head, and use that.
    int tUseCursor = mPlayerCommandCursor;
    if(rPlayerStackList != rCurrentStackHead)
    {
        //--Zero the cursor.
        tUseCursor = 0;

        //--Iterate.
        CommandListEntry *rCheckEntry = (CommandListEntry *)mCommandList->PushIterator();
        while(rCheckEntry)
        {
            //--Match.
            if(rCheckEntry->mSublist == mCommandList)
            {
                mCommandList->PopIterator();
                break;
            }

            //--Next.
            tUseCursor ++;
            rCheckEntry = (CommandListEntry *)mCommandList->AutoIterate();
        }
    }

    ///--[Render Buttons]
    CarnButton *rButton = (CarnButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--Render the button backing.
        CarnationImg.Image.rFrame_Button->Draw(rButton->mPosition.mLft, rButton->mPosition.mTop);

        //--Render the name, justified.
        float cMidX = rButton->mPosition.mXCenter;
        float cMidY = rButton->mPosition.mYCenter;
        CarnationImg.Font.rFontButton->DrawTextFixed(cMidX, cMidY, 134.0f, SUGARFONT_AUTOCENTER_XY, rButton->mDisplayString);

        //--Next.
        rButton = (CarnButton *)mButtonList->AutoIterate();
    }
}
void CarnationCombat::RenderTargetWindow(float pAlpha)
{
    ///--[Documentation]
    //--Renders a window depicting the entries on the current target list. If not in target mode, disappears.
    if(!mIsSelectingTargets || pAlpha <= 0.0f) return;

    //--If there is dialogue or pending events, don't show the window.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rWorldDialogue->IsVisible()) return;
    if(rCutsceneManager->HasAnyEvents()) return;
    if(mIsShowingTutorialOverlay) return;

    ///--[Render Buttons]
    CarnButton *rButton = (CarnButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--Render the button backing.
        CarnationImg.Image.rFrame_Button->Draw(rButton->mPosition.mLft, rButton->mPosition.mTop);

        //--Render the name, justified.
        float cMidX = rButton->mPosition.mXCenter;
        float cMidY = rButton->mPosition.mYCenter;
        CarnationImg.Font.rFontButton->DrawTextFixed(cMidX, cMidY, 134.0f, SUGARFONT_AUTOCENTER_XY, rButton->mDisplayString);

        //--Next.
        rButton = (CarnButton *)mButtonList->AutoIterate();
    }
}
void CarnationCombat::RenderSkillsWindow(AdvCombatEntity *pEntity, float pAlpha)
{
    ///--[Documentation]
    //--If the acting entity has opened the skills window, or items window, etc, this function gets called to render it below
    //  the main command window.
    if(!pEntity || pAlpha <= 0.0f) return;

    //--If the player is selecting targets, don't render the skill list.
    if(mIsSelectingTargets) return;

    //--Get the stack head.
    StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();

    //--Fast-access pointers.
    StarFont *rMPFont = CarnationImg.Font.rPlayerInterfaceFont;

    ///--[Render Buttons]
    int i = 0;
    CarnButton *rButton = (CarnButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--Render the button backing.
        CarnationImg.Image.rFrame_Button->Draw(rButton->mPosition.mLft, rButton->mPosition.mTop);

        //--Render the name, justified.
        float cMidX = rButton->mPosition.mXCenter;
        float cMidY = rButton->mPosition.mYCenter;
        CarnationImg.Font.rFontButton->DrawTextFixed(cMidX, cMidY, 134.0f, SUGARFONT_AUTOCENTER_XY, rButton->mDisplayString);

        //--Compute positions for MP/Cooldown.
        float cCDX = rButton->mPosition.mLft;
        float cCDY = rButton->mPosition.mBot;

        //--Check if there's a command-list entry that matches this. We need that to get the costs.
        CommandListEntry *rEntry = (CommandListEntry *)rPlayerStackList->GetElementBySlot(i);
        if(rEntry)
        {
            //--If not available, show the cooldown, or MP/CP cost if applicable.
            if(rEntry->rAssociatedAbility && !rEntry->rAssociatedAbility->IsUsableNow())
            {
                //--Manual locking disables requirements.
                if(rEntry->mIsManuallyLocked)
                {

                }
                //--Cooldown:
                else if(rEntry->rAssociatedAbility->GetCooldown() > 0)
                {
                    rMPFont->DrawTextArgs(cCDX, cCDY, 0, 1.0f, "CD %i", rEntry->rAssociatedAbility->GetCooldown());
                }
                //--MP:
                else if(rEntry->rAssociatedAbility->GetMPCost() > 0)
                {
                    rMPFont->DrawTextArgs(cCDX, cCDY, 0, 1.0f, "SP %i", rEntry->rAssociatedAbility->GetMPCost());
                }
                //--CP:
                else if(rEntry->rAssociatedAbility->GetCPCost() > 0)
                {
                    rMPFont->DrawTextArgs(cCDX, cCDY, 0, 1.0f, "CP %i", rEntry->rAssociatedAbility->GetCPCost());
                }
            }
            //--Available. Show MP/CP cost.
            else if(rEntry->rAssociatedAbility)
            {
                //--MP:
                if(rEntry->rAssociatedAbility->GetMPCost() > 0)
                {
                    rMPFont->DrawTextArgs(cCDX, cCDY, 0, 1.0f, "SP %i", rEntry->rAssociatedAbility->GetMPCost());
                }
                //--CP:
                else if(rEntry->rAssociatedAbility->GetCPCost() > 0)
                {
                    rMPFont->DrawTextArgs(cCDX, cCDY, 0, 1.0f, "CP %i", rEntry->rAssociatedAbility->GetCPCost());
                }
                //--Quantity. Optional, only used for items.
                else if(rEntry->mQuantity > 0)
                {
                    rMPFont->DrawTextArgs(cCDX, cCDY, 0, 1.0f, "x%i", rEntry->mQuantity);
                }
            }
        }

        //--Next.
        i ++;
        rButton = (CarnButton *)mButtonList->AutoIterate();
    }
}

///====================================== Command Entries =========================================
void CarnationCombat::RenderCommandEntry(float pX, float pY, bool pIsHighlight, StarFont *pFont, CommandListEntry *pEntry)
{
    ///--[Documentation]
    //--Renders a menu command at the given location. Automatically checks if the command should be highlighted, greyed out,
    //  show requirements, etc.
    if(!pFont || !pEntry) return;

    //--Constants
    float cCursorInd = 14.0f;
    float cCostIndent = 200.0f;

    ///--[Highlight]
    //--If this is the highlighted ability, show the cursor.
    if(pIsHighlight)
    {
        pFont->DrawText(pX, pY, 0, 1.0f, ">");
    }

    ///--[Display Name]
    //--Show the display name. Grey it out if not usable.
    if(pEntry->mDisplayName)
    {
        //--Always grey out if manually locked.
        if(pEntry->mIsManuallyLocked)
        {
            StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);
        }
        //--Grey out.
        else if(pEntry->rAssociatedAbility && !pEntry->rAssociatedAbility->IsUsableNow())
        {
            StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);
        }
        //--Alternately, no ability, and sublist has no entries.
        else if(!pEntry->rAssociatedAbility && (!pEntry->mSublist || pEntry->mSublist->GetListSize() < 1))
        {
            StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);
        }
        //--Usable, or has no ability, and is glowing.
        else if(pEntry->mGlowing)
        {
            float cGlowPct = EasingFunction::SinusoidalInOut(mCommandGlowTimer / (float)ABYCOM_COMMAND_GLOW_TICKS);
            StarlightColor::SetMixer(1.0f - (0.5f * cGlowPct), 1.0f - (0.5f * cGlowPct), 0.5f + (0.5f * cGlowPct), 1.0f);
        }

        //--Render.
        pFont->DrawTextArgs(pX + cCursorInd, pY, 0, 1.0f, pEntry->mDisplayName);
        StarlightColor::ClearMixer();
    }

    ///--[Requirements]
    //--If not available, show the cooldown, or MP/CP cost if applicable.
    if(pEntry->rAssociatedAbility && !pEntry->rAssociatedAbility->IsUsableNow())
    {
        //--Manual locking disables requirements.
        if(pEntry->mIsManuallyLocked)
        {

        }
        //--Cooldown:
        else if(pEntry->rAssociatedAbility->GetCooldown() > 0)
        {
            pFont->DrawTextArgs(pX + cCostIndent, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "CD %i", pEntry->rAssociatedAbility->GetCooldown());
        }
        //--MP:
        else if(pEntry->rAssociatedAbility->GetMPCost() > 0)
        {
            pFont->DrawTextArgs(pX + cCostIndent, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "MP %i", pEntry->rAssociatedAbility->GetMPCost());
        }
        //--CP:
        else if(pEntry->rAssociatedAbility->GetCPCost() > 0)
        {
            pFont->DrawTextArgs(pX + cCostIndent, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "CP %i", pEntry->rAssociatedAbility->GetCPCost());
        }
    }
    //--Available. Show MP/CP cost.
    else if(pEntry->rAssociatedAbility)
    {
        //--MP:
        if(pEntry->rAssociatedAbility->GetMPCost() > 0)
        {
            pFont->DrawTextArgs(pX + cCostIndent, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "MP %i", pEntry->rAssociatedAbility->GetMPCost());
        }
        //--CP:
        else if(pEntry->rAssociatedAbility->GetCPCost() > 0)
        {
            pFont->DrawTextArgs(pX + cCostIndent, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "CP %i", pEntry->rAssociatedAbility->GetCPCost());
        }
        //--Quantity. Optional, only used for items.
        else if(pEntry->mQuantity > 0)
        {
            pFont->DrawTextArgs(pX + cCostIndent, pY, SUGARFONT_RIGHTALIGN_X, 1.0f, "x%i", pEntry->mQuantity);
        }
    }
}
