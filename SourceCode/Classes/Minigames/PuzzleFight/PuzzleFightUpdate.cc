//--Base
#include "PuzzleFight.h"

//--Classes
#include "AdventureLevel.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "LuaManager.h"

///========================================== Update ==============================================
void PuzzleFight::Update()
{
    ///--[Documentation]
    //--Handles updating timers and script calls for the object. Does not handle player input, that is
    //  called by the owning AdventureLevel at the appropriate time.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;

    //--Fast-access pointers.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();

    //--Update code storage. Updates are called in order.
    StarLinkedList *tUpdateCodeList = new StarLinkedList(true);

    ///--[Takedown]
    //--If the object is taking itself down, only run these timers.
    if(mIsTakingDown)
    {
        if(mUISlideTimer > 0)
            mUISlideTimer --;
        else
            SetComplete(true);
        return;
    }

    ///--[Player's Turn Initialization]
    //--If the game state is MINIGAME_STATE_PLAYERSTURNINIT, run the update handler before anything else.
    //  This will set up things like the amount of time the player has, turns, and spawn objects.
    if(mGameState == MINIGAME_STATE_PLAYERSTURNINIT && mUpdateScript)
    {
        mBoardActionList->ClearList();
        LuaManager::Fetch()->ExecuteLuaFile(mUpdateScript, 1, "N", (float)MINIGAME_STATE_PLAYERSTURNINIT);
    }

    ///--[Notifiers]
    //--Updates all notifier packs. If one is done, deletes it.
    BoardNotifierPack *rNotifier = (BoardNotifierPack *)mNotifierList->SetToHeadAndReturn();
    while(rNotifier)
    {
        rNotifier->Update();
        if(rNotifier->IsComplete())
        {
            mNotifierList->RemoveRandomPointerEntry();
        }
        rNotifier = (BoardNotifierPack *)mNotifierList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Timers]
    //--UI Timer.
    if(mUISlideTimer < UI_SLIDE_TICKS) mUISlideTimer ++;

    //--Confirm overlay.
    if(mShowConfirmDialogue)
    {
        if(mConfirmTimer < UI_CONFIRM_TICKS) mConfirmTimer ++;
    }
    else
    {
        if(mConfirmTimer > 0) mConfirmTimer --;
    }

    //--Health Easing Packs
    mPlayerHealthEasing.Increment(EASING_CODE_QUADIN);
    mChallengerHealthEasing.Increment(EASING_CODE_QUADIN);

    //--Board objects.
    bool tBoardObjectsAreMoving = false;
    bool tBoardObjectsFinishedMove = true;
    for(int x = 0; x < mBoardW; x ++)
    {
        for(int y = 0; y < mBoardH; y ++)
        {
            if(mBoardObjects[x][y].mPosition.mTimer < mBoardObjects[x][y].mPosition.mTimerMax) tBoardObjectsAreMoving = true;
            mBoardObjects[x][y].mPosition.Increment(mBoardObjects[x][y].mEasingType);
            mBoardObjects[x][y].mDupePosition.Increment(mBoardObjects[x][y].mEasingType);
            if(mBoardObjects[x][y].mPosition.mTimer < mBoardObjects[x][y].mPosition.mTimerMax) tBoardObjectsFinishedMove = false;
        }
    }
    if(tBoardObjectsAreMoving == true && tBoardObjectsFinishedMove == true)
    {
        int *nUpdate = (int *)starmemoryalloc(sizeof(int));
        *nUpdate = HANDLER_CODE_SPAWNED_OBJECTS_MOVED;
        tUpdateCodeList->AddElementAsTail("X", nUpdate, &FreeThis);
    }

    //--Board timer. Dictates moving board objects.
    if(mBoardSlideTimer > 0)
    {
        //--Decrement.
        mBoardSlideTimer --;

        //--Update handled.
        if(mBoardSlideTimer == 0)
        {
            int *nUpdate = (int *)starmemoryalloc(sizeof(int));
            *nUpdate = HANDLER_CODE_MOVED;
            tUpdateCodeList->AddElementAsTail("X", nUpdate, &FreeThis);
        }

        //--Debug.
        if(false && mBoardSlideTimer == 0)
        {
            fprintf(stderr, "Board Layout:\n");
            for(int y = 0; y < mBoardH; y ++)
            {
                for(int x = 0; x < mBoardW; x ++)
                {
                    fprintf(stderr, "%02i ", rDislocationBoard[x][y]->mDebugNum);
                }
                fprintf(stderr, "\n");
            }
        }
    }

    //--Player's Timer.
    if(mPlayerTicksLeft > 0)
    {
        //--Player is on the "Accept" prompt. This stops the timer.
        if(mShowConfirmDialogue)
        {

        }
        //--Dialogue is open. Don't run the timer.
        else if(rWorldDialogue->IsManagingUpdates() || rCutsceneManager->HasAnyEvents())
        {

        }
        //--All other cases, decrement.
        else
        {
            //--Decrement.
            mPlayerTicksLeft --;

            //--If the ticks hit zero, this is the same as starting the action.
            if(mPlayerTicksLeft < 1)
            {
                mGameState = MINIGAME_STATE_PLAYERSTURNACT;
                LuaManager::Fetch()->ExecuteLuaFile(mUpdateScript, 1, "N", (float)HANDLER_CODE_CONFIRMACTIONDONE);
            }
        }
    }

    ///--[Title Print Handling]
    //--In the timing phase, run the timer. If it caps, go to the hold phase.
    if(mTitlePrintPhase == PF_TITLE_PRINT_PRINT)
    {
        //--Timer.
        mTitlePrintTimer ++;

        //--Boot sound effect.
        if(mTitlePrintTimer == 1)
        {
            AudioManager::Fetch()->PlaySound("Puzzle|TitleAppear");
        }

        //--Sound effect.
        if(mTitlePrintTimer % TITLE_PRINT_TICKS_PER_LETTER == 0 && mTitlePrintTimer > 5)
        {
            AudioManager::Fetch()->PlaySound("Puzzle|LetterPrint");
        }

        //--Cap.
        if(mTitlePrintTimer >= mTitlePrintTimerMax)
        {
            mTitlePrintPhase = PF_TITLE_PRINT_HOLD;
            mTitlePrintTimer = 0;
            mTitlePrintTimerMax = TITLE_PRINT_TICKS_TO_HOLD;
        }
    }
    //--Holding. Moves to fading when capped.
    else if(mTitlePrintPhase == PF_TITLE_PRINT_HOLD)
    {
        mTitlePrintTimer ++;
        if(mTitlePrintTimer >= mTitlePrintTimerMax)
        {
            mTitlePrintPhase = PF_TITLE_PRINT_FADE;
            mTitlePrintTimer = 0;
            mTitlePrintTimerMax = TITLE_PRINT_TICKS_TO_FADE;
        }
    }
    //--In the fading phase, run the timer. If it caps, reset.
    else if(mTitlePrintPhase == PF_TITLE_PRINT_FADE)
    {
        mTitlePrintTimer ++;
        if(mTitlePrintTimer >= mTitlePrintTimerMax)
        {
            mTitlePrintPhase = PF_TITLE_PRINT_NONE;
        }
    }

    ///--[Dislocations]
    //--Move dislocations towards their destinations.
    for(int i = 0; i < mDislocationsTotal; i ++)
    {
        //--Move towards X destination.
        if(mDislocations[i].mDestinationX < mDislocations[i].mCurrentX - mBoardScrollSpeed)
        {
            mDislocations[i].mCurrentX = mDislocations[i].mCurrentX - mBoardScrollSpeed;
        }
        else if(mDislocations[i].mDestinationX > mDislocations[i].mCurrentX + mBoardScrollSpeed)
        {
            mDislocations[i].mCurrentX = mDislocations[i].mCurrentX + mBoardScrollSpeed;
        }
        else
        {
            mDislocations[i].mCurrentX = mDislocations[i].mDestinationX;
        }

        //--Move towards Y destination.
        if(mDislocations[i].mDestinationY < mDislocations[i].mCurrentY - mBoardScrollSpeed)
        {
            mDislocations[i].mCurrentY = mDislocations[i].mCurrentY - mBoardScrollSpeed;
        }
        else if(mDislocations[i].mDestinationY > mDislocations[i].mCurrentY + mBoardScrollSpeed)
        {
            mDislocations[i].mCurrentY = mDislocations[i].mCurrentY + mBoardScrollSpeed;
        }
        else
        {
            mDislocations[i].mCurrentY = mDislocations[i].mDestinationY;
        }

        //--Set dislocation position.
        rActiveLevel->ModifyDislocationPack(mDislocations[i].mDislocationName, mDislocations[i].mCurrentX, mDislocations[i].mCurrentY);
    }

    ///--[Cursor Handling]
    //--Move the player's cursor to cover whatever area they are moving.
    if(mCursorMoveTimer < CURSOR_MOVE_TICKS)
    {
        //--Increment.
        mCursorMoveTimer ++;

        //--Compute percentage. Set position.
        float tPercent = EasingFunction::QuadraticOut(mCursorMoveTimer, CURSOR_MOVE_TICKS);
        mCursorCurrent.mLft = (mCursorStart.mLft + (mCursorFinish.mLft - mCursorStart.mLft) * tPercent);
        mCursorCurrent.mTop = (mCursorStart.mTop + (mCursorFinish.mTop - mCursorStart.mTop) * tPercent);
        mCursorCurrent.mRgt = (mCursorStart.mRgt + (mCursorFinish.mRgt - mCursorStart.mRgt) * tPercent);
        mCursorCurrent.mBot = (mCursorStart.mBot + (mCursorFinish.mBot - mCursorStart.mBot) * tPercent);
    }

    ///--[Update Handler]
    //--If there are any updates on the list, run them here. Then, wipe the list.
    if(mUpdateScript)
    {
        int *rUpdateCode = (int *)tUpdateCodeList->PushIterator();
        while(rUpdateCode)
        {
            int tValue = *rUpdateCode;
            LuaManager::Fetch()->ExecuteLuaFile(mUpdateScript, 1, "N", (float)tValue);
            rUpdateCode = (int *)tUpdateCodeList->AutoIterate();
        }
    }
}
void PuzzleFight::HandlePlayerControls()
{
    ///--[Documentation]
    //--Handles player input when the object is intercepting player controls.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--World dialogue is open. Do nothing.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rWorldDialogue->IsManagingUpdates() || rCutsceneManager->HasAnyEvents()) return;

    ///--[Debug]
    //--Push a set of controls to quit.
    if(mQuitSequence == PF_DEBUGQUIT_BACKSPACE && rControlManager->IsFirstPress("Backspace"))
    {
        mQuitSequence = PF_DEBUGQUIT_F5;
    }
    else if(mQuitSequence == PF_DEBUGQUIT_F5 && rControlManager->IsFirstPress("F5"))
    {
        SetComplete(true);
    }

    ///--[Control Lockout]
    //--If the mode of the game is not MINIGAME_STATE_PLAYERSTURNRUN, the player cannot do inputs.
    if(mGameState != MINIGAME_STATE_PLAYERSTURNRUN) return;

    ///--[Accept]
    //--If the player is not in confirm mode, moves to confirm mode.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Activate.
        if(!mShowConfirmDialogue)
        {
            mShowConfirmDialogue = true;
        }
        //--Accepts the board as it is and begins running the player's action. This
        //  blocks further controls and immediately issues a handler call.
        else
        {
            mShowConfirmDialogue = false;
            mGameState = MINIGAME_STATE_PLAYERSTURNACT;
            LuaManager::Fetch()->ExecuteLuaFile(mUpdateScript, 1, "N", (float)HANDLER_CODE_CONFIRMACTIONDONE);
            return;
        }
    }
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        if(mShowConfirmDialogue)
        {
            mShowConfirmDialogue = false;
            return;
        }
    }

    ///--[Cursor]
    //--Cursor is horizontal, moving objects left and right.
    bool tCursorChanged = false;
    if(mIsCursorHorizontal)
    {
        ///--[Pressing Up / Down]
        if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
        {
            tCursorChanged = true;
            mCursorSlot = mCursorSlot - 1;
            if(mCursorSlot < 0) mCursorSlot = mBoardH - 1;
            AudioManager::Fetch()->PlaySound("World|FlipSwitch");
        }
        else if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
        {
            tCursorChanged = true;
            mCursorSlot = mCursorSlot + 1;
            if(mCursorSlot >= mBoardH) mCursorSlot = 0;
            AudioManager::Fetch()->PlaySound("World|FlipSwitch");
        }

        ///--[Pressing Left / Right]
        //--These move objects on the board.
        if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
        {
            if(mPlayerMovesLeft > 0 && MoveRowLeft(mCursorSlot))
            {
                mPlayerMovesLeft --;
                CreateActionPack(BOARD_MOVE_RT, mCursorSlot);
                AudioManager::Fetch()->PlaySound("World|BlockSlide");
            }
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
        else if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
        {
            if(mPlayerMovesLeft > 0 && MoveRowRight(mCursorSlot))
            {
                mPlayerMovesLeft --;
                CreateActionPack(BOARD_MOVE_LF, mCursorSlot);
                AudioManager::Fetch()->PlaySound("World|BlockSlide");
            }
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }

        ///--[Switching Orientations]
        if(rControlManager->IsFirstPress("UpLevel") || rControlManager->IsFirstPress("DnLevel"))
        {
            tCursorChanged = true;
            mIsCursorHorizontal = !mIsCursorHorizontal;
            mCursorSlot = mBoardW / 2;
            AudioManager::Fetch()->PlaySound("World|FlipSwitch");
        }
    }
    //--Cursor is vertical, moving objects up and down.
    else
    {
        ///--[Pressing Left / Right]
        if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
        {
            tCursorChanged = true;
            mCursorSlot = mCursorSlot - 1;
            if(mCursorSlot < 0) mCursorSlot = mBoardW - 1;
            AudioManager::Fetch()->PlaySound("World|FlipSwitch");
        }
        else if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
        {
            tCursorChanged = true;
            mCursorSlot = mCursorSlot + 1;
            if(mCursorSlot >= mBoardW) mCursorSlot = 0;
            AudioManager::Fetch()->PlaySound("World|FlipSwitch");
        }

        ///--[Pressing Up / Down]
        //--These move objects on the board.
        if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
        {
            if(mPlayerMovesLeft > 0 && MoveColumnUp(mCursorSlot))
            {
                mPlayerMovesLeft --;
                CreateActionPack(BOARD_MOVE_DN, mCursorSlot);
                AudioManager::Fetch()->PlaySound("World|BlockSlide");
            }
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
        else if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
        {
            if(mPlayerMovesLeft > 0 && MoveColumnDown(mCursorSlot))
            {
                mPlayerMovesLeft --;
                CreateActionPack(BOARD_MOVE_UP, mCursorSlot);
                AudioManager::Fetch()->PlaySound("World|BlockSlide");
            }
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }

        ///--[Switching Orientations]
        if(rControlManager->IsFirstPress("UpLevel") || rControlManager->IsFirstPress("DnLevel"))
        {
            tCursorChanged = true;
            mIsCursorHorizontal = !mIsCursorHorizontal;
            mCursorSlot = mBoardH / 2;
            AudioManager::Fetch()->PlaySound("World|FlipSwitch");
        }
    }

    //--If the cursor changed, adjust it to the new position.
    if(tCursorChanged || mUpdateCursorImmediately)
    {
        //--Constants
        float cTileSize = 16.0f;

        //--Reset timer.
        mCursorMoveTimer = 0;

        //--Set the starting position of the cursor as whatever position it was in before.
        memcpy(&mCursorStart, &mCursorCurrent, sizeof(TwoDimensionReal));

        //--Compute the dimensions of the destination.
        if(mIsCursorHorizontal)
        {
            mCursorFinish.mLft = (mBoardStartX * cTileSize);
            mCursorFinish.mRgt = mCursorFinish.mLft + (mBoardTileSizeX * mBoardW * cTileSize);
            mCursorFinish.mTop = (mBoardStartY * cTileSize) + (mCursorSlot * mBoardTileSizeY * cTileSize);
            mCursorFinish.mBot = mCursorFinish.mTop + (mBoardTileSizeY * cTileSize);
        }
        else
        {
            mCursorFinish.mTop = (mBoardStartY * cTileSize);
            mCursorFinish.mBot = mCursorFinish.mTop + (mBoardTileSizeY * mBoardH * cTileSize);
            mCursorFinish.mLft = (mBoardStartX * cTileSize) + (mCursorSlot * mBoardTileSizeY * cTileSize);
            mCursorFinish.mRgt = mCursorFinish.mLft + (mBoardTileSizeY * cTileSize);
        }

        //--If this flag is active, move the cursor immediately.
        if(mUpdateCursorImmediately)
        {
            mUpdateCursorImmediately = false;
            mCursorMoveTimer = CURSOR_MOVE_TICKS - 1;
        }
    }

    ///--[Cancel an Action]
    //--If there are no actions taking place, and the player presses the cancel button:
    if(mBoardSlideTimer < 1 && mBoardActionList->GetListSize() > 0 && rControlManager->IsFirstPress("Cancel"))
    {
        //--Get the last action package. Note that the code will be the opposite of the original
        //  action that created it.
        BoardActionPack *rLastPackage = (BoardActionPack *)mBoardActionList->GetTail();
        if(rLastPackage->mCode == BOARD_MOVE_UP)
        {
            MoveColumnUp(rLastPackage->mCursor);
        }
        else if(rLastPackage->mCode == BOARD_MOVE_RT)
        {
            MoveRowRight(rLastPackage->mCursor);
        }
        else if(rLastPackage->mCode == BOARD_MOVE_DN)
        {
            MoveColumnDown(rLastPackage->mCursor);
        }
        else if(rLastPackage->mCode == BOARD_MOVE_LF)
        {
            MoveRowLeft(rLastPackage->mCursor);
        }

        //--Refund a move point.
        mPlayerMovesLeft ++;

        //--SFX.
        AudioManager::Fetch()->PlaySound("World|BlockSlide");

        //--In all cases, dispose of the package.
        mBoardActionList->DeleteTail();
    }
}
