//--Base
#include "MonoUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== Update ==============================================
void MonoUIVendor::UpdateModeUnlock()
{
    ///--[Documentation]
    //--Update handler for Unlock mode. Same as the base version, but adjusts the scrollbar behavior.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Run timers.
    StandardCycleTimer(mUnlockScrollTimer, mUnlockScrollTimerPause, 0, cxAdvUnlockCycleTicks, mUnlockScrollTimerUp, cxAdvUnlockPauseTicks);
    StandardTimer(mAskToEquipTimer,    0, cxAdvAskEquipTicks, mAskToEquip);
    StandardTimer(mUnlockConfirmTimer, 0, cxAdvBuyTicks,      mIsUnlockingItem);

    //--Update blockers. If these timers are not capped, stop the update.
    if(mDetailsTimer > 0 && mDetailsTimer < cxAdvDetailsTicks) return;

    ///--[List Resolve]
    //--Resolve which list to use. Categories can be changed.
    StarLinkedList *rActiveCategory = GetConsideredCategory();

    ///--[Mode Handlers]

    ///--[Unlocking Handler]
    //--Call mode handlers.
    if(mIsUnlockingItem) { UpdateConfirmUnlock(); return; }
    if(mAskToEquip)      { UpdateBuyEquipItem();  return; }

    //--Details update does not lock all controls, just accept/cancel.
    bool tIsDetailsUpdate = CommonDetailsUpdate();

    ///--[Shoulder Buttons]
    //--Shoulder-left. Decrements the comparison character, or changes materials page.
    if(rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel"))
    {
        //--Comparison character. The user is not holding ctrl. Attempt to scroll the character.
        if(!rControlManager->IsDown("Ctrl")) { ScrollComparisonCharacter(-1); return; }

        //--Change materials page.
        mMaterialsPage --;
        if(mMaterialsPage < 0) mMaterialsPage = mMaterialsPages->GetListSize() - 1;
        if(mMaterialsPage < 0) mMaterialsPage = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Shoulder-right. Increments the comparison character, or changes materials page.
    if(rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel"))
    {
        //--Comparison character. The user is not holding ctrl. Attempt to scroll the character.
        if(!rControlManager->IsDown("Ctrl")) { ScrollComparisonCharacter(-1); return; }

        //--Change materials page.
        mMaterialsPage ++;
        if(mMaterialsPage >= mMaterialsPages->GetListSize()) mMaterialsPage = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[OpenFieldAbilityMenu Button]
    //--Toggles the relative/absolute flag.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        mShowRelativeValuesOnly = !mShowRelativeValuesOnly;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Run Button]
    //--Toggles which comparison slot is in use.
    if(rControlManager->IsFirstPress("Run") && mCanSwapCompareSlot)
    {
        ToggleComparisonSlot();
        return;
    }

    ///--[Jump Button]
    //--Toggles materials vs comparison mode.
    if(rControlManager->IsFirstPress("Jump"))
    {
        mComparisonShowMaterials = !mComparisonShowMaterials;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Up and Down]
    //--Up decrements, down increments. Wrapping is enabled.
    if(AutoListUpDn(mCursor, mSkip, rActiveCategory->GetListSize(), cScrollBuf, cxMono_UnlScrollPage, true))
    {
        //--Modify cursor.
        RecomputeCursorPositions();

        //--Resets scroll timers.
        mUnlockScrollTimerUp = true;
        mUnlockScrollTimer = 0;
        mUnlockScrollTimerPause = cxAdvUnlockPauseTicks;

        //--The comparison character may have changed. Recheck that here.
        RecheckComparisonCharacter(GetConsideredItem());
        return;
    }

    ///--[Category Switching]
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && mCategoryList->GetListSize() > 0)
    {
        //--If the category list contains exactly one category, do nothing.
        if(mCategoryList->GetListSize() <= 1)
        {

        }
        //--Otherwise, switch:
        else
        {
            //--Flags.
            mCursor = 0;
            mSkip = 0;

            //--Decrement.
            mCategoryCursor --;
            if(mCategoryCursor < 0) mCategoryCursor = mCategoryList->GetListSize() - 1;
            if(mCategoryCursor < 0) mCategoryCursor = 0;

            //--Re-resolve the list in question.
            if(mCategoryList->GetListSize() > 0)
            {
                ShopCategoryPack *rCategoryPack = (ShopCategoryPack *)mCategoryList->GetElementBySlot(mCategoryCursor);
                if(rCategoryPack) rActiveCategory = rCategoryPack->mrMembers;
            }

            //--Cursor.
            RecomputeCursorPositions();
            RecomputeCategoryCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && mCategoryList->GetListSize() > 0)
    {
        //--If the category list contains exactly one category, do nothing.
        if(mCategoryList->GetListSize() <= 1)
        {

        }
        //--Otherwise, switch:
        else
        {
            //--Flags.
            mCursor = 0;
            mSkip = 0;

            //--Increment.
            mCategoryCursor ++;
            if(mCategoryCursor >= mCategoryList->GetListSize()) mCategoryCursor = 0;

            //--Re-resolve the list in question.
            if(mCategoryList->GetListSize() > 0)
            {
                ShopCategoryPack *rCategoryPack = (ShopCategoryPack *)mCategoryList->GetElementBySlot(mCategoryCursor);
                if(rCategoryPack) rActiveCategory = rCategoryPack->mrMembers;
            }

            //--Cursor.
            RecomputeCursorPositions();
            RecomputeCategoryCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Activate]
    //--Details mode locks controls below this point.
    if(tIsDetailsUpdate) return;

    //--Begins unlock of the highlighted item.
    if(rControlManager->IsFirstPress("Activate") && mShopInventory->GetListSize() > 0 && !mIsShowingDetails)
    {
        //--Fast-access pointers.
        AdventureInventory *rInventory = AdventureInventory::Fetch();

        //--Get the item in question.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)rActiveCategory->GetElementBySlot(mCursor);
        if(!rPackage) return;

        //--Scan across the requirements. If the player doesn't have all of them, fail.
        ShopUnlockPack *rUnlockPack = (ShopUnlockPack *)rPackage->mUnlockRequirements->PushIterator();
        while(rUnlockPack)
        {
            //--Get how many we have.
            int tItemCount = rInventory->GetCountOf(rUnlockPack->mItemName);

            //--Not enough: Fail.
            if(tItemCount < rUnlockPack->mNumberNeeded)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                rPackage->mUnlockRequirements->PopIterator();
                return;
            }

            rUnlockPack = (ShopUnlockPack *)rPackage->mUnlockRequirements->AutoIterate();
        }

        //--If we got this far, it's valid. Bring up the confirmation box.
        mIsUnlockingItem = true;
        mUnlockConfirmTimer = 0;
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        HandleExit();
        return;
    }
}

///===================================== Segment Rendering ========================================
void MonoUIVendor::RenderLftUnlock(float pVisAlpha)
{
    AdvUIVendor::RenderLftUnlock(pVisAlpha);
}
void MonoUIVendor::RenderTopUnlock(float pVisAlpha)
{
    AdvUIVendor::RenderTopUnlock(pVisAlpha);
}
void MonoUIVendor::RenderRgtUnlock(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    //--Comparison Block
    if(!mComparisonShowMaterials)
    {
        RenderUnlockProperties(cColorAlpha);
    }
    else
    {
        RenderMaterials(cColorAlpha);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderBotUnlock(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Category Selection]
    //--If there is more than zero categories, render the category selection boxes.
    if(mCategoryList->GetListSize() > 0)
    {
        ///--[Constants]
        float cTabW = 43.0f;

        ///--[Icons]
        //--Render icons across the top.
        int i = 0;
        ShopCategoryPack *rCategory = (ShopCategoryPack *)mCategoryList->PushIterator();
        while(rCategory)
        {
            //--Tab, selected.
            if(mCategoryCursor == i)
            {
                MonoImages.rTab_UnlockActive->Draw(cTabW * i, 0.0f);
            }
            //--Tab, unselected.
            else
            {
                MonoImages.rTab_Unlock->Draw(cTabW * i, 0.0f);
            }

            //--If the icon exists, render it.
            if(rCategory->rIcon)
            {
                float cRenderX = MonoImages.rTab_Unlock->GetXOffset() + 4 + (cTabW * i);
                float cRenderY = MonoImages.rTab_Unlock->GetYOffset() + 4;
                rCategory->rIcon->Draw(cRenderX, cRenderY);
            }

            //--Next.
            i ++;
            rCategory = (ShopCategoryPack *)mCategoryList->AutoIterate();
        }
    }

    //--Inventory Listing.
    AdvImages.rFrame_Unlock->Draw();

    //--Titles
    SetColor("Title", cColorAlpha);
    AdvImages.rFont_Heading->DrawText(cxMono_UnlHdgNameX,      cxMono_UnlHdgY, 0, 1.0f, xrStringData->str.mTextName);
    AdvImages.rFont_Heading->DrawText(cxMono_UnlHdgMaterialsX, cxMono_UnlHdgY, 0, 1.0f, xrStringData->str.mTextMaterials);
    SetColor("Clear", cColorAlpha);

    ///--[List Resolve]
    //--Resolve which list to use. If no categories are in place, use the mShopInventory list.
    StarLinkedList *rUseList = GetConsideredCategory();

    ///--[Normal Rendering]
    if(rUseList->GetListSize() > 0)
    {
        //--Backings. Rendered apart from the entries, allowing the cursor to be over the backing and under
        //  the text entries.
        float cEntrySpcH = 56.0f;
        int tBackingsToRender = rUseList->GetListSize();
        if(tBackingsToRender > cScrollPage) tBackingsToRender = cScrollPage;
        for(int i = 0; i < tBackingsToRender; i ++)
        {
            //--Skip backing right after the cursor.
            if(i == mCursor - mSkip + 1) continue;

            //--Render.
            MonoImages.rOverlay_Unlock->Draw(0.0f, i * cEntrySpcH);
        }

        //--Cursor.
        MonoImages.rOverlay_Selected->Draw(0.0f, mHighlightPos.mYCur - MonoImages.rOverlay_Selected->GetYOffset());

        //--Entries
        RenderUnlockListing(mSkip, cxMono_UnlScrollPage, cColorAlpha);
    }
    ///--[Zero Entries]
    else
    {
        AdvImages.rFont_Mainline->DrawText(345.0f, 232.0f, 0, 1.0f, xrStringData->str.mTextNothingUnlock);
    }

    //--Scrollbar.
    if(rUseList->GetListSize() > cScrollPage)
    {
        StandardRenderScrollbar(mSkip, cScrollPage, rUseList->GetListSize() - cScrollPage, 239.0f, 445.0f, false, AdvImages.rScrollbar_Scroller, MonoImages.rScrollbar_Unlock);
    }

    ///--[Help Strings]
    //--Help Strings, left side.
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    if(mShowSwapArrows) mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--Help Strings in materials mode:
    if(mComparisonShowMaterials)
    {
        if(mMaterialsUsePages) mToggleMaterialsPageString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 2.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
        mToggleCompareMaterialsString->                    DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    }
    //--Help Strings in properties mode:
    else
    {
        if(mCanSwapCompareSlot) mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 3.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
        mToggleRelativeString->                       DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 2.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
        mToggleCompareMaterialsString->               DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderUnalignedUnlock(float pVisAlpha)
{
    AdvUIVendor::RenderUnalignedUnlock(pVisAlpha);
}
void MonoUIVendor::RenderMaterials(float pColorAlpha)
{
    ///--[ =========== Documentation ========== ]
    //--Instead of rendering the comparison properties on the right side, instead shows a list of
    //  all materials that currently have aliases, and how many the player has. Also shows the
    //  number to be spent on the current transaction if applicable.

    ///--[Comparison Info]
    //--Resolve entry. Note that the shop package doesn't actually need to exist to begin rendering,
    //  it is only used to show the costs of the highlighted item.
    ShopInventoryPack *rShopPackage = GetConsideredItemPack();

    ///--[Backing]
    MonoImages.rFrame_Materials->Draw();

    ///--[Setup]
    //--Fast-access pointers.
    //AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Setup variables.
    float cTextY =  202.0f;
    float cTextH =   36.0f;
    float cNameX = 1080.0f;
    float cNeedR = cNameX + 150.0f;
    float cQuanR = cNameX + 228.0f;

    ///--[ ======= Render All Materials ======= ]
    //--Renders a list of all materials, not using pages.
    if(!mMaterialsUsePages)
    {
        ///--[Header]
        //--Title of materials area.
        SetColor("Heading", pColorAlpha);
        AdvImages.rFont_Heading->DrawText(1200.0f, 137.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextMaterials);
        SetColor("Clear", pColorAlpha);

        ///--[Iteration]
        //--Begin iterating across the material names.
        int i = 0;
        const char *rShortname = (const char *)mMaterialsList->PushIterator();
        while(rShortname)
        {
            //--Variables.
            const char *rItemName = mMaterialsList->GetIteratorName();
            float cYPos = cTextY + (cTextH * i);

            //--Call routine.
            RenderMaterial(rItemName, rShortname, rShopPackage, cNameX, cYPos, cQuanR, cNeedR, pColorAlpha);

            //--Next.
            i ++;
            rShortname = (const char *)mMaterialsList->AutoIterate();
        }
    }
    ///--[ ====== Render Materials Page ======= ]
    //--In cases where there are lots of materials, they can be organized into pages. Each page is a
    //  series of strings representing item names. The list mMaterialsPages stores ShopMaterialsPage's.
    else
    {
        //--Resolve the page in use.
        ShopMaterialsPage *rCurrentPage = (ShopMaterialsPage *)mMaterialsPages->GetElementBySlot(mMaterialsPage);
        if(!rCurrentPage) return;

        //--Title of materials area.
        SetColor("Heading", pColorAlpha);
        AdvImages.rFont_Heading->DrawText(1200.0f, 137.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mMaterialsPages->GetNameOfElementBySlot(mMaterialsPage));
        SetColor("Clear", pColorAlpha);

        //--If there is more than one materials page, put these scroll indicators on.
        if(mMaterialsPages->GetListSize() > 1)
        {
            MonoImages.rOverlay_ArrowMaterialLft->Draw();
            MonoImages.rOverlay_ArrowMaterialRgt->Draw();
        }

        //--Begin iterating across the material names.
        int i = 0;
        const char *rShortname = (const char *)rCurrentPage->mMaterials->PushIterator();
        while(rShortname)
        {
            //--Variables.
            const char *rItemName = rCurrentPage->mMaterials->GetIteratorName();
            float cYPos = cTextY + (cTextH * i);

            //--Call routine.
            RenderMaterial(rItemName, rShortname, rShopPackage, cNameX, cYPos, cQuanR, cNeedR, pColorAlpha);

            //--Next.
            i ++;
            rShortname = (const char *)rCurrentPage->mMaterials->AutoIterate();
        }

        //--Page number.
        SetColor("Paragraph", pColorAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(1200.0f, 684.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%i/%i", mMaterialsPage+1, mMaterialsPages->GetListSize());
    }

    //--Clean.
    SetColor("Clear", pColorAlpha);
}
void MonoUIVendor::RenderUnlockListing(int pSkip, int pMaxPerPage, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the currently visible entries on the shop's buyback listing by calling
    //  RenderUnlockPack() in an organized fashion.
    //--Same as the base version, but modified to handle showing the ingredients in-line.
    int i = 0;
    int tRenders = 0;
    int tYRender = 0;
    StarLinkedList *rUseList = GetConsideredCategory();

    ///--[Iterate]
    //--Render entries.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)rUseList->PushIterator();
    while(rPackage)
    {
        //--Beneath the skip, don't render.
        if(i < pSkip)
        {
            i ++;
            rPackage = (ShopInventoryPack *)rUseList->AutoIterate();
            continue;
        }

        //--If this is the highlighted entry, modify the scroll timer. It is otherwise zero.
        float tScrollPercent = 0.0f;
        if(i == mCursor) tScrollPercent = mUnlockScrollTimer / cxAdvUnlockCycleTicks;

        //--Subroutine.
        RenderUnlockPack(rPackage, tYRender, (i % 2 == 1), pColorAlpha, tScrollPercent);
        tYRender ++;

        //--If this was the cursor, add one to the render count.
        if(i == mCursor)
        {
            RenderUnlockMaterials(rPackage, tYRender, pColorAlpha);
            tYRender ++;
        }

        //--If we hit the render cap, stop. Note that the render cap is lower because one slot is used for materials.
        //  The tYRender will exceed the render value.
        tRenders ++;
        if(tRenders >= pMaxPerPage)
        {
            rUseList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rPackage = (ShopInventoryPack *)rUseList->AutoIterate();
    }
}
void MonoUIVendor::RenderUnlockProperties(float pVisAlpha)
{
    ///--[Documentation]
    //--Same as the buy properties but uses different positions to account for Unlock being a larger frame.
    AdventureItem *rComparisonItem = GetConsideredItem();
    if(!rComparisonItem || mCompareCharacterCur == -1) return;

    ///--[ =================== Common Rendering =================== ]
    ///--[Backing]
    MonoImages.rFrame_PropUnlock->Draw();

    ///--[Header]
    //--Title of Stats Area. Renders even if there is no item for properties.
    SetColor("Title", pVisAlpha);
    AdvImages.rFont_Heading->DrawText(1188.0f, 170.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextProperties);
    SetColor("Clear", pVisAlpha);

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);

    //--Setup variables.
    float cIconX         = 1050.0f;
    float cIconY         =  282.0f;
    float cIconH         =   36.0f;
    float cPropertyLeftX = cIconX         +  35.0f;
    float cPropertyBaseX = cIconX         +  81.0f;
    float cPropertyBaseR = cPropertyBaseX +  47.0f;
    float cDamageX       = cIconX         +  81.0f; //Resistance is not rendered in Monoceros so damage overlays it.
    float cDamageR       = cDamageX       +  80.0f;
    float cPropertyBaseY = cIconY         -   4.0f;

    //--Alt-weapon. If the comparison slot is a weapon-alt, then the stats in the comparison window need to subtract
    //  the main weapon and add in the alt-weapon, as if it was the main weapon.
    EquipmentSlotPack *rPackage = rActiveEntity->GetEquipmentSlotPackageS(mCompareSlot);
    if(!rPackage) return;

    ///--[ ====================== Gem Slots ======================= ]
    //--Renders above the property listing since it's not in the statistic index.
    MonoImages.rOverlay_GemSlotIcon->Draw(cIconX, cIconY + (cIconH * -1));
    int tOldGemSlots = 0;
    int tNewGemSlots = 0;

    //--Get old value.
    AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
    if(rCurrentItem) tOldGemSlots = rCurrentItem->GetGemSlots();

    //--Get new value.
    tNewGemSlots = rComparisonItem->GetGemSlots();

    ///--[Render Relative]
    //--Only show the difference, positive or negative, and not the actual value. There is no base value for
    //  an entity to have in terms of gem slots.

    //--Value is lower:
    if(tNewGemSlots < tOldGemSlots)
    {
        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pVisAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * -1), 0, 1.0f, "%i", (tNewGemSlots - tOldGemSlots));
    }
    //--Value is higher:
    else if(tNewGemSlots > tOldGemSlots)
    {
        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pVisAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * -1), 0, 1.0f, "%+i", (tNewGemSlots - tOldGemSlots));
    }

    //--Clean.
    SetColor("Clear", pVisAlpha);

    ///--[ ============== Iterate Across Properties =============== ]
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        ///--[Basics]
        //--Icon.
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Statistic / Resistance.
        int tEntityValue = rActiveEntity->GetStatistic(mStatisticIndexes[i]);
        if(mShowRelativeValuesOnly) tEntityValue = 0;

        //--Damage Bonus. May not always be used.
        int tEntityBonusTags = rActiveEntity->GetTagCount(mBonusTagNames[i]);
        int tEntityMalusTags = rActiveEntity->GetTagCount(mMalusTagNames[i]);
        int tEntityDamageBonus = 100 + tEntityBonusTags - tEntityMalusTags;

        ///--[Alt Weapon Check]
        //--Check if this is an alt-weapon slot. If so, we need to modify the statistics.
        //  Note: Alternate weapons are not used in Monoceros, the code is only here for possible mods.
        if(rPackage->mIsWeaponAlternate)
        {
            //--Get the weapon in the slot.
            int tWeaponSlot = rActiveEntity->GetSlotForWeaponDamage();
            EquipmentSlotPack *rWeaponPackage = rActiveEntity->GetEquipmentSlotPackageI(tWeaponSlot);

            //--If there's a weapon in the main slot:
            if(rWeaponPackage->mEquippedItem)
            {
                //--Statistic / Resistance
                tEntityValue = tEntityValue - rWeaponPackage->mEquippedItem->GetStatisticNoGem(mStatisticIndexes[i]);

                //--Damage Bonus
                int tItemBonusTags = rWeaponPackage->mEquippedItem->GetTagCount(mBonusTagNames[i]);
                int tItemMalusTags = rWeaponPackage->mEquippedItem->GetTagCount(mMalusTagNames[i]);
                tEntityDamageBonus = tEntityDamageBonus - tItemBonusTags + tItemMalusTags;
            }

            //--Add the value of the alternate weapon. This will give the player the stats they'd have if they swapped in battle.
            AdventureItem *rReplaceWeapon = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            if(rReplaceWeapon)
            {
                //--Statistic / Resistance
                tEntityValue = tEntityValue + rReplaceWeapon->GetStatisticNoGem(mStatisticIndexes[i]);

                //--Damage Bonus
                int tItemBonusTags = rReplaceWeapon->GetTagCount(mBonusTagNames[i]);
                int tItemMalusTags = rReplaceWeapon->GetTagCount(mMalusTagNames[i]);
                tEntityDamageBonus = tEntityDamageBonus + tItemBonusTags - tItemMalusTags;
            }
        }

        ///--[ ==== Stat Comparison - Base Property ===== ]
        //--This is a simple property with only one value to render.
        if(i < MONOUIVEN_RESISTANCE_BEGIN)
        {
            ///--[Normal]
            //--Setup.
            int tOldStatValue = 0;
            int tNewStatValue = 0;

            //--Stat value of the equipped item.
            AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            if(rCurrentItem) tOldStatValue = rCurrentItem->GetStatisticNoGem(mStatisticIndexes[i]);

            //--Stat value of the replacement.
            tNewStatValue = rComparisonItem->GetStatisticNoGem(mStatisticIndexes[i]);

            ///--[Render Relative]
            //--Only show the difference, positive or negative, and not the actual value.
            if(mShowRelativeValuesOnly)
            {
                //--Value is lower:
                if(tNewStatValue < tOldStatValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pVisAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * i), 0, 1.0f, "%i", (tNewStatValue - tOldStatValue));
                }
                //--Value is higher:
                else if(tNewStatValue > tOldStatValue)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pVisAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * i), 0, 1.0f, "%+i", (tNewStatValue - tOldStatValue));
                }
                //--Value is equal. Nothing renders.
                else
                {

                }
            }
            ///--[Render Absolute]
            //--Shows the character's stat value and then what it would change to.
            else
            {
                //--Base value.
                AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue);

                //--If the value in this slot is different from the old value, then display that.
                if(tOldStatValue != tNewStatValue)
                {
                    //--Value is lower:
                    if(tNewStatValue < tOldStatValue)
                    {
                        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pVisAlpha);
                    }
                    //--Higher:
                    else
                    {
                        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pVisAlpha);
                    }

                    //--Render.
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue + (tNewStatValue - tOldStatValue));
                }
            }

            //--Clean.
            SetColor("Clear", pVisAlpha);
        }
        ///--[ = Stat Comparison - Resistance and Damage = ]
        //--Resistance and Damage Bonus are side by side.
        else
        {
            ///--[Get Values]
            //--Equipped item values.
            AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            int tOldResistance = 0;
            int tOldBonusTags  = 0;
            int tOldMalusTags  = 0;
            if(rCurrentItem)
            {
                tOldResistance = rCurrentItem->GetStatisticNoGem(mStatisticIndexes[i]);
                tOldBonusTags  = rCurrentItem->GetTagCount(mBonusTagNames[i]);
                tOldMalusTags  = rCurrentItem->GetTagCount(mMalusTagNames[i]);
            }

            //--Replacement values.
            int tNewResistance = rComparisonItem->GetStatisticNoGem(mStatisticIndexes[i]);
            int tNewBonusTags  = rComparisonItem->GetTagCount(mBonusTagNames[i]);
            int tNewMalusTags  = rComparisonItem->GetTagCount(mMalusTagNames[i]);

            //--Compute Damage
            int tOldDamageValue = 100 + tOldBonusTags - tOldMalusTags;
            if(tOldDamageValue < 0) tOldDamageValue = 0;
            int tNewDamageValue = 100 + tNewBonusTags - tNewMalusTags;
            if(tNewDamageValue < 0) tNewDamageValue = 0;

            ///--[Absolute Values]
            //--Only show the difference, positive or negative, and not the actual value.
            if(mShowRelativeValuesOnly)
            {
                ///--[Resistance]
                //--Value is lower:
                if(tNewResistance < tOldResistance)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pVisAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * i), 0, 1.0f, "%i", (tNewResistance - tOldResistance));
                }
                //--Higher:
                else if(tNewResistance > tOldResistance)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pVisAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * i), 0, 1.0f, "%+i", (tNewResistance - tOldResistance));
                }

                ///--[Damage Bonus]
                //--Value is lower:
                if(tNewDamageValue < tOldDamageValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pVisAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * i), 0, 1.0f, "%i%%", (tNewDamageValue - tOldDamageValue));
                }
                //--Higher:
                else if(tNewDamageValue > tOldDamageValue)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pVisAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyLeftX, cPropertyBaseY + (cIconH * i), 0, 1.0f, "%+i%%", (tNewDamageValue - tOldDamageValue));
                }
            }
            ///--[Relative Values]
            //--Show the entity's value and then what it would change to.
            else
            {
                ///--[Resistance]
                //--Resistance scores never render in Monoceros, only damage bonuses.

                ///--[Damage Bonus]
                //--Base value.
                AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tEntityDamageBonus);

                //--If the value in this slot is different from the old value, then display that.
                if(tOldDamageValue != tNewDamageValue)
                {
                    //--Value is lower:
                    if(tNewDamageValue < tOldDamageValue)
                    {
                        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pVisAlpha);
                    }
                    //--Higher:
                    else
                    {
                        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pVisAlpha);
                    }

                    //--Render.
                    AdvImages.rFont_Statistic->DrawTextArgs(cDamageX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                    AdvImages.rFont_Statistic->DrawTextArgs(cDamageR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tEntityDamageBonus + (tNewDamageValue - tOldDamageValue));
                }
            }

            //--Clean.
            SetColor("Clear", pVisAlpha);
        }
    }

    ///--[Comparison Item]
    //--Show what item is being compared to.
    if(rCurrentItem)
    {
        //--Base.
        AdvImages.rFont_Statistic->DrawText(1104.0f, 642.0f, 0, 1.0f, xrStringData->str.mTextComparingTo);

        //--Icon.
        StarBitmap *rItemIcon = rCurrentItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(1114.0f, 665.0f);

        //--Name.
        AdvImages.rFont_Statistic->DrawText(1137.0f, 664.0f, 0, 1.0f, rCurrentItem->GetDisplayName());
    }
    else
    {
        //--Base.
        AdvImages.rFont_Statistic->DrawText(1104.0f, 642.0f, 0, 1.0f, xrStringData->str.mTextComparingTo);
        SetColor("Greyed", pVisAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(1137.0f, 664.0f, 0, 1.0f, "[%s]", mCompareSlot);
        SetColor("Clear", pVisAlpha);
    }
}
void MonoUIVendor::RenderUnlockMaterials(ShopInventoryPack *pItemPack, int pSlot, float pAlpha)
{
    ///--[Documentation]
    //--Render the list of materials in a small font, meant to be placed right below the item's entry
    //  on the unlock list.
    if(!pItemPack || !pItemPack->mItem) return;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *trUnlockRequirements = pItemPack->mUnlockRequirements;

    ///--[Setup]
    //--Rendering positions.
    float tRenderX = cxMono_IngredientL;
    float tRenderY = cxMono_UnlockT + (cxMono_UnlockH * pSlot) + cxMono_IngredientT;

    ///--[Rendering]
    ShopUnlockPack *rPackage = (ShopUnlockPack *)trUnlockRequirements->PushIterator();
    while(rPackage)
    {
        //--We have enough, switch to green.
        int tInInventory = rInventory->GetCountOf(rPackage->mItemName);
        if(tInInventory >= rPackage->mNumberNeeded)
        {
            StarlightColor::SetMixer(0.1f, 1.0f, 0.2f, pAlpha);
        }

        //--Render the name and counts.
        MonoImages.rFont_InlineMaterials->DrawTextFixedArgs(tRenderX, tRenderY, cxMono_IngredientW, 0, "%s (%i/%i)", rPackage->mItemDisplayName, tInInventory, rPackage->mNumberNeeded);

        //--If currently on the left position, move to the right.
        if(tRenderX == cxMono_IngredientL)
        {
            tRenderX = cxMono_IngredientM;
        }
        else if(tRenderX == cxMono_IngredientM)
        {
            tRenderX = cxMono_IngredientR;
        }
        //--If currently on the right position, move to the left, and down.
        else
        {
            tRenderX = cxMono_IngredientL;
            tRenderY = tRenderY + cxMono_IngredientH;
        }

        //--Next.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        rPackage = (ShopUnlockPack *)trUnlockRequirements->AutoIterate();
    }
}
