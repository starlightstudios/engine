///==================================== StarObjectPrototype =======================================
//--Represents a script object prototype. These are used to generate scripts for things like jobs,
//  abilities, characters, and so on. This is typically done on the title screen and requires a listing
//  of prototypes to exist, so it can be used by modders.
//--Each prototype contains a list of fields, which are strings, floats, or ints. It also contains an
//  execution script which is called with the prototype as the active object during execution. The
//  script then takes the information in the object, entered by the user, and generates a script using
//  it or barks a warning if a field was invalid.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"

///===================================== Local Structures =========================================
///--[StarObjectEnum]
//--Stores an integer/string pair for use in displaying enumerated strings.
typedef struct StarObjectEnum
{
    //--System
    int mEnumVal;
    char *mString;

    //--Methods
    void Initialize();
    void SetString(const char *pString);
    static void DeallocateThis(void *pPtr);
}StarObjectEnum;

///--[StarObjectField]
//--Stores a name/value pairing where the value can be Int/Float/String, along with handlers for
//  each type. Allows description storage and enumerations (for Int types only).
typedef struct StarObjectField
{
    //--System
    char *mName;
    int mType;
    FlexVal mNumeric;
    char *mAlpha;

    //--Description
    int mDescriptionLinesTotal;
    char **mDescriptionLines;

    //--Enumerations
    bool mLockToEnumerations;
    int mEnumerationsTotal;
    StarObjectEnum *mEnumerations;

    //--Functions
    void Initialize();
    void AllocateDescriptionLines(int pTotal);
    void SetDescriptionLine(int pSlot, const char *pDescription);
    void AllocateEnumerations(int pTotal);
    void SetEnumeration(int pSlot, int pValue, const char *pDescription);
    void CheckAgainstEnumerations();
    static void DeleteThis(void *pPtr);
}StarObjectField;

///===================================== Local Definitions ========================================
#define SOP_CREATE 0
#define SOP_EXECUTE 1

///========================================== Classes =============================================
class StarObjectPrototype
{
    private:
    ///--[System]
    char *mInternalName;
    char *mExecScript;
    StarLinkedList *mFieldListing; //StarObjectField *, master

    ///--[Description]
    int mDescriptionLinesTotal;
    char **mDescriptionLines;

    protected:

    public:
    //--System
    StarObjectPrototype();
    ~StarObjectPrototype();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    const char *GetName();
    const char *GetExecPath();
    int GetTotalFields();
    StarObjectField *GetFieldI(int pSlot);
    StarObjectField *GetFieldS(const char *pName);
    int GetIntegerField(const char *pName);
    float GetFloatField(const char *pName);
    const char *GetStringField(const char *pName);
    int GetTotalDescriptionLines();
    const char *GetDescriptionLine(int pSlot);

    //--Manipulators
    void SetInternalName(const char *pName);
    void SetExecScript(const char *pPath);
    void AllocateDescriptionLines(int pTotal);
    void SetDescriptionLine(int pLine, const char *pDescription);
    void AddIntegerField(const char *pName, int pDefaultValue);
    void AddFloatField(const char *pName, float pDefaultValue);
    void AddStringField(const char *pName, const char *pDefaultValue);
    void SetIntegerField(const char *pName, int pValue);
    void SetFloatField(const char *pName, float pValue);
    void SetStringField(const char *pName, const char *pValue);
    void AllocateEnumerationsInField(const char *pName, int pEnumerations);
    void SetEnumerationInField(const char *pName, int pSlot, int pValue, const char *pString);
    void SetEnumerationLocking(const char *pName, bool pIsLocked);

    //--Core Methods
    StarObjectField *CheckExistence(const char *pFunction, const char *pFieldName, int pFieldType);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    StarObjectField *GetField(const char *pName);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SOP_GetProperty(lua_State *L);
int Hook_SOP_SetProperty(lua_State *L);
