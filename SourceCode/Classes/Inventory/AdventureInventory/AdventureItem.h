///======================================= AdventureItem ==========================================
//--Represents an Item in Adventure Mode, which may be one of a consumable, key item, equipment, or overworld trap.
//  Some items can be used in multiple roles.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "AdvCombatDefStruct.h"

///===================================== Local Structures =========================================
///--[AdvItem_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define ADVITEM_STRINGS_TOTAL 21
#define ADVITEM_STRINGS_TYPE_START 8
class AdvItem_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[ADVITEM_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mCraftItemType;
            char *mNoTypeSet;
            char *mType;
            char *mIsKeyItem;
            char *mValueless;
            char *mValue;
            char *mGemSlots;
            char *mDamageType;
            char *mType_Protection;
            char *mType_Slashing;
            char *mType_Striking;
            char *mType_Piercing;
            char *mType_Flaming;
            char *mType_Freezing;
            char *mType_Shocking;
            char *mType_Crusading;
            char *mType_Obscuring;
            char *mType_Bleeding;
            char *mType_Poisoning;
            char *mType_Corroding;
            char *mType_Terrifying;
        }str;
    };

    //--Functions
    AdvItem_Strings();
    virtual ~AdvItem_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///===================================== Local Definitions ========================================
//--Upgrade item types
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Gem Colors
#define ADITEM_GEMCOLOR_GREY   0x01
#define ADITEM_GEMCOLOR_RED    0x02
#define ADITEM_GEMCOLOR_BLUE   0x04
#define ADITEM_GEMCOLOR_ORANGE 0x08
#define ADITEM_GEMCOLOR_VIOLET 0x10
#define ADITEM_GEMCOLOR_YELLOW 0x20
#define ADITEM_GEMCOLOR_ALL    0xFF
#define ADITEM_MAX_GEMS 6
#define ADITEM_MAX_GEM_RANK 5

//--Damage Type Equipment Categories
#define ADITEM_DAMAGETYPE_BASE 0
#define ADITEM_DAMAGETYPE_OVERRIDE 1
#define ADITEM_DAMAGETYPE_BONUS 2
#define ADITEM_DAMAGETYPE_TOTAL 3

//--Description
#define ADITEM_DESCRIPTION_MAX 7

///========================================== Classes =============================================
class AdventureItem : public RootObject
{
    private:
    //--System
    bool mIsAdamantite;
    bool mIsStackable;
    int mStackSize;
    uint32_t mItemUniqueID;
    char *mLocalName;
    char *mDisplayName;
    char *mDescription;
    char *mDescriptionType;
    StarlightString *mAdvancedDescription[ADITEM_DESCRIPTION_MAX];
    bool mHasOverrideQuality;
    float mOverrideQuality;

    //--Value
    int mValue;

    //--Consumable
    bool mIsConsumable;

    //--Key Item
    bool mIsUnique;
    bool mIsKeyItem;

    //--Equipment
    bool mIsEquipment;
    char *mEquipmentAttackSound;
    char *mEquipmentCriticalSound;
    char *mEquipmentAttackAnimation;
    StarLinkedList *mEquipSlotList; //dummyptr
    StarLinkedList *mEquipEntityList; //dummyptr
    CombatStatistics mEquipmentBonus;
    CombatStatistics mFinalEquipBonus; //After Gems
    DamageTypes mEquipmentDamageTypes[ADITEM_DAMAGETYPE_TOTAL];

    //--Gem Slots
    int mGemSlotsTotal;
    char *mOriginalGemName;
    StarBitmap *rOriginalGemIcon;
    char *mOriginalGemDescription;
    AdventureItem *mGemSlots[ADITEM_MAX_GEMS];

    //--Gems
    bool mIsGem;
    uint8_t mGemColorCode;

    //--Combat Items
    char *mAbilityPath;

    //--Tags
    StarLinkedList *mTagList; //int *, master

    //--Rendering
    StarBitmap *rIconImg;

    //--Static Listings
    static bool xHasBuiltStatIcoList;
    static StarBitmap *xStaticStatIcoList[STATS_TOTAL];
    static bool xHasBuiltDamageNameList;

    protected:

    public:
    //--System
    AdventureItem();
    virtual ~AdventureItem();
    static void InitializeStatIcoList();

    //--Public Variables
    bool mGemErrorFlag;
    AdventureItem *rGemParent;
    AdvCombatEntity *rEquippingEntity;
    static int xItemUniqueIDCounter;

    //--Comparison Lines
    static char *xComparisonItemA;
    static char *xComparisonItemB;
    static int xComparisonLinesTotal;
    static char **xComparisonLines;
    static AdvItem_Strings *xrStringData;

    //--Property Queries
    bool IsAdamantite();
    bool IsConsumable();
    bool IsStackable();
    int GetStackSize();
    const char *GetAbilityPath();
    uint32_t GetItemUniqueID();
    const char *GetName();
    const char *GetDisplayName();
    const char *GetDescription();
    const char *GetDescriptionType();
    int GetValue();
    int8_t GetUpgradeCode(int pSlot);
    int GetSlottedGems();
    int GetGemSlots();
    bool IsUnique();
    bool IsKeyItem();
    StarBitmap *GetIconImage();
    int GetTagCount(const char *pTag);
    StarlightString *GetAdvancedDescription(int pSlot);

    //--Manipulators
    void SetAdamantite();
    void SetConsumable();
    void SetStackable(bool pIsStackable);
    void SetStackSize(int pSize);
    void SetAbilityPath(const char *pPath);
    void OverrideID(uint32_t pID);
    void SetName(const char *pName);
    void SetDisplayName(const char *pName, bool pDisallowTranslation);
    void SetDescription(const char *pDescription);
    void SetType(const char *pType);
    void SetValue(int pValue);
    void SetUnique(bool pFlag);
    void SetKeyItem(bool pFlag);
    void SetConsumable(bool pFlag);
    void SetEquipmentFlag(bool pFlag);
    void SetIconImageS(const char *pPath);
    void SetIconImageP(StarBitmap *pImage);
    AdventureItem *PlaceGemInSlot(int pSlot, AdventureItem *pGem);
    AdventureItem *RemoveGemFromSlot(int pSlot);
    void SetOverrideQuality(float pQuality);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);
    static void SetStaticImage(int pSlot, const char *pPath);

    //--Equipment Items
    bool IsEquipment();
    bool IsEquippableBy(const char *pName);
    bool IsEquippableIn(const char *pSlot);
    bool MatchesEquippable(AdventureItem *pItem);
    const char *GetEquipmentAttackAnimation();
    const char *GetEquipmentAttackSound();
    const char *GetEquipmentCriticalSound();
    CombatStatistics *GetEquipStatistics();
    CombatStatistics *GetFinalStatistics();
    int GetStatistic(int pSlot);
    int GetStatisticNoGem(int pSlot);
    float GetDamageType(int pCategory, int pTypeIndex);
    void AddEquippableCharacter(const char *pName);
    void AddEquippableSlot(const char *pSlot);
    void SetEquipmentAttackAnimation(const char *pAnimation);
    void SetEquipmentAttackSound(const char *pSound);
    void SetEquipmentCriticalSound(const char *pSound);
    void SetGemCap(int pGemCap);
    void SetStatistic(int pSlot, int pValue);
    void SetDamageType(int pCategory, int pTypeIndex, float pValue);
    void AppendToStatsPack(CombatStatistics &sPackage);
    float ComputeQuality();
    void ComputeStatistics();

    //--Combat Items
    AdvCombatAbility *GetCombatAbility();
    void RegisterAbility(AdvCombatAbility *pAbility);

    //--Gems
    bool IsGem();
    const char *GetGemBaseName();
    StarBitmap *GetGemBaseImage();
    const char *GetGemBaseDescription();
    uint8_t GetGemColors();
    bool CanBeMerged(AdventureItem *pGem);
    int GetRank();
    void SetIsGem(uint8_t pGemColors);
    bool MergeWithGem(AdventureItem *pGem);
    void AutogenerateGemDescription();

    //--Core Methods
    AdventureItem *Clone();
    void ProcessDescription();
    void HandleDescriptionTag(const char *pString, StarAutoBuffer *pBuffer, StarLinkedList *pImageListing);
    char *GetDisplayNameOfItemWithModCount();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    AdventureItem *GetGemInSlot(int pSlot);

    //--Static Functions
    static StarBitmap *GetStatIconBySlot(int pSlot);
    static const char *GetDamageStringBySlot(int pSlot);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdItem_GetProperty(lua_State *L);
int Hook_AdItem_SetProperty(lua_State *L);
