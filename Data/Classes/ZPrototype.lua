--To-do
--Replace [ClassSample] with the name of the class
--Balance the headers
--Write a description
--Delete these instructions

-- |[ ================================ [ClassSample] Class File ================================ ]|
--Class file for [ClassSample]. Call this once at initialization.

--Place a description here.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
[ClassSample] = {}
[ClassSample].__index = [ClassSample] 

-- |[ ============ Class Methods ============= ]|
--System
function [ClassSample]:new() end
    
--Functions
--function [ClassSample]:fnDoAThing(pzaListing) end

-- |[ ========== Class Constructor =========== ]|
function [ClassSample]:new()
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, [ClassSample])
    
    -- |[ ======== Members ========= ]|
    --Reset local lists so they don't use the global class listings.
    zObject.iaRandomNumbers = {}

    -- |[ ======= Finish Up ======== ]|
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "[ClassSample]Functions.lua")
