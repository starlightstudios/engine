//--Base
//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
//--Libraries
//--Managers

void RenderExpandableHeader(float pXCenter, float pY, float pNeededSize, float pLftDeadZone, float pRgtDeadZone, StarBitmap *pImage)
{
    ///--[Documentation]
    //--Given a fixed-height header meant to display text, renders the center block stretched to the required dimensions.
    //  The blocks on the left and right sides are unaffected.
    //--The resulting header is necessarily centered.
    if(!pImage) return;

    //--If the needed size comes in at less than the normal width of the image, just render it normally.
    if(pNeededSize < (pImage->GetWidthSafe() - pLftDeadZone - pRgtDeadZone))
    {
        pImage->Draw(pXCenter - (pImage->GetWidthSafe() * 0.50f), pY);
        return;
    }

    //--Setup.
    float cHr[4]; //Pixels
    float cVr[4]; //Pixels
    float cTH[4]; //Texels
    float cVH[4]; //Texels

    //--Texture sizes.
    float cTxW = pImage->GetWidthSafe();
    float cTxH = pImage->GetHeightSafe();

    //--Start renderer.
    pImage->Bind();
    glBegin(GL_QUADS);

    ///--[Vertical Properties]
    //--These don't change.
    cVr[0] = pY;
    cVr[1] = pY;
    cVr[2] = pY + cTxH;
    cVr[3] = pY + cTxH;
    cVH[0] = 1.0f;
    cVH[1] = 1.0f;
    cVH[2] = 0.0f;
    cVH[3] = 0.0f;

    ///--[Leftmost Block]
    if(pLftDeadZone > 0.0f)
    {
        //--Leftmost block in pixels.
        cHr[0] = pXCenter - (pNeededSize * 0.50f) - pLftDeadZone;
        cHr[1] = pXCenter - (pNeededSize * 0.50f);
        cHr[2] = pXCenter - (pNeededSize * 0.50f);
        cHr[3] = pXCenter - (pNeededSize * 0.50f) - pLftDeadZone;

        //--Leftmost block in texels.
        cTH[0] = 0.0f;
        cTH[1] = pLftDeadZone / cTxW;
        cTH[2] = pLftDeadZone / cTxW;
        cTH[3] = 0.0f;

        //--Render.
        glTexCoord2f(cTH[0], cVH[0]); glVertex2f(cHr[0], cVr[0]);
        glTexCoord2f(cTH[1], cVH[1]); glVertex2f(cHr[1], cVr[1]);
        glTexCoord2f(cTH[2], cVH[2]); glVertex2f(cHr[2], cVr[2]);
        glTexCoord2f(cTH[3], cVH[3]); glVertex2f(cHr[3], cVr[3]);
    }

    ///--[Central Block]
    //--This is the part that stretches.
    cHr[0] = pXCenter - (pNeededSize * 0.50f);
    cHr[1] = pXCenter + (pNeededSize * 0.50f);
    cHr[2] = pXCenter + (pNeededSize * 0.50f);
    cHr[3] = pXCenter - (pNeededSize * 0.50f);

    //--Block in texels.
    cTH[0] = pLftDeadZone / cTxW;
    cTH[1] = 1.0f - (pRgtDeadZone / cTxW);
    cTH[2] = 1.0f - (pRgtDeadZone / cTxW);
    cTH[3] = pLftDeadZone / cTxW;

    //--Render.
    glTexCoord2f(cTH[0], cVH[0]); glVertex2f(cHr[0], cVr[0]);
    glTexCoord2f(cTH[1], cVH[1]); glVertex2f(cHr[1], cVr[1]);
    glTexCoord2f(cTH[2], cVH[2]); glVertex2f(cHr[2], cVr[2]);
    glTexCoord2f(cTH[3], cVH[3]); glVertex2f(cHr[3], cVr[3]);

    ///--[Rightmost Block]
    if(pRgtDeadZone > 0.0f)
    {
        //--Rightmost block in pixels.
        cHr[0] = pXCenter + (pNeededSize * 0.50f);
        cHr[1] = pXCenter + (pNeededSize * 0.50f) + pRgtDeadZone;
        cHr[2] = pXCenter + (pNeededSize * 0.50f) + pRgtDeadZone;
        cHr[3] = pXCenter + (pNeededSize * 0.50f);

        //--Rightmost block in texels.
        cTH[0] = 1.0f - (pRgtDeadZone / cTxW);
        cTH[1] = 1.0f;
        cTH[2] = 1.0f;
        cTH[3] = 1.0f - (pRgtDeadZone / cTxW);

        //--Render.
        glTexCoord2f(cTH[0], cVH[0]); glVertex2f(cHr[0], cVr[0]);
        glTexCoord2f(cTH[1], cVH[1]); glVertex2f(cHr[1], cVr[1]);
        glTexCoord2f(cTH[2], cVH[2]); glVertex2f(cHr[2], cVr[2]);
        glTexCoord2f(cTH[3], cVH[3]); glVertex2f(cHr[3], cVr[3]);
    }

    ///--[Clean Up]
    //--End rendering.
    glEnd();
}
