//--Base
#include "StarCamera2D.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers

void StarCamera2D::SetPolygonMode(bool pFlag)
{
    //--Clears off the Polygon list and changes the flag as needed.
    mIs2DPolygonMode = pFlag;
    mOldPolygonFlag = pFlag;
    if(!pFlag) mPolygonList->ClearList();
}
void StarCamera2D::AddPolygon(float *pArray)
{
    //--Adds a new polygon from this array.  Takes ownership of the array.
    if(!pArray) return;

    mPolygonList->AddElement("X", pArray, &FreeThis);
}
bool StarCamera2D::IsPolyPositionValid(ThreeDimensionReal pPosition)
{
    //--Returns true if the provided position is fully within at least one polygon, false if no
    //  polygon encapsulates it.
    if(!mIs2DPolygonMode || !mPolygonList) return true;

    //--Scan all the polygons.  In order to be within a polygon, the four edge points must all be
    //  within the polygon.  We assume all polygons are regular, space-filling, right-handed polygons
    //  and if any of these conditions are false, behavior is undefined.
    float tOffset = 1.0f;
    float *rArray = (float *)mPolygonList->PushIterator();
    while(rArray)
    {
        if(IsPolyCollisionRightHand(pPosition.mLft+tOffset, pPosition.mTop        , rArray) && IsPolyCollisionRightHand(pPosition.mLft        , pPosition.mBot-tOffset, rArray) &&
           IsPolyCollisionRightHand(pPosition.mRgt-tOffset, pPosition.mTop        , rArray) && IsPolyCollisionRightHand(pPosition.mRgt-tOffset, pPosition.mBot-tOffset, rArray))
        {
            mPolygonList->PopIterator();
            return true;
        }
        rArray = (float *)mPolygonList->AutoIterate();
    }

    //--No polygon hits.
    return false;
}
