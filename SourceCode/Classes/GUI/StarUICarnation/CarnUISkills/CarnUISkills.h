///======================================= CarnUISkills ===========================================
//--Shows the player their skills and lets them click on the skills to use them outside battle if
//  they are flagged for it.

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
typedef struct CarnSkillEntry
{
    //--Members
    bool mIsUIUsable;
    int mSPCost;
    char *mDisplayName;
    char *mDescriptionRaw;
    StarLinkedList *mDescription;

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}CarnSkillEntry;

///===================================== Local Definitions ========================================
#define ABILITY_UI_CALL 2001

///========================================== Classes =============================================
class CarnUISkills : public CarnUICommon
{
    protected:
    ///--[Constants]
    //--Scrollbar.
    static const int cxCarnScrollBuf = 3;
    static const int cxCarnScrollPage = 12;

    //--Hitboxes.
    static const int cxHitbox_ScrollUp =  0;
    static const int cxHitbox_ScrollBd =  1;
    static const int cxHitbox_ScrollDn =  2;
    static const int cxHitbox_SkillBgn =  3;
    static const int cxHitbox_SkillEnd = 14;

    ///--[System]
    int mCharacterCursor;
    int mSkillCursor;

    ///--[Skill Listing]
    int mSkillSkip;
    StarLinkedList *mSkillList; //CarnSkillEntry *, master

    ///--[Description]
    StarLinkedList *mDescription; //char *, master

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Description;
        StarFont *rFont_Statistic;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rOverlay_HPFill;
        StarBitmap *rOverlay_MeterFrames;
        StarBitmap *rOverlay_SPFill;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rScrollbar_Static;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUISkills();
    virtual ~CarnUISkills();
    virtual void Construct();

    //--Public Variables
    static char *xCharacterBuildScript;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    void RegisterSkill(const char *pInternalName);
    void SetSkillProperties(const char *pInternalName, const char *pDisplayName, bool pIsUsable, int pSPCost, const char *pDescription);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void ChangeCharacterCursor(int pNewCharacter);
    void ActivateSkill(int pSkillCursor);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    static CarnUISkills *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnUISkills_SetProperty(lua_State *L);
