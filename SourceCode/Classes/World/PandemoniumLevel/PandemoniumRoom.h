//--[PandemoniumRoom]
//--An element in a PandemoniumLevel. Contains connectivity information and a fast-access
//  list for all the entities currently within it. Note that they are all reference pointers
//  and are double-sided. The entities are owned by the EntityManager.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--It's called a DicePackage because the dots rendering look like die. HA HA.
typedef struct
{
    float mX, mY, mR;
    void Set(float pX, float pY, float pR)
    {
        mX = pX;
        mY = pY;
        mR = pR;
    }
}DicePackage;

//--Holds a list of movement instructions necessary to reach the named room from the current
//  location. This should be compiled by the owning Level using its path listing.
typedef struct
{
    //--Permanent Variables
    char *mTargetName;
    int mListSize;
    int *mPathList; //--Contains NAV_ instructions from Definitions.h

    //--Construction Variables
    int mShortestPath;
    PandemoniumRoom *rAttachedRoom;

    //--Construction Functions
    void Reset()
    {
        mTargetName = NULL;
        mListSize = 0;
        mPathList = NULL;
        mShortestPath = -1;
        rAttachedRoom = NULL;
    }
    void CopyPathList(int pSize, int *pList)
    {
        //--Deallocate.
        free(mPathList);
        mPathList = NULL;
        mListSize = 0;
        if(pSize < 1) return;

        //--Allocate.
        mListSize = pSize;
        SetMemoryData(__FILE__, __LINE__);
        mPathList = (int *)starmemoryalloc(sizeof(int) * pSize);

        //--Copy.
        for(int i = 0; i < mListSize; i ++)
        {
            mPathList[i] = pList[i];
        }
    }

}PathInstructions;

//--[Local Definitions]
#define MAX_DIE 6

//--[Classes]
class PandemoniumRoom : public RootObject
{
    protected:
    //--System
    char *mLocalName;

    //--Position
    float mWorldX;
    float mWorldY;
    int mWorldZ;

    //--Connectivity
    uint32_t mConnectionRenderList;
    char *mConnectivityNames[NAV_TOTAL];
    PandemoniumRoom *rConnection[NAV_TOTAL];
    int mShortestPathSize;
    PathInstructions *mShortestPaths;

    //--Entities
    StarLinkedList *mEntityList;

    //--Zone Effects
    StarLinkedList *mEffectList;

    //--Inventory
    InventorySubClass *mLocalInventory;
    StarLinkedList *mCompressedInventory;

    //--Container Listing
    StarLinkedList *mContainerList;

    //--Rendering Positions (Static Constant)
    static bool xHasSetupRenderingPositions;
    static DicePackage **xDicePackages;

    //--Rendering
    StarBitmap *rUpArrow;
    StarBitmap *rDnArrow;
    StarBitmap *rUpDnArrow;

    protected:

    public:
    //--System
    PandemoniumRoom();
    ~PandemoniumRoom();

    //--Public Variables
    static bool xFirstPath;
    int mPathDistance;
    static bool xHasBuiltDirectionNames;
    static char xDirectionNames[NAV_TOTAL][32];
    static char xReverseNames[NAV_TOTAL][32];

    //--Sizing Constants
    static const float cSpacing;
    static const float cSquareOuter;
    static const float cSquareInner;
    static const float cLineWidth;

    //--Property Queries
    char *GetName();
    int GetWorldX();
    int GetWorldY();
    int GetWorldZ();
    Actor *IsPlayerPresent();
    bool IsPlayerAdjacent();
    PandemoniumRoom *GetConnection(int pFlag);
    PandemoniumRoom *GetConnection(const char *pDirectionName);
    int GetEntitiesPresentTotal();
    Actor *GetEntity(int pSlot);
    int GetItemsTotal();
    int GetCompressedInventorySize();
    InventoryPack *GetItemPack(int pSlot);
    InventoryItem *GetItem(int pSlot);
    InventoryItem *GetItem(const char *pName);
    int GetContainersTotal();
    bool IsOneItemPresent();
    bool IsOneWeaponPresent();
    bool IsEffectPresent(const char *pName);

    //--Manipulators
    void SetName(const char *pName);
    void SetPosition(int pX, int pY, int pZ);
    void SetConnectivity(int pDirection, const char *pName);
    void RegisterActor(Actor *pActor);
    void UnregisterActor(Actor *pActor);
    void RegisterItem(InventoryItem *pItem);
    void LiberateItem(InventoryItem *pItem);
    void RegisterContainer(WorldContainer *pContainer);
    void OpenContainer(int pSlot, const char *pOpenerName, bool pIsFirstPerson);
    void RegisterEffect(RootEffect *pEffect);

    //--Core Methods
    virtual void BuildConnectivity(int pRoomsTotal, PandemoniumRoom **pRoomList);
    static void SetupPathingNames();
    bool HasHostileEntity(uint8_t pTeamFlags);
    bool HasDarknessCloud();
    void ReconstituteInventory();

    //--Pathing
    virtual PathInstructions *GetPathTo(const char *pRoomTarget);
    void ClearPathing();
    void BuildPathing(int pListSize, PandemoniumRoom **pRoomList);
    virtual bool RecursivePather(PandemoniumRoom *pCurrentRoom, int pMovesSoFar, int *pMoveList);
    PathInstructions *GetPathingPackS(const char *pName);
    PathInstructions *GetPathingPackP(PandemoniumRoom *pRoom);

    private:
    //--Private Core Methods
    public:
    //--Update
    void UpdatePulse();
    void EffectPulse();
    virtual void RespondToActorExit(Actor *pActor);
    virtual void RespondToActorEntry(Actor *pActor, PandemoniumRoom *pPreviousRoom);

    //--File I/O
    //--Drawing
    virtual void Render();
    virtual void RenderBody();
    virtual void RenderReducedBody();
    virtual void RenderEntities(bool pOnlyRenderIcons);
    virtual void RenderConnections();
    static void RenderCircle(float pXCenter, float pYCenter, float pRadius, int pAccuracy);
    static void RenderHumanSymbol(float pXCenter, float pYCenter);
    static void RenderMonsterSymbol(float pXCenter, float pYCenter);
    static void RenderTubeSymbol(float pXCenter, float pYCenter);
    static void RenderMirrorSymbol(float pXCenter, float pYCenter);
    static void RenderNestSymbol(float pXCenter, float pYCenter);
    static void RenderCoffinSymbol(float pXCenter, float pYCenter);
    static void RenderGlyphSymbol(float pXCenter, float pYCenter);
    static void RenderChestSymbol(float pXCenter, float pYCenter);
    static void RenderItemSymbol(float pXCenter, float pYCenter);
    static void RenderWeaponSymbol(float pXCenter, float pYCenter);

    //--Special Rendering
    bool IsActorWithTagPresent(int pTag);
    virtual void RenderSpecialIcons();

    //--Pointer Routing
    StarLinkedList *GetEntityList();
    InventorySubClass *GetLocalInventory();
    StarLinkedList *GetContainerList();
    StarLinkedList *GetEffectList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PR_SetInternalName(lua_State *L);
int Hook_PR_SetPosition(lua_State *L);
int Hook_PR_GetPosition(lua_State *L);
int Hook_PR_AddConnection(lua_State *L);
int Hook_PR_HasConnection(lua_State *L);
int Hook_PR_GetEntitiesTotal(lua_State *L);
int Hook_PR_PushEntity(lua_State *L);
int Hook_PR_GetItemsTotal(lua_State *L);
int Hook_PR_GetContainersTotal(lua_State *L);
int Hook_PR_PushItem(lua_State *L);
int Hook_PR_IsPlayerPresent(lua_State *L);
int Hook_PR_OpenContainer(lua_State *L);
int Hook_PR_GetNextPathTo(lua_State *L);
int Hook_PR_IsEffectPresent(lua_State *L);
