-- |[ =================================== Open Mod Load Menu =================================== ]|
--Switches the menu to mod load order mode.
MapM_PushMenuStackHead()
    MT_SetProperty("Show Mod Load Order")
DL_PopActiveObject()
