//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///--[Local Definitions]
#define ACI_EFFECTS_PER_PAGE 16

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void AdvCombat::ActivateInspector()
{
    //--Flags.
    mIsShowingCombatInspector = true;
    mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
    mInspectorEntityCursor = 0;

    //--Refresh the combat inspector's entity listing. This is a reference list of all entities
    //  in the battle on all sides, excluding reinforcements.
    mrInspectorEntityList->ClearList();
    mrCombatParty->CloneToList(mrInspectorEntityList);
    mrEnemyCombatParty->CloneToList(mrInspectorEntityList);

    //--If the listing comes back with zero entries, there's nothing to inspect.
    if(mrInspectorEntityList->GetListSize() < 1)
    {
        mIsShowingCombatInspector = false;
        return;
    }

    //--Build the effect listing for the inspector.
    AdvCombatEntity *rZeroEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(0);
    mrInspectorEffectListing = ConstructEffectListReferencing(rZeroEntity);

    //--Recompute cursor.
    RecomputeInspectorHighlight();
    mInspectorHighlightPos.Complete();
    mInspectorHighlightSize.Complete();
}
void AdvCombat::DeactivateInspector()
{
    mIsShowingCombatInspector = false;
    delete mrInspectorEffectListing;
    mrInspectorEffectListing = NULL;
}

///======================================= Core Methods ===========================================
void AdvCombat::RecomputeInspectorHighlight()
{
    ///--[Documentation and Setup]
    //--Computes the position of the cursor based on the mode and orders the highlight values
    //  to move to that position.

    //--Setup.
    float tLft = 0.0f;
    float tTop = 0.0f;
    float tRgt = 1.0f;
    float tBot = 1.0f;

    ///--[Entity Select]
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_CHARSELECT)
    {
        //--Constants.
        float cBaseLft =  59.0f;
        float cBaseTop = 109.0f;
        float cBaseHei =  32.0f;

        //--Entity under consideration.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
        if(!rEntity) return;

        //--Get name and length.
        float cNameWid = Images.Data.rInspectorMainline->GetTextWidth(rEntity->GetDisplayName());
        float cNameHei = 19.0f;

        //--Cap name width.
        if(cNameWid >= 137.0f) cNameWid = 137.0f;

        //--Compute.
        tLft = cBaseLft - 2.0f;
        tTop = cBaseTop + ((mInspectorEntityCursor - mInspectorEntitySkip) * cBaseHei) - 2.0f;
        tRgt = cBaseLft + cNameWid + 4.0f;
        tBot = cBaseTop + ((mInspectorEntityCursor - mInspectorEntitySkip) * cBaseHei) + (cNameHei) + 3.0f;
    }
    ///--[Effect Select]
    else if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_EFFECTS)
    {
        //--Error check. There must be an effect listing.
        if(!mrInspectorEffectListing) return;

        //--Constants.
        float cBaseLft = 905.0f;
        float cBaseTop = 134.0f;
        float cBaseHei =  26.0f;
        float cNameHei = 19.0f;

        //--Variables.
        float tNameWid = 1.0f;
        float cNameMaxWid = 203.0f;

        //--Entity under consideration.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
        if(!rEntity) return;

        //--Get the effect under consideration.
        AdvCombatEffect *rEffect = (AdvCombatEffect *)mrInspectorEffectListing->GetElementBySlot(mInspectorEffectCursor);
        if(!rEffect) return;

        //--Zero line is the left-aligned title.
        AdvCombatEffectTargetPack *rPackage = rEffect->GetTargetPackByID(rEntity->GetID());
        if(rPackage && rPackage->mDescriptionLinesTotal > 0)
        {
            StarlightString *rZeroLine = rPackage->mDescriptionLines[0];
            if(rZeroLine) tNameWid = rZeroLine->GetLength(Images.Data.rInspectorMainline);
        }

        //--Cap name width.
        if(tNameWid >= cNameMaxWid) tNameWid = cNameMaxWid;

        //--Compute.
        tLft = cBaseLft - 3.0f;
        tTop = cBaseTop + ((mInspectorEffectCursor - mInspectorEffectSkip) * cBaseHei) - 3.0f;
        tRgt = cBaseLft + tNameWid + 4.0f;
        tBot = cBaseTop + ((mInspectorEffectCursor - mInspectorEffectSkip) * cBaseHei) + (cNameHei) + 3.0f;
    }
    ///--[Ability Query]
    else if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_ABILITIES)
    {
        //--Variables.
        float cLft = 0.0f;
        float cTop = 0.0f;

        //--Constants.
        float cWid = 50.0f;
        float cHei = 50.0f;
        float cSpX = 53.0f;
        float cSpY = 53.0f;

        //--Job skills.
        if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_JOB)
        {
            //--Position.
            cLft = 362.0f + 60.0f;
            cTop = 524.0f + 20.0f;

            //--Extra.
            if(mInspectorAbilityCursorY >= 1) cTop = cTop + 8.0f;
        }
        //--Memorized skills.
        else if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_MEMORIZED)
        {
            //--Position.
            cLft = 256.0f + 60.0f;
            cTop = 524.0f + 20.0f;

            //--Extra.
            if(mInspectorAbilityCursorY >= 2) cTop = cTop + 8.0f;
        }
        //--Tactics skills.
        else if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_TACTICS)
        {
            //--Position.
            cLft =  44.0f + 60.0f;
            cTop = 532.0f + 20.0f - 8.0f;
        }

        //--Compute.
        tLft = cLft + (mInspectorAbilityCursorX * cSpX) - 0.0f;
        tTop = cTop + (mInspectorAbilityCursorY * cSpY) - 0.0f;
        tRgt = cLft + (mInspectorAbilityCursorX * cSpX) + cWid + 0.0f;
        tBot = cTop + (mInspectorAbilityCursorY * cSpY) + cHei + 0.0f;
    }

    ///--[Common]
    //--Set.
    mInspectorHighlightPos. MoveTo(tLft,        tTop,        ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
    mInspectorHighlightSize.MoveTo(tRgt - tLft, tBot - tTop, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
}
StarLinkedList *AdvCombat::ConstructEffectListReferencing(AdvCombatEntity *pEntity)
{
    ///--[Documentation]
    //--Given an entity, constructs a list of all effects that affect this entity and should appear
    //  on the combat inspector. The list is returned and must be deallocated by the caller.

    //--Get basic effect list. The function allocates it for us.
    StarLinkedList *nrEffectList = GetEffectsReferencing(pEntity);

    //--Strip off effects that shouldn't be on the UI for whatever reason.
    AdvCombatEffect *rCheckEffect = (AdvCombatEffect *)nrEffectList->SetToHeadAndReturn();
    while(rCheckEffect)
    {
        //--Not visible.
        if(!rCheckEffect->IsVisibleOnUI()) nrEffectList->RemoveRandomPointerEntry();

        //--Next.
        rCheckEffect = (AdvCombatEffect *)nrEffectList->IncrementAndGetRandomPointerEntry();
    }

    //--Done. Return list.
    return nrEffectList;
}

///=========================================== Update =============================================
void AdvCombat::UpdateInspector()
{
    ///--[Documentation and Setup]
    //--Updates the combat inspector. This UI shows abilities, effects, resistances, and statistics
    //  for all entities in the battle so far.
    //--It has several sub-modes depending on where the cursor is.

    ///--[Timers]
    //--Standard vis timers.
    StarUIPiece::StandardTimer(mCombatInspectorTimer,          0, ADVCOMBAT_INSPECTOR_TICKS, mIsShowingCombatInspector);
    StarUIPiece::StandardTimer(mCombatInspectorJobTimer,       0, ADVCOMBAT_INSPECTOR_ABILITY_SWITCH_TICKS, (mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_JOB));
    StarUIPiece::StandardTimer(mCombatInspectorMemorizedTimer, 0, ADVCOMBAT_INSPECTOR_ABILITY_SWITCH_TICKS, (mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_MEMORIZED));
    StarUIPiece::StandardTimer(mCombatInspectorTacticsTimer,   0, ADVCOMBAT_INSPECTOR_ABILITY_SWITCH_TICKS, (mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_TACTICS));

    //--Highlights.
    mInspectorHighlightPos.Increment(EASING_CODE_QUADINOUT);
    mInspectorHighlightSize.Increment(EASING_CODE_QUADINOUT);

    ///--[Lockout Check]
    //--Inspector is not the active object. Stop.
    if(!mIsShowingCombatInspector) return;

    ///--[Subtype]
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_CHARSELECT)
    {
        UpdateInspectorEntitySelect();
    }
    else if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_EFFECTS)
    {
        UpdateInspectorEffectSelect();
    }
    else
    {
        UpdateInspectorAbilityQuery();
    }
}
void AdvCombat::UpdateInspectorEntitySelect()
{
    ///--[Documentation]
    //--Handles input when the player is selecting an entity in the combat inspector.
    bool tRecomputeCursor = false;
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Cursor]
    //--Up. Decrements.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEntityCursor, mInspectorEntitySkip, tIterations, mrInspectorEntityList->GetListSize(), 3, ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE, true);
    }
    //--Down. Increments.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEntityCursor, mInspectorEntitySkip, tIterations, mrInspectorEntityList->GetListSize(), 3, ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE, true);
    }

    //--Cursor needs to recompute.
    if(tRecomputeCursor)
    {
        //--Call.
        RecomputeInspectorHighlight();

        //--When the entity changes, the effect listing must be rebuilt.
        delete mrInspectorEffectListing;
        mrInspectorEffectListing = NULL;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Get entity, rebuild listing.
        AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
        if(!rInspectEntity) return;
        mrInspectorEffectListing = ConstructEffectListReferencing(rInspectEntity);
    }

    ///--[Toggle Resistances/Statistics]
    //--Bound to the C button.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--Flag.
        mInspectorShowStatistics = !mInspectorShowStatistics;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Toggle Skill Block]
    //--Bound to the run button.
    if(rControlManager->IsFirstPress("Run"))
    {
        //--Increment, clamp.
        mCombatInspectorAbilitiesType ++;
        if(mCombatInspectorAbilitiesType >= ADVCOMBAT_INSPECTOR_ABILITIES_TOTAL) mCombatInspectorAbilitiesType = 0;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Activate]
    //--Switches to effect inspection.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get entity.
        AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
        if(!rInspectEntity) return;

        //--The given entity must have at least one effect.
        StarLinkedList *trEffectCheckList = ConstructEffectListReferencing(rInspectEntity);
        if(trEffectCheckList->GetListSize() < 1)
        {
            delete trEffectCheckList;
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--At least one effect. Set to effects mode.
        mCombatInspectorMode = ADVCOMBAT_INSPECTOR_EFFECTS;
        RecomputeInspectorHighlight();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Switch to Ability Query]
    //--Switches to ability inspection. This does not work for enemies, as their abilities cannot be inspected.
    if(rControlManager->IsFirstPress("Jump"))
    {
        //--Check the entity.
        AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
        if(!rInspectEntity) return;

        //--If the entity in question has the show abilities flag as anything other than zero, don't show.
        if(rInspectEntity->GetInspectorShowAbilities() > 0) return;

        //--Set.
        mCombatInspectorMode = ADVCOMBAT_INSPECTOR_ABILITIES;
        RecomputeInspectorHighlight();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Cancel]
    //--Close the inspector.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Subroutine handles all the flags.
        DeactivateInspector();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
void AdvCombat::UpdateInspectorEffectSelect()
{
    ///--[Documentation]
    //--Handles input when the player is selecting an effect in the combat inspector.
    bool tRecomputeCursor = false;
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Cursor]
    //--Up. Decrements.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEffectCursor, mInspectorEffectSkip, tIterations, mrInspectorEffectListing->GetListSize(), 3, ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE, true);
    }
    //--Down. Increments.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEffectCursor, mInspectorEffectSkip, tIterations, mrInspectorEffectListing->GetListSize(), 3, ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE, true);
    }

    //--Cursor needs to recompute.
    if(tRecomputeCursor)
    {
        //--Call.
        RecomputeInspectorHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Toggle Resistances/Statistics]
    //--Bound to the C button.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--Flag.
        mInspectorShowStatistics = !mInspectorShowStatistics;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Toggle Skill Block]
    //--Bound to the run button.
    if(rControlManager->IsFirstPress("Run"))
    {
        //--Increment, clamp.
        mCombatInspectorAbilitiesType ++;
        if(mCombatInspectorAbilitiesType >= ADVCOMBAT_INSPECTOR_ABILITIES_TOTAL) mCombatInspectorAbilitiesType = 0;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Cancel]
    //--Returns to entity query.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Call, recompute cursor.
        mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
        RecomputeInspectorHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
void AdvCombat::UpdateInspectorAbilityQuery()
{
    ///--[Documentation]
    //--Handles input when the player is querying abilities in the combat inspector.
    bool tRecomputeCursor = false;
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Cursor]
    //--Up. Decrements.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursorNoScroll(mInspectorAbilityCursorY, tIterations, 3, true);
    }
    //--Down. Increments.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursorNoScroll(mInspectorAbilityCursorY, tIterations, 3, true);
    }

    //--Left. Decrements. Wrap is variable.
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Wrap size.
        int tWrapSize = 3;
        if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_MEMORIZED) tWrapSize = 5;
        if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_TACTICS)   tWrapSize = 9;

        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursorNoScroll(mInspectorAbilityCursorX, tIterations, tWrapSize, true);
    }
    //--Right. Increments, wrap is variable.
    else if(rControlManager->IsFirstPress("Right"))
    {
        //--Wrap size.
        int tWrapSize = 3;
        if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_MEMORIZED) tWrapSize = 5;
        if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_TACTICS)   tWrapSize = 9;

        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursorNoScroll(mInspectorAbilityCursorX, tIterations, tWrapSize, true);
    }

    //--Cursor needs to recompute.
    if(tRecomputeCursor)
    {
        //--Call.
        RecomputeInspectorHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Toggle Resistances/Statistics]
    //--Bound to the C button.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--Flag.
        mInspectorShowStatistics = !mInspectorShowStatistics;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Toggle Descriptions]
    //--Press F2.
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowDetailedAbilityDescriptions ++;
        if(mShowDetailedAbilityDescriptions > ADVCOMBAT_DESCRIPTIONS_HIDDEN) mShowDetailedAbilityDescriptions = ADVCOMBAT_DESCRIPTIONS_SIMPLE;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Toggle Skill Block]
    //--Bound to the run button. Switches the cursor back to 0x0.
    if(rControlManager->IsFirstPress("Run"))
    {
        //--Increment, clamp.
        mCombatInspectorAbilitiesType ++;
        if(mCombatInspectorAbilitiesType >= ADVCOMBAT_INSPECTOR_ABILITIES_TOTAL) mCombatInspectorAbilitiesType = 0;

        //--Cursor.
        mInspectorAbilityCursorX = 0;
        mInspectorAbilityCursorY = 0;
        RecomputeInspectorHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Cancel]
    //--Returns to entity query.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Call, recompute cursor.
        mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
        RecomputeInspectorHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///========================================== Drawing =============================================
void AdvCombat::RenderInspector()
{
    ///--[Documentation and Setup]
    //--Renders the combat inspector. The inspector slides in from the top/bottom of the screen and
    //  is visible as it slides out, even when it is not commanding the update.
    if(!Images.mIsReady || mCombatInspectorTimer < 1) return;

    ///--[Timer]
    //--Resolve timer offset.
    float cPercent = 1.0f - EasingFunction::QuadraticInOut(mCombatInspectorTimer, (float)ADVCOMBAT_INSPECTOR_TICKS);
    float cLftOffset = VIRTUAL_CANVAS_X * cPercent * -1.0f;
    float cTopOffset = VIRTUAL_CANVAS_Y * cPercent * -1.0f;
    float cRgtOffset = VIRTUAL_CANVAS_X * cPercent;
    float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;

    ///--[Setup]
    //--Reset mixer.
    StarlightColor::ClearMixer();

    //--Darken the background.
    StarBitmap::DrawFullBlack((1.0f - cPercent) * 0.90f);

    //--Entity in question.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
    if(!rEntity) return;

    ///============================= Left Block ===============================
    //--Offset.
    glTranslatef(cLftOffset * 1.0f, 0.0f, 0.0f);

    //--Stencil and position.
    TwoDimensionRealPoint tRenderDim = rEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_INSPECTOR);

    //--Entity portrait. Renders under everything else.
    StarBitmap *rEntityPortrait = rEntity->GetCombatPortrait();
    if(rEntityPortrait) rEntityPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);

    //--Entities information.
    RenderInspectorEntities();

    //--Clean.
    glTranslatef(cLftOffset * -1.0f, 0.0f, 0.0f);

    ///============================= Top Block ================================
    //--Offset.
    glTranslatef(0.0f, cTopOffset * 1.0f, 0.0f);

    //--Header.
    Images.Data.rInspectorHeader->Draw();

    //--Heading text.
    mHeadingStatic.SetAsMixer();
    Images.Data.rInspectorHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 7.0f, SUGARFONT_AUTOCENTER_X, "Combat Inspector");
    StarlightColor::ClearMixer();

    //--Summary fills.
    float tHPPct = rEntity->GetDisplayHPPct();
    if(tHPPct > 0.0f) Images.Data.rInspectorAllyHPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tHPPct, 1.0f);
    float tMPPct = rEntity->GetDisplayMPPct();
    if(tMPPct > 0.0f) Images.Data.rInspectorAllyMPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tMPPct, 1.0f);

    //--Statistics Summary.
    Images.Data.rInspectorSummaryFrame->Draw();

    //--Render the HP Value. This is a sum of all HP values.
    int tHPValue = rEntity->GetHealth();
    int tAdrenValue  = rEntity->GetAdrenaline();
    int tShieldValue = rEntity->GetShields();
    Images.Data.rEnemyHPFont->DrawTextArgs(248.0f, 82.0f, 0, 1.0f, "%i", tHPValue + tAdrenValue + tShieldValue);

    //--Render the MP Value.
    int tMPValue = rEntity->GetMagic();
    Images.Data.rEnemyHPFont->DrawTextArgs(248.0f, 99.0f, 0, 1.0f, "%i", tMPValue);

    //--Render just the health value, just the adrenaline, and just the shields.
    Images.Data.rEnemyHPFont->DrawTextArgs(262.0f, 119.0f, 0, 1.0f, "%i", tHPValue);
    Images.Data.rEnemyHPFont->DrawTextArgs(262.0f, 140.0f, 0, 1.0f, "%i", tAdrenValue);
    Images.Data.rEnemyHPFont->DrawTextArgs(262.0f, 161.0f, 0, 1.0f, "%i", tShieldValue);

    //--CP. Renders above the statistics.
    int tCP = rEntity->GetComboPoints();
    for(int i = 0; i < tCP; i ++)
    {
        Images.Data.rInspectorAllyCPPip->Draw(611.0f + (i * 4.0f), 91.0f);
    }

    //--Help text.
    mCombatInspectorHelp->      DrawText(0.0f, ADVCOMBAT_INSPECTOR_MAINLINE_H * 0.0f, 0, 1.0f, Images.Data.rInspectorMainline);
    mCombatInspectorScrollFast->DrawText(0.0f, ADVCOMBAT_INSPECTOR_MAINLINE_H * 1.0f, 0, 1.0f, Images.Data.rInspectorMainline);

    //--Clean.
    glTranslatef(0.0f, cTopOffset * -1.0f, 0.0f);

    ///============================ Right Block ===============================
    //--Offset.
    glTranslatef(cRgtOffset * 1.0f, 0.0f, 0.0f);

    //--Frames.
    RenderInspectorResistStats();
    RenderInspectorEffects();

    //--If in entity select mode, this help text appears.
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_CHARSELECT)
    {
        mCombatInspectorEffects->DrawText(1015.0f, 107.0f, 0, 1.0f, Images.Data.rInspectorMainline);
    }

    //--Clean.
    glTranslatef(cRgtOffset * -1.0f, 0.0f, 0.0f);

    ///============================ Bottom Block ==============================
    //--Offset.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    //--Sub-handler.
    RenderInspectorAbilities();
    RenderInspectorDescription();

    //--If in not-abilities mode, render this help text.
    if(mCombatInspectorMode != ADVCOMBAT_INSPECTOR_ABILITIES && rEntity->GetInspectorShowAbilities() < 1)
    {
        mCombatInspectorAbilities->DrawText(580.0f, 515.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rInspectorMainline);
    }

    //--Other help text.
    float cYPos = VIRTUAL_CANVAS_Y - ADVCOMBAT_INSPECTOR_MAINLINE_H;
    mCombatInspectorToggleResistances->DrawText(0.0f, cYPos - (ADVCOMBAT_INSPECTOR_MAINLINE_H * 1.0f), 0, 1.0f, Images.Data.rInspectorMainline);
    mCombatInspectorToggleSkills->     DrawText(0.0f, cYPos - (ADVCOMBAT_INSPECTOR_MAINLINE_H * 0.0f), 0, 1.0f, Images.Data.rInspectorMainline);

    //--Clean.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);

    ///============================ No Blocking ===============================
    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}

///=================================== Rendering Subhandlers ======================================
void AdvCombat::RenderInspectorEntities()
{
    ///--[Documentation]
    //--Renders a list of all the entities currently on the field. Also renders the highlight cursor
    //  if the player is selecting an entity.

    ///--[Frame]
    Images.Data.rInspectorEntitiesFrame->Draw();

    ///--[Entries]
    //--Constants.
    float cIconLft =  0.0f;
    float cIconTop = 95.0f;
    float cIconHei = 32.0f;
    float cNameLft = 60.0f;
    float cIconScale = 0.666667f;
    float cIconScaleInv = 1.0f / cIconScale;

    //--Iterate.
    int tRender = 0;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrInspectorEntityList->PushIterator();
    while(rEntity)
    {
        //--Skip when scrolling.
        int tEffectiveRender = tRender - mInspectorEntitySkip;
        if(tEffectiveRender < 0)
        {
            tRender ++;
            rEntity = (AdvCombatEntity *)mrInspectorEntityList->AutoIterate();
            continue;
        }

        //--Position.
        float cYPos = cIconTop + (cIconHei * tEffectiveRender);

        //--Backing.
        if(tRender % 2 == 1)
        {
            StarBitmap::DrawRectFill(0.0f, cYPos+2.0f, 231.0f, cYPos + cIconHei+2.0f, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
        }

        //--Entity icon.
        StarBitmap *rTurnIcon = rEntity->GetTurnIcon();
        if(rTurnIcon)
        {
            //--Position, scale.
            glTranslatef(cIconLft, cYPos, 0.0f);
            glScalef(cIconScale, cIconScale, 1.0f);

            //--Render.
            rTurnIcon->Draw();

            //--Unscale, unposition.
            glScalef(cIconScaleInv, cIconScaleInv, 1.0f);
            glTranslatef(-cIconLft, -cYPos, 0.0f);
        }

        //--Name.
        Images.Data.rInspectorMainline->DrawTextFixed(cNameLft, cYPos + 12.0f, 137.0f, 0, rEntity->GetDisplayName());

        //--Clamp. Only render ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE entries.
        tRender ++;
        if(tEffectiveRender >= ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE-1)
        {
            mrInspectorEntityList->PopIterator();
            break;
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrInspectorEntityList->AutoIterate();
    }

    ///--[Highlight]
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_CHARSELECT)
    {
        RenderExpandableHighlight(mInspectorHighlightPos, mInspectorHighlightSize, 4.0f, Images.Data.rHighlight);
    }

    ///--[Scrollbar]
    if(mrInspectorEntityList->GetListSize() > ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE)
    {
        RenderScrollbar(mInspectorEntitySkip, ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE, mrInspectorEntityList->GetListSize(), 122.0f, 353.0f, true, Images.Data.rInspectorEntitiesScrollbar, Images.Data.rScrollbarFront);
    }
}
void AdvCombat::RenderInspectorResistStats()
{
    ///--[Documentation]
    //--Renders the Resistances/Statistics window, depending on which flag is active.

    ///--[Common]
    //--Backing and detail.
    Images.Data.rInspectorResistancesFrame->Draw();
    Images.Data.rInspectorResistancesDetail->Draw();

    //--Get entity.
    AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
    if(!rInspectEntity) return;

    ///--[Resistances]
    if(!mInspectorShowStatistics)
    {
        ///--[Common]
        //--Heading.
        mHeadingStatic.SetAsMixer();
        Images.Data.rInspectorHeading->DrawTextFixed(624.0f, 91.0f, 220.0f, 0, "Resistances");
        StarlightColor::ClearMixer();

        ///--[Do Not Show]
        //--Enemies need to be defeated a certain number of times to show their resistances. Allies never do this.
        if(!IsEntityInPlayerParty(rInspectEntity) && rInspectEntity->GetInspectorShowResists() > 0)
        {
            float cTxtHei =  18.0f;
            float cMaxWid = 237.0f;
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 0.0f), cMaxWid, 0, "Unknown.");
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 2.0f), cMaxWid, 0, "Defeat this enemy");
            Images.Data.rInspectorStatistic->DrawTextFixedArgs(614.0f, 134.0f + (cTxtHei * 3.0f), cMaxWid, 0, "%i more times to", rInspectEntity->GetInspectorShowResists());
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 4.0f), cMaxWid, 0, "reveal their resistances.");
        }
        ///--[Show All Resistances]
        else
        {
            //--Icon/Value setup.
            float cIconX = 622.0f;
            float cIconY = 136.0f;
            float cIconH =  25.0f;
            float cValueX = cIconX + 50.0f;
            float cValueY = cIconY -  3.0f;
            float cReductionX = 844.0f;
            float cReductionY = cIconY - 3.0f;

            //--Icons, resist values, reduction percentage.
            for(int i = 0; i < ADVCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL; i ++)
            {
                //--Backing.
                if(i % 2 == 1)
                {
                    StarBitmap::DrawRectFill(666.0f, cValueY + (cIconH * i), 851.0f, cValueY + (cIconH * i) + cIconH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
                }

                //--Setup.
                int tResistValue = rInspectEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_RESIST_START + i);

                //--Render icon.
                Images.Data.rInspectorResistIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

                //--Value is 1000 or higher. Render "Immune".
                if(tResistValue >= 1000)
                {
                    Images.Data.rInspectorStatistic->DrawText(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
                }
                //--Otherwise, render the value and the percentage reduction.
                else
                {
                    //--If a bonus is applied, render the statistic in purple. If a malus is applied, red.
                    int tBonusAbi = rInspectEntity->GetStatistic(ADVCE_STATS_TEMPEFFECT, STATS_RESIST_START + i);
                    if(tBonusAbi > 0)
                    {
                        mInspectorBonus.SetAsMixer();
                    }
                    else if(tBonusAbi < 0)
                    {
                        mInspectorMalus.SetAsMixer();
                    }

                    //--Resistance amount.
                    Images.Data.rInspectorStatistic->DrawTextArgs(cValueX, cValueY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistValue);

                    //--Computed reduction.
                    float cReduction = 1.0f - AdvCombatEntity::ComputeResistance(tResistValue);
                    Images.Data.rInspectorStatistic->DrawTextArgs(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%0.0f%%", cReduction * 100.0f);

                    //--Clean.
                    StarlightColor::ClearMixer();
                }
            }
        }
    }
    ///--[Statistics]
    else
    {
        ///--[Do Not Show]
        //--Enemies need to be defeated a certain number of times to show their resistances. Allies never do this.
        if(!IsEntityInPlayerParty(rInspectEntity) && rInspectEntity->GetInspectorShowStats() > 0)
        {
            //--Heading.
            mHeadingStatic.SetAsMixer();
            Images.Data.rInspectorHeading->DrawTextFixed(679.0f, 91.0f, 220.0f, 0, "Statistics");
            StarlightColor::ClearMixer();

            float cTxtHei =  18.0f;
            float cMaxWid = 237.0f;
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 0.0f), cMaxWid, 0, "Unknown.");
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 2.0f), cMaxWid, 0, "Defeat this enemy");
            Images.Data.rInspectorStatistic->DrawTextFixedArgs(614.0f, 134.0f + (cTxtHei * 3.0f), cMaxWid, 0, "%i more times to", rInspectEntity->GetInspectorShowStats());
            Images.Data.rInspectorStatistic->DrawTextFixed    (614.0f, 134.0f + (cTxtHei * 4.0f), cMaxWid, 0, "reveal their statistics.");
        }
        ///--[Show All Resistances]
        else
        {
            //--Heading.
            mHeadingStatic.SetAsMixer();
            Images.Data.rInspectorHeading->DrawTextFixed(624.0f, 91.0f, 165.0f, 0, "Statistics");
            StarlightColor::ClearMixer();

            //--Icon/Value setup.
            float cIconX  = 622.0f;
            float cIconY  = 136.0f;
            float cIconH  =  25.0f;
            float cValueX = 841.0f;
            float cValueY = cIconY -  3.0f;

            //--Lookup table.
            for(int i = 0; i < ADVCOMBAT_INSPECTOR_STATUS_ICON_TOTAL; i ++)
            {
                //--Backing.
                if(i % 2 == 1)
                {
                    StarBitmap::DrawRectFill(611.0f, cValueY + (cIconH * i), 851.0f, cValueY + (cIconH * i) + cIconH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
                }

                //--Image.
                Images.Data.rInspectorStatusIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

                //--Statistic.
                int tValue = rInspectEntity->GetStatistic(mInspectorStatLookups[i]);

                //--If a bonus is applied, render the statistic in purple. If a malus is applied, red.
                int tBonusAbi = rInspectEntity->GetStatistic(ADVCE_STATS_TEMPEFFECT, mInspectorStatLookups[i]);
                if(tBonusAbi > 0)
                {
                    mInspectorBonus.SetAsMixer();
                }
                else if(tBonusAbi < 0)
                {
                    mInspectorMalus.SetAsMixer();
                }

                //--Render.
                Images.Data.rInspectorStatistic->DrawTextArgs(cValueX, cValueY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
                StarlightColor::ClearMixer();
            }
        }
    }
}
void AdvCombat::RenderInspectorEffects()
{
    ///--[Documentation]
    //--Renders the Effects window, showing a list of all effects applying to the entity selected.
    AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
    if(!rInspectEntity) return;

    //--Do nothing if there's no effect list.
    if(!mrInspectorEffectListing) return;

    //--Get ID, used for naming.
    uint32_t tID = rInspectEntity->GetID();

    ///--[Constants]
    //--Positions.
    float cIconLft  =  877.0f;
    float cNameLft  =  905.0f;
    float cShortRgt = 1298.0f;
    float cIconTop  =  131.0f;
    float cIconHei  =   26.0f;

    //--Sizes.
    float cNameMaxWid = 203.0f;
    float cDescMaxWid = 165.0f;

    //--Scale.
    float cIconScale = 0.50f;
    float cIconScaleInv = 1.0f / cIconScale;

    ///--[Frame]
    //--Backing and detail.
    Images.Data.rInspectorEffectsFrame->Draw();
    Images.Data.rInspectorEffectsDetail->Draw();

    //--Header.
    mHeadingStatic.SetAsMixer();
    Images.Data.rInspectorHeading->DrawText(888.0f, 91.0f, 0, "Effects");
    StarlightColor::ClearMixer();

    ///--[Effect Listing]
    //--Iterate.
    int tRenders = 0;
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrInspectorEffectListing->PushIterator();
    while(rEffect)
    {
        //--Skip when scrolling.
        int tEffectiveRender = tRenders - mInspectorEffectSkip;
        if(tEffectiveRender < 0)
        {
            tRenders ++;
            rEffect = (AdvCombatEffect *)mrInspectorEffectListing->AutoIterate();
            continue;
        }

        //--Compute position.
        float cYPos = cIconTop + (cIconHei * tEffectiveRender);

        //--Backing.
        if(tRenders % 2 == 1)
        {
            StarBitmap::DrawRectFill(877.0f, cYPos, 1330.0f, cYPos + cIconHei, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
        }

        //--Render the effect origin icon.
        glTranslatef(cIconLft, cYPos, 0.0f);
        glScalef(cIconScale, cIconScale, 1.0f);
        rEffect->RenderAt(0.0f, 0.0f);
        glScalef(cIconScaleInv, cIconScaleInv, 1.0f);
        glTranslatef(-cIconLft, -cYPos, 0.0f);

        //--The 0th/1st description line is the title line of the effect.
        AdvCombatEffectTargetPack *rPackage = rEffect->GetTargetPackByID(tID);
        if(rPackage)
        {
            //--Zero line is the left-aligned title.
            if(rPackage->mDescriptionLinesTotal > 0)
            {
                StarlightString *rZeroLine = rPackage->mDescriptionLines[0];
                if(rZeroLine) rZeroLine->DrawTextFixed(cNameLft, cYPos + 0.0f, cNameMaxWid, 0, Images.Data.rInspectorMainline);
            }
            //--One line is the right-aligned title.
            if(rPackage->mDescriptionLinesTotal > 1)
            {
                StarlightString *rOneLine = rPackage->mDescriptionLines[1];
                if(rOneLine) rOneLine->DrawTextFixed(cShortRgt, cYPos + 0.0f, cDescMaxWid, SUGARFONT_RIGHTALIGN_X, Images.Data.rInspectorMainline);
            }
        }

        //--Clamp. Only render ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE entries.
        tRenders ++;
        if(tEffectiveRender >= ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE-1)
        {
            mrInspectorEffectListing->PopIterator();
            break;
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mrInspectorEffectListing->AutoIterate();
    }

    ///--[Highlight]
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_EFFECTS)
    {
        RenderExpandableHighlight(mInspectorHighlightPos, mInspectorHighlightSize, 4.0f, Images.Data.rHighlight);
    }

    ///--[Scrollbar]
    if(mrInspectorEffectListing->GetListSize() > ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE)
    {
        RenderScrollbar(mInspectorEffectSkip, ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE, mrInspectorEffectListing->GetListSize(), 164.0f, 270.0f, true, Images.Data.rInspectorEffectsScrollbar, Images.Data.rScrollbarFront);
    }
}
void AdvCombat::RenderInspectorAbilities()
{
    ///--[Documentation]
    //--Renders the abilities in the specified box (job/memorized/tactics) for the Combat Inspector.
    AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
    if(!rInspectEntity) return;

    //--If the entity in question has the show abilities flag as anything other than zero, don't show.
    if(rInspectEntity->GetInspectorShowAbilities() > 0) return;

    ///--[Constants]
    //--The Inspector versions of these functions is offset from the default slightly.
    float cOffsetX = 60.0f;
    float cOffsetY = 20.0f;

    ///--[Job]
    //--Renders based on timer.
    if(mCombatInspectorJobTimer > 0)
    {
        //--Setup.
        float cLft = 362.0f + cOffsetX;
        float cTop = 524.0f + cOffsetY;

        //--Offset.
        float cPercent = 1.0f - EasingFunction::QuadraticInOut(mCombatInspectorJobTimer, (float)ADVCOMBAT_INSPECTOR_ABILITY_SWITCH_TICKS);
        float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;
        glTranslatef(0.0f, cBotOffset, 0.0f);

        //--Backing.
        Images.Data.rJobSkillsFrame->Draw(cOffsetX, cOffsetY);

        //--Render.
        RenderAbilityBlock(cLft, cTop, ADVCOMBAT_ABILITY_BLOCK_JOB, rInspectEntity);

        //--Highlight.
        if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_ABILITIES && mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_JOB)
        {
            RenderExpandableHighlight(mInspectorHighlightPos, mInspectorHighlightSize, 4.0f, Images.Data.rHighlight);
        }

        //--Clean.
        glTranslatef(0.0f, -cBotOffset, 0.0f);
    }

    ///--[Memorized]
    //--Renders based on timer.
    if(mCombatInspectorMemorizedTimer > 0)
    {
        //--Setup.
        float cLft = 256.0f + cOffsetX;
        float cTop = 524.0f + cOffsetY;

        //--Offset.
        float cPercent = 1.0f - EasingFunction::QuadraticInOut(mCombatInspectorMemorizedTimer, (float)ADVCOMBAT_INSPECTOR_ABILITY_SWITCH_TICKS);
        float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;
        glTranslatef(0.0f, cBotOffset, 0.0f);

        //--Backing.
        Images.Data.rMemorizedSkillsFrame->Draw(cOffsetX, cOffsetY);

        //--Render.
        RenderAbilityBlock(cLft, cTop, ADVCOMBAT_ABILITY_BLOCK_MEMORIZED, rInspectEntity);

        //--Highlight.
        if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_ABILITIES && mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_MEMORIZED)
        {
            RenderExpandableHighlight(mInspectorHighlightPos, mInspectorHighlightSize, 4.0f, Images.Data.rHighlight);
        }

        //--Clean.
        glTranslatef(0.0f, -cBotOffset, 0.0f);
    }

    ///--[Tactics]
    //--Renders based on timer.
    if(mCombatInspectorTacticsTimer > 0)
    {
        //--Setup.
        float cLft =  44.0f + cOffsetX;
        float cTop = 532.0f + cOffsetY - 8.0f;

        //--Offset.
        float cPercent = 1.0f - EasingFunction::QuadraticInOut(mCombatInspectorTacticsTimer, (float)ADVCOMBAT_INSPECTOR_ABILITY_SWITCH_TICKS);
        float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;
        glTranslatef(0.0f, cBotOffset, 0.0f);

        //--Backing.
        Images.Data.rTacticsSkillsFrame->Draw(cOffsetX, cOffsetY - 8.0f);

        //--Render.
        RenderAbilityBlock(cLft, cTop, ADVCOMBAT_ABILITY_BLOCK_TACTICS, rInspectEntity);

        //--Highlight.
        if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_ABILITIES && mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_TACTICS)
        {
            RenderExpandableHighlight(mInspectorHighlightPos, mInspectorHighlightSize, 4.0f, Images.Data.rHighlight);
        }

        //--Clean.
        glTranslatef(0.0f, -cBotOffset, 0.0f);
    }
}
void AdvCombat::RenderInspectorDescription()
{
    ///--[Documentation]
    //--Renders the description for whatever is currently highlighted. This can be an ability or an effect.
    //  If in entity selection mode, just render the backing.
    AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
    if(!rInspectEntity) return;

    //--The Inspector versions of these functions is offset from the default slightly.
    float cOffsetX = 60.0f;
    float cOffsetY = 20.0f;

    ///--[Frames]
    //--Description frame.
    Images.Data.rDescription->Draw(cOffsetX, cOffsetY);

    ///--[Effect]
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_EFFECTS)
    {
        //--Get the effect in question.
        AdvCombatEffect *rEffect = (AdvCombatEffect *)mrInspectorEffectListing->GetElementBySlot(mInspectorEffectCursor);
        if(!rEffect) return;

        //--Get the pacakge associated with this entity. Some effects afflict multiple targets and the values
        //  may vary per target.
        AdvCombatEffectTargetPack *rPackage = rEffect->GetTargetPackByID(rInspectEntity->GetID());
        if(!rPackage) return;

        //--Name.
        Images.Data.rInspectorHeading->DrawText(619.0f + cOffsetX, 466.0f + cOffsetY, 0, 1.0f, rEffect->GetDisplayName());

        //--Position.
        float cTxtL = 561.0f + cOffsetX;
        float cTxtT = 516.0f + cOffsetY;
        float cTxtH =  29.0f;

        //--Render. We start at line 2 since lines 0/1 are the title lines.
        int tTotalLines = rPackage->mDescriptionLinesTotal;
        for(int i = 2; i < tTotalLines; i ++)
        {
            StarlightString *rDescription = rPackage->mDescriptionLines[i];
            if(rDescription) rDescription->DrawText(cTxtL, cTxtT + (cTxtH * (i-2)), 0, 1.0f, Images.Data.rAbilitySimpleDescriptionFont);
        }
    }
    ///--[Ability]
    else if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_ABILITIES)
    {
        //--Get the ability in question.
        AdvCombatAbility *rAbility = NULL;
        if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_JOB)
        {
            rAbility = rInspectEntity->GetAbilityBySlot(mInspectorAbilityCursorX, mInspectorAbilityCursorY);
        }
        else if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_MEMORIZED)
        {
            rAbility = rInspectEntity->GetAbilityBySlot(mInspectorAbilityCursorX + ACE_ABILITY_MEMORIZE_GRID_X0, mInspectorAbilityCursorY);
        }
        else if(mCombatInspectorAbilitiesType == ADVCOMBAT_INSPECTOR_ABILITIES_TACTICS)
        {
            rAbility = rInspectEntity->GetAbilityBySlot(mInspectorAbilityCursorX + ACE_ABILITY_TACTICS_GRID_X0, mInspectorAbilityCursorY);
        }

        //--Ability failed to resolve. Render nothing.
        if(!rAbility) return;

        //--Render description. This is identical to the normal description rendering, but offset.
        glTranslatef(cOffsetX, cOffsetY, 0.0f);
        RenderSkillDescription(rAbility);
        glTranslatef(-cOffsetX, -cOffsetY, 0.0f);
    }
}
