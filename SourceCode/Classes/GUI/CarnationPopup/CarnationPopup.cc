//--Base
#include "CarnationPopup.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "CarnationDef.h"
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

///========================================== System ==============================================
CarnationPopup::CarnationPopup()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ========= Derived ======== ]
    ///--[System]
    mIsShowing = true;
    mVisTimer = 0;

    ///--[Display]
    mTitle = NULL;
    mTitleX = 0;
    mTitleY = 0;
    mEntryList = new StarLinkedList(true);

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
CarnationPopup::~CarnationPopup()
{
    delete mEntryList;
}
void CarnationPopup::DeleteThis(void *pPtr)
{
    CarnationPopup *rPtr = (CarnationPopup *)pPtr;
    delete rPtr;
}

///===================================== Property Queries =========================================
bool CarnationPopup::IsShowing()
{
    return mIsShowing;
}
bool CarnationPopup::IsVisible()
{
    return (mVisTimer > 0);
}

///======================================= Manipulators ===========================================
void CarnationPopup::SetVisible(bool pIsVisible)
{
    mIsShowing = pIsVisible;
}
void CarnationPopup::SetTitle(const char *pTitle)
{
    if(!pTitle) return;
    ResetString(mTitle, pTitle);
}
void CarnationPopup::SetPosition(int pX, int pY)
{
    mTitleX = pX;
    mTitleY = pY;
}
void CarnationPopup::RegisterExaminable(const char *pInternalName, const char *pDisplayName, const char *pScript, const char *pArgument)
{
    //--Arg check.
    if(!pInternalName || !pDisplayName || !pScript || !pArgument) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    CarnExaminable *nExaminable = (CarnExaminable *)starmemoryalloc(sizeof(CarnExaminable));
    nExaminable->Initialize();

    //--Set values.
    ResetString(nExaminable->mDisplayName, pDisplayName);
    ResetString(nExaminable->mScript, pScript);
    ResetString(nExaminable->mArgument, pArgument);

    //--Register.
    mEntryList->AddElement(pInternalName, nExaminable, &CarnExaminable::DeleteThis);
}

///======================================= Core Methods ===========================================
void CarnationPopup::RecomputePositions(StarFont *pFont)
{
    ///--[Documentation]
    //--Recalculates where all the examinables within the popup are and how wide the object is.
    if(!pFont) return;
    mWidestEntry = 0.0f;

    ///--[Determine Width]
    //--Resolve what the widest entry is, including the title.
    CarnExaminable *rCheckEntry = (CarnExaminable *)mEntryList->PushIterator();
    while(rCheckEntry)
    {
        //--Get the display string's length.
        float cLength = pFont->GetTextWidth(rCheckEntry->mDisplayName);

        //--Use if it's the longest.
        if(cLength > mWidestEntry) mWidestEntry = cLength;

        //--Next.
        rCheckEntry = (CarnExaminable *)mEntryList->AutoIterate();
    }

    //--Also check the object's title.
    if(mTitle)
    {
        float cLength = pFont->GetTextWidth(mTitle);
        if(cLength > mWidestEntry) mWidestEntry = cLength;
    }

    //--Add 2x the padding.
    mWidestEntry = mWidestEntry + (CARNPOP_HPAD * 2.0f);

    ///--[Reposition]
    //--Make all entries widen out.
    int i = 0;
    CarnExaminable *rSetEntry = (CarnExaminable *)mEntryList->PushIterator();
    while(rSetEntry)
    {
        //--Compute X/Y Positions.
        rSetEntry->mHeartX = mTitleX;
        rSetEntry->mHeartY = mTitleY + (CARNPOP_VSIZE + CARNPOP_TITLEBREAK) + (CARNPOP_VSIZE * i);
        rSetEntry->mHeartR = rSetEntry->mHeartX + mWidestEntry;
        rSetEntry->mHeartB = rSetEntry->mHeartY + CARNPOP_VSIZE;

        //--Next.
        i ++;
        rSetEntry = (CarnExaminable *)mEntryList->AutoIterate();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void CarnationPopup::Update()
{
    ///--[Timer]
    if(mIsShowing)
    {
        if(mVisTimer < CARNPOP_VIS_TICKS) mVisTimer ++;
    }
    else
    {
        if(mVisTimer > 0) mVisTimer --;
    }
}
CarnExaminable *CarnationPopup::HandleClick(int pX, int pY)
{
    ///--[Documentation]
    //--Checks if the click in question hit any members of this list. If so, returns the clicked
    //  examinable. Returns NULL if nothing was hit. If the list is not showing, always returns NULL.
    if(!mIsShowing) return NULL;

    //--Scan examinables.
    CarnExaminable *rEntry = (CarnExaminable *)mEntryList->PushIterator();
    while(rEntry)
    {
        //--Check for a hit.
        if(IsPointWithin(pX, pY, rEntry->mHeartX, rEntry->mHeartY, rEntry->mHeartR, rEntry->mHeartB))
        {
            mEntryList->PopIterator();
            return rEntry;
        }

        //--Next.
        rEntry = (CarnExaminable *)mEntryList->AutoIterate();
    }

    //--No hits, return NULL.
    return NULL;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnationPopup::Render(StarBitmap *pHeart, StarBitmap *pFrame, StarFont *pFont)
{
    ///--[Documentation]
    //--Renders a popup menu, which shows its title and buttons. The menu has a percentage rendering
    //  done using stencils to make it "slide" into view.
    if(!pHeart || !pFrame || !pFont || !mTitle) return;

    ///--[Stencil]
    //--If the object is hiding, use stencils to slide the object into existence, or out.
    if(mVisTimer < CARNPOP_VIS_TICKS)
    {
        //--Set stencil mode.
        DisplayManager::ActivateMaskRender(5);

        //--Compute size.
        float cMaxSize = CARNPOP_VSIZE + CARNPOP_TITLEBREAK + (CARNPOP_VSIZE * mEntryList->GetListSize());
        float cUseSize = EasingFunction::QuadraticInOut(mVisTimer, CARNPOP_VIS_TICKS) * cMaxSize;

        //--Render the frame.
        pFrame->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.50f, 0.50f);
            glVertex2f(mTitleX +         0.0f, mTitleY + 0.0f);
            glVertex2f(mTitleX + mWidestEntry, mTitleY + 0.0f);
            glVertex2f(mTitleX + mWidestEntry, mTitleY + cUseSize);
            glVertex2f(mTitleX +         0.0f, mTitleY + cUseSize);
        glEnd();

        //--Set stencil mode.
        DisplayManager::ActivateStencilRender(5);
    }

    ///--[Title]
    //--Backing.
    pFrame->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(0.50f, 0.50f);
        glVertex2f(mTitleX +         0.0f, mTitleY + 0.0f);
        glVertex2f(mTitleX + mWidestEntry, mTitleY + 0.0f);
        glVertex2f(mTitleX + mWidestEntry, mTitleY + CARNPOP_VSIZE);
        glVertex2f(mTitleX +         0.0f, mTitleY + CARNPOP_VSIZE);
    glEnd();

    //--Title.
    pFont->DrawText(mTitleX + (mWidestEntry * 0.50f), mTitleY + (CARNPOP_VSIZE * 0.50f), SUGARFONT_AUTOCENTER_XY, 1.0f, mTitle);

    //--Heart.
    pHeart->Draw(mTitleX + CARNPOP_HEART_OFF_X, mTitleY);

    ///--[Entries]
    CarnExaminable *rEntry = (CarnExaminable *)mEntryList->PushIterator();
    while(rEntry)
    {
        //--Backing.
        pFrame->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.50f, 0.50f);
            glVertex2f(rEntry->mHeartX, rEntry->mHeartY);
            glVertex2f(rEntry->mHeartR, rEntry->mHeartY);
            glVertex2f(rEntry->mHeartR, rEntry->mHeartB);
            glVertex2f(rEntry->mHeartX, rEntry->mHeartB);
        glEnd();

        //--Name.
        if(rEntry->mDisplayName) pFont->DrawText(mTitleX + (mWidestEntry * 0.50f), rEntry->mHeartY + (CARNPOP_VSIZE * 0.50f), SUGARFONT_AUTOCENTER_XY, 1.0f, rEntry->mDisplayName);

        //--Next.
        rEntry = (CarnExaminable *)mEntryList->AutoIterate();
    }

    ///--[Clear Stencils]
    DisplayManager::DeactivateStencilling();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
