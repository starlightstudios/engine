//--Base
#include "ZoneEditor.h"

//--Classes
#include "ZoneNode.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

//--Functions concerning the NodeEditor, which controls Nodes within the program. Since it's basically
//  an editor within the editor, it needs some special care.

void ZoneEditor::ResetNodeEditor(ZoneNode *pNode)
{
    //--Given a node, reset all the variables and command positions for the node editor. Hides the
    //  editor if the node was NULL. Does nothing if the node didn't actually change.
    if(pNode == rActiveNode) return;

    //--If NULL, just hide the editor.
    rActiveNode = pNode;
    if(!rActiveNode) return;

    //--Rebuild the facing pack list.
    ResetFacingPacks(pNode);

    //--Rebuild the transition pack list.
    ResetTransitionPacks(pNode);
}
void ZoneEditor::ResetFacingPacks(ZoneNode *pNode)
{
    //--Whenever the facing pack list changes, run this to reset all the packs to their correct positions.
    if(!pNode) return;

    //--Reset the command positions, and remove the facing commands.
    CommandPack *rNewFacingPack = FindCommandPack("New Facing");
    if(rNewFacingPack)
    {
        rNewFacingPack->mYPos = mBoxCoords.mTop + (ZE_NAME_SIZE_PER*4.0f);
    }

    //--Search for all node packs. Some won't be found.
    char tCheckBuffer[32];
    for(int i = 0; i < 26; i ++)
    {
        //--Set.
        sprintf(tCheckBuffer, "Facing %c", i + 'A');

        //--If the pack exists, remove it. If it doesn't, no more packs exist after it.
        CommandPack *rCheckPack = FindCommandPack(tCheckBuffer);
        if(!rCheckPack) break;

        //--Pack exists, remove it.
        mCommandsList->RemoveElementP(rCheckPack);
    }

    //--Add facings for each facing on the new node.
    int tFacingsTotal = rActiveNode->GetFacingsTotal();
    for(int i = 0; i < tFacingsTotal; i ++)
    {
        //--Add the command.
        sprintf(tCheckBuffer, "Facing %c", i + 'A');
        mCommandsList->AddElement("X", CreateCommandPack(tCheckBuffer, mBoxCoords.mLft + cIndent, mBoxCoords.mTop + (ZE_NAME_SIZE_PER*(float)(4 + i)), ZE_FUNCTION_NODE_SET_FACING, mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);

        //--Move the facing pack down.
        if(rNewFacingPack) rNewFacingPack->mYPos = rNewFacingPack->mYPos + ZE_NAME_SIZE_PER;
    }
}
void ZoneEditor::ResetTransitionPacks(ZoneNode *pNode)
{
    //--Whenever the transition pack list changes, run this to reset all the packs to their correct positions.
    if(!pNode) return;

    //--Setup.
    char tBuffer[64];

    //--Search for all auto-transition packs. Remove what is found.
    for(int i = 0; i < 26; i ++)
    {
        //--Set.
        sprintf(tBuffer, "Trans %c", i + 'A');

        //--If the pack exists, remove it. If it doesn't, no more packs exist after it.
        CommandPack *rCheckPack = FindCommandPack(tBuffer);
        if(!rCheckPack) break;

        //--Pack exists, remove it.
        mCommandsList->RemoveElementP(rCheckPack);
    }

    //--Add commands corresponding to the auto-transition packs. This can be zero.
    int tAutoTransitionsTotal = pNode->GetAutoTransitionsTotal();
    for(int i = 0; i < tAutoTransitionsTotal; i ++)
    {
        //--Compute position.
        float tUseX = mBoxCoords.mRgt + ZE_NE_AUTOTRANS_X;
        float tUseY = mBoxCoords.mTop + ZE_NE_AUTOTRANS_Y + (ZE_NAME_SIZE_PER * i);

        //--Add the command.
        sprintf(tBuffer, "Trans %c", i + 'A');
        mCommandsList->AddElement("X", CreateCommandPack(tBuffer, tUseX, tUseY, -1, mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
    }
}
void ZoneEditor::AddTransitionToNodeEditor()
{
    //--Adds a new AutoTransition to the Node Editor. This is just the CommandPack associated with it,
    //  which gets dynamically renamed. A node must be active or this does nothing. We also assume that
    //  the transition has been added already, it is not done in this function.
    if(!rActiveNode) return;

    //--Setup.
    float tUseX = 0.0f;
    float tUseY = 0.0f;
    int tLastIndex = rActiveNode->GetAutoTransitionsTotal();

    //--Get the position of the last added command.
    char tBuffer[64];
    sprintf(tBuffer, "Trans %c", tLastIndex + 'A' - 2);
    CommandPack *rAbovePack = FindCommandPack(tBuffer);

    //--If the pack exists, use its position with an offset.
    if(rAbovePack)
    {
        tUseX = rAbovePack->mXPos;
        tUseY = rAbovePack->mYPos + ZE_NAME_SIZE_PER;
    }
    //--If the pack doesn't exist, use a standard position as this is the 0th pack.
    else
    {
        tUseX = mBoxCoords.mRgt + ZE_NE_AUTOTRANS_X;
        tUseY = mBoxCoords.mTop + ZE_NE_AUTOTRANS_Y;
    }

    //--Create the command.
    sprintf(tBuffer, "Trans %c", tLastIndex + 'A' - 1);
    mCommandsList->AddElement("X", CreateCommandPack(tBuffer, tUseX, tUseY, -1, mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
}
