///--[RayCollisionEngine]
//--An object normally held within an AdventureLevel object. Stores a Blockmap or a BSP tree which
//  speeds up raytracing. If the object is not initialized, always fails to find a collision.

#pragma once

///--[Includes]
#include "Definitions.h"
#include "Structures.h"
struct CollisionPack;

///--[Local Structures]
typedef struct
{
    //--Members
    float mXA, mYA;
    float mXB, mYB;
    float mAngle;
    uint32_t mLastRayPulse;

    //--Functions
    void RecomputeAngle()
    {
        mAngle = atan2f(mYB - mYA, mXB - mXA);
    }
}RCELine;

///--[Local Definitions]
///--[Classes]
class RayCollisionEngine
{
    private:
    //--System
    bool mIsBooted;
    uint32_t mCurrentRayPulse;

    //--Total Line List
    StarLinkedList *mMasterLineList;

    //--Map Properties
    float mTileSize;

    //--Blockmap Properties
    int mBlockmapSizeX;
    int mBlockmapSizeY;
    StarLinkedList ***mBlockmap;
    float mBlockmapDimX;
    float mBlockmapDimY;

    protected:

    public:
    //--System
    RayCollisionEngine();
    ~RayCollisionEngine();

    //--Public Variables
    //--Property Queries
    void GetShortestIntercept(float pXA, float pYA, float pXB, float pYB, float &sInterceptX, float &sInterceptY);

    //--Manipulators
    void SetTileSize(float pValue);
    void RegisterVariableLine(const char *pLineName, float pXA, float pYA, float pXB, float pYB);
    void UnsetVariableLine(const char *pLineName);
    void ResetVariableLine(const char *pLineName);

    //--Core Methods
    void BuildWith(CollisionPack pCollisionPack);

    //--Engine Lookups
    bool IsLftBlocked(int pX, int pY, CollisionPack pCollisionPack);
    bool IsTopBlocked(int pX, int pY, CollisionPack pCollisionPack);
    bool IsRgtBlocked(int pX, int pY, CollisionPack pCollisionPack);
    bool IsBotBlocked(int pX, int pY, CollisionPack pCollisionPack);
    void CreateLftLine(int pX, int pY, CollisionPack pCollisionPack);
    void CreateTopLine(int pX, int pY, CollisionPack pCollisionPack);
    void CreateRgtLine(int pX, int pY, CollisionPack pCollisionPack);
    void CreateBotLine(int pX, int pY, CollisionPack pCollisionPack);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    uint8_t *CreateDataBuffer(uint32_t &sDataSize);
    void ReadFromFile(VirtualFile *fInfile);

    //--Drawing
    void RenderLines(float pOffsetX, float pOffsetY, float pScale);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

