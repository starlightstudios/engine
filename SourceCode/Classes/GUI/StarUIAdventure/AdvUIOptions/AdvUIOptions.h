///======================================= AdvUIOptions ===========================================
//--UI object that encapsulates the Adventure Options Menu.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AudioManager.h"

///===================================== Local Structures =========================================
#ifndef ADVMENU_OPTIONS_CONSTANTS
#define ADVMENU_OPTIONS_CONSTANTS
typedef struct AdvMenuOptionPack
{
    //--Members
    FlexVal mOldValue;
    FlexVal mCurValue;
    bool mRequiresRestart;

    //--Functions
    void Initialize()
    {
        mOldValue.i = 0;
        mCurValue.i = 0;
        mRequiresRestart = false;
    }
    void SetI(int pValue)
    {
        mOldValue.i = pValue;
        mCurValue.i = pValue;
    }
    void SetF(float pValue)
    {
        mOldValue.f = pValue;
        mCurValue.f = pValue;
    }
    void SetB(bool pValue)
    {
        mOldValue.b = pValue;
        mCurValue.b = pValue;
    }

}AdvMenuOptionPack;

///===================================== Local Definitions ========================================

//--Timers
#define ADVMENU_OPTIONS_VIS_TICKS 15
#define ADVMENU_OPTIONS_ARROW_OSCILLATE_TICKS 120
#define ADVMENU_OPTIONS_CURSOR_MOVE_TICKS 7
#define ADVMENU_OPTIONS_DETAILS_SWITCH_TICKS 7
#define ADVMENU_OPTIONS_UNSAVED_CHANGES_TICKS 7
#define ADVMENU_OPTIONS_REBIND_TICKS 7

//--Sizes
#define ADVMENU_OPTIONS_ARROW_OSCILLATE_DISTANCE 5.0f

//--Icons

//--Modes
#define ADVMENU_OPTIONS_MODE_GAMEPLAY 0
#define ADVMENU_OPTIONS_MODE_AUDIO 1
#define ADVMENU_OPTIONS_MODE_CONTROLS 2
#define ADVMENU_OPTIONS_MODE_DEBUG 3
#define ADVMENU_OPTIONS_MODE_TOTAL 4

//--Gameplay Codes
#define ADVMENU_OPTIONS_GAMEPLAY_DOCTORBAG 0
#define ADVMENU_OPTIONS_GAMEPLAY_HASTEN 1
#define ADVMENU_OPTIONS_GAMEPLAY_TOURISTMODE 2
#define ADVMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR 3
#define ADVMENU_OPTIONS_GAMEPLAY_LIGHTBOOST 4
#define ADVMENU_OPTIONS_GAMEPLAY_SHOWCENSORBARS 5
#define ADVMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES 6
#define ADVMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING 7
#define ADVMENU_OPTIONS_GAMEPLAY_USERAMLOADING 8
#define ADVMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE 9
#define ADVMENU_OPTIONS_GAMEPLAY_USEFADINGUI 10
#define ADVMENU_OPTIONS_GAMEPLAY_EXPERTCOMBATUI 11
#define ADVMENU_OPTIONS_GAMEPLAY_BLOCKSMUSTACTIVATE 12
#define ADVMENU_OPTIONS_GAMEPLAY_SHOWSTAMINABAR 13
#define ADVMENU_OPTIONS_GAMEPLAY_DISALLOW_OVERLAYS 14
#define ADVMENU_OPTIONS_GAMEPLAY_AUTORUN 15
#define ADVMENU_OPTIONS_GAMEPLAY_TOTAL 16

//--Audio Codes
#define ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY 4
#define ADVMENU_OPTIONS_MUSIC_VOLUME 0
#define ADVMENU_OPTIONS_SOUND_VOLUME 1
#define ADVMENU_OPTIONS_VOICELINE_PLAY_CHANCE 2
#define ADVMENU_OPTIONS_CHARVOLUME0 3
#define ADVMENU_OPTIONS_CHARVOLUME1 4
#define ADVMENU_OPTIONS_CHARVOLUME2 5
#define ADVMENU_OPTIONS_CHARVOLUME3 6
#define ADVMENU_OPTIONS_AUDIO_TOTAL 7

#define ADVMENU_OPTION_AUDIO_MAX_PLAYCHANCE_SLOTS 7

//--Controls Codes
#define ADVMENU_OPTIONS_CONTROLS_ACTIVATE 0
#define ADVMENU_OPTIONS_CONTROLS_CANCEL 1
#define ADVMENU_OPTIONS_CONTROLS_RUN 2
#define ADVMENU_OPTIONS_CONTROLS_UP 3
#define ADVMENU_OPTIONS_CONTROLS_RIGHT 4
#define ADVMENU_OPTIONS_CONTROLS_DOWN 5
#define ADVMENU_OPTIONS_CONTROLS_LEFT 6
#define ADVMENU_OPTIONS_CONTROLS_SHOULDERLEFT 7
#define ADVMENU_OPTIONS_CONTROLS_SHOULDERRIGHT 8
#define ADVMENU_OPTIONS_CONTROLS_FIELDABILITIES 9
#define ADVMENU_OPTIONS_CONTROLS_ABILITY_1 10
#define ADVMENU_OPTIONS_CONTROLS_ABILITY_2 11
#define ADVMENU_OPTIONS_CONTROLS_ABILITY_3 12
#define ADVMENU_OPTIONS_CONTROLS_ABILITY_4 13
#define ADVMENU_OPTIONS_CONTROLS_ABILITY_5 14
#define ADVMENU_OPTIONS_CONTROLS_TOTAL 15

//--Debug Control Codes
#define ADVMENU_OPTIONS_DEBUG_OPENMENU 0
#define ADVMENU_OPTIONS_DEBUG_TOGGLEFULLSCREEN 1
#define ADVMENU_OPTIONS_DEBUG_REBUILDKERNING 2
#define ADVMENU_OPTIONS_DEBUG_REBUILDSHADERS 3
#define ADVMENU_OPTIONS_DEBUG_DISABLE_OUTLINE 4
#define ADVMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER 5
#define ADVMENU_OPTIONS_DEBUG_DISABLE_LIGHTING 6
#define ADVMENU_OPTIONS_DEBUG_TOTAL 7

#define ADVMENU_OPTIONS_HELP_MENU_STRINGS 24

#endif // ADVMENU_OPTIONS_CONSTANTS

///========================================== Classes =============================================
class AdvUIOptions : virtual public AdvUICommon
{
    protected:
    ///--[System]
    //--System
    int mMode;
    int mArrowTimer;

    //--Rebinding
    bool mIsRebinding;
    bool mIsRebindingSecondary;
    int mRebindTimer;
    int mRebindLockout;

    //--Unsaved Changes Dialogue
    bool mIsShowingUnsavedChanges;
    int mUnsavedChangesTimer;
    StarlightString *mAcceptString;
    StarlightString *mCancelString;

    //--Cursor
    int mCursor;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Volume Constants
    int mAbstractVolumeCurrentSlots[ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY];
    char mAbstractVolumeCurrentNames[ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY][32];
    char mAbstractVolumeNames[AM_ABSTRACT_VOLUME_TOTAL][32];

    //--Gameplay Values
    AdvMenuOptionPack mAutoUseDoctorBag;    //Boolean
    AdvMenuOptionPack mAutoHastenDialogue;  //Boolean
    AdvMenuOptionPack mTouristMode;         //Boolean
    AdvMenuOptionPack mCombatMemoryCursor;  //Boolean
    AdvMenuOptionPack mAmbientLightBoost;   //Float
    AdvMenuOptionPack mShowCensorBars;      //Boolean
    AdvMenuOptionPack mMaxAutosaves;        //Integer
    AdvMenuOptionPack mAllowAssetStreaming; //Boolean
    AdvMenuOptionPack mUseRAMLoading;       //Boolean
    AdvMenuOptionPack mUnloadActors;        //Boolean
    AdvMenuOptionPack mUseFadingUI;         //Boolean
    AdvMenuOptionPack mExpertCombatUI;      //Boolean
    AdvMenuOptionPack mBlocksMustActivate;  //Boolean
    AdvMenuOptionPack mShowStaminaBar;      //Boolean
    AdvMenuOptionPack mDisallowOverlays;    //Boolean
    AdvMenuOptionPack mAutorun;             //Boolean
    AdvMenuOptionPack mResolution;          //Integer
    AdvMenuOptionPack mCensorIzana;         //Boolean
    AdvMenuOptionPack mCensorCaelyn;        //Boolean
    AdvMenuOptionPack mCensorCyrano;        //Boolean
    AdvMenuOptionPack mCensorEnemies;       //Boolean

    //--Audio Values
    AdvMenuOptionPack mMusicVol;              //Float
    AdvMenuOptionPack mSoundVol;              //Float
    AdvMenuOptionPack mVoicePlay;             //Integer
    AdvMenuOptionPack mAbstractVol0;          //Float
    AdvMenuOptionPack mAbstractVol1;          //Float
    AdvMenuOptionPack mAbstractVol2;          //Float
    AdvMenuOptionPack mAbstractVol3;          //Float
    AdvMenuOptionPack mIzanaVoiceVol;         //Float
    AdvMenuOptionPack mCaelynVoiceVol;        //Float
    AdvMenuOptionPack mCyranoVoiceVol;        //Float
    AdvMenuOptionPack mSaveVolumesInSavefile; //Boolean

    //--Control Values
    //--Debug Values
    AdvMenuOptionPack mDisableShaderOutline;    //Boolean
    AdvMenuOptionPack mDisableShaderUnderwater; //Boolean
    AdvMenuOptionPack mDisableShaderLighting;   //Boolean

    //--Comparison Auto-Handler
    bool mHasUnsavedChanges;
    bool mRequiresRestart;
    StarLinkedList *mrOptionListList; //AdvMenuOptionPack *, reference

    //--Details
    bool mIsShowingDetails;
    int mDetailsTimer;

    //--Strings
    StarlightString *mShowHelpString;
    StarlightString *mInspectString;
    StarlightString *mBindPrimaryString;
    StarlightString *mBindSecondaryString;
    StarlightString *mSetToDefaultString;
    StarlightString *mSetAllDefaultString;
    StarlightString *mPageLftString;
    StarlightString *mPageRgtString;

    //--Constants
    float cOptionTop;
    float cOptionHei;
    float cNameLft;
    float cBoolRgt;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;

        //--Images
        StarBitmap *rArrowLft;
        StarBitmap *rArrowRgt;
        StarBitmap *rButtonDefaults;
        StarBitmap *rButtonOkay;
        StarBitmap *rDescriptionFrame;
        StarBitmap *rExpandableHeader;
        StarBitmap *rFrameDetail;
        StarBitmap *rFrame;
        StarBitmap *rHeader;
        StarBitmap *rOptionHeader;
        StarBitmap *rSliderIndicator;
        StarBitmap *rSlider;
        StarBitmap *rUnsavedChangesFrame;
        StarBitmap *rOverlay_Highlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIOptions();
    virtual ~AdvUIOptions();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void ReresolveOptionStrings();
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void RecheckChanges();
    void SaveOptions();
    void CancelOptions();
    void SetDefaultOptions();
    void SetDefaultControl(int pCursor);
    void SetAllDefaultControls();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();
    void HandleActivatePress(int pMode, int pCursor);
    void HandleRunPress(int pMode, int pCursor);
    void HandleLeftPress(int pMode, int pCursor);
    void HandleRightPress(int pMode, int pCursor);

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    void RenderOptionsGameplay(float pAlpha);
    void RenderOptionsAudio(float pAlpha);
    void RenderOptionsControls(float pAlpha);
    void RenderOptionsDebug(float pAlpha);
    void RenderBoolean(const char *pName, float pY, bool pValue);
    void RenderInteger(const char *pName, float pY, int pValue, int pMaximum);
    void RenderFloat(const char *pName, float pY, float pValue);
    void RenderControl(const char *pName, float pY, const char *pControlName);
    void RenderDetails();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

