///======================================= OpenGL Macros ==========================================
//--Functions that perform OpenGL actions quickly. These are mostly related to shaders.

#pragma once
#if defined _TARGET_OS_LINUX_
    #include "GL/gl.h"
#elif defined _TARGET_OS_WINDOWS_
    #include "GL/gl.h"
#elif defined _TARGET_OS_MAC_
    #include "osxgl.h"
#endif

void ShaderUniform1i(GLint pShaderHandle, GLint pUniformValue, const char *pFormat);
void ShaderUniform1iArg(GLint pShaderHandle, GLint pUniformValue, const char *pFormat, ...);
void ShaderUniform1f(GLint pShaderHandle, GLfloat pUniformValue, const char *pFormat);
void ShaderUniform1fArg(GLint pShaderHandle, GLfloat pUniformValue, const char *pFormat, ...);
void ShaderUniform4f(GLint pShaderHandle, GLfloat pUniformValue0, GLfloat pUniformValue1, GLfloat pUniformValue2, GLfloat pUniformValue3, const char *pFormat);
void ShaderUniform4fArg(GLint pShaderHandle, GLfloat pUniformValue0, GLfloat pUniformValue1, GLfloat pUniformValue2, GLfloat pUniformValue3, const char *pFormat, ...);
