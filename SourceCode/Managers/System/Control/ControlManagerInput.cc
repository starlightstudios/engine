//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

void ControlManager::InformOfPress(int pKeycode, int pMouseButton, int pJoyButton)
{
    ///--[Documentation]
    //--Events will inform of the press type. Overrides previous scancode, and the codes are
    //  wiped at the end of every logic tick.
    mPressedKeyboardKey  = pKeycode;
    mPressedMouseButton  = pMouseButton;
    mPressedJoypadButton = pJoyButton;
    bool tWasAnyKeyPressed = mIsAnyKeyPressed;
    mIsAnyKeyPressed = true;

    //--Debug.
    //fprintf(stderr, "Inform of press %i %i %i\n", pKeycode, pMouseButton, pJoyButton);

    //--Doesn't matter whether this was a keyboard or otherwise, a Thing has been Pressed.
    mIsAnyThingPressed = true;

    ///--[Allegro Version]
    //--If there was a keycode press, add it to the input buffer.
    #if defined _ALLEGRO_PROJECT_
        if(pKeycode > 0 && pKeycode < KEYCODES_MAX)
        {
            //--Store the keycode.
            char tKeycode = mKeycodes[pKeycode].mNormalChar;

            //--Check if the keycode is either Alt or Tab. If it is, it doesn't count as an any-key-pressed case.
            if(pKeycode == ALLEGRO_KEY_ALT || pKeycode == ALLEGRO_KEY_ALTGR || pKeycode == ALLEGRO_KEY_TAB)
            {
                mIsAnyKeyPressed = tWasAnyKeyPressed;
            }

            //--Check if the key is shifted.
            if(rKeyboardStatePtr && (al_key_down(rKeyboardStatePtr, ALLEGRO_KEY_LSHIFT) || al_key_down(rKeyboardStatePtr, ALLEGRO_KEY_RSHIFT)))
            {
                tKeycode = mKeycodes[pKeycode].mShiftedChar;
            }

            //--Only non-NULL keys get added.
            if(tKeycode != '\0')
            {
                SetMemoryData(__FILE__, __LINE__);
                char *nCode = (char *)starmemoryalloc(sizeof(char));
                *nCode = tKeycode;
                mKeyboardInputBuffer->AddElement("X", nCode, &FreeThis);
            }
        }

    ///--[SDL Version]
    //--Unlike the Allegro version, which can store a pointer to the keyboard's whole state, we need
    //  to store individual keypresses and process them during the update tick.
    #elif defined _SDL_PROJECT_

        //--Package.
        SetMemoryData(__FILE__, __LINE__);
        KeypressPack *nPack = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
        nPack->mIsPress = true;
        nPack->mScancode = pKeycode;
        nPack->mMouseButton = pMouseButton;
        nPack->mJoypad = pJoyButton;

        //--Register
        mInputBuffer->AddElement("X", nPack, &FreeThis);

        //--Check if the keycode is either Alt or Tab. If it is, it doesn't count as an any-key-pressed case.
        if(pKeycode == SDL_SCANCODE_LALT || pKeycode == SDL_SCANCODE_RALT || pKeycode == SDL_SCANCODE_TAB)
        {
            mIsAnyKeyPressed = tWasAnyKeyPressed;
        }

        //--Debug
        //fprintf(stderr, "Recorded key press: %i\n", pJoyButton);

    #endif
}
void ControlManager::InformOfRelease(int pKeycode, int pMouseButton, int pJoyButton)
{
    ///--[Documentation]
    //--Called when a key is released. This is only used by SDL, as Allegro stores the state of
    //  the keyboard and just queries it.
    #if defined _SDL_PROJECT_

        //--Package.
        SetMemoryData(__FILE__, __LINE__);
        KeypressPack *nPack = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
        nPack->mIsPress = false;
        nPack->mScancode = pKeycode;
        nPack->mMouseButton = pMouseButton;
        nPack->mJoypad = pJoyButton;

        //--Register
        mInputBuffer->AddElement("X", nPack, &FreeThis);

        //--Debug
        //fprintf(stderr, "Recorded key release: %i\n", pKeycode);

    #endif
}
void ControlManager::InformOfJoyAxis(int pAxis, int16_t pResult)
{
    ///--[Documentation]
    //--SDL only. Informs of an axis event. Axes tend to have events a lot unless they're idle.
    //  Note that at the moment, SDL only supports the 0th joystick.
    #if defined _SDL_PROJECT_

        //--Clamp.
        if(abs(pResult) < 1500) pResult = 128;

        //--Package.
        /*SetMemoryData(__FILE__, __LINE__);
        KeypressPack *nPack = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
        nPack->mIsPress = true;
        nPack->mScancode = -2;
        nPack->mMouseButton = -2;
        nPack->mJoypadValue = pResult;

        //--Start at the axis offset.
        nPack->mJoypad = JOYSTICK_OFFSET_STICK;

        //--Increase by axis count.
        nPack->mJoypad += pAxis * JOYSTICK_AXIS_CONSTANT;

        //--If the value was less than 0, flip the joypad value.
        if(pResult < -600) nPack->mJoypad *= -1;
        mPressedJoypadButton = nPack->mJoypad;

        //--Register
        mInputBuffer->AddElement("X", nPack, &FreeThis);*/

        //--Match pair: Create an opposite stop command so the controls don't jam if the stick suddenly changes.
        //if(abs(pResult) != 128)
        //{
            /*SetMemoryData(__FILE__, __LINE__);
            KeypressPack *nPack2 = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
            nPack2->mIsPress = false;
            nPack2->mScancode = -2;
            nPack2->mMouseButton = -2;
            nPack2->mJoypad = nPack->mJoypad * -1;
            nPack2->mJoypadValue = 128;
            mInputBuffer->AddElement("X", nPack2, &FreeThis);*/
        //}

        //--Debug
        //fprintf(stderr, "Recorded axis event: %i %i\n", pAxis, pResult);
        //fprintf(stderr, " Data: %i - %i\n", nPack->mJoypad, nPack->mIsPress);
        //fprintf(stderr, " Rev:  %i - %i\n", nPack2->mJoypad, nPack2->mIsPress);

        //--Resolve the code based on the stick/axis/position. Note that a press event for the negative
        //  axis will trigger a release event for the positive axis.
        int tNegativeCode = (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * 0) + (JOYSTICK_AXIS_CONSTANT * pAxis) + 0);
        int tPositiveCode = (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * 0) + (JOYSTICK_AXIS_CONSTANT * pAxis) + 1);

        //--Press event for the negative code.
        if(pResult < -10000)
        {
            ControlManager::Fetch()->InformOfPress(-2, -2, tNegativeCode);
            ControlManager::Fetch()->InformOfRelease(-2, -2, tPositiveCode);
        }
        //--Press event for the positive code.
        else if(pResult > 10000)
        {
            ControlManager::Fetch()->InformOfPress(-2, -2, tPositiveCode);
            ControlManager::Fetch()->InformOfRelease(-2, -2, tNegativeCode);
        }
        //--Release for both.
        else
        {
            ControlManager::Fetch()->InformOfRelease(-2, -2, tPositiveCode);
            ControlManager::Fetch()->InformOfRelease(-2, -2, tNegativeCode);
        }

    #endif
}
void ControlManager::InformAllegroOfJoyAxis()
{
    //--Tells Allegro that something happened with the joystick axes this tick.
    #if defined _ALLEGRO_PROJECT_
    mAllegroHasJoystickUpdate = true;
    #endif
}
