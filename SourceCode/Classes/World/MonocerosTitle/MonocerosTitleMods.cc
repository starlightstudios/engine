//--Base
#include "MonocerosTitle.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "ControlManager.h"

///--[Local Definitions]
#define MODTEXT_Y 123.0f
#define MODTEXT_NAMEX 399.0f

///========================================== System ==============================================
///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonocerosTitle::ShowModLoadOrder()
{
    mIsShowingModLoadOrder = true;
}
void MonocerosTitle::MoveModDown(int pIndex)
{
    mModLoadOrderList->MoveEntryTowardsTailI(pIndex);
}
void MonocerosTitle::MoveModUp(int pIndex)
{
    mModLoadOrderList->MoveEntryTowardsHeadI(pIndex);
}
void MonocerosTitle::ToggleModEnable(int pIndex)
{
    //--Get the package at the index.
    FM_ModInfo *rPackage = (FM_ModInfo *)mModLoadOrderList->GetElementBySlot(pIndex);
    if(!rPackage) return;

    //--Toggle.
    rPackage->mIsEnabled = !rPackage->mIsEnabled;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void MonocerosTitle::UpdateModMode()
{
    ///--[Documentation and Setup]
    //--Fast-access Pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Hotkeys]
    //--Pressing escape deactivates loading mode.
    if(rControlManager->IsFirstPress("Escape"))
    {
        mIsShowingModLoadOrder = false;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Mouse Movement]
    //--Get the mouse position. Note that mouse positioning is not associated with cursors as the
    //  user is expected to be dealing with dynamic buttons here.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    ///--[Selection via Mouse]
    //--Clicking.
    if(rControlManager->IsFirstPress("MouseLft") || rControlManager->IsFirstPress("Enter"))
    {
        //--Check the Back button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mBackBtn))
        {
            mIsShowingModLoadOrder = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Check the Save button.
        else if(IsPointWithinWH(tMouseX, tMouseY, 880.0f, 610.0f, 99.0f, 46.0f))
        {
            mModChangesPending = false;
            WriteLoadOrderToFile("Mod Load Order.txt");
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Clicking within the scrollbar.
        else if(IsPointWithinWH(tMouseX, tMouseY, 953.0f, 128.0f, 33.0f, 459.0f))
        {
            mIsClickingScrollbar = true;
            mScrollbarClickStartY = tMouseY;
        }
        //--Check if it's the upload button. This fails silently if the mod is not a
        //  candidate for upload.
        //--This is commented out right now because the workshop uploading stuff doesn't work and
        //  I need help from Valve :3
        else if(tMouseX > 377.0f && tMouseX <= 403.0f)
        {
            /*
            //--Resolve the entry.
            float cYStart = 130.0f;
            int tYRel = tMouseY - cYStart;

            //--Resolve which game index it is. If it's in range, call the upload code using this
            //  mod's name.
            int tModIndex = mModOffset + (tYRel / 41);
            if(tModIndex < mModLoadOrderList->GetListSize())
            {
                const char *rModName = mModLoadOrderList->GetNameOfElementBySlot(tModIndex);
                UploadModToWorkshop(rModName);
            }
            */
        }
        //--Check if it's within the buttons associated with mod entries.
        else if(tMouseX >= 855.0f && tMouseX <= 941.0f)
        {
            //--Resolve which entry it is by checking the Y modulus. The buttons are 27x27
            //  but have 30x41 XxY spacing, so a click between buttons should fail.
            float cYStart = 130.0f;
            float cXStart = 855.0f;
            int tXRel = tMouseX - cXStart;
            int tYRel = tMouseY - cYStart;

            //--We hit an actual button.
            if((tXRel % 30) <= 37 && (tYRel % 41) <= 27)
            {
                //--Resolve which game index it is.
                int tModIndex = mModOffset + (tYRel / 41);
                if(tModIndex < mModLoadOrderList->GetListSize())
                {
                    //--Mod-down button.
                    if(tMouseX < 884)
                    {
                        mModChangesPending = true;
                        MoveModDown(tModIndex);
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Mod-up button.
                    else if(tMouseX < 914)
                    {
                        mModChangesPending = true;
                        MoveModUp(tModIndex);
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Enable checkbox.
                    else
                    {
                        mModChangesPending = true;
                        ToggleModEnable(tModIndex);
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                }
            }
        }
    }

    //--Mouse is not down, stop scrollbar handling.
    if(!rControlManager->IsDown("MouseLft")) mIsClickingScrollbar = false;

    //--Scrollbar handling.
    if(mIsClickingScrollbar)
    {
        //--Constants.
        TwoDimensionReal cBoxDimensions;
        int cEntriesPerPage = MONOTITLE_MODS_PER_PAGE;
        //float cSizePerBox = 137.0f;
        //float cBoxSpacing =  10.0f;
        cBoxDimensions.SetWH(953.0f, 128.0f, 33.0f, 459.0f);

        //--Check the difference between the bar's top and the mouse current position.
        float cAmountPerMove = (1.0f / (float)(mModLoadOrderList->GetListSize() - cEntriesPerPage) * cBoxDimensions.GetHeight()) * 0.7f;
        if(tMouseY - mScrollbarClickStartY < -cAmountPerMove && mModOffset > 0)
        {
            mModOffset --;
            mScrollbarClickStartY = mScrollbarClickStartY - cAmountPerMove;
        }
        else if(tMouseY - mScrollbarClickStartY > cAmountPerMove)
        {
            mModOffset ++;
            if(mModOffset >= mModLoadOrderList->GetListSize() - MONOTITLE_MODS_PER_PAGE)
            {
                mModOffset --;
            }
            else
            {
                mScrollbarClickStartY = mScrollbarClickStartY + cAmountPerMove;
            }
        }
    }

    //--Save the mouse position.
    mPreviousMouseX = tMouseX;
    mPreviousMouseY = tMouseY;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonocerosTitle::RenderModMode()
{
    ///--[Documentation]
    //--Renders mod load order mode, showing a list of mods and allowing the player to move them
    //  around or enable/disable them.
    if(!Images.mIsReady || !MonoImg.mIsReady) return;

    ///--[Background]
    MonoImg.Data.rBacking->Draw();

    ///--[Frame]
    MonoImg.Data.rFrameModLoadOrder->Draw();

    ///--[Buttons]
    MonoImg.Data.rFrameBackButton->Draw();
    MonoImg.Data.rFontMenu->DrawText(mBackBtn.mLft+22.0f, mBackBtn.mTop-2.0f, 0, 0.7f, xrStringData->str.mTextBack);
    MonoImg.Data.rFrameSaveModButton->Draw();
    MonoImg.Data.rFontMenu->DrawText(880+22.0f, 610-2.0f, 0, 0.7f, xrStringData->str.mTextSaveMod);

    ///--[Mod Listing]
    //--Constants
    float cModNameX = 407.0f;
    float cModStartY = 118.0f;
    float cModHei = MonoImg.Data.rFontMenu->GetTextHeight();

    //--Render all of the mods currently in the load order list.
    int i = 0;
    int p = 0;
    FM_ModInfo *rModInfo = (FM_ModInfo *)mModLoadOrderList->PushIterator();
    while(rModInfo)
    {
        //--Skip entries until we reach the starting index.
        if(p < mModOffset)
        {
            p ++;
            rModInfo = (FM_ModInfo *)mModLoadOrderList->AutoIterate();
            continue;
        }

        //--Render the display name.
        MonoImg.Data.rFontMenu->DrawText(cModNameX, cModStartY + (cModHei * i), 0, 1.0f, rModInfo->mDisplayName);

        //--Render the button set.
        MonoImg.Data.rFrameModLoadButtons->Draw(0.0f, (cModHei * i));

        //--If the mod is enabled, render a checkmark.
        if(rModInfo->mIsEnabled)
        {
            MonoImg.Data.rFrameModLoadCheck->Draw(0.0f, (cModHei * i));
        }
        else
        {
            MonoImg.Data.rFrameModLoadX->Draw(0.0f, (cModHei * i));
        }

        //--If the mod can be uploaded, render an upload button. Commented out until I can get it to work.
        if(false)
        {
            MonoImg.Data.rFrameModUploadButton->Draw(0.0f, (cModHei * i));
        }

        //--Stop at 11, the most we can render in one go.
        if(i >= MONOTITLE_MODS_PER_PAGE)
        {
            mModLoadOrderList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rModInfo = (FM_ModInfo *)mModLoadOrderList->AutoIterate();
    }

    ///--[Scrollbar]
    //--If needed, render the scrollbar for the mod count.
    if(mModLoadOrderList->GetListSize() > MONOTITLE_MODS_PER_PAGE)
    {
        RenderScrollbar(mModOffset, MONOTITLE_MODS_PER_PAGE+1, mModLoadOrderList->GetListSize(), 129.0f, 478.0f, true, MonoImg.Data.rScrollbarModsBack, MonoImg.Data.rScrollbarFront);
    }

    ///--[Changes Text]
    //--Tell the player that changes are pending if flagged.
    if(mModChangesPending)
    {
        MonoImg.Data.rFontMenu->DrawText(cModNameX, 620.0f, xrStringData->str.mTextChangesPending);
    }
    ///--[Help Text]
    {
        MonoImg.Data.rFontSaveStat->DrawText(cModNameX, 700.0f, xrStringData->str.mTextChangesModInstruct);
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
