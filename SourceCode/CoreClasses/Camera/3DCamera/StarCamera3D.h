///======================================= StarCamera3D ===========================================
//--Represents a 3D camera. Inherits from the basic camera type and is handled by the CameraManager.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarCamera2D.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class StarCamera3D : public StarCamera2D
{
    private:

    protected:
    //--System
    //--Movement
    float mMoveSpeed;
    float mMoveSpeedMin, mMoveSpeedMax;

    //--Position
    float mMoveStore;
    //Borrows mCoordinates from 2D mode.

    //--Rotations
    float mXRotation, mYRotation, mZRotation;

    //--Flags
    float mMouseSensitivity;
    float mOldMouseX, mOldMouseY, mOldMouseZ;
    bool mNoClip;

    public:
    //--System
    StarCamera3D();
    virtual ~StarCamera3D();

    //--Public Variables
    //--Property Queries
    virtual bool Is2DCamera();
    virtual bool Is3DCamera();
    virtual void Get3DPosition(float &sX, float &sY, float &sZ);
    virtual void Get3DRotation(float &sX, float &sY, float &sZ);

    //--Manipulators
    void SetPosition(float pX, float pY, float pZ);
    void SetRotation(float pX, float pY, float pZ);

    //--Core Methods
    virtual void PositionInWorld();
    virtual void UnpositionInWorld();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    void AttemptMove(bool pAllowSlide, float pXSpeed, float pYSpeed, float pZSpeed);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
