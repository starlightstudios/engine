-- |[ ================================ ObjectPrototype Utility ================================= ]|
--Utility functions like making header strings.

-- |[ ======================== ObjectPrototype:fnCreateBalancedHeader() ======================== ]|
--Given a string, creates and returns a header of the format -- |[ === String === ]|
-- Attemps to even out the ='s to center the header. If one side has to be longer than the other
-- then the right side is always longer.
--Returns the header on success, or " " on failure.
function ObjectPrototype:fnCreateBalancedHeader(psString, piLength)

    -- |[Argument Check]|
    if(psString == nil) then return " " end
    if(piLength == nil) then return " " end

    -- |[Create]|
    --Constants
    local ciSpaceForEdges = 11
    
    --Subtract one from the length. This is because line columns count from 1, not 0, and this creates
    -- a slight misalignment.
    piLength = piLength - 1
    
    --Variables
    local ciStringLen = string.len(psString)
    
    --Basic string if there is just not enough space for any edgings.
    local sBasicHeader = "-- |[" .. psString .. "]|"
    
    --Not enough space provided.
    if(piLength <= ciSpaceForEdges + ciStringLen) then return sBasicHeader end
    
    --There is enough space provided to generate at least one equals on either side. Figure out how
    -- many are needed.
    local iNumberOfEquals = piLength - ciSpaceForEdges - ciStringLen
    local iEqualsPerSide = math.floor(iNumberOfEquals / 2)
    local iModulus = iNumberOfEquals % 2
    local bIsOddNumber = true
    if(iModulus == 0) then bIsOddNumber = false end
    
    --Assemble string.
    local sHeader = "-- |[ "
    for i = 1, iEqualsPerSide, 1 do
        sHeader = sHeader .. "="
    end
    
    --Append string to middle.
    sHeader = sHeader .. " " .. psString .. " "
    
    --Append equals. Add one if there was an odd number.
    if(bIsOddNumber) then iEqualsPerSide = iEqualsPerSide + 1 end
    for i = 1, iEqualsPerSide, 1 do
        sHeader = sHeader .. "="
    end
    
    --Ending.
    sHeader = sHeader .. " ]|"
    
    -- |[Return]|
    return sHeader
end

-- |[ ========================== ObjectPrototype:fnGetJPEnumeration() ========================== ]|
--Creates and returns a string representing the JP amount. If an enumeration is not found, returns
-- the numeric value passed in. Otherwise, uses the JP series in 000 Variables.lua.
--Note: During execution, 000 Variables.lua has not run yet so we mirror the JP values here.
function ObjectPrototype:fnGetJPEnumeration(piJPAmount)
    
    -- |[Argument Check]|
    if(piJPAmount == nil) then return "0" end
    
    -- |[Local Copies]|
    --Local versions of the JP globals.
    local gciJP_Cost_Locked = -1
    local gciJP_Cost_NoCost = 0
    local gciJP_Cost_Cheap = 50
    local gciJP_Cost_Normal = 100
    local gciJP_Cost_Advanced = 150
    local gciJP_Cost_Passive = 300
    local gciJP_Cost_Unique = 500
    
    -- |[Checker]|
    if(piJPAmount == gciJP_Cost_Locked) then
        return "gciJP_Cost_Locked"
    elseif(piJPAmount == gciJP_Cost_NoCost) then
        return "gciJP_Cost_NoCost"
    elseif(piJPAmount == gciJP_Cost_Cheap) then
        return "gciJP_Cost_Cheap"
    elseif(piJPAmount == gciJP_Cost_Normal) then
        return "gciJP_Cost_Normal"
    elseif(piJPAmount == gciJP_Cost_Advanced) then
        return "gciJP_Cost_Advanced"
    elseif(piJPAmount == gciJP_Cost_Passive) then
        return "gciJP_Cost_Passive"
    elseif(piJPAmount == gciJP_Cost_Unique) then
        return "gciJP_Cost_Unique"
    end
    
    -- |[Default]|
    --No special value used, so return the provided value.
    return piJPAmount
end

-- |[ ======================== ObjectPrototype:fnGetDamageEnumeration() ======================== ]|
--Given an integer, resolves the matching enumeration and returns it as a string, or returns the 
-- integer as a string if an enumeration was not found.
function ObjectPrototype:fnGetDamageEnumeration(piDamageFlag)
    
    -- |[Argument Check]|
    if(piDamageFlag == nil) then return "0" end
    
    -- |[Local Copies]|
    local gciDamageType_UseWeapon = -1
    local gciDamageType_Slashing   = 0
    local gciDamageType_Striking   = 1
    local gciDamageType_Piercing   = 2
    local gciDamageType_Flaming    = 3
    local gciDamageType_Freezing   = 4
    local gciDamageType_Shocking   = 5
    local gciDamageType_Crusading  = 6
    local gciDamageType_Obscuring  = 7
    local gciDamageType_Bleeding   = 8
    local gciDamageType_Poisoning  = 9
    local gciDamageType_Corroding  = 10
    local gciDamageType_Terrifying = 11
    
    -- |[Checker]|
    if(piDamageFlag == gciDamageType_UseWeapon) then
        return "gciDamageType_UseWeapon"
    elseif(piDamageFlag == gciDamageType_Slashing) then
        return "gciDamageType_Slashing"
    elseif(piDamageFlag == gciDamageType_Striking) then
        return "gciDamageType_Striking"
    elseif(piDamageFlag == gciDamageType_Piercing) then
        return "gciDamageType_Piercing"
    elseif(piDamageFlag == gciDamageType_Flaming) then
        return "gciDamageType_Flaming"
    elseif(piDamageFlag == gciDamageType_Freezing) then
        return "gciDamageType_Freezing"
    elseif(piDamageFlag == gciDamageType_Shocking) then
        return "gciDamageType_Shocking"
    elseif(piDamageFlag == gciDamageType_Crusading) then
        return "gciDamageType_Crusading"
    elseif(piDamageFlag == gciDamageType_Obscuring) then
        return "gciDamageType_Obscuring"
    elseif(piDamageFlag == gciDamageType_Bleeding) then
        return "gciDamageType_Bleeding"
    elseif(piDamageFlag == gciDamageType_Poisoning) then
        return "gciDamageType_Poisoning"
    elseif(piDamageFlag == gciDamageType_Corroding) then
        return "gciDamageType_Corroding"
    elseif(piDamageFlag == gciDamageType_Terrifying) then
        return "gciDamageType_Terrifying"
    end
    
    -- |[Default]|
    --No special value used, so return the provided value.
    return piDamageFlag
end

-- |[ ======================== ObjectPrototype:fnGetDamageEnumeration() ======================== ]|
--Given a damage type, returns a matching animation string. Returns "Weapon" if a match is not found.
function ObjectPrototype:fnGetDamageAnimFromType(piDamageFlag)
    
    -- |[Argument Check]|
    if(piDamageFlag == nil) then return "Weapon" end
    
    -- |[Local Copies]|
    local gciDamageType_UseWeapon = -1
    local gciDamageType_Slashing   = 0
    local gciDamageType_Striking   = 1
    local gciDamageType_Piercing   = 2
    local gciDamageType_Flaming    = 3
    local gciDamageType_Freezing   = 4
    local gciDamageType_Shocking   = 5
    local gciDamageType_Crusading  = 6
    local gciDamageType_Obscuring  = 7
    local gciDamageType_Bleeding   = 8
    local gciDamageType_Poisoning  = 9
    local gciDamageType_Corroding  = 10
    local gciDamageType_Terrifying = 11
    
    -- |[Checker]|
    if(piDamageFlag == gciDamageType_UseWeapon) then
        return "Weapon"
    elseif(piDamageFlag == gciDamageType_Slashing) then
        return "Claw Slash"
    elseif(piDamageFlag == gciDamageType_Striking) then
        return "Strike"
    elseif(piDamageFlag == gciDamageType_Piercing) then
        return "Pierce"
    elseif(piDamageFlag == gciDamageType_Flaming) then
        return "Flames"
    elseif(piDamageFlag == gciDamageType_Freezing) then
        return "Freeze"
    elseif(piDamageFlag == gciDamageType_Shocking) then
        return "Shock"
    elseif(piDamageFlag == gciDamageType_Crusading) then
        return "Light"
    elseif(piDamageFlag == gciDamageType_Obscuring) then
        return "ShadowA"
    elseif(piDamageFlag == gciDamageType_Bleeding) then
        return "Bleed"
    elseif(piDamageFlag == gciDamageType_Poisoning) then
        return "Poison"
    elseif(piDamageFlag == gciDamageType_Corroding) then
        return "Corrode"
    elseif(piDamageFlag == gciDamageType_Terrifying) then
        return "Terrify"
    end
    
    -- |[Default]|
    --No special value used, so return weapon type.
    return "Weapon"
end

-- |[ ============================== ObjectPrototype:fnNospace() =============================== ]|
--Given a string, returns that string with spaces removed. Used because lua variables aren't allowed
-- to have spaces but display strings are.
function ObjectPrototype:fnNospace(psString)

    -- |[Argument Check]|
    if(psString == nil) then return "" end

    -- |[Scan]|
    local sNewString = ""
    local iLen = string.len(psString)
    for i = 1, iLen, 1 do
        local sLetter = string.sub(psString, i, i)
        if(sLetter ~= " ") then
            sNewString = sNewString .. sLetter
        end
    end

    -- |[Finish]|
    return sNewString
end

-- |[ ========================= ObjectPrototype:fnGenerateDotStrings() ========================= ]|
--Generates and returns the three strings needed for DoT application and tags. Typically, a DoT
-- will show something like "Bleeding!" when a target is hit with a bleed. This function takes
-- in a damage type and returns the needed strings.
function ObjectPrototype:fnGenerateDotStrings(piDamageFlag)

    -- |[Argument Check]|
    if(piDamageFlag == nil) then return "", "", "" end
    
    -- |[Local Copies]|
    local gciDamageType_UseWeapon = -1
    local gciDamageType_Slashing   = 0
    local gciDamageType_Striking   = 1
    local gciDamageType_Piercing   = 2
    local gciDamageType_Flaming    = 3
    local gciDamageType_Freezing   = 4
    local gciDamageType_Shocking   = 5
    local gciDamageType_Crusading  = 6
    local gciDamageType_Obscuring  = 7
    local gciDamageType_Bleeding   = 8
    local gciDamageType_Poisoning  = 9
    local gciDamageType_Corroding  = 10
    local gciDamageType_Terrifying = 11
    
    -- |[Listing]|
    if(piDamageFlag == gciDamageType_Slashing) then
        return "Sliced!", "Badly Sliced!", "Slash Standard"
    elseif(piDamageFlag == gciDamageType_Striking) then
        return "Shaking!", "Badly Shaking!", "Strike Standard"
    elseif(piDamageFlag == gciDamageType_Piercing) then
        return "Pierced!", "Badly Pierced!", "Pierce Standard"
    elseif(piDamageFlag == gciDamageType_Flaming) then
        return "Burned!", "Badly Burned!", "Flame Standard"
    elseif(piDamageFlag == gciDamageType_Freezing) then
        return "Frozen!", "Badly Frozen!", "Freeze Standard"
    elseif(piDamageFlag == gciDamageType_Shocking) then
        return "Fried!", "Badly Fried!", "Shock Standard"
    elseif(piDamageFlag == gciDamageType_Crusading) then
        return "Exposed!", "Badly Exposed!", "Crusade Standard"
    elseif(piDamageFlag == gciDamageType_Obscuring) then
        return "Shrouded!", "Badly Shrouded!", "Obscure Standard"
    elseif(piDamageFlag == gciDamageType_Bleeding) then
        return "Bleeding!", "Badly Bleeding!", "Bleed Standard"
    elseif(piDamageFlag == gciDamageType_Poisoning) then
        return "Poisoned!", "Badly Poisoned!", "Poison Standard"
    elseif(piDamageFlag == gciDamageType_Corroding) then
        return "Corroded!", "Badly Corroded!", "Corrode Standard"
    elseif(piDamageFlag == gciDamageType_Terrifying) then
        return "Frightened!", "Badly Frightened!", "Terrify Standard"
    end
    
    -- |[Error]|
    io.write("Warning, ObjectPrototype:fnGenerateDotStrings() - no damage flag " .. piDamageFlag .. " found. Failing.\n")
    return "", "", ""
end

-- |[ ======================= ObjectPrototype:fnGetFloatWithPrecision() ======================== ]|
--Given a number, returns a string with that number represented as a floating point using printf.
-- Strips off the last zeroes until a non-zero is found or we reach two digits of precision.
--Returns the number as a string.
function ObjectPrototype:fnGetFloatWithPrecision(pfFloat)

    -- |[Argument Check]|
    if(pfFloat == nil) then return "-1.00" end
    
    -- |[Generate]|
    local sString = string.format("%f", pfFloat)
    local iLen = string.len(sString)
    
    -- |[Strip]|
    --Locate where the period is. This sets the bound for the precision.
    local iPeriod = -1
    local iLastNonZero = -1
    for i = 1, iLen, 1 do
        local sLetter = string.sub(sString, i, i)
        if(sLetter == ".") then
            iPeriod = i
        end
        if(sLetter ~= "0" and iPeriod ~= -1) then
            iLastNonZero = i
        end
    end
    
    --No period detected. Fail.
    if(iPeriod == -1) then return "-1.00" end
    
    --If the last non-zero number is 2 or fewer after the decimal, set it to 2. We expect a minimum
    -- default precision of 2.
    if(iLastNonZero < iPeriod + 2) then iLastNonZero = iPeriod + 2 end
    
    --Copy.
    local sFinalString = string.sub(sString, 1, iLastNonZero)
    return sFinalString
end

-- |[ ========================= ObjectPrototype:fnGetStatEnumeration() ========================= ]|
--Given an integer, returns a string representing the stat enumeration variable. This is the string
-- that appears in a statmod string, which is "Atk|Flat|10", as "Atk"
-- The available stats are seen on gzaModTable in 000 Variables.lua
function ObjectPrototype:fnGetStatEnumeration(piStatFlag)
    
    -- |[Argument Check]|
    if(piStatFlag == nil) then return "Error" end
    
    -- |[Local Copies]|
    local zaModTable = {}
    zaModTable[ 1] = {"HPMax",      "gciStatIndex_HPMax",            "[Hlt]"}
    zaModTable[ 2] = {"MPMax",      "gciStatIndex_MPMax",            "[Man]"}
    zaModTable[ 3] = {"MPReg",      "gciStatIndex_MPRegen",          "[MPT]"}
    zaModTable[ 4] = {"CPMax",      "gciStatIndex_CPMax",            "[Cmbp]"}
    zaModTable[ 5] = {"FAMax",      "gciStatIndex_FreeActionMax",    "[Adrn]"}
    zaModTable[ 6] = {"FAGen",      "gciStatIndex_FreeActionGen",    "[Adrn]"}
    zaModTable[ 7] = {"Atk",        "gciStatIndex_Attack",           "[Atk]"}
    zaModTable[ 8] = {"Ini",        "gciStatIndex_Initiative",       "[Ini]"}
    zaModTable[ 9] = {"Acc",        "gciStatIndex_Accuracy",         "[Acc]"}
    zaModTable[10] = {"Evd",        "gciStatIndex_Evade",            "[Evd]"}
    zaModTable[11] = {"Prt",        "gciStatIndex_Protection",       "[Prt]"}
    zaModTable[12] = {"ResSls",     "gciStatIndex_Resist_Slash",     "[Slsh]"}
    zaModTable[13] = {"ResStk",     "gciStatIndex_Resist_Strike",    "[Stk]"}
    zaModTable[14] = {"ResPrc",     "gciStatIndex_Resist_Pierce",    "[Prc]"}
    zaModTable[15] = {"ResFlm",     "gciStatIndex_Resist_Flame",     "[Flm]"}
    zaModTable[16] = {"ResFrz",     "gciStatIndex_Resist_Freeze",    "[Frz]"}
    zaModTable[17] = {"ResShk",     "gciStatIndex_Resist_Shock",     "[Shk]"}
    zaModTable[18] = {"ResCru",     "gciStatIndex_Resist_Crusade",   "[Cru]"}
    zaModTable[19] = {"ResObs",     "gciStatIndex_Resist_Obscure",   "[Obs]"}
    zaModTable[20] = {"ResBld",     "gciStatIndex_Resist_Bleed",     "[Bld]"}
    zaModTable[21] = {"ResPsn",     "gciStatIndex_Resist_Poison",    "[Psn]"}
    zaModTable[22] = {"ResCrd",     "gciStatIndex_Resist_Corrode",   "[Crd]"}
    zaModTable[23] = {"ResTfy",     "gciStatIndex_Resist_Terrify",   "[Tfy]"}
    zaModTable[24] = {"StunCap",    "gciStatIndex_StunCap",          "[Stun]"}
    zaModTable[25] = {"ThreatMult", "gciStatIndex_ThreatMultiplier", "[Threat]"}
    
    -- |[Checker]|
    if(piStatFlag < 1 or piStatFlag > 25) then
        return "Error"
    end
    
    --Otherwise, return the string in the first slot.
    return zaModTable[piStatFlag][1]
end

-- |[ ===================== ObjectPrototype:fnGetStatModifierEnumeration() ===================== ]|
--Given an integer, returns a string representing the stat modifier enumeration variable. This is the string
-- that appears in a statmod string, which is "Atk|Flat|10", as "Flat"
function ObjectPrototype:fnGetStatModifierEnumeration(piStatFlag)
    
    -- |[Argument Check]|
    if(piStatFlag == nil) then return "Error" end
    
    -- |[Check]|
    if(piStatFlag == 0) then
        return "Flat"
    elseif(piStatFlag == 1) then
        return "Pct"
    elseif(piStatFlag == 2) then
        return "FlatSev"
    end
    
    -- |[Error]|
    return "Error"
end

-- |[ ============================ ObjectPrototype:fnRespaceLine() ============================= ]|
--Given a string and a set of spacing integers in an array, respaces the entries on the line by
-- inserting spaces, then returns the string.
--As an example, the string "Dogs, Cats, 1, 10" given -4, -5, 3, 1 becomes
--Dogs, Cats,    1, 10
--Negative numbers mean to left-align the string and add excess spaces to the next entry.
function ObjectPrototype:fnRespaceLine(psString, piaLengths)

    -- |[Argument Check]|
    if(psString   == nil) then return " " end
    if(piaLengths == nil) then return " " end

    -- |[Iteration]|
    --First, create a table that contains the string sans all whitespace.
    local iLen = string.len(psString)
    local saNoWhitespace = {}
    for i = 1, iLen, 1 do
        local sLetter = string.sub(psString, i, i)
        if(sLetter ~= " ") then
            table.insert(saNoWhitespace, sLetter)
        end
    end
    
    --Create a set of strings by using commas as delimiters.
    local sCurrentString = ""
    local saTableOfStrings = {}
    for i = 1, #saNoWhitespace, 1 do
        if(saNoWhitespace[i] == ",") then
            table.insert(saTableOfStrings, sCurrentString)
            sCurrentString = ""
        else
            sCurrentString = sCurrentString .. saNoWhitespace[i]
        end
    end

    --If there is anything left of the last string, insert it.
    if(sCurrentString ~= "") then
        table.insert(saTableOfStrings, sCurrentString)
    end

    -- |[Assembly]|
    --Iterate across the array.
    local bWasLastNegative = false
    local sFinalString = ""
    for i = 1, #saTableOfStrings, 1 do

        --Comma check. Commas do not get appended to the last entry.
        local bHasAppendedComma = false
        if(i == #saTableOfStrings) then bHasAppendedComma = true end

        --Get the matching length value. If out of range, use 0.
        local iLengthVal = 0
        if(i <= #piaLengths) then iLengthVal = piaLengths[i] end
        
        --If this is a negative number, the previous was positive, and it's not the first print, then add a space. This is for formatting.
        if(iLengthVal < 0 and bWasLastNegative == false and i > 1) then
            sFinalString = sFinalString .. " "
        end

        --If it's zero, print the string as-is.
        if(iLengthVal == 0) then
            bWasLastNegative = false
            sFinalString = sFinalString .. saTableOfStrings[i]
        
        --If it's positive, this is a right-aligned string. Add spaces to the left.
        elseif(iLengthVal > 0) then
        
            --Length of this string.
            local iThisLen = string.len(saTableOfStrings[i])
            
            --Append spaces to the left. Note that if the string is longer than the space allocated then no spaces are added.
            for p = iThisLen, iLengthVal, 1 do
                sFinalString = sFinalString .. " "
            end

            --Append the string.
            bWasLastNegative = false
            sFinalString = sFinalString .. saTableOfStrings[i]
    
        --Negative, the string is left-aligned.
        else
        
            --Flip.
            bWasLastNegative = true
            iLengthVal = iLengthVal * -1

            --Append the string.
            sFinalString = sFinalString .. saTableOfStrings[i]
            if(bHasAppendedComma == false) then
                bHasAppendedComma = true
                sFinalString = sFinalString .. ","
            end
        
            --Length of this string.
            local iThisLen = string.len(saTableOfStrings[i])
            
            --Append spaces to the right. Note that if the string is longer than the space allocated then no spaces are added.
            for p = iThisLen, iLengthVal - 1, 1 do
                sFinalString = sFinalString .. " "
            end
        end

        --If no comma has been added, add one now.
        if(bHasAppendedComma == false) then
            sFinalString = sFinalString .. ","
        end
    end
    
    -- |[Finish]|
    return sFinalString
end