///==================================== Deletion Functions ========================================
//--These are general-purpose functions for freeing memory. This header includes primitive-type
//  free functions. To delete classes, a DeletionfunctionPtr should be created to handle the
//  specific type.

#pragma once

#include "Definitions.h"

void FreeThis(void *pPtr);
void DontDeleteThis(void *pPtr);
void FreeDubChar(void *pPtr);
void DubFreeDubChar(void *pPtr);
