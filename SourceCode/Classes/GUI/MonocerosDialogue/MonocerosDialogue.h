///===================================== MonocerosDialogue ========================================
//--Derived class of the WorldDialogue class. Changes rendering properties but is largely unaffected
//  otherwise. As the name suggests, used for Project Monoceros.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "WorldDialogue.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class MonocerosDialogue : public WorldDialogue
{
    private:
    ///--[System]
    ///--[Strings]
    char *mTextTopics;

    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontNameplate;
            StarFont *rFontMajorMainline;
            StarFont *rFontInfoMainline;
            StarFont *rFontTopicHeader;
            StarFont *rFontTopic;

            //--Images
            StarBitmap *rFrameDecision;
            StarBitmap *rFrameDialogue;
            StarBitmap *rFrameInfo;
            StarBitmap *rOverlayNameplate;
            StarBitmap *rOverlayTopicShade;
            StarBitmap *rOverlayTopicSelected;
            StarBitmap *rOverlayTopicUnselected;
        }Data;
    }MonoImg;

    protected:

    public:
    //--System
    MonocerosDialogue();
    virtual ~MonocerosDialogue();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    virtual float ComputeLengthOf(StarLinkedList *pListOfCharacters);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void UpdateTopicsMode();

    //--File I/O
    //--Drawing
    virtual void Render();
    virtual void RenderOnlyText(float pX, float pY, float pAlphaFactor);
    virtual void RenderMajorSequence();
    virtual void RenderDecisions();
    virtual void RenderTopicsMode();
    virtual void RenderScenesMode();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


