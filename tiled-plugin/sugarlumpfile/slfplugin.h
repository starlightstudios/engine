/*
 * SLF Tiled Plugin.
 * SaltyJustice 2015.


#ifndef SLFPLUGIN_H
#define SLFPLUGIN_H

#include "slf_global.h"

#include "gidmapper.h"
#include "mapwriterinterface.h"

#include <QDir>
#include <QObject>
#define _CRT_SECURE_NO_WARNINGS

namespace Tiled {
class MapObject;
class ObjectGroup;
class Properties;
class TileLayer;
class Tileset;
}

namespace SLF {


//--This class allows the export of existing Tiled information as an SLF binary file. These
//  are designed and optimized for SugarCube engine games.
class SLFSHARED_EXPORT SLFPlugin : public QObject,
                                   public Tiled::MapWriterInterface
{
    Q_OBJECT
    Q_INTERFACES(Tiled::MapWriterInterface)
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "org.mapeditor.MapReaderInterface" FILE "plugin.json")
#endif

public:
    SLFPlugin();

    // MapWriterInterface
    bool write(const Tiled::Map *map, const QString &fileName);
    QString nameFilter() const;
    QString errorString() const;

private:
    void ExportTo(const char *pPath, const Tiled::Map *pMap);
    //void writeMap(LuaTableWriter &, const Tiled::Map *);
    //void writeProperties(LuaTableWriter &, const Tiled::Properties &);
    //void writeTileset(LuaTableWriter &, const Tiled::Tileset *, unsigned firstGid);
    //void writeTileLayer(LuaTableWriter &, const Tiled::TileLayer *);
    //void writeObjectGroup(LuaTableWriter &, const Tiled::ObjectGroup *,
    //                      const QByteArray &key = QByteArray());
    //void writeImageLayer(LuaTableWriter &, const Tiled::ImageLayer *);
    //void writeMapObject(LuaTableWriter &, const Tiled::MapObject *);

    QString mError;
    QDir mMapDir;     // The directory in which the map is being saved
    Tiled::GidMapper mGidMapper;
};

} // namespace SLF

#endif // SLFPLUGIN_H
*/

//--[SLF Exporter Plugin]
//--SaltyJustice, 2020. Written for Starlight Engine integration.
//  Contact: saltyjustice@starlightstudios.org
//  Redistribution follows CC 3.0.
//--This is largely modified from the format of the lua plugin.

#pragma once

#include "slf_global.h"

#include "gidmapper.h"
#include "plugin.h"
#include "mapformat.h"
#include "tilesetformat.h"
#include <QDir>

namespace Slf {

class SLFSHARED_EXPORT SlfPlugin : public Tiled::Plugin
{
    Q_OBJECT
    Q_INTERFACES(Tiled::Plugin)
    Q_PLUGIN_METADATA(IID "org.mapeditor.Plugin" FILE "plugin.json")

public:
    void initialize() override;
};


class SLFSHARED_EXPORT SlfMapFormat : public Tiled::WritableMapFormat
{
    Q_OBJECT

public:
    explicit SlfMapFormat(QObject *parent = nullptr)
        : WritableMapFormat(parent)
    {}

    bool write(const Tiled::Map *map, const QString &fileName, Options options) override;
    void ExportTo(const char *pPath, const Tiled::Map *pMap);

    QString nameFilter() const override;
    QString shortName() const override;
    QString errorString() const override;

protected:
    QString mError;
    QDir mMapDir;     // The directory in which the map is being saved
    Tiled::GidMapper mGidMapper;
};

}
