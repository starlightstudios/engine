///================================ Structure Definition File =====================================
///--[Forward Declarations]
#include "Definitions.h"
#include "AdvCombatDefStruct.h"

///--[CarnationAction]
//--Because the party does not act immediately after the player selects their action, this stores the
//  action and intended target for when the character's turn comes up.
typedef struct CarnationAction
{
    //--Variables
    AdvCombatEntity *rOwner;
    AdvCombatAbility *rAbility;
    TargetCluster *rTarget;

    //--Functions
    void Initialize()
    {
        rOwner = NULL;
        rAbility = NULL;
        rTarget = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        CarnationAction *rPtr = (CarnationAction *)pPtr;
        TargetCluster::DeleteThis(rPtr->rTarget);
        free(rPtr);
    }
}CarnationAction;

///--[CarnCombatBGLayer]
//--Represents a layer of the background in CarnationCombat.
typedef struct CarnCombatBGLayer
{
    //--Variables
    int mLogicType;
    float mLogicFactor;
    StarBitmap *rImage;

    //--Functions
    void Initialize()
    {
        mLogicType = 0;
        mLogicFactor = 1.0f;
        rImage = NULL;
    }
}CarnCombatBGLayer;

///--[CarnLevelUp]
//--Structure that holds level-up information. Display-only.
typedef struct CarnLevelUp
{
    //--Variables
    int mLevel;
    int mHPMax;
    int mSPMax;
    int mAtk;
    int mMAtk;
    int mDef;
    int mMDef;
    int mIni;
    int mLck;
    int mEnd;
    int mInt;
    int mSpd;

    //--Previous
    int mOldLevel;
    int mOldHPMax;
    int mOldSPMax;
    int mOldAtk;
    int mOldMAtk;
    int mOldDef;
    int mOldMDef;
    int mOldIni;
    int mOldLck;
    int mOldEnd;
    int mOldInt;
    int mOldSpd;

    //--Functions
    void Initialize()
    {
        mHPMax = 0;
        mSPMax = 0;
        mAtk = 0;
        mMAtk = 0;
        mDef = 0;
        mMDef = 0;
        mIni = 0;
        mLck = 0;
        mEnd = 0;
        mInt = 0;
        mSpd = 0;

        mOldHPMax = 0;
        mOldSPMax = 0;
        mOldAtk = 0;
        mOldMAtk = 0;
        mOldDef = 0;
        mOldMDef = 0;
        mOldIni = 0;
        mOldLck = 0;
        mOldEnd = 0;
        mOldInt = 0;
        mOldSpd = 0;
    }
}CarnLevelUp;

///--[CarnButton]
//--Represents a button in combat. The button always stores the cursor index it represents, as well
//  as a hitbox for where it is and other timers/strings as needed for display.
typedef struct CarnButton
{
    //--Members
    int mIndex;
    char *mDisplayString;
    TwoDimensionReal mPosition;

    //--Methods
    void Initialize()
    {
        mIndex = -1;
        mDisplayString = NULL;
        mPosition.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        CarnButton *rPtr = (CarnButton *)pPtr;
        free(rPtr->mDisplayString);
        free(rPtr);
    }
}CarnButton;
