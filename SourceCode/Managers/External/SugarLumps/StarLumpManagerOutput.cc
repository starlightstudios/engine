//--Base
#include "StarLumpManager.h"

//--Classes
//--CoreClasses
#include "StarArray.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///--[ ==================================== Worker Functions ==================================== ]
int RootLumpComparator(const void *pLumpA, const void *pLumpB)
{
    RootLump *rLumpA = (RootLump *)pLumpA;
    RootLump *rLumpB = (RootLump *)pLumpB;
    return strcmp(rLumpA->mLookup.mName, rLumpB->mLookup.mName);
}

///--[ ===================================== Class Methods ====================================== ]
void StarLumpManager::RegisterLump(RootLump *pLump)
{
    ///--[Documentation]
    //--Registers a lump passed to it. Note that the address data doesn't matter yet. You may use
    //  this if a file was read off the hard drive to append, but be sure to use the MergeLumps()
    //  call before you write it, otherwise the lists will not be merged.
    //--This function takes ownership of the lump AND its attendant data.
    if(!pLump || !pLump->mLookup.mName) return;
    mNewLumpList->AddElement(pLump->mLookup.mName, pLump, &DeleteRootLump);
}
void StarLumpManager::MergeLumps()
{
    ///--[Documentation]
    //--Sometimes, both the mNewLumpList and the mLumpList are in use, such as when opening a SLF
    //  file for append. In these cases, the lists must be merged. Note that they are not sorted.
    //--The mLumpList will take ownership of all the lumps at this time, and the mNewLumpList is cleared.
    if(mNewLumpList->GetListSize() < 1) return;
    mNewLumpList->SetRandomPointerToHead();

    //--Reallocate to add more space.
    int tNewLumpTotal = mTotalLumps + mNewLumpList->GetListSize();
    mLumpList = (RootLump *)realloc(mLumpList, sizeof(RootLump) * tNewLumpTotal);

    //--The old lumps are in place, liberate and add the new ones.
    int tCounter = mTotalLumps;
    mNewLumpList->SetRandomPointerToHead();
    RootLump *rLiberatedLump = (RootLump *)mNewLumpList->LiberateRandomPointerEntry();
    while(rLiberatedLump)
    {
        memcpy(&mLumpList[tCounter], rLiberatedLump, sizeof(RootLump));
        free(rLiberatedLump);
        tCounter ++;

        mNewLumpList->SetRandomPointerToHead();
        rLiberatedLump = (RootLump *)mNewLumpList->LiberateRandomPointerEntry();
    }

    //--All is said and done, the list should be clear here.
    mTotalLumps = tNewLumpTotal;
}
void StarLumpManager::Write(const char *pPath)
{
    ///--[Documentation]
    //--Opens the given path and writes a .slf out, using the locally stored lump data. Data will
    //  be held until a Clear() action is received, so the same file can be saved multiple times.
    if(!pPath) return;

    ///--[Setup]
    //--Open and check for write protection
    FILE *fOutfile = fopen(pPath, "wb");
    if(!fOutfile)
    {
        DebugManager::ForcePrint("[StarLumpManager] - Error opening %s for writing\n", pPath);
        return;
    }

    ///--[Headers and Lookups]
    //--Setup. This cursor tracks how many bytes have been written to the file.
    uint64_t tFileCursor = 0;

    //--Write the header to the file.
    char tHeader[HEADER_MAX_LEN];
    strcpy(tHeader, STANDARD_HEADER);
    fwrite(tHeader, sizeof(char), strlen(tHeader), fOutfile);
    tFileCursor += (sizeof(char) * strlen(tHeader));

    //--Write total number of Lumps.
    fwrite(&mTotalLumps, sizeof(uint32_t), 1, fOutfile);
    tFileCursor += (sizeof(uint32_t) * 1);
    //fprintf(stderr, "Total number of lumps written: %i\n", mTotalLumps);

    //--Sort the list.
    qsort(mLumpList, mTotalLumps, sizeof(RootLump), &RootLumpComparator);
    //fprintf(stderr, "Sorted lump list.\n");

    //--Print the list to the console.
    //fprintf(stderr, "Dumping lump list:\n");
    //for(int i = 0; i < (int)mTotalLumps; i ++)
    //{
    //    fprintf(stderr, " %i: %s %i\n", i, mLumpList[i].mLookup.mName, mLumpList[i].mDataSize);
    //}

    //--Write the name of each Lump, and leave space for it's 64-bit address in the file.
    uint64_t tAddressStart = tFileCursor;
    for(int i = 0; i < (int)mTotalLumps; i ++)
    {
        //--Get the name
        const char *rName = mLumpList[i].mLookup.mName;
        uint8_t tLen = strlen(rName);

        //--Write it!
        fwrite(&tLen, sizeof(uint8_t), 1, fOutfile);
        fwrite(rName, sizeof(char), tLen, fOutfile);
        tFileCursor += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

        //--Write the address to pad. It will be 0 at this point.
        mLumpList[i].mLookup.mPosition = 0;
        fwrite(&mLumpList[i].mLookup.mPosition, sizeof(uint64_t), 1, fOutfile);
        tFileCursor += (sizeof(uint64_t) * 1);
    }
    //fprintf(stderr, "Wrote lump names.\n");

    ///--[Lump Data]
    //--Now write each lump.
    for(int i = 0; i < (int)mTotalLumps; i ++)
    {
        //--Debug.
        //fprintf(stderr, " Writing %i\n", i);

        //--Store the lookup position in the object.
        mLumpList[i].mLookup.mPosition = tFileCursor;
        //fprintf(stderr, " Set position\n");

        //--If the memory is stored in a data pointer, write it directly.
        if(!mLumpList[i].mUsesStarArray)
        {
            fwrite(mLumpList[i].mData, sizeof(uint8_t), mLumpList[i].mDataSize, fOutfile);
        }
        //--Memory is stored in a StarArray object.
        else
        {
            mLumpList[i].rDataObject->WriteFileFromMemory(mLumpList[i].mStarArrayOffset, mLumpList[i].mDataSize, fOutfile);
        }

        //--Advance the file cursor by the size of the data.
        tFileCursor += (sizeof(uint8_t) * mLumpList[i].mDataSize);
        //fprintf(stderr, " Completed %i\n", i);
    }
    //fprintf(stderr, "Wrote lumps.\n");

    ///--[Lump Addresses]
    //--Go back to each lump's address, and rewrite the data. This time the address will be valid.
    fseek(fOutfile, tAddressStart, SEEK_SET);
    for(int i = 0; i < (int)mTotalLumps; i ++)
    {
        //--Get the name
        const char *rName = mLumpList[i].mLookup.mName;
        uint8_t tLen = strlen(rName);

        //--Write it!
        fwrite(&tLen, sizeof(uint8_t), 1, fOutfile);
        fwrite(rName, sizeof(char), tLen, fOutfile);
        tFileCursor += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

        //--Write the address to pad. It will be 0 at this point.
        fwrite(&mLumpList[i].mLookup.mPosition, sizeof(uint64_t), 1, fOutfile);
        tFileCursor += (sizeof(uint64_t) * 1);
    }
    //fprintf(stderr, "Went back and rewrote lump addresses.\n");

    ///--[Finish Up]
    //--Clean up.
    fclose(fOutfile);
    //fprintf(stderr, "Finished.\n");
}
