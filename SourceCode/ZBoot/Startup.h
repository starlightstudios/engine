///========================================== Startup ============================================
//--Functions that are required for program startup, initializing variables and libraries.
//  The boot sequence should be called in order from top to bottom, with the exception of the
//  Lua registration going when the LM is created, but being listed last.

#pragma once

///--[Includes]
#include "Definitions.h"

///--[Definition]
#define STANDARD_FPS 60.0f

///--[Boot Sequence]
void InitializeGlobals();
void RegisterGames(); //Implemented in local project
void HandleCommandLine(int pArgsTotal, char **pArgs);
bool InitializeLibraries();
bool InitializeManagers();
#if defined _SDL_PROJECT_
#elif defined _ALLEGRO_PROJECT_
void RegisterEvents(ALLEGRO_EVENT_QUEUE *EQ);
#endif
void LoadResources();
void PostProcess();
void ProgramBoot();

///--[Lua Function Registration]
void RegisterCoreLuaFunctions(lua_State *pLuaState);    //--Core Lua
void RegisterEngineLuaFunctions(lua_State *pLuaState);  //--Engine Lua

///--[Translation Registration]
void RegisterCoreStrings();                             //--Core Strings
void RegisterEngineStrings();                           //--Engine Strings

///--[Program Boot]
void ProgramFlexibleBoot();
void ProgramFlexibleResources();

//--SDL Callback for the loop timer.
#if defined _SDL_PROJECT_
uint32_t Timer_Callback(uint32_t pInterval, void *pParameters);
#endif
