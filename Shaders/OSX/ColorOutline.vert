//--[Color Outline]
//--Creates an outline around a sprite. The outline's RGBA values should be provided to the shader when it is run.
void main(void)
{
    //--Provide the current texture coordinates for the fragment half of the
    //  shader.
    gl_TexCoord[0] = gl_MultiTexCoord0;

    //--Standard transformation.
    gl_Position = ftransform();
}
