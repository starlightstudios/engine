//--Base
#include "Definitions.h"
#include "Structures.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
//--Libraries
//--Managers

void RenderScrollbar(int pSkip, int pItemsPerPage, int pMaxOffset, float pScrollTop, float pScrollHei, bool pStaticIsFront, StarBitmap *pScroller, StarBitmap *pStatic)
{
    ///--[Documentation]
    //--Renders a scrollbar using the given image with the given properties. Uses a provided front/back both to render
    //  and to resolve scrollbar sizes.
    //--The back image is the scrollbar element that moves, the front image is the part that does not.
    //--Do not use this for StarUIPiece objects, there is a standard function in the class.
    if(!pScroller || !pStatic) return;

    ///--[Error Check]
    //--Can't have zero items per page, or zero max offset.
    if(pItemsPerPage < 1) return;
    if(pMaxOffset    < 1) return;

    ///--[Constants]
    //--Indent.
    float cInd = 6.0f;

    ///--[Static Part]
    //--Render if this is in back.
    if(!pStaticIsFront) pStatic->Draw();

    ///--[Front]
    //--Binding.
    pScroller->Bind();

    //--Determine what percentage of the inventory is presently represented.
    float cPctTop = (pSkip                ) / (float)pMaxOffset;
    float cPctBot = (pSkip + pItemsPerPage) / (float)pMaxOffset;

    //--Positions and Constants.
    float cLft = pScroller->GetXOffset() + (pScroller->GetWidth() * 0.50f) - (pScroller->GetWidth() * 0.50f);
    float cRgt = cLft + pScroller->GetWidth();
    float cEdg = cInd / (float)pScroller->GetHeight();

    //--Compute where the bar should be.
    float cTopTop = pScrollTop + (cPctTop * pScrollHei);
    float cTopBot = cTopTop + cInd;
    float cBotBot = pScrollTop + (cPctBot * pScrollHei);
    float cBotTop = cBotBot - cInd;

    glBegin(GL_QUADS);
        //--Render the top of the bar.
        glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
        glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
        glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
        glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

        //--Render the bottom of the bar.
        glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
        glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
        glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

        //--Render the middle of the bar.
        glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
        glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
        glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
        glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
    glEnd();

    ///--[Static Part]
    //--Render.
    if(pStaticIsFront) pStatic->Draw();
}
