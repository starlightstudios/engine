///==================================== InventoryPrototypes =======================================
//--A singleton static class designed to store prototypes for use in inventories. For example, this
//  object may store weapons or armors that can be quickly cloned to spawn in the game.
//--The contents are always *cloned* so they can be modified after spawning.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class InventoryPrototypes
{
    private:
    //--System
    InventoryPrototypes();

    //--Storage
    static StarLinkedList *xStorageList;

    protected:

    public:
    //--System
    ~InventoryPrototypes();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static InventoryItem *Fetch(const char *pName);
    static InventoryItem *FetchPrototype(const char *pName);
    static void Upload(const char *pName, InventoryItem *pPrototype);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

