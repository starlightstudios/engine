//--Base
#include "FlexMenu.h"

//--Classes
#include "WorkshopItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "SteamManager.h"

///========================================== System ==============================================
void FlexMenu::RegisterMod(const char *pInternal, const char *pDisplay, bool pIsEnabled)
{
    ///--[Documentation]
    //--Registers a mod to the mod load order, implicitly on the end of the list. If the named mod already
    //  exists, does not register anything.
    if(!pInternal || !pDisplay) return;

    ///--[Duplication Check]
    FM_ModInfo *rCheckPack = (FM_ModInfo *)mModLoadOrderList->GetElementByName(pInternal);
    if(rCheckPack) return;

    ///--[Create, Register]
    //--Create.
    FM_ModInfo *nPackage = (FM_ModInfo *)starmemoryalloc(sizeof(FM_ModInfo));
    nPackage->Initialize();
    nPackage->mInternalName = InitializeString(pInternal);
    nPackage->mDisplayName = InitializeString(pDisplay);
    nPackage->mIsEnabled = pIsEnabled;

    //--Register.
    mModLoadOrderList->AddElementAsTail(pInternal, nPackage, &FM_ModInfo::DeleteThis);
}
void FlexMenu::SetPathOfMod(const char *pInternal, const char *pPath)
{
    ///--[Documentation]
    //--Sets the path for a mod. This is only actually needed if the mod is intended to be uploaded to the
    //  workshop, in which case the mod will both need this path for its data, and will need a related path
    //  with Path_Upload/ containing upload details.
    if(!pInternal || !pPath) return;

    ///--[Locate]
    //--Get the pack.
    FM_ModInfo *rCheckPack = (FM_ModInfo *)mModLoadOrderList->GetElementByName(pInternal);
    if(!rCheckPack) return;

    //--Set.
    ResetString(rCheckPack->mPath, pPath);
}
void FlexMenu::UploadModToWorkshop(const char *pInternalName)
{
    ///--[Documentation]
    //--Attempts to send the mod to the steam workshop. This requires creating a special folder and populating
    //  it with information the mod will need to be on the workshop.
    //--This does nothing if Steam is not active.
    #if defined _STEAM_API_

    ///--[Checks]
    //--Boot Steam Manager if it isn't already booted.
    SteamManager::Fetch()->Boot(STEAM_GAME_INDEX_WITCHHUNTERIZANA);

    //--Package must exist.
    fprintf(stderr, "Uploading mod to workshop. %s\n", pInternalName);
    fprintf(stderr, " Checking if mod exists.\n");
    FM_ModInfo *rCheckPack = (FM_ModInfo *)mModLoadOrderList->GetElementByName(pInternalName);
    if(!rCheckPack) return;

    //--Package must be flagged as ready for upload.
    fprintf(stderr, " Checking if mod is flagged ready.\n");
    if(!rCheckPack->mIsUploadReady && false) return;

    ///--[Create Object]
    //--Call the API function to create the object.
    fprintf(stderr, " Mod is flagged ready. Creating object.\n");

    //--Storage object.
    WorkshopItem *nObject = new WorkshopItem();
    nObject->SetCreateMode(rCheckPack->mPath);
    //nObject->SetDeleteMode();
    SteamManager::Fetch()->RegisterWorkshopItem(nObject);

    #endif
}
void FlexMenu::ClearModList()
{
    mModLoadOrderList->ClearList();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
void FlexMenu::ReadLoadOrderFromFile(const char *pFilePath)
{
    ///--[Documentation]
    //--Reads the mod load order from a text file. These files are plaintext and can be hand-edited
    //  if necessary.
    if(!pFilePath) return;

    ///--[Existence Check]
    //--If the file doesn't exist, do nothing. Don't bark an error.
    FILE *fInfile = fopen(pFilePath, "r");
    if(!fInfile) return;

    ///--[Read]
    //--Setup.
    char tBuffer[256];
    StarLinkedList *nNewModList = new StarLinkedList(true);

    //--Begin reading.
    while(!feof(fInfile))
    {
        //--Get the next string.
        fgets(tBuffer, 255, fInfile);

        //--Remove the newline characters if they exist in the buffer.
        int tLen = (int)strlen(tBuffer);
        for(int i = 0; i < tLen; i ++)
        {
            if(tBuffer[i] == 10 || tBuffer[i] == 13) { tBuffer[i] = '\0'; break; }
        }

        //--Locate the entry on the existing mod load order. If it's not found, ignore it. If it is
        //  found, put it in the new listing.
        FM_ModInfo *rCheckPack = (FM_ModInfo *)mModLoadOrderList->GetElementByName(tBuffer);
        if(rCheckPack)
        {
            //--Liberate the package and put it on the new mod list.
            mModLoadOrderList->SetRandomPointerToThis(rCheckPack);
            mModLoadOrderList->LiberateRandomPointerEntry();
            nNewModList->AddElementAsTail(rCheckPack->mInternalName, rCheckPack, &FM_ModInfo::DeleteThis);

            //--Read the next line, which is a 0 or 1 that stores if the mod is enabled or not in the load order.
            fgets(tBuffer, 255, fInfile);
            if(tBuffer[0] == '0')
            {
                rCheckPack->mIsEnabled = false;
            }
            else
            {
                rCheckPack->mIsEnabled = true;
            }
        }
        //--Entry was not found, possibly because the mod was renamed or removed. Skip a line.
        else
        {
            fgets(tBuffer, 255, fInfile);
        }
    }

    ///--[Finish]
    //--All remaining mods in the load order are taken off the list and placed in the new list on the end,
    //  and are implicitly disabled.
    while(mModLoadOrderList->GetListSize() > 0)
    {
        //--Set to head and remove.
        mModLoadOrderList->SetRandomPointerToHead();
        FM_ModInfo *rHeadPackage = (FM_ModInfo *)mModLoadOrderList->GetHead();
        mModLoadOrderList->LiberateRandomPointerEntry();

        //--Disable.
        rHeadPackage->mIsEnabled = false;

        //--Add to the end of the new mod list.
        nNewModList->AddElementAsTail(rHeadPackage->mInternalName, rHeadPackage, &FM_ModInfo::DeleteThis);
    }

    //--The mod load order list should now by empty. Delete it.
    delete mModLoadOrderList;

    //--Replace the mod load order list with the new one we just built.
    mModLoadOrderList = nNewModList;

    ///--[Clean Up]
    fclose(fInfile);
}
void FlexMenu::WriteLoadOrderToFile(const char *pFilePath)
{
    ///--[Documentation]
    //--Writes the current load order list to the provided file as plaintext.
    if(!pFilePath) return;

    //--Open and check.
    FILE *fOutfile = fopen(pFilePath, "w");
    if(!fOutfile) return;

    ///--[Write]
    FM_ModInfo *rModInfo = (FM_ModInfo *)mModLoadOrderList->PushIterator();
    while(rModInfo)
    {
        //--Write the name of the mod.
        fprintf(fOutfile, "%s\n", rModInfo->mInternalName);

        //--Write if the mod is enabled or not. 0 is disabled, 1 is not.
        if(rModInfo->mIsEnabled)
        {
            fprintf(fOutfile, "1\n");
        }
        else
        {
            fprintf(fOutfile, "0\n");
        }

        //--Next.
        rModInfo = (FM_ModInfo *)mModLoadOrderList->AutoIterate();
    }

    ///--[Clean Up]
    fclose(fOutfile);
}

///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
