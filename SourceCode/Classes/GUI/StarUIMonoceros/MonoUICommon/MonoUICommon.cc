//--Base
#include "MonoUICommon.h"

//--Classes
//--CoreClasses
#include "StarPointerSeries.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
MonoUICommon::MonoUICommon()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Images]
    ///--[ ====== MonoUICommon ======= ]
    ///--[System]
    ///--[Colors]
    mMonoTitleColor.    SetRGBAI(236, 211, 160, 255);
    mMonoSubtitleColor. SetRGBAI(181, 112,  41, 255);
    mMonoParagraphColor.SetRGBAI(214, 188, 219, 255);
}
MonoUICommon::~MonoUICommon()
{
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void MonoUICommon::SetColor(const char *pColorName, float pAlpha)
{
    ///--[Documentation]
    //--Overridable function, takes in a name and sets the color mixer.
    if(!pColorName) return;

    //--Handle colors.
    if(!strcasecmp(pColorName, "Heading"))   { StarlightColor::SetMixer(0.50f, 0.35f, 0.40f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Title"))     { StarlightColor::SetMixer(0.93f, 0.83f, 0.63f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Subtitle"))  { StarlightColor::SetMixer(0.71f, 0.44f, 0.16f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Paragraph")) { StarlightColor::SetMixer(0.84f, 0.74f, 0.86f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Greyed"))    { StarlightColor::SetMixer(0.70f, 0.70f, 0.70f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Black"))     { StarlightColor::SetMixer(0.00f, 0.00f, 0.00f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Clear"))     { StarlightColor::SetMixer(1.00f, 1.00f, 1.00f, pAlpha); return; }

    //--Error.
    fprintf(stderr, "AdvUICommon:SetColor() - Subtype %s. Failed to locate color %s.\n", mSubtype, pColorName);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
