//--Base
#include "WADFile.h"

//--Classes
#include "Skybox.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

//--Handles skybox rendering. The WADFile doesn't necessarily contain all the information necessary to render
//  the skybox by itself, though in this case it does and the sky can be set during gameplay.
//--The stencil code STENCIL_RENDER_SKY is needed to render the skybox correctly. Textures flagged with this
//  value will show the sky even if there was something behind them.

#define DISABLE_SKYBOX_STENCIL

//--[Memory Handling]
Skybox *WADFile::LiberateSkybox()
{
    //--Returns our internal skybox after clearing its pointer. The object is not deallocated.
    Skybox *rReturnSkybox = mLocalSkybox;
    mLocalSkybox = NULL;
    return rReturnSkybox;
}
void WADFile::ReceiveSkybox(Skybox *pSkybox)
{
    //--Receives a skybox and takes ownership of it.
    delete mLocalSkybox;
    mLocalSkybox = pSkybox;
}

//--[Construction]
void WADFile::BuildSkyboxPattern(const char *pPattern)
{
    //--Constructs and stores the skybox. Deletes the old one if necessary.
    if(!pPattern) return;
    delete mLocalSkybox;
    mLocalSkybox = NULL;

    //--Construct and store.
    mLocalSkybox = new Skybox();
    mLocalSkybox->SetImagesFromPattern(pPattern);
}

//--[Statics]
void WADFile::SetupSkyboxStencil()
{
    //--Worker function which can be set statically. Sets up skybox stencil rendering. Note that you must
    //  call SetSkyboxStencilState() to activate or deactivate stencilling once rendering begins. It will
    //  be "off" (that is, don't render the skybox) by default.

    //--Disable case.
    #ifdef DISABLE_SKYBOX_STENCIL
        return;
    #endif

    //--Setup.
    glEnable(GL_STENCIL_TEST);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, STENCIL_DONT_RENDER_SKY, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    glColorMask(true, true, true, true);
    glDepthMask(true);
}
void WADFile::SetSkyboxStencilState(bool pShouldRenderSkybox)
{
    //--Sets whether or not the skybox should render on whatever geometry is being uploaded. You should call
    //  SetupSkyboxStencil() before this.

    //--Disable case.
    #ifdef DISABLE_SKYBOX_STENCIL
        return;
    #endif

    //--Set.
    if(pShouldRenderSkybox)
    {
        glStencilFunc(GL_ALWAYS, STENCIL_NO_SPECIAL_RENDER, 0xFF);
    }
    else
    {
        glStencilFunc(GL_ALWAYS, STENCIL_DONT_RENDER_SKY, 0xFF);
    }
}
void WADFile::DeactivateSkyboxStencil()
{
    //--Disable case.
    #ifdef DISABLE_SKYBOX_STENCIL
        return;
    #endif

    //--Turns off skybox stencil controls.
    glDisable(GL_STENCIL_TEST);
    glColorMask(true, true, true, true);
}
