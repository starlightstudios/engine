//--Base
#include "PairanormalDialogueCharacter.h"

//--Classes
#include "SpineExpression.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"

//--[Local Definitions]
#define FOCUS_FADE_TICKS 30

#define DEPTH_NOTFOCUS -0.000001f
#define DEPTH_FOCUS 0.000000f

#define EXPRESSION_CROSSFADE_TICKS 10

//=========================================== System ==============================================
PairanormalDialogueCharacter::PairanormalDialogueCharacter()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PAIRANORMALDIALOGUECHARACTER;

    //--[PairanormalDialogueCharacter]
    //--System
    mInternalName = InitializeString("Unnamed");
    mLastExpressionStore = InitializeString("Neutral");

    //--Rendering Position
    mIsFading = false;
    mIsFadingOut = false;
    mPositionTimer = 1;
    mPositionTimerMax = 1;
    mRenderHalfWidth = 0.0f;
    mRenderX = 0.0f;
    mRenderY = 0.0f;
    mPreviousRenderX = 0.0f;
    mPreviousRenderY = 0.0f;
    mRenderOffsetY = 0.0f;

    //--Greying
    mIsFocusCharacter = false;
    mIsOverrideFocusCharacter = false;
    mGreyTimer = 0;

    //--Chatting
    mDialoguePath = NULL;

    //--Aliases
    mAliasList = new SugarLinkedList(false);

    //--Emotions
    mExpressionCrossfadeTimer = EXPRESSION_CROSSFADE_TICKS;
    rLastActiveExpression = NULL;
    rActiveExpression = NULL;
    mExpressionList = new SugarLinkedList(true);
    mEmotionMapList = new SugarLinkedList(true);

    //--Public Variables
    mIsNextTransitionSlideFromSide = false;
    mIsNextTransitionFade = false;
    mPresentRenderX = 0.0f;
    mPresentRenderY = 0.0f;
}
PairanormalDialogueCharacter::~PairanormalDialogueCharacter()
{
    free(mInternalName);
    free(mLastExpressionStore);
    free(mDialoguePath);
    delete mAliasList;
    delete mExpressionList;
    delete mEmotionMapList;
}

//====================================== Property Queries =========================================
const char *PairanormalDialogueCharacter::GetInternalName()
{
    return (const char *)mInternalName;
}
const char *PairanormalDialogueCharacter::GetEmotionName()
{
    return (const char *)mLastExpressionStore;
}
const char *PairanormalDialogueCharacter::GetDialoguePath()
{
    return (const char *)mDialoguePath;
}
bool PairanormalDialogueCharacter::IsAliasOnList(const char *pAlias)
{
    return (mAliasList->GetElementByName(pAlias) != NULL);
}
float PairanormalDialogueCharacter::GetRenderX()
{
    return mRenderX;
}
float PairanormalDialogueCharacter::GetRenderY()
{
    return mRenderY;
}
bool PairanormalDialogueCharacter::IsFadingOut()
{
    if(mPositionTimer >= mPositionTimerMax) return false;
    return mIsFadingOut;
}

//========================================= Manipulators ==========================================
void PairanormalDialogueCharacter::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mInternalName, pName);
}
void PairanormalDialogueCharacter::SetRenderOffset(float pValue)
{
    mRenderOffsetY = pValue;
}
void PairanormalDialogueCharacter::SetDialoguePath(const char *pPath)
{
    ResetString(mDialoguePath, pPath);
}
void PairanormalDialogueCharacter::AddAlias(const char *pName)
{
    static bool xDummyPtr = false;
    mAliasList->AddElement(pName, &xDummyPtr);
}
void PairanormalDialogueCharacter::AddExpression(const char *pName, const char *pBasePath, const char *pImagePath, const char *pStateA, const char *pStateB)
{
    if(!pName || !pBasePath || !pImagePath || !pStateA || !pStateB) return;
    SpineExpression *nExpression = new SpineExpression();
    nExpression->Construct(pBasePath, pImagePath, pStateA, pStateB);
    mExpressionList->AddElement(pName, nExpression, &RootObject::DeleteThis);
    rActiveExpression = nExpression;
}
void PairanormalDialogueCharacter::AddExpression3(const char *pName, const char *pBasePath, const char *pImagePath, const char *pStateA, const char *pStateB, const char *pStateC)
{
    if(!pName || !pBasePath || !pImagePath || !pStateA || !pStateB || !pStateC) return;
    SpineExpression *nExpression = new SpineExpression();
    nExpression->Construct3(pBasePath, pImagePath, pStateA, pStateB, pStateC);
    mExpressionList->AddElement(pName, nExpression, &RootObject::DeleteThis);
    rActiveExpression = nExpression;
}
void PairanormalDialogueCharacter::SetEmotion(const char *pEmotion)
{
    //--Do nothing if the name is "NoChange".
    //fprintf(stderr, "Emotion %s\n", pEmotion);
    if(!pEmotion || !strcasecmp(pEmotion, "NoChange")) return;

    //--Locate the matching remap. If it doesn't exist, do nothing.
    EmotionMap *rRemap = (EmotionMap *)mEmotionMapList->GetElementByName(pEmotion);
    //fprintf(stderr, " Remap %p\n", rRemap);
    if(!rRemap) return;

    //--Get the expression out and set it as active, if it exists.
    SpineExpression *rCheckExpression = (SpineExpression *)mExpressionList->GetElementByName(rRemap->mExpression);
    //fprintf(stderr, " Expression %p\n", rCheckExpression);
    if(!rCheckExpression) return;

    //--Store.
    ResetString(mLastExpressionStore, pEmotion);

    //--If not the same as the existing expression, we need to run the update once it gets set.
    //  This prevents the image from being "balled".
    if(rActiveExpression != rCheckExpression)
    {
        mExpressionCrossfadeTimer = 0;
        rLastActiveExpression = rActiveExpression;
        rActiveExpression = rCheckExpression;
        rActiveExpression->SetEmotion(rRemap->mEmotion);
        rActiveExpression->Update();

        //--No previous expression.
        if(!rLastActiveExpression)
        {
            mExpressionCrossfadeTimer = EXPRESSION_CROSSFADE_TICKS;
            rLastActiveExpression = rActiveExpression;
        }
    }
    //--Otherwise, just change the emotion.
    else
    {
        rActiveExpression->SetEmotion(rRemap->mEmotion);
    }
}
void PairanormalDialogueCharacter::AddEmotionMap(const char *pEmotionName, const char *pExpression, const char *pEmotionStateName)
{
    if(!pEmotionName || !pExpression || !pEmotionStateName) return;
    EmotionMap *nMap = (EmotionMap *)malloc(sizeof(EmotionMap));
    strncpy(nMap->mExpression, pExpression, 31);
    strncpy(nMap->mEmotion, pEmotionStateName, 31);
    mEmotionMapList->AddElement(pEmotionName, nMap, &FreeThis);
}
void PairanormalDialogueCharacter::SetHalfWidth(float pValue)
{
    mRenderHalfWidth = pValue;
}
void PairanormalDialogueCharacter::SetFocus(bool pIsFocused)
{
    mIsFocusCharacter = pIsFocused;
}
void PairanormalDialogueCharacter::SetOverrideFocus(bool pIsFocused)
{
    mIsOverrideFocusCharacter = pIsFocused;
}
void PairanormalDialogueCharacter::SetRenderPosition(float pX, float pY)
{
    //--Instantly sets the rendering position of the character.
    mRenderX = pX;
    mRenderY = pY;
}
void PairanormalDialogueCharacter::SetRenderPositionByFade(float pX, float pY, int pFadeTicks)
{
    //--Sets the character to fade in at this rendering position. Pass a negative fade number
    //  to fade the character out. Passing 0 makes the character instantly vanish.
    if(pFadeTicks == 0)
    {
        mIsFading = false;
        mIsFadingOut = true;
        mPositionTimer = 1;
        mPositionTimerMax = 1;
        mRenderX = pX;
        mRenderY = pY;
        return;
    }

    //--Normal.
    mIsFading = true;
    mIsFadingOut = (pFadeTicks < 0);
    mPositionTimer = 0;
    mPositionTimerMax = abs(pFadeTicks);
    mRenderX = pX;
    mRenderY = pY;
}
void PairanormalDialogueCharacter::SetRenderPositionBySlide(float pX, float pY, int pSlideTicks)
{
    //--Sets the character to slide to the listed position. Slide ticks cannot be 0 or negative.
    //  To make a character slide in from offscreen, use SetRenderPosition() then this function,
    //  causing them to slide from the first function call to the second.
    if(pSlideTicks < 1) return;
    mIsFading = false;
    mPositionTimer = 0;
    mPositionTimerMax = pSlideTicks;
    mPreviousRenderX = mRenderX;
    mPreviousRenderY = mRenderY;
    mRenderX = pX;
    mRenderY = pY;
}
void PairanormalDialogueCharacter::MaxOutPosition()
{
    mPositionTimer = mPositionTimerMax-1;
}
void PairanormalDialogueCharacter::SetRenderHFlip(bool pFlag)
{
    //--Iterate and set.
    SpineExpression *rExpession = (SpineExpression *)mExpressionList->PushIterator();
    while(rExpession)
    {
        rExpession->SetHFlipFlag(pFlag);
        rExpession = (SpineExpression *)mExpressionList->AutoIterate();
    }
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void PairanormalDialogueCharacter::Update(bool pAlwaysUpdateExpression)
{
    //--Expression crossfade.
    if(mExpressionCrossfadeTimer < EXPRESSION_CROSSFADE_TICKS)
    {
        mExpressionCrossfadeTimer ++;
    }

    //--Update timers if requested.
    if(mPositionTimer < mPositionTimerMax)
    {
        mPositionTimer ++;
    }
    //--Reset flags.
    else
    {
        mIsNextTransitionFade = false;
        mIsNextTransitionSlideFromSide = false;
    }

    //--Focus changes.
    if(mIsFocusCharacter || mIsOverrideFocusCharacter)
    {
        if(mGreyTimer < FOCUS_FADE_TICKS) mGreyTimer ++;
    }
    else
    {
        if(mGreyTimer > 0) mGreyTimer --;
        if(mExpressionCrossfadeTimer < EXPRESSION_CROSSFADE_TICKS) mExpressionCrossfadeTimer = EXPRESSION_CROSSFADE_TICKS;
    }

    //--If an emotion is active, run its update function.
    if(rActiveExpression && (mIsFocusCharacter || mIsOverrideFocusCharacter || pAlwaysUpdateExpression))
    {
        if(rLastActiveExpression && mExpressionCrossfadeTimer < EXPRESSION_CROSSFADE_TICKS) rLastActiveExpression->Update();
        rActiveExpression->Update();
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void PairanormalDialogueCharacter::Render(float pZoom)
{
    //--Renders the character at whatever position they are currently at internally.
    //fprintf(stderr, "Rendering!\n");
    if(!rActiveExpression || pZoom == 0.0f) return;

    //--Greying value.
    float cGreyMix = 0.25f + ((mGreyTimer / (float)FOCUS_FADE_TICKS) * 0.75f);
    float cScale = 0.90f + ((mGreyTimer / (float)FOCUS_FADE_TICKS) * 0.10f);
    if(mIsFocusCharacter || mIsOverrideFocusCharacter) cScale = pZoom;

    //--Previous expression crossfade case.
    float cPrevAlpha = (1.0f - (mExpressionCrossfadeTimer / (float)EXPRESSION_CROSSFADE_TICKS));
    float cCurrAlpha = (mExpressionCrossfadeTimer / (float)EXPRESSION_CROSSFADE_TICKS);

    //--Depth modification.
    if(!mIsFocusCharacter && !mIsOverrideFocusCharacter) glTranslatef(0.0f, 0.0f, DEPTH_NOTFOCUS);

    //--If in a transition:
    if(mPositionTimer < mPositionTimerMax && mPositionTimerMax > 0)
    {
        //--Compute the percentage.
        float cAlpha = (float)mPositionTimer / (float)mPositionTimerMax;

        //--During a fade-in transition:
        if(mIsFading && !mIsFadingOut)
        {
            //--Previous expression.
            /*if(rLastActiveExpression && cPrevAlpha > 0.0f)
            {
                float cValue = cGreyMix * cAlpha;
                StarlightColor::SetMixer(cValue, cValue, cValue, cAlpha);
                rLastActiveExpression->RenderAtCenter(mRenderX + mRenderHalfWidth, mRenderY + mRenderOffsetY, cScale);
                StarlightColor::ClearMixer();
            }*/

            //--Current expression.
            float cValue = cGreyMix * cAlpha;
            StarlightColor::SetMixer(cValue, cValue, cValue, cAlpha);
            rActiveExpression->RenderAtCenter(mRenderX + mRenderHalfWidth, mRenderY + mRenderOffsetY, cScale);
            StarlightColor::ClearMixer();
        }
        //--During a fade-out transition:
        else if(mIsFading && mIsFadingOut)
        {
            //--Previous expression.
            /*if(rLastActiveExpression && cPrevAlpha > 0.0f)
            {
                float cValue = cGreyMix * (1.0f - cAlpha) * cPrevAlpha;
                StarlightColor::SetMixer(cValue, cValue, cValue, (1.0f - cAlpha) * cPrevAlpha);
                rLastActiveExpression->RenderAtCenter(mRenderX + mRenderHalfWidth, mRenderY + mRenderOffsetY, cScale);
                StarlightColor::ClearMixer();
            }*/

            //--Current expression.
            float cValue = cGreyMix * (1.0f - cAlpha) * cCurrAlpha;
            StarlightColor::SetMixer(cValue, cValue, cValue, (1.0f - cAlpha) * cCurrAlpha);
            rActiveExpression->RenderAtCenter(mRenderX + mRenderHalfWidth, mRenderY + mRenderOffsetY, cScale);
            StarlightColor::ClearMixer();
        }
        //--During a sliding transition:
        else
        {
            //--Position.
            float tCurrentX = mPreviousRenderX + ((mRenderX - mPreviousRenderX) * EasingFunction::QuadraticOut(cAlpha));
            float tCurrentY = mPreviousRenderY + ((mRenderY - mPreviousRenderY) * EasingFunction::QuadraticOut(cAlpha));

            //--Previous expression.
            if(rLastActiveExpression && cPrevAlpha > 0.0f)
            {
                float cValue = cGreyMix * cPrevAlpha;
                StarlightColor::SetMixer(cValue, cValue, cValue, cPrevAlpha);
                rLastActiveExpression->RenderAtCenter(tCurrentX + mRenderHalfWidth, tCurrentY + mRenderOffsetY, cScale);
                StarlightColor::ClearMixer();
            }

            //--Current expression.
            float cValue = cGreyMix * cCurrAlpha;
            StarlightColor::SetMixer(cValue, cValue, cValue, cCurrAlpha);
            rActiveExpression->RenderAtCenter(tCurrentX + mRenderHalfWidth, tCurrentY + mRenderOffsetY, cScale);
            StarlightColor::ClearMixer();
        }
    }
    //--Otherwise, render at the final position.
    else
    {
        //--Previous expression.
        if(rLastActiveExpression && cPrevAlpha > 0.0f)
        {
            float cValue = cGreyMix * cPrevAlpha;
            StarlightColor::SetMixer(cValue, cValue, cValue, cPrevAlpha);
            rLastActiveExpression->RenderAtCenter(mRenderX + mRenderHalfWidth, mRenderY + mRenderOffsetY, cScale);
            StarlightColor::ClearMixer();
        }

        //--Current expression.
        float cValue = cGreyMix * cCurrAlpha;
        StarlightColor::SetMixer(cValue, cValue, cValue, cCurrAlpha);
        rActiveExpression->RenderAtCenter(mRenderX + mRenderHalfWidth, mRenderY + mRenderOffsetY, cScale);
        StarlightColor::ClearMixer();
    }

    //--Depth modification.
    if(!mIsFocusCharacter && !mIsOverrideFocusCharacter) glTranslatef(0.0f, 0.0f, -DEPTH_NOTFOCUS);
}
void PairanormalDialogueCharacter::RenderAtCenter(float pX, float pY)
{
    //--Forces the character to render at the given position.
    if(!rActiveExpression) return;
    rActiveExpression->RenderAtCenter(pX, pY + mRenderOffsetY, 1.0f);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void PairanormalDialogueCharacter::HookToLuaState(lua_State *pLuaState)
{
    /* DiaChar_SetProperty("Add Expression", sExpressionName, sSpinePath, sEmotionA, sEmotionB)
       DiaChar_SetProperty("Add Expression", sExpressionName, sSpinePath, sEmotionA, sEmotionB, sEmotionC)
       DiaChar_SetProperty("Remap Emotion", sEmotionName, sExpression, sSubEmotion)
       DiaChar_SetProperty("Half Width", fValue)
       DiaChar_SetProperty("Render Offset", fValue)
       DiaChar_SetProperty("Render HFlip", bFlag)
       Sets the requested property in the active PairanormalDialogueCharacter. */
    lua_register(pLuaState, "DiaChar_SetProperty", &Hook_DiaChar_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_DiaChar_SetProperty(lua_State *L)
{
    //DiaChar_SetProperty("Add Expression", sExpressionName, sSpinePath, sImagePath, sEmotionA, sEmotionB)
    //DiaChar_SetProperty("Remap Emotion", sEmotionName, sExpression, sSubEmotion)
    //DiaChar_SetProperty("Half Width", fValue)
    //DiaChar_SetProperty("Render Offset", fValue)
    //DiaChar_SetProperty("Render HFlip", bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DiaChar_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active Object.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_PAIRANORMALDIALOGUECHARACTER)) return LuaTypeError("DiaChar_SetProperty", L);
    PairanormalDialogueCharacter *rActiveCharacter = (PairanormalDialogueCharacter *)DataLibrary::Fetch()->rActiveObject;

    //--Add a spine expression and two associated sub-emotions.
    if(!strcasecmp(rSwitchType, "Add Expression") && tArgs == 6)
    {
        rActiveCharacter->AddExpression(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
    }
    //--Add a spine expression and three associated sub-emotions.
    else if(!strcasecmp(rSwitchType, "Add Expression") && tArgs == 7)
    {
        rActiveCharacter->AddExpression3(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6), lua_tostring(L, 7));
    }
    //--Creates a single name remap for the expression/emotion combos.
    else if(!strcasecmp(rSwitchType, "Remap Emotion") && tArgs == 4)
    {
        rActiveCharacter->AddEmotionMap(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Approximate distance from side to center so the character offsets correctly when rendering.
    else if(!strcasecmp(rSwitchType, "Half Width") && tArgs == 2)
    {
        rActiveCharacter->SetHalfWidth(lua_tonumber(L, 2));
    }
    //--Vertical offset.
    else if(!strcasecmp(rSwitchType, "Render Offset") && tArgs == 2)
    {
        rActiveCharacter->SetRenderOffset(lua_tonumber(L, 2));
    }
    //--Character will render with a horizontal flip.
    else if(!strcasecmp(rSwitchType, "Render HFlip") && tArgs == 2)
    {
        rActiveCharacter->SetRenderHFlip(lua_toboolean(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("DiaChar_SetProperty", rSwitchType, tArgs);
    }

    //--Done.
    return 0;
}
