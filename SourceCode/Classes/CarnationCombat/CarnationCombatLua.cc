//--Base
#include "CarnationCombat.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================= Lua Hooking ==========================================
void CarnationCombat::HookToLuaState(lua_State *pLuaState)
{
    /* CarnCombat_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "CarnCombat_GetProperty", &Hook_CarnCombat_GetProperty);

    /* CarnCombat_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "CarnCombat_SetProperty", &Hook_CarnCombat_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnCombat_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnCombat_GetProperty("Ability Exec Phase") (1 Integer)
    //CarnCombat_GetProperty("Run RNG") (1 Float)
    //CarnCombat_GetProperty("Inventory Slot") (1 Integer)
    //CarnCombat_GetProperty("Is Action Effortless") (1 Boolean)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnCombat_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    if(!rAdventureCombat || !rAdventureCombat->IsOfType(POINTER_TYPE_CARNATIONCOMBAT)) return LuaTypeError("CarnCombat_GetProperty", L);

    //--Cast.
    CarnationCombat *rCarnCombat = (CarnationCombat *)rAdventureCombat;

    ///--[System]
    //--True if the given party slot is disabled, false otherwise.
    if(!strcasecmp(rSwitchType, "Ability Exec Phase") && tArgs == 1)
    {
        lua_pushinteger(L, rCarnCombat->GetAbilityExecutionPhase());
        tReturns = 1;
    }
    //--Advances the RNG by 1 and generates a new random number from 0 to 1.
    else if(!strcasecmp(rSwitchType, "Run RNG") && tArgs == 1)
    {
        lua_pushnumber(L, rCarnCombat->RunRNG());
        tReturns = 1;
    }
    //--Returns the inventory slot affiliated with the last executed command. Is -1 for non-items.
    else if(!strcasecmp(rSwitchType, "Inventory Slot") && tArgs == 1)
    {
        lua_pushinteger(L, rCarnCombat->GetStoredInventorySlot());
        tReturns = 1;
    }
    //--True when using an Effortless action, usually an item.
    else if(!strcasecmp(rSwitchType, "Is Action Effortless") && tArgs == 1)
    {
        lua_pushboolean(L, rCarnCombat->IsActionEffortless());
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_CarnCombat_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnCombat_SetProperty("Reply To Status Check")
    //CarnCombat_SetProperty("Reply To Action Phase")
    //CarnCombat_SetProperty("Clear Text")
    //CarnCombat_SetProperty("Append Text", sString)
    //CarnCombat_SetProperty("Victory Handler", sPath)
    //CarnCombat_SetProperty("Defeat Handler", sPath)
    //CarnCombat_SetProperty("Seed RNG", iSeed)
    //CarnCombat_SetProperty("Register Levelup", sPackName)
    //CarnCombat_SetProperty("Setup Levelup Stat", sPackName, sProperty, iValue)
    //CarnCombat_SetProperty("Clear Backgrounds")
    //CarnCombat_SetProperty("Register Background", sBGName, sImgPath, iLogicType, fLogicFactor)
    //CarnCombat_SetProperty("Status Print Block")

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnCombat_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    if(!rAdventureCombat || !rAdventureCombat->IsOfType(POINTER_TYPE_CARNATIONCOMBAT)) return LuaTypeError("CarnCombat_SetProperty", L);

    //--Cast.
    CarnationCombat *rCarnCombat = (CarnationCombat *)rAdventureCombat;

    ///--[System]
    //--Indicates the script will handle the character's action during a status effect check.
    if(!strcasecmp(rSwitchType, "Reply To Status Check") && tArgs == 1)
    {
        rCarnCombat->TripStatusResponse();
    }
    //--Indicates the script should run again, as the action has at least one more phase to it.
    else if(!strcasecmp(rSwitchType, "Reply To Action Phase") && tArgs == 1)
    {
        rCarnCombat->FlagAbilityReplied();
    }
    //--Clear the text crawl.
    else if(!strcasecmp(rSwitchType, "Clear Text") && tArgs == 1)
    {
        rCarnCombat->ClearText();
    }
    //--Append text to the crawl.
    else if(!strcasecmp(rSwitchType, "Append Text") && tArgs == 4)
    {
        rCarnCombat->AppendText(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Path of the victory handler.
    else if(!strcasecmp(rSwitchType, "Victory Handler") && tArgs == 2)
    {
        rCarnCombat->SetVictoryHandler(lua_tostring(L, 2));
    }
    //--Path of the defeat handler.
    else if(!strcasecmp(rSwitchType, "Defeat Handler") && tArgs == 2)
    {
        rCarnCombat->SetDefeatHandler(lua_tostring(L, 2));
    }
    //--Seeds the RNG. Only used for level-ups.
    else if(!strcasecmp(rSwitchType, "Seed RNG") && tArgs == 2)
    {
        rCarnCombat->SeedRNG(lua_tointeger(L, 2));
    }
    //--Registers a level-up package with a unique name.
    else if(!strcasecmp(rSwitchType, "Register Levelup") && tArgs == 2)
    {
        rCarnCombat->RegisterLevelUpPack(lua_tostring(L, 2));
    }
    //--Sets a level-up stat within the named package.
    else if(!strcasecmp(rSwitchType, "Setup Levelup Stat") && tArgs == 4)
    {
        rCarnCombat->SetLevelUpPackProperty(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Clears all background layers.
    else if(!strcasecmp(rSwitchType, "Clear Backgrounds") && tArgs == 1)
    {
        rCarnCombat->ClearBackgrounds();
    }
    //--Creates a new background with the given image. Remember to register images back-to-front.
    else if(!strcasecmp(rSwitchType, "Register Background") && tArgs == 5)
    {
        rCarnCombat->RegisterBackground(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4), lua_tonumber(L, 5));
    }
    //--Prevents character cards from advancing during status prints post-action.
    else if(!strcasecmp(rSwitchType, "Status Print Block") && tArgs >= 1)
    {
        rCarnCombat->SetPrintBlock();
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnCombat_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}

