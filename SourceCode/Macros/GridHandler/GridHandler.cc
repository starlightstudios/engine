//--Base
#include "GridHandler.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Definitions.h"
#include "DeletionFunctions.h"
#include "Structures.h"

//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
GridHandler::GridHandler()
{
    //--Constructor is private and the class cannot be instantiated.
}

int **GridHandler::BuildGrid(int pXSize, int pYSize, int pXStart, int pYStart, int pMaxEntries)
{
    ///--[ ====== Documentation and Setup ===== ]
    //--Given a size, creates a 2-dimensional array and populates it with priorities starting from the
    //  provided center point. These priorities are set to minimize the number of "moves" from the
    //  starting point the entries are.
    //--This is used in the grid-based UI for dynamically sized cases, where the number of entries
    //  may change as the game goes on.
    //--Null is returned on error. A static deletion function is provided below to handle the resulting grid.
    //--The grid MUST be at least 1x1 or the routine will fail. The edges are always padded with -1's
    //  to allow quicker for loops when moving the grid properties. Therefore, a 1x1 grid will be sized
    //  as 3x3.
    if(pXSize < 1 || pYSize < 1) return NULL;

    ///--[ ============ Allocation ============ ]
    //--True sizes.
    int tXTrue = pXSize + 2;
    int tYTrue = pYSize + 2;

    //--Allocate, clear to -1.
    SetMemoryData(__FILE__, __LINE__);
    int **nGrid = (int **)starmemoryalloc(sizeof(int *) * tXTrue);
    for(int x = 0; x < tXTrue; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        nGrid[x] = (int *)starmemoryalloc(sizeof(int) * tYTrue);
        for(int y = 0; y < tYTrue; y ++)
        {
            nGrid[x][y] = -1;
        }
    }

    //--If there are no expected entries, just pass back a grid of -1's.
    if(pMaxEntries < 1) return nGrid;

    //--If the starting X/Y position is outside the grid area, then the start is the middle position, for some
    //  definition of "middle". Integer math rounds down.
    if(pXStart < 0 || pXStart >= pXSize) pXStart = pXSize / 2;
    if(pYStart < 0 || pYStart >= pYSize) pYStart = pYSize / 2;

    ///--[ ========= Priority Builder ========= ]
    //--Set priorities. The priorities are set algorithmically in terms of keypresses from the zero point.
    //  The edges are disallowed. First, set the "seed" value. Note it is indented by 1 to handle the
    //  edges being all -1's.
    SetMemoryData(__FILE__, __LINE__);
    StarLinkedList *tPosList = new StarLinkedList(true);
    Point2DI *nPoint = (Point2DI *)starmemoryalloc(sizeof(Point2DI));
    nPoint->mX = pXStart+1;
    nPoint->mY = pYStart+1;
    tPosList->AddElementAsTail("X", nPoint, &FreeThis);

    //--Other setup.
    int tCurrentValue = 0;

    //--Now loop until all spaces are occupied or we run out of entries to place.
    while(tPosList->GetListSize() > 0)
    {
        ///--[Setup]
        //--Get the zeroth entry.
        Point2DI *rZeroPoint = (Point2DI *)tPosList->GetHead();

        //--Make sure the positions are legal. Should be logically impossible. Assuming the programmer
        //  isn't an idiot, which they are.
        int tX = rZeroPoint->mX;
        int tY = rZeroPoint->mY;
        if(tX < 1 || tY < 1 || tX >= tXTrue-1 || tY >= tYTrue-1)
        {
            fprintf(stderr, "Illegal pos %i %i\n", tX, tY);
            break;
        }

        //--Grid position is already occupied. Skip this.
        if(nGrid[tX][tY] != -1)
        {
            tPosList->DeleteHead();
            continue;
        }

        ///--[Position]
        //--Set the position on the grid to the current value.
        nGrid[tX][tY] = tCurrentValue;
        tCurrentValue ++;

        //--If the value exceeds the number of entries on the list, we're done.
        if(tCurrentValue >= pMaxEntries) break;

        ///--[Set Next by Priority]
        //--Adds the next entries to the grid by priority. Horizontal is given priority
        //  over vertical.

        //--Right.
        if(tX < tXTrue-2)
        {
            SetMemoryData(__FILE__, __LINE__);
            Point2DI *nPoint = (Point2DI *)starmemoryalloc(sizeof(Point2DI));
            nPoint->mX = tX+1;
            nPoint->mY = tY;
            tPosList->AddElementAsTail("X", nPoint, &FreeThis);
        }

        //--Left.
        if(tX > 1)
        {
            SetMemoryData(__FILE__, __LINE__);
            Point2DI *nPoint = (Point2DI *)starmemoryalloc(sizeof(Point2DI));
            nPoint->mX = tX-1;
            nPoint->mY = tY;
            tPosList->AddElementAsTail("X", nPoint, &FreeThis);
        }

        //--Up.
        if(tY > 1)
        {
            SetMemoryData(__FILE__, __LINE__);
            Point2DI *nPoint = (Point2DI *)starmemoryalloc(sizeof(Point2DI));
            nPoint->mX = tX;
            nPoint->mY = tY-1;
            tPosList->AddElementAsTail("X", nPoint, &FreeThis);
        }

        //--Down.
        if(tY < tYTrue-2)
        {
            SetMemoryData(__FILE__, __LINE__);
            Point2DI *nPoint = (Point2DI *)starmemoryalloc(sizeof(Point2DI));
            nPoint->mX = tX;
            nPoint->mY = tY+1;
            tPosList->AddElementAsTail("X", nPoint, &FreeThis);
        }

        ///--[Next]
        //--Remove the zeroth entry, to prevent double-placement.
        tPosList->DeleteHead();
    }

    ///--[ =============== Clean ============== ]
    //--Clean, return.
    delete tPosList;
    return nGrid;
}
void GridHandler::BuildGridList(StarLinkedList *pUseList, int **pGrid, int pXSize, int pYSize, int pEntries, float pCenterX, float pCenterY, float pSpacingX, float pSpacingY)
{
    ///--[Documentation]
    //--Once the grid has been built, a dynamically-sized list can be populated with control information. Statically
    //  sized lists need to be handled elsewhere. The provided list should (normally) be empty before this call.
    //--Pass in the initial size of the grid, not accounting for the -1 padding.
    if(!pUseList || !pGrid || pXSize < 1 || pYSize < 1 || pEntries < 1) return;

    //--Build the grid packages.
    for(int i = 0; i < pEntries; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        GridPackage *nPackage = (GridPackage *)starmemoryalloc(sizeof(GridPackage));
        nPackage->Initialize();
        pUseList->AddElement("X", nPackage, &FreeThis);
    }

    //--Now run across the entries and assign control values.
    int cXMiddle = (pXSize+2)/2;
    int cYMiddle = (pYSize+2)/2;
    for(int x = 1; x < pXSize-1+2; x ++)
    {
        for(int y = 1; y < pYSize-1+2; y ++)
        {
            //--Skip -1 instances.
            if(pGrid[x][y] == -1) continue;

            //--Get the entry.
            int i = pGrid[x][y];

            //--Find the matching grid pack.
            GridPackage *rPackage = (GridPackage *)pUseList->GetElementBySlot(i);
            if(!rPackage) continue;

            //--Set which controls go where.
            rPackage->mControlLookups[GRID_LFT] = pGrid[x-1][y+0];
            rPackage->mControlLookups[GRID_TOP] = pGrid[x+0][y-1];
            rPackage->mControlLookups[GRID_RGT] = pGrid[x+1][y+0];
            rPackage->mControlLookups[GRID_BOT] = pGrid[x+0][y+1];

            //--Set the grid position.
            rPackage->mXPos = pCenterX + ((x-cXMiddle) * pSpacingX);
            rPackage->mYPos = pCenterY + ((y-cYMiddle) * pSpacingY);
        }
    }

}
void GridHandler::DeallocateGrid(int **pGrid, int pXSize)
{
    //--Cleans up a grid created by BuildGrid(). The object is unstable after being passed here.
    if(!pGrid || pXSize < 1) return;
    for(int x = 0; x < pXSize+2; x ++) free(pGrid[x]);
    free(pGrid);
}
