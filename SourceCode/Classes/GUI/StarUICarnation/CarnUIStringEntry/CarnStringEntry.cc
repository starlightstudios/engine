//--Base
#include "CarnStringEntry.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
CarnStringEntry::CarnStringEntry()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ====== StringEntry ======= ]
    ///--[ ==== CarnStringEntry ===== ]
    ///--[System]
    ///--[Images]
    mCarnTitle.    SetRGBAI(236, 211, 160, 255);
    mCarnSubtitle. SetRGBAI(181, 112,  41, 255);
    mCarnParagraph.SetRGBAI(214, 188, 219, 255);
    memset(&CarnImg, 0, sizeof(CarnImg));

    ///--[ ================ Construction ================ ]
    ///--[Setup]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Images]
    //--Fonts
    CarnImg.Data.rFontHeader   = rDataLibrary->GetFont("Carn String Entry Header");
    CarnImg.Data.rFontCurEntry = rDataLibrary->GetFont("Carn String Entry Current");
    CarnImg.Data.rFontMainline = rDataLibrary->GetFont("Carn String Entry Mainline");

    //--Images
    CarnImg.Data.rRenderBase    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/StringEntry/Base");
    //CarnImg.Data.rCursor        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarncerosUI/String/Cursor");
    CarnImg.Data.rRenderPressed = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/StringEntry/Pressed");

    ///--[Verify]
    CarnImg.mIsReady = VerifyStructure(&CarnImg.Data, sizeof(CarnImg.Data), sizeof(void *), false);

    //--Error print.
    if(!CarnImg.mIsReady)
    {
        VerifyStructure(&CarnImg.Data, sizeof(CarnImg.Data), sizeof(void *), true);
    }

    ///--[ ============ Rendering Positions ============= ]
    //--Rendering position constants.
    float cRenderX = 278.0f;
    float cRenderY =  61.0f;

    //--1234567890
    float cBtnSqr = 43.0f;
    float cBtnSpc = cBtnSqr + 5.0f;
    float cXPos = 108.0f + cRenderX;
    float tYPos = 307.0f;
    for(int i = 0; i < 10; i ++)
    {
        mButtons[SE_BTN_1 + i].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[SE_BTN_1 + i], "%i", (i+1) % 10);
    }

    //--QWERTYUIOP
    cXPos = 132.0f + cRenderX;
    tYPos = 355.0f;
    int cOrder[] = {SE_BTN_Q, SE_BTN_W, SE_BTN_E, SE_BTN_R, SE_BTN_T, SE_BTN_Y, SE_BTN_U, SE_BTN_I, SE_BTN_O, SE_BTN_P};
    for(int i = 0; i < 10; i ++)
    {
        mButtons[cOrder[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder[i]], "%c", cOrder[i] + 'a');
    }

    //--ASDFGHJKL
    cXPos = 156.0f + cRenderX;
    tYPos = 403.0f;
    int cOrder2[] = {SE_BTN_A, SE_BTN_S, SE_BTN_D, SE_BTN_F, SE_BTN_G, SE_BTN_H, SE_BTN_J, SE_BTN_K, SE_BTN_L};
    for(int i = 0; i < 9; i ++)
    {
        mButtons[cOrder2[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder2[i]], "%c", cOrder2[i] + 'a');
    }

    //--ZXCVBNM
    cXPos = 180.0f + cRenderX;
    tYPos = 451.0f;
    int cOrder3[] = {SE_BTN_Z, SE_BTN_X, SE_BTN_C, SE_BTN_V, SE_BTN_B, SE_BTN_N, SE_BTN_M};
    for(int i = 0; i < 7; i ++)
    {
        mButtons[cOrder3[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder3[i]], "%c", cOrder3[i] + 'a');
    }

    //--Other keys.
    mButtons[SE_BTN_CAPSLOCK].SetWH(14.0f + cRenderX, 403.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_CAPSLOCK], "Caps");

    mButtons[SE_BTN_SHIFT].SetWH(38.0f + cRenderX, 451.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SHIFT], "Shift");

    mButtons[SE_BTN_SPACE].SetWH(230.0f + cRenderX, 499.0f, 253.0f, 43.0f);
    strcpy(mBtnStrings[SE_BTN_SPACE], "Space");

    mButtons[SE_BTN_BACKSPACE].SetWH(593.0f + cRenderX, 307.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_BACKSPACE], "Undo");

    //--Smaller special keys.
    mButtons[SE_BTN_COMMA].SetWH(516.0f + cRenderX, 390.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_COMMA], ",");

    mButtons[SE_BTN_PERIOD].SetWH(564.0f + cRenderX, 390.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_PERIOD], ".");

    mButtons[SE_BTN_SLASH].SetWH(612.0f + cRenderX, 390.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SLASH], "/");

    mButtons[SE_BTN_MINUS].SetWH(516.0f + cRenderX, 198.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_MINUS], "-");

    mButtons[SE_BTN_EQUALS].SetWH(564.0f + cRenderX, 198.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_EQUALS], "=");

    mButtons[SE_BTN_BACKSLASH].SetWH(612.0f + cRenderX, 198.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_BACKSLASH], "\\");

    mButtons[SE_BTN_GRAVE].SetWH(60.0f + cRenderX, 246.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_GRAVE], "`");

    mButtons[SE_BTN_SEMICOLON].SetWH(588.0f + cRenderX, 342.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SEMICOLON], ";");

    mButtons[SE_BTN_QUOTE].SetWH(636.0f + cRenderX, 342.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_QUOTE], "'");

    mButtons[SE_BTN_LBRACE].SetWH(612.0f + cRenderX, 294.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_LBRACE], "[");

    mButtons[SE_BTN_RBRACE].SetWH(660.0f + cRenderX, 294.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_RBRACE], "]");

    //--Finishing button.
    mButtons[SE_BTN_DONE].SetWH(659.0f + cRenderX, 576.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_DONE], "Done");

    //--Cancel button.
    mButtons[SE_BTN_CANCEL].SetWH(659.0f + cRenderX, 624.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_CANCEL], "Cancel");
}
CarnStringEntry::~CarnStringEntry()
{

}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnStringEntry::Render(float pOpacity)
{
    ///--[Documentation and Setup]
    //--Renders the StringEntry object, including highlighting the selected button. This modifies the
    //  color mixer back to default when execution completes.
    if(!Images.mIsReady || !CarnImg.mIsReady || pOpacity <= 0.0f) return;

    //--Set opacity.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);

    //--Constants.
    float cRenderX = 0.0f;
    float cRenderY = 0.0f;
    float cTextX = 400.0f;
    float cTextY = 170.0f;
    float cMaxLength = 515.0f;

    //--Base.
    CarnImg.Data.rRenderBase->Draw(cRenderX, cRenderY);

    ///--[Text Parts]
    //--Current entry is the pinkish color.
    mCarnParagraph.SetAsMixerAlpha(pOpacity);

    //--Current text, if not NULL and containing at least one letter:
    bool tVeryLongString = false;
    if(mCurrentString[0] != '\0')
    {
        //--Check the length of the string.
        float cLength = CarnImg.Data.rFontCurEntry->GetTextWidth(mCurrentString);

        //--Under the mandated length, so render the string.
        if(cLength < cMaxLength)
        {
            CarnImg.Data.rFontCurEntry->DrawText(cTextX, cTextY, 0, 1.0f, mCurrentString);
        }
        //--Render "...LastPartOfString"
        else
        {
            //--Flag.
            tVeryLongString = true;

            //--Create a clone string that contains the original. Then cut the front off until it comes in
            //  under the mandated max length.
            int i = 0;
            char *rReadPoint = &mCurrentString[0];
            int tStringLen = (int)strlen(mCurrentString);
            while(cLength >= cMaxLength)
            {
                //--Check EOF case:
                if(i >= tStringLen-1) break;

                //--Advance the read point.
                i ++;
                rReadPoint = &mCurrentString[i];

                //--Recompute length.
                cLength = CarnImg.Data.rFontCurEntry->GetTextWidth(rReadPoint);
            }

            //--Render.
            CarnImg.Data.rFontCurEntry->DrawText(cTextX-20, cTextY, 0, 1.0f, "...");
            CarnImg.Data.rFontCurEntry->DrawText(cTextX, cTextY, 0, 1.0f, rReadPoint);
        }
    }
    //--Blank string.
    else
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pOpacity);
        CarnImg.Data.rFontCurEntry->DrawText(cTextX, cTextY, 0, 1.0f, mEmptyString);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pOpacity);
    }

    //--Flashing indicator and backing.
    float cLen = CarnImg.Data.rFontCurEntry->GetTextWidth(mCurrentString) * 1.0f;
    float cIndicatorX = cTextX + cLen;
    float cIndicatorY = cTextY;
    if(tVeryLongString) cIndicatorX = 970.0f;
    //CarnImg.Data.rCursor->Draw(cIndicatorX + 1.0f, cIndicatorY - 5.0f);

    //--Flashing indicator. Renders half the time.
    if(mTimer % 30 < 15)
    {
        //--Compute the indicator position.
        CarnImg.Data.rFontCurEntry->DrawText(cIndicatorX, cIndicatorY, 0, 1.0f, "_");
    }

    //--Render the prompt.
    if(mPromptString)
    {
        mCarnTitle.SetAsMixerAlpha(pOpacity);
        CarnImg.Data.rFontHeader->DrawText(683.0f, 93.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mPromptString);
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pOpacity);

    ///--[Input Clicks]
    //--Constants
    float cRenderWid = (float)CarnImg.Data.rRenderPressed->GetWidth();
    float cRenderHei = (float)CarnImg.Data.rRenderPressed->GetHeight();

    //--Show the keys pressed as slightly red.
    CarnImg.Data.rRenderPressed->Bind();
    glBegin(GL_QUADS);
    for(int i = 0; i < SE_BTN_TOTAL; i ++)
    {
        //--If the button's timer is less than the current timer, or higher than the fade value, don't render.
        if(mPressTimers[i] < mTimer) continue;

        //--Compute alpha.
        float cAlpha = (mPressTimers[i] - mTimer) / 15.0f;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pOpacity);

        //--Compute where this button is on the texture.
        float cTxL = (mButtons[i].mLft - 0.0f) / cRenderWid;
        float cTxT = (mButtons[i].mTop -  0.0f) / cRenderHei;
        float cTxR = (mButtons[i].mRgt - 0.0f) / cRenderWid;
        float cTxB = (mButtons[i].mBot -  0.0f) / cRenderHei;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex2f(mButtons[i].mLft, mButtons[i].mTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(mButtons[i].mRgt, mButtons[i].mTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(mButtons[i].mRgt, mButtons[i].mBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(mButtons[i].mLft, mButtons[i].mBot);
    }
    glEnd();
    StarlightColor::ClearMixer();

}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
StringEntry *CarnStringEntry::GenerateStringEntry()
{
    return new CarnStringEntry();
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
