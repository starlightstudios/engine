-- |[ ============================== Pandemonium Adventure Build =============================== ]|
--Operating System: Windows
--Library Type: Allegro/SDL/SDLFMOD
--Distribution: Independent
local sGameName = "Runes of Pandemonium"
local sBuildName = "Windows, Independent"

--Check existence.
if(BP_Exists(sGameName, sBuildName) == true) then return end

--Create.
BP_Create(sGameName, sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../builds/Pandemonium Adventure Win/")

-- |[ ========================================= Common ========================================= ]|
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

-- |[Basics]|
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

-- |[Executables]|
--Executable files.
BP_SetProperty("Register Command", "Copy File",   "Projects/Pandemonium/PandemoniumAL.exe",      "PandemoniumAL.exe")
BP_SetProperty("Register Command", "Copy File",   "Projects/Pandemonium/PandemoniumSDL.exe",     "PandemoniumSDL.exe")
BP_SetProperty("Register Command", "Copy File",   "Projects/Pandemonium/PandemoniumSDLFMOD.exe", "PandemoniumSDLFMOD.exe")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumAL.exe",                           "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumSDL.exe",                          "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumSDLFMOD.exe",                      "BuildFiles/IconAdv.ico")

--DLL files.
BP_SetProperty("Register Command", "Copy File", "bass.dll",    "bass.dll")
BP_SetProperty("Register Command", "Copy File", "bass_fx.dll", "bass_fx.dll")
BP_SetProperty("Register Command", "Copy File", "fmodL.dll",   "fmodL.dll")
BP_SetProperty("Register Command", "Copy File", "lua54.dll",   "lua54.dll")

-- |[Bootstraps and Counters]|
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

-- |[ =================================== Base Instructions ==================================== ]|
-- |[Text Files]|
--Relevant changelogs.
BP_SetProperty("Register Command", "Copy File", "Changelog (Adventure).txt", "Changelog.txt")

--Credits files.
BP_SetProperty("Register Command", "Copy File", "Credits.txt", "Credits.txt")

--Readme.
BP_SetProperty("Register Command", "Copy File", "Readme.txt", "Readme.txt")

-- |[Configuration Files]|
--Engine and Adventure configurations.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Adventure.lua", "Config_Adventure.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",    "Config_Engine.lua")

-- |[ =================================== Engine Subfolders ==================================== ]|
-- |[Data Directory]|
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

-- |[Saves Directory]|
--Copy the controls files.
BP_SetProperty("Register Command", "Create Directory", "Saves")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

--Copy the autosaves directory and the notifier on how to use autosaves.
BP_SetProperty("Register Command", "Create Directory", "Saves/Autosave")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/How to Recover.txt",  "Saves/Autosave/How to Recover.txt")

-- |[Shaders Directory]|
--Copy the Windows/Linux shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/WinLin", "Shaders/WinLin")

-- |[Font Precompiles]|
--Remove these from the folder, they are meant for developer use.
BP_SetProperty("Register Command", "Delete Folder", "Data/FontPrebuild/")

-- |[Delete Script Object Builder]|
BP_SetProperty("Register Command", "Delete Folder", "Data/Object Prototypes/")

-- |[ ==================================== Game Subfolders ===================================== ]|
-- |[Adventure Directories]|
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../adventure/Games/AdventureLevelGenerator",    "Games/AdventureLevelGenerator")
BP_SetProperty("Register Command", "Copy Folder", "../adventure/Games/AdventureMode",              "Games/AdventureMode")
BP_SetProperty("Register Command", "Copy Folder", "../adventure/Games/ElectrospriteTextAdventure", "Games/ElectrospriteTextAdventure")

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the active object.
DL_PopActiveObject()
