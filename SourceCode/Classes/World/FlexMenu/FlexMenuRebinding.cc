//--Base
#include "FlexMenu.h"

//--Classes
#include "AdventureMenu.h"
#include "VisualLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "SaveManager.h"

///--[Local Definitions]
#define PRIMARY_OFFSET 245.0f
#define SECONDARY_OFFSET 400.0f

///--[ ================================ FM_RebindGroup Functions ================================ ]
void FM_RebindGroup::Initialize()
{
    mLocalName = NULL;
    mControlsTotal = 0;
    mControlNames = NULL;
    mControlShow = NULL;
    mCurrentScancodes = NULL;
    mControlCoords = NULL;
}
void FM_RebindGroup::Allocate(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mControlsTotal; i ++)
    {
        free(mControlNames[i]);
        free(mControlShow[i]);
        free(mCurrentScancodes[i]);
    }
    free(mControlNames);
    free(mControlShow);
    free(mCurrentScancodes);
    free(mControlCoords);

    //--Clear.
    mControlsTotal = 0;
    mControlNames = NULL;
    mControlShow = NULL;
    mCurrentScancodes = NULL;
    mControlCoords = NULL;

    //--Range check.
    if(pTotal < 1) return;

    //--Set, allocate.
    SetMemoryData(__FILE__, __LINE__);
    mControlsTotal = pTotal;
    mControlNames     = (char **)           starmemoryalloc(sizeof(char *)           * mControlsTotal);
    mControlShow      = (char **)           starmemoryalloc(sizeof(char *)           * mControlsTotal);
    mCurrentScancodes = (int **)            starmemoryalloc(sizeof(int *)            * mControlsTotal);
    mControlCoords    = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal) * mControlsTotal);

    //--Clear.
    memset(mControlNames, 0, sizeof(char *) * mControlsTotal);
    memset(mControlShow,  0, sizeof(char *) * mControlsTotal);

    //--There are always 6 scancodes.
    for(int i = 0; i < mControlsTotal; i ++)
    {
        mCurrentScancodes[i] = (int *)starmemoryalloc(sizeof(int) * 6);
        for(int p = 0; p < 6; p ++) mCurrentScancodes[i][p] = -1;
    }
}
void FM_RebindGroup::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void FM_RebindGroup::SetControl(int pSlot, const char *pName, const char *pDisplay, TwoDimensionReal pPosition)
{
    if(pSlot < 0 || pSlot >= mControlsTotal) return;
    ResetString(mControlNames[pSlot], pName);
    ResetString(mControlShow[pSlot], pDisplay);
    memcpy(&mControlCoords[pSlot], &pPosition, sizeof(TwoDimensionReal));

    //--There are always 6 scancodes.
    ControlManager *rControlManager = ControlManager::Fetch();
    ControlState *rControlState = rControlManager->GetControlState(mControlNames[pSlot]);
    if(!rControlState) return;;

    //--Fill.
    mCurrentScancodes[pSlot][0] = rControlState->mWatchKeyPri;
    mCurrentScancodes[pSlot][1] = rControlState->mWatchMouseBtnPri;
    mCurrentScancodes[pSlot][2] = rControlState->mWatchJoyPri;
    mCurrentScancodes[pSlot][3] = rControlState->mWatchKeySec;
    mCurrentScancodes[pSlot][4] = rControlState->mWatchMouseBtnSec;
    mCurrentScancodes[pSlot][5] = rControlState->mWatchJoySec;
}
void FM_RebindGroup::DeallocateThis(void *pPtr)
{
    //--Cast and check. Note: This does not deallocate the base pointer as that is in an array, hence why the function
    //  is DeallocateThis() and not DeleteThis().
    FM_RebindGroup *rPtr = (FM_RebindGroup *)pPtr;
    if(!pPtr) return;

    //--Deallocate.
    for(int i = 0; i < rPtr->mControlsTotal; i ++)
    {
        free(rPtr->mControlNames[i]);
        free(rPtr->mControlShow[i]);
        free(rPtr->mCurrentScancodes[i]);
    }
    free(rPtr->mControlNames);
    free(rPtr->mControlShow);
    free(rPtr->mCurrentScancodes);
    free(rPtr->mControlCoords);
}

///--[ ===================================== Class Functions ==================================== ]
///--[Property Queries]
bool FlexMenu::IsRebindingMode()
{
    return mIsRebindingMode;
}

///--[Manipulators]
void FlexMenu::ActivateRebindingMode()
{
    ///--[Documentation and Setup]
    //--Builds a set of control rebinding groups, stores in FM_RebindGroup, that describe all the controls
    //  usable in the game and allow individual rebinding.
    if(!Images.mIsReady) return;

    //--Flags.
    mRebindingCursor = -1;
    mIsRebindingMode = true;
    mIsRebindSecondary = false;
    mCurrentRebindKey = -1;

    //--Position constants.
    float cLftPosition = VIRTUAL_CANVAS_X * 0.175f;
    float cTopPosition = VIRTUAL_CANVAS_Y * 0.27f;
    float cXSizePerControl = VIRTUAL_CANVAS_X * 0.65f;
    float cYSizePerControl = Images.Data.rUIFont->GetTextHeight() + (AM_MAINLINE_H * 1.0f);
    TwoDimensionReal tSampleDim;

    ///--[Create Rebind Packs]
    //--Only needs to be done the first time this is activated.
    if(mRebindPacksTotal < 1)
    {
        //--Setup.
        mActiveRebindPack = 0;
        mRebindPacksTotal = 4;
        mRebindPacks = (FM_RebindGroup *)starmemoryalloc(sizeof(FM_RebindGroup) * mRebindPacksTotal);

        ///--[Game Controls]
        mRebindPacks[0].Initialize();
        mRebindPacks[0].SetName("Game Controls");
        mRebindPacks[0].Allocate(9);
        const char *cGameNames[]   = {"Up", "Left", "Down", "Right", "Activate", "Cancel", "Run", "UpLevel",   "DnLevel"};
        const char *cGameDisplay[] = {"Up", "Left", "Down", "Right", "Activate", "Cancel", "Run", "Page Left", "Page Right"};
        for(int i = 0; i < 9; i ++)
        {
            tSampleDim.SetWH(cLftPosition, cTopPosition + (cYSizePerControl * (i-1)), cXSizePerControl, cYSizePerControl);
            mRebindPacks[0].SetControl(i, cGameNames[i], cGameDisplay[i], tSampleDim);
        }

        ///--[Field Abilities]
        mRebindPacks[1].Initialize();
        mRebindPacks[1].SetName("Field Abilities");
        mRebindPacks[1].Allocate(6);
        const char *cFieldNames[]   = {"OpenFieldAbilityMenu", "FieldAbility0", "FieldAbility1", "FieldAbility2", "FieldAbility3", "FieldAbility4"};
        const char *cFieldDisplay[] = {"Open Field Menu",      "Ability 1",     "Ability 2",     "Ability 3",     "Ability 4",     "Ability 5"};
        for(int i = 0; i < 6; i ++)
        {
            tSampleDim.SetWH(cLftPosition, cTopPosition + (cYSizePerControl * (i-1)), cXSizePerControl, cYSizePerControl);
            mRebindPacks[1].SetControl(i, cFieldNames[i], cFieldDisplay[i], tSampleDim);
        }

        ///--[UI Controls]
        mRebindPacks[2].Initialize();
        mRebindPacks[2].SetName("UI Controls");
        mRebindPacks[2].Allocate(3);
        const char *cUINames[]   = {"F2",                  "Jump",              "Ctrl"};
        const char *cUIDisplay[] = {"Toggle Descriptions", "Inspect Abilities", "(Hold) Move 10 Slots"};
        for(int i = 0; i < 3; i ++)
        {
            tSampleDim.SetWH(cLftPosition, cTopPosition + (cYSizePerControl * (i-1)), cXSizePerControl, cYSizePerControl);
            mRebindPacks[2].SetControl(i, cUINames[i], cUIDisplay[i], tSampleDim);
        }

        ///--[Debug]
        mRebindPacks[3].Initialize();
        mRebindPacks[3].SetName("UI Controls");
        mRebindPacks[3].Allocate(4);
        const char *cDebugNames[]   = {"OpenDebug",  "F5",             "RecompileShaders",  "RebuildKerning"};
        const char *cDebugDisplay[] = {"Debug Menu", "Buy All Skills", "Recompile Shaders", "Rebuild Kerning"};
        for(int i = 0; i < 4; i ++)
        {
            tSampleDim.SetWH(cLftPosition, cTopPosition + (cYSizePerControl * (i-1)), cXSizePerControl, cYSizePerControl);
            mRebindPacks[3].SetControl(i, cDebugNames[i], cDebugDisplay[i], tSampleDim);
        }
    }

    ///--[Other Buttons]
    //--Setup.
    float cPadding = 10.0f;
    float cIndent = AM_MAINLINE_H * 2.0f;
    cTopPosition = VIRTUAL_CANVAS_Y * 0.11f;
    int cTextWid = Images.Data.rUIFont->GetTextWidth(mRebindPacks[0].mLocalName) + AM_MAINLINE_H;

    //--Change-Grouping buttons. Fixed, never move.
    mRebindPackCurButton.SetWH((VIRTUAL_CANVAS_X - cTextWid) * 0.50f, cTopPosition, cTextWid + cIndent, cYSizePerControl);
    mRebindPackLftButton.SetWH(mRebindPackCurButton.mLft - cYSizePerControl - cPadding, cTopPosition, cYSizePerControl, cYSizePerControl);
    mRebindPackRgtButton.SetWH(mRebindPackCurButton.mRgt                    + cPadding, cTopPosition, cYSizePerControl, cYSizePerControl);

    //--Save and Defaults button. Fixed near the bottom of the screen.
    cTopPosition = VIRTUAL_CANVAS_Y * 0.80f;
    mSaveBtn.SetWH    (cLftPosition,                             cTopPosition, cXSizePerControl * 0.5f, cYSizePerControl);
    mDefaultsBtn.SetWH(cLftPosition + (cXSizePerControl * 0.5f), cTopPosition, cXSizePerControl * 0.5f, cYSizePerControl);
}
void FlexMenu::DeactivateRebindingMode(bool pSave)
{
    //--Deactivates rebinding mode. If pSave is true, saves the control changes. Otherwise, sets them
    //  back to what they were when the mode was activated.
    mIsRebindingMode = false;

    //--If flagged to save, run the overbind here.
    if(pSave)
    {
        //--Setup.
        ControlManager *rControlManager = ControlManager::Fetch();

        //--Run across all group packs:
        for(int i = 0; i < mRebindPacksTotal; i ++)
        {
            //--Run across all controls in this pack:
            for(int p = 0; p < mRebindPacks[i].mControlsTotal; p ++)
            {
                rControlManager->OverbindControl(mRebindPacks[i].mControlNames[p], false, mRebindPacks[i].mCurrentScancodes[p][0], mRebindPacks[i].mCurrentScancodes[p][1], mRebindPacks[i].mCurrentScancodes[p][2]);
                rControlManager->OverbindControl(mRebindPacks[i].mControlNames[p], true , mRebindPacks[i].mCurrentScancodes[p][3], mRebindPacks[i].mCurrentScancodes[p][4], mRebindPacks[i].mCurrentScancodes[p][5]);
            }
        }

        //--Write to the save file.
        SaveManager::Fetch()->SaveControlsFile();
    }

    //--Clear the packs.
    for(int i = 0; i < mRebindPacksTotal; i ++) FM_RebindGroup::DeallocateThis(&mRebindPacks[i]);
    free(mRebindPacks);

    //--Reinit.
    mRebindPacksTotal = 0;
    mRebindPacks = NULL;
}
void FlexMenu::DeactivateRebindingModeDefaults()
{
    //--Returns the controls to their default state. Also ceases rebinding mode.
    mIsRebindingMode = false;

    //--Clear the packs.
    for(int i = 0; i < mRebindPacksTotal; i ++) FM_RebindGroup::DeallocateThis(&mRebindPacks[i]);
    free(mRebindPacks);

    //--Reinit.
    mRebindPacksTotal = 0;
    mRebindPacks = NULL;

    //--Set.
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->OverbindControl("Up",       false, 84, -1, -1);
    rControlManager->OverbindControl("Up",       true,  23, -1, -1);
    rControlManager->OverbindControl("Left",     false, 82, -1, -1);
    rControlManager->OverbindControl("Left",     true,   1, -1, -1);
    rControlManager->OverbindControl("Down",     false, 85, -1, -1);
    rControlManager->OverbindControl("Down",     true,  19, -1, -1);
    rControlManager->OverbindControl("Right",    false, 83, -1, -1);
    rControlManager->OverbindControl("Right",    true,   4, -1, -1);
    rControlManager->OverbindControl("Activate", false, 26, -1, -1);
    rControlManager->OverbindControl("Activate", true,  -1, -1, -1);
    rControlManager->OverbindControl("Cancel",   false, 24, -1, -1);
    rControlManager->OverbindControl("Cancel",   true,  -1, -1, -1);
    rControlManager->OverbindControl("Run",      false,215, -1, -1);
    rControlManager->OverbindControl("Run",      true,  -1, -1, -1);
    rControlManager->OverbindControl("UpLevel",  false,  5, -1, -1);
    rControlManager->OverbindControl("UpLevel",  true,  81, -1, -1);
    rControlManager->OverbindControl("DnLevel",  false, 17, -1, -1);
    rControlManager->OverbindControl("DnLevel",  true,  80, -1, -1);
    rControlManager->OverbindControl("Field Abilities",  false, 3, -1, -1);
    rControlManager->OverbindControl("Field Abilities",  true,  -1, -1, -1);

    //--Write to the save file.
    SaveManager::Fetch()->SaveControlsFile();
}
void FlexMenu::UpdateRebindingMode()
{
    ///--[Setup]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Normal Mode]
    //--Handle mouse input.
    if(mCurrentRebindKey == -1)
    {
        //--Press Escape to go back to the loading menu.
        if(rControlManager->IsFirstPress("Escape"))
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            DeactivateRebindingMode(false);
            return;
        }

        //--Setup.
        int tOldCursor = mRebindingCursor;
        mRebindingCursor = -1;

        //--Get the mouse position.
        int tMouseX, tMouseY, tMouseZ;
        rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

        ///--[Fixed Buttons]
        //--Check the Back button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mBackBtn))
        {
            mRebindingCursor = -2;
        }
        //--Check the Save button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mSaveBtn))
        {
            mRebindingCursor = -3;
        }
        //--Check the Defaults button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mDefaultsBtn))
        {
            mRebindingCursor = -4;
        }
        //--Scroll rebind group left.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mRebindPackLftButton))
        {
            mRebindingCursor = -5;
        }
        //--Scroll rebind group right.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mRebindPackRgtButton))
        {
            mRebindingCursor = -6;
        }
        //--Otherwise, check the various control buttons.
        else
        {
            //--Get active group.
            FM_RebindGroup *rGrouping = &mRebindPacks[mActiveRebindPack];

            //--Iterate.
            for(int i = 0; i < rGrouping->mControlsTotal; i ++)
            {
                //--Check the primary button.
                if(IsPointWithin(tMouseX, tMouseY, rGrouping->mControlCoords[i].mLft + AM_MAINLINE_H + PRIMARY_OFFSET, rGrouping->mControlCoords[i].mTop + (AM_MAINLINE_H/2), rGrouping->mControlCoords[i].mLft + AM_MAINLINE_H + PRIMARY_OFFSET + 100.0f, rGrouping->mControlCoords[i].mTop + (AM_MAINLINE_H/2) + Images.Data.rUIFont->GetTextHeight()))
                {
                    mRebindingCursor = (i*2);
                }
                //--Check the secondary button.
                else if(IsPointWithin(tMouseX, tMouseY, rGrouping->mControlCoords[i].mLft + AM_MAINLINE_H + SECONDARY_OFFSET, rGrouping->mControlCoords[i].mTop + (AM_MAINLINE_H/2), rGrouping->mControlCoords[i].mLft + AM_MAINLINE_H + SECONDARY_OFFSET + 100.0f, rGrouping->mControlCoords[i].mTop + (AM_MAINLINE_H/2) + Images.Data.rUIFont->GetTextHeight()))
                {
                    mRebindingCursor = (i*2) + 1;
                }
            }
        }

        //--If the cursor changed, play a sound effect. If it's -1, don't. -2 is the back button.
        if(tOldCursor != mRebindingCursor && mRebindingCursor != -1)
        {
            //--Special: Don't play a sound if at clamp on the left button.
            if(mRebindingCursor == -5 && mActiveRebindPack == 0)
            {
            }
            //--Special: Don't play a sound if at clamp on the right button.
            else if(mRebindingCursor == -6 && mActiveRebindPack == mRebindPacksTotal-1)
            {
            }
            //--Play sound.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Clicking.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--If the cursor was on -2, go back.
            if(mRebindingCursor == -2)
            {
                DeactivateRebindingMode(false);
            }
            //--If the cursor was on -1, do nothing.
            else if(mRebindingCursor == -1)
            {
            }
            //--Save changes.
            else if(mRebindingCursor == -3)
            {
                DeactivateRebindingMode(true);
            }
            //--Restore defaults.
            else if(mRebindingCursor == -4)
            {
                DeactivateRebindingModeDefaults();
            }
            //--Go left one page.
            else if(mRebindingCursor == -5)
            {
                int tOldRebind = mActiveRebindPack;
                mActiveRebindPack --;
                if(mActiveRebindPack < 0) mActiveRebindPack = 0;
                if(tOldRebind != mActiveRebindPack) AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Go right one page.
            else if(mRebindingCursor == -6)
            {
                int tOldRebind = mActiveRebindPack;
                mActiveRebindPack ++;
                if(mActiveRebindPack >= mRebindPacksTotal) mActiveRebindPack = mRebindPacksTotal - 1;
                if(tOldRebind != mActiveRebindPack) AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--If the cursor was on any other control, begin rebinding.
            else
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

                //--Basic flags.
                mIsRebindSecondary = (mRebindingCursor % 2 == 1);

                //--Set to rebinding mode.
                mCurrentRebindKey = mRebindingCursor / 2;
            }
        }
    }
    ///--[Rebinding]
    //--Handle user inputs and rebind as necessary.
    else
    {
        //--Get active group.
        FM_RebindGroup *rGrouping = &mRebindPacks[mActiveRebindPack];

        //--Press Escape to clear the binding. The user cannot bind over Escape.
        if(rControlManager->IsFirstPress("Escape"))
        {
            //--Set the scancodes to -1's.
            int tOffset = 0;
            if(mIsRebindSecondary) tOffset = 3;
            rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 0] = -1;
            rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 1] = -1;
            rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 2] = -1;

            //--Cancel rebinding mode.
            mCurrentRebindKey = -1;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return;
        }

        //--Otherwise, wait for a keypress.
        if(rControlManager->IsAnyKeyPressed())
        {
            //--Store these scancodes. They don't get updated until the user exits this menu.
            int tKeyboard = -1;
            int tMouse = -1;
            int tJoypad = -1;
            rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoypad);

            //--Certain keys (F1, F2, F3) cannot be overbound.
            #if defined _ALLEGRO_PROJECT_
            if(tKeyboard == ALLEGRO_KEY_F1 || tKeyboard == ALLEGRO_KEY_F2 || tKeyboard == ALLEGRO_KEY_F3)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }
            #endif
            #if defined _SDL_PROJECT_
            if(tKeyboard == SDL_SCANCODE_F1 || tKeyboard == SDL_SCANCODE_F2 || tKeyboard == SDL_SCANCODE_F3)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }
            #endif


            //--Store these scancodes. They don't get updated until the user exits this menu.
            int tOffset = 0;
            if(mIsRebindSecondary) tOffset = 3;
            rControlManager->GetKeyPressCodes(rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 0], rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 1], rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 2]);
            //fprintf(stderr, "KMJ: %i %i %i\n", rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 0], rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 1], rGrouping->mCurrentScancodes[mCurrentRebindKey][tOffset + 2]);

            //--Cancel rebinding mode.
            mCurrentRebindKey = -1;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return;
        }
    }
}
void FlexMenu::RenderRebindingMode()
{
    ///--[Documentation and Setup]
    //--Renders Rebinding mode. Duh.
    if(!Images.mIsReady || mRebindPacksTotal < 1) return;

    //--Setup.
    char tBuffer[128];

    //--Colors.
    StarlightColor cBackingColorGrey = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.85f);
    StarlightColor cBackingColorBlue = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.4f, 0.85f);
    StarlightColor *rUseColor = &cBackingColorGrey;

    ///--[Back Button]
    if(mRebindingCursor == -2) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mBackBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mBackBtn.mLft + AM_MAINLINE_H - 9.0f, mBackBtn.mTop + AM_MAINLINE_H - 9.0f, 0, 1.0f, "Back");

    ///--[Header]
    //--Render some instructions.
    strcpy(tBuffer, "Click a key to edit.");
    float cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * 3.0f * 0.5f;
    float cTextHei = Images.Data.rUIFont->GetTextHeight() * 3.0f * 0.5f;
    Images.Data.rUIFont->DrawText((VIRTUAL_CANVAS_X * 0.5f) - cTextWid, (VIRTUAL_CANVAS_Y * 0.08f) - cTextHei, 0, 3.0f, tBuffer);

    ///--[Group Listing]
    //--Render the name of the active grouping.
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mRebindPackCurButton, 0x01FF);
    Images.Data.rUIFont->DrawText(mRebindPackCurButton.mLft + AM_MAINLINE_H, mRebindPackCurButton.mTop + (AM_MAINLINE_H/2), 0, 1.0f, mRebindPacks[mActiveRebindPack].mLocalName);

    //--Left and Right buttons.
    if(mActiveRebindPack > 0)
    {
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mRebindPackLftButton, 0x01FF);
        Images.Data.rUIFont->DrawText(mRebindPackLftButton.mLft + AM_MAINLINE_H/2, mRebindPackLftButton.mTop + (AM_MAINLINE_H/2), 0, 1.0f, "<<");
    }
    if(mActiveRebindPack < mRebindPacksTotal-1)
    {
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mRebindPackRgtButton, 0x01FF);
        Images.Data.rUIFont->DrawText(mRebindPackRgtButton.mLft + AM_MAINLINE_H/2, mRebindPackRgtButton.mTop + (AM_MAINLINE_H/2), 0, 1.0f, ">>");
    }

    ///--[List of Controls]
    //--Point.
    FM_RebindGroup *rGroup = &mRebindPacks[mActiveRebindPack];

    //--Render.
    for(int i = 0; i < rGroup->mControlsTotal; i ++)
    {
        //--Render a button around the object.
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, rGroup->mControlCoords[i], 0x01FF);

        //--Name of the control.
        glColor3f(1.0f, 1.0f, 1.0f);
        Images.Data.rUIFont->DrawText(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2), 0, 1.0f, rGroup->mControlShow[i]);

        //--If the primary scancode is selected...
        if(mRebindingCursor == (i*2))
            glColor3f(0.0f, 0.0f, 1.0f);
        else
            glColor3f(1.0f, 1.0f, 1.0f);

        //--Primary scancode.
        char tLetter = 'K';
        int tUseScancode = rGroup->mCurrentScancodes[i][0];
        if(tUseScancode == -1) {tLetter = 'M'; tUseScancode = rGroup->mCurrentScancodes[i][1]; }
        if(tUseScancode == -1) {tLetter = 'J'; tUseScancode = rGroup->mCurrentScancodes[i][2]; }

        //--If the scancode is still -1, print "Unbound" instead of the number.
        if(tUseScancode == -1)
        {
            Images.Data.rUIFont->DrawText(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H + PRIMARY_OFFSET, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2), 0, 1.0f, "Unbound");
        }
        //--Print the scancode, or an image if we found it.
        else
        {
            //--Keyboard:
            if(tLetter == 'K')
            {
                //--Check to see if an image exists to describe this scancode.
                StarBitmap *rErrImg = ControlManager::Fetch()->GetErrorImage();
                StarBitmap *rUseImg = ControlManager::Fetch()->ResolveScancodeImage(tUseScancode);
                if(rErrImg != rUseImg)
                {
                    rUseImg->Draw(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H + PRIMARY_OFFSET, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2));
                }
                //--Show the scancode directly.
                else
                {
                    Images.Data.rUIFont->DrawTextArgs(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H + PRIMARY_OFFSET, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2), 0, 1.0f, "%c: %i", tLetter, tUseScancode);
                }
            }
            //--Mouse/Joypad:
            else
            {
                Images.Data.rUIFont->DrawTextArgs(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H + PRIMARY_OFFSET, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2), 0, 1.0f, "%c: %i", tLetter, tUseScancode);
            }
        }

        //--If the secondary scancode is selected...
        if(mRebindingCursor == (i*2) + 1)
            glColor3f(0.0f, 0.0f, 1.0f);
        else
            glColor3f(1.0f, 1.0f, 1.0f);

        //--Secondary scancode.
        tLetter = 'K';
        tUseScancode = rGroup->mCurrentScancodes[i][3];
        if(tUseScancode == -2) {tLetter = 'M'; tUseScancode = rGroup->mCurrentScancodes[i][4]; }
        if(tUseScancode == -2) {tLetter = 'J'; tUseScancode = rGroup->mCurrentScancodes[i][5]; }

        //--If the scancode is still -1, print "Unbound" instead of the number.
        if(tUseScancode == -1)
        {
            Images.Data.rUIFont->DrawText(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H + SECONDARY_OFFSET, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2), 0, 1.0f, "Unbound");
        }
        //--Print the scancode, or an image if we found it.
        else
        {
            //--Check to see if an image exists to describe this scancode.
            StarBitmap *rErrImg = ControlManager::Fetch()->GetErrorImage();
            StarBitmap *rUseImg = ControlManager::Fetch()->ResolveScancodeImage(tUseScancode);
            if(rErrImg != rUseImg)
            {
                rUseImg->Draw(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H + SECONDARY_OFFSET, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2));
            }
            //--Show the scancode directly.
            else
            {
                Images.Data.rUIFont->DrawTextArgs(rGroup->mControlCoords[i].mLft + AM_MAINLINE_H + SECONDARY_OFFSET, rGroup->mControlCoords[i].mTop + (AM_MAINLINE_H/2), 0, 1.0f, "%c: %i", tLetter, tUseScancode);
            }
        }

        //--Clean.
        glColor3f(1.0f, 1.0f, 1.0f);
    }

    ///--[Save Button]
    rUseColor = &cBackingColorGrey;
    if(mRebindingCursor == -3) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mSaveBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mSaveBtn.mLft + AM_MAINLINE_H, mSaveBtn.mTop + (AM_MAINLINE_H/2), 0, 1.0f, "Save Changes");

    ///--[Defaults Button]
    rUseColor = &cBackingColorGrey;
    if(mRebindingCursor == -4) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mDefaultsBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mDefaultsBtn.mLft + AM_MAINLINE_H, mDefaultsBtn.mTop + (AM_MAINLINE_H/2), 0, 1.0f, "Defaults");

    ///--[Rebinding Mode]
    //--Render a box over the screen prompting the user to push a key.
    glColor3f(1.0f, 1.0f, 1.0f);
    if(mCurrentRebindKey != -1)
    {
        //--Setup.
        float cTextSize = 2.0f;

        //--Black underlay.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, 0.85f);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();

        //--Box.
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, VIRTUAL_CANVAS_X * 0.00f, VIRTUAL_CANVAS_Y * 0.36f, VIRTUAL_CANVAS_X * 1.00f, VIRTUAL_CANVAS_Y * 0.50f, 0x01FF);

        //--Text.
        strcpy(tBuffer, "Press the key/joypad/mouse button to bind with.");
        cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * cTextSize * 0.5f;
        cTextHei = Images.Data.rUIFont->GetTextHeight()       * cTextSize * 0.5f;
        Images.Data.rUIFont->DrawText((VIRTUAL_CANVAS_X * 0.5f) - cTextWid, (VIRTUAL_CANVAS_Y * 0.40f) - cTextHei, 0, cTextSize, tBuffer);

        //--How to clear.
        strcpy(tBuffer, "Press Escape to clear.");
        cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * cTextSize * 0.5f;
        cTextHei = Images.Data.rUIFont->GetTextHeight()       * cTextSize * 0.5f;
        Images.Data.rUIFont->DrawText((VIRTUAL_CANVAS_X * 0.5f) - cTextWid, (VIRTUAL_CANVAS_Y * 0.40f) - cTextHei + (Images.Data.rUIFont->GetTextHeight() * cTextSize), 0, cTextSize, tBuffer);
    }
}
