//--[Color Inversion]
//--Shader used for Glitch cases. Inverts the colors. Looks creepy.
#version 120
void main(void)
{
    //--Provide the current texture coordinates for the fragment half of the
    //  shader.
    gl_TexCoord[0] = gl_MultiTexCoord0;

    //--Standard transformation.
    gl_Position = ftransform();
}
