//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdvCombat.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///========================================== System ==============================================
AdventureInventory::AdventureInventory()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTUREINVENTORY;

    ///--[AdventureInventory]
    //--System
    mCurrentSort = AINV_SORT_CRITERIA_NAME;
    mWasAnythingCreated = false;

    //--Paths
    mGemNameResolvePath = NULL;

    //--Crafting Materials
    mPlatina = 0;
    memset(mCraftingItemCounts, 0, sizeof(int) * CRAFT_ADAMANTITE_TOTAL);

    //--Catalysts
    memset(mCatalystCounts,           0, sizeof(int) * CATALYST_TOTAL);
    memset(mCatalystCountsLocalCur,   0, sizeof(int) * CATALYST_TOTAL);
    memset(mCatalystCountsLocalTotal, 0, sizeof(int) * CATALYST_TOTAL);

    //--Upgrading
    mIsUpgradeable = false;

    //--Storage List
    mItemList = new StarLinkedList(true);
    mExtendedItemList = new StarLinkedList(false);
    mGemList = new StarLinkedList(false);
    mGemMergeList = new StarLinkedList(false);

    //--Doctor Bag
    mIsDoctorBagEnabled = false;
    mDoctorBagCharges = 150;
    mDoctorBagChargesMax = 150;
    mDoctorBagChargeRate = 1.0f;
    mDoctorBagPotency = 1.0f;

    //--Shop Buyback
    mShopBuybackList = new StarLinkedList(true);

    //--Gems
    rMarkedEquipment = NULL;
    rMasterGem = NULL;
    rSocketingItem = NULL;

    //--Public Variables
    rLastReggedItem = NULL;
    mBlockStackingOnce = false;
}
AdventureInventory::~AdventureInventory()
{
    free(mGemNameResolvePath);
    delete mItemList;
    delete mExtendedItemList;
    delete mGemList;
    delete mGemMergeList;
    delete mShopBuybackList;
}

///--[Public Statics]
//--Item used to show properties on the forge menu. When needed, the item registration script is called
//  as normal but the item is registered to this static slot, which can be queried by the menu to
//  show its properties as compared to the original. Item should be deleted and NULL-ed externally!
AdventureItem *AdventureInventory::xUpgradeItem = NULL;

//--Image path of the last query run through AdventureLevel::xItemImageListPath.
char *AdventureInventory::xLastItemImagePath = NULL;
StarBitmap *AdventureInventory::xrLastRegisteredIcon = NULL;

//--If true, items are going to a shop, not the inventory. Needed for achievements.
bool AdventureInventory::xIsRegisteringItemsToShop = false;

///===================================== Property Queries =========================================
const char *AdventureInventory::GetGemNameResolvePath()
{
    return mGemNameResolvePath;
}
int AdventureInventory::GetItemCount()
{
    return mItemList->GetListSize();
}
int AdventureInventory::GetCountOf(const char *pName)
{
    //--Error check.
    if(!pName) return 0;

    //--Adamantite Check:
    if(!strcasecmp(pName, "Adamantite Powder")) return mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER];
    if(!strcasecmp(pName, "Adamantite Flakes")) return mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES];
    if(!strcasecmp(pName, "Adamantite Shard"))  return mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD];
    if(!strcasecmp(pName, "Adamantite Piece"))  return mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE];
    if(!strcasecmp(pName, "Adamantite Chunk"))  return mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK];
    if(!strcasecmp(pName, "Adamantite Ore"))    return mCraftingItemCounts[CRAFT_ADAMANTITE_ORE];

    //--Other items:
    int tRunningTotal = 0;
    AdventureItem *rCheckItem = (AdventureItem *)mItemList->PushIterator();
    while(rCheckItem)
    {
        //--Name check.
        if(!strcasecmp(rCheckItem->GetName(), pName))
        {
            //--Stackables just return the stack count immediately.
            if(rCheckItem->IsStackable())
            {
                mItemList->PopIterator();
                return rCheckItem->GetStackSize();
            }

            //--Otherwise, increment the total and keep going.
            tRunningTotal ++;
        }

        //--Next.
        rCheckItem = (AdventureItem *)mItemList->AutoIterate();
    }

    return tRunningTotal;
}
int AdventureInventory::GetCountOfEquip(const char *pName)
{
    ///--[Documentation]
    //--Gets the total of the named item, and adds the number of that item that are equipped on
    //  active party members, if any.
    if(!pName) return 0;

    //--First, get the inventory count.
    int tTotal = GetCountOf(pName);

    //--Setup.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int tPartyCount = rAdventureCombat->GetActivePartyCount();

    //--Iterate.
    for(int i = 0; i < tPartyCount; i ++)
    {
        //--Get entity.
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        //--Scan equipment.
        int tEquipmentSlots = rEntity->GetEquipmentSlotsTotal();
        for(int p = 0; p < tEquipmentSlots; p ++)
        {
            //--Get item in slot.
            EquipmentSlotPack *rPackage = rEntity->GetEquipmentSlotPackageI(p);
            if(!rPackage || !rPackage->mEquippedItem) continue;

            //--Check the name.
            if(!strcasecmp(rPackage->mEquippedItem->GetName(), pName))
            {
                tTotal ++;
            }
        }
    }

    //--Finish up.
    return tTotal;
}
int AdventureInventory::GetPlatina()
{
    return mPlatina;
}
int AdventureInventory::GetCraftingCount(int pSlot)
{
    if(pSlot < 0 || pSlot >= CRAFT_ADAMANTITE_TOTAL) return 0;
    return mCraftingItemCounts[pSlot];
}
int AdventureInventory::GetDoctorBagCharges()
{
    return mDoctorBagCharges;
}
int AdventureInventory::GetDoctorBagChargesMax()
{
    return mDoctorBagChargesMax;
}
float AdventureInventory::GetDoctorBagChargeRate()
{
    return mDoctorBagChargeRate;
}
float AdventureInventory::GetDoctorBagPotency()
{
    return mDoctorBagPotency;
}

///======================================== Manipulators ==========================================
void AdventureInventory::SetGemNameResolvePath(const char *pPath)
{
    ResetString(mGemNameResolvePath, pPath);
}
bool AdventureInventory::RegisterItem(AdventureItem *pItem)
{
    ///--[Documentation]
    //--Search through the item list. If another item exists with the same name, and is stackable,
    //  then increment the stack for that item instead and delete the old item.
    //--Note: This algorithm will not spot items that are still being created because their stackable
    //  property will never be true (yet). This will only catch instances where an item was already
    //  fully formed and is being registered, such as when changing equipment or buying from a shop.
    //--Returns true if the item was successfully registered. If it was not, returns false, and indicates
    //  that the item pointer is unstable.
    if(!pItem) return false;
    mWasAnythingCreated = true;

    //--Iterm is stackable. Check if there's an existing entry.
    if(pItem->IsStackable() && !mBlockStackingOnce)
    {
        //--Iterate.
        AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
        while(rItem)
        {
            //--If the item in question is stackable and has the same name:
            if(rItem->IsStackable() && !strcasecmp(pItem->GetName(), rItem->GetName()))
            {
                //--Add to the stack.
                rItem->SetStackSize(rItem->GetStackSize() + 1);
                rLastReggedItem = rItem;

                //--Delete the item itself.
                delete pItem;

                //--Clean.
                mWasAnythingCreated = false;
                mItemList->PopIterator();
                return false;
            }

            //--Next item.
            rItem = (AdventureItem *)mItemList->AutoIterate();
        }
    }

    //--No matching stackables, so create a new instance.
    mItemList->AddElementAsTail(pItem->GetName(), pItem, &RootObject::DeleteThis);
    rLastReggedItem = pItem;
    return true;
}
void AdventureInventory::RegisterAdamantite(AdventureItem *pItem, bool pDeleteItem)
{
    //--Registers the item as if it was an adamantite crafting ingredient.
    if(!pItem) return;

    //--Fast-access pointers.
    const char *rItemName = pItem->GetName();

    //--Check the constant names. Add a crafting ingredient on a match.
    if(!strcasecmp(rItemName, "Adamantite Powder"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Flakes"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Shard"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Piece"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Chunk"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Ore"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_ORE] ++;
    }

    //--If flagged, the item is deleted. Adamantite purchased from shops is not deleted!
    if(pDeleteItem) delete pItem;
}
void AdventureInventory::SetPlatina(int pAmount)
{
    mPlatina = pAmount;
    if(mPlatina < 0) mPlatina = 0;
}
void AdventureInventory::SetCraftingMaterial(int pSlot, int pAmount)
{
    if(pSlot < 0 || pSlot >= CRAFT_ADAMANTITE_TOTAL || pAmount < 0) return;
    mCraftingItemCounts[pSlot] = pAmount;
}
bool AdventureInventory::RemoveItem(const char *pName)
{
    ///--[Documentation]
    //--Dumps the item out and deallocates it. Remember to remove any lingering references!
    //  Removes the first instance of the item found, in case of duplicates.
    if(!pName) return false;

    ///--[Adamantite]
    //--Check if this is an adamantite type! If so, those are removed as needed.
    if(!strcasecmp(pName, "Adamantite Powder"))
    {
        if(mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER] < 1) return false;

        mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER] --;
        return true;
    }
    else if(!strcasecmp(pName, "Adamantite Flakes"))
    {
        if(mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES] < 1) return false;

        mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES] --;
        return true;
    }
    else if(!strcasecmp(pName, "Adamantite Shard"))
    {
        if(mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD] < 1) return false;

        mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD] --;
        return true;
    }
    else if(!strcasecmp(pName, "Adamantite Piece"))
    {
        if(mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE] < 1) return false;

        mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE] --;
        return true;
    }
    else if(!strcasecmp(pName, "Adamantite Chunk"))
    {
        if(mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK] < 1) return false;

        mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK] --;
        return true;
    }
    else if(!strcasecmp(pName, "Adamantite Ore"))
    {
        if(mCraftingItemCounts[CRAFT_ADAMANTITE_ORE] < 1) return false;

        mCraftingItemCounts[CRAFT_ADAMANTITE_ORE] --;
        return true;
    }

    ///--[Items]
    //--First, find the first instance of the item. If it's stackable, we need to decrement the stack.
    //  If the item is not found, return false to indicate there are none.
    AdventureItem *rItem = (AdventureItem *)mItemList->GetElementByName(pName);
    if(!rItem) return false;

    //--Stackable? Decrement.
    if(rItem->IsStackable() && rItem->GetStackSize() > 1)
    {
        rItem->SetStackSize(rItem->GetStackSize() - 1);
    }
    //--Not stackable, or only one item. Remove it.
    else
    {
        mItemList->RemoveElementP(rItem);
        if(rLastReggedItem == rItem) rLastReggedItem = NULL;
        if(rMarkedEquipment== rItem) rMarkedEquipment = NULL;
        if(rMasterGem      == rItem) rMasterGem = NULL;
        if(rSocketingItem  == rItem) rSocketingItem = NULL;
    }

    //--Return true to indicate we removed something.
    return true;
}
void AdventureInventory::SetDoctorBagCharges(int pAmount)
{
    //--Set.
    mDoctorBagCharges = pAmount;
    if(mDoctorBagCharges < 0) mDoctorBagCharges = 0;
    if(mDoctorBagCharges > mDoctorBagChargesMax) mDoctorBagCharges = mDoctorBagChargesMax;

    //--Update the DataLibrary. This is used for saving/loading only.
    SysVar *rDoctorBagVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iDoctorBagCharges");
    if(rDoctorBagVariable) rDoctorBagVariable->mNumeric = (float)pAmount;
}
void AdventureInventory::SetDoctorBagMaxCharges(int pAmount)
{
    //--Set.
    mDoctorBagChargesMax = pAmount;
    if(mDoctorBagChargesMax < 0) mDoctorBagChargesMax = 0;
    if(mDoctorBagCharges > mDoctorBagChargesMax) mDoctorBagCharges = mDoctorBagChargesMax;

    //--Update the DataLibrary. This is used for saving/loading only.
    SysVar *rDoctorBagVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iDoctorBagChargesMax");
    if(rDoctorBagVariable) rDoctorBagVariable->mNumeric = (float)pAmount;
}
void AdventureInventory::SetDoctorBagChargeRate(float pRate)
{
    mDoctorBagChargeRate = pRate;
    if(mDoctorBagChargeRate < 0.0f) mDoctorBagChargeRate = 0.0f;
}
void AdventureInventory::SetDoctorBagPotency(float pPotency)
{
    mDoctorBagPotency = pPotency;
    if(mDoctorBagPotency < 1.0f) mDoctorBagPotency = 1.0f;
}
void AdventureInventory::MarkLastItemQuantity(int pQuantity)
{
    //--Specifies the last registered item's quantity. This should only be used when loading the game,
    //  or if the caller is 100% sure the item can have its quantity specified. Otherwise, just create
    //  the item and call the stacking routine.
    if(!rLastReggedItem || pQuantity < 1) return;

    //--Cast.
    AdventureItem *rCastedItem = (AdventureItem *)rLastReggedItem;

    //--Set.
    rCastedItem->SetStackSize(pQuantity);
}
void AdventureInventory::MarkLastItemAsMasterGem()
{
    //--Marks the last-created item in the inventory as the "master" gem. Subgems will be merged with
    //  the master gem when told to with MergeLastItemWithMasterGem().
    //--This is used for the loading handler when reassembling gems.
    rMasterGem = (AdventureItem *)rLastReggedItem;
}
void AdventureInventory::MergeLastItemWithMasterGem()
{
    //--Merge the last registered gem with the master gem, assuming both exist.
    if(!rMasterGem || !rLastReggedItem || (rMasterGem == rLastReggedItem)) return;
    bool tMerged = rMasterGem->MergeWithGem((AdventureItem *)rLastReggedItem);
    if(tMerged)
    {
        LiberateItemP(rLastReggedItem);
        rLastReggedItem = rMasterGem;
    }
}
void AdventureInventory::ClearMasterGem()
{
    //--Clean up after done merging gems.
    rMasterGem = NULL;
}
void AdventureInventory::MarkLastItemAsSocketItem()
{
    //--As above, used for socketing gems into equipped items during loading.
    rSocketingItem = (AdventureItem *)rLastReggedItem;
}
void AdventureInventory::SocketLastItemInSocketItem()
{
    if(!rSocketingItem || !rLastReggedItem) return;
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        if(rSocketingItem->GetGemInSlot(i)) continue;
        rSocketingItem->PlaceGemInSlot(i, (AdventureItem *)rLastReggedItem);
        LiberateItemP(rLastReggedItem);
        return;
    }
}
void AdventureInventory::ClearSocketItem()
{
    rSocketingItem = NULL;
}
void AdventureInventory::MarkLastItemAsEquipItem()
{
    rMarkedEquipment = (AdventureItem *)rLastReggedItem;
}
void AdventureInventory::ClearLastEquipItem()
{
    rMarkedEquipment = NULL;
}
void AdventureInventory::ClearBuyback()
{
    mShopBuybackList->ClearList();
}

///======================================== Core Methods ==========================================
void AdventureInventory::Clear()
{
    ///--[Documentation]
    //--Clear the inventory back to a factory-zero state. Do not use this while it has any pointers
    //  outstanding or you will quite-obviously crash the program.

    //--System
    mWasAnythingCreated = false;

    //--Crafting Materials
    mPlatina = 0;
    memset(mCraftingItemCounts, 0, sizeof(int) * CRAFT_ADAMANTITE_TOTAL);

    //--Catalysts
    memset(mCatalystCounts, 0, sizeof(int) * CATALYST_TOTAL);

    //--Storage List
    mItemList->ClearList();
    mExtendedItemList->ClearList();
    mGemList->ClearList();
    mGemMergeList->ClearList();
    mShopBuybackList->ClearList();

    //--Public Variables
    rLastReggedItem = NULL;
    rMarkedEquipment = NULL;
    rMasterGem = NULL;
    rSocketingItem = NULL;
}
int AdventureInventory::IsItemEquipped(const char *pItemName)
{
    //--Returns if the item is equipped by any character. Returns how many times it was equipped, if any. Can be 0.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int cRosterCount = rAdventureCombat->GetRosterCount();

    int tCount = 0;
    for(int i = 0; i < cRosterCount; i ++)
    {
        //--Check that the party member exists.
        AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberI(i);
        if(!rEntity) continue;

        //--Check their equipment.
        //TODO FOR BBP
    }

    //--Character not found.
    return tCount;
}
int AdventureInventory::IsItemEquippedBy(const char *pCharacter, const char *pItemName)
{
    //--Returns how many times an item is equipped by the named character. Can be 0 if they don't have it.
    int tFoundCount = 0;
    if(!pCharacter || !pItemName) return tFoundCount;
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Locate the character:
    AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberS(pCharacter);
    if(!rEntity) return tFoundCount;

    //--Parse the slots.
    int tEquipSlotsTotal = rEntity->GetEquipmentSlotsTotal();
    for(int i = 0; i < tEquipSlotsTotal; i ++)
    {
        //--Get and check.
        AdventureItem *rItem = rEntity->GetEquipmentBySlotI(i);
        if(!rItem) continue;

        //--See if the name matches.
        if(!strcasecmp(rItem->GetName(), pItemName)) tFoundCount ++;
    }

    //--Pass back however many we found.
    return tFoundCount;
}
bool AdventureInventory::IsAnyItemEquippedByIn(const char *pCharacter, const char *pSlotName)
{
    //--Returns true if there is any item equipped by the given character in the given slot, false if it's empty.
    if(!pCharacter || !pSlotName) return false;
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Locate the character:
    AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberS(pCharacter);
    if(!rEntity) return false;

    //--Get and check.
    AdventureItem *rItem = rEntity->GetEquipmentBySlotS(pSlotName);
    return (rItem != NULL);
}
void *AdventureInventory::xrDummyUnequipItem = new AdventureItem();
void *AdventureInventory::xrDummyGemsItem = new AdventureItem();
void AdventureInventory::BuildEquippableList(AdvCombatEntity *pEntity, const char *pEquipName, StarLinkedList *pList)
{
    //--Given a character and a list, modifes the list to include all the items that the character
    //  can equip in the given slot. Does not clear the list inherently, just appends.
    //--Deallocation should not be active for the given list.
    if(!pEntity || !pList || !pEquipName) return;

    //--Get the equipment slot package.
    EquipmentSlotPack *rEquipmentSlotPack = pEntity->GetEquipmentSlotPackageS(pEquipName);
    if(!rEquipmentSlotPack) return;

    //--If the slot is able to be empty, add the "Unequip" option. There must be a piece of equipment in the slot first.
    if(rEquipmentSlotPack->mEquippedItem  && rEquipmentSlotPack->mCanBeEmpty)
    {
        pList->AddElementAsTail("Unequip", xrDummyUnequipItem);
        ((AdventureItem *)xrDummyUnequipItem)->SetOverrideQuality(100001.0f);
    }

    //--If the currently equipped item has at least one gem slot, put the gem-change item on it.
    /*if(rEquipmentSlotPack->mEquippedItem && rEquipmentSlotPack->mEquippedItem->GetGemSlots() > 0)
    {
        pList->AddElementAsTail("Change Gems", xrDummyGemsItem);
        ((AdventureItem *)xrDummyUnequipItem)->SetOverrideQuality(100000.0f);
    }*/

    //--Scan.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        //--Item must be equippable by this character.
        if(rItem->IsEquippableBy(pEntity->GetName()) && rItem->IsEquippableIn(pEquipName))
        {
            pList->AddElementAsTail("X", rItem);
        }

        //--Next.
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }
}
void AdventureInventory::BuildExtendedItemList()
{
    //--Creates a list of all items currently held by the player, including ones that are equipped.
    //  The items that are equipped are denoted by having a named entry, while the ones in the inventory
    //  have "Null" for a name.
    mExtendedItemList->ClearList();

    //--Party list. Iterate across the roster.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int cRosterSize = rAdventureCombat->GetActivePartyCount();
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Get the entity in question.
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        //--Append all equipment to the list.
        int tEquipSlots = rEntity->GetEquipmentSlotsTotal();
        for(int p = 0; p < tEquipSlots; p ++)
        {
            AdventureItem *rItem = rEntity->GetEquipmentBySlotI(p);
            if(rItem) mExtendedItemList->AddElementAsTail(rEntity->GetName(), rItem);
        }
    }

    //--Now add the regular items.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        mExtendedItemList->AddElementAsTail("Null", rItem);
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }
}
void AdventureInventory::BuildGemList()
{
    ///--[Documentation]
    //--Creates a list of all items currently held by the player, including ones socketed into a piece
    //  of equipment. Socketed gems are denoted with the owning character's name. Gems that are in
    //  the inventory have the string "Null" for a name.
    mGemList->ClearList();

    //--Party list. Iterate across the roster.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int cRosterSize = rAdventureCombat->GetActivePartyCount();
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Get the entity in question.
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        //--Scan all equipment.
        int tSlotsTotal = rEntity->GetEquipmentSlotsTotal();
        for(int p = 0; p < tSlotsTotal; p ++)
        {
            AdventureItem *rSlotItem = rEntity->GetEquipmentBySlotI(p);
            if(!rSlotItem) continue;

            int tGemSlots = rSlotItem->GetGemSlots();
            for(int o = 0; o < tGemSlots; o ++)
            {
                AdventureItem *rGem = rSlotItem->GetGemInSlot(o);
                if(rGem)
                {
                    mGemList->AddElementAsTail(rEntity->GetName(), rGem);
                }
            }
        }
    }

    //--Now add the regular items.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        if(rItem->IsGem()) mGemList->AddElementAsTail("Null", rItem);
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }
}
void AdventureInventory::BuildGemListNoEquip()
{
    //--Builds a list of all gems that are only in the inventory, not equipped. Used when swapping gems out.
    mGemList->ClearList();

    //--Zeroth element is the unequip entry.
    mGemList->AddElementAsTail("Unequip", xrDummyUnequipItem);
    ((AdventureItem *)xrDummyUnequipItem)->SetOverrideQuality(100001.0f);

    //--Now build all the items.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        if(rItem->IsGem()) mGemList->AddElementAsTail("Null", rItem);
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }

    //--Sort the list.
    SortListByCriteria(mGemList, AINV_SORT_CRITERIA_VALUE, true);
}
StarLinkedList *AdventureInventory::BuildGemMergeList(AdventureItem *pGem)
{
    //--Builds a list of gems that can be merged with the provided gem. The returned list is the
    //  local mGemMergeList which may have zero entries.
    mGemMergeList->ClearList();
    if(!pGem) return mGemMergeList;

    //--Iterate across the items:
    //fprintf(stderr, "Building gem merge list.\n");
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        //--If the gem is the one passed in, skip it.
        if(pGem == rItem)
        {
            //fprintf(stderr, " Highlight gem is query gem.\n");
        }
        //--If the item is a gem:
        else if(rItem->IsGem())
        {
            //--Get its color and check for exclusion.
            uint8_t tGemColor = rItem->GetGemColors();
            if((tGemColor & pGem->GetGemColors()) == 0)
            {
                mGemMergeList->AddElementAsTail("X", rItem);
                //fprintf(stderr, " Success %i vs %i\n", tGemColor, pGem->GetGemColors());
            }
            else
            {
                //fprintf(stderr, " Failed %i vs %i\n", tGemColor, pGem->GetGemColors());
            }
        }

        //--Next.
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }

    //--Pass it back for easy access.
    return mGemMergeList;
}
AdventureItem *AdventureInventory::LocateItemByItemID(uint32_t pSearchID)
{
    //--Attempts to locate the item in the item list based on its ItemUniqueID. This routine is
    //  explicitly coded to be used during loading. It searches the player's equipment, the sockets
    //  in the player's equipment, and the base inventory. The first item found with the matching
    //  ID is returned, or NULL if no item is found.
    //--While it is not normally logically possible for an item to have a duplicate ID, it could
    //  happen as the result of a bug. In those cases, the first item found is returned.
    //--Does not work with stackable items, so don't use those.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Party list. Holds all characters regardless of who's in the party.
    int cRosterSize = rAdventureCombat->GetActivePartyCount();
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Check the entity.
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        //--Check all equipment slots for the item.
        //TODO FOR BBP
    }

    //--Not on a character. Check the main list.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        //--Match.
        if(rItem->GetItemUniqueID() == pSearchID)
        {
            mItemList->PopIterator();
            return rItem;
        }


        //--Next.
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }

    //--No matches.
    return NULL;
}
void AdventureInventory::StackItems()
{
    ///--[Documentation]
    //--Stacks identical, stackable items together into one slot. Each time a stack occurs, we need
    //  to restart the process in order to maintain pointer consistency.

    //--This flag allows blocking of stacking. It is used when creating items for other inventories,
    //  such as shop vendor items.
    if(mBlockStackingOnce)
    {
        return;
    }

    //--Proceed with stacking!
    bool tAtLeastOneRestack = true;
    while(tAtLeastOneRestack)
    {
        //--Flag reset.
        tAtLeastOneRestack = false;

        //--Iterate.
        AdventureItem *rFirstItem = (AdventureItem *)mItemList->PushIterator();
        while(rFirstItem)
        {
            //--If this item is stackable:
            if(rFirstItem->IsStackable())
            {
                //--Iterate ahead. Liberate any matches.
                mItemList->SetRandomPointerToThis(rFirstItem);
                AdventureItem *rSecondItem = (AdventureItem *)mItemList->IncrementAndGetRandomPointerEntry();

                //--Iterate across the list.
                while(rSecondItem)
                {
                    //--Item is stackable and shares a name with the first item:
                    if(rSecondItem->IsStackable() && !strcasecmp(rFirstItem->GetName(), rSecondItem->GetName()))
                    {
                        //--Liberate second entry, +1 stack for the first entry.
                        rFirstItem->SetStackSize(rFirstItem->GetStackSize() + 1);
                        mItemList->RemoveRandomPointerEntry();
                        if(rLastReggedItem == rSecondItem) rLastReggedItem = NULL;
                        if(rMasterGem      == rSecondItem) rMasterGem = NULL;
                        if(rSocketingItem  == rSecondItem) rSocketingItem = NULL;

                        //--Flag gets tripped.
                        tAtLeastOneRestack = true;
                        break;
                    }
                    else
                    {
                        //fprintf(stderr, "   Not stackable with this item.\n");
                    }

                    //--Next item.
                    rSecondItem = (AdventureItem *)mItemList->IncrementAndGetRandomPointerEntry();
                }
            }

            //--If a re-order occurred, stop here and restart.
            if(tAtLeastOneRestack)
            {
                mItemList->PopIterator();
                break;
            }

            //--Otherwise, check the next item.
            rFirstItem = (AdventureItem *)mItemList->AutoIterate();
        }
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *AdventureInventory::GetExtendedItemList()
{
    return mExtendedItemList;
}
StarLinkedList *AdventureInventory::GetItemList()
{
    return mItemList;
}
StarLinkedList *AdventureInventory::GetGemList()
{
    return mGemList;
}
StarLinkedList *AdventureInventory::GetGemMergeList()
{
    return mGemMergeList;
}
StarLinkedList *AdventureInventory::GetBuybackList()
{
    return mShopBuybackList;
}
AdventureItem *AdventureInventory::GetItem(const char *pName)
{
    //--Note: Can legally return NULL.
    return (AdventureItem *)mItemList->GetElementByName(pName);
}
AdventureItem *AdventureInventory::LiberateItemS(const char *pName)
{
    mItemList->SetDeallocation(false);
    AdventureItem *mLiberatedItem = (AdventureItem *)mItemList->RemoveElementS(pName);
    mItemList->SetDeallocation(true);
    if(mLiberatedItem == rLastReggedItem)  rLastReggedItem = NULL;
    if(mLiberatedItem == rMarkedEquipment) rMarkedEquipment = NULL;
    if(mLiberatedItem == rMasterGem)       rMasterGem = NULL;

    //--Socketing item is NOT cleared when liberated, as it may be equipped to an entity.
    //  Use caution!
    //if(mLiberatedItem == rSocketingItem)  rSocketingItem = NULL;
    return mLiberatedItem;
}
AdventureItem *AdventureInventory::GetLastRegisteredItem()
{
    return (AdventureItem *)rLastReggedItem;
}
AdventureItem *AdventureInventory::GetMarkedEquipment()
{
    return rMarkedEquipment;
}
AdventureItem *AdventureInventory::GetMasterGem()
{
    return rMasterGem;
}
void AdventureInventory::LiberateItemP(void *pPtr)
{
    ///--[Documentation]
    //--Removes the given pointer from the inventory without deallocating it. Caller is responsible
    //  for deallocation. If the item is not in the inventory, does nothing.
    //--For safety this also clears various reference pointers inside the inventory, making it
    //  impossible to accidentally equip gems to a liberated item.
    if(!mItemList->IsElementOnList(pPtr)) return;

    //--Liberate.
    mItemList->SetDeallocation(false);
    mItemList->RemoveElementP(pPtr);
    mItemList->SetDeallocation(true);

    //--Clear reference pointers.
    if(pPtr == rLastReggedItem)  rLastReggedItem = NULL;
    if(pPtr == rMarkedEquipment) rMarkedEquipment = NULL;
    if(pPtr == rMasterGem)       rMasterGem = NULL;

    //--Socketing item is NOT cleared when liberated, as it may be equipped to an entity.
    //  Use caution!
    //if(pPtr == rSocketingItem)  rSocketingItem = NULL;
}

///===================================== Static Functions =========================================
AdventureInventory *AdventureInventory::Fetch()
{
    return MapManager::Fetch()->GetAdventureInventory();
}
