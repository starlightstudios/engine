//--Base
#include "WADFile.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--Contains functions which convert data from basic WAD to UDMF format. The UDMF format is expandable, so many
//  members of the UDMF will be left in their default states by this.

void WADFile::LoadVertexData(WAD_Directory_Entry *pVertexEntry)
{
    //--Loads vertex data from the given entry. Vertexes don't gain much from UDMF except extra precision.
    mVerticesTotal = 0;
    free(mVertices);
    mVertices = NULL;
    if(!pVertexEntry) return;

    //--Check the vertex count. Do nothing if it's zero.
    int tVertexCount = pVertexEntry->mSize  / sizeof(WAD_Vertex16i);
    if(tVertexCount < 1) return;

    //--Structure to fill with.
    WAD_Vertex16i tTempStruct;

    //--Count and allocate.
    mVerticesTotal = tVertexCount;
    SetMemoryData(__FILE__, __LINE__);
    mVertices = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex) * mVerticesTotal);

    //--For each vertex...
    for(int i = 0; i < mVerticesTotal; i ++)
    {
        //--Copy.
        int tMemoryCursor = i * sizeof(WAD_Vertex16i);
        memcpy(&tTempStruct, &pVertexEntry->mData[tMemoryCursor], sizeof(WAD_Vertex16i));

        //--Translate.
        mVertices[i].Initialize();
        mVertices[i].mX = tTempStruct.mX;
        mVertices[i].mY = tTempStruct.mY;
    }

    //--Flip the y's of all vertex data. This is because Star^3 uses a different coordinate system
    //  from ID1.
    for(int i = 0; i < mVerticesTotal; i ++)
    {
        mVertices[i].mY = mVertices[i].mY * -1.0f;
    }

    //--Debug.
    DebugManager::Print("Finished converting vertex data.\n");
}
void WADFile::LoadLinedefData(WAD_Directory_Entry *pLinedefEntry)
{
    //--Loads linedef data from WAD_Linedef to UDMFLinedef format.
    mLinedefsTotal = 0;
    free(mLinedefs);
    mLinedefs = NULL;
    if(!pLinedefEntry) return;

    //--Make sure the entry has size for linedefs.
    int tLinedefCount = pLinedefEntry->mSize / sizeof(WAD_Linedef);
    if(tLinedefCount < 1) return;

    //--Structure to fill with.
    WAD_Linedef tTempStruct;

    //--Count and allocate.
    mLinedefsTotal = tLinedefCount;
    SetMemoryData(__FILE__, __LINE__);
    mLinedefs = (UDMFLinedef *)starmemoryalloc(sizeof(UDMFLinedef) * mLinedefsTotal);

    //--For each linedef...
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        //--Copy.
        int tMemoryCursor = i * sizeof(WAD_Linedef);
        memcpy(&tTempStruct, &pLinedefEntry->mData[tMemoryCursor], sizeof(WAD_Linedef));

        //--Translate.
        mLinedefs[i].Initialize();
        mLinedefs[i].mID = tTempStruct.mTag;
        mLinedefs[i].mStartVertex = tTempStruct.mStartVertex;
        mLinedefs[i].mEndVertex = tTempStruct.mEndVertex;
        mLinedefs[i].mSidedefL = tTempStruct.mLeftSidedef;
        mLinedefs[i].mSidedefR = tTempStruct.mRightSidedef;
    }

    //--Debug.
    DebugManager::Print("Finished converting linedef data.\n");
}
void WADFile::LoadSidedefData(WAD_Directory_Entry *pSidedefEntry)
{
    //--Loads sidedef data from WAD_Sidedef to UDMFSidedef format.
    mSidedefsTotal = 0;
    free(mSidedefs);
    mSidedefs = NULL;
    if(!pSidedefEntry) return;

    //--Make sure the entry has size for linedefs.
    int tSidedefCount = pSidedefEntry->mSize / sizeof(WAD_Sidedef);
    if(tSidedefCount < 1) return;

    //--Structure to fill with.
    WAD_Sidedef tTempStruct;

    //--Count and allocate.
    mSidedefsTotal = tSidedefCount;
    SetMemoryData(__FILE__, __LINE__);
    mSidedefs = (UDMFSidedef *)starmemoryalloc(sizeof(UDMFSidedef) * mSidedefsTotal);

    //--For each sidedef...
    for(int i = 0; i < mSidedefsTotal; i ++)
    {
        //--Copy.
        int tMemoryCursor = i * sizeof(WAD_Sidedef);
        memcpy(&tTempStruct, &pSidedefEntry->mData[tMemoryCursor], sizeof(WAD_Sidedef));

        //--Translate.
        mSidedefs[i].Initialize();
        mSidedefs[i].mOffsetX = tTempStruct.mXOffset;
        mSidedefs[i].mOffsetY = tTempStruct.mYOffset;
        CopyEight(mSidedefs[i].mTexTop, tTempStruct.mUpperTex);
        CopyEight(mSidedefs[i].mTexMid, tTempStruct.mMiddlTex);
        CopyEight(mSidedefs[i].mTexBot, tTempStruct.mLowerTex);
        mSidedefs[i].mSector = tTempStruct.mSector;
    }

    //--Debug.
    DebugManager::Print("Finished converting sidedef data.\n");
}
void WADFile::LoadSectorData(WAD_Directory_Entry *pSectorEntry)
{
    //--Loads sector data from WAD_Sector to UDMFSector format.
    mSectorsTotal = 0;
    free(mSectors);
    mSectors = NULL;
    if(!pSectorEntry) return;

    //--Make sure the entry has size for linedefs.
    int tSectorCount = pSectorEntry->mSize / sizeof(WAD_Sector);
    if(tSectorCount < 1) return;

    //--Structure to fill with.
    WAD_Sector tTempStruct;

    //--Count and allocate.
    mSectorsTotal = tSectorCount;
    SetMemoryData(__FILE__, __LINE__);
    mSectors = (UDMFSector *)starmemoryalloc(sizeof(UDMFSector) * mSectorsTotal);

    //--For each linedef...
    for(int i = 0; i < mSectorsTotal; i ++)
    {
        //--Copy.
        int tMemoryCursor = i * sizeof(WAD_Sector);
        memcpy(&tTempStruct, &pSectorEntry->mData[tMemoryCursor], sizeof(WAD_Sector));

        //--Translate.
        mSectors[i].Initialize();
        mSectors[i].mID = tTempStruct.mTag;
        mSectors[i].mSpecial = tTempStruct.mSpecialFlag;
        mSectors[i].mHeightFloor = tTempStruct.mFloorHeight;
        mSectors[i].mHeightCeiling = tTempStruct.mCeilingHeight;
        CopyEight(mSectors[i].mTexFloor, tTempStruct.mFloorName);
        CopyEight(mSectors[i].mTexCeiling, tTempStruct.mCeilingName);
        mSectors[i].mLightLevel = tTempStruct.mLightLevel;
    }

    //--Debug.
    DebugManager::Print("Finished converting sector data.\n");
}
