///====================================== StarlightString =========================================
//--Special string for the Starlight engine. This string can contain images in addition to
//  letters, and allows for advanced color/italics options.
//--These are not a replacement for normal strings, they should only be used when required
//  for advanced rendering as they are slower than conventional font rendering.
//--The formatting for the string is identical to the usual printf format, except [IMG0] will
//  be replaced in-stride with the provided image. The string is designed to precache itself
//  and needs to be modified whenever the variables/images change.
//--The StarlightString class uses [IMG0] [IMG1] [IMG2] etc to specify images, which must
//  be uploaded seperately. If an out-of-range index is specified, nothing renders.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
//--Represents a chunk of text which has an image at the start. Taken together, these
//  represent all images and text in the object.
typedef struct StarlightStringBreakpoint
{
    //--Variables
    int mIndex;
    char *mString;
    float mYOffset;
    float mImgScale;
    StarBitmap *rImage;

    //--Special
    bool mIsReposition;
    float mRepositionX;

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
    int ComputeLength(StarFont *pFont);
}StarlightStringBreakpoint;

//--Not used by this class directly, but other classes looking to store the information needed
//  to assemble a StarlightString can use this pack to store info for assembly.
typedef struct StarlightStringImagePack
{
    //--Variables
    float mYOffset;
    float mImgScale;
    StarBitmap *rImage;

    //--Functions
    void Initialize()
    {
        mYOffset = 0.0f;
        mImgScale = 1.0f;
        rImage = NULL;
    }
}StarlightStringImagePack;

///===================================== Local Definitions ========================================
#define STARLIGHT_STRING_BUFFER_PIXELS 1.0f
#define STARLIGHT_STRING_BUFFER_LETTERS 1024

///========================================== Classes =============================================
class StarlightString : public RootObject
{
    private:
    //--Breakpoints
    StarLinkedList *mBreakpointList; //StarlightStringBreakpoint *, master

    //--Images
    int mImagesTotal;
    float *mYOffsets;
    float *mImgScales;
    StarBitmap **mrImages;

    //--Colors
    StarlightColor mTextColor;
    StarlightColor mImageColor;

    protected:

    public:
    //--System
    StarlightString();
    virtual ~StarlightString();

    //--Public Variables
    //--Property Queries
    float GetLength(StarFont *pFont);
    int GetTotalChars();

    //--Manipulators
    void SetReposition(float pXPosition);
    void AllocateImages(int pTotal);
    void SetImageS(int pSlot, float pYOffset, const char *pDLPath);
    void SetImageP(int pSlot, float pYOffset, StarBitmap *pImage);
    void SetImageScale(int pSlot, float pScale);
    void SetTextColor(StarlightColor pColor);
    void SetImageColor(StarlightColor pColor);

    //--Core Methods
    void SetString(const char *pString, ...);
    void CrossreferenceImages();
    StarlightString *Clone();
    void AutoSetControlString(const char *pString, int pImages, ...);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void DrawText(float pX, float pY, int pFlags, float pScale, StarFont *pFont);
    void DrawTextFixed(float pX, float pY, float pMaxLen, int pFlags, StarFont *pFont);
    void DrawTextImgScale(float pX, float pY, int pFlags, float pScale, float pImgScale, StarFont *pFont);

    //--Pointer Routing
    StarlightStringBreakpoint *GetBreakpoint(int pIndex);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

