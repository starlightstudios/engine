//--Base
#include "AdvUIMap.h"

//--Classes
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Debug]
//#define AMENUMAPBASIC_DEBUG
#ifdef AMENUMAPBASIC_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Drawing =============================================
void AdvUIMap::RenderBasicMapMode()
{
    ///--[Documentation]
    //--Renders a map and a set of pins indicating where the player can warp to.
    DebugPush(true, "RenderBasicMapMode() - Begin.\n");

    //--If this flag is tripped, reposition the map to center on the player.
    if(mResetFocusOnPlayer)
    {
        mResetFocusOnPlayer = false;
        RepositionMap(mPlayerX - (VIRTUAL_CANVAS_X * 0.50f) + 30.0f, mPlayerY - (VIRTUAL_CANVAS_Y * 0.50f) + 30.0f);
    }

    ///--[Constants]
    float cXOffset = -mMapFocusX;
    float cYOffset = -mMapFocusY;
    float cScreenX = (VIRTUAL_CANVAS_X * 0.50f);
    float cScreenY = (VIRTUAL_CANVAS_Y * 0.50f);
    float cMapCenterX = cScreenX;
    float cMapCenterY = cScreenY;

    ///--[Positioning]
    glTranslatef(cMapCenterX, cMapCenterY, 0.0f);
    glScalef(mMapZoom.mXCur, mMapZoom.mXCur, 1.0f);
    glTranslatef(-cMapCenterX, -cMapCenterY, 0.0f);

    //--Offset by the player's manually-inputted position, allowing scrolling.
    glTranslatef(cXOffset, cYOffset, 0.0f);

    ///--[Map Background]
    //--Centering.
    if(rBasicMap) rBasicMap->Draw();

    ///--[Player Indicators]
    //--Render the player actor to indicate where the player is. This occurs at the same spot regardless of the map's scrolling.
    TilemapActor *rActor = AdventureLevel::Fetch()->LocatePlayerActor();//Can't fail because the level owns the menu!
    if(rActor)
    {
        //--Get the walking image for this actor.
        StarBitmap *rIdleImage = rActor->GetMapImage(mPlayerSpriteTimer / 2);
        if(rIdleImage)
        {
            //--Setup.
            float cScale = 3.0f;

            //--Resolve position.
            float tX = mPlayerX - (rIdleImage->GetTrueWidth()  / 2.0f * cScale);
            float tY = mPlayerY - (rIdleImage->GetTrueHeight() / 2.0f * cScale);
            if(DisplayManager::xLowDefinitionFlag)
            {
                tX = tX * 0.5f;
                tY = tY * 0.5f;
            }

            //--Translate to position.
            glTranslatef(tX, tY, 0.0f);

            //--Render scaled down.
            glScalef(cScale, cScale, 1.0f);
            rIdleImage->Draw();
            glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);

            //--Clean.
            glTranslatef(-tX, -tY, 0.0f);
        }
    }

    //--Render party members around the character.
    int i = 0;
    rActor = AdventureLevel::Fetch()->LocateFollowingActor(i);
    while(rActor)
    {
        //--Get the walking image for this actor.
        StarBitmap *rIdleImage = rActor->GetMapImage((mPlayerSpriteTimer / 2) + 20);
        if(rIdleImage)
        {
            //--Setup.
            float cScale = 3.0f;

            //--Resolve position.
            float tX = mPlayerX - (rIdleImage->GetTrueWidth()  / 2.0f * cScale) + ((i+1) * 16.0f * cScale);
            float tY = mPlayerY - (rIdleImage->GetTrueHeight() / 2.0f * cScale);
            if(DisplayManager::xLowDefinitionFlag)
            {
                tX = tX * 0.5f;
                tY = tY * 0.5f;
            }

            //--Translate to position.
            glTranslatef(tX, tY, 0.0f);

            //--Render scaled down.
            glScalef(cScale, cScale, 1.0f);
            rIdleImage->Draw();
            glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);

            //--Clean.
            glTranslatef(-tX, -tY, 0.0f);
        }

        //--Next actor.
        i ++;
        rActor = AdventureLevel::Fetch()->LocateFollowingActor(i);
    }

    ///--[Map Pins]
    MapPin *rMapPin = (MapPin *)mMapPinList->PushIterator();
    while(rMapPin)
    {
        //--Render.
        if(rMapPin->rImage)
        {
            float cHfWid = rMapPin->rImage->GetWidth() * -0.50f;
            float cHeigh = rMapPin->rImage->GetHeight() * -1.0f;
            rMapPin->rImage->Draw(rMapPin->mX + cHfWid, rMapPin->mY + cHeigh);
        }

        //--Next.
        rMapPin = (MapPin *)mMapPinList->AutoIterate();
    }

    ///--[Unset Position]
    //--Clean.
    glTranslatef(-cXOffset, -cYOffset, 0.0f);
    glTranslatef(cMapCenterX, cMapCenterY, 0.0f);
    glScalef(1.0f / mMapZoom.mXCur, 1.0f / mMapZoom.mXCur, 1.0f);
    glTranslatef(-cMapCenterX, -cMapCenterY, 0.0f);

    ///--[Finish Up]
    DebugPop("RenderBasicMapMode() - Finished normally.\n");
}
