//--Base
#include "PairanormalDialogue.h"

//--Classes
//--CoreClasses
#include "SugarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

//--[Manipulators]
void PairanormalDialogue::ActivateNameEntry()
{
    mIsNameEntryMode = true;
}
void PairanormalDialogue::DeactivateNameEntry()
{
    //--Must actually be in name entry mode.
    if(!mIsNameEntryMode) return;

    //--Must be at least one character in the name.
    if(mCurrentPlayerName[0] == '\0') return;

    //--All checks passed.
    mIsNameEntryMode = false;

    //--Save it as a variable.
    SysVar *rPlayerNameVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/Playername");
    if(rPlayerNameVar)
    {
        ResetString(rPlayerNameVar->mAlpha, mCurrentPlayerName);
    }
}

//--[Manipulators]
void PairanormalDialogue::SetNameEntryString(const char *pString)
{
    strncpy(mNameEntryString, pString, 127);
}

//--[Update]
void PairanormalDialogue::UpdateNameEntry()
{
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Timer.
    mNameEntryTimer ++;

    //--Get the current keypress codes for this tick.
    int tKeyboard, tMouse, tJoy;
    rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoy);

    //--Get the matching name. It's auto-generated.
    const char *rPressedKeyName = rControlManager->GetNameOfKeyIndex(tKeyboard);
    if(rPressedKeyName)
    {
        //--To be valid, the key must have one letter in its name, and it must be standard ASCII.
        //  Numbers are illegal.
        if(strlen(rPressedKeyName) == 1 && rPressedKeyName[0] < 128)
        {
            //--Setup.
            char tBuf[2];
            tBuf[0] = rPressedKeyName[0];
            tBuf[1] = '\0';

            //--Downshift it if it's a letter.
            if(tBuf[0] >= 'A' && tBuf[0] <= 'Z')
            {
                tBuf[0] = tBuf[0] + 'a' - 'A';
            }

            //--If the key is shifted, move it to uppercase.
            if(mIsShifted || rControlManager->IsDown("Shift"))
            {
                //--Letters.
                if(tBuf[0] >= 'a' && tBuf[0] <= 'z')
                {
                    tBuf[0] = tBuf[0] + 'A' - 'a';
                }
                //--Numerics.
                else if(tBuf[0] >= '0' && tBuf[0] <= '9')
                {
                    if(tBuf[0] == '1') tBuf[0] = '!';
                    if(tBuf[0] == '2') tBuf[0] = '@';
                    if(tBuf[0] == '3') tBuf[0] = '#';
                    if(tBuf[0] == '4') tBuf[0] = '$';
                    if(tBuf[0] == '5') tBuf[0] = '%';
                    if(tBuf[0] == '6') tBuf[0] = '^';
                    if(tBuf[0] == '7') tBuf[0] = '&';
                    if(tBuf[0] == '8') tBuf[0] = '*';
                    if(tBuf[0] == '9') tBuf[0] = '(';
                    if(tBuf[0] == '0') tBuf[0] = ')';
                }
            }

            //--If Caps-Locked, switch case.
            if(mIsCapsLocked)
            {
                //--Letters lowercase.
                if(tBuf[0] >= 'a' && tBuf[0] <= 'z')
                {
                    tBuf[0] = tBuf[0] + 'A' - 'a';
                }
                //--Letters uppercase.
                else if(tBuf[0] >= 'A' && tBuf[0] <= 'Z')
                {
                    tBuf[0] = tBuf[0] + 'a' - 'A';
                }
            }

            //--Add it if it's a valid letter. All other types are ignored.
            if((tBuf[0] >= 'a' && tBuf[0] <= 'z') || (tBuf[0] >= 'A' && tBuf[0] <= 'Z'))
            {
                //--Last letter. Just overwrite it.
                if(mLetterCursor == DIABTN_NAME_MAXLEN)
                {
                    mCurrentPlayerName[DIABTN_NAME_MAXLEN-1] = tBuf[0];
                    mCurrentPlayerName[DIABTN_NAME_MAXLEN+0] = '\0';
                }
                //--Overwrite and increment.
                else
                {
                    mCurrentPlayerName[mLetterCursor+0] = tBuf[0];
                    mCurrentPlayerName[mLetterCursor+1] = '\0';
                    mLetterCursor ++;
                }
            }

            //--Cancel shift.
            mIsShifted = false;
        }
        //--Backspace.
        else if(!strcasecmp(rPressedKeyName, "Backspace"))
        {
            //--If on the 0th letter, do nothing:
            if(mLetterCursor == 0)
            {
                mCurrentPlayerName[mLetterCursor] = '\0';
            }
            //--Otherwise, delete the last letter and move back one.
            else
            {
                mCurrentPlayerName[mLetterCursor-1] = '\0';
                mLetterCursor --;
            }
        }
        //--Space.
        else if(!strcasecmp(rPressedKeyName, "Space"))
        {
            //--Last letter. Just overwrite it.
            if(mLetterCursor == DIABTN_NAME_MAXLEN)
            {
                mCurrentPlayerName[DIABTN_NAME_MAXLEN-1] = ' ';
                mCurrentPlayerName[DIABTN_NAME_MAXLEN+0] = '\0';
            }
            //--Overwrite and increment.
            else
            {
                mCurrentPlayerName[mLetterCursor+0] = ' ';
                mCurrentPlayerName[mLetterCursor+1] = '\0';
                mLetterCursor ++;
            }
        }
        //--Space.
        else if(!strcasecmp(rPressedKeyName, "CapsLock"))
        {
            mIsCapsLocked = !mIsCapsLocked;
        }
        //--Shift. Already handles by a control case.
        else if(!strcasecmp(rPressedKeyName, "LShift") || !strcasecmp(rPressedKeyName, "RShift"))
        {
        }
        //--Return/Enter
        else if(!strcasecmp(rPressedKeyName, "Return") || !strcasecmp(rPressedKeyName, "Enter"))
        {
            DeactivateNameEntry();
            return;
        }
    }
}

//--[Rendering]
void PairanormalDialogue::RenderNameEntry()
{
    //--Renders the name entry screen. This is just some simple instructions and the current name.
    if(!Images.mIsReady) return;

    //--Darken overlay.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 0.75f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    StarlightColor::ClearMixer();

    //--Render the current name in the middle of the screen.
    float cStartX = VIRTUAL_CANVAS_X * 0.50f;
    float cStartY = VIRTUAL_CANVAS_Y * 0.30f;
    float cTextHeight = Images.Data.rHeadingFont->GetTextHeight() + 5.0f;
    Images.Data.rHeadingFont->DrawText(cStartX, cStartY, SUGARFONT_AUTOCENTER_X, 1.0f, mNameEntryString);

    //--Compute name centering. This is done to avoid auto-centering with the underscore at the end.
    float cTextWid = Images.Data.rHeadingFont->GetTextWidth(mCurrentPlayerName);
    if(mCurrentPlayerName[0] == '\0')
        ;
    else if(mNameEntryTimer % 30 < 15)
        Images.Data.rHeadingFont->DrawText(cStartX - (cTextWid * 0.5f), cStartY + cTextHeight + 20.0f, 0, 1.0f, mCurrentPlayerName);
    else
        Images.Data.rHeadingFont->DrawTextArgs(cStartX - (cTextWid * 0.5f), cStartY + cTextHeight + 20.0f, 0, 1.0f, "%s_", mCurrentPlayerName);

    //--Instructions.
    cStartX = VIRTUAL_CANVAS_X * 0.50f;
    cStartY = cStartY + (cTextHeight * 6.0f);
    Images.Data.rDialogueFont->DrawText(cStartX, cStartY +                 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "(Letters only, 10 character maximum)");
    Images.Data.rDialogueFont->DrawText(cStartX, cStartY + (cTextHeight * 1.0f), SUGARFONT_AUTOCENTER_X, 1.0f, "(1 letter minimum)");
    Images.Data.rDialogueFont->DrawText(cStartX, cStartY + (cTextHeight * 2.0f), SUGARFONT_AUTOCENTER_X, 1.0f, "(Press Enter when finished)");
}
