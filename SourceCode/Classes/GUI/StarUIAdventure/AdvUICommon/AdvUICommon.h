///======================================== AdvUICommon ===========================================
//--Common UI object to all Adventure sub-components. This allows interoperability among derived objects,
//  such as using the same colors or timings. Help images and some fonts/images can be standardized.

#pragma once

///========================================= Includes =============================================
#include "StarUIPiece.h"

#ifdef _TARGET_OS_WINDOWS_
    #ifndef _STEAM_API_
        #define constexpr const
    #endif
#endif
#ifdef _TARGET_OS_MAC_
#define constexpr const
#endif

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUICommon : virtual public StarUIPiece
{
    protected:
    ///--[System]
    ///--[Constants]
    //--Timers.
    static const int cxAdvVisTicks =  15;               //Standardized ticks to show/hide an object.
    static const int cxAdvDetTicks =   7;               //Standardized ticks to show/hide details mode.
    static const int cxAdvCurTicks =   7;               //Standardized ticks to move the cursor.
    static const int cxAdvCharSwapTicks = 15;           //Ticks to move characters when switching.
    static const int cxAdvOscTicks = 120;               //Standardized tick periodicity for oscillating arrows.

    //--Distances.
    static constexpr float cxAdvOscDist = 5.0f;             //Distance that arrows oscillate.

    //--Sizings.
    static constexpr float cxAdvMainlineFontH = 22.0f;      //Height, in pixels, between lines of rFont_Mainline.
    static constexpr float cxAdvBigIconOffset = 5.0f;       //Height, in pixels, that statistic icons are offset when used in a StarlightString.
    static constexpr float cxAdvBigControlOffset = 10.0f;   //Height, in pixels, that controls icons are offset when used in a StarlightString.
    static constexpr float cxAdvGemIconW = 24.0f;           //Width, in pixels, to space gem frames when rendering using the common routine.

    ///--[Colors]
    StarlightColor mColorGrey;
    StarlightColor mColorHeading;

    ///--[Images]

    protected:

    public:
    //--System
    AdvUICommon();
    virtual ~AdvUICommon();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    virtual void SetColor(const char *pColorName, float pAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void RenderComparisonCharacters(int pRearIndex, int pIndex, int pTimer, int pTimerMax, int pUIIndex);
    virtual void RenderGemSlotsLine(float pX, float pY, AdventureItem *pItem, StarBitmap *pGemSlotImg);
    virtual void RenderGemSlotsTwoLine(float pX, float pY, AdventureItem *pItem, StarBitmap *pGemSlotImg);
    virtual void RenderGemSlotsSwitch(float pX, float pY, AdventureItem *pItem, StarBitmap *pGemSlotImg);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


