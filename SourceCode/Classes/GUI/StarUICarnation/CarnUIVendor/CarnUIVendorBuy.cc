//--Base
#include "CarnUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== Update ==============================================
void CarnUIVendor::UpdateModeBuy()
{
    ///--[ ========== Documentation =========== ]
    //--Update handler for buy mode. This is a reimplementation of the Adventure version, built to
    //  use the mouse and scrollbars.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[ ======= Timers and Submodes ======== ]
    ///--[Timers]
    //--Standard timers.
    StandardTimer(mBuyTimer,        0, cxAdvBuyTicks,      mIsBuyingItem);
    StandardTimer(mAskToEquipTimer, 0, cxAdvAskEquipTicks, mAskToEquip);

    //--Update blockers. If these timers are not capped, stop the update.
    if(mBuyTimer     > 0 && mBuyTimer     < cxAdvBuyTicks)     return;
    if(mDetailsTimer > 0 && mDetailsTimer < cxAdvDetailsTicks) return;

    ///--[Mode Handlers]
    //--In this mode, handle controls for buying a specific item. This gets its own subroutine.
    if(mIsBuyingItem) { UpdateBuyItem(); return; }

    //--After the player decides to buy an item, the game may ask the player to equip it. That is this update.
    if(mAskToEquip) { UpdateBuyEquipItem(); return; }

    //--If in Details mode, pressing cancel exits details mode. All other controls are ignored.
    if(CommonDetailsUpdate()) return;

    ///--[ ========== Mouse Controls ========== ]
    ///--[Scrollbar]
    //--If the inventory size changes, recompute the scrollbar size here.
    if(mRecomputeScrollbarSize)
    {
        ScrollbarPack *rScrollbarPack = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
        if(rScrollbarPack) rScrollbarPack->mMaxSkip = mShopInventory->GetListSize() - cxCarnScrollPageSize;
    }

    ///--[Mouse Handlers]
    //--Update mouse info.
    GetMouseInfo();

    //--Scrollbars update before anything else.
    if(UpdateScrollbars()) return;

    ///--[Mouse Over]
    //--If the mouse moves, recheck hitboxes.
    if(mMouseMoved)
    {
        ///--[Selection Mouse Over]
        //--Check the item hitbox. If it changed, play a sound effect.
        int tOldCursor = mCursor;
        RunHitboxCheck();

        //--Iterate across the reply packs.
        int tReply = CheckRepliesWithin(cxHitbox_ItemsLo, cxHitbox_ItemsHi);
        if(tReply != -1) mCursor = tReply - cxHitbox_ItemsLo + mSkip;

        //--Range check, can't pick a blank item.
        if(mCursor >= mShopInventory->GetListSize()) mCursor = tOldCursor;

        //--If the class cursor changed, play a sound effect.
        if(tOldCursor != mCursor && mCursor != -1)
        {
            //--Description.
            AdventureItem *rItem = GetConsideredItem();
            RebuildDescriptionList(rItem);

            //--Adjust comparison item.
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);
            RecheckComparisonSlot(rItem, rActiveEntity);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Left Click]
    //--Player left clicks. Switches modes, selects items, selects comparison character.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Iterate across the reply packs.
        RunHitboxCheck();
        int tReply = CheckRepliesWithin(cxHitbox_ItemsLo, cxHitbox_Exit);
        if(tReply == -1) return;

        //--Selecting an item:
        if(tReply >= cxHitbox_ItemsLo && tReply <= cxHitbox_ItemsHi)
        {
            //--Get the item in question.
            ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
            if(!rPackage) return;

            //--Make sure the item exists.
            AdventureItem *rItem = rPackage->mItem;
            if(!rItem) return;

            //--If the item has 0 quantity available, stop.
            if(rPackage->mQuantity < 1 && rPackage->mQuantity != -1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Get how much the item will cost. If we don't have enough money to buy one, fail.
            int tCost = rItem->GetValue();
            if(rPackage->mPriceOverride > -1) tCost = rPackage->mPriceOverride;
            if(tCost > AdventureInventory::Fetch()->GetPlatina())
            {
                RollQuote("CantAfford", 0, false);
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Otherwise, activate buying mode.
            mIsBuyingItem = true;
            mBuyQuantity = 1;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }
        //--Changing shop modes:
        else if(tReply >= cxHitbox_Buy && tReply <= cxHitbox_Sell)
        {
            //--Only do something on the sell button.
            if(tReply == cxHitbox_Sell) ScrollMode(1);
            return;
        }
        //--Changing comparison character:
        else if(tReply >= cxHitbox_CharLo && tReply <= cxHitbox_CharHi)
        {
            SetComparisonCharacter(tReply - cxHitbox_CharLo);
            return;
        }
        //--Exit.
        else if(tReply == cxHitbox_Exit)
        {
            HandleExit();
            return;
        }

        //--Stop update.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Exits the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        HandleExit();
        return;
    }
}
void CarnUIVendor::UpdateBuyItem()
{
    ///--[Documentation]
    //--Handles the confirmation window, allowing the user to confirm or cancel a purchase. Same as
    //  the base version but updated to allow mouse usage.
    ControlManager *rControlManager = ControlManager::Fetch();
    ShopInventoryPack *rPack;
    AdventureItem *rItem;

    //--Fast-access pointers.
    if(!(rPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor))) return;
    if(!(rItem = rPack->mItem)) return;

    ///--[Mouse Handlers]
    //--Update mouse info.
    GetMouseInfo();

    ///--[Left Click / Buttons]
    //--Player left clicks. Check buttons.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Iterate across the reply packs.
        RunHitboxCheck();
        int tReply = CheckRepliesWithin(cxHitbox_Confirm, cxHitbox_Cancel);
        if(tReply == -1) return;

        //--If it's cancel, stop here.
        if(tReply == cxHitbox_Cancel)
        {
            mIsBuyingItem = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--If it's confirm, purchase the item.
        if(tReply == cxHitbox_Confirm)
        {
            //--Run routine.
            PurchaseItem(rPack, mBuyQuantity, true, true);

            //--If not asking to equip, spawn a quote.
            if(!mAskToEquip) RollQuote("Purchase", 15, false);
        }
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Cancels purchase.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        mIsBuyingItem = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
void CarnUIVendor::UpdateBuyEquipItem()
{
    ///--[Documentation]
    //--Handles the confirmation window, allowing the user to equip an item they just purchased. Same as
    //  the base version but updated to allow mouse usage.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Mouse Handlers]
    //--Update mouse info.
    GetMouseInfo();

    ///--[Left Click / Buttons]
    //--Player left clicks. Check buttons.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Iterate across the reply packs.
        RunHitboxCheck();
        int tReply = CheckRepliesWithin(cxHitbox_Confirm, cxHitbox_Cancel);
        if(tReply == -1) return;

        //--If it's cancel, stop here.
        if(tReply == cxHitbox_Cancel)
        {
            //--Set Fluffs' emotions.
            RollQuote("Purchase", 15, false);

            //--Exit.
            mAskToEquip = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--If it's confirm, purchase the item.
        if(tReply == cxHitbox_Confirm)
        {
            //--Flags.
            mAskToEquip = false;
            EquipPurchasedItem();

            //--Fluffs.
            RollQuote("Purchase", 15, false);

            //--SFX.
            AudioManager::Fetch()->PlaySound("World|TakeItem");
            return;
        }
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Cancels purchase.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        //--Set Fluffs' emotions.
        RollQuote("Purchase", 15, false);

        //--Exit.
        mAskToEquip = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///======================================= Dummy Render ===========================================
//--The Carnation UI renders all pieces in the unaligned section. These functions disable all other rendering.
void CarnUIVendor::RenderLftBuy(float pVisAlpha) { }
void CarnUIVendor::RenderTopBuy(float pVisAlpha) { }
void CarnUIVendor::RenderRgtBuy(float pVisAlpha) { }
void CarnUIVendor::RenderBotBuy(float pVisAlpha) { }

///====================================== Main Rendering ==========================================
void CarnUIVendor::RenderUnalignedBuy(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders buy mode, which is mostly the same as sell mode except which inventory is queried changes.
    float cHeightPerEntry =  23.0f;

    ///--[Common]
    //--Static pieces.
    CarnImages.rBackground_Fluffs->Draw();
    CarnImages.rBackground_Clouds->Draw();
    CarnImages.rBackground_Shelf->Draw();
    CarnImages.rBackground_Covers->Draw();
    RenderFluffs();
    CarnImages.rBackground_Desk->Draw();
    CarnImages.rFrame_BoxRgt->Draw();
    CarnImages.rFrame_BoxBot->Draw();
    CarnImages.rFrame_Hexes->Draw();

    ///--[Labels]
    //--Buy and Sell labels
    CarnImages.rFont_BuySell->DrawTextArgs(208.0f, 30.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
    CarnImages.rFont_BuySell->DrawTextArgs(400.0f, 30.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");

    //--Hexes.
    CarnImages.rFont_Hexes->DrawTextArgs(1212.0f, 26.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", AdventureInventory::Fetch()->GetPlatina());

    ///--[Purchaseable Items]
    //--Setup.
    ShopInventoryPack *rDescriptionPack = GetConsideredItemPack();

    //--If a scrollbar is needed, render it.
    if(mShopInventory->GetListSize() > cxCarnScrollPageSize)
    {
        StandardRenderScrollbar(mSkip, cxCarnScrollPageSize, mShopInventory->GetListSize() - cxCarnScrollPageSize, 159.0f, 192.0f, false, CarnImages.rScrollbar_Scroller, CarnImages.rScrollbar_Static);
    }

    //--Render entries.
    RenderShopItemListing(mSkip, cScrollPage, 1.0f);

    ///--[Cursor]
    int tPosOnScreen = mCursor - mSkip;
    if(tPosOnScreen >= 0 && tPosOnScreen < cxCarnScrollPageSize) CarnImages.rOverlay_Cursor->Draw(0.0f, tPosOnScreen * cHeightPerEntry);

    ///--[Highlighted Item Description]
    //--If there is a highlighted item, render its description.
    if(mDescriptionList && rDescriptionPack && rDescriptionPack->mItem)
    {
        //--Position.
        float cXPosition = 87.0f;
        float cYPosition = 587.0f;
        float cLineHei = CarnImages.rFont_Description->GetTextHeight();

        //--Name.
        AdventureItem *rItem = rDescriptionPack->mItem;
        CarnImages.rFont_Name->DrawText(cXPosition, 536.0f, 0, 1.0f, rItem->GetName());

        //--Render.
        char *rString = (char *)mDescriptionList->PushIterator();
        while(rString)
        {
            //--Render.
            CarnImages.rFont_Description->DrawText(cXPosition, cYPosition, 0, 1.0f, rString);

            //--Next.
            cYPosition = cYPosition + cLineHei;
            rString = (char *)mDescriptionList->AutoIterate();
        }
    }

    ///--[Exit Button]
    TwoDimensionReal *rExitHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Exit");
    if(rExitHitbox)
    {
        CarnImages.rFrame_Button->Draw(rExitHitbox->mLft, rExitHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rExitHitbox->mXCenter, rExitHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Exit");
    }

    ///--[Highlighted Item Properties]
    //--If the highlighted item is a piece of equipment, shows its stats and comparison values.
    if(rDescriptionPack) RenderComparisonPane(rDescriptionPack->mItem, 1.0f);

    ///--[Overlays]
    //--Purchasing an item. Does nothing if the timer is zeroed.
    RenderBuyItem(1.0f);

    //--Confirming equipping an item.
    RenderBuyEquipItem(1.0f);
}

///=================================== Subroutine Rendering =======================================
void CarnUIVendor::RenderItemPack(ShopInventoryPack *pPack, int pSlot, bool pIsSelected, float pColorAlpha)
{
    ///--[Documentation]
    //--For buy/sell, renders the information of an item in a standard format. This only shows the icon
    //  at the moment, but the positioning is different from the base version.
    if(!pPack || !pPack->mItem) return;

    //--Resolve position.
    float cIconX  = 128.0f;
    float cNameX  = 154.0f;
    float cPriceR = 482.0f;
    float cStartY = 104.0f;
    float cHeight =  23.0f;
    float cRenderY = cStartY + (pSlot * cHeight);

    //--Price can shift to the left if there's a scrollbar.
    if(mShopInventory->GetListSize() >= cxCarnScrollPageSize)
    {
        cPriceR = 449.0f;
    }

    ///--[Render]
    //--Icon.
    StarBitmap *rImage = pPack->mItem->GetIconImage();
    if(rImage) rImage->Draw(cIconX, cRenderY+4);

    //--Left-aligned name.
    const char *rName = pPack->mDisplayName;
    if(!rName) rName = pPack->mItem->GetName();
    if(rName) CarnImages.rFont_Description->DrawText(cNameX, cRenderY, 0, 1.0f, rName);

    //--Right-aligned price.
    int tValue = pPack->mItem->GetValue();
    CarnImages.rFont_Description->DrawTextArgs(cPriceR, cRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
}
void CarnUIVendor::RenderBuyItem(float pVisAlpha)
{
    ///--[Documentation]
    //--When the player chooses an item to purchase, this UI appears over the rest of the UI. It shows
    //  what they are purchasing, how much it costs, and allows the quantity to be adjusted.
    //--Carnation UI always uses fades.
    float cColorAlpha = EasingFunction::QuadraticInOut(mBuyTimer, cxAdvBuyTicks) * pVisAlpha;
    if(mBuyTimer < 1) return;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cColorAlpha * 0.7f));

    ///--[Item Resolve]
    //--Get the item package.
    ShopInventoryPack *rPack = GetConsideredItemPack();
    AdventureItem *rItem     = GetConsideredItem();
    if(!rPack || !rItem) return;

    ///--[Rendering]
    //--Frame.
    SetColor("Clear", cColorAlpha);
    CarnImages.rFrame_Confirm->Draw();

    //--Heading.
    SetColor("Heading", cColorAlpha);
    CarnImages.rFont_Name->DrawText(VIRTUAL_CANVAS_X * 0.50f, 218.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Purchase");
    SetColor("Clear", cColorAlpha);

    //--Centering computations.
    char *tTextBuf = InitializeString("%s x %i", rItem->GetName(), mBuyQuantity);
    float cTextWid = CarnImages.rFont_Description->GetTextWidth(tTextBuf);
    float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

    //--Compute the item cost.
    int tItemCost = rItem->GetValue();
    if(rPack->mPriceOverride > -1) tItemCost = rPack->mPriceOverride;
    int tTotalCost = tItemCost * mBuyQuantity;

    //--Stop rendering here when fading out.
    if(!mIsBuyingItem)
    {
        SetColor("Clear", cColorAlpha);
        free(tTextBuf);
        return;
    }

    //--Item name.
    CarnImages.rFont_Description->DrawTextArgs(cTextLft, 265.0f, 0, 1.0f, tTextBuf);
    CarnImages.rFont_Description->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 290.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Costs %i Hexes (You have %i)", tTotalCost, AdventureInventory::Fetch()->GetPlatina());

    //--Buttons.
    TwoDimensionReal *rConfirmHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Confirm");
    if(rConfirmHitbox)
    {
        CarnImages.rFrame_Button->Draw(rConfirmHitbox->mLft, rConfirmHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rConfirmHitbox->mXCenter, rConfirmHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Confirm");
    }
    TwoDimensionReal *rCancelHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Cancel");
    if(rCancelHitbox)
    {
        CarnImages.rFrame_Button->Draw(rCancelHitbox->mLft, rCancelHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rCancelHitbox->mXCenter, rCancelHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Cancel");
    }

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);
    mCancelPurchaseString-> DrawText(579.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);
    mAdjustQuantityString-> DrawText(757.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);

    ///--[Finish Up]
    //--Clean.
    SetColor("Clear", cColorAlpha);
    free(tTextBuf);
}
void CarnUIVendor::RenderBuyEquipItem(float pVisAlpha)
{
    ///--[Documentation]
    //--When the player purchases an item that can be equipped, asks them if they want to equip it
    //  to the comparison character right now.
    float cColorAlpha = EasingFunction::QuadraticInOut(mAskToEquipTimer, cxAdvAskEquipTicks) * pVisAlpha;
    if(mAskToEquipTimer < 1) return;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cColorAlpha * 0.7f));

    ///--[Item Resolve]
    //--Get the character who should equip it.
    AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(mAskToEquipSlot);
    if(!rCharacter) return;

    //--Get their name.
    const char *rEquipName = rCharacter->GetDisplayName();
    if(!rEquipName) return;

    ///--[Rendering]
    //--Frame.
    SetColor("Clear", cColorAlpha);
    CarnImages.rFrame_Confirm->Draw();

    //--Heading.
    SetColor("Heading", cColorAlpha);
    CarnImages.rFont_Name->DrawText(VIRTUAL_CANVAS_X * 0.50f, 218.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equip Item?");
    SetColor("Clear", cColorAlpha);

    //--Item name.
    CarnImages.rFont_Description->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 290.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equip this to %s?", rEquipName);

    //--Buttons.
    TwoDimensionReal *rConfirmHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Confirm");
    if(rConfirmHitbox)
    {
        CarnImages.rFrame_Button->Draw(rConfirmHitbox->mLft, rConfirmHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rConfirmHitbox->mXCenter, rConfirmHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Confirm");
    }
    TwoDimensionReal *rCancelHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Cancel");
    if(rCancelHitbox)
    {
        CarnImages.rFrame_Button->Draw(rCancelHitbox->mLft, rCancelHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rCancelHitbox->mXCenter, rCancelHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Cancel");
    }

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);
    mCancelPurchaseString-> DrawText(579.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);
    mAdjustQuantityString-> DrawText(757.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);

    ///--[Finish Up]
    //--Clean.
    SetColor("Clear", cColorAlpha);
}
