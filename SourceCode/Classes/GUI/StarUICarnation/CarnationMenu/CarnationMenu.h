///======================================= CarnationMenu ==========================================
//--Used for Project Carnation, replaces menu handling where required. Does not use a radial central
//  menu, instead boots directly into log mode.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "AdventureMenu.h"
struct ShopInventoryPack;

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///--[Menu Mode Constants]
#ifndef CARNMENU_MODE_CONSTANTS
#define CARNMENU_MODE_CONSTANTS

//--Menu modes themselves.
#define CARNMENU_MODE_STATUS 0
#define CARNMENU_MODE_EQUIP 1
#define CARNMENU_MODE_INVENTORY 2
#define CARNMENU_MODE_SKILLS 3
#define CARNMENU_MODE_OPTIONS 4
#define CARNMENU_MODE_SAVE 5
#define CARNMENU_MODE_REGEM 6
#define CARNMENU_MODE_HEALPARTY 7
#define CARNMENU_MODE_TOTAL 8

#define CARNMENU_TOP_RENDER 6

//--Backwards codes used to change menu modes.
#define CARNMENU_CHANGEMODE_OFFSET 20
#define CARNMENU_CHANGEMODE_STATUS    (CARNMENU_MODE_STATUS    + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_EQUIP     (CARNMENU_MODE_EQUIP     + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_INVENTORY (CARNMENU_MODE_INVENTORY + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_SKILLS    (CARNMENU_MODE_SKILLS    + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_OPTIONS   (CARNMENU_MODE_OPTIONS   + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_SAVE      (CARNMENU_MODE_SAVE      + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_REGEM     (CARNMENU_MODE_REGEM     + CARNMENU_CHANGEMODE_OFFSET)
#define CARNMENU_CHANGEMODE_HEALPARTY (CARNMENU_MODE_HEALPARTY + CARNMENU_CHANGEMODE_OFFSET)

#endif

///--[Status Indices]
#define CARNATION_STAT_HP 0
#define CARNATION_STAT_SP 1
#define CARNATION_STAT_MAGICATTACK 3
#define CARNATION_STAT_ENDURANCE 4
#define CARNATION_STAT_INTELLECT 5
#define CARNATION_STAT_ATTACK 6
#define CARNATION_STAT_INITIATIVE 7
#define CARNATION_STAT_MAGICDEFENSE 8
#define CARNATION_STAT_LUCK 9
#define CARNATION_STAT_DEFENSE 10
#define CARNATION_STAT_SPEED 18

#define CARNMENU_STAT_TITLES_TOTAL 8

///========================================== Classes =============================================
class CarnationMenu : public AdventureMenu
{
    private:
    ///--[System]
    int mCarnationMode;
    int mMouseX;
    int mMouseY;
    int mMouseZ;

    ///--[Save Mode]
    int mSelectedSaveFile;

    ///--[Shopping Mode]
    int mStatIndexes[CARNMENU_STAT_TITLES_TOTAL];
    char mStatTitles[CARNMENU_STAT_TITLES_TOTAL][25];

    ///--[Event Mode]
    bool mIsEventMode;
    StarLinkedList *mrEventComponents; //StarUIPiece *, reference

    ///--[Class Change Mode]
    bool mIsClassChangeMode;
    StarLinkedList *mrClassChangeComponents; //StarUIPiece *, reference

    ///--[Images]
    struct
    {
        //--General
        bool mIsReady;

        //--Common
        struct
        {
            StarFont *rTabFont;
            StarFont *rHexFont;
            StarFont *rNameFont;
            StarBitmap *rHeaderLft;
            StarBitmap *rHeaderRgt;
            StarBitmap *rHeadings;
            StarBitmap *rHexLabel;
        }Common;
    }CarnImages;

    protected:

    public:
    //--System
    CarnationMenu();
    virtual ~CarnationMenu();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void Show();
    void ActivateEventMode();
    void ActivateClassChangeMode();

    //--Core Methods
    void ChangeModeByTabCode(int pCode);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CMenu_SetProperty(lua_State *L);
