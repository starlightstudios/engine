-- |[ ====================================== Sample Script ===================================== ]|
--Not an actual script meant to be executed. Contains a variety of useful code snippets.

-- |[Argument Stuff]|
--Use the argument checker to make sure the args are valid. Use LM_GetScriptArgument to get the args.
-- Remember, args count from zero!
local iArgsTotal = LM_GetNumOfArgs()
if(fnArgCheck(1) == true) then
	sArgument = LM_GetScriptArgument(0)
	iArgument = LM_GetScriptArgument(1, "N")
end

-- |[ ================================= Copyable Divider to 100 ================================ ]|
-- |[ ============================ Copyable Divider to 90 ============================ ]|
-- |[ ======================= Copyable Divider to 80 ======================= ]|
-- |[ ================== Copyable Divider to 70 ================== ]|
-- |[ ============= Copyable Divider to 60 ============= ]|
-- |[ ======== Copyable Divider to 50 ======== ]|


-- |[ ====================================== Constructor ======================================= ]|
-- |[ ======================================== Triggers ======================================== ]|
-- |[ ====================================== Examination ======================================= ]|
-- |[ ========================================= Exits ========================================== ]|
-- |[ ==================================== Dialogue Handlers =================================== ]|
-- |[ ===================================== Warp Activation ==================================== ]|
-- |[Examination]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Thought:[VOICE|Leader] (Examination.)") ]])
fnCutsceneBlocker()

-- |[Dialogue with an NPC]|
TA_SetProperty("Face Character", "PlayerEntity")
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Mei:[VOICE|Mei] Dialogue goes here.[B][C]") ]])
fnCutsceneBlocker()

-- |[Minor Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Mei:[VOICE|Mei] Dialogue goes here.[B][C]") ]])
fnCutsceneBlocker()

-- |[Major Dialogue]|
--Function
fnStandardMajorDialogue()

--Or...
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] Dialogue goes here.[B][C]") ]])
fnCutsceneBlocker()

--Shows the party on the left side. Autogenerates which characters are present.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
fnCutscene([[ Append("Christine:[E|Neutral] Dialogue goes here.[B][C]") ]])
fnCutsceneBlocker()

--Splits the party between left and right sides. Autogenerates which characters are present.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
fnCutscene([[ Append("Christine:[E|Neutral] Dialogue goes here.[B][C]") ]])
fnCutsceneBlocker()

--Restores the characters previously on the field. Also uses Major Sequence Fast. Good for instantly swapping which characters are present.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Restore Previous Actors") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Dialogue goes here.[B][C]") ]])
fnCutsceneBlocker()

-- |[Scene]|
--Used during TF scenes, shows an image in the background.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ Append("Dogs![B][C]") ]])

-- |[Useful Tags]|
fnCutscene([[ Append("Mei:[E|Neutral][VOICE|MercF] Dialogue goes here.[SOUND|World|TakeItem][B][C]") ]])

-- |[ ==================================== Decision Handlers =================================== ]|
-- |[Dialogue Decisions]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Dogs or cats?[B]") ]])

--Short decision handler.
--fnDecisionSet(sCallPath, {{sNameA, sCallA}, {sNameB, sCallB}})
fnDecisionSet(LM_GetCallStack(0), {{"No", "Execution"}, {"Yes", "Skipper"}})

--Decision script is this script. It must be surrounded by quotes.
local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
fnCutscene(" WD_SetProperty(\"Add Decision\", \"Dogs\", " .. sDecisionScript .. ", \"Dogs\") ")
fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cats\",  " .. sDecisionScript .. ", \"Cats\") ")
fnCutsceneBlocker()

-- |[Post Decision]|
if(sTopicName == "Dogs") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Yep.[B][C]") ]])
end

-- |[ ============================== Movement and World Handlers =============================== ]|
-- |[Fading]|
--Fade out. Pass nothing to do so instantly.
fnCutsceneFadeOut(45)
fnCutsceneBlocker()
fnCutsceneFadeOut()
fnCutsceneBlocker()

--Fade in
fnCutsceneFadeIn(45)
fnCutsceneBlocker()

-- |[Function Versions]|
--As above, but quick-use functions.
fnCutsceneMove("Mei", 1.25, 1.50)
fnCutsceneMove("Mei", 1.25, 1.50, 1.00) --Specifies the speed.
fnCutsceneJumpTo("Mei", 1.25, 1.50, 25) --Last number is how many ticks
fnCutsceneMoveAmount("Mei", 1.00, 0.00) --Move a number of tiles instead of to a specific location.
fnCutsceneMoveFace("Mei", 1.25, 1.50, 0, 1) --Specifies the facing.
fnCutsceneMoveFace("Mei", 1.25, 1.50, 0, 1, 1.00) --Specifies the facing and speed.
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneFaceTarget("Christine", "Sophie")
fnCutsceneFacePos("Christine", 1.25, 1.50) --Faces a position based on where the character is at call time.
fnCutsceneTeleport("Mei", 1.25, 1.50)
fnCutsceneJumpNPC("Florentina", gcfStdJumpSpeed)
fnCutsceneSetFrame("Mei", "Crouch")
fnCutsceneSetAnim("Mei", "Dancing")
fnCutscenePlaySound("World|Thump")
fnCutsceneCamera("Actor ID", gcfCamera_Cutscene_Speed, 1)
fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Mei")
fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 1.25, 1.50)
fnCutsceneCall(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua")
fnCutsceneSetVar("Root/Variables/Arg/Dogs/iGoodBoys", "N", 1)
fnCutsceneSetVar("Root/Variables/Arg/Dogs/sGoodBoys", "S", "Yes")

--List-based call. Do not use iWaitTicks if multiple actors are moving at once or the blockers will interrupt each other.
--fnCutsceneMoveList("Florentina", { {fX, fY, fSpeed, iStopFacingX, iStopFacingY, iWaitTicks}, {EntryB}, {EntryC}, ..., {EntryX} })

-- |[Timing]|
--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Party Folding]|
--Move everyone onto the leader following the walk logic. Used at the start of cutscenes to make sure
-- characters don't get stuck on corners.
fnCutsceneMergeParty()
fnCutsceneBlocker()

--Move everyone onto the leader, and call the fold automatically.
fnAutoFoldParty()
fnCutsceneBlocker()

--Version where everyone is moved manually onto the party leader.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

-- |[ ============================= Non-Movement Cutscene Handlers ============================= ]|
-- |[Character Presence]|
--Returns whether or not a given character is currently *in the party*.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Returns whether the character is on the field. Does not matter if they are in the party.
local bIsCharacterPresent = fnCharacterExists("Florentina")

-- |[Rest Actions]|
--This restores the party to full and causes enemies to respawn.
fnCutscene([[ AM_SetProperty("Execute Rest") ]])

-- |[Animating with Layers]|
--Enables or disables a layer on the map. All layers are enabled by default.
AL_SetProperty("Set Layer Disabled", sLayerName, bFlag)
fnCutsceneLayerDisabled(sLayerName, bIsDisabled) --In-cutscene stride

-- |[Change Actor Collision Flag]|
--Useful during cutscenes when actors might walk through one another.
TA_ChangeCollisionFlag("Sophie", true)

-- |[Change Map Collisions]|
--Modifies a map collision, allowing an area to be opened or blocked off.
AL_SetProperty("Set Collision", iX, iY, iZ, iCollisionValue)

-- |[Map Transition]|
--Scene transit. MAPNAME can be LASTSAVE to go to the last save point the player used.
fnCutscene([[ AL_BeginTransitionTo("MAPNAME", gsRoot .. "Chapter1Scenes/Defeat_Cultists/Scene_PostTransition.lua") ]])
fnCutscene([[ AL_BeginTransitionTo("MAPNAME", "FORCEPOS:10.0x12.0x0") ]])
fnCutsceneBlocker()

--Set the last save point. Can be used to prevent a player from deathwarping past an important cutscene trigger.
AL_SetProperty("Last Save Point", "TrapDungeonA")

-- |[Music]|
--Changes theme playing. Pass "Null" to turn music off.
AL_SetProperty("Music", "Null")

-- |[Common Sound Effects]|
--Exits using auto-doors, access is denied. Used a lot in chpater 5.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access denied.") ]])
fnCutsceneBlocker()

--Auto door SFX.
AudioManager_PlaySound("World|AutoDoorOpen")

--Door sample, with face-changer.
if(sObjectName == "ToHydroponicsBLft") then
    gi_Force_Facing = gci_Face_South
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:4.0x7.0x0")
end

--Ladders.
AudioManager_PlaySound("World|ClimbLadder")

-- |[Topics]|
WD_SetProperty("Unlock Topic", "Rilmani", 1)
WD_SetProperty("Clear Topic Read", "Rilmani")

-- |[NPC Creation]|
TA_Create("NPCName")
	TA_SetProperty("Position", -1, -1)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", true)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/TranEquipVendor/Root.lua")
	fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
DL_PopActiveObject()
fnSpecialCharacter("Florentina", -100, -100, gci_Face_South, false, nil)

--All special characters:
fnSpecialCharacter("Mei", -100, -100, gci_Face_South, false, nil)
fnSpecialCharacter("Christine", -100, -100, gci_Face_South, false, nil)
fnSpecialCharacter("Sophie", -100, -100, gci_Face_South, false, nil)
fnSpecialCharacter("Florentina", -100, -100, gci_Face_South, false, nil)
fnSpecialCharacter("55", -100, -100, gci_Face_South, false, nil)
fnSpecialCharacter("SX399Lord", -100, -100, gci_Face_South, false, nil)
fnSpecialCharacter("Sammy", -100, -100, gci_Face_South, false, nil)
fnSpecialCharacter("JX101", -100, -100, gci_Face_South, false, nil)		
fnSpecialCharacter("SX399", -100, -100, gci_Face_South, false, nil)

--Party-member adders.
fnAddPartyMember("55", "Doll", "55", "Root/Variables/Chapter5/Scenes/iIs55Following")
fnAddPartyMember("JX101", "SteamDroid", "JX-101", "Root/Variables/Chapter5/Scenes/iIsJX101Following")
fnAddPartyMember("Sophie", "Golem", "Null", "Null")

--Preset spawners.
fnSpawnNPCPattern("Golem", "A", "F")
fnStandardNPCByPosition("Isabel")

-- |[Items]|
--Use this command to add items to the player's inventory.
LM_ExecuteScript(gsItemListing, "Recycleable Junk")








