///===================================== Peak Freaks Level ========================================
//--A level used in the game Peak Freaks. Inherits from the TiledLevel class. Contains world data
//  and UI pieces since the UI is baked into the game.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "TiledLevel.h"
#include "PeakFreaksCommonDef.h"
#include "PeakFreaksPuzzlePiece.h"
#include "PeakFreaksLevelStructures.h"
class TileLayer;

//--Fireworks
#ifndef PFTITLE_FIREWORKS_COLS
#define PFTITLE_FIREWORKS_COLS 4
#define PFTITLE_FIREWORKS_FRAMES 13
#endif

///===================================== Local Structures =========================================
///--[Level Layout]
//--Uses RECON_X constants to create an abstract level layout.
typedef struct PF_LevelLayout
{
    //--Members
    int mXSize;
    int mYSize;
    int **mArray;
    int **mPriorities;

    //--Functions
    void Initialize()
    {
        mXSize = 0;
        mYSize = 0;
        mArray = NULL;
        mPriorities = NULL;
    }
    void Deallocate()
    {
        for(int x = 0; x < mXSize; x ++) free(mArray[x]);
        for(int x = 0; x < mXSize; x ++) free(mPriorities[x]);
        free(mArray);
        free(mPriorities);
    }
}PF_LevelLayout;

///--[ZonePack]
//--Represents a hitbox. What the hitbox does is contextual.
class ZonePack
{
    public:
    int mSubtype; //Context-sensitive.
    int mLayer;
    int mFlag; //Context-sensitive.
    TwoDimensionReal mDim;

    ZonePack()
    {
        mSubtype = 0;
        mLayer = 0;
        mFlag = 0;
        mDim.Set(0.0f, 0.0f, 1.0f, 1.0f);
    }
    static void DeleteThis(void *pPtr)
    {
        ZonePack *rPtr = (ZonePack *)pPtr;
        delete rPtr;
    }
};

///--[DropZonePack]
//--Inherits from the ZonePack, stores an additional integer that specifies what layer to move the
//  player to when they touch it.
class DropZonePack : public ZonePack
{
    public:
    int mDestination;
    int mLayer;
    TwoDimensionReal mDim;

    DropZonePack()
    {
        mLayer = 0;
        mDestination = 0;
        mDim.Set(0.0f, 0.0f, 1.0f, 1.0f);
    }
    static void DeleteThis(void *pPtr)
    {
        DropZonePack *rPtr = (DropZonePack *)pPtr;
        delete rPtr;
    }
};

///--[Particle]
//--A sprite that moves under some logic.
typedef struct PF_Particle
{
    //--Variables
    int mTimer;
    float mXPos;
    float mYPos;
    float mXSpeed;
    float mYSpeed;
    SugarBitmap *rImage;

    //--Functions
    void Initialize()
    {
        mTimer = 0;
        mXPos = 0.0f;
        mYPos = 0.0f;
        mXSpeed = 0.0f;
        mYSpeed = 0.0f;
        rImage = NULL;
    }
}PF_Particle;

//--Particle Constants
#define PFCB_SPAWN_TIME 12
#define PFCB_PL_LFT 3
#define PFCB_PL_TOP 86
#define PFCB_PL_WID 174
#define PFCB_PL_HEI 349
#define PFCB_RV_LFT 1158
#define PFCB_RV_TOP 86
#define PFCB_RV_WID 174
#define PFCB_RV_HEI 636
#define PFCB_XSPEED_LO 1.0f
#define PFCB_XSPEED_RANGE 30
#define PFCB_YSPEED_LO -2.0f
#define PFCB_YSPEED_RANGE 20
#define PFCB_GRAVITY 0.625f
#define PFCB_ICE_GRAVITY 0.125f

///===================================== Local Definitions ========================================
///--[Reconstituting Constants]
#define RECON_EMPTY 0
#define RECON_START_N 1
#define RECON_START_E 2
#define RECON_START_S 3
#define RECON_START_W 4
#define RECON_END_N 5
#define RECON_END_E 6
#define RECON_END_S 7
#define RECON_END_W 8
#define RECON_HORIZONTAL 9
#define RECON_VERTICAL 10
#define RECON_ELBOW_LU 11
#define RECON_ELBOW_RU 12
#define RECON_ELBOW_LD 13
#define RECON_ELBOW_RD 14

#define LEVEL_LENGTH 7
#define LEVEL_INIT_X 7
#define LEVEL_INIT_Y 7

#define ROLL_N 0
#define ROLL_E 1
#define ROLL_S 2
#define ROLL_W 3
#define ROLL_CAP 4

///--[Puzzle Grid Constants]
//--Grid Size
#define PLL_PUZZLE_SIZE_X 7
#define PLL_PUZZLE_SIZE_Y 7
#define PLL_PUZZLE_MID_X 3
#define PLL_PUZZLE_MID_Y 3

//--Grid UI Position
#define PLL_PUZZLE_X  16.0f
#define PLL_PUZZLE_Y 452.0f
#define PLL_PUZZLE_W  42.0f
#define PLL_PUZZLE_H  42.0f

//--Grid Timers
#define PLL_PUZZLE_MOVE_TICKS 3

//--Speeds
#define PLL_PUZZLE_SPEEDS 5

//--Power Clamps
#define PLL_POWER_MAX 3

///--[Stencil Codes]
#define PEAKFREAKS_PLAYER_STENCIL 1
#define PEAKFREAKS_PUZZLE_STENCIL 2

///--[Reconstitution Constants]
//--Sizes
#define CHUNK_W 8
#define CHUNK_H 8

///--[Pause Menu]
//--Frame Constants
#define PFLEVEL_PAUSE_ALIEN_FRAMES 3

//--Cursor
#define PFLEVEL_PAUSE_RESUME 0
#define PFLEVEL_PAUSE_RESTART 1
#define PFLEVEL_PAUSE_QUIT 2

//--Timers
#define PFLEVEL_PAUSE_CURSOR_TICKS 30
#define PFLEVEL_PAUSE_CURSOR_DIST 5.0f
#define PFLEVEL_PAUSE_SLIDE_TICKS 15
#define PFLEVEL_PAUSE_SLIDE_TICKS_CURSOR_OFF -5
#define PFLEVEL_PAUSE_ALIEN_FLOAT_TICKS 25
#define PFLEVEL_PAUSE_ALIEN_FLOAT_DIST 5.0f
#define PFLEVEL_PAUSE_ALIEN_ANIM_TPF 5.0f

///--[Opening]
//--Frame Constants
#define PFLEVEL_OPENING_ALIEN_FRAMES 4
#define PFLEVEL_OPENING_BANNER_FRAMES 4
#define PFLEVEL_OPENING_COUNTDOWN_FRAMES 5

///--[Coins and Powerups]
//--Subtypes
#define PFLEVEL_COIN_SUBTYPE_COIN 0
#define PFLEVEL_COIN_SUBTYPE_GUANOBOMB 1
#define PFLEVEL_COIN_SUBTYPE_DRONESTRIKE 2
#define PFLEVEL_COIN_SUBTYPE_SUPERSTRIKE 3
#define PFLEVEL_COIN_SUBTYPE_CANYONBAR 4
#define PFLEVEL_COIN_SUBTYPE_HELILIFT 5
#define PFLEVEL_COIN_SUBTYPE_ICEBEAM 6
#define PFLEVEL_COIN_SUBTYPE_ROLL_LOW (PFLEVEL_COIN_SUBTYPE_GUANOBOMB)
#define PFLEVEL_COIN_SUBTYPE_ROLL_HI (PFLEVEL_COIN_SUBTYPE_ICEBEAM)

//--Ice Beam
#define PFLEVEL_ICEBEAM_ACTIVATION_COOLDOWN 120
#define PFLEVEL_ICEBEAM_DURATION 600

//--Coins
#define PFLEVEL_COIN_FRAMES 4
#define PFLEVEL_COIN_TPF 6

///--[Hazard Constants]
#define PF_SLIDER_TRACE_TICKS 3
#define PF_SLIDER_SHAKE_TICKS 45
#define PF_SLIDER_MOVE_TICKS 60

///--[Timers]
#define PF_VICTORY_TICKS 300
#define PF_SELECTION_OFFSET_TIMER 30
#define PF_SELECTION_OFFSET_REPEAT 30
#define PF_SELECTION_OFFSET_DISTANCE 16.0f
#define PF_CROSSFADE_TICKS 30
#define PF_FLAG_FRAMES 30
#define PF_FLAG_TPF 3

//--Cursor
#define PF_CURSOR_FRAMES 3
#define PF_CURSOR_TPF 4

///========================================== Classes =============================================
class PeakFreaksLevel : public TiledLevel
{
    private:
    ///--[System]
    bool mReturnToTitle;
    bool mRestartLevel;
    bool mSkipResultsScreen;
    char *mTriggerScript;
    bool mIsTutorial;
    float mLastCameraX;
    float mLastCameraY;
    int mSelectionOffsetTick;
    int mSelectionOffsetTimer;
    float mMusicVolumeNormal;

    ///--[Dialogue Handling / Tutorial]
    //--Main Dialogue
    bool mIsDialogueVisible;
    int mDialogueVisTimer;
    int mDialogueLinesTotal;
    char **mDialogueLines;
    char **mDialogueColors;
    int mTutorialClearState;

    //--Display
    int mDialogueFloatTimer;

    //--Current Goal
    int mCurrentGoalTimer;
    int mCurrentGoalTimerMax;
    float mCurrentGoalWid;
    char *mDialogueCurrentGoal;

    //--Tutorial State
    bool mDirectionMoveTutorialActive;
    bool mDirectionTutorialUp;
    bool mDirectionTutorialRt;
    bool mDirectionTutorialDn;
    bool mDirectionTutorialLf;
    bool mBigMoveTutorialActive;
    int mBigMoveCount;
    bool mFillTutorialActive;
    int mFillCount;
    bool mPowerJumpTutorialActive;
    int mPowerJumpCount;

    public:
    bool IsDialogueBlockingUpdate();
    void AllocateDialogueLines(int pTotal);
    void SetDialogueLine(int pSlot, const char *pDialogue);
    void SetDialogueColors(int pSlot, const char *pColorString);
    void SetObjective(const char *pObjective);
    void ActivateTutorialA();
    void ActivateTutorialB();
    void ActivateTutorialC();
    void ActivateTutorialD();
    void ActivateTutorialE();
    void ActivateTutorialF();
    void ActivateTutorialG();
    void ActivateTutorialH();
    void ActivateTutorialI();
    void ActivateTutorialJ();
    void ActivateTutorialK();
    void ActivateTutorialL();
    void ActivateTutorialM();
    void ActivateTutorialN();
    void ActivateTutorialO();
    void ActivateTutorialP();
    void ActivateTutorialQ();
    void ActivateTutorialR();
    void ActivateTutorialS();
    void ActivateTutorialT();
    void ActivateTutorialU();
    void ActivateTutorialByCode(int pCode);
    void UpdateDialogue();
    void RenderDialogue();
    void RenderObjective();
    private:

    ///--[Track Minimap]
    float mPlayerTrackProgress;
    float mRivalTrackProgress;
    int mProgressSizeX;
    int mProgressSizeY;
    float **mTileProgressValues;

    ///--[Finale Fireworks]
    int mFireworksTimer;
    SugarLinkedList *mFireworks; //PFTitle_Firework *, master

    ///--[Victory/Defeat]
    bool mPlayerReachedVictoryFirst;
    void *rVictoriousEntity;
    int mVictoryTimer;
    bool mReturnToTitleVictory;
    bool mReturnToTitleDefeat;
    bool mShowPersonalBest;

    ///--[Opening]
    bool mIsOpening;
    int mOpeningTimer;
    int mOverallOpenTimer;
    float mOpeningCameraX;
    float mOpeningCameraY;
    float mPrevOpeningX;
    float mPrevOpeningY;
    SugarLinkedList *mOpeningPositions; //Point2DF *, master
    public:
    void ClearOpening();
    void UpdateOpening();
    void RenderOpening();
    private:

    ///--[Pause Menu]
    bool mIsPaused;
    int mPauseToTitleTimer;
    int mPauseCursor;
    int mPauseTimerCursor;
    int mPauseTimerBG;
    int mPauseTimerMenu;
    int mPauseTimerAlienFloat;
    int mPauseTimerAlienAnim;

    ///--[Puzzle Handling]
    SugarLinkedList *mFragmentList; //PeakFreaksFragment *, master
    PeakFreaksPuzzlePiece ***mPuzzleGrid;

    ///--[Clouds]
    int mCloudSpawnTimer;
    SugarLinkedList *mCloudList; //PFTitle_Cloud *, master

    ///--[Player Handling]
    //--Player.
    int mCursorTimer;
    PeakFreaksPlayer *mPlayer;
    float mSpeeds[PLL_PUZZLE_SPEEDS];

    //--Rival.
    PeakFreaksPlayer *mRival;

    //--UI Badges
    SugarLinkedList *mBadgeList; //PeakFreaksBadge *, master

    //--World Particles
    SugarLinkedList *mWorldFragmentList; //PeakFreaksFragment *, master

    //--Checkpoints
    char *mActiveCheckpointName;
    float mCheckpointX;
    float mCheckpointY;
    int mCheckpointZ;
    char *mRivalActiveCheckpointName;
    float mRivalCheckpointX;
    float mRivalCheckpointY;
    int mRivalCheckpointZ;

    //--UI Particles
    SugarLinkedList *mCanyonBarParticles; //PF_Particle *, master
    SugarLinkedList *mIceBeamParticles; //PF_Particle *, master
    SugarLinkedList *mSpeedBoostParticles; //PF_Particle *, master

    //--Zones
    SugarLinkedList *mPlayerStartZones; //ZonePack *, master
    SugarLinkedList *mDamageZones; //ZonePack *, master
    SugarLinkedList *mVictoryZones; //ZonePack *, master
    SugarLinkedList *mCoinZones; //ZonePack *, master
    SugarLinkedList *mTreasureZones; //ZonePack *, master
    SugarLinkedList *mLayerDropZones; //DropZonePack *, master
    SugarLinkedList *mCheckpointZones; //ZonePack *, master
    SugarLinkedList *mSlipZones; //ZonePack *, master
    SugarLinkedList *mClimbZones; //ZonePack *, master
    SugarLinkedList *mTriggerZones; //ZonePack *, master

    //--Hazard Entities
    SugarLinkedList *mSliderList;//PF_Slider *, master
    SugarLinkedList *mSpinnerList;//PF_Spinner *, master
    SugarLinkedList *mWaterfallList;//PF_Waterfall *, master
    SugarLinkedList *mBombSpikeList;//PF_BombSpike *, master

    //--Reconstituting Zones
    int mCheckpointCount;
    SugarLinkedList *mReconPlayerStartZones; //ZonePack *, master
    SugarLinkedList *mReconDamageZones; //ZonePack *, master
    SugarLinkedList *mReconVictoryZones; //ZonePack *, master
    SugarLinkedList *mReconCoinZones; //ZonePack *, master
    SugarLinkedList *mReconTreasureZones; //ZonePack *, master
    SugarLinkedList *mReconLayerDropZones; //DropZonePack *, master
    SugarLinkedList *mReconCheckpointZones; //ZonePack *, master
    SugarLinkedList *mReconSlipZones; //ZonePack *, master
    SugarLinkedList *mReconClimbZones; //ZonePack *, master
    SugarLinkedList *mReconTriggerZones; //ZonePack *, master
    SugarLinkedList *mReconSliderList;//PF_Slider *, master
    SugarLinkedList *mReconSpinnerList;//PF_Spinner *, master
    SugarLinkedList *mReconWaterfallList;//PF_Waterfall *, master
    SugarLinkedList *mReconBombSpikeList;//PF_BombSpike *, master

    ///--[Powerups]
    //--Coin Timer.
    int mCoinTimer;

    //--Ice beam. Only one.
    int mIceBeamCountdown;
    int mIceBeamTimer;

    //--Flybys. These handle drone stike, super strike, and guano bomb.
    SugarLinkedList *mFlybyList; //PeakFreaksFlyby *, master
    SugarLinkedList *mBombList; //PeakFreaksBomb *, master

    ///--[Goal Flags]
    bool mResolveGoalPosition;
    int mGoalX;
    int mGoalY;
    int mGoalFlagTimer;

    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            //--Utility
            SugarFont *rUIFont;
            SugarFont *rTutorialFont;
            SugarFont *rTutorialGoalFont;

            //--Objects
            SugarBitmap *rCoin[PFLEVEL_COIN_FRAMES];
            SugarBitmap *rTreasureClosed;
            SugarBitmap *rPowerup_Strike;
            SugarBitmap *rPowerup_SuperStrike;
            SugarBitmap *rPowerup_IceBeam;
            SugarBitmap *rPowerup_CanyonBar;
            SugarBitmap *rPowerup_HeliLift;
            SugarBitmap *rPowerup_GuanoBomb;
            SugarBitmap *rHazardSlider;
            SugarBitmap *rHazardSpinner[PF_SPINNER_SMALL_DIRS];
            SugarBitmap *rHazardSpinnerBig[PF_SPINNER_BIG_DIRS];
            SugarBitmap *rHazardWaterfallFrames[PF_WATERFALL_FRAMES];
            SugarBitmap *rHazardWaterfallTop;
            SugarBitmap *rHazardWaterfallMid;
            SugarBitmap *rHazardWaterfallBot;
            SugarBitmap *rHazardBombSpikeNeutral;
            SugarBitmap *rHazardBombSpikeExplosion[PF_BOMBSPIKE_EXPLOSION_FRAMES];
            SugarBitmap *rDustParticle;
            SugarBitmap *rGoalFlags[PF_FLAG_FRAMES];

            //--Checkpoints
            SugarBitmap *rCheckpointIH4;
            SugarBitmap *rCheckpointAH4;
            SugarBitmap *rCheckpointIH3;
            SugarBitmap *rCheckpointAH3;
            SugarBitmap *rCheckpointIH2;
            SugarBitmap *rCheckpointAH2;
            SugarBitmap *rCheckpointIV4;
            SugarBitmap *rCheckpointAV4;
            SugarBitmap *rCheckpointIV3;
            SugarBitmap *rCheckpointAV3;
            SugarBitmap *rCheckpointIV2;
            SugarBitmap *rCheckpointAV2;

            //--Tutorial
            SugarBitmap *rTutorialDialogue;
            SugarBitmap *rTutorialAlien;
            SugarBitmap *rTutorialPrompt;
            SugarBitmap *rTutorialGoal;
            SugarBitmap *rTutorialHand;
            SugarBitmap *rTutorialGoalMask;

            //--Powerup: Drone Strike
            SugarBitmap *rPowerupDroneImg0;
            SugarBitmap *rPowerupDroneImg1;
            SugarBitmap *rPowerupDroneTarget;
            SugarBitmap *rPowerupBombNormal;
            SugarBitmap *rPowerupBombSuper;
            SugarBitmap *rPowerupExplosion[4];

            //--Powerup: Guano Bomb
            SugarBitmap *rPowerupGuanoBird;
            SugarBitmap *rPowerupGuanoFall;
            SugarBitmap *rPowerupGuanoTrap;
            SugarBitmap *rPowerupGuanoOverlay;

            //--Powerup: Ice Beam
            SugarBitmap *rPowerupIceBeamOver;
            SugarBitmap *rPowerupIceBeamRain;

            //--UI
            SugarBitmap *rCursor[PF_CURSOR_FRAMES];
            SugarBitmap *rCrushIndicator;
            SugarBitmap *rUIBackground;
            SugarBitmap *rUIMenuButton;
            SugarBitmap *rUIPlayerBacking;
            SugarBitmap *rUIPlayerFrame;
            SugarBitmap *rUIPlayerGuanoOverlay;
            SugarBitmap *rUIRivalBacking;
            SugarBitmap *rUIRivalFrame;
            SugarBitmap *rUIRivalGuanoOverlay;
            SugarBitmap *rUIPlayerPuzzleAreaMask;
            SugarBitmap *rUIPlayerPuzzleArea;
            SugarBitmap *rUIPlayerSpeedFill0;
            SugarBitmap *rUIPlayerSpeedFill1;
            SugarBitmap *rUIPlayerSpeedFill2;
            SugarBitmap *rUIPlayerSpeedFill3;
            SugarBitmap *rUIPlayerSpeedFill4;
            SugarBitmap *rUIPlayerSpeedFrame;
            SugarBitmap *rTutorialA;
            SugarBitmap *rTutorialB;
            SugarBitmap *rCanyonBarFlame;
            SugarBitmap *rSpeedParticle;
            SugarBitmap *rTrackBack;
            SugarBitmap *rTrackFill;

            //--Medallions/Badges
            SugarBitmap *rBadgeMedallion;
            SugarBitmap *rBadgeAirstrike;
            SugarBitmap *rTitleAirstrike;
            SugarBitmap *rBadgeCanyonBar;
            SugarBitmap *rTitleCanyonBar;
            SugarBitmap *rBadgeGuano;
            SugarBitmap *rTitleGuano;
            SugarBitmap *rBadgeHelilift;
            SugarBitmap *rTitleHelilift;
            SugarBitmap *rBadgeIcebeam;
            SugarBitmap *rTitleIcebeam;
            SugarBitmap *rBadgeSuperstrike;
            SugarBitmap *rTitleSuperstrike;

            //--Pause Menu
            SugarBitmap *rPauseAlien[PFLEVEL_PAUSE_ALIEN_FRAMES];
            SugarBitmap *rPauseCursor;
            SugarBitmap *rPauseBG;
            SugarBitmap *rPauseSelQuit;
            SugarBitmap *rPauseSelResume;
            SugarBitmap *rPauseSelRestart;
            SugarBitmap *rPauseTextQuit;
            SugarBitmap *rPauseTextResume;
            SugarBitmap *rPauseTextRestart;

            //--Opening
            SugarBitmap *rOpeningAlien[PFLEVEL_OPENING_ALIEN_FRAMES];
            SugarBitmap *rOpeningBanner[PFLEVEL_OPENING_BANNER_FRAMES];
            SugarBitmap *rOpeningCountdown[PFLEVEL_OPENING_COUNTDOWN_FRAMES];

            //--Background
            SugarBitmap *rBackgroundSky;
            SugarBitmap *rClouds[PFTITLE_CLOUD_FRAMES];

            //--Puzzle Pieces
            SugarBitmap *mPuzzleFragments[PEAKFREAK_PUZZLE_TYPE_MAX];
            SugarBitmap *mPuzzleArray[PEAKFREAK_PUZZLE_TYPE_MAX];
        }Data;

        ///--[Finale]
        struct
        {
            SugarBitmap *rFireworks[PFTITLE_FIREWORKS_COLS][PFTITLE_FIREWORKS_FRAMES];
        }Finale;
    }Images;

    protected:

    public:
    //--System
    PeakFreaksLevel();
    virtual ~PeakFreaksLevel();

    //--Public Variables
    static int xMandatedSeed;
    static int xDifficulty;
    static int xTimeElapsed;
    static bool xPlayerVictory;
    static int xStartType;

    //--Property Queries
    virtual bool IsOfType(int pType);
    virtual bool GetClipAt(float pX, float pY, float pZ, int pLayer);
    virtual bool GetClipAt(float pX, float pY, float pZ);
    bool GetClipAtTile(int pX, int pY);
    bool IsOutOfBounds(int pX, int pY);
    int IsTouchingZone(SugarLinkedList *pZoneList, float pLft, float pTop, float pRgt, float pBot, int pLayer, bool pRemoveOnTrue);
    bool IsTouchingDamageZone(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    bool IsTouchingVictoryZone(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    int IsTouchingCoinZone(float pLft, float pTop, float pRgt, float pBot, int pLayer, bool pRemoveOnTrue);
    bool IsTouchingTreasureZone(float pLft, float pTop, float pRgt, float pBot, int pLayer, bool pRemoveOnTrue);
    int IsTouchingLayerDropZone(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    const char *IsTouchingCheckpoint(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    bool IsTouchingSlipZone(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    bool IsWithinSlipZone(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    bool IsTouchingClimbZone(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    bool IsWithinClimbZone(float pLft, float pTop, float pRgt, float pBot, int pLayer);
    float GetCheckpointX(void *pCaller);
    float GetCheckpointY(void *pCaller);
    int GetCheckpointZ(void *pCaller);
    bool IsTrampoline(int pTileX, int pTileY);

    //--Manipulators
    void SetTutorial();
    void SetTriggerScript(const char *pPath);
    void SpawnPlayer();
    void SpawnRival();
    void PositionPlayer(float pX, float pY);
    void SetPlayerLayer(int pZ);
    void SetRenderingDisabled(const char *pLayerName, bool pFlag);
    void ActivateCheckpoint(void *pCaller, const char *pCheckpointName);
    void ActivateRivalCheckpoint(const char *pCheckpointName);
    void SpawnBadge(bool pIsRival, int pCode);
    void ActivateIceBeam();
    void SpawnGuanoProjectile(int pX, int pY, float pHeight);
    void SpawnDroneProjectile(int pX, int pY, float pHeight);
    void SpawnSuperProjectile(int pX, int pY, float pHeight);
    void SpawnIceBeamParticles();
    void SpawnSpeedParticle(float pX, float pY, int pTimer);
    void StartVictory(void *pEntity);
    void SpawnDustParticles(int pTileX, int pTileY);

    //--Core Methods
    void SpawnCloud();
    void SpawnCloudEdge();
    void RandomizePuzzleGrid();
    void MovePuzzleGridUp();
    void MovePuzzleGridDn();
    void MovePuzzleGridLf();
    void MovePuzzleGridRt();
    void RebalanceList(SugarLinkedList *pList);
    void RebalanceGrid();
    void SpawnPowerups();
    void SpawnPowerupsInChunk(int pStartX, int pStartY);
    void BumpPlayerAbsolute(int pSrcX, int pSrcY, int pDstX, int pDstY);
    void BumpPlayerRelative(int pSrcX, int pSrcY, int pDstX, int pDstY);
    void HandlePlayerPowerup(void *rCallingEntity, int pCode);
    void HandlePlayerProgress(void *rCallingEntity, int pX, int pY);

    ///--Crushing
    void AutoCrush();
    bool CrushPuzzleGridUp();
    bool CrushPuzzleGridDn();
    bool CrushPuzzleGridLf();
    bool CrushPuzzleGridRt();
    void SpawnPuzzleFragments(int pGridX, int pGridY);

    ///--Generate
    PF_LevelLayout *GenerateLevel();
    PF_LevelLayout *GenerateTutorial();

    ///--Hazards
    ///--Pause Menu
    void Pause();
    void UpdatePause();
    void RenderPause();

    ///--Reconstitute
    void Reconstitute();
    void ReconstituteGrid(int pCode, int pX, int pY, int pLayersTotal, TileLayer **pLayerList, CollisionPack *pCollisions, CollisionPack *pAICollisions, CollisionPack *pPowerups);
    void CloneListInArea(SugarLinkedList *pList, SugarLinkedList *pNewList, float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop);
    void CloneCheckpointsInArea(SugarLinkedList *pList, SugarLinkedList *pNewList, float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop);
    void CloneSlidersInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop);
    void CloneSpinnersInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop);
    void CloneWaterfallsInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop);
    void CloneBombSpikesInArea(float pLft, float pTop, float pRgt, float pBot, float pNewLft, float pNewTop);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    void HandleControlUpdate(PeakFreaksPlayer *pPlayer);

    //--File I/O
    virtual void ParseObjectData();
    ZonePack *CreateStandardZone(ObjectInfoPack *pPack, bool pOffsetYTile);
    void HandleObject(ObjectInfoPack *pPack);

    //--Drawing
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();
    void RenderEntities();
    void RenderTilemap();
    void RenderUI();
    void SetMatchingPiecesOffsetTick();

    //--Pointer Routing
    PeakFreaksPlayer *GetPlayer();
    PeakFreaksPlayer *GetRival();
    ZonePack *GetPlayerStart(int pLayer);

    //--Static Functions
    static PeakFreaksLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PeakLev_Create(lua_State *L);
int Hook_PeakLev_ParseSLF(lua_State *L);
int Hook_PeakLev_GetProperty(lua_State *L);
int Hook_PeakLev_SetProperty(lua_State *L);

