///==================================== AbyCombatPrediction =======================================
//--Prediction box that appears over a target during target selection. Modified for the new UI pieces
//  of the AbyssCombat class, but is only a visual change.

#include "Definitions.h"
#include "Structures.h"
#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "AdvCombatPrediction.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AbyCombatPrediction : public AdvCombatPrediction
{
    private:
    protected:
    //--System
    float mLocalAlpha;

    public:
    //--System
    AbyCombatPrediction();
    virtual ~AbyCombatPrediction();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    void SetLocalAlpha(float pAlpha);

    //--Core Methods

    private:
    //--Private Core Methods
    public:
    //--Update

    //--File I/O
    //--Drawing
    virtual void Render();

    //--Pointer Routing

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

