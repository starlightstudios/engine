-- |[ ================================== Options Routing Menu ================================== ]|
--Allows the player to select one of several submenus with their own option sets.

-- |[Setup]|
local sBasePath = fnResolvePath()
Debug_PushPrint(false, "Populating options router.\n")

-- |[Header]|
FlexMenu_SetHeaderSize(1)
FlexMenu_SetHeaderLine(0, "Options")

-- |[Game Checking]|
--If Classic mode is not available, then the game options for Classic don't appear.
local bAdventure = fnGameExists("Adventure Mode")
local bClassic   = fnGameExists("Classic Mode")

-- |[ ====================================== Menu Options ====================================== ]|
-- |[Buttons]|
--Engine and Display, sets things that affect the engine itself. This includes things like resolution and mipmapping.
FlexMenu_RegisterButton("Engine and Display Options")
	StarButton_SetProperty("Priority", 0)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Go To Submenu.lua", 0)
DL_PopActiveObject()

--Adventure Mode options.
if(bAdventure == true) then
    FlexMenu_RegisterButton("Adventure Mode Options")
        StarButton_SetProperty("Priority", 1)
        StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Go To Submenu.lua", 1)
    DL_PopActiveObject()

end

--Classic Mode options.
if(bClassic == true) then
    FlexMenu_RegisterButton("Classic Mode Options")
        StarButton_SetProperty("Priority", 2)
        StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Go To Submenu.lua", 2)
    DL_PopActiveObject()
end

--Back to the main menu.
FlexMenu_RegisterButton("Back")
	StarButton_SetProperty("Priority", 10000)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "900 Close.lua", 0)
DL_PopActiveObject()

-- |[Debug]|
Debug_PopPrint("Completed populating main menu.\n")