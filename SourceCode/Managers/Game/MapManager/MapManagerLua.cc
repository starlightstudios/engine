//--Base
#include "MapManager.h"

//--Classes
#include "WorldDialogue.h"

//--Definitions
//--Generics
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void MapManager::HookToLuaState(lua_State *pLuaState)
{
    /* MapM_IssueReset(sResetType)
       Causes the MapManager to run one of its internal reset codes. */
    lua_register(pLuaState, "MapM_IssueReset", &Hook_MapM_IssueReset);

    /* MapM_SetOneGame(bFlag)
       Sets the only-one-game flag. Some title screens use this to exit the program directly. */
    lua_register(pLuaState, "MapM_SetOneGame", &Hook_MapM_SetOneGame);

    /* MapM_SetOverlayColor(fRed, fGreen, fBlue, fAlpha)
       Sets the color that is rendered over top of the map.  It's a quad of the matching color and
       has blending turned on.  Pass a negative fAlpha to turn overlays off altogether. */
    lua_register(pLuaState, "MapM_SetOverlayColor", &Hook_MapM_SetOverlayColor);

    /* MapM_Shake(fRadius, fSeconds)
       Shakes the screen with the provided radius for the provided duration. */
    lua_register(pLuaState, "MapM_Shake", &Hook_MapM_Shake);

    /* MapM_PushMenuStackHead()
       Pushes whatever is currently atop the Menu Stack onto the Activity Stack. Can legally push NULL! */
    lua_register(pLuaState, "MapM_PushMenuStackHead", &Hook_MapM_PushMenuStackHead);

    /* MapM_BackToTitle()
       Flags the program to return to the Title Screen. */
    lua_register(pLuaState, "MapM_BackToTitle", &Hook_MapM_BackToTitle);

    /* MapM_ReplaceDialogueWithWorldDialogue()
       Replaces the existing dialogue with a fresh WorldDialogue class. Construct() will need to be called. */
    lua_register(pLuaState, "MapM_ReplaceDialogueWithWorldDialogue", &Hook_MapM_ReplaceDialogueWithWorldDialogue);

    /* MapM_ReplaceDialogueWithMonocerosDialogue()
       Replaces the existing dialogue with a fresh MonocerosDialogue class. Reset() will need to be called. */
    lua_register(pLuaState, "MapM_ReplaceDialogueWithMonocerosDialogue", &Hook_MapM_ReplaceDialogueWithMonocerosDialogue);

    /* MapM_ChangeMenuHandler(iType)
       Changes what type of adventure menu class handles normal menu activites. */
    lua_register(pLuaState, "MapM_ChangeMenuHandler", &Hook_MapM_ChangeMenuHandler);

    /* MapM_ChangeCombatHandler(iType)
       Changes what type of adventure combat class handles normal combat activites. */
    lua_register(pLuaState, "MapM_ChangeCombatHandler", &Hook_MapM_ChangeCombatHandler);

    /* MapM_ClearCatalystList()
       Clears and resets the catalyst listing. */
    lua_register(pLuaState, "MapM_ClearCatalystList", &Hook_MapM_ClearCatalystList);

    /* MapM_RegisterCatalyst(sChapter, sLevel, sChest, sContents)
       Registers a new catalyst entry. This is used to count catalysts in the UI based on the associated chapter. */
    lua_register(pLuaState, "MapM_RegisterCatalyst", &Hook_MapM_RegisterCatalyst);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_MapM_IssueReset(lua_State *L)
{
    //MapM_IssueReset(sResetType)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("MapM_SetOneGame");

    MapManager::Fetch()->Reset(lua_tostring(L, 1));
    return 0;
}
int Hook_MapM_SetOneGame(lua_State *L)
{
    //MapM_SetOneGame(bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("MapM_SetOneGame");

    MapManager::xOnlyOneGame = lua_toboolean(L, 1);
    return 0;
}
int Hook_MapM_SetOverlayColor(lua_State *L)
{
    //MapM_SetOverlayColor(fRed, fGreen, fBlue, fAlpha)
    int tArgs = lua_gettop(L);
    if(tArgs != 4) return LuaArgError("MapM_SetOverlayColor");

    MapManager::Fetch()->SetOverlayColor(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    return 0;
}
int Hook_MapM_Shake(lua_State *L)
{
    //MapM_Shake(fRadius, fSeconds)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("MapM_Shake");

    MapManager::Fetch()->BeginShake(lua_tonumber(L, 1), lua_tonumber(L, 2) * SECONDSTOTICKS);
    return 0;
}
int Hook_MapM_PushMenuStackHead(lua_State *L)
{
    //MapM_PushMenuStackHead()
    void *rPtr = MapManager::Fetch()->GetMenuStackHead();
    DataLibrary::Fetch()->PushActiveEntity(rPtr);
    return 0;
}
int Hook_MapM_BackToTitle(lua_State *L)
{
    //MapM_BackToTitle()
    MapManager::Fetch()->mBackToTitle = true;
    return 0;
}
int Hook_MapM_ReplaceDialogueWithWorldDialogue(lua_State *L)
{
    //MapM_ReplaceDialogueWithWorldDialogue()
    WorldDialogue *rDialogue = WorldDialogue::Fetch();
    if(rDialogue && rDialogue->GetType() == POINTER_TYPE_WORLDDIALOGUE) return 0;

    MapManager::Fetch()->ChangeWorldDialogueToWorldDialogue();
    return 0;
}
int Hook_MapM_ReplaceDialogueWithMonocerosDialogue(lua_State *L)
{
    //MapM_ReplaceDialogueWithMonocerosDialogue()
    WorldDialogue *rDialogue = WorldDialogue::Fetch();
    if(rDialogue && rDialogue->GetType() == POINTER_TYPE_MONOCEROSDIALOGUE) return 0;

    MapManager::Fetch()->ChangeDialogueType(POINTER_TYPE_MONOCEROSDIALOGUE);
    return 0;
}
int Hook_MapM_ChangeMenuHandler(lua_State *L)
{
    //Hook_MapM_ChangeMenuHandler(iType)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("Hook_MapM_ChangeMenuHandler");

    //--Set.
    MapManager::Fetch()->SetMenuType(lua_tointeger(L, 1));
    return 0;
}
int Hook_MapM_ChangeCombatHandler(lua_State *L)
{
    //MapM_ChangeCombatHandler(iType)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("MapM_ChangeCombatHandler");

    //--Set.
    MapManager::Fetch()->SetCombatType(lua_tointeger(L, 1));
    return 0;
}
int Hook_MapM_ClearCatalystList(lua_State *L)
{
    //MapM_ClearCatalystList()
    MapManager::Fetch()->ClearCatalystList();
    return 0;
}
int Hook_MapM_RegisterCatalyst(lua_State *L)
{
    //MapM_RegisterCatalyst(sChapter, sLevel, sChest, sContents)
    int tArgs = lua_gettop(L);
    if(tArgs != 4) return LuaArgError("MapM_RegisterCatalyst");
    MapManager::Fetch()->RegisterCatalyst(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    return 0;
}
