//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers

///========================================== System ==============================================
void LuaManager::ActivateRemapMode()
{
    ///--[Documentation]
    //--Boots remaps mode, allowing the engine to attempt to call a different file based on its path.
    //  This is used for mods that want to override a specific lua file to change how the game works.
    if(mIsModRemappingActive) return;
    mIsModRemappingActive = true;
}
void LuaManager::DeactivateRemapMode()
{
    ///--[Documentation]
    //--Unsets remaps mode, cleaning up all outstanding data.
    if(!mIsModRemappingActive) return;

    //--Flag.
    mIsModRemappingActive = false;

    //--Deallocate fast-access paths.
    for(int i = 0; i < mRemapPathsTotal; i ++)
    {
        free(mRemapPathList[i].mOriginalPath);
        free(mRemapPathList[i].mModifiedPath);
    }
    free(mRemapPathList);
    delete mModRemapBuildList;

    //--NULL everything.
    mRemapPathsTotal = 0;
    mRemapPathList = NULL;
    mModRemapBuildList = NULL;
}

///======================================= Remap Addition =========================================
void LuaManager::BeginRemapAddition()
{
    ///--[Documentation]
    //--Path remaps are compiled and sorted to speed up execution. Adding new remaps must be done
    //  using these functions, as ConcludeRemapAddition() will clean up the memory and sort the list.
    //--If you attempt to add a remap when this is not active, nothing happens.
    if(mModRemapBuildList) return;

    //--This linked-list holds the un-sorted elements.
    mModRemapBuildList = new StarLinkedList(true);

    //--Add all the existing remaps, if any, to the build list.
    for(int i = 0; i < mRemapPathsTotal; i ++)
    {
        mModRemapBuildList->AddElementAsTail(mRemapPathList[i].mOriginalPath, InitializeString(mRemapPathList[i].mModifiedPath), &FreeThis);
    }

    //--Clear the remaps list.
    for(int i = 0; i < mRemapPathsTotal; i ++)
    {
        free(mRemapPathList[i].mOriginalPath);
        free(mRemapPathList[i].mModifiedPath);
    }
    free(mRemapPathList);
    mRemapPathsTotal = 0;
    mRemapPathList = NULL;
}
void LuaManager::AddRemap(const char *pOriginalPath, const char *pModifiedPath)
{
    ///--[Documentation]
    //--Adds a path remap, where the program will take the original path and instead execute the modified
    //  path, allowing a mod to change how the game works.
    //--This will do nothing unless it is called between a call to BeginRemapAddition() and ConcludeRemapAddition().
    if(!pOriginalPath || !pModifiedPath || !mModRemapBuildList) return;

    //--Simplify both paths, if needed.
    const char *tSimpleOriginalPath = SimplifyPath(pOriginalPath);
    const char *tSimpleModifiedPath = SimplifyPath(pModifiedPath);
    if(!tSimpleOriginalPath) tSimpleOriginalPath = pOriginalPath;
    if(!tSimpleModifiedPath) tSimpleModifiedPath = pModifiedPath;

    //--Debug.
    //fprintf(stderr, "Adding remap: %s -> %s\n", tSimpleOriginalPath, tSimpleModifiedPath);

    //--Once simplified, check to see if the original path already exists. If so, overwrite it.
    char *rExistingModifiedPath = (char *)mModRemapBuildList->GetElementByName(tSimpleOriginalPath);
    if(rExistingModifiedPath)
    {
        //--Remove the element from the list.
        mModRemapBuildList->RemoveElementP(rExistingModifiedPath);

        //--If the modified path is "REMOVE" then remove this element and return that path to its original
        //  functionality. Don't re-add it.
        if(!strcasecmp(pModifiedPath, "REMOVE")) return;
    }

    //--Add it.
    mModRemapBuildList->AddElement(tSimpleOriginalPath, InitializeString(tSimpleModifiedPath), &FreeThis);

    //--If needed, deallocate:
    if(tSimpleOriginalPath != pOriginalPath) free((void *)tSimpleOriginalPath);
    if(tSimpleModifiedPath != pModifiedPath) free((void *)tSimpleModifiedPath);
}
void LuaManager::ConcludeRemapAddition()
{
    ///--[Documentation]
    //--Once all remaps are in place, sort them and place them in the finalized list. This also
    //  simplifies the paths where needed.
    if(!mModRemapBuildList) return;

    //--Sort the list.
    mModRemapBuildList->SortList(false);

    //--Allocate.
    mRemapPathsTotal = mModRemapBuildList->GetListSize();
    if(mRemapPathsTotal < 1) return;
    mRemapPathList = (LuaPathRemapPack *)starmemoryalloc(sizeof(LuaPathRemapPack) * mRemapPathsTotal);

    //--Iterate, placing data in the array.
    int i = 0;
    char *rModifiedPath = (char *)mModRemapBuildList->PushIterator();
    while(rModifiedPath)
    {
        //--Get the original path.
        char *rOriginalPath = mModRemapBuildList->GetIteratorName();

        //--Set.
        mRemapPathList[i].mOriginalPath = InitializeString(rOriginalPath);
        mRemapPathList[i].mModifiedPath = InitializeString(rModifiedPath);

        //--Next.
        i ++;
        rModifiedPath = (char *)mModRemapBuildList->AutoIterate();
    }

    //--Deallocate the list.
    delete mModRemapBuildList;
    mModRemapBuildList = NULL;
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
int LuaManager::BinaryGetRemap(const char *pName, int pMin, int pMax)
{
    ///--[Documentation]
    //--Recursive binary search algorithm. Scans the path remaps looking for the original path that
    //  matched the provided path. Paths are assumed to be alphabetized.
    //fprintf(stderr, "Searching for %s\n", pName);

    //--Array is empty
    if(pMax < pMin)
    {
        return -1;
    }

    //--Find the halfway point, cut the set in half
    int tMid = (pMin + pMax) / 2;

    //--Check the given position.
    const char *rCheckName = mRemapPathList[tMid].mOriginalPath;
    //fprintf(stderr, " Slot %i: %s\n", tMid, rCheckName);

    //--Case sensitive:
    int tRet = strcasecmp(pName, rCheckName);

    //--After Midpoint
    if(tRet > 0)
    {
        //fprintf(stderr, " Check after.\n");
        return BinaryGetRemap(pName, tMid+1, pMax);
    }
    //--Before midpoint
    else if(tRet < 0)
    {
        //fprintf(stderr, " Check before.\n");
        return BinaryGetRemap(pName, pMin, tMid-1);
    }
    //--Match
    else
    {
        //fprintf(stderr, " Match.\n");
        return tMid;
    }

    //--Failure.
    return -1;
}
char *LuaManager::SimplifyPath(const char *pPath)
{
    ///--[Documentation]
    //--Given a path, simplifies it by removing instances of ".." and ".", trying to make it the
    //  simplest possible path. Note that not all directory instances like this can actually be
    //  removed if this path is ultimately relative to the game's directory.
    //--Returns the modified path. If the path is already as simple as possible, returns NULL.
    //  The returned string is heap-allocated and must be freed by the caller.
    if(!pPath) return NULL;

    ///--[Initial Scan]
    //--Scan to see if anything needs simplifying. Right now we are looking for "../" and "./"
    bool tStopExecution = true;
    bool tNeedsReplacement = false;
    int tLen = strlen(pPath);
    for(int i = 0; i < tLen; i ++)
    {
        //--If we spot one of the required phrases, execution must continue.
        if(!strncmp(&pPath[i], "./", 2) || !strncmp(&pPath[i], "../", 3))
        {
            tStopExecution = false;
        }
        //--If we spot a backslash, the string needs to be updated.
        else if(pPath[i] == '\\')
        {
            tStopExecution = false;
            tNeedsReplacement = true;
        }
        //--If we spot two slashes, execution must continue. Note that backslashes may be intermixed, so this
        //  check needs to happen twice.
        else if(!strncmp(&pPath[i], "//", 2))
        {
            tStopExecution = false;
        }
    }

    //--If this flag is set, the string is already simplified.
    if(tStopExecution)
    {
        return NULL;
    }

    ///--[Replace Backslashes]
    //--If during scanning we spotted a backslash, we need to replace the string with a forward-slash version.
    const char *tUseString = (char *)pPath;
    if(tNeedsReplacement)
    {
        //--Create the replacement. Modify it.
        char *tReplacementString = InitializeString(pPath);
        for(int i = 0; i < tLen; i ++)
        {
            if(tReplacementString[i] == '\\') tReplacementString[i] = '/';
        }

        //--Mark this as the use string.
        tUseString = tReplacementString;
    }

    //--Debug.
    //fprintf(stderr, "Received path: %s\n", pPath);

    ///--[Duplicate '/' Check]
    //--Multiple '/' in a row is a legal part of the path but will trip up the subdivider. Those need to be
    //  replaced by a single '/'.
    //--Because it's possible the user used backslashes and forward slashes in a mix, we can only call this
    //  once the backslashes have been replaced.
    bool tSimplifySlashes = false;
    int tOrigPathLen = (int)strlen(tUseString);
    for(int i = 0; i < tOrigPathLen-1; i ++)
    {
        if(!strncmp(&tUseString[i], "//", 2))
        {
            tSimplifySlashes = true;
            break;
        }
    }

    //--Slash simplification needs to occur.
    if(tSimplifySlashes)
    {
        //--Create a new string that is the same length as the old one.
        int c = 0;
        char *tSimplifiedString = (char *)starmemoryalloc(sizeof(char) * tOrigPathLen);

        //--Copy over, but don't copy duplicate slashes.
        bool tIsInSlashGroup = false;
        for(int i = 0; i < tOrigPathLen; i ++)
        {
            //--Is this a slash?
            if(tUseString[i] == '/')
            {
                //--Was it previously a slash?
                if(tIsInSlashGroup)
                {
                    //--Skip.
                }
                //--Copy, but mark it.
                else
                {
                    tIsInSlashGroup = true;
                    tSimplifiedString[c] = tUseString[i];
                    c ++;
                }
            }
            //--Otherwise, copy as normal.
            else
            {
                tIsInSlashGroup = false;
                tSimplifiedString[c] = tUseString[i];
                c ++;
            }
        }

        //--Cap.
        tSimplifiedString[c] = '\0';

        //--Save.
        if(tUseString != pPath) free((void *)tUseString);
        tUseString = tSimplifiedString;

        //--Compare.
        //fprintf(stderr, "Original %s\n", tUseString);
        //fprintf(stderr, "Slashes  %s\n", tSimplifiedString);
    }


    ///--[Subivision]
    //--Break the path up by "/" into individual words.
    StarLinkedList *tBrokenUpPath = Subdivide::SubdivideStringToList(tUseString, "/");

    //--Begin iterating across the path.
    char *rPrevWord = NULL;
    char *rWord = (char *)tBrokenUpPath->SetToHeadAndReturn();
    while(rWord)
    {
        //--If the word is a "." and nothing else, it can be safely removed.
        if(!strcasecmp(rWord, "."))
        {
            tBrokenUpPath->RemoveRandomPointerEntry();
            rWord = (char *)tBrokenUpPath->IncrementAndGetRandomPointerEntry();
            continue;
        }
        //--If the word is a ".." and nothing else, it removes the previous entry if that wasn't
        //  also a "..". If there is no previous entry, do nothing.
        else if(!strcasecmp(rWord, ".."))
        {
            //--There is no previous entry. Do nothing.
            if(!rPrevWord)
            {
            }
            //--There is a previous entry, but it's a "..". Do nothing.
            else if(!strcasecmp(rPrevWord, ".."))
            {
            }
            //--Remove both this and the previous entry. Restart iteration from the front.
            else
            {
                tBrokenUpPath->RemoveRandomPointerEntry();
                tBrokenUpPath->RemoveRandomPointerEntry();
                rPrevWord = NULL;
                rWord = (char *)tBrokenUpPath->SetToHeadAndReturn();
                continue;
            }
        }

        //--Next.
        rPrevWord = rWord;
        rWord = (char *)tBrokenUpPath->IncrementAndGetRandomPointerEntry();
    }

    ///--[String Assembly]
    //--Now the linked list represents the simplified path. Place it in a string. Note that the final
    //  null-termination is accounted for in this length, as there is no final '/'.
    int tComputedLen = 0;
    rWord = (char *)tBrokenUpPath->PushIterator();
    while(rWord)
    {
        //--Add length of the word.
        tComputedLen += (int)strlen(rWord);

        //--Add an extra for the following '/'.
        tComputedLen ++;

        //--Next.
        rWord = (char *)tBrokenUpPath->AutoIterate();
    }

    //--Allocate space.
    char *nFinalString = (char *)starmemoryalloc(sizeof(char) * tComputedLen);
    memset(nFinalString, 0, sizeof(char) * tComputedLen);

    //--Print it.
    void *rTailPtr = tBrokenUpPath->GetTail();
    rWord = (char *)tBrokenUpPath->PushIterator();
    while(rWord)
    {
        //--Concatenate the word.
        strcat(nFinalString, rWord);

        //--If this is not the last string, concatenate a '/'.
        if(rWord != rTailPtr) strcat(nFinalString, "/");

        //--Next.
        rWord = (char *)tBrokenUpPath->AutoIterate();
    }

    ///--[Finish Up]
    //--Clean.
    delete tBrokenUpPath;

    //--If the tUseString is not the path, then it was allocated. We need to free it.
    if(tUseString != pPath) free((void *)tUseString);

    //--Debug.
    //fprintf(stderr, " Final path: %s\n", nFinalString);
    //fprintf(stderr, " Length: %i\n", tComputedLen);

    //--Pass back the final string.
    return nFinalString;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LM_SimplifyPath(lua_State *L)
{
    ///--[Argument Listing]
    //LM_SimplifyPath(sPath) (1 String)

    ///--[Argument Check]
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("LM_SimplifyPath");
        lua_pushstring(L, "Null");
        return 1;
    }

    ///--[Execution]
    //--Call.
    char *tSimplifiedPath = LuaManager::SimplifyPath(lua_tostring(L, 1));

    //--If already simplified, NULL is returned. Pass back the original.
    if(!tSimplifiedPath)
    {
        lua_pushstring(L, lua_tostring(L, 1));
        return 1;
    }

    //--Otherwise, pass back the simplified string.
    lua_pushstring(L, tSimplifiedPath);
    return 1;
}
int Hook_LM_SetPathProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //LM_SetPathProperty("Stop Script Alteration") (Static)

    //--[Remaps]
    //LM_SetPathProperty("Activate Remapping")
    //LM_SetPathProperty("Deactivate Remapping")
    //LM_SetPathProperty("Begin Remap Addition")
    //LM_SetPathProperty("Conclude Remap Addition")
    //LM_SetPathProperty("Add Remap", sOrigPath, sModifiedPath)

    //--[Addendums]
    //LM_SetPathProperty("Add Addendum", sUniqueName, sOrigPath, sModifiedPath)
    //LM_SetPathProperty("Add Predendum", sUniqueName, sOrigPath, sModifiedPath)
    //LM_SetPathProperty("Clear Addendums")

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LM_SetPathProperty");

    //--Switching.
    LuaManager *rLuaManager = LuaManager::Fetch();
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[System]
    //--Stops pre-during-post script alteration, that is, adding extra scripts to fire during execution.
    //  If a predendum calls this, the main script won't execute either!
    if(!strcasecmp(rSwitchType, "Stop Script Alteration") && tArgs == 1)
    {
        rLuaManager->SetStopExecution();
    }
    ///--[Remaps]
    //--Begins path remapping mode.
    else if(!strcasecmp(rSwitchType, "Activate Remapping") && tArgs == 1)
    {
        rLuaManager->ActivateRemapMode();
    }
    //--Deactivates path remapping and deallocates variables.
    else if(!strcasecmp(rSwitchType, "Deactivate Remapping") && tArgs == 1)
    {
        rLuaManager->DeactivateRemapMode();
    }
    //--Begins adding new elements to the path remap. Won't affect calls until the conclude is called.
    else if(!strcasecmp(rSwitchType, "Begin Remap Addition") && tArgs == 1)
    {
        rLuaManager->BeginRemapAddition();
    }
    //--Finalized all pending path remaps and sorts them.
    else if(!strcasecmp(rSwitchType, "Conclude Remap Addition") && tArgs == 1)
    {
        rLuaManager->ConcludeRemapAddition();
    }
    //--Adds a new path remap.
    else if(!strcasecmp(rSwitchType, "Add Remap") && tArgs == 3)
    {
        rLuaManager->AddRemap(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Addendums]
    //--Adds a new addendum.
    else if(!strcasecmp(rSwitchType, "Add Addendum") && tArgs == 4)
    {
        rLuaManager->RegisterAddendum(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Adds a new predendum.
    else if(!strcasecmp(rSwitchType, "Add Predendum") && tArgs == 4)
    {
        rLuaManager->RegisterPredendum(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Clears existing addendums.
    else if(!strcasecmp(rSwitchType, "Clear Addendums") && tArgs == 1)
    {
        rLuaManager->ClearAddendums();
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
