///======================================= AdvUICampfire ==========================================
//--Campfire UI. Basically identical to the BaseUI, but has a modified layout since certain actions
//  can only take place on the campfire UI, such as saving and passwords.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIGrid.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#ifndef _ADVUIBASE_BUTTONS_
#define _ADVUIBASE_BUTTONS_

//--Campfire Menu Buttons
#define AM_CAMPFIRE_REST 0
#define AM_CAMPFIRE_CHAT 1
#define AM_CAMPFIRE_SAVE 2
#define AM_CAMPFIRE_COSTUMES 3
#define AM_CAMPFIRE_FORM 4
#define AM_CAMPFIRE_WARP 5
#define AM_CAMPFIRE_RELIVE 6
#define AM_CAMPFIRE_PASSWORD 7
#define AM_CAMPFIRE_TOTAL 8

//--Grid Positioning Values
#define AM_CAMPFIRE_GRID_CENT_X (VIRTUAL_CANVAS_X * 0.50f)
#define AM_CAMPFIRE_GRID_CENT_Y (VIRTUAL_CANVAS_Y * 0.30f)
#define AM_CAMPFIRE_GRID_SPAC_X (VIRTUAL_CANVAS_X * 0.10f)
#define AM_CAMPFIRE_GRID_SPAC_Y (VIRTUAL_CANVAS_X * 0.10f)

#endif

///========================================== Classes =============================================
class AdvUICampfire : virtual public AdvUICommon, virtual public AdvUIGrid
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxAdvCampfireHelpStrings = 10;

    ///--[Variables]
    //--System
    bool mIsBenchMode;
    bool mHasBuiltGrid;
    bool mDontShowHighlight;

    //--Cursor
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Strings
    StarlightString *mShowHelpString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;

        //--Images
        StarBitmap *rOverlay_Footer;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;

        //--Icons
        StarBitmap *rIcons[AM_CAMPFIRE_TOTAL];
    }AdvImages;

    protected:

    public:
    //--System
    AdvUICampfire();
    virtual ~AdvUICampfire();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void SetCursorByBackwardsCode(int pMode);
    virtual void SetCursor(int pCursor);
    virtual void TakeForeground();
    virtual void QuicksetGridEntry(int pX, int pY, int pCode, const char *pName, StarBitmap *pImagePtr);
    void SetBenchMode(bool pFlag);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
