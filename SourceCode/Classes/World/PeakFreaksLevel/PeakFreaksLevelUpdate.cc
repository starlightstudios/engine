//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "PeakFreaksBadge.h"
#include "PeakFreaksBomb.h"
#include "PeakFreaksFlyby.h"
#include "PeakFreaksFragment.h"
#include "PeakFreaksPlayer.h"
#include "PeakFreaksTitle.h"
#include "TileLayer.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DisplayManager.h"
#include "DebugManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///--[Local Definitions]
//#define PEAKFREAKSLEVELUPDATE_DEBUG
#ifdef PEAKFREAKSLEVELUPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Update ==============================================
void PeakFreaksLevel::Update()
{
    ///--[Debug]
    DebugPush(true, "PeakFreaksLevel:Update Begins.\n");

    //--Common timers.
    mGoalFlagTimer ++;

    ///--[Return to Title]
    //--If true, quit back to the title screen.
    if(mReturnToTitle)
    {
        //--Debug.
        DebugPop("PeakFreaksLevel: Returning to title.\n");

        //--Timer.
        mPauseToTitleTimer ++;

        //--Zero tick.
        if(mPauseToTitleTimer == 1)
        {
            AudioManager::Fetch()->FadeMusic(5);
        }

        //--Create a new object and give it to the MapManager. This object becomes unstable.
        if(mPauseToTitleTimer >= PF_CROSSFADE_TICKS)
        {
            //--If restarting, instead create a new level.
            if(mRestartLevel)
            {
                //--Resolve path.
                DataLibrary *rDataLibrary = DataLibrary::Fetch();
                SysVar *rPathVariable = (SysVar *)rDataLibrary->GetEntry("Root/Paths/System/Startup/sPeakFreaksPath");
                if(!rPathVariable || !rPathVariable->mAlpha) return;

                //--Setup.
                char *tPath = NULL;
                tPath = InitializeString("%s%s", rPathVariable->mAlpha, "/Maps/SampleLevel/Constructor.lua");

                //--Execute.
                LuaManager::Fetch()->ExecuteLuaFile(tPath);

                //--This object is now unstable and needs to exit.
                free(tPath);
            }
            //--Otherwise, go back to the menu.
            else
            {
                PeakFreaksTitle *nLevel = new PeakFreaksTitle();
                nLevel->Construct();
                MapManager::Fetch()->ReceiveLevel(nLevel);
            }
        }
        return;
    }

    ///--[Win/Lose]
    //--Winning the match. Can be triggered by a debug key if needed.
    if(mReturnToTitleVictory)
    {
        //--Store flags.
        PeakFreaksLevel::xPlayerVictory = true;

        //--Flag victory if needed.
        PFTitle_Highscore *tBestTimeData = PeakFreaksTitle::ReadHighscores();
        if(tBestTimeData && mRival)
        {
            if(xDifficulty == PTTITLE_DIFFICULTY_SIMPLE)
            {
                tBestTimeData->mWonEasy = true;
            }
            else if(xDifficulty == PTTITLE_DIFFICULTY_CHALLENGING)
            {
                tBestTimeData->mWonMedium = true;
            }
            else if(xDifficulty == PTTITLE_DIFFICULTY_IMPOSSIBLE)
            {
                tBestTimeData->mWonHard = true;
            }

            //--Write.
            PeakFreaksTitle::WriteHighscores(tBestTimeData);
            free(tBestTimeData);
        }


        //--Music.
        AudioManager::Fetch()->FadeMusic(5);
        DebugPop("PeakFreaksLevel: Returning to title.\n");

        //--Create a new object and give it to the MapManager. This object becomes unstable.
        xMandatedSeed = -1;
        PeakFreaksTitle *nLevel = new PeakFreaksTitle();
        nLevel->ActivateFinale(); //Must be called before Construct()!
        if(mShowPersonalBest) nLevel->ShowPersonalBest();
        nLevel->Construct();
        MapManager::Fetch()->ReceiveLevel(nLevel);
        return;
    }
    if(mReturnToTitleDefeat || ControlManager::Fetch()->IsFirstPress("PF Debug Lose"))
    {
        //--Store flags.
        PeakFreaksLevel::xPlayerVictory = false;

        //--Music.
        AudioManager::Fetch()->FadeMusic(5);
        DebugPop("PeakFreaksLevel: Returning to title.\n");

        //--Create a new object and give it to the MapManager. This object becomes unstable.
        xMandatedSeed = -1;
        PeakFreaksTitle *nLevel = new PeakFreaksTitle();
        nLevel->ActivateFinale(); //Must be called before Construct()!
        if(mShowPersonalBest) nLevel->ShowPersonalBest();
        nLevel->Construct();
        MapManager::Fetch()->ReceiveLevel(nLevel);
        return;
    }

    //--Debug key:
    if(ControlManager::Fetch()->IsFirstPress("PF Debug Win"))
    {
        //StartVictory(mPlayer);
        if(mPlayer) mPlayer->Injure();
    }

    ///--[Victory Timer]
    //--Someone has won the match. Run this timer and stop other updates.
    if(rVictoriousEntity)
    {
        ///--[Fireworks Handling]
        //--Update timers for all fireworks. None will spawn if this isn't the player's victory.
        PFTitle_Firework *rFirework = (PFTitle_Firework *)mFireworks->SetToHeadAndReturn();
        while(rFirework)
        {
            //--Timer.
            rFirework->mAnimTimer ++;

            //--Clamp.
            int tFrame = (int)(rFirework->mAnimTimer / PFTITLE_FIREWORK_TPF);
            if(tFrame >= PFTITLE_FIREWORKS_FRAMES) mFireworks->RemoveRandomPointerEntry();

            //--Next.
            rFirework = (PFTitle_Firework *)mFireworks->IncrementAndGetRandomPointerEntry();
        }

        //--If the victorious entity is the player, go to the title screen.
        if(rVictoriousEntity == mPlayer)
        {
            //--Mandate track position.
            mPlayerTrackProgress = 1.0f;

            //--Run.
            mVictoryTimer --;

            //--If the fireworks timer reaches zero, spawn a new firework.
            if(mFireworksTimer > 0)
            {
                //--Timer.
                mFireworksTimer --;

                //--Spawn a new one.
                if(mFireworksTimer < 1)
                {
                    //--Reset flags.
                    mFireworksTimer = (rand() % (PFTITLE_FIREWORK_SPAWN_TICKS_HI - PFTITLE_FIREWORK_SPAWN_TICKS_LO)) + PFTITLE_FIREWORK_SPAWN_TICKS_LO;

                    //--New firework.
                    PFTitle_Firework *nFirework = (PFTitle_Firework *)starmemoryalloc(sizeof(PFTitle_Firework));
                    nFirework->Initialize();

                    //--Pick a scattered X location. The entire screen is fair game. -80 to center the firework frame.
                    nFirework->mXPos = (rand() % (int)VIRTUAL_CANVAS_X) - 80;

                    //--Y positions are mostly towards the bottom of the canvas.
                    nFirework->mYPos = (rand() % 575) + 150;

                    //--Random color.
                    nFirework->mColorIndex = rand() % PFTITLE_FIREWORKS_COLS;

                    //--Register.
                    mFireworks->AddElement("X", nFirework, &FreeThis);
                }
            }

            //--On this tick, play the victory music.
            if(mVictoryTimer == PF_VICTORY_TICKS - 60)
            {
                AudioManager::Fetch()->PlaySound("PF Entity FinishLine");
            }

            //--Next tick, go back to the title screen.
            if(mVictoryTimer < 1)
            {
                if(mPlayerReachedVictoryFirst)
                {
                    mReturnToTitleVictory = true;
                }
                else
                {
                    mReturnToTitleDefeat = true;
                }

                //--Special: If this flag is set, just return to the title screen without showing the victory screen.
                if(mSkipResultsScreen)
                {
                    mReturnToTitle = true;
                    mReturnToTitleVictory = false;
                    mReturnToTitleDefeat = false;
                }
            }

            //--Debug.
            DebugPop("PeakFreaksLevel: Victory sequence.\n");
            return;
        }
        //--Rival got there first. Player still has to finish the stage.
        else
        {
            mRivalTrackProgress = 1.0f;
        }
    }

    ///--[Dialogue]
    //--Run the dialogue's internal update.
    UpdateDialogue();

    ///--[Fragment Handling]
    //--UI Fragments.
    PeakFreaksFragment *rUIFragment = (PeakFreaksFragment *)mFragmentList->SetToHeadAndReturn();
    while(rUIFragment)
    {
        //--Run update.
        rUIFragment->Update();

        //--If expired, delete.
        if(rUIFragment->IsExpired()) mFragmentList->RemoveRandomPointerEntry();

        //--Next.
        rUIFragment = (PeakFreaksFragment *)mFragmentList->IncrementAndGetRandomPointerEntry();
    }

    //--World Fragments.
    PeakFreaksFragment *rWorldFragment = (PeakFreaksFragment *)mWorldFragmentList->SetToHeadAndReturn();
    while(rWorldFragment)
    {
        //--Run update.
        rWorldFragment->Update();

        //--If expired, delete.
        if(rWorldFragment->IsExpired()) mWorldFragmentList->RemoveRandomPointerEntry();

        //--Next.
        rWorldFragment = (PeakFreaksFragment *)mWorldFragmentList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Cloud Handling]
    //--When the timer hits its cap, reset it and spawn a cloud.
    mCloudSpawnTimer ++;
    if(mCloudSpawnTimer >= PFTITLE_CLOUD_TICKS)
    {
        //--Reset.
        mCloudSpawnTimer = 0;

        //--Spawn.
        SpawnCloudEdge();
    }

    //--Move clouds.
    PFTitle_Cloud *rCloud = (PFTitle_Cloud *)mCloudList->SetToHeadAndReturn();
    while(rCloud)
    {
        //--Move.
        rCloud->mXPos = rCloud->mXPos + rCloud->mXSpeed;

        //--Upon hitting the right side of the screen, despawn.
        if(rCloud->mXPos >= VIRTUAL_CANVAS_X)
        {
            mCloudList->RemoveRandomPointerEntry();
        }

        //--Next.
        rCloud = (PFTitle_Cloud *)mCloudList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Opening Sequence]
    if(mIsOpening)
    {
        UpdateOpening();
        return;
    }

    ///--[Pause Menu]
    //--If the game is paused, this will handle timers and input. Otherwise it just handles timers.
    if(mIsPaused)
    {
        UpdatePause();
        return;
    }
    else
    {
        UpdatePause();
    }

    ///--[Timers]
    //--Match timer.
    xTimeElapsed ++;
    mCursorTimer ++;

    //--Selection offset timer. This controls the bouncing of puzzle pieces that match the selected piece.
    mSelectionOffsetTimer ++;
    if(mSelectionOffsetTimer > PF_SELECTION_OFFSET_REPEAT) mSelectionOffsetTimer = 0;

    //--Coin timer always runs.
    mCoinTimer ++;

    //--Ice Beam cooldown.
    if(mIceBeamCountdown > -1)
    {
        //--Timer.
        mIceBeamCountdown --;

        //--On this tick, spawn the ice beam world particles.
        if(mIceBeamCountdown == PFLEVEL_ICEBEAM_ACTIVATION_COOLDOWN - 5)
        {
            SpawnIceBeamParticles();
            AudioManager::Fetch()->PlaySound("PF Powerup IceBeam");
        }

        //--On tick zero, activate the ice beam.
        if(mIceBeamCountdown == 0)
        {
            mIceBeamTimer = PFLEVEL_ICEBEAM_DURATION;
        }
    }

    //--Update all puzzle pieces.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            mPuzzleGrid[x][y]->Update();
        }
    }

    //--Hazards. Don't update if Ice Beam is active.
    if(mIceBeamTimer < 1)
    {
        PF_Slider *rSlider = (PF_Slider *)mSliderList->PushIterator();
        while(rSlider)
        {
            rSlider->Update();
            rSlider = (PF_Slider *)mSliderList->AutoIterate();
        }
        PF_Spinner *rSpinner = (PF_Spinner *)mSpinnerList->PushIterator();
        while(rSpinner)
        {
            rSpinner->Update();
            rSpinner = (PF_Spinner *)mSpinnerList->AutoIterate();
        }
        PF_Waterfall *rWaterfall = (PF_Waterfall *)mWaterfallList->PushIterator();
        while(rWaterfall)
        {
            rWaterfall->Update();
            rWaterfall = (PF_Waterfall *)mWaterfallList->AutoIterate();
        }
        PF_BombSpike *rBombSpike = (PF_BombSpike *)mBombSpikeList->SetToHeadAndReturn();
        while(rBombSpike)
        {
            rBombSpike->Update();
            if(rBombSpike->mSelfDestruct)
            {
                mBombSpikeList->RemoveRandomPointerEntry();
            }
            rBombSpike = (PF_BombSpike *)mBombSpikeList->IncrementAndGetRandomPointerEntry();
        }
    }
    //--Decrement the ice beam timer.
    else
    {
        mIceBeamTimer --;
    }

    //--Powerup Projectiles.
    PeakFreaksBomb *rBomb = (PeakFreaksBomb *)mBombList->SetToHeadAndReturn();
    while(rBomb)
    {
        rBomb->Update();
        if(rBomb->IsComplete())
        {
            mBombList->RemoveRandomPointerEntry();
        }
        rBomb = (PeakFreaksBomb *)mBombList->IncrementAndGetRandomPointerEntry();
    }

    //--Flybys.
    PeakFreaksFlyby *rFlyby = (PeakFreaksFlyby *)mFlybyList->SetToHeadAndReturn();
    while(rFlyby)
    {
        rFlyby->Update();
        if(rFlyby->IsComplete())
        {
            mFlybyList->RemoveRandomPointerEntry();
        }
        rFlyby = (PeakFreaksFlyby *)mFlybyList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Canyon Bar Particles]
    //--Player check.
    if(mPlayer && mPlayer->GetCanyonBarTimer() > 0 && mPlayer->GetCanyonBarTimer() % PFCB_SPAWN_TIME == 1)
    {
        PF_Particle *nParticle = (PF_Particle *)starmemoryalloc(sizeof(PF_Particle));
        nParticle->Initialize();
        nParticle->mXPos = (rand() % PFCB_PL_WID) + PFCB_PL_LFT;
        nParticle->mYPos = (rand() % PFCB_PL_HEI) + PFCB_PL_TOP;
        nParticle->mXSpeed = ((rand() % PFCB_XSPEED_RANGE) /  10.0f) + PFCB_XSPEED_LO;
        nParticle->mYSpeed = ((rand() % PFCB_YSPEED_RANGE) / -10.0f) + PFCB_YSPEED_LO;//Y Speed is negative so it goes up.
        if(rand() % 2 == 0) nParticle->mXSpeed = nParticle->mXSpeed * -1.0f;
        mCanyonBarParticles->AddElement("X", nParticle, &FreeThis);

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Powerup CanyonFire");
    }

    //--Rival check.
    if(mRival && mRival->GetCanyonBarTimer() > 0 && mRival->GetCanyonBarTimer() % PFCB_SPAWN_TIME == 1)
    {
        PF_Particle *nParticle = (PF_Particle *)starmemoryalloc(sizeof(PF_Particle));
        nParticle->Initialize();
        nParticle->mXPos = (rand() % PFCB_RV_WID) + PFCB_RV_LFT;
        nParticle->mYPos = (rand() % PFCB_RV_HEI) + PFCB_RV_TOP;
        nParticle->mXSpeed = ((rand() % PFCB_XSPEED_RANGE) /  10.0f) + PFCB_XSPEED_LO;
        nParticle->mYSpeed = ((rand() % PFCB_YSPEED_RANGE) / -10.0f) + PFCB_YSPEED_LO;//Y Speed is negative so it goes up.
        if(rand() % 2 == 0) nParticle->mXSpeed = nParticle->mXSpeed * -1.0f;
        mCanyonBarParticles->AddElement("X", nParticle, &FreeThis);

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Powerup CanyonFire");
    }

    //--Update.
    PF_Particle *rParticle = (PF_Particle *)mCanyonBarParticles->SetToHeadAndReturn();
    while(rParticle)
    {
        //--Update position by speed.
        rParticle->mXPos = rParticle->mXPos + rParticle->mXSpeed;
        rParticle->mYPos = rParticle->mYPos + rParticle->mYSpeed;

        //--Update speed by gravity.
        rParticle->mYSpeed = rParticle->mYSpeed + PFCB_GRAVITY;

        //--Remove particles that fell off the screen.
        if(rParticle->mYPos > VIRTUAL_CANVAS_Y + 300.0f) mCanyonBarParticles->RemoveRandomPointerEntry();

        //--Next.
        rParticle = (PF_Particle *)mCanyonBarParticles->IncrementAndGetRandomPointerEntry();
    }

    ///--[Ice Beam Particles]
    //--These particles are spawned elsewhere and fall under gravity.
    float cYCap = 1000.0f;
    if(mCollisionLayersTotal > 0) cYCap = (mCollisionLayers[0].mCollisionSizeY * TileLayer::cxSizePerTile) + 200.0f;

    //--Update.
    rParticle = (PF_Particle *)mIceBeamParticles->SetToHeadAndReturn();
    while(rParticle)
    {
        //--Update position by speed.
        rParticle->mXPos = rParticle->mXPos + rParticle->mXSpeed;
        rParticle->mYPos = rParticle->mYPos + rParticle->mYSpeed;

        //--Update speed by gravity.
        rParticle->mYSpeed = rParticle->mYSpeed + PFCB_ICE_GRAVITY;

        //--Remove particles that fell off the screen.
        if(rParticle->mYPos > cYCap) mIceBeamParticles->RemoveRandomPointerEntry();

        //--Next.
        rParticle = (PF_Particle *)mIceBeamParticles->IncrementAndGetRandomPointerEntry();
    }

    ///--[Speed Boost Particles]
    //--The X/Y speed are the starting position of the particle, set when it is created.
    rParticle = (PF_Particle *)mSpeedBoostParticles->SetToHeadAndReturn();
    while(rParticle)
    {
        //--Compute.
        rParticle->mTimer ++;

        //--Do nothing if timer is negative.
        if(rParticle->mTimer < 0)
        {

        }
        //--Ending case.
        else if(rParticle->mTimer >= 30)
        {
            mSpeedBoostParticles->RemoveRandomPointerEntry();
        }
        //--Otherwise, update position.
        else
        {
            float tPct = EasingFunction::QuadraticOut(rParticle->mTimer, 30);
            rParticle->mXPos = rParticle->mXSpeed + ((114.0f - rParticle->mXSpeed) * tPct);
            rParticle->mYPos = rParticle->mYSpeed + ((429.0f - rParticle->mYSpeed) * tPct);
        }

        //--Next.
        rParticle = (PF_Particle *)mSpeedBoostParticles->IncrementAndGetRandomPointerEntry();
    }

    ///--[Pause Menu]
    //--Regardless of AI control and whatnot, check if the player is pausing the game.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("PF Cancel"))
    {
        AudioManager::Fetch()->PlaySound("PF Cancel");
        mIsPaused = true;

        //--Halve music volume.
        AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVolumeNormal * 0.50f);
        return;
    }

    //--Debug:
    if(rControlManager->IsFirstPress("PF Kill Rival"))
    {
        if(mRival) mRival->Injure();
    }

    ///--[Player Update]
    if(mPlayer) mPlayer->Update();
    if(mRival)  mRival->Update();

    ///--[GUI Badges]
    PeakFreaksBadge *rBadge = (PeakFreaksBadge *)mBadgeList->SetToHeadAndReturn();
    while(rBadge)
    {
        rBadge->Update();
        if(rBadge->IsComplete())
        {
            mBadgeList->RemoveRandomPointerEntry();
        }
        rBadge = (PeakFreaksBadge *)mBadgeList->IncrementAndGetRandomPointerEntry();
    }

    ///--[AI Handling]
    //--Player is AI controlled. Handle that.
    if(mPlayer && mPlayer->IsAIControlled())
    {
        mPlayer->HandleAIControlUpdate();
    }
    else
    {
        HandleControlUpdate(mPlayer);
    }

    //--Rival is AI controlled, handle that.
    if(mRival && mRival->IsAIControlled())
    {
        mRival->HandleAIControlUpdate();
    }

    ///--[Trigger Checking]
    //--After entities have moved, check if any of the triggers are being tripped by the player.
    if(mTriggerScript)
    {
        //--Fast-access player position.
        TwoDimensionReal tPlayerDim;
        tPlayerDim.mLft = (mPlayer->GetWorldX() * 16.0f) + 1.0f;
        tPlayerDim.mTop = (mPlayer->GetWorldY() * 16.0f) + 1.0f;
        tPlayerDim.mRgt = tPlayerDim.mLft + 16.0f - 2.0f;
        tPlayerDim.mBot = tPlayerDim.mTop + 16.0f - 2.0f;

        //--Check triggers.
        ZonePack *rTriggerPack = (ZonePack *)mTriggerZones->PushIterator();
        while(rTriggerPack)
        {
            //--Hit.
            if(IsCollision2DReal(rTriggerPack->mDim, tPlayerDim))
            {
                LuaManager::Fetch()->ExecuteLuaFile(mTriggerScript, 1, "S", mTriggerZones->GetIteratorName());
            }

            //--Next.
            rTriggerPack = (ZonePack *)mTriggerZones->AutoIterate();
        }
    }

    ///--[Debug]
    DebugPop("PeakFreaksLevel:Update ends, ended normally.\n");
}
void PeakFreaksLevel::HandleControlUpdate(PeakFreaksPlayer *pPlayer)
{
    ///--[Documentation]
    //--When a player is not AI controlled, handles their puzzle grid update.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(!pPlayer) return;

    //--If the tutorial is open, don't accept player input.
    if(IsDialogueBlockingUpdate()) return;

    ///--[Debug: Direct Movement]
    //--Direct Movement.
    if(false)
    {
        //--Constants.
        float cSpeed = 0.01f;

        if(rControlManager->IsDown("Up"))
        {
            pPlayer->QueueNextSpeeds(0.0f, -cSpeed);
        }
        else if(rControlManager->IsDown("Right"))
        {
            pPlayer->QueueNextSpeeds(cSpeed, 0.0f);
        }
        else if(rControlManager->IsDown("Down"))
        {
            pPlayer->QueueNextSpeeds(0.0f, cSpeed);
        }
        else if(rControlManager->IsDown("Left"))
        {
            pPlayer->QueueNextSpeeds(-cSpeed, 0.0f);
        }
    }
    ///--[Puzzle Grid Movement]
    //--Puzzle movement.
    else
    {
        if(rControlManager->IsFirstPress("PF Up"))
        {
            mSelectionOffsetTimer = 0;
            MovePuzzleGridDn();
            AudioManager::Fetch()->PlaySound("PF Puzzle TileSelect");
        }
        if(rControlManager->IsFirstPress("PF Right"))
        {
            mSelectionOffsetTimer = 0;
            MovePuzzleGridLf();
            AudioManager::Fetch()->PlaySound("PF Puzzle TileSelect");
        }
        if(rControlManager->IsFirstPress("PF Down"))
        {
            mSelectionOffsetTimer = 0;
            MovePuzzleGridUp();
            AudioManager::Fetch()->PlaySound("PF Puzzle TileSelect");
        }
        if(rControlManager->IsFirstPress("PF Left"))
        {
            mSelectionOffsetTimer = 0;
            MovePuzzleGridRt();
            AudioManager::Fetch()->PlaySound("PF Puzzle TileSelect");
        }
    }

    ///--[Crushing]
    //--Calls a different algorithm to "Crush" tiles. This overrides them or powers them up,
    //  depending on how they combine.
    if(rControlManager->IsFirstPress("PF Crush"))
    {
        AutoCrush();
        mSelectionOffsetTimer = 0;
    }

    ///--[Activation]
    //--Takes whatever tile is in the center slot on the board and uses it to direct player movement.
    if(rControlManager->IsFirstPress("PF Activate"))
    {
        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Puzzle TileConfirm");

        //--Get the central tile.
        mSelectionOffsetTimer = 0;
        int tCentralCode = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPuzzleType();
        int tCodeCount = 1;
        int tPowerBonus = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPower();

        //--Set the tile to PEAKFREAK_PUZZLE_TYPE_NONE, it will get replaced later.
        int tPuzzleType = rand() % PEAKFREAK_PUZZLE_TYPE_MAX;
        SpawnPuzzleFragments(PLL_PUZZLE_MID_X, PLL_PUZZLE_MID_Y);
        mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->SetPuzzleType(PEAKFREAK_PUZZLE_TYPE_NONE);
        mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->SetPower(0);

        //--Tutorial tag. If the tutorial is active, flag which direction we moved.
        if(mDirectionMoveTutorialActive)
        {
            //--Flags.
            if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_UP) mDirectionTutorialUp = true;
            if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_RT) mDirectionTutorialRt = true;
            if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_DN) mDirectionTutorialDn = true;
            if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_LF) mDirectionTutorialLf = true;

            //--If all four are on, trip the E tutorial.
            if(mDirectionTutorialUp && mDirectionTutorialRt && mDirectionTutorialDn && mDirectionTutorialLf)
            {
                mDirectionMoveTutorialActive = false;
                ActivateTutorialE();
            }
        }

        //--Power jump tutorial. If active, and power is above the base, increment.
        if(mPowerJumpTutorialActive)
        {
            int tPlayerSpeedLevel = mPlayer->GetSpeedBarLevel();
            if(tPlayerSpeedLevel >= 1)
            {
                mPowerJumpCount ++;
                if(mPowerJumpCount >= 4)
                {
                    mPowerJumpTutorialActive = false;
                    ActivateTutorialS();
                }
            }
        }

        ///--[Iterate Left]
        for(int x = PLL_PUZZLE_MID_X-1; x >= 0; x --)
        {
            if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == tCentralCode)
            {
                tCodeCount ++;
                tPowerBonus += mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPower();
                SpawnPuzzleFragments(x, PLL_PUZZLE_MID_Y);
                mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->SetPuzzleType(PEAKFREAK_PUZZLE_TYPE_NONE);
                mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->SetPower(0);
            }
            else
            {
                break;
            }
        }

        ///--[Iterate Right]
        for(int x = PLL_PUZZLE_MID_X+1; x < PLL_PUZZLE_SIZE_X; x ++)
        {
            if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == tCentralCode)
            {
                tCodeCount ++;
                tPowerBonus += mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPower();
                SpawnPuzzleFragments(x, PLL_PUZZLE_MID_Y);
                mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->SetPuzzleType(PEAKFREAK_PUZZLE_TYPE_NONE);
                mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->SetPower(0);
            }
            else
            {
                break;
            }
        }

        ///--[Iterate Up]
        for(int y = PLL_PUZZLE_MID_Y-1; y >= 0; y --)
        {
            if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == tCentralCode)
            {
                tCodeCount ++;
                tPowerBonus += mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPower();
                SpawnPuzzleFragments(PLL_PUZZLE_MID_X, y);
                mPuzzleGrid[PLL_PUZZLE_MID_X][y]->SetPuzzleType(PEAKFREAK_PUZZLE_TYPE_NONE);
                mPuzzleGrid[PLL_PUZZLE_MID_X][y]->SetPower(0);
            }
            else
            {
                break;
            }
        }

        ///--[Iterate Down]
        for(int y = PLL_PUZZLE_MID_Y+1; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == tCentralCode)
            {
                tCodeCount ++;
                tPowerBonus += mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPower();
                SpawnPuzzleFragments(PLL_PUZZLE_MID_X, y);
                mPuzzleGrid[PLL_PUZZLE_MID_X][y]->SetPuzzleType(PEAKFREAK_PUZZLE_TYPE_NONE);
                mPuzzleGrid[PLL_PUZZLE_MID_X][y]->SetPower(0);
            }
            else
            {
                break;
            }
        }

        //--Total.
        //fprintf(stderr, "Total count: %i\n", tCodeCount);
        //fprintf(stderr, "Power bonus: %i\n", tPowerBonus);

        //--If the power bonus is over 0, immediately fill the speed bar.
        if(tPowerBonus > 0)
        {
            mPlayer->SetSpeedBar(4);
        }

        //--Resolve speed.
        int tSpeedSlot = tPowerBonus+2;
        if(tSpeedSlot < 0) tSpeedSlot = 0;
        if(tSpeedSlot >= PLL_PUZZLE_SPEEDS) tSpeedSlot = PLL_PUZZLE_SPEEDS - 1;
        float tUseSpeed = mSpeeds[tSpeedSlot];

        //--Tutorial tag. If the tutorial is active, increment when the power is 3 or higher.
        if(mBigMoveTutorialActive)
        {
            if(tCodeCount >= 3) mBigMoveCount ++;
            if(mBigMoveCount >= 4)
            {
                mBigMoveTutorialActive = false;
                ActivateTutorialI();
            }
        }

        //--Resolve number of moves.
        int tCurrentSpeed = pPlayer->GetSpeedBarLevel();
        int tActualSpeed = pPlayer->GetActualSpeedBarLevel();
        int tMoves = tCodeCount;
        //pPlayer->SetMovesRemaining(tMoves);
        //pPlayer->SetSpeedBar(tActualSpeed - 1);

        //--Cap the moves at 5.
        if(tMoves > 5) tMoves = 5;

        //--Set the direction.
        if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_DN)
        {
            pPlayer->QueueJump(0, tMoves);
            //pPlayer->QueueNextSpeeds(0.0f, tUseSpeed);
        }
        else if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_UP)
        {
            pPlayer->QueueJump(0, -tMoves);
            //pPlayer->QueueNextSpeeds(0.0f, tUseSpeed * -1.0f);
        }
        if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_LF)
        {
            pPlayer->QueueJump(-tMoves, 0);
            //pPlayer->QueueNextSpeeds(tUseSpeed * -1.0f, 0.0f);
        }
        else if(tCentralCode == PEAKFREAK_PUZZLE_TYPE_ARROW_RT)
        {
            pPlayer->QueueJump(tMoves, 0);
            //pPlayer->QueueNextSpeeds(tUseSpeed, 0.0f);
        }

        ///--[Grid Handling]
        //--Replace used blocks on the grid.
        RebalanceGrid();
    }
}
