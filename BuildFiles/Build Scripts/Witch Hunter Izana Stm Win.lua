-- |[ ============================= Witch Hunter Izana Build, Steam ============================ ]|
--Operating System: Windows
--Library Type: Allegro/SDL
--Distribution: Steam
local sGameName = "Witch Hunter Izana"
local sBuildName = "Windows, Steam, Full"

--Check existence.
if(BP_Exists(sGameName, sBuildName) == true) then return end

--Create.
BP_Create(sGameName, sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../../Uploaders/WHISteam/WitchHunterIzana/")

-- |[ ========================================= Common ========================================= ]|
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

-- |[Basics]|
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

-- |[Executables]|
--Executable files.
BP_SetProperty("Register Command", "Copy File",   "Projects/Pandemonium/PandemoniumSteamAL.exe",  "WitchHunterIzanaSteamAL.exe")
BP_SetProperty("Register Command", "Copy File",   "Projects/Pandemonium/PandemoniumSteamSDL.exe", "WitchHunterIzanaSteamSDL.exe")
BP_SetProperty("Register Command", "Change Icon", "WitchHunterIzanaSteamAL.exe",                  "BuildFiles/IconWHI.ico")
BP_SetProperty("Register Command", "Change Icon", "WitchHunterIzanaSteamSDL.exe",                 "BuildFiles/IconWHI.ico")

--DLL files.
BP_SetProperty("Register Command", "Copy File", "bass.dll",    "bass.dll")
BP_SetProperty("Register Command", "Copy File", "bass_fx.dll", "bass_fx.dll")
BP_SetProperty("Register Command", "Copy File", "lua54.dll",   "lua54.dll")

-- |[Bootstraps and Counters]|
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

-- |[Steam Files]|
--The API's dll file, and the program's appid file, need to be copied over.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/steam_api.dll",       "steam_api.dll")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/whi_steam_appid.txt", "steam_appid.txt")

-- |[ =================================== Base Instructions ==================================== ]|
-- |[Text Files]|
--Relevant changelogs.
BP_SetProperty("Register Command", "Copy File", "Changelog (WHI).txt", "Changelog.txt")

--Credits files.
BP_SetProperty("Register Command", "Copy File", "Credits WHI.txt", "Credits.txt")

--Readme.
BP_SetProperty("Register Command", "Copy File", "Readme WHI.txt", "Readme.txt")

-- |[Configuration Files]|
--Engine and Adventure configurations.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Adventure.lua", "Config_Adventure.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",    "Config_Engine.lua")

-- |[ =================================== Engine Subfolders ==================================== ]|
-- |[Data Directory]|
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

-- |[Saves Directory]|
--Copy the controls files.
BP_SetProperty("Register Command", "Create Directory", "Saves")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

--Copy the autosaves directory and the notifier on how to use autosaves.
BP_SetProperty("Register Command", "Create Directory", "Saves/Autosave")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/How to Recover.txt",  "Saves/Autosave/How to Recover.txt")

-- |[Shaders Directory]|
--Copy the Windows/Linux shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/WinLin", "Shaders/WinLin")

-- |[Font Precompiles]|
--Remove these from the folder, they are meant for developer use.
BP_SetProperty("Register Command", "Delete Folder", "Data/FontPrebuild/")

-- |[Overrides]|
--Changes to make sure the game runs standalone.
BP_SetProperty("Register Command", "Delete File", "Data/Scripts/MainMenu/129 Launch Monoceros.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/129 Launch Monoceros.lua", "Data/Scripts/MainMenu/129 Launch Monoceros.lua")

BP_SetProperty("Register Command", "Delete File", "Data/Scripts/100 Startup.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/WHI Startup File.lua", "Data/Scripts/100 Startup.lua")

-- |[Delete Script Object Builder]|
BP_SetProperty("Register Command", "Delete Folder", "Data/Object Prototypes/")

-- |[ ==================================== Game Subfolders ===================================== ]|
-- |[Adventure Directories]|
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../adventure/Games/AdventureMode", "Games/AdventureMode")

-- |[Monoceros Directories]|
BP_SetProperty("Register Command", "Copy Folder", "../projectmonoceros/Chapter C", "Games/AdventureMode/Chapter C")

-- |[Overrides]|
--Taken from the special files, used to make Monoceros more standalone.
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/System/101 Graphics Portraits.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/101 Graphics Portraits.lua", "Games/AdventureMode/System/101 Graphics Portraits.lua")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Maps/Nowhere/Constructor.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Nowhere Constructor.lua",    "Games/AdventureMode/Maps/Nowhere/Constructor.lua")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/ZLaunch.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/WHI Launch File.lua",        "Games/AdventureMode/ZLaunch.lua")

-- |[ ====================================== Mod Cleanup ======================================= ]|
--Delete any mods that are in the assembly folders, the game is not distributed with mods by default.
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Mods/")

--Create a blank folder and put Mod Info.txt in it. This is a blank file that indicates this is a folder users can use for mods.
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Chapter C/Mods")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Mod Info.txt", "Games/AdventureMode/Chapter C/Mods/Mod Info.txt")

--If the engine folder had the file Mod Load Order.txt in it, delete it. The player needs to manually
-- enable mods after installation which generates the load order file.
BP_SetProperty("Register Command", "Delete File", "Mod Load Order.txt")

-- |[ ===================================== File Deletion ====================================== ]|
--Delete files not used for Monoceros. This keeps the file size down.

-- |[Audio Files]|
--Clear folders.
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Audio/Music/")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Audio/Pending/")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Audio/PuzzleFight/")

--Music Directory.
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/Music")
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/Music/Ambient Tracks")
BP_SetProperty("Register Command", "Copy File", "../adventure/Games/AdventureMode/Audio/Music/Ambient Tracks/Cave Quiet.ogg",     "Games/AdventureMode/Audio/Music/Ambient Tracks/Cave Quiet.ogg")
BP_SetProperty("Register Command", "Copy File", "../adventure/Games/AdventureMode/Audio/Music/Ambient Tracks/Windy Mountain.ogg", "Games/AdventureMode/Audio/Music/Ambient Tracks/Windy Mountain.ogg")
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/Music/Battle Themes")
BP_SetProperty("Register Command", "Copy File", "../adventure/Games/AdventureMode/Audio/Music/Battle Themes/CombatVictory.ogg", "Games/AdventureMode/Audio/Music/Battle Themes/CombatVictory.ogg")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Audio Routing.lua", "Games/AdventureMode/Audio/Music/ZRouting.lua")

--Puzzlefight Directory.
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/PuzzleFight")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Puzzlefight Audio Routing.lua", "Games/AdventureMode/Audio/PuzzleFight/ZRouting.lua")

-- |[Chapter Data]|
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter 0")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter 1")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter 2")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter 5")

-- |[Datafiles]|
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CassandraTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH0Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH1Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH2Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH5Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_ChristineTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_GalaDress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_MeiRune.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_MeiTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CassandraTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH0Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH1Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH2Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH5Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_ChristineTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_GalaDress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_MeiRune.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_MeiTF.slf")
    
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_56.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Aquillia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Cassandra.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Christine.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Empress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Florentina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Izuna.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_JX101.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Marriedraunes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Mei.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Mia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Miho.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Miso.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Nina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Odar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_PDU.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Polaris.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sammy.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sanya.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Septima.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sharelock.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sophie.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_SX399.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Tiffany.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Yukina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Zeke.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH0Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH0Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH1Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH1Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH2Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH2Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH5Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_CH5Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_56.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Aquillia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Cassandra.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Christine.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Empress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Florentina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Izuna.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_JX101.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Marriedraunes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Mei.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Mia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Miho.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Miso.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Nina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Odar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_PDU.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Polaris.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sammy.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sanya.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Septima.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sharelock.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sophie.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_SX399.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Tiffany.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Yukina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Zeke.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH0Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH0Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH1Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH1Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH2Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH2Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH5Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH5Emote.slf")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PuzzleBattle.slf")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapAnimations.slf")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Ch1_Mausoleum.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Ch1_Trannadar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Ch1_TrannadarWest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Biolabs.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_CRTNoise.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_CryoLower.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_CryoMain.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Equinox.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_LRTEast.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_LRTWest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_MtSarulente.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_NixNedar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Northwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Regulus.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_System.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Trannadar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Westwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Westwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Ch1_Mausoleum.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Ch1_Trannadar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Ch1_TrannadarWest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Biolabs.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_CRTNoise.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_CryoLower.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_CryoMain.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Equinox.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_LRTEast.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_LRTWest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_MtSarulente.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_NixNedar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Northwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Regulus.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_System.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Trannadar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Westwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Westwoods.slf")
    
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionAquaticGenetics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionArbonnePlains.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionArcaneUniversity.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsAlpha.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsBeta.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsDatacore.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsDelta.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsEpsilon.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsGamma.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsHydroponics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsRaijuRanch.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionCryogenics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionDimensionalTrap.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionEquinox.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionEvermoonForest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionLRTFacility.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionNixNedar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionNorthwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionMtSarulente.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionQuantirEstate.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionQuantirHighWastes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionRegulusCitySector15.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionRegulusCitySector96.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionSerenityObservatory.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionStarfieldSwamp.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionTrafalGlacier.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionTrannadarTradingPost.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionWestwoods.slf")

-- |[Maps]|
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Chapter 1")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Chapter 5")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/AAA Debug")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/ArbonnePlains")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Beehive")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/DimensionalTrap")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Evermoon")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/NixNedar")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Quantir")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/QuantirManse")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusArcane")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusBiolabs")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusCity")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusCryo")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusEquinox")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusExterior")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusFinale")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusFlashback")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusLRT")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusManufactory")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusMines")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusSerenity")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RiverWilds")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/StarfieldCaves")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/StarfieldSwamp")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/StForas")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Trafal")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/TrapBasement")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/TrapDungeon")

-- |[ =================================== Datafile Trimming ==================================== ]|
--Datafiles can contain extraneous data that is not needed to actually run the game. This data needs
-- to be left in place in the dev copy (as the dev can run all games from the same install) but
-- independent installs don't need to keep the data.
--To do this, a list of SLF files and paths is made. The SLF file is then opened and all the marked
-- images within it are removed and replaced with "Dummy" bitmaps that have a minimum of data.

-- |[Electrosprite Adventure]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/ElectrospriteAdventure.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "Player")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCACored")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCAGolem")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCANormal")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCATF0")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCATF1")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCATF2")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCBCored")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCBGolem")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCBNormal")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCBTF0")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCBTF1")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCBTF2")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCCCored")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCCGolem")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCCNormal")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCCTF0")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCCTF1")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCCTF2")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCDCored")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCDGolem")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCDNormal")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCDTF0")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCDTF1")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCDTF2")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCECored")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCEGolem")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCENormal")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCETF0")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCETF1")
BP_SetProperty("Register Command", "Dummy Image In File", "NPCETF2")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Base File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuBase.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Footer")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Adamantite Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Adamantite Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Adamantite Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Catalyst Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Catalyst Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Catalyst Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Character Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Character HP Fill")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Character HP Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Character Level Back")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Character Level Front")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Character Name Banner")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|Platina Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvBaseMenu|ICO|DoctorBag")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Campfire File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuCampfire.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireIcon|Rest")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireIcon|Chat")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Doctor File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuDoctor.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvDoctor|Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvDoctor|Bar Fill")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvDoctor|Bar Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvDoctor|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvDoctor|Symbol")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Equipment File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuEquipment.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Description Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Frames Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Frames Top")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Arrow Lft")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Arrow Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Equipment Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Expandable Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Replacement Divider")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Scrollbar Front")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvEquipment|Scrollbar Back")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Field Abilities]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuFieldAbilities.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuFieldAbility|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuFieldAbility|Footer")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[File Select UI]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuFileSelect.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvFileSelect|Arrow Lft")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvFileSelect|Arrow Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvFileSelect|Grid Backing")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvFileSelect|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvFileSelect|New File")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Icons File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvIcons.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "DmgTypeIco|Shocking")
BP_SetProperty("Register Command", "Dummy Image In File", "DmgTypeIco|Crusading")
BP_SetProperty("Register Command", "Dummy Image In File", "DmgTypeIco|Obscuring")
BP_SetProperty("Register Command", "Dummy Image In File", "DmgTypeIco|Bleeding")
BP_SetProperty("Register Command", "Dummy Image In File", "DmgTypeIco|Poisoning")
BP_SetProperty("Register Command", "Dummy Image In File", "DmgTypeIco|Corroding")
BP_SetProperty("Register Command", "Dummy Image In File", "DmgTypeIco|Terrifying")
BP_SetProperty("Register Command", "Dummy Image In File", "AbilityIco|Catalyst")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Inventory File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuInventory.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Deconstruct Box")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Description Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Expandable Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Item Listing Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Item Listing Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Item Listing Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Scrollbar Back")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvInventory|Scrollbar Front")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Journal File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuJournal.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Static Parts Achievements")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Static Parts Achievements Over Ch1")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Static Parts")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Static Parts Paragons")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Scrollbar Back")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Scrollbar Front")
BP_SetProperty("Register Command", "Dummy Image In File", "Achievements|System|CheckboxNo")
BP_SetProperty("Register Command", "Dummy Image In File", "Achievements|System|CheckboxYes")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Achievement Block")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|Achievement Scrollbar Back")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|BestiaryIconsL")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|BestiaryIconsR")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|BestiaryIconsRMonoceros")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|BestiaryNameBanner")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|BestiaryPortraitMask")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|ParagonSticker")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvJournal|ProfilesNameBanner")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Options File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuOptions.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Arrow Lft")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Arrow Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Button Defaults")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Button Okay")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Description Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Expandable Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Frame Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Option Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Slider Indicator")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Slider")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvOptions|Unsaved Changes Frame")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Quit File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuQuit.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvQuit|Backing")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvQuit|Button Exit Program")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvQuit|Button To Title")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Skills File]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuSkills.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Scrollbar Scroller")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Arrow Lft")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Arrow Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Buy Skill Popup")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Class Default Icon")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Description Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Equipped Icon")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Job Skills Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Jobs Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Jobs Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Jobs Scrollbar")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Memorized Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Skills Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Skills Detail Special")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Skills Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Skills Scrollbar")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Tactics Skills Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Profiles Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Profiles Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Profiles Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Profiles Instructions Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Profiles Memorized")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvMenuSkill|Profiles Scrollbar")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Standard UI]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuStandard.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStandard|Description Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStandard|Expandable Header")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Status UI]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuStatus.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Arrow Lft")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Arrow Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Equipment Backing")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Equipment Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Equipment Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Name Banner")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Resistance Backing")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Resistance Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Resistance Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status Backing")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status Detail")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status HP Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status HP Bar Fill")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status Level Backing")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status Level Outer")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status XP Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvStatus|Status XP Bar Fill")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Trainer]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuTrainer.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Frame_Base")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Frame_Confirm")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Frame_Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Frame_Payment")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Frame_Platina")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Overlay_ArrowRgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Overlay_ArrowUp")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvTrainer|Overlay_NameBanner")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Vendor UI]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuVendor.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Arrow Lft")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Arrow Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Buy Item Popup")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Comparison Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Description Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Detail Buy")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Detail Buyback")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Detail Gems")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Detail Gems Merge")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Detail Sell")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Expandable Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Platina Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Mat Arrow Lft")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Mat Arrow Rgt")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Scrollbar Static")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Scrollbar Scroller")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Vendor Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Unlock Frame")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Unlock Stencil")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvVendor|Category Frames")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[Warp UI]|
BP_SetProperty("Register Command", "Open File For Modification", "Games/AdventureMode/Datafiles/UIAdvMenuCampfire.slf")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Header")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Footer")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Map Location Backing")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Map Location Scrollbar")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Pin_Accuracy")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Pin_Evade")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Pin_Health")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Pin_Initiative")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Pin_Power")
BP_SetProperty("Register Command", "Dummy Image In File", "AdvCampfireMenu|Pin_Skill")
BP_SetProperty("Register Command", "Finish Dummy Changes")

-- |[ ==================================== Non-Demo Cleanup ==================================== ]|
--This is not the demo build, so delete the map used for the demo.
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Debug/SampleDemo")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/BowmillGraveyard/GraveyardBDemo")

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the active object.
DL_PopActiveObject()