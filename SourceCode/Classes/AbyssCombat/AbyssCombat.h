///======================================== AbyssCombat ===========================================
//--Daughter class of AdvCombat, designed to make use of its basic functionality but adjust the
//  UI/Character layout in battle.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "AdvCombat.h"

///====================================== Local Structures ========================================
///--[CommandListEntry]
//--Represents an option the player can select on the command list. Can be an ability or open another menu.
typedef struct CommandListEntry
{
    ///--[Members]
    bool mIsManuallyLocked;
    bool mGlowing;
    char *mDisplayName;
    char *mDisplayString;
    int mQuantity;
    int mInventorySlot;
    bool mIsCharmSkill;
    StarBitmap *rSuffixImg;
    StarLinkedList *mIconsList; //StarBitmap *, reference
    StarLinkedList *mSublist; //CommandListEntry *, master
    AdvCombatAbility *rAssociatedAbility;

    ///--[Functions]
    void Initialize();
    static void DeleteThis(void *pPtr);
}CommandListEntry;

///--[CalloutPack]
//--Represents a speech bubble on the field. This allows characters to speak briefly.
typedef struct CalloutPack
{
    ///--[Members]
    uint32_t mUserID;
    int mTextTimer;
    int mRemoveTicks;
    bool mIsBackwards;
    bool mIsUpDown;
    float mX;
    float mY;
    float mW;
    float mH;
    int mLinesTotal;
    char **mLines;

    ///--[Functions]
    void Initialize();
    static void DeleteThis(void *pPtr);
}CalloutPack;

///--[TutorialStringPack]
typedef struct TutorialStringPack
{
    float mX;
    float mY;
    char *mText;
    void Initialize();
    static void DeleteThis(void *pPtr);
}TutorialStringPack;

///===================================== Local Definitions ========================================
//--Timers
#define ABYCOM_BATTLE_START_FADE_TICKS 15
#define ABYCOM_COMMAND_SCROLL_TICKS 15
#define ABYCOM_COMMAND_NEWTURN_INDICATOR_TICKS 25
#define ABYCOM_COMMAND_GLOW_TICKS 30
#define ABYCOM_ABILITY_DESCRIPTION_TICKS 15
#define ABYCOM_CALLOUT_TICKS_PER_LETTER 1
#define ABYCOM_CALLOUT_TICKS_TO_HOLD 300
#define ABYCOM_CALLOUT_TICKS_FADE_IN 5
#define ABYCOM_CALLOUT_TICKS_FADE_OUT 5
#define ABYCOM_DEFEAT_SLIDE_TICKS 15
#define ABYCOM_DEFEAT_FADEUI_TICKS 60
#define ABYCOM_DEFEAT_FADE_TICKS 60
#define ABYCOM_DEFEAT_HOLD_TICKS 60
#define ABYCOM_TUTORIAL_FADE_TICKS 22
#define ABYCOM_DISABLE_FADE_TICKS 22
#define ABYCOM_REPOSITION_TICKS 22

//--Sizes / Positions / Offsets
#define ABYCOM_PLAYER_PARTY_MAX_SIZE 3
#define ABYCOM_COMMAND_MAX_ENTRIES_PER_PAGE 6
#define ABYCOM_MAX_DESCRIPTION_LINES 8
#define ABYCOM_REPOSITION_Y_OFFSET -800.0f
#define ABYCOM_FIXED_ENEMY_Y 245.0f

//--Stencil Codes
#define ABYCOM_STENCIL_COMMANDWINDOW 1
#define ABYCOM_STENCIL_SECONDWINDOW 2
#define ABYCOM_STENCIL_TARGETWINDOW 3

//--Script Calls
#define ACE_SCRIPT_CODE_ABYDEFEAT 20

//--Defeat Handling
#define ABYCOM_DEFEAT_PHASE_FADE_OUT_UI 0
#define ABYCOM_DEFEAT_PHASE_HOLD 1
#define ABYCOM_DEFEAT_PHASE_CHAR0_SHOW 2
#define ABYCOM_DEFEAT_PHASE_CHAR0_HOLD 3
#define ABYCOM_DEFEAT_PHASE_CHAR0_HIDE 4
#define ABYCOM_DEFEAT_PHASE_CHAR1_SHOW 5
#define ABYCOM_DEFEAT_PHASE_CHAR1_HOLD 6
#define ABYCOM_DEFEAT_PHASE_CHAR1_HIDE 7
#define ABYCOM_DEFEAT_PHASE_CHAR2_SHOW 8
#define ABYCOM_DEFEAT_PHASE_CHAR2_HOLD 9
#define ABYCOM_DEFEAT_PHASE_CHAR2_HIDE 10
#define ABYCOM_DEFEAT_PHASE_FADEOUT 11

//--Icons
#define ABYCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL 5

//--Finale
#define ABYCOMBAT_FINALE_LINES_TOTAL 3
#define ABYCOMBAT_FINALE_LINE_LEN 128
#define ABYCOMBAT_FINALE_SCRIPT_INITIALIZE 0.0f
#define ABYCOMBAT_FINALE_SCRIPT_POPULATE 1.0f

//--Strings/Translation
#define ABYCOMBAT_STRING_TOTAL 6

///========================================== Classes =============================================
class AbyssCombat : public AdvCombat
{
    protected:
    ///--[Constants]
    static const int cxCommand_SizePerPage = 28;            //Number of commands that can fit on one page. There are two columns of 14 each.

    ///--[System]
    bool mHasInitializedAbyCombat;
    int mStartBattleFadeTimer;
    bool mIsEmergencyEscape;
    bool mDisallowEmergencyEscape;

    ///--[Settings]
    float mCombatTextScale;                                 //Combat text (eg: "Guarding!" "Poisoned!") is scaled by this amount.

    ///--[UI]
    int mTransparencyCursor;
    void *rLastHighlightedPtr; //Only used for comparisons
    bool mShowAbilityDescription;
    int mAbilityDescriptionTimer;
    AdvCombatAbility *rDescriptionAbility;
    StarlightColor mColorHeader;
    StarlightColor mColorSubheader;
    StarlightColor mColorParagraph;
    StarlightString *mCyclePredictionString;
    StarlightString *mEmergencyEscapeString;

    ///--[Command Input]
    bool mShowCommandWindow;
    int mCommandScrollTimer;
    int mCommandTimer;
    int mCommandSecondaryTimer;
    int mCommandGlowTimer;
    int mPlayerCommandBaseCursor;
    int mPlayerCommandCursor;
    int mPlayerCommandOffset;
    int mSkillItemSkip;
    StarBitmap *rActingCharacterImg;
    StarLinkedList *mrPlayerCommandStack; //CommandListEntry *, reference

    ///--[Command Storage]
    CommandListEntry *mDummyEntry;
    StarLinkedList *mCommandList; //CommandListEntry *, master
    StarLinkedList *mrCommandStack; //CommandListEntry *, reference

    ///--[Callouts]
    StarLinkedList *mCalloutList; //CalloutPack *, master

    ///--[Defeat Checking]
    bool mIsMemberDisabled[ABYCOM_PLAYER_PARTY_MAX_SIZE];
    int mDisableTimers[ABYCOM_PLAYER_PARTY_MAX_SIZE];
    int mDefeatCharacterPhase;
    int mDefeatTimer;

    ///--[Universal Abilities]
    StarLinkedList *mGlobalAbilities; //AdvCombatAbility *, master

    ///--[Tutorial Overlay]
    bool mIsShowingTutorialOverlay;
    int mTutorialTimer;
    StarBitmap *rTutorialOverlayCur;
    StarBitmap *rTutorialOverlayPrv;
    StarLinkedList *mTutorialStringsCur;//TutorialStringPack *, master
    StarLinkedList *mTutorialStringsPrv;//TutorialStringPack *, master

    ///--[Finale Callout]
    float mFinalePortraitX;
    float mFinalePortraitY;
    StarBitmap *rFinalePortraitImg;
    char *mFinalePortraitName;
    char mFinaleLines[ABYCOMBAT_FINALE_LINES_TOTAL][ABYCOMBAT_FINALE_LINE_LEN];

    ///--[Strings]
    union NormalString
    {
        struct
        {
            char *mPtr[ABYCOMBAT_STRING_TOTAL];
        }mem;
        struct
        {
            char *mEmergencyEscapeA;
            char *mEmergencyEscapeB;
            char *mCyclePredictions;
            char *mEmergencyCombatExit;
            char *mTextRewardPlusPlatina;
            char *mTextPlatina;
        }str;
    }AbyssString;

    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            ///--[Fonts]
            //--Command Window
            StarFont *rFontHeader;
            StarFont *rFontCommandWindow;
            StarFont *rFontSubcommand;
            StarFont *rFontCallout;
            StarFont *rFontConfirmBig;
            StarFont *rFontConfirmSmall;
            StarFont *rFontPlayerUI;
            StarFont *rFontControl;
            StarFont *rFontDescription;
            StarFont *rFontPrediction;
            StarFont *rFontVictory;
            StarFont *rFontLoot;
            StarFont *rFontNameplate;
            StarFont *rFontDialogue;
            StarFont *rFontPlatina;
            StarFont *rFontEnemyUI;
            StarFont *rFontCombatText;

            ///--[Bitmaps]
            //--UI Pieces
            StarBitmap *rFillEnemyHP;
            StarBitmap *rFillEnemyStar;
            StarBitmap *rFillEnemyStun;
            StarBitmap *rFillPlayerHP;
            StarBitmap *rFillPlayerMP;
            StarBitmap *rFrameDescription;
            StarBitmap *rFrameEnemyHP;
            StarBitmap *rFrameEnemyStun;
            StarBitmap *rFrameOption;
            StarBitmap *rFramePlatina;
            StarBitmap *rFramePlayerHP;
            StarBitmap *rFramePlayerMP;
            StarBitmap *rFramePrediction;
            StarBitmap *rFrameSkillItems;
            StarBitmap *rFrameTurnOrder;
            StarBitmap *rFrameTurnOrderCur;
            StarBitmap *rFrameVictoryDialogue;
            StarBitmap *rFrameVictoryLoot;
            StarBitmap *rOverlayCPPip;
            StarBitmap *rOverlayEnemyHPIcon;
            StarBitmap *rOverlayExclamation;
            StarBitmap *rOverlayHPIcon;
            StarBitmap *rOverlayOptionSelect;
            StarBitmap *rOverlaySkillItemSelect;
            StarBitmap *rOverlaySkillItemShade;
            StarBitmap *rOverlayVictorySelection;
            StarBitmap *rOverlayVictoryShade;
            StarBitmap *rScrollbarScroller;
            StarBitmap *rScrollbarStatic;
            StarBitmap *rSpeechBubble;
            StarBitmap *rSpeechBubbleBot;
        }Data;
    }AbyssImg;

    protected:

    public:
    //--System
    AbyssCombat();
    virtual ~AbyssCombat();

    //--Public Variables
    bool mReplyToDefeated;
    static char *xVictoryDialoguePath;
    static bool xAlwaysPlayCallouts;
    static bool xIgnoreDisablesForTargets;

    //--Property Queries
    virtual bool IsOfType(int pType);
    bool IsPartyDisabled(int pSlot);
    bool IsPartyDisabledID(uint32_t pID);
    bool DoesGlobalAbilityExist(const char *pName);
    AdvCombatAbility *GetGlobalAbility(const char *pAbilityName);
    const char *GetPathOfGlobalAbility(const char *pAbilityName);

    //--Manipulators
    void SetCombatTextScale(float pScale);
    void SetDisallowEmergencyEscape(bool pFlag);
    void SetPartyDisabledFlag(int pSlot, bool pFlag);
    void SetPartyDisabledFlagByID(uint32_t pUniqueID, bool pFlag);
    void SetTutorialOverlay(const char *pDLPath);
    void RegisterTutorialString(float pX, float pY, const char *pString);

    //--Core Methods
    StarLinkedList *ResolvePlayerCommandList();
    virtual void ReresolveDescriptionAbility();
    virtual float ComputeIdealX(bool pIsLeftSide, bool pIsOffscreen, int pPosition, int pTotalEntries, StarBitmap *pCombatPortrait);
    virtual float ComputeIdealY(AdvCombatEntity *pEntity);
    virtual void PositionEnemiesByTarget(StarLinkedList *pTargetList);
    virtual bool IsPlayerDefeated();
    void TakeAbilityFromEntity(const char *pAbilityName);
    virtual void CreatePredictionBox(const char *pBoxName);
    virtual void SetPredictionBoxStringImage(const char *pBoxName, int pStringIndex, int pSlot, float pYOffset, const char *pPath);

    ///--Applications
    virtual void ResolveBattlePosition(AdvCombatEntity *pEntity, int pTypeFlag, float &sXPos, float &sYPos);
    virtual void SetPredictionBoxHostI(const char *pBoxName, uint32_t pHostID);
    //virtual void HandleApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString);

    ///--Callouts
    void RegisterCallout(const char *pName);
    void SetCalloutUserID(uint32_t pID);
    void AllocateCalloutLines(int pLines);
    void SetCalloutLine(int pSlot, const char *pText);
    void FlipCalloutX();
    void FlipCalloutY();
    void PositionCallout(float pXTarget, float pYTarget);
    void SetCalloutTimer(int pTicks);
    void UpdateCallouts();
    void RenderCallouts();

    ///--Commands
    CommandListEntry *GetActiveCommand();
    bool DoesCommandExist(const char *pInternalName);
    void SetCommandColorFromEntry(CommandListEntry *pEntry, float pAlpha);
    void RegisterCommandListEntry(const char *pInternalName);
    void PushCommandListEntry(const char *pInternalName);
    void PopCommandList();
    void ClearCommandList();
    CommandListEntry *ResolveStackHead(const char *pErrorFunction);
    void SetCommandDisplayName(const char *pDisplayName);
    void SetCommandDisplayString(const char *pDisplayString);
    void SetCommandManualLock(bool pIsLocked);
    void SetCommandGlowing(bool pIsGlowing);
    void SetCommandAssociatedAbility(const char *pAbilityName);
    void SetCommandQuantity(int pQuantity);
    void SetInventorySlot(int pSlot);
    void AddCommandIcon(const char *pIconPath);
    void AddSuffixIcon(const char *pIconPath);
    void SetCommandIsCharm(bool pFlag);

    ///--Construction
    virtual void Initialize();
    virtual void Construct();
    virtual void Reinitialize(bool pHandleMusic);
    virtual void Disassemble();

    ///--Inspector
    virtual void RenderInspectorResistStats();

    ///--Resolution
    void SetVictorySpeaker(const char *pName);
    void SetVictoryLine(int pSlot, const char *pString);
    virtual void UpdateResolution();
    virtual void RenderVictory();
    virtual void RenderVictoryBanner(float pPercent);
    virtual void RenderVictoryDrops(float pPercent);
    virtual void RenderVictoryDoctor(float pPercent);
    virtual void RenderExpBars(float pPercent);
    virtual void RenderDefeat();

    ///--Targeting
    virtual void PopulateTargetsByCode(const char *pCodeName, void *pCaller);

    ///--Turn Order
    virtual void BeginTurn();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    virtual void HandlePlayerControls(bool pAnyEntitiesMoving);
    virtual void UpdateInspectorEntitySelect();
    virtual void UpdateInspectorEffectSelect();

    //--File I/O
    //--Drawing
    virtual void Render(bool pBypassResolution, float pAlpha);
    void RenderStringPackList(StarLinkedList *pList);
    void RenderPartyMember(int pSlot, float pAlpha);
    void RenderPartyMemberUI(int pSlot, float pAlpha);
    void RenderEnemy(int pSlot, float pAlpha);
    virtual void RenderEntityUI(AdvCombatEntity *pEntity, float pAlpha);
    virtual void RenderTurnOrder(float pAlpha);
    void RenderAbyssEntity(AdvCombatEntity *pEntity, float pAlpha);
    void RenderCommandWindow(AdvCombatEntity *pEntity, float pAlpha);
    void RenderAbilityDescriptions(float pAlpha);
    virtual void RenderInspector();
    virtual void RenderInspectorAbilities();
    virtual void RenderConfirmation();
    virtual void RenderPredictionBoxes();
    virtual void RenderTitle();

    //--Pointer Routing
    //--Static Functions
    static AbyssCombat *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AbyCombat_GetProperty(lua_State *L);
int Hook_AbyCombat_SetProperty(lua_State *L);

