//--Base
#include "SlittedEyeLevel.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "MapManager.h"

///========================================== System ==============================================
SlittedEyeLevel::SlittedEyeLevel()
{
    ///--[Variable Initialization]
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_SLITTEDEYELEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System
    mName = NULL;
    ResetString(mName, "Unnamed Level");

    //--[SlittedEyeLevel]
    //--System
    mPrintedNotFunctionWarning = false;

    //--Player Statistics
    mDisableHiss = false;
    mDisableManipulate = false;
    mAllowGameOver = false;
    mGameTime = 0;
    mStamina = 0.0f;
    mNoise = 0.0f;

    //--Hissing
    mIsHissing = false;
    mHissTimer = 0;
    mStaminaLossPerHiss = 0.0f;
    mHissHitboxes = new StarLinkedList(true);

    //--Mouse Handling
    mMouseX = -100;
    mMouseY = -100;

    //--Scripting
    mUpdateScript = NULL;
    mGameOverScript = NULL;

    //--World Rendering
    mDisallowScrolling = false;
    mXOffset = 217;
    mImagesTotal = 0;
    mrImages = NULL;

    //--Fading
    mIsFading = false;
    mFadePersists = false;
    mFadeTicksCur = 0;
    mFadeTicksMax = 1;
    mFadeColBegin.SetRGBAF(1.0f, 1.0f, 1.0f, 0.0f);
    mFadeColCurrent.SetRGBAF(1.0f, 1.0f, 1.0f, 0.0f);
    mFadeColFinish.SetRGBAF(1.0f, 1.0f, 1.0f, 0.0f);

    ///--[Construction]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Images.GUI.rGUIDefaultFont = rDataLibrary->GetFont("Slitted Eye GUI");

    ///--[Verify]
    Images.mIsReady = VerifyStructure(&Images.GUI, sizeof(Images.GUI), sizeof(void *), false);
}
SlittedEyeLevel::~SlittedEyeLevel()
{
    delete mHissHitboxes;
    free(mUpdateScript);
    free(mGameOverScript);
    free(mrImages);
}

///--[Private Static Variables]
///--[Public Static Variables]

///===================================== Property Queries =========================================
bool SlittedEyeLevel::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_SLITTEDEYELEVEL) return true;
    if(pType == POINTER_TYPE_ROOTLEVEL) return true;
    return false;
}
int SlittedEyeLevel::GetPlayerTime()
{
    return mGameTime;
}
float SlittedEyeLevel::GetPlayerStamina()
{
    return mStamina;
}
float SlittedEyeLevel::GetPlayerNoise()
{
    return mNoise;
}

///======================================= Manipulators ===========================================
void SlittedEyeLevel::SetHissDisabled(bool pFlag)
{
    mDisableHiss = pFlag;
}
void SlittedEyeLevel::SetManipulateDisabled(bool pFlag)
{
    mDisableManipulate = pFlag;
}
void SlittedEyeLevel::SetGameOverEnabled(bool pFlag)
{
    mAllowGameOver = pFlag;
}
void SlittedEyeLevel::SetGameTime(int pTime)
{
    mGameTime = pTime;
}
void SlittedEyeLevel::SetStamina(float pAmount)
{
    mStamina = pAmount;
}
void SlittedEyeLevel::SetNoise(float pAmount)
{
    mNoise = pAmount;
}
void SlittedEyeLevel::SetStaminaLossPerHiss(float pAmount)
{
    mStaminaLossPerHiss = pAmount;
}
void SlittedEyeLevel::SetDisallowScrolling(bool pFlag)
{
    mDisallowScrolling = pFlag;
}
void SlittedEyeLevel::SetUpdatePath(const char *pPath)
{
    //--Reset to NULL.
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mUpdateScript, NULL);
        return;
    }

    //--Normal case.
    ResetString(mUpdateScript, pPath);
}
void SlittedEyeLevel::SetGameOverPath(const char *pPath)
{
    //--Reset to NULL.
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mGameOverScript, NULL);
        return;
    }

    //--Normal case.
    ResetString(mGameOverScript, pPath);
}
void SlittedEyeLevel::AllocateImages(int pTotal)
{
    //--Deallocate.
    mImagesTotal = 0;
    free(mrImages);
    mrImages = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    mImagesTotal = pTotal;
    mrImages = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mImagesTotal);

    //--Clear.
    memset(mrImages, 0, sizeof(StarBitmap *) * mImagesTotal);
}
void SlittedEyeLevel::SetImage(int pSlot, const char *pDLPath)
{
    //--Range check.
    if(pSlot < 0 || pSlot >= mImagesTotal) return;

    //--If the specification is NULL or "Null", null it off. Slots can be empty and will be bypassed
    //  at render time.
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        mrImages[pSlot] = NULL;
        return;
    }

    //--Set.
    mrImages[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    if(!mrImages[pSlot])
    {
        DebugManager::ForcePrint("Warning: Image %s was NULL.\n", pDLPath);
    }
}
void SlittedEyeLevel::RegisterHissHitbox(const char *pName, float pLft, float pTop, float pRgt, float pBot, const char *pPath, float pFiringCode)
{
    //--Error check.
    if(!pName || !pPath) return;

    //--Allocate, set values.
    HissingHitbox *nHitbox = (HissingHitbox *)starmemoryalloc(sizeof(HissingHitbox));
    nHitbox->Initialize();
    nHitbox->mHitbox.Set(pLft, pTop, pRgt, pBot);
    ResetString(nHitbox->mScriptPath, pPath);
    nHitbox->mFiringCode = pFiringCode;

    //--Register.
    mHissHitboxes->AddElement(pName, nHitbox, &HissingHitbox::DeleteThis);
}
void SlittedEyeLevel::FlagHissHitboxForDeletion(const char *pName)
{
    //--Marks all hitboxes matching the name for deletion, in case of duplicates.
    if(!pName) return;
    HissingHitbox *rHitbox = (HissingHitbox *)mHissHitboxes->PushIterator();
    while(rHitbox)
    {
        if(!strcasecmp(pName, mHissHitboxes->GetIteratorName()))
        {
            rHitbox->mFlagForDeletion = true;
        }
        rHitbox = (HissingHitbox *)mHissHitboxes->AutoIterate();
    }
}
void SlittedEyeLevel::ActivateFade(int pTicks, bool pPersists, StarlightColor pStartCol, StarlightColor pEndCol)
{
    //--Error check.
    if(pTicks < 1) return;

    //--Flags.
    mIsFading = true;
    mFadePersists = pPersists;
    mFadeTicksCur = 0;
    mFadeTicksMax = pTicks;
    memcpy(&mFadeColBegin, &pStartCol, sizeof(StarlightColor));
    memcpy(&mFadeColFinish, &pEndCol, sizeof(StarlightColor));

    //--Set the current fade to the beginning fade.
    memcpy(&mFadeColCurrent, &mFadeColBegin, sizeof(StarlightColor));
}
void SlittedEyeLevel::ActivateFadeUsingCurrent(int pTicks, bool pPersists, StarlightColor pEndCol)
{
    //--Same as above, but uses whatever the current color of the fade is as the starting color.
    //  If no fade is taking place or persisting, then uses zero-alpha-white.
    if(mIsFading)
    {
        ActivateFade(pTicks, pPersists, mFadeColCurrent, pEndCol);
    }
    //--No fade, but the last fade persists. Use the ending color.
    else if(!mIsFading && mFadePersists)
    {
        ActivateFade(pTicks, pPersists, mFadeColFinish, pEndCol);
    }
    //--No fade, no persisting fade. Use the default color.
    else
    {
        ActivateFade(pTicks, pPersists, StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 0.0f), pEndCol);
    }
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
SlittedEyeLevel *SlittedEyeLevel::Fetch()
{
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_SLITTEDEYELEVEL)) return NULL;
    return (SlittedEyeLevel *)rCheckLevel;
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
