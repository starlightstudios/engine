-- |[ ================================= SysPaths Script Hunter ================================= ]|
--The Script Hunter is a debug object that can scan lua files for errors. It needs to know what
-- paths it needs to scan, which is what this file is for.
--The Script Hunter is activated from the Debug Menu in Adventure Mode. You will then be able to 
-- select which file tree to scan.

-- |[ ========================== SysPaths:fnBuildScriptHunterPaths() =========================== ]|
--Call once at startup to build paths for the Script Hunter.
function SysPaths:fnBuildScriptHunterPaths()
    
    --Call.
    ADebug_SetProperty("Clear Script Hunter")
    ADebug_SetProperty("Add Script Hunter Path", "Games/AdventureLevelGenerator/")
    ADebug_SetProperty("Add Script Hunter Path", "../adventure/Games/AdventureLevelGenerator/")
    ADebug_SetProperty("Add Script Hunter Path", "Games/AdventureMode/")
    ADebug_SetProperty("Add Script Hunter Path", "../adventure/Games/AdventureMode/")
    ADebug_SetProperty("Add Script Hunter Path", "Games/ClassicMode/")
    ADebug_SetProperty("Add Script Hunter Path", "../classic/Games/ClassicMode/")
    ADebug_SetProperty("Add Script Hunter Path", "Games/ClassicMode3D/")
    ADebug_SetProperty("Add Script Hunter Path", "../classic/Games/ClassicMode3D/")
    ADebug_SetProperty("Add Script Hunter Path", "Games/CorrupterMode/")
    ADebug_SetProperty("Add Script Hunter Path", "../classic/Games/CorrupterMode/")
    ADebug_SetProperty("Add Script Hunter Path", "Games/DollManor/")
    ADebug_SetProperty("Add Script Hunter Path", "../dollmanor/Games/DollManor/")
    ADebug_SetProperty("Add Script Hunter Path", "Games/ElectrospriteTextAdventure/")
    ADebug_SetProperty("Add Script Hunter Path", "../adventure/Games/ElectrospriteTextAdventure/")
    
end