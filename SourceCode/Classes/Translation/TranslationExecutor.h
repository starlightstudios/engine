///==================================== Translation Executor =======================================
//--Class responsible for reading and executing a translation file in a standardized format. Translations
//  can be run on-the-fly though it is best to run one at program startup or early on.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define TE_MODE_NONE 0
#define TE_MODE_FILENAME 1
#define TE_MODE_ORIGINAL 2
#define TE_MODE_CHANGETO 3

///========================================== Classes =============================================
class TranslationExecutor
{
    private:
    ///--[System]
    int mCurrentMode;
    char *mRootPath;
    char *mTranslationFilePath;

    ///--[Input Parsing]
    int mBypassInstructionLinesLeft;
    StarAutoBuffer *mParsingData;

    ///--[Current File]
    char *mCurrentFileName;
    StarLinkedList *mOrigTextList;
    StarLinkedList *mReplaceTextList;

    ///--[Untranslation]
    char *mUntranslateFilePath;
    StarAutoBuffer *mUntranslateFileData;

    protected:

    public:
    //--System
    TranslationExecutor();
    ~TranslationExecutor();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    void ExecuteTranslationFile(const char *pRootPath, const char *pFileName);

    private:
    //--Private Core Methods
    char *ReadLine(VirtualFile *fInfile);
    void HandleLine(const char *pLine, bool pIgnoreInstructions);
    void ResetAutobuffer();
    void AddParsedLinesToList(char *pBuffer, int pLen, StarLinkedList *pList);
    void ChangeLines();

    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_TE_Execute(lua_State *L);


