//--Base
#include "AdventureLevel.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "DisplayManager.h"

//--Script Fading
void AdventureLevel::SetAlternateBlending(bool pFlag)
{
    //--This gets un-set everytime ActivateScriptFade() is called. It must be re-set afterwards.
    mUseAlternateBlend = pFlag;
}
void AdventureLevel::ActivateScriptFade(int pTicks, StarlightColor pStartColor, StarlightColor pEndColor, int pDepthFlag, bool pHoldsOnComplete)
{
    //--Activates fading, causing the screen to fade from the start color to the end color. If the start color has a negative
    //  alpha, then it will use whatever the current blending color is.
    //--If a zero-tick value is set, then the fade completes immediately. Use pHoldsOnComplete to make this stick since it'd otherwise display nothing.
    SetAlternateBlending(false);
    if(pTicks < 1)
    {
        mScriptFadeDepthFlag = pDepthFlag;
        mScriptFadeTimer = 1;
        mScriptFadeTimerMax = 1;
        mScriptFadeHolds = true;
        memcpy(&mStartFadeColor, &pStartColor, sizeof(StarlightColor));
        memcpy(&mEndFadeColor,   &pEndColor,   sizeof(StarlightColor));
        return;
    }

    //--If the start color has a positive or zero alpha, just copy it.
    if(pStartColor.a >= 0.0f)
    {
        memcpy(&mStartFadeColor, &pStartColor, sizeof(StarlightColor));
    }
    //--Otherwise, use the current mixing color.
    else
    {
        //--If there was no fade in progress and the fade was not holding, use a zero-alpha with the same RGB as the end color.
        if(!mIsScriptFading && !mScriptFadeHolds)
        {
            memcpy(&mStartFadeColor, &pEndColor, sizeof(StarlightColor));
            mStartFadeColor.a = 0.0f;
        }
        //--If there is a fade event currently taking place:
        else if(mIsScriptFading)
        {
            //--Compute how far along the fade is.
            float tFadePercent = mScriptFadeTimer / (float)mScriptFadeTimerMax;

            //--The color is this percentage between the start and end colors.
            mStartFadeColor.r = mStartFadeColor.r + ((mEndFadeColor.r - mStartFadeColor.r) * tFadePercent);
            mStartFadeColor.g = mStartFadeColor.g + ((mEndFadeColor.g - mStartFadeColor.g) * tFadePercent);
            mStartFadeColor.b = mStartFadeColor.b + ((mEndFadeColor.b - mStartFadeColor.b) * tFadePercent);
            mStartFadeColor.a = mStartFadeColor.a + ((mEndFadeColor.a - mStartFadeColor.a) * tFadePercent);
        }
        //--If the fade is in the hold case, use the end color.
        else if(mScriptFadeHolds)
        {
            memcpy(&mStartFadeColor, &pEndColor, sizeof(StarlightColor));
        }
    }

    //--The end color is always fixed.
    memcpy(&mEndFadeColor, &pEndColor, sizeof(StarlightColor));

    //--Set these flags independent of the colors. They must be set afterwards since the colors might need to query
    //  them for state information.
    mIsScriptFading = true;
    mScriptFadeHolds = pHoldsOnComplete;
    mScriptFadeDepthFlag = pDepthFlag;
    mScriptFadeTimer = 0;
    mScriptFadeTimerMax = pTicks;
}
void AdventureLevel::UpdateScriptFade()
{
    //--Handles updating the script fading procedure. Mostly just runs the timer.
    if(mIsScriptFading)
    {
        //--Timer.
        if(mScriptFadeTimer < mScriptFadeTimerMax)
        {
            mScriptFadeTimer ++;
        }
        //--Ending case.
        else
        {
            mIsScriptFading = false;
        }
    }
}
void AdventureLevel::RenderScriptFade(int pAtDepth)
{
    ///--[Documentation and Setup]
    //--Renders the overlay responsible for the script fade actually doing something in the game.

    ///--[Not Match]
    //--The flag doesn't match. There is one instance of special behavior here.
    if(pAtDepth != mScriptFadeDepthFlag)
    {
        //--If the script is not in a fade, or is not in a fade and is not holding, ignore the special case.
        if(!mIsScriptFading && !mScriptFadeHolds) return;

        //--If the mScriptFadeDepthFlag is SCRIPT_FADE_OVER_UI_BLACKOUT_WORLD and this is SCRIPT_FADE_UNDER_UI,
        //  blackout the world. This is used once, at game start, for the tutorial.
        if(pAtDepth == SCRIPT_FADE_UNDER_UI && mScriptFadeDepthFlag == SCRIPT_FADE_OVER_UI_BLACKOUT_WORLD)
        {
            //--Setup.
            glDisable(GL_TEXTURE_2D);
            glColor3f(0.0f, 0.0f, 0.0f);

            //--Render.
            glBegin(GL_QUADS);
                glVertex2f(            0.0f,             0.0f);
                glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
                glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
                glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
            glEnd();

            //--Clean.
            glEnable(GL_TEXTURE_2D);
            StarlightColor::ClearMixer();
            return;
        }
        //--Special case: If the mScriptFadeDepthFlag is SCRIPT_FADE_OVER_UI_BLACKOUT_WORLD and this is SCRIPT_FADE_OVER_UI,
        //  behave normally.
        else if(pAtDepth == SCRIPT_FADE_OVER_UI && mScriptFadeDepthFlag == SCRIPT_FADE_OVER_UI_BLACKOUT_WORLD)
        {
        }
        //--Otherwise, don't render here.
        else
        {
            return;
        }
    }

    ///--[Match]
    //--In the event of specialized blending:
    if(mUseAlternateBlend)
    {
        glBlendFunc(GL_DST_COLOR, GL_ZERO);
    }

    //--If the script fade is currently in a hold, render its end color.
    if(mScriptFadeHolds && !mIsScriptFading)
    {
        //--Setup.
        glDisable(GL_TEXTURE_2D);
        mEndFadeColor.SetAsMixer();

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();
    }
    //--If the script is currently fading, render the partial fade.
    else if(mIsScriptFading)
    {
        //--Compute how far along the fade is.
        float tFadePercent = mScriptFadeTimer / (float)mScriptFadeTimerMax;

        //--The color is this percentage between the start and end colors.
        float tR = mStartFadeColor.r + ((mEndFadeColor.r - mStartFadeColor.r) * tFadePercent);
        float tG = mStartFadeColor.g + ((mEndFadeColor.g - mStartFadeColor.g) * tFadePercent);
        float tB = mStartFadeColor.b + ((mEndFadeColor.b - mStartFadeColor.b) * tFadePercent);
        float tA = mStartFadeColor.a + ((mEndFadeColor.a - mStartFadeColor.a) * tFadePercent);

        //--Setup.
        glDisable(GL_TEXTURE_2D);
        glColor4f(tR, tG, tB, tA);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();
    }

    //--Clear blending mixer.
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
