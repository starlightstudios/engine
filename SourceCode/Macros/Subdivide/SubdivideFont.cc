//--Base
#include "Subdivide.h"

//--Classes
//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "utf8.h"
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define SUBDIVIDE_FONT_DEBUG
#ifdef SUBDIVIDE_FONT_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///======================================= Font Version ===========================================
StarLinkedList *Subdivide::SubdivideStringFlags(const char *pString, uint32_t pFlags, StarFont *pFont, float pLength)
{
    ///--[ ========== Documentation =========== ]
    //--Version of subdivide meant to explicitly work with StarFont using flags, allowing subdivision based on length.
    //  That is, the length of the text on screen, with kerning, is used to add breakpoints. This also supports \n and
    //  [BR] for adding manual breakpoints. These behaviors can be toggled using flags, which are in the SUBDIVIDE_FLAG_
    //  series in Subdivide.h
    //--The extra arguments can be whatever values you want if the flags don't require them. Passing in flags that do will
    //  print error messages and return an empty list.
    //--As in other functions, this returns a StarLinkedList with the strings on it, but the list can contain zero
    //  entries on error.
    //--This implicitly supports UTF.
    DebugPush(pFlags & SUBDIVIDE_FLAG_DIAGNOSTICS, "Beginning subdivision of string.\n");

    //--Basic setup.
    StarLinkedList *nStringList = new StarLinkedList(true);
    if(!pString)
    {
        DebugPop("No string provided, stopping.\n");
        return nStringList;
    }

    ///--[ ========== Error Checking ========== ]
    //--If the request has the flag requiring a font for length checks, but the font is invalid, stop.
    if(pFlags & SUBDIVIDE_FLAG_FONTLENGTH && !pFont)
    {
        fprintf(stderr, "Subdivide::SubdivideStringFlags() - Warning, flagged for font length but no font was provided. Failing.\n");
        DebugPop("Stopping due to error.\n");
        return nStringList;
    }

    ///--[ ============== Setup =============== ]
    //--Create a buffer that will hold strings as they are assembled. A string with no breakpoints will only take that
    //  much space, though in most cases considerably less buffer size is needed.
    int cMemLen = (int)strlen(pString);  //Size of the string in bytes
    int cCharLen = utf8_strlen(pString); //Size of the string in unique characters
    char *tBuffer = (char *)starmemoryalloc(sizeof(char) * (cMemLen+1));

    //--Variables.
    int tCurAppendIndex = 0;
    int tLastSpaceMem = 0;
    int tLastSpaceChar = 0;

    ///--[ ============ Iteration ============= ]
    //--Iterate across the string and break into substrings.
    for(int i = 0; i < cCharLen; i ++)
    {
        ///--[Setup]
        //--Get the current byte position in the string, which may not be the same as the letter position
        //  if UTF characters are in it.
        int tActualBytePos = u8_offset(pString, i);

        //--Get the letter in question. If it's a space, store its information. UTF characters can't have
        //  overlap with ASCII characters so this code won't present a problem with UTF letters.
        char tCheckLetter = pString[tActualBytePos];
        if(tCheckLetter == ' ')
        {
            tLastSpaceMem = tCurAppendIndex;
            tLastSpaceChar = i;
        }

        //--Get the letter in question.
        //uint32_t tLetter = utf8_nextletter(pString, i);

        //--Run the special sequence handler on the byte position. This only occurs if breakpoints are enabled.
        //  These are \n and [BR] cases.
        int tToSkip = -1;
        if(pFlags & SUBDIVIDE_FLAG_COMMON_BREAKPOINTS)
        {
            tToSkip = HandleSpecialSequences(&pString[tActualBytePos]);
        }

        ///--[Newline from Special Sequence]
        //--If the skip value is not -1, then we encountered something that should end the current line.
        if(tToSkip != -1)
        {
            DebugPrint("Hit breakpoint at %i\n", i);
            //--Get how many bytes are in the current buffer. If it's zero, save a string with a single space.
            if(tCurAppendIndex == 0)
            {
                //--Blank line.
                char *nNewLine = InitializeString(" ");
                nStringList->AddElementAsTail("X", nNewLine, &FreeThis);
                DebugPrint("Store: %s\n", tBuffer);

                //--Add the skip value.
                i = i + tToSkip;
                continue;
            }

            //--Allocate a new string and populate it with the buffer.
            char *nNewLine = InitializeString(tBuffer);
            nStringList->AddElementAsTail("X", nNewLine, &FreeThis);
            DebugPrint("Store: %s\n", tBuffer);

            //--Reset the buffer.
            tCurAppendIndex = 0;
            tBuffer[0] = '\0';
            tLastSpaceMem = 0;
            tLastSpaceChar = 0;

            //--Add the skip onto the current letter position
            i = i + tToSkip;

            //--Ignore line length checks.
            continue;
        }

        ///--[Append Letter]
        //--The skip was -1, so no special sequences were detected or were enabled. Append the letter to the buffer.
        int tByteDif = 0;

        //--This was the last letter, so the final byte length is used for the last position.
        if(i == cCharLen - 1)
        {
            tByteDif = cMemLen - tActualBytePos;
        }
        //--Get the next letter's byte position to resolve how big this one is.
        else
        {
            int tNextBytePos = u8_offset(pString, i+1);
            tByteDif = tNextBytePos - tActualBytePos;
        }

        //--Append the requested number of bytes from the base string to the target string.
        int tIndexBeforeAppend = tCurAppendIndex;
        for(int p = 0; p < tByteDif; p ++)
        {
            tBuffer[tCurAppendIndex] = pString[tActualBytePos];
            tCurAppendIndex ++;
            tActualBytePos ++;
        }
        tBuffer[tCurAppendIndex] = '\0';

        ///--[Check Length for Overages]
        //--Once appendation is done, check the length of the line. If it's over the provided length,
        //  we need to create a breakpoint here. This requires a flag to be enabled. It is not possible
        //  for the font to be invalid at this point as the function error checks that at the start.
        if(!(pFlags & SUBDIVIDE_FLAG_FONTLENGTH)) continue;

        //--Get the line length. If not over the length, stop.
        float tLineLength = pFont->GetTextWidth(tBuffer);
        if(tLineLength < pLength) continue;
        DebugPrint("Hit overrun at %i\n", i);

        ///--[No Backtracking]
        //--"Backtracking" is where we move back to the last ASCII space and use that as the breakpoint.
        //  This makes words not get cut off in the middle of rendering. It can be disabled with a flag.
        //--If backtracking is not enabled, then remove the last appended letter from the buffer and
        //  start a new line. In addition, if backtracking is enabled but no space was detected on this
        //  line, backtracking will not occur.
        if(!(pFlags & SUBDIVIDE_FLAG_ALLOW_BACKTRACK) || !tLastSpaceMem)
        {
            //--Reset the buffer's initial append back to 0. This undoes the last letter appended.
            tBuffer[tIndexBeforeAppend] = '\0';

            //--Copy the existing buffer into a new line. InitializeString() will automatically drop any
            //  extra bytes past tIndexBeforeAppend in case of a UTF letter.
            char *nNewLine = InitializeString(tBuffer);
            nStringList->AddElementAsTail("X", nNewLine, &FreeThis);
            DebugPrint("Store: %s\n", tBuffer);

            //--Reset the buffer.
            tCurAppendIndex = 0;
            tBuffer[0] = '\0';
            tLastSpaceMem = 0;
            tLastSpaceChar = 0;

            //--Subtract one letter.
            i --;
            continue;
        }

        ///--[Backtracking]
        //--Backtracking is enabled. The last ASCII space should be stored, use that as the breakpoint. If
        //  no space is stored, then the above no-backtrack case will have triggered.
        //--If the last letter added was a space, then we don't need to do anything since a space won't actually
        //  render over the edge.
        DebugPrint("Backtracking.\n", i);
        if(tLastSpaceChar != i)
        {
            DebugPrint("Last space was at %i %i\n", tLastSpaceChar, tLastSpaceMem);
            i = tLastSpaceChar;
            tBuffer[tLastSpaceMem] = '\0';
        }
        //--Otherwise, move the buffer position back.
        else
        {
        }

        //--New line.
        char *nNewLine = InitializeString(tBuffer);
        nStringList->AddElementAsTail("X", nNewLine, &FreeThis);
        DebugPrint("Store: %s\n", tBuffer);

        //--Reset the buffer.
        tCurAppendIndex = 0;
        tBuffer[0] = '\0';
        tLastSpaceMem = 0;
        tLastSpaceChar = 0;
    }

    ///--[ ========== Remaining Data ========== ]
    //--If there is anything left in the buffer at the end of parsing, put it on the end of the linked list.
    if(tCurAppendIndex > 0)
    {
        char *nNewLine = InitializeString(tBuffer);
        nStringList->AddElementAsTail("X", nNewLine, &FreeThis);
        DebugPrint("Store: %s\n", tBuffer);
    }

    ///--[ ============ Finish Up ============= ]
    free(tBuffer);
    DebugPop("Subdivide finished normally.\n");
    return nStringList;
}
int Subdivide::HandleSpecialSequences(const char *pString)
{
    ///--[Documentation]
    //--Given a string, checks to see if that string starts with a special sequence. If it does, returns an integer
    //  representing how many letters the parser should skip, and it is assumed a breakpoint was achieved. If no
    //  special sequences exist, returns -1.
    //--All special sequences are in ASCII so UTF is not checked here.
    int tLength = (int)strlen(pString);

    ///--[Case Handler]
    //--\n and the ASCII equivalent will cause a newline case. Parsing skips one letter.
    if(tLength >= 2 && pString[0] == '\\' && pString[1] == 'n')
    {
        return 1;
    }
    //--Ascii equivalent, which is 10 or 13 depending on the system.
    else if(tLength >= 1 && pString[0] == 10)
    {
        return 0;
    }
    //--[BR], same as \n but is translation-friendly.
    else if(tLength >= 4 && !strncasecmp(pString, "[BR]", 4))
    {
        return 3;
    }

    //--No special sequences.
    return -1;
}
