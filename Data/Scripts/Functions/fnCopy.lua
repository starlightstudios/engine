-- |[ ======================================== fnCopy() ======================================== ]|
--Guarantees a shallow copy is made of the variable in question. This is to bypass Lua's frustrating
-- tendency to pass-by-reference when you're trying to make a flippin' copy. Bad language design, lua!
function fnCopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end