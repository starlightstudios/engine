///========================================= MagicPack ============================================
//--A package used by the CorrupterMenu. Represents a magic spell, which can either be cast from the
//  magic menu, or from the context menu. Spells that are cast from the context menu still cost MP,
//  and their description can be seen on the CorrupterMenu.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
typedef struct
{
    //--Target Info
    void *rTargetPtr;
    MagicPack *rCallingPtr;
}MagicContextPack;

///===================================== Local Definitions ========================================
#define MAGIC_TARGET_NONE 0x00
#define MAGIC_TARGET_ACTOR 0x01
#define MAGIC_TARGET_ITEM 0x02
#define MAGIC_TARGET_CONTAINER 0x04

#define MAGIC_SCRIPT_CONSTRUCTOR 0
#define MAGIC_SCRIPT_CONTEXTCANTARGET 1
#define MAGIC_SCRIPT_MENUCANCAST 2
#define MAGIC_SCRIPT_CAST 3

#define MAGIC_QUERY_ACTOR 0
#define MAGIC_QUERY_ITEM 1
#define MAGIC_QUERY_CONTAINER 2

///========================================== Classes =============================================
class MagicPack : public RootObject
{
    private:
    //--System
    char *mLocalName;

    //--Storage
    int mMPCost;
    char *mScriptPath;
    char *mDescription;

    //--Context Menu
    bool mIsCastFromContextMenu;
    uint8_t mContextTargetFlags;

    //--Corrupter Menu
    bool mCanCastNow;
    bool mIsCastFromCorrupterMenu;

    protected:

    public:
    //--System
    MagicPack();
    virtual ~MagicPack();

    //--Public Variables
    static bool xCanTargetThis;
    static void *xrTargetObject;
    static StarLinkedList *xMagicList;

    //--Property Queries
    const char *GetName();
    const char *GetDescription();
    int GetMPCost();
    bool IsCastFromContextMenu();
    bool IsCastFromCorrupterMenu();
    bool CanCastNow();

    //--Manipulators
    void SetName(const char *pName);
    void SetDescription(const char *pString);
    void SetScript(const char *pPath);
    void SetMPCost(int pCost);
    void SetContextMenuFlags(uint8_t pCastFlags);
    void SetCorrupterMenuFlag(bool pIsCastFromMenu);

    //--Core Methods
    void RecheckMenuCast();
    bool AppearsOnContextActor(Actor *pTarget);
    bool AppearsOnContextItem(InventoryItem *pTarget);
    bool AppearsOnContextContainer(WorldContainer *pTarget);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Cast(void *pTarget);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static void ExecuteMagicCommand(void *pDataPtr);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Magic_CreateSpell(lua_State *L);
int Hook_Magic_ClearSpells(lua_State *L);
int Hook_Magic_GetProperty(lua_State *L);
int Hook_Magic_SetProperty(lua_State *L);
int Hook_Magic_PushTarget(lua_State *L);
