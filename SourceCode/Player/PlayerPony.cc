//--Base
#include "PlayerPony.h"

//--Classes
#include "LuaContextOption.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarCamera2D.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"
#include "EntityClipFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "EntityManager.h"
#include "MapManager.h"

//=========================================== System ==============================================
PlayerPony::PlayerPony()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PLAYER;

    //--[RootEntity]
    //--System
    ResetString(mLocalName, "Player");

    //--[PlayerPony]
    //--System
    //--Controls
    mLockoutList = new StarLinkedList(true);

    //--Mouse Handling
    mClickBeginX = -100;
    mClickBeginY = -100;

    //--Special Context Options
    mContextOptionList = new StarLinkedList(true);
}
PlayerPony::~PlayerPony()
{
    delete mLockoutList;
    delete mContextOptionList;
}
void PlayerPony::Construct()
{
    //--Called once at program start. If the player has any graphics or tables unique to it, they
    //  get built or loaded here.
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void PlayerPony::RegisterContextOption(const char *pPath)
{
    //--Creates a new context option and initializes it. Note that its name will be set by the
    //  initialization script.
    if(!pPath) return;

    //--Create.
    LuaContextOption *nOption = new LuaContextOption();
    nOption->SetControlScript(pPath);

    //--Register.
    mContextOptionList->AddElement(nOption->GetName(), nOption, &RootObject::DeleteThis);
}

//===================================== Private Core Methods ======================================
//========================================= Core Methods ==========================================
void PlayerPony::HandleCamera(float &sLft, float &sTop)
{
    //--Called by some instances of the camera. The lft/top is the ideal position of the camera when
    //  the player is the focused object. If the player is not the focus object, this is not called.
}
void PlayerPony::WipeContextCommands()
{
    mContextOptionList->ClearList();
}
void PlayerPony::Reset(const char *pResetString)
{
    //--Reset. Called by the EM, not the IResettable.
    if(!pResetString) return;

    //--The Full Reset is only called at the end of a tick. It wipes the EM.
    if(!strcasecmp(pResetString, "Full Game Reset"))
    {
        mContextOptionList->ClearList();
    }
    //--Unhandled case.
    else
    {
    }

    //--General.
    mLockoutList->ClearList();
    mClickBeginX = -100;
    mClickBeginY = -100;
}

//======================================= Pointer Routing =========================================
StarLinkedList *PlayerPony::GetContextCommandList()
{
    return mContextOptionList;
}

//====================================== Static Functions =========================================
PlayerPony *PlayerPony::Fetch()
{
    return EntityManager::Fetch()->GetPlayer();
}

//========================================= Lua Hooking ===========================================
void PlayerPony::HookToLuaState(lua_State *pLuaState)
{

}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
