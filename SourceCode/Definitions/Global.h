///========================================== Global ==============================================
//--Contains objects and variables whose scope is the entirety of the program.  They should also
//  have a duration of the entire program's execution, though the managers in particular must be
//  built in a specific order, as there are some interreliances.

#pragma once

#include "Definitions.h"
#include "Structures.h"
typedef void(*EventHandlerFnPtr)(MainPackage &);

typedef struct
{
    ///--[Allegro Variables]
    #if defined _ALLEGRO_PROJECT_
        ALLEGRO_DISPLAY *gDisplay;
        ALLEGRO_EVENT_SOURCE gCustomEventSource;
        ALLEGRO_EVENT_QUEUE *gEventQueue;
        ALLEGRO_EVENT gEvent;
        ALLEGRO_TIMEOUT gSpeedCounter;
        ALLEGRO_TIMER *gSpeedTimer;
    ///--[SDL Variables]
    #elif defined _SDL_PROJECT_
        SDL_Event gEvent;
    #endif

    ///--[Generation Pointers]
    TitleMenuCreateFuncPtr rCreateTitleMenu;

    ///--[Static Classes]
    StarLoadInterrupt *gLoadInterrupt;
    StarLinkedList *gFileSystemStack;

    ///--[Managers]
    //--External
    LuaManager *gLuaManager;
    NetworkManager *gNetworkManager;
    SaveManager *gSaveManager;
    SteamManager *gSteamManager;
    StarLumpManager *gStarLumpManager;

    //--Game
    CutsceneManager *gCutsceneManager;
    EntityManager *gEntityManager;
    MapManager *gMapManager;
    ResetManager *gResetManager;

    //--Interface
    AudioManager *gAudioManager;
    CameraManager *gCameraManager;
    ControlManager *gControlManager;
    DisplayManager *gDisplayManager;
    OptionsManager *gOptionsManager;

    //--System
    TranslationManager *gTranslationManager;

    ///--[GUI]
    StarFont *gSystemFont;
    StarFont *gBitmapFont;

    ///--[Libraries]
    DataLibrary *gDataLibrary;

    ///--[Primitives]
    //--Display Variables
    int gWindowWidth;
    int gWindowHeight;
    int gScreenWidthPixels;
    int gScreenHeightPixels;
    int gRenderedPixelsW;
    int gRenderedPixelsH;
    char *gProgramPath;

    //--Program Variables
    bool gQuit;
    bool gReset;
    bool gFastTitle;
    int mResetKeyPresses;
    int mResetKeyCooldown;

    //--FPS/TPS trackers
    float gTickStartTime;
    float gTickWarnTime;
    uint32_t gTicksElapsed;
    float gExpectedFPS;
    float gActualFPS;
    float gActualTPS;
    bool gWriteFPS;

    //--Debug
    bool gBootWithJoystick;
    char gVersionString[STD_MAX_LETTERS];

    //--Engine Options
    bool gDisallowPerlinNoise;
    bool gSkipPairanormalDisclaimer;

    ///--[Debug Variables]
    //--Used only in the boot sequence before the OptionsManager gets booted.
    bool gShowStartupDebug;

    ///--[Debug Prints]
    //--Variables set in "DebugDefinitions.h". Space is allocated during startup.
    char **gDebugPrintStrings;

    ///--[Function Pointers for Gears]
    EventHandlerFnPtr  rCurrentEventHandler;
    LogicHandlerFnPtr  rCurrentLogicHandler;
    RenderHandlerFnPtr rCurrentRenderHandler;

    ///--[Game Registry]
    StarLinkedList *gGameRegistry;
}GLOBAL;

///--[Static Singleton]
class Global
{
    private:
    //--Tracks the UniqueID counter
    //--Note:  The Global's ID is 0, so it will increment to 1 the first time it is used.
    //  An invalid UniqueID is thus 0, as it is unsigned.
	Global()
	{
	}

    public:
	~Global()
	{
	}

	static GLOBAL *Shared()
	{
        static GLOBAL Global;
		return &Global;
	}
	static uint32_t GetNextID()
	{
        static uint32_t mUniqueIDCounter = 0;
        mUniqueIDCounter ++;
	    return mUniqueIDCounter;
	}
};
