//--Base
#include "CarnUIOptions.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
//--Managers

///========================================= Functions ============================================
void CarnMenuOptionPack::Initialize()
{
    mDisplayName = NULL;
    mEditType = CMOP_BOOLEAN;
    mValueStrings = new StarLinkedList(true);
    mRangeLo = 0.0f;
    mRangeHi = 1.0f;
    mOldValue.i = 0;
    mCurValue.i = 0;
    mRequiresRestart = false;
    mDescriptionList = NULL;
}
void CarnMenuOptionPack::SetName(const char *pName)
{
    ResetString(mDisplayName, pName);
}
void CarnMenuOptionPack::SetI(int pValue)
{
    mOldValue.i = pValue;
    mCurValue.i = pValue;
}
void CarnMenuOptionPack::SetF(float pValue)
{
    mOldValue.f = pValue;
    mCurValue.f = pValue;
}
void CarnMenuOptionPack::SetB(bool pValue)
{
    mOldValue.b = pValue;
    mCurValue.b = pValue;
}
void CarnMenuOptionPack::SetDescription(const char *pString, StarFont *pFont, float pMaxWidth)
{
    ///--[Documentation]
    //--Subdivides the given string into a linked list for storage.
    if(!pString) return;

    //--Clear existing description.
    delete mDescriptionList;
    mDescriptionList = NULL;

    //--Set.
    mDescriptionList = Subdivide::SubdivideStringFlags(pString, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, pFont, pMaxWidth);
    CarnUICommon::ReplaceCommonTagsInList(mDescriptionList);
}
void CarnMenuOptionPack::DeleteThis(void *pPtr)
{
    CarnMenuOptionPack *rPtr = (CarnMenuOptionPack *)pPtr;
    if(!rPtr) return;

    free(rPtr->mDisplayName);
    delete rPtr->mValueStrings;
    delete rPtr->mDescriptionList;
    free(rPtr);
}
