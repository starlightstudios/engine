//--Base
#include "MonoUIMap.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== System ==============================================
MonoUIMap::MonoUIMap()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIMap ======= ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUIMap ====== ]
    ///--[System]
    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIMap::~MonoUIMap()
{
}
void MonoUIMap::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Map UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Map UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",          &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIMap_Strings *MonoUIMap::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIMap_Strings::MonoUIMap_Strings()
{
    //--Local name: "MonoUIBase"
    MonoUIMap::xrStringData = this;

    //--Strings
    str.mTextMap = InitializeString("MonoUIMap_mMapHeading"); //"Map"
}
MonoUIMap_Strings::~MonoUIMap_Strings()
{
    for(int i = 0; i < MONOUIMAP_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIMap_Strings::GetSize()
{
    return MONOUIMAP_STRINGS_TOTAL;
}
void MonoUIMap_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIMAP_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIMap::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Simple switch between map rendering types. Common fade handling is also done.
    //--Fade percentage. Renders a fadeout for the first half, and a fadein for the second half.
    float cFadePct = EasingFunction::QuadraticOut(mVisibilityTimer, ADVMENU_MAP_VIS_TICKS);

    ///--[Fullblack]
    //--Don't render anything except a black fadeout if below half on the fade percent.
    if(cFadePct < 0.50f)
    {
        StarBitmap::DrawFullBlack(cFadePct * 2.0f);
        return;
    }

    //--When using advanced rendering, render a black underlay.
    StarBitmap::DrawFullBlack(1.0f);

    ///--[Subrender Switch]
    //--Basic map, no layers or special effects.
    if(mIsUsingBasicMap)
    {
        RenderBasicMapMode();
    }
    //--Render the advanced image set.
    else
    {
        RenderAdvancedMapMode();
    }

    ///--[Overlay]
    //--If there is an overlay, render it with a fixed position.
    if(rBasicOverlay) rBasicOverlay->Draw();

    ///--[Header]
    //--Header text is yellow.
    SetColor("Title", pVisAlpha);
    MonoImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextMap);
    SetColor("Clear", pVisAlpha);

    ///--[Help Strings]
    ///--[Fading]
    //--If we got this far, render a fullblack fade in.
    if(cFadePct < 1.0f)
    {
        float cAlpha = (cFadePct - 0.50f) * 2.0f;
        StarBitmap::DrawFullBlack(1.0f - cAlpha);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
