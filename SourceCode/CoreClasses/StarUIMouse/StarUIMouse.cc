//--Base
#include "StarUIMouse.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
StarUIMouse::StarUIMouse()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ====== StarUIMouse ======= ]
    ///--[System]
    mMouseMoved = false;
    mMouseX = -1;
    mMouseY = -1;
    mMouseZ = 0;
    mMouseZPrev = 0;

    ///--[Hitboxes]
    mHitboxList = new StarLinkedList(true);
    mHitboxReplyList = new StarLinkedList(true);

    ///--[Scrollbars]
    mClickStartY = 0;
    mClickIncY = 1;
    rClickedScrollbar = NULL;
    rWheelScrollbar = NULL;
    mScrollbarList = new StarLinkedList(true);

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
StarUIMouse::~StarUIMouse()
{
    delete mHitboxList;
    delete mHitboxReplyList;
    delete mScrollbarList;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void StarUIMouse::RegisterHitbox(const char *pName, float pLft, float pTop, float pRgt, float pBot)
{
    ///--[Documentation]
    //--Creates and registers a hitbox. Name can legally be NULL, an "X" will be used instead.

    //--Create.
    TwoDimensionReal *nHitbox = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
    nHitbox->Set(pLft, pTop, pRgt, pBot);

    //--Register.
    if(!pName)
    {
        mHitboxList->AddElementAsTail("X", nHitbox, &FreeThis);
    }
    else
    {
        mHitboxList->AddElementAsTail(pName, nHitbox, &FreeThis);
    }
}
void StarUIMouse::RegisterHitboxWH(const char *pName, float pLft, float pTop, float pWid, float pHei)
{
    ///--[Documentation]
    //--Same as above but uses width/height.
    RegisterHitbox(pName, pLft, pTop, pLft + pWid, pTop + pHei);
}

///======================================= Core Methods ===========================================
void StarUIMouse::RunHitboxCheck()
{
    ///--[Documentation]
    //--Checks all hitboxes in mHitboxList against the mouse. Every hitbox will have an (int *) added
    //  to the mHitboxReplyList in order of which was hit.
    mHitboxReplyList->ClearList();

    ///--[Iterate]
    //--Run across all hitboxes and populate the return list.
    int i = 0;
    TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->PushIterator();
    while(rHitbox)
    {
        //--On hit:
        if(rHitbox->IsPointWithin(mMouseX, mMouseY))
        {
            //--Create and register a reply package.
            HitboxReplyPack *nPack = (HitboxReplyPack *)starmemoryalloc(sizeof(HitboxReplyPack));
            nPack->Initialize();
            mHitboxReplyList->AddElementAsTail("X", nPack, &FreeThis);

            //--Set data.
            nPack->mIndex = i;
            nPack->rName = mHitboxList->GetIteratorName();
            nPack->rHitbox = rHitbox;
        }

        //--Next.
        i ++;
        rHitbox = (TwoDimensionReal *)mHitboxList->AutoIterate();
    }
}
int StarUIMouse::CheckRepliesWithin(int pRangeLo, int pRangeHi)
{
    ///--[Documentation]
    //--An extremely common behavior when hitbox checking is that all hitboxes within a specific range
    //  are checked and the rest are ignored. For example, the code might check all hitboxes referring
    //  to items, and ignore the close/use buttons, on an inventory.
    //--This routine checks all replies within the given range and returns the first one found. If no
    //  reply is found within the range, returns -1.
    //--Only the first reply found will be handled, as these are usually exclusive. Sometimes a scrollbar
    //  might overlay with another hitbox, it is up to the coder to decide what order to handle these in.

    ///--[Execution]
    //--Iterate across all replies.
    HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
    while(rReplyPack)
    {
        //--If it's in the selection range:
        if(rReplyPack->mIndex >= pRangeLo && rReplyPack->mIndex <= pRangeHi)
        {
            mHitboxReplyList->PopIterator();
            return rReplyPack->mIndex;
        }

        //--Next.
        rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
    }

    //--No hits within range.
    return -1;
}

///===================================== Scrollbar Routines =======================================
int StarUIMouse::ComputeScrollbarHit(int pMouseX, int pMouseY, int pSkip, int pPerPage, int pMax, TwoDimensionReal pHitbox)
{
    ///--[Documentation]
    //--Checks what percentage along a hitbox the mouse position is vertically, then checks that against
    //  the skip/size/max values and returns one of the STARMOUSE_SCROLL_NOHIT series to determine where
    //  the click was on a normal scrollbar.

    ///--[No Hit]
    //--If the mouse isn't in the hitbox, stop.
    if(!pHitbox.IsPointWithin(pMouseX, pMouseY)) return STARMOUSE_SCROLL_NOHIT;

    //--Also fail if the per-page or max were zero to avoid div0 errors.
    float cHitboxHei = pHitbox.GetHeight();
    if(pPerPage < 1 || pMax < 1 || cHitboxHei < 1.0f) return STARMOUSE_SCROLL_NOHIT;

    ///--[Computations]
    //--Compute where the breakpoints on the scrollbar are. Note that the max value is
    //  the max of what pSkip can be, not what the bottom-most value in a list is.
    //  We need to calc a true max from max+pagesize.
    int cTrueMax = pMax + pPerPage;
    float cHitPct = (pMouseY - pHitbox.mTop) / cHitboxHei;
    float cTopPct = pSkip / (float)cTrueMax;
    float cBotPct = (pSkip + pPerPage) / (float)cTrueMax;

    //--Above:
    if(cHitPct < cTopPct) return STARMOUSE_SCROLL_ABOVE;

    //--Below:
    if(cHitPct > cBotPct) return STARMOUSE_SCROLL_BELOW;

    //--Hit was on top of the scrollbar.
    return STARMOUSE_SCROLL_ON;
}
bool StarUIMouse::UpdateScrollbarMouseLeft(ScrollbarPack *pPackage)
{
    ///--[Documentation]
    //--Updates a scrollbar being clicked. Returns true if the scrollbar handled the update, false if not.
    if(!pPackage) return false;

    //--Package does not have a skip value, do nothing.
    if(!pPackage->rSkipPtr) return false;

    //--If there is a clicked scrollbar, do nothing, even if it's this scrollbar.
    if(rClickedScrollbar) return false;

    ///--[Button Clicks]
    //--Scan to see if the up/down buttons were clicked. If they were, edit the skip value.
    HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
    while(rReplyPack)
    {
        //--Fast-access.
        int tIndex = rReplyPack->mIndex;
        int tSkipValue = *pPackage->rSkipPtr;

        //--If the index was on the scrollbar itself, check to see if the click was above, below, or on the bar.
        int tScrollClick = STARMOUSE_SCROLL_NOHIT;
        if(tIndex == pPackage->mIndexBd && rReplyPack->rHitbox)
        {
            tScrollClick = ComputeScrollbarHit(mMouseX, mMouseY, tSkipValue, pPackage->mPerPage, pPackage->mMaxSkip, *rReplyPack->rHitbox);
        }

        //--Click was on the scrollbar-up button.
        if(tIndex == pPackage->mIndexUp || tScrollClick == STARMOUSE_SCROLL_ABOVE)
        {
            //--Decrease.
            tSkipValue --;
            if(tSkipValue < 0) tSkipValue = 0;
            *pPackage->rSkipPtr = tSkipValue;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mHitboxReplyList->PopIterator();
            return true;
        }
        //--Click was on the scrollbar-down button.
        else if(tIndex == pPackage->mIndexDn || tScrollClick == STARMOUSE_SCROLL_BELOW)
        {
            //--Increase.
            tSkipValue ++;
            if(tSkipValue >= pPackage->mMaxSkip) tSkipValue = pPackage->mMaxSkip;
            *pPackage->rSkipPtr = tSkipValue;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mHitboxReplyList->PopIterator();
            return true;
        }
        //--Click was on the scrollbar.
        else if(tScrollClick == STARMOUSE_SCROLL_ON)
        {
            //--Store this as the clicked object.
            mClickStartY = mMouseY;
            mClickIncY = 1;
            rClickedScrollbar = pPackage;

            //--Compute how many pixels need to be moved to modify the skip. This is based on the scrollbar size.
            int tEntriesOnBar = pPackage->mMaxSkip + pPackage->mPerPage;
            float tHei = rReplyPack->rHitbox->GetHeight();
            if(tHei >= 1.0f && tEntriesOnBar > 0)
            {
                mClickIncY = tHei / tEntriesOnBar;
            }

            //--Extra: Make sure the increment is at least 1.
            if(mClickIncY < 1) mClickIncY = 1;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mHitboxReplyList->PopIterator();
            return true;
        }

        //--Next.
        rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
    }

    return false;
}


///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool StarUIMouse::GetMouseInfo()
{
    ///--[Documentation]
    //--Requests and stores mouse information into this local object. Returns true if the mouse moved,
    //  false if it did not. "Moved" includes the mouse wheel.
    int tOldMouseX = mMouseX;
    int tOldMouseY = mMouseY;
    int tOldMouseZ = mMouseZ;
    mMouseZPrev = mMouseZ;

    //--Retrieve.
    ControlManager::Fetch()->GetMouseCoords(mMouseX, mMouseY, mMouseZ);

    //--If anything changed, returns true.
    mMouseMoved = (mMouseX != tOldMouseX || mMouseY != tOldMouseY || mMouseZ != tOldMouseZ);
    return mMouseMoved;
}
bool StarUIMouse::UpdateScrollbars()
{
    ///--[Documentation]
    //--Automated scrollbar handling, should be called before anything else during updates where
    //  a scrollbar is active. This handles checking if the scrollbars were clicked, click/drag on
    //  the scrollbar, and release checking.
    //--Returns true if it caught the update, false if not.
    //--The caller should already have captured mouse information before calling this.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Mousewheel]
    if(mMouseZPrev != mMouseZ && rWheelScrollbar && rWheelScrollbar->rSkipPtr)
    {
        //--Double-check that the wheel scrollbar exists. If it does not, clear it.
        if(!mScrollbarList->IsElementOnList(rWheelScrollbar))
        {
            rWheelScrollbar = NULL;
            return true;
        }

        //--Fast-access.
        int tSkipValue = *rWheelScrollbar->rSkipPtr;

        //--Scrollbar decrements.
        if(mMouseZ > mMouseZPrev)
        {
            //--Decrease.
            tSkipValue --;
            if(tSkipValue < 0) tSkipValue = 0;
            *rWheelScrollbar->rSkipPtr = tSkipValue;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mHitboxReplyList->PopIterator();
            return true;
        }
        //--Scrollbar increments.
        else if(mMouseZ < mMouseZPrev)
        {
            //--Increase.
            tSkipValue ++;
            if(tSkipValue >= rWheelScrollbar->mMaxSkip) tSkipValue = rWheelScrollbar->mMaxSkip;
            *rWheelScrollbar->rSkipPtr = tSkipValue;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mHitboxReplyList->PopIterator();
            return true;
        }
    }

    ///--[Mouse Over]
    //--If a scrollbar is currently being clicked and dragged, handle the update.
    if(rClickedScrollbar)
    {
        //--If the left mouse was released, unset the click and stop the update.
        if(!rControlManager->IsDown("MouseLft"))
        {
            rClickedScrollbar = NULL;
            return true;
        }

        //--Otherwise, the mouse is down so handle the movement. Handle moving up:
        while(mMouseY <= mClickStartY - mClickIncY && *rClickedScrollbar->rSkipPtr > 0)
        {
            //--Decrement the skip value.
            *rClickedScrollbar->rSkipPtr = *rClickedScrollbar->rSkipPtr - 1;

            //--Decrement the click start.
            mClickStartY -= mClickIncY;
        }

        //--Handle moving down.
        while(mMouseY >= mClickStartY + mClickIncY && *rClickedScrollbar->rSkipPtr < rClickedScrollbar->mMaxSkip)
        {
            //--Decrement the skip value.
            *rClickedScrollbar->rSkipPtr = *rClickedScrollbar->rSkipPtr + 1;

            //--Decrement the click start.
            mClickStartY += mClickIncY;
        }

        //--The update is considered handled.
        return true;
    }

    ///--[Mouse Left Click]
    //--Iterate across the scrollbar packs, checking if any of them caught the update.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        ScrollbarPack *rScrollbar = (ScrollbarPack *)mScrollbarList->PushIterator();
        while(rScrollbar)
        {
            //--Run routine. If it returns true, stop.
            if(UpdateScrollbarMouseLeft(rScrollbar))
            {
                mScrollbarList->PopIterator();
                return true;
            }

            //--Next.
            rScrollbar = (ScrollbarPack *)mScrollbarList->AutoIterate();
        }
    }

    ///--[No Handling]
    //--None of the scrollbar packages caught the update.
    return false;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
