//--Base
#include "AdvUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "LuaManager.h"

///================================= Editing Global Properties ====================================
void AdvUIVendor::SetGemcutterFlag(bool pFlag)
{
    ///--[Documentation]
    //--Called during shop setup. By default, most vendors do not handle gemcutting. Set this flag
    //  to true to allow the vendor to handle gems.
    mAllowGemcutting = pFlag;
}
void AdvUIVendor::SetBuyResponseScript(const char *pPath)
{
    ///--[Documentation]
    //--The buy response flag is the script called when the player buys something. This script doesn't need to exist,
    //  if it does it may manipulate the inventory after the item is created. If it does it flips a flag.
    //--This is used when the player buys something abstract using the vendor interface, such as in Project Monoceros,
    //  the player can buy level-ups for the party.
    //--Pass NULL or "NULL" to clear the response.
    if(!pPath || !strcasecmp(pPath, "NULL"))
    {
        ResetString(mBuyResponseScript, NULL);
        return;
    }

    //--Normal case.
    ResetString(mBuyResponseScript, pPath);
}
void AdvUIVendor::ScrollMode(int pAmount)
{
    ///--[Documentation]
    //--Typically in response to pressing left or right, increments or decrements the mode by the
    //  requested amount. For example, in Buy mode, pressing right increments by 1 to Sell mode.
    //  This routine handles the change, sound effects, and calling needed functions.
    //--pAmount is only used for its sign, increment or decrement.
    if(pAmount == 0) return;

    ///--[Lockout]
    //--If this flag is set, modes cannot scroll.
    if(mIsBuyOnly) return;

    ///--[Mode Switching]
    if(mIsBuyMode)
    {
        if(pAmount > 0)
            ActivateModeSell();
        else
            ActivateModeGems();
    }
    else if(mIsSellMode)
    {
        if(pAmount > 0)
            ActivateModeBuyback();
        else
            ActivateModeBuy();
    }
    else if(mIsBuybackMode)
    {
        if(pAmount > 0)
            ActivateModeGems();
        else
            ActivateModeSell();
    }
    else if(mIsGemsMode)
    {
        if(pAmount > 0)
            ActivateModeBuy();
        else
            ActivateModeBuyback();
    }

    ///--[SFX]
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
}

///==================================== Register/Unregister =======================================
void AdvUIVendor::RegisterItem(const char *pName, int pPriceOverride, int pQuantity)
{
    ///--[Documentation]
    //--Registers an item for sale. Items are cloned when purchased. If the price override value is
    //  -1, then the price is the default value for the item. If the quantity is -1, then the item
    //  may be purchased an unlimited number of times.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    if(!pName || !AdventureLevel::xItemListPath) return;

    //--Run the item list code to create the item in question.
    rInventory->xIsRegisteringItemsToShop = true;
    rInventory->rLastReggedItem = NULL;
    rInventory->mBlockStackingOnce = true;
    LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", pName, "N", 1.0f);
    rInventory->mBlockStackingOnce = false;
    rInventory->xIsRegisteringItemsToShop = false;

    //--Unregister the item that was last created and add it to our local list instead.
    AdventureItem *rLastItem = (AdventureItem *)rInventory->rLastReggedItem;
    if(!rLastItem) return;
    rInventory->LiberateItemP(rLastItem);

    //--Create a package to hold the item.
    SetMemoryData(__FILE__, __LINE__);
    ShopInventoryPack *nPackage = (ShopInventoryPack *)starmemoryalloc(sizeof(ShopInventoryPack));
    nPackage->Initialize();
    nPackage->mItem = rLastItem;
    nPackage->mPriceOverride = pPriceOverride;
    nPackage->mQuantity = pQuantity;
    nPackage->mOwnedDisplay = 0;

    //--All checks passed, register.
    mShopInventory->AddElementAsTail(nPackage->mItem->GetName(), nPackage, &ShopInventoryPack::DeleteThis);
}
void AdvUIVendor::UnregisterItem(const char *pName)
{
    ///--[Documentation]
    //--Removes an item from the sale list. This can be called from the buy response script to dynamically
    //  trim the menu or do other special things.
    mShopInventory->RemoveElementS(pName);
}

/// [ =============================== Purchase/Equip Routines =============================== ] ///
void AdvUIVendor::PurchaseItem(ShopInventoryPack *pInvPack, int pQuantity, bool pAllowScriptResponse, bool pAllowAskEquipCheck)
{
    ///--[Documentation]
    //--Called when the user accepts a purchase of an item.
    if(!pInvPack || pQuantity < 1) return;

    //--Pack must contain an item.
    AdventureItem *rItem = pInvPack->mItem;
    if(!rItem) return;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Flags.
    mIsBuyingItem = false;
    mAskToEquip = false;

    ///--[Item Creation]
    //--For each time the item was purchased:
    for(int i = 0; i < pQuantity; i ++)
    {
        //--Tell the owning package to increase the quantity.
        pInvPack->mOwnedDisplay ++;

        //--Put the item in the player's inventory if it was not adamantite.
        if(!rItem->IsAdamantite())
        {
            //--Simply run the construction script again.
            const char *rItemName = rItem->GetName();
            LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", rItemName, "N", 1.0f);
        }
        //--Adamantite. Has a special registering algorithm. If the item was flagged as "UNLIMITED" then we won't deallocate it.
        else
        {
            rInventory->RegisterAdamantite(rItem, false);
        }

        //--Get how much the item costs.
        int tItemCost = rItem->GetValue();
        if(pInvPack->mPriceOverride > -1) tItemCost = pInvPack->mPriceOverride;

        //--Decrement the player's case.
        rInventory->SetPlatina(rInventory->GetPlatina() - tItemCost);

        //--Decrement the available quantity if it's not -1.
        if(pInvPack->mQuantity != -1) pInvPack->mQuantity --;
    }

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

    ///--[Response Handling]
    //--Buy response. If this script is set, fire a script which may manipulate the inventory. The flag
    //  xDisallowEquipFromResponse should be set to true if the script is going to delete the item
    //  we just bought.
    //--The caller can also set a flag to disallow scripts from firing.
    xDisallowEquipFromResponse = false;
    if(mBuyResponseScript && pAllowScriptResponse)
    {
        LuaManager::Fetch()->ExecuteLuaFile(mBuyResponseScript);
    }

    ///--[Ask To Equip]
    //--If blocked for some reason, don't ask the player to equip the item.
    if(!pAllowAskEquipCheck) return;

    //--If the player purchased exactly one item, and the item is equipment that the party can equip,
    //  check if anyone can equip it, and who. The subroutine determines who is queried and how.
    if(pQuantity > 1 || xDisallowEquipFromResponse) return;

    //--Run routine to determine if anyone can equip the item. If it's -1, don't ask.
    mAskToEquipSlot = CheckEquippableFromPurchase(rItem);
    if(mAskToEquipSlot == -1) return;

    //--If we got this far, we should show the equip prompt.
    mAskToEquip = true;
}
int AdvUIVendor::CheckEquippableFromPurchase(AdventureItem *pItem)
{
    ///--[Documentation]
    //--After an item is purchased, check if the party can in some way equip this item. If so, it will
    //  return the active party slot of the character who can equip the item. If not, returns -1.
    if(!pItem) return -1;

    //--Fast-access pointers.
    AdvCombat *rCombat = AdvCombat::Fetch();

    ///--[Execution]
    //--Setup.
    int tAskSlot = -1;

    //--Run across the party.
    for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
    {
        //--Get character.
        AdvCombatEntity *rCharacter = rCombat->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--Can the character equip it?
        if(pItem->IsEquippableBy(rCharacter->GetName()))
        {
            //--If the equip slot is -1, this is the first character that can equip it.
            if(tAskSlot == -1)
            {
                tAskSlot = i;
            }
            //--If it's not -1, then at least two characters can equip the item. Don't show the equip dialogue.
            else
            {
                return -1;
            }
        }
    }

    //--If we got this far, return the ask slot. It may be -1 if nobody could use it, and might be
    //  the slot of the only character who can.
    return tAskSlot;
}
void AdvUIVendor::EquipPurchasedItem()
{
    ///--[Documentation]
    //--Called when the player is in the ask-to-equip interface and confirms they want to equip the item.
    AdvCombatEntity *rEntity = AdvCombat::Fetch()->GetActiveMemberI(mAskToEquipSlot);
    AdventureItem *rLastReggedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
    if(!rEntity || !rLastReggedItem) return;

    ///--[Slot Selection]
    //--Dummy variable.
    int tDummy = 5;

    //--Make a list of all the slots that can accept this item. Weapon alternates are ignored.
    int tEquipSlotsTotal = rEntity->GetEquipmentSlotsTotal();
    StarLinkedList *tSlotList = new StarLinkedList(false);
    for(int i = 0; i < tEquipSlotsTotal; i ++)
    {
        //--Ignore weapon alts.
        if(rEntity->IsWeaponAlternateSlot(i)) continue;

        //--If it can be equipped in this slot, store it.
        const char *rSlotName = rEntity->GetNameOfEquipmentSlot(i);
        if(rLastReggedItem->IsEquippableIn(rSlotName))
        {
            tSlotList->AddElement(rSlotName, &tDummy);
        }
    }

    //--We now have a list of slots that can accept the item. If the list somehow came back zero-length, do nothing.
    if(tSlotList->GetListSize() < 1)
    {
        delete tSlotList;
        return;
    }

    //--If there is exactly one entry, equip to that slot. Done!
    int tUseSlot = -1;
    if(tSlotList->GetListSize() == 1)
    {
    }
    //--Otherwise, check if any of the slots in question are empty. If so, use those.
    else
    {
        for(int i = 0; i < tSlotList->GetListSize(); i ++)
        {
            //--Name.
            const char *rSlotName = tSlotList->GetNameOfElementBySlot(i);

            //--Check if the slot is empty.
            AdventureItem *rCheckItem = rEntity->GetEquipmentBySlotS(rSlotName);
            if(!rCheckItem)
            {
                tUseSlot = i;
                break;
            }
        }
    }

    ///--[Equip Action]
    //--If no slot was selected, use the slot that is being compared against.
    bool tAcceptedItem = false;
    if(tUseSlot == -1)
    {
        tAcceptedItem = rEntity->EquipItemToSlot(mCompareSlot, rLastReggedItem);
    }
    //--Otherwise, equip to the slot.
    else
    {
        const char *rSlotName = tSlotList->GetNameOfElementBySlot(tUseSlot);
        tAcceptedItem = rEntity->EquipItemToSlot(rSlotName, rLastReggedItem);
    }

    //--If flagged, the entity accepted the item.
    if(tAcceptedItem) AdventureInventory::Fetch()->LiberateItemP(rLastReggedItem);

    ///--[Inventory Handling]
    //--Check the last registered item, which will be the item unequipped. If it exists, remove any gems from it.
    //  Gems get passed across by the equip routine, but if the new item has fewer slots, some may be left over.
    AdventureItem *rUnequippedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
    if(rUnequippedItem)
    {
        for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
        {
            AdventureItem *rPrevGem = rUnequippedItem->RemoveGemFromSlot(i);
            if(rPrevGem)
            {
                AdventureInventory::Fetch()->RegisterItem(rPrevGem);
            }
        }
    }

    //--Recompute statistics.
    rLastReggedItem->ComputeStatistics();
    rEntity->ComputeStatistics();

    ///--[External Call]
    //--If a script is specified, call it to handle any post-menu changes.
    SetCloseCode("Change Equipment");

    ///--[Finish Up]
    //--Clean.
    delete tSlotList;
}

///================================= Editing Entry Properties =====================================
void AdvUIVendor::SetItemDisplayName(const char *pName, const char *pDisplayName)
{
    ///--[Documentation]
    //--For various reasons an item may have a different name in the shop than its actual game name. This
    //  allows a registered item to have its display name overridden.
    if(!pName || !pDisplayName) return;

    //--Locate the item.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementByName(pName);
    if(!rPackage) return;

    //--Set.
    ResetString(rPackage->mDisplayName, pDisplayName);
}

///======================================== Categories ============================================
void AdvUIVendor::RegisterCategory(const char *pName, const char *pImgPath)
{
    ///--[Documentation]
    //--Categories are used to help sort the shop inventory. If present they can have item packages registered to them.
    if(!pName || !pImgPath) return;

    //--Duplicate check.
    if(mCategoryList->GetElementByName(pName)) return;

    //--Create, register.
    ShopCategoryPack *nCategory = (ShopCategoryPack *)starmemoryalloc(sizeof(ShopCategoryPack));
    nCategory->Initialize();
    nCategory->rIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);
    mCategoryList->AddElementAsTail(pName, nCategory, &ShopCategoryPack::DeleteThis);
}
void AdvUIVendor::RegisterItemToCategory(const char *pItemName, const char *pCategoryName)
{
    ///--[Documentation]
    //--Adds an already-registered ShopInventoryPack as a reference to the named category, if it exists.
    if(!pItemName || !pCategoryName) return;

    //--Locate the item and category. Both must exist.
    ShopInventoryPack *rItemPack     = (ShopInventoryPack *)mShopInventory->GetElementByName(pItemName);
    ShopCategoryPack  *rCategoryPack = (ShopCategoryPack  *)mCategoryList ->GetElementByName(pCategoryName);
    if(!rItemPack || !rCategoryPack) return;

    //--Add!
    rCategoryPack->mrMembers->AddElementAsTail("X", rItemPack);
}

///======================================= Details Mode ===========================================
bool AdvUIVendor::CommonDetailsUpdate()
{
    ///--[Documentation]
    //--Several modes use the same basic handler for item details. Pressing F2 brings up details mode,
    //  when in details mode all other controls are ignored.
    //--Returns true if details mode is blocking the rest of the update, false if it is not.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Details Mode]
    //--The only allowed controls both exit details mode. Always blocks updates.
    if(mIsShowingDetails)
    {
        //--Cancel handler.
        if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("F2"))
        {
            mIsShowingDetails = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Always block.
        return true;
    }

    ///--[Not Details Mode]
    //--Check if the user pressed F2 to open details mode. If they did, stops the update.
    if(rControlManager->IsFirstPress("F2"))
    {
        mIsShowingDetails = true;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    //--If we got this far, allow the update to proceed.
    return false;
}

///========================================= Help Mode ============================================
///======================================== Quantities ============================================
void AdvUIVendor::AdjustQuantity(int &sQuantity, int pAmount)
{
    ///--[Documentation]
    //--Modifies the given quantity value by the given amount, clamping against the minimum of 1
    //  and the maximum which is either a mandated value or based on how many of something can be
    //  bought or sold. This depends on the mode.
    if(pAmount == 0) return;

    ///--[Get Clamps]
    int cClampLo, cClampHi;
    ResolveQuantityClamps(cClampLo, cClampHi);

    ///--[Modify]
    //--Store.
    int tOriginalQuantity = sQuantity;

    //--Get the absolute value of the amount.
    int tIncrement = 1;
    if(pAmount < 0)
    {
        tIncrement = -1;
        pAmount = pAmount * -1;
    }

    //--Loop iterate. Wrapping is only viable on the first pass.
    for(int i = 0; i < pAmount; i ++)
    {
        //--Add.
        sQuantity = sQuantity + tIncrement;

        //--Range check. On the first pass, wrap.
        if(i == 0)
        {
            if(sQuantity > cClampHi) sQuantity = cClampLo;
            if(sQuantity < cClampLo) sQuantity = cClampHi;
        }
        //--On all successive passes, hitting the clamp does not wrap.
        else
        {
            if(sQuantity > cClampHi) { sQuantity = cClampHi; break; }
            if(sQuantity < cClampLo) { sQuantity = cClampLo; break; }
        }
    }

    //--If after adjustment the quantity is an invalid number, such as zero or negative,
    //  reset and don't play a sound effect. There's no point in buying zero of an item,
    //  the player can just hit cancel.
    if(sQuantity < 1)
    {
        sQuantity = tOriginalQuantity;
        return;
    }

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
}
void AdvUIVendor::ResolveQuantityClamps(int &sClampLo, int &sClampHi)
{
    ///--[Documentation]
    //--Each of the modes has a possible quantity clamp. This routine figures out what they are and
    //  returns them with shared variables.
    //--If the values come back as 0, that is an error.
    sClampLo = 0;
    sClampHi = 0;

    ///--[Buy Mode]
    //--In buy mode, the low clamp is 1 and the high clamp is either the quantity of item available,
    //  a mandated max of 100, or however many of the item the player can afford.
    if(mIsBuyMode)
    {
        //--Get the item package. This is the item the player is attempting to buy.
        ShopInventoryPack *rConsideredPack = GetConsideredItemPack();
        AdventureItem     *rConsideredItem = GetConsideredItem();
        if(!rConsideredPack || !rConsideredItem) return;

        //--Minimum is 1.
        sClampLo = 1;

        //--Get the max quantity. By default it's 100, but items can have mandated quantities
        //  because a shop only sells so many of them.
        sClampHi = 100;
        if(rConsideredPack->mQuantity < 100 && rConsideredPack->mQuantity != -1) sClampHi = rConsideredPack->mQuantity;

        //--Get the item cost and current cash.
        int tItemCost = rConsideredItem->GetValue();
        if(rConsideredPack->mPriceOverride > -1) tItemCost = rConsideredPack->mPriceOverride;

        //--Find out the highest number the player can afford. If the cost is zero, skip this step.
        //  If this afford value is lower, clamp to that instead of the quantity.
        if(tItemCost >= 1)
        {
            int tCanAffordToBuy = AdventureInventory::Fetch()->GetPlatina() / tItemCost;
            if(tCanAffordToBuy < sClampHi) sClampHi = tCanAffordToBuy;
        }
    }
    ///--[Sell Mode]
    //--The quantity is clamped by how many the player has.
    else if(mIsSellMode)
    {
        //--Setup.
        AdventureItem *rConsideredItem = GetConsideredItem();
        if(!rConsideredItem) return;

        //--Low count is always 1.
        sClampLo = 1;

        //--High clamp is the selected item's quantity. Can also be 1.
        sClampHi = rConsideredItem->GetStackSize();
    }
    ///--[Buyback Mode]
    //--The player is clamped by how many are available and their cash.
    else if(mIsBuybackMode)
    {
        //--Get the item package. This is the item the player is attempting to buy.
        ShopInventoryPack *rConsideredPack = GetConsideredItemPack();
        AdventureItem     *rConsideredItem = GetConsideredItem();
        if(!rConsideredPack || !rConsideredItem) return;

        //--Minimum is 1.
        sClampLo = 1;

        //--Get the max quantity. By default it's 100, but items can have mandated quantities
        //  because a shop only sells so many of them.
        sClampHi = 100;
        if(rConsideredPack->mQuantity < 100 && rConsideredPack->mQuantity != -1) sClampHi = rConsideredPack->mQuantity;

        //--Get the item cost and current cash.
        int tItemCost = rConsideredItem->GetValue();
        if(rConsideredPack->mPriceOverride > -1) tItemCost = rConsideredPack->mPriceOverride;

        //--Find out the highest number the player can afford. If the cost is zero, skip this step.
        //  If this afford value is lower, clamp to that instead of the quantity.
        if(tItemCost >= 1)
        {
            int tCanAffordToBuy = AdventureInventory::Fetch()->GetPlatina() / tItemCost;
            if(tCanAffordToBuy < sClampHi) sClampHi = tCanAffordToBuy;
        }
    }
}

///======================================== Unlock Mode ===========================================
void AdvUIVendor::SetUnlockCanEquip(bool pCanEquip)
{
    ///--[Documentation]
    //--If true, the unlock interface is allowed to ask the user to equip the unlocked item. Should only
    //  be used if the interface is creating items.
    //--Should be called as part of the item creation code. If toggled to true, it will ask the user to
    //  equip the item and then toggle back off.
    mUnlockCanEquip = pCanEquip;
}
void AdvUIVendor::SetUnlockVariable(const char *pItemName, const char *pVariablePath)
{
    ///--[Documentation]
    //--When in unlock mode, if an item is unlocked by the player, this variable will be toggled to 1.0f.
    if(!pItemName || !pVariablePath) return;

    //--Locate the item.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementByName(pItemName);
    if(!rPackage) return;

    //--Set.
    ResetString(rPackage->mAssociatedVariable, pVariablePath);
}
void AdvUIVendor::SetUnlockRemove(const char *pItemName, bool pRemoveOnUnlock)
{
    ///--[Documentation]
    //--When in unlock mode, if an item is unlocked it can be removed from the list if this flag is true.
    if(!pItemName) return;

    //--Locate the item.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementByName(pItemName);
    if(!rPackage) return;

    //--Set.
    rPackage->mRemoveOnUnlock = pRemoveOnUnlock;
}
void AdvUIVendor::SetUnlockExecute(const char *pItemName, const char *pExecPath)
{
    ///--[Documentation]
    //--When in unlock mode, unlocking an item can optionally run a script.
    if(!pItemName || !pExecPath) return;

    //--Locate the item.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementByName(pItemName);
    if(!rPackage) return;

    //--Set.
    ResetString(rPackage->mExecOnUnlock, pExecPath);
}
void AdvUIVendor::AddItemUnlockRequirement(const char *pItemName, const char *pRequirementName, const char *pShortName, int pQuantity)
{
    ///--[Documentation]
    //--When in unlock mode, items can have other items be required to unlock them. This function staples a requirement onto
    //  the named item, if it is found. The required item is just a string, and the inventory is queried for that string.
    if(!pItemName || !pRequirementName || !pShortName || pQuantity < 1) return;

    //--Locate the item.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementByName(pItemName);
    if(!rPackage) return;

    //--Create an unlocking package.
    ShopUnlockPack *nUnlockPack = (ShopUnlockPack *)starmemoryalloc(sizeof(ShopUnlockPack));
    nUnlockPack->Initialize();
    ResetString(nUnlockPack->mItemName, pRequirementName);
    ResetString(nUnlockPack->mItemDisplayName, pRequirementName);
    ResetString(nUnlockPack->mItemShort, pShortName);
    nUnlockPack->mNumberNeeded = pQuantity;

    //--Register.
    rPackage->mUnlockRequirements->AddElementAsTail("X", nUnlockPack, &ShopUnlockPack::DeleteThis);

    //--Display name. By default this is identical to the base name, but can optionally be run through a translation
    //  if needed.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_ITEMS);
    if(rTranslation)
    {
        StarTranslation::StringTranslateWorker(nUnlockPack->mItemDisplayName, rTranslation);
    }

    ///--[Maximum Scroll]
    //--Recompute scroll maximum.
    rPackage->mScrollMax = 0.0f;
    ShopUnlockPack *rUnlockPack = (ShopUnlockPack *)rPackage->mUnlockRequirements->PushIterator();
    while(rUnlockPack)
    {
        //--If this is not the final entry, put a comma on the string:
        if(rPackage->mUnlockRequirements->GetTail() != rUnlockPack)
        {
            //--Get shorthand name.
            char *tBuffer = InitializeString("%s (x%i), ", rUnlockPack->mItemShort, rUnlockPack->mNumberNeeded);
            rPackage->mScrollMax = rPackage->mScrollMax + (AdvImages.rFont_Mainline->GetTextWidth(tBuffer));

            //--Clean.
            free(tBuffer);
        }
        //--Final entry.
        else
        {
            //--Get shorthand name.
            char *tBuffer = InitializeString("%s (x%i)", rUnlockPack->mItemShort, rUnlockPack->mNumberNeeded);
            rPackage->mScrollMax = rPackage->mScrollMax + (AdvImages.rFont_Mainline->GetTextWidth(tBuffer));

            //--Clean.
            free(tBuffer);
        }

        //--Next.
        rUnlockPack = (ShopUnlockPack *)rPackage->mUnlockRequirements->AutoIterate();
    }

    //--Subtract the width of the display area.
    rPackage->mScrollMax = rPackage->mScrollMax - 381.0f;
    if(rPackage->mScrollMax < 0.0f) rPackage->mScrollMax = 0.0f;
}

///==================================== Materials Handling ========================================
void AdvUIVendor::AddUnlockMaterial(const char *pItemName, const char *pShorthandName)
{
    ///--[Documentation]
    //--Adds this name to the materials list. Used for display only.
    if(!pItemName) return;

    //--Check for duplicates.
    if(mMaterialsList->GetElementByName(pItemName)) return;

    //--Create a local copy of the string.
    char *nLocalString = InitializeString(pShorthandName);

    //--Dummy pointer. Add.
    mMaterialsList->AddElement(pItemName, nLocalString, &FreeThis);
}
void AdvUIVendor::SetUnlockMaterialUsePages(bool pFlag)
{
    mMaterialsUsePages = pFlag;
}
void AdvUIVendor::ClearMaterialsPages()
{
    mMaterialsPage = 0;
    mMaterialsPages->ClearList();
}
void AdvUIVendor::AddMaterialsPage(const char *pName)
{
    if(!pName) return;
    ShopMaterialsPage *nPage = (ShopMaterialsPage *)starmemoryalloc(sizeof(ShopMaterialsPage));
    nPage->Initialize();
    mMaterialsPages->AddElement(pName, nPage, &ShopMaterialsPage::DeleteThis);
}
void AdvUIVendor::AddUnlockMaterialToPage(const char *pPageName, const char *pItemName, const char *pShorthand)
{
    if(!pPageName || !pItemName || !pShorthand) return;
    ShopMaterialsPage *rActivePage = (ShopMaterialsPage *)mMaterialsPages->GetElementByName(pPageName);
    if(!rActivePage) return;
    char *nLocalString = InitializeString(pShorthand);
    rActivePage->mMaterials->AddElement(pItemName, nLocalString, &FreeThis);
}

///===================================== Cursor Resolving =========================================
AdventureItem *AdvUIVendor::GetConsideredItem()
{
    ///--[Documentation]
    //--Returns a pointer to the item that is currently under "consideration", be that to buy, sell,
    //  buyback, etc.
    if(mIsBuyMode)
    {
        //--Items are stored inside a package held by the shop.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
        if(!rPackage) return NULL;

        //--Pass back the internal pointer.
        return rPackage->mItem;
    }
    //--Sell mode.
    else if(mIsSellMode)
    {
        //--Items are taken from the inventory.
        StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
        return (AdventureItem *)rItemList->GetElementBySlot(mCursor);
    }
    //--Buyback.
    else if(mIsBuybackMode)
    {
        ShopInventoryPack *rPackage = (ShopInventoryPack *)AdventureInventory::Fetch()->GetBuybackList()->GetElementBySlot(mCursor);
        if(!rPackage) return NULL;

        //--Pass back the internal pointer.
        return rPackage->mItem;
    }
    //--Gems mode.
    else if(mIsGemsMode)
    {
        //--Taken from the internally stored gems list.
        StarLinkedList *rGemsList = AdventureInventory::Fetch()->GetGemList();
        if(!rGemsList) return NULL;

        //--Cast and pass back.
        return (AdventureItem *)rGemsList->GetElementBySlot(mCursor);
    }
    //--Unlock.
    else if(mIsUnlockMode)
    {
        //--Unlocking mode uses categories. The zeroth category is just the shop inventory, further categories are usually sorted
        //  by a type. The same cursor is used for all category cases.
        StarLinkedList *rActiveCategory = GetConsideredCategory();
        if(!rActiveCategory) return NULL;

        //--Get the item package from the selected category.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)rActiveCategory->GetElementBySlot(mCursor);
        if(!rPackage) return NULL;

        //--Pass back the internal pointer.
        return rPackage->mItem;
    }

    //--Mode not handled.
    return NULL;
}
ShopInventoryPack *AdvUIVendor::GetConsideredItemPack()
{
    ///--[Documentation]
    //--Returns a pointer to the item package that is currently under "consideration", be that to buy, sell,
    //  buyback, etc. In sell mode, items are not stored in packages so NULL is always returned.
    if(mIsBuyMode)
    {
        return (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
    }
    //--Sell mode.
    else if(mIsSellMode)
    {
        return NULL;
    }
    //--Buyback.
    else if(mIsBuybackMode)
    {
        return (ShopInventoryPack *)AdventureInventory::Fetch()->GetBuybackList()->GetElementBySlot(mCursor);
    }
    //--Unlock.
    else if(mIsUnlockMode)
    {
        //--Unlocking mode uses categories. The zeroth category is just the shop inventory, further categories are usually sorted
        //  by a type. The same cursor is used for all category cases.
        StarLinkedList *rActiveCategory = GetConsideredCategory();
        if(!rActiveCategory) return NULL;

        //--Get the item package from the selected category.
        return (ShopInventoryPack *)rActiveCategory->GetElementBySlot(mCursor);
    }

    //--Mode not handled.
    return NULL;
}
StarLinkedList *AdvUIVendor::GetConsideredCategory()
{
    ///--[Documentation]
    //--Categories, used in unlock mode, are a series of StarLinkedList entries which may contain truncated
    //  versions of the shop inventory. The zeroth entry is always no category and displays the entire inventory.
    if(mCategoryList->GetListSize() < 1) return mShopInventory;

    //--Locate the category.
    ShopCategoryPack *rCategoryPack = (ShopCategoryPack *)mCategoryList->GetElementBySlot(mCategoryCursor);
    if(!rCategoryPack)
    {
        fprintf(stderr, "AdvUIVendor:GetConsideredCategory() - Subtype %s. Warning, category in slot %i was NULL.\n", mSubtype, mCategoryCursor);
        return mShopInventory;
    }

    //--Return the linked list.
    return rCategoryPack->mrMembers;
}

///==================================== Comparison Handling =======================================
void AdvUIVendor::SetComparisonCharacter(int pPartySlot)
{
    ///--[Documentation]
    //--Sets the comparison character to the given slot. This does check if the character can use the
    //  item and will do nothing if it can't.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[Error Check]
    //--Get the item in question. Stop if there is an error.
    AdventureItem *rCheckItem = GetConsideredItem();
    if(!rCheckItem) return;

    //--If there's only one character, do nothing.
    int tPartySize = rAdventureCombat->GetActivePartyCount();
    if(tPartySize <= 1) return;

    ///--[Setting]
    //--Get the member.
    AdvCombatEntity *rCharacter = rAdventureCombat->GetActiveMemberI(pPartySlot);
    if(!rCharacter) return;

    //--If the character can use the item, set.
    if(rCheckItem->IsEquippableBy(rCharacter->GetName()))
    {
        mCompareCharacterCur = pPartySlot;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdvUIVendor::ScrollComparisonCharacter(int pAmount)
{
    ///--[Documentation]
    //--Changes the comparison character up or down, usually in response to the shoulder buttons being pressed.
    //  This cannot be done by simply incrementing or decrementing the character cursor, as it is
    //  possible that only one or no characters can use the item being highlighted. Characters who cannot use
    //  the item should be excluded from comparison.
    //--The pAmount value should be +1 or -1. All other values are ignored, we only use the sign.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[Error Check]
    //--Get the item in question. Stop if there is an error.
    AdventureItem *rCheckItem = GetConsideredItem();
    if(!rCheckItem) return;

    //--If there's only one character, do nothing.
    int tPartySize = rAdventureCombat->GetActivePartyCount();
    if(tPartySize <= 1) return;

    //--If the requested move was 0, do nothing.
    if(pAmount == 0) return;

    ///--[Issue Move]
    //--Setup.
    mCompareCharacterOld = mCompareCharacterCur;

    //--Resolve increment, positive or negative depending on the sign.
    int tIncrement = 1;
    if(pAmount < 0) tIncrement = -1;

    //--Scan across the party, wrapping if needed, until a valid character is found.
    int tMaxScrolls = tPartySize;
    while(tMaxScrolls > 0)
    {
        //--Increment/decrement the cursor.
        mCompareCharacterCur = mCompareCharacterCur + tIncrement;

        //--Wrap.
        if(mCompareCharacterCur <           0) mCompareCharacterCur = tPartySize - 1;
        if(mCompareCharacterCur >= tPartySize) mCompareCharacterCur = 0;

        //--Get the member.
        AdvCombatEntity *rCharacter = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);
        if(!rCharacter) continue;

        //--Check if this character can use the item. If they can, stop scrolling and leave the cursor on them.
        if(rCheckItem->IsEquippableBy(rCharacter->GetName()))
        {
            break;
        }

        //--Next iteration. Decrement tMaxScrolls so the loop can loop all the way around in case
        //  only the current character can use the item. If nobody can use the item, then this will
        //  stop the loop eventually.
        tMaxScrolls --;
    }

    ///--[Character Changed]
    //--If the character cursor changed, play SFX and set timers.
    if(mCompareCharacterCur != mCompareCharacterOld)
    {
        //--Reset timers.
        mCompareSwapTimer = 0;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdvUIVendor::ToggleComparisonSlot()
{
    ///--[Documentation]
    //--When an item can be compared against more than one equipment slot, such as accessories, the
    //  player can switch which slot is being checked against with a keypress. This function handles that.
    //--Get the slot currently in use. The comparison slot is stored as a string.
    int tCurrentSlot = mComparisonSlotList->GetSlotOfElementByName(mCompareSlot);
    tCurrentSlot ++;

    //--Wrap if needed.
    if(tCurrentSlot >= mComparisonSlotList->GetListSize()) tCurrentSlot = 0;

    //--Set.
    const char *rSlotName = mComparisonSlotList->GetNameOfElementBySlot(tCurrentSlot);
    ResetString(mCompareSlot, rSlotName);

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
}
void AdvUIVendor::RecheckComparisonCharacter(AdventureItem *pItem)
{
    ///--[Documentation]
    //--Checks which characters the given item can be compared against. If the character currently
    //  in focus can use it, nothing happens. Otherwise, the first available character who can use the
    //  item becomes the comparison character.
    //--If nobody can be a comparison character, sets comparison to -1.
    if(!pItem) return;

    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int tPartySize = rAdventureCombat->GetActivePartyCount();

    ///--[Current Check]
    //--Get the current comparison character, if one exists, and see if they can use the item.
    AdvCombatEntity *rCurrentCharacter = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);
    if(rCurrentCharacter)
    {
        //--If the character can use the item, we need to check if anyone else can to show the swap arrows.
        if(pItem->IsEquippableBy(rCurrentCharacter->GetName()))
        {
            //--Reset flag.
            mShowSwapArrows = false;

            //--Check if the comparison slot is the same. It may change even if the character is the same.
            RecheckComparisonSlot(pItem, rCurrentCharacter);

            //--Iterate across the party.
            for(int i = 0; i < tPartySize; i ++)
            {
                //--Get the member.
                AdvCombatEntity *rCharacter = rAdventureCombat->GetActiveMemberI(i);
                if(!rCharacter || i == mCompareCharacterCur) continue;

                //--If they can use the item, then at least two characters can use it, show swap arrows.
                if(pItem->IsEquippableBy(rCharacter->GetName()))
                {
                    mShowSwapArrows = true;
                    return;
                }
            }

            //--Only this character can use the item, arrows are hidden.
            return;
        }
    }

    ///--[Next Check]
    //--Reset flags.
    bool tFoundMatch = false;
    mShowSwapArrows = false;
    ResetString(mCompareSlot, NULL);

    //--The current character either didn't exist or can't use the item, so find the first party member
    //  who can and make them the current character.
    for(int i = 0; i < tPartySize; i ++)
    {
        //--Get the member.
        AdvCombatEntity *rCharacter = rAdventureCombat->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--If they can use the item, flag them as the current character and reset the swap timer.
        if(pItem->IsEquippableBy(rCharacter->GetName()))
        {
            //--First match, set this as the comparison character.
            if(!tFoundMatch)
            {
                tFoundMatch = true;
                mCompareCharacterOld = mCompareCharacterCur;
                mCompareCharacterCur = i;
                mCompareSwapTimer = 0;
                RecheckComparisonSlot(pItem, rCharacter);
            }
            //--Otherwise, we should show swap arrows as there are at least two comparison characters.
            else
            {
                mShowSwapArrows = true;
                return;
            }
        }
    }

    //--Found one match, stop here.
    if(tFoundMatch) return;

    ///--[No Matches]
    //--Set the current character to -1, nobody can use the item.
    mCompareCharacterOld = mCompareCharacterCur;
    mCompareCharacterCur = -1;
    mCompareSwapTimer = 0;
}
void AdvUIVendor::RecheckComparisonSlot(AdventureItem *pItem, AdvCombatEntity *pEntity)
{
    ///--[Documentation]
    //--Given an item and an entity that can nominally equip that item, checks which slots the item
    //  can be equipped in. If the current comparison slot is valid, does nothing.
    //--Otherwise, sets the comparison slot to the first valid slot.
    if(!pItem || !pEntity) return;

    //--Make sure the entity can use the item.
    if(!pItem->IsEquippableBy(pEntity->GetName())) return;

    //--Storage list.
    int tDummyPtr = 5;
    mComparisonSlotList->ClearList();

    //--Reset this flag.
    mCanSwapCompareSlot = false;

    ///--[Scan Equipment Slots]
    //--Get total equipment slots. Iterate across them, making a list of all usable slots.
    int tTotalSlots = pEntity->GetEquipmentSlotsTotal();
    for(int i = 0; i < tTotalSlots; i ++)
    {
        //--Get the slot.
        const char *rEquipSlotName = pEntity->GetNameOfEquipmentSlot(i);
        if(!rEquipSlotName) continue;

        //--Check if the character can equip it in that slot.
        if(pItem->IsEquippableIn(rEquipSlotName))
        {
            mComparisonSlotList->AddElementAsTail(rEquipSlotName, &tDummyPtr);
        }
    }

    //--If the list has zero element, something went wrong. The entity can nominally equip the item
    //  but can't put it in any slots.
    if(mComparisonSlotList->GetListSize() < 1)
    {
        return;
    }

    //--If there is more than one element on the list, then the comparison slot can be swapped.
    if(mComparisonSlotList->GetListSize() > 1) mCanSwapCompareSlot = true;

    ///--[Check Current]
    //--If there is a current slot under consideration, check if it is on the list. If so, we don't
    //  need to change anything.
    if(mCompareSlot)
    {
        void *rCheckPtr = mComparisonSlotList->GetElementByName(mCompareSlot);
        if(rCheckPtr)
        {
            return;
        }
    }

    ///--[Set To Zeroth]
    //--The current slot is not on the list. Set the comparison slot to the zeroth on the list.
    const char *rZeroEntry = mComparisonSlotList->GetNameOfElementBySlot(0);
    ResetString(mCompareSlot, rZeroEntry);
}
