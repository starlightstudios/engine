//--Base
#include "AdventureLevelStructures.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

///====================================== Tiled Objects ===========================================
///--[ClimbablePackage]
void ClimbablePackage::Initialize()
{
    //--Position
    mX = 0;
    mY = 0;
    mW = 1;
    mH = 1;

    //--State
    mIsLadder = false;
    mIsActivated = false;

    //--Depths
    mLowerDepth = 0;
    mUpperDepth = 0;
}

///--[DoorPackage]
void DoorPackage::Initialize()
{
    //--Position
    mX = -100;
    mY = -100;
    mZ = 0;
    mIsWide = false;

    //--Contents
    mCanRespawn = false;
    mContents = NULL;
    mKeyword = NULL;
    mRandomScript = NULL;

    //--State
    mIsOpened = false;
    mIsScriptLocked = false;
    mExamineScript = NULL;
    mLevelTransitOnOpen = NULL;
    mLevelTransitExit = NULL;
    mUseSpaceSound = false;
    mIsSpaceEW = false;

    //--Rendering
    rRenderOpen = NULL;
    rRenderClosed = NULL;

    //--Render Overrides
    mRenderHi = -2.0f;
    mRenderLo = -2.0f;
}
void DoorPackage::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    DoorPackage *rPackage = (DoorPackage *)pPtr;
    free(rPackage->mExamineScript);
    free(rPackage->mLevelTransitOnOpen);
    free(rPackage->mLevelTransitExit);
    free(rPackage->mContents);
    free(rPackage->mKeyword);
    free(rPackage->mRandomScript);
    free(rPackage);
}

///--[ExaminePackage]
void ExaminePackage::Initialize()
{
    //--Position
    mX = -100;
    mY = -100;
    mW = 1;
    mH = 1;

    //--Flags
    mFlags = 0;

    //--Automated Transition
    mIsAutoTransition = false;
    mAutoRoomDest[0] = '\0';
    mAutoLocationDest[0] = '\0';
    mAutoSound[0] = '\0';
}

///--[ExitPackage]
void ExitPackage::Initialize()
{
    //--Position
    mX = -100;
    mY = -100;
    mW = 1;
    mH = 1;
    mDepth = 0;

    //--Destination Info
    mMapDestination[0] = '\0';
    mExitDestination[0] = '\0';

    //--Rendering
    mIsStaircase = false;
    rRenderImg = NULL;
}

///--[InflectionPackage]
void InflectionPackage::Initialize()
{
    //--Position
    mX = -100;
    mY = -100;
    mW = 1;
    mH = 1;
    mLowerDepth = 0;
    mUpperDepth = 0;
}

///--[TriggerPackage]
void TriggerPackage::Initialize()
{
    mSelfDestruct = false;
    mDimensions.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    strcpy(mFiringName, "Unset");
}

///--[AuraParticle]
void AuraParticle::Initialize()
{
    //--Lifetime
    mLifetime = AURAPARTICLE_LIFETIME;

    //--Display
    mXPos = 0.0f;
    mYPos = 0.0f;
    mScale = 1.0f;
    rImage = NULL;

    //--Movement
    mXSpeed = 0.0f;
    mYSpeed = 0.0f;

    //--Sizing
    mSizeTimer = 0;
    mSizeLo = 0.50f;
    mSizeHi = 1.30f;
}
void AuraParticle::Update()
{
    //--Lifetime/Postion.
    mLifetime --;
    mXPos = mXPos + mXSpeed;
    mYPos = mYPos + mYSpeed;

    //--Size.
    mSizeTimer ++;
}
void AuraParticle::Render()
{
    //--Error check.
    if(!rImage) return;

    //--Fades out when despawning.
    float tAlpha = 1.0f;
    if(mLifetime < 40)
    {
        tAlpha = (float)(mLifetime / 40.0f);
    }
    //--Fades out when spawning.
    else if(mLifetime > (AURAPARTICLE_LIFETIME - 40))
    {
        tAlpha = 1.0f - ((mLifetime - (AURAPARTICLE_LIFETIME - 40)) / 40.0f);
    }

    //--Compute scale.
    float cScalePct = (float)mSizeTimer / (float)AURAPARTICLE_SIZE_PERIOD;
    float cScale = mSizeLo + (sinf(cScalePct * 3.1415926f) * (mSizeHi - mSizeLo));
    if(cScale < 0.001f) return;

    //--Scale affects alpha.
    if(cScale < 1.0f) tAlpha = tAlpha * cScale;

    //--Position.
    glTranslatef(mXPos, mYPos, 0.0f);
    glScalef(cScale, cScale, 1.0f);

    //--Render.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);
    rImage->Draw(rImage->GetTrueWidth() * -0.50f, rImage->GetTrueHeight() * -0.50f);

    //--Clean.
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    glTranslatef(-mXPos, -mYPos, 0.0f);
}

///--[ObjectivePack]
void ObjectivePack::Initialize()
{
    mIsComplete = false;
    mDisplayName = NULL;
}
void ObjectivePack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    ObjectivePack *rPtr = (ObjectivePack *)pPtr;
    free(rPtr->mDisplayName);
    free(rPtr);
}
