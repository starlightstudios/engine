//--Base
#include "WADFile.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"
#include "StarBitmap.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--[Property Queries]
int WADFile::GetTexturesTotal()
{
    return mWallTexturesTotal + mFlatTexturesTotal;
}
WAD_Texture *WADFile::GetTextureEntry(int pSlot)
{
    //--Range check.
    if(pSlot < 0 || pSlot >= mWallTexturesTotal + mFlatTexturesTotal) return NULL;

    //--Flats are listed first, then wall textures.
    if(pSlot < mFlatTexturesTotal)
    {
        return &mFlatTextures[pSlot];
    }

    //--Wall Textures.
    return &mWallTextures[pSlot - mFlatTexturesTotal];
}

//--[Search Functions]
StarBitmap *WADFile::GetFlatTexture(const char *pName)
{
    //--Returns the matching flat texture, assuming it exists. Null on error.
    if(!pName) return NULL;
    for(int i = 0; i < mFlatTexturesTotal; i ++)
    {
        if(CompareEight(pName, mFlatTextures[i].mName))
        {
            return mFlatTextures[i].mImage;
        }
    }

    //--Search the wall textures in case the texture was there instead. Since these functions call each
    //  other, the xIsRecurse flag prevents an infinite loop.
    if(!xIsRecurse)
    {
        xIsRecurse = true;
        StarBitmap *rCheckTexture = GetWallTexture(pName);
        xIsRecurse = false;
        return rCheckTexture;
    }

    //--Not found on either list.
    return NULL;
}
StarBitmap *WADFile::GetWallTexture(const char *pName)
{
    //--Returns the matching wall texture. Null on error.
    if(!pName) return NULL;
    for(int i = 0; i < mWallTexturesTotal; i ++)
    {
        if(CompareEight(pName, mWallTextures[i].mName))
        {
            return mWallTextures[i].mImage;
        }
    }

    //--Search the wall textures in case the texture was there instead. Since these functions call each
    //  other, the xIsRecurse flag prevents an infinite loop.
    if(!xIsRecurse)
    {
        xIsRecurse = true;
        StarBitmap *rCheckTexture = GetFlatTexture(pName);
        xIsRecurse = false;
        return rCheckTexture;
    }

    //--Not found on either list.
    return NULL;
}

void WADFile::LoadPatchNames()
{
    //--Loads all wall patch names. A wall patch is a horizontal segment of a wall (duh) that
    //  can be assembled together with other patches to create a wall texture.
    //--Wall patches are loaded once in the wad file and are referred to during texture uploading.
    //  A single wall patch could be part of many textures.
    //--Also, the patch names are always exactly 8 characters long. Use CompareEight() for them.
    for(int i = 0; i < mPatchesTotal; i ++) free(mPatchNames[i]);
    free(mPatchNames);
    mPatchesTotal = 0;
    mPatchNames = NULL;

    //--Get the lump in question.
    WAD_Directory_Entry *rPatchNameLump = FindLump("PNAMES");
    if(!rPatchNameLump) return;

    //--Check how many entries it has.
    int tCursor = 0;
    memcpy(&mPatchesTotal, &rPatchNameLump->mData[tCursor], sizeof(int32_t));
    tCursor += sizeof(int32_t);

    //--Check data.
    if(mPatchesTotal < 0)
    {
        mPatchesTotal = 0;
        return;
    }

    //--Allocate space and copy data.
    SetMemoryData(__FILE__, __LINE__);
    mPatchNames = (char **)starmemoryalloc(sizeof(char *) * mPatchesTotal);
    for(int i = 0; i < mPatchesTotal; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mPatchNames[i] = (char *)starmemoryalloc(sizeof(char) * 8);
        memcpy(mPatchNames[i], &rPatchNameLump->mData[tCursor], sizeof(char) * 8);
        tCursor += (sizeof(char) * 8);
    }
}
char **WADFile::GetFlatTexturesNeeded(int &sTotalTextures)
{
    //--Once the floor/ceiling polygons have been resolved, we need to build a list of all textures
    //  that appear on them. This function returns a list and how many textures are on it.
    //--ALL textures are 8-letter strings that are not null-terminated. Use CompareEight() to deal
    //  with this oddity.
    //--Returns NULL on error. Otherwise, you are responsible for deallocation of the list. While
    //  each string is of a fixed length, the array is still double-allocated since this may be
    //  expanded at some point.
    //--One last note: All sector/wall textures are loaded, even if those sectors never appear in the
    //  map due to compilation errors.
    sTotalTextures = 0;
    if(mSectorsTotal < 1) return NULL;

    //--Create a linked-list to store all the names for now.
    StarLinkedList *tNameList = new StarLinkedList(false);

    //--Begin scan.
    for(int i = 0; i < mSectorsTotal; i ++)
    {
        //--Quick-access pointers.
        char *rNameOfFloor = mSectors[i].mTexFloor;
        char *rNameOfCeiling = mSectors[i].mTexCeiling;

        //--Scan by iteration through the list.
        bool tFloorMatch = false;
        bool tCeilingMatch = false;
        char *rName = (char *)tNameList->PushIterator();
        while(rName)
        {
            //--Match found on the floor.
            if(CompareEight(rName, rNameOfFloor))
            {
                tFloorMatch = true;
            }

            //--Match found on the ceiling.
            if(CompareEight(rName, rNameOfCeiling))
            {
                tCeilingMatch = true;
            }

            //--If both were matched, then we can leave.
            if(tFloorMatch && tCeilingMatch)
            {
                tNameList->PopIterator();
                break;
            }
            rName = (char *)tNameList->AutoIterate();
        }

        //--Was the floor string found? If not, add it.
        if(!tFloorMatch)
        {
            tNameList->AddElement("X", rNameOfFloor);
        }

        //--Was the ceiling string found? If not, add it.
        if(!tCeilingMatch && !CompareEight(rNameOfCeiling, rNameOfFloor))
        {
            tNameList->AddElement("X", rNameOfCeiling);
        }
    }

    //--Debug report.
    if(false)
    {
        fprintf(stderr, "Texture report: %i unique loads.\n", tNameList->GetListSize());
        char *rName = (char *)tNameList->PushIterator();
        while(rName)
        {
            fprintf(stderr, " %s\n", rName);
            rName = (char *)tNameList->AutoIterate();
        }
    }

    //--Allocate and dump the data.
    sTotalTextures = tNameList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    char **nList = (char **)starmemoryalloc(sizeof(char *) * sTotalTextures);
    for(int i = 0; i < sTotalTextures; i ++)
    {
        nList[i] = (char *)tNameList->GetElementBySlot(i);
    }

    //--Clean up.
    delete tNameList;
    return nList;
}
char **WADFile::GetWallTexturesNeeded(int &sTotalTextures)
{
    //--Follows the same logic as GetFlatTexturesNeeded, except this is for wall textures (the two
    //  are stored and formatted differently).
    sTotalTextures = 0;
    if(mSidedefsTotal < 1) return NULL;

    //--Create a linked-list to store all the names for now.
    StarLinkedList *tNameList = new StarLinkedList(false);

    //--Begin scan. We check all sidedefs, not sectors. Also, unused or invisible sidedefs are not
    //  checked for, for simplicity.
    for(int i = 0; i < mSidedefsTotal; i ++)
    {
        //--Quick-access pointers.
        char *rNameOfUpper = mSidedefs[i].mTexTop;
        char *rNameOfMiddl = mSidedefs[i].mTexMid;
        char *rNameOfLower = mSidedefs[i].mTexBot;

        //--Scan by iteration through the list.
        bool tUpperMatch = false;
        bool tMiddlMatch = false;
        bool tLowerMatch = false;
        char *rName = (char *)tNameList->PushIterator();
        while(rName)
        {
            //--Match found on the lower.
            if(CompareEight(rName, rNameOfUpper))
            {
                tUpperMatch = true;
            }

            //--Match found on the middle. This may be invisible, '-' is not loaded later.
            if(CompareEight(rName, rNameOfMiddl))
            {
                tMiddlMatch = true;
            }

            //--Match found on the middle. This may be invisible, '-' is not loaded later.
            if(CompareEight(rName, rNameOfLower))
            {
                tLowerMatch = true;
            }

            //--If all three matched, then this sidedef is handled.
            if(tUpperMatch && tMiddlMatch && tLowerMatch)
            {
                tNameList->PopIterator();
                break;
            }
            rName = (char *)tNameList->AutoIterate();
        }

        //--Was the upper string found? If not, add it.
        if(!tUpperMatch)
        {
            tNameList->AddElement("X", rNameOfUpper);
        }

        //--Was the middle string found? If not, add it.
        if(!tMiddlMatch && !CompareEight(rNameOfUpper, rNameOfMiddl))
        {
            tNameList->AddElement("X", rNameOfMiddl);
        }

        //--Was the lower string found? If not, add it.
        if(!tLowerMatch && !CompareEight(rNameOfLower, rNameOfMiddl) && !CompareEight(rNameOfUpper, rNameOfLower))
        {
            tNameList->AddElement("X", rNameOfLower);
        }
    }

    //--Debug report.
    if(false)
    {
        //--Setup
        char tBuffer[9];
        tBuffer[8] = '\0';

        DebugManager::PushPrint(false, "[Wall Texture] Report: %i unique loads.\n", tNameList->GetListSize());
        char *rName = (char *)tNameList->PushIterator();
        while(rName)
        {
            for(int i = 0; i < 8; i ++) tBuffer[i] = rName[i];

            DebugManager::Print("%s\n", tBuffer);
            rName = (char *)tNameList->AutoIterate();
        }
        DebugManager::PopPrint("[Wall Texture] Complete\n");
    }

    //--Allocate and dump the data.
    sTotalTextures = tNameList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    char **nList = (char **)starmemoryalloc(sizeof(char *) * sTotalTextures);
    for(int i = 0; i < sTotalTextures; i ++)
    {
        nList[i] = (char *)tNameList->GetElementBySlot(i);
    }

    //--Clean up.
    delete tNameList;
    return nList;
}
void WADFile::LoadTextures()
{
    //--Loads all texture data in the specified names list. It is expected that all sectors have been
    //  loaded, otherwise this will assume there is no data needed.

    //--Properties: Textures can't use filtering or they look bad. Hi-def textures will later.
    StarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
    StarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
    StarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
    StarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;

    //--[Flats]
    LoadFlats();

    //--[Wall Patches]
    LoadWalls(false);
    LoadWalls(true);
}
void WADFile::LoadFlats()
{
    //--Flats are textures that appear on floors and ceilings only. They are always the exact same
    //  size and use a unique format, so they must be loaded differently from walls.
    for(int i = 0; i < mFlatTexturesTotal; i ++)
    {
        delete mFlatTextures[i].mImage;
    }
    free(mFlatTextures);
    mFlatTextures = NULL;
    mFlatTexturesTotal = 0;
    if(mSectorsTotal < 1) return;

    //--Get a list of all textures needed.
    char **tTextureList = GetFlatTexturesNeeded(mFlatTexturesTotal);
    if(mFlatTexturesTotal < 1) return;

    //--Allocate space. The bitmaps stay NULL, so in case of error, they fail to render.
    SetMemoryData(__FILE__, __LINE__);
    mFlatTextures = (WAD_Texture *)starmemoryalloc(sizeof(WAD_Texture) * mFlatTexturesTotal);
    for(int i = 0; i < mFlatTexturesTotal; i ++)
    {
        mFlatTextures[i].Initialize();
        for(int p = 0; p < 9; p ++)
        {
            mFlatTextures[i].mName[p] = tTextureList[i][p];
        }
        mFlatTextures[i].mName[8] = '\0';
        mFlatTextures[i].mImage = NULL;
    }

    //--Setup.
    uint8_t tColorData[16384];

    //--Begin loading. A directory entry must match the name AND be a flat.
    for(int i = 0; i < mFlatTexturesTotal; i ++)
    {
        //--Scan the entries.
        WAD_Directory_Entry *rEntry = NULL;
        for(int p = 0; p < (int)mLumps; p ++)
        {
            if(mDirectoryEntries[p].mIsFlat && CompareEight(mDirectoryEntries[p].mType, mFlatTextures[i].mName))
            {
                rEntry = &mDirectoryEntries[p];
                break;
            }
        }

        //--No match!
        if(!rEntry)
        {
            DebugManager::ForcePrint("Warning: Flat %s was not found.\n", mFlatTextures[i].mName);
            continue;
        }

        //--Match, copy data.
        //fprintf(stderr, "Flat %s was found\n", mFlatTextures[i].mName);

        //--Wrangle the data. It needs to be converted to RGBA for openGL to use it. Flats are
        //  always 64x64, so that simplifies the calcs.
        for(int p = 0; p < 4096; p ++)
        {
            int tIndex = p * 4;
            int tLookup = rEntry->mData[p];

            tColorData[tIndex+0] = mPaletteData[tLookup].r * 255.0f;
            tColorData[tIndex+1] = mPaletteData[tLookup].g * 255.0f;
            tColorData[tIndex+2] = mPaletteData[tLookup].b * 255.0f;
            tColorData[tIndex+3] = 255;
        }

        //--Upload the data.
        StarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
        StarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;
        mFlatTextures[i].mImage = new StarBitmap(tColorData, 64, 64);

        //--Check if this is a sky texture.
        if(CompareEight(mFlatTextures[i].mName, "F_SKY1\0\0"))
        {
            rSkyTexture = mFlatTextures[i].mImage;
        }
    }

    //--Clean up.
    free(tTextureList);
}
void WADFile::LoadWalls(bool pIsSecondRun)
{
    //--These are used on walls. Unlike flats, transparency is possible on walls.
    DebugManager::PushPrint(false, "[Load Walls] Begin.\n");

    //--On the first pass, deallocate any existing textures.
    if(!pIsSecondRun)
    {
        for(int i = 0; i < mWallTexturesTotal; i ++)
        {
            delete mWallTextures[i].mImage;
        }
        free(mWallTextures);
        mWallTextures = NULL;
        mWallTexturesTotal = 0;
    }

    //--Check sidedefs. If there aren't any, there's no need to load any textures.
    if(mSidedefsTotal < 1)
    {
        DebugManager::PopPrint("[Load Walls] No Sidedefs, exiting.\n");
        return;
    }

    //--Get a list of all unique wall patch names in the current level.
    int tTexturesToLoad = 0;
    char **tNamesList = GetWallTexturesNeeded(tTexturesToLoad);
    if(tTexturesToLoad < 1)
    {
        DebugManager::PopPrint("[Load Walls] No texture names found, exiting.\n");
        return;
    }

    //--Next, we need to get the TEXTURE1 or TEXTURE2 lump. On the second run, we are looking for the TEXTURE2 lump
    //  and will append it if there was a TEXTURE1 lump.
    WAD_Directory_Entry *rTextureLump = NULL;
    if(!pIsSecondRun)
        rTextureLump = FindLump("TEXTURE1");
    else
        rTextureLump = FindLump("TEXTURE2");

    //--If we failed to find the lump, get outta here.
    if(!rTextureLump)
    {
        free(tNamesList);
        DebugManager::ForcePrint("[Load Walls] Warning, the needed texture lump was not found.\n");
        DebugManager::Pop();
        return;
    }

    //--First four bytes describe how many textures are in the lump.
    int32_t tNumTexturesInLump = 0;
    memcpy(&tNumTexturesInLump, &rTextureLump->mData[0], sizeof(int32_t));
    DebugManager::Print("Walls: Textures in lump %i\n", tNumTexturesInLump);

    //--Then we have a byte offset lookup. The offsets are relative to the start of the texture lump.
    SetMemoryData(__FILE__, __LINE__);
    int32_t *tOffsets = (int32_t *)starmemoryalloc(sizeof(int32_t) * tNumTexturesInLump);
    memcpy(tOffsets, &rTextureLump->mData[4], sizeof(int32_t) * tNumTexturesInLump);

    //--Debug Setup.
    char tBuffer[9];
    tBuffer[8] = '\0';

    //--Begin loading the textures from their directories.
    SetMemoryData(__FILE__, __LINE__);
    WAD_Texture_Definition *tTextureDefs = (WAD_Texture_Definition *)starmemoryalloc(sizeof(WAD_Texture_Definition) * tNumTexturesInLump);
    memset(tTextureDefs, 0, sizeof(WAD_Texture_Definition) * tNumTexturesInLump);
    for(int i = 0; i < tNumTexturesInLump; i ++)
    {
        //--Set the cursor.
        int tCursor = tOffsets[i];

        //--Parse out the data. Cursor is incremented in jumps. Some data is always zero and ignored.
        memcpy(tTextureDefs[i].mName,          &rTextureLump->mData[tCursor+ 0], sizeof(char) * 8);
        memcpy(&tTextureDefs[i].mWidth,        &rTextureLump->mData[tCursor+12], sizeof(int16_t));
        memcpy(&tTextureDefs[i].mHeight,       &rTextureLump->mData[tCursor+14], sizeof(int16_t));
        memcpy(&tTextureDefs[i].mPatchesTotal, &rTextureLump->mData[tCursor+20], sizeof(int16_t));
        tCursor += 22;

        //--Store patch data.
        SetMemoryData(__FILE__, __LINE__);
        tTextureDefs[i].mPatches = (WAD_Patch_Descriptor *)starmemoryalloc(sizeof(WAD_Patch_Descriptor) * tTextureDefs[i].mPatchesTotal);
        for(int p = 0; p < tTextureDefs[i].mPatchesTotal; p ++)
        {
            //--Copy
            memcpy(&tTextureDefs[i].mPatches[p], &rTextureLump->mData[tCursor], sizeof(WAD_Patch_Descriptor));

            //--Iterate up.
            tCursor += sizeof(WAD_Patch_Descriptor);
        }

        //--Output
        CopyEight(tBuffer, tTextureDefs[i].mName);
        //DebugManager::Print(" %i - %i: %s - %i %i\n", tCursor, i, tBuffer, tTextureDefs[i].mWidth, tTextureDefs[i].mHeight);
        for(int p = 0; p < tTextureDefs[i].mPatchesTotal; p ++)
        {
            //DebugManager::Print("  %i: %i %i - %i\n", p, tTextureDefs[i].mPatches[p].mXOffset, tTextureDefs[i].mPatches[p].mYOffset, tTextureDefs[i].mPatches[p].mLumpNameIndex);
        }
    }

    //--List that contains all wall textures we are uploading.
    StarLinkedList *tWallTextureList = new StarLinkedList(false);

    //--Compare the marked texture list against the new one. Upload all matches.
    for(int i = 0; i < tTexturesToLoad; i ++)
    {
        //--If the texture happens to be "-" then ignore it, that means 'Blank'.
        if(CompareEight(tNamesList[i], "-\0\0\0\0\0\0\0")) continue;

        //--Scan.
        int tMatchIndex = -1;
        for(int p = 0; p < tNumTexturesInLump; p ++)
        {
            if(CompareEight(tNamesList[i], tTextureDefs[p].mName))
            {
                tMatchIndex = p;
                break;
            }
        }

        //--No match, print an error.
        if(tMatchIndex == -1)
        {
            //DebugManager::Print(" Error: %s has no matching texture name.\n", tNamesList[i]);
            continue;
        }
        //--Match. Upload the data.
        else
        {
            //--Safe name buffer.
            CopyEight(tBuffer, tNamesList[i]);

            //DebugManager::Print(" Texture %s matched texture %i.\n", tBuffer, tMatchIndex);
            StarBitmap *nUploadedImage = UploadWallTexture(&tTextureDefs[tMatchIndex]);

            tWallTextureList->AddElement(tBuffer, nUploadedImage);
        }
    }

    //--First pass: Crossload.
    if(!pIsSecondRun)
    {
        //--Debug.
        DebugManager::Print(" Beginning crossload.\n");

        //--Allocate.
        mWallTexturesTotal = tWallTextureList->GetListSize();
        SetMemoryData(__FILE__, __LINE__);
        mWallTextures = (WAD_Texture *)starmemoryalloc(sizeof(WAD_Texture) * mWallTexturesTotal);

        //--Store the textures.
        for(int i = 0; i < mWallTexturesTotal; i ++)
        {
            mWallTextures[i].Initialize();
            const char *rName = tWallTextureList->GetNameOfElementBySlot(i);
            StarBitmap *rImage = (StarBitmap *)tWallTextureList->GetElementBySlot(i);
            CopyEight(mWallTextures[i].mName, rName);
            mWallTextures[i].mImage = rImage;
            DebugManager::Print(" Texture %s stored in %i.\n", rName, i);
        }
    }
    //--Second pass: Append.
    else
    {
        //--Debug.
        DebugManager::Print(" Beginning append crossload.\n");

        //--Allocate.
        int tStartSlot = mWallTexturesTotal;
        mWallTexturesTotal = mWallTexturesTotal + tWallTextureList->GetListSize();
        mWallTextures = (WAD_Texture *)realloc(mWallTextures, sizeof(WAD_Texture) * mWallTexturesTotal);

        //--Store the textures.
        for(int i = tStartSlot; i < mWallTexturesTotal; i ++)
        {
            mWallTextures[i].Initialize();
            const char *rName = tWallTextureList->GetNameOfElementBySlot(i-tStartSlot);
            StarBitmap *rImage = (StarBitmap *)tWallTextureList->GetElementBySlot(i-tStartSlot);
            CopyEight(mWallTextures[i].mName, rName);
            mWallTextures[i].mImage = rImage;
            DebugManager::Print("  Texture %s stored in %i.\n", rName, i);
        }
    }

    //--Clean up.
    free(tNamesList);
    free(tOffsets);
    for(int i = 0; i < tNumTexturesInLump; i ++) free(tTextureDefs[i].mPatches);
    free(tTextureDefs);
    delete tWallTextureList;
    DebugManager::PopPrint("[Load Walls] Finished.\n");
}
StarBitmap *WADFile::UploadWallTexture(WAD_Texture_Definition *pTextureDefinition)
{
    //--Uploads and returns a StarBitmap containing the requested wall texture. This function will
    //  assemble the texture out of the listed patches.
    if(!pTextureDefinition) return NULL;

    //--Debug.
    DebugManager::PushPrint(false, "Uploading texture\n");

    //--Allocate space based on the sizes.
    int tSizeOfData = pTextureDefinition->mWidth * pTextureDefinition->mHeight * 4;
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tColorData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tSizeOfData);
    //memset(tColorData, 0, sizeof(uint8_t) * tSizeOfData);
    for(int i = 0; i < tSizeOfData; i += 4)
    {
        tColorData[i+0] = 255;
        tColorData[i+1] = 255;
        tColorData[i+2] = 255;
        tColorData[i+3] = 0;
    }

    int tLegalCount = 0;
    int tIllegalCounter = 0;
    DebugManager::Print("  Texture sizes for %s: %i %i\n", pTextureDefinition->mName, pTextureDefinition->mWidth, pTextureDefinition->mHeight);

    //--Begin appending the patches.
    for(int i = 0; i < pTextureDefinition->mPatchesTotal; i ++)
    {
        //--Get the image data using the pnames.
        DebugManager::Print("  Patch %i: %i %i - %i/%i\n", i, pTextureDefinition->mPatches[i].mXOffset, pTextureDefinition->mPatches[i].mYOffset, pTextureDefinition->mPatches[i].mLumpNameIndex, mPatchesTotal);

        //--Make sure the patch is in range.
        if(pTextureDefinition->mPatches[i].mLumpNameIndex < 0 || pTextureDefinition->mPatches[i].mLumpNameIndex >= mPatchesTotal)
        {
            DebugManager::Print("  Error - Patch is out of range: %i vs %i\n", pTextureDefinition->mPatches[i].mLumpNameIndex, mPatchesTotal);
            continue;
        }

        //--Get the requested patch name.
        char tPatchName[9];
        memset(tPatchName, 0, sizeof(char) * 9);
        CopyEight(tPatchName, mPatchNames[pTextureDefinition->mPatches[i].mLumpNameIndex]);
        DebugManager::Print("   Name %s\n", tPatchName);

        //--Locate the patch lump.
        WAD_Directory_Entry *rPatchEntry = FindLumpNonFlat(tPatchName);
        if(!rPatchEntry)
        {
            DebugManager::Print("  Error, could not find patch %s\n", tPatchName);
            continue;
        }
        DebugManager::Print("   Found matching lump.\n");

        //--Get the data out.
        int tCursor = 0;
        int16_t tWidth = 0;
        int16_t tHeight = 0;
        memcpy(&tWidth,   &rPatchEntry->mData[tCursor+0], sizeof(int16_t));
        memcpy(&tHeight,  &rPatchEntry->mData[tCursor+2], sizeof(int16_t));
        tCursor = tCursor + (sizeof(int16_t) * 4);
        //DebugManager::Print("   Lump sizes: %i %i\n", tWidth, tHeight);

        //--Columns data.
        for(int c = 0; c < tWidth; c ++)
        {
            //--Get the location of the column data, relative to the 0 in the picture lump.
            int tColumnCursor = 0;
            memcpy(&tColumnCursor, &rPatchEntry->mData[tCursor], sizeof(int32_t));
            DebugManager::Print("   Column Ptr %i: %i / %i\n", c, tColumnCursor, rPatchEntry->mSize);

            //--Columns are stored sequentially. They end on a 255 bytes.
            bool tIsDone = false;
            while(!tIsDone)
            {
                //--Header for the column.
                uint8_t tYOffset = 0;
                uint8_t tHeight = 0;
                memcpy(&tYOffset, &rPatchEntry->mData[tColumnCursor+0], sizeof(uint8_t));
                memcpy(&tHeight,  &rPatchEntry->mData[tColumnCursor+1], sizeof(uint8_t));
                tColumnCursor = tColumnCursor + 2;
                //DebugManager::Print("    Column Data %i: %i\n", tYOffset, tHeight);

                //--Begin cross-loading. For whatever reason, the first and last bytes aren't rendered.
                //  Just add one to the offset.
                tColumnCursor = tColumnCursor + 1;
                for(int y = 0; y < tHeight; y ++)
                {
                    //--Calculate where the color data goes.
                    int tXPos = pTextureDefinition->mPatches[i].mXOffset + c;
                    int tYPos = pTextureDefinition->mHeight - (tYOffset + y + pTextureDefinition->mPatches[i].mYOffset) - 1;
                    int tColorCursor = (tXPos + (tYPos * pTextureDefinition->mWidth)) * 4;

                    //--Edge check.
                    if(tXPos < 0 || tYPos < 0 || tXPos >= pTextureDefinition->mWidth || tYPos >= pTextureDefinition->mHeight)
                    {
                        tIllegalCounter ++;
                    }
                    //--Don't place anything outside the buffer's edges.
                    else if(tColorCursor < 0 || tColorCursor >= tSizeOfData)
                    {
                        tIllegalCounter ++;
                    }
                    //--Place the pixel.
                    else
                    {
                        //--Get the palette pixel.
                        uint8_t tPalettePosition;
                        memcpy(&tPalettePosition, &rPatchEntry->mData[tColumnCursor], sizeof(uint8_t));
                        StarlightColor *rColor = &mPaletteData[tPalettePosition];

                        //--Send it over.
                        tColorData[tColorCursor+0] = rColor->r * 255.0f;
                        tColorData[tColorCursor+1] = rColor->g * 255.0f;
                        tColorData[tColorCursor+2] = rColor->b * 255.0f;
                        tColorData[tColorCursor+3] = rColor->a * 255.0f;

                        //--Storage.
                        tLegalCount ++;
                    }

                    //--Iterate up.
                    tColumnCursor ++;
                }

                //--One extra byte at the end.
                tColumnCursor ++;

                //--Check if that's the end of the column data.
                uint8_t tCheckByte = 0;
                memcpy(&tCheckByte, &rPatchEntry->mData[tColumnCursor], sizeof(uint8_t));
                if(tCheckByte == 0xFF) tIsDone = true;
            }

            //--Iterate up.
            tCursor += sizeof(int32_t);
        }
    }

    //--Upload the data.
    DebugManager::Print("Uploading data. %i legal pixels, %i illegal pixels.\n", tLegalCount, tIllegalCounter);
    StarBitmap *nBitmap = new StarBitmap(tColorData, pTextureDefinition->mWidth, pTextureDefinition->mHeight);
    DebugManager::Print("Upload complete.\n");

    //--Debug.
    DebugManager::PopPrint("Completed upload of texture.\n");

    //--Clean up.
    free(tColorData);
    return nBitmap;
}
void WADFile::FloorsReResolveTextures()
{
    //--Called after a texture-override event. The floors need to re-resolve which textures they are using
    //  in case the bitmap got changed.
    for(int i = 0; i < mFloorPolysTotal; i ++)
    {
        //--Range-check.
        if(mFloorPolys[i].mAssociatedSector < 0 || mFloorPolys[i].mAssociatedSector >= mSectorsTotal) continue;

        //--Associate.
        mFloorPolys[i].rCeilingTexture = GetFlatTexture(mSectors[mFloorPolys[i].mAssociatedSector].mTexCeiling);
        mFloorPolys[i].rFloorTexture   = GetFlatTexture(mSectors[mFloorPolys[i].mAssociatedSector].mTexFloor);
    }
}
void WADFile::WallsReResolveTextures()
{
    //--Called after a texture-override event. Wall polygons need to re-resolve their textures.
    for(int i = 0; i < mWallPolysTotal; i ++)
    {
        if(mWallPolys[i].mTexName[0] == '\0') continue;
        mWallPolys[i].rTexture = GetWallTexture(mWallPolys[i].mTexName);
    }
}
