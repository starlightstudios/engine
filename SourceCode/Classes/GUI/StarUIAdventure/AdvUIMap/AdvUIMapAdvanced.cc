//--Base
#include "AdvUIMap.h"

//--Classes
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"

///=================================== Advanced Map Features ======================================
///--[System]
void AdvUIMap::ClearAdvancedMapLayers()
{
    //--Clears the map layers, resets back to basic mode.
    mIsUsingBasicMap = true;
    mAdvancedMapList->ClearList();
}
void AdvUIMap::SetAdvancedRemaps(float pX, float pY, float pScaleX, float pScaleY)
{
    //--Sets where the player renders on local maps, and how many pixels the map is versus world coordinates.
    mMapUsePlayerXBase = pX;
    mMapUsePlayerYBase = pY;
    mMapPlayerCoordScaleX = pScaleX;
    mMapPlayerCoordScaleY = pScaleY;
}

///--[Creation]
void AdvUIMap::AddAdvancedLayer(const char *pName, const char *pDLPath)
{
    ///--[Documentation]
    //--Creates a new advanced map layer with the given name. If not already in advanced map mode,
    //  flags the class to use that mode during rendering.
    if(!pName || !pDLPath) return;

    //--Unset flag.
    mIsUsingBasicMap = false;

    //--Allocate.
    AdvancedMapPack *nMapPack = (AdvancedMapPack *)starmemoryalloc(sizeof(AdvancedMapPack));
    nMapPack->Initialize();
    nMapPack->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--If the image exists, resolve its data.
    if(nMapPack->rImage) nMapPack->rImage->LoadDataFromSLF();

    //--Register.
    mAdvancedMapList->AddElementAsTail(pName, nMapPack, &FreeThis);
}

///--[Manipulators]
void AdvUIMap::SetAdvancedLayerToggleable(const char *pName, bool pIsToggleable)
{
    //--Sets if a layer stops rendering when the player pushes a key.
    AdvancedMapPack *rPackage = (AdvancedMapPack *)mAdvancedMapList->GetElementByName(pName);
    if(!rPackage) return;

    //--Set.
    rPackage->mCanBeToggled = pIsToggleable;
}
void AdvUIMap::SetAdvancedLayerChance(const char *pName, int pLoRoll, int pHiRoll)
{
    //--Sets the chance that a given layer will render. The roll is from 0 to 1000. Used for special effects.
    AdvancedMapPack *rPackage = (AdvancedMapPack *)mAdvancedMapList->GetElementByName(pName);
    if(!rPackage) return;

    //--Set.
    rPackage->mRenderChanceLo = pLoRoll;
    rPackage->mRenderChanceHi = pHiRoll;
}
void AdvUIMap::SetAdvancedLayerPlayerRender(const char *pName, const char *pImgPath, bool pFlashes, float pX, float pY)
{
    //--Sets that this layer renders the player at its depth. Also specifies if the player sprite flashes.
    //  Note that flashing is universal. If more than one layer has player sprites for some reason, they all flash.
    AdvancedMapPack *rPackage = (AdvancedMapPack *)mAdvancedMapList->GetElementByName(pName);
    if(!rPackage) return;

    //--Global flashing flag.
    mPlayerSpriteFlashes = pFlashes;

    //--Set.
    rPackage->mRenderPlayer = true;
    rPackage->rPlayerImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);
    rPackage->mPlayerOffX = pX;
    rPackage->mPlayerOffY = pY;

    //--If the image exists, resolve its data.
    if(rPackage->rPlayerImg) rPackage->rPlayerImg->LoadDataFromSLF();
}
void AdvUIMap::SetAdvancedLayerIsFixed(const char *pName, bool pIsFixed)
{
    //--Sets whether this layer is fixed in place. Fixed layers do not scale or pan. Used for frames/UI.
    AdvancedMapPack *rPackage = (AdvancedMapPack *)mAdvancedMapList->GetElementByName(pName);
    if(!rPackage) return;

    //--Set.
    rPackage->mNoPanNoScale = pIsFixed;
}
void AdvUIMap::SetAdvancedLayerReadsStencil(const char *pName, int pStencilVal)
{
    //--If called, the layer will check the given stencil value to render.
    AdvancedMapPack *rPackage = (AdvancedMapPack *)mAdvancedMapList->GetElementByName(pName);
    if(!rPackage) return;

    //--Set.
    rPackage->mReadsStencil = true;
    rPackage->mReadStencil = pStencilVal;
}
void AdvUIMap::SetAdvancedLayerWritesStencil(const char *pName, int pStencilVal)
{
    //--If called, the layer will write the given stencil value when rendering.
    AdvancedMapPack *rPackage = (AdvancedMapPack *)mAdvancedMapList->GetElementByName(pName);
    if(!rPackage) return;

    //--Set.
    rPackage->mWritesStencil = true;
    rPackage->mWriteStencil = pStencilVal;
}

///========================================== Drawing =============================================
void AdvUIMap::RenderAdvancedMapMode()
{
    ///--[ ================ Documentation =============== ]
    //--Called by the base rendering routine, handles rendering advanced map packages.

    ///--[Constants]
    float cXOffset = -mMapFocusX;
    float cYOffset = -mMapFocusY;
    float cScreenX = (VIRTUAL_CANVAS_X * 0.50f);
    float cScreenY = (VIRTUAL_CANVAS_Y * 0.50f);

    ///--[Stencil Setup]
    glEnable(GL_STENCIL_TEST);
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilFunc(GL_ALWAYS, 0, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    glStencilMask(0xFF);

    ///--[Centering]
    //--In order to keep zooming center, center the screen then scale it.
    float cMapCenterX = cScreenX;
    float cMapCenterY = cScreenY;
    glTranslatef(cMapCenterX, cMapCenterY, 0.0f);
    glScalef(mMapZoom.mXCur, mMapZoom.mXCur, 1.0f);
    glTranslatef(-cMapCenterX, -cMapCenterY, 0.0f);

    //--Offset by the player's manually-inputted position, allowing scrolling.
    glTranslatef(cXOffset, cYOffset, 0.0f);

    ///--[ ============== Package Rendering ============= ]
    //--Iterate across the advanced map packs.
    AdvancedMapPack *rMapPack = (AdvancedMapPack *)mAdvancedMapList->PushIterator();
    while(rMapPack)
    {
        ///--[Skip Rendering]
        //--If this layer does not render when toggled off, don't render it.
        if(!mIsShowingHiddenLayers && rMapPack->mCanBeToggled)
        {
            rMapPack = (AdvancedMapPack *)mAdvancedMapList->AutoIterate();
            continue;
        }

        ///--[Stencil Handling]
        //--If the layer does not read or write stencils:
        if(!rMapPack->mReadsStencil && !rMapPack->mWritesStencil)
        {
            glStencilFunc(GL_ALWAYS, 0, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        }
        //--Layer reads stencils.
        else if(rMapPack->mReadsStencil && !rMapPack->mWritesStencil)
        {
            glStencilFunc(GL_EQUAL, rMapPack->mReadStencil, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        }
        //--Layer writes stencils.
        else if(!rMapPack->mReadsStencil && rMapPack->mWritesStencil)
        {
            glStencilFunc(GL_ALWAYS, rMapPack->mWriteStencil, 0xFF);
            glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        }
        //--Layer does both. Use the read value. Doesn't matter much since this doesn't really write anything different.
        else if(rMapPack->mReadsStencil && rMapPack->mWritesStencil)
        {
            glStencilFunc(GL_EQUAL, rMapPack->mReadStencil, 0xFF);
            glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        }

        //--This layer shows the player's position.
        if(rMapPack->mRenderPlayer)
        {
            //--If the player does not have an image, render a white dot using their relative position within the room.
            //  This provides a high-accuracy position. It's used for the chapter 5 pdu.
            if(!rMapPack->rPlayerImg)
            {
                //--Player's level position is needed.
                TilemapActor *rActor = AdventureLevel::Fetch()->LocatePlayerActor();//Can't fail because the level owns the menu!
                if(rActor)
                {
                    float tPlayerX = mMapUsePlayerXBase + (rActor->GetWorldX() * mMapPlayerCoordScaleX);
                    float tPlayerY = mMapUsePlayerYBase + (rActor->GetWorldY() * mMapPlayerCoordScaleY);
                    glDisable(GL_TEXTURE_2D);
                    glBegin(GL_QUADS);
                        glVertex2f(tPlayerX - 5.0f, tPlayerY - 5.0f);
                        glVertex2f(tPlayerX + 5.0f, tPlayerY - 5.0f);
                        glVertex2f(tPlayerX + 5.0f, tPlayerY + 5.0f);
                        glVertex2f(tPlayerX - 5.0f, tPlayerY + 5.0f);
                    glEnd();
                    glEnable(GL_TEXTURE_2D);
                }

                //--If this flag is tripped, reposition the map to center on the player.
                if(mResetFocusOnPlayer)
                {
                    float tPlayerX = mMapUsePlayerXBase + (rActor->GetWorldX() * mMapPlayerCoordScaleX);
                    float tPlayerY = mMapUsePlayerYBase + (rActor->GetWorldY() * mMapPlayerCoordScaleY);
                    mResetFocusOnPlayer = false;
                    RepositionMap(tPlayerX - (VIRTUAL_CANVAS_X * 0.50f) + 30.0f, tPlayerY - (VIRTUAL_CANVAS_Y * 0.50f) + 30.0f);
                }
            }
            //--Otherwise, we render the player's associated image.
            else
            {
                //--Don't render if flashing and not at the right time.
                if(!mPlayerSpriteFlashes || (mPlayerSpriteTimer % ADVMENU_PLAYER_FLASH_TICKS) < ADVMENU_PLAYER_FLASH_INTERVAL)
                {
                    rMapPack->rPlayerImg->Draw(rMapPack->mPlayerOffX, rMapPack->mPlayerOffY);
                }

                //--If this flag is tripped, reposition the map to center on the player.
                if(mResetFocusOnPlayer)
                {
                    mResetFocusOnPlayer = false;
                    RepositionMap(rMapPack->mPlayerOffX - (VIRTUAL_CANVAS_X * 0.50f) + 30.0f, rMapPack->mPlayerOffY - (VIRTUAL_CANVAS_Y * 0.50f) + 30.0f);
                }
            }
        }

        //--Skip if the image is not present for some reason.
        if(!rMapPack->rImage)
        {
            rMapPack = (AdvancedMapPack *)mAdvancedMapList->AutoIterate();
            continue;
        }

        //--If the roll is within this pack's range, render.
        if(mAdvancedMapRoll >= rMapPack->mRenderChanceLo && mAdvancedMapRoll <= rMapPack->mRenderChanceHi)
        {
            //--Fixed position rendering.
            if(rMapPack->mNoPanNoScale)
            {
                glTranslatef(cScreenX, cScreenY, 0.0f);
                glScalef(1.0f / mMapZoom.mXCur, 1.0f / mMapZoom.mXCur, 1.0f);
                glTranslatef(-cScreenX, -cScreenY, 0.0f);
                glTranslatef(-cXOffset, -cYOffset, 0.0f);

                rMapPack->rImage->Draw();


                glTranslatef(cXOffset, cYOffset, 0.0f);
                glTranslatef(cScreenX, cScreenY, 0.0f);
                glScalef(mMapZoom.mXCur, mMapZoom.mXCur, 1.0f);
                glTranslatef(-cScreenX, -cScreenY, 0.0f);
                /*
                glScalef(1.0f / mMapZoom.mXCur, 1.0f / mMapZoom.mXCur, 1.0f);
                glTranslatef(-cXOffset, -cYOffset, 0.0f);
                rMapPack->rImage->Draw();
                glTranslatef(cXOffset, cYOffset, 0.0f);
                glScalef(mMapZoom.mXCur, mMapZoom.mXCur, 1.0f);
                */
            }
            //--Normal.
            else
            {
                rMapPack->rImage->Draw();
            }
        }

        //--Next.
        rMapPack = (AdvancedMapPack *)mAdvancedMapList->AutoIterate();
    }

    ///--[Map Pins]
    MapPin *rMapPin = (MapPin *)mMapPinList->PushIterator();
    while(rMapPin)
    {
        //--Render.
        if(rMapPin->rImage)
        {
            float cHfWid = rMapPin->rImage->GetWidth() * -0.50f;
            float cHeigh = rMapPin->rImage->GetHeight() * -1.0f;
            rMapPin->rImage->Draw(rMapPin->mX + cHfWid, rMapPin->mY + cHeigh);
        }

        //--Next.
        rMapPin = (MapPin *)mMapPinList->AutoIterate();
    }

    ///--[ ================== Clean Up ================== ]
    //--Unset stencils.
    glDisable(GL_STENCIL_TEST);

    //--Clean up translations.
    glTranslatef(-cXOffset, -cYOffset, 0.0f);

    //--Scale out.
    glTranslatef(cMapCenterX, cMapCenterY, 0.0f);
    glScalef(1.0f / mMapZoom.mXCur, 1.0f / mMapZoom.mXCur, 1.0f);
    glTranslatef(-cMapCenterX, -cMapCenterY, 0.0f);

    ///--[ ================ Diagnostics ================= ]
    /*
    if(false)
    {
        float cHei = Images.BaseMenu.rMainlineFont->GetTextHeight();
        Images.BaseMenu.rMainlineFont->DrawTextArgs(0.0f, cHei * 0.0f, 0, 1.0f, "XFocus: %i", (int)mMapFocusX);
        Images.BaseMenu.rMainlineFont->DrawTextArgs(0.0f, cHei * 1.0f, 0, 1.0f, "YFocus: %i", (int)mMapFocusY);
        Images.BaseMenu.rMainlineFont->DrawTextArgs(0.0f, cHei * 2.0f, 0, 1.0f, "XFinal: %i", (int)(cMapCenterX + ((cXOffset - cMapCenterX) * mMapZoom.mXCur)) );
        Images.BaseMenu.rMainlineFont->DrawTextArgs(0.0f, cHei * 3.0f, 0, 1.0f, "YFinal: %i", (int)(cMapCenterY + ((cYOffset - cMapCenterY) * mMapZoom.mXCur)) );
    }
    */
}
