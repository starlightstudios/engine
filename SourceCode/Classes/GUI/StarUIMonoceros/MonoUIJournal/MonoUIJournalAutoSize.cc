//--Base
#include "MonoUIJournal.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DebugManager.h"

void MonoUIJournal::SetCombatGlossaryText(const char *pInternalName, int pSlot, int pLine, const char *pText, const char *pFont, int pFlags)
{
    //--Error check.
    AdvMenuJournalCombatGlossaryEntry *rEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pText || !pFont) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rEntry->mPiecesTotal) return;
    if(pLine < 0 || pLine >= rEntry->mPieces[pSlot].mLinesTotal) return;

    //--Replace any [PCT] tags with percents. This is the final step before rendering.
    if(true)
    {
        //--Run the subdivide routine on this using both [ and ] as delimiters. This will make every
        //  odd numbered entry a tag, and even-numbered entries the strings between them.
        StarAutoBuffer *nNewBuf = new StarAutoBuffer();
        StarLinkedList *tSplitString = Subdivide::SubdivideStringToListTags(pText);

        //--Run across the strings. Even numbered strings are appended directly, odd numbered strings
        //  check for special tags.
        int i = 0;
        char *rSubString = (char *)tSplitString->PushIterator();
        while(rSubString)
        {
            //--Even numbered.
            if(i % 2 == 0)
            {
                nNewBuf->AppendStringWithoutNull(rSubString);
            }
            //--Odd numbered.
            else
            {
                if(!strcmp(rSubString, "[PCT]"))
                {
                    nNewBuf->AppendStringWithoutNull("%%");
                }
                else
                {
                    nNewBuf->AppendStringWithoutNull(rSubString);
                }
            }

            //--Next.
            i ++;
            rSubString = (char *)tSplitString->AutoIterate();
        }

        //--Finish the string up.
        nNewBuf->AppendNull();

        //--Apply the final string using the data from the autobuffer.
        rEntry->mPieces[pSlot].mLines[pLine] = (char *)nNewBuf->LiberateData();
        rEntry->mPieces[pSlot].mFlags[pLine] = pFlags;

        //--Clean up.
        delete nNewBuf;
        delete tSplitString;
    }

    //--Resolve font.
    if(!strcasecmp(pFont, "Big Header"))
    {
        rEntry->mPieces[pSlot].mrFonts[pLine] = MonoImages.rFontDoubleHeading;
    }
    else if(!strcasecmp(pFont, "Header"))
    {
        rEntry->mPieces[pSlot].mrFonts[pLine] = MonoImages.rFontHeading;
    }
    else
    {
        rEntry->mPieces[pSlot].mrFonts[pLine] = MonoImages.rFontDescription;
    }
}
void MonoUIJournal::SetBestiaryDescriptionAuto(const char *pInternalName, const char *pDescription)
{
    ///--[Documentation]
    //--Same as the base version, but has explicit sizing meant for the bestiary.
    if(!pInternalName || !pDescription) return;

    //--Locate entry.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!rEntry) return;

    ///--[Run Subdivide]
    //--Flags.
    uint32_t tFlags = SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK;

    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pDescription, tFlags, MonoImages.rFontDescription, 360.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "MonoUIJournal:SetBestiaryDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        //--Liberate the string.
        tStringList->SetRandomPointerToHead();
        char *rString = (char *)tStringList->LiberateRandomPointerEntry();
        rEntry->mDescriptionLines[i] = rString;
    }

    //--Clean the list up.
    delete tStringList;
}
void MonoUIJournal::SetQuestDescriptionAuto(const char *pInternalName, const char *pDescription)
{
    ///--[Documentation]
    //--Same as the base version, but has explicit sizing meant for the quest log.
    if(!pInternalName || !pDescription) return;

    //--Get entry.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mQuestEntries->GetElementByName(pInternalName);
    if(!rEntry) return;

    ///--[Run Subdivide]
    //--Flags.
    uint32_t tFlags = SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK;

    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pDescription, tFlags, MonoImages.rFontMainline, 760.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "MonoUIJournal:SetQuestDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        //--Liberate the string.
        tStringList->SetRandomPointerToHead();
        char *rString = (char *)tStringList->LiberateRandomPointerEntry();
        rEntry->mDescriptionLines[i] = rString;
    }

    //--Clean the list up.
    delete tStringList;
}
void MonoUIJournal::SetLocationDescriptionAuto(const char *pInternalName, const char *pDescription)
{
    ///--[Documentation]
    //--Same as the base version, but has explicit sizing meant for the quest log.
    if(!pInternalName || !pDescription) return;

    //--Get entry.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementByName(pInternalName);
    if(!rEntry) return;

    ///--[Run Subdivide]
    //--Flags.
    uint32_t tFlags = SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK;

    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pDescription, tFlags, MonoImages.rFontMainline, 760.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "MonoUIJournal:SetLocationDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        //--Liberate the string.
        tStringList->SetRandomPointerToHead();
        char *rString = (char *)tStringList->LiberateRandomPointerEntry();
        rEntry->mDescriptionLines[i] = rString;
    }

    //--Clean the list up.
    delete tStringList;
}
void MonoUIJournal::SetCombatGlossaryAuto(const char *pInternalName, int pSlot, int pX, int pY, const char *pText, const char *pFont, int pFlags)
{
    ///--[Documentation]
    //--Same as the base version, but uses font and sizing.
    AdvMenuJournalCombatGlossaryEntry *rEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pText) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rEntry->mPiecesTotal) return;

    ///--[Run Subdivide]
    //--Flags.
    uint32_t tFlags = SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK;

    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pText, tFlags, MonoImages.rFontDescription, 900.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "MonoUIJournal:SetCombatGlossaryAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->mPieces[pSlot].mX = pX;
    rEntry->mPieces[pSlot].mY = pY;
    rEntry->mPieces[pSlot].AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        const char *rString = (const char *)tStringList->GetElementBySlot(i);
        SetCombatGlossaryText(pInternalName, pSlot, i, rString, pFont, pFlags);
    }

    //--Clean the list up.
    delete tStringList;
}
