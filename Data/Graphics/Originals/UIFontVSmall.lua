--[UI Font: Small]
--Edging script.

--Local Functions
local fnCreateLetter = function(sLetterString, iLft, iTop, iWid, iHei, iStdSpacing)
	iWid = iWid + 2
	iHei = iHei + 1
	StarFont_SetBitmapCharacter(string.byte(sLetterString), iLft, iTop, iLft + iWid, iTop + iHei, iWid + iStdSpacing)
end

--Letters
fnCreateLetter("A",  30, 67, 11, 11, 1)
fnCreateLetter("B",  45, 67,  8, 11, 1)
fnCreateLetter("C",  59, 67,  8, 11, 1)
fnCreateLetter("D",  72, 67, 10, 11, 1)
fnCreateLetter("E",  87, 67,  9, 11, 1)
fnCreateLetter("F", 100, 67,  7, 11, 1)
fnCreateLetter("G", 113, 67, 10, 11, 1)
fnCreateLetter("H", 127, 67, 12, 11, 1)
fnCreateLetter("I", 143, 67,  5, 11, 1)
fnCreateLetter("J", 151, 67,  7, 14, 1)
fnCreateLetter("K", 163, 67, 10, 11, 1)
fnCreateLetter("L", 178, 67,  8, 11, 1)
fnCreateLetter("M", 191, 67, 13, 11, 1)
fnCreateLetter("N", 209, 67, 11, 11, 1)
fnCreateLetter("O", 226, 67,  9, 11, 1)
fnCreateLetter("P", 240, 67,  8, 11, 1)
fnCreateLetter("Q", 253, 67, 10, 14, 1)
fnCreateLetter("R", 267, 67, 10, 11, 1)
fnCreateLetter("S", 282, 67,  6, 11, 1)
fnCreateLetter("T", 293, 67, 10, 11, 1)
fnCreateLetter("U", 307, 67, 11, 11, 1)
fnCreateLetter("V", 323, 67, 11, 11, 1)
fnCreateLetter("W", 337, 67, 14, 11, 1)
fnCreateLetter("X", 355, 67, 11, 11, 1)
fnCreateLetter("Y", 370, 67,  9, 11, 1)
fnCreateLetter("Z", 384, 67,  8, 11, 1)

fnCreateLetter("a",  31, 27,  6, 11, 1)
fnCreateLetter("b",  41, 27,  7, 11, 1)
fnCreateLetter("c",  54, 27,  5, 11, 1)
fnCreateLetter("d",  65, 27,  4, 11, 1)
fnCreateLetter("e",  77, 27,  5, 11, 1)
fnCreateLetter("f",  88, 27,  6, 11, 1)
fnCreateLetter("g",  96, 27,  8, 15, 1)
fnCreateLetter("h", 109, 27,  8, 11, 1)
fnCreateLetter("i", 123, 27,  3, 11, 1)
fnCreateLetter("j", 128, 27,  4, 15, 1)
fnCreateLetter("k", 138, 27,  8, 11, 1)
fnCreateLetter("l", 151, 27,  3, 11, 1)
fnCreateLetter("m", 159, 27, 13, 11, 1)
fnCreateLetter("n", 177, 27,  8, 11, 1)
fnCreateLetter("o", 191, 27,  6, 11, 1)
fnCreateLetter("p", 202, 27,  7, 15, 1)
fnCreateLetter("q", 215, 27,  6, 15, 1)
fnCreateLetter("r", 227, 27,  5, 11, 1)
fnCreateLetter("s", 236, 27,  5, 11, 1)
fnCreateLetter("t", 246, 27,  4, 11, 1)
fnCreateLetter("u", 255, 27,  8, 11, 1)
fnCreateLetter("v", 268, 27,  8, 11, 1)
fnCreateLetter("w", 280, 27, 11, 11, 1)
fnCreateLetter("x", 295, 27,  8, 11, 1)
fnCreateLetter("y", 307, 27,  8, 16, 1)
fnCreateLetter("z", 319, 27,  6, 11, 1)

--Numbers
fnCreateLetter("1",  32, 138,  5, 11, 1)
fnCreateLetter("2",  44, 138,  6, 11, 1)
fnCreateLetter("3",  55, 138,  6, 11, 1)
fnCreateLetter("4",  68, 138,  7, 11, 1)
fnCreateLetter("5",  80, 138,  6, 11, 1)
fnCreateLetter("6",  91, 138,  7, 11, 1)
fnCreateLetter("7", 103, 138,  8, 11, 1)
fnCreateLetter("8", 116, 138,  6, 11, 1)
fnCreateLetter("9", 128, 138,  6, 11, 1)
fnCreateLetter("0", 139, 138,  7, 11, 1)

--Misc
fnCreateLetter(" ",    0,   0,  4, 11, 1)
fnCreateLetter("/",  207, 105,  4, 11, 1)
fnCreateLetter("'",  225, 105,  2, 11, 1)
fnCreateLetter("\"", 231, 105,  4, 11, 1)
fnCreateLetter(";",  241, 105,  2, 11, 1)
fnCreateLetter(":",  249, 105,  1, 11, 1)
fnCreateLetter("[",  192, 105,  3, 11, 1)
fnCreateLetter("]",  200, 105,  3, 11, 1)
fnCreateLetter("-",  183, 105,  4, 11, 1)
fnCreateLetter("+",  159, 105,  6, 11, 1)
fnCreateLetter("_",  148, 105,  5, 11, 1)
fnCreateLetter("=",  171, 105,  8, 11, 1)
fnCreateLetter("!",   30, 105,  3, 11, 1)
fnCreateLetter("@",   38, 105, 11, 15, 1)
fnCreateLetter("#",   54, 105,  8, 11, 1)
fnCreateLetter("$",   67, 105,  6, 11, 1)
fnCreateLetter("%",   79, 105, 11, 12, 1)
fnCreateLetter("^",   95, 105,  6, 11, 1)
fnCreateLetter("&",  106, 105, 10, 11, 1)
fnCreateLetter("*",  121, 105,  5, 11, 1)
fnCreateLetter("(",  132, 105,  4, 11, 1)
fnCreateLetter(")",  140, 105,  4, 11, 1)
fnCreateLetter("?",  216, 105,  5, 11, 1)
fnCreateLetter("~",    0, 105,  1, 11, 1)
fnCreateLetter("<",  269, 105,  7, 11, 1)
fnCreateLetter(">",  280, 105,  7, 11, 1)

fnCreateLetter(".",  263, 105,  1, 17, 1)
fnCreateLetter(",",  256, 105,  2, 21, 1)







