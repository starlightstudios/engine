//--Base
#include "AdvCombatAnimation.h"

//--Classes
#include "StarBitmap.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
AdvCombatAnimation::AdvCombatAnimation()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATANIMATION;

    //--[AdvCombatAnimation]
    //--System
    //--Position
    mXOffset = 0.0f;
    mYOffset = 0.0f;

    //--Storage
    mTicksPerFrame = 1.0f;
    mTotalTicks = 1;
    mImagesTotal = 0;
    mrImages = NULL;

    //--Scroll/Fade Functionality
    mIsScrollFadeUp = false;
    mIsScrollFadeDn = false;

    //--Instances
    mInstanceList = new StarLinkedList(true);
}
AdvCombatAnimation::~AdvCombatAnimation()
{
    free(mrImages);
    delete mInstanceList;
}

///===================================== Property Queries =========================================
bool AdvCombatAnimation::HasAnyInstances()
{
    if(mInstanceList->GetListSize() > 0) return true;
    return false;
}

///======================================== Manipulators ==========================================
void AdvCombatAnimation::SetOffsets(float pX, float pY)
{
    mXOffset = pX;
    mYOffset = pY;
}
void AdvCombatAnimation::SetTicksPerFrame(float pTicksPerFrame)
{
    mTicksPerFrame = pTicksPerFrame;
    if(mTicksPerFrame < 0.01f) mTicksPerFrame = 0.01f;
    mTotalTicks = mImagesTotal * mTicksPerFrame;
}
void AdvCombatAnimation::AllocateFrames(int pTotal)
{
    //--Dealloacte.
    free(mrImages);

    //--Reset.
    mImagesTotal = 0;
    mrImages = NULL;
    mTotalTicks = 0.0f;
    if(pTotal < 1) return;

    //--Allocate.
    mImagesTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mrImages = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mImagesTotal);
    memset(mrImages, 0, sizeof(StarBitmap **) * mImagesTotal);
    mTotalTicks = mImagesTotal * mTicksPerFrame;
}
void AdvCombatAnimation::OverrideTotalTicks(int pTotal)
{
    mTotalTicks = pTotal;
    if(mTotalTicks < 1) mTotalTicks = 1;
}
void AdvCombatAnimation::SetFrame(int pSlot, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mImagesTotal) return;
    mrImages[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatAnimation::SetScrollUpFunction()
{
    mIsScrollFadeUp = true;
}
void AdvCombatAnimation::SetScrollDnFunction()
{
    mIsScrollFadeDn = true;
}
void AdvCombatAnimation::CreateInstance(const char *pRefname, int pStartTicks, float pX, float pY)
{
    if(!pRefname) return;
    AnimationInstance *nInstance = (AnimationInstance *)starmemoryalloc(sizeof(AnimationInstance));
    nInstance->Initialize();
    nInstance->mXCenter = pX;
    nInstance->mYCenter = pY;
    nInstance->mTimer = pStartTicks;
    mInstanceList->AddElement(pRefname, nInstance, &FreeThis);
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void AdvCombatAnimation::Update()
{
    AnimationInstance *rInstance = (AnimationInstance *)mInstanceList->SetToHeadAndReturn();
    while(rInstance)
    {
        rInstance->mTimer ++;
        if(rInstance->mTimer >= mTotalTicks) mInstanceList->RemoveRandomPointerEntry();
        rInstance = (AnimationInstance *)mInstanceList->IncrementAndGetRandomPointerEntry();
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void AdvCombatAnimation::Render()
{
    ///--[Documentation]
    //--Renders the given animation for each instance it exists in. This rendering can be split into
    //  several different modes based on the flags in the class.

    ///--[Iterate]
    AnimationInstance *rInstance = (AnimationInstance *)mInstanceList->PushIterator();
    while(rInstance)
    {
        ///--[Basic Behavior]
        //--Most animations run through their frames individually with a given ticks-per-frame.
        if(!mIsScrollFadeUp && !mIsScrollFadeDn)
        {
            //--Resolve frame.
            int tFrame = rInstance->mTimer / mTicksPerFrame;
            if(tFrame < 0 || tFrame >= mImagesTotal || !mrImages[tFrame])
            {
                rInstance = (AnimationInstance *)mInstanceList->AutoIterate();
                continue;
            }

            //--Render.
            mrImages[tFrame]->Draw(rInstance->mXCenter + mXOffset, rInstance->mYCenter + mYOffset);
        }
        ///--[Fade-In, Scroll Up]
        //--Advanced behavior. The 0th image fades in and scrolls upwards. Often used for buffs.
        else if(mIsScrollFadeUp)
        {
            //--Resolve 0th frame.
            if(mImagesTotal < 1 || !mrImages[0])
            {
                rInstance = (AnimationInstance *)mInstanceList->AutoIterate();
                continue;
            }

            //--Constants.
            int tStageZeroTick = mTotalTicks * 0.25f;
            int tStageOneTick  = mTotalTicks * 0.75f;
            float tFadeInTime  = mTotalTicks * 0.25f;
            float tFadeOutTime = mTotalTicks * 0.25f;
            float cYOffset     = 50.0f;

            //--Resolve basic positions.
            float tXPos = rInstance->mXCenter + mXOffset;
            float tYPos = rInstance->mYCenter + mYOffset;

            //--Fade handler. Alpha increases to the 1/3rd counter, holds at 1.0f to 2/3rds, then fades to 0.0f.
            float tAlpha = 1.0f;
            if(rInstance->mTimer < tStageZeroTick)
            {
                //--Percentage.
                float tPercent = EasingFunction::QuadraticOut(rInstance->mTimer, tFadeInTime);

                //--Alpha is a direct set.
                tAlpha = tPercent;

                //--Y position.
                tYPos = tYPos + (cYOffset * (1.0f - tPercent));
            }
            else if(rInstance->mTimer < tStageOneTick)
            {
                tAlpha = 1.0f;
            }
            //--Fade out.
            else
            {
                //--Percentage.
                float tPercent = EasingFunction::QuadraticOut(rInstance->mTimer - tStageOneTick, tFadeOutTime);

                //--Alpha is a direct set.
                tAlpha = 1.0f - tPercent;

                //--Y position.
                tYPos = tYPos - (cYOffset * (tPercent));
            }

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);
            mrImages[0]->Draw(tXPos, tYPos);
            StarlightColor::ClearMixer();
        }
        ///--[Fade-In, Scroll Down]
        //--Advanced behavior. The 0th images fades in and scrolls downwards. Often used for debuffs.
        else if(mIsScrollFadeDn)
        {

        }

        ///--[Next Instance]
        rInstance = (AnimationInstance *)mInstanceList->AutoIterate();
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
