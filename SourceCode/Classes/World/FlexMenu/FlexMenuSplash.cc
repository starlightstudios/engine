//--Base
#include "FlexMenu.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "ControlManager.h"
#include "MapManager.h"

///========================================== System ==============================================
///===================================== Property Queries =========================================
bool FlexMenu::IsSplashMode()
{
    return (mSplashState > FM_SPLASH_STATE_NONE);
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void FlexMenu::UpdateSplashMode()
{
    ///--[Documentation and Setup]
    //--Displays the splash screen, showing the Bottled Starlight logo. Quickly exits if the player
    //  pushes any button.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Always run.
    mSplashTimer ++;
    mSplashStateTimer ++;

    //--Acceleration case.
    if(mAccelerateSplashState)
    {
        mSplashStateTimer += 5;
    }

    //--Fading in.
    if(mSplashState == FM_SPLASH_STATE_FADEIN)
    {
        if(mSplashStateTimer >= FM_SPLASH_FADEIN_TICKS)
        {
            mSplashStateTimer = 0;
            mSplashState = FM_SPLASH_STATE_HOLD;
        }
    }
    //--Holding.
    else if(mSplashState == FM_SPLASH_STATE_HOLD)
    {
        if(mSplashStateTimer >= FM_SPLASH_HOLD_TICKS || mAccelerateSplashState)
        {
            mSplashStateTimer = 0;
            mSplashState = FM_SPLASH_STATE_FADEOUT;
        }
    }
    //--Fading out.
    else if(mSplashState == FM_SPLASH_STATE_FADEOUT)
    {
        if(mSplashStateTimer >= FM_SPLASH_HOLD_TICKS)
        {
            mSplashState = FM_SPLASH_STATE_NONE;
            MapManager::Fetch()->BeginFade();
        }
    }

    ///--[Player Input]
    //--Pushing any button exits the mode.
    if(rControlManager->IsAnyKeyPressed()) mAccelerateSplashState = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void FlexMenu::RenderSplashMode()
{
    ///--[Base Render]
    //--Flag.
    MapManager::xHasRenderedMenus = true;

    //--Backing.
    StarBitmap::DrawFullBlack(1.0f);

    //--Resolve frame.
    int tFrame = ((int)((float)mSplashTimer / FM_SPLASH_TPF)) % FM_SPLASH_FRAMES_TOTAL;

    //--Render.
    if(rSplashImg[tFrame]) rSplashImg[tFrame]->Draw();

    ///--[State Overlays]
    //--Fading in.
    if(mSplashState == FM_SPLASH_STATE_FADEIN)
    {
        float tPct = EasingFunction::QuadraticIn(mSplashStateTimer, FM_SPLASH_FADEIN_TICKS);
        StarBitmap::DrawFullBlack(1.0f - tPct);
    }
    //--Holding. No overlay.
    else if(mSplashState == FM_SPLASH_STATE_HOLD)
    {
    }
    //--Fading out.
    else if(mSplashState == FM_SPLASH_STATE_FADEOUT)
    {
        float tPct = EasingFunction::QuadraticIn(mSplashStateTimer, FM_SPLASH_FADEOUT_TICKS);
        StarBitmap::DrawFullBlack(tPct);
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
