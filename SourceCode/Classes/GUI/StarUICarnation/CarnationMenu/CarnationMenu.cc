//--Base
#include "CarnationMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"
#include "CarnUIClassChange.h"
#include "CarnUIEquip.h"
#include "CarnUIEvent.h"
#include "CarnUIHealParty.h"
#include "CarnUIFileSelect.h"
#include "CarnUIInventory.h"
#include "CarnUIOptions.h"
#include "CarnUIRegem.h"
#include "CarnUISkills.h"
#include "CarnUIStatus.h"
#include "CarnUIVendor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarPointerSeries.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///--[Debug]
//#define CARNMENU_DEBUG
#ifdef CARNMENU_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
CarnationMenu::CarnationMenu()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    mType = POINTER_TYPE_CARNATIONMENU;

    ///--[ ===== AdventureMenu ====== ]
    ///--[ ===== CarnationMenu ====== ]
    ///--[System]
    mCarnationMode = CARNMENU_MODE_STATUS;
    mMouseX = 0;
    mMouseY = 0;
    mMouseZ = 0;

    ///--[Save Mode]
    mSelectedSaveFile = -1;

    ///--[Shop Mode]
    //--Stat indexes.
    mStatIndexes[0] = CARNATION_STAT_HP;
    mStatIndexes[1] = CARNATION_STAT_SP;
    mStatIndexes[2] = CARNATION_STAT_ATTACK;
    mStatIndexes[3] = CARNATION_STAT_MAGICATTACK;
    mStatIndexes[4] = CARNATION_STAT_DEFENSE;
    mStatIndexes[5] = CARNATION_STAT_MAGICDEFENSE;
    mStatIndexes[6] = CARNATION_STAT_INITIATIVE;
    mStatIndexes[7] = CARNATION_STAT_LUCK;

    //--Stat Titles.
    strcpy(mStatTitles[0], "HP");
    strcpy(mStatTitles[1], "SP");
    strcpy(mStatTitles[2], "Atk");
    strcpy(mStatTitles[3], "MAtk");
    strcpy(mStatTitles[4], "Def");
    strcpy(mStatTitles[5], "MDef");
    strcpy(mStatTitles[6], "Ini");
    strcpy(mStatTitles[7], "Lck");

    ///--[Event Mode]
    mIsEventMode = false;
    mrEventComponents = new StarLinkedList(false);

    ///--[Class Change Mode]
    mIsClassChangeMode = false;
    mrClassChangeComponents = new StarLinkedList(false);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    ///--[Components]
    //--Remove unused components.
    UnregisterUI("Doctor", mrMainMenuComponents);

    //--Build components.
    RegisterUI("ClassChange", new CarnUIClassChange(), AM_MODE_NONE,            mrClassChangeComponents);
    RegisterUI("Event",       new CarnUIEvent(),       AM_MODE_NONE,            mrEventComponents);
    RegisterUI("Vendor",      new CarnUIVendor(),      AM_MODE_NONE,            mrVendorComponents);
    RegisterUI("Equip",       new CarnUIEquip(),       CARNMENU_MODE_EQUIP,     mrMainMenuComponents);
    RegisterUI("Inventory",   new CarnUIInventory(),   CARNMENU_MODE_INVENTORY, mrMainMenuComponents);
    RegisterUI("Skills",      new CarnUISkills(),      CARNMENU_MODE_SKILLS,    mrMainMenuComponents);
    RegisterUI("Status",      new CarnUIStatus(),      CARNMENU_MODE_STATUS,    mrMainMenuComponents);
    RegisterUI("Options",     new CarnUIOptions(),     CARNMENU_MODE_OPTIONS,   mrMainMenuComponents);
    RegisterUI("File Select", new CarnUIFileSelect(),  CARNMENU_MODE_SAVE,      mrMainMenuComponents);
    RegisterUI("Regem",       new CarnUIRegem(),       CARNMENU_MODE_REGEM,     mrMainMenuComponents);//Is accessed from the inventory
    RegisterUI("Heal Party",  new CarnUIHealParty(),   CARNMENU_MODE_HEALPARTY, mrMainMenuComponents);//Is accessed from the inventory

    //--List checking.
    bool tImagesResolved = true;
    StarUIPiece *rUIPiece = (StarUIPiece *)mAllComponents->PushIterator();
    while(rUIPiece)
    {
        rUIPiece->Construct();
        rUIPiece->RunTranslation();
        tImagesResolved = (tImagesResolved && rUIPiece->DidImagesResolve());
        rUIPiece = (StarUIPiece *)mAllComponents->AutoIterate();
    }

    ///--[Debug]
    if(!tImagesResolved)
    {
        //--Debug.
        fprintf(stderr, "Warning, error in Carnation Menu. Image/Font failed to resolve.\n");
        fprintf(stderr, " Printing results from sub-components.\n");

        //--Component UI.
        StarUIPiece *rUIPiece = (StarUIPiece *)mAllComponents->PushIterator();
        while(rUIPiece)
        {
            if(!rUIPiece->DidImagesResolve()) rUIPiece->ImageDiagnostics();
            rUIPiece = (StarUIPiece *)mAllComponents->AutoIterate();
        }
    }
}
CarnationMenu::~CarnationMenu()
{
    delete mrEventComponents;
    delete mrClassChangeComponents;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CarnationMenu::Show()
{
    //--Base call.
    AdventureMenu::Show();

    //--Carnation flags.
    mCarnationMode = CARNMENU_MODE_STATUS;
    ActivateComponentInListName(mrMainMenuComponents, "Status");

    //--All characters must rebuild their portrait info.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    if(!rAdvCombat) return;

    //--Party size.
    int tPartySize = rAdvCombat->GetActivePartyCount();
    for(int i = 0; i < tPartySize; i ++)
    {
        AdvCombatEntity *rEntity = rAdvCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        if(!rEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) continue;
        CarnationCombatEntity *rCarnEntity = (CarnationCombatEntity *)rEntity;

        const char *rResponsePath = rCarnEntity->GetResponsePath();
        if(!rResponsePath) continue;

        LuaManager::Fetch()->PushExecPop(rCarnEntity, rResponsePath, 1, "N", 100.0f);
    }
}
void CarnationMenu::ActivateEventMode()
{
    ///--[Documentation]
    //--Activates the UI into event mode. This mode cannot be accessed from key commands.

    ///--[Execution]
    //--Show.
    AdventureMenu::Show();

    //--Switch to event mode.
    mIsEventMode = true;

    //--Make object take foreground.
    ActivateComponentInListName(mrEventComponents, "Event");
}
void CarnationMenu::ActivateClassChangeMode()
{
    ///--[Documentation]
    //--Activates the UI into class change mode. This mode cannot be access from key commands.

    ///--[Execution]
    //--Show.
    AdventureMenu::Show();

    //--Switch to event mode.
    mIsClassChangeMode = true;

    //--Make object take foreground.
    ActivateComponentInListName(mrClassChangeComponents, "ClassChange");
}

///======================================= Core Methods ===========================================
void CarnationMenu::ChangeModeByTabCode(int pCode)
{
    ///--[Documentation]
    //--Given a mode in the series CARNMENU_CHANGEMODE_STATUS, changes the active mode. This is
    //  usually done by the player clicking the tabs at the top of the UI.
    char tBuffer[64];

    //--Switch by mode.
    if(pCode == CARNMENU_CHANGEMODE_STATUS)
    {
        strcpy(tBuffer, "Status");
        mCarnationMode = CARNMENU_MODE_STATUS;
    }
    else if(pCode == CARNMENU_CHANGEMODE_EQUIP)
    {
        strcpy(tBuffer, "Equip");
        mCarnationMode = CARNMENU_MODE_EQUIP;
    }
    else if(pCode == CARNMENU_CHANGEMODE_INVENTORY)
    {
        strcpy(tBuffer, "Inventory");
        mCarnationMode = CARNMENU_MODE_INVENTORY;
    }
    else if(pCode == CARNMENU_CHANGEMODE_OPTIONS)
    {
        strcpy(tBuffer, "Options");
        mCarnationMode = CARNMENU_MODE_OPTIONS;
    }
    else if(pCode == CARNMENU_CHANGEMODE_SAVE)
    {
        strcpy(tBuffer, "File Select");
        mCarnationMode = CARNMENU_MODE_SAVE;
    }
    else if(pCode == CARNMENU_CHANGEMODE_SKILLS)
    {
        strcpy(tBuffer, "Skills");
        mCarnationMode = CARNMENU_MODE_SKILLS;
    }
    else if(pCode == CARNMENU_CHANGEMODE_REGEM)
    {
        strcpy(tBuffer, "Regem");
        mCarnationMode = CARNMENU_MODE_REGEM;
    }
    else if(pCode == CARNMENU_CHANGEMODE_HEALPARTY)
    {
        strcpy(tBuffer, "Heal Party");
        mCarnationMode = CARNMENU_MODE_HEALPARTY;
    }
    //--Not found. Fail.
    else
    {
        return;
    }

    //--Common code.
    ActivateComponentInListName(mrMainMenuComponents, tBuffer);

    //--Locate the entry. It needs to be dynamically casted.
    StarUIPiece *rStarUI = (StarUIPiece *)mrMainMenuComponents->GetElementByName(tBuffer);
    CarnUICommon *rCommonUI = dynamic_cast<CarnUICommon *>(rStarUI);
    if(rCommonUI) rCommonUI->FinishVisTimer();
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void CarnationMenu::Update()
{
    ///--[Documentation and Setup]
    //--Reset this flag.
    mStartedHidingThisTick = false;
    mHandledUpdate = false;

    //--Diagnostics.
    DebugPush(true, "CarnationMenu:Update() - Begin.\n");

    ///--[Timers]
    //--If this object is visible, opacity increases.
    if(mIsVisible)
    {
        if(mMainOpacityTimer < AM_MAIN_VIS_TICKS) mMainOpacityTimer ++;
    }
    //--Object is not visible. Stop update, decrease opacity timer.
    else
    {
        if(mMainOpacityTimer > 0) mMainOpacityTimer --;
    }

    ///--[Opening Tick]
    //--To prevent idiots from opening the menu and then closing it in the same keypress, the first tick is skipped.
    if(mOpenedThisTick)
    {
        mOpenedThisTick = false;
        DebugPop("Finished, same tick as opening.\n");
        return;
    }

    ///--[Event Mode]
    //--Event mode. Has no submodes.
    if(mIsEventMode)
    {
        //--Execute.
        bool tDummyFlag = false;
        int tExitFlag = ListUpdateForeground(tDummyFlag, true, mrEventComponents);

        //--Flagged to exit.
        if(tExitFlag != AM_MODE_NOEXIT)
        {
            Hide();
            mIsEventMode = false;
            mMainOpacityTimer = 0;
        }

        //--Always block the rest of the update.
        return;
    }

    ///--[ClassChange Mode]
    //--Class Change UI, has no submodes.
    if(mIsClassChangeMode)
    {
        //--Execute.
        bool tDummyFlag = false;
        int tExitFlag = ListUpdateForeground(tDummyFlag, true, mrClassChangeComponents);

        //--Flagged to exit.
        if(tExitFlag != AM_MODE_NOEXIT)
        {
            Hide();
            mIsClassChangeMode = false;
            mMainOpacityTimer = 0;
        }

        //--Always block the rest of the update.
        return;
    }

    ///--[Shop Mode]
    //--Shop mode. Passes the update between mutually-exclusive submodes.
    if(mSubmodeCode == AM_SUBMODE_VENDOR)
    {
        //--Execute.
        bool tDummyFlag = false;
        int tExitFlag = ListUpdateForeground(tDummyFlag, true, mrVendorComponents);

        //--Flagged to exit.
        if(tExitFlag != AM_MODE_NOEXIT)
        {
            Hide();
            mSubmodeCode = AM_SUBMODE_NONE;
            mMainOpacityTimer = 0;
        }

        //--Always block the rest of the update.
        return;
    }

    ///--[Base Menu Mode]
    //--Route the update to whichever handler is in charge.
    bool tUpdateAlreadyHandled = false;

    //--Update all component UIs in the list.
    //fprintf(stderr, "Update cycle. Active %i\n", mCarnationMode);
    StarUIPiece *rUIPiece = (StarUIPiece *)mrMainMenuComponents->PushIterator();
    while(rUIPiece)
    {
        //--Run update. If the update was already handled by another UI, or if the current mode
        //  is not this UI's mode, the UI should switch to background mode.
        int tBackwardsCode = rUIPiece->GetCodeBackward();
        //fprintf(stderr, " Update %s: %i, %i\n", rUIPiece->GetSubtype(), tUpdateAlreadyHandled, tUpdateAlreadyHandled || (mCarnationMode != tBackwardsCode));
        tUpdateAlreadyHandled |= rUIPiece->UpdateForeground(tUpdateAlreadyHandled || (mCarnationMode != tBackwardsCode));

        //--If the object flagged to exit:
        if(rUIPiece->IsFlaggingExit())
        {
            //--Unflag the exit in all cases.
            rUIPiece->UnflagExit();

            //--Get the new backwards code. If it's the same as the old one, just hide the menu.
            int tNewBackwardsCode = rUIPiece->GetCodeBackward();
            if(tNewBackwardsCode == tBackwardsCode)
            {
                Hide();
            }
            //--Otherwise, the UI is asking for a mode change. Reset the UI's backwards code and change modes.
            else
            {
                //--Zero the visibility on the current UI.
                CarnUICommon *rCommonUI = dynamic_cast<CarnUICommon *>(rUIPiece);
                rCommonUI->ZeroVisTimer();

                //--Change modes.
                rUIPiece->SetCodeBackward(tBackwardsCode);
                ChangeModeByTabCode(tNewBackwardsCode);
            }
        }

        //--If the close code fired, handle it.
        const char *rCloseCode = rUIPiece->GetCloseBuffer();
        if(rCloseCode[0] != '\0')
        {
            //--If there is a close script, fire it.
            if(mClosingScript) LuaManager::Fetch()->ExecuteLuaFile(mClosingScript, 1, "S", rCloseCode);

            //--Reset the close code.
            rUIPiece->SetCloseCode(NULL);
        }

        //--Next.
        rUIPiece = (StarUIPiece *)mrMainMenuComponents->AutoIterate();
    }

    ///--[Diagnostics]
    DebugPop("Finished, normal mode exited normally.\n");
}
void CarnationMenu::UpdateBackground()
{
    ///--[Documentation]
    //--Update called when in the background. Just handles hiding.
    mHandledUpdate = true;
    mStartedHidingThisTick = false;

    //--Main opacity.
    if(mMainOpacityTimer > 0) mMainOpacityTimer --;

    ///--[Sub-Handlers]
    //--Update all component UIs in the list.
    StarUIPiece *rUIPiece = (StarUIPiece *)mAllComponents->PushIterator();
    while(rUIPiece)
    {
        //--Run update.
        rUIPiece->UpdateBackground();

        //--If the close code fired, handle it. This still occurs in the background.
        const char *rCloseCode = rUIPiece->GetCloseBuffer();
        if(rCloseCode[0] != '\0')
        {
            //--If there is a close script, fire it.
            if(mClosingScript) LuaManager::Fetch()->ExecuteLuaFile(mClosingScript, 1, "S", rCloseCode);

            //--Reset the close code.
            rUIPiece->SetCloseCode(NULL);
        }

        //--Next.
        rUIPiece = (StarUIPiece *)mAllComponents->AutoIterate();
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnationMenu::Render()
{
    ///--[Event Mode]
    //--Event mode, single object render handler.
    if(mIsEventMode)
    {
        //--Render all component UIs in the list.
        StarUIPiece *rUIPiece = (StarUIPiece *)mrEventComponents->PushIterator();
        while(rUIPiece)
        {
            //--Run update.
            rUIPiece->RenderForeground(1.0f);

            //--Next.
            rUIPiece = (StarUIPiece *)mrEventComponents->AutoIterate();
        }
        return;
    }

    ///--[Class Change Mode]
    //--Class Change mode, single object render handler.
    if(mIsClassChangeMode)
    {
        //--Render all component UIs in the list.
        StarUIPiece *rUIPiece = (StarUIPiece *)mrClassChangeComponents->PushIterator();
        while(rUIPiece)
        {
            //--Run update.
            rUIPiece->RenderForeground(1.0f);

            //--Next.
            rUIPiece = (StarUIPiece *)mrClassChangeComponents->AutoIterate();
        }
        return;
    }

    ///--[Shop Mode]
    //--Shopping mode has its own rendering handler.
    if(mSubmodeCode == AM_SUBMODE_VENDOR)
    {
        //--Render all component UIs in the list.
        StarUIPiece *rUIPiece = (StarUIPiece *)mrVendorComponents->PushIterator();
        while(rUIPiece)
        {
            //--Run update.
            rUIPiece->RenderForeground(1.0f);

            //--Next.
            rUIPiece = (StarUIPiece *)mrVendorComponents->AutoIterate();
        }
        return;
    }

    ///--[Base Menu]
    //--Backing.
    float tBackingPct = (float)mMainOpacityTimer / (float)AM_MAIN_VIS_TICKS;
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * tBackingPct));

    //--Render all component UIs in the list.
    StarUIPiece *rUIPiece = (StarUIPiece *)mrMainMenuComponents->PushIterator();
    while(rUIPiece)
    {
        //--Run update.
        rUIPiece->RenderForeground(tBackingPct);

        //--Next.
        rUIPiece = (StarUIPiece *)mrMainMenuComponents->AutoIterate();
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void CarnationMenu::HookToLuaState(lua_State *pLuaState)
{
    /* [System]
       CMenu_SetProperty("Open Event Mode")
       CMenu_SetProperty("Open Class Change Mode")
       Sets the requested property in the CarnationMenu. */
    lua_register(pLuaState, "CMenu_SetProperty", &Hook_CMenu_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CMenu_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    ///--[Static]
    //CMenu_SetProperty("Party Resolve Script", sPath) (Static)

    ///--[Dynamic]
    //--[System]
    //CMenu_SetProperty("Open Event Mode")
    //CMenu_SetProperty("Open Class Change Mode")

    ///--[Arg Resolve]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CMenu_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Statics]
    //--Statics.
    //if(!strcasecmp(rSwitchType, "Party Resolve Script") && tArgs == 2)
    //{
    //    ResetString(AdventureMenu::xChatResolveScript, lua_tostring(L, 2));
    //    return 0;
    //}

    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu || !rMenu->IsOfType(POINTER_TYPE_CARNATIONMENU)) return LuaTypeError("AM_SetProperty", L);

    //--Cast to CarnationMenu.
    CarnationMenu *rCarnMenu = (CarnationMenu *)rMenu;

    ///--[Dynamic System]
    //--Open in Event Mode.
    if(!strcasecmp(rSwitchType, "Open Event Mode") && tArgs == 1)
    {
        rCarnMenu->ActivateEventMode();
    }
    //--Open in ClassChange Mode.
    else if(!strcasecmp(rSwitchType, "Open Class Change Mode") && tArgs == 1)
    {
        rCarnMenu->ActivateClassChangeMode();
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CMenu_SetProperty", rSwitchType, tArgs);
    }

    //--Success.
    return 0;
}
