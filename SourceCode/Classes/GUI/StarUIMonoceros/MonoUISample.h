//Replace MonoUISample with name of UI
//Balance header
//Switch AdvUISample to matching inheritance type
//Delete these instructions

///======================================= MonoUISample ===========================================
//--Description

#pragma once

///========================================= Includes =============================================
#include "AdvUISample.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class MonoUISample : public MonoUICommon, virtual public AdvUISample
{
    protected:
    ///--[System]
    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;

        //--Images
        StarBitmap *rFrame_Base;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUISample();
    virtual ~MonoUISample();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


