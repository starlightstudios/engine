///========================================= AdvUIMap =============================================
//--Map UI, shows the player where they are. Map can be made of multiple layers.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///======================================= Definitions ============================================
#ifndef ADVUIMAP_DEFINITIONS
#define ADVUIMAP_DEFINITIONS

///--[Local Definitions]
//--Timers
#define ADVMENU_MAP_VIS_TICKS 15
#define ADVMENU_MAP_ZOOM_TICKS 30
#define ADVMENU_PLAYER_FLASH_TICKS 60
#define ADVMENU_PLAYER_FLASH_INTERVAL (ADVMENU_PLAYER_FLASH_TICKS / 2)

///======================================== Structures ============================================
//--There is no images pack for this as there is minimal interactable UI.

///--[Map Pin]
typedef struct MapPin
{
    float mX;
    float mY;
    StarBitmap *rImage;
    void Initialize()
    {
        mX = 0.0f;
        mY = 0.0f;
        rImage = NULL;
    }
}MapPin;
#endif

///--[Advanced Map Pack]
//--Contains an image layer when using multi-layered images, as well as a chance for that
//  image to appear. This creates the flashing effect.
#ifndef ADVANCEDMAPPACK
#define ADVANCEDMAPPACK
typedef struct AdvancedMapPack
{
    //--Members
    bool mCanBeToggled;
    StarBitmap *rImage;
    bool mNoPanNoScale;
    int mRenderChanceLo;
    int mRenderChanceHi;

    //--Player Rendering
    bool mRenderPlayer;
    float mPlayerOffX;
    float mPlayerOffY;
    StarBitmap *rPlayerImg;

    //--Stencil Behaviors
    bool mWritesStencil;
    int mWriteStencil;
    bool mReadsStencil;
    int mReadStencil;

    //--Functions
    void Initialize()
    {
        //--System.
        mCanBeToggled = false;
        rImage = NULL;
        mNoPanNoScale = false;
        mRenderChanceLo = 0;
        mRenderChanceHi = 1000;

        //--Player Rendering
        mRenderPlayer = false;
        mPlayerOffX = 0.0f;
        mPlayerOffY = 0.0f;
        rPlayerImg = NULL;

        //--Stencil Behaviors
        mWritesStencil = false;
        mWriteStencil = 0;
        mReadsStencil = false;
        mReadStencil = 0;
    }
}AdvancedMapPack;
#endif // ADVANCEDMAPPACK

///========================================== Classes =============================================
class AdvUIMap : virtual public AdvUICommon
{
    protected:
    ///--[Variables]
    //--System
    bool mIsShowingHiddenLayers;

    //--Basic Map
    bool mIsUsingBasicMap;
    StarBitmap *rBasicMap;
    StarBitmap *rBasicOverlay;

    //--Advanced Map
    int mAdvancedMapRoll;
    int mAdvancedPackRerollTimer;
    StarLinkedList *mAdvancedMapList; //AdvancedMapPack *, master

    //--Player Display
    bool mResetFocusOnPlayer;
    bool mPlayerSpriteFlashes;
    int mPlayerSpriteTimer;
    float mPlayerX;
    float mPlayerY;
    float mMapUsePlayerXBase;
    float mMapUsePlayerYBase;
    float mMapPlayerCoordScaleX;
    float mMapPlayerCoordScaleY;

    //--Map Pins
    StarLinkedList *mMapPinList; //MapPin *, master

    //--Common
    int mMapEdgePadding;
    float mMapFocusX;
    float mMapFocusY;

    //--Zoom Handling
    bool mIsZoomClose;
    EasingPack1D mMapZoom;

    //--Help Strings
    StarlightString *mToggleHiddenLayersString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_MapHelp;

        //--Images
        StarBitmap *rOverlayHeader;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIMap();
    virtual ~AdvUIMap();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    void SetMapPlayerPos(float pPlayerX, float pPlayerY);
    void SetMapInfo(const char *pDLPath, const char *pDLPathOverlay, int pPlayerX, int pPlayerY);
    void SetMapEdgePad(int pPadding);
    void ClearMapPins();
    void RegisterMapPin(float pX, float pY, const char *pImagePath);
    void RepositionMap(int pX, int pY);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    ///--[Basic Map]
    void RenderBasicMapMode();

    ///--[Advanced Map]
    void ClearAdvancedMapLayers();
    void SetAdvancedRemaps(float pX, float pY, float pScaleX, float pScaleY);
    void AddAdvancedLayer(const char *pName, const char *pDLPath);
    void SetAdvancedLayerToggleable(const char *pName, bool pIsToggleable);
    void SetAdvancedLayerChance(const char *pName, int pLoRoll, int pHiRoll);
    void SetAdvancedLayerPlayerRender(const char *pName, const char *pImgPath, bool pFlashes, float pX, float pY);
    void SetAdvancedLayerIsFixed(const char *pName, bool pIsFixed);
    void SetAdvancedLayerReadsStencil(const char *pName, int pStencilVal);
    void SetAdvancedLayerWritesStencil(const char *pName, int pStencilVal);
    void RenderAdvancedMapMode();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvUIMap_SetProperty(bool &sHandled, lua_State *L);
