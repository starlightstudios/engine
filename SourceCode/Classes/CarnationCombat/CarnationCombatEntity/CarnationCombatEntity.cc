//--Base
#include "CarnationCombatEntity.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
CarnationCombatEntity::CarnationCombatEntity()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_CARNATIONCOMBATENTITY;

    ///--[ ==== AdvCombatEntity ===== ]
    ///--[ = CarnationCombatEntity == ]
    ///--[System]
    mIsPlayerControlled = true;
    mStopKnockout = true;

    ///--[Combat]
    mIgnoresProficiency = false;
    mFlatAccuracy = 80;
    mFlatCritRate = 0;

    ///--[Switchable Layers]
    mInjuryTimer = 0;
    mSwitchableLayerName = NULL;
    rInjuredImg = NULL;
    rKnockoutImg = NULL;

    ///--[Sounds]
    mSoundHit   = InitializeString("Carnation|Hit");
    mSoundMiss  = InitializeString("Carnation|Miss");
    mSoundSwing = InitializeString("Carnation|Swing");
    mSoundHeal  = InitializeString("Carnation|Heal");
    mSoundSpell = InitializeString("Carnation|Spell");

    ///--[HP/SP Handling]
    mHPScrollHold = false;
    mCompleteTimersImmediately = false;
    mHPPack.Initialize();
    mSPPack.Initialize();

    ///--[Influence]
    mInfluence = 0;
    mInfluenceMax = 1;
    mInfPack.Initialize();

    ///--[Rendering]
    mEnemyUsesComplexRendering = false;
    mIgnoreRenderOffset = false;
    mRenderRefreshScript = NULL;
    mSavePortraitPath = InitializeString("Null");
    mAnimationTimer = 0;
    mAttackTimer = 0;
    mRenderingLayers = new StarLinkedList(true);
}
CarnationCombatEntity::~CarnationCombatEntity()
{
    free(mSwitchableLayerName);
    free(mSoundHit);
    free(mSoundMiss);
    free(mSoundSwing);
    free(mSoundHeal);
    free(mSoundSpell);
    free(mRenderRefreshScript);
    free(mSavePortraitPath);
    delete mRenderingLayers;
}

///===================================== Property Queries =========================================
bool CarnationCombatEntity::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVCOMBATENTITY) return true;
    if(pType == POINTER_TYPE_CARNATIONCOMBATENTITY) return true;
    return false;
}
bool CarnationCombatEntity::IsPlayerControlled()
{
    return mIsPlayerControlled;
}
const char *CarnationCombatEntity::GetSoundHit()
{
    return mSoundHit;
}
const char *CarnationCombatEntity::GetSoundMiss()
{
    return mSoundMiss;
}
const char *CarnationCombatEntity::GetSoundSwing()
{
    return mSoundSwing;
}
const char *CarnationCombatEntity::GetSoundHeal()
{
    return mSoundHeal;
}
const char *CarnationCombatEntity::GetSoundSpell()
{
    return mSoundSpell;
}
bool CarnationCombatEntity::IsKnockingOut()
{
    return (mIsKnockingOut && !mStopKnockout);
}
bool CarnationCombatEntity::IsKnockoutPending()
{
    if(mHP < 1 && mStopKnockout) return true;
    return false;
}
bool CarnationCombatEntity::HasKnockoutBlock()
{
    return mStopKnockout;
}
char *CarnationCombatEntity::GetSavePortraitPath()
{
    return mSavePortraitPath;
}
float CarnationCombatEntity::GetDisplayInfluence()
{
    return mInfPack.mXCur;
}
bool CarnationCombatEntity::IgnoresProficiency()
{
    return mIgnoresProficiency;
}
int CarnationCombatEntity::GetFlatAccuracy()
{
    return mFlatAccuracy;
}
int CarnationCombatEntity::GetFlatCritRate()
{
    return mFlatCritRate;
}

///======================================= Manipulators ===========================================
void CarnationCombatEntity::SetPlayerControlThisTurn(bool pFlag)
{
    mIsPlayerControlled = pFlag;
}
void CarnationCombatEntity::SetRenderRefreshScript(const char *pPath)
{
    if(!pPath || !strcasecmp(pPath, "NULL"))
    {
        ResetString(mRenderRefreshScript, NULL);
        return;
    }
    ResetString(mRenderRefreshScript, pPath);
}
void CarnationCombatEntity::SetComplexRendering(bool pFlag)
{
    mEnemyUsesComplexRendering = pFlag;
}
void CarnationCombatEntity::SetIgnoreXPosition(bool pFlag)
{
    mIgnoreRenderOffset = pFlag;
}
void CarnationCombatEntity::AddRenderLayer(const char *pName, int pPriority, float pTPF, int pFrames)
{
    ///--[Documentation]
    //--Adds a new rendering layer, or, if the named layer already exists, overwrites its properties.
    if(!pName || pTPF <= 0.0f || pFrames < 1) return;

    ///--[Duplicate Check]
    CCELayer *rCheckLayer = (CCELayer *)mRenderingLayers->GetElementByName(pName);
    if(rCheckLayer)
    {
        rCheckLayer->mMaxFrames = pFrames;
        rCheckLayer->mPriority = pPriority;
        rCheckLayer->mTicksPerFrame = pTPF;
        return;
    }

    ///--[Create New]
    CCELayer *nLayer = (CCELayer *)starmemoryalloc(sizeof(CCELayer));
    nLayer->Initialize();
    nLayer->mMaxFrames = pFrames;
    nLayer->mPriority = pPriority;
    nLayer->mTicksPerFrame = pTPF;
    mRenderingLayers->AddElement(pName, nLayer, &FreeThis);
}
void CarnationCombatEntity::RemoveRenderLayer(const char *pName)
{
    mRenderingLayers->RemoveElementS(pName);
}
void CarnationCombatEntity::ClearRenderLayers()
{
    mRenderingLayers->ClearList();
}
void CarnationCombatEntity::SetRenderLayerFrame(const char *pName, int pSlot, const char *pImagePath)
{
    //--Range check.
    if(pSlot < 0 || pSlot >= CCELAYER_MAX_FRAMES) return;

    //--Verify.
    CCELayer *rCheckLayer = (CCELayer *)mRenderingLayers->GetElementByName(pName);
    if(!rCheckLayer) return;

    //--Set.
    rCheckLayer->rImages[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
}
void CarnationCombatEntity::SetRenderLayerWorld(const char *pName, bool pWorldRender)
{
    //--Verify.
    CCELayer *rCheckLayer = (CCELayer *)mRenderingLayers->GetElementByName(pName);
    if(!rCheckLayer) return;

    //--Set.
    rCheckLayer->mRendersInWorld = pWorldRender;
}
void CarnationCombatEntity::SetRenderLayerDisabled(const char *pName, bool pIsDisabled)
{
    CCELayer *rCheckLayer = (CCELayer *)mRenderingLayers->GetElementByName(pName);
    if(!rCheckLayer) return;
    rCheckLayer->mIsDisabled = pIsDisabled;
}
void CarnationCombatEntity::SetRenderLayerOffsets(const char *pName, float pXOffset, float pYOffset)
{
    CCELayer *rCheckLayer = (CCELayer *)mRenderingLayers->GetElementByName(pName);
    if(!rCheckLayer) return;
    rCheckLayer->mXOffset = pXOffset;
    rCheckLayer->mYOffset = pYOffset;
}
void CarnationCombatEntity::SortRenderLayers()
{
    mRenderingLayers->SortListUsing(&CarnationCombatEntity::LayerCompare);
}
void CarnationCombatEntity::SetSwitchableLayerName(const char *pName)
{
    ResetString(mSwitchableLayerName, pName);
}
void CarnationCombatEntity::SetInjuredImage(const char *pImagePath)
{
    rInjuredImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
}
void CarnationCombatEntity::SetKnockoutImage(const char *pImagePath)
{
    rKnockoutImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
}
void CarnationCombatEntity::Injure()
{
    mHPScrollHold = false;
    mInjuryTimer = CCE_INJURY_TICKS;
}
void CarnationCombatEntity::Heal()
{
    mHPScrollHold = false;
}
void CarnationCombatEntity::Attack()
{
    mAttackTimer = CCE_ATTACK_TICKS;
}
void CarnationCombatEntity::SetHPScrollHold(bool pFlag)
{
    mHPScrollHold = pFlag;
}
void CarnationCombatEntity::CompleteTimers()
{
    mCompleteTimersImmediately = true;
}
void CarnationCombatEntity::ClearKnockoutBlock()
{
    mStopKnockout = false;
}
void CarnationCombatEntity::EditFlag(const char *pString)
{
    ///--[Documentation]
    //--Edits a flag in the datalibrary. By default, flags are stored at "Root/Variables/Combat/[ID]/[FLAGNAME]".
    //  This function expects the format "FLAGNAME|FLAGVALUE" and will set the flag immediately, if it exists.
    if(!pString) return;

    //--Setup.
    int tLen = (int)strlen(pString);

    //--Locate the '|' that splits the strings.
    int tBarLoc = -1;
    for(int i = 0; i < tLen; i ++)
    {
        if(pString[i] == '|')
        {
            tBarLoc = i;
            break;
        }
    }

    //--Error, bar not found.
    if(tBarLoc == -1 || tBarLoc == 0 || tBarLoc == tLen-1)
    {
        fprintf(stderr, "CarnationCombatEntity::EditFlag(): Warning, invalid format %s\n", pString);
        return;
    }

    ///--[Break Strings]
    char tVarBuf[256];
    strncpy(tVarBuf, pString, tBarLoc);
    tVarBuf[tBarLoc+0] = '\0';

    //--Value string.
    int tRem = tLen-tBarLoc-2;
    char tValBuf[256];
    strncpy(tValBuf, &pString[tBarLoc+1], tRem);
    tValBuf[tLen-tBarLoc] = '\0';
    //fprintf(stderr, "Strings: %s %s Rem was %i\n", tVarBuf, tValBuf, tRem);

    ///--[Set Variable]
    //--Create a variable path.
    char tVariablePath[512];
    sprintf(tVariablePath, "Root/Variables/Combat/%i/%s", mUniqueID, tVarBuf);
    //fprintf(stderr, " Variable path: %s\n", tVariablePath);

    //--Verify.
    SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry(tVariablePath);
    if(!rVariable)
    {
        fprintf(stderr, "CarnationCombatEntity::EditFlag(): Warning, variable %s does not exist.\n", tVariablePath);
        return;
    }

    //--Set.
    rVariable->mNumeric = atof(tValBuf);
    //fprintf(stderr, " Set variable to %f\n", rVariable->mNumeric);
}
void CarnationCombatEntity::SetSavePortraitPath(const char *pString)
{
    ResetString(mSavePortraitPath, pString);
}
void CarnationCombatEntity::SetIgnoresProficiency(bool pFlag)
{
    mIgnoresProficiency = pFlag;
}
void CarnationCombatEntity::SetFlatAccuracy(int pAccuracy)
{
    mFlatAccuracy = pAccuracy;
}
void CarnationCombatEntity::SetFlatCritRate(int pCritRate)
{
    mFlatCritRate = pCritRate;
}

///--[Worker Function for the Above Sort]
int CarnationCombatEntity::LayerCompare(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortRenderLayers(). Compares the effect priority number. Ties are in error.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Get the layers.
    CCELayer *rLayerA = (CCELayer *)(*rEntryA)->rData;
    CCELayer *rLayerB = (CCELayer *)(*rEntryB)->rData;

    //--Check their priority.
    if(rLayerA->mPriority < rLayerB->mPriority)
    {
        return -1;
    }
    else if(rLayerA->mPriority > rLayerB->mPriority)
    {
        return 1;
    }

    //--Tie. Return -1.
    return -1;
}

///======================================= Core Methods ===========================================
void CarnationCombatEntity::TripRenderRefresh()
{
    if(!mRenderRefreshScript) return;
    LuaManager::Fetch()->PushExecPop(this, mRenderRefreshScript);
}
void CarnationCombatEntity::FullRestore()
{
    SetHealth(mSummedStatistics.GetStatByIndex(STATS_HPMAX));
    SetMagic(mSummedStatistics.GetStatByIndex(STATS_MPMAX));
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void CarnationCombatEntity::UpdateTimers()
{
    //--Always run the animation timer.
    mAnimationTimer ++;

    //--Update the run-to-zero timers.
    if(mInjuryTimer > 0) mInjuryTimer --;
    if(mAttackTimer > 0) mAttackTimer --;

    //--HP timer. If the HP differs from the display value, move the display to the true value over
    //  time proportional to the difference, but with a cap. This is more dynamic than the base class
    //  version of the same code.
    if(mHP+mShield != (int)mHPPack.mXEnd)
    {
        //--Compute the needed ticks.
        float tDif = fabsf(mHPPack.mXCur - (mHP+mShield));
        int tTicks = (int)(tDif * CCE_TICKS_PER_HP);
        if(tTicks > CCE_TICKS_HP_CAP) tTicks = CCE_TICKS_HP_CAP;

        //--Move.
        mHPPack.MoveTo(mHP+mShield, tTicks);
    }
    if(mMP != (int)mSPPack.mXEnd)
    {
        //--Compute the needed ticks.
        float tDif = fabsf(mSPPack.mXCur - mMP);
        int tTicks = (int)(tDif * CCE_TICKS_PER_SP);
        if(tTicks > CCE_TICKS_SP_CAP) tTicks = CCE_TICKS_SP_CAP;

        //--Move.
        mSPPack.MoveTo(mMP, tTicks);
    }
    if(mInfluence != (int)mInfPack.mXEnd)
    {
        //--Compute the needed ticks.
        float tDif = fabsf(mInfPack.mXCur - mInfluence);
        int tTicks = (int)(tDif * CCE_TICKS_PER_INF);
        if(tTicks > CCE_TICKS_INF_CAP) tTicks = CCE_TICKS_INF_CAP;

        //--Move.
        mInfPack.MoveTo(mInfluence, tTicks);
    }

    //--Increment. If a scroll-hold exists, don't do that.
    if(!mHPScrollHold) mHPPack.Increment(EASING_CODE_LINEAR);
    mSPPack.Increment(EASING_CODE_LINEAR);
    mInfPack.Increment(EASING_CODE_LINEAR);

    //--If this flag was set, unset it and immediately complete the timers.
    if(mCompleteTimersImmediately)
    {
        mCompleteTimersImmediately = false;
        mHPPack.Complete();
        mSPPack.Complete();
        mInfPack.Complete();
    }

    ///--[ ======== Base Call ========]
    ///--[Internal Timers]
    //--Easing packages.
    mHPEasingPack.Increment(EASING_CODE_QUADINOUT);
    mMPEasingPack.Increment(EASING_CODE_QUADINOUT);
    mAdrenalineEasingPack.Increment(EASING_CODE_QUADINOUT);
    mShieldEasingPack.Increment(EASING_CODE_QUADINOUT);
    mStunEasingPack.Increment(EASING_CODE_QUADINOUT);

    //--Fading timer.
    if(mFadeInTimer < mFadeInTimerMax) mFadeInTimer ++;

    //--White flash. Is a toggle.
    if(mIsFlashingWhite)
    {
        if(mWhiteFlashTimer < ADVCE_FLASH_WHITE_TICKS) mWhiteFlashTimer ++;
    }
    else
    {
        if(mWhiteFlashTimer > 0) mWhiteFlashTimer --;
    }

    //--Black flash. Plays as a sequence.
    if(mIsFlashingBlack)
    {
        mBlackFlashTimer ++;
        if(mBlackFlashTimer >= ADVCE_FLASH_BLACK_TICKS) mIsFlashingBlack = false;
    }

    //--Knockout. Plays as a sequence.
    if(mIsKnockingOut && !mStopKnockout)
    {
        mKnockoutTimer ++;
    }

    //--Crossfade.
    mCrossfadeTimer ++;

    //--Shaking.
    if(mShakeTimer > 0)
    {
        mShakeTimer --;
        if(mShakeTimer % ADVCE_SHAKE_PERIODICITY == 1)
        {
            mShakeX = (rand() % ADVCE_SHAKE_SEVERITY) - (ADVCE_SHAKE_SEVERITY / 2);
        }
    }
    else
    {
        mShakeTimer = 0;
        mShakeX = 0.0f;
    }

    ///--[External Timers]
    //--If there is an external animation handler, call it here.
    if(mAnimationScriptPath)
    {
        LuaManager::Fetch()->PushExecPop(this, mAnimationScriptPath, 1, "N", (float)ACE_AI_ANIMATION_CODE_UPDATE);
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
const int   CarnationCombatEntity::xInjuryYOffsetCap = 12;
const float CarnationCombatEntity::xInjuryYOffsetTPF = 1.0f;
const int   CarnationCombatEntity::xInjuryYOffsets[] = {-16, 16, -16, 16, -12, 12, -6, 6, -2, 2, -1, 1};
void CarnationCombatEntity::RenderPortraitAt(float pX, float pY, bool pShowOnlyWorld)
{
    ///--[Documentation]
    //--Renders the layers for this character, making modifications based on special layer names and animations.
    //  First, apply a 32-pixel edge padding. This is to allow out-of-frame overlays.
    //--If the flag pShowOnlyWorld is set, only layers with the flag mRendersInWorld will render.
    pX = pX - 32.0f;
    pY = pY - 32.0f;

    //--If injured, offset the Y position.
    if(mInjuryTimer > 0)
    {
        //--Resolve the index.
        int tIndex = (int)((CCE_INJURY_TICKS - mInjuryTimer) / xInjuryYOffsetTPF);
        if(tIndex < 0) tIndex = 0;
        if(tIndex >= xInjuryYOffsetCap) tIndex = xInjuryYOffsetCap-1;
        pY = pY + xInjuryYOffsets[tIndex];
    }

    ///--[Iterate]
    CCELayer *rLayer = (CCELayer *)mRenderingLayers->PushIterator();
    while(rLayer)
    {
        //--Check if the layer should skip rendering due to this being a UI call.
        if(pShowOnlyWorld && !rLayer->mRendersInWorld)
        {
            rLayer = (CCELayer *)mRenderingLayers->AutoIterate();
            continue;
        }

        //--Resolve the frame.
        int tUseSlot = ((int)(mAnimationTimer / rLayer->mTicksPerFrame)) % rLayer->mMaxFrames;
        if(tUseSlot < 0 || tUseSlot >= CCELAYER_MAX_FRAMES) tUseSlot = 0;
        StarBitmap *rImage = rLayer->rImages[tUseSlot];

        //--Offsets.
        float tXOffset = rLayer->mXOffset;
        float tYOffset = rLayer->mYOffset;

        //--Check if this layer is the switchable layer. If so, switch it based on injury/KO.
        if(mSwitchableLayerName && !strcasecmp(mSwitchableLayerName, mRenderingLayers->GetIteratorName()))
        {
            //--Knockout.
            if(mHP < 1 && rKnockoutImg)
            {
                rKnockoutImg->Draw(pX+tXOffset, pY+tYOffset);
            }
            //--Injury:
            else if(mInjuryTimer > 0 && rInjuredImg)
            {
                rInjuredImg->Draw(pX+tXOffset, pY+tYOffset);
            }
            //--Normal, or no override.
            else
            {
                if(rImage) rImage->Draw(pX+tXOffset, pY+tYOffset);
            }
        }
        //--If the layer name is "HPSP" then this is the layer that renders the character's current
        //  HP/SP values, in depth stride.
        else if(!strcasecmp(mRenderingLayers->GetIteratorName(), "HPSP"))
        {
            //--HP:
            RenderThreeDigitNumber((int)mHPPack.mXCur, pX+ 72, pY+266, rImage);
            RenderThreeDigitNumber(GetHealthMax(),     pX+117, pY+266, rImage);

            //--SP:
            RenderThreeDigitNumber((int)mSPPack.mXCur, pX+189, pY+266, rImage);

            //--Influence
            RenderThreeDigitNumber((int)mInfPack.mXCur, pX+79, pY+296, rImage);
        }
        //--Render normally.
        else
        {
            if(rImage) rImage->Draw(pX+tXOffset, pY+tYOffset);
        }

        //--Next.
        rLayer = (CCELayer *)mRenderingLayers->AutoIterate();
    }
}
void CarnationCombatEntity::RenderSwitchableAt(float pX, float pY)
{
    ///--[Documentation]
    //--Similar to general portrait rendering, but only renders the layer named "Switchable". This is used for
    //  the status UI.
    if(!mSwitchableLayerName) return;

    //--Locate the layer.
    CCELayer *rLayer = (CCELayer *)mRenderingLayers->GetElementByName(mSwitchableLayerName);
    if(!rLayer) return;

    //--Optional layers. These have specific names of "Underlay" and "Eye".
    CCELayer *rUnderlay = (CCELayer *)mRenderingLayers->GetElementByName("Underlay");
    if(rUnderlay)
    {
        StarBitmap *rImage = rUnderlay->rImages[0];
        if(rImage) rImage->Draw(pX, pY);
    }
    CCELayer *rEye = (CCELayer *)mRenderingLayers->GetElementByName("Eye");
    if(rEye)
    {
        StarBitmap *rImage = rEye->rImages[0];
        if(rImage) rImage->Draw(pX, pY);
    }

    //--Render.
    StarBitmap *rImage = rLayer->rImages[0];
    if(rImage) rImage->Draw(pX, pY);
}
void CarnationCombatEntity::RenderThreeDigitNumber(int pNumber, float pX, float pY, StarBitmap *pImage)
{
    ///--[Documentation]
    //--Given a three-digit number (or less, which will pad with zeroes), renders the number as a set of
    //  images taken from the master bitmap pImage. The master bitmap is assumed to be broken into 10 parts,
    //  numbering 0 to 9.
    if(!pImage) return;

    //--Setup.
    bool tHasRenderedNonzero = false;
    float cLetWid = (pImage->GetWidth() / 10) - 3;
    float cLetHei = pImage->GetHeight();
    pImage->Bind();

    //--Resolve the three digits.
    int tDigits[3];
    tDigits[0] = (pNumber / 100) % 10;
    tDigits[1] = (pNumber /  10) % 10;
    tDigits[2] = (pNumber /   1) % 10;

    //--Render.
    for(int i = 0; i < 3; i ++)
    {
        //--If the value is nonzero, or this is the last digit, flip this flag.
        if(tDigits[i] > 0 || i == 2) tHasRenderedNonzero = true;

        //--If we haven't rendered a non-zero value yet, and this is a zero, skip it.
        if(tHasRenderedNonzero == false && tDigits[i] == 0) continue;

        //--Texel Position.
        float cTxL = tDigits[i] * 0.10f + 0.005f;
        float cTxT = 0.0f;
        float cTxR = cTxL + 0.10f - 0.005f;
        float cTxB = 1.0f;

        //--Pixel Position.
        float cLft = pX + (i * cLetWid);
        float cTop = pY;
        float cRgt = cLft + cLetWid;
        float cBot = cTop + cLetHei;

        //--Render.
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);
        glEnd();
    }
}
void CarnationCombatEntity::RenderLayersAt(float pX, float pY)
{
    ///--[Documentation]
    //--Renders all the active layers for an enemy. If the enemy does not use complex rendering, just
    //  renders their basic portrait.
    if(!mEnemyUsesComplexRendering)
    {
        if(!rCombatImage) return;
        rCombatImage->Draw(pX, pY);
        return;
    }

    //--If complex rendering is enabled, the entity has multiple frames and may change them during battle.
    CCELayer *rLayer = (CCELayer *)mRenderingLayers->PushIterator();
    while(rLayer)
    {
        //--Layer is disabled. Don't render.
        if(rLayer->mIsDisabled)
        {
            rLayer = (CCELayer *)mRenderingLayers->AutoIterate();
            continue;
        }

        //--Resolve the frame.
        int tUseSlot = ((int)(mAnimationTimer / rLayer->mTicksPerFrame)) % rLayer->mMaxFrames;
        if(tUseSlot < 0 || tUseSlot >= CCELAYER_MAX_FRAMES) tUseSlot = 0;
        StarBitmap *rImage = rLayer->rImages[tUseSlot];

        //--Render.
        if(rImage) rImage->Draw(pX + rLayer->mXOffset, pY + rLayer->mYOffset);

        //--Next.
        rLayer = (CCELayer *)mRenderingLayers->AutoIterate();
    }
}
void CarnationCombatEntity::RenderAsEnemy(float pX, float pY, int pTargetTimer)
{
    ///--[Documentation]
    //--Enemies in Carnation render as large single-entities and don't animate much. They do flash when
    //  attacking, blink when taking damage, and can animate being afflicted by a status.
    //--The provided X position is the center, the provided Y position is the top of the entity portrait.
    if(!rCombatImage) return;

    //--If we ignore the offset, the render position becomes the exact center.
    if(mIgnoreRenderOffset)
    {
        pX = VIRTUAL_CANVAS_X * 0.50f;
    }

    //--Resolve render position.
    float cTrueRenderX = pX - (rCombatImage->GetTrueWidth() * 0.50f);

    ///--[Dying]
    //--Dying entities flash up to white, then vanish.
    if(mKnockoutTimer > 0)
    {
        //--Fading up to white. First, render the normal image and stencil.
        if(mKnockoutTimer < ADVCE_KNOCKOUT_TOWHITE_TICKS)
        {
            //--Normal image.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            RenderLayersAt(cTrueRenderX, pY);

            //--Render a white overlay.
            float cPercent = EasingFunction::QuadraticOut(mKnockoutTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glDisable(GL_TEXTURE_2D);
            glColor4f(1.0f, 1.0f, 1.0f, cPercent);
            RenderLayersAt(cTrueRenderX, pY);

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
            return;
        }
        //--Fading down to black.
        else
        {
            //--Get percent.
            int tUseTimer = mKnockoutTimer - ADVCE_KNOCKOUT_TOWHITE_TICKS;

            //--Portrait, color mask is off.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            RenderLayersAt(cTrueRenderX, pY);

            //--Fullwhite.
            glDisable(GL_TEXTURE_2D);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            RenderLayersAt(cTrueRenderX, pY);

            //--Render a black overlay.
            float cPercent = EasingFunction::QuadraticOut(tUseTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            glDisable(GL_TEXTURE_2D);
            glColor4f(0.0f, 0.0f, 0.0f, cPercent);
            RenderLayersAt(cTrueRenderX, pY);

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
            return;
        }
    }

    ///--[Injury]
    //--Injured enemies don't render on a modulus.
    if(mInjuryTimer % 2 >= 1) return;

    ///--[Attacking]
    //--An attacking entity blinks black twice.
    if(mAttackTimer % 4 >= 2)
    {
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        RenderLayersAt(cTrueRenderX, pY);
        StarlightColor::ClearMixer();
        return;
    }

    ///--[Normal]
    //--Render the portrait normally.
    if(pTargetTimer % 30 < 15)
    {
        RenderLayersAt(cTrueRenderX, pY);
    }
    //--Target. Flashes black.
    else
    {
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        RenderLayersAt(cTrueRenderX, pY);
        StarlightColor::ClearMixer();
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void CarnationCombatEntity::HookToLuaState(lua_State *pLuaState)
{
    /* CarnationCombatEntity_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "CarnationCombatEntity_GetProperty", &Hook_CarnationCombatEntity_GetProperty);

    /* CarnationCombatEntity_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "CarnationCombatEntity_SetProperty", &Hook_CarnationCombatEntity_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnationCombatEntity_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnationCombatEntity_GetProperty("Is Knockout Pending") (1 Boolean)
    //CarnationCombatEntity_GetProperty("Is Ignoring Profiency") (1 Boolean)
    //CarnationCombatEntity_GetProperty("Flat Accuracy") (1 Integer)
    //CarnationCombatEntity_GetProperty("Flat Crit Rate") (1 Integer)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnationCombatEntity_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_CARNATIONCOMBATENTITY)) return LuaTypeError("CarnationCombatEntity_GetProperty", L);
    CarnationCombatEntity *rEntity = (CarnationCombatEntity *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--If the character should be knocked out, but is waiting for the text prompt, this will be true.
    if(!strcasecmp(rSwitchType, "Is Knockout Pending") && tArgs == 1)
    {
        lua_pushboolean(L, rEntity->IsKnockoutPending());
        tReturns = 1;
    }
    //--If true, entity ignores profiency and uses flat accuracy/crit rates.
    else if(!strcasecmp(rSwitchType, "Is Ignoring Profiency") && tArgs == 1)
    {
        lua_pushboolean(L, rEntity->IgnoresProficiency());
        tReturns = 1;
    }
    //--Flat accuracy when ignoring proficiency. 0 = always miss, 100 = always hit.
    else if(!strcasecmp(rSwitchType, "Flat Accuracy") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetFlatAccuracy());
        tReturns = 1;
    }
    //--Flat crit rate when ignoring proficiency. 0 = never crit, 100 = always crit.
    else if(!strcasecmp(rSwitchType, "Flat Crit Rate") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetFlatCritRate());
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("CarnationCombatEntity_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_CarnationCombatEntity_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnationCombatEntity_SetProperty("Player Control", bFlag)
    //CarnationCombatEntity_SetProperty("Ignore Proficiency", bFlag)
    //CarnationCombatEntity_SetProperty("Flat Accuracy", iAccuracy)
    //CarnationCombatEntity_SetProperty("Flat Crit Rate", iCritRate)

    //--[Timers]
    //CarnationCombatEntity_SetProperty("Complete Timers")
    //CarnationCombatEntity_SetProperty("HP Scroll Hold", bFlag)

    //--[Rendering]
    //CarnationCombatEntity_SetProperty("Set Save Portrait Path", sPath)
    //CarnationCombatEntity_SetProperty("Set Complex Rendering", bFlag)
    //CarnationCombatEntity_SetProperty("Ignore Render Offset", bFlag)
    //CarnationCombatEntity_SetProperty("Set Render Refresh Script", sPath)
    //CarnationCombatEntity_SetProperty("Add Render Layer", sName, iPriority, fRPF, iFrames)
    //CarnationCombatEntity_SetProperty("Rem Render Layer", sName)
    //CarnationCombatEntity_SetProperty("Clear Render Layers")
    //CarnationCombatEntity_SetProperty("Set Render Layer Frame", sName, iSlot, sDLPath)
    //CarnationCombatEntity_SetProperty("Set Render Layer World", sName, bFlag)
    //CarnationCombatEntity_SetProperty("Set Render Layer Disabled", sName, bIsDisabled)
    //CarnationCombatEntity_SetProperty("Set Render Layer Offsets", sName, fX, fY)
    //CarnationCombatEntity_SetProperty("Sort Render Layers")
    //CarnationCombatEntity_SetProperty("Set Switchable Layer Name", sName)
    //CarnationCombatEntity_SetProperty("Set Injured Image", sDLPath)
    //CarnationCombatEntity_SetProperty("Set Knockout Image", sDLPath)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnationCombatEntity_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_CARNATIONCOMBATENTITY)) return LuaTypeError("CarnationCombatEntity_SetProperty", L);
    CarnationCombatEntity *rEntity = (CarnationCombatEntity *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--If true, the player must input a command for this character. Otherwise they get skipped.
    if(!strcasecmp(rSwitchType, "Player Control") && tArgs == 2)
    {
        rEntity->SetPlayerControlThisTurn(lua_toboolean(L, 2));
    }
    //--If true, bypasses proficiency checks. Used for enemies who never have proficiencies.
    else if(!strcasecmp(rSwitchType, "Ignore Proficiency") && tArgs == 2)
    {
        rEntity->SetIgnoresProficiency(lua_toboolean(L, 2));
    }
    //--If ignoring accuracy, sets the hit rate.
    else if(!strcasecmp(rSwitchType, "Flat Accuracy") && tArgs == 2)
    {
        rEntity->SetFlatAccuracy(lua_tointeger(L, 2));
    }
    //--If ignoring profiencies, sets the critical hit rate.
    else if(!strcasecmp(rSwitchType, "Flat Crit Rate") && tArgs == 2)
    {
        rEntity->SetFlatCritRate(lua_tointeger(L, 2));
    }
    ///--[Timers]
    //--Causes the HP/SP timers to immediately complete the next time the UpdateTimers() routine is run.
    else if(!strcasecmp(rSwitchType, "Complete Timers") && tArgs == 1)
    {
        rEntity->CompleteTimers();
    }
    //--Prevents HP from scrolling. Implicitly unset by the [INJURE] tag.
    else if(!strcasecmp(rSwitchType, "HP Scroll Hold") && tArgs == 2)
    {
        rEntity->SetHPScrollHold(lua_toboolean(L, 2));
    }
    ///--[Rendering]
    //--Portrait path used during loading to render characters.
    else if(!strcasecmp(rSwitchType, "Set Save Portrait Path") && tArgs == 2)
    {
        rEntity->SetSavePortraitPath(lua_tostring(L, 2));
    }
    //--Allows entities to animate and have layered rendering.
    else if(!strcasecmp(rSwitchType, "Set Complex Rendering") && tArgs == 2)
    {
        rEntity->SetComplexRendering(lua_toboolean(L, 2));
    }
    //--Ignore X position and render at a fixed location. Used for bosses.
    else if(!strcasecmp(rSwitchType, "Ignore Render Offset") && tArgs == 2)
    {
        rEntity->SetIgnoreXPosition(lua_toboolean(L, 2));
    }
    //--Sets the script that gets called when a [RENDERREFRESH] tag is tripped.
    else if(!strcasecmp(rSwitchType, "Set Render Refresh Script") && tArgs == 2)
    {
        rEntity->SetRenderRefreshScript(lua_tostring(L, 2));
    }
    //--Adds a rendering layer to the entity.
    else if(!strcasecmp(rSwitchType, "Add Render Layer") && tArgs == 5)
    {
        rEntity->AddRenderLayer(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tonumber(L, 4), lua_tointeger(L, 5));
    }
    //--Removes a rendering layer.
    else if(!strcasecmp(rSwitchType, "Rem Render Layer") && tArgs == 2)
    {
        rEntity->RemoveRenderLayer(lua_tostring(L, 2));
    }
    //--Removes all rendering layers.
    else if(!strcasecmp(rSwitchType, "Clear Render Layers") && tArgs == 1)
    {
        rEntity->ClearRenderLayers();
    }
    //--Specifies a frame in the rendering layer's animation.
    else if(!strcasecmp(rSwitchType, "Set Render Layer Frame") && tArgs == 4)
    {
        rEntity->SetRenderLayerFrame(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--If true, this layer renders in the world UI, by default it doesn't.
    else if(!strcasecmp(rSwitchType, "Set Render Layer World") && tArgs == 3)
    {
        rEntity->SetRenderLayerWorld(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Layer can be toggled on or off.
    else if(!strcasecmp(rSwitchType, "Set Render Layer Disabled") && tArgs == 3)
    {
        rEntity->SetRenderLayerDisabled(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--X/Y Offset of the layer.
    else if(!strcasecmp(rSwitchType, "Set Render Layer Offsets") && tArgs == 4)
    {
        rEntity->SetRenderLayerOffsets(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Sorts rendering layers by priority.
    else if(!strcasecmp(rSwitchType, "Sort Render Layers") && tArgs == 1)
    {
        rEntity->SortRenderLayers();
    }
    //--Sets the name of the layer that changes its image based on internal timers, such as getting injured or KO'd.
    else if(!strcasecmp(rSwitchType, "Set Switchable Layer Name") && tArgs == 2)
    {
        rEntity->SetSwitchableLayerName(lua_tostring(L, 2));
    }
    //--Sets the injury frame to use if the timer is set.
    else if(!strcasecmp(rSwitchType, "Set Injured Image") && tArgs == 2)
    {
        rEntity->SetInjuredImage(lua_tostring(L, 2));
    }
    //--Sets the knockout frame to use if the timer is set.
    else if(!strcasecmp(rSwitchType, "Set Knockout Image") && tArgs == 2)
    {
        rEntity->SetKnockoutImage(lua_tostring(L, 2));
    }
    //--Error case.
    else
    {
        LuaPropertyError("CarnationCombatEntity_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}

