///======================================== AdvUIEquip ============================================
//--Equipment UI. Lets the player check items, equip them, and modify gems.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Icons
#define ADVUIEQUIP_ICONS_TOTAL 18
#define ADVUIEQUIP_RESISTANCE_BEGIN 6

///========================================== Classes =============================================
class AdvUIEquip : virtual public AdvUICommon
{
    protected:
    ///--[System]
    //--Timing.
    static const int cDetailTicks = 7;                  //Number of ticks to show/hide details mode.
    static const int cReplaceTicks = 7;                 //Ticks to bring up the replace overlay.

    //--Sizes and Offsets
    static const int cItemNameMaxLen = 268;             //Maximum width, in pixels, of a an equipment item name. Longer names get squished.
    int cReplaceEntriesPerPage;                         //How many replacement entries fit on a single page. Based on font/frame sizes.
    static const int cReplaceEdgeBuf = 3;               //How many entries from the edge of the scrolling list before scrolling.

    //--Stencil Interoperability
    static const int cStencil = 20;                     //Stencil value, must be unique among all StarUIPieces so they don't cause render bugs.

    //--Help.
    static const int cHelpStringsTotal = 24;            //Number of unique StarlightStrings appearing on the help menu.

    ///--[Selection]
    //--Cursor Integers.
    int mCharacterCursor;
    int mEquipmentCursor;
    int mGemCursor;

    //--Cursor Positions
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Other timers.
    int mArrowTimer;

    ///--[Character Swap]
    bool mIsSwappingCharacter;
    int mCharacterSwapTimer;
    int mPrevCharacterSlot;

    ///--[Replacement]
    bool mIsReplacing;
    int mReplacementTimer;
    int mReplacementFlashTimer;
    int mReplacementSkip;
    int mReplacementCursor;
    StarLinkedList *mValidReplacementsList; //AdventureItem *, reference

    ///--[Details]
    bool mIsShowingDetails;
    int mDetailsTimer;
    AdventureItem *rDetailsItem;

    ///--[Statistic Lookups]
    int mStatisticIndexes[ADVUIEQUIP_ICONS_TOTAL];
    char mBonusTagNames[ADVUIEQUIP_ICONS_TOTAL][64];
    char mMalusTagNames[ADVUIEQUIP_ICONS_TOTAL][64];

    ///--[Rendering Constants]
    float cSlotNameX;
    float cSlotNameY;
    float cSlotH;
    float cEquipIconX;
    float cEquipIconY;
    float cEquipNameX;
    float cEquipNameY;
    float cGemIconX;
    float cGemIconY;
    float cGemIconW;
    float cReplaceStartY;
    float cReplaceStartH;
    float cReplaceDividerH;

    ///--[Help Strings]
    StarlightString *mShowHelpString;
    StarlightString *mInspectString;
    StarlightString *mScrollFastString;
    StarlightString *mCharacterLftString;
    StarlightString *mCharacterRgtString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Statistics;

        //--Images
        StarBitmap *rFrame_Description;
        StarBitmap *rFrame_Rgt;
        StarBitmap *rFrame_Top;
        StarBitmap *rHighlight_Standard;
        StarBitmap *rOverlay_ArrowLft;
        StarBitmap *rOverlay_ArrowRgt;
        StarBitmap *rOverlay_EquipDetail;
        StarBitmap *rOverlay_GemSlot;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_ReplaceDivider;
        StarBitmap *rScrollbar_Static;
        StarBitmap *rScrollbar_Scroller;

        //--Common Parts
        StarBitmap *rPropertyIcons[ADVUIEQUIP_ICONS_TOTAL];
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIEquip();
    virtual ~AdvUIEquip();
    virtual void Construct();

    //--Public Variables
    virtual bool IsOfType(int pType);

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    ///--[Selection]
    void ScrollCharacter(int pAmount);
    void ReresolveDetailsItem();
    virtual void UpdateSelection();

    ///--[Replacement]
    virtual bool ActivateReplacement();
    void ReresolveReplaceDetailsItem();
    virtual void UpdateReplacement();

    protected:
    //--Private Core Methods
    void BuildSlotList(int pCharacterSlot, const char *pSlotType);

    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    virtual void RenderEquipDetails(float pColorAlpha);
    virtual void RenderEquipEntry(int pSlot, float pColorAlpha);
    virtual void RenderEquipReplacements();
    virtual void RenderEquipProperties(float pColorAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


