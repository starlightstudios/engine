///--[SDL Version]
//--The GameGear makes extensive use of the event procedures, so it's split up into versions for
//  each of the handlers available for events. This is the SDL version.
#if defined _SDL_PROJECT_

//--Base
#include "GameGear.h"

//--Classes
#include "PlayerPony.h"
#include "VisualLevel.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLoadInterrupt.h"
#include "StarBitmap.h"
#include "StarCamera2D.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DebugDefinitions.h"
#include "Global.h"
#include "Program.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "MapManager.h"
#include "NetworkManager.h"
#include "OptionsManager.h"

//--Function forward declarations.
void RenderDebugStrings();
void RenderFPS();
void RenderJoypadDepression();

///--[Event Handler]
void GameGear::EventHandler(DriveShaft &sDriveShaft)
{
    //--Event handler, any event other than FORM_TIMER_EXPIRED should be handled here.
    GLOBAL *rGlobal = Global::Shared();
    SDL_Event tEvent;
    memcpy(&tEvent, &rGlobal->gEvent, sizeof(SDL_Event));
    DebugManager::PushPrint(false, "[Event Handler] Begin %i\n", tEvent.type);

    //--Logic tick is passed to the function pointed at.
    if(tEvent.type == SDL_USEREVENT && tEvent.user.code == 0)
    {
        if(sDriveShaft.GameTime >= 1.0f) Global::Shared()->gTicksElapsed ++;
        rGlobal->rCurrentLogicHandler(sDriveShaft);
    }
    //--User clicks the big fat X button on their window.
    else if(tEvent.type == SDL_QUIT)
    {
        rGlobal->gQuit = true;
    }
    //--Key pressed down.
    else if(tEvent.type == SDL_KEYDOWN)
    {
        if(!tEvent.key.repeat)
            ControlManager::Fetch()->InformOfPress(tEvent.key.keysym.scancode, -2, -2);
    }
    //--Key released.
    else if(tEvent.type == SDL_KEYUP)
    {
        ControlManager::Fetch()->InformOfRelease(tEvent.key.keysym.scancode, -2, -2);
    }
    //--Mouse moved.
    else if(tEvent.type == SDL_MOUSEMOTION)
    {
        rGlobal->gControlManager->SetMouseCoordsByDisplay(tEvent.motion.x, tEvent.motion.y, -700);
    }
    //--Mouse button.
    else if(tEvent.type == SDL_MOUSEBUTTONDOWN)
    {
        rGlobal->gControlManager->InformOfPress(-2, tEvent.button.button, -2);
    }
    //--Mouse release.
    else if(tEvent.type == SDL_MOUSEBUTTONUP)
    {
        rGlobal->gControlManager->InformOfRelease(-2, tEvent.button.button, -2);
    }
    //--Mouse wheel.
    else if(tEvent.type == SDL_MOUSEWHEEL)
    {
        rGlobal->gControlManager->SetMouseZRelative(tEvent.wheel.y);
    }
    //--Joypad down.
    else if(tEvent.type == SDL_JOYBUTTONDOWN)
    {
        rGlobal->gControlManager->InformOfPress(-2, -2, tEvent.jbutton.button);
    }
    //--Joypad up.
    else if(tEvent.type == SDL_JOYBUTTONUP)
    {
        rGlobal->gControlManager->InformOfRelease(-2, -2, tEvent.jbutton.button);
    }
    //--Joypad axis.
    else if(tEvent.type == SDL_JOYAXISMOTION)
    {
        rGlobal->gControlManager->InformOfJoyAxis(tEvent.jaxis.axis, tEvent.jaxis.value);
    }

    //--Debug
    DebugManager::PopPrint("[Event Handler] Complete\n");
}

///--[Logic Handler]
void GameGear::LogicHandler(DriveShaft &sDriveShaft)
{
    ///--[Setup]
    //--FORM_TIMER_EXPIRED occupies such a large amount of game logic that it gets its own function.
    DebugManager::PushPrint(false, "[Timer] Begin\n");

    //--Store when this logic tick started. Logic ticks have 1/60 seconds to operate.
    GLOBAL *rGlobal = Global::Shared();
    rGlobal->gTickStartTime = GetGameTime();

    //--Store the "End time" for the tick. This is not actually the end time, it's a warning to subroutines that they need to pack it
    //  in and stop doing stuff because they're hitting the outer edge of what they should do.
    //--For example, enemy pathing will add random offsets to make the AI stop running pathing if it gets too close to the end of the
    //  tick when running pathing.
    rGlobal->gTickWarnTime = rGlobal->gTickStartTime + (1.0f / rGlobal->gExpectedFPS * 0.85);

    //--Zero this global.
    TilemapActor::xActorsPathingThisTick = 0;

    ///--[Network]
    //--Network logic goes first.
    NetworkManager::Fetch()->Update();

    ///--[Control Input]
    //--Controls are only actually updated here, instead of during keypress logics.  If no keys
    //  were pressed since the last logic tick, nothing happens.
    DebugManager::Print("Controls\n");
    ControlManager::Fetch()->Update(true, false);
    ControlManager::Fetch()->CheckHotkeys();

    ///--[Load Interrupt Keypress]
    //--If the load interrupt is waiting for keypresses, that handles here.
    StarLoadInterrupt *rStarLoadInterrupt = StarLoadInterrupt::Fetch();
    if(rStarLoadInterrupt && rStarLoadInterrupt->IsAwaitingKeypress() && StarBitmap::xInterruptCall)
    {
        StarBitmap::xInterruptCall();
        if(ControlManager::Fetch()->IsAnyKeyPressed()) rStarLoadInterrupt->SetKeypressFlag(false);
        DebugManager::PopPrint("[Timer] Complete by Interrupt\n");
        return;
    }

    ///--[Maps]
    DebugManager::Print("Map Manager\n");
    MapManager::Fetch()->Update();

    ///--[Entities]
    DebugManager::Print("Entity Manager\n");
    EntityManager::Fetch()->Update();

    ///--[Camera]
    DebugManager::Print("Camera Manager\n");
    CameraManager::Fetch()->Update();

    //--Flag.
    sDriveShaft.ScreenNeedsUpdate = true;

    ///--[Finish Up]
    //--Post-tick updates.
    DebugManager::Print("Running post-tick logic.\n");
    PostLogicHandler();

    //--Debug.
    DebugManager::PopPrint("[Timer] Complete\n");
}

///--[Rendering Handler]
void GameGear::RenderHandler(DriveShaft &sDriveShaft)
{
    //--Rendering handler, exactly what it says on the tin.
    GLOBAL *rGlobal = Global::Shared();

    //--If the load interrupt is waiting for keypresses, no rendering occurs.
    StarLoadInterrupt *rStarLoadInterrupt = StarLoadInterrupt::Fetch();
    if(rStarLoadInterrupt && rStarLoadInterrupt->IsAwaitingKeypress())
    {
        return;
    }

    //--Error check
    DebugManager::PushPrint(false, "[GameGear Render Begin]\n");

    //--Only renders if the event queue has no pending events, and at least one logic tick passed.
    if(!sDriveShaft.ScreenNeedsUpdate || SDL_HasEvents(SDL_FIRSTEVENT, SDL_LASTEVENT))
    {
        DebugManager::PopPrint("[GameGear Render Unnecessary]\n");
        return;
    }
    DebugManager::Print("Clearing\n");
    sDriveShaft.ScreenNeedsUpdate = false;

    //--Is there a VisualLevel? If so, the DisplayManager does not use hard letterboxes, since widescreen
    //  will render more 3D space.
    DisplayManager::xUseHardLetterbox = (VisualLevel::Fetch() == NULL);

    //--Setup GL
    DisplayManager::LetterboxOffsets();
    DisplayManager::StdOrtho();

    //--Draw to the front buffer directly. Bypass Allegro's backbuffer options.
    glDrawBuffer(GL_BACK);

    //--Clear the screen off
    glClearDepth(1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearStencil(0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    //--Variable reset.
    TilemapActor::xViewconeCountLastTick = TilemapActor::xViewconeCount;
    TilemapActor::xViewconeCount = 0;

    //--[Immediate Rendering]
    if(!rGlobal->gDisplayManager->IsUsingSortedRendering())
    {
        //--Screen shaking.
        float tShakeX, tShakeY;
        MapManager::Fetch()->GetShakeFactors(tShakeX, tShakeY);
        glTranslatef(tShakeX, tShakeY, 0.0f);

        //--Render the Entities.
        DebugManager::Print("Rendering Entities\n");
        EntityManager::Fetch()->Render();
        DebugManager::Print("Rendered Entities\n");

        //--Render the foreground.
        MapManager::Fetch()->Render();

        //--Debug render calls.
        RenderDebugStrings();
        RenderFPS();
        RenderJoypadDepression();
    }
    //--[Delayed Rendering]
    //--Rendering mode which uses a sorted depth list to render all objects in the depth order,
    //  which is back-to-front to allow proper alpha layering. This is only really useful in a
    //  2D application, since 3D graphics don't necessarily have an easily sorted depth list.
    else
    {
        //--Screen shaking.
        float tShakeX, tShakeY;
        MapManager::Fetch()->GetShakeFactors(tShakeX, tShakeY);
        glTranslatef(tShakeX, tShakeY, 0.0f);

        //--Reset the rendering list.
        StarLinkedList *rRenderingList = DisplayManager::FetchRenderingList();
        rRenderingList->ClearList();

        //--Render the Background.
        MapManager::Fetch()->AddToRenderList(rRenderingList);
        DebugManager::Print("Queued Map\n");

        //--Render the Entities.
        EntityManager::Fetch()->AddToRenderingList(rRenderingList);
        DebugManager::Print("Queued Entities\n");

        //--Sort the rendering list.
        rRenderingList->SortListUsing(&IRenderable::CompareRenderable);
        DebugManager::Print("Sorted Entities\n");

        //--Render all the objects on the rendering list.
        IRenderable *rRenderObject = (IRenderable *)rRenderingList->PushIterator();
        while(rRenderObject)
        {
            DebugManager::Print("Rendering %p", rRenderObject);
            rRenderObject->Render();
            rRenderObject = (IRenderable *)rRenderingList->AutoIterate();
        }
        DebugManager::Print("Rendered all objects.\n");

        //--Debug render calls. These are technically part of the GUI.
        RenderDebugStrings();
        RenderFPS();
        RenderJoypadDepression();
    }

    ///--[Diagnostics]
    //--These render nothing if their flags aren't set.
    CutsceneManager::Fetch()->RenderDiagnostics();

    //--Send the rendered data to the GL context.
    SDL_GL_SwapWindow(DisplayManager::Fetch()->GetWindow());

    //--Clean
    DisplayManager::CleanOrtho();

    //--Toggle this flag.
    DisplayManager::xHasHadRenderPassSinceFullscreen = true;

    //--<DEBUG>
    LogFPS(sDriveShaft);
    DebugManager::PopPrint("[GameGear Render Complete]\n");
}
void GameGear::ShiftTo()
{
    //--Shifts the program into GameGear
    GLOBAL *rGlobal = Global::Shared();
    rGlobal->rCurrentEventHandler  = &GameGear::EventHandler;
    rGlobal->rCurrentLogicHandler  = &GameGear::LogicHandler;
    rGlobal->rCurrentRenderHandler = &GameGear::RenderHandler;
}

///--[Worker Function]
void RenderDebugStrings()
{
    //--Error in the fonts.
    GLOBAL *rGlobal = Global::Shared();
    if(!rGlobal->gBitmapFont) return;

    //--Setup
    const float cScale = 0.25f;

    //--For each debug line, independent of what's on the line, render...
    for(int i = 0; i < DEBUG_LINES_TOTAL; i ++)
    {
        //--Make sure there's something to render.
        if(!rGlobal->gDebugPrintStrings[i]) continue;

        //--Position/Scale
        glTranslatef(0.0f, 40.0f * cScale * (i+1), 0.0f);
        glScalef(cScale, -cScale, 1.0f);

        //--Render
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, rGlobal->gDebugPrintStrings[i]);

        //--Clean
        glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
        glTranslatef(0.0f * -1.0f, -40.0f * cScale * (i+1), 0.0f);
    }
}
void RenderFPS()
{
    //--Render the current FPS in the top right, averaged over a few seconds by the logger. This can
    //  be disabled with a define change.
    #ifndef DBG_SHOW_FPS
        return;
    #endif
    return;

    //--Error in the fonts.
    GLOBAL *rGlobal = Global::Shared();
    if(!rGlobal->gBitmapFont) return;

    //--Setup
    float tRenderX = VIRTUAL_CANVAS_X - 40.0f;
    const float cScale = 0.25f;
    char tBufferFPS[32];
    sprintf(tBufferFPS, "%04.1f", rGlobal->gActualFPS);

    //--Position/Scale
    glTranslatef(tRenderX, 40.0f * cScale, 0.0f);
    glScalef(cScale, -cScale, 1.0f);

    //--Render
    rGlobal->gBitmapFont->DrawText(0.0f,   0.0f, SUGARFONT_AUTOCENTER_X, tBufferFPS);

    //--Clean
    glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
    glTranslatef(tRenderX * -1.0f, -40.0f * cScale, 0.0f);
}
void RenderJoypadDepression()
{
    //--Does nothing in SDL.
}
#endif
