-- |[ ================================ StatGenerator Class File ================================ ]|
--Class file for StatGenerator. Call this once at initialization.

--Namespacing class designed to generate stats using various formulae. Each of the different chapters
-- and various Starlight games has their own formula since the balance is intended to change.
--This is a singleton class and not meant to be instantiated.

-- |[ ============= Enumerations ============= ]|
--These are normally set in 000 Variables.lua but are mirrored here since that file has likely
-- not run yet when using the object builder.
gciStatIndex_Platina = -2
gciStatIndex_Experience = -1
gciStatIndex_HPMax = 0
gciStatIndex_MPMax = 1
gciStatIndex_MPRegen = 2
gciStatIndex_CPMax = 3
gciStatIndex_FreeActionMax = 4
gciStatIndex_FreeActionGen = 5
gciStatIndex_Attack = 6
gciStatIndex_Initiative = 7
gciStatIndex_Accuracy = 8
gciStatIndex_Evade = 9
gciStatIndex_Protection = 10
gciStatIndex_Resist_Start = 11
gciStatIndex_Resist_Slash = 11
gciStatIndex_Resist_Strike = 12
gciStatIndex_Resist_Pierce = 13
gciStatIndex_Resist_Flame = 14
gciStatIndex_Resist_Freeze = 15
gciStatIndex_Resist_Shock = 16
gciStatIndex_Resist_Crusade = 17
gciStatIndex_Resist_Obscure = 18
gciStatIndex_Resist_Bleed = 19
gciStatIndex_Resist_Poison = 20
gciStatIndex_Resist_Corrode = 21
gciStatIndex_Resist_Terrify = 22
gciStatIndex_Resist_End = 22
gciStatIndex_StunCap = 23
gciStatIndex_ThreatMultiplier = 24

-- |[ =========== Mode Structures ============ ]|
--Local structure that stores an execution mode.
--zObject.sName                = "Chapter 1"                --String representing the mode's name.
--zObject.fnGenerateHealth     = nil                        --Function pointer, called when resolving HP values.
--zObject.fnGenerateAttack     = nil                        --Function pointer, called when resolving Atk values.
--zObject.fnGenerateInitiative = nil                        --Function pointer, called when resolving Ini values.
--zObject.fnGenerateAccuracy   = nil                        --Function pointer, called when resolving Acc values.
--zObject.fnGenerateEvade      = nil                        --Function pointer, called when resolving Evd values.
--zObject.fnGenerateStun       = nil                        --Function pointer, called when resolving Stun values.
--zObject.fnGeneratePlatina    = nil                        --Function pointer, called when resolving Platina values.
--zObject.fnGenerateExperience = nil                        --Function pointer, called when resolving Experience values.

-- |[ ============ Class Members ============= ]|
-- |[System]|
StatGenerator = {}
StatGenerator.__index = StatGenerator
StatGenerator.zaValidModes = {}
StatGenerator.zrActiveMode = nil

-- |[ ============ Class Methods ============= ]|
--System
--Common
function StatGenerator:fnRegisterMode(pzMode)                        end
function StatGenerator:fnSetMode(psMode)                             end
function StatGenerator:fnCommonRangeCheck(piLevel, piGradeIndex)     end
function StatGenerator:fnGenerateStat(piStatIndex, piLevel, piGrade) end

--Chapter 1
function StatGenerator:fnChapter1HP  (piLevel, piGradeIndex) end
function StatGenerator:fnChapter1Atk (piLevel, piGradeIndex) end
function StatGenerator:fnChapter1Ini (piLevel, piGradeIndex) end
function StatGenerator:fnChapter1Acc (piLevel, piGradeIndex) end
function StatGenerator:fnChapter1Evd (piLevel, piGradeIndex) end
function StatGenerator:fnChapter1Stun(piLevel, piGradeIndex) end
function StatGenerator:fnChapter1Plt (piLevel, piGradeIndex) end
function StatGenerator:fnChapter1Exp (piLevel, piGradeIndex) end
 
-- |[ ========== Class Constructor =========== ]|
--Namepsacing class, not meant to be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "StatGeneratorCommon.lua")
LM_ExecuteScript(fnResolvePath() .. "StatGeneratorChapter1.lua")

-- |[ ========== Call Mode Creation ========== ]|
--Create modes.
local zChapter1 = {}
zChapter1.sName                = "Chapter 1"
zChapter1.fnGenerateHealth     = StatGenerator.fnChapter1HP
zChapter1.fnGenerateAttack     = StatGenerator.fnChapter1Atk
zChapter1.fnGenerateInitiative = StatGenerator.fnChapter1Ini
zChapter1.fnGenerateAccuracy   = StatGenerator.fnChapter1Acc
zChapter1.fnGenerateEvade      = StatGenerator.fnChapter1Evd
zChapter1.fnGenerateStun       = StatGenerator.fnChapter1Stun
zChapter1.fnGeneratePlatina    = StatGenerator.fnChapter1Plt
zChapter1.fnGenerateExperience = StatGenerator.fnChapter1Exp
table.insert(StatGenerator.zaValidModes, zChapter1)
