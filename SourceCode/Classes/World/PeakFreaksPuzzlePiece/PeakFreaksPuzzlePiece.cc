//--Base
#include "PeakFreaksPuzzlePiece.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
PeakFreaksPuzzlePiece::PeakFreaksPuzzlePiece()
{
    ///--[Variable Initialization]
    //--System
    mPuzzleType = PEAKFREAK_PUZZLE_TYPE_NONE;
    mPower = 0;
    mOffsetTick = 0;

    //--Position
    mPosition.Initialize();
}
PeakFreaksPuzzlePiece::~PeakFreaksPuzzlePiece()
{

}

///===================================== Property Queries =========================================
int PeakFreaksPuzzlePiece::GetPuzzleType()
{
    return mPuzzleType;
}
int PeakFreaksPuzzlePiece::GetPower()
{
    return mPower;
}
EasingPack2D PeakFreaksPuzzlePiece::GetPosition()
{
    return mPosition;
}
int PeakFreaksPuzzlePiece::GetOffsetTick()
{
    return mOffsetTick;
}

///======================================= Manipulators ===========================================
void PeakFreaksPuzzlePiece::SetPuzzleType(int pType)
{
    mPuzzleType = pType;
}
void PeakFreaksPuzzlePiece::SetPower(int pPower)
{
    mPower = pPower;
}
void PeakFreaksPuzzlePiece::SetPosition(float pX, float pY)
{
    mPosition.MoveTo(pX, pY, 0);
}
void PeakFreaksPuzzlePiece::MoveTo(float pX, float pY, int pTicks)
{
    mPosition.MoveTo(pX, pY, pTicks);
}
void PeakFreaksPuzzlePiece::SetOffsetTick(int pTick)
{
    mOffsetTick = pTick;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksPuzzlePiece::Update()
{
    mPosition.Increment(EASING_CODE_QUADOUT);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
