//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"
#include "MapManager.h"

///========================================= Lua Hooking ==========================================
void AdvUIJournal::HookToLuaState(lua_State *pLuaState)
{
    /* AM_GetPropertyJournal("Show All Entries") (1 Boolean) (Static) */
    lua_register(pLuaState, "AM_GetPropertyJournal", &Hook_AM_GetPropertyJournal);

    /* AM_SetPropertyJournal("Party Resolve Script", sPath) (Static)
       Sets the requested property in the AdventureMenu. */
    lua_register(pLuaState, "AM_SetPropertyJournal", &Hook_AM_SetPropertyJournal);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AM_GetPropertyJournal(lua_State *L)
{
    ///--[Argument List]
    //--[Static]
    //AM_GetPropertyJournal("Show All Entries") (1 Boolean) (Static)
    //AM_GetPropertyJournal("Can Set Images") (1 Boolean) (Static)

    //--[Dynamic]
    //AM_GetPropertyJournal("Achievement Exists", sInternalName) (1 Boolean)
    //AM_GetPropertyJournal("Achievement Is Unlocked", sInternalName) (1 Boolean)
    //AM_GetPropertyJournal("Achievement Progress Slots", sInternalName) (1 Integer)
    //AM_GetPropertyJournal("Achievement Progress Current", sInternalName, iSlot) (1 Float)
    //AM_GetPropertyJournal("Achievement Progress Max", sInternalName, iSlot) (1 Float)

    ///--[Arguments]
    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_GetPropertyJournal");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Returns a debug flag.
    if(!strcasecmp(rSwitchType, "Show All Entries"))
    {
        lua_pushboolean(L, AdvUIJournal::xShowAllEntries);
        return 1;
    }
    //--Returns true if the objects are set up to call AM_SetPropertyJournal() functions.
    else if(!strcasecmp(rSwitchType, "Can Set Images"))
    {
        AdventureMenu *rAdvMenu = AdventureMenu::Fetch();
        lua_pushboolean(L, (rAdvMenu != NULL));
        return 1;
    }

    ///--[Dynamic]
    //--Get the base menu.
    AdventureMenu *rAdvMenu = AdventureMenu::Fetch();
    if(!rAdvMenu)
    {
        //--If no menu is found, but a level is found, then the level type is not compatible. Ignore it.
        if(MapManager::Fetch()->GetActiveLevel() != NULL)
        {
            return 0;
        }

        //--Otherwise, issue a debug call.
        return LuaTypeError("AM_GetPropertyJournal", L);
    }

    //--Get the sub-object.
    AdvUIJournal *rJournalUI = rAdvMenu->GetJournalUI();
    if(!rJournalUI)
    {
        return LuaTypeError("AM_GetPropertyJournal", L);
    }

    ///--[System]
    //--Returns true if the achievement named exists, false otherwise.
    if(!strcasecmp(rSwitchType, "Achievement Exists") && tArgs == 2)
    {
        AdvMenuJournalAchievementEntry *rAchievement = rJournalUI->LocateAchievement(lua_tostring(L, 2));
        lua_pushboolean(L, (rAchievement != NULL));
        tReturns = 1;
    }
    //--Returns true if the achievement is unlocked.
    else if(!strcasecmp(rSwitchType, "Achievement Is Unlocked") && tArgs == 2)
    {
        AdvMenuJournalAchievementEntry *rAchievement = rJournalUI->LocateAchievement(lua_tostring(L, 2));
        if(rAchievement)
        {
            lua_pushboolean(L, rAchievement->mIsUnlocked);
        }
        else
        {
            lua_pushboolean(L, false);
        }
        tReturns = 1;
    }
    //--Returns how many progress slots an achievement has, or 0 on error. Can legally return 0.
    else if(!strcasecmp(rSwitchType, "Achievement Progress Slots") && tArgs == 2)
    {
        //--Locate.
        AdvMenuJournalAchievementEntry *rAchievement = rJournalUI->LocateAchievement(lua_tostring(L, 2));
        if(!rAchievement)
        {
            lua_pushinteger(L, 0);
        }
        else
        {
            lua_pushinteger(L, rAchievement->mProgressEntriesTotal);
        }

        //--Always 1 return.
        tReturns = 1;
    }
    //--Returns the current achievement progress for a given slot.
    else if(!strcasecmp(rSwitchType, "Achievement Progress Current") && tArgs == 3)
    {
        //--Locate.
        AdvMenuJournalAchievementEntry *rAchievement = rJournalUI->LocateAchievement(lua_tostring(L, 2));
        if(!rAchievement)
        {
            lua_pushnumber(L, 0.0f);
        }
        else
        {
            //--Range check:
            int tSlot = lua_tointeger(L, 3);
            if(tSlot < 0 || tSlot >= rAchievement->mProgressEntriesTotal)
            {
                lua_pushnumber(L, 0.0f);
            }
            else
            {
                lua_pushnumber(L, rAchievement->mProgressEntries[tSlot].mValueCur);
            }
        }

        //--Always 1 return.
        tReturns = 1;
    }
    //--Returns the maximum requirement for a given achievement progress slot.
    else if(!strcasecmp(rSwitchType, "Achievement Progress Maximum") && tArgs == 3)
    {
        //--Locate.
        AdvMenuJournalAchievementEntry *rAchievement = rJournalUI->LocateAchievement(lua_tostring(L, 2));
        if(!rAchievement)
        {
            lua_pushnumber(L, 0.0f);
        }
        else
        {
            //--Range check:
            int tSlot = lua_tointeger(L, 3);
            if(tSlot < 0 || tSlot >= rAchievement->mProgressEntriesTotal)
            {
                lua_pushnumber(L, 0.0f);
            }
            else
            {
                lua_pushnumber(L, rAchievement->mProgressEntries[tSlot].mValueMax);
            }
        }

        //--Always 1 return.
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AM_GetPropertyJournal", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AM_SetPropertyJournal(lua_State *L)
{
    ///--[Argument List]
    //--[Static]
    //AM_SetPropertyJournal("Bestiary Resolve Script", sPath) (Static)
    //AM_SetPropertyJournal("Profile Resolve Script", sPath) (Static)
    //AM_SetPropertyJournal("Quest Resolve Script", sPath) (Static)
    //AM_SetPropertyJournal("Location Resolve Script", sPath) (Static)
    //AM_SetPropertyJournal("Paragon Resolve Script", sPath) (Static)
    //AM_SetPropertyJournal("Combat Resolve Script", sPath) (Static)

    //--[Bestiary]
    //AM_SetPropertyJournal("Add Bestiary Entry", sInternalName, sDisplayName)
    //AM_SetPropertyJournal("Set Bestiary Image", sInternalName, sDLPath, fRenderX, fRenderY)
    //AM_SetPropertyJournal("Set Bestiary Paragon", sInternalName, sDLPath, fRenderX, fRenderY)
    //AM_SetPropertyJournal("Set Bestiary Turn Ico", sInternalName, sDLPath)
    //AM_SetPropertyJournal("Set Bestiary Defeat Count", sInternalName, iCount)
    //AM_SetPropertyJournal("Set Bestiary Defeat Paragon", sInternalName, iCount)
    //AM_SetPropertyJournal("Set Bestiary Defeat Max", sInternalName, iCount)
    //AM_SetPropertyJournal("Set Bestiary Can Transform You", sInternalName, bFlag)
    //AM_SetPropertyJournal("Set Bestiary Level", sInternalName, iLevel)
    //AM_SetPropertyJournal("Set Bestiary Statistics", sInternalName, iHP, iAtk, iIni, iAcc, iEvd, iPrt)
    //AM_SetPropertyJournal("Set Bestiary Results", sInternalName, iExp, iPlatina)
    //AM_SetPropertyJournal("Set Bestiary Resistances", sInternalName, iSlash, iStrike, iPierce, iFlame, iFreeze, iShock, iCrusade, iObscure, iBleed, iPoison, iCorrode, iTerrify)
    //AM_SetPropertyJournal("Allocate Bestiary Description Lines", sInternalName, iCount)
    //AM_SetPropertyJournal("Set Bestiary Description Line", sInternalName, iLine, sDescription)

    //--[Profiles]
    //AM_SetPropertyJournal("Add Profile Entry", sInternalName, sDisplayName)
    //AM_SetPropertyJournal("Set Profile Image", sInternalName, sDLPath, fOffsetX, fOffsetY)
    //AM_SetPropertyJournal("Allocate Profile Description Lines", sInternalName, iCount)
    //AM_SetPropertyJournal("Set Profile Description Line", sInternalName, iLine, sDescription)
    //AM_SetPropertyJournal("Set Profile Description Auto", sInternalName, sDescription)

    //--[Quests]
    //AM_SetPropertyJournal("Add Quest Entry", sInternalName, sDisplayName)
    //AM_SetPropertyJournal("Allocate Quest Description Lines", sInternalName, iCount)
    //AM_SetPropertyJournal("Set Quest Description Line", sInternalName, iLine, sDescription)
    //AM_SetPropertyJournal("Set Quest Objective", sInternalName, sObjective)

    //--[Locations]
    //AM_SetPropertyJournal("Add Location Entry", sInternalName, sDisplayName)
    //AM_SetPropertyJournal("Set Location Image", sInternalName, sDLPath)
    //AM_SetPropertyJournal("Set Location Image Offsets", sInternalName, fXOffset, fYOffset)
    //AM_SetPropertyJournal("Allocate Location Description Lines", sInternalName, iCount)
    //AM_SetPropertyJournal("Set Location Description Line", sInternalName, iLine, sDescription)

    //--[Paragons]
    //AM_SetPropertyJournal("Add Paragon Entry", sInternalName, sDisplayName)
    //AM_SetPropertyJournal("Set Paragon Enabled", sInternalName, bIsEnabled)
    //AM_SetPropertyJournal("Set Paragon KO Counts", sInternalName, iKOCurrent, iKONeeded)
    //AM_SetPropertyJournal("Set Paragon Has Defeated Activator", sInternalName, bHasDefeatedActivator)
    //AM_SetPropertyJournal("Set Paragon Enabled Path", sInternalName, sPath)

    //--[Combat]
    //AM_SetPropertyJournal("Add Combat Glossary Entry", sInternalName, sDisplayName)
    //AM_SetPropertyJournal("Allocate Combat Glossary Pieces", sInternalName, iTotalPieces)
    //AM_SetPropertyJournal("Allocate Combat Glossary Text Lines", sInternalName, iSlot, iTotal, fXPos, fYPos)
    //AM_SetPropertyJournal("Set Combat Glossary Text", sInternalName, iSlot, iLine, sTextLine, sFont, iFlags)
    //AM_SetPropertyJournal("Set Combat Glossary Image", sInternalName, iSlot, sDLPath, fXPos, fYPos)

    //--[Achievements]
    //AM_SetPropertyJournal("Set Achievement Title", sName) (Static)
    //AM_SetPropertyJournal("Set Achievement Show String", sName) (Static)
    //AM_SetPropertyJournal("Set Achievement Hide String", sName) (Static)
    //AM_SetPropertyJournal("Set Achievement Over Image", sDLPath)
    //AM_SetPropertyJournal("Clear Achievements")
    //AM_SetPropertyJournal("Register Achievement Category", sInternalName, sDisplayName, sImagePath)
    //AM_SetPropertyJournal("Register Achievement", sCategoryName, sInternalName, sDisplayName, sImagePath, sDescription)
    //AM_SetPropertyJournal("Allocate Achievement Requirements", sInternalName, iRequirements)
    //AM_SetPropertyJournal("Set Achievement Requirements", sInternalName, iSlot, sRequirementName, fValueMax, bDisplayAsInt, bDisplayAsCheck)
    //AM_SetPropertyJournal("Set Achievement Progress", sInternalName, iSlot, fCurrentValue)
    //AM_SetPropertyJournal("Set Achievement Locked", sInternalName, bIsLocked)
    //AM_SetPropertyJournal("Set Achievement Spoilered", sInternalName, bIsSpoilered)
    //AM_SetPropertyJournal("Unlock Achievement", sInternalName)
    //AM_SetPropertyJournal("Write Achievements")
    //AM_SetPropertyJournal("Read Achievements")
    //AM_SetPropertyJournal("Crossreference Achievements")
    //AM_SetPropertyJournal("Allocate Achievement Remaps", iTotal)
    //AM_SetPropertyJournal("Set Achievement Remap", iSlot, sProgramName, sSteamName)

    ///--[Arg Check]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetPropertyJournal");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Script fired to determine bestiary content.
    if(!strcasecmp(rSwitchType, "Bestiary Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xBestiaryResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine profile content.
    else if(!strcasecmp(rSwitchType, "Profile Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xProfileResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine quest content.
    else if(!strcasecmp(rSwitchType, "Quest Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xQuestResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine location content.
    else if(!strcasecmp(rSwitchType, "Location Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xLocationResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine paragon entries.
    else if(!strcasecmp(rSwitchType, "Paragon Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xParagonResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine combat glossary.
    else if(!strcasecmp(rSwitchType, "Combat Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xCombatResolveScript, lua_tostring(L, 2));
        return 0;
    }

    ///--[Dynamic]
    //--Dynamic. Get the object in question.
    AdventureMenu *rAdvMenu = AdventureMenu::Fetch();
    if(!rAdvMenu)
    {
        //--If no menu is found, but a level is found, then the level type is not compatible. Ignore it.
        if(MapManager::Fetch()->GetActiveLevel() != NULL)
        {
            return 0;
        }

        //--Otherwise, issue a debug call.
        return LuaTypeError("AM_GetPropertyJournal", L);
    }

    //--Get the sub-object.
    AdvUIJournal *rJournalUI = rAdvMenu->GetJournalUI();
    if(!rJournalUI)
    {
        return LuaTypeError("AM_GetPropertyJournal", L);
    }

    ///--[Bestiary]
    //--Adds a new bestiary entry.
    if(!strcasecmp(rSwitchType, "Add Bestiary Entry") && tArgs == 3)
    {
        rJournalUI->CreateBestiaryEntry(lua_tostring(L, 2));
        rJournalUI->SetBestiaryDisplayName(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets image for a bestiary entry.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Image") && tArgs == 5)
    {
        rJournalUI->SetBestiaryImage(lua_tostring(L, 2), lua_tostring(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Sets paragon image for a bestiary entry.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Paragon") && tArgs == 5)
    {
        rJournalUI->SetBestiaryParagon(lua_tostring(L, 2), lua_tostring(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Sets turn order indicator.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Turn Ico") && tArgs == 3)
    {
        rJournalUI->SetBestiaryTurnOrderIco(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets how many times an enemy has been defeated.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Defeat Count") && tArgs == 3)
    {
        rJournalUI->SetBestiaryKOCount(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets how many times the paragon of this enemy has been defeated.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Defeat Paragon") && tArgs == 3)
    {
        rJournalUI->SetBestiaryParagonKOCount(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets how many times an enemy has to be defeated to see all their stats.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Defeat Max") && tArgs == 3)
    {
        rJournalUI->SetBestiaryKOMax(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets if the bestiary shows the "Can Transform You" line or not.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Can Transform You") && tArgs == 3)
    {
        rJournalUI->SetBestiaryCanTransformYou(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Level indicator. Used for JP computations.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Level") && tArgs == 3)
    {
        rJournalUI->SetBestiaryLevel(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets statistics. If not called, stats are hidden.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Statistics") && tArgs == 8)
    {
        rJournalUI->SetBestiaryStatistics(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7), lua_tointeger(L, 8));
    }
    //--Sets what the player gets for combat from this enemy.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Results") && tArgs == 4)
    {
        rJournalUI->SetBestiaryResults(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets resistance. If not called, resists are hidden.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Resistances") && tArgs == 14)
    {
        rJournalUI->SetBestiaryResistances(lua_tostring (L, 2), lua_tointeger(L,  3), lua_tointeger(L,  4), lua_tointeger(L,  5), lua_tointeger(L,  6), lua_tointeger(L,  7), lua_tointeger(L, 8),
                                      lua_tointeger(L, 9), lua_tointeger(L, 10), lua_tointeger(L, 11), lua_tointeger(L, 12), lua_tointeger(L, 13), lua_tointeger(L, 14));
    }
    //--Automatically handles bestiary description.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Description Auto") && tArgs == 3)
    {
        rJournalUI->SetBestiaryDescriptionAuto(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Allocates how many description lines to render.
    else if(!strcasecmp(rSwitchType, "Allocate Bestiary Description Lines") && tArgs == 3)
    {
        rJournalUI->AllocateBestiaryDescriptionLines(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets a description line.
    else if(!strcasecmp(rSwitchType, "Set Bestiary Description Line") && tArgs == 4)
    {
        rJournalUI->SetBestiaryDescriptionLine(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    ///--[Profiles]
    //--Create a new profile entry.
    else if(!strcasecmp(rSwitchType, "Add Profile Entry") && tArgs == 3)
    {
        rJournalUI->CreateProfileEntry(lua_tostring(L, 2));
        rJournalUI->SetProfileDisplayName(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Set the portrait for a profile and its offsets.
    else if(!strcasecmp(rSwitchType, "Set Profile Image") && tArgs == 5)
    {
        rJournalUI->SetProfilePortrait(lua_tostring(L, 2), lua_tostring(L, 3));
        rJournalUI->SetProfileOffsets(lua_tostring(L, 2), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Set how many description lines for a profile.
    else if(!strcasecmp(rSwitchType, "Allocate Profile Description Lines") && tArgs == 3)
    {
        rJournalUI->AllocateProfileDescriptionLines(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the description line in question.
    else if(!strcasecmp(rSwitchType, "Set Profile Description Line") && tArgs == 4)
    {
        rJournalUI->SetProfileDescriptionLine(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Automatically uses [BR] and \\n to set the sizings rather than doing it in Lua.
    else if(!strcasecmp(rSwitchType, "Set Profile Description Auto") && tArgs >= 3)
    {
        rJournalUI->SetProfileDescriptionAuto(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Quests]
    //--Creates a new quest entry.
    else if(!strcasecmp(rSwitchType, "Add Quest Entry") && tArgs == 3)
    {
        rJournalUI->CreateQuestEntry(lua_tostring(L, 2));
        rJournalUI->SetQuestDisplayName(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Automates setting quest description lines using internal lengths and breakpoints.
    else if(!strcasecmp(rSwitchType, "Set Quest Description Auto") && tArgs == 3)
    {
        rJournalUI->SetQuestDescriptionAuto(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Allocates description lines for the quest description.
    else if(!strcasecmp(rSwitchType, "Allocate Quest Description Lines") && tArgs == 3)
    {
        rJournalUI->AllocateQuestDescriptionLines(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the description for the quest.
    else if(!strcasecmp(rSwitchType, "Set Quest Description Line") && tArgs == 4)
    {
        rJournalUI->SetQuestDescriptionLine(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Sets the objective line for the quest.
    else if(!strcasecmp(rSwitchType, "Set Quest Objective") && tArgs == 3)
    {
        rJournalUI->SetQuestObjective(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Locations]
    //--Creates a new location entry.
    else if(!strcasecmp(rSwitchType, "Add Location Entry") && tArgs == 3)
    {
        rJournalUI->CreateLocationEntry(lua_tostring(L, 2));
        rJournalUI->SetLocationDisplayName(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets which image displays for the given area.
    else if(!strcasecmp(rSwitchType, "Set Location Image") && tArgs == 3)
    {
        rJournalUI->SetLocationImage(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets the offsets for the location image.
    else if(!strcasecmp(rSwitchType, "Set Location Image Offsets") && tArgs == 4)
    {
        rJournalUI->SetLocationImageOffsets(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Allocates location description lines.
    else if(!strcasecmp(rSwitchType, "Allocate Location Description Lines") && tArgs == 3)
    {
        rJournalUI->AllocateLocationDescriptionLines(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the location description line.
    else if(!strcasecmp(rSwitchType, "Set Location Description Line") && tArgs == 4)
    {
        rJournalUI->SetLocationDescriptionLine(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Sets the location description automatically.
    else if(!strcasecmp(rSwitchType, "Set Location Description Auto") && tArgs == 3)
    {
        rJournalUI->SetLocationDescriptionAuto(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Paragons]
    //--Registers a new paragon journal entry.
    else if(!strcasecmp(rSwitchType, "Add Paragon Entry") && tArgs == 3)
    {
        rJournalUI->CreateParagonEntry(lua_tostring(L, 2));
        rJournalUI->SetParagonDisplayName(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets whether or not the paragon should spawn.
    else if(!strcasecmp(rSwitchType, "Set Paragon Enabled") && tArgs == 3)
    {
        rJournalUI->SetParagonIsEnabled(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets how many KOs should appear on the UI.
    else if(!strcasecmp(rSwitchType, "Set Paragon KO Counts") && tArgs == 4)
    {
        rJournalUI->SetParagonKOCounts(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets whether or not the player has defeated the activating paragon for an area.
    else if(!strcasecmp(rSwitchType, "Set Paragon Has Defeated Activator") && tArgs == 3)
    {
        rJournalUI->SetParagonHasDefeatedActivator(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets the DataLibrary path for the enabled variable.
    else if(!strcasecmp(rSwitchType, "Set Paragon Enabled Path") && tArgs == 3)
    {
        rJournalUI->SetParagonEnabledPath(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Combat]
    //--Creates a new combat glossary page.
    else if(!strcasecmp(rSwitchType, "Add Combat Glossary Entry") && tArgs == 3)
    {
        rJournalUI->CreateCombatGlossaryEntry(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Allocates how many pieces the glossary page has.
    else if(!strcasecmp(rSwitchType, "Allocate Combat Glossary Pieces") && tArgs == 3)
    {
        rJournalUI->AllocateCombatGlossaryPieces(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Marks a piece for text lines and allocates space.
    else if(!strcasecmp(rSwitchType, "Allocate Combat Glossary Text Lines") && tArgs == 6)
    {
        rJournalUI->AllocateCombatGlossaryText(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6));
    }
    //--Sets a text line for a text piece.
    else if(!strcasecmp(rSwitchType, "Set Combat Glossary Text") && tArgs == 7)
    {
        rJournalUI->SetCombatGlossaryText(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5), lua_tostring(L, 6), lua_tointeger(L, 7));
    }
    //--Set text lines automatically using internal breakpoints.
    else if(!strcasecmp(rSwitchType, "Set Combat Glossary Text Auto") && tArgs == 8)
    {
        rJournalUI->SetCombatGlossaryAuto(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tostring(L, 6), lua_tostring(L, 7), lua_tointeger(L, 8));
    }
    //--Sets the image for an image piece.
    else if(!strcasecmp(rSwitchType, "Set Combat Glossary Image") && tArgs == 6)
    {
        rJournalUI->SetCombatGlossaryImage(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6));
    }
    ///--[Achievements]
    //--Changes the "Achievements" title.
    else if(!strcasecmp(rSwitchType, "Set Achievement Title") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xAchievementTitle, lua_tostring(L, 2));
    }
    //--Changes the "Show Achievements" string.
    else if(!strcasecmp(rSwitchType, "Set Achievement Show String") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xAchievementShowString, lua_tostring(L, 2));
    }
    //--Changes the "Hide Achievements" string.
    else if(!strcasecmp(rSwitchType, "Set Achievement Hide String") && tArgs == 2)
    {
        ResetString(AdvUIJournal::xAchievementHideString, lua_tostring(L, 2));
    }
    //--DL path of the small image that appears on top of the achievement UI. Changes based on the chapter.
    else if(!strcasecmp(rSwitchType, "Set Achievement Over Image") && tArgs == 2)
    {
        rJournalUI->SetAchievementOverGraphic(lua_tostring(L, 2));
    }
    //--Clears achievements.
    else if(!strcasecmp(rSwitchType, "Clear Achievements") && tArgs == 1)
    {
        AdvUIJournal::xAchievementCategories->ClearList();
    }
    //--Creates a new achievement category.
    else if(!strcasecmp(rSwitchType, "Register Achievement Category") && tArgs == 4)
    {
        rJournalUI->RegisterAchievementCategory(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Creates an achievement within a category.
    else if(!strcasecmp(rSwitchType, "Register Achievement") && tArgs == 6)
    {
        rJournalUI->RegisterAchievement(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
    }
    //--Allocates how many progress slots an achievement has.
    else if(!strcasecmp(rSwitchType, "Allocate Achievement Requirements") && tArgs == 3)
    {
        rJournalUI->AllocateAchievementRequirements(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the arbitrary progress requirement for an achievement.
    else if(!strcasecmp(rSwitchType, "Set Achievement Requirements") && tArgs == 7)
    {
        rJournalUI->SetAchievementRequirements(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4), lua_tonumber(L, 5), lua_toboolean(L, 6), lua_toboolean(L, 7));
    }
    //--Sets the arbitrary progress value for an achievement.
    else if(!strcasecmp(rSwitchType, "Set Achievement Progress") && tArgs == 4)
    {
        rJournalUI->SetAchievementProgress(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tonumber(L, 4));
    }
    //--Sets if an achievement hides its details until unlocked.
    else if(!strcasecmp(rSwitchType, "Set Achievement Locked") && tArgs == 3)
    {
        rJournalUI->SetAchievementLocked(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets if an achievement hides its details until unlocked, or the player presses a button.
    else if(!strcasecmp(rSwitchType, "Set Achievement Spoilered") && tArgs == 3)
    {
        rJournalUI->SetAchievementSpoilered(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Immediately unlocks the marked achievement. Also implicitly writes achievement data.
    else if(!strcasecmp(rSwitchType, "Unlock Achievement") && tArgs == 2)
    {
        rJournalUI->UnlockAchievement(lua_tostring(L, 2));
        rJournalUI->WriteAchievementsToFile();
    }
    //--Writes achievements right now.
    else if(!strcasecmp(rSwitchType, "Write Achievements") && tArgs == 1)
    {
        fprintf(stderr, "Write achievements.\n");
        rJournalUI->WriteAchievementsToFile();
    }
    //--Reads achievements right now.
    else if(!strcasecmp(rSwitchType, "Read Achievements") && tArgs == 1)
    {
        rJournalUI->ReadAchievementsFromFile();
    }
    //--Creates steam achievements for all program achievements.
    else if(!strcasecmp(rSwitchType, "Crossreference Achievements") && tArgs == 1)
    {
        rJournalUI->CrossReferenceAchievements();
    }
    //--Sets how many achievement remaps there are total.
    else if(!strcasecmp(rSwitchType, "Allocate Achievement Remaps") && tArgs == 2)
    {
        rJournalUI->AllocateAchievementRemaps(lua_tointeger(L, 2));
    }
    //--Sets a specific remap program and steam name.
    else if(!strcasecmp(rSwitchType, "Set Achievement Remap") && tArgs == 4)
    {
        rJournalUI->SetAchievementRemap(lua_tointeger(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("AM_SetPropertyJournal", rSwitchType, tArgs);
    }
    return 0;
}
