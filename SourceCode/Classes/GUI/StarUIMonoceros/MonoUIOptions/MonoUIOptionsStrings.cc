//--Base
#include "MonoUIOptions.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///======================================== Translation ===========================================
MonoUIOptions_Strings::MonoUIOptions_Strings()
{
    //--Local name: "MonoUIOptions"
    MonoUIOptions::xrStringData = this;

    //--Strings
    str.mTextSettings                          = InitializeString("MonoUIOptions_mSettings");             //"Settings"
    str.mTextGameplay                          = InitializeString("MonoUIOptions_mGameplay");             //"Gameplay"
    str.mTextAudio                             = InitializeString("MonoUIOptions_mAudio");                //"Audio"
    str.mTextControls                          = InitializeString("MonoUIOptions_mControls");             //"Controls"
    str.mTextDebug                             = InitializeString("MonoUIOptions_mDebug");                //"Debug"
    str.mTextDefaults                          = InitializeString("MonoUIOptions_mDefaults");             //"Defaults"
    str.mTextOkay                              = InitializeString("MonoUIOptions_mOkay");                 //"Okay"
    str.mTextChangesPending                    = InitializeString("MonoUIOptions_mChangesPending");       //"Changes Pending..."
    str.mTextSomeChangesRequireAProgramRestart = InitializeString("MonoUIOptions_mProgramRestart");       //"Some Changes Require A Program Restart."
    str.mTextYouHaveUnsavedChangesPending      = InitializeString("MonoUIOptions_mUnsavedChanges");       //"You have unsaved changes pending."
    str.mTextReallyExit                        = InitializeString("MonoUIOptions_mReallyExit");           //"Really Exit?"
    str.mTextPressNewPrimaryKeyNow             = InitializeString("MonoUIOptions_mNewPrimaryKey");        //"Press new primary key now."
    str.mTextPressNewSecondaryKeyNow           = InitializeString("MonoUIOptions_mNewSecondaryKey");      //"Press new secondary key now."
    str.mTextHelpMenu                          = InitializeString("MonoUIOptions_mHelpMenu");             //"Help Menu"
    str.mTextTouristMode                       = InitializeString("MonoUIOptions_mTouristMode");          //"Tourist Mode"
    str.mTextAutoHastenDialogue                = InitializeString("MonoUIOptions_mAutoHastenDialogue");   //"Auto-Hasten Dialogue"
    str.mTextCombatMemoryCursor                = InitializeString("MonoUIOptions_mCombatMemoryCursor");   //"Combat Memory Cursor"
    str.mTextMaxAutosaves                      = InitializeString("MonoUIOptions_mMaxAutosaves");         //"Max Autosaves"
    str.mTextAllowAssetStreaming               = InitializeString("MonoUIOptions_mAssetStreaming");       //"Allow Asset Streaming"
    str.mTextUseRAMLoading                     = InitializeString("MonoUIOptions_mRamLoading");           //"Use RAM Loading"
    str.mTextUnloadActorsAfterDialogue         = InitializeString("MonoUIOptions_mUnloadActors");         //"Unload Actors After Dialogue"
    str.mTextUseFadingUI                       = InitializeString("MonoUIOptions_mFadingUI");             //"Use Fading UI"
    str.mTextResolution                        = InitializeString("MonoUIOptions_mResolution");           //"Resolution"
    str.mTextCensorIzana                       = InitializeString("MonoUIOptions_mCensorIzana");          //"Censor Izana"
    str.mTextCensorCaelyn                      = InitializeString("MonoUIOptions_mCensorCaelyn");         //"Censor Caelyn"
    str.mTextCensorCyrano                      = InitializeString("MonoUIOptions_mCensorCyrano");         //"Censor Cyrano"
    str.mTextCensorEnemies                     = InitializeString("MonoUIOptions_mCensorEnemies");        //"Censor Enemies"
    str.mTextMusicVolume                       = InitializeString("MonoUIOptions_mMusicVolume");          //"Music Volume"
    str.mTextSoundVolume                       = InitializeString("MonoUIOptions_mSoundVolume");          //"Sound Volume"
    str.mTextIzanaVoiceVolume                  = InitializeString("MonoUIOptions_mIzanaVolume");          //"Izana Voice Volume"
    str.mTextCaelynVoiceVolume                 = InitializeString("MonoUIOptions_mCaelynVolume");         //"Caelyn Voice Volume"
    str.mTextCyranoVoiceVolume                 = InitializeString("MonoUIOptions_mCyranoVolume");         //"Cyrano Voice Volume"
    str.mTextSaveVolumesInSavefile             = InitializeString("MonoUIOptions_mSaveVolumes");          //"Save Volumes In Savefile"
    str.mTextActivateAccept                    = InitializeString("MonoUIOptions_mControlActivate");      //"Activate/Accept"
    str.mTextCancelOpenMenu                    = InitializeString("MonoUIOptions_mControlCancel");        //"Cancel/Open Menu"
    str.mTextRun                               = InitializeString("MonoUIOptions_mControlRun");           //"Run"
    str.mTextUp                                = InitializeString("MonoUIOptions_mControlUp");            //"Up"
    str.mTextRight                             = InitializeString("MonoUIOptions_mControlRight");         //"Right"
    str.mTextDown                              = InitializeString("MonoUIOptions_mControlDown");          //"Down"
    str.mTextLeft                              = InitializeString("MonoUIOptions_mControlLeft");          //"Left"
    str.mTextShoulderLeft                      = InitializeString("MonoUIOptions_mControlShoulderLeft");  //"Shoulder Left"
    str.mTextShoulderRight                     = InitializeString("MonoUIOptions_mControlShoulderRight"); //"Shoulder Right"
    str.mTextAbility1                          = InitializeString("MonoUIOptions_mControlAbility1");      //"Ability 1"
    str.mTextAbility2                          = InitializeString("MonoUIOptions_mControlAbility2");      //"Ability 2"
    str.mTextAbility3                          = InitializeString("MonoUIOptions_mControlAbility3");      //"Ability 3"
    str.mTextAbility4                          = InitializeString("MonoUIOptions_mControlAbility4");      //"Ability 4"
    str.mTextAbility5                          = InitializeString("MonoUIOptions_mControlAbility5");      //"Ability 5"
    str.mTextEmergencyEscape                   = InitializeString("MonoUIOptions_mControlEscape");        //"Emergency Escape"
    str.mTextOpenDebugMenu                     = InitializeString("MonoUIOptions_mControlDebug");         //"Open Debug Menu"
    str.mTextToggleFuilscreen                  = InitializeString("MonoUIOptions_mControlFullscreen");    //"Toggle Fullscreen"
    str.mTextRebuildKerning                    = InitializeString("MonoUIOptions_mControlKerning");       //"Rebuild Kerning"
    str.mTextRebuildShaders                    = InitializeString("MonoUIOptions_mControlShaders");       //"Rebuild Shaders"
    str.mTextDisableShaderOutline              = InitializeString("MonoUIOptions_mDisableOutline");       //"Disable Shader: Outline"
    str.mTextDisableShaderUnderwater           = InitializeString("MonoUIOptions_mDisableUnderwater");    //"Disable Shader: Underwater"
    str.mTextDisableShaderLighting             = InitializeString("MonoUIOptions_mDisableLighting");      //"Disable Shader: Lighting"
    str.mTextDebugControlsMayNotBeRebound      = InitializeString("MonoUIOptions_mDebugControlsRebound"); //"Debug controls may not be rebound."
    str.mTextTheyAreHereForReference           = InitializeString("MonoUIOptions_mHereForReference");     //"They are here for reference."
    str.mTextTrue                              = InitializeString("MonoUIOptions_mTrue");                 //"True"
    str.mTextFalse                             = InitializeString("MonoUIOptions_mFalse");                //"False"
    str.mTextUnbound                           = InitializeString("MonoUIOptions_mUnbound");              //"Unbound"
    str.mTextControlsSavedImmediately          = InitializeString("MonoUIOptions_mImmediateRebound");     //"Controls are saved immediately when rebound."

    //--Help Sequence
    char tBuffer[100];
    for(int i = 0; i < 6; i ++)
    {
        sprintf(tBuffer, "MonoUIOptions_mHelpText%02i", i);
        str.mTextHelp[i] = InitializeString(tBuffer);
    }

    //--Control Strings
    str.mTextControlShowHelp           = InitializeString("MonoUIOptions_mTextControl");                   //"[IMG0] Show Help"
    str.mTextControlDiscardChanges     = InitializeString("MonoUIOptions_mTextControlDiscardChanges");     //"[IMG0] Discard Changes"
    str.mTextControlReturnToMenu       = InitializeString("MonoUIOptions_mTextControlReturnToMenu");       //"[IMG0] Return to Options Menu"
    str.mTextControlDetails            = InitializeString("MonoUIOptions_mTextControlDetails");            //"[IMG0] Option Details"
    str.mTextControlPrevPage           = InitializeString("MonoUIOptions_mTextControlPrevPage");           //"[IMG0] Previous Page"
    str.mTextControlNextPage           = InitializeString("MonoUIOptions_mTextControlNextPage");           //"[IMG0] Next Page"
    str.mTextControlBindPrimary        = InitializeString("MonoUIOptions_mTextControlBindPrimary");        //"[IMG0] Bind Primary"
    str.mTextControlBindSecondary      = InitializeString("MonoUIOptions_mTextControlBindSecondary");      //"[IMG0] Bind Secondary"
    str.mTextControlControlDefaults    = InitializeString("MonoUIOptions_mTextControlControlDefaults");    //"[IMG0] Restore Control Defaults"
    str.mTextControlAllControlDefaults = InitializeString("MonoUIOptions_mTextControlAllControlDefaults"); //"[IMG0] Restore All Control Defaults"

    //--Options Descriptions
    str.mOptionHelpTourist[0]              = InitializeString("All your attacks are guaranteed to hit, and deal triple damage. This option is meant for players");
    str.mOptionHelpTourist[1]              = InitializeString("who just want to see what the game is about and have an easier time, are messing around, or");
    str.mOptionHelpTourist[2]              = InitializeString("are stuck on one particular boss.");
    str.mOptionHelpTourist[3]              = InitializeString("The flag is stored in the save file and must be toggled on or off in-game, not on the title screen.");
    str.mOptionHelpHasten[0]               = InitializeString("During conversation, you can hold down the [Activate] key to speed up dialogue.");
    str.mOptionHelpHasten[1]               = InitializeString("If this option is True, dialogue will always be sped up even if the key is not pressed.");
    str.mOptionHelpMemory[0]               = InitializeString("During battle, the cursor will reset to the top whenever a new action begins.");
    str.mOptionHelpMemory[1]               = InitializeString("If this option is True, the menu will remember your last option and use that instead.");
    str.mOptionHelpAutosaves[0]            = InitializeString("The game autosaves when transitioning between areas. This option specifies how many backup autosaves you");
    str.mOptionHelpAutosaves[1]            = InitializeString("would like to keep. They are in your Program/Saves/Autosave/ folder.");
    str.mOptionHelpAutosaves[2]            = InitializeString("If the value is 0, no autosaves will be made.");
    str.mOptionHelpAssetStreaming[0]       = InitializeString("If this option is True, the game will dynamically load assets on the fly. This may cause minor stuttering");
    str.mOptionHelpAssetStreaming[1]       = InitializeString("in some locations, but greatly decreases RAM usage.");
    str.mOptionHelpAssetStreaming[2]       = InitializeString("If this option is False, all assets are loaded when the game loads a chapter. This will take longer and ");
    str.mOptionHelpAssetStreaming[3]       = InitializeString("use more RAM, particularly video card RAM, but will not stutter.");
    str.mOptionHelpAssetStreaming[4]       = InitializeString("Set this option to True if you are using an older computer without sufficient RAM.");
    str.mOptionHelpAssetStreaming[5]       = InitializeString("You will need to restart the game for this option to take effect.");
    str.mOptionHelpRAMLoading[0]           = InitializeString("If this option is True, the program will load assets by loading the entire file into memory, then parsing");
    str.mOptionHelpRAMLoading[1]           = InitializeString("the data. This is faster than loading each piece from the hard drive, especially when using a magnetic");
    str.mOptionHelpRAMLoading[2]           = InitializeString("disk drive, but consumes considerably more RAM.");
    str.mOptionHelpRAMLoading[3]           = InitializeString("If this option is False, less RAM is used by the loading will take longer.");
    str.mOptionHelpRAMLoading[4]           = InitializeString("Set this option to False if you are using an older computer without sufficient RAM.");
    str.mOptionHelpRAMLoading[5]           = InitializeString("You will need to restart the game for this option to take effect.");
    str.mOptionHelpUnloadActors[0]         = InitializeString("Actors use portraits to speak during dialogue. These portraits consume video memory when not in use.");
    str.mOptionHelpUnloadActors[1]         = InitializeString("If this option is True, these portraits are unloaded when not in use, and dynamically loaded when");
    str.mOptionHelpUnloadActors[2]         = InitializeString("dialogue begins. You may briefly see a dialogue frame not appear when loading.");
    str.mOptionHelpUnloadActors[3]         = InitializeString("Set this option to True if you are using an older computer without sufficient RAM.");
    str.mOptionHelpUnloadActors[4]         = InitializeString("You will need to restart the game for this option to take effect.");
    str.mOptionHelpFadingUI[0]             = InitializeString("When switching UIs, the program will either fade the menus in, or slide them from offscreen.");
    str.mOptionHelpFadingUI[1]             = InitializeString("Use whichever you find most comfortable.");
    str.mOptionHelpResolution[0]           = InitializeString("The size of the screen the game uses to render. The game's native resolution is 1366x768.");
    str.mOptionHelpResolution[1]           = InitializeString("You can toggle fullscreen with the F11 key. A restart is required to take effect.");
    str.mOptionHelpCensorI[0]              = InitializeString("'Censor' Izana's various forms, some of which are topless. This can also be accessed from the");
    str.mOptionHelpCensorI[1]              = InitializeString("guardian statues at the Heronstadt inn.");
    str.mOptionHelpCensorI[2]              = InitializeString("I do not guarantee this will keep you from getting banned on Twitch.");
    str.mOptionHelpCensorI[3]              = InitializeString("This setting can only be edited in-game and is stored in the savefile.");
    str.mOptionHelpCensorA[0]              = InitializeString("'Censor' Caelyn's various forms, some of which are topless. This can also be accessed from the");
    str.mOptionHelpCensorA[1]              = InitializeString("guardian statues at the Heronstadt inn.");
    str.mOptionHelpCensorA[2]              = InitializeString("I do not guarantee this will keep you from getting banned on Twitch.");
    str.mOptionHelpCensorA[3]              = InitializeString("This setting can only be edited in-game and is stored in the savefile.");
    str.mOptionHelpCensorY[0]              = InitializeString("'Censor' Cyrano's various forms, some of which are topless. This can also be accessed from the");
    str.mOptionHelpCensorY[1]              = InitializeString("guardian statues at the Heronstadt inn.");
    str.mOptionHelpCensorY[2]              = InitializeString("I do not guarantee this will keep you from getting banned on Twitch.");
    str.mOptionHelpCensorY[3]              = InitializeString("This setting can only be edited in-game and is stored in the savefile.");
    str.mOptionHelpCensorE[0]              = InitializeString("'Censor' enemies with topless exposed chests. Only the female ones, luckily those hulking orcs are a-okay.");
    str.mOptionHelpCensorE[1]              = InitializeString("I do not guarantee this will keep you from getting banned on Twitch.");
    str.mOptionHelpMusicVol[0]             = InitializeString("This slider affects how loud all music tracks are.");
    str.mOptionHelpMusicVol[1]             = InitializeString("Hold the Run (LShift) button to adjust volume by 1 per press instead of 5.");
    str.mOptionHelpSoundVol[0]             = InitializeString("This slider affects how loud all sound effects are.");
    str.mOptionHelpSoundVol[1]             = InitializeString("Hold the Run (LShift) button to adjust volume by 1 per press instead of 5.");
    str.mOptionHelpIzanaVol[0]             = InitializeString("This slider affects how loud Izana speaking during battle is.");
    str.mOptionHelpIzanaVol[1]             = InitializeString("Hold the Run (LShift) button to adjust volume by 1 per press instead of 5.");
    str.mOptionHelpCaelynVol[0]            = InitializeString("This slider affects how loud Caelyn speaking during battle is.");
    str.mOptionHelpCaelynVol[1]            = InitializeString("Hold the Run (LShift) button to adjust volume by 1 per press instead of 5.");
    str.mOptionHelpCyranoVol[0]            = InitializeString("This slider affects how loud Cyrano speaking during battle is.");
    str.mOptionHelpCyranoVol[1]            = InitializeString("Hold the Run (LShift) button to adjust volume by 1 per press instead of 5.");
    str.mOptionHelpAudioInSaves[0]         = InitializeString("Sound and Music volumes are written to each adventure save file.");
    str.mOptionHelpAudioInSaves[1]         = InitializeString("If this option is True, the volumes will be loaded when the save file is loaded.");
    str.mOptionHelpAudioInSaves[2]         = InitializeString("If this option is False, the volumes will stay the same when loading a save file.");
    str.mOptionHelpControlActivate[0]      = InitializeString("Press this button to examine things, confirm menu options, and who knows what else.");
    str.mOptionHelpControlActivate[1]      = InitializeString("If you've gotten to this options menu, you've pressed this button before.");
    str.mOptionHelpControlActivate[2]      = InitializeString("You know what it does.");
    str.mOptionHelpControlCancel[0]        = InitializeString("Exits interfaces and cancels things. Hold this down along with Activate to greatly speed up text crawling");
    str.mOptionHelpControlCancel[1]        = InitializeString("during dialogue.");
    str.mOptionHelpControlRun[0]           = InitializeString("Hold this down in the overworld to run.");
    str.mOptionHelpControlUp[0]            = InitializeString("It's the up key. Up is an axiomatic, prime concept.");
    str.mOptionHelpControlRight[0]         = InitializeString("This is the one that doesn't make an L with your thumb.");
    str.mOptionHelpControlDown[0]          = InitializeString("Represents the quality of the jokes in these descriptions.");
    str.mOptionHelpControlLeft[0]          = InitializeString("Not right. Not middle. Not forward. Left. Accept no substitutes.");
    str.mOptionHelpControlShoulderLeft[0]  = InitializeString("Page-up or shoulder button left. Switches pages in some UI's.");
    str.mOptionHelpControlShoulderRight[0] = InitializeString("Page-down or shoulder button right. Switches pages in some UI's.");
    str.mOptionHelpControlAbility1[0]      = InitializeString("Activates the field ability in the first slot.");
    str.mOptionHelpControlAbility2[0]      = InitializeString("Activates the field ability in the second slot.");
    str.mOptionHelpControlAbility3[0]      = InitializeString("Activates the field ability in the fourth slot.");
    str.mOptionHelpControlAbility3[1]      = InitializeString("Just kidding, activates the one in the third slot.");
    str.mOptionHelpControlAbility4[0]      = InitializeString("Activates the field ability in the fourth slot.");
    str.mOptionHelpControlAbility5[0]      = InitializeString("Activates the field ability in the fifth slot.");
    str.mOptionHelpEmergencyEscape[0]      = InitializeString("Press this button twice to debug-exit from a battle. Emergencies only!");
    str.mOptionHelpDebugOpenMenu[0]        = InitializeString("Opens the Debug Menu when on the overworld. You must have debug mode activated or it won't do anything. ");
    str.mOptionHelpDebugOpenMenu[1]        = InitializeString("You can activate it with a handy password given out to patrons. Instructions are on the patreon!");
    str.mOptionHelpDebugOpenMenu[2]        = InitializeString("");
    str.mOptionHelpDebugFullscreen[0]      = InitializeString("Switches between fullscreen and windowed when pressed. Bound to both F12 and F11 as OSX holds the F12 key ");
    str.mOptionHelpDebugFullscreen[1]      = InitializeString("for its stupid crap.");
    str.mOptionHelpDebugFullscreen[2]      = InitializeString("I hate OSX. -Salty");
    str.mOptionHelpDebugKerning[0]         = InitializeString("Re-runs the kerning files to allow real-time editing of font kerning.");
    str.mOptionHelpDebugKerning[1]         = InitializeString("Useful if you're making a mod. Useless to normal players.");
    str.mOptionHelpDebugShaders[0]         = InitializeString("Re-runs the shaders and recompiles them. Good for modders.");
    str.mOptionHelpDebugOutline[0]         = InitializeString("Enemies on the overworld are extra tough if they have different coloured outlines. This is done via a shader.");
    str.mOptionHelpDebugOutline[1]         = InitializeString("If you believe the shader is causing system instability, disable it here.");
    str.mOptionHelpDebugUnderwater[0]      = InitializeString("The Underwater shader causes the screen to distort slightly when underwater.");
    str.mOptionHelpDebugUnderwater[1]      = InitializeString("If you believe the shader is causing system instability, disable it here.");
    str.mOptionHelpDebugLightning[0]       = InitializeString("Some areas of the game are dark. This is done via a shader.");
    str.mOptionHelpDebugLightning[1]       = InitializeString("If you believe the shader is causing system instability, disable it here.");
}
MonoUIOptions_Strings::~MonoUIOptions_Strings()
{
    for(int i = 0; i < MONOUIOPTIONS_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIOptions_Strings::GetSize()
{
    return MONOUIOPTIONS_STRINGS_TOTAL;
}
void MonoUIOptions_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIOPTIONS_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}
