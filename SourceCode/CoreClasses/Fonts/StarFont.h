///========================================== StarFont ============================================
//--API-independent font wrapper, for use in hazardous environments involving strings that must
//  appear on the screen. Supports UTF-8, assuming the API does anyway.
//--When using TTF styles, the font pre-caches all the bitmap data and uses atlasing to speed
//  rendering up. This takes time at boot but massively improves rendering speeds. Precaching
//  only uses the 96 ASCII letters, ignoring the others.
//--The Bitmap version uses Freetype by default. Other APIs could be supported in the future.
//  Allegro support has been dropped.

///--[How Kerning Works]
//--Because of the very high number of characters possible in UTF, kerning between them would
//  take too much space. Instead kerning is only stored for the 96 ASCII characters. Any other
//  characters just use their basic advance value when computing kerning size.

#pragma once

//--Defines what type of fonts the project is using. These are usually in the project
//  build options.
//#define _FONTTYPE_FREETYPE_

//#define _SYSTEMFONT_TTF_
#define _SYSTEMFONT_BITMAP_

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
struct RootLump;

///===================================== Local Definitions ========================================
///--[Local typedefs]
#if defined _FONTTYPE_FREETYPE_
    #include <ft2build.h>
    #include FT_FREETYPE_H
    typedef FT_Face StarInternalFont;
#endif

///--[Local Definitions]
#define SUGARFONT_NOEFFECTS 0x0000
#define SUGARFONT_BOLD 0x0001
#define SUGARFONT_ITALIC 0x0002
#define SUGARFONT_UNDERLINE 0x0004
#define SUGARFONT_AUTOCENTER_X 0x0008
#define SUGARFONT_AUTOCENTER_Y 0x0010
#define SUGARFONT_AUTOCENTER_XY (SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y)
#define SUGARFONT_RIGHTALIGN_X 0x0020
#define SUGARFONT_MIRRORX 0x0040
#define SUGARFONT_MIRRORY 0x0080
#define SUGARFONT_DOUBLERENDERX 0x0100
#define SUGARFONT_DOUBLERENDERY 0x0200
#define SUGARFONT_NOCOLOR 0x0400
#define SUGARFONT_DIAGNOSTICS 0x8000

#define SUGARFONT_PRECACHE_WITH_NEAREST 0x01
#define SUGARFONT_PRECACHE_WITH_EDGE 0x02
#define SUGARFONT_PRECACHE_WITH_DOWNFADE 0x04
#define SUGARFONT_PRECACHE_WITH_SPECIAL_S 0x08
#define SUGARFONT_PRECACHE_WITH_SPECIAL_EXC 0x10
#define SUGARFONT_PRECACHE_WITH_UNICODE 0x20
#define SUGARFONT_PRECACHE_WITH_DUMMY 0x40
#define SUGARFONT_PRECACHE_DEBUG_FLAG 0x80

#define SUGARFONT_DEFAULTSIZE 60.0f

#define SUGARFONT_PRECACHE_OFF 32
#define SUGARFONT_PRECACHE_MAX 96
#define SUGARFONT_PRECACHE_HI (SUGARFONT_PRECACHE_OFF + SUGARFONT_PRECACHE_MAX)

#define SUGARFONT_VAARG_BUF_SIZE 2048

///--[SLF Prebuild]
#define STARFONT_SLF_PREBUILD_NONE 0
#define STARFONT_SLF_PREBUILD_COMPILE 1
#define STARFONT_SLF_PREBUILD_READ 2

///===================================== Local Structures =========================================
///--[StarFontPrecacheData]
//--Used when precaching the data for a set of letters.
typedef struct
{
    //--OpenGL Rendering
    uint32_t mHandle;
    uint32_t mGLList;
    uint32_t mGLListItalic;
    uint32_t mGLListXMirror;
    uint32_t mGLListYMirror;

    //--Offset on bitmap
    float mLftOffset, mTopOffset;
    float mFlippedTopOffset;

    //--Fast-access copies of metrics
    float mWidth, mHeight;
    float mAdvanceX;
    float mHoriBearingX;
    float mHoriBearingY;

    //--Kernings
    float mKerningDistance[SUGARFONT_PRECACHE_MAX];
}StarFontPrecacheData;

///--[StarFontListMetrics]
//--Used when compiling font files, this stores the specific values of glLists when rasterizing glyphs.
typedef struct StarFontListMetrics
{
    float mLft;
    float mTop;
    float mRgt;
    float mBot;
    float mTrueWid;
    float mTrueHei;
    float mTexLft;
    float mTexTop;
    float mTexRgt;
    float mTexBot;
}StarFontListMetrics;

///--[StarFontGlyphRemap]
//--Remap. Maps a unicode value to a glyph index.
typedef struct
{
    int32_t mGlyphIndex;
    int32_t mUnicodeValue;
}StarFontGlyphRemap;

///--[BitmapFontCharacter]
//--When in Bitmap Font mode, stores the texel positions of a character on a texture.
typedef struct
{
    float mBmpLft, mBmpTop, mBmpRgt, mBmpBot;
    int mActualWid, mActualHei;
    int mPixelWid;
}BitmapFontCharacter;

///========================================== Classes =============================================
class StarFont : public RootObject
{
    private:
    //--System
    bool mIsReady;
    bool mOwnsFont;
    int mTotalGlyphs;
    char *mLocalName;

    //--Dummy Mode
    bool mIsDummyMode;

    //--Remaps
    int mTotalRemaps;
    StarFontGlyphRemap *mRemaps;

    //--Font storage
    #if defined _FONTTYPE_FREETYPE_
    StarInternalFont rFont;
    #endif

    //--Font precaching
    float mEndPadding;
    uint32_t mPrecachedHandle;
    float mTallestCharacter;
    StarFontPrecacheData *mPrecacheData;
    StarFontListMetrics *mListMetricData;
    float mPrecachedSizeX, mPrecachedSizeY;

    //--Precache Special Effects
    static float xDownfadeBegin;//StarFontSpecialEffects.cc
    static float xDownfadeEnd;//StarFontSpecialEffects.cc
    static int xBlackOutlineCutoff;//StarFontSpecialEffects.cc
    static int xOutlineSize;//StarFontSpecialEffects.cc
    static float xNextMonospace;//StarFontSpecialEffects.cc

    //--Offsets
    float mInternalSize;

    //--Kerning
    bool mNeedsKerning;
    float mKerningScaler;
    float mMonospacing;

    //--Bitmap Mode
    bool mOwnsBitmapFont;
    bool mIsBitmapMode;
    bool mIsBitmapFixedWidth;
    bool mIsBitmapFlipped;
    StarBitmap *rAlphabetBitmap;
    BitmapFontCharacter *mBitmapCharacters;

    //--File Output
    StarAutoBuffer *mOutputBuffer;

    protected:

    public:
    //--System
    StarFont();
    virtual ~StarFont();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    static int xFontSLFMode;
    static char *xFontSLFFilePath;
    static bool xScaleAffectsX;
    static bool xScaleAffectsY;
    #if defined _FONTTYPE_FREETYPE_
    static FT_Library xFTLibrary;
    #endif

    //--Property Queries
    bool IsReady();
    bool NeedsKerning();
    const char *GetName();
    float GetKerningBetween(int pLetterLft, int pLetterRgt);
    int GetGlyphFromRemap(int pUnicodeValue);
    int BinaryGlyphSearch(int pUnicodeValue, int pMin, int pMax);

    //--Manipulators
    void SetInternalName(const char *pName);
    void SetAsDummy();
    void SetKerningScaler(float pAmount);
    void SetKerning(int pLft, int pRgt, float pAmount);

    //--Core Methods
    void PushPropertyStack();
    void PopPropertyStack();

    ///--[FreeType]
    void ConstructWith(const char *pLoadPath, int pFontSize, uint8_t pFlags);
    int GetTextHeight();
    float GetPrecacheSizeX();
    float GetPrecacheSizeY();
    void Bind();

    ///--[Precaching]
    void ClearPrecache();
    void PrecacheData(uint8_t pFilterFlag);
    void BuildGlyphLists(float pWidest, float pTallest, uint8_t pFilterFlag);

    ///--[Processing]
    int GetTextWidth(const char *pString);
    int GetTextWidthDiagnostic(const char *pString);

    ///--[Special Effects]
    static void SetDownfade(float pStart, float pEnd);
    static void SetOutlineWidth(int pPixels);
    static void SetMonospacing(float pPixels);
    static void ApplyBlackOutline(int pXSize, int pYSize, uint32_t *pTextureData);

    ///--[Bitmap Mode]
    //private:
    void SetInternalBitmap(StarBitmap *pBitmap);
    void RunVariableWidthScript(const char *pScriptPath);
    bool IsBitmapMode();
    int GetBitmapTextWidth(const char *pString);
    int GetBitmapTextHeight();
    void SetBitmapCharacterSizes(int pIndex, int pLft, int pTop, int pRgt, int pBot, int pForceSize);
    void DrawTextBitmap(const char *pString);
    void DrawTextBitmap(float pX, float pY, const char *pString);
    void DrawTextBitmap(float pX, float pY, int pFlags, const char *pString);
    void DrawTextBitmap(float pX, float pY, int pFlags, float pSize, const char *pString);

    public:
    //--Update
    //--File I/O
    static void ActivateCompilation(const char *pPath);
    static void FinishCompilation();
    static void ActivateReadCompiledFonts(const char *pPath);
    void SendAutobufferToSLM();
    bool ReadFromSLM();

    //--Drawing
    void DrawText(const char *pText);
    void DrawText(float pX, float pY, const char *pText);
    void DrawText(float pX, float pY, int pFlags, const char *pText);
    void DrawTextColor(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText);
    void DrawText(float pX, float pY, int pFlags, float pSize, const char *pText);
    void DrawTextArgs(float pX, float pY, int pFlags, float pSize, const char *pText, ...);
    void DrawTextColorArgs(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText, ...);
    void DrawTextFixed(float pX, float pY, float pW, int pFlags, const char *pText);
    void DrawTextFixedArgs(float pX, float pY, float pW, int pFlags, const char *pText, ...);

    //--Single-Letter. Used for Major Dialogue cases.
    float DrawLetter(float pX, float pY, int pFlags, float pSize, int pLetter);
    float DrawLetter(float pX, float pY, int pFlags, float pSize, int pLetter, int pNextLetter);

    //--Drawing Text with Image Remaps
    void DrawTextImg(float pX, float pY, int pFlags, float pSize, const char *pText, int pRemapsTotal, char **pRemaps, char **pImgPaths);

    //--Pointer Routing
    //--Static Functions
    static StarFont *FetchSystemFont();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_StarFont_SetBitmapCharacter(lua_State *pLuaState);
int Hook_StarFont_SetKerning(lua_State *pLuaState);
int Hook_StarFont_BeginCompilation(lua_State *L);
int Hook_StarFont_FinishCompilation(lua_State *L);
int Hook_StarFont_OpenCompiledFile(lua_State *L);
