///====================================== SlittedEyeLevel =========================================
//--Level in the game Slitted Eye. Can refer to a nighttime defense sequence, or a daytime
//  investigation sequence. In either case, the world is pre-rendered images that are layered
//  onto one another. Scripts handle updates.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"

///===================================== Local Structures =========================================
///--[HissingHitbox]
//--Hissing hitbox. If the player hisses inside this hitbox while it is active, a script gets fired.
//  The structure contains the dimensions of the hitbox, the script to fire, and the calling code to
//  pass when firing it.
typedef struct HissingHitbox
{
    //--Members.
    bool mFlagForDeletion;
    TwoDimensionReal mHitbox;
    char *mScriptPath;
    float mFiringCode;

    //--Functions.
    void Initialize()
    {
        mFlagForDeletion = false;
        mHitbox.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
        mScriptPath = NULL;
        mFiringCode = 0.0f;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        HissingHitbox *rPtr = (HissingHitbox *)pPtr;
        free(rPtr->mScriptPath);
        free(rPtr);
    }
}HissingHitbox;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class SlittedEyeLevel : public RootLevel
{
    private:
    //--System
    bool mPrintedNotFunctionWarning;

    //--Player Statistics
    bool mDisableHiss;
    bool mDisableManipulate;
    bool mAllowGameOver;
    int mGameTime;
    float mStamina;
    float mNoise;

    //--Hissing
    bool mIsHissing;
    int mHissTimer;
    float mStaminaLossPerHiss;
    StarLinkedList *mHissHitboxes; //HissingHitbox *, master

    //--Mouse Handling
    int mMouseX;
    int mMouseY;

    //--Scripting
    char *mUpdateScript;
    char *mGameOverScript;

    //--World Rendering
    bool mDisallowScrolling;
    int mXOffset;
    int mImagesTotal;
    StarBitmap **mrImages;

    //--Fading
    bool mIsFading;
    bool mFadePersists;
    int mFadeTicksCur;
    int mFadeTicksMax;
    StarlightColor mFadeColBegin;
    StarlightColor mFadeColCurrent;
    StarlightColor mFadeColFinish;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            StarFont *rGUIDefaultFont;
        }GUI;
    }Images;

    protected:

    public:
    //--System
    SlittedEyeLevel();
    virtual ~SlittedEyeLevel();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    int GetPlayerTime();
    float GetPlayerStamina();
    float GetPlayerNoise();

    //--Manipulators
    void SetHissDisabled(bool pFlag);
    void SetManipulateDisabled(bool pFlag);
    void SetGameOverEnabled(bool pFlag);
    void SetGameTime(int pTime);
    void SetStamina(float pAmount);
    void SetNoise(float pAmount);
    void SetStaminaLossPerHiss(float pAmount);
    void SetDisallowScrolling(bool pFlag);
    void SetUpdatePath(const char *pPath);
    void SetGameOverPath(const char *pPath);
    void AllocateImages(int pTotal);
    void SetImage(int pSlot, const char *pDLPath);
    void RegisterHissHitbox(const char *pName, float pLft, float pTop, float pRgt, float pBot, const char *pPath, float pFiringCode);
    void FlagHissHitboxForDeletion(const char *pName);
    void ActivateFade(int pTicks, bool pPersists, StarlightColor pStartCol, StarlightColor pEndCol);
    void ActivateFadeUsingCurrent(int pTicks, bool pPersists, StarlightColor pEndCol);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    static SlittedEyeLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SEL_Create(lua_State *L);
int Hook_SEL_GetProperty(lua_State *L);
int Hook_SEL_SetProperty(lua_State *L);

