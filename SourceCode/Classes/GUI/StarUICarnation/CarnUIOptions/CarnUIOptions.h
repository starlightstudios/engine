///======================================= CarnUIOptions ==========================================
//--Options menu, allows the player to change options, save them, and revert to defaults. Options are
//  all stored as an CarnMenuOptionPack, which can handle ints, floats, and booleans.

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"

///===================================== Local Definitions ========================================
//--Modes
#define CMO_MODE_GAME 0
#define CMO_MODE_AUDIO 1
#define CMO_MODE_VIDEO 2
#define CMO_MODE_TOTAL 3

//--Editing Types
#define CMOP_BOOLEAN 0
#define CMOP_INTRANGE 1
#define CMOP_BAR 2

///===================================== Local Structures =========================================
///--[CarnMenuOptionPack]
//--Stores an option, both its value and also how the player edits it.
typedef struct CarnMenuOptionPack
{
    //--Members
    char *mDisplayName;
    int mEditType;
    StarLinkedList *mValueStrings;
    float mRangeLo;
    float mRangeHi;
    FlexVal mOldValue;
    FlexVal mCurValue;
    bool mRequiresRestart;
    StarLinkedList *mDescriptionList; //char *, master

    //--Functions
    void Initialize();
    void SetName(const char *pName);
    void SetI(int pValue);
    void SetF(float pValue);
    void SetB(bool pValue);
    void SetDescription(const char *pString, StarFont *pFont, float pMaxWidth);
    static void DeleteThis(void *pPtr);
}CarnMenuOptionPack;

///========================================== Classes =============================================
class CarnUIOptions : public CarnUICommon
{
    protected:
    ///--[Constants]
    //--Hitboxes.
    static const int cxHitbox_ModeGame       = 0;
    static const int cxHitbox_ModeAudio      = 1;
    static const int cxHitbox_ModeVideo      = 2;
    static const int cxHitbox_ButtonConfirm  = 3;
    static const int cxHitbox_ButtonCancel   = 4;
    static const int cxHitbox_ButtonDefaults = 5;
    static const int cxHitbox_OptionStartL   = 6;
    static const int cxHitbox_OptionStartR   = 7;
    static const int cxHitbox_OptionEndL     = 20;
    static const int cxHitbox_OptionEndR     = 21;

    ///--[System]
    bool mPrintRestartRequired;                     //If true, prints a message asking the player to restart for changes to take effect.
    bool mIsTitleMode;                              //If true, this is the Title Screen options. Do not respect or display UI tabs.

    ///--[Modes]
    int mActiveMode;
    char mModeNames[CMO_MODE_TOTAL][32];

    ///--[Options]
    CarnMenuOptionPack *rDraggedOption;
    CarnMenuOptionPack *rDescriptionPack;
    StarLinkedList *mMasterOptionList; //StarLinkedList *, master

    ///--[Resolutions]
    int mResolutionsTotal;                          //Resolution values need to be stored locally as they are based on the driver and thus
    char **mResolutionStrings;                      //differ on different machines. When the player changes resolution the string selected
                                                    //is parsed to get the x/y size.

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Type;
        StarFont *rFont_Button;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Button;
        StarBitmap *rOverlay_ArrowLft;
        StarBitmap *rOverlay_ArrowRgt;
        StarBitmap *rOverlay_MenuSelection;
        StarBitmap *rOverlay_ValueBar;
        StarBitmap *rOverlay_ValuePip;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIOptions();
    virtual ~CarnUIOptions();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    float ComputeBarPosition(TwoDimensionReal *pHitboxR);

    //--Manipulators
    virtual void TakeForeground();
    void SetTitleMode();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    ///--Handling
    void ConstructOptions();
    void RefreshOptions();
    void SaveOptions();
    void DefaultOptions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();
    void HandleMouseOver();
    void HandleLeftClick();
    void HandleRightClick();

    //--File I/O
    //--Drawing
    virtual void RenderForeground(float pVisAlpha);
    virtual void RenderPieces(float pAlpha);
    void RenderOption(CarnMenuOptionPack *pOptionPack, TwoDimensionReal *pHitboxL, TwoDimensionReal *pHitboxR);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


