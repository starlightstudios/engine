//--Base
#include "PeakFreaksLevel.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"
#include "ControlManager.h"

#define PFLEVEL_DIALOGUE_VIS_TICKS 15

///--[Tutorial Clear States]
//--If mTutorialClearState is zero, closing the dialogue box does nothing. Otherwise, it will activate
//  the next tutorial state according to these constants.
#define PFLEVEL_TUTORIAL_STATE_A 1
#define PFLEVEL_TUTORIAL_STATE_B 2
#define PFLEVEL_TUTORIAL_STATE_C 3
#define PFLEVEL_TUTORIAL_STATE_D 4
#define PFLEVEL_TUTORIAL_STATE_E 5
#define PFLEVEL_TUTORIAL_STATE_F 6
#define PFLEVEL_TUTORIAL_STATE_G 7
#define PFLEVEL_TUTORIAL_STATE_H 8
#define PFLEVEL_TUTORIAL_STATE_I 9
#define PFLEVEL_TUTORIAL_STATE_J 10
#define PFLEVEL_TUTORIAL_STATE_K 11
#define PFLEVEL_TUTORIAL_STATE_L 12
#define PFLEVEL_TUTORIAL_STATE_M 13
#define PFLEVEL_TUTORIAL_STATE_N 14
#define PFLEVEL_TUTORIAL_STATE_O 15
#define PFLEVEL_TUTORIAL_STATE_P 16
#define PFLEVEL_TUTORIAL_STATE_Q 17
#define PFLEVEL_TUTORIAL_STATE_R 18
#define PFLEVEL_TUTORIAL_STATE_S 19
#define PFLEVEL_TUTORIAL_STATE_T 20
#define PFLEVEL_TUTORIAL_STATE_U 21

///===================================== Property Queries =========================================
bool PeakFreaksLevel::IsDialogueBlockingUpdate()
{
    if(mRival) return false;
    if(mIsDialogueVisible || mDialogueVisTimer > 10) return true;

    return false;
}

///======================================= Manipulators ===========================================
void PeakFreaksLevel::AllocateDialogueLines(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mDialogueLinesTotal; i ++)
    {
        free(mDialogueLines[i]);
        free(mDialogueColors[i]);
    }
    free(mDialogueLines);
    free(mDialogueColors);

    //--Zero.
    mDialogueLinesTotal = 0;
    mDialogueLines = NULL;
    mDialogueColors = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mDialogueLinesTotal = pTotal;
    mDialogueLines  = (char **)starmemoryalloc(sizeof(char *) * mDialogueLinesTotal);
    mDialogueColors = (char **)starmemoryalloc(sizeof(char *) * mDialogueLinesTotal);
    for(int i = 0; i < mDialogueLinesTotal; i ++)
    {
        mDialogueLines[i] = NULL;
        mDialogueColors[i] = NULL;
    }
}
void PeakFreaksLevel::SetDialogueLine(int pSlot, const char *pDialogue)
{
    if(pSlot < 0 || pSlot >= mDialogueLinesTotal) return;
    if(mDialogueLines[pSlot]) free(mDialogueLines[pSlot]);
    mDialogueLines[pSlot] = InitializeString(pDialogue);
}
void PeakFreaksLevel::SetDialogueColors(int pSlot, const char *pColorString)
{
    if(pSlot < 0 || pSlot >= mDialogueLinesTotal) return;
    if(mDialogueColors[pSlot]) free(mDialogueColors[pSlot]);
    mDialogueColors[pSlot] = InitializeString(pColorString);
}
void PeakFreaksLevel::SetObjective(const char *pObjective)
{
    //--Flags.
    mCurrentGoalTimer = 0;
    ResetString(mDialogueCurrentGoal, pObjective);

    //--Recompute max.
    mCurrentGoalWid = Images.Data.rTutorialGoalFont->GetTextWidth(mDialogueCurrentGoal) * 2.0f;
    mCurrentGoalTimerMax = mCurrentGoalWid / 2.0f;
}

///--[ == Tutorial Section 1 ==]
void PeakFreaksLevel::ActivateTutorialA()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "So, you want to be a Peak Freak, eh? The best");
    SetDialogueColors(0, "000000000000000000000RRRRRRRRRR00000000000000");
    SetDialogueLine  (1, "climber in the biz!");
    SetDialogueColors(1, "0000000000000000000");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_C;
}
void PeakFreaksLevel::ActivateTutorialB()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(1);
    SetDialogueLine  (0, "Let me show you how to get started...");
    SetDialogueColors(0, "0000000000000000000000000000000000000");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_C;
}
void PeakFreaksLevel::ActivateTutorialC()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "Use the arrow keys to choose movement blocks,");
    SetDialogueColors(0, "00000000RRRRRRRRRR0000BBBBBBBBBBBBBBBBBBBBBB0");
    SetDialogueLine  (1, "Then press 'Z' to move.");
    SetDialogueColors(1, "00000RRRRRRRRR0000BBBB0");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_D;
}
void PeakFreaksLevel::ActivateTutorialD()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(1);
    SetDialogueLine  (0, "Try to move around in all four directions!");
    SetDialogueColors(0, "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");

    //--Objective.
    SetObjective("Use the arrow keys to choose movement blocks. Then press 'Z' to move!");

    //--Go to next block.
    mTutorialClearState = 0;

    //--Tutorial listener.
    mDirectionMoveTutorialActive = true;
    mDirectionTutorialUp = false;
    mDirectionTutorialRt = false;
    mDirectionTutorialDn = false;
    mDirectionTutorialLf = false;
}

///--[ == Tutorial Section 2 ==]
void PeakFreaksLevel::ActivateTutorialE()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(1);
    SetDialogueLine  (0, "You're getting the hang of it!");
    SetDialogueColors(0, "000000000000000000000000000000");

    //--Objective.
    SetObjective(" ");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_F;
}
void PeakFreaksLevel::ActivateTutorialF()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "Notice that if you move with adjacent matching");
    SetDialogueColors(0, "0000000000000000000BBBB000000BBBBBBBBBBBBBBBBB");
    SetDialogueLine  (1, "movement blocks you can make big jumps!");
    SetDialogueColors(1, "BBBBBBBBBBBBBBB00000000000000BBBBBBBBBB");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_G;
}
void PeakFreaksLevel::ActivateTutorialG()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "For each adjacent movement block you select,");
    SetDialogueColors(0, "00000000000000000000000000000000000000000000");
    SetDialogueLine  (1, "you'll jump one extra tile on the map.");
    SetDialogueColors(1, "00000000000000000000000000000000000000");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_H;
}
void PeakFreaksLevel::ActivateTutorialH()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "Try to make some jumps with 3 or more");
    SetDialogueColors(0, "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");
    SetDialogueLine  (1, "adjacent blocks.");
    SetDialogueColors(1, "GGGGGGGGGGGGGGGG");

    //--Objective.
    SetObjective("Make some BIG jumps by selecting 3 or more movement blocks at once! Then press 'Z' to move!");

    //--Go to next block.
    mTutorialClearState = 0;

    //--Tutorial listener.
    mBigMoveTutorialActive = true;
    mBigMoveCount = 0;
}

///--[ == Tutorial Section 3 ==]
void PeakFreaksLevel::ActivateTutorialI()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(1);
    SetDialogueLine  (0, "Nice moves!");
    SetDialogueColors(0, "00000000000");

    //--Objective.
    SetObjective(" ");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_J;
}
void PeakFreaksLevel::ActivateTutorialJ()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(3);
    SetDialogueLine  (0, "Sometimes there will be obstacles or hazards in");
    SetDialogueColors(0, "00000000000000000000000000000000000000000000000");
    SetDialogueLine  (1, "your way, so controlling how far you jump is");
    SetDialogueColors(1, "00000000000000000000000000000000000000000000");
    SetDialogueLine  (2, "important.");
    SetDialogueColors(2, "0000000000");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_K;
}
void PeakFreaksLevel::ActivateTutorialK()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(1);
    SetDialogueLine  (0, "But I'll bet you want to go FAST, too!");
    SetDialogueColors(0, "00000000000000000000000000000000000000");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_L;
}
void PeakFreaksLevel::ActivateTutorialL()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(3);
    SetDialogueLine  (0, "If you choose several adjacent movement blocks ");
    SetDialogueColors(0, "00000000000000BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
    SetDialogueLine  (1, "and  press 'X', you can crush them into ");
    SetDialogueColors(1, "00000RRRRRRRRR0000000000BBBBBBBBBBBBBBBB");
    SetDialogueLine  (2, "concentrated energy!");
    SetDialogueColors(2, "BBBBBBBBBBBBBBBBBBB0");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_M;
}
void PeakFreaksLevel::ActivateTutorialM()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "Every time you create energy, you'll pick up");
    SetDialogueColors(0, "0000000000000000000000000000000000000BBBBBBB");
    SetDialogueLine  (1, "some speed!");
    SetDialogueColors(1, "BBBBBBBBBB0");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_N;
}
void PeakFreaksLevel::ActivateTutorialN()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "Get some energy by crushing several groups of 3");
    SetDialogueColors(0, "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");
    SetDialogueLine  (1, "or more adjacent blocks.");
    SetDialogueColors(1, "GGGGGGGGGGGGGGGGGGGGGGGG");

    //--Objective.
    SetObjective("Crush tiles with 'X' to fill your speed bar.");

    //--Go to next block.
    mTutorialClearState = 0;

    //--Tutorial state.
    mFillTutorialActive = true;
    mFillCount = 0;
}

///--[ == Tutorial Section 4 == ]
void PeakFreaksLevel::ActivateTutorialO()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(1);
    SetDialogueLine  (0, "You're full of energy, I can tell!");
    SetDialogueColors(0, "0000000000000000000000000000000000");

    //--Objective.
    SetObjective(" ");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_P;
}
void PeakFreaksLevel::ActivateTutorialP()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "Your energy depletes over time, so remember to");
    SetDialogueColors(0, "00000BBBBBBBBBBBBBBBBBBBBBBBBB0000000000000000");
    SetDialogueLine  (0, "crush blocks regularly.");
    SetDialogueColors(0, "00000000000000000000000");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_Q;
}
void PeakFreaksLevel::ActivateTutorialQ()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "Now it's time to feel the wind in your hair! Make");
    SetDialogueColors(0, "000000000000000000000000000000000000000000000GGGG");
    SetDialogueLine  (1, "four jumps while you have energy to use.");
    SetDialogueColors(1, "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG.");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_R;
}
void PeakFreaksLevel::ActivateTutorialR()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(2);
    SetDialogueLine  (0, "If you run out of energy, just crush some more");
    SetDialogueColors(0, "00000000000000000000000000BBBBBBBBBBBBBBBBBBBB");
    SetDialogueLine  (1, "blocks.");
    SetDialogueColors(1, "BBBBBB0");

    //--Objective.
    SetObjective("Crush a group of 3 blocks, four times. Crush tiles with 'X'.");

    //--Go to next block.
    mTutorialClearState = 0;

    //--Tutorial state.
    mPowerJumpTutorialActive = true;
    mPowerJumpCount = 0;
}

///--[ == Tutorial Section 5 == ]
void PeakFreaksLevel::ActivateTutorialS()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(1);
    SetDialogueLine  (0, "You did it! Those are all the basics I can teach you.");
    SetDialogueColors(0, "00000000000000000000000000000000000000000000000000000");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_T;
}
void PeakFreaksLevel::ActivateTutorialT()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(3);
    SetDialogueLine  (0, "When you're in a race, you'll face lots of");
    SetDialogueColors(0, "000000000000000000000000000000000000000000");
    SetDialogueLine  (1, "challenges, but with these tricks I know");
    SetDialogueColors(1, "0000000000000000000000000000000000BBBBBB");
    SetDialogueLine  (2, "you can handle them all!");
    SetDialogueColors(2, "BBBBBBBBBBBBBBBBBBBBBBB0");

    //--Go to next block.
    mTutorialClearState = PFLEVEL_TUTORIAL_STATE_U;
}
void PeakFreaksLevel::ActivateTutorialU()
{
    //--Set lines.
    mIsDialogueVisible = true;
    AllocateDialogueLines(3);
    SetDialogueLine  (0, "Okay, you can keep on practicing until you feel");
    SetDialogueColors(0, "00000000000000000000000000000000000000000000000");
    SetDialogueLine  (1, "you're ready. The 'C' key will let you leave");
    SetDialogueColors(1, "00000000000000RRRRRRRRRRRBBBBBBBBBBBBBBBBBBB");
    SetDialogueLine  (2, "the tutorial. Good luck!");
    SetDialogueColors(2, "BBBBBBBBBBBB000000000000");

    //--Objective.
    SetObjective("Keep on practicing until you�fre ready to leave. 'Z' = Move. 'X' = Crush Tiles. 'C' pause menu�h.");

    //--Go to next block.
    mTutorialClearState = 0;
}

///--[ ==== Tutorial Codes ==== ]
void PeakFreaksLevel::ActivateTutorialByCode(int pCode)
{
    if(pCode == PFLEVEL_TUTORIAL_STATE_A) { ActivateTutorialA(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_B) { ActivateTutorialB(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_C) { ActivateTutorialC(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_D) { ActivateTutorialD(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_E) { ActivateTutorialE(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_F) { ActivateTutorialF(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_G) { ActivateTutorialG(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_H) { ActivateTutorialH(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_I) { ActivateTutorialI(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_J) { ActivateTutorialJ(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_K) { ActivateTutorialK(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_L) { ActivateTutorialL(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_M) { ActivateTutorialM(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_N) { ActivateTutorialN(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_O) { ActivateTutorialO(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_P) { ActivateTutorialP(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_Q) { ActivateTutorialQ(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_R) { ActivateTutorialR(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_S) { ActivateTutorialS(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_T) { ActivateTutorialT(); return; }
    if(pCode == PFLEVEL_TUTORIAL_STATE_U) { ActivateTutorialU(); return; }
}

///========================================== Update ==============================================
void PeakFreaksLevel::UpdateDialogue()
{
    ///--[Documentation]
    //--Handles the dialogue prompts, used during the tutorial. Runs a timer, handles player keypresses,
    //  and adds new dialogue lines as needed.
    //--If this is not the tutorial, stop immediately.
    if(mRival) return;

    //--Common timers. These always run regardless of mode.
    mDialogueFloatTimer ++;
    mCurrentGoalTimer ++;
    if(mCurrentGoalTimer >= mCurrentGoalTimerMax) mCurrentGoalTimer = 0;

    ///--[Hiding]
    if(!mIsDialogueVisible)
    {
        //--Decrement the timer.
        if(mDialogueVisTimer > 0) mDialogueVisTimer --;
        return;
    }

    ///--[Showing]
    if(mDialogueVisTimer < PFLEVEL_DIALOGUE_VIS_TICKS) mDialogueVisTimer ++;

    //--If the player pushes the activate key (Z by default) then end the tutorial and fire the provided
    //  code. If the code is zero, ends the tutorial.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("PF Activate"))
    {
        mIsDialogueVisible = false;
        ActivateTutorialByCode(mTutorialClearState);
    }
}

///========================================== Drawing =============================================
void PeakFreaksLevel::RenderDialogue()
{
    ///--[Documentation]
    //--Renders the on-screen dialogue used for the tutorial. Does nothing if not in the tutorial.
    if(mRival) return;

    ///--[Background]
    //--A copy of the sky background renders at half transparency over the screen.
    float cBGTransparency = EasingFunction::QuadraticInOut(mDialogueVisTimer, PFLEVEL_DIALOGUE_VIS_TICKS) * 0.50f;
    if(cBGTransparency > 0.0f)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cBGTransparency);
        Images.Data.rBackgroundSky->Draw();
        StarlightColor::ClearMixer();
    }

    ///--[Eileen the Alien]
    //--Always renders during the tutorial.
    float cEileenX = 1061.0f;
    float cEileenY =  173.0f;
    float cEileenVariance = 22.0f;
    float cEileenCycleTicks = 75.0f;
    float cEileenDownNotSpeaking = 45.0f;

    //--Compute Y offset.
    float cEileenPct = mDialogueFloatTimer / cEileenCycleTicks;
    float cDiaVisPct = 1.0f - (mDialogueVisTimer / (float)PFLEVEL_DIALOGUE_VIS_TICKS);
    float tEileenY = cEileenY + (sinf(cEileenPct * 3.1415926f) * cEileenVariance) + (cEileenDownNotSpeaking * cDiaVisPct);
    Images.Data.rTutorialAlien->Draw(cEileenX, tEileenY);

    //--If the dialogue visibility timer is zero, do nothing.
    if(mDialogueVisTimer == 0) return;

    ///--[Backing]

    ///--[Text Box]
    //--Slides in from above.
    float cTextSlidePct = 1.0f - EasingFunction::QuadraticInOut(mDialogueVisTimer, PFLEVEL_DIALOGUE_VIS_TICKS);
    float cTextSlideOffY = -768.0f * cTextSlidePct;
    glTranslatef(0.0f, cTextSlideOffY, 0.0f);

    //--Render the text box itself.
    Images.Data.rTutorialDialogue->Draw();

    //--Render the button prompt.
    Images.Data.rTutorialPrompt->Draw(1068.0f, 172.0f);

    //--Render the little hand. Uses the same timer as Eileen but offset a bit.
    float cHandX = 1069.0f;
    float cHandY =   82.0f;
    float cHandYVar = 10.0f;
    float tHandY = cHandY + (sinf((cEileenPct + 0.70f) * 3.1415926f) * cHandYVar);
    Images.Data.rTutorialHand->Draw(cHandX, tHandY);

    ///--[Text]
    //--Constants.
    float cTextStartX = 141.0f;
    float cTextStartY =  63.0f;
    float cTextHeight = Images.Data.rTutorialFont->GetTextHeight();

    //--Render.
    for(int i = 0; i < mDialogueLinesTotal; i ++)
    {
        //--Skip NULL lines.
        if(!mDialogueLines[i] || !mDialogueColors[i]) continue;

        //--Position.
        float tUseX = cTextStartX;
        float cUseY = cTextStartY + (cTextHeight * i);
        int tLen = (int)strlen(mDialogueLines[i]);
        for(int x = 0; x < tLen-1; x ++)
        {
            if(mDialogueColors[i][x] == 'R')
            {
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }
            else if(mDialogueColors[i][x] == 'G')
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
            }
            else if(mDialogueColors[i][x] == 'B')
            {
                StarlightColor::SetMixer(0.0f, 0.0f, 1.0f, 1.0f);
            }
            else
            {
                StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
            }

            tUseX = tUseX + Images.Data.rTutorialFont->DrawLetter(tUseX, cUseY, SUGARFONT_NOCOLOR, 1.0f, mDialogueLines[i][x], mDialogueLines[i][x+1]);
        }
        Images.Data.rTutorialFont->DrawLetter(tUseX, cUseY, SUGARFONT_NOCOLOR, 1.0f, mDialogueLines[i][tLen-1]);
    }

    ///--[Clean Up]
    glTranslatef(0.0f, -cTextSlideOffY, 0.0f);
    StarlightColor::ClearMixer();
}
void PeakFreaksLevel::RenderObjective()
{
    ///--[Documentation]
    //--During the tutorial, the objective scrolls across the bottom of the screen. Does nothing if not
    //  in tutorial mode.
    if(mRival) return;

    ///--[Backing]
    Images.Data.rTutorialGoal->Draw();

    ///--[Objective]
    //--Stencilled text, scrolls by over time.
    if(mDialogueCurrentGoal)
    {
        //--Constants.
        float cTextLft = 1200.0f;
        float cTextTop =  668.0f;

        //--Render the stencil.
        DisplayManager::ActivateMaskRender(3);
        glColorMask(false, false, false, false);
        Images.Data.rTutorialGoalMask->Draw();

        //--Activate mask rendering.
        DisplayManager::ActivateStencilRender(3);
        glColorMask(true, true, true, true);

        //--Offset.
        float cPct = EasingFunction::Linear(mCurrentGoalTimer, mCurrentGoalTimerMax);
        float cXOff = mCurrentGoalWid * cPct * -1.0f;

        //--Render.
        Images.Data.rTutorialGoalFont->DrawText(cTextLft + cXOff, cTextTop, 0, 1.0f, mDialogueCurrentGoal);
        if(cPct >= 0.40f)
        {
            float cLength = Images.Data.rTutorialGoalFont->GetTextWidth(mDialogueCurrentGoal);
            Images.Data.rTutorialGoalFont->DrawText(cTextLft + cXOff + cLength + 32.0f, cTextTop, 0, 1.0f, mDialogueCurrentGoal);
        }

        //--Clean up.
        DisplayManager::DeactivateStencilling();
    }
}
