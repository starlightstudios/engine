//--Base
#include "StarLoadInterrupt.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
StarLoadInterrupt::StarLoadInterrupt()
{
    ///--[StarLoadInterrupt]
    //--System
    mIsInitted = false;
    mIsAwaitingKeypress = false;

    //--Loading Bar
    mCurrent = 0;
    mCurrentMax = 1;

    //--Animation Data
    mLastResetTime = 0.0f;
    mFrame = 0;
    mFrameDenominator = 4;

    //--Timing
    mForceNextUpdate = true;
    mLastUpdateCall = 0.0f;

    //--Public Variables
    mIsSuppressed = false;
}
StarLoadInterrupt::~StarLoadInterrupt()
{
}
void StarLoadInterrupt::Construct()
{
    ///--[Documentation]
    //--Virtual function. Callers should load any local image data they need here, and then verify
}

///===================================== Property Queries =========================================
int StarLoadInterrupt::GetCallsSoFar()
{
    return mCurrent;
}
bool StarLoadInterrupt::IsAwaitingKeypress()
{
    return mIsAwaitingKeypress;
}

///======================================== Manipulators ==========================================
void StarLoadInterrupt::SetKeypressFlag(bool pFlag)
{
    mIsAwaitingKeypress = pFlag;
}
void StarLoadInterrupt::WaitForKeypress()
{
    StarLoadInterrupt *rInterrupt = StarLoadInterrupt::Fetch();
    if(!rInterrupt) return;
    rInterrupt->SetKeypressFlag(true);
}

///======================================== Core Methods ==========================================
void StarLoadInterrupt::Reset(int pMaxCount)
{
    ///--[Documentation]
    //--Resets the timers as required, meaning the logic will always run once the first time this
    //  call hits. Note that this does not re-parse the text file or reload any images.
    mForceNextUpdate = true;

    //--Reset variables.
    mLastResetTime = GetGameTime();
    mFrame = 0;
    mCurrent = 0;
    mCurrentMax = pMaxCount;

    //--Clamp. mCurrentMax can never be less than one.
    if(mCurrentMax < 1) mCurrentMax = 1;

    //--Store the current time for the loading animations.
    mLastUpdateCall = GetGameTime();
}
void StarLoadInterrupt::Finish()
{
    ///--[Documentation]
    //--Prints some debug data, if you need it.
    if(false) return;

    //--Call one last time.
    Call(true);
    mCurrent --;

    //--Debug prints.
    //fprintf(stderr, "[Load Interrupt]\n");
    //fprintf(stderr, " Completed loading action!\n");
    //fprintf(stderr, " Counter was %i/%i\n", mCurrent, mCurrentMax);
}
void StarLoadInterrupt::Interrupt()
{
    ///--[Documentation]
    //--Does the job of handling interrupt calls issues by other objects. This consists of locating
    //  the dynamic object and ordering it to handle the call.
    //--Because the Call() function is a virtual, the subtype of class is not needed.
    StarLoadInterrupt *rInterrupt = StarLoadInterrupt::Fetch();
    if(!rInterrupt) return;

    //--Call renderer.
    rInterrupt->Call(false);
}
void StarLoadInterrupt::DeallocateImages()
{
    ///--[Documentation]
    //--Virtual function, derived classes should deallocate any local images they may have, and also
    //  call this function in case (at some point) common images get added.
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
//--These functions perform the logic of the interrupt, in addition to the rendering needed.
bool StarLoadInterrupt::CommonLogicUpdate(bool pForceRender)
{
    ///--[Documentation]
    //--Most load interrupt call types run a common logic update. This function returns true if rendering
    //  should proceed, false otherwise.
    DebugManager::PushPrint(false, "[StarLoadInterrupt] Begin\n");
    if(mIsSuppressed || StarBitmap::xSuppressInterrupt)
    {
        DebugManager::PopPrint("[StarLoadInterrupt] Suppressed\n");
        return false;
    }

    //--Increment the counter. This tracks how many items we've loaded and is used for the progress
    //  bar. The denominator mCurrentMax is a constant.
    mCurrent ++;

    //--Setup.
    float tCurrentTime = GetGameTime();

    //--Get how long it's been since last we rendered anything. This clamps the StarLoadInterrupt at
    //  60 fps (1.0 / 60.0 == 0.01667)
    //--pForceRender will bypass this check.
    //fprintf(stderr, "Time since last tick = %f\n", tCurrentTime - mLastUpdateCall);
    if(tCurrentTime - mLastUpdateCall < 0.01667f && !pForceRender)
    {
        DebugManager::PopPrint("[StarLoadInterrupt] Too Soon\n");
        return false;
    }

    //--Store the time, increment the frame count.
    mLastUpdateCall = tCurrentTime;
    mFrame ++;
    DebugManager::PopPrint("[StarLoadInterrupt] Normal completion.\n");
    return true;
}
void StarLoadInterrupt::CommonRendererSetup()
{
    ///--[Documentation]
    //--Renderer setup used by most of the calls. The DisplayManager may not be set up yet, so
    //  we need to do the setup ourselves.
    DisplayManager::LetterboxOffsets();
    DisplayManager::StdOrtho();
    glDrawBuffer(GL_BACK);

    //--Clean the screen to dark grey.
    glClearDepth(1.0f);
    glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}
void StarLoadInterrupt::CommonRendererClean()
{
    ///--[Documentation]
    //--Renderer clean up used by most calls.
    DisplayManager::CleanOrtho();

    //--[Allegro Version]
    //--Data is sent to the graphics card by flipping the display.
    #if defined _ALLEGRO_PROJECT_
        al_flip_display();

    //--[SDL Version]
    //--Data is sent to the graphics card by order the context to dump to the window.
    #elif defined _SDL_PROJECT_
        SDL_GL_SwapWindow(DisplayManager::Fetch()->GetWindow());
    #endif
}
void StarLoadInterrupt::Call(bool pForceRender)
{
    ///--[Documentation]
    //--Virtual function, derived classes can override this with their rendering calls.
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
StarLoadInterrupt *StarLoadInterrupt::Fetch()
{
    if(!Global::Shared()->gLoadInterrupt) return NULL;
    return Global::Shared()->gLoadInterrupt;
}

///======================================== Lua Hooking ===========================================
void StarLoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_WaitForKeypress()
       Load Interrupt now waits until the player presses a key, blocking timer updates. */
    lua_register(pLuaState, "LI_WaitForKeypress", &Hook_LI_WaitForKeypress);

    /* LI_Interrupt()
       Runs the interrupt code. Used so Lua can emulate load interrupt cases. */
    lua_register(pLuaState, "LI_Interrupt", &Hook_LI_Interrupt);

    /* LI_Reset(iCount)
       Resets the StarLoadInterrupt's values and changes the max to the given amount. */
    lua_register(pLuaState, "LI_Reset", &Hook_LI_Reset);

    /* LI_Finish()
       Completes the StarLoadInterrupt sequence. */
    lua_register(pLuaState, "LI_Finish", &Hook_LI_Finish);

    /* LI_GetLastInterruptCount() (1 Integer)
       Returns how many interrupts have been called since the last reset. */
    lua_register(pLuaState, "LI_GetLastInterruptCount", &Hook_LI_GetLastInterruptCount);

    /* LI_SetSuppression(bFlag)
       Suppresses or unsuppresses the load interrupt. */
    lua_register(pLuaState, "LI_SetSuppression", &Hook_LI_SetSuppression);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LI_WaitForKeypress(lua_State *L)
{
    //LI_WaitForKeypress()
    StarLoadInterrupt::WaitForKeypress();
    return 0;
}
int Hook_LI_Interrupt(lua_State *L)
{
    //LI_Interrupt()
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
    return 0;
}
int Hook_LI_Reset(lua_State *L)
{
    //LI_Reset(iCount)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LI_Reset");
    StarLoadInterrupt::Fetch()->Reset(lua_tointeger(L, 1));
    return 0;
}
int Hook_LI_Finish(lua_State *L)
{
    //LI_Finish()
    StarLoadInterrupt::Fetch()->Finish();
    return 0;
}
int Hook_LI_GetLastInterruptCount(lua_State *L)
{
    //LI_GetLastInterruptCount()
    lua_pushinteger(L, StarLoadInterrupt::Fetch()->GetCallsSoFar());
    return 1;
}
int Hook_LI_SetSuppression(lua_State *L)
{
    //LI_SetSuppression(bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LI_SetSuppression");
    StarLoadInterrupt::Fetch()->mIsSuppressed = lua_toboolean(L, 1);
    return 0;
}
