//--Base
#include "AdventureDebug.h"

//--Classes
//--CoreClasses
#include "StarFileSystem.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
#define ADEBUGTRANS_DEBUG
#ifdef ADEBUGTRANS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== General =============================================
void AdventureDebug::BuildTranslationFileOnPath(const char *pPath, int pFlag, const char *pOutputPath)
{
    ///--[Documentation]
    //--Given a file path, scans every file on that path including subdirectories, and then checks the lua files/
    //--Depending on the flag passed in, we're either looking for
    //  Dialogue:
    //  Append("Crew:[VOICE|CrewmemberB] We're just going to stay here and leave the rest to you, milady!")
    //  Items:
    //  --LUA FILE HAS ITEM TRANSLATION--
    //  fnAdd("Item Name"
    //  fnAddWpn("Weapon Name"
    //--Any valid instances of these are added to the output file as a candidate for translation.
    if(!pPath || !pOutputPath) return;

    //--Debug.
    DebugPush(true, "Running translation builder on path %s\n", pPath);

    ///--[Output File]
    //--This file stores the generated output. It's a standard CSV file with a particular line format.
    fTranslationOutfile = fopen(pOutputPath, "w");

    //--Header.
    if(pFlag == TRANSLATE_RUN_DIALOGUE)
        fprintf(fTranslationOutfile, "Original,Machine,Improved,Prefix\n");
    else if(pFlag == TRANSLATE_RUN_ITEMSSKILLS)
        fprintf(fTranslationOutfile, "Original,Machine,Improved,\"Sample Formula: =GOOGLETRANSLATE(A3, \"\"en\"\", \"\"ja\"\")\"\n");

    ///--[Filesystem Scan]
    //--Scan the directory in question.
    StarFileSystem *tFileSystem = new StarFileSystem();
    tFileSystem->SetRecursiveFlag(true);
    tFileSystem->ScanDirectory(pPath);
    int tTotalEntries = tFileSystem->GetTotalEntries();

    //--Report results.
    #ifdef ADEBUGTRANS_DEBUG
    DebugPrint("Scanned directory. Found %i files. Printing file results.\n", tTotalEntries);
    for(int i = 0; i < tTotalEntries; i ++)
    {
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo) continue;
        DebugPrint("%s\n", rFileInfo->mPath);
    }
    #endif

    ///--[Iterate for Lua Files]
    //--Only files ending in .lua are checked for translation.
    for(int i = 0; i < tTotalEntries; i ++)
    {
        //--Make sure the entry is not invalid.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo) continue;

        //--Skip anything marked as a directory.
        if(rFileInfo->mIsDirectory) continue;

        //--Check if this is a lua file by checking the last three letters.
        int tPathLen = strlen(rFileInfo->mPath);
        if(tPathLen > 3)
        {
            if(rFileInfo->mPath[tPathLen-3] == 'l' && rFileInfo->mPath[tPathLen-2] == 'u' && rFileInfo->mPath[tPathLen-1] == 'a')
            {
                //--Debug.
                DebugPrint("File %s is a lua file.\n", rFileInfo->mPath);

                //--Call subfunction depending on the type.
                if(pFlag == TRANSLATE_RUN_DIALOGUE)
                    BuildDialogueTranslationForFile(rFileInfo->mPath);
                else if(pFlag == TRANSLATE_RUN_ITEMSSKILLS)
                    BuildItemSkillTranslationForFile(rFileInfo->mPath);
            }
        }
    }

    ///--[Finish Up]
    fclose(fTranslationOutfile);
    fTranslationOutfile = NULL;
    delete tFileSystem;
    DebugPop("Finished translation builder.\n");
}

///=========================================== Items ==============================================
void AdventureDebug::BuildItemSkillTranslationForFile(const char *pPath)
{
    ///--[Documentation]
    //--Given a file path pointing to a lua file, scans that file line-by-line looking for all sequences matching
    //  the pattern:
    //--LUA FILE HAS ITEM TRANSLATION--
    //--If this is found, then all instances of fnAdd( and fnAddWeapon( will be checked for a translatable string.
    if(!pPath) return;

    ///--[File Handling]
    //--Get and check the file.
    FILE *fInfile = fopen(pPath, "r");
    if(!fInfile)
    {
        fprintf(stderr, "File not found!\n");
        return;
    }

    //--Debug.
    DebugPrint("Scanning %s for appends.\n", pPath);

    //--FILEINFO setup. As soon as a hit is recorded, a FILEINFO block gets created, specifying which file
    //  the line is from. This is local to the Chapter C/ directory.
    int tFileStartFrom = 0;
    int tPathLen = strlen(pPath);
    for(int i = 0; i < tPathLen - 9; i ++)
    {
        if(!strncasecmp(&pPath[i], "Chapter C", 9))
        {
            tFileStartFrom = i + 9;
            break;
        }
    }

    ///--[Iteration]
    bool tIsFirstHit = true;
    char tInBuf[2048];
    while(!feof(fInfile))
    {
        //--Read a string. If it comes back NULL, then we're done.
        void *rCheckPtr = fgets(tInBuf, 2047, fInfile);
        if(!rCheckPtr) break;

        //--If this is the "first hit" then we're looking for --LUA FILE HAS ITEM TRANSLATION--
        //  Don't do any parsing if we haven't found that yet.
        if(tIsFirstHit)
        {
            //--Disable the flag and print the header to the outfile.
            if(!strncasecmp(tInBuf, "--LUA FILE HAS ITEM TRANSLATION--", 33))
            {
                tIsFirstHit = false;
                fprintf(fTranslationOutfile, "FILEINFO,%s\n", &pPath[tFileStartFrom]);
            }
        }
        //--After this point, we have found the required header. Check for fnAdd() and fnAddWpn()
        else
        {
            //--Begin parsing the string, looking for the fnAdd() and fnAddWpn() pattern. Note that we don't scan the full
            //  theoretical 2048 bytes, instead 2020, because the string cannot possibly contain any useful data
            //  to be translated if it somehow has hit the end of the string buffer size.
            for(int i = 0; i < 2020; i ++)
            {
                //--End of string.
                if(tInBuf[i] == '\0') break;

                //--Lowercase f, check ahead.
                if(tInBuf[i] == 'f')
                {
                    //--Item version.
                    if(!strncasecmp(&tInBuf[i], "fnAdd(", 6))
                    {
                        HandleItemSkillTranslationLine(tInBuf, i+6);
                        break;
                    }
                    //--Weapon version.
                    if(!strncasecmp(&tInBuf[i], "fnAddWpn(", 9))
                    {
                        HandleItemSkillTranslationLine(tInBuf, i+9);
                        break;
                    }
                    //--Description.
                    if(!strncasecmp(&tInBuf[i], "fnSetDescription(", 17))
                    {
                        HandleItemSkillDescriptionLine(tInBuf, i+17);
                        break;
                    }
                }
            }
        }
    }

    ///--[Finish Up]
    DebugPrint("Finished scan.\n");
    fclose(fInfile);
}
void AdventureDebug::HandleItemSkillTranslationLine(const char *pString, int pLeadChars)
{
    ///--[Documentation]
    //--Given a string, locates the part that needs to be translated and splits it off from the rest of the entry.
    //--pLeadChars describes the first letter of the string after the fnAdd( or fnAddWpn( where we need to start looking. It
    //  will usually be the first letter of the item or skill's name.
    if(!pString) return;

    //--Common.
    int tLen = strlen(pString);

    ///--[Iterate]
    //--Setup.
    bool tHasFoundFirstQuote = false;
    int tNameCursor = 0;
    char tNameBuf[2048];

    //--Iterate across the string.
    for(int i = pLeadChars; i < tLen; i ++)
    {
        //--If we haven't found the first quote, don't copy anything.
        if(!tHasFoundFirstQuote)
        {
            //--Found the first quote, set flag for copying.
            if(pString[i] == '"') tHasFoundFirstQuote = true;
            continue;
        }

        //--If we got this far, we should copy letters into the tNameBuf. However,
        //  when we hit the second quote, stop iterating.
        if(pString[i] == '"')
        {
            tNameBuf[tNameCursor] = '\0';
            break;
        }

        //--Copy letter.
        tNameBuf[tNameCursor] = pString[i];
        tNameCursor ++;
    }

    //--Print to outfile.
    fprintf(fTranslationOutfile, "\"%s\",,,\n", tNameBuf);
}
void AdventureDebug::HandleItemSkillDescriptionLine(const char *pString, int pLeadChars)
{
    ///--[Documentation]
    //--Similar to above, but looks for the fnSetDescription(sName, sDescription) format.
    if(!pString) return;

    //--Common.
    int tLen = strlen(pString);

    ///--[Iterate]
    //--Setup.
    bool tHasFoundFirstQuote = false;
    bool tHasFoundSecondQuote = false;
    bool tHasFoundThirdQuote = false;
    int tDescCursor = 0;
    char tDescBuf[2048];

    //--Iterate across the string.
    for(int i = pLeadChars; i < tLen; i ++)
    {
        //--If we haven't found the first quote, don't copy anything.
        if(!tHasFoundFirstQuote)
        {
            //--Found the first quote.
            if(pString[i] == '"') tHasFoundFirstQuote = true;
            continue;
        }
        //--If we haven't found the second quote, don't copy anything.
        if(!tHasFoundSecondQuote)
        {
            if(pString[i] == '"') tHasFoundSecondQuote = true;
            continue;
        }
        //--If we haven't found the third quote, don't copy anything.
        if(!tHasFoundThirdQuote)
        {
            if(pString[i] == '"') tHasFoundThirdQuote = true;
            continue;
        }

        //--If we got this far, we should copy letters into the tDescBuf. However,
        //  when we hit the fourth quote, stop iterating.
        if(pString[i] == '"')
        {
            tDescBuf[tDescCursor] = '\0';
            break;
        }

        //--Copy letter.
        tDescBuf[tDescCursor] = pString[i];
        tDescCursor ++;
    }

    //--Print to outfile.
    fprintf(fTranslationOutfile, "\"%s\",,,\n", tDescBuf);
}

///========================================= Dialogue =============================================
void AdventureDebug::BuildDialogueTranslationForFile(const char *pPath)
{
    ///--[Documentation]
    //--Given a file path pointing to a lua file, scans that file line-by-line looking for all sequences matching
    //  the pattern:
    //  Append("Crew:[VOICE|CrewmemberB] We're just going to stay here and leave the rest to you, milady!")
    //--When a sequence is found, the line is copied into a translation spreadsheet.
    if(!pPath) return;

    ///--[File Handling]
    //--Get and check the file.
    FILE *fInfile = fopen(pPath, "r");
    if(!fInfile)
    {
        fprintf(stderr, "File not found!\n");
        return;
    }

    //--Debug.
    DebugPrint("Scanning %s for appends.\n", pPath);

    //--FILEINFO setup. As soon as a hit is recorded, a FILEINFO block gets created, specifying which file
    //  the line is from. This is local to the Chapter C/ directory.
    int tFileStartFrom = 0;
    int tPathLen = strlen(pPath);
    for(int i = 0; i < tPathLen - 9; i ++)
    {
        if(!strncasecmp(&pPath[i], "Chapter C", 9))
        {
            tFileStartFrom = i + 9;
            break;
        }
    }

    ///--[Iteration]
    bool tIsFirstHit = true;
    char tInBuf[2048];
    while(!feof(fInfile))
    {
        //--Read a string. If it comes back NULL, then we're done.
        void *rCheckPtr = fgets(tInBuf, 2047, fInfile);
        if(!rCheckPtr) break;

        //--Begin parsing the string, looking for the Append(" ") pattern. Note that we don't scan the full
        //  theoretical 2048 bytes, instead 2020, because the string cannot possibly contain any useful data
        //  to be translated if it somehow has hit the end of the string buffer size.
        for(int i = 0; i < 2020; i ++)
        {
            //--End of string.
            if(tInBuf[i] == '\0') break;

            //--Capital A. Check ahead.
            if(tInBuf[i] == 'A')
            {
                if(!strncasecmp(&tInBuf[i], "Append(\"", 8))
                {
                    //--If this is the first hit, apply a header to the file.
                    if(tIsFirstHit)
                    {
                        tIsFirstHit = false;
                        fprintf(fTranslationOutfile, "FILEINFO %s,\n", &pPath[tFileStartFrom]);
                    }

                    //--Handle the translation.
                    HandleDialogueTranslationLine(tInBuf, i+8);
                }
            }
        }
    }

    ///--[Finish Up]
    DebugPrint("Finished scan.\n");
    fclose(fInfile);
}
int AdventureDebug::GetIndexOfLineStart(const char *pString, int pLeadChars)
{
    ///--[Documentation]
    //--Worker function for HandleTranslationLine(), given a string figures out what character index the prefix
    //  ends and the actual line begins. The prefix looks like this:
    //  Izana:[E|Neutral]       or Izana:[VOICE|Izana]
    //  The index of the character after the final space is returned. Returns -1 if something went wrong.
    //--pLeadChars optionally starts parsing later in the string. This is used when extracting the strings
    //  from lua files as opposed to receiving them during an AppendString() call.
    if(!pString) return -1;

    //--Setup.
    int tLen = (int)strlen(pString);
    bool tFoundColon = false;
    bool tHandledSpeaker = false;
    bool tHandledEmotion = false;

    ///--[Scan]
    //--Scan across the string.
    for(int i = pLeadChars; i < tLen; i ++)
    {
        //--If we haven't found the colon : yet, then we're still on the speaker's name. If the colon is
        //  found then that's the mandatory part of the prefix.
        if(!tFoundColon)
        {
            if(pString[i] == ':')
            {
                tFoundColon = true;
            }
            continue;
        }

        //--If we have found the colon, next we check for the [VOICE|Speaker] tag. This doesn't get translated.
        //  This tag may not exist in all cases.
        if(!tHandledSpeaker)
        {
            //--In any case, flag this off.
            tHandledSpeaker = true;

            //--The tag [VOICE needs to be right after the colon or it is ignored.
            if(!strncasecmp(&pString[i], "[VOICE|", 7))
            {
                //--Locate the matching ] to end the tag. There is also supposed to be a space right after the tag,
                //  so skip one character.
                for(int p = i; p < tLen; p ++)
                {
                    //--Check.
                    if(pString[p] == ']')
                    {
                        i = p;
                        break;
                    }
                }
                continue;
            }
        }

        //--Next, check if there's an emotion tag, used in major dialogues. This also does not get translated. If there
        //  is an emotion tag, there will not be a voice tag.
        if(!tHandledEmotion)
        {
            //--The tag [E| needs to be right after the colon or it is ignored.
            if(!strncasecmp(&pString[i], "[E|", 3))
            {
                //--Locate the matching ] to end the tag. There is also supposed to be a space right after the tag,
                //  so skip one character.
                for(int p = i; p < tLen; p ++)
                {
                    //--Check.
                    if(pString[p] == ']')
                    {
                        i = p+1;
                        break;
                    }
                }
            }

            //--In any case, flag this off.
            tHandledEmotion = true;
            continue;
        }

        //--At this point, we've passed the prefix.
        return i;
    }

    //--Something went wrong, and no prefix was found. Return -1.
    return -1;
}
void AdventureDebug::HandleDialogueTranslationLine(const char *pString, int pLeadChars)
{
    ///--[Documentation]
    //--Given a string, locates the part that needs to be translated and splits it off from the rest of the entry.
    //  Builds a table of where the needed tags will go, who the speaker is, and then outputs everything to the
    //  active spreadsheet.
    //--pLeadChars describes the first letter of the string after the Append(" where we need to start looking. It
    //  will usually be the first letter of the speaker's name.
    if(!pString) return;

    //--Common.
    int tLen = strlen(pString);

    //--Get the prefix length. If the function returns -1, that means there's no prefix and the whole line needs
    //  to be copied until the terminating quote.
    int tPrefixPos = GetIndexOfLineStart(pString, pLeadChars);

    ///--[No Prefix]
    //--No prefix was detected, copy the entire line into the translation.
    if(tPrefixPos == -1)
    {
        //--Locate the final quote.
        int tStringEnd = pLeadChars+1;
        for(int i = pLeadChars+1; i < tLen; i ++)
        {
            tStringEnd = i;
            if(pString[i] == '"')
            {
                tStringEnd --;
                break;
            }
        }

        //--Copy.
        char tOutBuf[2048];
        strncpy(tOutBuf, &pString[pLeadChars], (tStringEnd - pLeadChars) + 1);
        tOutBuf[(tStringEnd - pLeadChars) + 1] = '\0';

        //--Print.
        fprintf(fTranslationOutfile, "\"%s\",,,\n", tOutBuf);
    }
    ///--[Has Prefix]
    //--Store the prefix and the main line.
    else
    {
        //--Locate the final quote.
        int tStringEnd = tPrefixPos+1;
        for(int i = tPrefixPos+1; i < tLen; i ++)
        {
            tStringEnd = i;
            if(pString[i] == '"')
            {
                tStringEnd --;
                break;
            }
        }

        //--Get a prefix and main string buffer.
        char tPrefixBuf[2048];
        char tStringBuf[2048];
        strncpy(tPrefixBuf, &pString[pLeadChars], (tPrefixPos - pLeadChars));
        strncpy(tStringBuf, &pString[tPrefixPos], (tStringEnd - tPrefixPos) + 1);
        tPrefixBuf[(tPrefixPos - pLeadChars)] = '\0';
        tStringBuf[(tStringEnd - tPrefixPos) + 1] = '\0';

        //--Print.
        fprintf(fTranslationOutfile, "\"%s\",,,\"%s\"\n", tStringBuf, tPrefixBuf);
    }
}
void AdventureDebug::RunRepairOnDialogueSpreadsheet(const char *pPath, const char *pOutputPath)
{
    ///--[Documentation]
    //--Scans the provided .csv file, line by line, and makes sure that the entries in the first column
    //  that have [B][C] in them also have the second column have a [B][C] case at the end.
    //--The results are sent to pOutputPath.
    if(!pPath || !pOutputPath) return;

    //--Make sure both files are workable.
    FILE *fInfile = fopen(pPath, "r");
    FILE *fOutfile = fopen(pOutputPath, "w");
    if(!fInfile || !fOutfile)
    {
        fclose(fInfile);
        fclose(fOutfile);
        return;
    }

    ///--[Scan]
    //--Iterate across the file.
    char tInBuf[2048];
    while(!feof(fInfile))
    {
        //--Read a string. If it comes back NULL, then we're done.
        void *rCheckPtr = fgets(tInBuf, 2047, fInfile);
        if(!rCheckPtr) break;

        //--Run though the repair function.
        RunRepairOnLine(tInBuf, fOutfile);
    }

    ///--[Finish Up]
    fclose(fInfile);
    fclose(fOutfile);
}
void AdventureDebug::RunRepairOnLine(const char *pLine, FILE *fOutfile)
{
    ///--[Documentation]
    //--Checks the current line, letter by letter, looking for the [B][C] tag set right before the first
    //  comma. If found, outputs the line to fOutfile making sure to add [B][C] to the end of the second
    //  column in the csv file.
    if(!pLine || !fOutfile) return;

    //--Setup.
    int tCursor = 0;

    //--Get the first two lines.
    char *tLineA = StarTranslation::ParseLine(pLine, tCursor);
    char *tLineB = StarTranslation::ParseLine(pLine, tCursor);

    //--If either of them broke, stop.
    if(!tLineA || !tLineB)
    {
        free(tLineA);
        free(tLineB);
        return;
    }

    //--Check the end of the first line for the [B][C] tags. If the line is too short to even have the
    //  tags, then just print them both to the outfile.
    int tLineLenA = (int)strlen(tLineA);
    int tLineLenB = (int)strlen(tLineB);
    if(tLineLenA < 6)
    {
        fprintf(fOutfile, "\"%s\",\"%s\",,\n", tLineA, tLineB);
    }
    //--Otherwise, check for a match.
    else
    {
        //--Create a substring.
        const char *rSubstring = &tLineA[tLineLenA - 6];

        //--No match, nothing to be done.
        if(strcmp(rSubstring, "[B][C]"))
        {
            fprintf(fOutfile, "\"%s\",\"%s\",,\n", tLineA, tLineB);
        }
        //--Match. Scan the second string to see if its ending matches.
        else
        {
            //--Second line is too short, append the tag.
            if(tLineLenB < 6)
            {
                fprintf(fOutfile, "\"%s\",\"%s[B][C]\",,\n", tLineA, tLineB);
            }
            //--Check.
            else
            {
                //--Substring.
                const char *rSubstringB = &tLineB[tLineLenB - 6];

                //--No match, put the tags on the end.
                if(strcmp(rSubstringB, "[B][C]"))
                {
                    fprintf(fOutfile, "\"%s\",\"%s[B][C]\",,\n", tLineA, tLineB);
                }
                //--Match, has the tags. Print them normally.
                else
                {
                    fprintf(fOutfile, "\"%s\",\"%s\",,\n", tLineA, tLineB);
                }
            }
        }
    }

    //--Clean up.
    free(tLineA);
    free(tLineB);
}
