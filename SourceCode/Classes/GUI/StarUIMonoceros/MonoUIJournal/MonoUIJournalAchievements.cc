//--Base
#include "MonoUIJournal.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== System ==============================================
///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void MonoUIJournal::RecomputeJournalAchievementCursorPos()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Specific]
    tCursor = mAchievementCategoryCursor - mAchievementCategoryOffset;
    cNameLen = 100.0f;

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}
float MonoUIJournal::ComputeEntryHeight(AdvMenuJournalAchievementEntry *pEntry)
{
    ///--[Documentation]
    //--Given an achievement, computes how much space is needed to render it vertically. It will always have a minimum
    //  height to allow the icon, but description and progress requirements can extend it downwards.
    float cBlockHei = 86.0f;
    float cDescHei  = 16.0f;

    //--Error check:
    if(!pEntry) return cBlockHei;

    //--First, the height is set as the block basic height plus progress entries *2, as each progress entry has a name and progress bar.
    float cThisEntryHei = cBlockHei + (cDescHei * pEntry->mProgressEntriesTotal * 2.0f);

    //--If the description doesn't use all 3 alloted lines, reduce the height.
    if(pEntry->mDescriptionLines < 3) cThisEntryHei = cThisEntryHei - (cDescHei * (3 - pEntry->mDescriptionLines));

    //--If a progress entry does not have a name, or is a checkbox, decrease the height.
    for(int i = 0; i < pEntry->mProgressEntriesTotal; i ++)
    {
        if(pEntry->mProgressEntries[i].mDisplayName == NULL || pEntry->mProgressEntries[i].mDisplayAsCheckbox) cThisEntryHei = cThisEntryHei - cDescHei;
    }

    //--If the height comes back below the minimum, set that.
    if(cThisEntryHei < cBlockHei) cThisEntryHei = cBlockHei;

    //--Add an offset for the modified sizing.
    cThisEntryHei = cThisEntryHei + 8.0f;

    //--Finish up.
    return cThisEntryHei;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void MonoUIJournal::UpdateJournalAchievements()
{
    ///--[Documentation and Setup]
    //--Update handler. This is called after timers in the main update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    mAchievementScroll.Increment(EASING_CODE_QUADOUT);

    ///--[Arrow Keys]
    //--Setup.
    bool tRecompute = false;

    //--Up and Down handled by standard.
    tRecompute = AutoListUpDn(mAchievementCategoryCursor, mAchievementCategoryOffset, xAchievementCategories->GetListSize(), 3, ADVMENU_JOURNAL_ENTRIES_ON_LIST, true);
    if(tRecompute)
    {
        mAchievementEntryCursor = 0;
        mAchievementScroll.MoveTo(0.0f, 0);
    }

    ///--[Uplevel and Dnlevel]
    //--Uplevel and Dnlevel are handled by the standard and scroll the achievement entry offset, but are not actually
    //  the cursor and don't trigger a recompute.
    int tOldEntryCursor = mAchievementEntryCursor;
    AdvMenuJournalAchievementCategory *rActiveCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->GetElementBySlot(mAchievementCategoryCursor);
    if(rActiveCategory)
    {
        //--Get achievement list, check its size.
        StarLinkedList *rAchievementList = rActiveCategory->mAchievementList;
        int tUseSize = rAchievementList->GetListSize() - cxScrollbarAchPage + 1;
        if(tUseSize < 1) tUseSize = 0;

        //--Handle controls. Recompute is only checked for SFX.
        AutoListControl("UpLevel", "DnLevel", "Ctrl", mAchievementEntryCursor, mAchievementEntryOffset, tUseSize, 1, cxScrollbarAchPage, true);

        //--If the position changed, recompute the target offset.
        if(mAchievementEntryCursor != tOldEntryCursor)
        {
            //--Run across the entries.
            int i = 0;
            float tRunningTotal = 0.0f;
            AdvMenuJournalAchievementEntry *rEntry = (AdvMenuJournalAchievementEntry *)rAchievementList->PushIterator();
            while(rEntry)
            {
                //--If we hit the cursor, stop iterating.
                if(i >= mAchievementEntryCursor)
                {
                    rAchievementList->PopIterator();
                    break;
                }

                //--Add to the Y position.
                tRunningTotal = tRunningTotal + ComputeEntryHeight(rEntry);

                //--Next.
                i ++;
                rEntry = (AdvMenuJournalAchievementEntry *)rAchievementList->AutoIterate();
            }

            //--Set.
            mAchievementScroll.MoveTo(tRunningTotal, ADVMENU_JOURNAL_ACHIEVEMENT_SCROLL_TICKS);

            //--SFX.
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Finish Up]
    //--If this flag is true, recompute the highlight and play a sound effect.
    if(tRecompute)
    {
        //--Recalculate.
        RecomputeCursorPositions();

        //--SFX.
        //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Toggle Spoilers]
    //--Pressing run toggles spoilers on or off.
    if(rControlManager->IsFirstPress("Run"))
    {
        mHideSpoilers = !mHideSpoilers;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Close]
    //--Pressing cancel returns to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        HandleJournalModeChange();
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIJournal::RenderJournalAchievements()
{
    ///--[Documentation and Setup]
    //--Achievements mode. Gets its own submenu.
    if(!mImagesReady) return;

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, mVisibilityTimerMax);
    if(cAlpha <= 0.0f) return;

    ///--[Static Parts]
    //--Common universal parts.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Major Header.
    MonoImages.rOverlayTitleShadow->Draw();
    mMonoTitleColor.SetAsMixerAlpha(cAlpha);
    MonoImages.rFontDoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextAchievements);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Main Menu]
    //--Render the menu options. These always render regardless of mode.
    float cHelpLft = 45.0f;
    float cHelpTop = 73.0f;
    float cHelpRgt = 1321.0f;

    //--Help text indicating control options.
    mMainMenuLftString->DrawText(cHelpLft, cHelpTop, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontMainline);

    //--Unselected tabs.
    for(int i = 0; i < 6; i ++)
    {
        if(mCurrentMode != i)
        {
            MonoImages.rFrameTab->Draw(153.0f * i, 0.0f);
        }
        else
        {
            MonoImages.rFrameTabSelected->Draw(153.0f * i, 0.0f);
        }
    }

    //--Menu options. These render gold when selected.
    float cBestiaryLft    = 176.0f;
    float cProfilesLft    = 330.0f;
    float cQuestsLft      = 496.0f;
    float cLocationsLft   = 627.0f;
    float cCombatLft      = 780.0f;
    float cAchievementLft = 929.0f;
    float cMenuOptionTop  = 84.0f;
    RenderMenuOption(cBestiaryLft,    cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 0, mCurrentMode, xrStringData->str.mTextBestiary);
    RenderMenuOption(cProfilesLft,    cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 1, mCurrentMode, xrStringData->str.mTextProfiles);
    RenderMenuOption(cQuestsLft,      cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 2, mCurrentMode, xrStringData->str.mTextQuests);
    RenderMenuOption(cLocationsLft,   cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 3, mCurrentMode, xrStringData->str.mTextLocations);
    RenderMenuOption(cCombatLft,      cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 4, mCurrentMode, xrStringData->str.mTextCombatGlossary);
    RenderMenuOption(cAchievementLft, cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 5, mCurrentMode, xrStringData->str.mTextAchievements);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Backing.
    MonoImages.rFrameLft->Draw();
    MonoImages.rFrameRgt->Draw();

    ///--[Help Strings]
    //--Left-aligned Help.
    //mReturnString->           DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontMainline);
    mMove10String->           DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontMainline);
    mToggleSpoilerString->    DrawText(0.0f, cxMonoFontMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontMainline);
    mScrollAchievementString->DrawText(0.0f, cxMonoFontMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontMainline);

    //--Right-aligned Help.
    //mHideAchievementsString->DrawText(VIRTUAL_CANVAS_X, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, MonoImages.rFontMainline);

    ///--[Scrollbar]
    //--If there are over cxScrollbarListPage entries, we need a scrollbar.
    if(xAchievementCategories->GetListSize() > cxScrollbarListPage)
    {
        StandardRenderScrollbar(mAchievementCategoryOffset, cxScrollbarListPage, xAchievementCategories->GetListSize() - cxScrollbarListPage, true, 142.0f, 522.0f, MonoImages.rScrollbar_Scroller, MonoImages.rOverlayScrollbarAchievement);
    }

    ///--[Cat]
    MonoImages.rCatchievements->Draw();

    //--Right side. Right-aligned text.
    mMainMenuRgtString->DrawText(cHelpRgt, cHelpTop, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontMainline);

    ///--[Category Listing]
    //--Font
    StarFont *rUseFont = MonoImages.rFontEntries;

    //--Render Constants. The X/Y position is where the backing renders, the text has an offset.
    float cSelectionLft =  78.0f;
    float cSelectionTop = 168.0f;
    float cSelectionHei =  38.0f;

    //--Image Constants
    //float cScale = 0.41f;
    //float cScaleInv = 1.0f / cScale;

    //--Variables
    int tRenders = 0;
    int tOption = 0;
    float tYPos = cSelectionTop;

    ///--[Backings]
    //--A single cycle of backings renders, then the selection indicator, then the entries.
    //  This allows the selection to render under the text which looks better.
    AdvMenuJournalAchievementCategory *rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCategory)
    {
        //--If we haven't reached the start-at value, don't render anything.
        if(tOption < mAchievementCategoryOffset)
        {
            tOption ++;
            rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
            continue;
        }

        //--Backing.
        tRenders ++;
        MonoImages.rOverlayCell->Draw(cSelectionLft, tYPos);

        //--If we hit the end of the list, stop rendering:
        if(tRenders >= ADVMENU_JOURNAL_ENTRIES_ON_LIST)
        {
            xAchievementCategories->PopIterator();
            break;
        }

        //--Next.
        tOption ++;
        tYPos = tYPos + cSelectionHei;
        rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    ///--[Selection]
    MonoImages.rOverlaySelect->Draw(mHighlightPos.mXCur, mHighlightPos.mYCur);

    //--Iterate.
    tYPos = cSelectionTop;
    rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCategory)
    {
        //--If we haven't reached the start-at value, don't render anything.
        if(tOption < mAchievementCategoryOffset)
        {
            tOption ++;
            rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
            continue;
        }

        //--Render.
        tRenders ++;
        rUseFont->DrawText(cSelectionLft + 18.0f, tYPos, 0, 1.0f, rCategory->mDisplayName);

        //--If there's an icon associated with this entry, render it left-aligned.
        if(rCategory->rIcon)
        {
            //--Compute position.
            float cTopSpot = tYPos + (cSelectionHei - 14.0f) - (rCategory->rIcon->GetTrueHeight());
            float cLftSpot = cSelectionLft;

            //--Position, scale.
            glTranslatef(cLftSpot, cTopSpot, 0.0f);

            //--Render.
            rCategory->rIcon->Draw();

            //--Clean.
            glTranslatef(-cLftSpot, -cTopSpot, 0.0f);
        }

        //--If we hit the end of the list, stop rendering:
        if(tRenders >= ADVMENU_JOURNAL_ENTRIES_ON_LIST)
        {
            xAchievementCategories->PopIterator();
            break;
        }

        //--Next.
        tOption ++;
        tYPos = tYPos + cSelectionHei;
        rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    ///--[Achievement Listing]
    //--For each achievement in the selected category, render a backing, icon, description, etc. We render one before and
    //  one after the range requirement to handle scrolling.
    AdvMenuJournalAchievementCategory *rActiveCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->GetElementBySlot(mAchievementCategoryCursor);
    if(rActiveCategory)
    {
        ///--[Stencil Block]
        //--Rendering block.
        TwoDimensionReal tRenderBlock;
        tRenderBlock.SetWH(510.0f, 151.0f, 816.0f, 561.0f);

        //--Keeps things from rendering out of range.
        glEnable(GL_STENCIL_TEST);
        glColorMask(false, false, false, false);
        glDepthMask(false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        glStencilMask(0xFF);
        StarBitmap::DrawRectFill(tRenderBlock.mLft, tRenderBlock.mTop, tRenderBlock.mRgt, tRenderBlock.mBot, StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

        //--Switch stencilling to only draw where the rectangle was.
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilMask(0xFF);

        //--Setup.
        float tYPos = mAchievementScroll.mXCur * -1.0f;

        ///--[Rendering Loop]
        //--Iterate.
        AdvMenuJournalAchievementEntry *rEntry = (AdvMenuJournalAchievementEntry *)rActiveCategory->mAchievementList->PushIterator();
        while(rEntry)
        {
            ///--[Range Checks]
            //--Compute the entry size. It's cBlockHei plus one cDescHei for each requirement.
            float cThisEntryHei = ComputeEntryHeight(rEntry);

            //--If this entry would render off the top of the screen, skip it.
            if(tYPos + cThisEntryHei < 0.0f)
            {
                tYPos = tYPos + cThisEntryHei;
                rEntry = (AdvMenuJournalAchievementEntry *)rActiveCategory->mAchievementList->AutoIterate();
                continue;
            }

            //--If this entry would render off the bottom of the screen, stop rendering.
            if(tYPos > tRenderBlock.mBot - tRenderBlock.mTop)
            {
                rActiveCategory->mAchievementList->PopIterator();
                break;
            }

            ///--[Rendering]
            RenderJournalAchievement(tYPos, rEntry, cAlpha);

            ///--[Next]
            //--Advance by the size.
            tYPos = tYPos + cThisEntryHei;
            rEntry = (AdvMenuJournalAchievementEntry *)rActiveCategory->mAchievementList->AutoIterate();
        }

        //--Clean stencilling.
        glDisable(GL_STENCIL_TEST);

        //--Scrollbar if needed.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        if(rActiveCategory->mAchievementList->GetListSize() > cxScrollbarAchPage)
        {
            StarUIPiece::StandardRenderScrollbar(mAchievementEntryCursor, cxScrollbarAchPage, rActiveCategory->mAchievementList->GetListSize() - cxScrollbarAchPage, 168.0f, 524.0f, false, MonoImages.rScrollbar_Scroller, MonoImages.rOverlayScrollbarAchievement);
        }
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Highlight]
    //if(!mShowFullBestiaryImage)RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, Images.StandardUI.rHighlight);

    ///--[Overlay]
    //MonoImages.rStaticPartsAchievementsOver->Draw();

    ///--[Clean Up]
    StarlightColor::ClearMixer();
}
void MonoUIJournal::RenderJournalAchievement(float pYPos, AdvMenuJournalAchievementEntry *pEntry, float pAlpha)
{
    ///--[Documentation]
    //--Given an achievement, renders it at the given Y position. Because there's a lot of possibilities with achievements,
    //  this subroutine makes the above code more legible.
    if(!pEntry) return;

    //--Setup.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Constants.
    float cBlockHei =   85.0f;
    float cIconIndX =  522.0f;
    float cIconIndY =  166.0f;
    float cNameIndX =  588.0f;
    float cNameIndY =  154.0f;
    float cDescIndX =  598.0f;
    float cDescIndY =  191.0f;
    float cDescHei  =   16.0f;
    float cUnlIndX  = 1144.0f;
    float cUnlIndY  =  156.0f;
    float cTimeIndX = 1090.0f;
    float cTimeIndY =  174.0f;
    float cProgBufX = 15.0f;
    float cProgIndX = cDescIndX + cProgBufX;

    ///--[Static Parts]
    //--Compute entry height.
    float cThisEntryHei = ComputeEntryHeight(pEntry);

    //--Backing. Always at normal color. Stretches with special code to handle progress requirements.
    if(cThisEntryHei == cBlockHei)
    {
        MonoImages.rFrameAchievement->Draw(0.0f, pYPos);
    }
    //--Needs to be stretched.
    else
    {
        //--Compute scale.
        float cScale = (cThisEntryHei - 10.0f) / 80.0f;

        //--Position.
        float cXPos = MonoImages.rFrameAchievement->GetXOffset();
        float cYPos = pYPos + MonoImages.rFrameAchievement->GetYOffset();
        glTranslatef(cXPos, cYPos, 0.0f);
        glScalef(1.0f, cScale, 1.0f);

        //--Render.
        MonoImages.rFrameAchievement->Bind();
        MonoImages.rFrameAchievement->RenderAt();

        //--Unposition.
        glScalef(1.0f, 1.0f / cScale, 1.0f);
        glTranslatef(-cXPos, -cYPos, 0.0f);
    }

    ///--[Hidden When Not Unlocked]
    //--Hidden achievements show a lock icon and a standard display. When unlocked, this stops.
    if(pEntry->mHidesWhenNotUnlocked && !pEntry->mIsUnlocked)
    {
        //--Icon.
        AdvImages.rAchievementIconLocked->Draw(cIconIndX, cIconIndY + pYPos);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        //--Display Name.
        MonoImages.rFontAchieveHeader->DrawText(cNameIndX, cNameIndY + pYPos, 0, 1.0f, xrStringData->str.mTextLocked);

        //--Description.
        MonoImages.rFontAchieveMain->DrawText(cDescIndX, cDescIndY + pYPos, 0, 1.0f, xrStringData->str.mTextHiddenUntil);
        return;
    }

    ///--[Spoilered When Not Unlocked]
    //--These achievements can be revealed by pressing a key.
    if(pEntry->mIsSpoilered && !pEntry->mIsUnlocked && mHideSpoilers)
    {
        //--Icon.
        AdvImages.rAchievementIconLocked->Draw(cIconIndX, cIconIndY + pYPos);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        //--Display Name.
        MonoImages.rFontAchieveHeader->DrawText(cNameIndX, cNameIndY + pYPos, 0, 1.0f, xrStringData->str.mTextSpoiler);

        //--Description.
        MonoImages.rFontAchieveMain->DrawText(cDescIndX, cDescIndY + pYPos, 0, 1.0f, xrStringData->str.mTextSpoilerExpl);
        return;
    }

    //--Icon, if applicable. Renders darkened if the achievement is not unlocked.
    if(!pEntry->mIsUnlocked) StarlightColor::SetMixer(0.2f, 0.2f, 0.2f, pAlpha);
    if(pEntry->rImage) pEntry->rImage->Draw(cIconIndX, cIconIndY + pYPos);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Display Name.
    MonoImages.rFontAchieveHeader->DrawText(cNameIndX, cNameIndY + pYPos, 0, 1.0f, pEntry->mDisplayTitle);

    //--Description.
    for(int i = 0; i < pEntry->mDescriptionLines; i ++)
    {
        MonoImages.rFontAchieveMain->DrawText(cDescIndX, cDescIndY + pYPos + (cDescHei * i), 0, 1.0f, pEntry->mDescription[i]);
    }

    //--If unlocked, print when:
    if(pEntry->mIsUnlocked && pEntry->mUnlockTimestamp)
    {
        MonoImages.rFontAchieveMain->DrawText(cUnlIndX,  cUnlIndY  + pYPos, 0, 1.0f, xrStringData->str.mTextUnlocked);
        MonoImages.rFontAchieveMain->DrawText(cTimeIndX, cTimeIndY + pYPos, 0, 1.0f, pEntry->mUnlockTimestamp);
    }

    ///--[Requirements]
    //--Renders additional information below the description about progress towards achievement. Most achievements
    //  do not have these.
    float cProgressBarWid = 250.0f;
    float cProgressBarHei = cDescHei;
    StarlightColor cProgBarFill = StarlightColor::MapRGBAF(0.6f, 0.8f, 0.3f, pAlpha);
    StarlightColor cProgBarBack = StarlightColor::MapRGBAF(0.3f, 0.3f, 0.3f, pAlpha);
    StarlightColor cProgBarFrme = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha);

    //--Render position.
    float tYRender = cDescIndY + pYPos + (cDescHei * pEntry->mDescriptionLines);

    //--Iterate.
    for(int i = 0; i < pEntry->mProgressEntriesTotal; i ++)
    {
        ///--[Basics]
        //--Compute Y position.
        float tProgBarX = cProgIndX;

        ///--[Checkbox Case]
        //--If this entry is a checkbox, it does not render a progress bar. Instead it renders a cute little X or empty box.
        if(pEntry->mProgressEntries[i].mDisplayAsCheckbox)
        {
            //--Checkbox is on:
            if(pEntry->mProgressEntries[i].mValueCur >= pEntry->mProgressEntries[i].mValueMax)
            {
                AdvImages.rAchievementCheckYes->Draw(tProgBarX, tYRender + 10.0f);
            }
            //--Checkbox is off:
            else
            {
                AdvImages.rAchievementCheckNo->Draw(tProgBarX, tYRender + 10.0f);
            }

            //--For checkboxes, the name is printed to the right.
            if(pEntry->mProgressEntries[i].mDisplayName)
            {
                MonoImages.rFontAchieveMain->DrawText(tProgBarX + 16.0f, tYRender+1.0f, 0, 1.0f, pEntry->mProgressEntries[i].mDisplayName);
            }

            //--Advance the Y cursor.
            tYRender = tYRender + cDescHei;
        }
        //--If displaying as a number:
        else
        {
            //--If this has a name, render it above to the progress bar. This also moves the rendering cursor down.
            if(pEntry->mProgressEntries[i].mDisplayName)
            {
                MonoImages.rFontAchieveMain->DrawText(cProgIndX, tYRender, 0, 1.0f, pEntry->mProgressEntries[i].mDisplayName);
                tYRender = tYRender + cDescHei;
            }
            else
            {
                tProgBarX = cProgIndX - cProgBufX;
            }

            //--Progress Bar. Has a frame.
            StarBitmap::DrawRectFill(tProgBarX,      tYRender+2.0f, tProgBarX + cProgressBarWid,      tYRender + cProgressBarHei,      cProgBarFrme);
            StarBitmap::DrawRectFill(tProgBarX+1.0f, tYRender+3.0f, tProgBarX + cProgressBarWid-1.0f, tYRender + cProgressBarHei-1.0f, cProgBarBack);

            //--Get the progress percentage. If zero, don't render a fill.
            float tProgressPct = pEntry->mProgressEntries[i].mValueCur / pEntry->mProgressEntries[i].mValueMax;
            if(tProgressPct > 1.0f) tProgressPct = 1.0f;
            if(tProgressPct > 0)
            {
                StarBitmap::DrawRectFill(tProgBarX+1, tYRender + 3.0f, tProgBarX + ((cProgressBarWid-2.0f) * tProgressPct), tYRender + cProgressBarHei - 1.0f, cProgBarFill);
            }

            //--Progress values. Integer version.
            if(pEntry->mProgressEntries[i].mDisplayAsInteger)
            {
                MonoImages.rFontAchieveMain->DrawTextArgs(tProgBarX + cProgressBarWid + 3.0f, tYRender - 1.0f, 0, 1.0f, "%i / %i", (int)pEntry->mProgressEntries[i].mValueCur, (int)pEntry->mProgressEntries[i].mValueMax);
            }
            //--Floating point version.
            else
            {
                MonoImages.rFontAchieveMain->DrawTextArgs(tProgBarX + cProgressBarWid + 3.0f, tYRender - 1.0f, 0, 1.0f, "%.1f / %.1f", pEntry->mProgressEntries[i].mValueCur, pEntry->mProgressEntries[i].mValueMax);
            }

            //--Advance the Y cursor.
            tYRender = tYRender + cDescHei;
        }
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
