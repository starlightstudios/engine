//--Base
#include "CarnUICommon.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///--[ ====================================== Lua Queries ======================================= ]
//--Because class activity is handled mostly via lua and not the C++ state, it is necessary to have
//  a centralized way of querying those. This is done via a lua script which takes in strings and
//  populates DataLibrary variables with the results.

///--[ ====================================== System Calls ====================================== ]
int CarnUICommon::CallQueryScript(const char *pSwitchType, int pArgs, ...)
{
    ///--[Documentation]
    //--Given a switch type and more optional string arguments, calls the lua class query script. The script
    //  should populate results into the datalibrary to be received by the caller.
    //--It is possible for no additional arguments to be required, but a switch type is always required.
    //--Returns how many replies the script produced. Can legally be zero.
    if(!pSwitchType)
    {
        fprintf(stderr, "CarnUICommon:CallQueryScript() - Warning. Switch type was NULL.\n");
        return 0;
    }

    ///--[Get Call Path]
    //--The call path is stored in the DataLibrary. If it doesn't exist, print a warning.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rCallPathVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Carnation|ReplyData/System/sCallScript");
    if(!rCallPathVar || !rCallPathVar->mAlpha)
    {
        fprintf(stderr, "CarnUICommon:CallQueryScript() - Warning. Call script in 'Root/Variables/Carnation|ReplyData/System/sCallScript' is null or does not exist.\n");
        return 0;
    }

    ///--[No-Arguments Case]
    //--Argument count is zero, call script with just the switch type.
    if(pArgs < 1)
    {
        LuaManager::Fetch()->ExecuteLuaFile(rCallPathVar->mAlpha, 1, "S", pSwitchType);
    }
    ///--[Assemble Args, Call]
    //--Append arguments to the Lua Manager then call.
    else
    {
        //--Setup.
        va_list tArgList;
        va_start(tArgList, pArgs);

        //--Lua manager sets argument list size. Note that it's provided arguments +1 since the switch type counts.
        LuaManager *rLuaManager = LuaManager::Fetch();
        rLuaManager->SetArgumentListSize(pArgs+1);

        //--First argument is the switch type.
        rLuaManager->AddArgument(pSwitchType);

        //--For each additional argument:
        for(int i = 0; i < pArgs; i ++)
        {
            const char *rString = va_arg(tArgList, const char *);
            rLuaManager->AddArgument(rString);
        }

        //--Finish.
        va_end(tArgList);

        //--Call the script.
        LuaManager::Fetch()->ExecuteLuaFileBypass(rCallPathVar->mAlpha);
    }

    ///--[Results]
    //--Retrieve this from the DataLibrary and check the pointer.
    SysVar *rReturnsVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Carnation|ReplyData/Data/iTotalReplies");
    if(!rReturnsVar)
    {
        fprintf(stderr, "CarnUICommon:CallQueryScript() - Warning. Return variable was NULL, could not check return count.\n");
        return 0;
    }

    //--Return the numeric value.
    return (int)rReturnsVar->mNumeric;
}
bool CarnUICommon::CheckQueryReplies(const char *pCallerName, int pNeededReplies, int pReceivedReplies)
{
    ///--[Documentation]
    //--Checks if the received replies was at least equal to the needed replies. If not, returns false and prints a warning.
    //  If the replies were enough, returns true.
    if(!pCallerName) return true;
    if(pReceivedReplies >= pNeededReplies) return true;

    //--Error case. Print a warning.
    fprintf(stderr, "%s - Warning. Needs %i replies, got %i replies. Failing.\n", pCallerName, pNeededReplies, pReceivedReplies);
    return false;
}
int CarnUICommon::GetIntSafe(const char *pVariablePath)
{
    ///--[Documentation]
    //--Checks if the given variable exists. If it does, returns its numeric value, if not returns 0 on error.
    if(!pVariablePath) return 0;

    //--Check the variable.
    SysVar *rStringReply = (SysVar *)DataLibrary::Fetch()->GetEntry(pVariablePath);
    if(!rStringReply) return 0;

    //--Valid.
    return (int)rStringReply->mNumeric;
}
const char *CarnUICommon::GetStringSafe(const char *pVariablePath)
{
    ///--[Documentation]
    //--Checks if the given variable exists. If it does, returns its alpha string, if not returns "Null" on error.
    if(!pVariablePath) return "Null";

    //--Check the variable.
    SysVar *rStringReply = (SysVar *)DataLibrary::Fetch()->GetEntry(pVariablePath);
    if(!rStringReply || !rStringReply->mAlpha) return "Null";

    //--Valid.
    return rStringReply->mAlpha;
}

///--[ =================================== C++ Query Handlers==================================== ]
void CarnUICommon::QueryClassDataInto(int pIndex, const char *pCharacterName, CarnationUIClass &sCallStructure)
{
    ///--[Documentation]
    //--Given a character name, queries the class data in the given index into the given structure.
    if(pIndex < 0 || !pCharacterName) return;

    //--Run query to get all class info into a specific format.
    char tIndexBuf[32];
    sprintf(tIndexBuf, "%i", pIndex);
    int tReturns = CallQueryScript("Class|Class Info In Slot", 2, pCharacterName, tIndexBuf);
    if(!CarnUICommon::CheckQueryReplies("CarnUICommon:QueryClassDataInto()", 4, tReturns)) return;

    //--Class name.
    const char *rClassName = GetStringSafe("Root/Variables/Carnation|ReplyData/Data/zStringReply00");
    strcpy(sCallStructure.mName, rClassName);

    //--XP.
    sCallStructure.mClassLevel  = GetIntSafe("Root/Variables/Carnation|ReplyData/Data/zStringReply01");
    sCallStructure.mClassXP     = GetIntSafe("Root/Variables/Carnation|ReplyData/Data/zStringReply02");
    sCallStructure.mClassXPNext = GetIntSafe("Root/Variables/Carnation|ReplyData/Data/zStringReply03");
}
const char *CarnUICommon::QueryClassNameFor(int pIndex, const char *pCharacterName)
{
    ///--[Documentation]
    //--Given a character name, resolves the class names. At least 1 class name will be populated,
    //  and the class name at the given index is returned.
    if(pIndex < 0 || !pCharacterName) return NULL;

    //--Call handler.
    int tReturns = CallQueryScript("Class|Active Classes", 1, pCharacterName);
    if(!CheckQueryReplies("CarnUICommon:QueryClassNameFor()", pIndex+1, tReturns)) return NULL;

    //--Resolve the path that the result should be placed at.
    char tBuf[256];
    sprintf(tBuf, "Root/Variables/Carnation|ReplyData/Data/sStringReply%02i", pIndex); //String
    fprintf(stderr, "Check %s\n", tBuf);

    //--Check if the variable exists.
    SysVar *rStringReply = (SysVar *)DataLibrary::Fetch()->GetEntry(tBuf);
    if(!rStringReply) return NULL;

    //--Return.
    return rStringReply->mAlpha;
}
int CarnUICommon::QueryClassXPFor(int pIndex, const char *pCharacterName)
{
    ///--[Documentation]
    //--Given a character name, resolves the class XP values. At least 1 should be populated.
    if(pIndex < 0 || !pCharacterName) return 0;

    //--Call handler.
    int tReturns = CallQueryScript("Class|Active Class XP", 1, pCharacterName);
    if(!CheckQueryReplies("CarnUICommon:QueryClassXPFor()", pIndex+1, tReturns)) return 0;

    //--Resolve the path that the result should be placed at.
    char tBuf[256];
    sprintf(tBuf, "Root/Variables/Carnation|ReplyData/Data/iStringReply%02i", pIndex); //Integer

    //--Check if the variable exists.
    SysVar *rStringReply = (SysVar *)DataLibrary::Fetch()->GetEntry(tBuf);
    if(!rStringReply) return 0;

    //--Return.
    return (int)rStringReply->mNumeric;
}
