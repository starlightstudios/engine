//--Base
#include "DisplayManager.h"

//--Classes
//--Definitions
#include "GlDfn.h"

//--Generics
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

void *DisplayManager::WrangleSubroutine(const char *pFuncName, void *pAltPtr)
{
    //--Worker Function
    if(!pFuncName) return NULL;

    //--Setup
    void *rFuncPtr;

    //--[Allegro Version]
    #if defined _ALLEGRO_PROJECT_
    {
        rFuncPtr = al_get_opengl_proc_address(pFuncName);
    }
    //--[SDL Version]
    #elif defined _SDL_PROJECT_
    {
        rFuncPtr = SDL_GL_GetProcAddress(pFuncName);
    }
    #endif

    //--Error check:
    if(!rFuncPtr)
    {
        DebugManager::ForcePrint("Error wrangling %s %p, using dummy pointer.\n", pFuncName, rFuncPtr);
        return pAltPtr;
    }

    //--Normal case. Return the found pointer.
    DebugManager::Print("%s %p\n", pFuncName, rFuncPtr);
    return rFuncPtr;
}
void DisplayManager::WrangleExec()
{
    //--Wrangles all extensions not provided under normal GL functionality.
    bool xFlag = true;
    DebugManager::PushPrint(xFlag, "[GL Wrangling] Begin\n");

    //--A
    sglActiveTexture = (GLACTIVETEXTURE_FUNC)WrangleSubroutine("glActiveTexture", (void *)&dsglActiveTexture);
    sglAttachShader = (GLATTACHSHADER_FUNC)WrangleSubroutine("glAttachShader", (void *)&dsglAttachShader);

    //--B
    sglBindBuffer = (GLBINDBUFFER_FUNC)WrangleSubroutine("glBindBuffer", (void *)&dsglBindBuffer);
    sglBindFramebuffer = (GLBINDFRAMEBUFFER_FUNC)WrangleSubroutine("glBindFramebuffer", (void *)&dsglBindFramebuffer);
    sglBufferData = (GLBUFFERDATA_FUNC)WrangleSubroutine("glBufferData", (void *)&dsglBufferData);
    sglBlendFuncSeparate = (GLBLENDFUNCSEPARATE_FUNC)WrangleSubroutine("glBlendFuncSeparate", (void *)&dsglBlendFuncSeparate);
    sglBindVertexArray = (GLBINDVERTEXARRAY_FUNC)WrangleSubroutine("glBindVertexArray", (void *)&dsglBindVertexArray);
    sglBlendEquation = (GLBLENDEQUATION_FUNC)WrangleSubroutine("glBlendEquation", (void *)&dsglBlendEquation);

    //--C
    sglCheckFramebufferStatus = (GLCHECKFRAMEBUFFERSTATUS_FUNC)WrangleSubroutine("glCheckFramebufferStatus", (void *)&dsglCheckFramebufferStatus);
    sglClientActiveTexture = (GLCLIENTACTIVETEXTURE_FUNC)WrangleSubroutine("glClientActiveTexture", (void *)&dsglClientActiveTexture);
    sglCompileShader = (GLCOMPILESHADER_FUNC)WrangleSubroutine("glCompileShader", (void *)&dsglCompileShader);
    sglCompressedTexImage2D = (GLCOMPRESSEDTEX2D_FUNC)WrangleSubroutine("glCompressedTexImage2D", (void *)&dsglCompressedTexImage2D);
    sglCreateProgram = (GLCREATEPROGRAM_FUNC)WrangleSubroutine("glCreateProgram", (void *)&dsglCreateProgram);
    sglCreateShader = (GLCREATESHADER_FUNC)WrangleSubroutine("glCreateShader", (void *)&dsglCreateShader);

    //--D
    sglDeleteBuffers = (GLDELETEBUFFERS_FUNC)WrangleSubroutine("glDeleteBuffers", (void *)&dsglDeleteBuffers);
    sglDeleteProgram = (GLDELETEPROGRAM_FUNC)WrangleSubroutine("glDeleteProgram", (void *)&dsglDeleteProgram);
    sglDeleteShader = (GLDELETESHADER_FUNC)WrangleSubroutine("glDeleteShader", (void *)&dsglDeleteShader);
    sglDeleteVertexArrays = (GLDELETEVERTEXARRAYS_FUNC)WrangleSubroutine("glDeleteVertexArrays", (void *)&dsglDeleteVertexArrays);
    sglDeleteFramebuffers = (GLDELETEFRAMEBUFFERS_FUNC)WrangleSubroutine("glDeleteFramebuffers", (void *)&dsglDeleteFramebuffers);
    sglDrawBuffers = (GLDRAWBUFFERS_FUNC)WrangleSubroutine("glDrawBuffers", (void *)&dsglDrawBuffers);

    //--E
    sglEnableVertexAttribArray = (GLENABLEVERTEXATTRIBARRAY_FUNC)WrangleSubroutine("glEnableVertexAttribArray", (void *)&dsglEnableVertexAttribArray);

    //--F
    sglFramebufferTexture = (GLFRAMEBUFFERTEXTURE_FUNC)WrangleSubroutine("glFramebufferTexture", (void *)&dsglFramebufferTexture);

    //--G
    sglGenerateMipmap = (GLGENERATEMIPMAP_FUNC)WrangleSubroutine("glGenerateMipmap", (void *)&dsglGenerateMipmap);
    sglGenBuffers = (GLGENBUFFERS_FUNC)WrangleSubroutine("glGenBuffers", (void *)&dsglGenBuffers);
    sglGenFramebuffers = (GLGENFRAMEBUFFERS_FUNC)WrangleSubroutine("glGenFramebuffers", (void *)&dsglGenFramebuffers);
    sglGenVertexArrays = (GLGENVERTEXARRAYS_FUNC)WrangleSubroutine("glGenVertexArrays", (void *)&dsglGenVertexArrays);
    sglGetShaderiv = (GLGETSHADERIV_FUNC)WrangleSubroutine("glGetShaderiv", (void *)&dsglGetShaderiv);
    sglGetShaderInfoLog = (GLGETSHADERINFOLOG_FUNC)WrangleSubroutine("glGetShaderInfoLog", (void *)&dsglGetShaderInfoLog);
    sglGetProgramiv = (GLGETSHADERIV_FUNC)WrangleSubroutine("glGetProgramiv", (void *)&dsglGetProgramiv);
    sglGetProgramInfoLog = (GLGETSHADERINFOLOG_FUNC)WrangleSubroutine("glGetProgramInfoLog", (void *)&dsglGetProgramInfoLog);
    sglGetUniformLocation = (GLGETUNIFORMLOCATION)WrangleSubroutine("glGetUniformLocation", (void *)&dsglGetUniformLocation);

    //--L
    sglLinkProgram = (GLLINKPROGRAM_FUNC)WrangleSubroutine("glLinkProgram", (void *)&dsglLinkProgram);

    //--M
    sglMultiTexCoord2f = (GLMULTITEXCOORD2F_FUNC)WrangleSubroutine("glMultiTexCoord2f", (void *)&dsglMultiTexCoord2f);
    sglMultiTexCoord3f = (GLMULTITEXCOORD3F_FUNC)WrangleSubroutine("glMultiTexCoord3f", (void *)&dsglMultiTexCoord3f);

    //--S
    sglShaderSource = (GLSHADERSOURCE_FUNC)WrangleSubroutine("glShaderSource", (void *)&dsglShaderSource);
    swglSwapIntervalEXT = (WGLSWAPINTERVALEXT_FUNC)WrangleSubroutine("wglSwapIntervalEXT", (void *)&dswglSwapIntervalEXT);

    //--U
    sglUseProgram = (GLUSEPROGRAM_FUNC)WrangleSubroutine("glUseProgram", (void *)&dsglUseProgram);
    sglUniform1i = (GLUNIFORM1I)WrangleSubroutine("glUniform1i", (void *)&dsglUniform1i);
    sglUniform1f = (GLUNIFORM1F)WrangleSubroutine("glUniform1f", (void *)&dsglUniform1i);
    sglUniform4f = (GLUNIFORM4F)WrangleSubroutine("glUniform4f", (void *)&dsglUniform4f);

    //--V
    sglVertexAttribPointer = (GLVERTEXATTRIBPOINTER_FUNC)WrangleSubroutine("glVertexAttribPointer", (void *)&dsglVertexAttribPointer);

    DebugManager::PopPrint("[GL Wrangling] Done\n");
}
