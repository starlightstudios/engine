//--Base
#include "StarBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

///======================================== Draw Series ===========================================
//--Using the 'Draw' commands handles most of the work for you. It binds, translates, and renders
//  the bitmap automatically.
void StarBitmap::Draw()
{
    Draw(0, 0, 0);
}
void StarBitmap::Draw(float pTargetX, float pTargetY)
{
    Draw(pTargetX, pTargetY, 0);
}
void StarBitmap::Draw(float pTargetX, float pTargetY, int pFlags)
{
    ///--[Documentation and Setup]
    //--Multi-purpose drawing function. Binds the bitmap and renders it in 2 dimensions, automatically
    //  translating and flipping if necessary.

    ///--[Dummy Case]
    //--If this is a dummy bitmap, bark a warning and draw nothing.
    if(mIsDummyMode)
    {
        fprintf(stderr, "StarBitmap:Draw() - Warning. Dummy bitmap attempts to render. Case: %s\n", mDummyPath);
        return;
    }

    ///--[Multi-Texture Case]
    //--If this image is part of a larger texture, call that subroutine.
    if(mUsesMultipleTextures)
    {
        DrawMultiTexture(pTargetX, pTargetY, pFlags);
        return;
    }

    ///--[Delayed Loading Check]
    //--If the image does not have a GL Handle, it can't render. This may be because it is incomplete,
    //  or it may be because the data is in the SLF file and should hot-load. The function HandleNoDataAtRender()
    //  will determine if it should attempt to continue, and may enqueue delayed loading.
    if(!mGLHandle)
    {
        bool tProceedWithRender = HandleNoDataAtRender(SUGARBITMAP_NODATA_DONT_CARE, true);
        if(!tProceedWithRender || !mGLHandle) return;
    }

    ///--[Setup]
    //--Texture coordinates.
    float tTxL = mAtlasLft;
    float tTxT = mAtlasTop;
    float tTxR = mAtlasRgt;
    float tTxB = mAtlasBot;

    //--Horizontal flipping.
    int tUseFlipFlag = 0;
    if(pFlags & SUGAR_FLIP_HORIZONTAL)
    {
        pTargetX += (mTrueWidth - mWidth - mXOffset);
        tTxL = mAtlasRgt;
        tTxR = mAtlasLft;
        tUseFlipFlag |= SUGAR_FLIP_HORIZONTAL;
    }
    else
    {
        pTargetX += mXOffset;
    }

    //--Vertical flipping.
    if(pFlags & SUGAR_FLIP_VERTICAL)
    {
        pTargetY += (mTrueHeight - mHeight - mYOffset);
        tTxT = mAtlasBot;
        tTxB = mAtlasTop;
        tUseFlipFlag |= SUGAR_FLIP_VERTICAL;
    }
    else
    {
        pTargetY += mYOffset;
    }

    //--Internal flipping. Flips the texture but not the positioning. This is caused by OpenGL's
    //  internal flipping of bitmaps to match the mathematical coordinate system.
    if(mIsInternallyFlipped)
    {
        float tTemp = tTxB;
        tTxB = tTxT;
        tTxT = tTemp;
    }

    //--Positioning.
    glTranslatef(pTargetX, pTargetY, 0.0f);
    float tLft = 0.0f;
    float tTop = 0.0f;
    float tRgt = mWidth;
    float tBot = mHeight;

    //--List handling. There is a GLList for each of the flipping configurations. If the list has
    //  not been generated yet, generate it and compile. Otherwise, simply call it.
    //--The tUseFlipFlag will correspond to the array position to use.
    //--A StarBitmap may not necessarily use all four configurations if it is never flipped.
    if(!mGLLists[tUseFlipFlag])
    {
        mGLLists[tUseFlipFlag] = glGenLists(1);
        glNewList(mGLLists[tUseFlipFlag], GL_COMPILE_AND_EXECUTE);

        //--Actual rendering.
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glBegin(GL_QUADS);
            glTexCoord2f(tTxL, tTxT); glVertex2f(tLft, tTop);
            glTexCoord2f(tTxR, tTxT); glVertex2f(tRgt, tTop);
            glTexCoord2f(tTxR, tTxB); glVertex2f(tRgt, tBot);
            glTexCoord2f(tTxL, tTxB); glVertex2f(tLft, tBot);
        glEnd();

        //--Clean up.
        glEndList();
    }
    else
    {
        glCallList(mGLLists[tUseFlipFlag]);
    }

    //--Clean.
    glTranslatef(-pTargetX, -pTargetY, 0.0f);
}
void StarBitmap::DrawScaled(float pTargetX, float pTargetY, float pScaleX, float pScaleY)
{
    ///--[Documentation]
    //--Renders the bitmap scaled. Note that the scale only applies to the bitmap, not its translation coordinates.
    if(pScaleX == 0.0f || pScaleY == 0.0f) return;

    ///--[Execution]
    glTranslatef(pTargetX, pTargetY, 0.0f);
    glScalef(pScaleX, pScaleY, 1.0f);
    Draw();
    glScalef(1.0f / pScaleX, 1.0f / pScaleY, 1.0f);
    glTranslatef(-pTargetX, -pTargetY, 0.0f);
}
void StarBitmap::DrawScaled(float pTargetX, float pTargetY, float pScaleX, float pScaleY, int pFlags)
{
    ///--[Documentation and Setup]
    //--Multi-purpose drawing function. Binds the bitmap and renders it in 2 dimensions, automatically
    //  translating and flipping if necessary.
    //--This version also handles scaling.

    ///--[Dummy Case]
    //--If this is a dummy bitmap, bark a warning and draw nothing.
    if(mIsDummyMode)
    {
        fprintf(stderr, "StarBitmap:DrawScaled() - Warning. Dummy bitmap attempts to render. Case: %s\n", mDummyPath);
        return;
    }

    ///--[Delayed Loading Check]
    //--If the image does not have a GL Handle, it can't render. This may be because it is incomplete,
    //  or it may be because the data is in the SLF file and should hot-load. The function HandleNoDataAtRender()
    //  will determine if it should attempt to continue, and may enqueue delayed loading.
    if(!mGLHandle)
    {
        bool tProceedWithRender = HandleNoDataAtRender(SUGARBITMAP_NODATA_DONT_CARE, true);
        if(!tProceedWithRender || !mGLHandle) return;
    }

    ///--[Argument Check]
    if(pScaleX == 0.0f || pScaleY == 0.0f) return;

    //--Position and Scale.
    glTranslatef(pTargetX, pTargetY, 0.0f);
    glScalef(pScaleX, pScaleY, 1.0f);

    //--Variables.
    float tInternalXOffset = 0.0f;
    float tInternalYOffset = 0.0f;

    //--Texture coordinates.
    float tTxL = mAtlasLft;
    float tTxT = mAtlasTop;
    float tTxR = mAtlasRgt;
    float tTxB = mAtlasBot;

    //--Horizontal flipping.
    int tUseFlipFlag = 0;
    if(pFlags & SUGAR_FLIP_HORIZONTAL)
    {
        tInternalXOffset = (mTrueWidth - mWidth - mXOffset);
        tTxL = mAtlasRgt;
        tTxR = mAtlasLft;
        tUseFlipFlag |= SUGAR_FLIP_HORIZONTAL;
    }
    else
    {
        tInternalXOffset = mXOffset;
    }

    //--Vertical flipping.
    if(pFlags & SUGAR_FLIP_VERTICAL)
    {
        tInternalYOffset = (mTrueHeight - mHeight - mYOffset);
        tTxT = mAtlasBot;
        tTxB = mAtlasTop;
        tUseFlipFlag |= SUGAR_FLIP_VERTICAL;
    }
    else
    {
        tInternalYOffset = mYOffset;
    }

    //--Internal flipping. Flips the texture but not the positioning. This is caused by OpenGL's
    //  internal flipping of bitmaps to match the mathematical coordinate system.
    if(mIsInternallyFlipped)
    {
        float tTemp = tTxB;
        tTxB = tTxT;
        tTxT = tTemp;
    }

    //--Positioning.
    glTranslatef(tInternalXOffset, tInternalYOffset, 0.0f);
    float tLft = 0.0f;
    float tTop = 0.0f;
    float tRgt = mWidth;
    float tBot = mHeight;

    //--List handling. There is a GLList for each of the flipping configurations. If the list has
    //  not been generated yet, generate it and compile. Otherwise, simply call it.
    //--The tUseFlipFlag will correspond to the array position to use.
    //--A StarBitmap may not necessarily use all four configurations if it is never flipped.
    if(!mGLLists[tUseFlipFlag])
    {
        mGLLists[tUseFlipFlag] = glGenLists(1);
        glNewList(mGLLists[tUseFlipFlag], GL_COMPILE_AND_EXECUTE);

        //--Actual rendering.
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glBegin(GL_QUADS);
            glTexCoord2f(tTxL, tTxT); glVertex2f(tLft, tTop);
            glTexCoord2f(tTxR, tTxT); glVertex2f(tRgt, tTop);
            glTexCoord2f(tTxR, tTxB); glVertex2f(tRgt, tBot);
            glTexCoord2f(tTxL, tTxB); glVertex2f(tLft, tBot);
        glEnd();

        //--Clean up.
        glEndList();
    }
    else
    {
        glCallList(mGLLists[tUseFlipFlag]);
    }

    //--Clean.
    glTranslatef(-tInternalXOffset, -tInternalYOffset, 0.0f);
    glScalef(1.0f / pScaleX, 1.0f / pScaleY, 1.0f);
    glTranslatef(-pTargetX, -pTargetY, 0.0f);
}
void StarBitmap::DrawMultiTexture(int pTargetX, int pTargetY, int pFlags)
{
    ///--[Documentation]
    //--If the bitmap is broken into parts, renders each part instead of all them at once. Does not
    //  use glLists to boost render speed, and ignores flip instructions!

    ///--[Dummy Case]
    //--If this is a dummy bitmap, bark a warning and draw nothing.
    if(mIsDummyMode)
    {
        fprintf(stderr, "StarBitmap:DrawMultiTexture() - Warning. Dummy bitmap attempts to render. Case: %s\n", mDummyPath);
        return;
    }

    ///--[Rendering]
    glTranslatef(pTargetX, pTargetY, 0.0f);

    //--Setup.
    float tLft = 0.0f;
    float tTop = 0.0f;
    float tRgt = mWidth;
    float tBot = mHeight;

    for(int x = 0; x < mMultiTexX; x ++)
    {
        for(int y = 0; y < xMultiTexY; y ++)
        {
            //--Resolve
            tLft = x * xMaxTextureSize;
            tTop = y * xMaxTextureSize;
            tRgt = ((x+1) * xMaxTextureSize);
            tBot = ((y+1) * xMaxTextureSize);
            if(tRgt > mWidth) tRgt = mWidth;
            if(tBot > mHeight) tBot = mHeight;

            //--Actual rendering.
            glBindTexture(GL_TEXTURE_2D, mMultiGLHandles[x][y]);
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(tLft, tTop);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(tRgt, tTop);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(tRgt, tBot);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(tLft, tBot);
            glEnd();
        }
    }

    //--Clean.
    glTranslatef(-pTargetX, -pTargetY, 0.0f);
}
void StarBitmap::RenderPercent(float pXOffset, float pYOffset, float pLft, float pTop, float pRgt, float pBot)
{
    ///--[Documentation]
    //--Renders a percentage of the image from pLft to pRgt, pTop to pBot. Often used for health bars and the like,
    //  assumes the image is rendering at its original size and offset, but only part is being rendered.

    ///--[Dummy Case]
    //--If this is a dummy bitmap, bark a warning and draw nothing.
    if(mIsDummyMode)
    {
        fprintf(stderr, "StarBitmap:RenderPercent() - Warning. Dummy bitmap attempts to render. Case: %s\n", mDummyPath);
        return;
    }

    ///--[Execution]
    //--Compute pixel position.
    float cLft = pXOffset + mXOffset + (pLft * mWidth);
    float cTop = pYOffset + mYOffset + (pTop * mHeight);
    float cRgt = pXOffset + mXOffset + (pRgt * mWidth);
    float cBot = pYOffset + mYOffset + (pBot * mHeight);

    //--Compute texel position. Remember that images are flipped in opengl.
    float cTxL = pLft;
    float cTxT = 1.0f - pTop;
    float cTxR = pRgt;
    float cTxB = 1.0f - pBot;

    //--Render.
    glBindTexture(GL_TEXTURE_2D, mGLHandle);
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
    glEnd();
}
void StarBitmap::RenderNine(float pLft, float pMLf, float pMRt, float pRgt, float pTop, float pMTp, float pMBt, float pBot, float pTxMLf, float pTxMRt, float pTxMTp, float pTxMBt)
{
    ///--[Documentation]
    //--Splits the image into nine blocks and renders them. The middle blocks stretch along an axis, but the corners remain the same.
    //  This is basically a variant of content-aware scaling. Takes a lot of work!
    //--Note the order arguments are passed in. The last four arguments are the texture percentages where the pieces cut.

    ///--[Dummy Case]
    //--If this is a dummy bitmap, bark a warning and draw nothing.
    if(mIsDummyMode)
    {
        fprintf(stderr, "StarBitmap:RenderNine() - Warning. Dummy bitmap attempts to render. Case: %s\n", mDummyPath);
        return;
    }

    ///--[Execution]
    float cTxLft = 0.0f;
    float cTxRgt = 1.0f;
    float cTxTop = 0.0f;
    float cTxBot = 1.0f;

    //--Flip all vertical coordinates.
    cTxTop = 1.0f - cTxTop;
    pTxMTp = 1.0f - pTxMTp;
    pTxMBt = 1.0f - pTxMBt;
    cTxBot = 1.0f - cTxBot;

    //--Begin rendering.
    glBindTexture(GL_TEXTURE_2D, mGLHandle);
    glBegin(GL_QUADS);

        ///--[Top Blocks]
        //--Top, left.
        glTexCoord2f(cTxLft, cTxTop); glVertex2f(pLft, pTop);
        glTexCoord2f(pTxMLf, cTxTop); glVertex2f(pMLf, pTop);
        glTexCoord2f(pTxMLf, pTxMTp); glVertex2f(pMLf, pMTp);
        glTexCoord2f(cTxLft, pTxMTp); glVertex2f(pLft, pMTp);

        //--Top, middle. Stretches.
        glTexCoord2f(pTxMLf, cTxTop); glVertex2f(pMLf, pTop);
        glTexCoord2f(pTxMRt, cTxTop); glVertex2f(pMRt, pTop);
        glTexCoord2f(pTxMRt, pTxMTp); glVertex2f(pMRt, pMTp);
        glTexCoord2f(pTxMLf, pTxMTp); glVertex2f(pMLf, pMTp);

        //--Top, right.
        glTexCoord2f(pTxMRt, cTxTop); glVertex2f(pMRt, pTop);
        glTexCoord2f(cTxRgt, cTxTop); glVertex2f(pRgt, pTop);
        glTexCoord2f(cTxRgt, pTxMTp); glVertex2f(pRgt, pMTp);
        glTexCoord2f(pTxMRt, pTxMTp); glVertex2f(pMRt, pMTp);

        ///--[Middle Blocks]
        //--Middle, left. Stretches.
        glTexCoord2f(cTxLft, pTxMTp); glVertex2f(pLft, pMTp);
        glTexCoord2f(pTxMLf, pTxMTp); glVertex2f(pMLf, pMTp);
        glTexCoord2f(pTxMLf, pTxMBt); glVertex2f(pMLf, pMBt);
        glTexCoord2f(cTxLft, pTxMBt); glVertex2f(pLft, pMBt);

        //--Middle, middle. Stretches.
        glTexCoord2f(pTxMLf, pTxMTp); glVertex2f(pMLf, pMTp);
        glTexCoord2f(pTxMRt, pTxMTp); glVertex2f(pMRt, pMTp);
        glTexCoord2f(pTxMRt, pTxMBt); glVertex2f(pMRt, pMBt);
        glTexCoord2f(pTxMLf, pTxMBt); glVertex2f(pMLf, pMBt);

        //--Middle, right. Stretches.
        glTexCoord2f(pTxMRt, pTxMTp); glVertex2f(pMRt, pMTp);
        glTexCoord2f(cTxRgt, pTxMTp); glVertex2f(pRgt, pMTp);
        glTexCoord2f(cTxRgt, pTxMBt); glVertex2f(pRgt, pMBt);
        glTexCoord2f(pTxMRt, pTxMBt); glVertex2f(pMRt, pMBt);

        ///--[Bottom Blocks]
        //--Bottom, left.
        glTexCoord2f(cTxLft, pTxMBt); glVertex2f(pLft, pMBt);
        glTexCoord2f(pTxMLf, pTxMBt); glVertex2f(pMLf, pMBt);
        glTexCoord2f(pTxMLf, cTxBot); glVertex2f(pMLf, pBot);
        glTexCoord2f(cTxLft, cTxBot); glVertex2f(pLft, pBot);

        //--Bottom, middle. Stretches.
        glTexCoord2f(pTxMLf, pTxMBt); glVertex2f(pMLf, pMBt);
        glTexCoord2f(pTxMRt, pTxMBt); glVertex2f(pMRt, pMBt);
        glTexCoord2f(pTxMRt, cTxBot); glVertex2f(pMRt, pBot);
        glTexCoord2f(pTxMLf, cTxBot); glVertex2f(pMLf, pBot);

        //--Bottom, right.
        glTexCoord2f(pTxMRt, pTxMBt); glVertex2f(pMRt, pMBt);
        glTexCoord2f(cTxRgt, pTxMBt); glVertex2f(pRgt, pMBt);
        glTexCoord2f(cTxRgt, cTxBot); glVertex2f(pRgt, pBot);
        glTexCoord2f(pTxMRt, cTxBot); glVertex2f(pMRt, pBot);

    glEnd();
}

///--[Render Series]
//--This allows more 'hands-on' control over the rendering process. If performed in sequence, the
//  bitmap will be translated, rotated, rendered, and then the GL state is reset.
//--Note that, at no point, is the glhandle bound. Use a call of Bind() to do that.
void StarBitmap::TranslateOffsets()
{
    //--Translates down the StarBitmap's internal offsets.  Assumes nothing about the GLState.
    glTranslatef(mXOffset, mYOffset, 0.0f);
}
void StarBitmap::Rotate(float pAngle)
{
    //--Translate to the center and rotate.
    glTranslatef(mWidth /  2.0f, mHeight /  2.0f, 0.0f);
    glRotatef(pAngle, 0.0f, 0.0f, 1.0f);
    glTranslatef(mWidth / -2.0f, mHeight / -2.0f, 0.0f);
}
void StarBitmap::RenderAt()
{
    ///--[Dummy Case]
    //--If this is a dummy bitmap, bark a warning and draw nothing.
    if(mIsDummyMode)
    {
        fprintf(stderr, "StarBitmap:RenderAt() - Warning. Dummy bitmap attempts to render. Case: %s\n", mDummyPath);
        return;
    }

    ///--[Execution]
    //--Renders a quad using this StarBitmap's internal sizes.
    glBegin(GL_QUADS);
        glTexCoord2f(mAtlasLft, mAtlasBot); glVertex2f(     0,       0);
        glTexCoord2f(mAtlasRgt, mAtlasBot); glVertex2f(mWidth,       0);
        glTexCoord2f(mAtlasRgt, mAtlasTop); glVertex2f(mWidth, mHeight);
        glTexCoord2f(mAtlasLft, mAtlasTop); glVertex2f(     0, mHeight);
    glEnd();
}
void StarBitmap::Unrotate(float pAngle)
{
    //--Undoes a Rotate() call.
    glTranslatef(mWidth /  2.0f, mHeight /  2.0f, 0.0f);
    glRotatef(pAngle, 0.0f, 0.0f, -1.0f);
    glTranslatef(mWidth / -2.0f, mHeight / -2.0f, 0.0f);
}
void StarBitmap::UntranslateOffsets()
{
    //--Undoes a Translate() call.
    glTranslatef(-mXOffset, -mYOffset, 0.0f);
}
