//--Base
#include "MonocerosTitle.h"

//--Classes
//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== System ==============================================
///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonocerosTitle::SetStartingLanguage(const char *pString)
{
    ///--[Documentation]
    //--At boot, sets the language that we are starting at. This should only be called from Lua once
    //  all language packs have resolved.
    mStartingLanguage = 0;
    if(!pString || mLanguageList->GetListSize() < 1) return;

    ///--[Locate]
    //--Find the language pack that has this language in it.
    int i = 0;
    LanguagePack *rPackage = (LanguagePack *)mLanguageList->PushIterator();
    while(rPackage)
    {
        //--Match:
        if(!strcasecmp(rPackage->mLanguageCode, pString))
        {
            mStartingLanguage = i;
            mSelectedLanguage = i;
            mLanguageList->PopIterator();
            return;
        }

        //--Next.
        i ++;
        rPackage = (LanguagePack *)mLanguageList->AutoIterate();
    }

    ///--[Not Found]
    //--Defaults to 0, English. Write this to the language file.
    FILE *fOutfile = fopen("Data/Language.txt", "w");
    if(!fOutfile) return;
    fprintf(fOutfile, "English");
    fclose(fOutfile);
}
void MonocerosTitle::ChangeLanguage(const char *pString)
{
    ///--[Documentation]
    //--In addition to changing internal flags local to this object, we also save the new language
    //  onto the hard drive. A restart is required if the language actually does change.
    //--The language is provided, meaning this can be called from Lua and it will back-locate
    //  the appropriate flag/index.
    mSelectedLanguage = 0;
    if(!pString || mLanguageList->GetListSize() < 1) return;

    ///--[Locate]
    //--Find the language pack that has this language in it.
    int i = 0;
    LanguagePack *rPackage = (LanguagePack *)mLanguageList->PushIterator();
    while(rPackage)
    {
        //--Match:
        if(!strcasecmp(rPackage->mLanguageCode, pString))
        {
            mSelectedLanguage = i;
            mLanguageList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rPackage = (LanguagePack *)mLanguageList->AutoIterate();
    }

    ///--[Reset Files]
    //--The file Data/Language.txt is how the engine knows which language to boot into.
    FILE *fOutfile = fopen("Data/Language.txt", "w");
    if(!fOutfile) return;

    //--Write the selected language code.
    LanguagePack *rSelectedPack = (LanguagePack *)mLanguageList->GetElementBySlot(mSelectedLanguage);
    fprintf(fOutfile, "%s", rSelectedPack->mLanguageCode);

    ///--[Finish Up]
    fclose(fOutfile);
}
void MonocerosTitle::RegisterLanguage(const char *pLanguageName, const char *pExtension, const char *pFolderPath, const char *pFlagFilePath, const char *pRestartPath)
{
    ///--[Documentation]
    //--Registers a new language package with the given name and flag. This is expected to be a DL Path to the flag image.
    if(!pLanguageName || !pExtension || !pFlagFilePath || !pRestartPath || !pFolderPath) return;

    //--Create.
    LanguagePack *nPack = (LanguagePack *)starmemoryalloc(sizeof(LanguagePack));
    nPack->Initialize();
    nPack->mLanguageCode = InitializeString(pLanguageName);
    nPack->mExtension = InitializeString(pExtension);
    nPack->mFolderPath = InitializeString(pFolderPath);
    nPack->mFlag = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pFlagFilePath);
    nPack->mRestartProgram = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pRestartPath);

    //--The position of the flag is based on how many entries are already registered.
    int tCurEntries = mLanguageList->GetListSize();
    int tXPos = tCurEntries % 3;
    int tYPos = tCurEntries / 3;
    float cStartX = 499.0f;
    float cStartY = 120.0f;
    float cWid = 124.0f;
    float cHei = 64.0f;
    nPack->mPosition.SetWH(cStartX + (cWid * tXPos), cStartY + (cHei * tYPos), 120.0f, 60.0f);

    //--Register.
    mLanguageList->AddElementAsTail("X", nPack, &LanguagePack::DeleteThis);
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonocerosTitle::UpdateLanguageMode()
{
    ///--[ =============== Documentation ================ ]
    //--Takes over the update when the language menu is being displayed. Returns true if the update
    //  should be blocked, false if not.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get mouse position.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    ///--[Timers]
    //--Language Timer
    if(mIsLanguageMenuActive)
    {
        if(mLanguageTimer < MONOTITLE_LANGUAGE_TICKS) mLanguageTimer ++;
    }
    else
    {
        if(mLanguageTimer > 0) mLanguageTimer --;
    }

    ///--[ ================ Redirects ================== ]
    ///--[Inactive Block]
    //--If not in language mode, stop.
    if(!mIsLanguageMenuActive) return false;

    ///--[Execution of Packages]
    if(mIsLanguageExecuting)
    {
        //--Run timer. At 5 ticks, execute.
        mExecutionTimer ++;
        if(mExecutionTimer == 5)
        {
            //--Creation of new packs:
            if(mConfirmCreate)
            {
                //--Data.
                LanguagePack *rActivePack = (LanguagePack *)mLanguageList->GetElementBySlot(mLanguageUseForPacks);
                if(rActivePack)
                {
                    CreateLanguageDirectory(rActivePack->mLanguageCode, rActivePack->mFolderPath, rActivePack->mExtension);
                    //CreateLanguagePack(rActivePack->mLanguageCode, rActivePack->mFolderPath, rActivePack->mExtension);
                }
            }
            //--Deployment of package.
            else if(mConfirmDeploy)
            {
                //--Data.
                LanguagePack *rActivePack = (LanguagePack *)mLanguageList->GetElementBySlot(mLanguageUseForPacks);
                if(rActivePack)
                {
                    DeployLanguagePack(rActivePack->mLanguageCode, rActivePack->mFolderPath);
                }
            }
        }

        //--At 10 ticks, end execution.
        if(mExecutionTimer >= 10)
        {
            mIsLanguageExecuting = false;
            mConfirmCreate = false;
            mConfirmDeploy = false;
            AudioManager::Fetch()->PlaySound("Menu|Save");
        }

        return true;
    }

    ///--[Language Selection]
    //--Player must click a language pack, or outside the packs to cancel.
    if(mIsSelectingLanguagePack)
    {
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Reset flag.
            mLanguageUseForPacks = -1;

            //--Check if we hit any language packs.
            int i = 0;
            LanguagePack *rCheckPack = (LanguagePack *)mLanguageList->PushIterator();
            while(rCheckPack)
            {
                //--Hit, change language.
                if(rCheckPack->mPosition.IsPointWithin(tMouseX, tMouseY))
                {
                    //--Order the menu to close.
                    mLanguageUseForPacks = i;

                    //--Stop iterating.
                    mLanguageList->PopIterator();
                    break;
                }

                //--Next.
                i ++;
                rCheckPack = (LanguagePack *)mLanguageList->AutoIterate();
            }

            //--If no flag was hit, exit language selection:
            if(mLanguageUseForPacks == -1)
            {
                mIsSelectingLanguagePack = false;
                mConfirmCreate = false;
                mConfirmDeploy = false;
            }
            //--If the zeroth language is picked, English, print a warning. Packs can't be made for English.
            else if(mLanguageUseForPacks == 0)
            {
                mIsSelectingLanguagePack = false;
                mConfirmCreate = false;
                mConfirmDeploy = false;
                fprintf(stderr, "English cannot be used as a base for language packs.\n");
            }
            //--Pack was selected, go to confirmation.
            else
            {
                mIsSelectingLanguagePack = false;
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    ///--[Confirm Creation]
    //--Brings up a confirmation box.
    if(mConfirmCreate)
    {
        //--Confirm:
        if(rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Confirm))
        {
            mIsLanguageExecuting = true;
            mExecutionTimer = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Cancel:
        else if(rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Cancel))
        {
            mConfirmCreate = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Toggle:
        else if(rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Toggle))
        {
            mCreateBackups = !mCreateBackups;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    ///--[Confirm Deploy]
    //--Brings up a confirmation box.
    if(mConfirmDeploy)
    {
        //--Confirm:
        if(rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Confirm))
        {
            mIsLanguageExecuting = true;
            mExecutionTimer = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Cancel:
        else if(rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Cancel))
        {
            mConfirmDeploy = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Toggle:
        else if(rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Toggle))
        {
            mCreateBackups = !mCreateBackups;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    ///--[ ================ Primary Menu ================ ]
    ///--[Setup]
    //--Storage.
    int tPrevSelect = mSelectedLanguage;

    ///--[Mouse Over]
    //--If the submenu is open, and on the Create hitbox:
    if(mIsLanguageSubmenuActive && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Create))
    {
        mLanguageHighlight = cxLanguage_Create;
    }
    //--If the submenu is open, and on the Deploy hitbox:
    else if(mIsLanguageSubmenuActive && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Deploy))
    {
        mLanguageHighlight = cxLanguage_Deploy;
    }
    else
    {
        mLanguageHighlight = cxLanguage_Nothing;
    }

    ///--[Keyboard]
    //--Arrow keys.
    if(rControlManager->IsFirstPress("Left"))
    {
        int tModulo = mSelectedLanguage % 3;
        if(tModulo > 0)
        {
            mSelectedLanguage --;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tModulo = mSelectedLanguage % 3;
        if(tModulo < 2)
        {
            mSelectedLanguage ++;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    if(rControlManager->IsFirstPress("Up"))
    {
        if(mSelectedLanguage >= 3)
        {
            mSelectedLanguage -= 3;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tMaxEntries = mLanguageList->GetListSize();
        if(mSelectedLanguage < tMaxEntries - 3)
        {
            mSelectedLanguage += 3;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Pushing enter will close the menu.
    if(rControlManager->IsFirstPress("Activate") || rControlManager->IsFirstPress("Enter"))
    {
        mIsLanguageMenuActive = false;
        mIsLanguageSubmenuActive = false;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Click Handling]
    //--Left-click:
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--If the click is inside the language box:
        if(tMouseX >= 464.0f && tMouseY >= 89.0f && tMouseX <= 464.0f + 437.0f && tMouseY <= 89.0f + 589.0f)
        {
            //--Check if we hit any language packs.
            int i = 0;
            LanguagePack *rCheckPack = (LanguagePack *)mLanguageList->PushIterator();
            while(rCheckPack)
            {
                //--Hit, change language.
                if(rCheckPack->mPosition.IsPointWithin(tMouseX, tMouseY))
                {
                    //--Order the menu to close.
                    mSelectedLanguage = i;
                    mIsLanguageMenuActive = false;
                    mIsLanguageSubmenuActive = false;

                    //--SFX.
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

                    //--Stop iterating.
                    mLanguageList->PopIterator();
                    break;
                }

                //--Next.
                i ++;
                rCheckPack = (LanguagePack *)mLanguageList->AutoIterate();
            }
        }
        //--If the submenu is not open, and the click is in the submenu's box:
        else if(!mIsLanguageSubmenuActive && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_LanguageSubmenu))
        {
            //--Flags.
            mIsLanguageSubmenuActive = true;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If the submenu is open, and on the Create hitbox:
        else if(mIsLanguageSubmenuActive && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Create))
        {
            mConfirmCreate = true;
            mIsSelectingLanguagePack = true;
        }
        //--If the submenu is open, and on the Deploy hitbox:
        else if(mIsLanguageSubmenuActive && IsPointWithin2DReal(tMouseX, tMouseY, mHitbox_Deploy))
        {
            mConfirmDeploy = true;
            mIsSelectingLanguagePack = true;
        }
        //--Outside the box, close.
        else
        {
            mIsLanguageMenuActive = false;
            mIsLanguageSubmenuActive = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    //--Language selection changed.
    if(tPrevSelect != mSelectedLanguage)
    {
        //--Locate package.
        LanguagePack *rCheckPack = (LanguagePack *)mLanguageList->GetElementBySlot(mSelectedLanguage);
        if(rCheckPack)
        {
            ChangeLanguage(rCheckPack->mLanguageCode);
        }
        else
        {
            mSelectedLanguage = tPrevSelect;
        }
    }

    ///--[Handled Update]
    return true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonocerosTitle::RenderLanguageMode()
{
    ///--[ =============== Documentation ================ ]
    //--Language display, renders over the rest of the menu. In most cases just shows a frame and
    //  the languages available, but power users can use the special menu in the bottom left to do more.
    if(mLanguageTimer < 1) return;

    ///--[Setup]
    //--Resolve alpha.
    float cVisAlpha = EasingFunction::QuadraticInOut(mLanguageTimer, MONOTITLE_LANGUAGE_TICKS);

    //--Set mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cVisAlpha);

    ///--[ ================ Primary Menu ================ ]
    ///--[Frame, Flags]
    //--Render frame.
    MonoImg.Data.rFrameLanguage->Draw();

    //--Begin rendering flags.
    int i = 0;
    LanguagePack *rPackage = (LanguagePack *)mLanguageList->PushIterator();
    while(rPackage)
    {
        //--Skip if the flag didn't resolve.
        if(!rPackage->mFlag)
        {
            rPackage = (LanguagePack *)mLanguageList->AutoIterate();
            continue;
        }

        //--Render.
        rPackage->mFlag->Draw(rPackage->mPosition.mLft, rPackage->mPosition.mTop);

        //--If this is the selected language, render this overlay.
        if(i == mSelectedLanguage) MonoImg.Data.rOverlayFlagSelect->Draw(rPackage->mPosition.mLft, rPackage->mPosition.mTop);

        //--Next.
        i ++;
        rPackage = (LanguagePack *)mLanguageList->AutoIterate();
    }

    ///--[Submenu]
    //--If selecting a language pack, don't render the submenu.
    if(mIsSelectingLanguagePack)
    {

    }
    //--If not active, render a little button.
    else if(!mIsLanguageSubmenuActive)
    {
        MonoImg.Data.rButton_LanguageShowMore->Draw();
    }
    //--If active, render more detailed data and buttons.
    else
    {
        //--Constants.
        float cEdgeWid  =  5.0f;
        float cTexWid   = 32.0f;
        float cTxEdgeLo = cEdgeWid / cTexWid;
        float cTxEdgeHi = 1.0f - (cTxEdgeLo);

        //--Determine the widest string. This changes based on the current language!
        float cWidest = 100.0f;
        float tLen = MonoImg.Data.rFontMenu->GetTextWidth(xrStringData->str.mTextDeveloperMenu);
        if(tLen > cWidest) cWidest = tLen;
        tLen = MonoImg.Data.rFontMenu->GetTextWidth(xrStringData->str.mTextCreateNewPack);
        if(tLen > cWidest) cWidest = tLen;
        tLen = MonoImg.Data.rFontMenu->GetTextWidth(xrStringData->str.mTextLoadPack);
        if(tLen > cWidest) cWidest = tLen;

        //--String Data.
        float cPadW   =  10.0f;
        float cPadH   =   3.0f;
        float cTextH  = MonoImg.Data.rFontMenu->GetTextHeight();

        //--Frame X positions.
        float cFrameL  = 10.0f;
        float cFrameML = cFrameL + cEdgeWid;
        float cFrameMR = cFrameML + cWidest + cPadW + cPadW;
        float cFrameR  = cFrameMR + cEdgeWid;

        //--Frame Y positions. Bottom-up.
        float cFrameB  = VIRTUAL_CANVAS_Y - 10.0f;
        float cFrameMB = cFrameB - cEdgeWid;
        float cFrameMT = cFrameMB - (cTextH * 4.0f) - cPadH - cPadH;
        float cFrameT  = cFrameMT - cEdgeWid;

        //--Render the frame.
        MonoImg.Data.rFrame_LanguageEx->RenderNine(cFrameL, cFrameML, cFrameMR, cFrameR, cFrameT, cFrameMT, cFrameMB, cFrameB, cTxEdgeLo, cTxEdgeHi, cTxEdgeLo, cTxEdgeHi);

        //--Sizings.
        float cTextL = cFrameML + cPadW;
        float cTextT = cFrameMT + cPadH - 3.0f;

        //--Strings.
        MonoImg.Data.rFontMenu->DrawText(cTextL, cTextT + (cTextH * 0.0f), 0, 1.0f, xrStringData->str.mTextDeveloperMenu);

        //--Highlight case:
        if(mLanguageHighlight == cxLanguage_Create) StarlightColor::SetMixer(0.1f, 1.0f, 0.1f, cVisAlpha);
        MonoImg.Data.rFontMenu->DrawText(cTextL, cTextT + (cTextH * 2.0f), 0, 1.0f, xrStringData->str.mTextCreateNewPack);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cVisAlpha);

        if(mLanguageHighlight == cxLanguage_Deploy) StarlightColor::SetMixer(0.1f, 1.0f, 0.1f, cVisAlpha);
        MonoImg.Data.rFontMenu->DrawText(cTextL, cTextT + (cTextH * 3.0f), 0, 1.0f, xrStringData->str.mTextLoadPack);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cVisAlpha);

        //--Store this data to be used as hitboxes.
        mHitbox_Create.Set(cTextL, cTextT + (cTextH * 2.0f), cFrameMR, cTextT + (cTextH * 2.9f));
        mHitbox_Deploy.Set(cTextL, cTextT + (cTextH * 3.0f), cFrameMR, cTextT + (cTextH * 3.9f));
    }

    ///--[Clean Up]
    StarlightColor::ClearMixer();

    ///--[ ================== Overlays ================== ]
    //--Executing.
    if(mIsLanguageExecuting)
    {
        //--Backing. Grey out the rest of the menu.
        StarBitmap::DrawFullBlack(0.70f);

        //--Frame.
        MonoImg.Data.rFrame_Confirm->Draw();

        //--Text.
        MonoImg.Data.rFontMenu->DrawText(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextWorking);
    }
    //--Selecting a language pack:
    else if(mIsSelectingLanguagePack)
    {
        MonoImg.Data.rFontMenu->DrawText(VIRTUAL_CANVAS_X * 0.50f, 40.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSelectLanguage);
    }
    //--Confirm creation:
    else if(mConfirmCreate)
    {
        //--Paths.
        char *rUseLanguageCode = NULL;
        LanguagePack *rLangPackage = (LanguagePack *)mLanguageList->GetElementBySlot(mLanguageUseForPacks);
        if(rLangPackage) rUseLanguageCode = rLangPackage->mLanguageCode;

        //--Backing. Grey out the rest of the menu.
        StarBitmap::DrawFullBlack(0.70f);

        //--Frame.
        MonoImg.Data.rFrame_Confirm->Draw();

        //--Checkbox:
        if(mCreateBackups)
            MonoImg.Data.rFrame_LanguageCheck->Draw();
        else
            MonoImg.Data.rFrame_LanguageX->Draw();

        //--Text.
        MonoImg.Data.rFontMenu->DrawText(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextAskCreateNewPack);
        MonoImg.Data.rFontMenu->DrawTextArgs(310.0f, 213.0f, 0, 0.5f, xrStringData->str.mTextNewPackagePath, rUseLanguageCode);
        MonoImg.Data.rFontMenu->DrawText(310.0f, 245.0f, 0, 0.5f, xrStringData->str.mTextBackupPack);

        //--Buttons.
        MonoImg.Data.rFontMenu->DrawText(mHitbox_Confirm.mXCenter, mHitbox_Confirm.mYCenter - 8.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, xrStringData->str.mTextConfirm);
        MonoImg.Data.rFontMenu->DrawText(mHitbox_Cancel.mXCenter,  mHitbox_Cancel.mYCenter  - 8.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, xrStringData->str.mTextCancel);
    }
    //--Confirm deployment:
    else if(mConfirmDeploy)
    {
        //--Paths.
        char *rUseLanguageCode = NULL;
        LanguagePack *rLangPackage = (LanguagePack *)mLanguageList->GetElementBySlot(mLanguageUseForPacks);
        if(rLangPackage) rUseLanguageCode = rLangPackage->mExtension;

        //--Backing. Grey out the rest of the menu.
        StarBitmap::DrawFullBlack(0.70f);

        //--Frame.
        MonoImg.Data.rFrame_Confirm->Draw();

        //--Checkbox:
        if(mCreateBackups)
            MonoImg.Data.rFrame_LanguageCheck->Draw();
        else
            MonoImg.Data.rFrame_LanguageX->Draw();

        //--Text.
        MonoImg.Data.rFontMenu->DrawText(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextDeployPackage);
        MonoImg.Data.rFontMenu->DrawTextArgs(310.0f, 213.0f, 0, 0.5f, xrStringData->str.mTextWarningOverwrite, rUseLanguageCode);
        MonoImg.Data.rFontMenu->DrawText(310.0f, 245.0f, 0, 0.5f, xrStringData->str.mTextBackupFiles);

        //--Buttons.
        MonoImg.Data.rFontMenu->DrawText(mHitbox_Confirm.mXCenter, mHitbox_Confirm.mYCenter - 8.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, xrStringData->str.mTextConfirm);
        MonoImg.Data.rFontMenu->DrawText(mHitbox_Cancel.mXCenter,  mHitbox_Cancel.mYCenter  - 8.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, xrStringData->str.mTextCancel);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
