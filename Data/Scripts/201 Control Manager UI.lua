-- |[ =================================== Control Manager UI =================================== ]|
--Contains UI instructions for the Control Manager. This is where the little keys that appear in text
-- are resolved.

-- |[ ========================================= Loading ======================================== ]|
--Images used for the controls interfaces.
SLF_Open("Data/UIControls.slf")
DL_AddPath("Root/Images/ControlsUI/Keyboard/")

--Master list:
local saMasterList = {"Escape", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", 
                      "Tilde", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "Backspace", "Ins", "Home", "PgUp",
                      "Tab", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "LBrace", "RBrace", "Backslash", "Del", "End", "PgDn",
                      "Capslock", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Semicolon", "Apostrophie", "Pound", "Enter", "ArrU",
                      "LShift", "Z", "X", "C", "V", "B", "N", "M", "Comma", "Period", "Slash", "RShift", "ArrL", "ArrD", "ArrR",
                      "LCtrl", "LAlt", "Space", "RAlt", "RCtrl", "Error"}
for i = 1, #saMasterList, 1 do
    DL_ExtractBitmap("Keyboard|" .. saMasterList[i], "Root/Images/ControlsUI/Keyboard/" .. saMasterList[i])
end

-- |[ ======================================== Functions ======================================= ]|
--Adder function.
local iCurrentPack = 0
local fnSetImage = function(psString, psDLPath)
    if(psString == nil) then return end
    if(psDLPath == nil) then return end
    local iKeyboard, iMouse, iJoypad = ControlManager_GetProperty("Input Codes By String", psString)
    ControlManager_SetProperty("Set Control Image Pack", iCurrentPack, iKeyboard, iMouse, iJoypad, psDLPath)
    iCurrentPack = iCurrentPack + 1
end

-- |[ ================================ Upload to Control Manager =============================== ]|
--Create control references for the Control Manager. After this point, the program can request a
-- control's image and the Control Manager will return it.
ControlManager_SetProperty("Set Control Error Image", "Root/Images/ControlsUI/Keyboard/Error")

--Total
ControlManager_SetProperty("Allocate Control Image Packs", 81)

-- |[ ===================================== Allegro Version ==================================== ]|
if(LM_GetSystemLibrary() == "Allegro") then
    
    --Function Row
    fnSetImage("KEY_ESCAPE", "Root/Images/ControlsUI/Keyboard/Escape")
    fnSetImage("KEY_F1",  "Root/Images/ControlsUI/Keyboard/F1")
    fnSetImage("KEY_F2",  "Root/Images/ControlsUI/Keyboard/F2")
    fnSetImage("KEY_F3",  "Root/Images/ControlsUI/Keyboard/F3")
    fnSetImage("KEY_F4",  "Root/Images/ControlsUI/Keyboard/F4")
    fnSetImage("KEY_F5",  "Root/Images/ControlsUI/Keyboard/F5")
    fnSetImage("KEY_F6",  "Root/Images/ControlsUI/Keyboard/F6")
    fnSetImage("KEY_F7",  "Root/Images/ControlsUI/Keyboard/F7")
    fnSetImage("KEY_F8",  "Root/Images/ControlsUI/Keyboard/F8")
    fnSetImage("KEY_F9",  "Root/Images/ControlsUI/Keyboard/F9")
    fnSetImage("KEY_F10", "Root/Images/ControlsUI/Keyboard/F10")
    fnSetImage("KEY_F11", "Root/Images/ControlsUI/Keyboard/F11")
    fnSetImage("KEY_F12", "Root/Images/ControlsUI/Keyboard/F12")
    
    --Numeric Row
    fnSetImage("KEY_Tilde",     "Root/Images/ControlsUI/Keyboard/Tilde")
    fnSetImage("KEY_1",         "Root/Images/ControlsUI/Keyboard/1")
    fnSetImage("KEY_2",         "Root/Images/ControlsUI/Keyboard/2")
    fnSetImage("KEY_3",         "Root/Images/ControlsUI/Keyboard/3")
    fnSetImage("KEY_4",         "Root/Images/ControlsUI/Keyboard/4")
    fnSetImage("KEY_5",         "Root/Images/ControlsUI/Keyboard/5")
    fnSetImage("KEY_6",         "Root/Images/ControlsUI/Keyboard/6")
    fnSetImage("KEY_7",         "Root/Images/ControlsUI/Keyboard/7")
    fnSetImage("KEY_8",         "Root/Images/ControlsUI/Keyboard/8")
    fnSetImage("KEY_9",         "Root/Images/ControlsUI/Keyboard/9")
    fnSetImage("KEY_0",         "Root/Images/ControlsUI/Keyboard/0")
    fnSetImage("KEY_Minus",     "Root/Images/ControlsUI/Keyboard/-")
    fnSetImage("KEY_Equals",    "Root/Images/ControlsUI/Keyboard/=")
    fnSetImage("KEY_Backspace", "Root/Images/ControlsUI/Keyboard/Backspace")
    fnSetImage("KEY_INSERT",    "Root/Images/ControlsUI/Keyboard/Ins")
    fnSetImage("KEY_HOME",      "Root/Images/ControlsUI/Keyboard/Home")
    fnSetImage("KEY_PGUP",      "Root/Images/ControlsUI/Keyboard/PgUp")
    
    --QWERTY Row
    fnSetImage("KEY_TAB",       "Root/Images/ControlsUI/Keyboard/Tab")
    fnSetImage("KEY_Q",         "Root/Images/ControlsUI/Keyboard/Q")
    fnSetImage("KEY_W",         "Root/Images/ControlsUI/Keyboard/W")
    fnSetImage("KEY_E",         "Root/Images/ControlsUI/Keyboard/E")
    fnSetImage("KEY_R",         "Root/Images/ControlsUI/Keyboard/R")
    fnSetImage("KEY_T",         "Root/Images/ControlsUI/Keyboard/T")
    fnSetImage("KEY_Y",         "Root/Images/ControlsUI/Keyboard/Y")
    fnSetImage("KEY_U",         "Root/Images/ControlsUI/Keyboard/U")
    fnSetImage("KEY_I",         "Root/Images/ControlsUI/Keyboard/I")
    fnSetImage("KEY_O",         "Root/Images/ControlsUI/Keyboard/O")
    fnSetImage("KEY_P",         "Root/Images/ControlsUI/Keyboard/P")
    fnSetImage("KEY_LBrace",    "Root/Images/ControlsUI/Keyboard/LBrace")
    fnSetImage("KEY_RBrace",    "Root/Images/ControlsUI/Keyboard/RBrace")
    fnSetImage("KEY_Backslash", "Root/Images/ControlsUI/Keyboard/Backslash")
    fnSetImage("KEY_DELETE",    "Root/Images/ControlsUI/Keyboard/Del")
    fnSetImage("KEY_END",       "Root/Images/ControlsUI/Keyboard/End")
    fnSetImage("KEY_PGDN",      "Root/Images/ControlsUI/Keyboard/PgDn")

    --ASDF Row
    fnSetImage("KEY_CAPSLOCK",  "Root/Images/ControlsUI/Keyboard/Capslock")
    fnSetImage("KEY_A",         "Root/Images/ControlsUI/Keyboard/A")
    fnSetImage("KEY_S",         "Root/Images/ControlsUI/Keyboard/S")
    fnSetImage("KEY_D",         "Root/Images/ControlsUI/Keyboard/D")
    fnSetImage("KEY_F",         "Root/Images/ControlsUI/Keyboard/F")
    fnSetImage("KEY_G",         "Root/Images/ControlsUI/Keyboard/G")
    fnSetImage("KEY_H",         "Root/Images/ControlsUI/Keyboard/H")
    fnSetImage("KEY_J",         "Root/Images/ControlsUI/Keyboard/J")
    fnSetImage("KEY_K",         "Root/Images/ControlsUI/Keyboard/K")
    fnSetImage("KEY_L",         "Root/Images/ControlsUI/Keyboard/L")
    fnSetImage("KEY_Semicolon", "Root/Images/ControlsUI/Keyboard/Semicolon")
    fnSetImage("KEY_Quote",     "Root/Images/ControlsUI/Keyboard/Apostrophie")
    fnSetImage("KEY_Enter",     "Root/Images/ControlsUI/Keyboard/Enter")

    --ZXCV Row
    fnSetImage("KEY_LSHIFT",   "Root/Images/ControlsUI/Keyboard/LShift")
    fnSetImage("KEY_Z",        "Root/Images/ControlsUI/Keyboard/Z")
    fnSetImage("KEY_X",        "Root/Images/ControlsUI/Keyboard/X")
    fnSetImage("KEY_C",        "Root/Images/ControlsUI/Keyboard/C")
    fnSetImage("KEY_V",        "Root/Images/ControlsUI/Keyboard/V")
    fnSetImage("KEY_B",        "Root/Images/ControlsUI/Keyboard/B")
    fnSetImage("KEY_N",        "Root/Images/ControlsUI/Keyboard/N")
    fnSetImage("KEY_M",        "Root/Images/ControlsUI/Keyboard/M")
    fnSetImage("KEY_Comma",    "Root/Images/ControlsUI/Keyboard/Comma")
    fnSetImage("KEY_Period",   "Root/Images/ControlsUI/Keyboard/Period")
    fnSetImage("KEY_Slash",    "Root/Images/ControlsUI/Keyboard/Slash")
    fnSetImage("KEY_RSHIFT",   "Root/Images/ControlsUI/Keyboard/RShift")

    --Utility Row
    fnSetImage("KEY_LCTRL",  "Root/Images/ControlsUI/Keyboard/LCtrl")
    fnSetImage("KEY_ALT",    "Root/Images/ControlsUI/Keyboard/LAlt")
    fnSetImage("KEY_SPACE",  "Root/Images/ControlsUI/Keyboard/Space")
    fnSetImage("KEY_ALTGR",  "Root/Images/ControlsUI/Keyboard/RAlt")
    fnSetImage("KEY_RCTRL",  "Root/Images/ControlsUI/Keyboard/RCtrl")

    --Arrow Keys
    fnSetImage("KEY_UP",    "Root/Images/ControlsUI/Keyboard/ArrU")
    fnSetImage("KEY_DOWN",  "Root/Images/ControlsUI/Keyboard/ArrD")
    fnSetImage("KEY_LEFT",  "Root/Images/ControlsUI/Keyboard/ArrL")
    fnSetImage("KEY_RIGHT", "Root/Images/ControlsUI/Keyboard/ArrR")
    
-- |[ ======================================= SDL Version ====================================== ]|
else

    --Function Row
    fnSetImage("KEY_Escape", "Root/Images/ControlsUI/Keyboard/Escape")
    fnSetImage("KEY_F1",  "Root/Images/ControlsUI/Keyboard/F1")
    fnSetImage("KEY_F2",  "Root/Images/ControlsUI/Keyboard/F2")
    fnSetImage("KEY_F3",  "Root/Images/ControlsUI/Keyboard/F3")
    fnSetImage("KEY_F4",  "Root/Images/ControlsUI/Keyboard/F4")
    fnSetImage("KEY_F5",  "Root/Images/ControlsUI/Keyboard/F5")
    fnSetImage("KEY_F6",  "Root/Images/ControlsUI/Keyboard/F6")
    fnSetImage("KEY_F7",  "Root/Images/ControlsUI/Keyboard/F7")
    fnSetImage("KEY_F8",  "Root/Images/ControlsUI/Keyboard/F8")
    fnSetImage("KEY_F9",  "Root/Images/ControlsUI/Keyboard/F9")
    fnSetImage("KEY_F10", "Root/Images/ControlsUI/Keyboard/F10")
    fnSetImage("KEY_F11", "Root/Images/ControlsUI/Keyboard/F11")
    fnSetImage("KEY_F12", "Root/Images/ControlsUI/Keyboard/F12")
    
    --Numeric Row
    fnSetImage("KEY_Tilde",     "Root/Images/ControlsUI/Keyboard/Tilde")
    fnSetImage("KEY_1",         "Root/Images/ControlsUI/Keyboard/1")
    fnSetImage("KEY_2",         "Root/Images/ControlsUI/Keyboard/2")
    fnSetImage("KEY_3",         "Root/Images/ControlsUI/Keyboard/3")
    fnSetImage("KEY_4",         "Root/Images/ControlsUI/Keyboard/4")
    fnSetImage("KEY_5",         "Root/Images/ControlsUI/Keyboard/5")
    fnSetImage("KEY_6",         "Root/Images/ControlsUI/Keyboard/6")
    fnSetImage("KEY_7",         "Root/Images/ControlsUI/Keyboard/7")
    fnSetImage("KEY_8",         "Root/Images/ControlsUI/Keyboard/8")
    fnSetImage("KEY_9",         "Root/Images/ControlsUI/Keyboard/9")
    fnSetImage("KEY_0",         "Root/Images/ControlsUI/Keyboard/0")
    fnSetImage("KEY_Minus",     "Root/Images/ControlsUI/Keyboard/-")
    fnSetImage("KEY_Equals",    "Root/Images/ControlsUI/Keyboard/=")
    fnSetImage("KEY_Backspace", "Root/Images/ControlsUI/Keyboard/Backspace")
    fnSetImage("KEY_Insert",    "Root/Images/ControlsUI/Keyboard/Ins")
    fnSetImage("KEY_Home",      "Root/Images/ControlsUI/Keyboard/Home")
    fnSetImage("KEY_PageUp",    "Root/Images/ControlsUI/Keyboard/PgUp")
    
    --QWERTY Row
    fnSetImage("KEY_Tab",       "Root/Images/ControlsUI/Keyboard/Tab")
    fnSetImage("KEY_Q",         "Root/Images/ControlsUI/Keyboard/Q")
    fnSetImage("KEY_W",         "Root/Images/ControlsUI/Keyboard/W")
    fnSetImage("KEY_E",         "Root/Images/ControlsUI/Keyboard/E")
    fnSetImage("KEY_R",         "Root/Images/ControlsUI/Keyboard/R")
    fnSetImage("KEY_T",         "Root/Images/ControlsUI/Keyboard/T")
    fnSetImage("KEY_Y",         "Root/Images/ControlsUI/Keyboard/Y")
    fnSetImage("KEY_U",         "Root/Images/ControlsUI/Keyboard/U")
    fnSetImage("KEY_I",         "Root/Images/ControlsUI/Keyboard/I")
    fnSetImage("KEY_O",         "Root/Images/ControlsUI/Keyboard/O")
    fnSetImage("KEY_P",         "Root/Images/ControlsUI/Keyboard/P")
    fnSetImage("KEY_LBrace",    "Root/Images/ControlsUI/Keyboard/LBrace")
    fnSetImage("KEY_RBrace",    "Root/Images/ControlsUI/Keyboard/RBrace")
    fnSetImage("KEY_Backslash", "Root/Images/ControlsUI/Keyboard/Backslash")
    fnSetImage("KEY_Delete",    "Root/Images/ControlsUI/Keyboard/Del")
    fnSetImage("KEY_End",       "Root/Images/ControlsUI/Keyboard/End")
    fnSetImage("KEY_PageDown",  "Root/Images/ControlsUI/Keyboard/PgDn")

    --ASDF Row
    fnSetImage("KEY_CapsLock",  "Root/Images/ControlsUI/Keyboard/Capslock")
    fnSetImage("KEY_A",         "Root/Images/ControlsUI/Keyboard/A")
    fnSetImage("KEY_S",         "Root/Images/ControlsUI/Keyboard/S")
    fnSetImage("KEY_D",         "Root/Images/ControlsUI/Keyboard/D")
    fnSetImage("KEY_F",         "Root/Images/ControlsUI/Keyboard/F")
    fnSetImage("KEY_G",         "Root/Images/ControlsUI/Keyboard/G")
    fnSetImage("KEY_H",         "Root/Images/ControlsUI/Keyboard/H")
    fnSetImage("KEY_J",         "Root/Images/ControlsUI/Keyboard/J")
    fnSetImage("KEY_K",         "Root/Images/ControlsUI/Keyboard/K")
    fnSetImage("KEY_L",         "Root/Images/ControlsUI/Keyboard/L")
    fnSetImage("KEY_Semicolon", "Root/Images/ControlsUI/Keyboard/Semicolon")
    fnSetImage("KEY_Quote",     "Root/Images/ControlsUI/Keyboard/Apostrophie")
    fnSetImage("KEY_Return",    "Root/Images/ControlsUI/Keyboard/Enter")

    --ZXCV Row
    fnSetImage("KEY_LShift",   "Root/Images/ControlsUI/Keyboard/LShift")
    fnSetImage("KEY_Z",        "Root/Images/ControlsUI/Keyboard/Z")
    fnSetImage("KEY_X",        "Root/Images/ControlsUI/Keyboard/X")
    fnSetImage("KEY_C",        "Root/Images/ControlsUI/Keyboard/C")
    fnSetImage("KEY_V",        "Root/Images/ControlsUI/Keyboard/V")
    fnSetImage("KEY_B",        "Root/Images/ControlsUI/Keyboard/B")
    fnSetImage("KEY_N",        "Root/Images/ControlsUI/Keyboard/N")
    fnSetImage("KEY_M",        "Root/Images/ControlsUI/Keyboard/M")
    fnSetImage("KEY_Comma",    "Root/Images/ControlsUI/Keyboard/Comma")
    fnSetImage("KEY_Period",   "Root/Images/ControlsUI/Keyboard/Period")
    fnSetImage("KEY_Slash",    "Root/Images/ControlsUI/Keyboard/Slash")
    fnSetImage("KEY_RShift",   "Root/Images/ControlsUI/Keyboard/RShift")

    --Utility Row
    fnSetImage("KEY_LCtrl",     "Root/Images/ControlsUI/Keyboard/LCtrl")
    fnSetImage("KEY_Left Alt",  "Root/Images/ControlsUI/Keyboard/LAlt")
    fnSetImage("KEY_Space",     "Root/Images/ControlsUI/Keyboard/Space")
    fnSetImage("KEY_Right Alt", "Root/Images/ControlsUI/Keyboard/RAlt")
    fnSetImage("KEY_RCtrl",     "Root/Images/ControlsUI/Keyboard/RCtrl")

    --Arrow Keys
    fnSetImage("KEY_Up",    "Root/Images/ControlsUI/Keyboard/ArrU")
    fnSetImage("KEY_Down",  "Root/Images/ControlsUI/Keyboard/ArrD")
    fnSetImage("KEY_Left",  "Root/Images/ControlsUI/Keyboard/ArrL")
    fnSetImage("KEY_Right", "Root/Images/ControlsUI/Keyboard/ArrR")
    
end
