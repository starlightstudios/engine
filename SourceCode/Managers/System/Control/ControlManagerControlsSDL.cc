//--Base
#include "ControlManager.h"

//--Classes
//--Definitions
#include "Global.h"

//--Generics
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///--[ ========================================== SDL =========================================== ]
#if defined _SDL_PROJECT_
void ControlManager::SetupControls()
{
    ///--[Joysticks]
    if(false)
    {
        fprintf(stderr, "Number of attached joysticks: %i\n", SDL_NumJoysticks());
        if(SDL_NumJoysticks() > 0)
        {
            mJoystickHandle = SDL_JoystickOpen(0);
            printf("Opened Joystick 0\n");
            printf("Name: %s\n", SDL_JoystickName(0));
            printf("Number of Axes: %d\n", SDL_JoystickNumAxes(mJoystickHandle));
            printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(mJoystickHandle));
            printf("Number of Balls: %d\n", SDL_JoystickNumBalls(mJoystickHandle));
        }
    }

    ///--[Standard keys]
    //--Mappings for directional movements.
    RegisterControl("Up"   ,   CreateControlWatchingKeyboard(SDL_SCANCODE_UP,    SDL_SCANCODE_W));
    RegisterControl("Down" ,   CreateControlWatchingKeyboard(SDL_SCANCODE_DOWN,  SDL_SCANCODE_S));
    RegisterControl("Left" ,   CreateControlWatchingKeyboard(SDL_SCANCODE_LEFT,  SDL_SCANCODE_A));
    RegisterControl("Right",   CreateControlWatchingKeyboard(SDL_SCANCODE_RIGHT, SDL_SCANCODE_D));
    RegisterControl("UpLevel", CreateControlWatchingKeyboard(SDL_SCANCODE_Q,     SDL_SCANCODE_PAGEUP));
    RegisterControl("DnLevel", CreateControlWatchingKeyboard(SDL_SCANCODE_E,     SDL_SCANCODE_PAGEDOWN));

    //--Action mappings.
    RegisterControl("Activate", CreateControlWatchingKeyboard(SDL_SCANCODE_Z,      -1));
    RegisterControl("Cancel",   CreateControlWatchingKeyboard(SDL_SCANCODE_X,      -1));
    RegisterControl("Run",      CreateControlWatchingKeyboard(SDL_SCANCODE_LSHIFT, -1));
    RegisterControl("Jump",     CreateControlWatchingKeyboard(SDL_SCANCODE_SPACE,  -1));

    //--Field Abilities
    RegisterControl("OpenFieldAbilityMenu", CreateControlWatchingKeyboard(SDL_SCANCODE_C,      -1));
    RegisterControl("FieldAbility0", CreateControlWatchingKeyboard(SDL_SCANCODE_1, -1));
    RegisterControl("FieldAbility1", CreateControlWatchingKeyboard(SDL_SCANCODE_2, -1));
    RegisterControl("FieldAbility2", CreateControlWatchingKeyboard(SDL_SCANCODE_3, -1));
    RegisterControl("FieldAbility3", CreateControlWatchingKeyboard(SDL_SCANCODE_4, -1));
    RegisterControl("FieldAbility4", CreateControlWatchingKeyboard(SDL_SCANCODE_5, -1));

    ///--[Mouse Buttons]
    RegisterControl("MouseLft", CreateControlWatchingMouse(SDL_BUTTON_LEFT,  -1));
    RegisterControl("MouseRgt", CreateControlWatchingMouse(SDL_BUTTON_RIGHT, -1));

    ///--[Witch Hunter Izana]
    RegisterControl("WHI Emergency Escape", CreateControlWatchingKeyboard(SDL_SCANCODE_END, -1));

    ///--[GUI Keys]
    RegisterControl("Shift",              CreateControlWatchingKeyboard(SDL_SCANCODE_LSHIFT, SDL_SCANCODE_RSHIFT));
    RegisterControl("Backspace",          CreateControlWatchingKeyboard(SDL_SCANCODE_BACKSPACE,               -1));
    RegisterControl("F1",                 CreateControlWatchingKeyboard(SDL_SCANCODE_F1,                      -1));
    RegisterControl("F2",                 CreateControlWatchingKeyboard(SDL_SCANCODE_F2,                      -1));
    RegisterControl("F3",                 CreateControlWatchingKeyboard(SDL_SCANCODE_F3,                      -1));
    RegisterControl("F4",                 CreateControlWatchingKeyboard(SDL_SCANCODE_F4,                      -1));
    RegisterControl("F5",                 CreateControlWatchingKeyboard(SDL_SCANCODE_F5,                      -1));
    RegisterControl("Enter",              CreateControlWatchingKeyboard(SDL_SCANCODE_RETURN,  SDL_SCANCODE_KP_ENTER));
    RegisterControl("Ctrl",               CreateControlWatchingKeyboard(SDL_SCANCODE_LCTRL,   SDL_SCANCODE_RCTRL));
    RegisterControl("Delete",             CreateControlWatchingKeyboard(SDL_SCANCODE_DELETE,                  -1));
    RegisterControl("Escape",             CreateControlWatchingKeyboard(SDL_SCANCODE_ESCAPE,                  -1));
    RegisterControl("ToggleFreeMovement", CreateControlWatchingKeyboard(SDL_SCANCODE_TAB,                     -1));
    RegisterControl("Quick Node",         CreateControlWatchingKeyboard(SDL_SCANCODE_F1,                      -1));
    RegisterControl("Reload Level",       CreateControlWatchingKeyboard(SDL_SCANCODE_F2,                      -1));
    RegisterControl("Build Lighting",     CreateControlWatchingKeyboard(SDL_SCANCODE_F3,                      -1));
    RegisterControl("Save Level Data",    CreateControlWatchingKeyboard(SDL_SCANCODE_F9,                      -1));
    RegisterControl("Load Level Data",    CreateControlWatchingKeyboard(SDL_SCANCODE_F10,                     -1));
    RegisterControl("ToggleFullscreen",   CreateControlWatchingKeyboard(SDL_SCANCODE_F11,                     -1));
    RegisterControl("PageUp",             CreateControlWatchingKeyboard(SDL_SCANCODE_PAGEUP,                  -1));
    RegisterControl("PageDn",             CreateControlWatchingKeyboard(SDL_SCANCODE_PAGEDOWN,                -1));
    RegisterControl("GUI|Up",             CreateControlWatchingKeyboard(SDL_SCANCODE_UP,                      -1));
    RegisterControl("GUI|Dn",             CreateControlWatchingKeyboard(SDL_SCANCODE_DOWN,                    -1));
    RegisterControl("GUI|Home",           CreateControlWatchingKeyboard(SDL_SCANCODE_HOME,                    -1));
    RegisterControl("GUI|End",            CreateControlWatchingKeyboard(SDL_SCANCODE_END,                     -1));
    RegisterControl("GUI|BackToTitle",    CreateControlWatchingKeyboard(SDL_SCANCODE_F8,                      -1));

    ///--[Numeric Keys]
    RegisterControl("Key_1", CreateControlWatchingKeyboard(SDL_SCANCODE_1, -1));
    RegisterControl("Key_2", CreateControlWatchingKeyboard(SDL_SCANCODE_2, -1));
    RegisterControl("Key_3", CreateControlWatchingKeyboard(SDL_SCANCODE_3, -1));
    RegisterControl("Key_4", CreateControlWatchingKeyboard(SDL_SCANCODE_4, -1));
    RegisterControl("Key_5", CreateControlWatchingKeyboard(SDL_SCANCODE_5, -1));
    RegisterControl("Key_6", CreateControlWatchingKeyboard(SDL_SCANCODE_6, -1));
    RegisterControl("Key_7", CreateControlWatchingKeyboard(SDL_SCANCODE_7, -1));
    RegisterControl("Key_8", CreateControlWatchingKeyboard(SDL_SCANCODE_8, -1));
    RegisterControl("Key_9", CreateControlWatchingKeyboard(SDL_SCANCODE_9, -1));
    RegisterControl("Key_0", CreateControlWatchingKeyboard(SDL_SCANCODE_0, -1));

    ///--[Debug Keys]
    //--Debug menu.
    RegisterControl("OpenDebug",    CreateControlWatchingKeyboard(SDL_SCANCODE_I,     -1));
    RegisterControl("CameraUnlock", CreateControlWatchingKeyboard(SDL_SCANCODE_LCTRL, -1));

    //--Shader Toggles
    RegisterControl("Shader0", CreateControlWatchingKeyboard(SDL_SCANCODE_0, -1));
    RegisterControl("Shader1", CreateControlWatchingKeyboard(SDL_SCANCODE_1, -1));
    RegisterControl("Shader2", CreateControlWatchingKeyboard(SDL_SCANCODE_2, -1));

    //--Loop Controls
    RegisterControl("LoopBack", CreateControlWatchingKeyboard(SDL_SCANCODE_LEFTBRACKET,  -1));
    RegisterControl("LoopForw", CreateControlWatchingKeyboard(SDL_SCANCODE_RIGHTBRACKET, -1));

    //--Kerning
    RegisterControl("RebuildKerning", CreateControlWatchingKeyboard(SDL_SCANCODE_F10, -1));
    RegisterControl("ToggleVisTrace", CreateControlWatchingKeyboard(SDL_SCANCODE_F6, -1));

    ///--[Peak Freaks]
    RegisterControl("PF Left",        CreateControlWatchingKeyboard(SDL_SCANCODE_LEFT,  -1));
    RegisterControl("PF Right",       CreateControlWatchingKeyboard(SDL_SCANCODE_RIGHT, -1));
    RegisterControl("PF Up",          CreateControlWatchingKeyboard(SDL_SCANCODE_UP,    -1));
    RegisterControl("PF Down",        CreateControlWatchingKeyboard(SDL_SCANCODE_DOWN,  -1));
    RegisterControl("PF Activate",    CreateControlWatchingKeyboard(SDL_SCANCODE_Z,     -1));
    RegisterControl("PF Crush",       CreateControlWatchingKeyboard(SDL_SCANCODE_X,     -1));
    RegisterControl("PF Cancel",      CreateControlWatchingKeyboard(SDL_SCANCODE_C,     -1));
    RegisterControl("PF Debug Win",   CreateControlWatchingKeyboard(SDL_SCANCODE_F1,    -1));
    RegisterControl("PF Debug Lose",  CreateControlWatchingKeyboard(SDL_SCANCODE_F2,    -1));
    RegisterControl("PF Kill Rival",  CreateControlWatchingKeyboard(SDL_SCANCODE_F3,    -1));

    ///--[Slitted Eye]
    //--Game controls.
    RegisterControl("SE Manipulate", CreateControlWatchingMouse(SDL_BUTTON_LEFT, -1));
    RegisterControl("SE Hiss", CreateControlWatchingMouse(SDL_BUTTON_RIGHT, -1));
    RegisterControl("SE Game Over", CreateControlWatchingKeyboard(SDL_SCANCODE_SPACE, -1));

    ///--[Witches of Ravenbrook]
    RegisterControl("ScndUp"   ,   CreateControlWatchingKeyboard(SDL_SCANCODE_W, -1));
    RegisterControl("ScndDown" ,   CreateControlWatchingKeyboard(SDL_SCANCODE_S, -1));
    RegisterControl("ScndLeft" ,   CreateControlWatchingKeyboard(SDL_SCANCODE_A, -1));
    RegisterControl("ScndRight",   CreateControlWatchingKeyboard(SDL_SCANCODE_D, -1));
}
#endif
