//--Base
#include "ActorNotice.h"

//--Classes
//--CoreClasses
#include "StarFont.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

///--[Local Definitions]
#define AN_FADE_IN_TICKS 15
#define AN_FADE_OUT_TICKS 15
#define AN_FADE_OUT_RISE -15.0f

///========================================== System ==============================================
ActorNotice::ActorNotice()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_ACTORNOTICE;

    ///--[ActorNotice]
    //--System
    mTimer = 0;
    mTimerMax = 1;

    //--Display
    mText = NULL;
    rRenderFont = NULL;

    //--Position
    mX = 0.0f;
    mY = 0.0f;

    //--Moving
    mMoveTimer = 1;
    mMoveTimerMax = 1;
    mStrtX = 0.0f;
    mStrtY = 0.0f;
    mDestX = 0.0f;
    mDestY = 0.0f;

    ///--[Construction]
    //--Resolve the font.
    rRenderFont = DataLibrary::Fetch()->GetFont("Adventure Mug");
}
ActorNotice::~ActorNotice()
{
    free(mText);
}

///===================================== Property Queries =========================================
bool ActorNotice::IsExpired()
{
    if(mTimer >= mTimerMax) return true;
    return false;
}
float ActorNotice::GetTargetX()
{
    return mDestX;
}
float ActorNotice::GetTargetY()
{
    return mDestY;
}
float ActorNotice::GetWidth()
{
    if(!rRenderFont || !mText) return 0.0f;
    return rRenderFont->GetTextWidth(mText);
}
float ActorNotice::GetHeight()
{
    if(!rRenderFont) return 0.0f;
    return rRenderFont->GetTextHeight();
}

///======================================= Manipulators ===========================================
void ActorNotice::SetTimerMax(int pTicks)
{
    mTimerMax = pTicks;
    if(mTimerMax < 1) mTimerMax = 1;
}
void ActorNotice::SetText(const char *pText)
{
    ResetString(mText, pText);
}
void ActorNotice::MoveTo(float pDestX, float pDestY, int pMoveTicks)
{
    //--Set all flags.
    mMoveTimer = 0;
    mMoveTimerMax = pMoveTicks;
    mStrtX = mX;
    mStrtY = mY;
    mDestX = pDestX;
    mDestY = pDestY;

    //--If the move timer max is 0, instantly move to the target.
    if(mMoveTimerMax < 1)
    {
        mMoveTimer = 1;
        mMoveTimerMax = 1;
        mX = pDestX;
        mY = pDestY;
    }
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void ActorNotice::Update()
{
    //--Run the timer.
    mTimer ++;

    //--During the last AN_FADE_OUT_TICKS ticks of the object's lifetime, it does not move.
    int tTicksLeft = mTimerMax - mTimer;
    if(tTicksLeft < AN_FADE_OUT_TICKS) return;

    //--Move to the destination if ordered to move.
    if(mMoveTimer < mMoveTimerMax)
    {
        //--Timer.
        mMoveTimer ++;

        //--Compute.
        float cPercent = EasingFunction::QuadraticOut(mMoveTimer, mMoveTimerMax);
        mX = mStrtX + ((mDestX - mStrtX) * cPercent);
        mY = mStrtY + ((mDestY - mStrtY) * cPercent);
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void ActorNotice::Render()
{
    //--If the font did not resolve, don't render anything.
    if(!rRenderFont || !mText) return;

    //--Variables.
    float cAlpha = 1.0f;
    float cVOff = 0.0f;

    //--Compute alpha, and vertical offset.
    int tTicksLeft = mTimerMax - mTimer;
    if(mTimer < AN_FADE_IN_TICKS)
    {
        cAlpha = EasingFunction::QuadraticOut(mTimer, AN_FADE_IN_TICKS);
    }
    else if(tTicksLeft < AN_FADE_OUT_TICKS)
    {
        float cPercent = EasingFunction::QuadraticOut(tTicksLeft, AN_FADE_OUT_TICKS);
        cAlpha = cPercent;
        cVOff = AN_FADE_OUT_RISE * (1.0f - cPercent);
    }

    //--If the alpha is zero, don't render.
    if(cAlpha <= 0.0f) return;

    //--Render.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    rRenderFont->DrawText(mX, mY + cVOff, SUGARFONT_AUTOCENTER_XY, 0.5f, mText);
    StarlightColor::ClearMixer();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
