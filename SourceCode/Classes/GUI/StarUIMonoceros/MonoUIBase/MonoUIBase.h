///======================================== MonoUIBase ============================================
//--Base, handles the grid. This is a 3x3 interface that calls other components. In Monoceros it's
//  almost the same as Adventure but the grid layout is different and the visual parts are changed.

#pragma once

///========================================= Includes =============================================
#include "AdvUIBase.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///======================================== Translation ===========================================
///--[MonoUIBase_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIBASE_STRINGS_TOTAL 30
class MonoUIBase_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIBASE_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextMainMenu;
            char *mTextPlatina;
            char *mTextAdamantite;
            char *mTextPowder;
            char *mTextFlakes;
            char *mTextShards;
            char *mTextPieces;
            char *mTextChunks;
            char *mTextOre;
            char *mTextHelpMenu;
            char *mTextEquipment;
            char *mTextItems;
            char *mTextStatus;
            char *mTextSkills;
            char *mTextMap;
            char *mTextOptions;
            char *mTextJournal;
            char *mTextQuit;
            char *mTextSwitchMenu;
            char *mTextHelp[10];
            char *mTextControlShowHelp;
        }str;
    };

    //--Functions
    MonoUIBase_Strings();
    virtual ~MonoUIBase_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIBase : public MonoUICommon, virtual public AdvUIBase
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxMonoHelpStrings = 10;

    ///--[Variables]
    //--System
    bool mIsSwitchMode;
    bool mIsReplaceMode;
    int mSwitchTimer;
    int mSwitchCharCursor;
    int mSwitchReplaceCursor;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rDoubleHeadingFont;
        StarFont *rHeadingFont;
        StarFont *rMainlineFont;
        StarFont *rButtonFont;
        StarFont *rHealthFont;
        StarFont *rHelpFont;
        StarFont *rFont_Control;

        //--Images
        StarBitmap *rButton;
        StarBitmap *rButtonSel;
        StarBitmap *rFrameButtonCancel;
        StarBitmap *rFrameButtonCancelSel;
        StarBitmap *rFrameButtonToTitle;
        StarBitmap *rFrameButtonToTitleSel;
        StarBitmap *rFrameExit;
        StarBitmap *rFramesLft;
        StarBitmap *rFramesRgt;
        StarBitmap *rFrameHelp;
        StarBitmap *rFrameWarpLocation;
        StarBitmap *rHealthFront;
        StarBitmap *rOverlayWarpSelect;
        StarBitmap *rOverlaySwitchSelect;
        StarBitmap *rTitleBack;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIBase();
    virtual ~MonoUIBase();
    virtual void Construct();
    virtual void RunTranslation();

    //--Public Variables
    static MonoUIBase_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    virtual void SetCursorByBackwardsCode(int pMode);
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();

    ///--[Switch Mode]
    void SwitchActiveParty(int pSlotA, int pSlotB);
    void UpdateMainMenuSwitch();
    void RenderMainMenuSwitch(float pAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    virtual void RenderMainCharacter(int pSlot, int pPartySize);
    virtual void RenderMainCharacterUI(int pSlot, int pPartySize, float pVisAlpha);
    virtual void RenderGridEntryIcon(GridPackage *pEntry, float pVisAlpha);
    virtual void RenderGridEntryName(GridPackage *pEntry, float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
