//--Base
#include "AdvUIInventory.h"

//--Classes
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== Update ==============================================
void AdvUIInventory::UpdateDeconstruct()
{
    ///--[Documentation]
    //--Deconstruction mode. Acts as a confirm/cancel to deconstruct an item the player has selected.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    ///--[Control Handling]
    //--Activate. Deconstructs the item.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Flag, SFX.
        mIsDeconstructMode = false;

        //--If either error flag is set, do nothing:
        if(mDeconstructErrorEquipped || mDeconstructErrorNoResults)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Otherwise, remove the item in question and add new items.
        else
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

            //--Fetch.
            AdventureInventory *rInventory = AdventureInventory::Fetch();

            //--Remove.
            rInventory->RemoveItem(mStoredItemName);

            //--Create the requested items.
            void *rPtr = mDeconstructResults->PushIterator();
            while(rPtr)
            {
                //--Render.
                const char *rItemName = mDeconstructResults->GetIteratorName();
                LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rItemName);

                //--Next.
                rPtr = mDeconstructResults->AutoIterate();
            }

            //--Reconstitute the extended item list.
            rInventory->BuildExtendedItemList();

            //--Recheck the cursor in case it went off the end of the list.
            int tNewListSize = rInventory->GetExtendedItemList()->GetListSize();
            if(mItemCursor >= tNewListSize) mItemCursor = tNewListSize;
            RecomputeCursorPositions();
        }

        //--Stop update.
        return;
    }

    //--Cancel. Exits deconstruct mode.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsDeconstructMode = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///========================================== Drawing =============================================
void AdvUIInventory::RenderDeconstruct(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the deconstruction overlay. This is a window and some text showing what item is getting
    //  deconstructed and what it will break down into.
    if(mDeconstructTimer < 1 || pVisAlpha <= 0.0f) return;

    ///--[Backing]
    //--Compute timer percent.
    float cPct = EasingFunction::QuadraticInOut(mDeconstructTimer, cxDeconstructTicks);

    //--Darken the background.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cPct));

    ///--[Window and Text]
    //--Position offset.
    float cYPos = (1.0f - cPct) * 700.0f;
    glTranslatef(0.0f, cYPos, 0.0f);

    //--Window.
    AdvImages.rFrame_DeconstructBox->Draw();

    //--Text if the item is equipped and threw an error.
    if(mDeconstructErrorEquipped)
    {
        AdvImages.rFont_Heading ->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 171.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Cannot Deconstruct");
        AdvImages.rFont_Mainline->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 208.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equipped items cannot be deconstructed.");
    }
    //--Text if the item cannot be deconstructed (or the script otherwise didn't handle it).
    else if(mDeconstructErrorNoResults)
    {
        AdvImages.rFont_Heading ->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 171.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Cannot Deconstruct");
        AdvImages.rFont_Mainline->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 208.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "This item does not break into anything else.");
    }
    //--Normal text.
    else
    {
        //--Get the item's name.
        //AdventureInventory *rInventory = AdventureInventory::Fetch();
        //StarLinkedList *rItemList = rInventory->GetExtendedItemList();
        //AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);

        //--Render it as the title.
        AdvImages.rFont_Heading ->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 171.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Deconstruct %s?", mStoredItemName);

        //--Setup.
        float cLft = 368.0f;
        //float cRgt = 656.0f;
        float cXTxt = cLft;
        float cYTxt = 208.0f;
        float cYHei = AdvImages.rFont_Mainline->GetTextHeight();

        //--List of items it will yield.
        void *rPtr = mDeconstructResults->PushIterator();
        while(rPtr)
        {
            //--Render.
            const char *rItemName = mDeconstructResults->GetIteratorName();
            AdvImages.rFont_Mainline->DrawTextArgs(cXTxt, cYTxt, 0, 1.0f, "%s", rItemName);

            //--Next.
            cYTxt = cYTxt + cYHei;
            rPtr = mDeconstructResults->AutoIterate();
        }

        //--Help strings.
        mConfirmString->DrawText(368.0f, 312.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mCancelString-> DrawText(865.0f, 312.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Clean]
    glTranslatef(0.0f, -cYPos, 0.0f);
}
