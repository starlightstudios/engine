-- |[ ======================================== System UI ======================================= ]|
--UI Images used by the main menu. This includes baseline backgrounds.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/UISystem.slf")
ImageLump_SetCompression(1)

-- |[Special Images]|
ImageLump_Rip("WhitePixel",  sBasePath .. "WhitePixel.png", 0, 0, -1, -1, 0)

-- |[Main Menu]|
--Background and Title
ImageLump_Rip("MenuBG",      sBasePath .. "MainMenu.png",     0, 0, -1, -1, 0)
ImageLump_Rip("MenuBGOver",  sBasePath .. "MainMenuOver.png", 0, 0, -1, -1, 0)

--Text Rendering/Borders
ImageLump_Rip("DownArrow",   sBasePath .. "DownArrow.png",  0, 0, -1, -1, 0)
ImageLump_Rip("UpArrow",     sBasePath .. "UpArrow.png",    0, 0, -1, -1, 0)
ImageLump_Rip("CardBorder",  sBasePath .. "CardBorder.png", 0, 0, -1, -1, 0)

-- |[Program Icon and Fonts]|
--Cannot allow compression since it must be uploaded to an ALLEGRO_BITMAP or SDL_SURFACE. No complex stuff!
ImageLump_SetAllowLineCompression(false)
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("UIFontSmall",  sBasePath .. "UIFontEdged.png",       0, 0, -1, -1, 0)
ImageLump_Rip("UIFontVSmall", sBasePath .. "UIFontEdgedVSmall.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Icon",         sBasePath .. "Icon.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)
ImageLump_SetAllowLineCompression(true)

-- |[Subfolders]|
LM_ExecuteScript(sBasePath .. "StringEntry/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "MonoStringEntry/ZRouting.lua")

--Finish
SLF_Close()