//--Base
#include "MonoUIVendor.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"

///========================================== Update ==============================================
void MonoUIVendor::UpdateModeGemsMerge()
{
    ///--[Documentation]
    //--Handles update for the buy mode, which is mostly controls. Common timers are handled
    //  by the UpdateVendor() function which calls this one.
    //--Identical to the base version but changes the scrollbar sizings.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rGemsList = rInventory->GetGemList();

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(CommonHelpUpdate()) return;

    ///--[Merge Confirm]
    //--Allows the player to accept or cancel the pending merge.
    if(mIsConfirmMergeMode)
    {
        //--Activate. Merges gems.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Flag.
            mIsConfirmMergeMode = false;

            //--Fast-access pointers.
            StarLinkedList *rGemList = rInventory->GetGemList();

            //--Get the merge candidate.
            AdventureItem *rMergeCandidate = (AdventureItem *)rGemList->GetElementBySlot(mCursor);
            if(!mMergeGem || !rMergeCandidate) return;

            //--Compute total tier, used for costs.
            int tTotalTier = mMergeGem->GetRank() + rMergeCandidate->GetRank();

            //--Subtract platina.
            int tPlatinaCost = AdventureInventory::ComputeMonocerosGemUpgradePlatinaCost(tTotalTier);
            rInventory->SetPlatina(rInventory->GetPlatina() - tPlatinaCost);

            //--Subtract adamantite.
            for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
            {
                int tAdamantiteCost = AdventureInventory::ComputeGemUpgradeCost(tTotalTier, i);
                rInventory->SetCraftingMaterial(i, rInventory->GetCraftingCount(i) - tAdamantiteCost);
            }

            //--Merge the gems together.
            mMergeGem->MergeWithGem(rMergeCandidate);
            mMergeGem->ProcessDescription();

            //--Does the candidate have an owner? If so, we need to unequip it from whatever slot it was in.
            int tMergeSlot = rGemList->GetSlotOfElementByPtr(rMergeCandidate);
            const char *rOwnerName = rGemList->GetNameOfElementBySlot(tMergeSlot);
            if(strcasecmp(rOwnerName, "Null"))
            {
                //--Locate the owner.
                AdvCombatEntity *rOwner = AdvCombat::Fetch()->GetActiveMemberS(rOwnerName);
                if(!rOwner)
                {
                    rGemList->RemoveElementP(rMergeCandidate);
                    DebugManager::ForcePrint("AdventureMenu::UpdateVendorGemsMerge() - Error, gem owner was somehow not in the party.\n");
                    return;
                }

                //--Order the owner to unsocket the gem.
                rOwner->RemoveGemFromEquipment(rMergeCandidate);
            }
            //--The gem was in the base inventory, so just remove it from there. We do not want it deallocated.
            else
            {
                rInventory->LiberateItemP(rMergeCandidate);
            }

            //--Remove the newly merged gem from the gem list. This will not deallocate it.
            rGemList->RemoveElementP(rMergeCandidate);

            //--If the gem list now has zero elements, exit merge mode. This is the same as pushing cancel during gem selection.
            if(rGemList->GetListSize() < 1)
            {
                //--Take the merge gem and return it to the gems list.
                rGemList->AddElementAsHead(mMergeGemOwner, mMergeGem);

                //--Reset.
                ResetString(mMergeGemOwner, NULL);
                mMergeGem = NULL;

                //--Flags.
                mIsMergeMode = false;

                //--Reset cursor.
                mCursor = 0;
                mSkip = 0;
                RecomputeCursorPositions();
                mHighlightPos.Complete();
                mHighlightSize.Complete();
            }
            //--Otherwise, check if the cursor is past the end of the list.
            else if(mCursor >= rGemList->GetListSize())
            {
                mCursor --;
                RecomputeCursorPositions();
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
        }

        //--Cancel. Exits confirm mode.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsConfirmMergeMode = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return;
    }

    ///--[Up and Down]
    //--Increments/decrements on the gem selection list. Wrapping is supported.
    if(AutoListUpDn(mCursor, mSkip, rGemsList->GetListSize(), cScrollBuf, cxMonoScrollGemPageSize, true))
    {
        RecomputeCursorPositions();
        return;
    }

    ///--[Activate]
    //--Begin merging procedure.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get the gem in question.
        AdventureItem *rMergeCandidate = (AdventureItem *)rInventory->GetGemList()->GetElementBySlot(mCursor);
        if(!rMergeCandidate) return;

        //--Gems are incompatible.
        if(!rMergeCandidate->CanBeMerged(mMergeGem))
        {
            fprintf(stderr, "Gems are not compatible.\n");
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Compute the total gem tier.
        int tTotalTier = rMergeCandidate->GetRank() + mMergeGem->GetRank();
        int tPlatinaCost = AdventureInventory::ComputeMonocerosGemUpgradePlatinaCost(tTotalTier);
        if(rInventory->GetPlatina() < tPlatinaCost)
        {
            fprintf(stderr, "Not enough cash. Have %i, need %i\n", rInventory->GetPlatina(), tPlatinaCost);
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Check adamantite costs.
        for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
        {
            int tCost = AdventureInventory::ComputeGemUpgradeCost(tTotalTier, i);
            if(tCost > rInventory->GetCraftingCount(i))
            {
                fprintf(stderr, "Needs adamantite %i.\n", i);
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }
        }

        //--All checks passed, bring up the confirmation screen.
        mIsConfirmMergeMode = true;
    }

    ///--[Cancel]
    //--Exit this UI.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Take the merge gem and return it to the gems list.
        AdventureInventory::Fetch()->GetGemList()->AddElementAsHead(mMergeGemOwner, mMergeGem);

        //--Reset.
        ResetString(mMergeGemOwner, NULL);
        mMergeGem = NULL;

        //--Flags.
        mIsMergeMode = false;

        //--Reset cursor.
        mCursor = 0;
        mSkip = 0;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();

        //--Switch back to basic help menu.
        RefreshGemMenuHelp();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}

///===================================== Segment Rendering ========================================
void MonoUIVendor::RenderTopGemsMerge(float pVisAlpha)
{
    ///--[Position]
    //--Merge mode always renders at full alpha if active.
    if(!mIsMergeMode) return;
    pVisAlpha = 1.0f;

    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    //--Constants.
    float cGemScale = 1.50f;
    float cGemScaleInv = 1.0f / cGemScale;

    ///--[Render]
    //--Note: Header is rendered by base.

    //--Gem Name Frame.
    MonoImages.rFrame_GemListing->Draw();
    AdvImages.rFrame_GemsContents->Draw();

    //--Get the gem's name and icon.
    if(mMergeGem)
    {
        //--Icon. Renders at 150% size.
        StarBitmap *rGemIcon = mMergeGem->GetIconImage();
        if(rGemIcon)
        {
            glTranslatef(109.0f, 115.0f, 0.0f);
            glScalef(cGemScale, cGemScale, 1.0f);
            rGemIcon->Draw();
            glScalef(cGemScaleInv, cGemScaleInv, 1.0f);
            glTranslatef(-109.0f, -115.0f, 0.0f);
        }

        //--Name.
        AdvImages.rFont_Heading->DrawText(140.0f, 106.0f, 0, 1.0f, mMergeGem->GetDisplayName());
    }

    //--Render which colors the gem contains.
    if(mMergeGem)
    {
        //--Darkening color.
        StarlightColor cGemDarken;
        cGemDarken.SetRGBAF(0.20f, 0.20f, 0.20f, cColorAlpha);

        //--Get color flag.
        uint8_t tCurCol = 0x01;
        uint8_t tColorFlag = mMergeGem->GetGemColors();

        //--Position constants.
        float cGemX = 612.0f;
        float cGemY = 121.0f;
        float cGemW =  55.0f;

        //--Loop.
        for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
        {
            //--If this color is present:
            if(tColorFlag & tCurCol)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);
            }
            else
            {
                cGemDarken.SetAsMixer();
            }

            //--Compute X.
            float cRenderX = cGemX + (cGemW * i);

            //--Position. Render at 1.5x scale.
            glTranslatef(cRenderX, cGemY, 0.0f);
            glScalef(cGemScale, cGemScale, 1.0f);
            AdvImages.rGemIcons[i]->Draw();
            glScalef(cGemScaleInv, cGemScaleInv, 1.0f);
            glTranslatef(-cRenderX, -cGemY, 0.0f);

            //--Next.
            tCurCol = tCurCol* 2;
        }

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderRgtGemsMerge(float pVisAlpha)
{
    //--Merge mode always renders at full alpha if active.
    if(!mIsMergeMode) return;
    AdvUIVendor::RenderRgtGemsMerge(1.0f);
}
void MonoUIVendor::RenderBotGemsMerge(float pVisAlpha)
{
    ///--[Position]
    //--Merge mode always renders at full alpha if active.
    if(!mIsMergeMode) return;
    pVisAlpha = 1.0f;

    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Fast-access pointers.
    StarLinkedList *rGemList = AdventureInventory::Fetch()->GetGemList();

    ///--[Render]
    //--Inventory frame.
    MonoImages.rFrame_GemListing->Draw();

    //--Titles
    SetColor("Title", cColorAlpha);
    AdvImages.rFont_Heading->DrawText(206.0f, 190.0f, 0, 1.0f, xrStringData->str.mTextName);
    AdvImages.rFont_Heading->DrawText(503.0f, 190.0f, 0, 1.0f, xrStringData->str.mTextTier);
    AdvImages.rFont_Heading->DrawText(752.0f, 190.0f, 0, 1.0f, xrStringData->str.mTextColors);
    SetColor("Clear", cColorAlpha);

    //--Backings. Rendered apart from the entries, allowing the cursor to be over the backing and under
    //  the text entries.
    float cEntrySpcH = 43.0f;
    int tBackingsToRender = rGemList->GetListSize();
    if(tBackingsToRender > cxMonoScrollGemPageSize) tBackingsToRender = cxMonoScrollGemPageSize;
    for(int i = 0; i < tBackingsToRender; i ++)
    {
        MonoImages.rOverlay_GemList->Draw(0.0f, i * cEntrySpcH);
    }

    //--Cursor.
    MonoImages.rOverlay_GemSelect->Draw(0.0f, mHighlightPos.mYCur - MonoImages.rOverlay_GemSelect->GetYOffset());

    //--List of other gems. Cost is shown when highlighting.
    RenderGemsMergeListing(mSkip, cxMonoScrollGemPageSize, cColorAlpha);

    //--Scrollbar.
    if(rGemList->GetListSize() > cxMonoScrollGemPageSize)
    {
        StandardRenderScrollbar(mSkip, cxMonoScrollGemPageSize, rGemList->GetListSize() - cxMonoScrollGemPageSize, 219.0f, 480.0f, false, AdvImages.rScrollbar_Scroller, MonoImages.rScrollbar_GemsStatic);
    }

    ///--[Help Strings]
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mShowHelpString->   DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderUnalignedGemsMerge(float pVisAlpha)
{
    //--Merge mode always renders at full alpha if active.
    if(!mIsMergeMode) return;
    AdvUIVendor::RenderUnalignedGemsMerge(1.0f);
}

///===================================== Common Rendering =========================================
void MonoUIVendor::RenderGemsMergeAdamantite(float pColorAlpha)
{
    ///--[Documentation]
    //--Given the selected gem and a to-merge gem, renders the player's adamantite counts plus any costs
    //  that are calculated.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Basics]
    //--Frame.
    AdvImages.rFrame_GemsAdamantite->Draw();

    //--Title.
    SetColor("Title", pColorAlpha);
    AdvImages.rFont_Heading->DrawText(1198.0f, 103.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextAdamantite);
    SetColor("Clear", pColorAlpha);

    //--Icons and Names
    float cAdamantiteIconX = 1054.0f;
    float cAdamantiteTextX = 1076.0f;
    float cAdamantiteTextR = 1255.0f;
    float cAdamantiteMinsX = 1273.0f;
    float cAdamantiteCostR = 1310.0f;
    float cAdamantiteIconY =  156.0f;
    float cAdamantiteIconH =   30.0f;
    int cFlagsL = SUGARFONT_NOCOLOR;
    int cFlagsR = SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X;

    //--Backings. We re-use the material shade since it's the same size and needs an offset.
    for(int i = 0; i < 6; i ++)
    {
        MonoImages.rOverlay_MaterialShade->Draw(53.0f, -43.0f + (i * cAdamantiteIconH));
    }

    //--Render data.
    AdvImages.rAdamantiteIcons[0]->Draw    (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 0.0f) + 0.0f);
    AdvImages.rFont_Statistic->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 0.0f) - 2.0f, cFlagsL, 1.0f, xrStringData->str.mTextPowder);
    AdvImages.rFont_Statistic->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 0.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_POWDER));
    AdvImages.rAdamantiteIcons[1]->Draw    (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 1.0f) + 0.0f);
    AdvImages.rFont_Statistic->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 1.0f) - 2.0f, cFlagsL, 1.0f, xrStringData->str.mTextFlakes);
    AdvImages.rFont_Statistic->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 1.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_FLAKES));
    AdvImages.rAdamantiteIcons[2]->Draw    (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 2.0f) + 0.0f);
    AdvImages.rFont_Statistic->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 2.0f) - 2.0f, cFlagsL, 1.0f, xrStringData->str.mTextShards);
    AdvImages.rFont_Statistic->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 2.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_SHARD));
    AdvImages.rAdamantiteIcons[3]->Draw    (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 3.0f) + 0.0f);
    AdvImages.rFont_Statistic->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 3.0f) - 2.0f, cFlagsL, 1.0f, xrStringData->str.mTextPieces);
    AdvImages.rFont_Statistic->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 3.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_PIECE));
    AdvImages.rAdamantiteIcons[4]->Draw    (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 4.0f) + 0.0f);
    AdvImages.rFont_Statistic->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 4.0f) - 2.0f, cFlagsL, 1.0f, xrStringData->str.mTextChunks);
    AdvImages.rFont_Statistic->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 4.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_CHUNK));
    AdvImages.rAdamantiteIcons[5]->Draw    (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 5.0f) + 0.0f);
    AdvImages.rFont_Statistic->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 5.0f) - 2.0f, cFlagsL, 1.0f, xrStringData->str.mTextOre);
    AdvImages.rFont_Statistic->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 5.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_ORE));

    ///--[Upgrade]
    //--If the highlighted gem is a valid merge target, render what it will cost in terms of adamantite.
    StarLinkedList *rGemList = rInventory->GetGemList();
    AdventureItem *rHighlightGem = (AdventureItem *)rGemList->GetElementBySlot(mCursor);
    if(!rHighlightGem || !mMergeGem) return;

    //--Gem is incompatible with the selected gem.
    if(!rHighlightGem->CanBeMerged(mMergeGem)) return;

    //--Get the total gem tier or rank.
    int tTotalTier = rHighlightGem->GetRank() + mMergeGem->GetRank();

    //--For each adamantite type, render.
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        //--Get the cost at this thier.
        int tCost = AdventureInventory::ComputeGemUpgradeCost(tTotalTier, i);
        if(tCost < 1) continue;

        //--Cost is nonzero, so render it.
        AdvImages.rFont_Statistic->DrawText    (cAdamantiteMinsX, cAdamantiteIconY + (cAdamantiteIconH * i) - 2.0f, cFlagsL, 1.0f, "-");
        AdvImages.rFont_Statistic->DrawTextArgs(cAdamantiteCostR, cAdamantiteIconY + (cAdamantiteIconH * i) - 2.0f, cFlagsR, 1.0f, "%3i", tCost);
    }
}
void MonoUIVendor::RenderGemsMergeComparison(float pColorAlpha)
{
    ///--[Documentation]
    //--Given the selected gem and a to-merge gem, renders the base properties of the gem and what
    //  those would change to if the gems were merged.

    ///--[Base Gem]
    //--Frame.
    SetColor("Clear", pColorAlpha);
    MonoImages.rFrame_GemProperties->Draw();

    //--Title.
    SetColor("Subtitle", pColorAlpha);
    AdvImages.rFont_Heading->DrawText(1198.0f, 384.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextProperties);
    SetColor("Clear", pColorAlpha);

    //--Constants.
    //float cLft     = 1044.0f;
    //float cRgt     = 1366.0f;
    float cPropX   = 1110.0f;
    float cResistX = 1110.0f;
    float cDamageX = 1270.0f;
    float cIconX   = 1060.0f;
    float cIconY   =  440.0f;
    float cIconH   =   30.0f;

    ///--[Common Backing]
    //--Even if there's no merge gem, render the icons.
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));
    }

    //--When fading out, the merge gem stops existing. We instead render the icons and backings, and then stop.
    if(!mMergeGem)
    {
        return;
    }

    ///--[Merge Gem Properties]
    //--Icons and values.
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        //--Backing.
        MonoImages.rOverlay_MaterialShade->Draw(53.0f, 237.0f + (i * cIconH));

        ///--[Simple Statistics]
        //--If this is a statistic, render a single value.
        if(i < MONOUIVEN_RESISTANCE_BEGIN)
        {
            //--Value.
            int tValue = mMergeGem->GetStatistic(mStatisticIndexes[i]);
            AdvImages.rFont_Statistic->DrawTextArgs(cPropX, cIconY + (cIconH * i) - 4.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
        }
        ///--[Resistance and Damage]
        //--Shows two values. Resistance, and Damage Bonus.
        else
        {
            //--Get all values.
            int tResistance = mMergeGem->GetStatistic(mStatisticIndexes[i]);
            int tBonusTags = mMergeGem->GetTagCount(mBonusTagNames[i]);
            int tMalusTags = mMergeGem->GetTagCount(mMalusTagNames[i]);

            //--Resistance Value
            AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cIconY + (cIconH * i) - 4.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistance);

            //--Damage Bonus
            int tDamageValue = 100 + tBonusTags - tMalusTags;
            if(tDamageValue < 0) tDamageValue = 0;

            //--Render. Only renders when not replacing.
            AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cIconY + (cIconH * i) - 4.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tDamageValue);
        }
    }

    ///--[Merge Properties]
    //--If the gem in question got merged in, it'd change the properties. Show those.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rGemList = rInventory->GetGemList();
    AdventureItem *rHighlightGem = (AdventureItem *)rGemList->GetElementBySlot(mCursor);
    if(!rHighlightGem) return;

    //--Constants.
    float cCompX    = 1182.0f;
    float cResCompX = 1182.0f;
    float cDamCompX = 1347.0f;

    //--Icons and values.
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        ///--[Simple Statistics]
        //--If this is a statistic, render a single value.
        if(i < MONOUIVEN_RESISTANCE_BEGIN)
        {
            //--Values.
            int tOldValue = mMergeGem->GetStatistic(mStatisticIndexes[i]);
            int tNewValue = rHighlightGem->GetStatistic(mStatisticIndexes[i]);

            //--Value is lower:
            if(tNewValue < 0)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
            }
            //--Higher:
            else if(tNewValue > 0)
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
            }
            else
            {
                continue;
            }

            //--Render.
            AdvImages.rFont_Statistic->DrawTextArgs (cPropX + 15, cIconY + (cIconH * i) - 4.0f, 0, 1.0f, "->");
            AdvImages.rFont_Statistic->DrawTextArgs(cCompX,      cIconY + (cIconH * i) - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tOldValue + tNewValue);
        }
        ///--[Resistance and Damage]
        //--Shows two values. Resistance, and Damage Bonus.
        else
        {
            ///--[Setup]
            //--Get all values.
            int tOldResistance = mMergeGem->GetStatistic(mStatisticIndexes[i]);
            int tOldBonusTags  = mMergeGem->GetTagCount(mBonusTagNames[i]);
            int tOldMalusTags  = mMergeGem->GetTagCount(mMalusTagNames[i]);
            int tNewResistance = rHighlightGem->GetStatistic(mStatisticIndexes[i]);
            int tNewBonusTags  = rHighlightGem->GetTagCount(mBonusTagNames[i]);
            int tNewMalusTags  = rHighlightGem->GetTagCount(mMalusTagNames[i]);

            ///--[Resistance]
            //--Value is lower:
            if(tNewResistance < 0)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
                AdvImages.rFont_Statistic->DrawTextArgs(cResistX + 15, cIconY + (cIconH * i) - 4.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistic->DrawTextArgs(cResCompX,     cIconY + (cIconH * i) - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tOldResistance + tNewResistance);
            }
            //--Higher:
            else if(tNewResistance > 0)
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
                AdvImages.rFont_Statistic->DrawTextArgs(cResistX + 15, cIconY + (cIconH * i) - 4.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistic->DrawTextArgs(cResCompX,     cIconY + (cIconH * i) - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tOldResistance + tNewResistance);
            }

            ///--[Damage Bonus]
            //--Compute damage values.
            int tOldDamageValue = tOldBonusTags - tOldMalusTags;
            if(tOldDamageValue < 0) tOldDamageValue = 0;
            int tNewDamageValue = tNewBonusTags - tNewMalusTags;
            if(tNewDamageValue < 0) tNewDamageValue = 0;

            //--Value is lower:
            if(tNewDamageValue < 0)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
                AdvImages.rFont_Statistic->DrawTextArgs(cDamageX + 7, cIconY + (cIconH * i) - 4.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistic->DrawTextArgs(cDamCompX,    cIconY + (cIconH * i) - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tOldDamageValue + tNewDamageValue + 100);
            }
            //--Higher:
            else if(tNewDamageValue > 0)
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
                AdvImages.rFont_Statistic->DrawTextArgs(cDamageX + 7, cIconY + (cIconH * i) - 4.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistic->DrawTextArgs(cDamCompX,    cIconY + (cIconH * i) - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tOldDamageValue + tNewDamageValue + 100);
            }
        }

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    }
}
void MonoUIVendor::RenderGemsMergeItem(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--When the player chooses a gem to merge, this appears to confirm the merge.
    if(mGemConfirmMergeTimer < 1) return;

    ///--[Position]
    //--Percentage.
    float cMergePct = EasingFunction::QuadraticInOut(mGemConfirmMergeTimer, cxAdvAskMergeTicks) * pVisAlpha;

    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, cMergePct);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cMergePct * 0.7f));

    ///--[Item Resolve]
    //--Get the item package.
    StarLinkedList *rGemList = AdventureInventory::Fetch()->GetGemList();
    AdventureItem *rMergeCandidate = (AdventureItem *)rGemList->GetElementBySlot(mCursor);

    ///--[Rendering]
    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--All information specific to the item is not shown when hiding the UI.
    if(rMergeCandidate && mMergeGem && mIsConfirmMergeMode)
    {
        //--Heading.
        SetColor("Heading", cColorAlpha);
        AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextConfirmMerge);
        SetColor("Clear", cColorAlpha);

        //--Centering computations.
        char *tTextBuf = InitializeString(xrStringData->str.mTextWillMergeWith, mMergeGem->GetDisplayName());
        float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tTextBuf);
        float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

        //--Item Icon
        StarBitmap *rItemIcon = mMergeGem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 215.0f);

        //--Second Icon.
        rItemIcon = rMergeCandidate->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 237.0f);

        //--Item name.
        AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, tTextBuf);
        AdvImages.rFont_Mainline->DrawText    (cTextLft, 227.0f, 0, 1.0f, rMergeCandidate->GetDisplayName());
        free(tTextBuf);
    }

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(890.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
