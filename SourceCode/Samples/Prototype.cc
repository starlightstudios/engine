//--Base
#include "Prototype.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define SAMPLE_DEBUG
#ifdef SAMPLE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
Prototype::Prototype()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[ ========= Derived ======== ]
    ///--[System]
    ///--[Etc]
    ///--[Etc]

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
Prototype::~Prototype()
{

}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
