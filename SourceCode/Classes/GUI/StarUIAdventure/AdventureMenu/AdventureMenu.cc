//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "AdvHelp.h"
#include "AdvCombatEntity.h"
#include "CarnationLevel.h"
#include "FlexMenu.h"
#include "StringEntry.h"
#include "VisualLevel.h"
#include "AdvUIBase.h"
#include "AdvUICampfire.h"
#include "AdvUIChat.h"
#include "AdvUICostumes.h"
#include "AdvUIDoctor.h"
#include "AdvUIEquip.h"
#include "AdvUIFieldAbilities.h"
#include "AdvUIFileSelect.h"
#include "AdvUIForms.h"
#include "AdvUIInventory.h"
#include "AdvUIJournal.h"
#include "AdvUIMap.h"
#include "AdvUIOptions.h"
#include "AdvUIPassword.h"
#include "AdvUIQuit.h"
#include "AdvUIRelive.h"
#include "AdvUISkills.h"
#include "AdvUIStatus.h"
#include "AdvUITrainer.h"
#include "AdvUIVendor.h"
#include "AdvUIWarp.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///--[Debug]
//#define AMENU_DEBUG
#ifdef AMENU_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
AdventureMenu::AdventureMenu()
{
    ///--[ ========== Variable Initialization =========== ]
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTUREMENU;
    DebugPush(true, "AdventureMenu: Construction begin.\n");

    ///--[Component UI]
    //--Individual Components.
    rUIBase = NULL;
    rCampBase = NULL;

    //--Lists.
    mrCampfireComponents     = new StarLinkedList(false);
    mrVendorComponents       = new StarLinkedList(false);
    mrFieldAbilityComponents = new StarLinkedList(false);
    mrTrainerComponents      = new StarLinkedList(false);
    mrMainMenuComponents     = new StarLinkedList(false);
    mAllComponents           = new StarLinkedList(true);

    ///--[AdventureMenu]
    //--System
    mOpenedThisTick = false;
    mStartedHidingThisTick = false;
    mHandledUpdate = false;
    mMainOpacityTimer = 0;
    mIsVisible = false;
    mCurrentMode = AM_MODE_BASE;

    //--Closing Handler
    mClosingScript = NULL;

    //--Sub-modes.
    mSubmodeCode = AM_SUBMODE_NONE;

    //--Colors
    mGreyStatic.SetRGBAF(0.70f, 0.70f, 0.70f, 1.0f);
    mHeadingStatic.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    //--[Help Menus]
    mMainMenuHelp = new AdvHelp();

    ///--[ =========== Construction =========== ]
    ///--[Components]
    //--Create the Base UI. It is held on mrMainMenuComponents which will handle deallocation.
    rUIBase   = new AdvUIBase();
    rCampBase = new AdvUICampfire();

    //--Standalone Menus.
    RegisterUI("Vendor",         new AdvUIVendor(),         AM_MODE_NONE, mrVendorComponents);
    RegisterUI("FieldAbilities", new AdvUIFieldAbilities(), AM_MODE_NONE, mrFieldAbilityComponents);
    RegisterUI("Trainer",        new AdvUITrainer(),        AM_MODE_NONE, mrTrainerComponents);

    //--Campfire Menu.
    RegisterUI("Campfire",       rCampBase,                 AM_CAMPFIRE_MODE_BASE,       mrCampfireComponents);
    RegisterUI("Chat",           new AdvUIChat(),           AM_CAMPFIRE_MODE_CHAT,       mrCampfireComponents);
    RegisterUI("Costumes",       new AdvUICostumes(),       AM_CAMPFIRE_MODE_COSTUMES,   mrCampfireComponents);
    RegisterUI("File Select",    new AdvUIFileSelect(),     AM_CAMPFIRE_MODE_FILESELECT, mrCampfireComponents);
    RegisterUI("Forms",          new AdvUIForms(),          AM_CAMPFIRE_MODE_FORMS,      mrCampfireComponents);
    RegisterUI("Password",       new AdvUIPassword(),       AM_CAMPFIRE_MODE_PASSWORD,   mrCampfireComponents);
    RegisterUI("Relive",         new AdvUIRelive(),         AM_CAMPFIRE_MODE_RELIVE,     mrCampfireComponents);
    RegisterUI("Warp",           new AdvUIWarp(),           AM_CAMPFIRE_MODE_WARP,       mrCampfireComponents);

    //--Primary Menu.
    RegisterUI("Base",           rUIBase,                   AM_MODE_BASE,    mrMainMenuComponents);
    RegisterUI("Doctor",         new AdvUIDoctor(),         AM_MODE_DOCTOR,  mrMainMenuComponents);
    RegisterUI("Equip",          new AdvUIEquip(),          AM_MODE_EQUIP,   mrMainMenuComponents);
    RegisterUI("Inventory",      new AdvUIInventory(),      AM_MODE_ITEMS,   mrMainMenuComponents);
    RegisterUI("Journal",        new AdvUIJournal(),        AM_MODE_JOURNAL, mrMainMenuComponents);
    RegisterUI("Map",            new AdvUIMap(),            AM_MODE_MAP,     mrMainMenuComponents);
    RegisterUI("Options",        new AdvUIOptions(),        AM_MODE_OPTIONS, mrMainMenuComponents);
    RegisterUI("Quit",           new AdvUIQuit(),           AM_MODE_QUIT,    mrMainMenuComponents);
    RegisterUI("Skills",         new AdvUISkills(),         AM_MODE_SKILLS,  mrMainMenuComponents);
    RegisterUI("Status",         new AdvUIStatus(),         AM_MODE_STATUS,  mrMainMenuComponents);

    ///--[Structures]
    //--List checking. Run construction then check that images resolved.
    bool tAreImagesReady = true;
    StarUIPiece *rUIPiece = (StarUIPiece *)mAllComponents->PushIterator();
    while(rUIPiece)
    {
        rUIPiece->Construct();
        rUIPiece->RunTranslation();
        tAreImagesReady = (tAreImagesReady && rUIPiece->DidImagesResolve());
        rUIPiece = (StarUIPiece *)mAllComponents->AutoIterate();
    }

    ///--[Debug]
    //--If the images failed to resolve, try to establish why by re-running the verifications.
    if(!tAreImagesReady && !AdventureMenu::xSuppressLoadingErrors)
    {
        //--Heading.
        fprintf(stderr, "=== UI Images Failed to Resolve ===\n");
        fprintf(stderr, "Rendering piece flags.\n");

        //--Component UI.
        StarUIPiece *rUIPiece = (StarUIPiece *)mAllComponents->PushIterator();
        while(rUIPiece)
        {
            rUIPiece->ImageDiagnostics();
            rUIPiece = (StarUIPiece *)mAllComponents->AutoIterate();
        }
    }

    ///--[String Reinitialization]
    ///--[Help Legend]
    //--Does not change, does not need to rebuilt when settings change.
    if(mMainMenuHelp)
    {
        mMainMenuHelp->Construct();
        mMainMenuHelp->AssembleLegend();
    }

    ///--[Diagnostics]
    DebugPop("AdventureMenu: Construction completed.\n");
}
AdventureMenu::~AdventureMenu()
{
    ///--[System]
    //--[System]
    //--System
    //--Closing Handler
    free(mClosingScript);

    ///--[Component UI]
    delete mrCampfireComponents;
    delete mrVendorComponents;
    delete mrFieldAbilityComponents;
    delete mrTrainerComponents;
    delete mrMainMenuComponents;
    delete mAllComponents;

    //--[Help Menus]
    delete mMainMenuHelp;
}

///--[Public Statics]
//--Suppresses loading error printing.
bool AdventureMenu::xSuppressLoadingErrors = false;

//--Script that fires when the rest executes. Often used to trigger dialogues.
char *AdventureMenu::xRestResolveScript = NULL;

//--Script that fires when figuring out which field skills are available.
char *AdventureMenu::xFieldAbilityResolveScript = NULL;

//--Script that fires when the menu initializes. Allows the player to warp.
char *AdventureMenu::xWarpResolveScript = NULL;

//--Script that fires when the player elects to warp.
char *AdventureMenu::xWarpExecuteScript = NULL;

//--Tracks the total number of Catalysts the player can have collected so far.
int AdventureMenu::xCatalystTotal = 0;

//--Stores the last means of sorting files so the same type is used when opening a new menu instance.
//  This gets saved to the configuration file whenever the player changes the sorting type and closes
//  the menu.
int AdventureMenu::xLastSavefileSortingType = AM_SORT_DATEREV;

//--Icon path for the Costume icon.
char *AdventureMenu::xCostumeIconPath = NULL;

//--Icon path for the Transform icon.
char *AdventureMenu::xTransformIconPath = NULL;

//--Icon path for the Relive icon.
char *AdventureMenu::xReliveIconPath = NULL;

//--Stupid bullshit.
bool AdventureMenu::xUseAntchievement = false;

//--Flag used when there is a buy response. If the inventory intends to remove the item the player
//  just bought, trip this flag so they won't be prompted to equip it.
bool AdventureMenu::xDisallowEquippingFromResponse = false;

//--Script that gets called when an item is marked for deconstruction. If the script is NULL then
//  deconstruction is turned off.
char *AdventureMenu::xDeconstructScript = NULL;

///--[Text Display Remaps]
//--These are two paralell arrays which allow remapping between images and letters when in a text box
//  display, such as inventory or equipment descriptions.
//--The typical format is [IMG|Name] which remaps to a DataLibrary path.
//--The names are stored in xTextImgRemap, and the DL paths are in xTextImgPath.
int AdventureMenu::xTextImgRemapsTotal = 0;
char **AdventureMenu::xTextImgRemap = NULL;
char **AdventureMenu::xTextImgPath = NULL;

///===================================== Property Queries =========================================
bool AdventureMenu::StartedHidingThisTick()
{
    //--Used to prevent the menu from closing and opening in the same tick since the control is the
    //  same for both.
    return mStartedHidingThisTick;
}
bool AdventureMenu::IsVisible()
{
    return mIsVisible;
}
bool AdventureMenu::NeedsToBlockControls()
{
    ///--[Documentation]
    //--The menu blocks controls if it is the active object. It can still render if it is not the active
    //  object but the players controls are unlocked.

    ///--[Execution]
    //--Object does not block if not visible.
    if(mIsVisible) return true;

    //--If currently in Field Ability mode, block.
    if(mSubmodeCode == AM_SUBMODE_FIELDABILITIES) return true;

    //--All checks failed, controls return to the caller.
    return false;
}
bool AdventureMenu::NeedsToRender()
{
    //--If the menu is hiding itself, it may be rendering even if it is not the active object.
    if(mIsVisible) return true;
    return true;

    //--Hiding.
    if(mMainOpacityTimer > 0) return true;

    //--All checks failed, don't render.
    return false;
}

///======================================== Manipulators ==========================================
void AdventureMenu::Show()
{
    ///--[Documentation]
    //--Shows the object. This sets the AdvUIBase as the active object and resets its cursor.
    if(!rUIBase) return;

    ///--[Execution]
    //--Reset rUIBase to the center of the grid, which is the equipment entry.
    mCurrentMode = AM_MODE_BASE;
    rUIBase->TakeForeground();
    rUIBase->SetCursorByBackwardsCode(AM_MODE_EQUIP);

    //--Set flags.
    mOpenedThisTick = true;
    mIsVisible = true;

    //--Disable alternate types.
    mSubmodeCode = AM_SUBMODE_NONE;
}
void AdventureMenu::Hide()
{
    ///--[Documentation]
    //--Causes the object to hide, which reduces the visibility timer and stops blocking updates.
    //  If the object is already hiding, does nothing.
    if(mStartedHidingThisTick) return;

    ///--[Execution]
    //--Set flags.
    mIsVisible = false;
    mStartedHidingThisTick = true;

    //--If a script is specified, call it to handle any post-menu changes.
    if(mClosingScript)
    {
        LuaManager::Fetch()->ExecuteLuaFile(mClosingScript, 1, "S", "Close Main");
    }
}
void AdventureMenu::ZeroVisibility()
{
    mMainOpacityTimer = 0;
}
void AdventureMenu::SetCloseScript(const char *pPath)
{
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mClosingScript, NULL);
        return;
    }
    ResetString(mClosingScript, pPath);
}
void AdventureMenu::AppendDeconstructItem(const char *pString)
{
    ///--[Documentation]
    //--When handling item deconstructing, this function is called by an external script to help list what items
    //  are created from a deconstruction.
    if(!pString) return;

    ///--[Locate]
    //--Find the inventory object and retrieve its deconstruction list.
    StarUIPiece *rCheckPtr = (StarUIPiece *)mrMainMenuComponents->GetElementByName("Inventory");
    if(!rCheckPtr || !rCheckPtr->IsOfType(POINTER_TYPE_ADVUIINVENTORY)) return;

    ///--[Append]
    //--Cast.
    AdvUIInventory *rAdvUIInventory = dynamic_cast<AdvUIInventory *>(rCheckPtr);

    //--Get the list.
    StarLinkedList *rDeconstructList = rAdvUIInventory->GetDeconstructList();
    if(!rDeconstructList) return;

    //--Add it.
    int tDummyPtr;
    rDeconstructList->AddElement(pString, &tDummyPtr);
}

///======================================== Core Methods ==========================================
void AdventureMenu::OpenUIByBackwardsCode(int pCode)
{
    ///--[Documentation]
    //--Given a code provided by the AdvUIBase object, handles closing the base object and opening
    //  a sub-object. If the code AM_MODE_NONE is provided, then closes the UI.
    //--Additional setup for some objects may be needed, which is handled here or in their TakeForeground()
    //  calls, depending on the object.
    if(pCode == AM_MODE_NONE)
    {
        Hide();
        return;
    }

    ///--[No Party Members]
    //--Check if the player's party has zero entries. If so, several parts of the UI are locked out.
    //  This is the case in places like Nowhere.
    bool tPlayerPartyHasMembers = (AdvCombat::Fetch()->GetActivePartyCount() > 0);
    if(!tPlayerPartyHasMembers)
    {
        //--Journal goes to achievements mode.
        if(pCode == AM_MODE_JOURNAL)
        {
            //--Activate.
            mCurrentMode = AM_MODE_JOURNAL;
            ActivateComponentInListName(mrMainMenuComponents, "Journal");

            //--Get the journal UI and switch it to Achievement mode immediately.
            StarUIPiece *rStarUIPiece = (StarUIPiece *)RetrieveUI("Journal", POINTER_TYPE_ADVUIJOURNAL, mrMainMenuComponents, "AdventureMenu:UpdateMainMenu()");
            AdvUIJournal *rJournalUI = dynamic_cast<AdvUIJournal *>(rStarUIPiece);
            if(rJournalUI) rJournalUI->SetToAchievementsMode();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Options, behaves as normal.
        else if(pCode == AM_MODE_OPTIONS)
        {
            mCurrentMode = AM_MODE_OPTIONS;
            ActivateComponentInListName(mrMainMenuComponents, "Options");
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--All other modes fail.
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }

        //--Block update.
        return;
    }

    ///--[Normal Execution]
    //--Switch to Inventory mode.
    if(pCode == AM_MODE_ITEMS)
    {
        mCurrentMode = AM_MODE_ITEMS;
        ActivateComponentInListName(mrMainMenuComponents, "Inventory");
    }
    else if(pCode == AM_MODE_EQUIP)
    {
        mCurrentMode = AM_MODE_EQUIP;
        AdventureInventory::Fetch()->SortItemListByInternalCriteria();
        ActivateComponentInListName(mrMainMenuComponents, "Equip");
    }
    else if(pCode == AM_MODE_SKILLS)
    {
        mCurrentMode = AM_MODE_SKILLS;
        ActivateComponentInListName(mrMainMenuComponents, "Skills");
    }
    else if(pCode == AM_MODE_STATUS)
    {
        mCurrentMode = AM_MODE_STATUS;
        ActivateComponentInListName(mrMainMenuComponents, "Status");
    }
    else if(pCode == AM_MODE_DOCTOR)
    {
        mCurrentMode = AM_MODE_DOCTOR;
        ActivateComponentInListName(mrMainMenuComponents, "Doctor");
    }
    else if(pCode == AM_MODE_MAP)
    {
        mCurrentMode = AM_MODE_MAP;
        ActivateComponentInListName(mrMainMenuComponents, "Map");
    }
    else if(pCode == AM_MODE_OPTIONS)
    {
        mCurrentMode = AM_MODE_OPTIONS;
        ActivateComponentInListName(mrMainMenuComponents, "Options");
    }
    else if(pCode == AM_MODE_JOURNAL)
    {
        mCurrentMode = AM_MODE_JOURNAL;
        ActivateComponentInListName(mrMainMenuComponents, "Journal");
    }
    else if(pCode == AM_MODE_QUIT)
    {
        mCurrentMode = AM_MODE_QUIT;
        ActivateComponentInListName(mrMainMenuComponents, "Quit");
    }
}
void AdventureMenu::OpenCampUIByBackwardsCode(int pCode)
{
    ///--[Documentation]
    //--Same as above, except used for the campfire menu.
    if(pCode == AM_CAMPFIRE_MODE_BASE)
    {
        Hide();
        return;
    }

    ///--[Normal Execution]
    if(pCode == AM_CAMPFIRE_MODE_CHAT)
    {
        //--Set.
        mCurrentMode = AM_CAMPFIRE_MODE_CHAT;
        ActivateComponentInListName(mrCampfireComponents, "Chat");

        //--If the mode is immediately flagging to exit, unset. This can happen if there's no one to chat with.
        StarUIPiece *rChatUI = (StarUIPiece *)mrCampfireComponents->GetElementByName("Chat");
        if(rChatUI && rChatUI->IsFlaggingExit())
        {
            mCurrentMode = AM_CAMPFIRE_MODE_BASE;
            rChatUI->UnflagExit();
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    else if(pCode == AM_CAMPFIRE_MODE_FILESELECT)
    {
        mCurrentMode = AM_CAMPFIRE_MODE_FILESELECT;
        ActivateComponentInListName(mrCampfireComponents, "File Select");
    }
    else if(pCode == AM_CAMPFIRE_MODE_COSTUMES)
    {
        mCurrentMode = AM_CAMPFIRE_MODE_COSTUMES;
        ActivateComponentInListName(mrCampfireComponents, "Costumes");
    }
    else if(pCode == AM_CAMPFIRE_MODE_PASSWORD)
    {
        mCurrentMode = AM_CAMPFIRE_MODE_PASSWORD;
        ActivateComponentInListName(mrCampfireComponents, "Password");
    }
    else if(pCode == AM_CAMPFIRE_MODE_RELIVE)
    {
        //--Set.
        mCurrentMode = AM_CAMPFIRE_MODE_RELIVE;
        ActivateComponentInListName(mrCampfireComponents, "Relive");

        //--If the mode is immediately flagging to exit, unset. This can happen if there's no one to chat with.
        StarUIPiece *rReliveUI = (StarUIPiece *)mrCampfireComponents->GetElementByName("Relive");
        if(rReliveUI && rReliveUI->IsFlaggingExit())
        {
            mCurrentMode = AM_CAMPFIRE_MODE_BASE;
            rReliveUI->UnflagExit();
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    else if(pCode == AM_CAMPFIRE_MODE_FORMS)
    {
        //--Set.
        mCurrentMode = AM_CAMPFIRE_MODE_FORMS;
        ActivateComponentInListName(mrCampfireComponents, "Forms");

        //--If the mode is immediately flagging to exit, unset. This can happen if there's no one to chat with.
        StarUIPiece *rFormsUI = (StarUIPiece *)mrCampfireComponents->GetElementByName("Forms");
        if(rFormsUI && rFormsUI->IsFlaggingExit())
        {
            mCurrentMode = AM_CAMPFIRE_MODE_BASE;
            rFormsUI->UnflagExit();
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    else if(pCode == AM_CAMPFIRE_MODE_WARP)
    {
        //--Set.
        mCurrentMode = AM_CAMPFIRE_MODE_WARP;
        ActivateComponentInListName(mrCampfireComponents, "Warp");

        //--If the mode is immediately flagging to exit, unset. This can happen if warping is blocked.
        StarUIPiece *rWarpUI = (StarUIPiece *)mrCampfireComponents->GetElementByName("Warp");
        if(rWarpUI && rWarpUI->IsFlaggingExit())
        {
            mCurrentMode = AM_CAMPFIRE_MODE_BASE;
            rWarpUI->UnflagExit();
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    //--Called when the Warp UI is forcing the object to close entirely and not just out of its own UI.
    else if(pCode == AM_CAMPFIRE_MODE_WARP_EXIT_NOW)
    {
        Hide();
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void AdventureMenu::UpdateBackground()
{
    ///--[Documentation]
    //--Update called when in the background. Just handles hiding.
    mHandledUpdate = true;
    mStartedHidingThisTick = false;

    //--Main opacity.
    if(mMainOpacityTimer > 0) mMainOpacityTimer --;

    ///--[Sub-Handlers]
    //--Update all component UIs in the list. This applies to all sublists.
    ListUpdateBackground(mAllComponents);
}
void AdventureMenu::Update()
{
    ///--[Documentation and Setup]
    //--Reset this flag.
    mStartedHidingThisTick = false;
    mHandledUpdate = false;

    ///--[Timers]
    //--If this object is visible, opacity increases.
    if(mIsVisible)
    {
        if(mMainOpacityTimer < AM_MAIN_VIS_TICKS) mMainOpacityTimer ++;
    }
    //--Object is not visible. Stop update, decrease opacity timer.
    else
    {
        if(mMainOpacityTimer > 0) mMainOpacityTimer --;
        mHandledUpdate = true;
    }

    ///--[Opening Tick]
    //--To prevent idiots from opening the menu and then closing it in the same keypress, the first tick is skipped.
    if(mOpenedThisTick)
    {
        mOpenedThisTick = false;
        return;
    }

    ///--[Component Update Handling]
    //--Nominally, all objects need to run their updates here. The "foreground" is whatever objects are in the
    //  active mode, and the "background" is whatever objects are not. Note that objects are not in the foreground
    //  when the menu is hiding.
    bool tDummyFlag = false;
    StarLinkedList *rForegroundList = NULL;

    //--Primay UI listing.
    if(mSubmodeCode == AM_SUBMODE_NONE)
    {
        rForegroundList = mrMainMenuComponents;
        ListUpdateForeground(mHandledUpdate, false, mrMainMenuComponents);
    }
    //--In campfire mode, call foreground update for all components in mrCampfireComponents.
    else if(mSubmodeCode == AM_SUBMODE_CAMPFIRE)
    {
        rForegroundList = mrCampfireComponents;
        ListUpdateForeground(mHandledUpdate, false, mrCampfireComponents);
    }
    //--Shop mode. Call foreground update for the vendor UI, which is the only member in mrVendorComponents.
    else if(mSubmodeCode == AM_SUBMODE_VENDOR)
    {
        //--Execute.
        rForegroundList = mrVendorComponents;
        int tExitFlag = ListUpdateForeground(tDummyFlag, true, mrVendorComponents);

        //--Flagged to exit.
        if(tExitFlag != AM_MODE_NOEXIT) CloseVendor();
    }
    //--Trainer mode. As above.
    else if(mSubmodeCode == AM_SUBMODE_TRAINER)
    {
        //--Run update.
        rForegroundList = mrTrainerComponents;
        int tExitFlag = ListUpdateForeground(tDummyFlag, true, mrTrainerComponents);

        //--If the exit flag is tripped, call the extra Close() functionality.
        if(tExitFlag != AM_MODE_NOEXIT) CloseTrainer();
    }
    //--If in Field Ability mode, that handles the update.
    else if(mSubmodeCode == AM_SUBMODE_FIELDABILITIES)
    {
        //--Run update.
        rForegroundList = mrFieldAbilityComponents;
        int tExitFlag = ListUpdateForeground(tDummyFlag, true, mrFieldAbilityComponents);

        //--If the exit flag is tripped, call the extra Close() functionality.
        if(tExitFlag != AM_MODE_NOEXIT) CloseFieldAbilities();
    }

    //--Call background updates. All component lists that are not rForegroundList will be updated
    //  as background objects.
    if(rForegroundList != mrMainMenuComponents)     ListUpdateBackground(mrMainMenuComponents);
    if(rForegroundList != mrCampfireComponents)     ListUpdateBackground(mrCampfireComponents);
    if(rForegroundList != mrVendorComponents)       ListUpdateBackground(mrVendorComponents);
    if(rForegroundList != mrTrainerComponents)      ListUpdateBackground(mrTrainerComponents);
    if(rForegroundList != mrFieldAbilityComponents) ListUpdateBackground(mrFieldAbilityComponents);
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void AdventureMenu::Render()
{
    ///--[Component Render Handling]
    //--All objects render as background objects, but depending on the current mode, there may be
    //  extra things like backings rendering. Any objects not on the component list rForegroundList
    //  will render as a background object.
    DebugPush(true, "AdventureMenu: Render begins.\n");

    //--Variables.
    float cVisAlpha = 1.0f;

    //--Primay UI listing. Has a black background.
    if(mSubmodeCode == AM_SUBMODE_NONE)
    {
        cVisAlpha = (float)mMainOpacityTimer / (float)AM_MAIN_VIS_TICKS;
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cVisAlpha));
    }
    //--Campfire mode. Has a black background.
    else if(mSubmodeCode == AM_SUBMODE_CAMPFIRE)
    {
        cVisAlpha = (float)mMainOpacityTimer / (float)AM_MAIN_VIS_TICKS;
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cVisAlpha));
    }
    //--Shop mode. Call foreground update for the vendor UI, which is the only member in mrVendorComponents.
    else if(mSubmodeCode == AM_SUBMODE_VENDOR)
    {
        cVisAlpha = 1.0f;
    }
    //--Trainer mode. As above.
    else if(mSubmodeCode == AM_SUBMODE_TRAINER)
    {
        cVisAlpha = 1.0f;
    }
    //--If in Field Ability mode, that handles the update.
    else if(mSubmodeCode == AM_SUBMODE_FIELDABILITIES)
    {
        cVisAlpha = 1.0f;
    }

    //--Rendering.
    ListRenderForeground(mAllComponents, cVisAlpha);


/*



    ///--[Campfire Mode]
    //--Campfire mode has its own subset that is exclusive with the base menu.
    if(mIsCampfireMode)
    {
        //--Backing.
        float tBackingPct = (float)mMainOpacityTimer / (float)AM_MAIN_VIS_TICKS;
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * tBackingPct));

        //--List.
        ListRenderForeground(mrCampfireComponents, tBackingPct);

        DebugPop("AdventureMenu: Render completes campfire mode.\n");
        return;
    }

    ///--[Shop Mode]
    //--Shopping mode has its own rendering handler.
    if(mIsVendor)
    {
        //--Render all component UIs in the list.
        ListRenderForeground(mrVendorComponents, 1.0f);
        DebugPop("AdventureMenu: Render completes shop mode.\n");
        return;
    }

    ///--[Trainer Mode]
    //--Not dependent on the main opacity. Has an internal handler.
    ListRenderForeground(mrTrainerComponents, 1.0f);
    if(mIsTrainer) return;

    ///--[Field Abilities Menu]
    //--Not dependendent on the main opacity. Has an internal handler.
    ListRenderForeground(mrFieldAbilityComponents, 1.0f);

    ///--[Base Menu]
    //--Backing.
    if(mMainOpacityTimer < 1) return;
    float tBackingPct = (float)mMainOpacityTimer / (float)AM_MAIN_VIS_TICKS;
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * tBackingPct));

    //--Render all component UIs in the list.
    ListRenderForeground(mrMainMenuComponents, tBackingPct);

    //--Campfire gets a background render.
    ListRenderForeground(mrCampfireComponents, tBackingPct);

    */
    //--Diagnostics.
    DebugPop("AdventureMenu: Render completes normal mode.\n");
}
void AdventureMenu::RenderCursor(float pLft, float pYCenter)
{
    //--Renders a cursor at the given location. Used by many of the submenus.
    float cTextHeight = 14.0f;
    float cCursorWid = 7.0f;
    float cCursorHei = cTextHeight * 0.90f;
    float cCursorHeiHf = cCursorHei * 0.50f;
    pYCenter = pYCenter + 2.0f;

    //--Render.
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLES);

        //--Black Underlay.
        glColor3f(0.0f, 0.0f, 0.0f);
        glVertex2f(pLft - 1.0f,              pYCenter - cCursorHeiHf - 1.0f);
        glVertex2f(pLft + cCursorWid + 1.0f, pYCenter                + 0.0f);
        glVertex2f(pLft - 1.0f,              pYCenter + cCursorHeiHf + 1.0f);

        //--White overlay.
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(pLft,              pYCenter - cCursorHeiHf);
        glVertex2f(pLft + cCursorWid, pYCenter);
        glVertex2f(pLft,              pYCenter + cCursorHeiHf);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}
void AdventureMenu::RenderBox(float pLft, float pTop, float pWid, float pHei)
{
    //--Renders a box, black borders with grey backing. Used here temporarily until we have a proper UI.
    //  For speed, assumes that textures have already been turned off.
    float pRgt = pLft + pWid;
    float pBot = pTop + pHei;
    glBegin(GL_QUADS);
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(pLft     , pTop);
        glVertex2f(pLft+3.0f, pTop);
        glVertex2f(pLft+3.0f, pBot);
        glVertex2f(pLft     , pBot);

        glVertex2f(pRgt     , pTop);
        glVertex2f(pRgt-3.0f, pTop);
        glVertex2f(pRgt-3.0f, pBot);
        glVertex2f(pRgt     , pBot);

        glVertex2f(pLft, pTop);
        glVertex2f(pRgt, pTop);
        glVertex2f(pRgt, pTop+3.0f);
        glVertex2f(pLft, pTop+3.0f);

        glVertex2f(pLft, pBot);
        glVertex2f(pRgt, pBot);
        glVertex2f(pRgt, pBot-3.0f);
        glVertex2f(pLft, pBot-3.0f);

        glColor3f(0.1f, 0.1f, 0.1f);
        glVertex2f(pLft+3.0f, pTop+3.0f);
        glVertex2f(pRgt-3.0f, pTop+3.0f);
        glVertex2f(pRgt-3.0f, pBot-3.0f);
        glVertex2f(pLft+3.0f, pBot-3.0f);
    glEnd();
    glColor3f(1.0f, 1.0f, 1.0f);
}

///====================================== Pointer Routing =========================================
StarLinkedList *AdventureMenu::GetComponentList()
{
    return mAllComponents;
}
StarUIPiece *AdventureMenu::ResolveUIPiece(int &sIsFailErroneous, const char *pName, int pType)
{
    ///--[Documentation]
    //--Static call, resolves the menu itself (which may be a derived type) and then uses that to
    //  resolve the named UI object. If it does not exist, or the menu doesn't, then returns NULL,
    //  otherwise returns the piece as the base object.
    //--The flag sIsFailErroneous will report an error code if there's a problem. It is possible for
    //  this call to fail but not be erroneous.
    sIsFailErroneous = cxAdvError_None;

    ///--[Resolve Menu]
    //--Check if the menu failed to resolve.
    AdventureMenu *rActiveMenu = AdventureMenu::Fetch();
    if(!rActiveMenu)
    {
        //--If no menu is found, but a level is found, then the level type is not compatible. Ignore it.
        if(MapManager::Fetch()->GetActiveLevel() != NULL)
        {
            sIsFailErroneous = cxAdvError_None;
        }
        //--Otherwise, this is an error.
        else
        {
            sIsFailErroneous = cxAdvError_NoMenu;
        }
        return NULL;
    }

    ///--[Resolve Object]
    //--Check if the object failed to resolve. Passing NULL for the list uses the all-component list.
    StarUIPiece *rUIPiece = rActiveMenu->RetrieveUI(pName, pType, NULL, "AdventureMenu:ResolveUIPiece()");
    if(!rUIPiece)
    {
        sIsFailErroneous = cxAdvError_NotFound;
        return NULL;
    }

    ///--[Success]
    return rUIPiece;
}
AdvUIVendor *AdventureMenu::GetVendor()
{
    //--Check.
    StarUIPiece *rUIPiece = RetrieveUI("Vendor", POINTER_TYPE_ADVUIVENDOR, mrVendorComponents, "AdventureMenu:GetVendor()");
    if(!rUIPiece) return NULL;

    //--Cast and return.
    AdvUIVendor *rUIVendor = dynamic_cast<AdvUIVendor *>(rUIPiece);
    return rUIVendor;
}
AdvUIWarp *AdventureMenu::GetWarpUI()
{
    //--Check.
    StarUIPiece *rUIPiece = RetrieveUI("Warp", POINTER_TYPE_ADVUIWARP, mrCampfireComponents, "AdventureMenu:GetWarpUI()");
    if(!rUIPiece) return NULL;

    //--Cast and return.
    AdvUIWarp *rUIWarp = dynamic_cast<AdvUIWarp *>(rUIPiece);
    return rUIWarp;
}
AdvUIJournal *AdventureMenu::GetJournalUI()
{
    //--Check.
    StarUIPiece *rUIPiece = RetrieveUI("Journal", POINTER_TYPE_ADVUIJOURNAL, mrMainMenuComponents, "AdventureMenu:GetJournalUI()");
    if(!rUIPiece) return NULL;

    //--Cast and return.
    AdvUIJournal *rUIJournal = dynamic_cast<AdvUIJournal *>(rUIPiece);
    return rUIJournal;
}
AdvUISkills *AdventureMenu::GetSkillsUI()
{
    //--Check.
    StarUIPiece *rUIPiece = RetrieveUI("Skills", POINTER_TYPE_ADVUIJOURNAL, mrMainMenuComponents, "AdventureMenu:GetSkillsUI()");
    if(!rUIPiece) return NULL;

    //--Cast and return.
    AdvUISkills *rUISkills = dynamic_cast<AdvUISkills *>(rUIPiece);
    return rUISkills;
}

///===================================== Static Functions =========================================
AdventureMenu *AdventureMenu::Fetch()
{
    ///--[Adventure Version]
    //--Try to locate an AdventureMenu held in an AdventureLevel.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel)
    {
        return rActiveLevel->GetMenu();
    }

    ///--[Carnation Version]
    //--A CarnationMenu is an AdventureMenu, however, a CarnationLevel is not an AdventureLevel. Thus
    //  we may need to check if a CarnationLevel exists and is holding a menu seperately.
    CarnationLevel *rCarnationLevel = CarnationLevel::Fetch();
    if(rCarnationLevel)
    {
        return rCarnationLevel->GetMenu();
    }

    ///--[Failed]
    //--Menu not resolved.
    return NULL;
}
