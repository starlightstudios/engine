//--Base
#include "StarFont.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
#include "utf8.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

//--This is a freetype-only function.
#if defined _FONTTYPE_FREETYPE_
void StarFont::DrawTextImg(float pX, float pY, int pFlags, float pSize, const char *pText, int pRemapsTotal, char **pRemaps, char **pImgPaths)
{
    ///--[Documentation]
    //--Renders a block of text using the given properties. The extra capability of this function is that it can dynamically
    //  print images in-stride with the text. Obviously these images should be sized for the text because the program won't care.
    //--The way to print these images is to use [IMG|Name]. The Name is then checked against the pRemaps array, which is paralell
    //  with pImagPaths. If a match is found, the image from pImgPaths replaces the [IMG|Name] tag in its entirety.
    //--This routine is obviously computationally more expensive than normal rendering. Only use it in certain instances.
    if(!pText || !pRemaps || !pImgPaths) return;

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawTextImg() - Warning. Attempted to use dummy font %s.\n", mLocalName); return; }

    ///--[Length Check]
    //--Don't bother with a string with 0 letters.
    int tLen = utf8_strlen(pText);
    if(tLen < 1) return;

    ///--[Error Checking]
    //fprintf(stderr, "Draw Text %s\n", pText);
    //fprintf(stderr, "Length is %i or %i bytes\n", tLen, (int)strlen(pText));
    if(!mPrecachedHandle || !mPrecacheData) return;
    if(pSize == 0.0f) pSize = 1.0f;
    //fprintf(stderr, " Passed checks.\n");

    ///--[Positioning]
    //--Move cursor. If flagged, use this as the center point (mod the offset).
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (GetTextWidth(pText) / 2.0f * pSize);
    }
    else if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (GetTextWidth(pText) * pSize);
    }
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (GetTextHeight() / 2.0f * pSize);
    }
    //fprintf(stderr, " Translating.\n");
    glTranslatef(pX, pY, 0.0f);

    //--Sizing. This is only used if the size is not 1.0f, since scaling is non-trivial.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(pSize, pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    ///--[Rendering]
    //--Grab each glyph and render it in turn. First, bind the texture (precached mode).
    //fprintf(stderr, " Binding.\n");
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    int tIndex = 0;
    float tRunningOffset = 0.0f;

    //--For each letter...
    int tLetter = utf8_nextletter(pText, tIndex);
    for(int i = 0; i < tLen-1; i ++)
    {
        ///--[Get Letter and Glyph]
        //--Get the next letter.
        int tNextLetter = utf8_nextletter(pText, tIndex);

        //--Get additional letters.
        int tOff = u8_offset(pText, i);

        ///--[Error Check]
        //--Remap.
        int tGlyphLft = GetGlyphFromRemap(tLetter);

        //--Range check on the letter:
        if(tGlyphLft == -1 || tGlyphLft >= mTotalGlyphs)
        {
            fprintf(stderr, "Out of range. Skipping.\n");
            tLetter = tNextLetter;
            continue;
        }

        //--Precache doesn't exist:
        if(!mPrecacheData[tGlyphLft].mGLList)
        {
            fprintf(stderr, "No precache Skipping.\n");
            tLetter = tNextLetter;
            continue;
        }

        ///--[Image Check]
        //--If the letter is a [, check for the [IMG| tag. This is non-unicode, it must be specifically these ASCII letters.
        if(pText[tOff+0] == '[' && pText[tOff+1] == 'I' && pText[tOff+2] == 'M' && pText[tOff+3] == 'G' && pText[tOff+4] == '|')
        {
            int c = 0;
            char tBuffer[STD_MAX_LETTERS];
            for(int p = tOff+5; p < tLen; p ++)
            {
                if(pText[p] == ']')
                {
                    break;
                }
                else
                {
                    tBuffer[c+0] = pText[p];
                    tBuffer[c+1] = '\0';
                    c ++;
                }
                i = i + c - 1;
            }

            //--Check the remaps.
            for(int p = 0; p < pRemapsTotal; p ++)
            {
                //--Error check.
                if(pRemaps[p] == NULL) continue;
                if(strcmp(pRemaps[p], tBuffer)) continue;

                //--Match. Display it.
                StarBitmap *rBitmap = (StarBitmap *)rDataLibrary->GetEntry(pImgPaths[p]);
                if(rBitmap)
                {
                    rBitmap->Draw(0, 2);
                    glTranslatef(rBitmap->GetTrueWidth(), 0.0f, 0.0f);
                    tRunningOffset = tRunningOffset + rBitmap->GetTrueWidth();
                    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);
                }

                //--In all cases, stop remap checking.
                break;
            }

        }
        //--Normal case.
        else
        {
            //--Render.
            glCallList(mPrecacheData[tGlyphLft].mGLList);

            //--Get the expected kerning distance.
            float cKernDistance = mPrecacheData[tGlyphLft].mWidth;
            if(tNextLetter >= SUGARFONT_PRECACHE_OFF && tNextLetter < SUGARFONT_PRECACHE_HI)
            {
                cKernDistance = mPrecacheData[tGlyphLft].mKerningDistance[tNextLetter-SUGARFONT_PRECACHE_OFF];
            }
            else if(mMonospacing != -1.0f)
            {
                cKernDistance = mMonospacing;
            }

            //--Reposition the cursor.
            glTranslatef(cKernDistance, 0.0f, 0.0f);
            tRunningOffset = tRunningOffset + cKernDistance;
        }

        //--Move to the next letter.
        tLetter = tNextLetter;
    }

    ///--[Final Letter]
    //--Remap check.
    if(true)
    {
        //--Get remap.
        int tGlyphLft = GetGlyphFromRemap(tLetter);

        //--Remap fails.
        if(tGlyphLft < 0 || tGlyphLft >= mTotalGlyphs)
        {
        }
        //--No precache.
        else if(!mPrecacheData[tGlyphLft].mGLList)
        {
        }
        //--Italics.
        else if(pFlags & SUGARFONT_ITALIC)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListItalic);
        }
        //--Mirror around the X axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORX)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListXMirror);
        }
        //--Mirror around the Y axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORY)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListYMirror);
        }
        //--Double-render X. Renders the letter twice, once normally and once flipped on the X-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERX)
        {
            //--Normal.
            glCallList(mPrecacheData[tGlyphLft].mGLList);

            //--Flipped.
            glTranslatef(mPrecacheData[tGlyphLft].mAdvanceX, 0.0f, 0.0f);
            glScalef(-1.0f, 1.0f, 1.0f);
            glCallList(mPrecacheData[tGlyphLft].mGLList);
            glScalef(-1.0f, 1.0f, 1.0f);
            glTranslatef(mPrecacheData[tGlyphLft].mAdvanceX * -1.0f, 0.0f, 0.0f);
        }
        //--Double-render Y. Renders the letter twice, once normally and once flipped on the Y-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERY)
        {
            //--Normal.
            glCallList(mPrecacheData[tGlyphLft].mGLList);

            //--Compute.
            float cYPosition = mPrecacheData[tGlyphLft].mFlippedTopOffset * 1.0f;

            //--Flipped.
            glTranslatef(0.0f, cYPosition, 0.0f);
            glScalef(1.0f, -1.0f, 1.0f);
            glCallList(mPrecacheData[tGlyphLft].mGLList);
            glScalef(1.0f, -1.0f, 1.0f);
            glTranslatef(0.0f, -cYPosition, 0.0f);
        }
        //--Normal rendering.
        else
        {
            glCallList(mPrecacheData[tGlyphLft].mGLList);
        }
    }

    ///--[Clean Up]
    //--Remove running offset.
    glTranslatef(tRunningOffset * -1.0f, 0.0f, 0.0f);

    //--Undo the size change.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(1.0f / pSize, 1.0f / pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--Undo by cursor.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);
}

//--If freetype is not defined, a dummy version of the function is declared. It does nothing.
#else
void StarFont::DrawTextImg(float pX, float pY, int pFlags, float pSize, const char *pText, int pRemapsTotal, char **pRemaps, char **pImgPaths)
{
    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawTextImg() - Warning. Attempted to use dummy font %s\n", mLocalName); return; }

    ///--[Nothing]
    return;
}

#endif
