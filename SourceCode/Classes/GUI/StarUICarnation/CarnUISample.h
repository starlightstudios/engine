//Replace CarnUISample with name of UI
//Balance header
//Switch AdvUISample to matching inheritance type
//Delete these instructions

///======================================= CarnUISample ===========================================
//--Description

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"
#include "AdvUISample.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CarnUISample : public CarnUICommon, virtual public AdvUISample
{
    protected:
    ///--[System]
    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;

        //--Images
        StarBitmap *rFrame_Base;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUISample();
    virtual ~CarnUISample();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


