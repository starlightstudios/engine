//--Base
#include "StarUIPiece.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarPointerSeries.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

///--[Debug]
//#define STARUIPIECE_DEBUG
#ifdef STARUIPIECE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
StarUIPiece::StarUIPiece()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    mFlagExit = false;
    strcpy(mSubtype, "STAR");
    mCodeBackward = 0;
    memset(mCloseCode, 0, sizeof(char) * STARUI_CLOSE_CODE_SIZE);

    ///--[Diagnostics]
    mVerifyList = new StarLinkedList(true);
    mDiagnostics = new StarLinkedList(true);

    ///--[Visiblity]
    mVisibilityTimer = 0;
    mVisibilityTimerMax = 1;

    ///--[Rendering]
    mLastOffsetX = 0.0f;
    mLastOffsetY = 0.0f;
    mTranslateStack = new StarLinkedList(true);

    ///--[Help Menu]
    mIsShowingHelp = false;
    mHelpVisibilityTimer = 0;
    mHelpVisibilityTimerMax = cHlpTicks;
    mHelpHeaderCol.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);
    mHelpControlOffset = 0.0f;
    mHelpStringsMax = 0;
    mHelpMenuStrings = NULL;
    memset(&HelpImages, 0, sizeof(HelpImages));

    ///--[Images]
    mUseFade = false;
    mImagesReady = false;
}
StarUIPiece::~StarUIPiece()
{
    delete mVerifyList;
    delete mDiagnostics;
    delete mTranslateStack;
    for(int i = 0; i < mHelpStringsMax; i ++) delete mHelpMenuStrings[i];
    free(mHelpMenuStrings);
}
void StarUIPiece::Construct()
{
    ///--[Documentation]
    //--By default, this class does nothing. This is where the coder is expected to resolve images
    //  and fonts from the DataLibrary.
}
bool StarUIPiece::VerifyImages()
{
    ///--[Documentation]
    //--Calls each (VerifyPack *) and checks if any of them come back false. If they do, returns false. If all
    //  packs verify correctly, returns true.
    //--This does not necessarily mean all the images resolved if the verify packages do not reflect all of the
    //  images. It is understood that a derived class may not care about certain images, but it's usually good
    //  practice to set those to a valid dummy pointer and check them all to be safe.
    VerifyPack *rVerifyPack = (VerifyPack *)mVerifyList->PushIterator();
    while(rVerifyPack)
    {
        //--Subroutine will scan the image pointers and return false if any are NULL.
        if(!VerifyStructure(rVerifyPack->rPtr, rVerifyPack->mSize, sizeof(void *), false))
        {
            mVerifyList->PopIterator();
            return false;
        }
        rVerifyPack = (VerifyPack *)mVerifyList->AutoIterate();
    }

    //--All checks passed, images are verified.
    return true;
}
void StarUIPiece::ImageDiagnostics()
{
    ///--[Documentation]
    //--When a class has images/fonts/etc in it, if something fails to resolve the caller will usually
    //  check the verification boolean with DidImagesResolve() and then call this function, which
    //  should print out which pointers failed to resolve.
    //--The stock version can be used to automatically check internal lists. Derived types may override
    //  this if they are only interested in certain types of diagnostics or want to only check parts
    //  of the structures.
    fprintf(stderr, "Diagnostics for %s %p.\n", mSubtype, this);
    fprintf(stderr, "Printing Internal Diagnostics. %i entries.\n", mDiagnostics->GetListSize());
    char *rDiagnosticString = (char *)mDiagnostics->PushIterator();
    while(rDiagnosticString)
    {
        fprintf(stderr, " %s\n", rDiagnosticString);
        rDiagnosticString = (char *)mDiagnostics->AutoIterate();
    }
    fprintf(stderr, "Printing VerifyStructure() Results\n");
    VerifyPack *rVerifyPack = (VerifyPack *)mVerifyList->PushIterator();
    while(rVerifyPack)
    {
        fprintf(stderr, "Pack Name: %s\n", mVerifyList->GetIteratorName());
        VerifyStructure(rVerifyPack->rPtr, rVerifyPack->mSize, sizeof(void *), true);
        rVerifyPack = (VerifyPack *)mVerifyList->AutoIterate();
    }
}
void StarUIPiece::RunTranslation()
{
    ///--[Documentation]
    //--UI pieces often have assorted text in them that can be run through translation at program startup.
    //  This routine is called for each UI piece when it is created, if a translation is active.
}

///===================================== Property Queries =========================================
bool StarUIPiece::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    return false;
}
bool StarUIPiece::IsFlaggingExit()
{
    ///--[Documentation]
    //--Queries if this object is being asked to close, usually by the player hitting the cancel
    //  button or selecting an exit option. This can also be flagged programmatically. Note that
    //  the object does not hide itself, whether or not it is allowed to close is determined by
    //  the calling object.
    return mFlagExit;
}
const char *StarUIPiece::GetSubtype()
{
    return mSubtype;
}
int StarUIPiece::GetCodeBackward()
{
    return mCodeBackward;
}
const char *StarUIPiece::GetCloseBuffer()
{
    return mCloseCode;
}
int StarUIPiece::GetVisibilityTimer()
{
    return mVisibilityTimer;
}
int StarUIPiece::GetVisibilityTimerMax()
{
    return mVisibilityTimerMax;
}
bool StarUIPiece::DidImagesResolve()
{
    ///--[Documentation]
    //--Returns true if the images have resolved and the object is ready for rendering, false if not.
    //  This may be called for diagnostics by the host object.
    return mImagesReady;
}

///======================================= Manipulators ===========================================
void StarUIPiece::TakeForeground()
{
    ///--[Documentation]
    //--The rough equivalent of "Show", the object assumes it has been opened. Resets timers and resolves
    //  anything that needs to be done at runtime, such as control strings.
    mIsForegroundObject = true;
}
void StarUIPiece::TakeBackground()
{
    ///--[Documentation]
    //--The rough equivalent of "Hide", if anything needs to be cleaned up it can be done now if it
    //  is not visible. Keep in mind most objects will continue to display briefly as they fade out
    //  or scroll offscreen, so it may be wiser to wait until the object is fully hidden to set some
    //  properties. If that's not an option, make the renderer use copies to keep things synched.
    //--This is *not* called internally by the object, the calling object should handle calling this
    //  based on IsFlaggingExit() or its own internal logic.
}
void StarUIPiece::FlagExit()
{
    ///--[Documentation]
    //--Sets the flag exit value used by IsFlaggingExit(). You can normally do this with a simple
    //  setting line, but it is provided as a function which will be inlined for diagnostic reasons.
    //  Just add a print to find out where/why your exit is being called. Handy!
    //--You can also override this function if you want something else to happen when exiting.
    mFlagExit = true;
    mIsForegroundObject = false;
}
void StarUIPiece::UnflagExit()
{
    ///--[Documentation]
    //--Order the object to unset the exit flag, should be called any time the exit flag is true
    //  by the calling object if it the UI was not allowed to close.
    mFlagExit = false;
}
void StarUIPiece::SetCodeBackward(int pValue)
{
    ///--[Documentation]
    //--An integer entirely determined by the caller. It is set by the caller and can be retrieved by
    //  the caller, typically when the user calls FlagExit() by pressing cancel. As an example, the AdventureMenu
    //  uses this code to determine which menu option the grid should point at when an object exits.
    mCodeBackward = pValue;
}
void StarUIPiece::SetCloseCode(const char *pString)
{
    ///--[Documentation]
    //--In some games, closing the UI or doing things like equipping skills and items can fire a script response.
    //  For example, Runes of Pandemonium can change which enemies target the player when equipping badges.
    //  The caller fires a response script when an object sets the close code to something other than NULL.
    //--Pass NULL to clear.
    if(!pString)
    {
        memset(mCloseCode, 0, sizeof(char) * STARUI_CLOSE_CODE_SIZE);
    }
    else
    {
        strncpy(mCloseCode, pString, STARUI_CLOSE_CODE_SIZE-1);
    }
}
void StarUIPiece::AppendDiagnostic(const char *pString, ...)
{
    ///--[Documentation]
    //--Appends a diagnostic string to the end of the diagnostics list. The string needs to be duplicated,
    //  the parameter string may itself be an unstable pointer.
    if(!pString) return;

    //--Construct the string.
    char *nString = (char *)starmemoryalloc(sizeof(char) * 256);
    va_list tArgList;
    va_start(tArgList, pString);
    int tReturnSize = vsprintf(nString, pString, tArgList);
    va_end(tArgList);

    //--Store it.
    mDiagnostics->AddElementAsTail("X", nString, &FreeThis);

    //--Warnings.
    if(tReturnSize >= 256)
    {
        fprintf(stderr, "StarUIPiece:AppendDiagnostic() - Warning. vsprintf() wrote more than 256 characters to buffer.\n");
    }
}
void StarUIPiece::AppendVerifyPack(const char *pName, void *pPtr, size_t pStructureSize)
{
    ///--[Documentation]
    //--Appends a (VerifyPack *) to the end of the verification list, allowing automation of font/image pointers
    //  resolving from datafiles.
    if(!pName || !pPtr) return;

    //--Allocate.
    VerifyPack *nVerifyPack = (VerifyPack *)starmemoryalloc(sizeof(VerifyPack));
    nVerifyPack->rPtr = pPtr;
    nVerifyPack->mSize = pStructureSize;

    //--Register.
    mVerifyList->AddElementAsHead(pName, nVerifyPack, &FreeThis);
}

///======================================= Core Methods ===========================================
void StarUIPiece::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Most UIs have an optional help menu that shows controls. Because controls can be changed at
    //  any time (mostly via options menus but it's theoretically possible to do it in script) the
    //  menu can't pre-set the control strings and must re-resolve the control images when it is opened.
    //--This function does that. If your UI doesn't have a help menu, don't implement it.
}
void StarUIPiece::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--It is extremely common for the cursor to be set based on various easing functions using internal
    //  state variables. This function can be called to recompute where the cursor should be whenever
    //  it changes. Cursors often use EasingPack1D or EasingPack2D objects so the update ticks will
    //  handle finalizing the cursor position.
}
void StarUIPiece::ResolveSeriesInto(const char *pSeriesName, void *pPtr, size_t pSize)
{
    ///--[Documentation]
    //--Using a StarPointerSeries held in the DataLibrary at the given name, crossloads the data from
    //  the DataLibrary into the provided structure with the given size.
    //--If the series is not found, a diagnostic is logged but no error prints immediately, as this
    //  may occur when loading with mods that will later rectify the problem.
    if(!pSeriesName) return;

    //--Check the series. Log an error if not found.
    StarPointerSeries *rSeries = DataLibrary::Fetch()->GetStarPointerSeries(pSeriesName);
    if(!rSeries) { AppendDiagnostic("Failed to find '%s.", pSeriesName); return; }

    ///--[Load]
    //--Execute the crossload.
    rSeries->Crossload(pPtr, pSize / sizeof(void *));
}
void StarUIPiece::HandlePositioning(float pXOffset, float pYOffset, float pPercent)
{
    ///--[Documentation]
    //--Objects in a UI typically either fade in or out, or slide in from the sides. This is set with
    //  the flag mUseFade (true to fade, else slides) and is basically a glColor() or glTranslate() call.
    //--This function organizes that. You can also handle custom introductions if desired.
    //--The offsets are the max value if the object is totally offscreen. Call the function again with the
    //  value multiplied by -1.0f to untranslate.
    if(mUseFade)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pPercent);
    }
    //--Translate.
    else
    {
        glTranslatef(pXOffset, pYOffset, 0.0f);
    }
}
void StarUIPiece::SetColor(const char *pColorName, float pAlpha)
{
    ///--[Documentation]
    //--Overridable function, takes in a name and sets the color mixer.
    if(!pColorName) return;

    //--By default, only allows "Clear" which resets the mixer.
    if(!strcasecmp(pColorName, "Clear")) { StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha); return; }

    //--Error.
    fprintf(stderr, "StarUIPiece:SetColor() - Subtype %s. Failed to locate color %s.\n", mSubtype, pColorName);
}
bool StarUIPiece::CommonHelpUpdate()
{
    ///--[Documentation]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    //--Returns true if the update is blocked by the help menu, false if not.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Help Mode]
    //--Always blocks input. The user can disable help by cancelling it.
    if(mIsShowingHelp)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingHelp = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return true;
    }

    ///--[Not Help Mode]
    //--Otherwise, push F1 to activate help.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsShowingHelp = true;
        RefreshMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    //--User is not in help mode, update proceeds.
    return false;
}

///=================================== Private Core Methods =======================================
void StarUIPiece::AllocateHelpStrings(int pCount)
{
    ///--[Documentation]
    //--Allocates help strings. Also auto-deallocates existing strings if there were any, pass 0 to
    //  deallocate help strings and clear the memory.

    ///--[Deallocate]
    for(int i = 0; i < mHelpStringsMax; i ++) delete mHelpMenuStrings[i];
    free(mHelpMenuStrings);

    ///--[Zero]
    //--Reset variables to zero state.
    mHelpStringsMax = 0;
    mHelpMenuStrings = NULL;

    //--Count is zero, stop here.
    if(pCount < 1) return;

    ///--[Allocate]
    //--Allocate and initialize.
    mHelpStringsMax = pCount;
    mHelpMenuStrings = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mHelpStringsMax);
    for(int i = 0; i < mHelpStringsMax; i ++)
    {
        mHelpMenuStrings[i] = new StarlightString();
    }
}
void StarUIPiece::SetHelpString(int &sSlot, const char *pString)
{
    ///--[Documentation]
    //--Overload, sets a help string normally without any images.
    SetHelpString(sSlot, pString, 0);
}
void StarUIPiece::SetHelpString(int &sSlot, const char *pString, int pImages, ...)
{
    ///--[Documentation]
    //--A very common action is setting a help string and incrementing the slot immediately after setting it.
    //  This function does that and automatically range checks the help string to make sure the programmer
    //  didn't count wrong.
    //--sSlot will increment by 1 when the function ends. If out of range, a warning is barked and nothing
    //  happens. Handle these warnings, the user should not see them!

    ///--[Range Check]
    if(sSlot < 0 || sSlot >= mHelpStringsMax)
    {
        DebugManager::ForcePrint("StarUIPiece:SetHelpString() - %s - Warning, string is out of range. %i vs max %i\n", mSubtype, sSlot, mHelpStringsMax);
        return;
    }

    ///--[Empty String]
    //--This is legal but requires special handling. Ignores arguments.
    if(!pString)
    {
        mHelpMenuStrings[sSlot]->SetString("");
        sSlot ++;
        return;
    }

    ///--[Set String]
    //--Set the basic strings.
    mHelpMenuStrings[sSlot]->SetString(pString);

    //--Argument handling. The first argument should be an integer describing how many images are expected.
    //  If it's zero, don't set any images.
    if(pImages > 0)
    {
        //--Fast-access pointer.
        ControlManager *rControlManager = ControlManager::Fetch();

        //--Start argument list and set it to the last argument.
        va_list tArgList;
        va_start(tArgList, pImages);

        //--Allocate images.
        mHelpMenuStrings[sSlot]->AllocateImages(pImages);

        //--For each image:
        for(int i = 0; i < pImages; i ++)
        {
            //--Get the name of the control as an argument.
            const char *rControlName = va_arg(tArgList, const char *);

            //--Set it.
            mHelpMenuStrings[sSlot]->SetImageP(i, mHelpControlOffset, rControlManager->ResolveControlImage(rControlName));
        }

        //--Clean up.
        va_end(tArgList);
    }

    //--Increment slot counter.
    sSlot ++;
}

///========================================== Update ==============================================
bool StarUIPiece::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Main update, should handle input, timers, and computations. If pCannotHandleUpdate is true,
    //  the object is not in the foreground, and should call UpdateBackground() and exit.

    ///--[Timers]
    //--By default, the visiblity timer runs to its max and then stops.
    if(mVisibilityTimer < mVisibilityTimerMax) mVisibilityTimer ++;
    return true;
}
void StarUIPiece::UpdateBackground()
{
    ///--[Documentation]
    //--Background update, typically just runs timers until the object finishes hiding, then does nothing.
    //  Depending on how the source UI works, objects may be calling this update every tick, allowing
    //  background processing, or only call when that source UI is shown.
    //--Main UIs typically don't call this directly, but call UpdateForeground() with pCannotHandleUpdate
    //  as true. This is dependent on the implementation of the calling UI. If the caller is itself
    //  a backgroundable object it may call this for all subcomponents.
    if(mVisibilityTimer > 0) mVisibilityTimer --;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void StarUIPiece::RenderForeground(float pVisAlpha)
{
    ///--[Documentation]
    //--Rendering when the object is the front object. The caller may optionally modify the alpha,
    //  which may be handled as a fade mixing or as a translation. See HandlePositioning().
    //--This is the standard rendering cycle. It does error checks, calls RenderPieces(), and then
    //  cleans up.

    ///--[Visibility Checks]
    //--Images failed to resolve. Do nothing.
    if(!mImagesReady) return;

    //--Caller set the visibility at zero, do nothing.
    if(pVisAlpha <= 0.0f) return;

    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    //--Re-resolve if we should fade or translate. The option can theoretically change at any time.
    mUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Call Pieces Rendering]
    //--Compute how on-screen this object is by calculating the alpha from the vis timer. Multiply
    //  with the provided alpha so the host doesn't fade differently from sub-components.
    float cLocalAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)mVisibilityTimerMax) * pVisAlpha;

    //--Call subroutine. Can be overriden.
    RenderPieces(cLocalAlpha);

    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void StarUIPiece::RenderBackground(float pVisAlpha)
{
    ///--[Documentation]
    //--Rendering when the object is not the front object. Typically calls RenderForeground() in 99%
    //  of objects, as the foreground rendering is handled via an internal timer for fading/scrolling.
    //--If your UI does nothing interesting in the background, don't override this.
    RenderForeground(pVisAlpha);
}
void StarUIPiece::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Most UIs do a standard setup then render left/top/right/bottom/help in order, then stop. This function
    //  does that and is called from RenderForeground() and RenderBackground(). It can be overridden if a different
    //  order is needed or there are more sub-components that need rendering. It can also be bypassed entirely
    //  by overriding RenderForeground().
    //--By default, objects slide in from the screen edges.

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);
}
void StarUIPiece::RenderLft(float pVisAlpha)
{
    ///--[Documentation]
    //--Most UIs consist of several major pieces, which will be called in order of Left, Top, Right, Bottom, Help,
    //  by the standard handler. This represents the top block. If using translation, it slides in from that side.
    //  If using fading, all pieces fade in together and there is no particular distinction.
}
void StarUIPiece::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Most UIs consist of several major pieces, which will be called in order of Left, Top, Right, Bottom, Help,
    //  by the standard handler. This represents the left block. If using translation, it slides in from that side.
    //  If using fading, all pieces fade in together and there is no particular distinction.
}
void StarUIPiece::RenderRgt(float pVisAlpha)
{
    ///--[Documentation]
    //--Most UIs consist of several major pieces, which will be called in order of Left, Top, Right, Bottom, Help,
    //  by the standard handler. This represents the right block. If using translation, it slides in from that side.
    //  If using fading, all pieces fade in together and there is no particular distinction.
}
void StarUIPiece::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Most UIs consist of several major pieces, which will be called in order of Left, Top, Right, Bottom, Help,
    //  by the standard handler. This represents the bottom block. If using translation, it slides in from that side.
    //  If using fading, all pieces fade in together and there is no particular distinction.
}
void StarUIPiece::RenderHelp(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the help screen. Usually called by RenderForeground() and RenderBackground(). By default, internal variables
    //  can handle rendering help screens, which are usually just a list of strings and a few simple headers and backgrounds.
    StandardHelp(pVisAlpha);
}
void StarUIPiece::StandardHelp(float pVisAlpha)
{
    ///--[Documentation]
    //--Standardized version of the help screen using internal variables when possible.
    if(!mImagesReady) return;

    ///--[Visiblity Check]
    //--If the parent object is not visible, do nothing.
    if(pVisAlpha <= 0.0f) return;

    //--If the visiblity timer is zero, do nothing.
    if(mHelpVisibilityTimer < 1) return;

    ///--[Setup]
    //--Compute offset.
    float cHelpAlpha = EasingFunction::QuadraticInOut(mHelpVisibilityTimer, (float)mHelpVisibilityTimerMax) * pVisAlpha;
    float cHelpPercent = 1.0f - cHelpAlpha;
    float cHelpOffset = -VIRTUAL_CANVAS_Y * cHelpPercent;

    //--If the UI is not fading, set the color alpha to 1.0f;
    float cColorAlpha = cHelpAlpha;
    if(!mUseFade) cColorAlpha = 1.0f;

    ///--[Rendering]
    //--Darkening the UI behind the help window.
    StarBitmap::DrawFullBlack(cHelpAlpha * 0.80f);

    //--Translate/Fade.
    HandlePositioning(0.0f, cHelpOffset * 1.0f, cHelpAlpha);

    //--Render.
    HelpImages.rFrame_Main->Draw();

    //--Heading.
    mHelpHeaderCol.SetAsMixerAlpha(cColorAlpha);
    HelpImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Help Menu");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Render.
    float cHelpX = 55.0f;
    float cHelpY = 89.0f;
    float cHelpH = 26.0f;
    for(int i = 0; i < mHelpStringsMax; i ++)
    {
        mHelpMenuStrings[i]->DrawText(cHelpX, cHelpY + (cHelpH * i), SUGARFONT_NOCOLOR, 1.0f, HelpImages.rFont_Mainline);
    }

    ///--[Clean]
    HandlePositioning(0.0f, cHelpOffset * -1.0f, 1.0f);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
