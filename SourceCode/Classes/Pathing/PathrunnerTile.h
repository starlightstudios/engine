///====================================== PathrunnerTile ==========================================
//--Version of the Pathrunner class which goes by-tile instead of by-collision. Faster, but tends to
//  struggle around advanced collisions like angles and bars.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "Pathrunner.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class PathrunnerTile : public Pathrunner
{
    private:
    ///--[System]
    ///--[Etc]

    protected:

    public:
    //--System
    PathrunnerTile();
    virtual ~PathrunnerTile();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    virtual bool IsMoveShortest(int pX, int pY, int pPulseVal);
    virtual bool IsMoveValid(int pX, int pY, int pPulseVal, int pDirection);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual StarLinkedList *RunPathingIntoList();
    virtual void Initialize(int pStartX, int pStartY, int pEndX, int pEndY);
    virtual void RunIterator(int pPulseCap, bool pStopOnReachingTarget);
    virtual StarLinkedList *ResolvePath();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PathTile_RunPathing(lua_State *L);

