//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEffect.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///--[Property Queries]
int AdvCombatEntity::GetHealth()
{
    return mHP;
}
int AdvCombatEntity::GetHealthMax()
{
    return GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
}
int AdvCombatEntity::GetMagic()
{
    return mMP;
}
int AdvCombatEntity::GetComboPoints()
{
    return mCP;
}
int AdvCombatEntity::GetAdrenaline()
{
    return mAdrenaline;
}
int AdvCombatEntity::GetShields()
{
    return mShield;
}
int AdvCombatEntity::GetStun()
{
    return mStunValue;
}
int AdvCombatEntity::GetStunResist()
{
    return mStunResist;
}
int AdvCombatEntity::GetStunResistTimer()
{
    return mStunResistDecrementTurns;
}
int AdvCombatEntity::GetInfluence()
{
    return mInfluence;
}
int AdvCombatEntity::GetInfluenceMax()
{
    return mInfluenceMax;
}
float AdvCombatEntity::GetHealthPercent()
{
    //--Range-check on max HP.
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(tHPMax < 1) tHPMax = 1;
    return (float)mHP / (float)tHPMax;
}
float AdvCombatEntity::GetMagicPercent()
{
    int tMPMax = mSummedStatistics.GetStatByIndex(STATS_MPMAX);
    if(tMPMax < 1) return 0.0f;
    return (float)mMP / (float)tMPMax;
}
int AdvCombatEntity::GetStatistic(int pStatisticIndex)
{
    //--Assumes the final statistic is the one being queried.
    return GetStatistic(ADVCE_STATS_FINAL, pStatisticIndex);
}
int AdvCombatEntity::GetStatistic(int pGroupIndex, int pStatisticIndex)
{
    CombatStatistics *rStatGroup = GetStatisticsGroup(pGroupIndex);
    return rStatGroup->GetStatByIndex(pStatisticIndex);
}

///--[Manipulators]
void AdvCombatEntity::SetHealth(int pHP)
{
    //--Get HP Max.
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(tHPMax < 1) tHPMax = 1;

    //--Set HP.
    mHP = pHP;
    if(mHP >= tHPMax) mHP = tHPMax;
    if(mHP < 0) mHP = 0;

    //--Special Flag: Entity cannot be KO'd.
    if(mCannotBeKnockedOut && mHP < 1) mHP = 1;

    //--Ease to destination.
    float tPercent = (float)mHP / (float)tHPMax;
    mHPEasingPack.MoveTo(tPercent, ADVCE_EASING_TICKS);
    //fprintf(stderr, "Setting to percent %f which is %i, actual is %i\n", tPercent, (int)((float)tHPMax * tPercent), mHP);

    //--If the HP value is above zero, the entity is no longer knocked out.
    if(mHP > 0) mIsKnockedOut = false;
}
void AdvCombatEntity::SetMagic(int pMP)
{
    //--Get MP Max. Can legally be 0.
    int tMPMax = mSummedStatistics.GetStatByIndex(STATS_MPMAX);
    if(tMPMax < 1) tMPMax = 0;

    //--Set MP.
    mMP = pMP;
    if(mMP >= tMPMax) mMP = tMPMax;
    if(mMP < 0) mMP = 0;

    //--Ease to destination.
    if(tMPMax == 0)
    {
        mMPEasingPack.MoveTo(0.0f, ADVCE_EASING_TICKS);
    }
    else
    {
        float tPercent = (float)mMP / (float)tMPMax;
        mMPEasingPack.MoveTo(tPercent, ADVCE_EASING_TICKS);
    }
}
void AdvCombatEntity::SetComboPoints(int pCP)
{
    //--Get CP Max. Can legally be 0.
    int tCPMax = mSummedStatistics.GetStatByIndex(STATS_CPMAX);
    if(tCPMax < 1) tCPMax = 0;

    //--Set MP.
    mCP = pCP;
    if(mCP >= tCPMax) mCP = tCPMax;
    if(mCP < 0) mCP = 0;
}
void AdvCombatEntity::SetAdrenaline(int pAdrenaline)
{
    mAdrenaline = pAdrenaline;
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(mAdrenaline > tHPMax / 2) mAdrenaline = tHPMax / 2;

    mAdrenalineEasingPack.MoveTo(mAdrenaline, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetShields(int pShields)
{
    mShield = pShields;
    mShieldEasingPack.MoveTo(mShield, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetStunnable(bool pIsStunnable)
{
    mIsStunnable = pIsStunnable;
}
void AdvCombatEntity::SetStun(int pStunValue)
{
    mStunValue = pStunValue;
    mStunEasingPack.MoveTo(mStunValue, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetStunNoDisplay(int pStunValue)
{
    mStunValue = pStunValue;
}
void AdvCombatEntity::SetStunOnlyDisplay()
{
    mStunEasingPack.MoveTo(mStunValue, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::AddStun(int pStunValue)
{
    mStunValue = mStunValue + pStunValue;
    mStunEasingPack.MoveTo(mStunValue, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetStunResist(int pStunResist)
{
    mStunResist = pStunResist;
}
void AdvCombatEntity::SetStunResistTimer(int pTimer)
{
    mStunResistDecrementTurns = pTimer;
}
void AdvCombatEntity::SetInfluence(int pValue)
{
    mInfluence = pValue;
    if(mInfluence > mInfluenceMax) mInfluence = mInfluenceMax;
    if(mInfluence < 0) mInfluence = 0;
}
void AdvCombatEntity::SetInfluenceMax(int pValue)
{
    mInfluenceMax = pValue;
    if(mInfluenceMax < 1) mInfluenceMax = 1;
}
void AdvCombatEntity::SetHealthPercent(float pPercent, bool pIsHeal)
{
    //--If the flag pIsHeal is true, then the health value cannot be below the current health value.
    //  This prevents rounding errors.
    int tHPStart = mHP;
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    SetHealth(tHPMax * pPercent);

    //--If the flag is set and the HP went down, reset it.
    if(pIsHeal && tHPStart > mHP) mHP = tHPStart;

    //--If the percent was over zero and the absolute health hit 0, give the entity 1 HP.
    if(pPercent > 0.0f && mHP < 1)
    {
        mHP = 1;
    }
}
void AdvCombatEntity::SetMagicPercent(float pPercent)
{
    int tMPMax = mSummedStatistics.GetStatByIndex(STATS_MPMAX);
    SetHealth(tMPMax * pPercent);
}
void AdvCombatEntity::SetStatistic(int pGroupIndex, int pStatisticIndex, int pValue)
{
    CombatStatistics *rStatGroup = GetStatisticsGroup(pGroupIndex);
    rStatGroup->SetStatByIndex(pStatisticIndex, pValue);
}

///--[Core Methods]
void AdvCombatEntity::InflictDamage(int pDamage, int pHealthPriority, int pAdrenalinePriority, int pShieldPriority)
{
    //--Inflicts damage to the entity in order of priority. If a priority is zero, that type is bypassed.
    //  A recursive pattern is used instead of a loop.
    if(pDamage < 1) return;

    //--Damage health.
    if(pHealthPriority > 0 && pHealthPriority >= pAdrenalinePriority && pHealthPriority >= pShieldPriority)
    {
        //--If we we have more health than the incoming damage, just decrement it off.
        int tStartHealth = GetHealth();
        if(tStartHealth >= pDamage)
        {
            SetHealth(tStartHealth - pDamage);
            return;
        }

        //--We have less health than the incoming damage, so set health to zero and re-call
        //  the routine with the other damage types.
        SetHealth(0);
        InflictDamage(pDamage - tStartHealth, 0, pAdrenalinePriority, pShieldPriority);
        return;
    }

    //--Damage adrenaline.
    if(pAdrenalinePriority > 0 && pAdrenalinePriority >= pHealthPriority && pAdrenalinePriority >= pShieldPriority)
    {
        //--If we we have more adrenaline than the incoming damage, just decrement it off.
        int tStartAdrenaline = GetAdrenaline();
        if(tStartAdrenaline >= pDamage)
        {
            SetAdrenaline(tStartAdrenaline - pDamage);
            return;
        }

        //--We have less adrenaline than the incoming damage, so set adrenaline to zero and re-call
        //  the routine with the other damage types.
        SetAdrenaline(0);
        InflictDamage(pDamage - tStartAdrenaline, pHealthPriority, 0, pShieldPriority);
        return;
    }

    //--Damage shields.
    if(pShieldPriority > 0 && pShieldPriority >= pHealthPriority && pShieldPriority >= pAdrenalinePriority)
    {
        //--If we we have more adrenaline than the incoming damage, just decrement it off.
        int tStartShields = GetShields();
        if(tStartShields >= pDamage)
        {
            SetShields(tStartShields - pDamage);
            return;
        }

        //--We have less shields than the incoming damage, so set shields to zero and re-call
        //  the routine with the other damage types.
        SetShields(0);
        InflictDamage(pDamage - tStartShields, pHealthPriority, pAdrenalinePriority, 0);
        return;
    }
}
void AdvCombatEntity::RerunEffects()
{
    //--Whenever a new effect is applied, scrap them all and rerun the effects.
    mTemporaryEffectStatistics.Zero();

    //--Get a list of all effects that point to us.
    StarLinkedList *tEffectList = AdvCombat::Fetch()->GetEffectsReferencing(this);

    //--Run all effects to sum up their bonuses.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)tEffectList->PushIterator();
    while(rEffect)
    {
        rEffect->ApplyStatsToTarget(mUniqueID);
        rEffect = (AdvCombatEffect *)tEffectList->AutoIterate();
    }

    //--Clean.
    delete tEffectList;
}
void AdvCombatEntity::ComputeStatistics()
{
    ///--[Documentation]
    //--Recomputes statistics for a character, assuming that something changed. Or not!
    float tOldHPPct = GetHealthPercent();

    //--Clear the final status holder.
    mSummedStatistics.Zero();

    //--Zero the equipment statistics. Run across all equipment and add them together.
    mEquipmentStatistics.Zero();
    EquipmentSlotPack *rEquipmentPack = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
    while(rEquipmentPack)
    {
        //--Skip slots not used for computation, or that are empty.
        if(!rEquipmentPack->mEquippedItem || !rEquipmentPack->mIsComputedForStats)
        {
        }
        //--Add.
        else
        {
            rEquipmentPack->mEquippedItem->ComputeStatistics();
            rEquipmentPack->mEquippedItem->AppendToStatsPack(mEquipmentStatistics);
        }

        //--Next.
        rEquipmentPack = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
    }

    //--Permanent Passives. These are skills that, once unlocked, apply always and cannot be equipped.
    //  All jobs need to be scanned for these.
    AdvCombatJob *rCheckJob = (AdvCombatJob *)mJobList->PushIterator();
    while(rCheckJob)
    {
        rCheckJob = (AdvCombatJob *)mJobList->AutoIterate();
    }

    //--Now add all the values together.
    mSummedStatistics.AddStatistics(mBaseStatistics);
    mSummedStatistics.AddStatistics(mJobStatistics);
    mSummedStatistics.AddStatistics(mEquipmentStatistics);
    mSummedStatistics.AddStatistics(mScriptStatistics);
    mSummedStatistics.AddStatistics(mPermaEffectStatistics);
    mSummedStatistics.AddStatistics(mTemporaryEffectStatistics);

    //--Clamp to make sure nothing was out of range.
    mSummedStatistics.ClampStatistics();

    //--Finally, reset the HP to the HP percent, in case the max HP change.
    SetHealthPercent(tOldHPPct, false);
}
void AdvCombatEntity::RefreshStatsForUI()
{
    ///--[Documentation]
    //--Resets statistics back to zero, then applies abilities, not effects, for UI purposes. These are placed
    //  in the temporary effect category.

    ///--[HP Storage]
    //--Because there are multiple calls to ComputeStatistics(), it is possible that the HP Percent will get lost
    //  due to accuracy changes when the Max HP is recomputed several times. For example, if the base HP is 168, and
    //  after a buff it becomes 293, then a health percentage of 0.631 will become 0.627, and a point of HP will be lost.
    //  To prevent this, we store the HP values going into and out of this call. If the final HP value is the same, then we
    //  manually reset the HP, we are not expecting a buff to have changed it.
    int tOldHPCur = mHP;
    int tOldHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);

    ///--[Run Refresh]
    mTemporaryEffectStatistics.Zero();
    RunAbilityScripts(ACA_SCRIPT_CODE_GUI_APPLY_EFFECT);
    ComputeStatistics();

    ///--[Recheck HP]
    //--If, after recomputing everything, the HPMax was the same, set the current HP to its original.
    int tCurHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(tOldHPMax == tCurHPMax)
    {
        SetHealth(tOldHPCur);
    }
}

///--[Pointer Routing]
CombatStatistics *AdvCombatEntity::GetStatisticsGroup(int pIndex)
{
    //--Get stat group by index.
    if(pIndex == ADVCE_STATS_BASE)        return &mBaseStatistics;
    if(pIndex == ADVCE_STATS_JOB)         return &mJobStatistics;
    if(pIndex == ADVCE_STATS_EQUIPMENT)   return &mEquipmentStatistics;
    if(pIndex == ADVCE_STATS_SCRIPT)      return &mScriptStatistics;
    if(pIndex == ADVCE_STATS_PERMAEFFECT) return &mPermaEffectStatistics;
    if(pIndex == ADVCE_STATS_TEMPEFFECT)  return &mTemporaryEffectStatistics;
    if(pIndex == ADVCE_STATS_FINAL)       return &mSummedStatistics;

    //--Range errors always return the base statistics.
    fprintf(stderr, "Warning: Statistics index %i is out of range.\n", pIndex);
    return &mBaseStatistics;
}
CombatStatistics *AdvCombatEntity::GetStatisticsGroup(const char *pName)
{
    //--Errors return the base statistics.
    if(!pName)
    {
        fprintf(stderr, "Warning: Null statistics group requested, returning base.\n");
        return &mBaseStatistics;
    }

    //--Groupings by string.
    if(!strcasecmp(pName, "Base"))        return &mBaseStatistics;
    if(!strcasecmp(pName, "Job"))         return &mJobStatistics;
    if(!strcasecmp(pName, "Equipment"))   return &mEquipmentStatistics;
    if(!strcasecmp(pName, "Script"))      return &mScriptStatistics;
    if(!strcasecmp(pName, "PermaEffect")) return &mPermaEffectStatistics;
    if(!strcasecmp(pName, "TempEffect"))  return &mTemporaryEffectStatistics;

    //--If not found, return base statistics.
    fprintf(stderr, "Warning: Statistics group %s not found.\n", pName);
    return &mBaseStatistics;
}
