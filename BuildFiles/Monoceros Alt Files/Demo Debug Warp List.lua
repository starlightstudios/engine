-- |[ ================================= Build Debug Warp List ================================== ]|
--Builds a list of locations the player can warp to. This is only used by the debug menu, as not all levels
-- are considered valid destinations for normal play. Some are used only for cutscenes, so only scripts can
-- warp there unless there is another reason to visit them.

-- |[Arguments]|
--Argument Listing:
-- 0: sChapterName - Name of the chapter to build a list for.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sChapterName = LM_GetScriptArgument(0)

-- |[Common]|
local zaListList = {}

-- |[ ====================== List Building ======================= ]|
--Baseline lists.
local saDebugList = {"Sample"}
local saVillageList = {"VillageA", "VillageB", "VillageC"}
local saBowmillList = {"BowmillA", "BowmillB", "BowmillC", "BowmillD", "BowmillE", "GraveyardA", "GraveyardB"}
local saBowmillCoastList = {"BowmillCoastB"}

--Add the lists to the master list.
table.insert(zaListList, {saDebugList,        "Debug/"})
table.insert(zaListList, {saVillageList,      "Village/"})
table.insert(zaListList, {saBowmillList,      "Bowmill/"})
table.insert(zaListList, {saBowmillCoastList, "BowmillCoast/"})

-- |[ ======================= List Upload ======================== ]|
-- |[Counting]|
--Iterate across the list of lists, counting all the entries:
local iGlobalCount = 0
for i = 1, #zaListList, 1 do
    iGlobalCount = iGlobalCount + #zaListList[i][1]
end

-- |[Finalize List]|
--Set this as the total count.
ADebug_SetProperty("Warp Destinations Total", iGlobalCount)

--Re-iterate, except this time actually add them.
iGlobalCount = 0
for i = 1, #zaListList, 1 do
	
    --Setup.
    local saSublist = zaListList[i][1]
    local sPrefix = zaListList[i][2]
        
	--All members of this sublist have the second element appended before their name.
    for p = 1, #saSublist, 1 do
		ADebug_SetProperty("Warp Destination", iGlobalCount, sPrefix .. saSublist[p])
		iGlobalCount = iGlobalCount + 1
	end
end
