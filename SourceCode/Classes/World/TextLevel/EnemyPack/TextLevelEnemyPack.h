///============================= Text Level Combat Enemy Packages =================================
//--These are the storage structures for enemies in Text Adventure Mode. Both types of combat use the same pack
//  but might not use the same variables.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "TextLevelStructures.h"
#include "DeletionFunctions.h"
#include "StarLinkedList.h"

///======================================== Structures ============================================
///--[CombatEnemyAttackPack]
//--Represents an attack done by an enemy. Enemies can have multiple attacks to choose from based
//  on a weighted roll. Attacks can have an elemental flag and associated attack power. Prep Ticks
//  is how many ticks until the attack goes off, giving a rough idea of DPS.
typedef struct CombatEnemyAttackPack
{
    int mRollWeight;
    int mAttackPower;
    int mPrepTicks;
    uint8_t mElementFlags; //Uses the TL_ELEMENT_FIRE series.
}CombatEnemyAttackPack;

///--[CombatEnemyPack]
//--Represents an enemy in combat in Text Adventure mode. The update and render routines are in WordCombat.cc
//  in the update/render subcategories.
#define CEP_STUN_TICKS 540
#define CEP_HP_TICKS 30
#define CEP_AP_TICKS 5
typedef struct CombatEnemyPack
{
    ///--[Variables]
    //--Common Variables
    int mHP;
    int mHPMax;
    int mFlashTimer;
    int mStunTimer;
    StarBitmap *rImage;

    //--HP Falloff
    int mHPTimer;
    float mHPRender;
    float mHPStart;
    float mHPEnd;

    //--Typing-Mode Variables
    int mAttackPower;
    int mDefensePower;

    //--Sentence-Mode Variables
    int mTotalAttackWeight;
    int mCurrentAttack;
    int mPrepTicksLeft;
    int mPrepTicksMax;
    int mAttackTicks;
    int mAttackTicksNeeded;
    float mAPRender;
    StarLinkedList *mAttackList;

    //--Elemental Weaknesses
    int mElementWeakness[TL_ELEMENTS_TOTAL];

    ///--[Functions]
    //--System
    void Initialize();
    static void DeleteThis(void *pPtr);

    //--Common
    void SetHP(int pHP, int pMaxHP);
    void SetWeakness(int pSlot, int pAmount);

    //--Sentence Mode
    void BootSentenceMode();
    void AddSentenceAttack(int pWeight, int pPower, int pTicks, uint8_t pElementalFlags);
}CombatEnemyPack;
