///====================================== CarnUIHealParty =========================================
//--When the player wants to use items or skills on their party in order to heal them, this UI is
//  brought up. It shows all characters in the active party and allows the user to click them.
//--This is for out-of-combat uses and must be activated from the inventory or skills UI.

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
///--[HealPartyLabel]
//--When the user heals HP/SP etc, labels can appear to announce the effect.
typedef struct HealPartyLabel
{
    //--Members
    int mHideTimer;
    int mLingerTimer;
    int mNumber;
    StarlightColor mColor;
    EasingPack2D mPosition;

    //--Functions
    void Initialize()
    {
        mHideTimer = 0;
        mLingerTimer = 0;
        mNumber = 0;
        mColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
        mPosition.MoveTo(0.0f, 0.0f, 0);
    }
}HealPartyLabel;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CarnUIHealParty : public CarnUICommon
{
    protected:
    ///--[Constants]
    static const int cxLabelMoveTicks   = 30;       //How many ticks a lbel moves for.
    static const int cxLabelFadeTicks   = 15;       //Once a label stops moving it will fade out in this many ticks.
    static const int cxLabelExpireTicks = 30;       //Once a label stops moving it will despawn in this many ticks.

    ///--[System]
    bool mIsClosePending;
    bool mExitsToInventory;
    bool mExitsToSkills;

    ///--[Inventory Item]
    bool mIsInventoryItem;
    int mInventorySlot;
    char *mItemName;

    ///--[Skill]
    bool mIsSkill;
    int mSkillCharacter;
    char *mSkillPath;
    char *mSkillName;

    ///--[Common]
    StarLinkedList *mDescription; //char *, master
    StarLinkedList *mLabels; //HealPartyLabel *, master

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Description;
        StarFont *rFont_Statistic;
        StarFont *rFont_Recovery;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Character;
        StarBitmap *rOverlay_Numbers;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIHealParty();
    virtual ~CarnUIHealParty();
    virtual void Construct();

    //--Public Variables
    static char *xUseItemOnCharacterScript;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    void ClearModeData();
    void SetItemMode(int pSlot);
    void SetSkillMode(int pCharIndex, const char *pSkillName, const char *pDescription);
    void SetExitToInventory();
    void SetExitToSkills();
    void HandleExit();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void ExecuteOn(int pIndex);
    void SpawnLabelOnCharacter(int pCharIndex, int pHealing, const char *pColor, int pDelay);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pAlpha);

    //--Pointer Routing
    //--Static Functions
    static CarnUIHealParty *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnUIHealParty_SetProperty(lua_State *L);
