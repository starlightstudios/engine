//--Base
#include "ZoneEditor.h"

//--Classes
#include "VisualLevel.h"
#include "WADFile.h"
#include "ZoneNode.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CameraManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== System ==============================================
ZoneEditor::ZoneEditor()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_ZONEEDITOR;

    ///--[ZoneEditor]
    //--System
    mRenderNodesInWorld = true;
    mCurrentSavePath = NULL;

    //--Node Listing
    mLastMouseZ = 0;
    mNodeCountOffset = 0;
    mStoredNodeY = 0.0f;
    rActiveNode = NULL;
    rHighlightedNode = NULL;
    mNodeList = new StarLinkedList(true);

    //--Node Editor
    cIndent = 3.0f;
    mIsRenamingMode = false;
    mBoxCoords.SetWH((VIRTUAL_CANVAS_X / 2.0f) - 200.0f, (VIRTUAL_CANVAS_Y / 2.0f) - 200.0f, 600.0f, 400.0f);

    //--"Are You Sure" Box
    mShowConfirmDialog = false;
    mConfirmDialogCoords.SetWH((VIRTUAL_CANVAS_X / 2.0f) - 150.0f, (VIRTUAL_CANVAS_Y / 2.0f) - 50.0f, 300.0f, 100.0f);

    //--Selecting Node Connections
    mIsNodeSelectionMode = ZE_NO_FACING_EDIT;
    rSelectedFacingCommand = NULL;

    //--Selecting Auto Transitions
    mIsAutoTransitionMode = ZE_AUTO_NONE;
    mOriginNodeID = 0;

    //--Selecting Nodes for Deletion
    mIsNodeDeletionMode = ZE_NDEL_NONE;

    //--Font
    mFontSize = ZE_NAME_SIZE_PER;
    mRenderFont = DataLibrary::Fetch()->GetFont("Zone Editor Main");

    //--Command Strings
    rHighlightedCommand = NULL;
    mCommandsList = new StarLinkedList(true);

    //--Rendering Lists
    mNodeListHandle = 0;
    mNodeWorldHandle = 0;
    mNodeEditorHandle = 0;

    //--Public Variables
    mHasMovedPlayer = false;

    ///--[Build Commands]
    //--General Purpose
    mCommandsList->AddElement("X", CreateCommandPack("New Node",    VIRTUAL_CANVAS_X - ZE_NAME_INDENT_X, VIRTUAL_CANVAS_Y - ZE_NAME_INDENT_Y - (ZE_NAME_SIZE_PER *  3), ZE_FUNCTION_NEW_NODE,           mFontSize, true, true, ZE_RENDER_BASE), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Remove Node", VIRTUAL_CANVAS_X - ZE_NAME_INDENT_X, VIRTUAL_CANVAS_Y - ZE_NAME_INDENT_Y - (ZE_NAME_SIZE_PER *  2), ZE_FUNCTION_START_NODE_REMOVAL, mFontSize, true, true, ZE_RENDER_BASE), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Reset Pitch", VIRTUAL_CANVAS_X - ZE_NAME_INDENT_X, VIRTUAL_CANVAS_Y - ZE_NAME_INDENT_Y - (ZE_NAME_SIZE_PER *  1), ZE_FUNCTION_LOOK_STRAIGHT,      mFontSize, true, true, ZE_RENDER_BASE), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Save",        VIRTUAL_CANVAS_X - ZE_NAME_INDENT_X, VIRTUAL_CANVAS_Y - ZE_NAME_INDENT_Y - (ZE_NAME_SIZE_PER *  0), ZE_FUNCTION_SAVE_CONFIRM,       mFontSize, true, true, ZE_RENDER_BASE), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Load",        VIRTUAL_CANVAS_X - ZE_NAME_INDENT_X, VIRTUAL_CANVAS_Y - ZE_NAME_INDENT_Y - (ZE_NAME_SIZE_PER * -1), ZE_FUNCTION_LOAD_CONFIRM,       mFontSize, true, true, ZE_RENDER_BASE), &FreeThis);

    //--Node Editor
    mCommandsList->AddElement("X", CreateCommandPack("Name of Node",    mBoxCoords.mLft + cIndent,          mBoxCoords.mTop + cIndent,                  ZE_FUNCTION_START_RENAME,           mFontSize,         false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Set To Here",     mBoxCoords.mLft + cIndent,          mBoxCoords.mTop + (ZE_NAME_SIZE_PER*2.0f),  ZE_FUNCTION_NODE_SET_POSITION,      mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Move To Node",    mBoxCoords.mLft + cIndent + 150.0f, mBoxCoords.mTop + (ZE_NAME_SIZE_PER*2.0f),  ZE_FUNCTION_NODE_MOVETO_POSITION,   mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Look At Node",    mBoxCoords.mLft + cIndent + 310.0f, mBoxCoords.mTop + (ZE_NAME_SIZE_PER*2.0f),  ZE_FUNCTION_NODE_LOOK_AT,           mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("New Facing",      mBoxCoords.mLft + cIndent,          mBoxCoords.mTop + (ZE_NAME_SIZE_PER*4.0f),  ZE_FUNCTION_NODE_NEW_FACING,        mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Set Connection",  mBoxCoords.mLft + cIndent,          mBoxCoords.mBot - (ZE_NAME_SIZE_PER*-1.0f), ZE_FUNCTION_NODE_CONNECT_FACING,    mFontSize * 0.75f, false, true,  ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Set Auto Trans",  mBoxCoords.mRgt - cIndent,          mBoxCoords.mBot - (ZE_NAME_SIZE_PER*-1.0f), ZE_FUNCTION_NODE_CREATE_TRANSITION, mFontSize * 0.75f, true,  true,  ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Toggle Teleport", mBoxCoords.mRgt - cIndent,          mBoxCoords.mBot - (ZE_NAME_SIZE_PER* 0.0f), ZE_FUNCTION_TOGGLE_TELEPORT,        mFontSize * 0.75f, true,  true,  ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Snap 8",          mBoxCoords.mRgt - cIndent,          mBoxCoords.mBot - (ZE_NAME_SIZE_PER* 1.0f), ZE_SNAP_BY_8,                       mFontSize * 0.75f, true,  true,  ZE_RENDER_NODE_EDITOR), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("Snap 16",         mBoxCoords.mRgt - cIndent,          mBoxCoords.mBot - (ZE_NAME_SIZE_PER* 2.0f), ZE_SNAP_BY_16,                      mFontSize * 0.75f, true,  true,  ZE_RENDER_NODE_EDITOR), &FreeThis);

    //--Confirmation Dialog Box
    mCommandsList->AddElement("X", CreateCommandPack("Yes", mConfirmDialogCoords.mLft +  40.0f, mConfirmDialogCoords.mTop + 50.0f,                        -1, mFontSize, false, false, ZE_RENDER_CONFIRM), &FreeThis);
    mCommandsList->AddElement("X", CreateCommandPack("No",  mConfirmDialogCoords.mLft + 200.0f, mConfirmDialogCoords.mTop + 50.0f, ZE_FUNCTION_CLOSE_CONFIRM, mFontSize, false, false, ZE_RENDER_CONFIRM), &FreeThis);
}
ZoneEditor::~ZoneEditor()
{
    glDeleteLists(mNodeListHandle, 1);
    glDeleteLists(mNodeWorldHandle, 1);
    glDeleteLists(mNodeEditorHandle, 1);
    delete mNodeList;
    delete mCommandsList;
    //delete mRenderFont; No longer has ownership.
}

///===================================== Property Queries =========================================
bool ZoneEditor::IsMouseOverNode(float pMouseX, float pMouseY, float pYCursor, ZoneNode *pNode)
{
    //--Returns whether or not the mouse is over the node in the top-left corner. pYCursor is needed
    //  since the node doesn't know where it is in the list by default.
    if(!pNode || !mRenderFont) return false;

    //--Compute the length of the string. We may need to shorten the name if it's too long.
    char tBuf[64];
    sprintf(tBuf, "%2i: %s", 0, pNode->GetName());
    if((int)strlen(tBuf) >= 27)
    {
        tBuf[25] = '.';
        tBuf[26] = '.';
        tBuf[27] = '.';
        tBuf[28] = '\0';
    }

    float tStringLen = mRenderFont->GetTextWidth(tBuf) * 1.0f;

    //--If the click was on this node...
    return IsPointWithin(pMouseX, pMouseY, ZE_NAME_INDENT_X, pYCursor, ZE_NAME_INDENT_X + tStringLen, pYCursor + ZE_NAME_SIZE_PER);
}
bool ZoneEditor::IsMouseOverCommand(float pMouseX, float pMouseY, CommandPack *pPack)
{
    //--Returns whether or not the mouse is over the command. Commands can be located anywhere on the screen.
    if(!pPack || !mRenderFont) return false;

    //--Compute the length of the string.
    float tStringLen = mRenderFont->GetTextWidth(pPack->mText) * 1.0f;

    //--If the click was on this node...
    return IsPointWithin(pMouseX, pMouseY, pPack->mXPos, pPack->mYPos, pPack->mXPos + tStringLen, pPack->mYPos + ZE_NAME_SIZE_PER);
}
ZoneNode *ZoneEditor::GetHighlightedNode(float pMouseX, float pMouseY)
{
    //--Returns the node which the mouse is currently over, or returns NULL if it's not over a node.
    //  -1 is added to the node offset to account for the fonts rendering top-down. TTF fonts render bottom-up,
    //  but we're using a top-down bitmap font here.
    float tYCursor = mStoredNodeY - ((mNodeCountOffset-1) * ZE_NAME_SIZE_PER);
    ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
    while(rNode)
    {
        //--Success?
        if(IsMouseOverNode(pMouseX, pMouseY, tYCursor, rNode))
        {
            mNodeList->PopIterator();
            return rNode;
        }

        //--Check the next node.
        tYCursor = tYCursor + ZE_NAME_SIZE_PER;
        rNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--All checks failed, mouse is not over a node.
    return NULL;
}
CommandPack *ZoneEditor::GetHighlightedCommand(float pMouseX, float pMouseY)
{
    //--Returns the command which is currently highlighted, or NULL if no commands are highlighted.
    CommandPack *rPack = (CommandPack *)mCommandsList->PushIterator();
    while(rPack)
    {
        //--Range check.
        if(IsMouseOverCommand(pMouseX, pMouseY, rPack))
        {
            //--Some commands require certain cases to be highlighted. Check if this is for the save window.
            if(mShowConfirmDialog && rPack->mRenderFlag == ZE_RENDER_CONFIRM)
            {
                mCommandsList->PopIterator();
                return rPack;
            }
            //--Check if this is NOT for the save window.
            else if(!mShowConfirmDialog && rPack->mRenderFlag != ZE_RENDER_CONFIRM)
            {
                mCommandsList->PopIterator();
                return rPack;
            }

            //--Command is not visible or executing. Ignore it.
        }

        //--Next.
        rPack = (CommandPack *)mCommandsList->AutoIterate();
    }

    //--All checks failed, mouse is not over a command.
    return NULL;
}
uint32_t ZoneEditor::GetNextAvailableNodeID()
{
    //--Scans the node list and returns the next node ID that is not in use. Cannot return 0 under
    //  any circumstances.
    uint32_t tNodeCheck = 1;
    while(true)
    {
        //--Scan the list for the node ID.
        bool tFoundAnything = false;
        ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
        while(rNode)
        {
            //--If the ID matches, flip the flag and break out of the inner loop.
            if(rNode->GetNodeID() == tNodeCheck)
            {
                tFoundAnything = true;
                mNodeList->PopIterator();
                break;
            }

            //--Next.
            rNode = (ZoneNode *)mNodeList->AutoIterate();
        }

        //--If this flag is false, then the ID in question is unused. We can break out.
        if(!tFoundAnything) break;

        //--Otherwise, restart the loop looking for the next index.
        tNodeCheck ++;
    }

    //--Done, pass it back. This is guaranteed to be an unused node ID.
    return tNodeCheck;
}

///======================================= Manipulators ===========================================
void ZoneEditor::SetSavePath(const char *pPath)
{
    ResetString(mCurrentSavePath, pPath);
}
void ZoneEditor::AddNode(const char *pName)
{
    //--Adds a node, and automatically sets it as the active node.
    if(!pName) return;

    //--Setup.
    int i = 0;
    char tBuffer[STD_MAX_LETTERS];
    sprintf(tBuffer, "%s", pName);

    //--Check if a node with that name exists. If it does, keep trying to add new numbers on the end
    //  until we find a unique name.
    while(mNodeList->GetElementByName(tBuffer))
    {
        //--Append the parts together.
        sprintf(tBuffer, "%s %i", pName, i);
        i ++;
    }

    //--Create.
    ZoneNode *nNewnode = new ZoneNode();
    nNewnode->SetName(tBuffer);
    nNewnode->SetUniqueID(GetNextAvailableNodeID());

    //--Register, set as active.
    rActiveNode = nNewnode;
    mNodeList->AddElement(tBuffer, nNewnode, &ZoneNode::DeleteThis);

    //--Flag for modification.
    ClearNodeListList();
    ClearNodeWorldList();
}
void ZoneEditor::SetActiveNodeS(const char *pName)
{
    //--Sets the active node by the name. Sets to NULL if it's not found.
    rActiveNode = (ZoneNode *)mNodeList->GetElementByName(pName);
    ClearNodeEditorList();
}
void ZoneEditor::SetActiveNodeP(void *pPointer)
{
    //--Sets the active node by its pointer. The point *must* be on the node list, even if it's valid!
    //  The exception is NULL, which clears the rActiveNode. If the element is not on the list, the
    //  rActiveNode is set to NULL.
    ClearNodeEditorList();
    rActiveNode = NULL;
    if(!pPointer) return;

    //--Check if it's on the list.
    if(mNodeList->IsElementOnList(pPointer))
    {
        rActiveNode = (ZoneNode *)pPointer;
    }
}
void ZoneEditor::SetNodePosition(float pX, float pY, float pZ)
{
    //--Uses the rActiveNode to set the position.
    if(!rActiveNode) return;
    rActiveNode->SetPosition(pX, pY, pZ);
    ClearNodeEditorList();
}
void ZoneEditor::SetNodePosition(const char *pName, float pX, float pY, float pZ)
{
    //--Sets the position by the name, bypasses the rActiveNode.
    ZoneNode *rCheckNode = (ZoneNode *)mNodeList->GetElementByName(pName);
    if(rCheckNode) rCheckNode->SetPosition(pX, pY, pZ);
    ClearNodeEditorList();
}
void ZoneEditor::DeleteNodeI(int pSlot)
{
    //--Removes the node in question. Also scans all other nodes and removes any connections or
    //  immediate transitions which match the node's ID.
    ZoneNode *rNodeToDelete = (ZoneNode *)mNodeList->GetElementBySlot(pSlot);
    if(!rNodeToDelete) return;

    //--Get the Node's ID. This is how other nodes refer to it.
    uint32_t tTargetNodeID = rNodeToDelete->GetNodeID();

    //--Scan the node list.
    ZoneNode *rCheckNode = (ZoneNode *)mNodeList->PushIterator();
    while(rCheckNode)
    {
        //--Scan the facings.
        int tFacingsTotal = rCheckNode->GetFacingsTotal();
        for(int i = 0; i < tFacingsTotal; i ++)
        {
            //--Get the facing in question.
            FacingInfo *rFacingInfo = rCheckNode->GetFacing(i);
            if(!rFacingInfo) continue;

            //--If it matches the ID, remove this transition.
            if(rFacingInfo->mNodeDestination == tTargetNodeID) rFacingInfo->mNodeDestination = 0;
        }

        //--Look at the automatic transitions.
        int tTransitionTotal = rCheckNode->GetAutoTransitionsTotal();
        for(int i = 0; i < tTransitionTotal; i ++)
        {
            //--Get the transition.
            AutoTransitionInfo *rTransitionInfo = rCheckNode->GetAutoTransition(i);
            if(!rTransitionInfo) continue;

            //--If either the source or destination matches, remove this. These will be flagged for removal
            //  and be cleaned up after the loop is done.
            if(rTransitionInfo->mNodeDestination == tTargetNodeID || rTransitionInfo->mPreviousNode == tTargetNodeID)
            {
                rTransitionInfo->mNodeDestination = 0;
                rTransitionInfo->mPreviousNode = 0;
            }
        }

        //--Remove any transitions that got flagged for removal.
        rCheckNode->RemoveIllegalAutoTransitions();

        //--Next.
        rCheckNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--Finally, delete the node. Also reconstitute the NodeEditor.
    mNodeList->RemoveElementI(pSlot);

    if(rActiveNode == rNodeToDelete) rActiveNode = NULL;
    ResetNodeEditor(rActiveNode);

    //--Flags.
    ClearNodeListList();
    ClearNodeWorldList();
    ClearNodeEditorList();
}
void ZoneEditor::ClearNodeListList()
{
    if(!mNodeListHandle) return;
    glDeleteLists(mNodeListHandle, 1);
    mNodeListHandle = 0;
}
void ZoneEditor::ClearNodeWorldList()
{
    if(!mNodeWorldHandle) return;
    glDeleteLists(mNodeWorldHandle, 1);
    mNodeWorldHandle = 0;
}
void ZoneEditor::ClearNodeEditorList()
{
    if(!mNodeEditorHandle) return;
    glDeleteLists(mNodeEditorHandle, 1);
    mNodeEditorHandle = 0;
}

///======================================= Core Methods ===========================================
void ZoneEditor::LookAtNode(ZoneNode *pNode)
{
    //--Causes the camera to look at the requested node. Does nothing if the node was NULL.
    if(!pNode) return;

    //--Camera position.
    float tXPos, tYPos, tZPos;
    StarCamera3D *rActiveCamera = CameraManager::FetchActiveCamera3D();
    rActiveCamera->Get3DPosition(tXPos, tYPos, tZPos);

    //--Camera is using a different coordinate system.
    tXPos = tXPos * -1.0f;
    tZPos = tZPos * -1.0f;

    //--Position of node.
    float tNodeX = pNode->GetX();
    float tNodeY = pNode->GetY();
    float tNodeZ = pNode->GetZ();

    //--Compute.
    float tAngleH = (atan2f(tNodeY - tYPos, tNodeX - tXPos) * TODEGREE) - 90.0f;
    float tAngleV = (atan2f(tNodeZ - tZPos, GetPlanarDistance(tNodeX, tNodeY, tXPos, tYPos)) * TODEGREE) + 90.0f;

    //--Set.
    rActiveCamera->SetRotation(tAngleV, 0.0f, tAngleH);
}

CommandPack *ZoneEditor::CreateCommandPack(const char *pString, float pX, float pY, int pFunctionCode, float pTextSize, bool pIsRightAligned, bool pIsBottomAligned, uint8_t pRenderFlag)
{
    //--Allocates and returns a new CommandPack with the requested properties. Can never return NULL.
    SetMemoryData(__FILE__, __LINE__);
    CommandPack *nNewPack = (CommandPack *)starmemoryalloc(sizeof(CommandPack));

    //--If the user passes NULL, give it a default name.
    if(!pString)
    {
        strcpy(nNewPack->mText, "New Command Pack");
    }
    else
    {
        strncpy(nNewPack->mText, pString, sizeof(char) * 31);
    }

    //--Other members.
    nNewPack->mXPos = pX;
    nNewPack->mYPos = pY;
    nNewPack->mFunctionCode = pFunctionCode;
    nNewPack->mRenderFlag = pRenderFlag;
    nNewPack->mTextSize = 60.0f;

    //--Alignment.
    if(pIsRightAligned && mRenderFont) nNewPack->mXPos = nNewPack->mXPos - (mRenderFont->GetTextWidth(nNewPack->mText));
    if(pIsBottomAligned) nNewPack->mYPos = nNewPack->mYPos - nNewPack->mTextSize;

    //--All done, pass it back.
    return nNewPack;
}

///=================================== Private Core Methods =======================================
CommandPack *ZoneEditor::FindCommandPack(const char *pName)
{
    //--Finds and returns the command pack with the listed text. Returns NULL if not found. This is
    //  private since external callers should not be messing with commands!
    if(!pName) return NULL;

    //--Iterate.
    CommandPack *rCommandPack = (CommandPack *)mCommandsList->PushIterator();
    while(rCommandPack)
    {
        //--Check.
        if(!strcasecmp(rCommandPack->mText, pName))
        {
            mCommandsList->PopIterator();
            return rCommandPack;
        }

        //--Next.
        rCommandPack = (CommandPack *)mCommandsList->AutoIterate();
    }

    //--All checks failed.
    return NULL;
}

///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void ZoneEditor::Render()
{
    //--[Documentation]
    //--Render the ZoneEditor's information. Does not internally check for hidden status, the caller
    //  should decide whether or not to render the object based on its own variables.
    static bool xAllowLists = false;
    if(!mRenderFont) return;

    //--[Setup]
    //--GL properties.
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Cursor used for the left column.
    float tYCursor = ZE_NAME_INDENT_Y;

    //--[Backing]
    //--Render a darkened overlay. This goes over the whole screen, accounting for fullscreen.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = VIRTUAL_CANVAS_X;
    float cBot = VIRTUAL_CANVAS_Y;

    //--Recompute the borders. We need to make the overlay cover the whole screen, accounting for no letterbox in 3D mode.
    GLOBAL *rGlobal =  Global::Shared();
    if(rGlobal->gWindowWidth > VIRTUAL_CANVAS_X)
    {
        cLft = cLft - (rGlobal->gWindowWidth - VIRTUAL_CANVAS_X) / 2.0f;
        cRgt = cRgt + (rGlobal->gWindowWidth - VIRTUAL_CANVAS_X) / 2.0f;
    }

    //--Render.
    glColor4f(0.0f, 0.0f, 0.0f, 0.75f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--[Camera Position]
    //--Render the current camera information.
    float tXPos, tYPos, tZPos;
    float tXRot, tYRot, tZRot;
    StarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
    rCamera->Get3DPosition(tXPos, tYPos, tZPos);
    rCamera->Get3DRotation(tXRot, tYRot, tZRot);
    mRenderFont->DrawTextArgs(ZE_NAME_INDENT_X, tYCursor += ZE_NAME_SIZE_PER, 0, 1.0f, "Position: %5.0f %5.0f %5.0f", -tXPos, tYPos, -tZPos);
    mRenderFont->DrawTextArgs(ZE_NAME_INDENT_X, tYCursor += ZE_NAME_SIZE_PER, 0, 1.0f, "Rotation: %5.0f %5.0f %5.0f",  tXRot, tYRot,  tZRot);

    //--[Node List]
    //--If lists are allowed and one has been generated...
    if(mNodeListHandle && xAllowLists)
    {
        glCallList(mNodeListHandle);
    }
    //--No list, or lists disallowed. Either way, render normally.
    else
    {
        //--If we don't have a list yet, build one.
        if(!mNodeListHandle && xAllowLists) mNodeListHandle = glGenLists(1);

        //--Assemble the instruction list.
        if(xAllowLists) glNewList(mNodeListHandle, GL_COMPILE_AND_EXECUTE);

        //--Render the node listing.
        int i = 0;
        tYCursor += ZE_NAME_SIZE_PER;
        mRenderFont->DrawTextArgs(ZE_NAME_INDENT_X, tYCursor += ZE_NAME_SIZE_PER, 0, 1.0f, "Node Listing:");
        mStoredNodeY = tYCursor;
        ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
        while(rNode)
        {
            //--Don't render nodes off the top of the list.
            if(i < mNodeCountOffset)
            {
                i ++;
                rNode = (ZoneNode *)mNodeList->AutoIterate();
                continue;
            }

            //--If this node is the active node, render it in red.
            if(rNode == rActiveNode)
            {
                glColor3f(1.0f, 0.0f, 0.0f);
            }
            //--If this is the highlighted node, render it in yellow.
            else if(rNode == rHighlightedNode)
            {
                glColor3f(1.0f, 1.0f, 0.0f);
            }
            //--No special case, render white.
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            //--Get the node name. We may shorten it.
            char tNameBuf[64];
            strcpy(tNameBuf, mNodeList->GetIteratorName());
            if((int)strlen(tNameBuf) >= 23)
            {
                tNameBuf[21] = '.';
                tNameBuf[22] = '.';
                tNameBuf[23] = '.';
                tNameBuf[24] = '\0';
            }

            //--Render.
            mRenderFont->DrawTextArgs(ZE_NAME_INDENT_X, tYCursor += ZE_NAME_SIZE_PER, 0, 1.0f, "%2i: %s", rNode->GetNodeID(), tNameBuf);

            //--Iterate, move the cursor.
            i ++;
            rNode = (ZoneNode *)mNodeList->AutoIterate();
        }

        //--Finish up.
        if(xAllowLists) glEndList();
    }

    //--[Subroutines]
    //--Render the commands.
    RenderCommands(ZE_RENDER_BASE);

    //--Render the node editor. This will do nothing if no node is active.
    RenderNodeEditor(rActiveNode);

    //--Render the confirmation dialog. Auto-hides if not visible.
    RenderConfirmDialog();

    //--[Finish Up]
    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);
}
void ZoneEditor::RenderNodesInWorld()
{
    //--If flagged to, will render the nodes at their world coordinates. This flag can be toggled
    //  with a command in the bottom right.
    static bool xAllowLists = false;
    if(!mRenderNodesInWorld || mNodeList->GetListSize() < 1) return;

    //--If we already have a list for this, just render that and we're done.
    if(mNodeWorldHandle && xAllowLists)
    {
        glCallList(mNodeWorldHandle);
        return;
    }

    //--If we don't have a list yet, build one.
    if(xAllowLists) mNodeWorldHandle = glGenLists(1);

    //--Begin assembling list geometry.
    if(xAllowLists) glNewList(mNodeWorldHandle, GL_COMPILE_AND_EXECUTE);

    //--Nodes never render skybox pixels.
    WADFile::SetupSkyboxStencil();
    WADFile::SetSkyboxStencilState(false);

    //--Nodes render as cubes, facing outwards.
    float cSiz = 10.0f;
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);

    //--Iterate.
    ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
    while(rNode)
    {
        //--Positions.
        float tX = rNode->GetX();
        float tY = rNode->GetY() * -1.0f;
        float tZ = rNode->GetZ() - (cSiz * 1.5f);
        glTranslatef(tX, tY, tZ);

        //--Set color. Text will match.
        rNode->ActivateColorMixer();

        //--Render this as a cube in 3D space.
        static int xCubeList = 0;
        if(xCubeList)
        {
            glCallList(xCubeList);
        }
        else
        {
            xCubeList = glGenLists(1);
            glNewList(xCubeList, GL_COMPILE_AND_EXECUTE);

            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);

                //--Top face.
                glVertex3f( cSiz, -cSiz,  cSiz);
                glVertex3f(-cSiz, -cSiz,  cSiz);
                glVertex3f(-cSiz,  cSiz,  cSiz);
                glVertex3f( cSiz,  cSiz,  cSiz);

                //--North face.
                glVertex3f(-cSiz, -cSiz,  cSiz);
                glVertex3f( cSiz, -cSiz,  cSiz);
                glVertex3f( cSiz, -cSiz, -cSiz);
                glVertex3f(-cSiz, -cSiz, -cSiz);

                //--East face.
                glVertex3f( cSiz, -cSiz,  cSiz);
                glVertex3f( cSiz,  cSiz,  cSiz);
                glVertex3f( cSiz,  cSiz, -cSiz);
                glVertex3f( cSiz, -cSiz, -cSiz);

                //--South face.
                glVertex3f( cSiz,  cSiz,  cSiz);
                glVertex3f(-cSiz,  cSiz,  cSiz);
                glVertex3f(-cSiz,  cSiz, -cSiz);
                glVertex3f( cSiz,  cSiz, -cSiz);

                //--West face.
                glVertex3f(-cSiz,  cSiz,  cSiz);
                glVertex3f(-cSiz, -cSiz,  cSiz);
                glVertex3f(-cSiz, -cSiz, -cSiz);
                glVertex3f(-cSiz,  cSiz, -cSiz);

                //--Bottom face.
                glVertex3f(-cSiz, -cSiz, -cSiz);
                glVertex3f( cSiz, -cSiz, -cSiz);
                glVertex3f( cSiz,  cSiz, -cSiz);
                glVertex3f(-cSiz,  cSiz, -cSiz);

            //--Clean.
            glEnd();
            glEndList();
        }
        glTranslatef(-tX, -tY, -tZ);

        //--Next.
        rNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_CULL_FACE);
    WADFile::DeactivateSkyboxStencil();

    //--Finish the list.
    if(xAllowLists) glEndList();
}
void ZoneEditor::RenderNodeNames()
{
    //--Renders the names of all the nodes right above them. These are billboards, so they can't be
    //  done in a list format since billboards unrotate the camera.
    if(!mRenderNodesInWorld || mNodeList->GetListSize() < 1 || !mRenderFont) return;

    //--GL Setup.
    glDisable(GL_BLEND);

    //--Camera properties.
    float tPitch, tYaw, tRoll;
    StarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
    rCamera->Get3DRotation(tPitch, tYaw, tRoll);

    //--Nodes never render skybox pixels.
    WADFile::SetupSkyboxStencil();
    WADFile::SetSkyboxStencilState(false);

    //--Render spacing. This should be the same as the cube version above.
    float cSiz = 10.0f;
    float cTextSize = 1.0f;
    //float cInd = 0.40f;
    float cVOffset = -0.40f;
    //float cBak = 0.01f; //Causes the backing text to render behind the front text. Avoids Z-fighting.

    //--Iterate.
    ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
    while(rNode)
    {
        //--Positions.
        float tX = rNode->GetX();
        float tY = rNode->GetY() * -1.0f;
        float tZ = rNode->GetZ() - (cSiz * 1.5f);
        glTranslatef(tX, tY, tZ);

        //--Render the name above it. It's not a billboard since that'd make generating a glList impossible.
        if(true)
        {
            //--Setup. Rotate so it's "up" as opposed to being on the X/Y plane.
            glTranslatef(0.0f, 0.0f, 12.0f);
            glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

            //--Centering position.
            float cCenterX = (mRenderFont->GetTextWidth(rNode->GetName()) * -0.50f * cTextSize);

            //--Unrotate the camera's Z axis. Note that, because we rotate the pitch, it's now on the Y axis,
            //  but it's still the Roll value.
            glRotatef(tRoll, 0.0f, 1.0f, 0.0f);

            //--Render a constrasting base.
            //rNode->ActivateReverseColorMixer();
            //glTranslatef(cCenterX + cInd, cVOffset + cInd, -cBak);
            //    mRenderFont->DrawText(0.0f, 0.0f, 0, rNode->GetName());
            //glTranslatef(-cCenterX - cInd, -cVOffset - cInd, cBak);

            //--Render the text.
            rNode->ActivateColorMixer();
            glTranslatef(cCenterX, cVOffset, 0.0f);
                mRenderFont->DrawText(0.0f, 0.0f, 0, rNode->GetName());
            glTranslatef(-cCenterX, cVOffset, 0.0f);

            //--Unrotate.
            glRotatef(tRoll, 0.0f, -1.0f, 0.0f);

            //--Clean.
            glRotatef(-90.0f, -1.0f, 0.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, -12.0f);
        }

        //--Clean position.
        glTranslatef(-tX, -tY, -tZ);

        //--Next.
        rNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_BLEND);
    WADFile::DeactivateSkyboxStencil();
}

///====================================== Pointer Routing =========================================
ZoneNode *ZoneEditor::GetNode(uint32_t pID)
{
    //--Returns the node with the matching ID, or NULL if not found. Nodes can never have an ID that
    //  is zero, that is deliberately set as "No Node".
    if(!pID) return NULL;

    //--Scan.
    ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
    while(rNode)
    {
        if(rNode->GetNodeID() == pID)
        {
            mNodeList->PopIterator();
            return rNode;
        }
        rNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--Node not found.
    return NULL;
}
ZoneNode *ZoneEditor::GetNode(const char *pName)
{
    //--Returns the node with the matching name. The node's internal name is checked, not its list name.
    if(!pName) return NULL;

    //--Scan.
    ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
    while(rNode)
    {
        if(!strcasecmp(pName, rNode->GetName()))
        {
            mNodeList->PopIterator();
            return rNode;
        }
        rNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--Node not found.
    return NULL;
}
ZoneNode *ZoneEditor::GetNodeNearestTo(float pX, float pY)
{
    //--Finds and returns the node closest to the given position by planar distance. Can return NULL if
    //  there are no nodes at all.
    if(mNodeList->GetListSize() < 1) return NULL;

    //--Setup.
    float tLowestDistance = 0.0f;
    ZoneNode *rReturnNode = NULL;

    //--Scan.
    ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
    while(rNode)
    {
        //--Compute the distance.
        float tOurDistance = GetPlanarDistance(pX, pY, rNode->GetX(), rNode->GetY());

        //--First pass, set this as the node always. Otherwise, we must be nearer than the previous node.
        if(!rReturnNode || tOurDistance < tLowestDistance)
        {
            rReturnNode = rNode;
            tLowestDistance = tOurDistance;
        }

        //--Next.
        rNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--Pass back the return node.
    return rReturnNode;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
