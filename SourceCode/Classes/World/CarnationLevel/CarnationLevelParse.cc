//--Base
#include "CarnationLevel.h"

//--Classes
#include "TiledRawData.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///========================================== Parser ==============================================
void CarnationLevel::ExecCarnationParse(const char *pPath)
{
    ///--[Documentation]
    //--Given a file path to an slf mapdata file, parses that file and stores the information in a
    //  set of variables that can be queried by Lua files in order to figure out what rooms are where,
    //  how they connect, collisions, etc.
    //--This largely uses the basic TiledLevel parsers with some extra object processing. The function
    //  DeallocateCarnationParse() should be called to clear the memory when lua files are done with it.
    if(!pPath) return;

    ///--[Setup Lists]
    //--Create the lists necessary for querying.
    mQuerySLFRoomInfo = new StarLinkedList(true);
    mQuerySLFConnectionInfo = new StarLinkedList(true);

    ///--[Parse File]
    //--Open the file in the SLM and make sure it exists.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    rSLM->Open(pPath);
    if(!rSLM->IsFileOpen()) return;

    //--Pass it to the base class for parsing. This will call ParseObjectData() below.
    ParseFile(rSLM);

    ///--[Check Validity]
    //--Check to make sure a name, image, and other variables got set. If anything didn't, bark a
    //  warning so the user knows what they need to add.
    bool tIsValid = true;
    if(!mQuerySLFName) tIsValid = false;
    if(!mQuerySLFImage) tIsValid = false;
    if(mQuerySLFRoomInfo->GetListSize() < 1) tIsValid = false;

    //--Finish.
    rSLM->Close();

    ///--[Report Errors]
    //--If the entry is not valid, we report the needed errors and then clear the data.
    if(!tIsValid)
    {
        //--Base.
        DebugManager::ForcePrint("CarnationLevel::ExecCarnationParse(): Error. During parse of %s, one or more values failed to populate.\n", pPath);

        //--Specific errors.
        if(!mQuerySLFName)
            DebugManager::ForcePrint(" No internal area name found.\n");
        if(!mQuerySLFImage)
            DebugManager::ForcePrint(" No display image path found.\n");
        if(mQuerySLFRoomInfo->GetListSize() < 1)
            DebugManager::ForcePrint(" There were no rooms in the parsed data.\n");

        //--Deallocate.
        DeallocateCarnationParse();
    }
}

void CarnationLevel::DeallocateCarnationParse()
{
    ///--[Documentation]
    //--Clears all memory associated with the queryable SLF data.
    free(mQuerySLFName);
    free(mQuerySLFImage);
    delete mQuerySLFRoomInfo;
    delete mQuerySLFConnectionInfo;
    for(int i = 0; i < mTilesetsTotal; i ++) TiledRawData::DeleteTilesetPack(&mTilesetPacks[i]);
    free(mTilesetPacks);
    delete mTileLayers;
    delete mObjectData;
    delete mImageData;
    for(int i = 0; i < mCollisionLayersTotal; i ++) CollisionPack::FreeThis(&mCollisionLayers[i]);
    free(mCollisionLayers);

    //--Zero it.
    mQuerySLFName = NULL;
    mQuerySLFImage = NULL;
    mQuerySLFRoomInfo = NULL;
    mQuerySLFConnectionInfo = NULL;
    mTilesetsTotal = 0;
    mTilesetPacks = NULL;
    mTileLayers = NULL;
    mCollisionLayersTotal = 0;
    mCollisionLayers = NULL;

    //--Reinit some of the TiledLevel variables.
    mTileLayers = new StarLinkedList(true);
    mObjectData = new StarLinkedList(true);
    mImageData = new StarLinkedList(true);
}

///====================================== Object Handling =========================================
void CarnationLevel::ParseObjectData()
{
    ///--[Documentation]
    //--Once object data is stored in ParseFile(), this function is called which goes across all the
    //  object packs and stores them as necessary for query by Lua files.
    ObjectInfoPack *rObjectPack = (ObjectInfoPack *)mObjectData->PushIterator();
    while(rObjectPack)
    {
        HandleObject(rObjectPack);
        rObjectPack = (ObjectInfoPack *)mObjectData->AutoIterate();
    }
}
void CarnationLevel::HandleObject(ObjectInfoPack *pPack)
{
    ///--[Documentation]
    //--Given an object pack storing information, creates any needed objects for later query.
    if(!pPack) return;

    //--Fast-access pointers.
    const char *rType = pPack->mType;

    ///--[MapInfo]
    //--Contains info about the map, like its name and associated image. Only one needs to exist, if multiple
    //  exist then the values will be the ones of the last one parsed.
    if(!strcasecmp(rType, "MapInfo"))
    {
        //--Property Check.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Name. Internal name of the object.
            if(!strcasecmp(rKey, "Name"))
            {
                ResetString(mQuerySLFName, rVal);
            }
            //--Image. DL Path of the image to render on the minimap.
            else if(!strcasecmp(rKey, "Image"))
            {
                ResetString(mQuerySLFImage, rVal);
            }
        }
    }
    ///--[Room]
    //--A room is something the player can move between. This stores a set of position information, a name,
    //  and some encounter data.
    else if(!strcasecmp(rType, "Room"))
    {
        //--Create and register a new room.
        CarnationRoom *nRoom = (CarnationRoom *)starmemoryalloc(sizeof(CarnationRoom));
        nRoom->Initialize();
        strcpy(nRoom->mName, pPack->mName);

        //--Position is divided by tile size.
        nRoom->mX = (int)(pPack->mX / 16.0f);
        nRoom->mY = (int)(pPack->mY / 16.0f);
        //fprintf(stderr, "Room %s at %i %i\n", nRoom->mName, nRoom->mX, nRoom->mY);

        //--Register.
        mQuerySLFRoomInfo->AddElementAsTail(nRoom->mName, nRoom, &FreeThis);

        //--Property Check.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Layout for standardized connection placement.
            if(!strcasecmp(rKey, "Layout"))
            {
                strcpy(nRoom->mLayout, rVal);
            }
            //--Second half of the path to the background.
            else if(!strcasecmp(rKey, "BGPath"))
            {
                strcpy(nRoom->mBGPath, rVal);
            }
            //--Danger level, an integer representing how much the encounter rating increases per move.
            else if(!strcasecmp(rKey, "Danger"))
            {
                nRoom->mDanger = atoi(rVal);
            }
            //--Enemy data. Unused.
            else if(!strcasecmp(rKey, "Enemy"))
            {
                strcpy(nRoom->mEnemy, rVal);
            }
            //--Music. Name of the song that plays in this room.
            else if(!strcasecmp(rKey, "Music"))
            {
                strcpy(nRoom->mMusic, rVal);
            }
            else if(!strcasecmp(rKey, "IsSafe"))
            {
                nRoom->mIsSafe = (!strcasecmp(rVal, "true"));
            }
            else if(!strcasecmp(rKey, "ParticlePath"))
            {
                strcpy(nRoom->mParticlePath, rVal);
            }
            else if(!strcasecmp(rKey, "ParticleRate"))
            {
                nRoom->mParticleRate = atoi(rVal);
            }
        }
    }
    ///--[Connection]
    //--Connections automatically spawn traversable examinables between two rooms if they touch.
    else if(!strcasecmp(rType, "Connection"))
    {
        //--Create and register a new connection.
        CarnationConnection *nConnection = (CarnationConnection *)starmemoryalloc(sizeof(CarnationConnection));
        nConnection->Initialize();
        strcpy(nConnection->mName, pPack->mName);

        //--Position is divided by tile size.
        nConnection->mLft = (int)(pPack->mX / 16.0f);
        nConnection->mTop = (int)(pPack->mY / 16.0f);
        nConnection->mRgt = (int)((pPack->mX + pPack->mW) / 16.0f);
        nConnection->mBot = (int)((pPack->mY + pPack->mH) / 16.0f);

        //--Register.
        mQuerySLFConnectionInfo->AddElementAsTail(nConnection->mName, nConnection, &FreeThis);
    }
    ///--[Error]
    //--Error, type not found.
    else
    {
        DebugManager::ForcePrint("CarnationLevel::HandleObject(): Warning. Unable to resolve object type %s\n", rType);
    }
}


///===================================== Property Queries =========================================
const char *CarnationLevel::GetParsedLevelName()
{
    if(!mQuerySLFName) return "Null";
    return mQuerySLFName;
}
const char *CarnationLevel::GetParsedImagePath()
{
    if(!mQuerySLFImage) return "Null";
    return mQuerySLFImage;
}
int CarnationLevel::GetParsedXSize()
{
    return mXSize;
}
int CarnationLevel::GetParsedYSize()
{
    return mYSize;
}
int CarnationLevel::GetParsedCollision(int pX, int pY)
{
    return GetClipAt(pX, pY, 0, 0);
}
int CarnationLevel::GetParsedRoomCount()
{
    if(!mQuerySLFRoomInfo) return 0;
    return mQuerySLFRoomInfo->GetListSize();
}
CarnationRoom *CarnationLevel::GetParsedRoomEntry(int pSlot)
{
    if(!mQuerySLFRoomInfo) return NULL;
    return (CarnationRoom *)mQuerySLFRoomInfo->GetElementBySlot(pSlot);
}
const char *CarnationLevel::GetParsedConnectionInDirectionFrom(int pSlot, int pDirection)
{
    ///--[Documentation]
    //--Checks to see if any connections coincide with the room in the given slot's borders in the
    //  given direction. If so, returns the room on the other side of the connection's name. If not,
    //  returns "Null".
    if(!mQuerySLFRoomInfo || !mQuerySLFConnectionInfo) return "Null";

    //--Make sure the room exists.
    CarnationRoom *rRoom = (CarnationRoom *)mQuerySLFRoomInfo->GetElementBySlot(pSlot);
    if(!rRoom) return "Null";

    //--Scan connections:
    CarnationConnection *rConnection = (CarnationConnection *)mQuerySLFConnectionInfo->PushIterator();
    while(rConnection)
    {
        //--Direction: North.
        if(pDirection == DIR_NORTH)
        {
            //--On match, scan rooms for the other side of this connection.
            if(rConnection->mBot == rRoom->mY && rConnection->mLft == rRoom->mX)
            {
                mQuerySLFConnectionInfo->PopIterator();
                return GetRoomOnOtherSideOfConnection(rConnection->mLft, rConnection->mTop, pDirection);
            }
        }
        //--Direction: East.
        else if(pDirection == DIR_EAST)
        {
            if(rConnection->mLft == rRoom->mX + 1 && rConnection->mTop == rRoom->mY)
            {
                mQuerySLFConnectionInfo->PopIterator();
                return GetRoomOnOtherSideOfConnection(rConnection->mRgt, rConnection->mTop, pDirection);
            }
        }
        //--Direction: South.
        else if(pDirection == DIR_SOUTH)
        {
            if(rConnection->mTop == rRoom->mY + 1 && rConnection->mLft == rRoom->mX)
            {
                mQuerySLFConnectionInfo->PopIterator();
                return GetRoomOnOtherSideOfConnection(rConnection->mLft, rConnection->mBot, pDirection);
            }
        }
        //--Direction: West.
        else if(pDirection == DIR_WEST)
        {
            if(rConnection->mRgt == rRoom->mX && rConnection->mTop == rRoom->mY)
            {
                mQuerySLFConnectionInfo->PopIterator();
                return GetRoomOnOtherSideOfConnection(rConnection->mLft, rConnection->mTop, pDirection);
            }
        }

        //--Next.
        rConnection = (CarnationConnection *)mQuerySLFConnectionInfo->AutoIterate();
    }

    //--No matches found.
    return "Null";
}
const char *CarnationLevel::GetRoomOnOtherSideOfConnection(int pX, int pY, int pDirection)
{
    ///--[Documentation]
    //--Given an X/Y position (what pPosition is depends on pDirection), finds a room that has a shared
    //  edge and returns its name.
    CarnationRoom *rRoom = (CarnationRoom *)mQuerySLFRoomInfo->PushIterator();
    while(rRoom)
    {
        //--Skip if this is the room in question:
        //fprintf(stderr, "  Alternate: %s. %i %i\n", rRoom->mName, rRoom->mY, rConnection->mTop);

        //--North. We are checking if the room's south side matches the position.
        if(pDirection == DIR_NORTH)
        {
            if(rRoom->mX == pX && rRoom->mY+1 == pY)
            {
                mQuerySLFRoomInfo->PopIterator();
                return rRoom->mName;
            }
        }
        //--East. We are checking if the room's west side matches the position.
        else if(pDirection == DIR_EAST)
        {
            if(rRoom->mX == pX && rRoom->mY == pY)
            {
                mQuerySLFRoomInfo->PopIterator();
                return rRoom->mName;
            }
        }
        //--South. We are checking if the room's north side matches the position.
        else if(pDirection == DIR_SOUTH)
        {
            if(rRoom->mX == pX && rRoom->mY == pY)
            {
                mQuerySLFRoomInfo->PopIterator();
                return rRoom->mName;
            }
        }
        //--West. We are checking if the room's east side matches the position.
        else if(pDirection == DIR_WEST)
        {
            if(rRoom->mX+1 == pX && rRoom->mY == pY)
            {
                mQuerySLFRoomInfo->PopIterator();
                return rRoom->mName;
            }
        }

        //--Next.
        rRoom = (CarnationRoom *)mQuerySLFRoomInfo->AutoIterate();
    }

    //--No matches.
    return "Null";
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
