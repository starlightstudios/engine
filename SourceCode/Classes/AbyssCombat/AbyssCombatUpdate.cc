//--Base
#include "AbyssCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"
#include "AdventureMenu.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///--[Debug]
//#define ABYSSCOMBAT_UPDATE_DEBUG
#ifdef ABYSSCOMBAT_UPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Update ==============================================
void AbyssCombat::Update()
{
    ///--[Documentation and Setup]
    //--Handles the combat update. Combat runs a set of timers then handles controls or logic updates, depending
    //  on its internal state.

    ///--[Emergency Escape]
    //--If the player pushed Escape to break out of a busted combat:
    if(mIsEmergencyEscape && mIsActive && !mDisallowEmergencyEscape)
    {
        //--Fire exiting the battle in defeat.
        if(ControlManager::Fetch()->IsFirstPress("WHI Emergency Escape"))
        {
            //--Flag.
            mWasLastDefeatSurrender = true;

            //--If there is an override script, handle it here:
            if(mCurrentDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentDefeatScript, 1, "S", "Emergency");
            }
            //--Run the standard defeat script if it exists:
            else if(mStandardDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardDefeatScript, 1, "S", "Emergency");
            }

            //--All entities handle the post-battle code.
            AdventureLevel *rLevel = AdventureLevel::Fetch();
            if(rLevel) rLevel->RunPostCombatOnEntities();

            //--Return characters to their starting jobs and handle stat changes.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->HandleCombatEnd();
                rEntity->RevertToCombatStartJob();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--In all cases, deactivate this object.
            Deactivate();
            AudioManager::Fetch()->StopAllMusic();
            CleanDataLibrary();
        }
        //--Cancel.
        else if(ControlManager::Fetch()->IsFirstPress("Activate"))
        {
            mIsEmergencyEscape = false;
        }
        return;
    }
    //--Player can push Escape to enter emergency exit mode:
    else if(mIsActive)
    {
        if(ControlManager::Fetch()->IsFirstPress("WHI Emergency Escape"))
        {
            mIsEmergencyEscape = true;
            return;
        }
    }

    ///--[Base Call]
    AdvCombat::Update();

    ///--[Fast-Access Pointers]
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();

    ///--[Timers]
    //--Starts at zero. As soon as pulsing ends, counts up. Causes a fade-in.
    if((rActiveLevel && rActiveLevel->IsPulsing()) || mIsIntroduction)
    {
        mStartBattleFadeTimer = 0;
    }
    else if(mStartBattleFadeTimer < ABYCOM_BATTLE_START_FADE_TICKS)
    {
        mStartBattleFadeTimer ++;
    }

    //--Standard timers.
    StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();
    StarUIPiece::StandardTimer(mCommandTimer,          0, ABYCOM_COMMAND_SCROLL_TICKS, mShowCommandWindow && !mIsSelectingTargets);
    StarUIPiece::StandardTimer(mCommandSecondaryTimer, 0, ABYCOM_COMMAND_SCROLL_TICKS, mShowCommandWindow && rPlayerStackList != mCommandList && !mIsSelectingTargets);
    StarUIPiece::StandardTimer(mTutorialTimer,         0, ABYCOM_TUTORIAL_FADE_TICKS,  mIsShowingTutorialOverlay);
    if(mTutorialTimer < 1)
    {
        rTutorialOverlayCur = NULL;
    }
    else if(mTutorialTimer >= ABYCOM_TUTORIAL_FADE_TICKS)
    {
        rTutorialOverlayPrv = NULL;
    }
    if(mCommandScrollTimer < ABYCOM_COMMAND_NEWTURN_INDICATOR_TICKS) mCommandScrollTimer ++;

    //--Disabled timers.
    for(int i = 0; i < ABYCOM_PLAYER_PARTY_MAX_SIZE; i ++)
    {
        StarUIPiece::StandardTimer(mDisableTimers[i], 0, ABYCOM_DISABLE_FADE_TICKS, mIsMemberDisabled[i]);
    }

    //--Glow timer. Cycles.
    mCommandGlowTimer ++;
    if(mCommandGlowTimer >= ABYCOM_COMMAND_GLOW_TICKS * 2) mCommandGlowTimer = 0;

    //--Callouts.
    UpdateCallouts();
}
void AbyssCombat::HandlePlayerControls(bool pAnyEntitiesMoving)
{
    ///--[Documentation and Setup]
    //--Handles player controls. Overrides the base version, which uses cards/an ability grid. This
    //  reroutes control handling to the command window, if applicable.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    StarUIPiece::StandardTimer(mAbilityDescriptionTimer, 0, ABYCOM_ABILITY_DESCRIPTION_TICKS, mShowAbilityDescription);

    ///--[Tutorial]
    //--If the tutorial is showing, it disables all other input. Any keypress will instead dismiss or
    //  advance the tutorial.
    if(mIsShowingTutorialOverlay)
    {
        if(rControlManager->IsAnyKeyPressed() && mTutorialTimer > (ABYCOM_TUTORIAL_FADE_TICKS / 4)) mIsShowingTutorialOverlay = false;
        return;
    }

    //--Other blockers:
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rWorldDialogue->IsVisible()) return;
    if(rCutsceneManager->HasAnyEvents()) return;

    ///--[First tick]
    //--If this is the first tick that the player has gained control. It is implied, as this is the first
    //  possible tick, that target selection is not taking place.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(mActionFirstTick)
    {
        //--Unset.
        mShowAbilityDescription = false;
        mActionFirstTick = false;

        //--Memory cursor.
        if(mMemoryCursor)
        {
            //--Memory cursor for the primary menu is stored as the CardX value.
            if(rActingEntity) mPlayerCommandBaseCursor = rActingEntity->GetMemoryCardX();

            //--Clamp.
            StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();
            int tCommandSize = rPlayerStackList->GetListSize();
            if(mPlayerCommandBaseCursor >= tCommandSize) mPlayerCommandBaseCursor = 0;
        }

        //--Set description.
        ReresolveDescriptionAbility();
    }

    ///--[Combat Inspector]
    //--Not used in this game mode.

    ///--[Description Toggle]
    //--Shows description of the selected ability. Can be toggled.
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowAbilityDescription = !mShowAbilityDescription;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[UI Toggle]
    //--The player can toggle the UI on and off at any time.
    if(rControlManager->IsFirstPress("F4"))
    {
        mHideCombatUI = !mHideCombatUI;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Debug]
    //--Clicking the mouse in certain areas can trigger diagnostics.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--If the callout handler is NULL, do nothing.
        if(!AdvCombat::xDebugCalloutPath) return;

        //--Variables.
        int tClickIndex = -1;

        //--Get mouse.
        int tMouseX, tMouseY, tMouseZ;
        rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

        //--If done in the bottom-left corner, switch the callout mode.
        if(IsPointWithin(tMouseX, tMouseY, 0.0f, 668.0f, 100.0f, 768.0f))
        {
            LuaManager::Fetch()->ExecuteLuaFile(AdvCombat::xDebugCalloutPath, 1, "N", -1.0f);
            return;
        }
        //--Clicking the leftmost heart toggles Izana's defeat state.
        if(IsPointWithinWH(tMouseX, tMouseY, 51.0f, 655.0f, 35.0f, 35.0f))
        {
            LuaManager::Fetch()->ExecuteLuaFile(AdvCombat::xDebugCalloutPath, 1, "N", -2.0f);
            return;
        }
        //--Clicking the leftmost heart toggles Caelyn's defeat state.
        if(IsPointWithinWH(tMouseX, tMouseY, 385.0f, 655.0f, 35.0f, 35.0f))
        {
            LuaManager::Fetch()->ExecuteLuaFile(AdvCombat::xDebugCalloutPath, 1, "N", -3.0f);
            return;
        }
        //--Clicking the leftmost heart toggles Cyrano's defeat state.
        if(IsPointWithinWH(tMouseX, tMouseY, 719.0f, 655.0f, 35.0f, 35.0f))
        {
            LuaManager::Fetch()->ExecuteLuaFile(AdvCombat::xDebugCalloutPath, 1, "N", -4.0f);
            return;
        }

        //--Top character. Wider.
        if(IsPointWithin(tMouseX, tMouseY, 0.0f, 0.0f, 105.0f, 68.0f))
        {
            tClickIndex = 0;
        }
        //--Second-top character. Wider then the others.
        else if(IsPointWithin(tMouseX, tMouseY, 0.0f, 68.0f, 83.0f, 68.0f + 53.0f))
        {
            tClickIndex = 1;
        }
        //--Third-top character. Still very slightly wider!
        else if(IsPointWithin(tMouseX, tMouseY, 0.0f, 121.0f, 66.0f, 121.0f + 42.0f))
        {
            tClickIndex = 2;
        }
        //--After this point all hitboxes are 41px high.
        else
        {
            //--Must be within 63 px of the left edge:
            if(tMouseX <= 63.0f)
            {
                tClickIndex = (int)((tMouseY - 163.0f) / 41.0f) + 3;
            }
        }

        //--If the click index is still -1, no hit was found.
        if(tClickIndex == -1) return;

        //--Locate the entity on the turn order list in the given slot.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(tClickIndex);
        if(!rEntity) return;

        //--Entity exists. Call the callout script with its ID.
        int tEntityID = rEntity->GetID();
        LuaManager::Fetch()->ExecuteLuaFile(AdvCombat::xDebugCalloutPath, 1, "N", (float)tEntityID);
        return;
    }

    ///--[Ability Selection]
    //--Selecting which ability to use this action.
    if(!mIsSelectingTargets)
    {
        ///--[Setup]
        int tUseCursor = 0;
        bool tRecompute = false;

        ///--[Resolve List]
        //--Resolve which command list is in use.
        StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();

        ///--[Base Command List]
        //--Always consists of, at most, six commands.
        if(rPlayerStackList == mCommandList)
        {
            //--Up and down cycle the cursor by 1.
            if(rControlManager->IsFirstPress("Up"))
            {
                //--Cycle.
                mPlayerCommandBaseCursor -= 1;

                //--Range check.
                if(mPlayerCommandBaseCursor < 0) mPlayerCommandBaseCursor += rPlayerStackList->GetListSize();
                if(mPlayerCommandBaseCursor < 0) mPlayerCommandBaseCursor = 0;
                tRecompute = true;
            }
            else if(rControlManager->IsFirstPress("Down"))
            {
                //--Cycle.
                mPlayerCommandBaseCursor += 1;

                //--Range check.
                if(mPlayerCommandBaseCursor >= rPlayerStackList->GetListSize()) mPlayerCommandBaseCursor -= rPlayerStackList->GetListSize();
                if(mPlayerCommandBaseCursor >= rPlayerStackList->GetListSize()) mPlayerCommandBaseCursor = rPlayerStackList->GetListSize() - 1;
                if(mPlayerCommandBaseCursor < 0) mPlayerCommandBaseCursor = 0;
                tRecompute = true;
            }

            //--Flags.
            tUseCursor = mPlayerCommandBaseCursor;

            //--Store this character's cursor in memory.
            if(rActingEntity) rActingEntity->SetMemoryCard(mPlayerCommandBaseCursor);
        }
        ///--[Derived List]
        else
        {
            //--Up and down cycle the cursor by 2.
            if(rControlManager->IsFirstPress("Up"))
            {
                //--Store the modulus.
                //int tCurModulus = mPlayerCommandCursor % 2;
                //int tMaxModulus = rPlayerStackList->GetListSize() % 2;

                //--Cycle.
                mPlayerCommandCursor -= 2;

                //--Range check.
                if(mPlayerCommandCursor < 0)
                {
                    while(mPlayerCommandCursor + 2 < rPlayerStackList->GetListSize()) mPlayerCommandCursor += 2;
                }
                if(mPlayerCommandCursor < 0) mPlayerCommandCursor = 0;
                tRecompute = true;
            }
            else if(rControlManager->IsFirstPress("Down"))
            {
                //--Store the modulus.
                int tCurModulus = mPlayerCommandCursor % 2;

                //--Cycle.
                mPlayerCommandCursor += 2;

                //--Range check.
                if(mPlayerCommandCursor >= rPlayerStackList->GetListSize()) mPlayerCommandCursor = tCurModulus;
                if(mPlayerCommandCursor < 0) mPlayerCommandCursor = 0;
                tRecompute = true;
            }

            //--Left and right cycle it by 1.
            if(rControlManager->IsFirstPress("Left"))
            {
                //--Cycle.
                int tPreMod = mPlayerCommandCursor % 2;
                if(tPreMod == 0)
                {
                    mPlayerCommandCursor += 1;
                }
                else
                {
                    mPlayerCommandCursor --;
                }

                //--Range check.
                if(mPlayerCommandCursor >= rPlayerStackList->GetListSize()) mPlayerCommandCursor = rPlayerStackList->GetListSize() - 1;
                if(mPlayerCommandCursor < 0) mPlayerCommandCursor = 0;
                tRecompute = true;
            }
            //--Right, cycle up by 1.
            else if(rControlManager->IsFirstPress("Right"))
            {
                //--Cycle.
                int tPreMod = mPlayerCommandCursor % 2;
                if(tPreMod == 1)
                {
                    mPlayerCommandCursor -= 1;
                }
                else
                {
                    mPlayerCommandCursor ++;
                }

                //--Range check.
                if(mPlayerCommandCursor >= rPlayerStackList->GetListSize()) mPlayerCommandCursor = rPlayerStackList->GetListSize() - 1;
                if(mPlayerCommandCursor < 0) mPlayerCommandCursor = 0;
                tRecompute = true;
            }

            //--Flags.
            tUseCursor = mPlayerCommandCursor;

            //--Store the command cursor.
            if(rActingEntity)
            {
                //--Skills.
                if(mPlayerCommandBaseCursor == 2)
                {
                    rActingEntity->SetMemoryJob(2, mPlayerCommandCursor);
                }
                //--Specials.
                else if(mPlayerCommandBaseCursor == 3)
                {
                    rActingEntity->SetMemoryMemorized(3, mPlayerCommandCursor);
                }
                //--Items.
                else if(mPlayerCommandBaseCursor == 4)
                {
                    rActingEntity->SetMemoryTactics(3, mPlayerCommandCursor);
                }
            }
        }

        ///--[Cursor Handling]
        if(tRecompute)
        {
            //--Locate whatever command is under the cursor and store it for display.
            ReresolveDescriptionAbility();

            //--Check the skip value and set it as needed.
            int tModDebuff = 0;
            if(mPlayerCommandCursor % 2 == 1) tModDebuff = -1;
            int tEffectiveCursorPos = mPlayerCommandCursor + tModDebuff - mSkillItemSkip;
            //int tMaxScroll = rPlayerStackList->GetListSize() - cxCommand_SizePerPage;
            while(tEffectiveCursorPos < 4 && mSkillItemSkip >= 2)
            {
                mSkillItemSkip -= 2;
                tEffectiveCursorPos = mPlayerCommandCursor + tModDebuff - mSkillItemSkip;
            }
            while(tEffectiveCursorPos > cxCommand_SizePerPage - 2)
            {
                mSkillItemSkip += 2;
                tEffectiveCursorPos = mPlayerCommandCursor + tModDebuff - mSkillItemSkip;
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Attempts to activate the ability in the slot. May fail.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Resolve the ability.
            StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();
            CommandListEntry *rSelectedEntry = (CommandListEntry *)rPlayerStackList->GetElementBySlot(tUseCursor);
            if(!rSelectedEntry)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Ability is manually locked. Do nothing.
            if(rSelectedEntry->mIsManuallyLocked)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--If the entry has an associated ability, begin target mode:
            else if(rSelectedEntry->rAssociatedAbility)
            {
                mTransparencyCursor = 0;
                BeginTargetingAbility(rSelectedEntry->rAssociatedAbility);
                mShowAbilityDescription = false;
            }
            //--If the entry doesn't have an ability, but does have a sublist, activate that:
            else if(rSelectedEntry->mSublist)
            {
                //--Increment the stack.
                mPlayerCommandCursor = 0;
                mPlayerCommandOffset = 0;
                mrPlayerCommandStack->AddElementAsHead("X", rSelectedEntry);

                //--Memory cursor. Which cursor gets used depends on which submenu the player selected.
                if(rActingEntity && mMemoryCursor)
                {
                    //--Skills.
                    if(mPlayerCommandBaseCursor == 2)
                    {
                        mPlayerCommandCursor = rActingEntity->GetMemoryJobY();
                    }
                    //--Specials.
                    else if(mPlayerCommandBaseCursor == 3)
                    {
                        mPlayerCommandCursor = rActingEntity->GetMemoryMemorizedY();
                        rActingEntity->SetMemoryMemorized(3, mPlayerCommandCursor);
                    }
                    //--Items.
                    else if(mPlayerCommandBaseCursor == 4)
                    {
                        mPlayerCommandCursor = rActingEntity->GetMemoryTacticsY();
                    }
                }

                //--Re-resolve the highlighted ability.
                ReresolveDescriptionAbility();

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }

            //--Stop execution.
            return;
        }

        //--Attempts to move to the previous menu, if applicable.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            //--Descriptions are up, remove them.
            if(mShowAbilityDescription)
            {
                mShowAbilityDescription = false;
                AudioManager::Fetch()->PlaySound("Menu|Select");
                return;
            }

            //--No sublist.
            if(mrPlayerCommandStack->GetListSize() < 1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Store current entry.
            StarLinkedList *rOldStackHead = ResolvePlayerCommandList();

            //--Pop stack.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            mrPlayerCommandStack->RemoveElementI(0);

            //--Locate the element that contains the previous head. Set the cursor to that.
            mPlayerCommandCursor = 0;
            mPlayerCommandOffset = -2;
            StarLinkedList *rNewStackHead = ResolvePlayerCommandList();
            CommandListEntry *rCheckEntry = (CommandListEntry *)rNewStackHead->PushIterator();
            while(rCheckEntry)
            {
                //--Match.
                if(rCheckEntry->mSublist == rOldStackHead)
                {
                    rNewStackHead->PopIterator();
                    break;
                }

                //--Next.
                mPlayerCommandCursor ++;
                mPlayerCommandOffset ++;
                rCheckEntry = (CommandListEntry *)rNewStackHead->AutoIterate();
            }

            //--Clamp.
            if(mPlayerCommandOffset < 0) mPlayerCommandOffset = 0;
            if(rNewStackHead->GetListSize() <= 6) mPlayerCommandOffset = 0;

            //--Re-resolve the description ability.
            ReresolveDescriptionAbility();
        }
    }
    ///--[Target Selection]
    //--Selecting which target to execute the ability on.
    else
    {
        //--Entities were moving, cannot allow target selection yet.
        if(pAnyEntitiesMoving) return;

        //--Transparency cursor. Only changes if there's 4 or more boxes.
        int tPredictionBoxes = mPredictionBoxes->GetListSize();
        if(tPredictionBoxes >= 4)
        {
            //--Decrement the transparency cursor.
            if(rControlManager->IsFirstPress("UpLevel"))
            {
                mTransparencyCursor --;
                if(mTransparencyCursor < 0) mTransparencyCursor = tPredictionBoxes - 1;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Increment the transparency cursor.
            if(rControlManager->IsFirstPress("DnLevel"))
            {
                mTransparencyCursor ++;
                if(mTransparencyCursor >= tPredictionBoxes) mTransparencyCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Activate, executes action on target cluster.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Hide command window.
            mShowCommandWindow = false;

            //--Hide description.
            mShowAbilityDescription = false;

            //--Fire the ability.
            mIsSelectingTargets = false;
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
            MovePartyByActiveCluster();
            ExecuteActiveAbility();
            ClearPredictionBoxes();
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Cancel, exits target mode.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            //--Descriptions are up, remove them.
            if(mShowAbilityDescription)
            {
                mShowAbilityDescription = false;
                AudioManager::Fetch()->PlaySound("Menu|Select");
                return;
            }

            mIsSelectingTargets = false;
            rActiveTargetCluster = NULL;
            MovePartyByActiveCluster();
            ClearPredictionBoxes();
            PositionEnemiesByTarget(NULL);
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Storage.
        int tOldTargetCluster = mTargetClusterCursor;

        //--Decrements target cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            mTransparencyCursor = 0;
            mTargetClusterCursor --;
            if(mTargetClusterCursor < 0) mTargetClusterCursor = mTargetClusters->GetListSize() - 1;
        }
        //--Increments target cursor.
        if(rControlManager->IsFirstPress("Right"))
        {
            mTransparencyCursor = 0;
            mTargetClusterCursor ++;
            if(mTargetClusterCursor >= mTargetClusters->GetListSize()) mTargetClusterCursor = 0;
        }

        //--If this flag was set, we need to reposition the highlight.
        if(mTargetClusterCursor != tOldTargetCluster)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Move party around.
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
            MovePartyByActiveCluster();
            SetClusterAsActive(mTargetClusterCursor);

            //--Move enemies around.
            PositionEnemiesByTarget(rActiveTargetCluster->mrTargetList);
        }
    }
}
void AbyssCombat::UpdateInspectorEntitySelect()
{
    ///--[Documentation]
    //--Handles input when the player is selecting an entity in the combat inspector. Same as the base
    //  version but disables ability inspection.
    bool tRecomputeCursor = false;
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Cursor]
    //--Up. Decrements.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEntityCursor, mInspectorEntitySkip, tIterations, mrInspectorEntityList->GetListSize(), 3, ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE, true);
    }
    //--Down. Increments.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEntityCursor, mInspectorEntitySkip, tIterations, mrInspectorEntityList->GetListSize(), 3, ADVCOMBAT_INSPECTOR_ENTITIES_PER_PAGE, true);
    }

    //--Cursor needs to recompute.
    if(tRecomputeCursor)
    {
        //--Call.
        RecomputeInspectorHighlight();

        //--When the entity changes, the effect listing must be rebuilt.
        delete mrInspectorEffectListing;
        mrInspectorEffectListing = NULL;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Get entity, rebuild listing.
        AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
        if(!rInspectEntity) return;
        mrInspectorEffectListing = ConstructEffectListReferencing(rInspectEntity);
    }

    ///--[Toggle Resistances/Statistics]
    //--Bound to the C button.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--Flag.
        mInspectorShowStatistics = !mInspectorShowStatistics;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Activate]
    //--Switches to effect inspection.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get entity.
        AdvCombatEntity *rInspectEntity = (AdvCombatEntity *)mrInspectorEntityList->GetElementBySlot(mInspectorEntityCursor);
        if(!rInspectEntity) return;

        //--The given entity must have at least one effect.
        StarLinkedList *trEffectCheckList = ConstructEffectListReferencing(rInspectEntity);
        if(trEffectCheckList->GetListSize() < 1)
        {
            delete trEffectCheckList;
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--At least one effect. Set to effects mode.
        mCombatInspectorMode = ADVCOMBAT_INSPECTOR_EFFECTS;
        RecomputeInspectorHighlight();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Cancel]
    //--Close the inspector.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Subroutine handles all the flags.
        DeactivateInspector();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
void AbyssCombat::UpdateInspectorEffectSelect()
{
    ///--[Documentation]
    //--Handles input when the player is selecting an effect in the combat inspector. Same as
    //  the base version but disables skill inspection.
    bool tRecomputeCursor = false;
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Cursor]
    //--Up. Decrements.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEffectCursor, mInspectorEffectSkip, tIterations, mrInspectorEffectListing->GetListSize(), 3, ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE, true);
    }
    //--Down. Increments.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase. Scroll extra entries when Ctrl is held down.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Subroutine.
        tRecomputeCursor = StarUIPiece::StandardCursor(mInspectorEffectCursor, mInspectorEffectSkip, tIterations, mrInspectorEffectListing->GetListSize(), 3, ADVCOMBAT_INSPECTOR_EFFECTS_PER_PAGE, true);
    }

    //--Cursor needs to recompute.
    if(tRecomputeCursor)
    {
        //--Call.
        RecomputeInspectorHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Toggle Resistances/Statistics]
    //--Bound to the C button.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--Flag.
        mInspectorShowStatistics = !mInspectorShowStatistics;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Cancel]
    //--Returns to entity query.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Call, recompute cursor.
        mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
        RecomputeInspectorHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
