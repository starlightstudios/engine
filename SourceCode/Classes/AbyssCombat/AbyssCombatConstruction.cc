//--Base
#include "AbyssCombat.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "LuaManager.h"

///--[Debug]
//#define ABYSSCOMBAT_CONSTRUCTION_DEBUG
#ifdef ABYSSCOMBAT_CONSTRUCTION_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///--[Local Definitions]
#define ADVCOMBAT_INTRO_TICKS 15

///========================================= Initialize ===========================================
void AbyssCombat::Initialize()
{
    ///--[ ============= Root Call ============ ]
    AdvCombat::Initialize();

    //--If already initialized, stop.
    if(mHasInitializedAbyCombat) return;
    mHasInitializedAbyCombat = true;

    ///--[ ====== Variable Initialization ===== ]
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_ABYSSCOMBAT;

    ///--[AdvCombat]
    //--[Changeable Constants]
    //--Constants that can be updated by derived classes.
    cIntroMoveTicks = 0;
    cStdMoveTicks = 0;
    cMoveTargetTicks = 0;

    ///--[AbyssCombat]
    //--System
    mStartBattleFadeTimer = 0;
    mIsEmergencyEscape = 0;
    mDisallowEmergencyEscape = false;

    //--Settings
    mCombatTextScale = 4.0f;

    //--UI
    mTransparencyCursor = 0;
    mShowAbilityDescription = false;
    mAbilityDescriptionTimer = 0;
    rDescriptionAbility = NULL;
    mColorHeader.   SetRGBAI(236, 211, 160, 255);
    mColorSubheader.SetRGBAI(181, 112,  41, 255);
    mColorParagraph.SetRGBAI(214, 188, 219, 255);
    mCyclePredictionString = new StarlightString();
    mEmergencyEscapeString = new StarlightString();

    //--Command Input
    mShowCommandWindow = false;
    mCommandScrollTimer = 0;
    mCommandTimer = 0;
    mCommandSecondaryTimer = 0;
    mCommandGlowTimer = 0;
    mPlayerCommandCursor = 0;
    mPlayerCommandBaseCursor = 0;
    mPlayerCommandOffset = 0;
    mSkillItemSkip = 0;
    rActingCharacterImg = NULL;
    mrPlayerCommandStack = new StarLinkedList(false);

    //--Command Storage
    mDummyEntry = (CommandListEntry *)starmemoryalloc(sizeof(CommandListEntry)); //Do not use, only holds a spot on the stack.
    mCommandList = new StarLinkedList(true);
    mrCommandStack = new StarLinkedList(false);

    //--Callouts
    mCalloutList = new StarLinkedList(true);

    //--Defeat Checking
    memset(mIsMemberDisabled, 0, sizeof(bool) * ABYCOM_PLAYER_PARTY_MAX_SIZE);
    memset(mDisableTimers,    0, sizeof(int)  * ABYCOM_PLAYER_PARTY_MAX_SIZE);
    mDefeatCharacterPhase = ABYCOM_DEFEAT_PHASE_FADE_OUT_UI;
    mDefeatTimer = 0;

    //--Universal Abilities
    mGlobalAbilities = new StarLinkedList(true);

    //--Tutorial Overlay
    mIsShowingTutorialOverlay = false;
    mTutorialTimer = 0;
    rTutorialOverlayCur = NULL;
    rTutorialOverlayPrv = NULL;
    mTutorialStringsCur = NULL;
    mTutorialStringsPrv = NULL;

    //--Finale
    mFinalePortraitX = 0.0f;
    mFinalePortraitY = 0.0f;
    rFinalePortraitImg = NULL;
    mFinalePortraitName = NULL;
    memset(mFinaleLines, 0, sizeof(char) * ABYCOMBAT_FINALE_LINES_TOTAL * ABYCOMBAT_FINALE_LINE_LEN);

    //--Strings
    memset(&AbyssString, 0, sizeof(AbyssString));

    //--Public Variables
    mReplyToDefeated = false;
}

///======================================= Reinitialize ===========================================
void AbyssCombat::Reinitialize(bool pHandleMusic)
{
    ///--[ =========== Documentation and Setup ========== ]
    //--Re-initializes the class back to neutral variables, but leaves constants and referenced images in place. Used when the
    //  combat reboots between battles.
    AdvCombat::Reinitialize(pHandleMusic);

    ///--[ =========== Variable Initialization ========== ]
    ///--[ ====== AbyssCombat ======= ]
    ///--[System]
    mStartBattleFadeTimer = 0;

    ///--[UI]
    ///--[Command Input]
    mShowCommandWindow = false;
    mCommandTimer = 0;
    mCommandGlowTimer = 0;

    ///--[Command Storage]
    ///--[Defeat Checking]
    memset(mIsMemberDisabled, 0, sizeof(bool) * ABYCOM_PLAYER_PARTY_MAX_SIZE);
    memset(mDisableTimers,    0, sizeof(int)  * ABYCOM_PLAYER_PARTY_MAX_SIZE);
    mDefeatCharacterPhase = ABYCOM_DEFEAT_PHASE_FADE_OUT_UI;
    mDefeatTimer = 0;

    ///--[Callouts]
    mCalloutList->ClearList();

    ///--[Universal Abilities]
    ///--[Tutorial Overlay]
    mIsShowingTutorialOverlay = false;
    mTutorialTimer = 0;
    rTutorialOverlayCur = NULL;
    rTutorialOverlayPrv = NULL;
    mTutorialStringsCur = NULL;
    mTutorialStringsPrv = NULL;

    ///--[Finale]
    mFinalePortraitX = 0.0f;
    mFinalePortraitY = 0.0f;
    rFinalePortraitImg = NULL;
    free(mFinalePortraitName);
    mFinalePortraitName = NULL;
    memset(mFinaleLines, 0, sizeof(char) * ABYCOMBAT_FINALE_LINES_TOTAL * ABYCOMBAT_FINALE_LINE_LEN);

    ///--[ ================ Data Library ================ ]
    ///--[ ================= Statistics ================= ]
    ///--[ =================== Effects ================== ]
    ///--[ ============= Memory Cursor Reset ============ ]
    ///--[ ==================== Music =================== ]
    ///--[ ================== Scripts =================== ]
    if(xVictoryDialoguePath) LuaManager::Fetch()->ExecuteLuaFile(xVictoryDialoguePath, 1, "N", ABYCOMBAT_FINALE_SCRIPT_INITIALIZE);

    //--Re-resolve control string.
    ControlManager *rControlManager = ControlManager::Fetch();
    mCyclePredictionString->SetString(AbyssString.str.mCyclePredictions);
    mCyclePredictionString->AllocateImages(2);
    mCyclePredictionString->SetImageP(0, 3.0f, rControlManager->ResolveControlImage("DnLevel"));
    mCyclePredictionString->SetImageP(1, 3.0f, rControlManager->ResolveControlImage("UpLevel"));
    mCyclePredictionString->CrossreferenceImages();

    //--Emergency control string.
    mEmergencyEscapeString->SetString(AbyssString.str.mEmergencyCombatExit);
    mEmergencyEscapeString->AllocateImages(1);
    mEmergencyEscapeString->SetImageP(0, 3.0f, rControlManager->ResolveControlImage("WHI Emergency Escape"));
    mEmergencyEscapeString->CrossreferenceImages();

    ///--[ =============== Party Size Cap =============== ]
    //--WHI has a hard party size limit of 3. Additional characters will swap into battle if a character
    //  gets disabled. Therefore, only copy the first 3 Active party onto the Combat party list.
    //--The characters have already been initialized by the base call.
    int tCopyCount = 0;
    mrCombatParty->ClearList();
    AdvCombatEntity *rCharacterPtr = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rCharacterPtr)
    {
        //--Copy.
        mrCombatParty->AddElementAsTail(mrActiveParty->GetIteratorName(), rCharacterPtr);

        //--Range check.
        tCopyCount ++;
        if(tCopyCount >= 3){ mrActiveParty->PopIterator(); break; }

        //--Next.
        rCharacterPtr = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}

///======================================= Construction ===========================================
void AbyssCombat::Construct()
{
    ///--[ ============= Base Call ============ ]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    AdvCombat::Construct();

    //--Overrides.
    const char *cInspectorResistNames[] = {"Protection", "Physical", "Bane", "Arcane", "Flame", "Freezing", "Shocking", "Crusading", "Obscuring", "Bleeding", "Poisoning", "Corroding", "Terrifying"};
    Crossload(Images.Data.rInspectorResistIcons, sizeof(StarBitmap *) * ADVCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL, sizeof(StarBitmap *), "Root/Images/AdventureUI/DamageTypeIcons/%s", cInspectorResistNames, false);

    ///--[ =========== Construction =========== ]
    ///--[Override Fonts]
    Images.Data.rAbilityTitleFont  = rDataLibrary->GetFont("Abyss Combat Ability Title");

    ///--[Crossload Fonts]
    AbyssImg.Data.rFontHeader        = rDataLibrary->GetFont("Abyss Combat Header");
    AbyssImg.Data.rFontCommandWindow = rDataLibrary->GetFont("Abyss Combat Command");
    AbyssImg.Data.rFontSubcommand    = rDataLibrary->GetFont("Abyss Combat Subcommand");
    AbyssImg.Data.rFontCallout       = rDataLibrary->GetFont("Abyss Combat Callout");
    AbyssImg.Data.rFontConfirmBig    = rDataLibrary->GetFont("Abyss Combat Confirm Big");
    AbyssImg.Data.rFontConfirmSmall  = rDataLibrary->GetFont("Abyss Combat Confirm Small");
    AbyssImg.Data.rFontPlayerUI      = rDataLibrary->GetFont("Abyss Combat Player UI");
    AbyssImg.Data.rFontControl       = rDataLibrary->GetFont("Monoceros Standard Control");
    AbyssImg.Data.rFontDescription   = rDataLibrary->GetFont("Abyss Combat Description");
    AbyssImg.Data.rFontPrediction    = rDataLibrary->GetFont("Abyss Combat Prediction");
    AbyssImg.Data.rFontVictory       = rDataLibrary->GetFont("Abyss Combat Victory");
    AbyssImg.Data.rFontLoot          = rDataLibrary->GetFont("Abyss Combat Loot");
    AbyssImg.Data.rFontNameplate     = rDataLibrary->GetFont("Abyss Combat Nameplate");
    AbyssImg.Data.rFontDialogue      = rDataLibrary->GetFont("Abyss Combat Dialogue");
    AbyssImg.Data.rFontPlatina       = rDataLibrary->GetFont("Abyss Combat Dialogue");
    AbyssImg.Data.rFontEnemyUI       = rDataLibrary->GetFont("Abyss Combat Enemy UI");
    AbyssImg.Data.rFontCombatText    = rDataLibrary->GetFont("Abyss Combat Text");

    ///--[Crossload Images]
    AbyssImg.Data.rFillEnemyHP             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Fill_EnemyHP");
    AbyssImg.Data.rFillEnemyStar           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Fill_EnemyStar");
    AbyssImg.Data.rFillEnemyStun           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Fill_EnemyStun");
    AbyssImg.Data.rFillPlayerHP            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Fill_PlayerHP");
    AbyssImg.Data.rFillPlayerMP            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Fill_PlayerMP");
    AbyssImg.Data.rFrameDescription        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_Description");
    AbyssImg.Data.rFrameEnemyHP            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_EnemyHP");
    AbyssImg.Data.rFrameEnemyStun          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_EnemyStun");
    AbyssImg.Data.rFrameOption             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_Option");
    AbyssImg.Data.rFramePlayerHP           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_PlayerHP");
    AbyssImg.Data.rFramePlayerMP           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_PlayerMP");
    AbyssImg.Data.rFramePlatina            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_Platina");
    AbyssImg.Data.rFramePrediction         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_Prediction");
    AbyssImg.Data.rFrameSkillItems         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_SkillItems");
    AbyssImg.Data.rFrameTurnOrder          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_TurnOrder");
    AbyssImg.Data.rFrameTurnOrderCur       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_TurnOrderCur");
    AbyssImg.Data.rFrameVictoryDialogue    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_VictoryDialogue");
    AbyssImg.Data.rFrameVictoryLoot        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Frame_VictoryLoot");
    AbyssImg.Data.rOverlayCPPip            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_CPPip");
    AbyssImg.Data.rOverlayEnemyHPIcon      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_EnemyHPIcon");
    AbyssImg.Data.rOverlayExclamation      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_Exclamation");
    AbyssImg.Data.rOverlayHPIcon           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_HPIcon");
    AbyssImg.Data.rOverlayOptionSelect     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_OptionSelect");
    AbyssImg.Data.rOverlaySkillItemSelect  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_SkillItemSelect");
    AbyssImg.Data.rOverlaySkillItemShade   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_SkillItemShade");
    AbyssImg.Data.rOverlayVictorySelection = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_VictorySelection");
    AbyssImg.Data.rOverlayVictoryShade     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Overlay_VictoryShade");
    AbyssImg.Data.rScrollbarScroller       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Scrollbar_Scroller");
    AbyssImg.Data.rScrollbarStatic         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/Scrollbar_Static");
    AbyssImg.Data.rSpeechBubble            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/SpeechBubble");
    AbyssImg.Data.rSpeechBubbleBot         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AbyssUI/Pieces/SpeechBubbleBot");

    ///--[Verify]
    AbyssImg.mIsReady = VerifyStructure(&AbyssImg.Data, sizeof(AbyssImg.Data), sizeof(void *), false);

    //--Error report:
    if(!AbyssImg.mIsReady)
    {
        fprintf(stderr, "Error in AbyCombat construction. Report:\n");
        VerifyStructure(&AbyssImg.Data, sizeof(AbyssImg.Data), sizeof(void *), true);
    }

    ///--[ ============== Strings ============= ]
    //--Initialize strings.
    AbyssString.str.mEmergencyEscapeA      = InitializeString("Immediately lose the battle and return to your last save point?");
    AbyssString.str.mEmergencyEscapeB      = InitializeString("This bypasses bad ends. Press Escape to exit the battle, Activate to cancel.");
    AbyssString.str.mCyclePredictions      = InitializeString("[IMG0]/[IMG1] Cycle Predictions");
    AbyssString.str.mEmergencyCombatExit   = InitializeString("[IMG0] Emergency Combat Exit");
    AbyssString.str.mTextRewardPlusPlatina = InitializeString("+ [iGainPlatina] Platina");
    AbyssString.str.mTextPlatina           = InitializeString("Platina: ");

    //--Run them through translations.
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_COMBAT);
    if(rTranslation)
    {
        for(int i = 0; i < ABYCOMBAT_STRING_TOTAL; i ++)
        {
            rTranslation->Translate(AbyssString.mem.mPtr[i]);
        }
    }
}

///======================================== Disassemble ===========================================
void AbyssCombat::Disassemble()
{
    free(mDummyEntry);
    delete mrPlayerCommandStack;
    delete mCommandList;
    delete mrCommandStack;
    delete mCalloutList;
    delete mGlobalAbilities;
    free(mFinalePortraitName);
    delete mCyclePredictionString;
    delete mEmergencyEscapeString;
    for(int i = 0; i < ABYCOMBAT_STRING_TOTAL; i ++)
    {
        free(AbyssString.mem.mPtr[i]);
    }
    delete mTutorialStringsCur;
    delete mTutorialStringsPrv;
}
