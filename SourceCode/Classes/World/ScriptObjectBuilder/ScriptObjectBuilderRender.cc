//--Base
#include "ScriptObjectBuilder.h"

//--Classes
#include "StarObjectPrototype.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

///--[Debug]
//#define SOB_RENDER_DEBUG
#ifdef SOB_RENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Drawing =============================================
void ScriptObjectBuilder::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void ScriptObjectBuilder::Render()
{
    ///--[Documentation and Setup]
    //--Diagnostics.
    DebugPush(true, "ScriptObjectBuilder::Render() - Begin.\n");

    //--Flip this flag.
    MapManager::xHasRenderedMenus = true;

    //--Error check.
    if(!Images.mIsReady)
    {
        DebugPop("ScriptObjectBuilder::Render() - Images failed to resolve. Stopping.\n");
        return;
    }

    ///--[Mode Switch]
    //--Prototype select.
    if(mCurrentMode == SOB_MODE_PROTOTYPE)
    {
        RenderPrototypes();
    }
    //--Note: Edit mode is also handled here, it has additional rendering over the field mode render.
    else if(mCurrentMode == SOB_MODE_FIELD || mCurrentMode == SOB_MODE_EDIT)
    {
        RenderFields();
    }

    ///--[Finish Up]
    //--Diagnostics.
    DebugPop("ScriptObjectBuilder::Render() - Completed normally.\n");
}

///====================================== Prototype Mode ==========================================
void ScriptObjectBuilder::RenderPrototypes()
{
    ///--[Documentation and Setup]
    //--Renders prototypes mode, showing a list of registered prototypes and their descriptions when
    //  highlighted, as well as some UI features.
    DebugPush(true, "ScriptObjectBuilder::RenderPrototype() - Begin.\n");

    //--Constants.
    float cHIndent     = Images.Data.rFontMainline->GetTextWidth(">");
    float cSizePerLine = Images.Data.rFontMainline->GetTextHeight();

    ///--[No Prototypes]
    if(!mPrototypeList || mPrototypeList->GetListSize() < 1)
    {
        Images.Data.rFontMainline->DrawText(0.0f, 0.0f, 0, 1.0f, "No prototypes found. Press Escape to exit.");
        DebugPop("ScriptObjectBuilder::RenderPrototype() - Completed normally, no prototypes.\n");
        return;
    }

    ///--[Listing]
    //--Variables.
    int tCurrentEntry = 0;
    int tEntriesDisplayed = 0;
    float tHighestY = cSizePerLine * cListIndentLines;
    float tLowestY = 0;
    StarObjectPrototype *rSelected = NULL;

    //--Header.
    Images.Data.rFontMainline->DrawText(0.0f, cSizePerLine * 0.0f, 0, 1.0f, "Use arrow keys to highlight. Press Enter to select.");

    //--Display listing.
    StarObjectPrototype *rPrototype = (StarObjectPrototype *)mPrototypeList->PushIterator();
    while(rPrototype)
    {
        //--Current entry is below the skip. Don't render.
        if(tCurrentEntry < mPrototypeSkip)
        {
            tCurrentEntry ++;
            rPrototype = (StarObjectPrototype *)mPrototypeList->AutoIterate();
            continue;
        }

        //--Rendered entries has exceeded page size. Stop.
        if(tEntriesDisplayed >= mItemsPerPage)
        {
            mPrototypeList->AutoIterate();
            break;
        }

        //--Cursor.
        if(mPrototypeCursor == tCurrentEntry)
        {
            rSelected = rPrototype;
            Images.Data.rFontMainline->DrawTextArgs(0, cSizePerLine * (tEntriesDisplayed + cListIndentLines), 0, 1.0f, ">");
        }

        //--Render.
        Images.Data.rFontMainline->DrawTextArgs(cHIndent, cSizePerLine * (tEntriesDisplayed + cListIndentLines), 0, 1.0f, "%s", rPrototype->GetName());

        //--Store the Y position.
        tLowestY = cSizePerLine * (tEntriesDisplayed + cListIndentLines);

        //--Next entry.
        tEntriesDisplayed ++;
        tCurrentEntry ++;
        rPrototype = (StarObjectPrototype *)mPrototypeList->AutoIterate();
    }

    ///--[Scrollbar]
    //--If the maximum number of entries is higher than what fits on one page, render a counter in the bottom right.
    if(mPrototypeList->GetListSize() >= mItemsPerPage)
    {
        //--Positions.
        float cLft = VIRTUAL_CANVAS_X - 100.0f;
        float cRgt = cLft + 10.0f;
        float cHighPos = tHighestY + cSizePerLine;

        //--Top indicator.
        Images.Data.rFontMainline->DrawTextArgs(VIRTUAL_CANVAS_X - 100, tHighestY, 0, 1.0f, "^");

        //--Bottom indicator.
        Images.Data.rFontMainline->DrawTextArgs(VIRTUAL_CANVAS_X - 100, tLowestY, 0, 1.0f, "v");

        //--Percentages.
        float cPercentTop = (float)mPrototypeSkip                 / (float)mPrototypeList->GetListSize();
        float cPercentBot = (float)(mPrototypeSkip+mItemsPerPage) / (float)mPrototypeList->GetListSize();

        //--Compute the positions of the top and bottom of the scrollbar.
        float cPositionTop = cHighPos + (cPercentTop * (tLowestY - cHighPos));
        float cPositionBot = cHighPos + (cPercentBot * (tLowestY - cHighPos));

        //--Render.
        glDisable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(cLft, cPositionTop);
            glVertex2f(cRgt, cPositionTop);
            glVertex2f(cRgt, cPositionBot);
            glVertex2f(cLft, cPositionBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }

    ///--[Explanation Divider]
    glDisable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f, cDivisorTop);
        glVertex2f(VIRTUAL_CANVAS_X, cDivisorTop);
        glVertex2f(VIRTUAL_CANVAS_X, cDivisorBot);
        glVertex2f(            0.0f, cDivisorBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);

    ///--[Explanation]
    //--Header.
    Images.Data.rFontMainline->DrawTextArgs(0.0f, cExplanationTop, 0, 1.0f, "Description");

    //--Explanation is stored inside the class.
    if(rSelected)
    {
        //--Get total description lines.
        int tTotalDescriptionLines = rSelected->GetTotalDescriptionLines();
        for(int i = 0; i < tTotalDescriptionLines; i ++)
        {
            //--Null check. NULL is a valid description line.
            const char *rDescriptionLine = rSelected->GetDescriptionLine(i);
            if(!rDescriptionLine) continue;

            //--Render.
            Images.Data.rFontMainline->DrawTextArgs(0.0f, cExplanationTop + (cSizePerLine) + (cSizePerLine * i), 0, 1.0f, "%s", rDescriptionLine);
        }
    }

    ///--[Finish Up]
    //--Diagnostics.
    DebugPop("ScriptObjectBuilder::RenderPrototype() - Completed normally.\n");
}

///======================================== Fields Mode ===========================================
void ScriptObjectBuilder::RenderField(float pNameX, float pValX, float pY, StarObjectField *pField)
{
    ///--[Documentation]
    //--Renders a field to the screen with the given locations. Some fields have extra complexities like
    //  enumerations so this gets a subroutine.
    if(!pField) return;

    ///--[Name]
    Images.Data.rFontMainline->DrawTextArgs(pNameX, pY, 0, 1.0f, "%s", pField->mName);

    ///--[Basic Data]
    //--Value.
    if(pField->mType == POINTER_TYPE_INT && pField->mEnumerationsTotal < 1)
    {
        Images.Data.rFontMainline->DrawTextArgs(pValX, pY, 0, 1.0f, "%i", pField->mNumeric.i);
        return;
    }
    else if(pField->mType == POINTER_TYPE_FLOAT)
    {
        Images.Data.rFontMainline->DrawTextArgs(pValX, pY, 0, 1.0f, "%f", pField->mNumeric.f);
        return;
    }
    else if(pField->mType == POINTER_TYPE_STRING)
    {
        Images.Data.rFontMainline->DrawTextArgs(pValX, pY, 0, 1.0f, "%s", pField->mAlpha);
        return;
    }

    ///--[Enumeration Handler]
    //--If this is an integer type with enumerations, it may be necessary to render the enumeration string
    //  instead of the number.
    if(pField->mType != POINTER_TYPE_INT || pField->mEnumerationsTotal < 1) return;

    //--Scan the object for a matching enumeration. There is no guarantee they will be in order or space-filling.
    for(int i = 0; i < pField->mEnumerationsTotal; i ++)
    {
        if(pField->mNumeric.i == pField->mEnumerations[i].mEnumVal && pField->mEnumerations[i].mString)
        {
            Images.Data.rFontMainline->DrawTextArgs(pValX, pY, 0, 1.0f, "%s", pField->mEnumerations[i].mString);
            return;
        }
    }

    //--If we got this far, there was an error. However, the enumeration being malformed should appear
    //  during editing to let the user know. No need to bark an error.
}
void ScriptObjectBuilder::RenderFields()
{
    ///--[Documentation and Setup]
    //--Renders fields mode, showing the editable fields for the currently selected prototype, as well
    //  as descriptions, scrollbars, and other useful UI features.
    DebugPush(true, "ScriptObjectBuilder::RenderFields() - Begin.\n");

    //--Error check: Make sure there's an active prototype.
    if(!rActivePrototype)
    {
        DebugPop("ScriptObjectBuilder::RenderFields() - No active prototype, stopping.\n");
        return;
    }

    ///--[Listing]
    //--Constants.
    int cTotalFields   = rActivePrototype->GetTotalFields();
    float cHIndent     = Images.Data.rFontMainline->GetTextWidth(">");
    float cSizePerLine = Images.Data.rFontMainline->GetTextHeight();
    float cValCurX     = VIRTUAL_CANVAS_X * 0.25f;
    float cValEdtX     = VIRTUAL_CANVAS_X * 0.50f;

    //--Variables.
    int tEntriesDisplayed = 0;
    float tHighestY = cSizePerLine * cListIndentLines;
    float tLowestY = 0;
    StarObjectField *rSelected = NULL;

    //--Header.
    Images.Data.rFontMainline->DrawText(0.0f, cSizePerLine * 0.0f, 0, 1.0f, "Use arrow keys to highlight. Press Enter to edit.");

    //--Display listing.
    for(int i = 0; i < cTotalFields; i ++)
    {
        //--Retrieve the field.
        StarObjectField *rDisplayField = rActivePrototype->GetFieldI(i);
        if(!rDisplayField) continue;

        //--Current entry is below the skip. Don't render.
        if(i < mFieldSkip) continue;

        //--Rendered entries has exceeded page size. Stop.
        if(tEntriesDisplayed >= mItemsPerPage) break;

        //--Cursor.
        if(mFieldCursor == i)
        {
            rSelected = rDisplayField;
            Images.Data.rFontMainline->DrawTextArgs(0, cSizePerLine * (tEntriesDisplayed + cListIndentLines), 0, 1.0f, ">");
        }

        //--Render.
        RenderField(cHIndent, cValCurX, cSizePerLine * (tEntriesDisplayed + cListIndentLines), rDisplayField);
        Images.Data.rFontMainline->DrawTextArgs(cHIndent, cSizePerLine * (tEntriesDisplayed + cListIndentLines), 0, 1.0f, "%s", rDisplayField->mName);

        //--Edit buffer, if this is the active entry.
        if(mFieldCursor == i && mCurrentMode == SOB_MODE_EDIT)
        {
            RenderEditBuf(cValEdtX, cSizePerLine * (tEntriesDisplayed + cListIndentLines));
        }

        //--Store the Y position.
        tLowestY = cSizePerLine * (tEntriesDisplayed + cListIndentLines);

        //--Next entry.
        tEntriesDisplayed ++;
    }

    ///--[Scrollbar]
    //--If the maximum number of entries is higher than what fits on one page, render a counter in the bottom right.
    if(cTotalFields >= mItemsPerPage)
    {
        //--Positions.
        float cLft = VIRTUAL_CANVAS_X - 100.0f;
        float cRgt = cLft + 10.0f;
        float cHighPos = tHighestY + cSizePerLine;

        //--Top indicator.
        Images.Data.rFontMainline->DrawTextArgs(VIRTUAL_CANVAS_X - 100, tHighestY, 0, 1.0f, "^");

        //--Bottom indicator.
        Images.Data.rFontMainline->DrawTextArgs(VIRTUAL_CANVAS_X - 100, tLowestY, 0, 1.0f, "v");

        //--Percentages.
        float cPercentTop = (float)mFieldSkip                 / (float)cTotalFields;
        float cPercentBot = (float)(mFieldSkip+mItemsPerPage) / (float)cTotalFields;

        //--Compute the positions of the top and bottom of the scrollbar.
        float cPositionTop = cHighPos + (cPercentTop * (tLowestY - cHighPos));
        float cPositionBot = cHighPos + (cPercentBot * (tLowestY - cHighPos));

        //--Render.
        glDisable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(cLft, cPositionTop);
            glVertex2f(cRgt, cPositionTop);
            glVertex2f(cRgt, cPositionBot);
            glVertex2f(cLft, cPositionBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }

    ///--[Explanation Divider]
    glDisable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f, cDivisorTop);
        glVertex2f(VIRTUAL_CANVAS_X, cDivisorTop);
        glVertex2f(VIRTUAL_CANVAS_X, cDivisorBot);
        glVertex2f(            0.0f, cDivisorBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);

    ///--[Explanation]
    //--Header.
    Images.Data.rFontMainline->DrawTextArgs(0.0f, cExplanationTop, 0, 1.0f, "Description");

    //--Explanation is stored inside the class.
    if(rSelected)
    {
        //--Get total description lines.
        for(int i = 0; i < rSelected->mDescriptionLinesTotal; i ++)
        {
            //--Null check. NULL is a valid description line.
            if(!rSelected->mDescriptionLines[i]) continue;

            //--Render.
            Images.Data.rFontMainline->DrawTextArgs(0.0f, cExplanationTop + (cSizePerLine) + (cSizePerLine * i), 0, 1.0f, "%s", rSelected->mDescriptionLines[i]);
        }
    }

    ///--[Help Text]
    Images.Data.rFontMainline->DrawTextArgs(VIRTUAL_CANVAS_X - 200, (cSizePerLine * 0), 0, 1.0f, "[F2] Export");

    ///--[Finish Up]
    //--Diagnostics.
    DebugPop("ScriptObjectBuilder::RenderFields() - Completed normally.\n");
}

///====================================== Editing Buffer ==========================================
void ScriptObjectBuilder::RenderEditBuf(float pX, float pY)
{
    ///--[Documentation]
    //--Renders the editing buffer, one letter at a time, in fixed-width. A cursor is rendered under
    //  the active letter.
    int i = 0;
    float cWidPerLetter = Images.Data.rFontMainline->GetTextWidth("W");
    float cHei = Images.Data.rFontMainline->GetTextHeight();

    ///--[Render Cycle]
    //--Iterate until we hit a NULL character.
    while(mEditingBuffer[i] != '\0')
    {
        //--Render.
        Images.Data.rFontMainline->DrawTextArgs(pX + (cWidPerLetter * i), pY, 0, 1.0f, "%c", mEditingBuffer[i]);

        //--Next.
        i ++;

        //--Range check. End of buffer, stop rendering.
        if(i >= SOB_EDIT_BUF_SIZE) break;
    }

    //--If no cursor was rendered, render it now after the last letter.
    {
        //--Position.
        float cLft = pX + (cWidPerLetter * mEditCursor) - 1.0f;
        float cRgt = cLft + 2.0f;
        float cTop = pY;
        float cBot = cTop + cHei;

        //--Render.
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glVertex2f(cLft, cTop);
            glVertex2f(cRgt, cTop);
            glVertex2f(cRgt, cBot);
            glVertex2f(cLft, cBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }
}
