//--Base
#include "CarnationLevel.h"

//--Classes
#include "AdventureMenu.h"
#include "AdvCombat.h"
#include "CarnationMinimap.h"
#include "CarnationPopup.h"
#include "CarnUICommon.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "CarnationDef.h"
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///--[Debug]
//#define CARNATIONLEVEL_UPDATE_DEBUG
#ifdef CARNATIONLEVEL_UPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Update ==============================================
void CarnationLevel::Update()
{
    ///--[ ========================= Debug ======================== ]
    DebugPush(true, "Carnation Level Update begin.\n");

    //--Fast-access pointers.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get mouse position.
    rControlManager->GetMouseCoords(mMouseX, mMouseY, mMouseZ);

    ///--[ ======================== Timers ======================== ]
    //--Update minimap.
    mMinimap->Update();

    //--Update event notifications.
    UpdateEvents();

    //--In minimap mode, run this timer.
    if(mIsMinimapMode)
    {
        if(mMinimapTimer < CL_MINIMAP_TICKS) mMinimapTimer ++;
    }
    else
    {
        if(mMinimapTimer > 0) mMinimapTimer --;
    }

    //--Check examinables for mouse over. Does not occur during a transition.
    if(!mIsLevelTransitioning)
    {
        //--Store the previous highlight.
        void *rOldHighlight = rHighlightedExaminable;

        //--Iterate.
        CarnExaminable *rExaminable = (CarnExaminable *)mExaminablesList->PushIterator();
        while(rExaminable)
        {
            //--Get distance.
            float tCenterX = (rExaminable->mHeartX + rExaminable->mHeartR) * 0.50f;
            float tCenterY = (rExaminable->mHeartY + rExaminable->mHeartB) * 0.50f;
            float tDistance = GetPlanarDistance(tCenterX, tCenterY, mMouseX, mMouseY);

            //--Too far away. Ignore this object. If the object was the highlighted examinable, de-highlight it.
            if(tDistance > CARNEXAMINE_DISCOVERY_DISTANCE)
            {
                if(rHighlightedExaminable == rExaminable) rHighlightedExaminable = NULL;
                rExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
                continue;
            }

            //--The distance is within "discovery" range. If this object is the highlighted examinable, do nothing,
            //  because we already have triggered a response.
            if(rHighlightedExaminable == rExaminable)
            {
                rExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
                continue;
            }

            //--If we got this far, this object became highlighted for the first time. Fire a response. Responses
            //  mean firing the associated script with the suffix " Highlight". So an object named "Gate" becomes "Gate Highlight".
            rHighlightedExaminable = rExaminable;
            SetAlarmTarget(rExaminable->mHighlightVal, 15);

            //--If the value is over 45, issue a kick.
            if(rExaminable->mHighlightVal >= 45) KickAlarmAuto();

            /*
            //--Skip fully revealed cases.
            if(rExaminable->mDiscoveryTimer >= CARNEXAMINE_DISCOVERY_TICKS || rExaminable->mAlwaysVisible)
            {
                rExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
                continue;
            }

            //--Get distance.
            float tCenterX = (rExaminable->mHeartX + rExaminable->mHeartR) * 0.50f;
            float tCenterY = (rExaminable->mHeartY + rExaminable->mHeartB) * 0.50f;
            float tDistance = GetPlanarDistance(tCenterX, tCenterY, mMouseX, mMouseY);

            //--Shorter than the double-distance, increases timer faster.
            if(tDistance <= CARNEXAMINE_DISCOVERY_DISTANCE_DOUBLE)
            {
                rExaminable->mDiscoveryTimer += 2;
            }
            //--Shorter than the distance, increases timer.
            else if(tDistance <= CARNEXAMINE_DISCOVERY_DISTANCE)
            {
                rExaminable->mDiscoveryTimer ++;
            }
            //--Decrement timer to zero.
            else
            {
                if(rExaminable->mDiscoveryTimer > 0) rExaminable->mDiscoveryTimer --;
            }

            //--If the threshold was crossed this tick, record that in the datalibrary.
            if(rExaminable->mDiscoveryTimer >= CARNEXAMINE_DISCOVERY_TICKS)
            {
                //--Clamp.
                rExaminable->mDiscoveryTimer = CARNEXAMINE_DISCOVERY_TICKS;

                //--Set variable.
                char tBuffer[128];
                sprintf(tBuffer, "Root/Variables/Carnation|Discoveries/%s/%s", mCurrentAreaName, mExaminablesList->GetIteratorName());

                //--Create a SysVar.
                SysVar *nVariable = (SysVar *)starmemoryalloc(sizeof(SysVar));
                nVariable->mIsSaved = true;
                nVariable->mNumeric = 1.0f;
                nVariable->mAlpha = InitializeString("Null");
                DataLibrary::Fetch()->AddPath(tBuffer);
                DataLibrary::Fetch()->RegisterPointer(tBuffer, nVariable, &DataLibrary::DeleteSysVar);

                //--SFX.
            }

            //--Next.
            rExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();*/
        }

        //--If the old highlight changed, and the new highlight is NULL, zero the alarm value.
        if(rOldHighlight != rHighlightedExaminable && rHighlightedExaminable == NULL)
        {
            SetAlarmTarget(0, 15);
        }
    }

    //--Update all popups here.
    CarnationPopup *rUpdatePopup = (CarnationPopup *)mPopupList->SetToHeadAndReturn();
    while(rUpdatePopup)
    {
        //--Run the popup's internal update.
        rUpdatePopup->Update();

        //--Check if the popup has hidden and should be removed.
        if(!rUpdatePopup->IsShowing() && !rUpdatePopup->IsVisible())
        {
            mPopupList->RemoveRandomPointerEntry();
        }

        //--Next.
        rUpdatePopup = (CarnationPopup *)mPopupList->IncrementAndGetRandomPointerEntry();
    }

    //--Update all background layers here.
    CarnMapLayer *rBGLayer = (CarnMapLayer *)mBackgroundLayers->PushIterator();
    while(rBGLayer)
    {
        rBGLayer->Update();
        rBGLayer = (CarnMapLayer *)mBackgroundLayers->AutoIterate();
    }

    //--Confirm retreat.
    if(mIsConfirmRetreat)
    {
        if(mConfirmRetreatTimer < cxTimer_ConfirmRetreat) mConfirmRetreatTimer ++;
    }
    else
    {
        if(mConfirmRetreatTimer > 0) mConfirmRetreatTimer --;
    }

    ///--[Aura Particles]
    //--Run update on all aura particles, remove ones that have expired.
    AuraParticle *rParticle = (AuraParticle *)mAuraParticleList->SetToHeadAndReturn();
    while(rParticle)
    {
        //--Update.
        rParticle->Update();

        //--Expired. Delete.
        if(rParticle->mLifetime < 1) mAuraParticleList->RemoveRandomPointerEntry();

        //--Next.
        rParticle = (AuraParticle *)mAuraParticleList->IncrementAndGetRandomPointerEntry();
    }

    //--If particles are spawning, do so here.
    if(rSpawnAuraImage && mSpawnAuraChance > 0)
    {
        //--Roll.
        int tRoll = rand() % 100;

        //--Spawn a random particle.
        if(tRoll < mSpawnAuraChance)
        {
            //--Setup.
            AuraParticle *nParticle = (AuraParticle *)starmemoryalloc(sizeof(AuraParticle));
            nParticle->Initialize();
            nParticle->rImage = rSpawnAuraImage;

            //--Random position.
            nParticle->mXPos = 240.0f + 25.0f + (rand() % (int)(886.0f - 25.0f));
            nParticle->mYPos =  56.0f + 25.0f + (rand() % (int)(498.0f - 25.0f));

            //--Random speeds.
            float cSpeed = 0.2f;
            float tAngle = rand() % 360;
            nParticle->mXSpeed = cosf(tAngle * TORADIAN) * cSpeed;
            nParticle->mYSpeed = sinf(tAngle * TORADIAN) * cSpeed;

            //--Scatter size.
            nParticle->mSizeTimer = rand() % AURAPARTICLE_SIZE_PERIOD;

            //--Register.
            mAuraParticleList->AddElement("X", nParticle, &FreeThis);
        }
    }

    ///--[ ==================== Alarm Handling ==================== ]
    ///--[Timer Handling]
    //--Alarm timer. The timer runs across to the target value, and may trigger animations as it
    //  runs to that value. This means that rapidly increasing the alarm value is smooth.
    //--The alarm timer is ignored during room transition, though the physics do not stop.
    mAlarmVal.IncrementLinear();
    /*
    if(!mIsLevelTransitioning)
    {
        //--Increment/Decrement.
        bool tAlarmIncreased = false;
        if(mAlarmValue < mAlarmValueTarget)
        {
            tAlarmIncreased = true;
            mAlarmValue ++;
        }
        else if(mAlarmValue > mAlarmValueTarget)
        {
            mAlarmValue --;
        }

        //--If the value changed, check to see if it hit certain thresholds. This only happens if it increased
        //  in order to make the alarm not constantly trigger pushes if it happens to land on a threshold.
        if(tAlarmIncreased)
        {
            //--If we hit 25%, 45%, or 65%, issue a "kick".
            if(mAlarmValue == 25 || mAlarmValue == 45 || mAlarmValue == 65)
            {
                //--Shake value is negative, issue a positive kick.
                if(mAlarmShake < 0.0f)
                {
                    mAlarmShakeDelta = mAlarmShakeDelta + mAlarmShakePush;
                }
                //--Positive, issue a negative kick.
                else if(mAlarmShake > 0.0f)
                {
                    mAlarmShakeDelta = mAlarmShakeDelta - mAlarmShakePush;
                }
                //--At zero, randomize the kick direction, and the kick power slightly.
                else
                {
                    //--Power factor.
                    float cPct = (float)((rand() % 100) / 100.0f);
                    float cUsePower = 0.85f + (0.30f * cPct);

                    //--Kick left.
                    if(rand() % 100 < 50)
                    {
                        mAlarmShakeDelta = mAlarmShakeDelta - (mAlarmShakePush * cUsePower);
                    }
                    //--Kick right.
                    else
                    {
                        mAlarmShakeDelta = mAlarmShakeDelta + (mAlarmShakePush * cUsePower);
                    }
                }
            }
        }
    }*/

    ///--[Handle Shaking]
    //--The alarm can continue to shake regardless of other things going on.
    mAlarmShake = mAlarmShake + mAlarmShakeDelta;

    //--Friction is always applied, causing the object to eventually slow to a stop.
    float cFriction = 0.97f;
    mAlarmShakeDelta = mAlarmShakeDelta * cFriction;

    //--Thresholds to stop shaking. The alarm must be very close to zero for this many ticks, at
    //  which point it will stop shaking entirely.
    float cThresholdRange = 0.25f;
    int cNeededTicks = 30;
    if(mAlarmShake >= -cThresholdRange && mAlarmShake < cThresholdRange)
    {
        mAlarmShakeZeroTicks ++;
        if(mAlarmShakeZeroTicks >= cNeededTicks)
        {
            mAlarmShake = 0.0f;
            mAlarmShakeDelta = 0.0f;
        }
    }
    else
    {
        mAlarmShakeZeroTicks = 0;
    }

    //--Delta decreases by the friction coefficient. This only happens if it's above 0.05.
    //--Force of gravity is a constant times the sin of double the angle. If the angle exceeds
    //  90 degrees the force of gravity is always at 100%.
    float cFullGravity = 3.0f;
    if(mAlarmShake < -90.0f)
    {
        mAlarmShakeDelta = mAlarmShakeDelta + cFullGravity;
    }
    else if(mAlarmShake > 90.0f)
    {
        mAlarmShakeDelta = mAlarmShakeDelta - cFullGravity;
    }
    //--In between extremes.
    else
    {
        float cUseGravity = sinf(mAlarmShake * 2.0f * TORADIAN);
        mAlarmShakeDelta = mAlarmShakeDelta - cUseGravity;
    }

    //--If the shake hits the negative cap:
    if(mAlarmShake < mAlarmShakeCap * -1.0f)
    {
        mAlarmShake = mAlarmShakeCap * -1.0f;
        if(mAlarmShakeDelta < 0.0f) mAlarmShakeDelta = mAlarmShakeDelta * -1.0f;
    }
    //--If it hits the positive cap:
    else if(mAlarmShake > mAlarmShakeCap)
    {
        mAlarmShake = mAlarmShakeCap;
        if(mAlarmShakeDelta > 0.0f) mAlarmShakeDelta = mAlarmShakeDelta * -1.0f;
    }

    ///--[ ================== Dialogue Handling =================== ]
    ///--[World Dialogue]
    //--If the dialogue is open, handles its updates. This takes priority over combat, allowing
    //  dialogue to play over top of combat.
    if(rWorldDialogue->IsVisible())
    {
        ///--[Debug]
        //--Debug.
        DebugPrint("Updating world dialogue.\n");

        ///--[Dialogue is Hiding]
        //--If not managing updates, the dialogue is hiding, and needs only to update.
        if(!rWorldDialogue->IsManagingUpdates())
        {
            //--Update.
            rWorldDialogue->Update();
        }
        ///--[Dialogue is Running]
        //--The dialogue is open and taking player input.
        else
        {
            //--Run the dialogue.
            rWorldDialogue->Update();

            //--Cutscenes can also update here.
            CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
            if(rCutsceneManager->HasAnyEvents())
            {
                rCutsceneManager->Update();
            }

            //--Background menu.
            mMenu->UpdateBackground();

            //--Timer.
            //if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;

            //--Debug.
            DebugPop("Carnation Level Update completes, world dialogue sequence is running.\n");
            return;
        }
    }

    ///--[Internal Dialogue]
    if(mInternalDialogue->IsVisible())
    {
        ///--[Debug]
        //--Debug.
        DebugPrint("Updating internal dialogue.\n");

        ///--[Dialogue is Hiding]
        //--If not managing updates, the dialogue is hiding, and needs only to update.
        if(!mInternalDialogue->IsManagingUpdates())
        {
            //--Update.
            mInternalDialogue->Update();
        }
        ///--[Dialogue is Running]
        //--The dialogue is open and taking player input.
        else
        {
            //--Run the dialogue.
            mInternalDialogue->Update();

            //--Cutscenes can also update here.
            CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
            if(rCutsceneManager->HasAnyEvents())
            {
                rCutsceneManager->Update();
            }

            //--Background menu.
            mMenu->UpdateBackground();

            //--Timer.
            //if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;

            //--Debug.
            DebugPop("Carnation Level Update completes, internal dialogue sequence is running.\n");
            return;
        }
    }

    ///--[ ================== Cutscene Handling =================== ]
    //--Setup.
    int tIterations = 1;
    bool tMustReturnOut = false;

    //--Get how many times to run the manager.
    //if(tIsHoldingActivate && tIsHoldingCancel) tIterations = 9;

    //--If the manager has events, run the update.
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rCutsceneManager->HasAnyEvents())
    {
        //--Run the update.
        DebugPrint("Updating cutscenes.\n");
        rCutsceneManager->Update();
        DebugPrint("Finished cutscenes.\n");

        //--Timer.
        //if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;

        //--If this flag is set, parallel cutscenes will update.
        //DebugPrint("Updating parallel events.\n");
        //if(mRunParallelDuringCutscene) rCutsceneManager->UpdateParallelEvents();
        //DebugPrint("Finished parallel events.\n");
        tMustReturnOut = true;
        tIterations --;
    }
    else
    {
        tIterations = 0;
    }

    //--Successive updates. We don't need to rerun the field ability cancel cases.
    while(tIterations > 0)
    {
        rCutsceneManager->Update();
        tIterations --;
    }

    //--If ordered, end the update here. The cutscene manager will stop updating if at least one cutscene
    //  event took place, even if multiple iterations passed.
    if(tMustReturnOut)
    {
        mMenu->UpdateBackground();
        DebugPop("Carnation Level Update completes, cutscene handled it.\n");
        return;
    }

    ///--[ ======================== Combat ======================== ]
    //--Run the update.
    DebugPrint("Updating Combat UI.\n");
    rAdventureCombat->Update();

    //--The combat UI will stop the update based on its internal logic.
    if(!rAdventureCombat->IsStoppingWorldUpdate())
    {
        if(mCombatTimer > 0) mCombatTimer --;
    }
    //--Otherwise, it does. Stop the update here.
    else
    {
        if(mCombatTimer < CL_COMBAT_TICKS) mCombatTimer ++;
        mMenu->UpdateBackground();
        //if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;
        DebugPop("Carnation Level Update completes, combat handled the update.\n");
        return;
    }

    ///--[ ===================== Menu Handling ==================== ]
    //--If the menu is not visible, run the background update.
    DebugPrint("Updating Menu.\n");
    if(!mMenu->IsVisible())
    {
        mMenu->UpdateBackground();
    }
    //--If the menu is visible, run the foreground update and block further update.
    else
    {
        //--Run.
        mMenu->Update();

        //--Finish.
        DebugPop("Carnation Level Update completes, menu handled the update.\n");
        return;
    }

    ///--[ ================= Transitioning Rooms ================== ]
    //--Locks out controls when transitioning between rooms.
    if(mIsLevelTransitioning)
    {
        //--Fadeout. Runs a timer.
        if(mTransitionStage == CL_TRANSITION_FADEOUT)
        {
            if(mTransitionTimer < CL_TRANSITION_FADEOUT_TICKS)
            {
                mTransitionTimer ++;
            }
            else
            {
                mTransitionTimer = 0;
                mTransitionStage = CL_TRANSITION_LEVELSWITCH;
            }
        }
        //--Switch. A brief black period, clears everything and then runs the new constructor.
        else if(mTransitionStage == CL_TRANSITION_LEVELSWITCH)
        {
            if(mTransitionTimer < CL_TRANSITION_LEVELSWITCH_TICKS)
            {
                //--Run timer.
                mTransitionTimer ++;

                //--On the 2nd tick, clear this level's data. This includes backgrounds, popups, etc.
                if(mTransitionTimer == 2)
                {
                    ClearOldLevelData();
                }
                //--On the 4th tick, run the new constructor.
                else if(mTransitionTimer == 4)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(mTransitionToScript, 1, "N", 0.0f);
                }
            }
            else
            {
                mTransitionTimer = 0;
                mTransitionStage = CL_TRANSITION_FADEIN;
            }
        }
        //--Fadein. Runs a timer. When the timer finishes, attempts to run the local triggers file.
        else if(mTransitionStage == CL_TRANSITION_FADEIN)
        {
            //--Increment.
            if(mTransitionTimer < CL_TRANSITION_FADEIN_TICKS)
            {
                mTransitionTimer ++;
            }
            //--Finish.
            else
            {
                //--Reset variables.
                mIsLevelTransitioning = false;
                mTransitionTimer = 0;
                mTransitionStage = 0;

                //--If the triggers script exists, fire it here.
                if(mTriggersPath) LuaManager::Fetch()->ExecuteLuaFile(mTriggersPath, 1, "S", "Auto");
            }
        }

        DebugPop("Carnation Level Update completes, room transition.\n");
        return;
    }

    ///--[ ===================== Minimap Mode ===================== ]
    //--If the player clicks on the minimap, it brings out a full map. Clicking again hides the map.
    if(mIsMinimapMode)
    {
        //--Left-click can activate buttons or be used for click and drag.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--If on the recenter button:
            if(IsPointWithin(mMouseX, mMouseY, 1136.0f, 704.0f, 1136.0f + 228.0f, 704.0f + 61.0f))
            {
                //--Disable drag mode.
                mIsMinimapDrag = false;

                //--Recompute position and center the map.
                int tPlayerX = mMinimap->GetPlayerX();
                int tPlayerY = mMinimap->GetPlayerY();
                mMinimapOffsetX = (VIRTUAL_CANVAS_X * 0.5f)  + (tPlayerX * CARNMINIMAP_DISCOVERY_BLOCK_W * -1.0f) + (CARNMINIMAP_DISCOVERY_BLOCK_W * -4.0f);
                mMinimapOffsetY = (VIRTUAL_CANVAS_Y * 0.35f) + (tPlayerY * CARNMINIMAP_DISCOVERY_BLOCK_H * -1.0f) + (CARNMINIMAP_DISCOVERY_BLOCK_H * -4.0f);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                return;
            }

            //--Not on the button. Handle click and drag.
            mIsMinimapDrag = true;
            mMinimapDragX = mMouseX;
            mMinimapDragY = mMouseY;
        }
        //--Click is held down.
        else if(rControlManager->IsDown("MouseLft"))
        {
            //--Only activates if drag is active.
            if(mIsMinimapDrag)
            {
                mMinimapOffsetX = mMinimapOffsetX + (mMouseX - mMinimapDragX);
                mMinimapOffsetY = mMinimapOffsetY + (mMouseY - mMinimapDragY);
                mMinimapDragX = mMouseX;
                mMinimapDragY = mMouseY;
            }
        }
        //--Release.
        else
        {
            mIsMinimapDrag = false;
        }

        //--Right-click exits minimap mode.
        if(rControlManager->IsFirstPress("MouseRgt"))
        {
            mIsMinimapMode = false;

        }
        DebugPop("Carnation Level Update completes, minimap mode.\n");
        return;
    }

    ///--[ =================== Confirm Retreat ==================== ]
    //--If the player clicks the button to retreat from the dungeon, this menu pops up to confirm.
    if(mConfirmRetreatTimer)
    {
        //--Left-click.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Confirm:
            if(mButtonConfirmRetreat.IsPointWithin(mMouseX, mMouseY))
            {
                mIsConfirmRetreat = false;
                LuaManager::Fetch()->ExecuteLuaFile(mRetreatScript);
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Cancel.
            if(mButtonCancelRetreat.IsPointWithin(mMouseX, mMouseY))
            {
                mIsConfirmRetreat = false;
                AudioManager::Fetch()->PlaySound("Menu|UIOpen");
            }
        }
        //--Right click always cancels.
        else if(rControlManager->IsFirstPress("MouseRgt"))
        {
            mIsConfirmRetreat = false;
            AudioManager::Fetch()->PlaySound("Menu|UIOpen");
        }

        DebugPop("Carnation Level Update completes, confirming retreat.\n");
        return;
    }

    ///--[ ===================== Player Input ===================== ]
    ///--[ ============ Arrow Keys ============ ]
    //--The player can use the arrow keys to navigate. This looks for examinables in specific positions
    //  and effectively "clicks" them.
    //--The function returns true if it handled the click, which should stop the update.
    if(rControlManager->IsDown("Up")    || rControlManager->IsDown("ScndUp"))    { if(EmulateClickAt( 683.0f, 170.0f)) return; }
    if(rControlManager->IsDown("Right") || rControlManager->IsDown("ScndRight")) { if(EmulateClickAt(1020.0f, 320.0f)) return; }
    if(rControlManager->IsDown("Down")  || rControlManager->IsDown("ScndDown"))  { if(EmulateClickAt( 683.0f, 440.0f)) return; }
    if(rControlManager->IsDown("Left")  || rControlManager->IsDown("ScndLeft"))  { if(EmulateClickAt( 330.0f, 320.0f)) return; }

    ///--[ ========== Click Handling ========== ]
    //--Player left-clicks.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        ///--[Common Setup]
        //--Handling flag.
        bool tHasHandledInput = false;
        CarnExaminable *rHitExaminable = NULL;

        ///--[Notifications]
        //--These get priority, if one is clicked it stops the update.
        if(UpdateEventLeftClick(mMouseX, mMouseY)) return;

        ///--[Fixed Buttons]
        //--Character A portrait, opens UI pointing at this character.
        int tActivePartySize = AdvCombat::Fetch()->GetActivePartyCount();
        if(mCharacterA.IsPointWithin(mMouseX, mMouseY) && tActivePartySize >= 1)
        {
            CarnUICommon::xPersistentCharacterCursor = 0;
            mMenu->Show();
            tHasHandledInput = true;
            AudioManager::Fetch()->PlaySound("Menu|UIOpen");
        }
        if(mCharacterB.IsPointWithin(mMouseX, mMouseY) && tActivePartySize >= 2)
        {
            CarnUICommon::xPersistentCharacterCursor = 1;
            mMenu->Show();
            tHasHandledInput = true;
            AudioManager::Fetch()->PlaySound("Menu|UIOpen");
        }
        if(mCharacterC.IsPointWithin(mMouseX, mMouseY) && tActivePartySize >= 3)
        {
            CarnUICommon::xPersistentCharacterCursor = 2;
            mMenu->Show();
            tHasHandledInput = true;
            AudioManager::Fetch()->PlaySound("Menu|UIOpen");
        }

        //--Clicking these UI buttons opens up the menus.
        if(mButtonJournal.IsPointWithin(mMouseX, mMouseY))
        {
            CarnUICommon::xPersistentCharacterCursor = 0;
            mMenu->Show();
            tHasHandledInput = true;
            AudioManager::Fetch()->PlaySound("Menu|UIOpen");
        }

        //--Retreat button.
        if(mButtonOptions.IsPointWithin(mMouseX, mMouseY) && mCanRetreat && mRetreatScript)
        {
            tHasHandledInput = true;
            mIsConfirmRetreat = true;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Map.
        if(IsPointWithin(mMouseX, mMouseY, 1144.0f, 63.0f, 1144.0f + 202.0f, 63.0f + 402.0f))
        {
            //--Set flags.
            mIsMinimapMode = true;

            //--Resolve offsets to center the minimap.
            int tPlayerX = mMinimap->GetPlayerX();
            int tPlayerY = mMinimap->GetPlayerY();
            mMinimapOffsetX = (VIRTUAL_CANVAS_X * 0.5f)  + (tPlayerX * CARNMINIMAP_DISCOVERY_BLOCK_W * -1.0f) + (CARNMINIMAP_DISCOVERY_BLOCK_W * -4.0f);
            mMinimapOffsetY = (VIRTUAL_CANVAS_Y * 0.35f) + (tPlayerY * CARNMINIMAP_DISCOVERY_BLOCK_H * -1.0f) + (CARNMINIMAP_DISCOVERY_BLOCK_H * -4.0f);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|UIOpen");
        }

        ///--[Popup Handling]
        //--If any popups are able to handle clicks, they get checked. If no popups were clicked,
        //  then all popups are ordered to hide.
        if(!tHasHandledInput && AreAnyPopupsAcceptingClicks())
        {
            //--Scan popups.
            CarnationPopup *rClickPopup = (CarnationPopup *)mPopupList->PushIterator();
            while(rClickPopup)
            {
                //--Check if the popup was clicked.
                rHitExaminable = rClickPopup->HandleClick(mMouseX, mMouseY);
                if(rHitExaminable)
                {
                    tHasHandledInput = true;
                }

                //--Next.
                rClickPopup = (CarnationPopup *)mPopupList->AutoIterate();
            }

            //--If the handle-input flag is still false, close all popups.
            if(!tHasHandledInput)
            {
                CarnationPopup *rClickPopup = (CarnationPopup *)mPopupList->PushIterator();
                while(rClickPopup)
                {
                    rClickPopup->SetVisible(false);
                    rClickPopup = (CarnationPopup *)mPopupList->AutoIterate();
                }
            }
        }
        ///--[World/UI Handling]
        //--If no popups are in the way, handle clicks to buttons/examinables.
        else if(!tHasHandledInput)
        {
            //--Examinable objects.
            CarnExaminable *rClickExaminable = (CarnExaminable *)mExaminablesList->PushIterator();
            while(rClickExaminable)
            {
                //--Fire the examinable's script if the click is within range.
                if(IsPointWithin(mMouseX, mMouseY, rClickExaminable->mHeartX, rClickExaminable->mHeartY, rClickExaminable->mHeartR, rClickExaminable->mHeartB))
                {
                    tHasHandledInput = true;
                    rHitExaminable = rClickExaminable;
                    mExaminablesList->PopIterator();
                    break;
                }

                //--Next.
                rClickExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
            }
        }

        ///--[Firing a Hit]
        //--If an examinable got hit, fire it here. This means the loop can't get screwed up by editing the popups in a lua script.
        if(rHitExaminable)
        {
            //--Setup.
            const char *rPath = rHitExaminable->mScript;

            //--Automatic transition to a room:
            if(!strcasecmp(rPath, "AUTOTRANSITION"))
            {
                BeginTransitionTo(rHitExaminable->mArgument);
            }
            //--Normal case, fire the script.
            else
            {
                LuaManager::Fetch()->ExecuteLuaFile(rHitExaminable->mScript, 1, "S", rHitExaminable->mArgument);
            }
        }
    }

    ///--[Right Click]
    //--Opens the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        CarnUICommon::xPersistentCharacterCursor = 0;
        mMenu->Show();
        AudioManager::Fetch()->PlaySound("Menu|UIOpen");
    }

    ///--[Debug]
    DebugPop("Carnation Level Update completes.\n");
}
bool CarnationLevel::EmulateClickAt(float pX, float pY)
{
    ///--[Documentation]
    //--Attempts to click the given position, but only actually does anything if it's an Auto-Transition case.
    //  This allows the player to do rudimentary navigation with the arrow keys.
    //--Returns true if it handled the click, false if it did not.

    ///--[Scan]
    //--Run a click check.
    CarnExaminable *rHitExaminable = NULL;
    CarnExaminable *rClickExaminable = (CarnExaminable *)mExaminablesList->PushIterator();
    while(rClickExaminable)
    {
        //--Fire the examinable's script if the click is within range.
        if(IsPointWithin(pX, pY, rClickExaminable->mHeartX, rClickExaminable->mHeartY, rClickExaminable->mHeartR, rClickExaminable->mHeartB))
        {
            rHitExaminable = rClickExaminable;
            mExaminablesList->PopIterator();
            break;
        }

        //--Next.
        rClickExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
    }

    ///--[Fire]
    //--If an examinable got hit, fire it here. This means the loop can't get screwed up by editing the popups in a lua script.
    if(rHitExaminable)
    {
        //--Setup.
        const char *rPath = rHitExaminable->mScript;

        //--Automatic transition to a room. All other cases are ignored.
        if(!strcasecmp(rPath, "AUTOTRANSITION"))
        {
            BeginTransitionTo(rHitExaminable->mArgument);
        }
        return true;
    }

    ///--[No Hits]
    return false;
}
