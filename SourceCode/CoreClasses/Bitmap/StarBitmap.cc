//--Base
#include "StarBitmap.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "GlDfn.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "StarLumpManager.h"

///====================================== Public Statics ==========================================
//--Bitmap Flags
bool StarBitmap::xForcePowerOfTwo = false;
bool StarBitmap::gLockTarget = false;
bool StarBitmap::xSuppressInterrupt = false;
InterruptPtr StarBitmap::xInterruptCall = NULL;
bool StarBitmap::xPadSpriteForEdging = false;

//--Statistics
int32_t StarBitmap::xMaxTextureSize = 1024;

//--Multi-Texture
bool StarBitmap::xThisIsMultiTexture = false;
int StarBitmap::xMultiTexX = 0;
int StarBitmap::xMultiTexY = 0;
int StarBitmap::xMultiTexSizeX = 0;
int StarBitmap::xMultiTexSizeY = 0;
uint32_t **StarBitmap::xMultiGLHandles = NULL;

///========================================== System ==============================================
void StarBitmap::DefaultInit(uint32_t pGLHandle)
{
    ///--[Documentation and Setup]
    //--System function, calls the common behavior between the constructors, becuase MinGW doesn't
    //  support C2011 or whatever yet.
    //--This might not actually be true in MinGW64 but I haven't updated it.

    ///--[Variables]
    //--System
    mIsDummyMode = false;
    mDummyPath = NULL;
    mIsInternallyFlipped = false;
    mUploadedPixels = 0;
    mXOffset = 0;
    mYOffset = 0;
    mWidth = 0;
    mHeight = 0;
    mTrueWidth = 0;
    mTrueHeight = 0;
    mUseMipmapping = false;

    //--GL Stuff
    mOwnsHandle = true;
    mGLHandle = pGLHandle;
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++) mGLLists[i] = 0;

    //--Atlasing
    mAtlasLft = 0.0f;
    mAtlasRgt = 1.0f;
    mAtlasTop = 0.0f;
    mAtlasBot = 1.0f;
    mDependencyList = NULL;

    //--Multi-part images for old systems.
    mUsesMultipleTextures = false;
    mMultiTexX = 0;
    mMultiTexY = 0;
    mMultiGLHandles = NULL;

    //--Cache
    mIsHoldingData = false;
    mDontDiscardDataOnUpload = false;
    mDataLength = 0;
    mCompressionType = cNoCompression;
    rLoadBuffer = NULL;

    //--If we already have a valid handle, get some data from it.
    if(mGLHandle)
    {
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &mWidth);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &mHeight);
        mTrueWidth = mWidth;
        mTrueHeight = mHeight;
        mUploadedPixels = (mTrueWidth * mTrueHeight);
    }

    //--Hitboxes
    mTotalHitboxes = 0;
    mHitboxes = NULL;

    //--Path Storage
    mSLMName = NULL;
    rPathLookup = NULL;
    mPadSpriteForEdging = false;
    memset(&mFlagsAtTimeOfCreation, 0, sizeof(StarBitmapFlags));
    mWarnWhenFileRendersWithNoHandle = xNewBitmapsPrintNoHandleWarnings;
    mNoDataHandlerFlag = SUGARBITMAP_NODATA_LOAD_AT_END_OF_TICK;

    ///--[Flag Resolve]
    //--Store the bitmap flags in place at the time of this bitmap's creation, in case they differ
    //  from the ones when the data is uploaded.
    mPadSpriteForEdging = xPadSpriteForEdging;
    memcpy(&mFlagsAtTimeOfCreation, &xStaticFlags, sizeof(StarBitmapFlags));
}
StarBitmap::StarBitmap()
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--'Zero' constructor. A blank StarBitmap which is populated with data later by a different
    //  function.
    DefaultInit(0);
}
StarBitmap::StarBitmap(uint32_t pGLHandle)
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--Default constructor.  Accepts a GLHandle and wraps it.  Pass 0 to cause the "NULL" behavior.
    //  The Bitmap considers itself the texture's owner.
    DefaultInit(pGLHandle);
}
StarBitmap::StarBitmap(const char *pPath)
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--Constructor which accepts a path from the hard drive, and loads the bitmap at that path.
    //  The texture is uploaded and the handle is found.
    //--Passing NULL will cause NULL behavior, no texture.
    uint32_t tHandle = StarBitmap::ProcessBitmap(pPath);
    DefaultInit(tHandle);

    //--If the handle came back as zero, then this might be a multi-texture.
    if(!tHandle && xThisIsMultiTexture)
    {
        //--Copy over the multi-texture data.
        //fprintf(stderr, "Detected multi-texture in %i %s\n", tHandle, pPath);
        //fprintf(stderr, " Sizes were %ix%i textures\n", xMultiTexX, xMultiTexY);
        mUsesMultipleTextures = true;
        mMultiTexX = xMultiTexX;
        mMultiTexY = xMultiTexY;
        mMultiGLHandles = xMultiGLHandles;

        //--Calc sizes.
        mWidth = xMultiTexSizeX;
        mHeight = xMultiTexSizeY;

        //--Null off old data.
        xThisIsMultiTexture = false;
        xMultiGLHandles = NULL;
    }
}
StarBitmap::StarBitmap(uint8_t *pData, int pWidth, int pHeight)
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--Constructor which accepts data (typically from the EIM) and uploads it to get the texture
    //  handle. Doesn't free the data!
    uint32_t tHandle = StarBitmap::UploadData(pData, pWidth, pHeight);
    DefaultInit(tHandle);
}
StarBitmap::~StarBitmap()
{
    ///--[External]
    //--Decrement the static.
    xTotalBitmaps --;

    //--Decrement how many pixels have been loaded.
    xTotalPixels -= mUploadedPixels;

    //--If this bitmap is on the delayed-load list, remove it from that.
    if(!mGLHandle && rPathLookup && !xDontRemoveFromLoadLookups)
    {
        StarLumpManager::Fetch()->RemoveBitmapFromLoadList(this);
    }

    ///--[GL Handlers]
    //--Clear the texture, if you own it.
    if(mOwnsHandle && mGLHandle) glDeleteTextures(1, &mGLHandle);
    delete mDependencyList;

    //--Clear multi-part handles.
    for(int x = 0; x < mMultiTexX; x ++)
    {
        for(int y = 0; y < mMultiTexY; y ++)
        {
            //glDeleteTextures(1, &mMultiGLHandles[x][y]);
        }
        free(mMultiGLHandles[x]);
    }
    free(mMultiGLHandles);

    //--Clear any lists.
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
    }

    ///--[Internal]
    //--If there's anything in the load buffer, clear it.
    free(rLoadBuffer);
    rLoadBuffer = NULL;

    //--Other heap memory.
    free(mDummyPath);
    free(mHitboxes);
    free(mSLMName);
}
void StarBitmap::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((StarBitmap *)pPtr);
}

///--[Public Statics]
//--Tracks how many bitmaps exist. Can be used to make sure bitmaps are getting deallocated correctly.
int StarBitmap::xTotalBitmaps = 0;

//--Tracks how many pixels have been uploaded.
int StarBitmap::xTotalPixels = 0;

//--Tracks how many ticks at least 1 bitmap has been on the loading queue. Used for diagnostics.
int StarBitmap::xTicksWithPendingBitmaps = 0;

//--When a new bitmap is created, it stores this flag value. Bitmaps that attempt to render without
//  having a GL handle *should* load their graphics beforehand, and will bark a warning if they weren't
//  set to. Some images do this deliberately, so set this to false to suppress the warning.
bool StarBitmap::xNewBitmapsPrintNoHandleWarnings = true;

//--When the program is destructing, don't remove bitmaps from the datalibrary's load list. There's
//  no point and it causes memory read errors.
bool StarBitmap::xDontRemoveFromLoadLookups = false;

///===================================== Property Queries =========================================
bool StarBitmap::IsSubBitmap()
{
    return mOwnsHandle;
}
int StarBitmap::GetWidth()
{
    return mWidth;
}
int StarBitmap::GetHeight()
{
    return mHeight;
}
int StarBitmap::GetTrueWidth()
{
    return mTrueWidth;
}
int StarBitmap::GetTrueHeight()
{
    return mTrueHeight;
}
int StarBitmap::GetWidthSafe()
{
    if(mWidth < 1) return 1;
    return mWidth;
}
int StarBitmap::GetHeightSafe()
{
    if(mHeight < 1) return 1;
    return mHeight;
}
int StarBitmap::GetTrueWidthSafe()
{
    if(mTrueWidth < 1) return 1;
    return mTrueWidth;
}
int StarBitmap::GetTrueHeightSafe()
{
    if(mTrueHeight < 1) return 1;
    return mTrueHeight;
}
int StarBitmap::GetXOffset()
{
    return mXOffset;
}
int StarBitmap::GetYOffset()
{
    return mYOffset;
}
uint32_t StarBitmap::GetGLTextureHandle()
{
    return mGLHandle;
}
float StarBitmap::GetAtlasLft()
{
    return mAtlasLft;
}
float StarBitmap::GetAtlasTop()
{
    return mAtlasTop;
}
float StarBitmap::GetAtlasRgt()
{
    return mAtlasRgt;
}
float StarBitmap::GetAtlasBot()
{
    return mAtlasBot;
}

///======================================== Manipulators ==========================================
void StarBitmap::Bind()
{
    glBindTexture(GL_TEXTURE_2D, mGLHandle);
}
void StarBitmap::SetDummyMode(const char *pDummyString)
{
    ///--[Documentation]
    //--Activates dummy mode, meant to be used for bitmaps that are not intended to render but instead act
    //  as placeholders. Often used in mods to allow "core" UI pieces to consider themselves complete classes
    //  but never actually render their image pointers.
    //--Once dummy mode is active, it cannot be un-activated. If no string is provided, it uses a default.
    mIsDummyMode = true;
    ResetString(mDummyPath, pDummyString);

    //--Default case.
    if(!mDummyPath) ResetString(mDummyPath, "No Dummy Path");
}
void StarBitmap::SetInternalFlip(bool pFlag)
{
    mIsInternallyFlipped = pFlag;
}
void StarBitmap::SetSizes(int pXSize, int pYSize)
{
    mWidth = pXSize;
    mHeight = pYSize;
}
void StarBitmap::SetOffset(int pXOffset, int pYOffset)
{
    mXOffset = pXOffset;
    mYOffset = pYOffset;
}
void StarBitmap::SetTrueSizes(int pTrueWid, int pTrueHei)
{
    mTrueWidth = pTrueWid;
    mTrueHeight = pTrueHei;
}
void StarBitmap::SetMipMapping(bool pFlag)
{
    mUseMipmapping = pFlag;
}
void StarBitmap::AssignHandle(uint32_t pHandle, bool pTakeOwnership, bool pDontDeriveSizes)
{
    ///--[Documentation]
    //--Assigns the handle to the bitmap, and also assigns ownership, if necessary. The old
    //  handle is deleted if it was owned. Passing zero will deallocate the handle and set the
    //  bitmap to NULL mode.
    if(mOwnsHandle && mGLHandle)
    {
        glDeleteTextures(1, &mGLHandle);
        mGLHandle = 0;
        mOwnsHandle = false;
    }
    if(!pHandle) return;

    //--Set handle and ownership flag.
    mGLHandle = pHandle;
    mOwnsHandle = pTakeOwnership;

    //--Retrieve data. Note that the 'true' values cannot be discerned from just the bitmap.
    if(!pDontDeriveSizes)
    {
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &mWidth);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &mHeight);
        mTrueWidth = mWidth;
        mTrueHeight = mHeight;
        mUploadedPixels = (mTrueWidth * mTrueHeight);
    }

    //--Clear any lists. Any lists that existed before are now invalid.
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
        mGLLists[i] = 0;
    }
}
void StarBitmap::SetAtlasCoordinates(float pLft, float pTop, float pRgt, float pBot)
{
    mAtlasLft = pLft;
    mAtlasTop = pTop;
    mAtlasRgt = pRgt;
    mAtlasBot = pBot;
}
void StarBitmap::FlipAtlasCoordinates()
{
    mAtlasTop = 1.0f - mAtlasTop;
    mAtlasBot = 1.0f - mAtlasBot;
}
void StarBitmap::MoveAtlasInBy(float pX, float pY)
{
    mAtlasLft = mAtlasLft + pX;
    mAtlasRgt = mAtlasRgt - pX;
    mAtlasTop = mAtlasTop - pY;
    mAtlasBot = mAtlasBot + pY;
}
void StarBitmap::SetSLFName(const char *pName)
{
    ResetString(mSLMName, pName);
}
void StarBitmap::SetSLFPathLookup(PathLookup *pLookup)
{
    rPathLookup = pLookup;
}

///======================================== Core Methods ==========================================
void StarBitmap::LoadDataFromSLF()
{
    ///--[Documentation and Setup]
    //--If this object was marked for delayed load, it will not load its data immediately from the SLM,
    //  instead storing which file it was in and what name it had. This function can be called to
    //  order the image to load its data.
    //--It will do nothing if the image did not get any path data during loading.
    if(mGLHandle || !mSLMName || !rPathLookup) return;

    //--Flags.
    bool tWasAtlas = false;

    //--Order the SLM to open the named file. The path lookup is stored inside the SLM.
    //  For safety, the path lookup verifies that it still exists.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    if(!rSLM->DoesPathExist(rPathLookup))
    {
        fprintf(stderr, "Warning: Bitmap %p %s checked for path lookup, SLM reported it did not exist.\n", this, mSLMName);
        rPathLookup = NULL;
        return;
    }

    //--Order the SLM to open the file.
    rSLM->Open(rPathLookup->mPath);

    ///--[Precache Data]
    //--Get the precache data. If it doesn't exist, bark a warning, and disable delayed loading so it doesn't jam the program.
    StarBitmapPrecache *tDataCache = rSLM->GetImageData(mSLMName);
    if(!tDataCache)
    {
        fprintf(stderr, "Warning: Bitmap %p %s tried to load data from SLF file %s and could not find its data.\n", this, mSLMName, rPathLookup->mPath);
        rPathLookup = NULL;
        return;
    }

    //--If the data's compression type is cDummyCompression, switch to dummy mode.
    if(tDataCache->mCompressionType == cDummyCompression)
    {
        SetDummyMode(mSLMName);
        rPathLookup = NULL;
        return;
    }

    ///--[Store Old Flags]
    //--Variables.
    bool tOldPadFlag = xPadSpriteForEdging;

    //--Set current flags.
    memcpy(&tDataCache->mFlags, &mFlagsAtTimeOfCreation, sizeof(StarBitmapFlags));
    xPadSpriteForEdging = mPadSpriteForEdging;

    //--Disable interrupt calls.
    InterruptPtr rOldInterrupt = StarBitmap::xInterruptCall;
    StarBitmap::xInterruptCall = NULL;

    ///--[Pixel Data]
    //--Upload it.
    uint32_t tHandle = StarBitmap::UploadCompressed(tDataCache);
    AssignHandle(tHandle, true, true);
    //fprintf(stderr, "Assigned handle for bitmap %p %s as %i.\n", this, mSLMName, tHandle);

    ///--[Basic Properties]
    //--Common properties.
    SetOffset(tDataCache->mXOffset, tDataCache->mYOffset);
    SetTrueSizes(tDataCache->mTrueWidth, tDataCache->mTrueHeight);
    SetSizes(tDataCache->mWidth, tDataCache->mHeight);
    if(StarBitmap::xPadSpriteForEdging && !tWasAtlas)
    {
        SetOffset(tDataCache->mXOffset - 2, tDataCache->mYOffset - 2);
        SetSizes(tDataCache->mWidth + 4, tDataCache->mHeight + 4);
    }

    //--Hitboxes, if they exist.
    AllocateHitboxes(tDataCache->mTotalHitboxes);
    for(int i = 0; i < tDataCache->mTotalHitboxes; i ++)
    {
        SetHitbox(i, tDataCache->mHitboxes[i]);
    }

    //--Any image that was loaded out of the SLM is considered to be flipped. Note that this is not
    //  the case for Allegro-stripped bitmaps.
    SetInternalFlip(true);

    ///--[Clean]
    //--Reset bitmap flags.
    xPadSpriteForEdging = tOldPadFlag;

    //--Clean up the data cache.
    free(tDataCache->mArray);
    free(tDataCache->mHitboxes);
    free(tDataCache);

    //--Reset the interrupt pointer.
    StarBitmap::xInterruptCall = rOldInterrupt;
}
void StarBitmap::UnloadData()
{
    ///--[Documentation and Setup]
    //--The object unloads its data and stops rendering. If the object does not have SLF loading data,
    //  an error will be barked because the image would not be able to recover its data.

    //--If this define is set, data never unloads.
    #ifdef ALWAYS_LOAD_BITMAPS_IMMEDIATELY
        return;
    #endif

    //--Decrement pixel total.
    xTotalPixels -= mUploadedPixels;
    mUploadedPixels = 0;

    //--If there's no recovery handlers, spit an error.
    if(!mSLMName || !rPathLookup)
    {
        //--Warning.
        fprintf(stderr, "StarBitmap::UnloadData: Warning, image %p does not have recovery variables.\n", this);

        //--Attempt to locate the image in the datalibrary. This is useful for debug.
        char *tPathToEntry = DataLibrary::Fetch()->GetPathOfEntry(this);

        //--Clean.
        free(tPathToEntry);
    }

    //--If this bitmap is on the delayed-load list, remove it from that.
    StarLumpManager::Fetch()->RemoveBitmapFromLoadList(this);

    //--If there's no GL Handle, stop here. The image may have been enqueued for loading, in which case
    //  it won't have any data to unload.
    if(!mGLHandle) return;

    ///--[GL Handlers]
    //--Clear the texture, if you own it.
    if(mOwnsHandle && mGLHandle) glDeleteTextures(1, &mGLHandle);
    delete mDependencyList;
    mDependencyList = NULL;

    //--Clear multi-part handles.
    for(int x = 0; x < mMultiTexX; x ++)
    {
        for(int y = 0; y < mMultiTexY; y ++)
        {
            //glDeleteTextures(1, &mMultiGLHandles[x][y]);
        }
        free(mMultiGLHandles[x]);
    }
    free(mMultiGLHandles);
    mMultiGLHandles = NULL;

    //--Clear any lists.
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
        mGLLists[i] = 0;
    }

    //--Zero the handle.
    mGLHandle = 0;
}
bool StarBitmap::HandleNoDataAtRender(int pOverride, bool pPrintWarning)
{
    ///--[Documentation and Setup]
    //--If this object attempts to render, but does not have its data loaded (no GL handle), this
    //  function gets called instead. Depending on the set flags, it will either load the data ASAP,
    //  which may cause stutter, or enqueue the image for later loading when the CPU has time.
    //--The function will return whether or not rendering should proceed. If the data uploads immediately,
    //  it can proceed and will return true.
    if(mGLHandle) return true;

    //--If there is no data to load, stop. This bitmap is broken, or incomplete.
    if(!mSLMName || !rPathLookup)
    {
        fprintf(stderr, "Error, bitmap %p attempted to render with no handle, and had no file lookups.\n", this);
        return false;
    }

    //--Debug. Warning only prints if the bitmap is not queued to load.
    if(mWarnWhenFileRendersWithNoHandle && pPrintWarning)
    {
        //--If the bitmap is queued for loading, don't print a warning.
        if(!StarLumpManager::Fetch()->IsQueuedForLoading(this))
        {
            fprintf(stderr, "Image %p %s attempted to render with no handle.\n", this, mSLMName);
            fprintf(stderr, " %i Internal vs %i %i\n", mFlagsAtTimeOfCreation.mUplMagFilter, GL_NEAREST, GL_LINEAR);
        }
    }

    //--Flag override.
    if(pOverride < SUGARBITMAP_NODATA_DONT_CARE || pOverride > SUGARBITMAP_NODATA_LOAD_AT_END_OF_TICK) pOverride = SUGARBITMAP_NODATA_DONT_CARE;

    ///--[Do Nothing]
    //--Just stops right now.
    if(mNoDataHandlerFlag == SUGARBITMAP_NODATA_DO_NOTHING && pOverride == SUGARBITMAP_NODATA_DONT_CARE)
    {
        return false;
    }
    ///--[Load Immediately]
    //--Loads the data immediately, even in the middle of a rendering cycle.
    else if(pOverride == SUGARBITMAP_NODATA_LOAD_IMMEDIATELY || (mNoDataHandlerFlag == SUGARBITMAP_NODATA_LOAD_IMMEDIATELY && pOverride == SUGARBITMAP_NODATA_DONT_CARE))
    {
        LoadDataFromSLF();
        return false;
    }
    ///--[Load Delayed]
    //--Enqueues this bitmap for later upload at the end of a tick.
    else if(pOverride == SUGARBITMAP_NODATA_LOAD_AT_END_OF_TICK || (mNoDataHandlerFlag == SUGARBITMAP_NODATA_LOAD_AT_END_OF_TICK && pOverride == SUGARBITMAP_NODATA_DONT_CARE))
    {
        if(!StarLumpManager::Fetch()->IsQueuedForLoading(this))
        {
            StarLumpManager::Fetch()->AddBitmapToLoadList(this);
        }
        return false;
    }

    //--Error.
    return false;
}
void StarBitmap::CloneFrom(StarBitmap *pBitmap)
{
    ///--[Documentation]
    //--Given a bitmap, extracts all of its information and places it inside this class, implicitly
    //  taking ownership. This is used to replace a bitmap's data but keep any pointers that were
    //  pointing at this it valid.
    //--The provided bitmap should be deleted after this call.
    if(!pBitmap) return;

    ///--[Deallocate]
    //--Decrement how many pixels have been loaded.
    xTotalPixels -= mUploadedPixels;

    //--Clear the texture, if you own it.
    if(mOwnsHandle && mGLHandle) glDeleteTextures(1, &mGLHandle);
    delete mDependencyList;

    //--Clear multi-part handles.
    for(int x = 0; x < mMultiTexX; x ++) free(mMultiGLHandles[x]);
    free(mMultiGLHandles);

    //--Clear any lists.
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
    }

    //--If there's anything in the load buffer, clear it.
    free(rLoadBuffer);
    rLoadBuffer = NULL;

    //--Other heap memory.
    free(mHitboxes);
    free(mSLMName);

    ///--[Liberate]
    //--Copy all of the data from the other bitmap.

    //--System
    mIsInternallyFlipped = pBitmap->mIsInternallyFlipped;
    mUploadedPixels = pBitmap->mUploadedPixels;
    mXOffset = pBitmap->mXOffset;
    mYOffset = pBitmap->mYOffset;
    mWidth = pBitmap->mWidth;
    mHeight = pBitmap->mHeight;
    mTrueWidth  = pBitmap->mTrueWidth;
    mTrueHeight = pBitmap->mTrueHeight;
    mUseMipmapping = pBitmap->mUseMipmapping;

    //--GL Stuff
    mOwnsHandle = pBitmap->mOwnsHandle;
    mGLHandle = pBitmap->mGLHandle;
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        mGLLists[i] = pBitmap->mGLLists[i];
    }

    //--Atlasing
    mAtlasLft = pBitmap->mAtlasLft;
    mAtlasRgt = pBitmap->mAtlasRgt;
    mAtlasTop = pBitmap->mAtlasTop;
    mAtlasBot = pBitmap->mAtlasBot;
    mDependencyList = pBitmap->mDependencyList;

    //--Multi-part images for old systems
    mUsesMultipleTextures = pBitmap->mUsesMultipleTextures;
    mMultiTexX = pBitmap->mMultiTexX;
    mMultiTexY = pBitmap->mMultiTexY;
    //uint32_t **mMultiGLHandles = pBitmap->mIsInternallyFlipped;;

    //--Cache
    mIsHoldingData = pBitmap->mIsHoldingData;;
    mDataNeedsUpload = pBitmap->mDataNeedsUpload;
    mDontDiscardDataOnUpload = pBitmap->mDontDiscardDataOnUpload;
    mDataLength = pBitmap->mDataLength;
    mCompressionType = pBitmap->mCompressionType;
    rLoadBuffer = pBitmap->rLoadBuffer;

    //--Hitboxes
    mTotalHitboxes = pBitmap->mTotalHitboxes;
    mHitboxes = pBitmap->mHitboxes;

    //--Monocolor mode.
    mIsMonoColor = pBitmap->mIsMonoColor;
    memcpy(&mMonoColor, &pBitmap->mMonoColor, sizeof(StarlightColor));

    //--Path Storage
    mSLMName = pBitmap->mSLMName;
    rPathLookup = pBitmap->rPathLookup;
    mPadSpriteForEdging = pBitmap->mPadSpriteForEdging;
    memcpy(&mFlagsAtTimeOfCreation, &pBitmap->mFlagsAtTimeOfCreation, sizeof(StarBitmapFlags));
    mWarnWhenFileRendersWithNoHandle = pBitmap->mWarnWhenFileRendersWithNoHandle;
    mNoDataHandlerFlag = pBitmap->mNoDataHandlerFlag;

    ///--[Zero External]
    //--Remove any heap references from the other bitmap so it doesn't delete them when it gets deallocated.
    pBitmap->mOwnsHandle = false;
    pBitmap->mDependencyList = NULL;
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        mGLLists[i] = 0;
    }
    pBitmap->rLoadBuffer = NULL;
    pBitmap->mHitboxes = NULL;
    pBitmap->mSLMName = NULL;
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void StarBitmap::SaveToDrive(uint32_t pHandle, const char *pDrivePath)
{
    ///--[Documentation]
    //--Saves the bitmap to the hard drive at the given path. This is computationally expensive,
    //  and also only works in the Allegro version.
    //--Static, requires only the handle. Pulls the data off the graphics card.
    if(!pDrivePath) return;
    #if defined _ALLEGRO_PROJECT_

    ///--[Bind]
    if(!pHandle) return;
    glBindTexture(GL_TEXTURE_2D, pHandle);

    ///--[Holding Buffer]
    //--Get width/height.
    int tWidth = 0;
    int tHeight = 0;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH,  &tWidth);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &tHeight);

    //--Create a buffer to receive the pixels.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tBitmapData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tWidth * tHeight * 4);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, tBitmapData);

    ///--[Allegro Bitmap]
    //--Create a new bitmap for output to the hard drive.
    ALLEGRO_BITMAP *tOutBitmap = al_create_bitmap(tWidth, tHeight);
    al_set_target_bitmap(tOutBitmap);
    al_clear_to_color(al_map_rgba(0, 0, 0, 0));

    ///--[Transfer Pixel Data]
    //--Place the pixel data in.
    int i = 0;
    for(int y = 0; y < tHeight; y ++)
    {
        for(int x = 0; x < tWidth; x ++)
        {
            al_put_pixel(x, tHeight-y-1, al_map_rgba(tBitmapData[i+0], tBitmapData[i+1], tBitmapData[i+2], tBitmapData[i+3]));
            i += 4;
        }
    }

    ///--[Write Data, Clean]
    //--Write it. The extension determines the format.
    al_save_bitmap(pDrivePath, tOutBitmap);

    //--Clean.
    al_destroy_bitmap(tOutBitmap);
    free(tBitmapData);

    #endif
}
void StarBitmap::SaveToDrive(const char *pDrivePath)
{
    //--Overload, non-static. Uses local handle.
    SaveToDrive(mGLHandle, pDrivePath);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
void StarBitmap::InterceptGlTexImage2D(GLenum pTarget, GLint pLevel, GLint pInternalFormat, GLsizei pWidth, GLsizei pHeight, GLenum pBorder, GLenum pFormat, GLenum pType, const GLvoid *pData)
{
    ///--[Documentation]
    //--Special call that should be used in place of glTexImage2D. This one automatically trims the
    //  edges of the image off if it happens to be larger than the max texture size. Returns the
    //  texture handle that was generated.
    //--This is also needed for image atlasing.
    //--Data is always assumed to be in the RGBA format using 1 byte for each color.
    if(pWidth <= xMaxTextureSize && pHeight <= xMaxTextureSize)
    {
        xTotalPixels += (pWidth * pHeight);
        glTexImage2D(pTarget, pLevel, pInternalFormat, pWidth, pHeight, pBorder, pFormat, pType, pData);
        return;
    }

    //--Warning print.
    static bool xHasPrintedWarning = false;
    if(!xHasPrintedWarning)
    {
        //--Flag.
        xHasPrintedWarning = true;

        //--Warning notice.
        fprintf(stderr, "==[WARNING]==\n The engine had to trim a bitmap off as it was too large for your OpenGL implementation. Visual bugs may result!\n");
    }

    //--Cast the array.
    uint8_t *rOldData = (uint8_t *)pData;

    //--If we got this far, we need to truncate off the edges of the data. This is an expensive operation,
    //  but at least the program will run.
    int tUseWid = pWidth;
    int tUseHei = pHeight;
    if(tUseWid >= xMaxTextureSize) tUseWid = xMaxTextureSize;
    if(tUseHei >= xMaxTextureSize) tUseHei = xMaxTextureSize;

    //--Allocate a new data array.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tNewData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tUseWid * tUseHei * 4);
    if(!tNewData)
    {
        fprintf(stderr, "Memory Error: Not enough space, malloc returned null.\n");
        return;
    }

    //--Copy across.
    for(int x = 0; x < tUseWid; x ++)
    {
        for(int y = 0; y < tUseHei; y ++)
        {
            //--Position in old array.
            int tOldPos = ((x) + (y * pWidth)) * 4;

            //--Position in new array.
            int tNewPos = ((x) + (y * tUseWid)) * 4;

            //--Copy.
            tNewData[tNewPos+0] = rOldData[tOldPos+0];
            tNewData[tNewPos+1] = rOldData[tOldPos+1];
            tNewData[tNewPos+2] = rOldData[tOldPos+2];
            tNewData[tNewPos+3] = rOldData[tOldPos+3];
        }
    }

    //--Issue the upload.
    xTotalPixels += (tUseWid * tUseHei);
    glTexImage2D(pTarget, pLevel, pInternalFormat, tUseWid, tUseHei, pBorder, pFormat, pType, tNewData);

    //--Clean.
    free(tNewData);
}
void StarBitmap::RestoreDefaultTextureFlags()
{
    //--Restores the texture flags back to program defaults.
    memcpy(&StarBitmap::xStaticFlags, &StarBitmap::xDefaultFlags, sizeof(StarBitmapFlags));

    //--Set them.
    glBindTexture(GL_TEXTURE_2D, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, xStaticFlags.mUplMinFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, xStaticFlags.mUplMagFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, xStaticFlags.mUplBaseLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, xStaticFlags.mUplMaxLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, xStaticFlags.mUplWrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, xStaticFlags.mUplWrapT);
}

///======================================== Lua Hooking ===========================================
void StarBitmap::HookToLuaState(lua_State *pLuaState)
{
    /* ALB_SetTextureProperty(sPropertyName, iEnumeration)
       Sets the property specified for all future texture uploads. Maps ignore this setting. Should
       be called during loading at startup, or during enemy loading, only. */
    lua_register(pLuaState, "ALB_SetTextureProperty", &Hook_ALB_SetTextureProperty);

    /* Bitmap_ActivateAtlasing()
       Bitmap_DeactivateAtlasing()
       Activates/deactivates sprite atlas mode. Images will be placed on the same bitmap instead of
       many differing individual bitmaps. This reduces the number of bind operations that need to
       take place. */
    lua_register(pLuaState, "Bitmap_ActivateAtlasing",   &Hook_Bitmap_ActivateAtlasing);
    lua_register(pLuaState, "Bitmap_DeactivateAtlasing", &Hook_Bitmap_DeactivateAtlasing);

    /* Bitmap_SetNewBitmapNoHandleWarnings(bPrintWarning)
       If a bitmap is set for asset streaming, and has not ordered its data to be displayed before it
       attempts to render, it barks a warning. Newly created bitmaps can unflag this if it's part of
       their design to load data when they need to render. Default is true.  */
    lua_register(pLuaState, "Bitmap_SetNewBitmapNoHandleWarnings", &Hook_Bitmap_SetNewBitmapNoHandleWarnings);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_ALB_SetTextureProperty(lua_State *L)
{
    //ALB_SetTextureProperty("Special Sprite Padding", bValue)
    //ALB_SetTextureProperty("Default MagFilter", iEnumeration)
    //ALB_SetTextureProperty("Default MinFilter", iEnumeration)
    //ALB_SetTextureProperty("Default Wrap S", iEnumeration)
    //ALB_SetTextureProperty("Default Wrap T", iEnumeration)
    //ALB_SetTextureProperty("MagFilter", iEnumeration)
    //ALB_SetTextureProperty("MinFilter", iEnumeration)
    //ALB_SetTextureProperty("Wrap S", iEnumeration)
    //ALB_SetTextureProperty("Wrap T", iEnumeration)
    //ALB_SetTextureProperty("Restore Defaults")
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ALB_SetTextureProperty");

    //--Switcher.
    const char *pPropertyName = lua_tostring(L, 1);

    //--[Flags]
    if(!strcmp(pPropertyName, "Special Sprite Padding") && tArgs == 2)
    {
        StarBitmap::xPadSpriteForEdging = lua_toboolean(L, 2);
    }
    //--[Modify Defaults]
    else if(!strcmp(pPropertyName, "Default MagFilter") && tArgs == 2)
    {
        StarBitmap::xDefaultFlags.mUplMagFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Default MinFilter") && tArgs == 2)
    {
        StarBitmap::xDefaultFlags.mUplMinFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Default Wrap S") && tArgs == 2)
    {
        StarBitmap::xDefaultFlags.mUplWrapS = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Default Wrap T") && tArgs == 2)
    {
        StarBitmap::xDefaultFlags.mUplWrapT = lua_tointeger(L, 2);
    }
    //--[Modify Current Version]
    else if(!strcmp(pPropertyName, "MagFilter") && tArgs == 2)
    {
        StarBitmap::xStaticFlags.mUplMagFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "MinFilter") && tArgs == 2)
    {
        StarBitmap::xStaticFlags.mUplMinFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Wrap S") && tArgs == 2)
    {
        StarBitmap::xStaticFlags.mUplWrapS = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Wrap T") && tArgs == 2)
    {
        StarBitmap::xStaticFlags.mUplWrapT = lua_tointeger(L, 2);
    }
    //--[Restore Defaults]
    else if(!strcmp(pPropertyName, "Restore Defaults") && tArgs == 1)
    {
        StarBitmap::RestoreDefaultTextureFlags();
    }

    return 0;
}
int Hook_Bitmap_ActivateAtlasing(lua_State *L)
{
    //Bitmap_ActivateAtlasing()

    //--Debug
    //fprintf(stderr, "Atlasing begins.\n");
    StarBitmap::xActivationTime = GetGameTime();
    StarBitmap::xIsAtlasModeActive = true;

    return 0;
}
int Hook_Bitmap_DeactivateAtlasing(lua_State *L)
{
    //Bitmap_DeactivateAtlasing()

    //--If an atlas is currently running, finish it.
    if(StarBitmap::xrActiveAtlas)
    {
        //fprintf(stderr, "Finishing atlas.\n");
        StarBitmap::xrActiveAtlas->FinishAtlas();
        //fprintf(stderr, "Finished atlas.\n");
        StarBitmap::xrActiveAtlas = NULL;
    }

    //--Debug.
    StarBitmap::xIsAtlasModeActive = false;

    return 0;
}
int Hook_Bitmap_SetNewBitmapNoHandleWarnings(lua_State *L)
{
    //Bitmap_SetNewBitmapNoHandleWarnings(bPrintWarning)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Bitmap_SetNewBitmapNoHandleWarnings");

    //--Set.
    StarBitmap::xNewBitmapsPrintNoHandleWarnings = lua_toboolean(L, 1);
    return 0;
}
