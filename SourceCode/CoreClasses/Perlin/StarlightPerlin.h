///====================================== StarlightPerlin =========================================
//--Perlin noise wrapper for the Starlight engine. Class can be instantiated if different settings
//  are required, most versions of the engine should use the "gStatic" class which can be accessed
//  statically and uses common properties.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "XExternal/PerlinNoise/PerlinNoise.hpp"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class StarlightPerlin
{
    private:
    //--System
    bool mIsReady;

    //--Storage
    uint32_t mSeed;
    float mFrequency;
    int mOctaves;
    siv::PerlinNoise *mLocalNoise;

    protected:

    public:
    //--System
    StarlightPerlin();
    ~StarlightPerlin();

    //--Public Variables
    //--Property Queries
    float Roll(float pX);
    float Roll(float pX, float pY);
    float Roll(float pX, float pY, float pZ);
    float RollClamped(float pX);
    float RollClamped(float pX, float pY);
    float RollClamped(float pX, float pY, float pZ);

    //--Manipulators
    void SetSeed(uint32_t pSeed);
    void SetFrequency(float pFrequency);
    void SetOctaves(int pOctaves);
    void Boot();

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static StarlightPerlin *xgStatic;

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_STPerlin_Quickroll(lua_State *L);

