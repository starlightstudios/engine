//--Base
#include "TranslationExecutor.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define TE_DEBUG
#ifdef TE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
TranslationExecutor::TranslationExecutor()
{
    ///--[TranslationExecutor]
    //--[System]
    mCurrentMode = TE_MODE_NONE;
    mRootPath = NULL;
    mTranslationFilePath = NULL;

    //--[Input Parsing]
    mBypassInstructionLinesLeft = 0;
    mParsingData = new StarAutoBuffer();

    //--[Current File]
    mCurrentFileName = NULL;
    mOrigTextList = new StarLinkedList(true);
    mReplaceTextList = new StarLinkedList(true);

    //--[Untranslation]
    mUntranslateFilePath = NULL;
    mUntranslateFileData = new StarAutoBuffer();
}
TranslationExecutor::~TranslationExecutor()
{
    free(mRootPath);
    free(mTranslationFilePath);
    delete mParsingData;
    free(mCurrentFileName);
    delete mOrigTextList;
    delete mReplaceTextList;
    free(mUntranslateFilePath);
    delete mUntranslateFileData;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void TranslationExecutor::ExecuteTranslationFile(const char *pRootPath, const char *pFileName)
{
    ///--[Documentation]
    //--Given a root path (usually a game's directory) and the path to a translation file, runs across
    //  the translation file and applies the translation within.
    if(!pRootPath || !pFileName) return;

    //--Debug.
    DebugPush(true, "Executing translation file.\n");
    DebugPrint("Root path: %s\n", pRootPath);
    DebugPrint("Translation file path: %s\n", pFileName);

    //--Store the paths.
    ResetString(mRootPath, pRootPath);
    ResetString(mTranslationFilePath, pFileName);

    ///--[Execution]
    //--Open the translation file and begin execution.
    VirtualFile *fInfile = new VirtualFile(pFileName, false);
    if(!fInfile->IsReady())
    {
        delete fInfile;
        DebugPop("Error, translation file was not found.\n");
        return;
    }

    //--Get file length. Iterate across the file.
    int tFileLength = fInfile->GetLength();
    DebugPrint("Parsing file lines. File length: %i\n", tFileLength);

    //--Iterate across the file.
    int tOldPosition = fInfile->GetCurPosition();
    while(fInfile->GetCurPosition() < tFileLength)
    {
        //--Get the line in question.
        char *tLine = ReadLine(fInfile);
        DebugPrint("Read line - ");
        DebugPrint("%s", tLine);
        DebugPrint("%03i ", tLine[0]);

        //--Handle the line.
        HandleLine(tLine, (mBypassInstructionLinesLeft > 0));
        DebugPrint("Handled lined.\n");

        //--Clean.
        free(tLine);

        //--No letters read check.
        //fprintf(stderr, "%i %i\n", tOldPosition, fInfile->GetCurPosition());
        if(tOldPosition >= fInfile->GetCurPosition())
        {
            DebugPrint("Breakout, line read zero characters.\n");
            break;
        }

        //--Next read.
        tOldPosition = fInfile->GetCurPosition();
    }

    ///--[Clean]
    delete fInfile;

    //--Debug.
    DebugPop("Finished translation.\n");
}

///=================================== Private Core Methods =======================================
char *TranslationExecutor::ReadLine(VirtualFile *fInfile)
{
    ///--[Documentation]
    //--Given a virtual file, reads the next line. Looks for EOF/EOL cases. Returns the parsed line,
    //  which can be NULL on error, or zero characters. Note that the line may contain UTF letters.
    //--The caller must free() the returned string.
    if(!fInfile) return NULL;

    //--Create a buffer.
    uint8_t tByte = 0;
    StarAutoBuffer *tLineBuf = new StarAutoBuffer();

    //--Store file length.
    int tFileLength = fInfile->GetLength();

    ///--[Read Bytes]
    int tCount = 0;
    while(true)
    {
        ///--[Read]
        //--Read a byte.
        fInfile->Read(&tByte, sizeof(uint8_t), 1);

        ///--[Check]
        //--EOL characters 10/13.
        if(tByte == 10 || tByte == 13)
        {
            //--Mark the end of the string.
            tLineBuf->AppendNull();

            //--Check if the next byte is also a 10/13. If so, read it so we don't double-newline.
            //  If not, seek back one byte.
            fInfile->Read(&tByte, sizeof(uint8_t), 1);
            if(tByte != 10 && tByte != 13)
            {
                fInfile->SeekRelative(-1);
            }
            break;
        }

        ///--[Store]
        //--Append to buffer.
        tCount ++;
        tLineBuf->AppendUInt8(tByte);

        ///--[Ending Case]
        //--EOF case:
        if(fInfile->GetCurPosition() >= tFileLength)
        {
            tLineBuf->AppendNull();
            //fprintf(stderr, "Hit EOF.\n");
            break;
        }
    }

    ///--[Handle]
    uint8_t *tRawData = tLineBuf->LiberateData();
    //fprintf(stderr, "Read raw data. %i bytes.\n%s\n", tCount, tRawData);

    ///--[Clean]
    delete tLineBuf;
    return (char *)tRawData;
}
void TranslationExecutor::HandleLine(const char *pLine, bool pIgnoreInstructions)
{
    ///--[Documentation]
    //--Given a string from the parser, executes the necessary instructions or stores the results
    //  based on what mode the object is in. Note that the line may contain UTF letters, but instructions
    //  cannot have these, only strings for storage.
    if(!pLine) return;

    ///--[Zero-Length]
    //--Strings that are zero-length occur because some operating systems (Windows) encode their EOL
    //  with two numbers, 10 and 13 (NL/CR). These can be safely ignored.
    if(pLine[0] == '\0') return;

    //--Otherwise, store the length in number of bytes.
    uint8_t tTotalBytes = strlen(pLine);

    ///--[Instruction Line]
    //--If the zeroth letter is a ':', then this is an instruction line. These change the state and
    //  may have arguments. If the argument is missing, it is assumed to be the lowest value possible,
    //  which is usually 1.
    DebugPush(true, "Handling line.\n", pLine);

    //--Instructions are bypassed if the flag pIgnoreInstructions is set. This is because it is possible,
    //  theoretically, for a string that needs translation to begin with a ':'.
    if(pLine[0] == ':' && !pIgnoreInstructions)
    {
        //--Comment. Ignored by parser.
        if(!strncmp(pLine, "::", (int)strlen("::")))
        {
            fprintf(stderr, "Ignoring comment.\n");
        }
        //--Filename. Changes mode to store the name of the file in question. Optional argument
        //  for number of lines expected for the filename, default is 1.
        else if(!strncmp(pLine, ":FILENAME", (int)strlen(":FILENAME")))
        {
            //--Debug.
            DebugPrint("Filename case.\n");

            //--Flags.
            mCurrentMode = TE_MODE_FILENAME;
            mBypassInstructionLinesLeft = 1;

            //--Scrap and reset the parsing autobuffer.
            ResetAutobuffer();

            //--New files also clear the strings.
            mOrigTextList->ClearList();
            mReplaceTextList->ClearList();

            //--Check if there's no extra arguments.
            if(tTotalBytes == (int)strlen(":FILENAME"))
            {
                DebugPop("Handled line.\n");
                return;
            }

            //--Optional: A number may be appended on the end after a space.
            int tStartLetter = (int)strlen(":FILENAME ");
            int tArgument = atoi(&pLine[tStartLetter]);

            //--Set, apply cap. Must be at least 1.
            mBypassInstructionLinesLeft = tArgument;
            if(mBypassInstructionLinesLeft < 1) mBypassInstructionLinesLeft = 1;
        }
        //--End-Filename. Exits filename mode and stores the file name.
        else if(!strncmp(pLine, ":ENDFILENAME", (int)strlen(":ENDFILENAME")))
        {
            //--Debug.
            DebugPrint("End-Filename case.\n");

            //--Error check:
            if(mCurrentMode != TE_MODE_FILENAME)
            {
                fprintf(stderr, "Warning: Malformed file. Found an End-Filename but was not in Filename mode.\n");
                DebugPop("Malformation error.\n");
                return;
            }

            //--Flags.
            mCurrentMode = TE_MODE_NONE;

            //--Take whatever is in the parsing buffer and make it the filename.
            char *tParsedData = (char *)mParsingData->LiberateData();
            ResetString(mCurrentFileName, tParsedData);

            //--Scrap and reset the parsing autobuffer.
            ResetAutobuffer();
        }
        //--Original Line. Adds an untranslated line.
        else if(!strncmp(pLine, ":ORIG", (int)strlen(":ORIG")))
        {
            //--Debug.
            DebugPrint("Original Line case.\n");

            //--Flags.
            mCurrentMode = TE_MODE_ORIGINAL;
            mBypassInstructionLinesLeft = 1;

            //--Scrap and reset the parsing autobuffer.
            ResetAutobuffer();

            //--Check if there's no extra arguments.
            if(tTotalBytes == (int)strlen(":ORIG"))
            {
                DebugPop("Handled line.\n");
                return;
            }

            //--Optional: A number may be appended on the end after a space.
            int tStartLetter = (int)strlen(":ORIG ");
            int tArgument = atoi(&pLine[tStartLetter]);

            //--Set, apply cap. Must be at least 1.
            mBypassInstructionLinesLeft = tArgument;
            if(mBypassInstructionLinesLeft < 1) mBypassInstructionLinesLeft = 1;
        }
        //--End-Original Line. Ends original line input. Should be followed by a Change Line input.
        else if(!strncmp(pLine, ":ENDORIG", (int)strlen(":ENDORIG")))
        {
            //--Debug.
            DebugPrint("End-Original Line case.\n");

            //--Error check:
            if(mCurrentMode != TE_MODE_ORIGINAL)
            {
                fprintf(stderr, "Warning: Malformed file. Found an End-Original but was not in Original mode.\n");
                DebugPop("Malformation error.\n");
                return;
            }

            //--Flags.
            mCurrentMode = TE_MODE_NONE;

            //--Run across the buffer, add each line to the list.
            AddParsedLinesToList((char *)mParsingData->GetRawData(), mParsingData->GetCursor(), mOrigTextList);

            //--Scrap and reset autobuffer.
            ResetAutobuffer();
        }
        //--Change Line. Adds translated lines.
        else if(!strncmp(pLine, ":CHANGETO", (int)strlen(":CHANGETO")))
        {
            //--Debug.
            DebugPrint("Changeto case.\n");

            //--Flags.
            mCurrentMode = TE_MODE_CHANGETO;
            mBypassInstructionLinesLeft = 1;

            //--Scrap and reset the parsing autobuffer.
            ResetAutobuffer();

            //--Check if there's no extra arguments.
            if(tTotalBytes == (int)strlen(":CHANGETO"))
            {
                DebugPop("Handled line.\n");
                return;
            }

            //--Optional: A number may be appended on the end after a space.
            int tStartLetter = (int)strlen(":CHANGETO ");
            int tArgument = atoi(&pLine[tStartLetter]);

            //--Set, apply cap. Must be at least 1.
            mBypassInstructionLinesLeft = tArgument;
            if(mBypassInstructionLinesLeft < 1) mBypassInstructionLinesLeft = 1;
        }
        //--End-Change Line. Ends changed line input. Applies the translation to the file in question.
        else if(!strncmp(pLine, ":ENDCHANGETO", (int)strlen(":ENDCHANGETO")))
        {
            //--Debug.
            DebugPrint("End-Changeto case.\n");

            //--Error check:
            if(mCurrentMode != TE_MODE_CHANGETO)
            {
                fprintf(stderr, "Warning: Malformed file. Found an End-Change but was not in Change mode.\n");
                DebugPop("Malformation error.\n");
                return;
            }

            //--Flags.
            mCurrentMode = TE_MODE_NONE;

            //--Run across the buffer, add each line to the list.
            AddParsedLinesToList((char *)mParsingData->GetRawData(), mParsingData->GetCursor(), mReplaceTextList);

            //--Scrap and reset autobuffer.
            ResetAutobuffer();

            //--Execute line changing.
            ChangeLines();
        }
        //--When the file is finished, writes the untranslate file.
        else if(!strncmp(pLine, ":WRITEUNTRANSLATE", (int)strlen(":WRITEUNTRANSLATE")))
        {
            //--Debug.
            DebugPrint("Write-Untranslate case.\n");

            //--The file is stored in the Translations folder of the game.
            char *tUntranslatePath = InitializeString("%s/Translations/Untranslate.lua", mRootPath);

            //--Open it again in write mode.
            FILE *fWriteFile = fopen(tUntranslatePath, "wb");
            uint8_t *rFileData = mUntranslateFileData->GetRawData();
            fwrite(rFileData, sizeof(uint8_t), mUntranslateFileData->GetCursor(), fWriteFile);

            //--Clean.
            free(tUntranslatePath);
            fclose(fWriteFile);
        }

        //--Do not copy this line into any buffer.
        DebugPop("Handled line.\n");
        return;
    }

    ///--[Non-Instruction Line]
    //--Depending on the mode, non-instruction lines are copied into a buffer and used for something.
    //  They might also be ignored if the mode is not set or the file is malformed.
    if(mBypassInstructionLinesLeft > 0) mBypassInstructionLinesLeft --;

    //--Add whatever was found in this line to the parsing buffer. Does nothing if not in a mode.
    if(mCurrentMode == TE_MODE_NONE) return;
    mParsingData->AppendString(pLine);
    DebugPop("Non-instruction line. Copied.\n");
}
void TranslationExecutor::ResetAutobuffer()
{
    //--Scraps and resets the parsing autobuffer. Happens a lot.
    delete mParsingData;
    mParsingData = new StarAutoBuffer();
}
void TranslationExecutor::AddParsedLinesToList(char *pBuffer, int pLen, StarLinkedList *pList)
{
    //--Given a buffer which contains one or more strings, each null-terminated, splits them up
    //  and places them on a list.
    if(!pBuffer || !pList) return;

    //--Setup.
    int tStart = 0;

    //--Iterate.
    for(int i = 0; i < pLen; i ++)
    {
        //--Null, ends current string.
        if(pBuffer[i] == '\0')
        {
            char *nNewString = InitializeString(&pBuffer[tStart]);
            pList->AddElementAsTail("X", nNewString, &FreeThis);
            tStart = i+1;
        }
    }
}
void TranslationExecutor::ChangeLines()
{
    ///--[Documentation]
    //--Once a set of original and change lines is assembled, this algorithm runs across the lines
    //  in the given file, locates them, and replaces them with their changes versions.

    ///--[Error Check]
    //--The original block should have the same number of lines as the change block.
    if(mOrigTextList->GetListSize() != mReplaceTextList->GetListSize())
    {
        fprintf(stderr, "Warning: Original and Replacement lines have different counts. %i %i\n", mOrigTextList->GetListSize(), mReplaceTextList->GetListSize());
        return;
    }

    ///--[Auto Buffer]
    //--This will contain the outputted file.
    StarAutoBuffer *tOutfile = new StarAutoBuffer();

    ///--[Open]
    //--Open the file in question.
    char *tFilePath = InitializeString("%s%s", mRootPath, mCurrentFileName);
    //fprintf(stderr, "Opening %s for editing.\n", tFilePath);
    VirtualFile *fChangeFile = new VirtualFile(tFilePath, false);

    //--Get file length. Iterate across the file.
    int tFileLength = fChangeFile->GetLength();
    int tPreviousPosition = fChangeFile->GetCurPosition();
    while(fChangeFile->GetCurPosition() < tFileLength)
    {
        //--Get the line in question.
        char *tLine = ReadLine(fChangeFile);
        //fprintf(stderr, "Read line out %s\n", tLine);

        //--Check the original set to see if it matches any.
        bool tMatch = false;
        char *rOriginal = (char *)mOrigTextList->SetToHeadAndReturn();
        char *rReplace  = (char *)mReplaceTextList->SetToHeadAndReturn();
        while(rOriginal && rReplace)
        {
            //--Exact match: Replace.
            if(!strcmp(rOriginal, tLine))
            {
                //--Put the replacement line in.
                tMatch = true;
                tOutfile->AppendStringWithoutNull(rReplace);
                tOutfile->AppendCharacter(13);
                tOutfile->AppendCharacter(10);
                //fprintf(stderr, " Line matched. Replacing.\n");

                //--Remove the entries to speed up execution.
                //mOrigTextList->RemoveRandomPointerEntry();
                //mReplaceTextList->RemoveRandomPointerEntry();
                break;
            }

            //--Next.
            rOriginal = (char *)mOrigTextList->IncrementAndGetRandomPointerEntry();
            rReplace  = (char *)mReplaceTextList->IncrementAndGetRandomPointerEntry();
        }

        //--No match found. Print the original line.
        if(!tMatch)
        {
            tOutfile->AppendStringWithoutNull(tLine);
            tOutfile->AppendCharacter(13);
            tOutfile->AppendCharacter(10);
        }

        //--Clean.
        free(tLine);

        //--Error check: No letters got read somehow.
        //fprintf(stderr, "%i %i\n", tPreviousPosition, fChangeFile->GetCurPosition());
        if(tPreviousPosition >= fChangeFile->GetCurPosition())
        {
            //fprintf(stderr, "No letters read! Exiting.\n");
            break;
        }

        //--Next line.
        tPreviousPosition = fChangeFile->GetCurPosition();
    }

    ///--[Write]
    //--Close the virtual file.
    delete fChangeFile;

    //--Open it again in write mode.
    FILE *fWriteFile = fopen(tFilePath, "wb");
    uint8_t *rFileData = tOutfile->GetRawData();
    fwrite(rFileData, sizeof(uint8_t), tOutfile->GetCursor(), fWriteFile);
    fclose(fWriteFile);

    ///--[Add to Untranslation]
    //--The reverse set of instructions is written to the untranslate buffer, allowing the translation to be
    //  reversed quickly and easily.
    //--First, the filename.
    mUntranslateFileData->AppendStringWithoutNull(":FILENAME 1\n");
    mUntranslateFileData->AppendStringWithoutNull(mCurrentFileName);
    mUntranslateFileData->AppendCharacter('\n');
    mUntranslateFileData->AppendStringWithoutNull(":ENDFILENAME\n");

    //--Next, the "original" lines are the translated lines.
    char *tOrigLine = InitializeString(":ORIG %i\n", mReplaceTextList->GetListSize());
    mUntranslateFileData->AppendStringWithoutNull(tOrigLine);
    char *rReplace = (char *)mReplaceTextList->PushIterator();
    while(rReplace)
    {
        mUntranslateFileData->AppendStringWithoutNull(rReplace);
        mUntranslateFileData->AppendCharacter('\n');
        rReplace = (char *)mReplaceTextList->AutoIterate();
    }
    mUntranslateFileData->AppendStringWithoutNull(":ENDORIG\n");

    //--Now the "replacement" lines are the original lines.
    char *tReplaceLine = InitializeString(":CHANGETO %i\n", mOrigTextList->GetListSize());
    mUntranslateFileData->AppendStringWithoutNull(tReplaceLine);
    char *rOriginal = (char *)mOrigTextList->PushIterator();
    while(rOriginal)
    {
        mUntranslateFileData->AppendStringWithoutNull(rOriginal);
        mUntranslateFileData->AppendCharacter('\n');
        rOriginal = (char *)mOrigTextList->AutoIterate();
    }
    mUntranslateFileData->AppendStringWithoutNull(":ENDCHANGETO\n");

    //--Clean.
    free(tOrigLine);
    free(tReplaceLine);

    ///--[Clean]
    //--Clear off the text lists.
    mOrigTextList->ClearList();
    mReplaceTextList->ClearList();

    //--Deallocate memory.
    free(tFilePath);
    delete tOutfile;

    //--Debug.
    //fprintf(stderr, "Finished writing translated file.\n");
}

///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void TranslationExecutor::HookToLuaState(lua_State *pLuaState)
{
    /* TE_Execute(sRootPath, sTranslationFilePath)
       Creates a TranslationExecutor and takes the given files to run the translation. */
    lua_register(pLuaState, "TE_Execute", &Hook_TE_Execute);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_TE_Execute(lua_State *L)
{
    ///--[Argument Listing]
    //TE_Execute(sRootPath, sTranslationFilePath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("TE_Execute");

    //--Create object.
    TranslationExecutor *tExecutor = new TranslationExecutor();
    tExecutor->ExecuteTranslationFile(lua_tostring(L, 1), lua_tostring(L, 2));

    //--Clean.
    delete tExecutor;
    return 0;
}
