//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "CorrupterMenu.h"
#include "ContextMenu.h"
#include "InventoryItem.h"
#include "InventorySubClass.h"

//--CoreClasses
#include "DataList.h"
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DisplayManager.h"
#include "EntityManager.h"

//--System
void PandemoniumLevel::SetupUIConstants()
{
    //--Fail if the images didn't resolve.
    if(!Images.mIsReady) return;

    //--Color setup. Can also be done by the entities pane.
    cTextColor.SetRGBAF(1.00f, 1.00f, 1.00f, 1.0f);
    cSelectColor.SetRGBAF(0.50f, 0.05f, 0.40f, 1.0f);
    cBorderColor.SetRGBI(192, 192, 192);
    cInnerColor.SetRGBAI(0, 0, 0, 192);

    //--Buttons at the top of the screen.
    float cFontScale = 1.0f;
    mSystemMenuBtn.SetWH   (0.0f,                0.0f, Images.Data.rUIFontSmall->GetTextWidth(">Open System Menu<")    * cFontScale, (Images.Data.rUIFontSmall->GetTextHeight() * cFontScale) + 5.0f);
    mCorrupterMenuBtn.SetWH(0.0f, mSystemMenuBtn.mBot, Images.Data.rUIFontSmall->GetTextWidth(">Open Corrupter Menu<") * cFontScale, (Images.Data.rUIFontSmall->GetTextHeight() * cFontScale) + 5.0f);

    //--Constants.
    float cCharPaneWidth = 400.0f;
    float cCharPaneHeight = 600.0f;
    float cEntityPaneWidth = cCharPaneWidth;
    float cEntityPaneHeight = VIRTUAL_CANVAS_Y - cCharPaneHeight;
    float cConsolePaneWidth = VIRTUAL_CANVAS_X - cCharPaneWidth;
    float cConsolePaneHeight = 400.0f;

    //--Size Constants
    cFontSize = 1.0f;
    cFontSpacing = Images.Data.rUIFontSmall->GetTextHeight() * cFontSize;

    //--Set the character pane.
    CharPane.cIndent = 3.0f;
    CharPane.cLft = VIRTUAL_CANVAS_X - cCharPaneWidth;
    CharPane.cTop = 0.0f;
    CharPane.cWid = cCharPaneWidth;
    CharPane.cHei = cCharPaneHeight;

    //--Set the entity pane.
    EntityPane.cIndent = 3.0f;
    EntityPane.cLft = VIRTUAL_CANVAS_X - cEntityPaneWidth;
    EntityPane.cTop = CharPane.cTop + cCharPaneHeight;
    EntityPane.cWid = cEntityPaneWidth;
    EntityPane.cHei = cEntityPaneHeight;

    //--Set the console pane. Bottom left.
    ConsolePane.cIndent = 3.0f;
    ConsolePane.cLft = 0.0f;
    ConsolePane.cTop = VIRTUAL_CANVAS_Y - cConsolePaneHeight;
    ConsolePane.cWid = cConsolePaneWidth;
    ConsolePane.cHei = cConsolePaneHeight;

    //--Inventory values.
    mInventoryMembers = 0;
}

//--Manipulators
void PandemoniumLevel::SetActivePortraitName(const char *pName)
{
    ResetString(mActivePortraitName, pName);
}
void PandemoniumLevel::SetActivePortrait(StarBitmap *pImage)
{
    rActivePortrait = pImage;
}

//--Core Methods
void PandemoniumLevel::CharacterHandleItemUse(int pItemSlot, bool pIsUse, bool pIsDrop)
{
    //--Routing function. When the player clicks on an item in their inventory pane (which is part
    //  of the character pane) then this function is called to handle it.
    //--If both flags are somehow true, then the Use case takes priority.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerCharacter) return;

    //--If the item slot is 0, this is the "Show Self-Menu" click. All other cases are dropped.
    if(pItemSlot == 0)
    {
        //--Run the population function, which is in the Lua. The startup will set the script
        //  which gets called.
        //--Under most circumstances, the menu will be the only thing on the stack. In any case,
        //  do the usual push.
        PushSelfMenu();
    }
    //--If the Use case is in effect...
    else if(pIsUse)
    {
        //--These all fail if the actor is stunned or controlled.
        if(rPlayerCharacter->IsStunned() || rPlayerCharacter->IsControlled() || rPlayerCharacter->GetTeam() != TEAM_PLAYER) return;

        //--If the item slot is 1 and the extra slots are in use, AND mExtraIsWeapon is true, then
        //  this is a weapon-unequip case.
        if(pItemSlot == 1 && mExtraIsWeapon && mExtraInventorySlots > 1)
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
                rPlayerCharacter->UnequipWeapon();
            Actor::xInstruction = INSTRUCTION_COMPLETETURN;
        }
        //--If the item slot is 1 and the extra slots are in use, but the extra isn't a weapon, then
        //  it's the armor.
        /*else if(pItemSlot == 1 && !mExtraIsWeapon && mExtraInventorySlots > 1)
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
                rPlayerCharacter->UnequipArmor();
            Actor::xInstruction = INSTRUCTION_COMPLETETURN;
        }
        //--If the item slot is 2 and mExtraInventorySlots is 3, it's the armor.
        else if(pItemSlot == 2 && mExtraInventorySlots == 3)
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
                rPlayerCharacter->UnequipArmor();
            Actor::xInstruction = INSTRUCTION_COMPLETETURN;
        }*/
        //--Otherwise, use the item by the slot.
        else
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
            {
                InventoryPack *rPack = (InventoryPack *)mInventoryList->GetElementBySlot(pItemSlot - mExtraInventorySlots);
                if(rPack)
                    rPlayerCharacter->UseItemByPtr(rPack->rItem);
            }
        }
    }
    //--Otherwise, if the Drop case is in effect...
    else if(pIsDrop)
    {
        //--These all fail if the actor is stunned or controlled.
        if(rPlayerCharacter->IsStunned() || rPlayerCharacter->IsControlled() || rPlayerCharacter->GetTeam() != TEAM_PLAYER) return;

        //--If the item slot is 0 and the extra slots are in use, AND mExtraIsWeapon is true, then
        //  this is a weapon-drop case.
        if(pItemSlot == 1 && mExtraIsWeapon && mExtraInventorySlots > 0)
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
                rPlayerCharacter->DropItem(rPlayerCharacter->GetWeapon());
            Actor::xInstruction = INSTRUCTION_COMPLETETURN;
        }
        //--If the item slot is 0 and the extra slots are in use, but the extra isn't a weapon, then
        //  it's the armor.
        /*else if(pItemSlot == 1 && !mExtraIsWeapon && mExtraInventorySlots > 0)
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
                rPlayerCharacter->DropItem(rPlayerCharacter->GetArmor());
            Actor::xInstruction = INSTRUCTION_COMPLETETURN;
        }
        //--If the item slot is 1 and mExtraInventorySlots is 2, it's the armor.
        else if(pItemSlot == 2 && mExtraInventorySlots == 3)
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
                rPlayerCharacter->DropItem(rPlayerCharacter->GetArmor());
            Actor::xInstruction = INSTRUCTION_COMPLETETURN;
        }*/
        //--Otherwise, drop the item by the slot.
        else
        {
            if(!rPlayerCharacter->IsStunned() && !rPlayerCharacter->IsControlled())
            {
                InventoryPack *rPack = (InventoryPack *)mInventoryList->GetElementBySlot(pItemSlot - mExtraInventorySlots);
                if(rPack) rPlayerCharacter->DropItemByPtr(rPack->rItem);
                Actor::xInstruction = INSTRUCTION_COMPLETETURN;
            }
        }
    }
    //--This is an examine case.
    else
    {
        //--If the item slot is 0 and the extra slots are in use, AND mExtraIsWeapon is true, then
        //  this is a weapon-drop case.
        if(pItemSlot == 1 && mExtraIsWeapon && mExtraInventorySlots > 0)
        {
            ContextMenu::ExamineTargetItem(rPlayerCharacter->GetWeapon());
        }
        //--If the item slot is 0 and the extra slots are in use, but the extra isn't a weapon, then
        //  it's the armor.
        /*else if(pItemSlot == 1 && !mExtraIsWeapon && mExtraInventorySlots > 0)
        {
            ContextMenu::ExamineTargetItem(rPlayerCharacter->GetArmor());
        }
        //--If the item slot is 1 and mExtraInventorySlots is 2, it's the armor.
        else if(pItemSlot == 2 && mExtraInventorySlots == 3)
        {
            ContextMenu::ExamineTargetItem(rPlayerCharacter->GetArmor());
        }*/
        //--Otherwise, it's an inventory pack.
        else
        {
            InventoryPack *rPack = (InventoryPack *)mInventoryList->GetElementBySlot(pItemSlot - mExtraInventorySlots);
            if(rPack && rPack->rItem) ContextMenu::ExamineTargetItem(rPack->rItem);
        }
    }
}

//--Rendering
void PandemoniumLevel::RenderCharacterPane()
{
    //--Renders the pane on the right side which shows the current character and their status. Also
    //  has buttons for inventory and examination.

    //--Position setup.
    glTranslatef(CharPane.cLft, CharPane.cTop, 0.0f);

    //--Fast-access. Keeps things aligned.
    float cWid = CharPane.cWid;
    float cHei = CharPane.cHei;
    float cInd = CharPane.cIndent;

    //--[Border]
    //--Subroutine handles it.
    RenderBorder(0.0f, 0.0f, cWid, cHei, cInd, cBorderColor, cInnerColor);

    //--[Character]
    //--Main portrait. May be NULL. This will be overridden by RenderDamageAnimation() if that is occurring.
    if(IsDamageAnimating())
    {
        RenderDamageAnimation();
    }
    //--Character portrait.
    else if(rActivePortrait)
    {
        //--Constants
        float cPorWid = CharPane.cWid;
        float cPorHei = CharPane.cHei - 100.0f;

        //--Sizes
        float cImgWidth = rActivePortrait->GetTrueWidth();
        float cImgHeight = rActivePortrait->GetTrueHeight();

        //--Error check. It's nominally not possible for these to be zero.
        if(cImgWidth == 0.0f) cImgWidth = 1.0f;
        if(cImgHeight == 0.0f) cImgHeight = 1.0f;

        //--Calculate the max amount of stretch available.
        float cHSize = cPorWid - (CharPane.cIndent * 2.0f);
        float cVSize = cPorHei - (CharPane.cIndent * 2.0f);
        float cHMult = cHSize / cImgWidth;
        float cVMult = cVSize / cImgHeight;

        //--Error check.
        if(cHMult == 0.0f || cVMult == 0.0f)
        {

        }
        //--Safe case.
        else
        {
            //--Rendering positions.
            glTranslatef(cPorWid * 0.50f, cPorHei / 2.0f, 0.0f);
            float tRenderX = cImgWidth / -2.0f;
            float tRenderY = cImgHeight / -2.0f;

            //--Render down slightly.
            tRenderY = tRenderY + 25.0f;

            //--If there are more (or equal) HMult, use the VMult.
            if(cHMult > cVMult)
            {
                glScalef(cVMult, cVMult, 1.0f);
                rActivePortrait->Draw(tRenderX, tRenderY);
                glScalef(1.0f / cVMult, 1.0f / cVMult, 1.0f);
            }
            //--Use the HMult.
            else
            {
                glScalef(cHMult, cHMult, 1.0f);
                rActivePortrait->Draw(tRenderX, tRenderY);
                glScalef(1.0f / cHMult, 1.0f / cHMult, 1.0f);
            }

            //--Clean.
            glTranslatef(cPorWid * -0.50f, cPorHei / -2.0f, 0.0f);
        }
    }

    //--Render the character's stats. This is in the top left.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    void *rSlimeTFPtr = rPlayerCharacter->GetDataList()->FetchDataEntry("iSlimeTFTurns");
    if(rPlayerCharacter)
    {
        //--Setup.
        StarlightColor::ClearMixer();

        //--Get the stats.
        CombatStats tPlayerStats = rPlayerCharacter->GetCombatStatistics();

        //--Header.
        cTextColor.SetAsMixer();
        float tCursor = CharPane.cIndent;
        Images.Data.rUIFontSmall->DrawText(CharPane.cIndent, tCursor, 0, cFontSize, " Your Stats:");
        tCursor = tCursor + cFontSpacing;

        //--Health/Will/Stamina.
        Images.Data.rUIFontSmall->DrawTextArgs(CharPane.cIndent, tCursor, 0, cFontSize, "  Health: %i/%i", tPlayerStats.mHP, tPlayerStats.mHPMax);
        tCursor = tCursor + cFontSpacing;
        Images.Data.rUIFontSmall->DrawTextArgs(CharPane.cIndent, tCursor, 0, cFontSize, "  Willpower: %i/%i", tPlayerStats.mWillPower, tPlayerStats.mWillPowerMax);
        tCursor = tCursor + cFontSpacing;
        Images.Data.rUIFontSmall->DrawTextArgs(CharPane.cIndent, tCursor, 0, cFontSize, "  Stamina: %i/%i", tPlayerStats.mStamina, tPlayerStats.mStaminaMax);
        tCursor = tCursor + cFontSpacing;

        //--Special stuff.
        if(rPlayerCharacter->IsStunned())
        {
            Images.Data.rUIFontSmall->DrawTextArgs(CharPane.cIndent, tCursor, 0, cFontSize, "  Stunned! (%i)", rPlayerCharacter->GetTurnsToRecovery());
            tCursor = tCursor + cFontSpacing;
        }
        //--If confounded...
        else if(tPlayerStats.mWillPower < 1)
        {
            Images.Data.rUIFontSmall->DrawTextArgs(CharPane.cIndent, tCursor, 0, cFontSize, "  Confounded!");
            tCursor = tCursor + cFontSpacing;
        }

        //--Slime infection!
        if(rSlimeTFPtr && rPlayerCharacter->GetTeam() == TEAM_PLAYER)
        {
            //--Cast.
            float *rSlimeTFTurnsPtr = (float *)rSlimeTFPtr;
            int tValue = (int)(*rSlimeTFTurnsPtr);
            if(tValue > 0)
            {
                Images.Data.rUIFontSmall->DrawTextArgs(CharPane.cIndent, tCursor, 0, cFontSize, "  Infected! (%i turns left)", 30 - tValue);
                tCursor = tCursor + cFontSpacing;
            }
        }
    }

    //--Render the game status.
    if(mStatusStrings)
    {
        //--Setup.
        float cFontSize = 1.0f;
        StarlightColor::ClearMixer();

        //--Setup.
        cTextColor.SetAsMixer();
        float cIndentL = CharPane.cWid - (CharPane.cIndent * 2.0f);
        float cIndentT = CharPane.cIndent;

        //--Loop.
        cTextColor.SetAsMixer();
        for(int i = 0; i < mStatusStringsTotal; i ++)
        {
            //--Skip blank lines.
            if(!mStatusStrings[i]) continue;

            //--Render.
            float tExpectedSize = Images.Data.rUIFontSmall->GetTextWidth(mStatusStrings[i]) * cFontSize;
            Images.Data.rUIFontSmall->DrawText(cIndentL - tExpectedSize, (i * (Images.Data.rUIFontSmall->GetTextHeight() * cFontSize)) + cIndentT, 0, cFontSize, mStatusStrings[i]);
        }
    }

    //--Render the character's inventory. This is always the player, not the character being
    //  examined! Use the EM's function for this.
    if(rPlayerCharacter)
    {
        //--Move the cursor up by one if we have a weapon.
        InventoryItem *rPlayerWeapon = rPlayerCharacter->GetWeapon();

        //--Grey underlay on selected item.
        glColor4f(0.25f, 0.25f, 0.25f, 1.0f);
        if(mSelectedInventorySlot != -1)
        {
            glDisable(GL_TEXTURE_2D);
            float tTop = CharPane.cHei + ((mInventoryMembers * cFontSpacing * -1.0f) + cInvOffset) + 15.0f + (cFontSpacing * 1.5f) + ((float)mSelectedInventorySlot * cFontSpacing) + 0.0f;
            float tBot = tTop + cFontSpacing;
            glBegin(GL_QUADS);
                glVertex2f(                CharPane.cIndent, tTop);
                glVertex2f(CharPane.cWid - CharPane.cIndent, tTop);
                glVertex2f(CharPane.cWid - CharPane.cIndent, tBot);
                glVertex2f(                CharPane.cIndent, tBot);
            glEnd();
            glEnable(GL_TEXTURE_2D);
        }

        //--Setup.
        float cFontSize = 1.0f;
        cTextColor.SetAsMixer();
        float tCursorY = cHei + ((mInventoryMembers * cFontSpacing * -1.0f) + cInvOffset) + 15.0f + (cFontSpacing * 0.5f);
        //if(rPlayerWeapon) tCursorY = tCursorY - cFontSpacing;

        //--Render the "Inventory" bit.
        Images.Data.rUIFontSmall->DrawText(CharPane.cIndent + 8.0f, tCursorY, 0, cFontSize, "Inventory");
        tCursorY = tCursorY + cFontSpacing;

        //--The 0th entry is the "Self-Menu" option. This always exists.
        mExtraInventorySlots = 1;

        //--Render.
        Images.Data.rUIFontSmall->DrawText(CharPane.cIndent + 16.0f, tCursorY, 0, cFontSize, "Open Self-Menu");
        tCursorY = tCursorY + cFontSpacing;

        //--The first entry is always the Weapon, if it exists. If not, skip it.
        mExtraIsWeapon = false;
        if(rPlayerWeapon)
        {
            //--Increment the offset counter.
            mExtraInventorySlots ++;
            mExtraIsWeapon = true;

            //--Mark this as the weapon.
            char tBuffer[64];
            sprintf(tBuffer, "(Weapon) %s", rPlayerWeapon->GetName());

            //--If unequipping a weapon also drops it, use this function.
            if(Actor::xAllowOneWeapon)
            {
                RenderCharacterPaneItemNoDrop(tBuffer, "Unequip", (1 == mSelectedInventorySlot), mIsUseSelected, mIsDropSelected, tCursorY);
            }
            //--Otherwise, show both unequip and drop.
            else
            {
                RenderCharacterPaneItem(tBuffer, "Unequip", (1 == mSelectedInventorySlot), mIsUseSelected, mIsDropSelected, tCursorY);
            }
        }

        //--Second is Armor, if it exists.
        InventoryItem *rPlayerArmor = rPlayerCharacter->GetArmor();
        if(rPlayerArmor)
        {
            //--Increment the offset counter.
            mExtraInventorySlots ++;

            //--Mark this as the weapon.
            char tBuffer[64];
            sprintf(tBuffer, "(Armor) %s", rPlayerArmor->GetName());

            //--If unequipping the armor also drops it, use this function.
            if(Actor::xAllowOneArmor)
            {
                RenderCharacterPaneItemNoDrop(tBuffer, "Unequip", (2 == mSelectedInventorySlot), mIsUseSelected, mIsDropSelected, tCursorY);
            }
            //--Otherwise, show both unequip and drop.
            else
            {
                RenderCharacterPaneItem(tBuffer, "Unequip", (2 == mSelectedInventorySlot), mIsUseSelected, mIsDropSelected, tCursorY);
            }
        }

        //--Get their local inventory size.
        RenderInventory(tCursorY);
    }

    //--Final clean up.
    glTranslatef(-CharPane.cLft, -CharPane.cTop, 0.0f);
    StarlightColor::ClearMixer();
}
void PandemoniumLevel::RenderCharacterPaneItem(const char *pName, const char *pUseString, bool pIsSelected, bool pHighlightUse, bool pHighlightDrop, float &sCursor)
{
    //--Renders an item to the character pane, with the attendant Use/Equip/Drop if it's highlighted.
    if(!pName || !pUseString) return;

    //--Render its name.
    float cSize = 1.0f;
    glColor3f(1.0f, 1.0f, 1.0f);
    Images.Data.rUIFontSmall->DrawText(CharPane.cIndent + 12.0f, sCursor, 0, cSize, pName);

    //--If this is the selected item, then render "Use" or "Equip".
    if(pIsSelected)
    {
        //--Button Position
        float tRenderX = CharPane.cWid - CharPane.cIndent - 115.0f;

        //--Render.
        glColor3f(1.0f, 1.0f, 1.0f);
        if(pHighlightUse) glColor3f(1.0f, 0.0f, 0.0f);
        Images.Data.rUIFontSmall->DrawText(tRenderX, sCursor, 0, cSize, pUseString);

        //--Also render "Drop". This is for all items, and is not variable.
        glColor3f(1.0f, 1.0f, 1.0f);
        if(pHighlightDrop) glColor3f(1.0f, 0.0f, 0.0f);
        tRenderX = CharPane.cWid - CharPane.cIndent - 60.0f;
        Images.Data.rUIFontSmall->DrawText(tRenderX, sCursor, 0, cSize, "Drop");
    }

    //--Move the cursor down.
    sCursor = sCursor + cFontSpacing;
}
void PandemoniumLevel::RenderCharacterPaneItemNoDrop(const char *pName, const char *pUseString, bool pIsSelected, bool pHighlightUse, bool pHighlightDrop, float &sCursor)
{
    //--Identical to the above function, but does not render "Drop". This is used when unequipping
    //  the item in question is the same as dropping it.
    if(!pName || !pUseString) return;

    //--Render its name.
    float cSize = 1.0f;
    glColor3f(1.0f, 1.0f, 1.0f);
    Images.Data.rUIFontSmall->DrawText(CharPane.cIndent + 12.0f, sCursor, 0, cSize, pName);

    //--If this is the selected item, then render "Use" or "Equip".
    if(pIsSelected)
    {
        //--Button Position
        float tRenderX = CharPane.cWid - CharPane.cIndent - 115.0f;

        //--Render.
        if(pHighlightUse) glColor3f(1.0f, 0.0f, 0.0f);
        Images.Data.rUIFontSmall->DrawText(tRenderX, sCursor, 0, cSize, pUseString);
    }

    //--Move the cursor down.
    sCursor = sCursor + cFontSpacing;
}
