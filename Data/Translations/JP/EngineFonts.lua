-- |[ ======================================= Engine Fonts ====================================== ]|
--Boots all the fonts used by the core engine. This is called by the C++ program immediately after
-- Freetype is initialized, and is a replacement for the base call.
    
-- |[Setup]|
--Debug timer.
local fStartTime = 0.0
LM_StartTimer("Fonts")

--Paths.
local sFontPath      = "Data/Scripts/Fonts/"
local sKerningPath   = "Data/Scripts/Fonts/"
local sJPKerningPath = fnResolvePath()
local sLocalPath     = fnResolvePath()
local sHasNoKerningFile = "Null"

-- |[ ==================================== Font Prototyping ==================================== ]|
--Create prototypes for the fonts here. They don't load if the player isn't actually playing WHI
-- via its own title screen, but can be loaded later.

-- |[Pretendard JP 80 DF]|
local zPretendardJP40DFO = FontProto:new("PretendardJP 40 DFO", sLocalPath .. "PretendardJP-Regular.otf", 40, gciFontEdge + gciFontDownfade)
zPretendardJP40DFO:fnSetDownfades(0.90, 0.60)
zPretendardJP40DFO:fnSetOutline(2)
zPretendardJP40DFO:fnSetMonospacing(40)
zPretendardJP40DFO:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 40 DFO.slf")

-- |[Pretendard JP 60]|
local zPretendardJP60 = FontProto:new("PretendardJP 60", sLocalPath .. "PretendardJP-Regular.otf", 60, gciFontNoFlags)
zPretendardJP60:fnSetMonospacing(55)
zPretendardJP60:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 60.slf")

-- |[Pretendard JP 36]|
local zPretendardJP36 = FontProto:new("PretendardJP 36", sLocalPath .. "PretendardJP-Regular.otf", 36, gciFontNoFlags)
zPretendardJP36:fnSetMonospacing(35)
zPretendardJP36:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 36.slf")

-- |[Pretendard JP 42]|
local zPretendardJP42 = FontProto:new("PretendardJP 42", sLocalPath .. "PretendardJP-Regular.otf", 42, gciFontNoFlags)
zPretendardJP42:fnSetMonospacing(42)
zPretendardJP42:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 42.slf")

-- |[Pretendard JP 30]|
local zPretendardJP30 = FontProto:new("PretendardJP 30", sLocalPath .. "PretendardJP-Regular.otf", 30, gciFontNoFlags)
zPretendardJP30:fnSetMonospacing(30)
zPretendardJP30:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 30.slf")

-- |[Pretendard JP 20]|
local zPretendardJP20 = FontProto:new("PretendardJP 20", sLocalPath .. "PretendardJP-Regular.otf", 20, gciFontNoFlags)
zPretendardJP20:fnSetMonospacing(20)
zPretendardJP20:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 20.slf")

-- |[ ==================================== Font Registration =================================== ]|
--Note: For speed, these two fonts are intentionally set to null paths. Causes the title screen to load a lot faster.

-- |[PretendardJP 36 DFO]|
--local zPretendardJP36DFO = FontProto:new("PretendardJP 36 DFO", sLocalPath .. "PretendardJP-Regular.otf", 36, gciFontEdge + gciFontDownfade)
local zPretendardJP36DFO = FontProto:new("PretendardJP 36 DFO", "", 36, gciFontEdge + gciFontDownfade)
zPretendardJP36DFO:fnSetDownfades(0.90, 0.60)
zPretendardJP36DFO:fnSetOutline(3)
zPretendardJP36DFO:fnSetMonospacing(30)
zPretendardJP36DFO:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 36 DFO.slf")
zPretendardJP36DFO:fnLoad(gcbNoFontCompiling)

-- |[PretendardJP 20 DFO]|
--local zPretendardJP20DFO = FontProto:new("PretendardJP 20 DFO", sLocalPath .. "PretendardJP-Regular.otf", 20, gciFontEdge + gciFontDownfade)
local zPretendardJP20DFO = FontProto:new("PretendardJP 20 DFO", "", 20, gciFontEdge + gciFontDownfade)
zPretendardJP20DFO:fnSetDownfades(0.90, 0.60)
zPretendardJP20DFO:fnSetOutline(1)
zPretendardJP20DFO:fnSetMonospacing(20)
zPretendardJP20DFO:fnSetPrecompilePath("Data/FontPrebuild/PretendardJP 20 DFO.slf")
zPretendardJP20DFO:fnLoad(gcbNoFontCompiling)

-- |[Set Common Aliases]|
--PretendardJP 36 DFO
Font_SetProperty("Add Alias", "PretendardJP 36 DFO", "String Entry Header")

--PretendardJP 20 DFO
Font_SetProperty("Add Alias", "PretendardJP 20 DFO", "String Entry Main")

-- |[ ==================== Game-Specific Title Screens ===================== ]|
--Playing Witch Hunter Izana: Load these fonts for the title screen.
if(gsMandateTitle == "Witch Hunter Izana") then

    -- |[Order Fonts to Load]|
    --If the player is using the WHI title screen, we need these fonts right now.
    zPretendardJP40DFO:fnLoad()
    zPretendardJP60:fnLoad()
    zPretendardJP36:fnLoad()
    zPretendardJP30:fnLoad()
    zPretendardJP20:fnLoad()
    
    --Note: PretendardJP42 doesn't load here.

    -- |[Set Aliases]|
    Font_SetProperty("Add Alias", "PretendardJP 40 DFO", "Abyss Combat Text")
    
    Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Title Double Header")
    Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Option Double Header")
    Font_SetProperty("Add Alias", "PretendardJP 60", "Mono String Entry Header")
    
    Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Title Option")
    Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Option Header")
    Font_SetProperty("Add Alias", "PretendardJP 36", "Mono String Entry Current")
    
    Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Option Help")
    Font_SetProperty("Add Alias", "PretendardJP 30", "Mono String Entry Mainline")
    
    Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Standard Control")
    Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Title Statistic")
    Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Option Mainline")
end

-- |[ ===================================== System Aliases ====================================== ]|
--The fonts "System Font", "Bitmap Font", and "Bitmap Font Small" are system fonts guaranteed to exist.
-- We can register aliases for them here.

--System Font
Font_SetProperty("Add Alias", "System Font", "Virtual Console Main")

--Bitmap Font
Font_SetProperty("Add Alias", "Bitmap Font", "Adventure Debug Font")
Font_SetProperty("Add Alias", "Bitmap Font", "Corrupter Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Flex Button Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Flex Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Gallery Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Classic Level Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Classic Level Damage")
Font_SetProperty("Add Alias", "Bitmap Font", "Zone Editor Main")

--Bitmap Font Small
Font_SetProperty("Add Alias", "Bitmap Font Small", "Corrupter Menu Small")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Blue Sphere Main")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Context Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Classic Level Small")

-- |[ ==================================== End Compilation ===================================== ]|
--Timer diagnostics.
local fEndTime = LM_CheckTimer("Fonts")
local bDiagnostics = Debug_GetFlag("Fonts: Timing")
if(bDiagnostics == true) then
    io.write(string.format("%6.4f: Engine Font Loading\n", fEndTime - fStartTime))
end

--Clear flags.
LM_FinishTimer("Fonts")
