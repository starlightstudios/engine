-- |[ ================================ SysConstants Directions ================================ ]|
--Functions related to directional constants, such as inverting them, or changing from string
-- to integers.

-- |[ =============================== Translation Between Types =============================== ]|
-- |[SysConstants:fnGetStringFromDirCode()]|
--Given an integer constant, returns a string constant representing the direction.
function SysConstants:fnGetStringFromDirCode(piCode)
    
    -- |[Argument Check]|
    if(piCode == nil) then return "S" end
    
    -- |[Execution]|
    if(piCode == gci_Face_North)     then return "N" end
    if(piCode == gci_Face_NorthEast) then return "NE" end
    if(piCode == gci_Face_East)      then return "E" end
    if(piCode == gci_Face_SouthEast) then return "SE" end
    if(piCode == gci_Face_South)     then return "S" end
    if(piCode == gci_Face_SouthWest) then return "SW" end
    if(piCode == gci_Face_West)      then return "W" end
    if(piCode == gci_Face_NorthWest) then return "NW" end
    
    --Error case:
    io.write("SysConstants:fnGetStringFromDirCode() - Error. No string equivalent for integer " .. piCode .. " found.\n")
    return gci_Face_South
end

-- |[SysConstants:fnGetDirCodeFromString()]|
--Given a string, returns an integer constant representing the same direction.
function SysConstants:fnGetDirCodeFromString(psString)
    
    -- |[Argument Check]|
    if(psString == nil) then return gci_Face_South end
    
    -- |[Execution]|
    if(psString == "N")  then return gci_Face_North     end
    if(psString == "NE") then return gci_Face_NorthEast end
    if(psString == "E")  then return gci_Face_East      end
    if(psString == "SE") then return gci_Face_SouthEast end
    if(psString == "S")  then return gci_Face_South     end
    if(psString == "SW") then return gci_Face_SouthWest end
    if(psString == "W")  then return gci_Face_West      end
    if(psString == "NW") then return gci_Face_NorthWest end
    
    --Error case:
    io.write("SysConstants:fnGetDirCodeFromString() - Error. No integer equivalent for string \"" .. psString .. "\" found.\n")
    return gci_Face_South
end

-- |[ =========================== Reversal Translation Between Types =========================== ]|
-- |[SysConstants:fnGetRevStringFromDirCode()]|
--Given an integer constant, returns a string constant representing the opposite direction.
function SysConstants:fnGetRevStringFromDirCode(piCode)
    
    -- |[Argument Check]|
    if(piCode == nil) then return "S" end
    
    -- |[Execution]|
    if(piCode == gci_Face_North)     then return "S" end
    if(piCode == gci_Face_NorthEast) then return "SW" end
    if(piCode == gci_Face_East)      then return "W" end
    if(piCode == gci_Face_SouthEast) then return "NW" end
    if(piCode == gci_Face_South)     then return "N" end
    if(piCode == gci_Face_SouthWest) then return "NE" end
    if(piCode == gci_Face_West)      then return "E" end
    if(piCode == gci_Face_NorthWest) then return "SE" end
    
    --Error case:
    io.write("SysConstants:fnGetRevStringFromDirCode() - Error. No string equivalent for integer " .. piCode .. " found.\n")
    return gci_Face_South
end

-- |[SysConstants:fnGetRevDirCodeFromString()]|
--Given a string, returns an integer constant representing the opposite direction.
function SysConstants:fnGetRevDirCodeFromString(psString)
    
    -- |[Argument Check]|
    if(psString == nil) then return gci_Face_South end
    
    -- |[Execution]|
    if(psString == "S")  then return gci_Face_North     end
    if(psString == "SW") then return gci_Face_NorthEast end
    if(psString == "W")  then return gci_Face_East      end
    if(psString == "NW") then return gci_Face_SouthEast end
    if(psString == "N")  then return gci_Face_South     end
    if(psString == "NE") then return gci_Face_SouthWest end
    if(psString == "E")  then return gci_Face_West      end
    if(psString == "SE") then return gci_Face_NorthWest end
    
    --Error case:
    io.write("SysConstants:fnGetRevDirCodeFromString() - Error. No integer equivalent for string \"" .. psString .. "\" found.\n")
    return gci_Face_South
end

-- |[ ========================= Reversal Translation Between Same Type ========================= ]|
-- |[SysConstants:fnGetReverseDirFromCode()]|
function SysConstants:fnGetReverseDirFromCode(piCode)
    
    -- |[Argument Check]|
    if(piCode == nil) then return gci_Face_South end
    
    if(piCode == gci_Face_North)     then return gci_Face_South end
    if(piCode == gci_Face_NorthEast) then return gci_Face_SouthWest end
    if(piCode == gci_Face_East)      then return gci_Face_West end
    if(piCode == gci_Face_SouthEast) then return gci_Face_NorthWest end
    if(piCode == gci_Face_South)     then return gci_Face_North end
    if(piCode == gci_Face_SouthWest) then return gci_Face_NorthEast end
    if(piCode == gci_Face_West)      then return gci_Face_East end
    if(piCode == gci_Face_NorthWest) then return gci_Face_SouthEast end
    
    --Error case:
    io.write("SysConstants:fnGetReverseDirFromCode() - Error. No direction reverse for code " .. piCode .. " found.\n")
    return gci_Face_South
end

-- |[SysConstants:fnGetReverseDirFromString()]|
function SysConstants:fnGetReverseDirFromString(psString)
    
    -- |[Argument Check]|
    if(psString == nil) then return "S" end
    
    -- |[Execution]|
    if(psString == "S")  then return "N"  end
    if(psString == "SW") then return "NE" end
    if(psString == "W")  then return "E"  end
    if(psString == "NW") then return "SE" end
    if(psString == "N")  then return "S"  end
    if(psString == "NE") then return "SW" end
    if(psString == "E")  then return "W"  end
    if(psString == "SE") then return "NW" end
    
    --Error case:
    io.write("SysConstants:fnGetReverseDirFromString() - Error. No direction reverse for code " .. piCode .. " found.\n")
    return "S"
end