//--Base
#include "StarBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

///======================================= Documentation ==========================================
//--Subroutines dedicated to processing bitmaps loaded off the hard drive.  Some of these sections
//  are specific to the graphics library being used.

///========================================= Functions ============================================
uint32_t StarBitmap::ProcessBitmap(const char *pPath)
{
    ///--[Documentation]
    //--Given a position on the hard drive, loads in a bitmap and strips it, then uploads it.
    //  The resulting texture handle is passed back.
    //--Returns 0 on error.
    if(!pPath) return 0;

    ///--[Execution]
    //--Setup.
    int tWidth, tHeight;

    //--This will do the load/strip work.
    uint8_t *tArray = StarBitmap::LoadBitmap(pPath, tWidth, tHeight);
    if(!tArray) return 0;

    //--Upload the data to the graphics card.
    uint32_t tHandle = StarBitmap::UploadData(tArray, tWidth, tHeight);

    //--Clean up.
    free(tArray);
    return tHandle;
}
uint8_t *StarBitmap::LoadBitmap(const char *pPath, int &sWidth, int &sHeight)
{
    ///--[Documentation]
    //--Given a position on the hard drive, loads in the requested bitmap.  Passes back an array
    //  of its RGBA data in 32 bit format, to be passed to the GL Uploading function. Returns NULL
    //  on failure.
    //--You are required to free() the array when done with it.
    if(!pPath) return NULL;

    ///--[Allegro Code]
    #if defined _ALLEGRO_PROJECT_

        //--Set flags such that the bitmap is a memory bitmap.  This skips the step
        //  of uploading it and allows us to quickly manipulate it in memory.
        al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE);
        al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP | ALLEGRO_MIN_LINEAR | ALLEGRO_NO_PREMULTIPLIED_ALPHA);

        //--Actually load the bitmap.
        ALLEGRO_BITMAP *tBitmap = al_load_bitmap(pPath);
        if(!tBitmap) return NULL;

        //--Get the X and Y Sizes.
        sWidth = al_get_bitmap_width(tBitmap);
        sHeight = al_get_bitmap_height(tBitmap);

        //--Run the Strip subroutine.
        uint8_t *nArray = StarBitmap::StripBitmap(tBitmap);

        //--Destroy the bitmap.
        al_destroy_bitmap(tBitmap);

        //--Return it.  If StripBitmap failed, this is NULL anyway.
        return nArray;

    ///--[SDL Code]
    #elif defined _SDL_PROJECT_

        return NULL;

        //--Load the bitmap as an SDL_Surface.
        SDL_Surface *tBitmapSurface = IMG_Load(pPath);
        if(!tBitmapSurface)
        {
            return NULL;
        }

        //--Flags.
        SDL_SetSurfaceRLE(tBitmapSurface, 0);

        //--Get the sizes.
        sWidth = tBitmapSurface->w;
        sHeight = tBitmapSurface->h;

        //--Copy the pixel data.
        SetMemoryData(__FILE__, __LINE__);
        uint8_t *nPixelData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * sWidth * sHeight * sizeof(uint32_t));
        memcpy(nPixelData, tBitmapSurface->pixels, sizeof(uint8_t) * sWidth * sHeight * sizeof(uint32_t));

        //--Wipe the surface.
        SDL_FreeSurface(tBitmapSurface);

        //--Return the pixel data.
        return nPixelData;

    #endif
}
#if defined _ALLEGRO_PROJECT_
uint8_t *StarBitmap::StripBitmap(ALLEGRO_BITMAP *pBitmap)
{
    ///--[Documentation]
    //--Allegro projects only. SDL does not have an equivalent.
    //--Takes in an ALLEGRO_BITMAP, and strips off its RGBA data.  Passes this back as an array that
    //  must be freed.  Doesn't destroy the bitmap.
    //--Returns NULL on failure.
    if(!pBitmap) return NULL;

    ///--[Execution]
    //--Needs local width and height.
    int tWidth = al_get_bitmap_width(pBitmap);
    int tHeight = al_get_bitmap_height(pBitmap);

    //--Lock it.  This allows us to get its array.
    ALLEGRO_LOCKED_REGION *tRegion = al_lock_bitmap(pBitmap, ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE, ALLEGRO_LOCK_READONLY);

    //--Allocate an array of the same size as the bitmap.  Note the use of the pitch instead of
    //  the width!
    int tPitch = tRegion->pitch;
    int tBytes = tRegion->pixel_size;
    if(tPitch < 0) tPitch = tPitch * -1;
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *nArray = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tPitch * tHeight);

    //--Casting.
    uint32_t tLineStart = 0;
    uint8_t *rData = (uint8_t *)tRegion->data;

    //--Copy the data into the array.  Pads the edges if necessary.
    uint32_t tArrayCursor = 0;
    for(int y = 0; y < tHeight; y ++)
    {
        //--Reset the data cursor.
        tLineStart = tPitch * y;

        //--Copy all the pixel data.
        for(int x = 0; x < tWidth * tBytes; x ++)
        {
            nArray[tArrayCursor] = rData[tLineStart + x];
            tArrayCursor ++;
        }

        //--Pad the edges.
        for(int x = tWidth * tBytes; x < tPitch; x ++)
        {
            nArray[tArrayCursor] = 0;
            tArrayCursor ++;
        }
    }

    //--Unlock the bitmap before passing it back to prevent crashing.
    al_unlock_bitmap(pBitmap);

    //--Pass back the array.
    return nArray;
}
#endif
