//--Base
#include "CarnUIStatus.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "CarnationCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
CarnUIStatus::CarnUIStatus()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CSTA");

    ///--[Visiblity]
    ///--[Help Menu]

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== CarnUIStatus ======= ]
    ///--[System]
    ///--[Character Storage]
    memset(&mClassData, 0, sizeof(CarnationUIClass) * CARNUI_CLASS_TOTAL);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    ///--[Hitboxes]
    RegisterHitboxWH("Forward",  203.0f, 590.0f, 66.0f, 61.0f);
    RegisterHitboxWH("Backward", 203.0f, 653.0f, 66.0f, 61.0f);
}
CarnUIStatus::~CarnUIStatus()
{
}
void CarnUIStatus::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation Status UI",           &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI",           &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Status UI Carnation", &AdvImages,        sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",             &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CarnUIStatus::TakeForeground()
{
    ///--[Documentation]
    //--Change the character cursor to 0 by calling a subroutine which will resolve the display data.
    mCharacterCursor = -1;
    ChangeCharacterCursor(xPersistentCharacterCursor);
}

///======================================= Core Methods ===========================================
void CarnUIStatus::RefreshMenuHelp()
{
}
void CarnUIStatus::RecomputeCursorPositions()
{
}
void CarnUIStatus::ChangeCharacterCursor(int pNewValue)
{
    ///--[Documentation]
    //--If the character cursor changes, then refreshes properties for display. This calls once and
    //  stores the values internally rather than repeatedly querying the scripts to display values.
    if(pNewValue == mCharacterCursor) return;

    //--Error check, the character must exist.
    AdvCombat *rCombat = AdvCombat::Fetch();
    if(!rCombat) return;

    //--Character.
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(pNewValue);
    if(!rAdvEntity) return;

    //--Get character name.
    const char *rCharacterName = rAdvEntity->GetName();

    ///--[Basics]
    //--Set cursor.
    mCharacterCursor = pNewValue;

    ///--[Script Queries]
    //--Run into class structures.
    QueryClassDataInto(0, rCharacterName, mClassData[0]);
    QueryClassDataInto(1, rCharacterName, mClassData[1]);
    QueryClassDataInto(2, rCharacterName, mClassData[2]);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIStatus::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ========== Documentation =========== ]
    //--Updates stats menu mode. Mostly just allows switching characters and modes.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--By default, the visiblity timer runs to its max and then stops.
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, true);

    ///--[ ========== Mouse Handling ========== ]
    //--Retrieve the mouse coordinates. Subroutines can then use them freely.
    GetMouseInfo();

    ///--[ ========== Click Handling ========== ]
    //--Player left-clicks.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        HandleLeftClick();
    }

    //--Player right-clicks.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        FlagExit();
    }

    ///--[ ======= No Controls Handled ======== ]
    return true;
}
void CarnUIStatus::UpdateBackground()
{
    ///--[Timers]
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
}
void CarnUIStatus::HandleLeftClick()
{
    ///--[Documentation]
    //--Does the bulk of the work of a left click. Returns out when it hits anything.
    int tNewCharacterCursor = mCharacterCursor;

    //--Common functions. If a tab was clicked, switch modes.
    if(CommonTabClickUpdate()) return;
    if(CommonCharClickUpdate(tNewCharacterCursor))
    {
        ChangeCharacterCursor(tNewCharacterCursor);
        return;
    }

    ///--[Party Sorting]
    //--Mouse info.
    GetMouseInfo();
    RunHitboxCheck();
    int tReply = CheckRepliesWithin(cxHitbox_OrderForward, cxHitbox_OrderBackward);

    //--Character will move forward in the active party order.
    if(tReply == cxHitbox_OrderForward)
    {
        //--If the character is in slot 0, do nothing.
        StarLinkedList *rActivePartyList = AdvCombat::Fetch()->GetActivePartyList();
        if(!rActivePartyList) return;
        if(mCharacterCursor == 0) return;

        //--Move up by a slot, then decrement the character cursor.
        rActivePartyList->MoveEntryTowardsHeadI(mCharacterCursor);
        mCharacterCursor --;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Character will move backward in the active party order.
    else if(tReply == cxHitbox_OrderBackward)
    {
        //--If the character is already at the rear of the party, do nothing.
        StarLinkedList *rActivePartyList = AdvCombat::Fetch()->GetActivePartyList();
        if(!rActivePartyList) return;

        //--Get size, check for the end.
        int tActivePartySize = rActivePartyList->GetListSize();
        if(mCharacterCursor >= tActivePartySize) return;

        //--Move down by a slot, then increment the character cursor.
        rActivePartyList->MoveEntryTowardsTailI(mCharacterCursor);
        mCharacterCursor ++;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIStatus::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the status display for the selected character. If the party is empty, renders the
    //  backing but no character properties.
    //--Does nothing if the images didn't resolve.
    if(pVisAlpha <= 0.0f) return;

    ///--[Character Select]
    //--Shows the current party members.
    AdvCombat *rCombat = AdvCombat::Fetch();
    if(!rCombat) return;

    //--Tabs.
    RenderCharacterTabs(mCharacterCursor);

    ///--[Static Backing]
    CarnImages.rFrame_Base->Draw();
    CarnImages.rOverlay_StatDetail->Draw();

    //--Common.
    RenderTopTabs(mCodeBackward);
    RenderHexes();

    ///--[Character Resolve]
    //--Get the combat character under the cursor. If it doesn't exist, stop.

    //--Character.
    StarLinkedList *rActivePartyList = AdvCombat::Fetch()->GetActivePartyList();
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity || !rActivePartyList) return;

    ///--[Portrait]
    if(rAdvEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
    {
        CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rAdvEntity;
        rCarnationEntity->RenderSwitchableAt(196, 62);
    }

    ///--[Name/Stats]
    //--Name.
    CarnImages.rFont_Header->DrawTextArgs(445.0f, 68.0f, 0, 1.0f, "%s", rAdvEntity->GetDisplayName());

    //--Level.
    CarnImages.rFont_Mainline->DrawTextArgs(658.0f, 83.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Lv. %i", rAdvEntity->GetLevel()+1);

    //--EXP/Needed.
    CarnImages.rFont_Mainline->DrawTextArgs(445.0f, 115.0f, 0, 1.0f, "XP: %i / %i", rAdvEntity->GetXP(), rAdvEntity->GetXP() + rAdvEntity->GetXPToNextLevel());

    //--Class information.
    float tYPos = 334.0f;
    float cYHei =  60.0f;
    //for(int i = 0; i < CARNUI_CLASS_TOTAL; i ++) --Disabled for the prototype, don't render a locked class slot :3
    for(int i = 0; i < 2; i ++)
    {
        //--Empty class:
        if(mClassData[i].mName[0] == '\0' || !strcasecmp(mClassData[i].mName, "Null"))
        {
            CarnImages.rFont_Mainline->DrawTextArgs(220.0f, tYPos, 0, 1.0f, "(Empty Class Slot)");
            tYPos = tYPos + cYHei;
            continue;
        }

        //--Normal class information.
        CarnImages.rFont_Mainline->DrawTextArgs(220.0f, tYPos, 0, 1.0f, "%s", mClassData[i].mName);
        CarnImages.rFont_Mainline->DrawTextArgs(433.0f, tYPos, SUGARFONT_RIGHTALIGN_X, 1.0f, "Lv: %i", mClassData[i].mClassLevel+1);
        if(mClassData[i].mClassXP < mClassData[i].mClassXPNext)
        {
            CarnImages.rFont_Mainline->DrawTextArgs(220.0f, tYPos + 23.0f, 0, 1.0f, "Class XP: %i / %i", mClassData[i].mClassXP, mClassData[i].mClassXPNext);
        }
        else
        {
            CarnImages.rFont_Mainline->DrawTextArgs(220.0f, tYPos + 23.0f, 0, 1.0f, "Class XP: Maxed");
        }

        //--Next.
        tYPos = tYPos + cYHei;
    }

    ///--[HP/SP Meters]
    //--Render static components.
    CarnImages.rOverlay_MeterFrames->Draw();

    //--HP Values.
    int tHPCur = rAdvEntity->GetHealth();
    int tHPMax = rAdvEntity->GetHealthMax();
    float tHPPct = rAdvEntity->GetHealthPercent();
    CarnImages.rFont_Statistic->DrawTextArgs(480.0f, 159.0f, 0, 1.0f, "%i / %i", tHPCur, tHPMax);
    CarnImages.rOverlay_HPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tHPPct, 1.0f);

    //--SP Values.
    int tSPCur = rAdvEntity->GetMagic();
    int tSPMax = rAdvEntity->GetStatistic(STATS_MPMAX);
    float tSPPct = rAdvEntity->GetMagicPercent();
    CarnImages.rFont_Statistic->DrawTextArgs(480.0f, 220.0f, 0, 1.0f, "%i / %i", tSPCur, tSPMax);
    CarnImages.rOverlay_SPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tSPPct, 1.0f);

    //--HP/SP.
    //CarnImages.rOverlay_Meters->Draw();

    ///--[Influence]
    //--Renders below status.
    CarnImages.rFont_Statistic->DrawTextArgs(445.0f, 285.0f, 0, 1.0f, "Influence: %i / %i", rAdvEntity->GetInfluence(), rAdvEntity->GetInfluenceMax());

    ///--[Statistics]
    //--Fixed labels.
    CarnImages.rFont_Header->DrawText(935.0f, 134.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Statistics");

    //--Constants.
    float cLft =  787.0f;
    float cRgt = 1088.0f;
    float cFlagL = 0;
    float cFlagR = SUGARFONT_RIGHTALIGN_X;
    float cTop = 200.0f;
    float cHei = 39.0f;

    //--Variables.
    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 0.0f), cFlagL, 1.0f, "HP");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 0.0f), cFlagR, 1.0f, "%i", rAdvEntity->GetHealthMax());

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 1.0f), cFlagL, 1.0f, "SP");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 1.0f), cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 1));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 2.0f), cFlagL, 1.0f, "Attack");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 2.0f), cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 6));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 3.0f), cFlagL, 1.0f, "M. Atk.");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 3.0f), cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 3));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 4.0f)-3, cFlagL, 1.0f, "Defense");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 4.0f)-3, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 10));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 5.0f)-3, cFlagL, 1.0f, "M. Def.");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 5.0f)-3, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 8));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 6.0f)-3, cFlagL, 1.0f, "Initiative");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 6.0f)-3, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 7));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 7.0f)-4, cFlagL, 1.0f, "Strength");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 7.0f)-4, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 18));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 8.0f)-5, cFlagL, 1.0f, "Intellect");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 8.0f)-5, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 5));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei * 9.0f)-6, cFlagL, 1.0f, "Endurance");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei * 9.0f)-6, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 4));

    CarnImages.rFont_Statistic->DrawText    (cLft, cTop + (cHei *10.0f)-5, cFlagL, 1.0f, "Luck");
    CarnImages.rFont_Statistic->DrawTextArgs(cRgt, cTop + (cHei *10.0f)-5, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 9));

    ///--[Reordering Buttons]
    //--Setup.
    bool tShowReorderText = false;

    //--If the character is not in slot 0:
    if(mCharacterCursor > 0)
    {
        tShowReorderText = true;
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Forward");
        if(rHitbox) CarnImages.rFrame_BtnForward->Draw(rHitbox->mLft, rHitbox->mTop);
    }
    //--If the character is not in the rearmost slot:
    if(mCharacterCursor < rActivePartyList->GetListSize()-1)
    {
        tShowReorderText = true;
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Backward");
        if(rHitbox) CarnImages.rFrame_BtnBackward->Draw(rHitbox->mLft, rHitbox->mTop);
    }

    //--Help text.
    if(tShowReorderText)
    {
        CarnImages.rFont_Statistic->DrawText(274.0f, 577.0f, 0, 1.0f, "These buttons will reorder the party.");
        CarnImages.rFont_Statistic->DrawText(274.0f, 607.0f, 0, 1.0f, "The character in front is the");
        CarnImages.rFont_Statistic->DrawText(274.0f, 637.0f, 0, 1.0f, "representative during cutscenes.");
        CarnImages.rFont_Statistic->DrawText(274.0f, 667.0f, 0, 1.0f, "This has no impact on combat.");
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
