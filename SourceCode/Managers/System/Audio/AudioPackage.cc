//--Base
#include "AudioPackage.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "AudioManager.h"
#include "DebugManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
AudioPackage::AudioPackage()
{
    ///--[AudioPackage]
    //--System
    mAudioPackageType = AUDIOPACKAGE_TYPE_BASE;
    mIsReady = false;
    mLocalVolume = 1.0f;
    mGlobalVolume = 1.0f;
    mAbstractSlot = -1;
    mIsFading = false;
    mPlaysWhenAtZeroVolume = false;

    //--Bass Only
    #if defined _BASS_AUDIO_
        mHandle = 0;
        mTempoHandle = 0;
    #endif

    //--FMOD Only
    #if defined _FMOD_AUDIO_
        mSound = NULL;
        rSoundToPlay = NULL;
        rChannel = NULL;
        mSubSoundsTotal = 0;
        mStartLoop = 0.0f;
        mEndLoop = 0.0f;
    #endif

    //--Sample
    mIsSample = false;

    //--Stream
    mIsStream = false;

    //--Extra Data
    mStartLoopTime = 0.0f;
    mEndLoopTime = 0.0f;
    mExtraData = NULL;
}
AudioPackage::~AudioPackage()
{
    free(mExtraData);
}
void AudioPackage::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((AudioPackage *)pPtr);
}

///===================================== Property Queries =========================================
uint32_t AudioPackage::GetAudioPackageType()
{
    return mAudioPackageType;
}
int AudioPackage::GetAbstractSlot()
{
    return mAbstractSlot;
}
float AudioPackage::GetDuration()
{
    //--Expects a derived use.
    return 0.0f;
}
bool AudioPackage::IsPlaying()
{
    DebugManager::ForcePrint("AudioPackage::IsPlaying() - Warning, function should not be used. Virtual override preferred.\n");
    return false;
}
float AudioPackage::GetVolume()
{
    return mLocalVolume;
}
float AudioPackage::GetPosition()
{
    DebugManager::ForcePrint("AudioPackage::GetPosition() - Warning, function should not be used. Virtual override preferred.\n");
    return 0.0f;
}
bool AudioPackage::IsLooping()
{
    if(mStartLoopTime == mEndLoopTime) return false;
    return true;
}
float AudioPackage::GetStartLoopTime()
{
    return mStartLoopTime;
}
float AudioPackage::GetEndLoopTime()
{
    return mEndLoopTime;
}
bool AudioPackage::PlaysWhenAtZeroVolume()
{
    return mPlaysWhenAtZeroVolume;
}

///======================================= Manipulators ===========================================
void AudioPackage::SetAbstractSlot(int pSlot)
{
    mAbstractSlot = pSlot;
}
void AudioPackage::Load(const char *pPath)
{
    DebugManager::ForcePrint("AudioPackage::Load() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::Play()
{
    DebugManager::ForcePrint("AudioPackage::Play() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::FadeIn(int pTicks)
{
    DebugManager::ForcePrint("AudioPackage::FadeIn() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::FadeOut(int pTicks)
{
    DebugManager::ForcePrint("AudioPackage::FadeOut() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::FadeOutToStop(int pTicks)
{
    DebugManager::ForcePrint("AudioPackage::FadeOutToStop() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::Pause()
{
    DebugManager::ForcePrint("AudioPackage::Pause() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::Stop()
{
    DebugManager::ForcePrint("AudioPackage::Stop() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::SetLoop(bool pIsLooping)
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_

    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        if(!pIsLooping)
        {
            FMOD_Channel_SetLoopCount(rChannel, 0);
        }
        else
        {
            FMOD_Channel_SetLoopCount(rChannel, -1);
        }
    #endif
}
void AudioPackage::SetVolume(float pVolume)
{
    DebugManager::ForcePrint("AudioPackage::SetVolume() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::SetGlobalVolume(float pVolume)
{
    DebugManager::ForcePrint("AudioPackage::SetGlobalVolume() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::SlideVolume(float pVolume, int pTicks)
{
    DebugManager::ForcePrint("AudioPackage::SlideVolume() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::SeekTo(float pSeconds)
{
    DebugManager::ForcePrint("AudioPackage::SeekTo() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::SetTempo(float pTempo)
{
    DebugManager::ForcePrint("AudioPackage::SetTempo() - Warning, function should not be used. Virtual override preferred.\n");
}
void AudioPackage::SetPlaysWhenAtZeroVolume(bool pFlag)
{
    mPlaysWhenAtZeroVolume = pFlag;
}

///======================================= Core Methods ===========================================
void AudioPackage::AddLoop(float pStartLoop, float pEndLoop)
{
    DebugManager::ForcePrint("AudioPackage::AddLoop() - Warning, function should not be used. Virtual override preferred.\n");
}
float AudioPackage::ResolveAbstractVolume()
{
    //--Resolves the abstract volume as a factor from 0.0f to 1.0f. If no abstract index is set, always
    //  returns 1.0f. This can be multiplied into local/global volume settings.
    if(mAbstractSlot == -1) return 1.0f;

    //--Setup. Resolve the option name.
    char tString[32];
    sprintf(tString, "AbstractVolume%02i", mAbstractSlot);

    //--Return the requested value.
    return (OptionsManager::Fetch()->GetOptionI(tString) / 100.0f);
}

///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void AudioPackage::HookToLuaState(lua_State *pLuaState)
{
    /* AudioPackage_ActivateLoop(fStartLoop, fEndLoop)
       The last registered package gains looping properties.  Values are in seconds. */
    lua_register(pLuaState, "AudioPackage_ActivateLoop", &Hook_AudioPackage_ActivateLoop);

    /* AudioPackage_SetLocalVolume(fVolume)
       Sets the volume for this package, overriding the AudioManagers.  Auto-clamps 0 to 1. */
    lua_register(pLuaState, "AudioPackage_SetLocalVolume", &Hook_AudioPackage_SetLocalVolume);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
//--Audio packages do not print an error if they don't exist. This is typically because the audio
//  manager detected a re-registered audio package and nulled the pointer instead.
int Hook_AudioPackage_ActivateLoop(lua_State *L)
{
    //AudioPackage_ActivateLoop(fStartLoop, fEndLoop)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("AudioPackage_ActivateLoop");

    //--Get and Check
    AudioPackage *rCurrentPackage = AudioManager::Fetch()->rLastPackage;
    if(!rCurrentPackage) return 0;//LuaTypeError("AudioPackage_ActivateLoop");

    rCurrentPackage->AddLoop(lua_tonumber(L, 1), lua_tonumber(L, 2));

    return 0;
}
int Hook_AudioPackage_SetLocalVolume(lua_State *L)
{
    //AudioPackage_SetLocalVolume(fVolume)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("AudioPackage_SetLocalVolume");

    //--Get and Check
    AudioPackage *rCurrentPackage = AudioManager::Fetch()->rLastPackage;
    if(!rCurrentPackage) return 0;//LuaTypeError("AudioPackage_SetLocalVolume");

    rCurrentPackage->SetVolume(lua_tonumber(L, 1));

    return 0;
}
