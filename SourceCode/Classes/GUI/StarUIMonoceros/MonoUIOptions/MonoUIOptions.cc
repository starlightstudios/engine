//--Base
#include "MonoUIOptions.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"
#include "SaveManager.h"

///========================================== System ==============================================
MonoUIOptions::MonoUIOptions()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MOPT");

    ///--[Visiblity]
    mVisibilityTimerMax = cMonoVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIOptions ====== ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ===== MonoUIOptions ====== ]
    ///--[System]
    mEnableMouse = true;
    mPreviousMouseX = 0;
    mPreviousMouseY = 0;
    mResolutionsTotal = DisplayManager::Fetch()->GetTotalDisplayModes();
    mResolutionStrings = (char **)starmemoryalloc(sizeof(char *) * mResolutionsTotal);
    for(int i = 0; i < mResolutionsTotal; i ++)
    {
        DisplayInfo tInfo = DisplayManager::Fetch()->GetDisplayMode(i);
        mResolutionStrings[i] = InitializeString("%i x %i : %i", tInfo.mWidth, tInfo.mHeight, tInfo.mRefreshRate);
    }

    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Help Strings
    AllocateHelpStrings(cxMonoHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIOptions::~MonoUIOptions()
{
    for(int i = 0; i < mResolutionsTotal; i ++) free(mResolutionStrings[i]);
    free(mResolutionStrings);
}
void MonoUIOptions::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Options UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Options UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",              &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}
void MonoUIOptions::ConstructTitle()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Options UI Title",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Options UI Monoceros Title", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI Title",              &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[Debug]
    //--If images failed to resolve, this is local to this UI since the others are not in use on the title.
    //  Print diagnostics.
    if(!mImagesReady) ImageDiagnostics();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIOptions_Strings *MonoUIOptions::xrStringData = NULL;

///===================================== Property Queries =========================================
bool MonoUIOptions::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE)  return true;
    return false;
}

///======================================= Manipulators ===========================================
void MonoUIOptions::TakeForeground()
{
    AdvUIOptions::TakeForeground();
}

///======================================= Core Methods ===========================================
void MonoUIOptions::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Execution]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Set lines.
    SetHelpString(i, xrStringData->str.mTextHelp[0], 2, "DnLevel", "UpLevel");
    SetHelpString(i, xrStringData->str.mTextHelp[1]);
    SetHelpString(i, xrStringData->str.mTextHelp[2]);
    SetHelpString(i, xrStringData->str.mTextHelp[3], 1, "Cancel");
    SetHelpString(i, xrStringData->str.mTextHelp[4], 1, "F2");
    SetHelpString(i, xrStringData->str.mTextHelp[5], 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void MonoUIOptions::ReresolveOptionStrings()
{
    ///--[Call Base Class]
    AdvUIOptions::ReresolveOptionStrings();

    ///--[Overrides]
    //--"Accept" string
    mAcceptString->SetString(xrStringData->str.mTextControlDiscardChanges);
    mAcceptString->CrossreferenceImages();

    //--"Cancel" string
    mCancelString->SetString(xrStringData->str.mTextControlReturnToMenu);
    mCancelString->CrossreferenceImages();

    //--"Show Help" string
    mShowHelpString->SetString(xrStringData->str.mTextControlShowHelp);
    mShowHelpString->CrossreferenceImages();

    //--"Item Details" string
    mInspectString->SetString(xrStringData->str.mTextControlShowHelp);
    mInspectString->CrossreferenceImages();

    //--"Previous Page" string
    mPageLftString->SetString(xrStringData->str.mTextControlPrevPage);
    mPageLftString->CrossreferenceImages();

    //--"Next Page" string
    mPageRgtString->SetString(xrStringData->str.mTextControlNextPage);
    mPageRgtString->CrossreferenceImages();

    //--"Bind Primary" string
    mBindPrimaryString->SetString(xrStringData->str.mTextControlBindPrimary);
    mBindPrimaryString->CrossreferenceImages();

    //--"Bind Secondary" string
    mBindSecondaryString->SetString(xrStringData->str.mTextControlBindSecondary);
    mBindSecondaryString->CrossreferenceImages();

    //--"Restore Defaults" string
    mSetToDefaultString->SetString(xrStringData->str.mTextControlControlDefaults);
    mSetToDefaultString->CrossreferenceImages();

    //--"Restore All Control Defaults" string.
    mSetAllDefaultString->SetString(xrStringData->str.mTextControlAllControlDefaults);
    mSetAllDefaultString->CrossreferenceImages();
}
void MonoUIOptions::RecomputeCursorPositions()
{
    ///--[Okay/Defaults Case]
    //--Okay button.
    if((mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY && mCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_AUDIO    && mCursor == MONOMENU_OPTIONS_AUDIO_TOTAL    + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_CONTROLS && mCursor == MONOMENU_OPTIONS_CONTROLS_TOTAL + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_DEBUG    && mCursor == MONOMENU_OPTIONS_DEBUG_TOTAL    + 0))
    {
        mHighlightPos.MoveTo(449.0f, 686.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(154.0f, 51.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        return;
    }
    //--Defaults button.
    else if((mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY && mCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_AUDIO    && mCursor == MONOMENU_OPTIONS_AUDIO_TOTAL    + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_CONTROLS && mCursor == MONOMENU_OPTIONS_CONTROLS_TOTAL + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_DEBUG    && mCursor == MONOMENU_OPTIONS_DEBUG_TOTAL    + 1))
    {
        mHighlightPos.MoveTo(763.0f, 686.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(154.0f,  51.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        return;
    }

    ///--[Normal Case]
    //--Setup.
    float cLft = cNameLft - 13.0f;
    float cTop = cOptionTop + (cOptionHei * mCursor) - 6.0f;
    float cRgt = cBoolRgt + 12.0f;
    float cBot = cTop + cOptionHei + 9.0f;

    //--Set.
    mHighlightPos.MoveTo(cLft, cTop, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIOptions::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer,     0, mVisibilityTimerMax,             true);
    StandardTimer(mDetailsTimer,        0, ADVMENU_OPTIONS_DETAILS_SWITCH_TICKS,  (mIsShowingDetails));
    StandardTimer(mUnsavedChangesTimer, 0, ADVMENU_OPTIONS_UNSAVED_CHANGES_TICKS, (mIsShowingUnsavedChanges));
    StandardTimer(mRebindTimer,         0, ADVMENU_OPTIONS_REBIND_TICKS,          (mIsRebinding));
    StandardTimer(mHelpVisibilityTimer, 0, cMonoVisTicks,           (mIsShowingHelp));

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Arrow oscillation.
    mArrowTimer = (mArrowTimer + 1) % ADVMENU_OPTIONS_ARROW_OSCILLATE_TICKS;

    ///--[Error Checking]
    //--Disable control inputs during a lockout.
    if(mRebindLockout > 0) { mRebindLockout --; return true; }

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[Control Handling]
    //--Setup.
    bool tRecomputeCursorPosition = false;

    ///--[Rebinding Keys]
    //--When in this mode, the next key pressed instead becomes the desired control.
    if(mIsRebinding)
    {
        //--Otherwise, wait for a keypress.
        if(rControlManager->IsAnyKeyPressed())
        {
            //--Store these scancodes. They don't get updated until the user exits this menu.
            int tKeyboard = -1;
            int tMouse = -1;
            int tJoypad = -1;
            rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoypad);

            //--Cancel rebinding mode.
            mIsRebinding = false;
            mRebindLockout = 3;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Certain keys (F1, F2, F3) cannot be overbound.
            #if defined _ALLEGRO_PROJECT_
            if(tKeyboard == ALLEGRO_KEY_F1 || tKeyboard == ALLEGRO_KEY_F2 || tKeyboard == ALLEGRO_KEY_F3)
            {
                return true;
            }
            #endif
            #if defined _SDL_PROJECT_
            if(tKeyboard == SDL_SCANCODE_F1 || tKeyboard == SDL_SCANCODE_F2 || tKeyboard == SDL_SCANCODE_F3)
            {
                return true;
            }
            #endif

            //--Resolve the control's name from its cursor value.
            if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
            {
                if(mCursor == MONOMENU_OPTIONS_CONTROLS_ACTIVATE)
                {
                    rControlManager->OverbindControl("Activate", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_CANCEL)
                {
                    rControlManager->OverbindControl("Cancel", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_RUN)
                {
                    rControlManager->OverbindControl("Run", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_UP)
                {
                    rControlManager->OverbindControl("Up", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_RIGHT)
                {
                    rControlManager->OverbindControl("Right", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_DOWN)
                {
                    rControlManager->OverbindControl("Down", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_LEFT)
                {
                    rControlManager->OverbindControl("Left", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_SHOULDERLEFT)
                {
                    rControlManager->OverbindControl("UpLevel", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)
                {
                    rControlManager->OverbindControl("DnLevel", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_1)
                {
                    rControlManager->OverbindControl("FieldAbility0", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_2)
                {
                    rControlManager->OverbindControl("FieldAbility1", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_3)
                {
                    rControlManager->OverbindControl("FieldAbility2", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_4)
                {
                    rControlManager->OverbindControl("FieldAbility3", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_5)
                {
                    rControlManager->OverbindControl("FieldAbility4", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == MONOMENU_OPTIONS_CONTROLS_EMERGENCY_ESCAPE)
                {
                    rControlManager->OverbindControl("WHI Emergency Escape", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
            }

            //--Write to the save file.
            SaveManager::Fetch()->SaveControlsFile();

            //--Unset any control cases.
            rControlManager->BlankControls();
            rControlManager->ClearControlFlags();

            //--Check if any strings changed.
            ReresolveOptionStrings();
            return true;
        }
        return true;
    }

    ///--[Unsaved Changes Reminder]
    //--Intercepts player controls when exiting with unsaved changes.
    if(mIsShowingUnsavedChanges)
    {
        if(rControlManager->IsFirstPress("Activate"))
        {
            mIsShowingUnsavedChanges = false;
            CancelOptions();
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingUnsavedChanges = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return true;
    }

    ///--[Details Handler]
    //--If currently in details mode, pressing cancel exits it. This will also block certain other controls.
    if(mIsShowingDetails)
    {
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingDetails = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return true;
        }
    }

    //--Immediately switches to details mode on whatever option is highlighted. Stops the rest of the update.
    if(rControlManager->IsFirstPress("F2") && !mIsShowingDetails)
    {
        mIsShowingDetails = true;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Mouse Movement]
    //--Mouse can be used on this menu if it is enabled. Left-click is the same as pressing left, right click
    //  is the same as pressing right, and mouse movement can change the highlighted option.
    if(mEnableMouse)
    {
        //--Get mouse position.
        int tMouseX, tMouseY, tMouseZ;
        rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

        //--If the mouse moved, change the highlighted object.
        if(tMouseX != mPreviousMouseX || tMouseY != mPreviousMouseY)
        {
            //--Setup.
            bool tChangedMenu = false;

            //--Resolve clamp for this mode.
            int tClamp = 0;
            if(     mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY) tClamp = MONOMENU_OPTIONS_GAMEPLAY_TOTAL;
            else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)    tClamp = MONOMENU_OPTIONS_AUDIO_TOTAL;
            else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS) tClamp = MONOMENU_OPTIONS_CONTROLS_TOTAL;
            else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)    tClamp = MONOMENU_OPTIONS_DEBUG_TOTAL;

            //--Outside the bounds of the box.
            if(!IsPointWithin(tMouseX, tMouseY, 381, 110, 381+605, 110+637))
            {
                mCursor = -1;
            }
            //--"Okay" button.
            else if(IsPointWithin(tMouseX, tMouseY, 446, 684, 446+159, 684+55))
            {
                tChangedMenu = (mCursor != tClamp);
                mCursor = tClamp;
            }
            //--"Defaults" button.
            else if(IsPointWithin(tMouseX, tMouseY, 759, 684, 759+159, 684+55))
            {
                tChangedMenu = (mCursor != tClamp+1);
                mCursor = tClamp+1;
            }
            //--All other buttons, must align horizontally.
            else if(tMouseX >= 393 && tMouseX <= 971)
            {
                //--Compute the menu value.
                int tProspectValue = (tMouseY - cOptionTop) / cOptionHei;
                if(tProspectValue >= 0 && tProspectValue < tClamp)
                {
                    tChangedMenu = (mCursor != tProspectValue);
                    mCursor = tProspectValue;
                }
            }
            //--Set to -1.
            else
            {

            }

            //--If a change in menu happened, SFX and recompute cursor position.
            if(tChangedMenu)
            {
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }

            //--Save the mouse position.
            mPreviousMouseX = tMouseX;
            mPreviousMouseY = tMouseY;
        }
    }

    ///--[Shoulder Buttons]
    //--Shoulder-left, decrements the mode cursor. Wraps to maximum.
    if(rControlManager->IsFirstPress("UpLevel") || (rControlManager->IsDown("Ctrl") && rControlManager->IsFirstPress("Left")))
    {
        //--Set.
        mMode --;
        mCursor = 0;
        RecomputeCursorPositions();

        //--Wrap.
        if(mMode < 0)
        {
            mMode = ADVMENU_OPTIONS_MODE_TOTAL - 1;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }
    //--Shoulder-right, increments the mode cursor. Wraps to zero.
    if(rControlManager->IsFirstPress("DnLevel") || (rControlManager->IsDown("Ctrl") && rControlManager->IsFirstPress("Right")))
    {
        //--Set.
        mMode ++;
        mCursor = 0;
        RecomputeCursorPositions();

        //--Wrap.
        if(mMode >= ADVMENU_OPTIONS_MODE_TOTAL)
        {
            mMode = 0;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Up and Down]
    //--Decrements cursor. Wraps to end.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Resolve clamp for this mode.
        int tClamp = 0;
        if(     mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY) tClamp = MONOMENU_OPTIONS_GAMEPLAY_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)    tClamp = MONOMENU_OPTIONS_AUDIO_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS) tClamp = MONOMENU_OPTIONS_CONTROLS_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)    tClamp = MONOMENU_OPTIONS_DEBUG_TOTAL;

        //--Set.
        mCursor --;
        tRecomputeCursorPosition = true;

        //--Wrap.
        if(mCursor < 0)
        {
            mCursor = tClamp; //Sets it to the Okay button.
        }
        else if(mCursor == tClamp+0)
        {
            mCursor = tClamp-1;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Shoulder-right, increments the mode cursor. Wraps to zero.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Resolve clamp for this mode.
        int tClamp = 0;
        if(     mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY) tClamp = MONOMENU_OPTIONS_GAMEPLAY_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)    tClamp = MONOMENU_OPTIONS_AUDIO_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS) tClamp = MONOMENU_OPTIONS_CONTROLS_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)    tClamp = MONOMENU_OPTIONS_DEBUG_TOTAL;

        //--Set.
        mCursor ++;
        tRecomputeCursorPosition = true;

        //--Wrap.
        if(mCursor >= tClamp + 1) //Offset by one for the okay button.
        {
            mCursor = 0;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Left and Right]
    //--These are used to manipulate options and pass to a subroutine.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && !mIsShowingDetails)
    {
        HandleLeftPress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && !mIsShowingDetails)
    {
        HandleRightPress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--If the mouse is enabled, Left-click is the same as pushing left, Right-click is the same as pushing right.
    //  This does not apply to the Okay/Defaults buttons.
    //--Mouse variant. Pressing left click on Okay or Defaults is the same as Activate.
    if(mEnableMouse)
    {
        //--Resolve clamp for this mode. Affects both click types.
        int tClamp = 0;
        if(     mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY) tClamp = MONOMENU_OPTIONS_GAMEPLAY_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)    tClamp = MONOMENU_OPTIONS_AUDIO_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS) tClamp = MONOMENU_OPTIONS_CONTROLS_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)    tClamp = MONOMENU_OPTIONS_DEBUG_TOTAL;

        //--Special: If over the mode indicators, switches mode. This is only used for left-click.
        if(rControlManager->IsFirstPress("MouseLft") && !mIsShowingDetails)
        {
            //--Get mouse coordinates.
            int tMouseX, tMouseY, tMouseZ;
            rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

            //--Gameplay button.
            if(IsPointWithin(tMouseX, tMouseY, 383, 114, 383+150, 114+47))
            {
                mMode = ADVMENU_OPTIONS_MODE_GAMEPLAY;
                mCursor = -1;
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Audio button.
            else if(IsPointWithin(tMouseX, tMouseY, 533, 114, 533+150, 114+47))
            {
                mMode = ADVMENU_OPTIONS_MODE_AUDIO;
                mCursor = -1;
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Controls button.
            else if(IsPointWithin(tMouseX, tMouseY, 683, 114, 683+150, 114+47))
            {
                mMode = ADVMENU_OPTIONS_MODE_CONTROLS;
                mCursor = -1;
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Debug button.
            else if(IsPointWithin(tMouseX, tMouseY, 833, 114, 833+150, 114+47))
            {
                mMode = ADVMENU_OPTIONS_MODE_DEBUG;
                mCursor = -1;
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Outside.
            else if(mCursor == -1)
            {

            }
            //--Emulate a left-press for the selected option.
            else if(mCursor < tClamp)
            {
                HandleLeftPress(mMode, mCursor);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right-click.
        else if(rControlManager->IsFirstPress("MouseRgt") && mCursor < tClamp && !mIsShowingDetails)
        {
            HandleRightPress(mMode, mCursor);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Activate, Run, and Cancel]
    //--Activate. Begins editing if on an option.
    if(rControlManager->IsFirstPress("Activate") && !mIsShowingDetails)
    {
        HandleActivatePress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Mouse variant. Pressing left click on Okay or Defaults is the same as Activate.
    else if(mEnableMouse && rControlManager->IsFirstPress("MouseLft") && !mIsShowingDetails)
    {
        //--Resolve clamp for this mode.
        int tClamp = 0;
        if(     mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY) tClamp = MONOMENU_OPTIONS_GAMEPLAY_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)    tClamp = MONOMENU_OPTIONS_AUDIO_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS) tClamp = MONOMENU_OPTIONS_CONTROLS_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)    tClamp = MONOMENU_OPTIONS_DEBUG_TOTAL;

        //--If the cursor is on the clamp, that's the Okay button. Clamp+1 is the Defaults button.
        if(mCursor == tClamp || mCursor == tClamp+1)
        {
            HandleActivatePress(mMode, mCursor);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    //--Run. Only used when rebinding controls.
    if(rControlManager->IsFirstPress("Run") && !mIsShowingDetails)
    {
        HandleRunPress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel") && !mIsShowingDetails)
    {
        //--Has unsaved changes:
        if(mHasUnsavedChanges)
        {
            mIsShowingUnsavedChanges = true;
        }
        //--No unsaved changes, exit immediately:
        else
        {
            CancelOptions();
            FlagExit();
        }
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Restore Defaults Keys]
    //--In controls mode, and only controls mode, pressing these keys can reset controls back to their defaults.
    if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS && !mIsShowingDetails)
    {
        //--Resets the selected control to its defaults.
        if(rControlManager->IsFirstPress("F3"))
        {
            SetDefaultControl(mCursor);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Resets all controls to their defaults. Useful if the player overbinds everything.
        if(rControlManager->IsFirstPress("F5"))
        {
            SetAllDefaultControls();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    ///--[Cursor Handling]
    if(tRecomputeCursorPosition)
    {
        RecomputeCursorPositions();
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}
void MonoUIOptions::UpdateBackground()
{
    AdvUIOptions::UpdateBackground();
}
void MonoUIOptions::HandleActivatePress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Given a menu mode and cursor, handles activate. Booleans toggle, most other entries enter
    //  editing mode.
    if(pMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Boolean handlers.
        if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOURIST_MODE)
        {
            mTouristMode.mCurValue.b = !mTouristMode.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            mAutoHastenDialogue.mCurValue.b = !mAutoHastenDialogue.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            mCombatMemoryCursor.mCurValue.b = !mCombatMemoryCursor.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            mAllowAssetStreaming.mCurValue.b = !mAllowAssetStreaming.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            mUseRAMLoading.mCurValue.b = !mUseRAMLoading.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            mUnloadActors.mCurValue.b = !mUnloadActors.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            mUseFadingUI.mCurValue.b = !mUseFadingUI.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_RESOLUTION)
        {
            mResolution.mCurValue.i --;
            if(mResolution.mCurValue.i < 0) mResolution.mCurValue.i = mResolutionsTotal - 1;
            if(mResolution.mCurValue.i < 0) mResolution.mCurValue.i = 0;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_I)
        {
            mCensorIzana.mCurValue.b = !mCensorIzana.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_A)
        {
            mCensorCaelyn.mCurValue.b = !mCensorCaelyn.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_Y)
        {
            mCensorCyrano.mCurValue.b = !mCensorCyrano.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_E)
        {
            mCensorEnemies.mCurValue.b = !mCensorEnemies.mCurValue.b;
        }
        //--Okay. Exits mode.
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Toggle boolean.
        if(pCursor == MONOMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            mSaveVolumesInSavefile.mCurValue.b = !mSaveVolumesInSavefile.mCurValue.b;
        }
        //--Okay. Exits mode.
        else if(pCursor == MONOMENU_OPTIONS_AUDIO_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == MONOMENU_OPTIONS_AUDIO_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        //--Rebinding.
        if(pCursor < MONOMENU_OPTIONS_CONTROLS_TOTAL)
        {
            mIsRebinding = true;
            mIsRebindingSecondary = false;
        }
        //--Okay. Exits mode.
        if(pCursor == MONOMENU_OPTIONS_CONTROLS_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == MONOMENU_OPTIONS_CONTROLS_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        //--Toggle booleans.
        if(pCursor == MONOMENU_OPTIONS_DEBUG_DISABLE_OUTLINE)
        {
            mDisableShaderOutline.mCurValue.b = !mDisableShaderOutline.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER)
        {
            mDisableShaderUnderwater.mCurValue.b = !mDisableShaderUnderwater.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_DEBUG_DISABLE_LIGHTING)
        {
            mDisableShaderLighting.mCurValue.b = !mDisableShaderLighting.mCurValue.b;
        }
        //--Okay. Exits mode.
        if(pCursor == MONOMENU_OPTIONS_DEBUG_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == MONOMENU_OPTIONS_DEBUG_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }

    //--Subroutine checks changes list.
    RecheckChanges();
}
void MonoUIOptions::HandleRunPress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Only actually does anything during control handling, where it will rebind the secondary input instead of the primary.
    //  It is otherwise ignored.
    if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        //--Okay/Defaults button ignored.
        if(pCursor >= MONOMENU_OPTIONS_CONTROLS_TOTAL) return;

        //--Activate secondary binding.
        mIsRebinding = true;
        mIsRebindingSecondary = true;
    }
}
void MonoUIOptions::HandleLeftPress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Given a menu mode and cursor, handles left press.
    if(pMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Boolean handlers.
        if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOURIST_MODE)
        {
            mTouristMode.mCurValue.b = !mTouristMode.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            mAutoHastenDialogue.mCurValue.b = !mAutoHastenDialogue.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            mCombatMemoryCursor.mCurValue.b = !mCombatMemoryCursor.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)
        {
            mMaxAutosaves.mCurValue.i --;
            if(mMaxAutosaves.mCurValue.i < 0) mMaxAutosaves.mCurValue.i = 0;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            mAllowAssetStreaming.mCurValue.b = !mAllowAssetStreaming.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            mUseRAMLoading.mCurValue.b = !mUseRAMLoading.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            mUnloadActors.mCurValue.b = !mUnloadActors.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            mUseFadingUI.mCurValue.b = !mUseFadingUI.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_RESOLUTION)
        {
            mResolution.mCurValue.i --;
            if(mResolution.mCurValue.i < 0) mResolution.mCurValue.i = mResolutionsTotal - 1;
            if(mResolution.mCurValue.i < 0) mResolution.mCurValue.i = 0;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_I)
        {
            mCensorIzana.mCurValue.b = !mCensorIzana.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_A)
        {
            mCensorCaelyn.mCurValue.b = !mCensorCaelyn.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_Y)
        {
            mCensorCyrano.mCurValue.b = !mCensorCyrano.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_E)
        {
            mCensorEnemies.mCurValue.b = !mCensorEnemies.mCurValue.b;
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 0)
        {
            mCursor = MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 1)
        {
            mCursor = MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Increment rate.
        float cIncrement = 0.05f;
        if(ControlManager::Fetch()->IsDown("Run")) cIncrement = 0.01f;

        //--Float handlers.
        if(pCursor == MONOMENU_OPTIONS_MUSIC_VOLUME)
        {
            mMusicVol.mCurValue.f = mMusicVol.mCurValue.f - cIncrement;
            if(mMusicVol.mCurValue.f < 0.0f) mMusicVol.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_SOUND_VOLUME)
        {
            mSoundVol.mCurValue.f = mSoundVol.mCurValue.f - cIncrement;
            if(mSoundVol.mCurValue.f < 0.0f) mSoundVol.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeSoundVolumeTo(mSoundVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_IZANA_VOLUME)
        {
            mIzanaVoiceVol.mCurValue.f = mIzanaVoiceVol.mCurValue.f - cIncrement;
            if(mIzanaVoiceVol.mCurValue.f < 0.0f) mIzanaVoiceVol.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(0, mIzanaVoiceVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_CAELYN_VOLUME)
        {
            mCaelynVoiceVol.mCurValue.f = mCaelynVoiceVol.mCurValue.f - cIncrement;
            if(mCaelynVoiceVol.mCurValue.f < 0.0f) mCaelynVoiceVol.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(1, mCaelynVoiceVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_CYRANO_VOLUME)
        {
            mCyranoVoiceVol.mCurValue.f = mCyranoVoiceVol.mCurValue.f - cIncrement;
            if(mCyranoVoiceVol.mCurValue.f < 0.0f) mCyranoVoiceVol.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(2, mCyranoVoiceVol.mCurValue.f);
        }
        //--Toggle boolean.
        else if(pCursor == MONOMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            mSaveVolumesInSavefile.mCurValue.b = !mSaveVolumesInSavefile.mCurValue.b;
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == MONOMENU_OPTIONS_AUDIO_TOTAL + 0)
        {
            mCursor = MONOMENU_OPTIONS_AUDIO_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == MONOMENU_OPTIONS_AUDIO_TOTAL + 1)
        {
            mCursor = MONOMENU_OPTIONS_AUDIO_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        //--Okay. Exits mode.
        if(pCursor == MONOMENU_OPTIONS_DEBUG_TOTAL + 0)
        {
            mCursor = MONOMENU_OPTIONS_DEBUG_TOTAL + 1;
            RecomputeCursorPositions();
        }
        //--Defaults.
        else if(pCursor == MONOMENU_OPTIONS_DEBUG_TOTAL + 1)
        {
            mCursor = MONOMENU_OPTIONS_DEBUG_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }

    //--Subroutine checks changes list.
    RecheckChanges();
}
void MonoUIOptions::HandleRightPress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Given a menu mode and cursor, handles right press.
    if(pMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Boolean handlers.
        if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOURIST_MODE)
        {
            mTouristMode.mCurValue.b = !mTouristMode.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            mAutoHastenDialogue.mCurValue.b = !mAutoHastenDialogue.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            mCombatMemoryCursor.mCurValue.b = !mCombatMemoryCursor.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)
        {
            mMaxAutosaves.mCurValue.i ++;
            if(mMaxAutosaves.mCurValue.i > 100) mMaxAutosaves.mCurValue.i = 100;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            mAllowAssetStreaming.mCurValue.b = !mAllowAssetStreaming.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            mUseRAMLoading.mCurValue.b = !mUseRAMLoading.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            mUnloadActors.mCurValue.b = !mUnloadActors.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            mUseFadingUI.mCurValue.b = !mUseFadingUI.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_RESOLUTION)
        {
            mResolution.mCurValue.i ++;
            if(mResolution.mCurValue.i >= mResolutionsTotal) mResolution.mCurValue.i = 0;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_I)
        {
            mCensorIzana.mCurValue.b = !mCensorIzana.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_A)
        {
            mCensorCaelyn.mCurValue.b = !mCensorCaelyn.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_Y)
        {
            mCensorCyrano.mCurValue.b = !mCensorCyrano.mCurValue.b;
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_E)
        {
            mCensorEnemies.mCurValue.b = !mCensorEnemies.mCurValue.b;
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 0)
        {
            mCursor = MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 1)
        {
            mCursor = MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Increment rate.
        float cIncrement = 0.05f;
        if(ControlManager::Fetch()->IsDown("Run")) cIncrement = 0.01f;

        //--Float handlers.
        if(pCursor == MONOMENU_OPTIONS_MUSIC_VOLUME)
        {
            mMusicVol.mCurValue.f = mMusicVol.mCurValue.f + cIncrement;
            if(mMusicVol.mCurValue.f > 1.0f) mMusicVol.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_SOUND_VOLUME)
        {
            mSoundVol.mCurValue.f = mSoundVol.mCurValue.f + cIncrement;
            if(mSoundVol.mCurValue.f > 1.0f) mSoundVol.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeSoundVolumeTo(mSoundVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_IZANA_VOLUME)
        {
            mIzanaVoiceVol.mCurValue.f = mIzanaVoiceVol.mCurValue.f + cIncrement;
            if(mIzanaVoiceVol.mCurValue.f > 1.0f) mIzanaVoiceVol.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(0, mIzanaVoiceVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_CAELYN_VOLUME)
        {
            mCaelynVoiceVol.mCurValue.f = mCaelynVoiceVol.mCurValue.f + cIncrement;
            if(mCaelynVoiceVol.mCurValue.f > 1.0f) mCaelynVoiceVol.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(1, mCaelynVoiceVol.mCurValue.f);
        }
        else if(pCursor == MONOMENU_OPTIONS_CYRANO_VOLUME)
        {
            mCyranoVoiceVol.mCurValue.f = mCyranoVoiceVol.mCurValue.f + cIncrement;
            if(mCyranoVoiceVol.mCurValue.f > 1.0f) mCyranoVoiceVol.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(2, mCyranoVoiceVol.mCurValue.f);
        }
        //--Toggle boolean.
        else if(pCursor == MONOMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            mSaveVolumesInSavefile.mCurValue.b = !mSaveVolumesInSavefile.mCurValue.b;
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == MONOMENU_OPTIONS_AUDIO_TOTAL + 0)
        {
            mCursor = MONOMENU_OPTIONS_AUDIO_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == MONOMENU_OPTIONS_AUDIO_TOTAL + 1)
        {
            mCursor = MONOMENU_OPTIONS_AUDIO_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        //--Okay. Exits mode.
        if(pCursor == MONOMENU_OPTIONS_DEBUG_TOTAL + 0)
        {
            mCursor = MONOMENU_OPTIONS_DEBUG_TOTAL + 1;
            RecomputeCursorPositions();
        }
        //--Defaults.
        else if(pCursor == MONOMENU_OPTIONS_DEBUG_TOTAL + 1)
        {
            mCursor = MONOMENU_OPTIONS_DEBUG_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }

    //--Subroutine checks changes list.
    RecheckChanges();
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIOptions::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Render this UI.
    if(!mImagesReady) return;

    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    //--Option. Selects between fading and sliding.
    bool tUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Timer]
    //--Resolve timer offset.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)ADVMENU_OPTIONS_VIS_TICKS);

    //--Arrow oscillation.
    float cArrowOscillate = sinf((mArrowTimer / (float)ADVMENU_OPTIONS_ARROW_OSCILLATE_TICKS) * 3.1415926f * 2.0f) * ADVMENU_OPTIONS_ARROW_OSCILLATE_DISTANCE;

    ///--[Setup]
    //--If the UI is using translate logic, set the alpha to 1.0f.
    if(!tUseFade) cAlpha = 1.0f;

    //--Reset mixer.
    StarlightColor::ClearMixer();

    ///============================= Left Block ===============================
    //--Offset.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);

    //--Left-side arrow.
    MonoImages.rArrowLft->Draw(cArrowOscillate * -1.0f, 0.0f);

    //--Clean.
    AutoPieceClose();

    ///============================= Top Block ================================
    //--Offset.
    AutoPieceOpen(DIR_UP, pVisAlpha);

    //--Header.
    MonoImages.rOverlayTitleShade->Draw();

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cAlpha);
    MonoImages.rFontDoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 17.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSettings);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Control strings
    mShowHelpString->DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    mInspectString->DrawText (0.0f, cxMonoFontMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--In the controls menu, render these.
    if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        mBindPrimaryString->DrawText  (0.0f, cxMonoFontMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
        mBindSecondaryString->DrawText(0.0f, cxMonoFontMainlineFontH * 3.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
        mSetToDefaultString->DrawText (0.0f, cxMonoFontMainlineFontH * 4.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
        mSetAllDefaultString->DrawText(0.0f, cxMonoFontMainlineFontH * 5.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    }

    //--Clean.
    AutoPieceClose();

    ///============================ Right Block ===============================
    //--Offset.
    AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    //--Right-side arrow.
    MonoImages.rArrowRgt->Draw(cArrowOscillate * 1.0f, 0.0f);

    //--Clean.
    AutoPieceClose();

    ///============================ Bottom Block ==============================
    //--Offset.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Tabs.
    float cTabWid = 150.0f;
    for(int i = 0; i < 4; i ++)
    {
        if(mMode == i)
            MonoImages.rFrameTabSelected->Draw(cTabWid * i, 0.0f);
        else
            MonoImages.rFrameTab->Draw(cTabWid * i, 0.0f);
    }

    //--Tab Text.
    mMonoTitleColor.SetAsMixerAlpha(cAlpha);
    MonoImages.rFontHeading->DrawTextFixed(461.0f + (cTabWid * 0.0f), 111.0f, 145.0f, SUGARFONT_AUTOCENTER_X, xrStringData->str.mTextGameplay);
    MonoImages.rFontHeading->DrawTextFixed(461.0f + (cTabWid * 1.0f), 111.0f, 145.0f, SUGARFONT_AUTOCENTER_X, xrStringData->str.mTextAudio);
    MonoImages.rFontHeading->DrawTextFixed(461.0f + (cTabWid * 2.0f), 111.0f, 145.0f, SUGARFONT_AUTOCENTER_X, xrStringData->str.mTextControls);
    MonoImages.rFontHeading->DrawTextFixed(461.0f + (cTabWid * 3.0f), 111.0f, 145.0f, SUGARFONT_AUTOCENTER_X, xrStringData->str.mTextDebug);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Frame. Does not change based on option mode.
    MonoImages.rFrameOptions->Draw();

    //--Mode-based rendering.
    if(mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
        RenderOptionsGameplay(cAlpha);
    else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)
        RenderOptionsAudio(cAlpha);
    else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
        RenderOptionsControls(cAlpha);
    else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)
        RenderOptionsDebug(cAlpha);

    //--Defaults Button
    if(mMode != ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        if((mMode  == ADVMENU_OPTIONS_MODE_GAMEPLAY && mCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_AUDIO    && mCursor == MONOMENU_OPTIONS_AUDIO_TOTAL    + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_CONTROLS && mCursor == MONOMENU_OPTIONS_CONTROLS_TOTAL + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_DEBUG    && mCursor == MONOMENU_OPTIONS_DEBUG_TOTAL    + 1))
        {
            MonoImages.rButtonDefaultsSel->Draw();
            MonoImages.rFontHeading->DrawText(839.0f, 684.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextDefaults);
        }
        else
        {
            MonoImages.rButtonDefaults->Draw();
            MonoImages.rFontHeading->DrawText(839.0f, 684.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextDefaults);
        }
    }
    //--Okay Button
    if((mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY && mCursor == MONOMENU_OPTIONS_GAMEPLAY_TOTAL + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_AUDIO    && mCursor == MONOMENU_OPTIONS_AUDIO_TOTAL    + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_CONTROLS && mCursor == MONOMENU_OPTIONS_CONTROLS_TOTAL + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_DEBUG    && mCursor == MONOMENU_OPTIONS_DEBUG_TOTAL    + 0))
    {
        MonoImages.rButtonOkaySel->Draw();
        MonoImages.rFontHeading->DrawText(525.0f, 684.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextOkay);
    }
    else
    {
        MonoImages.rButtonOkay->Draw();
        MonoImages.rFontHeading->DrawText(525.0f, 684.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextOkay);
    }

    //--Highlight.
    //RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, Images.StandardUI.rHighlight);

    //--Unsaved changes text.
    if(mHasUnsavedChanges)
    {
        MonoImages.rFontMainline->DrawText(VIRTUAL_CANVAS_X * 0.50f, 633.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextChangesPending);
    }

    //--Changes require restart text.
    if(mRequiresRestart)
    {
        MonoImages.rFontMainline->DrawText(VIRTUAL_CANVAS_X * 0.50f, 658.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSomeChangesRequireAProgramRestart);
    }

    //--Control Strings.
    float cYPos = VIRTUAL_CANVAS_Y - cxMonoFontMainlineFontH;
    mPageLftString->DrawText(            0.0f, cYPos,                          SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    mPageRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--Details. Renders over everything else in this block.
    RenderDetails();

    //--Clean.
    AutoPieceClose();

    ///============================ No Blocking ===============================
    ///--[Unsaved Changes Overlay]
    if(mUnsavedChangesTimer > 0)
    {
        //--Percentage.
        float cUnsavedPercent = EasingFunction::QuadraticInOut(mUnsavedChangesTimer, ADVMENU_OPTIONS_UNSAVED_CHANGES_TICKS);

        //--Offsets.
        float cTopOffset = VIRTUAL_CANVAS_Y * (1.0f - cUnsavedPercent) * -1.0f;

        //--Darken everything behind this UI.
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cUnsavedPercent * 0.7f));

        //--Slides in from the top of the screen.
        glTranslatef(0.0f, cTopOffset * 1.0f, 0.0f);

        //--Frame.
        MonoImages.rFrameConfirm->Draw();

        //--String.
        MonoImages.rFontHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 180.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextYouHaveUnsavedChangesPending);
        MonoImages.rFontHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 215.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextReallyExit);
        mAcceptString->DrawText(410.0f, 280.0f, 0, 1.0f, MonoImages.rFontControl);
        mCancelString->DrawText(955.0f, 280.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, MonoImages.rFontControl);

        //--Clean.
        glTranslatef(0.0f, cTopOffset * -1.0f, 0.0f);
    }

    ///--[Rebinding Controls Overlay]
    if(mRebindTimer > 0)
    {
        //--Percentage.
        float cRebindPercent = EasingFunction::QuadraticInOut(mRebindTimer, ADVMENU_OPTIONS_REBIND_TICKS);

        //--Offsets.
        float cTopOffset = VIRTUAL_CANVAS_Y * (1.0f - cRebindPercent) * -1.0f;

        //--Darken everything behind this UI.
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cRebindPercent * 0.7f));

        //--Slides in from the top of the screen.
        glTranslatef(0.0f, cTopOffset * 1.0f, 0.0f);

        //--Frame.
        MonoImages.rFrameConfirm->Draw();

        //--Text.
        if(!mIsRebindingSecondary)
        {
            MonoImages.rFontHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 180.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextPressNewPrimaryKeyNow);
        }
        else
        {
            MonoImages.rFontHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 180.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextPressNewSecondaryKeyNow);
        }

        //--Clean.
        glTranslatef(0.0f, cTopOffset * -1.0f, 0.0f);
    }

    ///========================== Help Rendering ==============================
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}

///========================================= Subrenders ===========================================
void MonoUIOptions::RenderOptionsGameplay(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the gameplay set.

    ///--[Header]
    ///--[Selection]
    if(mHighlightPos.mYCur < 640.0f && mCursor != -1)
    {
        MonoImages.rOverlaySelection->Draw(mHighlightPos.mXCur, mHighlightPos.mYCur+9.0f);
    }

    ///--[Options Rendering]
    //--Boolean set.
    RenderBoolean(xrStringData->str.mTextTouristMode,               (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_TOURIST_MODE)),              mTouristMode.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextAutoHastenDialogue,        (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_HASTEN)),                    mAutoHastenDialogue.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextCombatMemoryCursor,        (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)),              mCombatMemoryCursor.mCurValue.b);
    RenderInteger(xrStringData->str.mTextMaxAutosaves,              (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)),              mMaxAutosaves.mCurValue.i, 100);
    RenderBoolean(xrStringData->str.mTextAllowAssetStreaming,       (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)),       mAllowAssetStreaming.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextUseRAMLoading,             (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_USERAMLOADING)),             mUseRAMLoading.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextUnloadActorsAfterDialogue, (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)), mUnloadActors.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextUseFadingUI,               (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_USEFADINGUI)),               mUseFadingUI.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextCensorIzana,               (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_CENSOR_I)),                  mCensorIzana.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextCensorCaelyn,              (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_CENSOR_A)),                  mCensorCaelyn.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextCensorCyrano,              (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_CENSOR_Y)),                  mCensorCyrano.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextCensorEnemies,             (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_CENSOR_E)),                  mCensorEnemies.mCurValue.b);

    //--Resolution is a string stored in the class. Render that instead.
    MonoImages.rFontMainline->DrawText(cNameLft, (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_RESOLUTION)), 0, 1.0f, xrStringData->str.mTextResolution);
    if(mResolution.mCurValue.i >= 0 && mResolution.mCurValue.i < mResolutionsTotal)
    {
        MonoImages.rFontMainline->DrawTextArgs(cBoolRgt, (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_GAMEPLAY_RESOLUTION)), SUGARFONT_RIGHTALIGN_X, 1.0f, "%s", mResolutionStrings[mResolution.mCurValue.i]);
    }
}
void MonoUIOptions::RenderOptionsAudio(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the audio set.

    ///--[Header]
    //--Static.
    //Images.OptionsUI.rOptionHeader->Draw();

    //--Text.
    mMonoTitleColor.SetAsMixerAlpha(pAlpha);
    //MonoImages.rFontHeading->DrawText(404.0f, 118.0f, 0, 1.0f, "Audio");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Selection]
    if(mHighlightPos.mYCur < 640.0f && mCursor != -1)
    {
        MonoImages.rOverlaySelection->Draw(mHighlightPos.mXCur, mHighlightPos.mYCur+9.0f);
    }

    ///--[Options Rendering]
    //--Boolean set.
    RenderFloat(  xrStringData->str.mTextMusicVolume,           (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_MUSIC_VOLUME)),         mMusicVol.mCurValue.f);
    RenderFloat(  xrStringData->str.mTextSoundVolume,           (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_SOUND_VOLUME)),         mSoundVol.mCurValue.f);
    RenderFloat(  xrStringData->str.mTextIzanaVoiceVolume,      (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_IZANA_VOLUME)),         mIzanaVoiceVol.mCurValue.f);
    RenderFloat(  xrStringData->str.mTextCaelynVoiceVolume,     (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CAELYN_VOLUME)),        mCaelynVoiceVol.mCurValue.f);
    RenderFloat(  xrStringData->str.mTextCyranoVoiceVolume,     (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CYRANO_VOLUME)),        mCyranoVoiceVol.mCurValue.f);
    RenderBoolean(xrStringData->str.mTextSaveVolumesInSavefile, (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_SAVEVALUESINSAVEFILE)), mSaveVolumesInSavefile.mCurValue.b);
}
void MonoUIOptions::RenderOptionsControls(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the control set.

    ///--[Header]
    //--Static.
    //Images.OptionsUI.rOptionHeader->Draw();

    //--Text.
    mMonoTitleColor.SetAsMixerAlpha(pAlpha);
    //MonoImages.rFontHeading->DrawText(404.0f, 118.0f, 0, 1.0f, "Controls");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Selection]
    if(mHighlightPos.mYCur < 640.0f && mCursor != -1)
    {
        MonoImages.rOverlaySelection->Draw(mHighlightPos.mXCur, mHighlightPos.mYCur+9.0f);
    }

    ///--[Options Rendering]
    //--List of controls.
    RenderControl(xrStringData->str.mTextActivateAccept, (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_ACTIVATE)),         "Activate");
    RenderControl(xrStringData->str.mTextCancelOpenMenu, (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_CANCEL)),           "Cancel");
    RenderControl(xrStringData->str.mTextRun,            (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_RUN)),              "Run");
    RenderControl(xrStringData->str.mTextUp,             (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_UP)),               "Up");
    RenderControl(xrStringData->str.mTextRight,          (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_RIGHT)),            "Right");
    RenderControl(xrStringData->str.mTextDown,           (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_DOWN)),             "Down");
    RenderControl(xrStringData->str.mTextLeft,           (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_LEFT)),             "Left");
    RenderControl(xrStringData->str.mTextShoulderLeft,   (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_SHOULDERLEFT)),     "UpLevel");
    RenderControl(xrStringData->str.mTextShoulderRight,  (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)),    "DnLevel");
    RenderControl(xrStringData->str.mTextAbility1,       (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_ABILITY_1)),        "FieldAbility0");
    RenderControl(xrStringData->str.mTextAbility2,       (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_ABILITY_2)),        "FieldAbility1");
    RenderControl(xrStringData->str.mTextAbility3,       (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_ABILITY_3)),        "FieldAbility2");
    RenderControl(xrStringData->str.mTextAbility4,       (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_ABILITY_4)),        "FieldAbility3");
    RenderControl(xrStringData->str.mTextAbility5,       (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_ABILITY_5)),        "FieldAbility4");
    RenderControl(xrStringData->str.mTextEmergencyEscape,(cOptionTop + (cOptionHei * MONOMENU_OPTIONS_CONTROLS_EMERGENCY_ESCAPE)), "WHI Emergency Escape");

    //--Text.
    mMonoSubtitleColor.SetAsMixerAlpha(pAlpha);
    MonoImages.rFontMainline->DrawText(404.0f, 600.0f, 0, 1.0f, xrStringData->str.mTextControlsSavedImmediately);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void MonoUIOptions::RenderOptionsDebug(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the control set.

    ///--[Header]
    //--Static.
    //Images.OptionsUI.rOptionHeader->Draw();

    //--Text.
    mMonoTitleColor.SetAsMixerAlpha(pAlpha);
    //MonoImages.rFontHeading->DrawText(404.0f, 118.0f, 0, 1.0f, "Debug");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Selection]
    if(mHighlightPos.mYCur < 640.0f && mCursor != -1)
    {
        MonoImages.rOverlaySelection->Draw(mHighlightPos.mXCur, mHighlightPos.mYCur+9.0f);
    }

    ///--[Options Rendering]
    //--Controls
    RenderControl(xrStringData->str.mTextOpenDebugMenu,           (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_DEBUG_OPENMENU)),           "OpenDebug");
    RenderControl(xrStringData->str.mTextToggleFuilscreen,        (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_DEBUG_TOGGLEFULLSCREEN)),   "ToggleFullscreen");
    RenderControl(xrStringData->str.mTextRebuildKerning,          (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_DEBUG_REBUILDKERNING)),     "RebuildKerning");
    RenderControl(xrStringData->str.mTextRebuildShaders,          (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_DEBUG_REBUILDSHADERS)),     "RecompileShaders");
    RenderBoolean(xrStringData->str.mTextDisableShaderOutline,    (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_DEBUG_DISABLE_OUTLINE)),    mDisableShaderOutline.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextDisableShaderUnderwater, (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER)), mDisableShaderUnderwater.mCurValue.b);
    RenderBoolean(xrStringData->str.mTextDisableShaderLighting,   (cOptionTop + (cOptionHei * MONOMENU_OPTIONS_DEBUG_DISABLE_LIGHTING)),   mDisableShaderLighting.mCurValue.b);

    //--Text.
    mMonoSubtitleColor.SetAsMixerAlpha(pAlpha);
    MonoImages.rFontMainline->DrawText(404.0f, 575.0f, 0, 1.0f, xrStringData->str.mTextDebugControlsMayNotBeRebound);
    MonoImages.rFontMainline->DrawText(404.0f, 600.0f, 0, 1.0f, xrStringData->str.mTextTheyAreHereForReference);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}

///===================================== Rendering Workers ========================================
void MonoUIOptions::RenderBoolean(const char *pName, float pY, bool pValue)
{
    ///--[Documentation]
    //--Given a boolean value, renders the name and True or False on the right edge. Also renders
    //  things like dividers.
    if(!pName) return;

    //--Name.
    MonoImages.rFontMainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Value.
    if(pValue)
        MonoImages.rFontMainline->DrawText(cBoolRgt, pY + 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, xrStringData->str.mTextTrue);
    else
        MonoImages.rFontMainline->DrawText(cBoolRgt, pY + 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, xrStringData->str.mTextFalse);

    //--Divider.
    //Images.OptionsUI.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void MonoUIOptions::RenderInteger(const char *pName, float pY, int pValue, int pMaximum)
{
    ///--[Documentation]
    //--Given an integer value, renders the name and a slider on the right side.
    if(!pName || pMaximum < 1) return;

    //--Name.
    MonoImages.rFontMainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Slider.
    MonoImages.rSliderBack->Draw(805.0f, pY + 3.0f);

    //--Render the slider indicator at the given position.
    float cMin = 815.0f;
    float cMax = 939.0f;
    float cPercent = (float)pValue / (float)pMaximum;
    float cXPos = (cMin + ((cMax - cMin) * cPercent));
    MonoImages.rSliderIndicator->Draw(cXPos, pY + 3.0f);

    //--Numerical value.
    MonoImages.rFontMainline->DrawTextArgs(cMin - 10.0f, pY - 2.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pValue);

    //--Divider.
    //Images.OptionsUI.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void MonoUIOptions::RenderFloat(const char *pName, float pY, float pValue)
{
    ///--[Documentation]
    //--Given a floating-point value, renders the name and a slider on the right side. pValue should
    //  be clamped between 0.0 and 1.0.
    if(!pName) return;

    //--Name.
    MonoImages.rFontMainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Slider.
    MonoImages.rSliderBack->Draw(805.0f, pY + 3.0f);

    //--Render the slider indicator at the given position.
    float cMin = 815.0f;
    float cMax = 939.0f;
    float cXPos = (cMin + ((cMax - cMin) * pValue));
    MonoImages.rSliderIndicator->Draw(cXPos, pY + 3.0f);

    //--Numerical value.
    MonoImages.rFontMainline->DrawTextArgs(cMin - 10.0f, pY - 2.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(pValue * 100.0f));

    //--Divider.
    //Images.OptionsUI.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void MonoUIOptions::RenderControl(const char *pName, float pY, const char *pControlName)
{
    ///--[Documentation]
    //--Given a control, renders the name and the primary/secondary inputs as symbols.
    if(!pName || !pControlName) return;

    //--Name.
    MonoImages.rFontMainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Resolve the images for the control.
    ControlManager *rControlManager = ControlManager::Fetch();
    ControlState *rControlState = rControlManager->GetControlState(pControlName);
    StarBitmap *rErrImg = rControlManager->GetErrorImage();

    ///--[Primary]
    //--Primary scancode.
    char tLetter = 'K';
    int tUseScancode = rControlState->mWatchKeyPri;
    if(tUseScancode < 0) {tLetter = 'M'; tUseScancode = rControlState->mWatchMouseBtnPri; }
    if(tUseScancode < 0) {tLetter = 'J'; tUseScancode = rControlState->mWatchJoyPri; }

    //--If the scancode is -1 or -2, the key is unbound.
    if(tUseScancode < 0)
    {
        MonoImages.rFontMainline->DrawTextArgs(728.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextUnbound);
    }
    //--Otherwise, the key has a primary binding.
    else
    {
        //--Keyboard:
        if(tLetter == 'K')
        {
            //--Check to see if an image exists to describe this scancode.
            StarBitmap *rUseImg = rControlManager->ResolveScancodeImage(tUseScancode);

            //--If the image is not the error image, we can render it.
            if(rErrImg != rUseImg)
            {
                rUseImg->Draw(728.0f - (rUseImg->GetTrueWidth() / 2.0f), pY + 2.0f);
            }
            //--Show the scancode directly.
            else
            {
                MonoImages.rFontMainline->DrawTextArgs(728.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
            }
        }
        //--Mouse/Joypad:
        else
        {
            MonoImages.rFontMainline->DrawTextArgs(728.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
        }

    }

    ///--[Secondary]
    //--Secondary scancode.
    tLetter = 'K';
    tUseScancode = rControlState->mWatchKeySec;
    if(tUseScancode < 0) {tLetter = 'M'; tUseScancode = rControlState->mWatchMouseBtnSec; }
    if(tUseScancode < 0) {tLetter = 'J'; tUseScancode = rControlState->mWatchJoySec; }

    //--If the scancode is -1 or -2, the key is unbound.
    if(tUseScancode < 0)
    {
        MonoImages.rFontMainline->DrawText(920.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextUnbound);
    }
    //--Otherwise, the key has a primary binding.
    else
    {
        //--Keyboard:
        if(tLetter == 'K')
        {
            //--Check to see if an image exists to describe this scancode.
            StarBitmap *rUseImg = rControlManager->ResolveScancodeImage(tUseScancode);

            //--If the image is not the error image, we can render it.
            if(rErrImg != rUseImg)
            {
                rUseImg->Draw(920.0f - (rUseImg->GetTrueWidthSafe() / 2.0f), pY + 2.0f);
            }
            //--Show the scancode directly.
            else
            {
                MonoImages.rFontMainline->DrawTextArgs(870.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
            }
        }
        //--Mouse/Joypad:
        else
        {
            MonoImages.rFontMainline->DrawTextArgs(870.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
        }
    }

    //--Divider.
    //Images.OptionsUI.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void MonoUIOptions::RenderDetails()
{
    ///--[Documentation]
    //--Renders the details window at the bottom of the screen, showing what the option highlighted does.
    //  Every option has a unique description so they are enumerated here.
    if(mDetailsTimer < 1) return;

    //--Percentage.
    float cDetailsPercent = EasingFunction::QuadraticInOut(mDetailsTimer, ADVMENU_OPTIONS_DETAILS_SWITCH_TICKS);

    //--Offsets.
    float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cDetailsPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cDetailsPercent * 0.7f));

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    //--Frame.
    MonoImages.rFrameDescription->Draw();

    //--Expandable header and name. Note that there is an additional 48 pixels of space on the header
    //  within the dead zones.
    char *tHeaderText = NULL;

    ///--[Description]
    //--Setup.
    float cX = 105.0f;
    float cY = 558.0f;
    float cH   =  25.0f;
    StarFont *rFont = MonoImages.rFontMainline;

    //--Gameplay Block:
    if(mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Tourist Mode:
        if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_TOURIST_MODE)
        {
            ResetString(tHeaderText, xrStringData->str.mTextTouristMode);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpTourist[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpTourist[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpTourist[2]);
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, xrStringData->str.mOptionHelpTourist[3]);
        }
        //--Auto-hasten Dialogue:
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            ResetString(tHeaderText, xrStringData->str.mTextAutoHastenDialogue);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpHasten[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpHasten[1]);
        }
        //--Combat memory cursor.
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCombatMemoryCursor);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpMemory[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpMemory[1]);
        }
        //--Autosaves:
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)
        {
            ResetString(tHeaderText, xrStringData->str.mTextMaxAutosaves);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpAutosaves[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpAutosaves[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpAutosaves[2]);
        }
        //--Asset Streaming:
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            ResetString(tHeaderText, xrStringData->str.mTextAllowAssetStreaming);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpAssetStreaming[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpAssetStreaming[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpAssetStreaming[2]);
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, xrStringData->str.mOptionHelpAssetStreaming[3]);
            rFont->DrawText(cX, cY + (cH * 4.0f), 0, 1.0f, xrStringData->str.mOptionHelpAssetStreaming[4]);
            rFont->DrawText(cX, cY + (cH * 5.0f), 0, 1.0f, xrStringData->str.mOptionHelpAssetStreaming[5]);
        }
        //--RAM Loading:
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            ResetString(tHeaderText, xrStringData->str.mTextUseRAMLoading);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpRAMLoading[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpRAMLoading[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpRAMLoading[2]);
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, xrStringData->str.mOptionHelpRAMLoading[3]);
            rFont->DrawText(cX, cY + (cH * 4.0f), 0, 1.0f, xrStringData->str.mOptionHelpRAMLoading[4]);
            rFont->DrawText(cX, cY + (cH * 5.0f), 0, 1.0f, xrStringData->str.mOptionHelpRAMLoading[5]);
        }
        //--Unload Actors:
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            ResetString(tHeaderText, xrStringData->str.mTextUnloadActorsAfterDialogue);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpUnloadActors[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpUnloadActors[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpUnloadActors[2]);
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, xrStringData->str.mOptionHelpUnloadActors[3]);
            rFont->DrawText(cX, cY + (cH * 4.0f), 0, 1.0f, xrStringData->str.mOptionHelpUnloadActors[4]);
        }
        //--Use Fading UI
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            ResetString(tHeaderText, xrStringData->str.mTextUseFadingUI);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpFadingUI[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpFadingUI[1]);
        }
        //--Resolution.
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_RESOLUTION)
        {
            ResetString(tHeaderText, xrStringData->str.mTextResolution);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpResolution[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpResolution[1]);
        }
        //--Censor Izana.
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_I)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCensorIzana);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorI[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorI[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorI[2]);
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorI[3]);
        }
        //--Censor Caelyn.
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_A)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCensorCaelyn);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorA[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorA[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorA[2]);
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorA[3]);
        }
        //--Censor Cyrano.
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_Y)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCensorCyrano);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorY[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorY[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorY[2]);
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorY[3]);
        }
        //--Censor Enemies.
        else if(mCursor == MONOMENU_OPTIONS_GAMEPLAY_CENSOR_E)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCensorEnemies);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorE[0]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpCensorE[1]);
        }
    }
    //--Audio Block:
    else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Music Volume:
        if(mCursor == MONOMENU_OPTIONS_MUSIC_VOLUME)
        {
            ResetString(tHeaderText, xrStringData->str.mTextMusicVolume);
            rFont->DrawText(cX, cY + (cY * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpMusicVol[0]);
            rFont->DrawText(cX, cY + (cY * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpMusicVol[1]);
        }
        //--Sound Volume:
        else if(mCursor == MONOMENU_OPTIONS_SOUND_VOLUME)
        {
            ResetString(tHeaderText, xrStringData->str.mTextSoundVolume);
            rFont->DrawText(cX, cY + (cY * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpSoundVol[0]);
            rFont->DrawText(cX, cY + (cY * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpSoundVol[1]);
        }
        //--Izana Voice Volume:
        else if(mCursor == MONOMENU_OPTIONS_IZANA_VOLUME)
        {
            ResetString(tHeaderText, xrStringData->str.mTextIzanaVoiceVolume);
            rFont->DrawText(cX, cY + (cY * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpIzanaVol[0]);
            rFont->DrawText(cX, cY + (cY * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpIzanaVol[1]);
        }
        //--Caelyn Voice Volume:
        else if(mCursor == MONOMENU_OPTIONS_CAELYN_VOLUME)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCaelynVoiceVolume);
            rFont->DrawText(cX, cY + (cY * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpCaelynVol[0]);
            rFont->DrawText(cX, cY + (cY * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpCaelynVol[1]);
        }
        //--Cyrano Voice Volume:
        else if(mCursor == MONOMENU_OPTIONS_CYRANO_VOLUME)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCyranoVoiceVolume);
            rFont->DrawText(cX, cY + (cY * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpCyranoVol[0]);
            rFont->DrawText(cX, cY + (cY * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpCyranoVol[1]);
        }
        //--Save Volumes In Savefile:
        else if(mCursor == MONOMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            ResetString(tHeaderText, xrStringData->str.mTextSaveVolumesInSavefile);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpAudioInSaves[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpAudioInSaves[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpAudioInSaves[2]);
        }
    }
    //--Debug Block:
    else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        if(mCursor == MONOMENU_OPTIONS_CONTROLS_ACTIVATE)
        {
            ResetString(tHeaderText, xrStringData->str.mTextActivateAccept);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlActivate[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlActivate[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlActivate[2]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_CANCEL)
        {
            ResetString(tHeaderText, xrStringData->str.mTextCancelOpenMenu);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlCancel[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlCancel[1]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_RUN)
        {
            ResetString(tHeaderText, xrStringData->str.mTextRun);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlRun[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_UP)
        {
            ResetString(tHeaderText, xrStringData->str.mTextUp);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlUp[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_RIGHT)
        {
            ResetString(tHeaderText, xrStringData->str.mTextRight);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlRight[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_DOWN)
        {
            ResetString(tHeaderText, xrStringData->str.mTextDown);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlDown[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_LEFT)
        {
            ResetString(tHeaderText, xrStringData->str.mTextLeft);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlLeft[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_SHOULDERLEFT)
        {
            ResetString(tHeaderText, xrStringData->str.mTextShoulderLeft);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlShoulderLeft[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)
        {
            ResetString(tHeaderText, xrStringData->str.mTextShoulderRight);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlShoulderRight[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_1)
        {
            ResetString(tHeaderText, xrStringData->str.mTextAbility1);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlAbility1[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_2)
        {
            ResetString(tHeaderText, xrStringData->str.mTextAbility2);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlAbility2[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_3)
        {
            ResetString(tHeaderText, xrStringData->str.mTextAbility3);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlAbility3[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlAbility3[1]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_4)
        {
            ResetString(tHeaderText, xrStringData->str.mTextAbility4);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlAbility4[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_ABILITY_5)
        {
            ResetString(tHeaderText, xrStringData->str.mTextAbility5);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpControlAbility5[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_CONTROLS_EMERGENCY_ESCAPE)
        {
            ResetString(tHeaderText, xrStringData->str.mTextEmergencyEscape);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpEmergencyEscape[0]);
        }
    }
    //--Controls Block:
    else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        if(mCursor == MONOMENU_OPTIONS_DEBUG_OPENMENU)
        {
            ResetString(tHeaderText, xrStringData->str.mTextOpenDebugMenu);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugOpenMenu[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugOpenMenu[1]);
        }
        else if(mCursor == MONOMENU_OPTIONS_DEBUG_TOGGLEFULLSCREEN)
        {
            ResetString(tHeaderText, xrStringData->str.mTextToggleFuilscreen);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugFullscreen[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugFullscreen[1]);
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugFullscreen[2]);
        }
        else if(mCursor == MONOMENU_OPTIONS_DEBUG_REBUILDKERNING)
        {
            ResetString(tHeaderText, xrStringData->str.mTextRebuildKerning);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugKerning[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugKerning[1]);
        }
        else if(mCursor == MONOMENU_OPTIONS_DEBUG_REBUILDSHADERS)
        {
            ResetString(tHeaderText, xrStringData->str.mTextRebuildShaders);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugShaders[0]);
        }
        else if(mCursor == MONOMENU_OPTIONS_DEBUG_DISABLE_OUTLINE)
        {
            ResetString(tHeaderText, xrStringData->str.mTextDisableShaderOutline);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugOutline[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugOutline[1]);
        }
        else if(mCursor == MONOMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER)
        {
            ResetString(tHeaderText, xrStringData->str.mTextDisableShaderUnderwater);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugUnderwater[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugUnderwater[1]);
        }
        else if(mCursor == MONOMENU_OPTIONS_DEBUG_DISABLE_LIGHTING)
        {
            ResetString(tHeaderText, xrStringData->str.mTextDisableShaderLighting);
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugLightning[0]);
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, xrStringData->str.mOptionHelpDebugLightning[1]);
        }
    }

    ///--[Header]
    //--Now that we know the name to render, do that.
    if(tHeaderText)
    {
        //float cNameLen = MonoImages.rFontHeading->GetTextWidth(tHeaderText);
        //RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, cNameLen - 48.0f, 76.0f, 76.0f, Images.OptionsUI.rExpandableHeader);
        MonoImages.rFontHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 510.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tHeaderText);
    }

    ///--[Finish Up]
    //--Clean.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
    free(tHeaderText);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================

