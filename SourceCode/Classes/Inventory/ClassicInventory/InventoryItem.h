///======================================= InventoryItem ==========================================
//--An entry in the inventory. May be a derived class (weapon, armor) with special properties.
//  Items do not have a quantity, each one is unique. However, the display may lump them together
//  if they are the same/similar.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
typedef struct ItemCommand
{
    //--Variables
    char *mString;

    //--Script Version
    bool mIsScriptVersion;
    char *mControlScript;

    //--Standard Initialization.
    void Initialize()
    {
        mString = InitializeString("Unnamed Command");
        mIsScriptVersion = false;
        mControlScript = NULL;
    }
    void InitializeToScript(const char *pString, const char *pLuaScript)
    {
        Initialize();
        if(!pString || !pLuaScript) return;

        mString = InitializeString(pString);
        mIsScriptVersion = true;
        mControlScript = InitializeString(pLuaScript);
    }

    //--Cloning.
    ItemCommand *Clone()
    {
        SetMemoryData(__FILE__, __LINE__);
        ItemCommand *nCommand = (ItemCommand *)starmemoryalloc(sizeof(ItemCommand));
        nCommand->mString = InitializeString(mString);
        nCommand->mIsScriptVersion = mIsScriptVersion;
        nCommand->mControlScript = InitializeString(mControlScript);
        return nCommand;
    }

    //--Deletion
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        ItemCommand *rCommand = (ItemCommand *)pPtr;
        free(rCommand->mString);
        free(rCommand->mControlScript);
        free(rCommand);
    }
}ItemCommand;

///===================================== Local Definitions ========================================
#define ITEM_TYPE_NONE 0
#define ITEM_TYPE_CONSUMABLE 1
#define ITEM_TYPE_WEAPON 2
#define ITEM_TYPE_ARMOR 3
#define ITEM_TYPE_TOTAL 4

///========================================== Classes =============================================
class InventoryItem : public RootObject
{
    private:
    //--System
    bool mAutoDespawn;
    char *mLocalName;
    char *mDescription;
    int mItemTier;

    //--Durability
    int mDurability;
    int mDurabilityMax;
    int mBreakDurability;

    //--Storage
    char *mUseScript;
    int mItemType;
    Actor *rUser;
    StarLinkedList *mTargetList;

    //--Trap Properties
    char *mTrapScript;

    //--Banes
    StarLinkedList *mBaneList;

    //--Combat Statistics
    CombatStats mCombatStats;

    //--Player Commands
    bool mCanUseOffGround;
    StarLinkedList *mCommandList;

    protected:

    public:
    //--System
    InventoryItem();
    virtual ~InventoryItem();
    InventoryItem *Clone();

    //--Public Variables
    bool mIsConsumed;
    bool mIsSelfDestructing;

    //--Public Statics
    static int xSoundTimer;

    //--Property Queries
    char *GetName();
    char *GetDescription();
    bool CanUseOffGround();
    bool AutoDespawns();
    const char *GetTrapScript();
    int GetItemType();
    int GetItemTier();
    const char *GetUseString();
    int GetDurability();
    int GetDurabilityMax();
    int GetDurabilityBreak();
    int GetAttackPower();
    int GetDefensePower();
    int GetAccuracyBonus();
    float GetBaneFactor(const char *pName);

    //--Manipulators
    void SetName(const char *pName);
    void SetAutoDespawn(bool pFlag);
    void SetDurability(int pValue);
    void SetDurabilityMax(int pValue);
    void SetDurabilityBreak(int pValue);
    void SetItemTier(int pTier);
    void SetItemType(int pType);
    void SetUsePath(const char *pPath);
    void SetTrapPath(const char *pPath);
    void SetTypeByString(const char *pString);
    void SetDescription(const char *pString);
    void SetGroundUseFlag(bool pFlag);
    void RegisterCommand(ItemCommand *pCommand);
    void RegisterCommand(const char *pString, const char *pLuaScript);
    void SetAttackPower(int pPower);
    void SetDefensePower(int pPower);
    void SetAccuracyPower(int pPower);
    void FlagConsumed();
    void AddBane(const char *pName, float pMultiplier);

    //--Core Methods
    void UseItemOn(Actor *pUser, Actor *pTarget);
    StarBitmap *Get3DImageFor();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    StarLinkedList *GetCommandList();
    StarLinkedList *GetBaneList();
    Actor *GetUser();
    void *GetTarget(int pSlot);

    //--Static Functions
    static void PlayPickupSound(InventoryItem *pItem);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_InventoryItem_CreatePrototype(lua_State *L);
int Hook_InventoryItem_GetProperty(lua_State *L);
int Hook_InventoryItem_SetProperty(lua_State *L);
int Hook_InventoryItem_CreateFromPrototype(lua_State *L);
int Hook_InventoryItem_PushPrototype(lua_State *L);
int Hook_InventoryItem_PushCurrentUser(lua_State *L);
int Hook_InventoryItem_PushCurrentTarget(lua_State *L);
int Hook_InventoryItem_Consume(lua_State *L);
