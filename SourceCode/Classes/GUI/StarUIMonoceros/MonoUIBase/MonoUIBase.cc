//--Base
#include "MonoUIBase.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
MonoUIBase::MonoUIBase()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MBAS");

    ///--[Visiblity]
    mVisibilityTimerMax = cMonoVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = 394.0f;
    mGridRenderCenterY  = 387.0f;
    mGridRenderSpacingX = 107.0f;
    mGridRenderSpacingY = 107.0f;

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ======= AdvUIBase ======== ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= MonoUIBase ======= ]
    ///--[Variables]
    //--System
    mIsSwitchMode = false;
    mIsReplaceMode = false;
    mSwitchTimer = 0;
    mSwitchCharCursor = 0;
    mSwitchReplaceCursor = 0;

    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    ///--[String Initialization]
    //--Allocate Help Strings
    AllocateHelpStrings(cxMonoHelpStrings);

    ///--[Image Verification]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIBase::~MonoUIBase()
{
}
void MonoUIBase::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Base UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Base UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",           &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = MonoImages.rHeadingFont;
}
void MonoUIBase::RunTranslation()
{
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIBase_Strings *MonoUIBase::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIBase_Strings::MonoUIBase_Strings()
{
    //--Local name: "MonoUIBase"
    MonoUIBase::xrStringData = this;

    //--Strings
    str.mTextMainMenu     = InitializeString("MonoUIBase_mMainMenu");      //"Main Menu"
    str.mTextPlatina      = InitializeString("MonoUIBase_mPlatina");       //"Platina"
    str.mTextAdamantite   = InitializeString("MonoUIBase_mAdamantite");    //"Adamantite"
    str.mTextPowder       = InitializeString("MonoUIBase_mPowder");        //"Powder"
    str.mTextFlakes       = InitializeString("MonoUIBase_mFlakes");        //"Flakes"
    str.mTextShards       = InitializeString("MonoUIBase_mShards");        //"Shards"
    str.mTextPieces       = InitializeString("MonoUIBase_mPieces");        //"Pieces"
    str.mTextChunks       = InitializeString("MonoUIBase_mChunks");        //"Chunks"
    str.mTextOre          = InitializeString("MonoUIBase_mOre");           //"Ore"
    str.mTextHelpMenu     = InitializeString("MonoUIBase_mHelp Menu");     //"Help Menu"
    str.mTextEquipment    = InitializeString("MonoUIBase_mEquipment");     //"Equipment"
    str.mTextItems        = InitializeString("MonoUIBase_mItems");         //"Items"
    str.mTextStatus       = InitializeString("MonoUIBase_mStatus");        //"Status"
    str.mTextSkills       = InitializeString("MonoUIBase_mSkills");        //"Skills"
    str.mTextMap          = InitializeString("MonoUIBase_mMap");           //"Map"
    str.mTextOptions      = InitializeString("MonoUIBase_mOptions");       //"Options"
    str.mTextJournal      = InitializeString("MonoUIBase_mJournal");       //"Journal"
    str.mTextQuit         = InitializeString("MonoUIBase_mQuit");          //"Quit"
    str.mTextSwitchMenu   = InitializeString("MonoUIBase_mPartyOrdering"); //"Party Ordering"

    //--Help Sequence
    char tBuffer[100];
    for(int i = 0; i < 10; i ++)
    {
        sprintf(tBuffer, "MonoUIBase_mHelpText%02i", i);
        str.mTextHelp[i] = InitializeString(tBuffer);
    }

    //--Control Sequence
    str.mTextControlShowHelp = InitializeString("MonoUIBase_mControlShowHelp"); //"[IMG0] Show Help"
}
MonoUIBase_Strings::~MonoUIBase_Strings()
{
    for(int i = 0; i < MONOUIBASE_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIBase_Strings::GetSize()
{
    return MONOUIBASE_STRINGS_TOTAL;
}
void MonoUIBase_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIBASE_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUIBase::SetCursorByBackwardsCode(int pMode)
{
    ///--[Documentation]
    //--Given a code of the series AM_MODE_BASE defined in MonoUIBase.h, resolves the menu position
    //  of the entry matching that code.
    //--Same as the base class but adjusted for the altered Monoceros layout.

    ///--[Execution]
    //--Base mode, sets to whatever the 0th entry is.
    if(pMode == AM_MODE_BASE)
    {
        SetCursor(0);
    }
    //--Other modes:
    else if(pMode == AM_MODE_ITEMS)
    {
        SetCursor(AM_MAIN_ITEMS);
    }
    else if(pMode == AM_MODE_EQUIP)
    {
        SetCursor(AM_MAIN_EQUIPMENT);
    }
    else if(pMode == AM_MODE_STATUS)
    {
        SetCursor(AM_MAIN_STATUS);
    }
    else if(pMode == AM_MODE_SKILLS)
    {
        SetCursor(AM_MAIN_SKILLS);
    }
    else if(pMode == AM_MODE_MAP)
    {
        SetCursor(AM_MAIN_MAP);
    }
    else if(pMode == AM_MODE_OPTIONS)
    {
        SetCursor(AM_MAIN_OPTIONS);
    }
    else if(pMode == AM_MODE_QUIT)
    {
        SetCursor(AM_MAIN_QUIT);
    }
    else if(pMode == AM_MODE_DOCTOR)
    {
        SetCursor(AM_MAIN_DOCTOR);
    }
    else if(pMode == AM_MODE_JOURNAL)
    {
        SetCursor(AM_MAIN_JOURNAL);
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("AdventureMenu:SetToMainMenuByCode() - Warning. Mode %i was unhandled.\n", pMode);
    }
}
void MonoUIBase::TakeForeground()
{
    ///--[Variables]
    //--Grid.
    SetCursor(0);

    ///--[Construction]
    //--On the first build, setup the menu grid.
    if(!mHasBuiltGrid)
    {
        ///--[Basic Setup]
        //--Construct the grid and set its positions.
        mHasBuiltGrid = true;
        AllocateGrid(3, 3);
        ProcessGrid();

        ///--[Components]
        //--Special: The Switch option only exists if there are 4 or more party members.
        AdvCombat *rAdvCombat = AdvCombat::Fetch();
        if(rAdvCombat && rAdvCombat->GetActivePartyCount() >= 4)
        {
            QuicksetGridEntry(0, 0, AM_MAIN_SWITCH, "Switch", AdvImages.rIcons[AM_MAIN_SWITCH]);
        }

        //--Populate the grid.
        QuicksetGridEntry(1, 0, AM_MAIN_STATUS,    xrStringData->str.mTextStatus,    AdvImages.rIcons[AM_MAIN_STATUS]);
        QuicksetGridEntry(2, 0, AM_MAIN_OPTIONS,   xrStringData->str.mTextOptions,   AdvImages.rIcons[AM_MAIN_OPTIONS]);
        QuicksetGridEntry(0, 1, AM_MAIN_JOURNAL,   xrStringData->str.mTextJournal,   AdvImages.rIcons[AM_MAIN_JOURNAL]);
        QuicksetGridEntry(1, 1, AM_MAIN_EQUIPMENT, xrStringData->str.mTextEquipment, AdvImages.rIcons[AM_MAIN_EQUIPMENT]);
        QuicksetGridEntry(2, 1, AM_MAIN_ITEMS,     xrStringData->str.mTextItems,     AdvImages.rIcons[AM_MAIN_ITEMS]);
        QuicksetGridEntry(0, 2, AM_MAIN_QUIT,      xrStringData->str.mTextQuit,      AdvImages.rIcons[AM_MAIN_QUIT]);
        QuicksetGridEntry(1, 2, AM_MAIN_SKILLS,    xrStringData->str.mTextSkills,    AdvImages.rIcons[AM_MAIN_SKILLS]);
        QuicksetGridEntry(2, 2, AM_MAIN_MAP,       xrStringData->str.mTextMap,       AdvImages.rIcons[AM_MAIN_MAP]);
    }

    //--Cursor.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString(xrStringData->str.mTextControlShowHelp);
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    ///--[Party]
    //--Run across the party and recompute stats. This makes sure their health values line up.
    AdvCombat *rCombat = AdvCombat::Fetch();
    int tPartySize = rCombat->GetActivePartyCount();
    for(int i = 0; i < tPartySize; i ++)
    {
        AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
        if(rCharacter) rCharacter->RefreshStatsForUI();
    }
}

///======================================= Core Methods ===========================================
void MonoUIBase::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Execution]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Common lines.
    SetHelpString(i, xrStringData->str.mTextHelp[0], 2, "Activate", "Cancel");
    SetHelpString(i, xrStringData->str.mTextHelp[1]);
    SetHelpString(i, xrStringData->str.mTextHelp[2]);
    SetHelpString(i, xrStringData->str.mTextHelp[3]);
    SetHelpString(i, xrStringData->str.mTextHelp[4]);
    SetHelpString(i, xrStringData->str.mTextHelp[5]);
    SetHelpString(i, xrStringData->str.mTextHelp[6]);
    SetHelpString(i, xrStringData->str.mTextHelp[7]);
    SetHelpString(i, xrStringData->str.mTextHelp[8]);
    SetHelpString(i, xrStringData->str.mTextHelp[9], 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIBase::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        mVisibilityTimer ++;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--Standard Timers.
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks, mIsShowingHelp);
    StandardTimer(mSwitchTimer,         0, ADV_MENU_VIS_TICKS,          mIsSwitchMode);

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Set this flag.
    mDontShowHighlight = false;

    ///--[Switch Submode]
    if(mIsSwitchMode)
    {
        UpdateMainMenuSwitch();
        return true;
    }

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[ =========== Update Call ============ ]
    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return true;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get what option is under the cursor.
        int tGridOption = GetGridCodeAtCursor();

        //--Mode handlers.
        if(tGridOption == AM_MAIN_ITEMS)
        {
            mCodeBackward = AM_MODE_ITEMS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_EQUIPMENT)
        {
            mCodeBackward = AM_MODE_EQUIP;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_SKILLS)
        {
            mCodeBackward = AM_MODE_SKILLS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_STATUS)
        {
            mCodeBackward = AM_MODE_STATUS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_MAP)
        {
            mCodeBackward = AM_MODE_MAP;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_OPTIONS)
        {
            mCodeBackward = AM_MODE_OPTIONS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_JOURNAL)
        {
            mCodeBackward = AM_MODE_JOURNAL;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_QUIT)
        {
            mCodeBackward = AM_MODE_QUIT;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_SWITCH)
        {
            mIsSwitchMode = true;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Cancel]
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel") && !mIsFirstTick)
    {
        mCodeBackward = AM_MODE_NONE;
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update. If we passed the cancel check, unset the first-tick flag.
    mIsFirstTick = false;
    return true;
}
void MonoUIBase::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;

    //--Run timers.
    mVisibilityTimer --;
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Unset this flag.
    mDontShowHighlight = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIBase::RenderPieces(float pVisAlpha)
{
    ///--[Setup]
    StarlightColor::ClearMixer();

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Render Switch Mode]
    RenderMainMenuSwitch(pVisAlpha);

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);

    ///--[Clean]
    StarlightColor::ClearMixer();
}
void MonoUIBase::RenderLft(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);

    ///--[Execution]
    //--Frame.
    MonoImages.rFramesLft->Draw();

    //--Options grid is on the left frame in the Monoceros menu.
    RenderGrid(pVisAlpha);

    //--Render the name of the selected entry.
    RenderGridEntryName(GetPackageAtCursor(), pVisAlpha);

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Clean]
    AutoPieceClose();
}
void MonoUIBase::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    MonoImages.rTitleBack->Draw();

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cColorAlpha);
    MonoImages.rDoubleHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 42.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextMainMenu);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    AutoPieceClose();
}
void MonoUIBase::RenderRgt(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Execution]
    //--Constants.
    float cAdamIcoL =  689.0f;
    float cAdamTtlL =  727.0f;
    float cAdamValL =  880.0f;
    float cAdamIcoR =  929.0f;
    float cAdamTtlR =  967.0f;
    float cAdamValR = 1120.0f;
    float cAdamIcoY1 = 338.0f;
    float cAdamIcoY2 = 394.0f;
    float cAdamIcoY3 = 450.0f;
    float cAdamTxtY1 = 323.0f;
    float cAdamTxtY2 = 382.0f;
    float cAdamTxtY3 = 438.0f;

    //--Frame.
    MonoImages.rFramesRgt->Draw();

    //--Platina.
    mMonoSubtitleColor.SetAsMixerAlpha(cColorAlpha);
    MonoImages.rHeadingFont->DrawText(689.0f, 164.0f, 0, 1.0f, xrStringData->str.mTextPlatina);
    mMonoParagraphColor.SetAsMixerAlpha(cColorAlpha);
    MonoImages.rMainlineFont->DrawTextArgs(850.0f, 167.0f, 0, 1.0f, "%i", rInventory->GetPlatina());
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Adamantite.
    mMonoSubtitleColor.SetAsMixerAlpha(cColorAlpha);
    MonoImages.rHeadingFont->DrawText(689.0f, 276.0f, 0, 1.0f, xrStringData->str.mTextAdamantite);
    mMonoParagraphColor.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rAdamantiteImg[0]->Draw (    cAdamIcoL, cAdamIcoY1);
    MonoImages.rMainlineFont->DrawText(    cAdamTtlL, cAdamTxtY1,                      0, 1.0f, xrStringData->str.mTextPowder);
    MonoImages.rMainlineFont->DrawTextArgs(cAdamValL, cAdamTxtY1, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetCraftingCount(0));

    AdvImages.rAdamantiteImg[1]->Draw (    cAdamIcoR, cAdamIcoY1);
    MonoImages.rMainlineFont->DrawText(    cAdamTtlR, cAdamTxtY1,                      0, 1.0f, xrStringData->str.mTextFlakes);
    MonoImages.rMainlineFont->DrawTextArgs(cAdamValR, cAdamTxtY1, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetCraftingCount(1));

    AdvImages.rAdamantiteImg[2]->Draw (    cAdamIcoL, cAdamIcoY2);
    MonoImages.rMainlineFont->DrawText(    cAdamTtlL, cAdamTxtY2,                      0, 1.0f, xrStringData->str.mTextShards);
    MonoImages.rMainlineFont->DrawTextArgs(cAdamValL, cAdamTxtY2, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetCraftingCount(2));

    AdvImages.rAdamantiteImg[3]->Draw (    cAdamIcoR, cAdamIcoY2);
    MonoImages.rMainlineFont->DrawText(    cAdamTtlR, cAdamTxtY2,                      0, 1.0f, xrStringData->str.mTextPieces);
    MonoImages.rMainlineFont->DrawTextArgs(cAdamValR, cAdamTxtY2, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetCraftingCount(3));

    AdvImages.rAdamantiteImg[4]->Draw (    cAdamIcoL, cAdamIcoY3);
    MonoImages.rMainlineFont->DrawText(    cAdamTtlL, cAdamTxtY3,                      0, 1.0f, xrStringData->str.mTextChunks);
    MonoImages.rMainlineFont->DrawTextArgs(cAdamValL, cAdamTxtY3, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetCraftingCount(4));

    AdvImages.rAdamantiteImg[5]->Draw (    cAdamIcoR, cAdamIcoY3);
    MonoImages.rMainlineFont->DrawText(    cAdamTtlR, cAdamTxtY3,                      0, 1.0f, xrStringData->str.mTextOre);
    MonoImages.rMainlineFont->DrawTextArgs(cAdamValR, cAdamTxtY3, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetCraftingCount(5));
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    ///--[Clean]
    AutoPieceClose();
}
void MonoUIBase::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Fast-access pointers.
    AdvCombat *rCombat = AdvCombat::Fetch();
    int tPartySize = rCombat->GetActivePartyCount();

    ///--[Execution]
    //--Health blocks. Renders left to right. Capped to three characters.
    for(int i = 0; i < 3; i ++)
    {
        RenderMainCharacter(i, tPartySize);
    }

    //--Render the UIs above the characters as a group.
    for(int i = 0; i < 3; i ++)
    {
        RenderMainCharacterUI(i, tPartySize, cColorAlpha);
    }

    ///--[Clean]
    AutoPieceClose();
}
void MonoUIBase::RenderMainCharacter(int pSlot, int pPartySize)
{
    ///--[Documentation]
    //--Given a character slot, renders that character and their information. If there is no character in that party slot,
    //  does nothing.
    //--This UI is designed for exactly 4 character slots. In theory the code could just keep expanding to the left. Characters
    //  Also render in the rightmost slots possible based on the party size.
    //--Note that stencilling is expected to be on for the character but not the rest of the UI.
    if(pSlot < 0 || pSlot >= 4 || pPartySize < 1) return;

    //--Check if there is a character in this slot.
    AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(pSlot);
    if(!rCharacter) return;

    ///--[Position]
    //--Translate based on the party ordering. All of the parts are designed for the leftmost slot
    //  and must be translated over to the right.
    float cSpacingX = 253.0f;
    float tOffsetX = (pSlot * cSpacingX);
    glTranslatef(tOffsetX, 0.0f, 0.0f);

    ///--[Portrait]
    //--Renders without any masking.
    StarBitmap *rPortrait = rCharacter->GetCombatPortrait();
    if(rPortrait)
    {
        //--Check if the entity has a countermask.
        StarBitmap *rCountermask = rCharacter->GetCombatCounterMask();

        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rCharacter->GetUIRenderPosition(ACE_UI_INDEX_BASEMENU);
        glTranslatef(tRenderDim.mXCenter, tRenderDim.mYCenter, 0.0f);
        glScalef(0.75f, 0.75f, 1.0f);

        //--Normal rendering.
        if(!rCountermask)
        {
            //--Don't render over the footer.
            glEnable(GL_STENCIL_TEST);
            glColorMask(true, true, true, true);
            glDepthMask(true);
            glStencilMask(0xFF);
            glStencilFunc(GL_NOTEQUAL, AM_STENCIL_FOOTER, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

            //--Render.
            rPortrait->Draw();

            //--Clean.
            DisplayManager::DeactivateStencilling();
        }
        //--Countermask rendering.
        else
        {
            //--Render countermask to lock off pixels. The countermask renders the same code as the footer.
            glEnable(GL_STENCIL_TEST);
            glColorMask(false, false, false, false);
            glDepthMask(false);
            glStencilMask(0xFF);
            glStencilFunc(GL_ALWAYS, AM_STENCIL_FOOTER, 0xFF);
            glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
            rCountermask->Draw();

            //--Render the portrait.
            glColorMask(true, true, true, true);
            glDepthMask(true);
            glStencilMask(0xFF);
            glStencilFunc(GL_NOTEQUAL, AM_STENCIL_FOOTER, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            rPortrait->Draw();

            //--Clean.
            DisplayManager::DeactivateStencilling();
        }

        //--Clean.
        glScalef(1.0f / 0.75f, 1.0f / 0.75f, 1.0f);
        glTranslatef(-tRenderDim.mXCenter, -tRenderDim.mYCenter, 0.0f);
    }

    ///--[Clean]
    glTranslatef(tOffsetX * -1.0f, 0.0f, 0.0f);
}
void MonoUIBase::RenderMainCharacterUI(int pSlot, int pPartySize, float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the UI component of a character, including their HP and level.
    if(pSlot < 0 || pSlot >= 4 || pPartySize < 1) return;

    //--Check if there is a character in this slot.
    AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(pSlot);
    if(!rCharacter) return;

    ///--[Position]
    //--Translate based on the party ordering. All of the parts are designed for the leftmost slot
    //  and must be translated over to the right.
    float cSpacingX = 253.0f;
    float tOffsetX = (pSlot * cSpacingX);
    glTranslatef(tOffsetX, 0.0f, 0.0f);

    ///--[Static Piece]
    //--Render.
    MonoImages.rHealthFront->Draw(0.0f, 0.0f);

    ///--[Basic Data]
    //--Character's level.
    MonoImages.rHeadingFont->DrawTextArgs(526.0f, 711.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", rCharacter->GetLevel() + 1);

    //--Character's name.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, pVisAlpha);
    MonoImages.rHealthFont->DrawText(592.0f, 697.0f, 0, 1.0f, rCharacter->GetDisplayName());

    //--Character's HP value.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
    MonoImages.rHealthFont->DrawTextArgs(592.0f, 722.0f, 0, 1.0f, "%i", rCharacter->GetHealthMax());

    ///--[Clean]
    glTranslatef(tOffsetX * -1.0f, 0.0f, 0.0f);
}
void MonoUIBase::RenderGridEntryIcon(GridPackage *pEntry, float pVisAlpha)
{
    ///--[Documentation]
    //--Individual renderer for a grid element. Renders the associated icon. Same as the base version
    //  but also renders a backing button to show highlighting.
    if(!pEntry || !pEntry->mAlignments.rImage) return;

    ///--[Computations]
    //--Constants.
    float cCenterSize = 96.0f;

    //--Subroutine handles most of the work.
    float tXPos, tYPos, cScale, cBoostPct;
    if(!ResolveEntryRenderData(pEntry, tXPos, tYPos, cScale, cBoostPct, pVisAlpha)) return;

    //--Derived values.
    float cScaleInv = 1.0f / cScale;

    ///--[Position and Color]
    //--Color blending.
    float cColVal = 0.75f + (0.25f * cBoostPct);
    StarlightColor::SetMixer(cColVal, cColVal, cColVal, pVisAlpha);

    //--Position, scale.
    glTranslatef(tXPos, tYPos, 0.0f);
    glScalef(cScale, cScale, 1.0f);

    ///--[Rendering]
    //--Render icon. The alignment code is designed to allow larger images to partially render so as to meet
    //  sizing requirements on the grid.
    StarBitmap *rImage = pEntry->mAlignments.rImage;

    //--Resolve percentages.
    float cWid = rImage->GetWidthSafe();
    float cHei = rImage->GetHeightSafe();
    float cLft = pEntry->mAlignments.mImageX / cWid;
    float cTop = pEntry->mAlignments.mImageY / cHei;
    float cRgt = cLft + (pEntry->mAlignments.mImageW / cWid);
    float cBot = cTop + (pEntry->mAlignments.mImageH / cHei);

    //--Scale down.
    float cNeededScale = cCenterSize / pEntry->mAlignments.mImageW;
    float cNeededScaleInv = 1.0f / cNeededScale;

    //--X/Y Position.
    float cXPosition = pEntry->mAlignments.mOffsetX;
    float cYPosition = pEntry->mAlignments.mOffsetY;

    //--Bind the image.
    glTranslatef(cXPosition, cYPosition, 0.0f);
    glScalef(cNeededScale, cNeededScale, 1.0f);

    //--Also renders a backing piece.
    int tGridOption = GetGridCodeAtCursor();
    if(tGridOption == pEntry->mCode)
    {
        MonoImages.rButtonSel->Draw(-3, -3);
    }
    else
    {
        MonoImages.rButton->Draw();
    }

    //--Image.
    rImage->RenderPercent(rImage->GetXOffset() * -1.0f, rImage->GetYOffset() * -1.0f, cLft, cTop, cRgt, cBot);

    //--Clean.
    glScalef(cNeededScaleInv, cNeededScaleInv, 1.0f);
    glTranslatef(cXPosition * -1.0f, cYPosition * -1.0f, 0.0f);

    ///--[Clean]
    //--Unscale, unposition.
    glScalef(cScaleInv, cScaleInv, 1.0f);
    glTranslatef(-tXPos, -tYPos, 0.0f);

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
}
void MonoUIBase::RenderGridEntryName(GridPackage *pEntry, float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the name of the provided element. In Monoceros, this renders as a heading
    //  rather than on the object.
    if(!pEntry || !rFont_Name) return;

    //--Position.
    float cLft = 394.0f;
    float cTop = 150.0f;

    //--Color.
    mMonoTitleColor.SetAsMixerAlpha(pVisAlpha);

    //--Render.
    rFont_Name->DrawTextArgs(cLft, cTop, SUGARFONT_AUTOCENTER_X, 1.0f, pEntry->mLocalName);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
