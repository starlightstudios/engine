//--Base
#include "ActorEvent.h"

//--Classes
#include "AdventureLevel.h"
#include "RootEntity.h"
#include "TilemapActor.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "EntityManager.h"

///========================================== System ==============================================
ActorEvent::ActorEvent()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_EVENT_ACTOR;

    ///--[RootEvent]
    //--System
    //--Script Execution
    //--Data Storage

    ///--[ActorEvent]
    //--System
    mIsComplete = false;
    mInstructionType = AE_TYPE_NONE;

    //--Target
    mTargetID = 0;

    //--Data
    memset(&Data, 0, sizeof(Data));
}
ActorEvent::~ActorEvent()
{
}

///===================================== Property Queries =========================================
bool ActorEvent::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_EVENT_ROOT) return true;
    if(pType == POINTER_TYPE_EVENT_ACTOR) return true;
    return false;
}
bool ActorEvent::DisablesTimerAcceleration()
{
    return true;
}
uint32_t ActorEvent::GetSubjectID()
{
    return mTargetID;
}

///======================================== Manipulators ==========================================
void ActorEvent::SetTarget(uint32_t pID)
{
    mTargetID = pID;
}
void ActorEvent::SetTargetByName(const char *pName)
{
    //--Flags.
    mTargetID = 0;

    //--Locate the subject.
    RootEntity *rPotentialSubject = EntityManager::Fetch()->GetEntity(pName);
    if(!rPotentialSubject || !rPotentialSubject->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        DebugManager::ForcePrint("ActorEvent:SetTargetByName - Error, unable to locate TilemapActor %s.\n", pName);
        return;
    }

    //--Set flags.
    mTargetID = rPotentialSubject->GetID();
}
void ActorEvent::SetFaceTowards(int pX, int pY)
{
    mInstructionType = AE_TYPE_FACE;
    Data.Face.mAmountX = pX;
    Data.Face.mAmountY = pY;
    Data.Face.mFacingTarget[0] = '\0';
}
void ActorEvent::SetFaceTowards(const char *pTargetName)
{
    mInstructionType = AE_TYPE_FACE;
    Data.Face.mAmountX = 0.0f;
    Data.Face.mAmountY = 0.0f;
    strncpy(Data.Face.mFacingTarget, pTargetName, 31);
}
void ActorEvent::SetFaceTowardsPos(float pX, float pY)
{
    mInstructionType = AE_TYPE_FACEPOS;
    Data.FacePos.mXPos = pX;
    Data.FacePos.mYPos = pY;
}
void ActorEvent::SetTeleportTo(int pX, int pY)
{
    mInstructionType = AE_TYPE_TELEPORTTO;
    Data.TeleportTo.mTargetX = pX;
    Data.TeleportTo.mTargetY = pY;
    Data.TeleportTo.mTargetZ = -1;
}
void ActorEvent::SetTeleportTo(int pX, int pY, int pZ)
{
    mInstructionType = AE_TYPE_TELEPORTTO;
    Data.TeleportTo.mTargetX = pX;
    Data.TeleportTo.mTargetY = pY;
    Data.TeleportTo.mTargetZ = pZ;
}
void ActorEvent::SetMoveTo(int pX, int pY)
{
    mInstructionType = AE_TYPE_MOVETO;
    Data.MoveTo.mTargetX = pX;
    Data.MoveTo.mTargetY = pY;
    Data.MoveTo.mSpeedModifier = -1.0f;
}
void ActorEvent::SetMoveTo(int pX, int pY, float pSpeed)
{
    mInstructionType = AE_TYPE_MOVETO;
    Data.MoveTo.mTargetX = pX;
    Data.MoveTo.mTargetY = pY;
    Data.MoveTo.mSpeedModifier = pSpeed;
}
void ActorEvent::SetMoveToFacing(int pX, int pY, int pFaceX, int pFaceY)
{
    mInstructionType = AE_TYPE_MOVEWHILEFACING;
    Data.MoveToFacing.mTargetX = pX;
    Data.MoveToFacing.mTargetY = pY;
    Data.MoveToFacing.mFaceX = pFaceX;
    Data.MoveToFacing.mFaceY = pFaceY;
    Data.MoveToFacing.mSpeedModifier = -1.0f;
}
void ActorEvent::SetMoveToFacing(int pX, int pY, int pFaceX, int pFaceY, float pSpeed)
{
    mInstructionType = AE_TYPE_MOVEWHILEFACING;
    Data.MoveToFacing.mTargetX = pX;
    Data.MoveToFacing.mTargetY = pY;
    Data.MoveToFacing.mFaceX = pFaceX;
    Data.MoveToFacing.mFaceY = pFaceY;
    Data.MoveToFacing.mSpeedModifier = pSpeed;
}
void ActorEvent::SetMoveAmount(int pX, int pY)
{
    mInstructionType = AE_TYPE_MOVEFIXED;
    Data.MoveFixed.mInitialized = false;
    Data.MoveFixed.mAmountX = pX;
    Data.MoveFixed.mAmountY = pY;
}
void ActorEvent::SetStopMove()
{
    mInstructionType = AE_TYPE_STOPMOVEMENT;
}
void ActorEvent::SetChangeSpecialFrame(const char *pFrameName)
{
    if(!pFrameName) return;
    mInstructionType = AE_TYPE_CHANGESPECIALFRAME;
    strncpy(Data.SpecialFrame.mFrameName, pFrameName, 31);
}
void ActorEvent::SetFlashwhite(const char *pSpecialFrame)
{
    mInstructionType = AE_TYPE_FLASHWHITE;
    Data.Flashwhite.mIsRunning = false;
    strcpy(Data.Flashwhite.mFlashwhiteName, "NULL");
    if(pSpecialFrame) strcpy(Data.Flashwhite.mFlashwhiteName, pSpecialFrame);
}
void ActorEvent::SetFlashwhiteQuickly(const char *pSpecialFrame)
{
    mInstructionType = AE_TYPE_FLASHWHITE_QUICKLY;
    Data.Flashwhite.mIsRunning = false;
    strcpy(Data.Flashwhite.mFlashwhiteName, "NULL");
    if(pSpecialFrame) strcpy(Data.Flashwhite.mFlashwhiteName, pSpecialFrame);
}
void ActorEvent::SetResetMoveTimer()
{
    mInstructionType = AE_TYPE_RESETMOVETIMER;
}
void ActorEvent::SetAutoDespawn()
{
    mInstructionType = AE_TYPE_AUTODESPAWN;
}
void ActorEvent::SetPartyMerge()
{
    mInstructionType = AE_TYPE_PARTYMERGE;
    Data.PartyMerge.mTimer = 81;
}
void ActorEvent::SetPartyAutofold()
{
    mInstructionType = AE_TYPE_PARTYAUTOFOLD;
}
void ActorEvent::SetDelayMoveTimer(int pTicks)
{
    mInstructionType = AE_TYPE_DELAYMOVETIMER;
    Data.DelayMoveTimer.mTimer = pTicks;
}
void ActorEvent::SetChangeYOffset(float pOffset)
{
    mInstructionType = AE_TYPE_CHANGEYOFFSET;
    Data.ChangeYOffset.mOffset = pOffset;
}
void ActorEvent::SetDoNothing(int pTicks)
{
    mInstructionType = AE_TYPE_DONOTHING;
    Data.DoNothing.mTimer = pTicks;
}
void ActorEvent::SetJumpTo(int pX, int pY, int pTicks)
{
    mInstructionType = AE_TYPE_JUMPTO;
    Data.JumpTo.mInittedJump = false;
    Data.JumpTo.mTimer = 0;
    Data.JumpTo.mTimerMax = pTicks;
    Data.JumpTo.mStartX = 0;
    Data.JumpTo.mStartY = 0;
    Data.JumpTo.mTargetX = pX;
    Data.JumpTo.mTargetY = pY;
}
void ActorEvent::SetChangeSpecialAnim(const char *pAnimName)
{
    if(!pAnimName) return;
    mInstructionType = AE_TYPE_CHANGESPECIALANIM;
    strncpy(Data.ChangeAnim.mAnimName, pAnimName, 31);
}

///======================================== Core Methods ==========================================
bool ActorEvent::IsComplete()
{
    //--Returns whether or not the event is complete. Immediately completes if it has no target.
    if(!mTargetID) return true;

    //--Event will store its completion state during the Update() call.
    return mIsComplete;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void ActorEvent::Update()
{
    //--Handles the event update, or does nothing if there's no target ID.
    //fprintf(stderr, "Running instruction %p %i\n", this, Global::Shared()->gTicksElapsed);
    if(!mTargetID) return;

    //--Locate the TilemapActor in question.
    TilemapActor *rSubject = (TilemapActor *)EntityManager::Fetch()->GetEntityI(mTargetID);
    if(!rSubject || !rSubject->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        mIsComplete = true;
        return;
    }

    //--If the actor has already handle an instruction this tick, we end but don't flag for completion.
    //  This means multiple instructions can be queued but the actor will only execute the first one
    //  found until it expires.
    if(!rSubject->CanHandleInstructions(this)) return;
    //fprintf(stderr, " Yes!\n");

    //--Face a given direction.
    if(mInstructionType == AE_TYPE_FACE)
    {
        //--Flag.
        mIsComplete = true;

        //--Given a target's name, modify the facing amounts to match that target.
        if(Data.Face.mFacingTarget[0] != '\0')
        {
            //--Check to make sure the target actually exists.
            TilemapActor *rTarget = (TilemapActor *)EntityManager::Fetch()->GetEntity(Data.Face.mFacingTarget);
            if(rTarget)
            {
                //--Get the best facing possible.
                int tBestFacing = TilemapActor::GetBestFacing(rSubject->GetWorldX(), rSubject->GetWorldY(), rTarget->GetWorldX(), rTarget->GetWorldY());
                rSubject->SetFacing(tBestFacing);
            }
        }
        //--Otherwise, use hard numbers.
        else
        {
            //--Must be west...
            if(Data.Face.mAmountX < 0.0f)
            {
                //--Northwest.
                if(Data.Face.mAmountY < 0.0f)
                {
                    rSubject->SetFacing(TA_DIR_NW);
                }
                //--Straight west.
                else if(Data.Face.mAmountY == 0.0f)
                {
                    rSubject->SetFacing(TA_DIR_WEST);
                }
                //--Southwest.
                else
                {
                    rSubject->SetFacing(TA_DIR_SW);
                }
            }
            //--Must be North or South.
            else if(Data.Face.mAmountX == 0.0f)
            {
                //--Straight north.
                if(Data.Face.mAmountY < 0.0f)
                {
                    rSubject->SetFacing(TA_DIR_NORTH);
                }
                //--Double-zero. Defaults to south.
                else if(Data.Face.mAmountY == 0.0f)
                {
                    rSubject->SetFacing(TA_DIR_SOUTH);
                }
                //--Straight south.
                else
                {
                    rSubject->SetFacing(TA_DIR_SOUTH);
                }
            }
            //--Must be east...
            else
            {
                //--Northeast.
                if(Data.Face.mAmountY < 0.0f)
                {
                    rSubject->SetFacing(TA_DIR_NE);
                }
                //--Straight east.
                else if(Data.Face.mAmountY == 0.0f)
                {
                    rSubject->SetFacing(TA_DIR_EAST);
                }
                //--Southeast.
                else
                {
                    rSubject->SetFacing(TA_DIR_SE);
                }
            }
        }

        //--Always counts as an instruction.
        rSubject->FlagHandledInstruction(this);
        rSubject->SetOverrideDepth(-2.0f);
    }
    //--Teleport to a given location.
    else if(mInstructionType == AE_TYPE_TELEPORTTO)
    {
        mIsComplete = true;
        rSubject->SetPositionByPixel(Data.TeleportTo.mTargetX, Data.TeleportTo.mTargetY);
        if(Data.TeleportTo.mTargetZ != -1) rSubject->SetCollisionDepth(Data.TeleportTo.mTargetZ);
        rSubject->FlagHandledInstruction(this);
        rSubject->SetOverrideDepth(-2.0f);
    }
    //--Move to a given location.
    else if(mInstructionType == AE_TYPE_MOVETO)
    {
        mIsComplete = rSubject->HandleMoveTo(Data.MoveTo.mTargetX, Data.MoveTo.mTargetY, Data.MoveTo.mSpeedModifier);
        rSubject->FlagHandledInstruction(this);
        rSubject->SetOverrideDepth(-2.0f);
        if(mIsComplete)
        rSubject->SetRemainders(0.0f, 0.0f);
    }
    //--Move in a given direction.
    else if(mInstructionType == AE_TYPE_MOVEFIXED)
    {
        if(!Data.MoveFixed.mInitialized)
        {
            Data.MoveFixed.mInitialized = true;
            rSubject->SetRemainders(0.0f, 0.0f);
        }

        mIsComplete = rSubject->HandleMoveAmount(Data.MoveFixed.mAmountX, Data.MoveFixed.mAmountY);
        rSubject->FlagHandledInstruction(this);
        rSubject->SetOverrideDepth(-2.0f);
    }
    //--Stop movement. Completes immediately.
    else if(mInstructionType == AE_TYPE_STOPMOVEMENT)
    {
        mIsComplete = true;
        rSubject->StopMoving();
        rSubject->FlagHandledInstruction(this);
        rSubject->SetOverrideDepth(-2.0f);
    }
    //--Change to a special frame. Completes immediately.
    else if(mInstructionType == AE_TYPE_CHANGESPECIALFRAME)
    {
        mIsComplete = true;
        rSubject->ActivateSpecialFrame(Data.SpecialFrame.mFrameName);
        rSubject->SetOverrideDepth(-2.0f);
    }
    //--Begin the flashwhite sequence. Completes when the actor is no longer flashing white.
    else if(mInstructionType == AE_TYPE_FLASHWHITE)
    {
        //--First tick:
        if(!Data.Flashwhite.mIsRunning)
        {
            Data.Flashwhite.mIsRunning = true;
            rSubject->SetReserveFrame(Data.Flashwhite.mFlashwhiteName);
            rSubject->ActivateFlashwhite();
            rSubject->FlagHandledInstruction(this);
            rSubject->SetOverrideDepth(-2.0f);
        }

        //--Check for completion.
        if(!rSubject->IsFlashingWhite()) mIsComplete = true;
    }
    //--Begin the flashwhite sequence. Completes immediately. Used for specific timing in scenes like warping/transforming.
    else if(mInstructionType == AE_TYPE_FLASHWHITE_QUICKLY)
    {
        //--Send.
        rSubject->SetReserveFrame(Data.Flashwhite.mFlashwhiteName);
        rSubject->ActivateFlashwhite();
        rSubject->FlagHandledInstruction(this);
        rSubject->SetOverrideDepth(-2.0f);

        //--Completes immediately.
        mIsComplete = true;
    }
    //--Resets the movement timer and completes immediately.
    else if(mInstructionType == AE_TYPE_RESETMOVETIMER)
    {
        mIsComplete = true;
        rSubject->SetMoveTimer(0);
        rSubject->SetAutoAnimateFastFlag(true);
    }
    //--The entity will automatically despawn when it completes its next animation cycle. MUST BE AUTO-ANIMATING!
    else if(mInstructionType == AE_TYPE_AUTODESPAWN)
    {
        mIsComplete = true;
        rSubject->SetAutoDespawn(true);
    }
    //--Moves the subject while forcing them to face a specific direction.
    else if(mInstructionType == AE_TYPE_MOVEWHILEFACING)
    {
        mIsComplete = rSubject->HandleMoveTo(Data.MoveToFacing.mTargetX, Data.MoveToFacing.mTargetY, Data.MoveToFacing.mSpeedModifier);

        //--Must be west...
        if(Data.MoveToFacing.mFaceX < 0.0f)
        {
            //--Northwest.
            if(Data.MoveToFacing.mFaceY < 0.0f)
            {
                rSubject->SetFacing(TA_DIR_NW);
            }
            //--Straight west.
            else if(Data.MoveToFacing.mFaceY == 0.0f)
            {
                rSubject->SetFacing(TA_DIR_WEST);
            }
            //--Southwest.
            else
            {
                rSubject->SetFacing(TA_DIR_SW);
            }
        }
        //--Must be North or South.
        else if(Data.MoveToFacing.mFaceX == 0.0f)
        {
            //--Straight north.
            if(Data.MoveToFacing.mFaceY < 0.0f)
            {
                rSubject->SetFacing(TA_DIR_NORTH);
            }
            //--Double-zero. Defaults to south.
            else if(Data.MoveToFacing.mFaceY == 0.0f)
            {
                rSubject->SetFacing(TA_DIR_SOUTH);
            }
            //--Straight south.
            else
            {
                rSubject->SetFacing(TA_DIR_SOUTH);
            }
        }
        //--Must be east...
        else
        {
            //--Northeast.
            if(Data.MoveToFacing.mFaceY < 0.0f)
            {
                rSubject->SetFacing(TA_DIR_NE);
            }
            //--Straight east.
            else if(Data.MoveToFacing.mFaceY == 0.0f)
            {
                rSubject->SetFacing(TA_DIR_EAST);
            }
            //--Southeast.
            else
            {
                rSubject->SetFacing(TA_DIR_SE);
            }
        }
        rSubject->FlagHandledInstruction(this);
        rSubject->SetOverrideDepth(-2.0f);
    }
    //--Party merge.
    else if(mInstructionType == AE_TYPE_PARTYMERGE)
    {
        Data.PartyMerge.mTimer --;
        if(Data.PartyMerge.mTimer < 1)
        {
            mIsComplete = true;
        }
        else
        {
            bool tAnyoneMoved = AdventureLevel::Fetch()->MovePartyMembers();
            if(!tAnyoneMoved) mIsComplete = true;
        }
    }
    //--Autofold.
    else if(mInstructionType == AE_TYPE_PARTYAUTOFOLD)
    {
        if(AdventureLevel::Fetch()->AutofoldPartyMembers())
        {
            mIsComplete = true;
        }
    }
    //--Sets the move timer for this entity to be negative. The entity does not display until its
    //  movement timer is positive again.
    else if(mInstructionType == AE_TYPE_DELAYMOVETIMER)
    {
        rSubject->SetNegativeMoveTimer(Data.DelayMoveTimer.mTimer);
        mIsComplete = true;
    }
    //--Sets the Y offset for the given entity.
    else if(mInstructionType == AE_TYPE_CHANGEYOFFSET)
    {
        rSubject->SetYVerticalOffset(Data.ChangeYOffset.mOffset);
        mIsComplete = true;
    }
    //--Do nothing until the timer reaches zero. Blocks other events on the same entity from firing. Used to stagger
    //  or randomize movement without breaking flow.
    else if(mInstructionType == AE_TYPE_DONOTHING)
    {
        Data.DoNothing.mTimer --;
        if(Data.DoNothing.mTimer < 1) mIsComplete = true;
    }
    //--"Jump" to the target position, which is actually a series of teleports that ignores collisions.
    else if(mInstructionType == AE_TYPE_JUMPTO)
    {
        //--Zeroth tick: Get the position of the actor.
        if(!Data.JumpTo.mInittedJump)
        {
            Data.JumpTo.mInittedJump = true;
            Data.JumpTo.mStartX = rSubject->GetWorldX();
            Data.JumpTo.mStartY = rSubject->GetWorldY();

            //--Check if the start and end points are the same. If so, complete immediately.
            float cXDif = fabsf(Data.JumpTo.mStartX - Data.JumpTo.mTargetX);
            float cYDif = fabsf(Data.JumpTo.mStartY - Data.JumpTo.mTargetY);
            if(cXDif + cYDif < 0.50f)
            {
                //fprintf(stderr, "Stopped due to ultra short move.\n");
                mIsComplete = true;
                return;
            }
        }

        //--Otherwise, run it.
        Data.JumpTo.mTimer ++;
        rSubject->HandleJumpTo(Data.JumpTo.mStartX, Data.JumpTo.mStartY, Data.JumpTo.mTargetX, Data.JumpTo.mTargetY, Data.JumpTo.mTimer, Data.JumpTo.mTimerMax);
        rSubject->FlagHandledInstruction(this);

        //--Ending case.
        if(Data.JumpTo.mTimer >= Data.JumpTo.mTimerMax) mIsComplete = true;
    }
    //--Change to a special anim. Completes immediately.
    else if(mInstructionType == AE_TYPE_CHANGESPECIALANIM)
    {
        mIsComplete = true;
        rSubject->ActivateSpecialFrameAnim(Data.ChangeAnim.mAnimName);
    }
    //--Face a specific position. Completes immediately.
    else if(mInstructionType == AE_TYPE_FACEPOS)
    {
        mIsComplete = true;
        rSubject->SetFacingToPoint(Data.FacePos.mXPos, Data.FacePos.mYPos);
    }
    //--Unknown type.
    else
    {
        mIsComplete = true;
    }
}

///========================================== File I/O ============================================
void ActorEvent::HandleDiagnostics()
{
    //--Prints diagnostics information to the visual renderer in the DebugManager.
    DebugManager::RenderVisualTrace("ACTOR EV SUB %i TRG %i", mInstructionType, mTargetID);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void ActorEvent::HookToLuaState(lua_State *pLuaState)
{
    /* ActorEvent_SetProperty("Subject ID", iID)
       ActorEvent_SetProperty("Subject Name", sName)
       ActorEvent_SetProperty("Face", iDirectionX, iDirectionY)
       ActorEvent_SetProperty("Face", sEntityName)
       ActorEvent_SetProperty("Face Pos", fTargetX, fTargetY)
       ActorEvent_SetProperty("Move To", iTargetX, iTargetY)
       ActorEvent_SetProperty("Move To", iTargetX, iTargetY, fSpeed)
       ActorEvent_SetProperty("Move To While Facing", iTargetX, iTargetY, iFaceX, iFaceY)
       ActorEvent_SetProperty("Move To While Facing", iTargetX, iTargetY, iFaceX, iFaceY, fSpeed)
       ActorEvent_SetProperty("Move Amount", iDistanceX, iDistanceY)
       ActorEvent_SetProperty("Stop Moving")
       ActorEvent_SetProperty("Special Frame", sFrameName)
       ActorEvent_SetProperty("Special Anim", sAnimName)
       ActorEvent_SetProperty("Flashwhite")
       ActorEvent_SetProperty("Flashwhite", sSpecialFrame)
       ActorEvent_SetProperty("Reset Move Timer")
       ActorEvent_SetProperty("Auto Despawn")
       ActorEvent_SetProperty("Merge Party")
       ActorEvent_SetProperty("Autofold Party")
       ActorEvent_SetProperty("Negative Move Timer", iTicks)
       ActorEvent_SetProperty("Change Y Offset", fOffset)
       ActorEvent_SetProperty("Do Nothing", iTicks)
       Sets the given property in the active ActorEvent or derived type. */
    lua_register(pLuaState, "ActorEvent_SetProperty", &Hook_ActorEvent_SetProperty);

    /* ActorEvent_GetProperty("Subject ID") (1 integer)
       Returns the requested property from the active ActorEvent or derived type. */
    lua_register(pLuaState, "ActorEvent_GetProperty", &Hook_ActorEvent_GetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_ActorEvent_SetProperty(lua_State *L)
{
    //ActorEvent_SetProperty("Subject ID", iID)
    //ActorEvent_SetProperty("Subject Name", sName)
    //ActorEvent_SetProperty("Face", iDirectionX, iDirectionY)
    //ActorEvent_SetProperty("Face", sEntityName)
    //ActorEvent_SetProperty("Face Pos", fTargetX, fTargetY)
    //ActorEvent_SetProperty("Teleport To", iTargetX, iTargetY)
    //ActorEvent_SetProperty("Teleport To", iTargetX, iTargetY, iTargetZ)
    //ActorEvent_SetProperty("Move To", iTargetX, iTargetY)
    //ActorEvent_SetProperty("Move To", iTargetX, iTargetY, fSpeed)
    //ActorEvent_SetProperty("Move To While Facing", iTargetX, iTargetY, iFaceX, iFaceY)
    //ActorEvent_SetProperty("Move To While Facing", iTargetX, iTargetY, iFaceX, iFaceY, fSpeed)
    //ActorEvent_SetProperty("Move Amount", iDistanceX, iDistanceY)
    //ActorEvent_SetProperty("Stop Moving")
    //ActorEvent_SetProperty("Special Frame", sFrameName)
    //ActorEvent_SetProperty("Special Anim", sAnimName)
    //ActorEvent_SetProperty("Flashwhite")
    //ActorEvent_SetProperty("Flashwhite", sSpecialFrameName)
    //ActorEvent_SetProperty("Reset Move Timer")
    //ActorEvent_SetProperty("Auto Despawn")
    //ActorEvent_SetProperty("Merge Party")
    //ActorEvent_SetProperty("Autofold Party")
    //ActorEvent_SetProperty("Negative Move Timer", iTicks)
    //ActorEvent_SetProperty("Change Y Offset", fOffset)
    //ActorEvent_SetProperty("Do Nothing", iTicks)
    //ActorEvent_SetProperty("Jump To", iTargetX, iTargetY, iTicks)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ActorEvent_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Instantiated Cases]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    ActorEvent *rEvent = (ActorEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_ACTOR)) return LuaTypeError("ActorEvent_SetProperty", L);

    //--ID of the Actor to monitor.
    if(!strcasecmp(rSwitchType, "Subject ID") && tArgs == 2)
    {
        rEvent->SetTarget(lua_tointeger(L, 2));
    }
    //--Name of the Actor to monitor. Will attempt to resolve.
    else if(!strcasecmp(rSwitchType, "Subject Name") && tArgs == 2)
    {
        rEvent->SetTargetByName(lua_tostring(L, 2));
    }
    //--Faces a given direction, using the same logic as Move Amount. Diagonals can be face by using both coordinates.
    else if(!strcasecmp(rSwitchType, "Face") && tArgs == 3)
    {
        rEvent->SetFaceTowards(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Faces a given entity.
    else if(!strcasecmp(rSwitchType, "Face") && tArgs == 2)
    {
        rEvent->SetFaceTowards(lua_tostring(L, 2));
    }
    //--Faces a given position.
    else if(!strcasecmp(rSwitchType, "Face Pos") && tArgs == 3)
    {
        rEvent->SetFaceTowardsPos(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Instantly teleport to the given location.
    else if(!strcasecmp(rSwitchType, "Teleport To") && tArgs == 3)
    {
        rEvent->SetTeleportTo(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Instantly teleport to the given location with a depth.
    else if(!strcasecmp(rSwitchType, "Teleport To") && tArgs == 4)
    {
        rEvent->SetTeleportTo(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Moves to a given location.
    else if(!strcasecmp(rSwitchType, "Move To") && tArgs == 3)
    {
        rEvent->SetMoveTo(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Moves to a given location, with the given speed. Default speed is 1.25f, pass -1.0 to use whatever the default is.
    else if(!strcasecmp(rSwitchType, "Move To") && tArgs == 4)
    {
        rEvent->SetMoveTo(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Moves to a given location, with the given speed, while facing the given direction. Speed is default.
    else if(!strcasecmp(rSwitchType, "Move To While Facing") && tArgs == 5)
    {
        rEvent->SetMoveToFacing(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Moves to a given location, with the given speed, while facing the given direction. Speed is mandated.
    else if(!strcasecmp(rSwitchType, "Move To While Facing") && tArgs == 6)
    {
        rEvent->SetMoveToFacing(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Moves by a given distance.
    else if(!strcasecmp(rSwitchType, "Move Amount") && tArgs == 3)
    {
        rEvent->SetMoveAmount(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Actor stops their movement animation.
    else if(!strcasecmp(rSwitchType, "Stop Moving") && tArgs == 1)
    {
        rEvent->SetStopMove();
    }
    //--Actor uses a special frame.
    else if(!strcasecmp(rSwitchType, "Special Frame") && tArgs == 2)
    {
        rEvent->SetChangeSpecialFrame(lua_tostring(L, 2));
    }
    //--Actor uses a special animation.
    else if(!strcasecmp(rSwitchType, "Special Anim") && tArgs == 2)
    {
        rEvent->SetChangeSpecialAnim(lua_tostring(L, 2));
    }
    //--Actor flashes to white and back down.
    else if(!strcasecmp(rSwitchType, "Flashwhite") && tArgs == 1)
    {
        rEvent->SetFlashwhite("NULL");
    }
    //--Actor flashes to white and back down, switches to special frame.
    else if(!strcasecmp(rSwitchType, "Flashwhite") && tArgs == 2)
    {
        rEvent->SetFlashwhite(lua_tostring(L, 2));
    }
    //--Actor flashes to white and back down. Does not block other events.
    else if(!strcasecmp(rSwitchType, "Flashwhite Quickly") && tArgs == 1)
    {
        rEvent->SetFlashwhiteQuickly("NULL");
    }
    //--Actor flashes to white and back down, switches to special frame. Does not block other events.
    else if(!strcasecmp(rSwitchType, "Flashwhite Quickly") && tArgs == 2)
    {
        rEvent->SetFlashwhiteQuickly(lua_tostring(L, 2));
    }
    //--Zeroes the Actor's movement timer.
    else if(!strcasecmp(rSwitchType, "Reset Move Timer") && tArgs == 1)
    {
        rEvent->SetResetMoveTimer();
    }
    //--Actor will despawn when its animation completes. Must be auto-animating.
    else if(!strcasecmp(rSwitchType, "Auto Despawn") && tArgs == 1)
    {
        rEvent->SetAutoDespawn();
    }
    //--Actor waits until the whole party is merged onto the leader.
    else if(!strcasecmp(rSwitchType, "Merge Party") && tArgs == 1)
    {
        rEvent->SetPartyMerge();
    }
    //--Party moves onto the player character dynamically.
    else if(!strcasecmp(rSwitchType, "Autofold Party") && tArgs == 1)
    {
        rEvent->SetPartyAutofold();
    }
    //--Auto-animating entities will not appear or start animating until this many ticks have elapsed.
    else if(!strcasecmp(rSwitchType, "Negative Move Timer") && tArgs == 2)
    {
        rEvent->SetDelayMoveTimer(lua_tointeger(L, 2));
    }
    //--Modifies the local Y offset for the entity.
    else if(!strcasecmp(rSwitchType, "Change Y Offset") && tArgs == 2)
    {
        rEvent->SetChangeYOffset(lua_tonumber(L, 2));
    }
    //--Character does nothing for a few ticks.
    else if(!strcasecmp(rSwitchType, "Do Nothing") && tArgs == 2)
    {
        rEvent->SetDoNothing(lua_tointeger(L, 2));
    }
    //--Actor jumps to the target position.
    else if(!strcasecmp(rSwitchType, "Jump To") && tArgs == 4)
    {
        rEvent->SetJumpTo(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tointeger(L, 4));
    }
    //--Error.
    else
    {
        LuaPropertyError("ActorEvent_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_ActorEvent_GetProperty(lua_State *L)
{
    //ActorEvent_GetProperty("Subject ID") (1 integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ActorEvent_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    ActorEvent *rEvent = (ActorEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_ACTOR)) return LuaTypeError("ActorEvent_GetProperty", L);

    //--ID of the subject. Will return 0 if there's no subject.
    if(!strcasecmp(rSwitchType, "Subject ID") && tArgs == 1)
    {
        lua_pushinteger(L, rEvent->GetSubjectID());
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("ActorEvent_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
