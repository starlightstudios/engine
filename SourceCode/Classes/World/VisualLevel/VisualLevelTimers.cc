//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "VisualRoom.h"
#include "WorldContainer.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

//--At the end of a logic tick but before rendering, we check which objects have recently entered or exited the
//  visibility field. Objects which have appeared fade in, objects which have despawned or left the room fade out.
//  This routine handles that, but since there are several types of objects that can be on the field, it can be
//  quite convoluted. So, it has its own file.

void VisualLevel::UpdateVisiblityTimers()
{
    ///--[Documentation]
    //--Subroutine, handles visibility timers for all entities (including ones we can't see). When an entity
    //  leaves the room, it resets its visibility timer to 0. Entities who are in the room increment their timers.
    //--We determine which entities are outside the room by comparing the last tick they had their visibility incremented.
    //  If they weren't incremented this tick, they decrement. The current tick is stored in the Global structure.

    ///--[Increment Timers for Visible Objects]
    //--If we're moving, or if there's no active Player, then nothing is visible.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!mMoveToNodeID && rPlayerActor && !mIsTeleporting)
    {
        //--We need a room. If the acting entity is not in a room, then nobody will increment their timers.
        PandemoniumRoom *rActiveRoom = rPlayerActor->GetCurrentRoom();
        if(rActiveRoom)
        {
            //--Entity list.
            StarLinkedList *rEntityList = rActiveRoom->GetEntityList();

            //--Increment the timer for every visible Actor. We don't care if this includes the player.
            Actor *rActor = (Actor *)rEntityList->PushIterator();
            while(rActor)
            {
                rActor->IncrementVisible();
                rActor = (Actor *)rEntityList->AutoIterate();
            }

            //--Loop through the inventory entities.
            for(int i = 0; i < rActiveRoom->GetCompressedInventorySize(); i ++)
            {
                //--Get the item in the slot.
                InventoryPack *rItemPack = rActiveRoom->GetItemPack(i);
                if(!rItemPack || !rItemPack->rItem) continue;

                //--Increment.
                rItemPack->mLastSetVisible = Global::Shared()->gTicksElapsed;
                if(rItemPack->mIsVisibleTimer < VL_ENTITY_FADE_TICKS) rItemPack->mIsVisibleTimer ++;
            }

            //--Loop through the WorldContainers.
            StarLinkedList *rContainerList = rActiveRoom->GetContainerList();
            WorldContainer *rContainer = (WorldContainer *)rContainerList->PushIterator();
            while(rContainer)
            {
                //--Increment.
                rContainer->IncrementVisible();

                //--Next.
                rContainer = (WorldContainer *)rContainerList->AutoIterate();
            }
        }
    }

    ///--[Decrement Timers for Actors]
    //--Decrement the timers for all Actors which the player can no longer see. Actors can move between rooms, so
    //  every Actor needs to be checked.
    StarLinkedList *tActorList = EntityManager::Fetch()->GetListContaining(POINTER_TYPE_ACTOR);
    Actor *rActor = (Actor *)tActorList->PushIterator();
    while(rActor)
    {
        //--This routine does nothing if we updated this tick. No need for tests!
        rActor->DecrementVisible();

        //--If the Actor just became invisible, then we need to store their rendering information. They will
        //  render in addition to the active entities until they are completed faded out.
        if(rActor->JustBecameInvisible() && rActor->mLastRenderSlot != -1 && !IsEntityOnExtraRenderList(rActor->GetID()))
        {
            SetMemoryData(__FILE__, __LINE__);
            ExtraEntityRenderPack *nRenderPack = (ExtraEntityRenderPack *)starmemoryalloc(sizeof(ExtraEntityRenderPack));
            memset(nRenderPack, 0, sizeof(ExtraEntityRenderPack));
            nRenderPack->mParentID = rActor->GetID();
            nRenderPack->mRenderSlot = rActor->mLastRenderSlot;
            nRenderPack->mRenderTicksLeft = VL_ENTITY_FADE_TICKS;
            nRenderPack->rRenderImg = rActor->GetActiveImage();
            mExtraRenderEntitiesList->AddElement("X", nRenderPack, &FreeThis);
        }

        //--Next.
        rActor = (Actor *)tActorList->AutoIterate();
    }

    //--Clean up.
    delete tActorList;

    ///--[Decrement Timers for Items/Containers]
    //--Any item which is in a room the player cannot see is invisible. Items that are removed by picking them
    //  up or using them are handled elsewhere.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        ///--[Basics]
        //--Invalid check.
        if(!mRooms[i]) continue;

        ///--[Items]
        //--Decrement all the item packages in the room.
        for(int p = 0; p < mRooms[i]->GetCompressedInventorySize(); p ++)
        {
            //--Get the item in the slot.
            InventoryPack *rItemPack = mRooms[i]->GetItemPack(p);
            if(!rItemPack || !rItemPack->rItem) continue;

            //--Decrement.
            if(rItemPack->mLastSetVisible == (int)Global::Shared()->gTicksElapsed) continue;
            if(rItemPack->mIsVisibleTimer > 0) rItemPack->mIsVisibleTimer --;
        }

        ///--[WorldContainers]
        StarLinkedList *rContainerList = mRooms[i]->GetContainerList();
        WorldContainer *rContainer = (WorldContainer *)rContainerList->PushIterator();
        while(rContainer)
        {
            rContainer->DecrementVisible();
            rContainer = (WorldContainer *)rContainerList->AutoIterate();
        }
    }
}
