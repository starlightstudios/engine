//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "InventoryItem.h"
#include "WorldContainer.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "OptionsManager.h"

void VisualLevel::RenderActor(Actor *pActor, int pWorldSlot)
{
    //--Renders the requested Actor in 3D space. All checks should have already passed (such as the Actor being obscured).
    if(!pActor || !Images.mIsReady) return;

    //--Damage animation. Entities flash when struck. The pack provided can be NULL.
    DamageAnimPack *rPack = (DamageAnimPack *)mDamageAnimationQueue->GetElementBySlot(0);
    if(rPack && rPack->rTargetRefPtr == pActor)
    {
        //--In this condition, animate a flashing by not rendering the Actor in question.
        if(rPack->mDamageTimer < PL_DAM_FLASH_TICKS && rPack->mDamageTimer % 4 < 2 && (rPack->mDamageHP > 0 || rPack->mDamageWP > 0))
        {
            return;
        }
    }

    //--Option Flags
    OptionsManager *rOptionManager = OptionsManager::Fetch();
    bool tShowHP     = rOptionManager->GetOptionB("Show Entity HP");
    bool tShowWP     = rOptionManager->GetOptionB("Show Entity WP");
    bool tShowWeapon = rOptionManager->GetOptionB("Show Entity Weapon");

    //--Get the image we should render with.
    float tRenderAlpha = (float)pActor->GetVisibilityTimer() / (float)VL_ENTITY_FADE_TICKS;
    StarBitmap *rRenderImg = pActor->GetActiveImage();

    //--Normal case.
    int tRenderSlot = -1;
    if(!mDontRepositionOnWorldRender)
    {
        tRenderSlot = RenderBitmapInWorld(pWorldSlot, tRenderAlpha, rRenderImg);
        pActor->mLastRenderSlot = pWorldSlot;
    }
    //--Hold position as other objects animate.
    else
    {
        tRenderSlot = RenderBitmapInWorld(pActor->mLastRenderSlot, 1.0f, rRenderImg);
    }

    //--Render the name above the image.
    if(rRenderImg && tRenderSlot != -1)
    {
        //--Special: Set this flag to figure out if we need to render the extra information.
        bool tRenderExtraInfo = (pActor->GetRenderFlag() == RENDER_HUMAN || pActor->GetRenderFlag() == RENDER_MONSTER);

        //--Buffer. Figure out what to render. We render more detail if the HP/WP flags are set.
        char tBuffer[STD_MAX_LETTERS];
        sprintf(tBuffer, "%s", pActor->GetName());

        //--Position.
        float tX = mLastRenderX - (PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth(tBuffer) * 0.5f * cScreenTextScale);
        float tY = (VIRTUAL_CANVAS_Y * 0.45f) - (rRenderImg->GetTrueHeight() * 0.6f * mLastRenderScale);

        //--If the flag for HP/MP render is set, move the Y up a bit.
        if((tShowHP || tShowWP) && tRenderExtraInfo) tY = tY - 15.0f;

        //--If the flag tShowWeapon is true, move the Y up a bit. We render the weapon name below.
        InventoryItem *rActorWeapon = pActor->GetWeapon();
        if(tShowWeapon && rActorWeapon && tRenderExtraInfo) tY = tY - 15.0f;

        //--Render the text.
        glColor4f(1.0f, 1.0f, 1.0f, tRenderAlpha);
        PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tX, tY, 0, cScreenTextScale, tBuffer);
        tY = tY + 20.0f;

        //--Ignore the extra renders for non-humans and non-monsters.
        if(tRenderExtraInfo)
        {
            //--Render with just HP.
            if(tShowHP && !tShowWP)
            {
                CombatStats tActorStats = pActor->GetCombatStatistics();
                sprintf(tBuffer, "(H: %i/%i)", tActorStats.mHP, tActorStats.mHPMax);
                PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tX, tY, 0, cScreenTextScale * 0.75, tBuffer);
                tY = tY + 15.0f;
            }
            //--Render with just WP.
            else if(!tShowHP && tShowWP)
            {
                CombatStats tActorStats = pActor->GetCombatStatistics();
                sprintf(tBuffer, "(W: %i/%i)", tActorStats.mWillPower, tActorStats.mWillPowerMax);
                PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tX, tY, 0, cScreenTextScale * 0.75, tBuffer);
                tY = tY + 15.0f;
            }
            //--Render with both HP and WP.
            else if(tShowHP && tShowWP)
            {
                CombatStats tActorStats = pActor->GetCombatStatistics();
                sprintf(tBuffer, "H:%i W:%i", tActorStats.mHP, tActorStats.mWillPower);
                PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tX, tY, 0, cScreenTextScale * 0.75, tBuffer);
                tY = tY + 15.0f;
            }

            //--Render the weapon name.
            if(tShowWeapon && rActorWeapon)
            {
                PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tX, tY, 0, cScreenTextScale * 0.75f, rActorWeapon->GetName());
            }
        }
    }

    //--Damage rendering. Show the HP/WP/Resist text.
    if(rPack && rPack->rTargetRefPtr == pActor)
    {
        //--Position/Setup.
        float tX = mLastRenderX;
        float tY = (VIRTUAL_CANVAS_Y * 0.45f) - (rRenderImg->GetTrueHeight() * 0.6f * mLastRenderScale);
        float cTextScale = 3.0f;
        float tOffset = 0.0f + (EasingFunction::QuadraticInOut(rPack->mDamageTimer, PL_DAM_TICKS_TOTAL) * 60.0f);

        //--If the HP damage is 0 and the WP is -1, this is a "miss".
        if(rPack->mDamageHP == 0 && rPack->mDamageWP < 0)
        {
            //--Miss is rendered in grey.
            glColor3f(0.5f, 0.5f, 0.5f);

            //--Compute the position.
            float tRenderX = tX - (PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth("Miss!") * 0.5f * cTextScale);
            float tRenderY = tY - tOffset;

            //--Render it.
            PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tRenderX, tRenderY, 0, cTextScale, "Miss!");
        }
        //--If the HP damage is -1 and the WP is 0, this is a "resist".
        else if(rPack->mDamageHP < 0 && rPack->mDamageWP == 0)
        {
            //--Resist is rendered in violet.
            glColor3f(1.0f, 0.4f, 0.9f);

            //--Compute the position.
            float tRenderX = tX - (PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth("Resist!") * 0.5f * cTextScale);
            float tRenderY = tY - tOffset;

            //--Render it.
            PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tRenderX, tRenderY, 0, cTextScale, "Resist!");
        }
        //--Otherwise, animate whichever damage is above 0. This can be both.
        else
        {
            //--HP damage.
            if(rPack->mDamageHP > 0)
            {
                //--HP damage is red.
                glColor3f(1.0f, 0.0f, 0.0f);

                //--Buffer the damage.
                char tBuffer[16];
                sprintf(tBuffer, "%i", rPack->mDamageHP);

                //--Compute the position.
                float tRenderX = tX - (PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth(tBuffer) * 0.5f * cTextScale);
                float tRenderY = tY - tOffset;

                //--If WP damage was dealt, the X render moves to the left.
                if(rPack->mDamageWP > 0) tRenderX = tRenderX - tOffset;

                //--Render it.
                PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tRenderX, tRenderY, 0, cTextScale, tBuffer);
            }

            //--WP damage. Not exclusive with HP damage!
            if(rPack->mDamageWP > 0)
            {
                //--HP damage is violet.
                glColor3f(0.8f, 0.1f, 1.0f);

                //--Buffer the damage.
                char tBuffer[16];
                sprintf(tBuffer, "%i", rPack->mDamageWP);

                //--Compute the position.
                float tRenderX = tX - (PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth(tBuffer) * 0.5f * cTextScale);
                float tRenderY = tY - tOffset;

                //--If HP damage was dealt, the X render moves to the right.
                if(rPack->mDamageHP > 0) tRenderX = tRenderX + tOffset;

                //--Render it.
                PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tRenderX, tRenderY, 0, cTextScale, tBuffer);
            }
        }
    }
}
void VisualLevel::RenderInventoryPack(InventoryPack *pPack, int pWorldSlot)
{
    //--Renders an InventoryPack in 3D space.
    if(!pPack || !Images.mIsReady) return;
    float tRenderAlpha = (float)pPack->mIsVisibleTimer / (float)VL_ENTITY_FADE_TICKS;
    StarBitmap *rItemIcon = pPack->rItem->Get3DImageFor();

    //--Normal case.
    int tRenderSlot = -1;
    if(!mDontRepositionOnWorldRender)
    {
        tRenderSlot = RenderBitmapInWorld(pWorldSlot, tRenderAlpha, rItemIcon);
        pPack->mLastRenderSlot = pWorldSlot;
    }
    //--Hold position as other objects animate.
    else
    {
        tRenderSlot = RenderBitmapInWorld(pPack->mLastRenderSlot, tRenderAlpha, rItemIcon);
    }

    //--Render the name of the item right below it. Items always use the same icon so
    //  the offset is fixed.
    const char *rItemName = pPack->rItem->GetName();
    if(rItemName && tRenderSlot != -1)
    {
        //--Resolve the text. Print the quantity in addition to the name if it's 2 or more.
        char tBuffer[64];
        strcpy(tBuffer, rItemName);
        if(pPack->mQuantity == 1)
        {
            sprintf(tBuffer, "%s", rItemName);
        }
        else if(pPack->mQuantity > 1)
        {
            sprintf(tBuffer, "%sx%i", rItemName, pPack->mQuantity);
        }

        //--Compute text position. Account for scaling!
        float tX = mLastRenderX - (PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth(tBuffer) * 0.5f * cScreenTextScale);
        float tY = (VIRTUAL_CANVAS_Y * 0.5f) - (rItemIcon->GetTrueHeight() * 0.6f * mLastRenderScale);

        //--Render the text.
        glColor4f(1.0f, 1.0f, 1.0f, tRenderAlpha);
        PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tX, tY, 0, cScreenTextScale, tBuffer);
    }
}
void VisualLevel::RenderContainer(WorldContainer *pContainer, int pWorldSlot)
{
    //--Renders a WorldContainer in 3D space.
    if(!pContainer || !Images.mIsReady) return;
    float tRenderAlpha = (float)pContainer->GetVisibilityTimer() / (float)VL_ENTITY_FADE_TICKS;
    StarBitmap *rChestIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/TreasureChest");

    //--Normal case.
    int tRenderSlot = -1;
    if(!mDontRepositionOnWorldRender)
    {
        tRenderSlot = RenderBitmapInWorld(pWorldSlot, tRenderAlpha, rChestIcon);
        pContainer->mLastRenderSlot = pWorldSlot;
    }
    //--Hold position as other objects animate.
    else
    {
        tRenderSlot = RenderBitmapInWorld(pContainer->mLastRenderSlot, tRenderAlpha, rChestIcon);
    }

    //--Don't render if the object... shouldn't render.
    if(tRenderSlot != -1)
    {
        //--Position.
        float tX = mLastRenderX - (PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth(pContainer->GetName()) * 0.5f * cScreenTextScale);
        float tY = (VIRTUAL_CANVAS_Y * 0.5f) - (rChestIcon->GetTrueHeight() * 0.6f * mLastRenderScale);

        //--Render the text.
        glColor4f(1.0f, 1.0f, 1.0f, tRenderAlpha);
        PandemoniumLevel::Images.Data.rUIFontMedium->DrawText(tX, tY, 0, cScreenTextScale, pContainer->GetName());
    }
}
