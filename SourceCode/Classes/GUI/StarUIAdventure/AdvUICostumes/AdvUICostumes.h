///======================================= AdvUICostumes ==========================================
//--Shows multiple grids allowing the player to switch character costumes by executing scripts.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIGrid.h"
#include "StarLinkedList.h"

///===================================== Local Structures =========================================
#ifndef ADVUICOSTUME_DEFINITIONS
#define ADVUICOSTUME_DEFINITIONS

///--[Costume Heading Storage]
//--Goes parallel with the GridPackages that make up the initial selection grid. Stores the information
//  needed to make the character's sublist.
typedef struct CostumeHeadingPack
{
    //--Members
    char *mName;
    char *mDisplayName;
    char *mFormName;
    StarLinkedList *mSublist; //CostumeEntryPack *, master

    //--Display
    AdvMenuStandardAlignments mAlignments;

    //--Functions
    void Initialize()
    {
        //--Members
        mName = NULL;
        mDisplayName = NULL;
        mFormName = NULL;
        mSublist = new StarLinkedList(true);

        //--Display
        mAlignments.Initialize();
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        CostumeHeadingPack *rPtr = (CostumeHeadingPack *)pPtr;
        free(rPtr->mName);
        free(rPtr->mDisplayName);
        free(rPtr->mFormName);
        delete rPtr->mSublist;
        free(rPtr);
    }
}CostumeHeadingPack;

///--[Costume Entry Storage]
//--Goes parallel with the GridPackages that make up a character's subgrid. Stores images and execution
//  paths as needed.
typedef struct CostumeEntryPack
{
    //--Members
    char *mExecPath;

    //--Display
    AdvMenuStandardAlignments mAlignments;

    //--Functions
    void Initialize()
    {
        //--Members
        mExecPath = NULL;

        //--Display
        mAlignments.Initialize();
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        CostumeEntryPack *rPtr = (CostumeEntryPack *)pPtr;
        free(rPtr->mExecPath);
        free(rPtr);
    }
}CostumeEntryPack;
#endif

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUICostumes : virtual public AdvUICommon, virtual public AdvUIGrid
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxAdvCostumeHelpStrings = 4;

    ///--[Variables]
    //--System
    bool mIsEntryMode;
    int mEntryTimer;

    //--Cursor
    bool mDontShowHighlight;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Data
    StarLinkedList *mHeadingData; //CostumeHeadingPack *, master
    StarLinkedList *rEntryData;   //CostumeEntryPack *, reference

    //--Strings
    StarlightString *mShowHelpString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Mainline;

        //--Common
        StarBitmap *rOverlay_Footer;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUICostumes();
    virtual ~AdvUICostumes();
    virtual void Construct();

    //--Public Variables
    static char *xCostumeResolveScript;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    void AddCostumeHeading(const char *pInternalName, const char *pCharName, const char *pDisplayName, const char *pFormName, const char *pImagePath);
    void SetCostumeHeadingImageData(const char *pInternalName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei);
    void AddCostumeEntry(const char *pHeadingName, const char *pCostumeName, const char *pImagePath, const char *pScriptPath);
    void SetCostumeEntryImageData(const char *pHeadingName, const char *pInternalName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void ActivateEntriesMode();
    void DeactivateEntriesMode();
    void ResetCostumeImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


