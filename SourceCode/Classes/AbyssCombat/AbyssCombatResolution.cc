//--Base
#include "AbyssCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///======================================= Manipulators ===========================================
void AbyssCombat::SetVictorySpeaker(const char *pName)
{
    //--Locate entity.
    AdvCombatEntity *rCharacter = GetRosterMemberS(pName);
    if(!rCharacter) return;

    //--Use their display name.
    ResetString(mFinalePortraitName, rCharacter->GetDisplayName());

    //--Get their combat portrait and offsets.
    mFinalePortraitX = rCharacter->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_VICTORY).mXCenter;
    mFinalePortraitY = rCharacter->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_VICTORY).mYCenter;
    rFinalePortraitImg = rCharacter->GetCombatPortrait();
}
void AbyssCombat::SetVictoryLine(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= ABYCOMBAT_FINALE_LINES_TOTAL) return;
    strncpy(mFinaleLines[pSlot], pString, sizeof(char) * (ABYCOMBAT_FINALE_LINE_LEN - 1));
}

///========================================== Update ==============================================
void AbyssCombat::UpdateResolution()
{
    ///--[Documentation]
    //--For victory cases, same as the normal resolution. Defeat has special handlers.

    ///--No resolution, do nothing.
    if(mCombatResolution == ADVCOMBAT_END_NONE)
    {
        return;
    }
    ///--Victory.
    //--Call base class.
    else if(mCombatResolution == ADVCOMBAT_END_VICTORY)
    {
        AdvCombat::UpdateResolution();
    }
    ///--[Defeat/Surrender]
    //--Handled differently than the base class. Each of the three heroes is shown one at a time,
    //  pausing for the player to press a button. Then we fade to black and exit.
    else if(mCombatResolution == ADVCOMBAT_END_DEFEAT || mCombatResolution == ADVCOMBAT_END_SURRENDER)
    {
        ///--[Fade Out]
        //--Run to timer, auto-advance.
        if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_FADE_OUT_UI)
        {
            //--Music:
            if(mDefeatTimer == 1)
            {
                AudioManager::Fetch()->FadeMusic(30);
            }

            mDefeatTimer ++;
            if(mDefeatTimer >= ABYCOM_DEFEAT_FADEUI_TICKS)
            {
                mDefeatTimer = 0;
                mDefeatCharacterPhase ++;
            }
        }
        //--Hold. Auto-advance.
        else if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_HOLD)
        {
            //--Music:
            if(mDefeatTimer == 1)
            {
                AudioManager::Fetch()->PlayMusicNoFade("NotVictory");
            }

            mDefeatTimer ++;
            if(mDefeatTimer >= ABYCOM_DEFEAT_HOLD_TICKS)
            {
                mDefeatTimer = 0;
                mDefeatCharacterPhase ++;
            }
        }
        ///--[Show Phases]
        //--Counts up to a timer. Advances to the next stage.
        else if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR0_SHOW || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR1_SHOW || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR2_SHOW)
        {
            mDefeatTimer ++;
            if(mDefeatTimer >= ABYCOM_DEFEAT_SLIDE_TICKS)
            {
                mDefeatTimer = 0;
                mDefeatCharacterPhase ++;
            }
        }
        ///--[Hold Phases]
        //--Holds until the player presses a button.
        else if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR0_HOLD || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR1_HOLD || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR2_HOLD)
        {
            if(ControlManager::Fetch()->IsAnyKeyPressed())
            {
                mDefeatCharacterPhase ++;
            }
        }
        ///--[Exit Phases]
        //--Counts up to a timer. Advances to the next stage.
        else if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR0_HIDE || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR1_HIDE || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR2_HIDE)
        {
            mDefeatTimer ++;
            if(mDefeatTimer >= ABYCOM_DEFEAT_SLIDE_TICKS)
            {
                mDefeatTimer = 0;
                mDefeatCharacterPhase ++;
            }
        }
        ///--[Fadout]
        //--Exit at timer expire.
        else if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_FADEOUT)
        {
            mDefeatTimer ++;
            if(mDefeatTimer >= ABYCOM_DEFEAT_FADE_TICKS)
            {
                //--If this is a surrender, set this flag.
                if(mCombatResolution == ADVCOMBAT_END_SURRENDER)
                {
                    mWasLastDefeatSurrender = true;
                }

                //--If there is an override script, handle it here:
                if(mCurrentDefeatScript)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(mCurrentDefeatScript);
                }
                //--Run the standard defeat script if it exists:
                else if(mStandardDefeatScript)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(mStandardDefeatScript);
                }

                //--All entities handle the post-battle code.
                AdventureLevel *rLevel = AdventureLevel::Fetch();
                if(rLevel) rLevel->RunPostCombatOnEntities();

                //--Return characters to their starting jobs and handle stat changes.
                AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
                while(rEntity)
                {
                    rEntity->HandleCombatEnd();
                    rEntity->RevertToCombatStartJob();
                    rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
                }

                //--In all cases, deactivate this object.
                Deactivate();
                AudioManager::Fetch()->StopAllMusic();
                CleanDataLibrary();
            }
        }
    }
    ///--[Retreat]
    //--Call base class.
    else if(mCombatResolution == ADVCOMBAT_END_RETREAT)
    {
        AdvCombat::UpdateResolution();
    }
}

///========================================== Drawing =============================================
void AbyssCombat::RenderVictory()
{
    ///--[Documentation and Setup]
    //--Victory! The player sees UI showing their party and the spoils of the fight.

    ///--[Backing]
    //--Backing underlay.
    StarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    ///--[Victory Overlay]
    //--In the first phase, render the "VICTORY!" text.
    if(mResolutionState == ADVCOMBAT_VICTORY_OVERLAY)
    {
        ///--[Fade-out of Combat]
        //--Everything that was rendering normally fades out.
        float cSubAlpha = 1.0f - (mResolutionTimer / (float)ADVCOMBAT_VICTORY_OVERLAY_TICKS);
        Render(true, cSubAlpha);

        //--Render nothing else if the dialogue is up.
        if(WorldDialogue::Fetch()->IsVisible()) return;

        ///--[Victory Stuff]
        //--Banner.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_VICTORY_OVERLAY_TICKS / 10.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent);
        AbyssImg.Data.rOverlayVictoryShade->Draw();
        StarlightColor::ClearMixer();

        //--Get the full length. We compute the left position using the full length.
        float cRenderX = (VIRTUAL_CANVAS_X * 0.50f) - (AbyssImg.Data.rFontVictory->GetTextWidth("VICTORY!") * 4.0f * 0.50f);
        float cRenderY = 267.0f;

        //--Create a buffer with the number of letters we are currently displaying.
        int tLetters = mResolutionTimer / ADVCOMBAT_VICTORY_OVERLAY_TICKS_PER_LETTER;
        if(tLetters >= 1)
        {
            char tBuffer[10];
            memset(tBuffer, 0, sizeof(char) * 10);
            strncpy(tBuffer, "VICTORY!", tLetters);
            mColorHeader.SetAsMixer();
            AbyssImg.Data.rFontVictory->DrawText(cRenderX, cRenderY, SUGARFONT_AUTOCENTER_Y, 4.0f, tBuffer);
            StarlightColor::ClearMixer();
        }
    }
    //--Zoom "Victory" to the top of the screen, slide in the banner beneath it.
    else if(mResolutionState == ADVCOMBAT_VICTORY_ZOOM)
    {
        //--Populate victory dialogue at this tick.
        if(mResolutionTimer == 5 && xVictoryDialoguePath) LuaManager::Fetch()->ExecuteLuaFile(xVictoryDialoguePath, 1, "N", ABYCOMBAT_FINALE_SCRIPT_POPULATE);

        //--Compute percent.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_VICTORY_ZOOM_TICKS);
        RenderVictoryBanner(cPercent);
    }
    //--Slide displays in from the side of the screen.
    else if(mResolutionState == ADVCOMBAT_VICTORY_SLIDE)
    {
        //--Victory! banner does not move.
        RenderVictoryBanner(1.0f);

        //--Completion percentage.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCBOMAT_VICTORY_SLIDE_TICKS);
        RenderVictoryDrops(cPercent);
        RenderVictoryDoctor(cPercent);
        RenderExpBars(cPercent);
    }
    //--XP/JP/Drops/etc count off.
    else if(mResolutionState == ADVCOMBAT_VICTORY_COUNT_OFF)
    {
        //--Victory! banner does not move.
        RenderVictoryBanner(1.0f);

        //--All these render at full completion.
        RenderVictoryDrops(1.0f);
        RenderVictoryDoctor(1.0f);
        RenderExpBars(1.0f);
    }
    //--Changes doctor bag quantities. This is handled within the subroutine.
    else if(mResolutionState == ADVCOMBAT_VICTORY_DOCTOR)
    {
        RenderVictoryBanner(1.0f);
        RenderVictoryDrops(1.0f);
        RenderVictoryDoctor(1.0f);
        RenderExpBars(1.0f);
    }
    //--Slide all panels offscreen.
    else if(mResolutionState == ADVCOMBAT_VICTORY_HIDE)
    {
        //--Victory! banner slides up.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCBOMAT_VICTORY_HIDE_TICKS);
        float cSlideOff = -110.0f * cPercent;
        glTranslatef(0.0f, cSlideOff, 0.0f);
        RenderVictoryBanner(1.0f);
        glTranslatef(0.0f, -cSlideOff, 0.0f);

        //--Completion percentage.
        RenderVictoryDrops(1.0f - cPercent);
        RenderVictoryDoctor(1.0f - cPercent);
        RenderExpBars(1.0f - cPercent);
    }
}

///===================================== Rendering Workers ========================================
void AbyssCombat::RenderVictoryBanner(float pPercent)
{
    ///--[Documentation]
    //--Worker function, used to render the "VICTORY!" and backing banner the same way in many places
    //  within the below function.
    //--Similar to the basic version, but modifies the backing and font.

    //--Compute position.
    float cScale = 1.00f - (0.50f * pPercent);
    float cBanRenderX = ( VIRTUAL_CANVAS_X * 0.50f / cScale) - (AbyssImg.Data.rOverlayVictoryShade->GetTrueWidth() * 0.50f);
    float cBanRenderY = -140.0f * pPercent;
    float cTxtRenderX = ((VIRTUAL_CANVAS_X * 0.50f / cScale) - (AbyssImg.Data.rFontVictory->GetTextWidth("VICTORY!") * 4.0f * 0.50f));
    float cTxtRenderY = (267.0f + cBanRenderY);

    //--Scale, render.
    glScalef(cScale, cScale, 1.0f);
    AbyssImg.Data.rOverlayVictoryShade->Draw(cBanRenderX, cBanRenderY);
    mColorHeader.SetAsMixer();
    AbyssImg.Data.rFontVictory->DrawText(cTxtRenderX, cTxtRenderY, SUGARFONT_AUTOCENTER_Y, 4.0f, "VICTORY!");

    //--Clean.
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    StarlightColor::ClearMixer();
}
void AbyssCombat::RenderVictoryDrops(float pPercent)
{
    ///--[Documentation]
    //--Renders the drops box. The passed percentage is how far off the screen to scroll. Drops are
    //  handled the same as the base class but the fonts and positioning are different.
    float cFrameX = -1366.0f * (1.0f - pPercent);
    glTranslatef(cFrameX, 0.0f, 0.0f);

    //--Sizing/Positions.
    float cTextX = 434.0f;
    float cTextY = 163.0f;
    float cTextH = 30.0f;

    //--Backing.
    AbyssImg.Data.rFrameVictoryLoot->Draw();

    //--Text.
    float cPercent = EasingFunction::Linear(mResolutionTimer, ADVCBOMAT_VICTORY_AWARD_TICKS);
    char *tRenderString = Subdivide::ReplaceTagsWithStrings(AbyssString.str.mTextRewardPlusPlatina, 1, "[iGainPlatina]", (int)(mPlatinaToAwardStorage * cPercent));
    AbyssImg.Data.rFontLoot->DrawTextArgs(cTextX, cTextY, 0, 1.0f, tRenderString);
    cTextY = cTextY + cTextH;
    free(tRenderString);

    //--Render items gained. The list only has items that have been awarded thus far. They also fade in over time.
    int i = 0;
    AdvVictoryItemPack *rPackage = (AdvVictoryItemPack *)mItemsAwardedList->PushIterator();
    while(rPackage)
    {
        //--Alpha:
        float cAlpha = EasingFunction::Linear(rPackage->mTimer, ADVCOMBAT_VICTORY_PACK_TICKS);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Icon, if it exists.
        if(rPackage->rItemImg) rPackage->rItemImg->Draw(cTextX, cTextY+9.0f);

        //--Name.
        AbyssImg.Data.rFontLoot->DrawText(cTextX + 24.0f, cTextY, 0, 1.0f, rPackage->rItemNameRef);

        //--Next.
        i ++;
        cTextY = cTextY + cTextH;
        if(i == 9)
        {
            cTextX = 730.0f;
            cTextY = 163.0f;
        }
        else if(i == 19)
        {
            mItemsAwardedList->PopIterator();
            break;
        }
        rPackage = (AdvVictoryItemPack *)mItemsAwardedList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
    glTranslatef(-cFrameX, 0.0f, 0.0f);

    ///--[Platina Display]
    //--Appears in the top right, showing total cash.
    cFrameX = 400.0f * (1.0f - pPercent);
    glTranslatef(cFrameX, 0.0f, 0.0f);

    //--Backing/Title.
    AbyssImg.Data.rFramePlatina->Draw();
    StarlightColor::SetMixer(0.5f, 0.35f, 0.40f, 1.0f);
    AbyssImg.Data.rFontHeader->DrawText(1029.0f, 20.0f, 0, 1.0f, AbyssString.str.mTextPlatina);

    //--Actual value:
    mColorParagraph.SetAsMixer();
    AbyssImg.Data.rFontHeader->DrawTextArgs(1343.0f, 20.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", AdventureInventory::Fetch()->GetPlatina());
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);

    //--Clean.
    glTranslatef(-cFrameX, 0.0f, 0.0f);
}
void AbyssCombat::RenderVictoryDoctor(float pPercent)
{
    //--Does nothing in Abyss Combat.
}
void AbyssCombat::RenderExpBars(float pPercent)
{
    ///--[Documentation]
    //--Used to render the post-combat callouts, if there are any. Experience/JP are not used in this
    //  game mode so the function is recycled.
    if(!mFinalePortraitName) return;

    //--Scroll in from offscreen.
    float cPosition = 700.0f * (1.0f - pPercent);
    glTranslatef(0.0f, cPosition, 0.0f);

    //--Render parts.
    if(rFinalePortraitImg)
    {
        rFinalePortraitImg->Draw(mFinalePortraitX, mFinalePortraitY);
    }
    AbyssImg.Data.rFrameVictoryDialogue->Draw();

    //--Name.
    AbyssImg.Data.rFontNameplate->DrawText(164.0f, 505.0f, 0, 1.0f, mFinalePortraitName);

    //--Dialogue.
    float cLineHei = 29.0f;
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
    for(int i = 0; i < ABYCOMBAT_FINALE_LINES_TOTAL; i ++)
    {
        AbyssImg.Data.rFontDialogue->DrawText(77.0f, 574.0f + (cLineHei * i), 0, 1.0f, mFinaleLines[i]);
    }
    StarlightColor::ClearMixer();

    //--Clean.
    glTranslatef(0.0f, -cPosition, 0.0f);
}
void AbyssCombat::RenderDefeat()
{
    ///--[Documentation]
    //--Shows the party members then fades to black.

    ///--[Fade UI Out]
    //--Do nothing in this phase, it's handled elsewhere.
    if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_FADE_OUT_UI) return;

    ///--[Fading Out]
    //--Does not require a character to resolve.
    if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_FADEOUT)
    {
        StarBitmap::DrawFullBlack(1.0f);
        return;
    }
    else
    {
        StarBitmap::DrawFullBlack(1.0f);
    }

    ///--[Resolve Character]
    //--Get the character in question. All code below this requires a valid portrait.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(0);
    if(mDefeatCharacterPhase <= ABYCOM_DEFEAT_PHASE_CHAR0_HIDE)
    {
    }
    else if(mDefeatCharacterPhase <= ABYCOM_DEFEAT_PHASE_CHAR1_HIDE)
    {
        rEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(1);
    }
    else if(mDefeatCharacterPhase <= ABYCOM_DEFEAT_PHASE_CHAR2_HIDE)
    {
        rEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(2);
    }

    //--Character did not resolve. Stop.
    if(!rEntity) return;

    //--Get combat portrait. It must resolve.
    StarBitmap *rCombatPortrait = rEntity->GetCombatPortrait();
    if(!rCombatPortrait) return;

    //--Sizes.
    float cWid = rCombatPortrait->GetTrueWidth();
    float cHei = rCombatPortrait->GetTrueHeight();

    ///--[Slide Character In]
    //--Character slides in based on the timer.
    if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR0_SHOW || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR1_SHOW || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR2_SHOW)
    {
        //--Resolve position.
        float cXLft = -200.0f;
        float cXRgt = VIRTUAL_CANVAS_X * 0.50f;
        float tXPosition = cXLft + ((cXRgt - cXLft) * EasingFunction::QuadraticIn(mDefeatTimer, ABYCOM_DEFEAT_SLIDE_TICKS));
        float tYPosition = VIRTUAL_CANVAS_Y * 0.50f;

        //--Render.
        rCombatPortrait->Draw(tXPosition - (cWid / 2.0f), tYPosition - (cHei / 2.0f));
    }
    ///--[Hold Position]
    //--Holds position in the center.
    else if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR0_HOLD || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR1_HOLD || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR2_HOLD)
    {
        //--Resolve position.
        float tXPosition = VIRTUAL_CANVAS_X * 0.50f;
        float tYPosition = VIRTUAL_CANVAS_Y * 0.50f;

        //--Render.
        rCombatPortrait->Draw(tXPosition - (cWid / 2.0f), tYPosition - (cHei / 2.0f));
    }
    ///--[Exit Phases]
    //--Slide offscreen to the right.
    else if(mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR0_HIDE || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR1_HIDE || mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_CHAR2_HIDE)
    {
        //--Resolve position.
        float cXLft = VIRTUAL_CANVAS_X * 0.50f;
        float cXRgt = VIRTUAL_CANVAS_X * 1.00f + 200.0f;
        float tXPosition = cXLft + ((cXRgt - cXLft) * EasingFunction::QuadraticOut(mDefeatTimer, ABYCOM_DEFEAT_SLIDE_TICKS));
        float tYPosition = VIRTUAL_CANVAS_Y * 0.50f;

        //--Render.
        rCombatPortrait->Draw(tXPosition - (cWid / 2.0f), tYPosition - (cHei / 2.0f));
    }
}
