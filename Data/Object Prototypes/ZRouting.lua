-- |[ ================================ Object Prototype Routing ================================ ]|
--Sets up globals and then calls all job prototype scripts to build their initial listing and
-- requirements.
local sBasePath = fnResolvePath()

-- |[Activate Prototype Builder Mode]|
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()
SOB_Create()

-- |[First Execution]|
--Create a namespacing class that stores some useful functions.
if(ObjectPrototype == nil) then
    LM_ExecuteScript(sBasePath .. "Class/ObjectPrototype.lua")
    LM_ExecuteScript(sBasePath .. "Stats/StatGenerator.lua")
end

-- |[Paths]|
--These paths should be set in your Engine/DeveloperLocal/Object Prototype Paths.lua file. They are
-- used in the export of certain things like enemy stat prototypes.
--The object format is:
--zObject.iEnumIndex = 0
--zObject.sDisplayName = "Chapter 1 Enemies"
--zObject.sExecPath = "../adventure/Games/AdventureMode/Combat/Enemies/Chapter 1/Enemy Stat Chart.lua" --Note: Is relative to the Engine/ directory as that's where the executable is.
gzaPathListing = {}
LM_ExecuteScript(sBasePath .. "../../DeveloperLocal/Object Prototype Paths.lua")

-- |[Call]|
LM_ExecuteScript(sBasePath .. "Character Job.lua",         ObjectPrototype.ciScriptCreate)
LM_ExecuteScript(sBasePath .. "Damage Ability.lua",        ObjectPrototype.ciScriptCreate)
LM_ExecuteScript(sBasePath .. "DoT Ability.lua",           ObjectPrototype.ciScriptCreate)
LM_ExecuteScript(sBasePath .. "Heal Ability.lua",          ObjectPrototype.ciScriptCreate)
LM_ExecuteScript(sBasePath .. "HoT Ability.lua",           ObjectPrototype.ciScriptCreate)
LM_ExecuteScript(sBasePath .. "Effect Ability.lua",        ObjectPrototype.ciScriptCreate)
LM_ExecuteScript(sBasePath .. "Damage Effect Ability.lua", ObjectPrototype.ciScriptCreate)
LM_ExecuteScript(sBasePath .. "Enemy Auto Stats.lua",      ObjectPrototype.ciScriptCreate)