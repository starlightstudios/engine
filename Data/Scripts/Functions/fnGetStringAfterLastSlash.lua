-- |[ =============================== fnGetStringAfterLastSlash ================================ ]|
--Given a string, returns the substring that is after the last slash. If no slash is found, returns
-- the whole string.
--Ex: "/Root/Dogs"
--Returns: "Dogs"
function fnGetStringAfterLastSlash(psString)
    
    -- |[Arg Check]|
    --Check arguments.
    if(psString == nil) then return "" end
    
    -- |[Setup]|
    --Variables.
    local iLen = string.len(psString)
    
    --Iterate backwards from the end to the front, looking for slashes.
    for x = iLen, 1, -1 do
        if(string.sub(psString, x, x) == "/") then
            return string.sub(psString, x+1)
        end
    end
    
    --If we got this far, no slash was found. Exit.
    return psString
end

