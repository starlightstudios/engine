///========================================= RootEvent =============================================
//--Events used by the CutsceneManager. Allows derived classes. Any instance can be a blocker, but it's
//  usually best to have a dedicated blocker which does nothing else.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define SCRIPT_EVENT_CONSTRUCTOR 0
#define SCRIPT_EVENT_EXECUTE 1
#define SCRIPT_EVENT_CLEANUP 2

///========================================== Classes =============================================
class RootEvent : public RootObject
{
    private:

    protected:
    //--System
    bool mCheckpointState;
    bool mIsMalformed;
    char *mLocalName;
    bool mIsBlocker;

    //--Script Execution
    bool mIsScriptExecutor;
    bool mIsScriptConstructor;
    bool mIsScriptRawInstruction;
    char *mScriptPath;

    //--Data Storage
    DataList *mDataList;

    public:
    //--System
    RootEvent();
    virtual ~RootEvent();

    //--Public Variables
    static bool xIsScriptComplete;
    static bool xCurrentCheckpointState;
    static bool xIsRunningToCheckpoint;

    //--Property Queries
    virtual bool IsOfType(int pType);
    virtual bool DisablesTimerAcceleration();
    const char *GetName();
    bool IsBlocker();
    bool IsScriptExecutionCase();
    bool IsInstructionExecutionCase();

    //--Manipulators
    void SetName(const char *pName);
    void SetBlockingFlag(bool pFlag);
    void SetScriptExecutionMode(const char *pScript);
    void SetInstructionExecutionMode(const char *pInstructionData);

    //--Core Methods
    virtual bool IsComplete();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    virtual void HandleDiagnostics();

    //--Drawing
    //--Pointer Routing
    DataList *GetDataList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_REvent_SetProperty(lua_State *L);
int Hook_REvent_GetProperty(lua_State *L);
int Hook_REvent_DefineData(lua_State *L);
int Hook_REvent_GetData(lua_State *L);
int Hook_REvent_SetData(lua_State *L);
