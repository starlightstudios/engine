//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvUIJournalTypes.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
void AdvUIJournal::SetJournalToLocationsMode()
{
    //--Clear the list.
    ClearLocations();

    //--Variables.
    mCurrentMode = ADVMENU_JOURNAL_MODE_LOCATIONS;

    //--Run this script to set the location's properties.
    LuaManager::Fetch()->ExecuteLuaFile(xLocationResolveScript);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIJournal::CreateLocationEntry(const char *pInternalName)
{
    //--Arg check.
    if(!pInternalName) return;

    //--Duplicate check.
    if(mLocationEntries->GetElementByName(pInternalName)) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuJournalLocationEntry *nEntry = (AdvMenuJournalLocationEntry *)starmemoryalloc(sizeof(AdvMenuJournalLocationEntry));
    nEntry->Initialize();
    ResetString(nEntry->mInternalName, pInternalName);
    mLocationEntries->AddElementAsTail(pInternalName, nEntry, &AdvMenuJournalLocationEntry::DeleteThis);
}
void AdvUIJournal::SetLocationDisplayName(const char *pInternalName, const char *pDisplayName)
{
    //--Error check.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDisplayName) return;

    //--Set.
    ResetString(rEntry->mDisplayName, pDisplayName);
}
void AdvUIJournal::SetLocationImage(const char *pInternalName, const char *pDLPath)
{
    //--Error check.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDLPath) return;

    //--Set.
    rEntry->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvUIJournal::SetLocationImageOffsets(const char *pInternalName, float pX, float pY)
{
    //--Error check.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mOffsetX = pX;
    rEntry->mOffsetY = pY;
}
void AdvUIJournal::AllocateLocationDescriptionLines(const char *pInternalName, int pCount)
{
    //--Error check.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->AllocateDescriptionLines(pCount);
}
void AdvUIJournal::SetLocationDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine)
{
    //--Error check.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDescriptionLine) return;

    //--Range check.
    if(pLine < 0 || pLine >= rEntry->mDescriptionLinesTotal) return;

    //--Set.
    ResetString(rEntry->mDescriptionLines[pLine], pDescriptionLine);
}
void AdvUIJournal::SetLocationDescriptionAuto(const char *pInternalName, const char *pDescription)
{
    ///--[Documentation]
    //--Uses subdivision to automatically set the location description lines via breakpoints.
    if(!pInternalName || !pDescription) return;

    //--Get entry.
    AdvMenuJournalLocationEntry *rEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementByName(pInternalName);
    if(!rEntry) return;

    ///--[Run Subdivide]
    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pDescription, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS, NULL, -1.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "AdvUIJournal::SetLocationDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        //--Liberate the string.
        tStringList->SetRandomPointerToHead();
        char *rString = (char *)tStringList->LiberateRandomPointerEntry();
        rEntry->mDescriptionLines[i] = rString;
    }

    //--Clean the list up.
    delete tStringList;
}
void AdvUIJournal::ClearLocations()
{
    mLocationEntries->ClearList();
}

///======================================= Core Methods ===========================================
void AdvUIJournal::ComputeLocationCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mLocationCursor - mLocationOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mLocationEntries->GetElementBySlot(mLocationCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = AdvImages.rFont_Mainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIJournal::RenderLocationsMode(float pVisAlpha)
{
    ///--[Location Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mLocationEntries, mLocationOffset, mLocationCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the information about the highlighted quest.
    AdvMenuJournalLocationEntry *rActiveEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementBySlot(mLocationCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Title]
        //--Title goes in the top middle of the description pane.
        float cTitleX = 918.0f;
        float cTitleY = 116.0f;
        mColorHeading.SetAsMixerAlpha(pVisAlpha);
        AdvImages.rFont_Header->DrawTextArgs(cTitleX, cTitleY, SUGARFONT_AUTOCENTER_X, 1.0f, rActiveEntry->mDisplayName);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Title Image.
        float cImageScale = 0.50f;
        float cImageScaleInv = 1.0f / cImageScale;
        float cImageX = 918.0f + rActiveEntry->mOffsetX;
        float cImageY =  90.0f + rActiveEntry->mOffsetY;
        float cDescriptionX = 518.0f;
        float cDescriptionY = 355.0f;
        if(rActiveEntry->rImage)
        {
            //--Centering.
            cImageX = cImageX - (rActiveEntry->rImage->GetTrueWidth() * cImageScale * 0.50f);

            //--Position.
            glTranslatef(cImageX, cImageY, 0.0f);
            glScalef(cImageScale, cImageScale, 1.0f);

            //--Render.
            rActiveEntry->rImage->Draw();

            //--Clean.
            glScalef(cImageScaleInv, cImageScaleInv, 1.0f);
            glTranslatef(-cImageX, -cImageY, 0.0f);
        }
        //--No image, moves the description up.
        else
        {
            cDescriptionY = 163.0f;
        }

        //--Description.
        float cDescriptionH = AdvImages.rFont_Mainline->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            AdvImages.rFont_Mainline->DrawTextArgs(cDescriptionX, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
