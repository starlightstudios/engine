//--Base
#include "CarnUIHealParty.h"

//--Classes
#include "AbyssCombat.h"
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
CarnUIHealParty::CarnUIHealParty()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUIHEALPARTY;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CHLP");

    ///--[Visiblity]
    mVisibilityTimerMax = 1;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== CarnUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== CarnUIHealParty ======= ]
    ///--[System]
    mIsClosePending = false;
    mExitsToInventory = false;
    mExitsToSkills = false;

    ///--[Inventory Item]
    mIsInventoryItem = false;
    mInventorySlot = -1;
    mItemName = NULL;

    ///--[Skill]
    mIsSkill = false;
    mSkillCharacter = -1;
    mSkillPath = NULL;
    mSkillName = NULL;

    ///--[Common]
    mDescription = NULL;
    mLabels = new StarLinkedList(true);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    ///--[Hitboxes]
    //--Hitboxes can change position based on the party size. These are not final positions.
    RegisterHitboxWH("Char0", 0.0f, 0.0f, 216.0f, 319.0f);
    RegisterHitboxWH("Char1", 1.0f, 0.0f, 216.0f, 319.0f);
    RegisterHitboxWH("Char2", 2.0f, 0.0f, 216.0f, 319.0f);
}
CarnUIHealParty::~CarnUIHealParty()
{
    free(mItemName);
    free(mSkillPath);
    free(mSkillName);
    delete mDescription;
    delete mLabels;
}
void CarnUIHealParty::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation HealParty UI", &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI",    &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Help UI",      &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
char *CarnUIHealParty::xUseItemOnCharacterScript = NULL;

///===================================== Property Queries =========================================
bool CarnUIHealParty::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_CARNUIHEALPARTY) return true;
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    return (pType == mType);
}

///======================================= Manipulators ===========================================
void CarnUIHealParty::TakeForeground()
{
    ///--[Documentation]
    //--Called when the object is first shown. Recomputes hitbox positions.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    if(!rAdvCombat) return;

    ///--[Execution]
    //--Setup.
    float cBoxSizeHf = 108.0f;

    //--Party size determines the positions of the hitboxes.
    int tPartySize = rAdvCombat->GetActivePartyCount();
    if(tPartySize == 1)
    {
        TwoDimensionReal *rHitbox0 = (TwoDimensionReal *)mHitboxList->GetElementBySlot(0);
        if(rHitbox0) rHitbox0->SetPosition((VIRTUAL_CANVAS_X * 0.50f) - cBoxSizeHf, 120.0f);
    }
    else if(tPartySize == 2)
    {
        TwoDimensionReal *rHitbox0 = (TwoDimensionReal *)mHitboxList->GetElementBySlot(0);
        if(rHitbox0) rHitbox0->SetPosition((VIRTUAL_CANVAS_X * 0.33f) - cBoxSizeHf, 120.0f);
        TwoDimensionReal *rHitbox1 = (TwoDimensionReal *)mHitboxList->GetElementBySlot(1);
        if(rHitbox1) rHitbox1->SetPosition((VIRTUAL_CANVAS_X * 0.66f) - cBoxSizeHf, 120.0f);
    }
    else if(tPartySize == 3)
    {
        TwoDimensionReal *rHitbox0 = (TwoDimensionReal *)mHitboxList->GetElementBySlot(0);
        if(rHitbox0) rHitbox0->SetPosition((VIRTUAL_CANVAS_X * 0.25f) - cBoxSizeHf, 120.0f);
        TwoDimensionReal *rHitbox1 = (TwoDimensionReal *)mHitboxList->GetElementBySlot(1);
        if(rHitbox1) rHitbox1->SetPosition((VIRTUAL_CANVAS_X * 0.50f) - cBoxSizeHf, 120.0f);
        TwoDimensionReal *rHitbox2 = (TwoDimensionReal *)mHitboxList->GetElementBySlot(2);
        if(rHitbox2) rHitbox2->SetPosition((VIRTUAL_CANVAS_X * 0.75f) - cBoxSizeHf, 120.0f);
    }
}
void CarnUIHealParty::ClearModeData()
{
    ///--[Documentation]
    //--Clear any lingering mode data. Used when changing between item and skill modes.

    ///--[Deallocate]
    free(mItemName);
    free(mSkillPath);
    free(mSkillName);

    ///--[Zero]
    mIsInventoryItem = false;
    mInventorySlot = -1;
    mItemName = NULL;
    mIsSkill = false;
    mSkillCharacter = -1;
    mSkillPath = NULL;
    mSkillName = NULL;
}
void CarnUIHealParty::SetItemMode(int pSlot)
{
    ///--[Documentation]
    //--Activates item mode, populating the item name and making sure it is a viable item. Pass
    //  an invalid slot to turn this mode off.
    ClearModeData();
    if(pSlot < 0) return;

    ///--[Execution]
    //--Get and check the item.
    AdventureInventory *rInventory;
    StarLinkedList *rItemList;
    AdventureItem *rItem;
    if(!(rInventory = AdventureInventory::Fetch())) return;
    if(!(rItemList = rInventory->GetItemList())) return;
    if(!(rItem = (AdventureItem *)rItemList->GetElementBySlot(pSlot))) return;

    //--Use the item's internal name.
    mIsInventoryItem = true;
    mInventorySlot = pSlot;
    ResetString(mItemName, rItem->GetName());

    //--Item description.
    mDescription = CreateDescriptionListFromString(rItem->GetDescription(), CarnImages.rFont_Description, 940.0f);

    //--Diagnostics.
    //fprintf(stderr, "Successfully set item to %i - %s\n", mInventorySlot, mItemName);
}
void CarnUIHealParty::SetSkillMode(int pCharIndex, const char *pSkillName, const char *pDescription)
{
    ///--[Documentation]
    //--Activates skills mode, populating the character in the active party using the skill and getting the skill
    //  data from the global listing.
    ClearModeData();
    if(pCharIndex < 0 || !pSkillName) return;

    ///--[Execution]
    //--Make sure the character exists.
    AbyssCombat *rAbyCombat;
    AdvCombatEntity *rActiveCharacter;
    if(!(rAbyCombat = AbyssCombat::Fetch())) return;
    if(!(rActiveCharacter = rAbyCombat->GetActiveMemberI(pCharIndex)))
    {
        fprintf(stderr, "CarnUIHealParty::SetSkillMode() - Warning. Character slot %i is out of range.\n", pCharIndex);
        return;
    }

    //--Make sure the skill exists.
    AdvCombatAbility *rAbility = rAbyCombat->GetGlobalAbility(pSkillName);
    if(!rAbility)
    {
        fprintf(stderr, "CarnUIHealParty::SetSkillMode() - Warning. Could not find skill %s on the global list.\n", pSkillName);
        return;
    }

    ///--[Store]
    //--All checks passed, activate mode and store data.
    mIsSkill = true;
    mSkillCharacter = pCharIndex;
    ResetString(mSkillPath, rAbility->GetScriptPath());
    ResetString(mSkillName, rAbility->GetDisplayName());

    //--Skill description.
    if(pDescription)
    {
        mDescription = CreateDescriptionListFromString(pDescription, CarnImages.rFont_Description, 940.0f);
    }

    //--Diagnostics.
    //fprintf(stderr, "Successfully set skill to %i - %s\n", pCharIndex, pSkillName);
}
void CarnUIHealParty::SetExitToInventory()
{
    mExitsToInventory = true;
}
void CarnUIHealParty::SetExitToSkills()
{
    mExitsToSkills = true;
}
void CarnUIHealParty::HandleExit()
{
    ///--[Documentation]
    //--Flags the UI to close and handles setting codes so it closes back to the inventory or skills
    //  menu depending on how it was opened.

    ///--[Execution]
    //--Common.
    FlagExit();

    //--Inventory:
    if(mExitsToInventory)
    {
        mCodeBackward = CARNMENU_MODE_INVENTORY + CARNMENU_CHANGEMODE_OFFSET;
    }
    //--Skills.
    else if(mExitsToSkills)
    {
        mCodeBackward = CARNMENU_MODE_SKILLS + CARNMENU_CHANGEMODE_OFFSET;
    }

    //--Clear flags.
    mExitsToInventory = false;
    mExitsToSkills = false;

    //--Clear labels.
    mLabels->ClearList();
}

///======================================= Core Methods ===========================================
void CarnUIHealParty::RefreshMenuHelp()
{
}
void CarnUIHealParty::RecomputeCursorPositions()
{
}
void CarnUIHealParty::ExecuteOn(int pIndex)
{
    ///--[Documentation]
    //--Fires a script that will use the given item/skill on the character in the active party that
    //  is in the given slot. It is possible for this action to fail (not enough MP, no need to use
    //  on a full HP character, etc).
    if(!xUseItemOnCharacterScript)
    {
        fprintf(stderr, "CarnUIHealParty:ExecuteOn() - Failed. No execution path set.\n");
        return;
    }

    ///--[Inventory Item Case]
    //--Inventory items provide a name and inventory slot.
    if(mIsInventoryItem)
    {
        //--Setup.
        AdventureInventory *rInventory;
        StarLinkedList *rItemList;

        //--Retrieve the item from the inventory.
        if(!(rInventory = AdventureInventory::Fetch())) return;
        if(!(rItemList = rInventory->GetItemList())) return;
        void *rCheckItem = rItemList->GetElementBySlot(mInventorySlot);

        //--Execute.
        LuaManager::Fetch()->ExecuteLuaFile(xUseItemOnCharacterScript, 3, "S", mItemName, "N", (float)pIndex, "N", (float)mInventorySlot);

        //--It is possible the inventory deleted the item, so the pointer is now considered unstable. Check if the item
        //  pointer is still in the inventory. If not, we need to close the UI.
        void *rRecheckItem = rItemList->GetElementBySlot(mInventorySlot);
        if(rRecheckItem != rCheckItem)
        {
            mIsClosePending = true;
            return;
        }

        //--If the item still exists, recheck the item name which may have changed.
        AdventureItem *rGetNameItem = (AdventureItem *)rRecheckItem;
        ResetString(mItemName, rGetNameItem->GetName());
        mDescription = CreateDescriptionListFromString(rGetNameItem->GetDescription(), CarnImages.rFont_Description, 940.0f);
    }
    ///--[Skill Case]
    //--Using a skill will (sometimes) consume SP.
    else if(mIsSkill)
    {
        //--Execute. Order of arguments is:
        //  Switch Code, Target Index, Caster Index, Inventory Slot (-1)
        LuaManager::Fetch()->ExecuteLuaFile(mSkillPath, 4, "N", (float)ACA_SCRIPT_CODE_USE_ON_UI, "N", (float)pIndex, "N", (float)mSkillCharacter, "N", -1.0f);
    }
}
void CarnUIHealParty::SpawnLabelOnCharacter(int pCharIndex, int pHealing, const char *pColor, int pDelay)
{
    ///--[Documentation]
    //--Using a character's hitbox, spawns a label.
    if(!pColor) return;

    ///--[Resolve Position]
    TwoDimensionReal *rUseHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(pCharIndex);
    if(!rUseHitbox)
    {
        fprintf(stderr, "CarnUIHealParty::SpawnLabelOnCharacter() - Warning. Character index %i is out of range.\n", pCharIndex);
        return;
    }

    ///--[Create]
    HealPartyLabel *nLabel = (HealPartyLabel *)starmemoryalloc(sizeof(HealPartyLabel));
    nLabel->Initialize();
    nLabel->mPosition.MoveTo(rUseHitbox->mXCenter - 55, rUseHitbox->mBot - 88, 0);
    nLabel->mPosition.MoveTo(rUseHitbox->mXCenter - 55, rUseHitbox->mBot -120.0f, cxLabelMoveTicks);
    nLabel->mHideTimer = pDelay;
    mLabels->AddElement("X", nLabel, &FreeThis);

    //--Hardcap on the length of the string.
    nLabel->mNumber = pHealing;

    ///--[Colors]
    if(!strcasecmp(pColor, "Green"))
    {
        nLabel->mColor.SetRGBAF(0.50f, 1.00f, 0.50f, 1.0f);
    }
    else if(!strcasecmp(pColor, "Blue"))
    {
        nLabel->mColor.SetRGBAF(1.00f, 0.25f, 0.45f, 1.0f);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIHealParty::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Updates when the object is active. Clicking a character activates the skill/item on them.
    //  Right click to exit.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, true);

    //--Update timers for all labels.
    HealPartyLabel *rLabel = (HealPartyLabel *)mLabels->SetToHeadAndReturn();
    while(rLabel)
    {
        //--Hide timer runs to zero before anything else happens.
        if(rLabel->mHideTimer > 0)
        {
            rLabel->mHideTimer --;
            rLabel = (HealPartyLabel *)mLabels->IncrementAndGetRandomPointerEntry();
            continue;
        }

        //--Advance.
        rLabel->mPosition.Increment(EASING_CODE_LINEAR);

        //--If the position is complete, increment the removal timer.
        if(rLabel->mPosition.mTimer >= rLabel->mPosition.mTimerMax)
        {
            rLabel->mLingerTimer ++;
            if(rLabel->mLingerTimer >= cxLabelExpireTicks)
            {
                mLabels->RemoveRandomPointerEntry();
            }
        }

        //--Next.
        rLabel = (HealPartyLabel *)mLabels->IncrementAndGetRandomPointerEntry();
    }

    ///--[Pending Close]
    //--A close is pending, disallow further actions.
    if(mIsClosePending)
    {
        //--When all labels have finished displaying, stop.
        if(mLabels->GetListSize() < 1)
        {
            mIsClosePending = false;
            HandleExit();
        }

        //--Either way block the update.
        return true;
    }

    ///--[Mouse Handling]
    GetMouseInfo();
    RunHitboxCheck();

    ///--[Left Click]
    //--Player left clicks. Executes on a character if clicked on.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Scan.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--Execute on the character in question.
            ExecuteOn(rReplyPack->mIndex);

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }

        //--Stop update.
        return true;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Exits the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        HandleExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    return true;
}
void CarnUIHealParty::UpdateBackground()
{
    ///--[Timers]
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIHealParty::RenderPieces(float pAlpha)
{
    ///--[Documenation]
    //--Carnation does not use by-piece rendering. This is the main rendering call.
    if(mVisibilityTimer < 1) return;

    //--Fast-access pointers.
    AdvCombat *rAdvCombat;

    //--Resolve pointers.
    if(!(rAdvCombat = AdvCombat::Fetch())) return;

    ///--[Darkening]
    //--Base UI does not render a backing in this mode. This object handles it.
    //float tBackingPct = (float)mVisibilityTimer / (float)mVisibilityTimerMax;
    //StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * tBackingPct));

    ///--[Static Parts]
    //--Common parts that always render.
    CarnImages.rFrame_Base->Draw();

    ///--[Characters]
    //--Character frames and information.
    int tActivePartySize = rAdvCombat->GetActivePartyCount();
    for(int i = 0; i < tActivePartySize; i ++)
    {
        //--Get hitbox.
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(i);
        if(!rHitbox) continue;

        //--Frame.
        //CarnImages.rFrame_Character->Draw(rHitbox->mLft, rHitbox->mTop);

        //--Character information.
        AdvCombatEntity *rCharacter = rAdvCombat->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--Make sure the character is a CarnationCombatEntity.
        if(!rCharacter->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) continue;
        CarnationCombatEntity *rCarnCharacter = (CarnationCombatEntity *)rCharacter;

        //--Render portrait.
        rCarnCharacter->UpdateTimers();
        rCarnCharacter->RenderPortraitAt(rHitbox->mLft, rHitbox->mTop, false);
    }

    ///--[Labels]
    //--Setup.
    float cHei = CarnImages.rFont_Description->GetTextHeight();

    //--Inventory item mode:
    if(mIsInventoryItem)
    {
        //--Name.
        CarnImages.rFont_Mainline->DrawText(212.0f, 437.0f, 0, 1.0f, mItemName);

        //--Description.
        RenderDescription(212.0f, 437.0f + cHei, cHei, mDescription, CarnImages.rFont_Description);
    }
    //--Skill mode:
    else
    {
        //--Name.
        CarnImages.rFont_Mainline->DrawText(212.0f, 437.0f, 0, 1.0f, mSkillName);

        //--Description.
        RenderDescription(212.0f, 437.0f + cHei, cHei, mDescription, CarnImages.rFont_Description);
    }

    //--Common labels.
    CarnImages.rFont_Mainline->DrawText(212.0f, 574.0f,      0, 1.0f, "Left-click a character to use this item on them.");
    CarnImages.rFont_Mainline->DrawText(212.0f, 574.0f+cHei, 0, 1.0f, "Right-click to exit.");

    ///--[Dynamic Labels]
    //--These appear and then fade out to indicate changes in player state.
    HealPartyLabel *rModLabel = (HealPartyLabel *)mLabels->PushIterator();
    while(rModLabel)
    {
        //--If this value is positive, do not render.
        if(rModLabel->mHideTimer > 0)
        {
            rModLabel = (HealPartyLabel *)mLabels->AutoIterate();
            continue;
        }

        //--Resolve color.
        //float tAlpha = 1.0f;
        //if(rModLabel->mLingerTimer >= cxLabelFadeTicks)
        //{
        //    tAlpha = 1.0f - ((rModLabel->mLingerTimer - cxLabelFadeTicks) / (float)(cxLabelExpireTicks - cxLabelFadeTicks));
        //}

        //--Render.
        //rModLabel->mColor.SetAsMixerAlpha(tAlpha);
        CarnationCombatEntity::RenderThreeDigitNumber(rModLabel->mNumber, rModLabel->mPosition.mXCur, rModLabel->mPosition.mYCur, CarnImages.rOverlay_Numbers);
        //CarnImages.rFont_Recovery->DrawTextArgs(rModLabel->mPosition.mXCur, rModLabel->mPosition.mYCur, SUGARFONT_NOCOLOR | SUGARFONT_AUTOCENTER_XY, 2.0f, rModLabel->mText);

        //--Next.
        rModLabel = (HealPartyLabel *)mLabels->AutoIterate();
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
CarnUIHealParty *CarnUIHealParty::Fetch()
{
    ///--[Documentation]
    //--Locates and returns the CarnationMenu version of this class. It is possible this doesn't exist
    //  so error-check the results.
    AdventureMenu *rAdventureMenu = AdventureMenu::Fetch();
    StarUIPiece *rStarUIPiece = rAdventureMenu->RetrieveUI("Heal Party", POINTER_TYPE_CARNUIHEALPARTY, NULL, "CarnUIHealParty::Fetch()");
    if(!rStarUIPiece) return NULL;

    //--Dynamic cast.
    CarnUIHealParty *rThisUI = dynamic_cast<CarnUIHealParty *>(rStarUIPiece);
    return rThisUI;
}

///======================================== Lua Hooking ===========================================
void CarnUIHealParty::HookToLuaState(lua_State *pLuaState)
{
    /* [System]
       CarnUIHealParty_SetProperty("Use On Character Path", sPath) (Static)
       Sets the requested property in the CarnUIHealParty. */
    lua_register(pLuaState, "CarnUIHealParty_SetProperty", &Hook_CarnUIHealParty_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnUIHealParty_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    //CarnUIHealParty_SetProperty("Use On Character Path", sPath) (Static)

    ///--[Dynamic]
    //--[Labels]
    //CarnUIHealParty_SetProperty("Register Label", iCharIndex, iHPRestored, sColor, iDelay)

    ///--[ =========== Arg Resolve ============ ]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnUIClassChange_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Statics.
    if(!strcasecmp(rSwitchType, "Use On Character Path") && tArgs >= 2)
    {
        ResetString(CarnUIHealParty::xUseItemOnCharacterScript, lua_tostring(L, 2));
        return 0;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("CarnUIHealParty_SetProperty", L);

    //--Get the sub-object.
    CarnUIHealParty *rUIObject = CarnUIHealParty::Fetch();
    if(!rUIObject) return LuaTypeError("CarnUIHealParty_SetProperty", L);

    ///--[System]
    //--Registers a new label.
    if(!strcasecmp(rSwitchType, "Register Label") && tArgs >= 5)
    {
        rUIObject->SpawnLabelOnCharacter(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4), lua_tointeger(L, 5));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnUIHealParty_SetProperty", rSwitchType, tArgs);
    }

    //--Success.
    return 0;
}
