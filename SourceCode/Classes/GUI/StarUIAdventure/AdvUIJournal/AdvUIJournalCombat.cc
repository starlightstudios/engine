//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvUIJournalTypes.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
void AdvUIJournal::SetJournalToCombatMode()
{
    //--Clear the list.
    ClearCombatGlossary();

    //--Variables.
    mCurrentMode = ADVMENU_JOURNAL_MODE_COMBATGLOSSARY;

    //--Run this script to set the glossary's properties.
    LuaManager::Fetch()->ExecuteLuaFile(xCombatResolveScript);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIJournal::CreateCombatGlossaryEntry(const char *pInternalName, const char *pDisplayName)
{
    //--Arg check.
    if(!pInternalName || !pDisplayName) return;

    //--Duplicate check.
    if(mCombatEntries->GetElementByName(pInternalName)) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuJournalCombatGlossaryEntry *nEntry = (AdvMenuJournalCombatGlossaryEntry *)starmemoryalloc(sizeof(AdvMenuJournalCombatGlossaryEntry));
    nEntry->Initialize();
    ResetString(nEntry->mInternalName, pInternalName);
    ResetString(nEntry->mDisplayName, pDisplayName);
    mCombatEntries->AddElementAsTail(pInternalName, nEntry, &AdvMenuJournalCombatGlossaryEntry::DeleteThis);
}
void AdvUIJournal::AllocateCombatGlossaryPieces(const char *pInternalName, int pTotal)
{
    //--Error check.
    AdvMenuJournalCombatGlossaryEntry *rEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->AllocatePieces(pTotal);
}
void AdvUIJournal::AllocateCombatGlossaryText(const char *pInternalName, int pSlot, int pEntries, float pX, float pY)
{
    //--Error check.
    AdvMenuJournalCombatGlossaryEntry *rEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rEntry->mPiecesTotal) return;

    //--Set.
    rEntry->mPieces[pSlot].mX = pX;
    rEntry->mPieces[pSlot].mY = pY;
    rEntry->mPieces[pSlot].AllocateDescriptionLines(pEntries);
}
void AdvUIJournal::SetCombatGlossaryText(const char *pInternalName, int pSlot, int pLine, const char *pText, const char *pFont, int pFlags)
{
    //--Error check.
    AdvMenuJournalCombatGlossaryEntry *rEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pText || !pFont) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rEntry->mPiecesTotal) return;
    if(pLine < 0 || pLine >= rEntry->mPieces[pSlot].mLinesTotal) return;

    //--Set.
    ResetString(rEntry->mPieces[pSlot].mLines[pLine], pText);
    rEntry->mPieces[pSlot].mFlags[pLine] = pFlags;

    //--Resolve font.
    if(!strcasecmp(pFont, "Big Header"))
    {
        rEntry->mPieces[pSlot].mrFonts[pLine] = AdvImages.rFont_BigHeader;
    }
    else if(!strcasecmp(pFont, "Header"))
    {
        rEntry->mPieces[pSlot].mrFonts[pLine] = AdvImages.rFont_Header;
    }
    else
    {
        rEntry->mPieces[pSlot].mrFonts[pLine] = AdvImages.rFont_Mainline;
    }
}
void AdvUIJournal::SetCombatGlossaryAuto(const char *pInternalName, int pSlot, int pX, int pY, const char *pText, const char *pFont, int pFlags)
{
    ///--[Documentation]
    //--Allocates and sets lines required for a description set automatically using internal breakpoints.
    AdvMenuJournalCombatGlossaryEntry *rEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pText) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rEntry->mPiecesTotal) return;

    ///--[Run Subdivide]
    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pText, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS, NULL, -1.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "AdvUIJournal::SetLocationDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->mPieces[pSlot].mX = pX;
    rEntry->mPieces[pSlot].mY = pY;
    rEntry->mPieces[pSlot].AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        const char *rString = (const char *)tStringList->GetElementBySlot(i);
        SetCombatGlossaryText(pInternalName, pSlot, i, rString, pFont, pFlags);
    }

    //--Clean the list up.
    delete tStringList;
}
void AdvUIJournal::SetCombatGlossaryImage(const char *pInternalName, int pSlot, const char *pDLPath, float pX, float pY)
{
    //--Error check.
    AdvMenuJournalCombatGlossaryEntry *rEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDLPath) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rEntry->mPiecesTotal) return;

    //--Set.
    rEntry->mPieces[pSlot].mX = pX;
    rEntry->mPieces[pSlot].mY = pY;
    rEntry->mPieces[pSlot].rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvUIJournal::ClearCombatGlossary()
{
    mCombatEntries->ClearList();
}

///======================================= Core Methods ===========================================
void AdvUIJournal::ComputeCombatCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mCombatCursor - mCombatOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mCombatEntries->GetElementBySlot(mCombatCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = AdvImages.rFont_Mainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIJournal::RenderCombatMode(float pVisAlpha)
{
    ///--[Glossary Page Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mCombatEntries, mCombatOffset, mCombatCursor, pVisAlpha);

    ///--[Active Glossary Page]
    //--Render the information about the highlighted quest.
    float cTextHei = AdvImages.rFont_Mainline->GetTextHeight();
    AdvMenuJournalCombatGlossaryEntry *rActiveEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementBySlot(mCombatCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        //--Iterate across all pieces.
        for(int i = 0; i < rActiveEntry->mPiecesTotal; i ++)
        {
            //--Has an image:
            if(rActiveEntry->mPieces[i].rImage)
            {
                rActiveEntry->mPieces[i].rImage->Draw(rActiveEntry->mPieces[i].mX, rActiveEntry->mPieces[i].mY);
            }

            //--Render any text:
            for(int p = 0; p < rActiveEntry->mPieces[i].mLinesTotal; p ++)
            {
                if(!rActiveEntry->mPieces[i].mrFonts[p]) continue;
                rActiveEntry->mPieces[i].mrFonts[p]->DrawTextArgs(rActiveEntry->mPieces[i].mX, rActiveEntry->mPieces[i].mY + (cTextHei * p), rActiveEntry->mPieces[i].mFlags[p], 1.0f, rActiveEntry->mPieces[i].mLines[p]);
            }
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
