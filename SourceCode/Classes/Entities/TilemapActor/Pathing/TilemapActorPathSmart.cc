//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"
#include "PathrunnerTile.h"
#include "TiledLevel.h"
#include "TileLayer.h"

//--CoreClasses
//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Local Definitions]

///--[Local Structures]
//--Pathing Pack. Represents a pathing pulse.
typedef struct PathPack
{
    int mPulseVal;
    int mX;
    int mY;
}PathPack;

///--[Debug]
//#define TA_SMART_PATH_DEBUG
#ifdef TA_SMART_PATH_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///======================================= Smart Pathing ==========================================
void TilemapActor::HandleSmartPathing()
{
    ///--[ =========== Documentation ========== ]
    //--When the entity is using smart pathing, it will attempt to create a path network to the target
    //  instead of walking directly towards it.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;
    DebugPush(true, "Entity begins smart pathing.\n");

    ///--[ ============ Path Exists =========== ]
    //--If we already have built the needed path, use that.
    if(mSmartPatrolInstructions->GetListSize() > 0)
    {
        HandleSmartMoving(mSmartPatrolInstructions, mSmartPatrolDistanceLeft);
        DebugPop("End, moving.\n");
        return;
    }

    ///--[ ==== Path Needs Building, Setup ==== ]
    //--We need to build a path between where we are now, and the target. This is done via a pulse
    //  method. It requires the level to create a spreading set of movement costs from the origin
    //  point and stop when it reaches the starting point. Then it walks along the lowest movement
    //  cost path and uses that to build the path.
    if(!mPathRunner)
    {
        //--Get the next patrol node we're pathing to.
        TwoDimensionReal tDimensions = rActiveLevel->GetPatrolNode(mPatrolNodeList[mPatrolNodeCurrent]);
        int tTargetTileX = tDimensions.mXCenter / TileLayer::cxSizePerTile;
        int tTargetTileY = tDimensions.mYCenter / TileLayer::cxSizePerTile;

        //--Create the path runner.
        mPathRunner = new PathrunnerTile();
        mPathRunner->Initialize(mTileX, mTileY, tTargetTileX, tTargetTileY);
        mPathRunner->SetIterationsPerCycle(50);

        //--Reset flags.
        mPathTicksSoFar = 0;
        mSmartPatrolDistanceLeft = TileLayer::cxSizePerTile;
    }

    //--If we've exceed our path pulses this tick, stop. If several entities try to run their pathing at the
    //  same time, it can cause lag, so instead this flag stops path checks after a certain number have run.
    //  Entities will just stand still and wait for pathing to free up.
    if(xActorPathPulsesThisTick > TA_MAX_PATH_PULSES)
    {
        DebugPop("End, reached path pulse cap.\n");
        return;
    }

    //--Run. The result can legally be NULL.
    StarLinkedList *tResultList = mPathRunner->RunPathingIntoList();
    mPathTicksSoFar ++;

    ///--[Error Checking]
    //--If the path runner failed for some reason, stop here. Switch to dumb wander pathing.
    if(mPathRunner->IsFailed())
    {
        mIsSmartPatrol = false;
        delete mPathRunner;
        mPathRunner = NULL;
        DebugPop("End, failed.\n");
        return;
    }

    //--If the path runner didn't have a path yet, we can run it again until it does.
    if(!tResultList)
    {
        DebugPop("End, no path available yet.\n");
        return;
    }

    //--If the path runner finished sooner than the minimum ticks, and has pulses remaining, let it
    //  keep running those pulses. This can result in better paths.
    if(mPathTicksSoFar < mMinimumPathTicks && mPathRunner->HasPulsesRemaining())
    {
        delete tResultList;
        DebugPop("End, minimum ticks not reached.\n");
        return;
    }

    ///--[Success, Store]
    //--The path list was passed back successfully. Store it.
    delete mSmartPatrolInstructions;
    mSmartPatrolInstructions = tResultList;

    //--Clean up the path runner.
    delete mPathRunner;
    mPathRunner = NULL;
    DebugPop("End, created path.\n");
}

///======================================= Smart Moving ===========================================
void TilemapActor::HandleSmartMoving(StarLinkedList *pList, float &sDistanceVar)
{
    ///--[Documentation]
    //--If the smart pather currently has moves available, handles the act of moving.
    if(!pList) return;

    //--Zeroth entry. If it doesn't exist, assume the move is complete.
    int *rDirection = (int *)pList->GetElementBySlot(0);
    if(!rDirection)
    {
        DecideNextPathDestination();
        return;
    }

    //--Setup.
    float tTargetX = mTrueX;
    float tTargetY = mTrueY;

    //--Resolve the target position.
    if(*rDirection == DIR_NORTH) tTargetY = tTargetY - sDistanceVar;
    if(*rDirection == DIR_SOUTH) tTargetY = tTargetY + sDistanceVar;
    if(*rDirection == DIR_WEST)  tTargetX = tTargetX - sDistanceVar;
    if(*rDirection == DIR_EAST)  tTargetX = tTargetX + sDistanceVar;

    ///--[Movement Finished]
    //--Move to the target. If the move completes:
    int tStoreMove = mMoveTimer;
    if(HandleMoveTo(tTargetX, tTargetY, mMoveSpeed * 0.40f))
    {
        //--Pop the patrol instructions.
        pList->DeleteHead();

        //--Path distance is equal to one tile.
        sDistanceVar = TileLayer::cxSizePerTile;

        //--Set this as our leashing point.
        SetLeashingPoint(mTrueX, mTrueY);

        //--If there are no instructions left, we reached the next node.
        if(pList->GetListSize() < 1)
        {
            DecideNextPathDestination();
        }
    }
    ///--[Movement Incomplete]
    //--Not at the location yet.
    else
    {
        //--Flags.
        mIsMoving = true;
        if(mMoveTimer != tStoreMove) mMoveTimer = tStoreMove + 1;

        //--Decrement this timer. If it reaches zero, we stop and look around a bit. This does not occur if the
        //  no-stop flag is set.
        mPatrolTicksLeft --;
        if(mPatrolTicksLeft < 1 && !mUseStopOnEachNode && !mNeverPauses)
        {
            mPatrolTicksLeft = (rand() % 360) + 90;
            mAIState = TA_AI_WAIT;
            mWaitTimer = 0;
            mWaitTimerMax = rand() % 60 + 60;
        }

        //--Set this as our leashing point.
        SetLeashingPoint(mTrueX, mTrueY);

        //--Get the distance between our position and the target. Store that as the distance remaining.
        sDistanceVar = GetPlanarDistance(mTrueX, mTrueY, tTargetX, tTargetY);
    }
}
