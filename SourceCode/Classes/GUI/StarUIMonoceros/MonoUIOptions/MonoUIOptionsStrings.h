///=================================== MonoUIOptionsStrings =======================================
//--Translation strings for the options menu. There's so many it gets its own file in order to avoid
//  clogging up the UI header file.
#pragma once

///======================================== Translation ===========================================
///--[MonoUIOptions_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIOPTIONS_STRINGS_TOTAL 170
class MonoUIOptions_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIOPTIONS_STRINGS_TOTAL];
        }mem;
        struct
        {
            //--Option Strings
            char *mTextSettings;
            char *mTextGameplay;
            char *mTextAudio;
            char *mTextControls;
            char *mTextDebug;
            char *mTextDefaults;
            char *mTextOkay;
            char *mTextChangesPending;
            char *mTextSomeChangesRequireAProgramRestart;
            char *mTextYouHaveUnsavedChangesPending;
            char *mTextReallyExit;
            char *mTextPressNewPrimaryKeyNow;
            char *mTextPressNewSecondaryKeyNow;
            char *mTextHelpMenu;
            char *mTextTouristMode;
            char *mTextAutoHastenDialogue;
            char *mTextCombatMemoryCursor;
            char *mTextMaxAutosaves;
            char *mTextAllowAssetStreaming;
            char *mTextUseRAMLoading;
            char *mTextUnloadActorsAfterDialogue;
            char *mTextUseFadingUI;
            char *mTextResolution;
            char *mTextCensorIzana;
            char *mTextCensorCaelyn;
            char *mTextCensorCyrano;
            char *mTextCensorEnemies;
            char *mTextMusicVolume;
            char *mTextSoundVolume;
            char *mTextIzanaVoiceVolume;
            char *mTextCaelynVoiceVolume;
            char *mTextCyranoVoiceVolume;
            char *mTextSaveVolumesInSavefile;
            char *mTextActivateAccept;
            char *mTextCancelOpenMenu;
            char *mTextRun;
            char *mTextUp;
            char *mTextRight;
            char *mTextDown;
            char *mTextLeft;
            char *mTextShoulderLeft;
            char *mTextShoulderRight;
            char *mTextAbility1;
            char *mTextAbility2;
            char *mTextAbility3;
            char *mTextAbility4;
            char *mTextAbility5;
            char *mTextEmergencyEscape;
            char *mTextOpenDebugMenu;
            char *mTextToggleFuilscreen;
            char *mTextRebuildKerning;
            char *mTextRebuildShaders;
            char *mTextDisableShaderOutline;
            char *mTextDisableShaderUnderwater;
            char *mTextDisableShaderLighting;
            char *mTextDebugControlsMayNotBeRebound;
            char *mTextTheyAreHereForReference;
            char *mTextTrue;
            char *mTextFalse;
            char *mTextUnbound;
            char *mTextControlsSavedImmediately;

            //--Help Strings
            char *mTextHelp[6];

            //--Control Strings
            char *mTextControlShowHelp;
            char *mTextControlDiscardChanges;
            char *mTextControlReturnToMenu;
            char *mTextControlDetails;
            char *mTextControlPrevPage;
            char *mTextControlNextPage;
            char *mTextControlBindPrimary;
            char *mTextControlBindSecondary;
            char *mTextControlControlDefaults;
            char *mTextControlAllControlDefaults;

            //--Options Descriptions
            char *mOptionHelpTourist[4];
            char *mOptionHelpHasten[2];
            char *mOptionHelpMemory[2];
            char *mOptionHelpAutosaves[3];
            char *mOptionHelpAssetStreaming[6];
            char *mOptionHelpRAMLoading[6];
            char *mOptionHelpUnloadActors[5];
            char *mOptionHelpFadingUI[2];
            char *mOptionHelpResolution[2];
            char *mOptionHelpCensorI[4];
            char *mOptionHelpCensorA[4];
            char *mOptionHelpCensorY[4];
            char *mOptionHelpCensorE[2];
            char *mOptionHelpMusicVol[2];
            char *mOptionHelpSoundVol[2];
            char *mOptionHelpIzanaVol[2];
            char *mOptionHelpCaelynVol[2];
            char *mOptionHelpCyranoVol[2];
            char *mOptionHelpAudioInSaves[3];
            char *mOptionHelpControlActivate[3];
            char *mOptionHelpControlCancel[2];
            char *mOptionHelpControlRun[1];
            char *mOptionHelpControlUp[1];
            char *mOptionHelpControlRight[1];
            char *mOptionHelpControlDown[1];
            char *mOptionHelpControlLeft[1];
            char *mOptionHelpControlShoulderLeft[1];
            char *mOptionHelpControlShoulderRight[1];
            char *mOptionHelpControlAbility1[1];
            char *mOptionHelpControlAbility2[1];
            char *mOptionHelpControlAbility3[2];
            char *mOptionHelpControlAbility4[1];
            char *mOptionHelpControlAbility5[1];
            char *mOptionHelpEmergencyEscape[1];
            char *mOptionHelpDebugOpenMenu[3];
            char *mOptionHelpDebugFullscreen[3];
            char *mOptionHelpDebugKerning[2];
            char *mOptionHelpDebugShaders[1];
            char *mOptionHelpDebugOutline[2];
            char *mOptionHelpDebugUnderwater[2];
            char *mOptionHelpDebugLightning[2];
        }str;
    };

    //--Functions
    MonoUIOptions_Strings();
    virtual ~MonoUIOptions_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};
