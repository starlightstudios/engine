//--Base
#include "CSteamStats.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

#if defined _STEAM_API_
CSteamStats::CSteamStats(Stat_t *Stats, int NumStats) : m_iAppID( 0 ), m_bInitialized( false ), m_CallbackUserStatsReceived( this, &CSteamStats::OnUserStatsReceived ), m_CallbackUserStatsStored( this, &CSteamStats::OnUserStatsStored )
{
	m_iAppID = SteamUtils()->GetAppID();
	m_pStats = Stats;
	m_iNumStats = NumStats;
	//RequestStats();
}
bool CSteamStats::RequestStats()
{
	//fprintf(stderr, "==> Called request stats.\n");
	// Is Steam loaded? If not we can't get stats.
	if ( NULL == SteamUserStats() || NULL == SteamUser() )
	{
		return false;
	}
	// Is the user logged on?  If not we can't get stats.
	if ( !SteamUser()->BLoggedOn() )
	{
		return false;
	}
	// Request user stats.
	//fprintf(stderr, "    Issued request.\n");
	return true;//SteamUserStats()->RequestCurrentStats();
}
bool CSteamStats::StoreStats()
{
	//fprintf(stderr, "==> Called store stats.\n");
	if(!m_bInitialized) return false;

    // load stats
    for(int iStat = 0; iStat < m_iNumStats; ++iStat)
    {
        Stat_t &stat = m_pStats[iStat];
        switch (stat.m_eStatType)
        {
        case STAT_INT:
            SteamUserStats()->SetStat( stat.m_pchStatName, (int32)stat.m_iValue );
            break;

        case STAT_FLOAT:
            SteamUserStats()->SetStat( stat.m_pchStatName, (float)stat.m_flValue );
            break;

        case STAT_AVGRATE:
            SteamUserStats()->UpdateAvgRateStat(stat.m_pchStatName, stat.m_flAvgNumerator, stat.m_flAvgDenominator );
            // The averaged result is calculated for us
            SteamUserStats()->GetStat(stat.m_pchStatName, &stat.m_flValue );
            break;

        default:
            break;
        }
    }

    return SteamUserStats()->StoreStats();
}
void CSteamStats::OnUserStatsReceived( UserStatsReceived_t *pCallback )
{
	// we may get callbacks for other games' stats arriving, ignore them
	//fprintf(stderr, "==> Stats received.\n");
	if(m_iAppID != pCallback->m_nGameID)
    {
        //fprintf(stderr, "    Incorrect app ID.\n");
        return;
    }
	//fprintf(stderr, "    Correct App ID.\n");

    if(k_EResultOK == pCallback->m_eResult)
    {
        //OutputDebugString( "Received stats and achievements from Steam\n" );
        // load stats
        for ( int iStat = 0; iStat < m_iNumStats; ++iStat )
        {
            Stat_t &stat = m_pStats[iStat];
            switch (stat.m_eStatType)
            {
            case STAT_INT:
                SteamUserStats()->GetStat(stat.m_pchStatName, &stat.m_iValue);
                break;

            case STAT_FLOAT:
            case STAT_AVGRATE:
                SteamUserStats()->GetStat(stat.m_pchStatName, &stat.m_flValue);
                break;

            default:
                break;
            }
        }
        m_bInitialized = true;
    }
    else
    {
        char buffer[128];
        _snprintf( buffer, 128, "RequestStats - failed, %d\n", pCallback->m_eResult );
        //OutputDebugString( buffer );
    }
	fprintf(stderr, "    Finished.\n");
}
void CSteamStats::OnUserStatsStored( UserStatsStored_t *pCallback )
{
	// we may get callbacks for other games' stats arriving, ignore them
	//fprintf(stderr, "==> Stats stored.\n");
	return;
	if ( m_iAppID != pCallback->m_nGameID )
    {
       // fprintf(stderr, "    Incorrect game ID.\n");
        return;
    }
    return;

	{
		if ( k_EResultOK == pCallback->m_eResult )
		{
			//OutputDebugString( "StoreStats - success\n" );
		}
		else if ( k_EResultInvalidParam == pCallback->m_eResult )
		{
			// One or more stats we set broke a constraint. They've been reverted,
			// and we should re-iterate the values now to keep in sync.
			//OutputDebugString( "StoreStats - some failed to validate\n" );
			// Fake up a callback here so that we re-load the values.
			UserStatsReceived_t callback;
			callback.m_eResult = k_EResultOK;
			callback.m_nGameID = m_iAppID;
			OnUserStatsReceived( &callback );
		}
		else
		{
			char buffer[128];
			_snprintf( buffer, 128, "StoreStats - failed, %d\n", pCallback->m_eResult );
			//OutputDebugString( buffer );
		}
	}
	//fprintf(stderr, "    Stats store finished.\n");
}
#endif
