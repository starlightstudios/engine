-- |[ ================================ FontProto List Handling ================================= ]|
--Handles global list functions.

-- |[ ============================= FontProto:fnClearGlobalList() ============================== ]|
--Clears the global MapPinSet list.
function FontProto:fnClearGlobalList()
    gzaFontProtoList = {}
end

-- |[ ============================ FontProto:fnRegisterPrototype() ============================= ]|
--Returns the named prototype, or nil if not found.
function FontProto:fnRegisterPrototype(pzPrototype)
    table.insert(gzaFontProtoList, pzPrototype)
end
    
-- |[ ============================= FontProto:fnLocatePrototype() ============================== ]|
--Returns the named prototype, or nil if not found.
function FontProto:fnLocatePrototype(psName)

	-- |[Arg Check]|
    if(psName == nil) then return nil end

    -- |[Scan]|
    for i = 1, #gzaFontProtoList, 1 do
        if(gzaFontProtoList[i].sLocalName == psName) then 
            return gzaFontProtoList[i]
        end
    end

    -- |[Not Found]|
    return nil
end

