//--Base
#include "MonoUIJournal.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

void MonoUIJournal::RenderBestiaryMode(float pVisAlpha)
{
    ///--[Enemy Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mBestiaryEntries, mBestiaryOffset, mBestiaryCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the image and information.
    AdvMenuJournalBestiaryEntry *rActiveEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementBySlot(mBestiaryCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Portrait]
        //--Resolve image.
        StarBitmap *rRenderImg = rActiveEntry->rImage;
        if(mShowParagon) rRenderImg = rActiveEntry->rParagon;

        //--Render the portrait behind a stencil.
        if(rRenderImg && mBestiaryImageTimer < ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS)
        {
            //--Portrait can fade out.
            float cPct = EasingFunction::QuadraticInOut(mBestiaryImageTimer, ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS);
            if(cPct > 0.0f) StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f - cPct);

            //--Render portrait.
            rRenderImg->Draw(rActiveEntry->mPortraitX, rActiveEntry->mPortraitY);

            //--Disable stencils.
            //glDisable(GL_STENCIL_TEST);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
        }

        ///--[Static Parts]
        //--Reset alpha mixer.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Render all static parts over the portrait.
        MonoImages.rOverlayInfo->Draw();
        MonoImages.rOverlayInfoName->Draw();
        MonoImages.rOverlayIcons->Draw();

        ///--[Basic Stats]
        //--Setup.
        float cStatsLft = 539.0f;
        float cStatsTop = 155.0f;
        float cStatsHei =  20.0f;

        //--Level. Always visible.
        MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 0.0f), 0, 1.0f, "%i", rActiveEntry->mLevel);

        //--If the basic stats are hidden:
        if(!rActiveEntry->mShowStatistics)
        {
            for(int i = 1; i < 6; i ++) MonoImages.rFontStatistics->DrawText(cStatsLft, cStatsTop + (cStatsHei * i), 0, 1.0f, "???");
            MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 6.0f), 0, 1.0f, "%i", rActiveEntry->mPlatina);
        }
        //--Basic stats are known:
        else
        {
            MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 1.0f), 0, 1.0f, "%i", rActiveEntry->mHP);
            MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 2.0f), 0, 1.0f, "%i", rActiveEntry->mAtk);
            MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 3.0f), 0, 1.0f, "%i", rActiveEntry->mIni);
            MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 4.0f), 0, 1.0f, "%i", rActiveEntry->mAcc);
            MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 5.0f), 0, 1.0f, "%i", rActiveEntry->mEvd);
            //MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 6.0f), 0, 1.0f, "%i", rActiveEntry->mPrt);
            MonoImages.rFontStatistics->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 6.0f), 0, 1.0f, "%i", rActiveEntry->mPlatina);
        }

        ///--[Resistances]
        //--Setup.
        float cResistsLft = 539.0f;
        float cResistsTop = 622.0f;
        float cResistsHei =  20.0f;

        //--Resistances are hidden:
        if(!rActiveEntry->mShowResistances)
        {
            for(int i = 0; i < 4; i ++) MonoImages.rFontStatistics->DrawText(cResistsLft, cResistsTop + (cResistsHei * i), 0, 1.0f, "???");
        }
        //--Resistances are known:
        else
        {
            RenderResistance(cResistsLft, cResistsTop + (cResistsHei *  0.0f), 0, 1.0f, rActiveEntry->mSlashRes,  MonoImages.rFontStatistics);
            RenderResistance(cResistsLft, cResistsTop + (cResistsHei *  1.0f), 0, 1.0f, rActiveEntry->mStrikeRes, MonoImages.rFontStatistics);
            RenderResistance(cResistsLft, cResistsTop + (cResistsHei *  2.0f), 0, 1.0f, rActiveEntry->mPierceRes, MonoImages.rFontStatistics);
            RenderResistance(cResistsLft, cResistsTop + (cResistsHei *  3.0f), 0, 1.0f, rActiveEntry->mFlameRes,  MonoImages.rFontStatistics);
        }

        ///--[Defeats / Scenes]
        //--Setup. This is bottom-aligned.
        float cMiscLft = 1010.0f;
        float cMiscTop =  150.0f;

        //--String.
        char *tRenderString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mTextTimesDefeated, 2, "[iCount]", rActiveEntry->mDefeatCount, "[iMax]", rActiveEntry->mDefeatMax);

        //--How many times the player has defeated this enemy.
        mMonoParagraphColor.SetAsMixerAlpha(pVisAlpha);
        MonoImages.rFontStatistics->DrawTextArgs(cMiscLft, cMiscTop, 0, 1.0f, tRenderString);

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
        free(tRenderString);

        ///--[Name, Description]
        //--Name.
        float cNameX = 1016.0f;
        float cNameY =  260.0f;
        mMonoParagraphColor.SetAsMixerAlpha(pVisAlpha);
        MonoImages.rFontHeading->DrawTextFixed(cNameX, cNameY, 256.0f, 0, rActiveEntry->mDisplayName);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Description.
        float cDescriptionX = 909.0f;
        float cDescriptionY = 329.0f;
        float cDescriptionH = MonoImages.rFontDescription->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, pVisAlpha);
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            MonoImages.rFontDescription->DrawTextArgs(cDescriptionX, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        ///--[Special: Show Full Portrait]
        //--If the player presses a button, they can see the full size image of the enemy.
        if(rRenderImg && mBestiaryImageTimer > 0)
        {
            //--Percentage.
            float cPct = EasingFunction::QuadraticInOut(mBestiaryImageTimer, ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS);

            //--Darkening overlay.
            StarBitmap::DrawFullBlack((cPct * 3.0f) * (0.90f / 3.0f));

            //--Compute position.
            float cIdealX = (VIRTUAL_CANVAS_X * 0.50f) - (rRenderImg->GetTrueWidth()  * 0.50f);
            float cIdealY = (VIRTUAL_CANVAS_Y * 0.50f) - (rRenderImg->GetTrueHeight() * 0.50f);
            float cPortraitX = cIdealX  - ((cIdealX - rActiveEntry->mPortraitX) * (1.0f - cPct));
            float cPortraitY = cIdealY  - ((cIdealY - rActiveEntry->mPortraitY) * (1.0f - cPct));

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);
            rRenderImg->Draw(cPortraitX, cPortraitY);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
        }
    }
}
void MonoUIJournal::RenderCombatMode(float pVisAlpha)
{
    ///--[Glossary Page Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mCombatEntries, mCombatOffset, mCombatCursor, pVisAlpha);

    ///--[Active Glossary Page]
    //--Render the information about the highlighted quest.
    float cTextHei = MonoImages.rFontMainline->GetTextHeight();
    AdvMenuJournalCombatGlossaryEntry *rActiveEntry = (AdvMenuJournalCombatGlossaryEntry *)mCombatEntries->GetElementBySlot(mCombatCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        //--Iterate across all pieces.
        for(int i = 0; i < rActiveEntry->mPiecesTotal; i ++)
        {
            //--Has an image:
            if(rActiveEntry->mPieces[i].rImage)
            {
                rActiveEntry->mPieces[i].rImage->Draw(rActiveEntry->mPieces[i].mX, rActiveEntry->mPieces[i].mY);
            }

            //--Render any text:
            for(int p = 0; p < rActiveEntry->mPieces[i].mLinesTotal; p ++)
            {
                if(!rActiveEntry->mPieces[i].mrFonts[p]) continue;
                rActiveEntry->mPieces[i].mrFonts[p]->DrawTextArgs(rActiveEntry->mPieces[i].mX, rActiveEntry->mPieces[i].mY + (cTextHei * p), rActiveEntry->mPieces[i].mFlags[p], 1.0f, rActiveEntry->mPieces[i].mLines[p]);
            }
        }
    }
}
void MonoUIJournal::RenderLocationsMode(float pVisAlpha)
{
    ///--[Location Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mLocationEntries, mLocationOffset, mLocationCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the information about the highlighted quest.
    AdvMenuJournalLocationEntry *rActiveEntry = (AdvMenuJournalLocationEntry *)mLocationEntries->GetElementBySlot(mLocationCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Title]
        //--Backing.
        MonoImages.rOverlayQuestShade->Draw();

        //--Title goes in the top middle of the description pane.
        float cTitleX = 895.0f;
        float cTitleY = 173.0f;
        mMonoTitleColor.SetAsMixerAlpha(pVisAlpha);
        MonoImages.rFontHeading->DrawTextArgs(cTitleX, cTitleY, SUGARFONT_AUTOCENTER_X, 1.0f, rActiveEntry->mDisplayName);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Title Image.
        float cImageScale = 0.50f;
        float cImageScaleInv = 1.0f / cImageScale;
        float cImageX = 918.0f + rActiveEntry->mOffsetX;
        float cImageY =  90.0f + rActiveEntry->mOffsetY;
        float cDescriptionX = 518.0f;
        float cDescriptionY = 408.0f;
        if(rActiveEntry->rImage)
        {
            //--Centering.
            cImageX = cImageX - (rActiveEntry->rImage->GetTrueWidth() * cImageScale * 0.50f);

            //--Position.
            glTranslatef(cImageX, cImageY, 0.0f);
            glScalef(cImageScale, cImageScale, 1.0f);

            //--Render.
            rActiveEntry->rImage->Draw();

            //--Clean.
            glScalef(cImageScaleInv, cImageScaleInv, 1.0f);
            glTranslatef(-cImageX, -cImageY, 0.0f);
        }
        //--No image, moves the description up.
        else
        {
            cDescriptionY = 216.0f;
        }

        //--Description.
        float cDescriptionH = MonoImages.rFontMainline->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        mMonoParagraphColor.SetAsMixerAlpha(pVisAlpha);
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            MonoImages.rFontMainline->DrawTextArgs(cDescriptionX, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
    }
}
void MonoUIJournal::RenderParagonsMode(float pVisAlpha)
{

}
void MonoUIJournal::RenderProfilesMode(float pVisAlpha)
{
    ///--[Profile Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mProfileEntries, mProfileOffset, mProfileCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the image and information.
    AdvMenuJournalProfileEntry *rActiveEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementBySlot(mProfileCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Portrait]
        //--Render the portrait behind a stencil.
        if(rActiveEntry->rDisplayImg)
        {
            //--Position.
            float cScale = 0.50f;
            float cScaleInv = 1.0f / cScale;
            float cXPos = 515.0f + rActiveEntry->mPortraitOffsetX;
            float cYPos = 113.0f + rActiveEntry->mPortraitOffsetY;

            //--Translate, scale.
            glTranslatef(cXPos, cYPos, 0.0f);
            glScalef(cScale, cScale, 1.0f);

            //--Render.
            rActiveEntry->rDisplayImg->LoadDataFromSLF();
            rActiveEntry->rDisplayImg->Draw();

            //--Clean.
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-cXPos, -cYPos, 0.0f);
        }

        ///--[Static Parts]
        //--Render all static parts over the portrait.
        MonoImages.rOverlayProfileName->Draw();

        ///--[Name, Description]
        //--Name.
        float cNameX = 575.0f;
        float cNameY = 480.0f;
        mMonoSubtitleColor.SetAsMixerAlpha(pVisAlpha);
        MonoImages.rFontHeading->DrawTextArgs(cNameX, cNameY, 0, 1.0f, rActiveEntry->mDisplayName);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Description.
        float cDescriptionX1 = 866.0f;
        float cDescriptionX2 = 525.0f;
        float cDescriptionY = 155.0f;
        float cDescriptionH = MonoImages.rFontDescription->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        mMonoParagraphColor.SetAsMixerAlpha(pVisAlpha);
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            if(i < 18)
                MonoImages.rFontDescription->DrawTextArgs(cDescriptionX1, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            else
                MonoImages.rFontDescription->DrawTextArgs(cDescriptionX2, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
    }
}
void MonoUIJournal::RenderQuestsMode(float pVisAlpha)
{
    ///--[Profile Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mQuestEntries, mQuestOffset, mQuestCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the information about the highlighted quest.
    AdvMenuJournalQuestEntry *rActiveEntry = (AdvMenuJournalQuestEntry *)mQuestEntries->GetElementBySlot(mQuestCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Title]
        //--Backing.
        MonoImages.rOverlayQuestShade->Draw();

        //--Title goes in the top middle of the description pane.
        float cTitleX = 895.0f;
        float cTitleY = 173.0f;
        mMonoTitleColor.SetAsMixerAlpha(pVisAlpha);
        MonoImages.rFontHeading->DrawTextArgs(cTitleX, cTitleY, SUGARFONT_AUTOCENTER_X, 1.0f, rActiveEntry->mDisplayName);

        //--Objective. Displayed in green.
        float cObjectiveX = 518.0f;
        float cObjectiveY1 = 220.0f;
        float cObjectiveY2 = cObjectiveY1 + MonoImages.rFontMainline->GetTextHeight();
        StarlightColor::SetMixer(0.0f, 0.7f, 0.0f, pVisAlpha);
        MonoImages.rFontMainline->DrawTextArgs(cObjectiveX,       cObjectiveY1, 0, 1.0f, xrStringData->str.mTextObjective);
        MonoImages.rFontMainline->DrawTextArgs(cObjectiveX+10.0f, cObjectiveY2, 0, 1.0f, rActiveEntry->mObjective);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Description.
        float cDescriptionX = 518.0f;
        float cDescriptionY = 280.0f;
        float cDescriptionH = MonoImages.rFontMainline->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        mMonoParagraphColor.SetAsMixerAlpha(pVisAlpha);
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            MonoImages.rFontMainline->DrawTextArgs(cDescriptionX, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
    }
}
