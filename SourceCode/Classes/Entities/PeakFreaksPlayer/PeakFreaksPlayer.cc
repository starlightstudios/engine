//--Base
#include "PeakFreaksPlayer.h"

//--Classes
#include "Pathrunner.h"
#include "TileLayer.h"
#include "PeakFreaksLevel.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"

///========================================== System ==============================================
PeakFreaksPlayer::PeakFreaksPlayer()
{
    ///--[ ===== Variable Initialization ====== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_PEAKFREAKSPLAYER;

    ///--[ ======= RootEntity ======= ]
    //--System
    //--Nameable
    mLocalName = InitializeString("Player");

    //--Renderable
    //--Data Storage
    //--Public Variables

    ///--[ ==== PeakFreaksPlayer ==== ]
    ///--[System]
    mIdleTimer = rand() % 100;
    mRespawnTimer = 0;
    mShockTicks = 0;
    mHappyTicks = 0;

    ///--[Position]
    mTileX = 0;
    mTileY = 0;
    mLayer = 0;
    mPartialX = 0.0f;
    mPartialY = 0.0f;

    ///--[Instructions]
    mMovesRemaining = 0;
    mHasPendingChange = false;
    mSpeedX = 0.0f;
    mSpeedY = 0.0f;
    mNextSpeedX = 0.0f;
    mNextSpeedY = 0.0f;

    //--Jumping Queue
    mIsJumpHelicopter = false;
    mHelicopterTimer = 0;
    mBypassJumpCharge = false;
    mOverrideJumpSpeed = -1;
    mJumpLaunchTicks = 0;
    mJumpAirTimerCur = 0;
    mJumpAirTimerMax = 1;
    mActiveJump = NULL;
    mJumpingQueue = new SugarLinkedList(true);

    //--Speed Bar
    mSpeedBarLevel = 0;
    mSpeedBarTicks = PLAYER_SPEED_BAR_TICKS_PER_LOSS;

    //--Canyon Bar.
    mCanyonTimer = 0;

    //--Guano'd.
    mGuanoTimer = 0;

    ///--[Animation]
    mMoveTimer = 0.0f;
    mMoveTPF = 5.0f;

    ///--[Damage Sequence]
    mIsGettingDamaged = false;
    mFellThroughHole = false;
    mDamageTimer = 0;
    mDamageOffX = 0.0f;
    mDamageOffY = 0.0f;
    mDamageSpeedX = 0.0f;
    mDamageSpeedY = 0.0f;

    ///--[Victory Sequence]
    mIsWinning = false;
    mWinTimer = 0;

    ///--[Slip/Climb]
    mForceSlip = false;
    mForceClimb = false;

    ///--[AI Control]
    mIsAIControlled = false;
    mClearQueueWhenLanding = false;
    mStallTicks = 10;
    mPathTicksSoFar = 0;
    mAIInstructions = NULL;
    mPathRunner = NULL;
    mAIAlwaysStopTicks = 0;
    mAIAlwaysStopTicksLeft = 0;
    mAIMistakeMovesLeft = 1000;
    mAIMistakeLo = 1000;
    mAIMistakeHi = 1000;
    mAIStopTicks = 0;
    mAIStopMovesLeft = 1000;
    mAIStopLo = 1000;
    mAIStopHi = 1000;
    mAIDoubleMoveSpeedLeft = -1;
    mAIDoubleMoveSpeedChance = -1;

    ///--[Images]
    memset(&Images, 0, sizeof(Images));

    ///--[ =========== Construction =========== ]
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Portraits
    Images.Data.rPorNeutral = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Kidd/Neutral");
    Images.Data.rPorHappy   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Kidd/Happy");
    Images.Data.rPorSad     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Kidd/Sad");
    Images.Data.rPorShock   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Kidd/Shock");
    Images.Data.rTrackHead  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TrPor|Player");

    //--Images.
    Images.Data.rIdle[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PIdle0");
    Images.Data.rIdle[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PIdle1");
    Images.Data.rIdle[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PIdle2");
    Images.Data.rIdle[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PIdle3");
    Images.Data.rClimbRt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PClimbRt0");
    Images.Data.rClimbLf = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PClimbLf0");
    Images.Data.rSlip    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PSlip0");
    Images.Data.rVictory = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PVictory0");
    Images.Data.rShadow  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PShadow0");

    //--Heli-Lift
    Images.Data.rHeliLift[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli0");
    Images.Data.rHeliLift[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli1");

    //--Directional Debug
    Images.Data.rArrowU = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowUp");
    Images.Data.rArrowR = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowRt");
    Images.Data.rArrowD = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowDn");
    Images.Data.rArrowL = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowLf");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), false);
}
PeakFreaksPlayer::~PeakFreaksPlayer()
{
    free(mActiveJump);
    delete mJumpingQueue;
}
void PeakFreaksPlayer::SetScooterGraphics()
{
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Portraits
    Images.Data.rPorNeutral = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scooter/Neutral");
    Images.Data.rPorHappy   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scooter/Happy");
    Images.Data.rPorSad     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scooter/Sad");
    Images.Data.rPorShock   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scooter/Shock");
    Images.Data.rTrackHead  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TrPor|Scooter");

    //--Images.
    Images.Data.rIdle[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SIdle0");
    Images.Data.rIdle[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SIdle1");
    Images.Data.rIdle[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SIdle2");
    Images.Data.rIdle[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SIdle3");
    Images.Data.rClimbRt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SClimbRt0");
    Images.Data.rClimbLf = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SClimbLf0");
    Images.Data.rSlip    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SSlip0");
    Images.Data.rVictory = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Scooter/SVictory0");
    Images.Data.rShadow  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PShadow0");

    //--Heli-Lift
    Images.Data.rHeliLift[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli0");
    Images.Data.rHeliLift[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli1");

    //--Directional Debug
    Images.Data.rArrowU = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowUp");
    Images.Data.rArrowR = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowRt");
    Images.Data.rArrowD = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowDn");
    Images.Data.rArrowL = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowLf");
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
}
void PeakFreaksPlayer::SetMimiGraphics()
{
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Portraits
    Images.Data.rPorNeutral = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Mimi/Neutral");
    Images.Data.rPorHappy   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Mimi/Happy");
    Images.Data.rPorSad     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Mimi/Sad");
    Images.Data.rPorShock   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Mimi/Shock");
    Images.Data.rTrackHead  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TrPor|Mimi");

    //--Images.
    Images.Data.rIdle[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MIdle0");
    Images.Data.rIdle[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MIdle1");
    Images.Data.rIdle[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MIdle2");
    Images.Data.rIdle[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MIdle3");
    Images.Data.rClimbRt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MClimbRt0");
    Images.Data.rClimbLf = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MClimbLf0");
    Images.Data.rSlip    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MSlip0");
    Images.Data.rVictory = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Mimi/MVictory0");
    Images.Data.rShadow  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PShadow0");

    //--Heli-Lift
    Images.Data.rHeliLift[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli0");
    Images.Data.rHeliLift[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli1");

    //--Directional Debug
    Images.Data.rArrowU = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowUp");
    Images.Data.rArrowR = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowRt");
    Images.Data.rArrowD = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowDn");
    Images.Data.rArrowL = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowLf");
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
}
void PeakFreaksPlayer::SetAllenGraphics()
{
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Portraits
    Images.Data.rPorNeutral = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Allen/Neutral");
    Images.Data.rPorHappy   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Allen/Happy");
    Images.Data.rPorSad     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Allen/Sad");
    Images.Data.rPorShock   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Allen/Shock");
    Images.Data.rTrackHead  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/TrPor|Allen");

    //--Images.
    Images.Data.rIdle[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/AIdle0");
    Images.Data.rIdle[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/AIdle1");
    Images.Data.rIdle[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/AIdle2");
    Images.Data.rIdle[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/AIdle3");
    Images.Data.rClimbRt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/AClimbRt0");
    Images.Data.rClimbLf = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/AClimbLf0");
    Images.Data.rSlip    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/ASlip0");
    Images.Data.rVictory = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Allen/AVictory0");
    Images.Data.rShadow  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Player/PShadow0");

    //--Heli-Lift
    Images.Data.rHeliLift[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli0");
    Images.Data.rHeliLift[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Entities/Helicopter/Heli1");

    //--Directional Debug
    Images.Data.rArrowU = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowUp");
    Images.Data.rArrowR = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowRt");
    Images.Data.rArrowD = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowDn");
    Images.Data.rArrowL = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/World/ArrowLf");
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
}

///===================================== Property Queries =========================================
bool PeakFreaksPlayer::IsFlashingWhite()
{
    if(mCanyonTimer == 0) return false;
    return (mCanyonTimer % 6 < 3);
}
bool PeakFreaksPlayer::IsAirborne()
{
    if(!mActiveJump) return false;
    if(mJumpLaunchTicks < PLAYER_JUMP_WAIT_TICKS) return false;
    return true;
}
bool PeakFreaksPlayer::IsIgnoringHazards()
{
    //--Canyon Bar: Ignore it.
    if(mCanyonTimer > 0) return true;

    //--Airborne: Ignore it.
    if(IsAirborne()) return true;

    //--Otherwise, no.
    return false;
}
float PeakFreaksPlayer::GetWorldX()
{
    return (float)mTileX + mPartialX;
}
float PeakFreaksPlayer::GetWorldY()
{
    return (float)mTileY + mPartialY;
}
float PeakFreaksPlayer::GetDamageOffsetX()
{
    if(!mIsGettingDamaged) return 0.0f;
    return mDamageOffX;
}
float PeakFreaksPlayer::GetDamageOffsetY()
{
    if(!mIsGettingDamaged) return 0.0f;
    return mDamageOffY;
}
float PeakFreaksPlayer::GetDamageRotation()
{
    if(!mIsGettingDamaged) return 0.0f;
    return ((mDamageTimer / 30.0f) * 360.0f);
}
float PeakFreaksPlayer::GetOffsetX()
{
    //--No active move, offset is always zero.
    if(!mActiveJump) return 0.0f;

    //--X Coordinate is zero.
    if(mActiveJump->mJumpX == 0) return 0.0f;

    //--Compute.
    float cPct = EasingFunction::QuadraticOut(mJumpAirTimerCur, mJumpAirTimerMax);
    return (mActiveJump->mJumpX * cPct);
}
float PeakFreaksPlayer::GetOffsetY()
{
    //--No active move, offset is always zero.
    if(!mActiveJump) return 0.0f;

    //--Y Coordinate is zero.
    if(mActiveJump->mJumpY == 0) return 0.0f;

    //--Compute.
    float cPct = EasingFunction::QuadraticOut(mJumpAirTimerCur, mJumpAirTimerMax);
    return (mActiveJump->mJumpY * cPct);
}
float PeakFreaksPlayer::GetOffsetZ()
{
    //--No active move, offset is always zero.
    if(!mActiveJump) return 0.0f;

    //--Compute.
    float cPct = sinf(mJumpAirTimerCur / (float)mJumpAirTimerMax * 3.1415926f);
    return (PLAYER_JUMP_HEIGHT * cPct);
}
int PeakFreaksPlayer::GetWorldZ()
{
    return mLayer;
}
float PeakFreaksPlayer::GetSpeedX()
{
    return mSpeedX;
}
float PeakFreaksPlayer::GetSpeedY()
{
    return mSpeedY;
}
int PeakFreaksPlayer::GetSpeedBarLevel()
{
    //--If the player is canyon bar'd, return max.
    if(mCanyonTimer > 0) return 4;

    //--If the player is guano'd, return 0.
    if(mGuanoTimer > 0) return 0;

    //--Return the real value.
    return mSpeedBarLevel;
}
int PeakFreaksPlayer::GetActualSpeedBarLevel()
{
    return mSpeedBarLevel;
}
int PeakFreaksPlayer::GetDirectionFromSpeeds(float pXSpeed, float pYSpeed)
{
    //--Given two speeds, returns a directional code based on those speeds. Ignores diagonals.
    if(pXSpeed < 0.0f) return DIR_LEFT;
    if(pXSpeed > 0.0f) return DIR_RIGHT;
    if(pYSpeed < 0.0f) return DIR_UP;
    if(pYSpeed > 0.0f) return DIR_DOWN;

    //--Not moving.
    return DIR_NONE;
}
int PeakFreaksPlayer::GetCanyonBarTimer()
{
    return mCanyonTimer;
}
bool PeakFreaksPlayer::IsGuanod()
{
    return (mGuanoTimer > 0);
}
int PeakFreaksPlayer::GetRespawnTimer()
{
    return mRespawnTimer;
}

///======================================= Manipulators ===========================================
void PeakFreaksPlayer::SetPosition(float pX, float pY)
{
    mTileX = (int)pX;
    mTileY = (int)pY;
    mPartialX = pX - mTileX;
    mPartialY = pY - mTileY;
}
void PeakFreaksPlayer::SetPositionNoPartial(float pX, float pY)
{
    mTileX = (int)pX;
    mTileY = (int)pY;
}
void PeakFreaksPlayer::SetLayer(int pZ)
{
    mLayer = pZ;
}
void PeakFreaksPlayer::SetSpeeds(float pX, float pY)
{
    mSpeedX = pX;
    mSpeedY = pY;
}
void PeakFreaksPlayer::QueueNextSpeeds(float pX, float pY)
{
    mHasPendingChange = true;
    mNextSpeedX = pX;
    mNextSpeedY = pY;
}
void PeakFreaksPlayer::QueueJump(int pX, int pY)
{
    //--Add a jump to the queue. If there's no active jump, the update will activate it next time
    //  the update cycle happens.
    PeakFreaksPlayerJump *nJump = (PeakFreaksPlayerJump *)starmemoryalloc(sizeof(PeakFreaksPlayerJump));
    nJump->mHasCheckedClips = false;
    nJump->mJumpX = pX;
    nJump->mJumpY = pY;
    mJumpingQueue->AddElementAsTail("X", nJump, &FreeThis);
}
void PeakFreaksPlayer::QueueBump(int pX, int pY)
{
    //--A bump is the same as a jump, but it happens immediately and clears the jump queue. Bumps also
    //  bypass collision checks, and can therefore knock the player onto a cliff which kills them.
    if(mActiveJump) free(mActiveJump);
    mActiveJump = NULL;
    ClearJumps();
    QueueJump(pX, pY);
    mBypassJumpCharge = true;
    mOverrideJumpSpeed = 4;

    //--Get the last jump and mark it as having already checked clips.
    PeakFreaksPlayerJump *rLastJump = (PeakFreaksPlayerJump *)mJumpingQueue->GetTail();
    if(rLastJump) rLastJump->mHasCheckedClips = true;
}
void PeakFreaksPlayer::ResetToLastCheckpoint()
{
    //--Can't do anything if there's no checkpoint.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--Position.
    mTileX = rActiveLevel->GetCheckpointX(this) / 16.0f;
    mTileY = rActiveLevel->GetCheckpointY(this) / 16.0f;
    mLayer = rActiveLevel->GetCheckpointZ(this);
    mPartialX = 0.0f;
    mPartialY = 0.0f;

    //--Stop all movements.
    mHasPendingChange = false;
    mSpeedX = 0.0f;
    mSpeedY = 0.0f;
    mNextSpeedX = 0.0f;
    mNextSpeedY = 0.0f;

    //--Order the level to select the right depth.
    rActiveLevel->SetPlayerLayer(mLayer);
    free(mActiveJump);
    mActiveJump = NULL;
    ClearJumps();

    //--AI-controlled players re-run their pathing routines now.
    if(mIsAIControlled)
    {
        delete mAIInstructions;
        mAIInstructions = NULL;
    }
}
void PeakFreaksPlayer::SetMovesRemaining(int pValue)
{
    mMovesRemaining = pValue;
}
void PeakFreaksPlayer::SetSpeedBar(int pLevel)
{
    mSpeedBarLevel = pLevel;
    if(mSpeedBarLevel < 0) mSpeedBarLevel = 0;
    if(mSpeedBarLevel > 5) mSpeedBarLevel = 5;
    mSpeedBarTicks = PLAYER_SPEED_BAR_TICKS_PER_LOSS;
}
void PeakFreaksPlayer::ActivateCanyonBar()
{
    mCanyonTimer = PLAYER_CANYON_TICKS;
}
void PeakFreaksPlayer::ActivateHelicopter()
{
    mIsJumpHelicopter = true;
    AudioManager::Fetch()->PlaySound("PF Powerup Helicopter");
}
void PeakFreaksPlayer::Injure()
{
    //--Canyon timer blocks this.
    if(mCanyonTimer > 0) return;
    if(mIsGettingDamaged) return;

    //--Start the timer.
    mIsGettingDamaged = true;
    mDamageTimer = 0;
    mDamageOffX = 0.0f;
    mDamageOffY = 0.0f;
    mDamageSpeedX = ((rand() % 30) / 10.0f) + 1.0f;
    if(rand() % 100 < 50) mDamageSpeedX = mDamageSpeedX * -1.0f;
    mDamageSpeedY = -5.0f;

    //--Clear queue.
    ClearJumps();

    //--SFX.
    AudioManager::Fetch()->PlaySound("PF Entity Death");
}
void PeakFreaksPlayer::Guano()
{
    mGuanoTimer = PLAYER_GUANO_TICKS;
}
void PeakFreaksPlayer::Shock()
{
    mShockTicks = PLAYER_SHOCK_TICKS;
}
void PeakFreaksPlayer::MakeHappy()
{
    mHappyTicks = PLAYER_HAPPY_TICKS;
}
void PeakFreaksPlayer::ClearJumps()
{
    mJumpingQueue->ClearList();
}

///======================================= Core Methods ===========================================
void PeakFreaksPlayer::CheckJumpCollisions()
{
    ///--[Documentation]
    //--When a jump begins, we need to scan across its movement to check if it hit any collisions.
    //  If it did, the jump falls short of the collision.
    //--Nominally, a jump is only orthogonal. However, the algorithm still nominally handles diagonal
    //  jumps, but is fairly rigid about stopping on a collision.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--No active jump, stop.
    if(!mActiveJump) return;

    //--Flag.
    mActiveJump->mHasCheckedClips = true;

    //--Scan handling.
    int tPosX = mTileX;
    int tPosY = mTileY;
    int tJumpX = mActiveJump->mJumpX;
    int tJumpY = mActiveJump->mJumpY;

    ///--[Iteration]
    //--Loop until the jump is complete.
    int tLegalMovesX = 0;
    int tLegalMovesY = 0;
    while(true)
    {
        //--Flag so we check if we performed a move.
        bool tMoved = false;

        //--Left:
        if(tJumpX < 0)
        {
            //--Decrement.
            tMoved = true;
            tJumpX ++;

            //--Check position. If clipped, stop.
            if(rActiveLevel->GetClipAtTile(tPosX-1, tPosY))
            {
                break;
            }
            //--Not clipped. Continue.
            else
            {
                tPosX --;
                tLegalMovesX --;
            }
        }
        //--Right:
        else if(tJumpX > 0)
        {
            //--Increment.
            tMoved = true;
            tJumpX --;

            //--Check position. If clipped, stop.
            if(rActiveLevel->GetClipAtTile(tPosX+1, tPosY))
            {
                break;
            }
            //--Not clipped. Continue.
            else
            {
                tPosX ++;
                tLegalMovesX ++;
            }
        }

        //--Up:
        if(tJumpY < 0)
        {
            //--Decrement.
            tMoved = true;
            tJumpY ++;

            //--Check position. If clipped, stop.
            if(rActiveLevel->GetClipAtTile(tPosX, tPosY-1))
            {
                break;
            }
            //--Not clipped. Continue.
            else
            {
                tPosY --;
                tLegalMovesY --;
            }
        }
        //--Down:
        else if(tJumpY > 0)
        {
            //--Increment
            tMoved = true;
            tJumpY --;

            //--Check position. If clipped, stop.
            if(rActiveLevel->GetClipAtTile(tPosX, tPosY+1))
            {
                break;
            }
            //--Not clipped. Continue.
            else
            {
                tPosY ++;
                tLegalMovesY ++;
            }
        }

        //--No moves left. Stop.
        if(!tMoved) break;
    }

    ///--[Finish Up]
    //--The tLegalMoves variables now indicate how far the jump could go.
    mActiveJump->mJumpX = tLegalMovesX;
    mActiveJump->mJumpY = tLegalMovesY;
}
void PeakFreaksPlayer::SetAIDifficulty(int pLevel)
{
    ///--[Documentation]
    //--Sets the "Difficulty" flag for the AI. This is how often it will screw up or speed up.
    if(pLevel <= 0)
    {
        mAIAlwaysStopTicks = 30;
        mAIMistakeMovesLeft = 13;
        mAIMistakeLo = 10;
        mAIMistakeHi = 15;
        mAIStopMovesLeft = 2;
        mAIStopLo = 1;
        mAIStopHi = 3;
        mAIDoubleMoveSpeedLeft = 1;
        mAIDoubleMoveSpeedChance = -1;
    }
    else if(pLevel == 1)
    {
        mAIAlwaysStopTicks = 20;
        mAIMistakeMovesLeft = 23;
        mAIMistakeLo = 20;
        mAIMistakeHi = 25;
        mAIStopMovesLeft = 5;
        mAIStopLo = 4;
        mAIStopHi = 6;
        mAIDoubleMoveSpeedLeft = 1;
        mAIDoubleMoveSpeedChance = -1;
    }
    else if(pLevel >= 2)
    {
        mAIAlwaysStopTicks = 5;
        mAIMistakeMovesLeft = 33;
        mAIMistakeLo = 30;
        mAIMistakeHi = 35;
        mAIStopMovesLeft = 1000;
        mAIStopLo = 1000;
        mAIStopHi = 1000;
        mAIDoubleMoveSpeedLeft = 0;
        mAIDoubleMoveSpeedChance = 3;
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksPlayer::Update()
{
    ///--[ ========== Setup ========= ]
    //--Fast-access pointers.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    ///--[ ========= Timers ========= ]
    //--Canyon timer.
    if(mCanyonTimer > 0) mCanyonTimer --;

    //--Guano timer.
    if(mGuanoTimer > 0) mGuanoTimer --;

    //--Emotion timers.
    if(mShockTicks > 0) mShockTicks --;
    if(mHappyTicks > 0) mHappyTicks --;

    //--Respawn timer.
    if(mRespawnTimer > 0) mRespawnTimer --;

    //--Always run the helicopter timer.
    mHelicopterTimer ++;

    //--Idle.
    mIdleTimer ++;

    //--If the speed bar is above 0, decrement the speed bar timer.
    if(mSpeedBarLevel > 0)
    {
        //--Decrement.
        mSpeedBarTicks --;

        //--Zeroes out, decrement the speed bar.
        if(mSpeedBarTicks < 1)
        {
            mSpeedBarLevel --;
            mSpeedBarTicks = PLAYER_SPEED_BAR_TICKS_PER_LOSS;
        }
    }
    //--Otherwise, cap the timer.
    else
    {
        mSpeedBarTicks = PLAYER_SPEED_BAR_TICKS_PER_LOSS;
    }

    ///--[ ======== Lockouts ======== ]
    ///--[Lockout Handlers]
    //--Winning!
    if(mIsWinning)
    {
        //--Tick.
        mWinTimer ++;

        //--Ending.
        if(mWinTimer > 5)
        {
            rActiveLevel->StartVictory(this);
        }
        return;
    }

    //--Getting damaged.
    if(mIsGettingDamaged)
    {
        //--Tick.
        mDamageTimer ++;

        //--Movement.
        mDamageOffX = mDamageOffX + mDamageSpeedX;
        mDamageOffY = mDamageOffY + mDamageSpeedY;

        //--Gravity.
        mDamageSpeedY = mDamageSpeedY + PLAYER_DAMAGE_GRAVITY;

        //--Ending.
        if(mDamageTimer > 120)
        {
            //--Unset.
            mIsGettingDamaged = false;
            mDamageTimer = 0;
            mDamageOffY = 0;
            ResetToLastCheckpoint();
            mRespawnTimer = PLAYER_RESPAWN_TICKS;
        }

        return;
    }

    ///--[Slip/Climb Handling]
    //--If slipping, the player moves downwards regardless of anything else until they are no longer in a slip zone.
    if(mForceSlip)
    {
        HandleSlip();
        return;
    }

    //--Climbing. A similar deal except upwards.
    if(mForceClimb)
    {
        HandleClimb();
        return;
    }

    //--Block further updates if respawning.
    if(mRespawnTimer > 0) return;

    ///--[ ====== Hit Detection ===== ]
    //--Get positions.
    float tLft = ((mTileX + mPartialX)        * TileLayer::cxSizePerTile) + 2.0f;
    float tTop = ((mTileY + mPartialY)        * TileLayer::cxSizePerTile) + 2.0f;
    float tRgt = ((mTileX + mPartialX + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;
    float tBot = ((mTileY + mPartialY + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;

    //--Setup:
    bool tIsAirborne = IsAirborne();
    if(!tIsAirborne)
    {
        //--Win:
        if(rActiveLevel->IsTouchingVictoryZone(tLft, tTop, tRgt, tBot, mLayer))
        {
            mIsWinning = true;
            mWinTimer = 0;
            return;
        }

        //--Injury.
        if(rActiveLevel->IsTouchingDamageZone(tLft, tTop, tRgt, tBot, mLayer))
        {
            Injure();
            return;
        }

        //--Landed on a collision. Only possible when bumped by a hazard.
        if(rActiveLevel->GetClipAtTile(mTileX, mTileY))
        {
            Injure();
            return;
        }

        //--If the player went out of bounds:
        if(rActiveLevel->IsOutOfBounds(mTileX, mTileY))
        {
            Injure();
            return;
        }

        //--Picking up a coin/powerup.
        int tCoinSubtype = rActiveLevel->IsTouchingCoinZone(tLft, tTop, tRgt, tBot, mLayer, true);
        if(tCoinSubtype == -1)
        {

        }
        else if(tCoinSubtype == PFLEVEL_COIN_SUBTYPE_COIN)
        {
            SetSpeedBar(mSpeedBarLevel + 2);
            rActiveLevel->HandlePlayerPowerup(this, tCoinSubtype);
            AudioManager::Fetch()->PlaySound("Coin Pickup");
        }
        //--Powerups:
        else
        {
            rActiveLevel->HandlePlayerPowerup(this, tCoinSubtype);
            AudioManager::Fetch()->PlaySound("Coin Pickup");
        }

        //--Picking up a treasure chest.
        if(rActiveLevel->IsTouchingTreasureZone(tLft, tTop, tRgt, tBot, mLayer, true))
        {
            AudioManager::Fetch()->PlaySound("Coin Pickup");
        }

        //--Switching layers.
        int tLayerSwitch = rActiveLevel->IsTouchingLayerDropZone(tLft, tTop, tRgt, tBot, mLayer);
        if(tLayerSwitch > -1)
        {
            rActiveLevel->SetPlayerLayer(tLayerSwitch);
        }

        //--Order the level to recheck the progress variables.
        rActiveLevel->HandlePlayerProgress(this, GetWorldX(), GetWorldY());
    }

    //--Activate checkpoints. This can occur even when airborne. This requires us to calculate the position
    //  using the jump offset.
    float cAirborneX = GetWorldX() + GetOffsetX();
    float cAirborneY = GetWorldY() + GetOffsetY();
    tLft = ( cAirborneX         * TileLayer::cxSizePerTile) + 2.0f;
    tTop = ( cAirborneY         * TileLayer::cxSizePerTile) + 2.0f;
    tRgt = ((cAirborneX + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;
    tBot = ((cAirborneY + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;

    const char *rCheckpointName = rActiveLevel->IsTouchingCheckpoint(tLft, tTop, tRgt, tBot, mLayer);
    if(rCheckpointName)
    {
        rActiveLevel->ActivateCheckpoint(this, rCheckpointName);
    }

    ///--[==== Currently Jumping ==== ]
    ///--[New Movement]
    //--If the player was previously stationary, and enqueued a new movement, or ended a previous movement
    //  during the last tick, handle that:
    if(!mActiveJump && mJumpingQueue->GetListSize() > 0)
    {
        mJumpingQueue->SetRandomPointerToHead();
        mActiveJump = (PeakFreaksPlayerJump *)mJumpingQueue->LiberateRandomPointerEntry();
    }

    ///--[Existing Movement]
    //--If the player is currently jumping, or if the player is running the timer to perform a jump:
    if(mActiveJump)
    {
        //--Collision check. Adjust the current jump if the player tries to leap over a clipped tile.
        if(!mActiveJump->mHasCheckedClips)
        {
            CheckJumpCollisions();
        }

        //--Bypass the jump charge.
        if(mBypassJumpCharge)
        {
            mBypassJumpCharge = false;
            mJumpLaunchTicks = PLAYER_JUMP_WAIT_TICKS - 1;
        }

        //--Run this timer to prep for jump.
        if(mJumpLaunchTicks < PLAYER_JUMP_WAIT_TICKS)
        {
            //--Timer.
            mJumpLaunchTicks ++;

            //--Hit the cap. Compute the timer max based on the speed.
            if(mJumpLaunchTicks >= PLAYER_JUMP_WAIT_TICKS)
            {
                //--Get the speed bar.
                int tSpeedBar = GetSpeedBarLevel(); //This function applies Canyon Bar/Guano for us.

                //--Check for an override. Bumps cause this.
                if(mOverrideJumpSpeed != -1)
                {
                    tSpeedBar = mOverrideJumpSpeed;
                    mOverrideJumpSpeed = -1;
                }

                //--Set the timer.
                mJumpAirTimerCur = 0;
                if(     tSpeedBar == 0) mJumpAirTimerMax = PLAYER_JUMP_DURATION0;
                else if(tSpeedBar == 1) mJumpAirTimerMax = PLAYER_JUMP_DURATION1;
                else if(tSpeedBar == 2) mJumpAirTimerMax = PLAYER_JUMP_DURATION2;
                else if(tSpeedBar == 3) mJumpAirTimerMax = PLAYER_JUMP_DURATION3;
                else if(tSpeedBar == 4) mJumpAirTimerMax = PLAYER_JUMP_DURATION4;

                //--SFX.
                if(rActiveLevel->GetPlayer() == this)
                    AudioManager::Fetch()->PlaySound("PF Entity PlayerJump");
                else
                    AudioManager::Fetch()->PlaySound("PF Entity RivalJump");
            }
        }
        //--Get airborne.
        else
        {
            //--Timer.
            mJumpAirTimerCur ++;

            //--Check if we landed. Remove the active jump. If needed, put the next queued jump on.
            if(mJumpAirTimerCur >= mJumpAirTimerMax)
            {
                //--Clear helicopter.
                mIsJumpHelicopter = false;

                //--Reposition the player to the target position.
                mTileX = mTileX + mActiveJump->mJumpX;
                mTileY = mTileY + mActiveJump->mJumpY;

                //--Timer.
                mJumpLaunchTicks = 0;
                mJumpAirTimerCur = 0;

                //--Check if we landed on a trampoline case (such as a hazard). If that happens, clear
                //  the movement queue and restart the move with the current move again. Whee!
                if(rActiveLevel->IsTrampoline(mTileX, mTileY))
                {
                    mJumpingQueue->ClearList();
                    mJumpLaunchTicks = PLAYER_JUMP_WAIT_TICKS - 1;
                    AudioManager::Fetch()->PlaySound("Blink");
                    rActiveLevel->SpawnDustParticles(mTileX, mTileY);
                }
                //--Otherwise, enqueue the next jump.
                else
                {
                    //--Deallocate/clear.
                    free(mActiveJump);
                    mActiveJump = NULL;

                    //--If a new jump exists, queue it right away.
                    if(mJumpingQueue->GetListSize() > 0)
                    {
                        mJumpingQueue->SetRandomPointerToHead();
                        mActiveJump = (PeakFreaksPlayerJump *)mJumpingQueue->LiberateRandomPointerEntry();
                    }
                }
            }
        }
    }
}
void PeakFreaksPlayer::BeginMove(float pXVec, float pYVec)
{
    ///--[Documentation]
    //--The player attempts to begin moving to the next tile given a movement vector.
    mSpeedX = 0.0f;
    mSpeedY = 0.0f;

    //--Level doesn't exist. Fail.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--Movement vector is zeroes.
    if(pXVec == 0.0f && pYVec == 0.0f)
    {
        return;
    }

    ///--[Clip Check]
    //--Query the level to see if it's possible to move into the tiles in question.
    int tQueryXA = 0;
    int tQueryYA = 0;

    //--Resolve query positions based on the speed vectors. Moving Right:
    if(pXVec > 0.0f)
    {
        tQueryXA = mTileX + 1;
        tQueryYA = mTileY + 0;
    }
    //--Moving left:
    else if(pXVec < 0.0f)
    {
        tQueryXA = mTileX - 1;
        tQueryYA = mTileY + 0;
    }
    //--Moving up:
    else if(pYVec < 0.0f)
    {
        tQueryXA = mTileX + 0;
        tQueryYA = mTileY - 1;
    }
    //--Moving down:
    else if(pYVec > 0.0f)
    {
        tQueryXA = mTileX + 0;
        tQueryYA = mTileY + 1;
    }

    //--Queries are made in pixel coordinates:
    tQueryXA = tQueryXA * TileLayer::cxSizePerTile;
    tQueryYA = tQueryYA * TileLayer::cxSizePerTile;

    //--Ask the level for the tile clips. If either of the queries is clipped, zero the speeds.
    bool tClipA = rActiveLevel->GetClipAt(tQueryXA, tQueryYA, 0.0f, mLayer);
    if(!tClipA)
    {
        mSpeedX = pXVec;
        mSpeedY = pYVec;
        return;
    }
}
void PeakFreaksPlayer::HandleSlip()
{
    ///--[Documentation]
    //--Called when the player is in a slip zone. Forcibly moves the player downwards until they are no longer in a slip zone.

    ///--[Movement]
    //--Player is moving. Check if they passed over a tile boundary.
    mPartialX = mPartialX + 0.0f;
    mPartialY = mPartialY + PLAYER_SLIP_SPEED;

    float tLft = ((mTileX + mPartialX)        * TileLayer::cxSizePerTile) + 2.0f;
    float tTop = ((mTileY + mPartialY)        * TileLayer::cxSizePerTile) + 2.0f;
    float tRgt = ((mTileX + mPartialX + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;
    float tBot = ((mTileY + mPartialY + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;

    //--Crossed a boundary moving down:
    if(mPartialY >= 1.0f)
    {
        //--Reposition.
        mTileY ++;
        mPartialY = mPartialY - 1.0f;

        //--Check if still touching a slip zone.
        PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
        if(rActiveLevel && !rActiveLevel->IsTouchingSlipZone(tLft, tTop, tRgt, tBot, mLayer))
        {
            mForceSlip = false;
            mPartialX = 0.0f;
            mPartialY = 0.0f;
            mHasPendingChange = false;
            mSpeedX = 0.0f;
            mSpeedY = 0.0f;
            mNextSpeedX = 0.0f;
            mNextSpeedY = 0.0f;
        }
    }
}
void PeakFreaksPlayer::HandleClimb()
{
    ///--[Documentation]
    //--Called when the player is in a slip zone. Forcibly moves the player downwards until they are no longer in a slip zone.

    ///--[Movement]
    //--Player is moving. Check if they passed over a tile boundary.
    mPartialX = mPartialX + 0.0f;
    mPartialY = mPartialY + PLAYER_CLIMB_SPEED;

    float tLft = ((mTileX + mPartialX)        * TileLayer::cxSizePerTile) + 2.0f;
    float tTop = ((mTileY + mPartialY)        * TileLayer::cxSizePerTile) + 2.0f;
    float tRgt = ((mTileX + mPartialX + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;
    float tBot = ((mTileY + mPartialY + 1.0f) * TileLayer::cxSizePerTile) - 2.0f;

    //--Crossed a boundary moving down:
    if(mPartialY <= -1.0f)
    {
        //--Reposition.
        mTileY --;
        mPartialY = mPartialY + 1.0f;

        //--Check if still touching a slip zone.
        PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
        if(rActiveLevel && !rActiveLevel->IsTouchingClimbZone(tLft, tTop, tRgt, tBot, mLayer))
        {
            mForceClimb = false;
            mPartialX = 0.0f;
            mPartialY = 0.0f;
            mHasPendingChange = false;
            mSpeedX = 0.0f;
            mSpeedY = 0.0f;
            mNextSpeedX = 0.0f;
            mNextSpeedY = 0.0f;
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
SugarBitmap *PeakFreaksPlayer::ResolveFrame()
{
    //--Getting damaged, always return the same frame:
    if(mIsGettingDamaged)
    {
        return Images.Data.rSlip;
    }

    //--Victory!
    if(mIsWinning)
    {
        return Images.Data.rVictory;
    }

    //--OH SHHIIITTT!
    if(mForceSlip)
    {
        return Images.Data.rSlip;
    }

    //--Moving Left:
    if(mActiveJump && mActiveJump->mJumpX < 0)
    {
        return Images.Data.rClimbLf;
    }
    //--Moving Right:
    else if(mActiveJump && mActiveJump->mJumpX > 0)
    {
        return Images.Data.rClimbRt;
    }
    //--Moving Up:
    else if(mActiveJump && mActiveJump->mJumpY < 0)
    {
        return Images.Data.rIdle[0];
    }
    //--Moving Down:
    else if(mActiveJump && mActiveJump->mJumpY > 0)
    {
        return Images.Data.rIdle[0];
    }

    //--Stationary.
    int tIdleFrame = ((int)(mIdleTimer / PLAYER_IDLE_TPF) % 4);
    return Images.Data.rIdle[tIdleFrame];
}
SugarBitmap *PeakFreaksPlayer::ResolveArrow()
{
    //--Images failed to resolve.
    if(!Images.mIsReady) return NULL;

    //--Directions.
    if(mSpeedX < 0.0f) return Images.Data.rArrowL;
    if(mSpeedX > 0.0f) return Images.Data.rArrowR;
    if(mSpeedY < 0.0f) return Images.Data.rArrowU;
    if(mSpeedY > 0.0f) return Images.Data.rArrowD;

    //--Player is not moving, return NULL.
    return NULL;
}
SugarBitmap *PeakFreaksPlayer::GetPortrait()
{
    //--Player is winning, or under a powerup, so display Happy.
    if(mIsWinning || mCanyonTimer > 0 || mHappyTicks > 0) return Images.Data.rPorHappy;

    //--Dead.
    if(mIsGettingDamaged || mForceSlip) return Images.Data.rPorSad;

    //--SHOCKING.
    if(mShockTicks > 0) return Images.Data.rPorShock;

    //--Player is guano'd which also shocks.
    if(mGuanoTimer > 0) return Images.Data.rPorShock;

    //--No special cases.
    return Images.Data.rPorNeutral;
}
SugarBitmap *PeakFreaksPlayer::GetTrackHead()
{
    return Images.Data.rTrackHead;
}
SugarBitmap *PeakFreaksPlayer::GetShadow()
{
    if(!mActiveJump || mJumpLaunchTicks < PLAYER_JUMP_WAIT_TICKS) return NULL;
    return Images.Data.rShadow;
}
SugarBitmap *PeakFreaksPlayer::GetHelicopter()
{
    //--Note: Returns NULL if the player is not heli-boosting or respawning.
    if(mRespawnTimer > 0)
    {
        int tFrame = ((PLAYER_RESPAWN_TICKS - mRespawnTimer) / 6) % 2;
        return Images.Data.rHeliLift[tFrame];
    }


    if(!mIsJumpHelicopter) return NULL;
    int tFrame = (mHelicopterTimer / 6) % 2;
    return Images.Data.rHeliLift[tFrame];
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
