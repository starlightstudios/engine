//--Base
#include "PuzzleFight.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

///========================================== Drawing =============================================
void PuzzleFight::RenderWorld()
{
    ///--[Documentation]
    //--Renders the world component, which is the cursor, mostly. All other board effects are done
    //  via dislocations.
    //--This is called during world translation.
    if(!Images.mIsReady) return;

    //--Highlight indicates the player's current move.
    if(mGameState == MINIGAME_STATE_PLAYERSTURNRUN)
    {
        float cHighlightDepth = -0.999995f;
        glTranslatef(0.0f, 0.0f, cHighlightDepth);
        RenderExpandableHighlight(mCursorCurrent.mLft, mCursorCurrent.mTop, mCursorCurrent.mRgt, mCursorCurrent.mBot, 4.0f, Images.Data.rHighlight);
        glTranslatef(0.0f, 0.0f, -cHighlightDepth);
    }

    //--Render all board objects.
    float cBoardObjectDepth = -0.999996f;
    glTranslatef(0.0f, 0.0f, cBoardObjectDepth);
    for(int x = 0; x < mBoardW; x ++)
    {
        for(int y = 0; y < mBoardH; y ++)
        {
            //--Skip if this slot is empty.
            BoardObjectPack *rPack = &mBoardObjects[x][y];
            if(rPack->mObjectType < 0 || rPack->mObjectType >= BOARD_OBJECT_TOTAL) continue;

            //--Make sure there's an image to render.
            StarBitmap *rImgPtr = Images.Data.rBoardObjects[rPack->mObjectType];
            if(!rImgPtr) continue;

            //--Render the object.
            rImgPtr->Draw(rPack->mPosition.mXCur, rPack->mPosition.mYCur);

            //--If the object's "dupe" timer is running, render that, too.
            if(rPack->mDupePosition.mTimer < rPack->mDupePosition.mTimerMax)
            {
                rImgPtr->Draw(rPack->mDupePosition.mXCur, rPack->mDupePosition.mYCur);
            }
        }
    }
    glTranslatef(0.0f, 0.0f, -cBoardObjectDepth);

    ///--[Notifiers]
    //--Render these fairly high above most world objects.
    float cNotifierDepth = -0.100000f;
    glTranslatef(0.0f, 0.0f, cNotifierDepth);
    BoardNotifierPack *rNotifier = (BoardNotifierPack *)mNotifierList->PushIterator();
    while(rNotifier)
    {
        rNotifier->Render();
        rNotifier = (BoardNotifierPack *)mNotifierList->AutoIterate();
    }
    glTranslatef(0.0f, 0.0f, -cNotifierDepth);
}
void PuzzleFight::RenderUI()
{
    ///--[Documentation]
    //--Renders the UI associated with this object. Most special effects should be handled by the owning
    //  AdventureLevel using conventional sprite objects.
    if(!Images.mIsReady) return;

    ///--[Static Pieces]
    //--Compute the slide percentage.
    float tPercent = 1.0f - EasingFunction::QuadraticIn(mUISlideTimer, UI_SLIDE_TICKS);

    //--Objects that slide in from the top. The timer box does not appear if the timer is -1.
    float cSlideDistY = -86.0f;
    float cCurY = cSlideDistY * tPercent;

    //--Objects that slide in from the left.
    float cSlideDistL = -25.0f;
    float cCurL = cSlideDistL * tPercent;

    //--Objects that slide in from the right.
    float cSlideDistR = 25.0f;
    float cCurR = cSlideDistR * tPercent;
    Images.Data.rHealthFrameRgt->Draw(cCurR, 0);

    ///--[Moves Left]
    //--How many moves the player has left. Also displays how many they would have if they
    //  cancelled all moves made so far. Does not appear if the value is -1.
    if(mPlayerMovesLeft > -1)
    {
        //--Static piece.
        Images.Data.rMovesBox->Draw(0, cCurY);

        //--Get the length of the cancel stack. Add that and it's how many moves there are total.
        int tMoveStack = mPlayerMovesLeft + mBoardActionList->GetListSize();

        //--Render.
        Images.Data.rMovesFont->DrawTextArgs(12.0f, 19.0f, 0, 1.0f, "Moves: %i / %i", mPlayerMovesLeft, tMoveStack);
    }

    ///--[Time Left]
    //--Player's time left. Does not appear when it's -1. Gets set to -1 during the player's action.
    if(mPlayerTicksLeft > 0)
    {
        //--Static piece.
        Images.Data.rTimeHeader->Draw(0, cCurY);

        //--Resolve the seconds and decimals to render.
        int tSeconds = mPlayerTicksLeft / 60;
        float tFraction = tSeconds + ((mPlayerTicksLeft % 60) / 60.0f);

        //--Render them into a buffer.
        char tBuf[32];
        sprintf(tBuf, "Time: %5.3f", tFraction);

        //--Render.
        Images.Data.rTimeFont->DrawText(566.0f, 20.0f, 0, 1.0f, tBuf);
    }

    ///--[Player's Health Bar]
    //--The health bar renders under the static frame. It is also a partial
    //  render based on an easing pack.
    float tRenderPct = mPlayerHealthEasing.mXCur / (float)MAX_HEALTH;
    if(tRenderPct > 0.0f)
    {
        Images.Data.rHealthFillLft->RenderPercent(cCurL, 0, 0.0f, 1.0f - tRenderPct, 1.0f, 1.0f);
    }

    //--Render the frame over the bar.
    Images.Data.rHealthFrameLft->Draw(cCurL, 0);

    ///--[Challenger's Health Bar]
    //--The health bar renders under the static frame.
    tRenderPct = mChallengerHealthEasing.mXCur / (float)MAX_HEALTH;
    if(tRenderPct > 0.0f)
    {
        Images.Data.rHealthFillRgt->RenderPercent(cCurR, 0, 0.0f, 1.0f - tRenderPct, 1.0f, 1.0f);
    }

    //--Render the frame over the bar.
    Images.Data.rHealthFrameRgt->Draw(cCurR, 0);

    ///--[Confirm Overlay]
    //--Appears when the player confirms their action. Darkens the rest of the screen.
    if(mConfirmTimer > 0)
    {
        //--Fade percent.
        float cMaxOverlay = 0.50f;
        float tPercent = EasingFunction::Linear(mConfirmTimer, UI_CONFIRM_TICKS);

        //--Dark overlay.
        StarBitmap::DrawFullBlack(tPercent * cMaxOverlay);

        //--Render the overlay box. This is a fade as well.
        glColor4f(1.0f, 1.0f, 1.0f, tPercent);
        Images.Data.rConfirmBox->Draw();

        //--Text.
        Images.Data.rConfirmFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 180.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Begin your move?");
        mConfirmAccept->DrawText(600.0f, 280.0f, SUGARFONT_AUTOCENTER_X, 1.0f, Images.Data.rConfirmSmallFont);
        mConfirmCancel->DrawText(733.0f, 280.0f, SUGARFONT_AUTOCENTER_X, 1.0f, Images.Data.rConfirmSmallFont);

        //--Clean.
        StarlightColor::ClearMixer();
    }

    ///--[Title Overlay]
    //--In print mode, the letters render one by one.
    if(mTitlePrintString && mTitlePrintPhase == PF_TITLE_PRINT_PRINT)
    {
        ///--[Static Pieces]
        //--These render slide-scrolling in.
        int cUseTimer = LowestI(45, mTitlePrintTimerMax);
        float cPiecePercent = EasingFunction::QuadraticIn(mTitlePrintTimer, cUseTimer);
        Images.Data.rTextTop->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cPiecePercent, 1.0f);
        Images.Data.rTextBot->RenderPercent(0.0f, 0.0f, 1.0f - cPiecePercent, 0.0f, 1.0f, 1.0f);

        ///--[Text]
        //--Number of letters to render is based on the timer.
        int tLettersToRender = mTitlePrintTimer / TITLE_PRINT_TICKS_PER_LETTER;

        //--Get the expected length of the entire string. It centers automatically.
        float tExpectedLen = Images.Data.rCenterTitleText->GetTextWidth(mTitlePrintString);
        float tLft = (VIRTUAL_CANVAS_X - tExpectedLen) * 0.50f;

        //--Copy the letters into a fixed-size buffer.
        char tBuf[128];
        int tLetters = LowestI(tLettersToRender, 127);
        strncpy(tBuf, mTitlePrintString, tLetters);
        tBuf[tLetters] = '\0';

        //--Print the fixed-size buffer at the left side. This will dynamically center across the screen.
        Images.Data.rCenterTitleText->DrawText(tLft, 170.0f, 0, 1.0f, tBuf);
    }
    //--In hold mode, render the whole string with no fading.
    else if(mTitlePrintString && mTitlePrintPhase == PF_TITLE_PRINT_HOLD)
    {
        //--Static Pieces render whole.
        Images.Data.rTextTop->Draw();
        Images.Data.rTextBot->Draw();

        //--Get the expected length of the entire string. We make sure to use the same centering logic as above
        //  to avoid incongruencies.
        float tExpectedLen = Images.Data.rCenterTitleText->GetTextWidth(mTitlePrintString);
        float tLft = (VIRTUAL_CANVAS_X - tExpectedLen) * 0.50f;
        Images.Data.rCenterTitleText->DrawText(tLft, 170.0f, 0, 1.0f, mTitlePrintString);
    }
    //--In fade mode, the letters all render and fade out.
    else if(mTitlePrintString && mTitlePrintPhase == PF_TITLE_PRINT_FADE)
    {
        //--Compute and set alpha.
        float tAlpha = EasingFunction::QuadraticIn(mTitlePrintTimer, mTitlePrintTimerMax);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f - tAlpha);

        //--Static Pieces render whole matching the alpha.
        Images.Data.rTextTop->Draw();
        Images.Data.rTextBot->Draw();

        //--Get the expected length of the entire string. We make sure to use the same centering logic as above
        //  to avoid incongruencies.
        float tExpectedLen = Images.Data.rCenterTitleText->GetTextWidth(mTitlePrintString);
        float tLft = (VIRTUAL_CANVAS_X - tExpectedLen) * 0.50f;
        Images.Data.rCenterTitleText->DrawText(tLft, 170.0f, 0, 1.0f, mTitlePrintString);

        //--Clean.
        StarlightColor::ClearMixer();
    }
}
