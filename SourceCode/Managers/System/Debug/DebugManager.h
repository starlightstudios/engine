///======================================= DebugManager ===========================================
//--A singleton static Manager, it cannot be instantiated.  Instead, it exists entirely as a static
//  set of function calls and variables.
//--This manager allows a structured usage of the console for debug output.  Using its push/pop
//  causes debug prints to automatically be indented by topic.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class DebugManager
{
    private:
    //--System
    DebugManager();
    ~DebugManager();

    //--Flags
    static StarLinkedList *xDebugFlagList; //bool *, master

    //--Visual Trace
    static int xVisTraceCount;
    static float xVisTraceX;
    static float xVisTraceY;

    //--Stack controller
    static bool xFullDisable;
    static int xCurrentIndent;
    static StarLinkedList *xSuppressList;

    protected:

    public:
    static uint32_t xVisTraceFlag;
    static uint32_t xVisTraceGlobalFlag;
    static int xPrintNextVisTrace;

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods


    //--Update
    //--File I/O
    static void Indent();
    static void IndentSubOne();
    static void Push(bool pFlag);
    static void Pop();
    static void Print(const char *pString, ...);
    static void PushPrint(const char *pVarName, const char *pString, ...);
    static void PushPrint(bool pFlag, const char *pString, ...);
    static void PopPrint(const char *pString, ...);
    static void ForcePrint(const char *pString, ...);

    //--Drawing
    static void ResetVisualTrace();
    static void RenderVisualTrace(const char *pLetter, ...);

    //--Pointer Routing
    //--Static Functions
    static void RegisterDebugFlag(const char *pName, bool pFlag);
    static void SetDebugFlag(const char *pName, bool pFlag);
    static bool GetDebugFlag(const char *pName);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Debug_SetVersion(lua_State *L);
int Hook_Debug_PushPrint(lua_State *L);
int Hook_Debug_PushPrintS(lua_State *L);
int Hook_Debug_PopPrint(lua_State *L);
int Hook_Debug_Print(lua_State *L);
int Hook_Debug_ForcePrint(lua_State *L);
int Hook_Debug_DropEvents(lua_State *L);
int Hook_Debug_RegisterFlag(lua_State *L);
int Hook_Debug_GetFlag(lua_State *L);
int Hook_Debug_SetFlag(lua_State *L);
