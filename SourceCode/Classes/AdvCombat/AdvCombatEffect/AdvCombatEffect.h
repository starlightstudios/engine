///====================================== AdvCombatEffect =========================================
//--An effect in combat, which can be buffs, debuffs, damage-over-times, or anything a scripter
//  can dream up.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "StarlightString.h"

///===================================== Local Structures =========================================
//--Wrapper around an AdvCombatEntity and a StarlightString.
typedef struct AdvCombatEffectTargetPack
{
    //--Members
    uint32_t mTargetID;
    AdvCombatEntity *rTargetPtr;
    StarlightString *mDisplayString;
    int mDescriptionLinesTotal;
    StarlightString **mDescriptionLines;

    //--Functions
    void Initialize();
    void AllocateDescriptionLines(int pTotal);
    void SetDescriptionText(int pSlot, const char *pText);
    void AllocateDescriptionImages(int pSlot, int pImageTotal);
    void SetDescriptionImage(int pSlot, int pImageIndex, float pYOffset, const char *pDLPath);
    void CrossloadImages(int pSlot);
    static void DeleteThis(void *pPtr);
}AdvCombatEffectTargetPack;

///===================================== Local Definitions ========================================
#define ADVCE_MAX_ARGS 10

///========================================== Classes =============================================
class AdvCombatEffect : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;
    bool mRemoveNow;
    bool mDontShowOnUI;
    bool mDontShowOnQuickUI;
    bool mDontShowShortText;

    //--Sort Priority
    int mEffectPriority;
    uint32_t mEffectTimeIndex;

    //--Display
    StarBitmap *rBackImg;
    StarBitmap *rFrameImg;
    StarBitmap *rFrontImg;

    //--Script Handler
    float mArguments[ADVCE_MAX_ARGS];
    int mApplicationPriority;
    char *mScriptPath;
    char *mEffectPrototypeName;

    //--Targets
    AdvCombatEntity *rOriginator;
    StarLinkedList *mTargetList; //AdvCombatEffectTargetPack *, master

    //--Tags
    StarLinkedList *mTagList; //int *, master

    protected:

    public:
    //--System
    AdvCombatEffect();
    virtual ~AdvCombatEffect();

    //--Public Variables
    static char *xGlobalEffectName;

    //--Property Queries
    const char *GetDisplayName();
    const char *GetScriptPath();
    bool IsRemovedNow();
    bool IsIDOnTargetList(uint32_t pID);
    int GetTagCount(const char *pTag);
    int GetTagCountPartial(const char *pTag);
    const char *GetFirstTagNameMatching(const char *pTag);
    bool IsVisibleOnUI();
    bool IsVisibleOnQuickUI();
    bool HasShortText();
    int GetEffectPriority();
    uint32_t GetTimeIndex();
    float GetArgument(int pIndex);
    const char *GetGlobalPrototypeName();

    //--Manipulators
    void SetDisplayName(const char *pName);
    void SetOriginatorByID(uint32_t pID);
    void SetScriptPath(const char *pPath);
    void SetRemoveNow(bool pFlag);
    void SetHideOnUIFlag(bool pFlag);
    void SetHideOnQuickUIFlag(bool pFlag);
    void SetHideShortText(bool pFlag);
    void AddTargetByID(uint32_t pID);
    void SetFrontImage(const char *pPath);
    void SetFrameImage(const char *pPath);
    void SetBackImage(const char *pPath);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);
    void SetEffectPriority(int pPriority);
    void SetGlobalPrototypeName(const char *pName);
    void SetArgument(int pIndex, float pValue);

    //--Core Methods
    void ApplyStatsToTargets();
    void UnApplyStatsToTargets();
    void ApplyStatsToTarget(uint32_t pTargetID);
    void UnApplyStatsToTarget(uint32_t pTargetID);
    void SetLocalPrototypeAsStatic();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void RenderAt(float pX, float pY);

    //--Pointer Routing
    StarBitmap *GetBackImage();
    StarBitmap *GetFrameImage();
    StarBitmap *GetFrontImage();
    AdvCombatEntity *GetOriginator();
    StarLinkedList *GetTargetList();
    StarlightString *GetStringByTargetID(uint32_t pTargetID);
    AdvCombatEffectTargetPack *GetTargetPackByID(uint32_t pTargetID);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatEffect_GetProperty(lua_State *L);
int Hook_AdvCombatEffect_SetProperty(lua_State *L);

