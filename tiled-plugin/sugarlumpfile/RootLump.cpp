#include "RootLump.h"
#include <stdlib.h>

//--System
RootLump::RootLump()
{
    mTypeCode = 0;
    mAddressInFile = 0;
    mAddressOfAddress = 0;

    mName = NULL;
}
RootLump::~RootLump()
{
    free(mName);
}

//--Public Variables
//--Property Queries
int RootLump::GetType()
{
    return mTypeCode;
}

//--Manipulators
void RootLump::SetAddressOfAddress(uint64_t pAddress)
{
    mAddressOfAddress = pAddress;
}

//--Core Methods
//--Update
//--File I/O
void RootLump::WriteTo(uint64_t &sCounter, FILE *pOutfile)
{
    //--Write the Lump's data to a file.  The WriteData function should be overridden, the default
    //  behavior of WriteTo will store the position of the Lump in the file.
    mAddressInFile = sCounter;
    WriteData(sCounter, pOutfile);
}

void RootLump::WriteData(uint64_t &, FILE *)
{
    //--Override this in derived classes.
}

void RootLump::WriteAddress(FILE *pOutfile)
{
    //--Write the address stored by the Lump to the provided file.  The SugarLumpFile takes care
    //  of writing the name.
    if(!pOutfile) return;
    fseek(pOutfile, (long)mAddressOfAddress, SEEK_SET);
    fwrite(&mAddressInFile, sizeof(uint64_t), 1, pOutfile);
    //fprintf(stderr, "Address of lump %s is %i\n", mName, mAddressInFile);
}

