//--Base
#include "AbyssCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdvCombatPrediction.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Local Definitions]
#define ABYCOM_CHAR0_X 170.0f
#define ABYCOM_CHAR1_X 530.0f
#define ABYCOM_CHAR2_X 850.0f
#define ABYCOM_CHAR_Y  400.0f

///========================================= Functions ============================================
void AbyssCombat::ResolveBattlePosition(AdvCombatEntity *pEntity, int pTypeFlag, float &sXPos, float &sYPos)
{
    ///--[Documentation and Setup]
    //--Given an entity, returns its nominal position in the shared variables. This varies slightly
    //  by text versus animations.
    if(!pEntity) return;

    //--Constants.
    float cMainTextY = 274.0f;
    float cSecondaryTextY = 194.0f;
    float cAnimationY = 100.0f;

    ///--[Resolve]
    //--Primary Text:
    if(pTypeFlag == ADVCOMBAT_POSTYPE_PRIMARY_TEXT)
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cMainTextY;
    }
    //--Secondary Text:
    else if(pTypeFlag == ADVCOMBAT_POSTYPE_SECONDARY_TEXT)
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cSecondaryTextY;
    }
    //--Animation:
    else
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cAnimationY;
    }

    ///--[Player Party]
    //--The player's party is positioned specially. If the entity is in the player's party, override
    //  the positions.
    uint32_t tTargetID = pEntity->GetID();
    int tTargetPartyID = GetPartyGroupingID(tTargetID);

    //--Not in the player's party, ignore.
    if(tTargetPartyID != AC_PARTY_GROUP_PARTY) return;

    //--Get the slot of the party member.
    if(pEntity == mrCombatParty->GetElementBySlot(0))
    {
        sXPos = ABYCOM_CHAR0_X;
        sYPos = ABYCOM_CHAR_Y;
    }
    else if(pEntity == mrCombatParty->GetElementBySlot(1))
    {
        sXPos = ABYCOM_CHAR1_X;
        sYPos = ABYCOM_CHAR_Y;
    }
    else if(pEntity == mrCombatParty->GetElementBySlot(2))
    {
        sXPos = ABYCOM_CHAR2_X;
        sYPos = ABYCOM_CHAR_Y;
    }
}
void AbyssCombat::SetPredictionBoxHostI(const char *pBoxName, uint32_t pHostID)
{
    ///--[Documentation]
    //--Identical to the base copy, but if the prediction box is focused on a player party member,
    //  then it also sets lock positions so the box shows in the right spot.
    int tTargetPartyID = GetPartyGroupingID(pHostID);
    AdvCombat::SetPredictionBoxHostI(pBoxName, pHostID);

    //--Not in the player's party, ignore.
    if(tTargetPartyID != AC_PARTY_GROUP_PARTY) return;

    //--Get prediction box.
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;

    //--Get associated entity.
    AdvCombatEntity *rEntity = GetEntityByID(pHostID);

    //--Get the slot of the party member.
    if(rEntity == mrCombatParty->GetElementBySlot(0))
    {
        rPredictionBox->SetLockPositionFlag(true);
        rPredictionBox->SetLockPositions(ABYCOM_CHAR0_X, ABYCOM_CHAR_Y);
    }
    else if(rEntity == mrCombatParty->GetElementBySlot(1))
    {
        rPredictionBox->SetLockPositionFlag(true);
        rPredictionBox->SetLockPositions(ABYCOM_CHAR1_X, ABYCOM_CHAR_Y);
    }
    else if(rEntity == mrCombatParty->GetElementBySlot(2))
    {
        rPredictionBox->SetLockPositionFlag(true);
        rPredictionBox->SetLockPositions(ABYCOM_CHAR2_X, ABYCOM_CHAR_Y);
    }
}
/*
void AbyssCombat::HandleApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString)
{
    ///--[ ====== Documentation and Setup ===== ]
    //--Override of the base version's HandleApplication(), can execute special instructions. If an
    //  override is not required, calls the parent class version.
    if(!pString || !pTargetID) return;

    ///--[No Override]
    //--Break the string using the Subdivide algorithm.
    StarLinkedList *tStringList = Subdivide::SubdivideStringToList(pString, "|");
    int tArgs = tStringList->GetListSize();
    const char *rSwitchType = (char *)tStringList->GetElementBySlot(0);
    if(!rSwitchType)
    {
        delete tStringList;
        return;
    }

    //--Only the following applications deal with an override. All others get sent to the parent.
    if(!(!strcasecmp(rSwitchType, "Create Animation") && tArgs >= 3) &&
       !(!strcasecmp(rSwitchType, "Text")             && tArgs >= 2)   )
    {
        fprintf(stderr, "Bypass %s %i\n", rSwitchType, tArgs);
        AdvCombat::HandleApplication(pOriginatorID, pTargetID, pString);
        delete tStringList;
        return;
    }

    ///--[Target Acquisition]
    //--First, locate the target. It can be either in the party or the enemy roster.
    AdvCombatEntity *rApplyEntity = NULL;

    //--Search the player's party. Note we search the *entire* roster, which means party members
    //  can be affected if they're not even in the battle!
    AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pTargetID)
        {
            rApplyEntity = rCheckEntity;
            mPartyRoster->PopIterator();
            break;
        }
        rCheckEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }

    //--If not found in the player's roster, search the enemy roster.
    rCheckEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pTargetID)
        {
            rApplyEntity = rCheckEntity;
            mEnemyRoster->PopIterator();
            break;
        }
        rCheckEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--If there was no target, fail here.
    fprintf(stderr, "Handling up override %s %p\n", rSwitchType, rApplyEntity);
    if(!rApplyEntity) return;

    ///--[ ======== Effect Application ======== ]
    //--Constants.
    float cMainTextY = 274.0f;
    float cSecondaryTextY = 194.0f;
    float cAnimationY = 100.0f;

    ///--[Animation]
    if(!strcasecmp(rSwitchType, "Create Animation") && tArgs >= 3)
    {
        //--X/Y Position.
        float tUseX = rApplyEntity->GetCombatX();
        float tUseY = cAnimationY;

        //--Special: If the target is in the player's party, it needs to be repositioned.
        int tTargetPartyID = GetPartyGroupingID(pTargetID);
        if(tTargetPartyID == AC_PARTY_GROUP_PARTY)
        {
            //--Get the slot of the party member.
            if(rApplyEntity == mrCombatParty->GetElementBySlot(0))
            {
                tUseX = ABYCOM_CHAR0_X;
                tUseY = ABYCOM_CHAR_Y;
            }
            else if(rApplyEntity == mrCombatParty->GetElementBySlot(1))
            {
                tUseX = ABYCOM_CHAR1_X;
                tUseY = ABYCOM_CHAR_Y;
            }
            else if(rApplyEntity == mrCombatParty->GetElementBySlot(2))
            {
                tUseX = ABYCOM_CHAR2_X;
                tUseY = ABYCOM_CHAR_Y;
            }
        }

        //--Optional arguments:
        for(int i = 3; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Override the X position.
            if(!strcasecmp(rSubSwitchType, "UseX") && tSubArgs == 2)
            {
                tUseX = atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Override the Y position.
            else if(!strcasecmp(rSubSwitchType, "UseY") && tSubArgs == 2)
            {
                tUseY = atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Offset the X position.
            else if(!strcasecmp(rSubSwitchType, "OffX") && tSubArgs == 2)
            {
                tUseX = tUseX + atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Offset the Y position.
            else if(!strcasecmp(rSubSwitchType, "OffY") && tSubArgs == 2)
            {
                tUseY = tUseY + atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Effect' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--Create.
        CreateAnimationInstance((char *)tStringList->GetElementBySlot(1), (char *)tStringList->GetElementBySlot(2), 0, tUseX, tUseY);
    }
    //--Text. Spawns a text package.
    else if(!strcasecmp(rSwitchType, "Text") && tArgs >= 2)
    {
        //--Create and set package to defaults.
        CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
        nPack->Initialize();
        nPack->mTicks = 0;
        nPack->mTicksMax = ADVCOMBAT_INFIELD_TEXT_TICKS;
        nPack->mText = InitializeString((const char *)tStringList->GetElementBySlot(1));
        nPack->mX = rApplyEntity->GetCombatX();
        nPack->mY = cSecondaryTextY;
        nPack->mColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
        mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

        //--Special: If the target is in the player's party, it needs to be repositioned.
        int tTargetPartyID = GetPartyGroupingID(pTargetID);
        if(tTargetPartyID == AC_PARTY_GROUP_PARTY)
        {
            //--Get the slot of the party member.
            if(rApplyEntity == mrCombatParty->GetElementBySlot(0))
            {
                nPack->mX = ABYCOM_CHAR0_X;
                nPack->mY = ABYCOM_CHAR_Y;
            }
            else if(rApplyEntity == mrCombatParty->GetElementBySlot(1))
            {
                nPack->mX = ABYCOM_CHAR1_X;
                nPack->mY = ABYCOM_CHAR_Y;
            }
            else if(rApplyEntity == mrCombatParty->GetElementBySlot(2))
            {
                nPack->mX = ABYCOM_CHAR2_X;
                nPack->mY = ABYCOM_CHAR_Y;
            }
        }
        fprintf(stderr, "Special!\n");

        //--Scatter the text position.
        ScatterPosition(mTextScatterCounter, nPack->mX, nPack->mY);

        //--Run optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Color. Argument can be the name of a preset color, or of format 0xRRRxGGGxBBBxAAA to specify RGBA.
            if(!strcasecmp(rSubSwitchType, "Color") && tSubArgs == 2)
            {
                //--Color string.
                const char *rColor = (const char *)tArgumentList->GetElementBySlot(1);

                //--Preset white.
                if(!strcasecmp(rColor, "White"))
                {
                    nPack->mColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
                }
                else if(!strcasecmp(rColor, "Green"))
                {
                    nPack->mColor.SetRGBAF(0.1f, 0.8f, 0.1f, 1.0f);
                }
                //--Preset purple.
                else if(!strcasecmp(rColor, "Purple"))
                {
                    nPack->mColor.SetRGBAF(0.7f, 0.2f, 0.7f, 1.0f);
                }
                //--Preset red.
                else if(!strcasecmp(rColor, "Red"))
                {
                    nPack->mColor.SetRGBAF(0.9f, 0.2f, 0.2f, 1.0f);
                }
                //--Preset blue.
                else if(!strcasecmp(rColor, "Blue"))
                {
                    nPack->mColor.SetRGBAF(0.2f, 0.2f, 0.9f, 1.0f);
                }
                //--Preset yellow
                else if(!strcasecmp(rColor, "Yellow"))
                {
                    nPack->mColor.SetRGBAF(0.8f, 0.8f, 0.1f, 1.0f);
                }
                //--Use integers. Expects format 0xRRRxGGGxBBBxAAA
                else if(!strncasecmp(rColor, "0x", 2) && strlen(rColor) >= 17)
                {
                    char tRedBuf[4];
                    char tBluBuf[4];
                    char tGrnBuf[4];
                    char tAlpBuf[4];
                    for(int i = 0; i < 3; i ++)
                    {
                        tRedBuf[i] = rColor[ 2+i];
                        tBluBuf[i] = rColor[ 6+i];
                        tGrnBuf[i] = rColor[10+i];
                        tAlpBuf[i] = rColor[14+i];
                    }
                    tRedBuf[3] = '\0';
                    tBluBuf[3] = '\0';
                    tGrnBuf[3] = '\0';
                    tAlpBuf[3] = '\0';
                    nPack->mColor.SetRGBAI(atoi(tRedBuf), atoi(tBluBuf), atoi(tGrnBuf), atoi(tAlpBuf));
                }
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Text' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }
    }

    ///--[Clean up]
    delete tStringList;
    fprintf(stderr, "Cleaned up override %s\n", rSwitchType);
}*/
