///=============================== PeakFreaks Common Definitions ==================================
//--Definitions common between multiple Peak Freaks classes.

#pragma once

///======================================== Definitions ===========================================
//--Cloud Constants
#define PFTITLE_CLOUD_FRAMES  3
#define PFTITLE_CLOUD_TICKS 240 //Ticks between spawning clouds
#define PFTITLE_CLOUD_Y_LO   50 //Highest Y position a cloud can spawn at on the screen
#define PFTITLE_CLOUD_Y_HI  600 //Lowest Y position a cloud can spawn at on the screen
#define PFTITLE_CLOUD_SPEED_LO 7
#define PFTITLE_CLOUD_SPEED_HI 25

//--Fireworks
#define PFTITLE_FIREWORK_TPF 5
#define PFTITLE_FIREWORK_SPAWN_TICKS_LO 3
#define PFTITLE_FIREWORK_SPAWN_TICKS_HI 15

///========================================= Structures ===========================================
///--[PFTitle_Cloud]
//--Represents a cloud in the background. Scrolls across the sky.
typedef struct PFTitle_Cloud
{
    float mXPos;
    int mYPos;
    float mXSpeed;
    SugarBitmap *rImage;
    void Initialize()
    {
        mXPos = 0.0f;
        mYPos = 0;
        mXSpeed = 0.0f;
        rImage = NULL;
    }
}PFTitle_Cloud;

///--[PFTitle_Firework]
//--Represents a firework in the background on the finale screen. Runs through an animation
//  then despawns.
typedef struct PFTitle_Firework
{
    float mXPos;
    float mYPos;
    int mAnimTimer;
    int mColorIndex;
    void Initialize()
    {
        mXPos = 0.0f;
        mYPos = 0.0f;
        mAnimTimer = 0;
        mColorIndex = 0;
    }
}PFTitle_Firework;

///--[PFTitle_Highscore]
//--Represents a high-score file. Stores 9 best times and 3 victory flags.
typedef struct PFTitle_Highscore
{
    //--Variables
    int mEasyTimes[3];
    int mMediumTimes[3];
    int mHardTimes[3];
    bool mWonEasy;
    bool mWonMedium;
    bool mWonHard;

    //--Functions
    void Initialize()
    {
        memset(mEasyTimes, 0, sizeof(int) * 3);
        memset(mMediumTimes, 0, sizeof(int) * 3);
        memset(mHardTimes, 0, sizeof(int) * 3);
        mWonEasy = false;
        mWonMedium = false;
        mWonHard = false;
    }
}PFTitle_Highscore;
