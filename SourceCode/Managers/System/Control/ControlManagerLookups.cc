//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

///--[ ========================================= Allegro ======================================== ]
#if defined _ALLEGRO_PROJECT_
void ControlManager::BuildKeyNameLookups()
{
    ///--[Documentation]
    //--What it says on the tin, constructs a series of strings which each represent a human-readable
    //  name of a key, like "Mouse 1" or "A" or "JoyBtn 7".
    //--These are stored in the ControlManager and pointers can be tossed back for printing.
    //--How the data is extracted is based on what library is being used.

    //--Defaults.
    mKeyboardNamesTotal = 0;
    mKeyboardNames = NULL;
    mMouseNamesTotal = 0;
    mMouseNames = NULL;
    mJoypadNamesTotal = 0;
    mJoypadNames = NULL;

    ///--[Keyboard]
    //--Under Allegro, there are some handy constants and functions already setup to make this easy.
    mKeyboardNamesTotal = ALLEGRO_KEY_MAX;
    SetMemoryData(__FILE__, __LINE__);
    mKeyboardNames = (char **)starmemoryalloc(sizeof(char *) * mKeyboardNamesTotal);
    for(int i = 0; i < mKeyboardNamesTotal; i ++)
    {
        mKeyboardNames[i] = NULL;
        ResetString(mKeyboardNames[i], al_keycode_to_name(i));
        //fprintf(stderr, "Set keyboard name: %s\n", mKeyboardNames[i]);
    }

    //--Manual overrides. Allegro does not know the names of all the keys.
    ResetString(mKeyboardNames[ALLEGRO_KEY_TILDE],      "Tilde");
    ResetString(mKeyboardNames[ALLEGRO_KEY_MINUS],      "Minus");
    ResetString(mKeyboardNames[ALLEGRO_KEY_EQUALS],     "Equals");
    ResetString(mKeyboardNames[ALLEGRO_KEY_OPENBRACE],  "LBrace");
    ResetString(mKeyboardNames[ALLEGRO_KEY_CLOSEBRACE], "RBrace");
    ResetString(mKeyboardNames[ALLEGRO_KEY_SEMICOLON],  "Semicolon");
    ResetString(mKeyboardNames[ALLEGRO_KEY_QUOTE],      "Quote");
    ResetString(mKeyboardNames[ALLEGRO_KEY_BACKSLASH],  "Backslash");
    ResetString(mKeyboardNames[ALLEGRO_KEY_BACKSLASH2], "Backslash");
    ResetString(mKeyboardNames[ALLEGRO_KEY_COMMA],      "Comma");
    ResetString(mKeyboardNames[ALLEGRO_KEY_FULLSTOP],   "Period");
    ResetString(mKeyboardNames[ALLEGRO_KEY_SLASH],      "Slash");
    ResetString(mKeyboardNames[ALLEGRO_KEY_PAD_SLASH],  "Slash");
    ResetString(mKeyboardNames[ALLEGRO_KEY_PAD_MINUS],  "Minus");
    ResetString(mKeyboardNames[ALLEGRO_KEY_PAD_PLUS],   "Plus");
    ResetString(mKeyboardNames[ALLEGRO_KEY_PAD_DELETE], "Backspace");
    ResetString(mKeyboardNames[ALLEGRO_KEY_PAD_ENTER],  "Enter");

    //--Print results.
    if(false)
    {
        FILE *fOutfile = fopen("KeysAL.txt", "w");
        for(int i = 0; i < mKeyboardNamesTotal; i ++)
        {
            fprintf(fOutfile, "Key %3i is %s\n", i, mKeyboardNames[i]);
        }
        fclose(fOutfile);
    }

    ///--[Mouse]
    //--All descriptions are "Mouse X" where X is a number.
    mMouseNamesTotal = al_get_mouse_num_buttons();
    SetMemoryData(__FILE__, __LINE__);
    mMouseNames = (char **)starmemoryalloc(sizeof(char *) * mMouseNamesTotal);
    for(int i = 0; i < mMouseNamesTotal; i ++)
    {
        char tBuffer[32];
        sprintf(tBuffer, "Mouse %i", i);

        mMouseNames[i] = NULL;
        ResetString(mMouseNames[i], tBuffer);
        //fprintf(stderr, "Mouse %3i is %s\n", i, mMouseNames[i]);
    }

    ///--[Joypad]
    //--Descriptions are provided by allegro, but are usually... non-descript. Based on the driver.
    ALLEGRO_JOYSTICK *rStick0 = al_get_joystick(0);
    if(rStick0)
    {
        mJoypadNamesTotal = al_get_joystick_num_buttons(rStick0);
        SetMemoryData(__FILE__, __LINE__);
        SetMemoryData(__FILE__, __LINE__);
        mJoypadNames = (char **)starmemoryalloc(sizeof(char *) * mJoypadNamesTotal);
        for(int i = 0; i < mJoypadNamesTotal; i ++)
        {
            mJoypadNames[i] = NULL;
            ResetString(mJoypadNames[i], al_get_joystick_button_name(rStick0, i));
            //fprintf(stderr, "Joypad %3i is %s\n", i, mJoypadNames[i]);
        }
    }
    //--Warning of no joysticks. Ignore it if you're not using one, duh.
    else
    {
        //fprintf(stderr, "No joysticks currently detected.\n");
    }

    //--In addition, builds a lookup of key scancodes and what their 'names' are, that is, what
    //  they are on the keyboard. Some of this is automatic, some is manual.
    for(int i = 0; i < KEYCODES_MAX; i ++)
    {
        mKeycodes[i].mNormalChar = '\0';
        mKeycodes[i].mShiftedChar = '\0';
    }

    //--Alphabetical keys.
    for(int i = ALLEGRO_KEY_A; i <= ALLEGRO_KEY_Z; i ++)
    {
        mKeycodes[i].mNormalChar = (i - ALLEGRO_KEY_A) + 'a';
        mKeycodes[i].mShiftedChar = (i - ALLEGRO_KEY_A) + 'A';
    }

    //--Numerical keys.
    for(int i = ALLEGRO_KEY_0; i < ALLEGRO_KEY_9; i ++)
    {
        mKeycodes[i].mNormalChar = (i - ALLEGRO_KEY_0) + '0';
    }
    mKeycodes[ALLEGRO_KEY_0].mShiftedChar = ')';
    mKeycodes[ALLEGRO_KEY_1].mShiftedChar = '!';
    mKeycodes[ALLEGRO_KEY_2].mShiftedChar = '@';
    mKeycodes[ALLEGRO_KEY_3].mShiftedChar = '#';
    mKeycodes[ALLEGRO_KEY_4].mShiftedChar = '$';
    mKeycodes[ALLEGRO_KEY_5].mShiftedChar = '%';
    mKeycodes[ALLEGRO_KEY_6].mShiftedChar = '^';
    mKeycodes[ALLEGRO_KEY_7].mShiftedChar = '&';
    mKeycodes[ALLEGRO_KEY_8].mShiftedChar = '*';
    mKeycodes[ALLEGRO_KEY_9].mShiftedChar = '(';

    //--Other keys.
    mKeycodes[ALLEGRO_KEY_TILDE].mNormalChar = '`';
    mKeycodes[ALLEGRO_KEY_TILDE].mShiftedChar = '~';
    mKeycodes[ALLEGRO_KEY_MINUS].mNormalChar = '-';
    mKeycodes[ALLEGRO_KEY_MINUS].mShiftedChar = '_';
    mKeycodes[ALLEGRO_KEY_EQUALS].mNormalChar = '=';
    mKeycodes[ALLEGRO_KEY_EQUALS].mShiftedChar = '+';

    mKeycodes[ALLEGRO_KEY_OPENBRACE].mNormalChar = '[';
    mKeycodes[ALLEGRO_KEY_OPENBRACE].mShiftedChar = '{';
    mKeycodes[ALLEGRO_KEY_CLOSEBRACE].mNormalChar = ']';
    mKeycodes[ALLEGRO_KEY_CLOSEBRACE].mShiftedChar = '}';
    mKeycodes[ALLEGRO_KEY_BACKSLASH].mNormalChar = '\\';
    mKeycodes[ALLEGRO_KEY_BACKSLASH].mShiftedChar = '|';

    mKeycodes[ALLEGRO_KEY_SEMICOLON].mNormalChar = ';';
    mKeycodes[ALLEGRO_KEY_SEMICOLON].mShiftedChar = ':';
    mKeycodes[ALLEGRO_KEY_QUOTE].mNormalChar = '\'';
    mKeycodes[ALLEGRO_KEY_QUOTE].mShiftedChar = '"';

    mKeycodes[ALLEGRO_KEY_COMMA].mNormalChar = ',';
    mKeycodes[ALLEGRO_KEY_COMMA].mShiftedChar = '<';
    mKeycodes[ALLEGRO_KEY_FULLSTOP].mNormalChar = '.';
    mKeycodes[ALLEGRO_KEY_FULLSTOP].mShiftedChar = '>';
    mKeycodes[ALLEGRO_KEY_SLASH].mNormalChar = '/';
    mKeycodes[ALLEGRO_KEY_SLASH].mShiftedChar = '?';

    mKeycodes[ALLEGRO_KEY_SPACE].mNormalChar = ' ';
    mKeycodes[ALLEGRO_KEY_SPACE].mShiftedChar = ' ';

    mKeycodes[ALLEGRO_KEY_BACKSPACE].mNormalChar = SPECIALCODE_BACKSPACE;
    mKeycodes[ALLEGRO_KEY_BACKSPACE].mShiftedChar = SPECIALCODE_BACKSPACE;

    mKeycodes[ALLEGRO_KEY_ENTER].mNormalChar = SPECIALCODE_RETURN;
    mKeycodes[ALLEGRO_KEY_ENTER].mShiftedChar = SPECIALCODE_RETURN;

    mKeycodes[ALLEGRO_KEY_ESCAPE].mNormalChar = SPECIALCODE_CANCEL;
    mKeycodes[ALLEGRO_KEY_ESCAPE].mShiftedChar = SPECIALCODE_CANCEL;
}

//--[ =========================================== SDL =========================================== ]
#elif defined _SDL_PROJECT_
void ControlManager::BuildKeyNameLookups()
{
    ///--[Documentation]
    //--What it says on the tin, constructs a series of strings which each represent a human-readable
    //  name of a key, like "Mouse 1" or "A" or "JoyBtn 7".
    //--These are stored in the ControlManager and pointers can be tossed back for printing.
    //--How the data is extracted is based on what library is being used.

    //--Defaults.
    mKeyboardNamesTotal = 0;
    mKeyboardNames = NULL;
    mMouseNamesTotal = 0;
    mMouseNames = NULL;
    mJoypadNamesTotal = 0;
    mJoypadNames = NULL;

    //--[Keyboard]
    //--Autogenerate the listing of keys.
    mKeyboardNamesTotal = SDL_NUM_SCANCODES;
    SetMemoryData(__FILE__, __LINE__);
    mKeyboardNames = (char **)starmemoryalloc(sizeof(char *) * mKeyboardNamesTotal);
    for(int i = 0; i < mKeyboardNamesTotal; i ++)
    {
        mKeyboardNames[i] = NULL;
        SDL_Keycode tKeycode = SDL_GetKeyFromScancode((SDL_Scancode)i);
        const char *rKeyName = SDL_GetKeyName(tKeycode);
        ResetString(mKeyboardNames[i], rKeyName);
    }

    //--Manual overrides. This is to make sure they match the Allegro versions.
    ResetString(mKeyboardNames[SDL_SCANCODE_GRAVE],        "Tilde");
    ResetString(mKeyboardNames[SDL_SCANCODE_MINUS],        "Minus");
    ResetString(mKeyboardNames[SDL_SCANCODE_EQUALS],       "Equals");
    ResetString(mKeyboardNames[SDL_SCANCODE_LEFTBRACKET],  "LBrace");
    ResetString(mKeyboardNames[SDL_SCANCODE_RIGHTBRACKET], "RBrace");
    ResetString(mKeyboardNames[SDL_SCANCODE_SEMICOLON],    "Semicolon");
    ResetString(mKeyboardNames[SDL_SCANCODE_APOSTROPHE],   "Quote");
    ResetString(mKeyboardNames[SDL_SCANCODE_BACKSLASH],    "Backslash");
    ResetString(mKeyboardNames[SDL_SCANCODE_COMMA],        "Comma");
    ResetString(mKeyboardNames[SDL_SCANCODE_PERIOD],       "Period");
    ResetString(mKeyboardNames[SDL_SCANCODE_SLASH],        "Slash");
    ResetString(mKeyboardNames[SDL_SCANCODE_LSHIFT],       "LShift");
    ResetString(mKeyboardNames[SDL_SCANCODE_RSHIFT],       "RShift");
    ResetString(mKeyboardNames[SDL_SCANCODE_LCTRL],        "LCtrl");
    ResetString(mKeyboardNames[SDL_SCANCODE_RCTRL],        "RCtrl");

    //--Print results.
    if(false)
    {
        FILE *fOutfile = fopen("KeysSDL.txt", "w");
        for(int i = 0; i < mKeyboardNamesTotal; i ++)
        {
            fprintf(fOutfile, "Key %3i is %s\n", i, mKeyboardNames[i]);
        }
        fclose(fOutfile);
    }

    //--[Mouse]
    //--All descriptions are "Mouse X" where X is a number.
    mMouseNamesTotal = 10;
    SetMemoryData(__FILE__, __LINE__);
    mMouseNames = (char **)starmemoryalloc(sizeof(char *) * mMouseNamesTotal);
    for(int i = 0; i < mMouseNamesTotal; i ++)
    {
        char tBuffer[32];
        sprintf(tBuffer, "Mouse %i", i);

        mMouseNames[i] = NULL;
        ResetString(mMouseNames[i], tBuffer);
        //fprintf(stderr, "Mouse %3i is %s\n", i, mMouseNames[i]);
    }

    //--[Joypad]
    //--Descriptions are provided by allegro, but are usually... non-descript. Based on the driver.
    /*
    ALLEGRO_JOYSTICK *rStick0 = al_get_joystick(0);
    if(rStick0)
    {
        mJoypadNamesTotal = al_get_joystick_num_buttons(rStick0);
        SetMemoryData(__FILE__, __LINE__);
        mJoypadNames = (char **)starmemoryalloc(sizeof(char *) * mJoypadNamesTotal);
        for(int i = 0; i < mJoypadNamesTotal; i ++)
        {
            mJoypadNames[i] = NULL;
            ResetString(mJoypadNames[i], al_get_joystick_button_name(rStick0, i));
            //fprintf(stderr, "Joypad %3i is %s\n", i, mJoypadNames[i]);
        }
    }
    //--Warning of no joysticks. Ignore it if you're not using one, duh.
    else
    {
        //fprintf(stderr, "No joysticks currently detected.\n");
    }
    */

    //--In addition, builds a lookup of key scancodes and what their 'names' are, that is, what
    //  they are on the keyboard. Some of this is automatic, some is manual.
    for(int i = 0; i < KEYCODES_MAX; i ++)
    {
        mKeycodes[i].mNormalChar = '\0';
        mKeycodes[i].mShiftedChar = '\0';
    }

    //--Alphabetical keys.
    for(int i = SDL_SCANCODE_A; i <= SDL_SCANCODE_Z; i ++)
    {
        mKeycodes[i].mNormalChar = (i - SDL_SCANCODE_A) + 'a';
        mKeycodes[i].mShiftedChar = (i - SDL_SCANCODE_A) + 'A';
    }

    //--Numerical keys. SDL does these in a different order than standard ASCII so we need to do extra setup.
    mKeycodes[SDL_SCANCODE_0].mNormalChar = '0';
    for(int i = SDL_SCANCODE_1; i < SDL_SCANCODE_9; i ++)
    {
        mKeycodes[i].mNormalChar = (i - SDL_SCANCODE_1) + '1';
    }
    mKeycodes[SDL_SCANCODE_0].mShiftedChar = ')';
    mKeycodes[SDL_SCANCODE_1].mShiftedChar = '!';
    mKeycodes[SDL_SCANCODE_2].mShiftedChar = '@';
    mKeycodes[SDL_SCANCODE_3].mShiftedChar = '#';
    mKeycodes[SDL_SCANCODE_4].mShiftedChar = '$';
    mKeycodes[SDL_SCANCODE_5].mShiftedChar = '%';
    mKeycodes[SDL_SCANCODE_6].mShiftedChar = '^';
    mKeycodes[SDL_SCANCODE_7].mShiftedChar = '&';
    mKeycodes[SDL_SCANCODE_8].mShiftedChar = '*';
    mKeycodes[SDL_SCANCODE_9].mShiftedChar = '(';

    //--Other keys.
    mKeycodes[SDL_SCANCODE_GRAVE].mNormalChar = '`';
    mKeycodes[SDL_SCANCODE_GRAVE].mShiftedChar = '~';
    mKeycodes[SDL_SCANCODE_MINUS].mNormalChar = '-';
    mKeycodes[SDL_SCANCODE_MINUS].mShiftedChar = '_';
    mKeycodes[SDL_SCANCODE_EQUALS].mNormalChar = '=';
    mKeycodes[SDL_SCANCODE_EQUALS].mShiftedChar = '+';

    mKeycodes[SDL_SCANCODE_LEFTBRACKET].mNormalChar = '[';
    mKeycodes[SDL_SCANCODE_LEFTBRACKET].mShiftedChar = '{';
    mKeycodes[SDL_SCANCODE_RIGHTBRACKET].mNormalChar = ']';
    mKeycodes[SDL_SCANCODE_RIGHTBRACKET].mShiftedChar = '}';
    mKeycodes[SDL_SCANCODE_BACKSLASH].mNormalChar = '\\';
    mKeycodes[SDL_SCANCODE_BACKSLASH].mShiftedChar = '|';

    mKeycodes[SDL_SCANCODE_SEMICOLON].mNormalChar = ';';
    mKeycodes[SDL_SCANCODE_SEMICOLON].mShiftedChar = ':';
    mKeycodes[SDL_SCANCODE_APOSTROPHE].mNormalChar = '\'';
    mKeycodes[SDL_SCANCODE_APOSTROPHE].mShiftedChar = '"';

    mKeycodes[SDL_SCANCODE_COMMA].mNormalChar = ',';
    mKeycodes[SDL_SCANCODE_COMMA].mShiftedChar = '<';
    mKeycodes[SDL_SCANCODE_PERIOD].mNormalChar = '.';
    mKeycodes[SDL_SCANCODE_PERIOD].mShiftedChar = '>';
    mKeycodes[SDL_SCANCODE_SLASH].mNormalChar = '/';
    mKeycodes[SDL_SCANCODE_SLASH].mShiftedChar = '?';

    mKeycodes[SDL_SCANCODE_SPACE].mNormalChar = ' ';
    mKeycodes[SDL_SCANCODE_SPACE].mShiftedChar = ' ';

    mKeycodes[SDL_SCANCODE_BACKSPACE].mNormalChar = SPECIALCODE_BACKSPACE;
    mKeycodes[SDL_SCANCODE_BACKSPACE].mShiftedChar = SPECIALCODE_BACKSPACE;

    mKeycodes[SDL_SCANCODE_RETURN].mNormalChar = SPECIALCODE_RETURN;
    mKeycodes[SDL_SCANCODE_RETURN].mShiftedChar = SPECIALCODE_RETURN;

    mKeycodes[SDL_SCANCODE_ESCAPE].mNormalChar = SPECIALCODE_CANCEL;
    mKeycodes[SDL_SCANCODE_ESCAPE].mShiftedChar = SPECIALCODE_CANCEL;

}
#else
void ControlManager::BuildKeyNameLookups()
{
    fprintf(stderr, "No input library is active. Cannot build key lookups.\n");
}
#endif
