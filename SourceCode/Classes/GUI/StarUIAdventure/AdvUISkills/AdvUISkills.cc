//--Base
#include "AdvUISkills.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "StringEntry.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
AdvUISkills::AdvUISkills()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUISkills ======= ]
    ///--[System]
    ///--[Skills UI]
    //--System
    mArrowTimer = 0;
    mPurchaseTimer = 0;
    mShowAdvancedDescriptions = false;

    //--Mode
    mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
    mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;

    //--Cursors
    mCharacterCursor = 0;
    mJobCursor = 0;
    mJobSkip = 0;
    mSkillCursor = 0;
    mSkillSkip = 0;
    mAbilityCursorX = 0;
    mAbilityCursorY = 0;

    //--Tactics Grid
    mIsUsingTacticsGrid = false;
    mTacticsGridTimer = 0;

    //--Character Swap
    mIsSwappingCharacter = false;
    mCharacterSwapTimer = 0;
    mPrevCharacterSlot = -1;

    //--Details
    mIsShowingDetails = false;
    mIsDetailsFromGrid = false;
    mDetailsTimer = 0;

    //--Strings
    mShowHelpString = new StarlightString();
    mScrollFastString = new StarlightString();
    mEditProfilesString = new StarlightString();
    mSkillDetailsString = new StarlightString();
    mInspectSkillsString = new StarlightString();
    mShowTacticsString = new StarlightString();
    mCharacterLftString = new StarlightString();
    mCharacterRgtString = new StarlightString();
    mConfirmPurchaseString = new StarlightString();
    mConfirmDeleteString = new StarlightString();
    mCancelPurchaseString = new StarlightString();

    ///--[Profiles UI]
    //--System
    mIsProfilesMode = false;
    mIsProfileConfirm = false;
    mProfilesVisibilityTimer = 0;
    mProfileConfirmTimer = 0;

    //--Profile Select
    mProfileCursor = 0;
    mProfileSkip = 0;
    mProfileJobCursor = -1;

    //--New Profile
    mStringEntryTimer = 0;
    mStringEntryForm = StringEntry::rGenerateStringEntry();

    //--Instructions
    for(int i = 0; i < ADVMENU_SKILLS_INSTRUCTION_STRINGS_TOTAL; i ++)
    {
        mInstructionString[i] = new StarlightString();
    }

    //--Strings
    mNewProfileString = new StarlightString();
    mRenameProfileString = new StarlightString();
    mDeleteProfileString = new StarlightString();

    ///--[Common]
    //--Cursor Movement
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Backup Cursors
    mJobHighlightPos.Initialize();
    mJobHighlightSize.Initialize();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUISkills::~AdvUISkills()
{
    delete mStringEntryForm;
    delete mShowHelpString;
    delete mScrollFastString;
    delete mEditProfilesString;
    delete mSkillDetailsString;
    delete mInspectSkillsString;
    delete mShowTacticsString;
    delete mCharacterLftString;
    delete mCharacterRgtString;
    delete mConfirmPurchaseString;
    delete mConfirmDeleteString;
    delete mCancelPurchaseString;
    for(int i = 0; i < ADVMENU_SKILLS_INSTRUCTION_STRINGS_TOTAL; i ++)
    {
        delete mInstructionString[i];
    }
    delete mNewProfileString;
    delete mRenameProfileString;
    delete mDeleteProfileString;
}
void AdvUISkills::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Skills UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
bool AdvUISkills::IsCatalystSlot(int pX, int pY)
{
    ///--[Documentation]
    //--Given a slot, returns true if that slot is locked by catalyst requirements. All other slots
    //  return false.
    if(pX <  ACE_ABILITY_MEMORIZE_GRID_X0) return false;
    if(pX >= ACE_ABILITY_TACTICS_GRID_X0)  return false;
    if(pY < 2) return false;

    //--Get how many slots are unlocked by catalysts.
    int tCatalystSlots = AdvCombat::Fetch()->GetSkillCatalystSlots();
    if(pX < ACE_ABILITY_MEMORIZE_GRID_X0 + tCatalystSlots) return false;

    //--All checks passed, this is a catalyst slot. It is not usable.
    return true;
}
bool AdvUISkills::IsCustomizableSlot(int pX, int pY)
{
    ///--[Documentation]
    //--Given a slot, returns true if that slot is available for customization, false otherwise.
    //  This dynamically checks skill catalysts to unlock the extra slots.
    if(pX < 3)  return false;
    if(pX >= 8) return false;
    if(pY < 0)  return false;
    if(pY >= 3) return false;

    //--The 10 slots between x==3 and x==8, y==0 and y==2, are always free.
    if(pY < 2) return true;

    //--Check skill catalysts. The last row may be usable.
    int tCatalystSlots = AdvCombat::Fetch()->GetSkillCatalystSlots();
    if(pX < 3 + tCatalystSlots) return true;

    //--All checks failed.
    return false;
}
AdvCombatJob *AdvUISkills::GetHighlightedJob(int pJobCursor)
{
    ///--[Documentation]
    //--The jobs menu in the skills UI places the active job on the top of the list to give it priority.
    //  This means the other jobs are slightly out of order. Given a job cursor, this function figures
    //  out which job is actually highlighted and returns it.
    //--Returns NULL on error.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return NULL;

    //--Get the job list.
    AdvCombatJob *rActiveJob = rCurrentCharacter->GetActiveJob();
    StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();
    if(!rActiveJob || !rJobList) return NULL;

    //--If the slot is zero, it's always the active job.
    if(pJobCursor == 0) return rActiveJob;

    //--Get the slot the active job is in.
    int tActiveJobSlot = rJobList->GetSlotOfElementByPtr(rActiveJob);

    //--If the active slot came back -1, that means the active job is not on the list. In that case,
    //  return the element at the cursor position.
    if(tActiveJobSlot == -1) return (AdvCombatJob *)rJobList->GetElementBySlot(pJobCursor-1);

    //--If the active job's slot is above the cursor, offset.
    if(tActiveJobSlot >= pJobCursor) return (AdvCombatJob *)rJobList->GetElementBySlot(pJobCursor -1);

    //--Otherwise, the active job is already offset. Return by cursor.
    return (AdvCombatJob *)rJobList->GetElementBySlot(pJobCursor);
}

///======================================= Manipulators ===========================================
void AdvUISkills::TakeForeground()
{
    ///--[Construction]
    //--Reset all cursors.
    mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
    mCharacterCursor = 0;
    mJobCursor = 0;
    mJobSkip = 0;
    mSkillCursor = 0;
    mSkillSkip = 0;
    mAbilityCursorX = 0;
    mAbilityCursorY = 0;

    //--Run across all party characters. Order them to refresh their skill lists. This is important
    //  if they have changed equipment.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
    {
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        AdvCombatJob *rJob = rEntity->GetActiveJob();
        if(!rJob) continue;

        rJob->CallScript(ADVCJOB_CODE_BEGINCOMBAT);
    }

    ///--[Store JP Total]
    //--Entering the skills menu will store how much party JP you have, and thus unset the indicator on the main menu.
    AdvCombat *rCombat = AdvCombat::Fetch();

    //--Variables.
    int tJPTally = 0;
    int tPartySize = rCombat->GetActivePartyCount();

    //--Iterate.
    for(int i = 0; i < tPartySize; i ++)
    {
        //--Error check.
        AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--Sum JP.
        tJPTally = tJPTally + rCharacter->GetTotalJP();
    }

    //--Store the tally.
    SysVar *rTallyVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/CrossChapter/SkillsTracker/iJPTally");
    if(rTallyVar) rTallyVar->mNumeric = tJPTally;

    ///--[Skill Strings]
    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    //--"Scroll x10" string
    mScrollFastString->SetString("[IMG0] (Hold) Scroll x10");
    mScrollFastString->AllocateImages(1);
    mScrollFastString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Ctrl"));
    mScrollFastString->CrossreferenceImages();

    //--"Edit Profiles" string
    mEditProfilesString->SetString("[IMG0] Edit Profiles");
    mEditProfilesString->AllocateImages(1);
    mEditProfilesString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F4"));
    mEditProfilesString->CrossreferenceImages();

    //--"Show Skill Details" string
    mSkillDetailsString->SetString("[IMG0] Show Skill Details");
    mSkillDetailsString->AllocateImages(1);
    mSkillDetailsString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F2"));
    mSkillDetailsString->CrossreferenceImages();

    //--"Inspect Skills" string
    mInspectSkillsString->SetString("[IMG0] Inspect Skills");
    mInspectSkillsString->AllocateImages(1);
    mInspectSkillsString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mInspectSkillsString->CrossreferenceImages();

    //--"Toggle Tactics Grid" string
    mShowTacticsString->SetString("[IMG0] Toggle Tactics Grid");
    mShowTacticsString->AllocateImages(1);
    mShowTacticsString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Jump"));
    mShowTacticsString->CrossreferenceImages();

    //--"Previous Character" string
    mCharacterLftString->SetString("[IMG0] or [IMG1]+[IMG2] Previous Character");
    mCharacterLftString->AllocateImages(3);
    mCharacterLftString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("UpLevel"));
    mCharacterLftString->SetImageP(1, 3.0f, ControlManager::Fetch()->ResolveControlImage("Ctrl"));
    mCharacterLftString->SetImageP(2, 3.0f, ControlManager::Fetch()->ResolveControlImage("Left"));
    mCharacterLftString->CrossreferenceImages();

    //--"Next Character" string
    mCharacterRgtString->SetString("[IMG0] or [IMG1]+[IMG2] Next Character");
    mCharacterRgtString->AllocateImages(3);
    mCharacterRgtString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mCharacterRgtString->SetImageP(1, 3.0f, ControlManager::Fetch()->ResolveControlImage("Ctrl"));
    mCharacterRgtString->SetImageP(2, 3.0f, ControlManager::Fetch()->ResolveControlImage("Right"));
    mCharacterRgtString->CrossreferenceImages();

    //--Memorize
    mConfirmPurchaseString->SetString("[IMG0] Memorize Skill");
    mConfirmPurchaseString->AllocateImages(1);
    mConfirmPurchaseString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mConfirmPurchaseString->CrossreferenceImages();

    //--Confirm Delete
    mConfirmDeleteString->SetString("[IMG0] Delete Profile");
    mConfirmDeleteString->AllocateImages(1);
    mConfirmDeleteString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mConfirmDeleteString->CrossreferenceImages();

    //--Cancel
    mCancelPurchaseString->SetString("[IMG0] Cancel");
    mCancelPurchaseString->AllocateImages(1);
    mCancelPurchaseString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mCancelPurchaseString->CrossreferenceImages();

    ///--[Profile Strings]
    //--Instructions
    mInstructionString[0]->SetString("Select a profile with [IMG0][IMG1], set active profile with [IMG2].");
    mInstructionString[0]->AllocateImages(3);
    mInstructionString[0]->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Up"));
    mInstructionString[0]->SetImageP(1, 3.0f, ControlManager::Fetch()->ResolveControlImage("Down"));
    mInstructionString[0]->SetImageP(2, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mInstructionString[0]->CrossreferenceImages();

    mInstructionString[1]->SetString("Any skills equipped are set to the active profile. Jobs are associated with profiles.");
    mInstructionString[1]->AllocateImages(0);
    mInstructionString[1]->CrossreferenceImages();

    mInstructionString[2]->SetString("Switching jobs, even in combat, will change all abilities to use the associated profile.");
    mInstructionString[2]->AllocateImages(0);
    mInstructionString[2]->CrossreferenceImages();

    mInstructionString[3]->SetString(" ");
    mInstructionString[3]->AllocateImages(0);
    mInstructionString[3]->CrossreferenceImages();

    mInstructionString[4]->SetString("You may have up to 100 profiles. You may not delete or rename the Default profile.");
    mInstructionString[4]->AllocateImages(0);
    mInstructionString[4]->CrossreferenceImages();

    mInstructionString[5]->SetString("Modify which job uses which profile with [IMG0][IMG1] and [IMG2]");
    mInstructionString[5]->AllocateImages(3);
    mInstructionString[5]->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Left"));
    mInstructionString[5]->SetImageP(1, 3.0f, ControlManager::Fetch()->ResolveControlImage("Right"));
    mInstructionString[5]->SetImageP(2, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mInstructionString[5]->CrossreferenceImages();

    mInstructionString[6]->SetString("[IMG0] Returns to the Skills Menu.");
    mInstructionString[6]->AllocateImages(1);
    mInstructionString[6]->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mInstructionString[6]->CrossreferenceImages();

    //--"New Profile" string
    mNewProfileString->SetString("[IMG0] Create New Profile");
    mNewProfileString->AllocateImages(1);
    mNewProfileString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mNewProfileString->CrossreferenceImages();

    //--"Rename Profile" string
    mRenameProfileString->SetString("[IMG0] Rename Profile");
    mRenameProfileString->AllocateImages(1);
    mRenameProfileString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Jump"));
    mRenameProfileString->CrossreferenceImages();

    //--"Delete Profile" string
    mDeleteProfileString->SetString("[IMG0] Delete Profile");
    mDeleteProfileString->AllocateImages(1);
    mDeleteProfileString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("OpenFieldAbilityMenu"));
    mDeleteProfileString->CrossreferenceImages();

    ///--[Finalize Cursor]
    //--Set cursor to 0th position.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}

///======================================= Core Methods ===========================================
void AdvUISkills::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Set lines.
    mHelpMenuStrings[i]->SetString("The skills menu can be used to learn new skills and edit what skills your characters are");
    i ++;

    mHelpMenuStrings[i]->SetString("using. Change characters with [IMG0] and [IMG1].");
    mHelpMenuStrings[i]->AllocateImages(2);
    mHelpMenuStrings[i]->SetImageP(0, cxHelpControlOffsetY, rControlManager->ResolveControlImage("UpLevel"));
    mHelpMenuStrings[i]->SetImageP(1, cxHelpControlOffsetY, rControlManager->ResolveControlImage("DnLevel"));
    i ++;

    mHelpMenuStrings[i]->SetString("");
    mHelpMenuStrings[i]->AllocateImages(0);
    i ++;

    mHelpMenuStrings[i]->SetString("First, select a job. Each job has a certain amount of JP. You gain JP by winning battles");
    i ++;
    mHelpMenuStrings[i]->SetString("as that job. You can change jobs at a campfire.");
    i ++;
    mHelpMenuStrings[i]->SetString("You can then memorize skills for that job by spending that JP. Once memorized, you");
    i ++;
    mHelpMenuStrings[i]->SetString("may equip that skill to your 'Memorized' block. Even if you change jobs, you keep all");
    i ++;
    mHelpMenuStrings[i]->SetString("skills in the memorized block.");
    i ++;

    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("If you learn every skill in a job, then that job is 'mastered' and all JP for that job");
    i ++;
    mHelpMenuStrings[i]->SetString("will be placed in the 'global' JP pool. This JP can be spent on skills if your job");
    i ++;
    mHelpMenuStrings[i]->SetString("does not have enough.");
    i ++;

    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("Jobs come with their own preset of skills. These are marked as 'CD'. Skills in your");
    i ++;
    mHelpMenuStrings[i]->SetString("Memory block are marked with 'E'. You can press [IMG0] to inspect your skills.");
    mHelpMenuStrings[i]->AllocateImages(1);
    mHelpMenuStrings[i]->SetImageP(0, cxHelpControlOffsetY, rControlManager->ResolveControlImage("Run"));
    i ++;

    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("Press [IMG0] to bring up the 'Profiles' menu. Each job can have a profile associated");
    mHelpMenuStrings[i]->AllocateImages(1);
    mHelpMenuStrings[i]->SetImageP(0, cxHelpControlOffsetY, rControlManager->ResolveControlImage("F4"));
    i ++;
    mHelpMenuStrings[i]->SetString("with it, and changing to that job will activate that profile. You can also just store");
    i ++;
    mHelpMenuStrings[i]->SetString("sets of skills to quickly change them with profiles.");
    i ++;

    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("Press [IMG0] or [IMG1] to exit this help menu.");
    mHelpMenuStrings[i]->AllocateImages(2);
    mHelpMenuStrings[i]->SetImageP(0, cxHelpControlOffsetY, rControlManager->ResolveControlImage("F1"));
    mHelpMenuStrings[i]->SetImageP(1, cxHelpControlOffsetY, rControlManager->ResolveControlImage("Cancel"));
    i ++;

    //--Error check.
    if(i > ADVMENU_SKILLS_HELP_MENU_STRINGS)
    {
        fprintf(stderr, "Warning: Skill Help Menu has too many strings %i vs %i\n", i, ADVMENU_SKILLS_HELP_MENU_STRINGS);
    }

    //--Order all strings to crossreference images.
    for(int i = 0; i < ADVMENU_SKILLS_HELP_MENU_STRINGS; i ++)
    {
        mHelpMenuStrings[i]->CrossreferenceImages();
    }
}
void AdvUISkills::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Depending on which mode the skills menu is in, figures out where the highlight should be
    //  and moves it there.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = 0.0f;
    float cBot = 0.0f;

    //--Resolve active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    ///--[Job Select]
    if(mMode == ADVMENU_SKILLS_MODE_JOB_SELECT)
    {
        //--Constants.
        float cTextX = 509.0f;
        float cTextY = 161.0f;
        float cTextW =   1.0f;
        float cTextH =  22.0f;

        //--Which font to use.
        StarFont *rUseFont = AdvImages.rFont_Mainline;
        if(mJobCursor == 0) rUseFont = AdvImages.rFont_Heading;

        //--Get the name of the selected job.
        AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
        if(!rHighlightedJob) return;

        //--Calculate width.
        cTextW = rUseFont->GetTextWidth(rHighlightedJob->GetDisplayName());
        if(cTextW >= ADVMENU_SKILLS_JOBNAME_MAXLEN) cTextW = ADVMENU_SKILLS_JOBNAME_MAXLEN;

        //--Set.
        cLft = cTextX;
        cRgt = cTextX + cTextW;

        //--Calculate Y position. Zero slot uses a different font.
        if(mJobCursor == 0)
        {
            cTop = 117.0f;
            cBot = 148.0f;
        }
        //--All other slots.
        else
        {
            cTop = cTextY + (cTextH * (mJobCursor - 1));
            cBot = cTop + cTextH;
        }

        //--Adjustments. Large font.
        if(mJobCursor == 0)
        {
            cLft = cLft - 7.0f;
            cTop = cTop - 3.0f;
            cRgt = cRgt + 0.0f;
            cBot = cBot + 3.0f;
        }
        //--Adjustments, small font.
        else
        {
            cLft = cLft - 5.0f;
            cTop = cTop - 3.0f;
            cRgt = cRgt + 0.0f;
            cBot = cBot - 1.0f;
        }
    }
    ///--[Skill Select]
    else if(mMode == ADVMENU_SKILLS_MODE_SKILL_SELECT)
    {
        //--Constants.
        float cTextX = 891.0f;
        float cTextY = 163.0f;
        float cTextH =  26.0f;

        //--Get the job.
        AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
        if(!rHighlightedJob) return;

        //--Get the ability list for that job.
        StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();
        if(!rAbilityList) return;

        //--Get the highlighted ability.
        AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillCursor);
        if(!rAbility) return;

        //--Compute X positions.
        cLft = cTextX;
        cRgt = cTextX + AdvImages.rFont_Mainline->GetTextWidth(rAbility->GetDisplayName());

        //--Compute Y psotions.
        cTop = cTextY + (cTextH * (mSkillCursor - mSkillSkip));
        cBot = cTop + cTextH;

        //--Adjustments.
        cLft = cLft - 3.0f;
        cTop = cTop - 3.0f;
        cRgt = cRgt + 2.0f;
        cBot = cBot - 5.0f;
    }
    ///--[Skill Query]
    else if(mMode == ADVMENU_SKILLS_MODE_SKILL_QUERY || mMode == ADVMENU_SKILLS_MODE_SKILL_REPLACE)
    {
        //--Constants.
        float cSkillX =  66.0f;
        float cSkillY = 520.0f;
        float cSkillW =  53.0f;
        float cSkillH =  53.0f;

        //--Compute.
        cLft = cSkillX + (cSkillW * mAbilityCursorX);
        cTop = cSkillY + (cSkillH * mAbilityCursorY);

        //--These offsets only occur when not showing the tactics window.
        if(!mIsUsingTacticsGrid)
        {
            //--Manual offsetting: Class grid, bottom six.
            if(mAbilityCursorX < 3 && mAbilityCursorY > 0)
            {
                cTop = cTop + 8.0f;
            }

            //--Offset, Memorized Skills.
            if(mAbilityCursorX >= 3)
            {
                cLft = cLft + 24.0f;
            }

            //--Memorized skills, bottom section.
            if(mAbilityCursorX >= 3 && mAbilityCursorY > 1)
            {
                cTop = cTop + 8.0f;
            }
        }
        else
        {
            cLft = cLft - 15.0f;
        }

        //--Finalize.
        cRgt = cLft + 50.0f;
        cBot = cTop + 50.0f;

        //--Adjust.
        cLft = cLft - 3.0f;
        cTop = cTop - 3.0f;
        cRgt = cRgt + 3.0f;
        cBot = cBot + 3.0f;
    }

    //--Set.
    mHighlightPos.MoveTo(cLft, cTop, cxAdvCurTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxAdvCurTicks);
}
void AdvUISkills::SetAbilityCursorToFirstEmptySlot()
{
    ///--[Documentation]
    //--When placing skills, positions the cursor on the first empty skill slot, if there is one.
    //  If not, places it in the upper left slot.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Iterate.
    for(int y = 0; y < 3; y ++)
    {
        for(int x = 3; x < 8; x ++)
        {
            //--Slot is occupied.
            if(rCurrentCharacter->GetAbilityBySlot(x, y)) continue;

            //--Slot is not occupied, set and finish.
            mAbilityCursorX = x;
            mAbilityCursorY = y;
            return;
        }
    }

    //--No empty slots.
    mAbilityCursorX = 3;
    mAbilityCursorY = 0;
}

///=================================== Private Core Methods =======================================
///========================================= File I/O =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
