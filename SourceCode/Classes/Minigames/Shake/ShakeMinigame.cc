//--Base
#include "ShakeMinigame.h"

//--Classes
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "ControlManager.h"
#include "EntityManager.h"

///========================================== System ==============================================
ShakeMinigame::ShakeMinigame()
{
    ///--[Variable Initialization]
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_SHAKEGAME;

    //--[ShakeMinigame]
    //--System
    mIsPendingRemoval = false;
    rGrabbingActor = NULL;

    //--Keypresses
    mPlayerKeypresses = 0;
    mPlayerKeypressesNeeded = 10;

    //--Animation
    mPlayerAnimTimer = 0;
    mPlayerAnimDenom = 15;
}
ShakeMinigame::~ShakeMinigame()
{
}

///===================================== Property Queries =========================================
bool ShakeMinigame::IsComplete()
{
    return mIsPendingRemoval;
}

///======================================= Manipulators ===========================================
void ShakeMinigame::SetGrabbingActorS(const char *pName)
{
    //--Locates the actor via the Entity Manager.
    rGrabbingActor = NULL;
    RootEntity *rEntity = EntityManager::Fetch()->GetEntity(pName);
    if(!rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return;

    //--Set.
    rGrabbingActor = (TilemapActor *)rEntity;
}
void ShakeMinigame::SetGrabbingActorP(TilemapActor *pActor)
{
    rGrabbingActor = pActor;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void ShakeMinigame::Update()
{
    ///--[Documentation]
    //--The player needs to press buttons a certain number of times to break free of the grab. This
    //  update tracks that. Only certain button presses work.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timer]
    mPlayerAnimTimer ++;
    if(mPlayerAnimTimer >= mPlayerAnimDenom * 3) mPlayerAnimTimer = 0;

    ///--[Player Handling]
    TilemapActor *rPlayer = AdventureLevel::Fetch()->LocatePlayerActor();
    if(rPlayer)
    {
        //--Resolve special frame name.
        char tBuf[80];
        sprintf(tBuf, "Shake%i", mPlayerAnimTimer / mPlayerAnimDenom);

        //--Activate it.
        rPlayer->ActivateSpecialFrame(tBuf);
    }

    ///--[Keypresses]
    //--Any of these keypresses counts. Multiple keys can be pressed at once.
    if(rControlManager->IsFirstPress("Activate"))
    {
        mPlayerKeypresses ++;
    }
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mPlayerKeypresses ++;
    }
    if(rControlManager->IsFirstPress("Up"))
    {
        mPlayerKeypresses ++;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        mPlayerKeypresses ++;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        mPlayerKeypresses ++;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        mPlayerKeypresses ++;
    }

    ///--[Ending]
    //--If the keypresses exceed the required number, the object marks itself for deletion. On the tick
    //  that it finishes, it executes this code to do things like stun nearby enemies.
    if(mPlayerKeypresses >= mPlayerKeypressesNeeded && !mIsPendingRemoval)
    {
        //--Flag.
        mIsPendingRemoval = true;

        //--Stun the grabbing enemy.
        if(rGrabbingActor)
        {
            rGrabbingActor->BeginStun();
            rGrabbingActor->SetDisable(false);
        }

        //--Clear player frame.
        if(rPlayer) rPlayer->ActivateSpecialFrame("NULL");
    }
}
void ShakeMinigame::HandlePlayerControls()
{

}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void ShakeMinigame::Render()
{
    ///--[Documentation]
    //--Doesn't render anything of interest yet.
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
ShakeMinigame *ShakeMinigame::Fetch()
{
    //--Note: Is reliant on an AdventureLevel to have ownership.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return NULL;

    //--Get from within the level.
    ShakeMinigame *rMinigame = rActiveLevel->GetShakeMinigame();
    return rMinigame;
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
