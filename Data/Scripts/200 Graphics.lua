-- |[ ======================================== Graphics ======================================== ]|
--Handles graphics related to the main menu and other must-haves.
Debug_PushPrint(false, "[Loading] System Graphics\n")

-- |[Reader Function]|
local fnReadImages = function(sPrefix, iCount, sDLPath)
	for i = 1, iCount, 1 do
		DL_ExtractBitmap(sPrefix .. (i-1), sDLPath .. sPrefix .. (i-1))
	end
end

-- |[ ============================ Splash Screen =========================== ]|
--The splash screen nominally only appears the first time the player boots up the game. A static
-- internal flag is toggled off when this happens.
local bShowSplash = FlexMenu_GetProperty("Show Splash")
if(bShowSplash == true) then
    
    --Open SLF File.
    SLF_Open("Data/Loading.slf")

    --Load.
    DL_AddPath("Root/Images/GUI/SplashScreen/")
    for i = 0, 23, 1 do
        local sIntName = string.format("Splash|Logo%02i", i)
        local sDLPName = string.format("Root/Images/GUI/SplashScreen/Logo%02i", i)
        DL_ExtractBitmap(sIntName, sDLPName)
    end
    io.write("=== Loaded splash screen data. ===\n")
end

-- |[ ========================== Engine Selector =========================== ]|
--Shows a collection of windows that allow the player to set engine options or
-- select which game they want to play, if there are many available.
SLF_Open("Data/UISystem.slf")

--Font, uses different filters.
DL_AddPath("Root/Images/GUI/System/")
ALB_SetTextureProperty("MagFilter", DM_GetEnumeration("GL_NEAREST"))
ALB_SetTextureProperty("MinFilter", DM_GetEnumeration("GL_LINEAR"))
DL_ExtractBitmap("UIFontSmall",  "Root/Images/GUI/System/UIFontSmall")
ALB_SetTextureProperty("MagFilter", DM_GetEnumeration("GL_NEAREST"))
ALB_SetTextureProperty("MinFilter", DM_GetEnumeration("GL_LINEAR"))
DL_ExtractBitmap("UIFontVSmall", "Root/Images/GUI/System/UIFontVSmall")
ALB_SetTextureProperty("Restore Defaults")

--Background and Title
DL_AddPath("Root/Images/GUI/Background/")
DL_ExtractBitmap("MenuBG",     "Root/Images/GUI/Background/MenuBG")
DL_ExtractBitmap("MenuBGOver", "Root/Images/GUI/Background/MenuBGOver")

--Text and Borders
DL_AddPath("Root/Images/GUI/System/")
DL_ExtractBitmap("CardBorder", "Root/Images/GUI/System/CardBorder")
DL_ExtractBitmap("UpArrow",    "Root/Images/GUI/System/UpArrow")
DL_ExtractBitmap("DownArrow",  "Root/Images/GUI/System/DnArrow")

--String Entry
DL_AddPath("Root/Images/AdventureUI/String/")
DL_ExtractBitmap("AdvString|Base",    "Root/Images/AdventureUI/String/Base")
DL_ExtractBitmap("AdvString|Pressed", "Root/Images/AdventureUI/String/Pressed")

-- |[ ==================== Adventure Mode Loading Icons ==================== ]|
--These icons are used on the UI which allows the player to select a file. If Adventure Mode is not found then
-- none of the bitmaps will resolve.
if(DL_Exists("Root/Paths/System/Startup/sAdventurePath") == true) then
    
    --Character sprites for Adventure Mode.
    local sAdventurePath = VM_GetVar("Root/Paths/System/Startup/sAdventurePath", "S")
    SLF_Open(sAdventurePath .. "Datafiles/Sprites.slf")
    fnExecAutoload("AutoLoad|Adventure|Sprites_Title")
    
    --Chapter Completion sprites.
    SLF_Open(sAdventurePath .. "Datafiles/UIAdventure.slf")
    fnExecAutoload("AutoLoad|Adventure|ChapterCompletion")

end

-- |[Mods]|
--All mods run a special lua file to handle loading any graphics they need for the title screen.
for i = 1, #gsModDirectories, 1 do
    if(FS_Exists(gsModDirectories[i] .. "System/001 Load Sprites For Title.lua") == true) then
        LM_ExecuteScript(gsModDirectories[i] .. "System/001 Load Sprites For Title.lua")
    end
end

-- |[Clean]|
--If a mod accidentally left any flags modified, reset them here.
ALB_SetTextureProperty("Restore Defaults")

-- |[ ================================ Clean =============================== ]|
Debug_PopPrint("[Loading] System Graphics Done\n")
