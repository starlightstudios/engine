///=================================== Chapter1LoadInterrupt ======================================
//--Version of the StarLoadInterrupt built for chapter 1.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLoadInterrupt.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define LI_CH1_LOADFRAMES 11

///========================================== Classes =============================================
class Chapter1LoadInterrupt : public StarLoadInterrupt
{
    private:
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    bool mIsReady;
    StarBitmap *mBacking;
    StarBitmap *mMeiFrames;
    StarBitmap *mLoadingFrames[LI_CH1_LOADFRAMES];

    protected:

    public:
    //--System
    Chapter1LoadInterrupt();
    virtual ~Chapter1LoadInterrupt();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators

    //--Core Methods
    virtual void DeallocateImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Call(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_BootChapter1(lua_State *L);
