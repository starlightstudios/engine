-- |[ ===================================== Character Job ====================================== ]|
--A job taken by a character, mostly using a lua class object to handle processing.
local iArgsTotal = LM_GetNumOfArgs()
if(iArgsTotal < 1) then return end

--Argument resolve.
local ciSwitchCode = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Construction ====================================== ]|
if(ciSwitchCode == ObjectPrototype.ciScriptCreate) then
    
    -- |[Setup]|
    --Create.
    SOB_CreateEntry("Character Job")
    
    --System.
    SOP_SetProperty("Execution Path", LM_GetCallStack(0))
    
    --Description.
    SOP_SetProperty("Set Description Auto", 
        "A job (or form, for runebearers). Jobs make use of a prototype and standardized job script. Typically found in[BR]"..
        "gsRoot/Combat/[Character]/Jobs/[Jobname]/000 Job Script.lua")

    -- |[Fields]|
    SOP_SetProperty("Add String Field",      "Character Name", "Mei")
    SOP_SetProperty("Set Field Description", "Character Name", "Name of the character in question. Eg: Mei, Christine.")

    SOP_SetProperty("Add String Field",      "Job Name", "Fencer")
    SOP_SetProperty("Set Field Description", "Job Name", "Name of the job in question. Eg: Fencer, Lancer.")

    SOP_SetProperty("Add String Field",      "Form Name", "Null")
    SOP_SetProperty("Set Field Description", "Form Name", "Form of the job in question. If Null, will use the character's name.")
    
    SOP_SetProperty("Add Boolean Field",     "Is Runebearer", 0)
    SOP_SetProperty("Set Field Description", "Is Runebearer", "Runebearers use a slightly different variable format than non-runebearers.[BR]Set to 1 for runebearer, all else are 0.")
    
    SOP_SetProperty("Add Integer Field",     "Face Index X", 0)
    SOP_SetProperty("Set Field Description", "Face Index X", "X position on the face sheet. This face appears next to inventory items this character has equipped.")
    
    SOP_SetProperty("Add Integer Field",     "Face Index Y", 0)
    SOP_SetProperty("Set Field Description", "Face Index Y", "Y position on the face sheet. This face appears next to inventory items this character has equipped.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 0", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 0", "Name of the first buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 1", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 1", "Name of the second buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 2", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 2", "Name of the third buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 3", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 3", "Name of the fourth buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 4", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 4", "Name of the fifth buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 5", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 5", "Name of the sixth buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 6", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 6", "Name of the seventh buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 7", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 7", "Name of the eighth buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 8", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 8", "Name of the ninth buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    SOP_SetProperty("Add String Field",      "Buyable Skill 9", "Null")
    SOP_SetProperty("Set Field Description", "Buyable Skill 9", "Name of the tenth buyable skill. Skills are automatically put on the skill grid when exporting.[BR] Leave as Null to not generate the skill listing.")

    -- |[Finish Up]|
    --Finish Up.
    DL_PopActiveObject()

-- |[ ======================================= Execution ======================================== ]|
elseif(ciSwitchCode == ObjectPrototype.ciScriptExecute) then

    -- |[ =================== Setup ==================== ]|
    -- |[Diagnostics]|
    local iTotalFields = SOP_GetProperty("Total Fields")

    -- |[Fields]|
    local sCharName     = SOP_GetProperty("Get String Field",  "Character Name")
    local sJobName      = SOP_GetProperty("Get String Field",  "Job Name")
    local sFormName     = SOP_GetProperty("Get String Field",  "Form Name")
    local iIsRunebearer = SOP_GetProperty("Get Integer Field", "Is Runebearer")
    local iFaceIndexX   = SOP_GetProperty("Get Integer Field", "Face Index X")
    local iFaceIndexY   = SOP_GetProperty("Get Integer Field", "Face Index Y")
    
    --Skills array.
    local saSkills = {}
    for i = 0, 9, 1 do
        saSkills[i] = SOP_GetProperty("Get String Field",  "Buyable Skill " .. i)
    end
    
    --If the form is "Null", use the job's name.
    if(sFormName == "Null") then sFormName = sJobName end

    -- |[Open File]|
    --Generate a filename.
    local sFilename = ObjectPrototype:fnGeneratePathname("Character Job ")
    if(sFilename == "Null") then
        io.write("Warning. Attempted to generate a unique filename but failed.\nPrototype: " .. fnResolvePath() .. "\n")
        return
    end
    
    --Open/create it.
    local fOutfile = io.open(sFilename, "w")

    -- |[ ================== Writing =================== ]|
    -- |[Header]|
    --Generate a header and a comment.
    fOutfile:write(ObjectPrototype:fnCreateBalancedHeader(sJobName, 100) .. "\n")
    fOutfile:write([[--Class for ]] .. sCharName .. "\n")
    fOutfile:write([[--AUTOGENERATED TO-DO]] .. "\n")
    fOutfile:write([[--*Add this job to the character's job chart for stats]] .. "\n")
    fOutfile:write([[--*Add this job to the character's job call list]] .. "\n")
    fOutfile:write([[--*Make sure the second ability in the ability grid is correct. Everyone has their own, this uses "Parry"]] .. "\n")
    fOutfile:write([[--*Setup the ability listing in the correct order]] .. "\n")
    fOutfile:write([[--*Make sure the form/class distinction is handled correctly, particularly for sprite names]] .. "\n")
    fOutfile:write([[--*Realign the spacing on the ability grid]] .. "\n")
    fOutfile:write([[--*Remove this TO-DO when done!]] .. "\n")
    fOutfile:write([[]] .. "\n")
    
    -- |[Argument Handling Block]|
    --Standard block, no generation is needed.
    fOutfile:write([[-- |[ ========== Argument Handling =========== ]|]] .. "\n")
    fOutfile:write([[--Argument handling.]] .. "\n")
    fOutfile:write([[local iArgumentsTotal = LM_GetNumOfArgs()]] .. "\n")
    fOutfile:write([[if(iArgumentsTotal < 1) then return end]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--Zeroth argument determines what this script does.]] .. "\n")
    fOutfile:write([[local iSwitchType = LM_GetScriptArgument(0, "N")]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--1st argument is only used for level-up calls.]] .. "\n")
    fOutfile:write([[local iLevelReached = 0]] .. "\n")
    fOutfile:write([[if(iArgumentsTotal >= 2) then]] .. "\n")
    fOutfile:write([[    iLevelReached = LM_GetScriptArgument(1, "N")]] .. "\n")
    fOutfile:write([[end]] .. "\n")
    fOutfile:write([[]] .. "\n")

    -- |[Variable Generation Block]|
    --This is where the magic happens.
    fOutfile:write([[-- |[ ============ Variable Setup ============ ]|]] .. "\n")
    fOutfile:write([[-- |[Enumerations]|]] .. "\n")
    fOutfile:write([[local gciNoSkillbooks = 0]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[-- |[Encapsulation]|]] .. "\n")
    fOutfile:write([[local zEntry = {}]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[-- |[Basic Variables]|]] .. "\n")
    fOutfile:write([[--Character Variables.]] .. "\n")
    fOutfile:write([[zEntry.sCharacterFormVarPath = "Root/Variables/Global/]] .. sCharName .. [[/sForm"]] .. "\n")
    fOutfile:write([[zEntry.sSkillbookVarPath     = "Root/Variables/Global/]] .. sCharName .. [[/iSkillbookTotal"]] .. "\n")
    fOutfile:write([[zEntry.zaJobChart            = gza]] .. sCharName .. [[JobChart]] .. "\n")
    fOutfile:write([[zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/]] .. sCharName .. [[/400 Common Stat Profile.lua"]] .. "\n")
    
    --Runebearers have a secondary menu:
    if(iIsRunebearer == 1.0) then
        fOutfile:write([[zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/]] .. sCharName .. [[/200 Build Secondary Menu.lua"]] .. "\n")

    --Non-runebearers do not.
    else
        fOutfile:write([[zEntry.sSecondaryMenuPath    = "Null"]] .. "\n")
    end
    fOutfile:write([[zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/]] .. sCharName .. [[/401 Mastery Bonus Handler.lua"]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--Class Variables.]] .. "\n")
    fOutfile:write([[zEntry.sClassName   = "]] .. sJobName .. [["]] .. "\n")
    fOutfile:write([[zEntry.sFormName    = "]] .. sFormName .. [["]] .. "\n")
    fOutfile:write([[zEntry.sCostumeName = "]] .. sCharName .. [[_]] .. sFormName .. [["]] .. "\n")
    fOutfile:write([[zEntry.fFaceIndexX  = ]] .. iFaceIndexX .. [[]] .. "\n")
    fOutfile:write([[zEntry.fFaceIndexY  = ]] .. iFaceIndexY .. [[]] .. "\n")
    fOutfile:write([[]] .. "\n")

    -- |[Ability Generation]|
    --Ability variables inevitably have to be put in by hand, though the prototype allows some to be placed automatically.
    -- If none are specified, the lists still generate but have no entries.
    fOutfile:write([[-- |[Ability Variables]|]] .. "\n")
    fOutfile:write([[--Purchaseable abilities.]] .. "\n")
    fOutfile:write([[zEntry.saExternalAbilities = {}]] .. "\n")
    for i = 0, 9, 1 do
        if(saSkills[i] == "Null") then break end
        fOutfile:write([[table.insert(zEntry.saExternalAbilities, "]] .. saSkills[i] .. [[")]] .. "\n")
    end
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--Class-equipped abilities.]] .. "\n")
    fOutfile:write([[zEntry.saInternalAbilities = {}]] .. "\n")
    for i = 0, 9, 1 do
        if(saSkills[i] == "Null") then break end
        fOutfile:write([[table.insert(zEntry.saInternalAbilities, "]] .. saSkills[i] .. [[")]] .. "\n")
    end
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--Purchaseable abilities, in the order of purchase.]] .. "\n")
    fOutfile:write([[zEntry.saPurchaseAbilities = {}]] .. "\n")
    for i = 0, 9, 1 do
        if(saSkills[i] == "Null") then break end
        fOutfile:write([[table.insert(zEntry.saPurchaseAbilities, "]] .. saSkills[i] .. [[")]] .. "\n")
    end
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[-- |[Skillbar Variables]|]] .. "\n")
    fOutfile:write([[--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})]] .. "\n")
    fOutfile:write([[zEntry.zaSkillbarAbilities = {}]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack", 0, 0, gciNoSkillbooks})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",  1, 0, gciNoSkillbooks})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"]] .. sJobName .. [[ Internal|]] .. saSkills[0] .. [[", 2, 0, gciNoSkillbooks})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"]] .. sJobName .. [[ Internal|]] .. saSkills[1] .. [[", 0, 1, gciNoSkillbooks})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"]] .. sJobName .. [[ Internal|]] .. saSkills[2] .. [[", 1, 1, gciNoSkillbooks})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"]] .. sJobName .. [[ Internal|]] .. saSkills[3] .. [[", 2, 1, 1})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"]] .. sJobName .. [[ Internal|]] .. saSkills[4] .. [[", 0, 2, 2})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"]] .. sJobName .. [[ Internal|]] .. saSkills[5] .. [[", 1, 2, 3})]] .. "\n")
    fOutfile:write([[table.insert(zEntry.zaSkillbarAbilities, {"]] .. sJobName .. [[ Internal|]] .. saSkills[6] .. [[", 2, 2, 4})]] .. "\n")
    fOutfile:write([[]] .. "\n")

    -- |[Standard Job Call Block]|
    --Standard block, no generation is needed.
    fOutfile:write([[-- |[ ============ Call Standard ============= ]|]] .. "\n")
    fOutfile:write([[--Add to the global stack. There may be multiple executions of this script.]] .. "\n")
    fOutfile:write([[table.insert(gzJobCallStack, zEntry)]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--Call.]] .. "\n")
    fOutfile:write([[LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--Pop the global stack.]] .. "\n")
    fOutfile:write([[table.remove(gzJobCallStack)]] .. "\n")


    -- |[ ================= Finish Up ================== ]|
    --Close the file.
    fOutfile:close()
    
    --Report success to the console.
    io.write("Successfully wrote file: " .. sFilename .. "\n")
end