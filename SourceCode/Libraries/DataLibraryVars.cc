//--Base
#include "DataLibrary.h"

//--Classes
#include "PairanormalLevel.h"
#include "RootEvent.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//--Variable Manager Handling. These are a series of Lua functions that are specially designed to
//  operate on script variables. The variables are of type SysVar, defined in DataLibrary.h.
//  By default, they are all flagged to be saved by the SaveManager, though you can switch this off.

//--The SaveManager will make a copy of the DataLibrary's Root/Variables/ section and reinit it on game
//  load. Any other variables stored on any other list will be ignored!

//--Note: VM_SetVar will create a var if it doesn't exist. If the existence of a variable matters,
//  you can check if it exists with VM_Exists.

///--[Debug]
//#define DLVARS_DEBUG
#ifdef DLVARS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//#define TRACK_STRING_POOL_USES

///--[Worker Functions]
void AddStringToPool(StarLinkedList *pStringPool, const char *pString)
{
    //--Given a string, adds that string to the provided linked list. If that string already exists on the list,
    //  instead increments an attached integer value.
    //--The integer value is not added if TRACK_STRING_POOL_USES is not defined.
    int *rStringPoolVar = (int *)pStringPool->GetElementByName(pString);
    if(!rStringPoolVar)
    {
        //--Tracking the integer, create and set to one, since it exists once on the list.
        #if defined TRACK_STRING_POOL_USES
            int *nValue = (int *)malloc(sizeof(int));
            *nValue = 1;
            pStringPool->AddElement(pString, nValue, &FreeThis);

        //--Use a dummy value to save RAM.
        #else
            int tDummyValue = 0;
            pStringPool->AddElement(pString, &tDummyValue);
        #endif
    }
    //--Element is already on the list.
    else
    {
        //--When tracking, increment the associated integer. If not tracking, do nothing.
        #if defined TRACK_STRING_POOL_USES
            *rStringPoolVar = (*rStringPoolVar) + 1;
        #endif
    }
}

///=========================================== Saving =============================================
void DataLibrary::WriteToBuffer(StarAutoBuffer *pBuffer, bool pUsePathCompression)
{
    ///--[ ================ Documentation and Setup =============== ]
    //--Writes all VM variables to the given buffer. VM variables are valid if they are in Root/Variables/
    //  and have their saving flag set.
    if(!pBuffer) return;
    DebugPush(true, "Writing SCRIPTVARS_ block to data buffer %p.\n", pBuffer);

    ///--[Check Size]
    //--Get the variable section. If it doesn't exist, write 0 to indicate there were no script variables.
    StarLinkedList *rVariableSection = GetSection("Root/Variables/");
    StarLinkedList *rSavingSection   = GetSection("Root/Saving/");
    if(!rVariableSection && !rSavingSection)
    {
        if(pUsePathCompression) pBuffer->AppendInt32(0);
        pBuffer->AppendInt32(0);
        DebugPop("Error, no variables/saving section.\n");
        return;
    }

    ///--[ =============== Assemble Variables to Master List ================ ]
    ///--[Setup]
    //--Create a linked list that will store reference copies to all variables.
    char tBuffer[512];
    StarLinkedList *tAllVarsList = new StarLinkedList(false);
    DebugPrint("Created master list.\n");

    ///--[Variables Section]
    //--Now build a listing of all the script variables.
    if(rVariableSection)
    {
        StarLinkedList *rCatalogueList = (StarLinkedList *)rVariableSection->PushIterator();
        while(rCatalogueList)
        {
            //--Check the catalogue's name. If it's "Combat", skip adding it.
            const char *rCatalogueName = rVariableSection->GetIteratorName();
            if(!strcasecmp(rCatalogueName, "Combat"))
            {
                rCatalogueList = (StarLinkedList *)rVariableSection->AutoIterate();
                continue;
            }

            //--Iterate across the headings.
            StarLinkedList *rHeadingList = (StarLinkedList *)rCatalogueList->PushIterator();
            while(rHeadingList)
            {
                //--Iterate across the variables in this heading. Store them in the tAllVarsList.
                void *rVariable = rHeadingList->PushIterator();
                while(rVariable)
                {
                    //--The name is the path. The value is the pointer.
                    sprintf(tBuffer, "Root/%s/%s/%s/%s", "Variables", rVariableSection->GetIteratorName(), rCatalogueList->GetIteratorName(), rHeadingList->GetIteratorName());
                    tAllVarsList->AddElement(tBuffer, rVariable);

                    //--Next variable.
                    rVariable = rHeadingList->AutoIterate();
                }

                //--Next heading.
                rHeadingList = (StarLinkedList *)rCatalogueList->AutoIterate();
            }

            //--Next catalogue.
            rCatalogueList = (StarLinkedList *)rVariableSection->AutoIterate();
        }
    }
    #if defined DLVARS_DEBUG
    int tAllVarsTotal = tAllVarsList->GetListSize();
    DebugPrint("Iterated across variables listing. %i variables found.\n", tAllVarsTotal);
    #endif

    ///--[Saving Section]
    //--Iterate across the saving section and write it as well.
    if(rSavingSection)
    {
        StarLinkedList *rCatalogueList = (StarLinkedList *)rSavingSection->PushIterator();
        while(rCatalogueList)
        {
            //--Iterate across the headings.
            StarLinkedList *rHeadingList = (StarLinkedList *)rCatalogueList->PushIterator();
            while(rHeadingList)
            {
                //--Iterate across the variables in this heading. Store them in the tAllVarsList.
                void *rVariable = rHeadingList->PushIterator();
                while(rVariable)
                {
                    //--The name of the entry is its path.
                    sprintf(tBuffer, "Root/%s/%s/%s/%s", "Saving", rSavingSection->GetIteratorName(), rCatalogueList->GetIteratorName(), rHeadingList->GetIteratorName());
                    tAllVarsList->AddElement(tBuffer, rVariable);

                    //--Next variable.
                    rVariable = rHeadingList->AutoIterate();
                }

                //--Next heading.
                rHeadingList = (StarLinkedList *)rCatalogueList->AutoIterate();
            }

            //--Next catalogue.
            rCatalogueList = (StarLinkedList *)rSavingSection->AutoIterate();
        }
    }
    #if defined DLVARS_DEBUG
    DebugPrint("Iterated across saving listing. %i variables found.\n", tAllVarsList->GetListSize() - tAllVarsTotal);
    #endif

    ///--[ ====================== Assemble String Pool ====================== ]
    ///--[String Pool Builder]
    //--If this flag is true, then this savefile has path compression. Rather than printing Root/Variables/X/Y/VariableName, the savefile
    //  stores a table containing "Root/Variables/X/Y" and simply refers to the table entry, which greatly reduces the number of times
    //  a string gets written and makes the file smaller.
    //--This feature is expected in STARv201 and later. In STARv200, skip this.

    ///--[Write To File]
    //--Print how many elements are in the lookup. This is a 32-bit integer.
    if(pUsePathCompression)
    {
        //--Write expected number.
        uint32_t tElements = (uint32_t)mPathPool->GetListSize();
        pBuffer->AppendUInt32(tElements);
        //fprintf(stderr, "String pool elements: %i\n", tElements);

        //--Each element is a string, with the length printed ahead of it.
        void *rCheckVar = mPathPool->PushIterator();
        while(rCheckVar)
        {
            //--Get the element. Append it to the file.
            const char *rName = mPathPool->GetIteratorName();
            pBuffer->AppendStringWithLen(rName);

            //--Next.
            rCheckVar = mPathPool->AutoIterate();
        }
    }

    /*
    //--Create a linked-list that has a unique copy of all the paths.
    #if defined TRACK_STRING_POOL_USES
        StarLinkedList *tPathList = new StarLinkedList(true);
    #else
        StarLinkedList *tPathList = new StarLinkedList(false);
    #endif

    //--When using path compression:
    if(pUsePathCompression)
    {
        ///--[Setup]
        //--Variables.
        ResolvePack tTempResolvePack;

        //--Initialize.
        memset(&tTempResolvePack, 0, sizeof(ResolvePack));

        ///--[Scan All Stored Variables]
        //--Iterate across all the stored variables.
        void *rVariable = tAllVarsList->PushIterator();
        while(rVariable)
        {
            ///--[Get Path and Resolve Pack]
            //--Get the variable's path.
            const char *rPath = tAllVarsList->GetIteratorName();

            //--Break it into a ResolvePack, which splits up the Section/Catalogue/Heading/Variable into strings.
            //  We don't care about pointers, just the names.
            GetNames(rPath, tTempResolvePack);

            ///--[Add to Master List]
            //--Now check each of the resolve packs to see if any are on the paths list. If not, add them.
            //  Note that we add a reference copy of the string from tAllVarsList, meaning the list tPathList
            //  therefore has the same scope as tAllVarsList.
            //--For debug purposes, TRACK_STRING_POOL_USES can be defined in which case the program will use
            //  a stored integer to track how many times the string was used when writing the variables.
            AddStringToPool(tPathList, tTempResolvePack.mSection);
            AddStringToPool(tPathList, tTempResolvePack.mCatalogue);
            AddStringToPool(tPathList, tTempResolvePack.mHeading);
            AddStringToPool(tPathList, tTempResolvePack.mName);

            //--Next.
            rVariable = tAllVarsList->AutoIterate();
        }

        ///--[Write To File]
        //--Print how many elements are in the lookup. This is a 32-bit integer.
        uint32_t tElements = (uint32_t)tPathList->GetListSize();
        pBuffer->AppendUInt32(tElements);
        fprintf(stderr, "String pool elements: %i\n", tElements);

        //--Each element is a string, with the length printed ahead of it.
        void *rCheckVar = tPathList->PushIterator();
        while(rCheckVar)
        {
            //--Get the element. Append it to the file.
            const char *rName = tPathList->GetIteratorName();
            pBuffer->AppendStringWithLen(rName);

            //--Next.
            rCheckVar = tPathList->AutoIterate();
        }

        ///--[Debug]
        //--Display all of the names and how many times they were used.
        #if defined TRACK_STRING_POOL_USES
        if(false)
        {
            //--Setup.
            int tTotalThatAppearOnce = 0;
            int tTotalBytesFromFull = 0;
            int tTotalBytesFromPool = 0;

            //--The variable tTotalBytesFromFull represents how many bytes the listing would occupy if all of
            //  the variable paths were written in full. It will therefore be (2 bytes * number of variables)
            //  at the start in order to print the lengths of the paths.
            tTotalBytesFromFull = (2 * tAllVarsList->GetListSize());

            //--Every path in the full case then is written Root/X/Y/Z/Name, so we add three bytes for the
            //  slashes present in that, plus the 4 bytes for "Root".
            tTotalBytesFromFull = ((3 + 4) * tAllVarsList->GetListSize());

            //--Heading.
            fprintf(stderr, "Displaying string pool, %i elements:\n", tPathList->GetListSize());
            fprintf(stderr, "Total number of variables: %i\n", tAllVarsList->GetListSize());

            //--Iterate.
            int i = 0;
            int *rCount = (int *)tPathList->PushIterator();
            while(rCount)
            {
                //--Get the element.
                const char *rName = tPathList->GetIteratorName();
                int tNameLen = (int)strlen(rName);

                //--If the element appears once, don't display it to keep from clogging the list.
                if(*rCount < 2)
                {
                    tTotalThatAppearOnce ++;
                }
                //--Display the element:
                else
                {
                    //fprintf(stderr, " %04i: %s appears %i times.\n", i, rName, *rCount);
                }

                //--Add 2 bytes plus the length of the element to the "pool" tally. This is how large
                //  the string pool will be in the file.
                tTotalBytesFromPool = tTotalBytesFromPool + (2 + tNameLen);

                //--If we were to instead write out all the variables longform, then the name would be written
                //  out *rCount times. We already took care of the header for the strings, so just add here.
                tTotalBytesFromFull = tTotalBytesFromFull + ((*rCount) * tNameLen);

                //--Next.
                i ++;
                rCount = (int *)tPathList->AutoIterate();
            }

            //--Once all display is done, show how many entries appear once.
            fprintf(stderr, "Entries that appear multiple times: %i\n", tPathList->GetListSize() - tTotalThatAppearOnce);
            fprintf(stderr, "Entries that appear exactly once: %i\n", tTotalThatAppearOnce);

            //--Statistics:
            fprintf(stderr, "Bytes when rendered as a full set: %i\n", tTotalBytesFromFull);
            fprintf(stderr, "Bytes when rendered as a pool: %i\n", tTotalBytesFromPool);
            fprintf(stderr, "Bytes saved when using a pool: %i\n", tTotalBytesFromFull - tTotalBytesFromPool);
        }
        #endif
    }*/

    ///--[ ========================= Write to Buffer ======================== ]
    ///--[Write All Variables]
    //--Write how many variables there are.
    pBuffer->AppendInt32(tAllVarsList->GetListSize());
    DebugPrint("Stored variable count: %i.\n", tAllVarsList->GetListSize());

    //--Setup.
    ResolvePack tTempNamesPack;
    memset(&tTempNamesPack, 0, sizeof(ResolvePack));

    //--Write the variables.
    int i = 0;
    SysVar *rVariable = (SysVar *)tAllVarsList->PushIterator();
    while(rVariable)
    {
        //--When writing the full length strings, render the entire path as one string:
        if(!pUsePathCompression)
        {
            DebugPrint(" %03i: %s - ", i, tAllVarsList->GetIteratorName());
            pBuffer->AppendStringWithLen(tAllVarsList->GetIteratorName());
        }
        //--If we are using compressed paths, render 4 16-bit integers. These reference lookups
        //  in the string pool.
        else
        {
            //--Retrieve the name.
            const char *rFullPath = tAllVarsList->GetIteratorName();

            //--Move them into a storage package.
            GetNames(rFullPath, tTempNamesPack);

            //--Find them in the linked list of the string pool. It should be logically impossible to not
            //  find an entry, but if it does happen, -1 gets returned. This will be the max value which
            //  will be out of range, and will throw an error.
            uint16_t tSlotOfSection   = mPathPool->GetSlotOfElementByName(tTempNamesPack.mSection);
            uint16_t tSlotOfCatalogue = mPathPool->GetSlotOfElementByName(tTempNamesPack.mCatalogue);
            uint16_t tSlotOfHeading   = mPathPool->GetSlotOfElementByName(tTempNamesPack.mHeading);
            uint16_t tSlotOfName      = mPathPool->GetSlotOfElementByName(tTempNamesPack.mName);

            //--Error check:
            if(tSlotOfSection == 0xFFFF || tSlotOfCatalogue == 0xFFFF || tSlotOfHeading == 0xFFFF || tSlotOfName == 0xFFFF)
            {
                fprintf(stderr, "Warning: Element %s had a name not in the string pool.\n", rFullPath);
                fprintf(stderr, " Print: %i %i %i %i\n", tSlotOfSection, tSlotOfCatalogue, tSlotOfHeading, tSlotOfName);
            }

            //--Write.
            pBuffer->AppendUInt16(tSlotOfSection);
            pBuffer->AppendUInt16(tSlotOfCatalogue);
            pBuffer->AppendUInt16(tSlotOfHeading);
            pBuffer->AppendUInt16(tSlotOfName);
        }

        //--If this is a numerical type, place an 'N' and the value as a float.
        if(!rVariable->mAlpha || !strcasecmp(rVariable->mAlpha, "NULL"))
        {
            DebugPrint("N: ");
            pBuffer->AppendCharacter('N');
            pBuffer->AppendFloat(rVariable->mNumeric);
            DebugPrint(" %f\n", rVariable->mNumeric);
        }
        //--String type, place an 'S' and the value as a string.
        else
        {
            DebugPrint("S: ");
            pBuffer->AppendCharacter('S');
            pBuffer->AppendStringWithLen(rVariable->mAlpha);
            DebugPrint(" %s\n", rVariable->mAlpha);
        }

        //--Next.
        i++;
        rVariable = (SysVar *)tAllVarsList->AutoIterate();
    }
    DebugPrint("Wrote variables.\n");

    ///--[ ============================ Finish Up =========================== ]
    //--Deallocate.
    delete tAllVarsList;

    //--Debug.
    DebugPop("Finished writing SCRIPTVARS_ block.\n");
}

///========================================== Loading =============================================
void DataLibrary::ReadFromFile(VirtualFile *fInfile, bool pUsePathPool)
{
    ///--[Documentation and Setup]
    //--Reads the script variables from the file, assuming the cursor is in position. Script variables that
    //  already exist are overwritten, new ones are instantiated as needed. Variables not in the file are
    //  left alone.
    if(!fInfile) return;
    fInfile->mAssumeZeroIsMax = true;

    ///--[String Pool]
    //--If the flag pUsePathPool is true, then the variable names are reassembled by using lookups from
    //  the string pool. This creates an overall-smaller file size by re-referencing repeated name patterns.
    int32_t tExpectedPathElements = 0;
    char **tPathElements = NULL;
    if(pUsePathPool)
    {
        //--Read how many elements should be on the path list, and allocate that many.
        fInfile->Read(&tExpectedPathElements, sizeof(int32_t), 1);

        //--If there are zero path elements, leave the lookup as NULL.
        if(tExpectedPathElements > 0)
        {
            //--Allocate.
            SetMemoryData(__FILE__, __LINE__);
            tPathElements = (char **)starmemoryalloc(sizeof(char *) * tExpectedPathElements);

            //--Read. Each string is led by a 2-byte integer giving its length.
            for(int i = 0; i < tExpectedPathElements; i ++)
            {
                tPathElements[i] = fInfile->ReadLenString();
            }
        }
    }

    ///--[Variable Reading]
    //--Get how many variables are expected.
    int32_t tExpectedVariables = 0;
    fInfile->Read(&tExpectedVariables, sizeof(int32_t), 1);

    //--Read the variables.
    for(int i = 0; i < tExpectedVariables; i ++)
    {
        ///--[Setup]
        char *tVariablePath = NULL;

        ///--[Full Path]
        //--Simply read the path as a single string.
        if(!pUsePathPool)
        {
            tVariablePath = fInfile->ReadLenString();
        }
        ///--[String Pool]
        //--Reassemble the path in the pattern of Root/Section/Catalogue/Heading/Name. Four 16-bit integers
        //  are provided which are lookups into the path pool.
        else
        {
            //--Read the four 16-bit integers.
            uint16_t tSectionIndex, tCatalogueIndex, tHeadingIndex, tNameIndex;
            fInfile->Read(&tSectionIndex,   sizeof(uint16_t), 1);
            fInfile->Read(&tCatalogueIndex, sizeof(uint16_t), 1);
            fInfile->Read(&tHeadingIndex,   sizeof(uint16_t), 1);
            fInfile->Read(&tNameIndex,      sizeof(uint16_t), 1);

            //--Error check:
            if(tSectionIndex >= tExpectedPathElements || tCatalogueIndex >= tExpectedPathElements || tHeadingIndex >= tExpectedPathElements || tNameIndex >= tExpectedPathElements)
            {
                //--Print a warning.
                fprintf(stderr, "Error: File has a pool entry out of range. Entries: %i. Values: %i %i %i %i\n", tExpectedPathElements, tSectionIndex, tCatalogueIndex, tHeadingIndex, tNameIndex);

                //--Skip creating the variable. Read the next value but don't store it anywhere.
                char tLetter = 'N';
                fInfile->Read(&tLetter, sizeof(char), 1);
                if(tLetter == 'N')
                {
                    float tDummy;
                    fInfile->Read(&tDummy, sizeof(float), 1);
                }
                //--String.
                else
                {
                    fInfile->ReadLenString();
                }

                //--Skip ahead.
                continue;
            }
            //--Check if any of the entires were NULL somehow. This shouldn't be logically possible.
            else if(!tPathElements[tSectionIndex] || !tPathElements[tCatalogueIndex] || !tPathElements[tHeadingIndex] || !tPathElements[tNameIndex])
            {
                //--Print a warning.
                fprintf(stderr, "Error: File has a pool entry that was unfilled. Values: %i %i %i %i\n", tSectionIndex, tCatalogueIndex, tHeadingIndex, tNameIndex);
                fprintf(stderr, " Pointers %p %p %p %p\n", tPathElements[tSectionIndex], tPathElements[tCatalogueIndex], tPathElements[tHeadingIndex], tPathElements[tNameIndex]);

                //--Skip creating the variable. Read the next value but don't store it anywhere.
                char tLetter = 'N';
                fInfile->Read(&tLetter, sizeof(char), 1);
                if(tLetter == 'N')
                {
                    float tDummy;
                    fInfile->Read(&tDummy, sizeof(float), 1);
                }
                //--String.
                else
                {
                    fInfile->ReadLenString();
                }

                //--Skip ahead.
                continue;
            }
            //--All checks passed. Reassemble the path name.
            else
            {
                tVariablePath = InitializeString("Root/%s/%s/%s/%s", tPathElements[tSectionIndex], tPathElements[tCatalogueIndex], tPathElements[tHeadingIndex], tPathElements[tNameIndex]);
            }
        }

        //--Check if the variable exists.
        mFailSilently = true;
        SysVar *rVar = (SysVar *)GetEntry(tVariablePath);
        mFailSilently = false;

        //--If the variable does not exist, create it.
        if(!rVar)
        {
            //--Create the variable.
            SetMemoryData(__FILE__, __LINE__);
            rVar = (SysVar *)starmemoryalloc(sizeof(SysVar));
            rVar->mIsSaved = true;
            rVar->mNumeric = 0.0f;
            SetMemoryData(__FILE__, __LINE__);
            rVar->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen("NULL") + 1));
            strcpy(rVar->mAlpha, "NULL");

            //--Add the path.
            AddPath(tVariablePath);
            RegisterPointer(tVariablePath, rVar, &DataLibrary::DeleteSysVar);
        }

        //--Read a character. If it's N, this is a numeric value.
        char tLetter = 'N';
        fInfile->Read(&tLetter, sizeof(char), 1);
        if(tLetter == 'N')
        {
            fInfile->Read(&rVar->mNumeric, sizeof(float), 1);
        }
        //--String.
        else
        {
            free(rVar->mAlpha);
            rVar->mAlpha = fInfile->ReadLenString();
        }

        //--Clean.
        free(tVariablePath);
    }

    ///--[Clean]
    fInfile->mAssumeZeroIsMax = false;
    for(int i = 0; i < tExpectedPathElements; i ++) free(tPathElements[i]);
    free(tPathElements);
}

///--[Lua Functions]
int Hook_VM_Exists(lua_State *L)
{
    //--Returns true if the entry exists. This doesn't check for a specific SysVar, it actually
    //  just returns true if the DL entry exists at all.
    //VM_Exists(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("VM_Exists", L);

    //--Check
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    void *rCheckPtr = rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    lua_pushboolean(L, (rCheckPtr != NULL));
    return 1;
}
int Hook_VM_SetVar(lua_State *L)
{
    //--Sets the variable. If it doesn't exist, creates it.
    //VM_SetVar(sDLPath, "S", sString)
    //VM_SetVar(sDLPath, "N", fValue)
    int tArgs = lua_gettop(L);
    if(tArgs != 3)
    {
        //--Get the executing line.
        lua_Debug tLuaDebug;
        lua_getstack(L, 1, &tLuaDebug);
        lua_getinfo(L, "nSl", &tLuaDebug);
        int tExecLine = tLuaDebug.currentline;

        if(tArgs < 1) return LuaArgError("VM_SetVar");
        fprintf(stderr, "VM_SetVar. Incorrect argument count. Variable: %s.\nIn file: %s\nLine: %i\n", lua_tostring(L, 1), LuaManager::Fetch()->GetCallStack(0), tExecLine);
        return 0;
    }

    //--Fail if running to a checkpoint. Used by the VisualNovel engine.
    if(RootEvent::xIsRunningToCheckpoint) return 0;

    //--Nil check.
    if(lua_isnil(L, 3))
    {
        //--Get the executing line.
        lua_Debug tLuaDebug;
        lua_getstack(L, 1, &tLuaDebug);
        lua_getinfo(L, "nSl", &tLuaDebug);
        int tExecLine = tLuaDebug.currentline;

        DebugManager::ForcePrint("VM_SetVar: Error, argument 3 was nil. Variable: %s.\nIn file: %s\nLine: %i\n", lua_tostring(L, 1), LuaManager::Fetch()->GetCallStack(0), tExecLine);
        return 0;
    }

    //--Fetch the var.
    bool tNewReg = false;
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    SysVar *rVar = (SysVar *)rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    //--Doesn't exist?  Create one.
    if(!rVar)
    {
        SetMemoryData(__FILE__, __LINE__);
        rVar = (SysVar *)starmemoryalloc(sizeof(SysVar));
        rVar->mIsSaved = true;
        rVar->mNumeric = 0.0f;
        SetMemoryData(__FILE__, __LINE__);
        rVar->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen("NULL") + 1));
        strcpy(rVar->mAlpha, "NULL");
        tNewReg = true;
    }

    //--Set the variable appropriately.
    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "S"))
    {
        const char *rString = lua_tostring(L, 3);
        free(rVar->mAlpha);
        SetMemoryData(__FILE__, __LINE__);
        rVar->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen(rString) + 1));
        strcpy(rVar->mAlpha, rString);
    }
    else if(!strcmp(rTypeString, "N"))
    {
        rVar->mNumeric = lua_tonumber(L, 3);
    }
    else if(!strcmp(rTypeString, "I"))
    {
        rVar->mNumeric = (float)lua_tointeger(L, 3);
    }

    //--If this is a new var, reg it.
    if(tNewReg)
    {
        rLibrary->AddPath(lua_tostring(L, 1));
        rLibrary->RegisterPointer(lua_tostring(L, 1), rVar, &DataLibrary::DeleteSysVar);
    }
    return 0;
}
int Hook_VM_RemVar(lua_State *L)
{
    //--Removes and deallocates the variable.
    //VM_RemVar(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("VM_RemVar");

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    SysVar *rVar = (SysVar *)rLibrary->RemovePointer(lua_tostring(L, 1));

    //--Doesn't exist?  Done!
    if(!rVar) return 0;

    //--Deallocate it.
    free(rVar->mAlpha);
    free(rVar);
    return 0;
}
int Hook_VM_GetVar(lua_State *L)
{
    //--Returns the value in the variable, based on the type passed.
    //VM_GetVar(sDLPath, "S")
    //VM_GetVar(sDLPath, "N")
    //VM_GetVar(sDLPath, "I")
    int tArgs = lua_gettop(L);
    if(tArgs != 2)
    {
        if(tArgs < 1) return LuaArgError("VM_GetVar");
        fprintf(stderr, "VM_GetVar. Incorrect argument count. Variable: %s.\n", lua_tostring(L, 1));
        return 0;
    }

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    SysVar *rVar = (SysVar *)rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    //--Doesn't exist?  Done.
    if(!rVar)
    {
        //fprintf(stderr, "VM_GetVar:  Failed, var not found\n");
        lua_pushnumber(L, 0.0f);
        return 1;
    }

    //--Return the variable appropriately.
    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "S"))
    {
        lua_pushstring(L, rVar->mAlpha);
        return 1;
    }
    else if(!strcmp(rTypeString, "N"))
    {
        lua_pushnumber(L, rVar->mNumeric);
        return 1;
    }
    else if(!strcmp(rTypeString, "I"))
    {
        lua_pushinteger(L, rVar->mNumeric);
        return 1;
    }

    fprintf(stderr, "VM_GetVar:  Failed, can't resolve %s\n", rTypeString);
    lua_pushnumber(L, 0.0f);
    return 1;
}
int Hook_VM_SetSaveFlag(lua_State *L)
{
    //--Edits the save flag. If true, the game will save it and load it. This is true by default.
    //VM_SetSaveFlag(sDLPath, bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("VM_SetSaveFlag", L);

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    SysVar *rVar = (SysVar *)rLibrary->RemovePointer(lua_tostring(L, 1));

    //--Doesn't exist?  Done!
    if(!rVar) return 0;

    //--Flip the flag.
    rVar->mIsSaved = lua_toboolean(L, 2);
    return 0;
}
