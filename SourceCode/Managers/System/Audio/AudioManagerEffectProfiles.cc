//--Base
#include "AudioManager.h"

//--Classes
#include "AudioPackageStream.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"

uint32_t AudioManager::GetBassEnumeration(const char *pEnumerationName)
{
    ///--[Documentation]
    //--Given an enumeration from the Bass library, spelled out, returns its integer value. Used by Lua scripts
    //  that need to interact with the library. Returns 0 on error.
    if(!pEnumerationName) return 0;

    ///--[Enumerations]
    #if defined _BASS_AUDIO_
    if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_ROTATE"))
    {
        return BASS_FX_BFX_ROTATE;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_VOLUME"))
    {
        return BASS_FX_BFX_VOLUME;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_PEAKEQ"))
    {
        return BASS_FX_BFX_PEAKEQ;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_MIX"))
    {
        return BASS_FX_BFX_MIX;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_DAMP"))
    {
        return BASS_FX_BFX_DAMP;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_AUTOWAH"))
    {
        return BASS_FX_BFX_AUTOWAH;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_PHASER"))
    {
        return BASS_FX_BFX_PHASER;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_CHORUS"))
    {
        return BASS_FX_BFX_CHORUS;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_DISTORTION"))
    {
        return BASS_FX_BFX_DISTORTION;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_COMPRESSOR2"))
    {
        return BASS_FX_BFX_COMPRESSOR2;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_VOLUME_ENV"))
    {
        return BASS_FX_BFX_VOLUME_ENV;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_BQF"))
    {
        return BASS_FX_BFX_BQF;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_ECHO4"))
    {
        return BASS_FX_BFX_ECHO4;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_PITCHSHIFT"))
    {
        return BASS_FX_BFX_PITCHSHIFT;
    }
    else if(!strcasecmp(pEnumerationName, "BASS_FX_BFX_FREEVERB"))
    {
        return BASS_FX_BFX_FREEVERB;
    }


    //--Error.
    DebugManager::ForcePrint("AudioManager::GetBassEnumeration() - Error, enumeration %s was not found.\n", pEnumerationName);
    return 0;
    #endif
    return 0;
}
EffectProfilePackage *AudioManager::GetEffectProfilePackage(const char *pName)
{
    return (EffectProfilePackage *)mEffectProfiles->GetElementByName(pName);
}
void AudioManager::RegisterEffectProfile(const char *pName, uint32_t pType)
{
    ///--[Documentation]
    //--Registers a new effect profile with a given type, or fails if the profile already exists.
    void *rCheckProfile = mEffectProfiles->GetElementByName(pName);
    if(rCheckProfile) return;

    ///--[Create, Register]
    EffectProfilePackage *nPackage = (EffectProfilePackage *)starmemoryalloc(sizeof(EffectProfilePackage));
    memset(nPackage, 0, sizeof(EffectProfilePackage));
    nPackage->mType = pType;
    mEffectProfiles->AddElementAsTail(pName, nPackage, &FreeThis);
}
void AudioManager::SetEffectProfileProperty(const char *pName, uint32_t pType, const char *pPropertyString, float pValue)
{
    ///--[Documentation]
    //--Sets a property in the given typeset, or prints an error if the package is not found or if the property is invalid.
    if(!pName) return;

    //--Locate package.
    EffectProfilePackage *rPackage = (EffectProfilePackage *)mEffectProfiles->GetElementByName(pName);
    if(!rPackage)
    {
        DebugManager::ForcePrint("AudioManager::SetEffectProfileProperty() - Warning, package %s was not found.\n", pName);
        return;
    }

    ///--[Set By Type]
    #if defined _BASS_AUDIO_
    if(pType == BASS_FX_BFX_ROTATE)
    {
        if(!strcasecmp(pPropertyString, "Rate"))
        {
            rPackage->mData.mRotate.fRate = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mRotate.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_VOLUME)
    {
        if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mRotate.lChannel = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Volume"))
        {
            rPackage->mData.mVolume.fVolume = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_PEAKEQ)
    {
        if(!strcasecmp(pPropertyString, "Band"))
        {
            rPackage->mData.mPeakEq.lBand = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Bandwidth"))
        {
            rPackage->mData.mPeakEq.fBandwidth = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Q"))
        {
            rPackage->mData.mPeakEq.fQ = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Center"))
        {
            rPackage->mData.mPeakEq.fCenter = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Gain"))
        {
            rPackage->mData.mPeakEq.fGain = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mPeakEq.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_MIX)
    {
        //--Not supported
    }
    else if(pType == BASS_FX_BFX_DAMP)
    {
        if(!strcasecmp(pPropertyString, "Target"))
        {
            rPackage->mData.mDampener.fTarget = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Quiet"))
        {
            rPackage->mData.mDampener.fQuiet = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Rate"))
        {
            rPackage->mData.mDampener.fRate = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Gain"))
        {
            rPackage->mData.mDampener.fGain = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Delay"))
        {
            rPackage->mData.mDampener.fDelay = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mDampener.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_AUTOWAH)
    {
        if(!strcasecmp(pPropertyString, "Dry Mix"))
        {
            rPackage->mData.mAutoWah.fDryMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Wet mix"))
        {
            rPackage->mData.mAutoWah.fWetMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Feedback"))
        {
            rPackage->mData.mAutoWah.fFeedback = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Rate"))
        {
            rPackage->mData.mAutoWah.fRate = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Range"))
        {
            rPackage->mData.mAutoWah.fRange = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Frequency"))
        {
            rPackage->mData.mAutoWah.fFreq = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mAutoWah.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_PHASER)
    {
        if(!strcasecmp(pPropertyString, "Dry Mix"))
        {
            rPackage->mData.mPhaser.fDryMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Wet Mix"))
        {
            rPackage->mData.mPhaser.fWetMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Feedback"))
        {
            rPackage->mData.mPhaser.fFeedback = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Rate"))
        {
            rPackage->mData.mPhaser.fRate = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Range"))
        {
            rPackage->mData.mPhaser.fRange = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Frequency"))
        {
            rPackage->mData.mPhaser.fFreq = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mPhaser.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_CHORUS)
    {
        if(!strcasecmp(pPropertyString, "Dry Mix"))
        {
            rPackage->mData.mChorus.fDryMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Wet Mix"))
        {
            rPackage->mData.mChorus.fWetMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Feedback"))
        {
            rPackage->mData.mChorus.fFeedback = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Min Sweep"))
        {
            rPackage->mData.mChorus.fMinSweep = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Max Sweep"))
        {
            rPackage->mData.mChorus.fMaxSweep = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Rate"))
        {
            rPackage->mData.mChorus.fRate = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mChorus.lChannel = (int)pValue;
        }
    }
    else if(pType == BASS_FX_BFX_DISTORTION)
    {
        if(!strcasecmp(pPropertyString, "Drive"))
        {
            rPackage->mData.mDistortion.fDrive = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Dry Mix"))
        {
            rPackage->mData.mDistortion.fDryMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Wet Mix"))
        {
            rPackage->mData.mDistortion.fWetMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Feedback"))
        {
            rPackage->mData.mDistortion.fFeedback = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Volume"))
        {
            rPackage->mData.mDistortion.fVolume = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mDistortion.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_COMPRESSOR2)
    {
        if(!strcasecmp(pPropertyString, "Gain"))
        {
            rPackage->mData.mCompressor.fGain = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Threshold"))
        {
            rPackage->mData.mCompressor.fThreshold = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Ratio"))
        {
            rPackage->mData.mCompressor.fRatio = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Attack"))
        {
            rPackage->mData.mCompressor.fAttack = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Release"))
        {
            rPackage->mData.mCompressor.fRelease = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mCompressor.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_VOLUME_ENV)
    {
        //--Not supported
    }
    else if(pType == BASS_FX_BFX_BQF)
    {
        //--Not supported
    }
    else if(pType == BASS_FX_BFX_ECHO4)
    {
        if(!strcasecmp(pPropertyString, "Dry Mix"))
        {
            rPackage->mData.mEcho.fDryMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Wet Mix"))
        {
            rPackage->mData.mEcho.fWetMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Feedback"))
        {
            rPackage->mData.mEcho.fFeedback = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Delay"))
        {
            rPackage->mData.mEcho.fDelay = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Stereo"))
        {
            rPackage->mData.mEcho.bStereo = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mEcho.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_PITCHSHIFT)
    {
        if(!strcasecmp(pPropertyString, "Pitch Shift"))
        {
            rPackage->mData.mPitchShift.fPitchShift = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Semitones"))
        {
            rPackage->mData.mPitchShift.fSemitones = pValue;
        }
        else if(!strcasecmp(pPropertyString, "FFTSize"))
        {
            rPackage->mData.mPitchShift.lFFTsize = pValue;
        }
        else if(!strcasecmp(pPropertyString, "OSamp"))
        {
            rPackage->mData.mPitchShift.lOsamp = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mPitchShift.lChannel = pValue;
        }
    }
    else if(pType == BASS_FX_BFX_FREEVERB)
    {
        if(!strcasecmp(pPropertyString, "Dry Mix"))
        {
            rPackage->mData.mReverb.fDryMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Wet Mix"))
        {
            rPackage->mData.mReverb.fWetMix = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Room Size"))
        {
            rPackage->mData.mReverb.fRoomSize = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Damp"))
        {
            rPackage->mData.mReverb.fDamp = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Width"))
        {
            rPackage->mData.mReverb.fWidth = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Mode"))
        {
            rPackage->mData.mReverb.lMode = pValue;
        }
        else if(!strcasecmp(pPropertyString, "Channel"))
        {
            rPackage->mData.mReverb.lChannel = pValue;
        }
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("AudioManager::SetEffectProfileProperty() - Error, setting type %i in package %s was out of range.\n", pType, pName);
    }
    #endif
}
void AudioManager::ApplyEffectProfileToSound(const char *pSoundName, const char *pProfileName)
{
    ///--[Documentation]
    //--Applies the named effect to the named sound, assuming the sound is a stream. If "Null" is passed, then the sound
    //  will clear any effects.
    AudioPackage *rCheckPackage = (AudioPackage *)mSoundList->GetElementByName(pSoundName);
    if(!rCheckPackage || rCheckPackage->GetAudioPackageType() != AUDIOPACKAGE_TYPE_STREAM) return;

    //--Set.
    AudioPackageStream *rStream = (AudioPackageStream *)rCheckPackage;
    rStream->SetEffectPackage(pProfileName);
}
void AudioManager::ClearEffectProfiles()
{
    mEffectProfiles->ClearList();
}
