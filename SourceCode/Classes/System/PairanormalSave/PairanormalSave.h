///====================================== PairanormalSave ==========================================
//--Handles saving and loading for Pairanormal's unique formats. Most of the variables are statics.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class PairanormalSave
{
    private:
    ///--[System]
    ///--[Etc]

    protected:

    public:
    //--System
    PairanormalSave();
    ~PairanormalSave();

    //--Public Variables
    static bool xRunBackupSave;

    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    static void SavePairanormalFile(const char *pPath);
    static void LoadPairanormalFile(const char *pPath);
    static void SavePairanormalGallery(const char *pPath);
    static void LoadPairanormalGallery(const char *pPath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Save_WritePairanormalGalleryData(lua_State *L);
int Hook_Save_ReadPairanormalGalleryData(lua_State *L);


