-- |[ ================================= SysPaths Game Entries ================================== ]|
--Functions for adding and querying GameEntry objects.

-- |[ ============================== SysPaths:fnGameEntryExists() ============================== ]|
--Returns true if the named game entry exists, false if it does not. This only checks if the entry
-- already exists on the list, not if it's valid.
function SysPaths:fnGameEntryExists(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return end
    
    -- |[Execution]|
    for i = 1, #self.zaGameEntries, 1 do
        if(self.zaGameEntries[i].sName == psName) then return true end
    end
    
    --Not found.
    return false
end

-- |[ =============================== SysPaths:fnGetGameEntry() ================================ ]|
--Returns the GameEntry table referenced by the given name. If not found, returns nil. Does not
-- bark a warning.
function SysPaths:fnGetGameEntry(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return end
    
    -- |[Execution]|
    for i = 1, #self.zaGameEntries, 1 do
        if(self.zaGameEntries[i].sName == psName) then return self.zaGameEntries[i] end
    end
    
    --Not found.
    return nil
end

-- |[ ============================= SysPaths:fnGetGameEntryValid() ============================= ]|
--Returns the GameEntry table referenced by the given name. If not found, returns nil. In addition,
-- if the game entry exists but does not have an active path, returns nil. This occurs when the
-- entry is registered but not on the hard drive.
function SysPaths:fnGetGameEntryValid(psName)
    
    -- |[Existence Check]|
    --Check if the entry exists at all.
    local zGameEntry = self:fnGetGameEntry(psName)
    if(zGameEntry == nil) then return nil end
    
    -- |[Active Path Check]|
    --If the active path is nil or "Null", return nil.
    if(zGameEntry.sActivePath == nil or zGameEntry.sActivePath == "Null") then return end
    
    -- |[All Checks Passed]|
    return zGameEntry
end
    
-- |[ =============================== SysPaths:fnAddGameEntry() ================================ ]|
--Creates a new GameEntry object and registers it to the master list.
function SysPaths:fnAddGameEntry(psName, psVarPath, psButtonText, piPriority, psFileLaunchPath, piNegativeLen)
    
    -- |[Argument Check]|
    if(psName           == nil) then return end
    if(psVarPath        == nil) then return end
    if(psButtonText     == nil) then return end
    if(piPriority       == nil) then return end
    if(psFileLaunchPath == nil) then return end
    if(piNegativeLen    == nil) then return end
    
    -- |[Duplicate Check]|
    --Scan the game entries list. If the game entry already exists, do nothing.
    for i = 1, #self.zaGameEntries, 1 do
        if(self.zaGameEntries[i].sName == psName) then return end
    end
    
    -- |[Create]|
    local zEntry = {}
    zEntry.sName = psName
    zEntry.sVarPath = psVarPath
    zEntry.sButtonText = psButtonText
    zEntry.iPriority = piPriority
    zEntry.sFileLaunchPath = psFileLaunchPath
    zEntry.sActivePath = "Null"
    zEntry.saSearchPaths = {}
    zEntry.iNegativeLen = piNegativeLen
    
    -- |[Register]|
    table.insert(self.zaGameEntries, zEntry)
end

-- |[ =========================== SysPaths:fnAddSearchPathToEntry() ============================ ]|
--Given the unique name of a GameEntry, adds a new Search Path to its list.
function SysPaths:fnAddSearchPathToEntry(psName, psSearchPath)
    
    -- |[Argument Check]|
    if(psName       == nil) then return end
    if(psSearchPath == nil) then return end
    
    -- |[Locate Entry]|
    --Locate the entry.
    local zEditEntry = nil
    for i = 1, #self.zaGameEntries, 1 do
        if(self.zaGameEntries[i].sName == psName) then
            zEditEntry = self.zaGameEntries[i]
            break
        end
    end
    
    --Not found:
    if(zEditEntry == nil) then
        io.write("SysPaths:fnAddSearchPathToEntry() - Warning. No GameEntry " .. psName .. " found. Cannot add search path " .. psSearchPath .. "\n")
        return
    end
    
    -- |[Duplicate Check]|
    --Make sure this search path hasn't already been added.
    for i = 1, #zEditEntry.saSearchPaths, 1 do
        if(zEditEntry.saSearchPaths[i] == psSearchPath) then return end
    end
    
    -- |[Addition]|
    --Append the search path.
    table.insert(zEditEntry.saSearchPaths, psSearchPath)
end

-- |[ =========================== SysPaths:fnGameEntryHasValidPath() =========================== ]|
--Causes the GameEntry to scan its search paths. If one is found that actually exists on the hard
-- drive, sets the active path for the entry and returns true. If none are found, clears the active
-- path to "Null" and returns false.
function SysPaths:fnGameEntryHasValidPath(psName)

    -- |[Argument Check]|
    if(psName == nil) then return end

    -- |[Locate]|
    --Make sure the entry exists.
    local zCheckEntry = self:fnGetGameEntry(psName)
    if(zCheckEntry == nil) then
        io.write("SysPaths:fnGameEntryHasValidPath() - Warning. Game entry " .. psName .. " was not found on the internal list.\n")
        return false
    end
    
    -- |[Scan Paths]|
    --Reset active path to "Null".
    zCheckEntry.sActivePath = "Null"
    
    --Scan all the search paths:
    for i = 1, #zCheckEntry.saSearchPaths, 1 do
        
        --If the file exists, set it as the active path and return true.
        if(FS_Exists(zCheckEntry.saSearchPaths[i])) then
            zCheckEntry.sActivePath = zCheckEntry.saSearchPaths[i]
            VM_SetVar(zCheckEntry.sVarPath, "S", string.sub(zCheckEntry.sActivePath, 1, zCheckEntry.iNegativeLen))
            return true
        end
    end
    
    -- |[Not Found]|
    return false
end


