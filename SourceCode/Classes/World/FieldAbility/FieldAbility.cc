//--Base
#include "FieldAbility.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
FieldAbility::FieldAbility()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVFIELDABILITY;

    //--[FieldAbility]
    //--System
    mLocalName = InitializeString("Unnamed Ability");
    mDisplayName = InitializeString("Unnamed Ability");
    mScriptPath = InitializeString("Null");
    mCancelsEvenWhenInactive = false;

    //--Cooldown
    mCooldownCur = 0;
    mCooldownMax = 0;

    //--Display
    rBackingImg = NULL;
    rFrameImg = NULL;
    rMainImg = NULL;
    mDisplayStringsTotal = 0;
    mDisplayStrings = NULL;
}
FieldAbility::~FieldAbility()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    for(int i = 0; i < mDisplayStringsTotal; i ++)
    {
        free(mDisplayStrings[i]);
    }
    free(mDisplayStrings);
}

///===================================== Property Queries =========================================
bool FieldAbility::IsCooling()
{
    return (mCooldownCur > 0);
}
int FieldAbility::GetCooldown()
{
    return mCooldownCur;
}
int FieldAbility::GetCooldownMax()
{
    if(mCooldownMax < 1) return 1;
    return mCooldownMax;
}
const char *FieldAbility::GetInternalName()
{
    return mLocalName;
}
const char *FieldAbility::GetDisplayName()
{
    return mDisplayName;
}
int FieldAbility::GetDisplayStringsTotal()
{
    return mDisplayStringsTotal;
}
const char *FieldAbility::GetDisplayString(int pSlot)
{
    if(pSlot < 0 || pSlot >= mDisplayStringsTotal) return NULL;
    return mDisplayStrings[pSlot];
}
bool FieldAbility::CancelsEvenWhenInactive()
{
    return mCancelsEvenWhenInactive;
}

///======================================== Manipulators ==========================================
void FieldAbility::SetInternalName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void FieldAbility::SetDisplayName(const char *pName)
{
    ResetString(mDisplayName, pName);
}
void FieldAbility::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void FieldAbility::SetCooldownCur(int pTicks)
{
    mCooldownCur = pTicks;
    if(mCooldownCur < 0) mCooldownCur = 0;
}
void FieldAbility::SetCooldownMax(int pTicks)
{
    mCooldownMax = pTicks;
    if(mCooldownCur >= mCooldownMax) mCooldownCur = mCooldownMax;
}
void FieldAbility::SetBackingImg(const char *pDLPath)
{
    rBackingImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void FieldAbility::SetFrameImg(const char *pDLPath)
{
    rFrameImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void FieldAbility::SetMainImg(const char *pDLPath)
{
    rMainImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void FieldAbility::AllocateDisplayStrings(int pTotal)
{
    for(int i = 0; i < mDisplayStringsTotal; i ++) free(mDisplayStrings[i]);
    free(mDisplayStrings);
    mDisplayStringsTotal = 0;
    mDisplayStrings = NULL;
    if(pTotal < 1) return;

    mDisplayStringsTotal = pTotal;
    mDisplayStrings = (char **)starmemoryalloc(sizeof(char *) * mDisplayStringsTotal);
    for(int i = 0; i < mDisplayStringsTotal; i ++) mDisplayStrings[i] = NULL;
}
void FieldAbility::SetDisplayString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= mDisplayStringsTotal) return;
    ResetString(mDisplayStrings[pSlot], pString);
}
void FieldAbility::SetCancelWhenInactiveFlag(bool pFlag)
{
    mCancelsEvenWhenInactive = pFlag;
}

///======================================== Core Methods ==========================================
void FieldAbility::ActivateCooldown()
{
    mCooldownCur = mCooldownMax;
}
void FieldAbility::Execute(int pCode)
{
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)pCode);
}
void FieldAbility::ExecuteOn(int pCode, void *pPtr)
{
    LuaManager::Fetch()->PushExecPop(pPtr, mScriptPath, 1, "N", (float)pCode);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void FieldAbility::Render(float pX, float pY, int pControlSlot, int pStencilCode, float pAlpha)
{
    ///--[Documentation]
    //--Renders the field ability at the given location, along with its associated control image
    //  and with a shading indicating cooldown state.
    //--To disallow rendering the cooldown shading, pass -1 for the stencil code. To disallow
    //  rendering the control image, pass -1 for the control slot.
    float cWid = 50.0f;
    float cHei = 50.0f;

    ///--[Baseline]
    //--Alpha.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Activate mask mode.
    if(pStencilCode != -1 && mCooldownCur > 0)
    {
        DisplayManager::ActivateMaskRender(pStencilCode);
        glColorMask(true, true, true, true);
    }

    //--Backing is rendered at transparency.
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if(rBackingImg) rBackingImg-> Draw(pX, pY);

    //--Front images match alpha. This makes them fade together.
    glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if(rMainImg) rMainImg-> Draw(pX, pY);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    ///--[Cooldown]
    //--Render by stencil. Not needed if not cooling down.
    if(pStencilCode != -1 && mCooldownCur > 0)
    {
        //--Stencil activate.
        DisplayManager::ActivateStencilRender(pStencilCode);

        //--Percentage complete.
        float tPercent = 1.0f - ((float)mCooldownCur / (float)mCooldownMax);

        //--Compute.
        float cDarkLft = pX;
        float cDarkTop = pY + (cHei * tPercent);
        float cDarkRgt = cDarkLft + cWid;
        float cDarkBot = pY + (cHei * 1.0f);

        //--Render.
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, pAlpha * 0.75f);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glVertex2f(cDarkLft, cDarkTop);
            glVertex2f(cDarkRgt, cDarkTop);
            glVertex2f(cDarkRgt, cDarkBot);
            glVertex2f(cDarkLft, cDarkBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    //--Render the frame.
    if(rFrameImg) rFrameImg->Draw(pX, pY);

    ///--[Controls]
    //--No stencils.
    DisplayManager::DeactivateStencilling();

    //--Get the associated control. Render that.
    if(pControlSlot > -1)
    {
        //--Figure out the name of the associated control.
        char tBuf[32];
        sprintf(tBuf, "FieldAbility%i", pControlSlot);

        //--Get the image for the control, if it exists. If not it won't render.
        StarBitmap *rControlImage = ControlManager::Fetch()->ResolveControlImage(tBuf);
        if(rControlImage) rControlImage->Draw(pX + cWid - rControlImage->GetWidth(), pY + cHei - rControlImage->GetHeight());
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
StarBitmap *FieldAbility::GetBackingImage()
{
    return rBackingImg;
}
StarBitmap *FieldAbility::GetFrameImage()
{
    return rFrameImg;
}
StarBitmap *FieldAbility::GetMainImage()
{
    return rMainImg;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void FieldAbility::HookToLuaState(lua_State *pLuaState)
{
    /* FieldAbility_Create(sInternalName, sDLPath) (Pushes Activity Stack)
       Creates and pushes a FieldAbility to be set up by further calls. */
    lua_register(pLuaState, "FieldAbility_Create", &Hook_FieldAbility_Create);

    /* FieldAbility_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Field Ability class. */
    lua_register(pLuaState, "FieldAbility_GetProperty", &Hook_FieldAbility_GetProperty);

    /* FieldAbility_SetProperty("Display Name")
       FieldAbility_SetProperty("Script Path", sPath)
       FieldAbility_SetProperty("Backing Image", sDLPath)
       FieldAbility_SetProperty("Frame Image", sDLPath)
       FieldAbility_SetProperty("Main Image", sDLPath)
       FieldAbility_SetProperty("Allocate Display Strings", iTotal)
       FieldAbility_SetProperty("Set Display Strings", iSlot, sString)
       FieldAbility_SetProperty("Cancels Even When Inactive", bFlag)
       Sets the requested property in the Field Ability class. */
    lua_register(pLuaState, "FieldAbility_SetProperty", &Hook_FieldAbility_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_FieldAbility_Create(lua_State *L)
{
    //FieldAbility_Create(sInternalName, sDLPath) (Pushes Activity Stack)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("FieldAbility_Create");

    //--Create.
    FieldAbility *nFieldAbility = new FieldAbility();
    nFieldAbility->SetInternalName(lua_tostring(L, 1));
    rDataLibrary->rActiveObject = nFieldAbility;

    //--Store in the DataLibrary.
    rDataLibrary->RegisterPointer(lua_tostring(L, 2), nFieldAbility, &RootObject::DeleteThis);
    return 0;
}
int Hook_FieldAbility_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //FieldAbility_GetProperty("Dummy") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FieldAbility_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVFIELDABILITY)) return LuaTypeError("FieldAbility_GetProperty", L);
    //FieldAbility *rAbility = (FieldAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("FieldAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_FieldAbility_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //FieldAbility_SetProperty("Display Name", sName)
    //FieldAbility_SetProperty("Script Path", sPath)
    //FieldAbility_SetProperty("Cooldown Max", iTicks)
    //FieldAbility_SetProperty("Backing Image", sDLPath)
    //FieldAbility_SetProperty("Frame Image", sDLPath)
    //FieldAbility_SetProperty("Main Image", sDLPath)
    //FieldAbility_SetProperty("Allocate Display Strings", iTotal)
    //FieldAbility_SetProperty("Set Display Strings", iSlot, sString)
    //FieldAbility_SetProperty("Cancels Even When Inactive", bFlag)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FieldAbility_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVFIELDABILITY)) return LuaTypeError("FieldAbility_SetProperty", L);
    FieldAbility *rAbility = (FieldAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Name that appears in the UI.
    if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rAbility->SetDisplayName(lua_tostring(L, 2));
    }
    //--Script called for various activities.
    else if(!strcasecmp(rSwitchType, "Script Path") && tArgs == 2)
    {
        rAbility->SetScriptPath(lua_tostring(L, 2));
    }
    //--Sets ability cooldown when used.
    else if(!strcasecmp(rSwitchType, "Cooldown Max") && tArgs == 2)
    {
        rAbility->SetCooldownMax(lua_tointeger(L, 2));
    }
    //--Backing.
    else if(!strcasecmp(rSwitchType, "Backing Image") && tArgs == 2)
    {
        rAbility->SetBackingImg(lua_tostring(L, 2));
    }
    //--Frame.
    else if(!strcasecmp(rSwitchType, "Frame Image") && tArgs == 2)
    {
        rAbility->SetFrameImg(lua_tostring(L, 2));
    }
    //--Primary image.
    else if(!strcasecmp(rSwitchType, "Main Image") && tArgs == 2)
    {
        rAbility->SetMainImg(lua_tostring(L, 2));
    }
    //--Allocates display strings for the ability.
    else if(!strcasecmp(rSwitchType, "Allocate Display Strings") && tArgs == 2)
    {
        rAbility->AllocateDisplayStrings(lua_tointeger(L, 2));
    }
    //--Sets a given display string.
    else if(!strcasecmp(rSwitchType, "Set Display Strings") && tArgs == 3)
    {
        rAbility->SetDisplayString(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--If true, the ability runs its cancel code even when inactive.
    else if(!strcasecmp(rSwitchType, "Cancels Even When Inactive") && tArgs == 2)
    {
        rAbility->SetCancelWhenInactiveFlag(lua_toboolean(L, 2));
    }
    //--Error case.
    else
    {
        LuaPropertyError("FieldAbility_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
