///======================================== AdvUISkills ===========================================
//--UI object that encapsulates the Adventure Skills UI.

#pragma once

#ifdef _TARGET_OS_WINDOWS_
    #ifndef _STEAM_API_
        #define constexpr const
    #endif
#endif
#ifdef _TARGET_OS_MAC_
#define constexpr const
#endif

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvCombatDefStruct.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#ifndef ADVUISKILLS_DEF
#define ADVUISKILLS_DEF

#define ADVMENU_SKILLS_HELP_MENU_STRINGS 24

//--Timers
#define ADVMENU_SKILLS_VIS_TICKS 15
#define ADVMENU_SKILLS_CURSOR_TICKS 15
#define ADVMENU_SKILLS_PROFILE_VIS_TICKS 15
#define ADVMENU_SKILLS_ARROW_OSCILLATE_TICKS 120
#define ADVMENU_SKILLS_CHARACTER_SWAP_TICKS 15
#define ADVMENU_SKILLS_PURCHASE_TICKS 15
#define ADVMENU_SKILLS_DETAILS_SWITCH_TICKS 7
#define ADVMENU_SKILLS_STRING_VIS_TICKS 7
#define ADVMENU_SKILLS_TACTICS_VIS_TICKS 7

//--Sizes
#define ADVMENU_SKILLS_ARROW_OSCILLATE_DISTANCE 5.0f
#define ADVMENU_SKILLS_MAX_JOBS_PER_PAGE 12
#define ADVMENU_SKILLS_JOB_SCROLL_RANGE 3
#define ADVMENU_SKILLS_MAX_SKILLS_PER_PAGE 11
#define ADVMENU_SKILLS_SKILL_SCROLL_RANGE 3
#define ADVMENU_SKILLS_MAX_PROFILES_PER_PAGE 12
#define ADVMENU_SKILLS_PROFILE_SCROLL_RANGE 3
#define ADVMENU_SKILLS_MAX_DESCRIPTION_LINES 8
#define ADVMENU_SKILLS_JOBNAME_MAXLEN 208.0f
#define ADVMENU_SKILLS_SKILLNAME_MAXLEN 244.0f
#define ADVMENU_SKILLS_INSTRUCTION_STRINGS_TOTAL 7

//--Modes
#define ADVMENU_SKILLS_MODE_JOB_SELECT 0
#define ADVMENU_SKILLS_MODE_SKILL_SELECT 1
#define ADVMENU_SKILLS_MODE_SKILL_QUERY 2
#define ADVMENU_SKILLS_MODE_SKILL_REPLACE 3
#define ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE 4
#define ADVMENU_SKILLS_MODE_PROFILES_SELECT 5
#define ADVMENU_SKILLS_MODE_PROFILES_NEW 6
#define ADVMENU_SKILLS_MODE_PROFILES_RENAME 7

#endif // defined

///========================================== Classes =============================================
class AdvUISkills : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    //--Sizes.
    static constexpr float cxHelpControlOffsetY = 3.0f;             //Pixels, how offset the images representing keys are from the text position.
    static constexpr float cxHelpSymbolH = 22.0f;                   //Pixels, height of help symbol.
    static constexpr float cxMainlineH = 22.0f;                     //Pixels, height of Mainline font.

    //--Scrollbars.
    static const int cxScrollbarJobsPerPage     = 12;
    static const int cxScrollbarJobsScrollBuf   =  3;
    static const int cxScrollbarSkillsPerPage   = 11;
    static const int cxScrollbarSkillsScrollBuf =  3;

    //--Profiles Mode.
    static const int cxProfileScrollBuf = 3;
    static const int cxProfilePageSize = 12;

    ///--[System]
    ///--[Skills UI]
    //--System
    int mArrowTimer;
    int mPurchaseTimer;
    bool mShowAdvancedDescriptions;

    //--Mode
    int mPrevMode;
    int mMode;

    //--Cursors
    int mCharacterCursor;
    int mJobCursor;
    int mJobSkip;
    int mSkillCursor;
    int mSkillSkip;
    int mAbilityCursorX;
    int mAbilityCursorY;

    //--Tactics Grid
    bool mIsUsingTacticsGrid;
    int mTacticsGridTimer;

    //--Character Swap
    bool mIsSwappingCharacter;
    int mCharacterSwapTimer;
    int mPrevCharacterSlot;

    //--Details
    bool mIsShowingDetails;
    bool mIsDetailsFromGrid;
    int mDetailsTimer;

    //--Strings
    StarlightString *mShowHelpString;
    StarlightString *mScrollFastString;
    StarlightString *mEditProfilesString;
    StarlightString *mSkillDetailsString;
    StarlightString *mInspectSkillsString;
    StarlightString *mShowTacticsString;
    StarlightString *mCharacterLftString;
    StarlightString *mCharacterRgtString;
    StarlightString *mConfirmPurchaseString;
    StarlightString *mConfirmDeleteString;
    StarlightString *mCancelPurchaseString;

    ///--[Profiles UI]
    //--System
    bool mIsProfilesMode;
    bool mIsProfileConfirm;
    int mProfilesVisibilityTimer;
    int mProfileConfirmTimer;

    //--Profile Select
    int mProfileCursor;
    int mProfileSkip;
    int mProfileJobCursor;

    //--New Profile
    int mStringEntryTimer;
    StringEntry *mStringEntryForm;

    //--Instructions
    StarlightString *mInstructionString[ADVMENU_SKILLS_INSTRUCTION_STRINGS_TOTAL];

    //--Strings
    StarlightString *mNewProfileString;
    StarlightString *mRenameProfileString;
    StarlightString *mDeleteProfileString;

    ///--[Common]
    //--Cursor Movement
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Backup Cursors
    EasingPack2D mJobHighlightPos;
    EasingPack2D mJobHighlightSize;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_SimpleDescription;
        StarFont *rFont_Description;
        StarFont *rFont_Statistics;

        //--Images for this UI
        StarBitmap *rFrame_Description;
        StarBitmap *rHeader_Expandable;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rArrowLft;
        StarBitmap *rArrowRgt;
        StarBitmap *rBuySkillPopup;
        StarBitmap *rClassDefaultIcon;
        StarBitmap *rDescriptionFrame;
        StarBitmap *rEquippedIcon;
        StarBitmap *rHeader;
        StarBitmap *rJobSkillsFrame;
        StarBitmap *rJobsDetail;
        StarBitmap *rJobsFrame;
        StarBitmap *rJobsScrollbar;
        StarBitmap *rMemorizedFrame;
        StarBitmap *rSkillsDetail;
        StarBitmap *rSkillsDetailSpecial;
        StarBitmap *rSkillsFrame;
        StarBitmap *rSkillsScrollbar;
        StarBitmap *rTacticsSkillsFrame;
        StarBitmap *rOverlay_Highlight;

        //--Icons
        StarBitmap *rCatalystIcon;

        //--Profiles Sub-UI
        StarBitmap *rProfilesDetail;
        StarBitmap *rProfilesFrame;
        StarBitmap *rProfilesHeader;
        StarBitmap *rProfilesInstructionsFrame;
        StarBitmap *rProfilesMemorized;
        StarBitmap *rProfilesScrollbar;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUISkills();
    virtual ~AdvUISkills();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    bool IsCatalystSlot(int pX, int pY);
    bool IsCustomizableSlot(int pX, int pY);
    AdvCombatJob *GetHighlightedJob(int pJobCursor);

    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void SetAbilityCursorToFirstEmptySlot();

    ///--[Profiles Mode]
    void SetToProfilesMode();
    void RecomputeProfilesCursorPos();
    bool UpdateProfilesMode(bool pDisallowControls);
    void UpdateProfilesSelectMode();
    void UpdateProfilesNewMode();
    void UpdateProfilesRenameMode();
    void RenderProfilesMode();
    void RenderProfile(float pY, const char *pName, bool pRenderBacking, AbilityProfile *pProfile, float pAlpha);
    void RenderProfileAbilities(AbilityProfile *pProfile);
    void RenderProfileNameEntry();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();
    void UpdateSkillsJobSelect();
    void UpdateSkillsSkillSelect();
    void UpdateSkillsSkillQuery();
    void UpdateSkillsPlacement();
    void UpdateSkillsPurchase();
    void UpdateSkillsDetails();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    void RenderSkillsJobData(float pAlpha);
    void RenderSkillsSkillsData(float pAlpha);
    void RenderSkillsClassData(float pAlpha);
    void RenderSkillsTacticsData(float pAlpha);
    void RenderSkillsDescription(float pAlpha);
    void RenderSkillsPurchase();
    void RenderSkillsDetails();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


