//--Base
#include "LuaContextOption.h"

//--Classes
#include "Actor.h"
#include "PlayerPony.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
LuaContextOption::LuaContextOption()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_CONTEXTOPTION;

    //--[LuaContextOption]
    //--System
    mControllerScript = NULL;
    mLocalName = InitializeString("Unnamed Option");
}
LuaContextOption::~LuaContextOption()
{
    free(mControllerScript);
    free(mLocalName);
}

//--[Public Statics]
bool LuaContextOption::xIsLegalTarget = false;
Actor *LuaContextOption::xrCaller = NULL;
RootEntity *LuaContextOption::xrTarget = NULL;

//====================================== Property Queries =========================================
char *LuaContextOption::GetName()
{
    return mLocalName;
}

//========================================= Manipulators ==========================================
void LuaContextOption::SetControlScript(const char *pScript)
{
    //--Set.
    ResetString(mControllerScript, pScript);

    //--If the script is legal, run it in initialization mode.
    if(mControllerScript)
    {
        DataLibrary *rDataLibrary = DataLibrary::Fetch();
        rDataLibrary->PushActiveEntity(this);
        LuaManager::Fetch()->ExecuteLuaFile(mControllerScript, 1, "N", CONTEXT_INITIALIZE);
        rDataLibrary->PopActiveEntity();
    }
}
void LuaContextOption::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}

//========================================= Core Methods ==========================================
bool LuaContextOption::IsLegalOnTarget(Actor *pCaller, RootEntity *pTarget)
{
    //--Checks if the context menu applies to the given object. Note that the object can be of several
    //  types, including Actors and InventoryItems.
    xIsLegalTarget = false;
    if(!mControllerScript || !pCaller || !pTarget) return false;

    //--First, store the caller and target pointers.
    xrCaller = pCaller;
    xrTarget = pTarget;

    //--Push, execute, pop.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity(this);
    LuaManager::Fetch()->ExecuteLuaFile(mControllerScript, 1, "N", CONTEXT_ISLEGALTARGET);
    rDataLibrary->PopActiveEntity();

    //--Return the result, which should have been flipped by the script.
    return xIsLegalTarget;
}
void LuaContextOption::ExecuteOnTarget(Actor *pCaller, RootEntity *pTarget)
{
    //--Once the player actually clicks the context menu option, executes it on the target.
    if(!mControllerScript || !pCaller || !pTarget) return;

    //--First, store the caller and target pointers.
    xrCaller = pCaller;
    xrTarget = pTarget;

    //--Push, execute, pop.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity(this);
    LuaManager::Fetch()->ExecuteLuaFile(mControllerScript, 1, "N", CONTEXT_EXECUTION);
    rDataLibrary->PopActiveEntity();
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void LuaContextOption::HookToLuaState(lua_State *pLuaState)
{
    /* CO_WipePlayerCommands()
       Removes all existing non-standard context menu options from the Player. */
    lua_register(pLuaState, "CO_WipePlayerCommands", &Hook_CO_WipePlayerCommands);

    /* CO_RegisterCommand(sPath)
       Creates and registers a LuaContextOption to the player given the requested properties. The
       initialization script does most of the work. */
    lua_register(pLuaState, "CO_RegisterCommand", &Hook_CO_RegisterCommand);

    /* CO_MarkTargetAsLegal()
       If used during the legality query part of the script, the context option in question will
       appear and be usable on the context menu. */
    lua_register(pLuaState, "CO_MarkTargetAsLegal", &Hook_CO_MarkTargetAsLegal);

    /* CO_PushCaller()
       Pushes the entity who is calling the command. This is the player 99.9% of the time. */
    lua_register(pLuaState, "CO_PushCaller", &Hook_CO_PushCaller);

    /* CO_PushTarget()
       Pushes the target of the context menu click. This can be any RootEntity, but is usually
       an Actor or an InventoryItem. */
    lua_register(pLuaState, "CO_PushTarget", &Hook_CO_PushTarget);

    /* CO_SetProperty("Name", sName)
       Sets the requested property in the Active LuaContextOption. */
    lua_register(pLuaState, "CO_SetProperty", &Hook_CO_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_CO_WipePlayerCommands(lua_State *L)
{
    //CO_WipePlayerCommands()
    PlayerPony::Fetch()->WipeContextCommands();
    return 0;
}
int Hook_CO_RegisterCommand(lua_State *L)
{
    //CO_RegisterCommand(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CO_RegisterCommand");

    PlayerPony::Fetch()->RegisterContextOption(lua_tostring(L, 1));
    return 0;
}
int Hook_CO_MarkTargetAsLegal(lua_State *L)
{
    //CO_MarkTargetAsLegal()
    LuaContextOption::xIsLegalTarget = true;
    return 0;
}
int Hook_CO_PushCaller(lua_State *L)
{
    //CO_PushCaller()
    DataLibrary::Fetch()->PushActiveEntity(LuaContextOption::xrCaller);
    return 0;
}
int Hook_CO_PushTarget(lua_State *L)
{
    //CO_PushTarget()
    DataLibrary::Fetch()->PushActiveEntity(LuaContextOption::xrTarget);
    return 0;
}
int Hook_CO_SetProperty(lua_State *L)
{
    //CO_SetProperty("Name", sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CO_SetProperty");

    //--Active object.
    LuaContextOption *rOption = (LuaContextOption *)DataLibrary::Fetch()->rActiveObject;
    if(!rOption || rOption->GetType() != POINTER_TYPE_CONTEXTOPTION) return LuaTypeError("CO_SetProperty", L);

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Sets the name, which is what will be displayed on the ContextMenu.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rOption->SetName(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("CO_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
