-- |[ ================================== Graphics: Portraits =================================== ]|
--Portraits that are universal between either all chapters, or for the Nowhere between chapters.

-- |[Load List]|
--Put everything in here on a "System Portraits" loading list. These resolve when convenient.
-- The "Nowhere" portraits resolve immediately.
local sEverythingList = "System Portraits"
fnCreateDelayedLoadList(sEverythingList)

-- |[ =================================== Loading Subroutine =================================== ]|
--Used for the automated list sections.
gbExtractListDebug = false
fnExtractList = function(sPrefix, sSuffix, sDLPath, saList)
	
	--Arg check.
	if(sPrefix == nil) then return end
	if(sSuffix == nil) then return end
	if(sDLPath == nil) then return end
	if(saList  == nil) then return end
	
	--Run across the list and extract everything.
	local i = 1
	while(saList[i] ~= nil) do
		
		--If the name is "SKIP", ignore it. This means it will be filled in later.
		if(saList[i] == "SKIP") then
		
		--Extract the image.
        elseif(sCharacterPrefix == nil) then
        
            --Diagnostic text:
            if(gbExtractListDebug == true) then
                io.write("Extract: " .. sPrefix .. saList[i] .. sSuffix .. " to " .. sDLPath .. saList[i] .. "\n")
            end
        
            --Execute.
			fnExtractDelayedBitmapToList(sEverythingList, sPrefix .. saList[i] .. sSuffix, sDLPath .. saList[i])
		end
		
		--Next.
		i = i + 1
	end
end

-- |[ =================================== Dialogue Portraits =================================== ]|
-- |[ ========================================= Actors ========================================= ]|
--Rilmani.
fnCreateDialogueActor("Septima", "Voice|Septima", "Root/Images/Portraits/Septima/", true, {"Rilmani"}, {"Neutral", "NeutralUp"})
fnCreateDialogueActor("Maram",   "Voice|Maram",   "Root/Images/Portraits/Maram/",   true, {"Rilmani"}, {"Neutral"})

-- |[Saving Images Out]|
--Sample code.
--DL_SaveBitmapToDrive("Root/Images/Portraits/Maram/Neutral", "Maram.png")
