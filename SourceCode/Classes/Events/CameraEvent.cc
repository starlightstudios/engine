//--Base
#include "CameraEvent.h"

//--Classes
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
//--Definitions
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "EntityManager.h"

#define DEFAULT_MOVE_SPEED 5.0f
#define MINIMUM_MOVE_SPEED 1.0f

///========================================== System ==============================================
CameraEvent::CameraEvent()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_EVENT_CAMERA;

    //--[RootEvent]
    //--System
    //--Script Execution
    //--Data Storage

    //--[CameraEvent]
    //--System
    mIsComplete = false;
    mInstructionType = CE_TYPE_NONE;

    //--Speeds
    mMaxMoveSpeed = DEFAULT_MOVE_SPEED;

    //--Data
    memset(&Data, 0, sizeof(Data));
}
CameraEvent::~CameraEvent()
{
}

///===================================== Property Queries =========================================
bool CameraEvent::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_EVENT_ROOT) return true;
    if(pType == POINTER_TYPE_EVENT_CAMERA) return true;
    return false;
}

///======================================== Manipulators ==========================================
void CameraEvent::SetMaxMoveSpeed(float pAmount)
{
    mMaxMoveSpeed = pAmount;
    if(mMaxMoveSpeed < MINIMUM_MOVE_SPEED) mMaxMoveSpeed = MINIMUM_MOVE_SPEED;
}
void CameraEvent::SetFocusActorByID(uint32_t pID)
{
    mInstructionType = CE_TYPE_FOCUS_ACTOR;
    Data.FocusActor.mActorID = pID;
}
void CameraEvent::SetFocusActorByName(const char *pName)
{
    //--Attempt to locate the actor. Fails with a message if the TilemapActor is not found.
    mInstructionType = CE_TYPE_NONE;

    //--Locate the subject.
    RootEntity *rPotentialSubject = EntityManager::Fetch()->GetEntity(pName);
    if(!rPotentialSubject || !rPotentialSubject->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        DebugManager::ForcePrint("CameraEvent:SetFocusActorByName - Error, unable to locate TilemapActor %s.\n", pName);
        return;
    }

    //--Set flags.
    mInstructionType = CE_TYPE_FOCUS_ACTOR;
    Data.FocusActor.mActorID = rPotentialSubject->GetID();
}
void CameraEvent::SetFocusPoint(float pX, float pY)
{
    mInstructionType = CE_TYPE_FOCUS_POINT;
    Data.FocusPoint.mPointX = pX;
    Data.FocusPoint.mPointY = pY;
}

///======================================== Core Methods ==========================================
bool CameraEvent::MoveCameraTowards(TwoDimensionReal &sSubject, TwoDimensionReal pTarget)
{
    //--Subroutine, handles common movement behavior. Moves two TDR structures towards each other as
    //  dictated by the max speed.
    //--Returns true if the function moved the subject exactly onto the target.
    float tAngle = atan2f(pTarget.mTop - sSubject.mTop, pTarget.mLft - sSubject.mLft);
    float tDistance = GetPlanarDistance(pTarget.mLft, pTarget.mTop, sSubject.mLft, sSubject.mTop);

    //--Close to the target, so move exactly to it and finish.
    if(tDistance <= mMaxMoveSpeed)
    {
        memcpy(&sSubject, &pTarget, sizeof(TwoDimensionReal));
        mIsComplete = true;
        return true;
    }

    //--Move towards.
    sSubject.mLft = sSubject.mLft + (cosf(tAngle) * mMaxMoveSpeed);
    sSubject.mRgt = sSubject.mRgt + (cosf(tAngle) * mMaxMoveSpeed);
    sSubject.mTop = sSubject.mTop + (sinf(tAngle) * mMaxMoveSpeed);
    sSubject.mBot = sSubject.mBot + (sinf(tAngle) * mMaxMoveSpeed);
    sSubject.mXCenter = sSubject.mXCenter + (sinf(tAngle) * mMaxMoveSpeed);
    sSubject.mYCenter = sSubject.mYCenter + (sinf(tAngle) * mMaxMoveSpeed);
    return false;
}
bool CameraEvent::IsComplete()
{
    //--Returns if the event is complete. Malformed objects always complete immediately.
    if(mInstructionType == CE_TYPE_NONE) return true;

    //--Otherwise, return the stored completion state.
    return mIsComplete;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void CameraEvent::Update()
{
    //--Update, store completion state.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) { mIsComplete = true; return; }

    //--Always activate locking if it wasn't active already.
    rActiveLevel->SetCameraLocking(true);

    //--Updates need the Camera's dimensions.
    TwoDimensionReal tCameraDimensions = rActiveLevel->GetCameraDimensions();

    //--Focusing on an Actor.
    if(mInstructionType == CE_TYPE_FOCUS_ACTOR)
    {
        //--Unlock the level.
        rActiveLevel->SetCameraFollowTarget(0);

        //--Locate our focus subject. Complete immediately if they don't exist.
        TilemapActor *rSubject = (TilemapActor *)EntityManager::Fetch()->GetEntityI(Data.FocusActor.mActorID);
        if(!rSubject || !rSubject->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            if(Data.FocusActor.mActorID != 0) fprintf(stderr, "Unable to find actor with ID: %i\n", Data.FocusActor.mActorID);
            mIsComplete = true;
            return;
        }

        //--Find the point where the camera would be, ideally, on them.
        TwoDimensionReal tIdealPosition = rActiveLevel->GetIdealCameraPosition(rSubject->GetWorldX(), rSubject->GetWorldY());

        //--Completion case. This will modify the Camera's dimensions even if it returns false!
        if(MoveCameraTowards(tCameraDimensions, tIdealPosition))
        {
            mIsComplete = true;
            rActiveLevel->SetCameraPosition(tIdealPosition);
            rActiveLevel->SetCameraFollowTarget(rSubject->GetID());
        }
        //--Incomplete.
        else
        {
            rActiveLevel->SetCameraPosition(tCameraDimensions);
        }
    }
    //--Focusing on a point.
    else if(mInstructionType == CE_TYPE_FOCUS_POINT)
    {
        //--Move the Camera towards the provided point. The point will be the center.
        TwoDimensionReal tIdealPosition = rActiveLevel->GetIdealCameraPosition(Data.FocusPoint.mPointX, Data.FocusPoint.mPointY);
        rActiveLevel->SetCameraFollowTarget(0);

        //--Completion case. This will modify the Camera's dimensions even if it returns false!
        if(MoveCameraTowards(tCameraDimensions, tIdealPosition))
        {
            mIsComplete = true;
            rActiveLevel->SetCameraPosition(tIdealPosition);
            rActiveLevel->SetCameraFollowTarget(0);
        }
        //--Incomplete.
        else
        {
            rActiveLevel->SetCameraPosition(tCameraDimensions);
        }
    }
    //--Unhandled instruction, always completes instantly.
    else
    {
        mIsComplete = true;
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void CameraEvent::HookToLuaState(lua_State *pLuaState)
{
    /* CameraEvent_SetProperty("Max Move Speed", fAmount)
       CameraEvent_SetProperty("Focus Actor ID", iActorID)
       CameraEvent_SetProperty("Focus Actor Name", sActorName)
       CameraEvent_SetProperty("Focus Position", fX, fY)
       Sets the given property in the active RootEvent or derived type. */
    lua_register(pLuaState, "CameraEvent_SetProperty", &Hook_CameraEvent_SetProperty);

    /* CameraEvent_GetProperty("Dummy") (1 integer)
       Returns the requested property from the active RootEvent or derived type. */
    lua_register(pLuaState, "CameraEvent_GetProperty", &Hook_CameraEvent_GetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CameraEvent_SetProperty(lua_State *L)
{
    //CameraEvent_SetProperty("Max Move Speed", fAmount)
    //CameraEvent_SetProperty("Focus Actor ID", iActorID)
    //CameraEvent_SetProperty("Focus Actor Name", sActorName)
    //CameraEvent_SetProperty("Focus Position", fX, fY)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CameraEvent_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Instantiated Cases]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    CameraEvent *rEvent = (CameraEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_CAMERA)) return LuaTypeError("CameraEvent_SetProperty", L);

    //--Maximum movement speed of the camera event.
    if(!strcasecmp(rSwitchType, "Max Move Speed") && tArgs == 2)
    {
        rEvent->SetMaxMoveSpeed(lua_tonumber(L, 2));
    }
    //--Move the Camera to the given Actor and then finish the event.
    else if(!strcasecmp(rSwitchType, "Focus Actor ID") && tArgs == 2)
    {
        rEvent->SetFocusActorByID(lua_tointeger(L, 2));
    }
    //--Focus on an Actor, located by their name.
    else if(!strcasecmp(rSwitchType, "Focus Actor Name") && tArgs == 2)
    {
        rEvent->SetFocusActorByName(lua_tostring(L, 2));
    }
    //--Focus on a given position.
    else if(!strcasecmp(rSwitchType, "Focus Position") && tArgs == 3)
    {
        rEvent->SetFocusPoint(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Error.
    else
    {
        LuaPropertyError("CameraEvent_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_CameraEvent_GetProperty(lua_State *L)
{
    //CameraEvent_GetProperty("Dummy") (1 integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CameraEvent_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    CameraEvent *rEvent = (CameraEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_CAMERA)) return LuaTypeError("CameraEvent_GetProperty", L);

    //--Dummy.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("CameraEvent_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
