//--Base
#include "CarnationCombat.h"

//--Classes
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///========================================= Functions ============================================
void CarnationCombat::HandleApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString)
{
    ///--[Documentation and Setup]
    //--Given a string, finds the target and then applies the string's effect to that target. Note that
    //  the originator ID can legally be 0 in some cases.
    //--This is mostly the same as the base version, except it deliberately does not spawn text indicators,
    //  animations, or sounds. That is instead done by the text parser, such that everything is in line
    //  with the text crawl.
    if(!pString || !pTargetID) return;

    ///--[Target Acquisition]
    //--First, locate the target. It can be either in the party or the enemy roster.
    AdvCombatEntity *rApplyEntity = NULL;

    //--Search the player's party. Note we search the *entire* roster, which means party members
    //  can be affected if they're not even in the battle!
    AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pTargetID)
        {
            rApplyEntity = rCheckEntity;
            mPartyRoster->PopIterator();
            break;
        }
        rCheckEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }

    //--If not found in the player's roster, search the enemy roster.
    rCheckEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pTargetID)
        {
            rApplyEntity = rCheckEntity;
            mEnemyRoster->PopIterator();
            break;
        }
        rCheckEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--If there was no target, fail here.
    if(!rApplyEntity) return;

    ///--[String Splitting]
    //--Break the string using the Subdivide algorithm.
    StarLinkedList *tStringList = Subdivide::SubdivideStringToList(pString, "|");

    //--Debug.
    if(false)
    {
        fprintf(stderr, "String Break Report %s\n", pString);
        fprintf(stderr, " %i strings.\n", tStringList->GetListSize());
        for(int i = 0; i < tStringList->GetListSize(); i ++)
        {
            fprintf(stderr, " %s\n", (char *)tStringList->GetElementBySlot(i));
        }
    }

    ///--[Effect Application]
    //--0th part of the string is what it does. How many additional parts are needed is handled per-type.
    int tArgs = tStringList->GetListSize();
    const char *rSwitchType = (char *)tStringList->GetElementBySlot(0);
    if(!rSwitchType) return;

    //--HP Damage:
    if(!strcasecmp(rSwitchType, "Damage") && tArgs >= 2)
    {
        //--Arg check.
        int tDamage = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tDamage < 1) return;

        //--Priorities. By default, it's Shield->Adrenaline->Health.
        int tHealthPriority = 1;
        int tAdrenalinePriority = 2;
        int tShieldPriority = 3;

        //--Run optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Priority-Shield. Integer value, 0 means don't hit shields.
            if(!strcasecmp(rSubSwitchType, "PrioritySh") && tSubArgs == 2)
            {
                const char *rString = (const char *)tArgumentList->GetElementBySlot(1);
                tShieldPriority = atoi(rString);
            }
            //--Priority-Health.
            else if(!strcasecmp(rSubSwitchType, "PriorityHe") && tSubArgs == 2)
            {
                const char *rString = (const char *)tArgumentList->GetElementBySlot(1);
                tHealthPriority = atoi(rString);
            }
            //--Priority-Adrenaline.
            else if(!strcasecmp(rSubSwitchType, "PriorityAd") && tSubArgs == 2)
            {
                const char *rString = (const char *)tArgumentList->GetElementBySlot(1);
                tAdrenalinePriority = atoi(rString);
            }
            //--Glancing blow. Spawns an extra animation.
            else if(!strcasecmp(rSubSwitchType, "Glance") && tSubArgs == 1)
            {
                //--Does nothing in Carnation.
            }
            //--Critical Strike. Adds a "!" to the end of the damage, but is otherwise the same.
            else if(!strcasecmp(rSubSwitchType, "Critical") && tSubArgs == 1)
            {
                //--Does nothing in Carnation.
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Damage' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--Unloseable: If this is the case, set the damage such that the target never goes below 1 HP.
        //  This can zero off the damage!
        if(mIsUnloseable && IsEntityInPlayerParty(rApplyEntity))
        {
            //--Clamp.
            int tTargetHP = rApplyEntity->GetHealth();
            if(tDamage >= tTargetHP) tDamage = tTargetHP - 1;
            if(tDamage < 0) tDamage = 0;
        }
        //--Unwinnable: Enemies cannot be defeated.
        else if(mIsUnwinnable && !IsEntityInPlayerParty(rApplyEntity))
        {
            //--Clamp.
            int tTargetHP = rApplyEntity->GetHealth();
            if(tDamage >= tTargetHP) tDamage = tTargetHP - 1;
            if(tDamage < 0) tDamage = 0;
        }

        //--Damage the entity.
        rApplyEntity->InflictDamage(tDamage, tHealthPriority, tAdrenalinePriority, tShieldPriority);
        rApplyEntity->Shake();
    }
    //--Influence Damage or Healing
    else if(!strcasecmp(rSwitchType, "Influence") && tArgs >= 2)
    {
        //--Arg check.
        int tInfluence = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tInfluence < 1) return;

        //--Apply.
        rApplyEntity->SetInfluence(rApplyEntity->GetInfluence() + tInfluence);
    }
    //--Healing!
    else if(!strcasecmp(rSwitchType, "Healing") && tArgs == 2)
    {
        //--Arg check.
        int tHealing = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tHealing < 1) return;

        //--"Damage" the entity.
        rApplyEntity->SetHealth(rApplyEntity->GetHealth() + tHealing);
    }
    //--Shields!
    else if(!strcasecmp(rSwitchType, "Shields") && tArgs == 2)
    {
        //--Arg check.
        int tShieldsValue = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tShieldsValue < 1) return;

        //--Damage the entity.
        rApplyEntity->SetShields(tShieldsValue);
    }
    //--Gain MP!
    else if(!strcasecmp(rSwitchType, "MPGain") && tArgs == 2)
    {
        //--Arg check.
        int tMPToGain = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tMPToGain < 1) return;

        //--Apply MP gain.
        rApplyEntity->SetMagic(rApplyEntity->GetMagic() + tMPToGain);
    }
    //--Adrenaline!
    else if(!strcasecmp(rSwitchType, "Adrenaline") && tArgs == 2)
    {
        //--Arg check.
        int tAdrenaline = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tAdrenaline < 1) return;

        //--Damage the entity.
        rApplyEntity->SetAdrenaline(rApplyEntity->GetAdrenaline() + tAdrenaline);
    }
    //--Stun Damage. Sets to the value, does not increment it.
    else if(!strcasecmp(rSwitchType, "Stun") && tArgs >= 2)
    {
        //--Stun damage amount.
        int tStunDamage = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tStunDamage < 1) return;

        //--Apply
        rApplyEntity->SetStun(tStunDamage);
    }
    //--Text. Spawns a text package.
    else if(!strcasecmp(rSwitchType, "Text") && tArgs >= 2)
    {
        //--Does nothing in Carnation.
    }
    //--Play sound effect:
    else if(!strcasecmp(rSwitchType, "Play Sound") && tArgs == 2)
    {
        //--Does nothing in Carnation.
    }
    //--Create an animation.
    else if(!strcasecmp(rSwitchType, "Create Animation") && tArgs >= 3)
    {
        //--Does nothing in Carnation.
    }
    //--Flash black twice.
    else if(!strcasecmp(rSwitchType, "Flash Black") && tArgs == 1)
    {
        rApplyEntity->FlashBlack();
    }
    //--Create a title at the top of the screen.
    else if(!strcasecmp(rSwitchType, "Title") && tArgs >= 2)
    {
        //--Does nothing in Carnation.
    }
    //--Effect. Applies an effect to the ID'd target.
    else if(!strcasecmp(rSwitchType, "Effect") && tArgs >= 2)
    {
        //--Get the script to call.
        const char *rScriptPath = (const char *)tStringList->GetElementBySlot(1);

        //--Create and register the effect.
        AdvCombatEffect *nEffect = new AdvCombatEffect();
        RegisterEffect(nEffect);
        nEffect->AddTargetByID(pTargetID);

        //--Variables.
        bool tUsesPrototype = false;
        bool tIsCritical = false;
        float tSeverity = 1.0f;

        //--Run optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Originator. The ID of the entity that created the effect.
            if(!strcasecmp(rSubSwitchType, "Originator") && tSubArgs == 2)
            {
                nEffect->SetOriginatorByID(atoi((const char *)tArgumentList->GetElementBySlot(1)));
            }
            //--Severity. Affects the power of the effect. Default is 1.0f.
            else if(!strcasecmp(rSubSwitchType, "Severity") && tSubArgs == 2)
            {
                tSeverity = atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Store ID. Stores the newly created AdvCombatEffect's ID in the DataLibrary so the ability script can get it.
            else if(!strcasecmp(rSubSwitchType, "StoreID") && tSubArgs == 2)
            {
                //--Get the provided ID.
                int tStoreID = atoi((const char *)tArgumentList->GetElementBySlot(1));

                //--Locate the DataLibrary entry.
                char *tBuffer = InitializeString("Root/Variables/Combat/%i/iManagedEffectID", tStoreID);
                SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry(tBuffer);
                if(rVariable) rVariable->mNumeric = (float)nEffect->GetID();
                free(tBuffer);
            }
            //--Prototype, the effect is created using a prototype stored on a global list.
            else if(!strcasecmp(rSubSwitchType, "Prototype") && tSubArgs == 2)
            {
                //--Store it internally.
                tUsesPrototype = true;
                nEffect->SetGlobalPrototypeName((const char *)tArgumentList->GetElementBySlot(1));

                //--Also set the static global value for the creation function.
                ResetString(AdvCombatEffect::xGlobalEffectName, (const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Critical strike. If using a prototype, adds ".Crit" onto the end of it.
            else if(!strcasecmp(rSubSwitchType, "Critical") && tSubArgs == 1)
            {
                tIsCritical = true;
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Effect' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--If the attack uses a prototype, and was a critical hit, then we append ".Crit" onto the end of the name
        //  of the prototype. The script will then look for a critical hit version of the effect. If it does not
        //  exist then the base version is used.
        if(tIsCritical && tUsesPrototype)
        {
            char *tTempString = InitializeString("%s.Crit", AdvCombatEffect::xGlobalEffectName);
            ResetString(AdvCombatEffect::xGlobalEffectName, tTempString);
            free(tTempString);
        }

        //--Call the setup script.
        LuaManager::Fetch()->PushExecPop(nEffect, rScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_CREATE, "N", tSeverity);

        //--Order the Effect to apply stat changes to all entities on its target list. The target list consists of
        //  the target we set earlier but can be modified by the creation script.
        nEffect->ApplyStatsToTargets();
    }
    //--Remove Effect. Removes the effect with the given ID.
    else if(!strcasecmp(rSwitchType, "Remove Effect") && tArgs >= 2)
    {
        //--ID.
        uint32_t tEffectID = atoi((const char *)tStringList->GetElementBySlot(1));

        //--Remove it.
        MarkEffectForRemoval(tEffectID);
    }
    //--Changes the party affiliation of the target entity. Passing AC_PARTY_GROUP_NONE for the party moves them to the roster.
    else if(!strcasecmp(rSwitchType, "Change Party") && tArgs >= 2)
    {
        //--Change party.
        int tPartyGroup = atoi((const char *)tStringList->GetElementBySlot(1));
        MoveEntityToPartyGroup(rApplyEntity->GetID(), tPartyGroup);

        //--Setup.
        float cRenderX = ComputeIdealX(true, true, 0, 1, rApplyEntity->GetCombatPortrait()); //Offscreen
        float cRenderY = ComputeIdealY(rApplyEntity);

        //--Set.
        rApplyEntity->SetIdealPosition(cRenderX, cRenderY);
        rApplyEntity->MoveToIdealPosition(0);

        //--Recompute all positions.
        PositionEnemies();
    }
    //--Orders the entity to change position. Second argument is type, third is a code or a number.
    else if(!strcasecmp(rSwitchType, "Position") && tArgs >= 2)
    {
        //--Setup.
        int tTicks = ADVCOMBAT_STD_MOVE_TICKS;
        float tPosition = 0.0f;

        //--Get type. It's a single-integer code.
        int tPositionType = atoi((const char *)tStringList->GetElementBySlot(1));

        //--Optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--How many ticks this will take.
            if(!strcasecmp(rSubSwitchType, "Ticks") && tSubArgs == 2)
            {
                tTicks = atoi((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Position. Used for AC_POSCODE_DIRECT, specifies exact X position.
            else if(!strcasecmp(rSubSwitchType, "Pos") && tSubArgs == 2)
            {
                tPosition = atoi((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Position' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--Position with a direct X code. This will be the center point.
        if(tPositionType == AC_POSCODE_DIRECT)
        {
            rApplyEntity->SetIdealPositionX(tPosition);
            rApplyEntity->MoveToIdealPosition(tTicks);
        }
        //--Position as a member of the player's party. The player party will move as needed.
        else if(tPositionType == AC_POSCODE_PLAYER_PARTY)
        {
            //--If the entry is already on the party list, just put the party onscreen.
            if(mrCombatParty->IsElementOnList(rApplyEntity))
            {
                PositionListOnscreen(true, tTicks, mrCombatParty);
            }
            //--Otherwise, add them to a temporary list with the combat party and position that onscreen.
            else
            {
                //--Create.
                StarLinkedList *trTempList = new StarLinkedList(false);

                //--Clone.
                void *rPtr = mrCombatParty->PushIterator();
                while(rPtr)
                {
                    trTempList->AddElementAsTail("X", rPtr);
                    rPtr = mrCombatParty->AutoIterate();
                }

                //--Apply.
                trTempList->AddElementAsTail("X", rApplyEntity);
                PositionListOnscreen(true, tTicks, trTempList);

                //--Clean.
                delete trTempList;
            }
        }
        //--Position as a member of the opposing party.
        else if(tPositionType == AC_POSCODE_ENEMY_PARTY)
        {
            //--If the entry is already on the enemy list, just put the enemy onscreen.
            if(mrEnemyCombatParty->IsElementOnList(rApplyEntity))
            {
                PositionListOnscreen(false, tTicks, mrEnemyCombatParty);
            }
            //--Otherwise, add them to a temporary list with the combat party and position that onscreen.
            else
            {
                //--Create.
                StarLinkedList *trTempList = new StarLinkedList(false);

                //--Clone.
                void *rPtr = mrEnemyCombatParty->PushIterator();
                while(rPtr)
                {
                    trTempList->AddElementAsTail("X", rPtr);
                    rPtr = mrEnemyCombatParty->AutoIterate();
                }

                //--Apply.
                trTempList->AddElementAsTail("X", rApplyEntity);
                PositionListOnscreen(false, tTicks, trTempList);

                //--Clean.
                delete trTempList;
            }
        }
    }
    //--Executes the named script.
    else if(!strcasecmp(rSwitchType, "Run Script") && tArgs >= 2)
    {
        //--Script path.
        const char *rScriptPath = (const char *)tStringList->GetElementBySlot(1);

        //--No additional arguments:
        if(tArgs == 2)
        {
            LuaManager::Fetch()->ExecuteLuaFile(rScriptPath);
        }
        //--Arguments are present.
        else
        {
            //--Number of arguments expected is 1 for each substring.
            int tExpectedArgs = tArgs - 2;
            LuaManager *rLuaManager = LuaManager::Fetch();
            rLuaManager->SetArgumentListSize(tExpectedArgs);
            for(int i = 0; i < tExpectedArgs; i ++)
            {
                //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
                StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i+2), ":");
                int tSubArgs = tArgumentList->GetListSize();
                const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

                //--Number:
                if(!strcasecmp(rSubSwitchType, "N") && tSubArgs == 2)
                {
                    rLuaManager->AddArgument(atoi((const char *)tArgumentList->GetElementBySlot(1)));
                }
                //--String:
                else if(!strcasecmp(rSubSwitchType, "S") && tSubArgs == 2)
                {
                    rLuaManager->AddArgument((const char *)tArgumentList->GetElementBySlot(1));
                }
                //--Error.
                else
                {
                    DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Position' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
                }

                //--Clean.
                delete tArgumentList;
            }

            //--Execute.
            LuaManager::Fetch()->ExecuteLuaFileBypass(rScriptPath);
        }
    }
    //--Flashes up to white. Is a toggle, be sure to unset later!
    else if(!strcasecmp(rSwitchType, "FlashWhite") && tArgs >= 1)
    {
        rApplyEntity->SetFlashingWhite(true);
    }
    //--Unsets the white flash.
    else if(!strcasecmp(rSwitchType, "UnflashWhite") && tArgs >= 1)
    {
        rApplyEntity->SetFlashingWhite(false);
    }
    //--Change jobs in combat.
    else if(!strcasecmp(rSwitchType, "JobChange") && tArgs >= 2)
    {
        const char *rJobName = (const char *)tStringList->GetElementBySlot(1);
        rApplyEntity->SetActiveJob(rJobName);
    }
    //--Stun Damage. Does nothing in Carnation.
    else if(!strcasecmp(rSwitchType, "Stun") && tArgs >= 2)
    {
    }
    //--Stun display. Does nothing in Carnation.
    else if(!strcasecmp(rSwitchType, "Stun Display"))
    {
    }
    //--No specific application. This is used for AI scripts. The AI can see the arguments and respond to them, but the C++
    //  code does not need to print an error for this.
    else if(!strcasecmp(rSwitchType, "AI_APPLICATION") && tArgs >= 1)
    {
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, no application type %s found with %i arguments.\n", rSwitchType, tArgs);
    }

    //--Clean up.
    delete tStringList;
}

///===================================== CL App Pack Case =========================================
void CarnationCombat::HandleCLAppPack(CLAppPack *pPackage)
{
    ///--[Documentation]
    //--Handles a CLAppPack, which is an application-like entry. It is a string but it also has an
    //  originator and target ID in it. These are meant to fire in time with the text box.
    if(!pPackage || !pPackage->mString) return;

    ///--[Fast-Access Pointers]
    //--Resolve the originator. If it's a CarnationCombatEntity, get that version of the pointer.
    AdvCombatEntity *rOriginator = GetEntityByID(pPackage->mOriginatorID);
    CarnationCombatEntity *rCarnOriginator = NULL;
    if(rOriginator && rOriginator->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) rCarnOriginator = (CarnationCombatEntity *)rOriginator;

    //--Resolve the target. Target is optional. If it's a CarnationCombatEntity, get that version of the pointer.
    AdvCombatEntity *rTarget = GetEntityByID(pPackage->mTargetID);
    CarnationCombatEntity *rCarnTarget = NULL;
    if(rTarget && rTarget->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) rCarnTarget = (CarnationCombatEntity *)rTarget;

    //--Debug.
    if(false)
    {
        fprintf(stderr, "Handling CL App Pack %s\n", pPackage->mString);
        fprintf(stderr, " Originator: %i %p %p\n", pPackage->mOriginatorID, rOriginator, rCarnOriginator);
        if(rOriginator) fprintf(stderr, "  %s\n", rOriginator->GetName());
        fprintf(stderr, " Target: %i %p %p\n", pPackage->mTargetID, rTarget, rCarnTarget);
        if(rTarget) fprintf(stderr, "  %s\n", rTarget->GetName());
    }

    ///--[Timing]
    //--Player has to push a key to advance the text when it reaches the end of the current line.
    if(!strcasecmp(pPackage->mString, "[WAIT]"))
    {
        mTextTimerWaitForKeypress = true;
    }
    //--Pause briefly at the end of a line. Is normally done automatically, some intercepts may want a line pause.
    else if(!strcasecmp(pPackage->mString, "[PAUSE]"))
    {
        mTextPauseTimer = ResolvePauseTime();
    }
    ///--[Sound Effects]
    //--Swing, uses the swing sound effect. Requires a CarnationCombatEntity.
    else if(!strcasecmp(pPackage->mString, "[SWING]"))
    {
        if(rCarnOriginator) AudioManager::Fetch()->PlaySound(rCarnOriginator->GetSoundSwing());
    }
    //--Spell, uses the spell sound effect. Requires a CarnationCombatEntity.
    else if(!strcasecmp(pPackage->mString, "[SPELL]"))
    {
        if(rCarnOriginator) AudioManager::Fetch()->PlaySound(rCarnOriginator->GetSoundSpell());
    }
    //--Miss sound effect. Requires a CarnationCombatEntity.
    else if(!strcasecmp(pPackage->mString, "[MISS]"))
    {
        if(rCarnOriginator) AudioManager::Fetch()->PlaySound(rCarnOriginator->GetSoundMiss());
    }
    //--Hit sound effect. Requires a CarnationCombatEntity.
    else if(!strcasecmp(pPackage->mString, "[HIT]"))
    {
        if(rCarnOriginator) AudioManager::Fetch()->PlaySound(rCarnOriginator->GetSoundHit());
    }
    ///--[Injury / Healing]
    //--Triggers the injury timer, causing the target to flinch.
    else if(!strcasecmp(pPackage->mString, "[INJURE]"))
    {
        if(rCarnTarget) rCarnTarget->Injure();
    }
    //--Unsets the HP lock so healing can take place.
    else if(!strcasecmp(pPackage->mString, "[HEAL]"))
    {
        if(rCarnOriginator) AudioManager::Fetch()->PlaySound(rCarnOriginator->GetSoundHeal());
        if(rCarnTarget) rCarnTarget->Heal();
    }
    //--"Quietly" injure the target, unsetting the scroll hold but not making SFX.
    else if(!strcasecmp(pPackage->mString, "[QUIETINJURE]"))
    {
        if(rCarnTarget) rCarnTarget->SetHPScrollHold(false);
    }
    ///--[Attack]
    //--Causes the entity to flash black when they attack. Only hostile entities do this.
    else if(!strcasecmp(pPackage->mString, "[ATTACK]"))
    {
        if(rCarnOriginator) rCarnOriginator->Attack();
    }
    //--Clear the pending knockout flag, which allows the entity in question to die.
    else if(!strcasecmp(pPackage->mString, "[KNOCKOUT]"))
    {
        AudioManager::Fetch()->PlaySound("Combat|MonsterDie");
        if(rCarnTarget) rCarnTarget->ClearKnockoutBlock();
    }
    //--Used for party members who got KO'd.
    else if(!strcasecmp(pPackage->mString, "[COLLAPSE]"))
    {
        AudioManager::Fetch()->PlaySound("Combat|MonsterDie");
        if(rCarnTarget) rCarnTarget->ClearKnockoutBlock();
    }
    ///--[Animations]
    //--If called, the entity will run their animation refresh script to modify which
    //  layers are showing.
    else if(!strcasecmp(pPackage->mString, "[ORIGANIMREFRESH]"))
    {
        if(rCarnOriginator) rCarnOriginator->TripRenderRefresh();
    }
    //--Target version.
    else if(!strcasecmp(pPackage->mString, "[TARGANIMREFRESH]"))
    {
        if(rCarnTarget) rCarnTarget->TripRenderRefresh();
    }
    //--Edit a flag local to the originator. This requires their Unique ID.
    else if(!strncasecmp(pPackage->mString, "[FLAG|", 6))
    {
        if(rCarnOriginator) rCarnOriginator->EditFlag(&pPackage->mString[6]);
    }
    ///--[General Sound Effects]
    //--If the start of the tag is [SFX| then this is playing a sound effect independent of the
    //  character that is performing the action.
    else if(!strncasecmp(pPackage->mString, "[SFX|", 5))
    {
        //--Remove the ending ] and the rest if the name of the sound effect.
        char tBuf[128];
        memset(tBuf, 0, 128);
        strncpy(tBuf, &pPackage->mString[5], (int)strlen(pPackage->mString) - 6);

        //--Play.
        AudioManager::Fetch()->PlaySound(tBuf);
    }
    ///--[Level Up Statistics]
    else if(!strncasecmp(pPackage->mString, "[SHOWLEVELUP|", 13))
    {
        //--Remove the ending ] and the rest if the name of the package.
        char tBuf[128];
        memset(tBuf, 0, 128);
        strncpy(tBuf, &pPackage->mString[13], (int)strlen(pPackage->mString) - 14);

        //--Play.
        ResetString(mShowingLevelUpPack, tBuf);
    }
    ///--[Error]
    //--The string wasn't handled.
    else
    {

    }
}
