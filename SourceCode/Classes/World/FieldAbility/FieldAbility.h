///============================= FieldAbility =================================
//--Stores an ability, usually executed by a script. These can be used in the field
//  and can have unique interactions with trigger scripts and NPCs.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define FIELDABILITY_EXEC_CREATE 0
#define FIELDABILITY_EXEC_RUN 1
#define FIELDABILITY_EXEC_RUNWHILECOOLING 2
#define FIELDABILITY_EXEC_RUNCANCEL 3
#define FIELDABILITY_EXEC_TOUCHENEMY 4
#define FIELDABILITY_EXEC_MODIFYACTOR 5

///========================================== Classes =============================================
class FieldAbility : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;
    char *mScriptPath;
    bool mCancelsEvenWhenInactive;

    //--Cooldown
    int mCooldownCur;
    int mCooldownMax;

    //--Display
    StarBitmap *rBackingImg;
    StarBitmap *rFrameImg;
    StarBitmap *rMainImg;
    int mDisplayStringsTotal;
    char **mDisplayStrings;

    protected:

    public:
    //--System
    FieldAbility();
    virtual ~FieldAbility();

    //--Public Variables
    //--Property Queries
    bool IsCooling();
    int GetCooldown();
    int GetCooldownMax();
    const char *GetInternalName();
    const char *GetDisplayName();
    int GetDisplayStringsTotal();
    const char *GetDisplayString(int pSlot);
    bool CancelsEvenWhenInactive();

    //--Manipulators
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetScriptPath(const char *pPath);
    void SetCooldownCur(int pTicks);
    void SetCooldownMax(int pTicks);
    void SetBackingImg(const char *pDLPath);
    void SetFrameImg(const char *pDLPath);
    void SetMainImg(const char *pDLPath);
    void AllocateDisplayStrings(int pTotal);
    void SetDisplayString(int pSlot, const char *pString);
    void SetCancelWhenInactiveFlag(bool pFlag);

    //--Core Methods
    void ActivateCooldown();
    void Execute(int pCode);
    void ExecuteOn(int pCode, void *pPtr);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void Render(float pX, float pY, int pControlSlot, int pStencilCode, float pAlpha);

    //--Pointer Routing
    StarBitmap *GetBackingImage();
    StarBitmap *GetFrameImage();
    StarBitmap *GetMainImage();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_FieldAbility_Create(lua_State *L);
int Hook_FieldAbility_GetProperty(lua_State *L);
int Hook_FieldAbility_SetProperty(lua_State *L);

