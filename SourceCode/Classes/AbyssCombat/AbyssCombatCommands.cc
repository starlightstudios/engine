//--Base
#include "AbyssCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"
#include "StarUIPiece.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CutsceneManager.h"

///========================================== System ==============================================
///--[CommandListEntry Functions]
void CommandListEntry::Initialize()
{
    mIsManuallyLocked = false;
    mGlowing = false;
    mDisplayName = NULL;
    mDisplayString = NULL;
    mQuantity = 0;
    mInventorySlot = -1;
    mIsCharmSkill = false;
    rSuffixImg = NULL;
    mIconsList = new StarLinkedList(false);
    mSublist = NULL;
    rAssociatedAbility = NULL;
}
void CommandListEntry::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    CommandListEntry *rPtr = (CommandListEntry *)pPtr;
    free(rPtr->mDisplayName);
    free(rPtr->mDisplayString);
    delete rPtr->mIconsList;
    delete rPtr->mSublist;
    free(pPtr);
}

///===================================== Property Queries =========================================
CommandListEntry *AbyssCombat::GetActiveCommand()
{
    if(mrCommandStack->GetListSize() < 1) return NULL;
    return (CommandListEntry *)mrCommandStack->GetHead();
}
bool AbyssCombat::DoesCommandExist(const char *pInternalName)
{
    //--If there is something on the stack:
    CommandListEntry *rStackHead = (CommandListEntry *)mrCommandStack->GetHead();
    if(rStackHead)
    {
        //--No sublist, stop.
        if(!rStackHead->mSublist) return false;

        //--Locate the element in question.
        void *rCheckEntry = rStackHead->mSublist->GetElementByName(pInternalName);
        return (rCheckEntry != NULL);
    }

    //--No stack, use the base list.
    void *rCheckEntry = mCommandList->GetElementByName(pInternalName);
    return (rCheckEntry != NULL);
}

///======================================= Manipulators ===========================================
void AbyssCombat::SetCommandColorFromEntry(CommandListEntry *pEntry, float pAlpha)
{
    ///--[Documentation]
    //--Given a command list entry, sets the global color mixer according to its properties.
    if(!pEntry) return;

    //--Default to white.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Always grey out if manually locked.
    if(pEntry->mIsManuallyLocked)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pAlpha);
    }
    //--Grey out.
    else if(pEntry->rAssociatedAbility && !pEntry->rAssociatedAbility->IsUsableNow())
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pAlpha);
    }
    //--Alternately, no ability, and sublist has no entries.
    else if(!pEntry->rAssociatedAbility && (!pEntry->mSublist || pEntry->mSublist->GetListSize() < 1))
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pAlpha);
    }
    //--Usable, or has no ability, and is glowing.
    else if(pEntry->mGlowing)
    {
        float cGlowPct = EasingFunction::SinusoidalInOut(mCommandGlowTimer / (float)ABYCOM_COMMAND_GLOW_TICKS);
        StarlightColor::SetMixer(1.0f - (0.5f * cGlowPct), 1.0f - (0.5f * cGlowPct), 0.5f + (0.5f * cGlowPct), pAlpha);
    }
}
void AbyssCombat::RegisterCommandListEntry(const char *pInternalName)
{
    ///--[Documentation]
    //--Registers a new command list entry. If no entry is active, then registers it to the base
    //  command list. Otherwise, registers it to the active command list, booting the SLL if needed.
    //--Implicitly pushes the new entry. Remember to pop it after setup.
    if(!pInternalName)
    {
        mrCommandStack->AddElementAsHead("X", mDummyEntry);
        return;
    }

    ///--[Execution]
    //--Registering any command activates the command window.
    mShowCommandWindow = true;
    mCommandScrollTimer = 0;
    mPlayerCommandBaseCursor = 0;

    //--Get the acting entity and store their portrait.
    rActingCharacterImg = NULL;
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(rActingEntity) rActingCharacterImg = rActingEntity->GetCombatPortrait();

    //--Create the entry.
    CommandListEntry *nEntry = (CommandListEntry *)starmemoryalloc(sizeof(CommandListEntry));
    nEntry->Initialize();

    //--Figure out what list to register it to.
    StarLinkedList *rRegisterList = mCommandList;
    CommandListEntry *rStackHead = (CommandListEntry *)mrCommandStack->GetHead();
    if(rStackHead)
    {
        //--If the stack head is the dummy, print a warning, and register to the base list.
        //  This is just to make sure the program doesn't crash.
        if(rStackHead == mDummyEntry)
        {
            fprintf(stderr, "Warning: Attempting to register command %s placed it on the dummy entry!\n", pInternalName);
        }
        //--Otherwise, boot the list if needed and register.
        else
        {
            if(!rStackHead->mSublist) rStackHead->mSublist = new StarLinkedList(true);
            rRegisterList = rStackHead->mSublist;
        }
    }

    //--Register it to that list.
    rRegisterList->AddElementAsTail(pInternalName, nEntry, &CommandListEntry::DeleteThis);

    //--Set it as the head.
    mrCommandStack->AddElementAsHead("X", nEntry);
}
void AbyssCombat::PushCommandListEntry(const char *pInternalName)
{
    ///--[Documentation]
    //--Pushes the named CommandListEntry as the head of the mrCommandStack if it exists on the current
    //  stack head, or if nothing is on the stack, the base list.
    if(!pInternalName || !strcasecmp(pInternalName, "DUMMY"))
    {
        mrCommandStack->AddElementAsHead("X", mDummyEntry);
        return;
    }

    ///--[Execution]
    //--If there is something on the stack:
    CommandListEntry *rStackHead = (CommandListEntry *)mrCommandStack->GetHead();
    if(rStackHead)
    {
        //--If the stack head is the dummy, print a warning, and push the dummy (again).
        //  This is just to make sure the program doesn't crash, and the stack is a constant size.
        if(rStackHead == mDummyEntry)
        {
            fprintf(stderr, "Warning: Attempting to push command %s, but the head was the dummy pointer.\n", pInternalName);
            mrCommandStack->AddElementAsHead("X", mDummyEntry);
            return;
        }
        //--If the list is not initialized, push the dummy.
        if(!rStackHead->mSublist)
        {
            fprintf(stderr, "Warning: Attempting to push command %s, but the stack head did not initialize its list yet.\n", pInternalName);
            mrCommandStack->AddElementAsHead("X", mDummyEntry);
            return;
        }

        //--Locate the element in question.
        CommandListEntry *rPushingEntry = (CommandListEntry *)rStackHead->mSublist->GetElementByName(pInternalName);

        //--Doesn't exist. Print warning, push the dummy.
        if(!rPushingEntry)
        {
            fprintf(stderr, "Warning: Attempting to push command %s, not found.\n", pInternalName);
            mrCommandStack->AddElementAsHead("X", mDummyEntry);
            return;
        }

        //--Entry exists. Push it.
        mrCommandStack->AddElementAsHead("X", rPushingEntry);
        return;
    }

    //--Nothing on the stack, use the base list.
    CommandListEntry *rPushingEntry = (CommandListEntry *)mCommandList->GetElementByName(pInternalName);

    //--Doesn't exist. Print warning, push the dummy.
    if(!rPushingEntry)
    {
        fprintf(stderr, "Warning: Attempting to push command %s from base list, not found.\n", pInternalName);
        mrCommandStack->AddElementAsHead("X", mDummyEntry);
        return;
    }

    //--Entry exists. Push it.
    mrCommandStack->AddElementAsHead("X", rPushingEntry);
}
void AbyssCombat::PopCommandList()
{
    ///--[Documentation]
    //--Pops the command list. Does not print a warning if nothing was on the list.
    mrCommandStack->DeleteHead();
}
void AbyssCombat::ClearCommandList()
{
    ///--[Documentation]
    //--Wipes the command list, deallocating everything on it.
    mrCommandStack->ClearList();
    mCommandList->ClearList();

    //--Implicitly resets the player's input as well.
    mShowCommandWindow = false;
    mCommandTimer = 0;
    mPlayerCommandCursor = 0;
    mPlayerCommandOffset = 0;
    mrPlayerCommandStack->ClearList();
}
CommandListEntry *AbyssCombat::ResolveStackHead(const char *pErrorFunction)
{
    ///--[Documentation]
    //--Worker function. Resolves the stack head, and checks if it exists or is the dummy. If the entry does not
    //  exist, or is the dummy entry, barks a warning and returns NULL.
    CommandListEntry *rStackHead = (CommandListEntry *)mrCommandStack->GetHead();
    if(!rStackHead)
    {
        fprintf(stderr, "%s - Warning: No entry on the stack.\n", pErrorFunction);
        return NULL;
    }
    //--Command is the dummy.
    else if(rStackHead == mDummyEntry)
    {
        fprintf(stderr, "%s - Warning: Entry is the dummy.\n", pErrorFunction);
        return NULL;
    }

    //--Return it.
    return rStackHead;
}
void AbyssCombat::SetCommandDisplayName(const char *pDisplayName)
{
    ///--[Documentation]
    //--Sets the display name for whatever the active command is. Does nothing if it's the dummy.
    if(!pDisplayName) return;

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetCommandDisplayName()");
    if(!rStackHead) return;

    ///--[Translation]
    //--Check for translations. If there is no Combat translation, check the Item translation as well.
    bool tPerformedTranslation = false;
    const char *rUseString = pDisplayName;
    StarTranslation *rCombatTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_COMBAT);
    StarTranslation *rItemTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_ITEMS);
    if(rCombatTranslation)
    {
        const char *rTranslatedString = rCombatTranslation->GetTranslationFor(pDisplayName);
        if(rTranslatedString)
        {
            tPerformedTranslation = true;
            rUseString = rTranslatedString;
        }
    }

    //--If no translation took place for any reason, check the items version.
    if(rItemTranslation && !tPerformedTranslation)
    {
        const char *rTranslatedString = rItemTranslation->GetTranslationFor(pDisplayName);
        if(rTranslatedString) rUseString = rTranslatedString;
    }

    ///--[Set]
    ResetString(rStackHead->mDisplayName, rUseString);
}
void AbyssCombat::SetCommandDisplayString(const char *pDisplayString)
{
    ///--[Documentation]
    //--Sets the display string for the active command. Does nothing for the dummy. The display string
    //  is only used if there is no ability associated with this entry.
    if(!pDisplayString) return;

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetCommandDisplayString()");
    if(!rStackHead) return;

    //--Check for translations.
    const char *rUseString = pDisplayString;
    StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_COMBAT);
    if(rTranslation)
    {
        const char *rTranslatedString = rTranslation->GetTranslationFor(pDisplayString);
        if(rTranslatedString) rUseString = rTranslatedString;
    }

    //--Set.
    ResetString(rStackHead->mDisplayString, rUseString);
}
void AbyssCombat::SetCommandManualLock(bool pIsLocked)
{
    ///--[Documentation]
    //--Command is locked via scripts and always is greyed out and unusable.

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetCommandManualLock()");
    if(!rStackHead) return;

    //--Set.
    rStackHead->mIsManuallyLocked = pIsLocked;
}
void AbyssCombat::SetCommandGlowing(bool pIsGlowing)
{
    ///--[Documentation]
    //--Glowing commands render with a highlight to draw attention to them.

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetCommandGlowing()");
    if(!rStackHead) return;

    //--Set.
    rStackHead->mGlowing = pIsGlowing;
}
void AbyssCombat::SetCommandAssociatedAbility(const char *pAbilityName)
{
    ///--[Documentation]
    //--Sets the ability associated with this list entry. This is the one that will activate if the player
    //  selects this command.
    if(!pAbilityName) return;

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetCommandAssociatedAbility()");
    if(!rStackHead) return;

    ///--[Null Ability]
    //--If the provided name is NULL then just clear the entry.
    if(!strcasecmp(pAbilityName, "NULL"))
    {
        rStackHead->rAssociatedAbility = NULL;
        return;
    }

    ///--[Check Global List]
    //--Check to see if the global list has the ability available. This is often used for inventory items.
    AdvCombatAbility *rCheckAbility = (AdvCombatAbility *)mGlobalAbilities->GetElementByName(pAbilityName);
    if(rCheckAbility)
    {
        rStackHead->rAssociatedAbility = rCheckAbility;
        return;
    }

    ///--[Resolve Ability]
    //--The active object is expected to be the AdvCombatJob.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATJOB))
    {
        fprintf(stderr, "Warning: Attempting to set ability %s, but the active object is not an AdvCombatJob.\n", pAbilityName);
        return;
    }

    //--Cast.
    AdvCombatJob *rJob = (AdvCombatJob *)DataLibrary::Fetch()->rActiveObject;

    //--The job nominally has a list of abilities in it. However, it is possible for the command list
    //  to use any ability the AdvCombatEntity owns, so we need the entity.
    AdvCombatEntity *rOwner = rJob->GetOwner();
    if(!rOwner)
    {
        fprintf(stderr, "Warning: Attempting to set ability %s, but the AdvCombatJob somehow has no owner!\n", pAbilityName);
        return;
    }

    //--Ability list.
    StarLinkedList *rAbilityList = rOwner->GetAbilityList();
    if(!rAbilityList)
    {
        fprintf(stderr, "Warning: Attempting to set ability %s, but the AdvCombatJob somehow has no ability list!\n", pAbilityName);
        return;
    }

    //--Locate the ability in question.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementByName(pAbilityName);
    if(!rAbility)
    {
        //--Special instances:
        if(!strcasecmp(pAbilityName, "System|Retreat"))
        {
            rAbility = AdvCombat::Fetch()->GetSystemRetreat();
        }
        else if(!strcasecmp(pAbilityName, "System|Surrender"))
        {
            rAbility = AdvCombat::Fetch()->GetSystemSurrender();
        }
        else if(!strcasecmp(pAbilityName, "System|Volunteer"))
        {
            rAbility = AdvCombat::Fetch()->GetSystemVolunteer();
        }
        //--Error:
        else
        {
            fprintf(stderr, "Warning: Attempting to set ability %s, but the named ability does not exist.\n", pAbilityName);
            return;
        }
    }

    //--Set.
    rStackHead->rAssociatedAbility = rAbility;
}
void AbyssCombat::SetCommandQuantity(int pQuantity)
{
    ///--[Documentation]
    //--Used for items. Entirely cosmetic, shows how many are in the inventory. Defaults to 0, if 0 doesn't display.

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetCommandQuantity()");
    if(!rStackHead) return;

    //--Set.
    rStackHead->mQuantity = pQuantity;
}
void AbyssCombat::SetInventorySlot(int pSlot)
{
    ///--[Documentation]
    //--Used for items, specifies the inventory slot an item is affiliated with. Not used in Monoceros,
    //  but derived classes may have a use for this.
    //--The default for no affiliated slot is -1.

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetInventorySlot()");
    if(!rStackHead) return;

    //--Set.
    rStackHead->mInventorySlot = pSlot;
}
void AbyssCombat::AddCommandIcon(const char *pIconPath)
{
    ///--[Documentation]
    //--Adds an icon to the icon list. These are rendered right-to-left next to the skill name. Icons can
    //  use any DLPath but you should use the 19x19 ones in the "Root/Images/AbyssUI/Pieces/Overlay_Icon" series.

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:AddCommandIcon()");
    if(!rStackHead) return;

    //--Get the image.
    StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pIconPath);
    if(!rImage) return;

    //--Append.
    rStackHead->mIconsList->AddElementAsTail("X", rImage);
}
void AbyssCombat::AddSuffixIcon(const char *pIconPath)
{
    ///--[Documentation]
    //--Adds an icon that appears after the name, used for charm icons.

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:AddCommandIcon()");
    if(!rStackHead) return;

    //--Get the image.
    StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pIconPath);
    if(!rImage) return;

    //--Append.
    rStackHead->rSuffixImg = rImage;
}
void AbyssCombat::SetCommandIsCharm(bool pFlag)
{
    ///--[Documentation]
    //--Skills that are flagged this way render an icon on the skills menu. This is used to denote skills
    //  that a player might have gained from charms, which is controlled via tags.
    //--This causes the entry to have a different color.

    //--Get the active command.
    CommandListEntry *rStackHead = ResolveStackHead("AbyssCombat:SetCommandIsCharm()");
    if(!rStackHead) return;

    //--Set.
    rStackHead->mIsCharmSkill = pFlag;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AbyssCombat::RenderCommandWindow(AdvCombatEntity *pEntity, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the command window for the currently acting entity, which is assumed to be the one passed in.
    //  The commands are kept globally by the AbyssCombat object, but reconstructed whenever an entity begins
    //  its turn. The window appears over the acting character, and the translation is expected to have already
    //  taken place when this is called.
    if(!pEntity || pAlpha <= 0.0f || mCommandTimer < 1) return;

    //--If there is dialogue or pending events, don't show the window.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rWorldDialogue->IsVisible()) return;
    if(rCutsceneManager->HasAnyEvents()) return;
    if(mIsShowingTutorialOverlay) return;

    ///--[Command Alpha/Offset]
    //--Resolve the alpha and multiply it against the global alpha.
    float cLocalAlpha = EasingFunction::QuadraticInOut(mCommandTimer, ABYCOM_COMMAND_SCROLL_TICKS);
    float cCommandAlpha = cLocalAlpha * pAlpha;

    ///--[Resolve List]
    //--Resolve which command list is in use.
    StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();

    //--Right-side Command Positions
    float cOptionTitleX  = 1195.0f;
    float cOptionTitleY  =  289.0f;
    float cOptionSpacing =   78.0f;

    //--Move up by one entry if the list is 7 entries long. This only occurs if "Switch" is on the list.
    if(mCommandList && mCommandList->GetListSize() == 7) glTranslatef(0.0f, -cOptionSpacing, 0.0f);

    //--Get the zeroth command list. This list always renders.
    const char *rSublistHeaderName = NULL;
    if(mCommandList)
    {
        //--Colors.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cCommandAlpha);

        //--Render backings.
        int i = 0;
        CommandListEntry *rEntry = (CommandListEntry *)mCommandList->PushIterator();
        while(rEntry)
        {
            //--Backing.
            AbyssImg.Data.rFrameOption->Draw(0.0f, i * cOptionSpacing);

            //--Cursor.
            if(mPlayerCommandBaseCursor == i)
            {
                AbyssImg.Data.rOverlayOptionSelect->Draw(0.0f, i * cOptionSpacing);
            }

            //--Next.
            i ++;
            rEntry = (CommandListEntry *)mCommandList->AutoIterate();
        }

        //--Names.
        i = 0;
        rEntry = (CommandListEntry *)mCommandList->PushIterator();
        while(rEntry)
        {
            if(rEntry->mDisplayName)
            {
                //--Set color.
                SetCommandColorFromEntry(rEntry, cCommandAlpha);

                //--Render.
                AbyssImg.Data.rFontCommandWindow->DrawTextArgs(cOptionTitleX, cOptionTitleY + (i * cOptionSpacing), SUGARFONT_AUTOCENTER_XY, 1.0f, rEntry->mDisplayName);
                StarlightColor::ClearMixer();

                //--Store the name if the cursor is on this.
                if(mPlayerCommandBaseCursor == i) rSublistHeaderName = rEntry->mDisplayName;
            }

            //--Next.
            i ++;
            rEntry = (CommandListEntry *)mCommandList->AutoIterate();
        }
    }

    //--Clean.
    if(mCommandList && mCommandList->GetListSize() == 7) glTranslatef(0.0f, cOptionSpacing, 0.0f);

    ///--[Render Derived List]
    //--If the player is picking from a secondary list, handle that here.
    if(rPlayerStackList != mCommandList && rPlayerStackList && mCommandSecondaryTimer > 0)
    {
        //--Resolve secondary alpha.
        float cLocalAlpha = EasingFunction::QuadraticInOut(mCommandSecondaryTimer, ABYCOM_COMMAND_SCROLL_TICKS);
        float cSubAlpha = cLocalAlpha * cCommandAlpha;

        //--Constants.
        float cSpcX  = 419.0f;
        float cSpcY  =  35.0f;
        float cNameX = 186.0f;
        float cNameY = 218.0f;

        //--Backing.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cSubAlpha);
        AbyssImg.Data.rFrameSkillItems->Draw();

        //--Header.
        if(rSublistHeaderName)
        {
            mColorHeader.SetAsMixerAlpha(cSubAlpha);
            AbyssImg.Data.rFontHeader->DrawText(581.0f, 170.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rSublistHeaderName);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cSubAlpha);
        }

        //--Render commands.
        int i = 0;
        int tRenders = 0;
        CommandListEntry *rEntry = (CommandListEntry *)rPlayerStackList->PushIterator();
        while(rEntry)
        {
            //--If this is below the skip, don't render it.
            if(i < mSkillItemSkip)
            {
                i ++;
                rEntry = (CommandListEntry *)rPlayerStackList->AutoIterate();
                continue;
            }

            //--If the render count has hit the cap, stop.
            if(tRenders >= cxCommand_SizePerPage)
            {
                rPlayerStackList->PopIterator();
                break;
            }

            //--Resolve position slot, check range.
            int tXSlot = tRenders % 2;
            int tYSlot = tRenders / 2;

            //--Backing position:
            float tUseXPos = cSpcX * tXSlot;
            float tUseYPos = cSpcY * tYSlot;
            AbyssImg.Data.rOverlaySkillItemShade->Draw(tUseXPos, tUseYPos - 40.0f);

            //--Cursor.
            if(mPlayerCommandCursor == i)
            {
                AbyssImg.Data.rOverlaySkillItemSelect->Draw(tUseXPos, tUseYPos - 40.0f);
            }

            //--Resolve position for text rendering.
            float cTxtRenderX = cNameX + tUseXPos;
            float cTxtRenderR = cNameX + tUseXPos + 370.0f;
            float cTxtRenderY = cNameY + tUseYPos;

            //--For each icon registered to the entry, render it.
            float cIconWid = 19.0f;
            float cIconRenderX = cTxtRenderX - cIconWid;
            StarBitmap *rIcon = (StarBitmap *)rEntry->mIconsList->PushIterator();
            while(rIcon)
            {
                //--Render.
                rIcon->Draw(cIconRenderX, cTxtRenderY + 12.0f);

                //--Next.
                cIconRenderX = cIconRenderX - cIconWid;
                rIcon = (StarBitmap *)rEntry->mIconsList->AutoIterate();
            }

            //--Entry.
            if(rEntry->mDisplayName)
            {
                //--Set color.
                SetCommandColorFromEntry(rEntry, cSubAlpha);

                //--Render.
                AbyssImg.Data.rFontSubcommand->DrawTextArgs(cTxtRenderX, cTxtRenderY, 0, 1.0f, rEntry->mDisplayName);
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cSubAlpha);
            }

            //--Suffix image.
            if(rEntry->rSuffixImg)
            {
                float cIconX = cTxtRenderX + AbyssImg.Data.rFontSubcommand->GetTextWidth(rEntry->mDisplayName) + 5.0f;
                rEntry->rSuffixImg->Draw(cIconX, cTxtRenderY + 12.0f);
            }

            //--If not available, show the cooldown, or MP/CP cost if applicable.
            if(rEntry->rAssociatedAbility && !rEntry->rAssociatedAbility->IsUsableNow())
            {
                //--Manual locking disables requirements.
                if(rEntry->mIsManuallyLocked)
                {

                }
                //--Cooldown:
                else if(rEntry->rAssociatedAbility->GetCooldown() > 0)
                {
                    AbyssImg.Data.rFontSubcommand->DrawTextArgs(cTxtRenderR, cTxtRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "CD %i", rEntry->rAssociatedAbility->GetCooldown());
                }
                //--MP:
                else if(rEntry->rAssociatedAbility->GetMPCost() > 0)
                {
                    AbyssImg.Data.rFontSubcommand->DrawTextArgs(cTxtRenderR, cTxtRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "MP %i", rEntry->rAssociatedAbility->GetMPCost());
                }
                //--CP:
                else if(rEntry->rAssociatedAbility->GetCPCost() > 0)
                {
                    AbyssImg.Data.rFontSubcommand->DrawTextArgs(cTxtRenderR, cTxtRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "CP %i", rEntry->rAssociatedAbility->GetCPCost());
                }
            }
            //--Available. Show MP/CP cost.
            else if(rEntry->rAssociatedAbility)
            {
                //--MP:
                if(rEntry->rAssociatedAbility->GetMPCost() > 0)
                {
                    AbyssImg.Data.rFontSubcommand->DrawTextArgs(cTxtRenderR, cTxtRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "MP %i", rEntry->rAssociatedAbility->GetMPCost());
                }
                //--CP:
                else if(rEntry->rAssociatedAbility->GetCPCost() > 0)
                {
                    AbyssImg.Data.rFontSubcommand->DrawTextArgs(cTxtRenderR, cTxtRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "CP %i", rEntry->rAssociatedAbility->GetCPCost());
                }
                //--Quantity. Optional, only used for items.
                else if(rEntry->mQuantity > 0)
                {
                    AbyssImg.Data.rFontSubcommand->DrawTextArgs(cTxtRenderR, cTxtRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "x%i", rEntry->mQuantity);
                }
            }

            //--Next.
            i ++;
            tRenders ++;
            rEntry = (CommandListEntry *)rPlayerStackList->AutoIterate();
        }

        ///--[Scrollbar]
        if(rPlayerStackList->GetListSize() > cxCommand_SizePerPage)
        {
            StarUIPiece::StandardRenderScrollbar(mSkillItemSkip, cxCommand_SizePerPage, rPlayerStackList->GetListSize() - cxCommand_SizePerPage, 208.0f, 477.0f, false, AbyssImg.Data.rScrollbarScroller, AbyssImg.Data.rScrollbarStatic);
        }

        //--Clean up.
        glDisable(GL_STENCIL_TEST);
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
