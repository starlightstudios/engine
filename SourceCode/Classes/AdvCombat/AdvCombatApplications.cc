//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

///========================================= Functions ============================================
void AdvCombat::ResolveBattlePosition(AdvCombatEntity *pEntity, int pTypeFlag, float &sXPos, float &sYPos)
{
    ///--[Documentation and Setup]
    //--Given an entity, returns its nominal position in the shared variables. This varies slightly
    //  by text versus animations.
    if(!pEntity) return;

    //--Constants.
    float cMainTextY = 274.0f;
    float cSecondaryTextY = 194.0f;
    float cAnimationY = 100.0f;

    ///--[Resolve]
    //--Primary Text:
    if(pTypeFlag == ADVCOMBAT_POSTYPE_PRIMARY_TEXT)
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cMainTextY;
    }
    //--Secondary Text:
    else if(pTypeFlag == ADVCOMBAT_POSTYPE_SECONDARY_TEXT)
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cSecondaryTextY;
    }
    //--Animation:
    else
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cAnimationY;
    }
}
void AdvCombat::ScatterPosition(int &sIndex, float &sXPos, float &sYPos)
{
    //--Subroutine that scatters the X/Y position of something, usually damage numbers, according to the index.
    //  The index is then moved to the next scatter position. This allows multiple damage numbers and text to
    //  fire at the same time without slowing combat down.
    float cScatterAngle = 50.0f * (sIndex-1);
    float cScatterAngleOff = -135.0f;
    float cScatterRadius = 0.0f;
    float cRadiusPerCircle = 100.0f;

    //--Scatter radius is zero for the first scatter. After that, every time the angle passes 2*Pi, it increases.
    //  This creates an expanding circle of locations.
    if(sIndex > 0)
    {
        int cTotalRevolutions = (int)cScatterAngle / 360.0f;
        cScatterRadius = cRadiusPerCircle * (cTotalRevolutions+1);
    }

    //--Compute and place scatter.
    sIndex ++;
    sXPos = sXPos + (cosf((cScatterAngle+cScatterAngleOff) * TORADIAN) * cScatterRadius);
    sYPos = sYPos + (sinf((cScatterAngle+cScatterAngleOff) * TORADIAN) * cScatterRadius);
}
void AdvCombat::HandleApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString)
{
    ///--[Documentation and Setup]
    //--Given a string, finds the target and then applies the string's effect to that target. Note that
    //  the originator ID can legally be 0 in some cases.
    if(!pString || !pTargetID) return;

    ///--[Target Acquisition]
    //--First, locate the target. It can be either in the party or the enemy roster.
    AdvCombatEntity *rApplyEntity = NULL;

    //--Search the player's party. Note we search the *entire* roster, which means party members
    //  can be affected if they're not even in the battle!
    AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pTargetID)
        {
            rApplyEntity = rCheckEntity;
            mPartyRoster->PopIterator();
            break;
        }
        rCheckEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }

    //--If not found in the player's roster, search the enemy roster.
    rCheckEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pTargetID)
        {
            rApplyEntity = rCheckEntity;
            mEnemyRoster->PopIterator();
            break;
        }
        rCheckEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--If there was no target, fail here.
    if(!rApplyEntity) return;

    ///--[String Splitting]
    //--Break the string using the Subdivide algorithm.
    StarLinkedList *tStringList = Subdivide::SubdivideStringToList(pString, "|");

    //--Debug.
    if(false)
    {
        fprintf(stderr, "String Break Report %s\n", pString);
        fprintf(stderr, " %i strings.\n", tStringList->GetListSize());
        for(int i = 0; i < tStringList->GetListSize(); i ++)
        {
            fprintf(stderr, " %s\n", (char *)tStringList->GetElementBySlot(i));
        }
    }

    ///--[Effect Application]
    //--0th part of the string is what it does. How many additional parts are needed is handled per-type.
    int tArgs = tStringList->GetListSize();
    const char *rSwitchType = (char *)tStringList->GetElementBySlot(0);
    if(!rSwitchType) return;

    //--HP Damage:
    if(!strcasecmp(rSwitchType, "Damage") && tArgs >= 2)
    {
        //--Arg check.
        int tDamage = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tDamage < 1) return;

        //--Spawn the damage numbers over the target.
        CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
        nPack->Initialize();
        nPack->mTicks = 0;
        nPack->mTicksMax = 30;
        nPack->mText = InitializeString("%i", tDamage);
        nPack->mColor.SetRGBAF(1.0f, 0.0f, 0.0f, 1.0f);
        mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

        //--Resolve Y position.
        ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_PRIMARY_TEXT, nPack->mX, nPack->mY);

        //--Scatter the text position.
        ScatterPosition(mTextScatterCounter, nPack->mX, nPack->mY);

        //--Priorities. By default, it's Shield->Adrenaline->Health.
        int tHealthPriority = 1;
        int tAdrenalinePriority = 2;
        int tShieldPriority = 3;

        //--Run optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Priority-Shield. Integer value, 0 means don't hit shields.
            if(!strcasecmp(rSubSwitchType, "PrioritySh") && tSubArgs == 2)
            {
                const char *rString = (const char *)tArgumentList->GetElementBySlot(1);
                tShieldPriority = atoi(rString);
            }
            //--Priority-Health.
            else if(!strcasecmp(rSubSwitchType, "PriorityHe") && tSubArgs == 2)
            {
                const char *rString = (const char *)tArgumentList->GetElementBySlot(1);
                tHealthPriority = atoi(rString);
            }
            //--Priority-Adrenaline.
            else if(!strcasecmp(rSubSwitchType, "PriorityAd") && tSubArgs == 2)
            {
                const char *rString = (const char *)tArgumentList->GetElementBySlot(1);
                tAdrenalinePriority = atoi(rString);
            }
            //--Glancing blow. Spawns an extra animation.
            else if(!strcasecmp(rSubSwitchType, "Glance") && tSubArgs == 1)
            {
                //--Spawn the 'glance' string above everything.
                CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
                nPack->Initialize();
                nPack->mTicks = 0;
                nPack->mTicksMax = 30;
                nPack->mText = InitializeString("Glance!");
                nPack->mScale = 0.5f;
                nPack->mColor.SetRGBAF(1.0f, 8.0f, 0.3f, 1.0f);
                mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

                //--Resolve Y position.
                ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_SECONDARY_TEXT, nPack->mX, nPack->mY);

                //--Sound effect.
                PlaySoundPackage *nPackage = (PlaySoundPackage *)starmemoryalloc(sizeof(PlaySoundPackage));
                nPackage->Initialize();
                strcpy(nPackage->mSoundName, "Combat|Glance");
                EnqueueSound(nPackage);
            }
            //--Critical Strike. Adds a "!" to the end of the damage, but is otherwise the same.
            else if(!strcasecmp(rSubSwitchType, "Critical") && tSubArgs == 1)
            {
                free(nPack->mText);
                nPack->mText = InitializeString("%i!", tDamage);
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Damage' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--Unloseable: If this is the case, set the damage such that the target never goes below 1 HP.
        //  This can zero off the damage!
        if(mIsUnloseable && IsEntityInPlayerParty(rApplyEntity))
        {
            //--Clamp.
            int tTargetHP = rApplyEntity->GetHealth();
            if(tDamage >= tTargetHP) tDamage = tTargetHP - 1;
            if(tDamage < 0) tDamage = 0;

            //--Reset text values.
            free(nPack->mText);
            nPack->mText = InitializeString("%i", tDamage);
        }
        //--Unwinnable: Enemies cannot be defeated.
        else if(mIsUnwinnable && !IsEntityInPlayerParty(rApplyEntity))
        {
            //--Clamp.
            int tTargetHP = rApplyEntity->GetHealth();
            if(tDamage >= tTargetHP) tDamage = tTargetHP - 1;
            if(tDamage < 0) tDamage = 0;

            //--Reset text values.
            free(nPack->mText);
            nPack->mText = InitializeString("%i", tDamage);
        }

        //--Damage the entity.
        rApplyEntity->InflictDamage(tDamage, tHealthPriority, tAdrenalinePriority, tShieldPriority);
        rApplyEntity->Shake();
    }
    //--Influence Damage or Healing
    else if(!strcasecmp(rSwitchType, "Influence") && tArgs >= 2)
    {
        //--Arg check.
        int tInfluence = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tInfluence < 1) return;

        //--Apply.
        rApplyEntity->SetInfluence(rApplyEntity->GetInfluence() + tInfluence);
    }
    //--Healing!
    else if(!strcasecmp(rSwitchType, "Healing") && tArgs == 2)
    {
        //--Arg check.
        int tHealing = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tHealing < 1) return;

        //--Spawn the healing numbers over the target.
        CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
        nPack->Initialize();
        nPack->mTicks = 0;
        nPack->mTicksMax = 30;
        nPack->mText = InitializeString("%i", tHealing);
        nPack->mColor.SetRGBAF(0.0f, 1.0f, 0.0f, 1.0f);
        mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

        //--Resolve position.
        ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_PRIMARY_TEXT, nPack->mX, nPack->mY);

        //--Scatter the text position.
        ScatterPosition(mTextScatterCounter, nPack->mX, nPack->mY);

        //--Damage the entity.
        rApplyEntity->SetHealth(rApplyEntity->GetHealth() + tHealing);
    }
    //--Shields!
    else if(!strcasecmp(rSwitchType, "Shields") && tArgs == 2)
    {
        //--Arg check.
        int tShieldsValue = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tShieldsValue < 1) return;

        //--Spawn the shields numbers over the target.
        CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
        nPack->Initialize();
        nPack->mTicks = 0;
        nPack->mTicksMax = 30;
        nPack->mText = InitializeString("%i", tShieldsValue);
        nPack->mColor.SetRGBAF(0.0f, 0.0f, 1.0f, 1.0f);
        mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

        //--Resolve position.
        ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_PRIMARY_TEXT, nPack->mX, nPack->mY);

        //--Scatter the text position.
        ScatterPosition(mTextScatterCounter, nPack->mX, nPack->mY);

        //--Damage the entity.
        rApplyEntity->SetShields(tShieldsValue);
    }
    //--Gain MP!
    else if(!strcasecmp(rSwitchType, "MPGain") && tArgs == 2)
    {
        //--Arg check.
        int tMPToGain = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tMPToGain < 1) return;

        //--Spawn the MP numbers over the target.
        CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
        nPack->Initialize();
        nPack->mTicks = 0;
        nPack->mTicksMax = 30;
        nPack->mText = InitializeString("%i MP", tMPToGain);
        nPack->mColor.SetRGBAF(0.65f, 0.1f, 1.0f, 1.0f);
        mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

        //--Resolve position.
        ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_PRIMARY_TEXT, nPack->mX, nPack->mY);

        //--Scatter the text position.
        ScatterPosition(mTextScatterCounter, nPack->mX, nPack->mY);

        //--Apply MP gain.
        rApplyEntity->SetMagic(rApplyEntity->GetMagic() + tMPToGain);
    }
    //--Adrenaline!
    else if(!strcasecmp(rSwitchType, "Adrenaline") && tArgs == 2)
    {
        //--Arg check.
        int tAdrenaline = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tAdrenaline < 1) return;

        //--Spawn the adrenaline numbers over the target.
        CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
        nPack->Initialize();
        nPack->mTicks = 0;
        nPack->mTicksMax = 30;
        nPack->mText = InitializeString("%i", tAdrenaline);
        nPack->mColor.SetRGBAF(1.0f, 0.6f, 0.3f, 1.0f);
        mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

        //--Resolve position.
        ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_PRIMARY_TEXT, nPack->mX, nPack->mY);

        //--Scatter the text position.
        ScatterPosition(mTextScatterCounter, nPack->mX, nPack->mY);

        //--Damage the entity.
        rApplyEntity->SetAdrenaline(rApplyEntity->GetAdrenaline() + tAdrenaline);
    }
    //--Stun Damage. Sets to the value, does not increment it.
    else if(!strcasecmp(rSwitchType, "Stun") && tArgs >= 2)
    {
        //--Stun damage amount.
        int tStunDamage = atoi((const char *)tStringList->GetElementBySlot(1));
        if(tStunDamage < 1) return;

        //--Apply
        rApplyEntity->SetStun(tStunDamage);
    }
    //--Stun display. Orders the stun value to run to its current value but does not change the base value.
    else if(!strcasecmp(rSwitchType, "Stun Display"))
    {
        rApplyEntity->SetStunOnlyDisplay();
    }
    //--Text. Spawns a text package.
    else if(!strcasecmp(rSwitchType, "Text") && tArgs >= 2)
    {
        //--Create and set package to defaults.
        CombatTextPack *nPack = (CombatTextPack *)starmemoryalloc(sizeof(CombatTextPack));
        nPack->Initialize();
        nPack->mTicks = 0;
        nPack->mTicksMax = ADVCOMBAT_INFIELD_TEXT_TICKS;
        nPack->mText = InitializeString((const char *)tStringList->GetElementBySlot(1));
        nPack->mColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
        mTextPackages->AddElement("X", nPack, CombatTextPack::DeleteThis);

        //--Resolve position.
        ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_SECONDARY_TEXT, nPack->mX, nPack->mY);

        //--Scatter the text position.
        ScatterPosition(mTextScatterCounter, nPack->mX, nPack->mY);

        //--Run optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Color. Argument can be the name of a preset color, or of format 0xRRRxGGGxBBBxAAA to specify RGBA.
            if(!strcasecmp(rSubSwitchType, "Color") && tSubArgs == 2)
            {
                //--Color string.
                const char *rColor = (const char *)tArgumentList->GetElementBySlot(1);

                //--Preset white.
                if(!strcasecmp(rColor, "White"))
                {
                    nPack->mColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
                }
                else if(!strcasecmp(rColor, "Green"))
                {
                    nPack->mColor.SetRGBAF(0.1f, 0.8f, 0.1f, 1.0f);
                }
                //--Preset purple.
                else if(!strcasecmp(rColor, "Purple"))
                {
                    nPack->mColor.SetRGBAF(0.7f, 0.2f, 0.7f, 1.0f);
                }
                //--Preset red.
                else if(!strcasecmp(rColor, "Red"))
                {
                    nPack->mColor.SetRGBAF(0.9f, 0.2f, 0.2f, 1.0f);
                }
                //--Preset blue.
                else if(!strcasecmp(rColor, "Blue"))
                {
                    nPack->mColor.SetRGBAF(0.2f, 0.2f, 0.9f, 1.0f);
                }
                //--Preset yellow
                else if(!strcasecmp(rColor, "Yellow"))
                {
                    nPack->mColor.SetRGBAF(0.8f, 0.8f, 0.1f, 1.0f);
                }
                //--Use integers. Expects format 0xRRRxGGGxBBBxAAA
                else if(!strncasecmp(rColor, "0x", 2) && strlen(rColor) >= 17)
                {
                    char tRedBuf[4];
                    char tBluBuf[4];
                    char tGrnBuf[4];
                    char tAlpBuf[4];
                    for(int i = 0; i < 3; i ++)
                    {
                        tRedBuf[i] = rColor[ 2+i];
                        tBluBuf[i] = rColor[ 6+i];
                        tGrnBuf[i] = rColor[10+i];
                        tAlpBuf[i] = rColor[14+i];
                    }
                    tRedBuf[3] = '\0';
                    tBluBuf[3] = '\0';
                    tGrnBuf[3] = '\0';
                    tAlpBuf[3] = '\0';
                    nPack->mColor.SetRGBAI(atoi(tRedBuf), atoi(tBluBuf), atoi(tGrnBuf), atoi(tAlpBuf));
                }
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Text' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }
    }
    //--Play sound effect:
    else if(!strcasecmp(rSwitchType, "Play Sound") && tArgs >= 2)
    {
        //--Create a sound package.
        PlaySoundPackage *nPackage = (PlaySoundPackage *)starmemoryalloc(sizeof(PlaySoundPackage));
        nPackage->Initialize();
        strncpy(nPackage->mSoundName, (const char *)tStringList->GetElementBySlot(1), STD_MAX_LETTERS-1);

        //--Debug.
        const char *rCheckSound = (const char *)tStringList->GetElementBySlot(1);
        if(!rCheckSound)
        {
            fprintf(stderr, "Error. Check sound is null.\n");
            fprintf(stderr, "%s\n", pString);
        }
        else if(rCheckSound[0] == '\0')
        {
            fprintf(stderr, "Error. Check sound has zero letters.\n");
            fprintf(stderr, "%s\n", pString);
        }

        //--Optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Order the sound to play with an effect package, pre-defined in the AudioManager.
            if(!strcasecmp(rSubSwitchType, "Effect") && tSubArgs == 2)
            {
                strncpy(nPackage->mEffectName, (const char *)tArgumentList->GetElementBySlot(1), STD_MAX_LETTERS-1);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--Enqueue it.
        EnqueueSound(nPackage);
    }
    //--Create an animation.
    else if(!strcasecmp(rSwitchType, "Create Animation") && tArgs >= 3)
    {
        //--X/Y Position.
        float tUseX = 0.0f;
        float tUseY = 0.0f;

        //--Resolve position.
        ResolveBattlePosition(rApplyEntity, ADVCOMBAT_POSTYPE_ANIMATION, tUseX, tUseY);

        //--Optional arguments:
        for(int i = 3; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Override the X position.
            if(!strcasecmp(rSubSwitchType, "UseX") && tSubArgs == 2)
            {
                tUseX = atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Override the Y position.
            else if(!strcasecmp(rSubSwitchType, "UseY") && tSubArgs == 2)
            {
                tUseY = atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Offset the X position.
            else if(!strcasecmp(rSubSwitchType, "OffX") && tSubArgs == 2)
            {
                tUseX = tUseX + atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Offset the Y position.
            else if(!strcasecmp(rSubSwitchType, "OffY") && tSubArgs == 2)
            {
                tUseY = tUseY + atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Effect' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--Create.
        CreateAnimationInstance((char *)tStringList->GetElementBySlot(1), (char *)tStringList->GetElementBySlot(2), 0, tUseX, tUseY);
    }
    //--Flash black twice.
    else if(!strcasecmp(rSwitchType, "Flash Black") && tArgs == 1)
    {
        rApplyEntity->FlashBlack();
    }
    //--Create a title at the top of the screen.
    else if(!strcasecmp(rSwitchType, "Title") && tArgs >= 2)
    {
        //--If a third argument is present, it's how many ticks to show the title.
        int tTicks = ADVCOMBAT_TITLE_TICKS_STANDARD;
        if(tArgs >= 3)
        {
            tTicks = atoi((const char *)tStringList->GetElementBySlot(2));
        }

        //--Check if there's a translation for the title.
        const char *rTitle = (const char *)tStringList->GetElementBySlot(1);
        const char *rUseString = rTitle;
        StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_COMBAT);
        if(rTranslation)
        {
            const char *rTranslatedString = rTranslation->GetTranslationFor(rTitle);
            if(rTranslatedString) rUseString = rTranslatedString;
        }

        //--Spawn.
        SpawnTitle(tTicks, rUseString);
    }
    //--Effect. Applies an effect to the ID'd target.
    else if(!strcasecmp(rSwitchType, "Effect") && tArgs >= 2)
    {
        //--Get the script to call.
        const char *rScriptPath = (const char *)tStringList->GetElementBySlot(1);

        //--Create and register the effect.
        AdvCombatEffect *nEffect = new AdvCombatEffect();
        RegisterEffect(nEffect);
        nEffect->AddTargetByID(pTargetID);

        //--Variables.
        bool tUsesPrototype = false;
        bool tIsCritical = false;
        float tSeverity = 1.0f;

        //--Run optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--Originator. The ID of the entity that created the effect.
            if(!strcasecmp(rSubSwitchType, "Originator") && tSubArgs == 2)
            {
                nEffect->SetOriginatorByID(atoi((const char *)tArgumentList->GetElementBySlot(1)));
            }
            //--Severity. Affects the power of the effect. Default is 1.0f.
            else if(!strcasecmp(rSubSwitchType, "Severity") && tSubArgs == 2)
            {
                tSeverity = atof((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Store ID. Stores the newly created AdvCombatEffect's ID in the DataLibrary so the ability script can get it.
            else if(!strcasecmp(rSubSwitchType, "StoreID") && tSubArgs == 2)
            {
                //--Get the provided ID.
                int tStoreID = atoi((const char *)tArgumentList->GetElementBySlot(1));

                //--Locate the DataLibrary entry.
                char *tBuffer = InitializeString("Root/Variables/Combat/%i/iManagedEffectID", tStoreID);
                SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry(tBuffer);
                if(rVariable) rVariable->mNumeric = (float)nEffect->GetID();
                free(tBuffer);
            }
            //--Prototype, the effect is created using a prototype stored on a global list.
            else if(!strcasecmp(rSubSwitchType, "Prototype") && tSubArgs == 2)
            {
                //--Store it internally.
                tUsesPrototype = true;
                nEffect->SetGlobalPrototypeName((const char *)tArgumentList->GetElementBySlot(1));

                //--Also set the static global value for the creation function.
                ResetString(AdvCombatEffect::xGlobalEffectName, (const char *)tArgumentList->GetElementBySlot(1));
                //fprintf(stderr, "Set global prototype name: %s\n", AdvCombatEffect::xGlobalEffectName);
            }
            //--Critical strike. If using a prototype, adds ".Crit" onto the end of it.
            else if(!strcasecmp(rSubSwitchType, "Critical") && tSubArgs == 1)
            {
                tIsCritical = true;
            }
            //--Additional arguments. Up to ADVCE_MAX_ARGS are supported. Allows effects with variable values.
            else if(!strncasecmp(rSubSwitchType, "Arg", 3) && tSubArgs == 2)
            {
                //--Get the slot of the argument, from 0 to 9.
                int tSlot = rSubSwitchType[3] - '0';
                float tValue = atof((const char *)tArgumentList->GetElementBySlot(1));

                //--Set.
                nEffect->SetArgument(tSlot, tValue);
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Effect' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--If the attack uses a prototype, and was a critical hit, then we append ".Crit" onto the end of the name
        //  of the prototype. The script will then look for a critical hit version of the effect. If it does not
        //  exist then the base version is used.
        if(tIsCritical && tUsesPrototype)
        {
            char *tTempString = InitializeString("%s.Crit", AdvCombatEffect::xGlobalEffectName);
            ResetString(AdvCombatEffect::xGlobalEffectName, tTempString);
            free(tTempString);
        }

        //--Call the setup script.
        LuaManager::Fetch()->PushExecPop(nEffect, rScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_CREATE, "N", tSeverity);

        //--Order the Effect to apply stat changes to all entities on its target list. The target list consists of
        //  the target we set earlier but can be modified by the creation script.
        nEffect->ApplyStatsToTargets();
    }
    //--Remove Effect. Removes the effect with the given ID.
    else if(!strcasecmp(rSwitchType, "Remove Effect") && tArgs >= 2)
    {
        //--ID.
        uint32_t tEffectID = atoi((const char *)tStringList->GetElementBySlot(1));

        //--Remove it.
        MarkEffectForRemoval(tEffectID);
    }
    //--Changes the party affiliation of the target entity. Passing AC_PARTY_GROUP_NONE for the party moves them to the roster.
    else if(!strcasecmp(rSwitchType, "Change Party") && tArgs >= 2)
    {
        //--Change party.
        int tPartyGroup = atoi((const char *)tStringList->GetElementBySlot(1));
        MoveEntityToPartyGroup(rApplyEntity->GetID(), tPartyGroup);

        //--Setup.
        float cRenderX = ComputeIdealX(true, true, 0, 1, rApplyEntity->GetCombatPortrait()); //Offscreen
        float cRenderY = ComputeIdealY(rApplyEntity);

        //--Set.
        rApplyEntity->SetIdealPosition(cRenderX, cRenderY);
        rApplyEntity->MoveToIdealPosition(0);

        //--Recompute all positions.
        PositionEnemies();
    }
    //--Orders the entity to change position. Second argument is type, third is a code or a number.
    else if(!strcasecmp(rSwitchType, "Position") && tArgs >= 2)
    {
        //--Setup.
        int tTicks = ADVCOMBAT_STD_MOVE_TICKS;
        float tPosition = 0.0f;

        //--Get type. It's a single-integer code.
        int tPositionType = atoi((const char *)tStringList->GetElementBySlot(1));

        //--Optional arguments:
        for(int i = 2; i < tStringList->GetListSize(); i ++)
        {
            //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
            StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i), ":");
            int tSubArgs = tArgumentList->GetListSize();
            const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

            //--How many ticks this will take.
            if(!strcasecmp(rSubSwitchType, "Ticks") && tSubArgs == 2)
            {
                tTicks = atoi((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Position. Used for AC_POSCODE_DIRECT, specifies exact X position.
            else if(!strcasecmp(rSubSwitchType, "Pos") && tSubArgs == 2)
            {
                tPosition = atoi((const char *)tArgumentList->GetElementBySlot(1));
            }
            //--Error.
            else
            {
                DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Position' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
            }

            //--Clean.
            delete tArgumentList;
        }

        //--Position with a direct X code. This will be the center point.
        if(tPositionType == AC_POSCODE_DIRECT)
        {
            rApplyEntity->SetIdealPositionX(tPosition);
            rApplyEntity->MoveToIdealPosition(tTicks);
        }
        //--Position as a member of the player's party. The player party will move as needed.
        else if(tPositionType == AC_POSCODE_PLAYER_PARTY)
        {
            //--If the entry is already on the party list, just put the party onscreen.
            if(mrCombatParty->IsElementOnList(rApplyEntity))
            {
                PositionListOnscreen(true, tTicks, mrCombatParty);
            }
            //--Otherwise, add them to a temporary list with the combat party and position that onscreen.
            else
            {
                //--Create.
                StarLinkedList *trTempList = new StarLinkedList(false);

                //--Clone.
                void *rPtr = mrCombatParty->PushIterator();
                while(rPtr)
                {
                    trTempList->AddElementAsTail("X", rPtr);
                    rPtr = mrCombatParty->AutoIterate();
                }

                //--Apply.
                trTempList->AddElementAsTail("X", rApplyEntity);
                PositionListOnscreen(true, tTicks, trTempList);

                //--Clean.
                delete trTempList;
            }
        }
        //--Position as a member of the opposing party.
        else if(tPositionType == AC_POSCODE_ENEMY_PARTY)
        {
            //--If the entry is already on the enemy list, just put the enemy onscreen.
            if(mrEnemyCombatParty->IsElementOnList(rApplyEntity))
            {
                PositionListOnscreen(false, tTicks, mrEnemyCombatParty);
            }
            //--Otherwise, add them to a temporary list with the combat party and position that onscreen.
            else
            {
                //--Create.
                StarLinkedList *trTempList = new StarLinkedList(false);

                //--Clone.
                void *rPtr = mrEnemyCombatParty->PushIterator();
                while(rPtr)
                {
                    trTempList->AddElementAsTail("X", rPtr);
                    rPtr = mrEnemyCombatParty->AutoIterate();
                }

                //--Apply.
                trTempList->AddElementAsTail("X", rApplyEntity);
                PositionListOnscreen(false, tTicks, trTempList);

                //--Clean.
                delete trTempList;
            }
        }
    }
    //--Executes the named script.
    else if(!strcasecmp(rSwitchType, "Run Script") && tArgs >= 2)
    {
        //--Script path.
        const char *rScriptPath = (const char *)tStringList->GetElementBySlot(1);

        //--No additional arguments:
        if(tArgs == 2)
        {
            LuaManager::Fetch()->ExecuteLuaFile(rScriptPath);
        }
        //--Arguments are present.
        else
        {
            //--Number of arguments expected is 1 for each substring.
            int tExpectedArgs = tArgs - 2;
            LuaManager *rLuaManager = LuaManager::Fetch();
            rLuaManager->SetArgumentListSize(tExpectedArgs);
            for(int i = 0; i < tExpectedArgs; i ++)
            {
                //--Break the string into parts. We use ':' instead of "|" for smaller argument lists.
                StarLinkedList *tArgumentList = Subdivide::SubdivideStringToList((const char *)tStringList->GetElementBySlot(i+2), ":");
                int tSubArgs = tArgumentList->GetListSize();
                const char *rSubSwitchType = (const char *)tArgumentList->GetElementBySlot(0);

                //--Number:
                if(!strcasecmp(rSubSwitchType, "N") && tSubArgs == 2)
                {
                    rLuaManager->AddArgument(atoi((const char *)tArgumentList->GetElementBySlot(1)));
                }
                //--String:
                else if(!strcasecmp(rSubSwitchType, "S") && tSubArgs == 2)
                {
                    rLuaManager->AddArgument((const char *)tArgumentList->GetElementBySlot(1));
                }
                //--Error.
                else
                {
                    DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, 'Position' no optional argument %s with %i args.\n", rSubSwitchType, tSubArgs);
                }

                //--Clean.
                delete tArgumentList;
            }

            //--Execute.
            LuaManager::Fetch()->ExecuteLuaFileBypass(rScriptPath);
        }
    }
    //--Flashes up to white. Is a toggle, be sure to unset later!
    else if(!strcasecmp(rSwitchType, "FlashWhite") && tArgs >= 1)
    {
        rApplyEntity->SetFlashingWhite(true);
    }
    //--Unsets the white flash.
    else if(!strcasecmp(rSwitchType, "UnflashWhite") && tArgs >= 1)
    {
        rApplyEntity->SetFlashingWhite(false);
    }
    //--Change jobs in combat.
    else if(!strcasecmp(rSwitchType, "JobChange") && tArgs >= 2)
    {
        const char *rJobName = (const char *)tStringList->GetElementBySlot(1);
        rApplyEntity->SetActiveJob(rJobName);
    }
    //--No specific application. This is used for AI scripts. The AI can see the arguments and respond to them, but the C++
    //  code does not need to print an error for this.
    else if(!strcasecmp(rSwitchType, "AI_APPLICATION") && tArgs >= 1)
    {
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("AdvCombat:HandleApplication - Error, no application type %s found with %i arguments.\n", rSwitchType, tArgs);
    }

    //--Clean up.
    delete tStringList;
}
