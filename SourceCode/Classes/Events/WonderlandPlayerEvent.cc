//--Base
#include "WonderlandPlayerEvent.h"

//--Classes
#include "RootEntity.h"

//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"

///========================================== System ==============================================
WonderlandPlayerEvent::WonderlandPlayerEvent()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_EVENT_WONDERLANDPLAYER;

    ///--[RootEvent]
    //--System
    //--Script Execution
    //--Data Storage

    ///--[WonderlandPlayerEvent]
    //--System
    mIsComplete = false;
    mInstructionType = WLPE_TYPE_NONE;

    //--Target
    mTargetID = 0;

    //--Data
    memset(&Data, 0, sizeof(Data));
}
WonderlandPlayerEvent::~WonderlandPlayerEvent()
{
}

///===================================== Property Queries =========================================
bool WonderlandPlayerEvent::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_EVENT_ROOT) return true;
    if(pType == POINTER_TYPE_EVENT_WONDERLANDPLAYER) return true;
    return false;
}
uint32_t WonderlandPlayerEvent::GetSubjectID()
{
    return mTargetID;
}

///======================================= Manipulators ===========================================
void WonderlandPlayerEvent::SetTarget(uint32_t pID)
{
    mTargetID = pID;
}
void WonderlandPlayerEvent::SetTargetByName(const char *pName)
{
    //--Flags.
    mTargetID = 0;

    //--Level check.
    WonderlandLevel *rActiveLevel = WonderlandLevel::Fetch();
    if(!rActiveLevel) return;

    //--Locate the subject.
    RootEntity *rPotentialSubject = (RootEntity *)rActiveLevel->GetEntityS(pName);
    if(!rPotentialSubject || !rPotentialSubject->IsOfType(POINTER_TYPE_WONDERLANDPLAYER))
    {
        DebugManager::ForcePrint("WonderlandPlayerEvent:SetTargetByName - Error, unable to locate WonderlandPlayer %s.\n", pName);
        return;
    }

    //--Set flags.
    mTargetID = rPotentialSubject->GetID();
}
void WonderlandPlayerEvent::SetEmulateKeypress(int pKeycode, int pTicks)
{
    mInstructionType = WLPE_TYPE_EMULATEKEYPRESS;
    Data.EmulateKeypress.mKeycode = pKeycode;
    Data.EmulateKeypress.mTicks = pTicks;
}

///======================================= Core Methods ===========================================
bool WonderlandPlayerEvent::IsComplete()
{
    //--Returns whether or not the event is complete. Immediately completes if it has no target.
    if(!mTargetID) return true;

    //--Event will store its completion state during the Update() call.
    return mIsComplete;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void WonderlandPlayerEvent::Update()
{
    //--Handles the event update, or does nothing if there's no target ID.
    if(!mTargetID) return;

    //--Level check.
    WonderlandLevel *rActiveLevel = WonderlandLevel::Fetch();
    if(!rActiveLevel)
    {
        mIsComplete = true;
        return;
    }

    //--Locate the WonderlandPlayer in question.
    WonderlandPlayer *rSubject = (WonderlandPlayer *)rActiveLevel->GetEntityI(mTargetID);
    if(!rSubject || !rSubject->IsOfType(POINTER_TYPE_WONDERLANDPLAYER))
    {
        mIsComplete = true;
        return;
    }

    //--If the actor has already handled an instruction this tick, we end but don't flag for completion.
    //  This means multiple instructions can be queued but the actor will only execute the first one
    //  found until it expires.
    if(!rSubject->CanHandleInstructions()) return;

    //--Emulate a keypress.
    if(mInstructionType == WLPE_TYPE_EMULATEKEYPRESS)
    {
        //--Run timer.
        Data.EmulateKeypress.mTicks --;
        if(Data.EmulateKeypress.mTicks < 1) mIsComplete = true;

        //--Emulate the keypress.
        if(Data.EmulateKeypress.mKeycode == WLPE_EMULATE_KEYPRESS_CODE_LEFT)
            rSubject->EmulateKeypressLeft();
        else if(Data.EmulateKeypress.mKeycode == WLPE_EMULATE_KEYPRESS_CODE_RIGHT)
            rSubject->EmulateKeypressRight();
        else if(Data.EmulateKeypress.mKeycode == WLPE_EMULATE_KEYPRESS_CODE_JUMP)
            rSubject->EmulateKeypressJump();
        else if(Data.EmulateKeypress.mKeycode == WLPE_EMULATE_KEYPRESS_CODE_SHOOT)
            rSubject->EmulateKeypressShoot();
    }
    //--Unknown type.
    else
    {
        mIsComplete = true;
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void WonderlandPlayerEvent::HookToLuaState(lua_State *pLuaState)
{
    /* WonderlandPlayerEvent_SetProperty("Subject ID", iID)
       WonderlandPlayerEvent_SetProperty("Subject Name", sName)
       WonderlandPlayerEvent_SetProperty("Emulate Keypress", iKeycode, iTicks)
       Sets the given property in the active ActorEvent or derived type. */
    lua_register(pLuaState, "WonderlandPlayerEvent_SetProperty", &Hook_WonderlandPlayerEvent_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_WonderlandPlayerEvent_SetProperty(lua_State *L)
{
    ///--[Argument List]
    //WonderlandPlayerEvent_SetProperty("Subject ID", iID)
    //WonderlandPlayerEvent_SetProperty("Subject Name", sName)
    //WonderlandPlayerEvent_SetProperty("Emulate Keypress", iKeycode, iTicks)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("WonderlandPlayerEvent_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Instantiated Cases]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    WonderlandPlayerEvent *rEvent = (WonderlandPlayerEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_WONDERLANDPLAYER)) return LuaTypeError("WonderlandPlayerEvent_SetProperty", L);

    //--ID of the Actor to monitor.
    if(!strcasecmp(rSwitchType, "Subject ID") && tArgs == 2)
    {
        rEvent->SetTarget(lua_tointeger(L, 2));
    }
    //--Name of the Actor to monitor. Will attempt to resolve.
    else if(!strcasecmp(rSwitchType, "Subject Name") && tArgs == 2)
    {
        rEvent->SetTargetByName(lua_tostring(L, 2));
    }
    //--Faces a given direction, using the same logic as Move Amount. Diagonals can be face by using both coordinates.
    else if(!strcasecmp(rSwitchType, "Emulate Keypress") && tArgs == 3)
    {
        rEvent->SetEmulateKeypress(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Error.
    else
    {
        LuaPropertyError("WonderlandPlayerEvent_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
