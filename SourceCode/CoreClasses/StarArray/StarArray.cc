//--Base
#include "StarArray.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "MemoryManager.h"

///========================================== System ==============================================
StarArray::StarArray()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= StarArray ======== ]
    //--[Memory Blocks]
    mIsMemoryAllocated = false;
    mMemoryBlocksTotal = 0;
    mMemoryBlocks = NULL;

    //--[Segmentation]
    mAllowSegmentation = true;
    //mSegmentSize = Memory::xPageSize;
    mSegmentSize = 1024 * 128; //128 KiB per block

    //--[Statistics]
    mNominalMemorySize = 0;
    mActualMemorySize = 0;

    ///--[ ================ Construction ================ ]
    //--Error check the segment size just in case the OS messed up.
    if(mSegmentSize < 1) mSegmentSize = 4096; //4 KiB is the standard page size for most architectures.
}
StarArray::~StarArray()
{
    for(int i = 0; i < mMemoryBlocksTotal; i ++) free(mMemoryBlocks[i]);
    free(mMemoryBlocks);
}

///===================================== Property Queries =========================================
bool StarArray::IsReady()
{
    return mIsMemoryAllocated;
}
bool StarArray::IsSegmented()
{
    return mAllowSegmentation;
}
int StarArray::GetTotalBlocks()
{
    return mMemoryBlocksTotal;
}
uint32_t StarArray::GetSegmentSize()
{
    return mSegmentSize;
}
int StarArray::GetNominalMemorySize()
{
    return mNominalMemorySize;
}
int StarArray::GetActualMemorySize()
{
    return mActualMemorySize;
}

///======================================= Manipulators ===========================================
void StarArray::SetSegmentationFlag(bool pFlag)
{
    if(mIsMemoryAllocated) return;
    mAllowSegmentation = pFlag;
}
void StarArray::OverrideSegmentSize(uint32_t pSizePerSegment)
{
    if(mIsMemoryAllocated) return;
    if(pSizePerSegment < 1) return;
    mSegmentSize = pSizePerSegment;
}
uint8_t StarArray::GetByte(uint32_t pPosition)
{
    //--Defaults to 0 if nothing is allocated.
    if(!mIsMemoryAllocated) return 0;

    //--Out of range.
    if(pPosition < 0 || (int)pPosition >= mActualMemorySize) return 0;

    //--Single segment.
    if(!mAllowSegmentation)
    {
        return mMemoryBlocks[0][pPosition];
    }

    //--Segmented. Resolve segment and return.
    int tSegment = pPosition / mSegmentSize;
    int tByte = pPosition % mSegmentSize;
    return mMemoryBlocks[tSegment][tByte];
}

///======================================= Core Methods ===========================================
void StarArray::Allocate(uint32_t pBytes)
{
    ///--[Setup]
    //--If memory is already allocated, fail.
    if(mIsMemoryAllocated) return;

    //--Bytes is zero, do nothing.
    if(pBytes == 0) return;

    ///--[Allocation]
    //--If segmentation is disabled, allocate a single block.
    if(!mAllowSegmentation)
    {
        //--Allocate the requested number of bytes.
        mMemoryBlocksTotal = 1;

        //--Initial allocation.
        SetMemoryData(__FILE__, __LINE__);
        mMemoryBlocks = (uint8_t **)starmemoryalloc(sizeof(uint8_t *) * mMemoryBlocksTotal);
        if(!mMemoryBlocks) return;

        //--Large block allocation.
        mMemoryBlocks[0] = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * pBytes);
        if(!mMemoryBlocks[0])
        {
            free(mMemoryBlocks);
            return;
        }

        //--Track allocated bytes.
        mActualMemorySize = pBytes;
    }
    //--If segmentation is enabled, but the requested space is smaller than a single segment, increase
    //  the allocated size to that of a single segment.
    else if(pBytes <= mSegmentSize)
    {
        //--Allocate a single block of segment size.
        mMemoryBlocksTotal = 1;

        //--Initial allocation.
        SetMemoryData(__FILE__, __LINE__);
        mMemoryBlocks = (uint8_t **)starmemoryalloc(sizeof(uint8_t *) * mMemoryBlocksTotal);
        if(!mMemoryBlocks) return;

        //--Block allocation.
        mMemoryBlocks[0] = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mSegmentSize);
        if(!mMemoryBlocks[0])
        {
            free(mMemoryBlocks);
            return;
        }

        //--Track allocated bytes.
        mActualMemorySize = mSegmentSize;
    }
    //--Segmentation is enabled so allocate multiple segments. All segments are the same size, though
    //  the last one may not be completely filled.
    else
    {
        //--Calculate how many blocks are needed.
        mMemoryBlocksTotal = pBytes / mSegmentSize;

        //--If the modulus is nonzero, then one additional block is allocated but will not be totally filled.
        if(pBytes % mSegmentSize > 0) mMemoryBlocksTotal ++;

        //--Initial allocation.
        SetMemoryData(__FILE__, __LINE__);
        mMemoryBlocks = (uint8_t **)starmemoryalloc(sizeof(uint8_t *) * mMemoryBlocksTotal);
        if(!mMemoryBlocks) return;

        //--Block allocation. If any blocks fail to allocate, the whole thing fails.
        for(int i = 0; i < mMemoryBlocksTotal; i ++)
        {
            mMemoryBlocks[i] = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mSegmentSize);
            if(!mMemoryBlocks[i])
            {
                free(mMemoryBlocks);
                return;
            }
        }

        //--Track allocated bytes.
        mActualMemorySize = mMemoryBlocksTotal * mSegmentSize;
    }

    ///--[Common Flags]
    //--Mark memory as allocated, preventing some flags from being edited.
    mIsMemoryAllocated = true;

    //--Store the nominal number of bytes. This tracks how many bytes the program requested regardless of
    //  how many were actually allocated.
    mNominalMemorySize = pBytes;
}
void StarArray::Reallocate(uint32_t pBytes)
{
    ///--[Documentation]
    //--Given a byte count, reallocates space within the array. Attempts to keep the original memory according
    //  to the C++ standard. Reallocation may not actually decrease the size of the object if less than a "page"
    //  was removed.
    //--If no memory is allocated, just call Allocate().
    if(!mIsMemoryAllocated)
    {
        Allocate(pBytes);
        return;
    }

    //--If asked to allocate zero bytes, deallocate instead.
    if(pBytes < 1)
    {
        Deallocate();
        return;
    }

    ///--[No Segmentation]
    //--If segmentation is disabled, then realloc() can be used.
    if(!mAllowSegmentation)
    {
        //--If the requested size is identical to the current size, do nothing.
        if(mActualMemorySize == (int)pBytes) return;

        //--Store.
        //uint32_t cOldBufSize = mActualMemorySize;
        uint8_t *rOldBuf = mMemoryBlocks[0];

        //--Reallocate.
        mMemoryBlocks[0] = (uint8_t *)realloc(mMemoryBlocks[0], sizeof(uint8_t) * pBytes);

        //--If there was an error, the pointer will come back NULL. According to the C++ standard
        //  this means the original memory was not deallocated. This also necessitates an error message.
        if(!mMemoryBlocks[0])
        {
            fprintf(stderr, "StarArray:Reallocate() - Failed. Unable to allocate single-segment memory of size %i.\n", pBytes);
            mMemoryBlocks[0] = rOldBuf;
        }
        //--On success, store the changed size.
        else
        {
            mActualMemorySize = pBytes;
            mNominalMemorySize = pBytes;
        }
        return;
    }

    ///--[Recheck Size]
    //--First, if the requested byte size is less than a single block, set it to the size of a single block.
    int tStoredSize = pBytes;
    if(pBytes < mSegmentSize) pBytes = mSegmentSize;

    //--Next, check how many blocks are needed.
    int tBlocksNeeded = pBytes / mSegmentSize;
    if(pBytes % mSegmentSize > 0) tBlocksNeeded ++;

    //--Recheck if the size actually needs to change. Reallocations within an already allocated block do not
    //  require any actual executions.
    if(tBlocksNeeded == mMemoryBlocksTotal)
    {
        mNominalMemorySize = tStoredSize;
        return;
    }

    ///--[Size Reduction]
    //--If the requested space is smaller than the currently allocated space, just deallocate excess memory.
    if(tBlocksNeeded < mMemoryBlocksTotal)
    {
        //--Deallocate excess blocks.
        for(int i = tBlocksNeeded; i < mMemoryBlocksTotal; i++)
        {
            free(mMemoryBlocks[i]);
        }

        //--Run the reallocation.
        mMemoryBlocks = (uint8_t **)realloc(mMemoryBlocks, sizeof(uint8_t *) * tBlocksNeeded);

        //--If there was an error, print a warning. This will likely lead to a program crash.
        if(!mMemoryBlocks)
        {
            fprintf(stderr, "StarArray:Reallocate() - Failed to reallocate down in size. Target blocks: %i, Current blocks: %i\n", tBlocksNeeded, mMemoryBlocksTotal);
            return;
        }

        //--Success. Store the changed size.
        mMemoryBlocksTotal = tBlocksNeeded;
        mNominalMemorySize = tStoredSize;
        mActualMemorySize = mMemoryBlocksTotal * mSegmentSize;
    }
    ///--[Size Increase]
    //--If the requested space is larger than the currently allocated space, allocate additional blocks past the end.
    else if(tBlocksNeeded > mMemoryBlocksTotal)
    {
        //--Run the reallocation.
        mMemoryBlocks = (uint8_t **)realloc(mMemoryBlocks, sizeof(uint8_t *) * tBlocksNeeded);

        //--If there was an error, print a warning. This will likely lead to a program crash.
        if(!mMemoryBlocks)
        {
            fprintf(stderr, "StarArray:Reallocate() - Failed to reallocate up in size. Target blocks: %i, Current blocks: %i\n", tBlocksNeeded, mMemoryBlocksTotal);
            return;
        }

        //--Allocate space in the newly sized array.
        for(int i = mMemoryBlocksTotal; i < tBlocksNeeded; i++)
        {
            mMemoryBlocks[i] = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mSegmentSize);
        }

        //--Store the changed size.
        mMemoryBlocksTotal = tBlocksNeeded;
        mNominalMemorySize = tStoredSize;
        mActualMemorySize = mMemoryBlocksTotal * mSegmentSize;
    }
}
void StarArray::Deallocate()
{
    //--Deallocate.
    for(int i = 0; i < mMemoryBlocksTotal; i ++) free(mMemoryBlocks[i]);
    free(mMemoryBlocks);

    //--Null.
    mIsMemoryAllocated = false;
    mMemoryBlocksTotal = 0;
    mMemoryBlocks = NULL;
    mNominalMemorySize = 0;
    mActualMemorySize = 0;
}
void StarArray::WriteToArray(const void *pSource, int pStartLocation, size_t pBytes)
{
    ///--[Documentation]
    //--Given a data source, copies the given number of bytes from the source into this object. Automatically
    //  handles paging.

    ///--[Error Checks]
    //--Memory is not allocated. Do nothing.
    if(!mIsMemoryAllocated) return;

    //--If the source doesn't exist, fail.
    if(!pSource) return;

    //--If the requested bytes is zero then we don't need to do anything.
    if(pBytes < 1) return;

    //--If the start position is a negative, set it to 0.
    if(pStartLocation < 0) pStartLocation = 0;

    //--If the start position is larger than the allocated memory, stop.
    if(pStartLocation >= mNominalMemorySize) return;

    ///--[Reading]
    //--Compute the largest possible read action.
    int tMaxReadBytes = mNominalMemorySize - pStartLocation;
    if((int)pBytes > tMaxReadBytes) pBytes = tMaxReadBytes;

    //--If memory is not split into blocks, then just perform a single write action.
    if(!mAllowSegmentation)
    {
        memcpy(&mMemoryBlocks[0][pStartLocation], pSource, pBytes);
        return;
    }

    //--Memory is split into blocks. Reading needs to be done in a while loop to read to the end.
    int tCursor = 0;
    uint8_t *rSourceAsInt = (uint8_t *)pSource;
    uint8_t *rSourcePos = &rSourceAsInt[0];
    int tRemainingBytes = pBytes;
    int tCurrentBlock = pStartLocation / mSegmentSize;
    int tBlockStartPos = pStartLocation % mSegmentSize;
    while(true)
    {
        //--Compute how much we can read into this block.
        int tReadSize = mSegmentSize - tBlockStartPos;

        //--If the read size exceeds the remaining bytes, set it to the remaining bytes.
        if(tReadSize >= tRemainingBytes)
        {
            tReadSize = tRemainingBytes;
        }

        //--Perform the read.
        memcpy(&mMemoryBlocks[tCurrentBlock][tBlockStartPos], rSourcePos, tReadSize);

        //--Advance the source cursor.
        tCursor = tCursor + tReadSize;
        rSourcePos = &rSourceAsInt[tCursor];

        //--Decrement the remaining bytes by the amount read. If it hits zero, we've read all the data.
        tRemainingBytes = tRemainingBytes - tReadSize;
        if(tRemainingBytes < 1) break;

        //--Next block.
        tCurrentBlock ++;
        tBlockStartPos = 0;
    }
}
void StarArray::ReadFromArray(void *pDestination, int pStartLocation, size_t pBytes)
{
    ///--[Documentation]
    //--Given a destination array, copies the given number of bytes from this object into it. Automatically
    //  handles reading off pages to copy into the destination.
    //--On error, does nothing.

    ///--[Error Checks]
    //--Memory is not allocated. Do nothing.
    if(!mIsMemoryAllocated) return;

    //--No destination.
    if(!pDestination) return;

    //--Asked to read zero bytes.
    if(pBytes < 1) return;

    //--Start location is negative, set to zero.
    if(pStartLocation < 0) pStartLocation = 0;

    //--Start location is past the end of the memory. Do nothing.
    if(pStartLocation >= mNominalMemorySize) return;

    ///--[Copying]
    //--Resolve the largest possible copy that can occur given the size of our memory. The destination
    //  is not checked!
    int tLargestCopy = mNominalMemorySize - pStartLocation;
    if((int)pBytes > tLargestCopy) pBytes = tLargestCopy;

    //--If memory is not split into blocks, then just perform a single copy action.
    if(!mAllowSegmentation)
    {
        memcpy(pDestination, &mMemoryBlocks[0][pStartLocation], pBytes);
        return;
    }

    //--To write ahead we will need to advance the copy position. This is done by casting the void destination
    //  to a byte pointer type.
    int tDestinationOffset = 0;
    uint8_t *rDestinationPtr = (uint8_t *)pDestination;

    //--Memory is split into blocks. This requires a while loop.
    int tRemainingBytes = pBytes;
    int tCurrentBlock = pStartLocation / mSegmentSize;
    int tBlockStartPos = pStartLocation % mSegmentSize;
    while(true)
    {
        //--Compute how much we can read into this block.
        int tWriteSize = mSegmentSize - tBlockStartPos;

        //--If the read size exceeds the remaining bytes, set it to the remaining bytes.
        if(tWriteSize >= tRemainingBytes)
        {
            tWriteSize = tRemainingBytes;
        }

        //--Perform the write.
        memcpy(&rDestinationPtr[tDestinationOffset], &mMemoryBlocks[tCurrentBlock][tBlockStartPos], tWriteSize);
        tDestinationOffset += tWriteSize;

        //--Decrement the remaining bytes by the amount read. If it hits zero, we've read all the data.
        tRemainingBytes = tRemainingBytes - tWriteSize;
        if(tRemainingBytes < 1) break;

        //--Next block.
        tCurrentBlock ++;
        tBlockStartPos = 0;
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
void StarArray::ReadFileIntoMemory(int pStartPosition, size_t pBytes, FILE *pInfile)
{
    ///--[Documentation]
    //--Given a stream, reads from that stream into this object's arrays the given number of bytes.
    //  This will involve reading into multiple memory blocks.

    ///--[Error Checks]
    //--Memory is not allocated. Do nothing.
    if(!mIsMemoryAllocated) return;

    //--If the file doesn't exist, fail.
    if(!pInfile) return;

    //--If the requested bytes is zero then we don't need to do anything.
    if(pBytes < 1) return;

    //--If the start position is a negative, set it to 0.
    if(pStartPosition < 0) pStartPosition = 0;

    //--If the start position is larger than the allocated memory, stop.
    if(pStartPosition >= mNominalMemorySize) return;

    ///--[Reading]
    //--Compute the largest possible read action.
    int tMaxReadBytes = mNominalMemorySize - pStartPosition;
    if((int)pBytes > tMaxReadBytes) pBytes = tMaxReadBytes;

    //--If memory is not split into blocks, then just perform a single read action.
    if(!mAllowSegmentation)
    {
        fread(&mMemoryBlocks[0][pStartPosition], sizeof(uint8_t), pBytes, pInfile);
        return;
    }

    //--Memory is split into blocks. Reading needs to be done in a while loop to read to the end.
    int tRemainingBytes = pBytes;
    int tCurrentBlock = pStartPosition / mSegmentSize;
    int tBlockStartPos = pStartPosition % mSegmentSize;
    while(true)
    {
        //--Compute how much we can read into this block.
        int tReadSize = mSegmentSize - tBlockStartPos;

        //--If the read size exceeds the remaining bytes, set it to the remaining bytes.
        if(tReadSize >= tRemainingBytes)
        {
            tReadSize = tRemainingBytes;
        }

        //--Perform the read.
        fread(&mMemoryBlocks[tCurrentBlock][tBlockStartPos], sizeof(uint8_t), tReadSize, pInfile);

        //--Decrement the remaining bytes by the amount read. If it hits zero, we've read all the data.
        tRemainingBytes = tRemainingBytes - tReadSize;
        if(tRemainingBytes < 1) break;

        //--Next block.
        tCurrentBlock ++;
        tBlockStartPos = 0;
    }
}
void StarArray::WriteFileFromMemory(int pStartPosition, size_t pBytes, FILE *pOutfile)
{
    ///--[Documentation]
    //--Given a file, writes this memory object's data to it using fwrite().

    ///--[Error Checks]
    //--Memory is not allocated. Do nothing.
    if(!mIsMemoryAllocated) return;

    //--If the file doesn't exist, fail.
    if(!pOutfile) return;

    //--If the requested bytes is zero then we don't need to do anything.
    if(pBytes < 1) return;

    //--If the start position is a negative, set it to 0.
    if(pStartPosition < 0) pStartPosition = 0;

    //--If the start position is larger than the allocated memory, stop.
    if(pStartPosition >= mNominalMemorySize) return;

    ///--[Reading]
    //--Compute the largest possible write action.
    int tMaxWriteBytes = mNominalMemorySize - pStartPosition;
    if((int)pBytes > tMaxWriteBytes) pBytes = tMaxWriteBytes;

    //--If memory is not split into blocks, then just perform a single write action.
    if(!mAllowSegmentation)
    {
        fwrite(&mMemoryBlocks[0][pStartPosition], sizeof(uint8_t), pBytes, pOutfile);
        return;
    }

    //--Memory is split into blocks. Writing needs to be done in a while loop.
    int tRemainingBytes = pBytes;
    int tCurrentBlock = pStartPosition / mSegmentSize;
    int tBlockStartPos = pStartPosition % mSegmentSize;
    while(true)
    {
        //--Compute how much we can write into this block.
        int tWriteSize = mSegmentSize - tBlockStartPos;

        //--If the read size exceeds the remaining bytes, set it to the remaining bytes.
        if(tWriteSize >= tRemainingBytes)
        {
            tWriteSize = tRemainingBytes;
        }

        //--Perform the write.
        fwrite(&mMemoryBlocks[tCurrentBlock][tBlockStartPos], sizeof(uint8_t), tWriteSize, pOutfile);

        //--Decrement the remaining bytes by the amount written. If it hits zero, we've written all the data.
        tRemainingBytes = tRemainingBytes - tWriteSize;
        if(tRemainingBytes < 1) break;

        //--Next block.
        tCurrentBlock ++;
        tBlockStartPos = 0;
    }
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
uint8_t **StarArray::GetMemoryAddress()
{
    ///--[Documentation]
    //--The pointer will go out of scope if most other non-read functions are called, if you
    //  are using this for direct access then it's on you to make sure you don't call Deallocate()
    //  and keep trying to use the pointer.
    return mMemoryBlocks;
}
uint8_t *StarArray::LiberateData()
{
    ///--[Documentation]
    //--Returns the zeroth member of the data array and relinquishes ownership of it. This should only
    //  be called immediately before this object is deallocated as it will cause instability. This
    //  is used when the caller needs to keep a permanent copy of the data. It is also meant for
    //  non-segmented memory use, if using segmented memory a copy or file-write action should be used.
    if(!mMemoryBlocks) return NULL;
    uint8_t *rMemAddress = mMemoryBlocks[0];
    mMemoryBlocks[0] = NULL;
    return rMemAddress;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
