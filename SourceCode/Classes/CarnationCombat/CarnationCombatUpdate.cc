//--Base
#include "CarnationCombat.h"

//--Classes
#include "AdventureLevel.h"
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatJob.h"
#include "AdvCombatEntity.h"
#include "AdventureMenu.h"
#include "CarnationCombatEntity.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///--[Debug]
//#define CARNATIONCOMBAT_UPDATE_DEBUG
#ifdef CARNATIONCOMBAT_UPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

void CarnationCombat::Update()
{
    ///--[Documentation]
    //--Handles the combat update. Combat runs a set of timers then handles controls or logic updates, depending
    //  on its internal state.
    DebugPush(mIsActive, "Carnation Combat Update: Begin.\n");

    ///--[Pulsing]
    //--If the AdventureLevel is still pulsing, this object will be active but should not do anything.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsPulsing()) { DebugPop("Level pulsing, exited normally.\n"); return; }

    ///--[Stop Non-Active Update]
    //--If the object is not active, update stops at this point.
    if(!mIsActive) { DebugPop("Object not active, exited normally.\n"); return; }

    ///--[Post-Reinitialize]
    //--After combat called Reinitialize(), this flag will be true. Once all enemies have registered themselves
    //  and all setup is done, this will be called.
    if(mCombatReinitialized)
    {
        //--Unset flag.
        DebugPrint("Running post-reinitialize.\n");
        mCombatReinitialized = false;

        //--Player's party resets their jobs. This is also where Script Response and Ability Script codes are
        //  called, and any other setup takes place.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
        while(rEntity)
        {
            //--Call subroutine.
            rEntity->HandleCombatStart();

            //--Next.
            rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
        }

        //--Debug.
        DebugPrint("Finished post-reinitialize.\n");
    }

    ///--[Timers]
    //--Debug.
    DebugPrint("Running timers.\n");

    //--Background timer.
    mBackgroundTimer ++;

    //--This timer runs if not in a victory case, but only after the background has run enough.
    if(mBackgroundTimer > CC_BG_FADEOUT_STOP && mCombatResolution != ADVCOMBAT_END_VICTORY)
    {
        if(mLetterboxTimer < CC_BG_LETTERBOX_TICKS) mLetterboxTimer ++;
    }
    else
    {
        if(mLetterboxTimer > 0) mLetterboxTimer --;
    }

    //--Reset the application scatter counter.
    mTextScatterCounter = 0;

    //--Turn display timer. Indicates what turn it is, auto-caps out and stops rendering.
    if(mTurnDisplayTimer < ACE_TURN_DISPLAY_TICKS) mTurnDisplayTimer ++;

    //--Title timer.
    if(mShowTitle)
    {
        mTitleTicks ++;
        if(mTitleTicks >= mTitleTicksMax) mShowTitle = false;
    }

    //--Run the turn-order length handler.
    if(mTurnBarMoveTimer < mTurnBarMoveTimerMax)
    {
        //--Increment.
        mTurnBarMoveTimer ++;

        //--Recompute size.
        float cPercent = EasingFunction::QuadraticInOut(mTurnBarMoveTimer, mTurnBarMoveTimerMax);
        mTurnWidCurrent = mTurnWidStart + ((mTurnWidTarget - mTurnWidStart) * cPercent);
    }

    //--Turn order slider.
    if(mTurnBarNewTurnTimer < mTurnBarNewTurnTimerMax)
    {
        mTurnBarNewTurnTimer ++;
    }

    //--Prediction box timers.
    UpdatePredictionBoxes();

    //--Confirmation window timer.
    StarUIPiece::StandardTimer(mConfirmationTimer,    0, ADVCOMBAT_CONFIRMATION_TICKS,      mIsShowingConfirmationWindow);
    StarUIPiece::StandardTimer(mPlayerCardTimer,      0, ADVCOMBAT_PLAYER_INTERFACE_TICKS, (mPlayerMode == ADVCOMBAT_INPUT_CARDS));
    StarUIPiece::StandardTimer(mPlayerJobTimer,       0, ADVCOMBAT_PLAYER_INTERFACE_TICKS, (mPlayerMode == ADVCOMBAT_INPUT_JOB));
    StarUIPiece::StandardTimer(mPlayerMemorizedTimer, 0, ADVCOMBAT_PLAYER_INTERFACE_TICKS, (mPlayerMode == ADVCOMBAT_INPUT_MEMORIZED));
    StarUIPiece::StandardTimer(mPlayerTacticsTimer,   0, ADVCOMBAT_PLAYER_INTERFACE_TICKS, (mPlayerMode == ADVCOMBAT_INPUT_TACTICS));
    StarUIPiece::StandardTimer(mExpertPageSwapTime,   0, ADVCOMBAT_PAGE_CHANGE_TICKS,      (mExpertSecondPage));

    //--Text Pause timer. After a line is finished displaying, pauses briefly before the next
    //  event fires.
    if(mTextPauseTimer > 0)
    {
        //--If this flag is set, the pause will not decrement until a key is pressed.
        if(mTextTimerWaitForKeypress)
        {
            if(ControlManager::Fetch()->IsFirstPress("Activate") || ControlManager::Fetch()->IsFirstPress("MouseLft"))
            {
                mTextTimerWaitForKeypress = false;
                mTextPauseTimer = 0;
                mStatusPrintBlock = false;
            }
        }
        //--Run the timer.
        else
        {
            mTextPauseTimer --;
        }
    }

    //--Text timer.
    if(mTextTimer < mTextTimerMax)
    {
        //--Increment.
        mTextTimer ++;

        //--Run any tags/applications on the letters just revealed.
        RunLetterPackages();

        //--If the timer hits its current cap, initiate a pause.
        if(mTextTimer >= mTextTimerMax)
        {
            mTextPauseTimer = ResolvePauseTime();
        }
    }

    //--Command timers.
    if(mPartyDecisionMode)
    {
        int tIndex = 0;
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rEntity)
        {
            //--Highlighted:
            if(mPartyDecisionIndex == tIndex)
            {
                int tCommandTimer = rEntity->GetCommandTimer();
                if(tCommandTimer < CC_ACTIONSELECT_TICKS) rEntity->SetCommandTimer(tCommandTimer + 1);
            }
            //--Not acting.
            else
            {
                int tCommandTimer = rEntity->GetCommandTimer();
                if(tCommandTimer > 0) rEntity->SetCommandTimer(tCommandTimer - 1);
            }

            //--Next.
            tIndex ++;
            rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }
    }
    //--Characters are never acting. Zero all timers.
    else
    {
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rEntity)
        {
            //--Decrement.
            int tCommandTimer = rEntity->GetCommandTimer();
            if(tCommandTimer > 0) rEntity->SetCommandTimer(tCommandTimer - 1);

            //--Next.
            rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }
    }

    //--Arrow timer, always runs.
    mExpertArrowTimer ++;
    if(mExpertArrowTimer >= ADVCOMBAT_ARROW_OSCILLATE_TICKS) mExpertArrowTimer = 0;

    //--Player's highlight cursor. Auto-clamps.
    mAbilityHighlightPos.Increment(EASING_CODE_QUADOUT);
    mAbilityHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--All entities in both rosters update their movement coordinates as needed. This is also where they update timers.
    bool tAnyEntitiesMoving = false;
    bool tAnyEntitiesFinishedKnockout = false;
    AdvCombatEntity *rUpdateEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rUpdateEntity)
    {
        //--Timers.
        rUpdateEntity->UpdatePosition();
        rUpdateEntity->UpdateTimers();

        //--Knockout handler. This will cause the entity to stop rendering until it is safely offscreen
        //  or has otherwise stopped moving. The player's combat party moves offscreen when KO'd.
        if(rUpdateEntity->IsKnockingOut() && rUpdateEntity->IsKnockoutFinished())
        {
            rUpdateEntity->FinishKnockout();
            rUpdateEntity->SetBlockRenderForKO(true);
        }

        //--Flag for movement.
        tAnyEntitiesMoving = (tAnyEntitiesMoving || rUpdateEntity->IsMoving() || rUpdateEntity->IsKnockingOut());

        //--Next.
        rUpdateEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
    rUpdateEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rUpdateEntity)
    {
        //--Timers.
        rUpdateEntity->UpdatePosition();
        rUpdateEntity->UpdateTimers();

        //--Knockout handler. Enemies get moved to the graveyard list.
        if(rUpdateEntity->IsKnockingOut() && rUpdateEntity->IsKnockoutFinished())
        {
            //--Finish the internal counter.
            rUpdateEntity->FinishKnockout();
            tAnyEntitiesFinishedKnockout = true;

            //--Run response scripts. Typically only AIs reply to this. The entity in question
            //  is placed for query.
            rKnockoutEntity = rUpdateEntity;
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_AIKNOCKOUT);
            rKnockoutEntity = NULL;

            //--Remove from active party.
            mrEnemyCombatParty->RemoveElementP(rUpdateEntity);

            //--Add to graveyard.
            rUpdateEntity->SetTurnKOd(mCurrentTurn);
            mrEnemyGraveyard->AddElement(mEnemyRoster->GetIteratorName(), rUpdateEntity);
        }

        //--Flag for movement.
        tAnyEntitiesMoving = (tAnyEntitiesMoving || rUpdateEntity->IsMoving() || rUpdateEntity->IsKnockingOut());

        //--Next.
        rUpdateEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--Target timer.
    if(mIsSelectingTargets)
    {
        mTargetFlashTimer ++;
        if(mTargetFlashTimer >= ADVCOMBAT_TARGET_FLASH_PERIOD) mTargetFlashTimer = 0;
    }
    //--Clear.
    else
    {
        mTargetFlashTimer = 0;
    }

    //--Run this timer.
    mPlayerCPTimer ++;
    mPlayerCPFrameTimer ++;

    //--Description timer.
    if(mShowDetailedAbilityDescriptions != ADVCOMBAT_DESCRIPTIONS_HIDDEN)
    {
        if(mPlayerDescriptionTimer > 0) mPlayerDescriptionTimer --;
    }
    else
    {
        if(mPlayerDescriptionTimer < ADVCOMBAT_DESCRIPTION_TICKS) mPlayerDescriptionTimer ++;
    }

    //--Update all animations.
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->PushIterator();
    while(rAnimation)
    {
        rAnimation->Update();
        rAnimation = (AdvCombatAnimation *)mCombatAnimations->AutoIterate();
    }

    //--Update all combat text.
    CombatTextPack *rTextPack = (CombatTextPack *)mTextPackages->SetToHeadAndReturn();
    while(rTextPack)
    {
        rTextPack->mTicks ++;
        if(rTextPack->mTicks >= rTextPack->mTicksMax) mTextPackages->RemoveRandomPointerEntry();
        rTextPack = (CombatTextPack *)mTextPackages->IncrementAndGetRandomPointerEntry();
    }

    //--If the entity has moved offscreen and was knocked out, it will unset the block flag. This means
    //  that the entity can move onscreen for revival later.
    rUpdateEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rUpdateEntity)
    {
        if(rUpdateEntity->IsBlockRenderForKO() && rUpdateEntity->GetCombatX() < -50.0f)
        {
            rUpdateEntity->SetBlockRenderForKO(false);
        }
        rUpdateEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Debug.
    DebugPrint("Finished timers.\n");

    //--If any entities finished their knockout animation, move other enemies to compensate.
    if(tAnyEntitiesFinishedKnockout)
    {
        tAnyEntitiesMoving = true;
        PositionEnemies();
    }

    ///--[Combat Text]
    //--If the combat text has not finished running, block the rest of the update. The player can press
    //  the activate key to instantly finish the text. This only happens during normal play.
    if(mTextTimer < mTextTimerMax)
    {
        ControlManager *rControlManager = ControlManager::Fetch();
        if(rControlManager->IsFirstPress("Activate") || rControlManager->IsFirstPress("MouseLft"))
        {
            mTextTimer = mTextTimerMax-1;
        }
        return;
    }
    else if(mTextPauseTimer > 0)
    {
        return;
    }

    ///--[Cutscene Handling]
    //--If the cutscene manager has any events, or the world dialogue is visible:
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsManagingUpdates() || rCutsceneManager->HasAnyEvents())
    {
        //--Debug.
        DebugPrint("Updating world dialogue.\n");

        //--Run the dialogue.
        rWorldDialogue->Update();

        //--Cutscenes can also update here.
        if(rCutsceneManager->HasAnyEvents())
        {
            //--Run the update.
            rCutsceneManager->Update();
        }

        //--If stopping events, stop here.
        if(rWorldDialogue->IsStoppingEvents())
        {
            if(mIsShowingCombatInspector) DeactivateInspector();
            DebugPop("Updated normally, exited from dialogue.\n");
            return;
        }
    }

    //--Combat inspector.
    bool tWasShowingInspector = mIsShowingCombatInspector;
    UpdateInspector();

    ///--[List Building]
    //--Rebuild any/all effect references.
    DebugPrint("Rebuilding effect references.\n");
    RebuildEffectReferences();

    ///--[Subhandlers]
    //--Subhandlers take control away from the usual combat update.
    DebugPrint("Running subhandlers.\n");
    if(mCombatResolution != ADVCOMBAT_END_NONE) { UpdateResolution();   DebugPop("Updated resolution, exited normally.\n"); return; }
    if(mIsIntroduction)                         { UpdateIntroduction(); DebugPop("Updated introduction, exited normally.\n"); return; }
    if(mIsShowingConfirmationWindow)            { UpdateConfirmation(); DebugPop("Updated confirmation exited normally.\n"); return; }
    if(tWasShowingInspector)                    { DebugPop("Updated normally, exited from combat inspector.\n"); return; }
    DebugPrint("Finished subhandlers.\n");

    //--Application packs don't run if entities are moving!
    if(tAnyEntitiesMoving)
    {
        //--If the player was acting, then we can allow the player's interface to keep operating even if entities are moving.
        if(mIsPerformingPlayerUpdate)
        {
            DebugPrint("Handling player controls.\n");
            HandlePlayerControls(true);
            DebugPrint("Finished player controls.\n");
        }

        DebugPop("Updated. Waiting for entities to finish moving.\n");
        return;
    }

    ///--[Application]
    //--Application packs are what changes HP/MP/Effects in an organized fashion. The effect (whatever it is)
    //  applies when the timer hits zero.
    bool tAnyOneApplicationRunning = false;
    DebugPrint("Running application packages.\n");

    //--If any applications are in the queue, hide the player's interface.
    if(mApplicationQueue->GetListSize() > 0 || mEventQueue->GetListSize() > 0)
    {
        if(mPlayerInterfaceTimer > 0) mPlayerInterfaceTimer --;
    }

    //--Run applications.
    ApplicationPack *rPackage = (ApplicationPack *)mApplicationQueue->SetToHeadAndReturn();
    while(rPackage)
    {
        //--Decrement.
        rPackage->mTicks --;

        //--Ending case.
        if(rPackage->mTicks < 0)
        {
            //--Run responses to the application. They can query the application both before and after.
            rCurrentApplicationPack = rPackage;
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY);
            HandleApplication(rPackage->mOriginatorID, rPackage->mTargetID, rPackage->mEffectString);
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY);

            //--Clean.
            rCurrentApplicationPack = NULL;
            mApplicationQueue->RemoveRandomPointerEntry();
        }
        else
        {
            tAnyOneApplicationRunning = true;
        }

        //--Next.
        rPackage = (ApplicationPack *)mApplicationQueue->IncrementAndGetRandomPointerEntry();
    }
    DebugPrint("Finished application packages.\n");

    ///--[Events]
    //--Debug.
    DebugPrint("Running event handlers.\n");
    if(tAnyOneApplicationRunning)
    {
        DebugPop("Updated. Waiting for applications to finish.\n");
        return;
    }

    //--Check if any applications scored a KO.
    if(CheckKnockouts())
    {
        DebugPop("Updated. Handling knockout SFX.\n");
        PlayMonsterKnockout();
        return;
    }

    //--If any events are in the queue, handle those.
    CombatEventPack *rEventPackage = (CombatEventPack *)mEventQueue->GetElementBySlot(0);
    while(rEventPackage)
    {
        //--On the 0th tick, execute the event's ability script.
        if(mEventFirstTick)
        {
            //--Ask the event if it can still run using code ACA_SCRIPT_CODE_QUERY_CAN_RUN. Only the
            //  ability itself runs this, no other scripts get to respond.
            mEventCanRun = false;
            if(rEventPackage->rAbility)
            {
                //--Set flags. By default, the event fails and the script must mark it as functional.
                rFiringEventPackage = rEventPackage;
                rEventPackage->rAbility->CallCode(ACA_SCRIPT_CODE_QUERY_CAN_RUN);
                rFiringEventPackage = NULL;
            }

            //--If we got this far, the event can run.
            mEventFirstTick = false;
            mEventTimer = 0;
            mEntitiesHaveRepositioned = false;

            //--Order all entities involved to get onscreen.
            if(mEventCanRun)
            {
                PositionCharactersOnScreenForEvent(rEventPackage);
            }
            //--Event cannot run, so all party members need to go offscreen.
            else
            {
                PositionCharactersOffScreen();
            }
        }

        //--If any entities are repositioning, stop the update.
        /*if(IsAnyoneMoving())
        {
            DebugPop("Event cycle stopped due to entities move.\n");
            return;
        }*/

        //--If this flag is still false, but nobody is moving, the entities have repositioned. We can call the event now.
        if(!mEntitiesHaveRepositioned)
        {
            //--Flag.
            mEntitiesHaveRepositioned = true;

            //--Run the event, if it can run.
            if(rEventPackage->rAbility && mEventCanRun)
            {
                rFiringEventPackage = rEventPackage;
                rEventPackage->rAbility->CallCode(ACA_SCRIPT_CODE_EXECUTE);
                rFiringEventPackage = NULL;
            }

            //--If the event timer does not reach the minimum ticks, set it to that. This makes sure the UI has time to scroll
            //  offscreen.
            if(mEventTimer < ADVCOMBAT_EVENT_TICKS_MINIMUM)
            {
                mEventTimer = ADVCOMBAT_EVENT_TICKS_MINIMUM;
            }
        }

        //--Decrement this timer.
        //mEventTimer --;

        //--Special Code: To prevent needless dragging, if there is only one event present and the timer is above ADVCOMBAT_MIN_EVENT_ENDING_TICKS,
        //  lock it to ADVCOMBAT_MIN_EVENT_ENDING_TICKS ticks.
        if(mEventQueue->GetListSize() == 1 && mEventTimer > ADVCOMBAT_MIN_EVENT_ENDING_TICKS) mEventTimer = ADVCOMBAT_MIN_EVENT_ENDING_TICKS;

        //--End. Start the next event.
        //if(mEventTimer < 1)
        if(mTextTimer >= mTextTimerMax)
        {
            //--If there is no next event, but mAnyAbilityReplied was triggered, this is a multi-part ability. In that
            //  case, increment the ability counter by 1 and reset the flag, then re-run the ability.
            if(mAnyAbilityReplied)
            {
                mEntitiesHaveRepositioned = false;
                mAnyAbilityReplied = false;
                mAbilityExecPhase ++;
                DebugPop("Event cycle running, exited normally.\n");
                return;
            }

            //--Pop off the event.
            mEventFirstTick = true;
            mEventQueue->RemoveElementI(0);

            //--Set the next event.
            rEventPackage = (CombatEventPack *)mEventQueue->GetElementBySlot(0);

            //--If there's no next event, mark the action as completed. This only occurs if the acting character
            //  actually acted, toggling the mNoEventsEndsAction flag to true.
            //--If a passive effect or ability caused an event to occur, then we don't end the character's turn.
            if(!rEventPackage && mNoEventsEndsAction)
            {
                //--Finish the action.
                CompleteAction();

                //--If handling an effortless action, clear the flag and reset the command stack.
                if(mIsHandlingEffortlessAction)
                {
                    //--Flags.
                    mIsSelectingTargets = false;
                    mSelecterBuiltCommands = false;
                    mIsHandlingEffortlessAction = false;
                    mrPlayerCommandStack->ClearList();
                    SetHitboxesResolve();
                    mAbilityExecPhase = 0;
                    rEffortlessActionCharacter = NULL;

                    //--Run the statistics script. This will reset the target list in the statistics pack.
                    CallStatisticsScript(ACS_SCRIPT_CODE_ACTIONSTART);
                    mrTurnOrder->DeleteHead();
                }
            }

            //--Check if any entities got knocked out. If they did, stop here. Give them time to animate the KO
            //  before moving to the next event.
            if(CheckKnockouts())
            {
                PlayMonsterKnockout();
                DebugPop("Event cycle running, exited due to knockout.\n");
                return;
            }

            //--Run the next event, if it exists.
            continue;
        }
        //--Otherwise, end the update here.
        else
        {
            DebugPop("Event cycle running, exited normally.\n");
            return;
        }
    }

    //--Debug.
    DebugPrint("Finished event handlers.\n");

    //--If, after responses are done, text is running, stop.
    if(mTextTimer < mTextTimerMax)
    {
        DebugPop("Finished event cycle, spawned new text before turn ended. Stopping.\n");
        return;
    }

    ///--[Turn Resolution]
    //--This control loop will start turns and actions until someone is able to handle the action, or the battle is over.
    //  If 10 turns start with no resolution and nobody able to act, the engine assumes it jammed. Note that a stunned
    //  entity is still considered to be acting even if they will pass their turn, so this should not theoretically occur.
    DebugPrint("Checking turn resolution.\n");
    int tTurnsStarted = 0;
    while(true)
    {
        //--Block if anything is on the dialogue or cutscene. This happens even if the cutscene managed isn't necessarily
        //  stopping events.
        if(rWorldDialogue->IsVisible()) return;
        if(rCutsceneManager->HasAnyEvents()) return;

        //--Application is occurring.
        if(mApplicationQueue->GetListSize() > 0)
        {
            DebugPop("Application queue gained event after other action, exited.\n");
            return;
        }

        //--If we reach this point and an event is occurring, it may have been added. Wait until it's done before resuming.
        if(mEventQueue->GetListSize() > 0)
        {
            DebugPop("Event queue gained event after other action, exited.\n");
            return;
        }

        //--Victory condition check. First, is the player defeated? This gets priority. Even if the player and enemy
        //  are defeated on the same turn, the player still loses.
        if(IsPlayerDefeated())
        {
            BeginResolutionSequence(ADVCOMBAT_END_DEFEAT);
            DebugPop("Beginning defeat sequence, exited.\n");
            return;
        }

        //--Is the enemy defeated?
        if(IsEnemyDefeated())
        {
            BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
            DebugPop("Beginning victory sequence, exited.\n");
            return;
        }

        //--If a turn is active, run the end-of-turn code. If any events fire, those get handled and we return out. The next
        //  time this code will be bypassed and a new turn will begin.
        if(mIsTurnActive)
        {
            EndTurn();
            continue;
        }

        //--If the turn order list is empty, start a new turn. This will be the case immediately after the
        //  intro has concluded, as well.
        if(mrTurnOrder->GetListSize() < 1)
        {
            //--If there are entities that are unable to act during a turn for 10 turns in a row, but the battle
            //  is not concluded, break out here.
            if(tTurnsStarted >= 10)
            {
                DebugManager::ForcePrint("AdvCombat:Update - Warning, combat loop jammed for 10 turns without concluding.\n");
                BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
                DebugPop("Combat loop jammed, exiting.\n");
                return;
            }

            //--Begin a new turn and flag that we did so. If the control loop starts 2 new turns but somehow does
            //  not conclude, it got jammed.
            tTurnsStarted ++;
            BeginTurn();
            continue;
        }

        //--If no action is taking place, begin an action. This breaks out of the control loop, and allows the acting
        //  entity to get on with the business of acting.
        if(!mIsActionOccurring)
        {
            mAnyAbilityReplied = false;
            mAbilityExecPhase = 0;
            BeginAction();
            break;
        }

        //--If the action is not concluding, and is occurring, begin normal action handling. This occurs on successive ticks
        //  while the player is making a decision.
        if(mIsActionOccurring) break;
    }

    //--Debug.
    DebugPrint("Finished turn resolution loop.\n");

    ///--[Cutscene Running]
    //--Scripts may optionally run here. One script is run per tick.
    char *rZerothScript = (char *)mCutsceneExecList->GetHead();
    if(rZerothScript)
    {
        //--If anyone is moving, don't run it.
        if(IsAnyoneMoving()) return;

        //--Run it.
        LuaManager::Fetch()->ExecuteLuaFile(rZerothScript);

        //--Remove the zeroth script and stop execution.
        mCutsceneExecList->RemoveElementI(0);
        return;
    }

    ///--[Player Action Input]
    //--At the start of the turn, the player inputs the actions they want their party to perform.
    if(mPartyDecisionMode)
    {
        //--Check if the index is past the end of the party count. If so, start executing the turn.
        if(mPartyDecisionIndex >= mrCombatParty->GetListSize())
        {
            mPartyDecisionMode = false;
            return;
        }

        //--If the selecting character hasn't built their command list, do that now:
        if(!mSelecterBuiltCommands)
        {
            //--Flag.
            mSelecterBuiltCommands = true;

            //--Get the entity. If it exists, order its job to re-run the begin action code.
            AdvCombatEntity *rEntity = ResolveDecidingEntity();
            if(rEntity)
            {
                AdvCombatJob *rActiveJob = rEntity->GetActiveJob();
                if(rActiveJob)
                {
                    rActiveJob->CallScript(ADVCJOB_CODE_BEGINACTION);
                }
            }
            else
            {
                mPartyDecisionMode = false;
                return;
            }
        }

        //--Run timer.
        if(mPartyDecisionTimer < CC_ACTIONSELECT_TICKS) mPartyDecisionTimer ++;

        //--Handle the player's controls.
        mIsPerformingPlayerUpdate = true;
        DebugPrint("Handling player controls.\n");
        HandlePlayerControls(false);
        DebugPrint("Finished player controls.\n");
        DebugPop("Handling player controls. Exiting.\n");
        return;
    }

    ///--[Acting Entity]
    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) { DebugPop("No acting entity, exiting.\n"); return; }

    ///--[AI Handling]
    //--If the entity has an AI controlling it:
    if(rActingEntity->GetAIPath() != NULL)
    {
        //--Stop updating for AIs if anyone is moving or anything is animating. This causes a nice breather
        //  between actions but is not strictly necessary.
        if(mPlayerInterfaceTimer > 0) mPlayerInterfaceTimer --;
        if(IsAnyoneMoving() || IsAnythingAnimating() || mPlayerInterfaceTimer > 0)
        {
            DebugPop("Pausing AI for animations.\n");
            return;
        }

        //--Set this flag to false.
        mAIHandledAction = false;

        //--Clear any outstanding variables.
        ClearTargetClusters();

        //--Execute the AI. It should decide what action to take. This is the only time this code should
        //  be called, as it is only there to decide actions.
        AdvCombat::xIsAISpawningEvents = true;
        const char *rPath = rActingEntity->GetAIPath();
        LuaManager::Fetch()->PushExecPop(rActingEntity, rPath, 1, "N", (float)ACE_AI_SCRIPT_CODE_DECIDE_ACTION);
        AdvCombat::xIsAISpawningEvents = false;

        //--If the AI spawned at least one event, it handled the update. If it did not, we spawn a "Pass Turn"
        //  action for the AI. This is to prevent infinite loops.
        if(mEventQueue->GetListSize() < 1)
        {
            ExecuteAbility(mSysAbilityPassTurn, NULL);
        }
    }
    ///--[Player Handling]
    //--If there is no commanding AI, the player inputs commands here. This gets its own file.
    else
    {
        //--If the player character got KO'd, end their turn.
        if(rActingEntity->GetHealth() < 1)
        {
            ExecuteAbility(mSysAbilityPassTurn, NULL);
        }
        //--Locate the action package associated with this entity, and fire it.
        else
        {
            //--Flag to make sure an action was performed.
            bool tFoundAction = false;

            //--Scan.
            CarnationAction *rCheckPtr = (CarnationAction *)mPartyActions->PushIterator();
            while(rCheckPtr)
            {
                if(rCheckPtr->rOwner == rActingEntity)
                {
                    //--Flags.
                    tFoundAction = true;
                    rActiveAbility = rCheckPtr->rAbility;
                    rActiveTargetCluster = rCheckPtr->rTarget;

                    //--Special: Check if all the targets in the cluster are dead. If they are, order the ability
                    //  to run targeting again and pick the zeroth target cluster. This can occur if the player
                    //  kills an enemy and has more attacks aimed at them.
                    //--In addition, this will check and remove targets who died in case a splash-damage attack
                    //  is being used. Otherwise it would hit a dead enemy.
                    bool tAnyTargetsAlive = false;
                    AdvCombatEntity *rTargetCheck = (AdvCombatEntity *)rActiveTargetCluster->mrTargetList->SetToHeadAndReturn();
                    while(rTargetCheck)
                    {
                        //--If the entity has 1 HP or more, it is alive.
                        if(rTargetCheck->GetHealth() > 0)
                        {
                            tAnyTargetsAlive = true;
                        }
                        //--If this target has 0 HP, it is dead. Remove it from the target cluster.
                        else
                        {
                            rActiveTargetCluster->mrTargetList->RemoveRandomPointerEntry();
                        }

                        //--Next.
                        rTargetCheck = (AdvCombatEntity *)rActiveTargetCluster->mrTargetList->IncrementAndGetRandomPointerEntry();
                    }

                    //--Everyone is dead!
                    if(!tAnyTargetsAlive)
                    {
                        //--If the ability in question requires permission to fire, do nothing.
                        if(rActiveAbility->ActivatesConfirmation())
                        {

                        }
                        //--Does not need confirmation. Use.
                        else
                        {
                            //--Clear target info and call the painting script.
                            ClearTargetClusters();
                            rActiveAbility->CallCode(ACA_SCRIPT_CODE_PAINTTARGETS);

                            //--Deallocate the previous target cluster.
                            delete rCheckPtr->rTarget;
                            rCheckPtr->rTarget = NULL;

                            //--Set it to the zeroth cluster.
                            rCheckPtr->rTarget = (TargetCluster *)mTargetClusters->GetElementBySlot(0);
                            rActiveTargetCluster = rCheckPtr->rTarget;

                            //--Take ownership of the target cluster.
                            mTargetClusters->SetRandomPointerToThis(rCheckPtr->rTarget);
                            mTargetClusters->LiberateRandomPointerEntry();
                        }
                    }

                    //--Run the entity's response script, checking if a status effect is intercepting
                    //  ability execution. If so, a flag will be tripped.
                    mStatusInterceptFlag = false;
                    const char *rExecScript = rActingEntity->GetResponsePath();
                    LuaManager::Fetch()->PushExecPop(rActingEntity, rExecScript, 1, "N", (float)CCE_SCRIPT_CALL_CHECK_STATUS_INTERCEPT);

                    //--Flag not tripped, execute.
                    if(!mStatusInterceptFlag)
                    {
                        ExecuteActiveAbility();
                    }
                    //--Flag was tripped, run the "Pass Turn" ability in addition to whatever the entity
                    //  did in its response.
                    else
                    {
                        ExecuteAbility(mSysAbilityPassTurn, NULL);
                    }
                }
                rCheckPtr = (CarnationAction *)mPartyActions->AutoIterate();
            }

            //--No actions found. Find the character's "Fail" action and use that.
            if(!tFoundAction)
            {
                //--Execute the pass-turn ability.
                ExecuteAbility(mSysAbilityPassTurn, NULL);

                //--Call the Status Intercept check before the fail-to-act check. It may be the case that a status
                //  effect was going to stop the action anyway.
                mStatusInterceptFlag = false;
                const char *rExecScript = rActingEntity->GetResponsePath();
                LuaManager::Fetch()->PushExecPop(rActingEntity, rExecScript, 1, "N", (float)CCE_SCRIPT_CALL_CHECK_STATUS_INTERCEPT);

                //--If the status check didn't handle the call, run the fail-to-act check.
                if(!mStatusInterceptFlag)
                {
                    LuaManager::Fetch()->PushExecPop(rActingEntity, rExecScript, 1, "N", (float)CCE_SCRIPT_CALL_FAILED_TO_ACT);
                }
            }
        }
    }

    //--Debug.
    DebugPop("Reached end of update function, exited normally.\n");
}
void CarnationCombat::HandlePlayerControls(bool pAnyEntitiesMoving)
{
    ///--[ ====== Documentation and Setup ===== ]
    //--Handles player controls. Overrides the base version, which uses cards/an ability grid. This
    //  reroutes control handling to the command window, if applicable.
    //--Unlike the AbyssCombat version, this stores the ability/target in a list. Abilities are selected
    //  at turn start, but execute by Initiative score.
    ControlManager *rControlManager = ControlManager::Fetch();
    DebugPrint("Handling player controls\n");

    ///--[Timers]
    StarUIPiece::StandardTimer(mAbilityDescriptionTimer, 0, ABYCOM_ABILITY_DESCRIPTION_TICKS, mShowAbilityDescription);

    ///--[Tutorial]
    //--If the tutorial is showing, it disables all other input. Any keypress will instead dismiss or
    //  advance the tutorial.
    if(mIsShowingTutorialOverlay)
    {
        if(rControlManager->IsAnyKeyPressed() && mTutorialTimer > (ABYCOM_TUTORIAL_FADE_TICKS / 4)) mIsShowingTutorialOverlay = false;
        return;
    }

    //--Other blockers:
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rWorldDialogue->IsVisible()) return;
    if(rCutsceneManager->HasAnyEvents()) return;

    ///--[First tick]
    //--If this is the first tick that the player has gained control. It is implied, as this is the first
    //  possible tick, that target selection is not taking place.
    if(mActionFirstTick)
    {
        //--Unset.
        mShowAbilityDescription = false;
        mActionFirstTick = false;

        //--Set description.
        ReresolveDescriptionAbility();
    }

    ///--[Combat Inspector]
    //--Can be triggered at any time during the player's turn.
    if(rControlManager->IsFirstPress("F1"))
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        ActivateInspector();
        return;
    }

    ///--[Description Toggle]
    //--Shows description of the selected ability. Can be toggled.
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowAbilityDescription = !mShowAbilityDescription;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[UI Toggle]
    //--The player can toggle the UI on and off at any time.
    if(rControlManager->IsFirstPress("F4"))
    {
        mHideCombatUI = !mHideCombatUI;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Debug]
    //--If the player presses the 9 key, they win! This is for debug.
    if(rControlManager->IsFirstPress("Key_9"))
    {
        BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
        return;
    }
    if(rControlManager->IsFirstPress("Key_8"))
    {
        BeginResolutionSequence(ADVCOMBAT_END_DEFEAT);
        return;
    }

    ///--[Debug]
    //--Debug keys here.

    ///--[ ========== Hitbox Resolve ========== ]
    //--Debug.
    DebugPrint("Resolving hitboxes.\n");

    //--Mouse Storage.
    rControlManager->GetMouseCoords(mMouseX, mMouseY, mMouseZ);

    //--Hitbox scanning.
    int i = 0;
    int tUseCursor = -1;
    CarnButton *rButton = (CarnButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--If the mouse is in this hitbox, set it as the in-use cursor.
        if(rButton->mPosition.IsPointWithin(mMouseX, mMouseY))
        {
            tUseCursor = i;
            mButtonList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rButton = (CarnButton *)mButtonList->AutoIterate();
    }

    //--Add the command offset for scrollable windows to get the final actual cursor.
    //if(tUseCursor != -1) tUseCursor = tUseCursor + mPlayerCommandOffset;

    ///--[ ========= Ability Selection ======== ]
    //--Selecting which ability to use this action.
    if(!mIsSelectingTargets)
    {
        //--Debug.
        DebugPrint("Ability selection mode.\n");

        //--Recheck cursor.
        bool tRecompute = (tUseCursor != mPlayerCommandCursor);
        mPlayerCommandCursor = tUseCursor;

        //--If needed, recompute the description ability.
        if(tRecompute)
        {
            //--Locate whatever command is under the cursor and store it for display.
            ReresolveDescriptionAbility();

            //--SFX.
            if(mPlayerCommandCursor != -1) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Special: The 4 key sets Chris' influence to max and HP to 1.
        if(rControlManager->IsFirstPress("Key_4"))
        {
            AdvCombatEntity *rCombatEntity = GetActiveMemberS("Chris");
            if(rCombatEntity)
            {
                rCombatEntity->SetInfluence(10);
                rCombatEntity->SetHealth(2);
            }
        }
        //--Special: The 5 key sets Beth's influence to max and HP to 1.
        if(rControlManager->IsFirstPress("Key_5"))
        {
            AdvCombatEntity *rCombatEntity = GetActiveMemberS("Beth");
            if(rCombatEntity)
            {
                rCombatEntity->SetInfluence(10);
                rCombatEntity->SetHealth(2);
            }
        }
        //--Special: The 6 key sets Lore's influence to max and HP to 1.
        if(rControlManager->IsFirstPress("Key_6"))
        {
            AdvCombatEntity *rCombatEntity = GetActiveMemberS("Lore");
            if(rCombatEntity)
            {
                rCombatEntity->SetInfluence(10);
                rCombatEntity->SetHealth(2);
            }
        }

        //--Attempts to activate the ability in the slot. May fail.
        if(rControlManager->IsFirstPress("MouseLft") && mPlayerCommandCursor != -1)
        {
            //--Debug.
            DebugPrint("Left click.\n");

            //--Resolve the ability.
            StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();
            CommandListEntry *rSelectedEntry = (CommandListEntry *)rPlayerStackList->GetElementBySlot(mPlayerCommandCursor);
            if(!rSelectedEntry)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Ability is manually locked. Do nothing.
            if(rSelectedEntry->mIsManuallyLocked)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Special: During the tutorial the player cannot retreat. The unretreatable flag will be set.
            else if(!strcasecmp(rSelectedEntry->mDisplayName, "Retreat") && mIsUnretreatable)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--If the entry has an associated ability, begin target mode:
            else if(rSelectedEntry->rAssociatedAbility)
            {
                //--Base.
                BeginTargetingAbility(rSelectedEntry->rAssociatedAbility);
                mShowAbilityDescription = false;
                SetHitboxesResolve();

                //--Extra: If the command refers to a consumable item, store the inventory slot. When targeting
                //  is complete, the item in the slot will be decremented or removed.
                mStoredCommandInventorySlot = rSelectedEntry->mInventorySlot;

                //--Special: If the ability has the tag "One Click Self Skill", this skill automatically uses the 0th
                //  painted target and should target self. This completes in a single click.
                if(rSelectedEntry->rAssociatedAbility->GetTagCount("One Click Self Skill") > 0)
                {
                    SelectTargetInSlot(0);
                }
            }
            //--If the entry doesn't have an ability, but does have a sublist, activate that:
            else if(rSelectedEntry->mSublist)
            {
                //--Increment the stack.
                mPlayerCommandCursor = -1;
                mPlayerCommandOffset = 0;
                mrPlayerCommandStack->AddElementAsHead("X", rSelectedEntry);

                //--Re-resolve the highlighted ability.
                ReresolveDescriptionAbility();
                SetHitboxesResolve();

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }

            //--Debug.
            DebugPrint("Left click completed.\n");

            //--Stop execution.
            return;
        }

        //--Check if the player clicked in the hitbox to toggle descriptions.
        if(rControlManager->IsFirstPress("MouseLft") && mToggleDescriptionHitbox.IsPointWithin(mMouseX, mMouseY))
        {
            mShowDescriptions = !mShowDescriptions;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Attempts to move to the previous menu, if applicable.
        if(rControlManager->IsFirstPress("MouseRgt"))
        {
            //--No sublist. Cycle command back one character.
            if(mrPlayerCommandStack->GetListSize() < 1)
            {
                if(!CycleCommandBackwards())
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                return;
            }

            //--Store current entry.
            StarLinkedList *rOldStackHead = ResolvePlayerCommandList();

            //--Pop stack.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            mrPlayerCommandStack->RemoveElementI(0);

            //--Locate the element that contains the previous head. Set the cursor to that.
            mPlayerCommandCursor = 0;
            mPlayerCommandOffset = -2;
            StarLinkedList *rNewStackHead = ResolvePlayerCommandList();
            CommandListEntry *rCheckEntry = (CommandListEntry *)rNewStackHead->PushIterator();
            while(rCheckEntry)
            {
                //--Match.
                if(rCheckEntry->mSublist == rOldStackHead)
                {
                    rNewStackHead->PopIterator();
                    break;
                }

                //--Next.
                mPlayerCommandCursor ++;
                mPlayerCommandOffset ++;
                rCheckEntry = (CommandListEntry *)rNewStackHead->AutoIterate();
            }

            //--Clamp.
            if(mPlayerCommandOffset < 0) mPlayerCommandOffset = 0;
            if(rNewStackHead->GetListSize() <= 6) mPlayerCommandOffset = 0;

            //--Re-resolve the description ability.
            ReresolveDescriptionAbility();
            SetHitboxesResolve();
        }
    }
    ///--[ ========= Target Selection ========= ]
    //--Selecting which target to execute the ability on.
    else
    {
        //--Debug.
        DebugPrint("Target selection mode.\n");

        //--Entities were moving, cannot allow target selection yet.
        if(pAnyEntitiesMoving) return;

        //--Recheck cursor.
        bool tRecompute = (tUseCursor != mTargetClusterCursor);
        mTargetClusterCursor = tUseCursor;

        //--Activate, executes action on target cluster.
        if(rControlManager->IsFirstPress("MouseLft") && mTargetClusterCursor != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Select");
            SelectTargetInSlot(mTargetClusterCursor);
            return;
        }

        //--Cancel, exits target mode.
        if(rControlManager->IsFirstPress("MouseRgt"))
        {
            mIsSelectingTargets = false;
            rActiveTargetCluster = NULL;
            MovePartyByActiveCluster();
            ClearPredictionBoxes();
            PositionEnemiesByTarget(NULL);
            AudioManager::Fetch()->PlaySound("Menu|Select");
            SetHitboxesResolve();
            return;
        }

        /*
        //--Storage.
        int tOldTargetCluster = mTargetClusterCursor;

        //--Decrements target cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            mTargetClusterCursor --;
            if(mTargetClusterCursor < 0) mTargetClusterCursor = mTargetClusters->GetListSize() - 1;
        }
        //--Increments target cursor.
        if(rControlManager->IsFirstPress("Right"))
        {
            mTargetClusterCursor ++;
            if(mTargetClusterCursor >= mTargetClusters->GetListSize()) mTargetClusterCursor = 0;
        }*/

        //--If this flag was set, we need to reposition the highlight.
        if(tRecompute && mTargetClusterCursor != -1)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Move party around.
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
            MovePartyByActiveCluster();
            SetClusterAsActive(mTargetClusterCursor);

            //--Move enemies around.
            PositionEnemiesByTarget(rActiveTargetCluster->mrTargetList);
        }

        //--Debug.
        DebugPrint("Target selection update has completed.\n");
    }
}

///--[ ==================================== Worker Functions ==================================== ]
void CarnationCombat::SelectTargetInSlot(int pSlot)
{
    ///--[Documentation]
    //--When the player clicks on a target slot, performs the follow through necessary to act on that target.
    //  Effortless actions will execute immediately, delayed actions are enqueued.
    if(!rActiveAbility) return;

    ///--[Execute Now]
    //--Ability executes immediately and the character can choose a new action. "Effortless" abilities do this.
    if(rActiveAbility->GetTagCount("Is Effortless Action") > 0)
    {
        //--Error check.
        AdvCombatEntity *rActingEntity = ResolveDecidingEntity();
        if(!rActingEntity)
        {
            fprintf(stderr, "Warning: Attempted to activate an effortless action, but there was no acting entity resolved.\n");
            return;
        }

        //--Effortless action flag.
        rEffortlessActionCharacter = rActingEntity;
        mIsHandlingEffortlessAction = true;

        //--Clear text.
        ClearText();

        //--Push the deciding character onto the head of the turn order list. This will cause them to act,
        //  be removed after acting, and not modify the turn order list.
        mrTurnOrder->AddElementAsHead("X", rActingEntity);
        //fprintf(stderr, "Pushed effortless action to head of turn order. Size: %i\n", mrTurnOrder->GetListSize());

        //--Target cluster is set.
        rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);

        //--Debug.
        //fprintf(stderr, "Beginning effortless action.\n");
        //fprintf(stderr, " Acting entity is in slot %i, %p.\n", mPartyDecisionIndex, rActingEntity);
        //fprintf(stderr, " Name %s\n", rActingEntity->GetName());

        //--Run the entity's response script, checking if a status effect is intercepting
        //  ability execution. If so, a flag will be tripped.
        mStatusInterceptFlag = false;
        const char *rExecScript = rActingEntity->GetResponsePath();
        LuaManager::Fetch()->PushExecPop(rActingEntity, rExecScript, 1, "N", (float)CCE_SCRIPT_CALL_CHECK_STATUS_INTERCEPT);

        //--Flag not tripped, execute.
        if(!mStatusInterceptFlag)
        {
            ExecuteActiveAbility();
        }
        //--Flag was tripped, run the "Pass Turn" ability in addition to whatever the entity
        //  did in its response.
        else
        {
            ExecuteAbility(mSysAbilityPassTurn, NULL);
        }
        return;
    }

    ///--[Execute Later]
    //--Ability executes in turn stride.
    mShowCommandWindow = false;

    //--Hide description.
    mShowAbilityDescription = false;

    //--Clear any pending actions by this owner. This makes sure that cancelling and remarking the action
    //  doesn't create any duplicates.
    void *rOwner = ResolveDecidingEntity();
    RemoveActionsConcerning(rOwner);

    //--Create a storage pack.
    CarnationAction *nAction = (CarnationAction *)starmemoryalloc(sizeof(CarnationAction));
    nAction->rOwner = (AdvCombatEntity *)rOwner;
    nAction->rAbility = rActiveAbility;
    nAction->rTarget = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
    mPartyActions->AddElement("X", nAction, &CarnationAction::DeleteThis);

    //--Take ownership of the target cluster.
    mTargetClusters->SetRandomPointerToThis(nAction->rTarget);
    mTargetClusters->LiberateRandomPointerEntry();

    //--Next selection.
    mPartyDecisionTimer = 0;
    mSelecterBuiltCommands = false;
    mIsSelectingTargets = false;
    ClearPredictionBoxes();

    //--Cycle command index forward. If the function returns false, no more characters need to act,
    //  so set the number high to end selection mode.
    if(!CycleCommandForwards()) mPartyDecisionIndex = mrCombatParty->GetListSize() + 1;

    //--Clear command stack.
    mrPlayerCommandStack->ClearList();

    //--Reresolve hitboxes.
    SetHitboxesResolve();
}
