//--Base
#include "MonoUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

///======================================== Activation ============================================
void MonoUIVendor::ActivateModeLevelup()
{
    ///--[Documentation]
    //--Switches to levelup mode.

    ///--[Variables]
    //--Common subroutines.
    ActivateMode(mIsLevelupMode);
    ResetCursor();

    ///--[Other]
    //--Build levelup strings.
    ConstructLevelupStrings(mCursor);
}

///======================================= Core Methods ===========================================
void MonoUIVendor::ConstructLevelupStrings(int pActiveSlot)
{
    ///--[Documentation]
    //--Given a slot referring to the active party, retrieves that character and rebuilds the level
    //  up stat strings. These strings indicate how much of an increase in HP/ATK/etc the character
    //  will get for increasing in level. This is done by running their levelup script and storing
    //  the differences.
    //--In the event the character does not exist, the strings will be NULL and not render.

    ///--[Reset Strings]
    for(int i = 0; i < MONOUIVEN_LEVELUP_STATS; i ++)
    {
        free(mLevelupStatIncreases[i]);
        mLevelupStatIncreases[i] = NULL;
    }

    ///--[Get Character]
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AdvCombatEntity *rCharacter = rAdvCombat->GetActiveMemberI(pActiveSlot);
    if(!rCharacter) return;

    //--Get the level-up script.
    AdvCombatJob *rActiveJob = rCharacter->GetActiveJob();
    if(!rActiveJob) return;

    //--If the character is at level 100 (99 in-code) then do nothing.
    if(rCharacter->GetLevel() >= 99) return;

    ///--[Get Stats]
    //--Store the character's current stats. These are after additions like gems and equipment
    //  which will increase the stats past their base values.
    int tCurVals[MONOUIVEN_LEVELUP_STATS];
    tCurVals[0] = rCharacter->GetHealthMax();
    tCurVals[1] = rCharacter->GetStatistic(STATS_ATTACK);
    tCurVals[2] = rCharacter->GetStatistic(STATS_ACCURACY);
    tCurVals[3] = rCharacter->GetStatistic(STATS_EVADE);
    tCurVals[4] = rCharacter->GetStatistic(STATS_INITIATIVE);

    //--Call the level up script to populate the storage structure with the "base" values for this level.
    CombatStatistics *rStats = rAdvCombat->GetJobLevelUpStorage();
    rActiveJob->CallScriptLevelUp(rCharacter->GetLevel());
    int tBaseVals[MONOUIVEN_LEVELUP_STATS];
    tBaseVals[0] = rStats->GetStatByIndex(STATS_HPMAX);
    tBaseVals[1] = rStats->GetStatByIndex(STATS_ATTACK);
    tBaseVals[2] = rStats->GetStatByIndex(STATS_ACCURACY);
    tBaseVals[3] = rStats->GetStatByIndex(STATS_EVADE);
    tBaseVals[4] = rStats->GetStatByIndex(STATS_INITIATIVE);

    //--Call the character's level-up script at one level higher.
    rActiveJob->CallScriptLevelUp(rCharacter->GetLevel() + 1);
    int tNewVals[MONOUIVEN_LEVELUP_STATS];
    tNewVals[0] = rStats->GetStatByIndex(STATS_HPMAX);
    tNewVals[1] = rStats->GetStatByIndex(STATS_ATTACK);
    tNewVals[2] = rStats->GetStatByIndex(STATS_ACCURACY);
    tNewVals[3] = rStats->GetStatByIndex(STATS_EVADE);
    tNewVals[4] = rStats->GetStatByIndex(STATS_INITIATIVE);

    ///--[Construct]
    //--The strings are simple "%i -> %i" comparisons.
    for(int i = 0; i < MONOUIVEN_LEVELUP_STATS; i ++)
    {
        mLevelupStatIncreases[i] = InitializeString("%i -> %i", tCurVals[i], tCurVals[i] + (tNewVals[i] - tBaseVals[i]));
    }

    ///--[Clean Up]
    //--Clear the level up structure.
    rStats->Zero();
}

///====================================== Primary Update ==========================================
void MonoUIVendor::UpdateModeLevelup()
{
    ///--[Documentation]
    //--Updates level-up, a very simplified buy menu that allows purchasing level-ups for the player
    //  characters. The buy menu exists under this but the controls and rendering are changed.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Buying items timer.
    StandardTimer(mBuyTimer, 0, cxAdvBuyTicks, mIsBuyingItem);

    //--If these times are not zeroed or capped, block the update.
    if(mBuyTimer > 0 && mBuyTimer     < cxAdvBuyTicks) return;

    ///--[Buying Item Handler]
    //--In this mode, handle controls for buying a specific item. This gets its own subroutine.
    if(mIsBuyingItem)
    {
        //--Run the update.
        UpdateBuyItem();

        //--If buying was ended, it may mean the character leveled up. Re-run the resolver.
        if(!mIsBuyingItem) ConstructLevelupStrings(mCursor);
        return;
    }

    ///--[Left and Right]
    //--Changes which character is under consideration.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Decrement.
        mCursor --;

        //--Clamp.
        if(mCursor < 0) mCursor = 2;

        //--Reconstruct comparison.
        ConstructLevelupStrings(mCursor);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Changes which character is under consideration.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Increment.
        mCursor ++;

        //--Clamp.
        if(mCursor >= 3) mCursor = 0;

        //--Reconstruct comparison.
        ConstructLevelupStrings(mCursor);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Activate]
    //--Begins purchasing the highlighted item. If you don't have enough money to buy even one, fails.
    if(rControlManager->IsFirstPress("Activate") && !mIsShowingDetails)
    {
        //--Check character. Can't level-up a character at level 99.
        AdvCombat *rAdvCombat = AdvCombat::Fetch();
        AdvCombatEntity *rCharacter = rAdvCombat->GetActiveMemberI(mCursor);
        if(rCharacter && rCharacter->GetLevel() >= 99) return;

        //--Get the item in question.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
        if(!rPackage) return;

        //--Make sure the item exists.
        AdventureItem *rItem = rPackage->mItem;
        if(!rItem) return;

        //--If the item has 0 quantity available, stop.
        if(rPackage->mQuantity < 1 && rPackage->mQuantity != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Get how much the item will cost. If we don't have enough money to buy one, fail.
        int tCost = rItem->GetValue();
        if(rPackage->mPriceOverride > -1) tCost = rPackage->mPriceOverride;
        if(tCost > AdventureInventory::Fetch()->GetPlatina())
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Otherwise, activate buying mode.
        mIsBuyingItem = true;
        mBuyQuantity = 1;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel") && !mIsShowingDetails)
    {
        HandleExit();
        return;
    }
}

///===================================== Segment Rendering ========================================
void MonoUIVendor::RenderLftLevelup(float pVisAlpha)
{

}
void MonoUIVendor::RenderTopLevelup(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Render]
    //--Header.
    MonoImages.rOverlay_TitleShade->Draw();

    //--Header Text.
    SetColor("Heading", cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 16.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextLevelUp);
    SetColor("Clear", cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderRgtLevelup(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
#define MONOMENU_VENDOR_LEVELUP_CHARS 3
void MonoUIVendor::RenderBotLevelup(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Fast-access pointers.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();

    ///--[Render]
    //--Positions.
    float cCharXPos[MONOMENU_VENDOR_LEVELUP_CHARS];
    float cCharYPos[MONOMENU_VENDOR_LEVELUP_CHARS];
    cCharXPos[0] = 136.0f;
    cCharYPos[0] = 413.0f;
    cCharXPos[1] = 495.0f;
    cCharYPos[1] = 474.0f;
    cCharXPos[2] = 839.0f;
    cCharYPos[2] = 421.0f;

    //--Character portraits.
    for(int i = 0; i < MONOMENU_VENDOR_LEVELUP_CHARS; i ++)
    {
        //--Character.
        AdvCombatEntity *rCharacter = rAdvCombat->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--Get combat portrait.
        StarBitmap *rCombatPortrait = rCharacter->GetCombatPortrait();
        if(!rCombatPortrait) continue;

        //--Non-highlighted character is greyed out.
        if(i != mCursor)
            SetColor("Greyed", cColorAlpha);
        else
            SetColor("Clear", cColorAlpha);

        //--Render.
        TwoDimensionRealPoint cRenderPos = rCharacter->GetUIRenderPosition(ACE_UI_INDEX_MONO_LEVELUP);
        rCombatPortrait->Draw(cRenderPos.mXCenter, cRenderPos.mYCenter);
    }

    //--Stat Frames/Levels/Costs
    for(int i = 0; i < 3; i ++)
    {
        //--Character.
        AdvCombatEntity *rCharacter = rAdvCombat->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--Levelup Item Package.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementBySlot(i);
        if(!rPackage) continue;

        //--Non-highlighted character is greyed out.
        if(i != mCursor)
            SetColor("Greyed", cColorAlpha);
        else
            SetColor("Clear", cColorAlpha);

        //--Frame.
        MonoImages.rOverlay_LevelupInfo->Draw(cCharXPos[i], cCharYPos[i]);

        //--Name.
        SetColor("Black", cColorAlpha);
        AdvImages.rFont_Mainline->DrawText(cCharXPos[i] + 140.0f, cCharYPos[i] + 29.0f, 0, 1.0f, rCharacter->GetDisplayName());
        SetColor("Clear", cColorAlpha);

        //--Current level.
        AdvImages.rFont_Heading->DrawTextArgs(cCharXPos[i] + 66.0f, cCharYPos[i] + 44.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", rCharacter->GetLevel()+1);

        //--Levelup cost. If it's less than 99:
        if(rCharacter->GetLevel() < 99)
        {
            int tCost = rPackage->mPriceOverride;
            if(tCost == -1) tCost = rPackage->mItem->GetValue();
            AdvImages.rFont_Mainline->DrawTextArgs(cCharXPos[i] + 106.0f, cCharYPos[i] + 62.0f, 0, 1.0f, xrStringData->str.mTextLevelUpCost, tCost);
        }
        //--At max level, just display "Max!"
        else
        {
            AdvImages.rFont_Mainline->DrawTextArgs(cCharXPos[i] + 106.0f, cCharYPos[i] + 62.0f, 0, 1.0f, xrStringData->str.mTextLevelUpMaxed);
        }

        ///--[ =========== Selected Character =========== ]
        //--Highlighted character renders a button / stat listing.
        if(i == mCursor)
        {
            ///--[Level Up Button]
            //--Position.
            float cUseX = cCharXPos[i] +  63.0f;
            float cUseY = cCharYPos[i] + 110.0f;

            //--Frame.
            MonoImages.rFrame_LevelupButton->Draw(cUseX, cUseY);

            //--Selection.
            MonoImages.rOverlay_Selected->Draw(cUseX-27.0f - MonoImages.rOverlay_Selected->GetXOffset(), cUseY+1.0f - MonoImages.rOverlay_Selected->GetYOffset());

            //--Text.
            SetColor("Paragraph", cColorAlpha);
            AdvImages.rFont_Mainline->DrawText(cUseX + 30.0f, cUseY + 2.0f, 0, 1.0f, xrStringData->str.mTextLevelUp);
            SetColor("Clear", cColorAlpha);

            ///--[Level Up Stats]
            //--Constants for stat display.
            float cTabOffsetX = 0.0f;
            float cTabOffsetY = 0.0f;
            float cIconX = cCharXPos[i] + 100.0f;
            float cIconY = cCharYPos[0] + 160.0f;
            float cIconH = 25.0f;
            float cStatYOff = -8.0f;

            //--Adjust positions based on slot.
            if(i == 1)
            {
                cIconY = cIconY + 62.0f;
                cTabOffsetX = 354.0f;
                cTabOffsetY =  62.0f;
            }
            if(i == 2)
            {
                cIconY = cIconY + 9.0f;
                cTabOffsetX = 703.0f;
                cTabOffsetY =   9.0f;
            }

            //--Stat backing.
            MonoImages.rTab_VendorStats->Draw(cTabOffsetX, cTabOffsetY);

            //--Render.
            for(int p = 0; p < MONOUIVEN_LEVELUP_STATS; p ++)
            {
                AdvImages.rPropertyIcons[p]->Draw(cIconX, cIconY + (cIconH * p) + 4.0f);
                if(!mLevelupStatIncreases[p]) continue;
                AdvImages.rFont_Mainline->DrawTextArgs(cIconX + 25.0f, cIconY + (cIconH * p) + cStatYOff, 0, 1.0f, mLevelupStatIncreases[p]);
            }
        }
    }

    //--Clean.
    SetColor("Clear", cColorAlpha);

    //--Help Strings.
    //mShowHelpString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (AM_MAINLINE_H * 1.0f), 0, 1.0f, Images.VendorUI.rMainlineFont);
    //mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (AM_MAINLINE_H * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImg.Standard.rFontControl);
    //mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (AM_MAINLINE_H * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImg.Standard.rFontControl);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderUnalignedLevelup(float pVisAlpha)
{
    RenderBuyItem(pVisAlpha);
}
