///========================================= INameable ============================================
//--Interface for any object which can be named. Has a simple get and set.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class INameable
{
    private:

    protected:

    public:
    //--System
    virtual ~INameable() {};

    //--Public Variables
    //--Property Queries
    virtual char *GetName() = 0;

    //--Manipulators
    virtual void SetName(const char *pName) = 0;

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

