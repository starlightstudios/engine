///=================================== Chapter2LoadInterrupt ======================================
//--Version of the StarLoadInterrupt built for chapter 2.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLoadInterrupt.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class Chapter2LoadInterrupt : public StarLoadInterrupt
{
    private:
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    bool mIsReady;

    protected:

    public:
    //--System
    Chapter2LoadInterrupt();
    virtual ~Chapter2LoadInterrupt();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators

    //--Core Methods
    virtual void DeallocateImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Call(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_BootChapter2(lua_State *L);
