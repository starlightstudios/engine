///===================================== Class Definitions ========================================
//--File that contains forward declarations of most of the classes available in the engine. Not totally
//  exhaustive as some local-use classes are not here.

///--[AbyssCombat]
class AbyssCombat;
class AbyCombatPrediction;

///--[AdvCombat]
class AdvCombat;
class AdvCombatAbility;
class AdvCombatAnimation;
class AdvCombatEffect;
class AdvCombatEntity;
class AdvCombatJob;
class AdvCombatPrediction;

///--[CarnationCombat]
class CarnationCombat;
class CarnationCombatEntity;
class CarnationLetter;

///--[Entities]
class RootEntity;
class Actor;
class RootEffect;
class TilemapActor;
class PairanormalDialogueCharacter;
class SpineExpression;
class ActorNotice;
class PeakFreaksPlayer;
class PeakFreaksPuzzlePiece;

///--[Events]
class RootEvent;
class ActorEvent;
class CameraEvent;
class TimerEvent;

///--[GUI]
class AdventureDebug;
class AdventureMenu;
class AdvHelp;
class BeehiveUI;
class CarnationMenu;
class CarnationMinimap;
class CorrupterMenu;
class DeckEditor;
class DialogueActor;
class DialogueTopic;
class MonocerosMenu;
class CarnStringEntry;
class MonoStringEntry;
class StringEntry;
class WorldDialogue;
class PairanormalMenu;
class PairanormalSettingsMenu;
class PairanormalDialogue;
class StringTyrantTitle;

///--[GUI Components]
class AdvUICommon;
class AdvUIDoctor;
class AdvUIEquip;
class AdvUIFileSelect;
class AdvUIInventory;
class AdvUIJournal;
class AdvUIOptions;
class AdvUISkills;
class AdvUIStatus;
class AdvUIVendor;
class AdvUIWarp;
class CarnUIClassChange;
class CarnUICommon;
class CarnUIEquip;
class CarnUIEvent;
class CarnUIInventory;
class CarnUIOptions;
class CarnUIRegem;
class CarnUIStatus;
class CarnUIVendor;
class MonoUICommon;
class MonoUIEquip;
class MonoUIFileSelect;
class MonoUIInventory;
class MonoUIStatus;
class MonoUIVendor;

///--[Inventory]
class AdventureInventory;
class AdventureItem;
class InventoryItem;
class InventorySubClass;
class WorldContainer;

///--[Lights]
class AdventureLight;

///--[Loading]
class StringTyrantLoadInterrupt;

///--[Minigame]
class KPopDanceGame;
class KPopDanceMove;
class KPopDancer;
class PuzzleFight;
class ShakeMinigame;

///--[Packages]
class MagicPack;
class PerkPack;
class TransformPack;

///--[Pathing]
class Pathrunner;

///--[Storage]
class AliasStorage;

///--[Title Screens]
class CarnationTitle;

///--[Translation]
class TranslationExecutor;

///--[World]
class RootLevel;
class AdventureLevelGenerator;
class ALGTile;
class AdventureLevel;
class BeehiveLevel;
struct BeehiveLevelTile;
class CarnationLevel;
class CarnationTitle;
class ContextMenu;
class PandemoniumLevel;
class PandemoniumRoom;
class FieldAbility;
class FlexButton;
class FlexMenu;
class GalleryMenu;
class PeakFreaksLevel;
class PeakFreaksTitle;
class RunningMinigame;
class Skybox;
class SlittedEyeLevel;
class TetrisLevel;
class TextLevel;
class TiledLevel;
class TextLevOptions;
class TypingCombat;
class VisualRoom;
class VisualLevel;
class WADFile;
class WordCard;
class WordCombat;
class WorldLight;
class WorldPolygon;
class ZoneEditor;
class ZoneNode;

///--[Other Definitions]
struct CarnExaminable;
