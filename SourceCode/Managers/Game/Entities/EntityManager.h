///======================================= EntityManager ==========================================
//--Manager that handles all entities in the game, including the Player. Entities can be added,
//  removed, or searched for.
//--There are also lists of 'special' entities that ignore the usual update/render cycle, but
//  can still be searched for.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "Interfaces/IResettable.h"

///===================================== Local Structures =========================================
typedef struct
{
    float mXPos, mYPos, mZPos;
    StarlightColor mColor;
}ParticleRenderPack;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class EntityManager : public IResettable
{
    private:
    //--System
    PlayerPony *mPlayer;

    //--Storage
    int mLastCreatedID;
    StarLinkedList *mEntityList;

    //--Special Lists
    StarLinkedList *mSpecialListList;

    //--Turn Controlling
    int mTurnCount;
    Actor *rActingEntity;
    Actor *rLastPlayerEntity;
    char *mTurnControlScript;

    //--Resetting
    bool mIsFirstResetCall;
    char *mCleanerScript;

    protected:

    public:
    //--System
    EntityManager();
    virtual ~EntityManager();
    static void PreserveThis(void *pPtr);

    //--Public Variables
    bool mHasPendingClear;
    bool mDisplayEntities;

    //--Particle special handling
    static int xRunningRenderCounter;
    static ParticleRenderPack *xParticleRenderList;

    //--Property Queries
    RootEntity *GetEntityI(uint32_t pSearchID);
    RootEntity *GetEntity(const char *pName);
    RootEntity *GetEntityPartialS(const char *pPartialName);
    bool IsClipped(int pX, int pY);
    int GetLastReggedID();

    //--Manipulators
    void RenamePointer(const char *pName, void *pPtr);
    void RegisterPointer(const char *pName, void *pPtr);
    void RegisterPointer(const char *pName, void *pPtr, DeletionFunctionPtr pDelFunc);
    void RemovePointer(const char *pName);
    void RemovePointerUC(uint32_t pUniqueID);
    void LiberatePointerP(void *pPtr);
    void ClearAll();
    void SetCleanerScript(const char *pPath);

    //--Core Methods
    virtual void Reset(const char *pResetType);

    //--Macros
    static float GetDistanceBetweenEntities(RootEntity *pEntity1, RootEntity *pEntity2);
    float GetDistanceBetweenEntities(const char *pName, const char *pName2);
    static float GetAngleBetweenEntities(RootEntity *pEntity1, RootEntity *pEntity2);
    float GetAngleBetweenEntities(const char *pName, const char *pName2);
    bool IsEntityWithinWaterZone(RootEntity *pEntity);

    //--Special Lists
    void BootSpecialLists();
    void RegisterSpecialObject(const char *pSubListName, const char *pName, void *pObject);
    void RegisterSpecialObject(const char *pSubListName, const char *pName, void *pObject, DeletionFunctionPtr pDelFunc);
    void AutoPurge(void *pPtr);
    StarLinkedList *GetSpecialList(const char *pSubListName);
    void ClearSpecialLists();
    void DebugSpecialLists();

    //--Turn Handling
    int GetTurnCount();
    Actor *GetActingEntity();
    Actor *GetLastPlayerEntity();
    void SetTurnControlScript(const char *pPath);
    void UpdateTurns();
    void NewTurn();

    //--Update
    void PurgeDestructingEntities();
    void Update();
    void PostUpdate();
    void UpdatePaused(uint8_t pPauseFlags);

    //--File I/O
    //--Drawing
    void Render();
    void AddToRenderingList(StarLinkedList *pRenderingList);

    //--Pointer Routing
    PlayerPony *GetPlayer();
    StarLinkedList *GetEntityList();
    StarLinkedList *GetListContaining(int pType);
    StarLinkedList *GetListContaining(int pTypeBegin, int pTypeEnd);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);

    //--Static Functions
    static EntityManager *Fetch();
};

//--Hooking Functions
int Hook_EM_Exists(lua_State *L);
int Hook_EM_PushEntity(lua_State *L);
int Hook_EM_GetLastID(lua_State *L);
int Hook_EM_SetTurnController(lua_State *L);
int Hook_EM_GetTurnCount(lua_State *L);
int Hook_EM_PushIterator(lua_State *L);
int Hook_EM_AutoIterate(lua_State *L);
int Hook_EM_PopIterator(lua_State *L);
int Hook_EM_SetCleanerScript(lua_State *L);
