//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueTopic.h"
#include "VisualLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///--[Debug]
//#define WORLD_DIALOGUE_TOPICS_DEBUG
#ifdef WORLD_DIALOGUE_TOPICS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///===================================== Property Queries =========================================
bool WorldDialogue::IsTopicRead(const char *pName)
{
    //--Get. On no existence, false is returned.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pName);
    if(!rCheckTopic) return false;

    //--Return state.
    return (rCheckTopic->GetLevel() >= 1);
}

///======================================= Manipulators ===========================================
void WorldDialogue::WipeTopicData()
{
    //--Clears all topic packages. Only use during startup.
    mTopicListing->ClearList();
}
void WorldDialogue::SetAllTopicsToDefaults()
{
    //--Clears topic packages back to their starting states.
    DialogueTopic *rTopic = (DialogueTopic *)mTopicListing->PushIterator();
    while(rTopic)
    {
        rTopic->SetToDefaultLevel();
        rTopic = (DialogueTopic *)mTopicListing->AutoIterate();
    }
}
void WorldDialogue::ActivateTopicsModeAfterDialogue(const char *pNPCName)
{
    //--Scripting access function. Flags topics mode to be activated after the dialogue has finished printing,
    //  instead of closing as normal. Pass NULL to disable this behavior.
    mRunTopicsAfterDialogue = false;
    if(!pNPCName || !strcasecmp(pNPCName, "Null")) return;

    //--Set.
    mRunTopicsAfterDialogue = true;
    strncpy(mRunTopicsAfterDialogueName, pNPCName, STD_NPC_LETTERS - 1);
    SetGoodbyeName("Goodbye");
}
void WorldDialogue::ActivateTopicsMode(const char *pNPCName)
{
    //--Activates Topics Mode with the given NPC as the speaker. Doesn't set portraits or the like,
    //  just sets flags and builds the topic listing.
    mIsTopicsMode = true;
    mIsMajorSequenceMode = false;
    mIsSceneMode = false;
    mTopicTimer = 0;
    mTopicCursor = mLastTopicCursor;
    mLastTopicCursor = 0;
    mTopicScroll = 0;

    //--Check if the NPC in question is the one currently talking. If not, reset the cursor.
    if(strcasecmp(pNPCName, mCurrentTopicActor))
    {
        mTopicCursor = 0;
    }

    //--Build topic listing.
    BuildTopicListing(pNPCName);

    //--Recheck topic scroll, in case the last topic cursor was off the bottom.
    int tMaxTopicScroll = (mrCurrentTopicListing->GetListSize() - 10);
    if(tMaxTopicScroll > 0 && tMaxTopicScroll % 2 == 1) tMaxTopicScroll ++;
    if(tMaxTopicScroll > 0)
    {
        mTopicScroll = (mTopicCursor - 4);
        if(mTopicScroll % 2 == 1) mTopicScroll --;
        if(mTopicScroll < 0) mTopicScroll = 0;
        if(mTopicScroll > tMaxTopicScroll) mTopicScroll = tMaxTopicScroll;
    }
}
void WorldDialogue::SetGoodbyeName(const char *pName)
{
    //--Changes the display name of the "Goodbye" option on the topic listing. Does *not* change which
    //  script gets executed, just the display name!
    //--Every time topics gets activated, the name reverts to "Goodbye" so it needs to be re-set each
    //  time if you want it changed.
    if(!pName || !mGoodbyeTopic) return;

    //--Translation.
    StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_UI);
    if(!rTranslation)
    {
        mGoodbyeTopic->SetDisplayName(pName);
    }
    else
    {
        const char *rTranslatedText = rTranslation->GetTranslationFor(pName);
        if(rTranslatedText)
        {
            mGoodbyeTopic->SetDisplayName(rTranslatedText);
        }
        else
        {
            mGoodbyeTopic->SetDisplayName(pName);
        }
    }
}
void WorldDialogue::RegisterTopic(const char *pName, const char *pDisplayName, int pStartingLevel)
{
    ///--[Documentation]
    //--Adds a new topic to the database. If the topic already exists, then its level is increased
    //  if the requested level is higher.
    if(!pName || !pDisplayName) return;
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pName);
    if(rCheckTopic)
    {
        if(rCheckTopic->GetLevel() < pStartingLevel) rCheckTopic->SetLevel(pStartingLevel);
        return;
    }

    //--Allocate and initialize.
    DialogueTopic *nTopic = new DialogueTopic();
    nTopic->SetName(pName);
    nTopic->SetDisplayName(pDisplayName);
    nTopic->SetLevel(pStartingLevel);
    nTopic->SetDefaultLevel(pStartingLevel);

    //--Register.
    mTopicListing->AddElement(pName, nTopic, &RootObject::DeleteThis);
}
void WorldDialogue::RegisterTopicFor(const char *pTopicName, const char *pNPCName, int pStartingLevel)
{
    ///--[Documentation]
    //--Informs the given topic that the given NPC has a response to it if the topic is at least the given level.
    if(!pTopicName || !pNPCName) return;

    //--Make sure the topic actually exists.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pTopicName);
    if(!rCheckTopic) return;

    //--Add the NPC. It will internally fail if they were already registered.
    rCheckTopic->RegisterNPC(pNPCName, pStartingLevel);
}
void WorldDialogue::UnlockTopic(const char *pTopicName, int pMinimumLevel)
{
    ///--[Documentation]
    //--Get the named topic. If its level is less than the provided level, increase it. This effectively unlocks
    //  the state of a topic. Remember that topics at -1 never appear on the topic listing.
    //--Note: Fails silently if the topic is "Null".
    if(!pTopicName || !strcasecmp(pTopicName, "Null")) return;

    //--Verify the topic exists.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pTopicName);
    if(!rCheckTopic) return;

    //--If it's lower, upgrade it.
    if(rCheckTopic->GetLevel() < pMinimumLevel) rCheckTopic->SetLevel(pMinimumLevel);
}
void WorldDialogue::ClearTopicReadFlag(const char *pTopicName)
{
    ///--[Documentation]
    //--If the given topic exists, mark it as unread. This is to indicate something has changed and the player
    //  can gain more/new information.
    if(!pTopicName) return;

    //--Make sure the topic actually exists.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pTopicName);
    if(!rCheckTopic) return;

    //--Topic found. Flag it as unread.
    rCheckTopic->MarkAsUnread();
}

///======================================= Core Methods ===========================================
void WorldDialogue::BuildTopicListing(const char *pNPCName)
{
    ///--[Documentation]
    //--Given an NPC's name, builds a list of all topics that that NPC has something to say about. It is possible
    //  for the topic listing to come back empty, in which case the only topic present will be "Goodbye".
    //--The current topic listing is always cleared. Pass NULL to clear it exclusively.
    mrCurrentTopicListing->ClearList();
    mDecisionHitboxes->ClearList();
    if(!pNPCName)
    {
        mrCurrentTopicListing->AddElement("Yes", mGoodbyeTopic);
        return;
    }

    //--Store the name of the NPC.
    strncpy(mCurrentTopicActor, pNPCName, STD_NPC_LETTERS - 1);

    ///--[Clear Deprecated]
    //--Clear any topics with the display name of NULL. Those are topics that are from previous versions and
    //  were deprecated for whatever reason.
    DialogueTopic *rTopic = (DialogueTopic *)mTopicListing->SetToHeadAndReturn();
    while(rTopic)
    {
        if(rTopic->GetDisplayName() == NULL)
        {
            mTopicListing->RemoveRandomPointerEntry();
        }
        rTopic = (DialogueTopic *)mTopicListing->IncrementAndGetRandomPointerEntry();
    }

    ///--[Iterate]
    //--Iterate across the topics and store all the relevant ones.
    DebugPush(true, "Scanning dialogue topics. NPC name: %s\n", pNPCName);
    rTopic = (DialogueTopic *)mTopicListing->PushIterator();
    while(rTopic)
    {
        //--Debug.
        DebugPrint(" Topic: %s\n", mTopicListing->GetIteratorName());

        //--If the level of the NPC comes back as -1, the NPC is not listed for this topic.
        if(rTopic->GetLevelOfNPC(pNPCName) == -1)
        {
            DebugPrint("  Not relevant to this NPC.\n");
        }
        //--If the level of the topic comes back as -1, then the topic has not been unlocked yet.
        else if(rTopic->GetLevel() == -1)
        {
            DebugPrint("  Invalid level.\n");
        }
        //--NPC has something to say, add them.
        else
        {
            //--The name stores whether or not this topic has been discussed before. If dicussed before,
            //  the name is "Yes". Otherwise, it's "No".
            DebugPrint("  Relevant.\n");
            if(rTopic->HasDiscussedBefore(pNPCName))
            {
                mrCurrentTopicListing->AddElement("Yes", rTopic);
            }
            else
            {
                mrCurrentTopicListing->AddElement("No", rTopic);
            }

            //--If in mouse mode, add a new entry to the hitbox list.
            if(mDecisionUsesMouse)
            {
                //--Constants.
                float cLft =  17.0f;
                float cTop = 479.0f;
                float cVOf = -65.0f;
                float cWid = 228.0f;
                float cHei =  61.0f;

                //--Create.
                TwoDimensionReal *nHitbox = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
                nHitbox->SetWH(cLft, cTop + (mDecisionHitboxes->GetListSize() * cVOf), cWid, cHei);

                //--Register as head, making this the topmost hitbox.
                mDecisionHitboxes->AddElementAsHead("X", nHitbox, &FreeThis);
            }
        }

        //--Next.
        rTopic = (DialogueTopic *)mTopicListing->AutoIterate();
    }

    ///--[Goodbye Handler]
    //--The "Goodbye" topic is always added last. It always gets the "Yes" case since it's never "new".
    DebugPrint("Added goodbye handler.\n");
    mrCurrentTopicListing->AddElement("Yes", mGoodbyeTopic);

    //--If in mouse mode, add a new entry to the hitbox list.
    if(mDecisionUsesMouse)
    {
        //--Constants.
        float cLft =  17.0f;
        float cTop = 479.0f;
        float cVOf = -65.0f;
        float cWid = 228.0f;
        float cHei =  61.0f;

        //--Create.
        TwoDimensionReal *nHitbox = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
        nHitbox->SetWH(cLft, cTop + (mDecisionHitboxes->GetListSize() * cVOf), cWid, cHei);

        //--Register as head, making this the topmost hitbox.
        mDecisionHitboxes->AddElementAsHead("X", nHitbox, &FreeThis);
    }

    ///--[Debug]
    DebugPop("Finished.\n");
}

///========================================== Update ==============================================
void WorldDialogue::UpdateTopicsMode()
{
    ///--[Documentation]
    //--When in topics mode, the player can select a topic and activate it. This can switch between
    //  keyboard and mouse controls.

    ///--[Common]
    //--Timer.
    mTopicTimer ++;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Keyboard Version]
    if(!mDecisionUsesMouse)
    {
        //--Pressing up decrements the topic cursor by 2.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--Move.
            if(mTopicCursor >= 2)
            {
                mTopicCursor -= 2;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--If the cursor is close to the top of the scroll, move the scroll. Scroll moves in increments of 2.
            if(mTopicScroll > 0 && mTopicCursor < mTopicScroll + 4)
            {
                mTopicScroll -= 2;
            }
        }
        //--Down, scrolls positively by 2.
        else if(rControlManager->IsFirstPress("Down"))
        {
            //--Move.
            if(mTopicCursor < mrCurrentTopicListing->GetListSize() - 2)
            {
                mTopicCursor += 2;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--If the cursor is close to the top of the scroll, move the scroll. Scroll moves in increments of 2.
            int tMaxTopicScroll = (mrCurrentTopicListing->GetListSize() - 10);
            if(tMaxTopicScroll > 0 && tMaxTopicScroll % 2 == 1) tMaxTopicScroll ++;
            if(mTopicScroll < tMaxTopicScroll && mTopicCursor >= mTopicScroll + 6)
            {
                mTopicScroll += 2;
            }
        }
        //--Left, if the index is odd decrements.
        else if(rControlManager->IsFirstPress("Left"))
        {
            if(mTopicCursor % 2 == 1)
            {
                mTopicCursor --;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right, if the index is even increments.
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mTopicCursor % 2 == 0 && mTopicCursor < mrCurrentTopicListing->GetListSize() - 1)
            {
                mTopicCursor ++;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Activate will execute the given topic script.
        else if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the topic in question.
            DialogueTopic *rSelectedTopic = (DialogueTopic *)mrCurrentTopicListing->GetElementBySlot(mTopicCursor);
            if(!rSelectedTopic) return;

            //--Normal case: Execute.
            if(rSelectedTopic != mGoodbyeTopic)
            {
                //--Flags.
                mLastTopicCursor = mTopicCursor;
                mIsTopicsMode = false;

                //--Auto-update the topic.
                rSelectedTopic->SetNPCLevel(mCurrentTopicActor, rSelectedTopic->GetLevel());

                //--Call this script. It will handle the rest.
                char *tPathBuf = InitializeString("%s/Topic Launcher.lua", xTopicsDirectory);
                Clear();
                LuaManager::Fetch()->ExecuteLuaFile(tPathBuf, 2, "S", mCurrentTopicActor, "S", rSelectedTopic->GetInternalName());
                free(tPathBuf);

                //--Build path.
                //char *tPathBuffer = InitializeString("%s/%s/100 %s.lua", xTopicsDirectory, mCurrentTopicActor, rSelectedTopic->GetInternalName());

                //--Run.
                //Clear();
                //LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer, 1, "S", "Hello");

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Clean.
                //free(tPathBuffer);
            }
            //--Goodbye case: Execute the goodbye text. The dialogue closes afterwards.
            else
            {
                //--Flags.
                mLastTopicCursor = 0;
                mIsTopicsMode = false;

                //--Call this script. It will handle the rest.
                char *tPathBuf = InitializeString("%s/Topic Launcher.lua", xTopicsDirectory);
                Clear();
                LuaManager::Fetch()->ExecuteLuaFile(tPathBuf, 2, "S", mCurrentTopicActor, "S", "Goodbye");
                free(tPathBuf);

                //--Build path.
                //char *tPathBuffer = InitializeString("%s/%s/100 %s.lua", xTopicsDirectory, mCurrentTopicActor, "Goodbye");

                //--Run.
                //Clear();
                //LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Clean.
                //free(tPathBuffer);
            }
        }
        //--Cancel will move the cursor to the "Goodbye" entry.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            //--Set.
            mTopicCursor = mrCurrentTopicListing->GetListSize() - 1;
            if(mTopicCursor < 0) mTopicCursor = 0;

            //--Handle the topic scroll.
            mTopicScroll = (mrCurrentTopicListing->GetListSize() - 10);
            if(mTopicScroll % 2 == 1 && mTopicScroll >= 0) mTopicScroll ++;
            if(mTopicScroll < 0) mTopicScroll = 0;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    ///--[Mouse Mode]
    else
    {
        //--Get mouse positions.
        int tMouseX, tMouseY, tMouseZ;
        rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

        //--Next, scan the hitboxes to see if the mouse is over one. This sets the cursor.
        //  We borrow the decision hitbox list since decisions and topics can't be active at the same time.
        int i = 0;
        int tOldCursor = mTopicCursor;
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mDecisionHitboxes->PushIterator();
        while(rHitbox)
        {
            //--Point is within:
            if(rHitbox->IsPointWithin(tMouseX, tMouseY))
            {
                mTopicCursor = i;
                mDecisionHitboxes->PopIterator();
                break;
            }

            //--Next.
            i ++;
            rHitbox = (TwoDimensionReal *)mDecisionHitboxes->AutoIterate();
        }

        //--If the cursor changed and the new cursor was not -1, play the highlight sound.
        if(tOldCursor != mTopicCursor && mTopicCursor != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--If the player left-clicks, execute the decision under the cursor.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Get the topic in question.
            DialogueTopic *rSelectedTopic = (DialogueTopic *)mrCurrentTopicListing->GetElementBySlot(mTopicCursor);
            if(!rSelectedTopic) return;

            //--Normal case: Execute.
            if(rSelectedTopic != mGoodbyeTopic)
            {
                //--Flags.
                mLastTopicCursor = mTopicCursor;
                mIsTopicsMode = false;

                //--Auto-update the topic.
                rSelectedTopic->SetNPCLevel(mCurrentTopicActor, rSelectedTopic->GetLevel());

                //--Call this script. It will handle the rest.
                char *tPathBuf = InitializeString("%s/Topic Launcher.lua", xTopicsDirectory);
                Clear();
                LuaManager::Fetch()->ExecuteLuaFile(tPathBuf, 2, "S", mCurrentTopicActor, "S", rSelectedTopic->GetInternalName());
                free(tPathBuf);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Goodbye case: Execute the goodbye text. The dialogue closes afterwards.
            else
            {
                //--Flags.
                mLastTopicCursor = 0;
                mIsTopicsMode = false;

                //--Call this script. It will handle the rest.
                char *tPathBuf = InitializeString("%s/Topic Launcher.lua", xTopicsDirectory);
                Clear();
                LuaManager::Fetch()->ExecuteLuaFile(tPathBuf, 2, "S", mCurrentTopicActor, "S", "Goodbye");
                free(tPathBuf);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
    }
}

///========================================== Drawing =============================================
void WorldDialogue::RenderTopicsMode()
{
    ///--[Documentation]
    //--Rendering routine for topics mode. Shares a lot of code with Major Sequences since portraits are expected
    //  to be shown during a topic sequence. Does not have the top dialogue box, though.
    if(!Images.mIsReady) return;

    //--In mouse mode:
    if(mDecisionUsesMouse)
    {
        RenderTopicsMouse();
        return;
    }

    ///--[Mixing]
    float tAlphaFactor = 1.0f;
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);

    ///--[Backing and Widescreen]
    //--Base borders.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = VIRTUAL_CANVAS_X;
    float cBot = VIRTUAL_CANVAS_Y;

    //--Render a black border under everything else. This makes portraits pop out of the screen better.
    glColor4f(0.1f, 0.1f, 0.1f, 0.80f * tAlphaFactor);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);

    ///--[Backing]
    //--Backing image
    Images.Data.rNamelessBox->Draw();

    ///--[Topic Listing]
    //--Constants.
    float cTextHeight = Images.Data.rDialogueFont->GetTextHeight() * mTextScale;

    //--Positioning.
    float cCursorIndent = 12.0f;
    float tCursorX =  42.0f;
    float tCursorY = 592.0f;
    glTranslatef(tCursorX, tCursorY, 0.0f);

    //--Render the topics.
    int i = 0;
    int tActualRenders = 0;
    DialogueTopic *rTopic = (DialogueTopic *)mrCurrentTopicListing->PushIterator();
    while(rTopic)
    {
        //--Value is less than the topic scroll, so don't render it.
        if(i < mTopicScroll)
        {
            i ++;
            rTopic = (DialogueTopic *)mrCurrentTopicListing->AutoIterate();
            continue;
        }

        //--Stop rendering if 10 topics render.
        if(tActualRenders >= 10)
        {
            mrCurrentTopicListing->PopIterator();
            break;
        }

        //--Compute position.
        float tPosX = ((i-mTopicScroll) % 2) * 460.0f;
        float tPosY = ((i-mTopicScroll) / 2) * (cTextHeight);

        //--If this dialogue has been seen before, nothing but its name gets rendered.
        if(mrCurrentTopicListing->GetIteratorName()[0] == 'Y')
        {

        }
        //--Otherwise, render an indicator that there's something new.
        else
        {
            glColor4f(0.9f, 0.0f, 0.9f, 1.0f * tAlphaFactor);
        }

        //--Render the topic, indented slightly for the cursor.
        Images.Data.rDialogueFont->DrawTextArgs(tPosX + cCursorIndent, tPosY, 0, mTextScale, "%s", rTopic->GetDisplayName());

        //--Next.
        i ++;
        tActualRenders ++;
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
        rTopic = (DialogueTopic *)mrCurrentTopicListing->AutoIterate();
    }

    //--Render the cursor.
    float cCursorWid = 5.0f;
    float cCursorHei = (cTextHeight * 0.80f) - 2.0f;
    float tTopicCursorX = ((mTopicCursor-mTopicScroll) % 2) * 460.0f;
    float tTopicCursorY = (((mTopicCursor-mTopicScroll) / 2) * (cTextHeight)) + 5.0f;

    //--Render.
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLES);
        glVertex2f(tTopicCursorX,              tTopicCursorY);
        glVertex2f(tTopicCursorX + cCursorWid, tTopicCursorY + cCursorHei / 2.0f);
        glVertex2f(tTopicCursorX,              tTopicCursorY + cCursorHei);
    glEnd();
    glEnable(GL_TEXTURE_2D);

    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);
    glTranslatef(tCursorX * -1, tCursorY * -1, 0.0f);

    ///--[Portraits]
    //--Render the portraits with the subroutine.
    RenderDialogueActors(1.0f);

    ///--[Flash]
    RenderFlash();
}
void WorldDialogue::RenderTopicsMouse()
{
    ///--[Documentation]
    //--Renders the topic boxes in mouse mode. This gives each topic a button. Very similar to the decision
    //  code as the same hitboxes are used.

    ///--[Execution]
    //--Run across the topic/hitbox list, which should have the same number of entries.
    DialogueTopic *rTopic = (DialogueTopic *)mrCurrentTopicListing->PushIterator();
    TwoDimensionReal *rHitbox = (TwoDimensionReal *)mDecisionHitboxes->PushIterator();
    while(rTopic && rHitbox)
    {
        //--Render at position.
        Images.Data.rBorderCard->Draw(rHitbox->mLft, rHitbox->mTop);

        //--Text.
        Images.Data.rDecisionFont->DrawText(rHitbox->mLft + 20.0f, rHitbox->mTop + 15.0f, 0, 1.0f, rTopic->GetDisplayName());

        //--Next.
        rTopic = (DialogueTopic *)mrCurrentTopicListing->AutoIterate();
        rHitbox = (TwoDimensionReal *)mDecisionHitboxes->AutoIterate();
    }
}
