///======================================== CarnUIEvent ===========================================
//--Event handler, allows the player to respond to an event, such as opening a gate or a treasure
//  chest. This is done by picking a character and an action, and the character performs the action.
//  Most of this routes through scripts.

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"
#include "StarLinkedList.h"

///===================================== Local Structures =========================================
typedef struct CEventOptionPack
{
    //--Members
    StarLinkedList *mDescription; //char *, master
    StarLinkedList *mRevealed; //char *, master
    char *mScript;
    char *mFiringCode;

    //--Functions
    void Initialize()
    {
        mDescription = NULL;
        mRevealed = NULL;
        mScript = NULL;
        mFiringCode = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        CEventOptionPack *rPtr = (CEventOptionPack *)pPtr;
        delete rPtr->mDescription;
        delete rPtr->mRevealed;
        free(rPtr->mScript);
        free(rPtr->mFiringCode);
        free(rPtr);
    }
}CEventOptionPack;

///===================================== Local Definitions ========================================
//--Option Code
#define CARNUI_EVENT_NO_OPTION -1

//--Hitboxes
#define CARNUI_EVENT_HITBOX_ARROWLFT 0
#define CARNUI_EVENT_HITBOX_ARROWRGT 1
#define CARNUI_EVENT_HITBOX_REVEAL 2
#define CARNUI_EVENT_HITBOX_OPTION_START 3

//--Positions
#define CARNUI_EVENT_OPTION_Y 60.0f

///========================================== Classes =============================================
class CarnUIEvent : public CarnUICommon
{
    protected:
    ///--[System]
    ///--[Event Data]
    bool mRecheckEvents;
    char *mEventName;
    StarLinkedList *mEventDescription; //char *, master

    ///--[Character]
    int mInstanceNumber;
    int mCharacterCursor;
    const char *rCharacterInternalName;
    const char *rCharacterName;
    int mCharacterHP;
    int mCharacterHPMax;
    int mCharacterSP;
    int mCharacterSPMax;
    int mCharacterInf;
    int mCharacterInfMax;

    ///--[Revealing]
    bool mIsRevealed;
    bool mCanReveal;
    char *mRevealScriptPath;
    char *mRevealString;

    ///--[Options]
    int mOptionCursor;
    StarLinkedList *mCharacterList; //StarLinkedList, master. Holds CEventOptionPack *, master

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Name;
        StarFont *rFont_Stats;
        StarFont *rFont_Description;
        StarFont *rFont_Button;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Option;
        StarBitmap *rOverlay_ArrowLft;
        StarBitmap *rOverlay_ArrowRgt;
        StarBitmap *rOverlay_HPFill;
        StarBitmap *rOverlay_MeterFrames;
        StarBitmap *rOverlay_PortraitBack;
        StarBitmap *rOverlay_RevealButton;
        StarBitmap *rOverlay_SPFill;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIEvent();
    virtual ~CarnUIEvent();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    void SetInstanceNumber(int pNumber);
    void SetEventName(const char *pName);
    void SetEventDescription(const char *pDescription);
    void SetIsRevealed(bool pIsRevealed);
    void SetCanReveal(bool pCanReveal);
    void SetRevealPath(const char *pPath);
    void SetRevealString(const char *pPath);
    void SetOptionCursor(int pCursor);
    void ClearOptions();
    void RegisterCharacter(const char *pName);
    void RegisterOption(const char *pCharacter, const char *pName, const char *pScript, const char *pFiringCode, const char *pDescription);
    void SetOptionRevealText(const char *pCharacter, const char *pName, const char *pRevealText);
    void RecheckEvents();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void SetCharacterCursor(int pCursor);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnUIEvent_SetProperty(lua_State *L);
