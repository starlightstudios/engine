//--Base
#include "CarnUIInventory.h"

//--Classes
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "CarnUIHealParty.h"
#include "CarnUIRegem.h"
#include "CarnationMenu.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarUIPopup.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

///======================================= Core Methods ===========================================
void CarnUIInventory::BuildPopupMenu(int pItemIndex)
{
    ///--[Documentation]
    //--Given an item index, builds a popup menu around that item.
    mPopupItemIndex = -1;
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetItemList();
    AdventureItem *rHighlightItem = (AdventureItem *)rItemList->GetElementBySlot(pItemIndex);
    if(!rHighlightItem) return;

    ///--[List Assembly]
    //--Clear the list of any existing commands.
    mPopup->ClearEntries();

    //--If the item can be used:
    if(rHighlightItem->GetTagCount("Can Use") > 0)
    {
        mPopup->RegisterEntry("00 Use", "Use");
    }

    //--If the item can be modified:
    if(rHighlightItem->GetTagCount("Can Modify") > 0)
    {
        mPopup->RegisterEntry("01 Modify", "Modify");
    }

    //--If the item can be alchemized:
    if(rHighlightItem->GetTagCount("Can Alchemize") > 0)
    {
        mPopup->RegisterEntry("02 Alchemize", "Alchemize");
    }

    //--If the item can be discarded:
    if(rHighlightItem->GetTagCount("Can Discard") > 0)
    {
        mPopup->RegisterEntry("03 Discard", "Discard");
    }

    ///--[Activation]
    //--Check if the command list registered any commands. If so, activate popup mode.
    if(mPopup->GetTotalEntries() < 1) return;

    //--Flags.
    mIsPopupActive = true;
    mPopupItemIndex = pItemIndex;
    mPopup->SetPosition(mMouseX + 30.0f, mMouseY);
    mPopup->FinalizeList();
}

///===================================== Popup Execution =========================================
void CarnUIInventory::ExecutePopupAction(const char *pAction)
{
    ///--[Documentation]
    //--Replies to the given action. The standard "CLOSE" is handled as well as performing actions
    //  like alchemizing or discarding items.
    if(!pAction) return;

    ///--[System Calls]
    //--"CLOSE" and "CLOSE OUT" will exit popup mode.
    if(!strcasecmp(pAction, "CLOSE") || !strcasecmp(pAction, "CLOSE OUT"))
    {
        mIsPopupActive = false;
    }
    //--"NO HIT" is ignored.
    else if(!strcasecmp(pAction, "NO HIT"))
    {

    }
    //--"NOTHING" is ignored.
    else if(!strcasecmp(pAction, "NOTHING"))
    {

    }
    ///--[Execution Calls]
    //--"00 Use" brings up the HealParty interface, allowing the user to use the item on the party.
    else if(!strcasecmp(pAction, "00 Use"))
    {
        //--Fetch the HealParty UI.
        CarnUIHealParty *rHealPartyUI = CarnUIHealParty::Fetch();
        if(!rHealPartyUI) { FlagExit(); return; }

        //--Give the inventory index to the UI.
        rHealPartyUI->SetItemMode(mPopupItemIndex);
        rHealPartyUI->SetExitToInventory();

        //--Order the UI to switch to the Regem UI.
        FlagExit();
        mCodeBackward = CARNMENU_MODE_HEALPARTY + CARNMENU_CHANGEMODE_OFFSET;

        //--Close the popup immediately.
        mIsPopupActive = false;
    }
    //--"01 Modify" brings up the gem interface to modify the item. This can include disassembling it.
    else if(!strcasecmp(pAction, "01 Modify"))
    {
        //--Fetch the RegemUI. If it fails to resolve, just exit.
        CarnUIRegem *rRegemUI = CarnUIRegem::Fetch();
        if(!rRegemUI) { FlagExit(); return; }

        //--Get the item in question.
        AdventureInventory *rInventory = AdventureInventory::Fetch();
        StarLinkedList *rItemList = rInventory->GetItemList();
        AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);
        if(!rItem) { FlagExit(); return; }

        //--Order the UI to switch to the Regem UI.
        FlagExit();
        mCodeBackward = CARNMENU_MODE_REGEM + CARNMENU_CHANGEMODE_OFFSET;

        //--Tell the Regem UI to modify the selected item.
        rRegemUI->SetExitToInventory(true);
        rRegemUI->SetConsideredItem(rItem);

        //--Close the popup immediately.
        mIsPopupActive = false;
    }
    //--"02 Alchemize" confirms if the player wants to alchemize the item to get some cash back.
    else if(!strcasecmp(pAction, "02 Alchemize"))
    {
        mConfirmCode = CARNUI_INV_ALCHEMIZE;
        mConfirmItemSlot = mItemCursor;
        mIsPopupActive = false;
    }
    //--"03 Discard" confirms if the player wants to discard the item entirely.
    else if(!strcasecmp(pAction, "03 Discard"))
    {
        mConfirmCode = CARNUI_INV_DISCARD;
        mConfirmItemSlot = mItemCursor;
        mIsPopupActive = false;
    }
    ///--[Error Case]
    //--Unhandled.
    else
    {
        fprintf(stderr, "CarnUIInventory:ExecutePopupAction() - Warning, no handler for case %s.\n", pAction);
    }
}
void CarnUIInventory::HandleConfirmation()
{
    ///--[Documentation]
    //--User clicked the confirm button, carry on with whatever action was being confirmed. Needs to reset
    //  the flag after handling.

    ///--[Execution]
    //--Alchemize the item.
    if(mConfirmCode == CARNUI_INV_ALCHEMIZE)
    {
        AlchemizeItem(mConfirmItemSlot);
    }
    //--Discard the item.
    else if(mConfirmCode == CARNUI_INV_DISCARD)
    {
        DiscardItem(mConfirmItemSlot);
    }

    //--Reset flag.
    mConfirmCode = CARNUI_INV_NONE;
}
void CarnUIInventory::AlchemizeItem(int pItemIndex)
{
    ///--[Documentation]
    //--Removes the item from the inventory and gives 25% of its value back in cash. Any sub-items
    //  are also alchemized.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Execution]
    //--Get the item.
    StarLinkedList *rItemList = rInventory->GetItemList();
    AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mConfirmItemSlot);
    if(!rItem) return;

    //--Get the value.
    int tAlchemizeValue = GetAlchemizeValue(rItem);

    //--Remove it.
    rItemList->RemoveElementI(pItemIndex);

    //--Put cash in the inventory.
    rInventory->SetPlatina(rInventory->GetPlatina() + tAlchemizeValue);
}
void CarnUIInventory::DiscardItem(int pItemIndex)
{
    ///--[Documentation]
    //--Discard an item entirely.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Execution]
    //--Flatly remove the item from the list.
    StarLinkedList *rItemList = rInventory->GetItemList();
    rItemList->RemoveElementI(pItemIndex);
}
void CarnUIInventory::DisassembleItem(int pItemIndex)
{

}
