//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"
#include "AliasStorage.h"
#include "FieldAbility.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

///======================================== Lua Hooking ===========================================
void AdvCombat::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombat_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombat_GetProperty", &Hook_AdvCombat_GetProperty);

    /* AdvCombat_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombat_SetProperty", &Hook_AdvCombat_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AdvCombat_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Statics]
    //AdvCombat_GetProperty("Is AI Spawning Events") (1 Boolean) (Static)
    //AdvCombatEntity_GetProperty("Is Abyss Combat") (1 Boolean) (Static)

    //--[System]
    //AdvCombat_GetProperty("Party Of ID", iUniqueID) (1 Integer)
    //AdvCombat_GetProperty("Is Memory Cursor") (1 Boolean)
    //AdvCombat_GetProperty("Is Unretreatable") (1 Boolean)
    //AdvCombat_GetProperty("Is Unsurrenderable") (1 Boolean)
    //AdvCombat_GetProperty("Is Unloseable") (1 Boolean)
    //AdvCombat_GetProperty("Is Tourist Mode") (1 Boolean)
    //AdvCombat_GetProperty("Get Resolution Type") (1 Integer)
    //AdvCombat_GetProperty("Is Showing Detailed Descriptions") (1 Boolean)
    //AdvCombat_GetProperty("Is Introduction") (1 Boolean)

    //--[Aliases]
    //AdvCombat_GetProperty("Get Script Of Alias", sAlias) (1 String)

    //--[Field Abilities]
    //AdvCombat_GetProperty("Field Ability Name", iSlot) (1 String)

    //--[Turn Handling]
    //AdvCombat_GetProperty("Does Player Have Initiative") (1 Boolean)
    //AdvCombat_GetProperty("Current Turn") (1 Integer)
    //AdvCombat_GetProperty("ID of Acting Entity") (1 Integer)
    //AdvCombat_GetProperty("Is Entity On Turn List", iUniqueID) (1 Boolean)

    //--[Enemies]
    //AdvCombat_GetProperty("Enemy Party Size") (1 Integer)
    //AdvCombat_GetProperty("Enemy ID", iSlot) (1 Integer)
    //AdvCombat_GetProperty("Generate Unique ID") (1 Integer)
    //AdvCombat_GetProperty("Do Any Enemies Block Auto Win") (1 Boolean)
    //AdvCombat_GetProperty("Enemy Auto Handler") (1 String)

    //--[Party]
    //AdvCombat_GetProperty("Does Party Member Exist", sMemberName) (1 Boolean)
    //AdvCombat_GetProperty("Is Member In Active Party", sMemberName) (1 Boolean)
    //AdvCombat_GetProperty("Is Member In Active Party By ID", iUniqueID) (1 Boolean)
    //AdvCombat_GetProperty("Name of Active Member", iSlot) (1 String)
    //AdvCombat_GetProperty("Name of Combat Member", iSlot) (1 String)
    //AdvCombat_GetProperty("Roster Size") (1 Integer)
    //AdvCombat_GetProperty("Active Party Size") (1 Integer)
    //AdvCombat_GetProperty("Combat Party Size") (1 Integer)
    //AdvCombat_GetProperty("Active Party ID", iSlot) (1 Integer)
    //AdvCombat_GetProperty("Combat Party ID", iSlot) (1 Integer)
    //AdvCombat_GetProperty("Slot of Member In Active Party", sMemberName) (1 Integer)
    //AdvCombat_GetProperty("Slot of Member In Combat Party", sMemberName) (1 Integer)

    //--[Abilities]
    //AdvCombat_GetProperty("Is Ability Unlocked", sPartyMember, sJob, sAbilityName) (1 Boolean)

    //--[AI Target Queries]
    //--Target cluster info. Can only be used during AI scripts. If you need target information
    //  for an executing ability, use [Ability Execution] below.
    //AdvCombat_GetProperty("Last Target Code") (1 String)
    //AdvCombat_GetProperty("Total Target Clusters") (1 Integer)
    //AdvCombat_GetProperty("Name of Target Cluster", iIndex) (1 String)
    //AdvCombat_GetProperty("Total Targets In Cluster", iClusterIndex) (1 Integer)
    //AdvCombat_GetProperty("Target ID In Cluster", iClusterIndex, iTargetIndex) (1 Integer)

    //--[Ability Execution]
    //AdvCombat_GetProperty("Active Event Targets") (1 Integer)
    //AdvCombat_GetProperty("ID Of Target", iSlot) (1 Integer)

    //--[Event Query]
    //--Note: Can **Only** be used when running response code ACA_SCRIPT_CODE_EVENT_QUEUED. rEventZeroPackage
    //  will be NULL at all other times.
    //AdvCombat_GetProperty("Query Originator ID") (1 Integer)
    //AdvCombat_GetProperty("Query Ability Name") (1 String)
    //AdvCombat_GetProperty("Query Ability Path") (1 String)
    //AdvCombat_GetProperty("Query Targets Cluster Name") (1 String)
    //AdvCombat_GetProperty("Query Targets Total") (1 Integer)
    //AdvCombat_GetProperty("Query Targets ID", iSlot) (1 Integer)

    //--[Prediction Boxes]
    //--Note: Can only be used during ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX calls.
    //AdvCombat_GetProperty("Prediction Get Cluster ID") (1 Integer)
    //AdvCombat_GetProperty("Prediction Get Cluster Name") (1 String)
    //AdvCombat_GetProperty("Prediction Get Targets Total") (1 Integer)
    //AdvCombat_GetProperty("Prediction Get Targets ID", iSlot) (1 Integer)

    //--[Application Response]
    //--Queries rCurrentApplicationPack, which is only populated during ACE_AI_SCRIPT_CODE_APPLICATION_START and ACE_AI_SCRIPT_CODE_APPLICATION_END.
    //AdvCombat_GetProperty("Application Get Originator ID") (1 Integer)
    //AdvCombat_GetProperty("Application Get Target ID") (1 Integer)
    //AdvCombat_GetProperty("Application Get String") (1 String)

    //--[Victory]
    //--Only valid during the victory sequence.
    //AdvCombat_GetProperty("Victory XP") (1 Integer)
    //AdvCombat_GetProperty("Victory Platina") (1 Integer)
    //AdvCombat_GetProperty("Victory JP") (1 Integer)
    //AdvCombat_GetProperty("Victory Doctor") (1 Integer)
    //AdvCombat_GetProperty("Bonus XP") (1 Integer)
    //AdvCombat_GetProperty("Bonus Platina") (1 Integer)
    //AdvCombat_GetProperty("Bonus JP") (1 Integer)
    //AdvCombat_GetProperty("Bonus Doctor") (1 Integer)

    //--[Graveyard]
    //AdvCombat_GetProperty("Entities in Graveyard") (1 Integer)
    //AdvCombat_GetProperty("ID of Graveyard Entity", iSlot) (1 Integer)

    //--[Effects]
    //AdvCombat_GetProperty("Does Effect Exist", iEffectID)

    ///--[Arguments]
    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombat_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--True if the AI should push events, false if the turn is beginning but the AI
    //  should not spawn events.
    if(!strcasecmp(rSwitchType, "Is AI Spawning Events"))
    {
        lua_pushboolean(L, AdvCombat::xIsAISpawningEvents);
        return 1;
    }
    //--True if the the current combat type is AbyssCombat, false otherwise.
    else if(!strcasecmp(rSwitchType, "Is Abyss Combat") && tArgs == 1)
    {
        AdvCombat *rAdventureCombat = AdvCombat::Fetch();
        if(!rAdventureCombat || !rAdventureCombat->IsOfType(POINTER_TYPE_ABYSSCOMBAT))
        {
            lua_pushboolean(L, false);
        }
        else
        {
            lua_pushboolean(L, true);
        }
        return 1;
    }

    ///--[Dynamic]
    //--Type check. Can never fail, as the Fetch() function performs lazy init.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[System]
    //--Which party grouping, of series AC_PARTY_GROUP_[X] the given ID is in.
    if(!strcasecmp(rSwitchType, "Party Of ID") && tArgs == 2)
    {
        lua_pushinteger(L, rAdventureCombat->GetPartyGroupingID(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Whether the memory cursor option is true or false.
    else if(!strcasecmp(rSwitchType, "Is Memory Cursor") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->IsMemoryCursor());
        tReturns = 1;
    }
    //--Returns whether or not the current battle cannot be retreated from.
    else if(!strcasecmp(rSwitchType, "Is Unretreatable") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->IsUnretreatable());
        tReturns = 1;
    }
    //--Returns whether or not the current battle cannot be surrendered in.
    else if(!strcasecmp(rSwitchType, "Is Unsurrenderable") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->IsUnsurrenderable());
        tReturns = 1;
    }
    //--Returns whether or not the current battle can be lost.
    else if(!strcasecmp(rSwitchType, "Is Unloseable") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->IsUnloseable());
        tReturns = 1;
    }
    //--Returns whether or not tourist mode is active.
    else if(!strcasecmp(rSwitchType, "Is Tourist Mode") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->IsTouristMode());
        tReturns = 1;
    }
    //--Returns a code indicating who won the battle, if anyone, and how.
    else if(!strcasecmp(rSwitchType, "Get Resolution Type") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetResolutionCode());
        tReturns = 1;
    }
    //--Returns whether or not detailed descriptions are being shown.
    else if(!strcasecmp(rSwitchType, "Is Showing Detailed Descriptions") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->IsShowingDetailedDescriptions());
        tReturns = 1;
    }
    //--Returns true if currently in the introduction.
    else if(!strcasecmp(rSwitchType, "Is Introduction") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->IsIntroduction());
        tReturns = 1;
    }
    ///--[Aliases]
    //--Returns the enemy path of the associated alias.
    else if(!strcasecmp(rSwitchType, "Get Script Of Alias") && tArgs == 2)
    {
        AliasStorage *rAliasStorage = rAdventureCombat->GetEnemyAliasStorage();
        const char *rPath = rAliasStorage->GetString(lua_tostring(L, 2));
        if(!rPath)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rPath);
        }
        tReturns = 1;
    }
    ///--[Field Abilities]
    //--Returns the unique name of the ability in the given slot, or "Null" if empty.
    else if(!strcasecmp(rSwitchType, "Field Ability Name") && tArgs == 2)
    {
        FieldAbility *rAbility = rAdventureCombat->GetFieldAbility(lua_tointeger(L, 2));
        if(!rAbility)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rAbility->GetInternalName());
        }
        tReturns = 1;
    }
    ///--[Turn Handling]
    //--Returns true if the player ambushed the enemy from behind, false otherwise.
    else if(!strcasecmp(rSwitchType, "Does Player Have Initiative") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->DoesPlayerHaveInitiative());
        tReturns = 1;
    }
    //--Turn index, starting at 0 and incrementing.
    else if(!strcasecmp(rSwitchType, "Current Turn") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetTurn());
        tReturns = 1;
    }
    //--UniqueID of the acting entity. 0 if no entity is acting.
    else if(!strcasecmp(rSwitchType, "ID of Acting Entity") && tArgs == 1)
    {
        AdvCombatEntity *rEntity = rAdventureCombat->GetActingEntity();
        if(rEntity)
            lua_pushinteger(L, rEntity->GetID());
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Whether or not the entity with the ID is on the turn list. Acting entity is always on the list.
    //  This is used to detect if an entity has already acted this turn.
    else if(!strcasecmp(rSwitchType, "Is Entity On Turn List") && tArgs == 2)
    {
        lua_pushboolean(L, rAdventureCombat->IsEntityOnTurnList(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    ///--[Enemies]
    //--How many enemies are in the enemy party.
    else if(!strcasecmp(rSwitchType, "Enemy Party Size") && tArgs == 1)
    {
        StarLinkedList *rEnemyList = rAdventureCombat->GetEnemyPartyList();
        lua_pushinteger(L, rEnemyList->GetListSize());
        tReturns = 1;
    }
    //--Returns the enemy ID in the given slot, or 0 on error.
    else if(!strcasecmp(rSwitchType, "Enemy ID") && tArgs == 2)
    {
        StarLinkedList *rEnemyList = rAdventureCombat->GetEnemyPartyList();
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rEnemyList->GetElementBySlot(lua_tointeger(L, 2));
        if(!rEntity)
        {
            lua_pushinteger(L, 0);
        }
        else
        {
            lua_pushinteger(L, rEntity->GetID());
        }
        tReturns = 1;
    }
    //--Returns an integer that is unique for generating enemy names. Is reset each time combat reinitializes.
    //  Standard naming pattern is "Autogen|XX" where XX is the ID. Use leading zeroes to pad.
    else if(!strcasecmp(rSwitchType, "Generate Unique ID") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetNextEnemyID());
        tReturns = 1;
    }
    //--Returns true if any enemies in the current world references block mug auto wins.
    else if(!strcasecmp(rSwitchType, "Do Any Enemies Block Auto Win") && tArgs == 1)
    {
        lua_pushboolean(L, rAdventureCombat->DoAnyEnemiesBlockAutoWin());
        tReturns = 1;
    }
    //--Returns the path to the enemy auto handler.
    else if(!strcasecmp(rSwitchType, "Enemy Auto Handler") && tArgs == 1)
    {
        lua_pushstring(L, rAdventureCombat->GetEnemyAutoScript());
        tReturns = 1;
    }
    ///--[Party]
    //--True if the named member exists, false if not.
    else if(!strcasecmp(rSwitchType, "Does Party Member Exist") && tArgs == 2)
    {
        lua_pushboolean(L, rAdventureCombat->DoesPartyMemberExist(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--True if the named member is in the active party.
    else if(!strcasecmp(rSwitchType, "Is Member In Active Party") && tArgs == 2)
    {
        lua_pushboolean(L, rAdventureCombat->IsPartyMemberActive(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--True if the named member is in the active party, uses unique ID.
    else if(!strcasecmp(rSwitchType, "Is Member In Active Party By ID") && tArgs == 2)
    {
        lua_pushboolean(L, rAdventureCombat->IsPartyMemberActiveI(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Name of the active party member in the given slot.
    else if(!strcasecmp(rSwitchType, "Name of Active Member") && tArgs == 2)
    {
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(lua_tointeger(L, 2));
        if(rEntity)
            lua_pushstring(L, rEntity->GetName());
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    //--Name of the combat party member in the given slot.
    else if(!strcasecmp(rSwitchType, "Name of Combat Member") && tArgs == 2)
    {
        AdvCombatEntity *rEntity = rAdventureCombat->GetCombatMemberI(lua_tointeger(L, 2));
        if(rEntity)
            lua_pushstring(L, rEntity->GetName());
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    //--Total number of characters in the player roster.
    else if(!strcasecmp(rSwitchType, "Roster Size") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetRosterCount());
        tReturns = 1;
    }
    //--How many entities are in the active party.
    else if(!strcasecmp(rSwitchType, "Active Party Size") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetActivePartyCount());
        tReturns = 1;
    }
    //--How many entities are in the combat party, including temporary allies.
    else if(!strcasecmp(rSwitchType, "Combat Party Size") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetCombatPartyCount());
        tReturns = 1;
    }
    //--ID of active party member in the given slot.
    else if(!strcasecmp(rSwitchType, "Active Party ID") && tArgs == 2)
    {
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(lua_tointeger(L, 2));
        if(rEntity)
            lua_pushinteger(L, rEntity->GetID());
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--ID of combat party member in the given slot.
    else if(!strcasecmp(rSwitchType, "Combat Party ID") && tArgs == 2)
    {
        AdvCombatEntity *rEntity = rAdventureCombat->GetCombatMemberI(lua_tointeger(L, 2));
        if(rEntity)
            lua_pushinteger(L, rEntity->GetID());
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Returns which slot the named member is in in the active party. Returns -1 if not found.
    else if(!strcasecmp(rSwitchType, "Slot of Member In Active Party") && tArgs == 2)
    {
        StarLinkedList *rActiveList = rAdventureCombat->GetActivePartyList();
        lua_pushinteger(L, rActiveList->GetSlotOfElementByName(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns which slot the named member is in in the combat party. Returns -1 if not found.
    else if(!strcasecmp(rSwitchType, "Slot of Member In Combat Party") && tArgs == 2)
    {
        StarLinkedList *rCombatList = rAdventureCombat->GetCombatPartyList();
        lua_pushinteger(L, rCombatList->GetSlotOfElementByName(lua_tostring(L, 2)));
        tReturns = 1;
    }
    ///--[Abilities]
    //AdvCombat_GetProperty("Is Ability Unlocked", sPartyMember, sJob, sAbilityName) (1 Boolean)
    else if(!strcasecmp(rSwitchType, "Is Ability Unlocked") && tArgs == 4)
    {
        lua_pushboolean(L, rAdventureCombat->IsAbilityUnlocked(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4)));
        tReturns = 1;
    }
    ///--[AI Target Queries]
    //--Last-used target code when AdvCombat_SetProperty("Run Active Ability Target Script") is used.
    else if(!strcasecmp(rSwitchType, "Last Target Code") && tArgs == 1)
    {
        lua_pushstring(L, rAdventureCombat->GetLastTargetCode());
        tReturns = 1;
    }
    //--These are typically only available during ACE_AI_SCRIPT_CODE_ACTIONBEGIN and ACE_AI_SCRIPT_CODE_FREEACTIONBEGIN.
    //--How many target clusters are populated.
    else if(!strcasecmp(rSwitchType, "Total Target Clusters") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetTargetClusterTotal());
        tReturns = 1;
    }
    //--Name of the target cluster in the given slot.
    else if(!strcasecmp(rSwitchType, "Name of Target Cluster") && tArgs == 2)
    {
        lua_pushstring(L, rAdventureCombat->GetNameOfTargetCluster(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--How many targets are in the given cluster.
    else if(!strcasecmp(rSwitchType, "Total Targets In Cluster") && tArgs == 2)
    {
        //--Get and check the cluster.
        TargetCluster *rCheckCluster = rAdventureCombat->GetTargetClusterI(lua_tointeger(L, 2));
        if(rCheckCluster)
            lua_pushinteger(L, rCheckCluster->mrTargetList->GetListSize());
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--The ID of the target in the given cluster.
    else if(!strcasecmp(rSwitchType, "Target ID In Cluster") && tArgs == 3)
    {
        TargetCluster *rCheckCluster = rAdventureCombat->GetTargetClusterI(lua_tointeger(L, 2));
        if(rCheckCluster && rCheckCluster->mrTargetList)
        {
            AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)rCheckCluster->mrTargetList->GetElementBySlot(lua_tointeger(L, 3));
            if(rCheckEntity)
                lua_pushinteger(L, rCheckEntity->GetID());
            else
                lua_pushinteger(L, 0);
        }
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    ///--[Ability Execution]
    //--How many targets are in the active event package. Only active during ACA_SCRIPT_CODE_EXECUTE.
    else if(!strcasecmp(rSwitchType, "Active Event Targets") && tArgs == 1)
    {
        CombatEventPack *rPackage = rAdventureCombat->GetFiringEventPack();
        if(rPackage && rPackage->mTargetCluster)
        {
            lua_pushinteger(L, rPackage->mTargetCluster->mrTargetList->GetListSize());
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--ID of target in the given slot. Available during ACA_SCRIPT_CODE_EXECUTE.
    else if(!strcasecmp(rSwitchType, "ID Of Target") && tArgs == 2)
    {
        CombatEventPack *rPackage = rAdventureCombat->GetFiringEventPack();
        if(rPackage && rPackage->mTargetCluster)
        {
            AdvCombatEntity *rEntity = (AdvCombatEntity *)rPackage->mTargetCluster->mrTargetList->GetElementBySlot(lua_tointeger(L, 2));
            if(rEntity)
                lua_pushinteger(L, rEntity->GetID());
            else
                lua_pushinteger(L, 0);
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    ///--[Event Query]
    //--During event response, gets the ID of the entity that is acting and enqueued the ability.
    else if(!strcasecmp(rSwitchType, "Query Originator ID") && tArgs == 1)
    {
        CombatEventPack *rQueryPackage = rAdventureCombat->GetEventQueryPack();
        if(rQueryPackage && rQueryPackage->rOriginator)
        {
            lua_pushinteger(L, rQueryPackage->rOriginator->GetID());
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--During event response, gets the name of the ability the acting entity enqueued. This is the internal name
    //  which may be different than the display name.
    else if(!strcasecmp(rSwitchType, "Query Ability Name") && tArgs == 1)
    {
        CombatEventPack *rQueryPackage = rAdventureCombat->GetEventQueryPack();
        if(rQueryPackage && rQueryPackage->rAbility)
        {
            lua_pushstring(L, rQueryPackage->rAbility->GetInternalName());
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--During event response, get the path of the script the ability enqueue is using.
    else if(!strcasecmp(rSwitchType, "Query Ability Path") && tArgs == 1)
    {
        CombatEventPack *rQueryPackage = rAdventureCombat->GetEventQueryPack();
        if(rQueryPackage && rQueryPackage->rAbility)
        {
            lua_pushstring(L, rQueryPackage->rAbility->GetScriptPath());
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--During event response, get the name of the target cluster selected.
    else if(!strcasecmp(rSwitchType, "Query Targets Cluster Name") && tArgs == 1)
    {
        CombatEventPack *rQueryPackage = rAdventureCombat->GetEventQueryPack();
        if(rQueryPackage && rQueryPackage->mTargetCluster)
        {
            lua_pushstring(L, rQueryPackage->mTargetCluster->mDisplayName);
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--During event response, get how many targets are in the target cluster.
    else if(!strcasecmp(rSwitchType, "Query Targets Total") && tArgs == 1)
    {
        CombatEventPack *rQueryPackage = rAdventureCombat->GetEventQueryPack();
        if(rQueryPackage && rQueryPackage->mTargetCluster)
        {
            lua_pushinteger(L, rQueryPackage->mTargetCluster->mrTargetList->GetListSize());
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--During event response, get the ID of the target in the slot.
    else if(!strcasecmp(rSwitchType, "Query Targets ID") && tArgs == 2)
    {
        CombatEventPack *rQueryPackage = rAdventureCombat->GetEventQueryPack();
        if(rQueryPackage && rQueryPackage->mTargetCluster)
        {
            AdvCombatEntity *rEntity = (AdvCombatEntity *)rQueryPackage->mTargetCluster->mrTargetList->GetElementBySlot(lua_tointeger(L, 2));
            if(rEntity)
            {
                lua_pushinteger(L, rEntity->GetID());
            }
            else
            {
                lua_pushinteger(L, 0);
            }
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    ///--[Prediction Boxes]
    //--Note: Can only be used during ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX calls.
    //--Target Cluster's unique ID.
    else if(!strcasecmp(rSwitchType, "Prediction Get Cluster ID") && tArgs == 1)
    {
        TargetCluster *rTargetCluster = rAdventureCombat->GetPredictionTargetCluster();
        if(rTargetCluster)
            lua_pushinteger(L, rTargetCluster->mUniqueID);
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Target Cluster's name.
    else if(!strcasecmp(rSwitchType, "Prediction Get Cluster Name") && tArgs == 1)
    {
        TargetCluster *rTargetCluster = rAdventureCombat->GetPredictionTargetCluster();
        if(rTargetCluster)
            lua_pushstring(L, rTargetCluster->mDisplayName);
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    //--How many targets are in the cluster.
    else if(!strcasecmp(rSwitchType, "Prediction Get Targets Total") && tArgs == 1)
    {
        TargetCluster *rTargetCluster = rAdventureCombat->GetPredictionTargetCluster();
        if(rTargetCluster && rTargetCluster->mrTargetList)
            lua_pushinteger(L, rTargetCluster->mrTargetList->GetListSize());
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--ID of the target in the given slot.
    else if(!strcasecmp(rSwitchType, "Prediction Get Targets ID") && tArgs == 2)
    {
        TargetCluster *rTargetCluster = rAdventureCombat->GetPredictionTargetCluster();
        if(rTargetCluster && rTargetCluster->mrTargetList)
        {
            AdvCombatEntity *rEntity = (AdvCombatEntity *)rTargetCluster->mrTargetList->GetElementBySlot(lua_tointeger(L, 2));
            if(rEntity)
                lua_pushinteger(L, rEntity->GetID());
            else
                lua_pushinteger(L, 0);
        }
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    ///--[Application Response]
    //--Queries rCurrentApplicationPack, which is only populated during ACE_AI_SCRIPT_CODE_APPLICATION_START and ACE_AI_SCRIPT_CODE_APPLICATION_END.
    //--Get originator of application.
    else if(!strcasecmp(rSwitchType, "Application Get Originator ID") && tArgs == 1)
    {
        ApplicationPack *rApplicationPack = rAdventureCombat->GetApplicationPack();
        if(rApplicationPack)
            lua_pushinteger(L, rApplicationPack->mOriginatorID);
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Get target of application.
    else if(!strcasecmp(rSwitchType, "Application Get Target ID") && tArgs == 1)
    {
        ApplicationPack *rApplicationPack = rAdventureCombat->GetApplicationPack();
        if(rApplicationPack)
            lua_pushinteger(L, rApplicationPack->mTargetID);
        else
            lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Get application string.
    else if(!strcasecmp(rSwitchType, "Application Get String") && tArgs == 1)
    {
        ApplicationPack *rApplicationPack = rAdventureCombat->GetApplicationPack();
        if(rApplicationPack)
            lua_pushstring(L, rApplicationPack->mEffectString);
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    ///--[Victory]
    //--XP gained from enemies.
    else if(!strcasecmp(rSwitchType, "Victory XP") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetVictoryXP());
        tReturns = 1;
    }
    //--Platina gained from enemies.
    else if(!strcasecmp(rSwitchType, "Victory Platina") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetVictoryPlatina());
        tReturns = 1;
    }
    //--JP gained from enemies.
    else if(!strcasecmp(rSwitchType, "Victory JP") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetVictoryJP());
        tReturns = 1;
    }
    //--Doctor Bag gained from enemies.
    else if(!strcasecmp(rSwitchType, "Victory Doctor") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetVictoryDoctor());
        tReturns = 1;
    }
    //--XP bonus to be applied.
    else if(!strcasecmp(rSwitchType, "Bonus XP") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetBonusXP());
        tReturns = 1;
    }
    //--Platina bonus to be applied.
    else if(!strcasecmp(rSwitchType, "Bonus Platina") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetBonusPlatina());
        tReturns = 1;
    }
    //--JP bonus to be applied.
    else if(!strcasecmp(rSwitchType, "Bonus JP") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetBonusJP());
        tReturns = 1;
    }
    //--Doctor Bonus to be applied.
    else if(!strcasecmp(rSwitchType, "Bonus Doctor") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetBonusDoctor());
        tReturns = 1;
    }
    ///--[Graveyard]
    //--How many entities are currently in the graveyard.
    else if(!strcasecmp(rSwitchType, "Entities in Graveyard") && tArgs == 1)
    {
        StarLinkedList *rGraveyard = rAdventureCombat->GetGraveyard();
        lua_pushinteger(L, rGraveyard->GetListSize());
        tReturns = 1;
    }
    //--ID of the entity in the graveyard. Can be used to push them.
    else if(!strcasecmp(rSwitchType, "ID of Graveyard Entity") && tArgs == 2)
    {
        StarLinkedList *rGraveyard = rAdventureCombat->GetGraveyard();
        AdvCombatEntity *rEntityInSlot = (AdvCombatEntity *)rGraveyard->GetElementBySlot(lua_tointeger(L, 2));
        if(!rEntityInSlot)
        {
            lua_pushinteger(L, 0);
        }
        else
        {
            lua_pushinteger(L, rEntityInSlot->GetID());
        }
        tReturns = 1;
    }
    ///--[Effects]
    //--Returns true if the given effect ID is in use, false if not.
    else if(!strcasecmp(rSwitchType, "Does Effect Exist") && tArgs == 2)
    {
        lua_pushboolean(L, rAdventureCombat->DoesEffectExist(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    ///--[Error case]
    else
    {
        LuaPropertyError("AdvCombat_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombat_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AdvCombat_SetProperty("Set Diagnostic Callout Path", sPath) (Static)

    //--[System]
    //AdvCombat_SetProperty("Construct")
    //AdvCombat_SetProperty("Reinitialize")
    //AdvCombat_SetProperty("Activate")
    //AdvCombat_SetProperty("Response Path", sPath)
    //AdvCombat_SetProperty("World Pulse", bDontAddNearbyEnemies)
    //AdvCombat_SetProperty("Default Combat Music", sMusicName, fStartPoint)
    //AdvCombat_SetProperty("Default Victory Music", sMusicName)
    //AdvCombat_SetProperty("Next Combat Music", sMusicName, fStartPoint)
    //AdvCombat_SetProperty("Purge Combat DataLibrary")
    //AdvCombat_SetProperty("Push Entity By ID", iEntityID) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Position Party Normally")
    //AdvCombat_SetProperty("Position Enemies Normally")
    //AdvCombat_SetProperty("Set Doctor Resolve Path", sPath)
    //AdvCombat_SetProperty("Set Doctor Resolve Value", iValue)
    //AdvCombat_SetProperty("Set Skill Catalyst Slots", iValue)
    //AdvCombat_SetProperty("Set Paragon Script", sScriptPath)
    //AdvCombat_SetProperty("Set Enemy Auto Script", sScriptPath)
    //AdvCombat_SetProperty("Set Paragon Value", iValue)
    //AdvCombat_SetProperty("Set Post Combat Handler", sPath)
    //AdvCombat_SetProperty("Set Detailed Description Flag", bFlag)
    //AdvCombat_SetProperty("Set Auto Win Path", sPath)
    //AdvCombat_SetProperty("Add Script Execution", sPath)
    //AdvCombat_SetProperty("Set Statistics Path", sPath)
    //AdvCombat_SetProperty("Set Defeated Unit Path", sPath)

    //--[Turn Order]
    //AdvCombat_SetProperty("Give Initiative")
    //AdvCombat_SetProperty("Remove Initiative")
    //AdvCombat_SetProperty("Insert Entity In Turn Order", iEntityID, iTurnSlot)

    //--[System Abilities]
    //AdvCombat_SetProperty("Set Pass Turn Properties", sScriptPath)
    //AdvCombat_SetProperty("Set Retreat Properties", sScriptPath)
    //AdvCombat_SetProperty("Set Surrender Properties", sScriptPath)
    //AdvCombat_SetProperty("Set Volunteer Properties", sScriptPath)
    //AdvCombat_SetProperty("Set Ambush Properties", sScriptPath)
    //AdvCombat_SetProperty("Set Volunteer Script", sScriptPath)
    //AdvCombat_SetProperty("Order Retreat")
    //AdvCombat_SetProperty("Order Surrender")
    //AdvCombat_SetProperty("Instant End")

    //--[Field Abilities]
    //AdvCombat_SetProperty("Set Field Ability", iSlot, sDLPath)
    //AdvCombat_SetProperty("Register Extra Script", sPath)

    //--[Options]
    //AdvCombat_SetProperty("Auto Doctor Bag", bFlag)
    //AdvCombat_SetProperty("Tourist Mode", bFlag)
    //AdvCombat_SetProperty("Memory Cursor", bFlag)

    //--[Resolution Handlers]
    //AdvCombat_SetProperty("Bonus XP", iValue)
    //AdvCombat_SetProperty("Bonus Platina", iValue)
    //AdvCombat_SetProperty("Bonus JP", iValue)
    //AdvCombat_SetProperty("Bonus Doctor", iValue)
    //AdvCombat_SetProperty("Unwinnable", bFlag)
    //AdvCombat_SetProperty("Unloseable", bFlag)
    //AdvCombat_SetProperty("Unsurrenderable", bFlag)
    //AdvCombat_SetProperty("Unretreatable", bFlag)
    //AdvCombat_SetProperty("Standard Retreat Script", sScriptPath)
    //AdvCombat_SetProperty("Standard Victory Script", sScriptPath)
    //AdvCombat_SetProperty("Standard Defeat Script", sScriptPath)
    //AdvCombat_SetProperty("Retreat Script", sScriptPath)
    //AdvCombat_SetProperty("Victory Script", sScriptPath)
    //AdvCombat_SetProperty("Defeat Script", sScriptPath)

    //--[Enemies]
    //AdvCombat_SetProperty("Register Enemy", sInternalName, iReinforcementTurns) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Clear Enemy Aliases")
    //AdvCombat_SetProperty("Create Enemy Path", sPathName, sScriptPath)
    //AdvCombat_SetProperty("Create Enemy Alias", sAliasName, sPathName)
    //AdvCombat_SetProperty("Trip Auto Win")

    //--[Party]
    //AdvCombat_SetProperty("Push Party Member", sInternalName) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Party Member By Slot", iSlot) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Active Party Member", sInternalName) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Active Party Member By Slot", iSlot) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Combat Party Member", sInternalName) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Combat Party Member By Slot", iSlot) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Register Party Member", sInternalName) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Clear Party")
    //AdvCombat_SetProperty("Party Slot", iSlot, sPartyMemberName)
    //AdvCombat_SetProperty("Add To Combat Party", sPartyMemberName)
    //AdvCombat_SetProperty("Move Active To Combat Party", iCombatSlot, sPartyMemberName)
    //AdvCombat_SetProperty("Restore Party")
    //AdvCombat_SetProperty("Restore Roster")
    //AdvCombat_SetProperty("Move Entity To Party", iEntityID)
    //AdvCombat_SetProperty("Remove Entity From Active Party", iEntityID)
    //AdvCombat_SetProperty("Remove Entity From Combat Party", iEntityID)

    //--[Level Up Storage]
    //AdvCombat_SetProperty("Clear Level Up Struct")
    //AdvCombat_SetProperty("Set Level Up Struct Stat", iStatistic, iValue)

    //--[AI Control]
    //AdvCombat_SetProperty("Mark Handled Action")
    //AdvCombat_SetProperty("Set Ability As Active", iSlot)
    //AdvCombat_SetProperty("Set Ability As Active Name", sAbilityName)
    //AdvCombat_SetProperty("Run Active Ability Target Script")
    //AdvCombat_SetProperty("Set Target Cluster As Active", iSlot)
    //AdvCombat_SetProperty("Run Active Ability On Active Targets")
    //AdvCombat_SetProperty("Push Knockout Entity") (Pushes Activity Stack)

    //--[Targeting]
    //AdvCombat_SetProperty("Reset Target Code")
    //AdvCombat_SetProperty("Clear Target Clusters")
    //AdvCombat_SetProperty("Create Target Cluster", sClusterName, sDisplayName)
    //AdvCombat_SetProperty("Add Target To Cluster", sClusterName, iTargetUniqueID)
    //AdvCombat_SetProperty("Remove Target Cluster By Index", iIndex)
    //AdvCombat_SetProperty("Remove Target Cluster By Name", sName)
    //AdvCombat_SetProperty("Remove Target From Cluster", iIndex, iTargetUniqueID)
    //AdvCombat_SetProperty("Run Target Macro", sMacroName, iOriginatorID)

    //--[Prediction Boxes]
    //--Note: Can only be used during ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX calls.
    //AdvCombat_SetProperty("Prediction Push Target", iSlot) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Prediction Create Box", sBoxName)
    //AdvCombat_SetProperty("Prediction Set Host", sBoxName, iEntityID)
    //AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, fOffsetX, fOffsetY)
    //AdvCombat_SetProperty("Prediction Allocate Strings", sBoxName, iStrings)
    //AdvCombat_SetProperty("Prediction Set String Text", sBoxName, iString, sText)
    //AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, iString, iImages)
    //AdvCombat_SetProperty("Prediction Set String Image", sBoxName, iString, iSlot, fYOffset, sDLPath)

    //--[Ability Execution]
    //AdvCombat_SetProperty("Set Event Can Run", bFlag)
    //AdvCombat_SetProperty("Push Acting Entity") (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Event Originator") (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Target", iSlot) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Event Timer", iTicks)
    //AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTicks, sEffect)
    //AdvCombat_SetProperty("Stage Entity By ID", iUniqueID)
    //AdvCombat_SetProperty("Set As Free Action")
    //AdvCombat_SetProperty("Set As Effortless Action")

    //--[Animations]
    //AdvCombat_SetProperty("Create Animation", sName) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Create Animation Instance", sAnimationName, sRefName, iStartTicks, fXCenter, fYCenter)

    //--[Effects]
    //AdvCombat_SetProperty("Create Effect", iTargetID) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Effect", iEffectID) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Store Effects Referencing", iTargetID) (1 Integer)
    //AdvCombat_SetProperty("Push Temp Effects", iSlot) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Clear Temp Effects")
    //AdvCombat_SetProperty("Pulse Effect Removal")
    //AdvCombat_SetProperty("Remove Effect", iUniqueID)
    //AdvCombat_SetProperty("Remove All Effects")

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombat_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Script called for callout diagnostics. Used in Monoceros.
    if(!strcasecmp(rSwitchType, "Set Diagnostic Callout Path") && tArgs >= 2)
    {
        ResetString(AdvCombat::xDebugCalloutPath, lua_tostring(L, 2));
        return 0;
    }

    ///--[Dynamic]
    //--Type check. Can never fail, as the Fetch() function performs lazy init.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[System]
    //--Tells the AdvCombat class it can crossload its image pointers.
    if(!strcasecmp(rSwitchType, "Construct") && tArgs == 1)
    {
        rAdventureCombat->Construct();
    }
    //--Clears the AdvCombat back to a blank state. Call this before setting up combat.
    else if(!strcasecmp(rSwitchType, "Reinitialize") && tArgs == 1)
    {
        rAdventureCombat->Reinitialize(true);
    }
    //--Activates the combat handler. Setup can occur before activation.
    else if(!strcasecmp(rSwitchType, "Activate") && tArgs == 1)
    {
        rAdventureCombat->Activate();
    }
    //--Path to a script that runs at various points during combat.
    else if(!strcasecmp(rSwitchType, "Response Path") && tArgs == 2)
    {
        rAdventureCombat->SetCombatResponseScript(lua_tostring(L, 2));
    }
    //--Causes the AdventureLevel to show the combat overlay. Optionally prevents nearby enemies from joining the battle.
    else if(!strcasecmp(rSwitchType, "World Pulse") && tArgs == 2)
    {
        rAdventureCombat->PulseWorld(false, lua_toboolean(L, 2), false);
    }
    //--Sets which song plays during combat if no override is in place.
    else if(!strcasecmp(rSwitchType, "Default Combat Music") && tArgs == 3)
    {
        rAdventureCombat->SetDefaultCombatMusic(lua_tostring(L, 2), lua_tonumber(L, 3));
    }
    //--Sets which song plays on victory.
    else if(!strcasecmp(rSwitchType, "Default Victory Music") && tArgs >= 2)
    {
        rAdventureCombat->SetDefaultVictoryMusic(lua_tostring(L, 2));
    }
    //--Next combat music, overrides music once.
    else if(!strcasecmp(rSwitchType, "Next Combat Music") && tArgs == 3)
    {
        rAdventureCombat->SetNextCombatMusic(lua_tostring(L, 2), lua_tonumber(L, 3));
    }
    //--Delete temporary variables in the DataLibrary used by combat.
    else if(!strcasecmp(rSwitchType, "Purge Combat DataLibrary") && tArgs == 1)
    {
        rAdventureCombat->CleanDataLibrary();
    }
    //--Pushes the entity with the matching ID on the activity stack. Can be friendly or enemy.
    else if(!strcasecmp(rSwitchType, "Push Entity By ID") && tArgs == 2)
    {
        DataLibrary::Fetch()->PushActiveEntity(rAdventureCombat->GetEntityByID(lua_tointeger(L, 2)));
    }
    //--Order all entities to move to their normal positions.
    else if(!strcasecmp(rSwitchType, "Position Party Normally") && tArgs == 1)
    {
        rAdventureCombat->PositionCharactersNormally();
    }
    //--Order enemies to reposition.
    else if(!strcasecmp(rSwitchType, "Position Enemies Normally") && tArgs == 1)
    {
        rAdventureCombat->PositionEnemies();
    }
    //--Sets the script used to resolve doctor bag values based on the turn the enemy was KO'd.
    else if(!strcasecmp(rSwitchType, "Set Doctor Resolve Path") && tArgs == 2)
    {
        rAdventureCombat->SetDoctorResolvePath(lua_tostring(L, 2));
    }
    //--Used by the above script to resolve the value of doctor charges gained.
    else if(!strcasecmp(rSwitchType, "Set Doctor Resolve Value") && tArgs == 2)
    {
        rAdventureCombat->SetDoctorResolvedValue(lua_tointeger(L, 2));
    }
    //--Sets how many skill slots are available due to catalysts.
    else if(!strcasecmp(rSwitchType, "Set Skill Catalyst Slots") && tArgs == 2)
    {
        rAdventureCombat->SetSkillCatalystSlots(lua_tointeger(L, 2));
    }
    //--Sets the script called to determine if an enemy should be a paragon or not.
    else if(!strcasecmp(rSwitchType, "Set Paragon Script") && tArgs == 2)
    {
        rAdventureCombat->SetParagonScript(lua_tostring(L, 2));
    }
    //--If an enemy script path is "AUTO", call this script instead.
    else if(!strcasecmp(rSwitchType, "Set Enemy Auto Script") && tArgs == 2)
    {
        rAdventureCombat->SetEnemyAutoScript(lua_tostring(L, 2));
    }
    //--Sets if the enemy in question should be a paragon or not.
    else if(!strcasecmp(rSwitchType, "Set Paragon Value") && tArgs == 2)
    {
        rAdventureCombat->SetParagonFlag(lua_tointeger(L, 2));
    }
    //--Script that is called after combat. Used to track kills.
    else if(!strcasecmp(rSwitchType, "Set Post Combat Handler") && tArgs == 2)
    {
        rAdventureCombat->SetPostCombatHandler(lua_tostring(L, 2));
    }
    //--Sets whether or not detailed descriptions are shown in combat.
    else if(!strcasecmp(rSwitchType, "Set Detailed Description Flag") && tArgs == 2)
    {
        rAdventureCombat->SetDetailedDescriptionFlag(lua_toboolean(L, 2));
    }
    //--Sets the path used to query auto-win handlers.
    else if(!strcasecmp(rSwitchType, "Set Auto Win Path") && tArgs == 2)
    {
        rAdventureCombat->SetAutoWinPath(lua_tostring(L, 2));
    }
    //--Adds a script that will execute next time the player would gain control. Can run cutscenes.
    else if(!strcasecmp(rSwitchType, "Add Script Execution") && tArgs == 2)
    {
        rAdventureCombat->AddScriptToExec(lua_tostring(L, 2));
    }
    //--Sets which script handles collecting statistics.
    else if(!strcasecmp(rSwitchType, "Set Statistics Path") && tArgs == 2)
    {
        rAdventureCombat->SetStatisticsPath(lua_tostring(L, 2));
    }
    //--Sets script that is called when a unit (friendly or enemy) is defeated.
    else if(!strcasecmp(rSwitchType, "Set Defeated Unit Path") && tArgs == 2)
    {
        rAdventureCombat->SetUnitDefeatScript(lua_tostring(L, 2));
    }
    ///--[Turn Order]
    //--Gives player initiative. Must be called before turn order is resolved to do anything.
    else if(!strcasecmp(rSwitchType, "Give Initiative") && tArgs == 1)
    {
        rAdventureCombat->GivePlayerInitiative();
    }
    //--Removes player initiative. Must be called before turn order is resolved to do anything.
    else if(!strcasecmp(rSwitchType, "Remove Initiative") && tArgs == 1)
    {
        rAdventureCombat->RemovePlayerInitiative();
    }
    //--Inserts the entity with the given ID into the turn order in the given slot.
    else if(!strcasecmp(rSwitchType, "Insert Entity In Turn Order") && tArgs >= 3)
    {
        rAdventureCombat->InsertEntityInTurnOrder(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    ///--[System Abilities]
    //--Pushes the system ability Pass Turn onto the activity stack, then calls the given script to set its properties.
    else if(!strcasecmp(rSwitchType, "Set Pass Turn Properties") && tArgs == 2)
    {
        AdvCombatAbility *rSystemPassTurn = rAdventureCombat->GetSystemPassTurn();
        LuaManager::Fetch()->PushExecPop(rSystemPassTurn, lua_tostring(L, 2), 1, "N", (float)ACA_SCRIPT_CODE_CREATE);
    }
    //--Pushes the system ability Retreat onto the activity stack, then calls the given script to set its properties.
    else if(!strcasecmp(rSwitchType, "Set Retreat Properties") && tArgs == 2)
    {
        AdvCombatAbility *rSystemRetreat = rAdventureCombat->GetSystemRetreat();
        LuaManager::Fetch()->PushExecPop(rSystemRetreat, lua_tostring(L, 2), 1, "N", (float)ACA_SCRIPT_CODE_CREATE);
    }
    //--Pushes the system ability Surrender onto the activity stack, then calls the given script to set its properties.
    else if(!strcasecmp(rSwitchType, "Set Surrender Properties") && tArgs == 2)
    {
        AdvCombatAbility *rSystemSurrender = rAdventureCombat->GetSystemSurrender();
        LuaManager::Fetch()->PushExecPop(rSystemSurrender, lua_tostring(L, 2), 1, "N", (float)ACA_SCRIPT_CODE_CREATE);
    }
    //--Pushes the system ability Volunteer onto the activity stack, then calls the given script to set its properties.
    else if(!strcasecmp(rSwitchType, "Set Volunteer Properties") && tArgs == 2)
    {
        AdvCombatAbility *rSystemVolunteer = rAdventureCombat->GetSystemVolunteer();
        LuaManager::Fetch()->PushExecPop(rSystemVolunteer, lua_tostring(L, 2), 1, "N", (float)ACA_SCRIPT_CODE_CREATE);
    }
    //--Sets which script is called at combat startup to determine if the player uses "Surrender" or "Volunteer".
    else if(!strcasecmp(rSwitchType, "Set Volunteer Script") && tArgs == 2)
    {
        rAdventureCombat->SetVolunteerScript(lua_tostring(L, 2));
    }
    //--Causes the player to immediately retreat from the battle.
    else if(!strcasecmp(rSwitchType, "Order Retreat") && tArgs == 1)
    {
        rAdventureCombat->BeginResolutionSequence(ADVCOMBAT_END_RETREAT);
    }
    //--Causes the player to immediately lose the battle.
    else if(!strcasecmp(rSwitchType, "Order Surrender") && tArgs == 1)
    {
        rAdventureCombat->BeginResolutionSequence(ADVCOMBAT_END_SURRENDER);
    }
    //--Instantly end the battle without firing victory, defeat, etc. cases.
    else if(!strcasecmp(rSwitchType, "Instant End") && tArgs == 1)
    {
        rAdventureCombat->Deactivate();
        rAdventureCombat->ResumeMusic();
    }
    ///--[Field Abilities]
    //--Sets a field ability by its path.
    else if(!strcasecmp(rSwitchType, "Set Field Ability") && tArgs == 3)
    {
        FieldAbility *rAbilityPtr = (FieldAbility *)DataLibrary::Fetch()->GetEntry(lua_tostring(L, 3));
        rAdventureCombat->SetFieldAbility(lua_tointeger(L, 2), rAbilityPtr);
    }
    //--Registers a field ability script to execute during combat.
    else if(!strcasecmp(rSwitchType, "Register Extra Script") && tArgs == 2)
    {
        ExtraScriptPack *nPackage = (ExtraScriptPack *)starmemoryalloc(sizeof(ExtraScriptPack));
        nPackage->Initialize();
        ResetString(nPackage->mPath, lua_tostring(L, 2));
        rAdventureCombat->RegisterExtraScriptPack(nPackage);
    }
    ///--[Options]
    //--Automatically use the Doctor Bag after combat flag.
    else if(!strcasecmp(rSwitchType, "Auto Doctor Bag") && tArgs == 2)
    {
        rAdventureCombat->SetAutoUseDoctorBag(lua_toboolean(L, 2));
    }
    //--Toggles Tourist Mode flag.
    else if(!strcasecmp(rSwitchType, "Tourist Mode") && tArgs == 2)
    {
        rAdventureCombat->SetTouristMode(lua_toboolean(L, 2));
    }
    //--Store last used cursor position for each character in combat flag.
    else if(!strcasecmp(rSwitchType, "Memory Cursor") && tArgs == 2)
    {
        rAdventureCombat->SetMemoryCursor(lua_toboolean(L, 2));
    }
    ///--[Resolution Handlers]
    //--Sets bonus XP. Only available during combat end handlers.
    else if(!strcasecmp(rSwitchType, "Bonus XP") && tArgs == 2)
    {
        rAdventureCombat->SetBonusXP(lua_tointeger(L, 2));
    }
    //--Sets bonus Platina. Only available during combat end handlers.
    else if(!strcasecmp(rSwitchType, "Bonus Platina") && tArgs == 2)
    {
        rAdventureCombat->SetBonusPlatina(lua_tointeger(L, 2));
    }
    //--Sets bonus JP. Only available during combat end handlers.
    else if(!strcasecmp(rSwitchType, "Bonus JP") && tArgs == 2)
    {
        rAdventureCombat->SetBonusJP(lua_tointeger(L, 2));
    }
    //--Sets bonus Doctor. Only available during combat end handlers.
    else if(!strcasecmp(rSwitchType, "Bonus Doctor") && tArgs == 2)
    {
        rAdventureCombat->SetBonusDoctor(lua_tointeger(L, 2));
    }
    //--If true, the player cannot win the battle.
    else if(!strcasecmp(rSwitchType, "Unwinnable") && tArgs == 2)
    {
        rAdventureCombat->SetUnwinnable(lua_toboolean(L, 2));
    }
    //--If true, the player cannot lose the battle.
    else if(!strcasecmp(rSwitchType, "Unloseable") && tArgs == 2)
    {
        rAdventureCombat->SetUnloseable(lua_toboolean(L, 2));
    }
    //--If true, the player cannot surrender.
    else if(!strcasecmp(rSwitchType, "Unsurrenderable") && tArgs == 2)
    {
        rAdventureCombat->SetUnsurrenderable(lua_toboolean(L, 2));
    }
    //--If true, the player cannot retreat.
    else if(!strcasecmp(rSwitchType, "Unretreatable") && tArgs == 2)
    {
        rAdventureCombat->SetUnretreatable(lua_toboolean(L, 2));
    }
    //--Retreat script used if no override is in place.
    else if(!strcasecmp(rSwitchType, "Standard Retreat Script") && tArgs == 2)
    {
        rAdventureCombat->SetStandardRetreatScript(lua_tostring(L, 2));
    }
    //--Victory script used if no override is in place.
    else if(!strcasecmp(rSwitchType, "Standard Victory Script") && tArgs == 2)
    {
        rAdventureCombat->SetStandardVictoryScript(lua_tostring(L, 2));
    }
    //--Defeat script used if no override is in place.
    else if(!strcasecmp(rSwitchType, "Standard Defeat Script") && tArgs == 2)
    {
        rAdventureCombat->SetStandardDefeatScript(lua_tostring(L, 2));
    }
    //--Retreat script for the next battle.
    else if(!strcasecmp(rSwitchType, "Retreat Script") && tArgs == 2)
    {
        rAdventureCombat->SetRetreatScript(lua_tostring(L, 2));
    }
    //--Victory script for the next battle.
    else if(!strcasecmp(rSwitchType, "Victory Script") && tArgs == 2)
    {
        rAdventureCombat->SetVictoryScript(lua_tostring(L, 2));
    }
    //--Defeat script for the next battle.
    else if(!strcasecmp(rSwitchType, "Defeat Script") && tArgs == 2)
    {
        rAdventureCombat->SetDefeatScript(lua_tostring(L, 2));
    }
    ///--[Enemies]
    //--Creates and registers a new AdvCombatEntity as an enemy.
    else if(!strcasecmp(rSwitchType, "Register Enemy") && tArgs == 3)
    {
        AdvCombatEntity *nEntity = new AdvCombatEntity();
        nEntity->SetInternalName(lua_tostring(L, 2));
        rAdventureCombat->RegisterEnemy(lua_tostring(L, 2), nEntity, lua_tointeger(L, 3));
        DataLibrary::Fetch()->PushActiveEntity(nEntity);
    }
    //--Creates and registers a new CarnationCombatEntity as an enemy.
    else if(!strcasecmp(rSwitchType, "Register Enemy Carnation") && tArgs == 3)
    {
        CarnationCombatEntity *nEntity = new CarnationCombatEntity();
        nEntity->SetInternalName(lua_tostring(L, 2));
        rAdventureCombat->RegisterEnemy(lua_tostring(L, 2), nEntity, lua_tointeger(L, 3));
        DataLibrary::Fetch()->PushActiveEntity(nEntity);
    }
    //--Clears all existing enemy aliases.
    else if(!strcasecmp(rSwitchType, "Clear Enemy Aliases") && tArgs == 1)
    {
        AliasStorage *rAliasStorage = rAdventureCombat->GetEnemyAliasStorage();
        rAliasStorage->Wipe();
    }
    //--Creates a new enemy path.
    else if(!strcasecmp(rSwitchType, "Create Enemy Path") && tArgs == 3)
    {
        AliasStorage *rAliasStorage = rAdventureCombat->GetEnemyAliasStorage();
        rAliasStorage->StoreString(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Creates an alias to a path.
    else if(!strcasecmp(rSwitchType, "Create Enemy Alias") && tArgs == 3)
    {
        AliasStorage *rAliasStorage = rAdventureCombat->GetEnemyAliasStorage();
        rAliasStorage->RegisterAliasToString(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Used for the auto-win handler, means the lua script decided the mug results in an auto-win.
    else if(!strcasecmp(rSwitchType, "Trip Auto Win") && tArgs == 1)
    {
        rAdventureCombat->TripAutoWin();
    }
    ///--[Party]
    //--Pushes the named party member on the activity stack, or NULL on error.
    else if(!strcasecmp(rSwitchType, "Push Party Member") && tArgs == 2)
    {
        rAdventureCombat->PushPartyMemberS(lua_tostring(L, 2));
    }
    //--Pushes the party member in the slot.
    else if(!strcasecmp(rSwitchType, "Push Party Member By Slot") && tArgs == 2)
    {
        rAdventureCombat->PushPartyMemberI(lua_tointeger(L, 2));
    }
    //--Pushes the named active party member.
    else if(!strcasecmp(rSwitchType, "Push Active Party Member") && tArgs == 2)
    {
        rAdventureCombat->PushActivePartyMemberS(lua_tostring(L, 2));
    }
    //--Pushes the active party member in the given slot, or NULL on error.
    else if(!strcasecmp(rSwitchType, "Push Active Party Member By Slot") && tArgs == 2)
    {
        rAdventureCombat->PushActivePartyMemberI(lua_tointeger(L, 2));
    }
    //--Pushes the combat party member with the given name, or NULL.
    else if(!strcasecmp(rSwitchType, "Push Combat Party Member") && tArgs == 2)
    {
        rAdventureCombat->PushCombatPartyMemberS(lua_tostring(L, 2));
    }
    //--Pushes the combat party member in the given slot, or NULL on error.
    else if(!strcasecmp(rSwitchType, "Push Combat Party Member By Slot") && tArgs == 2)
    {
        rAdventureCombat->PushCombatPartyMemberI(lua_tointeger(L, 2));
    }
    //--Registers a new party member and pushes them on the activity stack.
    else if(!strcasecmp(rSwitchType, "Register Party Member") && tArgs == 2)
    {
        AdvCombatEntity *nEntity = new AdvCombatEntity();
        nEntity->SetInternalName(lua_tostring(L, 2));
        nEntity->SetDisplayName("DEFAULT");
        rAdventureCombat->RegisterPartyMember(lua_tostring(L, 2), nEntity);
        DataLibrary::Fetch()->PushActiveEntity(nEntity);
    }
    //--As above, but creates a CarnationCombatEntity, which has extra animation properties.
    else if(!strcasecmp(rSwitchType, "Register Party Member Carnation") && tArgs == 2)
    {
        CarnationCombatEntity *nEntity = new CarnationCombatEntity();
        nEntity->SetInternalName(lua_tostring(L, 2));
        nEntity->SetDisplayName("DEFAULT");
        rAdventureCombat->RegisterPartyMember(lua_tostring(L, 2), nEntity);
        DataLibrary::Fetch()->PushActiveEntity(nEntity);
    }
    //--Removes all party members from the current party.
    else if(!strcasecmp(rSwitchType, "Clear Party") && tArgs == 1)
    {
        rAdventureCombat->ClearParty();
    }
    //--Which party member is in which active slot.
    else if(!strcasecmp(rSwitchType, "Party Slot") && tArgs == 3)
    {
        rAdventureCombat->SetPartyBySlot(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Which party member is in which combat slot.
    else if(!strcasecmp(rSwitchType, "Add To Combat Party") && tArgs >= 2)
    {
        rAdventureCombat->AddCombatMember(lua_tostring(L, 2));
    }
    //--Given a combat slot, switches the character in the active party with the given name into that slot. Can be
    //  used during battle.
    else if(!strcasecmp(rSwitchType, "Move Active To Combat Party") && tArgs == 3)
    {
        rAdventureCombat->SwitchCombatPartyWithActivePartyMember(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Heals party to full. Usually used by rest sequences.
    else if(!strcasecmp(rSwitchType, "Restore Party") && tArgs == 1)
    {
        rAdventureCombat->FullRestoreParty();
    }
    //--Heals entire roster to full.
    else if(!strcasecmp(rSwitchType, "Restore Roster") && tArgs == 1)
    {
        rAdventureCombat->FullRestoreRoster();
    }
    //--Moves the entity with the given ID to the combat party.
    else if(!strcasecmp(rSwitchType, "Move Entity To Party") && tArgs == 2)
    {
        rAdventureCombat->MoveEntityToPartyGroup(lua_tointeger(L, 2), AC_PARTY_GROUP_PARTY);
    }
    //--Removes the entity from the active party.
    else if(!strcasecmp(rSwitchType, "Remove Entity From Active Party") && tArgs == 2)
    {
        rAdventureCombat->RemoveActiveMember(lua_tointeger(L, 2));
    }
    //--Removes the entity from the combat party.
    else if(!strcasecmp(rSwitchType, "Remove Entity From Combat Party") && tArgs == 2)
    {
        rAdventureCombat->MoveEntityToPartyGroup(lua_tointeger(L, 2), -1);
    }
    ///--[Level Up Storage]
    //--Clears the job level up pack.
    else if(!strcasecmp(rSwitchType, "Clear Level Up Struct") && tArgs == 1)
    {
        CombatStatistics *rStats = rAdventureCombat->GetJobLevelUpStorage();
        rStats->Zero();
    }
    //--Sets the given value in the job level up pack.
    else if(!strcasecmp(rSwitchType, "Set Level Up Struct Stat") && tArgs == 3)
    {
        CombatStatistics *rStats = rAdventureCombat->GetJobLevelUpStorage();
        rStats->SetStatByIndex(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    ///--[AI Control]
    //--Informs the object that the AI handled the action safely.
    else if(!strcasecmp(rSwitchType, "Mark Handled Action") && tArgs == 1)
    {
        rAdventureCombat->MarkHandledAction();
    }
    //--Sets the ability in the given slot as the active ability.
    else if(!strcasecmp(rSwitchType, "Set Ability As Active") && tArgs == 2)
    {
        rAdventureCombat->SetAbilityAsActiveI(lua_tointeger(L, 2));
    }
    //--As above, but uses the name instead of the slot.
    else if(!strcasecmp(rSwitchType, "Set Ability As Active Name") && tArgs == 2)
    {
        rAdventureCombat->SetAbilityAsActiveS(lua_tostring(L, 2));
    }
    //--Populates target clusters using the active ability's script.
    else if(!strcasecmp(rSwitchType, "Run Active Ability Target Script") && tArgs == 1)
    {
        rAdventureCombat->RunActiveAbilityTargetScript();
    }
    //--Sets the given target cluster as active.
    else if(!strcasecmp(rSwitchType, "Set Target Cluster As Active") && tArgs == 2)
    {
        rAdventureCombat->SetTargetClusterAsActive(lua_tointeger(L, 2));
    }
    //--Runs the active ability on the active target cluster.
    else if(!strcasecmp(rSwitchType, "Run Active Ability On Active Targets") && tArgs == 1)
    {
        rAdventureCombat->ExecuteActiveAbility();
    }
    //--When ADVCOMBAT_RESPONSE_AIKNOCKOUT is running, this can be used to get the entity being KO'd.
    else if(!strcasecmp(rSwitchType, "Push Knockout Entity") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rAdventureCombat->GetKnockoutEntity());
    }
    ///--[Targeting]
    //--Resets the last-used target code to "Custom". Used for some AI scripts.
    else if(!strcasecmp(rSwitchType, "Reset Target Code") && tArgs == 1)
    {
        rAdventureCombat->ResetTargetCode();
    }
    //--Clears all existing target clusters.
    else if(!strcasecmp(rSwitchType, "Clear Target Clusters") && tArgs == 1)
    {
        rAdventureCombat->ClearTargetClusters();
    }
    //--Creates a new target cluster with the given name.
    else if(!strcasecmp(rSwitchType, "Create Target Cluster") && tArgs == 3)
    {
        rAdventureCombat->CreateTargetCluster(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Adds the entity with the matching ID to the named cluster.
    else if(!strcasecmp(rSwitchType, "Add Target To Cluster") && tArgs == 3)
    {
        rAdventureCombat->RegisterEntityToClusterByID(lua_tostring(L, 2), (uint32_t)lua_tointeger(L, 3));
    }
    //--Deletes the target cluster in the given slot.
    else if(!strcasecmp(rSwitchType, "Remove Target Cluster By Index") && tArgs == 2)
    {
        rAdventureCombat->RemoveTargetClusterI(lua_tointeger(L, 2));
    }
    //--Deletes the target cluster with the given name.
    else if(!strcasecmp(rSwitchType, "Remove Target Cluster By Name") && tArgs == 2)
    {
        rAdventureCombat->RemoveTargetClusterS(lua_tostring(L, 2));
    }
    //--Removes the given target from the given target cluster, by ID.
    else if(!strcasecmp(rSwitchType, "Remove Target From Cluster") && tArgs == 3)
    {
        TargetCluster *rTargetCluster = rAdventureCombat->GetTargetClusterI(lua_tointeger(L, 2));
        if(!rTargetCluster)
        {
            DebugManager::ForcePrint("Error, cannot remove target from cluster %i, cluster does not exist.\n", lua_tointeger(L, 2));
        }
        else
        {
            rTargetCluster->RemoveElementID(lua_tointeger(L, 3));
        }
    }
    //--Automatically populates targets using an in-built macro. The ID is which entity this is from the perspective of.
    else if(!strcasecmp(rSwitchType, "Run Target Macro") && tArgs == 3)
    {
        //--Locate the entity with the matching ID.
        AdvCombatEntity *rEntity = rAdventureCombat->GetEntityByID(lua_tointeger(L, 3));

        //--Populate.
        rAdventureCombat->PopulateTargetsByCode(lua_tostring(L, 2), rEntity);
    }
    ///--[Prediction Boxes]
    //--Note: Can only be used during ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX calls.
    //--Pushes the target in the given index.
    else if(!strcasecmp(rSwitchType, "Prediction Push Target") && tArgs == 2)
    {
        DataLibrary::Fetch()->PushActiveEntity();
        TargetCluster *rTargetCluster = rAdventureCombat->GetPredictionTargetCluster();
        if(rTargetCluster && rTargetCluster->mrTargetList) DataLibrary::Fetch()->rActiveObject = rTargetCluster->mrTargetList->GetElementBySlot(lua_tointeger(L, 2));
    }
    //--Creates a new Prediction Box with the given name.
    else if(!strcasecmp(rSwitchType, "Prediction Create Box") && tArgs == 2)
    {
        rAdventureCombat->CreatePredictionBox(lua_tostring(L, 2));
    }
    //--Sets which entity the given Prediction Box is associated with. This is only used for positioning.
    else if(!strcasecmp(rSwitchType, "Prediction Set Host") && tArgs == 3)
    {
        rAdventureCombat->SetPredictionBoxHostI(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the X/Y offset of the given prediction box.
    else if(!strcasecmp(rSwitchType, "Prediction Set Offsets") && tArgs == 4)
    {
        rAdventureCombat->SetPredictionBoxOffsets(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Allocates how many strings the given prediction box needs.
    else if(!strcasecmp(rSwitchType, "Prediction Allocate Strings") && tArgs == 3)
    {
        rAdventureCombat->AllocatePredictionBoxStrings(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the text for the indexed string.
    else if(!strcasecmp(rSwitchType, "Prediction Set String Text") && tArgs == 4)
    {
        rAdventureCombat->SetPredictionBoxStringText(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Sets how many images the given string needs.
    else if(!strcasecmp(rSwitchType, "Prediction Set String Images Total") && tArgs == 4)
    {
        rAdventureCombat->SetPredictionBoxStringImagesTotal(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets the image used in the given slot for the box.
    else if(!strcasecmp(rSwitchType, "Prediction Set String Image") && tArgs == 6)
    {
        rAdventureCombat->SetPredictionBoxStringImage(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tonumber(L, 5), lua_tostring(L, 6));
    }
    ///--[Ability Execution]
    //--Marks whether or not the event in question can still run. Events may be stopped if the calling entity was KO'd by a counterattack
    //  or a DoT.
    else if(!strcasecmp(rSwitchType, "Set Event Can Run") && tArgs == 2)
    {
        rAdventureCombat->MarkEventCanRun(lua_toboolean(L, 2));
    }
    //--Pushes whoever is currently acting. Note that this may not be who is originating the ability that is executing. This is whoever's
    //  turn it currnetly is.
    else if(!strcasecmp(rSwitchType, "Push Acting Entity") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rAdventureCombat->GetActingEntity());
    }
    //--Push the originator of the current CombatEventPack. Used for abilities during ACA_SCRIPT_CODE_EXECUTE.
    else if(!strcasecmp(rSwitchType, "Push Event Originator") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rAdventureCombat->GetEventOriginator());
    }
    //--Pushes the target in the given slot onto the activity stack. Pushes NULL if out of range. Used for abilities during ACA_SCRIPT_CODE_EXECUTE.
    else if(!strcasecmp(rSwitchType, "Push Target") && tArgs == 2)
    {
        //--Push.
        DataLibrary::Fetch()->PushActiveEntity();

        //--Error check:
        CombatEventPack *rFiringPackage = rAdventureCombat->GetFiringEventPack();
        if(!rFiringPackage || !rFiringPackage->mTargetCluster)
        {
            fprintf(stderr, "Warning: %s - No firing package, or no target cluster in package.\n", LuaManager::Fetch()->GetCallStack(0));
        }
        //--Get the entity in the slot.
        else
        {
            AdvCombatEntity *rSlotTarget = (AdvCombatEntity *)rFiringPackage->mTargetCluster->mrTargetList->GetElementBySlot(lua_tointeger(L, 2));
            if(!rSlotTarget)
            {
                fprintf(stderr, "Warning: %s - Target slot %i was out of range.\n", LuaManager::Fetch()->GetCallStack(0), (int)lua_tointeger(L, 2));
            }
            else
            {
                DataLibrary::Fetch()->rActiveObject = rSlotTarget;
            }
        }
    }
    //--How many ticks this event runs before it ends.
    else if(!strcasecmp(rSwitchType, "Event Timer") && tArgs == 2)
    {
        rAdventureCombat->SetEventTimer(lua_tointeger(L, 2));
    }
    //--Registers an ApplicationPack which modifies the target's HP.
    else if(!strcasecmp(rSwitchType, "Register Application Pack") && tArgs == 5)
    {
        ApplicationPack *nPack = (ApplicationPack *)starmemoryalloc(sizeof(ApplicationPack));
        nPack->Initialize();
        nPack->mOriginatorID = lua_tointeger(L, 2);
        nPack->mTargetID = lua_tointeger(L, 3);
        nPack->mTicks = lua_tointeger(L, 4);
        nPack->mEffectString = InitializeString(lua_tostring(L, 5));
        rAdventureCombat->EnqueueApplicationPack(nPack);
    }
    //--If an entity needs to be added to an event after the event has started executing, or is otherwise
    //  not on the target list, this will place it in the field.
    else if(!strcasecmp(rSwitchType, "Stage Entity By ID") && tArgs == 2)
    {
        rAdventureCombat->RestageCharactersWithNewID(lua_tointeger(L, 2));
    }
    //--Indicates the executing ability is a free action.
    else if(!strcasecmp(rSwitchType, "Set As Free Action") && tArgs == 1)
    {
        rAdventureCombat->SetAsFreeAction();
    }
    //--Indicates the executing ability is an effortless action.
    else if(!strcasecmp(rSwitchType, "Set As Effortless Action") && tArgs == 1)
    {
        rAdventureCombat->SetAsEffortlessAction();
    }
    ///--[Animations]
    //--Creates, registers, and pushes a new AdvCombatAnimation.
    else if(!strcasecmp(rSwitchType, "Create Animation") && tArgs == 2)
    {
        AdvCombatAnimation *nAnimation = new AdvCombatAnimation();
        rAdventureCombat->RegisterAnimation(lua_tostring(L, 2), nAnimation);
        DataLibrary::Fetch()->PushActiveEntity(nAnimation);
    }
    //--Creates a new instance of an animation at the given location. The RefName can be used to query it later.
    else if(!strcasecmp(rSwitchType, "Create Animation Instance") && tArgs == 6)
    {
        rAdventureCombat->CreateAnimationInstance(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    ///--[Effects]
    //--Creates a new effect and registers it, pointing at the given target. Pushes the effect
    //  for further setup.
    else if(!strcasecmp(rSwitchType, "Create Effect") && tArgs == 2)
    {
        AdvCombatEffect *nEffect = new AdvCombatEffect();
        rAdventureCombat->RegisterEffect(nEffect);
        nEffect->AddTargetByID(lua_tointeger(L, 2));
        DataLibrary::Fetch()->PushActiveEntity(nEffect);
    }
    //--Pushes the effect with the matching ID.
    else if(!strcasecmp(rSwitchType, "Push Effect") && tArgs == 2)
    {
        rAdventureCombat->PushEffectByID(lua_tointeger(L, 2));
    }
    //--Builds a list of effects referencing the given entity. Returns how many were found.
    else if(!strcasecmp(rSwitchType, "Store Effects Referencing") && tArgs == 2)
    {
        AdvCombatEntity *rEntity = rAdventureCombat->GetEntityByID(lua_tointeger(L, 2));
        if(rEntity)
        {
            rAdventureCombat->StoreEffectsReferencing(lua_tointeger(L, 2));
            //rAdventureCombat->GetEffectsReferencing(rEntity); //Remove if this doesn't actually do anything important
            StarLinkedList *rTempEffectList = rAdventureCombat->GetTemporaryEffectList();
            lua_pushinteger(L, rTempEffectList->GetListSize());
        }
        else
        {
            lua_pushinteger(L, 0);
        }

        return 1;
    }
    //--Pushes the slotted effect from the reference list.
    else if(!strcasecmp(rSwitchType, "Push Temp Effects") && tArgs == 2)
    {
        DataLibrary::Fetch()->PushActiveEntity();
        StarLinkedList *rTempEffectList = rAdventureCombat->GetTemporaryEffectList();
        void *rEffect = rTempEffectList->GetElementBySlot(lua_tointeger(L, 2));
        DataLibrary::Fetch()->rActiveObject = rEffect;
    }
    //--Clears the reference list of effects.
    else if(!strcasecmp(rSwitchType, "Clear Temp Effects") && tArgs == 1)
    {
        rAdventureCombat->ClearTemporaryEffectList();
    }
    //--Removes any effects that have expired.
    else if(!strcasecmp(rSwitchType, "Pulse Effect Removal") && tArgs == 1)
    {
        rAdventureCombat->PulseEffectsForRemoval();
    }
    //--Marks an effect for removal ASAP.
    else if(!strcasecmp(rSwitchType, "Remove Effect") && tArgs == 2)
    {
        rAdventureCombat->MarkEffectForRemoval(lua_tointeger(L, 2));
    }
    //--Remove all effects.
    else if(!strcasecmp(rSwitchType, "Remove All Effects") && tArgs >= 1)
    {
        rAdventureCombat->ClearEffects();
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombat_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
