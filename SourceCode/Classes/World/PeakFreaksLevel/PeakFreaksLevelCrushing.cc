//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "PeakFreaksPlayer.h"
#include "PeakFreaksFragment.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"

///========================================== System ==============================================
void PeakFreaksLevel::AutoCrush()
{
    ///--[Documentation]
    //--Calls each of the crush handlers below until one works. Does nothing if all of them fail.
    int tTotalCrushes = 0;
    bool tAnyCrushes = false;
    while(CrushPuzzleGridUp())
    {
        tTotalCrushes ++;
        tAnyCrushes = true;
    }
    while(CrushPuzzleGridDn())
    {
        tTotalCrushes ++;
        tAnyCrushes = true;
    }
    while(CrushPuzzleGridLf())
    {
        tTotalCrushes ++;
        tAnyCrushes = true;
    }
    while(CrushPuzzleGridRt())
    {
        tTotalCrushes ++;
        tAnyCrushes = true;
    }

    //--SFX.
    if(tAnyCrushes)
    {
        AudioManager::Fetch()->PlaySound("PF Puzzle TileCrush");
    }
    //--No crushes, void sound effect.
    else
    {
        AudioManager::Fetch()->PlaySound("PF Puzzle Void");
    }

    //--Give speed bonus based on the number of crushed tiles.

    //--Add one to the player's speed meter. This only happens at specific intervals.
    if(!mPlayer) return;
    int tCurSpeedBar = mPlayer->GetActualSpeedBarLevel();

    //--1 or none: No bonus.
    if(tTotalCrushes <= 1)
    {

    }
    //--2, small bonus.
    else if(tTotalCrushes <= 2)
    {
        mPlayer->SetSpeedBar(tCurSpeedBar + 1);
    }
    //--3 or 4: +2
    else if(tTotalCrushes <= 4)
    {
        //--Bonus.
        mPlayer->SetSpeedBar(tCurSpeedBar + 1);

        //--Fill tutorial. Crushing 3 or more blocks increment.
        if(mFillTutorialActive)
        {
            mFillCount ++;
            if(mFillCount >= 4)
            {
                mFillTutorialActive = false;
                ActivateTutorialO();
            }
        }
    }
    //--Otherwise, +3.
    else
    {
        //--Bonus.
        mPlayer->SetSpeedBar(tCurSpeedBar + 2);

        //--Fill tutorial. Crushing 3 or more blocks increment.
        if(mFillTutorialActive)
        {
            mFillCount ++;
            if(mFillCount >= 4)
            {
                mFillTutorialActive = false;
                ActivateTutorialO();
            }
        }
    }
}
bool PeakFreaksLevel::CrushPuzzleGridUp()
{
    ///--[Documentation]
    //--Checks if the tile above the cursor matches the cursor. If so, moves the entire column above
    //  the cursor down by one, and increases the power of that tile.

    //--Check if the tiles match.
    int tCurTile = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPuzzleType();
    int tModTile = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y-1]->GetPuzzleType();

    //--Types don't match, fail.
    if(tCurTile != tModTile) return false;

    //--Add one to the crush count.
    //if(mTutorialBActive) mTutorialBCrushes ++;

    ///--[Crush Operation]
    //--Increase power of selected tile.
    mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->SetPower(mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPower() + 2);

    //--Store the "Crushed" tile.
    PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y-1];

    //--Spawn fragments.
    SpawnPuzzleFragments(PLL_PUZZLE_MID_X, PLL_PUZZLE_MID_Y-1);

    //--Move the tiles.
    float cXPos = PLL_PUZZLE_X + (PLL_PUZZLE_MID_X * PLL_PUZZLE_W);
    for(int y = PLL_PUZZLE_MID_Y-1; y >= 1; y --)
    {
        float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);
        mPuzzleGrid[PLL_PUZZLE_MID_X][y] = mPuzzleGrid[PLL_PUZZLE_MID_X][y-1];
        mPuzzleGrid[PLL_PUZZLE_MID_X][y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
    }

    //--Zeroth tile becomes the crushed piece.
    mPuzzleGrid[PLL_PUZZLE_MID_X][0] = rCrushedPiece;

    //--The crushed piece randomizes its type, resets its power, and moves instantly.
    float cYPos = PLL_PUZZLE_Y + (0 * PLL_PUZZLE_H);
    rCrushedPiece->SetPower(0);
    rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
    rCrushedPiece->MoveTo(cXPos, cYPos, 1);

    //--Particles.
    SpawnSpeedParticle(240.0f, 610.0f, 0);
    SpawnSpeedParticle(240.0f, 610.0f, -1);
    SpawnSpeedParticle(240.0f, 610.0f, -2);
    SpawnSpeedParticle(240.0f, 610.0f, -3);

    //--The crushed piece is added to the rebalance list.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);
    tRebalanceList->AddElement("X", rCrushedPiece);

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;

    //--Return true to indicate we successfully crushed.
    return true;
}
bool PeakFreaksLevel::CrushPuzzleGridDn()
{
    ///--[Documentation]
    //--Checks if the tile below the cursor matches the cursor. If so, moves the entire column below
    //  the cursor up by one, and increases the power of that tile.

    //--Check if the tiles match.
    int tCurTile = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPuzzleType();
    int tModTile = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y+1]->GetPuzzleType();

    //--Types don't match, fail.
    if(tCurTile != tModTile) return false;

    //--Add one to the crush count.
    //if(mTutorialBActive) mTutorialBCrushes ++;

    ///--[Crush Operation]
    //--Increase power of selected tile.
    mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->SetPower(mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPower() + 2);

    //--Store the "Crushed" tile.
    PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y+1];

    //--Spawn fragments.
    SpawnPuzzleFragments(PLL_PUZZLE_MID_X, PLL_PUZZLE_MID_Y+1);

    //--Move the tiles.
    float cXPos = PLL_PUZZLE_X + (PLL_PUZZLE_MID_X * PLL_PUZZLE_W);
    for(int y = PLL_PUZZLE_MID_Y+1; y < PLL_PUZZLE_SIZE_Y-1; y ++)
    {
        float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);
        mPuzzleGrid[PLL_PUZZLE_MID_X][y] = mPuzzleGrid[PLL_PUZZLE_MID_X][y+1];
        mPuzzleGrid[PLL_PUZZLE_MID_X][y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
    }

    //--Zeroth tile becomes the crushed piece.
    mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_SIZE_Y-1] = rCrushedPiece;

    //--The crushed piece randomizes its type, resets its power, and moves instantly.
    float cYPos = PLL_PUZZLE_Y + ((PLL_PUZZLE_SIZE_Y-1) * PLL_PUZZLE_H);
    rCrushedPiece->SetPower(0);
    rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
    rCrushedPiece->MoveTo(cXPos, cYPos, 1);

    //--Particles.
    SpawnSpeedParticle(240.0f, 610.0f, 0);
    SpawnSpeedParticle(240.0f, 610.0f, -1);
    SpawnSpeedParticle(240.0f, 610.0f, -2);
    SpawnSpeedParticle(240.0f, 610.0f, -3);

    //--The crushed piece is added to the rebalance list.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);
    tRebalanceList->AddElement("X", rCrushedPiece);

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;

    //--Return true to indicate we successfully crushed.
    return true;
}
bool PeakFreaksLevel::CrushPuzzleGridLf()
{
    ///--[Documentation]
    //--Checks if the tile left of the cursor matches the cursor. If so, moves the entire row left of
    //  the cursor right by one, and increases the power of that tile.

    //--Check if the tiles match.
    int tCurTile = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPuzzleType();
    int tModTile = mPuzzleGrid[PLL_PUZZLE_MID_X-1][PLL_PUZZLE_MID_Y]->GetPuzzleType();

    //--Types don't match, fail.
    if(tCurTile != tModTile) return false;

    //--Add one to the crush count.
    //if(mTutorialBActive) mTutorialBCrushes ++;

    ///--[Crush Operation]
    //--Increase power of selected tile.
    mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->SetPower(mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPower() + 2);

    //--Store the "Crushed" tile.
    PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[PLL_PUZZLE_MID_X-1][PLL_PUZZLE_MID_Y];

    //--Spawn fragments.
    SpawnPuzzleFragments(PLL_PUZZLE_MID_X-1, PLL_PUZZLE_MID_Y);

    //--Move the tiles.
    float cYPos = PLL_PUZZLE_Y + (PLL_PUZZLE_MID_Y * PLL_PUZZLE_H);
    for(int x = PLL_PUZZLE_MID_X-1; x >= 1; x --)
    {
        float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);
        mPuzzleGrid[x][PLL_PUZZLE_MID_Y] = mPuzzleGrid[x-1][PLL_PUZZLE_MID_Y];
        mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
    }

    //--Zeroth tile becomes the crushed piece.
    mPuzzleGrid[0][PLL_PUZZLE_MID_Y] = rCrushedPiece;

    //--Particles.
    SpawnSpeedParticle(240.0f, 610.0f, 0);
    SpawnSpeedParticle(240.0f, 610.0f, -1);
    SpawnSpeedParticle(240.0f, 610.0f, -2);
    SpawnSpeedParticle(240.0f, 610.0f, -3);

    //--The crushed piece randomizes its type, resets its power, and moves instantly.
    float cXPos = PLL_PUZZLE_X + (0 * PLL_PUZZLE_W);
    rCrushedPiece->SetPower(0);
    rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
    rCrushedPiece->MoveTo(cXPos, cYPos, 1);

    //--The crushed piece is added to the rebalance list.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);
    tRebalanceList->AddElement("X", rCrushedPiece);

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;

    //--Return true to indicate we successfully crushed.
    return true;
}
bool PeakFreaksLevel::CrushPuzzleGridRt()
{
    ///--[Documentation]
    //--Checks if the tile right of the cursor matches the cursor. If so, moves the entire row right of
    //  the cursor left by one, and increases the power of that tile.

    //--Check if the tiles match.
    int tCurTile = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPuzzleType();
    int tModTile = mPuzzleGrid[PLL_PUZZLE_MID_X+1][PLL_PUZZLE_MID_Y]->GetPuzzleType();

    //--Types don't match, fail.
    if(tCurTile != tModTile) return false;

    //--Add one to the crush count.
    //if(mTutorialBActive) mTutorialBCrushes ++;

    ///--[Crush Operation]
    //--Increase power of selected tile.
    mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->SetPower(mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPower() + 2);

    //--Store the "Crushed" tile.
    PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[PLL_PUZZLE_MID_X+1][PLL_PUZZLE_MID_Y];

    //--Spawn fragments.
    SpawnPuzzleFragments(PLL_PUZZLE_MID_X+1, PLL_PUZZLE_MID_Y);

    //--Move the tiles.
    float cYPos = PLL_PUZZLE_Y + (PLL_PUZZLE_MID_Y * PLL_PUZZLE_H);
    for(int x = PLL_PUZZLE_MID_X+1; x < PLL_PUZZLE_SIZE_X-1; x ++)
    {
        float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);
        mPuzzleGrid[x][PLL_PUZZLE_MID_Y] = mPuzzleGrid[x+1][PLL_PUZZLE_MID_Y];
        mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
    }

    //--Zeroth tile becomes the crushed piece.
    mPuzzleGrid[PLL_PUZZLE_SIZE_X-1][PLL_PUZZLE_MID_Y] = rCrushedPiece;

    //--The crushed piece randomizes its type, resets its power, and moves instantly.
    float cXPos = PLL_PUZZLE_X + ((PLL_PUZZLE_SIZE_X-1) * PLL_PUZZLE_W);
    rCrushedPiece->SetPower(0);
    rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
    rCrushedPiece->MoveTo(cXPos, cYPos, 1);

    //--Particles.
    SpawnSpeedParticle(240.0f, 610.0f, 0);
    SpawnSpeedParticle(240.0f, 610.0f, -1);
    SpawnSpeedParticle(240.0f, 610.0f, -2);
    SpawnSpeedParticle(240.0f, 610.0f, -3);

    //--The crushed piece is added to the rebalance list.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);
    tRebalanceList->AddElement("X", rCrushedPiece);

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;

    //--Return true to indicate we successfully crushed.
    return true;
}

///===================================== Property Queries =========================================
//--These are versions of crushing where an entire row/column moves updown/leftright at once.

/*
void PeakFreaksLevel::CrushPuzzleGridUp()
{
    ///--[Documentation]
    //--Causes the rows of tiles right below the cursor row to all move up. This "crushes" tiles
    //  in the cursor row and can increase their power.
    //--The bottom row is replaced by random tiles.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);

    //--Iterate.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        //--Compute X position.
        float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);

        //--The row below the cursor is treated differently as this row gets empowered if it
        //  crushes a matching tile.
        if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == mPuzzleGrid[x][PLL_PUZZLE_MID_Y+1]->GetPuzzleType())
        {
            mPuzzleGrid[x][PLL_PUZZLE_MID_Y+1]->SetPower(mPuzzleGrid[x][PLL_PUZZLE_MID_Y+1]->GetPower() + 1);
        }

        //--Store the "Crushed" tile.
        PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[x][PLL_PUZZLE_MID_Y];

        //--Move the tiles.
        for(int y = PLL_PUZZLE_MID_Y; y < PLL_PUZZLE_SIZE_Y-1; y ++)
        {
            float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);
            mPuzzleGrid[x][y] = mPuzzleGrid[x][y+1];
            mPuzzleGrid[x][y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
        }

        //--The bottom row becomes the crushed piece.
        mPuzzleGrid[x][PLL_PUZZLE_SIZE_Y-1] = rCrushedPiece;

        //--The crushed piece randomizes its type, resets its power, and moves instantly.
        float cYPos = PLL_PUZZLE_Y + ((PLL_PUZZLE_SIZE_Y-1) * PLL_PUZZLE_H);
        rCrushedPiece->SetPower(0);
        rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
        rCrushedPiece->MoveTo(cXPos, cYPos, 1);

        //--The crushed piece is added to the rebalance list.
        tRebalanceList->AddElement("X", rCrushedPiece);
    }

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;
}
void PeakFreaksLevel::CrushPuzzleGridDn()
{
    ///--[Documentation]
    //--Causes the rows of tiles right above the cursor row to all move down. This "crushes" tiles
    //  in the cursor row and can increase their power.
    //--The top row is replaced by random tiles.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);

    //--Iterate.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        //--Compute X position.
        float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);

        //--The row above the cursor is treated differently as this row gets empowered if it
        //  crushes a matching tile.
        if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == mPuzzleGrid[x][PLL_PUZZLE_MID_Y-1]->GetPuzzleType())
        {
            mPuzzleGrid[x][PLL_PUZZLE_MID_Y-1]->SetPower(mPuzzleGrid[x][PLL_PUZZLE_MID_Y-1]->GetPower() + 1);
        }

        //--Store the "Crushed" tile.
        PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[x][PLL_PUZZLE_MID_Y];

        //--Move the tiles.
        for(int y = PLL_PUZZLE_MID_Y; y >= 1; y --)
        {
            float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);
            mPuzzleGrid[x][y] = mPuzzleGrid[x][y-1];
            mPuzzleGrid[x][y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
        }

        //--The top row becomes the crushed piece.
        mPuzzleGrid[x][0] = rCrushedPiece;

        //--The crushed piece randomizes its type, resets its power, and moves instantly.
        float cYPos = PLL_PUZZLE_Y + (0 * PLL_PUZZLE_H);
        rCrushedPiece->SetPower(0);
        rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
        rCrushedPiece->MoveTo(cXPos, cYPos, 1);

        //--The crushed piece is added to the rebalance list.
        tRebalanceList->AddElement("X", rCrushedPiece);
    }

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;
}
void PeakFreaksLevel::CrushPuzzleGridLf()
{
    ///--[Documentation]
    //--Causes the column of tiles right of the cursor row to all move left. This "crushes" tiles
    //  in the cursor column and can increase their power.
    //--The rightmost column is replaced by random tiles.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);

    //--Iterate.
    for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
    {
        //--Compute Y position.
        float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);

        //--The column right of the cursor is treated differently as this column gets empowered if it
        //  crushes a matching tile.
        if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == mPuzzleGrid[PLL_PUZZLE_MID_X+1][y]->GetPuzzleType())
        {
            mPuzzleGrid[PLL_PUZZLE_MID_X+1][y]->SetPower(mPuzzleGrid[PLL_PUZZLE_MID_X+1][y]->GetPower() + 1);
        }

        //--Store the "Crushed" tile.
        PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[PLL_PUZZLE_MID_X][y];

        //--Move the tiles.
        for(int x = PLL_PUZZLE_MID_X; x < PLL_PUZZLE_SIZE_X-1; x ++)
        {
            float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);
            mPuzzleGrid[x][y] = mPuzzleGrid[x+1][y];
            mPuzzleGrid[x][y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
        }

        //--The rightmost column becomes the crushed piece.
        mPuzzleGrid[PLL_PUZZLE_SIZE_X-1][y] = rCrushedPiece;

        //--The crushed piece randomizes its type, resets its power, and moves instantly.
        float cXPos = PLL_PUZZLE_X + ((PLL_PUZZLE_SIZE_X-1) * PLL_PUZZLE_W);
        rCrushedPiece->SetPower(0);
        rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
        rCrushedPiece->MoveTo(cXPos, cYPos, 1);

        //--The crushed piece is added to the rebalance list.
        tRebalanceList->AddElement("X", rCrushedPiece);
    }

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;
}
void PeakFreaksLevel::CrushPuzzleGridRt()
{
    ///--[Documentation]
    //--Causes the column of tiles left of the cursor row to all move right. This "crushes" tiles
    //  in the cursor column and can increase their power.
    //--The leftmost column is replaced by random tiles.
    SugarLinkedList *tRebalanceList = new SugarLinkedList(false);

    //--Iterate.
    for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
    {
        //--Compute Y position.
        float cYPos = PLL_PUZZLE_Y + (y * PLL_PUZZLE_H);

        //--The column left of the cursor is treated differently as this column gets empowered if it
        //  crushes a matching tile.
        if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == mPuzzleGrid[PLL_PUZZLE_MID_X-1][y]->GetPuzzleType())
        {
            mPuzzleGrid[PLL_PUZZLE_MID_X-1][y]->SetPower(mPuzzleGrid[PLL_PUZZLE_MID_X-1][y]->GetPower() + 1);
        }

        //--Store the "Crushed" tile.
        PeakFreaksPuzzlePiece *rCrushedPiece = mPuzzleGrid[PLL_PUZZLE_MID_X][y];

        //--Move the tiles.
        for(int x = PLL_PUZZLE_MID_X; x >= 1; x --)
        {
            float cXPos = PLL_PUZZLE_X + (x * PLL_PUZZLE_W);
            mPuzzleGrid[x][y] = mPuzzleGrid[x-1][y];
            mPuzzleGrid[x][y]->MoveTo(cXPos, cYPos, PLL_PUZZLE_MOVE_TICKS);
        }

        //--The leftmost column becomes the crushed piece.
        mPuzzleGrid[0][y] = rCrushedPiece;

        //--The crushed piece randomizes its type, resets its power, and moves instantly.
        float cXPos = PLL_PUZZLE_X + (0 * PLL_PUZZLE_W);
        rCrushedPiece->SetPower(0);
        rCrushedPiece->SetPuzzleType(rand() % PEAKFREAK_PUZZLE_TYPE_MAX);
        rCrushedPiece->MoveTo(cXPos, cYPos, 1);

        //--The crushed piece is added to the rebalance list.
        tRebalanceList->AddElement("X", rCrushedPiece);
    }

    //--Run the rebalance list to adjust the new piece composition to match board minimums.
    RebalanceList(tRebalanceList);
    delete tRebalanceList;
}*/

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================

void PeakFreaksLevel::SpawnPuzzleFragments(int pGridX, int pGridY)
{
    ///--[Documentation]
    //--Creates a set of fragments based on the grid position. Used to give the UI some polish.
    if(pGridX < 0 || pGridY < 0) return;
    if(pGridX >= PLL_PUZZLE_SIZE_X || pGridY >= PLL_PUZZLE_SIZE_Y) return;

    ///--[Resolve Data]
    //--Get where on the screen the center of the selected block is. The block may be under
    //  a move order so we need to retrieve the block's current position.
    EasingPack2D tEasingPack = mPuzzleGrid[pGridX][pGridY]->GetPosition();
    float cBlockX = tEasingPack.mXCur + (PLL_PUZZLE_W * 0.50f);
    float cBlockY = tEasingPack.mYCur + (PLL_PUZZLE_H * 0.50f);

    //--Get the associated image.
    int tType = mPuzzleGrid[pGridX][pGridY]->GetPuzzleType();
    if(tType < 0 || tType >= PEAKFREAK_PUZZLE_TYPE_MAX) return;
    SugarBitmap *rUseFragmentImg = Images.Data.mPuzzleFragments[tType];

    ///--[Spawn Fragments]
    //--Constants.
    int cLifetime = 30;
    float cPosOff = 4.0f;
    float cPosSct = 2.0f;
    float cSpdOff = 1.0f;
    float cSpdSct = 0.25f;

    //--NW Fragment
    PeakFreaksFragment *nNWFragment = new PeakFreaksFragment();
    nNWFragment->SetLifetime(cLifetime);
    nNWFragment->SetImage(rUseFragmentImg);
    nNWFragment->SetPosition(cBlockX - ScatterWithinRange(cPosOff, cPosSct), cBlockY - ScatterWithinRange(cPosOff, cPosSct));
    nNWFragment->SetSpeeds(ScatterWithinRange(-cSpdOff, cSpdSct), ScatterWithinRange(-cSpdOff, cSpdSct));
    mFragmentList->AddElement("X", nNWFragment, PeakFreaksFragment::DeleteThis);

    //--NE Fragment.
    PeakFreaksFragment *nNEFragment = new PeakFreaksFragment();
    nNEFragment->SetLifetime(cLifetime);
    nNEFragment->SetImage(rUseFragmentImg);
    nNEFragment->SetPosition(cBlockX + ScatterWithinRange(cPosOff, cPosSct), cBlockY - ScatterWithinRange(cPosOff, cPosSct));
    nNEFragment->SetSpeeds(ScatterWithinRange(cSpdOff, cSpdSct), ScatterWithinRange(-cSpdOff, cSpdSct));
    mFragmentList->AddElement("X", nNEFragment, PeakFreaksFragment::DeleteThis);

    //--SE Fragment.
    PeakFreaksFragment *nSEFragment = new PeakFreaksFragment();
    nSEFragment->SetLifetime(cLifetime);
    nSEFragment->SetImage(rUseFragmentImg);
    nSEFragment->SetPosition(cBlockX + ScatterWithinRange(cPosOff, cPosSct), cBlockY + ScatterWithinRange(cPosOff, cPosSct));
    nSEFragment->SetSpeeds(ScatterWithinRange(cSpdOff, cSpdSct), ScatterWithinRange(cSpdOff, cSpdSct));
    mFragmentList->AddElement("X", nSEFragment, PeakFreaksFragment::DeleteThis);

    //--SW Fragment.
    PeakFreaksFragment *nSWFragment = new PeakFreaksFragment();
    nSWFragment->SetLifetime(cLifetime);
    nSWFragment->SetImage(rUseFragmentImg);
    nSWFragment->SetPosition(cBlockX - ScatterWithinRange(cPosOff, cPosSct), cBlockY + ScatterWithinRange(cPosOff, cPosSct));
    nSWFragment->SetSpeeds(ScatterWithinRange(-cSpdOff, cSpdSct), ScatterWithinRange(cSpdOff, cSpdSct));
    mFragmentList->AddElement("X", nSWFragment, PeakFreaksFragment::DeleteThis);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
