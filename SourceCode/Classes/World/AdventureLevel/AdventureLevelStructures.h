///================================ Adventure Level Structures ====================================
//--There are a lot of substructures used by AdventureLevels, so they got their own file.

#pragma once
#include "StarBitmap.h"

///--[Forward Declarations]
class StarlightString;

///====================================== Tiled Objects ===========================================
//--Objects which typically are created in Tiled and read into the map from a datafile.

///--[ClimbablePackage]
//--Can be either a rope or a ladder.
typedef struct ClimbablePackage
{
    //--Position. Refers to the top of the zone.
    int mX;
    int mY;
    int mW;
    int mH;

    //--State
    bool mIsLadder; //false == rope.
    bool mIsActivated;
    bool mIsAlwaysActivated;

    //--Depths
    int mLowerDepth;
    int mUpperDepth;

    //--Methods
    void Initialize();
}ClimbablePackage;

///--[DoorPackage]
//--Exactly what it sounds like.
typedef struct DoorPackage
{
    //--Position
    int mX;
    int mY;
    int mZ;
    bool mIsWide;

    //--State
    bool mIsOpened;
    bool mIsScriptLocked;
    char *mExamineScript;
    char *mLevelTransitOnOpen;
    char *mLevelTransitExit;
    bool mUseSpaceSound;
    bool mIsSpaceEW;

    //--Contents. Used for chests only.
    bool mCanRespawn;
    char *mContents;
    char *mKeyword;
    char *mRandomScript;

    //--Rendering
    StarBitmap *rRenderOpen;
    StarBitmap *rRenderClosed;

    //--Render Overrides
    float mRenderHi;
    float mRenderLo;

    //--Methods
    void Initialize();
    static void DeleteThis(void *pPtr);
}DoorPackage;

///--[ExaminePackage]
//--Calls a script with a string instruction to distinguish it.
#define EXAMINE_NOT_FROM_NORTH 0x01
#define EXAMINE_RESPOND_GUNSHOT 0x02
typedef struct
{
    //--Position
    int mX;
    int mY;
    int mW;
    int mH;

    //--Examination Flags
    uint8_t mFlags;

    //--Automated Transition
    bool mIsAutoTransition;
    char mAutoRoomDest[STD_MAX_LETTERS];
    char mAutoLocationDest[STD_MAX_LETTERS];
    char mAutoSound[STD_MAX_LETTERS];

    //--Methods
    void Initialize();
}ExaminePackage;

///--[ExitPackage]
//--Travels to a new map.
typedef struct
{
    //--Position
    int mX;
    int mY;
    int mW;
    int mH;
    int mDepth;

    //--Destination Info
    char mMapDestination[STD_MAX_LETTERS];
    char mExitDestination[STD_MAX_LETTERS];

    //--Rendering
    bool mIsStaircase;
    StarBitmap *rRenderImg;

    //--Methods
    void Initialize();
}ExitPackage;

///--[InflectionPackage]
//--Causes entities to move between depths.
typedef struct
{
    //--Position
    int mX;
    int mY;
    int mW;
    int mH;

    //--Depths
    int mLowerDepth;
    int mUpperDepth;

    //--Methods
    void Initialize();
}InflectionPackage;

///--[TriggerPackage]
//--These fire when the player steps in them. A script is called with the trigger's name.
typedef struct
{
    //--System
    bool mSelfDestruct;

    //--Position
    TwoDimensionReal mDimensions;

    //--Firing Properties
    char mFiringName[STD_MAX_LETTERS];

    //--Methods
    void Initialize();
}TriggerPackage;

///--[ ======== Spawn Package ======= ]
//--Used to spawn enemy NPCs.
#define PARAGON_GROUP_LETTERS 80
#define ITEM_KEYWORD_LETTERS 80
#define PATROLPATH_MAX_LETTERS 2048
#ifndef TILEMAP_ACTOR_DETECTION
#define TILEMAP_ACTOR_DETECTION
#define TA_DEFAULT_DETECT_RANGE 80
#define TA_DEFAULT_DETECT_ANGLE 120
#endif
typedef struct
{
    ///--[Variables]
    //--[System]
    char mRawName[STD_MAX_LETTERS];              //Name of the entity from Tiled.
    char mUniqueSpawnName[STD_MAX_LETTERS];      //Unique name used for respawn checking.
    TwoDimensionReal mDimensions;                //Spawn position.
    bool mDontSpawnIfDead;                       //If the entity is dead, don't spawn it at all. If false, enemies can leave corpses.

    //--[Chance To Spawn]
    bool mUseSeededSpawnChance;                  //If true, this entity spawns based on a seed value. Seeds reset during rests.
    int mSeedSpawnChance;                        //Chance this entity will spawn.

    //--[Combat Properties]
    bool mNeverTriggerBattles;                   //Usually used for debug, if true the entity doesn't trigger battles.
    char mPartyEnemy[STD_MAX_LETTERS];           //Combat enemy name when this enemy is added to combat.
    char mWinCutscene[STD_MAX_LETTERS];          //Alias of cutscene to play when the player wins.
    char mLoseCutscene[STD_MAX_LETTERS];         //Alias of cutscene to play when the player loses.

    //--[Appearance]
    char mAppearancePath[STD_MAX_LETTERS];       //Sprite name.
    int mToughness;                              //Gives the enemy an outline to visually indicate difficulty.

    //--[Field AI Properties]
    bool mUseAutoPatrol;                         //If true, uses the enemy's name to resolve which nodes it patrols to or if it follows a leader.
    int mDetectRange;                            //Range, in pixels, of the entity's viewcone. Default is 80px (5 tiles). (TA_DEFAULT_DETECT_RANGE)
    int mDetectAngle;                            //Angle, in degrees, of the entity's viewcone. Default is 120deg. (TA_DEFAULT_DETECT_ANGLE)
    char mIgnoreFlag[STD_MAX_LETTERS];           //List of player forms to ignore. Delimited by "|".
    int mDepth;                                  //Collision depth the entity spawns using.
    char mFollowTarget[STD_MAX_LETTERS];         //Name of another enemy to follow behind.
    char mPatrolPathway[PATROLPATH_MAX_LETTERS]; //List of positions to patrol between. Delimited by "|'.
    bool mAdvancedPatrol;                        //If true, uses a per-pixel path algorithm instead of straight lines.
    bool mSmartPatrol;                           //If true, uses a per-tile path algorithm instead of straight lines.
    bool mPatrolToAnyNode;                       //If true, the entity can path to any patrol node instead of the next one.
    int mLockFacing;                             //Enemy will always face the provided direction.
    char mCommandsEntities[STD_MAX_LETTERS];     //All names on this list are "commanded" by this entity. If it spots the player, all commanded
                                                 //entities will chase the player.

    //--[Flags]
    bool mIsBlind;                               //If true, the entity never runs detect calls. Bumping into them still triggers battles.
    bool mIsImmobile;                            //If true, entity doesn't move for any reason.
    bool mIsFast;                                //Enemy chases at increased speed when chasing player.
    bool mIsRelentless;                          //Enemy chases for farther/longer when chasing the player.
    bool mNeverPauses;                           //Enemy does not stop and look around when wandering/patrolling.
    char mParagonGroup[PARAGON_GROUP_LETTERS];   //Name of paragon grouping to increment when this enemy is defeated.
    bool mDontRefaceFlag;                        //If true, enemy doesn't stop to reface to the next patrol node and moves immediately.
    char mActivateKeyword[ITEM_KEYWORD_LETTERS]; //Used for item nodes, this is passed to the activation script to determine the item gained.

    //--[Mugging Flags]
    bool mBlockAutoWin;                          //Enemy cannot be mugged and instantly defeated.
    bool mCannotBeMugged;                        //Enemy cannot be mugged at all.
    bool mMuggingNeverDropsItems;                //If true, mugging won't drop items.

    ///--[Methods]
    void Initialize()
    {
        //--[System]
        mRawName[0] = '\0';
        mUniqueSpawnName[0] = '\0';
        mDimensions.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
        mDontSpawnIfDead = false;

        //--[Chance To Spawn]
        mUseSeededSpawnChance = false;
        mSeedSpawnChance = 100;

        //--[Combat Properties]
        mNeverTriggerBattles = false;
        mPartyEnemy[0] = '\0';
        mWinCutscene[0] = '\0';
        mLoseCutscene[0] = '\0';

        //--[Appearance]
        mAppearancePath[0] = '\0';
        mToughness = 0;

        //--[Field AI Properties]
        mUseAutoPatrol = false;
        mDetectRange = TA_DEFAULT_DETECT_RANGE;
        mDetectAngle = TA_DEFAULT_DETECT_ANGLE;
        mIgnoreFlag[0] = '\0';
        mDepth = 0;
        mFollowTarget[0] = '\0';
        mPatrolPathway[0] = '\0';
        mAdvancedPatrol = false;
        mSmartPatrol = false;
        mPatrolToAnyNode = false;
        mLockFacing = -1;
        mCommandsEntities[0] = '\0';

        //--[Flags]
        mIsBlind = false;
        mIsImmobile = false;
        mIsFast = false;
        mIsRelentless = false;
        mNeverPauses = false;
        strcpy(mParagonGroup, "Null");
        mDontRefaceFlag = false;
        strcpy(mActivateKeyword, "Null");

        //--[Mugging Flags]
        mBlockAutoWin = false;
        mCannotBeMugged = false;
        mMuggingNeverDropsItems = false;
    }
}SpawnPackage;

///--[Save Point]
//--Lets you do a variety of lovely things, saving being the least interesting.
typedef struct
{
    //--System
    bool mIsLit;
    bool mDoesNotRenderLit;
    int mTimer;

    //--Position
    int mX;
    int mY;

    //--Properties
    bool mIsSpace;
    bool mIsBench;

    //--Images
    StarBitmap *rUnlitImg;
    StarBitmap *rLitImgA;
    StarBitmap *rLitImgB;

    //--Methods
    void Initialize()
    {
        mIsLit = false;
        mDoesNotRenderLit = false;
        mTimer = 0;
        mIsSpace = false;
        mIsBench = false;
        rUnlitImg = NULL;
        rLitImgA = NULL;
        rLitImgB = NULL;
    }
}SavePointPackage;

///--[Position]
//--Used to spawn things, can store useful data. Typically used for NPCs.
typedef struct
{
    //--System
    char mSpawnedActor[128];
    int mX;
    int mY;
    int mZ;
    int mFacing;
    bool mIsEightDirectional;
    bool mIsTwoDirectional;
    bool mLoadRunFrames;

    //--NPC Properties
    char mDialoguePath[STD_PATH_LEN];
    char mSpritePath[64];
    bool mIsClipped;
    bool mAutoWander;
    bool mAutofireDialogue;
    int mExtendedActivate;
    bool mAutoAnimate;
    bool mOscillates;
    float mOverrideDepth;
    int mToughness;

    //--Methods
    void Initialize()
    {
        //--System
        mSpawnedActor[0] = '\0';
        mX = 0;
        mY = 0;
        mZ = 0;
        mFacing = DIR_DOWN;
        mIsEightDirectional = false;
        mIsTwoDirectional = false;
        mLoadRunFrames = false;

        //--NPC Properties
        mDialoguePath[0] = '\0';
        mSpritePath[0] = '\0';
        mIsClipped = true;
        mAutoWander = false;
        mAutofireDialogue = false;
        mExtendedActivate = -1;
        mAutoAnimate = false;
        mOscillates = false;
        mOverrideDepth = -2.0f;
        mToughness = 0;
    }
}PositionPackage;

///--[Overlays]
//--This represents a foreground layer.
typedef struct
{
    //--Image.
    StarBitmap *rImage;

    //--Scale Factor
    float mScaleFactor;

    //--Autoscroll
    float mAutoscrollX;
    float mAutoscrollY;

    //--Alpha.
    int mAlphaTimer;
    int mAlphaTimerMax;
    float mAlphaCurrent;
    float mAlphaStart;
    float mAlphaEnd;

    //--Offset Computation.
    float mOffsetBaseX;
    float mOffsetBaseY;
    float mScrollFactorX;
    float mScrollFactorY;

    //--Methods
    void Initialize()
    {
        //--Image.
        rImage = NULL;

        //--Scale Factor
        mScaleFactor = 1.0f;

        //--Autoscroll
        mAutoscrollX = 0.0f;
        mAutoscrollY = 0.0f;

        //--Alpha.
        mAlphaTimer = 100;
        mAlphaTimerMax = 100;
        mAlphaCurrent = 0.0f;
        mAlphaStart = 0.0f;
        mAlphaEnd = 0.0f;

        //--Offset Computation.
        mOffsetBaseX = 0.0f;
        mOffsetBaseY = 0.0f;
        mScrollFactorX = 0.0f;
        mScrollFactorY = 0.0f;
    }
}OverlayPackage;

///--[AuraParticle]
#define AURAPARTICLE_LIFETIME 600
#define AURAPARTICLE_SIZE_PERIOD 180
typedef struct AuraParticle
{
    //--Lifetime
    int mLifetime;

    //--Display
    float mXPos;
    float mYPos;
    float mScale;
    StarBitmap *rImage;

    //--Movement
    float mXSpeed;
    float mYSpeed;

    //--Sizing
    int mSizeTimer;
    float mSizeLo;
    float mSizeHi;

    //--Methods
    void Initialize();
    void Update();
    void Render();
}AuraParticle;

///--[Dislocated Render Package]
//--Causes a section of the level to render in a different location. Used for special effects.
typedef struct
{
    //--Variables.
    char mLayerName[STD_MAX_LETTERS];
    int mOrigXStart;
    int mOrigYStart;
    int mWidth;
    int mHeight;
    float mNewXRender;
    float mNewYRender;

    //--Functions
    void Initialize()
    {
        mLayerName[0] = '\0';
        mOrigXStart = 0;
        mOrigYStart = 0;
        mWidth = 1;
        mHeight = 1;
        mNewXRender = 0.0f;
        mNewYRender = 0.0f;
    }
}DislocatedRenderPackage;

///--[Per-map Animation]
//--Per-map Animation. Uses a tileset to create animations, usually of characters. This means that animations
//  only used in one area don't clog up the main loading sequence and get dropped when the map is out of scope.
//--Images will be tilesets, usually one long filmstrip.
#define PMA_LOOP_NONE 0
#define PMA_LOOP_SINUSOIDAL 1
#define PMA_LOOP_LINEAR 2
#define PMA_LOOP_REVERSE 3
typedef struct PerMapAnimation
{
    //--Variables.
    bool mIsRendering;
    float mRenderDepth;
    float mRenderPosX;
    float mRenderPosY;
    float mTimer;
    float mTicksPerFrame;
    float mDestinationFrame;
    int mTotalFrames;
    StarBitmap **mrImagePtrs;
    float mSizePerFrameX;
    float mTickRate;

    //--Simple loop handler.
    bool mRebootLoopTimer;
    int8_t mLoopType;
    float mLowFrame;
    float mHiFrame;
    float mInternalLoopTimer;
    float mTicksPerWholeLoop;

    //--Functions
    void Initialize()
    {
        mIsRendering = false;
        mRenderDepth = 0.0f;
        mRenderPosX = 0.0f;
        mRenderPosY = 0.0f;
        mTimer = 0;
        mTicksPerFrame = 5;
        mDestinationFrame = 0;
        mTotalFrames = 0;
        mrImagePtrs = NULL;
        mSizePerFrameX = 10.0f;
        mTickRate = 1.0f;
        mRebootLoopTimer = false;
        mLoopType = PMA_LOOP_NONE;
        mLowFrame = 0.0f;
        mHiFrame = 0.0f;
        mInternalLoopTimer = 0.0f;
        mTicksPerWholeLoop = 10.0f;
    }
    void Allocate(int pTotalFrames)
    {
        mTotalFrames = 0;
        free(mrImagePtrs);
        mrImagePtrs = NULL;
        if(pTotalFrames < 1) return;

        mTotalFrames = pTotalFrames;
        SetMemoryData(__FILE__, __LINE__);
        mrImagePtrs = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mTotalFrames);
        memset(mrImagePtrs, 0, sizeof(StarBitmap *) * mTotalFrames);
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        PerMapAnimation *rPtr = (PerMapAnimation *)pPtr;
        free(rPtr->mrImagePtrs);
        free(rPtr);
    }

    //--Ticks towards the requested value. Returns true if the target was reached, false otherwise.
    bool TickTowardsValue(float pTarget, float pTickRate)
    {
        if(mTimer < pTarget - pTickRate)
        {
            mTimer = mTimer + pTickRate;
            return false;
        }
        else if(mTimer > pTarget + pTickRate)
        {
            mTimer = mTimer - pTickRate;
            return false;
        }
        else
        {
            mTimer = pTarget;
            return true;
        }
    }

    //--Logic tick for the animation.
    void Tick()
    {
        //--Normal ticking when not looping.
        if(mLoopType == PMA_LOOP_NONE)
        {
            TickTowardsValue(mDestinationFrame * mTicksPerFrame, mTickRate);
        }
        //--When doing a sinusoidal loop:
        else if(mLoopType == PMA_LOOP_SINUSOIDAL)
        {
            //--If we haven't booted the loop yet, run to the middle target.
            if(mRebootLoopTimer)
            {
                float tMidframe = (mLowFrame + mHiFrame) / 2.0f;
                bool tHitResult = TickTowardsValue(tMidframe * mTicksPerFrame, mTickRate);
                if(tHitResult) mRebootLoopTimer = false;
                mInternalLoopTimer = 0.0f;
            }
            //--Loop is booted. Start running the internal timer.
            else
            {
                //--Timer handling.
                mInternalLoopTimer = mInternalLoopTimer + 1.0f;

                //--Computations.
                float tMidframe = (mLowFrame + mHiFrame) / 2.0f;
                float tAmplitude = mHiFrame - tMidframe;
                float tTargetFrame = tMidframe + (sinf(mInternalLoopTimer * 3.1415926f / mTicksPerWholeLoop) * tAmplitude);

                //--Override the timer.
                mTimer = tTargetFrame * mTicksPerFrame;
            }
        }
        //--When doing a linear loop:
        else if(mLoopType == PMA_LOOP_LINEAR)
        {
            //--If we haven't booted the loop yet, run to the end target.
            if(mRebootLoopTimer)
            {
                bool tHitResult = TickTowardsValue(mHiFrame * mTicksPerFrame, mTickRate);
                if(tHitResult) mRebootLoopTimer = false;
                mInternalLoopTimer = 0.0f;
            }
            //--Loop is booted. Start running the internal timer.
            else
            {
                //--Timer handling.
                mInternalLoopTimer = mInternalLoopTimer + 1.0f;

                //--Computations.
                float tPercentComplete = (float)fmod((mInternalLoopTimer / mTicksPerWholeLoop), 1.0f);
                float tFramesFromZero = (mHiFrame - mLowFrame) * tPercentComplete;
                float tSetFrame = mLowFrame + tFramesFromZero;

                //--Override the timer.
                mTimer = tSetFrame * mTicksPerFrame;
            }
        }
        //--When doing a reverse loop:
        else if(mLoopType == PMA_LOOP_REVERSE)
        {
            //--If we haven't booted the loop yet, run to the start target.
            if(mRebootLoopTimer)
            {
                bool tHitResult = TickTowardsValue(mLowFrame * mTicksPerFrame, mTickRate);
                if(tHitResult) mRebootLoopTimer = false;
                mInternalLoopTimer = 0.0f;
            }
            //--Loop is booted. Start running the internal timer.
            else
            {
                //--Timer handling.
                mInternalLoopTimer = mInternalLoopTimer + 1.0f;

                //--Computations.
                float tPercentComplete = (float)fmod((mInternalLoopTimer / mTicksPerWholeLoop), 2.0f);
                if(tPercentComplete > 1.0f) tPercentComplete = 2.0f - tPercentComplete;
                float tFramesFromZero = (mHiFrame - mLowFrame) * tPercentComplete;
                float tSetFrame = mLowFrame + tFramesFromZero;

                //--Override the timer.
                mTimer = tSetFrame * mTicksPerFrame;
            }
        }
    }
}PerMapAnimation;

///--[Invisible Zone]
//--When the player is in one of these, enemy AI can't see them.
typedef struct
{
    //--Position. Top-left of the zone and Width/Height. Units are in tiles.
    TwoDimensionReal mDimensions;

    //--State. Can be deactivated by script commands.
    bool mIsActivated;

    //--Methods
    void Initialize()
    {
        //--Position
        mDimensions.Set(0.0f, 0.0f, 1.0f, 1.0f);

        //--State
        mIsActivated = true;
    }

}InvisibleZone;

///===================================== Local Structures =========================================
///--[FallingRockPack]
typedef struct
{
    float mX, mY;
    float mYSpeed;
    float mTheta, mThetaDelta;
    StarBitmap *rImage;
    void Initialize()
    {
        mX = 0.0f;
        mY = 0.0f;
        mYSpeed = 0.0f;
        mTheta = 0.0f;
        mThetaDelta = 0.0f;
        rImage = NULL;
    }
}FallingRockPack;

///--[FieldAbilityPack]
//--Field Ability Pack, represents a field ability that is currently executing. Can have
//  many concurrent packs for the same ability.
typedef struct
{
    //--Variables
    FieldAbility *rParentAbility;
    bool mIsLockingControls;
    int mTimer;
    bool mIsExpired;

    //--Functions
    void Initialize()
    {
        rParentAbility = NULL;
        mIsLockingControls = false;
        mTimer = 0;
        mIsExpired = false;
    }
}FieldAbilityPack;

///--[FieldAbilityObjectPack]
//--Object Pack. Used for Field Abilities, stores a name and type for objects that intersect with
//  a given position.
typedef struct
{
    //--Variables
    int mType;
    char mName[128];

    //--Functions
    void Initialize()
    {
        mType = POINTER_TYPE_FAIL;
        mName[0] = '\0';
    }
}FieldAbilityObjectPack;

///--[PatrolNodePack]
//--Represents a patrol node. Contains a location, and can optionally have extra data.
typedef struct PatrolNodePack
{
    TwoDimensionReal mDimensions;
    int mLingerFacing;
    int mLingerTicks;
    void Initialize()
    {
        mDimensions.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
        mLingerFacing = DIR_NORTH;
        mLingerTicks = 0;
    }
}PatrolNodePack;

///--[DeepLayerPack]
//--Indicates that a collision layer governs "deep snow" or "deep water" etc. These can slow down
//  the player as they move through them, may change rendering properties, and have SFX associated.
typedef struct DeepLayerPack
{
    //--Members.
    bool mPreventsRun;
    int mCollisionAssociated;
    int mPixelSink;
    char mSpecialFrameOverlayName[32];
    char mWalkSound[32];
    char mRunSound[32];

    //--Functions.
    void Initialize()
    {
        mPreventsRun = false;
        mCollisionAssociated = -1;
        mPixelSink = 0;
        mSpecialFrameOverlayName[0] = '\0';
        mWalkSound[0] = '\0';
        mRunSound[0] = '\0';
    }
}DeepLayerPack;

///--[NotificationPack]
//--Appears in the bottom left corner of the screen with a piece of text as a StarlightString,
//  and thus able to show icons. Appears only if a specific option is tripped.
typedef struct NotificationPack
{
    //--Members
    StarlightString *mString;
    int mTimer;
    int mTimerMax;
    EasingPack1D mYPosition;

    //--Functions.
    void Initialize();
    static void DeleteThis(void *pPtr);
}NotificationPack;

///--[ObjectivePack]
//--Specifies an objective that appears in the top left of the screen.
typedef struct ObjectivePack
{
    //--Members
    bool mIsComplete;
    char *mDisplayName;

    //--Methods
    void Initialize();
    static void DeleteThis(void *pPtr);
}ObjectivePack;
