//--Base
#include "AdventureMenu.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"
#include "StarUIPiece.h"

//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

///======================================= List Handlers ==========================================
//--Handlers related to the many StarLinkedLists that exist in the UI, both in retrieving things
//  and checking things on these lists, to running common calls on everything in a list.

///===================================== List Manipulators ========================================
void AdventureMenu::RegisterUI(const char *pName, StarUIPiece *pElement, int pBackwardCode, StarLinkedList *pList)
{
    ///--[Documentation]
    //--Registers the given UI to the list provided and the master list. If an existing element is on the
    //  list with the same name, removes it.
    if(!pName || !pElement || !pList) return;

    //--If the list is the master list, fail. The algorithm already registers to the master list.
    if(pList == mAllComponents) return;

    ///--[Existing Check]
    //--If the element is on the component list, remove it.
    void *rCheckPtrList = pList->GetElementByName(pName);
    if(rCheckPtrList) pList->RemoveElementP(rCheckPtrList);

    //--If the element is on the master list, remove it.
    void *rCheckPtrMaster = mAllComponents->GetElementByName(pName);
    if(rCheckPtrMaster) mAllComponents->RemoveElementP(rCheckPtrMaster);

    ///--[Register]
    //--Set the backward code on the list element.
    pElement->SetCodeBackward(pBackwardCode);

    //--Register to the sublist and the master list.
    pList->AddElement(pName, pElement);
    mAllComponents->AddElement(pName, pElement, &RootObject::DeleteThis);
}
void AdventureMenu::UnregisterUI(const char *pName, StarLinkedList *pList)
{
    ///--[Documentation]
    //--Removes the named UI from the provided list, and the master list.
    if(!pName || !pList) return;

    //--If the list is the master list, fail. It is important that UIs be removed from their derived list AND
    //  the master list, because if the master list copy is deleted, it deallocates and leaves a dangling pointer.
    if(pList == mAllComponents) return;

    //--Derived list.
    pList->RemoveElementS(pName);
    mAllComponents->RemoveElementS(pName);
}
StarUIPiece *AdventureMenu::RetrieveUI(const char *pName, int pType, StarLinkedList *pList, const char *pErrorCall)
{
    ///--[Documentation]
    //--Locates a UI in the given list. If the list is NULL, checks the master list instead. The pErrorCall text
    //  will indicate which function is the caller, on error. If NULL no error is reported if the UI is not found.
    if(!pName) return NULL;

    ///--[List Resolve]
    //--If the caller passed NULL, use the all-component list.
    StarLinkedList *rUseList = pList;
    if(!rUseList) rUseList = mAllComponents;

    ///--[Retrieve and Check]
    //--Check if the UI exists.
    StarUIPiece *rCheckUI = (StarUIPiece *)rUseList->GetElementByName(pName);
    if(!rCheckUI)
    {
        if(pErrorCall) fprintf(stderr, "%s - Failed. UI %s is NULL.\n", pErrorCall, pName);
        return NULL;
    }

    //--Check that the UI is of the correct type.
    if(!rCheckUI->IsOfType(pType))
    {
        if(pErrorCall) fprintf(stderr, "%s - Failed. UI %s is of type %i and not %i.\n", pErrorCall, pName, rCheckUI->GetType(), pType);
        return NULL;
    }

    //--Return valid pointer.
    return rCheckUI;
}
StarUIPiece *AdventureMenu::LocateComponentInList(StarLinkedList *pList, const char *pCode)
{
    ///--[Documentation]
    //--Given a StarLinkedList of StarUIPiece objects, locates the entry that matches the 4 letter code
    //  provided and returns it.
    //--Returns NULL on error or if not found.
    if(!pList || !pCode) return NULL;

    ///--[Iterate]
    StarUIPiece *rUIPiece = (StarUIPiece *)pList->PushIterator();
    while(rUIPiece)
    {
        //--Get the 4-letter code of the piece.
        const char *rSubtypeString = rUIPiece->GetSubtype();

        //--Match, return.
        if(!strcasecmp(pCode, rSubtypeString))
        {
            pList->PopIterator();
            return rUIPiece;
        }

        //--Next.
        rUIPiece = (StarUIPiece *)pList->AutoIterate();
    }

    //--Not found. Return NULL.
    return NULL;
}
void AdventureMenu::ActivateComponentInListName(StarLinkedList *pList, const char *pName)
{
    ///--[Documentation]
    //--Given a StarLinkedList of StarUIPiece objects, locates the entry with the matching name
    //  and sets it as active, marking all other pieces as inactive.
    //--If the code is not found or NULL, all pieces are set to inactive.
    if(!pList) return;

    ///--[Locate]
    //--Get the pointer to the object. It can be NULL.
    void *rCheckPtr = pList->GetElementByName(pName);

    ///--[Iterate]
    StarUIPiece *rUIPiece = (StarUIPiece *)pList->PushIterator();
    while(rUIPiece)
    {
        //--Not a match, take background.
        if(rCheckPtr != rUIPiece)
        {
            rUIPiece->TakeBackground();
        }
        //--Foreground the piece.
        else
        {
            rUIPiece->TakeForeground();
        }

        //--Next.
        rUIPiece = (StarUIPiece *)pList->AutoIterate();
    }
}
void AdventureMenu::ActivateComponentInListCode(StarLinkedList *pList, const char *pCode)
{
    ///--[Documentation]
    //--Given a StarLinkedList of StarUIPiece objects, locates the entry that matches the 4 letter code
    //  provided and set it as the active piece, setting all other pieces as inactive.
    //--If the code is not found or NULL, all pieces are set to inactive.
    if(!pList) return;

    ///--[Iterate]
    StarUIPiece *rUIPiece = (StarUIPiece *)pList->PushIterator();
    while(rUIPiece)
    {
        //--Get the 4-letter code of the piece.
        const char *rSubtypeString = rUIPiece->GetSubtype();

        //--If it does not match, or the provided code was NULL:
        if(!pCode || strcasecmp(pCode, rSubtypeString))
        {
            rUIPiece->TakeBackground();
        }
        //--Foreground the piece.
        else
        {
            rUIPiece->TakeForeground();
        }

        //--Next.
        rUIPiece = (StarUIPiece *)pList->AutoIterate();
    }
}

///======================================= List Updates ===========================================
void AdventureMenu::ListUpdateBackground(StarLinkedList *pList)
{
    ///--[Documentation]
    //--Calls UpdateBackground() on a list of StarUIPiece's.
    if(!pList) return;

    ///--[Iterate]
    //--Update all component UIs in the list. This applies to all sublists.
    StarUIPiece *rUIPiece = (StarUIPiece *)pList->PushIterator();
    while(rUIPiece)
    {
        //--Run update.
        rUIPiece->UpdateBackground();

        //--If the close code fired, handle it. This still occurs in the background.
        const char *rCloseCode = rUIPiece->GetCloseBuffer();
        if(rCloseCode[0] != '\0')
        {
            //--If there is a close script, fire it.
            if(mClosingScript) LuaManager::Fetch()->ExecuteLuaFile(mClosingScript, 1, "S", rCloseCode);

            //--Reset the close code.
            rUIPiece->SetCloseCode(NULL);
        }

        //--Next.
        rUIPiece = (StarUIPiece *)pList->AutoIterate();
    }
}
int AdventureMenu::ListUpdateForeground(bool &sHandledUpdate, bool pIgnoreModes, StarLinkedList *pList)
{
    ///--[ ============== Documentation ================ ]
    //--Calls UpdateForeground() on a list of StarUIPiece's. Returns the exit flag if a UI is flagged to
    //  close. The flag AM_MODE_NOEXIT is returned if no UIs are flagging close.
    if(!pList) return AM_MODE_NOEXIT;

    ///--[ ================= Execution ================== ]
    //--Setup.
    int tExitFlag = AM_MODE_NOEXIT;

    //--Update all component UIs in the list.
    StarUIPiece *rUIPiece = (StarUIPiece *)pList->PushIterator();
    while(rUIPiece)
    {
        ///--[Update]
        //--Check the active mode handler. The mode is an integer, and the UI object has a backwards code in
        //  it that should match that integer. If the UI is not currently active, it should internally switch
        //  to running its background code.
        bool tShouldExit = false;
        int tBackwardsCode = rUIPiece->GetCodeBackward();
        bool tCannotHandleUpdate = (mCurrentMode != tBackwardsCode);

        //--If this flag is true, always run the update as foreground regardless of the mode.
        if(pIgnoreModes) tCannotHandleUpdate = false;

        //--Run update. If the update was already handled by another UI, or if the current mode
        //  is not this UI's mode, the UI should switch to background mode.
        sHandledUpdate |= rUIPiece->UpdateForeground(sHandledUpdate || tCannotHandleUpdate);

        ///--[Object Is Closing]
        //--If the object flagged to exit:
        if(rUIPiece->IsFlaggingExit())
        {
            //--Re-get the backwards code, in case it changed. This can be done if the UI piece wants to
            //  redirect to another UI piece, which will be handled by SetCursorByBackwardsCode();
            tShouldExit = true;
            tExitFlag = rUIPiece->GetCodeBackward();

            //--Unflag the exit in all cases.
            rUIPiece->UnflagExit();
            rUIPiece->SetCodeBackward(tBackwardsCode);

            //--If this is the base object, closing will exit the menu, unless a backwards code is present.
            //  If a backwards code is provided, then the UI instead changes and opens a new object.
            if(rUIPiece == rUIBase)
            {
                OpenUIByBackwardsCode(tExitFlag);
            }
            //--If this is the campfire object, same as above but uses the campfire reopen codes.
            else if(rUIPiece == rCampBase)
            {
                OpenCampUIByBackwardsCode(tExitFlag);
            }
            //--Non-base object, closing will return to the base object.
            else
            {
                //--All UI pieces in this list default back to the main menu. Note that this goes by mode,
                //  not menu position. The subroutine resolves the position by the mode code.
                mCurrentMode = AM_MODE_BASE;

                //--If the backwards code is AM_MODE_EXITNOW, immediately hide the UI. This is caused by
                //  a UI, such as the Warp UI, explicitly needing to exit without allowing the player to stop it.
                if(tExitFlag == AM_MODE_EXITNOW)
                {
                    Hide();
                }
                //--Normal cases, adjust the UI's cursor.
                else
                {
                    rUIBase->SetCursorByBackwardsCode(tBackwardsCode);
                }
            }
        }

        ///--[Close Code Scripts]
        //--If the close code fired, handle it.
        const char *rCloseCode = rUIPiece->GetCloseBuffer();
        if(rCloseCode[0] != '\0')
        {
            //--If there is a close script, fire it.
            if(mClosingScript) LuaManager::Fetch()->ExecuteLuaFile(mClosingScript, 1, "S", rCloseCode);

            //--Reset the close code.
            rUIPiece->SetCloseCode(NULL);
        }

        //--Stop iterating if the object exited.
        if(tShouldExit)
        {
            pList->PopIterator();
            break;
        }

        //--Next.
        rUIPiece = (StarUIPiece *)pList->AutoIterate();
    }

    ///--[ ================= Finish Up ================== ]
    //--Pass back the exit code.
    return tExitFlag;
}
void AdventureMenu::ListRenderForeground(StarLinkedList *pList, float pOpacity)
{
    ///--[Documentation]
    //--Calls RenderForeground() for all StarUIPiece's in a list.
    if(!pList) return;

    ///--[Iterate]
    StarUIPiece *rUIPiece = (StarUIPiece *)pList->PushIterator();
    while(rUIPiece)
    {
        //--Run update.
        //fprintf(stderr, "Rendering %s %p ", rUIPiece->GetSubtype(), rUIPiece);
        rUIPiece->RenderForeground(pOpacity);
        //fprintf(stderr, "Completed.\n");

        //--Next.
        rUIPiece = (StarUIPiece *)pList->AutoIterate();
    }
}
