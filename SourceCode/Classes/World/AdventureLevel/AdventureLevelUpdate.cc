//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdvCombat.h"
#include "ActorNotice.h"
#include "AdventureDebug.h"
#include "AdventureMenu.h"
#include "FieldAbility.h"
#include "KPopDanceGame.h"
#include "PuzzleFight.h"
#include "RootEntity.h"
#include "RunningMinigame.h"
#include "ShakeMinigame.h"
#include "TilemapActor.h"
#include "WorldDialogue.h"

//--CoreClasses
//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"

///--[Local Definitions]
///--[Debug]
//#define AL_UPDATE_DEBUG
#ifdef AL_UPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Update ==============================================
void AdventureLevel::Update()
{
    ///--[Documentation and Setup]
    //--Debug.
    DebugPush(true, "AdventureLevel: Update begin.\n");

    //--Flag reset each tick.
    xIsPlayerInInvisZone = false;

    ///--[Special: Cutscene Acceleration]
    //--Set this flag if the player is holding down the accelerate keys. This allows entities to
    //  run more than one event update per tick.
    ControlManager *rControlManager = ControlManager::Fetch();
    bool tIsHoldingActivate = rControlManager->IsDown("Activate");
    bool tIsHoldingCancel = rControlManager->IsDown("Cancel");
    if(tIsHoldingActivate && tIsHoldingCancel)
    {
        CutsceneManager::xUpdatesPerEntityThisTick = CM_ACCELERATED_CUTSCENE_CYCLES;
        CutsceneManager::xTimerUpdatesThisTick = CM_ACCELERATED_CUTSCENE_CYCLES;
    }

    ///--[Music]
    //--Music update.
    UpdateMusicLayering();

    ///--[Running Minigame]
    //--If active, takes over updates. Other objects may still update.
    if(mRunningMinigame)
    {
        //--Run the update.
        mRunningMinigame->Update();

        //--If active, stop updates here.
        if(!mRunningMinigame->IsGameFinished())
        {
            return;
        }
        //--If the game is finished, and no longer visible, delete it.
        else if(!mRunningMinigame->IsVisible())
        {
            delete mRunningMinigame;
            mRunningMinigame = NULL;
        }
    }

    ///--[KPop Minigame]
    //--Yes, really.
    if(mKPopDanceGame)
    {
        //--Update.
        mKPopDanceGame->Update();

        //--If active, stop updates here.
        if(mKPopDanceGame->IsActive())
        {
            return;
        }
        //--If not active and not visible, delete it.
        else if(!mKPopDanceGame->IsVisible())
        {
            delete mKPopDanceGame;
            mKPopDanceGame = NULL;
        }
    }
    ///--[Puzzle Fight]
    //--Not exclusive with the rest of the update. Will also intercept controls later.
    if(mPuzzleFightGame)
    {
        //--Update.
        mPuzzleFightGame->Update();

        //--Clearing case.
        if(mPuzzleFightGame->IsComplete())
        {
            delete mPuzzleFightGame;
            mPuzzleFightGame = NULL;
        }
    }

    ///--[Shaking Minigame]
    //--Intercepts controls.
    if(mShakeMinigame)
    {
        //--Update.
        mShakeMinigame->Update();

        //--Clearing case.
        if(mShakeMinigame->IsComplete())
        {
            delete mShakeMinigame;
            mShakeMinigame = NULL;
        }
        return;
    }

    ///--[Region Notifiers]
    //--Does nothing when fading.
    if(mIsTransitionIn || mIsTransitionOut || mTransitionHoldTimer > -1)
    {
        //--If there is no lockout in place, but a region notifier is going to appear,
        //  place a lockout.
        if(mRegionNotifierAnimState == AL_REGION_STATE_FADEIN && !mControlLockList->GetElementByName("Region Notifier"))
        {
            mPlayerStoppedMovingTimer = AL_UITIME_STANDSTILL_TICKS;
            mControlLockList->AddElement("Region Notifier", &mRegionNotifierAnimTimer);
        }
    }
    //--Fade in animation.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_FADEIN)
    {
        mRegionNotifierAnimTimer ++;
        mPlayerStoppedMovingTimer = AL_UITIME_STANDSTILL_TICKS;
        if(mRegionNotifierAnimTimer >= AL_REGION_FADE_TICKS)
        {
            mRegionNotifierAnimTimer = 0;
            mRegionNotifierAnimState = AL_REGION_STATE_ANIMATE;
        }
    }
    //--Render animation.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_ANIMATE)
    {
        mRegionNotifierAnimTimer ++;
        mPlayerStoppedMovingTimer = AL_UITIME_STANDSTILL_TICKS;
        if(mRegionNotifierAnimTimer >= mRegionNotifierAnimTimerMax + 30)
        {
            mRegionNotifierAnimTimer = 0;
            mRegionNotifierAnimState = AL_REGION_STATE_SHRINK;
            mControlLockList->RemoveElementS("Region Notifier");
        }
    }
    //--Moving to the top right.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_SHRINK)
    {
        mRegionNotifierAnimTimer ++;
        mPlayerStoppedMovingTimer = AL_UITIME_STANDSTILL_TICKS;
        if(mRegionNotifierAnimTimer >= AL_REGION_SHRINK_TICKS)
        {
            mRegionNotifierAnimTimer = 0;
            mRegionNotifierAnimState = AL_REGION_STATE_IDLE;
        }
    }
    //--Holding position in the top right.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_IDLE)
    {
    }

    ///--[Autosave Indicator]
    if(mAutosavePostTimer > 0) mAutosavePostTimer --;

    ///--[Notifications]
    UpdateNotifications();

    ///--[Auta Particles]
    //--Run update on all aura particles, remove ones that have expired.
    AuraParticle *rParticle = (AuraParticle *)mAuraParticleList->SetToHeadAndReturn();
    while(rParticle)
    {
        //--Update.
        rParticle->Update();

        //--Expired. Delete.
        if(rParticle->mLifetime < 1) mAuraParticleList->RemoveRandomPointerEntry();

        //--Next.
        rParticle = (AuraParticle *)mAuraParticleList->IncrementAndGetRandomPointerEntry();
    }

    //--If particles are spawning, do so here.
    if(rSpawnAuraImage && mSpawnAuraChance > 0)
    {
        //--Roll.
        int tRoll = rand() % 100;

        //--Spawn a random particle.
        if(tRoll < mSpawnAuraChance)
        {
            //--Setup.
            AuraParticle *nParticle = (AuraParticle *)starmemoryalloc(sizeof(AuraParticle));
            nParticle->Initialize();
            nParticle->rImage = rSpawnAuraImage;

            //--Random position.
            nParticle->mXPos = 50.0f + (rand() % (int)(VIRTUAL_CANVAS_X - 100.0f));
            nParticle->mYPos = 50.0f + (rand() % (int)(VIRTUAL_CANVAS_Y - 100.0f));

            //--Random speeds.
            float cSpeed = 0.5f;
            float tAngle = rand() % 360;
            nParticle->mXSpeed = cosf(tAngle * TORADIAN) * cSpeed;
            nParticle->mYSpeed = sinf(tAngle * TORADIAN) * cSpeed;

            //--Scatter size.
            nParticle->mSizeTimer = rand() % AURAPARTICLE_SIZE_PERIOD;

            //--Register.
            mAuraParticleList->AddElement("X", nParticle, &FreeThis);
        }
    }

    ///--[Shaking]
    //--Screen shake handling.
    if(mScreenShakeTimer > 0)
    {
        //--Decrement.
        mScreenShakeTimer --;

        //--If the timer reaches zero and the periodicity is non-zero, roll a new shake time.
        if(mScreenShakeTimer == 0 && mShakePeriodicityRoot > 0)
        {
            //--If the periodicity's scatter is zero, it plays at fixed intervals.
            if(mShakePeriodicityScatter  <= 0)
            {
                mScreenShakeTimer = -mShakePeriodicityRoot;
            }
            //--Otherwise, roll.
            else
            {
                int tRoll = mShakePeriodicityRoot + (rand() % mShakePeriodicityScatter);
                mScreenShakeTimer = tRoll * -1;
            }
        }
    }
    //--If the shake timer is less than zero, it is counting up to a positive.
    else if(mScreenShakeTimer < 0)
    {
        //--Increment.
        mScreenShakeTimer ++;

        //--If the timer reaches zero, start a new shake sequence.
        if(mScreenShakeTimer == 0)
        {
            //--Set the timer.
            mScreenShakeTimer = mShakePeriodLength;

            //--Get a sound effect and play it, if any exist.
            char *rSoundName = (char *)mShakeSoundList->GetEntryByRandomRoll();
            if(rSoundName) AudioManager::Fetch()->PlaySound(rSoundName);
        }
    }

    ///--[Falling Rocks]
    //--Spawn Falling Rocks
    if(mRockFallingFrequency > 0)
    {
        //--Roll, 100% with two-decimal accuracy. Spawn a rock if the roll passes.
        int tRoll = rand() % 10000;
        if(tRoll <= mRockFallingFrequency)
        {
            SetMemoryData(__FILE__, __LINE__);
            FallingRockPack *nNewRock = (FallingRockPack *)starmemoryalloc(sizeof(FallingRockPack));
            nNewRock->Initialize();
            nNewRock->mX = (float)(rand() & (int)VIRTUAL_CANVAS_X);
            nNewRock->mYSpeed = 6.0f;
            nNewRock->mTheta = rand() % 360;
            nNewRock->mThetaDelta = (rand() % 9) - 4;
            nNewRock->rImage = (StarBitmap *)mrFallingRockImageList->GetEntryByRandomRoll();
            mFallingRockList->AddElement("X", nNewRock, &FreeThis);
        }
    }

    //--Update falling rocks.
    FallingRockPack *rRock = (FallingRockPack *)mFallingRockList->SetToHeadAndReturn();
    while(rRock)
    {
        //--Update Y position.
        rRock->mY = rRock->mY + rRock->mYSpeed;
        rRock->mTheta = rRock->mTheta + rRock->mThetaDelta;

        //--Y position exceeds bottom of screen, delete.
        if(rRock->mY >= VIRTUAL_CANVAS_Y) mFallingRockList->RemoveRandomPointerEntry();

        //--Next.
        rRock = (FallingRockPack *)mFallingRockList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Localized Animations]
    //--Per-map animations update.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->PushIterator();
    while(rAnimation)
    {
        //--If the animation is not rendering, ignore it.
        if(!rAnimation->mIsRendering)
        {
        }
        //--Tick it.
        else
        {
            rAnimation->Tick();
        }

        //--Next.
        rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
    }

    ///--[Stamina Bar]
    //--Stamina bar is visible, increase its vis timer.
    if(IsStaminaBarVisible() && !mPuzzleFightGame)
    {
        if(mStaminaVisTimer < STAMINA_VIS_TICKS) mStaminaVisTimer ++;
    }
    //--Decrement.
    else
    {
        if(mStaminaVisTimer > 0) mStaminaVisTimer --;
    }

    //--Stamina ring visibility. The stamina ring rapidly becomes visible if the player sprints
    //  and slowly becomes invisible at full stamina.
    if(mPlayerStamina >= mPlayerStaminaMax)
    {
        mFullStaminaTicks ++;
        if(mFullStaminaTicks > STAMINA_RING_MAX_TICKS)
        {
            if(mStaminaRingVisTimer > 0)
            {
                mStaminaRingVisTimer --;
                mStaminaRingVisPct = mStaminaRingVisTimer / (float)STAMINA_RING_VIS_TICKS;
            }
        }
    }
    //--Not at max stamina. Rapidly show the bar and reset other timers.
    else
    {
        mFullStaminaTicks = 0;
        if(mStaminaRingVisTimer < STAMINA_RING_VIS_TICKS)
        {
            mStaminaRingVisTimer += 3;
            if(mStaminaRingVisTimer >= STAMINA_RING_VIS_TICKS) mStaminaRingVisTimer = STAMINA_RING_VIS_TICKS;
            mStaminaRingVisPct = mStaminaRingVisTimer / (float)STAMINA_RING_VIS_TICKS;
        }
    }

    ///--[Overall UI Fading]
    //--In general, the world UI tries to be seen only when necessary. The UI appears if the player stands
    //  still for AL_UITIME_STANDSTILL_TICKS, or if any field abilities are cooling, or if the player is
    //  using the stamina bar (not the ring) and does not have full stamina.
    //--If any of the above conditions are true, the UI will fade in. If they are all false, the UI fades out.
    bool tAllowUIFadeIn = false;

    //--Any field abilities are cooling.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
    {
        FieldAbility *rFieldAbility = rAdventureCombat->GetFieldAbility(i);
        if(rFieldAbility && rFieldAbility->IsCooling()) tAllowUIFadeIn = true;
    }

    //--Stamina bar.
    bool tShowStaminaBar = OptionsManager::Fetch()->GetOptionB("Show Stamina Bar");
    if(tShowStaminaBar && mPlayerStamina < mPlayerStaminaMax)
    {
        //tAllowUIFadeIn = true;
    }

    //--Player has been stationary for AL_UITIME_STANDSTILL_TICKS.
    if(mPlayerStoppedMovingTimer >= AL_UITIME_STANDSTILL_TICKS)
    {
        tAllowUIFadeIn = true;
    }

    //--UI should fade in:
    if(tAllowUIFadeIn)
    {
        if(mEntireUIVisTimer <  AL_UITIME_FADE_TICKS) mEntireUIVisTimer += AL_UITIME_FADEIN_TICKRATE;
        if(mEntireUIVisTimer >= AL_UITIME_FADE_TICKS) mEntireUIVisTimer  = AL_UITIME_FADE_TICKS;
    }
    //--UI should fade out:
    else
    {
        if(mEntireUIVisTimer >  0) mEntireUIVisTimer -= AL_UITIME_FADEOUT_TICKRATE;
        if(mEntireUIVisTimer <= 0) mEntireUIVisTimer  = 0;
    }

    ///--[Lighting]
    //--Lighting update. This is done in case any of the sub-updates return out before entities update.
    //  Normally we want to update lighting after entities have moved.
    if(mAreLightsActive) UpdateEntityLightPositions();

    ///--[Catalyst Tone]
    //--Catalyst Tone.
    if(mPlayCatalystTone)
    {
        mCatalystToneCountdown --;
        if(mCatalystToneCountdown < 1)
        {
            mPlayCatalystTone = false;
            AudioManager::Fetch()->PlaySound("World|Catalyst");
        }
    }

    ///--[Field Abilities]
    //--Field Abilities
    bool tAreControlsLockedByFieldAbility = false;
    bool tBlockFieldAbilityUpdate = mAdventureMenu->NeedsToBlockControls();
    if(!tBlockFieldAbilityUpdate)
    {
        //--Iterate.
        FieldAbilityPack *rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->SetToHeadAndReturn();
        while(rFieldAbilityPack)
        {
            //--Mark the global package. This is the one referenced in lua scripts.
            rExecutingPackage = rFieldAbilityPack;

            //--Run the field ability update.
            if(rFieldAbilityPack->rParentAbility)
            {
                //--In normal mode:
                if(!mFieldAbilitiesInCancelMode || mDontCancelFieldAbilitiesForOneScene)
                {
                    rFieldAbilityPack->rParentAbility->Execute(FIELDABILITY_EXEC_RUN);
                }
                //--In cancel mode:
                else
                {
                    rFieldAbilityPack->rParentAbility->Execute(FIELDABILITY_EXEC_RUNCANCEL);
                }
            }
            //--Parent was not set correctly, remove it.
            else
            {
                rFieldAbilityPack->mIsExpired = true;
            }

            //--Increment the timer.
            rFieldAbilityPack->mTimer ++;

            //--If the package was marked as expired, terminate it here.
            if(rFieldAbilityPack->mIsExpired)
            {
                mExecutingFieldAbilities->RemoveRandomPointerEntry();
            }
            //--Otherwise, if the field ability pack is blocking controls, mark that here.
            else if(rFieldAbilityPack->mIsLockingControls)
            {
                tAreControlsLockedByFieldAbility = true;
            }

            //--Next.
            rExecutingPackage = NULL;
            rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->IncrementAndGetRandomPointerEntry();
        }
    }

    //--If field abilities are in cancel mode, the flag is cleared as soon as all abilities are removed.
    if((mFieldAbilitiesInCancelMode && mExecutingFieldAbilities->GetListSize() == 0) || mDontCancelFieldAbilitiesForOneScene)
    {
        mFieldAbilitiesInCancelMode = false;
    }

    ///--[Music Debug]
    //--Music controls.
    if(rControlManager->IsFirstPress("LoopBack"))
    {
        AudioManager::Fetch()->AdvanceMusicToLoopPoint();
        //AudioManager::Fetch()->ModifyLoopPointBackward();
    }
    if(rControlManager->IsFirstPress("LoopForw"))
    {
        AudioManager::Fetch()->AdvanceMusicToLoopPoint();
        //AudioManager::Fetch()->ModifyLoopPointForward();
    }

    ///--[Underlays]
    //--Underlays.
    for(int i = 0; i < mUnderlaysTotal; i ++)
    {
        //--Alpha change over time.
        if(mUnderlayPacks[i].mAlphaTimer < mUnderlayPacks[i].mAlphaTimerMax)
        {
            //--Increment timer.
            mUnderlayPacks[i].mAlphaTimer ++;

            //--Compute percentage.
            float tPercent = (float)mUnderlayPacks[i].mAlphaTimer / (float)mUnderlayPacks[i].mAlphaTimerMax;
            mUnderlayPacks[i].mAlphaCurrent = mUnderlayPacks[i].mAlphaStart + ((mUnderlayPacks[i].mAlphaEnd - mUnderlayPacks[i].mAlphaStart) * tPercent);
        }

        //--Autoscroll.
        mUnderlayPacks[i].mOffsetBaseX = mUnderlayPacks[i].mOffsetBaseX + mUnderlayPacks[i].mAutoscrollX;
        mUnderlayPacks[i].mOffsetBaseY = mUnderlayPacks[i].mOffsetBaseY + mUnderlayPacks[i].mAutoscrollY;
    }

    ///--[Overlays]
    //--Overlays.
    for(int i = 0; i < mOverlaysTotal; i ++)
    {
        //--Alpha change over time.
        if(mOverlayPacks[i].mAlphaTimer < mOverlayPacks[i].mAlphaTimerMax)
        {
            //--Increment timer.
            mOverlayPacks[i].mAlphaTimer ++;

            //--Compute percentage.
            float tPercent = (float)mOverlayPacks[i].mAlphaTimer / (float)mOverlayPacks[i].mAlphaTimerMax;
            mOverlayPacks[i].mAlphaCurrent = mOverlayPacks[i].mAlphaStart + ((mOverlayPacks[i].mAlphaEnd - mOverlayPacks[i].mAlphaStart) * tPercent);
        }

        //--Autoscroll.
        mOverlayPacks[i].mOffsetBaseX = mOverlayPacks[i].mOffsetBaseX + mOverlayPacks[i].mAutoscrollX;
        mOverlayPacks[i].mOffsetBaseY = mOverlayPacks[i].mOffsetBaseY + mOverlayPacks[i].mAutoscrollY;
    }

    ///--[Notifications]
    //--Notifications.
    ActorNotice *rNotice = (ActorNotice *)mNotices->SetToHeadAndReturn();
    while(rNotice)
    {
        //--Update.
        rNotice->Update();

        //--If the notice has expired, delete it.
        if(rNotice->IsExpired()) mNotices->RemoveRandomPointerEntry();

        //--Next.
        rNotice = (ActorNotice *)mNotices->IncrementAndGetRandomPointerEntry();
    }

    ///--[Player Control Locking]
    //--Autosave timer. Decrements to zero, when it hits zero, attempts to autosave if the player has no control
    //  lockouts.
    if(mAutosaveTimer > -1) mAutosaveTimer --;

    //--Check if the player's controls are locked before the update starts. This removes double-control
    //  issues from consideration.
    DebugPrint("Checking control locking.\n");
    mWasMenuUpLastTick = false;
    bool tAreControlsLocked = AreControlsLocked();
    DebugPrint(" Control lock state %i\n", tAreControlsLocked);

    ///--[Script Fading]
    //--Script fading always runs.
    DebugPrint("Updating fade script.\n");
    UpdateScriptFade();

    ///--[Resting Sequence]
    //--Updates the timer. Locks out everything else.
    DebugPrint("Checking rest sequence.\n");
    if(mIsRestSequence)
    {
        //--Debug.
        DebugPrint("Resting sequence.\n");

        //--The resting timer goes until it hits RESTING_TICKS_TOTAL.
        if(mRestingTimer < RESTING_TICKS_TOTAL)
        {
            //--On this tick, restore the party and respawn enemies. This is done by the AdventureMenu.
            if(mRestingTimer == (RESTING_TICKS_FADEOUT))
            {
                mAdventureMenu->ExecuteRest();
                ResetChestsDuringRest();
            }
            //--Five ticks later, execute the rest resolve script. This also increments the timer by 1 to prevent
            //  an infinite event loop.
            else if(mRestingTimer == (RESTING_TICKS_FADEOUT + 5) && AdventureMenu::xRestResolveScript)
            {
                mRestingTimer ++;
                LuaManager::Fetch()->ExecuteLuaFile(AdventureMenu::xRestResolveScript);
            }

            //--If the dialogue is open, run that. This also prevents the resting timer from running while it's open!
            CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
            WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
            if(rWorldDialogue->IsManagingUpdates() || rCutsceneManager->HasAnyEvents())
            {
                //--Debug.
                DebugPrint("Updating world dialogue.\n");

                //--Run the dialogue.
                rWorldDialogue->Update();

                //--Cutscenes can also update here.
                if(rCutsceneManager->HasAnyEvents())
                {
                    //--Run the update.
                    rCutsceneManager->Update();
                }

                //--Debug.
                DebugPop("AdventureLevel: Update completes, resting sequence is running.\n");
                return;
            }
            //--If the dialogue is hiding, update it.
            else
            {
                rWorldDialogue->Update();
            }

            //--Run timer.
            mRestingTimer ++;
            DebugPop("AdventureLevel: Update completes, resting sequence is running.\n");
            return;
        }
        //--Resting is over, resume operations.
        else
        {
            mRestingTimer = 0;
            mIsRestSequence = false;
        }
    }

    ///--[Transitions]
    //--Transitions take priority.
    DebugPrint("Checking transitions.\n");
    if(mIsTransitionIn || mIsTransitionOut)
    {
        //--Timer.
        mTransitionTimer ++;

        //--Debug.
        DebugPrint("Transitioning.\n");

        //--Ending case.
        if(mTransitionTimer >= TRANSITION_FADE_TICKS + TRANSITION_HOLD_TICKS)
        {
            if(mIsTransitionIn) mTransitionTimer = 0;
            mIsTransitionIn = false;

            //--Stop the update during the hold part.
            if(mTransitionTimer < TRANSITION_HOLD_TICKS)
            {
                DebugPop("AdventureLevel: Update completes, transitioning in, holding.\n");
                return;
            }
        }
        //--Counting case. Stops execution for fade-outs.
        else
        {
            if(mIsTransitionOut)
            {
                if(mTransitionTimer < TRANSITION_FADE_TICKS + TRANSITION_HOLD_TICKS) mTransitionTimer = TRANSITION_FADE_TICKS + TRANSITION_HOLD_TICKS;
                DebugPop("AdventureLevel: Update completes, transitioning out.\n");
                return;
            }
        }
    }

    ///--[World Dialogue]
    //--Update the WorldDialogue, if it's visible. If combat is running, it will handle the dialogue instead.
    DebugPrint("Checking world dialogue.\n");
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible())
    {
        ///--[Debug]
        //--Debug.
        DebugPrint("Updating world dialogue.\n");

        ///--[Dialogue is Hiding]
        //--If not managing updates, the dialogue is hiding, and needs only to update.
        if(!rWorldDialogue->IsManagingUpdates())
        {
            //--Update.
            rWorldDialogue->Update();
        }
        ///--[Dialogue is Running]
        //--The dialogue is open and taking player input.
        else
        {
            //--Clear field abilities before continuing with the dialogue.
            if(mExecutingFieldAbilities->GetListSize() > 0 && !mDontCancelFieldAbilitiesForOneScene)
            {
                mFieldAbilitiesInCancelMode = true;
                DebugPop("AdventureLevel: Update completes, cancelling field abilities.\n");
                return;
            }

            //--Run the dialogue.
            rWorldDialogue->Update();

            //--Cutscenes can also update here.
            CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
            if(rCutsceneManager->HasAnyEvents())
            {
                //--Run the update.
                rCutsceneManager->Update();
            }

            //--Timer.
            if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;

            //--If this flag is set, parallel cutscenes will update.
            if(mRunParallelDuringDialogue) rCutsceneManager->UpdateParallelEvents();

            //--Debug.
            mAdventureMenu->Update();
            DebugPop("AdventureLevel: Update completes, dialogue sequence is running.\n");
            return;
        }
    }

    ///--[Combat World Pulse]
    //--Player cannot act.
    DebugPrint("Checking pulsing.\n");
    if(mIsPulseMode)
    {
        //--Menu hides if already hiding.
        mAdventureMenu->UpdateBackground();

        //--Enemies are considered as close as possible.
        xClosestEnemy = 0.0f;

        //--Run the timer.
        mPulseTimer ++;
        if(mPulseTimer >= PULSE_TICKS + 30) mIsPulseMode = false;

        //--Debug.
        DebugPop("AdventureLevel: Update completes, pulsing.\n");
        return;
    }

    ///--[Combat]
    //--Run the update.
    DebugPrint("Updating Combat UI.\n");
    rAdventureCombat->Update();

    //--The combat UI will stop the update based on its internal logic.
    if(!rAdventureCombat->IsStoppingWorldUpdate())
    {
    }
    //--Otherwise, it does. Stop the update here.
    else
    {
        mAdventureMenu->UpdateBackground();
        mFieldAbilitiesInCancelMode = false;
        if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;
        DebugPop("AdventureLevel: Update completes, combat handled the update.\n");
        return;
    }

    ///--[GUI]
    //--Update the AdventureMenu. It won't do anything if it's not visible except run timers.
    DebugPrint("Updating Adventure Menu.\n");
    mAdventureMenu->Update();

    //--If the menu needs to block controls, stop here.
    if(mAdventureMenu->NeedsToBlockControls())
    {
        DebugPop("AdventureLevel: Update completes, menu handled the update.\n");
        return;
    }

    //--Check if there is a pending transition. Exits set these, but they can also be set by cutscenes or combat.
    DebugPrint("Checking transitions.\n");
    if(HandleTransition())
    {
        DebugPop("AdventureLevel: Update completes, transitioning.\n");
        return;
    }

    ///--[Cutscenes]
    //--Cutscenes! If acceleration is active, this can fire more than once.
    DebugPrint("Checking cutscenes.\n");
    bool tMustReturnOut = false;
    int tIterations = 0;

    //--Get how many times to run the manager.
    if(tIsHoldingActivate && tIsHoldingCancel) tIterations = 9;

    //--If the manager has events, run the update.
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rCutsceneManager->HasAnyEvents())
    {
        //--If there are any field abilities executing, mark them for removal and wait a tick.
        if(mExecutingFieldAbilities->GetListSize() > 0 && !mDontCancelFieldAbilitiesForOneScene)
        {
            mFieldAbilitiesInCancelMode = true;
            DebugPop("AdventureLevel: Update completes, cancelling field abilities.\n");
            return;
        }

        //--If waiting for field abilities to expire, skip this.
        if(mFieldAbilitiesInCancelMode)
        {
            DebugPop("AdventureLevel: Update completes, waiting for field abilities before cutscene fires.\n");
            return;
        }

        //--Run the update.
        DebugPrint("Updating cutscenes.\n");
        rCutsceneManager->Update();
        DebugPrint("Finished cutscenes.\n");

        //--Timer.
        if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;

        //--If this flag is set, parallel cutscenes will update.
        DebugPrint("Updating parallel events.\n");
        if(mRunParallelDuringCutscene) rCutsceneManager->UpdateParallelEvents();
        DebugPrint("Finished parallel events.\n");
        tMustReturnOut = true;
    }
    else
    {
        tIterations = 0;
        mDontCancelFieldAbilitiesForOneScene = false;
    }

    //--Successive updates. We don't need to rerun the field ability cancel cases.
    while(tIterations > 0)
    {
        rCutsceneManager->Update();
        tIterations --;
    }

    //--If ordered, end the update here. The cutscene manager will stop updating if at least one cutscene
    //  event took place, even if multiple iterations passed.
    if(tMustReturnOut)
    {
        DebugPop("AdventureLevel: Update completes, cutscene handled it.\n");
        return;
    }

    //--No cutscene is playing, so unlock the camera.
    else
    {
    }

    //--If there were no cutscenes running, unlock the camera.
    SetCameraLocking(false);

    ///--[Paralell Events]
    //--Cutscene manager always runs parallel events.
    rCutsceneManager->UpdateParallelEvents();

    ///--[Autofire Handling]
    //--Run autofire cases for NPCs.
    if(TilemapActor::xAutofireCooldown > 0)
    {
        TilemapActor::xAutofireCooldown --;
    }
    if(TilemapActor::xAutofireCooldown < 1)
    {
        //--Iterate across the tilemap actor list.
        StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
        RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
        while(rCheckEntity)
        {
            //--Right type? Check.
            if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                //--If the actor reports true, it handled the activation and set the flag.
                if(((TilemapActor *)rCheckEntity)->HandleAutoActivation((int)mPlayerXPositions[0], (int)mPlayerYPositions[0]))
                {
                    rEntityList->PopIterator();
                    break;
                }
            }

            //--Next.
            rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
        }
    }

    ///--[Control Lockout]
    //--Control lockout: Ignore player controls.
    if(tAreControlsLocked || tAreControlsLockedByFieldAbility)
    {
        //--If controls are locked due to a cutscene or somesuch, run this timer.
        //  This takes priority over field abilities.
        if(tAreControlsLocked)
        {
            DebugPrint("Controls are locked.\n");
            if(mControlLockTimer < AL_CONTROL_LOCK_TICKS && !mNoBordersThisCutscene) mControlLockTimer ++;
        }
        //--If running a field ability, we still need to check against exits and triggers.
        //  If a trigger is hit, a special mode is activated.
        else
        {
            //--Get the actor in question.
            TilemapActor *rActor = LocatePlayerActor();
            if(!rActor) return;

            //--Check if the actor in question moved into any exits. If so, set the variables for those exits.
            DebugPrint("Checking exits and triggers.\n");
            if(CheckActorAgainstExits(rActor, true))
            {
                mFieldAbilitiesInCancelMode = true;
            }
            //--If that didn't go through, check the actor against triggers.
            else if(CheckActorAgainstTriggers(rActor, false))
            {
                //--Check if anything to lock controls got added, such as cutscene instructions. If so, activate cancel mode.
                if(rCutsceneManager->HasAnyEvents())
                {
                    mFieldAbilitiesInCancelMode = true;
                }
            }
        }
    }
    ///--[Player Controls]
    //--Not locked, pass instructions to the TilemapActor.
    else
    {
        //--Reset this flag.
        mNoBordersThisCutscene = false;

        //--Increment this flag so AIs can target the player.
        mEnemyAICycles ++;

        //--Camo timer only decrements if the player can move.
        if(mPlayerCamoTimer > 0) mPlayerCamoTimer --;

        //--Setup.
        ControlManager *rControlManager = ControlManager::Fetch();

        //--Timers.
        if(mControlLockTimer > 0) mControlLockTimer --;

        //--If nothing else was stealing control, then the DebugMenu can steal it now. Hah!
        DebugPrint("Checking debug menu.\n");
        if(mDebugMenu->IsVisible())
        {
            mWasMenuUpLastTick = true;
            mDebugMenu->Update();
            DebugPop("AdventureLevel: Update completes, debug menu handled it.\n");
            return;
        }

        //--Run autosave. If the internal flag is false, this does nothing.
        if(mAutosaveTimer == 0) RunAutosave();

        //--Retreat timer.
        if(mRetreatTimer > 0) mRetreatTimer --;

        //--Puzzle Fight game can intercept controls.
        if(mPuzzleFightGame && mPuzzleFightGame->IsHandlingUpdate())
        {
            mPuzzleFightGame->HandlePlayerControls();
        }
        //--Shooting mode. If it handled the controls, it will return true and block further inputs.
        else if(UpdateShootingControls())
        {

        }
        //--The player may press a key to open the debug menu at any time their controls are not locked.
        else if(rControlManager->IsFirstPress("OpenDebug"))
        {
            //--Setup.
            const char *rCheckPassword = OptionsManager::Fetch()->GetOptionS("Patron Password");

            //--If this flag is set, open the debug menu. This is activated by the password menu.
            if(AdventureDebug::xManualActivation)
            {
                mDebugMenu->Show();
            }
            //--Otherwise, the player needs to enter the correct patron password before the game starts.
            else if(!rCheckPassword || strcasecmp(rCheckPassword, "Ares Defense Industries"))
            {
            }
            //--Correct password.
            else
            {
                mDebugMenu->Show();
            }
        }
        //--Menu keypress.
        else if(rControlManager->IsFirstPress("Cancel") && mCannotOpenMenuTimer < 1 && !mAdventureMenu->StartedHidingThisTick() && !mCannotOpenMenu)
        {
            mAdventureMenu->Show();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Field Abilities menu.
        else if(rControlManager->IsFirstPress("OpenFieldAbilityMenu") && mCanEditFieldAbilities && !mAdventureMenu->StartedHidingThisTick() && !xLockoutFieldAbilityEditing)
        {
            mAdventureMenu->OpenFieldAbilities();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Field Ability 1:
        else if(rControlManager->IsFirstPress("FieldAbility0"))
        {
            ExecuteFieldAbility(0);
        }
        //--Field Ability 2:
        else if(rControlManager->IsFirstPress("FieldAbility1"))
        {
            ExecuteFieldAbility(1);
        }
        //--Field Ability 3:
        else if(rControlManager->IsFirstPress("FieldAbility2"))
        {
            ExecuteFieldAbility(2);
        }
        //--Field Ability 4:
        else if(rControlManager->IsFirstPress("FieldAbility3"))
        {
            ExecuteFieldAbility(3);
        }
        //--Field Ability 5:
        else if(rControlManager->IsFirstPress("FieldAbility4"))
        {
            ExecuteFieldAbility(4);
        }
        ///--[Entity Control Handling]
        //--Check for entity control handling.
        else
        {
            //--Debug.
            DebugPrint("Checking actor existence.\n");

            ///--[Locate the Party Leader]
            //--Make sure the entity exists and is of the right type.
            TilemapActor *rActor = LocatePlayerActor();
            if(rActor)
            {
                ///--[Movement Handling]
                //--Check whether or not the player can run.
                bool tCanRun = true;
                if(mBlockRunningToRegen) tCanRun = false;

                //--Let the player move.
                DebugPrint("Handling player controls.\n");
                bool tPlayerPressedMove = rActor->HandlePlayerControls(tCanRun);
                float tAmountMovedLastTick = rActor->GetMovementLastTick();

                ///--[Stamina Handling]
                //--If the player ran last tick, check for sound effects. This occurs even if no
                //  stamina is being drained. If the result of the function is NULL, no sound
                //  will be played.
                //--Note that, if there are any followers, the footstep sound plays anyway.
                if(rActor->mRanLastTick && tAmountMovedLastTick >= 1.0f)
                {
                    //--Check for a footstep sound. Flying forms have no footsteps.
                    char *rFootstepSound = rActor->GetRunFootstepSFX(mFollowEntityIDList->GetListSize() > 0);
                    if(rFootstepSound) AudioManager::Fetch()->PlaySound(rFootstepSound);
                }
                //--Walking SFX. Most of the time there isn't one, but certain surfaces may have them.
                else if(!rActor->mRanLastTick && tAmountMovedLastTick >= 1.0f)
                {
                    char *rFootstepSound = rActor->GetWalkFootstepSFX(mFollowEntityIDList->GetListSize() > 0);
                    if(rFootstepSound) AudioManager::Fetch()->PlaySound(rFootstepSound);
                }

                //--If the player ran last tick, drain stamina. There must be at least one hostile entity on the map anywhere.
                if(rActor->mRanLastTick && tPlayerPressedMove && tAmountMovedLastTick >= 1.0f && xEntitiesDrainStamina)
                {
                    //--Decrement.
                    mPlayerStamina = mPlayerStamina - STAMINA_CONSUME_TICK;

                    //--At zero, block running.
                    if(mPlayerStamina < 1.0f)
                    {
                        mBlockRunningToRegen = true;
                    }
                }
                //--Stamina regen while stationary.
                else if(!tPlayerPressedMove && tAmountMovedLastTick < 1.0f)
                {
                    //--Regen.
                    mPlayerStamina = mPlayerStamina + STAMINA_REGEN_STANDING;

                    //--If the player's stamina is under the threshold while not running, block running until it regens.
                    if(mPlayerStamina < STAMINA_RUN_LOWER) mBlockRunningToRegen = true;
                }
                //--Stamina regen while walking.
                else
                {
                    //--Regen.
                    mPlayerStamina = mPlayerStamina + STAMINA_REGEN_TICK;

                    //--If the player's stamina is under the threshold while not running, block running until it regens.
                    if(mPlayerStamina < STAMINA_RUN_LOWER) mBlockRunningToRegen = true;
                }

                //--Stamina clamps.
                if(mPlayerStamina < 0.0f) mPlayerStamina = 0.0f;
                if(mPlayerStamina > STAMINA_MAX) mPlayerStamina = STAMINA_MAX;

                ///--[Field Abilities Tick]
                //--Field ability cooldowns.
                AdvCombat *rCombat = AdvCombat::Fetch();
                for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
                {
                    FieldAbility *rAbility = rCombat->GetFieldAbility(i);
                    if(!rAbility) continue;

                    int tCooldown = rAbility->GetCooldown();
                    if(tCooldown > 0) rAbility->SetCooldownCur(tCooldown - 1);
                }

                //--Remove the blocking flag if the player's stamina goes over this constant.
                if(mBlockRunningToRegen && mPlayerStamina >= STAMINA_RUN_ALLOW) mBlockRunningToRegen = false;

                ///--[Party Members Follow]
                //--If the position is different from the 0th element on the list, shift them down one.
                if(tPlayerPressedMove)
                {
                    //--Re-randomize the leader's idle animations.
                    rActor->RandomizeIdleTimer();

                    //--Move the followers.
                    mPlayerStoppedMovingTimer = 0;
                    MovePartyMembers();
                }
                ///--[Party Membders Idle]
                //--Player did not move. All followers stop movement.
                else
                {
                    //--Inform the leader that they may run idle timers now.
                    rActor->IdleUpdate();

                    //--If this is the zeroth tick that the player is not moving, issue an order for all party
                    //  members to stop their movement.
                    if(mPlayerStoppedMovingTimer == 0)
                    {
                        //--Iterate.
                        int i = 0;
                        TilemapActor *rFollower = LocateFollowingActor(i);
                        while(rFollower)
                        {
                            //--Animation.
                            rFollower->StopMoving();

                            //--Next.
                            i ++;
                            rFollower = LocateFollowingActor(i);
                        }
                    }

                    //--Increment.
                    mPlayerStoppedMovingTimer ++;

                    //--All party members perform their idle update.
                    int i = 0;
                    TilemapActor *rFollower = LocateFollowingActor(i);
                    while(rFollower)
                    {
                        //--Animation.
                        rFollower->IdleUpdate();

                        //--Next.
                        i ++;
                        rFollower = LocateFollowingActor(i);
                    }
                }

                ///--[Invisibility Zones]
                //--Check if the actor is in any of the invisible zones.
                CheckActorAgainstInvisZones(rActor);

                ///--[Exits and Triggers]
                //--Check if the actor in question moved into any exits. If so, set the variables for those exits. This is
                //  not an emulation.
                DebugPrint("Checking exits and triggers.\n");
                if(CheckActorAgainstExits(rActor, false))
                {
                }
                //--If that didn't go through, check the actor against triggers. This is not an emulation, the trigger
                //  should execute.
                else if(CheckActorAgainstTriggers(rActor, false))
                {
                }
            }
        }
    }

    ///--[Rub Activation]
    //--Check if the player rubbed anything to activate it this tick.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Check.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            if(rActor->ShouldRubActivate())
            {
                rActor->HandleRubActivation();
                rEntityList->PopIterator();
                break;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    ///--[Lighting Post-Update]
    //--Update entity lighting positions after entities have moved.
    if(mAreLightsActive) UpdateEntityLightPositions();

    ///--[Post-Tick Flags]
    //--Decrement this timer.
    if(mCannotOpenMenuTimer > 0) mCannotOpenMenuTimer --;

    //--At the end of a normal control tick, reset this flag. When Enemies go to update, they can flip
    //  it back to true. It should be flipped AFTER the player uses it.
    xEntitiesDrainStamina = false;

    //--Debug.
    DebugPop("AdventureLevel: Update completes at end of function.\n");
}
