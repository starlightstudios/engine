//--Base
#include "CarnUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"

///--[Debug]
//#define CARNUIEQUIPREPLACE_DEBUG
#ifdef CARNUIEQUIPREPLACE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
bool CarnUIEquip::ActivateReplacement()
{
    ///--[Documentation]
    //--Same as the base version, but also updates the scrollbar.
    if(!AdvUIEquip::ActivateReplacement()) return false;

    //--Clear cursor.
    mReplacementCursor = -1;
    ResetString(mHighlightName, NULL);
    delete mDescription;
    mDescription = NULL;

    //--Update scrollbar cap.
    ScrollbarPack *rScrollbarPack = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
    if(rScrollbarPack) rScrollbarPack->mMaxSkip = mValidReplacementsList->GetListSize() - cxCarnScrollPage;

    ///--[Proficiencies]
    //--Clear the list.
    mrReplacementProficiencyImages->ClearList();

    //--Fast-access pointers.
    AdvCombat *rCombat = AdvCombat::Fetch();
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity) return true;

    //--For each element on the replacement list, add a pointer to its bitmap.
    AdventureItem *rCheckItem = (AdventureItem *)mValidReplacementsList->PushIterator();
    while(rCheckItem)
    {
        //--Get the icon if it exists.
        StarBitmap *rProficiencyImg = ResolveProficiencyIconForItem(rAdvEntity, rCheckItem);

        //--If the icon does not exist, put a dummy in. This will not render but does maintain spacing.
        if(!rProficiencyImg)
        {
            mrReplacementProficiencyImages->AddElementAsTail("X", CarnImages.rFrame_Base);
        }
        //--Exists, store.
        else
        {
            mrReplacementProficiencyImages->AddElementAsTail("X", rProficiencyImg);
        }

        //--Next.
        rCheckItem = (AdventureItem *)mValidReplacementsList->AutoIterate();
    }

    ///--[Finish]
    //--Legal.
    return true;
}

///========================================== Update ==============================================
void CarnUIEquip::UpdateReplacement()
{
    ///--[Documentation]
    //--Updates replacement mode. The user can exit the mode by cancelling, or select a piece of
    //  equipment to replace the selected one.
    ControlManager *rControlManager = ControlManager::Fetch();
    DebugPush(true, "CarnUIEquip:UpdateReplacement() - Begin.\n");

    ///--[Timers]
    ///--[Sub-Handlers]
    //--Scrollbars update before anything else.
    if(UpdateScrollbars()) return;

    ///--[Mouse Over]
    int tReply = CheckRepliesWithin(cxReplaceBoxMin, cxReplaceBoxMax);
    if(tReply != -1)
    {
        //--Set.
        int tOldCursor = mReplacementCursor;
        mReplacementCursor = (tReply - cxReplaceBoxMin) + mReplacementSkip;

        //--Check if the replacement slot is past the edge of the list.
        if(mReplacementCursor >= mValidReplacementsList->GetListSize()) mReplacementCursor = -1;

        //--SFX, description.
        if(tOldCursor != mReplacementCursor && mReplacementCursor != -1)
        {
            //--Locate the item in question, set description.
            AdventureItem *rReplacementItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
            if(rReplacementItem)
            {
                //--Flags.
                ResetString(mHighlightName, rReplacementItem->GetDisplayName());
                delete mDescription;
                mDescription = CreateDescriptionListFromString(rReplacementItem->GetDescription(), CarnImages.rFont_Description, 440.0f);

                //--Special: If it's the unequip item, clear.
                if(rReplacementItem == AdventureInventory::xrDummyUnequipItem) ResetString(mHighlightName, NULL);
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Left Click / Activate]
    //--Selects the entry under the cursor.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Diagnostics.
        DebugPrint("Received left click.\n");
        HandleReplaceLeftClick();

        //--Diagnostics.
        DebugPop("Finished left click.\n");
        return;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Returns to selection mode.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        mIsReplacing = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        DebugPop("Finished right click.\n");
        return;
    }

    ///--[Finish Up]
    DebugPop("Finished with no inputs.\n");
}
void CarnUIEquip::HandleReplaceLeftClick()
{
    ///--[Documentation]
    //--When selecting an item to replace the given item with, handles left clicks.

    ///--[Curse Replace Warning]
    //--If the player attempts to unequip a cursed item, a popup warns them that this will inflict
    //  influence damage. This brings up confirm/cancel buttons.
    if(mConfirmUnequipCursed)
    {
        //--Get entity.
        AdvCombat *rCombat = AdvCombat::Fetch();
        if(!rCombat) return;
        AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor);
        if(!rAdvEntity) return;

        //--Check for a reply.
        int tReply = CheckRepliesWithin(cxConfirm, cxCancel);

        //--Confirm the unequip/replace action.
        if(tReply == cxConfirm)
        {
            //--Exit mode.
            mConfirmUnequipCursed = false;

            //--Get the item in the other list.
            const char *rSlotName = rAdvEntity->GetNameOfEquipmentSlot(mEquipmentCursor);
            AdventureItem *rItemInList = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mConfirmStoredSlot);
            if(rItemInList)
            {
                bool tAcceptedItem = rAdvEntity->EquipItemToSlotNoGems(rSlotName, rItemInList);
                if(tAcceptedItem) AdventureInventory::Fetch()->LiberateItemP(rItemInList);
            }

            //--Exit this mode.
            mIsReplacing = false;
            mEquipmentCursor = -1;

            //--Order the calling object to handle the close code. This will both handle changing items
            //  and also damage a character's influence by 1.
            SetCloseCode("Damage Influence");

            //--Store who is going to get the influence damage.
            SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/DungeonPrep/Party/sInfluenceDamageTo");
            if(rVariable) ResetString(rVariable->mAlpha, rAdvEntity->GetName());

            //--Rebuild proficiency icons.
            RecheckProficienciesForCharacter(mCharacterCursor);

            //--SFX.
            AudioManager::Fetch()->PlaySound("World|TakeItem");
        }
        //--Cancel.
        else
        {
            mConfirmUnequipCursed = false;
            AudioManager::Fetch()->PlaySound("World|Select");
        }
        return;
    }

    ///--[Scan Replacement List]
    //--Setup.
    AdvCombat *rCombat = AdvCombat::Fetch();
    if(!rCombat) return;

    //--Make sure the character exists.
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity) return;

    //--The hitboxes map onto the replacement list. Upon clicking a hitbox, make that item replace
    //  the item in the replacement slot.
    int tReplacementSlot = -1;

    //--The selection list and replacement list use the same hitboxes, offset slightly.
    HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
    while(rReplyPack)
    {
        //--Fast-access pointers.
        int tIndex = rReplyPack->mIndex;

        //--Resolve which inventory item was being selected.
        int tInventorySlot = tIndex - cxReplaceBoxMin + mReplacementSkip;

        //--If it was within range of item boxes, begin replacement mode with that item cursor.
        if(tIndex >= cxReplaceBoxMin && tIndex <= cxReplaceBoxMax)
        {
            tReplacementSlot = tInventorySlot;
        }
        //--If it was a scrollbar hit, reset the replacement slot.
        else if(tIndex >= cxScrollMin && tIndex <= cxScrollMax)
        {
            tReplacementSlot = -1;
            mHitboxReplyList->PopIterator();
            break;
        }

        //--Next.
        rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
    }

    ///--[Replace]
    //--If the replacement slot is not -1, issue a replacement order.
    if(tReplacementSlot != -1)
    {
        //--Check if the current item is cursed. If an item is cursed, the player is warned before
        //  unequipping it. If the slot is empty, skip this check.
        const char *rSlotName = rAdvEntity->GetNameOfEquipmentSlot(mEquipmentCursor);
        SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/DungeonPrep/Party/iIsInDungeonNow");
        AdventureItem *rEquipmentInSlot = rAdvEntity->GetEquipmentBySlotS(rSlotName);
        if(rEquipmentInSlot && rVariable && rVariable->mNumeric >= 1.0f)
        {
            //--Get tags.
            int tCursedTags = rEquipmentInSlot->GetTagCount("Is Cursed");

            //--It's more than zero, start warning mode.
            if(tCursedTags > 0)
            {
                mConfirmUnequipCursed = true;
                mConfirmStoredSlot = tReplacementSlot;
                AudioManager::Fetch()->PlaySound("World|Select");
                return;
            }
        }

        //--Get the item in the other list.
        AdventureItem *rItemInList = (AdventureItem *)mValidReplacementsList->GetElementBySlot(tReplacementSlot);
        if(rItemInList)
        {
            bool tAcceptedItem = rAdvEntity->EquipItemToSlotNoGems(rSlotName, rItemInList);
            if(tAcceptedItem) AdventureInventory::Fetch()->LiberateItemP(rItemInList);
        }

        //--Exit this mode.
        mIsReplacing = false;
        mEquipmentCursor = -1;

        //--Order the calling object to handle the close code.
        SetCloseCode("Change Equipment");

        //--Rebuild proficiency icons.
        RecheckProficienciesForCharacter(mCharacterCursor);

        //--SFX.
        AudioManager::Fetch()->PlaySound("World|TakeItem");
        return;
    }

    ///--[Toggle Description]
    int tReply = CheckRepliesWithin(cxBtnToggleDesc, cxBtnToggleDesc);
    if(tReply != -1)
    {
        mIsDescriptionMode = !mIsDescriptionMode;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}

///========================================== Drawing =============================================
void CarnUIEquip::RenderReplaceMode()
{
    ///--[Documentation]
    //--Renders a modified status screen, showing the selected item and its possible replacements.
    DebugPush(true, "CarnUIEquip:RenderReplaceMode() - Begin.\n");
    if(!mIsReplacing) { DebugPop("Not in replacement mode. Stopping.\n"); return; }

    //--Frames.
    CarnImages.rFrame_Base->Draw();

    //--Resolve the acting entity.
    AdvCombat *rCombat = AdvCombat::Fetch();
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity) { DebugPop("No active entity found. Stopping.\n"); return; }

    ///--[Replacement Column]
    //--Render the item we're replacing.
    AdventureItem *rReplaceItem = rAdvEntity->GetEquipmentBySlotI(mEquipmentCursor);
    if(rReplaceItem)
    {
        AdvImages.rFont_Statistics->DrawTextArgs(427.0f, 147.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Replacing %s", rReplaceItem->GetDisplayName());
    }
    //--Render 'Nothing'
    else
    {
        AdvImages.rFont_Statistics->DrawTextArgs(427.0f, 147.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select Equipment");
    }

    //--Render the available replacements.
    int i = 0;
    int tRenders = 0;
    float cVSpacing = 37.0f;
    AdventureItem *rNewItem = (AdventureItem *)mValidReplacementsList->PushIterator();
    while(rNewItem)
    {
        //--Don't render below the skip.
        if(i < mReplacementSkip)
        {
            i ++;
            rNewItem = (AdventureItem *)mValidReplacementsList->AutoIterate();
            continue;
        }

        //--Get the name.
        char tBuf[128];

        //--This is the unequip item.
        if(rNewItem == AdventureInventory::xrDummyUnequipItem)
        {
            strcpy(tBuf, "Unequip");
        }
        //--If the item has gems, add those to the name.
        else if(rNewItem->GetSlottedGems() > 0)
        {
            sprintf(tBuf, "%s (+%i)", rNewItem->GetDisplayName(), rNewItem->GetSlottedGems());
        }
        //--Just put the name in the buffer.
        else
        {
            strcpy(tBuf, rNewItem->GetDisplayName());
        }

        //--Render.
        RenderEquipmentEntry(285.0f, 202.0f + (cVSpacing * tRenders), tBuf, rNewItem->GetIconImage());

        //--Get the proficiency image. If it's the dummy, don't render it.
        StarBitmap *rProficiencyImage = (StarBitmap *)mrReplacementProficiencyImages->GetElementBySlot(i);
        if(rProficiencyImage != CarnImages.rFrame_Base)
        {
            rProficiencyImage->Draw(285.0f + 273.0f, 202.0f + (cVSpacing * tRenders));
        }

        //--Hit cap.
        tRenders ++;
        if(tRenders >= cxCarnScrollPage)
        {
            mValidReplacementsList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rNewItem = (AdventureItem *)mValidReplacementsList->AutoIterate();
    }

    //--Scrollbar.
    if(mValidReplacementsList->GetListSize() > cxCarnScrollPage)
    {
        StandardRenderScrollbar(mReplacementSkip, cxCarnScrollPage, mValidReplacementsList->GetListSize(), 245.0f, 330.0f, false, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    ///--[Description]
    //--Render if it exists.
    if(mDescription && mIsDescriptionMode && mHighlightName)
    {
        //--Constants.
        float cTitleX = 709.0f;
        float cTitleY = 101.0f;
        float cDescX  = 709.0f;
        float cDescY  = 147.0f;
        float cDescVSpacing = 30.0f;

        //--Item name.
        CarnImages.rFont_Header->DrawText(cTitleX, cTitleY, 0, 1.0f, mHighlightName);

        //--Subroutine.
        RenderDescription(cDescX, cDescY, cDescVSpacing, mDescription, CarnImages.rFont_Description);
    }
    ///--[Statistics]
    else if(!mIsDescriptionMode)
    {
        //--Backings.
        CarnImages.rOverlay_StatBase->Draw();
        CarnImages.rOverlay_StatDetail->Draw();

        //--Fixed labels.
        AdvImages.rFont_Heading->DrawText(935.0f, 134.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Statistics");

        //--Constants.
        float cTop = 200.0f;
        float cHei = 39.0f;

        //--Variables.
        RenderStatistic(cTop + (cHei *  0.0f),    "HP",         STATS_HPMAX);
        RenderStatistic(cTop + (cHei *  1.0f),    "SP",         STATS_MPMAX);
        RenderStatistic(cTop + (cHei *  2.0f),    "Attack",     STATS_ATTACK);
        RenderStatistic(cTop + (cHei *  3.0f),    "M. Atk.",    STATS_CPMAX); //Override in Carnation
        RenderStatistic(cTop + (cHei *  4.0f)- 3, "Defense",    STATS_RESIST_PROTECTION);
        RenderStatistic(cTop + (cHei *  5.0f)- 3, "M. Def.",    STATS_ACCURACY); //Override in Carnation
        RenderStatistic(cTop + (cHei *  6.0f)- 3, "Initiative", STATS_INITIATIVE);
        RenderStatistic(cTop + (cHei *  7.0f)- 4, "Strength",   STATS_RESIST_OBSCURE); //Override in Carnation
        RenderStatistic(cTop + (cHei *  8.0f)- 5, "Intellect",  STATS_FREEACTIONGEN); //Override in Carnation
        RenderStatistic(cTop + (cHei *  9.0f)- 6, "Endurance",  STATS_FREEACTIONMAX); //Override in Carnation
        RenderStatistic(cTop + (cHei * 10.0f)- 5, "Luck",       STATS_EVADE); //Override in Carnation

        //AdvImages.rFont_Statistics->DrawText    (cLft, cTop + (cHei *10.0f)-5, cFlagL, 1.0f, "Luck");
        //AdvImages.rFont_Statistics->DrawTextArgs(cRgt, cTop + (cHei *10.0f)-5, cFlagR, 1.0f, "%i", rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, 9));
    }

    ///--[Toggle Button]
    TwoDimensionReal *rToggleHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("ToggleDesc");
    if(rToggleHitbox)
    {
        CarnImages.rFrame_Button->Draw(rToggleHitbox->mLft, rToggleHitbox->mTop);
        CarnImages.rFont_Statistics->DrawText(rToggleHitbox->mXCenter, rToggleHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Toggle Stats");
    }

    ///--[Finish Up]
    DebugPop("Finished normally.\n");
}
void CarnUIEquip::RenderStatistic(float pY, const char *pString, int pStatisticCode)
{
    ///--[Documentation]
    //--Renders a comparison statistic. This renders the stat name, the current value, and the value after replacement.

    ///--[Setup]
    //--Colors.
    StarlightColor cBlack, cGreen, cRed;
    cBlack.SetRGBAF(0.00f, 0.00f, 0.00f, 1.00f);
    cGreen.SetRGBAF(0.30f, 1.00f, 0.50f, 1.00f);
    cRed.  SetRGBAF(0.80f, 0.30f, 0.25f, 1.00f);

    //--Get the entity in question. The entity must exist.
    AdvCombatEntity *rAdvEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity) return;

    //--Set variables.
    int tStatBefore   = rAdvEntity->GetStatistic(ADVCE_STATS_FINAL, pStatisticCode);
    int tStatFromItem = 0;
    int tStatToItem   = 0;

    //--Get the item that is currently equipped and being replaced. If it does not exist,
    //  the base value for the stat is 0.
    AdventureItem *rReplacingItem = rAdvEntity->GetEquipmentBySlotI(mEquipmentCursor);
    if(rReplacingItem)
    {
        tStatFromItem = rReplacingItem->GetStatistic(pStatisticCode);
        tStatToItem = tStatFromItem;
    }

    //--Get the item currently highlighted. If it does not exist, the value is the replacing item's,
    //  which can be zero.
    AdventureItem *rReplacementItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
    if(rReplacementItem)
    {
        tStatToItem = rReplacementItem->GetStatistic(pStatisticCode);
    }

    //--If not in replacement mode, set the stat mods to zero.
    if(!mIsReplacing)
    {
        tStatFromItem = 0;
        tStatToItem = 0;
    }

    ///--[Rendering]
    //--Flags.
    float cFlagL = 0;
    float cFlagR = SUGARFONT_RIGHTALIGN_X;
    int tStatAfterReplace = tStatBefore - tStatFromItem + tStatToItem;

    //--Render. Base stat is always black.
    cBlack.SetAsMixer();
    CarnImages.rFont_Statistics->DrawText    ( 787.0f, pY, cFlagL, 1.0f, pString);
    CarnImages.rFont_Statistics->DrawTextArgs( 930.0f, pY, cFlagL, 1.0f, "%i", tStatBefore);

    //--If the stat is higher, display as green:
    if(tStatAfterReplace > tStatBefore)
    {
        cGreen.SetAsMixer();
    }
    //--If the stat is lower, display as red:
    else if(tStatAfterReplace < tStatBefore)
    {
        cRed.SetAsMixer();
    }

    //--Render.
    CarnImages.rFont_Statistics->DrawText    ( 995.0f, pY, cFlagL, 1.0f, "->");
    CarnImages.rFont_Statistics->DrawTextArgs(1088.0f, pY, cFlagR, 1.0f, "%i", tStatAfterReplace);

    ///--[Clear]
    StarlightColor::ClearMixer();
}
