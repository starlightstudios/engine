-- |[ ===================================== Up Folder Paths ==================================== ]|
--At program boot, checks every folder in the "Engine/../" directory. This is mostly used for developers
-- who are making mods and the like and don't want all the games in the same repository.

-- |[Diagnostics]|
local bDiagnostics = fnAutoCheckDebugFlag("Startup: Populate Paths")
Debug_PushPrint(bDiagnostics, "Startup: Up Folder Paths.\n")

--Exceptions. Any directory found on this list will not be executed.
local saExceptionsList = {"build archive", "builds", "guide", "physfs", "sound effects", "stringtyrantartpack", "sugarlumps"}

-- |[ ====================================== Primary Paths ===================================== ]|
--Build a list of paths that are available. This is the one-folder-up set.
local saOneFolderUp = {}

-- |[Iteration]|
--Open the directory.
FS_Open("../", false)
local sPath = FS_Iterate()
    
    --Iterate across the paths.
	while(sPath ~= "NULL") do

        --Setup.
        local bBypass = false
        local sFolderName = fnResolveFolderName(sPath)

        --Check if the string is on the exclusion list:
        for i = 1, #saExceptionsList, 1 do
            if(string.lower(sFolderName) == saExceptionsList[i]) then
                bBypass = true
                break
            end
        end

		--Check the last byte for '/'. This indicates it's a directory. Ignore files.
		local iLength = string.len(sPath)
		if(string.byte(sPath, iLength) ~= string.byte("/")) then
            bBypass = true
		end

        --If not bypassing, add it to the list.
        if(bBypass == false) then
            table.insert(saOneFolderUp, sPath)
			Debug_Print("Candidate: " .. sPath .. "XGameInfo.lua" .. "\n")
        end

        --Next.
		sPath = FS_Iterate()
    end
    
--Clean up.
FS_Close()

-- |[Debug]|
if(false) then
    io.write("One folder up: \n")
    for i = 1, #saOneFolderUp, 1 do
        io.write(" " .. saOneFolderUp[i] .. "\n")
    end
end

-- |[ ==================================== Secondary Paths ===================================== ]|
--Each of the secondary folders can contain its own "Games/" directory. If it does, check for the
-- usual XGameInfo.lua in each of those folders.
for i = 1, #saOneFolderUp, 1 do

    --Open.
    FS_Open(saOneFolderUp[i] .. "Games/", false)
    local sPath = FS_Iterate()
    
        --Iterate across the paths.
        while(sPath ~= "NULL") do
            
            --Setup.
            local bBypass = false
            local sFolderName = fnResolveFolderName(sPath)
            
            --Check the last byte for '/'. This indicates it's a directory. Ignore files.
            local iLength = string.len(sPath)
            if(string.byte(sPath, iLength) ~= string.byte("/")) then
                bBypass = true
            end

            --If not bypassing, execute the game info script.
            if(bBypass == false) then
                if(FS_Exists(sPath .. "XGameInfo.lua") == true) then
					Debug_Print("Executing candidate: " .. sPath .. "XGameInfo.lua" .. "\n")
                    LM_ExecuteScript(sPath .. "XGameInfo.lua")
                end
            end
            
            --Next.
            sPath = FS_Iterate()
        end

    --Clean up.
    FS_Close()

end

--Diagnostics.
Debug_PopPrint("Up Folder Paths completes.\n")
