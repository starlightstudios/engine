//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureDebug.h"
#include "AdventureLevelGenerator.h"
#include "AdventureMenu.h"
#include "KPopDanceGame.h"
#include "PuzzleFight.h"
#include "RayCollisionEngine.h"
#include "RunningMinigame.h"
#include "ShakeMinigame.h"
#include "TileLayer.h"
#include "TilemapActor.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "GlDfn.h"
#include "Global.h"
#include "OpenGLMacros.h"
#if defined _TARGET_OS_MAC_
#include "gl_ext_defs.h"
#endif

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"
#include "MapManager.h"

///--[Local Definitions]
///--[Debug]
//#define AL_RENDER_DEBUG
#ifdef AL_RENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Drawing =============================================
void AdventureLevel::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void AdventureLevel::Render()
{
    ///--[Setup]
    //--Flip this flag.
    MapManager::xHasRenderedMenus = true;
    if(!Images.mIsReady) return;

    //--Error check.
    if(mCurrentScale < 0.01f) return;

    ///--[Minigame Rendering]
    //--If the running minigame exists and is fully opaque, render it and stop here.
    if(mRunningMinigame && mRunningMinigame->IsFullyOpaque())
    {
        mRunningMinigame->Render();
        return;
    }

    ///--[Debug]
    //--Debug.
    DebugPush(true, "AdventureLevel: Render begins.\n");

    ///--[Diagnostics]
    DebugManager::RenderVisualTrace("ADVL");

    ///--[Screen Shaking]
    //--Screen shake.
    float cShakeXOff = 0.0f;
    float cShakeYOff = 0.0f;
    if(mScreenShakeTimer > 0)
    {
        //--Rolls.
        float cRadius = 0.7f;
        float cAngle = (float)(rand() % 360) / 360.0f * 3.1415925f * 2.0f;
        cShakeXOff = cosf(cAngle) * cRadius;
        cShakeYOff = sinf(cAngle) * cRadius;
    }

    ///--[Water Rendering]
    //--Requires OpenGL 3.0 or higher, since it uses framebuffers. Can be disabled.
    if(mIsUnderwaterMode)
    {
        //--Order the program to target the framebuffer.
        sglBindFramebuffer(GL_FRAMEBUFFER, mUnderwaterFramebuffer);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        //--Error check.
        if(sglCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            fprintf(stderr, "Error! Could not set up the framebuffers.\n");
            sglBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
        //--Proceed.
        else
        {
            glViewport(0, 0, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        }
    }

    ///--[Background Rendering]
    //--Only renders if a background exists. Most levels don't use this!
    DebugManager::RenderVisualTrace("BG");
    if(rBackgroundImg)
    {
        //--Compute position.
        float tRenderX = mBackgroundStartX + (mCameraDimensions.mLft * mBackgroundScrollX);
        float tRenderY = mBackgroundStartY + (mCameraDimensions.mTop * mBackgroundScrollY);

        //--Render.
        glTranslatef(0.0f, 0.0f, -1.0f);
        rBackgroundImg->Draw(tRenderX, tRenderY);

        //--If the render position plus the width is less than the virtual canvas, tile it horizontally.
        if(tRenderX + rBackgroundImg->GetTrueWidth() < VIRTUAL_CANVAS_X)
        {
            rBackgroundImg->Draw(tRenderX + rBackgroundImg->GetTrueWidth(), tRenderY);
        }

        //--If the render position plus the height is less than the virtual canvas, tile it vertically.
        if(tRenderY + rBackgroundImg->GetTrueHeight() < VIRTUAL_CANVAS_Y)
        {
            rBackgroundImg->Draw(tRenderX, tRenderY + rBackgroundImg->GetTrueHeight());
        }

        //--Both cases are true:
        if(tRenderX + rBackgroundImg->GetTrueWidth() < VIRTUAL_CANVAS_X && tRenderY + rBackgroundImg->GetTrueHeight() < VIRTUAL_CANVAS_Y)
        {
            rBackgroundImg->Draw(tRenderX + rBackgroundImg->GetTrueWidth(), tRenderY + rBackgroundImg->GetTrueHeight());
        }

        //--Clean.
        glTranslatef(0.0f, 0.0f, 1.0f);
    }

    ///--[Underlays]
    //--Renders over the background and under the world.
    DebugManager::RenderVisualTrace("UND");
    glTranslatef(0.0f, 0.0f, -1.0f);
    for(int i = 0; i < mUnderlaysTotal; i ++)
    {
        //--Skip rendering if the alpha is 0.0f, or no image exists.
        if(!mUnderlayPacks[i].rImage || mUnderlayPacks[i].mAlphaCurrent <= 0.0f) continue;

        //--Set the alpha value.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, mUnderlayPacks[i].mAlphaCurrent);

        //--Fast-access variables.
        float cTexWid = mUnderlayPacks[i].rImage->GetTrueWidth();
        float cTexHei = mUnderlayPacks[i].rImage->GetTrueHeight();

        //--Determine the starting X/Y positions.
        float cStartX = mUnderlayPacks[i].mOffsetBaseX + (mCameraDimensions.mLft * mUnderlayPacks[i].mScrollFactorX);
        float cStartY = mUnderlayPacks[i].mOffsetBaseY + (mCameraDimensions.mTop * mUnderlayPacks[i].mScrollFactorY);
        float cTxL = (cStartX / cTexWid);
        float cTxT = (cStartY / cTexHei);
        float cTxR = (cStartX / cTexWid) + (CANX / cTexWid * mUnderlayPacks[i].mScaleFactor);
        float cTxB = (cStartY / cTexHei) - (CANY / cTexHei * mUnderlayPacks[i].mScaleFactor);

        //--Bind, and render.
        mUnderlayPacks[i].rImage->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxB); glVertex2f(0.0f, 0.0f);
            glTexCoord2f(cTxR, cTxB); glVertex2f(CANX, 0.0f);
            glTexCoord2f(cTxR, cTxT); glVertex2f(CANX, CANY);
            glTexCoord2f(cTxL, cTxT); glVertex2f(0.0f, CANY);
        glEnd();
    }
    glTranslatef(0.0f, 0.0f, 1.0f);

    ///--[Scaling]
    //--Scaling applies before camera but after background.
    glTranslatef(cShakeXOff, cShakeYOff, 0.0f);
    glScalef(mCurrentScale, mCurrentScale, 1.0f);

    ///--[Camera Handling]
    //--If the Camera is not currently locked, it will attempt to focus on the player's character.
    //  The camera usually gets locked as a result of cutscenes, but they can modify the locking state
    //  at any time with SetCameraLocking().
    //--The camera will automatically unlock if no cutscene is active.
    DebugPrint("Updating camera position.\n");
    UpdateCameraPosition();

    //--Camera.
    glTranslatef(-mCameraDimensions.mLft, -mCameraDimensions.mTop, 0.0f);
    DebugManager::RenderVisualTrace("CAM");

    ///--[Layer Rendering]
    //--Compute which subsection should be rendered. Clamping is done by the TileLayers.
    int tLft = (mCameraDimensions.mLft / TileLayer::cxSizePerTile) - 1;
    int tTop = (mCameraDimensions.mTop / TileLayer::cxSizePerTile) - 1;
    int tRgt = (mCameraDimensions.mRgt / TileLayer::cxSizePerTile) + 1;
    int tBot = (mCameraDimensions.mBot / TileLayer::cxSizePerTile) + 1;

    //--Patrol node debug.
    if(false)
    {
        RenderPatrolNodes();
    }

    ///--[Script Fading]
    //--Render the fade if it's flagged to be over the tiles but under the entities.
    DebugPrint("Render script fade.\n");
    RenderScriptFade(SCRIPT_FADE_UNDER_CHARACTERS);

    ///--[Lights]
    //--Activate the lighting shader.
    if(mAreLightsActive) RenderLights();

    ///--[Sub-Rendering]
    //--Sub-functions. These render various objects or areas.
    if(Images.mIsReady)
    {
        //--Basic rendering.
        DebugPush(true, "Rendering sub components.\n");
        RenderEntities(false);
        RenderTilemap(tLft, tTop, tRgt, tBot);
        RenderDoors();
        RenderClimbables();
        RenderChests();
        RenderExits();
        RenderSwitches();
        RenderSavePoints();
        RenderViewcones();
        RenderPerMapAnimations();
        RenderEntityUILayer();
        DebugPop("Done rendering sub components.\n");
    }

    ///--[Puzzles]
    //--If it exists, it gets rendering here.
    if(mPuzzleFightGame) mPuzzleFightGame->RenderWorld();

    ///--[Clean]
    //--Un-set underwater effect rendering.
    if(mIsUnderwaterMode)
    {
        sglBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(DisplayManager::xHorizontalOffset, DisplayManager::xVerticalOffset, DisplayManager::xViewportW, DisplayManager::xViewportH);
    }

    //--Deactivate the lighting/underwater shader.
    DisplayManager::Fetch()->ActivateProgram(NULL);

    //--Clean.
    DebugPrint("Cleaning.\n");
    TilemapActor::xAllowRender = false;
    glTranslatef(mCameraDimensions.mLft, mCameraDimensions.mTop, 0.0f);

    ///--[Rocks Overlay]
    //--Falling rocks. Ignores the camera position when rendering.
    FallingRockPack *rRock = (FallingRockPack *)mFallingRockList->SetToHeadAndReturn();
    while(rRock)
    {
        //--Check image.
        if(rRock->rImage)
        {
            rRock->rImage->Draw(rRock->mX, rRock->mY);
        }

        //--Next.
        rRock = (FallingRockPack *)mFallingRockList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Underwater Framebuffer]
    //--If underwater, render the framebuffer.
    if(mIsUnderwaterMode)
    {
        //--Place the texture in slot 1. The shader looks for it there.
        sglActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mUnderwaterTexture);
        sglActiveTexture(GL_TEXTURE0);

        //--Activate shader.
        if(DisplayManager::Fetch()->ActivateProgram("AdventureLevel Underwater"))
        {
            //--Setup.
            GLint cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
            ShaderUniform1i(cShaderHandle, Global::Shared()->gTicksElapsed, "uTimer");

            //--Render.
            glScalef(1.0f / mCurrentScale, 1.0f / mCurrentScale, 1.0f);
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(            0.0f,             0.0f);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
            glEnd();
            glScalef(mCurrentScale, mCurrentScale, 1.0f);

            //--Clean.
            DisplayManager::Fetch()->ActivateProgram(NULL);
        }

        //--Remove the Texture1 case.
        sglActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        sglActiveTexture(GL_TEXTURE0);
    }

    ///--[Clean]
    //--Clean the mixer.
    StarlightColor::ClearMixer();
    glTranslatef(-mCameraDimensions.mLft, -mCameraDimensions.mTop, 0.0f);
        RenderEntities(true);
    glTranslatef(mCameraDimensions.mLft, mCameraDimensions.mTop, 0.0f);
    TilemapActor::xAllowRender = false;

    ///--[Unscale]
    //--Undo scaling. This is done *after* the overlay renders.
    glScalef(1.0f / mCurrentScale, 1.0f / mCurrentScale, 1.0f);
    glTranslatef(-cShakeXOff, -cShakeYOff, 0.0f);

    ///--[Overlays]
    //--If set, render an overlay over the world but under the UI.
    DebugManager::RenderVisualTrace("OVER");
    if(!OptionsManager::Fetch()->GetOptionB("Disallow World Overlays"))
    {
        glDepthMask(false);
        for(int i = 0; i < mOverlaysTotal; i ++)
        {
            //--Skip rendering if the alpha is 0.0f, or no image exists.
            if(!mOverlayPacks[i].rImage || mOverlayPacks[i].mAlphaCurrent <= 0.0f)
            {
                continue;
            }

            //--Set the alpha value.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, mOverlayPacks[i].mAlphaCurrent);

            //--Fast-access variables.
            float cTexWid = mOverlayPacks[i].rImage->GetTrueWidth();
            float cTexHei = mOverlayPacks[i].rImage->GetTrueHeight();

            //--Determine the starting X/Y positions.
            float cCenterX = (mCameraDimensions.mLft + mCameraDimensions.mRgt) / 2.0f;
            float cCenterY = (mCameraDimensions.mTop + mCameraDimensions.mBot) / 2.0f;
            float cStartX = mOverlayPacks[i].mOffsetBaseX + (cCenterX * mOverlayPacks[i].mScrollFactorX);
            float cStartY = mOverlayPacks[i].mOffsetBaseY + (cCenterY * mOverlayPacks[i].mScrollFactorY);
            float cTxCX = (cStartX / cTexWid);
            float cTxCY = (cStartY / cTexHei);
            float cTxL = cTxCX - (CANX / cTexWid * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);
            float cTxT = cTxCY + (CANY / cTexHei * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);
            float cTxR = cTxCX + (CANX / cTexWid * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);
            float cTxB = cTxCY - (CANY / cTexHei * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);

            //--Bind, and render.
            mOverlayPacks[i].rImage->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(cTxL, cTxB); glVertex2f(0, 0);
                glTexCoord2f(cTxR, cTxB); glVertex2f(CANX*1.0f, 0);
                glTexCoord2f(cTxR, cTxT); glVertex2f(CANX*1.0f, CANY*1.0f);
                glTexCoord2f(cTxL, cTxT); glVertex2f(0, CANY*1.0f);
            glEnd();
        }
        glDepthMask(true);
    }
    StarlightColor::ClearMixer();
    DebugPrint("Overlays done.\n");

    ///--[GUI]
    //--Stamina bar setup. Only renders if the vis timer is above zero.
    DebugManager::RenderVisualTrace("WUI");
    RenderWorldUI();
    RenderRegionOverlay();
    DebugPrint("Stamina bar done.\n");

    //--Particles.
    AuraParticle *rParticle = (AuraParticle *)mAuraParticleList->PushIterator();
    while(rParticle)
    {
        //--Render.
        rParticle->Render();

        //--Next.
        rParticle = (AuraParticle *)mAuraParticleList->AutoIterate();
    }
    StarlightColor::ClearMixer();

    ///--[Puzzles]
    //--If it exists, it gets rendering here.
    if(mPuzzleFightGame) mPuzzleFightGame->RenderUI();

    ///--[Objectives]
    //--Doesn't render while control is locked out.
    if(mObjectivesList->GetListSize() > 0 && mControlLockTimer < 1)
    {
        float tYPos = 27.0f;
        ObjectivePack *rObjectivePtr = (ObjectivePack *)mObjectivesList->PushIterator();
        while(rObjectivePtr)
        {
            //--Render it.
            if(!rObjectivePtr->mIsComplete)
            {
                Images.Data.rUIFont->DrawText(0.0f, tYPos, 1.0f, 0, rObjectivePtr->mDisplayName);
            }
            else
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
                Images.Data.rUIFont->DrawTextArgs(0.0f, tYPos, 1.0f, 0, "%s: Complete", rObjectivePtr->mDisplayName);
                StarlightColor::ClearMixer();
            }

            //--Next.
            tYPos = tYPos + Images.Data.rUIFont->GetTextHeight();
            rObjectivePtr = (ObjectivePack *)mObjectivesList->AutoIterate();
        }
    }
    DebugPrint("Objectives done.\n");

    ///--[Borders]
    //--These render during cutscenes or when the player's control is locked.
    if(mControlLockTimer > 0 && !mPuzzleFightGame)
    {
        //--Calculations.
        float cRgt = VIRTUAL_CANVAS_X;
        float tPercent = mControlLockTimer / (float)AL_CONTROL_LOCK_TICKS;

        //--Mixer.
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, tPercent);
        glDisable(GL_TEXTURE_2D);

        //--Top border.
        float cTop = AL_CONTROL_LOCK_PIXELS * tPercent;
        glBegin(GL_QUADS);
            glVertex2f(0.0f, 0.0f);
            glVertex2f(cRgt, 0.0f);
            glVertex2f(cRgt, cTop);
            glVertex2f(0.0f, cTop);
        glEnd();

        //--Bottom border.
        cTop = VIRTUAL_CANVAS_Y - (AL_CONTROL_LOCK_PIXELS * tPercent);
        float cBot = VIRTUAL_CANVAS_Y;
        glBegin(GL_QUADS);
            glVertex2f(0.0f, cBot);
            glVertex2f(cRgt, cBot);
            glVertex2f(cRgt, cTop);
            glVertex2f(0.0f, cTop);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();
    }

    ///--[Notifications]
    RenderNotifications();

    ///--[Last Seed]
    //--If this is a randomly-generated level, render the seed value and floor information.
    if(mIsRandomLevel)
    {
        //--Rendering position is right below the stamina bar.
        float cXPos = 10.0f;
        float cYPos = Images.Data.rStaminaBarMiddle->GetHeight() + 8.0f;

        //--Render the floor number.
        int tFloorNumber = 0;
        SysVar *rFloorNumberVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor");
        if(rFloorNumberVar) tFloorNumber = (int)rFloorNumberVar->mNumeric;
        Images.Data.rUIFont->DrawTextArgs(cXPos, cYPos, 0, 1.0f, "Floor %i", tFloorNumber);
        cYPos = cYPos + Images.Data.rUIFont->GetTextHeight();

        //--Render the seed number.
        Images.Data.rUIFont->DrawTextArgs(cXPos, cYPos, 0, 1.0f, "Seed: %i", AdventureLevelGenerator::xLastSeed);
    }
    DebugPrint("Random Seed done.\n");

    ///--[Script Fade]
    //--Render the fade if it's flagged to be over the characters but under the GUI.
    DebugPrint("Render fade script under UI.\n");
    DebugManager::RenderVisualTrace("FADE");
    RenderScriptFade(SCRIPT_FADE_UNDER_UI);

    ///--[Major UI Objects]
    //--AdventureCombat handler. It always calls the render, but may render nothing if not active.
    DebugPrint("Render combat UI.\n");
    DebugManager::RenderVisualTrace("CBT");
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    rAdventureCombat->Render(false, 1.0f);

    //--Major animations. These render under the dialogue and also darken the whole screen.
    if(mIsMajorAnimationMode)
    {
        //--Darken the screen.
        StarlightColor cDarkenCol = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f);
        StarBitmap::DrawFullColor(cDarkenCol);

        //--Make sure the major animation actually exists.
        PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(mMajorAnimationName);
        if(!rAnimation || !rAnimation->mrImagePtrs || !rAnimation->mrImagePtrs[0])
        {
            //--Error goes here when detecting such things. Nominally this is logically impossible.
        }
        //--Rendering.
        else
        {
            //--Determine the centering point. Animations are expected to have all frames of constant sizes.
            //--The size can be modified by cWorldScale if the animation needs to be scaled up.
            float cSizePerFrameX = rAnimation->mrImagePtrs[0]->GetTrueWidth();
            float cSizePerFrameY = rAnimation->mrImagePtrs[0]->GetTrueWidth();

            //float cCalibration = 555.0f;
            float cWorldScale = 1.0f;
            float cRenderX = (VIRTUAL_CANVAS_X - (cSizePerFrameX * cWorldScale)) / 2.0f;
            float cRenderY = (VIRTUAL_CANVAS_Y - (cSizePerFrameY * cWorldScale)) / 2.0f;

            //--Determine which frame we're playing.
            float tFrame = rAnimation->mTimer / rAnimation->mTicksPerFrame;
            if(tFrame >= rAnimation->mTotalFrames) tFrame = rAnimation->mTotalFrames - 1;
            if(tFrame < 1) tFrame = 0;

            //--Convert to an integer so frames don't scroll.
            int tIntegerFrame = (int)tFrame;

            //--Compute the coordinates for this frame.
            rAnimation->mrImagePtrs[tIntegerFrame]->Draw(cRenderX, cRenderY);
        }
    }

    ///--[Dialogue GUI]
    //--Render WorldDialogue GUI if it's visible.
    DebugPrint("Render dialogue.\n");
    DebugManager::RenderVisualTrace("DIA");
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible() && !mIsRestSequence && !rAdventureCombat->IsActive())
    {
        rWorldDialogue->Render();
    }

    ///--[Menu]
    //--Adventure Menu, if it's visible.
    DebugPrint("Render menu.\n");
    DebugManager::RenderVisualTrace("MENU");
    if(mAdventureMenu->NeedsToRender()) mAdventureMenu->Render();

    ///--[Debug Menu]
    //--Debug menu renders over everything else if it is visible.
    DebugPrint("Render debug menu.\n");
    if(mDebugMenu->IsVisible())
    {
        mDebugMenu->Render();
    }
    //--If the DebugMenu was up the previous tick, render an overlay. This just prevents an annoying discontinuity
    //  when the menu is hidden.
    else if(mWasMenuUpLastTick)
    {
        AdventureDebug::RenderOverlay();
    }

    ///--[Transition Overlay]
    //--Overlay.
    DebugPrint("Render transition.\n");
    DebugManager::RenderVisualTrace("TRAN");
    if(mIsTransitionIn || mIsTransitionOut || mTransitionHoldTimer > -1)
    {
        //--Setup.
        float tAlpha = 0.0f;
        glDisable(GL_TEXTURE_2D);

        //--Compute alpha.
        if(mTransitionHoldTimer == -1)
        {
            //--Transitioning in uses a different alpha for the hold duration. Updates stop during this part as well.
            if(mIsTransitionIn)
            {
                //--Full alpha, ignore controls.
                if(mTransitionTimer < TRANSITION_HOLD_TICKS)
                {
                    tAlpha = 1.0f;
                }
                //--Fade alpha, controls restored.
                else
                {
                    tAlpha = 1.0f - ((mTransitionTimer - TRANSITION_HOLD_TICKS) / (float)TRANSITION_FADE_TICKS);
                }
            }
            //--Transition out.
            else
            {
                tAlpha = 0.0f;//mTransitionTimer / (float)TRANSITION_FADE_TICKS;
            }
        }
        //--Transition hold timer uses different math.
        else if(mTransitionHoldTimer > 0)
        {
            tAlpha = 1.0f - (mTransitionHoldTimer / (float)TRANSITION_HOLD);
        }
        //--At exactly zero, alpha is 1 on the hold timer.
        else if(mTransitionHoldTimer == 0)
        {
            tAlpha = 1.0f;
        }

        //--Render.
        glColor4f(0.0f, 0.0f, 0.0f, tAlpha);
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
    }

    ///--[Enemy Pulse Overlay]
    //--Enemy pulse transition: Fades the screen and shows a spinning animation.
    DebugPrint("Render pulse mode.\n");
    if(mIsPulseMode && PULSE_TICKS != 0)
    {
        //--Setup. Compute color. It becomes green if the player got an initiative bonus.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, 0.75f);
        if(rAdventureCombat->DoesPlayerHaveInitiative())
        {
            float cPercent = (mPulseTimer - 15) / (float)(PULSE_TICKS - 30);
            float cGreenliness = 0.5f * (1.0f - cPercent);
            glColor4f(0.0f, cGreenliness, cGreenliness - 0.30f, 0.75f);
        }

        //--Central positioning.
        glTranslatef(VIRTUAL_CANVAS_X * 0.5f, VIRTUAL_CANVAS_Y * 0.5f, 0.0f);

        //--Angle. Increases slowly over the run of the pulse.
        float cStartAngle = (mPulseTimer / ((float)PULSE_TICKS * 1.5f)) * 3.1415926f * 2.0f;
        if(mPulseTimer > 30) cStartAngle = cStartAngle + (((mPulseTimer-30) / ((float)PULSE_TICKS * 0.05f)) * 3.1415926f * 2.0f);

        //--There are ten triangles, each rotating at 36 degrees to one another.
        float tPercentage = EasingFunction::QuadraticIn(mPulseTimer, PULSE_TICKS);
        float cRadius = (tPercentage * (VIRTUAL_CANVAS_X * 1.65f)) + 15.0f;
        glBegin(GL_TRIANGLES);
        for(int i = 0; i < 10; i ++)
        {
            //--Get the angle, compute the position.
            float tAngleA = cStartAngle + ((36.0f * TORADIAN) * (float)i);
            float tAngleB = cStartAngle + ((36.0f * TORADIAN) * ((float)i + 1.001000f));

            //--Render.
            glVertex2f(0.0f,                           0.0f);
            glVertex2f(cosf(tAngleA) * cRadius,        sinf(tAngleA) * cRadius);
            glVertex2f(cosf(tAngleB) * cRadius * 0.5f, sinf(tAngleB) * cRadius * 0.5f);
        }
        glEnd();

        //--Clean.
        glTranslatef(VIRTUAL_CANVAS_X * -0.5f, VIRTUAL_CANVAS_Y * -0.5f, 0.0f);
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
    }

    ///--[Resting Overlay]
    //--Resting overlay.
    DebugPrint("Render rest sequence.\n");
    DebugManager::RenderVisualTrace("REST");
    if(mIsRestSequence)
    {
        //--Setup.
        float tAlpha = 0.0f;

        //--Fade out. Alpha increases.
        if(mRestingTimer < RESTING_TICKS_FADEOUT)
        {
            tAlpha = mRestingTimer / (float)RESTING_TICKS_FADEOUT;
        }
        //--Hold. Alpha stays at 1.0f.
        else if(mRestingTimer < RESTING_TICKS_FADEOUT + RESTING_TICKS_HOLD)
        {
            tAlpha = 1.0f;
        }
        //--Fade in. Alpha decreases.
        else
        {
            tAlpha = 1.0f - ((mRestingTimer - RESTING_TICKS_FADEOUT - RESTING_TICKS_HOLD) / (float)RESTING_TICKS_FADEIN);
        }

        //--Setup.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, tAlpha);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);

        //--During the rest sequence, the dialogue can render above the fadeout. This is the only thing that does this!
        if(rWorldDialogue->IsVisible())
        {
            rWorldDialogue->Render();
        }
    }

    ///--[Script Fade]
    //--Render the fade if it's flagged to be over the GUI. It will render over everything, including other transitions.
    DebugPrint("Render script fade over UI.\n");
    DebugManager::RenderVisualTrace("FADEOVER");
    RenderScriptFade(SCRIPT_FADE_OVER_UI);

    ///--[Ray Collision Engine Debug]
    //--Order all Ray Collision Engines to render for debug.
    if(true)
    {
        for(int i = 0; i < mRayCollisionEnginesTotal; i ++)
        {
            mRayCollisionEngines[i]->RenderLines(-mCameraDimensions.mLft, -mCameraDimensions.mTop, GetCameraScale());
        }
    }

    //--Debug.
    DebugPop("AdventureLevel: Render completes at end of function.\n");

    ///--[Minigames]
    //--If the minigame exists and is not fully opaque, render it here.
    if(mRunningMinigame && !mRunningMinigame->IsFullyOpaque())
    {
        mRunningMinigame->Render();
    }
    //--KPop minigame.
    else if(mKPopDanceGame)
    {
        mKPopDanceGame->Render();
    }
    //--Shaking Game.
    else if(mShakeMinigame)
    {
        mShakeMinigame->Render();
    }

    ///--[Diagnostics]
    DebugManager::RenderVisualTrace("ADVEND");

    ///--[FPS Indicator]
    if(false)
    {
        StarlightColor::ClearMixer();
        glDisable(GL_DEPTH_TEST);
        Images.Data.rUIFont->DrawTextArgs(VIRTUAL_CANVAS_X, 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "FPS: %0.2f", Global::Shared()->gActualFPS);
        glEnable(GL_DEPTH_TEST);
    }
}
