//--Base
#include "AdvUISkills.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"

//--Definitions
#include "Definitions.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"
#include "OptionsManager.h"

///========================================== Drawing =============================================
void AdvUISkills::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the side pieces, overlays, and help if needed.

    ///--[Profiles Mode]
    //--If the profiles vis timer is maxed out, don't render anything from the base menu.
    if(mProfilesVisibilityTimer >= ADVMENU_SKILLS_PROFILE_VIS_TICKS)
    {
        RenderProfilesMode();
        return;
    }

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Overlays]
    //--Purchase overlay.
    RenderSkillsPurchase();

    //--Details overlay.
    RenderSkillsDetails();

    //--Profiles mode.
    RenderProfilesMode();

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);
}
void AdvUISkills::RenderLft(float pVisAlpha)
{
    ///--[Setup]
    //--Resolve selected character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);

    //--Arrow.
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1)
    {
        float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
        AdvImages.rArrowLft->Draw(cArrowOscillate * -1.0f, 0.0f);
    }

    ///--[Normal Portrait]
    //--Render the character in question.
    if(!mIsSwappingCharacter)
    {
        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rCurrentCharacter->GetUIRenderPosition(ACE_UI_INDEX_SKILLS);

        //--Position.
        StarBitmap *rCharacterCombatPortrait = rCurrentCharacter->GetCombatPortrait();
        if(rCharacterCombatPortrait) rCharacterCombatPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);
    }
    ///--[Swapping Characters]
    else
    {
        //--Percentage.
        float cScrollPercent = EasingFunction::QuadraticInOut(mCharacterSwapTimer, ADVMENU_SKILLS_CHARACTER_SWAP_TICKS);

        //--Get the previous entity.
        AdvCombatEntity *rPreviousEntity = AdvCombat::Fetch()->GetActiveMemberI(mPrevCharacterSlot);
        if(rPreviousEntity)
        {
            //--Stencil and position.
            TwoDimensionRealPoint tRenderDim = rPreviousEntity->GetUIRenderPosition(ACE_UI_INDEX_SKILLS);

            //--Get portrait.
            StarBitmap *rPortrait = rPreviousEntity->GetCombatPortrait();
            if(rPortrait)
            {
                float cXOffset = VIRTUAL_CANVAS_X * (cScrollPercent) * -1.0f;
                rPortrait->Draw(tRenderDim.mXCenter + cXOffset, tRenderDim.mYCenter);
            }
        }

        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rCurrentCharacter->GetUIRenderPosition(ACE_UI_INDEX_SKILLS);

        //--Current entity renders above.
        StarBitmap *rCharacterCombatPortrait = rCurrentCharacter->GetCombatPortrait();
        float cXOffset = VIRTUAL_CANVAS_X * (1.0f - cScrollPercent) * -1.0f;
        if(rCharacterCombatPortrait) rCharacterCombatPortrait->Draw(tRenderDim.mXCenter + cXOffset, tRenderDim.mYCenter);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUISkills::RenderTop(float pVisAlpha)
{
    ///--[Setup]
    //--Resolve selected character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    //--Header.
    AdvImages.rHeader->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(257.0f, 30.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Skills");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Global JP Available.
    AdvImages.rFont_Heading->DrawText    ( 976.0f, 53.0f, 0, 1.0f, "Global JP:");
    AdvImages.rFont_Heading->DrawTextArgs(1258.0f, 53.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rCurrentCharacter->GetGlobalJP());

    //--Sub-handlers.
    RenderSkillsJobData(cColorAlpha);
    RenderSkillsSkillsData(cColorAlpha);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Strings, top left.
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Strings, top right.
    mScrollFastString->DrawText  (VIRTUAL_CANVAS_X, cxAdvMainlineFontH * 0.0f, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mEditProfilesString->DrawText(VIRTUAL_CANVAS_X, cxAdvMainlineFontH * 1.0f, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUISkills::RenderRgt(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    //--Arrow.
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1)
    {
        float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
        AdvImages.rArrowRgt->Draw(cArrowOscillate * 1.0f, 0.0f);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUISkills::RenderBot(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Grid timers.
    float cClassPercent   =        EasingFunction::QuadraticInOut(mTacticsGridTimer, (float)ADVMENU_SKILLS_TACTICS_VIS_TICKS);
    float cTacticsPercent = 1.0f - EasingFunction::QuadraticInOut(mTacticsGridTimer, (float)ADVMENU_SKILLS_TACTICS_VIS_TICKS);
    float cClassOffset    = VIRTUAL_CANVAS_Y * cClassPercent;
    float cTacticsOffset  = VIRTUAL_CANVAS_Y * cTacticsPercent;

    //--Class skill grid. Can be offscreen.
    if(mTacticsGridTimer < ADVMENU_SKILLS_TACTICS_VIS_TICKS)
    {
        glTranslatef(0.0f, cClassOffset * 1.0f, 0.0f);
        RenderSkillsClassData(cColorAlpha);
        glTranslatef(0.0f, cClassOffset * -1.0f, 0.0f);
    }

    //--Tactics skill grid. Can be offscreen.
    if(mTacticsGridTimer > 0)
    {
        glTranslatef(0.0f, cTacticsOffset * 1.0f, 0.0f);
        RenderSkillsTacticsData(cColorAlpha);
        glTranslatef(0.0f, cTacticsOffset * -1.0f, 0.0f);
    }

    //--Description. Always renders.
    RenderSkillsDescription(cColorAlpha);

    //--Strings, bottom left.
    float cYPos = VIRTUAL_CANVAS_Y - cxAdvMainlineFontH;
    mInspectSkillsString->DrawText(0.0f, cYPos - (cxAdvMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mSkillDetailsString ->DrawText(0.0f, cYPos - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCharacterLftString ->DrawText(0.0f, cYPos - (cxAdvMainlineFontH * 0.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Strings, bottom right.
    mShowTacticsString ->DrawText(VIRTUAL_CANVAS_X, cYPos - (cxAdvMainlineFontH * 1.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCharacterRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos - (cxAdvMainlineFontH * 0.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUISkills::RenderSkillsJobData(float pAlpha)
{
    ///--[Documentation]
    //--Given a character in the skills menu, shows what jobs they have available.

    //--Frame.
    AdvImages.rJobsFrame->Draw();
    AdvImages.rJobsDetail->Draw();

    //--Header Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(643.0f, 70.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Jobs");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Character Resolve]
    //--If the character does not exist for some reason, stop here.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Get a list of jobs, and the current job. Current goes at the top.
    AdvCombatJob *rActiveJob = rCurrentCharacter->GetActiveJob();
    StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();
    if(!rActiveJob || !rJobList) return;

    ///--[Heading Job]
    //--This job appears in a larger font to indicate it's the currently active job.
    AdvImages.rFont_Heading->DrawTextFixed(507.0f, 113.0f, ADVMENU_SKILLS_JOBNAME_MAXLEN, 0, rActiveJob->GetDisplayName());

    //--How much JP the job has.
    AdvImages.rFont_Statistics->DrawTextArgs(757.0f, 127.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rActiveJob->GetCurrentJP());

    ///--[All Other Jobs]
    //--Iterate across the jobs list. All jobs that are not the active job render.
    float cJobX = 507.0f;
    float cJobY = 159.0f;
    float cJobH =  22.0f;
    float cJPX  = 757.0f;

    //--Iterate.
    int i = 0;
    AdvCombatJob *rCheckJob = (AdvCombatJob *)rJobList->PushIterator();
    while(rCheckJob)
    {
        //--If this is the active job, skip it without counting it as a render.
        if(rCheckJob == rActiveJob) { rCheckJob = (AdvCombatJob *)rJobList->AutoIterate(); continue; }

        //--Y Position.
        float cYPos = cJobY + (cJobH * i);

        //--Backing.
        if(i % 2 == 0)
        {
            StarBitmap::DrawRectFill(492.0f, cYPos, 794.0f, cYPos + cJobH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }

        //--Render the name.
        AdvImages.rFont_Mainline->DrawTextFixed(cJobX, cYPos - 1.0f, ADVMENU_SKILLS_JOBNAME_MAXLEN, 0, rCheckJob->GetDisplayName());

        //--Render how much JP this job has.
        AdvImages.rFont_Statistics->DrawTextArgs(cJPX, cYPos + 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rCheckJob->GetCurrentJP());

        //--Next.
        i ++;
        rCheckJob = (AdvCombatJob *)rJobList->AutoIterate();
    }

    ///--[Scrollbar]
    //--Render scrollbar if more than one page is present.
    if(rJobList->GetListSize() > cxScrollbarJobsPerPage)
    {
        StandardRenderScrollbar(mJobSkip, cxScrollbarJobsPerPage, rJobList->GetListSize() - cxScrollbarJobsPerPage, 182.0f, 225.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rJobsScrollbar);
    }

    ///--[Highlight]
    //--In jobs mode, render the active highlight.
    if(mMode == ADVMENU_SKILLS_MODE_JOB_SELECT)
    {
        RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
    }
    //--Otherwise, render the highlight at the stored position, and grey it out.
    else
    {
        StarlightColor::SetMixer(0.70f, 0.70f, 0.70f, pAlpha);
        RenderExpandableHighlight(mJobHighlightPos, mJobHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }
}
void AdvUISkills::RenderSkillsSkillsData(float pAlpha)
{
    ///--[Documentation]
    //--Given a character and selected job, shows what skills are available to memorize.

    //--Frame.
    AdvImages.rSkillsFrame->Draw();
    AdvImages.rSkillsDetail->Draw();

    //--Header Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(864.0f, 104.0f, 0, 1.0f, "Job Skills");
    AdvImages.rFont_Mainline->DrawText(1237.0f, 118.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Memorize");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Character Resolve]
    //--If the character does not exist for some reason, stop here.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    ///--[Job Resolve]
    AdvCombatJob *rSelectedJob = GetHighlightedJob(mJobCursor);
    if(!rSelectedJob) return;

    ///--[Skill Rendering]
    //--For each skill available to the selected job, render its icon, name, and JP cost.
    StarLinkedList *rAbilityList = rSelectedJob->GetAbilityList();
    if(!rAbilityList) return;

    //--Position setup.
    float cEqpX    =  826.0f;
    float cCDX     =  840.0f;
    float cIconX   =  859.0f;
    float cNameX   =  891.0f;
    float cCostX   = 1187.0f;
    float cKnownX  = 1138.0f;
    float cSkillsY =  157.0f;
    float cSkillsH =   26.0f;

    //--Iterate.
    int i = 0;
    int tRenders = 0;
    AdvCombatAbility *rRenderAbility = (AdvCombatAbility *)rAbilityList->PushIterator();
    while(rRenderAbility)
    {
        //--Skip when scrolling.
        if(i < mSkillSkip)
        {
            i ++;
            rRenderAbility = (AdvCombatAbility *)rAbilityList->AutoIterate();
            continue;
        }

        //--If we hit the render count cap:
        if(tRenders >= ADVMENU_SKILLS_MAX_SKILLS_PER_PAGE)
        {
            rAbilityList->PopIterator();
            break;
        }

        //--Compute Y position.
        float cYPos = cSkillsY + (cSkillsH * tRenders);

        //--Backing.
        if(i % 2 == 0)
        {
            //--Normal case:
            if(tRenders < ADVMENU_SKILLS_MAX_SKILLS_PER_PAGE-1)
            {
                StarBitmap::DrawRectFill(830.0f, cYPos, 1270.0f, cYPos + cSkillsH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            }
            //--Special, prevents going outside the edge of the frame.
            else
            {
                AdvImages.rSkillsDetailSpecial->Draw();
            }
        }

        //--Position, render, unposition.
        glTranslatef(cIconX, cYPos + 1.0f, 0.0f);
        glScalef(0.5f, 0.5f, 1.0f);
        rRenderAbility->RenderAt(0.0f, 0.0f);
        glScalef(2.0f, 2.0f, 1.0f);
        glTranslatef(cIconX * -1.0f, (cYPos + 1.0f) * -1.0f, 0.0f);

        //--Name.
        AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cYPos + 3.0f, ADVMENU_SKILLS_SKILLNAME_MAXLEN, 0, rRenderAbility->GetDisplayName());

        //--If the ability is equipped, render this icon.
        if(rRenderAbility->IsEquipped()) AdvImages.rEquippedIcon->Draw(cEqpX, cYPos + 5.0f);

        //--If there is a class-internal version of the ability, render this icon.
        if(rSelectedJob->HasInternalVersionOfAbility(rRenderAbility)) AdvImages.rClassDefaultIcon->Draw(cCDX, cYPos + 5.0f);

        //--Cost.
        if(!rRenderAbility->IsUnlocked())
        {
            //--If the JP Cost is -1, this ability is "Locked" and cannot be learned with JP.
            if(rRenderAbility->GetJPCost() == -1)
            {
                AdvImages.rFont_Statistics->DrawTextArgs(cKnownX, cYPos + 3.0f, 0, 1.0f, "Locked");
            }
            //--Normal:
            else
            {
                AdvImages.rFont_Statistics->DrawTextArgs(cCostX, cYPos + 3.0f, 0, 1.0f, "%i", rRenderAbility->GetJPCost());
            }
        }
        //--Already known.
        else
        {
            AdvImages.rFont_Statistics->DrawText(cKnownX, cYPos + 3.0f, 0, 1.0f, "Learned");
        }

        //--Next.
        i ++;
        tRenders ++;
        rRenderAbility = (AdvCombatAbility *)rAbilityList->AutoIterate();
    }

    ///--[Highlight]
    //--In skill select mode, render the active highlight.
    if(mMode == ADVMENU_SKILLS_MODE_SKILL_SELECT)
    {
        RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
    }

    ///--[Scrollbar]
    //--Render scrollbar if more than one page is present.
    if(rAbilityList->GetListSize() > cxScrollbarSkillsPerPage)
    {
        StandardRenderScrollbar(mSkillSkip, cxScrollbarSkillsPerPage, rAbilityList->GetListSize() - cxScrollbarSkillsPerPage, 182.0f, 225.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rSkillsScrollbar);
    }
}
void AdvUISkills::RenderSkillsClassData(float pAlpha)
{
    ///--[Documentation]
    //--Given a character and active job, shows what skills this job has, and which ones are memorized.

    //--Active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    ///--[Class Skills]
    //--Frame.
    AdvImages.rJobSkillsFrame->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Statistics->DrawText(144.0f, 491.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Job");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Setup.
    float cJobSkillX =  66.0f;
    float cJobSkillY = 520.0f;
    float cJobSkillW =  53.0f;
    float cJobSkillH =  53.0f;

    //--Render all the abilities they currently have equipped.
    for(int x = 0; x < 3; x ++)
    {
        for(int y = 0; y < 3; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rCurrentCharacter->GetAbilityBySlot(x, y);
            if(!rAbility) continue;

            //--Compute position.
            float cRenderX = cJobSkillX + (cJobSkillW * x);
            float cRenderY = cJobSkillY + (cJobSkillH * y);
            if(y > 0) cRenderY = cRenderY + 8.0f;

            //--Render.
            rAbility->RenderAt(cRenderX, cRenderY);
        }
    }

    ///--[Memorized Skills]
    //--Frame.
    AdvImages.rMemorizedFrame->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Statistics->DrawText(380.0f, 491.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Memorized");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Setup.
    float cMemorizedSkillX = 249.0f;
    float cMemorizedSkillY = 520.0f;
    float cMemorizedSkillW =  53.0f;
    float cMemorizedSkillH =  53.0f;

    //--Render all the abilities they currently have equipped.
    for(int x = 0; x < 5; x ++)
    {
        for(int y = 0; y < 3; y ++)
        {
            //--Compute position.
            float cRenderX = cMemorizedSkillX + (cMemorizedSkillW * x);
            float cRenderY = cMemorizedSkillY + (cMemorizedSkillH * y);
            if(y > 1) cRenderY = cRenderY + 8.0f;

            //--Slot is not usable. This is a catalyst slot.
            if(IsCatalystSlot(x + ACE_ABILITY_MEMORIZE_GRID_X0, y))
            {
                AdvImages.rCatalystIcon->Draw(cRenderX, cRenderY);
            }
            //--Render normally.
            else
            {
                //--Skip empty slots.
                AdvCombatAbility *rAbility = rCurrentCharacter->GetAbilityBySlot(x + ACE_ABILITY_MEMORIZE_GRID_X0, y);
                if(!rAbility) continue;

                //--Render.
                rAbility->RenderAt(cRenderX, cRenderY);
            }
        }
    }

    ///--[Highlight]
    if((mMode == ADVMENU_SKILLS_MODE_SKILL_QUERY || mMode == ADVMENU_SKILLS_MODE_SKILL_REPLACE) && !mIsUsingTacticsGrid)
    {
        RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
    }
}
void AdvUISkills::RenderSkillsTacticsData(float pAlpha)
{
    ///--[Documentation]
    //--Renders the "Tactics" grid, which is a set of non-customizable abilities that fall under the tactics
    //  grouping in combat. This contains skills like Retreat, Surrender, Items, and Transformations.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Constants.
    float cTacticsSkillX =  51.0f;
    float cTacticsSkillY = 520.0f;
    float cTacticsSkillW =  53.0f;
    float cTacticsSkillH =  53.0f;

    //--Frames.
    AdvImages.rTacticsSkillsFrame->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Statistics->DrawText(144.0f, 491.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Tactics");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Loop.
    for(int x = ACE_ABILITY_TACTICS_GRID_X0; x <= ACE_ABILITY_TACTICS_GRID_X1; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rCurrentCharacter->GetAbilityBySlot(x, y);
            if(!rAbility) continue;

            //--Compute position.
            float cRenderX = cTacticsSkillX + (cTacticsSkillW * (x - ACE_ABILITY_TACTICS_GRID_X0));
            float cRenderY = cTacticsSkillY + (cTacticsSkillH * y);

            //--Render.
            rAbility->RenderAt(cRenderX, cRenderY);
        }
    }

    ///--[Highlight]
    if(mMode == ADVMENU_SKILLS_MODE_SKILL_QUERY && mIsUsingTacticsGrid)
    {
        RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
    }
}
void AdvUISkills::RenderSkillsDescription(float pAlpha)
{
    ///--[Documentation]
    //--Given a skill, renders its description.
    float cTxtL = 561.0f;
    float cTxtT = 516.0f;
    float cTxtH =  29.0f;

    ///--[Static Parts]
    //--Frame.
    AdvImages.rDescriptionFrame->Draw();

    ///--[Resolve]
    //--Active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Setup.
    AdvCombatAbility *rAbility = NULL;

    //--In skill selection mode, resolve the ability from the job's ability list.
    if(mMode == ADVMENU_SKILLS_MODE_SKILL_SELECT || mMode == ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE)
    {
        //--Get highlighted job.
        AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
        if(!rHighlightedJob) return;

        //--Ability List.
        StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();
        if(!rAbilityList) return;

        //--Get ability.
        rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillCursor);
    }
    //--In skill query mode, resolve the ability from the grid.
    else if(mMode == ADVMENU_SKILLS_MODE_SKILL_QUERY || mMode == ADVMENU_SKILLS_MODE_SKILL_REPLACE)
    {
        //--Normal display:
        if(!mIsUsingTacticsGrid)
        {
            //--On the memory grid, check if we're over the catalyst slots.
            if(IsCatalystSlot(mAbilityCursorX, mAbilityCursorY))
            {
                AdvImages.rFont_Description->DrawText(cTxtL, cTxtT + (cTxtH * 0.0f), 0, 1.0f, "Slot locked, find skill catalysts to unlock it.");
            }
            //--Otherwise, set this ability as active.
            else
            {
                rAbility = rCurrentCharacter->GetAbilityBySlot(mAbilityCursorX, mAbilityCursorY);
            }
        }
        //--Tactics grid.
        else
        {
            rAbility = rCurrentCharacter->GetAbilityBySlot(mAbilityCursorX + ACE_ABILITY_TACTICS_GRID_X0, mAbilityCursorY);
        }
    }
    //--Otherwise, render a set of instructions and stop.
    else
    {
        AdvImages.rFont_Description->DrawText(cTxtL, cTxtT + (cTxtH * 0.0f), 0, 1.0f, "Select a job to purchase skills with JP.");
        AdvImages.rFont_Description->DrawText(cTxtL, cTxtT + (cTxtH * 1.0f), 0, 1.0f, "Skills equipped to the memory grid can be used ");
        AdvImages.rFont_Description->DrawText(cTxtL, cTxtT + (cTxtH * 2.0f), 0, 1.0f, "regardless of job.");
        AdvImages.rFont_Description->DrawText(cTxtL, cTxtT + (cTxtH * 3.0f), 0, 1.0f, "Skills on the job grid change when you change");
        AdvImages.rFont_Description->DrawText(cTxtL, cTxtT + (cTxtH * 4.0f), 0, 1.0f, "jobs. If you don't have enough class JP, you");
        AdvImages.rFont_Description->DrawText(cTxtL, cTxtT + (cTxtH * 5.0f), 0, 1.0f, "can spend global JP as well.");
    }


    ///--[Common]
    //--If the ability failed to resolve, such as because we're not in a mode with ability display, stop here.
    if(!rAbility) return;

    //--Name.
    AdvImages.rFont_Heading->DrawText(619.0f, 466.0f, 0, 1.0f, rAbility->GetDisplayName());

    ///--[Simple Render]
    if(!mShowAdvancedDescriptions)
    {
        //--Description lines.
        for(int i = 0; i < ADVMENU_SKILLS_MAX_DESCRIPTION_LINES; i ++)
        {
            //--Get line.
            StarlightString *rDescriptionLine = rAbility->GetSimplifiedDescriptionLine(i);
            if(!rDescriptionLine) break;

            //--Render.
            rDescriptionLine->DrawText(cTxtL, cTxtT + (cTxtH * i), 0, 1.0f, AdvImages.rFont_SimpleDescription);
        }
    }
    ///--[Advanced Render]
    else
    {
        //--Sizes.
        cTxtH = 26.0f;

        //--Description lines.
        for(int i = 0; i < ADVMENU_SKILLS_MAX_DESCRIPTION_LINES; i ++)
        {
            //--Get line.
            StarlightString *rDescriptionLine = rAbility->GetDescriptionLine(i);
            if(!rDescriptionLine) break;

            //--Render.
            rDescriptionLine->DrawText(cTxtL, cTxtT + (cTxtH * i), 0, 1.0f, AdvImages.rFont_Description);
        }
    }
}
void AdvUISkills::RenderSkillsPurchase()
{
    ///--[Documentation]
    //--When the player is purchasing a skill, this renders a menu that appears and shows controls.
    if(mPurchaseTimer < 1) return;

    //--Percentage.
    float cPurchasePct = EasingFunction::QuadraticInOut(mPurchaseTimer, ADVMENU_SKILLS_PURCHASE_TICKS);
    float cAlpha = cPurchasePct;

    //--Option. Selects between fading and sliding.
    bool tUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");
    if(!tUseFade) cAlpha = 1.0f;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cPurchasePct * 0.7f * cAlpha));

    ///--[Resolve Entry]
    //--Active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Get highlighted job.
    AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
    if(!rHighlightedJob) return;

    //--Ability List.
    StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();
    if(!rAbilityList) return;

    //--Get ability.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillCursor);
    if(!rAbility) return;

    ///--[Details]
    //--Position.
    AutoPieceOpen(DIR_UP, cAlpha);

    //--Frame.
    AdvImages.rBuySkillPopup->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(cAlpha);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Memorize");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Construct buffer.
    char *tBuffer = InitializeString("%s for %i JP", rAbility->GetDisplayName(), rAbility->GetJPCost());

    //--Position.
    float cTxtLft = (VIRTUAL_CANVAS_X * 0.50f) - (AdvImages.rFont_Mainline->GetTextWidth(tBuffer) * 0.50f);
    float cTxtTop = 223.0f;
    float cIcoLft = cTxtLft - 35.0f;
    float cIcoTop = 223.0f;

    //--Icon.
    glTranslatef(cIcoLft, cIcoTop, 0.0f);
    glScalef(0.5f, 0.5f, 1.0f);
    rAbility->RenderAt(0.0f, 0.0f);
    glScalef(2.0f, 2.0f, 1.0f);
    glTranslatef(-cIcoLft, -cIcoTop, 0.0f);

    //--Name, cost.
    AdvImages.rFont_Mainline->DrawText(cTxtLft, cTxtTop, 0, 1.0f, tBuffer);

    ///--[Strings]
    mConfirmPurchaseString->DrawText(376.0f, 285.0f,                          SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(987.0f, 285.0f, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Unposition.
    AutoPieceClose();
    //HandleFadeOrTranslate(tUseFade, 0.0f, cTopOffset * -1.0f, cAlpha);

    //--Clean.
    free(tBuffer);
}
void AdvUISkills::RenderSkillsDetails()
{
    ///--[Documentation]
    //--Renders extended details about the item in question. This uses the standard description frame,
    //  and an alternate set of description lines given to the ability.
    if(mDetailsTimer < 1) return;

    //--Percentage.
    float cDetailsPercent = EasingFunction::QuadraticInOut(mDetailsTimer, ADVMENU_SKILLS_DETAILS_SWITCH_TICKS);

    //--Offsets.
    float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cDetailsPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cDetailsPercent * 0.7f));

    ///--[Resolve Skill]
    //--Figure out which ability needs to have its description shown.
    AdvCombatAbility *rAbility = NULL;

    //--Active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Details from the memorized grid:
    if(mIsDetailsFromGrid)
    {
        //--Get from the character's internal grid.
        rAbility = rCurrentCharacter->GetAbilityBySlot(mAbilityCursorX, mAbilityCursorY);
    }
    //--Details from the jobs list.
    else
    {
        //--Get highlighted job.
        AdvCombatJob *rHighlightedJob = GetHighlightedJob(mJobCursor);
        if(!rHighlightedJob) return;

        //--Ability List.
        StarLinkedList *rAbilityList = rHighlightedJob->GetAbilityList();
        if(!rAbilityList) return;

        //--Get ability.
        rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillCursor);
    }

    //--No ability resolved. Show instrctions.
    if(!rAbility)
    {
        mIsShowingDetails = false;
        return;
    }

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    //--Frame.
    AdvImages.rFrame_Description->Draw();

    //--Expandable header and name. Note that there is an additional 48 pixels of space on the header
    //  within the dead zones.
    float cNameLen = AdvImages.rFont_Heading->GetTextWidth(rAbility->GetDisplayName());
    RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, cNameLen - 48.0f, 76.0f, 76.0f, AdvImages.rHeader_Expandable);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 510.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rAbility->GetDisplayName());

    ///--[Description]
    //--Setup.
    float cXPosition =  50.0f;
    float cYPosition = 548.0f;
    float cYHeight   =  28.0f;

    //--Render advanced description lines.
    int tAdvancedLines = rAbility->GetDescriptionLinesTotal();
    for(int i = 0; i < tAdvancedLines; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = rAbility->GetDescriptionLine(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), 0, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Finish Up]
    //--Clean.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
}
