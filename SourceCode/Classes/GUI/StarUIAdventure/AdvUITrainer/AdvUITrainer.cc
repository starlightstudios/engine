//--Base
#include "AdvUITrainer.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
AdvUITrainer::AdvUITrainer()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AEQP");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUITrainer ======= ]
    ///--[Variables]
    //--System
    //--Character Selection
    mCharacterCursor = 0;

    //--Cash Input
    mIsCashMode = false;
    mCashTimer = 0;
    mEXPValue = 0;
    mEXPEditSlot = 0;

    //--Confirmation
    mIsConfirmMode = false;
    mConfirmTimer = 0;

    //--Cursor
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    ///--[Strings]
    //--Purchase Strings
    mStringsPurchase[0] = InitializeString("Select how much EXP you would like");
    mStringsPurchase[1] = InitializeString("to purchase.");
    mStringsPurchase[2] = InitializeString("1 EXP costs 2 Platina.");

    //--Confirm Strings
    mStringsConfirm[0] = InitializeString("Really spend this much platina on");
    mStringsConfirm[1] = InitializeString("this much EXP?");

    //--Help strings are initialized but must re-resolve during setup as controls can be rebound.
    for(int i = 0; i < ADVMENU_TRAINER_STRING_HELP_CHARSELECT_TOTAL; i ++) mStringsCharacterHelp[i] = new StarlightString();
    for(int i = 0; i < ADVMENU_TRAINER_STRING_HELP_EXPSELECT_TOTAL;  i ++) mStringsEXPSelectHelp[i] = new StarlightString();
    for(int i = 0; i < ADVMENU_TRAINER_STRING_HELP_CONFIRM_TOTAL;    i ++) mStringsConfirmHelp[i]   = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUITrainer::~AdvUITrainer()
{
    for(int i = 0; i < ADVMENU_TRAINER_STRING_PURCHASE_TOTAL;        i ++) free(mStringsPurchase[i]);
    for(int i = 0; i < ADVMENU_TRAINER_STRING_CONFIRM_TOTAL;         i ++) free(mStringsConfirm[i]);
    for(int i = 0; i < ADVMENU_TRAINER_STRING_HELP_CHARSELECT_TOTAL; i ++) delete mStringsCharacterHelp[i];
    for(int i = 0; i < ADVMENU_TRAINER_STRING_HELP_EXPSELECT_TOTAL;  i ++) delete mStringsEXPSelectHelp[i];
    for(int i = 0; i < ADVMENU_TRAINER_STRING_HELP_CONFIRM_TOTAL;    i ++) delete mStringsConfirmHelp[i];
}
void AdvUITrainer::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Trainer UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",    &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUITrainer::TakeForeground()
{
    ///--[Cursor]
    //--Recompute cursor positions, finish instantly.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--Character selection controls.
    mStringsCharacterHelp[0]->AutoSetControlString("[IMG0][IMG1] Select Character", 2, "Left", "Right");
    mStringsCharacterHelp[1]->AutoSetControlString("[IMG0] Confirm  [IMG1] Cancel", 2, "Activate", "Cancel");

    //--EXP selection controls.
    mStringsEXPSelectHelp[0]->AutoSetControlString("[IMG0][IMG1] Select Digit [IMG2][IMG3] Set Value", 4, "Left", "Right", "Up", "Down");

    //--Confirm controls.
    mStringsConfirmHelp[0]->AutoSetControlString("[IMG0] Confirm  [IMG1] Cancel", 2, "Activate", "Cancel");
}

///======================================= Core Methods ===========================================
void AdvUITrainer::RefreshMenuHelp()
{

}
void AdvUITrainer::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Determines where the cursor should be.

    ///--[Character Select]
    if(!mIsCashMode)
    {
        //--Constants.
        float cLft =  52.0f;
        float cTop = 492.0f;
        float cWid = 276.0f;
        float cHei = 193.0f;
        float cSpcX = ADVMENU_TRAINER_XSPACING;
        float cBuf = 0.0f;

        //--Positions.
        float cFinalLft = cLft + (cSpcX * mCharacterCursor);
        float cFinalTop = cTop;
        float cFinalRgt = cFinalLft + cWid;
        float cFinalBot = cTop + cHei;

        //--Apply buffer.
        cFinalLft = cFinalLft - cBuf;
        cFinalTop = cFinalTop - cBuf;
        cFinalRgt = cFinalRgt + cBuf;
        cFinalBot = cFinalBot + cBuf;

        //--Resize the cursor.
        mHighlightPos.MoveTo(cFinalLft, cFinalTop, ADVMENU_TRAINER_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(cFinalRgt - cFinalLft, cFinalBot - cFinalTop, ADVMENU_TRAINER_CURSOR_MOVE_TICKS);
    }
    ///--[Cash Input]
    else if(mIsCashMode)
    {
        //--Constants.
        float cNumLft = ADVMENU_TRAINER_CASH_NUMBERLFT - 1.0f;
        float cNumTop = ADVMENU_TRAINER_CASH_NUMBERTOP + 6.0f;
        float cNumSpX = ADVMENU_TRAINER_CASH_NUMBERSPX;
        float cNumWid = 47.0f;
        float cNumHei = 58.0f;
        float cBuf = 3.0f;

        //--There are six positions, specified by constants.
        float cFinalLft = cNumLft + (cNumSpX * mEXPEditSlot);
        float cFinalTop = cNumTop;
        float cFinalRgt = cFinalLft + cNumWid;
        float cFinalBot = cFinalTop + cNumHei;

        //--Apply buffer.
        cFinalLft = cFinalLft - cBuf;
        cFinalTop = cFinalTop - cBuf;
        cFinalRgt = cFinalRgt + cBuf;
        cFinalBot = cFinalBot + cBuf;

        //--Resize the cursor.
        mHighlightPos.MoveTo(cFinalLft, cFinalTop, ADVMENU_TRAINER_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(cFinalRgt - cFinalLft, cFinalBot - cFinalTop, ADVMENU_TRAINER_CURSOR_MOVE_TICKS);
    }

    //--Note: Confirm mode does not use the cursor.
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUITrainer::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ====== Documentation ===== ]
    //--Handles updating timers and handling player control inputs for this menu. The Trainer UI is
    //  a standalone UI.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Timers that run between 0 and a constant based on a boolean.
    StandardTimer(mVisibilityTimer, 0, ADVMENU_TRAINER_VIS_TICKS,     true);
    StandardTimer(mCashTimer,       0, ADVMENU_TRAINER_CASH_TICKS,    mIsCashMode);
    StandardTimer(mConfirmTimer,    0, ADVMENU_TRAINER_CONFIRM_TICKS, mIsConfirmMode);

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[ ==== Character Select ==== ]
    if(!mIsCashMode)
    {
        ///--[Setup]
        //--Get party size.
        int tActivePartySize = rAdventureCombat->GetActivePartyCount();

        ///--[Character Select]
        //--Left decrements character cursor.
        if(rControlManager->IsFirstPress("Left") && tActivePartySize >= 2)
        {
            //--Cursor.
            mCharacterCursor --;
            if(mCharacterCursor < 0) mCharacterCursor = tActivePartySize - 1;

            //--Set cursor position.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Right increments character cursor.
        else if(rControlManager->IsFirstPress("Right") && tActivePartySize >= 2)
        {
            //--Cursor.
            mCharacterCursor ++;
            if(mCharacterCursor >= tActivePartySize) mCharacterCursor = 0;

            //--Set cursor position.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        ///--[Select Character]
        //--Selects the character and goes to the cash input interface.
        if(rControlManager->IsFirstPress("Activate") && tActivePartySize >= 1)
        {
            //--Cash mode.
            mIsCashMode = true;
            mEXPValue = 0;
            mEXPEditSlot = 5;
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Block the rest of the update.
            return true;
        }

        ///--[Exit]
        //--Exits the interface.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[ ======= Cash Input ======= ]
    else if(mIsCashMode && !mIsConfirmMode)
    {
        //--Left decrements the cash edit slot.
        if(rControlManager->IsFirstPress("Left"))
        {
            //--Cursor.
            mEXPEditSlot --;
            if(mEXPEditSlot < 0) mEXPEditSlot = 5;

            //--Set cursor position.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Right increments character cursor.
        else if(rControlManager->IsFirstPress("Right"))
        {
            //--Cursor.
            mEXPEditSlot ++;
            if(mEXPEditSlot >= 6) mEXPEditSlot = 0;

            //--Set cursor position.
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Up increases the value by the amount of the given slot.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--Resolve the value in the slot. Remember the slots are backwards.
            int tCashIncrement = round(pow(10.0f, ADVMENU_TRAINER_CASH_MAXSLOTS-mEXPEditSlot));
            int tCurValue = (mEXPValue / tCashIncrement) % 10;

            //--If the value is 9, wrap around by decrementing.
            if(tCurValue == 9)
            {
                mEXPValue = mEXPValue - (tCashIncrement * 9);
            }
            //--Otherwise, add one increment.
            else
            {
                mEXPValue = mEXPValue + tCashIncrement;
            }

            //--Clamp to max platina / 2.
            AdventureInventory *rInventory = AdventureInventory::Fetch();
            int tMaxPlatinaToSpend = rInventory->GetPlatina() / 2;
            if(mEXPValue > tMaxPlatinaToSpend) mEXPValue = tMaxPlatinaToSpend;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Down, decreases the value by the amount in the given slot.
        else if(rControlManager->IsFirstPress("Down"))
        {
            //--Resolve the value in the slot. Remember the slots are backwards.
            int tCashIncrement = round(pow(10, ADVMENU_TRAINER_CASH_MAXSLOTS-mEXPEditSlot));
            int tCurValue = (mEXPValue / tCashIncrement) % 10;

            //--If the value is 0, wrap around by incrementing.
            if(tCurValue == 0)
            {
                mEXPValue = mEXPValue + (tCashIncrement * 9);
            }
            //--Otherwise, subtract one increment.
            else
            {
                mEXPValue = mEXPValue - tCashIncrement;
            }

            //--Clamp to max platina / 2.
            AdventureInventory *rInventory = AdventureInventory::Fetch();
            int tMaxPlatinaToSpend = rInventory->GetPlatina() / 2;
            if(mEXPValue > tMaxPlatinaToSpend) mEXPValue = tMaxPlatinaToSpend;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Bring up the confirmation window.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Switch to confirm mode.
            mIsConfirmMode = true;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return true;
        }

        //--Returns to character selection.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsCashMode = false;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    ///--[ ====== Confirmation ====== ]
    else if(mIsConfirmMode)
    {
        //--Accepts the payment and returns to character selection.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Remove the amount of money from the inventory.
            AdventureInventory *rInventory = AdventureInventory::Fetch();
            rInventory->SetPlatina(rInventory->GetPlatina() - (mEXPValue * 2));

            //--Give that amount to the selected character, div 2, as EXP.
            AdvCombatEntity *rSelectedCharacter = rAdventureCombat->GetActiveMemberI(mCharacterCursor);
            if(rSelectedCharacter) rSelectedCharacter->SetXP(rSelectedCharacter->GetXP() + (mEXPValue));

            //--Reset flags.
            mEXPValue = 0;
            mIsConfirmMode = false;
            mIsCashMode = false;
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
            return true;
        }

        //--Returns to cash input.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsConfirmMode = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Update handled.
    return true;
}
void AdvUITrainer::UpdateBackground()
{
    if(mVisibilityTimer > 0) mVisibilityTimer --;
    if(mCashTimer       > 0) mCashTimer --;
    if(mConfirmTimer    > 0) mConfirmTimer --;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUITrainer::RenderPieces(float pVisAlpha)
{
    ///--[ ==================== Documentation ===================== ]
    //--Renders the trainer menu in various stages.

    ///--[Timer Resolving]
    //--Resolve timer offset.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)ADVMENU_TRAINER_VIS_TICKS);
    float cPercent = 1.0f - cAlpha;

    ///--[ ================= Character Selection ================== ]
    //--Backing, goes over the world display. This does not render if not in trainer mode, because the base menu
    //  will take over afterwards.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cAlpha));

    ///--[ ======= Top Block ======== ]
    if(cPercent < 1.0)
    {
        //--Setup.
        AdventureInventory *rInventory = AdventureInventory::Fetch();

        //--Translate.
        AutoPieceOpen(DIR_UP, cAlpha);

        //--Title
        AdvImages.rFrameHeader->Draw();
        mColorHeading.SetAsMixerAlpha(cAlpha);
        AdvImages.rFontDoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Trainer");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Platina.
        AdvImages.rFramePlatina->Draw();
        mColorHeading.SetAsMixerAlpha(cAlpha);
        AdvImages.rFontHeading->DrawText(1060.0f, 31.0f, 0, 1.0f, "Platina: ");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        AdvImages.rFontHeading->DrawTextArgs(1356.0f, 31.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetPlatina());

        //--Clean.
        AutoPieceClose();
    }

    ///--[ ====== Bottom Block ====== ]
    if(cPercent < 1.0)
    {
        //--Translate.
        AutoPieceOpen(DIR_DOWN, cAlpha);

        //--Setup.
        AdvCombat *rAdventureCombat = AdvCombat::Fetch();
        int tActivePartySize = rAdventureCombat->GetActivePartyCount();

        //--For each member of the active party:
        for(int i = 0; i < tActivePartySize; i ++)
        {
            //--Get the member in question.
            AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
            if(!rEntity) continue;

            //--Get and check portrait.
            StarBitmap *rPortrait = rEntity->GetCombatPortrait();
            if(!rPortrait) continue;

            //--Get the render position for this UI.
            TwoDimensionRealPoint tRenderDim = rEntity->GetUIRenderPosition(ACE_UI_INDEX_TRAINER);

            //--Render.
            glTranslatef(ADVMENU_TRAINER_XSPACING * i, 0.0f, 0.0f);
            rPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);
            glTranslatef(ADVMENU_TRAINER_XSPACING * i * -1.0f, 0.0f, 0.0f);
        }

        //--Once all party members have rendered, render the overlay information.
        for(int i = 0; i < tActivePartySize; i ++)
        {
            //--Get the member in question.
            AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
            if(!rEntity) continue;

            //--Position and call.
            glTranslatef(ADVMENU_TRAINER_XSPACING * i, 0.0f, 0.0f);
            RenderCharacterInfoPanel(rEntity);
            glTranslatef(ADVMENU_TRAINER_XSPACING * i * -1.0f, 0.0f, 0.0f);
        }

        //--Help text.
        float cBotHelpPos = VIRTUAL_CANVAS_Y - 30.0f;
        mStringsCharacterHelp[0]->DrawText(            0.0f, cBotHelpPos, SUGARFONT_NOCOLOR,                          1.0f, AdvImages.rFontHelp);
        mStringsCharacterHelp[1]->DrawText(VIRTUAL_CANVAS_X, cBotHelpPos, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFontHelp);

        //--Clean.
        AutoPieceClose();
    }

    //--Clean mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[ ==================== Cash Selection ==================== ]
    //--Cash selection mode. Renders over the character selection.
    if(mCashTimer > 0)
    {
        //--Compute offsets.
        float cCashTime = EasingFunction::QuadraticInOut(mCashTimer, (float)ADVMENU_TRAINER_CASH_TICKS);

        //--Position.
        AutoPieceOpen(DIR_UP, cCashTime);

        //--Backing.
        AdvImages.rFramePayment->Draw();

        //--Arrows, fixed positions. Left and right arrows exist once.
        AdvImages.rOverlayArrowRgt->Draw(865.0f, 236.0f);
        AdvImages.rOverlayArrowRgt->Draw(442.0f, 236.0f, SUGAR_FLIP_HORIZONTAL);

        //--Six vertical arrows for six digits.
        float cArrowLft = ADVMENU_TRAINER_CASH_ARROWLFT;
        float cArrowSpX = ADVMENU_TRAINER_CASH_ARROWSPX;
        float cArrowTop = ADVMENU_TRAINER_CASH_ARROWTOP;
        float cArrowBot = ADVMENU_TRAINER_CASH_ARROWBOT;
        for(int i = 0; i < 6; i ++)
        {
            AdvImages.rOverlayArrowUp->Draw(cArrowLft + (cArrowSpX * i), cArrowTop);
            AdvImages.rOverlayArrowUp->Draw(cArrowLft + (cArrowSpX * i), cArrowBot, SUGAR_FLIP_VERTICAL);
        }

        //--Render the current platina value, one digit at a time.
        float cNumberLft = ADVMENU_TRAINER_CASH_NUMBERLFT;
        float cNumberTop = ADVMENU_TRAINER_CASH_NUMBERTOP;
        float cNumberSpX = ADVMENU_TRAINER_CASH_NUMBERSPX;
        AdvImages.rFontCash->DrawTextArgs(cNumberLft + (cNumberSpX * 0), cNumberTop, 0, 1.0f, "%i", (mEXPValue / 100000) % 10);
        AdvImages.rFontCash->DrawTextArgs(cNumberLft + (cNumberSpX * 1), cNumberTop, 0, 1.0f, "%i", (mEXPValue /  10000) % 10);
        AdvImages.rFontCash->DrawTextArgs(cNumberLft + (cNumberSpX * 2), cNumberTop, 0, 1.0f, "%i", (mEXPValue /   1000) % 10);
        AdvImages.rFontCash->DrawTextArgs(cNumberLft + (cNumberSpX * 3), cNumberTop, 0, 1.0f, "%i", (mEXPValue /    100) % 10);
        AdvImages.rFontCash->DrawTextArgs(cNumberLft + (cNumberSpX * 4), cNumberTop, 0, 1.0f, "%i", (mEXPValue /     10) % 10);
        AdvImages.rFontCash->DrawTextArgs(cNumberLft + (cNumberSpX * 5), cNumberTop, 0, 1.0f, "%i", (mEXPValue /      1) % 10);

        //--Help strings.
        float cHelpLft = 430.0f;
        float cHelpTop = 326.0f;
        float cHelpSpY =  22.0f;
        AdvImages.rFontPaymentHelp->DrawText(cHelpLft, cHelpTop + (cHelpSpY * 0.0f), 0, 1.0f, mStringsPurchase[0]);
        AdvImages.rFontPaymentHelp->DrawText(cHelpLft, cHelpTop + (cHelpSpY * 1.0f), 0, 1.0f, mStringsPurchase[1]);
        AdvImages.rFontPaymentHelp->DrawText(cHelpLft, cHelpTop + (cHelpSpY * 2.0f), 0, 1.0f, mStringsPurchase[2]);
        mStringsEXPSelectHelp[0]->DrawText(cHelpLft, 410.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFontHelp);

        //--Clean.
        AutoPieceClose();
    }

    ///--[Highlight]
    //--Render the expandable highlight, which is used between the different modes and renders on top of them.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlayHighlight);

    ///--[ ===================== Confirmation ===================== ]
    if(mConfirmTimer > 0)
    {
        ///--[Level Setup]
        //--Setup.
        AdvCombat *rAdventureCombat = AdvCombat::Fetch();
        AdvCombatEntity *rSelectedCharacter = rAdventureCombat->GetActiveMemberI(mCharacterCursor);

        //--Levels. If the character fails to resolve leave both at zero.
        int tCurLevel = 0;
        int tNewLevel = 0;
        if(rSelectedCharacter)
        {
            tCurLevel = rSelectedCharacter->GetLevel();
            tNewLevel = rSelectedCharacter->GetLevelIfXPAdded(mEXPValue);
        }

        ///--[Rendering]
        //--Compute offsets.
        float cConfirmTime = EasingFunction::QuadraticInOut(mConfirmTimer, (float)ADVMENU_TRAINER_CONFIRM_TICKS);
        float cConfirmAlpha = cConfirmTime;

        //--Background fadeout.
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cConfirmTime));

        //--Position.
        AutoPieceOpen(DIR_UP, cConfirmAlpha);

        //--Frame.
        AdvImages.rFrameConfirm->Draw();

        //--String. Doesn't change.
        AdvImages.rFontHeading->DrawText(374.0f, 172.0f, 0, 1.0f, mStringsConfirm[0]);
        AdvImages.rFontHeading->DrawText(374.0f, 207.0f, 0, 1.0f, mStringsConfirm[1]);

        //--Variable XP/Platina amounts. Also renders level-up if they are different.
        if(tCurLevel == tNewLevel)
        {
            AdvImages.rFontStats->DrawTextArgs(683.0f, 262.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%iPl -> %iExp", mEXPValue*2, mEXPValue);
        }
        //--Same render but centered differently.
        else
        {
            AdvImages.rFontStats->DrawTextArgs(520.0f, 262.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%iPl -> %iExp", mEXPValue*2, mEXPValue);
            AdvImages.rFontStats->DrawTextArgs(810.0f, 262.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Lv %i -> Lv %i", tCurLevel, tNewLevel);
        }

        //--Help strings.
        mStringsConfirmHelp[0]->DrawText(381.0f, 287.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFontHelp);

        //--Clean.
        AutoPieceClose();
    }
}
void AdvUITrainer::RenderCharacterInfoPanel(AdvCombatEntity *pEntity)
{
    ///--[Documentation]
    //--Called by RenderTrainerMenu(), renders the info panel over the provided character, showing
    //  their name, EXP, and other information. This is built to be the leftmost panel, to render
    //  other characters use a glTranslate() call.
    if(!pEntity) return;

    //--Render backing.
    AdvImages.rFrameBase->Draw();
    AdvImages.rOverlayNameBanner->Draw();

    //--Render name.
    const char *rCharacterName = pEntity->GetDisplayName();
    if(rCharacterName) AdvImages.rFontName->DrawText(189.0f, 492.0f, SUGARFONT_AUTOCENTER_X, rCharacterName);

    //--Experience statistics.
    float cLft =  67.0f;
    float cRgt = 317.0f;
    float cTop = 543.0f;
    float cSpcY = 25.0f;
    uint32_t cFlagsLft = 0;
    uint32_t cFlagsRgt = SUGARFONT_RIGHTALIGN_X;
    AdvImages.rFontStats->DrawText    (cLft, cTop + (cSpcY * 0), cFlagsLft, 1.0f, "Level:");
    AdvImages.rFontStats->DrawTextArgs(cRgt, cTop + (cSpcY * 0), cFlagsRgt, 1.0f, "%i", pEntity->GetLevel()+1);
    AdvImages.rFontStats->DrawText    (cLft, cTop + (cSpcY * 1), cFlagsLft, 1.0f, "Total EXP:");
    AdvImages.rFontStats->DrawTextArgs(cRgt, cTop + (cSpcY * 1), cFlagsRgt, 1.0f, "%i", pEntity->GetXP());
    AdvImages.rFontStats->DrawText    (cLft, cTop + (cSpcY * 2), cFlagsLft, 1.0f, "EXP This:");
    AdvImages.rFontStats->DrawTextArgs(cRgt, cTop + (cSpcY * 2), cFlagsRgt, 1.0f, "%i", pEntity->GetXPToNextLevelMax());
    AdvImages.rFontStats->DrawText    (cLft, cTop + (cSpcY * 3), cFlagsLft, 1.0f, "EXP Next:");
    AdvImages.rFontStats->DrawTextArgs(cRgt, cTop + (cSpcY * 3), cFlagsRgt, 1.0f, "%i", pEntity->GetXPToNextLevel());
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
