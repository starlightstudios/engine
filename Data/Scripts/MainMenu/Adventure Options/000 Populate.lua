-- |[ ================================= Adventure Options Menu ================================= ]|
--Options concerning Adventure Mode. Can also be edited from the in-game options menu.
local sBasePath = fnResolvePath()

-- |[Setup]|
--Local vars.
local iAscendingPriority = 0

--Length.
local sLongestString = "Unload Dialogue Portraits"

-- |[Header]|
FlexMenu_SetHeaderSize(2)
FlexMenu_SetHeaderLine(0, "Adventure Options")
FlexMenu_SetHeaderLine(1, "May require program restart", 0.6)

-- |[ ====================================== Menu Options ====================================== ]|
-- |[Autosave Count]|
local iAutosaveMax = OM_GetOption("Max Autosaves")
FlexMenu_RegisterButton("Max Autosaves")

	--Description.
    FlexButton_SetProperty("Description", "How many autosaves are stored. Set to 0 to disable autosaves.")
	
    --Button properties.
	StarButton_SetProperty("Priority", iAscendingPriority)
	FlexButton_SetProperty("Slider Mode", 0, 100)
	FlexButton_SetProperty("Current Slider", iAutosaveMax)
	FlexButton_SetProperty("Slider IncVals", 1, 10)
	FlexButton_SetProperty("Lying Length", sLongestString)
    
    --Increment priority.
	iAscendingPriority = iAscendingPriority + 1
    
DL_PopActiveObject()

-- |[Asset Streaming]|
FlexMenu_RegisterButton("Asset Streaming")

	--Description.
    FlexButton_SetProperty("Description", "Instead of loading all game assets at program start, loads them when they are needed, and unloads them when no longer needed. Greatly decreases RAM usage but incurs periodic loading hiccups. Good for old systems. Requires restart to take effect without causing error warnings.")

	--Option cases.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "Enabled")
	FlexButton_SetProperty("Option Text", 1, "Disabled")
	
	--Set to current mode.
	local bAllowAssetStreaming = OM_GetOption("Allow Asset Streaming")
	if(bAllowAssetStreaming) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	FlexButton_SetProperty("Lying Length", sLongestString)
    
    --Increment priority.
	iAscendingPriority = iAscendingPriority + 1

DL_PopActiveObject()

-- |[Portrait Unload]|
FlexMenu_RegisterButton("Unload Dialogue Portraits")

	--Description.
    FlexButton_SetProperty("Description", "If Asset Streaming is enabled, unloads dialogue portraits when dialogue ends, and loads them on demand. Saves RAM, incurs loading times. Good for old systems. Requires restart to take effect without causing error warnings.")

	--Option cases.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "Enabled")
	FlexButton_SetProperty("Option Text", 1, "Disabled")
	
	--Set to current mode.
	local bActorsUnloadDialogue = OM_GetOption("Actors Unload After Dialogue")
	if(bActorsUnloadDialogue) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	FlexButton_SetProperty("Lying Length", sLongestString)
    
    --Increment priority.
	iAscendingPriority = iAscendingPriority + 1

DL_PopActiveObject()

-- |[Water Shader]|
FlexMenu_RegisterButton("Shader: Underwater")

	--Description.
    FlexButton_SetProperty("Description", "When underwater, uses a shader to cause ripple effects. Disable if on an old system without OpenGL 3.0, or if this causes instability.")

	--Option cases.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "Disabled")
	FlexButton_SetProperty("Option Text", 1, "Enabled")
	
	--Set to current mode.
	local bDisableUnderwaterShader = OM_GetOption("Disable Shader AdventureLevel Underwater")
	if(bDisableUnderwaterShader) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	FlexButton_SetProperty("Lying Length", sLongestString)
    
    --Increment priority.
	iAscendingPriority = iAscendingPriority + 1

DL_PopActiveObject()

-- |[Outline Shader]|
FlexMenu_RegisterButton("Shader: Outline")

	--Description.
    FlexButton_SetProperty("Description", "Tougher enemies have colored outlines on the overworld. Disable if this causes system instability.")

	--Option cases.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "Disabled")
	FlexButton_SetProperty("Option Text", 1, "Enabled")
	
	--Set to current mode.
	local bDisableOutlineShader = OM_GetOption("Disable Shader TilemapActor ColorOutline")
	if(bDisableUnderwaterShader) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	FlexButton_SetProperty("Lying Length", sLongestString)
    
    --Increment priority.
	iAscendingPriority = iAscendingPriority + 1

DL_PopActiveObject()

-- |[Lighting Shader]|
FlexMenu_RegisterButton("Shader: Lighting")

	--Description.
    FlexButton_SetProperty("Description", "Dark areas use a lighting shader to look atmospheric. Disable if this causes system instability or FPS loss.")

	--Option cases.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "Disabled")
	FlexButton_SetProperty("Option Text", 1, "Enabled")
	
	--Set to current mode.
	local bDisableLightingShader = OM_GetOption("Disable Shader AdventureLevel Lighting")
	if(bDisableLightingShader) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	FlexButton_SetProperty("Lying Length", sLongestString)
    
    --Increment priority.
	iAscendingPriority = iAscendingPriority + 1

DL_PopActiveObject()

-- |[ ======================================= Save / Exit ====================================== ]|
-- |[Save]|
--Save changes and go back to the main menu.
FlexMenu_RegisterButton("Save changes")

	--Description.
	FlexButton_SetProperty("Description", "Save changes and return to the main menu.")

	StarButton_SetProperty("Priority", 10000)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Exit And Save.lua", 0)
DL_PopActiveObject()

-- |[Back]|
--Go back to the main menu.
FlexMenu_RegisterButton("Exit without saving")

	--Description.
	FlexButton_SetProperty("Description", "Return to the main menu without saving changes.")

	StarButton_SetProperty("Priority", 10001)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "101 Exit No Save.lua", 0)
DL_PopActiveObject()
