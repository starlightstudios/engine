///========================================= Hit Area =============================================
//--General purpose functions designed to randomly create "hits" over a given area. These come in several
//  flavours depending on what is needed. Examples of the hit areas are below.
//--"Hits" are lists of Point2DI structures where 0 is the center of the hit area. Numbers can be negative
//  to indicate left and up, positive for right and down.

//--Unless otherwise specified, all lists returned do not have duplicate hits. A linked-list is returned
//  containing sets of hits that will be in no particular order. If more hits are requested than there are
//  valid spaces, less than the requested hits will be returned. If the distance is zero, only the center
//  will be returned, and negative numbers will return empty lists.

//--If a pSkipZero flag is provided, the list will never contain a {0,0}. This is because some algorithms
//  may want to hit 3 locations but one of them is always {0,0}, in which case you'd use this flag and generate
//  2 hits and manually plug in the 0 yourself.

///--[ ======= Shape Examples ======= ]
///--[Cross]
//  Size 1      Size 2      Size 3
//                             X
//                  X         XXX
//     X           XXX       XXXXX
//    XPX         XXPXX     XXXPXXX
//     X           XXX       XXXXX
//                  X         XXX
//                             X

///--[Fat Cross]
//  Size 1      Size 2      Size 3
//
//                            XXX
//                XXX        XXXXX
//     X         XXXXX      XXXXXXX
//    XPX        XXPXX      XXXPXXX
//     X         XXXXX      XXXXXXX
//                XXX        XXXXX
//                            XXX
//

///--[ ==== Forward Declarations ==== ]
#pragma once

#include "Structures.h"
class StarLinkedList;

///--[ === Functions Declarations === ]
StarLinkedList *HitXLocationsCross(int pDistance, int pCount, bool pSkipZero);
