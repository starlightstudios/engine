///====================================== MonoUICampfire ==========================================
//--Monoceros version of the campfire menu. Modified for alternate layout.

#pragma once

///========================================= Includes =============================================
#include "AdvUICampfire.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define MONOUICAMPFIRE_STRINGTRANS_TOTAL 2
#define MONOUICAMPFIRE_HELPTRANS_TOTAL 5

///======================================== Translation ===========================================
///--[MonoUICampfire_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUICAMPFIRE_STRINGS_TOTAL 10
class MonoUICampfire_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUICAMPFIRE_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextCampfireMenu;
            char *mTextWarp;
            char *mTextSave;
            char *mTextPassword;
            char *mTextHelp[5];
            char *mTextControlShowHelp;
        }str;
    };

    //--Functions
    MonoUICampfire_Strings();
    virtual ~MonoUICampfire_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUICampfire : public MonoUICommon, virtual public AdvUICampfire
{
    protected:
    ///--[Constants]
    static const int cxMonoHelpStrings = 5;

    ///--[System]
    ///--[Images]
    struct
    {
        //--Fonts.
        StarFont *rDoubleHeadingFont;
        StarFont *rFont_Control;

        //--Images
        StarBitmap *rTitleBack;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUICampfire();
    virtual ~MonoUICampfire();
    virtual void Construct();

    //--Public Variables
    static MonoUICampfire_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();

    private:
    //--Private Core Methods
    public:
    //--Update

    //--File I/O
    //--Drawing
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


