///========================================= AdvUIGrid ============================================
//--A grid component that can be attached to any UI. The grid is expandable and can be navigated in
//  four directions using the arrow keys/directional pad. Grid entries can have images, have a cursor
//  around them, and can change size when highlighted.
//--The Base UI for Adventure mode uses these, and some submodes like Warp use them as well.

#pragma once

///========================================= Includes =============================================
#include "GridHandler.h"

///===================================== Local Structures =========================================
typedef struct StoredGrid
{
    //--Members.
    int mCursorX;
    int mCursorY;
    int mGridSizeX;
    int mGridSizeY;
    GridPackage ***mGridEntries;

    //--Functions.
    void Initialize()
    {
        mCursorX = 0;
        mCursorY = 0;
        mGridSizeX = 0;
        mGridSizeY = 0;
        mGridEntries = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        //--Error check.
        if(!pPtr) return;

        //--Cast, deallocate.
        StoredGrid *rPtr = (StoredGrid *)pPtr;
        for(int x = 0; x < rPtr->mGridSizeX; x ++)
        {
            for(int y = 0; y < rPtr->mGridSizeY; y ++)
            {
                free(rPtr->mGridEntries[x][y]);
            }
            free(rPtr->mGridEntries[x]);
        }
        free(rPtr->mGridEntries);
        free(rPtr);
    }
}StoredGrid;


///===================================== Local Definitions ========================================
#define GRID_CODE_NONE -1

///========================================== Classes =============================================
class AdvUIGrid
{
    protected:
    ///--[External Constants]
    int cCursorMoveTicks;
    int cGridTimerMin;              //Lowest tick value for a selected grid entry, usually 0.
    int cGridTimerMax;              //Highest tick value for a selected grid entry.
    float cGridSizeBoost;           //When a grid entry is 100% selected, this is how much larger it is than normal, by percent.
    float cGridIconScale;           //If grid icons are smaller than the standard size (96) then this scale is applied to resize them.
    StarlightColor cGridNameColor;  //The color used to render names of grid entries.

    ///--[System]
    int mPulseValue;

    ///--[Cursor]
    int mGridCurrentX;
    int mGridCurrentY;
    int mGridCenterX;
    int mGridCenterY;

    ///--[Positioning]
    float mGridRenderCenterX;
    float mGridRenderCenterY;
    float mGridRenderSpacingX;
    float mGridRenderSpacingY;

    ///--[Grid Storage]
    //--Active Grid.
    int mGridSizeX;
    int mGridSizeY;
    GridPackage ***mGridEntries;

    //--List of stored grids, excluding active grid.
    char *mActiveGridName;
    StarLinkedList *mStoredGridList; //StoredGrid *, master

    ///--[Rendering]
    StarFont *rFont_Name;

    protected:
    public:
    //--System
    AdvUIGrid();
    virtual ~AdvUIGrid();

    //--Public Variables
    //--Property Queries
    int GetGridCodeAt(int pX, int pY);
    int GetGridCodeAtCursor();
    GridPackage *GetNextPriorityEntry();

    //--Manipulators
    void AllocateGrid(int pSizeX, int pSizeY);
    void ProcessGrid();
    virtual void QuicksetGridEntry(int pX, int pY, int pCode, const char *pName, StarBitmap *pImagePtr);
    void SetCursorByCode(int pCode);
    void CreateStoredGrid(const char *pName);
    void ActivateStoredGrid(const char *pName);
    void ClearStoredGrids();

    //--Core Methods
    void AutoAllocateSquareGrid(int pElements);
    void RecomputeGridCursorPosition(EasingPack2D &sHighlightPos, EasingPack2D &sHighlightSize, float pVisPercent);

    private:
    //--Private Core Methods
    public:
    //--Update
    bool UpdateGridControls();
    void UpdateGridTimers();

    //--File I/O
    //--Drawing
    bool ResolveEntryRenderData(GridPackage *pEntry, float &sXPos, float &sYPos, float &sScale, float &sBoostPct, float pVisAlpha);
    void RenderGrid(float pVisAlpha);
    virtual void RenderGridEntryIcon(GridPackage *pEntry, float pVisAlpha);
    virtual void RenderGridEntryName(GridPackage *pEntry, float pVisAlpha);

    //--Pointer Routing
    GridPackage *GetPackageAt(int pX, int pY);
    GridPackage *GetPackageAtCursor();

    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

