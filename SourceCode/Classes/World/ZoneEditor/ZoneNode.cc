//--Base
#include "ZoneNode.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
ZoneNode::ZoneNode()
{
    ///--[ZoneNode]
    //--System
    mLocalName = InitializeString("New Node");
    mNodeUniqueID = 0;

    //--Position
    mX = 0.0f;
    mY = 0.0f;
    mZ = 0.0f;

    //--Color
    mLocalColor.SetRGBAI(rand() % 255, rand() % 255, rand() % 255, 255);

    //--Directions
    mFacingList = new StarLinkedList(true);

    //--Automatic Transition Sets
    mImmediateTransitionList = new StarLinkedList(true);
}
ZoneNode::~ZoneNode()
{
    free(mLocalName);
    delete mFacingList;
    delete mImmediateTransitionList;
}
void ZoneNode::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((ZoneNode *)pPtr);
}

///===================================== Property Queries =========================================
uint32_t ZoneNode::GetNodeID()
{
    return mNodeUniqueID;
}
const char *ZoneNode::GetName()
{
    return (const char *)mLocalName;
}
float ZoneNode::GetX()
{
    return mX;
}
float ZoneNode::GetY()
{
    return mY;
}
float ZoneNode::GetZ()
{
    return mZ;
}
int ZoneNode::GetFacingsTotal()
{
    return mFacingList->GetListSize();
}
FacingInfo *ZoneNode::GetFacing(int pSlot)
{
    return (FacingInfo *)mFacingList->GetElementBySlot(pSlot);
}
FacingInfo *ZoneNode::GetFacingByAngle(float pAngle)
{
    //--Search the facing list for a facing that is within 1 degree of the provided angle.
    while(pAngle <    0.0f) pAngle = pAngle + 360.0f;
    while(pAngle >= 360.0f) pAngle = pAngle - 360.0f;

    //--Search.
    FacingInfo *rFacing = (FacingInfo *)mFacingList->PushIterator();
    while(rFacing)
    {
        //--The epsilon is 1.0f. No two facings should be that close!
        if(fabsf(rFacing->mZRotation - pAngle) < 1.0f)
        {
            mFacingList->PopIterator();
            return rFacing;
        }

        //--Next facing.
        rFacing = (FacingInfo *)mFacingList->AutoIterate();
    }

    //--Facing not found.
    return NULL;
}
FacingInfo *ZoneNode::GetFacingByDestination(uint32_t pTargetNodeID)
{
    //--Search the facing list for a facing which looks at the given node, and returns it. Returns NULL if not found.
    FacingInfo *rFacing = (FacingInfo *)mFacingList->PushIterator();
    while(rFacing)
    {
        if(rFacing->mNodeDestination == pTargetNodeID)
        {
            mFacingList->PopIterator();
            return rFacing;
        }

        //--Next facing.
        rFacing = (FacingInfo *)mFacingList->AutoIterate();
    }

    //--Facing not found.
    return NULL;
}
FacingInfo *ZoneNode::GetNearestFacingByAngle(float pAngle)
{
    //--Returns the facing which has the angle closest to the one provided. Can only return NULL if there are
    //  no facings at all.
    if(mFacingList->GetListSize() < 1) return NULL;

    //--Clamp.
    while(pAngle <    0.0f) pAngle = pAngle + 360.0f;
    while(pAngle >= 360.0f) pAngle = pAngle - 360.0f;

    //--Setup.
    float tClosest = 0.0f;
    FacingInfo *rClosestFacing = NULL;

    //--Search.
    FacingInfo *rFacing = (FacingInfo *)mFacingList->PushIterator();
    while(rFacing)
    {
        //--Compute.
        float tDif = fabsf(rFacing->mZRotation - pAngle);
        if(tDif >= 180.0f) tDif = 360.0f - tDif;

        //--If this hasn't been set, or it's closer, it becomes the new closest.
        if(!rClosestFacing || tDif < tClosest)
        {
            tClosest = tDif;
            rClosestFacing = rFacing;
        }

        //--Next facing.
        rFacing = (FacingInfo *)mFacingList->AutoIterate();
    }

    //--Pass back whatever facing we found.
    return rClosestFacing;
}
int ZoneNode::GetSlotOfFacing(void *pPtr)
{
    //--Returns -1 if the entry is not on the list.
    return mFacingList->GetSlotOfElementByPtr(pPtr);
}
int ZoneNode::GetAutoTransitionsTotal()
{
    return mImmediateTransitionList->GetListSize();
}
AutoTransitionInfo *ZoneNode::GetAutoTransition(int pSlot)
{
    return (AutoTransitionInfo *)mImmediateTransitionList->GetElementBySlot(pSlot);
}
AutoTransitionInfo *ZoneNode::GetAutoTransitionFrom(uint32_t pFromNode)
{
    //--Returns the AutoTransition which has its "From" as the requested node. Can return NULL.
    if(!pFromNode) return NULL;

    //--Scan.
    AutoTransitionInfo *rInfo = (AutoTransitionInfo *)mImmediateTransitionList->PushIterator();
    while(rInfo)
    {
        if(rInfo->mPreviousNode == pFromNode)
        {
            mImmediateTransitionList->PopIterator();
            return rInfo;
        }

        //--Next.
        rInfo = (AutoTransitionInfo *)mImmediateTransitionList->AutoIterate();
    }

    //--No match was found.
    return NULL;
}

///======================================= Manipulators ===========================================
void ZoneNode::SetUniqueID(uint32_t pID)
{
    //--Note: Setting to 0 will "clear" the ID. Nodes cannot normally have an ID of 0.
    mNodeUniqueID = pID;
}
void ZoneNode::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void ZoneNode::SetPosition(float pX, float pY, float pZ)
{
    mX = pX;
    mY = pY;
    mZ = pZ;
}
void ZoneNode::AddFacing(float pXRot, float pYRot, float pZRot)
{
    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    FacingInfo *nNewFacing = (FacingInfo *)starmemoryalloc(sizeof(FacingInfo));
    nNewFacing->mXRotation = pXRot;
    nNewFacing->mYRotation = pYRot;
    nNewFacing->mZRotation = pZRot;
    nNewFacing->mNodeDestination = 0;

    //--Z Rotation cannot loop.
    while(nNewFacing->mZRotation <    0.0f) nNewFacing->mZRotation = nNewFacing->mZRotation + 360.0f;
    while(nNewFacing->mZRotation >= 360.0f) nNewFacing->mZRotation = nNewFacing->mZRotation - 360.0f;

    //--Register.
    mFacingList->AddElement("X", nNewFacing, &FreeThis);
}
void ZoneNode::SetFacingDirection(int pSlot, float pXRot, float pYRot, float pZRot)
{
    //--Check.
    FacingInfo *rInfo = (FacingInfo *)mFacingList->GetElementBySlot(pSlot);
    if(!rInfo) return;

    //--Set.
    rInfo->mXRotation = pXRot;
    rInfo->mYRotation = pYRot;
    rInfo->mZRotation = pZRot;

    //--Z Rotation cannot loop.
    while(rInfo->mZRotation <    0.0f) rInfo->mZRotation = rInfo->mZRotation + 360.0f;
    while(rInfo->mZRotation >= 360.0f) rInfo->mZRotation = rInfo->mZRotation - 360.0f;
}
void ZoneNode::SetFacingConnection(int pSlot, uint32_t pID)
{
    //--Check.
    FacingInfo *rInfo = (FacingInfo *)mFacingList->GetElementBySlot(pSlot);
    if(!rInfo) return;

    //--Set.
    rInfo->mNodeDestination = pID;
}
void ZoneNode::RemoveFacing(int pSlot)
{
    //--Removes the facing in the given slot, or does nothing if out of range. If the facing is removed,
    //  then any automatic transitions matching its destination are also removed.
    FacingInfo *rFacingInfo = (FacingInfo *)mFacingList->GetElementBySlot(pSlot);
    if(!rFacingInfo) return;

    //--Look for transitions which match, and remove them.
    AutoTransitionInfo *rInfo = (AutoTransitionInfo *)mImmediateTransitionList->SetToHeadAndReturn();
    while(rInfo)
    {
        //--Match? Remove.
        if(rFacingInfo->mNodeDestination == rInfo->mNodeDestination)
        {
            mImmediateTransitionList->RemoveRandomPointerEntry();
        }

        //--Next.
        rInfo = (AutoTransitionInfo *)mImmediateTransitionList->IncrementAndGetRandomPointerEntry();
    }

    //--Now remove the element.
    mFacingList->RemoveElementI(pSlot);
}
bool ZoneNode::AddAutoTransition(uint32_t pZoneFrom, uint32_t pZoneTo)
{
    //--Creates and stores a new automatic transition. Note that pZoneTo must already exist as a
    //  valid facing transition, if not the transition will not be stored.
    //--First, neither can be 0. If either is zero, it's an illegal transition.
    //--Returns false if no transition was added, true if one was added.
    if(!pZoneFrom || !pZoneTo) return false;

    //--Look for a Facing which goes to pZoneTo. It must exist!
    FacingInfo *rFacing = (FacingInfo *)mFacingList->PushIterator();
    while(rFacing)
    {
        //--If the destination matches the target zone...
        if(rFacing->mNodeDestination == pZoneTo)
        {
            //--Create a transition entity.
            SetMemoryData(__FILE__, __LINE__);
            AutoTransitionInfo *nNewInfo = (AutoTransitionInfo *)starmemoryalloc(sizeof(AutoTransitionInfo));
            nNewInfo->mIsTeleport = false;
            nNewInfo->mPreviousNode = pZoneFrom;
            nNewInfo->mNodeDestination = pZoneTo;
            mImmediateTransitionList->AddElement("X", nNewInfo, &FreeThis);

            //--All done.
            mFacingList->PopIterator();
            return true;
        }

        //--Next.
        rFacing = (FacingInfo *)mFacingList->AutoIterate();
    }

    //--If we got this far, the transition could not be added.
    return false;
}
void ZoneNode::RemoveAutoTransition(int pSlot)
{
    //--Deletes the auto-transition in the given slot. Destabilizes iterators!
    mImmediateTransitionList->RemoveElementI(pSlot);
}
void ZoneNode::RemoveIllegalAutoTransitions()
{
    //--Removes all auto-transitions which point to node ID 0 as either destination or source.
    //  This is usually used after the transitions were flagged for removal during iteration.
    AutoTransitionInfo *rInfo = (AutoTransitionInfo *)mImmediateTransitionList->SetToHeadAndReturn();
    while(rInfo)
    {
        if(rInfo->mNodeDestination == 0 || rInfo->mPreviousNode == 0)
        {
            mImmediateTransitionList->RemoveRandomPointerEntry();
        }
        rInfo = (AutoTransitionInfo *)mImmediateTransitionList->IncrementAndGetRandomPointerEntry();
    }
}
void ZoneNode::ToggleTeleportCase()
{
    //--If there's no auto-transitions, does nothing. Otherwise, sets all auto-transitions to whatever
    //  the reverse of the 0th transition's teleport flag is.
    if(mImmediateTransitionList->GetListSize() < 1) return;

    //--Loop.
    bool tHasSet = false;
    bool tSetToThis = false;
    AutoTransitionInfo *rInfo = (AutoTransitionInfo *)mImmediateTransitionList->PushIterator();
    while(rInfo)
    {
        if(!tHasSet) {tHasSet = true; tSetToThis = !rInfo->mIsTeleport; }
        rInfo->mIsTeleport = tSetToThis;
        rInfo = (AutoTransitionInfo *)mImmediateTransitionList->AutoIterate();
    }
}

///======================================= Core Methods ===========================================
void ZoneNode::ActivateColorMixer()
{
    mLocalColor.SetAsMixer();
}
void ZoneNode::ActivateReverseColorMixer()
{
    glColor3f(1.0f - mLocalColor.r, 1.0f - mLocalColor.g, 1.0f - mLocalColor.b);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
void ZoneNode::WriteToFile(FILE *fOutfile)
{
    //--[Setup]
    //--Appends the information in this ZoneNode to the given file at whatever cursor it has.
    if(!fOutfile) return;

    //--Use a buffer to specify the information's size in advance.
    StarAutoBuffer *tAutoBuffer = new StarAutoBuffer();

    //--[Build]
    //--Write the name.
    tAutoBuffer->AppendStringWithLen(mLocalName);

    //--Write the UniqueID. IDs can be written out of order since nodes can be deleted and created.
    tAutoBuffer->AppendVoidData(&mNodeUniqueID, sizeof(uint32_t));

    //--Position.
    tAutoBuffer->AppendFloat(mX);
    tAutoBuffer->AppendFloat(mY);
    tAutoBuffer->AppendFloat(mZ);

    //--Write how many facings we have. Can be zero, can't be over 128.
    tAutoBuffer->AppendCharacter(mFacingList->GetListSize());
    for(int i = 0; i < mFacingList->GetListSize(); i ++)
    {
        FacingInfo *rInfo = (FacingInfo *)mFacingList->GetElementBySlot(i);
        tAutoBuffer->AppendVoidData(rInfo, sizeof(FacingInfo));
    }

    //--Write automatic transition information.
    tAutoBuffer->AppendCharacter(mImmediateTransitionList->GetListSize());
    for(int i = 0; i < mImmediateTransitionList->GetListSize(); i ++)
    {
        AutoTransitionInfo *rInfo = (AutoTransitionInfo *)mImmediateTransitionList->GetElementBySlot(i);
        tAutoBuffer->AppendVoidData(rInfo, sizeof(AutoTransitionInfo));
    }

    //--[Write]
    //--Write the results of the autobuffer to the file.
    int tDataLen = tAutoBuffer->GetCursor();
    uint8_t *rDataBuf = tAutoBuffer->GetRawData();
    fwrite(rDataBuf, sizeof(uint8_t), tDataLen, fOutfile);

    //--Clean.
    delete tAutoBuffer;
}
bool ZoneNode::ReadFromFile(FILE *fInfile)
{
    //--Reads information for a ZoneNode from the given file, assuming the cursor is in position.
    //  Overwrites whatever information is contained in this node.
    //--Returns true if everything went well, false on error.
    if(!fInfile) return false;

    //--Clear name data.
    free(mLocalName);
    mLocalName = NULL;
    mFacingList->ClearList();
    mImmediateTransitionList->ClearList();

    //--Read the name. It is a string with a single byte at the front indicating its length.
    uint8_t tNameLen;
    fread(&tNameLen, sizeof(uint8_t), 1, fInfile);

    //--If the name length was less than 1, somehow, that's an error.
    if(tNameLen < 1)
    {
        ResetString(mLocalName, "Erroneous Node");
        return false;
    }

    //--Read the name's letters.
    SetMemoryData(__FILE__, __LINE__);
    mLocalName = (char *)starmemoryalloc(sizeof(char) * (tNameLen + 1));
    fread(mLocalName, sizeof(char), tNameLen, fInfile);
    mLocalName[tNameLen] = '\0';

    //--Read the NodeUniqueID. This is how other nodes refer to this one.
    fread(&mNodeUniqueID, sizeof(uint32_t), 1, fInfile);

    //--Read the position.
    fread(&mX, sizeof(float), 1, fInfile);
    fread(&mY, sizeof(float), 1, fInfile);
    fread(&mZ, sizeof(float), 1, fInfile);

    //--Read the facings. First the total, then one facing for each.
    uint8_t tTotalFacings = 0;
    fread(&tTotalFacings, sizeof(uint8_t), 1, fInfile);
    for(int i = 0; i < tTotalFacings; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        FacingInfo *nNewInfo = (FacingInfo *)starmemoryalloc(sizeof(FacingInfo));
        fread(nNewInfo, sizeof(FacingInfo), 1, fInfile);
        mFacingList->AddElement("X", nNewInfo, &FreeThis);
    }

    //--Read the automatic transition listing.
    bool tModifyPositionForTeleport = false;
    uint8_t tTotalTransitions = 0;
    fread(&tTotalTransitions, sizeof(uint8_t), 1, fInfile);
    for(int i = 0; i < tTotalTransitions; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        AutoTransitionInfo *nNewInfo = (AutoTransitionInfo *)starmemoryalloc(sizeof(AutoTransitionInfo));
        fread(nNewInfo, sizeof(AutoTransitionInfo), 1, fInfile);
        mImmediateTransitionList->AddElement("X", nNewInfo, &FreeThis);
        if(nNewInfo->mIsTeleport) tModifyPositionForTeleport = true;
    }

    //--If flagged, then the X/Y position becomes modulated by 16. This makes lining up teleports easier.
    if(tModifyPositionForTeleport)
    {
        mX = mX - fmodf(mX, 16.0f);
        mY = mY - fmodf(mY, 16.0f);
    }

    //--Everything is okay, return true.
    return true;
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
