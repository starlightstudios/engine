-- |[ ======================================= Basic Math ======================================= ]|
--Math functions not implemented in the basic Lua libraries.

-- |[Sign]|
--Returns positive or negative one based on the sign of the passed number.
function math.sign(v)
	return (v >= 0 and 1) or -1
end

-- |[Round]|
--Rounds to the nearest integer.
function math.round(v, bracket)
	bracket = bracket or 1
	return math.floor(v/bracket + math.sign(v) * 0.5) * bracket
end