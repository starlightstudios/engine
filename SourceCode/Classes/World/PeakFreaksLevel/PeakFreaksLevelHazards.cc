//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "PeakFreaksPlayer.h"
#include "TileLayer.h"

//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--Libraries
//--Managers

///========================================== Sliders =============================================
///--[Local Definitions]
///--[Structure Functions]
void PF_Slider::Initialize()
{
    //--Current Position
    mTripleTimer = false;
    mTimer = PF_SLIDER_TRACE_TICKS+1;
    mXPos = 0;
    mYPos = 0;
    mOldXPos = -1;
    mOldYPos = -1;

    //--Tracks
    mXStart = 0;
    mYStart = 0;
    mXEnd = 0;
    mYEnd = 0;

    //--Bumping
    mBumpDirX = 0;
    mBumpDirY = 0;

    //--Rendering
    rRenderImg = NULL;
}
void PF_Slider::Update()
{
    ///--[Documentation]
    //--Updates the given slider. Sliders move when their timer reaches PF_SLIDER_MOVE_TICKS, and switch
    //  directions at the end of their "track".
    mTimer ++;
    if(mTimer < PF_SLIDER_MOVE_TICKS) return;

    //--If this flag is set, triple the number of wait ticks.
    if(mTripleTimer && mTimer < PF_SLIDER_MOVE_TICKS * 3) return;

    ///--[Timers]
    //--Moving. Reset timer.
    mTripleTimer = false;
    mTimer = 0;

    ///--[Collision]
    //--Unset collisions at our current position.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    ///--[Movement]
    //--Move one tile along the "bump dir".
    mOldXPos = mXPos;
    mOldYPos = mYPos;
    mXPos = mXPos + (mBumpDirX * TileLayer::cxSizePerTile);
    mYPos = mYPos + (mBumpDirY * TileLayer::cxSizePerTile);

    ///--[Bump Player]
    //--Player bump. Handles the rival as well.
    int tClipX = mXPos / TileLayer::cxSizePerTile;
    int tClipY = mYPos / TileLayer::cxSizePerTile;
    rActiveLevel->BumpPlayerAbsolute(tClipX, tClipY, tClipX + mBumpDirX, tClipY + mBumpDirY);

    ///--[Switchback Case]
    //--If this is the end position:
    if(mXPos == mXEnd && mYPos == mYEnd)
    {
        //--Flag.
        mTripleTimer = true;

        //--Switch Start/End Positions.
        int tTempX = mXStart;
        int tTempY = mYStart;
        mXStart = mXEnd;
        mYStart = mYEnd;
        mXEnd = tTempX;
        mYEnd = tTempY;

        //--Switch the bump directions.
        mBumpDirX = mBumpDirX * -1;
        mBumpDirY = mBumpDirY * -1;
    }
}
void PF_Slider::Render()
{
    ///--[Documentation]
    //--Renders the given slider. Sliders shake right before they move.
    if(!rRenderImg) return;

    ///--[Trace Rendering]
    //--For the first few ticks after a move, render "traces".
    if(mTimer <= PF_SLIDER_TRACE_TICKS)
    {
        //--Transparency.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.30f);

        //--Previous position to next position, create a series.
        for(int i = 0; i < PF_SLIDER_TRACE_TICKS; i ++)
        {
            float tXPosition = mOldXPos + ((mXPos - mOldXPos) * (0.33f * (float)i));
            float tYPosition = mOldYPos + ((mYPos - mOldYPos) * (0.33f * (float)i));
            rRenderImg->Draw(tXPosition, tYPosition);
        }

        //--Clean.
        StarlightColor::ClearMixer();
    }

    ///--[Normal Render]
    //--Compute position.
    float tRenderX = mXPos;
    float tRenderY = mYPos;
    if((!mTripleTimer && mTimer >= PF_SLIDER_SHAKE_TICKS) || (mTripleTimer && mTimer >= PF_SLIDER_SHAKE_TICKS + (PF_SLIDER_MOVE_TICKS * 2)))
    {
        int tXOffset = ((mTimer - PF_SLIDER_SHAKE_TICKS) % 3) - 1;
        tRenderX = tRenderX + tXOffset;
    }

    //--Render.
    rRenderImg->Draw(tRenderX, tRenderY);
}
void PF_Slider::RenderIceBeam(SugarBitmap *pImage)
{
    if(!pImage) return;
    float tRenderX = mXPos;
    float tRenderY = mYPos;
    pImage->Draw(tRenderX, tRenderY);
}

///========================================= Spinners =============================================
///--[Local Definitions]
#define PF_SPINNER_SHAKE_TICKS 45
#define PF_SPINNER_MOVE_TICKS 60

///--[Local Structures]
typedef struct
{
    int mXOff;
    int mYOff;
    int mXBump;
    int mYBump;
    void Set(int pX, int pY, int pBumpX, int pBumpY)
    {
        mXOff = pX;
        mYOff = pY;
        mXBump = pBumpX;
        mYBump = pBumpY;
    }
}PF_Bump;

///--[Structure Functions]
void PF_Spinner::Initialize()
{
    //--Current position.
    mTimer = 0;
    mXPos = 0;
    mYPos = 0;

    //--Spinning.
    mIsBig = false;
    mIsClockwise = false;
    mSpinPos = 0;

    //--Rendering.
    memset(rRenderImg, 0, sizeof(SugarBitmap *) * PF_SPINNER_SMALL_DIRS);
}
void PF_Spinner::Update()
{
    ///--[Documentation]
    //--Updates the given slider. Sliders move when their timer reaches PF_SLIDER_MOVE_TICKS, and switch
    //  directions at the end of their "track".
    if(mIsBig) { UpdateBig(); return; }

    mTimer ++;
    if(mTimer < PF_SPINNER_MOVE_TICKS) return;

    ///--[Timers]
    //--Moving. Reset timer.
    mTimer = 0;

    //--If clockwise:
    if(mIsClockwise)
    {
        mSpinPos ++;
        if(mSpinPos >= PF_SPINNER_SMALL_DIRS) mSpinPos = 0;
    }
    //--Counter-clockwise.
    else
    {
        mSpinPos --;
        if(mSpinPos < 0) mSpinPos = PF_SPINNER_SMALL_DIRS - 1;
    }

    ///--[Bumping]
    //--Figure out what spot is being bumped and what direction to send the player if bumped.
    int tBumpX = (mXPos / TileLayer::cxSizePerTile);
    int tBumpY = (mYPos / TileLayer::cxSizePerTile);
    int tBumpDirX = 0;
    int tBumpDirY = 0;

    //--If clockwise:
    if(mIsClockwise)
    {
        if(mSpinPos == 0)
        {
            tBumpX --;
            tBumpDirY = -1;
        }
        else if(mSpinPos == 1)
        {
            tBumpX --;
            tBumpY --;
            tBumpDirX = 1;
        }
        else if(mSpinPos == 2)
        {
            tBumpY --;
            tBumpDirX = 1;
        }
        else if(mSpinPos == 3)
        {
            tBumpX ++;
            tBumpY --;
            tBumpDirY = 1;
        }
        else if(mSpinPos == 4)
        {
            tBumpX ++;
            tBumpDirY = 1;
        }
        else if(mSpinPos == 5)
        {
            tBumpX ++;
            tBumpY ++;
            tBumpDirX = -1;
        }
        else if(mSpinPos == 6)
        {
            tBumpY ++;
            tBumpDirX = -1;
        }
        else if(mSpinPos == 7)
        {
            tBumpX --;
            tBumpY ++;
            tBumpDirY = -1;
        }
    }
    //--Counter-clockwise.
    else
    {
        if(mSpinPos == 0)
        {
            tBumpX --;
            tBumpDirY = 1;
        }
        else if(mSpinPos == 1)
        {
            tBumpX --;
            tBumpY --;
            tBumpDirY = 1;
        }
        else if(mSpinPos == 2)
        {
            tBumpY --;
            tBumpDirX = -1;
        }
        else if(mSpinPos == 3)
        {
            tBumpX ++;
            tBumpY --;
            tBumpDirX = -1;
        }
        else if(mSpinPos == 4)
        {
            tBumpX ++;
            tBumpDirY = -1;
        }
        else if(mSpinPos == 5)
        {
            tBumpX ++;
            tBumpY ++;
            tBumpDirY = -1;
        }
        else if(mSpinPos == 6)
        {
            tBumpY ++;
            tBumpDirX = 1;
        }
        else if(mSpinPos == 7)
        {
            tBumpX --;
            tBumpY ++;
            tBumpDirX = 1;
        }
    }

    ///--[Bump Player]
    //--Player bump. Handles the rival as well.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;
    rActiveLevel->BumpPlayerAbsolute(tBumpX, tBumpY, tBumpX + tBumpDirX, tBumpY + tBumpDirY);
}
void PF_Spinner::UpdateBig()
{
    ///--[Documentation]
    //--Handles rotating a spinner when it's "Big". This requires a different set of instructions since there's
    //  two wheels and only four states.
    mTimer ++;
    if(mTimer < PF_SPINNER_MOVE_TICKS) return;

    ///--[Timers]
    //--Moving. Reset timer.
    mTimer = 0;

    //--If clockwise:
    if(mIsClockwise)
    {
        mSpinPos ++;
        if(mSpinPos >= PF_SPINNER_BIG_DIRS) mSpinPos = 0;
    }
    //--Counter-clockwise.
    else
    {
        mSpinPos --;
        if(mSpinPos < 0) mSpinPos = PF_SPINNER_BIG_DIRS - 1;
    }

    ///--[Bumping]
    //--Figure out what spot is being bumped and what direction to send the player if bumped.
    //  Up to 6 tiles can be bumped at a time.
    PF_Bump tBumps[6];

    //--If clockwise:
    if(mIsClockwise)
    {
        if(mSpinPos == 0)
        {
            tBumps[0].Set( 1,  0,  1,  1);
            tBumps[1].Set( 2, -1,  2,  1);
            tBumps[2].Set( 2,  0,  2,  1);
            tBumps[3].Set(-2,  0, -2, -1);
            tBumps[4].Set(-2,  1, -2, -1);
            tBumps[5].Set(-1,  0, -1, -1);
        }
        else if(mSpinPos == 1)
        {
            tBumps[0].Set(-2, -1, -1, -2);
            tBumps[1].Set(-2, -2, -2, -2);
            tBumps[2].Set(-1, -1,  0, -1);
            tBumps[3].Set( 1,  1,  0,  1);
            tBumps[4].Set( 2,  2,  1,  2);
            tBumps[5].Set( 2,  1,  1,  2);
        }
        else if(mSpinPos == 2)
        {
            tBumps[0].Set(-1, -2,  1, -2);
            tBumps[1].Set( 0, -2,  1, -2);
            tBumps[2].Set( 0, -1,  1, -1);
            tBumps[3].Set( 0,  1, -1,  1);
            tBumps[4].Set( 0,  2,  1,  2);
            tBumps[5].Set( 1,  2, -1,  2);
        }
        else if(mSpinPos == 3)
        {
            tBumps[0].Set( 1, -2,  2, -1);
            tBumps[1].Set( 2, -2,  2, -1);
            tBumps[2].Set( 1, -1,  1,  0);
            tBumps[3].Set(-1,  1, -1,  0);
            tBumps[4].Set(-2,  2, -2,  1);
            tBumps[5].Set(-1,  2, -2,  1);
        }
    }
    //--Counter-clockwise.
    else
    {
        if(mSpinPos == 0)
        {
            tBumps[0].Set(-2, -1, -2,  1);
            tBumps[1].Set(-2,  0, -2,  1);
            tBumps[2].Set(-1,  0, -1,  1);
            tBumps[3].Set( 1,  0,  1, -1);
            tBumps[4].Set( 2,  0,  2, -1);
            tBumps[5].Set( 2,  1,  2, -1);
        }
        else if(mSpinPos == 1)
        {
            tBumps[0].Set(-2, -2, -2, -1);
            tBumps[1].Set(-1, -2, -2, -1);
            tBumps[2].Set(-1, -1, -1,  0);
            tBumps[3].Set( 1,  1,  1,  0);
            tBumps[4].Set( 1,  2,  2,  1);
            tBumps[5].Set( 2,  2,  2,  1);
        }
        else if(mSpinPos == 2)
        {
            tBumps[0].Set( 1, -2, -1, -2);
            tBumps[1].Set( 0, -2, -1, -2);
            tBumps[2].Set( 0, -1, -1, -1);
            tBumps[3].Set( 0,  1,  1,  1);
            tBumps[4].Set( 0,  2,  1,  2);
            tBumps[5].Set(-1,  2,  1,  2);
        }
        else if(mSpinPos == 3)
        {
            tBumps[0].Set( 2, -1,  1, -2);
            tBumps[1].Set( 2, -2,  1, -2);
            tBumps[2].Set( 1, -1,  0, -1);
            tBumps[3].Set(-1,  1,  0,  1);
            tBumps[4].Set(-2,  2, -1,  2);
            tBumps[5].Set(-2,  1, -1,  2);
        }
    }

    ///--[Bump Player]
    //--Player bump. Handles the rival as well.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--There are several possible bumps for each rotation.
    int tThisX = mXPos / TileLayer::cxSizePerTile;
    int tThisY = mYPos / TileLayer::cxSizePerTile;
    for(int i = 0; i < 6; i ++)
    {
        rActiveLevel->BumpPlayerAbsolute(tThisX+tBumps[i].mXOff, tThisY+tBumps[i].mYOff, tThisX + tBumps[i].mXBump, tThisY + tBumps[i].mYBump);
    }
}
void PF_Spinner::Render()
{
    ///--[Documentation]
    //--Renders the given spinner. Spinners vibrate before they rotate.
    SugarBitmap *rCurRenderImg = rRenderImg[mSpinPos];
    if(!rCurRenderImg) return;

    ///--[Normal Render]
    //--Compute position.
    float tRenderX = mXPos - TileLayer::cxSizePerTile;
    float tRenderY = mYPos - TileLayer::cxSizePerTile;
    if(mTimer >= PF_SPINNER_SHAKE_TICKS)
    {
        int tXOffset = ((mTimer - PF_SPINNER_SHAKE_TICKS) % 3) - 1;
        tRenderX = tRenderX + tXOffset;
    }

    //--If "Big", subtract another tile.
    if(mIsBig)
    {
        tRenderX = tRenderX - TileLayer::cxSizePerTile;
        tRenderY = tRenderY - TileLayer::cxSizePerTile;
    }

    //--Render.
    rCurRenderImg->Draw(tRenderX, tRenderY);
}
void PF_Spinner::RenderIceBeam(SugarBitmap *pImage)
{
    //--Error check.
    if(!pImage) return;

    //--Setup.
    float tRenderX = mXPos;// - TileLayer::cxSizePerTile;
    float tRenderY = mYPos;// - TileLayer::cxSizePerTile;

    //--If "Big", subtract another tile.
    if(mIsBig)
    {
        //tRenderX = tRenderX - TileLayer::cxSizePerTile;
        //tRenderY = tRenderY - TileLayer::cxSizePerTile;
    }

    //--Render.
    pImage->Draw(tRenderX, tRenderY);
}

///========================================= Waterfall ============================================
///--[Local Definitions]
///--[Structure Functions]
void PF_Waterfall::Initialize()
{
    //--Current position.
    mXPos = 0;
    mYPos = 0;

    //--Cycle
    mSize = 1;
    mCycleTimer = 0;
    mSpreadTimer = 0;
    mSpreadCount = 0;
    mHoldTimer = 0;
}
void PF_Waterfall::Update()
{
    ///--[Cycling]
    //--Waterfall is getting ready to open up.
    if(mCycleTimer < PF_WATERFALL_CYCLE_TICKS)
    {
        mCycleTimer ++;
        return;
    }

    ///--[Spreading]
    //--Setup.
    int tXTile = mXPos / TileLayer::cxSizePerTile;
    int tYTile = mYPos / TileLayer::cxSizePerTile;
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--Waterfall is now spreading water downwards.
    if(mSpreadCount < mSize)
    {
        //--Timer.
        mSpreadTimer ++;
        if(mSpreadTimer >= PF_WATERFALL_SPREAD_TICKS)
        {
            mSpreadTimer ++;
            mSpreadCount ++;
        }

        //--Bumping.
        for(int i = 0; i < mSpreadCount; i ++)
        {
            rActiveLevel->BumpPlayerAbsolute(tXTile, tYTile+1+i, tXTile, tYTile+2+i);
        }

        //--Stop.
        return;
    }
    //--Max size. Bump player.
    else
    {
        //--Bumping.
        for(int i = 0; i < mSize; i ++)
        {
            rActiveLevel->BumpPlayerAbsolute(tXTile, tYTile+1+i, tXTile, tYTile+2+i);
        }
    }

    ///--[Holding]
    //--Wait a bit before resetting.
    mHoldTimer ++;
    if(mHoldTimer >= PF_WATERFALL_HOLD_TICKS)
    {
        mCycleTimer = 0;
        mSpreadCount = 0;
        mSpreadTimer = 0;
        mHoldTimer = 0;
    }
}
void PF_Waterfall::Render()
{
    ///--[Cycling]
    //--Setup.
    int cTPF = (PF_WATERFALL_CYCLE_TICKS / (PF_WATERFALL_FRAMES-1));
    int tFrame = 0;

    //--Waterfall is getting ready to open up.
    if(mCycleTimer < PF_WATERFALL_CYCLE_TICKS)
    {
        tFrame = mCycleTimer / cTPF;
        if(tFrame >= PF_WATERFALL_FRAMES-1) tFrame = PF_WATERFALL_FRAMES-2;
    }
    //--Done, show the "open" frame.
    else
    {
        tFrame = PF_WATERFALL_FRAMES-1;
    }

    //--Render.
    float tRenderX = mXPos;
    float tRenderY = mYPos;
    rCycleImg[tFrame]->Draw(tRenderX, tRenderY);

    ///--[Spreading]
    for(int i = 0; i < mSpreadCount; i ++)
    {
        //--First frame:
        if(i == 0)
        {
            rWaterfallTop->Draw(tRenderX, tRenderY + (TileLayer::cxSizePerTile * (i+1)));
        }
        //--Middle:
        else if(i < mSpreadCount-1)
        {
            rWaterfallMid->Draw(tRenderX, tRenderY + (TileLayer::cxSizePerTile * (i+1)));
        }
        //--Bottom:
        else
        {
            rWaterfallBot->Draw(tRenderX, tRenderY + (TileLayer::cxSizePerTile * (i+1)));
        }
    }
}

///======================================== Bomb Spike ============================================
///--[Local Definitions]
#define PF_BOMBSPIKE_SHAKE_TICKS 45
#define PF_BOMBSPIKE_ACTIVATION_TICKS 60
#define PF_BOMBSPIKE_EXPLOSION_TICKS 30

///--[Structure Functions]
void PF_BombSpike::Initialize()
{
    //--Current position.
    mSelfDestruct = false;
    mIsActivated = false;
    mTimer = 0;
    mExplodeTicks = 0;
    mXPos = 0;
    mYPos = 0;
}
void PF_BombSpike::Update()
{
    ///--[Activation Check]
    //--Bomb is not active. Check if the player/rival is adjacent.
    if(!mIsActivated)
    {
        //--Setup.
        int cMyX = mXPos / TileLayer::cxSizePerTile;
        int cMyY = mYPos / TileLayer::cxSizePerTile;
        PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
        if(!rActiveLevel) return;

        //--Player.
        PeakFreaksPlayer *rPlayer = rActiveLevel->GetPlayer();
        if(rPlayer)
        {
            //--Player's position:
            int tPlayerWorldX = (int)rPlayer->GetWorldX();
            int tPlayerWorldY = (int)rPlayer->GetWorldY();

            //--Adjacent. Activate.
            if((tPlayerWorldX == cMyX - 1 && tPlayerWorldY == cMyY + 0) || (tPlayerWorldX == cMyX + 1 && tPlayerWorldY == cMyY + 0) ||
               (tPlayerWorldX == cMyX + 0 && tPlayerWorldY == cMyY - 1) || (tPlayerWorldX == cMyX + 0 && tPlayerWorldY == cMyY + 1))
            {
                mIsActivated = true;
            }
        }

        //--Rival.
        PeakFreaksPlayer *rRival = rActiveLevel->GetRival();
        if(rRival)
        {
            //--Player's position:
            int tRivalWorldX = (int)rRival->GetWorldX();
            int tRivalWorldY = (int)rRival->GetWorldY();

            //--Adjacent. Activate.
            if((tRivalWorldX == cMyX - 1 && tRivalWorldY == cMyY + 0) || (tRivalWorldX == cMyX + 1 && tRivalWorldY == cMyY + 0) ||
               (tRivalWorldX == cMyX + 0 && tRivalWorldY == cMyY - 1) || (tRivalWorldX == cMyX + 0 && tRivalWorldY == cMyY + 1))
            {
                mIsActivated = true;
            }
        }

        //--Stop here.
        return;
    }

    ///--[Explosion!]
    //--Counting up.
    mTimer ++;
    if(mTimer < PF_BOMBSPIKE_ACTIVATION_TICKS) return;

    //--Explosion is active.
    int cMyX = mXPos / TileLayer::cxSizePerTile;
    int cMyY = mYPos / TileLayer::cxSizePerTile;
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--Player.
    PeakFreaksPlayer *rPlayer = rActiveLevel->GetPlayer();
    if(rPlayer && !rPlayer->IsIgnoringHazards())
    {
        //--Player's position:
        int tPlayerWorldX = (int)rPlayer->GetWorldX();
        int tPlayerWorldY = (int)rPlayer->GetWorldY();

        //--Adjacent. Activate.
        if((tPlayerWorldX == cMyX - 1 && tPlayerWorldY == cMyY + 0) || (tPlayerWorldX == cMyX + 1 && tPlayerWorldY == cMyY + 0) ||
           (tPlayerWorldX == cMyX + 0 && tPlayerWorldY == cMyY - 1) || (tPlayerWorldX == cMyX + 0 && tPlayerWorldY == cMyY + 1))
        {
            rPlayer->Injure();
        }
    }

    //--Rival.
    PeakFreaksPlayer *rRival = rActiveLevel->GetRival();
    if(rRival && !rRival->IsIgnoringHazards())
    {
        //--Player's position:
        int tRivalWorldX = (int)rRival->GetWorldX();
        int tRivalWorldY = (int)rRival->GetWorldY();

        //--Adjacent. Activate.
        if((tRivalWorldX == cMyX - 1 && tRivalWorldY == cMyY + 0) || (tRivalWorldX == cMyX + 1 && tRivalWorldY == cMyY + 0) ||
           (tRivalWorldX == cMyX + 0 && tRivalWorldY == cMyY - 1) || (tRivalWorldX == cMyX + 0 && tRivalWorldY == cMyY + 1))
        {
            rRival->Injure();
        }
    }

    mExplodeTicks ++;
    if(mExplodeTicks < PF_BOMBSPIKE_EXPLOSION_TICKS) return;

    //--Delete this object.
    mSelfDestruct = true;
}
void PF_BombSpike::Render()
{
    //--Not active, or winding up to explode:
    if(mTimer < PF_BOMBSPIKE_ACTIVATION_TICKS)
    {
        //--Compute position.
        float tRenderX = mXPos;
        float tRenderY = mYPos;
        if(mTimer > PF_BOMBSPIKE_SHAKE_TICKS)
        {
            int tXOffset = ((mTimer - PF_BOMBSPIKE_SHAKE_TICKS) % 3) - 1;
            tRenderX = tRenderX + tXOffset;
        }

        //--Render.
        rNeutralImg->Draw(tRenderX, tRenderY);
        return;
    }

    //--Exploding!
    int tFrame = rand() % PF_BOMBSPIKE_EXPLOSION_FRAMES;
    rExplosions[tFrame]->Draw(mXPos - TileLayer::cxSizePerTile, mYPos);
    tFrame = rand() % PF_BOMBSPIKE_EXPLOSION_FRAMES;
    rExplosions[tFrame]->Draw(mXPos + TileLayer::cxSizePerTile, mYPos);
    tFrame = rand() % PF_BOMBSPIKE_EXPLOSION_FRAMES;
    rExplosions[tFrame]->Draw(mXPos, mYPos - TileLayer::cxSizePerTile);
    tFrame = rand() % PF_BOMBSPIKE_EXPLOSION_FRAMES;
    rExplosions[tFrame]->Draw(mXPos, mYPos + TileLayer::cxSizePerTile);
}
void PF_BombSpike::RenderIceBeam(SugarBitmap *pImage)
{
    //--Error check.
    if(!pImage) return;

    //--Don't render the overlay for exploding bombs.
    if(mTimer >= PF_BOMBSPIKE_ACTIVATION_TICKS) return;

    //--Setup.
    float tRenderX = mXPos;
    float tRenderY = mYPos;

    //--Render.
    pImage->Draw(tRenderX, tRenderY);
}
