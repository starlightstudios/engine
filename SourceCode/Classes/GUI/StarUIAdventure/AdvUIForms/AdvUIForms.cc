//--Base
#include "AdvUIForms.h"

//--Classes
#include "AdvUIBase.h"
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIForms::AdvUIForms()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AFOR");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = (VIRTUAL_CANVAS_X * 0.50f);
    mGridRenderCenterY  = (VIRTUAL_CANVAS_Y * 0.35f) + 50.0f;
    mGridRenderSpacingX = (VIRTUAL_CANVAS_X * 0.10f);
    mGridRenderSpacingY = (VIRTUAL_CANVAS_X * 0.10f);

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ======= AdvUIForms ======= ]
    ///--[Variables]
    //--System
    mIsInFormSelect = false;
    mFormSelectTimer = 0;

    //--Cursor
    mDontShowHighlight = false;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Data
    mCharactersData = new StarLinkedList(true);
    mFormClassData = new StarLinkedList(true);

    //--Comparison.
    //CombatStatistics mCharacterBaseStats;

    //--Strings
    mShowHelpString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Additional grids.
    CreateStoredGrid("Forms");

    //--Help Strings.
    AllocateHelpStrings(cxAdvFormsHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIForms::~AdvUIForms()
{
    delete mCharactersData;
    delete mFormClassData;
    delete mShowHelpString;
}
void AdvUIForms::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Forms UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",  &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = AdvImages.rFont_Mainline;
}

///--[Public Statics]
//--Script used by the form selection menu. When a character goes to transform, this script will be
//  called to provide a list of transformations.
char *AdvUIForms::xFormResolveScript = NULL;

//--Script used by the form selection menu. Builds a list of party members who appear on the transformation
//  grid. This may include characters who can't transform.
char *AdvUIForms::xFormPartyResolveScript = NULL;

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIForms::TakeForeground()
{
    ///--[Documentation and Setup]
    //--If there's no costume menu handler to call, stop here.
    if(!xFormPartyResolveScript || !xFormResolveScript)
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--If the base grid wasn't active, activate it now. Then, clear and reset the entries grid.
    ActivateStoredGrid("Base");
    ClearStoredGrids();
    CreateStoredGrid("Forms");

    //--The costumes menu needs to be rebuilt each time it is activated, since the party may have
    //  changed since last it was opened.
    mCharactersData->ClearList();
    mFormClassData->ClearList();

    //--Run the party population script.
    LuaManager::Fetch()->ExecuteLuaFile(xFormPartyResolveScript);
    if(mCharactersData->GetListSize() < 1)
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Play SFX to indicate the menu is opened.
    AudioManager::Fetch()->PlaySound("Menu|Select");

    //--Set flags.
    mIsInFormSelect = false;

    ///--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    AutoAllocateSquareGrid(mCharactersData->GetListSize());
    ProcessGrid();

    //--Iteration loop.
    int i = 0;
    AdvMenuFormCharacterPack *rPackage = (AdvMenuFormCharacterPack *)mCharactersData->PushIterator();
    while(rPackage)
    {
        //--Retrieve a grid package. If it's NULL then we ran out of space, bark an error.
        GridPackage *rGridPack = GetNextPriorityEntry();
        if(!rGridPack)
        {
            fprintf(stderr, "AdvUIForms:TakeForeground() - Warning, heading data has %i entries but the grid ran out of slots. Failing.\n", mCharactersData->GetListSize());
            mCharactersData->PopIterator();
            break;
        }

        //--Copy across.
        rGridPack->mCode = i;
        strcpy(rGridPack->mLocalName, rPackage->mDisplayName);
        memcpy(&rGridPack->mAlignments, &rPackage->mAlignments, sizeof(AdvMenuStandardAlignments));

        //--Next.
        i ++;
        rPackage = (AdvMenuFormCharacterPack *)mCharactersData->AutoIterate();
    }

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    ///--[Run Cursor]
    //--Update the cursor so it focuses on the selected entry.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}
void AdvUIForms::RegisterFormPartyMember(const char *pCharacterName, const char *pDisplayName, const char *pImgPath)
{
    ///--[Documentation]
    //--Adds a character to the listing.
    if(!pCharacterName || !pDisplayName || !pImgPath) return;

    //--Storage pack.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuFormCharacterPack *nPackage = (AdvMenuFormCharacterPack *)starmemoryalloc(sizeof(AdvMenuFormCharacterPack));
    nPackage->Initialize();
    ResetString(nPackage->mDisplayName, pDisplayName);
    nPackage->mAlignments.rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);

    //--Load data if it's not loaded already.
    if(nPackage->mAlignments.rImage) nPackage->mAlignments.rImage->LoadDataFromSLF();

    //--Register it.
    mCharactersData->AddElementAsTail(pCharacterName, nPackage, AdvMenuFormCharacterPack::DeleteThis);
}
void AdvUIForms::SetFormPartyMemberAlignments(const char *pCharacterName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei)
{
    ///--[Documentation]
    //--Adds a character to the listing.
    if(!pCharacterName) return;

    //--Locate package.
    AdvMenuFormCharacterPack *rPackage = (AdvMenuFormCharacterPack *)mCharactersData->GetElementByName(pCharacterName);
    if(!rPackage || !rPackage->mAlignments.rImage) return;

    //--Set.
    rPackage->mAlignments.mOffsetX = pXOff;
    rPackage->mAlignments.mOffsetY = pYOff;
    rPackage->mAlignments.mImageX = pLft - rPackage->mAlignments.rImage->GetXOffset();
    rPackage->mAlignments.mImageY = pTop - rPackage->mAlignments.rImage->GetYOffset();
    rPackage->mAlignments.mImageW = pWid;
    rPackage->mAlignments.mImageH = pHei;

    //--Clamp.
    if(rPackage->mAlignments.mImageW < 1.0f) rPackage->mAlignments.mImageW = 1.0f;
    if(rPackage->mAlignments.mImageH < 1.0f) rPackage->mAlignments.mImageH = 1.0f;
}
void AdvUIForms::RegisterFormTransformation(const char *pFormName, const char *pDisplayName, const char *pFormPath, const char *pJobScriptPath, const char *pImgPath)
{
    ///--[Documentation]
    //--Adds a transformation or class change to the transformation listing. This is only called when
    //  a character has been selected, so it is implied all the transformations are for the same character.
    if(!pFormName || !pDisplayName || !pFormPath || !pJobScriptPath || !pImgPath) return;

    //--Storage pack.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuFormClassPack *nPackage = (AdvMenuFormClassPack *)starmemoryalloc(sizeof(AdvMenuFormClassPack));
    nPackage->Initialize();
    ResetString(nPackage->mDisplayName, pDisplayName);
    ResetString(nPackage->mScriptPath, pFormPath);
    ResetString(nPackage->mJobPath, pJobScriptPath);
    nPackage->mAlignments.rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);

    //--Load data if it's not loaded already.
    if(nPackage->mAlignments.rImage) nPackage->mAlignments.rImage->LoadDataFromSLF();

    //--Register it.
    mFormClassData->AddElementAsTail(pFormName, nPackage, AdvMenuFormClassPack::DeleteThis);
}
void AdvUIForms::SetFormPartyTransformationAlignments(const char *pFormName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei)
{
    ///--[Documentation]
    //--Adds a character to the listing.
    if(!pFormName) return;

    //--Locate package.
    AdvMenuFormClassPack *rPackage = (AdvMenuFormClassPack *)mFormClassData->GetElementByName(pFormName);
    if(!rPackage || !rPackage->mAlignments.rImage) return;

    //--Set.
    rPackage->mAlignments.mOffsetX = pXOff;
    rPackage->mAlignments.mOffsetY = pYOff;
    rPackage->mAlignments.mImageX = pLft - rPackage->mAlignments.rImage->GetXOffset();
    rPackage->mAlignments.mImageY = pTop - rPackage->mAlignments.rImage->GetYOffset();
    rPackage->mAlignments.mImageW = pWid;
    rPackage->mAlignments.mImageH = pHei;

    //--Clamp.
    if(rPackage->mAlignments.mImageW < 1.0f) rPackage->mAlignments.mImageW = 1.0f;
    if(rPackage->mAlignments.mImageH < 1.0f) rPackage->mAlignments.mImageH = 1.0f;
}

///======================================= Core Methods ===========================================
void AdvUIForms::RefreshMenuHelp()
{
    /*
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Set lines.
    mHelpMenuStrings[i]->SetString("The forms menu allows you to change the job/form of your party members.");
    i ++;
    mHelpMenuStrings[i]->SetString("You will be able to see any stat changes associated with the form change.");
    i ++;
    mHelpMenuStrings[i]->SetString("For story reasons, it may not be possible to transform in some locations.");
    i ++;
    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("Press [IMG0] or [IMG1] to exit this help menu.");
    mHelpMenuStrings[i]->AllocateImages(2);
    mHelpMenuStrings[i]->SetImageP(0, ADVMENU_BIG_CONTROL_OFFSET_Y, rControlManager->ResolveControlImage("F1"));
    mHelpMenuStrings[i]->SetImageP(1, ADVMENU_BIG_CONTROL_OFFSET_Y, rControlManager->ResolveControlImage("Cancel"));
    i ++;

    //--Error check.
    if(i > ADVMENU_FORMS_HELP_MENU_STRINGS)
    {
        fprintf(stderr, "Warning: Forms Help Menu has too many strings %i vs %i\n", i, ADVMENU_FORMS_HELP_MENU_STRINGS);
    }

    //--Order all strings to crossreference images.
    for(int i = 0; i < ADVMENU_FORMS_HELP_MENU_STRINGS; i ++)
    {
        mHelpMenuStrings[i]->CrossreferenceImages();
    }
    */
}
void AdvUIForms::RecomputeCursorPositions()
{
    RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, 1.0f);
}
void AdvUIForms::ActivateFormsMode()
{
    ///--[Documentation]
    //--This object contains two grids. When selecting headings, the Entry grid is not in use. When
    //  a heading is selected, we move to Entries mode. This moves the existing grid into the Entry
    //  grid slots, and repopulates the current grid with the entry data.
    if(mIsInFormSelect)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    ///--[Entry]
    //--Store the active entry.
    int tSlotOfSelection = GetGridCodeAtCursor();
    AdvMenuFormCharacterPack *rCurrentCharacter = (AdvMenuFormCharacterPack *)mCharactersData->GetElementBySlot(tSlotOfSelection);
    const char *rCharacterName = mCharactersData->GetNameOfElementBySlot(tSlotOfSelection);
    if(!rCurrentCharacter || !rCharacterName)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Set flag.
    mIsInFormSelect = true;

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

    ///--[Allocate, Set]
    //--Switch to the Forms grid. This will archive the Base grid for later.
    ActivateStoredGrid("Forms");

    //--Run the script. This will populate the forms that character can change to.
    mFormClassData->ClearList();
    LuaManager::Fetch()->ExecuteLuaFile(xFormResolveScript, 1, "S", rCharacterName);

    //--If the character cannot change to any forms, fail here.
    if(mFormClassData->GetListSize() < 1)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Allocate space for the new data.
    AutoAllocateSquareGrid(mFormClassData->GetListSize());
    ProcessGrid();

    //--Iteration loop.
    int i = 0;
    AdvMenuFormClassPack *rPackage = (AdvMenuFormClassPack *)mFormClassData->PushIterator();
    while(rPackage)
    {
        //--Retrieve a grid package. If it's NULL then we ran out of space, bark an error.
        GridPackage *rGridPack = GetNextPriorityEntry();
        if(!rGridPack)
        {
            fprintf(stderr, "AdvUIForms:ActivateFormsMode() - Warning, heading data has %i entries but the grid ran out of slots. Failing.\n", mFormClassData->GetListSize());
            mFormClassData->PopIterator();
            break;
        }

        //--Copy across.
        rGridPack->mCode = i;
        strcpy(rGridPack->mLocalName, mFormClassData->GetIteratorName());
        memcpy(&rGridPack->mAlignments, &rPackage->mAlignments, sizeof(AdvMenuStandardAlignments));

        //--Next.
        i ++;
        rPackage = (AdvMenuFormClassPack *)mFormClassData->AutoIterate();
    }

    ///--[Combat Statistics]
    //--Locate the combat character in question.
    AdvCombatEntity *rCombatCharacter = AdvCombat::Fetch()->GetRosterMemberS(rCharacterName);
    if(!rCombatCharacter)
    {
        fprintf(stderr, "Error, no character %s on roster.\n", rCharacterName);
        return;
    }

    //--Character statistics. Get their final stats, and the bonus they get from their current job.
    CombatStatistics *rCurJobStats    = rCombatCharacter->GetStatisticsGroup(ADVCE_STATS_JOB);
    CombatStatistics *rCharacterStats = rCombatCharacter->GetStatisticsGroup(ADVCE_STATS_FINAL);

    //--Store these in the base comparison package.
    for(int i = 0; i < STATS_TOTAL; i ++)
    {
        mCharacterBaseStats.mValueList[i] = rCharacterStats->mValueList[i];
    }

    //--For each class on the grid, get their stats.
    AdvMenuFormClassPack *rClassPack = (AdvMenuFormClassPack *)mFormClassData->PushIterator();
    while(rClassPack)
    {
        //--Clone the base stats out, but subtract the job bonus.
        for(int i = 0; i < STATS_TOTAL; i ++)
        {
            rClassPack->mCombatStatistics.mValueList[i] = rCharacterStats->mValueList[i] - rCurJobStats->mValueList[i];
        }

        //--Run the job script and order it to compute the character's stats.
        LuaManager::Fetch()->ExecuteLuaFile(rClassPack->mJobPath, 2, "N", (float)ADVCJOB_CODE_LEVEL, "N", (float)rCombatCharacter->GetLevel());

        //--Apply the stats we got out to the stats package.
        CombatStatistics *rNewStats = AdvCombat::Fetch()->GetJobLevelUpStorage();
        for(int i = 0; i < STATS_TOTAL; i ++)
        {
            rClassPack->mCombatStatistics.mValueList[i] = rClassPack->mCombatStatistics.mValueList[i] + rNewStats->mValueList[i];
        }

        //--Next.
        rClassPack = (AdvMenuFormClassPack *)mFormClassData->AutoIterate();
    }

    ///--[Cursor]
    RecomputeCursorPositions();
}
void AdvUIForms::DeactivateFormsMode()
{
    ///--[Documentation]
    //--Called when the player cancels out of Entries mode.
    if(!mIsInFormSelect) return;

    ///--[Execution]
    //--Flag.
    mIsInFormSelect = false;

    //--Switch back to Base.
    ActivateStoredGrid("Base");

    ///--[Cursor]
    RecomputeCursorPositions();
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIForms::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        mVisibilityTimer ++;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--Standard Timers.
    StandardTimer(mFormSelectTimer,     0, cxAdvVisTicks, mIsInFormSelect);
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks, mIsShowingHelp);

    //--Grid timers.
    UpdateGridTimers();

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Set this flag.
    mDontShowHighlight = false;

    ///--[ ======== Update Intercepts ========= ]
    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[ =========== Update Call ============ ]
    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return true;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If not in Entries mode:
        if(!mIsInFormSelect)
        {
            ActivateFormsMode();
        }
        //--In Entries mode:
        else
        {
            //--Get the script path.
            int tGridCurrent = GetGridCodeAtCursor();
            AdvMenuFormClassPack *rPackage = (AdvMenuFormClassPack *)mFormClassData->GetElementBySlot(tGridCurrent);
            if(!rPackage)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return true;
            }

            //--Run the script.
            LuaManager::Fetch()->ExecuteLuaFile(rPackage->mScriptPath);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Hide this menu. Must be called after everything else since it clears the form list.
            FlagExit();
            SetCodeBackward(AM_MODE_EXITNOW);
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        //--If not in Entries mode:
        if(!mIsInFormSelect)
        {
            FlagExit();
        }
        //--In Entries mode:
        else
        {
            DeactivateFormsMode();
        }
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update.
    return true;
}
void AdvUIForms::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;

    //--Run timers.
    mVisibilityTimer --;
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Unset this flag.
    mDontShowHighlight = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIForms::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Rendering of the base UI. Mostly follows the standard, however the bottom of the UI that shows
    //  the player's party will continue to render even if this object is offscreen if the Doctor UI
    //  needs it to, which is handled via a static timer.
    StarlightColor::ClearMixer();

    ///--[Timer]
    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Render Grid]
    //--When in the Base mode, the Entries grid needs to render if its timer is nonzero.
    if(!mIsInFormSelect)
    {
        //--Compute alphas.
        float cBaseAlpha = (1.0f - EasingFunction::QuadraticInOut(mFormSelectTimer, cxAdvVisTicks)) * pVisAlpha;
        float cFormAlpha =         EasingFunction::QuadraticInOut(mFormSelectTimer, cxAdvVisTicks)  * pVisAlpha;

        //--Timer must be over zero or there's nothing to render.
        if(mFormSelectTimer > 0)
        {
            ActivateStoredGrid("Forms");
            RenderGrid(cFormAlpha);
            ActivateStoredGrid("Base");
        }

        //--Render the grid normally.
        RenderGrid(cBaseAlpha);
        if(!mDontShowHighlight) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
        RenderGridEntryName(GetPackageAtCursor(), cBaseAlpha);
    }
    //--When in Entries mode, render the Base grid if the timer isn't capped.
    else
    {
        //--Compute alphas.
        float cBaseAlpha = (1.0f - EasingFunction::QuadraticInOut(mFormSelectTimer, cxAdvVisTicks)) * pVisAlpha;
        float cFormAlpha =         EasingFunction::QuadraticInOut(mFormSelectTimer, cxAdvVisTicks)  * pVisAlpha;

        //--Render the grid normally.
        RenderGrid(cFormAlpha);
        if(!mDontShowHighlight) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
        RenderGridEntryName(GetPackageAtCursor(), cFormAlpha);

        //--Timer must not be capped.
        if(mFormSelectTimer < cxAdvVisTicks)
        {
            ActivateStoredGrid("Base");
            RenderGrid(cBaseAlpha);
            ActivateStoredGrid("Forms");
        }
    }

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUIForms::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the header at the top of the screen.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Form/Class Change Menu");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, 0, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    AutoPieceClose();
}
void AdvUIForms::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Execution]
    //--Footer.
    AdvImages.rOverlay_Footer->Draw();

    ///--[Clean]
    glDisable(GL_STENCIL_TEST);
    AutoPieceClose();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
