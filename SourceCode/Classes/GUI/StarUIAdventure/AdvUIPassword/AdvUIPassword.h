///======================================= AdvUIPassword ==========================================
//--Container object that holds a string entry form.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUIPassword : virtual public AdvUICommon
{
    protected:
    ///--[Variables]
    StringEntry *mStringEntryForm;

    protected:

    public:
    //--System
    AdvUIPassword();
    virtual ~AdvUIPassword();
    virtual void Construct();

    //--Public Variables
    static bool xHandledPassword;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderForeground(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


