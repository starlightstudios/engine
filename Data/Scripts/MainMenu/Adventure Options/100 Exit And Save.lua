-- |[ ================================= Save Back to Main Menu ================================= ]|
--Closes the existing menu and returns to the main menu. Also saves out the config file.
MapM_PushMenuStackHead()

-- |[Functions]|
--Default boolean resolver. Handy!
local fnHandleBoolean = function(sButtonName, sOptionName)
	
	FlexMenu_PushButton(sButtonName)
		local iFlag = FlexButton_GetProperty("Current Option")
		if(iFlag ~= nil and iFlag == 0) then
			OM_SetOption(sOptionName, true)
		elseif(iFlag ~= nil and iFlag == 1) then
			OM_SetOption(sOptionName, false)
		end
	DL_PopActiveObject()
	
end

-- |[ ===================================== Resolve Values ===================================== ]|
--Figure out which options the buttons were pointing to, and upload those values to the Options Manager.
fnHandleBoolean("Asset Streaming",           "Allow Asset Streaming")
fnHandleBoolean("Unload Dialogue Portraits", "Actors Unload After Dialogue")
fnHandleBoolean("Shader: Underwater",        "Disable Shader AdventureLevel Underwater")
fnHandleBoolean("Shader: Outline",           "Disable Shader TilemapActor ColorOutline")
fnHandleBoolean("Shader: Lighting",          "Disable Shader AdventureLevel Lighting")

--Autosave Count. Slider logic.
FlexMenu_PushButton("Max Autosaves")
	OM_SetOption("Max Autosaves", FlexButton_GetProperty("Current Slider"))
DL_PopActiveObject()

--The Nextboot for Asset Streaming needs to match the Asset Streaming value.
fnHandleBoolean("Asset Streaming", "Allow Asset Streaming Nextboot")

-- |[ ======================================== Finish Up ======================================= ]|

-- |[Write]|
--Save the data to the hard drive.
OM_WriteConfigFiles()

-- |[Reset]|
--Clear the old menu, and return to the main menu.
FlexMenu_Clear()

--Run the setup script. This is elsewhere.
LM_ExecuteScript("Data/Scripts/MainMenu/000 PopulateMainMenu.lua")

--Clean Up.
DL_PopActiveObject()