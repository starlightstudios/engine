///======================================= ScriptHunter ===========================================
//--A simple object which runs through all the scripts in the program and searches for possible errors
//  in the binary. It's looking for non-ASCII characters and illegal line ending cases. Any errors it
//  finds get spit to the console and a report log. If no errors are found, no log is created.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///========================================== Classes =============================================
class ScriptHunter
{
    private:
    //--System
    //--Target Info
    char *mTargetDirectory;

    protected:

    public:
    //--System
    ScriptHunter();
    ~ScriptHunter();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetTargetDirectory(const char *pPath);

    //--Core Methods
    void Execute();

    private:
    //--Private Core Methods
    bool CheckBinaryForBugs(const char *pPath);
    void FixComments(const char *pPath);

    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

