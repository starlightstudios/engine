//--Base
#include "HitArea.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

StarLinkedList *HitXLocationsCross(int pDistance, int pCount, bool pSkipZero)
{
    ///--[Documentation]
    //--Uses the "Cross" area type, specified in the header file.
    StarLinkedList *nReturnList = new StarLinkedList(true);
    if(pCount < 1 || pDistance < 0) return nReturnList;

    //--A zero-distance list is just returning a {0,0} list and being done with it.
    if(pDistance < 1)
    {
        //--On a skip-zero, just return an empty list.
        if(pSkipZero) return nReturnList;

        //--Populate.
        Point2DI *nPoint = (Point2DI *)starmemoryalloc(sizeof(Point2DI));
        nPoint->mX = 0;
        nPoint->mY = 0;
        nReturnList->AddElement("X", nPoint, &FreeThis);
        return nReturnList;
    }

    ///--[Generalized Formula]
    //--Iterate across the available distance x2 + 1. A size 1 cross has a range of 3, size 2 is 5, etc. The +1
    //  is for the center tile. Create a list of all possible spots we can hit within that range.
    StarLinkedList *tPossibleHits = new StarLinkedList(true);
    for(int x = 0; x < (pDistance * 2) + 1; x ++)
    {
        for(int y = 0; y < (pDistance * 2) + 1; y ++)
        {
            //--Skip locations that don't sum to a number less than or equal to the distance. Otherwise we'd
            //  just be hitting within a rectangle.
            int tXHit = x - pDistance;
            int tYHit = y - pDistance;
            if(abs(tXHit) + abs(tYHit) > pDistance) continue;

            //--Skip the zero if flagged.
            if(pSkipZero && tXHit == 0 && tYHit == 0) continue;

            //--Set.
            Point2DI *nPoint = (Point2DI *)starmemoryalloc(sizeof(Point2DI));
            nPoint->mX = tXHit;
            nPoint->mY = tYHit;
            tPossibleHits->AddElement("X", nPoint, &FreeThis);
        }
    }

    //--If the size of the possible hit list is less than or equal to the number of hits required, we can just
    //  return the possibility list instead since all possible hits will be hit.
    if(tPossibleHits->GetListSize() <= pCount)
    {
        delete nReturnList;
        return tPossibleHits;
    }

    //--We now have a list of every unique possible hit. Pick them at random and put them on the return list.
    //  We can be certain the modulus will never fail because there will always be at least 2 hits on the list
    //  since an equal-sized list to the number of hits would already have been returned by the above block.
    for(int i = 0; i < pCount; i ++)
    {
        //--Roll a random entry on the possible list.
        int tRoll = rand() % tPossibleHits->GetListSize();

        //--Liberate it from the possible list and put it on the return list.
        Point2DI *rLiberatedEntry = (Point2DI *)tPossibleHits->GetElementBySlot(tRoll);
        tPossibleHits->SetRandomPointerToThis(rLiberatedEntry);
        tPossibleHits->LiberateRandomPointerEntry();
        nReturnList->AddElement("X", rLiberatedEntry, &FreeThis);
    }

    //--Clean up and return.
    delete tPossibleHits;
    return nReturnList;
}
