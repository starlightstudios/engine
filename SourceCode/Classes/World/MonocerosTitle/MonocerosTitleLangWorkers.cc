//--Base
#include "MonocerosTitle.h"

//--Classes
//--CoreClasses
#include "StarArray.h"
#include "StarFileSystem.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "StarLumpManager.h"

///======================================= Documentation ==========================================
//--Contains worker functions for creating language packs, deploying language packs, and other things
//  that aren't part of the UI properties like updating and rendering.

///==================================== Language Directory ========================================
void MonocerosTitle::CreateLanguageDirectory(const char *pLanguageName, const char *pFolderPath, const char *pLuaSuffix)
{
    ///--[Documentation]
    //--Creates a language directory. This is a directory mirroring the distribution format of the program,
    //  except it only contains the files needed to distribute a translation. This is specifically the csv
    //  files and .lua[ext] files used by that translation.
    if(!pLanguageName || !pFolderPath || !pLuaSuffix) return;

    //--Filesystem.
    StarFileSystem *tFileSystem = new StarFileSystem();

    //--Diagnostics.
    fprintf(stderr, "Creating language directory.\n");
    fprintf(stderr, " Language: %s\n", pLanguageName);
    fprintf(stderr, " Folder: %s\n", pFolderPath);
    fprintf(stderr, " Extension: %s\n", pLuaSuffix);

    ///--[Base Directory]
    //--First, create the folder in the base directory if it's not already there.
    char tBaseFolderName[128];
    sprintf(tBaseFolderName, "Translation%s", pLanguageName);

    //--Operation buffer.
    char tOperationBuf[1024];
    sprintf(tOperationBuf, "mkdir %s", tBaseFolderName);
    system(tOperationBuf);

    //--Other.
    char tNewPathBuf[1024];

    ///--[Clone .csv Files]
    //--Create the path to the folder that holds translation information. Open it in the filesystem
    //  to look for .csv files. These files contain spreadsheet based translations which are supported.
    char tPathBuffer[STD_PATH_LEN];
    sprintf(tPathBuffer, "Data/Translations/%s/", pFolderPath);
    tFileSystem->Clear();
    tFileSystem->SetRecursiveFlag(false);
    tFileSystem->ScanDirectory(tPathBuffer);

    //--Get a list of all .csv files in the directory.
    fprintf(stderr, " Scanning for .csv files in %s\n", tPathBuffer);
    StarLinkedList *trSheetFileList = tFileSystem->GetFilesWithExtension(".csv");

    //--Diagnostics.
    fprintf(stderr, " Found %i files.\n", trSheetFileList->GetListSize());

    //--For each FileInfo object:
    FileInfo *rSheetInfo = (FileInfo *)trSheetFileList->PushIterator();
    while(rSheetInfo)
    {
        //--Pare it to filename.extension.
        char *tFilePlusExtension = StarFileSystem::PareFileName(rSheetInfo->mPath, true);

        //--Path where this new file will be in the clone directory.
        sprintf(tNewPathBuf, "%s/%s%s", tBaseFolderName, tPathBuffer, tFilePlusExtension);

        //--Create directories for the translation.
        StarFileSystem::CreateDirectoriesFor(tNewPathBuf);

        //--Create a command, get all slashes sorted out, call.
        sprintf(tOperationBuf, "copy \"%s\" \"%s\"", rSheetInfo->mPath, tNewPathBuf);
        StarFileSystem::ModifySlashes(tOperationBuf);
        system(tOperationBuf);

        //--Diagnostics.
        fprintf(stderr, "%s\n", tOperationBuf);

        //--Clean.
        free(tFilePlusExtension);

        //--Next.
        rSheetInfo = (FileInfo *)trSheetFileList->AutoIterate();
    }

    ///--[Search for Translated Lua Files]
    //--This requires scanning the entire filesystem for files with the extension of pLuaSuffix.
    //  For example, all files of ".luajp" for the Japanese translation.
    int tFoundBaseFiles = 0;
    fprintf(stderr, " Scanning base folder for extension %s.\n", pLuaSuffix);
    tFileSystem->Clear();
    tFileSystem->SetRecursiveFlag(true);
    tFileSystem->ScanDirectory("./");

    //--Get a list of all extension-matching files in the directory.
    fprintf(stderr, " Scanning for %s files in base directory and subdirectories.\n", pLuaSuffix);
    StarLinkedList *trLuaFileList = tFileSystem->GetFilesWithExtension(pLuaSuffix);
    fprintf(stderr, " Found %i files.\n", trLuaFileList->GetListSize());

    //--For each FileInfo object:
    FileInfo *rLuaInfo = (FileInfo *)trLuaFileList->PushIterator();
    while(rLuaInfo)
    {
        //--Pare it to filename.extension.
        char *tFilePlusExtension = StarFileSystem::PareFileName(rLuaInfo->mPath, true);

        //--Scroll back through the filename and locate the folder relative to the executable. This means locating
        //  either "Games/" or "Data/".
        int tLocalPathStart = 0;
        int tPathLen = (int)strlen(rLuaInfo->mPath);
        for(int i = 0; i < tPathLen; i ++)
        {
            if(!strncasecmp(&rLuaInfo->mPath[i], "Games/", 6) || !strncasecmp(&rLuaInfo->mPath[i], "Data/", 5))
            {
                tLocalPathStart = i;
                break;
            }
        }

        //--Copy that as the path buffer.
        strcpy(tPathBuffer, &rLuaInfo->mPath[tLocalPathStart]);

        //--Path where this new file will be in the clone directory.
        sprintf(tNewPathBuf, "%s/%s", tBaseFolderName, tPathBuffer);

        //--Create directories for the translation.
        StarFileSystem::CreateDirectoriesFor(tNewPathBuf);

        //--Create a command, get all slashes sorted out, call.
        sprintf(tOperationBuf, "copy \"%s\" \"%s\"", rLuaInfo->mPath, tNewPathBuf);
        StarFileSystem::ModifySlashes(tOperationBuf);
        system(tOperationBuf);

        //--Diagnostics.
        fprintf(stderr, "%s\n", tOperationBuf);

        //--Clean.
        free(tFilePlusExtension);

        //--Next.
        rLuaInfo = (FileInfo *)trLuaFileList->AutoIterate();
    }

    //--Diagnostics.
    fprintf(stderr, " Found %i files.\n", tFoundBaseFiles);

    ///--[Chapter C Alternate]
    ///--[Clean]
    delete trSheetFileList;
    delete trLuaFileList;
    delete tFileSystem;
}

///====================================== Language Packs ==========================================
void MonocerosTitle::CreateLanguagePack(const char *pLanguageName, const char *pFolderPath, const char *pLuaSuffix)
{
    ///--[Documentation]
    //--Creates a language package for the given language. pLanguageName should be the folder the
    //  translation is in (Data/Translations/[pLanguageName]/) and pLuaSuffix should be what files
    //  are being scooped up. For Japanese, pLuaSuffix is ".luajp".
    if(!pLanguageName || !pFolderPath || !pLuaSuffix) return;

    //--Fast-access pointers.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    StarFileSystem *tFileSystem = new StarFileSystem();

    //--Diagnostics.
    fprintf(stderr, "Creating language pack.\n");
    fprintf(stderr, " Language: %s\n", pLanguageName);
    fprintf(stderr, " Folder: %s\n", pFolderPath);
    fprintf(stderr, " Extension: %s\n", pLuaSuffix);

    ///--[Backup]
    //--Name of output file.
    char tNameBuf[STD_MAX_LETTERS];
    sprintf(tNameBuf, "Data/Translation%s.slf", pLanguageName);
    fprintf(stderr, " Output file name: %s\n", tNameBuf);

    //--If flagged, and the file already exists, move it.
    if(mCreateBackups && StarFileSystem::FileExists(tNameBuf))
    {
        StarFileSystem::MakeFileBackup(tNameBuf);
    }

    ///--[Clone .csv Files]
    //--Clean any data still in the SLM.
    rSLM->Clear();

    //--Create the path to the folder that holds translation information. Open it in the filesystem
    //  to look for .csv files. These files contain spreadsheet based translations which are supported.
    char tPathBuffer[STD_PATH_LEN];
    sprintf(tPathBuffer, "Data/Translations/%s/", pFolderPath);
    tFileSystem->Clear();
    tFileSystem->SetRecursiveFlag(false);
    tFileSystem->ScanDirectory(tPathBuffer);

    //--Diagnostics.
    fprintf(stderr, " Scanning for .csv files in %s.\n", tPathBuffer);

    //--Iterate and look for .csv files.
    int tFoundCSVFiles = 0;
    int tTotalFiles = tFileSystem->GetTotalEntries();
    for(int i = 0; i < tTotalFiles; i ++)
    {
        //--Get entry.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo) continue;

        //--No path, or path too short:
        if(!rFileInfo->mPath || strlen(rFileInfo->mPath) < 4) continue;

        //--Check the last four letters to see if they are .csv.
        int tLen = (int)strlen(rFileInfo->mPath);
        if(!strcasecmp(&rFileInfo->mPath[tLen-4], ".csv"))
        {
            tFoundCSVFiles ++;
            RootLump *nNewLump = CreateRootLumpFromFile(rFileInfo->mPath);
            if(nNewLump) rSLM->RegisterLump(nNewLump);
        }
    }

    //--Diagnostics.
    fprintf(stderr, " Found %i files.\n", tFoundCSVFiles);

    ///--[Search for Translated Lua Files]
    //--This requires scanning the entire filesystem for files with the extension of pLuaSuffix.
    //  For example, all files of ".luajp" for the Japanese translation.
    int tFoundBaseFiles = 0;
    fprintf(stderr, " Scanning base folder for extension %s.\n", pLuaSuffix);
    tFileSystem->Clear();
    tFileSystem->SetRecursiveFlag(true);
    tFileSystem->ScanDirectory("./");

    //--Properties of extension.
    int tExtensionLen = (int)strlen(pLuaSuffix);

    //--Iterate and look for .csv files.
    tTotalFiles = tFileSystem->GetTotalEntries();
    for(int i = 0; i < tTotalFiles; i ++)
    {
        //--Get entry.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo) continue;

        //--Check the extension:
        int tLen = (int)strlen(rFileInfo->mPath);
        if(!strcasecmp(&rFileInfo->mPath[tLen-tExtensionLen], pLuaSuffix))
        {
            tFoundBaseFiles ++;
            RootLump *nNewLump = CreateRootLumpFromFile(rFileInfo->mPath);
            if(nNewLump) rSLM->RegisterLump(nNewLump);
        }
    }

    //--Diagnostics.
    fprintf(stderr, " Found %i files.\n", tFoundBaseFiles);

    ///--[Chapter C Alternate]
    //--If in Developer mode, then the Chapter C/ folder is not in the same folder as the executable.
    //  Also searches Adventure's files since those may act as a base.
    if(xIsDeveloperMode)
    {
        //--Clear and rerun.
        int tFoundChapterCFiles = 0;
        tFileSystem->Clear();
        tFileSystem->SetRecursiveFlag(true);
        tFileSystem->ScanDirectory("../ProjectMonoceros/Chapter C/");
        fprintf(stderr, " Scanning monoceros folder for extension %s\n", pLuaSuffix);

        //--Properties of extension.
        int tExtensionLen = (int)strlen(pLuaSuffix);

        //--Iterate and look for .csv files.
        tTotalFiles = tFileSystem->GetTotalEntries();
        for(int i = 0; i < tTotalFiles; i ++)
        {
            //--Get entry.
            FileInfo *rFileInfo = tFileSystem->GetEntry(i);
            if(!rFileInfo) continue;

            //--Check the extension:
            int tLen = (int)strlen(rFileInfo->mPath);
            if(!strcasecmp(&rFileInfo->mPath[tLen-tExtensionLen], pLuaSuffix))
            {
                tFoundChapterCFiles ++;
                RootLump *nNewLump = CreateRootLumpFromFile(rFileInfo->mPath);
                if(nNewLump) rSLM->RegisterLump(nNewLump);
            }
        }

        //--Diagnostics.
        fprintf(stderr, " Found %i files.\n", tFoundChapterCFiles);

        //--Clear and rerun.
        int tFoundAdventureFiles = 0;
        tFileSystem->Clear();
        tFileSystem->SetRecursiveFlag(true);
        tFileSystem->ScanDirectory("../adventure/Games/AdventureMode/");
        fprintf(stderr, " Scanning adventure folder for extension %s\n", pLuaSuffix);

        //--Iterate and look for .csv files.
        tTotalFiles = tFileSystem->GetTotalEntries();
        for(int i = 0; i < tTotalFiles; i ++)
        {
            //--Get entry.
            FileInfo *rFileInfo = tFileSystem->GetEntry(i);
            if(!rFileInfo) continue;

            //--Check the extension:
            int tLen = (int)strlen(rFileInfo->mPath);
            if(!strcasecmp(&rFileInfo->mPath[tLen-tExtensionLen], pLuaSuffix))
            {
                tFoundAdventureFiles ++;
                RootLump *nNewLump = CreateRootLumpFromFile(rFileInfo->mPath);
                if(nNewLump) rSLM->RegisterLump(nNewLump);
            }
        }

        //--Diagnostics.
        fprintf(stderr, " Found %i files.\n", tFoundAdventureFiles);
    }

    ///--[Write]
    //--Diagnostics.
    fprintf(stderr, " Writing everything to disk.\n");

    //--Merge all lumps together.
    rSLM->MergeLumps();

    //--Write.
    rSLM->Write(tNameBuf);
    rSLM->Clear();

    ///--[Clean]
    delete tFileSystem;

    //--Diagnostics.
    fprintf(stderr, "--> Writing completed!\n");
}
void MonocerosTitle::DeployLanguagePack(const char *pLanguageName, const char *pFolderPath)
{
    ///--[Documentation]
    //--Locates a language pack on the hard drive, which is expected to contain a series of file
    //  paths and file data. Then, places the data on the hard drive at those paths. Can make
    //  backups if necessary.
    if(!pLanguageName || !pFolderPath) return;

    //--Fast-access pointers.
    StarLumpManager *rSLM = StarLumpManager::Fetch();

    //--Diagnostics.
    fprintf(stderr, "Deploying language pack.\n");
    fprintf(stderr, " Language: %s\n", pLanguageName);
    fprintf(stderr, " Folder: %s\n", pFolderPath);

    ///--[File Handling]
    //--Name of input file.
    char tNameBuf[STD_MAX_LETTERS];
    sprintf(tNameBuf, "Data/Translation%s.slf", pLanguageName);
    fprintf(stderr, " Input file name: %s\n", tNameBuf);

    //--Open and check.
    rSLM->Open(tNameBuf);
    if(!rSLM->IsFileOpen())
    {
        fprintf(stderr, " Unable to locate input file, stopping.\n");
        return;
    }

    ///--[Lump Handling]
    //--Get list of lumps.
    int tTotalLumps = rSLM->GetTotalLumps();
    RootLump *rLumpList = rSLM->GetLumpList();

    //--Diagnostics.
    fprintf(stderr, " Total Lumps: %i\n", tTotalLumps);

    //--Error check:
    if(!rLumpList || tTotalLumps < 1)
    {
        fprintf(stderr, " Lump count was zero or list was NULL. Stopping.\n");
        return;
    }

    //--For each lump:
    for(int i = 0; i < tTotalLumps; i ++)
    {
        DeployFileFromLump(i);
    }

    ///--[Clean]
    rSLM->Close();
}

///==================================== SLF to File Worker ========================================
void MonocerosTitle::DeployFileFromLump(int pSlot)
{
    ///--[Documentation]
    //--Given a slot which refers to a lump in the StarLumpManager, seeks to that slot and deploys
    //  it to the filesystem if it is a TRNFILE000.

    //--Fast-access pointers.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    VirtualFile *rVFile = rSLM->GetVirtualFile();
    RootLump *rLumpList = rSLM->GetLumpList();

    ///--[Constants]
    int cHeaderLen = 10;

    ///--[Setup]
    //--Seek to the location of that lump. This will skip over the header.
    rSLM->SlotwiseSeek(pSlot);

    //--Get the expected length from the lump. Subtract the header length which has already been skipped.
    int tExpectedDataSize = rLumpList[pSlot].mDataSize;
    tExpectedDataSize -= cHeaderLen;

    //--Copy out the path, which is null-terminated.
    char tPath[512];
    for(int i = 0; i < 512; i ++)
    {
        //--Copy.
        tExpectedDataSize --;
        rVFile->Read(&tPath[i], sizeof(char), 1);

        //--If it was a 0, stop.
        if(tPath[i] == 0) break;
    }

    //--Print.
    fprintf(stderr, "Path: %s\n", tPath);

    ///--[Path Redirects]
    //--Paths are broken into three categories. Base paths, Adventure paths, and Monoceros paths.
    //  Base paths are assumed to be local to the executable, Adventure is local to the Adventure
    //  install path, and Monoceros to the Monoceros path. These are technically variable since
    //  developer installations are located in different places. At this point, we redirect the path
    //  if it starts with certain directories.
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    fprintf(stderr, "Adventure Path: %s\n", rAdventurePath);

    //--Create the path needed for Monoceros. By default, it should be in the Adventure directory in the
    //  folder "Chapter C/". Note that we don't append the Chapter C/ part on because that's already
    //  in the infile path.
    char tMonocerosPath[512];
    if(!xIsDeveloperMode)
    {
        sprintf(tMonocerosPath, "%s", rAdventurePath);
    }
    //--On a developer build, Monoceros is external, and relative to the game execution directory.
    else
    {
        sprintf(tMonocerosPath, "../projectmonoceros/");
    }
    fprintf(stderr, "Monoceros Path: %s\n", tMonocerosPath);

    //--Monoceros Install:
    if(!strncasecmp(tPath, "Chapter C", 9))
    {
        //--Copy into temp buffer.
        char tTempPath[512];
        strcpy(tTempPath, tPath);

        //--Replace.
        sprintf(tPath, "%s%s", tMonocerosPath, tTempPath);
    }
    //--Adventure Install.
    else if(!strncasecmp(tPath, "AdventureMode", 13))
    {
        //--Copy into temp buffer.
        char tTempPath[512];
        strcpy(tTempPath, tPath);

        //--Replace.
        sprintf(tPath, "%s%s", rAdventurePath, tTempPath);
    }

    //--Diagnostics.
    fprintf(stderr, "Path after redirect: %s\n", tPath);

    ///--[Backups]
    //--If flagged, and the file already exists, move it.
    if(mCreateBackups && StarFileSystem::FileExists(tPath))
    {
        StarFileSystem::MakeFileBackup(tPath);
    }

    ///--[File Creation]
    //--The expected data size is now the size of the file.
    fprintf(stderr, "Size of file: %i\n", tExpectedDataSize);

    //--Stream the file so as not to run into large file problems.
    FILE *fOutfile = fopen(tPath, "wb");
    uint8_t tOutBuf[1024];
    while(tExpectedDataSize > 0)
    {
        //--Read 1024 bytes and output it.
        if(tExpectedDataSize >= 1024)
        {
            rVFile->Read(tOutBuf, sizeof(uint8_t), 1024);
            fwrite(tOutBuf, sizeof(uint8_t), 1024, fOutfile);
            tExpectedDataSize -= 1024;
        }
        //--Read the remainder of the file and output it, then stop.
        else
        {
            rVFile->Read(tOutBuf, sizeof(uint8_t), tExpectedDataSize);
            fwrite(tOutBuf, sizeof(uint8_t), tExpectedDataSize, fOutfile);
            tExpectedDataSize = 0;
        }
    }
    fclose(fOutfile);
}

///==================================== File To SLF Worker ========================================
RootLump *MonocerosTitle::CreateRootLumpFromFile(const char *pPath)
{
    ///--[Documentation]
    //--Given a path, scans that file and returns a RootLump containing its binary data. If something
    //  goes wrong, returns NULL.
    if(!pPath) return NULL;

    //--Constants.
    int cHeaderLen = 10;

    ///--[Paths]
    //--Get the name of the file. This is everything after the first slash.
    //  Ex: Data/Translations/Dogs.csv -> Dogs.csv
    int tLen = (int)strlen(pPath);
    int tNameStart = 0;
    for(int p = tLen; p >= 0; p --)
    {
        if(pPath[p] == '/' || pPath[p] == '\\')
        {
            tNameStart = p+1;
            break;
        }
    }

    //--Trim off the front of the path. We're looking for "Data/". This makes the path
    //  local to the installation directory.
    //  Ex: C:/Games/WHI/Data/Translations/Dogs.csv -> Data/Translations/Dogs.csv
    int tPathStart = -1;
    for(int p = tLen - 5; p >= 0; p --)
    {
        if(!strncasecmp(&pPath[p], "Data/", 5))
        {
            tPathStart = p;
            break;
        }
    }

    //--If the path start value is -1 then it wasn't found. Search for "AdventureMode/" instead.
    //  Alternately, if "Chapter C/" is found first, use that as the stopping point. These paths
    //  may be in different directories in developer mode!
    if(tPathStart == -1)
    {
        for(int p = tLen - 14; p >= 0; p --)
        {
            if(!strncasecmp(&pPath[p], "AdventureMode/", 14))
            {
                tPathStart = p;
                break;
            }
            if(!strncasecmp(&pPath[p], "Chapter C/", 10))
            {
                tPathStart = p;
                break;
            }
        }
    }

    //--Get the new path length.
    int tNewPathLen = (int)strlen(&pPath[tPathStart]);

    ///--[Lump Setup]
    //--Storage lump.
    RootLump *nLump = (RootLump *)starmemoryalloc(sizeof(RootLump));
    nLump->Init();
    nLump->mOwnsData = true;
    nLump->mUsesStarArray = true;
    nLump->rDataObject = new StarArray();
    ResetString(nLump->mLookup.mName, &pPath[tNameStart]);

    ///--[File Copying]
    //--Open and check the file. If it fails to open, returns NULL, clears data, and prints a warning.
    FILE *fInfile = fopen(pPath, "rb");
    if(!fInfile)
    {
        delete nLump->rDataObject;
        free(nLump->mLookup.mName);
        free(nLump);
        fprintf(stderr, "MonocerosTitle:CreateRootLumpFromFile() - Error. Unable to open file %s\n", pPath);
        return NULL;
    }

    //--Get filesize.
    fseek(fInfile, 0, SEEK_END);
    uint64_t tFileLength = ftell(fInfile);

    //--Allocate space in the array, plus the length of the path.
    fseek(fInfile, 0, SEEK_SET);
    nLump->mDataSize = tFileLength + tNewPathLen + cHeaderLen;
    nLump->rDataObject->Allocate(tFileLength + tNewPathLen + cHeaderLen);

    //--Write the header.
    char cHeader[11];
    strcpy(cHeader, "TRNFILE000");
    nLump->rDataObject->WriteToArray(cHeader, 0, cHeaderLen);

    //--Write the path into the array.
    nLump->rDataObject->WriteToArray(&pPath[tPathStart], cHeaderLen, tNewPathLen+1);

    //--Read.
    nLump->rDataObject->ReadFileIntoMemory(tNewPathLen+1+cHeaderLen, tFileLength, fInfile);

    ///--[Clean Up]
    //--Close.
    fclose(fInfile);

    //--Return.
    return nLump;
}
