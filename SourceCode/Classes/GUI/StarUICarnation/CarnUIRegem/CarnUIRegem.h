///======================================== CarnUIRegem ===========================================
//--UI used to modify gems, which are called "Modifications" in Carnation. These function the same
//  as RoP's gems except they can't be merged, and also only one can be removed from an item and
//  the item is destroyed in the process. This UI is for adding gems to items, and can be summoned
//  from the inventory UI to deconstruct an item and liberate one gem from it.

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CarnUIRegem : public CarnUICommon
{
    protected:
    ///--[Constants]
    //--Scrollbars
    static const int cxCarnScrollBuf = 3;
    static const int cxCarnScrollPage = 22;

    //--Hitboxes
    static const int cxHitbox_ScrollUp    =  0;
    static const int cxHitbox_ScrollBd    =  1;
    static const int cxHitbox_ScrollDn    =  2;
    static const int cxHitbox_SelectionLo =  3;
    static const int cxHitbox_SelectionHi =  8;
    static const int cxHitbox_ReplaceLo   =  9;
    static const int cxHitbox_ReplaceHi   = 30;
    static const int cxHitbox_Disassemble = 31;

    ///--[System]
    AdventureItem *rConsideredItem;
    bool mExitsToInventory;
    bool mExitsToEquipment;

    ///--[Disassembly]
    bool mIsDisassembleMode;

    ///--[Selection]
    int mSelectionCursor;
    int mSelectionTimer;

    ///--[Description]
    AdventureItem *rLastDescriptionObject;
    StarLinkedList *mDescription; //char *, master

    ///--[Replacements]
    int mReplacementSkip;
    StarLinkedList *mrReplacementList; //AdventureItem *, reference

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Button;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Button;
        StarBitmap *rOverlay_Cursor;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rScrollbar_Static;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIRegem();
    virtual ~CarnUIRegem();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    void SetExitToInventory(bool pFlag);
    void SetExitToEquipment(bool pFlag);
    void SetConsideredItem(AdventureItem *pItem);
    void HandleExit();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void SetDescription(AdventureItem *pHighlightedItem);
    void BuildReplacementList();
    void ExecuteReplacement(int pSelectionSlot, AdventureItem *pReplacement);
    void ExecuteDisassembly(int pSelectionSlot);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    void UpdateSelectionMode();
    void UpdateReplacementMode();
    void UpdateDisassemblyMode();
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pAlpha);

    //--Pointer Routing
    //--Static Functions
    static CarnUIRegem *Fetch();

    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


