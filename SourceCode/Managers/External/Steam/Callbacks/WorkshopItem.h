///======================================== WorkshopItem ===========================================
//--Represents a callback handler for steam workshop items. This is required to upload new items or
//  to download and unpack them. It technically isn't needed to actually run them, since the game is
//  designed to handle mods on other platforms without needing Steam to enumerate them.
//--Members and functions are disabled when not compiling for a steam build.
#include "RootObject.h"
#if defined _STEAM_API_
    #include "steam_api.h"
    #include "steam_api_flat.h"
#endif

#pragma once

///========================================= Includes =============================================
///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class WorkshopItem : public RootObject
{
    public:
    #if defined _STEAM_API_
    ///--[System]
    SteamAPICall_t mHandle;

    ///--[Upload Properties]
    char *mUploadFolder;
    char *mAltFolder;
    char *mUploadTitle;
    char *mUploadDescription;
    char *mUploadLanguage;
    char *mUploadMetadata;
    char *mUploadChangelog;

    ///--[Call Results]
    CCallResult<WorkshopItem, CreateItemResult_t>       mCreateItemResult;
    CCallResult<WorkshopItem, DeleteItemResult_t>       mDeleteItemResult;
    CCallResult<WorkshopItem, SubmitItemUpdateResult_t> mSubmitItemUpdateResult;
    #endif

    protected:

    public:
    //--System
    WorkshopItem();
    ~WorkshopItem();
    static void DeleteThis(void *pPtr);
    #if defined _STEAM_API_

    //--Public Variables
    //--Property Queries
    bool IsUploadReady();

    //--Manipulators
    void SetCreateMode(const char *pFolderPath);
    void SetUploadTitle(const char *pTitle);
    void SetUploadDescription(const char *pDescription);
    void SetUploadLanguage(const char *pLanguage);
    void SetUploadMetadata(const char *pMetadata);
    void SetUploadChangelog(const char *pChangelog);
    void SetDeleteMode();

    //--Core Methods
    void OnItemCreated(CreateItemResult_t *pCallback, bool pIOFailure);
    void OnItemDeleted(DeleteItemResult_t *pCallback, bool pIOFailure);
    void OnItemSubmitted(SubmitItemUpdateResult_t *pCallback, bool pIOFailure);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
    #endif
};

//--Hooking Functions
#if defined _STEAM_API_
int Hook_WorkshopItem_SetProperty(lua_State *L);
#endif


