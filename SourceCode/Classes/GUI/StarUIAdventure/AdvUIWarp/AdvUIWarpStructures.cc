//--Base
#include "AdvUIWarpStructures.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///====================================== WarpRegionPack ==========================================
#ifndef WARPSTRUCTURESIMP //Remove once refactoring is complete
#define WARPSTRUCTURESIMP
void WarpRegionPack::Initialize()
{
    //--Members
    mLocations = new StarLinkedList(true);
    mAlignments.Initialize();
    rMapImage = NULL;
    rMapOverlayImage = NULL;
    mCallBuffer = NULL;

    //--Advanced Maps
    mIsUsingAdvancedMaps = false;
    mAdvancedClampLft = 0.0f;
    mAdvancedClampTop = 0.0f;
    mAdvancedClampRgt = 0.0f;
    mAdvancedClampBot = 0.0f;
    mAdvancedMapPacks = new StarLinkedList(true);
}
void WarpRegionPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    WarpRegionPack *rPtr = (WarpRegionPack *)pPtr;
    delete rPtr->mLocations;
    free(rPtr->mCallBuffer);
    delete rPtr->mAdvancedMapPacks;
    free(rPtr);
}

///===================================== WarpLocationPack =========================================
void WarpLocationPack::Initialize()
{
    mRoom = NULL;
    mCoordX = 0.0f;
    mCoordY = 0.0f;
}
void WarpLocationPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    WarpLocationPack *rPtr = (WarpLocationPack *)pPtr;
    free(rPtr->mRoom);
    free(rPtr);
}
#endif
