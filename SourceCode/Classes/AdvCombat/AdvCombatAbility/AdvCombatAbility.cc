//--Base
#include "AdvCombatAbility.h"

//--Classes
#include "AdvCombat.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarlightString.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///--[Debug]
//#define ADVCAB_RESPONSE_DEBUG
#ifdef ADVCAB_RESPONSE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
    #include "AdvCombatEntity.h"
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
AdvCombatAbility::AdvCombatAbility()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATABILITY;

    //--[AdvCombatAbility]
    //--System
    mLocalName = InitializeString("Null");
    mDisplayName = InitializeString("Null");
    mScriptPath = NULL;
    memset(mScriptResponses, 0, sizeof(mScriptResponses));
    mCannotBeEquipped = false;
    mIsEquipped = false;
    mHasInternalVersion = ADVCA_HAS_NOT_RESOLVED;
    mLastCallNumber = 0;
    mRunIfNotEquipped = false;
    mOnlyRunIfUnlocked = false;

    //--Usage
    mMPCost = 0;
    mCPCost = 0;
    mIsUsableNow = false;
    mRequiresTarget = true;
    mOpensConfirmation = false;
    mCooldown = 0;

    //--Confirmation
    mConfirmationText = NULL;

    //--Display
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    mSimplifiedDescriptionLinesTotal = 0;
    mSimplifiedDescriptionLines = NULL;
    rIconBack = NULL;
    rIconFrame = NULL;
    rIcon = NULL;
    rCPIcon = NULL;
    rChargesIcon = NULL;

    //--Storage
    mIsUnlocked = false;
    mJPCost = 0;
    rOwningEntity = NULL;
    mTagList = new StarLinkedList(true);

    //--Special
    mUnlockExecScript = NULL;
}
AdvCombatAbility::~AdvCombatAbility()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    free(mConfirmationText);
    for(int i = 0; i < mDescriptionLinesTotal; i ++) delete mDescriptionLines[i];
    for(int i = 0; i < mSimplifiedDescriptionLinesTotal; i ++) delete mSimplifiedDescriptionLines[i];
    free(mDescriptionLines);
    free(mSimplifiedDescriptionLines);
    delete mTagList;
    free(mUnlockExecScript);
}

///--[Private Statics]
//--Used for enqueuing sound effects.
char AdvCombatAbility::xSoundEffectQueue[STD_MAX_LETTERS];
int AdvCombatAbility::xSoundEffectPriority = -1;

//--Used as a tracking number to quickly figure out which abilities are on a character's equip bar
//  when those abilities are calling a code.
int AdvCombatAbility::xLastCallNumber = 1;

///===================================== Property Queries =========================================
const char *AdvCombatAbility::GetInternalName()
{
    return mLocalName;
}
const char *AdvCombatAbility::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatAbility::GetScriptPath()
{
    return mScriptPath;
}
bool AdvCombatAbility::IsUsableNow()
{
    return mIsUsableNow;
}
bool AdvCombatAbility::NeedsTargetCluster()
{
    return mRequiresTarget;
}
bool AdvCombatAbility::ActivatesConfirmation()
{
    return mOpensConfirmation;
}
int AdvCombatAbility::GetCooldown()
{
    return mCooldown;
}
char *AdvCombatAbility::GetConfirmationText()
{
    return mConfirmationText;
}
int AdvCombatAbility::GetDescriptionLinesTotal()
{
    return mDescriptionLinesTotal;
}
StarlightString *AdvCombatAbility::GetDescriptionLine(int pSlot)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return NULL;
    return mDescriptionLines[pSlot];
}
int AdvCombatAbility::GetSimplifiedDescriptionLinesTotal()
{
    return mSimplifiedDescriptionLinesTotal;
}
StarlightString *AdvCombatAbility::GetSimplifiedDescriptionLine(int pSlot)
{
    if(pSlot < 0 || pSlot >= mSimplifiedDescriptionLinesTotal) return NULL;
    return mSimplifiedDescriptionLines[pSlot];
}
StarBitmap *AdvCombatAbility::GetIconBack()
{
    return rIconBack;
}
StarBitmap *AdvCombatAbility::GetIconFrame()
{
    return rIconFrame;
}
StarBitmap *AdvCombatAbility::GetIcon()
{
    return rIcon;
}
StarBitmap *AdvCombatAbility::GetCPIcon()
{
    return rCPIcon;
}
bool AdvCombatAbility::IsUnlocked()
{
    return mIsUnlocked;
}
int AdvCombatAbility::GetJPCost()
{
    return mJPCost;
}
int AdvCombatAbility::GetCPCost()
{
    return mCPCost;
}
int AdvCombatAbility::GetMPCost()
{
    return mMPCost;
}
bool AdvCombatAbility::IsEquipped()
{
    return mIsEquipped;
}
bool AdvCombatAbility::CanBeEquipped()
{
    return !mCannotBeEquipped;
}
int AdvCombatAbility::GetTagCount(const char *pTag)
{
    if(!pTag) return 0;
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(!rTag) return 0;
    return (*rTag);
}
int AdvCombatAbility::HasInternalVersion()
{
    return mHasInternalVersion;
}
int AdvCombatAbility::GetLastCallNumber()
{
    return mLastCallNumber;
}
const char *AdvCombatAbility::GetUnlockExecScript()
{
    return mUnlockExecScript;
}
bool AdvCombatAbility::RunsIfNotEquipped()
{
    return mRunIfNotEquipped;
}
bool AdvCombatAbility::RunsOnlyIfUnlocked()
{
    return mOnlyRunIfUnlocked;
}

///======================================== Manipulators ==========================================
void AdvCombatAbility::SetOwner(AdvCombatEntity *pOwner)
{
    rOwningEntity = pOwner;
}
void AdvCombatAbility::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void AdvCombatAbility::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
void AdvCombatAbility::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void AdvCombatAbility::SetScriptResponse(int pIndex, bool pFlag)
{
    if(pIndex < 0 || pIndex >= ACA_SCRIPT_CODE_TOTAL) return;
    mScriptResponses[pIndex] = pFlag;
}
void AdvCombatAbility::SetUsable(bool pIsUsable)
{
    mIsUsableNow = pIsUsable;
}
void AdvCombatAbility::SetEquippable(bool pIsEquippable)
{
    mCannotBeEquipped = !pIsEquippable;
}
void AdvCombatAbility::SetNeedsTarget(bool pNeedsTarget)
{
    mRequiresTarget = pNeedsTarget;
}
void AdvCombatAbility::SetActivatesConfirmation(bool pFlag, const char *pConfirmationString)
{
    mOpensConfirmation = pFlag;
    ResetString(mConfirmationText, pConfirmationString);
}
void AdvCombatAbility::SetDescription(const char *pDescription)
{
    ///--[Documentation]
    //--Resets the description, taking a single line and breaking it into pieces.

    ///--[Deallocate, Clear]
    //--Deallocate.
    for(int i = 0; i < mDescriptionLinesTotal; i ++) delete mDescriptionLines[i];
    free(mDescriptionLines);

    //--Zero.
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    if(!pDescription) return;

    ///--[Subdivide]
    //--Find out how many line-breaks there are in the description.
    int tLinesNeeded = 1;
    int cLen = (int)strlen(pDescription);
    for(int i = 0; i < cLen; i ++)
    {
        if(pDescription[i] == '\n' || pDescription[i] == 10 || pDescription[i] == 13)
        {
            tLinesNeeded ++;
        }
    }

    ///--[Allocate]
    //--Allocate space.
    SetMemoryData(__FILE__, __LINE__);
    mDescriptionLinesTotal = tLinesNeeded;
    mDescriptionLines = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mDescriptionLinesTotal);
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        mDescriptionLines[i] = new StarlightString();
        mDescriptionLines[i]->SetString(" ");
    }

    ///--[Populate]
    //--Iterate, populate.
    char tCurBuf[1024];
    memset(tCurBuf, 0, sizeof(char) * 1024);
    int tCurrentChar = 0;
    int tCurrentLine = 0;
    for(int i = 0; i < cLen; i ++)
    {
        //--On a line break, end the string and create a new one.
        if(pDescription[i] == '\n' || pDescription[i] == 10 || pDescription[i] == 13)
        {
            //--Error check:
            if(tCurrentLine >= mDescriptionLinesTotal) break;

            //--Set.
            mDescriptionLines[tCurrentLine]->SetString(tCurBuf);

            //--Reset string values.
            tCurrentChar = 0;
            tCurrentLine ++;
            tCurBuf[0] = '\0';
        }
        //--Copy across.
        else
        {
            tCurBuf[tCurrentChar+0] = pDescription[i];
            tCurBuf[tCurrentChar+1] = '\0';
            tCurrentChar ++;
        }
    }

    //--If there's anything in the buffer, put it in the last string.
    if(tCurBuf[0] != '\0' && tCurrentLine < mDescriptionLinesTotal)
    {
        mDescriptionLines[tCurrentLine]->SetString(tCurBuf);
    }
}
void AdvCombatAbility::AllocateDescriptionImages(int pImages)
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        if(mDescriptionLines[i]) mDescriptionLines[i]->AllocateImages(pImages);
    }
}
void AdvCombatAbility::SetDescriptionImage(int pSlot, float pYOffset, const char *pDLPath)
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        if(mDescriptionLines[i]) mDescriptionLines[i]->SetImageS(pSlot, pYOffset, pDLPath);
    }
}
void AdvCombatAbility::CrossreferenceDescriptionImages()
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        if(mDescriptionLines[i]) mDescriptionLines[i]->CrossreferenceImages();
    }
}
void AdvCombatAbility::SetSimplifiedDescription(const char *pDescription)
{
    //--Deallocate.
    for(int i = 0; i < mSimplifiedDescriptionLinesTotal; i ++) delete mSimplifiedDescriptionLines[i];
    free(mSimplifiedDescriptionLines);

    //--Zero.
    mSimplifiedDescriptionLinesTotal = 0;
    mSimplifiedDescriptionLines = NULL;
    if(!pDescription) return;

    //--Find out how many line-breaks there are in the description.
    int tLinesNeeded = 1;
    int cLen = (int)strlen(pDescription);
    for(int i = 0; i < cLen; i ++)
    {
        if(pDescription[i] == '\n' || pDescription[i] == 10 || pDescription[i] == 13)
        {
            tLinesNeeded ++;
        }
    }

    //--Allocate space.
    SetMemoryData(__FILE__, __LINE__);
    mSimplifiedDescriptionLinesTotal = tLinesNeeded;
    mSimplifiedDescriptionLines = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mSimplifiedDescriptionLinesTotal);
    for(int i = 0; i < mSimplifiedDescriptionLinesTotal; i ++) mSimplifiedDescriptionLines[i] = NULL;

    //--Iterate, populate.
    char tCurBuf[1024];
    tCurBuf[0] = '\0';
    int c = 0;
    int p = 0;
    for(int i = 0; i < cLen; i ++)
    {
        //--On a line break, end the string and create a new one.
        if(pDescription[i] == '\n' || pDescription[i] == 10 || pDescription[i] == 13)
        {
            //--Error check:
            if(p >= mSimplifiedDescriptionLinesTotal) break;

            //--Set.
            mSimplifiedDescriptionLines[p] = new StarlightString();
            mSimplifiedDescriptionLines[p]->SetString(tCurBuf);

            //--Reset string values.
            c = 0;
            p ++;
            tCurBuf[0] = '\0';
        }
        //--Copy across.
        else
        {
            tCurBuf[c+0] = pDescription[i];
            tCurBuf[c+1] = '\0';
            c ++;
        }
    }

    //--If there's anything in the buffer, put it in the last string.
    if(tCurBuf[0] != '\0' && p < mSimplifiedDescriptionLinesTotal)
    {
        mSimplifiedDescriptionLines[p] = new StarlightString();
        mSimplifiedDescriptionLines[p]->SetString(tCurBuf);
    }
}
void AdvCombatAbility::AllocateSimplifiedDescriptionImages(int pImages)
{
    for(int i = 0; i < mSimplifiedDescriptionLinesTotal; i ++)
    {
        if(mSimplifiedDescriptionLines[i]) mSimplifiedDescriptionLines[i]->AllocateImages(pImages);
    }
}
void AdvCombatAbility::SetSimplifiedDescriptionImage(int pSlot, float pYOffset, const char *pDLPath)
{
    for(int i = 0; i < mSimplifiedDescriptionLinesTotal; i ++)
    {
        if(mSimplifiedDescriptionLines[i]) mSimplifiedDescriptionLines[i]->SetImageS(pSlot, pYOffset, pDLPath);
    }
}
void AdvCombatAbility::CrossreferenceSimplifiedDescriptionImages()
{
    for(int i = 0; i < mSimplifiedDescriptionLinesTotal; i ++)
    {
        if(mSimplifiedDescriptionLines[i]) mSimplifiedDescriptionLines[i]->CrossreferenceImages();
    }
}
void AdvCombatAbility::SetIconBack(const char *pDLPath)
{
    //--Set.
    rIconBack = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--Error check.
    if(!rIconBack && strcasecmp(pDLPath, "Null"))
    {
        fprintf(stderr, "SetIconBack(): Warning, path %s not found.\n", pDLPath);
    }
}
void AdvCombatAbility::SetIconFrame(const char *pDLPath)
{
    //--Set.
    rIconFrame = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--Error check.
    if(!rIconFrame && strcasecmp(pDLPath, "Null"))
    {
        fprintf(stderr, "SetIconFrame(): Warning, path %s not found.\n", pDLPath);
    }
}
void AdvCombatAbility::SetIcon(const char *pDLPath)
{
    //--Set.
    rIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--Error check.
    if(!rIcon && strcasecmp(pDLPath, "Null"))
    {
        fprintf(stderr, "SetIcon(): Warning, path %s not found. Ability: %s\n", pDLPath, mLocalName);
    }
}
void AdvCombatAbility::SetCPIcon(const char *pDLPath)
{
    //--Set.
    rCPIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--Error check.
    if(!rCPIcon && strcasecmp(pDLPath, "Null"))
    {
        fprintf(stderr, "SetCPIcon(): Warning, path %s not found.\n", pDLPath);
    }
}
void AdvCombatAbility::SetChargesIcon(const char *pDLPath)
{
    //--Set.
    rChargesIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--Error check.
    if(!rChargesIcon && strcasecmp(pDLPath, "Null"))
    {
        fprintf(stderr, "SetChargesIcon(): Warning, path %s not found.\n", pDLPath);
    }
}
void AdvCombatAbility::SetUnlocked(bool pFlag)
{
    mIsUnlocked = pFlag;
}
void AdvCombatAbility::SetJPCost(int pCost)
{
    mJPCost = pCost;
}
void AdvCombatAbility::SetCPCost(int pCost)
{
    mCPCost = pCost;
}
void AdvCombatAbility::SetMPCost(int pCost)
{
    mMPCost = pCost;
}
void AdvCombatAbility::SetEquipped(bool pFlag)
{
    mIsEquipped = pFlag;
}
void AdvCombatAbility::SetCooldown(int pTurns)
{
    mCooldown = pTurns;
}
void AdvCombatAbility::AddTag(const char *pTag)
{
    if(!pTag) return;
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(rTag)
    {
        (*rTag) = (*rTag) + 1;
    }
    else
    {
        int *nTag = (int *)starmemoryalloc(sizeof(int));
        *nTag = 1;
        mTagList->AddElement(pTag, nTag, &FreeThis);
    }
}
void AdvCombatAbility::RemoveTag(const char *pTag)
{
    if(!pTag) return;
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(!rTag) return;

    if(*rTag > 1)
    {
        (*rTag) = (*rTag) - 1;
    }
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}
void AdvCombatAbility::SetInternalVersionFlag(int pFlag)
{
    mHasInternalVersion = pFlag;
}
void AdvCombatAbility::SetUnlockExecScript(const char *pScript)
{
    //--Clearing.
    if(!pScript || !strcasecmp(pScript, "NULL"))
    {
        ResetString(mUnlockExecScript, NULL);
        return;
    }

    //--Set.
    ResetString(mUnlockExecScript, pScript);
}
void AdvCombatAbility::SetRunIfNotEquipped(bool pFlag)
{
    mRunIfNotEquipped = pFlag;
}
void AdvCombatAbility::SetOnlyRunIfUnlocked(bool pFlag)
{
    mOnlyRunIfUnlocked = pFlag;
}

///======================================== Core Methods ==========================================
void AdvCombatAbility::CallCode(int pCode)
{
    //--Script path must exist.
    DebugPush(true, "Calling Ability response script: Ability: %s Codes: %i - %s\n", mLocalName, pCode, AdvCombatEntity::GetStringFromCode(pCode));
    if(!mScriptPath)
    {
        DebugPop("Failed, no response script.\n");
        return;
    }

    //--Code must be within range.
    if(pCode < 0 || pCode >= ACA_SCRIPT_CODE_TOTAL)
    {
        DebugPop("Failed, invalid code.\n");
        return;
    }

    //--Code must be toggled true.
    if(!mScriptResponses[pCode])
    {
        DebugPop("Failed, not flagged to respond to this code.\n");
        return;
    }

    //--Fire!
    mLastCallNumber = xLastCallNumber;
    DebugPrint("Executing script path %s with code %i\n", mScriptPath, (int)pCode);
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)pCode);
    DebugPop("Finished.\n");
}
void AdvCombatAbility::EnqueueAsEvent(int pPriority)
{
    ///--[Documentation]
    //--Called during ACA_SCRIPT_CODE_EVENT_QUEUED as a response, this causes the ability to enqueue itself as
    //  an event with the given priority. When doing this, the zeroth target cluster is the one selected.
    //--Priority indicates where in the event queue the ability needs to be. 0 is the ability that is active,
    //  negative is before it, positive is after it.
    CombatEventPack *nPackage = (CombatEventPack *)starmemoryalloc(sizeof(CombatEventPack));
    nPackage->Initialize();
    nPackage->mPriority = pPriority;
    nPackage->rAbility = this;
    nPackage->rOriginator = rOwningEntity;

    //--Order combat to clear target clusters.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    rAdventureCombat->ClearTargetClusters();

    //--Fire target painting. Always use the zeroth target cluster. If somehow there was no cluster, don't enqueue the event.
    CallCode(ACA_SCRIPT_CODE_PAINTTARGETS_RESPONSE);
    TargetCluster *rZeroCluster = rAdventureCombat->GetTargetClusterI(0);
    if(!rZeroCluster && NeedsTargetCluster())
    {
        CombatEventPack::DeleteThis(nPackage);
        return;
    }

    //--Clone the target cluster.
    nPackage->mTargetCluster = rZeroCluster->Clone();

    //--Enqueue the event. Response events cannot fire additional events.
    rAdventureCombat->EnqueueEvent(nPackage);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void AdvCombatAbility::RenderAt(float pX, float pY)
{
    //--Renders all layers of this ability's icons at the given location.
    if(rIconBack)  rIconBack-> Draw(pX, pY);
    if(rIconFrame) rIconFrame->Draw(pX, pY);
    if(rIcon)      rIcon->     Draw(pX, pY);
    if(rCPIcon)    rCPIcon->   Draw(pX, pY);
}
void AdvCombatAbility::RenderAtCombat(float pX, float pY, float pAlpha)
{
    //--As above, but changes color if this ability is not usable. Also renders
    if(IsUsableNow())
    {
        RenderAt(pX, pY);
        if(rChargesIcon) rChargesIcon->Draw(pX, pY);
        return;
    }

    //--Mix color down, render. Also renders charges if available.
    StarlightColor::SetMixer(0.50f, 0.50f, 0.50f, pAlpha);
    RenderAt(pX, pY);
    if(rChargesIcon) rChargesIcon->Draw(pX, pY);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}

///====================================== Pointer Routing =========================================
AdvCombatEntity *AdvCombatAbility::GetOwner()
{
    //--Gets the entity that owns this ability. Only populated during script calls, otherwise returns
    //  NULL. Assume this pointer is unstable.
    //--If the owner is NULL, the entity will ask the AdvCombat what to do as it is an ownerless
    //  object. The AdvCombat may order it to return NULL, or change logic.
    if(!rOwningEntity)
    {
        return AdvCombat::Fetch()->PushAbilityOwnerWhenNull();
    }

    //--Pass back the owner.
    return rOwningEntity;
}

///===================================== Static Functions =========================================
void AdvCombatAbility::HandleCombatActionSounds()
{
    //--Static function, called at the end of the tick. Whichever sound effect had the highest priority
    //  will play. If many sound effects trigger from actions at the same time, this prevents a lot of overlay.
    //  Keeps splash-damage attacks from hurting your ears.
    if(xSoundEffectPriority == -1) return;

    //--Play the sound effect if the priority was not -1.
    AudioManager::Fetch()->PlaySound(xSoundEffectQueue);

    //--Reset the data. If no new effect is set to play, the -1 code will be ignored.
    xSoundEffectPriority = -1;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
