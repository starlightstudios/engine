//--Base
#include "HitDetection.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

#define DEG90INRAD 3.1415926f / 2.0f
#define DEG45INRAD 3.1415926f / 4.0f

//#define _SHOW_GL_DEBUG_
#define _GET_EXTRA_ACCURACY_

#ifdef _SHOW_GL_DEBUG_
#include "DisplayManager.h"
#endif

void ProjectOntoAxis(float &sMin, float &sMax, PolygonalHitbox pPolygon, float pNormalAngle)
{
    //--Given a particular polygon, projects it onto a given axis.  Because we only need to know
    //  if there is a collision along that axis, it is only necessary to get the X component.
    //--If the GL flag is on, we can score the Y component to make the render easier to see.
    //--This version uses the structure PolygonalHitbox to make it easier to read.
    if(pPolygon.mPointsTotal < 3) return;

    //--For each point on the polygon...
    #ifdef _SHOW_GL_DEBUG_
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINE_LOOP);
    #endif
    for(int i = 0; i < pPolygon.mPointsTotal; i ++)
    {
        //--Optimized rotater.  We know the origin is zero, so the formulas are sped up.
        float tDistance = GetPlanarDistance(pPolygon.mXVertices[i], pPolygon.mYVertices[i], 0, 0);
        float tAngle = atan2(pPolygon.mYVertices[i], pPolygon.mXVertices[i]);
        tAngle = tAngle + pNormalAngle;
        float tX = cos(tAngle) * tDistance;
        #ifdef _SHOW_GL_DEBUG_
        float tY = sin(tAngle) * tDistance;
        glVertex2f(tX, tY);
        #endif

        //--Zeroth pass, always set.
        if(i == 0)
        {
            sMin = tX;
            sMax = tX;
        }
        //--Normal case`
        else
        {
            if(tX < sMin) sMin = tX;
            if(tX > sMax) sMax = tX;
        }
    }
    #ifdef _SHOW_GL_DEBUG_
    glEnd();
    #endif
}

bool IsCollisionPolyStruct(PolygonalHitbox pPolyA, PolygonalHitbox pPolyB)
{
    //--Identical to the above routine, but uses a structure to store the data for better readability
    //  and slightly less offset calculation.

    //--Debug Setup
    #ifdef _SHOW_GL_DEBUG_
    DisplayManager::StdModelPush();
    glTranslatef(400, 300, 0);
    glScalef(0.5f, 0.5f, 0.5f);
    #endif
    bool tSuccess = true;

    //--Variables
    int tSlot1;
    int tSlot2;
    float tMinA=0.0f, tMaxA=0.0f;
    float tMinB=0.0f, tMaxB=0.0f;

    //--Check all points on A.
    for(int i = 0; i < pPolyA.mPointsTotal; i ++)
    {
        //--Setup
        tSlot1 = i;
        tSlot2 = i+1;
        if(i == pPolyA.mPointsTotal - 1)
        {
            tSlot2 = 0;
        }

        //--Get the normal.
        float tAngle = atan2(pPolyA.mYVertices[tSlot2] - pPolyA.mYVertices[tSlot1], pPolyA.mXVertices[tSlot2] - pPolyA.mXVertices[tSlot1]);
        float tNormal = tAngle + DEG90INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }

        //--Project again for extra accuracy.  This is only necessary for non space-filling polygons
        //  of at least 3 sides.  Set the #define if you don't need this behavior.
        #ifdef _GET_EXTRA_ACCURACY_
        tNormal = tAngle + DEG45INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }
        #endif

        #ifdef _SHOW_GL_DEBUG_
        //--[Render the normal]
        float tMidX = (pPolyA.mXVertices[tSlot2] + pPolyA.mXVertices[tSlot1]) / 2.0f;
        float tMidY = (pPolyA.mYVertices[tSlot2] + pPolyA.mYVertices[tSlot1]) / 2.0f;
        float tNormalStartX = tMidX + (cos(tNormal) * 35.0f);
        float tNormalStartY = tMidY + (sin(tNormal) * 35.0f);
        glColor3f(1.0f, 1.0f, 0.0f);
        glBegin(GL_LINES);
            glVertex2f(tMidX, tMidY);
            glVertex2f(tNormalStartX, tNormalStartY);
        glEnd();

        //--Render a line representing the max and min positions.
        DisplayManager::StdModelPush();
        glColor3f(1.0f, 0.0f, 1.0f);
        //glTranslatef(400.0f, 300.0f, 0.f);
        //glRotatef(tAngle * TODEGREE, 0.0f, 0.0f, 1.0f);
        //glTranslatef(-400.0f, -300.0f, 0.f);
        glBegin(GL_LINES);
            glVertex2f(tMinA, 0);
            glVertex2f(tMaxA, 0);
        glEnd();
        glColor3f(0.0f, 1.0f, 1.0f);
        glBegin(GL_LINES);
            glVertex2f(tMinB, 0);
            glVertex2f(tMaxB, 0);
        glEnd();
        DisplayManager::StdModelPop();
        #endif
    }

    //--Check all points on B.
    for(int i = 0; i < pPolyB.mPointsTotal; i ++)
    {
        //--Setup
        tSlot1 = i;
        tSlot2 = i+1;
        if(i == pPolyB.mPointsTotal - 1)
        {
            tSlot2 = 0;
        }

        //--Get the normal.  It doesn't matter if it's normalized.
        float tAngle = atan2(pPolyB.mYVertices[tSlot2] - pPolyB.mYVertices[tSlot1], pPolyB.mXVertices[tSlot2] - pPolyB.mXVertices[tSlot1]);
        float tNormal = tAngle + DEG90INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }

        //--Project again for extra accuracy.
        #ifdef _GET_EXTRA_ACCURACY_
        tNormal = tAngle + DEG45INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }
        #endif

        #ifdef _SHOW_GL_DEBUG_
        //--[Render the normal]
        float tMidX = (pPolyB.mXVertices[tSlot2] + pPolyB.mXVertices[tSlot1]) / 2.0f;
        float tMidY = (pPolyB.mYVertices[tSlot2] + pPolyB.mYVertices[tSlot1]) / 2.0f;
        float tNormalStartX = tMidX + (cos(tNormal) * 35.0f);
        float tNormalStartY = tMidY + (sin(tNormal) * 35.0f);
        glColor3f(1.0f, 1.0f, 0.0f);
        glBegin(GL_LINES);
            glVertex2f(tMidX, tMidY);
            glVertex2f(tNormalStartX, tNormalStartY);
        glEnd();

        //--Render a line representing the max and min positions.
        DisplayManager::StdModelPush();
        glColor3f(1.0f, 0.0f, 1.0f);
        //glTranslatef(400.0f, 300.0f, 0.f);
        //glRotatef(tAngle * TODEGREE, 0.0f, 0.0f, 1.0f);
        //glTranslatef(-400.0f, -300.0f, 0.f);
        glBegin(GL_LINES);
            glVertex2f(tMinA, 0);
            glVertex2f(tMaxA, 0);
        glEnd();
        glColor3f(0.0f, 1.0f, 1.0f);
        glBegin(GL_LINES);
            glVertex2f(tMinB, 0);
            glVertex2f(tMaxB, 0);
        glEnd();
        DisplayManager::StdModelPop();
        #endif
    }
    #ifdef _SHOW_GL_DEBUG_
    DisplayManager::StdModelPop();
    #endif

    //--If we got this far, all cases crossed!  That's a collision.
    return tSuccess;
}
bool IsCollision3DRealPolyStruct(ThreeDimensionReal pDim1, PolygonalHitbox pPolyB)
{
    //--Macro, loads the pDim1 into a polygon and runs the above function.
    PolygonalHitbox tHitbox;
    float tVertexesX[4];
    float tVertexesY[4];
    tHitbox.mPointsTotal = 4;
    tHitbox.mXVertices = tVertexesX;
    tHitbox.mYVertices = tVertexesY;

    //--Cross-load.
    tVertexesX[0] = pDim1.mLft;
    tVertexesY[0] = pDim1.mTop;
    tVertexesX[1] = pDim1.mRgt;
    tVertexesY[1] = pDim1.mTop;
    tVertexesX[2] = pDim1.mRgt;
    tVertexesY[2] = pDim1.mBot;
    tVertexesX[3] = pDim1.mLft;
    tVertexesY[3] = pDim1.mBot;

    //--Run.
    return IsCollisionPolyStruct(tHitbox, pPolyB);
}

