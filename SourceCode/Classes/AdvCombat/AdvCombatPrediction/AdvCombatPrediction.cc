//--Base
#include "AdvCombatPrediction.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
AdvCombatPrediction::AdvCombatPrediction()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ============ RootObject ============ ]
    ///--[System]
    mType = POINTER_TYPE_ADVCBOMATPREDICTION;

    ///--[ ======= AdvCombatPrediction ======== ]
    ///--[System]
    mIsShowing = true;
    rHostEntity = NULL;

    ///--[Display]
    mUseLockPositions = false;
    mVisTimer = 0;
    mXOffset = 0.0f;
    mYOffset = 0.0f;
    mLockX = 0.0f;
    mLockY = 0.0f;
    mStringsTotal = 0;
    mStrings = NULL;

    ///--[Calculations]
    mXSize = 1.0f;
    mYSize = 1.0f;

    ///--[Images]
    mBorderCardInd = 1.0f;
    rTextFont = NULL;
    rBorderCard = NULL;
}
AdvCombatPrediction::~AdvCombatPrediction()
{
    for(int i = 0; i < mStringsTotal; i ++) delete mStrings[i];
    free(mStrings);
}

///===================================== Property Queries =========================================
bool AdvCombatPrediction::IsOfType(int pType)
{
    return (pType == mType);
}
bool AdvCombatPrediction::IsShowing()
{
    return mIsShowing;
}
bool AdvCombatPrediction::IsVisible()
{
    return (mVisTimer > 0);
}

///======================================== Manipulators ==========================================
void AdvCombatPrediction::SetShowing(bool pFlag)
{
    mIsShowing = pFlag;
}
void AdvCombatPrediction::SetLockPositionFlag(bool pFlag)
{
    mUseLockPositions = pFlag;
}
void AdvCombatPrediction::AllocateStrings(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mStringsTotal; i ++) delete mStrings[i];
    free(mStrings);

    //--Reset.
    mStringsTotal = 0;
    mStrings = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mStringsTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mStrings = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mStringsTotal);
    for(int i = 0; i < mStringsTotal; i ++) mStrings[i] = new StarlightString();
}
void AdvCombatPrediction::SetFont(StarFont *pFont)
{
    rTextFont = pFont;
}
void AdvCombatPrediction::SetBorderCard(StarBitmap *pBitmap, float pIndent)
{
    mBorderCardInd = pIndent;
    rBorderCard = pBitmap;
    if(mBorderCardInd < 1.0f) mBorderCardInd = 1.0f;
}
void AdvCombatPrediction::SetHostEntity(AdvCombatEntity *pEntity)
{
    rHostEntity = pEntity;
}
void AdvCombatPrediction::SetOffsets(float pXOffset, float pYOffset)
{
    mXOffset = pXOffset;
    mYOffset = pYOffset;
}
void AdvCombatPrediction::SetLockPositions(float pLockX, float pLockY)
{
    mLockX = pLockX;
    mLockY = pLockY;
}

///======================================== Core Methods ==========================================
void AdvCombatPrediction::RunCalculations()
{
    //--Once all setup work is completed, precalculates sizes.
    if(!rTextFont || !rBorderCard) return;

    //--Determine the longest string. There is a minimum size. We also order all strings to resolve
    //  their images while we're here.
    float tLongest = rBorderCard->GetTrueWidth();
    for(int i = 0; i < mStringsTotal; i ++)
    {
        mStrings[i]->CrossreferenceImages();
        float tLength = mStrings[i]->GetLength(rTextFont);
        if(tLength > tLongest) tLongest = tLength;
    }

    //--Width is the longest string plus padding.
    mXSize = (tLongest) + (ADVCOMPREDICTION_PADDING_BORDER * 2.0f) + (mBorderCardInd * 2.0f);

    //--Compute height.
    mYSize = ((rTextFont->GetTextHeight() + ADVCOMPREDICTION_PADDING_PER_LINE) * mStringsTotal) + (ADVCOMPREDICTION_PADDING_BORDER * 2.0f) + (mBorderCardInd * 2.0f) + ADVCOMPREDICTION_PADDING_TEXT_V;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void AdvCombatPrediction::Update()
{
    //--Visibility timing.
    if(mIsShowing)
    {
        if(mVisTimer < ADVCOMBPREDICTION_VIS_TICKS) mVisTimer ++;
    }
    else
    {
        if(mVisTimer > 0) mVisTimer --;
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void AdvCombatPrediction::Render()
{
    ///--[Documentation and Setup]
    //--Renders the object, either as offset from the host entity, or at a fixed position if no host
    //  entity exists.
    if(!rTextFont || !rBorderCard || mStringsTotal < 1) return;

    ///--[Visibility]
    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticOut(mVisTimer, ADVCOMBPREDICTION_VIS_TICKS);
    if(cAlpha <= 0.0f) return;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Compute X Position]
    //--If there is no entity to attach to, the offsets are the top-left positions.
    float cLft = mXOffset;

    //--If an entity exists to attach to, the offsets are relative to their center.
    if(rHostEntity)
    {
        cLft = rHostEntity->GetCombatX() + mXOffset - (mXSize * 0.50f);
    }

    //--Override if using a lock position.
    if(mUseLockPositions) cLft = mLockX;

    ///--[Compute Y Position]
    //--The prediction boxes always render bottom-up, flush with the entity UI. Offsets are applied after
    //  this calculation.
    float cBottomClamp = 380.0f;

    //--Top position.
    float cTop = cBottomClamp - mYSize;

    //--Add offset. This is to prevent prediction boxes from overlapping when there are many targets.
    cTop = cTop + mYOffset;

    //--Override if using a lock position.
    if(mUseLockPositions) cTop = mLockY;

    ///--[Border Card]
    //--Handled by subroutine.
    RenderExpandableHighlight(cLft, cTop, cLft + mXSize, cTop + mYSize, mBorderCardInd, rBorderCard);

    ///--[Strings]
    //--Render each string.
    float cXPos = cLft + mBorderCardInd + ADVCOMPREDICTION_PADDING_BORDER;
    float tYPos = cTop + mBorderCardInd + ADVCOMPREDICTION_PADDING_BORDER + ADVCOMPREDICTION_PADDING_TEXT_V;
    for(int i = 0; i < mStringsTotal; i ++)
    {
        mStrings[i]->DrawText(cXPos, tYPos, 0, 1.0f, rTextFont);
        tYPos = tYPos + rTextFont->GetTextHeight() + ADVCOMPREDICTION_PADDING_PER_LINE;
    }

    //--Clean.
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
AdvCombatEntity *AdvCombatPrediction::GetHost()
{
    return rHostEntity;
}
StarlightString *AdvCombatPrediction::GetString(int pIndex)
{
    if(pIndex < 0 || pIndex >= mStringsTotal) return NULL;
    return mStrings[pIndex];
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
