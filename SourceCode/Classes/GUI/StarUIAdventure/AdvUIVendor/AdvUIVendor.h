///======================================== AdvUIVendor ===========================================
//--Description

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIVendorStructures.h"
#include "AdventureItem.h"

///===================================== Local Structures =========================================
struct ShopInventoryPack;

///===================================== Local Definitions ========================================
//--Strings
#define ADVUIVENDOR_STRINGS_TOTAL 2

//--Icons
#define ADVUIVENDOR_ICONS_TOTAL 18
#define ADVUIVENDOR_RESISTANCE_BEGIN 6

//--Adamantite and Catalyst types. Defined in several spots.
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

///========================================== Classes =============================================
class AdvUIVendor : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    //--Timers
    static const int cxAdvBuyTicks = 7;
    static const int cxAdvSellTicks = 7;
    static const int cxAdvDetailsTicks = 7;
    static const int cxAdvAskEquipTicks = 7;
    static const int cxAdvAskMergeTicks = 7;
    static const int cxAdvAskDisassembleTicks = 7;
    static const int cxAdvUnlockCycleTicks = 60;
    static const int cxAdvUnlockPauseTicks = 30;

    //--Sizes
    static const int cxAdvScrollBuf = 3;
    static const int cxAdvScrollPageSize = 19;
    static constexpr float cxAdvMaxNameWid = 231.0f;        //For buy/sell modes, maximum width of an item name before it gets squished.
    static constexpr float cxAdvMaxGemNameWid = 600.0f;     //For gem modes, maximum width of an item name before it gets squished.

    //--Strings
    static const int cxHelpStringsTotal = 13;           //Number of unique StarlightStrings appearing on the help menu.
    static const int cxMergeStringsTotal = 13;          //Number of unique StarlightStrings appearing on the merge help menu.

    //--Stencil Interoperability
    static const int cxStencil = 20;                    //Stencil value, must be unique among all StarUIPieces so they don't cause render bugs.

    //--Unlock Positions.
    static const int cxAdv_UnlHdgNameX      = 269; //Heading for "Name"
    static const int cxAdv_UnlHdgY          = 142; //Y position of headings
    static const int cxAdv_UnlBackL         = 257; //L position for black backings.
    static const int cxAdv_UnlBackR         = 946; //R position for black backings.
    static const int cxAdv_UnlIconX         = 267; //X position to render item's icon.
    static const int cxAdv_UnlNameX         = 289; //X position of the name of the item
    static const int cxAdv_UnlMaterialsX    = 840; //X position of the materials count, or "Ready"
    static const int csAdv_UnlMaxNameWid    = 380; //Maximum name of an unlock item before it gets squished.
    static const int cxAdv_UnlockT          = 187; //Top position of the items listing.
    static const int cxAdv_UnlockH          =  26; //Height of the items listing.
    static const int cxAdv_IngredientL      = 300; //Ingredients stack in groups of three to use up space better. The L/M/R values
    static const int cxAdv_IngredientM      = 515; //represent the left, middle, and right positions.
    static const int cxAdv_IngredientR      = 730; //
    static const int cxAdv_IngredientW      = 215; //Width of an ingredient name before it gets squished.
    static const int cxAdv_IngredientT      =   0; //Added to the normal slot position to make it closer to the object ir represents.
    static const int cxAdv_IngredientH      =  24; //Height added per material.

    ///--[Per-Class Constants]
    //--Constants within a given class implementation, but can be modified by derived classes.
    int cScrollBuf;                                     //How many entries from the edge before scrolling when moving on a list.
    int cScrollPage;                                    //How many list entries appear on a page before scrollbars are necessary.
    float cYRenderConfirmCancelText;                    //When confirming a purchase, the text Confirm/Cancel appears at this Y position.

    ///--[System]
    bool mIsBuyOnly;
    int mArrowTimer;

    ///--[Common]
    int mCursor;
    int mSkip;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    ///--[Categories]
    int mCategoryCursor;
    EasingPack2D mCategoryHighlightPos;
    EasingPack2D mCategoryHighlightSize;
    StarLinkedList *mCategoryList; //ShopCategoryPack *, master

    ///--[Merge Help Menu]
    bool mIsShowingMergeHelp;
    int mMergeHelpVisibilityTimer;
    StarlightString *mMergeHelpMenuStrings[cxMergeStringsTotal];

    ///--[Shop Info]
    char *mShopName;
    char *mShopSetupPath;
    char *mShopTeardownPath;
    StarLinkedList *mShopInventory; //ShopInventoryPack *, master

    ///--[Comparison]
    bool mShowSwapArrows;
    int mCompareCharacterCur;
    int mCompareCharacterOld;
    int mCompareSwapTimer;
    bool mCanSwapCompareSlot;
    StarLinkedList *mComparisonSlotList; //char *, master
    char *mCompareSlot;

    ///--[Materials Pane]
    bool mComparisonShowMaterials;
    bool mMaterialsUsePages;
    int mMaterialsPage;
    StarLinkedList *mMaterialsPages; //ShopMaterialsPage *, master

    ///--[Details]
    bool mIsShowingDetails;
    int mDetailsTimer;

    ///--[Buy Mode]
    bool mIsBuyMode;
    bool mIsBuyingItem;
    int mBuyTimer;
    int mBuyQuantity;
    bool mAskToEquip;
    int mAskToEquipSlot;
    int mAskToEquipTimer;
    char *mBuyResponseScript;

    ///--[Sell Mode]
    bool mIsSellMode;
    bool mIsSellingItem;
    int mSellTimer;
    int mSellQuantity;

    ///--[Buyback Mode]
    bool mIsBuybackMode;

    ///--[Gems Mode]
    bool mAllowGemcutting;
    bool mIsGemsMode;
    bool mIsMergeMode;
    bool mIsConfirmMergeMode;
    bool mIsConfirmDisassembleMode;
    int mGemMergeTimer;
    int mGemConfirmMergeTimer;
    int mGemDisassembleTimer;
    char *mMergeGemOwner;
    AdventureItem *mMergeGem; //Note: Despite the m, not the master copy. Do not delete.

    ///--[Unlock Mode]
    bool mUnlockModePending;
    bool mIsUnlockMode;
    int mUnlockScrollTimer;
    int mUnlockScrollTimerPause;
    bool mUnlockScrollTimerUp;
    bool mIsUnlockingItem;
    int mUnlockConfirmTimer;
    bool mUnlockCanEquip;
    StarLinkedList *mMaterialsList; //char *, master

    ///--[Tags]
    int mStatisticIndexes[ADVUIVENDOR_ICONS_TOTAL];
    char mBonusTagNames[ADVUIVENDOR_ICONS_TOTAL][64];
    char mMalusTagNames[ADVUIVENDOR_ICONS_TOTAL][64];

    ///--[Strings]
    StarlightString *mShowHelpString;
    StarlightString *mShowDetailsString;
    StarlightString *mCharacterChangeString;
    StarlightString *mScrollFastString;
    StarlightString *mToggleCompareString;
    StarlightString *mConfirmPurchaseString;
    StarlightString *mCancelPurchaseString;
    StarlightString *mAdjustQuantityString;
    StarlightString *mTenQuantityString;
    StarlightString *mEquipNowString;
    StarlightString *mDontEquipString;
    StarlightString *mDisassembleString;
    StarlightString *mToggleCompareMaterialsString;
    StarlightString *mToggleMaterialsPageString;

    ///--[Strings
    union
    {
        struct
        {
            char *mPtr[ADVUIVENDOR_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mConfirmDisassembly;
            char *mDisassembleContents;
        }str;
    }AdvString;

    ///--[Images]
    struct
    {

        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Statistic;

        //--Images
        StarBitmap *rFrame_BuyItemPopup;
        StarBitmap *rFrame_Category;
        StarBitmap *rFrame_Comparison;
        StarBitmap *rFrame_Description;
        StarBitmap *rFrame_Platina;
        StarBitmap *rFrame_Vendor;
        StarBitmap *rFrame_Unlock;
        StarBitmap *rOverlay_ArrowLft;
        StarBitmap *rOverlay_ArrowRgt;
        StarBitmap *rOverlay_DetailBuy;
        StarBitmap *rOverlay_DetailBuyback;
        StarBitmap *rOverlay_DetailGems;
        StarBitmap *rOverlay_DetailGemsMerge;
        StarBitmap *rOverlay_DetailSell;
        StarBitmap *rOverlay_ExpandableHeader;
        StarBitmap *rOverlay_GemSlot;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;
        StarBitmap *rOverlay_MatArrowLft;
        StarBitmap *rOverlay_MatArrowRgt;
        StarBitmap *rScrollbar_Static;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rStencil_Unlock;

        //--Gems Subset
        StarBitmap *rFrame_GemDisassemblePopup;
        StarBitmap *rFrame_GemsAdamantite;
        StarBitmap *rFrame_GemsComparison;
        StarBitmap *rFrame_GemsContents;
        StarBitmap *rFrame_GemsInventory;
        StarBitmap *rFrame_GemsName;
        StarBitmap *rFrame_GemsPlatina;

        //--Common Parts
        StarBitmap *rPropertyIcons[ADVUIVENDOR_ICONS_TOTAL];
        StarBitmap *rGemIcons[ADITEM_MAX_GEMS];
        StarBitmap *rAdamantiteIcons[CRAFT_ADAMANTITE_TOTAL];
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIVendor();
    virtual ~AdvUIVendor();
    virtual void Construct();

    //--Public Variables
    static bool xDisallowEquipFromResponse;
    static char *xUnlockReplaceInfo;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    virtual void SetShopInformation(const char *pShopName, const char *pPath, const char *pTeardownPath);
    void SetBuyOnly(bool pFlag);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void RecomputeCategoryCursorPositions();
    virtual void ActivateMode(bool &sModeFlag);
    void ResetCursor();
    void HandleExit();

    ///--[ ===== Routine Files ====== ]
    ///--[Utility]
    //--Global Properties
    void SetGemcutterFlag(bool pFlag);
    void SetBuyResponseScript(const char *pPath);
    virtual void ScrollMode(int pAmount);

    //--Register/Unregister Items
    void RegisterItem(const char *pName, int pPriceOverride, int pQuantity);
    void UnregisterItem(const char *pName);

    //--Purchase/Equip Routines
    void PurchaseItem(ShopInventoryPack *rInvPack, int pQuantity, bool pAllowScriptResponse, bool pAllowAskEquipCheck);
    virtual int CheckEquippableFromPurchase(AdventureItem *pItem);
    virtual void EquipPurchasedItem();

    //--Entry Properties
    void SetItemDisplayName(const char *pName, const char *pDisplayName);

    //--Categories
    void RegisterCategory(const char *pName, const char *pImgPath);
    void RegisterItemToCategory(const char *pItemName, const char *pCategoryName);

    //--Details Mode
    bool CommonDetailsUpdate();

    //--Help Mode

    //--Quantities
    void AdjustQuantity(int &sQuantity, int pAmount);
    void ResolveQuantityClamps(int &sClampLo, int &sClampHi);

    //--Unlock Mode
    void SetUnlockCanEquip(bool pCanEquip);
    void SetUnlockVariable(const char *pItemName, const char *pVariablePath);
    void SetUnlockRemove(const char *pItemName, bool pRemoveOnUnlock);
    void SetUnlockExecute(const char *pItemName, const char *pExecPath);
    void AddItemUnlockRequirement(const char *pItemName, const char *pRequirementName, const char *pShortName, int pQuantity);

    //--Materials Handling
    void AddUnlockMaterial(const char *pItemName, const char *pShorthandName);
    void SetUnlockMaterialUsePages(bool pFlag);
    void ClearMaterialsPages();
    void AddMaterialsPage(const char *pName);
    void AddUnlockMaterialToPage(const char *pPageName, const char *pItemName, const char *pShorthand);

    //--Cursor Resolving
    AdventureItem *GetConsideredItem();
    ShopInventoryPack *GetConsideredItemPack();
    StarLinkedList *GetConsideredCategory();

    //--Comparison Handling
    virtual void SetComparisonCharacter(int pPartySlot);
    void ScrollComparisonCharacter(int pAmount);
    void ToggleComparisonSlot();
    virtual void RecheckComparisonCharacter(AdventureItem *pItem);
    void RecheckComparisonSlot(AdventureItem *pItem, AdvCombatEntity *pEntity);

    ///--[Common Render]
    void RenderOwnerFace(float pXPos, float pYPos, AdvCombatEntity *pCharacter);
    virtual void RenderModeTabs(float pColorAlpha);
    virtual void RenderModeTitles(float pColorAlpha);
    virtual void RenderCommonLft(float pVisAlpha);
    virtual void RenderCommonTop(float pVisAlpha);
    virtual void RenderCommonPlatina(float pColorAlpha);
    virtual void RenderItemDetails(int pTimer, int pTimerMax, AdventureItem *pItem);
    virtual void RenderItemPack(ShopInventoryPack *pItem, int pSlot, bool pIsOdd, float pAlpha);
    virtual void RenderItem(AdventureItem *pItem, int pSlot, bool pIsOdd, bool pIsSellPrice, float pAlpha);
    virtual void RenderGem(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha);
    virtual void RenderGemMerge(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha);
    virtual void RenderUnlockPack(ShopInventoryPack *pItem, int pSlot, bool pIsOdd, float pAlpha, float pScrollTimer);
    virtual void RenderComparisonPane(AdventureItem *pComparisonItem, float pAlpha);
    virtual bool RenderComparisonPaneGem(AdventureItem *pGem, float pAlpha);
    virtual void RenderUnlockMaterials(ShopInventoryPack *pItem, int pSlot, float pAlpha);

    ///--[Loop Handlers]
    void RenderShopItemListing(int pSkip, int pMaxPerPage, float pColorAlpha);
    void RenderInventoryItemListing(int pSkip, int pMaxPerPage, float pColorAlpha);
    void RenderBuybackItemListing(int pSkip, int pMaxPerPage, float pColorAlpha);
    void RenderGemsItemListing(int pSkip, int pMaxPerPage, float pColorAlpha);
    void RenderGemsMergeListing(int pSkip, int pMaxPerPage, float pColorAlpha);
    virtual void RenderUnlockListing(int pSkip, int pMaxPerPage, float pColorAlpha);

    ///--[ ===== Mode Handlers ====== ]
    ///--[Mode: Buy]
    void ActivateModeBuy();
    float GetBuyCursorNameWidth();
    virtual void UpdateModeBuy();
    virtual void UpdateBuyItem();
    virtual void UpdateBuyEquipItem();
    virtual void RenderLftBuy(float pVisAlpha);
    virtual void RenderTopBuy(float pVisAlpha);
    virtual void RenderRgtBuy(float pVisAlpha);
    virtual void RenderBotBuy(float pVisAlpha);
    virtual void RenderUnalignedBuy(float pVisAlpha);
    virtual void RenderBuyProperties(float pVisAlpha);
    virtual void RenderBuyItem(float pVisAlpha);
    virtual void RenderBuyEquipItem(float pVisAlpha);

    ///--[Mode: Sell]
    void ActivateModeSell();
    float GetSellCursorNameWidth();
    virtual void UpdateModeSell();
    virtual void UpdateSellItem();
    virtual void RenderLftSell(float pVisAlpha);
    virtual void RenderTopSell(float pVisAlpha);
    virtual void RenderRgtSell(float pVisAlpha);
    virtual void RenderBotSell(float pVisAlpha);
    virtual void RenderUnalignedSell(float pVisAlpha);
    void RenderSellProperties(float pVisAlpha);
    virtual void RenderSellItem(float pVisAlpha);

    ///--[Mode: Buyback]
    void ActivateModeBuyback();
    float GetBuybackCursorNameWidth();
    void UpdateModeBuyback();
    void UpdateBuybackItem();
    void UpdateBuybackEquipItem();
    virtual void RenderLftBuyback(float pVisAlpha);
    virtual void RenderTopBuyback(float pVisAlpha);
    virtual void RenderRgtBuyback(float pVisAlpha);
    virtual void RenderBotBuyback(float pVisAlpha);
    virtual void RenderUnalignedBuyback(float pVisAlpha);
    void RenderBuybackProperties(float pAlpha);
    void RenderBuybackItem(float pAlpha);
    void RenderBuybackEquipItem(float pAlpha);

    ///--[Mode: Gems]
    void ActivateModeGems();
    void RefreshGemMenuHelp();
    float GetGemsCursorNameWidth(float &sLeftPos);
    virtual void UpdateModeGems();
    void UpdateGemDisassembly();
    virtual void RenderTopGems(float pVisAlpha);
    virtual void RenderRgtGems(float pVisAlpha);
    virtual void RenderBotGems(float pVisAlpha);
    virtual void RenderUnalignedGems(float pVisAlpha);
    void RenderGemsDisassemble(float pVisAlpha);

    ///--[Mode: Gem Merge]
    void RefreshGemMergeMenuHelp();
    void UpdateModeGemsMerge();
    float GetGemsMergeCursorNameWidth(float &sLeftPos);
    void RenderGemsMerge(float pVisAlpha);
    virtual void RenderTopGemsMerge(float pVisAlpha);
    virtual void RenderRgtGemsMerge(float pVisAlpha);
    virtual void RenderBotGemsMerge(float pVisAlpha);
    virtual void RenderUnalignedGemsMerge(float pVisAlpha);
    virtual void RenderGemsMergeAdamantite(float pColorAlpha);
    virtual void RenderGemsMergeComparison(float pColorAlpha);
    virtual void RenderGemsMergeItem(float pVisAlpha);

    ///--[Mode: Unlock]
    void ActivateModeUnlock();
    float GetUnlockCursorNameWidth();
    bool HasEnoughOfUnlockItem(ShopUnlockPack *pUnlockPack, bool pIsFirstElement);
    bool MeetsUnlockRequirements(ShopInventoryPack *pPackage);
    void RemoveUnlockRequirements(ShopInventoryPack *pPackage);
    virtual void UpdateModeUnlock();
    void UpdateConfirmUnlock();
    virtual void RenderLftUnlock(float pVisAlpha);
    virtual void RenderTopUnlock(float pVisAlpha);
    virtual void RenderRgtUnlock(float pVisAlpha);
    virtual void RenderBotUnlock(float pVisAlpha);
    virtual void RenderUnalignedUnlock(float pVisAlpha);
    void RenderUnlockConfirm(float pAlpha);
    void RenderMaterial(const char *pName, const char *pShortname, ShopInventoryPack *pShopPackage, float pX, float pY, float pR, float pReqR, float pAlpha);
    virtual void RenderMaterials(float pAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderForeground(float pVisAlpha);
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


