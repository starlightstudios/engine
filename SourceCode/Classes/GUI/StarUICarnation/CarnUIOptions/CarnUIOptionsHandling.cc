//--Base
#include "CarnUIOptions.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

void CarnUIOptions::ConstructOptions()
{
    ///--[Documentation]
    //--Called when the object is opened, clears the existing list and builds options lists. This is deliberately
    //  done and redone on open in case derived classes want to dynamically change the option lists.
    //--This also calls the RefreshOptions() function to get their current values.
    mMasterOptionList->ClearList();
    for(int i = 0; i < mResolutionsTotal; i ++) free(mResolutionStrings[i]);
    free(mResolutionStrings);

    //--Zero.
    rDraggedOption = NULL;
    rDescriptionPack = NULL;
    mResolutionsTotal = 0;
    mResolutionStrings = NULL;

    //--Setup.
    CarnMenuOptionPack *nOptionPack = NULL;

    //--Constants.
    float cDescWid = 440.0f;

    ///--[Resolution Information]
    //--Get resolution information to be placed into the video options.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    mResolutionsTotal = rDisplayManager->GetTotalDisplayModes();
    mResolutionStrings = (char **)starmemoryalloc(sizeof(char *) * mResolutionsTotal);
    for(int i = 0; i < mResolutionsTotal; i ++)
    {
        DisplayInfo tInfo = rDisplayManager->GetDisplayMode(i);
        mResolutionStrings[i] = InitializeString("%i x %i : %i", tInfo.mWidth, tInfo.mHeight, tInfo.mRefreshRate);
    }

    ///--[Gameplay Options]
    //--Gameplay Options.
    StarLinkedList *nGameplayList = new StarLinkedList(true);
    mMasterOptionList->AddElementAsTail("Gameplay", nGameplayList, &StarLinkedList::DeleteThis);

    //--Difficulty. Uses strings.
    if(!mIsTitleMode)
    {
        nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
        nOptionPack->Initialize();
        nOptionPack->SetName("Difficulty");
        nOptionPack->mEditType = CMOP_INTRANGE;
        nOptionPack->mValueStrings->AddElementAsTail("Easy",   nOptionPack, &DontDeleteThis);
        nOptionPack->mValueStrings->AddElementAsTail("Normal", nOptionPack, &DontDeleteThis);
        nOptionPack->mValueStrings->AddElementAsTail("Hard",   nOptionPack, &DontDeleteThis);
        nOptionPack->mRangeLo = 0.0f;
        nOptionPack->mRangeHi = 2.0f;
        nOptionPack->SetDescription("Game Difficulty[BR]Determines how much progress is lost when you are defeated in a dungeon or retreat from one.[BR][BR]On Easy, you lose nothing.[BR][BR]Normal: 25[PCT] EXP/Cash, 1/2 items when retreating. 50[PCT] EXP/Cash, all items when dying.[BR][BR]Hard: 50[PCT] EXP/Cash, all items when retreating. 90[PCT] EXP/Cash, all items when dying.", CarnImages.rFont_Mainline, cDescWid);
        nGameplayList->AddElementAsTail("Difficulty", nOptionPack, &CarnMenuOptionPack::DeleteThis);
    }

    //--Dialogue Text Speed.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Dialogue Speed");
    nOptionPack->mEditType = CMOP_INTRANGE;
    nOptionPack->mValueStrings->AddElementAsTail("Very Slow",     nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Slow",          nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Normal",        nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Fast",          nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Gotta Go Fast", nOptionPack, &DontDeleteThis);
    nOptionPack->mRangeLo = 0.0f;
    nOptionPack->mRangeHi = 4.0f;
    nOptionPack->SetDescription("Dialogue Text Speed[BR]How quickly non-combat dialogue proceeds. Normal speed is about 1 letter every 1/15th of a second.[BR][BR]You can hold down the Z key or left-click to speed up dialogue, or Z+X to massively speed it up.", CarnImages.rFont_Mainline, cDescWid);
    nGameplayList->AddElementAsTail("Dialogue Speed", nOptionPack, &CarnMenuOptionPack::DeleteThis);

    //--Combat Dialogue Text Speed.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Combat Dialogue Speed");
    nOptionPack->mEditType = CMOP_INTRANGE;
    nOptionPack->mValueStrings->AddElementAsTail("Very Slow", nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Slow",      nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Normal",    nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Fast",      nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Sanic",     nOptionPack, &DontDeleteThis);
    nOptionPack->mRangeLo = 0.0f;
    nOptionPack->mRangeHi = 4.0f;
    nOptionPack->SetDescription("Combat Dialogue Text Speed[BR]How quickly the in-combat dialogue text scrolls. Normal speed is about 1 letter every 1/15th of a second.", CarnImages.rFont_Mainline, cDescWid);
    nGameplayList->AddElementAsTail("Combat Dialogue Speed", nOptionPack, &CarnMenuOptionPack::DeleteThis);

    //--Combat Pause Time.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Combat Pause Time");
    nOptionPack->mEditType = CMOP_INTRANGE;
    nOptionPack->mValueStrings->AddElementAsTail("Click To Advance", nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Long",             nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Normal",           nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Fast",             nOptionPack, &DontDeleteThis);
    nOptionPack->mValueStrings->AddElementAsTail("Basically None",   nOptionPack, &DontDeleteThis);
    nOptionPack->mRangeLo = 0.0f;
    nOptionPack->mRangeHi = 4.0f;
    nOptionPack->SetDescription("Combat Pause Time[BR]After certain actions in combat, such as attacks, the dialogue will pause a short time before continuing to the next action. This option specifies how long it pauses to let you read.[BR]Normal is about 1/4 of a second.", CarnImages.rFont_Mainline, cDescWid);
    nGameplayList->AddElementAsTail("Combat Pause Time", nOptionPack, &CarnMenuOptionPack::DeleteThis);

    //--Voice Ticks During Acceleration.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Disable Voice During Accel");
    nOptionPack->mEditType = CMOP_BOOLEAN;
    nOptionPack->SetDescription("Disable Voice During Accel[BR]A text sound is played when letters appear during dialogue. If you hold Z+X or mouselft to speed the dialogue up, it sometimes sounds odd. You can use this option to disable text ticks when accelerating dialogue.", CarnImages.rFont_Mainline, cDescWid);
    nGameplayList->AddElementAsTail("Disable Voice During Accel", nOptionPack, &CarnMenuOptionPack::DeleteThis);

    ///--[Audio Options]
    //--Audio Options.
    StarLinkedList *nAudioList = new StarLinkedList(true);
    mMasterOptionList->AddElementAsTail("Audio", nAudioList, &StarLinkedList::DeleteThis);

    //--Music Volume.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Music Volume");
    nOptionPack->mEditType = CMOP_BAR;
    nOptionPack->mRangeLo = 0.0f;
    nOptionPack->mRangeHi = 1.0f;
    nOptionPack->SetDescription("Music Volume[BR]Volume of the in-game music. 1.0 is full volume.", CarnImages.rFont_Mainline, cDescWid);
    nAudioList->AddElementAsTail("Music Volume", nOptionPack, &CarnMenuOptionPack::DeleteThis);

    //--Sound Volume.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Sound Volume");
    nOptionPack->mEditType = CMOP_BAR;
    nOptionPack->mRangeLo = 0.0f;
    nOptionPack->mRangeHi = 1.0f;
    nOptionPack->SetDescription("Sound Volume[BR]Volume of the in-game sound effects. 1.0 is full volume.", CarnImages.rFont_Mainline, cDescWid);
    nAudioList->AddElementAsTail("Sound Volume", nOptionPack, &CarnMenuOptionPack::DeleteThis);

    ///--[Video Options]
    //--Video Options.
    StarLinkedList *nVideoList = new StarLinkedList(true);
    mMasterOptionList->AddElementAsTail("Video", nVideoList, &StarLinkedList::DeleteThis);

    //--Fullscreen Toggle.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Fullscreen");
    nOptionPack->mEditType = CMOP_BOOLEAN;
    nOptionPack->SetDescription("Fullscreen[BR]If true, the game will run in fullscreen mode.", CarnImages.rFont_Mainline, cDescWid);
    nVideoList->AddElementAsTail("Fullscreen", nOptionPack, &CarnMenuOptionPack::DeleteThis);

    //--Resolution. Variable, based on what the driver provides.
    nOptionPack = (CarnMenuOptionPack *)starmemoryalloc(sizeof(CarnMenuOptionPack));
    nOptionPack->Initialize();
    nOptionPack->SetName("Resolution");
    nOptionPack->mEditType = CMOP_INTRANGE;
    nOptionPack->mRangeLo = 0.0f;
    nOptionPack->mRangeHi = (float)(mResolutionsTotal - 1);
    nOptionPack->SetDescription("Resolution.[BR]Size of the game window, in pixels. The game uses an internal canvas system optimized for 1366x768. Requires a program restart to take effect.", CarnImages.rFont_Mainline, cDescWid);
    nVideoList->AddElementAsTail("Resolution", nOptionPack, &CarnMenuOptionPack::DeleteThis);
    for(int i = 0; i < mResolutionsTotal; i ++)
    {
        nOptionPack->mValueStrings->AddElementAsTail(mResolutionStrings[i],   nOptionPack, &DontDeleteThis);
    }

    ///--[Call Subroutine]
    //--After construction, set all options to their current values.
    RefreshOptions();
}
void CarnUIOptions::RefreshOptions()
{
    ///--[Documentation]
    //--Sets the current values of all options into their GUI counterparts. This also sets the "old" value
    //  which is the one used when the user clicks the defaults button.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--Get and check the sublists.
    StarLinkedList *rGameplayList = (StarLinkedList *)mMasterOptionList->GetElementByName("Gameplay");
    StarLinkedList *rVideoList    = (StarLinkedList *)mMasterOptionList->GetElementByName("Video");
    StarLinkedList *rAudioList    = (StarLinkedList *)mMasterOptionList->GetElementByName("Audio");
    if(!rGameplayList || !rVideoList || !rAudioList)
    {
        fprintf(stderr, "CarnUIOptions:RefreshOptions() - Warning. Unable to find a sublist. Pointers: %p %p %p\n", rGameplayList, rVideoList, rAudioList);
        return;
    }

    ///--[Gameplay Options]
    //--Difficulty.
    SysVar *rDifficultyVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Carnation/System/sDifficulty");
    CarnMenuOptionPack *rDifficultyPack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Difficulty");
    if(rDifficultyVar && rDifficultyPack)
    {
        //--Current value.
        if(!strcasecmp(rDifficultyVar->mAlpha, "Easy"))
            rDifficultyPack->mCurValue.i = 0;
        else if(!strcasecmp(rDifficultyVar->mAlpha, "Normal"))
            rDifficultyPack->mCurValue.i = 1;
        else
            rDifficultyPack->mCurValue.i = 2;

        //--Default difficulty is 1, or "Normal".
        rDifficultyPack->mOldValue.i = 1;
    }

    //--Dialogue Text Speed.
    CarnMenuOptionPack *rDialogueSpeedPack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Dialogue Speed");
    if(rDialogueSpeedPack)
    {
        rDialogueSpeedPack->mCurValue.i = rOptionsManager->GetOptionI("Dialogue Speed");
        rDialogueSpeedPack->mOldValue.i = 2;
    }

    //--Combat Dialogue Text Speed.
    CarnMenuOptionPack *rCombatDialogueSpeedPack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Combat Dialogue Speed");
    if(rCombatDialogueSpeedPack)
    {
        rCombatDialogueSpeedPack->mCurValue.i = rOptionsManager->GetOptionI("Combat Dialogue Speed");
        rCombatDialogueSpeedPack->mOldValue.i = 2;
    }

    //--Combat Pause Time.
    CarnMenuOptionPack *rCombatPauseTimePack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Combat Pause Time");
    if(rCombatPauseTimePack)
    {
        rCombatPauseTimePack->mCurValue.i = rOptionsManager->GetOptionI("Combat Pause Time");
        rCombatPauseTimePack->mOldValue.i = 2;
    }

    //--Voice Ticks During Acceleration.
    CarnMenuOptionPack *rDisableVoicePack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Disable Voice During Accel");
    if(rDisableVoicePack)
    {
        rDisableVoicePack->mCurValue.b = rOptionsManager->GetOptionB("Disable Voice During Accel");
        rDisableVoicePack->mOldValue.b = false;
    }

    ///--[Video Options]
    //--Toggle fullscreen.
    CarnMenuOptionPack *rFullscreenPack = (CarnMenuOptionPack *)rVideoList->GetElementByName("Fullscreen");
    if(rFullscreenPack)
    {
        rFullscreenPack->mCurValue.b = DisplayManager::Fetch()->IsFullscreen();
        rFullscreenPack->mOldValue.b = false;
    }

    //--Figure out the current resolution and find the pack that represents it.
    CarnMenuOptionPack *rResolutionPack = (CarnMenuOptionPack *)rVideoList->GetElementByName("Resolution");
    if(rResolutionPack)
    {
        //--Get the active display mode and store that as the current.
        int tResolutionVal = 0;
        DisplayManager::Fetch()->GetActiveDisplayMode(tResolutionVal);
        rResolutionPack->mCurValue.i = tResolutionVal;

        //--The default value is 1366x768. Locate that in the display list and set it.
        bool tFound = false;
        for(int i = 0; i < mResolutionsTotal; i ++)
        {
            if(!strcasecmp(mResolutionStrings[i], "1366 x 768 : 60"))
            {
                tFound = true;
                rResolutionPack->mOldValue.i = i;
                break;
            }
        }

        //--If the default was somehow not found, set it to the current.
        if(!tFound)
        {
            rResolutionPack->mOldValue.i = tResolutionVal;
        }
    }

    ///--[Audio Options]
    //--Music volume.
    CarnMenuOptionPack *rMusicVolumePack = (CarnMenuOptionPack *)rAudioList->GetElementByName("Music Volume");
    if(rMusicVolumePack)
    {
        rMusicVolumePack->mCurValue.f = AudioManager::xMusicVolume;
        rMusicVolumePack->mOldValue.f = 0.50f;
    }

    //--Sound volume.
    CarnMenuOptionPack *rSoundVolumePack = (CarnMenuOptionPack *)rAudioList->GetElementByName("Sound Volume");
    if(rSoundVolumePack)
    {
        rSoundVolumePack->mCurValue.f = AudioManager::xSoundVolume;
        rSoundVolumePack->mOldValue.f = 0.75f;
    }
}
void CarnUIOptions::SaveOptions()
{
    ///--[Documentation]
    //--Saves option values out.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--Get and check the sublists.
    StarLinkedList *rGameplayList = (StarLinkedList *)mMasterOptionList->GetElementByName("Gameplay");
    StarLinkedList *rVideoList    = (StarLinkedList *)mMasterOptionList->GetElementByName("Video");
    StarLinkedList *rAudioList    = (StarLinkedList *)mMasterOptionList->GetElementByName("Audio");
    if(!rGameplayList || !rVideoList || !rAudioList)
    {
        fprintf(stderr, "CarnUIOptions:SaveOptions() - Warning. Unable to find a sublist. Pointers: %p %p %p\n", rGameplayList, rVideoList, rAudioList);
        return;
    }

    ///--[Gameplay Options]
    //--Difficulty.
    SysVar *rDifficultyVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Carnation/System/sDifficulty");
    CarnMenuOptionPack *rDifficultyPack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Difficulty");
    if(rDifficultyVar && rDifficultyPack)
    {
        //--Current value.
        if(rDifficultyPack->mCurValue.i == 0)
            ResetString(rDifficultyVar->mAlpha, "Easy");
        else if(rDifficultyPack->mCurValue.i == 1)
            ResetString(rDifficultyVar->mAlpha, "Normal");
        else
            ResetString(rDifficultyVar->mAlpha, "Hard");
    }

    //--Dialogue Text Speed.
    CarnMenuOptionPack *rDialogueSpeedPack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Dialogue Speed");
    if(rDialogueSpeedPack)
    {
        rOptionsManager->SetOptionI("Dialogue Speed", rDialogueSpeedPack->mCurValue.i);
    }

    //--Combat Dialogue Text Speed.
    CarnMenuOptionPack *rCombatDialogueSpeedPack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Combat Dialogue Speed");
    if(rCombatDialogueSpeedPack)
    {
        rOptionsManager->SetOptionI("Combat Dialogue Speed", rCombatDialogueSpeedPack->mCurValue.i);
    }

    //--Combat Pause Time.
    CarnMenuOptionPack *rCombatPauseTimePack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Combat Pause Time");
    if(rCombatPauseTimePack)
    {
        rOptionsManager->SetOptionI("Combat Pause Time", rCombatPauseTimePack->mCurValue.i);
    }

    //--Voice Ticks During Acceleration.
    CarnMenuOptionPack *rDisableVoicePack = (CarnMenuOptionPack *)rGameplayList->GetElementByName("Disable Voice During Accel");
    if(rDisableVoicePack)
    {
        rOptionsManager->SetOptionB("Disable Voice During Accel", rDisableVoicePack->mCurValue.b);
    }

    ///--[Video Options]
    //--Toggle fullscreen.
    OptionPack *rFullscreenOption = rOptionsManager->GetOption("Fullscreen");
    CarnMenuOptionPack *rFullscreenPack = (CarnMenuOptionPack *)rVideoList->GetElementByName("Fullscreen");
    if(rFullscreenOption && rFullscreenPack)
    {
        if(DisplayManager::Fetch()->IsFullscreen() != rFullscreenPack->mCurValue.b)
        {
            rFullscreenOption->mWasChanged = true;
            DisplayManager::Fetch()->FlipFullscreen();
        }
    }

    //--Resolution is set by resolving values from the DisplayManager.
    CarnMenuOptionPack *rResolutionPack = (CarnMenuOptionPack *)rVideoList->GetElementByName("Resolution");
    if(rResolutionPack)
    {
        //--Setup.
        DisplayManager *rDisplayManager = DisplayManager::Fetch();
        int tSelectedMode = rResolutionPack->mCurValue.i;
        int tTotalDisplayModes = rDisplayManager->GetTotalDisplayModes();
        DisplayInfo tDisplayInfo = rDisplayManager->GetDisplayMode(tSelectedMode);

        //--If within range, set.
        if(tSelectedMode >= 0 && tSelectedMode < tTotalDisplayModes)
        {
            rOptionsManager->SetOptionI("WinSizeX", tDisplayInfo.mWidth);
            rOptionsManager->SetOptionI("WinSizeY", tDisplayInfo.mHeight);
        }
    }

    ///--[Audio Options]
    //--Music volume.
    CarnMenuOptionPack *rMusicVolumePack = (CarnMenuOptionPack *)rAudioList->GetElementByName("Music Volume");
    if(rMusicVolumePack)
    {
        if(AudioManager::xMusicVolume != rMusicVolumePack->mCurValue.f)
        {
            AudioManager::Fetch()->ChangeMusicVolumeTo(rMusicVolumePack->mCurValue.f);
        }
    }

    //--Sound volume.
    CarnMenuOptionPack *rSoundVolumePack = (CarnMenuOptionPack *)rAudioList->GetElementByName("Sound Volume");
    if(rSoundVolumePack)
    {
        if(AudioManager::xSoundVolume != rSoundVolumePack->mCurValue.f)
        {
            AudioManager::Fetch()->ChangeSoundVolumeTo(rSoundVolumePack->mCurValue.f);
        }
    }

    ///--[Write Savefile]
    //--Write the engine config to save the results.
    rOptionsManager->WriteConfigFiles();
}
void CarnUIOptions::DefaultOptions()
{
    ///--[Documentation]
    //--Immediately sets all options to their "old" values, which are their defaults. This does not exit
    //  the UI and does not save anything.
    StarLinkedList *rOptionList = (StarLinkedList *)mMasterOptionList->PushIterator();
    while(rOptionList)
    {
        CarnMenuOptionPack *rPackage = (CarnMenuOptionPack *)rOptionList->PushIterator();
        while(rPackage)
        {
            rPackage->mCurValue.f = rPackage->mOldValue.f;
            rPackage = (CarnMenuOptionPack *)rOptionList->AutoIterate();
        }

        //--Next.
        rOptionList = (StarLinkedList *)mMasterOptionList->AutoIterate();
    }
}
