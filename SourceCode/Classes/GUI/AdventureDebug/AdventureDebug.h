///====================================== AdventureDebug ==========================================
//--Debug menu, available in Adventure Mode by pushing the I key. Allows changing of settings,
//  including instantly-defeating enemies, restoring the party, granting items or money, etc.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLinkedList.h"

///===================================== Local Structures =========================================
typedef struct WarpListing
{
    //--Members.
    char *mWarpTarget;
    StarLinkedList *mSubList; //List of WarpListings.

    //--Methods.
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        WarpListing *rListing = (WarpListing *)pPtr;
        free(rListing->mWarpTarget);
        delete rListing->mSubList;
        free(rListing);
    }
}WarpListing;
typedef struct
{
    int mCursor;
    StarLinkedList *rActiveList;
}CursorStackEntry;

///===================================== Local Definitions ========================================
///--[Main Menu Codes]
//--This is the menu that appears when you first press the I key.
#define ADM_MODE_MAIN 0
#define ADM_MODE_WARP 1
#define ADM_MODE_SCENES 2
#define ADM_MODE_SCRIPTVARS 3
#define ADM_MODE_ENEMIES 4
#define ADM_MODE_PARTY 5
#define ADM_MODE_FORMS 6
#define ADM_MODE_TOPICS 7
#define ADM_MODE_SCRIPTHUNTER 8
#define ADM_MODE_STATPROFILES 9
#define ADM_MODE_BUILDS 10
#define ADM_MODE_TRANSLATION 11
#define ADM_MODE_TOTAL 12

#define ADM_OPT_MAIN_OPEN_WARP_SUBMENU 0
#define ADM_OPT_MAIN_OPEN_SCENES_SUBMENU 1
#define ADM_OPT_MAIN_OPEN_SCRIPTVARS_SUBMENU 2
#define ADM_OPT_MAIN_OPEN_ENEMIES_SUBMENU 3
#define ADM_OPT_MAIN_OPEN_PARTY_SUBMENU 4
#define ADM_OPT_MAIN_OPEN_FORM_SUBMENU 5
#define ADM_OPT_MAIN_OPEN_TOPIC_SUBMENU 6
#define ADM_OPT_MAIN_CLEAR_OVERLAY 7
#define ADM_OPT_MAIN_OPEN_SCRIPT_HUNTER 8
#define ADM_OPT_MAIN_OPEN_PROFILES 9
#define ADM_OPT_MAIN_OPEN_BUILDS 10
#define ADM_OPT_MAIN_RUN_PNG_PAL 11
#define ADM_OPT_MAIN_BUILD_TREASURES 12
#define ADM_OPT_MAIN_OPEN_TRANSLATION 13
#define ADM_OPT_MAIN_CLOSE_THIS_MENU 14
#define ADM_OPT_MAIN_TOTAL 15

#define ADM_OPT_ENEMIES_WIPE_FIELD 0
#define ADM_OPT_ENEMIES_RESPAWN_ALL 1
#define ADM_OPT_ENEMIES_CAMOUFLAGE 2
#define ADM_OPT_ENEMIES_ALWAYS_PARAGONS 3
#define ADM_OPT_ENEMIES_PARAGONS_SUCK 4
#define ADM_OPT_ENEMIES_ALWAYS_SHOW_STATS 5
#define ADM_OPT_ENEMIES_BACK 6
#define ADM_OPT_ENEMIES_TOTAL 7

#define ADM_OPT_PARTY_FULL_RESTORE 0
#define ADM_OPT_PARTY_GIVE_EXPA 1
#define ADM_OPT_PARTY_GIVE_EXPB 2
#define ADM_OPT_PARTY_GIVE_EXPC 3
#define ADM_OPT_PARTY_GIVE_CASH 4
#define ADM_OPT_PARTY_GIVE_CRAFTING 5
#define ADM_OPT_PARTY_GIVE_JP 6
#define ADM_OPT_PARTY_LOSE_EXPA 7
#define ADM_OPT_PARTY_LOSE_EXPB 8
#define ADM_OPT_PARTY_LOSE_EXPC 9
#define ADM_OPT_PARTY_BACK 10
#define ADM_OPT_PARTY_TOTAL 11

//--Builds Menu
#define ADM_BUILD_TOGGLE_EMULATION 0
#define ADM_BUILD_TOGGLE_FILE 1
#define ADM_BUILD_WRITE_REMOVE_FILE 2
#define ADM_BUILD_ZERO_BUILD 3

//--Translation Menu
#define ADM_BUILD_TRANSLATE_BUILD_MONOCEROS 0
#define ADM_BUILD_TRANSLATE_BUILD_ITEMS_MONOCEROSEXTERNAL 1
#define ADM_BUILD_TRANSLATE_BUILD_DIALOGUE_MONOCEROSEXTERNAL 2
#define ADM_BUILD_TRANSLATE_REPAIR_DIALOGUE_MONOCEROSEXTERNAL 3
#define ADM_BUILD_TRANSLATE_CANCEL 4
#define ADM_BUILD_TRANSLATE_MAX 5

//--Build File Options
#define ADM_BUILD_FILE_NONE 0
#define ADM_BUILD_FILE_STDERR 1
#define ADM_BUILD_FILE_DRIVE 2
#define ADM_BUILD_FILE_TOTAL 3

///--[Stat Profile Structure]
#define ADM_CHAR_MAX_LEVELS 100
#define ADM_CHAR_MAX_LETTERS 64
typedef struct CharStatProfile
{
    char mName[ADM_CHAR_MAX_LETTERS];
    char mScript[STD_PATH_LEN];
    int mMaxHP[ADM_CHAR_MAX_LEVELS];
    int mAttack[ADM_CHAR_MAX_LEVELS];
    int mAccuracy[ADM_CHAR_MAX_LEVELS];
    int mEvade[ADM_CHAR_MAX_LEVELS];
    int mInitiative[ADM_CHAR_MAX_LEVELS];
}CharStatProfile;

///--[Stat Profile Groupings]
#define ADM_PROFILE_GROUP_HPMAX 0
#define ADM_PROFILE_GROUP_ATTACK 1
#define ADM_PROFILE_GROUP_ACCURACY 2
#define ADM_PROFILE_GROUP_EVADE 3
#define ADM_PROFILE_GROUP_INITIATIVE 4
#define ADM_PROFILE_GROUP_TOTAL 5

#define ADM_PROFILE_RANGE_VL 0
#define ADM_PROFILE_RANGE_LO 1
#define ADM_PROFILE_RANGE_FL 2
#define ADM_PROFILE_RANGE_BA 3
#define ADM_PROFILE_RANGE_AV 4
#define ADM_PROFILE_RANGE_AA 5
#define ADM_PROFILE_RANGE_FH 6
#define ADM_PROFILE_RANGE_HI 7
#define ADM_PROFILE_RANGE_VH 8
#define ADM_PROFILE_RANGE_TOTAL 9
typedef struct StatProfileGroup
{
    char mString[12];
    int mLowerBound;
    int mUpperBound;
    void SetBounds(int pLo, int pHi)
    {
        mLowerBound = pLo;
        mUpperBound = pHi;
    }
}StatProfileGroup;

///--[Translation Flags]
#define TRANSLATE_RUN_DIALOGUE 0
#define TRANSLATE_RUN_ITEMSSKILLS 1

///========================================== Classes =============================================
class AdventureDebug
{
    private:
    //--System
    bool mIsVisible;

    //--Menu Cursor
    int mMenuMode;
    int mMenuCursor;
    int mMenuMax;

    //--Script Variables Listing
    int mScriptVarsTotal;
    int mScriptVarOffset;
    char **mScriptVarNames;
    char **mScriptVarValues;

    //--Topic Listing
    int mTopicsTotal;
    int mTopicOffset;
    const char **mrTopicNames;
    int *mTopicValues;

    //--Builds Listing
    bool mIsEmulation;
    int mBuildFile;
    int mGameCursor;

    //--Stat Profiler
    int mStatProfilesTotal;
    CharStatProfile *mStatProfiles;
    int mActiveProfile;
    int mGraphMode;
    int mProfileLevel;

    //--Stat Profile Groups
    StatProfileGroup mStatProfileGroups[ADM_PROFILE_GROUP_TOTAL][ADM_PROFILE_RANGE_TOTAL];

    //--Translation
    FILE *fTranslationOutfile;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            StarFont *rMenuFont;
        }Data;
    }Images;

    ///--[Private Statics]
    //--Warp Listing
    static int xWarpDestinationsTotal;
    static char **xWarpDestinations;
    StarLinkedList *mWarpListing; //List of WarpListing's.
    StarLinkedList *rActiveListing;
    StarLinkedList *mCursorStack; //List of CursorStackEntry's.

    //--Scenes Listing
    static int xScenesTotal;
    static char **xSceneListing;

    //--Forms Listing
    static int xFormsTotal;
    static char **xFormListing;

    protected:

    public:
    //--System
    AdventureDebug();
    ~AdventureDebug();

    //--Public Variables
    static bool xManualActivation;
    static bool xCamouflageMode;
    static StarLinkedList *xmScriptHunterPathsList; //char *
    static char *xStatProfilePath;
    static bool xAlwaysShowEnemyStats;
    static char *xFormBuilderPath;
    static bool xParagonsSuck;

    //--Property Queries
    bool IsVisible();

    //--Manipulators
    void Show();
    void Hide();
    void SetVisibility(bool pFlag);
    void SetToMain();
    void SetToWarp();
    void SetToScenes();
    void SetToScriptVars();
    void SetToEnemies();
    void SetToParty();
    void SetToForms();
    void SetToTopics();
    void SetToScriptHunter();
    void SetToStatProfiles();
    void SetToBuilds();
    void SetToTranslate();

    //--Core Methods
    void AllocateScriptVariables(int pAmount);
    void RebuildVariableListing();
    void BuildTopicListing();
    void BuildWarpListing();
    void BuildPNGFromFolder(const char *pPath);

    ///--[Builds]
    void OutputPurgeFile(const char *pFilePath);
    void UpdateBuilds();

    ///--[Translation]
    void BuildTranslationFileOnPath(const char *pPath, int pFlag, const char *pOutputPath);
    void BuildDialogueTranslationForFile(const char *pPath);
    void BuildItemSkillTranslationForFile(const char *pPath);
    void HandleItemSkillTranslationLine(const char *pString, int pLeadChars);
    void HandleItemSkillDescriptionLine(const char *pString, int pLeadChars);
    static int GetIndexOfLineStart(const char *pString, int pLeadChars);
    void HandleDialogueTranslationLine(const char *pString, int pLeadChars);
    void RunRepairOnDialogueSpreadsheet(const char *pPath, const char *pOutputPath);
    void RunRepairOnLine(const char *pLine, FILE *fOutfile);

    //--Profiles
    void AllocateProfiles(int pTotal);
    void CreateProfile(int pSlot, const char *pName, const char *pScript);
    void SetProfileData(int pSlot, int pLevel, int pMaxHP, int pAttack, int pAccuracy, int pEvade, int pInitiative);
    void UpdateStatProfiles();
    void UpdateStatProfilesSelector();
    void UpdateStatProfilesGraphs();
    void RenderStatProfiles();
    void RenderStatProfilesSelector();
    void RenderStatProfilesGraphs();
    void RenderStatGraph(float pX, float pY, int *pStatArray, int pRange, StarlightColor pColor, StatProfileGroup *pGroupRanges);

    private:
    //--Private Core Methods
    public:
    //--Update
    void StandardCursorUpdate();
    void Update();
    void UpdateMain();
    void UpdateWarp();
    void UpdateScenes();
    void UpdateScriptVars();
    void UpdateEnemies();
    void UpdateParty();
    void UpdateForms();
    void UpdateTopics();
    void UpdateScriptHunter();
    void UpdateTranslate();

    //--File I/O
    //--Drawing
    static void RenderOverlay();
    void Render();
    void RenderText(int pCursorValue, float pX, float pY, float pScale, const char *pText);

    //--Pointer Routing
    //--Static Functions
    static void AllocateWarpDestinations(int pTotal);
    static void SetWarpDestination(int pSlot, const char *pPath);
    static void AllocateSceneListings(int pTotal);
    static void SetSceneListing(int pSlot, const char *pPath);
    static void AllocateFormHandlers(int pTotal);
    static void SetFormHandler(int pSlot, const char *pPath);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_ADebug_GetProperty(lua_State *L);
int Hook_ADebug_SetProperty(lua_State *L);
