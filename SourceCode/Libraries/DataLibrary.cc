//--Base
#include "DataLibrary.h"

//--Classes
#include "RootEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "StarPointerSeries.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
bool DataLibrary::xBlockSpecialDeletion = false;
DataLibrary::DataLibrary()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ====== DataLibrary ======= ]
    //--System
    mDebugMode = false;
    mErrorMode = true;

    //--Storage
    mSectionList = new StarLinkedList(true);
    mActiveEntityStack = new StarLinkedList(false);

    //--Counters
    mRunningIndexCounter = 1;

    //--Dummies
    mDummyPtr = new RootObject();

    //--Font Storage
    mMasterFontList = new StarLinkedList(true);
    mFontKerningPathList = new StarLinkedList(true);
    mrFontRefList = new StarLinkedList(false);

    //--Path Pool Storage
    mPathPool = new StarLinkedList(false);

    //--Unload Storage
    mUnloadList = new StarLinkedList(true);

    //--Pointer Series Listing
    mPointerSeriesList = new StarLinkedList(true);

    //--Public Variables
    mFailSilently = false;
    mSuppressFontErrors = false;
    rCurrentSection = NULL;
    rCurrentCatalogue = NULL;
    rCurrentHeading = NULL;
    rActiveObject = NULL;
    mErrorCode = 0;
}
DataLibrary::~DataLibrary()
{
    delete mSectionList;
    delete mActiveEntityStack;
    DataLibrary::xBlockSpecialDeletion = true;
    delete mDummyPtr;
    delete mMasterFontList;
    delete mFontKerningPathList;
    delete mrFontRefList;
    delete mPathPool;
    delete mUnloadList;
    delete mPointerSeriesList;
}
void DataLibrary::DeleteSysVar(void *pPtr)
{
    SysVar *rPtr = (SysVar *)pPtr;
    free(rPtr->mAlpha);
    free(rPtr);
}

///--[Public Statics]
//--When set to true, and the debug printer is on, prints GetEntry() calls as they happen and the address
//  of the result. This allows a debugger to find out which calls are failing. The debug define must be
//  turned on, otherwise it will unnecessarily slow data access.
//--Flip the flag to true over top of whatever section you want to analyze, and set it to false later.
#define GETENTRYDEBUG
bool DataLibrary::xGetEntryDebug = false;

//--For diagnostics, stores what the total uploaded pixels value was when diagnostics printed last.
int DataLibrary::xLastPixelCount = 0;

///===================================== Property Queries =========================================
char *DataLibrary::GetPathOfEntry(void *pPtr)
{
    //--Returns a heap-allocated string containing the path of the entry in question. If not found,
    //  returns a string with "NULL" in it.
    //--Caller is responsible for deallocating the string.
    if(!pPtr)
    {
        char *nNullString = InitializeString("Null");
        return nNullString;
    }

    //--Iterate across all the sections.
    StarLinkedList *rSection = (StarLinkedList *)mSectionList->PushIterator();
    while(rSection)
    {
        //--Iterate across the catalogues.
        StarLinkedList *rCatalogue = (StarLinkedList *)rSection->PushIterator();
        while(rCatalogue)
        {
            //--Iterate across the headings.
            StarLinkedList *rHeading = (StarLinkedList *)rCatalogue->PushIterator();
            while(rHeading)
            {
                //--Iterate across the entries.
                void *rCheckPtr = rHeading->PushIterator();
                while(rCheckPtr)
                {
                    //--Match.
                    if(rCheckPtr == pPtr)
                    {
                        //--Get names for each part.
                        const char *rSectionName   = mSectionList->GetIteratorName();
                        const char *rCatalogueName = rSection->GetIteratorName();
                        const char *rHeadingName   = rCatalogue->GetIteratorName();
                        const char *rEntryName     = rHeading->GetIteratorName();

                        //--Assemble.
                        char *nString = InitializeString("Root/%s/%s/%s/%s", rSectionName, rCatalogueName, rHeadingName, rEntryName);

                        //--Clean up all iterators.
                        rHeading->PopIterator();
                        rCatalogue->PopIterator();
                        rSection->PopIterator();
                        mSectionList->PopIterator();

                        //--Return the string.
                        return nString;
                    }

                    //--Next.
                    rCheckPtr = rHeading->AutoIterate();
                }

                //--Next.
                rHeading = (StarLinkedList *)rCatalogue->AutoIterate();
            }

            //--Next.
            rCatalogue = (StarLinkedList *)rSection->AutoIterate();
        }

        //--Next.
        rSection = (StarLinkedList *)mSectionList->AutoIterate();
    }

    //--Checked the whole library, found no matches.
    char *nNullString = InitializeString("Null");
    return nNullString;
}
StarLinkedList *DataLibrary::GetSection(const char *pResolveString)
{
    //--Finds the Section specified. Must be of format Root/Section/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    StarLinkedList *rSection = tResolvePack->rSection;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetSection(N)", pResolveString);
    return rSection;
}
StarLinkedList *DataLibrary::GetCatalogue(const char *pResolveString)
{
    //--Finds the Catalogue specified. Must be of format Root/Section/Catalogue/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    StarLinkedList *rCatalogue = tResolvePack->rCatalogue;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetCatalogue(N)", pResolveString);
    return rCatalogue;
}
StarLinkedList *DataLibrary::GetHeading(const char *pResolveString)
{
    //--Finds the Heading specified. Must be of format Root/Section/Catalogue/Heading/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    StarLinkedList *rHeading = tResolvePack->rHeading;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetHeading(N)", pResolveString);
    return rHeading;
}
void *DataLibrary::GetEntry(const char *pResolveString)
{
    //--Finds the Entry specified.  Must be of format Root/Section/Catalogue/Heading/Name
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    void *rEntry = tResolvePack->rEntry;
    free(tResolvePack);

    //--[Debug]
    #ifdef GETENTRYDEBUG
    if(xGetEntryDebug)
    {
        fprintf(stderr, "%s %p\n", pResolveString, rEntry);
        if(!rEntry) fprintf(stderr, "==> Null <==\n");
    }

    #endif

    if(!tResolvePack) ErrorNotFound("GetEntry(N)", pResolveString);
    return rEntry;
}
bool DataLibrary::DoesEntryExist(const char *pResolveString)
{
    //--Returns true if the entry exists, false if it does not.
    return (GetEntry(pResolveString) != NULL);
}
bool DataLibrary::IsActiveValid()
{
    //--Returns true if the rActiveObject is not NULL.
    return (rActiveObject != NULL);
}
bool DataLibrary::IsActiveValid(int pTypeFlag)
{
    //--Returns true if the rActiveObject is not NULL, AND has the matching type flag set.
    //  Otherwise, false.
    if(!rActiveObject) return false;
    RootObject *rRootObject = (RootObject *)rActiveObject;
    return (rRootObject->IsOfType(pTypeFlag));
}
bool DataLibrary::IsActiveValid(int pTypeStart, int pTypeEnd)
{
    //--Returns true if the rActiveObject is not NULL, AND has its type flag between the start and
    //  end flags passed.  Otherwise, false.
    if(!rActiveObject) return false;
    RootObject *rRootObject = (RootObject *)rActiveObject;
    return (rRootObject->GetType() >= pTypeStart && rRootObject->GetType() <= pTypeEnd);
}
int DataLibrary::GetActiveObjectType()
{
    //--Returns the type of the active object. If the object is NOT of the RootObject inheritance
    //  structure, the results will be undefined.
    //--If this object is NULL, POINTER_TYPE_FAIL will be returned.
    if(!rActiveObject) return POINTER_TYPE_FAIL;

    //--This is where the crash is going to occur if the object wasn't in the heirarchy.
    RootObject *rObj = (RootObject *)rActiveObject;
    return rObj->GetType();
}
int DataLibrary::GetSizeOfActivityStack()
{
    return mActiveEntityStack->GetListSize();
}
int DataLibrary::GetSizeOfList(const char *pResolveString)
{
    ///--[Documentation]
    //--Given a standardized path, resolves the list in question and returns how many elements are in it.
    //  This does not return how many subdivisions are in a Section, but rather, how many entries are in
    //  every catalogue and heading of that section. Because this is linked-list parsing it might take
    //  a non-trivial amount of time for anything other than a catalogue.
    //--Passing "Root/" gives the size of the entire DataLibrary.
    int tLevel = 0;
    StarLinkedList *rBaseList = NULL;

    //--Special, Root always does the entire library.
    if(!strcasecmp(pResolveString, "Root") || !strcasecmp(pResolveString, "Root/"))
    {
        tLevel = 3;
        rBaseList = mSectionList;
    }
    //--Otherwise, create a listing.
    else
    {
        //--This package will contain pointers to the subdivisions.
        ResolvePack *tResolvePack = CreateResolvePack(pResolveString);

        //--A heading pointer is available:
        if(tResolvePack->rHeading)
        {
            tLevel = 0;
            rBaseList = tResolvePack->rHeading;
        }
        //--A catalogue pointer is available:
        else if(tResolvePack->rCatalogue)
        {
            tLevel = 1;
            rBaseList = tResolvePack->rCatalogue;
        }
        //--A section pointer is available:
        else if(tResolvePack->rSection)
        {
            tLevel = 2;
            rBaseList = tResolvePack->rSection;
        }

        //--Clean.
        free(tResolvePack);
    }

    ///--[Call Recursion]
    //--From here, return the value returned by the recursive call. It will handle the root case or a missing
    //  pointer cases.
    return CountElementsInList(rBaseList, tLevel);
}
int DataLibrary::CountElementsInList(StarLinkedList *pList, int pLevel)
{
    ///--[Documentation]
    //--Given a linked list, counts how many elements are in it as though it is a nest of linked lists.
    //  The provided pLevel indicates how deep we are in the next. If it's zero, then return how many
    //  elements are on the provided list.
    //--If the level is not zero, then assume the list is entirely made of sublists, and recursively
    //  operate on each of those.
    if(!pList) return 0;

    //--Zero-level, just return the list's size.
    if(pLevel < 1) return pList->GetListSize();

    //--Otherwise, iterate across the list.
    int tSum = 0;
    StarLinkedList *rSublist = (StarLinkedList *)pList->PushIterator();
    while(rSublist)
    {
        tSum = tSum + CountElementsInList(rSublist, pLevel - 1);
        rSublist = (StarLinkedList *)pList->AutoIterate();
    }

    //--Finish up.
    return tSum;
}
StarPointerSeries *DataLibrary::GetStarPointerSeries(const char *pName)
{
    return (StarPointerSeries *)mPointerSeriesList->GetElementByName(pName);
}

///======================================== Manipulators ==========================================
void DataLibrary::SetErrorFlag(bool pFlag)
{
    mErrorMode = pFlag;
}
void DataLibrary::SetErrorCode(int pCode)
{
    mErrorCode = pCode;
}
void DataLibrary::RegisterStarPointerSeries(const char *pName, StarPointerSeries *pEntry)
{
    if(!pName || !pEntry) return;
    mPointerSeriesList->AddElementAsTail(pName, pEntry, &StarPointerSeries::DeleteThis);
}
void DataLibrary::AddPath(const char *pDLPath)
{
    ///--[Documentation]
    //--Adds the required path to the DataLibrary. If any of the sections, catalogues, or headings
    //  specified already exist, then they are considered activated. If they don't exist they are
    //  created, and set as active.
    //--The below three functions (AddSection, AddCatalogue, AddHeading) were built to be called
    //  through this function.

    ///--[Resolve]
    //--Use a resolve pack to split the path into individual elements. Set those as the "current"
    //  lists to speed up processing.
    ResolvePack *tResolvePack = CreateResolvePack(pDLPath);
    rCurrentSection = tResolvePack->rSection;
    rCurrentCatalogue = tResolvePack->rCatalogue;
    rCurrentHeading = tResolvePack->rHeading;

    ///--[Creation]
    //--Create the parts. If any pieces were already active, they fail.
    AddSection(tResolvePack->mSection);
    AddCatalogue(tResolvePack->mCatalogue);
    AddHeading(tResolvePack->mHeading);

    ///--[Clean]
    free(tResolvePack);
}
void DataLibrary::RegisterPointer(const char *pDLPath, void *pPointer)
{
    //--Overload, doesn't use a deletion function.
    RegisterPointer(pDLPath, pPointer, &DontDeleteThis);
}
void DataLibrary::RegisterPointer(const char *pDLPath, void *pPointer, DeletionFunctionPtr pDeletionFunction)
{
    ///--[Documentation]
    //--Registers a pointer at the given locations set. The DataLibrary does not support the
    //  registration of NULL pointers, and will fail if that is passed.
    if(!pDLPath || !pPointer) return;

    ///--[Resolve Path, Duplication Check]
    //--Resolve the path into parts.
    ResolvePack *tResolvePack = CreateResolvePack(pDLPath);
    char *tName = DataLibrary::GetPathPart(pDLPath, DL_NAME);

    //--Fast-access pointer.
    StarLinkedList *rHeading = tResolvePack->rHeading;

    //--If something went wrong and the heading does not exist, stop here.
    if(!rHeading || !tName)
    {
        free(tResolvePack);
        free(tName);
        return;
    }

    //--If the entry already exists, disallow duplicates. Delete the incoming pointer.
    void *rCheckEntry = rHeading->GetElementByName(tName);
    if(rCheckEntry)
    {
        //--Debug.
        fprintf(stderr, "==> Warning: Attempt to register pointer to %s. Already exists. Deleting passed pointer %p.\n", pDLPath, pPointer);

        //--If this is the active object, NULL it.
        if(rActiveObject == pPointer)
        {
            rActiveObject = NULL;
        }

        //--Clean.
        free(tResolvePack);
        free(tName);
        pDeletionFunction(pPointer);
        return;
    }

    ///--[Create]
    //--Register the pointer.
    rHeading->AddElement(tName, pPointer, pDeletionFunction);

    ///--[Adding to the Path Pool]
    //--If this is one of the saveable sections, register the name of the catalogue.
    if(!strcasecmp(tResolvePack->mSection, "Variables") || !strcasecmp(tResolvePack->mSection, "Saving"))
    {
        //--Already on the list, ignore it.
        if(mPathPool->GetElementByName(tName) != NULL)
        {
            free(tResolvePack);
            free(tName);
            return;
        }

        //--Don't add if it's in the variables/combat catalogue, as these are always temporary.
        if(!strcasecmp(tResolvePack->mSection, "Variables") && !strcasecmp(tResolvePack->mCatalogue, "Combat"))
        {
            free(tResolvePack);
            free(tName);
            return;
        }

        //--Add it.
        int tDummy;
        mPathPool->AddElement(tName, &tDummy);

        //--Debug.
        //fprintf(stderr, "==> Added entry: %s/%s/%s - (%i)\n", tResolvePack->mSection, tResolvePack->mCatalogue, tResolvePack->mHeading, mPathPool->GetListSize());
    }

    ///--[Clean]
    free(tResolvePack);
    free(tName);
}
void *DataLibrary::RemovePointer(const char *pDLPath)
{
    //--Removes a pointer from its list. The pointer is returned, but if the list had called for
    //  it to be deallocated, then the pointer returned will be unstable. If not, the pointer will
    //  be valid and you are responsible for deallocating it.
    //--Returns NULL if something went wrong, or the entry wasn't found.
    if(!pDLPath) return NULL;

    //--Resolve
    StarLinkedList *rHeading = GetHeading(pDLPath);
    char *tName = DataLibrary::GetPathPart(pDLPath, DL_NAME);
    if(!rHeading || !tName) return NULL;

    //--Get the pointer itself.
    void *rElement = rHeading->RemoveElementS(tName);
    free(tName);
    return rElement;
}

///--[Activity Stack]
void DataLibrary::SetActiveObject(const char *pDLPath)
{
    //--Sets the rActiveObject based on the path passed.  If the object is not found, it is set to
    //  NULL instead.
    rActiveObject = GetEntry(pDLPath);
}
void DataLibrary::PushActiveEntity()
{
    //--Overload of below, pushes NULL.
    PushActiveEntity(NULL);
}
void DataLibrary::PushActiveEntity(void *pReplacer)
{
    //--Pushes the rActiveObject back one on the Activity Stack.  If the rActiveObject was NULL
    //  then a dummy pointer is pushed onto the stack instead, as RLL's do not support NULL entries.
    //--Passing NULL is acceptable, it will place NULL on top of the Activity Stack.
    if(!rActiveObject) rActiveObject = mDummyPtr;
    mActiveEntityStack->AddElementAsHead("X|DL_ActiveEntity", rActiveObject);
    rActiveObject = pReplacer;
}
void *DataLibrary::PopActiveEntity()
{
    //--Pops off the rActiveObject and replaces it with the 0th entry on the Activity Stack.  If
    //  the stack was empty, this can be NULL.  In addition, if the mDummyPtr is spotted, the
    //  rActiveObject also becomes NULL.
    void *rReturn = rActiveObject;
    rActiveObject = mActiveEntityStack->RemoveElementI(0);
    if(rActiveObject == mDummyPtr) rActiveObject = NULL;
    return rReturn;
}

///======================================== Core Methods ==========================================
void DataLibrary::PrintDiagnostics()
{
    ///--[Documentation and Setup]
    //--Prints some basic diagnostics. Can be triggered with a keypress.

    ///--[Variables]
    //--Setup.
    float tPercentPixelChange = 0.0f;

    //--If the last pixel count is not zero, compute the percentage change from the last time the
    //  diagnostics were printed.
    if(xLastPixelCount != 0)
    {
        tPercentPixelChange = (float)StarBitmap::xTotalPixels / (float)xLastPixelCount;
    }

    ///--[Print]
    DebugManager::ForcePrint("Tick %i: Showing bitmap statistics.\n", Global::Shared()->gTicksElapsed);
    DebugManager::ForcePrint(" Total bitmaps in existence: %i\n", StarBitmap::xTotalBitmaps);
    DebugManager::ForcePrint(" Bitmaps on loading queue: %i\n", StarLumpManager::Fetch()->GetSizeOfLoadQueue());
    DebugManager::ForcePrint(" Total pixels currently uploaded: %i\n", StarBitmap::xTotalPixels);
    if(xLastPixelCount != 0)
    {
        DebugManager::ForcePrint(" Percentage change in pixels uploaded: %5.2f%%\n", tPercentPixelChange * 100.0f);
    }
    DebugManager::ForcePrint("\n");

    ///--[Store]
    //--Store data for next time.
    xLastPixelCount = StarBitmap::xTotalPixels;
}
void DataLibrary::EnqueueBitmapUnloading(const char *pCheckPath, StarBitmap *pBitmap)
{
    ///--[Documentation]
    //--Given a bitmap and a DLPath, enqueues the bitmap to run UnloadData() when the library next
    //  dumps data, usually when a level is transitioning. The pCheckPath variable is not actually
    //  used to resolve the bitmap, it's there to make sure the bitmap didn't get deleted by something
    //  else in the meantime and is thus a dangling pointer.
    //--The DLPath "ALWAYS" will bypass this check.
    if(!pCheckPath || !pBitmap) return;

    ///--[Data Check]
    //--If the given bitmap is already unloaded, or never had data to begin with, don't bother.
    if(pBitmap->GetGLTextureHandle() == 0) return;

    ///--[Create]
    UnloadEntry *nEntry = (UnloadEntry *)starmemoryalloc(sizeof(UnloadEntry));
    strncpy(nEntry->mCheckPath, pCheckPath, STD_MAX_LETTERS-1);
    nEntry->rUnloadBitmap = pBitmap;

    ///--[Register]
    mUnloadList->AddElementAsTail(pCheckPath, nEntry, &FreeThis);

    ///--[Debug]
    //fprintf(stderr, "Enqueued %p %s to unload listing.\n", pBitmap, pCheckPath);
}
void DataLibrary::ClearUnloadQueueForBitmap(StarBitmap *pBitmap)
{
    ///--[Documentation]
    //--Orders the unload list to remove any entries that mark the given bitmap for unloading. Because
    //  bitmaps may be marked for unloading for a while, it's possible that a new load order is received
    //  before the last unload order went through, so no work is necessary.
    if(!pBitmap) return;

    //--Variables.
    //int tTotalRemoved = 0;

    ///--[Iterate]
    UnloadEntry *rEntry = (UnloadEntry *)mUnloadList->SetToHeadAndReturn();
    while(rEntry)
    {
        //--If the bitmaps match, remove.
        if(rEntry->rUnloadBitmap == pBitmap)
        {
            //tTotalRemoved ++;
            mUnloadList->RemoveRandomPointerEntry();
        }

        //--Next.
        rEntry = (UnloadEntry *)mUnloadList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Diagnostics]
    //fprintf(stderr, "Cleared %i bitmaps from unload queue.\n", tTotalRemoved);
}
void DataLibrary::UnloadPendingBitmaps()
{
    ///--[Documentation]
    //--A list of UnloadEntry's exists on mUnloadList. These are bitmaps whose data is no longer needed
    //  but may still be displaying, such as during a cutscene. They are typically dropping their data
    //  when the next level transition occurs. This function is called to handle that.

    ///--[Iterate]
    UnloadEntry *rEntry = (UnloadEntry *)mUnloadList->PushIterator();
    while(rEntry)
    {
        //--If the bitmap is somehow NULL, do nothing.
        if(!rEntry->rUnloadBitmap)
        {

        }
        //--If the entry's path is "ALWAYS", always unload the data.
        else if(!strcasecmp(rEntry->mCheckPath, "ALWAYS"))
        {
            rEntry->rUnloadBitmap->UnloadData();
        }
        //--Otherwise, check if the DLPath exists. If it does, the bitmap is presumable still valid.
        else
        {
            //--Get.
            void *rCheckEntry = GetEntry(rEntry->mCheckPath);

            //--Entry exists, unload.
            if(rCheckEntry)
            {
                rEntry->rUnloadBitmap->UnloadData();
            }
        }

        //--Next.
        rEntry = (UnloadEntry *)mUnloadList->AutoIterate();
    }

    ///--[Clear]
    //--Clear the list and deallocate the entries.
    mUnloadList->ClearList();
}

///==================================== Private Core Methods ======================================
//--Note:  These are private for a reason!
void DataLibrary::AddSection(const char *pName)
{
    //--Adds the specified section, and sets it as active.  If there is already a section active
    //  then fail.
    if(!pName || rCurrentSection) return;

    //--Create and add the section.
    StarLinkedList *nSection = new StarLinkedList(true);
    rCurrentSection = nSection;
    mSectionList->AddElement(pName, nSection, &StarLinkedList::DeleteThis);

    //--Sections get added to the string pool when they are "Variables" or "Saving". All others
    //  are ignored.
    if(!strcasecmp(pName, "Variables") || !strcasecmp(pName, "Saving"))
    {
        //--Already on the list, ignore it.
        if(mPathPool->GetElementByName(pName) != NULL) return;

        //--Add it.
        int tDummy;
        mPathPool->AddElement(pName, &tDummy);

        //--Debug.
        //fprintf(stderr, "==> Added section: %s\n", pName);
    }
}
void DataLibrary::AddCatalogue(const char *pName)
{
    //--Adds the specified catalogue to the rCurrentSection. As above, don't call this directly,
    //  call it through AddPath().
    if(!pName || !rCurrentSection || rCurrentCatalogue) return;

    //--Create and add the catalogue.
    StarLinkedList *nCatalogue = new StarLinkedList(true);
    rCurrentCatalogue = nCatalogue;
    rCurrentSection->AddElement(pName, nCatalogue, &StarLinkedList::DeleteThis);

    //--Get the slot of the active section.
    int tSlot = mSectionList->GetSlotOfElementByPtr(rCurrentSection);
    if(tSlot < 0) return;

    //--Get the name of the section.
    const char *rSectionName = mSectionList->GetNameOfElementBySlot(tSlot);
    if(!rSectionName) return;

    //--If this is one of the saveable sections, register the name of the catalogue.
    if(!strcasecmp(rSectionName, "Variables") || !strcasecmp(rSectionName, "Saving"))
    {
        //--Already on the list, ignore it.
        if(mPathPool->GetElementByName(pName) != NULL) return;

        //--If the catalogue is "Variables/Combat" then it does not get added to the string pool. This is because
        //  the combat variables are expected to be temporary.
        if(!strcasecmp(rSectionName, "Variables") && !strcasecmp(pName, "Combat")) return;

        //--Add it.
        int tDummy;
        mPathPool->AddElement(pName, &tDummy);

        //--Debug.
        //fprintf(stderr, "==> Added catalogue: %s/%s\n", rSectionName, pName);
    }
}
void DataLibrary::AddHeading(const char *pName)
{
    //--Adds the specified heading to the rCurrentCatalogue.  Call this through AddPath().
    if(!pName || !rCurrentCatalogue || rCurrentHeading) return;

    //--Create and add the heading.
    StarLinkedList *nHeading = new StarLinkedList(true);
    rCurrentHeading = nHeading;
    rCurrentCatalogue->AddElement(pName, nHeading, &StarLinkedList::DeleteThis);

    //--Get the slot of the active section.
    int tSlot = mSectionList->GetSlotOfElementByPtr(rCurrentSection);
    if(tSlot < 0) return;

    //--Get the name of the section.
    const char *rSectionName = mSectionList->GetNameOfElementBySlot(tSlot);
    if(!rSectionName) return;

    //--If this is one of the saveable sections, register the name of the catalogue.
    if(!strcasecmp(rSectionName, "Variables") || !strcasecmp(rSectionName, "Saving"))
    {
        //--Already on the list, ignore it.
        if(mPathPool->GetElementByName(pName) != NULL) return;

        //--If the catalogue is "Variables/Combat" then it does not get added to the string pool. This is because
        //  the combat variables are expected to be temporary.
        int tCatalogueSlot = rCurrentSection->GetSlotOfElementByPtr(rCurrentCatalogue);
        const char *rCatalogueName = rCurrentSection->GetNameOfElementBySlot(tCatalogueSlot);
        if(!strcasecmp(rSectionName, "Variables") && !strcasecmp(rCatalogueName, "Combat")) return;

        //--Add it.
        int tDummy;
        mPathPool->AddElement(pName, &tDummy);

        //--Debug.
        //fprintf(stderr, "==> Added heading: %s/%s/%s\n", rSectionName, rCatalogueName, pName);
    }
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void DataLibrary::PrintToFile(const char *pFilePath)
{
    //--Prints the contents of the DataLibrary's directory structure.
    if(!pFilePath) return;

    //--Open it.  Write mode, don't check overwrite.
    FILE *fOutfile = fopen(pFilePath, "w");

    //--Sections
    StarLinkedList *rSection = (StarLinkedList *)mSectionList->PushIterator();
    while(rSection)
    {
        //--Name
        const char *rSectionName = mSectionList->GetIteratorName();

        //--Catalogues
        StarLinkedList *rCatalogue = (StarLinkedList *)rSection->PushIterator();
        while(rCatalogue)
        {
            //--Name
            const char *rCatalogueName = rSection->GetIteratorName();

            //--Headings
            StarLinkedList *rHeading = (StarLinkedList *)rCatalogue->PushIterator();
            while(rHeading)
            {
                //--Name
                const char *rHeadingName = rCatalogue->GetIteratorName();

                //--Entries
                void *rEntry = rHeading->PushIterator();
                while(rEntry)
                {
                    //--Print it
                    fprintf(fOutfile, "%s/%s/%s/%s\n", rSectionName, rCatalogueName, rHeadingName, rHeading->GetIteratorName());

                    rEntry = rHeading->AutoIterate();
                }

                //--Iterate
                rHeading = (StarLinkedList *)rCatalogue->AutoIterate();
            }

            //--Iterate
            rCatalogue = (StarLinkedList *)rSection->AutoIterate();
        }

        //--Iterate
        rSection = (StarLinkedList *)mSectionList->AutoIterate();
    }

    //--Clean
    fclose(fOutfile);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *DataLibrary::GetMasterFontList()
{
    return mMasterFontList;
}
StarLinkedList *DataLibrary::GetPointerSeriesList()
{
    return mPointerSeriesList;
}

///===================================== Static Functions =========================================
DataLibrary *DataLibrary::Fetch()
{
    return Global::Shared()->gDataLibrary;
}
const char *DataLibrary::GetGamePath(const char *pVariablePath)
{
    //--Returns a non-mutable string that is derived from the SysVar* at the location provided. If
    //  the location does not exist or is not a SysVar*, then the string returned is "NO PATH" to
    //  aid in debugging. An error message will be logged as well.
    //--A list of paths:
    //  "Root/Paths/System/Startup/sClassicModePath"
    //  "Root/Paths/System/Startup/sClassicMode3DPath"
    //  "Root/Paths/System/Startup/sCorrupterModePath"
    //  "Root/Paths/System/Startup/sAdventurePath"
    //  "Root/Paths/System/Startup/sDollManorPath"
    //  "Root/Paths/System/Startup/sElectrospriteAdventurePath"
    //  "Root/Paths/System/Startup/sMOTFPath"
    //  "Root/Paths/System/Startup/sLevelGeneratorPath"
    //  "Root/Paths/System/Startup/sPairanormalPath"
    //  "Root/Paths/System/Startup/sSlittedEyePath"
    //  "Root/Paths/System/Startup/sPeakFreaksPath"
    if(!pVariablePath) return "NO PATH";

    //--Verify the DataLibrary exists.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary) return "DATALIBRARY NOT BUILT";

    //--Find the variable.
    SysVar *rPathVariable = (SysVar *)rDataLibrary->GetEntry(pVariablePath);
    if(rPathVariable)
    {
        return (const char *)rPathVariable->mAlpha;
    }

    //--Return the error, print an error message.
    fprintf(stderr, "Attempting to acquire invalid path variable: %s\n", pVariablePath);
    sprintf(gxLastIssue, "Attempting to acquire invalid path variable: %s\n", pVariablePath);
    LogError();
    return "NO PATH";
}
