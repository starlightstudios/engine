///==================================== AdvUIFieldAbilities =======================================
//--UI that allows the player to reorganize their field abilities.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUIFieldAbilities : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    //--Timers
    //--Positions
    //--Sizes
    static const int cxAdv_FieldAbilitySlots = 5;

    ///--[Variables]
    //--System
    bool mIsFirstTick;
    bool mIsSwitching;
    int mHiCursor;
    int mLoCursor;

    //--Field Ability List
    StarLinkedList *mrFieldAbilityList; //FieldAbility *, reference

    //--Cursor Movement
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;
    EasingPack2D mLolightPos;
    EasingPack2D mLolightSize;

    //--Help Text
    StarlightString *mConfirmString;
    StarlightString *mCancelString;
    StarlightString *mClearString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rDoubleHeaderFont;
        StarFont *rHeaderFont;
        StarFont *rMainFont;

        //--Images for this UI
        StarBitmap *rHeader;
        StarBitmap *rFooter;
        StarBitmap *rOverlay_Highlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIFieldAbilities();
    virtual ~AdvUIFieldAbilities();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    void RegisterFieldAbility(const char *pDLPath);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


