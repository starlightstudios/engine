//--Base
#include "WorldDialogue.h"

//--Classes
#include "AdventureLevel.h"
#include "DialogueActor.h"
#include "DialogueTopic.h"
#include "StringEntry.h"
#include "VisualLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"
#include "MapManager.h"

///--[Debug]
//#define WD_SAVE_DEBUG
#ifdef WD_SAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
WorldDialogue::WorldDialogue()
{
    ///--[ ============ RootObject ============ ]
    //--System
    mType = POINTER_TYPE_WORLDDIALOGUE;

    ///--[ ========== WorldDialogue =========== ]
    //--System
    mIsAnyKeyMode = false;
    mNeedsToReappend = false;
    mAutoHastenDialogue = false;
    mInstantTextMode = false;
    mVisibilityState = WD_INVISIBLE;
    mHidingTimer = -1;
    mAllKeysHasten = false;
    mTagSkipLen = 0;
    mUnstagedActorsCanRemap = true;

    //--Constants
    cDialogueMaxWidth = 1280.0f;
    cMajorSceneDarkening = 0.65f;
    cMajorDialogueDarkening = 0.80f;

    //--Visual Novel Mode
    mIsVisualNovel = false;
    VN.mBackgroundTimer = 0;
    VN.rCurBackground = NULL;
    VN.rPrevBackground = NULL;
    VN.mBackgroundRemaps = new StarLinkedList(true);
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        VN.mActorCurrents[i] = 0.0f;
        VN.mActorStarts[i] = 0.0f;
        VN.mActorTargets[i] = 0.0f;
        VN.mActorTimers[i] = 0;
        VN.mActorTimersMax[i] = 0;
        VN.mActorMoveFlags[i] = WD_VN_MOVETPYE_NONE;
        VN.mActorPostExecs[i] = NULL;
    }

    //--Sound
    mIsSilenced = false;
    mTextTickTimer = 150.0f;

    //--Position
    mTextScale = 1.0f;
    mDimensions.SetWH(0.0f, 576.0f, VIRTUAL_CANVAS_X, 192.0f);
    mUpperDimensions.SetWH(0.0f, 0.0f, VIRTUAL_CANVAS_X, 221.0f);

    //--Colors
    mWhitePack.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

    //--Dialogue Storage
    mHasAppendedNonSpace = false;
    mDialogueUsesAutoresolveTarget = true;
    mFadeTimer = 0;
    mIsPrintingComplete = false;
    mPrintTimeRemainder = 0;
    mMajorSequenceSpeaker = NULL;
    mMajorSequenceSpeakerDisplay = NULL;
    mSpeakerNameRemaps = new StarLinkedList(true);
    mDialogueLog = new StarLinkedList(true);
    mDialogueList = new StarLinkedList(true);

    //--Decisions
    mTicksSinceDecisionOpen = 0;
    mIsDecisionMode = false;
    mDecisionUsesMouse = xDialogueUsesMouse;
    mDecisionCursor = 0;
    mHighlightedDecision = -1;
    mDecisionScale = 2.0f;
    mDecisionList = new StarLinkedList(true);
    mDecisionHitboxes = new StarLinkedList(true);
    mDecisionDimensions[0].Set(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.40f, 1.0f, 1.0f);
    mDecisionDimensions[1].Set(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.50f, 1.0f, 1.0f);
    mDecisionDimensions[2].Set(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.60f, 1.0f, 1.0f);

    //--Topics
    mRunTopicsAfterDialogue = false;
    memset(mRunTopicsAfterDialogueName, 0, sizeof(char) * STD_NPC_LETTERS);
    mIsTopicsMode = false;
    mTopicTimer = 0;
    mTopicCursor = 0;
    mTopicScroll = 0;
    mLastTopicCursor = 0;
    strncpy(mCurrentTopicActor, "Null", STD_NPC_LETTERS-1);
    mGoodbyeTopic = new DialogueTopic();
    mGoodbyeTopic->SetName("Goodbye");
    mGoodbyeTopic->SetDisplayName("Goodbye");
    mTopicListing = new StarLinkedList(true);
    mrCurrentTopicListing = new StarLinkedList(false);

    //--Continuation
    mContinuationScript = NULL;
    mContinuationString = NULL;

    //--Blocking
    mIsBlocking = false;
    mIsSoftBlocking = false;
    mIsPausing = false;
    mSoftBlockTimer = 0;
    mPendingDialogue = NULL;

    //--Temporary
    mHideDialogueBoxes = false;
    mIsSpookyText = false;
    mIsLargeText = false;
    mIsSmallText = false;
    mLettersAppended = 0;
    rActiveAppendList = NULL;

    //--Complex Actor Properties
    mIsMajorSequenceMode = false;
    mMajorSequenceNeedsReset = true;
    mMajorSequenceTimer = 0;
    mActorBench = new StarLinkedList(true);
    memset(mDialogueActors, 0, sizeof(DialogueActor *) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPortraitLookupsX, 0, sizeof(float) * WD_DIALOGUE_ACTOR_SLOTS);

    //--Actor Storage
    memset(mPreviousActorNames,    0, WD_DIALOGUE_ACTOR_SLOTS * WD_ACTOR_NAME_MAX);
    memset(mPreviousActorEmotions, 0, WD_DIALOGUE_ACTOR_SLOTS * WD_ACTOR_NAME_MAX);

    //--Crossfades
    memset(rPreviousActors,     0, sizeof(DialogueActor *) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(rPreviousImages,     0, sizeof(StarBitmap *)    * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPreviousFadeTimers, 0, sizeof(int)             * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPreviousXOffsets,   0, sizeof(float)           * WD_DIALOGUE_ACTOR_SLOTS);

    //--CG and TF Scene Handling
    mIsSceneMode = false;
    mIgnoreSceneOffsets = false;
    mSceneOffsetX = 0.0f;
    mSceneOffsetY = 0.0f;
    rSceneImg = NULL;
    mSceneTimer = 0;
    mSceneFadeTimeMax = WD_SCENE_CROSSFADE_TICKS;
    mSceneTicksPerFrame = 1;
    mSceneAnimFramesTotal = 0;
    mrSceneAnimFrames = NULL;
    mSceneTransitionTimer = 0;
    rOldSceneImg = NULL;
    mOldSceneOffsetX = 0.0f;
    mOldSceneOffsetY = 0.0f;
    mCensorBarList = NULL;
    mOldCensorBarList = NULL;

    //--Scene Flash
    mIsSceneFlash = false;
    mSceneFlashTimer = 0;

    //--Voice Controller
    mIsFreshClear = true;
    mLeaderVoice = InitializeString("NoLeader");
    mLastSetSpeaker = InitializeString("NoLeader");
    rCurrentVoice = mNormalTextTick;
    strcpy(mNormalTextTick, "Menu|TextTick");
    mVoiceListing = new StarLinkedList(true);

    //--String Entry
    mIsStringEntryMode = false;
    mStringEntryCallOnComplete = NULL;
    mStringEntryForm = NULL;

    //--Credits Sequence
    mIsCreditsSequence = false;
    mCreditsTimerCur = 0;
    mCreditsTimerMax = 1;
    mCreditsPacks = NULL;
    rCreditsBackground = NULL;

    //--Asset Streaming
    mrAllShownDialoguePortraits = new StarLinkedList(false);

    //--Parser
    mCurrentParserLen = 0.0f;

    //--Images
    memset(&Images, 0, sizeof(Images));
}
WorldDialogue::~WorldDialogue()
{
    delete VN.mBackgroundRemaps;
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++) free(VN.mActorPostExecs[i]);
    delete mSpeakerNameRemaps;
    delete mDialogueLog;
    delete mDialogueList;
    delete mDecisionList;
    delete mDecisionHitboxes;
    free(mPendingDialogue);
    delete mGoodbyeTopic;
    delete mTopicListing;
    delete mrCurrentTopicListing;
    free(mContinuationScript);
    free(mContinuationString);
    delete mActorBench;
    free(mMajorSequenceSpeaker);
    free(mMajorSequenceSpeakerDisplay);
    free(mrSceneAnimFrames);
    delete mCensorBarList;
    delete mOldCensorBarList;
    free(mLeaderVoice);
    free(mLastSetSpeaker);
    delete mVoiceListing;
    delete mStringEntryForm;
    delete mCreditsPacks;
    delete mrAllShownDialoguePortraits;
}
void WorldDialogue::Construct()
{
    ///--[Image Loading]
    //--Builds resources after setup has been completed.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(Images.mIsReady) return;

    //--Borders
    Images.Data.rBorderCard  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/BorderCard");
    Images.Data.rNameBox     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/NamelessBox");
    Images.Data.rNamelessBox = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/NamelessBox");
    Images.Data.rNamePanel   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/NamePanel");

    //--Fonts.
    Images.Data.rHeadingFont  = rDataLibrary->GetFont("World Dialogue Heading");
    Images.Data.rDecisionFont = rDataLibrary->GetFont("World Dialogue Decision");
    Images.Data.rDialogueFont = rDataLibrary->GetFont("World Dialogue Main");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    ///--[Rendering Constants]
    //--Default.
    mPortraitLookupsXDefault[0] = VIRTUAL_CANVAS_X * 0.10f;
    mPortraitLookupsXDefault[1] = VIRTUAL_CANVAS_X * 0.25f;
    mPortraitLookupsXDefault[2] = VIRTUAL_CANVAS_X * 0.40f;
    mPortraitLookupsXDefault[3] = VIRTUAL_CANVAS_X * 0.85f;
    mPortraitLookupsXDefault[4] = VIRTUAL_CANVAS_X * 0.70f;
    mPortraitLookupsXDefault[5] = VIRTUAL_CANVAS_X * 0.60f;

    //--Note: These constants can change when the field becomes full. See MoveActorToSlot() for more.
    mPortraitLookupsX[0] = VIRTUAL_CANVAS_X * 0.20f;
    mPortraitLookupsX[1] = VIRTUAL_CANVAS_X * 0.30f;
    mPortraitLookupsX[2] = VIRTUAL_CANVAS_X * 0.40f;
    mPortraitLookupsX[3] = VIRTUAL_CANVAS_X * 0.85f;
    mPortraitLookupsX[4] = VIRTUAL_CANVAS_X * 0.70f;
    mPortraitLookupsX[5] = VIRTUAL_CANVAS_X * 0.60f;

    //--Heavy left version. Used for when there are four characters on the left side.
    mPortraitLookupsXHeavyLft[0] = VIRTUAL_CANVAS_X * 0.20f;
    mPortraitLookupsXHeavyLft[1] = VIRTUAL_CANVAS_X * 0.30f;
    mPortraitLookupsXHeavyLft[2] = VIRTUAL_CANVAS_X * 0.40f;
    mPortraitLookupsXHeavyLft[3] = VIRTUAL_CANVAS_X * 0.90f;
    mPortraitLookupsXHeavyLft[4] = VIRTUAL_CANVAS_X * 0.75f;
    mPortraitLookupsXHeavyLft[5] = VIRTUAL_CANVAS_X * 0.65f;
    mPortraitLookupsXHeavyLft[6] = VIRTUAL_CANVAS_X * 0.10f;
}

///--[Public Statics]
//--Variable that tracks how many appends are in use, since the Append() function is re-entrant. Special
//  behaviors may occur on the 0th stack.
int WorldDialogue::xAppendStack = 0;

//--Set at AdventureMode startup, determines where to look for topics when NPCs enter topics mode.
char *WorldDialogue::xTopicsDirectory = NULL;

//--Parsing flag. Indicates the last parsed letter is '::' which resolves to ':'.
int WorldDialogue::xIsColon = 0;

//--Y distance, in pixels, between the lower and upper dialogues.
float WorldDialogue::xUpperDialogueYOffset = -540.0f;

//--If true, the WorldDialogue keeps track of which dialogue images it rendered, and drops them from memory
//  when it closes. This reduces memory overhead but requires extra asset streaming.
bool WorldDialogue::xUnloadDialoguePortraitsWhenClosing = false;

//--If true, uses buttons for the mouse instead of arrow keys when making decisions.
bool WorldDialogue::xDialogueUsesMouse = false;

///===================================== Property Queries =========================================
bool WorldDialogue::IsVisible()
{
    //--If the visibility state is "INVISIBLE", the object is not visible:
    if(mVisibilityState == WD_INVISIBLE) return false;

    //--Otherwise visible.
    return true;
}
bool WorldDialogue::IsManagingUpdates()
{
    //--If this function returns true, the dialogue is presently controlling player input and should
    //  stop anything calling it from updating.
    if(mVisibilityState == WD_INVISIBLE) return false;

    //--If the hiding timer is not -1, the object is hiding, and should not stop updates.
    if(mHidingTimer != -1) return false;

    //--Object is visible and not hiding.
    return true;
}
bool WorldDialogue::IsMajorSequence()
{
    return mIsMajorSequenceMode;
}
bool WorldDialogue::IsBlocking()
{
    return mIsBlocking;
}
bool WorldDialogue::IsStoppingEvents()
{
    //--If not visible, can't stop events.
    if(!IsVisible()) return false;
    if(IsManagingUpdates()) return true;

    //--If the dialogue is still printing, events stop.
    if(!mIsPrintingComplete)
    {
        return true;
    }

    //--If currently waiting on a key press, events stop.
    if(mIsBlocking)
    {
        return true;
    }

    //--All checks passed, events not stopped.
    return false;
}
bool WorldDialogue::HasTextFlowing()
{
    return !mIsPrintingComplete;
}
bool WorldDialogue::IsAutoHastening()
{
    return mAutoHastenDialogue;
}

///======================================== Manipulators ==========================================
void WorldDialogue::Show()
{
    Wipe();
    mHideDialogueBoxes = false;
    mIsFreshClear = true;
    mIsSilenced = false;
    mVisibilityState = WD_VISIBLE;
}
void WorldDialogue::Hide()
{
    //--Can't hide if already hiding!
    if(mHidingTimer != -1) return;
    mHidingTimer = 0;
}
void WorldDialogue::SetDialogueMaxWidth(float pLength)
{
    cDialogueMaxWidth = pLength;
    if(cDialogueMaxWidth < 100.0f) cDialogueMaxWidth = 100.0f;
}
void WorldDialogue::SetMajorSceneDarkening(float pAlpha)
{
    cMajorSceneDarkening = pAlpha;
}
void WorldDialogue::SetMajorDialogueDarkening(float pAlpha)
{
    cMajorDialogueDarkening = pAlpha;
}
void WorldDialogue::SetNormalTextTick(const char *pString)
{
    if(!pString) return;
    strcpy(mNormalTextTick, pString);
}
void WorldDialogue::SetAnyKeyMode(bool pFlag)
{
    mIsAnyKeyMode = pFlag;
}
void WorldDialogue::ActivateFlash(int pStartTicks)
{
    mIsSceneFlash = true;
    mSceneFlashTimer = pStartTicks;
}
void WorldDialogue::SetTargetAutoresolve(bool pFlag)
{
    mDialogueUsesAutoresolveTarget = pFlag;
}
void WorldDialogue::BypassFade()
{
    //--Use this to stop fading after calling Show().
    mMajorSequenceTimer = WD_MAJOR_SEQUENCE_FADE_TICKS;
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        mPreviousFadeTimers[i] = WD_CHARACTER_CROSSFADE_TICKS;
    }
}
void WorldDialogue::SetSilenceFlag(bool pFlag)
{
    mIsSilenced = pFlag;
}
void WorldDialogue::AddDialogueLockout()
{
    AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
    if(rFetchLevel) rFetchLevel->AddLockout(WD_STANDARD_LOCKOUT);
}
void WorldDialogue::ClearDialogueLockout()
{
    AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
    if(rFetchLevel) rFetchLevel->RemoveLockout(WD_STANDARD_LOCKOUT);
}
void WorldDialogue::SetSpeaker(const char *pSpeakerName)
{
    //--Reset the voice to the default text tick.
    rCurrentVoice = mNormalTextTick;

    //--Store this as the last set speaker. This happens even if the speaker is invalid for some reason.
    ResetString(mLastSetSpeaker, pSpeakerName);

    //--Portrait handling. Modifies the visiblity states of all actors. The bench is used here,
    //  since theoretically a benched actor could enter the scene and want their grey state set.
    //--If no speaker gets set, all actors are ungreyed.
    if(!pSpeakerName || !strcasecmp(pSpeakerName, "Null") || !strcasecmp(pSpeakerName, "Narrator"))
    {
        //--Iterate.
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            rActor->SetDarkenedState(true);
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }
    }
    //--If the speaker is "Thought" then the party leader is highlighted.
    else if(!strcasecmp(pSpeakerName, "Thought"))
    {
        //--Iterate.
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            //--Set.
            rActor->SetDarkeningFromName(mLeaderVoice);

            //--Next.
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }

        //--Override the voice to that of the leader.
        SetVoice(mLeaderVoice);
    }
    //--In all other cases, the Actors will set their grey state based on their aliases.
    else
    {
        //--Iterate.
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            //--Set.
            rActor->SetDarkeningFromName(pSpeakerName);

            //--If the Actor is not darkened, check their name against the voice list. If a match is found, set that as the active voice.
            //  The actor must be on the stage or they won't set their voice, since some actors share aliases.
            int tActorSlot = IsActorOnStage(rActor);
            if(!rActor->IsDarkened() && tActorSlot != -1)
            {
                //--Search voice list. Set that voice.
                char *rCheckVoice = (char *)mVoiceListing->GetElementByName(rActor->GetName());
                if(rCheckVoice) rCurrentVoice = rCheckVoice;
            }

            //--Next.
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }
    }
}
void WorldDialogue::SetSpeakerBySlot(int pSpeakerSlot)
{
    //--Marks the given slot as the speaker. Same as above, but uses a slot instead of a name.
    rCurrentVoice = mNormalTextTick;
    ResetString(mLastSetSpeaker, "Null");

    //--If the slot is out of range, everyone is darkened.
    if(pSpeakerSlot < 0 || pSpeakerSlot >= WD_DIALOGUE_ACTOR_SLOTS)
    {
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            rActor->SetDarkenedState(true);
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }
    }
    //--In all other cases, the actor in the slot becomes the speaker, and everyone else is darkened.
    else
    {
        //--Iterate.
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            //--Get slot.
            int tActorSlot = IsActorOnStage(rActor);
            if(tActorSlot == pSpeakerSlot)
            {
                //--Highlight.
                rActor->SetDarkenedState(false);

                //--Set voice.
                char *rCheckVoice = (char *)mVoiceListing->GetElementByName(rActor->GetName());
                if(rCheckVoice) rCurrentVoice = rCheckVoice;
            }
            //--Darkened.
            else
            {
                rActor->SetDarkenedState(true);
            }

            //--Next.
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }
    }
}
void WorldDialogue::AddSpeakerRemap(const char *pSpeakerName, const char *pRemapName)
{
    //--Adds a name that will be remapped. The pSpeakerName will be replaced with the pRemapName
    //  when it appears in a [NAME|pSpeakerName] tag, and also when setting the speaker with SetSpeaker().
    if(!pSpeakerName || !pRemapName) return;
    mSpeakerNameRemaps->AddElement(pSpeakerName, InitializeString(pRemapName), &FreeThis);
}
int WorldDialogue::IsActorOnStage(void *pCheckPtr)
{
    //--Returns which slot the actor is in, or -1 if it's not on the stage.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++) if(mDialogueActors[i] == pCheckPtr) return i;
    return -1;
}
void WorldDialogue::SetContinuationScript(const char *pPath, const char *pString)
{
    ResetString(mContinuationScript, pPath);
    ResetString(mContinuationString, pString);
}
void WorldDialogue::SetVoice(const char *pSpeaker)
{
    rCurrentVoice = (char *)mVoiceListing->GetElementByName(pSpeaker);
}
void WorldDialogue::SetLeaderVoice(const char *pLeaderVoice)
{
    if(!pLeaderVoice) return;
    ResetString(mLeaderVoice, pLeaderVoice);
}
void WorldDialogue::RegisterVoice(const char *pSpeaker, const char *pVoicePath)
{
    //--Note: Speaker will refer to the DialogueActor, *not* the alias! "Alraune" covers "Rochea", "Adina" and "Alraune", for example.
    if(!pSpeaker || !pVoicePath) return;

    //--If the entry already exists, replace it.
    mVoiceListing->RemoveElementS(pSpeaker);
    mVoiceListing->AddElement(pSpeaker, InitializeString(pVoicePath), &FreeThis);
}
void WorldDialogue::SetHideFlag(bool pHideDialogueBoxes)
{
    mHideDialogueBoxes = pHideDialogueBoxes;
}
void WorldDialogue::SetAutoHastenFlag(bool pAutoHastenDialogue)
{
    mAutoHastenDialogue = pAutoHastenDialogue;
}
void WorldDialogue::SetInstantTextFlag(bool pFlag)
{
    mInstantTextMode = pFlag;
}
void WorldDialogue::BypassBlock()
{
    //--Do nothing if not blocking.
    if(!mIsBlocking) return;

    //--Store the old pending dialogue.
    char *tOldPendingDialogue = mPendingDialogue;

    //--Clear the pointer to it in case AppendString() needs to use it again.
    mPendingDialogue = NULL;

    //--Re-append. Artificially increase the appendation stack so the emphasis does not change.
    //  Not that a [CLEAR] tag will override this.
    mIsBlocking = false;
    mIsSoftBlocking = false;
    mIsPausing = false;
    xAppendStack += 2;
    AppendString(tOldPendingDialogue, false);
    xAppendStack -= 2;

    //--Clean up the old dialogue.
    free(tOldPendingDialogue);
}
void WorldDialogue::ActivateStringEntry(const char *pPostExec)
{
    //--Error check.
    if(!pPostExec || mIsStringEntryMode) return;

    //--Flags.
    mIsStringEntryMode = true;
    ResetString(mStringEntryCallOnComplete, pPostExec);

    //--If this is the first time using the string entry, construct it.
    if(!mStringEntryForm)
    {
        mStringEntryForm = StringEntry::rGenerateStringEntry();
        mStringEntryForm->SetString(NULL);
        mStringEntryForm->SetCompleteFlag(false);
        mStringEntryForm->SetPromptString("Enter password:");
        mStringEntryForm->SetEmptyString("Password");
    }
    //--Otherwise, just reset it.
    else
    {
        mStringEntryForm->SetString(NULL);
        mStringEntryForm->SetCompleteFlag(false);
        mStringEntryForm->SetPromptString("Enter password:");
        mStringEntryForm->SetEmptyString("Password");
    }
}
void WorldDialogue::SetAllKeysHasten(bool pFlag)
{
    mAllKeysHasten = pFlag;
}
void WorldDialogue::ReplaceHeadingFont(const char *pDLFontName)
{
    //--If the font is not found, do nothing.
    StarFont *rCheckFont = DataLibrary::Fetch()->GetFont(pDLFontName);
    if(!rCheckFont) return;
    Images.Data.rHeadingFont = rCheckFont;
}
void WorldDialogue::ReplaceDecisionFont(const char *pDLFontName)
{
    //--If the font is not found, do nothing.
    StarFont *rCheckFont = DataLibrary::Fetch()->GetFont(pDLFontName);
    if(!rCheckFont) return;
    Images.Data.rDecisionFont = rCheckFont;
}
void WorldDialogue::ReplaceMainlineFont(const char *pDLFontName)
{
    //--If the font is not found, do nothing.
    StarFont *rCheckFont = DataLibrary::Fetch()->GetFont(pDLFontName);
    if(!rCheckFont) return;
    Images.Data.rDialogueFont = rCheckFont;
}
void WorldDialogue::ReplaceNamelessBox(const char *pDLPath)
{
    StarBitmap *rCheckBitmap = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    if(!rCheckBitmap) return;
    Images.Data.rNameBox = rCheckBitmap;
    Images.Data.rNamelessBox = rCheckBitmap;
}
void WorldDialogue::ReplaceDecisionBorderCard(const char *pDLPath)
{
    StarBitmap *rCheckBitmap = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    if(!rCheckBitmap) return;
    Images.Data.rBorderCard = rCheckBitmap;
}

///======================================== Core Methods ==========================================
void WorldDialogue::Clear()
{
    //--Clears all dialogue off the screen. This only affects whichever dialogue box is rendering.
    mDialogueLog->ClearList();
    mDialogueList->ClearList();

    //--Common.
    mIsSpookyText = false;
    mIsLargeText = false;
    mIsSmallText = false;
    rActiveAppendList = NULL;

    //--Reset flags.
    mHasAppendedNonSpace = false;
    mFadeTimer = 0;
    mTextTickTimer = 150.0f;
    mIsPrintingComplete = true;
    mPrintTimeRemainder = 0;
    rCurrentVoice = NULL;
    mCurrentParserLen = 0.0f;

    //--When in scenes mode, the current voice auto-switches to the text tick.
    if(mIsSceneMode) rCurrentVoice = mNormalTextTick;
}
void WorldDialogue::Wipe()
{
    ///--[Documentation]
    //--Wipes the dialogue back to factory zero. Used whenever this object is hidden.

    ///--[Un-load Dialogue Portraits]
    //--If flagged, order all dialogue portraits that were rendered to drop their assets.
    if(xUnloadDialoguePortraitsWhenClosing && false)
    {
        //--Debug,
        //fprintf(stderr, "Dialogue is unloading %i images.\n", mrAllShownDialoguePortraits->GetListSize());

        //--Iterate. Order the images to unload.
        StarBitmap *rBitmap = (StarBitmap *)mrAllShownDialoguePortraits->PushIterator();
        while(rBitmap)
        {
            rBitmap->UnloadData();
            rBitmap = (StarBitmap *)mrAllShownDialoguePortraits->AutoIterate();
        }

        //--Clear.
        mrAllShownDialoguePortraits->ClearList();
    }

    ///--[WorldDialogue Variables]
    //--System
    mNeedsToReappend = false;
    mVisibilityState = WD_INVISIBLE;
    mHidingTimer = -1;
    mAllKeysHasten = false;

    //--Sound
    mIsSilenced = false;
    mTextTickTimer = 150.0f;

    //--Position
    //--Colors
    //--Dialogue Storage
    mHasAppendedNonSpace = false;
    mFadeTimer = 0;
    mIsPrintingComplete = false;
    mPrintTimeRemainder = 0;
    ResetString(mMajorSequenceSpeaker, NULL);
    ResetString(mMajorSequenceSpeakerDisplay, NULL);
    mDialogueLog->ClearList();
    mDialogueList->ClearList();

    //--Decisions
    mIsDecisionMode = false;
    mDecisionList->ClearList();

    //--Topics
    mRunTopicsAfterDialogue = false;
    mIsTopicsMode = false;

    //--Continuation
    ResetString(mContinuationScript, NULL);
    ResetString(mContinuationString, NULL);

    //--Blocking
    mIsBlocking = false;
    ResetString(mPendingDialogue, NULL);

    //--Temporary
    rActiveAppendList = NULL;

    //--Complex Actor Properties
    mIsMajorSequenceMode = false;
    mMajorSequenceNeedsReset = true;
    mMajorSequenceResetTimer = Global::Shared()->gTicksElapsed;
    memset(rPreviousImages,     0, sizeof(StarBitmap *)   * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPreviousFadeTimers, 0, sizeof(int)             * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mDialogueActors,     0, sizeof(DialogueActor *) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPreviousXOffsets,   0, sizeof(float)           * WD_DIALOGUE_ACTOR_SLOTS);

    //--Scene mode.
    mIsSceneMode = false;
    mIgnoreSceneOffsets = false;
    mSceneOffsetX = 0.0f;
    mSceneOffsetY = 0.0f;
    rSceneImg = NULL;
    mSceneTimer = 0;
    mSceneTicksPerFrame = 1;
    mSceneAnimFramesTotal = 0;
    free(mrSceneAnimFrames);
    mrSceneAnimFrames = NULL;
    rCurrentVoice = NULL;
    //Note: The censor bars are NOT wiped, as they are needed in case of crossfade.

    //--Asset Streaming
    mrAllShownDialoguePortraits->ClearList();

    //--Reset these position flags.
    mPortraitLookupsX[0] = mPortraitLookupsXDefault[0];
    mPortraitLookupsX[1] = mPortraitLookupsXDefault[1];
    mPortraitLookupsX[2] = mPortraitLookupsXDefault[2];
}
StarLinkedList *WorldDialogue::ResolveActiveDialogueList()
{
    return mDialogueList;
}
float WorldDialogue::ComputeLengthOf(StarLinkedList *pListOfCharacters)
{
    ///--[Documentation]
    //--Provided a list containing DialogueCharacter structures, computes the length of the resulting string
    //  and returns it. This takes scaling into account.
    //--Because these are individual characters, we can use the kerning lookup tables.
    if(!pListOfCharacters || !Images.mIsReady) return 0.0f;

    //--Setup.
    float tRunningWidth = 0.0f;

    //--Iterate.
    DialogueCharacter *rCharacter = (DialogueCharacter *)pListOfCharacters->PushIterator();
    while(rCharacter)
    {
        //--Get the next character.
        DialogueCharacter *rNextCharacter = (DialogueCharacter *)pListOfCharacters->AutoIterate();

        //--Next character exists:
        if(rNextCharacter)
        {
            tRunningWidth = tRunningWidth + Images.Data.rDialogueFont->GetKerningBetween(rCharacter->mLetter, rNextCharacter->mLetter);
        }
        //--Next character does not exist, pass a NULL for the next character.
        else
        {
            tRunningWidth = tRunningWidth + Images.Data.rDialogueFont->GetKerningBetween(rCharacter->mLetter, '\0');
        }

        //--Iterate.
        rCharacter = rNextCharacter;
    }

    //--All done, return.
    return tRunningWidth;
}
void WorldDialogue::WriteActorsToConsole()
{
    ///--[Documentation]
    //--Diagnostics, writes actor information to the console.
    fprintf(stderr, "== Writing Actor Bench Information ==\n");
    DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
    while(rActor)
    {
        //--Print.
        fprintf(stderr, " %s\n", mActorBench->GetIteratorName());

        //--Next.
        rActor = (DialogueActor *)mActorBench->AutoIterate();
    }
}
void WorldDialogue::WriteActorsInfoToConsole(const char *pName)
{
    ///--[Documentation]
    //--Writes a specific actor's information to the console.
    DialogueActor *rActor = (DialogueActor *)mActorBench->GetElementByName(pName);
    if(!pName || !rActor)
    {
        fprintf(stderr, "Warning: Unable to write actor info. Actor %s was not found.\n", pName);
        return;
    }

    //--Write.
    rActor->DebugPrintData();
}
void WorldDialogue::SetDisplaySpeakerFromName(const char *pName)
{
    ///--[Documentation]
    //--Given a name, resolves the speaker name that should display above the dialogue box. This is done by
    //  first scanning if there is an external override, and then checking if the speaking character has
    //  an internal override. If none are present, the provided name becomes the speaker's name.
    //--This is intended to allow a layer of translations for other languages. It is entirely a display thing.
    //  The internal speaker name is handled by SetSpeaker().
    if(!pName) return;

    ///--[Major Sequence Speaker]
    //--The actual speaker name is always the name of the speaker, and cannot be remapped. It is only used
    //  internally.
    ResetString(mMajorSequenceSpeaker, pName);

    ///--[External Remaps]
    //--It is possible for a global remap to be applied before anything else is. This is character-independent.
    //  If a remap is applied, internal aliases are not. This allows for "generic" remaps or translations.
    //--An example is the word "Voice" appearing as the speaker, when an unknown person is speaking.
    char *rSpeakerRemap = (char *)mSpeakerNameRemaps->GetElementByName(pName);
    if(rSpeakerRemap)
    {
        ResetString(mMajorSequenceSpeakerDisplay, rSpeakerRemap);
        return;
    }

    ///--[Per-Actor Remaps]
    //--If there is a dialogue actor who is the speaker, then they might have an internal remap. This is
    //  typically done for translations, as the Alias system already allows for "remaps" by setting a speaker
    //  from a different word.
    //--To do this, a DialogueActor must be found and staged. If an actor is found but not staged it is ignored.
    DialogueActor *rActiveActor = NULL;
    DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
    while(rActor)
    {
        //--Actor must reply to this name, either as a direct match or an alias.
        if(rActor->IsSpeaking(pName))
        {
            //--If this flag is true, an actor does not need to be on stage in order to apply a remap.
            if(mUnstagedActorsCanRemap)
            {
                rActiveActor = rActor;
            }
            //--If the flag is false, the actor must be on stage. Check that.
            else if(IsActorOnStage(rActor) != -1)
            {
                rActiveActor = rActor;
            }

            //--If the active actor got set, it was valid. Stop iterating.
            if(rActiveActor)
            {
                mActorBench->PopIterator();
                break;
            }
        }

        //--Next.
        rActor = (DialogueActor *)mActorBench->AutoIterate();
    }

    //--If a DialogueActor was found and valid, then check if it has an internal override.
    if(rActiveActor)
    {
        //--Check for internal override. If it exists, use it.
        const char *rCheckOverride = rActiveActor->GetNameFromAlias(mMajorSequenceSpeaker);
        if(rCheckOverride)
        {
            ResetString(mMajorSequenceSpeakerDisplay, rCheckOverride);
        }
        //--There is no override. Use the given name as the display name.
        else
        {
            ResetString(mMajorSequenceSpeakerDisplay, pName);
        }

        //--In any case, return.
        return;
    }

    ///--[No Remaps]
    //--No remap or actor remap was found. In Major Sequence mode, this means nobody is technically speaking
    //  since only a staged character may speak. If this happens, clear the display name.
    if(mIsMajorSequenceMode)
    {
        ResetString(mMajorSequenceSpeaker, NULL);
        ResetString(mMajorSequenceSpeakerDisplay, NULL);
        return;
    }

    //--If not in Major Sequence mode, anyone can speak since no actors ought to be staged.
    ResetString(mMajorSequenceSpeakerDisplay, pName);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void WorldDialogue::Update()
{
    ///--[Documentation and Setup]
    //--Handles the update cycle for the dialogue. Handles all actor animation and controls.

    //--Debug.
    DebugPush(true, "WorldDialogue:Update - Begin.\n");

    ///--[Sub-Handlers]
    //--Credits.
    if(mIsCreditsSequence)
    {
        UpdateCredits();
        return;
    }

    ///--[Sub-Updates and Timers]
    //--Run the update routine for all dialogue actors. All they do is modify internal timers.
    DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
    while(rActor)
    {
        if(!mIsMajorSequenceMode) rActor->SetDarkenedState(true);
        rActor->Update();
        rActor = (DialogueActor *)mActorBench->AutoIterate();
    }

    //--When hiding, do nothing except run the hide timer.
    if(mHidingTimer != -1)
    {
        //--Hiding timer accelerates if the player is accelerating the cutscenes.
        ControlManager *rControlManager = ControlManager::Fetch();
        bool tIsHoldingActivate = rControlManager->IsDown("Activate") || rControlManager->IsDown("MouseLft");
        bool tIsHoldingCancel = rControlManager->IsDown("Cancel");
        if(tIsHoldingActivate && tIsHoldingCancel)
        {
            mHidingTimer += 10;
        }
        else
        {
            mHidingTimer ++;
        }

        //--Finished hiding.
        if(mHidingTimer >= WD_HIDING_TICKS)
        {
            //--Reset flags.
            mHidingTimer = -1;
            mVisibilityState = WD_INVISIBLE;

            //--Clear leftover data.
            Wipe();
            DeactivateScenesMode();
        }
        return;
    }

    //--Scene flash.
    if(mIsSceneFlash)
    {
        mSceneFlashTimer ++;
    }

    //--Run timer.
    mSceneTimer ++;

    //--Crossfades.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        if(mPreviousFadeTimers[i] < WD_CHARACTER_CROSSFADE_TICKS)
        {
            mPreviousFadeTimers[i] ++;
        }

        //--All timers autocomplete when fading in.
        if(mIsMajorSequenceMode && mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
        {
            mPreviousFadeTimers[i] = WD_CHARACTER_CROSSFADE_TICKS;
        }
    }

    //--Scene transition timer.
    if(mIsSceneMode && mSceneTransitionTimer < mSceneFadeTimeMax)
    {
        mSceneTransitionTimer ++;
    }

    //--Major sequence timer.
    if(mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
    {
        mMajorSequenceTimer ++;
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            mPreviousFadeTimers[i] = WD_CHARACTER_CROSSFADE_TICKS;
        }
    }

    //--Visual Novel Update
    if(mIsVisualNovel)
    {
        UpdateVisualNovel();
    }

    //--In topics mode, pass the update off.
    if(mIsTopicsMode)
    {
        UpdateTopicsMode();
        DebugPop("WorldDialogue:Update - Ended from topics mode.\n");
        return;
    }

    ///--[Print Timer Handling]
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Iterate across the lines, update the timers.
    if(!mIsPrintingComplete)
    {
        RunTextTimers();
    }
    ///--[Player Input]
    //--Printing has been completed. Keypresses will advance the dialogue.
    else
    {
        //--Blocking state. The player must press the activate key to resume dialogue.
        if(mIsBlocking)
        {
            //--Debug.
            DebugPrint("In block state.\n");

            //--Player is pressing "Activate". In any-key mode, any key will do.
            bool tIsPressingActivate = rControlManager->IsFirstPress("Activate") || rControlManager->IsFirstPress("MouseLft");
            bool tIsHoldingActivate = rControlManager->IsDown("Activate") || rControlManager->IsDown("MouseLft");
            bool tIsPressingCancel = rControlManager->IsFirstPress("Cancel");
            bool tIsHoldingCancel = rControlManager->IsDown("Cancel");
            if(mIsAnyKeyMode)
            {
                tIsPressingActivate = rControlManager->IsAnyKeyPressed();
                tIsPressingCancel = rControlManager->IsAnyKeyPressed();
            }

            //--Soft block timer. Waits half a second, then advances the dialogue automatically.
            if(mIsSoftBlocking)
            {
                mSoftBlockTimer ++;
                if(tIsHoldingCancel) mSoftBlockTimer = 25;
            }
            else
            {
                mSoftBlockTimer = 0;
            }

            //--Special: If the player is holding down cancel and activate at the same time, we never wait for keypresses.
            if(tIsHoldingActivate && tIsHoldingCancel)
            {
                mSoftBlockTimer = 25;
            }

            //--Must be a new keypress, or the soft-block timer activating.
            if(tIsPressingActivate || tIsPressingCancel || mSoftBlockTimer >= 25)
            {
                //--Store the old pending dialogue.
                char *tOldPendingDialogue = mPendingDialogue;

                //--Clear the pointer to it in case AppendString() needs to use it again.
                mPendingDialogue = NULL;

                //--Re-append. Artificially increase the appendation stack so the emphasis does not change.
                //  Not that a [CLEAR] tag will override this.
                mIsBlocking = false;
                mIsSoftBlocking = false;
                mIsPausing = false;
                xAppendStack += 2;
                //float tStartTime = GetGameTime();
                //fprintf(stderr, "Appending dialogue. Length %i, time %f\n", (int)strlen(tOldPendingDialogue), tStartTime);
                AppendString(tOldPendingDialogue, false);
                //float tEndTime = GetGameTime();
                //fprintf(stderr, "Finished append. Time %f, took %f\n", tEndTime, tEndTime-tStartTime);
                xAppendStack -= 2;

                //--Clean up the old dialogue.
                free(tOldPendingDialogue);
            }
        }
        //--Decision state.
        else if(mIsDecisionMode)
        {
            DebugPrint("In decision state.\n");
            UpdateDecisions();
        }
        //--String Entry
        else if(mIsStringEntryMode)
        {
            DebugPrint("In string entry state.\n");
            if(mStringEntryForm)
            {
                //--Run update.
                mStringEntryForm->Update();

                //--Completion case.
                if(mStringEntryForm->IsComplete())
                {
                    //--Get the string and make a copy.
                    const char *rExecString = mStringEntryForm->GetString();
                    char *tCallOnCompleteCopy = InitializeString(mStringEntryCallOnComplete);

                    //--Something illegal happened, like entering a string with no letters.
                    if(!rExecString)
                    {
                        //--Clean. We need to do this before the lua call, in case that creates a new string entry.
                        mIsStringEntryMode = false;
                        free(mStringEntryCallOnComplete);
                        mStringEntryCallOnComplete = NULL;

                        LuaManager::Fetch()->ExecuteLuaFile(tCallOnCompleteCopy, 1, "S", "Null");
                        free(tCallOnCompleteCopy);
                        return;
                    }

                    //--Make a copy of the passed string.
                    char *tCopyString = InitializeString(rExecString);

                    //--Clean. We need to do this before the lua call, in case that creates a new string entry.
                    mIsStringEntryMode = false;
                    free(mStringEntryCallOnComplete);
                    mStringEntryCallOnComplete = NULL;

                    //--Execute.
                    LuaManager::Fetch()->ExecuteLuaFile(tCallOnCompleteCopy, 1, "S", tCopyString);

                    //--Clean up.
                    free(tCallOnCompleteCopy);
                    free(tCopyString);
                }
                //--Cancel case. Always passes "Null".
                else if(mStringEntryForm->IsCancelled())
                {
                    //--Make a copy of the call-on-complete string.
                    char *tCallOnCompleteCopy = InitializeString(mStringEntryCallOnComplete);

                    //--Clean up before execution, in case the lua execution starts a new string entry.
                    mIsStringEntryMode = false;
                    free(mStringEntryCallOnComplete);
                    mStringEntryCallOnComplete = NULL;

                    //--This passes "Null" to the script.
                    LuaManager::Fetch()->ExecuteLuaFile(tCallOnCompleteCopy, 1, "S", "Null");

                    //--Clean.
                    free(tCallOnCompleteCopy);
                }
            }
        }
        //--Activate topics mode if flagged.
        else if(mRunTopicsAfterDialogue)
        {
            DebugPrint("Activating topics.\n");
            mRunTopicsAfterDialogue = false;
            ActivateTopicsMode(mRunTopicsAfterDialogueName);
        }
        //--Normal case.
        else
        {
            //--Player is pressing "Activate". In any-key mode, any key will do.
            bool tIsPressingActivate = rControlManager->IsFirstPress("Activate") || rControlManager->IsFirstPress("MouseLft");
            bool tIsPressingCancel = rControlManager->IsFirstPress("Cancel");
            if(mIsAnyKeyMode)
            {
                tIsPressingActivate = rControlManager->IsAnyKeyPressed();
                tIsPressingCancel = rControlManager->IsAnyKeyPressed();
            }

            //--Pressing this closes the dialogue or fires the next part of the sequence.
            DebugPrint("Waiting for close.\n");
            if((tIsPressingActivate || tIsPressingCancel))
            {
                DebugPrint("Handling close.\n");
                HandleCloseOrContinue();
            }
            //--Special: Holding down both Activate and Cancel auto-proceeds.
            else if(rControlManager->IsDown("Activate") && rControlManager->IsDown("Cancel"))
            {
                DebugPrint("Handling close.\n");
                HandleCloseOrContinue();
            }
        }
    }

    //--Debug.
    DebugPop("WorldDialogue:Update - Ended normally.\n");
}
void WorldDialogue::RunTextTimers()
{
    ///--[Documentation]
    //--Runs across all the letters currently in the dialogue, and advances their timers by the increment
    //  value. The increment value varies based on the text speed and on keys held by the player.
    //--Text can play voice ticks when running, or not if an option is set.

    ///--[Setup]
    //--Fast-access Pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--Diagnostics.
    DebugPrint("Printing is incomplete.\n");

    //--Flags.
    bool tSilenceTicksNow = false;
    bool tAnyLettersFading = false;
    float cUseTickInterval = WD_TEXT_TICK_INTERVAL;

    ///--[Determine Text Speed]
    //--The "ticks" required for a letter to display is WD_LETTER_FADE_TICKS, which is actually (Ticks * 100) to
    //  allow for extra integer granularity. The default scroll rate is 1/4 or 100/400, but the options menu allows
    //  players to edit this. If the option does not exist then use the default rate.
    int tIncrementBase = 100;
    OptionPack *rTextSpeedPack = (OptionPack *)rOptionsManager->GetOption("Dialogue Speed");
    if(rTextSpeedPack)
    {
        //--Value.
        int tTextSpeedValue = (*(int *)(rTextSpeedPack->rPtr));

        //--Very slow.
        if(tTextSpeedValue == WD_TEXT_SPEED_VSLOW)
        {
            tIncrementBase = 33;
        }
        else if(tTextSpeedValue == WD_TEXT_SPEED_SLOW)
        {
            tIncrementBase = 66;
        }
        else if(tTextSpeedValue == WD_TEXT_SPEED_NORMAL)
        {
            tIncrementBase = 100;
        }
        else if(tTextSpeedValue == WD_TEXT_SPEED_FAST)
        {
            tIncrementBase = 150;
        }
        else if(tTextSpeedValue == WD_TEXT_SPEED_VFAST)
        {
            tIncrementBase = 225;
        }
    }

    //--Set the value to the base.
    int tIncrementBy = tIncrementBase;

    //--In addition, the player can hold down keys to adjust the rate that text displays.
    bool tIsHoldingActivate = rControlManager->IsDown("Activate") || rControlManager->IsDown("MouseLft");
    bool tIsHoldingCancel = rControlManager->IsDown("Cancel");
    if(mIsAnyKeyMode)
    {
        tIsHoldingActivate = rControlManager->IsAnyKeyDown();
    }

    //--If the player is holding down the Activate key, text scrolls faster.
    if(tIsHoldingActivate || mAutoHastenDialogue)
    {
        tSilenceTicksNow = true;
        tIncrementBy = tIncrementBase * 3;
        cUseTickInterval = cUseTickInterval * WD_TEXT_TICK_FACTOR;
    }
    //--All keys hasten.
    else if(mAllKeysHasten && rControlManager->IsAnyKeyDown())
    {
        tSilenceTicksNow = true;
        tIncrementBy = tIncrementBase * 3;
        cUseTickInterval = cUseTickInterval * WD_TEXT_TICK_FACTOR;
    }

    //--If the player is holding down the Cancel key, text scrolls super-fast!
    if(tIsHoldingCancel)
    {
        tSilenceTicksNow = true;
        tIncrementBy = tIncrementBase * 10;
        cUseTickInterval = cUseTickInterval * WD_TEXT_TICK_FACTOR * 3.0f;
    }

    //--Instant text scroll.
    if(mInstantTextMode)
    {
        tSilenceTicksNow = true;
        tIncrementBy = 10000000;
    }

    ///--[Recheck Silence Flag]
    //--If this flag is set to false in the options manager, then never silence text ticks even if accelerating.
    if(rOptionsManager->GetOptionB("Disable Voice During Accel") == false) tSilenceTicksNow = false;

    ///--[Advance Text Timers]
    //--Resolve the active dialogue list.
    StarLinkedList *rActiveList = ResolveActiveDialogueList();

    //--Begin iterating.
    DebugPrint("Iterating.\n");
    StarLinkedList *rLetterList = (StarLinkedList *)rActiveList->PushIterator();
    while(rLetterList)
    {
        //--Iterate.
        DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
        while(rCharacterPack)
        {
            //--Does this character need to be updated?
            if(rCharacterPack->mTimer < WD_LETTER_FADE_TICKS)
            {
                //--Timer.
                rCharacterPack->mTimer += tIncrementBy;

                //--Recompute the alpha.
                if(rCharacterPack->mTimer >= 0 && rCharacterPack->mTimer < WD_LETTER_FADE_TICKS)
                {
                    //--Text tick handling.
                    if(!tAnyLettersFading)
                    {
                        //--Timer.
                        mTextTickTimer = mTextTickTimer + 1.0f;

                        //--Sound effect.
                        if(mTextTickTimer >= cUseTickInterval && !mIsSilenced && !tSilenceTicksNow)
                        {
                            if(cUseTickInterval > 0.0f) mTextTickTimer = fmodf(mTextTickTimer, cUseTickInterval);

                            //--If there's no voice, use the default tick.
                            if(!rCurrentVoice)
                            {
                                if(!IsFlashSuppressingTicks()) AudioManager::Fetch()->PlaySound(mNormalTextTick);
                            }
                            //--Use the character's voice.
                            else
                            {
                                if(!IsFlashSuppressingTicks()) AudioManager::Fetch()->PlaySound(rCurrentVoice);
                            }
                        }
                    }

                    //--Flags.
                    tAnyLettersFading = true;
                    rCharacterPack->mAlpha = rCharacterPack->mTimer / (float)WD_LETTER_FADE_TICKS;
                }
                //--Clamp alpha at 1.0f.
                else if(rCharacterPack->mTimer >= WD_LETTER_FADE_TICKS)
                {
                    rCharacterPack->mAlpha = 1.0f;
                }
            }

            //--Next letter.
            rCharacterPack = (DialogueCharacter *)rLetterList->AutoIterate();
        }

        //--Next line.
        rLetterList = (StarLinkedList *)rActiveList->AutoIterate();
    }

    //--If no letters were fading, then fading is complete.
    if(!tAnyLettersFading)
    {
        mIsPrintingComplete = true;
        mPrintTimeRemainder = 0;
    }
}
void WorldDialogue::HandleCloseOrContinue()
{
    ///--[Documentation]
    //--When the dialogue runs out of... dialogue... this function is called. If there is no more
    //  dialogue pending (the continuation strings are NULL) then this object closes. Otherwise,
    //  the continuation strings are used to run another script and presumably either generate
    //  more dialogue or fire an event sequence.
    if(!mContinuationScript || !mContinuationString)
    {
        //--Clear any dialogue locks.
        AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
        if(rFetchLevel) rFetchLevel->RemoveLockout(WD_STANDARD_LOCKOUT);

        //--Run the hiding sequence.
        Hide();
        return;
    }

    //--If we got this far, then there is both a continuation path and string. First, we need to
    //  copy the reference pointer since the new script will probably modify the continuation strings.
    char *rRefCopyScript = mContinuationScript;
    char *rRefCopyString = mContinuationString;
    mContinuationScript = NULL;
    mContinuationString = NULL;

    //--Now run the script in question.
    LuaManager::Fetch()->ExecuteLuaFile(rRefCopyScript, 1, "S", rRefCopyString);

    //--Clean up.
    free(rRefCopyScript);
    free(rRefCopyString);
}
void WorldDialogue::CloseImmediately()
{
    //--Closes the dialogue immediately. Used between decision handlers to prevent crossfades.
    if(!mContinuationScript || !mContinuationString)
    {
        //--Clear any dialogue locks.
        AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
        if(rFetchLevel) rFetchLevel->RemoveLockout(WD_STANDARD_LOCKOUT);

        //--Run the hiding sequence.
        mHidingTimer = -1;
        mVisibilityState = WD_INVISIBLE;
        Wipe();
        DeactivateScenesMode();
        return;
    }

    //--If we got this far, then there is both a continuation path and string. First, we need to
    //  copy the reference pointer since the new script will probably modify the continuation strings.
    char *rRefCopyScript = mContinuationScript;
    char *rRefCopyString = mContinuationString;
    mContinuationScript = NULL;
    mContinuationString = NULL;

    //--Now run the script in question.
    LuaManager::Fetch()->ExecuteLuaFile(rRefCopyScript, 1, "S", rRefCopyString);

    //--Clean up.
    free(rRefCopyScript);
    free(rRefCopyString);
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void WorldDialogue::Render()
{
    //--Rendering cycle. Does nothing if the object is not visible in any way, or the images did not resolve.
    if(!Images.mIsReady || mVisibilityState == WD_INVISIBLE) return;

    //--Debug.
    DebugPush(true, "WorldDialogue:Render - Begin.\n");

    //--Subsequences.
    if(mIsCreditsSequence)
    {
        RenderCredits();
        DebugPop("WorldDialogue:Render - Ended via credits.\n");
        return;
    }
    if(mIsMajorSequenceMode || (mIsTopicsMode && mDecisionUsesMouse))
    {
        RenderMajorSequence();
        DebugPop("WorldDialogue:Render - Ended via major sequence.\n");
        return;
    }
    if(mIsSceneMode)
    {
        RenderScenesMode();
        DebugPop("WorldDialogue:Render - Ended via scenes.\n");
        return;
    }
    if(mIsTopicsMode && !mDecisionUsesMouse)
    {
        RenderTopicsMode();
        DebugPop("WorldDialogue:Render - Ended via topics.\n");
        return;
    }

    //--Mixing.
    float tAlphaFactor = 1.0f;
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaFactor);

    //--Positioning.
    float tCursorX = 0.0f;
    float tCursorY = 0.0f;

    //--If there is a speaker:
    bool tIsSpeakerCase = (mMajorSequenceSpeaker && strcasecmp(mMajorSequenceSpeaker, "Null") && strcasecmp(mMajorSequenceSpeaker, "Narrator") && strcasecmp(mMajorSequenceSpeaker, "Thought"));
    if(tIsSpeakerCase)
    {
        //--Render.
        Images.Data.rNameBox->Draw();
        Images.Data.rNamePanel->Draw();
        Images.Data.rHeadingFont->DrawText(264.0f, 563.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, mMajorSequenceSpeaker);

        //--Cursor position.
        tCursorX =  42.0f;
        tCursorY = 600.0f;
    }
    //--No speaker.
    else
    {
        //--Render.
        Images.Data.rNamelessBox->Draw();

        //--Cursor position.
        tCursorX =  42.0f;
        tCursorY = 600.0f;
    }

    //--Subroutine renders the letters themselves.
    RenderOnlyText(tCursorX, tCursorY, tAlphaFactor);

    ///--[Decisions]
    //--Renders over everything else, but only when active.
    if(mIsDecisionMode && !mIsBlocking && mIsPrintingComplete) RenderDecisions();

    ///--[String Entry]
    if(mIsStringEntryMode && mStringEntryForm && mIsPrintingComplete) mStringEntryForm->Render();

    ///--[Flash]
    RenderFlash();
    DebugPop("WorldDialogue:Render - Ended normally.\n");
}
void WorldDialogue::RenderOnlyText(float pX, float pY, float pAlphaFactor)
{
    ///--[ ===================== Documentation ==================== ]
    //--Subroutine that only renders the text using the provided offsets. This is used both internally and for the
    //  TextLevel handlers since they use the same inputs.

    //--Reposition the rendering cursor for the dialogue.
    glTranslatef(pX, pY, 0.0f);

    ///--[ ================= Iterate Across Lines ================= ]
    //--Now start rendering.
    StarLinkedList *rLetterList = (StarLinkedList *)mDialogueList->PushIterator();
    while(rLetterList)
    {
        //--Offset value. Resets for each line.
        float tLineCursor = 0.0f;

        ///--[ ==== Iterate Across Letters ==== ]
        DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
        while(rCharacterPack)
        {
            ///--[Color Resolve]
            //--Spooky characters change color dynamically with a timer.
            if(rCharacterPack->mIsSpooky)
            {
                if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 2)
                {
                    StarlightColor::SetMixer(1.0f, 0.4f, 0.2f, rCharacterPack->mAlpha * pAlphaFactor);
                }
                else if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 4)
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 0.1f, rCharacterPack->mAlpha * pAlphaFactor);
                }
                else
                {
                    StarlightColor::SetMixer(1.0f, 0.2f, 0.5f, rCharacterPack->mAlpha * pAlphaFactor);
                }
            }
            //--Fixed color.
            else if(rCharacterPack->rColor)
            {
                StarlightColor::SetMixer(rCharacterPack->rColor->r, rCharacterPack->rColor->g, rCharacterPack->rColor->b, rCharacterPack->mAlpha * pAlphaFactor);
            }
            //--White.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, rCharacterPack->mAlpha * pAlphaFactor);
            }

            ///--[Size/Position Resolve]
            //--Store original size.
            float tOrigScale = mTextScale;

            //--Text is "Large". This makes it render at double size, and doubles the advance rate.
            if(rCharacterPack->mIsLarge)
            {
                mTextScale = mTextScale * 2.0f;
            }
            //--Text is "Small". Renders at half size, halves advance rate.
            else if(rCharacterPack->mIsSmall)
            {
                mTextScale = mTextScale * 0.5f;
            }

            ///--[Rendering]
            //--Next letter. It may be NULL. This also does the work of iterating.
            DialogueCharacter *rNextLetterPack = (DialogueCharacter *)rLetterList->AutoIterate();

            //--Render the letter.
            char tNextLetter = '\0';
            if(rNextLetterPack) tNextLetter = rNextLetterPack->mLetter;
            float tAdvance = Images.Data.rDialogueFont->DrawLetter(tLineCursor, 0.0f, 0, mTextScale, rCharacterPack->mLetter, tNextLetter);

            ///--[Next]
            //--Move the cursor over.
            tLineCursor = tLineCursor + (tAdvance * mTextScale);

            //--Reset the scale.
            mTextScale = tOrigScale;

            //--Get the next letter.
            rCharacterPack = rNextLetterPack;
        }

        //--Move the cursor down.
        pY = pY + (Images.Data.rDialogueFont->GetTextHeight() * mTextScale);
        glTranslatef(0.0f, (Images.Data.rDialogueFont->GetTextHeight() * mTextScale), 0.0f);

        //--Next line.
        rLetterList = (StarLinkedList *)mDialogueList->AutoIterate();
    }

    //--Clean up.
    StarlightColor::ClearMixer();
    glTranslatef(pX * -1, pY * -1, 0.0f);
}
bool WorldDialogue::IsFlashSuppressingTicks()
{
    //--Not if we're not flashing!
    if(!mIsSceneFlash) return false;

    //--Constants
    int cSeq1Timer = 10;
    int cSeq2Timer = cSeq1Timer + 60;

    //--Checks.
    if(mSceneFlashTimer <= cSeq2Timer - 40) return false;
    if(mSceneFlashTimer <= cSeq2Timer - 40 + 240) return true;
    return false;
}
void WorldDialogue::RenderFlash()
{
    //--Flashes white. Used for rune animations.
    if(!mIsSceneFlash) return;

    //--Hasn't started yet.
    if(mSceneFlashTimer < 0) return;

    //--Constants
    int cSeq1Timer = 10;
    int cSeq2Timer = cSeq1Timer + 60;
    int cSeq3Timer = cSeq2Timer + 5;
    int cSeq4Timer = cSeq3Timer + 120;
    int cSeq5Timer = cSeq4Timer + 120;

    //--SFX.
    if(mSceneFlashTimer == cSeq2Timer - 40) AudioManager::Fetch()->PlaySound("World|Flash");

    //--Ramps.
    float cRamp0 = 1.0f;
    float cRamp1 = 3.0f;
    float cRamp2 = 5.0f;
    float cRamp3 = CANY - cRamp2 - cRamp1 - cRamp0;

    //--Position.
    float cYMid = CANY * 0.50f;

    //--Disable textures.
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);

    //--Initial.
    if(mSceneFlashTimer < cSeq1Timer)
    {
        //--Compute height.
        float cPercent = EasingFunction::QuadraticIn(mSceneFlashTimer, cSeq1Timer);
        float cHeight = cRamp0 + (cRamp1 * cPercent);

        //--Render top.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(0.0f, cYMid - cHeight);
        glVertex2f(CANX, cYMid - cHeight);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(CANX, cYMid + 0.0f);
        glVertex2f(0.0f, cYMid + 0.0f);

        //--Render bottom.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(0.0f, cYMid - 0.0f);
        glVertex2f(CANX, cYMid - 0.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(CANX, cYMid + cHeight);
        glVertex2f(0.0f, cYMid + cHeight);
    }
    //--Minor ramp up.
    else if(mSceneFlashTimer < cSeq2Timer)
    {

        //--Compute height.
        float cPercent = EasingFunction::QuadraticOut(mSceneFlashTimer - cSeq1Timer, cSeq2Timer - cSeq1Timer);
        float cHeight = (cRamp0 + cRamp1) + (cRamp2 * cPercent);

        //--Render top.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(0.0f, cYMid - cHeight);
        glVertex2f(CANX, cYMid - cHeight);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(CANX, cYMid + 0.0f);
        glVertex2f(0.0f, cYMid + 0.0f);

        //--Render bottom.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(0.0f, cYMid - 0.0f);
        glVertex2f(CANX, cYMid - 0.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(CANX, cYMid + cHeight);
        glVertex2f(0.0f, cYMid + cHeight);
    }
    //--Rapid flash up.
    else if(mSceneFlashTimer < cSeq3Timer)
    {

        //--Compute height.
        float cPercent = EasingFunction::QuadraticIn(mSceneFlashTimer - cSeq2Timer, cSeq3Timer - cSeq2Timer);
        float cHeight = (cRamp0 + cRamp1 + cRamp2) + (cRamp3* cPercent);

        //--Render top.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(0.0f, cYMid - cHeight);
        glVertex2f(CANX, cYMid - cHeight);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(CANX, cYMid + 0.0f);
        glVertex2f(0.0f, cYMid + 0.0f);

        //--Render bottom.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(0.0f, cYMid - 0.0f);
        glVertex2f(CANX, cYMid - 0.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(CANX, cYMid + cHeight);
        glVertex2f(0.0f, cYMid + cHeight);
    }
    //--Fullwhite.
    else if(mSceneFlashTimer < cSeq4Timer)
    {
        glVertex2f(0.0f, 0.0f);
        glVertex2f(CANX, 0.0f);
        glVertex2f(CANX, CANY);
        glVertex2f(0.0f, CANY);
    }
    //--Fades down.
    else if(mSceneFlashTimer < cSeq5Timer)
    {
        float cPercent = 1.0f - EasingFunction::QuadraticIn(mSceneFlashTimer - cSeq4Timer, cSeq5Timer - cSeq4Timer);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent);
        glVertex2f(0.0f, 0.0f);
        glVertex2f(CANX, 0.0f);
        glVertex2f(CANX, CANY);
        glVertex2f(0.0f, CANY);
    }
    //--Cancel.
    else
    {
        mIsSceneFlash = false;
        AllocateSceneAnimFrames(0, 1, 0);
    }

    //--Clean.
    glEnd();
    glEnable(GL_TEXTURE_2D);
}
void WorldDialogue::RenderDiagnostics()
{
    ///--[Documentation]
    //--Only print diagnostics if there's something in the dialogue.
    int tLines = mDialogueList->GetListSize();
    if(tLines < 1) return;

    //--Heading.
    DebugManager::RenderVisualTrace("WDIA %i", tLines);

    ///--[Visible Lines]
    //--For each line:
    int i = 0;
    char tBuf[256];
    StarLinkedList *rLetterList = (StarLinkedList *)mDialogueList->PushIterator();
    while(rLetterList)
    {
        //--Offset value. Resets for each line.
        //float tLineCursor = 0.0f;

        ///--[ ==== Iterate Across Letters ==== ]
        DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
        while(rCharacterPack)
        {
            //--Append.
            tBuf[i] = rCharacterPack->mLetter;
            i ++;

            //--Cap.
            if(i >= 250)
            {
                rLetterList->PopIterator();
                break;
            }

            //--Next.
            rCharacterPack = (DialogueCharacter *)rLetterList->AutoIterate();
        }

        //--Finish.
        tBuf[i] = '\0';
        DebugManager::RenderVisualTrace("%s", tBuf);

        //--Next.
        tBuf[0] = '\0';
        i = 0;
        rLetterList = (StarLinkedList *)mDialogueList->AutoIterate();
    }

    ///--[Non-Visible Lines]
    if(mPendingDialogue)
    {
        DebugManager::RenderVisualTrace("PEN %s", mPendingDialogue);
    }
}

///====================================== Pointer Routing =========================================
DialogueActor *WorldDialogue::GetActor(const char *pName)
{
    return (DialogueActor *)mActorBench->GetElementByName(pName);
}
StarLinkedList *WorldDialogue::GetTopicList()
{
    return mTopicListing;
}

///===================================== Static Functions =========================================
WorldDialogue *WorldDialogue::Fetch()
{
    //--Cannot legally return NULL.
    return MapManager::Fetch()->GetWorldDialogue();
}

