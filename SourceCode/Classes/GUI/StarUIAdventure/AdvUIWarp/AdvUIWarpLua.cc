//--Base
#include "AdvUIWarp.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

///======================================== Lua Hooking ===========================================
//void AdvUIWarp::HookToLuaState(lua_State *pLuaState)
//{
//}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AdvUIWarp_SetProperty(bool &sHandled, lua_State *L)
{
    ///--[Documentation]
    //--Called from AdventureMenuLua's AM_SetProperty(). Handles routing to warp routines. If this routine
    //  caught the lua call, sHandled will be set to true.
    //--If there is an error, the base call will stop anyway. sHandled will be marked as true.
    sHandled = true;

    ///--[Argument List]
    //AM_SetProperty("Register Warp Region", sRegionName, sIconPath, sMapPath, sMapOverlayPath)
    //AM_SetProperty("Set Warp Region Alignments", sRegionName, fXOff, fYOff, fLft, fTop, fWid, fHei)
    //AM_SetProperty("Set Warp Region Advanced Clamps", sRegionName, fClampLft, fClampTop, fClampRgt, fClampBot)
    //AM_SetProperty("Register Warp Location", sRegionName, sDisplayName, sActualName, fImgX, fImgY)
    //AM_SetProperty("Register Warp Region Advanced Pack", sRegionName, sLayerName, sImagePath)
    //AM_SetProperty("Warp Region Advanced Render Chance", sRegionName, sLayerName, iRenderLo, iRenderHi)
    //AM_SetProperty("Warp Region Advanced Render Reads Stencil", sRegionName, sLayerName, iStencilValue)
    //AM_SetProperty("Warp Region Advanced Render Writes Stencil", sRegionName, sLayerName, iStencilValue)
    //AM_SetProperty("Warp Region Advanced Render Can Toggle", sRegionName, sLayerName, bCanToggle)

    ///--[Arg Resolve]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Resolve Object]
    //--Dynamic. Get the AdventureMenu which holds the AdvUIWarp object.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetProperty", L);

    //--Get the internal warp UI.
    AdvUIWarp *rWarpUI = rMenu->GetWarpUI();
    if(!rWarpUI) return LuaTypeError("AM_SetProperty", L);

    ///--[Warp Handlers]
    //--Creates a new warp region.
    if(!strcasecmp(rSwitchType, "Register Warp Region") && tArgs == 5)
    {
        rWarpUI->RegisterWarpRegion(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5));
    }
    //--Sets image alignments for an existing warp region.
    else if(!strcasecmp(rSwitchType, "Set Warp Region Alignments") && tArgs == 8)
    {
        rWarpUI->SetWarpRegionAlignments(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
    }
    //--Sets the max clamp positions for an advanced region renderer.
    else if(!strcasecmp(rSwitchType, "Set Warp Region Advanced Clamps") && tArgs == 6)
    {
        rWarpUI->SetWarpRegionAdvancedClamps(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Creates a new location within the specified region. This is both the room name and its location on the map.
    else if(!strcasecmp(rSwitchType, "Register Warp Location") && tArgs == 6)
    {
        rWarpUI->RegisterWarpLocation(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Creates a package for advanced map rendering.
    else if(!strcasecmp(rSwitchType, "Register Warp Region Advanced Pack") && tArgs == 4)
    {
        rWarpUI->RegisterWarpRegionAdvancedPack(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Chance of a warp region layer rendering.
    else if(!strcasecmp(rSwitchType, "Warp Region Advanced Render Chance") && tArgs == 5)
    {
        rWarpUI->SetWarpRegionPackRenderChance(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--Layer reads stencils when rendering.
    else if(!strcasecmp(rSwitchType, "Warp Region Advanced Render Reads Stencil") && tArgs == 4)
    {
        rWarpUI->SetWarpRegionPackReadsStencil(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Layer writes stencils when rendering.
    else if(!strcasecmp(rSwitchType, "Warp Region Advanced Render Writes Stencil") && tArgs == 4)
    {
        rWarpUI->SetWarpRegionPackWritesStencil(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Layer can be toggled on and off.
    else if(!strcasecmp(rSwitchType, "Warp Region Advanced Render Can Toggle") && tArgs == 4)
    {
        rWarpUI->SetWarpRegionPackCanToggle(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4));
    }
    ///--[Not Found]
    //--This routine did not catch the input, set the handled flag to false.
    else
    {
        sHandled = false;
    }

    ///--[Finish]
    return 0;
}
