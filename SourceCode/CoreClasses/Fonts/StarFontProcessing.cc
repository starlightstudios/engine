//--Base
#include "StarFont.h"

//--Classes
//--CoreClasses
//--Definitions
#include "utf8.h"

//--Libraries
//--Managers

///--[Freetype Only]
#if defined _FONTTYPE_FREETYPE_

///--[Debug]
//#define SUGARFONT_FREETYPE_DEBUG
#ifdef SUGARFONT_FREETYPE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///====================================== Normal Function =========================================
int StarFont::GetTextWidth(const char *pString)
{
    ///--[Documentation]
    //--Returns how wide the given string would be on screen, in pixels. Note that this does not
    //  take font scaling into account. If a coefficient has been applied, multiply this result
    //  by that coefficient to get the final pixel size.
    //--Normal version.
    if(!mIsReady || !pString) return 1;

    ///--[Bitmap Font]
    if(mIsBitmapMode) return GetBitmapTextWidth(pString);

    //--One-letter.
    if(pString[0] == '\0') return 0;

    //--Setup.
    float tRunningOffset = 0.0f;

    ///--[Debug]
    DebugPush(false, "Getting text width: %s\n", pString);

    ///--[Precached Case]
    //--Precached data runs *considerably* faster since the advances are already stored.
    if(mPrecacheData)
    {
        //--Setup.
        int tIndex = 0;
        int tLen = utf8_strlen(pString);

        //--Iterate until we're out of letters.
        DebugPrint(" Checking precached length. Total glyphs: %i\n", mTotalGlyphs);
        DebugPrint(" Length %i Bytes %i\n", tLen, (int)strlen(pString));
        int tLetter = utf8_nextletter(pString, tIndex);
        for(int i = 0; i < tLen - 1; i ++)
        {
            //--Get next letter.
            int tNextLetter = utf8_nextletter(pString, tIndex);
            DebugPrint(" Letter %i: %i %c, Next %i %c\n", i, tLetter, tLetter, tNextLetter, tNextLetter);

            //--Get the associated glyph slot.
            int tGlyphLft = GetGlyphFromRemap(tLetter);
            DebugPrint(" Glyph Index: %i\n", tGlyphLft);

            //--Glyph was -1, meaning this letter does not exist in this font.
            if(tGlyphLft == -1 || tGlyphLft >= mTotalGlyphs)
            {

            }
            else if(tLetter == 65306)
            {
                tRunningOffset = tRunningOffset + mMonospacing;
            }
            //--If the next letter is within the precache range:
            else if(tNextLetter >= SUGARFONT_PRECACHE_OFF && tNextLetter < SUGARFONT_PRECACHE_HI)
            {
                tRunningOffset = tRunningOffset + mPrecacheData[tGlyphLft].mKerningDistance[tNextLetter-SUGARFONT_PRECACHE_OFF];
                DebugPrint("  Precache was %f\n", mPrecacheData[tGlyphLft].mKerningDistance[tNextLetter-SUGARFONT_PRECACHE_OFF]);
            }
            //--If monospacing is active:
            else if(mMonospacing != -1.0f)
            {
                tRunningOffset = tRunningOffset + mMonospacing;
            }
            //--Not in range. Used the letter's width.
            else
            {
                tRunningOffset = tRunningOffset + mPrecacheData[tGlyphLft].mWidth;
            }
            DebugPrint("  Offset: %f\n", tRunningOffset);

            //--Next.
            tLetter = tNextLetter;
        }

        //--Last letter. Advance distance is just the width of the letter. Range check it.
        if(true)
        {
            //--Get remap.
            int tGlyphLft = GetGlyphFromRemap(tLetter);
            if(tGlyphLft > -1)
            {
                tRunningOffset = tRunningOffset + mPrecacheData[tGlyphLft].mAdvanceX;
            }
            else
            {
                tRunningOffset = tRunningOffset + 8.0f;
            }
        }
    }
    ///--[Non-Precached Case]
    //--Non-precached data is a lot slower. Don't use this unless you're experimenting.
    else if(rFont)
    {
        //--Setup.
        int tIndex = 0;
        int tLen = utf8_strlen(pString);

        //--Iterate until we're out of letters.
        DebugPrint(" Checking precached length. Total glyphs: %i\n", mTotalGlyphs);
        DebugPrint(" Length %i Bytes %i\n", tLen, (int)strlen(pString));
        int tLetter = utf8_nextletter(pString, tIndex);
        for(int i = 0; i < tLen - 1; i ++)
        {
            //--Get next letter.
            int tNextLetter = utf8_nextletter(pString, tIndex);
            DebugPrint(" Letter %i: %i %c, Next %i %c\n", i, tLetter, tLetter, tNextLetter, tNextLetter);

            //--Get the Glyph.  If it doesn't exist, 0 comes back.
            uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, tLetter);
            if(!tGlyphIndex) continue;

            FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
            FT_GlyphSlot rGlyph = rFont->glyph;

            tRunningOffset = tRunningOffset + (float)(rGlyph->advance.x >> 6);
            DebugPrint("  Offset: %f\n", tRunningOffset);

            //--Next.
            tLetter = tNextLetter;
        }

        //--Last letter. Advance distance is just the width of the letter. Range check it.
        if(true)
        {
            //--Get the Glyph.  If it doesn't exist, 0 comes back.
            uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, tLetter);
            if(!tGlyphIndex) return tRunningOffset;

            FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
            FT_GlyphSlot rGlyph = rFont->glyph;

            tRunningOffset = tRunningOffset + (float)(rGlyph->advance.x >> 6);
        }
    }

    //--Debug.
    DebugPop("Finished text width check. Was %f\n", tRunningOffset);

    //--Pass the result back.
    return tRunningOffset;
}

///=================================== Diagnostics Function =======================================
int StarFont::GetTextWidthDiagnostic(const char *pString)
{
    ///--[Documentation]
    //--Returns how wide the given string would be on screen, in pixels. Note that this does not
    //  take font scaling into account. If a coefficient has been applied, multiply this result
    //  by that coefficient to get the final pixel size.
    //--Diagnostics version, explicitly prints out sizings.
    if(!mIsReady || !pString) return 1;

    ///--[Bitmap Font]
    if(mIsBitmapMode) return GetBitmapTextWidth(pString);

    //--One-letter.
    if(pString[0] == '\0') return 0;

    //--Setup.
    float tRunningOffset = 0.0f;

    ///--[Debug]
    DebugPush(false, "Getting text width: %s\n", pString);

    ///--[Precached Case]
    //--Precached data runs *considerably* faster since the advances are already stored.
    fprintf(stderr, "Starting computation.\n");
    if(mPrecacheData)
    {
        //--Setup.
        int tIndex = 0;
        int tLen = utf8_strlen(pString);

        //--Iterate until we're out of letters.
        DebugPrint(" Checking precached length. Total glyphs: %i\n", mTotalGlyphs);
        DebugPrint(" Length %i Bytes %i\n", tLen, (int)strlen(pString));
        int tLetter = utf8_nextletter(pString, tIndex);
        for(int i = 0; i < tLen - 1; i ++)
        {
            //--Get next letter.
            int tNextLetter = utf8_nextletter(pString, tIndex);
            DebugPrint(" Letter %i: %i %c, Next %i %c\n", i, tLetter, tLetter, tNextLetter, tNextLetter);

            //--Get the associated glyph slot.
            int tGlyphLft = GetGlyphFromRemap(tLetter);
            DebugPrint(" Glyph Index: %i\n", tGlyphLft);

            //--Glyph was -1, meaning this letter does not exist in this font.
            if(tGlyphLft == -1 || tGlyphLft >= mTotalGlyphs)
            {
                fprintf(stderr, "Glyph out of range. XXX ");
            }
            else if(tLetter == 65306)
            {
                tRunningOffset = tRunningOffset + mMonospacing;
                fprintf(stderr, "Glyph is full-width colon. XXX ");
            }
            //--If the next letter is within the precache range:
            else if(tNextLetter >= SUGARFONT_PRECACHE_OFF && tNextLetter < SUGARFONT_PRECACHE_HI)
            {
                tRunningOffset = tRunningOffset + mPrecacheData[tGlyphLft].mKerningDistance[tNextLetter-SUGARFONT_PRECACHE_OFF];
                DebugPrint("  Precache was %f\n", mPrecacheData[tGlyphLft].mKerningDistance[tNextLetter-SUGARFONT_PRECACHE_OFF]);
                fprintf(stderr, "Glyph is within precache range. XXX ");
            }
            //--If monospacing is active:
            else if(mMonospacing != -1.0f)
            {
                tRunningOffset = tRunningOffset + mMonospacing;
                fprintf(stderr, "Glyph is monospaces. XXX ");
            }
            //--Not in range. Used the letter's width.
            else
            {
                tRunningOffset = tRunningOffset + mPrecacheData[tGlyphLft].mWidth;
                fprintf(stderr, "Glyph is using text default width. XXX ");
            }
            fprintf(stderr, "Offset %i is %i, %i, total glyphs %i\n", i, (int)tRunningOffset, tGlyphLft, mTotalGlyphs);
            DebugPrint("  Offset: %f\n", tRunningOffset);

            //--Next.
            tLetter = tNextLetter;
        }

        //--Last letter. Advance distance is just the width of the letter. Range check it.
        if(true)
        {
            //--Get remap.
            int tGlyphLft = GetGlyphFromRemap(tLetter);
            if(tGlyphLft > -1)
            {
                tRunningOffset = tRunningOffset + mPrecacheData[tGlyphLft].mAdvanceX;
            }
            else
            {
                tRunningOffset = tRunningOffset + 8.0f;
            }
        }
    }
    ///--[Non-Precached Case]
    //--Non-precached data is a lot slower. Don't use this unless you're experimenting.
    else if(rFont)
    {
        //--Setup.
        int tIndex = 0;
        int tLen = utf8_strlen(pString);

        //--Iterate until we're out of letters.
        DebugPrint(" Checking precached length. Total glyphs: %i\n", mTotalGlyphs);
        DebugPrint(" Length %i Bytes %i\n", tLen, (int)strlen(pString));
        int tLetter = utf8_nextletter(pString, tIndex);
        for(int i = 0; i < tLen - 1; i ++)
        {
            //--Get next letter.
            int tNextLetter = utf8_nextletter(pString, tIndex);
            DebugPrint(" Letter %i: %i %c, Next %i %c\n", i, tLetter, tLetter, tNextLetter, tNextLetter);

            //--Get the Glyph.  If it doesn't exist, 0 comes back.
            uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, tLetter);
            if(!tGlyphIndex) continue;

            FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
            FT_GlyphSlot rGlyph = rFont->glyph;

            tRunningOffset = tRunningOffset + (float)(rGlyph->advance.x >> 6);
            DebugPrint("  Offset: %f\n", tRunningOffset);

            //--Next.
            tLetter = tNextLetter;
        }

        //--Last letter. Advance distance is just the width of the letter. Range check it.
        if(true)
        {
            //--Get the Glyph.  If it doesn't exist, 0 comes back.
            uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, tLetter);
            if(!tGlyphIndex) return tRunningOffset;

            FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
            FT_GlyphSlot rGlyph = rFont->glyph;

            tRunningOffset = tRunningOffset + (float)(rGlyph->advance.x >> 6);
        }
    }

    //--Debug.
    DebugPop("Finished text width check. Was %f\n", tRunningOffset);

    //--Pass the result back.
    return tRunningOffset;
}

#endif
