-- |[ =============================== SysDiagnostics Class File ================================ ]|
--Class file for SysDiagnostics. Call this once at initialization.

--Class used for diagnostics of other classes.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
SysDiagnostics = {}
SysDiagnostics.__index = SysDiagnostics 

-- |[ ============ Class Methods ============= ]|
--System
--Arguments
function SysDiagnostics:fnPrintArgError(psFunctionName) end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "SysDiagnosticsArguments.lua")
