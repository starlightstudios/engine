============================================== Readme =============================================
Witch Hunter Izana

Primary website: https://bottledstarlight.com
Contact email: Pandemonium AT starlightstudios DAWT org

--[[ Common System Questions ]]--
1) Where are the saves and my configuration stored?
   
   Your savefiles are stored in the Saves/ folder locally. Your control configurations are also saved
   there. Engine configurations are stored in Configuration.lua.

2) What are the different executables for?
 
   These are here for players using older hardware. Sometimes, Allegro or SDL are unable to acquire
   the screen due to driver issues and the game cannot start. If this happens, use one of the 
   other executables and hope that the issue is fixed. Most players report that one or the other
   executable works, and both are otherwise identical.
   The SDLFMOD executable uses FMOD instead of Bass for audio, and should only be used if the
   program's audio fails to boot.

3) Can I transfer saves from the demo?

   You can! Copy the save files from the demo's saves directory into the main game's directory.

4) What's with this 'Adventure' stuff and the game's weird directory structure?

   The game technically runs as a standalone mod on top of Runes of Pandemonium. Some configuration
   and game data is borrowed from the base game.

--[[ Version History ]]
Please check the changelog file :3