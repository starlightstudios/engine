//--Base
#include "StarlightSocket.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "NetworkManager.h"

//--Default, no-network version of the socket handler. All functions do nothing.
#if defined _NETWORK_NONE_

//=========================================== System ==============================================
StarlightSocket::StarlightSocket()
{
}
StarlightSocket::~StarlightSocket()
{
}
void StarlightSocket::StartSocket()
{
}
void StarlightSocket::DisconnectSocket()
{
}
void StarlightSocket::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((StarlightSocket *)pPtr);
}
void StarlightSocket::TakeDownNetwork()
{
}

//====================================== Property Queries =========================================
bool StarlightSocket::IsSocketOpen()
{
    return false;
}
int StarlightSocket::GetConnectionsTotal()
{
    return 0;
}

//========================================= Manipulators ==========================================
void StarlightSocket::StartServerOn(int pPort)
{
}
void StarlightSocket::ConnectTo(const char *pAddress, int pPort)
{
}
void StarlightSocket::ForgetClient()
{
}
void StarlightSocket::SetConnecteeName(const char *pName)
{
}
void StarlightSocket::SetIdentifyTimer(int pTimer)
{
}
void StarlightSocket::SetPacketHandler(StarlightPacketHandler pNewHandler)
{
}

//========================================= Core Methods ==========================================
StarlightPacket StarlightSocket::PrefabPacket(int pType, void *pDataBuffer, int pLength)
{
    StarlightPacket nPacket;
    nPacket.mType = pType;
    nPacket.mLength = sizeof(uint32_t) + sizeof(uint16_t) + (sizeof(char) * pLength);
    memcpy(nPacket.mMessage, pDataBuffer, pLength);
    return nPacket;
}
void StarlightSocket::SendData(StarlightPacket pPacket)
{
}
void StarlightSocket::SendData(const char *pString)
{
}
void StarlightSocket::HandlePacket(StarlightSocket *rCaller, StarlightPacket &sPacket)
{
}

//============================================ Update =============================================
void StarlightSocket::Update()
{
}
void StarlightSocket::ClientUpdate()
{
}
void StarlightSocket::ServerUpdate()
{
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
int StarlightSocket::GetSocketErrorCode()
{
    //--Return failure, error code not known/handled.
    return STARLIGHTSOCKET_ERROR_NOTDEFINED;
}
void StarlightSocket::PrintError()
{
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================

#endif
