//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureDebug.h"
#include "AdvCombat.h"
#include "AdventureLevel.h"
#include "AdventureInventory.h"
#include "AliasStorage.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarAutoBuffer.h"

//--Definitions
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

///--[Local Definitions]
#define VOIDRIFT_ROLL_MIN 120
#define VOIDRIFT_ROLL_MAX 150

///--[Debug]
//#define ENEMY_UPDATE_DEBUG
#ifdef ENEMY_UPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///===================================== Property Queries =========================================
float TilemapActor::GetChaseDistance()
{
    if(AdventureLevel::Fetch()->GetRetreatTimer() > 0) return mViewDistance * 2.0f;
    return mViewDistance;
}

///======================================== Special AIs ===========================================
void TilemapActor::BagRefillAI()
{
    ///--[Documentation and Setup]
    //--Entity that stands in place and, when touched, refills the player's doctor bag and disappears.
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--Get the range to the player. If the player is on a different Z plane, the player becomes infinitely far.
    float tRangeToPlayer = GetRangeToPlayer();
    if(mCollisionDepth != tPlayerZ) tRangeToPlayer = 10000.0f;

    //--If the player touches us:
    if(tRangeToPlayer < xCombatTriggerRange)
    {
        //--Flag self for deletion.
        mSelfDestruct = true;
        AdventureLevel::KillEnemy(GetUniqueEnemyName());

        //--Dialogue pop up.
        WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
        rWorldDialogue->Show();
        rWorldDialogue->SetMajorSequence(false);
        rWorldDialogue->AppendString("[VOICE|Leader][SOUND|Combat|DoctorBag]Doctor Bag charges refilled!", true);
        rWorldDialogue->SetSilenceFlag(true);

        //--Refill the Doctor Bag.
        AdventureInventory::Fetch()->SetDoctorBagCharges(AdventureInventory::Fetch()->GetDoctorBagChargesMax());
    }
}
void TilemapActor::ItemDropAI()
{
    ///--[Documentation and Setup]
    //--In some cases, an enemy might drop an item. When the player touches it, a dialogue pops up and they are
    //  given the associated items.
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--Get the range to the player. If the player is on a different Z plane, the player becomes infinitely far.
    float tRangeToPlayer = GetRangeToPlayer();
    if(mCollisionDepth != tPlayerZ) tRangeToPlayer = 10000.0f;

    //--Player is not touching, do nothing.
    if(tRangeToPlayer >= xCombatTriggerRange) return;

    ///--[Player Contact]
    //--Flag self for deletion.
    mSelfDestruct = true;
    AdventureLevel::KillEnemy(GetUniqueEnemyName());

    //--Get the zeroth item. Construct a string.
    const char *rName = mEnemyListing->GetNameOfElementBySlot(0);

    //--Break it up by "|". Each entry is an item.
    StarLinkedList *rItemList = Subdivide::SubdivideStringToList(rName, "|");
    int tItemsTotal = rItemList->GetListSize();

    //--Dialogue pop up.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    rWorldDialogue->Show();
    rWorldDialogue->SetMajorSequence(false);

    //--Error, no items:
    if(tItemsTotal < 1)
    {
    }
    //--Exactly one item:
    else if(tItemsTotal == 1)
    {
        const char *rZeroEntry = (const char *)rItemList->GetElementBySlot(0);
        char *tBuffer = InitializeString("[VOICE|Narrator][SOUND|World|TakeItem]Got a %s!", rZeroEntry);
        rWorldDialogue->AppendString(tBuffer, false);
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rZeroEntry);
        free(tBuffer);
    }
    //--Exactly two items:
    else if(tItemsTotal == 2)
    {
        const char *rZeroEntry = (const char *)rItemList->GetElementBySlot(0);
        const char *rOneEntry = (const char *)rItemList->GetElementBySlot(1);
        char *tBuffer = InitializeString("[VOICE|Narrator][SOUND|World|TakeItem]Got a %s and a %s!", rZeroEntry, rOneEntry);
        rWorldDialogue->AppendString(tBuffer, false);
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rZeroEntry);
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rOneEntry);
        free(tBuffer);
    }
    //--Three or more items.
    else
    {
        //--Setup.
        StarAutoBuffer *tAutoBuffer = new StarAutoBuffer();
        tAutoBuffer->AppendStringWithoutNull("[VOICE|Narrator][SOUND|World|TakeItem]Got a ");

        //--Iterate.
        for(int i = 0; i < tItemsTotal - 2; i ++)
        {
            const char *rEntry = (const char *)rItemList->GetElementBySlot(i);
            LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rEntry);
            tAutoBuffer->AppendStringWithoutNull(rEntry);
            tAutoBuffer->AppendStringWithoutNull(", a ");
        }

        //--Second-last entry.
        const char *rEntryP = (const char *)rItemList->GetElementBySlot(tItemsTotal-2);
        tAutoBuffer->AppendStringWithoutNull(rEntryP);
        tAutoBuffer->AppendStringWithoutNull(", and a ");
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rEntryP);

        //--Last entry.
        const char *rEntryU = (const char *)rItemList->GetElementBySlot(tItemsTotal-1);
        tAutoBuffer->AppendStringWithoutNull(rEntryU);
        tAutoBuffer->AppendString("!");
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rEntryU);

        //--Append.
        rWorldDialogue->AppendString((const char *)tAutoBuffer->GetRawData(), false);

        //--Clean.
        delete tAutoBuffer;
    }

    //--Clean up.
    delete rItemList;
}

///===================================== Worker Functions =========================================
bool TilemapActor::IsIgnoringPlayer()
{
    ///--[Documentation and Setup]
    //--Checks if the enemy is ignoring the player. This can be caused by the player being in a friendly form, a global flag,
    //  being on a different depth, or being in a special zone.
    if(mIsIgnoringPlayer || TilemapActor::xAlwaysIgnorePlayer) return true;

    //--Get position.
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--Depth check. Ignore the player when on different depths.
    if(mCollisionDepth != tPlayerZ) return true;

    //--Ignore players in invisibility zones. This is often used next to room entrances to prevent accidentally starting a
    //  battle before the player can react.
    if(AdventureLevel::xIsPlayerInInvisZone) return true;

    //--Ignore the player if the AI can't run yet because the level just loaded.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && !rActiveLevel->EnemiesCanAcquireTargets()) return true;

    //--If the level's camo timer is over 0, the player is invisible. Field abilities can do this.
    if(rActiveLevel && rActiveLevel->GetPlayerCamoTimer() > 0) return true;

    //--All checks passed.
    return false;
}
bool TilemapActor::ShouldRenderViewcone()
{
    ///--[Documentation and Setup]
    //--Determine if the entity in question should render their viewcone, returning true if they should.
    if(mIsDisabled) return false;

    //--Flagged to not render viewcones at all.
    if(!xRenderViewcone) return false;

    //--This is not an enemy.
    if(!mIsEnemy) return false;

    //--Ignoring the player, usually because they are of the same enemy species.
    if(mIsIgnoringPlayer) return false;

    //--Blind enemies never render a viewcone.
    if(mIsBlind) return false;

    //--Stunned enemies don't render their viewcone.
    if(mMugStunTimer > 0) return false;

    //--Never render a viewcone if camouflage mode is activated. This is only done via the debug menu.
    if(AdventureDebug::xCamouflageMode) return false;

    //--Get the level and check for a level transition.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsLevelTransitioning()) return false;

    //--If the level's camo timer is over 0, the player is invisible. Field abilities can do this.
    if(rActiveLevel && rActiveLevel->GetPlayerCamoTimer() > 0) return false;

    //--Bag-Refills and item drops don't render.
    if(mIsBagRefill) return false;
    if(mIsItemNode) return false;
    if(mIsItemDrop) return false;

    //--Enemies who follow someone also don't render their viewcones.
    if(mFollowTarget && mAIState == TA_AI_WANDER) return false;

    //--Enemies who are leashing don't render a viewcone.
    if(mAIState == TA_AI_LEASH) return false;

    //--Don't render viewcone when dying. Or when not actually hostile.
    if(mIsDying || !mEnemyListing || mEnemyListing->GetListSize() < 1) return false;

    //--Don't render viewcones if on a different Z level from the player.
    float tPlayerX, tPlayerY, tPlayerZ;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
    if(mCollisionDepth != tPlayerZ) return false;

    //--All checks passed, render viewcone.
    return true;
}

///======================================= Update Routine =========================================
void TilemapActor::UpdateEnemyMode()
{
    ///--[Documentation and Setup]
    //--Handles enemy updating. Enemies patrol or wander, until they spot the player, at which point they
    //  chase the player. They can also be set to follow other entities or go back to their patrol points.
    DebugPush(true, "%s begins update cycle.\n", mLocalName);

    ///--[World Blocking]
    //--If the AdventureLevel is currently doing something to block our update, such as having the GUI be
    //  visible, then we stop here.
    if(AdventureLevel::IsWorldStopped() && !AdventureLevel::xRunEnemyUpdateForPatrolScatter)
    {
        //--Some enemies update when the world is stopped for a cutscene. This function will check that.
        if(mIgnoreWorldStop && AdventureLevel::IsWorldStoppedFromCutsceneOnly())
        {

        }
        //--Otherwise, this is a legitimate stop or the enemy doesn't ignore it.
        else
        {
            DebugPop("Blocked by world stop.\n");
            return;
        }
    }

    //--If the world is transitioning, in or out, stop them. This ignores the cutscene flag!
    if(AdventureLevel::Fetch()->IsLevelTransitioning() && !AdventureLevel::xRunEnemyUpdateForPatrolScatter)
    {
        DebugPop("Blocked by level transition.\n");
        return;
    }

    ///--[Doctor Bag Refill Enemy]
    //--Bag Refill: If the player touches us, we refill their Doctor Bag and show a dialogue box.
    if(mIsBagRefill)
    {
        BagRefillAI();
        DebugPop("Is Bag Refill.\n");
        return;
    }

    ///--[Item Node]
    //--If this is an item node, the player can activate it to receive an item.
    if(mIsItemNode)
    {
        DebugPop("Is Item Node.\n");
        return;
    }

    ///--[Loot Drop]
    //--Loot Drop. The player touches us, we give them a loot drop via dialogue box and despawn.
    if(mIsItemDrop)
    {
        ItemDropAI();
        DebugPop("Is Item Drop.\n");
        return;
    }

    ///--[Blocking Checks]
    //--Dying/Stunned/etc cases will stop the update if they return true.
    if(RunBlockCases())
    {
        DebugPop("Is block case.\n");
        return;
    }

    ///--[Special Timers]
    //--Guard mode external activation.
    if(mExternalGuardModeCooldown > 0) mExternalGuardModeCooldown --;

    //--Void Rift timer. Used for animations.
    if(mIsVoidRiftMode)
    {
        mVoidRiftTimer ++;

        //--Every tick there's a 5% chance of resetting the rift timer. At the max, it always resets.
        if(mVoidRiftTimer >= VOIDRIFT_ROLL_MAX || (mVoidRiftTimer >= VOIDRIFT_ROLL_MIN && rand() % 100 < 5))
        {
            mVoidRiftTimer = 0;
            mVoidRiftFrame = rand() % 3;
        }
    }

    //--Always run this timer.
    mSpotTimer ++;

    ///--[Mugging]
    //--Debug.
    DebugPrint("Checking mugging.\n");

    //--If an enemy has a mug timer over zero, they are in the process of being mugged. Decrement the timer.
    if(mMugTimer > 0)
    {
        //--If the mug timer is over the cap, the enemy is "Mugged" here.
        if(mMugTimer >= TA_MUG_TICKS_TOTAL)
        {
            HandleMugging();
        }
        //--Otherwise, decrement the timer.
        else
        {
            mMugTimer -= TA_MUG_TICK_LOSS_PER_TICK;
            if(mMugTimer < 0) mMugTimer = 0;
        }
    }
    //--If the enemy was stunned due to a mug, handle that here.
    else if(mMugStunTimer > 0)
    {
        //--Decrement.
        mMugStunTimer --;

        //--If the timer is still over 0, stop the update.
        DebugPop("Mug stunned.\n");
        return;
    }

    //--Enemies who are ignoring the player don't block unlimited stamina.
    if(!mIsIgnoringPlayer && !TilemapActor::xAlwaysIgnorePlayer) AdventureLevel::xEntitiesDrainStamina = true;

    ///--[Battle Trigger]
    //--Debug.
    DebugPrint("Checking battle trigger.\n");

    //--Run proximity function.
    if(CheckBattleTrigger())
    {
        DebugPop("Stopped by battle trigger.\n");
        return;
    }

    ///--[Spotting the Player]
    //--Debug.
    DebugPrint("Checking  player LOS.\n");

    //--If the player spotted timer is on, it increments a bit and then a chase occurs.
    if(HandleSightCheck())
    {
        DebugPop("Stopped by sight check.\n");
        return;
    }

    ///--[ ============= Wander AI ============ ]
    //--Wandering. Pace around a bit.
    if(mAIState == TA_AI_WANDER)
    {
        DebugPrint("Wander AI.\n");
        HandleWanderAI();
    }
    ///--[ ============ Waiting AI ============ ]
    //--Wait around until the timer hits the given amount. When it does, go back to wander mode.
    else if(mAIState == TA_AI_WAIT)
    {
        DebugPrint("Wait AI.\n");
        HandleWaitAI();
    }
    ///--[ ============ Chasing AI ============ ]
    //--Chase after the player!
    else if(mAIState == TA_AI_CHASE)
    {
        DebugPrint("Chase AI.\n");
        HandleChaseAI();
    }
    ///--[ =========== Leashing AI ============ ]
    //--Return to the leash point.
    else if(mAIState == TA_AI_LEASH)
    {
        DebugPrint("Leash AI.\n");
        HandleLeashAI();
    }
    ///--[ ============ Turning AI ============ ]
    //--Turning to face the next node.
    else if(mAIState == TA_AI_TURNTOFACE)
    {
        DebugPrint("Turn AI.\n");
        HandleTurnAI();
    }

    //--Debug.
    DebugPop("Exited normally.\n");
}

///===================================== AI Base Sequence =========================================
bool TilemapActor::RunBlockCases()
{
    ///--[Documentation and Setup]
    //--Cases where the enemy will not execute its AI because something is stopping it. Returns true if
    //  any cases are block, false if none are.

    ///--[Enemy Is Dying]
    //--If the enemy is dying, handle that instead. Dying enemies don't block unlimited stamina.
    if(mIsDying)
    {
        //--Timer.
        if(mDeathTimer < 30)
        {
            //--Increment.
            mDeathTimer ++;

            //--While dying, the enemy will randomly change directions.
            if(mDeathTimer % 3 == 0) mFacing = rand() % TA_DIR_TOTAL;

            //--On the exact 30, switch special frame:
            if(mDeathTimer == 30)
            {
                ActivateSpecialFrame("Wounded");
            }
        }

        //--Stop the update.
        return true;
    }

    ///--[Stunned]
    //--A stunned enemy doesn't move, but does count in terms of stamina blocking.
    if(mStunTimer > 0)
    {
        AdventureLevel::xEntitiesDrainStamina = true;
        mStunTimer --;
        return true;
    }

    ///--[All Checks Passed]
    return false;
}
bool TilemapActor::CheckBattleTrigger()
{
    ///--[Documentation and Setup]
    //--Check if the player and enemy should start a battle. Returns true if a battle is starting, false otherwise.
    //--Note: If a battle is triggered, the object may become unstable. Caller should immediately exit its current
    //  function to avoid crashes.
    DebugPush(true, "Checking battle trigger.\n");
    if(IsIgnoringPlayer())
    {
        DebugPop("Stopped, ignoring player.\n");
        return false;
    }

    //--Get the range to the player. If the player is on a different Z plane, the player becomes infinitely far.
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    float tRangeToPlayer = GetRangeToPlayer();
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
    if(mCollisionDepth != tPlayerZ) tRangeToPlayer = 10000.0f;

    //--If outside of combat trigger range, fail.
    if(tRangeToPlayer > xCombatTriggerRange)
    {
        DebugPop("Stopped, outside trigger range.\n");
        return false;
    }

    ///--[Cutscene Case]
    //--Sometimes an entity may trigger a cutscene instead of a battle. This still returns true.
    if(mDoesNotTriggerBattles)
    {
        //--Immediately run the "defeat" script. This actor is set to be the stack head when executing.
        AliasStorage *rAliasStorage = AliasStorage::GetChapterPathAliasStorage();
        const char *rPath = rAliasStorage->GetString(mDefeatSceneName);
        LuaManager::Fetch()->PushExecPop(this, rPath);

        //--Because this execution could theoretically delete this entity, we return out immediately.
        DebugPop("Ended, does-not-trigger-battles case.\n");
        return true;
    }

    ///--[Battle Case]
    //--Flag the enemy as stunned. If they die, no problem. If the player retreats, this will leave the
    //  enemy unable to chase briefly.
    DebugPrint("Handling battle.\n");
    mStunTimer = 45;
    mReinforcementTimer = 0;

    //--Battle!
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    DebugPrint("Booting combat %p.\n", rAdventureCombat);
    rAdventureCombat->Reinitialize(true);
    DebugPrint("Reinitialize completed. Activating.\n");
    rAdventureCombat->Activate();

    //--Get any applicable scripts and store them as overrides.
    DebugPrint("Handling aliases.\n");
    AliasStorage *rAliasStorage = AliasStorage::GetChapterPathAliasStorage();
    if(mDefeatSceneName)
    {
        const char *rPath = rAliasStorage->GetString(mDefeatSceneName);
        rAdventureCombat->SetDefeatScript(rPath);
    }
    if(mRetreatSceneName)
    {
        const char *rPath = rAliasStorage->GetString(mRetreatSceneName);
        rAdventureCombat->SetRetreatScript(rPath);
    }
    if(mVictorySceneName)
    {
        const char *rPath = rAliasStorage->GetString(mVictorySceneName);
        rAdventureCombat->SetVictoryScript(rPath);
    }

    //--If the player snuck up on this enemy, they get a huge initiative bonus! This cannot happen during
    //  retreat mode.
    DebugPrint("Checking initiative bonuses.\n");
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(mSpottedPlayerTicks == 0 && mAIState != TA_AI_LEASH && mAIState != TA_AI_CHASE && rActiveLevel->GetRetreatTimer() < 1)
    {
        rAdventureCombat->GivePlayerInitiative();
    }

    //--If retreat mode is active, then the next battle is unretreatable.
    if(rActiveLevel->GetRetreatTimer() > 0)
    {
        rAdventureCombat->SetUnretreatable(true);
    }

    //--Run world pulse. This also fires field abilities, which may modify the combat state.
    DebugPrint("Running world pulse.\n");
    rAdventureCombat->PulseWorld(false, false, false);
    if(rActiveLevel) rActiveLevel->RunFieldAbilitiesOnActor(this);

    //--The subroutine will add the enemies for us.
    DebugPrint("Appending enemies to combat.\n");
    AppendEnemiesToCombat(false);

    //--This entity adds itself to the combat's reference list. If the enemy party is wiped out, this
    //  entity will die.
    rAdventureCombat->RegisterWorldReference(this, 0);

    //--Indicate a battle started.
    DebugPop("Ended, battle triggered.\n");
    return true;
}
bool TilemapActor::HandleSightCheck()
{
    ///--[Documentation and Setup]
    //--Handles sight routines, checking if the enemy can see the player with their visibility cone. Also handles
    //  associated timers and AI state changes. Returns true if the sight check should stop the AI, false otherwise.
    //--A subroutine will check if the player is in the sight cone.
    bool tIsPlayerInSightCone = IsPlayerInSightCone(TA_USE_COMPUTED_VIEWDIST);

    //--Check if the entity is ignoring the player.
    bool tIsIgnoringPlayer = IsIgnoringPlayer();

    //--Get player position.
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--Get the range to the player. If the player is on a different Z plane, the player becomes infinitely far.
    float tRangeToPlayer = GetRangeToPlayer();
    if(mCollisionDepth != tPlayerZ) tRangeToPlayer = 10000.0f;

    ///--[Enemy Is Alert]
    //--If the player is in the cone, or the player was recently spotted and the timer has not zeroed:
    if(mSpottedPlayerTicks > 0 && tIsPlayerInSightCone)
    {
        //--Timer.
        mDropSuspicionTicks = 0;

        //--Effective view distance doubles when in retreat mode. This means spotting is also faster.
        float cUseViewDistance = mViewDistance;
        if(AdventureLevel::Fetch()->GetRetreatTimer() > 0) cUseViewDistance = mViewDistance * 2.0f;

        //--If we have spotted the player, set closest enemy distance to the lowest value that isn't a combat value.
        //  This increases music intensity. Enemies that are ignoring the player don't count.
        if(!tIsIgnoringPlayer)
        {
            if(AdventureLevel::xClosestEnemy > tRangeToPlayer) AdventureLevel::xClosestEnemy = 9.0f;
        }

        //--Detection increase: If closer than 25% of the detection range, detection rate is x4!
        if(tRangeToPlayer < cUseViewDistance * 0.25f)
        {
            mSpottedPlayerTicks += 4;
        }
        //--Midrange: Detection is doubled if closer than 50% of the max range.
        else if(tRangeToPlayer < cUseViewDistance * 0.50f)
        {
            mSpottedPlayerTicks += 2;
        }
        //--Normal detection speed.
        else
        {
            mSpottedPlayerTicks ++;
        }

        //--Stop the update here.
        if(mSpottedPlayerTicks < mSpottedPlayerTicksMax)
        {
            return true;
        }
        //--Go to chase mode!
        else
        {
            BeginChase();
        }
    }
    ///--[Enemy Is Not Alert]
    //--Reset back to zero over time. The enemy will pause for a while before cycling back to zero.
    else if(mSpottedPlayerTicks > 0)
    {
        //--Increment the drop-suspicion timer.
        mDropSuspicionTicks ++;

        //--Once the drop-suspicion timer passes a threshold, start dropping the spotting timer.
        if(mDropSuspicionTicks > 45)
        {
            if(mDropSuspicionTicks % 3 == 0) mSpottedPlayerTicks --;
        }
    }

    ///--[Viewcone Movement]
    //--Move the view angle to match the ideal.
    if(!tIsPlayerInSightCone)
    {
        //--Ideal angle.
        float cIdealAngle = (mFacing * 45.0f) - 90.0f;

        //--Turn radius.
        float cTurnPerTick = 12.0f;

        //--Subroutine handles the rest.
        mCurrentViewAngle = HomeInOnAngleDeg(mCurrentViewAngle, cIdealAngle, cTurnPerTick);
    }

    ///--[Flags]
    //--Store the intensity-changing flag if needed.
    if(!tIsIgnoringPlayer)
    {
        //--Only store if closer than the current.
        if(AdventureLevel::xClosestEnemy > tRangeToPlayer) AdventureLevel::xClosestEnemy = tRangeToPlayer;
    }

    ///--[Finish Up]
    return false;
}
