//--Base
#include "StarFont.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarBitmap.h"
#include "StarFileSystem.h"
#include "UString.h"

//--Definitions
#include "utf8.h"
#include "GlDfn.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Notes]
//--StarFont Implementation specific to Freetype. Entire file will not be included if Freetype is not
//  the compilation style.
#if defined _FONTTYPE_FREETYPE_

///--[Debug]
//#define SUGARFONT_FREETYPE_DEBUG
#ifdef SUGARFONT_FREETYPE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
void StarFont::ConstructWith(const char *pLoadPath, int pFontSize, uint8_t pFlags)
{
    ///--[Documentation]
    //--Creates the StarFont and manually loads the font according to the passed size. If size
    //  is zero or lower, the default size is used.

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:ConstructWith() - Warning. Attempted to use dummy font %s\n", mLocalName); return; }

    ///--[Startup]
    if(!pLoadPath) return;
    DebugPush(pFlags & SUGARFONT_PRECACHE_DEBUG_FLAG || true, "Loading font %s %p\n", pLoadPath, this);

    //--Resolve internal name.
    char *tFileName = StarFileSystem::PareFileName(pLoadPath, false);
    char *tUseName = InitializeString("%s %i", tFileName, pFontSize);
    SetInternalName(tUseName);
    DebugPrint("Set internal name to %s\n", tUseName);

    //--Clean.
    free(tFileName);
    free(tUseName);

    ///--[Compilation Mode]
    //--If flagged, create a StarAutoBuffer. This will hold the data for this font during construction.
    if(StarFont::xFontSLFMode == STARFONT_SLF_PREBUILD_COMPILE)
    {
        mOutputBuffer = new StarAutoBuffer();
        mOutputBuffer->SetBufferSegmentation(true);
        mOutputBuffer->AppendStringWithoutNull("FONTINFO00");
        mOutputBuffer->AppendStringWithLen(mLocalName);
        mOutputBuffer->AppendUInt32(pFlags);
    }

    ///--[Prebuilt Mode]
    //--If a .slf file is open, try to get all the data from the file, which should be faster than
    //  loading and rasterizing a font.
    if(ReadFromSLM())
    {
        mIsReady = true;
        DebugPop("Finished font creation, loaded from active .slf file.\n");
        return;
    }

    ///--[Store Basic Font]
    //--Range check.
    if(pFontSize < 1) pFontSize = SUGARFONT_DEFAULTSIZE;

    //--Font storage
    mOwnsFont = true;
    mInternalSize = pFontSize;
    FT_New_Face(xFTLibrary, pLoadPath, 0, &rFont);
    FT_Set_Pixel_Sizes(rFont, 0, pFontSize);

    //--If the font is valid, we can be ready immediately.
    if(rFont) mIsReady = true;

    ///--[Count Number of Glyphs]
    //--The glyphs don't necessarily correspond to letters in terms of slots.
    uint32_t tGIndex = 0;
    uint32_t tTotal = 0;
    uint32_t tHighest = 0;

    //--Iterate across all letters.
    int tCharCode = FT_Get_First_Char(rFont, &tGIndex);
    while(tGIndex > 0)
    {
        //--Figure out the highest glyph number. They may not be in order.
        //fprintf(stderr, "Glyph %i is %i\n", tTotal, tCharCode);
        if(tCharCode > (int)tHighest) tHighest = tCharCode;

        //--Next.
        tCharCode = FT_Get_Next_Char(rFont, tCharCode, &tGIndex);
        tTotal ++;
    }

    //--Store how many glyphs were iterated over.
    mTotalGlyphs = tTotal;

    //--Debug.
    DebugPrint(" Total glyphs: %i\n", tTotal);
    DebugPrint(" Highest index: %i\n", tHighest);

    ///--[Glyph Remap]
    //--Allocate space for all remaps. Set the default index to -1.
    mTotalRemaps = tTotal;
    mRemaps = (StarFontGlyphRemap *)starmemoryalloc(sizeof(StarFontGlyphRemap) * mTotalRemaps);

    //--Clear all remaps to -1.
    for(int i = 0; i < mTotalRemaps; i ++)
    {
        mRemaps[i].mGlyphIndex = -1;
        mRemaps[i].mUnicodeValue = -1;
    }

    ///--[Precache]
    //--If in precache mode, and in Freetype mode, then precache the font here.
    DebugPrint(" Beginning precache.\n");
    PrecacheData(pFlags);
    DebugPrint(" Finished precaching.\n");

    //--Print remaps.
    if(pFlags & SUGARFONT_PRECACHE_DEBUG_FLAG)
    {
        //--Determine cap.
        int tCap = 500;
        if(tCap > mTotalRemaps) tCap = mTotalRemaps;

        fprintf(stderr, " Printing array diagnostic.\n");
        for(int i = 0; i < tCap; i ++)
        {
            fprintf(stderr, "  Unicode %i is Glyph %i\n", mRemaps[i].mUnicodeValue, mRemaps[i].mGlyphIndex);
        }
    }

    //--Write to the autobuffer.
    if(mOutputBuffer)
    {
        //--Debug.
        DebugPrint(" Writing to output buffer. %f %f\n", mInternalSize, mKerningScaler);

        //--Flags.
        mOutputBuffer->AppendFloat(mInternalSize);
        mOutputBuffer->AppendFloat(mKerningScaler);

        //--Glyph Remaps.
        DebugPrint(" Appending remaps. Total: %i\n", mTotalRemaps);
        mOutputBuffer->AppendUInt32(mTotalRemaps);
        for(int i = 0; i < mTotalRemaps; i ++)
        {
            mOutputBuffer->AppendInt32(mRemaps[i].mGlyphIndex);
            mOutputBuffer->AppendInt32(mRemaps[i].mUnicodeValue);
            if(i < 10) DebugPrint("%i %i\n", mRemaps[i].mGlyphIndex, mRemaps[i].mUnicodeValue);
        }

        //--Precache Data.
        DebugPrint(" Finished remaps. Appending end data. %f %f %i\n", mEndPadding, mTallestCharacter, mTotalGlyphs);
        mOutputBuffer->AppendFloat(mEndPadding);
        mOutputBuffer->AppendFloat(mTallestCharacter);
        mOutputBuffer->AppendUInt32(mTotalGlyphs);

        //--Debug.
        DebugPrint(" Finished writing to output buffer.\n");
    }

    ///--[Debug]
    //--Save to disk so the coder can take a look at this.
    if(mOutputBuffer && false)
    {
        //--Get out the file name.
        char *tFileTitle = StarFileSystem::PareFileName(mLocalName, false);
        char *tFinalPath = InitializeString("Data/FontPrebuild/%s.png", tFileTitle);

        //--Save.
        DebugPrint(" Saving to %s\n", tFinalPath);
        StarBitmap::SaveToDrive(mPrecachedHandle, tFinalPath);

        //--Clean.
        free(tFileTitle);
        free(tFinalPath);
    }

    //--Debug.
    DebugPop("Finished font creation.\n");
}

///--[Private Statics]
FT_Library StarFont::xFTLibrary;

///===================================== Property Queries =========================================
int StarFont::GetTextHeight()
{
    //--Returns how tall the given string would be on screen, in pixels.
    if(mIsBitmapMode) return GetBitmapTextHeight();

    //--Safety check, cannot return 0.
    if(mTallestCharacter < 1) return 1;
    return mTallestCharacter;
}
float StarFont::GetPrecacheSizeX()
{
    return mPrecachedSizeX;
}
float StarFont::GetPrecacheSizeY()
{
    return mPrecachedSizeY;
}

///======================================== Manipulators ==========================================
void StarFont::Bind()
{
    ///--[Documentation]
    //--Binds the requested texture if precaching is enabled. Otherwise, does nothing.

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:Bind() - Warning. Attempted to use dummy font %s\n", mLocalName); return; }

    ///--[Execution]
    //--Must have finished other precache actions.
    if(!mIsReady) return;

    //--Call bind.
    if(mPrecachedHandle) glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);
}

///======================================== Core Methods ==========================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void StarFont::DrawText(float pX, float pY, int pFlags, float pSize, const char *pText)
{
    ///--[Documentation]
    //--Master draw function. Translate to the provided position and render the text.

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawText() - Warning. Attempted to use dummy font %s. Text %s.\n", mLocalName, pText); return; }

    //--Error checks.
    if(!mIsReady || !pText) return;

    ///--[Length Check]
    //--Never bother to render a string with zero length.
    int tLen = utf8_strlen(pText);
    if(tLen < 1) return;

    ///--[Bitmap]
    //--Independent of library type, uses a single bitmap for ASCII-only rendering.
    if(mIsBitmapMode) { DrawTextBitmap(pX, pY, pFlags, pSize, pText); return; }

    ///--[Error Checking]
    //--No precached data, fail.
    DebugPush(true, "Drawing text %s\n", pText);
    DebugPrint("Length is %i letters or %i bytes\n", tLen, (int)strlen(pText));
    if(!mPrecachedHandle || !mPrecacheData)
    {
        return;
    }

    //--Force a valid size multiplier.
    if(pSize == 0.0f) pSize = 1.0f;
    DebugPrint(" Passed checks.\n");

    ///--[Positioning]
    //--Move cursor. If flagged, use this as the center point (mod the offset).
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (GetTextWidth(pText) / 2.0f * pSize);
    }
    else if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (GetTextWidth(pText) * pSize);
    }
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (GetTextHeight() / 2.0f * pSize);
    }
    DebugPrint(" Translating.\n");
    glTranslatef(pX, pY, 0.0f);

    //--Sizing. This is only used if the size is not 1.0f, since scaling is non-trivial.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(pSize, pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    ///--[Rendering]
    //--Grab each glyph and render it in turn. First, bind the texture (precached mode).
    DebugPrint(" Binding.\n");
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--Setup.
    int tIndex = 0;
    float tRunningOffset = 0.0f;

    //--For each letter...
    //fprintf(stderr, "For each letter.\n");
    uint32_t tLetter = utf8_nextletter(pText, tIndex);
    for(int i = 0; i < tLen-1; i ++)
    {
        ///--[Get Letter and Glyph]
        //--Get the next letter.
        uint32_t tNextLetter = utf8_nextletter(pText, tIndex);
        DebugPrint(" Letter %i is %u - %c\n", i, tLetter, tLetter);
        DebugPrint("  Next Letter %i is %u - %c\n", i+1, tNextLetter, tNextLetter);

        //--Range check.
        if(tNextLetter < 0)
        {
            //--Don't print if this is a system key that doesn't normally have a glyph.
            if(tLetter >= 32)
            {
                //DebugManager::ForcePrint("Error: Font %s has no glyph (Unicode %i). Out of remap range.\n", mLocalName, tLetter);
            }
            tLetter = tNextLetter;
            continue;
        }

        //--Get glyph remap.
        int tGlyphLft = GetGlyphFromRemap(tLetter);
        DebugPrint("  Glyph Index: %i %p\n", tGlyphLft, this);

        //--Range check on the letter:
        if(tGlyphLft == -1 || tGlyphLft >= mTotalGlyphs)
        {
            //--Don't print if this is a system key that doesn't normally have a glyph.
            if(tLetter >= 32)
            {
                DebugManager::ForcePrint("Error: Font %s has no glyph %i (Unicode %u).\n", mLocalName, tGlyphLft, tLetter);
            }
            tLetter = tNextLetter;
            continue;
        }

        //--Precache doesn't exist:
        if(!mPrecacheData[tGlyphLft].mGLList) { tLetter = tNextLetter; continue; }

        ///--[Special Flag Cases]
        //--Debug.
        DebugPrint("  Rendering.\n");

        //--Italics.
        if(pFlags & SUGARFONT_ITALIC)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListItalic);
        }
        //--Mirror around the X axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORX)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListXMirror);
        }
        //--Mirror around the Y axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORY)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListYMirror);
        }
        //--Double-render X. Renders the letter twice, once normally and once flipped on the X-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERX)
        {
            //--Normal.
            glCallList(mPrecacheData[tGlyphLft].mGLList);

            //--Flipped.
            glTranslatef(mPrecacheData[tGlyphLft].mAdvanceX, 0.0f, 0.0f);
            glScalef(-1.0f, 1.0f, 1.0f);
            glCallList(mPrecacheData[tGlyphLft].mGLList);
            glScalef(-1.0f, 1.0f, 1.0f);
            glTranslatef(mPrecacheData[tGlyphLft].mAdvanceX * -1.0f, 0.0f, 0.0f);
        }
        //--Double-render Y. Renders the letter twice, once normally and once flipped on the Y-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERY)
        {
            //--Normal.
            glCallList(mPrecacheData[tGlyphLft].mGLList);

            //--Compute.
            float cYPosition = mPrecacheData[tGlyphLft].mFlippedTopOffset * 1.0f;

            //--Flipped.
            glTranslatef(0.0f, cYPosition, 0.0f);
            glScalef(1.0f, -1.0f, 1.0f);
            glCallList(mPrecacheData[tGlyphLft].mGLList);
            glScalef(1.0f, -1.0f, 1.0f);
            glTranslatef(0.0f, -cYPosition, 0.0f);
        }
        //--Normal rendering.
        else
        {
            DebugPrint("  Calling list %i\n", mPrecacheData[tGlyphLft].mGLList);
            glCallList(mPrecacheData[tGlyphLft].mGLList);
        }
        DebugPrint(" Done letter.\n");

        //--Get the expected kerning distance.
        float cKernDistance = mPrecacheData[tGlyphLft].mWidth;

        //--If the letter-right is the fullwidth colon, use the monospace value, if it exists.
        if(tLetter == 65306)
        {
            cKernDistance = mMonospacing;
        }
        else if(tNextLetter >= SUGARFONT_PRECACHE_OFF && tNextLetter < SUGARFONT_PRECACHE_HI)
        {
            cKernDistance = mPrecacheData[tGlyphLft].mKerningDistance[tNextLetter-SUGARFONT_PRECACHE_OFF];
        }
        else if(mMonospacing != -1.0f)
        {
            cKernDistance = mMonospacing;
        }
        //--Not in range. Used the letter's width.
        else
        {
            cKernDistance = mPrecacheData[tGlyphLft].mWidth;
        }

        //--Reposition the cursor.
        glTranslatef(cKernDistance, 0.0f, 0.0f);
        tRunningOffset = tRunningOffset + cKernDistance;

        //--Move to the next letter.
        tLetter = tNextLetter;
    }
    DebugPrint("Finished.\n");
    DebugPrint("Final Letter is %i - %c\n", tLetter, tLetter);

    ///--[Final Letter]
    //--Remap check.
    if(true)
    {
        //--Get remap.
        int tGlyphLft = GetGlyphFromRemap(tLetter);

        //--Remap fails.
        if(tGlyphLft < 0 || tGlyphLft >= mTotalGlyphs)
        {
            DebugPrint(" Final letter invalid glyph. %i\n", tGlyphLft);
        }
        //--No precache.
        else if(!mPrecacheData[tGlyphLft].mGLList)
        {
            DebugPrint(" Final letter no precache. %i\n", tGlyphLft);
        }
        //--Italics.
        else if(pFlags & SUGARFONT_ITALIC)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListItalic);
        }
        //--Mirror around the X axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORX)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListXMirror);
        }
        //--Mirror around the Y axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORY)
        {
            glCallList(mPrecacheData[tGlyphLft].mGLListYMirror);
        }
        //--Double-render X. Renders the letter twice, once normally and once flipped on the X-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERX)
        {
            //--Normal.
            glCallList(mPrecacheData[tGlyphLft].mGLList);

            //--Flipped.
            glTranslatef(mPrecacheData[tGlyphLft].mAdvanceX, 0.0f, 0.0f);
            glScalef(-1.0f, 1.0f, 1.0f);
            glCallList(mPrecacheData[tGlyphLft].mGLList);
            glScalef(-1.0f, 1.0f, 1.0f);
            glTranslatef(mPrecacheData[tGlyphLft].mAdvanceX * -1.0f, 0.0f, 0.0f);
        }
        //--Double-render Y. Renders the letter twice, once normally and once flipped on the Y-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERY)
        {
            //--Normal.
            glCallList(mPrecacheData[tGlyphLft].mGLList);

            //--Compute.
            float cYPosition = mPrecacheData[tGlyphLft].mFlippedTopOffset * 1.0f;

            //--Flipped.
            glTranslatef(0.0f, cYPosition, 0.0f);
            glScalef(1.0f, -1.0f, 1.0f);
            glCallList(mPrecacheData[tGlyphLft].mGLList);
            glScalef(1.0f, -1.0f, 1.0f);
            glTranslatef(0.0f, -cYPosition, 0.0f);
        }
        //--Normal rendering.
        else
        {
            glCallList(mPrecacheData[tGlyphLft].mGLList);
        }
    }

    ///--[Clean Up]
    //--Remove running offset.
    DebugPrint("Cleaning up.\n");
    glTranslatef(tRunningOffset * -1.0f, 0.0f, 0.0f);

    //--Undo the size change.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(1.0f / pSize, 1.0f / pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--Undo by cursor.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);
    DebugPop("Complete.\n");
}

///--[Single-Letter Rendering]
//--Overload. Passes 0 for the next letter, which ignores kerning.
float StarFont::DrawLetter(float pX, float pY, int pFlags, float pSize, int pLetter)
{
    return DrawLetter(pX, pY, pFlags, pSize, pLetter, 0);
}

float StarFont::DrawLetter(float pX, float pY, int pFlags, float pSize, int pLetter, int pNextLetter)
{
    ///--[Documentation]
    //--Used for cases where you want to render a single letter at a time due to varying colors,
    //  styles, or other effects. This is often used when rendering complex dialogue cases.
    //--Returns the advance amount to render the next letter, including Kerning. Pass 0 to not compute
    //  kerning with the advance rate. Ignore the return value if you're using fixed-width cases.

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawLetter() - Warning. Attempted to use dummy font %s\n", mLocalName); return mMonospacing; }

    ///--[Setup]
    float tDefault = mMonospacing;
    if(mMonospacing == -1.0f) tDefault = 0.0f;
    if(!mIsReady) return tDefault;

    //--Buffer, used for some size checks. Must be 4+null for unicode support.
    char tBuffer[5];
    memset(tBuffer, 0, sizeof(char) * 5);
    u8_wc_toutf8(tBuffer, pLetter);

    ///--[Bitmap]
    //--Independent of library type, uses a single bitmap for ASCII-only rendering. Always returns a fixed width.
    if(mIsBitmapMode)
    {
        DrawTextBitmap(pX, pY, pFlags, pSize, tBuffer);
        return GetBitmapTextWidth("A");
    }

    ///--[Error Checking]
    //--Data must be precached, size must be non-zero or scaling will crash the program.
    if(!mPrecachedHandle || !mPrecacheData) return tDefault;
    if(pSize == 0.0f) pSize = 1.0f;
    if(pLetter < 0) return tDefault;

    ///--[Positioning]
    //--Move cursor. If flagged, use this as the center point (mod the offset).
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (GetTextWidth(tBuffer) / 2.0f * pSize);
    }
    else if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (GetTextWidth(tBuffer) * pSize);
    }
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (GetTextHeight() / 2.0f * pSize);
    }
    glTranslatef(pX, pY, 0.0f);

    //--Sizing. This is only used if the size is not 1.0f, since scaling is non-trivial.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(pSize, pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    ///--[Rendering]
    //--Bind the texture.
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--Range-check the remap.
    if(pLetter < 0)
    {
        //--Don't print this for system keys.
        if(pLetter >= 32)
        {
            DebugManager::ForcePrint("Error: Font %s has no glyph (Unicode %i). Remap out of range.\n", mLocalName, pLetter);
        }
        fprintf(stderr, "Block rendering, remap range error.\n");
        return tDefault;
    }

    //--Get the glyph index.
    int tGlyphLft = GetGlyphFromRemap(pLetter);
    if(tGlyphLft == -1 || tGlyphLft >= mTotalGlyphs)
    {
        //--Don't print this for system keys.
        if(pLetter >= 32)
        {
            DebugManager::ForcePrint("Error: Font %s has no glyph %i (Unicode %i).\n", mLocalName, tGlyphLft, pLetter);
        }
        fprintf(stderr, "Block rendering, glyph range error.\n");
        return tDefault;
    }

    //--Render the letter.
    if(mPrecacheData[tGlyphLft].mGLList)
    {
        glCallList(mPrecacheData[tGlyphLft].mGLList);
    }

    ///--[Clean Up]
    //--Undo the size change.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(1.0f / pSize, 1.0f / pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--Undo by cursor.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);

    ///--[Kerning]
    //--If no next letter is provided, we return 0.0f.
    if(pNextLetter == 0)
    {
        return tDefault;
    }

    //--Get associated kerning.
    float cKerning = mPrecacheData[tGlyphLft].mWidth;
    if(tGlyphLft == 65306)
    {
        cKerning = mMonospacing;
    }
    else if(pNextLetter >= SUGARFONT_PRECACHE_OFF && pNextLetter < SUGARFONT_PRECACHE_HI)
    {
        cKerning = mPrecacheData[tGlyphLft].mKerningDistance[pNextLetter-SUGARFONT_PRECACHE_OFF];
    }
    else if(mMonospacing != -1.0f)
    {
        cKerning = mMonospacing;
    }

    //--A lookup table will provide the distance to the next letter.
    return cKerning;
}

///--[Fixed Sizes]
void StarFont::DrawTextFixed(float pX, float pY, float pW, int pFlags, const char *pText)
{
    ///--[Documentation]
    //--Given a fixed width, renders the text by stretching it if it exceeds that width. This is useful for the UI to prevent
    //  overruns for really long names.
    //--Note that not all flags will be able to correctly accomodate the scaling case.
    if(pW < 1.0f) return;

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawTextFixed() - Warning. Attempted to use dummy font %s\n", mLocalName); return; }

    ///--[Length Check]
    //--If the length of the string to render is less than the alotted width, just render it normally.
    float cRenderLen = GetTextWidth(pText);
    if(cRenderLen <= pW)
    {
        DrawText(pX, pY, pFlags, 1.0f, pText);
        return;
    }

    ///--[Position]
    //--Translate to the rendering position.
    glTranslatef(pX, pY, 0.0f);

    //--Compute the X Scale.
    float cXScale = pW / cRenderLen;
    glScalef(cXScale, 1.0f, 1.0f);

    //--Render.
    DrawText(0, 0, pFlags, 1.0f, pText);

    ///--[Clean Up]
    //--Unscale.
    glScalef(1.0f / cXScale, 1.0f, 1.0f);

    //--Untranslate.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);
}
void StarFont::DrawTextFixedArgs(float pX, float pY, float pW, int pFlags, const char *pText, ...)
{
    ///--[Documentation]
    //--Same as above, but compiles the string from variable arguments first.

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawTextFixedArgs() - Warning. Attempted to use dummy font %s\n", mLocalName); return; }

    ///--[Execution]
    va_list tArgList;
    va_start(tArgList, pText);

    //--Create a buffer.
    char tBuffer[SUGARFONT_VAARG_BUF_SIZE];
    int tLettersPrinted = vsprintf(tBuffer, pText, tArgList);
    va_end (tArgList);

    //--Warning: String was larger than the buffer size. Can cause errors, means the coder should
    //  increase the buffer size or go through pain-in-the-ass efforts to make variable buffering possible.
    if(tLettersPrinted >= SUGARFONT_VAARG_BUF_SIZE)
    {
        fprintf(stderr, "Warning: DrawTextFixedArgs() printed too many letters (%i) to buffer of size %i.\n", tLettersPrinted, SUGARFONT_VAARG_BUF_SIZE);
    }

    //--Call.
    DrawTextFixed(pX, pY, pW, pFlags, tBuffer);
}

#endif
