///======================================== AdvUIRelive ===========================================
//--Grid that lets the player pick a cutscene to relive.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIGrid.h"

///===================================== Local Structures =========================================
#ifndef ADVUI_RELIVE_DEFINITIONS
#define ADVUI_RELIVE_DEFINITIONS

///--[Relive Package]
//--Contains a path and an image, used for the Relive submenu.
typedef struct RelivePackage
{
    //--Members
    char *mScriptPath;

    //--Display
    AdvMenuStandardAlignments mAlignments;

    //--Functions
    void Initialize()
    {
        mScriptPath = NULL;
        mAlignments.Initialize();
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        RelivePackage *rPtr = (RelivePackage *)pPtr;
        free(rPtr->mScriptPath);
        free(rPtr);
    }
}RelivePackage;
#endif

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUIRelive : virtual public AdvUICommon, virtual public AdvUIGrid
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxAdvReliveHelpStrings = 9;

    ///--[Variables]
    //--Data.
    StarLinkedList *mReliveData; //RelivePackage *, master

    //--Cursor
    bool mDontShowCursor;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Strings
    StarlightString *mShowHelpString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Mainline;

        //--Common
        StarBitmap *rOverlay_Footer;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIRelive();
    virtual ~AdvUIRelive();
    virtual void Construct();

    //--Public Variables
    static char *xReliveResolveScript;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    void RegisterReliveScript(const char *pDisplayName, const char *pScriptPath, const char *pImagePath);
    void SetReliveAlignments(const char *pDisplayName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


