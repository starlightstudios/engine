//Replace MonoUISample with name of UI
//Replace AdvUISample with name of UI
//Update loading instructions with the correct type
//Delete these instructions

//--Base
#include "MonoUISample.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
MonoUISample::MonoUISample()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUISample ======= ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUISample ====== ]
    ///--[System]
    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUISample::~MonoUISample()
{
}
void MonoUISample::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    //ResolveSeriesInto("Monoceros Type UI",           &MonoImages, sizeof(MonoImages));
    //ResolveSeriesInto("Adventure Type UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",           &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUISample::TakeForeground()
{

}

///======================================= Core Methods ===========================================
void MonoUISample::RefreshMenuHelp()
{

}
void MonoUISample::RecomputeCursorPositions()
{

}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUISample::UpdateForeground(bool pCannotHandleUpdate)
{
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }
    return true;
}
void MonoUISample::UpdateBackground()
{

}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
