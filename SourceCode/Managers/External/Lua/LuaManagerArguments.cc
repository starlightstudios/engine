//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///===================================== Property Queries =========================================
int LuaManager::GetTotalScriptArguments()
{
    return mTotalArgs;
}
char *LuaManager::GetScriptArgument(int pSlot)
{
    if(pSlot < 0 || pSlot >= mTotalArgs) return NULL;
    return (char *)mScriptArguments[pSlot];
}

///======================================= Manipulators ===========================================
void LuaManager::SetArgumentListSize(int pSize)
{
    ClearArgumentList();
    if(pSize < 1) return;

    mTotalArgs = pSize;
    SetMemoryData(__FILE__, __LINE__);
    mScriptArguments = (char **)starmemoryalloc(sizeof(char *) * mTotalArgs);
    memset(mScriptArguments, 0, sizeof(char *) * mTotalArgs);
}
void LuaManager::AddArgument(float pNumber)
{
    //--Overload, uses a floating-point number. Integers passed in will convert correctly because
    //  we allocate 32 characters.
    if(mArgSlot < 0 || mArgSlot >= mTotalArgs) return;

    SetMemoryData(__FILE__, __LINE__);
    char *nBuffer = (char *)starmemoryalloc(sizeof(char) * 32);
    sprintf(nBuffer, "%f", pNumber);

    mScriptArguments[mArgSlot] = nBuffer;
    mArgSlot ++;
}
void LuaManager::AddArgument(const char *pString)
{
    //--String version, the string is COPIED.
    if(mArgSlot < 0 || mArgSlot >= mTotalArgs || !pString) return;

    SetMemoryData(__FILE__, __LINE__);
    char *nBuffer = (char *)starmemoryalloc(sizeof(char) * (strlen(pString) + 1));
    strcpy(nBuffer, pString);

    mScriptArguments[mArgSlot] = nBuffer;
    mArgSlot ++;
}

///======================================= Core Methods ===========================================
void LuaManager::ClearArgumentList()
{
    //--Deallocate.
    for(int i = 0; i < mTotalArgs; i ++)
    {
        free(mScriptArguments[i]);
    }
    free(mScriptArguments);

    //--Reset.
    mArgSlot = 0;
    mTotalArgs = 0;
    mScriptArguments = NULL;
}
