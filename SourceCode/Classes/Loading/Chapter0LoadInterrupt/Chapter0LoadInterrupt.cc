//--Base
#include "Chapter0LoadInterrupt.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
Chapter0LoadInterrupt::Chapter0LoadInterrupt()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======== StarLoadInterrupt ========= ]
    ///--[ ======= Chapter0LoadInterrupt ====== ]
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    mIsReady = false;
    mLoading = NULL;
    for(int i = 0; i < LI_CH0_DOTS; i ++) mLoadingPeriod[i] = NULL;
}
Chapter0LoadInterrupt::~Chapter0LoadInterrupt()
{
    delete mLoading;
    for(int i = 0; i < LI_CH0_DOTS; i ++) delete mLoadingPeriod[i];
}
void Chapter0LoadInterrupt::Construct()
{
    ///--[Documentation]
    //--Loads and verifies images associated with this object. Because this object is meant to appear during
    //  the loading of other objects, it handles its images internally, not using a conventional DataLibrary.

    ///--[Load Frames]
    //--Loading animation is a filmstrip series placed in a linked list.
    StarLumpManager *tSLM = new StarLumpManager();
    tSLM->Open("Data/Loading.slf");
    if(!tSLM->IsFileOpen()) return;

    //--Suppress the load interrupt.
    bool tOldFlag = StarBitmap::xSuppressInterrupt;
    StarBitmap::xSuppressInterrupt = true;

    //--Load.
    mLoading = tSLM->GetImage("Chapter0|Loading");
    char tBuffer[128];
    for(int i = 0; i < LI_CH0_DOTS; i ++)
    {
        sprintf(tBuffer, "Chapter0|LoadPeriod%i", i);
        mLoadingPeriod[i] = tSLM->GetImage(tBuffer);
    }

    //--Verify.
    mIsReady = mLoading;
    for(int i = 0; i < LI_CH0_DOTS; i ++)
    {
        if(!mLoadingPeriod[i]) mIsReady = false;
    }

    ///--[Finish Up]
    //--Set to the String Tyrant variant. If anything went wrong, the default one will be used.
    StarBitmap::xInterruptCall = &Chapter0LoadInterrupt::Interrupt;

    //--Set this flag.
    mIsAwaitingKeypress = false;

    //--Clean up.
    delete tSLM;
    StarBitmap::xSuppressInterrupt = tOldFlag;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void Chapter0LoadInterrupt::DeallocateImages()
{
    ///--[Documentation]
    //--Deallocates and NULLs all images associated with this object.
    mIsReady = false;
    StarLoadInterrupt::DeallocateImages();

    //--Derived call.
    delete mLoading;
    for(int i = 0; i < LI_CH0_DOTS; i ++) delete mLoadingPeriod[i];

    //--NULL.
    mLoading = NULL;
    for(int i = 0; i < LI_CH0_DOTS; i ++) mLoadingPeriod[i] = NULL;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void Chapter0LoadInterrupt::Call(bool pForceRender)
{
    ///--[Documentation and Setup]
    //--Chapter 2 Load Interrupt renderer. Doesn't do anything yet.
    if(!CommonLogicUpdate(pForceRender) || !mIsReady) return;

    //--Resolve how long the loading bar ought to be. Clipped 0 to 1.
    float tBarPercent = (float)mCurrent / ((float)(mCurrentMax) * 0.95f);
    if(tBarPercent > 1.0f) tBarPercent = 1.0f;
    if(tBarPercent < 0.0f) tBarPercent = 0.0f;

    //--Call the renderer setup. The screen is now ready for drawing.
    CommonRendererSetup();

    ///--[Loading Text]
    //--Loading text. Underlay always renders the same color.
    float cLft = ((1366.0f) * 0.50f) - (mLoading->GetTrueWidth() * 0.50f);
    float cTop = 250.0f;
    mLoading->Draw(cLft, cTop);

    //--Determine how many periods to render.
    int cDotsToRender = (mFrame / 10) % (LI_CH0_DOTS+1);
    for(int i = 0; i < cDotsToRender; i ++)
    {
        mLoadingPeriod[i]->Draw(cLft, cTop);
    }

    ///--[Finish Up]
    CommonRendererClean();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void Chapter0LoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_BootChapter0()
       Boots the default Chapter 0 load interrupt. */
    lua_register(pLuaState, "LI_BootChapter0", &Hook_LI_BootChapter0);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LI_BootChapter0(lua_State *L)
{
    //LI_BootChapter0()

    //--Create a new LoadInterrupt.
    Chapter0LoadInterrupt *nInterrupt = new Chapter0LoadInterrupt();
    nInterrupt->Construct();

    //--Dealloacte the old one.
    GLOBAL *rGlobal = Global::Shared();
    delete rGlobal->gLoadInterrupt;

    //--Replace.
    rGlobal->gLoadInterrupt = nInterrupt;
    return 0;
}
