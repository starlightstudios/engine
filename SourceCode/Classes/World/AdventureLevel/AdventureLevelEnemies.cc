//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdvCombat.h"
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"
#include "EntityManager.h"

///========================================== Static ==============================================
bool AdventureLevel::IsEnemyDead(const char *pName)
{
    ///--[Documentation]
    //--Resolves if the enemy is "Dead" and returns true if it is, false if not. "Dead" means the enemy was
    //  previously defeated, mugged, or manually flagged to not spawn.

    ///--[Destroyed Check]
    //--If the enemy is on the destroyed list, it's dead.
    if(xDestroyedEnemiesList->GetElementByName(pName) != NULL)
    {
        //--In randomly generated levels, this always fails. Enemies always spawn.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && !strcasecmp(rActiveLevel->GetName(), "RandomLevel")) return false;

        //--Otherwise, it's dead.
        return true;
    }

    ///--[NPC Check]
    //--If the enemy is currently available as an NPC, it's not dead.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Entity must be of the right type.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;

            //--Is it the enemy in question? It's alive, then.
            if(rActor->IsEnemy(pName))
            {
                rEntityList->PopIterator();
                return false;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    ///--[All Checks Failed]
    //--No NPC was found holding this entity, so it must be dead.
    return true;
}
bool AdventureLevel::IsEnemyMugged(const char *pName)
{
    //--If the enemy is on the mugged list, it has been mugged.
    if(xMuggedEnemiesList->GetElementByName(pName) != NULL)
    {
        //--In randomly generated levels, this always fails.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && !strcasecmp(rActiveLevel->GetName(), "RandomLevel")) return false;

        //--Otherwise, it has been mugged.
        return true;
    }

    //--Entity was not on the mugged list, therefore, it was not mugged.
    return false;
}
const char *AdventureLevel::GetEnemyNameFromPattern(const char *pPattern)
{
    //--Scans the mEnemySpawnList to see if any of the spawn packages match the pattern. The first one found
    //  is returned, even if there are multiple possible. Passes NULL if none are found.
    if(!pPattern) return NULL;

    //--Check pattern.
    if(pPattern[0] == '\0') return NULL;
    int tPatternLen = (int)strlen(pPattern);

    //--Iterate.
    SpawnPackage *rPackage = (SpawnPackage *)mEnemySpawnList->PushIterator();
    while(rPackage)
    {
        if(!strncmp(pPattern, rPackage->mRawName, tPatternLen))
        {
            mEnemySpawnList->PopIterator();
            return rPackage->mRawName;
        }

        rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
    }

    //--None found.
    return NULL;
}

///======================================= Manipulators ===========================================
void AdventureLevel::RegisterEnemyPack(SpawnPackage *pPackage)
{
    if(!pPackage) return;
    mEnemySpawnList->AddElement("X", pPackage, &FreeThis);
}
void AdventureLevel::KillEnemy(const char *pName)
{
    static int xDummy = 0;
    xDestroyedEnemiesList->AddElement(pName, &xDummy);
}
void AdventureLevel::MarkEnemyMugged(const char *pName)
{
    static int xDummy = 0;
    xMuggedEnemiesList->AddElement(pName, &xDummy);
}
void AdventureLevel::KillSpawnedEnemy(const char *pName)
{
    //--If an enemy has already spawned and we want to remove it after the fact, we do this instead of KillEnemy().
    KillEnemy(pName);

    //--Find the corresponding field actor and remove it.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Entity must be of the right type.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;

            //--Is it the enemy in question? It's alive, then.
            if(rActor->IsEnemy(pName))
            {
                rActor->mSelfDestruct = true;
                rEntityList->PopIterator();
                break;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::WipeDestroyedEnemies()
{
    xDestroyedEnemiesList->ClearList();
    xMuggedEnemiesList->ClearList();
}

///======================================= Core Methods ===========================================
void AdventureLevel::SpawnEnemies()
{
    ///--[Documentation]
    //--Spawns enemies within the level, based on a set of spawn packages stored in the level.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    const char *rParagonScript = rAdventureCombat->GetParagonScript();

    //--Check if this is a randomly generated level. If so, don't check the destroyed list.
    bool tBypassDestroyedList = (!strcasecmp(mName, "RandomLevel"));

    ///--[Seed Number]
    //--Locate the current seed number. This is used for specially flagged entities who may or may not spawn
    //  based on a random seed.
    int cSpawnSeed = 0;
    SysVar *rSpawnSeedPtr = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Enemies/Random/iRandomSpawnSeed");
    if(rSpawnSeedPtr)
    {
        cSpawnSeed = (int)rSpawnSeedPtr->mNumeric;
    }

    ///--[Iterate]
    //--Go through all the enemy spawn packs and spawn any that aren't dead.
    SpawnPackage *rPackage = (SpawnPackage *)mEnemySpawnList->PushIterator();
    while(rPackage)
    {
        ///--[Normal Spawn]
        //--Check if this enemy is "dead", but not on the destroyed list. If it is "dead", spawn it. Dead simply means
        //  they are not currently on the map.
        if(IsEnemyDead(rPackage->mUniqueSpawnName) || tBypassDestroyedList)
        {
            ///--[Don't Spawn If Dead]
            //--If the enemy was previously defeated in combat, check if this flag is set. If so, the enemy does not spawn at all.
            //  This is used for things that don't leave remnants, like doctor bag entities.
            if(xDestroyedEnemiesList->GetElementByName(rPackage->mUniqueSpawnName) != NULL && !tBypassDestroyedList && rPackage->mDontSpawnIfDead)
            {
                rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
                continue;
            }

            //--Check if the enemy was KO'd in combat.
            //bool tWasKOd = false;
            //if(xDestroyedEnemiesList->GetElementByName(rPackage->mUniqueSpawnName) != NULL && !tBypassDestroyedList) tWasKOd = true;

            ///--[Paragons]
            //--Paragon handler. Call the current paragon script with the name passed in. The script will
            //  toggle a value to indicate if this is a paragon enemy or not. Second argument is the name of this entity.
            //--If the paragon group is "NULL", it has no impact.
            bool tIsParagon = false;
            if(rParagonScript && strcasecmp(rPackage->mParagonGroup, "Null"))
            {
                //--Run the paragon script with the grouping.
                rAdventureCombat->SetParagonFlag(ADVCOMBAT_PARAGON_NO);
                LuaManager::Fetch()->ExecuteLuaFile(rParagonScript, 2, "S", rPackage->mParagonGroup, "S", rPackage->mUniqueSpawnName);

                //--Check.
                int tLastParagonValue = rAdventureCombat->GetLastParagonValue();
                if(tLastParagonValue == ADVCOMBAT_PARAGON_YES)
                {
                    tIsParagon = true;
                }
                //--Despawn the enemy.
                else if(tLastParagonValue == ADVCOMBAT_PARAGON_DESPAWN)
                {
                    rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
                    continue;
                }
            }

            ///--[Seeded Randomization]
            //--If this package has a flag on it, it only spawns sometimes, and the seed for this is randomized
            //  whenever the player rests.
            if(rPackage->mUseSeededSpawnChance)
            {
                //--Debug.
                //fprintf(stderr, "Using seeded spawn chance for %s.\n", rPackage->mRawName);
                //fprintf(stderr, " Current seed: %i\n", cSpawnSeed);
                //fprintf(stderr, " Current level name: %s\n", mName);

                //--Sum the letters of the level name plus the package name to get a unique id.
                int tWordSums = 0;
                for(int i = 0; i < (int)strlen(mName); i ++) tWordSums += mName[i];
                for(int i = 0; i < (int)strlen(rPackage->mRawName); i ++) tWordSums += rPackage->mRawName[i];
                //fprintf(stderr, " Sum of names: %i\n", tWordSums);

                //--Add the seed number.
                tWordSums += cSpawnSeed;
                //fprintf(stderr, " Sum of names plus seed: %i\n", tWordSums);

                //--Run a PRNG on it to make it obfuscated.
                tWordSums = abs(8253729 * tWordSums + 2396403);
                //fprintf(stderr, " After PRNG: %i\n", tWordSums);
                //fprintf(stderr, " After modulus: %i vs %i\n", tWordSums % 100, rPackage->mSeedSpawnChance);

                //--Modulus it down to 100 and check it against the spawn chance.
                if(tWordSums % 100 <= rPackage->mSeedSpawnChance)
                {
                    //fprintf(stderr, " Spawning.\n");
                }
                else
                {
                    //fprintf(stderr, " Failed to spawn.\n");
                    rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
                    continue;
                }
            }

            ///--[Basic Creation]
            //--Create, order the entity to resolve its properties from the provided package.
            TilemapActor *nActor = new TilemapActor();
            nActor->SetEnemyPropertiesFromPackage(rPackage, tIsParagon);

            //--If the entity is on the mugged list, they don't give additional loot when mugged.
            if(IsEnemyMugged(rPackage->mUniqueSpawnName))
            {
                nActor->SetMugLootable(false);
            }

            ///--[Register]
            //--Register to the EntityManager.
            EntityManager::Fetch()->RegisterPointer(rPackage->mRawName, nActor, &RootEntity::DeleteThis);

            ///--[Knockout Case]
            //--If the enemy was previously defeated in combat, KO it.
            if(xDestroyedEnemiesList->GetElementByName(rPackage->mUniqueSpawnName) != NULL && !tBypassDestroyedList)
            {
                nActor->DieQuickly();
            }
        }

        //--Next.
        rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
    }
}
void AdventureLevel::CheckActorAgainstInvisZones(TilemapActor *pActor)
{
    //--Checks the given actor against all invisible zones. If the actor is in such a zone, sets the static flag so enemies
    //  know to ignore the actor.
    if(!pActor) return;

    //--Iterate.
    InvisibleZone *rPackage = (InvisibleZone *)mInvisibleZoneList->PushIterator();
    while(rPackage)
    {
        //--If the package is not active, ignore it.
        if(!rPackage->mIsActivated)
        {

        }
        //--Check for a full collision.
        else if(pActor->GetWorldX() >= rPackage->mDimensions.mLft && pActor->GetWorldX() + TA_SIZE <= rPackage->mDimensions.mRgt &&
           pActor->GetWorldY() >= rPackage->mDimensions.mTop && pActor->GetWorldY() + TA_SIZE <= rPackage->mDimensions.mBot)
        {
            xIsPlayerInInvisZone = true;
        }
        //--Check for a partial collision.
        else if(IsCollision2DReal(rPackage->mDimensions, pActor->GetWorldX(), pActor->GetWorldY(), pActor->GetWorldX() + TA_SIZE, pActor->GetWorldY() + TA_SIZE))
        {
            xIsPlayerInInvisZone = true;
        }
        //--No hit.
        else
        {
        }

        //--If this flag is set, stop iterating here.
        if(xIsPlayerInInvisZone)
        {
            mInvisibleZoneList->PopIterator();
            break;
        }

        //--Next package.
        rPackage = (InvisibleZone *)mInvisibleZoneList->AutoIterate();
    }
}
