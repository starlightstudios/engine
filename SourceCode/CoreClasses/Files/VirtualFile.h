///======================================== VirtualFile ===========================================
//--Emulates reading and writing with file streams. This is intended to improve loading times. It
//  is much faster to read the entire file in one operation than to read it in hundreds of smaller
//  operations, as the hard-disk has to spin up to do each job. VirtualFiles can still be operated
//  in stream mode to provide backwards compatibility.
//--To operate in stream mode, set xUseRAMLoading to false before opening the file. This can be
//  optionally changed by the constructor. Also, if the OS fails to provide enough memory to a
//  memory-load file, it will switch to stream-load UNLESS it explicitly must be a memory-load
//  in which case the file will act like it failed to open.
//--If you intend to write to the file and not just read from it, the file must be in stream mode.
//  Read-only operations can be in memory or stream modes.

#pragma once

///========================================= Includes =============================================
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
class StarArray;

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class VirtualFile
{
    private:
    //--System
    bool mIsReady;
    bool mIsRAMLoading;
    bool mUseOneByteForStringLengths;

    //--Storage
    unsigned long int mFileLength;
    StarArray *mData;
    //unsigned char *mData;

    //--Reading
    FILE *fInfile;
    unsigned long int mFilePointer;

    protected:

    public:
    //--System
    VirtualFile(const char *pPath, bool pMustBeMemoryMode);
    ~VirtualFile();

    //--Public Variables
    bool mAssumeZeroIsMax;
    bool mDebugFlag;
    static bool xUseRAMLoading;

    //--Property Queries
    bool GetOneByteForLengthsFlag();
    bool IsReady();
    bool IsInMemoryMode();
    long int GetLength();
    long int GetCurPosition();
    void Read(void *pDest, int pSizePerItem, int pItemsTotal);
    char *ReadLenString();
    void ReadImage(void *pDest, int pXOrig, int pYOrig, int pXNew, int pYNew, int pSizePerItem);

    //--Manipulators
    void SetUseOneByteForStringLengths(bool pFlag);
    void Seek(int pPosition);
    void SeekRelative(int pChange);

    //--Core Methods
    void SkipLenString();

    //--Update
    //--File I/O
    void Write(const char *pFileDest);

    //--Drawing
    //--Pointer Routing
    StarArray *GetData();
    //unsigned char *GetData();

    //--Lua Hooking
};

//--Hooking Functions

