///==================================== ScriptObjectBuilder =======================================
//--A world interface typically called from the main menu, contains a list of StarObjectPrototypes.
//  The user can select one and then edit all the different fields inside it, then generate a
//  script from it which allows for rapid prototyping of script objects with fewer errors than by-hand.
//--This is a utility object and is not meant for casual users. Modders may get some use out of it.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define SOB_MODE_PROTOTYPE 0
#define SOB_MODE_FIELD 1
#define SOB_MODE_EDIT 2

#define SOB_EDIT_BUF_SIZE 33

///========================================== Classes =============================================
class ScriptObjectBuilder : public RootLevel
{
    private:
    ///--[System]
    int mCurrentMode;
    int mItemsPerPage;
    StarLinkedList *mPrototypeList; //StarObjectPrototype *, master

    ///--[Prototype Mode]
    int mPrototypeCursor;
    int mPrototypeSkip;

    ///--[Fields Mode]
    int mFieldCursor;
    int mFieldSkip;
    StarObjectPrototype *rActivePrototype;

    ///--[Editing Mode]
    int mEditCursor;
    char mEditingBuffer[SOB_EDIT_BUF_SIZE];

    ///--[Render Constants]
    int cListIndentLines;
    float cListBottomY;
    float cDivisorTop;
    float cDivisorBot;
    float cExplanationTop;

    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            StarFont *rFontMainline;
        }Data;
    }Images;

    protected:

    public:
    //--System
    ScriptObjectBuilder();
    virtual ~ScriptObjectBuilder();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void RegisterPrototype(StarObjectPrototype *pPrototype);

    //--Core Methods
    void AppendToEditBuffer(const char *pString);
    void ConditionalAppendToEditBuffer(const char *pString, const char *pString2, bool pCondition);
    void HandleExport();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    void UpdatePrototype();
    void UpdateFields();
    void UpdateFieldEdit();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();
    void RenderField(float pNameX, float pValX, float pY, StarObjectField *pField);
    void RenderPrototypes();
    void RenderFields();
    void RenderEditBuf(float pX, float pY);

    //--Pointer Routing
    StarFont *GetMainlineFont();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SOB_Create(lua_State *L);
int Hook_SOB_CreateEntry(lua_State *L);

