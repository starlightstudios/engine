///========================================= AdvUIWarp ============================================
//--Warp UI, allows the user to select regions and then areas to warp to, and then executes a lua
//  script to warp to that location.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIGrid.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUIWarp : virtual public AdvUICommon, virtual public AdvUIGrid
{
    protected:
    ///--[Constants]
    //--Scrollbar
    static const int cxAdvScrollBuf = 2;
    static const int cxAdvScrollPage = 6;

    //--Timers
    static const int cxAdvWarpScrollTicks = 15;
    static const int cxAdvWarpOffsetTicks = 15;

    //--Sizes
    static constexpr float cxAdvMaxLocationNameWid = 436.0f;

    //--Help
    static const int cxAdvHelpStrings = 9;          //Total number of strings in the help display.

    ///--[System]
    int mLocationVisTimer;
    int mAdvancedPackRoll;
    int mAdvancedPackRerollTimer;

    ///--[Display]
    float mWarpZoomFactor;

    ///--[Region Grid]
    StarLinkedList *mRegionData; //WarpRegionPack *, master. The AdvUIGrid value mCode stores the index in this list.

    ///--[Cursor]
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    ///--[Location Selection]
    bool mIsSelectingLocation;
    int mLocationOffsetTimer;
    int mLocationCursor;
    int mLocationSkip;
    EasingPack2D mLocationPos;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Mainline;

        //--Images
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;
        StarBitmap *rOverlay_MapPinSelected;
        StarBitmap *rOverlay_MapPinUnselected;
        StarBitmap *rScrollbar_Static;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rUnderlay_MapLocationBacking;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIWarp();
    virtual ~AdvUIWarp();
    virtual void Construct();

    //--Public Variables
    static char *xWarpResolveScript;
    static char *xWarpExecuteScript;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    void RegisterWarpRegion(const char *pRegionName, const char *pIconPath, const char *pMapImagePath, const char *pMapOverlayPath);
    void SetWarpRegionAlignments(const char *pRegionName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei);
    void SetWarpRegionAdvancedClamps(const char *pRegionName, float pClampLft, float pClampTop, float pClampRgt, float pClampBot);
    void RegisterWarpLocation(const char *pRegionName, const char *pDisplayName, const char *pActualName, float pImgX, float pImgY);
    void RegisterWarpRegionAdvancedPack(const char *pRegionName, const char *pLayerName, const char *pImagePath);
    void SetWarpRegionPackRenderChance(const char *pRegionName, const char *pLayerName, int pLowChance, int pHighChance);
    void SetWarpRegionPackReadsStencil(const char *pRegionName, const char *pLayerName, int pStencilValue);
    void SetWarpRegionPackWritesStencil(const char *pRegionName, const char *pLayerName, int pStencilValue);
    void SetWarpRegionPackCanToggle(const char *pRegionName, const char *pLayerName, bool pCanToggle);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void ClearGrid();
    void FocusOnWarpLocation();

    ///--[Region Mode]
    void UpdateRegionMode();
    void RenderRegionMode();

    ///--[Location Mode]
    void UpdateLocationMode();
    virtual void RenderLocationMode();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvUIWarp_SetProperty(bool &sHandled, lua_State *L);
