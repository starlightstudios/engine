//--Base
#include "AdvUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "LuaManager.h"
#include "ControlManager.h"

///======================================== Activation ============================================
void AdvUIVendor::ActivateModeBuy()
{
    ///--[Documentation]
    //--Switches to buy mode.

    ///--[Variables]
    //--Common subroutines.
    ActivateMode(mIsBuyMode);
    ResetCursor();

    ///--[Other]
    //--Inventory resets the list of gems.
    AdventureInventory::Fetch()->BuildGemList();

    //--Resolve comparison character.
    RecheckComparisonCharacter(GetConsideredItem());
}

///===================================== Property Queries =========================================
float AdvUIVendor::GetBuyCursorNameWidth()
{
    ///--[Documentation]
    //--In buy mode, resolves the highlighted item and returns its width. Used for cursor size computations.
    float tNameWid = 100.0f;

    //--Get name.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
    if(rPackage && rPackage->mItem)
    {
        tNameWid = AdvImages.rFont_Mainline->GetTextWidth(rPackage->mItem->GetName());
    }

    //--Name length clamp.
    if(tNameWid > cxAdvMaxNameWid) tNameWid = cxAdvMaxNameWid;

    //--Finish up.
    return tNameWid;
}

///====================================== Primary Update ==========================================
void AdvUIVendor::UpdateModeBuy()
{
    ///--[Documentation]
    //--Update handler for buy mode.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Standard timers.
    StandardTimer(mBuyTimer,        0, cxAdvBuyTicks,      mIsBuyingItem);
    StandardTimer(mAskToEquipTimer, 0, cxAdvAskEquipTicks, mAskToEquip);

    //--Update blockers. If these timers are not capped, stop the update.
    if(mBuyTimer     > 0 && mBuyTimer     < cxAdvBuyTicks)     return;
    if(mDetailsTimer > 0 && mDetailsTimer < cxAdvDetailsTicks) return;

    ///--[Mode Handlers]
    //--In this mode, handle controls for buying a specific item. This gets its own subroutine.
    if(mIsBuyingItem) { UpdateBuyItem(); return; }

    //--After the player decides to buy an item, the game may ask the player to equip it. That is this update.
    if(mAskToEquip) { UpdateBuyEquipItem(); return; }

    //--If in Details mode, pressing cancel exits details mode. All other controls are ignored.
    if(CommonDetailsUpdate()) return;

    ///--[Shoulder Buttons]
    //--Shoulder-left. Decrements the comparison character. Does nothing if there is only one comparison character.
    if(rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel") && mShowSwapArrows)
    {
        ScrollComparisonCharacter(-1);
        return;
    }
    //--Shoulder-right. Increments the comparison character. Does nothing if there is only one comparison character.
    if(rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel") && mShowSwapArrows)
    {
        ScrollComparisonCharacter(1);
        return;
    }

    ///--[Run Button]
    //--Toggles which comparison slot is in use.
    if(rControlManager->IsFirstPress("Run") && mCanSwapCompareSlot)
    {
        ToggleComparisonSlot();
        return;
    }

    ///--[Left and Right, Change Mode]
    //--Changes modes to the left, which is Gems.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && !mIsShowingDetails)
    {
        ScrollMode(-1);
        return;
    }
    //--Changes modes to the right, which is Sell.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && !mIsShowingDetails)
    {
        ScrollMode(1);
        return;
    }

    ///--[Up and Down]
    //--Up decrements, down increments. Wrapping is enabled.
    if(AutoListUpDn(mCursor, mSkip, mShopInventory->GetListSize(), cScrollBuf, cScrollPage, true))
    {
        //--Modify cursor.
        RecomputeCursorPositions();

        //--The comparison character may have changed. Recheck that here.
        RecheckComparisonCharacter(GetConsideredItem());
        return;
    }

    ///--[Activate]
    //--Begins purchasing the highlighted item. If you don't have enough money to buy even one, fails.
    if(rControlManager->IsFirstPress("Activate") && !mIsShowingDetails)
    {
        //--Get the item in question.
        ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
        if(!rPackage) return;

        //--Make sure the item exists.
        AdventureItem *rItem = rPackage->mItem;
        if(!rItem) return;

        //--If the item has 0 quantity available, stop.
        if(rPackage->mQuantity < 1 && rPackage->mQuantity != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Get how much the item will cost. If we don't have enough money to buy one, fail.
        int tCost = rItem->GetValue();
        if(rPackage->mPriceOverride > -1) tCost = rPackage->mPriceOverride;
        if(tCost > AdventureInventory::Fetch()->GetPlatina())
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Otherwise, activate buying mode.
        mIsBuyingItem = true;
        mBuyQuantity = 1;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel") && !mIsShowingDetails)
    {
        HandleExit();
        return;
    }
}

///================================== Quantity/Confirm Update ======================================
void AdvUIVendor::UpdateBuyItem()
{
    ///--[Documentation and Setup]
    //--When the player has selected an item they want to buy, this handles the input. The player
    //  can adjust the quantity, purchase, or cancel out.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the item package.
    ShopInventoryPack *rPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
    if(!rPack) return;

    //--Fast-access pointer.
    AdventureItem *rItem = rPack->mItem;
    if(!rItem) return;

    ///--[Left and Right]
    //--Adjust quantity down. Wraps to maximum.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        AdjustQuantity(mBuyQuantity, -1);
        return;
    }

    //--Adjust quantity up. Wraps to minimum.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        AdjustQuantity(mBuyQuantity, 1);
        return;
    }

    ///--[Shoulder Buttons]
    //--Decrement by 10.
    if(rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel"))
    {
        AdjustQuantity(mBuyQuantity, -10);
        return;
    }
    //--Increment by 10.
    else if(rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel"))
    {
        AdjustQuantity(mBuyQuantity, 10);
        return;
    }

    ///--[Activate]
    //--Buy the item in question in the given quantity.
    if(rControlManager->IsFirstPress("Activate"))
    {
        PurchaseItem(rPack, mBuyQuantity, true, true);
        /*
        //--Fast-access pointers.
        AdventureInventory *rInventory = AdventureInventory::Fetch();

        //--Flag.
        mIsBuyingItem = false;

        //--For each time the item was purchased:
        for(int i = 0; i < mBuyQuantity; i ++)
        {
            //--Tell the owning package to increase the quantity.
            rPack->mOwnedDisplay ++;

            //--Put the item in the player's inventory if it was not adamantite.
            if(!rItem->IsAdamantite())
            {
                //--Simply run the construction script again.
                const char *rItemName = rItem->GetName();
                LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", rItemName, "N", 1.0f);
            }
            //--Adamantite. Has a special registering algorithm. If the item was flagged as "UNLIMITED" then we won't deallocate it.
            else
            {
                rInventory->RegisterAdamantite(rItem, false);
            }

            //--Get how much the item costs.
            int tItemCost = rItem->GetValue();
            if(rPack->mPriceOverride > -1) tItemCost = rPack->mPriceOverride;

            //--Decrement the player's case.
            rInventory->SetPlatina(rInventory->GetPlatina() - tItemCost);

            //--Decrement the available quantity if it's not -1.
            if(rPack->mQuantity != -1) rPack->mQuantity --;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

        //--Buy response. If this script is set, fire a script which may manipulate the inventory. The flag
        //  xDisallowEquipFromResponse should be set to true if the script is going to delete the item
        //  we just bought.
        xDisallowEquipFromResponse = false;
        if(mBuyResponseScript)
        {
            LuaManager::Fetch()->ExecuteLuaFile(mBuyResponseScript);
        }

        //--If the player purchased exactly one item, and the item is equipment that exactly one character can use in the active party
        //  then we ask them if they want to equip it now.
        if(mBuyQuantity > 1 || xDisallowEquipFromResponse) return;

        //--Scan the active characters. See how many can equip the item.
        mAskToEquipSlot = -1;
        for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
        {
            //--Get character.
            AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
            if(!rCharacter) continue;

            //--Can the character equip it?
            if(rItem->IsEquippableBy(rCharacter->GetName()))
            {
                //--If the equip slot is -1, this is the first character that can equip it.
                if(mAskToEquipSlot == -1)
                {
                    mAskToEquipSlot = i;
                }
                //--If it's not -1, then at least two characters can equip the item. Don't show the equip dialogue.
                else
                {
                    mAskToEquipSlot = -1;
                    return;
                }
            }
        }

        //--If we exited this loop with the mAskToEquipSlot being -1, nobody can equip it.
        if(mAskToEquipSlot == -1) return;

        //--If we got this far, we should show the equip prompt.
        mAskToEquip = true;
        return;
        */
    }

    ///--[Cancel]
    //--Exit without buying anything.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsBuyingItem = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///===================================== Equip Item Update ========================================
void AdvUIVendor::UpdateBuyEquipItem()
{
    ///--[Documentation]
    //--Handles the update during the small menu which asks the player to equip the item they just purchased.
    //  If nobody in the party can equip the item, or it's not equippable, this is bypassed.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Activate]
    //--Equips the item.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Flags.
        mAskToEquip = false;
        EquipPurchasedItem();

        //--SFX.
        AudioManager::Fetch()->PlaySound("World|TakeItem");
        return;
    }

    ///--[Cancel]
    //--Doesn't.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mAskToEquip = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///===================================== Segment Rendering ========================================
void AdvUIVendor::RenderLftBuy(float pVisAlpha)
{
    RenderCommonLft(pVisAlpha);
}
void AdvUIVendor::RenderTopBuy(float pVisAlpha)
{
    RenderCommonTop(pVisAlpha);
}
void AdvUIVendor::RenderRgtBuy(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    //--Comparison Block
    AdvImages.rFrame_Comparison->Draw();
    RenderBuyProperties(pVisAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderBotBuy(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Render]
    //--Inventory Listing.
    AdvImages.rFrame_Vendor->Draw();

    //--Tabs and titles.
    RenderModeTabs(cColorAlpha);
    RenderModeTitles(cColorAlpha);

    //--Render entries.
    RenderShopItemListing(mSkip, cScrollPage, cColorAlpha);

    //--Scrollbar.
    if(mShopInventory->GetListSize() > cScrollPage)
    {
        StandardRenderScrollbar(mSkip, cScrollPage, mShopInventory->GetListSize(), 219.0f, 447.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    //--Detail. Goes over the entries since the columns overwrite the black backing.
    AdvImages.rOverlay_DetailBuy->Draw();

    //--Highlight.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Help Strings.
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (AM_MAINLINE_H * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (AM_MAINLINE_H * 2.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only renders if character swapping is possible.
    if(mShowSwapArrows)
        mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (AM_MAINLINE_H * 3.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only renders if multiple compare slots are present.
    if(mCanSwapCompareSlot)
        mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (AM_MAINLINE_H * 1.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderUnalignedBuy(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders components that do not associate with a side.

    ///--[Details]
    //--Standard details renderer.
    AdventureItem *rDetailsItem = GetConsideredItem();
    RenderItemDetails(mDetailsTimer, cxAdvDetailsTicks, rDetailsItem);

    ///--[Other]
    //--Purchasing an item. Does nothing if the timer is zeroed.
    RenderBuyItem(pVisAlpha);

    //--Asking to equip an item.
    RenderBuyEquipItem(pVisAlpha);
}

///===================================== Common Rendering =========================================
void AdvUIVendor::RenderBuyProperties(float pVisAlpha)
{
    ///--[Documentation]
    //--Handles rendering the properties of the item currently highlighted, as well as comparison to the
    //  comparison character if applicable.
    RenderComparisonPane(GetConsideredItem(), pVisAlpha);
}
void AdvUIVendor::RenderBuyItem(float pVisAlpha)
{
    ///--[Documentation]
    //--When the player chooses an item to purchase, this UI appears over the rest of the UI. It shows
    //  what they are purchasing, how much it costs, and allows the quantity to be adjusted.
    float cPurchasePct = EasingFunction::QuadraticInOut(mBuyTimer, cxAdvBuyTicks) * pVisAlpha;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cPurchasePct));

    ///--[Item Resolve]
    //--Get the item package.
    ShopInventoryPack *rPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
    if(!rPack) return;

    //--Fast-access pointer.
    AdventureItem *rItem = rPack->mItem;
    if(!rItem) return;

    ///--[Rendering]
    //--Position.
    float cColorAlpha = AutoPieceOpen(DIR_UP, cPurchasePct);

    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Purchase");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Centering computations.
    char *tTextBuf = InitializeString("%s x %i", rItem->GetName(), mBuyQuantity);
    float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tTextBuf);
    float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

    //--Item Icon
    StarBitmap *rItemIcon = rItem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 205.0f);

    //--Compute the item cost.
    int tItemCost = rItem->GetValue();
    if(rPack->mPriceOverride > -1) tItemCost = rPack->mPriceOverride;
    int tTotalCost = tItemCost * mBuyQuantity;

    //--Item name.
    AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, tTextBuf);
    AdvImages.rFont_Mainline->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 230.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Costs %i Platina (You have %i)", tTotalCost, AdventureInventory::Fetch()->GetPlatina());

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(579.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mAdjustQuantityString-> DrawText(757.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();

    //--Clean.
    free(tTextBuf);
}
void AdvUIVendor::RenderBuyEquipItem(float pVisAlpha)
{
    ///--[Documentation]
    //--This display pops up when the player purchases an item and asks them to equip it.
    float cPurchasePct = EasingFunction::QuadraticInOut(mAskToEquipTimer, cxAdvAskEquipTicks) * pVisAlpha;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cPurchasePct));

    ///--[Item Resolve]
    //--Get the character who should equip it.
    AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(mAskToEquipSlot);
    if(!rCharacter) return;

    //--Get their name.
    const char *rEquipName = rCharacter->GetDisplayName();
    if(!rEquipName) return;

    ///--[Rendering]
    //--Position.
    AutoPieceOpen(DIR_UP, cPurchasePct);

    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--Text.
    AdvImages.rFont_Heading->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equip this item to %s?", rEquipName);

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR,                          1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(993.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
