//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatPrediction.h"

//--CoreClasses
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
//--Managers

///========================================== System ==============================================
void AdvCombat::CreatePredictionBox(const char *pBoxName)
{
    //--Error check.
    if(!pBoxName) return;

    //--Create.
    AdvCombatPrediction *nBox = new AdvCombatPrediction();
    nBox->SetBorderCard(Images.Data.rPredictionBox, 6.0f);
    nBox->SetFont(Images.Data.rPredictionFont);

    //--Store.
    mPredictionBoxes->AddElement(pBoxName, nBox, &RootObject::DeleteThis);

    //--If there is a target cluster active, associate us with that cluster.
    if(rPredictionCluster)
    {
        rPredictionCluster->mrPredictionBoxList->AddElementAsTail("X", nBox);
    }
}

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void AdvCombat::AllocatePredictionBoxStrings(const char *pBoxName, int pStringsTotal)
{
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;
    rPredictionBox->AllocateStrings(pStringsTotal);
}
void AdvCombat::SetPredictionBoxOffsets(const char *pBoxName, float pXOffset, float pYOffset)
{
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;
    rPredictionBox->SetOffsets(pXOffset, pYOffset);
}
void AdvCombat::SetPredictionBoxHostI(const char *pBoxName, uint32_t pHostID)
{
    //--Note: Can legally set the entity to NULL to clear it.
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;

    AdvCombatEntity *rEntity = GetEntityByID(pHostID);
    rPredictionBox->SetHostEntity(rEntity);
}
void AdvCombat::SetPredictionBoxStringText(const char *pBoxName, int pStringIndex, const char *pText)
{
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;
    StarlightString *rInternalString = rPredictionBox->GetString(pStringIndex);
    if(!rInternalString) return;
    rInternalString->SetString(pText);
}
void AdvCombat::SetPredictionBoxStringImagesTotal(const char *pBoxName, int pStringIndex, int pImagesTotal)
{
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;
    StarlightString *rInternalString = rPredictionBox->GetString(pStringIndex);
    if(!rInternalString) return;
    rInternalString->AllocateImages(pImagesTotal);
}
void AdvCombat::SetPredictionBoxStringImage(const char *pBoxName, int pStringIndex, int pSlot, float pYOffset, const char *pPath)
{
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;
    StarlightString *rInternalString = rPredictionBox->GetString(pStringIndex);
    if(!rInternalString) return;
    rInternalString->SetImageS(pSlot, pYOffset, pPath);
}

///======================================== Core Methods ==========================================
void AdvCombat::BuildPredictionBoxes()
{
    //--Once the player has selected an ability and target clusters are set up, run the prediction algorithm. Each
    //  TargetCluster has a list of all prediction boxes associated with it.
    if(!rActiveAbility) return;

    //--For each target cluster:
    TargetCluster *rTargetCluster = (TargetCluster *)mTargetClusters->PushIterator();
    while(rTargetCluster)
    {
        //--Run the ability script for prediction box with the given index.
        rPredictionCluster = rTargetCluster;
        rActiveAbility->CallCode(ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX);
        rPredictionCluster = NULL;

        //--Next.
        rTargetCluster = (TargetCluster *)mTargetClusters->AutoIterate();
    }
}
void AdvCombat::RunPredictionBoxCalculations()
{
    //--Order all prediction boxes to perform their calculations at once. This also cross references images.
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->PushIterator();
    while(rPredictionBox)
    {
        rPredictionBox->RunCalculations();
        rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->AutoIterate();
    }
}
void AdvCombat::SetClusterAsActive(int pIndex)
{
    //--Marks the given target cluster as active, causing all its prediction boxes to show themselves
    //  and all other prediction boxes to hide.
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->PushIterator();
    while(rPredictionBox)
    {
        rPredictionBox->SetShowing(false);
        rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->AutoIterate();
    }

    //--Order all prediction boxes in the indexed target cluster to show.
    TargetCluster *rTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(pIndex);
    if(!rTargetCluster) return;

    //--Iterate.
    rPredictionBox = (AdvCombatPrediction *)rTargetCluster->mrPredictionBoxList->PushIterator();
    while(rPredictionBox)
    {
        rPredictionBox->SetShowing(true);
        rPredictionBox = (AdvCombatPrediction *)rTargetCluster->mrPredictionBoxList->AutoIterate();
    }
}
void AdvCombat::ClearPredictionBoxes()
{
    //--Order all prediction boxes to start hiding, causing them to vanish when their update timers expire.
    mIsClearingPredictionBoxes = true;
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->PushIterator();
    while(rPredictionBox)
    {
        rPredictionBox->SetShowing(false);
        rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->AutoIterate();
    }
}
void AdvCombat::ClearPredictionBoxesNow()
{
    //--Wipes prediction boxes without allowing them to fade out.
    mPredictionBoxes->ClearList();
}

///=========================================== Update =============================================
void AdvCombat::UpdatePredictionBoxes()
{
    //--Updates prediction boxes, removing the ones that are invisible and not showing.
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->SetToHeadAndReturn();
    while(rPredictionBox)
    {
        //--Run.
        rPredictionBox->Update();

        //--If not visible and not showing, remove.
        if(mIsClearingPredictionBoxes && !rPredictionBox->IsVisible() && !rPredictionBox->IsShowing())
        {
            mPredictionBoxes->RemoveRandomPointerEntry();
        }

        //--Next.
        rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->IncrementAndGetRandomPointerEntry();
    }
}

///========================================== Drawing =============================================
void AdvCombat::RenderPredictionBoxes()
{
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->PushIterator();
    while(rPredictionBox)
    {
        rPredictionBox->Render();
        rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->AutoIterate();
    }
}

///====================================== Pointer Routing =========================================
TargetCluster *AdvCombat::GetPredictionTargetCluster()
{
    //--Will be NULL unless ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX is running.
    return rPredictionCluster;
}
