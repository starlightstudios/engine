-- |[ ================================ StatGenerator Chapter 1 ================================= ]|
--Chapter 1 functions.

-- |[ ============================== StatGenerator:fnChapter1HP() ============================== ]|
--Generates an HP value given a level and grade.
function StatGenerator:fnChapter1HP(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Health consists of a flat starting bonus and then a factor. Health increases linearly by level.
    local faGradeFactors = {}
    faGradeFactors[0] = { 2, 10}      --Grade F
    faGradeFactors[1] = { 3, 12}      --Grade E
    faGradeFactors[2] = { 3, 12}      --Grade D
    faGradeFactors[3] = { 4, 17}      --Grade C
    faGradeFactors[4] = { 5, 20}      --Grade B
    faGradeFactors[5] = { 5, 24}      --Grade A
    faGradeFactors[6] = { 6, 27}      --Grade S
    faGradeFactors[7] = { 7, 31}      --Grade S+
    faGradeFactors[8] = {10, 35}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter1Atk() ============================== ]|
--Generates an Attack value given a level and grade.
function StatGenerator:fnChapter1Atk(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Attack consists of a flat starting bonus and then a factor. Attack increases linearly by level.
    local faGradeFactors = {}
    faGradeFactors[0] = { 5, 11}      --Grade F
    faGradeFactors[1] = { 7, 14}      --Grade E
    faGradeFactors[2] = { 8, 17}      --Grade D
    faGradeFactors[3] = {10, 19}      --Grade C
    faGradeFactors[4] = {12, 22}      --Grade B
    faGradeFactors[5] = {14, 24}      --Grade A
    faGradeFactors[6] = {17, 27}      --Grade S
    faGradeFactors[7] = {21, 32}      --Grade S+
    faGradeFactors[8] = {25, 35}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter1Ini() ============================== ]|
--Generates an Initiative value given a level and grade.
function StatGenerator:fnChapter1Ini(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Initiative scales very little with level and is primarily based on the grade.
    local faGradeFactors = {}
    faGradeFactors[0] = { 10, 1}      --Grade F
    faGradeFactors[1] = { 20, 1}      --Grade E
    faGradeFactors[2] = { 40, 2}      --Grade D
    faGradeFactors[3] = { 50, 2}      --Grade C
    faGradeFactors[4] = { 70, 2}      --Grade B
    faGradeFactors[5] = { 80, 3}      --Grade A
    faGradeFactors[6] = {100, 3}      --Grade S
    faGradeFactors[7] = {110, 4}      --Grade S+
    faGradeFactors[8] = {120, 5}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter1Acc() ============================== ]|
--Generates an Accuracy value given a level and grade.
function StatGenerator:fnChapter1Acc(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Accuracy is a relative value, and low-grades even start with negative accuracy.
    local faGradeFactors = {}
    faGradeFactors[0] = {-20,  1}      --Grade F
    faGradeFactors[1] = {-15,  2}      --Grade E
    faGradeFactors[2] = {  0,  3}      --Grade D
    faGradeFactors[3] = {  5,  4}      --Grade C
    faGradeFactors[4] = {  5,  5}      --Grade B
    faGradeFactors[5] = {  5,  6}      --Grade A
    faGradeFactors[6] = { 10,  7}      --Grade S
    faGradeFactors[7] = { 15,  8}      --Grade S+
    faGradeFactors[8] = { 20, 10}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter1Evd() ============================== ]|
--Generates an HP value given a level and grade.
function StatGenerator:fnChapter1Evd(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Evade is a relative value, and low-grades even start with negative evade. Not as severe as accuracy.
    local faGradeFactors = {}
    faGradeFactors[0] = {-10,  1}      --Grade F
    faGradeFactors[1] = { -5,  2}      --Grade E
    faGradeFactors[2] = {  5,  3}      --Grade D
    faGradeFactors[3] = {  8,  4}      --Grade C
    faGradeFactors[4] = { 10,  5}      --Grade B
    faGradeFactors[5] = { 13,  6}      --Grade A
    faGradeFactors[6] = { 19,  7}      --Grade S
    faGradeFactors[7] = { 22,  9}      --Grade S+
    faGradeFactors[8] = { 25, 12}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter1Stun() ============================== ]|
--Generates a stun value given a range. Level is typically ignored.
function StatGenerator:fnChapter1Stun(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --We typically use fixed values for stun. Stun doesn't scale with level for the player, so it
    -- doesn't scale with enemies.
    if(piGradeIndex == 0) then
        return 50
    elseif(piGradeIndex == 1) then
        return 50
    elseif(piGradeIndex == 2) then
        return 60
    elseif(piGradeIndex == 3) then
        return 80
    elseif(piGradeIndex == 4) then
        return 100
    elseif(piGradeIndex == 5) then
        return 120
    elseif(piGradeIndex == 6) then
        return 130
    elseif(piGradeIndex == 7) then
        return 130
    end
    
    --S++ is the max.
    return 150
end

-- |[ ============================== StatGenerator:fnChapter1Plt() ============================== ]|
--Generates a Platina reward value given a level and grade. Always gives 0 Plt for an F rating.
function StatGenerator:fnChapter1Plt(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Platina is generally higher than EXP in chapter 1. Enemies that are less rich should use lower grades.
    local faGradeFactors = {}
    faGradeFactors[0] = {  0,  0}      --Grade F
    faGradeFactors[1] = {  3,  3}      --Grade E
    faGradeFactors[2] = {  4,  4}      --Grade D
    faGradeFactors[3] = {  5,  6}      --Grade C
    faGradeFactors[4] = {  6,  7}      --Grade B
    faGradeFactors[5] = {  8,  8}      --Grade A
    faGradeFactors[6] = {  9,  9}      --Grade S
    faGradeFactors[7] = { 10, 10}      --Grade S+
    faGradeFactors[8] = { 12, 14}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter1Exp() ============================== ]|
--Generates an EXP reward value given a level and grade. Always gives 0 EXP for an F rating.
function StatGenerator:fnChapter1Exp(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Evade is a relative value, and low-grades even start with negative evade. Not as severe as accuracy.
    local faGradeFactors = {}
    faGradeFactors[0] = {  0,  0}      --Grade F
    faGradeFactors[1] = {  3,  2}      --Grade E
    faGradeFactors[2] = {  4,  3}      --Grade D
    faGradeFactors[3] = {  5,  4}      --Grade C
    faGradeFactors[4] = {  6,  5}      --Grade B
    faGradeFactors[5] = {  8,  6}      --Grade A
    faGradeFactors[6] = {  9,  7}      --Grade S
    faGradeFactors[7] = { 10,  9}      --Grade S+
    faGradeFactors[8] = { 12, 12}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end
