//--Base
#include "AdvUIFileSelect.h"

//--Classes
//--CoreClasses
//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers

///===================================== Sorting Functions ========================================
//--A series of functions that sort the file list by different criteria. Some are simpler than others.

///===================================== Worker Functions =========================================
///--[Forward Declarations]
void Quicksort(uint64_t *&sArray, int *&sIndexes, int pLo, int pHi);
int QuicksortPartition(uint64_t *&sArray, int *&sIndexes, int pLo, int pHi);

///--[Implementations]
void Quicksort(uint64_t *&sArray, int *&sIndexes, int pLo, int pHi)
{
    ///--[Documentation]
    //--Customized quicksort designed to be used with the date structure.

    //--Do nothing if the indices are misaligned.
    if(pLo < pHi)
    {
        //--Get the partitioning index.
        int tPI = QuicksortPartition(sArray, sIndexes, pLo, pHi);

        //--Sort the 'halves'.
        Quicksort(sArray, sIndexes, pLo, tPI - 1); // Before tPI
        Quicksort(sArray, sIndexes, tPI + 1, pHi); // After tPI
    }
}
int QuicksortPartition(uint64_t *&sArray, int *&sIndexes, int pLo, int pHi)
{
    ///--[Documentation]
    //--Partitioning function for the quicksort.

    //--Pivot. Value placed at the rightmost end.
    uint64_t tPivot = sArray[pHi];

    //--Index of the smaller element, represented by i.
    uint64_t tTempVal;
    int tTempIndex;
    int i = (pLo - 1);

    //--Iterate across the array section.
    for(int j = pLo; j <= pHi - 1; j ++)
    {
        //--If the current value is smaller than the pivot, move the small index and swap it.
        if(sArray[j] < tPivot)
        {
            //--Move the smaller index.
            i ++;

            //--Swap the actual values.
            tTempVal = sArray[i];
            sArray[i] = sArray[j];
            sArray[j] = tTempVal;

            //--Also swap the index.
            tTempIndex = sIndexes[i];
            sIndexes[i] = sIndexes[j];
            sIndexes[j] = tTempIndex;
        }
    }

    //--Final swap.
    tTempVal = sArray[i+1];
    sArray[i+1] = sArray[pHi];
    sArray[pHi] = tTempVal;
    tTempIndex = sIndexes[i+1];
    sIndexes[i+1] = sIndexes[pHi];
    sIndexes[pHi] = tTempIndex;

    //--Pass back the lower element's index plus 1.
    return (i + 1);
}

///====================================== Implementations =========================================
void AdvUIFileSelect::SortFilesBySlot()
{
    //--Sorts files by slot. This is just 0-99, no tricks at all!
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++) mFileListingIndex[i] = i;
}
void AdvUIFileSelect::SortFilesBySlotRev()
{
    //--Sort by reverse slot. 99-0, nothing special.
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++) mFileListingIndex[i] = ADVUIFILE_SCAN_TOTAL-i-1;
}
void AdvUIFileSelect::SortFilesByDate()
{
    //--Sorts files by date. More recent is given higher priority. All slots that are unoccupied
    //  are placed in order after all occupied slots.
    int tUnoccupiedSlotsTotal = 0;
    int tUnoccupiedSlots[ADVUIFILE_SCAN_TOTAL];
    int tOccupiedSlotsTotal = 0;
    uint64_t tLoadPackDateValues[ADVUIFILE_SCAN_TOTAL];

    //--Parallel array, stores the actual index of the file.
    int tActualIndexes[ADVUIFILE_SCAN_TOTAL];
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++) tActualIndexes[i] = -1;

    //--Variables.
    int tMonths = 0;
    int tDays = 0;
    int tYears = 0;
    int tHours = 0;
    int tMinutes = 0;
    int tSeconds = 0;
    uint64_t tTimeScore = 0;

    //--Scan.
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        //--If the slot is unoccupied, store its index.
        if(!mLoadingPacks[i])
        {
            tUnoccupiedSlots[tUnoccupiedSlotsTotal] = i;
            tUnoccupiedSlotsTotal ++;
            continue;
        }

        //--Slot is occupied, get its date. Convert the date into a number of seconds.
        LoadingPack *rPack = mLoadingPacks[i];
        //fprintf(stderr, "Slot %i: %s\n", i, rPack->mTimestamp);

        //--The format is MM/DD/YY for the first part. Compute how many days we have.
        tMonths = (((rPack->mTimestamp[0] - '0') * 10) + (rPack->mTimestamp[1] - '0'));
        tDays   = (((rPack->mTimestamp[3] - '0') * 10) + (rPack->mTimestamp[4] - '0'));
        tYears  = (((rPack->mTimestamp[6] - '0') * 10) + (rPack->mTimestamp[7] - '0'));
        //fprintf(stderr, " D M Y: %i %i %i\n", tDays, tMonths, tYears);

        //--Convert this to a "time" score. It's not exact, it just has to be relatively correct.
        tTimeScore = (tYears * 373) + (tMonths * 31) + tDays;
        tTimeScore = tTimeScore * (24 * 60 * 60); //Multiply by number of seconds per day.

        //--The second block is HH:MM:SS.
        tHours   = (((rPack->mTimestamp[ 9] - '0') * 10) + (rPack->mTimestamp[10] - '0'));
        tMinutes = (((rPack->mTimestamp[12] - '0') * 10) + (rPack->mTimestamp[13] - '0'));
        tSeconds = (((rPack->mTimestamp[15] - '0') * 10) + (rPack->mTimestamp[16] - '0'));
        //fprintf(stderr, " H M S: %i %i %i\n", tHours, tMinutes, tSeconds);

        //--Add the number of seconds to get our final time score.
        tTimeScore = tTimeScore + (tHours * 60 * 60) + (tMinutes * 60) + tSeconds;

        //--Final time score.
        //fprintf(stderr, " Timestamp: %s\n", rPack->mTimestamp);
        //fprintf(stderr, " Time score: %i\n", tTimeScore);

        //--Move the counter up.
        tLoadPackDateValues[tOccupiedSlotsTotal] = tTimeScore;
        tActualIndexes[tOccupiedSlotsTotal] = i;
        tOccupiedSlotsTotal ++;
    }

    //--Put zeroes in all slots that didn't have an entry.
    for(int i = tOccupiedSlotsTotal; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        tLoadPackDateValues[i] = 0;
        tActualIndexes[i] = -1;
    }

    //--Sort the occupied slots only.
    uint64_t *rArray = (uint64_t *)tLoadPackDateValues;
    int *rIndexes = (int *)tActualIndexes;
    Quicksort(rArray, rIndexes, 0, tOccupiedSlotsTotal-1);

    //--Give the lowest values to all remaining slots, and populate them with the unoccupied values, in order.
    int tSlotVal = 0;
    for(int i = tOccupiedSlotsTotal; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        tLoadPackDateValues[i] = tSlotVal;
        tActualIndexes[i] = tUnoccupiedSlots[tSlotVal];
        tSlotVal ++;
    }

    //--Copy everything into the file index.
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++) mFileListingIndex[i] = tActualIndexes[i];
}
void AdvUIFileSelect::SortFilesByDateRev()
{
    //--Sorts files by date, with the most recent being first. Basically just runs SortByDate() and then flips the array.
    SortFilesByDate();

    //--Scan across the array and find the first empty slot.
    int tUsedSlots = 0;
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        //--Error check just in case.
        int tFinal = mFileListingIndex[i];
        if(tFinal < 0 || tFinal >= ADVUIFILE_SCAN_TOTAL) continue;

        //--Mark this slot as occupied.
        tUsedSlots = i;

        //--Check this file in the actual lookups. If the file doesn't exist, stop here.
        if(!mLoadingPacks[tFinal])
        {
            tUsedSlots --;
            break;
        }
    }

    //--Now flip the positions in the array.
    for(int i = 0; i < tUsedSlots/2; i ++)
    {
        Swap(mFileListingIndex[i], mFileListingIndex[tUsedSlots-i]);
    }
}
