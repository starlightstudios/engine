//--Base
#include "SaveManager.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "FlexMenu.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"

///--[Debug]
//#define SAVE_DEBUG
#ifdef SAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///--[Forward Declarations]
bool CheckHeader(VirtualFile *fInfile, const char *pComparison);

///--[Verification]
LoadingPack *SaveManager::GetSaveInfo(const char *pFilePath)
{
    ///--[Documentation]
    //--Given a file on the hard drive, checks if it's a valid save file. If it is, returns a heap-allocated
    //  LoadingPack (defined in FileDef.h) which stores basic information about the save.
    //--If the file does not exist or is not a valid save, returns NULL.
    if(!pFilePath) return NULL;

    //--Variables.
    bool tDontLoadModActivity = false;
    bool tDontLoadChapterCompletion = false;

    ///--[Header Check]
    //--Open, check it's a valid file. It should have the correct header.
    VirtualFile *fInfile = new VirtualFile(pFilePath, false);
    if(!fInfile->IsReady())
    {
        delete fInfile;
        return NULL;
    }

    //--Debug. Starts here since the main menu scans all 100 files.
    DebugPush(true, "Retrieving save info from %s\n", pFilePath);

    //--This is done manually since there are two legal headers.
    char tInBuffer[32];
    memset(tInBuffer, 0, sizeof(char) * 32);
    fInfile->Read(tInBuffer, sizeof(char), (int)strlen("STARv203"));
    fInfile->SetUseOneByteForStringLengths(false);

    //--STARv200, does not use the string pool.
    if(!strcasecmp(tInBuffer, "STARv200"))
    {
        tDontLoadModActivity = true;
        tDontLoadChapterCompletion = true;
    }
    //--STARv201, does not store active mods.
    else if(!strcasecmp(tInBuffer, "STARv201"))
    {
        tDontLoadModActivity = true;
        tDontLoadChapterCompletion = true;
    }
    //--STARv202, does not have the chapter-completion booleans.
    else if(!strcasecmp(tInBuffer, "STARv202"))
    {
        tDontLoadChapterCompletion = true;
    }
    //--STARv203, modern file type.
    else if(!strcasecmp(tInBuffer, "STARv203"))
    {
    }
    //--Error, incorrect file type.
    else
    {
        DebugPop("Failed, incorrect file type %s\n", tInBuffer);
        delete fInfile;
        return NULL;
    }
    DebugPrint("File of type %s found.\n", tInBuffer);

    ///--[Data Loading]
    //--This is a valid file. We expect the next header to be "LOADINFO_", and to be used for what to put in the LoadingPack.
    if(!CheckHeader(fInfile, "LOADINFO_"))
    {
        DebugPop("Failed, no LOADINFO_ block found.\n");
        delete fInfile;
        return NULL;
    }
    DebugPrint("LOADINFO_ block found.\n");

    //--This pack will store the loading information.
    SetMemoryData(__FILE__, __LINE__);
    LoadingPack *nPack = (LoadingPack *)starmemoryalloc(sizeof(LoadingPack));
    nPack->Initialize();

    //--Get the name of the savegame. Store it in the pack.
    nPack->mFileName = fInfile->ReadLenString();
    DebugPrint("Savegame Name: %s\n", nPack->mFileName);

    //--Store the file path.
    nPack->mFilePath = InitializeString(pFilePath);
    DebugPrint("Savegame Path: %s\n", nPack->mFilePath);

    //--Store the "Short" file path. This is the file path with only the name and extension.
    int tStart = 0;
    for(int i = (int)strlen(pFilePath)-1; i >= 0; i --)
    {
        if(pFilePath[i] == '/' || pFilePath[i] == '\\')
        {
            tStart = i+1;
            break;
        }
    }
    nPack->mFilePathShort = InitializeString(&pFilePath[tStart]);
    DebugPrint("Savegame short Path: %s\n", nPack->mFilePathShort);

    //--Character information.
    for(int i = 0; i < LOADINGPACK_MAX_PARTY_SIZE; i ++)
    {
        //--Read the "character_job" string. This is used to build sprites. It can be "Null" if
        //  nobody was in the listed slot.
        nPack->mPartyNames[i] = fInfile->ReadLenString();

        //--Read the character's level.
        fInfile->Read(&nPack->mPartyLevels[i], sizeof(int32_t), 1);

        //--Debug.
        DebugPrint(" %i: %s %i\n", i, nPack->mPartyNames[i], nPack->mPartyLevels[i]);
    }

    //--Get the name of the map.
    nPack->mMapLocation = fInfile->ReadLenString();
    DebugPrint("Savegame Map: %s\n", nPack->mMapLocation);

    //--Timestamp.
    nPack->mTimestamp = fInfile->ReadLenString();
    DebugPrint("Savegame Timestamp: %s\n", nPack->mTimestamp);

    ///--[Mods]
    //--As of Starv202, we need to store which mods are active in a given savegame. If in a lower
    //  version then the list of mods is blank.
    if(!tDontLoadModActivity)
    {
        //--Debug.
        DebugPrint("Loading mod information.\n", nPack->mTimestamp);

        //--Read a 32-bit integer indicating how many mods there are.
        uint32_t tModCount = 0;
        fInfile->Read(&tModCount, sizeof(uint32_t), 1);
        DebugPrint("Mod count: %i\n", tModCount);

        //--Each one is a string. Store it on the mod list.
        for(int i = 0; i < (int)tModCount; i ++)
        {
            //--Copy.
            char *nString = fInfile->ReadLenString();
            nPack->mActiveMods->AddElementAsTail(nString, &tModCount);
            DebugPrint(" Mod: %s\n", nString);

            //--Remove unused string.
            free(nString);
        }
    }

    ///--[Chapter Completion]
    //--As of Starv203, stores a set of LOADINGPACK_MAX_CHAPTER booleans that determine which
    //  chapters have been completed. These appear as badges on the save file.
    if(!tDontLoadChapterCompletion)
    {
        fInfile->Read(&nPack->mIsChapterComplete, sizeof(int8_t), LOADINGPACK_MAX_CHAPTER);
    }

    ///--[Finish Up]
    delete fInfile;
    DebugPop("Completed normally.\n");
    return nPack;
}

///--[Entry]
void SaveManager::SaveAdventureFile(const char *pFilePath)
{
    ///--[Documentation]
    //--Entry point for the saving sequence in AdventureMode. Requires a valid hard drive path. Overwrites
    //  if the file already exists.
    if(!pFilePath) return;

    //--Debug.
    DebugPush(true, "SaveManager:SaveAdventureFile - Begin, path is %s\n", pFilePath);

    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[File Handling]
    //--Open the file, check it.
    FILE *fOutfile = fopen(pFilePath, "wb");
    if(!fOutfile)
    {
        DebugPop("Failed, file could not be opened.\n");
        return;
    }

    //--Create an auto-buffer to store the writes. It is faster to write the data to the hard drive
    //  in one stream rather than piecemeal. This gets the m-master letter since it's so vital.
    StarAutoBuffer *mOutBuffer = new StarAutoBuffer();
    DebugPrint("Created autobuffer.\n");

    ///--[Header]
    //--Write the four-byte header.
    mOutBuffer->AppendStringWithoutNull("STAR");

    //--Write the saving version.
    mOutBuffer->AppendStringWithoutNull("v203");
    DebugPrint("Appended header.\n");

    ///--[Load Info]
    //--Information that will be displayed by the loading screen later.
    mOutBuffer->AppendStringWithoutNull("LOADINFO_");

    //--Savegame name. Customizable by the player.
    mOutBuffer->AppendStringWithLen(mSavegameName);

    //--Combat information. This is the character appearances and level.
    AdvCombat::Fetch()->WriteLoadInfo(mOutBuffer);
    DebugPrint("Wrote combat loadinfo.\n");

    //--Write the name of the level that the player will end up in when they load the file.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel)
    {
        mOutBuffer->AppendStringWithLen(rActiveLevel->GetName());
    }
    //--Level didn't exist, so write "NOLEVEL". This is used between chapters.
    else
    {
        mOutBuffer->AppendStringWithLen("NOLEVEL");
    }

    //--Time of day, to make it easier to pick out save files.
    char tBuffer[128];
    time_t tRawTime;
    tm *tTimeInfo = NULL;
    time(&tRawTime);
    tTimeInfo = localtime(&tRawTime);
    strftime(tBuffer, sizeof(char) * 128, "%c", tTimeInfo);
    mOutBuffer->AppendStringWithLen(tBuffer);
    DebugPrint("Wrote name and timestamp.\n");

    ///--[Mods]
    //--As of Starv202, we need to store which mods are active in a given savegame. If in a lower
    //  version then the list of mods is blank.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    StarLinkedList *rModListing = rOptionsManager->GetActiveModList();
    uint32_t tModCount = rModListing->GetListSize();
    mOutBuffer->AppendUInt32(tModCount);

    //--Append each one as a len-string.
    char *rModName = (char *)rModListing->PushIterator();
    while(rModName)
    {
        mOutBuffer->AppendStringWithLen(rModListing->GetIteratorName());
        rModName = (char *)rModListing->AutoIterate();
    }

    ///--[Chapter Completion]
    //--As of Starv203, retrieve specific chapter-complete variables from the DataLibrary. These appear as badges
    //  on the load-game screen.
    for(int i = 0; i < LOADINGPACK_MAX_CHAPTER; i ++)
    {
        //--Resolve path.
        sprintf(tBuffer, "Root/Variables/Global/ChapterComplete/iChapter%i", i+1);

        //--Get variable.
        SysVar *rChapter1Var = (SysVar *)rDataLibrary->GetEntry(tBuffer);

        //--Variable does not exist, or the value is zero: Write 0.
        if(!rChapter1Var || rChapter1Var->mNumeric == 0.0f)
        {
            mOutBuffer->AppendInt8(0);
        }
        //--Variable exists and is nonzero, chapter is complete. Write 1.
        else
        {
            mOutBuffer->AppendInt8(1);
        }
    }

    ///--[External]
    //--Call an external file to do additional work before writing the scriptvars.
    if(xSaveAssemblerPath) LuaManager::Fetch()->ExecuteLuaFile(xSaveAssemblerPath);
    DebugPrint("Performed external call.\n");

    ///--[Script Variables]
    //--Header for the script variables.
    mOutBuffer->AppendStringWithoutNull("SCRIPTVARS_");
    rDataLibrary->WriteToBuffer(mOutBuffer, true);
    DebugPrint("Wrote script variables.\n");

    ///--[Map Information]
    //--Header for the map information.
    mOutBuffer->AppendStringWithoutNull("MAP_");

    //--Name of the level this saving action was called with.
    if(rActiveLevel)
    {
        rActiveLevel->WriteToBuffer(mOutBuffer);
    }
    //--Level didn't exist, so write "NOLEVEL". This is used between chapters.
    else
    {
        mOutBuffer->AppendStringWithLen("NOLEVEL");
    }
    DebugPrint("Wrote map variables.\n");

    ///--[Finish Up]
    //--Liberate the data.
    int mDataLen = mOutBuffer->GetCursor();
    uint8_t *mDataPtr = mOutBuffer->LiberateData();

    //--Write the buffer to the file.
    fwrite(mDataPtr, mDataLen, sizeof(uint8_t), fOutfile);

    //--Close the file, debug write.
    fclose(fOutfile);
    delete mOutBuffer;
    DebugPop("SaveManager:SaveAdventureFile - Completed normally.\n");

    ///--[Control Handling]
    //--Print the current control setup to a save file.
    SaveControlsFile();

    ///--[Clean Temp Vars]
    DataLibrary::Fetch()->Purge("Root/Saving/", false);
    free(mDataPtr);
}
void SaveManager::SaveControlsFile()
{
    //--Writes a control overbind file. Does not take a pathname, uses the library-specific definition.
    int cControlsTotal = 23;
    const char *mControlList[] = {"Up", "Left", "Down", "Right", "Activate", "Cancel", "Run", "DnLevel", "UpLevel", "OpenFieldAbilityMenu", "OpenFieldAbilityMenu",
    "FieldAbility0", "FieldAbility1", "FieldAbility2", "FieldAbility3", "FieldAbility4", "Jump", "Ctrl", "OpenDebug", "F5", "RecompileShaders", "RebuildKerning",
    "WHI Emergency Escape"};

    //--Allocate scancode space
    int **tCurrentScancodes = (int **)starmemoryalloc(sizeof(int *) * cControlsTotal);

    //--Get scancode information.
    ControlManager *rControlManager = ControlManager::Fetch();
    for(int i = 0; i < cControlsTotal; i ++)
    {
        //--Set all six entries to -1, to indicate they aren't bound.
        tCurrentScancodes[i] = (int *)starmemoryalloc(sizeof(int) * 6);
        for(int p = 0; p < 6; p ++) tCurrentScancodes[i][p] = -1;

        //--Get the control.
        ControlState *rControlState = rControlManager->GetControlState(mControlList[i]);
        if(!rControlState) continue;

        //--Fill.
        tCurrentScancodes[i][0] = rControlState->mWatchKeyPri;
        tCurrentScancodes[i][1] = rControlState->mWatchMouseBtnPri;
        tCurrentScancodes[i][2] = rControlState->mWatchJoyPri;
        tCurrentScancodes[i][3] = rControlState->mWatchKeySec;
        tCurrentScancodes[i][4] = rControlState->mWatchMouseBtnSec;
        tCurrentScancodes[i][5] = rControlState->mWatchJoySec;
    }

    //--Assemble the path to the controls file.
    char *tControlPath = NULL;
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    #ifdef _ALLEGRO_PROJECT_
        tControlPath = InitializeString("%s/../../Saves/AdventureControlsAL.lua", rAdventurePath);
    #elif defined _SDL_PROJECT_
        tControlPath = InitializeString("%s/../../Saves/AdventureControlsSDL.lua", rAdventurePath);
    #else
        fprintf(stderr, "Error: No primary control library, cannot boot controls!");
        tControlPath = InitializeString("Null");
    #endif

    //--Open a file and write.
    FILE *fControlOutfile = fopen(tControlPath, "w");
    if(fControlOutfile)
    {
        //--Header.
        fprintf(fControlOutfile, "-- |[Adventure Mode Control Rebinds]|\n");
        fprintf(fControlOutfile, "--These rebinds are stored whenever the game is saved and are loaded when a game is loaded or started.\n");
        fprintf(fControlOutfile, "-- Do not hand-edit this file as it will be overwritten during the next save.\n");

        //--Now write the overbind commands.
        for(int i = 0; i < cControlsTotal; i ++)
        {
            fprintf(fControlOutfile, "CM_Overbind(\"%s\", false, %i, %i, %i)\n", mControlList[i], tCurrentScancodes[i][0], tCurrentScancodes[i][1], tCurrentScancodes[i][2]);
            fprintf(fControlOutfile, "CM_Overbind(\"%s\", true, %i, %i, %i)\n",  mControlList[i], tCurrentScancodes[i][3], tCurrentScancodes[i][4], tCurrentScancodes[i][5]);
        }
        fclose(fControlOutfile);
    }

    //--Clean.
    for(int i = 0; i < cControlsTotal; i ++) free(tCurrentScancodes[i]);
    free(tCurrentScancodes);
    free(tControlPath);
}

///--[Worker Function]
bool CheckHeader(VirtualFile *fInfile, const char *pComparison)
{
    //--Given a VirtualFile with its cursor in position, seeks forward by the size of pComparison's string
    //  and returns true if the file is identical. Used for checking headers.
    if(!fInfile || !pComparison) return false;

    //--Read.
    char tInBuffer[32];
    memset(tInBuffer, 0, sizeof(char) * 32);
    fInfile->Read(tInBuffer, sizeof(char), (int)strlen(pComparison));

    //--Compare.
    return (!strcmp(tInBuffer, pComparison));
}

///--[Loading Entry]
void SaveManager::LoadFrom(const char *pPath)
{
    ///--[Documentation]
    //--Given a file path, loads from the file all of the data needed to reconstruct the game state. Loading should be the
    //  first thing done in the game after the initial state is built (that is, instead of going to the scenario launcher, load).
    //--Starting in Starv202, mods are specified in the loading header. These should already be set before this function is called.
    if(!pPath) return;

    //--Versioning Variables.
    bool tUseStringPool = true;
    bool tHasModsInInfo = true;
    bool tHasChapterInfo = true;

    //--Debug.
    DebugPush(true, "SaveManager:LoadFrom - Begin, path is %s\n", pPath);

    ///--[Basic File Checking]
    //--Open the file, check it. Do this is a VirtualFile to speed up reading.
    VirtualFile *fInfile = new VirtualFile(pPath, false);
    if(!fInfile->IsReady())
    {
        DebugPop("Failed, file could not be opened.\n");
        return;
    }

    //--Starv200 and higher files use 2 bytes for string lengths.
    fInfile->SetUseOneByteForStringLengths(false);

    //--Make sure this is a "STARv200" file.
    char tInBuffer[32];
    memset(tInBuffer, 0, sizeof(char) * 32);
    fInfile->Read(tInBuffer, sizeof(char), (int)strlen("STARv200"));

    //--If this is a STARv200 header, continue. Flag to not use the string pool.
    if(!strcasecmp(tInBuffer, "STARv200"))
    {
        tUseStringPool = false;
        tHasModsInInfo = false;
        tHasChapterInfo = false;
    }
    //--If this is a STARv201 header, continue.
    else if(!strcasecmp(tInBuffer, "STARv201"))
    {
        tHasModsInInfo = false;
        tHasChapterInfo = false;
    }
    //--If this is a STARv202 header, continue.
    else if(!strcasecmp(tInBuffer, "STARv202"))
    {
        tHasChapterInfo = false;
    }
    //--If this is a STARv203 header, continue.
    else if(!strcasecmp(tInBuffer, "STARv203"))
    {
    }
    //--Error, incorrect file type.
    else
    {
        DebugPop("Failed, file is not a STARv200/201 file.\n");
        return;
    }

    ///--[Clean Temp Vars]
    DataLibrary::Fetch()->Purge("Root/Saving/", false);

    ///--[Loading Info]
    //--Skip this, we're not interested in most of it.
    DebugPrint("Checking for LOADINFO_ at %i.\n", fInfile->GetCurPosition());
    if(!CheckHeader(fInfile, "LOADINFO_"))
    {
        DebugPop("Failed, expected LOADINFO_ lump.\n");
        return;
    }

    //--Savegame name. Set this as the active name.
    DebugPrint("Checking for savegame name at %i.\n", fInfile->GetCurPosition());
    char *tSavegameName = fInfile->ReadLenString();
    SetSavegameName(tSavegameName);
    free(tSavegameName);

    //--Skip the character info.
    DebugPrint("Checking for character info at %i.\n", fInfile->GetCurPosition());
    for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
    {
        fInfile->SkipLenString();
        fInfile->SeekRelative(sizeof(int32_t));
    }

    //--Skip the name of the level. This is only for the loading pack and may not be the same as the final name.
    DebugPrint("Checking for level name at %i.\n", fInfile->GetCurPosition());
    fInfile->SkipLenString();

    //--Skip the timestamp. We don't need it.
    DebugPrint("Checking for timestamp at %i.\n", fInfile->GetCurPosition());
    fInfile->SkipLenString();

    //--Skip mods if flagged.
    if(tHasModsInInfo)
    {
        //--Read how many mods there are.
        uint32_t tModCount = 0;
        fInfile->Read(&tModCount, sizeof(uint32_t), 1);

        //--Skip that many names.
        for(int i = 0; i < (int)tModCount; i ++) fInfile->SkipLenString();
    }

    //--Skip chapter completion if flagged.
    if(tHasChapterInfo)
    {
        int8_t tFlag = 0;
        fInfile->Read(&tFlag, sizeof(int8_t), LOADINGPACK_MAX_CHAPTER);
    }

    ///--[Script Variables]
    //--Check header.
    #if defined SAVE_DEBUG
        int tFileCursor = fInfile->GetCurPosition();
    #else
        fInfile->GetCurPosition();
    #endif
    if(!CheckHeader(fInfile, "SCRIPTVARS_"))
    {
        DebugPop("Failed, SCRIPTVARS_ info not found at slot %i.\n", tFileCursor);
        return;
    }

    //--Run.
    DebugPrint("Reading SCRIPTVARS_ block.\n");
    DataLibrary::Fetch()->ReadFromFile(fInfile, tUseStringPool);

    ///--[Map Data]
    //--Check header.
    bool tNoMapInfo = false;
    DebugPrint("Reading MAP_ block.\n");
    if(!CheckHeader(fInfile, "MAP_"))
    {
        tNoMapInfo = true;
        DebugPrint("Failed, map info not found. Using default level.\n");
    }

    //--If no map info was found, we default to EvermoonW.
    if(tNoMapInfo)
    {
        //--Debug.
        DebugPrint("Error, no MAP_ block found. Defaulting to EvermoonW.\n");

        //--Set to EvermoonW as default.
        char tUseString[128];
        strcpy(tUseString, "EvermoonW");

        //--Search for a remapping. If not found, use the default.
        const char *rUsePathing = tUseString;
        for(int i = 0; i < AdventureLevel::xRemappingsTotal; i ++)
        {
            if(!strcasecmp(AdventureLevel::xRemappingsCheck[i], tUseString))
            {
                rUsePathing = AdventureLevel::xRemappingsResult[i];
                break;
            }
        }

        //--Build the level directory.
        char *tBuffer = InitializeString("%s/Maps/%s/Constructor.lua", AdventureLevel::xRootPath, rUsePathing);

        //--Execute this level's construction sequence.
        LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

        //--Post-construction.
        LuaManager::Fetch()->ExecuteLuaFile(tBuffer, 1, "N", 1.0f);

        //--Clean.
        free(tBuffer);
    }
    //--Map info was found:
    else
    {
        //--Debug.
        DebugPrint("MAP_ block found.\n");

        //--This string is the name of the level. If the level didn't exist, it will be "NOLEVEL".
        char *tLevelName = fInfile->ReadLenString();
        DebugPrint("Level name %s.\n", tLevelName);
        if(strcasecmp(tLevelName, "NOLEVEL"))
        {
            //--Search for a remapping. If not found, use the default.
            const char *rUsePathing = tLevelName;
            for(int i = 0; i < AdventureLevel::xRemappingsTotal; i ++)
            {
                if(!strcasecmp(AdventureLevel::xRemappingsCheck[i], tLevelName))
                {
                    rUsePathing = AdventureLevel::xRemappingsResult[i];
                    break;
                }
            }

            //--Build the level directory.
            char *tBuffer = InitializeString("%s/Maps/%s/Constructor.lua", AdventureLevel::xRootPath, rUsePathing);
            DebugPrint("Level constructor remapped to %s.\n", tBuffer);

            //--Execute this level's construction sequence.
            DebugPrint("Executing constructor phase 1.\n");
            LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

            //--If the level exists, order it to finish its loading sequence.
            AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
            if(rActiveLevel) rActiveLevel->ReadFromFile(fInfile);

            //--Post-construction.
            DebugPrint("Executing constructor phase 2.\n");
            LuaManager::Fetch()->ExecuteLuaFile(tBuffer, 1, "N", 1.0f);
            DebugPrint("Done.\n");

            //--Clean.
            free(tBuffer);
            free(tLevelName);
        }
        //--No level was found.
        else
        {
            free(tLevelName);
            DebugPop("Failed, NOLEVEL was not expected!\n");
            return;
        }
        DebugPrint("Finished level setup.\n");
    }

    ///--[Clean]
    //--Debug.
    delete fInfile;
    DebugPop("SaveManager:LoadFrom - Completed normally.\n");
}
