//--Base
#include "MonoUIBase.h"

//--Classes
#include "AdvCombat.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

///======================================= Core Methods ===========================================
void MonoUIBase::SwitchActiveParty(int pSlotA, int pSlotB)
{
    ///--[Documentation]
    //--Switches active party members in the slots provided, re-ordering the list. Does nothing if
    //  the two slots are the same, you nut.
    if(pSlotA == pSlotB) return;

    //--Get party listing. Get the party members in the respective slots. If either doesn't exist,
    //  do nothing.
    StarLinkedList *rActivePartyList = AdvCombat::Fetch()->GetActivePartyList();
    AdvCombatEntity *rEntityA = (AdvCombatEntity *)rActivePartyList->GetElementBySlot(pSlotA);
    AdvCombatEntity *rEntityB = (AdvCombatEntity *)rActivePartyList->GetElementBySlot(pSlotB);
    if(!rEntityA || !rEntityB) return;

    ///--[List Clone]
    //--Create a duplicate list of the active party, except during construction, switch out the
    //  A and B entities in the order. This guarantees that the list in in the correct order without
    //  and issues of pSlotB is lower than pSlotA.
    StarLinkedList *tTempList = new StarLinkedList(false);
    void *rPtr = rActivePartyList->PushIterator();
    while(rPtr)
    {
        //--If this is neither the A or B entities, add it to the temp list.
        if(rPtr != rEntityA && rPtr != rEntityB)
        {
            tTempList->AddElementAsTail(rActivePartyList->GetIteratorName(), rPtr);
        }
        //--If this is the A entity, add the B entity.
        else if(rPtr == rEntityA)
        {
            tTempList->AddElementAsTail(rActivePartyList->GetNameOfElementBySlot(pSlotB), rEntityB);
        }
        //--If this is the B entity, add the A entity.
        else if(rPtr == rEntityB)
        {
            tTempList->AddElementAsTail(rActivePartyList->GetNameOfElementBySlot(pSlotA), rEntityA);
        }

        //--Next.
        rPtr = rActivePartyList->AutoIterate();
    }

    ///--[List Repopulate]
    //--We could just switch the temp list in with the active party list, but just in case something
    //  has a pointer to the rActivePartyList, we repopulate it instead. Safety first!
    rActivePartyList->ClearList();

    //--Run across the temp list and register everything.
    rPtr = tTempList->PushIterator();
    while(rPtr)
    {
        rActivePartyList->AddElementAsTail(tTempList->GetIteratorName(), rPtr);
        rPtr = tTempList->AutoIterate();
    }

    ///--[Clean]
    delete tTempList;

    ///--[Call Closing Script]
    //--Order the party to "reform", the Lua scripts will reset following sprites and such computations.
    SetCloseCode("Reform Party");
}

///========================================== Update ==============================================
void MonoUIBase::UpdateMainMenuSwitch()
{
    ///--[Documentation]
    //--Called in place of the controls update for the base menu, this handles character switching
    //  when there are 4 or more active party members.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get combat class for party info.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    int tActivePartySize = rAdvCombat->GetActivePartyCount();

    ///--[Initial Select Mode]
    //--Select which character you want to move.
    if(!mIsReplaceMode)
    {
        //--Left decrements the cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            mSwitchCharCursor --;
            if(mSwitchCharCursor < 0) mSwitchCharCursor = tActivePartySize-1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Right increments the cursor.
        if(rControlManager->IsFirstPress("Right"))
        {
            mSwitchCharCursor ++;
            if(mSwitchCharCursor >= tActivePartySize) mSwitchCharCursor = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Activate selects this slot and begins replacement.
        if(rControlManager->IsFirstPress("Activate"))
        {
            mIsReplaceMode = true;
            mSwitchReplaceCursor = mSwitchCharCursor;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel. Return to base menu.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsSwitchMode = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    ///--[Swap Mode]
    //--Swap with another character.
    else
    {
        //--Left decrements the cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            mSwitchReplaceCursor --;
            if(mSwitchReplaceCursor < 0) mSwitchReplaceCursor = tActivePartySize-1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Right increments the cursor.
        if(rControlManager->IsFirstPress("Right"))
        {
            mSwitchReplaceCursor ++;
            if(mSwitchReplaceCursor >= tActivePartySize) mSwitchReplaceCursor = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Activate switches the slots and goes back to selection mode.
        if(rControlManager->IsFirstPress("Activate"))
        {
            SwitchActiveParty(mSwitchCharCursor, mSwitchReplaceCursor);
            mIsReplaceMode = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel. Exits replace mode.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsSwitchMode = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
}

///========================================== Drawing =============================================
void MonoUIBase::RenderMainMenuSwitch(float pAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Renders the party switching menu.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    if(pAlpha <= 0.0f) return;

    //--Resolve local alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mSwitchTimer, (float)ADV_MENU_VIS_TICKS);
    float cPercent = 1.0f - cAlpha;
    if(cAlpha <= 0.0f) return;

    //--Set.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pAlpha);

    ///============================= Top Block ================================
    //--Skip rendering if everything is offscreen.
    if(cPercent < 1.0f)
    {
        //--Translate.
        AutoPieceOpen(DIR_UP, cAlpha);

        //--Header.
        MonoImages.rTitleBack->Draw();

        //--Header text is yellow.
        mMonoTitleColor.SetAsMixerAlpha(cAlpha);
        MonoImages.rDoubleHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 42.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSwitchMenu);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Help text.
        //mBaseVars.mShowHelpString->DrawText(0.0f, cMainlineH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImg.Standard.rFontControl);

        //--Clean.
        AutoPieceClose();
    }

    ///--[Active Party]
    //--Render all members of the active party, left to right. This does dynamic spacing since it may be
    //  that there are a lot of party members if the modders get out of control.
    if(cPercent < 1.0f)
    {
        //--Translate.
        AutoPieceOpen(DIR_DOWN, cAlpha);

        //--Constants.
        float cLeftPos = -474.0f;
        float cSpacingX = 253.0f;

        //--First, translate to the left to left-align the proceedings.
        glTranslatef(cLeftPos, 0.0f, 0.0f);

        //--Setup.
        int tPartyCount = rAdvCombat->GetActivePartyCount();

        //--Render all portraits.
        for(int i = 0; i < tPartyCount; i ++)
        {
            RenderMainCharacter(i, tPartyCount);
        }

        //--Render UIs.
        for(int i = 0; i < tPartyCount; i ++)
        {
            RenderMainCharacterUI(i, tPartyCount, 1.0f);
        }

        //--Clean character offset.
        glTranslatef(cLeftPos * -1.0f, 0.0f, 0.0f);

        //--Render the selection cursor.
        MonoImages.rOverlaySwitchSelect->Draw(79.0f + (cSpacingX * mSwitchCharCursor), 478.0f);
        if(mIsReplaceMode && mSwitchCharCursor != mSwitchReplaceCursor)
        {
            MonoImages.rOverlaySwitchSelect->Draw(79.0f + (cSpacingX * mSwitchReplaceCursor), 478.0f);
        }

        //--Clean up.
        AutoPieceClose();
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}
