//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvUIJournalTypes.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
void AdvUIJournal::SetJournalToQuestsMode()
{
    //--Clear the list.
    ClearQuests();

    //--Variables.
    mCurrentMode = ADVMENU_JOURNAL_MODE_QUESTS;

    //--Run this script to set the quest's properties.
    LuaManager::Fetch()->ExecuteLuaFile(xQuestResolveScript);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIJournal::CreateQuestEntry(const char *pInternalName)
{
    //--Arg check.
    if(!pInternalName) return;

    //--Duplicate check.
    if(mQuestEntries->GetElementByName(pInternalName)) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuJournalQuestEntry *nEntry = (AdvMenuJournalQuestEntry *)starmemoryalloc(sizeof(AdvMenuJournalQuestEntry));
    nEntry->Initialize();
    ResetString(nEntry->mInternalName, pInternalName);
    mQuestEntries->AddElementAsTail(pInternalName, nEntry, &AdvMenuJournalQuestEntry::DeleteThis);
}
void AdvUIJournal::SetQuestDisplayName(const char *pInternalName, const char *pDisplayName)
{
    //--Error check.
    AdvMenuJournalQuestEntry *rEntry = (AdvMenuJournalQuestEntry *)mQuestEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDisplayName) return;

    //--Set.
    ResetString(rEntry->mDisplayName, pDisplayName);
}
void AdvUIJournal::AllocateQuestDescriptionLines(const char *pInternalName, int pCount)
{
    //--Error check.
    AdvMenuJournalQuestEntry *rEntry = (AdvMenuJournalQuestEntry *)mQuestEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->AllocateDescriptionLines(pCount);
}
void AdvUIJournal::SetQuestDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine)
{
    //--Error check.
    AdvMenuJournalQuestEntry *rEntry = (AdvMenuJournalQuestEntry *)mQuestEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDescriptionLine) return;

    //--Range check.
    if(pLine < 0 || pLine >= rEntry->mDescriptionLinesTotal) return;

    //--Set.
    ResetString(rEntry->mDescriptionLines[pLine], pDescriptionLine);
}
void AdvUIJournal::SetQuestObjective(const char *pInternalName, const char *pQuestObjective)
{
    //--Error check.
    AdvMenuJournalQuestEntry *rEntry = (AdvMenuJournalQuestEntry *)mQuestEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pQuestObjective) return;

    //--Set.
    ResetString(rEntry->mObjective, pQuestObjective);
}
void AdvUIJournal::SetQuestDescriptionAuto(const char *pInternalName, const char *pDescription)
{
    ///--[Documentation]
    //--Uses subdivision to automatically set the quest description lines via breakpoints.
    if(!pInternalName || !pDescription) return;

    //--Get entry.
    AdvMenuJournalQuestEntry *rEntry = (AdvMenuJournalQuestEntry *)mQuestEntries->GetElementByName(pInternalName);
    if(!rEntry) return;

    ///--[Run Subdivide]
    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pDescription, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS, NULL, -1.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "AdvUIJournal::SetQuestDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        //--Liberate the string.
        tStringList->SetRandomPointerToHead();
        char *rString = (char *)tStringList->LiberateRandomPointerEntry();
        rEntry->mDescriptionLines[i] = rString;
    }

    //--Clean the list up.
    delete tStringList;
}
void AdvUIJournal::ClearQuests()
{
    mQuestEntries->ClearList();
}

///======================================= Core Methods ===========================================
void AdvUIJournal::ComputeQuestCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mQuestCursor - mQuestOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mQuestEntries->GetElementBySlot(mQuestCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = AdvImages.rFont_Mainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIJournal::RenderQuestsMode(float pVisAlpha)
{
    ///--[Profile Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mQuestEntries, mQuestOffset, mQuestCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the information about the highlighted quest.
    AdvMenuJournalQuestEntry *rActiveEntry = (AdvMenuJournalQuestEntry *)mQuestEntries->GetElementBySlot(mQuestCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Title]
        //--Title goes in the top middle of the description pane.
        float cTitleX = 918.0f;
        float cTitleY = 116.0f;
        mColorHeading.SetAsMixerAlpha(pVisAlpha);
        AdvImages.rFont_Header->DrawTextArgs(cTitleX, cTitleY, SUGARFONT_AUTOCENTER_X, 1.0f, rActiveEntry->mDisplayName);

        //--Objective. Displayed in green.
        float cObjectiveX = 518.0f;
        float cObjectiveY1 = 156.0f;
        float cObjectiveY2 = cObjectiveY1 + AdvImages.rFont_Mainline->GetTextHeight();
        StarlightColor::SetMixer(0.0f, 0.7f, 0.0f, pVisAlpha);
        AdvImages.rFont_Mainline->DrawTextArgs(cObjectiveX,       cObjectiveY1, 0, 1.0f, "Objective:");
        AdvImages.rFont_Mainline->DrawTextArgs(cObjectiveX+10.0f, cObjectiveY2, 0, 1.0f, rActiveEntry->mObjective);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Description.
        float cDescriptionX = 518.0f;
        float cDescriptionY = 216.0f;
        float cDescriptionH = AdvImages.rFont_Mainline->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            AdvImages.rFont_Mainline->DrawTextArgs(cDescriptionX, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
