///====================================== PeakFreaksBomb ==========================================
//--Bomb projectile in peak freaks. This can be both drone bombs and guano bombs.

#pragma once

///========================================== Includes ============================================
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Subtypes.
#define PFBOMB_SUBTYPE_GUANO 0
#define PFBOMB_SUBTYPE_BOMB 1
#define PFBOMB_SUBTYPE_SUPERBOMB 2
#define PFBOMB_SUBTYPE_MAX 3

//--Timers.
#define PFBOMB_TPF 30
#define PFBOMB_PERSIST_TICKS 120
#define PFGUANO_PERSIST_TICKS 600

///========================================== Classes =============================================
class PeakFreaksBomb
{
    private:
    ///--[System]
    int mSubtype;
    int mTimer;
    bool mHasPlayedFallSFX;

    ///--[Position]
    int mTileX;
    int mTileY;
    float mHeight;
    float mYSpeed;
    float mGravity;

    ///--[Images]
    SugarBitmap *rFlightImg;
    SugarBitmap *rTargetImg;
    SugarBitmap *rExplosionImg[4];

    protected:

    public:
    //--System
    PeakFreaksBomb();
    ~PeakFreaksBomb();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool IsComplete();

    //--Manipulators
    void SetSubtype(int pSubtype);
    void SetTilePosition(int pTileX, int pTileY);
    void SetHeight(float pHeight);
    void SetFlightImg(SugarBitmap *pImg);
    void SetTargetImg(SugarBitmap *pImg);
    void SetExplosionImg(int pSlot, SugarBitmap *pImg);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


