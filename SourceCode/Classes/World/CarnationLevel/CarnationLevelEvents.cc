//--Base
#include "CarnationLevel.h"

//--Classes
#include "CarnLevelEvent.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries

//--Managers

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CarnationLevel::CreateEventNotification(const char *pText)
{
    ///--[Documentation]
    //--Creates a new event notification and slides it into the screen from the left side. The new
    //  event will be created below the lowest current event.
    if(!pText) return;

    ///--[Scan]
    //--Run across all events and get the lowest Y position on the screen.
    float tLowestY = CARN_NOTIFICATION_HEI * -1.0f;
    CarnLevelEvent *rEvent = (CarnLevelEvent *)mEventNotificationList->PushIterator();
    while(rEvent)
    {
        //--Skip if this object is despawning.
        if(!rEvent->WillDespawnWhenDoneMoving())
        {
            //--If this object is lower, move it down.
            float tYPos = rEvent->GetYPosition();
            if(tYPos > tLowestY) tLowestY = tYPos;
        }

        //--Next.
        rEvent = (CarnLevelEvent *)mEventNotificationList->AutoIterate();
    }

    //--Compute lowest Y position.
    float cSetY = tLowestY + CARN_NOTIFICATION_HEI;

    //--Create.
    CarnLevelEvent *nEvent = new CarnLevelEvent();
    nEvent->SetText(pText);
    nEvent->RecomputeSizes();
    nEvent->MoveTo(nEvent->GetWidth() * -1, cSetY, 0);

    //--Order event to slide onscreen.
    nEvent->MoveTo(0.0f, cSetY, 15);

    //--Register.
    mEventNotificationList->AddElementAsTail("X", nEvent, &CarnLevelEvent::DeleteThis);
}

///========================================== Update ==============================================
void CarnationLevel::UpdateEvents()
{
    ///--[Documentation]
    //--Run across all events and updates them.
    CarnLevelEvent *rEvent = (CarnLevelEvent *)mEventNotificationList->SetToHeadAndReturn();
    while(rEvent)
    {
        //--Run the update.
        rEvent->Update();

        //--If flagged, remove the object.
        if(rEvent->ShouldDespawn())
        {
            mEventNotificationList->RemoveRandomPointerEntry();
        }

        //--Next.
        rEvent = (CarnLevelEvent *)mEventNotificationList->IncrementAndGetRandomPointerEntry();
    }
}
bool CarnationLevel::UpdateEventLeftClick(float pX, float pY)
{
    ///--[Documentation]
    //--Scans events. If any of them are left-clicked, causes them to move offscreen. Returns true
    //  if any events were clicked, false, if not.
    CarnLevelEvent *rEvent = (CarnLevelEvent *)mEventNotificationList->PushIterator();
    while(rEvent)
    {
        //--Check for a hit. If hit, move offscreen.
        if(rEvent->IsPointWithin(pX, pY))
        {
            //--Get the positions for the object.
            float cWid = rEvent->GetWidth();
            float cYPos = rEvent->GetYPosition();

            //--Order movement.
            rEvent->MoveTo(cWid * -1.0f, cYPos, 15);
            rEvent->MarkDespawnAfterMoving();

            //--Order all events other than this one to move up by CARN_NOTIFICATION_HEI. This only
            //  occurs if they were below this event.
            CarnLevelEvent *rSubEvent = (CarnLevelEvent *)mEventNotificationList->PushIterator();
            while(rSubEvent)
            {
                if(!rSubEvent->WillDespawnWhenDoneMoving())
                {
                    float cOtherXPos = rSubEvent->GetXPosition();
                    float cOtherYPos = rSubEvent->GetYPosition();
                    if(cOtherYPos > cYPos)
                        rSubEvent->MoveTo(cOtherXPos, cOtherYPos - CARN_NOTIFICATION_HEI, 15);
                }

                rSubEvent = (CarnLevelEvent *)mEventNotificationList->AutoIterate();
            }


            //--Stop.
            mEventNotificationList->PopIterator();
            return true;
        }

        //--Next.
        rEvent = (CarnLevelEvent *)mEventNotificationList->AutoIterate();
    }

    //--No events clicked.
    return false;
}

///========================================== Drawing =============================================
void CarnationLevel::RenderEvents()
{
    ///--[Documentation]
    //--Run across all events and render them. Only 7 events will render, after that additional
    //  events are invisible.
    int tRenders = 0;
    CarnLevelEvent *rEvent = (CarnLevelEvent *)mEventNotificationList->PushIterator();
    while(rEvent)
    {
        //--Ending check.
        if(tRenders >= 7)
        {
            mEventNotificationList->PopIterator();
            break;
        }

        //--Render.
        rEvent->Render();

        //--Next.
        tRenders ++;
        rEvent = (CarnLevelEvent *)mEventNotificationList->AutoIterate();
    }
}
