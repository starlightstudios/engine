//--Base
#include "Actor.h"

//--Classes
#include "InventoryItem.h"
#include "PandemoniumRoom.h"
#include "PandemoniumLevel.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "EntityManager.h"

///======================================== Lua Hooking ===========================================
void Actor::HookToLuaState(lua_State *pLuaState)
{
    /* Actor_Spawn(sName)
       Spawns an Actor directly. The Actor is pushed to the top of the activity stack, and the
       stack must be popped when you are done. */
    lua_register(pLuaState, "Actor_Spawn", &Hook_Actor_Spawn);

    /* Actor_ExistsWithName(sName) (1 boolean)
       Returns true if an Actor exists with that name, false if not. Actors that spawn, are killed,
       and then attempt to respawn can do so legally, but two Actors with the same name is normally
       not legal (depending on scenario). */
    lua_register(pLuaState, "Actor_ExistsWithName", &Hook_Actor_ExistsWithName);

    /* Actor_PathTo(sRoomName)
       The Actor will automatically path to the given room. This is done by appending instructions. */
    lua_register(pLuaState, "Actor_PathTo", &Hook_Actor_PathTo);

    /* Actor_SetProperty("Name", sName)
       Actor_SetProperty("Health", iHealth)
       Actor_SetProperty("Health Bypass", iHealth)
       Actor_SetProperty("Willpower", iWillpower)
       Actor_SetProperty("Stamina", iStamina)
       Actor_SetProperty("CurrentRoom", sRoomName)
       Actor_SetProperty("PlayerControlled", bFlag)
       Actor_SetProperty("IgnoresItems", bFlag)
       Actor_SetProperty("RenderType", iFlag)
       Actor_SetProperty("Color", fRed, fGreen, fBlue)
       Actor_SetProperty("Color", fRed, fGreen, fBlue, fAlpha)
       Actor_SetProperty("AIScript", sPath)
       Actor_SetProperty("OverrideScript", sPath)
       Actor_SetProperty("Instruction", iInstruction)
       Actor_SetProperty("Instruction String", sStringInstruction)
       Actor_SetProperty("Image", sRefName, sDLPath)
       Actor_SetProperty("ActiveImage Ref", sRefName)
       Actor_SetProperty("ActiveImage DL", sDLPath)
       Actor_SetProperty("StatMaxes", iHPMax, iWPMax, iSTAMax, iATP, iDEF, iACC)
       Actor_SetProperty("PersistsWhenDowned", bFlag)
       Actor_SetProperty("RecoversToFull", bFlag)
       Actor_SetProperty("ScoreForKill", iScore)
       Actor_SetProperty("RemoveStunned")
       Actor_SetProperty("RequiresStamina", bFlag)
       Actor_SetProperty("Wipe Instructions")
       Actor_SetProperty("AsPlayerAttackPhrase", sPhrase)
       Actor_SetProperty("AsEnemyAttackPhrase", sPhrase)
       Actor_SetProperty("AsNeutralAttackPhrase", sPhrase)
       Actor_SetProperty("ObscuredBy", iObscurerID)
       Actor_SetProperty("NotObscuredBy", iObscurerID) --Pass 0 to clear all obscuring info.
       Actor_SetProperty("Bane", sBaneName)
       Actor_SetProperty("MovementRestriction", bFlag)
       Actor_SetProperty("ResetWillpowerRegeneration")
       Actor_SetProperty("Give Item", sItemName)
       Actor_SetProperty("Lose Item", sItemName)
       Sets the requested property in the active Actor. */
    lua_register(pLuaState, "Actor_SetProperty", &Hook_Actor_SetProperty);

    /* Actor_GetProperty("Print Combat Rolls") (1 boolean)
       Actor_GetProperty("Print Padding Lines") (1 boolean)
       Actor_GetProperty("Name") (1 string)
       Actor_GetProperty("Team") (1 integer)
       Actor_GetProperty("Description") (1 string)
       Actor_GetProperty("CurrentRoom") (1 string)
       Actor_GetProperty("Is Player In Room")
       Actor_GetProperty("IsPlayerControlled") (1 boolean)
       Actor_GetProperty("Health") (1 integer)
       Actor_GetProperty("MaxHealth") (1 integer)
       Actor_GetProperty("Willpower") (1 integer)
       Actor_GetProperty("MaxWillpower") (1 integer)
       Actor_GetProperty("Stamina") (1 integer)
       Actor_GetProperty("MaxStamina") (1 integer)
       Actor_GetProperty("Base Attack") (1 integer)
       Actor_GetProperty("Base Accuracy") (1 integer)
       Actor_GetProperty("Base Defense") (1 integer)
       Actor_GetProperty("Has Weapon") (1 boolean)
       Actor_GetProperty("Weapon Attack Boost") (1 integer)
       Actor_GetProperty("Weapon Accuracy Boost") (1 integer)
       Actor_GetProperty("Weapon Defense Boost") (1 integer)
       Actor_GetProperty("IsStunned") (1 boolean)
       Actor_GetProperty("IsControlled") (1 boolean)
       Actor_GetProperty("ItemCount", sItemName) (1 integer)
       Actor_GetProperty("Weapon Tier") (1 integer)
       Actor_GetProperty("Instruction To Path To", sRoomName) (1 integer)
       Actor_GetProperty("Resolved Attack Phrase", iVictimID) (1 string)
       Actor_GetProperty("OverrideScript") (1 string)
       Actor_GetProperty("IsObscured") (1 boolean)
       Actor_GetProperty("IsObscuredBy", iID) (1 boolean)
       Gets and returns the requested property from the active Actor. Can return multiple variables
       or none (if it is meant to print something). */
    lua_register(pLuaState, "Actor_GetProperty", &Hook_Actor_GetProperty);

    /* Actor_FlagTurnComplete()
       The currently acting Actor will flag itself as having completed its turn. This should be
       called by subscripts, like item-usage. Note that the Active Object is not referenced, this
       is whichever Actor is *acting*. */
    lua_register(pLuaState, "Actor_FlagTurnComplete", &Hook_Actor_FlagTurnComplete);

    /* Actor_PrintMessage(iCode)
       Prints a message to the console, indicating what the Actor did. The message uses a set of
       constants defined in System/ASetupVariables.lua in your scenario's folder. */
    lua_register(pLuaState, "Actor_PrintMessage", &Hook_Actor_PrintMessage);

    /* Actor_IsXHostileToY(iActorXID, iActorYID)
       Returns true if the two actors are hostile to one another, false otherwise. May return false
       if the IDs are invalid or incorrect arguments were passed. */
    lua_register(pLuaState, "Actor_IsXHostileToY", &Hook_Actor_IsXHostileToY);

    /* Actor_PickUpItem(sItemName)
       The Actor will attempt to pick up the named item. If the item is not found, the Actor will
       skip their turn instead. */
    lua_register(pLuaState, "Actor_PickUpItem", &Hook_Actor_PickUpItem);

    /* Actor_UseItem(sItemName)
       Actor_UseItem(sItemName, iTargetID)
       The Actor will use the named item. If not found, the Actor passes their turn. If a target ID
       is passed, the Actor will use the item on that target. */
    lua_register(pLuaState, "Actor_UseItem", &Hook_Actor_UseItem);

    /* Actor_DropEverything()
       Actor drops everything they are carrying. */
    lua_register(pLuaState, "Actor_DropEverything", &Hook_Actor_DropEverything);

    /* Actor_TripWakeup()
       Trips the wakeup flag, indicating to the AI that it has found something interesting in the
       room it currently is in. This is only used when an AI or player has a queue of instructions.
       If the wakeup is tripped, the instructions are dropped and the AI script runs. */
    lua_register(pLuaState, "Actor_TripWakeup", &Hook_Actor_TripWakeup);

    /* Actor_OverridePortrait("ByPath", sDLPath)
       Actor_OverridePortrait("ByRef", sRefName)
       Causes the Actor to change the currently visible portrait. This can be done either by a direct
       path or by the relative image per-actor. If you use "ByPath" the Actor does not need to be
       active in order for this to work. */
    lua_register(pLuaState, "Actor_OverridePortrait", &Hook_Actor_OverridePortrait);

    /* Actor_GetBaneAgainst(iTargetID)
       Returns the bane factor of the active actor's current weapon against the target ID provided.
       Will return 1.0 in case of error (including the attacker having no weapon). */
    lua_register(pLuaState, "Actor_GetBaneAgainst", &Hook_Actor_GetBaneAgainst);

    /* Actor_SwapWeaponsWith(iTargetID)
       Causes the Active Actor to trade their weapon with the target ID, assuming its valid. */
    lua_register(pLuaState, "Actor_SwapWeaponsWith", &Hook_Actor_SwapWeaponsWith);

    /* Actor_PushWeapon()
       Push the Actor's weapon on the Activity Stack. Can legally push NULL if there's no weapon. */
    lua_register(pLuaState, "Actor_PushWeapon", &Hook_Actor_PushWeapon);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Actor_Spawn(lua_State *L)
{
    //Actor_Spawn(sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Actor_Spawn");

    //--Create.
    Actor *nActor = new Actor();
    nActor->SetName(lua_tostring(L, 1));
    DataLibrary::Fetch()->PushActiveEntity(nActor);

    //--Register.
    EntityManager::Fetch()->RegisterPointer(lua_tostring(L, 1), nActor, &RootEntity::DeleteThis);

    return 0;
}
int Hook_Actor_ExistsWithName(lua_State *L)
{
    //Actor_ExistsWithName(sName) (1 boolean)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("Actor_Spawn");
        lua_pushboolean(L, false);
        return 1;
    }

    //--Variables
    const char *rString = lua_tostring(L, 1);

    //--Check if any Actors match the given name. The pointer must be of the Actor type!
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rEntity = (RootEntity *)rEntityList->PushIterator();
    while(rEntity)
    {
        //--Check the name.
        if(!strcasecmp(rString, rEntity->GetName()) && rEntity->GetType() == POINTER_TYPE_ACTOR)
        {
            rEntityList->PopIterator();
            lua_pushboolean(L, true);
            return 1;
        }

        rEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    lua_pushboolean(L, false);
    return 1;
}
int Hook_Actor_PathTo(lua_State *L)
{
    //Actor_PathTo(sRoomName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Actor_PathTo");

    //--Active object.
    Actor *rActiveActor = (Actor *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveActor || rActiveActor->GetType() != POINTER_TYPE_ACTOR) return LuaTypeError("Actor_PathTo", L);

    //--Set.
    rActiveActor->AppendMoveToRoom(lua_tostring(L, 1));

    return 0;
}
int Hook_Actor_SetProperty(lua_State *L)
{
    //Actor_SetProperty("Name", sName)
    //Actor_SetProperty("Health", iHealth)
    //Actor_SetProperty("Health Bypass", iHealth)
    //Actor_SetProperty("Willpower", iWillpower)
    //Actor_SetProperty("Stamina", iStamina)
    //Actor_SetProperty("Team", iTeamFlag)
    //Actor_SetProperty("Description", sDescription)
    //Actor_SetProperty("CurrentRoom", sRoomName)
    //Actor_SetProperty("PlayerControlled", bFlag)
    //Actor_SetProperty("IgnoresItems", bFlag)
    //Actor_SetProperty("RenderType", iFlag)
    //Actor_SetProperty("Color", fRed, fGreen, fBlue)
    //Actor_SetProperty("Color", fRed, fGreen, fBlue, fAlpha)
    //Actor_SetProperty("AIScript", sPath)
    //Actor_SetProperty("OverrideScript", sPath)
    //Actor_SetProperty("WakeupScript", sPath)
    //Actor_SetProperty("Instruction", iInstruction)
    //Actor_SetProperty("Instruction String", sStringInstruction)
    //Actor_SetProperty("Append Instruction", iInstruction)
    //Actor_SetProperty("Append Instruction String", sStringInstruction)
    //Actor_SetProperty("Image", sRefName, sDLPath)
    //Actor_SetProperty("ActiveImage Ref", sRefName)
    //Actor_SetProperty("ActiveImage DL", sDLPath)
    //Actor_SetProperty("StatMaxes", iHPMax, iWPMax, iSTAMax, iATP, iDEF, iACC)
    //Actor_SetProperty("Health", iAmount)
    //Actor_SetProperty("Willpower", iAmount)
    //Actor_SetProperty("PersistsWhenDowned", bFlag)
    //Actor_SetProperty("RecoversToFull", bFlag)
    //Actor_SetProperty("ScoreForKill", iScore)
    //Actor_SetProperty("RemoveStunned")
    //Actor_SetProperty("RequiresStamina", bFlag)
    //Actor_SetProperty("Wipe Instructions")
    //Actor_SetProperty("AsPlayerAttackPhrase", sPhrase)
    //Actor_SetProperty("AsEnemyAttackPhrase", sPhrase)
    //Actor_SetProperty("AsNeutralAttackPhrase", sPhrase)
    //Actor_SetProperty("ObscuredBy", iObscurerID)
    //Actor_SetProperty("NotObscuredBy", iObscurerID) --Pass 0 to clear all obscuring info.
    //Actor_SetProperty("Bane", sBaneName)
    //Actor_SetProperty("MovementRestriction", bFlag)
    //Actor_SetProperty("ResetWillpowerRegeneration")
    //Actor_SetProperty("Give Item", sItemName)
    //Actor_SetProperty("Lose Item", sItemName)
    //Actor_SetProperty("Flee Cost", iCost) (Static)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Actor_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static]
    //--These don't need Actor copies to work.
    if(!strcasecmp(rSwitchType, "Flee Cost") && tArgs == 2)
    {
        Actor::xBaseFleeCost = lua_tointeger(L, 2);
        return 0;
    }

    //--[Activity Stack]
    //--Active object is required to exist.
    Actor *rActiveActor = (Actor *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_ACTOR)) return LuaTypeError("Actor_SetProperty", L);

    //--Actor's Name. Also sets their name in the EntityManager.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rActiveActor->SetName(lua_tostring(L, 2));
        EntityManager::Fetch()->RenamePointer(lua_tostring(L, 2), rActiveActor);
    }
    //--Actor's current Health. If set to 0, the Actor is stunned (but nothing will be printed).
    //  Monsters at 0 will vanish.
    else if(!strcasecmp(rSwitchType, "Health") && tArgs == 2)
    {
        rActiveActor->SetHealth(lua_tointeger(L, 2));
    }
    //--Same as above, but ignores HP caps.
    else if(!strcasecmp(rSwitchType, "Health Bypass") && tArgs == 2)
    {
        rActiveActor->SetHealthBypass(lua_tointeger(L, 2));
    }
    //--Actor's current Willpower. Can go as low as -5. Monsters at 0 will vanish, players will be
    //  stunned but nothing will automatically print.
    else if(!strcasecmp(rSwitchType, "Willpower") && tArgs == 2)
    {
        rActiveActor->SetWillPower(lua_tointeger(L, 2));
    }
    //--Stamina. Auto-clamps.
    else if(!strcasecmp(rSwitchType, "Stamina") && tArgs == 2)
    {
        rActiveActor->SetStamina(lua_tointeger(L, 2));
    }
    //--Sets which team the Actor is on.
    else if(!strcasecmp(rSwitchType, "Team") && tArgs == 2)
    {
        rActiveActor->SetTeam(lua_tointeger(L, 2));
    }
    //--Sets the Actor's description. Can be called multiple times if the Actor changes states.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rActiveActor->SetDescription(lua_tostring(L, 2));
    }
    //--Changing the Actor's location.
    else if(!strcasecmp(rSwitchType, "CurrentRoom") && tArgs == 2)
    {
        rActiveActor->SetRoomByName(lua_tostring(L, 2));
    }
    //--If the player issues commands during this Actor's turn...
    else if(!strcasecmp(rSwitchType, "PlayerControlled") && tArgs == 2)
    {
        rActiveActor->SetPlayerControl(lua_toboolean(L, 2));
    }
    //--Sets if the Actor cannot pick up items anymore.
    else if(!strcasecmp(rSwitchType, "IgnoresItems") && tArgs == 2)
    {
        rActiveActor->SetIgnoresItems(lua_toboolean(L, 2));
    }
    //--What the entity looks like on the map.
    else if(!strcasecmp(rSwitchType, "RenderType") && tArgs == 2)
    {
        rActiveActor->SetRenderFlag(lua_tointeger(L, 2));
    }
    //--Display color, no alpha.
    else if(!strcasecmp(rSwitchType, "Color") && tArgs == 4)
    {
        rActiveActor->SetLocalColor(StarlightColor::MapRGBF(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4)));
    }
    //--Display color, alpha.
    else if(!strcasecmp(rSwitchType, "Color") && tArgs == 5)
    {
        rActiveActor->SetLocalColor(StarlightColor::MapRGBAF(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5)));
    }
    //--Normal script the entity uses.
    else if(!strcasecmp(rSwitchType, "AIScript") && tArgs == 2)
    {
        rActiveActor->SetAIScript(lua_tostring(L, 2));
    }
    //--Script that controls a victim of a transformation. While active, they are helpless.
    else if(!strcasecmp(rSwitchType, "OverrideScript") && tArgs == 2)
    {
        rActiveActor->SetOverrideScript(lua_tostring(L, 2));
    }
    //--Script the entity uses to check for interesting things.
    else if(!strcasecmp(rSwitchType, "WakeupScript") && tArgs == 2)
    {
        rActiveActor->SetWakeupScript(lua_tostring(L, 2));
    }
    //--Which instruction to use. Direct code.
    else if(!strcasecmp(rSwitchType, "Instruction") && tArgs == 2)
    {
        //rActiveActor->HandleInstruction(lua_tointeger(L, 2));
        //fprintf(stderr, "==> Received instruction %i from script. <==\n", lua_tointeger(L, 2));
        Actor::xInstruction = lua_tointeger(L, 2);
    }
    //--Which instruction to use. Human-readable string.
    else if(!strcasecmp(rSwitchType, "Instruction String") && tArgs == 2)
    {
        //rActiveActor->HandleInstructionS(lua_tostring(L, 2));
        //fprintf(stderr, "==> Received instruction %s from script. <==\n", lua_tostring(L, 2));
        Actor::xInstruction = Actor::GetInstructionFromString(lua_tostring(L, 2));
    }
    //--Which instruction to use. Direct code. Appends to end of instruction list.
    else if(!strcasecmp(rSwitchType, "Append Instruction") && tArgs == 2)
    {
        rActiveActor->AppendInstruction(lua_tointeger(L, 2));
    }
    //--Which instruction to use. Human-readable string. Appends to end of instruction list.
    else if(!strcasecmp(rSwitchType, "Append Instruction String") && tArgs == 2)
    {
        rActiveActor->HandleInstructionS(lua_tostring(L, 2));
    }
    //--Associates an image with this character.
    else if(!strcasecmp(rSwitchType, "Image") && tArgs == 3)
    {
        rActiveActor->RegisterImageS(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets which image is active based on a local pointer.
    else if(!strcasecmp(rSwitchType, "ActiveImage Ref") && tArgs == 2)
    {
        rActiveActor->SetActiveImageS(lua_tostring(L, 2));
    }
    //--Sets which image is active based on a global pointer.
    else if(!strcasecmp(rSwitchType, "ActiveImage DL") && tArgs == 2)
    {
        rActiveActor->SetActiveImageDL(lua_tostring(L, 2));
    }
    //--Combat Statistics
    else if(!strcasecmp(rSwitchType, "StatMaxes") && tArgs == 7)
    {
        CombatStats tStatPack;
        tStatPack.Set(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7));
        rActiveActor->SetCombatStatistics(tStatPack);
    }
    //--If the Actor is defeated, do they disappear or just fall over for 15 turns?
    else if(!strcasecmp(rSwitchType, "PersistsWhenDowned") && tArgs == 2)
    {
        rActiveActor->SetPersistFlag(lua_toboolean(L, 2));
    }
    //--When the Actor gets back up, do they stay at 5HP or go to max?
    else if(!strcasecmp(rSwitchType, "RecoversToFull") && tArgs == 2)
    {
        rActiveActor->SetRecoverToFullFlag(lua_toboolean(L, 2));
    }
    //--When defeated by the player's team, how many points do the players get?
    else if(!strcasecmp(rSwitchType, "ScoreForKill") && tArgs == 2)
    {
        rActiveActor->SetScoreForKill(lua_tointeger(L, 2));
    }
    //--Immediately removes the stunned conditions. Will provide 5HP if the target does not
    //  heal to full when getting up.
    else if(!strcasecmp(rSwitchType, "RemoveStunned") && tArgs == 1)
    {
        rActiveActor->Revive(true);
    }
    //--Whether or not the Actor requires stamina to run from encounters.
    else if(!strcasecmp(rSwitchType, "RequiresStamina") && tArgs == 2)
    {
        rActiveActor->SetFleeingFlag(lua_toboolean(L, 2));
    }
    //--Wipe all active instructions that are queued.
    else if(!strcasecmp(rSwitchType, "Wipe Instructions") && tArgs == 1)
    {
        rActiveActor->WipeInstructions();
    }
    //--Phrase to use when attacking as a player.
    else if(!strcasecmp(rSwitchType, "AsPlayerAttackPhrase") && tArgs == 2)
    {
        rActiveActor->SetAsPlayerAttackPhrase(lua_tostring(L, 2));
    }
    //--Phrase to use when attacking (a player) as an enemy.
    else if(!strcasecmp(rSwitchType, "AsEnemyAttackPhrase") && tArgs == 2)
    {
        rActiveActor->SetAsEnemyAttackPhrase(lua_tostring(L, 2));
    }
    //--Phrase to use when two non-player entities attack each other.
    else if(!strcasecmp(rSwitchType, "AsNeutralAttackPhrase") && tArgs == 2)
    {
        rActiveActor->SetAsNeutralAttackPhrase(lua_tostring(L, 2));
    }
    //--If this entity is obscured by something.
    else if(!strcasecmp(rSwitchType, "ObscuredBy") && tArgs == 2)
    {
        rActiveActor->AddObscurer(lua_tointeger(L, 2));
    }
    //--Drop obscuring.
    else if(!strcasecmp(rSwitchType, "NotObscuredBy") && tArgs == 2)
    {
        rActiveActor->RemoveObscurer(lua_tointeger(L, 2));
    }
    //--Batmaaaaaaan.
    else if(!strcasecmp(rSwitchType, "Bane") && tArgs == 2)
    {
        rActiveActor->AddBane(lua_tostring(L, 2));
    }
    //--If this flag is true, the player cannot leave the lakeside.
    else if(!strcasecmp(rSwitchType, "MovementRestriction") && tArgs == 2)
    {
        rActiveActor->SetMovementRestriction(lua_toboolean(L, 2));
    }
    //--Resets the WP regen timer. It will kick in in 3 more turns.
    else if(!strcasecmp(rSwitchType, "ResetWillpowerRegeneration") && tArgs == 1)
    {
        rActiveActor->ResetWillpowerRegen();
    }
    //--Gives an Actor a copy of the named item.
    else if(!strcasecmp(rSwitchType, "Give Item") && tArgs == 2)
    {
        rActiveActor->ReceiveClonedItem(lua_tostring(L, 2));
    }
    //--Removes the named item from the Actor's inventory, if it exists.
    else if(!strcasecmp(rSwitchType, "Lose Item") && tArgs == 2)
    {
        rActiveActor->LoseItem(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("Actor_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_Actor_GetProperty(lua_State *L)
{
    //Actor_GetProperty("Print Combat Rolls") (1 boolean / static)
    //Actor_GetProperty("Print Padding Lines") (1 boolean / static)
    //Actor_GetProperty("Name") (1 string)
    //Actor_GetProperty("Team") (1 integer)
    //Actor_GetProperty("Description") (1 string)
    //Actor_GetProperty("CurrentRoom") (1 string)
    //Actor_GetProperty("Is Player In Room") (1 boolean)
    //Actor_GetProperty("IsPlayerControlled") (1 boolean)
    //Actor_GetProperty("Persists When Knocked Out") (1 boolean)
    //Actor_GetProperty("Health") (1 integer)
    //Actor_GetProperty("MaxHealth") (1 integer)
    //Actor_GetProperty("Willpower") (1 integer)
    //Actor_GetProperty("MaxWillpower") (1 integer)
    //Actor_GetProperty("Stamina") (1 integer)
    //Actor_GetProperty("MaxStamina") (1 integer)
    //Actor_GetProperty("Base Attack") (1 integer)
    //Actor_GetProperty("Base Accuracy") (1 integer)
    //Actor_GetProperty("Base Defense") (1 integer)
    //Actor_GetProperty("Has Weapon") (1 boolean)
    //Actor_GetProperty("Weapon Attack Boost") (1 integer)
    //Actor_GetProperty("Weapon Accuracy Boost") (1 integer)
    //Actor_GetProperty("Weapon Defense Boost") (1 integer)
    //Actor_GetProperty("IsStunned") (1 boolean)
    //Actor_GetProperty("IsControlled") (1 boolean)
    //Actor_GetProperty("ItemCount", sItemName) (1 integer)
    //Actor_GetProperty("Weapon Tier") (1 integer)
    //Actor_GetProperty("Instruction To Path To", sRoomName) (1 integer)
    //Actor_GetProperty("Resolved Attack Phrase", iVictimID) (1 string)
    //Actor_GetProperty("OverrideScript") (1 string)
    //Actor_GetProperty("IsObscured") (1 boolean)
    //Actor_GetProperty("IsObscuredBy", iID) (1 boolean)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Actor_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Types] Don't require an Actor.
    //--Static display boolean: Show combat rolls.
    if(!strcasecmp(rSwitchType, "Print Combat Rolls") && tArgs == 1)
    {
        lua_pushboolean(L, Actor::xPrintCombatRolls);
        return 1;
    }
    //--Static display boolean: Print padding lines.
    else if(!strcasecmp(rSwitchType, "Print Padding Lines") && tArgs == 1)
    {
        lua_pushboolean(L, Actor::xPrintPaddingLines);
        return 1;
    }

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Actor *rActiveActor = (Actor *)rDataLibrary->rActiveObject;
    if(!rActiveActor || rActiveActor->GetType() != POINTER_TYPE_ACTOR) return LuaTypeError("Actor_GetProperty", L);

    //--The name of the Actor.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        lua_pushstring(L, rActiveActor->GetName());
        tReturns = 1;
    }
    //--The team the Actor is on.
    else if(!strcasecmp(rSwitchType, "Team") && tArgs == 1)
    {
        lua_pushinteger(L, (int)rActiveActor->GetTeam());
        tReturns = 1;
    }
    //--The team the Actor is on.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 1)
    {
        lua_pushstring(L, rActiveActor->GetDescription());
        tReturns = 1;
    }
    //--What room the Actor is currently in.
    else if(!strcasecmp(rSwitchType, "CurrentRoom") && tArgs == 1)
    {
        //--Get and check. If invalid, return "Null".
        PandemoniumRoom *rCurrentRoom = rActiveActor->GetCurrentRoom();
        if(!rCurrentRoom)
        {
            lua_pushstring(L, "Null");
        }
        //--Valid result.
        else
        {
            lua_pushstring(L, rCurrentRoom->GetName());
        }

        //--In either case, 1 return.
        tReturns = 1;
    }
    //--Returns true if there's a player-controlled character in the room with this Actor.
    else if(!strcasecmp(rSwitchType, "Is Player In Room") && tArgs == 1)
    {
        //--Obviously, if the Actor is player-controlled then just return true right now.
        if(rActiveActor->IsPlayerControlled())
        {
            lua_pushboolean(L, true);
        }
        //--Otherwise, scan the room around them.
        else
        {
            PandemoniumRoom *rCurrentRoom = rActiveActor->GetCurrentRoom();
            if(!rCurrentRoom)
            {
                lua_pushboolean(L, false);
            }
            else
            {
                Actor *rPlayer = rCurrentRoom->IsPlayerPresent();
                lua_pushboolean(L, (rPlayer != NULL));
            }
        }
        return 1;
    }
    //--If the Actor is player-controlled.
    else if(!strcasecmp(rSwitchType, "IsPlayerControlled") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsPlayerControlled());
        return 1;
    }
    //--Whether the Actor sticks around after getting KO'd.
    else if(!strcasecmp(rSwitchType, "Persists When Knocked Out") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->PersistsThroughKnockout());
        return 1;
    }
    //--Actor's current health. Can't be negative.
    else if(!strcasecmp(rSwitchType, "Health") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mHP);
        return 1;
    }
    //--Actor's maximum health. Can't be less than one.
    else if(!strcasecmp(rSwitchType, "MaxHealth") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mHPMax);
        return 1;
    }
    //--Actor's current willpower. Can be negative!
    else if(!strcasecmp(rSwitchType, "Willpower") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mWillPower);
        return 1;
    }
    //--Actor's maximum willpower. Can't be less than one.
    else if(!strcasecmp(rSwitchType, "MaxWillpower") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mWillPowerMax);
        return 1;
    }
    //--Actor's stamina.
    else if(!strcasecmp(rSwitchType, "Stamina") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mStamina);
        return 1;
    }
    //--Actor's stamina maximum.
    else if(!strcasecmp(rSwitchType, "MaxStamina") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mStaminaMax);
        return 1;
    }
    //--Actor's innate attack boost.
    else if(!strcasecmp(rSwitchType, "Base Attack") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mAttackPower);
        return 1;
    }
    //--Actor's innate accuracy boost.
    else if(!strcasecmp(rSwitchType, "Base Accuracy") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mAccuracy);
        return 1;
    }
    //--Actor's innate defense boost.
    else if(!strcasecmp(rSwitchType, "Base Defense") && tArgs == 1)
    {
        CombatStats tStatPack = rActiveActor->GetCombatStatistics();
        lua_pushinteger(L, tStatPack.mDefensePower);
        return 1;
    }
    //--Whether or not this Actor is holding a weapon.
    else if(!strcasecmp(rSwitchType, "Has Weapon") && tArgs == 1)
    {
        InventoryItem *rWeapon = rActiveActor->GetWeapon();
        lua_pushboolean(L, (rWeapon != NULL));
        return 1;
    }
    //--Weapon damage boost.
    else if(!strcasecmp(rSwitchType, "Weapon Attack Boost") && tArgs == 1)
    {
        InventoryItem *rWeapon = rActiveActor->GetWeapon();
        if(!rWeapon)
        {
            lua_pushinteger(L, 0);
        }
        else
        {
            lua_pushinteger(L, rWeapon->GetAttackPower());
        }
        return 1;
    }
    //--Weapon accuracy boost.
    else if(!strcasecmp(rSwitchType, "Weapon Accuracy Boost") && tArgs == 1)
    {
        InventoryItem *rWeapon = rActiveActor->GetWeapon();
        if(!rWeapon)
        {
            lua_pushinteger(L, 0);
        }
        else
        {
            lua_pushinteger(L, rWeapon->GetAccuracyBonus());
        }
        return 1;
    }
    //--Weapon defense boost.
    else if(!strcasecmp(rSwitchType, "Weapon Defense Boost") && tArgs == 1)
    {
        InventoryItem *rWeapon = rActiveActor->GetWeapon();
        if(!rWeapon)
        {
            lua_pushinteger(L, 0);
        }
        else
        {
            lua_pushinteger(L, rWeapon->GetDefensePower());
        }
        return 1;
    }
    //--If the Actor is stunned, this returns true. Note that the Actor can be 'controlled' and
    //  will NOT qualify as stunned in these cases.
    else if(!strcasecmp(rSwitchType, "IsStunned") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsStunned());
        return 1;
    }
    //--If the Actor is under foreign control, returns true. This is the case for charming and
    //  other things which may make the character behave hostile-y. It doesn't reverse hostility flags.
    else if(!strcasecmp(rSwitchType, "IsControlled") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsControlled());
        return 1;
    }
    //--How many of a given item are in the inventory. Can be zero.
    else if(!strcasecmp(rSwitchType, "ItemCount") && tArgs == 2)
    {
        lua_pushinteger(L, rActiveActor->GetItemCount(lua_tostring(L, 2)));
        return 1;
    }
    //--The tier of the current weapon. If the Actor has no weapon, -1 is returned.
    else if(!strcasecmp(rSwitchType, "Weapon Tier") && tArgs == 1)
    {
        InventoryItem *rWeapon = rActiveActor->GetWeapon();
        if(!rWeapon)
        {
            lua_pushinteger(L, -1);
        }
        else
        {
            lua_pushinteger(L, rWeapon->GetItemTier());
        }

        return 1;
    }
    //--The instruction needed to path to the given location. Returns -1 if we're in the room.
    else if(!strcasecmp(rSwitchType, "Instruction To Path To") && tArgs == 2)
    {
        lua_pushinteger(L, rActiveActor->GetInstructionForMoveTo(lua_tostring(L, 2)));
        return 1;
    }
    //--The phrase used when attacking someone. Requires some cleanup.
    else if(!strcasecmp(rSwitchType, "Resolved Attack Phrase") && tArgs == 2)
    {
        //--Get the entity from the EM.
        Actor *rVictim = (Actor *)EntityManager::Fetch()->GetEntityI(lua_tointeger(L, 2));

        //--Error check.
        if(!rVictim || rVictim->GetType() != POINTER_TYPE_ACTOR)
        {
            lua_pushstring(L, "Error: Victim was invalid.");
        }
        //--Success case.
        else
        {
            char *tString = rActiveActor->AssembleAttackPhrase(rVictim);
            lua_pushstring(L, tString);
            free(tString);
        }

        return 1;
    }
    else if(!strcasecmp(rSwitchType, "OverrideScript") && tArgs == 1)
    {
        lua_pushstring(L, rActiveActor->GetOverrideScript());
        return 1;
    }
    //--Whether or not the actor is obscured by something.
    else if(!strcasecmp(rSwitchType, "IsObscured") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsObscured());
        return 1;
    }
    //--Whether or not the actor is obscured by the entity with the given ID.
    else if(!strcasecmp(rSwitchType, "IsObscuredBy") && tArgs == 2)
    {
        lua_pushboolean(L, rActiveActor->IsObscuredBy(lua_tointeger(L, 2)));
        return 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("Actor_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_Actor_FlagTurnComplete(lua_State *L)
{
    //Actor_FlagTurnComplete()
    Actor::xInstruction = INSTRUCTION_COMPLETETURN;
    return 0;
}
int Hook_Actor_PrintMessage(lua_State *L)
{
    //Actor_PrintMessage(sMessage)
    //Actor_PrintMessage(iCode, bIsPlayer, sActorName, sSubjectName)
    int tArgs = lua_gettop(L);

    //--Get the currently acting Actor.
    Actor *rCurrentActor = EntityManager::Fetch()->GetActingEntity();

    //--If 1 arg was passed, it's a string. Just print that.
    if(tArgs == 1)
    {
        Actor::PrintMessage(lua_tostring(L, 1));
    }
    //--4 args, print by code.
    else if(tArgs == 4 && rCurrentActor)
    {
        rCurrentActor->PrintByCode(lua_tointeger(L, 1), lua_toboolean(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), NULL);
    }
    //--Unknown argument counts.
    else
    {
        return LuaArgError("Actor_PrintMessage");
    }
    return 0;
}
int Hook_Actor_IsXHostileToY(lua_State *L)
{
    //Actor_IsXHostileToY(iActorXID, iActorYID)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("Actor_IsXHostileToY");

    //--Get both the Actors. If either is illegal, then return false.
    EntityManager *rEntityManager = EntityManager::Fetch();
    Actor *rActorX = (Actor *)rEntityManager->GetEntityI(lua_tointeger(L, 1));
    Actor *rActorY = (Actor *)rEntityManager->GetEntityI(lua_tointeger(L, 2));
    if(!rActorX || !rActorY)
    {
        lua_pushboolean(L, false);
        return 1;
    }

    //--Query the Actors against each other.
    lua_pushboolean(L, rActorX->IsHostileTo(rActorY));
    return 1;
}
int Hook_Actor_PickUpItem(lua_State *L)
{
    //Actor_PickUpItem(sItemName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Actor_PickUpItem");

    //--Get the currently acting Actor.
    Actor *rCurrentActor = EntityManager::Fetch()->GetActingEntity();
    if(!rCurrentActor) return 0;

    //--Pick the item up.
    rCurrentActor->PickUpItemByName(lua_tostring(L, 1));
    Actor::xInstruction = INSTRUCTION_COMPLETETURN;
    return 0;
}
int Hook_Actor_SwapWeaponsWith(lua_State *L)
{
    //Actor_SwapWeaponsWith(iActorID)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Actor_SwapWeaponsWith");

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Actor *rActiveActor = (Actor *)rDataLibrary->rActiveObject;
    if(!rActiveActor || rActiveActor->GetType() != POINTER_TYPE_ACTOR) return LuaTypeError("Actor_SwapWeaponsWith", L);

    rActiveActor->SwapWeaponsWith(lua_tointeger(L, 1));

    return 0;
}
int Hook_Actor_UseItem(lua_State *L)
{
    //Actor_UseItem(sItemName)
    //Actor_UseItem(sItemName, iTargetID)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Actor_UseItem");

    //--Get the currently acting Actor.
    Actor *rCurrentActor = EntityManager::Fetch()->GetActingEntity();
    if(!rCurrentActor) return 0;

    //--Use the item on yourself:
    if(tArgs == 1)
    {
        rCurrentActor->UseItemByName(lua_tostring(L, 1));
        Actor::xInstruction = INSTRUCTION_COMPLETETURN;
    }
    //--Use the item on someone else.
    else
    {
        rCurrentActor->UseItemByNameOn(lua_tostring(L, 1), lua_tointeger(L, 2));
        Actor::xInstruction = INSTRUCTION_COMPLETETURN;
    }
    return 0;
}
int Hook_Actor_DropEverything(lua_State *L)
{
    //Actor_DropEverything()

    //--Get the currently acting Actor.
    Actor *rActiveActor = (Actor *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveActor || rActiveActor->GetType() != POINTER_TYPE_ACTOR) return LuaTypeError("Actor_DropEverything", L);

    //--Pick the item up.
    rActiveActor->DropEverything();
    return 0;
}
int Hook_Actor_TripWakeup(lua_State *L)
{
    //Actor_TripWakeup()
    Actor::xWakeUpFlag = true;
    return 0;
}
int Hook_Actor_OverridePortrait(lua_State *L)
{
    //Actor_OverridePortrait("ByPath", sDLPath)
    //Actor_OverridePortrait("ByRef", sRefName)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("Actor_OverridePortrait");

    //--Make sure a level exists.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel) return 0;

    //--If this is "ByPath" we just set it directly.
    const char *rSwitchType = lua_tostring(L, 1);
    if(!strcasecmp(rSwitchType, "ByPath"))
    {
        //--Get it from the DL.
        StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(lua_tostring(L, 2));

        //--Set it.
        rActiveLevel->SetActivePortrait(rImage);
    }
    //--Relative, we need the Actor to exist first.
    else
    {
        //--Active object.
        Actor *rActiveActor = (Actor *)DataLibrary::Fetch()->rActiveObject;
        if(!rActiveActor || rActiveActor->GetType() != POINTER_TYPE_ACTOR) return LuaTypeError("Actor_OverridePortrait", L);

        //--Get the relative portrait.
        StarBitmap *rImage = rActiveActor->GetImageRef(lua_tostring(L, 2));
        rActiveLevel->SetActivePortrait(rImage);
    }
    return 0;
}
int Hook_Actor_GetBaneAgainst(lua_State *L)
{
    //Actor_GetBaneAgainst(iTargetID)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("Actor_GetBaneAgainst");
        lua_pushnumber(L, 1.0f);
        return 1;
    }

    //--Attacker should be atop the activity stack.
    Actor *rActiveActor = (Actor *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveActor || rActiveActor->GetType() != POINTER_TYPE_ACTOR)
    {
        LuaTypeError("Actor_GetBaneAgainst", L);
        lua_pushnumber(L, 1.0f);
        return 1;
    }

    //--Run the query.
    float tBaneValue = rActiveActor->GetBaneOfTarget(lua_tointeger(L, 1));
    lua_pushnumber(L, tBaneValue);
    return 1;
}
int Hook_Actor_PushWeapon(lua_State *L)
{
    //Actor_PushWeapon()
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Actor *rActiveActor = (Actor *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Type checker.
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_ACTOR))
    {
        return 0;
    }

    //--Get the weapon and push it. It can be null!
    InventoryItem *rWeapon = rActiveActor->GetWeapon();
    rDataLibrary->rActiveObject = rWeapon;
    return 0;
}
