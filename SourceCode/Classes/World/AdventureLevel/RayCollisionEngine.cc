//--Base
#include "RayCollisionEngine.h"

//--Classes
#include "TiledLevel.h"
#include "WADFile.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers

///========================================== System ==============================================
RayCollisionEngine::RayCollisionEngine()
{
    ///--[RayCollisionEngine]
    //--System
    mIsBooted = false;
    mCurrentRayPulse = 0;

    //--Total Line List
    mMasterLineList = new StarLinkedList(true);

    //--Map Properties
    mTileSize = 16.0f; //Default

    //--Blockmap Properties
    mBlockmapSizeX = 0;
    mBlockmapSizeY = 0;
    mBlockmap = NULL;
    mBlockmapDimX = 256.0f;
    mBlockmapDimY = 256.0f;
}
RayCollisionEngine::~RayCollisionEngine()
{
    delete mMasterLineList;
    for(int x = 0; x < mBlockmapSizeX; x ++)
    {
        for(int y = 0; y < mBlockmapSizeY; y ++)
        {
            delete mBlockmap[x][y];
        }
        free(mBlockmap[x]);
    }
    free(mBlockmap);
}

///===================================== Property Queries =========================================
void RayCollisionEngine::GetShortestIntercept(float pXA, float pYA, float pXB, float pYB, float &sInterceptX, float &sInterceptY)
{
    //--Uses the blockmap to quickly find the shortest resulting line after intersections are applied.
    sInterceptX = pXB;
    sInterceptY = pYB;
    if(!mIsBooted) return;

    //--Increment the pulse counter.
    mCurrentRayPulse ++;

    //--Locate the initial blockmap position.
    int cBlockmapXA = pXA / mBlockmapDimX;
    int cBlockmapYA = pYA / mBlockmapDimY;
    int cBlockmapXB = pXB / mBlockmapDimX;
    int cBlockmapYB = pYB / mBlockmapDimY;

    //--Switch values so the A is always the lower of the two.
    if(cBlockmapXB < cBlockmapXA)
    {
        int cTemp = cBlockmapXA;
        cBlockmapXA = cBlockmapXB;
        cBlockmapXB = cTemp;
    }
    if(cBlockmapYB < cBlockmapYA)
    {
        int cTemp = cBlockmapYA;
        cBlockmapYA = cBlockmapYB;
        cBlockmapYB = cTemp;
    }

    //--Clamp.
    if(cBlockmapXA < 0) cBlockmapXA = 0;
    if(cBlockmapYA < 0) cBlockmapYA = 0;
    if(cBlockmapXB < 0) cBlockmapXB = 0;
    if(cBlockmapYB < 0) cBlockmapYB = 0;
    if(cBlockmapXA >= mBlockmapSizeX-1) cBlockmapXA = mBlockmapSizeX-1;
    if(cBlockmapYA >= mBlockmapSizeY-1) cBlockmapYA = mBlockmapSizeY-1;
    if(cBlockmapXB >= mBlockmapSizeX-1) cBlockmapXB = mBlockmapSizeX-1;
    if(cBlockmapYB >= mBlockmapSizeY-1) cBlockmapYB = mBlockmapSizeY-1;

    //--Iterate across the blockmap.
    for(int x = cBlockmapXA; x <= cBlockmapXB; x ++)
    {
        for(int y = cBlockmapYA; y <= cBlockmapYB; y ++)
        {
            //--Iterate across all lines in this blockmap.
            float cInterceptionX = 0.0f;
            float cInterceptionY = 0.0f;
            RCELine *rLineSegment = (RCELine *)mBlockmap[x][y]->PushIterator();
            while(rLineSegment)
            {
                //--If the pulse on this line segment is equal to the current pulse, it's a duplicate. Ignore it.
                if(rLineSegment->mLastRayPulse == mCurrentRayPulse)
                {
                    rLineSegment = (RCELine *)mBlockmap[x][y]->AutoIterate();
                    continue;
                }

                //--Check if there's an interception. If there is, change the interception point and keep going.
                if(WADFile::GetLineIntersection(pXA, pYA, sInterceptX, sInterceptY, rLineSegment->mXA, rLineSegment->mYA, rLineSegment->mXB, rLineSegment->mYB, &cInterceptionX, &cInterceptionY) != 0)
                {
                    sInterceptX = cInterceptionX;
                    sInterceptY = cInterceptionY;
                }

                //--Mark this line to prevent duplication.
                rLineSegment->mLastRayPulse = mCurrentRayPulse;

                //--Next.
                rLineSegment = (RCELine *)mBlockmap[x][y]->AutoIterate();
            }
        }
    }
}

///======================================== Manipulators ==========================================
void RayCollisionEngine::SetTileSize(float pValue)
{
    mTileSize = pValue;
    if(mTileSize < 1.0f) mTileSize = 1.0f;
}
void RayCollisionEngine::RegisterVariableLine(const char *pLineName, float pXA, float pYA, float pXB, float pYB)
{
    //--Registers a line to the master list with the given name and properties. This line will be
    //  added to the blockmap. These lines usually represent objects like doors which can be
    //  disabled and therefore change the collision logic.
    if(!pLineName) return;

    //--Create the line and add it to the master list.
    SetMemoryData(__FILE__, __LINE__);
    RCELine *nLineSegment = (RCELine *)starmemoryalloc(sizeof(RCELine));
    nLineSegment->mXA = pXA;
    nLineSegment->mYA = pYA;
    nLineSegment->mXB = pXB;
    nLineSegment->mYB = pYB;
    nLineSegment->mLastRayPulse = 0;
    nLineSegment->RecomputeAngle();
    mMasterLineList->AddElementAsHead(pLineName, nLineSegment, &FreeThis);

    //--Register it to all valid blockmaps.
    float cDummyX = 0.0f;
    float cDummyY = 0.0f;
    for(int x = 0; x < mBlockmapSizeX; x ++)
    {
        for(int y = 0; y < mBlockmapSizeY; y ++)
        {
            //--Boundaries of this blockmap.
            RCELine tLineTop;
            tLineTop.mXA = mBlockmapDimX * (x+0);
            tLineTop.mYA = mBlockmapDimY * (y+0);
            tLineTop.mXB = mBlockmapDimX * (x+1);
            tLineTop.mYB = mBlockmapDimY * (y+0);

            RCELine tLineRgt;
            tLineRgt.mXA = mBlockmapDimX * (x+1);
            tLineRgt.mYA = mBlockmapDimY * (y+0);
            tLineRgt.mXB = mBlockmapDimX * (x+1);
            tLineRgt.mYB = mBlockmapDimY * (y+1);

            RCELine tLineBot;
            tLineBot.mXA = mBlockmapDimX * (x+0);
            tLineBot.mYA = mBlockmapDimY * (y+1);
            tLineBot.mXB = mBlockmapDimX * (x+1);
            tLineBot.mYB = mBlockmapDimY * (y+1);

            RCELine tLineLft;
            tLineLft.mXA = mBlockmapDimX * (x+0);
            tLineLft.mYA = mBlockmapDimY * (y+0);
            tLineLft.mXB = mBlockmapDimX * (x+0);
            tLineLft.mYB = mBlockmapDimY * (y+1);

            //--If point A is within this block:
            if(IsPointWithin(nLineSegment->mXA, nLineSegment->mYA, mBlockmapDimX * (x+0), mBlockmapDimY * (y+0), mBlockmapDimX * (x+1), mBlockmapDimY * (y+1)))
            {
                mBlockmap[x][y]->AddElement("X", nLineSegment);
            }
            //--If point B is within this block:
            else if(IsPointWithin(nLineSegment->mXB, nLineSegment->mYB, mBlockmapDimX * (x+0), mBlockmapDimY * (y+0), mBlockmapDimX * (x+1), mBlockmapDimY * (y+1)))
            {
                mBlockmap[x][y]->AddElement("X", nLineSegment);
            }
            //--If the line collides with the top dimension:
            else if(WADFile::GetLineIntersection(nLineSegment->mXA, nLineSegment->mYA, nLineSegment->mXB, nLineSegment->mYB, tLineTop.mXA, tLineTop.mYA, tLineTop.mXB, tLineTop.mYB, &cDummyX, &cDummyY) != 0)
            {
                mBlockmap[x][y]->AddElement("X", nLineSegment);
            }
            //--If the line collides with the right dimension:
            else if(WADFile::GetLineIntersection(nLineSegment->mXA, nLineSegment->mYA, nLineSegment->mXB, nLineSegment->mYB, tLineRgt.mXA, tLineRgt.mYA, tLineRgt.mXB, tLineRgt.mYB, &cDummyX, &cDummyY) != 0)
            {
                mBlockmap[x][y]->AddElement("X", nLineSegment);
            }
            //--If the line collides with the bottom dimension:
            else if(WADFile::GetLineIntersection(nLineSegment->mXA, nLineSegment->mYA, nLineSegment->mXB, nLineSegment->mYB, tLineBot.mXA, tLineBot.mYA, tLineBot.mXB, tLineBot.mYB, &cDummyX, &cDummyY) != 0)
            {
                mBlockmap[x][y]->AddElement("X", nLineSegment);
            }
            //--If the line collides with the left dimension:
            else if(WADFile::GetLineIntersection(nLineSegment->mXA, nLineSegment->mYA, nLineSegment->mXB, nLineSegment->mYB, tLineLft.mXA, tLineLft.mYA, tLineLft.mXB, tLineLft.mYB, &cDummyX, &cDummyY) != 0)
            {
                mBlockmap[x][y]->AddElement("X", nLineSegment);
            }
        }
    }
}
void RayCollisionEngine::UnsetVariableLine(const char *pLineName)
{
    //--Locate the line in question.
    RCELine *rLine = (RCELine *)mMasterLineList->GetElementByName(pLineName);
    if(!rLine) return;

    //--If the position of the line is above -1000.0f, unset it.
    if(rLine->mXA > -1000.0f)
    {
        rLine->mXA = -1000.0f - rLine->mXA;
        rLine->mYA = -1000.0f - rLine->mYA;
        rLine->mXB = -1000.0f - rLine->mXB;
        rLine->mYB = -1000.0f - rLine->mYB;
    }
}
void RayCollisionEngine::ResetVariableLine(const char *pLineName)
{
    //--Locate the line in question.
    RCELine *rLine = (RCELine *)mMasterLineList->GetElementByName(pLineName);
    if(!rLine) return;

    //--If the position of the line is below -1000.0f, reset it.
    if(rLine->mXA < -1000.0f)
    {
        rLine->mXA = -1000.0f - rLine->mXA;
        rLine->mYA = -1000.0f - rLine->mYA;
        rLine->mXB = -1000.0f - rLine->mXB;
        rLine->mYB = -1000.0f - rLine->mYB;
    }
}

///======================================== Core Methods ==========================================
void RayCollisionEngine::BuildWith(CollisionPack pCollisionPack)
{
    ///--[Documentation and Setup]
    //--Builds the collision engine against the provided collision pack.
    //fprintf(stderr, "Building against collision pack. Size: %i %i\n", pCollisionPack.mCollisionSizeX, pCollisionPack.mCollisionSizeY);

    ///--[Initial Scan]
    //--First, scan across all the collisions and build lines for each one.
    for(int x = 0; x < pCollisionPack.mCollisionSizeX; x ++)
    {
        for(int y = 0; y < pCollisionPack.mCollisionSizeY; y ++)
        {
            //--Skip empty slots.
            if(pCollisionPack.mCollisionData[x][y] == CLIP_NONE) continue;

            //--If the position to the left is not blocked, create the left line.
            if(!IsLftBlocked(x, y, pCollisionPack))
            {
                CreateLftLine(x, y, pCollisionPack);
            }

            //--If the position above is not blocked, create the top line.
            if(!IsTopBlocked(x, y, pCollisionPack))
            {
                CreateTopLine(x, y, pCollisionPack);
            }

            //--If the position to the right is not blocked, create the right line.
            if(!IsRgtBlocked(x, y, pCollisionPack))
            {
                CreateRgtLine(x, y, pCollisionPack);
            }

            //--If the position below is not blocked, create the bottom line.
            if(!IsBotBlocked(x, y, pCollisionPack))
            {
                CreateBotLine(x, y, pCollisionPack);
            }
        }
    }

    //--Report.
    //fprintf(stderr, "After collision scan, has %i lines total.\n", mMasterLineList->GetListSize());

    ///--[Simplification]
    //--Scan across all the lines. If two lines are parallel and share a point, they merge.
    int tPassCount = 0;
    while(true)
    {
        //--Setup.
        tPassCount ++;
        bool tRemoveOriginalLine = false;
        bool tDidAnything = false;

        //--Scan across the list and look for any two lines that match a point and are parallel.
        RCELine *rLineSegmentA = (RCELine *)mMasterLineList->PushIterator();
        while(rLineSegmentA)
        {
            //--Use the Random-Access Pointer. On a match we'll be removing the second line.
            mMasterLineList->SetRandomPointerToThis(rLineSegmentA);
            RCELine *rLineSegmentB = (RCELine *)mMasterLineList->IncrementAndGetRandomPointerEntry();
            while(rLineSegmentB)
            {
                //--Check the angle for parallel cases.
                if(rLineSegmentA->mAngle == rLineSegmentB->mAngle)
                {
                    //--BAAB case.
                    if(rLineSegmentA->mXA == rLineSegmentB->mXA && rLineSegmentA->mYA == rLineSegmentB->mYA)
                    {
                        //--Always remove the second line.
                        tDidAnything = true;

                        //--If the B positions are identical, both lines get removed.
                        if(rLineSegmentA->mXB == rLineSegmentB->mXB && rLineSegmentA->mYB == rLineSegmentB->mYB)
                        {
                            tRemoveOriginalLine = true;
                        }
                        //--Not identical, merge the lines.
                        else
                        {
                            rLineSegmentA->mXA = rLineSegmentB->mXB;
                            rLineSegmentA->mYA = rLineSegmentB->mYB;
                        }
                    }
                    //--ABBA case.
                    else if(rLineSegmentA->mXB == rLineSegmentB->mXB && rLineSegmentA->mYB == rLineSegmentB->mYB)
                    {
                        //--Always remove the second line.
                        tDidAnything = true;

                        //--Merge the lines. A can't be identical or the above case would have caught it.
                        rLineSegmentA->mXB = rLineSegmentB->mXA;
                        rLineSegmentA->mYB = rLineSegmentB->mYA;
                    }
                    //--BABA case.
                    else if(rLineSegmentA->mXA == rLineSegmentB->mXB && rLineSegmentA->mYA == rLineSegmentB->mYB)
                    {
                        //--Always remove the second line.
                        tDidAnything = true;

                        //--If the ABAB case is also true, remove both lines.
                        if(rLineSegmentA->mXB == rLineSegmentB->mXA && rLineSegmentA->mYB == rLineSegmentB->mYA)
                        {
                            tRemoveOriginalLine = true;
                        }
                        //--Merge the lines.
                        else
                        {
                            rLineSegmentA->mXA = rLineSegmentB->mXA;
                            rLineSegmentA->mYA = rLineSegmentB->mYA;
                        }
                    }
                    //--ABAB case.
                    else if(rLineSegmentA->mXB == rLineSegmentB->mXA && rLineSegmentA->mYB == rLineSegmentB->mYA)
                    {
                        //--Always remove the second line.
                        tDidAnything = true;

                        //--BABA case can't be true or the above case would have caught it.
                        rLineSegmentA->mXB = rLineSegmentB->mXB;
                        rLineSegmentA->mYB = rLineSegmentB->mYB;
                    }
                }

                //--If this flag got set, we need to remove the second point and stop iterating here.
                if(tDidAnything)
                {
                    mMasterLineList->RemoveRandomPointerEntry();
                    break;
                }

                //--Next.
                rLineSegmentB = (RCELine *)mMasterLineList->IncrementAndGetRandomPointerEntry();
            }

            //--If this flag got set, we also need to remove the first line.
            if(tRemoveOriginalLine)
            {
                mMasterLineList->SetRandomPointerToThis(rLineSegmentA);
                mMasterLineList->RemoveRandomPointerEntry();
                break;
            }

            //--If this flag got set, stop iterating.
            if(tDidAnything) break;

            //--Next.
            rLineSegmentA = (RCELine *)mMasterLineList->AutoIterate();
        }

        //--If we didn't edit the list at all, we're done.
        if(!tDidAnything) break;
    }

    ///--[Initializer]
    //--Performs post-hoc initialization. All lines need to set their pulse counters to zero. This is done after
    //  trimming since initializing a line that's going to get deleted is a waste.
    RCELine *rLineSegment = (RCELine *)mMasterLineList->PushIterator();
    while(rLineSegment)
    {
        rLineSegment->mLastRayPulse = 0;
        rLineSegment = (RCELine *)mMasterLineList->AutoIterate();
    }

    //--Report.
    //fprintf(stderr, "Optimized lines in %i passes. Final line count: %i\n", tPassCount, mMasterLineList->GetListSize());

    ///--[Build Blockmaps]
    //--Blockmaps default sizes.
    mBlockmapDimX = 64.0f;
    mBlockmapDimY = 64.0f;

    //--Compute the blockmap sizes. We need to go up by 1.
    mBlockmapSizeX = ((pCollisionPack.mCollisionSizeX * mTileSize) / mBlockmapDimX) + 1;
    mBlockmapSizeY = ((pCollisionPack.mCollisionSizeY * mTileSize) / mBlockmapDimY) + 1;

    //--Allocate space.
    SetMemoryData(__FILE__, __LINE__);
    mBlockmap = (StarLinkedList ***)starmemoryalloc(sizeof(StarLinkedList **) * mBlockmapSizeX);
    for(int x = 0; x < mBlockmapSizeX; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mBlockmap[x] = (StarLinkedList **)starmemoryalloc(sizeof(StarLinkedList *) * mBlockmapSizeY);
        for(int y = 0; y < mBlockmapSizeY; y ++)
        {
            mBlockmap[x][y] = new StarLinkedList(false);
        }
    }

    //--Iterate across the blockmaps. Build a list of all lines that intersect this blockmap or are wholly within it.
    float cDummyX = 0.0f;
    float cDummyY = 0.0f;
    for(int x = 0; x < mBlockmapSizeX; x ++)
    {
        for(int y = 0; y < mBlockmapSizeY; y ++)
        {
            //--Boundaries of this blockmap.
            RCELine tLineTop;
            tLineTop.mXA = mBlockmapDimX * (x+0);
            tLineTop.mYA = mBlockmapDimY * (y+0);
            tLineTop.mXB = mBlockmapDimX * (x+1);
            tLineTop.mYB = mBlockmapDimY * (y+0);

            RCELine tLineRgt;
            tLineRgt.mXA = mBlockmapDimX * (x+1);
            tLineRgt.mYA = mBlockmapDimY * (y+0);
            tLineRgt.mXB = mBlockmapDimX * (x+1);
            tLineRgt.mYB = mBlockmapDimY * (y+1);

            RCELine tLineBot;
            tLineBot.mXA = mBlockmapDimX * (x+0);
            tLineBot.mYA = mBlockmapDimY * (y+1);
            tLineBot.mXB = mBlockmapDimX * (x+1);
            tLineBot.mYB = mBlockmapDimY * (y+1);

            RCELine tLineLft;
            tLineLft.mXA = mBlockmapDimX * (x+0);
            tLineLft.mYA = mBlockmapDimY * (y+0);
            tLineLft.mXB = mBlockmapDimX * (x+0);
            tLineLft.mYB = mBlockmapDimY * (y+1);

            //--Iterate across all the lines.
            RCELine *rLineSegment = (RCELine *)mMasterLineList->PushIterator();
            while(rLineSegment)
            {
                //--If point A is within this block:
                if(IsPointWithin(rLineSegment->mXA, rLineSegment->mYA, mBlockmapDimX * (x+0), mBlockmapDimY * (y+0), mBlockmapDimX * (x+1), mBlockmapDimY * (y+1)))
                {
                    mBlockmap[x][y]->AddElement("X", rLineSegment);
                }
                //--If point B is within this block:
                else if(IsPointWithin(rLineSegment->mXB, rLineSegment->mYB, mBlockmapDimX * (x+0), mBlockmapDimY * (y+0), mBlockmapDimX * (x+1), mBlockmapDimY * (y+1)))
                {
                    mBlockmap[x][y]->AddElement("X", rLineSegment);
                }
                //--If the line collides with the top dimension:
                else if(WADFile::GetLineIntersection(rLineSegment->mXA, rLineSegment->mYA, rLineSegment->mXB, rLineSegment->mYB, tLineTop.mXA, tLineTop.mYA, tLineTop.mXB, tLineTop.mYB, &cDummyX, &cDummyY) != 0)
                {
                    mBlockmap[x][y]->AddElement("X", rLineSegment);
                }
                //--If the line collides with the right dimension:
                else if(WADFile::GetLineIntersection(rLineSegment->mXA, rLineSegment->mYA, rLineSegment->mXB, rLineSegment->mYB, tLineRgt.mXA, tLineRgt.mYA, tLineRgt.mXB, tLineRgt.mYB, &cDummyX, &cDummyY) != 0)
                {
                    mBlockmap[x][y]->AddElement("X", rLineSegment);
                }
                //--If the line collides with the bottom dimension:
                else if(WADFile::GetLineIntersection(rLineSegment->mXA, rLineSegment->mYA, rLineSegment->mXB, rLineSegment->mYB, tLineBot.mXA, tLineBot.mYA, tLineBot.mXB, tLineBot.mYB, &cDummyX, &cDummyY) != 0)
                {
                    mBlockmap[x][y]->AddElement("X", rLineSegment);
                }
                //--If the line collides with the left dimension:
                else if(WADFile::GetLineIntersection(rLineSegment->mXA, rLineSegment->mYA, rLineSegment->mXB, rLineSegment->mYB, tLineLft.mXA, tLineLft.mYA, tLineLft.mXB, tLineLft.mYB, &cDummyX, &cDummyY) != 0)
                {
                    mBlockmap[x][y]->AddElement("X", rLineSegment);
                }

                //--Next.
                rLineSegment = (RCELine *)mMasterLineList->AutoIterate();
            }
        }
    }

    //--Report.
    //fprintf(stderr, "Blockmap grid is %i x %i\n", mBlockmapSizeX, mBlockmapSizeY);
    //for(int x = 0; x < mBlockmapSizeX; x ++)
    //{
    //    for(int y = 0; y < mBlockmapSizeY; y ++)
    //    {
            //fprintf(stderr, " Block %2ix%2i: %i\n", x, y, mBlockmap[x][y]->GetListSize());
    //    }
    //}

    ///--[Finish Up]
    //--Report.
    mIsBooted = true;
    //fprintf(stderr, "Registered %i lines total.\n", mMasterLineList->GetListSize());
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
uint8_t *RayCollisionEngine::CreateDataBuffer(uint32_t &sDataSize)
{
    //--Creates and returns a data buffer which contains all the information of the RCE. Used to speed
    //  up level loading since collision data doesn't need to change.
    sDataSize = 0;
    StarAutoBuffer *tAutoBuffer = new StarAutoBuffer();

    //--Header.
    tAutoBuffer->AppendStringWithoutNull("RCEDATA000");

    //--How many lines there are total.
    tAutoBuffer->AppendUInt32(mMasterLineList->GetListSize());

    //--For each line, store its X1/Y1, X2/Y2, and angle.
    RCELine *rLineSegment = (RCELine *)mMasterLineList->PushIterator();
    while(rLineSegment)
    {
        //--Append.
        tAutoBuffer->AppendFloat(rLineSegment->mXA);
        tAutoBuffer->AppendFloat(rLineSegment->mYA);
        tAutoBuffer->AppendFloat(rLineSegment->mXB);
        tAutoBuffer->AppendFloat(rLineSegment->mYB);
        tAutoBuffer->AppendFloat(rLineSegment->mAngle);

        //--Next.
        rLineSegment = (RCELine *)mMasterLineList->AutoIterate();
    }

    //--Store the blockmap information.
    tAutoBuffer->AppendUInt32(mBlockmapSizeX);
    tAutoBuffer->AppendUInt32(mBlockmapSizeY);
    tAutoBuffer->AppendFloat(mBlockmapDimX);
    tAutoBuffer->AppendFloat(mBlockmapDimY);

    //--Append the blockmap data. Each blockmap has a count and then indices of lines.
    for(int x = 0; x < mBlockmapSizeX; x ++)
    {
        for(int y = 0; y < mBlockmapSizeY; y ++)
        {
            //--Write how many lines are in this blockmap.
            tAutoBuffer->AppendUInt32(mBlockmap[x][y]->GetListSize());

            //--For each line, write a 32-bit integer indicating its index in the master list.
            rLineSegment = (RCELine *)mBlockmap[x][y]->PushIterator();
            while(rLineSegment)
            {
                //--Get the index.
                int cIndex = mMasterLineList->GetSlotOfElementByPtr(rLineSegment);
                tAutoBuffer->AppendUInt32(cIndex);

                //--Next.
                rLineSegment = (RCELine *)mBlockmap[x][y]->AutoIterate();
            }
        }
    }

    //--Liberate the auto buffer's data and pass it back.
    sDataSize = tAutoBuffer->GetSize();
    uint8_t *nDataBuffer = tAutoBuffer->LiberateData();
    delete tAutoBuffer;
    return nDataBuffer;
}
void RayCollisionEngine::ReadFromFile(VirtualFile *fInfile)
{
    ///--[Documentation]
    //--Constructs the RCE data from the given virtual file, assuming the cursor is in position.
    //  Does nothing if the file doesn't exist.
    //--It is expected that you do this with a new class. Don't build collision data and then read
    //  data or the results are undefined you dingus.
    if(!fInfile) return;

    //--Get how many lines we can expect.
    uint32_t tExpectedLines = 0;
    fInfile->Read(&tExpectedLines, sizeof(uint32_t), 1);

    //--Read the lines out and add them to the master list.
    for(int i = 0; i < (int)tExpectedLines; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        RCELine *nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        fInfile->Read(&nLine->mXA, sizeof(float), 1);
        fInfile->Read(&nLine->mYA, sizeof(float), 1);
        fInfile->Read(&nLine->mXB, sizeof(float), 1);
        fInfile->Read(&nLine->mYB, sizeof(float), 1);
        fInfile->Read(&nLine->mAngle, sizeof(float), 1);
        nLine->mLastRayPulse = 0;
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }

    //--Read the blockmap data.
    fInfile->Read(&mBlockmapSizeX, sizeof(uint32_t), 1);
    fInfile->Read(&mBlockmapSizeY, sizeof(uint32_t), 1);
    fInfile->Read(&mBlockmapDimX, sizeof(float), 1);
    fInfile->Read(&mBlockmapDimY, sizeof(float), 1);

    //--Allocate blockmap space and read the data as we go.
    SetMemoryData(__FILE__, __LINE__);
    mBlockmap = (StarLinkedList ***)starmemoryalloc(sizeof(StarLinkedList **) * mBlockmapSizeX);
    for(int x = 0; x < mBlockmapSizeX; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mBlockmap[x] = (StarLinkedList **)starmemoryalloc(sizeof(StarLinkedList *) * mBlockmapSizeY);
        for(int y = 0; y < mBlockmapSizeY; y ++)
        {
            //--Allocate.
            mBlockmap[x][y] = new StarLinkedList(false);

            //--Get how many lines are in this block.
            uint32_t tLinesInThisBlock = 0;
            fInfile->Read(&tLinesInThisBlock, sizeof(uint32_t), 1);

            //--For each line, get the index in the master list and store a reference.
            for(int i = 0; i < (int)tLinesInThisBlock; i ++)
            {
                //--Get index.
                uint32_t tIndex = 0;
                fInfile->Read(&tIndex, sizeof(uint32_t), 1);

                //--Get pointer and store.
                void *rCheckPtr = mMasterLineList->GetElementBySlot(tIndex);
                mBlockmap[x][y]->AddElement("X", rCheckPtr);
            }
        }
    }

    //--No crash? Okay, this is ready.
    mIsBooted = true;
}

///========================================== Drawing =============================================
void RayCollisionEngine::RenderLines(float pOffsetX, float pOffsetY, float pScale)
{
    ///--[Documentation and Setup]
    //--Renders all the lines in the engine from the master list. Each end point has a circle on it.
    return;
    if(!mIsBooted || pScale == 0.0f) return;
    glDisable(GL_TEXTURE_2D);

    ///--[Setup]
    //--Set the camera and scaling up.
    glScalef(pScale, pScale, 1.0f);
    glTranslatef(pOffsetX, pOffsetY, 0.0f);

    ///--[Line Segments]
    //--Segments render in blue.
    glLineWidth(3.0f);
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINES);

        //--Iterate across the lines.
        RCELine *rLineSegment = (RCELine *)mMasterLineList->PushIterator();
        while(rLineSegment)
        {
            glVertex2f(rLineSegment->mXA, rLineSegment->mYA);
            glVertex2f(rLineSegment->mXB, rLineSegment->mYB);

            //--Next.
            rLineSegment = (RCELine *)mMasterLineList->AutoIterate();
        }
    glEnd();

    ///--[Points]
    //--Points render in red.
    glColor3f(1.0f, 0.0f, 0.0f);
    glPointSize(3.0f);
    glBegin(GL_POINTS);

        //--Iterate across the lines.
        rLineSegment = (RCELine *)mMasterLineList->PushIterator();
        while(rLineSegment)
        {
            glVertex2f(rLineSegment->mXA, rLineSegment->mYA);
            glVertex2f(rLineSegment->mXB, rLineSegment->mYB);

            //--Next.
            rLineSegment = (RCELine *)mMasterLineList->AutoIterate();
        }
    glEnd();

    ///--[Clean]
    glTranslatef(-pOffsetX, -pOffsetY, 0.0f);
    glScalef(1.0f / pScale, 1.0f / pScale, 1.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);
    glLineWidth(1.0f);
    glPointSize(1.0f);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
