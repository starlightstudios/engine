-- |[ ===================================== fnGameExists() ===================================== ]|
--Returns true if the entry has a valid path, false if not.
function fnGameExists(psGameName)
    
    --Deprecated Warning
    if(gbPrintDeprecatedCallWarnings == true) then
        io.write("Warning: Calling fnGameExists() is deprecated. Call from: " .. LM_GetCallStack(0) .. "\n")
    end
    
    -- |[Argument Check]|
    if(psGameName == nil) then return false end
    
    -- |[Scan]|
    --Get the index of the game.
    local iIndex = fnGetGameIndex(psGameName)
    if(iIndex == 0) then return false end
    
    --Scan the paths. If an active path is found, the game exists.
    local iActivePath = fnScanPaths(gsaGameEntries[iIndex].sSearchPaths)
    if(iActivePath > 0) then return true end
        
    --No active path.
    return false
end
