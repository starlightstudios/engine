//--Base
#include "WADFile.h"

//--Classes
#include "WorldPolygon.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "GlDfn.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--Handles the process for saving the compiled data to a file. This is not going to be a .wad file, as it will
//  be missing things like sector information and sidedefs. Instead, it just takes all the faces generated during
//  light construction and writes those. It also writes textures and lightmaps if flagged to.
//--Incidentally, this process makes the file a stand-alone level. It doesn't require root resources.

//--[Definitions]
//--Version. We don't support older versions. Versions increase linearly.
#define SGD_VERSION 0

//--Flags indicating what kind of data the file stores.
#define SGD_SAVE_POLYGONS 0x01
#define SGD_SAVE_TEXTURES 0x02
#define SGD_SAVE_LIGHTMAPS 0x04
#define SGD_SAVE_TEXNAMES 0x08

//--Which flag we should write to the file, indicating how much information should be provided.
#define SGD_SAVE_FLAG (SGD_SAVE_POLYGONS | SGD_SAVE_TEXTURES | SGD_SAVE_LIGHTMAPS)

//--[Debug Functions]
//#define WFSAVE_DEBUG
#ifdef WFSAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Entry Function]
#include "DataLibrary.h"
void WADFile::SaveToFile(const char *pPath)
{
    //--[Documenation]
    //--Does all the hard work of saving information to the file provided. Overwrites the file if it already exists.
    DebugPush(true, "Writing WADFile to hard drive.\n");
    if(!pPath)
    {
        DebugPop("Failed. No path.\n");
        return;
    }

    //--[Checks and Setup]
    //--Make sure we actually have something to save. If no faces were compiled and we're in primitive rendering move,
    //  we don't do anything.
    if(!mArbitraryPolysConstructionList)
    {
        //--No polygon list? Try to build one.
        DebugPrint("No polygon list, attempting to build one.\n");
        SubdividePolygons();

        //--Failed!
        if(!mArbitraryPolysConstructionList)
        {
            DebugPop("Failed. No arbitrary polygon list, unable to build one.\n");
            return;
        }
    }

    //--AutoBuffer, makes the writing process more streamlined.
    StarAutoBuffer *mOutBuffer = new StarAutoBuffer();

    //--Build a list of textures that are actually used in the level. The polygons refer to this list
    //  when storing their texture lookups.
    BuildCompressedTextureTable();

    //--[Header]
    //--Write some numbers to indicate the version and to make sure that this is the right type of file.
    mOutBuffer->AppendCharacter('S');
    mOutBuffer->AppendCharacter('G');
    mOutBuffer->AppendCharacter('D');
    mOutBuffer->AppendCharacter(SGD_VERSION);

    //--Type information. Write a single byte which indicates what sort of things are in the file.
    mOutBuffer->AppendCharacter(SGD_SAVE_FLAG);

    //--[Write Polygons]
    //--Only occurs if the flag is set to write these. If the texture flag is set, polygons will refer to texture
    //  indexes, otherwise they will write texture names.
    if(SGD_SAVE_FLAG & SGD_SAVE_POLYGONS)
    {
        //--Write a header. This makes it easier to find in the file.
        mOutBuffer->AppendCharacter('P');
        mOutBuffer->AppendCharacter('O');
        mOutBuffer->AppendCharacter('L');
        mOutBuffer->AppendCharacter('Y');

        //--Write how many polygons are expected.
        mOutBuffer->AppendInt32(mArbitraryPolysConstructionList->GetListSize());

        //--Loop through all the polygons and write their data. The polygons have an internal function for this.
        WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
        while(rPolygon)
        {
            rPolygon->WriteToBuffer(this, mOutBuffer, false);
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
        }
    }

    //--[Write Textures]
    //--Only occurs if the flag is set to write textures.
    if(SGD_SAVE_FLAG & SGD_SAVE_TEXTURES)
    {
        //--Write a header. This makes it easier to find in the file.
        mOutBuffer->AppendCharacter('T');
        mOutBuffer->AppendCharacter('E');
        mOutBuffer->AppendCharacter('X');
        mOutBuffer->AppendCharacter('S');

        //--Write how many textures we have.
        DebugPrint("Writing %i textures.\n", mCompressedTextureTable->GetListSize());
        mOutBuffer->AppendInt16(mCompressedTextureTable->GetListSize());

        //--Setup.
        int tWid = 0;
        int tHei = 0;
        void *pData = NULL;

        //--Loop through the compressed texture table, write each bitmap. This may take some time, as the data needs to
        //  be pulled off the graphics card. It will not necessarily be the exact same as the information from the base
        //  file, or the base file might not have been present at all.
        StarBitmap *rBitmap = (StarBitmap *)mCompressedTextureTable->PushIterator();
        while(rBitmap)
        {
            //--Size an array out if we need more space. This can save us some allocation.
            if(!pData || tWid * tHei < rBitmap->GetWidth() * rBitmap->GetHeight())
            {
                free(pData);
                tWid = rBitmap->GetWidth();
                tHei = rBitmap->GetHeight();
                SetMemoryData(__FILE__, __LINE__);
                pData = starmemoryalloc(sizeof(uint8_t) * tWid * tHei * 4);
            }

            //--Bind, get the data out.
            tWid = rBitmap->GetWidth();
            tHei = rBitmap->GetHeight();
            rBitmap->Bind();
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData);

            //--Write.
            mOutBuffer->AppendInt16(tWid);
            mOutBuffer->AppendInt16(tHei);
            mOutBuffer->AppendVoidData(pData, sizeof(uint8_t) * tWid * tHei * 4);

            //--Next.
            rBitmap = (StarBitmap *)mCompressedTextureTable->AutoIterate();
        }

        //--Clean up.
        free(pData);
    }

    //--[Write Lightmaps]
    //--Only occurs if the flag is set to write lightmaps. Lightmaps may not write even if flagged, if they were
    //  not generated in the engine options. In that case, the total number of lightmaps will be 0.
    //--If lightmaps do write, they are written in parallel with the polygons. Polygon0 uses Lightmap0.
    if(SGD_SAVE_FLAG & SGD_SAVE_LIGHTMAPS)
    {
        //--Write a header. This makes it easier to find in the file.
        mOutBuffer->AppendCharacter('L');
        mOutBuffer->AppendCharacter('G');
        mOutBuffer->AppendCharacter('H');
        mOutBuffer->AppendCharacter('T');

        //--We don't need to write how many lightmaps to expect here, because it's the same number as the number
        //  of polygons. Just write the information from the polygons.
        WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
        while(rPolygon)
        {
            rPolygon->WriteLightmapToBuffer(mOutBuffer);
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
        }
    }

    //--[Finish Up]
    //--Write the information to the file.
    FILE *fOutfile = fopen(pPath, "wb");
    fwrite(mOutBuffer->GetRawData(), sizeof(uint8_t), mOutBuffer->GetCursor(), fOutfile);
    fclose(fOutfile);

    //--Clean.
    delete mOutBuffer;
    DebugPop("Finished writing WADFile to hard drive.\n");
}

//--[Loading]
void WADFile::LoadFromFile(const char *pPath)
{
    //--[Documentation]
    //--Loads data from the provided path. If it doesn't exist, the level will not be cleared. However, the file
    //  can exist but not contain any data, in which case the level *is* cleared.
    if(!pPath) return;

    //--Debug.
    DebugManager::PushPrint(false, "WADFile:LoadFromFile - Starting load from %s.\n", pPath);

    //--[Setup]
    //--Check for file existence.
    VirtualFile *fInfile = new VirtualFile(pPath, false);
    if(!fInfile || !fInfile->IsReady())
    {
        DebugManager::PopPrint("Finished load, file was not found.\n");
        return;
    }

    //--Preserve the skybox.
    Skybox *rSkyboxStorage = LiberateSkybox();

    //--Clear the level. This purges *all* data.
    DebugManager::Print("Clearing previous data.\n");
    Clear(false);

    //--Set the skybox back in place.
    ReceiveSkybox(rSkyboxStorage);

    //--[Header]
    //--Read four characters. These will be the header and indicate the version. Stop here if it's the wrong file.
    DebugManager::Print("Checking header.\n");
    char tHeader[4];
    fInfile->Read(tHeader, sizeof(char), 4);
    if(tHeader[0] != 'S' || tHeader[1] != 'G' || tHeader[2] != 'D' || tHeader[3] != 0)
    {
        DebugManager::PopPrint("Finished load, file was of erroneous type. Header: %c%c%c%c.\n", tHeader[0], tHeader[1], tHeader[2], tHeader[3]);
        return;
    }

    //--Debug, report the header.
    DebugManager::Print("Successfully read header %c%c%c%c.\n", tHeader[0], tHeader[1], tHeader[2], tHeader[3]);

    //--Read which kinds of information are expected in the file. Sometimes, lightmap/texture data might be in a different file.
    uint8_t tExpectedData = 0x00;
    fInfile->Read(&tExpectedData, sizeof(uint8_t), 1);
    DebugManager::Print("Expected data flag: %i\n", tExpectedData);

    //--[Polygons]
    //--Read the polygon data if flagged for it.
    if(tExpectedData & SGD_SAVE_POLYGONS)
    {
        //--Debug.
        DebugManager::PushPrint(false, "Reading polygons.\n");

        //--Read the header to make sure this is of the right type. If the parts are stored in the wrong order,
        //  we need to report that to the user.
        fInfile->Read(tHeader, sizeof(char), 4);
        if(tHeader[0] != 'P' || tHeader[1] != 'O' || tHeader[2] != 'L' || tHeader[3] != 'Y')
        {
            DebugManager::PopPrint("Finished load, polygon header was erroneous. Header: %c%c%c%c.\n", tHeader[0], tHeader[1], tHeader[2], tHeader[3]);
            return;
        }

        //--Create the polygon list.
        mArbitraryPolysConstructionList = new StarLinkedList(true);

        //--Read how many polygons are expected as a 32-bit integer.
        int32_t tExpectedPolys = 0;
        fInfile->Read(&tExpectedPolys, sizeof(int32_t), 1);

        //--Read polygons and create them.
        for(int i = 0; i < tExpectedPolys; i ++)
        {
            //--Create a new WorldPolygon.
            SetMemoryData(__FILE__, __LINE__);
            WorldPolygon *nNewPolygon = (WorldPolygon *)starmemoryalloc(sizeof(WorldPolygon));
            nNewPolygon->Initialize();

            //--Read the polygon's data. A subroutine does this for us.
            nNewPolygon->ReadFromFile(fInfile);
            nNewPolygon->Compress();

            //--Store the polygon.
            if(nNewPolygon->mTemporaryTextureIndex != -2)
            {
                mArbitraryPolysConstructionList->AddElementAsTail("X", nNewPolygon, &WorldPolygon::DeleteThis);
            }
            //--Sky texture. Ignore this polygon.
            else
            {
                WorldPolygon::DeleteThis(nNewPolygon);
            }
        }

        //--Debug.
        DebugManager::PopPrint("Finished reading polygons. Got %i polygons.\n", mArbitraryPolysConstructionList->GetListSize());
    }

    //--[Atlas Storage]
    int tAtlasTexturesTotal = 0;
    int16_t *tStoredWid = NULL;
    int16_t *tStoredHei = NULL;
    uint8_t **tStoredData = NULL;

    //--[Read Textures]
    //--Reads out the textures and places them in the compressed texture table. There is no distinction between
    //  a wall and a flat in this case!
    if(tExpectedData & SGD_SAVE_TEXTURES)
    {
        //--Debug.
        DebugManager::PushPrint(false, "Reading textures.\n");

        //--Bitmap flags.
        StarBitmap::xStaticFlags.mUplMagFilter = GL_LINEAR;
        StarBitmap::xStaticFlags.mUplMinFilter = GL_LINEAR;
        StarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
        StarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;

        //--Flag this on, so we correctly deallocate textures when clearing.
        mIsCompressedTableMasterCopy = true;

        //--Read the header to make sure this is of the right type. If the parts are stored in the wrong order,
        //  we need to report that to the user.
        fInfile->Read(tHeader, sizeof(char), 4);
        if(tHeader[0] != 'T' || tHeader[1] != 'E' || tHeader[2] != 'X' || tHeader[3] != 'S')
        {
            DebugManager::PopPrint("Finished load, texture header was erroneous. Header: %c%c%c%c.\n", tHeader[0], tHeader[1], tHeader[2], tHeader[3]);
            return;
        }

        //--Read how many textures are expected as a 32-bit integer.
        int16_t tExpectedTextures = 0;
        fInfile->Read(&tExpectedTextures, sizeof(int16_t), 1);

        //--Setup. We reuse the buffer whenever possible, unless atlasing is enabled.
        int32_t tOldSize = 0;
        int16_t tWid, tHei;
        uint8_t *tDataPtr = NULL;

        //--Extra variables for the texture atlas.
        if(!xDisallowTextureAtlas)
        {
            tAtlasTexturesTotal = tExpectedTextures;
            SetMemoryData(__FILE__, __LINE__);
            tStoredWid = (int16_t *)starmemoryalloc(sizeof(int16_t) * tAtlasTexturesTotal);
            SetMemoryData(__FILE__, __LINE__);
            tStoredHei = (int16_t *)starmemoryalloc(sizeof(int16_t) * tAtlasTexturesTotal);
            SetMemoryData(__FILE__, __LINE__);
            tStoredData = (uint8_t **)starmemoryalloc(sizeof(void *) * tAtlasTexturesTotal);
        }

        //--Read and add the textures.
        for(int i = 0; i < tExpectedTextures; i ++)
        {
            //--Read the size.
            fInfile->Read(&tWid, sizeof(int16_t), 1);
            fInfile->Read(&tHei, sizeof(int16_t), 1);

            //--Allocate and read the data.
            if(!tDataPtr || tOldSize < (int)(sizeof(uint8_t) * tWid * tHei * 4))
            {
                tOldSize = sizeof(uint8_t) * tWid * tHei * 4;
                SetMemoryData(__FILE__, __LINE__);
                tDataPtr = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tWid * tHei * 4);
            }

            //--Read.
            fInfile->Read(tDataPtr, sizeof(uint8_t), tWid * tHei * 4);

            //--Create a new texture.
            StarBitmap *nNewBitmap = new StarBitmap((uint8_t *)tDataPtr, tWid, tHei);
            mCompressedTextureTable->AddElementAsTail("X", nNewBitmap, &StarBitmap::DeleteThis);

            //--If building atlases, store the texture data and clear the buffer.
            if(!xDisallowTextureAtlas)
            {
                tStoredWid[i] = tWid;
                tStoredHei[i] = tHei;
                tStoredData[i] = tDataPtr;
                tDataPtr = NULL;
            }
        }
        DebugManager::Print("Read %i textures and added them to compression table.\n", mCompressedTextureTable->GetListSize());

        //--Clean.
        free(tDataPtr);
        StarBitmap::RestoreDefaultTextureFlags();

        //--Debug.
        DebugManager::PopPrint("Finished reading texture data. Got %i textures.\n", mCompressedTextureTable->GetListSize());
    }

    //--[Read Lightmaps]
    //--Reads lightmaps into the polygons. The polygons are expected to have been created in the same order as the
    //  lightmaps were written.
    /*
    if(tExpectedData & SGD_SAVE_LIGHTMAPS)
    {
        //--Debug.
        DebugManager::PushPrint(true, "Reading lightmaps.\n");

        //--Bitmap flags.
        StarBitmap::xStaticFlags.mUplMagFilter = GL_LINEAR;
        StarBitmap::xStaticFlags.mUplMinFilter = GL_LINEAR;
        StarBitmap::xStaticFlags.mUplWrapS = GL_CLAMP_TO_EDGE;
        StarBitmap::xStaticFlags.mUplWrapT = GL_CLAMP_TO_EDGE;

        //--Read the header to make sure this is of the right type. If the parts are stored in the wrong order,
        //  we need to report that to the user.
        fInfile->Read(tHeader, sizeof(char), 4);
        if(tHeader[0] != 'L' || tHeader[1] != 'G' || tHeader[2] != 'H' || tHeader[3] != 'T')
        {
            DebugManager::PopPrint("Finished load, lightmap header was erroneous. Header: %c%c%c%c.\n", tHeader[0], tHeader[1], tHeader[2], tHeader[3]);
            return;
        }

        //--For each polygon, call the subroutine to load the lightmap data.
        WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
        while(rPolygon)
        {
            rPolygon->ReadLightmapFromFile(fInfile, true);
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
        }

        //--Debug.
        DebugManager::PopPrint("Finished reading lightmaps.\n");
        StarBitmap::RestoreDefaultTextureFlags();
    }*/

    //--[Texture Atlas]
    //--Compiles the read textures into a single texture. This is needed for Vertex Arrays to render correctly.
    if(!xDisallowTextureAtlas)
    {
        //--Subroutine builds the texture atlas.
        if(xRenderTextures)
        {
            DebugManager::Print("Assembling texture atlas.\n");
            BuildTextureAtlas(tStoredWid, tStoredHei, tStoredData);
            DebugManager::Print("Completed.\n");
        }

        //--Note: Lightmaps are currently disabled. A single white pixel will always be generated.
        if(xRenderLightmaps)
        {
            DebugManager::Print("Assembling lightmap atlas.\n");
            BuildLightmapAtlas(true);
            DebugManager::Print("Completed.\n");
        }

        //--Clean up.
        DebugManager::Print("Cleaning up.\n");
        for(int i = 0; i < tAtlasTexturesTotal; i ++) free(tStoredData[i]);
        free(tStoredData);
        free(tStoredWid);
        free(tStoredHei);
    }
    //--[Crossload]
    //--Provide texture references to all the polygons from the compressed list.
    else
    {
        DebugManager::Print("Crossloading texture references.\n");
        WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
        while(rPolygon)
        {
            //--The texture references the compressed texture array. If the index was out of range, a valid NULL
            //  gets passed back. This will get error barked.
            rPolygon->rTexture = (StarBitmap *)mCompressedTextureTable->GetElementBySlot(rPolygon->mTemporaryTextureIndex);
            if(!rPolygon->rTexture)
            {
                DebugManager::ForcePrint("WADFile:LoadFromFile - Error, polygon %i had a NULL texture in slot %i.\n", mArbitraryPolysConstructionList->GetSlotOfElementByPtr(rPolygon), rPolygon->mTemporaryTextureIndex);
            }

            //--Next polygon.
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
        }
    }

    //--[Finish Up]
    //--Clean up.
    DebugManager::Print("Cleaning up.\n");
    delete fInfile;

    //--If everything went well, we set these flags. The level is ready to go.
    mIsReady = true;
    mHasActiveLevel = true;

    //--Build height lookup information.
    BuildHeightLookups();

    //--Debug.
    DebugManager::PopPrint("Load completed.\n");
}
void WADFile::BuildTextureAtlas(int16_t *pWidths, int16_t *pHeights, uint8_t **pTextureData)
{
    //--[Documentation]
    //--Builds a single texture which contains all the other textures in the level. This reduces the number of times the
    //  renderer needs to bind the texture. Vertex-Array-Objects require an atlas to be built or they won't render.
    DebugPush(true, "WADFile:BuildTextureAtlas - Begin.\n");

    //--[Setup]
    //--Maximum size.
    int cMaxSize = StarBitmap::xMaxTextureSize;

    //--Size Constants
    int tTotalW = 0;
    int tTotalH = 0;
    int tCurX = 0;
    int tCurY = 0;

    //--Storage.
    int tTallestSoFar = 0;
    SetMemoryData(__FILE__, __LINE__);
    mTexCoordAtlas = (Point3D *)starmemoryalloc(sizeof(Point3D) * mCompressedTextureTable->GetListSize());

    //--[Size Computation]
    //--Determine how large the atlas needs to be. If the atlas exceeds the XSize, it goes to the next line downwards.
    DebugPrint("Scanning bitmap sizes.\n");
    for(int i = 0; i < mCompressedTextureTable->GetListSize(); i ++)
    {
        //--Set the position for this texture.
        mTexCoordAtlas[i].Set(tCurX, tCurY, 0.0f);

        //--Move the cursor.
        tCurX += pWidths[i];

        //--Right-edge check.
        if(tCurX > cMaxSize)
        {
            //--Clamp the total W at the texture max size.
            tTotalW = cMaxSize;

            //--X cursor moves back to the far right.
            tCurX = pWidths[i];

            //--Move the Y cursor down by the tallest.
            tCurY = tCurY + tTallestSoFar;

            //--Re-store the position.
            mTexCoordAtlas[i].Set(0, tCurY, 0.0f);

            //--Tallest is now the current height.
            tTallestSoFar = pHeights[i];
        }
        //--No edge violation, check if this is the tallest.
        else
        {
            if(pHeights[i] > tTallestSoFar) tTallestSoFar = pHeights[i];
        }
    }

    //--Set the max X to be the current cursor, or the max size.
    if(tTotalW == 0) tTotalW = tCurX;

    //--Set the max Y size to the current Y plus tallest.
    tTotalH = tCurY + tTallestSoFar;

    //--If there were somehow zero pixels, quit here.
    if(tTotalH < 1 || tTotalW < 1)
    {
        DebugPop("WADFile:BuildTextureAtlas - Exit, texture atlas size was somehow zero pixels.\n");
        return;
    }

    //--Report.
    DebugPrint("Atlas report:\n");
    DebugPrint(" Maximum texture dimensions: %i\n", cMaxSize);
    DebugPrint(" Expected atlas size: %i x %i\n", tTotalW, tTotalH);
    DebugPrint(" Number of textures in atlas: %i\n", mCompressedTextureTable->GetListSize());

    //--[Allocate and Fill]
    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tAtlasData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tTotalW * tTotalH * 4);

    //--Fill the atlas with the data from the original.
    DebugPrint("Filling data, %i pixels allocated.\n", tTotalW * tTotalH * 4);
    for(int i = 0; i < mCompressedTextureTable->GetListSize(); i ++)
    {
        //--For each pixel...
        for(int p = 0; p < pWidths[i] * pHeights[i]; p ++)
        {
            //--Determine the index of the pixel in the texture data.
            int tIndexXOrig = p % pWidths[i];
            int tIndexYOrig = p / pWidths[i];
            int tOriginalIndex = ((tIndexYOrig * pWidths[i]) + tIndexXOrig) * 4;

            //--Determine the index of the pixel in the atlas.
            int tIndexXAtlas = (mTexCoordAtlas[i].mX) + tIndexXOrig;
            int tIndexYAtlas = (mTexCoordAtlas[i].mY) + tIndexYOrig;
            int tFinalIndex = ((tIndexYAtlas * tTotalW) + tIndexXAtlas) * 4;
            if(tFinalIndex >= tTotalW * tTotalH * 4)
            {
                //fprintf(stderr, "Pos %i %i maps to %i %i and %i %i\n", tIndexXOrig, tIndexYOrig, tIndexXAtlas, tIndexYAtlas, tOriginalIndex, tFinalIndex);
            }
            else
            {
                //--Put the color information in.
                tAtlasData[tFinalIndex+0] =  pTextureData[i][tOriginalIndex+0];
                tAtlasData[tFinalIndex+1] =  pTextureData[i][tOriginalIndex+1];
                tAtlasData[tFinalIndex+2] =  pTextureData[i][tOriginalIndex+2];
                tAtlasData[tFinalIndex+3] =  pTextureData[i][tOriginalIndex+3];
                if(tAtlasData[tFinalIndex+3] < 192)
                    tAtlasData[tFinalIndex+3] = 0;
                else
                    tAtlasData[tFinalIndex+3] = 255;
            }
        }
    }

    //--Make the bitmap.
    DebugPrint("Creating bitmap.\n");
    StarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
    StarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
    StarBitmap::xStaticFlags.mUplWrapS = GL_CLAMP_TO_EDGE;
    StarBitmap::xStaticFlags.mUplWrapT = GL_CLAMP_TO_EDGE;
    mTextureAtlas = new StarBitmap(tAtlasData, tTotalW, tTotalH);

    //--Go through all the textures. Modify their texture coordinates to point to the atlas.
    DebugPrint("Modifying texture positions.\n");
    WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        //--Get the texture index and original properties.
        StarBitmap *rUseTexture = (StarBitmap *)mCompressedTextureTable->GetElementBySlot(rPolygon->mTemporaryTextureIndex);
        if(!rUseTexture)
        {
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Modify the texture coordinates.
        int p = rPolygon->mTemporaryTextureIndex;
        for(int i = 0; i < rPolygon->mPointsTotal; i ++)
        {
            //--Originals.
            int tOrigX = rPolygon->mTexCoords[i].mX * rUseTexture->GetWidth();
            int tOrigY = rPolygon->mTexCoords[i].mY * rUseTexture->GetHeight();

            //--Modify it to be the original texture coordinates system.
            rPolygon->mTexCoords[i].mX = (float)(mTexCoordAtlas[p].mX + tOrigX) / (float)tTotalW;
            rPolygon->mTexCoords[i].mY = (float)(mTexCoordAtlas[p].mY + tOrigY) / (float)tTotalH;
        }

        //--The use-texture is now the atlas.
        rPolygon->rTexture = mTextureAtlas;

        //--Next.
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Position report.
    for(int i = 0; i < mCompressedTextureTable->GetListSize(); i ++)
    {
        DebugPrint("%3i: %f %f\n", i, mTexCoordAtlas[i].mX, mTexCoordAtlas[i].mY);
    }

    //--Clean up.
    DebugPrint("Cleaning.\n");
    free(tAtlasData);
    StarBitmap::RestoreDefaultTextureFlags();

    //--Debug.
    DebugPop("WADFile:BuildTextureAtlas - Complete.\n");
}
void WADFile::BuildLightmapAtlas(bool pBuildWhitePixel)
{
    //--[Documentation]
    //--Builds an atlas of all the lightmaps present across all the faces. This texture can get pretty big.
    DebugPush(true, "WADFile:BuildLightmapAtlas - Begin.\n");

    //--[Debug]
    //--Builds a lightmap atlas with exactly one fixed pixel.
    if(pBuildWhitePixel)
    {
        //--Set the basic pixel.
        SetMemoryData(__FILE__, __LINE__);
        uint8_t *tData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * 4);
        tData[0] = 192;
        tData[1] = 192;
        tData[2] = 192;
        tData[3] = 255;

        //--Upload it.
        StarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
        StarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
        StarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
        StarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;
        mLightmapAtlas = new StarBitmap(tData, 1, 1);

        //--Set the lightmap positions for all the polygons.
        DebugPrint("Populating polygon lightmap data.\n");
        WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
        while(rPolygon)
        {
            for(int i = 0; i < rPolygon->mPointsTotal; i ++)
            {
                rPolygon->mLightmapCoords[i].mX = 0.5f;
                rPolygon->mLightmapCoords[i].mY = 0.5f;
            }
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
        }

        //--Clean.
        free(tData);
        return;
    }

    //--[Setup]
    //--Setup.
    int tTotalW = 0;
    int tTotalH = 0;
    int tCurMaxH = 0;
    int tRunningW = 0;
    int tRunningH = 0;

    //--Compute the maximum texture size for the lightmap. We can't go larger than the texture max size.
    int cMaxSizeX = 16384 - 2;
    if(cMaxSizeX > StarBitmap::xMaxTextureSize)
    {
        cMaxSizeX = StarBitmap::xMaxTextureSize - 2;
    }

    //--Max Sizes.
    int tLargestX = 0;
    int tLargestY = 0;
    int tSmallestX = cMaxSizeX;
    int tSmallestY = cMaxSizeX;

    //--[Get Max Atlas Size]
    //--Iterate.
    int i = 0;
    DebugPrint("Resolving required atlas size.\n");
    WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        //--Skip polys with no lightmap.
        if(!rPolygon->mLightmap)
        {
            i ++;
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Store.
        int tWid = rPolygon->mLightmap->GetWidth();
        int tHei = rPolygon->mLightmap->GetHeight();
        rPolygon->mLightmapLoadX = tRunningW+1;
        rPolygon->mLightmapLoadY = tRunningH+1;
        //DebugPrint(" %2i: %4i %4i, %4i\n", i, tWid, tHei, tTotalW);

        //--Comparisons.
        if(tLargestX < tWid) tLargestX = tWid;
        if(tLargestY < tHei) tLargestY = tHei;
        if(tSmallestX > tWid) tSmallestX = tWid;
        if(tSmallestY > tHei) tSmallestY = tHei;

        //--Add to the running width.
        tRunningW += rPolygon->mLightmap->GetWidth() + 2;
        if(tTotalW < cMaxSizeX) tTotalW = tRunningW;

        //--Edge check, go to the next line.
        if(tRunningW >= cMaxSizeX)
        {
            //--Place this lightmap on the far left edge. Move down a bit.
            tTotalW = cMaxSizeX + 2;
            tRunningW = rPolygon->mLightmap->GetWidth() + 2;
            tRunningH += tCurMaxH + 2;

            //--Store new values.
            rPolygon->mLightmapLoadX = 1;
            rPolygon->mLightmapLoadY = tRunningH+1;

            //--Reset flags.
            tCurMaxH = rPolygon->mLightmap->GetHeight() + 2;
        }
        else
        {
            if(tCurMaxH < rPolygon->mLightmap->GetHeight() + 2)
            {
                tCurMaxH = rPolygon->mLightmap->GetHeight() + 2;
            }
        }

        //--Next polygon.
        i ++;
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Apply this to the final result.
    tTotalH = tRunningH + tCurMaxH;

    //--Error check.
    if(tTotalW < 1 || tTotalH < 1)
    {
        DebugPop("WADFile:BuildLightmapAtlas - Error, illegal atlas sizes. %ix%i.\n", tTotalW, tTotalH);
        return;
    }

    //--Report.
    DebugPrint("Expected Atlas size: %ix%i\n", tTotalW, tTotalH);
    DebugPrint("Largest lightmap: %ix%i\n", tLargestX, tLargestY);
    DebugPrint("Smallest lightmap: %ix%i\n", tSmallestX, tSmallestY);

    //--[Allocate and Fill]
    //--We now know how big the texture needs to be. Allocate space.
    DebugPrint("Allocating memory for atlas of size %ix%i\n", tTotalW, tTotalH);
    int tFinalXSize = tTotalW;
    int tFinalYSize = tTotalH;
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tTexData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tFinalXSize * tFinalYSize * 4);
    memset(tTexData, 0, sizeof(sizeof(uint8_t) * tFinalXSize * tFinalYSize * 4));

    //--Populate.
    DebugPrint("Populating polygon lightmap data.\n");
    rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        //--Skip polys with no lightmap.
        if(!rPolygon->mLightmap)
        {
            rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Get variables.
        int tWid = rPolygon->mLightmap->GetWidth();
        int tHei = rPolygon->mLightmap->GetHeight();

        //--Set the final lightmap coordinates.
        for(int i = 0; i < rPolygon->mPointsTotal; i ++)
        {
            rPolygon->mLightmapCoords[i].mX = (float)(rPolygon->mLightmapLoadX + (rPolygon->mLightmapCoords[i].mX * tWid)) / tFinalXSize;
            rPolygon->mLightmapCoords[i].mY = (float)(rPolygon->mLightmapLoadY + (rPolygon->mLightmapCoords[i].mY * tHei)) / tFinalYSize;
        }

        //--The polygon stores its lightmap data for this. Copy it out.
        for(int x = 0; x < tWid; x ++)
        {
            for(int y = 0; y < tHei; y ++)
            {
                //--Calculate the indices.
                int tIndexXAtlas = rPolygon->mLightmapLoadX + x;
                int tIndexYAtlas = rPolygon->mLightmapLoadY + y;
                int tOriginalIndex = ((y * tWid) + x) * 4;
                int tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;

                //--Copy.
                memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);

                //--Left Edge
                if(x == 0)
                {
                    //--Base.
                    tIndexXAtlas = rPolygon->mLightmapLoadX + x - 1;
                    tIndexYAtlas = rPolygon->mLightmapLoadY + y;
                    tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                    memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);

                    //--Top left corner.
                    if(y == 0)
                    {
                        tIndexXAtlas = rPolygon->mLightmapLoadX + x - 1;
                        tIndexYAtlas = rPolygon->mLightmapLoadY + y - 1;
                        tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                        memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);
                    }
                    //--Bottom left corner.
                    if(y == tHei - 1)
                    {
                        tIndexXAtlas = rPolygon->mLightmapLoadX + x - 1;
                        tIndexYAtlas = rPolygon->mLightmapLoadY + y + 1;
                        tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                        memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);
                    }
                }

                //--Top Edge
                if(y == 0)
                {
                    tIndexXAtlas = rPolygon->mLightmapLoadX + x;
                    tIndexYAtlas = rPolygon->mLightmapLoadY + y - 1;
                    tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                    memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);
                }

                //--Right Edge
                if(x == tWid - 1)
                {
                    //--Base.
                    tIndexXAtlas = rPolygon->mLightmapLoadX + x + 1;
                    tIndexYAtlas = rPolygon->mLightmapLoadY + y;
                    tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                    memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);

                    //--Top right corner.
                    if(y == 0)
                    {
                        tIndexXAtlas = rPolygon->mLightmapLoadX + x + 1;
                        tIndexYAtlas = rPolygon->mLightmapLoadY + y - 1;
                        tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                        memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);
                    }
                    //--Bottom right corner.
                    if(y == tHei - 1)
                    {
                        tIndexXAtlas = rPolygon->mLightmapLoadX + x + 1;
                        tIndexYAtlas = rPolygon->mLightmapLoadY + y + 1;
                        tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                        memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);
                    }
                }

                //--Bottom Edge
                if(y == tHei - 1)
                {
                    tIndexXAtlas = rPolygon->mLightmapLoadX + x;
                    tIndexYAtlas = rPolygon->mLightmapLoadY + y + 1;
                    tFinalIndex = ((tIndexYAtlas * tFinalXSize) + tIndexXAtlas) * 4;
                    memcpy(&tTexData[tFinalIndex], &rPolygon->mLightmapData[tOriginalIndex], sizeof(uint8_t) * 4);
                }
            }
        }

        //--We don't need to store the lightmap data anymore.
        delete rPolygon->mLightmap;
        rPolygon->mLightmap = NULL;
        free(rPolygon->mLightmapData);
        rPolygon->mLightmapData = NULL;

        //--Next polygon.
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Upload.
    DebugPrint("Uploading atlas data.\n");
    StarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
    StarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
    StarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
    StarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;
    mLightmapAtlas = new StarBitmap(tTexData, tFinalXSize, tFinalYSize);

    //--[Clean Up]
    //--Clean up.
    DebugPrint("Cleaning up.\n");
    free(tTexData);
    StarBitmap::RestoreDefaultTextureFlags();

    //--Debug.
    DebugPop("WADFile:BuildLightmapAtlas - Complete.\n");
}
