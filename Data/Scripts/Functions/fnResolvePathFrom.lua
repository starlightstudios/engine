-- |[ =================================== fnResolvePathFrom() ================================== ]|
--Function that resolves the path of the given path, minus the name/extension at the end.
-- If a script is in C:/Dogs/Cats/Script.lua the result will be "C:/Dogs/Cats/"
function fnResolvePathFrom(psStartPath)

    -- |[Setup]|
	local iLen = string.len(psStartPath)

    -- |[Find Last Slash]|
	--Seek backwards until we find the last slash. It indicates the name of current file.
	local bFoundSlash = false
	local sLetter = ""
	for i = iLen, 1, -1 do
		sLetter = string.sub(psStartPath, i, i)
		if(sLetter == "/" or sLetter == "\\") then
			iLen = i
			bFoundSlash = true
			break
		end
	end

	--No slash found. We can skip a step, and just return "", indicating this is executing from the root.
	if(bFoundSlash == false) then
		return ""
	end

    -- |[Resolve Path]|
    --Pare off the filename from the path, and that's the result.
	local sFinishPath = ""
	for i = 1, iLen, 1 do
		sLetter = string.sub(psStartPath, i, i)
		sFinishPath = sFinishPath .. sLetter
	end

    --Return.
	return sFinishPath
end