//--Base
#include "MapManager.h"

//--Classes
#include "AbyssCombat.h"
#include "CarnationCombat.h"
#include "MonocerosDialogue.h"
#include "MonocerosMenu.h"
#include "CarnationMenu.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///--[Dialogue Classes]
//--These functions are used to change the dialogue mode.
void MapManager::ChangeWorldDialogueToWorldDialogue()
{
    //--Scraps and replaces the active world dialogue with a WorldDialogue class. Don't use when it's
    //  in use, obviously.
    //--The fresh class will need to run its Construct() code again.
    delete mWorldDialogue;
    mWorldDialogue = new WorldDialogue();
}

///--[Menu Generation]
WorldDialogue *MapManager::GenerateNewDialogue()
{
    if(mUseDialogueType == POINTER_TYPE_MONOCEROSDIALOGUE) return new MonocerosDialogue();
    return new WorldDialogue();
}
AdventureMenu *MapManager::GenerateNewMenu()
{
    ///--[Documentation and Setup]
    //--Subroutine that generates a new adventure menu handler. The program can change what kind of
    //  menu is in use for mods. By default, an AdventureMenu is used.
    if(mUseMenuType == POINTER_TYPE_MONOCEROSMENU) return new MonocerosMenu();
    if(mUseMenuType == POINTER_TYPE_CARNATIONMENU) return new CarnationMenu();
    return new AdventureMenu();
}
AdvCombat *MapManager::GenerateNewCombat()
{
    ///--[Documentation and Setup]
    //--Subroutine that generates a new adventure combat handler. The program can change what type of
    //  combat is in use, in order to change things like layout or update handling.
    //--By default, an AdvCombat is used.
    if(mUseCombatType == POINTER_TYPE_ABYSSCOMBAT) return new AbyssCombat();
    if(mUseCombatType == POINTER_TYPE_CARNATIONCOMBAT) return new CarnationCombat();
    return new AdvCombat();
}
