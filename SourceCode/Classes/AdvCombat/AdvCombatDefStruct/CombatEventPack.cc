//--Base
#include "AdvCombatDefStruct.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

//--Functions.
void CombatEventPack::Initialize()
{
    mPriority = 0;
    rOriginator = NULL;
    rAbility = NULL;
    mTargetCluster = NULL;
    mrAdditionalEntities = NULL;
}
void CombatEventPack::AddAdditionalEntity(AdvCombatEntity *pEntity)
{
    if(!mrAdditionalEntities) mrAdditionalEntities = new StarLinkedList(false);
    mrAdditionalEntities->AddElementAsTail("X", pEntity);
}
void CombatEventPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    CombatEventPack *rPtr = (CombatEventPack *)pPtr;
    TargetCluster::DeleteThis(rPtr->mTargetCluster);
    delete rPtr->mrAdditionalEntities;
    free(rPtr);
}
