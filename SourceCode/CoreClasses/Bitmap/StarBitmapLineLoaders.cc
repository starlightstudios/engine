//--Base
#include "StarBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

///======================================= Documentation ==========================================
//--Line-loaders are compressed by removing the transparent pixels from an image, and instead storing long
//  blocks of pixels with position info. This is similar to gif compression, but is lossless.

///========================================= Functions ============================================
uint8_t *StarBitmap::UncompressHorizontalData(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight)
{
    ///--[Documentation]
    //--Returns a heap-allocated array containing color data for a bitmap that was compressed using
    //  horizontal line compression.
    if(!pArray || sWidth < 1 || sHeight < 1) return NULL;

    ///--[Execution]
    //--Data array.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *nNewData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * sWidth * sHeight * 4);
    memset(nNewData, 0, sizeof(uint8_t) * sWidth * sHeight * 4);

    //--The array is actually a series indicating the start position and contents of pixels. We effectively 'draw' onto
    //  the array and then return it.
    int tDataCursor = 0;
    int tYCursor = sHeight - 1;
    while(tDataCursor < pDataSize)
    {
        //--First, read how many pixels we expect.
        uint16_t *rPixelCountPtr = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--If the value is zero, end the line.
        if(*rPixelCountPtr == 0)
        {
            tYCursor --;
            if(tYCursor < 0) break;
            continue;
        }

        //--If nonzero, that's how many pixels we expect. Next, read where the pixels start.
        uint16_t *rPixelStart = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--Translate them to proper ints so we don't waste time dereferencing.
        int tPixelCount = *rPixelCountPtr;
        int tPixelStart = *rPixelStart;

        //--Error check.
        if(tDataCursor >= pDataSize) break;

        //--Start drawing.
        for(int i = 0; i < tPixelCount; i ++)
        {
            int tArrayPos = ((tPixelStart + i) + (sWidth * tYCursor)) * 4;
            nNewData[tArrayPos+0] = pArray[tDataCursor+0];
            nNewData[tArrayPos+1] = pArray[tDataCursor+1];
            nNewData[tArrayPos+2] = pArray[tDataCursor+2];
            nNewData[tArrayPos+3] = pArray[tDataCursor+3];
            tDataCursor += (sizeof(uint8_t) * 4);
        }

        //--Error check.
        if(tDataCursor >= pDataSize) break;
    }

    //--If this flag is true, we need to re-composite this image to be padded by 2 rows/columns of transparent pixels.
    //  This is used for the ColorOutline shader and/or preventing edge pixel bleed for texture atlases.
    if(xPadSpriteForEdging)
    {
        //--Store old values.
        int tOldWid = sWidth;
        int tOldHei = sHeight;
        int tNewWid = sWidth + 4;
        int tNewHei = sHeight + 4;

        //--Allocate a new array.
        SetMemoryData(__FILE__, __LINE__);
        uint8_t *nPaddedData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tNewWid * tNewHei * 4);
        memset(nPaddedData, 0, sizeof(uint8_t) * tNewWid * tNewHei * 4);

        //--Copy the data across, indented by 2 in both directions.
        for(int x = 0; x < tOldWid; x ++)
        {
            for(int y = 0; y < tOldHei; y ++)
            {
                //--Compute array position.
                int tOldArrayPos = (( y    * tOldWid) +  x)    * 4;
                int tNewArrayPos = (((y+2) * tNewWid) + (x+2)) * 4;

                //--Copy over.
                nPaddedData[tNewArrayPos+0] = nNewData[tOldArrayPos+0];
                nPaddedData[tNewArrayPos+1] = nNewData[tOldArrayPos+1];
                nPaddedData[tNewArrayPos+2] = nNewData[tOldArrayPos+2];
                nPaddedData[tNewArrayPos+3] = nNewData[tOldArrayPos+3];
            }
        }

        //--Store size changes.
        sWidth = tNewWid;
        sHeight = tNewHei;

        //--Deallocate the old data, and replace it.
        free(nNewData);
        nNewData = nPaddedData;
    }

    //--Pass back the data.
    return nNewData;
}
uint8_t *StarBitmap::UncompressVerticalData(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight)
{
    ///--[Documentation]
    //--Returns a heap-allocated array containing color data for a bitmap that was compressed using
    //  vertical line compression.
    if(!pArray || sWidth < 1 || sHeight < 1) return NULL;

    ///--[Execution]
    //--Data array.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *nNewData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * sWidth * sHeight * 4);
    memset(nNewData, 0, sizeof(uint8_t) * sWidth * sHeight * 4);

    //--The array is actually a series indicating the start position and contents of pixels. We effectively 'draw' onto
    //  the array and then return it.
    int tDataCursor = 0;
    int tXCursor = 0;
    while(tDataCursor < pDataSize)
    {
        //--First, read how many pixels we expect.
        uint16_t *rPixelCountPtr = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--If the value is zero, end the line.
        if(*rPixelCountPtr == 0)
        {
            tXCursor ++;
            if(tXCursor >= sWidth) break;
            continue;
        }

        //--If nonzero, that's how many pixels we expect. Next, read where the pixels start.
        uint16_t *rPixelStart = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--Translate them to proper ints so we don't waste time dereferencing.
        int tPixelCount = *rPixelCountPtr;
        int tPixelStart = *rPixelStart;

        //--Error check.
        if(tDataCursor >= pDataSize) break;

        //--Start drawing.
        for(int i = 0; i < tPixelCount; i ++)
        {
            int tArrayPos = ((tXCursor) + (sWidth * (sHeight - (tPixelStart + i) - 1) )) * 4;
            nNewData[tArrayPos+0] = pArray[tDataCursor+0];
            nNewData[tArrayPos+1] = pArray[tDataCursor+1];
            nNewData[tArrayPos+2] = pArray[tDataCursor+2];
            nNewData[tArrayPos+3] = pArray[tDataCursor+3];
            tDataCursor += (sizeof(uint8_t) * 4);
        }

        //--Error check.
        if(tDataCursor >= pDataSize) break;
    }

    //--If this flag is true, we need to re-composite this image to be padded by 2 rows/columns of transparent pixels.
    //  This is used for the ColorOutline shader and/or preventing edge pixel bleed for texture atlases.
    if(xPadSpriteForEdging)
    {
        //--Store old values.
        int tOldWid = sWidth;
        int tOldHei = sHeight;
        int tNewWid = sWidth + 4;
        int tNewHei = sHeight + 4;

        //--Allocate a new array.
        SetMemoryData(__FILE__, __LINE__);
        uint8_t *nPaddedData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tNewWid * tNewHei * 4);
        memset(nPaddedData, 0, sizeof(uint8_t) * tNewWid * tNewHei * 4);

        //--Copy the data across, indented by 2 in both directions.
        for(int x = 0; x < tOldWid; x ++)
        {
            for(int y = 0; y < tOldHei; y ++)
            {
                //--Compute array position.
                int tOldArrayPos = (( y    * tOldWid) +  x)    * 4;
                int tNewArrayPos = (((y+2) * tNewWid) + (x+2)) * 4;

                //--Copy over.
                nPaddedData[tNewArrayPos+0] = nNewData[tOldArrayPos+0];
                nPaddedData[tNewArrayPos+1] = nNewData[tOldArrayPos+1];
                nPaddedData[tNewArrayPos+2] = nNewData[tOldArrayPos+2];
                nPaddedData[tNewArrayPos+3] = nNewData[tOldArrayPos+3];
            }
        }

        //--Store size changes.
        sWidth = tNewWid;
        sHeight = tNewHei;

        //--Deallocate the old data, and replace it.
        free(nNewData);
        nNewData = nPaddedData;
    }

    //--Pass back the data.
    return nNewData;
}
void StarBitmap::UncompressHorizontalDataIntoAtlas(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight)
{
    ///--[Documentation]
    //--Same as above, except decompresses the data directly into the atlas array.
    if(!pArray || sWidth < 1 || sHeight < 1) return;

    //--The array is actually a series indicating the start position and contents of pixels. We effectively 'draw' onto
    //  the array and then return it.
    int tDataCursor = 0;
    int tYCursor = sHeight - 1;
    while(tDataCursor < pDataSize)
    {
        //--First, read how many pixels we expect.
        uint16_t *rPixelCountPtr = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--If the value is zero, end the line.
        if(*rPixelCountPtr == 0)
        {
            tYCursor --;
            if(tYCursor < 0) break;
            continue;
        }

        //--If nonzero, that's how many pixels we expect. Next, read where the pixels start.
        uint16_t *rPixelStart = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--Translate them to proper ints so we don't waste time dereferencing.
        int tPixelCount = *rPixelCountPtr;
        int tPixelStart = *rPixelStart;

        //--Error check.
        if(tDataCursor >= pDataSize) break;

        //--Start drawing.
        for(int i = 0; i < tPixelCount; i ++)
        {
            int tArrayPos = ((xAtlasCurrentX + tPixelStart + i) + ((int)xAtlasMaxSize * (tYCursor + xAtlasCurrentY))) * 4;
            xAtlasData[tArrayPos+0] = pArray[tDataCursor+0];
            xAtlasData[tArrayPos+1] = pArray[tDataCursor+1];
            xAtlasData[tArrayPos+2] = pArray[tDataCursor+2];
            xAtlasData[tArrayPos+3] = pArray[tDataCursor+3];
            tDataCursor += (sizeof(uint8_t) * 4);
        }

        //--Error check.
        if(tDataCursor >= pDataSize) break;
    }
}
void StarBitmap::UncompressVerticalDataIntoAtlas(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight)
{
    ///--[Documentation]
    //--Same as above, except decompresses the data directly into the atlas array.
    if(!pArray || sWidth < 1 || sHeight < 1) return;

    //--The array is actually a series indicating the start position and contents of pixels. We effectively 'draw' onto
    //  the array and then return it.
    int tDataCursor = 0;
    int tXCursor = 0;
    while(tDataCursor < pDataSize)
    {
        //--First, read how many pixels we expect.
        uint16_t *rPixelCountPtr = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--If the value is zero, end the line.
        if(*rPixelCountPtr == 0)
        {
            tXCursor ++;
            if(tXCursor >= sWidth) break;
            continue;
        }

        //--If nonzero, that's how many pixels we expect. Next, read where the pixels start.
        uint16_t *rPixelStart = (uint16_t *)&pArray[tDataCursor];
        tDataCursor += (sizeof(uint16_t));

        //--Translate them to proper ints so we don't waste time dereferencing.
        int tPixelCount = *rPixelCountPtr;
        int tPixelStart = *rPixelStart;

        //--Error check.
        if(tDataCursor >= pDataSize) break;

        //--Start drawing.
        for(int i = 0; i < tPixelCount; i ++)
        {
            int tRealY = (sHeight - (tPixelStart + i) - 1);
            int tArrayPos = ((xAtlasCurrentX + tXCursor) + ((int)xAtlasMaxSize * (xAtlasCurrentY + tRealY))) * 4;
            xAtlasData[tArrayPos+0] = pArray[tDataCursor+0];
            xAtlasData[tArrayPos+1] = pArray[tDataCursor+1];
            xAtlasData[tArrayPos+2] = pArray[tDataCursor+2];
            xAtlasData[tArrayPos+3] = pArray[tDataCursor+3];
            tDataCursor += (sizeof(uint8_t) * 4);
        }

        //--Error check.
        if(tDataCursor >= pDataSize) break;
    }
}
