//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdventureItem.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
void AdvCombatEntity::CreateEquipmentSlot(const char *pName)
{
    //--Creates and registers a new equipment slot with the given name. Does not allow duplicates.
    if(!pName) return;
    if(mEquipmentSlotList->GetElementByName(pName)) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    EquipmentSlotPack *nPack = (EquipmentSlotPack *)starmemoryalloc(sizeof(EquipmentSlotPack));
    nPack->Initialize();
    mEquipmentSlotList->AddElementAsTail(pName, nPack, &EquipmentSlotPack::DeleteThis);

    //--Display name. By default, display name is identical to the registration name, but a translation
    //  may modify this if it is active.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_UI);
    if(!rTranslation)
    {
        nPack->mDisplayName = InitializeString(pName);
    }
    else
    {
        //--If a translation exists, use it, otherwise use the default name.
        const char *rTranslatedString = rTranslation->GetTranslationFor(pName);
        if(!rTranslatedString)
        {
            nPack->mDisplayName = InitializeString(pName);
        }
        else
        {
            nPack->mDisplayName = InitializeString(rTranslatedString);
        }
    }
}

///===================================== Property Queries =========================================
int AdvCombatEntity::GetEquipmentSlotsTotal()
{
    return mEquipmentSlotList->GetListSize();
}
const char *AdvCombatEntity::GetDisplayNameOfEquipmentSlot(int pSlot)
{
    //--Returns the display name of the equipment slot. If a translation is active, then this name
    //  may not the be the slot's internal name. If no translation is active, the two are the same.
    //--Get the entry, if it exists.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(pSlot);
    if(!rPackage) return NULL;

    //--Return the display name, if it exists.
    if(rPackage->mDisplayName) return rPackage->mDisplayName;

    //--If the display name doesn't exist, return the literal slot name.
    return mEquipmentSlotList->GetNameOfElementBySlot(pSlot);
}
const char *AdvCombatEntity::GetNameOfEquipmentSlot(int pSlot)
{
    //--Returns the exact name of the equipment slot, disallowing translations.
    return mEquipmentSlotList->GetNameOfElementBySlot(pSlot);
}
bool AdvCombatEntity::IsWeaponAlternateSlot(int pSlot)
{
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(pSlot);
    if(!rPackage) return false;
    return rPackage->mIsWeaponAlternate;
}
int AdvCombatEntity::GetSlotOfEquipmentByName(const char *pName)
{
    return mEquipmentSlotList->GetSlotOfElementByName(pName);
}
int AdvCombatEntity::GetSlotForWeaponDamage()
{
    return mSlotForWeaponDamage;
}
AdventureItem *AdvCombatEntity::GetEquipmentBySlotS(const char *pEquipSlot)
{
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pEquipSlot);
    if(!rPackage) return NULL;
    return rPackage->mEquippedItem;
}
AdventureItem *AdvCombatEntity::GetEquipmentBySlotI(int pEquipSlot)
{
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(pEquipSlot);
    if(!rPackage) return NULL;
    return rPackage->mEquippedItem;
}
AdventureItem *AdvCombatEntity::GetWeapon()
{
    //--Note: Can return NULL.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(mSlotForWeaponDamage);
    if(!rPackage) return NULL;
    return rPackage->mEquippedItem;
}
EquipmentSlotPack *AdvCombatEntity::GetEquipmentSlotPackageI(int pIndex)
{
    return (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(pIndex);
}
EquipmentSlotPack *AdvCombatEntity::GetEquipmentSlotPackageS(const char *pEquipSlot)
{
    return (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pEquipSlot);
}

///======================================== Manipulators ==========================================
void AdvCombatEntity::SetEquipmentSlotDisplayName(const char *pName, const char *pDisplayName)
{
    ///--[Documentation]
    //--Modifies the display name of the equipment slot, which can be used for translations or other
    //  nefarious purposes. If the display name is set to NULL or "Null" or "$Default", then then the
    //  normal name will be used instead.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(!rPackage) return;

    //--Display name is a resettable.
    if(!pDisplayName || !strcasecmp(pDisplayName, "Null") || !strcasecmp(pDisplayName, "$Default"))
    {
        ResetString(rPackage->mDisplayName, NULL);
    }
    //--Normal case.
    else
    {
        ResetString(rPackage->mDisplayName, pDisplayName);
    }
}
void AdvCombatEntity::SetEquipmentSlotIsUsedForStats(const char *pName, bool pFlag)
{
    ///--[Documentation]
    //--Default is true. If set to false, items equipped in this slot will not affect a character's
    //  stats, and will be cosmetic.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(rPackage) rPackage->mIsComputedForStats = pFlag;
}
void AdvCombatEntity::SetEquipmentSlotIsUsedForTags(const char *pName, bool pFlag)
{
    ///--[Documentation]
    //--Default is true. If set to false, items equipped in this slot will not affect a character's
    //  tags. It is possible to split statistics and tags between slots.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(rPackage) rPackage->mIsComputedForTags = pFlag;
}
void AdvCombatEntity::SetEquipmentSlotCanBeEmpty(const char *pName, bool pFlag)
{
    ///--[Documentation]
    //--Some slots, particularly weapons, aren't allowed to be empty. This flag allows a slot to
    //  be unequipped or not populated.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(rPackage) rPackage->mCanBeEmpty = pFlag;
}
void AdvCombatEntity::SetEquipmentSlotIsAltWeapon(const char *pName, bool pFlag)
{
    ///--[Documentation]
    //--AltWeapons are slots that can be swapped in combat with the use of a combat ability. This
    //  means gems can be equipped and it can be used to compute stats, but only if the player
    //  actually switches to that weapon in combat.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(rPackage) rPackage->mIsWeaponAlternate = pFlag;
}
void AdvCombatEntity::SetEquipmentSlotNoGems(const char *pName, bool pFlag)
{
    ///--[Documentation]
    //--If a slot cannot have gems in it, but can have equipment placed in it which might have gems,
    //  this flag will prevent the gems from appearing in the UI.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(rPackage) rPackage->mNoGems = pFlag;
}
bool AdvCombatEntity::EquipItemToSlot(const char *pSlotName, AdventureItem *pItem)
{
    ///--[Documentation]
    //--Equips the item to the given slot, and returns true if the item was taken ownership of. Returns
    //  false if this entity did *not* take ownership for any reason.
    //--If the slot is already occupied, the item previously held is moved to the inventory.
    if(!pSlotName || !pItem) return false;

    ///--[Special Case: Dummy item]
    //--Special: If the slot name is "ALL" and the item is the dummy item, unequip all items.
    if(!strcasecmp(pSlotName, "All") && pItem == AdventureInventory::xrDummyUnequipItem)
    {
        //--Iterate across all packages.
        EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
        while(rPackage)
        {
            //--Send the item to the inventory.
            if(rPackage->mEquippedItem)
                AdventureInventory::Fetch()->RegisterItem(rPackage->mEquippedItem);

            //--Clear slot.
            rPackage->mEquippedItem = NULL;

            //--Next.
            rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
        }

        //--Compute changes in stats.
        ComputeStatistics();
        return false;
    }

    ///--[Error Check]
    //--Make sure the equipment slot exists.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pSlotName);
    if(!rPackage) return false;

    ///--[Swap Case]
    //--If the slot is already occupied, unequip that item and send it to the inventory.
    AdventureItem *rGemSwapItem = NULL;
    if(rPackage->mEquippedItem)
    {
        AdventureInventory::Fetch()->RegisterItem(rPackage->mEquippedItem);
        rGemSwapItem = rPackage->mEquippedItem;
        rPackage->mEquippedItem = NULL;
    }

    ///--[Unequip]
    //--If the item was the dummy-unequip item, set the slot to NULL. We don't take ownership of that.
    if(pItem == AdventureInventory::xrDummyUnequipItem)
    {
        rPackage->mEquippedItem = NULL;
        ComputeStatistics();
        return false;
    }

    ///--[Equip]
    //--Now equip the item and inform the caller we took ownership.
    rPackage->mEquippedItem = pItem;

    ///--[Gem Transfer]
    //--If there is an rGemSwapItem, take all the gems out of it and put them in the new item. If there are
    //  not enough gem slots, leave them in the item, as the gem slots get cleared later.
    if(rGemSwapItem && pItem->GetGemSlots() > 0)
    {
        //--Debug.
        //fprintf(stderr, "Transferring gems.\n");
        //fprintf(stderr, " Gem slots for new item: %i\n", pItem->GetGemSlots());
        //fprintf(stderr, " Gem slots for old item: %i\n", rGemSwapItem->GetGemSlots());

        //--Setup.
        int tSlotsRegistered = 0;

        //--Iterate across old gem slots.
        for(int i = 0; i < rGemSwapItem->GetGemSlots(); i ++)
        {
            //--Get the gem in this slot.
            AdventureItem *rPrevGem = rGemSwapItem->RemoveGemFromSlot(i);
            if(!rPrevGem) continue;

            //--Place the gem in the next available slot.
            pItem->PlaceGemInSlot(tSlotsRegistered, rPrevGem);
            tSlotsRegistered ++;

            //--Check if there are any more slots available.
            if(tSlotsRegistered >= pItem->GetGemSlots()) break;
        }

        //--Debug.
        //fprintf(stderr, " Transferred %i gems total.\n", tSlotsRegistered);
    }

    //--Recompute character statistics.
    ComputeStatistics();
    return true;
}
bool AdvCombatEntity::EquipItemToSlotNoGems(const char *pSlotName, AdventureItem *pItem)
{
    ///--[Documentation]
    //--Same as the above routine, but does not swap gems between the items. This is used in games
    //  where gems cannot be freely switched, such as Carnation.
    if(!pSlotName || !pItem) return false;

    ///--[Special Case: Dummy item]
    //--Special: If the slot name is "ALL" and the item is the dummy item, unequip all items.
    if(!strcasecmp(pSlotName, "All") && pItem == AdventureInventory::xrDummyUnequipItem)
    {
        //--Iterate across all packages.
        EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
        while(rPackage)
        {
            //--Send the item to the inventory.
            if(rPackage->mEquippedItem)
                AdventureInventory::Fetch()->RegisterItem(rPackage->mEquippedItem);

            //--Clear slot.
            rPackage->mEquippedItem = NULL;

            //--Next.
            rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
        }

        //--Compute changes in stats.
        ComputeStatistics();
        return false;
    }

    ///--[Error Check]
    //--Make sure the equipment slot exists.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pSlotName);
    if(!rPackage) return false;

    ///--[Swap Case]
    //--If the slot is already occupied, unequip that item and send it to the inventory.
    //AdventureItem *rGemSwapItem = NULL;
    if(rPackage->mEquippedItem)
    {
        AdventureInventory::Fetch()->RegisterItem(rPackage->mEquippedItem);
        //rGemSwapItem = rPackage->mEquippedItem;
        rPackage->mEquippedItem = NULL;
    }

    ///--[Unequip]
    //--If the item was the dummy-unequip item, set the slot to NULL. We don't take ownership of that.
    if(pItem == AdventureInventory::xrDummyUnequipItem)
    {
        rPackage->mEquippedItem = NULL;
        ComputeStatistics();
        return false;
    }

    ///--[Equip]
    //--Now equip the item and inform the caller we took ownership.
    rPackage->mEquippedItem = pItem;

    //--Statistics.
    ComputeStatistics();
    return true;
}
void AdvCombatEntity::SetSlotUsedForWeaponDamage(int pSlot)
{
    mSlotForWeaponDamage = pSlot;
    mSlotOfOriginalWeapon = pSlot;
}

///======================================== Core Methods ==========================================
void AdvCombatEntity::ComputeEquipmentStatistics(CombatStatistics &sStatisticsPack)
{
    //--Iterate across all equipment and add the bonuses to the stats pack.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
    while(rPackage)
    {
        //--Slot is empty, or is not used for stat computations. Ignore it.
        if(!rPackage->mEquippedItem || !rPackage->mIsComputedForStats)
        {
            rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
            continue;
        }

        ///--ADDITION!!!

        //--Next.
        rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
    }
}
void AdvCombatEntity::SwapEquipmentSlots(const char *pSlotA, const char *pSlotB)
{
    //--Switches the equipment in the two given slots. Doesn't check if the slots are compatible, just
    //  switches the equipment. Yeah!
    //--Also tracks the index for weapon reversion.
    if(!pSlotA || !pSlotB) return;
    int tSlotA = mEquipmentSlotList->GetSlotOfElementByName(pSlotA);
    int tSlotB = mEquipmentSlotList->GetSlotOfElementByName(pSlotB);
    if(tSlotA == -1 || tSlotB == -1) return;

    //--Get the entries in the slots.
    EquipmentSlotPack *rItemPackA = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pSlotA);
    EquipmentSlotPack *rItemPackB = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pSlotB);

    //--Switch the equipment.
    AdventureItem *rTempPtr = rItemPackA->mEquippedItem;
    rItemPackA->mEquippedItem = rItemPackB->mEquippedItem;
    rItemPackB->mEquippedItem = rTempPtr;

    //--Tracking: If the A slot matched the weapon slot, move it.
    if(mSlotOfOriginalWeapon == tSlotA)
    {
        mSlotOfOriginalWeapon = tSlotB;
    }
    //--If the B slot was the weapon:
    else if(mSlotOfOriginalWeapon == tSlotB)
    {
        mSlotOfOriginalWeapon = tSlotA;
    }

    //--Recompute stats since they probably changed.
    ComputeStatistics();
}
void AdvCombatEntity::RemoveGemFromEquipment(AdventureItem *pGem)
{
    ///--[Documentation]
    //--Given a gem, scans all equipment this character has and removes it from any item it may be equipped in.
    //  Deliberately does not deallocate the gem. The gem will also not be sent to the inventory.
    if(!pGem) return;

    //--Scan slots.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
    while(rPackage)
    {
        //--Slot is empty. Ignore it.
        if(!rPackage->mEquippedItem)
        {
            rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
            continue;
        }

        //--Scan the item.
        AdventureItem *rItem = rPackage->mEquippedItem;
        int tSlots = rItem->GetGemSlots();
        for(int i = 0; i < tSlots; i ++)
        {
            //--Skip empties or non-matches.
            AdventureItem *rGemInSlot = rItem->GetGemInSlot(i);
            if(!rGemInSlot || rGemInSlot != pGem) continue;

            //--Match. Remove this gem.
            rItem->RemoveGemFromSlot(i);
        }

        //--Next.
        rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
    }
}
