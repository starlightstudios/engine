///--[AdvCombatEntity]
//--Represents an entity in combat, either party member or enemy.

///--[Statistics Information]
//--The AdvCombatEntity has many sets of statistics. A description of the types follows:
//--mBaseStatistics
//  Base statistics for the character that never change. These are not influenced by levels.
//  For enemies, these are the statistics they use as enemies never change jobs or use equipment.
//--mJobStatistics
//  Stat bonuses applied from the character's current job. Can change in combat.
//--mEquipmentStatistics
//  Stat bonuses applied from the character's equipment. Changes when they change equipment. Includes
//  all gem bonuses.
//--mScriptStatistics
//  Bonuses as applied by scripts, used for plot events. Under normal circumstances, these are zeroes.
//--mPermaEffectStatistics
//  Bonuses applied by effects that are permanent in some way. For example, some jobs have passives
//  that apply on top of their base job statistics, or effects that are applied by abilities equipped.
//--mTemporaryEffectStatistics
//  Bonuses from effects that are currently applied. These expire at the end of combat.
//--mSummedStatistics
//  All stats added together. This is the set that is queried in combat.

///--[Stunning Information]
//--If an entity is stunnable, they have a stun value. The standard stun value is 100, but this is set
//  as a statistic and can be changed by Effects, Equipment, etc.
//--When an entity receives stun damage, at the start of their action they check the stun value. If it
//  is less than their stun cap, they decrement it by (StunCap / ADVCE_STUN_DECREMENT_DIVISOR).
//--If it is greater than or equal to the stun cap, they decrement it by the stun cap and pass their turn.
//--When stunned, the stun resist increases by 1. The max stun resist is 3. The stun damage taken is checked
//  against the matching ADVCE_STUN_RESIST_FACTORX, which reduces it.
//--Every ADVCE_STUN_TURNS_TO_DECREMENT, the stun resist decreases by 1, minimum 0. The counter resets when
//  the entity is stunned.
//--The maximum amount of stun that can be accumulated at once is (StunCap * ADVCE_STUN_CAP_FROM_BASE_FACTOR).
//  Stun damage over this amount is lost.
//--Important: All stun handling is script driven. AIs can choose to ignore stun. Player scripts or effects
//  may cause stun to be ignored. Stun will continue to accumulate even if the entity is immune.

#pragma once

///--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "AdvCombatDefStruct.h"

///--[Local Structures]
///--[Local Definitions]
//--Statistic Groupings
#define ADVCE_STATS_BASE 0
#define ADVCE_STATS_JOB 1
#define ADVCE_STATS_EQUIPMENT 2
#define ADVCE_STATS_SCRIPT 3
#define ADVCE_STATS_PERMAEFFECT 4
#define ADVCE_STATS_TEMPEFFECT 5
#define ADVCE_STATS_FINAL 6
#define ADVCE_STATS_TOTAL 7

//--Easing
#define ADVCE_EASING_TICKS 15
#define ADVCE_CROSSFADE_TICKS 15

//--Flashing
#define ADVCE_FLASH_WHITE_TICKS 15
#define ADVCE_FLASH_BLACK_TICKS 15
#define ADVCE_FLASH_BLACK_SEGMENT 5

//--Knockout
#define ADVCE_KNOCKOUT_TOWHITE_TICKS 20
#define ADVCE_KNOCKOUT_TOBLACK_TICKS 20

//--Vendor Sprites
#define ADVCE_VENDOR_SPRITES_TOTAL 4

//--Stun
#define ADVCE_STUN_CAP_FROM_BASE_FACTOR 2
#define ADVCE_STUN_TURNS_TO_DECREMENT 3
#define ADVCE_STUN_DECREMENT_DIVISOR 5

//--Shake
#define ADVCE_SHAKE_SEVERITY 11
#define ADVCE_SHAKE_PERIODICITY 3
#define ADVCE_SHAKE_TICKS 15

//--Position
#define ADVCE_NORENDER -500.0f

///--[Classes]
class AdvCombatEntity : public RootObject
{
    protected:
    //--System
    bool mHasPositionedInBattle;
    int mLastCalloutTimer;
    char *mLocalName;
    char *mDisplayName;
    char *mClusterName;
    char *mResponseScriptPath;
    char *mAnimationScriptPath;
    bool mScriptResponses[ACE_SCRIPT_CODE_TOTAL];

    //--Carnation
    int mCommandTimer;

    //--Flags
    bool mCannotBeKnockedOut;
    bool mRemoveFromPartyOnKnockout;

    //--Combat Inspector
    int mInspectorShowStats;
    int mInspectorShowResists;
    int mInspectorShowAbilities;

    //--External
    char *mParagonGrouping;

    //--Persistent Statistics
    bool mHasPartingShot;
    bool mIsKnockedOut;
    int mTurnKOd;
    int mHP;
    int mInfluence;
    int mInfluenceMax;
    int mMP;
    int mCP;
    int mShield;
    int mAdrenaline;
    int mFreeActions;
    int mFreeActionsPerformed;
    EasingPack1D mHPEasingPack;
    EasingPack1D mMPEasingPack;
    EasingPack1D mAdrenalineEasingPack;
    EasingPack1D mShieldEasingPack;

    //--Memory Cursor
    int mMemoryCardX;
    int mMemoryJobX;
    int mMemoryJobY;
    int mMemoryMemorizedX;
    int mMemoryMemorizedY;
    int mMemoryTacticsX;
    int mMemoryTacticsY;

    //--Stun Handling
    bool mIsStunnable;
    int mStunValue;
    int mStunResist;
    int mStunResistDecrementTurns;
    EasingPack1D mStunEasingPack;

    //--AI
    bool mIsAmbushed;
    char *mAIScriptPath;

    //--Combat Position
    bool mHasBeenAssignedPosition;
    bool mIgnoreMovementForBlocking;
    float mCombatIdealX;
    float mCombatIdealY;
    EasingPack2D mCombatPosition;

    //--Shaking
    int mShakeTimer;
    float mShakeX;

    //--Turn Order
    int mTurnID;
    int mCurrentInitiative;

    //--Statistics
    CombatStatistics mBaseStatistics;
    CombatStatistics mJobStatistics;
    CombatStatistics mEquipmentStatistics;
    CombatStatistics mScriptStatistics;
    CombatStatistics mPermaEffectStatistics;
    CombatStatistics mTemporaryEffectStatistics;
    CombatStatistics mSummedStatistics;

    //--Levels
    int mLevel;
    int mTotalXP;
    int mLevelForMugAutoWin;

    //--Jobs
    int mGlobalJP;
    AdvCombatJob *rActiveJobAtCombatStart;
    AdvCombatJob *rActiveJob;
    StarLinkedList *mJobList; //AdvCombatJob *, master
    StarLinkedList *mrJobShowList; //AdvCombatJob *, reference

    //--Rewards
    int mRewardXP;
    int mRewardJP;
    int mRewardPlatina;
    int mRewardDoctor;
    StarLinkedList *mRewardItems; //const char *, master

    //--Reinforcement Handling
    int mReinforcementTurns;

    //--Ability Listing
    StarLinkedList *mAbilityList; //AdvCombatAbility *, master
    int mAbilityProbabilityGrid[ACE_ABILITY_GRID_SIZE_X][ACE_ABILITY_GRID_SIZE_Y];
    AdvCombatAbility *rAbilityGrid[ACE_ABILITY_GRID_SIZE_X][ACE_ABILITY_GRID_SIZE_Y];
    AdvCombatAbility *rSecondaryGrid[ACE_ABILITY_GRID_SIZE_X][ACE_ABILITY_GRID_SIZE_Y];
    StarLinkedList *mrPassiveAbilityList; //AdvCombatAbility *, ref

    //--Ability Profiles
    AbilityProfile *rActiveProfile;
    StarLinkedList *mProfilesList; //AbilityProfile *, master

    //--Equipment Listing
    int mSlotForWeaponDamage;
    int mSlotOfOriginalWeapon;
    StarLinkedList *mEquipmentSlotList; //EquipmentSlotPack *, master

    //--UI Positions
    TwoDimensionRealPoint mUIPositions[ACE_UI_INDEX_TOTAL];
    TwoDimensionReal mFaceTableDim;

    //--Effect Handling
    StarLinkedList *mrEffectsToShowThisTick; //AdvCombatEffect *, ref

    //--Tags
    StarLinkedList *mTagList; //int *, master

    //--TF Indicators. Used in AbyssCombat.
    int mTFIndicatorsTotal;
    StarBitmap *rTFIndicatorImg;

    //--Images
    bool mIsFlashingWhite;
    int mWhiteFlashTimer;
    bool mIsFlashingBlack;
    int mBlackFlashTimer;
    bool mIsKnockingOut;
    int mKnockoutTimer;
    bool mShouldSetBlockRenderForKO;
    bool mNewBlockRenderFlag;
    bool mBlockRenderForKO;
    float mOverrideY;
    float mOverrideCombatY;
    int mFadeInTimer;
    int mFadeInTimerMax;
    StarBitmap *rCombatImage;
    StarBitmap *rCombatCountermask;
    StarBitmap *rVictoryCountermask;
    StarBitmap *rTurnIcon;
    StarBitmap *rVendorImages[ADVCE_VENDOR_SPRITES_TOTAL];
    StarBitmap *rFaceTableImg;

    //--Image Crossfade
    int mCrossfadeTimer;
    StarBitmap *rCrossfadeImg;

    protected:

    public:
    //--System
    AdvCombatEntity();
    virtual ~AdvCombatEntity();
    void Reinitialize();

    //--Public Variables
    bool mLeveledUpThisBattle;
    int mLevelLastTick;
    float mLastRenderX;
    float mLastRenderY;

    //--Public Static Variables
    static int xMaxLevel;
    static int *xExpTable;

    //--Property Queries
    virtual bool IsOfType(int pType);
    bool HasPositionedInBattle();
    const char *GetName();
    const char *GetDisplayName();
    const char *GetClusterName();
    const char *GetResponsePath();
    const char *GetAIPath();
    int GetInspectorShowStats();
    int GetInspectorShowResists();
    int GetInspectorShowAbilities();
    bool IsAmbushed();
    bool RespondsToCode(int pCode);
    bool IsStunnable();
    bool IsStunnedThisTurn();
    int GetDisplayStun();
    bool IsDisplayHPRunning();
    bool IsDisplayMPRunning();
    float GetDisplayHPPct();
    float GetDisplayMPPct();
    float GetDisplayAdrenaline();
    float GetDisplayShield();
    int GetReinforcementTurns();
    StarBitmap *GetFaceProperties(TwoDimensionReal &sDimensions);
    bool IsMoving();
    float GetCombatX();
    float GetCombatY();
    float GetOverrideCombatY();
    TwoDimensionRealPoint GetUIRenderPosition(int pIndex);
    bool CanActThisTurn();
    bool IsDefeated();
    bool IsAnimating();
    int GetCurrentInitiative();
    int ComputeTurnBucket();
    int GetTurnID();
    int GetWhiteFlashTimer();
    bool IsFlashingBlack();
    virtual bool IsKnockingOut();
    bool IsKnockoutFinished();
    bool IsBlockRenderForKO();
    bool IsRemovedOnKO();
    int GetKnockoutTimer();
    int GetGlobalJP();
    int GetTotalJP();
    int GetFreeActions();
    int GetFreeActionsPerformed();
    int GetRewardXP();
    int GetRewardJP();
    int GetRewardPlatina();
    int GetRewardDoctor();
    int GetTurnKOd();
    StarLinkedList *GetRewardList();
    int GetEffectsWithTag(const char *pTag);
    int GetEffectsWithTagPartial(const char *pTag);
    int GetEffectIDWithTag(const char *pTag, int pSlot);
    int GetEffectIDWithTagPartial(const char *pTag, int pSlot);
    int GetBaseTagCount(const char *pTag);
    const char *GetFirstTagNameMatching(const char *pTag);
    int GetTagCount(const char *pTag);
    const char *GetParagonGrouping();
    float GetOverrideY();
    int GetMemoryCardX();
    int GetMemoryJobX();
    int GetMemoryJobY();
    int GetMemoryMemorizedX();
    int GetMemoryMemorizedY();
    int GetMemoryTacticsX();
    int GetMemoryTacticsY();
    int GetLevelForMugAutoWin();
    int GetLastCalloutTime();
    int GetCrossfadeTimer();
    int GetTFIndicatorCount();
    StarBitmap *GetTFIndicatorImg();
    bool HasBeenAssignedPosition();
    float GetFadePercent();
    float GetShakeX();
    int GetCommandTimer();

    //--Property Queries - World Statistics
    static int GetXPOfTable(int pLevel);
    int GetLevel();
    int GetXP();
    int GetXPOfLevel();
    int GetXPToNextLevel();
    int GetXPToNextLevelMax();
    int GetLevelIfXPAdded(int pXPAmount);

    //--Manipulators
    void SetCannotBeKnockedOutFlag(bool pFlag);
    void SetHasPartingShotFlag(bool pFlag);
    void SetRemoveFromPartyOnKnockoutFlag(bool pFlag);
    void MarkPositionedInBattle();
    void SetParagonGrouping(const char *pGrouping);
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetClusterName(const char *pName);
    void SetResponseScript(const char *pPath);
    void SetResponseScriptCode(int pCode, bool pFlag);
    void SetAnimationScript(const char *pPath);
    void SetAIScript(const char *pPath);
    void SetInspectorShowStats(int pAmount);
    void SetInspectorShowResists(int pAmount);
    void SetInspectorShowAbilities(int pAmount);
    void SetAmbushed(bool pIsAmbushed);
    void SetReinforcementTurns(int pTurns);
    void RegisterJob(const char *pName, AdvCombatJob *pJob);
    void SetActiveJob(const char *pJobName);
    void RevertToCombatStartJob();
    void RebuildSkillJobList();
    void SetCombatPortrait(const char *pDLPath);
    void SetCombatCountermask(const char *pDLPath);
    void SetVictoryCountermask(const char *pDLPath);
    void SetTurnIcon(const char *pDLPath);
    void SetVendorImage(int pSlot, const char *pDLPath);
    void SetUIRenderPos(int pSlot, int pX, int pY);
    void SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath);
    void SetIdealPosition(float pXPos, float pYPos);
    void SetIdealPositionX(float pXPos);
    void MoveToPosition(float pXPos, float pYPos, int pTicks);
    void MoveToIdealPosition(int pTicks);
    void SetIgnoreMovementForBlocking(bool pFlag);
    void SetCurrentInitiative(int pValue);
    void SetTurnID(int pValue);
    void SetFlashingWhite(bool pFlag);
    void FlashBlack();
    void BeginKnockout();
    void FinishKnockout();
    void SetBlockRenderForKO(bool pFlag);
    void SetGlobalJP(int pAmount);
    void SetFreeActions(int pFreeActions);
    void SetFreeActionsPerformed(int pFreeActionsPerformed);
    void ResetFreeActionsToTurnStart();
    void SetXP(int pXP);
    void ComputeLevelStatistics(int pLevel);
    void GainJP(int pJP);
    void GainGlobalJP(int pJP);
    void SetRewardXP(int pXP);
    void SetRewardJP(int pJP);
    void SetRewardPlatina(int pPlatina);
    void SetRewardDoctor(int pDoctor);
    void SetTurnKOd(int pTurn);
    void AddRewardItem(const char *pName);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);
    void SetOverrideY(float pY);
    void SetOverrideCombatY(float pY);
    void SetMemoryCard(int pX);
    void SetMemoryJob(int pX, int pY);
    void SetMemoryMemorized(int pX, int pY);
    void SetMemoryTactics(int pX, int pY);
    void ClearCombatStartJob();
    void SetLevelForMugAutoWin(int pLevel);
    void MarkCallout();
    void MarkCrossfade();
    void SetTFIndicatorCount(int pCount);
    void SetTFIndicatorIcon(const char *pDLPath);
    void MarkAssignedPosition();
    void SetFadeInTimers(int pTimerStart, int pTimerMax);
    void Shake();
    void SetCommandTimer(int pTimer);

    //--Core Methods
    void ClearMemoryCursors();
    virtual void FullRestore();
    void HandleCombatStart();
    void HandleCombatEnd();
    bool CheckKnockout();
    static const char *GetStringFromCode(int pCode);
    void CallResponseScript(int pCode);
    void CallAIResponseScript(int pCode);

    ///--Abilities
    bool DoesSecondPageHaveAbilities();
    bool IsAbilityEquipped(AdvCombatAbility *pCheckAbility);
    bool IsAbilityEquippedS(const char *pName);
    bool IsAbilityEquippedI(uint32_t pID);
    const char *GetAbilityNameInSlot(int pX, int pY);
    int GetProbabilityInSlot(int pX, int pY);
    void RegisterAbility(const char *pAbilityName, AdvCombatAbility *pAbility);
    void RegisterAbilityToJob(const char *pAbilityName, const char *pJobName, int pJPCost);
    void UnregisterAbilityFromJob(const char *pAbilityName, const char *pJobName);
    void RemoveAbility(const char *pAbilityName);
    void SetAbilitySlot(int pSlotX, int pSlotY, const char *pAbilityName);
    void SaveCurrentAbilitiesToActiveProfile();
    void SetAbilitySlotProbability(int pSlotX, int pSlotY, int pProbability);
    void SetSecondarySlot(int pSlotX, int pSlotY, const char *pAbilityName);
    void AddPassiveAbility(const char *pAbilityName);
    void RunAbilityScripts(int pCode);
    void PrintAbilitiesToConsole();

    ///--Equipment
    void CreateEquipmentSlot(const char *pName);
    int GetEquipmentSlotsTotal();
    const char *GetDisplayNameOfEquipmentSlot(int pSlot);
    const char *GetNameOfEquipmentSlot(int pSlot);
    bool IsWeaponAlternateSlot(int pSlot);
    int GetSlotOfEquipmentByName(const char *pName);
    int GetSlotForWeaponDamage();
    AdventureItem *GetEquipmentBySlotS(const char *pEquipSlot);
    AdventureItem *GetEquipmentBySlotI(int pEquipSlot);
    AdventureItem *GetWeapon();
    EquipmentSlotPack *GetEquipmentSlotPackageI(int pIndex);
    EquipmentSlotPack *GetEquipmentSlotPackageS(const char *pEquipSlot);
    void SetEquipmentSlotDisplayName(const char *pName, const char *pDisplayName);
    void SetEquipmentSlotIsUsedForStats(const char *pName, bool pFlag);
    void SetEquipmentSlotIsUsedForTags(const char *pName, bool pFlag);
    void SetEquipmentSlotCanBeEmpty(const char *pName, bool pFlag);
    void SetEquipmentSlotIsAltWeapon(const char *pName, bool pFlag);
    void SetEquipmentSlotNoGems(const char *pName, bool pFlag);
    bool EquipItemToSlot(const char *pSlotName, AdventureItem *pItem);
    bool EquipItemToSlotNoGems(const char *pSlotName, AdventureItem *pItem);
    void SetSlotUsedForWeaponDamage(int pSlot);
    void ComputeEquipmentStatistics(CombatStatistics &sStatisticsPack);
    void SwapEquipmentSlots(const char *pSlotA, const char *pSlotB);
    void RemoveGemFromEquipment(AdventureItem *pGem);

    ///--[Profiles]
    int GetTotalProfiles();
    AbilityProfile *GetActiveProfile();
    AbilityProfile *GetProfileS(const char *pName);
    AbilityProfile *GetProfileI(int pSlot);
    const char *GetNameOfProfileI(int pSlot);
    void SetActiveProfileS(const char *pName);
    void SetActiveProfileI(int pSlot);
    void RegisterProfile(const char *pName, AbilityProfile *pProfile);
    void SetProfileAbility(const char *pProfileName, int pX, int pY, const char *pAbilityName);
    void ClearProfiles();
    void RecheckJobsAgainstProfiles();
    StarLinkedList *GetProfilesList();

    ///--Statistics
    int GetHealth();
    int GetHealthMax();
    int GetMagic();
    int GetComboPoints();
    int GetAdrenaline();
    int GetShields();
    int GetStun();
    int GetStunResist();
    int GetStunResistTimer();
    int GetInfluence();
    int GetInfluenceMax();
    float GetHealthPercent();
    float GetMagicPercent();
    void SetHealth(int pHP);
    void SetMagic(int pMP);
    void SetComboPoints(int pCP);
    void SetAdrenaline(int pAdrenaline);
    void SetShields(int pShields);
    void SetStunnable(bool pIsStunnable);
    void SetStun(int pStunValue);
    void SetStunNoDisplay(int pStunValue);
    void SetStunOnlyDisplay();
    void AddStun(int pStunValue);
    void SetStunResist(int pStunResist);
    void SetStunResistTimer(int pTimer);
    void SetInfluence(int pValue);
    void SetInfluenceMax(int pValue);
    void SetHealthPercent(float pPercent, bool pIsHeal);
    void SetMagicPercent(float pPercent);
    int GetStatistic(int pStatisticIndex);
    int GetStatistic(int pGroupIndex, int pStatisticIndex);
    void SetStatistic(int pGroupIndex, int pStatisticIndex, int pValue);
    void InflictDamage(int pDamage, int pHealthPriority, int pAdrenalinePriority, int pShieldPriority);
    void RerunEffects();
    void ComputeStatistics();
    CombatStatistics *GetStatisticsGroup(int pIndex);
    CombatStatistics *GetStatisticsGroup(const char *pName);
    void RefreshStatsForUI();

    ///--Targeting
    bool IsNormalTarget();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void UpdateTimers();
    void UpdatePosition();

    //--File I/O
    //--Drawing
    StarBitmap *GetCombatPortrait();
    StarBitmap *GetCrossfadePortrait();
    StarBitmap *GetCombatCounterMask();
    StarBitmap *GetVictoryCounterMask();
    StarBitmap *GetTurnIcon();

    //--Pointer Routing
    StarLinkedList *GetAbilityList();
    AdvCombatAbility *GetAbilityBySlot(int pX, int pY);
    AdvCombatAbility *GetSecondaryBySlot(int pX, int pY);
    AdvCombatJob *GetActiveJob();
    StarLinkedList *GetJobList();
    StarLinkedList *GetJobSkillUIList();
    StarLinkedList *GetEffectRenderList();
    StarBitmap *GetVendorImage(int pIndex);

    //--Static Functions
    static float ComputeResistance(int pResistance);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatEntity_GetProperty(lua_State *L);
int Hook_AdvCombatEntity_SetProperty(lua_State *L);

