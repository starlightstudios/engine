//--Base
#include "TiledLevel.h"

//--Classes
#include "TiledRawData.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
TiledLevel::TiledLevel()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_TILEDLEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[TiledLevel]
    //--System
    //--Sizing
    mXSize = 0;
    mYSize = 0;
    mCurrentScale = 3.0f;

    //--Tileset Packs
    mTilesetsTotal = 0;
    mTilesetPacks = NULL;

    //--Raw Lists
    mTileLayers = new StarLinkedList(true);
    mObjectData = new StarLinkedList(true);
    mImageData = new StarLinkedList(true);

    //--Collision Storage
    mCollisionLayersTotal = 0;
    mCollisionLayers = NULL;
}
TiledLevel::~TiledLevel()
{
    //--Tilesets.
    for(int i = 0; i < mTilesetsTotal; i ++)
    {
        TiledRawData::DeleteTilesetPack(&mTilesetPacks[i]);
    }
    free(mTilesetPacks);

    //--Lists.
    delete mTileLayers;
    delete mObjectData;
    delete mImageData;

    //--Collision.
    for(int i = 0; i < mCollisionLayersTotal; i ++)
    {
        CollisionPack::FreeThis(&mCollisionLayers[i]);
    }
    free(mCollisionLayers);
}

///==================================== Structure Functions =======================================
void CollisionPack::Initialize()
{
    mCollisionSizeX = 0;
    mCollisionSizeY = 0;
    mCollisionData = NULL;
    mCollisionFlipData = NULL;
}
void CollisionPack::AllocateSizes(int pXSize, int pYSize)
{
    ///--[Documentation]
    //--Sets the package to have the given sizes. Should only be called once after initialization,
    //  does not attempt to deallocate or resize for speed reasons.
    if(pXSize < 1) return;
    if(pYSize < 1) return;

    //--Set.
    mCollisionSizeX = pXSize;
    mCollisionSizeY = pYSize;

    //--Allocate.
    mCollisionData     = (uint8_t  **)starmemoryalloc(sizeof(uint8_t  *) * mCollisionSizeX);
    mCollisionFlipData = (uint16_t **)starmemoryalloc(sizeof(uint16_t *) * mCollisionSizeX);
    for(int x = 0; x < mCollisionSizeX; x ++)
    {
        mCollisionData[x]     = (uint8_t *) starmemoryalloc(sizeof(uint8_t)  * mCollisionSizeY);
        mCollisionFlipData[x] = (uint16_t *)starmemoryalloc(sizeof(uint16_t) * mCollisionSizeY);
    }
}
void CollisionPack::ClearTo(int pCode)
{
    ///--[Documentation]
    //--Clears the structure to the given code, giving it a default collision value.
    for(int x = 0; x < mCollisionSizeX; x ++)
    {
        for(int y = 0; y < mCollisionSizeY; y ++)
        {
            mCollisionData[x][y] = pCode;
            mCollisionFlipData[x][y] = 0;
        }
    }
}
int CollisionPack::GetClipAt(int pX, int pY)
{
    if(pX < 0 || pY < 0) return CLIP_NONE;
    if(pX >= mCollisionSizeX || pY >= mCollisionSizeX) return CLIP_NONE;
    return mCollisionData[pX][pY];
}
void CollisionPack::FreeThis(void *pPtr)
{
    //--Assumes this object is stored in a fixed-size list, so the pointer itself is not deallocated.
    CollisionPack *rPack = (CollisionPack *)pPtr;
    for(int x = 0; x < rPack->mCollisionSizeX; x ++)
    {
        free(rPack->mCollisionData[x]);
        free(rPack->mCollisionFlipData[x]);
    }
    free(rPack->mCollisionData);
    free(rPack->mCollisionFlipData);
}

///===================================== Property Queries =========================================
bool TiledLevel::GetClipAt(float pX, float pY, float pZ, int pLayer)
{
    //--Range check: Must be within an existing collision layer.
    if(pLayer < 0 || pLayer >= mCollisionLayersTotal) return true;

    //--Edge check.
    if(pX < 0.0f || pY < 0.0f || pX >= mCollisionLayers[pLayer].mCollisionSizeX || pY >= mCollisionLayers[pLayer].mCollisionSizeY) return true;

    //--Return from the collision hull.
    return mCollisionLayers[pLayer].mCollisionData[(int)pX][(int)pY];
}
bool TiledLevel::GetClipAt(float pX, float pY, float pZ)
{
    return GetClipAt(pX, pY, pZ, 0);
}

///======================================== Manipulators ==========================================
///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
void TiledLevel::CompileAfterParse()
{
    //--Called after ParseSLF(), compiles any data that wasn't in the SLF. Does nothing in the default
    //  case, but can be overridden.
}

///=========================================== Update =============================================
void TiledLevel::Update()
{
    //--Does nothing by itself.
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void TiledLevel::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElementAsTail("X", this);
}
void TiledLevel::Render()
{
    //--Does nothing by itself. Derived classes should do something here.
}

///====================================== Pointer Routing =========================================
StarLinkedList *TiledLevel::GetObjectData()
{
    return mObjectData;
}
CollisionPack *TiledLevel::GetCollisionPack(int pDepth)
{
    if(pDepth < 0 || pDepth >= mCollisionLayersTotal) return NULL;
    return &mCollisionLayers[pDepth];
}

///===================================== Static Functions =========================================
TiledLevel *TiledLevel::Fetch()
{
    //--Note: Will always return a valid TiledLevel, or NULL. Never returns an object of the wrong type.
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_TILEDLEVEL)) return NULL;
    return (TiledLevel *)rCheckLevel;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
