//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvUICampfire.h"
#include "AdvUIFieldAbilities.h"
#include "AdvUITrainer.h"
#include "AdvUIVendor.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///======================================= Documentation ==========================================
//--Contains functions for handling the various submodes that the menu operates in. This includes
//  things like Campfire, Field Abilities, and Trainer UIs.

///========================================= Functions ============================================
///--[Campfire Routines]
void AdventureMenu::OpenCampfire()
{
    ///--[Documentation]
    //--Opens the UI and switches it so the CampfireUI is the primary UI. This will disallow use
    //  of the basic menu functions, and instead allow the player to save, enter passwords, etc.

    ///--[Error Checks]
    //--Make sure there is a campfire base UI.
    if(!rCampBase)
    {
        fprintf(stderr, "AdventureMenu:OpenCampfire() - Warning. Cannot open campfire UI, base UI was NULL.\n");
        return;
    }

    ///--[Execution]
    //--Order the menu to show.
    Show();

    //--Set flags.
    mSubmodeCode = AM_SUBMODE_CAMPFIRE;
    rCampBase->TakeForeground();
    rCampBase->SetBenchMode(false);
}
void AdventureMenu::SetBenchMode(bool pFlag)
{
    ///--[Documentation]
    //--Benches allow the player most of the same functionality as campfires, but notably don't allow
    //  warping. This is to allow the player to rest and save in a tough area that wasn't intended
    //  to allow warping, usually for story reasons.
    //--This should be called after OpenCampfire(), which resets the bench flag.

    ///--[Error Checks]
    //--Make sure there is a campfire base UI.
    if(!rCampBase)
    {
        fprintf(stderr, "AdventureMenu:SetBenchMode() - Warning. Cannot set flags in campfire UI, base UI was NULL.\n");
        return;
    }

    ///--[Execution]
    //--Set.
    rCampBase->SetBenchMode(pFlag);
}

///--[Field Ability Routines]
void AdventureMenu::OpenFieldAbilities()
{
    ///--[Documentation]
    //--Field abilities are edited from normal gameplay, not the usual UI. This function is called when the
    //  player presses the key (default: C)
    //--Note: This fails silently if no FieldAbilities UI is located, as not all games have one.
    mSubmodeCode = AM_SUBMODE_NONE;

    ///--[Error Checks]
    //--Locate the UI piece.
    StarUIPiece *rStarUIPiece = RetrieveUI("FieldAbilities", POINTER_TYPE_STARUIPIECE, mrFieldAbilityComponents, "AdventureMenu:OpenFieldAbilities()");
    if(!rStarUIPiece) return;

    ///--[Execution]
    //--Order the menu to show.
    Show();

    //--Flag.
    mSubmodeCode = AM_SUBMODE_FIELDABILITIES;

    //--Order the UI to initialize itself.
    AdvUIFieldAbilities *rFieldAbilityUI = dynamic_cast<AdvUIFieldAbilities *>(rStarUIPiece);
    rFieldAbilityUI->TakeForeground();
}
void AdventureMenu::CloseFieldAbilities()
{
    ///--[Documentation]
    //--Called when the FieldAbility UI is closed.

    ///--[Execution]
    Hide();
    mSubmodeCode = AM_SUBMODE_NONE;
}

///--[Trainer Routines]
void AdventureMenu::OpenTrainer()
{
    ///--[Documentation]
    //--Activates trainer mode. This implicitly calls the Show() routine if the UI exists.
    //--Fails silently if the UI is not found. Not all games use a Trainer UI.

    ///--[Error Checks]
    //--Locate the UI piece.
    StarUIPiece *rStarUIPiece = RetrieveUI("Trainer", POINTER_TYPE_STARUIPIECE, mrTrainerComponents, "AdventureMenu:OpenTrainer()");
    if(!rStarUIPiece) return;

    ///--[Execution]
    //--Order the menu to show.
    Show();

    //--Flag.
    mSubmodeCode = AM_SUBMODE_TRAINER;

    //--Cast the object and initialize it.
    AdvUITrainer *rTrainerUI = dynamic_cast<AdvUITrainer *>(rStarUIPiece);
    rTrainerUI->TakeForeground();
}
void AdventureMenu::CloseTrainer()
{
    Hide();
    mSubmodeCode = AM_SUBMODE_NONE;
}

///--[Vendor Routines]
void AdventureMenu::InitializeVendorMode(const char *pShopName, const char *pPath, const char *pTeardownPath)
{
    ///--[Documentation]
    //--Activates Vendor mode, which cuts off the rest of the UI and causes the cancel button to exit the UI
    //  directly. In Vendor mode, the player can buy stuff from the vendor, obviously, as well as sell, buyback
    //  items previously sold, and modify gems.

    ///--[Error Checks]
    //--If the shop name or path is invalid, cancels out of the UI immediately.
    if(!pShopName || !pPath) return;

    //--Locate. The UI must exist.
    StarUIPiece *rStarUIPiece = RetrieveUI("Vendor", POINTER_TYPE_ADVUIVENDOR, mrVendorComponents, "AdventureMenu:InitializeVendorMode()");
    if(!rStarUIPiece) return;

    ///--[Activate]
    //--Show the UI.
    Show();

    //--Dynamic cast.
    AdvUIVendor *rVendorUI = dynamic_cast<AdvUIVendor *>(rStarUIPiece);

    //--Set flags.
    mSubmodeCode = AM_SUBMODE_VENDOR;
    rVendorUI->SetShopInformation(pShopName, pPath, pTeardownPath);
}
void AdventureMenu::CloseVendor()
{
    Hide();
    mSubmodeCode = AM_SUBMODE_NONE;
}
