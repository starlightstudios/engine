//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdventureDebug.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "Global.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

///--[Sorter Function]
int CompareCombatEventPacks(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() below. This one is Z to A.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Cast to CombatEventPackage.
    CombatEventPack *rPackageA = (CombatEventPack *)(*rEntryA)->rData;
    CombatEventPack *rPackageB = (CombatEventPack *)(*rEntryB)->rData;

    return (rPackageA->mPriority - rPackageB->mPriority);
}

///======================================= Core Methods ===========================================
void AdvCombat::RecomputePlayerInputHighlight()
{
    ///--[Documentation]
    //--Based on where the cursor is and what mode we're in, determines where the highlight should
    //  be and orders it to move there.
    bool tShowExpertBar = OptionsManager::Fetch()->GetOptionB("Expert Combat UI");

    ///--[Normal Mode]
    //--Has several different submodes.
    if(!tShowExpertBar)
    {
        if(mPlayerMode == ADVCOMBAT_INPUT_CARDS)
        {
            //--Uses mAbilitySelectionX to determine the card.
            if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_ATTACK)
            {
                mAbilityHighlightPos.MoveTo(543.0f, 511.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
                mAbilityHighlightSize.MoveTo(150.0f, 170.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
            }
            else if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_JOB)
            {
                mAbilityHighlightPos.MoveTo(734.0f, 511.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
                mAbilityHighlightSize.MoveTo(150.0f, 170.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
            }
            else if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_MEMORIZED)
            {
                mAbilityHighlightPos.MoveTo(925.0f, 511.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
                mAbilityHighlightSize.MoveTo(150.0f, 170.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
            }
            else if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_TACTICS)
            {
                mAbilityHighlightPos.MoveTo(1116.0f, 511.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
                mAbilityHighlightSize.MoveTo(150.0f, 170.0f, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
            }
        }
        else if(mPlayerMode == ADVCOMBAT_INPUT_JOB)
        {
            //--Constants.
            float cLft = 362.0f;
            float cTop = 524.0f;
            float cWid =  50.0f;
            float cHei =  50.0f;
            float cSpX =  53.0f;
            float cSpY =  53.0f;

            //--Calculate.
            float tXPos = cLft + (cSpX * mAbilitySelectionX);
            float tYPos = cTop + (cSpY * mAbilitySelectionY);

            //--Offsets.
            if(mAbilitySelectionY >= 1) tYPos = tYPos + 8.0f;

            //--Set.
            mAbilityHighlightPos.MoveTo(tXPos, tYPos, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
            mAbilityHighlightSize.MoveTo(cWid, cHei, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
        }
        else if(mPlayerMode == ADVCOMBAT_INPUT_MEMORIZED)
        {
            //--Constants.
            float cLft = 256.0f;
            float cTop = 524.0f;
            float cWid =  50.0f;
            float cHei =  50.0f;
            float cSpX =  53.0f;
            float cSpY =  53.0f;

            //--Calculate.
            float tXPos = cLft + (cSpX * mAbilitySelectionX);
            float tYPos = cTop + (cSpY * mAbilitySelectionY);

            //--Offsets.
            if(mAbilitySelectionY >= 2) tYPos = tYPos + 8.0f;

            //--Set.
            mAbilityHighlightPos.MoveTo(tXPos, tYPos, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
            mAbilityHighlightSize.MoveTo(cWid, cHei, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
        }
        else if(mPlayerMode == ADVCOMBAT_INPUT_TACTICS)
        {
            //--Constants.
            float cLft =  44.0f;
            float cTop = 532.0f;
            float cWid =  50.0f;
            float cHei =  50.0f;
            float cSpX =  53.0f;
            float cSpY =  53.0f;

            //--Calculate.
            float tXPos = cLft + (cSpX * mAbilitySelectionX);
            float tYPos = cTop + (cSpY * mAbilitySelectionY);

            //--Set.
            mAbilityHighlightPos.MoveTo(tXPos, tYPos, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
            mAbilityHighlightSize.MoveTo(cWid, cHei, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
        }
    }
    ///--[Expert Mode]
    //--All the abilities are in one large bar.
    else
    {
        //--Constants.
        float cLft =  32.0f;
        float cTop = 576.0f;
        float cWid =  50.0f;
        float cHei =  50.0f;
        float cSpX =  53.0f;
        float cSpY =  53.0f;

        //--Variables.
        float tXPos = 0.0f;
        float tYPos = 0.0f;

        //--Primary Page:
        if(!mExpertSecondPage)
        {
            //--Calculate.
            tXPos = cLft + (cSpX * mAbilitySelectionX);
            tYPos = cTop + (cSpY * mAbilitySelectionY);

            //--X Offsets.
            if(mAbilitySelectionX >= ACE_ABILITY_MEMORIZE_GRID_X0) tXPos = tXPos + 18.0f;
            if(mAbilitySelectionX >= ACE_ABILITY_TACTICS_GRID_X0)  tXPos = tXPos + 18.0f;

            //--Y Offsets.
            if(mAbilitySelectionX <  ACE_ABILITY_MEMORIZE_GRID_X0 &&                                                     mAbilitySelectionY > 0) tYPos = tYPos + 8.0f;
            if(mAbilitySelectionX >= ACE_ABILITY_MEMORIZE_GRID_X0 && mAbilitySelectionX < ACE_ABILITY_TACTICS_GRID_X0 && mAbilitySelectionY > 1) tYPos = tYPos + 8.0f;
        }
        //--Second page:
        else
        {
            //--Calculate.
            tXPos = cLft + (cSpX * (mAbilitySelectionX - ACE_ABILITY_EXPERT_SIZE_X));
            tYPos = cTop + (cSpY * mAbilitySelectionY);
        }

        //--Set.
        mAbilityHighlightPos.MoveTo(tXPos, tYPos, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
        mAbilityHighlightSize.MoveTo(cWid, cHei, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
    }
}

///========================================== Update ==============================================
void AdvCombat::HandlePlayerControls(bool pAnyEntitiesMoving)
{
    ///--[Documentation and Setup]
    //--Handles player controls when an entity with no AI is acting.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Option.
    bool tShowExpertBar = OptionsManager::Fetch()->GetOptionB("Expert Combat UI");

    ///--[Timers]
    //--Player interface timer runs to ADVCOMBAT_MOVE_TRANSITION_TICKS. This causes it to slide up.
    if(mPlayerInterfaceTimer < ADVCOMBAT_MOVE_TRANSITION_TICKS)
    {
        //--Check the events list. If there are any events queued, don't do this.
        if(mEventQueue->GetListSize() < 1 && mApplicationQueue->GetListSize() < 1) mPlayerInterfaceTimer ++;
    }

    ///--[First tick]
    //--If this is the first tick that the player has gained control, we need to reposition the ability
    //  selection. It is implied, as this is the first tick, that target selection is not occurring.
    if(mActionFirstTick)
    {
        //--Flag.
        mActionFirstTick = false;
        mPlayerMode = ADVCOMBAT_INPUT_CARDS;
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;

        //--Memory cursor.
        if(mMemoryCursor)
        {
            //--Setup.
            AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

            //--Basic case: Use the card cursor.
            if(!tShowExpertBar)
            {
                if(rActingEntity) mAbilitySelectionX = rActingEntity->GetMemoryCardX();
            }
            //--Expert: Always use the tactics value.
            else
            {
                if(rActingEntity)
                {
                    mAbilitySelectionX = rActingEntity->GetMemoryTacticsX();
                    mAbilitySelectionY = rActingEntity->GetMemoryTacticsY();
                }
            }
        }

        //--Highlight computation.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();
    }

    ///--[Combat Inspector]
    //--Can be triggered at any time during the player's turn.
    if(rControlManager->IsFirstPress("F1"))
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        ActivateInspector();
        return;
    }

    ///--[Description Toggle]
    //--Toggles low and high detail ability descriptions.
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowDetailedAbilityDescriptions ++;
        if(mShowDetailedAbilityDescriptions > ADVCOMBAT_DESCRIPTIONS_HIDDEN) mShowDetailedAbilityDescriptions = ADVCOMBAT_DESCRIPTIONS_SIMPLE;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[UI Toggle]
    //--The player can toggle the UI on and off at any time.
    if(rControlManager->IsFirstPress("F4"))
    {
        mHideCombatUI = !mHideCombatUI;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Debug]
    //--If the player presses the 9 key, they win! This is for debug.
    if(rControlManager->IsFirstPress("Key_9") && !pAnyEntitiesMoving)
    {
        const char *rCheckPassword = OptionsManager::Fetch()->GetOptionS("Patron Password");
        if(AdventureDebug::xManualActivation || (rCheckPassword && !strcasecmp(rCheckPassword, "Ares Defense Industries")))
        {
            BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
        }
        return;
    }

    //--Fill player's MP.
    if(rControlManager->IsFirstPress("F5"))
    {
        const char *rCheckPassword = OptionsManager::Fetch()->GetOptionS("Patron Password");
        if(AdventureDebug::xManualActivation || (rCheckPassword && !strcasecmp(rCheckPassword, "Ares Defense Industries")))
        {
            AudioManager::Fetch()->PlaySound("Menu|Select");
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
            while(rEntity)
            {
                rEntity->SetMagic(rEntity->GetStatistic(STATS_MPMAX));
                rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
            }
        }
        return;
    }

    ///--[Ability Selection]
    //--Selecting which ability to use this action.
    if(!mIsSelectingTargets)
    {
        ///--[Normal Mode]
        if(!tShowExpertBar)
        {
            //--Cards that either activate an ability, or bring up a submenu.
            if(mPlayerMode == ADVCOMBAT_INPUT_CARDS)
            {
                HandleCardControls();
            }
            //--Job. Has an ability selection window.
            else if(mPlayerMode == ADVCOMBAT_INPUT_JOB)
            {
                HandleJobControls();
            }
            //--Memorized Skills. Has an ability selection window.
            else if(mPlayerMode == ADVCOMBAT_INPUT_MEMORIZED)
            {
                HandleMemorizedControls();
            }
            //--Tactics. Has an ability selection window.
            else if(mPlayerMode == ADVCOMBAT_INPUT_TACTICS)
            {
                HandleTacticsControls();
            }
        }
        ///--[Expert Mode]
        else
        {
            HandleExpertControls();
        }
    }
    ///--[Target Selection]
    //--Selecting which target to execute the ability on.
    else
    {
        //--Entities were moving, cannot allow target selection yet.
        if(pAnyEntitiesMoving) return;

        //--Activate, executes action on target cluster.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Fire the ability.
            mIsSelectingTargets = false;
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
            MovePartyByActiveCluster();
            ExecuteActiveAbility();
            ClearPredictionBoxes();
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Cancel, exits target mode.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsSelectingTargets = false;
            rActiveTargetCluster = NULL;
            MovePartyByActiveCluster();
            ClearPredictionBoxes();
            PositionEnemiesByTarget(NULL);
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Storage.
        int tOldTargetCluster = mTargetClusterCursor;

        //--Decrements target cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            mTargetClusterCursor --;
            if(mTargetClusterCursor < 0) mTargetClusterCursor = mTargetClusters->GetListSize() - 1;
        }
        //--Increments target cursor.
        if(rControlManager->IsFirstPress("Right"))
        {
            mTargetClusterCursor ++;
            if(mTargetClusterCursor >= mTargetClusters->GetListSize()) mTargetClusterCursor = 0;
        }

        //--If this flag was set, we need to reposition the highlight.
        if(mTargetClusterCursor != tOldTargetCluster)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Move party around.
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
            MovePartyByActiveCluster();
            SetClusterAsActive(mTargetClusterCursor);

            //--Move enemies around.
            PositionEnemiesByTarget(rActiveTargetCluster->mrTargetList);
        }
    }
}
void AdvCombat::HandleCardControls()
{
    ///--[Documentation]
    //--Handles control inputs when dealing with cards. There is always a fixed number.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Directional Keys]
    //--Directional keys change where the ability highlight is.
    bool tRepositionHighlight = false;
    if(rControlManager->IsFirstPress("Left"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX --;
        if(mAbilitySelectionX < 0) mAbilitySelectionX = ADVCOMBAT_INPUT_CARD_TOTAL - 1;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX ++;
        if(mAbilitySelectionX >= ADVCOMBAT_INPUT_CARD_TOTAL) mAbilitySelectionX = 0;
    }

    //--Highlight.
    if(tRepositionHighlight)
    {
        //--Subroutine.
        RecomputePlayerInputHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Activate]
    //--Either enters a submode, or activate the attack ability.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Set the card memory cursor for this entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
        if(rActingEntity) rActingEntity->SetMemoryCard(mAbilitySelectionX);

        //--Attack. Always uses the 0x0 skill on the grid.
        if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_ATTACK)
        {
            BeginTargetingAbilityBySlot(0, 0);
        }
        //--Job. Enters submenu.
        else if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_JOB)
        {
            //--Flags.
            mPlayerMode = ADVCOMBAT_INPUT_JOB;
            mAbilitySelectionX = 0;
            mAbilitySelectionY = 0;
            if(mMemoryCursor)
            {
                mAbilitySelectionX = rActingEntity->GetMemoryJobX();
                mAbilitySelectionY = rActingEntity->GetMemoryJobY();
            }

            //--Cursor change.
            RecomputePlayerInputHighlight();
            mAbilityHighlightPos.Complete();
            mAbilityHighlightSize.Complete();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Memorized. Enters submenu.
        else if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_MEMORIZED)
        {
            //--Flags.
            mPlayerMode = ADVCOMBAT_INPUT_MEMORIZED;
            mAbilitySelectionX = 0;
            mAbilitySelectionY = 0;
            if(mMemoryCursor)
            {
                mAbilitySelectionX = rActingEntity->GetMemoryMemorizedX();
                mAbilitySelectionY = rActingEntity->GetMemoryMemorizedY();
            }

            //--Cursor change.
            RecomputePlayerInputHighlight();
            mAbilityHighlightPos.Complete();
            mAbilityHighlightSize.Complete();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Tactics. Enters submenu.
        else if(mAbilitySelectionX == ADVCOMBAT_INPUT_CARD_TACTICS)
        {
            //--Flags.
            mPlayerMode = ADVCOMBAT_INPUT_TACTICS;
            mAbilitySelectionX = 0;
            mAbilitySelectionY = 0;
            if(mMemoryCursor)
            {
                mAbilitySelectionX = rActingEntity->GetMemoryTacticsX();
                mAbilitySelectionY = rActingEntity->GetMemoryTacticsY();
            }

            //--Cursor change.
            RecomputePlayerInputHighlight();
            mAbilityHighlightPos.Complete();
            mAbilityHighlightSize.Complete();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
}
void AdvCombat::HandleJobControls()
{
    ///--[Documentation]
    //--Handles controls when selecting a job-specific ability.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Directional Keys]
    //--Directional keys change where the ability highlight is.
    bool tRepositionHighlight = false;
    if(rControlManager->IsFirstPress("Left"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX --;
        if(mAbilitySelectionX < 0) mAbilitySelectionX = ACE_ABILITY_JOB_GRID_X1;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX ++;
        if(mAbilitySelectionX > ACE_ABILITY_JOB_GRID_X1) mAbilitySelectionX = 0;
    }
    if(rControlManager->IsFirstPress("Up"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionY --;
        if(mAbilitySelectionY < 0) mAbilitySelectionY = ACE_ABILITY_GRID_SIZE_Y - 1;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionY ++;
        if(mAbilitySelectionY >= ACE_ABILITY_GRID_SIZE_Y) mAbilitySelectionY = 0;
    }

    //--Highlight.
    if(tRepositionHighlight)
    {
        //--Subroutine.
        RecomputePlayerInputHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Shoulder]
    //--Switches card modes.
    if(rControlManager->IsFirstPress("UpLevel"))
    {
        //--Get entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

        //--Switch to Tactics.
        mPlayerMode = ADVCOMBAT_INPUT_TACTICS;
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        if(mMemoryCursor)
        {
            mAbilitySelectionX = rActingEntity->GetMemoryTacticsX();
            mAbilitySelectionY = rActingEntity->GetMemoryTacticsY();
        }

        //--Cursor change.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
    if(rControlManager->IsFirstPress("DnLevel"))
    {
        //--Get entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

        //--Switch to Memorized.
        mPlayerMode = ADVCOMBAT_INPUT_MEMORIZED;
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        if(mMemoryCursor)
        {
            mAbilitySelectionX = rActingEntity->GetMemoryMemorizedX();
            mAbilitySelectionY = rActingEntity->GetMemoryMemorizedY();
        }

        //--Cursor change.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Activate]
    //--Attempts to activate the ability in the slot. May fail.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Set the card memory cursor for this entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
        if(rActingEntity) rActingEntity->SetMemoryJob(mAbilitySelectionX, mAbilitySelectionY);

        //--Activate.
        BeginTargetingAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
        return;
    }

    ///--[Cancel]
    //--Return to the cards.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Flags.
        mAbilitySelectionX = ADVCOMBAT_INPUT_CARD_JOB;
        mPlayerMode = ADVCOMBAT_INPUT_CARDS;

        //--Highlight.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvCombat::HandleMemorizedControls()
{
    ///--[Documentation]
    //--Handles controls when selecting a memorized ability.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Directional Keys]
    //--Sizes.
    int tClampX = ACE_ABILITY_MEMORIZE_GRID_X1 - ACE_ABILITY_MEMORIZE_GRID_X0 + 1;

    //--Directional keys change where the ability highlight is.
    bool tRepositionHighlight = false;
    if(rControlManager->IsFirstPress("Left"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX --;
        if(mAbilitySelectionX < 0) mAbilitySelectionX = tClampX - 1;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX ++;
        if(mAbilitySelectionX >= tClampX) mAbilitySelectionX = 0;
    }
    if(rControlManager->IsFirstPress("Up"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionY --;
        if(mAbilitySelectionY < 0) mAbilitySelectionY = ACE_ABILITY_GRID_SIZE_Y - 1;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionY ++;
        if(mAbilitySelectionY >= ACE_ABILITY_GRID_SIZE_Y) mAbilitySelectionY = 0;
    }

    //--Highlight.
    if(tRepositionHighlight)
    {
        //--Subroutine.
        RecomputePlayerInputHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Shoulder]
    //--Switches card modes.
    if(rControlManager->IsFirstPress("UpLevel"))
    {
        //--Get entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

        //--Switch to Job Skills.
        mPlayerMode = ADVCOMBAT_INPUT_JOB;
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        if(mMemoryCursor)
        {
            mAbilitySelectionX = rActingEntity->GetMemoryJobX();
            mAbilitySelectionY = rActingEntity->GetMemoryJobY();
        }

        //--Cursor change.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
    if(rControlManager->IsFirstPress("DnLevel"))
    {
        //--Get entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

        //--Switch to Memorized.
        mPlayerMode = ADVCOMBAT_INPUT_TACTICS;
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        if(mMemoryCursor)
        {
            mAbilitySelectionX = rActingEntity->GetMemoryTacticsX();
            mAbilitySelectionY = rActingEntity->GetMemoryTacticsY();
        }

        //--Cursor change.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Activate]
    //--Attempts to activate the ability in the slot. May fail.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Set the card memory cursor for this entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
        if(rActingEntity) rActingEntity->SetMemoryMemorized(mAbilitySelectionX, mAbilitySelectionY);

        //--Activate.
        BeginTargetingAbilityBySlot(mAbilitySelectionX + ACE_ABILITY_MEMORIZE_GRID_X0, mAbilitySelectionY);
        return;
    }

    ///--[Cancel]
    //--Return to the cards.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Flags.
        mAbilitySelectionX = ADVCOMBAT_INPUT_CARD_MEMORIZED;
        mPlayerMode = ADVCOMBAT_INPUT_CARDS;

        //--Highlight.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvCombat::HandleTacticsControls()
{
    ///--[Documentation]
    //--Handles controls when selecting a tactics ability.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Directional Keys]
    //--Sizes.
    int tClampX = ACE_ABILITY_TACTICS_GRID_X1 - ACE_ABILITY_TACTICS_GRID_X0 + 1;

    //--Directional keys change where the ability highlight is.
    bool tRepositionHighlight = false;
    if(rControlManager->IsFirstPress("Left"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX --;
        if(mAbilitySelectionX < 0) mAbilitySelectionX = tClampX - 1;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionX ++;
        if(mAbilitySelectionX >= tClampX) mAbilitySelectionX = 0;
    }
    if(rControlManager->IsFirstPress("Up"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionY --;
        if(mAbilitySelectionY < 0) mAbilitySelectionY = ACE_ABILITY_GRID_SIZE_Y - 1;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        tRepositionHighlight = true;
        mAbilitySelectionY ++;
        if(mAbilitySelectionY >= ACE_ABILITY_GRID_SIZE_Y) mAbilitySelectionY = 0;
    }

    //--Highlight.
    if(tRepositionHighlight)
    {
        //--Subroutine.
        RecomputePlayerInputHighlight();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Shoulder]
    //--Switches card modes.
    if(rControlManager->IsFirstPress("UpLevel"))
    {
        //--Get entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

        //--Switch to Memorized.
        mPlayerMode = ADVCOMBAT_INPUT_MEMORIZED;
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        if(mMemoryCursor)
        {
            mAbilitySelectionX = rActingEntity->GetMemoryMemorizedX();
            mAbilitySelectionY = rActingEntity->GetMemoryMemorizedY();
        }

        //--Cursor change.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
    if(rControlManager->IsFirstPress("DnLevel"))
    {
        //--Get entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

        //--Switch to Job Skills.
        mPlayerMode = ADVCOMBAT_INPUT_JOB;
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        if(mMemoryCursor)
        {
            mAbilitySelectionX = rActingEntity->GetMemoryJobX();
            mAbilitySelectionY = rActingEntity->GetMemoryJobY();
        }

        //--Cursor change.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Activate]
    //--Attempts to activate the ability in the slot. May fail.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Set the card memory cursor for this entity.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
        if(rActingEntity) rActingEntity->SetMemoryTactics(mAbilitySelectionX, mAbilitySelectionY);

        //--Activate.
        BeginTargetingAbilityBySlot(mAbilitySelectionX + ACE_ABILITY_TACTICS_GRID_X0, mAbilitySelectionY);
        return;
    }

    ///--[Cancel]
    //--Return to the cards.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Flags.
        mAbilitySelectionX = ADVCOMBAT_INPUT_CARD_TACTICS;
        mPlayerMode = ADVCOMBAT_INPUT_CARDS;

        //--Highlight.
        RecomputePlayerInputHighlight();
        mAbilityHighlightPos.Complete();
        mAbilityHighlightSize.Complete();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvCombat::HandleExpertControls()
{
    ///--[ ====== Documentation ===== ]
    //--Handles expert controls. This has the entire skill set in a single block, no need for cards.
    //  Implicitly uses the "Tactics" memory slots.
    ControlManager *rControlManager = ControlManager::Fetch();
    bool tRepositionHighlight = false;

    ///--[ ======= First Page ======= ]
    //--Contains most of the abilities.
    if(!mExpertSecondPage)
    {
        ///--[Left and Right]
        //--Directional keys change where the ability highlight is.
        if(rControlManager->IsFirstPress("Left"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionX --;
            if(mAbilitySelectionX < 0) mAbilitySelectionX = ACE_ABILITY_EXPERT_SIZE_X - 1;
        }
        if(rControlManager->IsFirstPress("Right"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionX ++;
            if(mAbilitySelectionX >= ACE_ABILITY_EXPERT_SIZE_X) mAbilitySelectionX = 0;
        }

        ///--[Up and Down]
        //--Directional keys change the highlight. Can also scroll pages.
        if(rControlManager->IsFirstPress("Up"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionY --;
            if(mAbilitySelectionY < 0)
            {
                //--Set to second page.
                mAbilitySelectionX += ACE_ABILITY_EXPERT_SIZE_X;
                mAbilitySelectionY = ACE_ABILITY_EXPERT_SIZE_Y - 1;
                mExpertSecondPage = true;

                //--Clamp X.
                if(mAbilitySelectionX >= ACE_ABILITY_GRID_SIZE_X) mAbilitySelectionX = ACE_ABILITY_GRID_SIZE_X - 1;
            }
        }
        if(rControlManager->IsFirstPress("Down"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionY ++;
            if(mAbilitySelectionY >= ACE_ABILITY_EXPERT_SIZE_Y)
            {
                //--Set to second page.
                mAbilitySelectionX += ACE_ABILITY_EXPERT_SIZE_X;
                mAbilitySelectionY = 0;
                mExpertSecondPage = true;

                //--Clamp X.
                if(mAbilitySelectionX >= ACE_ABILITY_GRID_SIZE_X) mAbilitySelectionX = ACE_ABILITY_GRID_SIZE_X - 1;
            }
        }

        //--Highlight.
        if(tRepositionHighlight)
        {
            //--Subroutine.
            RecomputePlayerInputHighlight();
            mPlayerCPTimer = 0;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        ///--[Activate]
        //--Attempts to activate the ability in the slot. May fail.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Set the card memory cursor for this entity.
            AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
            if(rActingEntity) rActingEntity->SetMemoryTactics(mAbilitySelectionX, mAbilitySelectionY);

            //--Activate.
            BeginTargetingAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
            return;
        }
    }
    ///--[ ======= Second Page ====== ]
    //--Mostly has transformation and misc. abilities.
    else
    {
        ///--[Left and Right]
        //--Directional keys change where the ability highlight is.
        if(rControlManager->IsFirstPress("Left"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionX --;
            if(mAbilitySelectionX < ACE_ABILITY_EXPERT_SIZE_X) mAbilitySelectionX = ACE_ABILITY_GRID_SIZE_X - 1;
        }
        if(rControlManager->IsFirstPress("Right"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionX ++;
            if(mAbilitySelectionX >= ACE_ABILITY_GRID_SIZE_X) mAbilitySelectionX = ACE_ABILITY_EXPERT_SIZE_X;
        }

        ///--[Up and Down]
        //--Directional keys change the highlight. Can also scroll pages.
        if(rControlManager->IsFirstPress("Up"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionY --;
            if(mAbilitySelectionY < 0)
            {
                mAbilitySelectionX -= ACE_ABILITY_EXPERT_SIZE_X;
                mAbilitySelectionY = ACE_ABILITY_EXPERT_SIZE_Y - 1;
                mExpertSecondPage = false;
            }
        }
        if(rControlManager->IsFirstPress("Down"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionY ++;
            if(mAbilitySelectionY >= ACE_ABILITY_EXPERT_SIZE_Y)
            {
                mAbilitySelectionX -= ACE_ABILITY_EXPERT_SIZE_X;
                mAbilitySelectionY = 0;
                mExpertSecondPage = false;
            }
        }

        //--Highlight.
        if(tRepositionHighlight)
        {
            //--Subroutine.
            RecomputePlayerInputHighlight();
            mPlayerCPTimer = 0;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        ///--[Activate]
        //--Attempts to activate the ability in the slot. May fail.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Set the card memory cursor for this entity.
            AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
            if(rActingEntity) rActingEntity->SetMemoryTactics(mAbilitySelectionX, mAbilitySelectionY);

            //--Activate.
            BeginTargetingAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
            return;
        }
    }
}

///==================================== Ability Activation ========================================
void AdvCombat::BeginTargetingAbility(AdvCombatAbility *pAbility)
{
    ///--[Documentation]
    //--Given an ability, selects the ability to begin target selection.

    //--Ability does not exist for some reason.
    if(!pAbility)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
    }
    //--If the ability exists, but is unusuable, play the failure sound.
    else if(!pAbility->IsUsableNow())
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
    }
    //--If the ability opens a confirmation window without painting targets, do there here:
    else if(pAbility->ActivatesConfirmation())
    {
        ActivateConfirmationWindow();
        SetConfirmationText(pAbility->GetConfirmationText());
        SetConfirmationFunc(&AdvCombat::ConfirmExecuteAbility);
        rActiveAbility = pAbility;
        rActiveTargetCluster = NULL;
    }
    //--The ability exists and is usable, but does not need confirmation. Fire its target painting script.
    else
    {
        //--Execute the script.
        ClearTargetClusters();
        mTargetClusterCursor = 0;
        mTargetFlashTimer = 0;
        pAbility->CallCode(ACA_SCRIPT_CODE_PAINTTARGETS);

        //--If the script didn't paint any targets, fail.
        if(mTargetClusters->GetListSize() < 1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Script painted at least one target cluster, switch to targeting mode.
        else
        {
            //--Flag, storage.
            mIsSelectingTargets = true;
            rActiveAbility = pAbility;
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);

            //--Build prediction boxes.
            mIsClearingPredictionBoxes = false;
            BuildPredictionBoxes();
            RunPredictionBoxCalculations();
            SetClusterAsActive(mTargetClusterCursor);

            //--Move party.
            MovePartyByActiveCluster();

            //--Move enemies around.
            PositionEnemiesByTarget(rActiveTargetCluster->mrTargetList);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
}
void AdvCombat::BeginTargetingAbilityBySlot(int pX, int pY)
{
    ///--[Documentation]
    //--Given an X/Y position on the ability grid, selects the ability to begin target selection.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    //--Get the ability in question. If it doesn't exist, play the failure sound.
    AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(pX, pY);

    //--Run the above routine.
    BeginTargetingAbility(rAbility);
}

///===================================== Ability Execution ========================================
void AdvCombat::ExecuteAbility(AdvCombatAbility *pAbility, TargetCluster *pTargetCluster)
{
    ///--[Documentation]
    //--Executes the given ability on the given target cluster. The target cluster can be NULL for some
    //  abilities, but the ability should always be non-NULL.
    //--This enqueues the event that will hold the ability. We also need to check if any entities
    //  are going to "respond" to this event, which they may do before or after. They do this by
    //  enqueuing their own events.
    if(!pAbility)
    {
        return;
    }

    //--Ask the ability if it needs a target cluster. If it does, and the cluster is NULL, fail.
    if(pAbility->NeedsTargetCluster() && !pTargetCluster)
    {
        return;
    }

    //--Create a package around the ability.
    CombatEventPack *nPackage = (CombatEventPack *)starmemoryalloc(sizeof(CombatEventPack));
    nPackage->Initialize();
    nPackage->rAbility = pAbility;
    nPackage->rOriginator = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

    //--We need to clone the target cluster as it may get cleared if there are other events enqueued.
    if(pTargetCluster) nPackage->mTargetCluster = pTargetCluster->Clone();
    ClearTargetClusters();

    //--Enqueue. The rEventZeroPackage is used by abilities querying for responses.
    rEventZeroPackage = nPackage;
    EnqueueEvent(nPackage);

    //--Set this flag. This means the character who is acting has performed their action. This prevents
    //  the event queue from ending their turn if another event fired, such as a passive effect firing an event.
    mNoEventsEndsAction = true;

    //--Fire response handlers.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_QUEUEEVENT);

    //--Once all response events are assembled, sort the event queue by priority.
    mEventQueue->SortListUsing(&CompareCombatEventPacks);

    //--Clear the zero package.
    rEventZeroPackage = NULL;

    //--Player, if acting, is no longer acting.
    mIsPerformingPlayerUpdate = false;
}
void AdvCombat::ExecuteActiveAbility()
{
    //--Executes the ability stored in the rConfirmationAbility. Typically called from outside the class
    //  by a static function after the player presses the confirmation button.
    ExecuteAbility(rActiveAbility, rActiveTargetCluster);
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;
}
