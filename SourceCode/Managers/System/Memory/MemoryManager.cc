//--Base
#include "MemoryManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"

//#define MEMORY_DEBUG

///========================================== System ==============================================
Memory::MemoryManager()
{
    //--Static class. Not actually used.
}
Memory::~MemoryManager()
{
    //--Static class. Not actually used.
}
void Memory::DumpAllMemory()
{
    return;
    //--Can be used instead of the destructor, dumps out all pending memory regardless of whether
    //  it requires timed or manual deletion. This is similar to a manual flushing action.
    #if defined MEMORY_DEBUG
        DebugManager::PushPrint(true, "Memory: Dumping memory.\n");
    #endif

    GarbageEntry *rGarbageEntry = (GarbageEntry *)xGarbageList->PushIterator();
    while(rGarbageEntry)
    {
        #if defined MEMORY_DEBUG
            DebugManager::Print("Deleting object %p\n", rGarbageEntry->mGarbagePtr);
        #endif
        rGarbageEntry->DeleteObject();
        rGarbageEntry = (GarbageEntry *)xGarbageList->AutoIterate();
    }

    //--Once it's all gone, wipe the list.
    #if defined MEMORY_DEBUG
        DebugManager::Print("Clearing memory list.\n");
    #endif
    xGarbageList->ClearList();

    #if defined MEMORY_DEBUG
        DebugManager::PopPrint("Memory: Dumping memory completed.\n");
    #endif
}

///--[Private Statics]
//--Types of Deletion Cases
bool Memory::xAlwaysRequireManualPurging = false; //Set to true and the automatic cases below will not trigger.
int Memory::xMaxEntriesBeforeAutoPurge = 10000; //If the list has 10,000 entries, wipe the list. Set to 0 to stop entry auto-purges.
int Memory::xMaxTicksBeforeDeletion = TICKS_PER_SECOND * 1 * 1; //Auto-purges everything on the list every 1 minute(s). Set to 0 to stop timed auto-purges.

//--Garbage Handling
bool Memory::xIsFlaggedForFlush = false;
int Memory::xGarbageTimer = 0;
int Memory::xGarbageCheckTicks = TICKS_PER_SECOND * 120;
StarLinkedList *Memory::xGarbageList = new StarLinkedList(true);

///--[Public Statics]
//--Tracks the expected page size. Gets initialized during startup and may vary by operating system.
uint32_t Memory::xPageSize = 8192; //Default value, do not assume this is correct.

///===================================== Property Queries =========================================
int Memory::GetGarbageObjectsTotal()
{
    return xGarbageList->GetListSize();
}

///======================================== Manipulators ==========================================
void Memory::AddGarbage(void *pGarbagePtr, DeletionFunctionPtr pDeletionPtr)
{
    //--Overload: Assumes that manual deletion is off.
    Memory::AddGarbage(pGarbagePtr, pDeletionPtr, false);
}
void Memory::AddGarbage(void *pGarbagePtr, DeletionFunctionPtr pDeletionPtr, bool pRequiresManualDeletion)
{
    //--Adds a new piece of garbage to the list. Must have a legal pointer and legal deletion pointer.
    //  Passing DontDeleteThis() is not considered legal, and will bark an error.
    //--pRequiresManualDeletion will prevent the normal timer case from deleting objects. You will
    //  have to manually flush the list periodically (such as during a level transition).
    if(!pDeletionPtr || pDeletionPtr == &DontDeleteThis)
    {
        DebugManager::ForcePrint("Memory::AddGarbage: Error, attempted to pass illegal deletion function %p\n", pDeletionPtr);
        return;
    }

    //--If the garbage pointer itself is null, exit without a fuss.
    if(!pGarbagePtr) return;

    //--Otherwise, create the requested entry.
    SetMemoryData(__FILE__, __LINE__);
    GarbageEntry *nNewEntry = (GarbageEntry *)starmemoryalloc(sizeof(GarbageEntry));
    nNewEntry->mGarbagePtr = pGarbagePtr;
    nNewEntry->mTicksOnGarbageList = Memory::xGarbageTimer;
    nNewEntry->rDeletionFunction = pDeletionPtr;

    //--If manual deletion is on, set the timer to -1. This will prevent it from incrementing.
    if(pRequiresManualDeletion) nNewEntry->mTicksOnGarbageList = -1;

    //--Store it.
    xGarbageList->AddElement("X", nNewEntry, &GarbageEntry::DeleteThis);

    #if defined MEMORY_DEBUG
        DebugManager::ForcePrint("Received %p for deallocation.\n", nNewEntry->mGarbagePtr);
    #endif
}
void Memory::ManualFlush()
{
    //--Flags the object to flush memory. Doesn't actually flush the memory until the next tick interval.
    //  Tick() should *only* be called at the end of a logic tick when nothing is updating.
    xIsFlaggedForFlush = true;
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void Memory::Tick()
{
    //--Updates the memory manager. Every so often (xGarbageCheckTicks), the program will check to
    //  see if anything on the garbage list has been there for too long (xMaxTicksBeforeDeletion).
    //  Anything that has been there too long is purged unless that's flagged off.
    //--Alternatively, the manager will periodically purge the list if it gets too long (xMaxEntriesBeforeAutoPurge)
    //  which can also be disabled.
    if(xAlwaysRequireManualPurging || xIsFlaggedForFlush)
    {
        //--If flagged for a flush, do that here.
        if(xIsFlaggedForFlush)
        {
            #if defined MEMORY_DEBUG
                DebugManager::ForcePrint("Flagged for manual flush %i.\n", xGarbageList->GetListSize());
            #endif

            DumpAllMemory();
        }

        //--Unset the flag, don't finish the rest of the tick.
        xIsFlaggedForFlush = false;
        return;
    }

    //--Check the list size. Purge it if it's getting too long.
    if(xMaxEntriesBeforeAutoPurge > 0)
    {
        if(xMaxEntriesBeforeAutoPurge < xGarbageList->GetListSize())
        {
            //--Debug.
            #if defined MEMORY_DEBUG
                DebugManager::ForcePrint("List hit auto-purge. Dumping. %i.\n", xGarbageList->GetListSize());
            #endif

            //--Subroutine handles it.
            DumpAllMemory();
            return;
        }
    }

    //--Update the timer. Periodically remove anything over the timer.
    if(xMaxTicksBeforeDeletion > 0)
    {
        //--Timer.
        xGarbageTimer ++;

        //--To save time, don't check the entire list every tick. Just every xGarbageCheckTicks.
        if(xGarbageTimer % xGarbageCheckTicks == 0)
        {
            //--Debug.
            #if defined MEMORY_DEBUG
                DebugManager::ForcePrint("Scanning garbage list. Entries: %i\n", xGarbageList->GetListSize());
            #endif

            //--Scan the list.
            GarbageEntry *rGarbageEntry = (GarbageEntry *)xGarbageList->SetToHeadAndReturn();
            while(rGarbageEntry)
            {
                //--If the object's timer is -1, it means it will ignore timer deletion.
                if(rGarbageEntry->mTicksOnGarbageList != -1)
                {
                    //--Check if the object has outlived its usefulness.
                    int tTicksOnList = xGarbageTimer - rGarbageEntry->mTicksOnGarbageList;
                    if(tTicksOnList >= xMaxTicksBeforeDeletion)
                    {
                        #if defined MEMORY_DEBUG
                            DebugManager::ForcePrint(" Deallocating %p. Time was %i. Was on list for %i.\n", rGarbageEntry->mGarbagePtr, rGarbageEntry->mTicksOnGarbageList, tTicksOnList);
                        #endif
                        rGarbageEntry->DeleteObject();
                        xGarbageList->RemoveRandomPointerEntry();
                    }
                }

                //--Next.
                rGarbageEntry = (GarbageEntry *)xGarbageList->IncrementAndGetRandomPointerEntry();
            }

            //--Debug.
            #if defined MEMORY_DEBUG
                DebugManager::ForcePrint("List scan complete. Entries now at %i.\n", xGarbageList->GetListSize());
            #endif
        }
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
