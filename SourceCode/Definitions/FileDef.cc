//--Base
#include "FileDef.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///======================================== LoadingPack ===========================================
void LoadingPack::Initialize()
{
    //--System
    mFileName = NULL;
    mFilePath = NULL;
    mFilePathShort = NULL;

    //--Party Members
    for(int i = 0; i < LOADINGPACK_MAX_PARTY_SIZE; i ++)
    {
        mPartyNames[i] = NULL;
        mPartyLevels[i] = 0;
        rRenderImg[i] = NULL;
    }

    //--Location
    mMapLocation = NULL;

    //--Timestamp.
    mTimestamp = NULL;

    //--Mod Activity
    mActiveMods = new StarLinkedList(false);

    //--Chapter Completion
    for(int i = 0; i < LOADINGPACK_MAX_CHAPTER; i ++) mIsChapterComplete[i] = false;
}
void LoadingPack::Deallocate()
{
    free(mFileName);
    free(mFilePath);
    free(mFilePathShort);
    for(int i = 0; i < LOADINGPACK_MAX_PARTY_SIZE; i ++)
    {
        free(mPartyNames[i]);
    }
    free(mMapLocation);
    free(mTimestamp);
    delete mActiveMods;
}
void LoadingPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    LoadingPack *rPtr = (LoadingPack *)pPtr;
    rPtr->Deallocate();
    free(rPtr);
}
