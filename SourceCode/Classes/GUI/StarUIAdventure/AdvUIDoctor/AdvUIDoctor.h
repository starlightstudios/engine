///======================================== AdvUIDoctor ===========================================
//--Adventure Mode's Doctor Bag UI. This UI allows the player to heal their party members with the
//  doctor bag, assuming they have one.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUIDoctor : virtual public AdvUICommon
{
    private:
    ///--[System]
    ///--[Constants]
    static const int cVisTicks = 15;                    //Number of ticks to show/hide the object.
    static const int cHlpTicks = 15;                    //Number of ticks to show/hide the help screen.
    static const int cCurTicks = 7;                     //Number of ticks for the cursor to reposition when moved.
    static const int cHelpStringsTotal = 24;            //Number of unique StarlightStrings appearing on the help menu.

    ///--[Cursor]
    //--Indicator.
    int mCursor;

    //--Render positions.
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    ///--[Help Strings]
    StarlightString *mInstructionsStringA;
    StarlightString *mInstructionsStringB;
    StarlightString *mInstructionsStringC;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Statistics;

        //--Images
        StarBitmap *rFrame_Primary;
        StarBitmap *rHighlight_Standard;
        StarBitmap *rOverlay_BarFill;
        StarBitmap *rOverlay_BarFrame;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Symbol;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIDoctor();
    virtual ~AdvUIDoctor();
    virtual void Construct();

    //--Public Variables
    static int xDoctorVisTimer;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderTop(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


