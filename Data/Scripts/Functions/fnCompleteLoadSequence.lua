-- |[ ================================ fnCompleteLoadSequence() ================================ ]|
--After a loading sequence is completed, ends the loading sequence and stores its load count.
function fnCompleteLoadSequence()
	
    -- |[Activity Check]|
	--If there was no active load sequence, do nothing.
	if(gsLoadSequence == "None") then return end
	
    -- |[Execution]|
	--Order the LI to finish. This causes it to render one last time, and print reports if desired.
	LI_Finish()
	
    -- |[Store Load Count]|
	--Get the interrupt call count.
	local iInterruptCalls = LI_GetLastInterruptCount()
	
	--Send it to the OptionsManager.
	OM_SetOption("ExpectedLoad_" .. gsLoadSequence, iInterruptCalls)
	
	--Order the OptionsManager to write the load counter file. The program libraries sometimes vary in counts.
    -- This is always local to the executable, not the game directory.
	local sProgramLibrary = LM_GetSystemLibrary()
	if(sProgramLibrary == "Allegro") then
		OM_WriteLoadFile("LoadCountersAL.lua")
	else
		OM_WriteLoadFile("LoadCountersSDL.lua")
	end
	
    -- |[Finish Up]|
	--Reset this flag so further load sequences don't trip each other.
	gsLoadSequence = "None"
	
end