//--Base
#include "MonoUISkills.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "MonocerosMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
MonoUISkills::MonoUISkills()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_MONOUISKILLS;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MSKL");

    ///--[Visiblity]
    mVisibilityTimerMax = cMonoVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUISample ======= ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUISkills ====== ]
    ///--[Variables]
    mCurrentSkillsList = new StarLinkedList(false);

    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Help Strings
    AllocateHelpStrings(cxMonoHelpStrings);

    ///--[Verification Pack]
    //--Add the MonoImages structure to the verify list.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUISkills::~MonoUISkills()
{
    delete mCurrentSkillsList;
}
void MonoUISkills::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Skills UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Skills UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",             &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUISkills_Strings *MonoUISkills::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUISkills_Strings::MonoUISkills_Strings()
{
    //--Local name: "MonoUIBase"
    MonoUISkills::xrStringData = this;

    //--Strings
    str.mTextSkills          = InitializeString("MonoUISkills_mTextSkills");          //"Skills"
    str.mTextHelpMenu        = InitializeString("MonoUISkills_mTextHelpMenu");        //"Help Menu"
    str.mTextCharactersSkill = InitializeString("MonoUISkills_mTextCharactersSkill"); //"[sName]'s Skills"

    //--Help Sequence
    char tBuffer[100];
    for(int i = 0; i < 7; i ++)
    {
        sprintf(tBuffer, "MonoUISkills_mHelpText%02i", i);
        str.mTextHelp[i] = InitializeString(tBuffer);
    }

    //--Control Sequence
    str.mTextControlPurchaseSkills   = InitializeString("MonoUISkills_mControlPurchaseSkills");   //"[IMG0] Purchase Skills"
    str.mTextControlShowHelp         = InitializeString("MonoUISkills_mControlShowHelp");         //"[IMG0] Show Help"
    str.mTextControlScroll10         = InitializeString("MonoUISkills_mControlScroll10");         //"[IMG0] (Hold) Scroll x10"
    str.mTextControlEditProfiles     = InitializeString("MonoUISkills_mControlEditProfiles");     //"[IMG0] Edit Profiles"
    str.mTextControlShowSkillDetails = InitializeString("MonoUISkills_mControlShowSkillDetails"); //"[IMG0] Show Skill Details"
    str.mTextControlInspectSkills    = InitializeString("MonoUISkills_mControlInspectSkills");    //"[IMG0] Inspect Skills"
    str.mTextControlPrevChar         = InitializeString("MonoUISkills_mControlPrevChar");         //"[IMG0] or [IMG1]+[IMG2] Previous Character"
    str.mTextControlNextChar         = InitializeString("MonoUISkills_mControlNextChar");         //"[IMG0] or [IMG1]+[IMG2] Next Character"
    str.mTextControlMemorizeSkill    = InitializeString("MonoUISkills_mControlMemorizeSkill");    //"[IMG0] Memorize Skill"
    str.mTextControlDeleteProfile    = InitializeString("MonoUISkills_mControlDeleteProfile");    //"[IMG0] Delete Profile"
    str.mTextControlCancel           = InitializeString("MonoUISkills_mControlCancel");           //"[IMG0] Cancel"
}
MonoUISkills_Strings::~MonoUISkills_Strings()
{
    for(int i = 0; i < MONOUISKILLS_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUISkills_Strings::GetSize()
{
    return MONOUISKILLS_STRINGS_TOTAL;
}
void MonoUISkills_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUISKILLS_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
bool MonoUISkills::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_MONOUISKILLS) return true;
    if(pType == POINTER_TYPE_ADVUISKILLS)  return true;
    if(pType == POINTER_TYPE_STARUIPIECE)  return true;
    return false;
}

///======================================= Manipulators ===========================================
void MonoUISkills::TakeForeground()
{
    ///--[Base Call]
    MonocerosMenu::xIsMenuExecution = true;
    AdvUISkills::TakeForeground();
    MonocerosMenu::xIsMenuExecution = false;

    ///--[Refresh Skills Menu]
    //--Subroutine will order the active character (which should be the 0th one) to populate the skills
    //  menu for querying.
    PopulateSkillsBySelectedCharacter();
    RecomputeCursorPositions();

    //--"Toggle Tactics Grid" string, re-used for Activate.
    mShowTacticsString->SetString(xrStringData->str.mTextControlPurchaseSkills);
    mShowTacticsString->AllocateImages(1);
    mShowTacticsString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mShowTacticsString->CrossreferenceImages();

    ///--[Override Strings]
    //--"Show Help" string
    mShowHelpString->SetString(xrStringData->str.mTextControlShowHelp);
    mShowHelpString->CrossreferenceImages();

    //--"Scroll x10" string
    mScrollFastString->SetString(xrStringData->str.mTextControlScroll10);
    mScrollFastString->CrossreferenceImages();

    //--"Edit Profiles" string
    mEditProfilesString->SetString(xrStringData->str.mTextControlEditProfiles);
    mEditProfilesString->CrossreferenceImages();

    //--"Show Skill Details" string
    mSkillDetailsString->SetString(xrStringData->str.mTextControlShowSkillDetails);
    mSkillDetailsString->CrossreferenceImages();

    //--"Inspect Skills" string
    mInspectSkillsString->SetString(xrStringData->str.mTextControlInspectSkills);
    mInspectSkillsString->CrossreferenceImages();

    //--"Previous Character" string
    mCharacterLftString->SetString(xrStringData->str.mTextControlPrevChar);
    mCharacterLftString->CrossreferenceImages();

    //--"Next Character" string
    mCharacterRgtString->SetString(xrStringData->str.mTextControlNextChar);
    mCharacterRgtString->CrossreferenceImages();

    //--Memorize
    mConfirmPurchaseString->SetString(xrStringData->str.mTextControlMemorizeSkill);
    mConfirmPurchaseString->CrossreferenceImages();

    //--Confirm Delete
    mConfirmDeleteString->SetString(xrStringData->str.mTextControlDeleteProfile);
    mConfirmDeleteString->CrossreferenceImages();

    //--Cancel
    mCancelPurchaseString->SetString(xrStringData->str.mTextControlCancel);
    mCancelPurchaseString->CrossreferenceImages();
}
void MonoUISkills::ClearSkillsList()
{
    mCurrentSkillsList->ClearList();
}
void MonoUISkills::AddSkillReference(const char *pName)
{
    //--Locate the ability in the active entity, and add it to the query list.
    if(!pName) return;

    //--Active entity.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)DataLibrary::Fetch()->rActiveObject;
    if(!rEntity || !rEntity->IsOfType(POINTER_TYPE_ADVCOMBATENTITY)) return;

    //--Verify the list.
    StarLinkedList *rAbilityList = rEntity->GetAbilityList();
    if(!rAbilityList) return;

    //--Check for the ability.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementByName(pName);
    if(!rAbility) return;

    //--Add it to the reference list under the name it was listed with. This may not be the display name.
    mCurrentSkillsList->AddElementAsTail(pName, rAbility);
}

///======================================= Core Methods ===========================================
void MonoUISkills::PopulateSkillsBySelectedCharacter()
{
    ///--[Documentation]
    //--Takes the currently selected character and gets their combat script, then calls it to make
    //  them populate this menu with skills for querying.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Get script.
    const char *pCombatScript = rCurrentCharacter->GetResponsePath();
    if(!pCombatScript) return;

    //--Call it to populate. Script does the rest.
    DataLibrary::Fetch()->PushActiveEntity(rCurrentCharacter);
    LuaManager::Fetch()->ExecuteLuaFile(pCombatScript, 1, "N", 101.0f);
    DataLibrary::Fetch()->PopActiveEntity();
}
void MonoUISkills::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Execution]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Common lines.
    SetHelpString(i, xrStringData->str.mTextHelp[0]);
    SetHelpString(i, xrStringData->str.mTextHelp[1]);
    SetHelpString(i, xrStringData->str.mTextHelp[2]);
    SetHelpString(i, xrStringData->str.mTextHelp[3]);
    SetHelpString(i, xrStringData->str.mTextHelp[4]);
    SetHelpString(i, xrStringData->str.mTextHelp[5], 1, "F2");
    SetHelpString(i, xrStringData->str.mTextHelp[6], 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void MonoUISkills::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Modified cursor computation for the single-menu skill description checker.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = 0.0f;
    float cBot = 0.0f;

    //--Constants.
    float cSkillX = 507.0f;
    float cSkillY = 210.0f;
    float cSkillH =  43.0f;

    ///--[Resolve Skill Name]
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mCurrentSkillsList->GetElementBySlot(mJobCursor);
    if(rAbility)
    {
        //--Get name.
        const char *rDisplayName = rAbility->GetDisplayName();

        //--Position.
        cLft = cSkillX;
        cTop = cSkillY + (cSkillH * (mJobCursor - mJobSkip));
        cRgt = cLft + (MonoImages.rFontMainline->GetTextWidth(rDisplayName));
        cBot = cTop + cSkillH;
    }

    ///--[Finalize]
    //--Set.
    mHighlightPos.MoveTo(cLft, cTop, ADVMENU_SKILLS_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_SKILLS_CURSOR_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUISkills::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Player is selecting a "job". Monoceros doesn't have jobs like RoP does, and instead uses a single
    //  listing of abilities which can be inspected.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    bool tRecomputeCursorPosition = false;

    //--Get active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return true;

    ///--[Timers]
    //--Standards.
    StandardTimer(mVisibilityTimer,     0, mVisibilityTimerMax,                  true);
    StandardTimer(mPurchaseTimer,       0, ADVMENU_SKILLS_PURCHASE_TICKS,       (mMode == ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE));
    StandardTimer(mDetailsTimer,        0, ADVMENU_SKILLS_DETAILS_SWITCH_TICKS, (mIsShowingDetails));
    StandardTimer(mStringEntryTimer,    0, ADVMENU_SKILLS_STRING_VIS_TICKS,     (mMode == ADVMENU_SKILLS_MODE_PROFILES_NEW || mMode == ADVMENU_SKILLS_MODE_PROFILES_RENAME));
    StandardTimer(mTacticsGridTimer,    0, ADVMENU_SKILLS_TACTICS_VIS_TICKS,    (mIsUsingTacticsGrid));
    StandardTimer(mProfileConfirmTimer, 0, ADVMENU_SKILLS_VIS_TICKS,            (mIsProfileConfirm));
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks,                       (mIsShowingHelp));

    //--Arrow timer. Always runs.
    mArrowTimer ++;

    //--Character swapping timer.
    if(mIsSwappingCharacter)
    {
        if(mCharacterSwapTimer < ADVMENU_SKILLS_CHARACTER_SWAP_TICKS)
        {
            mCharacterSwapTimer ++;
        }
        else
        {
            mIsSwappingCharacter = false;
        }
    }

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[ ========= Mode Intercepts ========== ]
    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[Toggle Descriptions]
    if(rControlManager->IsFirstPress("F2"))
    {
        mShowAdvancedDescriptions = !mShowAdvancedDescriptions;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[ =========== Update Call ============ ]
    ///--[Cursor Handling]
    //--Up. Decrements job cursor. Wraps.
    if(AutoListUpDn(mJobCursor, mJobSkip, mCurrentSkillsList->GetListSize(), cxMonoScroll_JobBuf, cxMonoScroll_JobPage, true))
    {
        tRecomputeCursorPosition = true;
    }

    //--Cursor.
    if(tRecomputeCursorPosition)
    {
        RecomputeCursorPositions();
    }

    ///--[Switch Characters]
    //--Decrement the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Left")))
    {
        //--Does nothing if there is exactly one party member.
        if(AdvCombat::Fetch()->GetActivePartyCount() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return true;
        }
        //--Change it. Disables other controls while scrolling.
        else
        {
            //--Set.
            mIsSwappingCharacter = true;
            mCharacterSwapTimer = 0;
            mPrevCharacterSlot = mCharacterCursor;
            mCharacterCursor --;

            //--Reset flags.
            mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mJobCursor = 0;
            mJobSkip = 0;
            mSkillCursor = 0;
            mSkillSkip = 0;

            //--Wrap.
            if(mCharacterCursor < 0)
            {
                mCharacterCursor = AdvCombat::Fetch()->GetActivePartyCount() - 1;
            }

            //--Recompute cursor.
            PopulateSkillsBySelectedCharacter();
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return true;
        }
    }
    if((rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Right")))
    {
        //--Does nothing if there is exactly one party member.
        if(AdvCombat::Fetch()->GetActivePartyCount() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return true;
        }
        //--Change it. Disables other controls while scrolling.
        else
        {
            //--Set.
            mIsSwappingCharacter = true;
            mCharacterSwapTimer = 0;
            mPrevCharacterSlot = mCharacterCursor;
            mCharacterCursor ++;

            //--Reset flags.
            mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            mJobCursor = 0;
            mJobSkip = 0;
            mSkillCursor = 0;
            mSkillSkip = 0;

            //--Wrap.
            if(mCharacterCursor >= AdvCombat::Fetch()->GetActivePartyCount())
            {
                mCharacterCursor = 0;
            }

            //--Recompute cursor.
            PopulateSkillsBySelectedCharacter();
            RecomputeCursorPositions();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return true;
        }
    }

    ///--[Activate]
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Clearing.
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");

        //--Execute this script to handle switching to skills.
        mVisibilityTimer = 0;
        if(mCharacterCursor == 0)
            SetCloseCode("Close Skills Izana");
        else if(mCharacterCursor == 1)
            SetCloseCode("Close Skills Caelyn");
        else
            SetCloseCode("Close Skills Cyrano");
        return true;
    }

    ///--[Cancel]
    //--Cancel. Exits the skills menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    //--Handled update.
    return true;
}
void MonoUISkills::UpdateBackground()
{
    AdvUISkills::UpdateBackground();
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUISkills::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Render this UI.
    if(!mImagesReady) return;

    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    //--Option. Selects between fading and sliding.
    bool tUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Profiles Mode]
    //--If the profiles vis timer is maxed out, don't render anything from the base menu.
    if(mProfilesVisibilityTimer >= ADVMENU_SKILLS_PROFILE_VIS_TICKS)
    {
        RenderProfilesMode();
        return;
    }

    ///--[Timer]
    //--Resolve timer offset.
    float cAlpha = EasingFunction::QuadraticInOut((mVisibilityTimer - mProfilesVisibilityTimer), (float)ADVMENU_SKILLS_VIS_TICKS);

    //--Arrow oscillation.
    float cArrowOscillate = sinf((mArrowTimer / (float)ADVMENU_SKILLS_ARROW_OSCILLATE_TICKS) * 3.1415926f * 2.0f) * ADVMENU_SKILLS_ARROW_OSCILLATE_DISTANCE;

    ///--[Setup]
    //--If the UI is using translate logic, set the alpha to 1.0f.
    if(!tUseFade) cAlpha = 1.0f;

    //--Reset mixer.
    StarlightColor::ClearMixer();

    //--Resolve selected character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    ///============================= Left Block ===============================
    //--Offset.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);
    //HandleFadeOrTranslate(tUseFade, cLftOffset * 1.0f, 0.0f, cAlpha);

    //--Arrow.
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1) MonoImages.rArrowLft->Draw(cArrowOscillate * -1.0f, 0.0f);

    ///--[Normal Portrait]
    //--Render the character in question.
    if(!mIsSwappingCharacter)
    {
        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rCurrentCharacter->GetUIRenderPosition(ACE_UI_INDEX_SKILLS);

        //--Position.
        StarBitmap *rCharacterCombatPortrait = rCurrentCharacter->GetCombatPortrait();
        if(rCharacterCombatPortrait) rCharacterCombatPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);
    }
    ///--[Swapping Characters]
    else
    {
        //--Percentage.
        float cScrollPercent = EasingFunction::QuadraticInOut(mCharacterSwapTimer, ADVMENU_SKILLS_CHARACTER_SWAP_TICKS);

        //--Get the previous entity.
        AdvCombatEntity *rPreviousEntity = AdvCombat::Fetch()->GetActiveMemberI(mPrevCharacterSlot);
        if(rPreviousEntity)
        {
            //--Stencil and position.
            TwoDimensionRealPoint tRenderDim = rPreviousEntity->GetUIRenderPosition(ACE_UI_INDEX_SKILLS);

            //--Get portrait.
            StarBitmap *rPortrait = rPreviousEntity->GetCombatPortrait();
            if(rPortrait)
            {
                float cXOffset = VIRTUAL_CANVAS_X * (cScrollPercent) * -1.0f;
                rPortrait->Draw(tRenderDim.mXCenter + cXOffset, tRenderDim.mYCenter);
            }
        }

        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rCurrentCharacter->GetUIRenderPosition(ACE_UI_INDEX_SKILLS);

        //--Current entity renders above.
        StarBitmap *rCharacterCombatPortrait = rCurrentCharacter->GetCombatPortrait();
        float cXOffset = VIRTUAL_CANVAS_X * (1.0f - cScrollPercent) * -1.0f;
        if(rCharacterCombatPortrait) rCharacterCombatPortrait->Draw(tRenderDim.mXCenter + cXOffset, tRenderDim.mYCenter);
    }

    //--Clean.
    AutoPieceClose();
    //HandleFadeOrTranslate(tUseFade, cLftOffset * -1.0f, 0.0f, cAlpha);

    ///============================= Top Block ================================
    //--Offset.
    AutoPieceOpen(DIR_UP, pVisAlpha);
    //HandleFadeOrTranslate(tUseFade, 0.0f, cTopOffset * 1.0f, cAlpha);

    //--Header.
    MonoImages.rOverlayTitleShade->Draw();

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cAlpha);
    MonoImages.rFontDoubleHeader->DrawText(726.0f, 33.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSkills);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Sub-handlers.
    RenderSkillsListing(cAlpha);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Strings, top left.
    mShowHelpString->DrawText(0.0f, AM_MAINLINE_H * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--Strings, top right.
    mScrollFastString->DrawText  (VIRTUAL_CANVAS_X, AM_MAINLINE_H * 0.0f, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--Clean.
    AutoPieceClose();
    //HandleFadeOrTranslate(tUseFade, 0.0f, cTopOffset * -1.0f, cAlpha);

    ///============================ Right Block ===============================
    //--Offset.
    AutoPieceOpen(DIR_RIGHT, pVisAlpha);
    //HandleFadeOrTranslate(tUseFade, cRgtOffset * 1.0f, 0.0f, cAlpha);

    //--Arrow.
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1) MonoImages.rArrowRgt->Draw(cArrowOscillate * 1.0f, 0.0f);

    //--Clean.
    AutoPieceClose();
    //HandleFadeOrTranslate(tUseFade, cRgtOffset * -1.0f, 0.0f, cAlpha);

    ///============================ Bottom Block ==============================
    //--Offset.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);
    //HandleFadeOrTranslate(tUseFade, 0.0f, cBotOffset * 1.0f, cAlpha);

    //--Description. Always renders.
    RenderSkillsDescription(cAlpha);

    //--Strings, bottom left.
    float cYPos = VIRTUAL_CANVAS_Y - AM_HELP_SYMBOL_TEXT_H;
    //mInspectSkillsString->DrawText(0.0f, cYPos - (AM_MAINLINE_H * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    //mSkillDetailsString ->DrawText(0.0f, cYPos - (AM_MAINLINE_H * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    mCharacterLftString ->DrawText(0.0f, cYPos - (AM_MAINLINE_H * 0.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    mShowTacticsString -> DrawText(0.0f, cYPos - (AM_MAINLINE_H * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--Strings, bottom right.
    mCharacterRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos - (AM_MAINLINE_H * 0.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--Clean.
    AutoPieceClose();
    //HandleFadeOrTranslate(tUseFade, 0.0f, cBotOffset * -1.0f, cAlpha);

    ///============================ No Blocking ===============================
    //--Details overlay.
    RenderSkillsDetails();

    ///========================== Help Rendering ==============================
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void MonoUISkills::RenderSkillsListing(float pAlpha)
{
    ///--[Documentation]
    //--Different from the basic AdventureMenu, this menu only shows the skills and not job or JP information.
    //  Skills are also generically held by the class rather than being queries from the character, as they may
    //  be "Unlocked" at different points.

    //--Frame.
    MonoImages.rFrameSkills->Draw();
    MonoImages.rOverlayCharNameBack->Draw();

    //--Get name of active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;
    const char *rDisplayName = rCurrentCharacter->GetDisplayName();

    //--Header Text.
    char *tReplacedName = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mTextCharactersSkill, 1, "[sName]", rDisplayName);
    mMonoTitleColor.SetAsMixerAlpha(pAlpha);
    MonoImages.rFontHeader->DrawTextArgs(621.0f, 144.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tReplacedName);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    free(tReplacedName);

    ///--[Skill Listing]
    //--Variables.
    float cSkillX = 465.0f;
    float cSkillY = 206.0f;
    float cSkillH =  43.0f;

    //--Render backings. This is done such that the cursor renders over the backings but under the text.
    //  Only cxMonoScroll_JobPage are rendered at once.
    int tListSize = mCurrentSkillsList->GetListSize();
    if(tListSize > 11) tListSize = 11;
    for(int i = 0; i < tListSize; i ++)
    {
        MonoImages.rOverlaySkillBack->Draw(0.0f, i * cSkillH);
    }

    //--Scrollbar. Only used if larger than the page size.
    if(mCurrentSkillsList->GetListSize() > cxMonoScroll_JobPage)
    {
        StandardRenderScrollbar(mJobSkip, cxMonoScroll_JobPage, mCurrentSkillsList->GetListSize() - cxMonoScroll_JobPage, 210.0f, 468.0f, false, MonoImages.rScrollbarScroller, MonoImages.rScrollbarBack);
    }

    //--Cursor.
    MonoImages.rOverlaySkillHighlight->Draw(0.0f, mHighlightPos.mYCur - MonoImages.rOverlaySkillHighlight->GetYOffset() - 12.0f);

    //--Setup.
    int i = 0;
    int tRenders = 0;
    float tYPosition = cSkillY;
    mMonoTitleColor.SetAsMixerAlpha(pAlpha);

    //--Iterate.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mCurrentSkillsList->PushIterator();
    while(rAbility)
    {
        //--Below the skip, don't render.
        if(i < mJobSkip)
        {
            i ++;
            rAbility = (AdvCombatAbility *)mCurrentSkillsList->AutoIterate();
            continue;
        }

        //--Render the name.
        MonoImages.rFontMainline->DrawText(cSkillX, tYPosition, 0, 1.0f, rAbility->GetDisplayName());

        //--We hit the render cap, stop.
        if(tRenders >= cxMonoScroll_JobPage-1)
        {
            mCurrentSkillsList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        tRenders ++;
        tYPosition = tYPosition + cSkillH;
        rAbility = (AdvCombatAbility *)mCurrentSkillsList->AutoIterate();
    }

    ///--[Clean]
    StarlightColor::ClearMixer();

    ///--[Highlight]
    //RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, Images.StandardUI.rHighlight);
}
void MonoUISkills::RenderSkillsDescription(float pAlpha)
{
    ///--[Documentation]
    //--Given a skill, renders its description.
    float cTxtL = 852.0f;
    float cTxtT = 248.0f;
    float cTxtH =  29.0f;

    ///--[Static Parts]
    //--Frame.
    MonoImages.rFrameDescription->Draw();

    ///--[Resolve]
    //--Active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Resolve ability from list.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mCurrentSkillsList->GetElementBySlot(mJobCursor);
    if(!rAbility) return;

    ///--[Common render]
    //--Name.
    mMonoParagraphColor.SetAsMixerAlpha(pAlpha);
    MonoImages.rFontHeader->DrawText(1031.0f, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rAbility->GetDisplayName());

    ///--[Simple Render]
    //--The "Simplified" description is the one used for the skills menu. The advanced render is used for
    //  the combat description, if needed.
    for(int i = 0; i < cxMono_MaxDescLines; i ++)
    {
        //--Get line.
        StarlightString *rDescriptionLine = rAbility->GetSimplifiedDescriptionLine(i);
        if(!rDescriptionLine) break;

        //--Render.
        rDescriptionLine->DrawText(cTxtL, cTxtT + (cTxtH * i), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontDescription);
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void MonoUISkills::HookToLuaState(lua_State *pLuaState)
{
    /* MonoUISkills_SetProperty("Clear Skills")
       MonoUISkills_SetProperty("Add Skill", sSkillName) */
    lua_register(pLuaState, "MonoUISkills_SetProperty", &Hook_MonoUISkills_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_MonoUISkills_SetProperty(lua_State *L)
{
    ///--[Argument List]
    //--[Dynamic]
    //MonoUISkills_SetProperty("Clear Skills")
    //MonoUISkills_SetProperty("Add Skill", sSkillName)

    ///--[Arguments]
    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("MonoUISkills_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--No static types.

    ///--[Dynamic]
    //--Get the object using the static resolver.
    int tErrorCode;
    StarUIPiece *rUIPiece = AdventureMenu::ResolveUIPiece(tErrorCode, "Skills", POINTER_TYPE_MONOUISKILLS);
    if(!rUIPiece)
    {
        fprintf(stderr, "MonoUISkills_SetProperty: Error, unable to resolve UI piece. Code: %i\n", tErrorCode);
        return 0;
    }

    //--Cast.
    MonoUISkills *rSkillsUI = dynamic_cast<MonoUISkills *>(rUIPiece);

    ///--[Execution]
    //--Clears the existing skill list.
    if(!strcasecmp(rSwitchType, "Clear Skills") && tArgs >= 1)
    {
        rSkillsUI->ClearSkillsList();
    }
    //--Clears the existing skill list.
    else if(!strcasecmp(rSwitchType, "Add Skill") && tArgs >= 2)
    {
        rSkillsUI->AddSkillReference(lua_tostring(L, 2));
    }
    ///--[Error Case]
    else
    {
        LuaPropertyError("MonoUISkills_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
