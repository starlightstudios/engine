///===================================== AdventureInventory ========================================
//--Inventory used for Adventure Mode. Should not be confused with the other inventory which is used for
//  Classic/Enhanced/Corrupter Modes, this one does not use the same basic parts!

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Adamantite and Catalyst types. Defined in several spots.
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Catalyst types.
#ifndef _CATALYST_TYPES_
#define _CATALYST_TYPES_
#define CATALYST_HEALTH 0
#define CATALYST_ATTACK 1
#define CATALYST_INITIATIVE 2
#define CATALYST_DODGE 3
#define CATALYST_ACCURACY 4
#define CATALYST_SKILL 5
#define CATALYST_TOTAL 6
#endif

//--Buffs due to Catalysts.
#define CATALYST_NEEDED_HEALTH 5
#define CATALYST_BUFF_HEALTH 10
#define CATALYST_NEEDED_ATTACK 3
#define CATALYST_BUFF_ATTACK 3
#define CATALYST_NEEDED_INITIATIVE 3
#define CATALYST_BUFF_INITIATIVE 4
#define CATALYST_NEEDED_DODGE 3
#define CATALYST_BUFF_DODGE 3
#define CATALYST_NEEDED_ACCURACY 3
#define CATALYST_BUFF_ACCURACY 3
#define CATALYST_NEEDED_SKILL 6

//--Sort Criteria
#define AINV_SORT_CRITERIA_NAME 0
#define AINV_SORT_CRITERIA_TYPE 1
#define AINV_SORT_CRITERIA_VALUE 2
#define AINV_SORT_CRITERIA_MAX 3

//--Shop UI
#define AINV_MAX_BUYBACK 20

///========================================== Classes =============================================
class AdventureInventory : public RootObject
{
    private:
    //--System
    int mCurrentSort;
    bool mWasAnythingCreated;

    //--Paths
    char *mGemNameResolvePath;

    //--Crafting Materials
    int mPlatina;
    int mCraftingItemCounts[CRAFT_ADAMANTITE_TOTAL];

    //--Catalysts
    int mCatalystCounts[CATALYST_TOTAL];
    int mCatalystCountsLocalCur[CATALYST_TOTAL];
    int mCatalystCountsLocalTotal[CATALYST_TOTAL];

    //--Upgrading
    bool mIsUpgradeable;

    //--Storage List
    StarLinkedList *mItemList; //AdventureItem *, master
    StarLinkedList *mExtendedItemList; //AdventureItem *, reference
    StarLinkedList *mGemList; //AdventureItem *, reference
    StarLinkedList *mGemMergeList; //AdventureItem *, reference

    //--Doctor Bag
    bool mIsDoctorBagEnabled;
    float mDoctorBagChargeRate;
    float mDoctorBagPotency;
    int mDoctorBagCharges;
    int mDoctorBagChargesMax;

    //--Shop Buyback
    StarLinkedList *mShopBuybackList; //ShopInventoryPack *, master

    //--Gems
    AdventureItem *rMarkedEquipment;
    AdventureItem *rMasterGem;
    AdventureItem *rSocketingItem;

    protected:

    public:
    //--System
    AdventureInventory();
    virtual ~AdventureInventory();

    //--Public Variables
    void *rLastReggedItem;
    bool mBlockStackingOnce;
    static void *xrDummyUnequipItem;
    static void *xrDummyGemsItem;
    static AdventureItem *xUpgradeItem;
    static bool xSortListInReverse;
    static char *xLastItemImagePath;
    static StarBitmap *xrLastRegisteredIcon;
    static bool xIsRegisteringItemsToShop;

    //--Property Queries
    const char *GetGemNameResolvePath();
    int GetItemCount();
    int GetCountOf(const char *pName);
    int GetCountOfEquip(const char *pName);
    int GetPlatina();
    int GetCraftingCount(int pSlot);
    int GetDoctorBagCharges();
    int GetDoctorBagChargesMax();
    float GetDoctorBagChargeRate();
    float GetDoctorBagPotency();

    //--Manipulators
    void SetGemNameResolvePath(const char *pPath);
    bool RegisterItem(AdventureItem *pItem);
    void RegisterAdamantite(AdventureItem *pItem, bool pDeleteItem);
    void SetPlatina(int pAmount);
    void SetCraftingMaterial(int pSlot, int pAmount);
    bool RemoveItem(const char *pName);
    void SetDoctorBagCharges(int pAmount);
    void SetDoctorBagMaxCharges(int pAmount);
    void SetDoctorBagChargeRate(float pRate);
    void SetDoctorBagPotency(float pPotency);
    void MarkLastItemQuantity(int pQuantity);
    void MarkLastItemAsMasterGem();
    void MergeLastItemWithMasterGem();
    void ClearMasterGem();
    void MarkLastItemAsSocketItem();
    void SocketLastItemInSocketItem();
    void ClearSocketItem();
    void MarkLastItemAsEquipItem();
    void ClearLastEquipItem();
    void ClearBuyback();

    //--Core Methods
    void Clear();
    int IsItemEquipped(const char *pItemName);
    int IsItemEquippedBy(const char *pCharacter, const char *pItemName);
    bool IsAnyItemEquippedByIn(const char *pCharacter, const char *pSlotName);
    void BuildEquippableList(AdvCombatEntity *pEntity, const char *pEquipName, StarLinkedList *pList);
    void BuildExtendedItemList();
    void BuildGemList();
    void BuildGemListNoEquip();
    StarLinkedList *BuildGemMergeList(AdventureItem *pGem);
    AdventureItem *LocateItemByItemID(uint32_t pSearchID);
    void StackItems();

    ///--Catalysts
    int GetCatalystCount(int pType);
    int GetCatalystLocalCur(int pType);
    int GetCatalystLocalMax(int pType);
    void AddCatalyst(int pType);
    void SetCatalystCount(int pType, int pAmount);
    void SetCatalystLocalCur(int pType, int pAmount);
    void SetCatalystLocalMax(int pType, int pAmount);
    int ComputeCatalystBonus(int pType);
    void SetCatalystCountsByGlobalString(const char *pString);

    ///--Debug
    void Debug_AwardPlatina();
    void Debug_AwardCrafting();

    ///--Gems
    void DisassembleGem(AdventureItem *pPtr);
    static int ComputeGemUpgradePlatinaCost(int pTier);
    static int ComputeMonocerosGemUpgradePlatinaCost(int pTier);
    static int ComputeGemUpgradeCost(int pTier, int pAdamantiteType);

    ///--Saving
    void WriteToBuffer(StarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *fInfile);

    ///--Sorting
    int GetInternalSortFlag();
    void SetInternalSort(int pFlag);
    void IncrementInternalSort();
    void SortItemListByInternalCriteria();
    void SortListByCriteria(StarLinkedList *pList, int pCriteriaFlag, bool pHighestFirst);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    StarLinkedList *GetExtendedItemList();
    StarLinkedList *GetItemList();
    StarLinkedList *GetGemList();
    StarLinkedList *GetGemMergeList();
    StarLinkedList *GetBuybackList();
    AdventureItem *GetItem(const char *pName);
    AdventureItem *LiberateItemS(const char *pName);
    AdventureItem *GetLastRegisteredItem();
    AdventureItem *GetMarkedEquipment();
    AdventureItem *GetMasterGem();
    void LiberateItemP(void *pPtr);

    //--Static Functions
    static AdventureInventory *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdInv_CreateItem(lua_State *L);
int Hook_AdInv_GetProperty(lua_State *L);
int Hook_AdInv_SetProperty(lua_State *L);
int Hook_AdInv_PushItem(lua_State *L);
int Hook_AdInv_PushItemI(lua_State *L);
