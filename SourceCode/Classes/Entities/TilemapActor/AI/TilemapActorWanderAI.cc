//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
//--Definitions
#include "Global.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "EntityManager.h"

///--[Debug]
//#define ENEMY_WANDER_DEBUG
#ifdef ENEMY_WANDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
void TilemapActor::HandleWanderAI()
{
    ///--[Documentation and Setup]
    //--Handles the wandering part of the AI. Wandering means that the AI doesn't have a target to chase
    //  and is either standing still, patrolling, or randomly moving around.
    bool tIsIgnoringPlayer = IsIgnoringPlayer();
    bool tIsPlayerInSight = IsPlayerInSightCone(TA_USE_COMPUTED_VIEWDIST);
    float tRangeToPlayer = GetRangeToPlayer();
    float cChaseDistance = GetChaseDistance();

    ///--[Lock Facing]
    //--Entity always faces the same direction.
    if(mEnemyLockFacing != -1)
    {
        //--Range check. Chase the player if close enough.
        if(tRangeToPlayer <= cChaseDistance && !tIsIgnoringPlayer && tIsPlayerInSight)
        {
            if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
        }
        //--Face the given direction.
        else
        {
            mFacing = mEnemyLockFacing;
            mTrueFacing = mFacing;
            SetLeashingPoint(mTrueX, mTrueY);
        }
        DebugPrint("Lock Facing.\n");
        return;
    }

    ///--[Patrolling]
    //--Patrol case. Entity walks between points. Ignored if currently following someone.
    if(mIsPatrolling && mPatrolNodesTotal > 0 && !mFollowTarget)
    {
        //--Debug.
        DebugPrint("Patrolling.\n");

        ///--[Activate Chase]
        //--Range check. Chase the player if close enough.
        if(tRangeToPlayer <= cChaseDistance && !tIsIgnoringPlayer && tIsPlayerInSight)
        {
            mDropPatrolTicks = 0;
            if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
        }
        ///--[Pathing]
        //--Not close enough, path to the patrol node in question.
        else
        {
            //--Increment global. It gets reset at the start of each logic tick.
            TilemapActor::xActorsPathingThisTick ++;

            //--Drop patrol. This timer may randomly get set if other AIs are taking too long to run their pathing.
            if(mDropPatrolTicks > 0)
            {
                mDropPatrolTicks --;
                mTrueFacing = mFacing;
                return;
            }

            //--If our AI reaches the point of running pathing, but it's past the warning time in the tick, add a random
            //  wait time. The AI will still chase the player if spotted but won't run patrol checks until time has freed up.
            float tCurrentTime = GetGameTime();
            if(tCurrentTime >= Global::Shared()->gTickWarnTime)
            {
                //fprintf(stderr, "Dropping pathing.\n Actors pathing this tick: %i\n Tick warning time: %f\n Tick current time: %f\n", TilemapActor::xActorsPathingThisTick, Global::Shared()->gTickWarnTime, tCurrentTime);
                mDropPatrolTicks = (rand() % 59) + 1;
                mTrueFacing = mFacing;
                return;
            }

            //--Advanced Pathing:
            if(mIsAdvancedPatrol)
            {
                DebugPrint("Advanced patrol.\n");
                HandleAdvancedPathing();
            }
            //--Smart Pathing:
            else if(mIsSmartPatrol)
            {
                DebugPrint("Smart path.\n");
                HandleSmartPathing();
            }
            //--Standard Pathing:
            else
            {
                DebugPrint("Standard path.\n");
                HandleStandardPathing();
            }

            //--If after running the pathing, the timer exceeds the warning time, add the random offset for next tick.
            tCurrentTime = GetGameTime();
            if(tCurrentTime >= Global::Shared()->gTickWarnTime)
            {
                mDropPatrolTicks = (rand() % 59) + 1;
                mTrueFacing = mFacing;
            }
        }

        //--Store the true facing.
        mTrueFacing = mFacing;

        //--Stop the update.
        return;
    }
    ///--[Following a Leader]
    //--If following a target:
    else if(mFollowTarget)
    {
        //--If the follow target does not exist, stop here.
        RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowTarget);
        if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;

            //--If the entity is currently knocked out, stop following them.
            if(rActor->mIsDying)
            {
                ResetString(mFollowTarget, NULL);
            }
            //--Otherwise, follow.
            else
            {
                //--Register to this object, which will return a TDR indicating where we should path to.
                TwoDimensionReal tFollowPosition = rActor->GetFollowPosition(mLocalName);

                //--How fast we move. If the distance is close enough, don't move until it's higher.
                float cSpeedFactor = 0.40f;
                float tDistance = GetPlanarDistance(mTrueX, mTrueY, tFollowPosition.mXCenter, tFollowPosition.mYCenter);
                if(tDistance > mMoveSpeed * 0.70f * 5.0f)
                {
                    HandleMoveTo(tFollowPosition.mXCenter, tFollowPosition.mYCenter, mMoveSpeed * cSpeedFactor);
                    mTrueFacing = mFacing;
                }

                //--Reposition our leash point to where we are.
                SetLeashingPoint(mTrueX, mTrueY);

                //--Stop the update so the normal wander code doesn't occur. Followers don't spot the player.
                DebugPrint("Following.\n");
                return;
            }
        }
        //--If the entity died, unset the flags associated. It's not like they're coming back. This will enable
        //  wandering mode.
        else
        {
            ResetString(mFollowTarget, NULL);
        }
    }
    ///--[Wander Around Point]
    //--Normal Case: Either no special cases, or the special cases have an error (such as the follow target not existing).
    else
    {
        //--First, check if the entity needs a new wandering point.
        if(mMustRollWanderPoint)
        {
            //--Flag.
            mMustRollWanderPoint = false;

            //--Roll an angle and distance.
            float tAngleRoll = (rand() % 360) * 3.1415926f / 180.0f;
            float tDistanceRoll = 25.0f + (((rand() % 100) / 100.0f) * 15.0f);
            mWanderX = mLeashX + cosf(tAngleRoll) * tDistanceRoll;
            mWanderY = mLeashY + sinf(tAngleRoll) * tDistanceRoll;
        }

        ///--[Spotting Player in Viewcone]
        //--Range check. Chase the player if close enough.
        if(tRangeToPlayer <= cChaseDistance && !tIsIgnoringPlayer && tIsPlayerInSight)
        {
            if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
        }
        ///--[Wander Execution]
        //--Not close enough. Wander.
        else
        {
            //--Wander towards the destination. This can be done by emulating an ActorInstruction. It will
            //  return true when we're at the location.
            if(HandleMoveTo(mWanderX, mWanderY, mMoveSpeed * 0.40f))
            {
                StopMoving();
                mAIState = TA_AI_WAIT;
                mWaitTimer = 0;
                mWaitTimerMax = rand() % 30 + 150;
            }
            //--Not at the location yet.
            else
            {

            }

            //--Store the true facing.
            mTrueFacing = mFacing;
        }
    }
    DebugPrint("Exited, likely wandering.\n");
}
