///======================================== ActorEvent ============================================
//--Event that commands a TilemapActor to do something. Depending on the case, will complete instantly, or else monitor
//  the subject until they have completed their action.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootEvent.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define AE_TYPE_NONE 0
#define AE_TYPE_FACE 1
#define AE_TYPE_TELEPORTTO 2
#define AE_TYPE_MOVETO 3
#define AE_TYPE_MOVEFIXED 4
#define AE_TYPE_STOPMOVEMENT 5
#define AE_TYPE_CHANGESPECIALFRAME 6
#define AE_TYPE_FLASHWHITE 7
#define AE_TYPE_FLASHWHITE_QUICKLY 8
#define AE_TYPE_RESETMOVETIMER 9
#define AE_TYPE_AUTODESPAWN 10
#define AE_TYPE_MOVEWHILEFACING 11
#define AE_TYPE_PARTYMERGE 12
#define AE_TYPE_PARTYAUTOFOLD 13
#define AE_TYPE_DELAYMOVETIMER 14
#define AE_TYPE_CHANGEYOFFSET 15
#define AE_TYPE_DONOTHING 16
#define AE_TYPE_JUMPTO 17
#define AE_TYPE_CHANGESPECIALANIM 18
#define AE_TYPE_FACEPOS 19

///========================================== Classes =============================================
class ActorEvent : public RootEvent
{
    private:

    protected:
    //--System
    bool mIsComplete;
    int mInstructionType;

    //--Target
    uint32_t mTargetID;

    //--Data Storage
    union
    {
        struct
        {
            float mAmountX;
            float mAmountY;
            char mFacingTarget[32];
        }Face;
        struct
        {
            int mTargetX;
            int mTargetY;
            int mTargetZ;
        }TeleportTo;
        struct
        {
            int mTargetX;
            int mTargetY;
            float mSpeedModifier;
        }MoveTo;
        struct
        {
            int mTargetX;
            int mTargetY;
            int mFaceX;
            int mFaceY;
            float mSpeedModifier;
        }MoveToFacing;
        struct
        {
            bool mInitialized;
            float mAmountX;
            float mAmountY;
        }MoveFixed;
        struct
        {
            char mFrameName[32];
        }SpecialFrame;
        struct
        {
            bool mIsRunning;
            char mFlashwhiteName[32];
        }Flashwhite;
        struct
        {
            int mTimer;
        }PartyMerge;
        struct
        {
            int mTimer;
        }DelayMoveTimer;
        struct
        {
            float mOffset;
        }ChangeYOffset;
        struct
        {
            int mTimer;
        }DoNothing;
        struct
        {
            bool mInittedJump;
            int mTimer;
            int mTimerMax;
            int mStartX;
            int mStartY;
            int mTargetX;
            int mTargetY;
        }JumpTo;
        struct
        {
            char mAnimName[32];
        }ChangeAnim;
        struct
        {
            float mXPos;
            float mYPos;
        }FacePos;
    }Data;

    public:
    //--System
    ActorEvent();
    virtual ~ActorEvent();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    virtual bool DisablesTimerAcceleration();
    uint32_t GetSubjectID();

    //--Manipulators
    void SetTarget(uint32_t pID);
    void SetTargetByName(const char *pName);
    void SetFaceTowards(int pX, int pY);
    void SetFaceTowards(const char *pTargetName);
    void SetFaceTowardsPos(float pX, float pY);
    void SetTeleportTo(int pX, int pY);
    void SetTeleportTo(int pX, int pY, int pZ);
    void SetMoveTo(int pX, int pY);
    void SetMoveTo(int pX, int pY, float pSpeed);
    void SetMoveToFacing(int pX, int pY, int pFaceX, int pFaceY);
    void SetMoveToFacing(int pX, int pY, int pFaceX, int pFaceY, float pSpeed);
    void SetMoveAmount(int pX, int pY);
    void SetStopMove();
    void SetChangeSpecialFrame(const char *pFrameName);
    void SetFlashwhite(const char *pSpecialFrame);
    void SetFlashwhiteQuickly(const char *pSpecialFrame);
    void SetResetMoveTimer();
    void SetAutoDespawn();
    void SetPartyMerge();
    void SetPartyAutofold();
    void SetDelayMoveTimer(int pTicks);
    void SetChangeYOffset(float pOffset);
    void SetDoNothing(int pTicks);
    void SetJumpTo(int pX, int pY, int pTicks);
    void SetChangeSpecialAnim(const char *pAnimName);

    //--Core Methods
    virtual bool IsComplete();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    virtual void HandleDiagnostics();

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_ActorEvent_SetProperty(lua_State *L);
int Hook_ActorEvent_GetProperty(lua_State *L);
