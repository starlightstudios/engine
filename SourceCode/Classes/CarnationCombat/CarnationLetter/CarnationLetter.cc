//--Base
#include "CarnationLetter.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
CarnationLetter::CarnationLetter()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ==== CarnationLetter ===== ]
    ///--[System]
    mHasFiredApplications = false;
    mLetter = '\0';

    ///--[Applications]
    mApplicationPacks = new StarLinkedList(true);
}
CarnationLetter::~CarnationLetter()
{
    ///--[System]
    ///--[Applications]
    delete mApplicationPacks;
}

///===================================== Property Queries =========================================
bool CarnationLetter::HasFiredApplications()
{
    return mHasFiredApplications;
}
uint32_t CarnationLetter::GetLetter()
{
    return mLetter;
}

///======================================= Manipulators ===========================================
void CarnationLetter::SetFiredApplications()
{
    mHasFiredApplications = true;
}
void CarnationLetter::SetLetter(uint32_t pLetter)
{
    mLetter = pLetter;
}
void CarnationLetter::RegisterApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString)
{
    //--Note: Target can be zero (sometimes used for 'announcement' abilities) but Originator shouldn't be.
    if(!pString) return;
    CLAppPack *nPackage = (CLAppPack *)starmemoryalloc(sizeof(CLAppPack));
    nPackage->Initialize();
    nPackage->mOriginatorID = pOriginatorID;
    nPackage->mTargetID = pTargetID;
    ResetString(nPackage->mString, pString);
    mApplicationPacks->AddElementAsTail("X", nPackage, &CLAppPack::DeleteThis);
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
StarLinkedList *CarnationLetter::GetApplicationList()
{
    return mApplicationPacks;
}

///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
