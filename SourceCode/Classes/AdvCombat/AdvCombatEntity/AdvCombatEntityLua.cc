//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatJob.h"
#include "AdventureItem.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///======================================== Lua Hooking ===========================================
void AdvCombatEntity::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatEntity_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatEntity_GetProperty", &Hook_AdvCombatEntity_GetProperty);

    /* AdvCombatEntity_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatEntity_SetProperty", &Hook_AdvCombatEntity_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AdvCombatEntity_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AdvCombatEntity_GetProperty("Exp For Level", iLevel) (1 Integer) (Static)

    //--[System]
    //AdvCombatEntity_GetProperty("Internal Name") (1 String)
    //AdvCombatEntity_GetProperty("Display Name") (1 String)
    //AdvCombatEntity_GetProperty("Cluster Name") (1 String)
    //AdvCombatEntity_GetProperty("Response Path") (1 String)
    //AdvCombatEntity_GetProperty("Position") (2 Floats)
    //AdvCombatEntity_GetProperty("Is Ambushed") (1 Boolean)
    //AdvCombatEntity_GetProperty("Paragon Grouping") (1 String)
    //AdvCombatEntity_GetProperty("Mug Level For Auto Win") (1 Integer)
    //AdvCombatEntity_GetProperty("UI Render Position", iIndex) (2 Floats)
    //AdvCombatEntity_GetProperty("Last Callout Time") (1 Integer)

    //--[Permanent Stats]
    //AdvCombatEntity_GetProperty("Level") (1 Integer)
    //AdvCombatEntity_GetProperty("Exp") (1 Integer)
    //AdvCombatEntity_GetProperty("Global JP") (1 Integer)
    //AdvCombatEntity_GetProperty("Total JP") (1 Integer)
    //AdvCombatEntity_GetProperty("Current Job") (1 String)
    //AdvCombatEntity_GetProperty("Total Jobs") (1 Integer)

    //--[Tags]
    //AdvCombatEntity_GetProperty("Effects With Tag", sTagName) (1 Integer)
    //AdvCombatEntity_GetProperty("Effect ID With Tag", sTagName, iIndex) (1 Integer)
    //AdvCombatEntity_GetProperty("Effects With Tag Partial", sTagName) (1 Integer)
    //AdvCombatEntity_GetProperty("Effect ID With Tag Partial", sTagName, iIndex) (1 Integer)
    //AdvCombatEntity_GetProperty("Base Tag Count", sTagName) (1 Integer)
    //AdvCombatEntity_GetProperty("Tag Count", sTagName) (1 Integer)
    //AdvCombatEntity_GetProperty("Get First Tag Name Matching", sTagName) (1 String)

    //--[Functionality]
    //AdvCombatEntity_GetProperty("Can Act") (1 Boolean)
    //AdvCombatEntity_GetProperty("Free Actions Available") (1 Integer)
    //AdvCombatEntity_GetProperty("Free Actions Performed") (1 Integer)

    //--[Targeting]
    //AdvCombatEntity_GetProperty("Is Normal Target") (1 Boolean)

    //--[Abilities]
    //AdvCombatEntity_GetProperty("Abilities Total") (1 Integer)
    //AdvCombatEntity_GetProperty("Slot of Ability In Master List", sName) (1 Integer)
    //AdvCombatEntity_GetProperty("Ability In Slot", iX, iY) (1 String)
    //AdvCombatEntity_GetProperty("Probability In Slot", iX, iY) (1 Integer)
    //AdvCombatEntity_GetProperty("Is Ability Equipped", sAbilityName) (1 Boolean)
    //AdvCombatEntity_GetProperty("Is Ability Equipped ID", iUniqueID) (1 Boolean)
    //AdvCombatEntity_GetProperty("List Abilities") (No Returns)

    //--[Statistics]
    //AdvCombatEntity_GetProperty("Health") (1 Integer)
    //AdvCombatEntity_GetProperty("Health Max") (1 Integer)
    //AdvCombatEntity_GetProperty("Magic") (1 Integer)
    //AdvCombatEntity_GetProperty("Magic Max") (1 Integer)
    //AdvCombatEntity_GetProperty("Combo Points") (1 Integer)
    //AdvCombatEntity_GetProperty("Shields") (1 Integer)
    //AdvCombatEntity_GetProperty("Adrenaline") (1 Integer)
    //AdvCombatEntity_GetProperty("Influence") (1 Integer)
    //AdvCombatEntity_GetProperty("Influence Max") (1 Integer)
    //AdvCombatEntity_GetProperty("Statistic", iStatistic) (1 Integer)
    //AdvCombatEntity_GetProperty("Statistic", iGrouping, iStatistic) (1 Integer)

    //--[Stun]
    //AdvCombatEntity_GetProperty("Is Stunnable") (1 Boolean)
    //AdvCombatEntity_GetProperty("Is Stunned") (1 Boolean)
    //AdvCombatEntity_GetProperty("Stun") (1 Integer)
    //AdvCombatEntity_GetProperty("Stun Resist") (1 Integer)
    //AdvCombatEntity_GetProperty("Stun Resist Counter") (1 Integer)

    //--[Equipment]
    //AdvCombatEntity_GetProperty("Total Equipment Slots") (1 Integer)
    //AdvCombatEntity_GetProperty("Does Slot Exist", sSlotName) (1 Boolean)
    //AdvCombatEntity_GetProperty("Name of Equipment Slot", iIndex) (1 String)
    //AdvCombatEntity_GetProperty("Equipment In Slot S", sSlotName) (1 String)
    //AdvCombatEntity_GetProperty("Equipment In Slot I", iSlotIndex) (1 String)
    //AdvCombatEntity_GetProperty("Slot For Weapon Damage") (1 String)
    //AdvCombatEntity_GetProperty("Weapon Attack Animation") (1 String)
    //AdvCombatEntity_GetProperty("Weapon Attack Sound") (1 String)
    //AdvCombatEntity_GetProperty("Weapon Critical Sound") (1 String)

    //--[Profiles]
    //AdvCombatEntity_GetProperty("Total Profiles") (1 Integer)
    //AdvCombatEntity_GetProperty("Name of Profile", iIndex) (1 String)
    //AdvCombatEntity_GetProperty("Ability In Profile", iIndex, iX, iY) (1 String)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatEntity_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--EXP needed to reach a given level.
    if(!strcasecmp(rSwitchType, "Exp For Level") && tArgs == 2)
    {
        lua_pushinteger(L, AdvCombatEntity::GetXPOfTable(lua_tointeger(L, 2)));
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATENTITY))
    {
        LuaTypeError("AdvCombatEntity_GetProperty", L);
        return 0;
    }
    AdvCombatEntity *rEntity = (AdvCombatEntity *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Internal name of the character.
    if(!strcasecmp(rSwitchType, "Internal Name") && tArgs == 1)
    {
        lua_pushstring(L, rEntity->GetName());
        tReturns = 1;
    }
    //--Display name of the character.
    else if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 1)
    {
        lua_pushstring(L, rEntity->GetDisplayName());
        tReturns = 1;
    }
    //--Cluster name of the character.
    else if(!strcasecmp(rSwitchType, "Cluster Name") && tArgs == 1)
    {
        lua_pushstring(L, rEntity->GetClusterName());
        tReturns = 1;
    }
    //--Response script path.
    else if(!strcasecmp(rSwitchType, "Response Path") && tArgs == 1)
    {
        const char *rPath = rEntity->GetResponsePath();
        if(!rPath)
            lua_pushstring(L, "Null");
        else
            lua_pushstring(L, rPath);
        tReturns = 1;
    }
    //--Current position.
    else if(!strcasecmp(rSwitchType, "Position") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetCombatX());

        //--If there is an override value for the Y position, use that.
        float tOverrideY = rEntity->GetOverrideCombatY();
        if(tOverrideY != -1.0f)
            lua_pushinteger(L, tOverrideY);
        else
            lua_pushinteger(L, rEntity->GetCombatY());
        tReturns = 2;
    }
    //--Returns true if the enemy is ambushed, false if not.
    else if(!strcasecmp(rSwitchType, "Is Ambushed") && tArgs == 1)
    {
        lua_pushboolean(L, rEntity->IsAmbushed());
        tReturns = 1;
    }
    //--Paragon grouping associated with this entity.
    else if(!strcasecmp(rSwitchType, "Paragon Grouping") && tArgs == 1)
    {
        const char *rGrouping = rEntity->GetParagonGrouping();
        if(!rGrouping)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rGrouping);
        }
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Mug Level For Auto Win") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetLevelForMugAutoWin());
        tReturns = 1;
    }
    //--X/Y offsets at the associated render position.
    else if(!strcasecmp(rSwitchType, "UI Render Position") && tArgs == 2)
    {
        TwoDimensionRealPoint tPoint = rEntity->GetUIRenderPosition(lua_tointeger(L, 2));
        lua_pushnumber(L, tPoint.mXCenter);
        lua_pushnumber(L, tPoint.mYCenter);
        tReturns = 2;
    }
    //--Returns the tick number that the entity last did a callout. Used for AbyssCombat.
    else if(!strcasecmp(rSwitchType, "Last Callout Time") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetLastCalloutTime());
        tReturns = 1;
    }
    ///--[Permanent Stats]
    //--Current level.
    else if(!strcasecmp(rSwitchType, "Level") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetLevel());
        tReturns = 1;
    }
    //--Total experience, ignoring level.
    else if(!strcasecmp(rSwitchType, "Exp") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetXP());
        tReturns = 1;
    }
    //--Unspent global JP available.
    else if(!strcasecmp(rSwitchType, "Global JP") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetGlobalJP());
        tReturns = 1;
    }
    //--Total JP spent across all jobs.
    else if(!strcasecmp(rSwitchType, "Total JP") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetTotalJP());
        tReturns = 1;
    }
    //--String of the name of the current job the character is in. Can be "Null".
    else if(!strcasecmp(rSwitchType, "Current Job") && tArgs == 1)
    {
        AdvCombatJob *rActiveJob = rEntity->GetActiveJob();
        if(!rActiveJob)
            lua_pushstring(L, "Null");
        else
            lua_pushstring(L, rActiveJob->GetInternalName());
        tReturns = 1;
    }
    //--How many jobs the character has information for. Jobs are stored even if they
    //  are not necessarily unlocked.
    else if(!strcasecmp(rSwitchType, "Total Jobs") && tArgs == 1)
    {
        StarLinkedList *rJobList = rEntity->GetJobList();
        lua_pushinteger(L, rJobList->GetListSize());
        tReturns = 1;
    }
    ///--[Tags]
    //--Returns how many AdvCombatEffects are pointing at this entity and have the given tag.
    else if(!strcasecmp(rSwitchType, "Effects With Tag") && tArgs == 2)
    {
        lua_pushinteger(L, rEntity->GetEffectsWithTag(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns the unique ID of the AdvCombatEffect pointing at this entity with the given tag in the Nth slot.
    else if(!strcasecmp(rSwitchType, "Effect ID With Tag") && tArgs == 3)
    {
        lua_pushinteger(L, rEntity->GetEffectIDWithTag(lua_tostring(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    //--Returns how many AdvCombatEffects are pointing at this entity and have the given tag, except the tag can be a partial match.
    else if(!strcasecmp(rSwitchType, "Effects With Tag Partial") && tArgs == 2)
    {
        lua_pushinteger(L, rEntity->GetEffectsWithTagPartial(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns the unique ID of the AdvCombatEffect pointing at this entity with the given tag in the Nth slot, with the tag being a partial match.
    else if(!strcasecmp(rSwitchType, "Effect ID With Tag Partial") && tArgs == 3)
    {
        lua_pushinteger(L, rEntity->GetEffectIDWithTagPartial(lua_tostring(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    //--How many times the given tag is present, but only on the entity itself (ignores equipment, abilities, etc).
    else if(!strcasecmp(rSwitchType, "Base Tag Count") && tArgs == 2)
    {
        lua_pushinteger(L, rEntity->GetBaseTagCount(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--How many times the given tag is present.
    else if(!strcasecmp(rSwitchType, "Tag Count") && tArgs == 2)
    {
        lua_pushinteger(L, rEntity->GetTagCount(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Given a partial tag name, returns the full name of the tag. Only catches the first match.
    else if(!strcasecmp(rSwitchType, "Get First Tag Name Matching") && tArgs == 2)
    {
        lua_pushstring(L, rEntity->GetFirstTagNameMatching(lua_tostring(L, 2)));
        tReturns = 1;
    }
    ///--[Functionality]
    //--Returns true if the entity is able to act right now. Entities with zero HP or who are time-stopped cannot perform actions.
    else if(!strcasecmp(rSwitchType, "Can Act") && tArgs == 1)
    {
        lua_pushboolean(L, !rEntity->IsDefeated());
        tReturns = 1;
    }
    //--Returns how many Free Actions the entity has available.
    else if(!strcasecmp(rSwitchType, "Free Actions Available") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetFreeActions());
        tReturns = 1;
    }
    //--Returns how many Free Actions the entity has performed this turn.
    else if(!strcasecmp(rSwitchType, "Free Actions Performed") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetFreeActionsPerformed());
        tReturns = 1;
    }
    ///--[Targeting]
    //--Returns true if this entity is a "Normal" target. Normal targets are those with non-zero HP who are not afflicted
    //  by some status effect that prevents damage (like time-stop).
    else if(!strcasecmp(rSwitchType, "Is Normal Target") && tArgs == 1)
    {
        lua_pushboolean(L, rEntity->IsNormalTarget());
        tReturns = 1;
    }
    ///--[Abilities]
    //--How many abilities the entity has available, total.
    else if(!strcasecmp(rSwitchType, "Abilities Total") && tArgs == 1)
    {
        StarLinkedList *rAbilityList = rEntity->GetAbilityList();
        if(rAbilityList)
        {
            lua_pushinteger(L, rAbilityList->GetListSize());
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--Returns the slot of the ability in the master list, or -1 if not found.
    else if(!strcasecmp(rSwitchType, "Slot of Ability In Master List") && tArgs == 2)
    {
        StarLinkedList *rAbilityList = rEntity->GetAbilityList();
        if(rAbilityList)
        {
            lua_pushinteger(L, rAbilityList->GetSlotOfElementByName(lua_tostring(L, 2)));
        }
        else
        {
            lua_pushinteger(L, -1);
        }
        tReturns = 1;
    }
    //--Name of the ability in the given slot.
    else if(!strcasecmp(rSwitchType, "Ability In Slot") && tArgs == 3)
    {
        lua_pushstring(L, rEntity->GetAbilityNameInSlot(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    //--Chance of using the ability.
    else if(!strcasecmp(rSwitchType, "Probability In Slot") && tArgs == 3)
    {
        lua_pushinteger(L, rEntity->GetProbabilityInSlot(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    //--Returns true if the named ability is equipped.
    else if(!strcasecmp(rSwitchType, "Is Ability Equipped") && tArgs == 2)
    {
        lua_pushboolean(L, rEntity->IsAbilityEquippedS(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns true if the ability with the given ID is equipped.
    else if(!strcasecmp(rSwitchType, "Is Ability Equipped ID") && tArgs == 2)
    {
        lua_pushboolean(L, rEntity->IsAbilityEquippedI(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Prints a list of abilities to the console.
    else if(!strcasecmp(rSwitchType, "List Abilities") && tArgs == 1)
    {
        rEntity->PrintAbilitiesToConsole();
        tReturns = 0;
    }
    ///--[Statistics]
    //--Returns current HP.
    else if(!strcasecmp(rSwitchType, "Health") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetHealth());
        tReturns = 1;
    }
    //--Returns Max HP. Shortcut to replace the Get Statistic below.
    else if(!strcasecmp(rSwitchType, "Health Max") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX));
        tReturns = 1;
    }
    //--Returns current MP.
    else if(!strcasecmp(rSwitchType, "Magic") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetMagic());
        tReturns = 1;
    }
    //--Returns current max MP.
    else if(!strcasecmp(rSwitchType, "Magic Max") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_MPMAX));
        tReturns = 1;
    }
    //--Returns current CP.
    else if(!strcasecmp(rSwitchType, "Combo Points") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetComboPoints());
        tReturns = 1;
    }
    //--Returns current shield value.
    else if(!strcasecmp(rSwitchType, "Shields") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetShields());
        tReturns = 1;
    }
    //--Returns current adrenaline value.
    else if(!strcasecmp(rSwitchType, "Adrenaline") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetAdrenaline());
        tReturns = 1;
    }
    //--Returns current influence value.
    else if(!strcasecmp(rSwitchType, "Influence") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetInfluence());
        tReturns = 1;
    }
    //--Returns maximum influence value. Can change with levelups.
    else if(!strcasecmp(rSwitchType, "Influence Max") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetInfluenceMax());
        tReturns = 1;
    }
    //--Returns the statistic at the given index, assuming the final grouping.
    else if(!strcasecmp(rSwitchType, "Statistic") && tArgs == 2)
    {
        lua_pushinteger(L, rEntity->GetStatistic(ADVCE_STATS_FINAL, lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Returns the statistic at the given grouping/index.
    else if(!strcasecmp(rSwitchType, "Statistic") && tArgs == 3)
    {
        lua_pushinteger(L, rEntity->GetStatistic(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    ///--[Stun]
    //--True if the entity is stunnable, false if not.
    else if(!strcasecmp(rSwitchType, "Is Stunnable") && tArgs == 1)
    {
        lua_pushboolean(L, rEntity->IsStunnable());
        tReturns = 1;
    }
    //--Target is currently stunned, or will be next turn.
    else if(!strcasecmp(rSwitchType, "Is Stunned") && tArgs == 1)
    {
        lua_pushboolean(L, rEntity->IsStunnedThisTurn());
        tReturns = 1;
    }
    //--Current stun value.
    else if(!strcasecmp(rSwitchType, "Stun") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetStun());
        tReturns = 1;
    }
    //--Current stun resist state.
    else if(!strcasecmp(rSwitchType, "Stun Resist") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetStunResist());
        tReturns = 1;
    }
    //--Current stun resist decrement counter.
    else if(!strcasecmp(rSwitchType, "Stun Resist Counter") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetStunResistTimer());
        tReturns = 1;
    }
    ///--[Equipment]
    //--How many equipment slots the character has.
    else if(!strcasecmp(rSwitchType, "Total Equipment Slots") && tArgs == 1)
    {

        lua_pushinteger(L, rEntity->GetEquipmentSlotsTotal());
        tReturns = 1;
    }
    //--True if the named slot exists, false if not.
    else if(!strcasecmp(rSwitchType, "Does Slot Exist") && tArgs == 2)
    {
        int tCheckSlot = rEntity->GetSlotOfEquipmentByName(lua_tostring(L, 2));
        lua_pushboolean(L, (tCheckSlot != -1));
        tReturns = 1;
    }
    //--Name of the given equipment slot.
    else if(!strcasecmp(rSwitchType, "Name of Equipment Slot") && tArgs == 2)
    {
        lua_pushstring(L, rEntity->GetNameOfEquipmentSlot(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Name of the piece of equipment in the given slot, or "Null" if it's empty.
    else if(!strcasecmp(rSwitchType, "Equipment In Slot S") && tArgs == 2)
    {
        AdventureItem *rItemInSlot = rEntity->GetEquipmentBySlotS(lua_tostring(L, 2));
        if(!rItemInSlot)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rItemInSlot->GetName());
        }
        tReturns = 1;
    }
    //--Name of the piece of the equipment in the slot index, or "Null" if it's empty.
    else if(!strcasecmp(rSwitchType, "Equipment In Slot I") && tArgs == 2)
    {
        AdventureItem *rItemInSlot = rEntity->GetEquipmentBySlotI(lua_tointeger(L, 2));
        if(!rItemInSlot)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rItemInSlot->GetName());
        }
        tReturns = 1;
    }
    //--Which slot is used to compute weapon damage. Defaults to 0.
    else if(!strcasecmp(rSwitchType, "Slot For Weapon Damage") && tArgs == 1)
    {
        int tSlot = rEntity->GetSlotForWeaponDamage();
        const char *rSlotName = rEntity->GetNameOfEquipmentSlot(tSlot);
        if(!rSlotName)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rSlotName);
        }
        tReturns = 1;
    }
    //--Animation used by the current weapon. "Null" if none is equipped.
    else if(!strcasecmp(rSwitchType, "Weapon Attack Animation") && tArgs == 1)
    {
        AdventureItem *rWeapon = rEntity->GetWeapon();
        if(!rWeapon)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            const char *rAnimation = rWeapon->GetEquipmentAttackAnimation();
            if(!rAnimation)
            {
                lua_pushstring(L, "Null");
            }
            else
            {
                lua_pushstring(L, rAnimation);
            }
        }
        tReturns = 1;
    }
    //--Sound effect made by the current weapon. "Null" if none is equipped.
    else if(!strcasecmp(rSwitchType, "Weapon Attack Sound") && tArgs == 1)
    {
        AdventureItem *rWeapon = rEntity->GetWeapon();
        if(!rWeapon)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            const char *rSound = rWeapon->GetEquipmentAttackSound();
            if(!rSound)
            {
                lua_pushstring(L, "Null");
            }
            else
            {
                lua_pushstring(L, rSound);
            }
        }
        tReturns = 1;
    }
    //--Sound effect made by the current weapon for critical strikes. "Null" if none is equipped.
    else if(!strcasecmp(rSwitchType, "Weapon Critical Sound") && tArgs == 1)
    {
        AdventureItem *rWeapon = rEntity->GetWeapon();
        if(!rWeapon)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            const char *rSound = rWeapon->GetEquipmentCriticalSound();
            if(!rSound)
            {
                lua_pushstring(L, "Null");
            }
            else
            {
                lua_pushstring(L, rSound);
            }
        }
        tReturns = 1;
    }
    ///--[Profiles]
    //--How many profiles. Will always be at least 1 for the "Default" profile.
    else if(!strcasecmp(rSwitchType, "Total Profiles") && tArgs == 1)
    {
        lua_pushinteger(L, rEntity->GetTotalProfiles());
        tReturns = 1;
    }
    //--Name of given profile.
    else if(!strcasecmp(rSwitchType, "Name of Profile") && tArgs == 2)
    {
        lua_pushstring(L, rEntity->GetNameOfProfileI(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Name of the ability in the given slot for the given profile.
    else if(!strcasecmp(rSwitchType, "Ability In Profile") && tArgs == 4)
    {
        //--Get parts.
        AbilityProfile *rAbilityProfile = rEntity->GetProfileI(lua_tointeger(L, 2));
        int tX = lua_tointeger(L, 3);
        int tY = lua_tointeger(L, 4);

        //--Profile failed to resolve, or out of range.
        if(!rAbilityProfile || tX < 0 || tY < 0 || tX >= ACE_ABILITY_GRID_PROFILE_SIZE_X || tY >= ACE_ABILITY_GRID_PROFILE_SIZE_Y)
        {
            lua_pushstring(L, "Null");
            tReturns = 1;
        }
        else if(!rAbilityProfile->mAbilityNames[tX][tY])
        {
            lua_pushstring(L, "Null");
            tReturns = 1;
        }
        else
        {
            lua_pushstring(L, rAbilityProfile->mAbilityNames[tX][tY]);
            tReturns = 1;
        }
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatEntity_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatEntity_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AdvCombatEntity_SetProperty("Max Levels", iNumber) (Static)
    //AdvCombatEntity_SetProperty("Exp For Level", iLevel, iExpNeeded) (Static)
    //AdvCombatEntity_SetProperty("Check Level Validity") (Static)

    //--[System]
    //AdvCombatEntity_SetProperty("Display Name", sName) //"DEFAULT" reverts to internal name
    //AdvCombatEntity_SetProperty("Cluster Name", sName)
    //AdvCombatEntity_SetProperty("Response Script", sPath)
    //AdvCombatEntity_SetProperty("Response Code", iIndex, bFlag)
    //AdvCombatEntity_SetProperty("Animation Script", sPath)
    //AdvCombatEntity_SetProperty("AI Script", sPath)
    //AdvCombatEntity_SetProperty("Inspector Show Stats", iAmount)
    //AdvCombatEntity_SetProperty("Inspector Show Resists", iAmount)
    //AdvCombatEntity_SetProperty("Inspector Show Abilities", iAmount)
    //AdvCombatEntity_SetProperty("Ambushed", bFlag)
    //AdvCombatEntity_SetProperty("Paragon Grouping", sGrouping)
    //AdvCombatEntity_SetProperty("Clear Combat Start Job")
    //AdvCombatEntity_SetProperty("Level For Mug Auto Win", iLevel)
    //AdvCombatEntity_SetProperty("Cannot Be Knocked Out", bFlag)
    //AdvCombatEntity_SetProperty("Remove From Party On Knockout", bFlag)
    //AdvCombatEntity_SetProperty("Mark Callout")
    //AdvCombatEntity_SetProperty("Mark Crossfade")
    //AdvCombatEntity_SetProperty("Has Parting Shot", bFlag)

    //--[Permanent Stats]
    //AdvCombatEntity_SetProperty("Compute Level Statistics", iLevel or -1)
    //AdvCombatEntity_SetProperty("Current Exp", iExp)
    //AdvCombatEntity_SetProperty("Current JP", iJP)

    //--[Tags]
    //AdvCombatEntity_SetProperty("Add Tag", sTagName, iCount)
    //AdvCombatEntity_SetProperty("Remove Tag", sTagName, iCount)

    //--[Statistics]
    //AdvCombatEntity_SetProperty("Health", iValue)
    //AdvCombatEntity_SetProperty("Health Percent", fValue)
    //AdvCombatEntity_SetProperty("Magic", iValue)
    //AdvCombatEntity_SetProperty("Magic Percent", fValue)
    //AdvCombatEntity_SetProperty("Combo Points", iValue)
    //AdvCombatEntity_SetProperty("Shields", iValue)
    //AdvCombatEntity_SetProperty("Adrenaline", iValue)
    //AdvCombatEntity_SetProperty("Influence", iValue)
    //AdvCombatEntity_SetProperty("Influence Max", iValue)
    //AdvCombatEntity_SetProperty("Statistic", iGrouping, iStatistic, iValue)
    //AdvCombatEntity_SetProperty("Recompute Stats")
    //AdvCombatEntity_SetProperty("Refresh Stats for UI")

    //--[Rewards]
    //AdvCombatEntity_SetProperty("Reward XP", iValue)
    //AdvCombatEntity_SetProperty("Reward JP", iValue)
    //AdvCombatEntity_SetProperty("Reward Platina", iValue)
    //AdvCombatEntity_SetProperty("Reward Doctor", iValue)
    //AdvCombatEntity_SetProperty("Reward Item", sItemName)

    //--[Stun]
    //AdvCombatEntity_SetProperty("Is Stunnable", bValue)
    //AdvCombatEntity_SetProperty("Stun", iValue)
    //AdvCombatEntity_SetProperty("Stun No Display", iValue)
    //AdvCombatEntity_SetProperty("Stun Add", iValue)
    //AdvCombatEntity_SetProperty("Stun Resist", iValue)
    //AdvCombatEntity_SetProperty("Stun Resist Timer", iValue)

    //--[Display]
    //AdvCombatEntity_SetProperty("Combat Portrait", sDLPath)
    //AdvCombatEntity_SetProperty("Combat Countermask", sDLPath)
    //AdvCombatEntity_SetProperty("Turn Icon", sDLPath)
    //AdvCombatEntity_SetProperty("Vendor Image", iSlot, sDLPath)
    //AdvCombatEntity_SetProperty("UI Render Position", iSlot, iXOffset, iYOffset)
    //AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, sDLPath)
    //AdvCombatEntity_SetProperty("Override Y", fValue)
    //AdvCombatEntity_SetProperty("Override Combat Y", fValue)

    //--[Jobs]
    //AdvCombatEntity_SetProperty("Create Job", sJobName) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Push Job S", sJobName) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Push Job I", iSlot) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Active Job", sJobName)
    //AdvCombatEntity_SetProperty("Register Ability To Job", sAbilityName, sJobName, iDummy)
    //AdvCombatEntity_SetProperty("Unregister Ability From Job", sAbilityName, sJobName)
    //AdvCombatEntity_SetProperty("Rebuild Job Skills UI")
    //AdvCombatEntity_SetProperty("Recheck Jobs Against Profiles")

    //--[Abilities]
    //AdvCombatEntity_SetProperty("Create Ability", sAbilityName) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Push Ability S", sAbilityName) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Set Ability Slot", iSlotX, iSlotY, sAbilityName)
    //AdvCombatEntity_SetProperty("Set Ability Slot", iSlotX, iSlotY, sAbilityName, iProbability)
    //AdvCombatEntity_SetProperty("Set Ability Slot Probability", iSlotX, iSlotY, iProbability)
    //AdvCombatEntity_SetProperty("Set Secondary Slot", iSlotX, iSlotY, sAbilityName)
    //AdvCombatEntity_SetProperty("Push Ability In Slot", iSlotX, iSlotY) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Add Passive Ability", sAbilityName)
    //AdvCombatEntity_SetProperty("Remove Ability", sAbilityName)

    //--[Equipment]
    //AdvCombatEntity_SetProperty("Create Equipment Slot", sSlotName)
    //AdvCombatEntity_SetProperty("Set Equipment Slot Display Name", sSlotName, sDisplayName)
    //AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", sSlotName, bFlag)
    //AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Tags", sSlotName, bFlag)
    //AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", sSlotName, bFlag)
    //AdvCombatEntity_SetProperty("Set Equipment Slot Used For Weapon Damage", sSlotName)
    //AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", sSlotName, bFlag)
    //AdvCombatEntity_SetProperty("Set Equipment Slot No Gems", sSlotName, bFlag)
    //AdvCombatEntity_SetProperty("Equip Item To Slot", sSlotName, sItemName)
    //AdvCombatEntity_SetProperty("Equip Marked Item To Slot", sSlotName)
    //AdvCombatEntity_SetProperty("Unequip Slot", sSlotName)
    //AdvCombatEntity_SetProperty("Push Item In Slot S", sSlotName) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Push Item In Slot I", iSlotIndex) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Swap Equipment In Slots S", sSlotNameA, sSlotNameB)

    //--[Profiles]
    //AdvCombatEntity_SetProperty("Wipe Profiles")
    //AdvCombatEntity_SetProperty("Create Profile", sProfileName)
    //AdvCombatEntity_SetProperty("Set Profile Ability", sProfileName, iX, iY, sAbilityName)

    //--[TF Indicators]
    //AdvCombatEntity_SetProperty("TF Indicator Count", iNumber)
    //AdvCombatEntity_SetProperty("TF Indicator Image", sDLPath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatEntity_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--How many levels the player can gain, total.
    if(!strcasecmp(rSwitchType, "Max Levels") && tArgs == 2)
    {
        //--Args.
        int tLevels = lua_tointeger(L, 2);

        //--Deallocate.
        free(AdvCombatEntity::xExpTable);

        //--Reset.
        AdvCombatEntity::xMaxLevel = 0;
        AdvCombatEntity::xExpTable = NULL;
        if(tLevels < 1) return 0;

        //--Allocate.
        AdvCombatEntity::xMaxLevel = tLevels;
        AdvCombatEntity::xExpTable = (int *)starmemoryalloc(sizeof(int) * AdvCombatEntity::xMaxLevel);
        memset(AdvCombatEntity::xExpTable, 0, sizeof(int) * AdvCombatEntity::xMaxLevel);
        return 0;
    }
    //--XP needed to reach this level.
    else if(!strcasecmp(rSwitchType, "Exp For Level") && tArgs == 3)
    {
        int tLevel = lua_tointeger(L, 2);
        if(tLevel < 0 || tLevel >= AdvCombatEntity::xMaxLevel) return 0;
        AdvCombatEntity::xExpTable[tLevel] = lua_tointeger(L, 3);
        return 0;
    }
    //--Makes sure all the levels are valid. Used once the XP chart is built.
    else if(!strcasecmp(rSwitchType, "Check Level Validity") && tArgs == 1)
    {
        if(AdvCombatEntity::xMaxLevel < 1) return 0;
        if(AdvCombatEntity::xExpTable[0] < 0) AdvCombatEntity::xExpTable[0] = 0;
        for(int i = 1; i < AdvCombatEntity::xMaxLevel; i ++)
        {
            if(AdvCombatEntity::xExpTable[i] <= AdvCombatEntity::xExpTable[i-1])
            {
                AdvCombatEntity::xExpTable[i] = AdvCombatEntity::xExpTable[i-1] + 1;
            }
        }
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATENTITY))
    {
        fprintf(stderr, "%s: Failed, rActiveObject was wrong type, or NULL.\n", "AdvCombatEntity_SetProperty");
        fprintf(stderr, " Arguments: %i\n", tArgs);
        for(int i = 0; i < tArgs; i ++)
        {
            fprintf(stderr, "  %02i: %s\n", i, lua_tostring(L, i+1));
        }
        fprintf(stderr, " Path: %s\n", LuaManager::Fetch()->GetCallStack(0));
        return 0;
    }
    AdvCombatEntity *rEntity = (AdvCombatEntity *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Name that shows on the UI. Pass "DEFAULT" to return to the internal name.
    if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rEntity->SetDisplayName(lua_tostring(L, 2));
    }
    //--Name the enemy uses to track KOs.
    else if(!strcasecmp(rSwitchType, "Cluster Name") && tArgs == 2)
    {
        rEntity->SetClusterName(lua_tostring(L, 2));
    }
    //--Script called as a response to various actions, like turns beginning or actions ending.
    else if(!strcasecmp(rSwitchType, "Response Script") && tArgs == 2)
    {
        rEntity->SetResponseScript(lua_tostring(L, 2));
    }
    //--Marks whether or not the entity actually fires its response script for the given code.
    else if(!strcasecmp(rSwitchType, "Response Code") && tArgs == 3)
    {
        rEntity->SetResponseScriptCode(lua_tointeger(L, 2), lua_toboolean(L, 3));
    }
    //--Script called during animation updates that can affect rendering properties.
    else if(!strcasecmp(rSwitchType, "Animation Script") && tArgs == 2)
    {
        rEntity->SetAnimationScript(lua_tostring(L, 2));
    }
    //--Which AI controls this entity. Pass "Null" to return to player control.
    else if(!strcasecmp(rSwitchType, "AI Script") && tArgs == 2)
    {
        rEntity->SetAIScript(lua_tostring(L, 2));
    }
    //--Combat inspector shows statistics.
    else if(!strcasecmp(rSwitchType, "Inspector Show Stats") && tArgs == 2)
    {
        rEntity->SetInspectorShowStats(lua_tointeger(L, 2));
    }
    //--Combat inspector shows resistances.
    else if(!strcasecmp(rSwitchType, "Inspector Show Resists") && tArgs == 2)
    {
        rEntity->SetInspectorShowResists(lua_tointeger(L, 2));
    }
    //--Combat inspector shows abilities.
    else if(!strcasecmp(rSwitchType, "Inspector Show Abilities") && tArgs == 2)
    {
        rEntity->SetInspectorShowAbilities(lua_tointeger(L, 2));
    }
    //--Marks the AI as ambushed. Ambushed AIs usually skip their next turn.
    else if(!strcasecmp(rSwitchType, "Ambushed") && tArgs == 2)
    {
        rEntity->SetAmbushed(lua_toboolean(L, 2));
    }
    //--Tracks which paragon grouping this entity has.
    else if(!strcasecmp(rSwitchType, "Paragon Grouping") && tArgs == 2)
    {
        rEntity->SetParagonGrouping(lua_tostring(L, 2));
    }
    //--Clears what job the entity thinks they started a battle at. This will cause battle TFs to not
    //  revert, or if the entity has recently rejoined the party, to not revert.
    else if(!strcasecmp(rSwitchType, "Clear Combat Start Job") && tArgs == 1)
    {
        rEntity->ClearCombatStartJob();
    }
    //--Sets the effective level for determining mug auto-wins.
    else if(!strcasecmp(rSwitchType, "Level For Mug Auto Win") && tArgs == 2)
    {
        rEntity->SetLevelForMugAutoWin(lua_tointeger(L, 2));
    }
    //--If true, the entity will go to 1HP and not be KO'd.
    else if(!strcasecmp(rSwitchType, "Cannot Be Knocked Out") && tArgs == 2)
    {
        rEntity->SetCannotBeKnockedOutFlag(lua_toboolean(L, 2));
    }
    //--If true, the entity will be removed from the party after the action ends if they are KO'd.
    else if(!strcasecmp(rSwitchType, "Remove From Party On Knockout") && tArgs == 2)
    {
        rEntity->SetRemoveFromPartyOnKnockoutFlag(lua_toboolean(L, 2));
    }
    //--Marks when this entity last performed a callout.
    else if(!strcasecmp(rSwitchType, "Mark Callout") && tArgs == 1)
    {
        rEntity->MarkCallout();
    }
    //--Marks when an entity changes portrait, requiring a crossfade.
    else if(!strcasecmp(rSwitchType, "Mark Crossfade") && tArgs == 1)
    {
        rEntity->MarkCrossfade();
    }
    //--Has a parting shot, and thus won't die until the flag is false.
    else if(!strcasecmp(rSwitchType, "Has Parting Shot") && tArgs == 2)
    {
        rEntity->SetHasPartingShotFlag(lua_toboolean(L, 2));
    }
    ///--[Permanent Stats]
    //--Ask the entity to re-run its stats setting for its given level. Pass -1 to use current level.
    else if(!strcasecmp(rSwitchType, "Compute Level Statistics") && tArgs == 2)
    {
        rEntity->ComputeLevelStatistics(lua_tointeger(L, 2));
    }
    //--Sets the current XP of the entity.
    else if(!strcasecmp(rSwitchType, "Current Exp") && tArgs == 2)
    {
        rEntity->SetXP(lua_tointeger(L, 2));
    }
    //--Sets the current global JP of the entity.
    else if(!strcasecmp(rSwitchType, "Current JP") && tArgs == 2)
    {
        rEntity->SetGlobalJP(lua_tointeger(L, 2));
    }
    ///--[Tags]
    //--Adds a tag to the entity.
    else if(!strcasecmp(rSwitchType, "Add Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rEntity->AddTag(lua_tostring(L, 2));
    }
    //--Removes a tag from the entity.
    else if(!strcasecmp(rSwitchType, "Remove Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rEntity->RemoveTag(lua_tostring(L, 2));
    }
    ///--[Statistics]
    //--Sets the current HP.
    else if(!strcasecmp(rSwitchType, "Health") && tArgs == 2)
    {
        rEntity->SetHealth(lua_tointeger(L, 2));
    }
    //--Sets the current HP by percentage.
    else if(!strcasecmp(rSwitchType, "Health Percent") && tArgs == 2)
    {
        rEntity->SetHealthPercent(lua_tonumber(L, 2), false);
    }
    //--Sets the current MP.
    else if(!strcasecmp(rSwitchType, "Magic") && tArgs == 2)
    {
        rEntity->SetMagic(lua_tointeger(L, 2));
    }
    //--Sets the current MP by percentage.
    else if(!strcasecmp(rSwitchType, "Magic Percent") && tArgs == 2)
    {
        rEntity->SetMagicPercent(lua_tonumber(L, 2));
    }
    //--Sets the current CP.
    else if(!strcasecmp(rSwitchType, "Combo Points") && tArgs == 2)
    {
        rEntity->SetComboPoints(lua_tointeger(L, 2));
    }
    //--Sets the shields value.
    else if(!strcasecmp(rSwitchType, "Shields") && tArgs == 2)
    {
        rEntity->SetShields(lua_tointeger(L, 2));
    }
    //--Sets the adrenaline value.
    else if(!strcasecmp(rSwitchType, "Adrenaline") && tArgs == 2)
    {
        rEntity->SetAdrenaline(lua_tointeger(L, 2));
    }
    //--Sets the influence value.
    else if(!strcasecmp(rSwitchType, "Influence") && tArgs == 2)
    {
        rEntity->SetInfluence(lua_tointeger(L, 2));
    }
    //--Sets the influence value.
    else if(!strcasecmp(rSwitchType, "Influence Max") && tArgs == 2)
    {
        rEntity->SetInfluenceMax(lua_tointeger(L, 2));
    }
    //--Sets the requested statistic.
    else if(!strcasecmp(rSwitchType, "Statistic") && tArgs == 4)
    {
        rEntity->SetStatistic(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Calculates final statistics after modifications are made.
    else if(!strcasecmp(rSwitchType, "Recompute Stats") && tArgs == 1)
    {
        rEntity->ComputeStatistics();
    }
    //--Applies stat bonuses from abilities.
    else if(!strcasecmp(rSwitchType, "Refresh Stats for UI") && tArgs == 1)
    {
        rEntity->RefreshStatsForUI();
    }
    ///--[Rewards]
    //--Sets XP awarded for victory.
    else if(!strcasecmp(rSwitchType, "Reward XP") && tArgs == 2)
    {
        rEntity->SetRewardXP(lua_tointeger(L, 2));
    }
    //--Sets JP awarded for victory.
    else if(!strcasecmp(rSwitchType, "Reward JP") && tArgs == 2)
    {
        rEntity->SetRewardJP(lua_tointeger(L, 2));
    }
    //--Sets Platina awarded for victory.
    else if(!strcasecmp(rSwitchType, "Reward Platina") && tArgs == 2)
    {
        rEntity->SetRewardPlatina(lua_tointeger(L, 2));
    }
    //--Sets doctor bag awarded for victory.
    else if(!strcasecmp(rSwitchType, "Reward Doctor") && tArgs == 2)
    {
        rEntity->SetRewardDoctor(lua_tointeger(L, 2));
    }
    //--Adds the item to the rewards list.
    else if(!strcasecmp(rSwitchType, "Reward Item") && tArgs == 2)
    {
        rEntity->AddRewardItem(lua_tostring(L, 2));
    }
    ///--[Stun]
    //--Sets whether or not the entity is "Stunnable". In C++, this just determines if the stun
    //  statistics display. The script ultimately decides if an entity is stunnable.
    else if(!strcasecmp(rSwitchType, "Is Stunnable") && tArgs == 2)
    {
        rEntity->SetStunnable(lua_toboolean(L, 2));
    }
    //--Sets current stun value.
    else if(!strcasecmp(rSwitchType, "Stun") && tArgs == 2)
    {
        rEntity->SetStun(lua_tointeger(L, 2));
    }
    //--Sets current stun value without modifying displayed stun value.
    else if(!strcasecmp(rSwitchType, "Stun No Display") && tArgs == 2)
    {
        rEntity->SetStunNoDisplay(lua_tointeger(L, 2));
    }
    //--Adds to the current stun value.
    else if(!strcasecmp(rSwitchType, "Stun Add") && tArgs == 2)
    {
        rEntity->AddStun(lua_tointeger(L, 2));
    }
    //--Sets current stun resist.
    else if(!strcasecmp(rSwitchType, "Stun Resist") && tArgs == 2)
    {
        rEntity->SetStunResist(lua_tointeger(L, 2));
    }
    //--Sets current stun resist timer.
    else if(!strcasecmp(rSwitchType, "Stun Resist Timer") && tArgs == 2)
    {
        rEntity->SetStunResistTimer(lua_tointeger(L, 2));
    }
    ///--[Display]
    //--Sets the main portrait for the character. This appears in combat, on the UI, and a few other places.
    else if(!strcasecmp(rSwitchType, "Combat Portrait") && tArgs == 2)
    {
        rEntity->SetCombatPortrait(lua_tostring(L, 2));
    }
    //--Sets the optional countermask, used to prevent a character from rendering something that goes off
    //  and back on to the frame when on certain parts of the UI.
    else if(!strcasecmp(rSwitchType, "Combat Countermask") && tArgs == 2)
    {
        rEntity->SetCombatCountermask(lua_tostring(L, 2));
    }
    //--Victory countermask, same as combat version but for the victory screen.
    else if(!strcasecmp(rSwitchType, "Victory Countermask") && tArgs == 2)
    {
        rEntity->SetVictoryCountermask(lua_tostring(L, 2));
    }
    //--Set the turn icon that appears in combat.
    else if(!strcasecmp(rSwitchType, "Turn Icon") && tArgs == 2)
    {
        rEntity->SetTurnIcon(lua_tostring(L, 2));
    }
    //--Sets images that show during the vendor UI.
    else if(!strcasecmp(rSwitchType, "Vendor Image") && tArgs == 3)
    {
        rEntity->SetVendorImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Render position of the combat portrait at various UI locations.
    else if(!strcasecmp(rSwitchType, "UI Render Position") && tArgs == 4)
    {
        rEntity->SetUIRenderPos(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Set dimensions of the face table used on equipment and inventory rendering.
    else if(!strcasecmp(rSwitchType, "Face Table Data") && tArgs == 6)
    {
        rEntity->SetFaceTableData(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tostring(L, 6));
    }
    //--Sets the Y position of rendering, overriding the auto-setters.
    else if(!strcasecmp(rSwitchType, "Override Y") && tArgs == 2)
    {
        rEntity->SetOverrideY(lua_tonumber(L, 2));
    }
    //--Sets the Y position of application text, overriding the auto-setters.
    else if(!strcasecmp(rSwitchType, "Override Combat Y") && tArgs == 2)
    {
        rEntity->SetOverrideCombatY(lua_tonumber(L, 2));
    }
    ///--[Jobs]
    //--Creates a new AdvCombatJob and pushes it on the activity stack.
    else if(!strcasecmp(rSwitchType, "Create Job") && tArgs == 2)
    {
        AdvCombatJob *nNewJob = new AdvCombatJob();
        nNewJob->SetOwner(rEntity);
        nNewJob->SetInternalName(lua_tostring(L, 2));
        rEntity->RegisterJob(lua_tostring(L, 2), nNewJob);
        DataLibrary::Fetch()->PushActiveEntity(nNewJob);
    }
    //--Push the named job.
    else if(!strcasecmp(rSwitchType, "Push Job S") && tArgs == 2)
    {
        const char *rJobName = lua_tostring(L, 2);
        DataLibrary::Fetch()->PushActiveEntity();
        if(!strcasecmp(rJobName, "Active"))
        {
            void *rJob = rEntity->GetActiveJob();
            DataLibrary::Fetch()->rActiveObject = rJob;
        }
        else
        {
            StarLinkedList *rJobList = rEntity->GetJobList();
            void *rJob = rJobList->GetElementByName(lua_tostring(L, 2));
            DataLibrary::Fetch()->rActiveObject = rJob;
        }
    }
    //--Push the job in the given slot.
    else if(!strcasecmp(rSwitchType, "Push Job I") && tArgs == 2)
    {
        StarLinkedList *rJobList = rEntity->GetJobList();
        DataLibrary::Fetch()->PushActiveEntity();
        void *rJob = rJobList->GetElementBySlot(lua_tointeger(L, 2));
        DataLibrary::Fetch()->rActiveObject = rJob;
    }
    //--Orders the character to change to the requested job.
    else if(!strcasecmp(rSwitchType, "Active Job") && tArgs == 2)
    {
        rEntity->SetActiveJob(lua_tostring(L, 2));
    }
    //--Registers the named ability to the named job for purchase in the skills UI.
    else if(!strcasecmp(rSwitchType, "Register Ability To Job") && tArgs >= 3)
    {
        rEntity->RegisterAbilityToJob(lua_tostring(L, 2), lua_tostring(L, 3), 0);
    }
    //--Removes the named ability from the named job.
    else if(!strcasecmp(rSwitchType, "Unregister Ability From Job") && tArgs == 3)
    {
        rEntity->UnregisterAbilityFromJob(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Forcibly rebuilds the list of jobs on the skills UI.
    else if(!strcasecmp(rSwitchType, "Rebuild Job Skills UI") && tArgs == 1)
    {
        rEntity->RebuildSkillJobList();
    }
    //--Verifies that all jobs have an associated profile that exists. Any that don't use the default.
    else if(!strcasecmp(rSwitchType, "Recheck Jobs Against Profiles") && tArgs == 1)
    {
        rEntity->RecheckJobsAgainstProfiles();
    }
    ///--[Abilities]
    //--Creates a new ability and pushes the activity stack.
    else if(!strcasecmp(rSwitchType, "Create Ability") && tArgs == 2)
    {
        AdvCombatAbility *nNewAbility = new AdvCombatAbility();
        rEntity->RegisterAbility(lua_tostring(L, 2), nNewAbility);
        DataLibrary::Fetch()->PushActiveEntity(nNewAbility);
    }
    //--Pushes the ability with the matching name, if it exists.
    else if(!strcasecmp(rSwitchType, "Push Ability S") && tArgs == 2)
    {
        StarLinkedList *rAbilityList = rEntity->GetAbilityList();
        void *rAbilityPtr = rAbilityList->GetElementByName(lua_tostring(L, 2));
        DataLibrary::Fetch()->PushActiveEntity(rAbilityPtr);
    }
    //--Populates a slot with the named ability from the master list.
    else if(!strcasecmp(rSwitchType, "Set Ability Slot") && tArgs == 4)
    {
        rEntity->SetAbilitySlot(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Populates a slot with an ability from the master list and sets its AI probability.
    else if(!strcasecmp(rSwitchType, "Set Ability Slot") && tArgs == 5)
    {
        rEntity->SetAbilitySlot(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
        rEntity->SetAbilitySlotProbability(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 5));
    }
    //--Sets the chance an AI will use this ability.
    else if(!strcasecmp(rSwitchType, "Set Ability Slot Probability") && tArgs == 4)
    {
        rEntity->SetAbilitySlotProbability(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Populates a slot on the secondary page with the named ability from the master list.
    else if(!strcasecmp(rSwitchType, "Set Secondary Slot") && tArgs == 4)
    {
        rEntity->SetSecondarySlot(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Pushes the ability in the given slot to the activity stack.
    else if(!strcasecmp(rSwitchType, "Push Ability In Slot") && tArgs == 3)
    {
        void *rAbility = rEntity->GetAbilityBySlot(lua_tointeger(L, 2), lua_tointeger(L, 3));
        DataLibrary::Fetch()->PushActiveEntity(rAbility);
    }
    //--Adds an ability from the master list to the passive listing.
    else if(!strcasecmp(rSwitchType, "Add Passive Ability") && tArgs == 2)
    {
        rEntity->AddPassiveAbility(lua_tostring(L, 2));
    }
    //--Removes the named ability.
    else if(!strcasecmp(rSwitchType, "Remove Ability") && tArgs == 2)
    {
        rEntity->RemoveAbility(lua_tostring(L, 2));
    }
    ///--[Equipment]
    //--Creates a new equipment slot for the character.
    else if(!strcasecmp(rSwitchType, "Create Equipment Slot") && tArgs == 2)
    {
        rEntity->CreateEquipmentSlot(lua_tostring(L, 2));
    }
    //--Sets display name for the given equipment slot.
    else if(!strcasecmp(rSwitchType, "Set Equipment Slot Display Name") && tArgs >= 3)
    {
        rEntity->SetEquipmentSlotDisplayName(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets whether or not the given equipment slot is used to compute stats.
    else if(!strcasecmp(rSwitchType, "Set Equipment Slot Is Used For Statistics") && tArgs == 3)
    {
        rEntity->SetEquipmentSlotIsUsedForStats(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets whether or not the given equipment slot is used for tag application.
    else if(!strcasecmp(rSwitchType, "Set Equipment Slot Is Used For tags") && tArgs == 3)
    {
        rEntity->SetEquipmentSlotIsUsedForTags(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets whether or not the named slot can be empty. This spawns the Unequip entry on the equipment list.
    else if(!strcasecmp(rSwitchType, "Set Equipment Slot Can Be Empty") && tArgs == 3)
    {
        rEntity->SetEquipmentSlotCanBeEmpty(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets which equipment item is used to compute damage type.
    else if(!strcasecmp(rSwitchType, "Set Equipment Slot Used For Weapon Damage") && tArgs == 2)
    {
        int tSlot = rEntity->GetSlotOfEquipmentByName(lua_tostring(L, 2));
        rEntity->SetSlotUsedForWeaponDamage(tSlot);
    }
    //--Marks a slot as an alt-weapon slot.
    else if(!strcasecmp(rSwitchType, "Set Equipment Slot Is Alt Weapon") && tArgs == 3)
    {
        rEntity->SetEquipmentSlotIsAltWeapon(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--If true, the equipment slot will not show gems in the UI. Used for slots which have stats
    //  disabled but could have gem slotted equipment in them.
    else if(!strcasecmp(rSwitchType, "Set Equipment Slot No Gems") && tArgs == 3)
    {
        rEntity->SetEquipmentSlotNoGems(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Equips the named item in the inventory to the named slot. May fail if not compatible.
    else if(!strcasecmp(rSwitchType, "Equip Item To Slot") && tArgs == 3)
    {
        //--Make sure the item exists.
        AdventureItem *rCheckItem = AdventureInventory::Fetch()->GetItem(lua_tostring(L, 3));
        if(!rCheckItem) return 0;

        //--Equip it. If it was accepted, liberate it from the inventory.
        bool tEquippedItem = rEntity->EquipItemToSlot(lua_tostring(L, 2), rCheckItem);
        if(tEquippedItem) AdventureInventory::Fetch()->LiberateItemP(rCheckItem);

        //--Check the last registered item, which will be the item unequipped. If it exists, remove any gems from it.
        AdventureItem *rLastReggedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
        if(rLastReggedItem)
        {
            for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
            {
                AdventureItem *rPrevGem = rLastReggedItem->RemoveGemFromSlot(i);
                if(rPrevGem)
                {
                    AdventureInventory::Fetch()->RegisterItem(rPrevGem);
                }
            }
        }

        //--Clear this flag.
        AdventureInventory::Fetch()->rLastReggedItem = NULL;
    }
    //--Specially used for the loading routines, equips the item marked by the AdventureInventory as the last equipment item to the
    //  slot, as opposed to whichever one is found that matches the name. This allows gems to stay in the right items.
    else if(!strcasecmp(rSwitchType, "Equip Marked Item To Slot") && tArgs == 2)
    {
        //--Make sure the item exists.
        AdventureItem *rCheckItem = AdventureInventory::Fetch()->GetMarkedEquipment();
        if(!rCheckItem) return 0;

        //--Equip it. If it was accepted, liberate it from the inventory.
        bool tEquippedItem = rEntity->EquipItemToSlot(lua_tostring(L, 2), rCheckItem);
        if(tEquippedItem) AdventureInventory::Fetch()->LiberateItemP(rCheckItem);
    }
    //--Unequips the item in the given slot.
    else if(!strcasecmp(rSwitchType, "Unequip Slot") && tArgs == 2)
    {
        rEntity->EquipItemToSlot(lua_tostring(L, 2), (AdventureItem *)AdventureInventory::xrDummyUnequipItem);
    }
    //--Pushes the item in the given slot onto the activity stack. Can push NULL.
    else if(!strcasecmp(rSwitchType, "Push Item In Slot S") && tArgs == 2)
    {
        void *rCheckItem = rEntity->GetEquipmentBySlotS(lua_tostring(L, 2));
        DataLibrary::Fetch()->PushActiveEntity(rCheckItem);
    }
    //--Pushes the item in the given slot onto the activity slot, by index. Can push NULL.
    else if(!strcasecmp(rSwitchType, "Push Item In Slot I") && tArgs == 2)
    {
        void *rCheckItem = rEntity->GetEquipmentBySlotI(lua_tointeger(L, 2));
        DataLibrary::Fetch()->PushActiveEntity(rCheckItem);
    }
    //--Switches the equipment in the two named slots.
    else if(!strcasecmp(rSwitchType, "Swap Equipment In Slots S") && tArgs == 3)
    {
        rEntity->SwapEquipmentSlots(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Profiles]
    //--Wipes all profile data, but recreates a "Default" profile.
    else if(!strcasecmp(rSwitchType, "Wipe Profiles") && tArgs == 1)
    {
        rEntity->ClearProfiles();
    }
    //--Creates a new profile. Fails if it's the "Default" profile.
    else if(!strcasecmp(rSwitchType, "Create Profile") && tArgs == 2)
    {
        AbilityProfile *nProfile = (AbilityProfile *)starmemoryalloc(sizeof(AbilityProfile));
        nProfile->Initialize();
        rEntity->RegisterProfile(lua_tostring(L, 2), nProfile);
    }
    //--Sets the name of the ability in the given slot for the given profile.
    else if(!strcasecmp(rSwitchType, "Set Profile Ability") && tArgs == 5)
    {
        rEntity->SetProfileAbility(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5));
    }
    ///--[TF Indicators]
    //--Sets how many indicators appear above the entity's UI frame. Only in AbyssCombat.
    else if(!strcasecmp(rSwitchType, "TF Indicator Count") && tArgs == 2)
    {
        rEntity->SetTFIndicatorCount(lua_tointeger(L, 2));
    }
    //--Sets which indicators appear above the entity's UI frame. Only in AbyssCombat.
    else if(!strcasecmp(rSwitchType, "TF Indicator Image") && tArgs == 2)
    {
        rEntity->SetTFIndicatorIcon(lua_tostring(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatEntity_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
