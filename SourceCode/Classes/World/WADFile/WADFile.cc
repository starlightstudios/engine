//--Base
#include "WADFile.h"

//--Classes
#include "Skybox.h"
#include "WorldLight.h"
#include "WorldPolygon.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarCamera.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"
#include "GlDfn.h"
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Debug]
//#define WF_DEBUG
#ifdef WF_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
WADFile::WADFile()
{
    ///--[Debug]
    DebugPush(true, "WADFile:Constructor - begin.\n");

    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_WAD_FILE;

    ///--[RootLevel]
    //--System

    ///--[WADFile]
    //--System
    mIsReady = false;
    mShaderCode = 0;

    //--Storage
    mLumps = 0;
    mDirectoryEntries = NULL;

    //--Levels
    DebugPrint("Clearing level data.\n");
    mLevelsTotal = 32;
    SetMemoryData(__FILE__, __LINE__);
    mLevels = (WAD_Level *)starmemoryalloc(sizeof(WAD_Level) * mLevelsTotal);

    //--Active Level
    mHasActiveLevel = false;
    mLinedefsTotal = 0;
    mLinedefs = NULL;
    mSidedefsTotal = 0;
    mSidedefs = NULL;
    mVerticesTotal = 0;
    mVertices = NULL;
    mSectorsTotal = 0;
    mSectors = NULL;

    //--World Sprite Registry
    mWorldSpritesTotal = 0;
    mWorldSprites = NULL;
    mWorldSpriteConstructionList = NULL;

    //--Thing Lookups
    mThingLookupsTotal = 0;
    mThingLookups = NULL;

    //--Lighting Information
    mLightsTotal = 0;
    mLightList = NULL;

    //--Compiled Data
    mArbitraryPolysConstructionList = NULL;
    rActiveShadowList = NULL;
    mHeightPolysTotal = 0;
    rHeightPolys = NULL;

    //--Precached level data
    mIsPrecacheReady = false;
    mFloorPolysTotal = 0;
    mFloorPolys = NULL;
    mWallPolysTotal = 0;
    mWallPolys = NULL;

    //--PatchNames
    mPatchesTotal = 0;
    mPatchNames = NULL;

    //--Palette data
    DebugPrint("Clearing palette data.\n");
    for(int i = 0; i < 256; i ++)
    {
        mPaletteData[i].SetRGBAI(255, 255, 255, 255);
    }

    //--Textures data
    mFlatTexturesTotal = 0;
    mFlatTextures = NULL;
    mWallTexturesTotal = 0;
    mWallTextures = NULL;

    //--Global Texture Geometry
    DebugPrint("Setting texture lookups.\n");
    mIsCompressedTableMasterCopy = false;
    mCompressedTextureTable = new StarLinkedList(false);
    mGlobalTexturePolyList = new StarLinkedList(true);

    //--Skybox.
    mLocalSkybox = NULL;

    //--GL Rendering List
    mTexCoordAtlas = NULL;
    mTextureAtlas = NULL;
    mLightmapAtlas = NULL;
    mTriangleCount = 0;
    mListHandle = 0;
    mDataBufferHandle = 0;
    mVertexBufferHandle = 0;

    ///--[Debug]
    DebugPop("WADFile:Constructor - completed.\n");
}
WADFile::~WADFile()
{
    Clear(true);
}

///--[Public Statics]
bool WADFile::xIsRecurse = false;
float WADFile::xStaticArea = 0.0f;
float WADFile::xRunningArea = 0.0f;
bool WADFile::xAllowDoomSkyHack = false;
bool WADFile::xRenderNormals = false;

//--Allows rendering to improve its speed by rendering with Vertex Array Objects. This requires
//  more processing than usual, since we need to build an atlas of all textures and lightmaps to be
//  used during the render process. It also requires a shader.
bool WADFile::xRenderWithVAOs = false;

//--Set this to true to render textures. If textures are not rendering but lightmaps are, then the
//  lightmaps will render with no blending. If neither is set to render, wireframes will render instead.
bool WADFile::xRenderTextures = true;

//--Set this to true to render lightmaps over textures (if textures are enabled) or to render just
//  the lightmaps (if disabled). If both lightmaps and textures are disabled, wireframes will
//  render instead. Note that disabling lightmaps will also cause the engine to not build lights.
bool WADFile::xRenderLightmaps = false;

//--Set to true to render wireframes even if other faces are rendering. Useful for diagnosing errors
//  in world geometry.
bool WADFile::xAlwaysRenderWireframes = false;

//--Sets whether or not the engine will build a texture atlas. The atlas must be built for VAO rendering
//  to work, otherwise what flag is ignored.
bool WADFile::xDisallowTextureAtlas = false;

//--Force the texture atlas to render with NPOT texturing. This usually means increasing its size.
bool WADFile::xForceNPOTAtlas = false;

///===================================== Property Queries =========================================
void WADFile::GetPositionOfVertex(int pIndex, float &sX, float &sY)
{
    if(pIndex < 0 || pIndex >= mVerticesTotal) return;
    sX = mVertices[pIndex].mX;
    sY = mVertices[pIndex].mY;
}
float WADFile::GetHeightAtPoint(float pX, float pY)
{
    //--Returns the height at the given point. This always ignores 3D floors, or may incorrectly
    //  place you on top of a 3D floor.
    if(mHeightPolysTotal < 1)
    {
        WAD_FloorPoly *rContainingPolygon = FindContainingFloorPoly(pX, pY, mGlobalTexturePolyList, false);
        if(!rContainingPolygon) return 0.0f;

        //--Return the floor height of that polygon.
        return rContainingPolygon->mFloorHeight;
    }

    //--If the arbitrary list has been built, we can use the compiled data. This is faster.
    for(int i = 0; i < mHeightPolysTotal; i ++)
    {
        if(rHeightPolys[i]->IsPointInPlanarPolygon(pX, pY))
        {
            return rHeightPolys[i]->GetHeightAt(pX, pY);
        }
    }
    return 0.0f;
}
float WADFile::GetHeightAtPoint(float pX, float pY, float pZ)
{
    //--Returns the height at the given point which is lower than what is provided, assuming 3D
    //  floors exist. The lowest value that can be returned is the floor height of the containing
    //  polygon.
    //--If a 3D floor exists that is higher than the floor, but lower than the provided Z, that
    //  is returned. If the point is outside any polys, returns 0.0f.
    if(mHeightPolysTotal < 1)
    {
        //--Check if any polygon esconces this point.
        WAD_FloorPoly *rContainingPolygon = FindContainingFloorPoly(pX, pY, mGlobalTexturePolyList, false);
        if(!rContainingPolygon) return 0.0f;

        //--Check all 3D floors for the highest point lower than pZ.
        float tHighestPoint = rContainingPolygon->mFloorHeight;
        for(int i = 0; i < rContainingPolygon->m3DFloorsTotal; i ++)
        {
            //--Make sure the sector isn't out of range.
            if(rContainingPolygon->m3DFloorReferenceSlots[i] < 0 || rContainingPolygon->m3DFloorReferenceSlots[i] >= mSectorsTotal) continue;
            UDMFSector *rControlSector = &mSectors[rContainingPolygon->m3DFloorReferenceSlots[i]];

            //--It will probably be higher, but we check anyway. We're actually interested in the ceiling height since that
            //  is what renders as the floor.
            if(rControlSector->mHeightCeiling > tHighestPoint && rControlSector->mHeightCeiling < pZ)
            {
                tHighestPoint = rControlSector->mHeightCeiling;
            }
        }

        //--Now check against the decomposed polygons. These polygons, if they have exactly 3 points, can have slopes.
        WAD_FloorPoly *rBasePolygon = FindContainingFloorPoly(pX, pY);
        if(rBasePolygon && rBasePolygon->mVertexesTotal == 3)
        {
            //--If any one vertex has a special floor Z, we need to check this slope.
            if(rBasePolygon->mVertices[0].mHasSpecialFloor && rBasePolygon->mVertices[1].mHasSpecialFloor && rBasePolygon->mVertices[2].mHasSpecialFloor)
            {
                //--Compute the height of the point assuming all three have legal slope values.
                float tVecUX = rBasePolygon->mVertices[1].mX      - rBasePolygon->mVertices[0].mX;
                float tVecUY = rBasePolygon->mVertices[1].mY      - rBasePolygon->mVertices[0].mY;
                float tVecUZ = rBasePolygon->mVertices[1].mFloorZ - rBasePolygon->mVertices[0].mFloorZ;
                float tVecVX = rBasePolygon->mVertices[2].mX      - rBasePolygon->mVertices[0].mX;
                float tVecVY = rBasePolygon->mVertices[2].mY      - rBasePolygon->mVertices[0].mY;
                float tVecVZ = rBasePolygon->mVertices[2].mFloorZ - rBasePolygon->mVertices[0].mFloorZ;
                float tNormalX = (tVecUY * tVecVZ) - (tVecUZ * tVecVY);
                float tNormalY = (tVecUZ * tVecVX) - (tVecUX * tVecVZ);
                float tNormalZ = (tVecUX * tVecVY) - (tVecUY * tVecVX);

                //--Find the Z coordinate at the given point.
                if(tNormalZ != 0.0f)
                {
                    float tVectorX = pX - rBasePolygon->mVertices[0].mX;
                    float tVectorY = pY - rBasePolygon->mVertices[0].mY;
                    float tVectorZ = -((tNormalY * tVectorY) + (tNormalX * tVectorX)) / tNormalZ;
                    float tCheckHeight = rBasePolygon->mVertices[0].mFloorZ + tVectorZ; //Z coordinate is flipped, so add it here. Normally it's a subtraction.
                    return tCheckHeight;
                }
            }
        }

        //--Pass back.
        return tHighestPoint;
    }

    //--If the arbitrary list has been built, we can use the compiled data. This is faster.
    int tFoundMatches = 0;
    float tHighest = -10000.0f;
    float tLowest  = 10000.0f;
    for(int i = 0; i < mHeightPolysTotal; i ++)
    {
        if(rHeightPolys[i]->IsPointInPlanarPolygon(pX, pY))
        {
            //--Match, set flag.
            tFoundMatches ++;

            //--Get the height.
            float tComparison = rHeightPolys[i]->GetHeightAt(pX, pY);

            //--If this height is above the current highest, we need to move up as if this is a noclip case.
            if(tComparison > tHighest && tComparison <= pZ) tHighest = tComparison;
            if(tComparison < tLowest)                       tLowest  = tComparison;
        }
    }

    //--No matches, return default of 0.0f.
    if(tHighest == -10000.0f)
    {
        if(tFoundMatches == 0) return 0.0f;
        return tLowest;
    }
    return tHighest;
}
int WADFile::GetIndexOfCompressedTexture(StarBitmap *pBitmap)
{
    //--Can legally return -1 if the pointer was not found.
    return mCompressedTextureTable->GetSlotOfElementByPtr(pBitmap);
}
bool WADFile::IsSkyTexture(void *pCheckPtr)
{
    return (rSkyTexture == pCheckPtr);
}

///======================================= Manipulators ===========================================
void WADFile::SetShaderCode(int pCode)
{
    mShaderCode = pCode;
}

///======================================= Core Methods ===========================================
void WADFile::Clear(bool pIsExitCase)
{
    //--[Debug]
    DebugPush(true, "WADFile:Clear - Executing with flag %i\n", pIsExitCase);

    //--[Free Memory]
    //--Storage
    DebugPrint("Dealloc directories.\n");
    for(int i = 0; i < (int)mLumps; i ++)
    {
        free(mDirectoryEntries[i].mData);
    }
    free(mDirectoryEntries);

    //--Clear active Level
    DebugPrint("Clearing level lookups.\n");
    free(mLinedefs);
    free(mSidedefs);
    free(mVertices);
    free(mSectors);

    //--World Sprite Registry
    DebugPrint("Dealloc sprite registry.\n");
    free(mWorldSprites);
    delete mWorldSpriteConstructionList;

    //--Thing Lookups
    free(mThingLookups);

    //--Lighting Information
    for(int i = 0; i < mLightsTotal; i ++)
    {
        delete mLightList[i];
    }
    free(mLightList);

    //--Compiled Data
    DebugPrint("Dealloc polygon lists.\n");
    delete mArbitraryPolysConstructionList;
    free(rHeightPolys);

    //--Precached level data
    DebugPrint("Dealloc precached data.\n");
    for(int i = 0; i < mFloorPolysTotal; i ++)
    {
        free(mFloorPolys[i].mVertices);
    }
    free(mFloorPolys);
    free(mWallPolys);

    //--PatchNames
    DebugPrint("Dealloc patches.\n");
    for(int i = 0; i < mPatchesTotal; i ++) free(mPatchNames[i]);
    free(mPatchNames);

    //--Textures
    DebugPrint("Dealloc textures.\n");
    for(int i = 0; i < mFlatTexturesTotal; i ++)
    {
        delete mFlatTextures[i].mImage;
    }
    free(mFlatTextures);
    for(int i = 0; i < mWallTexturesTotal; i ++)
    {
        delete mWallTextures[i].mImage;
    }
    free(mWallTextures);

    //--Global Texture Geometry
    DebugPrint("Dealloc global texture lookups.\n");
    if(mIsCompressedTableMasterCopy && mCompressedTextureTable) mCompressedTextureTable->SetDeallocation(true);
    delete mCompressedTextureTable;
    delete mGlobalTexturePolyList;

    //--Skybox.
    delete mLocalSkybox;

    //--GL Rendering List
    DebugPrint("Dealloc rendering lists.\n");
    free(mTexCoordAtlas);
    delete mTextureAtlas;
    delete mLightmapAtlas;
    mTriangleCount = 0;
    glDeleteLists(mListHandle, 1);
    sglDeleteBuffers(1, &mDataBufferHandle);
    sglDeleteVertexArrays(1, &mVertexBufferHandle);

    //--[Clear flags]
    //--System
    DebugPrint("Resetting flags.\n");
    mIsReady = false;

    //--Storage
    mLumps = 0;
    mDirectoryEntries = NULL;

    //--Levels
    memset(mLevels, 0, sizeof(WAD_Level) * mLevelsTotal);
    for(int i = 0; i < (int)mLevelsTotal; i ++)
    {
        mLevels[i].mIsUDMF = false;
        mLevels[i].mLevelNumber = -1;
    }

    //--Active Level
    mHasActiveLevel = false;
    mLinedefsTotal = 0;
    mLinedefs = NULL;
    mSidedefsTotal = 0;
    mSidedefs = NULL;
    mVerticesTotal = 0;
    mVertices = NULL;
    mSectorsTotal = 0;
    mSectors = NULL;

    //--World Sprite Registry
    mWorldSpritesTotal = 0;
    mWorldSprites = NULL;
    mWorldSpriteConstructionList = NULL;

    //--Thing Lookups
    mThingLookupsTotal = 0;
    mThingLookups = NULL;

    //--Light Listing
    mLightsTotal = 0;
    mLightList = NULL;

    //--Compiled Data
    mArbitraryPolysConstructionList = NULL;
    rActiveShadowList = NULL;
    mHeightPolysTotal = 0;
    rHeightPolys = NULL;

    //--Modified level data
    mIsPrecacheReady = false;
    mFloorPolysTotal = 0;
    mFloorPolys = NULL;
    mWallPolysTotal = 0;
    mWallPolys = NULL;

    //--Patch names
    mPatchesTotal = 0;
    mPatchNames = NULL;

    //--Textures data
    rSkyTexture = NULL;
    mFlatTexturesTotal = 0;
    mFlatTextures = NULL;
    mWallTexturesTotal = 0;
    mWallTextures = NULL;

    //--Global Texture Geometry
    mIsCompressedTableMasterCopy = false;
    mCompressedTextureTable = NULL;
    mGlobalTexturePolyList = NULL;
    if(!pIsExitCase)
    {
        mCompressedTextureTable = new StarLinkedList(false);
        mGlobalTexturePolyList = new StarLinkedList(true);
    }

    //--Skybox.
    mLocalSkybox = NULL;

    //--GL Rendering List
    mTexCoordAtlas = NULL;
    mTextureAtlas = NULL;
    mLightmapAtlas = NULL;
    mTriangleCount = 0;
    mListHandle = 0;
    mDataBufferHandle = 0;
    mVertexBufferHandle = 0;

    //--[Debug]
    DebugPop("WADFile:Clear - completed.\n");
}
void WADFile::BuildCompressedTextureTable()
{
    //--Builds a table that contains only textures that have at least one reference in the current level.
    //  The textures will not be in any particular order, just the one in which they were found.
    //--This requires the arbitrary polygon list to have been built, otherwise it does nothing.
    mCompressedTextureTable->ClearList();
    if(!mArbitraryPolysConstructionList) return;

    //--We're building this list based on pointers, so there's no way to improve its efficiency. Two textures
    //  which contain identical information will get written twice if they had a different name.
    WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        //--Scan backwards through the table. If the texture is not found, add it.
        if(!mCompressedTextureTable->IsElementOnList(rPolygon->rTexture))
        {
            mCompressedTextureTable->AddElementAsTail("X", rPolygon->rTexture);
        }

        //--Next polygon.
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Report.
    DebugManager::ForcePrint("WADFile:BuildCompressedTextureTable - %i unique textures on the list.\n", mCompressedTextureTable->GetListSize());
}
WAD_Directory_Entry *WADFile::FindLump(const char *pName)
{
    //--Finds and returns the specified lump's directory and data, if the data exists. Can return
    //  NULL if the entry was not found.
    if(!mIsReady) return NULL;

    //--Search.
    for(int i = 0; i < (int)mLumps; i ++)
    {
        if(!strcmp(mDirectoryEntries[i].mType, pName)) return &mDirectoryEntries[i];
    }

    //--Not found!
    return NULL;
}
WAD_Directory_Entry *WADFile::FindLumpNonFlat(const char *pName)
{
    //--Finds and returns the specified lump, but only if it's not a flat-type. Some lumps are named
    //  twice in the file, but are of different types.
    if(!mIsReady) return NULL;

    //--Search.
    for(int i = 0; i < (int)mLumps; i ++)
    {
        if(!strcasecmp(mDirectoryEntries[i].mType, pName))
        {
            if(mDirectoryEntries[i].mIsFlat)
            {
            }
            else
            {
                return &mDirectoryEntries[i];
            }
        }
    }

    //--Not found!
    return NULL;
}
void WADFile::SetLevelPointers(int pNumber)
{
    //--Gives the numbered level its raw data pointers. The data is still held in the large list,
    //  each level just gets pointers to the data sets.
    if(pNumber < 0 || pNumber >= (int)mLevelsTotal) return;

    //--Setup.
    int tIndex = -1;
    char tSearchBuf[8];

    //--Look for the Doom format. Maps go E1M1-E1M9-E2M1-E2M9 etc.
    int tEpisode = pNumber / 10;
    int tMission = pNumber % 10;
    sprintf(tSearchBuf, "E%iM%i", tEpisode+1, tMission+1);

    //--Scan the lumps list for the numbered level.
    for(int i = 0; i < (int)mLumps; i ++)
    {
        if(!strcmp(mDirectoryEntries[i].mType, tSearchBuf))
        {
            tIndex = i+1;
        }
    }

    //--If there was no match, check the format used for Doom2, which is MAPXX (01-32)
    if(tIndex == -1)
    {
        //--Scan the lumps list for the numbered level. Count starts at 01 for Doom2.
        sprintf(tSearchBuf, "MAP%02i", pNumber + 1);
        for(int i = 0; i < (int)mLumps; i ++)
        {
            if(!strcmp(mDirectoryEntries[i].mType, tSearchBuf))
            {
                tIndex = i+1;
            }
        }
    }

    //--Fail if zero matches were found. For multiple matches, use the last one.
    if(tIndex == -1) return;

    //--Loop until the next non-component is found. This can be a map, or a texture lump, or any other
    //  lump that is not a level piece.
    bool tIsComplete = false;
    while(!tIsComplete)
    {
        //--Check the type.
        WAD_Directory_Entry *rEntry = &mDirectoryEntries[tIndex];

        //--UDMF entry. If this is found, the other lumps will be ignored (though still referenced).
        if(!strcasecmp(rEntry->mType, "TEXTMAP"))
        {
            mLevels[pNumber].mIsUDMF = true;
            mLevels[pNumber].rTextmapLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "THINGS"))
        {
            mLevels[pNumber].rThingsLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "LINEDEFS"))
        {
            mLevels[pNumber].rLinedefLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "SIDEDEFS"))
        {
            mLevels[pNumber].rSidedefLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "VERTEXES"))
        {
            mLevels[pNumber].rVertexLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "SEGS"))
        {
            mLevels[pNumber].rSegmentLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "SSECTORS"))
        {
            mLevels[pNumber].rSSectorLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "NODES"))
        {
            mLevels[pNumber].rNodeLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "SECTORS"))
        {
            mLevels[pNumber].rSectorLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "REJECT"))
        {
            mLevels[pNumber].rRejectLump = rEntry;
        }
        else if(!strcasecmp(rEntry->mType, "BLOCKMAP"))
        {
            mLevels[pNumber].rBlockmapLump = rEntry;
        }
        //--No match.
        else
        {
            tIsComplete = true;
        }

        //--Increment up.
        tIndex ++;
    }

    //--Flag the level as ready if all its types are specified.
    if(mLevels[pNumber].rThingsLump && mLevels[pNumber].rLinedefLump && mLevels[pNumber].rSidedefLump && mLevels[pNumber].rVertexLump &&
       mLevels[pNumber].rSegmentLump && mLevels[pNumber].rSSectorLump && mLevels[pNumber].rNodeLump && mLevels[pNumber].rSectorLump &&
       mLevels[pNumber].rRejectLump && mLevels[pNumber].rBlockmapLump)
    {
        mLevels[pNumber].mLevelNumber = pNumber;
    }
    //--Flag the level as ready if it's UDMF, which doesn't need the other lumps.
    else if(mLevels[pNumber].mIsUDMF)
    {
        mLevels[pNumber].mLevelNumber = pNumber;
    }

    //--Lastly, check if there's a floating TEXTMAP lump. When merging, a level which was modified from being WAD to UDMF
    //  will have an extra lump at the tail end of the list.
    for(int i = 0; i < (int)mLumps; i ++)
    {
        //fprintf(stderr, "Checking entry %s %i\n", mDirectoryEntries[i].mType, mDirectoryEntries[i].mBelongsToMap);
        if(!strcasecmp(mDirectoryEntries[i].mType, "TEXTMAP") && mDirectoryEntries[i].mBelongsToMap == pNumber)
        {
            mLevels[pNumber].mIsUDMF = true;
            mLevels[pNumber].mLevelNumber = pNumber;
            mLevels[pNumber].rTextmapLump = &mDirectoryEntries[i];
            fprintf(stderr, "--> This level went from WAD to UDMF.\n");
        }
    }

    //--Debug report.
    DebugManager::PushPrint(false, "[Level Report] %s\n", tSearchBuf);

    DebugManager::Print("Things: %p\n", mLevels[pNumber].rThingsLump);
    if(mLevels[pNumber].rThingsLump)
    {
        DebugManager::Print(" Size: %i Things: %i\n", mLevels[pNumber].rThingsLump->mSize, mLevels[pNumber].rThingsLump->mSize / 10);
    }

    DebugManager::PopPrint("[Level Report] Complete.\n");
}
void WADFile::ActivateLevel(int pNumber)
{
    //--Fills the active level data with the data from the specified lump. This will not handle UDMFs correctly,
    //  this is based on the assumption that the level is a standard Doom format instead.
    DebugManager::PushPrint(false, "[Activate Level] Activating room %i vs %i\n", pNumber, (int)mLevelsTotal);
    mHasActiveLevel = false;

    //--Range check.
    if(pNumber < 0 || pNumber >= (int)mLevelsTotal)
    {
        DebugManager::PopPrint("Error, room %i was out of range.\n", pNumber);
        return;
    }
    //--Make sure the level is not -1, meaning it was not found in the lookups.
    if(mLevels[pNumber].mLevelNumber == -1)
    {
        DebugManager::PopPrint("Room %i was -1 in the lookups. Skipping.\n", pNumber);
        return;
    }

    //--Clone Pointers.
    UDMFLinedef *tLinedefClone = NULL;
    UDMFSidedef *tSidedefClone = NULL;

    //--Get palette data. This must be done before the UDMF runs since it can load things, which need palette data.
    DebugManager::Print("Loading palette information.\n");
    WAD_Directory_Entry *rPaletteLump = FindLump("PLAYPAL");
    if(rPaletteLump)
    {
        for(int i = 0; i < 256; i ++)
        {
            int tIndex = i * 3;
            mPaletteData[i].SetRGBI(rPaletteLump->mData[tIndex+0], rPaletteLump->mData[tIndex+1], rPaletteLump->mData[tIndex+2]);
        }
    }

    //--If the level is a WAD file, run this routine to build the data.
    if(!mLevels[pNumber].mIsUDMF)
    {
        //--Subroutines which crossload information from WAD format to UDMF format.
        DebugManager::Print("Converting WAD data to UDMF.\n");
        LoadVertexData(mLevels[pNumber].rVertexLump);
        LoadLinedefData(mLevels[pNumber].rLinedefLump);
        LoadSidedefData(mLevels[pNumber].rSidedefLump);
        LoadSectorData(mLevels[pNumber].rSectorLump);
    }
    //--If this is a UDMF file, run this routine instead.
    else
    {
        //--Call a subroutine to handle this.
        DebugManager::Print("Running UDMF subroutine.\n");
        LoadAsUDMF(mLevels[pNumber].rTextmapLump);
    }

    //--Clone the linedef pointer data.
    DebugManager::Print("Cloning linedef/sidedef information.\n");
    SetMemoryData(__FILE__, __LINE__);
    tLinedefClone = (UDMFLinedef *)starmemoryalloc(sizeof(UDMFLinedef) * mLinedefsTotal);
    memcpy(tLinedefClone, mLinedefs, sizeof(UDMFLinedef) * mLinedefsTotal);

    //--Clone the sidedef pointer data.
    SetMemoryData(__FILE__, __LINE__);
    tSidedefClone = (UDMFSidedef *)starmemoryalloc(sizeof(UDMFSidedef) * mSidedefsTotal);
    memcpy(tSidedefClone, mSidedefs, sizeof(UDMFSidedef) * mSidedefsTotal);

    //--Load textures from sector data.
    DebugManager::Print("Loading texture information.\n");
    LoadTextures();

    //--Precache the polygon data for rendering.
    DebugManager::Print("Precaching floors.\n");
    ClearPrecache();
    PrecacheFloors();

    //--Re-load the linedef data. Linedefs are damaged during floor precaching due to polygon
    //  reconstruction. It's much easier to reload the data than to track and fix it.
    DebugManager::Print("Restoring sidedef/linedef data.\n");
    free(mLinedefs);
    free(mSidedefs);
    mLinedefs = tLinedefClone;
    mSidedefs = tSidedefClone;

    //--Precache the walls with the restored data.
    DebugManager::Print("Precaching walls.\n");
    PrecacheWalls();

    //--3D Floor generation. Can add new wall polys!
    DebugManager::Print("Building 3D floor info.\n");
    Build3DFloorInfo();

    //--Run the Things function. Things are largely optional but make the level look nicer.
    DebugManager::Print("Scanning things lump.\n");
    if(!mLevels[pNumber].mIsUDMF) ProcessThingsLump(mLevels[pNumber].rThingsLump);

    //--All checks passed, flag as ready.
    mHasActiveLevel = true;
    DebugManager::PopPrint("[Activate Level] Level has finished activation.\n");
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
void WADFile::ParseFile(const char *pPath)
{
    //--The given file is parsed and the data is loaded into the class. If anything goes wrong, the
    //  mIsReady flag will remain false to indicate the data was invalid or incomplete.
    DebugManager::PushPrint(false, "[WADFile] Parsing %s\n", pPath);
    if(!pPath)
    {
        DebugManager::PopPrint("[WADFile] Finished, path was NULL.\n");
        return;
    }

    //--[Verify]
    //--Make sure the file actually exists.
    FILE *fInfile = fopen(pPath, "rb");
    if(!fInfile)
    {
        DebugManager::PopPrint("[WADFile] Finished, file not found.\n");
        return;
    }

    //--[Header]
    //--Make sure everything is a-okay using magic numbers.
    char tBuffer[5];
    fread(tBuffer, sizeof(char), 4, fInfile);
    tBuffer[4] = '\0';
    if(strcmp(tBuffer, "IWAD") && strcmp(tBuffer, "PWAD"))
    {
        fclose(fInfile);
        DebugManager::PopPrint("[WADFile] Finished, file was not an IWAD or PWAD.\n");
        return;
    }

    //--How many lumps to expect.
    int tExpectedLumps = 0;
    fread(&tExpectedLumps, sizeof(uint32_t), 1, fInfile);
    DebugManager::Print("Report %i lumps.\n", tExpectedLumps);

    //--Allocate lump space.
    SetMemoryData(__FILE__, __LINE__);
    WAD_Directory_Entry *nNewEntries = (WAD_Directory_Entry *)starmemoryalloc(sizeof(WAD_Directory_Entry) * tExpectedLumps);

    //--Directory info.
    uint32_t tDirectoryOffset;
    fread(&tDirectoryOffset, sizeof(uint32_t), 1, fInfile);
    DebugManager::Print("Directory offset: %i bytes.\n", tDirectoryOffset);

    //--[Directories]
    //--Directory flags.
    bool tIsFlat = false;
    int8_t tActiveMapCode = -1;

    //--Each data lump has a directory entry which specifies its location. These are sequential.
    fseek(fInfile, tDirectoryOffset, SEEK_SET);
    for(int i = 0; i < (int)tExpectedLumps; i ++)
    {
        //--Data read.
        fread(&nNewEntries[i].mOffset, sizeof(uint32_t), 1, fInfile);
        fread(&nNewEntries[i].mSize, sizeof(uint32_t), 1, fInfile);
        fread(&nNewEntries[i].mType, sizeof(char), 8, fInfile);
        nNewEntries[i].mType[8] = '\0';//This is so we can use string functions like strcmp.

        //--Load the data.
        if(nNewEntries[i].mSize != 0)
        {
            uint32_t tCurOffset = ftell(fInfile);
            fseek(fInfile, nNewEntries[i].mOffset, SEEK_SET);
                SetMemoryData(__FILE__, __LINE__);
                nNewEntries[i].mData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * nNewEntries[i].mSize);
                fread(nNewEntries[i].mData, sizeof(uint8_t), nNewEntries[i].mSize, fInfile);
            fseek(fInfile, tCurOffset, SEEK_SET);
        }
        //--Sizeless entry.
        else
        {
            nNewEntries[i].mData = NULL;
        }

        //--If this is the F_START entry, everything afterwards is a flat. The entry itself is not
        //  a flat!
        if(CompareEight(nNewEntries[i].mType, "F_START\0") || CompareEight(nNewEntries[i].mType, "FF_START"))
        {
            nNewEntries[i].mIsFlat = false;
            tIsFlat = true;
        }
        //--F_END entry stops the whole 'flat' thing.
        else if(CompareEight(nNewEntries[i].mType, "F_END\0\0\0"))
        {
            nNewEntries[i].mIsFlat = false;
            tIsFlat = false;
        }
        //--Set type by the flag.
        else
        {
            nNewEntries[i].mIsFlat = tIsFlat;
        }

        //--If this is a MAPXX tag, then flag the lump as belonging to the new map. Only certain types of objects can
        //  belong to maps, specifically THINGS, LINEDEFS, SIDEDEFS, VERTEXES, and SECTORS.
        if(!strncmp(nNewEntries[i].mType, "MAP", 3))
        {
            //--Determine which map it is. Negatives are ignored, must be 31 or lower. 0 is the lowest code.
            //  In Doom, the map tag starts at MAP01, so subtract 1.
            tActiveMapCode = ((nNewEntries[i].mType[3] - '0') * 10) + (nNewEntries[i].mType[4] - '0') - 1;
            if(tActiveMapCode > 31) tActiveMapCode = -1;
        }
        //--Episode tag.
        else if(!strncmp(nNewEntries[i].mType, "E1M", 3))
        {
            tActiveMapCode = ((nNewEntries[i].mType[3] - '0') * 1) - 1;
            if(tActiveMapCode > 31) tActiveMapCode = -1;
        }
        //--If this is one of the map components, it gains the map code.
        else if(CompareEight(nNewEntries[i].mType, "THINGS\0\0") || CompareEight(nNewEntries[i].mType, "LINEDEFS") || CompareEight(nNewEntries[i].mType, "SIDEDEFS") ||
                CompareEight(nNewEntries[i].mType, "VERTEXES")   || CompareEight(nNewEntries[i].mType, "SECTORS\0"))
        {
            nNewEntries[i].mBelongsToMap = tActiveMapCode;
        }
        //--All other components cannot belong to a map.
        else
        {
            nNewEntries[i].mBelongsToMap = -1;
        }

        //--Debug.
        DebugManager::Print("Lump %i: %s\n", i, nNewEntries[i].mType);
    }

    //--Close up the file. All data has been parsed.
    mIsReady = true;
    fclose(fInfile);

    //--If there were no directory entries, then that becomes the new count.
    if(!mDirectoryEntries)
    {
        mLumps = tExpectedLumps;
        mDirectoryEntries = nNewEntries;
    }
    //--Otherwise, we need to merge the wad lumps.
    else
    {
        //--Merge.
        MergeLumps(tExpectedLumps, nNewEntries);

        //--Deallocate. The data pointers will have been dropped here.
        free(nNewEntries);
    }

    //--All checks passed, flag object as ready.
    DebugManager::PopPrint("[WADFile] Finished successfully.\n");
}
void WADFile::MergeLumps(int pLumpsTotal, WAD_Directory_Entry *pLumps)
{
    //--Given a set of lumps in a malloc'd array, merges those lumps into the existing mDirectoryEntries array.
    //  In WAD format, if a lump has the same directory properties as another lump, the new one overwrites the old one.
    if(pLumpsTotal < 1 || !pLumps) return;

    //--Setup.
    bool tIsMarkerLump = false;
    int tCurrentLevel = -2;
    int tNeedsMoreListSize = 0;

    //--Check each lump and see if it has a duplicate lump in the new file.
    for(int i = 0; i < (int)mLumps; i ++)
    {
        //--Reset the level flag.
        tIsMarkerLump = false;
        tCurrentLevel = -2;

        //--Scan the new lump list. See if there's any duplicates.
        bool tFoundDuplicate = false;
        for(int p = 0; p < pLumpsTotal; p ++)
        {
            //--If this is a map marker, set that as the level.
            if(!strncasecmp(pLumps[p].mType, "E1M", 3))
            {
                tIsMarkerLump = true;
                tCurrentLevel = pLumps[p].mType[3] - '0' - 1;
            }
            //--End map marker.
            else if(!strcasecmp(pLumps[p].mType, "ENDMAP"))
            {
                tIsMarkerLump = true;
                tCurrentLevel = -2;
            }

            //--Set level ownership.
            pLumps[p].mBelongsToMap = tCurrentLevel;
            //fprintf(stderr, "Lump %s belong to %i\n", pLumps[p].mType, tCurrentLevel);

            //--Match.
            if(CompareEight(pLumps[p].mType, mDirectoryEntries[i].mType) && !tIsMarkerLump)
            {
                //--To match, both must either have the same level tag, or be of a negative level.
                if((pLumps[p].mBelongsToMap == mDirectoryEntries[i].mBelongsToMap && mDirectoryEntries[i].mBelongsToMap > -1) ||
                   (pLumps[p].mBelongsToMap < 0 && mDirectoryEntries[i].mBelongsToMap < 0))
                {
                    //--Flag.
                    tFoundDuplicate = true;

                    //--Free the original data, and overwrite it.
                    free(mDirectoryEntries[i].mData);
                    memcpy(&mDirectoryEntries[i], &pLumps[p], sizeof(WAD_Directory_Entry));

                    //--Remove references to old data.
                    memset(&pLumps[p], 0, sizeof(WAD_Directory_Entry));

                    //--Finish up.
                    //fprintf(stderr, "The lump %s matched a new lump %i, map %i\n", pLumps[p].mType, p, mDirectoryEntries[i].mBelongsToMap);
                    break;
                }
                //--Lumps matched, but might have been for different levels. Ignore.
                else
                {

                }
            }
        }

        //--If this is still false, then this is a new entry and the list will need it appended on the end.
        if(!tFoundDuplicate)
        {
            tNeedsMoreListSize ++;
            //fprintf(stderr, " No match found for lump %s %i\n", mDirectoryEntries[i].mType, mDirectoryEntries[i].mBelongsToMap);
        }
    }

    //--Were there any non-duplicates? The list needs to be resized.
    if(tNeedsMoreListSize)
    {
        //--Debug.
        //fprintf(stderr, "New lumps were acquired, list should now be %i entries.\n", mLumps + tNeedsMoreListSize);

        //--Realloc the list.
        mDirectoryEntries = (WAD_Directory_Entry *)realloc(mDirectoryEntries, sizeof(WAD_Directory_Entry) * (mLumps + tNeedsMoreListSize));

        //--Any unmarked entries now get appended on to the end of the wad file. Presumably the program knows what to do with them.
        for(int p = 0; p < pLumpsTotal; p ++)
        {
            //--Skip entries with nothing in their data. These were dupes.
            if(pLumps[p].mData == NULL) continue;

            //--Append it. Note that we take ownership of its data pointer.
            //fprintf(stderr, "Applying lump %s %i to slot %i\n", pLumps[p].mType, p, mLumps);
            memcpy(&mDirectoryEntries[mLumps], &pLumps[p], sizeof(WAD_Directory_Entry));
            mLumps ++;
        }
    }
}
void WADFile::BeginLevel(int pEntry)
{
    //--Find specific lumps and pull the data out of them. This must be done once all level info has been parsed.
    DebugManager::PushPrint(false, "WADFile:BeginLevel - running with code %i\n", pEntry);

    //--Wall Patch data needs to be pulled before any levels are loaded. This requires palette info.
    DebugManager::Print("Loading patch names.\n");
    LoadPatchNames();

    //--Set all the pointers up.
    DebugManager::Print("Setting level pointers.\n");
    SetLevelPointers(pEntry);
    DebugManager::Print("Activating level.\n");
    ActivateLevel(pEntry);

    //--Debug.
    DebugManager::PopPrint("WADFile:BeginLevel - complete.\n");
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
WADFile *WADFile::CreateFromFile(const char *pPath)
{
    WADFile *nFile = new WADFile();
    nFile->ParseFile(pPath);
    return nFile;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
