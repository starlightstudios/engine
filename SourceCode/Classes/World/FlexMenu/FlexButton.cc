//--Base
#include "FlexButton.h"

//--Classes
#include "FlexMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"

///========================================== System ==============================================
FlexButton::FlexButton()
{
    ///--[StarButton]
    //--System
    //--Location
    //--Execution

    ///--[FlexButton]
    //--System
    mIsHighlighted = false;
    mDontRenderHighlight = false;

    //--Constants
    cIndicatorOffset = -3.0f;
    cBarWidth = 3.0f;
    cBarHeight = 10.0f;
    cThickness = 3.0f;

    //--Display
    mFontSize = 1.0f;
    rOverrideFont = NULL;

    //--Options
    mIsOptionMode = false;
    mCurrentOption = 0;
    mOptionsTotal = 0;
    mOptions = NULL;

    //--Slider Mode
    mIsSliderMode = false;
    mCurrentSlider = 0;
    mSliderMin = 0;
    mSliderMax = 1;
    mIncrementBtnA = 0;
    mIncrementBtnB = 0;
    mSliderWidth = DEF_SLIDER_WID;
    mLyingLengthString = NULL;

    //--Slider Controls
    mIsDraggingSlider = false;

    //--Description Mode
    mDescription = NULL;
    rImgPointer = NULL;

    ///--[Construction]
    //--Resolves the images required for rendering.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Base Images
    Images.Data.rUIFont = rDataLibrary->GetFont("Flex Button Main");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(StarBitmap *));
}
FlexButton::~FlexButton()
{
    for(int i = 0; i < mOptionsTotal; i ++) free(mOptions[i]);
    free(mOptions);
    free(mLyingLengthString);
    free(mDescription);
}

///--[Private Statics]
bool FlexButton::xHighlightIsBackground = false;

///===================================== Property Queries =========================================
bool FlexButton::IsHighlighted()
{
    return mIsHighlighted;
}
float FlexButton::GetTextWidth()
{
    //--Determines the width of the button (minimum, no padding) using the font it currently has loaded.
    //  Switches behavior based on the FlexMenu's UI flag.
    float cUseFontSize = mFontSize;
    StarFont *rUseFont = NULL;
    if(FlexMenu::xUseClassicUI)
    {
        rUseFont = ResolveFont();
    }
    else
    {
        cUseFontSize = 1.0f;
        rUseFont = Images.Data.rUIFont;
    }

    //--Make sure the font resolved safely.
    if(!rUseFont) return 0.0f; //Error!

    //--If in slider mode, add some extra pixels for the slider.
    if(mIsSliderMode)
    {
        //--If there's a lying length, use that instead.
        if(mLyingLengthString)
        {
            return (rUseFont->GetTextWidth(mLyingLengthString) * cUseFontSize) + mSliderWidth + DEF_SLIDER_PADDING;
        }

        //--Normal case.
        return (rUseFont->GetTextWidth(mLocalName) * cUseFontSize) + mSliderWidth + DEF_SLIDER_PADDING;
    }

    return rUseFont->GetTextWidth(mLocalName) * cUseFontSize;
}
bool FlexButton::IsMouseOverMe(float pMouseX, float pMouseY)
{
    return IsPointWithin2DReal(pMouseX, pMouseY, mCoordinates);
}
bool FlexButton::IsOptionMode()
{
    return mIsOptionMode;
}
bool FlexButton::IsSliderMode()
{
    return mIsSliderMode;
}
int FlexButton::GetOptionCursor()
{
    return mCurrentOption;
}
int FlexButton::GetSliderCursor()
{
    return mCurrentSlider;
}
const char *FlexButton::GetDescription()
{
    return (const char *)mDescription;
}
StarBitmap *FlexButton::GetDescriptionImage()
{
    return rImgPointer;
}

///======================================== Manipulators ==========================================
void FlexButton::SetHighlight(bool pFlag)
{
    mIsHighlighted = pFlag;
}
void FlexButton::SetDontRenderHighlight(bool pFlag)
{
    mDontRenderHighlight = pFlag;
}
void FlexButton::SetFontSize(float pFactor)
{
    if(pFactor == 0.0f) pFactor = 0.000001f;
    mFontSize = pFactor;
}
void FlexButton::ActivateOptionMode(int pOptions)
{
    //--Clear old data.
    mIsOptionMode = false;
    mIsSliderMode = false;
    mCurrentOption = 0;
    for(int i = 0; i < mOptionsTotal; i ++) free(mOptions[i]);
    free(mOptions);
    mOptions = NULL;
    mOptionsTotal = 0;
    if(pOptions < 1) return;

    //--Allocate and NULL new space.
    mIsOptionMode = true;
    mOptionsTotal = pOptions;
    SetMemoryData(__FILE__, __LINE__);
    mOptions = (char **)starmemoryalloc(sizeof(char *) * mOptionsTotal);
    for(int i = 0; i < mOptionsTotal; i ++) mOptions[i] = NULL;
}
void FlexButton::SetOption(int pSlot, const char *pText)
{
    if(pSlot < 0 || pSlot >= mOptionsTotal) return;
    ResetString(mOptions[pSlot], pText);
}
void FlexButton::SetOptionCursor(int pCursor)
{
    if(pCursor < 0) pCursor = 0;
    if(pCursor >= mOptionsTotal) pCursor = mOptionsTotal - 1;
    mCurrentOption = pCursor;
}
void FlexButton::SetSliderMode(int pMin, int pMax)
{
    //--Sets slider mode. If the min and max are the same, fails.
    if(pMin == pMax) return;

    //--Flag.
    mIsSliderMode = true;
    mIsOptionMode = false;
    mIncrementBtnA = 0;
    mIncrementBtnB = 0;

    //--Flip the min and max if necessary.
    if(pMin > pMax)
    {
        mSliderMin = pMax;
        mSliderMax = pMin;
    }
    //--Normal case.
    else
    {
        mSliderMin = pMin;
        mSliderMax = pMax;
    }

    //--Default.
    mCurrentSlider = mSliderMin;
}
void FlexButton::SetSliderCurrent(int pValue)
{
    mCurrentSlider = pValue;
    if(mCurrentSlider < mSliderMin) mCurrentSlider = mSliderMin;
    if(mCurrentSlider > mSliderMax) mCurrentSlider = mSliderMax;
}
void FlexButton::SetLyingLengthString(const char *pText)
{
    ResetString(mLyingLengthString, pText);
}
void FlexButton::SetDescription(const char *pDescription)
{
    ResetString(mDescription, pDescription);
}
void FlexButton::SetImageS(const char *pImagePath)
{
    rImgPointer = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
}
void FlexButton::SetFont(const char *pFontAlias)
{
    Images.Data.rUIFont = DataLibrary::Fetch()->GetFont(pFontAlias);
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(StarBitmap *));
}

///======================================== Core Methods ==========================================
StarFont *FlexButton::ResolveFont()
{
    //--Figures out and returns which font we're going to use. Can legally return NULL if the
    //  default font failed to load during startup.
    StarFont *rUseFont = rOverrideFont;
    return rUseFont;
}
void FlexButton::Execute()
{
    //--FlexButtons can, when in options mode, just cycle through the available options. If not
    //  in options mode, just executes the base behavior.
    if(mIsOptionMode)
    {
        mCurrentOption ++;
        if(mCurrentOption >= mOptionsTotal) mCurrentOption = 0;
    }
    //--In slider mode, this never executes.
    else if(mIsSliderMode)
    {

    }
    //--Normal behavior.
    else
    {
        StarButton::Execute();
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void FlexButton::Update(int pMouseX, int pMouseY)
{
    //--Updating only matters if using the slider. If not, ignore it.
    if(!mIsSliderMode) return;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Reposition by local coordinates.
    pMouseX = pMouseX - (int)mCoordinates.mLft;
    pMouseY = pMouseY - (int)mCoordinates.mTop;

    //--Release case.
    if(rControlManager->IsFirstRelease("MouseLft"))
    {
        mIsDraggingSlider = false;
    }
    //--Calculate the hitbox of the slider, and see if we can drag it.
    else if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Add distance for bar position.
        float tBarPercent = (float)(mCurrentSlider - mSliderMin) / (float)(mSliderMax - mSliderMin);

        //--Calc hitbox.
        float tLft = CalcSliderLeftEdge() + cBarWidth + (tBarPercent * mSliderWidth) - FB_TRIANGLE_PROP;
        float tTop = (mCoordinates.GetHeight() * 0.5f) - (cThickness * 0.5f) + cIndicatorOffset - FB_TRIANGLE_PROP;
        float tRgt = tLft + (FB_TRIANGLE_PROP * 2.0f);
        float tBot = tTop + FB_TRIANGLE_PROP;

        //--Check hitbox.
        if(IsPointWithin(pMouseX, pMouseY, tLft, tTop, tRgt, tBot))
        {
            mIsDraggingSlider = true;
        }
    }
    //--If the slider is currently dragging, handle its movements.
    else if(mIsDraggingSlider)
    {
        //--Set.
        float tLft = CalcSliderLeftEdge() + 3.0f;
        mCurrentSlider = mSliderMin + (((float)(pMouseX - tLft) / (float)mSliderWidth) * (mSliderMax - mSliderMin));

        //--Range check.
        if(mCurrentSlider < mSliderMin) mCurrentSlider = mSliderMin;
        if(mCurrentSlider > mSliderMax) mCurrentSlider = mSliderMax;
    }
}

///========================================== File I/O ============================================
float FlexButton::CalcSliderLeftEdge()
{
    //--Calculates the leftmost edge of the slider bar, when using a slider.
    float cUseFontSize = mFontSize;
    StarFont *rUseFont = NULL;
    if(FlexMenu::xUseClassicUI)
    {
        rUseFont = ResolveFont();
    }
    else
    {
        cUseFontSize = 1.0f;
        rUseFont = Images.Data.rUIFont;
    }

    //--Make sure the font resolved safely.
    if(!rUseFont) return 0.0f; //Error!

    //--Basic length.
    if(!mLyingLengthString)
    {
        return (rUseFont->GetTextWidth(mLocalName) * cUseFontSize) + (25.0f);
    }
    //--If using lying-length, use that here.
    else
    {
        return (rUseFont->GetTextWidth(mLyingLengthString) * cUseFontSize) + (25.0f);
    }

    //--Logically impossible, but here for completeness/debugging.
    return 0.0f;
}

///========================================== Drawing =============================================
void FlexButton::Render()
{
    ///--[Documenation]
    //--Rendering. FlexButtons render a text string that is equal to their name.
    if(!mLocalName || !Images.mIsReady) return;

    ///--[Common Code]
    //--Translate to position.
    glTranslatef(mCoordinates.mLft, mCoordinates.mTop, 0.0f);

    //--Debug: Render a border around the button.
    if(false)
    {
        float tWid = mCoordinates.GetWidth();
        float tHei = mCoordinates.GetHeight();
        glDisable(GL_TEXTURE_2D);
        glLineWidth(1.0f);
        glBegin(GL_LINE_LOOP);
            glColor3f(1.0f, 1.0f, 0.0f);
            glVertex2f(0.0f, 0.0f);
            glVertex2f(tWid, 0.0f);
            glVertex2f(tWid, tHei);
            glVertex2f(0.0f, tHei);
            glColor3f(1.0f, 1.0f, 1.0f);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }

    ///--[Classic Rendering]
    //--Simplistic rendering using a TTF font.
    if(FlexMenu::xUseClassicUI)
    {
        //--Get the font. If no font is provided, use the default font.
        StarFont *rUseFont = ResolveFont();
        if(!rUseFont)
        {
            glTranslatef(-mCoordinates.mLft, -mCoordinates.mTop, 0.0f);
            return;
        }

        //--If not highlighted, render black on white.
        if(!mIsHighlighted)
        {
            //--Text.
            glColor3f(0.0f, 0.0f, 0.0f);
            float tHeight = mCoordinates.GetHeight();
            rUseFont->DrawTextArgs(0.0f, tHeight - (mFontSize * 60.0f / 3.0f), 0, mFontSize, mLocalName);

            //--In options mode, render the option after the text.
            if(mIsOptionMode && mOptions[mCurrentOption])
            {
                float tXPosition = (rUseFont->GetTextWidth(mLocalName) * mFontSize) + (25.0f);
                rUseFont->DrawTextArgs(tXPosition, tHeight - (mFontSize * 60.0f / 3.0f), 0, mFontSize, mOptions[mCurrentOption]);
            }
        }
        //--If highlighted, render based on the static flag.
        else
        {
            //--If highlighting background is set...
            glColor3f(0.0f, 0.0f, 0.0f);
            if(xHighlightIsBackground && !mIsSliderMode)
            {
                //--Backing.
                float tWid = mCoordinates.GetWidth();
                float tHei = mCoordinates.GetHeight();
                glDisable(GL_TEXTURE_2D);
                glBegin(GL_QUADS);
                    glColor3f(0.0f, 0.0f, 0.0f);
                    glVertex2f(0.0f, 0.0f);
                    glVertex2f(tWid, 0.0f);
                    glVertex2f(tWid, tHei);
                    glVertex2f(0.0f, tHei);
                glEnd();
                glEnable(GL_TEXTURE_2D);

                //--Renders white.
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            //--Text.
            float tHeight = mCoordinates.GetHeight();
            rUseFont->DrawTextArgs(0.0f, tHeight - (mFontSize * 60.0f / 3.0f), 0, mFontSize, mLocalName);

            //--In options mode, render the option after the text.
            if(mIsOptionMode && mOptions[mCurrentOption])
            {
                float tXPosition = (rUseFont->GetTextWidth(mLocalName) * mFontSize) + (25.0f);
                rUseFont->DrawTextArgs(tXPosition, tHeight - (mFontSize * 60.0f / 3.0f), 0, mFontSize, mOptions[mCurrentOption]);
            }

            //--Highlight using an underline.
            if(!xHighlightIsBackground && !mIsSliderMode)
            {
                float tLft = 0.0f;
                float tTop = tHeight - (mFontSize * 60.0f / 3.0f) + 2.0f;
                float tRgt = rUseFont->GetTextWidth(mLocalName) * mFontSize;
                float tBot = tTop + 2.0f;
                glDisable(GL_TEXTURE_2D);
                glBegin(GL_QUADS);
                    glColor3f(0.0f, 0.0f, 0.0f);
                    glVertex2f(tLft, tTop);
                    glVertex2f(tRgt, tTop);
                    glVertex2f(tRgt, tBot);
                    glVertex2f(tLft, tBot);
                glEnd();
                glEnable(GL_TEXTURE_2D);
            }
        }
    }
    ///--[Modern Rendering]
    //--Uses a bitmap font, much faster.
    else
    {
        //--Font size.
        float cUseFontSize = 20.0f;

        //--Render the name.
        Images.Data.rUIFont->DrawTextArgs(0.0f, 3.0f, 0.0f, 1.0f, mLocalName);

        //--In options mode, render the option after the text.
        if(mIsOptionMode && mOptions[mCurrentOption])
        {
            //--Calculate the position.
            float tXPosition = (Images.Data.rUIFont->GetTextWidth(mLocalName)) + (25.0f);

            //--Using the lying length, right-align the option.
            if(mLyingLengthString)
            {
                tXPosition = CalcSliderLeftEdge();
            }

            //--Render.
            Images.Data.rUIFont->DrawTextArgs(tXPosition, 3.0f, 0.0f, 1.0f, mOptions[mCurrentOption]);
        }

        //--If highlighted, also render an underline.
        if(mIsHighlighted && !mIsSliderMode && !mDontRenderHighlight)
        {
            float tLft = 0.0f;
            float tTop = cUseFontSize;
            float tRgt = Images.Data.rUIFont->GetTextWidth(mLocalName);
            float tBot = tTop + 2.0f;
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex2f(tLft, tTop);
                glVertex2f(tRgt, tTop);
                glVertex2f(tRgt, tBot);
                glVertex2f(tLft, tBot);
            glEnd();
            glEnable(GL_TEXTURE_2D);
        }
    }

    ///--[Slider Rendering]
    //--Most of this is the same in Classic/Modern, except the value rendered beneath the bar.
    //--In slider mode, render the cursors here.
    if(mIsSliderMode)
    {
        //--Setup.
        float tXPosition = CalcSliderLeftEdge();
        float tYPosition = (mCoordinates.GetHeight() * 0.5f) - (cBarHeight * 0.5f);

        //--Render the slider bar. Color is grey.
        glColor3f(0.5f, 0.5f, 0.5f);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);

            //--Left edge.
            float cWid = cBarWidth;
            float cHei = cBarHeight;
            glVertex2f(tXPosition + 0.0f, tYPosition + 0.0f);
            glVertex2f(tXPosition + cWid, tYPosition + 0.0f);
            glVertex2f(tXPosition + cWid, tYPosition + cHei);
            glVertex2f(tXPosition + 0.0f, tYPosition + cHei);

            //--Reposition.
            tXPosition = tXPosition + cBarWidth;
            tYPosition = tYPosition + (cBarHeight * 0.5f) - (cThickness * 0.5f);

            //--Central bar.
            cWid = mSliderWidth;
            cHei = cThickness;
            glVertex2f(tXPosition + 0.0f, tYPosition + 0.0f);
            glVertex2f(tXPosition + cWid, tYPosition + 0.0f);
            glVertex2f(tXPosition + cWid, tYPosition + cHei);
            glVertex2f(tXPosition + 0.0f, tYPosition + cHei);

            //--Reposition.
            tXPosition = tXPosition + mSliderWidth;
            tYPosition = tYPosition - (cBarHeight * 0.5f) + (cThickness * 0.5f);

            //--Right edge.
            cWid = cBarWidth;
            cHei = cBarHeight;
            glVertex2f(tXPosition + 0.0f, tYPosition + 0.0f);
            glVertex2f(tXPosition + cWid, tYPosition + 0.0f);
            glVertex2f(tXPosition + cWid, tYPosition + cHei);
            glVertex2f(tXPosition + 0.0f, tYPosition + cHei);

        glEnd();

        //--Render the slider itself, over everything else.
        tXPosition = CalcSliderLeftEdge() + cBarWidth;
        tYPosition = (mCoordinates.GetHeight() * 0.5f) - (cThickness * 0.5f) + cIndicatorOffset;

        //--Add distance for bar position.
        float tBarPercent = (float)(mCurrentSlider - mSliderMin) / (float)(mSliderMax - mSliderMin);
        tXPosition = tXPosition + (tBarPercent * mSliderWidth);

        //--Now render the triangle to represent the slider.
        const float cOff = FB_TRIANGLE_PROP;
        if(FlexMenu::xUseClassicUI)
        {
            glColor3f(0.0f, 0.0f, 0.0f);
        }
        else
        {
            glColor3f(1.0f, 1.0f, 1.0f);
        }
        glBegin(GL_TRIANGLES);
            glVertex2f(tXPosition + 0.0f, tYPosition + 0.0f);
            glVertex2f(tXPosition + cOff, tYPosition - cOff);
            glVertex2f(tXPosition - cOff, tYPosition - cOff);
        glEnd();

        //--Clean up.
        glEnable(GL_TEXTURE_2D);

        //--Render text in the middle to show the current value.
        char tBuffer[32];
        sprintf(tBuffer, "%i", mCurrentSlider);
        tXPosition = CalcSliderLeftEdge() + (mSliderWidth * 0.5f) + cBarWidth;
        tYPosition = (mCoordinates.GetHeight() * 0.5f) + (cThickness * 0.5f) + 8.0f;

        //--Text rendering.
        if(FlexMenu::xUseClassicUI)
        {
            //--Make sure the font exists.
            StarFont *rUseFont = ResolveFont();
            if(!rUseFont)
            {
                glTranslatef(-mCoordinates.mLft, -mCoordinates.mTop, 0.0f);
                return;
            }

            //--Center the text.
            tXPosition = tXPosition - (rUseFont->GetTextWidth(tBuffer) * 0.5f * 10.0f / rUseFont->GetTextHeight());

            //--Render it.
            glColor3f(0.0f, 0.0f, 0.0f);
            rUseFont->DrawText(tXPosition, tYPosition, 0, 10.0f / rUseFont->GetTextHeight(), tBuffer);
            glColor3f(1.0f, 1.0f, 1.0f);
        }
        //--Modern text rendering.
        else
        {
            //--Center the text.
            float cTextScale = 0.50f;
            tXPosition = tXPosition - (Images.Data.rUIFont->GetTextWidth(tBuffer) * 0.5f * cTextScale);

            //--Render it.
            glColor3f(1.0f, 1.0f, 1.0f);
            Images.Data.rUIFont->DrawText(tXPosition, tYPosition, 0, cTextScale, tBuffer);
        }
    }

    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
    glTranslatef(-mCoordinates.mLft, -mCoordinates.mTop, 0.0f);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void FlexButton::HookToLuaState(lua_State *pLuaState)
{
    /* FlexButton_SetProperty("Option Count", iOptions)
       FlexButton_SetProperty("Option Text", iSlot, sText)
       FlexButton_SetProperty("Current Option", iSlot)
       FlexButton_SetProperty("Slider Mode", iMinimum, iMaximum)
       FlexButton_SetProperty("Current Slider", iValue)
       FlexButton_SetProperty("Slider IncVals", iValA, iValB)
       FlexButton_SetProperty("Lying Length", sString)
       FlexButton_SetProperty("Description", sString)
       FlexButton_SetProperty("Image", sImagePath)
       FlexButton_SetProperty("Font", sFontalias)
       FlexButton_SetProperty("Dont Render Highlight", bFlag)
       Sets the property in the given FlexButton. For "Option Count", pass 0 to disable options mode. */
    lua_register(pLuaState, "FlexButton_SetProperty", &Hook_FlexButton_SetProperty);

    /* FlexButton_GetProperty("Current Option") (1 integer)
       Returns the given property from the Active FlexButton. */
    lua_register(pLuaState, "FlexButton_GetProperty", &Hook_FlexButton_GetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
#include "DataLibrary.h"
int Hook_FlexButton_SetProperty(lua_State *L)
{
    //FlexButton_SetProperty("Option Count", iOptions)
    //FlexButton_SetProperty("Option Text", iSlot, sText)
    //FlexButton_SetProperty("Current Option", iSlot)
    //FlexButton_SetProperty("Slider Mode", iMinimum, iMaximum)
    //FlexButton_SetProperty("Current Slider", iValue)
    //FlexButton_SetProperty("Slider IncVals", iValA, iValB)
    //FlexButton_SetProperty("Lying Length", sString)
    //FlexButton_SetProperty("Description", sString)
    //FlexButton_SetProperty("Image", sImagePath)
    //FlexButton_SetProperty("Font", sFontalias)
    //FlexButton_SetProperty("Dont Render Highlight", bFlag)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FlexButton_SetProperty");

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid()) return LuaTypeError("FlexButton_SetProperty", L);
    FlexButton *rButton = (FlexButton *)rDataLibrary->rActiveObject;

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    //--Options mode on/off and how many options are available.
    if(!strcasecmp("Option Count", rSwitchType) && tArgs == 2)
    {
        rButton->ActivateOptionMode(lua_tointeger(L, 2));
    }
    //--Text of a requested option.
    else if(!strcasecmp("Option Text", rSwitchType) && tArgs == 3)
    {
        rButton->SetOption(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Which option is selected.
    else if(!strcasecmp("Current Option", rSwitchType) && tArgs == 2)
    {
        rButton->SetOptionCursor(lua_tointeger(L, 2));
    }
    //--Slider mode with attached ranges.
    else if(!strcasecmp("Slider Mode", rSwitchType) && tArgs == 3)
    {
        rButton->SetSliderMode(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Current slider value.
    else if(!strcasecmp("Current Slider", rSwitchType) && tArgs == 2)
    {
        rButton->SetSliderCurrent(lua_tointeger(L, 2));
    }
    //--Incrementors. Pass 0 to disable.
    else if(!strcasecmp("Slider IncVals", rSwitchType) && tArgs == 3)
    {
        //TODO
    }
    //--Length to use instead of the printed text, in string format.
    else if(!strcasecmp("Lying Length", rSwitchType) && tArgs == 2)
    {
        rButton->SetLyingLengthString(lua_tostring(L, 2));
    }
    //--Description to show when in description mode.
    else if(!strcasecmp("Description", rSwitchType) && tArgs == 2)
    {
        rButton->SetDescription(lua_tostring(L, 2));
    }
    //--Image to show when in description mode.
    else if(!strcasecmp("Image", rSwitchType) && tArgs == 2)
    {
        rButton->SetImageS(lua_tostring(L, 2));
    }
    //--Font to use when rendering. Uses a DataLibrary alias.
    else if(!strcasecmp("Font", rSwitchType) && tArgs == 2)
    {
        rButton->SetFont(lua_tostring(L, 2));
    }
    //--If true, doesn't render a line under the option when selected.
    else if(!strcasecmp("Dont Render Highlight", rSwitchType) && tArgs == 2)
    {
        rButton->SetDontRenderHighlight(lua_toboolean(L, 2));
    }
    //--Error case.
    else
    {
        //--Try to run the StarButton() version of the code.
        return Hook_StarButton_SetProperty(L);
    }

    return 0;
}
int Hook_FlexButton_GetProperty(lua_State *L)
{
    //FlexButton_GetProperty("Current Option") (1 integer)
    //FlexButton_GetProperty("Current Slider") (1 integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FlexButton_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    FlexButton *rActiveButton = (FlexButton *)rDataLibrary->rActiveObject;
    if(!rActiveButton) return LuaTypeError("FlexButton_GetProperty", L);

    //--Which option is currently active.
    if(!strcasecmp(rSwitchType, "Current Option") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveButton->GetOptionCursor());
        tReturns = 1;
    }
    //--The value of the slider variable.
    else if(!strcasecmp(rSwitchType, "Current Slider") && tArgs == 1)
    {
        lua_pushinteger(L, (int)rActiveButton->GetSliderCursor());
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("FlexButton_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
