///========================================= StarPath =============================================
//--An object containing three path components. The directories, the filename, and the extension.
//  This object is used to help keep path manipulation easy to understand.
//--The filename must not be NULL to be valid, but directory and extension can both be NULL and
//  the class is still considered valid.
#pragma once

///========================================= Includes =============================================
#include "Definitions.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class StarPath
{
    private:
    ///--[Variables]
    //--System
    char *mDirectory;
    char *mFilename;
    char *mExtension;

    protected:

    public:
    //--System
    StarPath();
    ~StarPath();

    //--Public Variables
    //--Property Queries
    const char *GetDirectory();
    const char *GetFilename();
    const char *GetExtension();

    //--Manipulators
    void ConstructFromPath(const char *pPath);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


