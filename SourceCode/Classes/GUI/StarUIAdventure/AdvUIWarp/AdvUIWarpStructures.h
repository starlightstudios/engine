///==================================== AdvUIWarpStructures =======================================
//--Structures and definitions needed by the warp UI.

#pragma once

///--[Includes]
#include "AdvMenuDefStruct.h"
#include "StarBitmap.h"
#include "StarLinkedList.h"

///======================================== Structures ============================================
///--[Warp Package]
//--Constains a map destination, an image, and a set of coordinates. Used by the Warp submenu.
typedef struct WarpRegionPack
{
    //--Members
    StarLinkedList *mLocations; //WarpLocationPack *, master
    AdvMenuStandardAlignments mAlignments;
    StarBitmap *rMapImage;
    StarBitmap *rMapOverlayImage;
    char *mCallBuffer;

    //--Advanced Maps
    bool mIsUsingAdvancedMaps;
    float mAdvancedClampLft;
    float mAdvancedClampTop;
    float mAdvancedClampRgt;
    float mAdvancedClampBot;
    StarLinkedList *mAdvancedMapPacks; //AdvancedMapPack *, master

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}WarpRegionPack;

///--[Warp Location]
//--Contains a location the player can warp to. Held within a warp region.
typedef struct WarpLocationPack
{
    //--Members
    char *mRoom;
    float mCoordX;
    float mCoordY;

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}WarpLocationPack;

///--[Advanced Map Pack]
#ifndef ADVANCEDMAPPACK
#define ADVANCEDMAPPACK
//--Contains an image layer when using multi-layered images, as well as a chance for that
//  image to appear. This creates the flashing effect.
typedef struct AdvancedMapPack
{
    //--Members
    bool mCanBeToggled;
    StarBitmap *rImage;
    bool mNoPanNoScale;
    int mRenderChanceLo;
    int mRenderChanceHi;

    //--Player Rendering
    bool mRenderPlayer;
    float mPlayerOffX;
    float mPlayerOffY;
    StarBitmap *rPlayerImg;

    //--Stencil Behaviors
    bool mWritesStencil;
    int mWriteStencil;
    bool mReadsStencil;
    int mReadStencil;

    //--Functions
    void Initialize()
    {
        //--System.
        mCanBeToggled = false;
        rImage = NULL;
        mNoPanNoScale = false;
        mRenderChanceLo = 0;
        mRenderChanceHi = 1000;

        //--Player Rendering
        mRenderPlayer = false;
        mPlayerOffX = 0.0f;
        mPlayerOffY = 0.0f;
        rPlayerImg = NULL;

        //--Stencil Behaviors
        mWritesStencil = false;
        mWriteStencil = 0;
        mReadsStencil = false;
        mReadStencil = 0;
    }
}AdvancedMapPack;
#endif
