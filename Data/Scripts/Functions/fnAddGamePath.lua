-- |[ ===================================== fnAddGamePath() ==================================== ]|
--Adds a new path to the list of game paths provided.
function fnAddGamePath(psaPathList, psPathName)
    
    --Deprecated Warning
    if(gbPrintDeprecatedCallWarnings == true) then
        io.write("Warning: Calling fnAddGamePath() is deprecated. Call from: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Argument Check]|
    if(psaPathList == nil) then return end
    if(psPathName  == nil) then return end
    
    -- |[Duplicate Check]|
    for i = 1, #psaPathList, 1 do
        if(psaPathList[i] == psPathName) then return end
    end
    
    -- |[Add]|
    table.insert(psaPathList, psPathName)
end