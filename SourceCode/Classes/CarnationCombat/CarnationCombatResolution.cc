//--Base
#include "CarnationCombat.h"

//--Classes
#include "AdventureLevel.h"
#include "AdvCombatEntity.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///======================================= Manipulators ===========================================
void CarnationCombat::SetVictoryHandler(const char *pPath)
{
    ResetString(mVictoryHandlerPath, pPath);
}
void CarnationCombat::SetDefeatHandler(const char *pPath)
{
    ResetString(mDefeatHandlerPath, pPath);
}
void CarnationCombat::RegisterLevelUpPack(const char *pName)
{
    //--Duplicate/Error check.
    if(!pName || mLevelUpPacks->GetElementByName(pName)) return;
    CarnLevelUp *nPackage = (CarnLevelUp *)starmemoryalloc(sizeof(CarnLevelUp));
    nPackage->Initialize();
    mLevelUpPacks->AddElement(pName, nPackage, &FreeThis);
}
void CarnationCombat::SetLevelUpPackProperty(const char *pName, const char *pProperty, int pValue)
{
    //--Check package.
    CarnLevelUp *rPack = (CarnLevelUp *)mLevelUpPacks->GetElementByName(pName);
    if(!rPack || !pProperty) return;

    //--Switch.
    if(!strcasecmp(pProperty, "Level")) { rPack->mLevel = pValue; return; }
    if(!strcasecmp(pProperty, "HPMax")) { rPack->mHPMax = pValue; return; }
    if(!strcasecmp(pProperty, "SPMax")) { rPack->mSPMax = pValue; return; }
    if(!strcasecmp(pProperty, "Atk"))   { rPack->mAtk   = pValue; return; }
    if(!strcasecmp(pProperty, "MAtk"))  { rPack->mMAtk  = pValue; return; }
    if(!strcasecmp(pProperty, "Def"))   { rPack->mDef   = pValue; return; }
    if(!strcasecmp(pProperty, "MDef"))  { rPack->mMDef  = pValue; return; }
    if(!strcasecmp(pProperty, "Ini"))   { rPack->mIni   = pValue; return; }
    if(!strcasecmp(pProperty, "Lck"))   { rPack->mLck   = pValue; return; }
    if(!strcasecmp(pProperty, "End"))   { rPack->mEnd   = pValue; return; }
    if(!strcasecmp(pProperty, "Int"))   { rPack->mInt   = pValue; return; }
    if(!strcasecmp(pProperty, "Spd"))   { rPack->mSpd   = pValue; return; }

    //--"Old" stats.
    if(!strcasecmp(pProperty, "OldLevel")) { rPack->mOldLevel = pValue; return; }
    if(!strcasecmp(pProperty, "OldHPMax")) { rPack->mOldHPMax = pValue; return; }
    if(!strcasecmp(pProperty, "OldSPMax")) { rPack->mOldSPMax = pValue; return; }
    if(!strcasecmp(pProperty, "OldAtk"))   { rPack->mOldAtk   = pValue; return; }
    if(!strcasecmp(pProperty, "OldMAtk"))  { rPack->mOldMAtk  = pValue; return; }
    if(!strcasecmp(pProperty, "OldDef"))   { rPack->mOldDef   = pValue; return; }
    if(!strcasecmp(pProperty, "OldMDef"))  { rPack->mOldMDef  = pValue; return; }
    if(!strcasecmp(pProperty, "OldIni"))   { rPack->mOldIni   = pValue; return; }
    if(!strcasecmp(pProperty, "OldLck"))   { rPack->mOldLck   = pValue; return; }
    if(!strcasecmp(pProperty, "OldEnd"))   { rPack->mOldEnd   = pValue; return; }
    if(!strcasecmp(pProperty, "OldInt"))   { rPack->mOldInt   = pValue; return; }
    if(!strcasecmp(pProperty, "OldSpd"))   { rPack->mOldSpd   = pValue; return; }
}

///=========================================== Update =============================================
void CarnationCombat::UpdateResolution()
{
    ///--[ ==================== No Resolution ===================== ]
    if(mCombatResolution == ADVCOMBAT_END_NONE)
    {
        return;
    }
    ///--[ ======================== Victory ======================= ]
    else if(mCombatResolution == ADVCOMBAT_END_VICTORY)
    {
        ///--[ =========== 0th Tick =========== ]
        //--0th tick: Run all combat resolution handlers.
        if(mResolutionTimer == 0)
        {
            //--Pointers.
            AudioManager *rAudioManager = AudioManager::Fetch();
            //AdventureLevel *rCheckLevel = AdventureLevel::Fetch();

            //--Stop music and play the victory theme.
            rAudioManager->StopAllMusic();
            rAudioManager->PlayMusicNoFade(mDefaultVictoryMusic);

            ///--[Party Entities]
            //--Reset this flag for all entities.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->mLeveledUpThisBattle = false;
                rEntity->mLevelLastTick = rEntity->GetLevel();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--Reset totals.
            mXPToAwardStorage = 0;
            mJPToAwardStorage = 0;
            mPlatinaToAwardStorage = 0;
            mDoctorToAwardStorage = 0;
            mItemsToAwardList->ClearList();

            ///--[Move Non-Permanent Party to Graveyard]
            //--Run across the player's party. Any entities not in the roster get moved to the graveyard. This is
            //  due to some abilities forcing enemies to become allies.
            AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mrCombatParty->SetToHeadAndReturn();
            while(rCheckEntity)
            {
                //--Not on the roster, liberate.
                if(!mPartyRoster->IsElementOnList(rCheckEntity))
                {
                    //--Move.
                    mrCombatParty->LiberateRandomPointerEntry();
                    mrEnemyGraveyard->AddElementAsTail("X", rCheckEntity);

                    //--Halve the rewards.
                    rCheckEntity->SetRewardXP(rCheckEntity->GetRewardXP() / 2);
                    rCheckEntity->SetRewardJP(rCheckEntity->GetRewardJP() / 2);
                    rCheckEntity->SetRewardPlatina(rCheckEntity->GetRewardPlatina() / 2);
                }

                //--Next.
                rCheckEntity = (AdvCombatEntity *)mrCombatParty->IncrementAndGetRandomPointerEntry();
            }

            ///--[Run External Handler]
            //--Iterate across all entities in the graveyard. Add their totals to the counters. This is also
            //  where the post-handler runs.
            if(mPostScriptHandler)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mPostScriptHandler, 1, "N", (float)ADVCOMBAT_END_VICTORY);
            }

            ///--[Tally KO Bonuses]
            //--Run across all enemies and tally values.
            AdvCombatEntity *rGraveEntity = (AdvCombatEntity *)mrEnemyGraveyard->PushIterator();
            while(rGraveEntity)
            {
                //--Totals.
                mXPToAwardStorage      += rGraveEntity->GetRewardXP();
                mPlatinaToAwardStorage += rGraveEntity->GetRewardPlatina();

                //--Look for this tag. The highest JP tag determines the awarded JP.
                int tJPTagCount = rGraveEntity->GetTagCount("Class XP");
                if(tJPTagCount > mJPToAwardStorage) mJPToAwardStorage = tJPTagCount;

                //--Add any items to the list.
                StarLinkedList *rItemsList = rGraveEntity->GetRewardList();
                char *rItemName = (char *)rItemsList->SetToHeadAndReturn();
                while(rItemName)
                {
                    rItemsList->LiberateRandomPointerEntry();
                    mItemsToAwardList->AddElement("X", rItemName, &FreeThis);
                    rItemName = (char *)rItemsList->SetToHeadAndReturn();
                }

                //--Next.
                rGraveEntity = (AdvCombatEntity *)mrEnemyGraveyard->AutoIterate();
            }

            ///--[Response Scripts]
            //--Run.
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDCOMBAT);

            ///--[Add Totals]
            //--Add bonuses to the storage values.
            mXPToAwardStorage += mXPBonus;
            mPlatinaToAwardStorage += mPlatinaBonus;

            //--Line up the current with totals.
            mXPToAward = mXPToAwardStorage;
            mPlatinaToAward = mPlatinaToAwardStorage;
            mJPToAward = mJPToAwardStorage;

            //--Run the doctor bag heal action.
            //if(mAutoUseDoctorBag) HealFromDoctorBag(-1);

            //--Diagnostics.
            //fprintf(stderr, "XP: %i\n", mXPToAward);
            //fprintf(stderr, "Pl: %i\n", mPlatinaToAward);
        }

        ///--[ ============ Normal ============ ]
        //--Run the timer.
        mResolutionTimer ++;

        //--Text is still running, or waiting on the player to press a key. Player can press activate to break the lockout.
        if(mTextTimer < mTextTimerMax || mTextTimerWaitForKeypress)
        {
            if(ControlManager::Fetch()->IsFirstPress("Activate") || ControlManager::Fetch()->IsFirstPress("MouseLft"))
            {
                mTextTimer = mTextTimerMax - 1;
                mTextTimerWaitForKeypress = false;
                mTextPauseTimer = 0;
            }
        }
        //--Text has hit the end of its timer. Run the victory handler again.
        else
        {
            if(mVictoryHandlerPath) LuaManager::Fetch()->ExecuteLuaFile(mVictoryHandlerPath, 1, "N", (float)mVictoryCalls);
            mVictoryCalls ++;
        }

        //--Still waiting for a keypress.
        if(mTextTimerWaitForKeypress) return;

        ///--[ ====== Finish Victory Mode ===== ]
        if(mTextTimer >= mTextTimerMax)
        {
            //--If there is an override script, handle it here:
            if(mCurrentVictoryScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentVictoryScript);
            }
            //--Run the standard victory script if it exists:
            else if(mStandardVictoryScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardVictoryScript);
            }

            //--By default, return everyone to their starting jobs if they changed jobs. This only applies to
            //  party members. This deliberately fires *after* the victory script, which may want to query
            //  the ending job of party members.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->HandleCombatEnd();
                rEntity->RevertToCombatStartJob();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--All entities handle the post-battle code.
            AdventureLevel *rLevel = AdventureLevel::Fetch();
            if(rLevel) rLevel->RunPostCombatOnEntities();

            //--Purge world references.
            WorldRefPack *rPackage = (WorldRefPack *)mWorldReferences->PushIterator();
            while(rPackage)
            {
                if(rPackage->mTurns < 1 && rPackage->rActor)
                {
                    rPackage->rActor->BeginDying();
                    rLevel->KillEnemy(rPackage->rActor->GetUniqueEnemyName());
                }
                rPackage = (WorldRefPack *)mWorldReferences->AutoIterate();
            }
            mWorldReferences->ClearList();

            //--In all cases, deactivate this object.
            Deactivate();
            ResumeMusic();
            CleanDataLibrary();
            return;
        }
    }
    ///--[ =================== Defeat/Surrender =================== ]
    else if(mCombatResolution == ADVCOMBAT_END_DEFEAT || mCombatResolution == ADVCOMBAT_END_SURRENDER)
    {
        ///--[ =========== 0th Tick =========== ]
        //--Run all combat resolution handlers.
        if(mResolutionTimer == 0)
        {
            AudioManager::Fetch()->StopAllMusic();
            AudioManager::Fetch()->PlayMusic("DesolateShort");
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDCOMBAT);
        }

        ///--[ ============ Normal ============ ]
        //--Run the timer.
        mResolutionTimer ++;

        //--Text is still running, or waiting on the player to press a key.
        if(mTextTimer < mTextTimerMax || mTextTimerWaitForKeypress)
        {

        }
        //--Text has hit the end of its timer. Run the victory handler again.
        else
        {
            if(mDefeatHandlerPath) LuaManager::Fetch()->ExecuteLuaFile(mDefeatHandlerPath, 1, "N", (float)mVictoryCalls);
            mVictoryCalls ++;
        }

        ///--[ ====== Finish Defeat Mode ====== ]
        //--If the text timer hit its cap and the script didn't reset it, finish combat.
        if(mTextTimer >= mTextTimerMax)
        {
            //--If this is a surrender, set this flag.
            if(mCombatResolution == ADVCOMBAT_END_SURRENDER)
            {
                mWasLastDefeatSurrender = true;
            }

            //--If there is an override script, handle it here:
            if(mCurrentDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentDefeatScript);
            }
            //--Run the standard defeat script if it exists:
            else if(mStandardDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardDefeatScript);
            }

            //--All entities handle the post-battle code.
            AdventureLevel *rLevel = AdventureLevel::Fetch();
            if(rLevel) rLevel->RunPostCombatOnEntities();

            //--Return characters to their starting jobs and handle stat changes.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->HandleCombatEnd();
                rEntity->RevertToCombatStartJob();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--In all cases, deactivate this object.
            Deactivate();
            ResumeMusic();
            AudioManager::Fetch()->StopAllMusic();
            CleanDataLibrary();
        }
    }
    ///--[Retreat]
    else if(mCombatResolution == ADVCOMBAT_END_RETREAT)
    {
        //--0th tick: Run all combat resolution handlers.
        if(mResolutionTimer == 0)
        {
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDCOMBAT);
        }

        //--Retreat just counts to the end.
        if(mResolutionTimer < ADVCOMBAT_RETREAT_TICKS)
        {
            mResolutionTimer ++;
        }
        //--Ending.
        else
        {
            //--If there is an override script, handle it here:
            if(mCurrentRetreatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentRetreatScript);
            }
            //--Run the standard retreat script if it exists:
            else if(mStandardRetreatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardRetreatScript);
            }

            //--All entities handle the post-battle code.
            AdventureLevel *rLevel = AdventureLevel::Fetch();
            if(rLevel) rLevel->RunPostCombatOnEntities();

            //--Return characters to their starting jobs and handle stat changes.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->HandleCombatEnd();
                rEntity->RevertToCombatStartJob();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--In all cases, deactivate this object.
            Deactivate();
            ResumeMusic();

            //--Send an instruction to the current level that the retreat case is active. This increases
            //  enemy detection range and, if the player gets in another fight, they can't retreat.
            AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
            if(rActiveLevel)
            {
                rActiveLevel->SetRetreatTimer(AL_RETREAT_STANDARD);
            }

            //--Clean the datalibrary.
            CleanDataLibrary();
        }
    }
}
void CarnationCombat::RenderResolution()
{
    //--Baseline.
    mPartyDecisionMode = false;
    StarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);
    Render(true, 1.0f);

    //--Render the Level-Up box, if someone has that present.
    RenderLevelUpBox();
}
void CarnationCombat::RenderStat(float pX, float pY, int pBaseStat, int pIncreaseAmt, const char *pNoIncrease, const char *pIncrease)
{
    if(pIncreaseAmt < 1)
    {
        CarnationImg.Font.rTextBox->DrawTextArgs(pX, pY, 0, 1.0f, pNoIncrease, pBaseStat);
    }
    else
    {
        CarnationImg.Font.rTextBox->DrawTextArgs(pX, pY, 0, 1.0f, pIncrease, pBaseStat, pBaseStat + pIncreaseAmt);
    }
}
void CarnationCombat::RenderLevelUpBox()
{
    ///--[Documentation]
    //--Renders a box showing the level up stats from the last level up. If there's no string present,
    //  renders nothing.
    CarnLevelUp *rUsePackage = (CarnLevelUp *)mLevelUpPacks->GetElementByName(mShowingLevelUpPack);
    if(!rUsePackage) return;

    //--Fast-Access Pointers.
    StarFont *rFont = CarnationImg.Font.rTextBox;

    //--Constants.
    float cCornerSize = 16.0f;
    float cTxtHei = rFont->GetTextHeight();
    float cWid = VIRTUAL_CANVAS_X * 0.40f;
    float cHei = VIRTUAL_CANVAS_Y * 0.30f;
    float cPad = 16.0f;

    //--Position.
    float cLft = (VIRTUAL_CANVAS_X * 0.50f) - (cWid * 0.50f);
    float cTop = 110.0f;
    float cRgt = cLft + cWid;
    float cBot = cTop + cHei;

    //--Columns.
    float cColL = cLft + cPad;
    float cColR = cLft + 250.0f;

    //--Backing.
    CarnationImg.Image.rTextFrame->RenderNine(cLft, cLft + cCornerSize, cRgt - cCornerSize, cRgt, cTop, cTop + cCornerSize, cBot - cCornerSize, cBot,
                                              0.08f, 0.92f, 0.07f, 0.93f);

    //--Level always increases.
    rFont->DrawTextArgs(cColL, cTop+cPad+(cTxtHei*0), 0, 1.0f, "Level: %i -> %i", rUsePackage->mOldLevel, rUsePackage->mLevel);

    //--Stats.
    RenderStat(cColL, cTop+cPad+(cTxtHei*1), rUsePackage->mOldHPMax, rUsePackage->mHPMax, "HP:  %i",  "HP:  %i -> %i");
    RenderStat(cColR, cTop+cPad+(cTxtHei*1), rUsePackage->mOldSPMax, rUsePackage->mSPMax, "SP:  %i",  "SP:  %i -> %i");
    RenderStat(cColL, cTop+cPad+(cTxtHei*2), rUsePackage->mOldAtk,   rUsePackage->mAtk,   "Atk: %i", "Atk: %i -> %i");
    RenderStat(cColR, cTop+cPad+(cTxtHei*2), rUsePackage->mOldMAtk,  rUsePackage->mMAtk,  "MAt: %i", "MAt: %i -> %i");
    RenderStat(cColL, cTop+cPad+(cTxtHei*3), rUsePackage->mOldDef,   rUsePackage->mDef,   "Def: %i", "Def: %i -> %i");
    RenderStat(cColR, cTop+cPad+(cTxtHei*3), rUsePackage->mOldMDef,  rUsePackage->mMDef,  "MDf: %i", "MDf: %i -> %i");
    RenderStat(cColL, cTop+cPad+(cTxtHei*4), rUsePackage->mOldSpd,   rUsePackage->mSpd,   "Spd: %i", "Spd: %i -> %i");
    RenderStat(cColR, cTop+cPad+(cTxtHei*4), rUsePackage->mOldIni,   rUsePackage->mIni,   "Ini: %i", "Ini: %i -> %i");
    RenderStat(cColL, cTop+cPad+(cTxtHei*5), rUsePackage->mOldEnd,   rUsePackage->mEnd,   "End: %i", "End: %i -> %i");
    RenderStat(cColR, cTop+cPad+(cTxtHei*5), rUsePackage->mOldInt,   rUsePackage->mInt,   "Int: %i", "Int: %i -> %i");
    RenderStat(cColL, cTop+cPad+(cTxtHei*6), rUsePackage->mOldLck,   rUsePackage->mLck,   "Lck: %i", "Lck: %i -> %i");
}
