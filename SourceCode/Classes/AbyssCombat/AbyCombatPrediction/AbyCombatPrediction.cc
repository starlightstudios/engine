//--Base
#include "AbyCombatPrediction.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
AbyCombatPrediction::AbyCombatPrediction()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ============ RootObject ============ ]
    ///--[System]
    mType = POINTER_TYPE_ABYCOMBATPREDICTION;

    ///--[ ======= AdvCombatPrediction ======== ]
    ///--[ ======= AbyCombatPrediction ======== ]
    ///--[System]
    mLocalAlpha = 1.0f;
}
AbyCombatPrediction::~AbyCombatPrediction()
{
}

///===================================== Property Queries =========================================
bool AbyCombatPrediction::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVCBOMATPREDICTION) return true;
    return (pType == mType);
}

///======================================== Manipulators ==========================================
void AbyCombatPrediction::SetLocalAlpha(float pAlpha)
{
    mLocalAlpha = pAlpha;
    if(mLocalAlpha < 0.0f) mLocalAlpha = 0.0f;
    if(mLocalAlpha > 1.0f) mLocalAlpha = 1.0f;
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void AbyCombatPrediction::Render()
{
    ///--[Documentation and Setup]
    //--Renders the object, either as offset from the host entity, or at a fixed position if no host
    //  entity exists.
    if(!rTextFont || !rBorderCard || mStringsTotal < 1) return;

    ///--[Visibility]
    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticOut(mVisTimer, ADVCOMBPREDICTION_VIS_TICKS);
    if(cAlpha <= 0.0f) return;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * mLocalAlpha);

    ///--[Compute X Position]
    //--If there is no entity to attach to, the offsets are the top-left positions.
    float cLft = mXOffset;
    //float cYOff = 0.0f;

    //--If an entity exists to attach to, the offsets are relative to their center.
    if(rHostEntity)
    {
        cLft = rHostEntity->GetCombatX() + mXOffset - (mXSize * 0.50f) + rHostEntity->GetUIRenderPosition(ACE_UI_INDEX_MONO_UI_OFFSET).mXCenter;
        //cYOff = rHostEntity->GetUIRenderPosition(ACE_UI_INDEX_MONO_UI_OFFSET).mYCenter;
    }

    //--Override if using a lock position.
    if(mUseLockPositions) cLft = mLockX;

    ///--[Compute Y Position]
    //--Top position. To prevent from covering up the stun bar, renders top-down. Many-target cases
    //  will instead feature transparency.
    float cTop = 360.0f;

    //--Add offset. This is to prevent prediction boxes from overlapping when there are many targets.
    //cTop = cTop + mYOffset + cYOff;

    //--Override if using a lock position.
    if(mUseLockPositions) cTop = mLockY;

    ///--[Border Card]
    //--Handled by subroutine. -9.0f offset to account for modified font positioning.
    RenderExpandableHighlight(cLft, cTop, cLft + mXSize, cTop + mYSize -9.0f, mBorderCardInd, rBorderCard);

    ///--[Strings]
    //--Render each string.
    float cXPos = cLft + mBorderCardInd + ADVCOMPREDICTION_PADDING_BORDER;
    float tYPos = cTop + mBorderCardInd + ADVCOMPREDICTION_PADDING_BORDER + ADVCOMPREDICTION_PADDING_TEXT_V;
    for(int i = 0; i < mStringsTotal; i ++)
    {
        mStrings[i]->DrawText(cXPos, tYPos-5.0f, SUGARFONT_NOCOLOR, 1.0f, rTextFont);
        tYPos = tYPos + rTextFont->GetTextHeight() + ADVCOMPREDICTION_PADDING_PER_LINE;
    }

    //--Clean.
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
