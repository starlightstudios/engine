//--Base
#include "DataLibrary.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

///--[Debug]
//#define DL_FONT_DEBUG
#ifdef DL_FONT_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
void DataLibrary::HandleFontAddition(const char *pName, const char *pFontPath, const char *pKerningPath, int pSize, int pFlags)
{
    ///--[Documentation]
    //--Handles the work of taking the name, font, kerning, etc and registering everything to the DataLibrary,
    //  and handling all related construction.
    if(!pName || !pFontPath || !pKerningPath) return;

    //--Debug.
    DebugPrint("Adding font %s\n", pName);

    //--Font must be at least size 1.
    if(pSize < 1)
    {
        fprintf(stderr, "%s: Failed, font has illegal size %i. %s.\n", "Font_Register", pSize, LuaManager::Fetch()->GetCallStack(0));
        return;
    }

    //--Font already exists.
    if(FontExists(pName))
    {
        //fprintf(stderr, "%s: Failed, font %s already exists. %s.\n", "Font_Register", lua_tostring(L, 1), LuaManager::Fetch()->GetCallStack(0));
        return;
    }

    ///--[Construction]
    //--Create the font.
    StarFont *nFont = new StarFont();
    DebugPrint(" Constructing %s %i %i.\n", pFontPath, pSize, pFlags);
    nFont->ConstructWith(pFontPath, pSize, pFlags);

    //--The font will specify if it needs the kerning path called. If so, call it.
    if(nFont->NeedsKerning() && strcasecmp(pKerningPath, "Null"))
    {
        DebugPrint(" Running kerning.\n");
        LuaManager::Fetch()->PushExecPop(nFont, pKerningPath);
    }

    //--Write any pending data to the StarLumpManager, if applicable. Note that this must be done after
    //  kerning is built.
    DebugPrint(" Sending autobuffer to SLM.\n");
    nFont->SendAutobufferToSLM();

    ///--[Registration]
    //--Put it on the global list.
    DebugPrint(" Registering.\n");
    RegisterFont(pName, nFont, pKerningPath);

    ///--[Interrupt]
    //--Call load interrupt if required.
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
}
void DataLibrary::HandleDummyFontAddition(const char *pName)
{
    ///--[Documentation]
    //--Creates and registers a dummy font. This font will sit on the name and prevent another font
    //  from being created with the same name. The font will not load anything and will not render,
    //  but will bark a warning if anything attempts to render with it.
    //--This is used to make games able to sit on top of other games, acting like mods, but greatly
    //  reduces the memory overhead since unused assets will not load.
    if(!pName) return;

    ///--[Execution]
    //--Font already exists.
    if(FontExists(pName)) return;

    //--Create, register, set as dummy.
    StarFont *nFont = new StarFont();
    nFont->SetAsDummy();
    nFont->SetInternalName(pName);
    RegisterFont(pName, nFont, "Null");
}

///===================================== Property Queries =========================================
bool DataLibrary::FontExists(const char *pName)
{
    return (mMasterFontList->GetSlotOfElementByName(pName) != -1);
}
bool DataLibrary::FontAliasExists(const char *pName)
{
    return (mrFontRefList->GetSlotOfElementByName(pName) != -1);
}

///======================================= Manipulators ===========================================
void DataLibrary::RegisterFont(const char *pName, StarFont *pFont, const char *pKerningPath)
{
    //--Registers a new font to the master list. The kerning path should be "NULL" if no kerning is desired.
    if(!pName || !pFont || !pKerningPath) return;

    //--If the font already exists, do nothing.
    if(mMasterFontList->GetElementByName(pName))
    {
        return;
    }

    //--Register.
    mMasterFontList->AddElementAsTail(pName, pFont, &RootObject::DeleteThis);
    mFontKerningPathList->AddElementAsTail(pName, InitializeString(pKerningPath), &FreeThis);
}
void DataLibrary::RegisterFontAlias(const char *pFontName, const char *pAliasName)
{
    //--Takes an existing font and gives it an additional name. Internal managers use these names to find fonts rather
    //  than the registered name of the font.
    if(!pFontName || !pAliasName) return;

    //--If the alias already exists, overwrite it.
    if(mrFontRefList->GetElementByName(pAliasName))
    {
        mrFontRefList->RemoveElementS(pAliasName);
    }

    //--Locate the font.
    void *rFont = mMasterFontList->GetElementByName(pFontName);
    if(!rFont) return;

    //--Give it the alias.
    mrFontRefList->AddElement(pAliasName, rFont);
}

///======================================= Core Methods ===========================================
void DataLibrary::RebuildAllKerning()
{
    //--Orders all fonts to rebuild their kerning. Fonts with "Null" as their kerning path do nothing.
    LuaManager *rLuaManager = LuaManager::Fetch();

    //--Iterate.
    void *rFont = mMasterFontList->PushIterator();
    char *rPath = (char *)mFontKerningPathList->PushIterator();
    while(rFont)
    {
        //--If the path is "Null", skip it.
        if(strcasecmp(rPath, "Null"))
        {
            rLuaManager->PushExecPop(rFont, rPath);
        }

        //--Next.
        rPath = (char *)mFontKerningPathList->AutoIterate();
        rFont = mMasterFontList->AutoIterate();
    }
}
void DataLibrary::RebuildFontKerning(const char *pName)
{
    //--Rebuilds the kerning for the named font.
    LuaManager *rLuaManager = LuaManager::Fetch();

    //--Locate both the font and its kerning entry.
    void *rFont = mMasterFontList->GetElementByName(pName);
    char *rPath = (char *)mFontKerningPathList->GetElementByName(pName);

    //--Execute.
    rLuaManager->PushExecPop(rFont, rPath);
}

///======================================= Pointer Routing ========================================
StarFont *DataLibrary::GetFont(const char *pName)
{
    //--Returns the named font. This must be an alias, not the font's core name. Returns NULL on error.
    if(!pName) return NULL;
    StarFont *rFont = (StarFont *)mrFontRefList->GetElementByName(pName);
    if(!rFont && !mSuppressFontErrors) fprintf(stderr, "Warning: No font with alias %s was found!\n", pName);
    return rFont;
}

///================================================================================================
///                                       Hooking Functions                                      ==
///================================================================================================
int Hook_Font_Register(lua_State *L)
{
    //Font_Register(sName, sFontPath, sKerningPath, iSize, iFlags)
    int tArgs = lua_gettop(L);
    if(tArgs < 5) return LuaArgError("Font_Register");

    //--Call subroutine.
    DataLibrary::Fetch()->HandleFontAddition(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    return 0;
}
int Hook_Font_RegisterAsDummy(lua_State *L)
{
    //Font_RegisterAsDummy(sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Font_RegisterAsDummy");

    //--Call subroutine.
    DataLibrary::Fetch()->HandleDummyFontAddition(lua_tostring(L, 1));
    return 0;
}
int Hook_Font_GetProperty(lua_State *L)
{
    ///--[List]
    //Font_GetProperty("Font Exists", sFontName) (1 Boolean)
    //Font_GetProperty("Alias Exists", sAliasName) (1 Boolean)
    //Font_GetProperty("Constant Precache With Nearest") (1 Integer)
    //Font_GetProperty("Constant Precache With Edge") (1 Integer)
    //Font_GetProperty("Constant Precache With Downfade") (1 Integer)
    //Font_GetProperty("Constant Precache With Special S") (1 Integer)
    //Font_GetProperty("Constant Precache With Special !") (1 Integer)
    //Font_GetProperty("Constant Precache Debug") (1 Integer)
    //Font_GetProperty("Constant Precache Dummy") (1 Integer)
    //Font_GetProperty("Constant Precache Full Unicode") (1 Integer)

    //--[Execution]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Font_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Returns whether or not the named font already exists.
    if(!strcasecmp(rSwitchType, "Font Exists") && tArgs == 2)
    {
        lua_pushboolean(L, DataLibrary::Fetch()->FontExists(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns whether or not the named alias already exists.
    else if(!strcasecmp(rSwitchType, "Alias Exists") && tArgs == 2)
    {
        lua_pushboolean(L, DataLibrary::Fetch()->FontAliasExists(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns a construction constant. This one uses GL_NEAREST instead of GL_LINEAR.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Nearest") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_NEAREST);
        tReturns = 1;
    }
    //--Returns a construction constant. This causes a black border to be created.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Edge") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_EDGE);
        tReturns = 1;
    }
    //--Returns a construction constant. This causes a the letters to grey from top to bottom.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Downfade") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_DOWNFADE);
        tReturns = 1;
    }
    //--Returns a construction constant. This causes a different algorithm to work on the 'S', needed for some fonts.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Special S") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_SPECIAL_S);
        tReturns = 1;
    }
    //--Returns a construction constant. This modifies the '!' character used for MrPixel.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Special !") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_SPECIAL_EXC);
        tReturns = 1;
    }
    //--Returns a construction constant. This makes a font use the whole unicode suite instead of the
    //  ASCII suite, allowing for non-English dialogue.
    else if(!strcasecmp(rSwitchType, "Constant Precache Full Unicode") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_UNICODE);
        tReturns = 1;
    }
    //--Returns a construction constant. Makes a font that is a valid pointer but doesn't render. Used
    //  to make unused UIs not have problems with pointer referencing, and not eat unnecessary memory.
    else if(!strcasecmp(rSwitchType, "Constant Precache Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_DUMMY);
        tReturns = 1;
    }
    //--Returns the debug constant for precaching.
    else if(!strcasecmp(rSwitchType, "Constant Precache Debug") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_DEBUG_FLAG);
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("Font_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_Font_SetProperty(lua_State *L)
{
    //--[List]
    //Font_SetProperty("Downfade", fStart, fFinish)
    //Font_SetProperty("Outline Width", iWidth)
    //Font_SetProperty("Add Alias", sFontName, sAliasName)
    //Font_SetProperty("Monospacing", fMonospaceValue) (-1 to disable)

    //--[Execution]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Font_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Sets downfade for the next constructed font.
    if(!strcasecmp(rSwitchType, "Downfade") && tArgs == 3)
    {
        StarFont::SetDownfade(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Sets the outline width for the next constructed font.
    else if(!strcasecmp(rSwitchType, "Outline Width") && tArgs == 2)
    {
        StarFont::SetOutlineWidth(lua_tointeger(L, 2));
    }
    //--Registers a new alias for the named font.
    else if(!strcasecmp(rSwitchType, "Add Alias") && tArgs == 3)
    {
        DataLibrary::Fetch()->RegisterFontAlias(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets monospacing for the next created font.
    else if(!strcasecmp(rSwitchType, "Monospacing") && tArgs >= 2)
    {
        StarFont::SetMonospacing(lua_tonumber(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("Font_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
