-- |[ ================================ SysDiagnostics Arguments ================================ ]|
--Handlers for argument-related diagnostic messages.

-- |[ ============================ SysDiagnostics:fnPrintArgError() ============================ ]|
--Given a function name, prints an error warning indicating the function received a nil argument.
-- If the function name is somehow also nil, get your head out of your ass. Also print a warning.
function SysDiagnostics:fnPrintArgError(psFunctionName)
    
    -- |[Argument Check]|
    --You think this is funny?
    if(psFunctionName == nil) then
        io.write("SysDiagnostics:fnPrintArgError() - Attempted to print a nil argument error but the function name was nil.\n")
        LM_PrintCallStack()
        return
    end
    
    -- |[Normal Execution]|
    io.write(psFunctionName .. " - One or more arguments was nil, failing.\n")
    LM_PrintCallStack()
end