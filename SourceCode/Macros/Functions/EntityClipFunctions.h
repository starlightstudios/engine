///==================================== EntityClipFunctions =======================================
//--Functions designed to perform movement operations on the standard dimension structures, such as
//  TwoDimensionReal and ThreeDimensionReal.

#pragma once

#include "Definitions.h"
#include "Structures.h"

///--[Local Definitions]
#define CRAWLZ_BOT 0.0f
#define CRAWLZ_LFT 1.0f
#define CRAWLZ_RGT 2.0f
#define CRAWLZ_TOP 3.0f

///--[Local Structures]
///--[Functions]
void ResetSlopeStatics();
void ResolveRemainderOnce(float &sSpeed, float &sRemainder, int &sMove);
void ResolveRemainders(float pXSpeed, float pYSpeed, float pZSpeed, int &sXMove, int &sYMove, int &sZMove, ThreeDimensionReal &sDimensions);
void MoveEntity(float pXSpeed, float pYSpeed, float pZSpeed, ThreeDimensionReal &sDimensions);
void MoveEntityWallCrawl(int pSpeed, ThreeDimensionReal &sDimensions);

