//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

void AdventureInventory::Debug_AwardPlatina()
{
    mPlatina += 1000;
}
void AdventureInventory::Debug_AwardCrafting()
{
    LuaManager *rLuaManager = LuaManager::Fetch();
    rLuaManager->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", "Adamantite Powder x10");
    rLuaManager->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", "Adamantite Flakes x10");
    rLuaManager->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", "Adamantite Shard x10");
    rLuaManager->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", "Adamantite Piece x10");
    rLuaManager->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", "Adamantite Chunk x10");
    rLuaManager->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", "Adamantite Ore x10");
}
