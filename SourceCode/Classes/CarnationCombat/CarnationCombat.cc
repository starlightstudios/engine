//--Base
#include "CarnationCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatPrediction.h"
#include "CarnationCombatEntity.h"
#include "CarnUICommon.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "OptionsManager.h"

///========================================== System ==============================================
CarnationCombat::CarnationCombat()
{
    mHasInitializedCarnCombat = false;
    Initialize();
}
CarnationCombat::~CarnationCombat()
{
    Disassemble();
}

///===================================== Property Queries =========================================
bool CarnationCombat::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVCOMBAT) return true;
    if(pType == POINTER_TYPE_ABYSSCOMBAT) return true;
    if(pType == POINTER_TYPE_CARNATIONCOMBAT) return true;
    return false;
}
int CarnationCombat::GetAbilityExecutionPhase()
{
    return mAbilityExecPhase;
}
int CarnationCombat::GetStoredInventorySlot()
{
    return mStoredCommandInventorySlot;
}
bool CarnationCombat::IsActionEffortless()
{
    return mIsHandlingEffortlessAction;
}

///======================================= Manipulators ===========================================
void CarnationCombat::FlagAbilityReplied()
{
    mAnyAbilityReplied = true;
}
void CarnationCombat::TripStatusResponse()
{
    mStatusInterceptFlag = true;
}
void CarnationCombat::EnqueueApplicationPack(ApplicationPack *pPack)
{
    if(!pPack) return;
    mApplicationQueue->AddElement("X", pPack, &ApplicationPack::DeleteThis);

    //--Special: Clamp applications so they can never have more than 15 ticks of duration.
    if(pPack->mTicks > 0) pPack->mTicks = 0;
}
void CarnationCombat::SeedRNG(uint32_t pSeed)
{
    mRandomNumberSeed = pSeed;
}
float CarnationCombat::RunRNG()
{
    //--Returns between 0.0f and 1.0f. Advances the seed by one.
    uint32_t a = 16807;
    uint32_t m = 2147483647;
    mRandomNumberSeed = (a * mRandomNumberSeed) % m;
    return (mRandomNumberSeed / (float)m);
}
void CarnationCombat::ClearBackgrounds()
{
    mBackgroundList->ClearList();
}
void CarnationCombat::RegisterBackground(const char *pName, const char *pImgPath, int pLogicType, float pLogicFactor)
{
    //--Error check. Duplicate names are not allowed.
    if(!pName || !pImgPath) return;
    if(mBackgroundList->GetElementByName(pName)) return;

    //--Allocate and initialize.
    CarnCombatBGLayer *nLayer = (CarnCombatBGLayer *)starmemoryalloc(sizeof(CarnCombatBGLayer));
    nLayer->Initialize();
    mBackgroundList->AddElementAsTail(pName, nLayer, &FreeThis);

    //--Set.
    nLayer->mLogicType = pLogicType;
    nLayer->mLogicFactor = pLogicFactor;
    nLayer->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);
}
void CarnationCombat::SetPrintBlock()
{
    mStatusPrintBlock = true;
}

///======================================= Core Methods ===========================================
int CarnationCombat::ResolvePauseTime()
{
    ///--[Documentation]
    //--When a line finishes writing, or on certain tags, the combat dialogue will pause. This determines
    //  how long it should pause for, based on the game options.
    OptionPack *rPauseTimePack = (OptionPack *)OptionsManager::Fetch()->GetOption("Combat Pause Time");
    if(rPauseTimePack)
    {
        //--Value.
        int tTextSpeedValue = (*(int *)(rPauseTimePack->rPtr));

        //--The "Wait" time forces a keypress.
        if(tTextSpeedValue == CC_TEXT_PAUSE_WAIT)
        {
            mTextTimerWaitForKeypress = true;
            return 1;
        }
        //--All other timer values do not require a keypress.
        else if(tTextSpeedValue == CC_TEXT_PAUSE_LONG)
        {
            return 45;
        }
        else if(tTextSpeedValue == CC_TEXT_PAUSE_NORMAL)
        {
            return 15;
        }
        else if(tTextSpeedValue == CC_TEXT_PAUSE_SHORT)
        {
            return 10;
        }
        else if(tTextSpeedValue == CC_TEXT_PAUSE_NEARZERO)
        {
            return 1;
        }
    }

    //--Option failed to resolve, set the default.
    return 15;
}
AdvCombatEntity *CarnationCombat::ResolveDecidingEntity()
{
    if(!mPartyDecisionMode) return NULL;
    return (AdvCombatEntity *)mrCombatParty->GetElementBySlot(mPartyDecisionIndex);
}
void CarnationCombat::BeginTurn()
{
    ///--[Documentation]
    //--Begins the turn, rolling turn order. In addition, Carnation combat has the player make all their
    //  action decisions at the beginning of the turn.
    AdvCombat::BeginTurn();

    //--Flags.
    mPartyDecisionMode = true;
    mPartyDecisionIndex = 0;
    mPartyDecisionTimer = 0;
    mPartyActions->ClearList();
    ClearText();

    ///--[Hitboxes]
    //--Reset hitboxes to the default set of six.
    SetHitboxesForPrimaryAction();

    ///--[Resolve Acting Entity]
    //--If the zeroth entity does not exist somehow, cycle forward.
    AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(mPartyDecisionIndex);
    if(!rCheckEntity || !rCheckEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
    {
        if(!CycleCommandForwards())
        {
            BeginResolutionSequence(ADVCOMBAT_END_DEFEAT);
            return;
        }
    }

    //--If that entity cannot act, cycle forward.
    CarnationCombatEntity *rCarnEntity = (CarnationCombatEntity *)rCheckEntity;
    if(!rCarnEntity->IsPlayerControlled())
    {
        if(!CycleCommandForwards())
        {
            BeginResolutionSequence(ADVCOMBAT_END_DEFEAT);
            return;
        }
    }

    //--At least one player character can act.
}
void CarnationCombat::ClearText()
{
    mHighestLineRendered = 0;
    mTextTimer = 0;
    mTextTimerMax = 0;
    mTextStrings->ClearList();
}
void CarnationCombat::AppendText(const char *pString, uint32_t pOriginatorID, uint32_t pTargetID)
{
    ///--[Documentation]
    //--Subdivide the string based on its render length. Each letter becomes a CarnationLetter class so it can hold actions on it.
    //  In addition, handle any tags. Anything in a tag is either an application string, or a variable that needs to be resolved.
    //--The originator ID should always be nonzero, but the target can be zero.
    //--First, remove all tags from the string to get just the raw text.
    if(!pString) return;
    int tOrigLen = (int)strlen(pString);

    ///--[Tag Removal]
    //--Create a string that has the text itself with no tags in it.
    char *tTaglessText = (char *)starmemoryalloc(sizeof(char) * (tOrigLen + 1));
    memset(tTaglessText, 0, sizeof(char) * (tOrigLen + 1));

    //--Copy across, skipping tags.
    int c = 0;
    bool tIsTagMode = false;
    for(int i = 0; i < tOrigLen; i ++)
    {
        //--No-Tag mode.
        if(!tIsTagMode)
        {
            //--If this is a '[', start tag mode.
            if(pString[i] == '[')
            {
                tIsTagMode = true;
            }
            //--Otherwise, copy the letter.
            else
            {
                tTaglessText[c] = pString[i];
                c ++;
            }
        }
        //--Tag mode.
        else
        {
            //--If this is a ']', the tag has ended. Go back to normal mode.
            if(pString[i] == ']')
            {
                tIsTagMode = false;
            }
            //--Otherwise, don't copy the character.
            else
            {

            }
        }
    }

    ///--[Text Line Splitting]
    //--Now that the tags have been removed, break the string up and create a series of CarnationStrings that
    //  can store the needed letters.
    //--First, create a reference list of all the strings we create. We need this for later, to place the
    //  application packs inside the tags on them.
    StarLinkedList *tStringRefList = new StarLinkedList(false);

    //--Parse.
    int tCharsParsedTotal = 0;
    int tCharsParsed = 0;
    char *nString = Subdivide::SubdivideString(tCharsParsed, &tTaglessText[tCharsParsedTotal], -1, CC_TEXTBOX_WID_EFF, CarnationImg.Font.rTextBox, 1.0f);
    tCharsParsedTotal += tCharsParsed;
    mTextTimerMax += (tCharsParsed * mLettersPerTick);

    //--Create a new CarnationString to hold the letters as a group.
    CarnationString *nCarnString = (CarnationString *)starmemoryalloc(sizeof(CarnationString));
    nCarnString->Initialize();
    nCarnString->SetString(nString);
    mTextStrings->AddElementAsTail("X", nCarnString, &CarnationString::DeleteThis);
    tStringRefList->AddElementAsTail("X", nCarnString);

    //--Deallocate the original string.
    free(nString);

    //--Debug.
    //fprintf(stderr, "Added %s\n", nString);

    //--Keep iterating until we parse the whole string.
    while(tCharsParsed > 0)
    {
        //--Check if there are more letters to parse.
        char *nString = Subdivide::SubdivideString(tCharsParsed, &tTaglessText[tCharsParsedTotal], -1, CC_TEXTBOX_WID_EFF, CarnationImg.Font.rTextBox, 1.0f);
        if(tCharsParsed < 1)
        {
            free(nString);
            break;
        }

        //--Create a new CarnationString to hold the letters as a group.
        CarnationString *nCarnString = (CarnationString *)starmemoryalloc(sizeof(CarnationString));
        nCarnString->Initialize();
        nCarnString->SetString(nString);
        mTextStrings->AddElementAsTail("X", nCarnString, &CarnationString::DeleteThis);
        tStringRefList->AddElementAsTail("X", nCarnString);

        //--Deallocate the original string.
        free(nString);

        //--Increment counters.
        tCharsParsedTotal += tCharsParsed;
        mTextTimerMax += (tCharsParsed * mLettersPerTick);

        //--Debug.
        //fprintf(stderr, "Added %s\n", nString);
    }

    //--If the text timer is too low due to a string being short, or the letters-per-tick value being too low,
    //  clamp it at a minimum of 5.
    if(mTextTimerMax < 5.0f) mTextTimerMax = 5.0f;

    //--Clean.
    free(tTaglessText);

    ///--[Tag Placement]
    //--Run across the string again, except this time, place the tags inside the letters within the strings.
    c = 0;
    tIsTagMode = false;
    int cTagStarted = 0;
    for(int i = 0; i < tOrigLen; i ++)
    {
        //--No-Tag mode.
        if(!tIsTagMode)
        {
            //--If this is a '[', start tag mode.
            if(pString[i] == '[')
            {
                tIsTagMode = true;
                cTagStarted = i;
            }
            //--Otherwise, this is a legit letter.
            else
            {
                c ++;
            }
        }
        //--Tag mode.
        else
        {
            //--If this is a ']', the tag has ended. Go back to normal mode.
            if(pString[i] == ']')
            {
                //--Flag.
                tIsTagMode = false;

                //--Create a substring from within the primary string.
                int tSubLen = (i - cTagStarted + 1);

                char *tNewString = (char *)starmemoryalloc(sizeof(char) * (tSubLen + 1));
                for(int p = cTagStarted; p < i+1; p ++)
                {
                    tNewString[p-cTagStarted] = pString[p];
                }
                tNewString[tSubLen] = '\0';

                //--Get the CarnationLetter that was last ticked. Register the tag to that latter.
                //  If the attempt fails, try again at the -1 position. This allows a tag to be placed after the
                //  last letter of a sentence.
                CarnationLetter *rLastLetter = RetrieveLetterInSlot(c, tStringRefList);
                if(!rLastLetter) rLastLetter = RetrieveLetterInSlot(c-1, tStringRefList);
                if(rLastLetter)
                {
                    rLastLetter->RegisterApplication(pOriginatorID, pTargetID, tNewString);
                }

                //--Clean.
                free(tNewString);
            }
            //--Otherwise, do nothing.
            else
            {

            }
        }
    }

    ///--[Clean Up]
    delete tStringRefList;
}
CarnationLetter *CarnationCombat::RetrieveLetterInSlot(int pSlot, StarLinkedList *pStringList)
{
    ///--[Documentation]
    //--Given a list of CarnationString *'s and a slot, finds the CarnationLetter in the given slot and
    //  returns it. Returns NULL on error or out of range.
    if(!pStringList) return NULL;
    if(pSlot < 0) pSlot = 0;

    //--Iterate across the strings.
    CarnationString *rString = (CarnationString *)pStringList->PushIterator();
    while(rString)
    {
        //--If the slot is within the length of this string:
        if(pSlot < rString->mLength)
        {
            pStringList->PopIterator();
            return rString->mLetters[pSlot];
        }

        //--Otherwise, decrement the slot and continue.
        pSlot -= rString->mLength;

        //--Next.
        rString = (CarnationString *)pStringList->AutoIterate();
    }

    //--Slot was past the last letter of the strings.
    return NULL;
}
void CarnationCombat::RunLetterPackages()
{
    ///--[Documentation]
    //--Run across all the letters that are presently visible. They execute any attached
    //  tags/applications on the first tick they become visible.
    //--Letters are not stored in a single series, but are broken up by string. We need a
    //  special iterator for them.
    CarnationString *rString = (CarnationString *)mTextStrings->GetElementBySlot(0);
    if(!rString) return;

    //--Compute how many letters would render if this were the rendering cycle.
    int tLettersToRender = (int)(mTextTimer / mLettersPerTick);

    //--Iterate across the letters.
    int tCurLetter = 0;
    int tCurLine = 0;
    for(int i = 0; i < tLettersToRender; i ++)
    {
        //--Get the ith letter.
        CarnationLetter *rCurLetter  = rString->mLetters[tCurLetter+0];
        uint32_t         tNextLetter = rString->mLetters[tCurLetter+1]->GetLetter();
        tCurLetter ++;

        //--If this letter hasn't run its tags, do that now.
        if(!rCurLetter->HasFiredApplications())
        {
            rCurLetter->SetFiredApplications();
            StarLinkedList *rAppPackList = rCurLetter->GetApplicationList();
            CLAppPack *rPackage = (CLAppPack *)rAppPackList->PushIterator();
            while(rPackage)
            {
                HandleCLAppPack(rPackage);
                rPackage = (CLAppPack *)rAppPackList->AutoIterate();
            }
        }

        //--If the next letter is a null, advance the string.
        if(tNextLetter == '\0')
        {
            //--Retrieve.
            tCurLetter = 0;
            tCurLine ++;
            rString = (CarnationString *)mTextStrings->GetElementBySlot(tCurLine);

            //--If the string doesn't exist, stop.
            if(!rString) break;
        }
    }
}
void CarnationCombat::RemoveActionsConcerning(void *pEntity)
{
    ///--[Documentation]
    //--Scans through the action list and removes any that were being performed by the provided entity.
    //  This is done to make sure that cancelling actions in the menu doesn't create duplicate actions.
    if(!pEntity) return;

    //--Scan.
    CarnationAction *rCheckAction = (CarnationAction *)mPartyActions->SetToHeadAndReturn();
    while(rCheckAction)
    {
        //--Match, remove.
        if(rCheckAction->rOwner == pEntity) mPartyActions->RemoveRandomPointerEntry();

        //--Next.
        rCheckAction = (CarnationAction *)mPartyActions->IncrementAndGetRandomPointerEntry();
    }
}
bool CarnationCombat::CycleCommandBackwards()
{
    ///--[Documentation]
    //--Decrements the command cursor down until it either finds the next character who can act,
    //  or hits zero. Clears action information as it goes.
    //--Returns true if it found a character who can act, false if not.
    int tCheckIndex = mPartyDecisionIndex;
    while(true)
    {
        //--Decrement slot.
        tCheckIndex --;

        //--Slot hit -1, stop.
        if(tCheckIndex < 0) return false;

        //--Entity doesn't exist, somehow. skip it.
        AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(tCheckIndex);
        if(!rCheckEntity || !rCheckEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) continue;

        //--Cast to carnation type.
        CarnationCombatEntity *rCarnEntity = (CarnationCombatEntity *)rCheckEntity;

        //--Entity can be controlled. Stop.
        if(rCarnEntity->IsPlayerControlled())
        {
            mSelecterBuiltCommands = false;
            mPartyDecisionIndex = tCheckIndex;
            return true;
        }

        //--Loop again.
    }

    //--Default case.
    return false;
}
bool CarnationCombat::CycleCommandForwards()
{
    ///--[Documentation]
    //--Increments the command cursor forwards. If it hits the party size cap, returns false to indicate
    //  there are no more entities for the player to order. True otherwise.
    int tCheckIndex = mPartyDecisionIndex;
    while(true)
    {
        //--Increment slot.
        tCheckIndex ++;

        //--Entity doesn't exist, meaning we hit the size cap. Return false.
        AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(tCheckIndex);
        if(!rCheckEntity || !rCheckEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) return false;

        //--Cast to carnation type.
        CarnationCombatEntity *rCarnEntity = (CarnationCombatEntity *)rCheckEntity;

        //--Entity can be controlled. Stop.
        if(rCarnEntity->IsPlayerControlled())
        {
            mSelecterBuiltCommands = false;
            mPartyDecisionIndex = tCheckIndex;
            return true;
        }

        //--Loop again.
    }

    //--Default case.
    return false;
}
void CarnationCombat::PlayMonsterKnockout()
{
    //--Does nothing. Monster knockout SFX is handled elsewhere.
}
bool CarnationCombat::IsPlayerDefeated()
{
    ///--[Documentation]
    //--If all the player's party members are KO'd or otherwise unable to act, the player loses.
    //  Note that the IsDefeated() call is not just a KO - permanent stun or stasis also counts.
    //--Carnation Combat also requires that the knockout block be lifted before a character is counted
    //  as defeated. This only affects the player's party.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        //--Must be a CarnationCombatEntity.
        if(!rEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
        {
            rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
            continue;
        }

        //--Cast.
        CarnationCombatEntity *rCarnEntity = (CarnationCombatEntity *)rEntity;

        //--Check.
        if(!rCarnEntity->IsDefeated() || rCarnEntity->HasKnockoutBlock())
        {
            mrCombatParty->PopIterator();
            return false;
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--If we got this far, none of the entities could act. Player loses.
    return true;
}
void CarnationCombat::ResolveBattlePosition(AdvCombatEntity *pEntity, int pTypeFlag, float &sXPos, float &sYPos)
{
    ///--[Documentation and Setup]
    //--Given an entity, returns its nominal position in the shared variables. This varies slightly
    //  by text versus animations.
    if(!pEntity) return;

    //--Constants.
    float cMainTextY = 274.0f;
    float cSecondaryTextY = 194.0f;
    float cAnimationY = 100.0f;

    ///--[Resolve]
    //--Primary Text:
    if(pTypeFlag == ADVCOMBAT_POSTYPE_PRIMARY_TEXT)
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cMainTextY;
    }
    //--Secondary Text:
    else if(pTypeFlag == ADVCOMBAT_POSTYPE_SECONDARY_TEXT)
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cSecondaryTextY;
    }
    //--Animation:
    else
    {
        sXPos = pEntity->GetCombatX();
        sYPos = cAnimationY;
    }

    ///--[Player Party]
    //--The player's party is positioned specially. If the entity is in the player's party, override
    //  the positions.
    uint32_t tTargetID = pEntity->GetID();
    int tTargetPartyID = GetPartyGroupingID(tTargetID);

    //--Not in the player's party, ignore.
    if(tTargetPartyID != AC_PARTY_GROUP_PARTY) return;

    //--Constants.
    float cSpacingX = 222.0f;
    float tRenderX  = (VIRTUAL_CANVAS_X * 0.50f) - cSpacingX - (99);
    float cRenderY  = 500.0f;

    //--Depending on the party size, adjust the starting position to center things.
    if(mrCombatParty->GetListSize() == 1)
    {
        tRenderX = tRenderX + cSpacingX;
    }
    else if(mrCombatParty->GetListSize() == 2)
    {
        tRenderX = tRenderX + (cSpacingX / 2);
    }

    //--Get the slot of the party member.
    if(pEntity == mrCombatParty->GetElementBySlot(0))
    {
        sXPos = tRenderX;
        sYPos = cRenderY;
    }
    else if(pEntity == mrCombatParty->GetElementBySlot(1))
    {
        sXPos = tRenderX + cSpacingX;
        sYPos = cRenderY;
    }
    else if(pEntity == mrCombatParty->GetElementBySlot(2))
    {
        sXPos = tRenderX + (tRenderX * 2);
        sYPos = cRenderY;
    }
}
void CarnationCombat::SetPredictionBoxHostI(const char *pBoxName, uint32_t pHostID)
{
    ///--[Documentation]
    //--Identical to the base copy, but if the prediction box is focused on a player party member,
    //  then it also sets lock positions so the box shows in the right spot.
    int tTargetPartyID = GetPartyGroupingID(pHostID);
    AdvCombat::SetPredictionBoxHostI(pBoxName, pHostID);

    //--Not in the player's party, ignore.
    if(tTargetPartyID != AC_PARTY_GROUP_PARTY) return;

    //--Get prediction box.
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;

    //--Get associated entity.
    AdvCombatEntity *rEntity = GetEntityByID(pHostID);

    ///--[Constants]
    //--Constants.
    float cSpacingX = 222.0f;
    float tRenderX  = (VIRTUAL_CANVAS_X * 0.50f) - cSpacingX - (99);
    float cRenderY  = 400.0f;
    float cCentering = 197.0f * 0.50f;

    //--Depending on the party size, adjust the starting position to center things.
    if(mrCombatParty->GetListSize() == 1)
    {
        tRenderX = tRenderX + cSpacingX;
    }
    else if(mrCombatParty->GetListSize() == 2)
    {
        tRenderX = tRenderX + (cSpacingX / 2);
    }

    ///--[Set Position]
    //--Get the slot of the party member.
    if(rEntity == mrCombatParty->GetElementBySlot(0))
    {
        rPredictionBox->SetLockPositionFlag(true);
        rPredictionBox->SetLockPositions(tRenderX + 0 + cCentering, cRenderY);
    }
    else if(rEntity == mrCombatParty->GetElementBySlot(1))
    {
        rPredictionBox->SetLockPositionFlag(true);
        rPredictionBox->SetLockPositions(tRenderX + cSpacingX + cCentering, cRenderY);
    }
    else if(rEntity == mrCombatParty->GetElementBySlot(2))
    {
        rPredictionBox->SetLockPositionFlag(true);
        rPredictionBox->SetLockPositions(tRenderX + (cSpacingX * 2) + cCentering, cRenderY);
    }
}
void CarnationCombat::WriteLoadInfo(StarAutoBuffer *pBuffer)
{
    ///--[Documentation and Setup]
    //--Writes the LOADINFO_ block to a given buffer to be used in savefiles. The LOADINFO_ block
    //  contains levels, sprites, and other data for members of the active party. It is not used by
    //  the game itself, but by the main menu when showing the player the loading menu.
    //--Carnation uses different portrait load formats, writing the DLPath of the image to render
    //  instead of a pattern.
    if(!pBuffer) return;

    //--Write the DLPaths.
    for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
    {
        //--Get entry.
        AdvCombatEntity *rCharacter = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(i);

        //--If the entity exists, append their save portrait path, and their level. Must be a CarnationCombatEntity.
        if(rCharacter && rCharacter->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
        {
            //--Case.
            CarnationCombatEntity *rCarnComEntity = (CarnationCombatEntity *)rCharacter;

            //--Print.
            pBuffer->AppendStringWithLen(rCarnComEntity->GetSavePortraitPath());
            pBuffer->AppendInt32(rCharacter->GetLevel());
        }
        //--If not, write "NULL" and 0.
        else
        {
            pBuffer->AppendStringWithLen("NULL");
            pBuffer->AppendInt32(0);
        }
    }
}
AdvCombatEntity *CarnationCombat::PushAbilityOwnerWhenNull()
{
    ///--[Documentation]
    //--Abilities typically have an owner, the character who can use the ability. Some abilities do
    //  not have an owner, in which case, when they are asked to push their owner, this function is
    //  called and will tell them what to return.
    //--In this class, push whatever character is selecting their action.
    if(mPartyDecisionMode)
    {
        AdvCombatEntity *rPartyEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(mPartyDecisionIndex);
        return rPartyEntity;
    }

    //--Otherwise, push the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    return rActingEntity;
}
void CarnationCombat::SetAbilityAsActiveI(int pSlot)
{
    ///--[Documentation]
    //--Given a slot, takes the given ability in that slot from the acting entity and makes it active.
    rActiveAbility = NULL;

    //--Make sure there's an active entity.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActiveEntity) return;

    //--Get the ability list.
    StarLinkedList *rAbilityList = rActiveEntity->GetAbilityList();
    if(!rAbilityList) return;

    //--Get the ability in the slot. If it's null, sets the active ability to NULL!
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(pSlot);
    rActiveAbility = rAbility;
}
void CarnationCombat::SetAbilityAsActiveS(const char *pName)
{
    ///--[Documentation]
    //--As above, but uses the ability name. This can also pull from the global list if the
    //  character does not have the ability registered.
    rActiveAbility = NULL;

    //--Make sure there's an active entity.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActiveEntity) return;

    //--Get the ability list.
    StarLinkedList *rAbilityList = rActiveEntity->GetAbilityList();
    if(!rAbilityList) return;

    //--Get the ability in the slot. If it's null, sets the active ability to NULL!
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementByName(pName);
    rActiveAbility = rAbility;

    //--If the ability came back NULL, try to get it from the global list.
    if(!rAbility)
    {
        AdvCombatAbility *rCheckAbility = (AdvCombatAbility *)mGlobalAbilities->GetElementByName(pName);
        rActiveAbility = rCheckAbility;
    }
}
void CarnationCombat::ReresolveDescriptionAbility()
{
    ///--[Documentation]
    //--Whenever the cursor or command stack changes, re-resolve which ability is highlighted and store
    //  it for the description window. If no ability is available, set it to NULL.
    //--In addition to the base version, also sets the local versions of the variables.

    ///--[Clear]
    //--Deallocate.
    free(mDescriptionAbilityName);
    delete mDescription;

    //--Clear.
    mDescriptionAbilityName = NULL;
    mDescription = NULL;
    rDescriptionAbility = NULL;

    ///--[Basic Execution]
    //--Get the active window.
    int tUseCursor = mPlayerCommandCursor;
    StarLinkedList *rCommandStack = ResolvePlayerCommandList();
    if(rCommandStack == mCommandList) tUseCursor = mPlayerCommandBaseCursor;

    //--Get the highlighted ability.
    CommandListEntry *rCommandEntry = (CommandListEntry *)rCommandStack->GetElementBySlot(tUseCursor);
    if(rCommandEntry)
    {
        rDescriptionAbility = rCommandEntry->rAssociatedAbility;
    }

    ///--[Description Copying]
    //--If there is a description ability:
    if(rDescriptionAbility && rCommandStack != mCommandList)
    {
        //--Basic name.
        ResetString(mDescriptionAbilityName, rDescriptionAbility->GetDisplayName());

        //--Extract description, everything should be in a single line.
        int tLinesTotal = rDescriptionAbility->GetDescriptionLinesTotal();
        if(tLinesTotal < 1) return;

        //--Get container.
        StarlightString *rString = rDescriptionAbility->GetDescriptionLine(0);
        if(!rString) return;

        //--First breakpoint should have the whole string.
        StarlightStringBreakpoint *rBreakpoint = rString->GetBreakpoint(0);
        if(!rBreakpoint) return;

        //--Store.
        mDescription = CarnUICommon::CreateDescriptionListFromString(rBreakpoint->mString, CarnationImg.Font.rFontDescriptionMainline, 1000.0f);
        CarnUICommon::ReplaceCommonTagsInList(mDescription);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
