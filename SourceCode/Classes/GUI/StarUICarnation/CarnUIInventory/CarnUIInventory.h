///====================================== CarnUIInventory ==========================================
//--Inventory UI object. Similar to the AdvUI version, but has some mouse functionality and allows items
//  to be used directly from the inventory.

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"
#include "AdvUIInventory.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define CARNUI_INV_NONE 0
#define CARNUI_INV_ALCHEMIZE 1
#define CARNUI_INV_DISCARD 2

///========================================== Classes =============================================
class CarnUIInventory : virtual public CarnUICommon, virtual public AdvUIInventory
{
    protected:
    ///--[Constants]
    //--Scrollbars
    static const int cxCarnScrollBuf = 3;
    static const int cxCarnScrollPage = 14;

    //--Hitboxes
    static const int cxItemBoxMin =  0;
    static const int cxItemBoxMax = 13;
    static const int cxScrollMin  = 14;
    static const int cxScrollMax  = 16;
    static const int cxConfirm    = 17;
    static const int cxCancel     = 18;

    //--Timers
    static const int cxPopupTimerMax = 15;
    static const int cxConfirmTimerMax = 15;

    ///--[System]
    ///--[Popup]
    bool mIsPopupActive;
    int mPopupItemIndex;
    StarUIPopup *mPopup;

    ///--[Confirmation]
    int mConfirmCode;
    int mConfirmItemSlot;
    int mConfirmTimer;

    ///--[Description]
    StarLinkedList *mDescription; //char *, master

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Confirm;
        StarFont *rFont_Header;
        StarFont *rFont_Name;
        StarFont *rFont_Popup;
        StarFont *rFont_Statistic;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Button;
        StarBitmap *rFrame_Confirm;
        StarBitmap *rFrame_Popup;
        StarBitmap *rOverlay_ItemSlot;
        StarBitmap *rOverlay_PortraitBox;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIInventory();
    virtual ~CarnUIInventory();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    int GetAlchemizeValue(AdventureItem *pItem);

    //--Manipulators
    virtual void TakeForeground();
    void RecomputeMaxSkip();

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();
    bool HandleMouseOver();
    bool HandleMouseLeft();
    bool HandleMouseRight();

    //--Popup Mode
    void BuildPopupMenu(int pItemIndex);
    void ExecutePopupAction(const char *pAction);
    void HandleConfirmation();
    void AlchemizeItem(int pItemIndex);
    void DiscardItem(int pItemIndex);
    void DisassembleItem(int pItemIndex);

    //--File I/O
    //--Drawing
    virtual void RenderForeground(float pVisAlpha);
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnUIInventory_SetProperty(lua_State *L);

