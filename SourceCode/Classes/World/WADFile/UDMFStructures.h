///====================================== UDMF Structures =========================================
//--Structures related to the UDMF format for WADFiles.
#include "WADFileMacros.h"

#pragma once

///--[UDMF Vertex]
//--Has positions as floating point, no defaults.
typedef struct UDMFVertex
{
    //--Members
    float mX, mY;

    //--Optional Members
    bool mHasSpecialFloor;
    bool mHasSpecialCeiling;
    float mFloorZ;
    float mCeilingZ;

    //--Functions
    void Initialize()
    {
        //--Members
        mX = 0.0f;
        mY = 0.0f;

        //--Optional Members
        mHasSpecialFloor = false;
        mHasSpecialCeiling = false;
    }
    bool CompareTo(UDMFVertex pOtherVertex)
    {
        return (mX == pOtherVertex.mX && mY == pOtherVertex.mY);
    }
}UDMFVertex;

///--[UDMF Linedef]
//--Declared in its own file.
#include "UDMFLinedef.h"

///--[UDMF Sidedef]
//--Used by Linedefs to get texturing info.
typedef struct UDMFSidedef
{
    //--Members
    int32_t mOffsetX, mOffsetY;
    char mTexTop[9];
    char mTexMid[9];
    char mTexBot[9];
    int32_t mSector;

    //--Specific Face Offsets
    int32_t mOffsetLX, mOffsetLY;
    int32_t mOffsetMX, mOffsetMY;
    int32_t mOffsetUX, mOffsetUY;

    //--Functions
    void Initialize()
    {
        mOffsetX = 0;
        mOffsetY = 0;
        mTexTop[8] = '\0';
        mTexMid[8] = '\0';
        mTexBot[8] = '\0';
        CopyEight(mTexTop, "-\0\0\0\0\0\0\0");
        CopyEight(mTexMid, "-\0\0\0\0\0\0\0");
        CopyEight(mTexBot, "-\0\0\0\0\0\0\0");
        mSector = -1;
        mOffsetLX = 0;
        mOffsetLY = 0;
        mOffsetMX = 0;
        mOffsetMY = 0;
        mOffsetUX = 0;
        mOffsetUY = 0;
    }
}UDMFSidedef;

///--[UDMF Sector]
//--Stores floor and ceiling information. Sidedefs point to these.
typedef struct UDMFSector
{
    //--Members
    int32_t mID;
    int32_t mSpecial;
    int32_t mHeightFloor;
    int32_t mHeightCeiling;
    char mTexFloor[9];
    char mTexCeiling[9];
    int32_t mLightLevel;

    //--Offsets
    int32_t mXOffsetF;
    int32_t mYOffsetF;
    int32_t mXOffsetC;
    int32_t mYOffsetC;

    //--Functions
    void Initialize()
    {
        mID = 0;
        mSpecial = 0;
        mHeightFloor = 0;
        mHeightCeiling = 0;
        CopyEight(mTexFloor, "-\0\0\0\0\0\0\0");
        CopyEight(mTexCeiling, "-\0\0\0\0\0\0\0");
        mTexFloor[8] = '\0';
        mTexCeiling[8] = '\0';
        mLightLevel = 160;
        mXOffsetF = 0;
        mYOffsetF = 0;
        mXOffsetC = 0;
        mYOffsetC = 0;
    }
    bool IsCeilingSky()
    {
        return CompareEight(mTexCeiling, "F_SKY1\0\0");
    }
    bool IsFloorSky()
    {
        return CompareEight(mTexFloor, "F_SKY1\0\0");
    }
}UDMFSector;
