//--Base
#include "CarnationTitle.h"

//--Classes
#include "CarnUIOptions.h"
#include "CarnStringEntry.h"
#include "StringEntry.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
CarnationTitle::CarnationTitle()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    mType = POINTER_TYPE_CARNATIONTITLE;

    ///--[ ======== StarMenu ======== ]
    ///--[ ======== FlexMenu ======== ]
    ///--[ ===== CarnationTitle ===== ]
    ///--[System]
    mHasClickedToBegin = false;
    mMode = CT_MODE_TITLE;
    mHighlightedHitbox = -1;

    ///--[Timers]
    mClickPromptTimer = 0;
    mClickExitTimer = 0;
    mTitleTimer = 0;
    mLoadFileTimer = 0;

    ///--[Transition Work]
    mTransitTimer = -1;
    mTransitToNewGame = false;

    ///--[Options Menu]
    mCarnOptionsMenu = new CarnUIOptions();
    mCarnOptionsMenu->SetTitleMode();

    ///--[Images]
    mImagesReady = false;
    memset(&CarnImages, 0, sizeof(CarnImages));
}
CarnationTitle::~CarnationTitle()
{
    delete mCarnOptionsMenu;
}
void CarnationTitle::Construct()
{
    ///--[Documentation]
    //--Crossload pointers and set hitboxes. First, call the base object to set that up.
    MonocerosTitle::Construct();

    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    ///--[Load]
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts.
    CarnImages.rFont_Header   = rDataLibrary->GetFont("Carn Title UI Header");
    CarnImages.rFont_Mainline = rDataLibrary->GetFont("Carn Title UI Mainline");
    CarnImages.rFont_SaveMain = rDataLibrary->GetFont("Carn Title UI Save Mainline");
    CarnImages.rFont_Small    = rDataLibrary->GetFont("Carn Title UI Small");

    //--Images.
    CarnImages.rBackground_Main    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Background_Main");
    CarnImages.rFrame_Button       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Frame_Button");
    CarnImages.rFrame_SaveInfo     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Frame_SaveInfo");
    CarnImages.rOverlay_BlackLft   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Overlay_BlackLft");
    CarnImages.rOverlay_BlackRgt   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Overlay_BlackRgt");
    CarnImages.rOverlay_Title      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Overlay_Title");
    CarnImages.rScrollbar_Scroller = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Scrollbar_Scroller");
    CarnImages.rScrollbar_Static   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnTitle/Pieces/Scrollbar_Static");

    //--Run a verification routine. If the diagnostic fails, do it again and print errors.
    mImagesReady = VerifyStructure(&CarnImages, sizeof(CarnImages), sizeof(void *), false);
    if(!mImagesReady) VerifyStructure(&CarnImages, sizeof(CarnImages), sizeof(void *), true);

    ///--[Hitboxes]
    //--Left side options.
    RegisterHitboxWH("New Game", 25.0f, 290.0f, 200.0f, 30.0f);
    RegisterHitboxWH("Continue", 25.0f, 332.0f, 200.0f, 30.0f);
    RegisterHitboxWH("Options",  25.0f, 374.0f, 200.0f, 30.0f);
    RegisterHitboxWH("Quit",     25.0f, 416.0f, 200.0f, 30.0f);

    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 1213.0f, 130.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollMd", 1213.0f, 188.0f, 36.0f, 404.0f);
    RegisterHitboxWH("ScrollDn", 1213.0f, 592.0f, 36.0f,  58.0f);

    //--Selecting a savefile.
    RegisterHitboxWH("SelectFile0",  320.0f, 126.0f, 871.0f, 167.0f);
    RegisterHitboxWH("SelectFile1",  320.0f, 306.0f, 871.0f, 167.0f);
    RegisterHitboxWH("SelectFile2",  320.0f, 486.0f, 871.0f, 167.0f);
    RegisterHitboxWH("BackToTitle", 1129.0f, 693.0f, 228.0f,  61.0f);

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mLoadingOffset;
    nScrollbarPack->mPerPage = cxScroll_SavesPerPage;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxHitbox_ScrollbarUp;
    nScrollbarPack->mIndexBd = cxHitbox_ScrollbarMd;
    nScrollbarPack->mIndexDn = cxHitbox_ScrollbarDn;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;

    ///--[Options UI]
    //--Order the Options UI to construct as well.
    mCarnOptionsMenu->Construct();
}

///===================================== Property Queries =========================================
bool CarnationTitle::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_MENU_ROOT)      return true;
    if(pType == POINTER_TYPE_MENU_FLEX)      return true;
    if(pType == POINTER_TYPE_MONOCEROSTITLE) return true;
    if(pType == POINTER_TYPE_CARNATIONTITLE) return true;
    return (pType == mType);
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void CarnationTitle::RunTimer(int &sTimer, int pLo, int pHi, bool pShouldIncrement)
{
    if(pShouldIncrement)
    {
        if(sTimer < pHi) sTimer ++;
    }
    else
    {
        if(sTimer > pLo) sTimer --;
    }
}
void CarnationTitle::ActivateNoteEntry(int pLoadingCursor, LoadingPack *pLoadingPack)
{
    ///--[Documentation]
    //--Opens the note entry interface using the given loading pack. Stores the loading cursor
    //  for later when the note is completed.
    if(!pLoadingPack) return;

    ///--[Execution]
    //--Set flags.
    mIsEnteringString = true;
    mFileStringEntering = pLoadingCursor + mLoadingOffset;
    mStringEntryForm->SetCompleteFlag(false);

    //--Get the string from the file in question.
    mStringEntryForm->SetString(pLoadingPack->mFileName);

    //--Set the loading cursor for later.
    mLoadingCursor = pLoadingCursor;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void CarnationTitle::Update()
{
    ///--[Documentation]
    //--Primary update entry point. Routes to other modes if needed.
    if(mSplashState > FM_SPLASH_STATE_NONE) { UpdateSplashMode();    return; }

    ///--[Transition Case]
    //--When the transition timer is above zero, it is counting down to change modes, such as launching
    //  a new game. This is also used for fade control.
    if(mTransitTimer > -1)
    {
        //--Decrement. If it reaches zero, perform the action.
        mTransitTimer --;
        if(mTransitTimer < 0)
        {
            //--New Game.
            if(mTransitToNewGame)
            {
                LuaManager::Fetch()->ExecuteLuaFile("Data/Scripts/MainMenu/130 Launch Carnation.lua");
                return;
            }
        }

        //--Otherwise, stop. The rest of the update is blocked.
        return;
    }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Click Prompt]
    //--When the splash screen is done, the title appears. The player is prompted to click to open
    //  the menu if they have not done anything else yet.
    if(!mHasClickedToBegin)
    {
        //--Run timer. Timer continues to run after reaching its cap to oscillate.
        mClickPromptTimer ++;

        //--If the player clicks, exit this mode.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Flag.
            mHasClickedToBegin = true;

            //--If the click prompt timer was not over the display cap, set the prompt fadeout timer
            //  to past its max, causing it to disappear.
            if(mClickPromptTimer < cxTimer_ClickPromptMax) mClickExitTimer = cxTimer_ClickExitMax;
        }

        //--Stop the update.
        return;
    }

    ///--[Timers]
    RunTimer(mTitleTimer,    0, cxTimer_TitleMax,    (mMode == CT_MODE_TITLE));
    RunTimer(mLoadFileTimer, 0, cxTimer_LoadFileMax, (mMode == CT_MODE_LOADFILE));

    //--Once the click prompt occurs, run the click to exit timer.
    if(mClickExitTimer < cxTimer_ClickExitMax) mClickExitTimer ++;

    ///--[Mouse Setup]
    //--Fast-access pointers.
    GetMouseInfo();

    ///--[Subtypes]
    if(mMode == CT_MODE_TITLE)
    {
        UpdateMainTitle();
        mCarnOptionsMenu->UpdateBackground();
    }
    else if(mMode == CT_MODE_OPTIONS)
    {
        UpdateOptions();
    }
    else if(mMode == CT_MODE_LOADFILE)
    {
        UpdateLoadFile();
        mCarnOptionsMenu->UpdateBackground();
    }
}
void CarnationTitle::UpdateMainTitle()
{
    ///--[Documentation]
    //--Primary title. Interested in the New Game, Continue, and Quit buttons.
    ControlManager *rControlManager = ControlManager::Fetch();
    RunHitboxCheck();

    ///--[Highlight Check]
    //--Play a sound when the highlight changes.
    if(mMouseMoved)
    {
        //--Get via reply.
        int tOldHighlight = mHighlightedHitbox;
        mHighlightedHitbox = CheckRepliesWithin(cxHitbox_NewGame, cxHitbox_Quit);

        //--If the highlight changes, and the new highlight is not -1, play a sound.
        if(tOldHighlight != mHighlightedHitbox && mHighlightedHitbox != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Click]
    //--Handle left clicking to activate the highlighted button.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--New game.
        if(mHighlightedHitbox == cxHitbox_NewGame)
        {
            mTransitTimer = cxTimer_TransitTicks;
            mTransitToNewGame = true;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            //LuaManager::Fetch()->ExecuteLuaFile("Data/Scripts/MainMenu/130 Launch Carnation.lua");
        }
        //--Continue. Brings up file select.
        else if(mHighlightedHitbox == cxHitbox_Continue)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Flags.
            mHighlightedHitbox = -1;
            mMode = CT_MODE_LOADFILE;
            ActivateLoadingMode();

            //--Recompute scrollbar size.
            ScrollbarPack *rScrollbar = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
            if(rScrollbar)
            {
                rScrollbar->mMaxSkip = mLoadingPackList->GetListSize() - cxScroll_SavesPerPage;
                if(rScrollbar->mMaxSkip < 0) rScrollbar->mMaxSkip = 0;
            }
        }
        //--Options. Shows the options menu.
        else if(mHighlightedHitbox == cxHitbox_Options)
        {
            mMode = CT_MODE_OPTIONS;
            mCarnOptionsMenu->TakeForeground();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Quit. Quits the game.
        else if(mHighlightedHitbox == cxHitbox_Quit)
        {
            AudioManager::Fetch()->PlaySound("Menu|Select");
            Global::Shared()->gQuit = true;
        }
    }
}
void CarnationTitle::UpdateOptions()
{
    mCarnOptionsMenu->UpdateForeground(false);
    if(mCarnOptionsMenu->IsFlaggingExit())
    {
        mMode = CT_MODE_TITLE;
        mCarnOptionsMenu->UnflagExit();
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnationTitle::Render()
{
    ///--[Documentation and Setup]
    //--Make sure the images resolved correctly.
    if(!mImagesReady) return;

    ///--[Version Info]
    //--Render the version number in the top left.
    GLOBAL *rGlobal = Global::Shared();
    if(rGlobal->gBitmapFont)
    {
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, 1.0f, rGlobal->gVersionString);
    }

    ///--[Mode Switching]
    //--If in Loading/Rebinding mode, do that instead.
    if(mSplashState > FM_SPLASH_STATE_NONE) { RenderSplashMode();    return; }

    ///--[Background]
    //--Background.
    CarnImages.rBackground_Main->Draw();

    //--Title. Fades in.
    if(mHasClickedToBegin)
    {
        CarnImages.rOverlay_Title->Draw();
    }
    else
    {
        //--Determine alpha.
        float tAlpha = ((mClickPromptTimer - cxTimer_ClickPromptMax) + 15) / 15.0f;
        if(tAlpha < 0.0f) tAlpha = 0.0f;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);

        //--Render.
        CarnImages.rOverlay_Title->Draw();

        //--Clean.
        StarlightColor::ClearMixer();
    }

    ///--[Click Prompt]
    //--If the player didn't click before the timer expires, prompt them to click.
    if(!mHasClickedToBegin)
    {
        //--Timer has expired, show text.
        if(mClickPromptTimer >= cxTimer_ClickPromptMax)
        {
            //--Resolve position.
            float cUseX = VIRTUAL_CANVAS_X * 0.50f;
            float cUseY = 515.0f + (sinf(mClickPromptTimer * TORADIAN) * 10.0f);

            //--Resolve alpha.
            if(mClickPromptTimer - cxTimer_ClickPromptMax < cxTimer_ClickPromptFadeIn)
            {
                float tAlpha = EasingFunction::QuadraticInOut(mClickPromptTimer - cxTimer_ClickPromptMax, cxTimer_ClickPromptFadeIn);
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);
            }

            //--Render.
            CarnImages.rFont_Header->DrawTextArgs(cUseX, cUseY, SUGARFONT_AUTOCENTER_XY, 1.0f, "Click anywhere to begin");

            //--Clean.
            StarlightColor::ClearMixer();
        }
        return;
    }
    //--Render the click anywhere string fading out.
    else if(mClickExitTimer < cxTimer_ClickExitMax)
    {
        //--Resolve position.
        float cUseX = VIRTUAL_CANVAS_X * 0.50f;
        float cUseY = 515.0f + (sinf(mClickPromptTimer * TORADIAN) * 10.0f);

        //--Resolve alpha.
        float tAlpha = 1.0f - EasingFunction::QuadraticInOut(mClickExitTimer, cxTimer_ClickExitMax);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);

        //--Render.
        CarnImages.rFont_Header->DrawTextArgs(cUseX, cUseY, SUGARFONT_AUTOCENTER_XY, 1.0f, "Click anywhere to begin");

        //--Clean.
        StarlightColor::ClearMixer();
    }

    ///--[Submodes]
    //--All submodes render, but internal timers prevent further rendering if not needed.
    RenderMainTitle();
    RenderLoadFile();

    //--Options menu.
    mCarnOptionsMenu->RenderForeground(1.0f);

    ///--[Fade Out]
    //--If transiting, fade to black.
    if(mTransitTimer > -1)
    {
        //--Percentage.
        float cFadePct = EasingFunction::QuadraticInOut(mTransitTimer, cxTimer_TransitTicks);

        //--Render.
        StarBitmap::DrawFullBlack(1.0f - cFadePct);
    }
}
void CarnationTitle::RenderMainTitle()
{
    ///--[Documentation]
    //--Renders a black underlay and some options.
    if(mTitleTimer < 1) return;

    ///--[Offset]
    float cLftOffset = (1.0f - EasingFunction::QuadraticInOut(mTitleTimer, cxTimer_TitleMax)) * -200.0f;
    glTranslatef(cLftOffset, 0.0f, 0.0f);

    ///--[Render]
    //--Black underlay.
    CarnImages.rOverlay_BlackLft->Draw();

    //--Options.
    RenderButton("New Game", "New Game", SUGARFONT_AUTOCENTER_Y);
    RenderButton("Continue", "Continue", SUGARFONT_AUTOCENTER_Y);
    RenderButton("Options",  "Options",  SUGARFONT_AUTOCENTER_Y);
    RenderButton("Quit",     "Quit",     SUGARFONT_AUTOCENTER_Y);

    ///--[Clean]
    glTranslatef(cLftOffset * -1.0f, 0.0f, 0.0f);
}
void CarnationTitle::RenderButton(const char *pHitboxName, const char *pText, uint32_t pFlags)
{
    ///--[Documentation]
    //--Locates the matching hitbox and renders the text inside it.
    if(!pHitboxName || !pText) return;

    ///--[Execution]
    //--Hitbox must exist.
    TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName(pHitboxName);
    if(!rHitbox) return;

    //--Resolve X position.
    float tUseX = rHitbox->mLft;
    if(pFlags & SUGARFONT_AUTOCENTER_X) tUseX = rHitbox->mXCenter;
    if(pFlags & SUGARFONT_RIGHTALIGN_X) tUseX = rHitbox->mRgt;

    //--Resolve Y position.
    float tUseY = rHitbox->mTop;
    if(pFlags & SUGARFONT_AUTOCENTER_Y) tUseY = rHitbox->mYCenter;

    //--Render text.
    CarnImages.rFont_Header->DrawTextArgs(tUseX, tUseY, pFlags, 1.0f, pText);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
FlexMenu *CarnationTitle::GenerateCarnationTitle()
{
    CarnationTitle *nTitle = new CarnationTitle();
    nTitle->Construct();
    return nTitle;
}

///========================================= Lua Hooking ==========================================
void CarnationTitle::HookToLuaState(lua_State *pLuaState)
{
    /* CT_SetProperty("Set As Title Generate") (Static)
       CT_SetProperty("Set As String Entry Generate") (Static)
       CT_SetProperty("Set Version", sVersionString) (Static)
       CT_SetProperty("Show Options")
       Sets the requested property. */
    lua_register(pLuaState, "CT_SetProperty", &Hook_CT_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CT_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CT_SetProperty("Set As Title Generate") (Static)
    //CT_SetProperty("Set As String Entry Generate") (Static)
    //CT_SetProperty("Set Version", sVersionString) (Static)
    //CT_SetProperty("Show Options")

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CT_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Changes static class generation to generate a CarnationTitle instead of the base FlexMenu.
    if(!strcasecmp(rSwitchType, "Set As Title Generate"))
    {
        Global::Shared()->rCreateTitleMenu = &CarnationTitle::GenerateCarnationTitle;
        return 0;
    }
    //--Changes static class generation to generate a CarnStringEntry instead of a String Entry.
    else if(!strcasecmp(rSwitchType, "Set As String Entry Generate"))
    {
        StringEntry::rGenerateStringEntry = &CarnStringEntry::GenerateStringEntry;
        return 0;
    }
    //--Changes static class generation to generate a MonoStringEntry instead of a String Entry.
    else if(!strcasecmp(rSwitchType, "Set Version") && tArgs == 2)
    {
        //ResetString(CarnationTitle::xVersionString, lua_tostring(L, 2));
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_CARNATIONTITLE)) return LuaTypeError("CT_SetProperty", L);
    //CarnationTitle *rTitleObject = (CarnationTitle *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Shows the options menu.
    if(!strcasecmp(rSwitchType, "Show Options") && tArgs == 1)
    {
        //rTitleObject->ShowOptions();
    }
    else if(!strcasecmp(rSwitchType, "Show Mod Load Order") && tArgs == 1)
    {
        //rTitleObject->ShowModLoadOrder();
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CT_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}

