///======================================== TiledLevel ============================================
//--Represents a level constructed in and exported from Tiled. Contains the routines necessary to
//  render and process it, as well as loading one off the hard drive.
#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"

///===================================== Local Structures =========================================
///--[PropertiesPack]
//--Properties, contains key/value pairs.
typedef struct PropertiesPack
{
    int16_t mPropertiesTotal;
    char **mKeys;
    char **mVals;
    void Initialize()
    {
        mPropertiesTotal = 0;
        mKeys = NULL;
        mVals = NULL;
    }
    void Deallocate()
    {
        for(int i = 0; i < mPropertiesTotal; i ++)
        {
            free(mKeys[i]);
            free(mVals[i]);
        }
        free(mKeys);
        free(mVals);
    }
}PropertiesPack;

///--[TilesetInfoPack]
//--Contains a tileset and tile indexers.
typedef struct TilesetInfoPack
{
    //--Members
    char *mName;
    bool mOwnsTileset;
    StarBitmap *mTileset;
    int mTilesToThisPoint;
    int mTilesTotal;

    //--Functions
    void Initialize()
    {
        mName = NULL;
        mOwnsTileset = false;
        mTileset = NULL;
        mTilesToThisPoint = 0;
        mTilesTotal = 0;
    }
    //Deletion function is TiledRawData::DeleteTilesetPack(void *pPtr)
}TilesetInfoPack;

///--[ObjectInfoPack]
//--Stores an object from Tiled. These are found in the Entity layers.
typedef struct ObjectInfoPack
{
    //--Members
    char *mName;
    char *mType;
    float mX;
    float mY;
    float mW;
    float mH;
    int mTileID;

    //--Properties
    PropertiesPack *mProperties;

    //--Static Methods
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        ObjectInfoPack *rPack = (ObjectInfoPack *)pPtr;
        free(rPack->mName);
        free(rPack->mType);
        rPack->mProperties->Deallocate();
        free(rPack->mProperties);
        free(rPack);
    }

}ObjectInfoPack;

///--[ImageInfoPack]
//--Stores an Image Layer from tiled.
typedef struct
{
    char *mName;
    float mX;
    float mY;

    int16_t mPropertiesTotal;
    char ***mProperties;

    StarBitmap *mImage;
}ImageInfoPack;

///--[CollisionPack]
//--At least one always exists per level, more can exist to depict different layers. Contains a single
//  integer for every tile indicating how it collides.
typedef struct CollisionPack
{
    //--Variables
    int mCollisionSizeX, mCollisionSizeY;
    uint8_t **mCollisionData;
    uint16_t **mCollisionFlipData;

    //--Methods, implemented in TiledLevel.cc
    void Initialize();
    void AllocateSizes(int pXSize, int pYSize);
    void ClearTo(int pCode);
    int GetClipAt(int pX, int pY);
    static void FreeThis(void *pPtr);
}CollisionPack;

///===================================== Local Definitions ========================================
//--Depth Sorting
#define DEPTH_MIDGROUND -0.500000f
#define DEPTH_PER_TILE 0.000100f

//--Collision
#define CLIP_NONE 0
#define CLIP_ALL 1
#define CLIP_EMPTYA 2
#define CLIP_EMPTYB 3
#define CLIP_TL 4
#define CLIP_TOP 5
#define CLIP_TR 6
#define CLIP_DTLE 7
#define CLIP_DTRE 8
#define CLIP_DTLF 9
#define CLIP_DTRF 10
#define CLIP_EMPTYC 11
#define CLIP_EMPTYD 12
#define CLIP_EMPTYE 13
#define CLIP_LFT 14
#define CLIP_NARROWV 15
#define CLIP_RGT 16
#define CLIP_DBLE 17
#define CLIP_DBRE 18
#define CLIP_DBLF 19
#define CLIP_DBRF 20
#define CLIP_EMPTYG 21
#define CLIP_EMPTYH 22
#define CLIP_EMPTYI 23
#define CLIP_BL 24
#define CLIP_BOT 25
#define CLIP_BR 26
#define CLIP_THIN_LF 27
#define CLIP_THIN_RT 28
#define CLIP_QTL 29
#define CLIP_QTR 30
#define CLIP_QBL 39
#define CLIP_QBR 40

//--Codes
#define TL_CODE_CONSTRUCTOR 0
#define TL_CODE_UPDATE 1

//--Compile Properties
#define PAD_TILE_EDGES true

//--Scale
#define TL_DEFAULT_CAMERA_SCALE 3.0f

///========================================== Classes =============================================
class TiledLevel : public RootLevel
{
    private:

    protected:
    friend class AdventureLevelGenerator;
    //--System
    //--Sizing
    float mCurrentScale;
    int mXSize, mYSize; //In Tiles

    //--Tileset Packs
    int mTilesetsTotal;
    TilesetInfoPack *mTilesetPacks;

    //--Raw Lists
    StarLinkedList *mTileLayers; //TileLayer *, master
    StarLinkedList *mObjectData;
    StarLinkedList *mImageData;

    //--Collision Storage
    int mCollisionLayersTotal;
    CollisionPack *mCollisionLayers;

    public:
    //--System
    TiledLevel();
    virtual ~TiledLevel();

    //--Public Variables
    //--Property Queries
    virtual bool GetClipAt(float pX, float pY, float pZ, int pLayer);
    virtual bool GetClipAt(float pX, float pY, float pZ);

    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    virtual void CompileAfterParse();

    public:
    //--Update
    virtual void Update();

    //--File I/O
    void ParseFile(StarLumpManager *pSLM);
    void ParseLayerLump(StarLumpManager *pSLM, bool pHasFlipData);
    void ReadObjectData(VirtualFile *pVFile);
    virtual void ParseObjectData();

    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    StarLinkedList *GetObjectData();
    CollisionPack *GetCollisionPack(int pDepth);

    //--Static Functions
    static TiledLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
