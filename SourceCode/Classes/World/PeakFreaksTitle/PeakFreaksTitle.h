///====================================== PeakFreaksTitle ==========================================
//--Title Screen for Peak Freaks.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"
#include "PeakFreaksCommonDef.h"

///===================================== Local Definitions ========================================
//--Frames and Timings
#define PFTITLE_LOGO_FRAMES 30
#define PFTITLE_LOGO_TPF 1
#define PFTITLE_FLAG_FRAMES 30
#define PFTITLE_FLAG_TPF 3
#define PFTITLE_SELECTION_MAX_TICKS 15
#define PFTITLE_SELECTION_MAX_SIZE 0.15f
#define PFTITLE_DIFFSWITCH_TICKS 15
#define PFTITLE_PLAYERSLIDE_TICKS 25
#define PFTITLE_RIVALSLIDE_TICKS 15
#define PFTITLE_MUSIC_TICKS 25
#define PFTITLE_SPINNER_FRAMES 4
#define PFTITLE_SPINNER_SHAKE 45
#define PFTITLE_SPINNER_TPF 60

//--Menu Options
#define PFTITLE_OPTION_START 0
#define PFTITLE_OPTION_TUTORIAL 1
#define PFTITLE_OPTION_SCORES 2
#define PFTITLE_OPTION_TOTAL 3

//--Cursor Cycle
#define PFTITLE_CURSOR_CYCLE_TICKS 30.0f
#define PFTITLE_CURSOR_CYCLE_DIST 5.0f

//--Difficulty
#define PTTITLE_DIFFICULTY_SIMPLE 0
#define PTTITLE_DIFFICULTY_CHALLENGING 1
#define PTTITLE_DIFFICULTY_IMPOSSIBLE 2
#define PTTITLE_DIFFICULTY_MAX 3

//--High Scores
#define PFTITLE_HIGHSCORES_TICKS_MAX 60
#define PFTITLE_HIGHSCORES_TICKS_FADEOUT 30
#define PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY 3
#define PFTITLE_HIGHSCORES_TIME_SCOOTER 240
#define PFTITLE_HIGHSCORES_TIME_MIMI 250
#define PFTITLE_HIGHSCORES_TIME_ALLEN 245

//--Finale
#define PFTITLE_FINALE_REMATCH 0
#define PFTITLE_FINALE_QUIT 1
#define PFTITLE_FINALE_TOTAL 2

//--Finale Timers
#define PFTITLE_FINALE_TICKS 45
#define PFTITLE_FINALE_FADE_TICKS 120
#define PFTITLE_FINALE_CREDITS_TICKS 1200
#define PFTITLE_FINALE_ALIEN_TICKS 120
#define PFTITLE_FINALE_ALIEN_DISTANCE 10.0f

//--Crossfading
#define PFTITLE_FADE_TO_LEVEL_TICKS 30
#define PFTITLE_TO_TUTORIAL 0
#define PFTITLE_TO_LEVEL 1

//--Fireworks
#ifndef PFTITLE_FIREWORKS_COLS
#define PFTITLE_FIREWORKS_COLS 4
#define PFTITLE_FIREWORKS_FRAMES 13
#endif

///========================================== Classes =============================================
class PeakFreaksTitle : public RootLevel
{
    private:
    ///--[System]
    int mOpenTimer;
    int mLogoTimer;
    int mFlagTimer;
    int mSpinnerTimer;
    float mMusicVolumeNormal;

    ///--[Music]
    int mMusicTimer;

    ///--[Seed Mandate]
    int mSeedProgress;
    int mPressedNumbers[4];

    ///--[Selection]
    int mCurSelection;
    int mCursorTimer;
    int mSelectionTimers[PFTITLE_OPTION_TOTAL];

    ///--[Clouds]
    bool mShouldSpawnClouds;
    int mCloudSpawnTimer;
    SugarLinkedList *mCloudList; //PFTitle_Cloud *, master

    ///--[Difficulty Select]
    bool mIsDifficultyMode;
    int mDifficultySelection;
    int mDifficultySelectionTimers[PTTITLE_DIFFICULTY_MAX];
    int mDifficultyTimer;
    int mPlayerSlideTimer;
    int mVersusSlideTimers[PTTITLE_DIFFICULTY_MAX];

    ///--[High Scores]
    bool mIsScoresMode;
    int mScoresTimer;
    int mScoresScooterTimer;
    int mScoresMimiTimer;
    int mScoresAllenTimer;
    int mScoreTableEasy[PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY];
    int mScoreTableMedium[PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY];
    int mScoreTableHard[PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY];
    bool mScoresDefeatedScooter;
    bool mScoresDefeatedMimi;
    bool mScoresDefeatedAllen;
    char mScoreTimeBuffer[80];

    ///--[Fade To Level]
    int mFadeCase;
    int mFadeToLevelTimer;

    ///--[Finale]
    bool mIsFinaleMode;
    int mFinaleTimer;
    int mFinaleFadeTimer;
    int mFireworksSFXTimer;
    int mCreditsTimer;
    int mAlienTimer;
    bool mShowPersonalBest;
    int mFinaleCursor;
    float mLftPorXOff;
    float mLftPorYOff;
    int mLftPorFlags;
    float mRgtPorXOff;
    float mRgtPorYOff;
    int mRgtPorFlags;
    SugarBitmap *rLftPortrait;
    SugarBitmap *rRgtPortrait;
    int mFireworksTimer;
    SugarLinkedList *mFireworks; //PFTitle_Firework *, master

    ///--[Images]
    struct
    {
        bool mIsReady;
        ///--[Main Menu]
        struct
        {
            //--Images
            SugarBitmap *rBackgroundSky;
            SugarBitmap *rBackgroundMtn;
            SugarBitmap *rClouds[PFTITLE_CLOUD_FRAMES];
            SugarBitmap *rCursor;
            SugarBitmap *rNotifierKeyboard;
            SugarBitmap *rNotifierGamepad;
            SugarBitmap *rTextScores;
            SugarBitmap *rTextStart;
            SugarBitmap *rTextTutorial;
            SugarBitmap *rSelectedScores;
            SugarBitmap *rSelectedStart;
            SugarBitmap *rSelectedTutorial;

            //--Difficulty Select
            SugarBitmap *rDiffQuotes[PTTITLE_DIFFICULTY_MAX];
            SugarBitmap *rDiffTitles[PTTITLE_DIFFICULTY_MAX];
            SugarBitmap *rDiffPortraits[PTTITLE_DIFFICULTY_MAX];
            SugarBitmap *rDiffSelected[PTTITLE_DIFFICULTY_MAX];
            SugarBitmap *rDiffText[PTTITLE_DIFFICULTY_MAX];
            SugarBitmap *rDiffVersus;
            SugarBitmap *rDiffPlayerPortrait;

            //--High Scores
            SugarFont *rScoresFont;
            SugarBitmap *rScoresBack;
            SugarBitmap *rScoresBoard;
            SugarBitmap *rScoresEmpty;
            SugarBitmap *rScoresLabel;
            SugarBitmap *rScoresTrophyAllen;
            SugarBitmap *rScoresTrophyMimi;
            SugarBitmap *rScoresTrophyScooter;

            //--Logo and Flags
            SugarBitmap *rLogoFrames[PFTITLE_LOGO_FRAMES];
            SugarBitmap *rFlagFrames[PFTITLE_FLAG_FRAMES];
            SugarBitmap *rSpinnerFrames[PFTITLE_SPINNER_FRAMES];

        }Data;
        ///--[Finale]
        struct
        {
            //--Fonts
            SugarFont *rTimeFont;
            SugarFont *rCreditsFont;

            //--Images
            SugarBitmap *rAlien;
            SugarBitmap *rBackgroundSky;
            SugarBitmap *rClouds[PFTITLE_CLOUD_FRAMES];
            SugarBitmap *rCreditsBack;
            SugarBitmap *rCreditsBar;
            SugarBitmap *rLabelTooBad;
            SugarBitmap *rLabelWinner;
            SugarBitmap *rPersonalBestIco;
            SugarBitmap *rSelection[PFTITLE_FINALE_TOTAL];
            SugarBitmap *rText[PFTITLE_FINALE_TOTAL];
            SugarBitmap *rTimerMenu;

            //--Fireworks
            SugarBitmap *rFireworks[PFTITLE_FIREWORKS_COLS][PFTITLE_FIREWORKS_FRAMES];
        }Finale;
    }Images;

    protected:

    public:
    //--System
    PeakFreaksTitle();
    virtual ~PeakFreaksTitle();
    void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators

    //--Core Methods
    void SpawnCloud();
    void SpawnCloudEdge();
    void SpawnCloudFinale();
    void SpawnCloudFinaleEdge();
    void ResolveScoreIntoBuffer(int pTicks);
    void LoadHighScores();

    ///--[Finale]
    void ActivateFinale();
    void ShowPersonalBest();
    void UpdateFinale();
    void RenderFinale();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    void UpdateDifficulty();
    void UpdateScores();

    //--File I/O
    static PFTitle_Highscore *ReadHighscores();
    static void WriteHighscores(PFTitle_Highscore *pScoreStruct);

    //--Drawing
    void RenderMenuOption(int pSlot, int pCursor, SugarBitmap *pImage, SugarBitmap *pSelImg);
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    static PeakFreaksTitle *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PeakTitle_Create(lua_State *L);

