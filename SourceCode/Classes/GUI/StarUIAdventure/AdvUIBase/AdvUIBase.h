///========================================= AdvUIBase ============================================
//--The base component of the UI, this is what appears when the player opens the menu. It contains
//  a grid that allows the player to select submenus to open. This also handles all other grid modes,
//  such as selecting a party member to chat with or a form to transform into.
//--This also displays Adamantite, Catalyst, Status, and other such useful information.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIGrid.h"
#include "DisplayManager.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#ifndef _ADVUIBASE_DEFINITIONS_
#define _ADVUIBASE_DEFINITIONS_

//--Timers
#define ADV_MENU_VIS_TICKS 7
#define ADV_MENU_SIZE_TICKS 10
#define ADV_MENU_SIZE_BOOST 0.50f
#define ADV_MENU_BASE_CURSOR_MOVE_TICKS 7

//--Main Menu Codes
#define AM_MAIN_ITEMS 0
#define AM_MAIN_EQUIPMENT 1
#define AM_MAIN_DOCTOR 2
#define AM_MAIN_STATUS 3
#define AM_MAIN_SKILLS 4
#define AM_MAIN_MAP 5
#define AM_MAIN_OPTIONS 6
#define AM_MAIN_JOURNAL 7
#define AM_MAIN_QUIT 8
#define AM_MAIN_SWITCH 9
#define AM_MAIN_TOTAL 10

//--Grid Positioning Values
#define AM_BASE_GRID_CENT_X (VIRTUAL_CANVAS_X * 0.50f)
#define AM_BASE_GRID_CENT_Y (VIRTUAL_CANVAS_Y * 0.30f)
#define AM_BASE_GRID_SPAC_X (VIRTUAL_CANVAS_X * 0.10f)
#define AM_BASE_GRID_SPAC_Y (VIRTUAL_CANVAS_X * 0.10f)

//--Alternate grid.
#define AM_ALT_GRID_CENT_Y (VIRTUAL_CANVAS_Y * 0.35f)

//--Stencil Codes
#define AM_STENCIL_FOOTER 1
#define AM_STENCIL_MAIN 2
#define AM_STENCIL_STATUS 10
#define AM_STENCIL_EQUIP 20
#define AM_STENCIL_SKILLS 30
#define AM_STENCIL_VENDOR 40

#define ADVMENU_BASE_HELP_MENU_STRINGS 14
#endif

///--[External Definitions]
//--Forward Declaration
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

#ifndef _CATALYST_TYPES_
#define _CATALYST_TYPES_
#define CATALYST_HEALTH 0
#define CATALYST_ATTACK 1
#define CATALYST_INITIATIVE 2
#define CATALYST_DODGE 3
#define CATALYST_ACCURACY 4
#define CATALYST_SKILL 5
#define CATALYST_TOTAL 6
#endif

///========================================== Classes =============================================
class AdvUIBase : virtual public AdvUICommon, virtual public AdvUIGrid
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxAdvBaseHelpStrings = 14;

    ///--[Variables]
    //--System
    bool mIsFirstTick;
    bool mHasBuiltGrid;
    bool mDisplaySkillAlt;
    bool mDontShowHighlight;

    //--Cursor
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Strings
    StarlightString *mShowHelpString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rDoubleHeadingFont;
        StarFont *rHeadingFont;
        StarFont *rMainlineFont;
        StarFont *rLevelFont;
        StarFont *rStatisticFont;

        //--Images
        StarBitmap *rAdamantiteDetail;
        StarBitmap *rAdamantiteFrame;
        StarBitmap *rAdamantiteHeader;
        StarBitmap *rCatalystDetail;
        StarBitmap *rCatalystFrame;
        StarBitmap *rCatalystHeader;
        StarBitmap *rCharacterFrame;
        StarBitmap *rCharacterHPFill;
        StarBitmap *rCharacterHPFrame;
        StarBitmap *rCharacterLevelBack;
        StarBitmap *rCharacterLevelFront;
        StarBitmap *rCharacterNameBanner;
        StarBitmap *rFooter;
        StarBitmap *rHeader;
        StarBitmap *rPlatinaFrame;

        //--Icons
        StarBitmap *rIcons[AM_MAIN_TOTAL];
        StarBitmap *rSkillAlt;

        //--Common
        StarBitmap *rHighlight;
        StarBitmap *rAdamantiteImg[CRAFT_ADAMANTITE_TOTAL];
        StarBitmap *rCatalystImg[CATALYST_TOTAL];
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIBase();
    virtual ~AdvUIBase();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void SetCursorByBackwardsCode(int pMode);
    virtual void SetCursor(int pCursor);
    virtual void TakeForeground();
    void ResetBuiltGridFlag();
    virtual void QuicksetGridEntry(int pX, int pY, int pCode, const char *pName, StarBitmap *pImagePtr);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderForeground(float pVisAlpha);
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    void RenderMainCharacter(int pSlot, int pPartySize);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


