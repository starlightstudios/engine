//--Base
#include "AdventureLevel.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

void AdventureLevel::AllocateForegroundPacks(int pTotal)
{
    //--Deallocate.
    mOverlaysTotal = 0;
    free(mOverlayPacks);
    if(pTotal < 1) return;

    //--Allocate.
    mOverlaysTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mOverlayPacks = (OverlayPackage *)starmemoryalloc(sizeof(OverlayPackage) * mOverlaysTotal);
    for(int i = 0; i < mOverlaysTotal; i ++)
    {
        mOverlayPacks[i].Initialize();
    }
}
void AdventureLevel::SetForegroundImage(int pSlot, const char *pPath)
{
    if(pSlot < 0 || pSlot >= mOverlaysTotal) return;
    mOverlayPacks[pSlot].rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdventureLevel::SetForegroundOffsets(int pSlot, float pOffsetX, float pOffsetY, float pScalerX, float pScalerY)
{
    if(pSlot < 0 || pSlot >= mOverlaysTotal) return;
    mOverlayPacks[pSlot].mOffsetBaseX = pOffsetX;
    mOverlayPacks[pSlot].mOffsetBaseY = pOffsetY;
    mOverlayPacks[pSlot].mScrollFactorX = pScalerX;
    mOverlayPacks[pSlot].mScrollFactorY = pScalerY;
}
void AdventureLevel::SetForegroundAutoscroll(int pSlot, float pAutoscrollX, float pAutoscrollY)
{
    if(pSlot < 0 || pSlot >= mOverlaysTotal) return;
    mOverlayPacks[pSlot].mAutoscrollX = pAutoscrollX;
    mOverlayPacks[pSlot].mAutoscrollY = pAutoscrollY;
}
void AdventureLevel::SetForegroundAlpha(int pSlot, float pTargetAlpha, int pTicks)
{
    //--Error check.
    if(pSlot < 0 || pSlot >= mOverlaysTotal) return;

    //--Clamp the target alpha.
    if(pTargetAlpha < 0.0f) pTargetAlpha = 0.0f;
    if(pTargetAlpha > 1.0f) pTargetAlpha = 1.0f;

    //--If the target alpha is the same as the current alpha, ignore it. This is to prevent
    //  hysterisis cases.
    if(pTargetAlpha == mOverlayPacks[pSlot].mAlphaCurrent) return;

    //--If the ticks value is 1 or lower, set it instantly.
    if(pTicks <= 1)
    {
        mOverlayPacks[pSlot].mAlphaTimer = 100;
        mOverlayPacks[pSlot].mAlphaTimerMax = 100;
        mOverlayPacks[pSlot].mAlphaCurrent = pTargetAlpha;
        mOverlayPacks[pSlot].mAlphaStart = pTargetAlpha;
        mOverlayPacks[pSlot].mAlphaEnd = pTargetAlpha;
    }
    //--Otherwise, slide across by the tick counter.
    else
    {
        mOverlayPacks[pSlot].mAlphaTimer = 0;
        mOverlayPacks[pSlot].mAlphaTimerMax = pTicks;
        mOverlayPacks[pSlot].mAlphaStart = mOverlayPacks[pSlot].mAlphaCurrent;
        mOverlayPacks[pSlot].mAlphaEnd = pTargetAlpha;
    }
}
void AdventureLevel::SetForegroundScaler(int pSlot, float pScale)
{
    if(pSlot < 0 || pSlot >= mOverlaysTotal) return;
    mOverlayPacks[pSlot].mScaleFactor = pScale;
}

///--[Background/Underlays]
void AdventureLevel::AllocateBackgroundPacks(int pTotal)
{
    //--Deallocate.
    mUnderlaysTotal = 0;
    free(mUnderlayPacks);
    if(pTotal < 1) return;

    //--Allocate.
    mUnderlaysTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mUnderlayPacks = (OverlayPackage *)starmemoryalloc(sizeof(OverlayPackage) * mUnderlaysTotal);
    for(int i = 0; i < mUnderlaysTotal; i ++)
    {
        mUnderlayPacks[i].Initialize();
    }
}
void AdventureLevel::SetBackgroundImage(int pSlot, const char *pPath)
{
    if(pSlot < 0 || pSlot >= mUnderlaysTotal) return;
    mUnderlayPacks[pSlot].rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdventureLevel::SetBackgroundOffsets(int pSlot, float pOffsetX, float pOffsetY, float pScalerX, float pScalerY)
{
    if(pSlot < 0 || pSlot >= mUnderlaysTotal) return;
    mUnderlayPacks[pSlot].mOffsetBaseX = pOffsetX;
    mUnderlayPacks[pSlot].mOffsetBaseY = pOffsetY;
    mUnderlayPacks[pSlot].mScrollFactorX = pScalerX;
    mUnderlayPacks[pSlot].mScrollFactorY = pScalerY;
}
void AdventureLevel::SetBackgroundAutoscroll(int pSlot, float pAutoscrollX, float pAutoscrollY)
{
    if(pSlot < 0 || pSlot >= mUnderlaysTotal) return;
    mUnderlayPacks[pSlot].mAutoscrollX = pAutoscrollX;
    mUnderlayPacks[pSlot].mAutoscrollY = pAutoscrollY;
}
void AdventureLevel::SetBackgroundAlpha(int pSlot, float pTargetAlpha, int pTicks)
{
    //--Error check.
    if(pSlot < 0 || pSlot >= mUnderlaysTotal) return;

    //--Clamp the target alpha.
    if(pTargetAlpha < 0.0f) pTargetAlpha = 0.0f;
    if(pTargetAlpha > 1.0f) pTargetAlpha = 1.0f;

    //--If the target alpha is the same as the current alpha, ignore it. This is to prevent
    //  hysterisis cases.
    if(pTargetAlpha == mUnderlayPacks[pSlot].mAlphaCurrent) return;

    //--If the ticks value is 1 or lower, set it instantly.
    if(pTicks <= 1)
    {
        mUnderlayPacks[pSlot].mAlphaTimer = 100;
        mUnderlayPacks[pSlot].mAlphaTimerMax = 100;
        mUnderlayPacks[pSlot].mAlphaCurrent = pTargetAlpha;
        mUnderlayPacks[pSlot].mAlphaStart = pTargetAlpha;
        mUnderlayPacks[pSlot].mAlphaEnd = pTargetAlpha;
    }
    //--Otherwise, slide across by the tick counter.
    else
    {
        mUnderlayPacks[pSlot].mAlphaTimer = 0;
        mUnderlayPacks[pSlot].mAlphaTimerMax = pTicks;
        mUnderlayPacks[pSlot].mAlphaStart = mUnderlayPacks[pSlot].mAlphaCurrent;
        mUnderlayPacks[pSlot].mAlphaEnd = pTargetAlpha;
    }
}
void AdventureLevel::SetBackgroundScaler(int pSlot, float pScale)
{
    if(pSlot < 0 || pSlot >= mUnderlaysTotal) return;
    mUnderlayPacks[pSlot].mScaleFactor = pScale;
}
