//--Base
#include "ScriptObjectBuilder.h"

//--Classes
#include "StringEntry.h"
#include "StarObjectPrototype.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "MapManager.h"

///--[Debug]
//#define SOB_UPDATE_DEBUG
#ifdef SOB_UPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Update ==============================================
void ScriptObjectBuilder::Update()
{
    ///--[Documentation and Setup]
    //--Diagnostics.
    DebugPush(true, "ScriptObjectBuilder::Update() - Begin.\n");

    ///--[Mode Switch]
    //--In prototype mode:
    if(mCurrentMode == SOB_MODE_PROTOTYPE)
    {
        UpdatePrototype();
    }
    //--In field editing mode:
    else if(mCurrentMode == SOB_MODE_FIELD)
    {
        UpdateFields();
    }
    //--Editing the buffer of a field.
    else if(mCurrentMode == SOB_MODE_EDIT)
    {
        UpdateFieldEdit();
    }
    //--Error.
    else
    {
        MapManager::Fetch()->mBackToTitle = true;
        DebugPop("ScriptObjectBuilder::Update() - Unhandled mode %i. Failing.\n", mCurrentMode);
        return;
    }

    ///--[Finish]
    //--Diagnostics.
    DebugPop("ScriptObjectBuilder::Update() - Completes normally.\n");
}

///====================================== Prototype Mode ==========================================
void ScriptObjectBuilder::UpdatePrototype()
{
    ///--[Documentation and Setup]
    //--Prototype mode. The user can select from a list of StarObjectPrototypes using the cursor keys. Handles
    //  scrolling, exiting the mode, and selecting a prototype.
    //--Diagnostics.
    DebugPush(true, "ScriptObjectBuilder::UpdatePrototype() - Begin.\n");

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Escape]
    //--Returns the player to the menu.
    if(rControlManager->IsFirstPress("SOB Escape"))
    {
        MapManager::Fetch()->mBackToTitle = true;
        DebugPop("ScriptObjectBuilder::Update() - User exited interface. Stopping.\n");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    //--No prototype list, or it has no entries.
    if(!mPrototypeList || mPrototypeList->GetListSize() < 1)
    {
        DebugPop("ScriptObjectBuilder::Update() - List failed to resolve or has zero entries. Stopping.\n");
        return;
    }

    ///--[Arrow Keys]
    //--Pressing Up.
    if(rControlManager->IsFirstPress("SOB Up") && !rControlManager->IsFirstPress("SOB Down"))
    {
        //--Speed increase. Scrolls 10 entries when Ctrl is held down. This is down iteratively
        //  to make sure that wrapping and scrolling happen naturally.
        int tIterations = -1;
        if(rControlManager->IsDown("SOB Accel")) tIterations = -10;

        //--Handler.
        IncrementWithSkipAndPages(mPrototypeCursor, mPrototypeSkip, tIterations, mItemsPerPage, mPrototypeList->GetListSize(), 3, 3, true);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Pressing Down.
    else if(rControlManager->IsFirstPress("SOB Down") && !rControlManager->IsFirstPress("SOB Up"))
    {
        //--Speed increase. Scrolls 10 entries when Ctrl is held down. This is down iteratively
        //  to make sure that wrapping and scrolling happen naturally.
        int tIterations = 1;
        if(rControlManager->IsDown("SOB Accel")) tIterations = 10;

        //--Handler.
        IncrementWithSkipAndPages(mPrototypeCursor, mPrototypeSkip, tIterations, mItemsPerPage, mPrototypeList->GetListSize(), 3, 3, true);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Selection]
    if(rControlManager->IsFirstPress("SOB Select"))
    {
        //--Set the active prototype. If it fails to resolve for some reason, stop.
        rActivePrototype = (StarObjectPrototype *)mPrototypeList->GetElementBySlot(mPrototypeCursor);
        if(!rActivePrototype)
        {
            DebugPop("ScriptObjectBuilder::UpdatePrototype() - User switched to Prototype mode, but prototype was not found. Failing.\n");
            AudioManager::Fetch()->PlaySound("Menu|Error");
            return;
        }

        //--Set mode.
        mCurrentMode = SOB_MODE_FIELD;

        //--Reset cursors.
        mFieldCursor = 0;
        mFieldSkip = 0;

        //--Finish.
        DebugPop("ScriptObjectBuilder::UpdatePrototype() - User switched to Prototype mode. Stopping.\n");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Finish]
    //--Diagnostics.
    DebugPop("ScriptObjectBuilder::UpdatePrototype() - Completes normally.\n");
}

///======================================== Fields Mode ===========================================
void ScriptObjectBuilder::UpdateFields()
{
    ///--[Documentation and Setup]
    //--Fields Mode. Once a prototype is selected, the user can edit the various fields within it, changing
    //  numbers and strings. Pressing the export button will run a lua file which takes the fields and builds
    //  and exports a lua script file, ready for use in the game.
    //--The user can also save the default fields for the prototype, revert them to their base values, and
    //  check the built-in field descriptions.
    DebugPush(true, "ScriptObjectBuilder::UpdateFields() - Begin.\n");

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Escape]
    //--Returns to the prototype selection menu.
    if(rControlManager->IsFirstPress("SOB Escape"))
    {
        mCurrentMode = SOB_MODE_PROTOTYPE;
        DebugPop("ScriptObjectBuilder::UpdateFields() - User returned to prototypes mode. Stopping.\n");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Export]
    //--Sends the data to the script that handles exporting. Stops the update.
    if(rControlManager->IsFirstPress("SOB Export"))
    {
        //--Call subroutine.
        HandleExport();

        //--Finish.
        DebugPop("ScriptObjectBuilder::UpdateFields() - User exported. Stopping.\n");
        return;
    }

    ///--[Arrow Keys]
    //--Pressing Up.
    if(rControlManager->IsFirstPress("SOB Up") && !rControlManager->IsFirstPress("SOB Down"))
    {
        //--Speed increase. Scrolls 10 entries when Ctrl is held down. This is down iteratively
        //  to make sure that wrapping and scrolling happen naturally.
        int tIterations = -1;
        if(rControlManager->IsDown("SOB Accel")) tIterations = -10;

        //--Handler.
        IncrementWithSkipAndPages(mFieldCursor, mFieldSkip, tIterations, mItemsPerPage, rActivePrototype->GetTotalFields(), 3, 3, true);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Pressing Down.
    else if(rControlManager->IsFirstPress("SOB Down") && !rControlManager->IsFirstPress("SOB Up"))
    {
        //--Speed increase. Scrolls 10 entries when Ctrl is held down. This is down iteratively
        //  to make sure that wrapping and scrolling happen naturally.
        int tIterations = 1;
        if(rControlManager->IsDown("SOB Accel")) tIterations = 10;

        //--Handler.
        IncrementWithSkipAndPages(mFieldCursor, mFieldSkip, tIterations, mItemsPerPage, rActivePrototype->GetTotalFields(), 3, 3, true);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Pressing Left.
    if(rControlManager->IsFirstPress("SOB Left") && !rControlManager->IsFirstPress("SOB Right"))
    {
        //--Get and check the field.
        StarObjectField *rDisplayField = rActivePrototype->GetFieldI(mFieldCursor);
        if(!rDisplayField)
        {
            DebugPop("ScriptObjectBuilder::UpdateFields() - Pressed left on an invalid field. Failing.\n");
            return;
        }

        //--For integer fields, decrement.
        if(rDisplayField->mType == POINTER_TYPE_INT)
        {
            rDisplayField->mNumeric.i = rDisplayField->mNumeric.i - 1;
            rDisplayField->CheckAgainstEnumerations();
            DebugPop("ScriptObjectBuilder::UpdateFields() - User decremented an integer. Stopping.\n");
            return;
        }

        //--Other field types, enter editing mode with a clear buffer.
        mEditCursor = 0;
        mCurrentMode = SOB_MODE_EDIT;
        memset(mEditingBuffer, 0, sizeof(char) * SOB_EDIT_BUF_SIZE);

        //--Finish.
        DebugPop("ScriptObjectBuilder::UpdateFields() - User began editing a field. Stopping.\n");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
    //--Pressing Right.
    else if(rControlManager->IsFirstPress("SOB Right") && !rControlManager->IsFirstPress("SOB Left"))
    {
        //--Get and check the field.
        StarObjectField *rDisplayField = rActivePrototype->GetFieldI(mFieldCursor);
        if(!rDisplayField)
        {
            DebugPop("ScriptObjectBuilder::UpdateFields() - Pressed right on an invalid field. Failing.\n");
            return;
        }

        //--For integer fields, decrement.
        if(rDisplayField->mType == POINTER_TYPE_INT)
        {
            rDisplayField->mNumeric.i = rDisplayField->mNumeric.i + 1;
            rDisplayField->CheckAgainstEnumerations();
            DebugPop("ScriptObjectBuilder::UpdateFields() - User incremented an integer. Stopping.\n");
            return;
        }

        //--Other field types, enter editing mode.
        mEditCursor = 0;
        mCurrentMode = SOB_MODE_EDIT;

        //--Copy into field.
        if(rDisplayField->mType == POINTER_TYPE_FLOAT)
        {
            sprintf(mEditingBuffer, "%f", rDisplayField->mNumeric.f);
        }
        else if(rDisplayField->mType == POINTER_TYPE_STRING)
        {
            sprintf(mEditingBuffer, "%s", rDisplayField->mAlpha);
        }

        //--Place the edit cursor on the last letter.
        bool tBufferIsFull = true;
        for(int i = 0; i < SOB_EDIT_BUF_SIZE-1; i ++)
        {
            if(mEditingBuffer[i] == 0)
            {
                tBufferIsFull = false;
                mEditCursor = i;
                break;
            }
        }

        //--Clamp. If the buffer is filled with all characters, set the edit cursor onto the last one.
        if(tBufferIsFull) mEditCursor = SOB_EDIT_BUF_SIZE - 1;

        //--Finish.
        DebugPop("ScriptObjectBuilder::UpdateFields() - User began editing a field. Stopping.\n");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Selection]
    if(rControlManager->IsFirstPress("SOB Select"))
    {
        //--Activates field editing mode. A valid field must be under the cursor.
        StarObjectField *rDisplayField = rActivePrototype->GetFieldI(mFieldCursor);
        if(!rDisplayField)
        {
            DebugPop("ScriptObjectBuilder::UpdateFields() - User activated editing on a field that is invalid. Failing.\n");
            return;
        }

        //--Flag.
        mCurrentMode = SOB_MODE_EDIT;
        mEditCursor = 0;

        //--Clear the current buffer.
        memset(mEditingBuffer, 0, SOB_EDIT_BUF_SIZE);

        mEditCursor = 0;
        mCurrentMode = SOB_MODE_EDIT;
        memset(mEditingBuffer, 0, sizeof(char) * SOB_EDIT_BUF_SIZE);

        //--Finish.
        DebugPop("ScriptObjectBuilder::UpdateFields() - User began editing a field. Stopping.\n");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Finish]
    //--Diagnostics.
    DebugPop("ScriptObjectBuilder::UpdateFields() - Completes normally.\n");
}

///====================================== Field Edit Mode =========================================
void ScriptObjectBuilder::UpdateFieldEdit()
{
    ///--[Documentation and Setup]
    //--When editing mode is active, this routine is called to handle keyboard input.
    DebugPush(true, "ScriptObjectBuilder::UpdateFieldEdit() - Begin.\n");

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Escape]
    //--Returns to the field selection, does not save the value.
    if(rControlManager->IsFirstPress("SOB Escape"))
    {
        mCurrentMode = SOB_MODE_FIELD;
        DebugPop("ScriptObjectBuilder::UpdateFieldEdit() - User cancelled. Stopping.\n");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Arrow Keys]
    //--Pressing Left.
    if(rControlManager->IsFirstPress("SOB Left") && !rControlManager->IsFirstPress("SOB Right"))
    {
        //--Move cursor left.
        mEditCursor --;

        //--Clamp.
        if(mEditCursor < 0) mEditCursor = 0;
    }
    //--Pressing Down.
    else if(rControlManager->IsFirstPress("SOB Right") && !rControlManager->IsFirstPress("SOB Left"))
    {
        //--Move cursor right.
        mEditCursor ++;

        //--Clamp.
        if(mEditCursor >= SOB_EDIT_BUF_SIZE) mEditCursor = SOB_EDIT_BUF_SIZE-1;
    }

    ///--[Backspace]
    //--Deletes the selected character, if one exists, and decrements the cursor.
    if(rControlManager->IsFirstPress("SOB Backspace"))
    {
        //--If the cursor is on space zero, do nothing.
        if(mEditCursor == 0)
        {

        }
        //--Valid case.
        else
        {
            //--Copy the buffer back one. This removes the previous letter. Space SOB_EDIT_BUF_SIZE-1 always becomes a NULL.
            for(int i = mEditCursor-1; i < SOB_EDIT_BUF_SIZE-1; i ++)
            {
                mEditingBuffer[i] = mEditingBuffer[i+1];
            }

            //--Decrement editing cursor.
            mEditCursor --;
        }
    }

    ///--[Selection]
    //--Takes the provided buffer and translates it into the type requested. If invalid, sets to a default.
    //  In either case, stops editing mode.
    if(rControlManager->IsFirstPress("SOB Select"))
    {
        //--Get the field.
        StarObjectField *rDisplayField = rActivePrototype->GetFieldI(mFieldCursor);
        if(!rDisplayField)
        {
            DebugPop("ScriptObjectBuilder::UpdateFieldEdit() - User finished editing, but the display field was somehow lost. Failing.\n");
            return;
        }

        //--Save.
        if(rDisplayField->mType == POINTER_TYPE_INT)
        {
            rDisplayField->mNumeric.i = atoi(mEditingBuffer);
            rDisplayField->CheckAgainstEnumerations();
        }
        else if(rDisplayField->mType == POINTER_TYPE_FLOAT)
        {
            rDisplayField->mNumeric.f = atof(mEditingBuffer);
        }
        else if(rDisplayField->mType == POINTER_TYPE_STRING)
        {
            ResetString(rDisplayField->mAlpha, mEditingBuffer);
        }

        //--End editing mode.
        mCurrentMode = SOB_MODE_FIELD;

        //--Finish.
        DebugPop("ScriptObjectBuilder::UpdateFieldEdit() - User finished editing. Stopping.\n");
        return;
    }

    ///--[Letter Handling]
    //--Shift tracking.
    bool tIsShifted = rControlManager->IsDown("SOB Shift");

    //--Handles conventional typing.
    int tKeyboard, tMouse, tJoy;
    rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoy);
    const char *rPressedKeyName = rControlManager->GetNameOfKeyIndex(tKeyboard);
    if(rPressedKeyName)
    {
        //--To be valid, the key must have one letter in its name, and it must be standard ASCII.
        if(strlen(rPressedKeyName) == 1 && rPressedKeyName[0] < 128)
        {
            //--Setup.
            char tBuf[2];
            tBuf[0] = rPressedKeyName[0];
            tBuf[1] = '\0';

            //--Downshift it if it's a letter.
            if(tBuf[0] >= 'A' && tBuf[0] <= 'Z')
            {
                tBuf[0] = tBuf[0] + 'a' - 'A';
            }

            //--If the key is shifted, move it to uppercase.
            if(tIsShifted)
            {
                //--Letters.
                if(tBuf[0] >= 'a' && tBuf[0] <= 'z')
                {
                    tBuf[0] = tBuf[0] + 'A' - 'a';
                }
                //--Numerics.
                else if(tBuf[0] >= '0' && tBuf[0] <= '9')
                {
                    if(tBuf[0] == '1') tBuf[0] = '!';
                    if(tBuf[0] == '2') tBuf[0] = '@';
                    if(tBuf[0] == '3') tBuf[0] = '#';
                    if(tBuf[0] == '4') tBuf[0] = '$';
                    if(tBuf[0] == '5') tBuf[0] = '%';
                    if(tBuf[0] == '6') tBuf[0] = '^';
                    if(tBuf[0] == '7') tBuf[0] = '&';
                    if(tBuf[0] == '8') tBuf[0] = '*';
                    if(tBuf[0] == '9') tBuf[0] = '(';
                    if(tBuf[0] == '0') tBuf[0] = ')';
                }
            }

            //--Append it.
            AppendToEditBuffer(tBuf);
        }
        //--Space.
        else if(!strcasecmp(rPressedKeyName, "Space"))
        {
            AppendToEditBuffer(" ");
        }
        //--Capslock.
        else if(!strcasecmp(rPressedKeyName, "CapsLock"))
        {
        }
        //--Shift.
        else if(!strcasecmp(rPressedKeyName, "LShift") || !strcasecmp(rPressedKeyName, "RShift"))
        {
        }
        //--Grave/Tilde.
        else if(!strcasecmp(rPressedKeyName, "Tilde"))
        {
            ConditionalAppendToEditBuffer("`", "~", (tIsShifted == true));
        }
        //--Minus. Can be from the pad.
        else if(!strcasecmp(rPressedKeyName, "Minus"))
        {
            ConditionalAppendToEditBuffer("-", "_", (tIsShifted == true));
        }
        //--Equals. Can be from the pad.
        else if(!strcasecmp(rPressedKeyName, "Equals"))
        {
            ConditionalAppendToEditBuffer("=", "+", (tIsShifted == true));
        }
        //--Left or Open Brace.
        else if(!strcasecmp(rPressedKeyName, "LBrace"))
        {
            ConditionalAppendToEditBuffer("[", "{", (tIsShifted == true));
        }
        //--Right or Close Brace.
        else if(!strcasecmp(rPressedKeyName, "RBrace"))
        {
            ConditionalAppendToEditBuffer("]", "}", (tIsShifted == true));
        }
        //--Semicolon.
        else if(!strcasecmp(rPressedKeyName, "Semicolon"))
        {
            ConditionalAppendToEditBuffer(";", ":", (tIsShifted == true));
        }
        //--Quote.
        else if(!strcasecmp(rPressedKeyName, "Quote"))
        {
            ConditionalAppendToEditBuffer("'", "\"", (tIsShifted == true));
        }
        //--Backslash.
        else if(!strcasecmp(rPressedKeyName, "Backslash"))
        {
            ConditionalAppendToEditBuffer("\\", "|", (tIsShifted == true));
        }
        //--Comma.
        else if(!strcasecmp(rPressedKeyName, "Comma"))
        {
            ConditionalAppendToEditBuffer(",", "<", (tIsShifted == true));
        }
        //--Period.
        else if(!strcasecmp(rPressedKeyName, "Period"))
        {
            ConditionalAppendToEditBuffer(".", ">", (tIsShifted == true));
        }
        //--Slash.
        else if(!strcasecmp(rPressedKeyName, "Slash"))
        {
            ConditionalAppendToEditBuffer("/", "?", (tIsShifted == true));
        }
    }

    ///--[Finish]
    //--Diagnostics.
    DebugPop("ScriptObjectBuilder::UpdateFieldEdit() - Completes normally.\n");
}
