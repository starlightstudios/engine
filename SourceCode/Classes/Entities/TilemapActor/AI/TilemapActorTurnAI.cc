//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers

void TilemapActor::HandleTurnAI()
{
    ///--[Documentation and Setup]
    //--When the AI is turning to face its next patrol node, calls this code.
    bool tIsIgnoringPlayer = IsIgnoringPlayer();
    bool tIsPlayerInSight = IsPlayerInSightCone(TA_USE_COMPUTED_VIEWDIST);
    float tRangeToPlayer = GetRangeToPlayer();
    float cChaseDistance = GetChaseDistance();

    //--Range check. Chase the player if close enough.
    if(tRangeToPlayer <= cChaseDistance && !tIsIgnoringPlayer && tIsPlayerInSight)
    {
        if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
    }
    //--Not close enough, turn to face the path node.
    else
    {
        //--Check the level.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(!rActiveLevel) return;

        //--Immediately go back to wandering if we have no patrol nodes.
        if(!mIsPatrolling || mPatrolNodesTotal < 1)
        {
            mAIState = TA_AI_WANDER;
            return;
        }

        //--Get the patrol node in question.
        TwoDimensionReal tDimensions = rActiveLevel->GetPatrolNode(mPatrolNodeList[mPatrolNodeCurrent]);

        //--Wait ~45 ticks between actions.
        mWaitTimer --;
        if(mWaitTimer < 1)
        {
            //--Reset.
            mWaitTimer = rand() % 30 + 30;

            //--Wait timer is 1/4'd when mNeverPauses is set.
            if(mNeverPauses) mWaitTimer = mWaitTimer / 4;

            //--Facing Angle.
            float cFaceAngle = (mFacing * 45.0f) - 90.0f;
            while(cFaceAngle <    0.0f) cFaceAngle = cFaceAngle + 360.0f;
            while(cFaceAngle >= 360.0f) cFaceAngle = cFaceAngle - 360.0f;

            //--Get the angle to its center point.
            float cAngleTo = GetAngleBetween(mTrueX, mTrueY, tDimensions.mXCenter, tDimensions.mYCenter) * TODEGREE;
            while(cAngleTo <    0.0f) cAngleTo = cAngleTo + 360.0f;
            while(cAngleTo >= 360.0f) cAngleTo = cAngleTo - 360.0f;

            //--Close enough.
            if(fabs(cAngleTo - cFaceAngle) <= 22.5f)
            {
                mAIState = TA_AI_WANDER;
            }
            //--Alternate version of close enough:
            else if(fabs((cAngleTo+360.0f) - cFaceAngle) <= 22.5f || fabs(cAngleTo - (cFaceAngle+360.0f)) <= 22.5f)
            {
                mAIState = TA_AI_WANDER;
            }
            //--Decrease.
            else if(cAngleTo < cFaceAngle)
            {
                mFacing = (mFacing - 1) % TA_DIR_TOTAL;
                if(mFacing < 0) mFacing += TA_DIR_TOTAL;
            }
            //--Increase.
            else
            {
                mFacing = (mFacing + 1) % TA_DIR_TOTAL;
            }
        }
    }

    //--Store the true facing.
    mTrueFacing = mFacing;
}
