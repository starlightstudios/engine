//--Base
#include "MonoUIJournal.h"

//--Classes
#include "AdvCombat.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
MonoUIJournal::MonoUIJournal()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MJOR");

    ///--[Visiblity]
    mVisibilityTimerMax = cMonoVisTicks;

    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUISample ======= ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUIJournal ====== ]
    ///--[System]
    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    ///--[Verification Pack]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIJournal::~MonoUIJournal()
{
}
void MonoUIJournal::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Journal UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Journal UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",              &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIJournal_Strings *MonoUIJournal::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIJournal_Strings::MonoUIJournal_Strings()
{
    //--Local name: "MonoUIJournal"
    MonoUIJournal::xrStringData = this;

    //--Strings
    str.mTextImmune         = InitializeString("MonoUIInventory_mImmune");             //"Immune"
    str.mTextJournal        = InitializeString("MonoUIInventory_mJournal");            //"Journal"
    str.mTextBestiary       = InitializeString("MonoUIInventory_mBestiary");           //"Bestiary"
    str.mTextProfiles       = InitializeString("MonoUIInventory_mProfiles");           //"Profiles"
    str.mTextQuests         = InitializeString("MonoUIInventory_mQuests");             //"Quests"
    str.mTextLocations      = InitializeString("MonoUIInventory_mLocations");          //"Locations"
    str.mTextCombatGlossary = InitializeString("MonoUIInventory_mCombatGlossary");     //"Combat Glossary"
    str.mTextAchievements   = InitializeString("MonoUIInventory_mAchievements");       //"Achievements"
    str.mTextTimesDefeated  = InitializeString("MonoUIInventory_mTimesDefeated");      //"Times Defeated: [iCount] / [iMax]"
    str.mTextObjective      = InitializeString("MonoUIInventory_mObjective");          //"Objective"
    str.mTextLocked         = InitializeString("MonoUIInventory_mLocked");             //"Locked"
    str.mTextHiddenUntil    = InitializeString("MonoUIInventory_mAchievementHidden");  //"This achievement is hidden until it is unlocked."
    str.mTextSpoiler        = InitializeString("MonoUIInventory_mSpoiler!");           //"Spoiler!"
    str.mTextSpoilerExpl    = InitializeString("MonoUIInventory_mAchievementSpoiler"); //"This achievement is a possible spoiler. Push the run key to reveal it."
    str.mTextUnlocked       = InitializeString("MonoUIInventory_mUnlocked:");          //"Unlocked:"

    //--Help Sequence
    //--Control Sequence
    str.mTextControlLeft                 = InitializeString("MonoUIInventory_mControlLeft");                 //"[IMG0] Left"
    str.mTextControlRight                = InitializeString("MonoUIInventory_mControlRight");                //"Right [IMG0]"
    str.mTextControlShowPlantchievements = InitializeString("MonoUIInventory_mControlShowPlantchievements"); //"[IMG0] Show Plantchievements"
    str.mTextControlHidePlantchievements = InitializeString("MonoUIInventory_mControlHidePlantchievements"); //"[IMG0] Hide Plantchievements"
    str.mTextControlScrollWithinCategory = InitializeString("MonoUIInventory_mControlScrollWithinCategory"); //"[IMG0][IMG1] Scroll Within Category"
    str.mTextControlToggleSpoilers       = InitializeString("MonoUIInventory_mControlToggleSpoilers");       //"[IMG0] Toggle Spoilers"
    str.mTextControlShowFullPortrait     = InitializeString("MonoUIInventory_mControlShowFullPortrait");     //"[IMG0] Show full portrait"
    str.mTextControlToggleParagon        = InitializeString("MonoUIInventory_mControlToggleParagon");        //"[IMG0] Toggle paragon"
    str.mTextControlEnableDisable        = InitializeString("MonoUIInventory_mControlEnableDisable");        //"[IMG0] Enable/Disable"
    str.mTextControlExit                 = InitializeString("MonoUIInventory_mControlExit");                 //"[IMG0] Exit"
    str.mTextControlMode10Entries        = InitializeString("MonoUIInventory_mControlMode10Entries");        //"[IMG0] (Hold) Move 10 Entries"
}
MonoUIJournal_Strings::~MonoUIJournal_Strings()
{
    for(int i = 0; i < MONOUIJOURNAL_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIJournal_Strings::GetSize()
{
    return MONOUIJOURNAL_STRINGS_TOTAL;
}
void MonoUIJournal_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIJOURNAL_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUIJournal::TakeForeground()
{
    AdvUIJournal::TakeForeground();
}
void MonoUIJournal::SetJournalToCombatMode()
{
    //--Clear the list.
    ClearCombatGlossary();

    //--Variables.
    mCurrentMode = MONOMENU_JOURNAL_MODE_COMBATGLOSSARY;

    //--Run this script to set the glossary's properties.
    LuaManager::Fetch()->ExecuteLuaFile(xCombatResolveScript);
}

///======================================= Core Methods ===========================================
void MonoUIJournal::RefreshMenuHelp()
{
    ///--[Call Base Class]
    AdvUIJournal::RefreshMenuHelp();

    ///--[Override Strings]
    //--Main Menu Left Indicator
    mMainMenuLftString->SetString(xrStringData->str.mTextControlLeft);
    mMainMenuLftString->CrossreferenceImages();

    //--Main Menu Right Indicator
    mMainMenuRgtString->SetString(xrStringData->str.mTextControlRight);
    mMainMenuRgtString->CrossreferenceImages();

    //--Show Plantchievements
    mShowAchievementsString->SetString(xrStringData->str.mTextControlShowPlantchievements);
    mShowAchievementsString->CrossreferenceImages();

    //--Hide Plantchievements
    mHideAchievementsString->SetString(xrStringData->str.mTextControlHidePlantchievements);
    mHideAchievementsString->CrossreferenceImages();

    //--Scroll Plantchievements
    mScrollAchievementString->SetString(xrStringData->str.mTextControlScrollWithinCategory);
    mScrollAchievementString->CrossreferenceImages();

    //--Toggle Plantchievement Spoiler
    mToggleSpoilerString->SetString(xrStringData->str.mTextControlToggleSpoilers);
    mToggleSpoilerString->CrossreferenceImages();

    //--Zoom in/out on bestiary entry
    mShowFullPortraitString->SetString(xrStringData->str.mTextControlShowFullPortrait);
    mShowFullPortraitString->CrossreferenceImages();

    //--Toggle Paragon image
    mToggleParagonString->SetString(xrStringData->str.mTextControlToggleParagon);
    mToggleParagonString->CrossreferenceImages();

    //--Enable/Disable Paragon
    mEnableParagonString->SetString(xrStringData->str.mTextControlEnableDisable);
    mEnableParagonString->CrossreferenceImages();

    //--Return to main menu
    mReturnString->SetString(xrStringData->str.mTextControlExit);
    mReturnString->CrossreferenceImages();

    //--Move 10 entries at a time
    mMove10String->SetString(xrStringData->str.mTextControlMode10Entries);
    mMove10String->CrossreferenceImages();
}
void MonoUIJournal::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Determines the highlight position on the left side of the screen. This is done by calling a subroutine
    //  based on the active mode.
    //--Adjusted because there's no Paragons mode in this UI.

    ///--[Execution]
    if(mCurrentMode == MONOMENU_JOURNAL_MODE_ACHIEVEMENTS || mIsAchievementsMode)
    {
        RecomputeJournalAchievementCursorPos();
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_BESTIARY)
    {
        ComputeBestiaryCursorPosition();
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_PROFILES)
    {
        ComputeProfileCursorPosition();
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_QUESTS)
    {
        ComputeQuestCursorPosition();
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_LOCATIONS)
    {
        ComputeLocationCursorPosition();
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_COMBATGLOSSARY)
    {
        ComputeCombatCursorPosition();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIJournal::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Handles timers, controls, and other update components for the UI object.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer,    0, ADVMENU_JOURNAL_VIS_TICKS,                   true);
    StandardTimer(mBestiaryImageTimer, 0, ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS, mShowFullBestiaryImage);

    //--Highlight.
    mHighlightPos. Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Toggle Achievements Mode]
    //--Achievements are part of the main bar in WHI.

    ///--[Help Handler]
    //--Toggle description flag.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--Get, cycle.
        int tComplexDescriptionFlag = rAdventureCombat->IsShowingDetailedDescriptions();
        tComplexDescriptionFlag ++;
        if(tComplexDescriptionFlag > ADVCOMBAT_DESCRIPTIONS_HIDDEN) tComplexDescriptionFlag = ADVCOMBAT_DESCRIPTIONS_SIMPLE;

        //--Set.
        rAdventureCombat->SetDetailedDescriptionFlag(tComplexDescriptionFlag);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Main Journal Menu]
    //--Setup.
    int tStartingMode = mCurrentMode;

    //--This is always active. Pressing left or right changes journal modes.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        mCurrentMode --;
        if(mCurrentMode < 0) mCurrentMode = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    else if(!rControlManager->IsFirstPress("Left") && rControlManager->IsFirstPress("Right"))
    {
        mCurrentMode ++;
        if(mCurrentMode >= MONOMENU_JOURNAL_MODE_TOTAL) mCurrentMode = MONOMENU_JOURNAL_MODE_TOTAL - 1;
        if(mCurrentMode < 0) mCurrentMode = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--If the mode changed for any reason, run this code:
    if(tStartingMode != mCurrentMode)
    {
        //--Reset any outstanding variables.
        HandleJournalModeChange();

        //--Call the requisite mode.
        if(mCurrentMode == MONOMENU_JOURNAL_MODE_BESTIARY)
            SetJournalToBestiaryMode();
        else if(mCurrentMode == MONOMENU_JOURNAL_MODE_PROFILES)
            SetJournalToProfilesMode();
        else if(mCurrentMode == MONOMENU_JOURNAL_MODE_QUESTS)
            SetJournalToQuestsMode();
        else if(mCurrentMode == MONOMENU_JOURNAL_MODE_LOCATIONS)
            SetJournalToLocationsMode();
        else if(mCurrentMode == MONOMENU_JOURNAL_MODE_COMBATGLOSSARY)
            SetJournalToCombatMode();

        //--Achievements handler is done apart from the others to unset the achievements flag
        //  if not in this mode.
        if(mCurrentMode == MONOMENU_JOURNAL_MODE_ACHIEVEMENTS)
        {
            mIsAchievementsMode = true;
            mAchievementCategoryOffset = 0;
            mAchievementCategoryCursor = 0;
            mAchievementEntryOffset = 0;
            mAchievementEntryCursor = 0;
            RecomputeJournalAchievementCursorPos();
        }
        else
        {
            mIsAchievementsMode = false;
        }

        //--Always recompute the cursor.
        RecomputeCursorPositions();
    }

    //--Pressing cancel returns to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--If in bestiary mode and showing the full portrait, cancels that.
        if(mCurrentMode == MONOMENU_JOURNAL_MODE_BESTIARY && mShowFullBestiaryImage)
        {
            mShowFullBestiaryImage = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return true;
        }

        //--Return to main menu.
        HandleJournalModeChange();
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Setup]
    bool tRecompute = false;

    ///--[Bestiary Menu]
    if(mCurrentMode == MONOMENU_JOURNAL_MODE_BESTIARY)
    {
        //--Up and Down handled by standard.
        tRecompute = AutoListUpDn(mBestiaryCursor, mBestiaryOffset, mBestiaryEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);

        //--Activate, toggles on and off showing the detailed portrait.
        if(rControlManager->IsFirstPress("Activate"))
        {
            mShowFullBestiaryImage = !mShowFullBestiaryImage;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Paragons are not handled as they do not exist in WHI.
    }
    ///--[Profile Menu]
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_PROFILES)
    {
        tRecompute = AutoListUpDn(mProfileCursor, mProfileOffset, mProfileEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }
    ///--[Quests]
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_QUESTS)
    {
        tRecompute = AutoListUpDn(mQuestCursor, mQuestOffset, mQuestEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }
    ///--[Locations]
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_LOCATIONS)
    {
        tRecompute = AutoListUpDn(mLocationCursor, mLocationOffset, mLocationEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }
    ///--[Combat Glossary]
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_COMBATGLOSSARY)
    {
        tRecompute = AutoListUpDn(mCombatCursor, mCombatOffset, mCombatEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }
    ///--[Achievements]
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_ACHIEVEMENTS)
    {
        UpdateJournalAchievements();
        return true;
    }

    ///--[Finish Up]
    //--If this flag is true, recompute the highlight and play a sound effect.
    if(tRecompute)
    {
        RecomputeCursorPositions();
    }

    //--Indicate we handled the update.
    return true;
}
void MonoUIJournal::UpdateBackground()
{
    AdvUIJournal::UpdateBackground();
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIJournal::RenderPieces(float pVisAlpha)
{
    ///--[Documentation and Setup]
    if(!mImagesReady) return;

    ///--[Achievements Handling]
    //--Does all of the rendering itself.
    if(mCurrentMode == MONOMENU_JOURNAL_MODE_ACHIEVEMENTS)
    {
        RenderJournalAchievements();
        return;
    }

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, ADVMENU_JOURNAL_VIS_TICKS);
    if(cAlpha <= 0.0f) return;

    ///--[Static Parts]
    //--Common universal parts.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Major Header.
    MonoImages.rOverlayTitleShadow->Draw();
    mMonoTitleColor.SetAsMixerAlpha(cAlpha);
    MonoImages.rFontDoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextJournal);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Main Menu]
    //--Render the menu options. These always render regardless of mode.
    float cHelpLft = 45.0f;
    float cHelpTop = 73.0f;
    float cHelpRgt = 1321.0f;

    //--Help text indicating control options.
    mMainMenuLftString->DrawText(cHelpLft, cHelpTop, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--Unselected tabs.
    for(int i = 0; i < 6; i ++)
    {
        if(mCurrentMode != i)
        {
            MonoImages.rFrameTab->Draw(153.0f * i, 0.0f);
        }
        else
        {
            MonoImages.rFrameTabSelected->Draw(153.0f * i, 0.0f);
        }
    }

    //--Menu options. These render gold when selected.
    float cBestiaryLft    = 176.0f;
    float cProfilesLft    = 330.0f;
    float cQuestsLft      = 496.0f;
    float cLocationsLft   = 627.0f;
    float cCombatLft      = 780.0f;
    float cAchievementLft = 929.0f;
    float cMenuOptionTop  = 84.0f;
    RenderMenuOption(cBestiaryLft,    cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 0, mCurrentMode, xrStringData->str.mTextBestiary);
    RenderMenuOption(cProfilesLft,    cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 1, mCurrentMode, xrStringData->str.mTextProfiles);
    RenderMenuOption(cQuestsLft,      cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 2, mCurrentMode, xrStringData->str.mTextQuests);
    RenderMenuOption(cLocationsLft,   cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 3, mCurrentMode, xrStringData->str.mTextLocations);
    RenderMenuOption(cCombatLft,      cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 4, mCurrentMode, xrStringData->str.mTextCombatGlossary);
    RenderMenuOption(cAchievementLft, cMenuOptionTop, MonoImages.rFontHeading, cAlpha, 5, mCurrentMode, xrStringData->str.mTextAchievements);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Background frames, render over the tabs.
    MonoImages.rFrameLft->Draw();
    MonoImages.rFrameRgt->Draw();

    //--Help options.
    mReturnString->DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    mMove10String->DrawText(0.0f, cxMonoFontMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    ///--[Cat]
    MonoImages.rCatchievements->Draw();

    //--Right side. Right-aligned text.
    mMainMenuRgtString->DrawText(cHelpRgt, cHelpTop, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);

    //--Only shows up in Bestiary mode:
    if(mCurrentMode == ADVMENU_JOURNAL_MODE_BESTIARY)
    {
        mShowFullPortraitString->DrawText(0.0f, cxMonoFontMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFontControl);
    }

    ///--[Submode Rendering]
    //--Render all bestiary entries and the exit option.
    if(mCurrentMode == MONOMENU_JOURNAL_MODE_BESTIARY)
    {
        RenderBestiaryMode(cAlpha);
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_PROFILES)
    {
        RenderProfilesMode(cAlpha);
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_QUESTS)
    {
        RenderQuestsMode(cAlpha);
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_LOCATIONS)
    {
        RenderLocationsMode(cAlpha);
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_COMBATGLOSSARY)
    {
        RenderCombatMode(cAlpha);
    }

    ///--[Highlight]
    //if(!mShowFullBestiaryImage)RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, Images.StandardUI.rHighlight);

    ///--[Clean Up]
    StarlightColor::ClearMixer();
}
void MonoUIJournal::RenderMenuOption(float pX, float pY, StarFont *pFont, float pAlpha, int pSlot, int pMenuSlot, const char *pText)
{
    ///--[Documentation]
    //--Used to render the options at the top of the screen. Selected option uses a highlight color.
    if(!pFont || pAlpha <= 0.0f || !pText) return;

    //--Color check.
    if(pSlot == pMenuSlot)
        glColor4f(0.8f, 0.7f, 0.80f, pAlpha);
    else
        glColor4f(0.5f, 0.3f, 0.32f, pAlpha);

    //--Render.
    pFont->DrawTextFixed(pX, pY, 140.0f, 0, pText);

    //--Clean.
    glColor4f(1.0f, 1.0f, 1.0f, pAlpha);
}
void MonoUIJournal::RenderResistance(float pX, float pY, int pFlags, float pScale, int pResist, StarFont *pFont)
{
    if(!pFont) return;
    if(pResist < 1000)
        pFont->DrawTextArgs(pX, pY, pFlags, pScale, "%i%%", pResist);
    else
        pFont->DrawText(pX, pY, pFlags, pScale, xrStringData->str.mTextImmune);
}

///======================================= Entry Listing ==========================================
void MonoUIJournal::RenderJournalEntryList(StarLinkedList *pList, int pStartAt, int pHighlighted, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Given a list of AdvMenuJournalBasicEntry or derived, renders the selection list on the left side of the screen.
    if(!pList) return;

    //--Font
    StarFont *rUseFont = MonoImages.rFontEntries;

    //--Render Constants. The X/Y position is where the backing renders, the text has an offset.
    float cSelectionLft =  78.0f;
    float cSelectionTop = 168.0f;
    float cSelectionHei =  38.0f;

    //--Image Constants
    float cScale = 0.41f;
    float cScaleInv = 1.0f / cScale;

    //--Variables
    int tOption = 0;
    float tYPos = cSelectionTop;

    ///--[Scrollbar]
    //--If there are over cxScrollbarListPage entries, we need a scrollbar. The scrollbar renders
    //  after the entries but must be computed here.
    bool tIsShowingScrollbar = false;
    if(pList->GetListSize() > cxScrollbarListPage)
    {
        tIsShowingScrollbar = true;
    }

    ///--[Resolve Active Cursor]
    int tActiveCursor = 0;
    if(mCurrentMode == MONOMENU_JOURNAL_MODE_BESTIARY)
    {
        tActiveCursor = mBestiaryCursor;
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_PROFILES)
    {
        tActiveCursor = mProfileCursor;
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_QUESTS)
    {
        tActiveCursor = mQuestCursor;
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_LOCATIONS)
    {
        tActiveCursor = mLocationCursor;
    }
    else if(mCurrentMode == MONOMENU_JOURNAL_MODE_COMBATGLOSSARY)
    {
        tActiveCursor = mCombatCursor;
    }

    ///--[Backings]
    //--A single cycle of backings renders, then the selection indicator, then the entries.
    //  This allows the selection to render under the text which looks better.
    int tRenders = 0;
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)pList->PushIterator();
    while(rEntry)
    {
        //--If we haven't reached the start-at value, don't render anything.
        if(tOption < pStartAt)
        {
            tOption ++;
            rEntry = (AdvMenuJournalBasicEntry *)pList->AutoIterate();
            continue;
        }

        //--Backing.
        MonoImages.rOverlayCell->Draw(cSelectionLft, tYPos);

        //--If we hit the end of the list, stop rendering:
        if(tRenders >= cxScrollbarListPage)
        {
            pList->PopIterator();
            break;
        }

        //--Next.
        tOption ++;
        tYPos = tYPos + cSelectionHei;
        rEntry = (AdvMenuJournalBasicEntry *)pList->AutoIterate();
    }

    ///--[Selection]
    MonoImages.rOverlaySelect->Draw(mHighlightPos.mXCur, mHighlightPos.mYCur);

    ///--[Iteration]
    //--Reset variables.
    tOption = 0;
    tRenders = 0;
    tYPos = cSelectionTop;

    //--Iterate across entries, this time rendering the text.
    rEntry = (AdvMenuJournalBasicEntry *)pList->PushIterator();
    while(rEntry)
    {
        //--If we haven't reached the start-at value, don't render anything.
        if(tOption < pStartAt)
        {
            tOption ++;
            rEntry = (AdvMenuJournalBasicEntry *)pList->AutoIterate();
            continue;
        }

        //--Render.
        tRenders ++;
        if(tOption == tActiveCursor)
            mMonoParagraphColor.SetAsMixerAlpha(pAlpha);
        else
            mMonoSubtitleColor.SetAsMixerAlpha(pAlpha);
        rUseFont->DrawText(cSelectionLft + 21.0f, tYPos, 0, 1.0f, rEntry->mDisplayName);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        //--If there's an icon associated with this entry, render it right-aligned.
        if(rEntry->rIcon)
        {
            //--Right-align position. Moves left if there's a scrollbar.
            float cTopSpot = tYPos + 26.0f - (rEntry->rIcon->GetTrueHeight() * cScale);
            float cLftSpot = 471.0f - (rEntry->rIcon->GetTrueWidth() * cScale * 0.50f);
            if(tIsShowingScrollbar) cLftSpot = cLftSpot - 39.0f;

            //--Position, scale.
            glTranslatef(cLftSpot, cTopSpot, 0.0f);
            glScalef(cScale, cScale, 1.0f);

            //--Render.
            rEntry->rIcon->Draw();

            //--Clean.
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-cLftSpot, -cTopSpot, 0.0f);
        }

        //--If we hit the end of the list, stop rendering:
        if(tRenders >= cxScrollbarListPage)
        {
            pList->PopIterator();
            break;
        }

        //--Next.
        tOption ++;
        tYPos = tYPos + cSelectionHei;
        rEntry = (AdvMenuJournalBasicEntry *)pList->AutoIterate();
    }

    ///--[Scrollbar]
    //--Render scrollbar after entries.
    if(tIsShowingScrollbar)
    {
        StandardRenderScrollbar(pStartAt, cxScrollbarListPage, pList->GetListSize() - cxScrollbarListPage, 162.0f, 537.0, false, MonoImages.rScrollbar_Scroller, MonoImages.rOverlayScrollbarBack);
    }
}


///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
