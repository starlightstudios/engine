///======================================= CarnationPopup ==========================================
//--Represents a popup window in a Carnation level. Contains a title and a set of buttons which can
//  be clicked to fire scripts.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Timers
#define CARNPOP_VIS_TICKS 15

//--Sizes and Padding
#define CARNPOP_HPAD 30.0f
#define CARNPOP_VSIZE 40.0f
#define CARNPOP_TITLEBREAK 10.0f
#define CARNPOP_HEART_OFF_X -20.0f

///========================================== Classes =============================================
class CarnationPopup
{
    private:
    ///--[System]
    bool mIsShowing;
    int mVisTimer;

    ///--[Display]
    char *mTitle;
    int mTitleX;
    int mTitleY;

    ///--[Entries]
    float mWidestEntry;
    StarLinkedList *mEntryList; //CarnExaminable *, master

    protected:

    public:
    //--System
    CarnationPopup();
    ~CarnationPopup();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool IsShowing();
    bool IsVisible();

    //--Manipulators
    void SetVisible(bool pIsVisible);
    void SetTitle(const char *pTitle);
    void SetPosition(int pX, int pY);
    void RegisterExaminable(const char *pInternalName, const char *pDisplayName, const char *pScript, const char *pArgument);

    //--Core Methods
    void RecomputePositions(StarFont *pFont);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();
    CarnExaminable *HandleClick(int pX, int pY);

    //--File I/O
    //--Drawing
    void Render(StarBitmap *pHeart, StarBitmap *pFrame, StarFont *pFont);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


