///=================================== WonderlandPlayerEvent ======================================
//--Event that commands the active WonderlandPlayer to do something.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootEvent.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Types
#define WLPE_TYPE_NONE 0
#define WLPE_TYPE_EMULATEKEYPRESS 1

//--Keycodes
#define WLPE_EMULATE_KEYPRESS_CODE_LEFT 0
#define WLPE_EMULATE_KEYPRESS_CODE_RIGHT 1
#define WLPE_EMULATE_KEYPRESS_CODE_JUMP 2
#define WLPE_EMULATE_KEYPRESS_CODE_SHOOT 3

///========================================== Classes =============================================
class WonderlandPlayerEvent : public RootEvent
{
    private:

    protected:
    //--System
    bool mIsComplete;
    int mInstructionType;

    //--Target
    uint32_t mTargetID;

    //--Data Storage
    union
    {
        struct
        {
            int mKeycode;
            int mTicks;
        }EmulateKeypress;
    }Data;

    public:
    //--System
    WonderlandPlayerEvent();
    virtual ~WonderlandPlayerEvent();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    uint32_t GetSubjectID();

    //--Manipulators
    void SetTarget(uint32_t pID);
    void SetTargetByName(const char *pName);
    void SetEmulateKeypress(int pKeycode, int pTicks);

    //--Core Methods
    virtual bool IsComplete();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_WonderlandPlayerEvent_SetProperty(lua_State *L);
