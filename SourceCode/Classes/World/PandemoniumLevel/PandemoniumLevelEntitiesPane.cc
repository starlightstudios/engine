//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "InventoryItem.h"
#include "InventorySubClass.h"
#include "PandemoniumRoom.h"
#include "RootEntity.h"
#include "RootEffect.h"
#include "VisualLevel.h"
#include "WorldContainer.h"

//--CoreClasses
#include "DataList.h"
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"
#include "OptionsManager.h"

void PandemoniumLevel::RenderEntitiesPane()
{
    //--Entities pane, featured in the bottom right corner. the constants are setup in the CharacterPane's
    //  code file.
    mMaxEntities = -1;

    //--Position setup.
    glTranslatef(EntityPane.cLft, EntityPane.cTop, 0.0f);

    //--Fast-access. Keeps things aligned.
    float cWid = EntityPane.cWid;
    float cHei = EntityPane.cHei;
    float cInd = EntityPane.cIndent;

    //--[Border]
    //--Subroutine handles it.
    RenderBorder(0.0f, 0.0f, cWid, cHei, cInd, cBorderColor, cInnerColor);

    //--[Contents]
    //--Get the acting entity. This will tell use the acting room, and which entity to ignore.
    //  If there's no acting entity, we don't need to render anything.
    Actor *rActingEntity = EntityManager::Fetch()->GetLastPlayerEntity();

    //--Scrollbar counter.
    int tDontRenderBelow = mEntityScrollOffset;
    mTotalValidEntities = 0; //Goes up independent of actual rendering. Used for scrollbars.

    //--Option Flags
    OptionsManager *rOptionManager = OptionsManager::Fetch();
    bool tShowHP     = rOptionManager->GetOptionB("Show Entity HP");
    bool tShowWP     = rOptionManager->GetOptionB("Show Entity WP");
    bool tShowWeapon = rOptionManager->GetOptionB("Show Entity Weapon");
    bool tShowIcons  = rOptionManager->GetOptionB("Show Entity Icons");

    //--Display a list of all entities in the room other than the currently acting one. Duh.
    if(rActingEntity)
    {
        //--Get the room it's standing in. Once again, no room, no rendering.
        PandemoniumRoom *rActiveRoom = rActingEntity->GetCurrentRoom();
        if(rActiveRoom)
        {
            //--Setup.
            float cStepRate = cFontSpacing + 2.0f;
            float tRenderCursor = (EntityPane.cIndent * 2.0f);

            //--Render using the text color.
            cTextColor.SetAsMixer();

            //--Render the name of the room.
            Images.Data.rUIFontMedium->DrawText(EntityPane.cIndent * 7.0f, tRenderCursor, 0, cFontSize, rActiveRoom->GetName());
            tRenderCursor = tRenderCursor + cStepRate;

            //--Get a list of all entities in the room. Render their names.
            int tCount = 0;
            StarLinkedList *rEntityList = rActiveRoom->GetEntityList();
            Actor *rActor = (Actor *)rEntityList->PushIterator();
            while(rActor)
            {
                //--Flag. If the Actor has a specific name, we don't render HP/WP/Weapon, since these ostensibly don't have them.
                bool tIgnoreStats = false;
                const char *rName = rActor->GetName();
                if(!strcasecmp( rName, "Harpy Nest")      || !strncasecmp(rName, "Golem Tube", 10) || !strcasecmp( rName, "Silver Mirror") ||
                   !strncasecmp(rName, "Warp Beacon", 11) || !strncasecmp(rName, "Pod ",        4) || !strncasecmp(rName, "Glyph ", 5) ||
                   !strncasecmp(rName, "Rune ",        5) || !strncasecmp(rName, "[",           1) || !strncasecmp(rName, "Quicksand", 9) ||
                   !strncasecmp(rName, "Sapper", 6))
                {
                    tIgnoreStats = true;
                }

                //--If the actor is NOT the acting one, render its name. Invisible actors don't appear (duh).
                if(rActor != rActingEntity && !rActor->IsObscured())
                {
                    //--Block rendering if the entity is below the scroll bar count.
                    if(tDontRenderBelow > 0)
                    {
                        mTotalValidEntities ++;
                        if(tDontRenderBelow < 1000) tDontRenderBelow --;
                    }
                    //--Render the entity data.
                    else
                    {
                        //--Left-edge.
                        float tLftEdge = EntityPane.cIndent * 4.5f;

                        //--Render the associated icon.
                        if(tShowIcons)
                        {
                            //--Move the rest of the display over slightly.
                            tLftEdge = tLftEdge + (EntityPane.cIndent * 4.0f);

                            //--Positioning.
                            float tIconX = EntityPane.cIndent * 3.5f;
                            float tIconY = tRenderCursor + (cFontSpacing * 0.5f);

                            //--Coloring.
                            rActor->GetLocalColor().SetAsMixer();

                            //--Humans:
                            if(rActor->GetRenderFlag() == RENDER_HUMAN)
                            {
                                PandemoniumRoom::RenderHumanSymbol(tIconX, tIconY + 5.0f);
                            }
                            //--Monsters:
                            else if(rActor->GetRenderFlag() == RENDER_MONSTER)
                            {
                                PandemoniumRoom::RenderMonsterSymbol(tIconX, tIconY);
                            }
                            //--Golem tube.
                            else if(rActor->GetRenderFlag() == RENDER_TUBE)
                            {
                                PandemoniumRoom::RenderTubeSymbol(tIconX, tIconY);
                            }
                            //--Rilmani mirror.
                            else if(rActor->GetRenderFlag() == RENDER_MIRROR)
                            {
                                PandemoniumRoom::RenderMirrorSymbol(tIconX, tIconY);
                            }
                            //--Harpy nest.
                            else if(rActor->GetRenderFlag() == RENDER_NEST)
                            {
                                PandemoniumRoom::RenderNestSymbol(tIconX, tIconY);
                            }
                            //--Vampire Coffin.
                            else if(rActor->GetRenderFlag() == RENDER_COFFIN)
                            {
                                PandemoniumRoom::RenderCoffinSymbol(tIconX, tIconY);
                            }
                            //--Glyph of power. Smaller than the rest, renders with a scale-down.
                            else if(rActor->GetRenderFlag() == RENDER_GLYPH)
                            {
                                float cScale = 0.45f;
                                glTranslatef(tIconX, tIconY, 0.0f);
                                glScalef(cScale, cScale, 1.0f);
                                PandemoniumRoom::RenderGlyphSymbol(0, -10.0f);
                                glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
                                glTranslatef(-tIconX, -tIconY, 0.0f);
                            }

                            //--Clean.
                            cTextColor.SetAsMixer();
                        }

                        //--Coloring.
                        if(mSelectedEntity == tCount)
                        {
                            cSelectColor.SetAsMixer();
                        }
                        //--Normal case.
                        else
                        {
                            cTextColor.SetAsMixer();
                        }

                        //--Name.
                        Images.Data.rUIFontMedium->DrawText(tLftEdge, tRenderCursor, 0, cFontSize, rActor->GetName());
                        tRenderCursor = tRenderCursor + 3;
                        cTextColor.SetAsMixer();

                        //--Get the slime infection pointer. Recompute the scale of the weapon display.
                        int tSlimeTurns = 0;
                        float tWeaponScale = 1.0f;
                        void *rSlimeTFPtr = rActor->GetDataList()->FetchDataEntry("iSlimeTFTurns");
                        if(rSlimeTFPtr && rActor->GetTeam() == TEAM_PLAYER)
                        {
                            //--Get how many turns they have.
                            float *rSlimeTFTurnsPtr = (float *)rSlimeTFPtr;
                            tSlimeTurns = (int)(*rSlimeTFTurnsPtr);

                            //--If the target has a weapon, we need to modify the rendering scale so they both fit.
                            if(tSlimeTurns > 0 && rActor->GetWeapon())
                            {
                                tWeaponScale = VisualLevel::ResolveLargestScale(Images.Data.rUIFontSmall, (EntityPane.cIndent * 94.0f) - (EntityPane.cIndent * 72.0f), rActor->GetWeapon()->GetName());
                            }
                        }

                        //--HP, if flagged.
                        if(tShowHP && !tIgnoreStats)
                        {
                            StarFont::xScaleAffectsY = false;
                            Images.Data.rUIFontSmall->DrawTextArgs(tLftEdge + (EntityPane.cIndent * 36.0f), tRenderCursor, 0, cFontSize, "%i/%i", rActor->GetCombatStatistics().mHP, rActor->GetCombatStatistics().mHPMax);
                            StarFont::xScaleAffectsY = true;
                        }
                        //--WP, if flagged.
                        if(tShowWP && !tIgnoreStats)
                        {
                            StarFont::xScaleAffectsY = false;
                            Images.Data.rUIFontSmall->DrawTextArgs(tLftEdge + (EntityPane.cIndent * 56.0f), tRenderCursor, 0, cFontSize, "%i/%i", rActor->GetCombatStatistics().mWillPower, rActor->GetCombatStatistics().mWillPowerMax);
                            StarFont::xScaleAffectsY = true;
                        }
                        //--Weapon, if flagged.
                        if(tShowWeapon && rActor->GetWeapon() && !tIgnoreStats)
                        {
                            StarFont::xScaleAffectsY = false;
                            Images.Data.rUIFontSmall->DrawTextArgs(tLftEdge + (EntityPane.cIndent * 72.0f), tRenderCursor, 0, cFontSize * tWeaponScale, "(%s)", rActor->GetWeapon()->GetName());
                            StarFont::xScaleAffectsY = true;
                        }

                        //--Infection!
                        if(rSlimeTFPtr && rActor->GetTeam() == TEAM_PLAYER && tSlimeTurns)
                        {
                            StarFont::xScaleAffectsY = false;
                            Images.Data.rUIFontSmall->DrawTextArgs(tLftEdge + (EntityPane.cIndent * 96.0f), tRenderCursor, 0, cFontSize * 0.85f, "(Inf: %i)", 30 - tSlimeTurns);
                            StarFont::xScaleAffectsY = true;
                        }

                        //--Move the render cursor down.
                        tRenderCursor = tRenderCursor - 3;
                        tRenderCursor = tRenderCursor + cStepRate;

                        //--Edge counter.
                        tCount ++;
                        mMaxEntities = tCount-1;
                        mTotalValidEntities ++;

                        //--Cease rendering if the count goes over the edge.
                        if(tCount > ENTITIES_DISPLAY_PER_GROUP)
                        {
                            tDontRenderBelow = 1000;
                        }
                    }
                }

                //--Next.
                rActor = (Actor *)rEntityList->AutoIterate();
            }

            //--Get a list of all items in the room, and render their names. Same as above, also skips rendering based on the scroll bar.
            for(int i = 0; i < rActiveRoom->GetCompressedInventorySize(); i ++)
            {
                //--Get the item in the slot.
                InventoryPack *rItemPack = rActiveRoom->GetItemPack(i);
                if(!rItemPack || !rItemPack->rItem) continue;

                //--Block rendering if the entity is below the scroll bar count.
                if(tDontRenderBelow > 0)
                {
                    mTotalValidEntities ++;
                    if(tDontRenderBelow < 1000) tDontRenderBelow --;
                }
                //--Render.
                else
                {
                    //--Coloring.
                    if(mSelectedEntity == tCount)
                    {
                        cSelectColor.SetAsMixer();
                    }
                    //--Normal case.
                    else
                    {
                        cTextColor.SetAsMixer();
                    }

                    //--Text.
                    Images.Data.rUIFontMedium->DrawTextArgs(EntityPane.cIndent * 4.5f, tRenderCursor, 0, cFontSize, "%s x%i", rItemPack->rItem->GetName(), rItemPack->mQuantity);
                    tRenderCursor = tRenderCursor + cStepRate;
                    cTextColor.SetAsMixer();

                    //--Flags.
                    mTotalValidEntities ++;
                    tCount ++;
                    mMaxEntities = tCount-1;

                    //--Cease rendering if the count goes over the edge.
                    if(tCount > ENTITIES_DISPLAY_PER_GROUP)
                    {
                        tDontRenderBelow = 1000;
                    }
                }
            }

            //--Get a list of all containers. Render those.
            StarLinkedList *rContainerList = rActiveRoom->GetContainerList();
            WorldContainer *rContainer = (WorldContainer *)rContainerList->PushIterator();
            while(rContainer)
            {
                //--Block rendering if the entity is below the scroll bar count.
                if(tDontRenderBelow > 0)
                {
                    mTotalValidEntities ++;
                    if(tDontRenderBelow < 1000) tDontRenderBelow --;
                }
                //--Render.
                else
                {
                    //--Coloring.
                    if(mSelectedEntity == tCount)
                    {
                        cSelectColor.SetAsMixer();
                    }
                    //--Normal case.
                    else
                    {
                        cTextColor.SetAsMixer();
                    }

                    //--Text.
                    Images.Data.rUIFontMedium->DrawText(EntityPane.cIndent * 4.5f, tRenderCursor, 0, cFontSize, rContainer->GetName());
                    tRenderCursor = tRenderCursor + cStepRate;
                    cTextColor.SetAsMixer();

                    //--Flags.
                    mTotalValidEntities ++;
                    tCount ++;
                    mMaxEntities = tCount-1;

                    //--Cease rendering if the count goes over the edge.
                    if(tCount > ENTITIES_DISPLAY_PER_GROUP)
                    {
                        tDontRenderBelow = 1000;
                    }
                }
                rContainer = (WorldContainer *)rContainerList->AutoIterate();
            }

            //--Get a list of all Effects. Render those (cripes).
            StarLinkedList *rEffectList = rActiveRoom->GetEffectList();
            RootEffect *rEffect = (RootEffect *)rEffectList->PushIterator();
            while(rEffect)
            {
                //--Block rendering if the entity is below the scroll bar count.
                if(tDontRenderBelow > 0)
                {
                    mTotalValidEntities ++;
                    if(tDontRenderBelow < 1000) tDontRenderBelow --;
                }
                //--Render.
                else
                {
                    //--Coloring.
                    if(mSelectedEntity == tCount)
                    {
                        cSelectColor.SetAsMixer();
                    }
                    //--Normal case.
                    else
                    {
                        cTextColor.SetAsMixer();
                    }

                    //--Text.
                    Images.Data.rUIFontSmall->DrawText(EntityPane.cIndent * 4.5f, tRenderCursor, 0, cFontSize, rEffect->GetDisplayName());
                    tRenderCursor = tRenderCursor + cStepRate;
                    cTextColor.SetAsMixer();

                    //--Flags.
                    mTotalValidEntities ++;
                    tCount ++;
                    mMaxEntities = tCount-1;

                    //--Cease rendering if the count goes over the edge.
                    if(tCount > ENTITIES_DISPLAY_PER_GROUP)
                    {
                        tDontRenderBelow = 1000;
                    }
                }

                //--Next.
                rEffect = (RootEffect *)rEffectList->AutoIterate();
            }
        }
    }

    //--Scroll Buttons/Bar. Only renders if there are enough entities present.
    glColor3f(1.0f, 1.0f, 1.0f);
    if(tDontRenderBelow > 100 || true)
    {
        //--Position values.
        float cLft = cWid - EntityPane.cIndent - Images.Data.rScrollBtnDn->GetWidth();
        float cTop = EntityPane.cIndent;
        float cRgt = cWid - EntityPane.cIndent;
        float cBot = cHei - EntityPane.cIndent - Images.Data.rScrollBtnDn->GetHeight();

        //--Top scrolling button.
        Images.Data.rScrollBtnDn->Draw(cLft, cTop, SUGAR_FLIP_VERTICAL);

        //--Bottom scrolling button.
        Images.Data.rScrollBtnDn->Draw(cLft, cBot, 0);

        //--Move the top constant down for faster rendering.
        cTop = cTop + Images.Data.rScrollBtnDn->GetHeight();

        //--Draw the vertical bars.
        glColor3f(0.0f, 0.0f, 0.0f);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);

            //--Left bar.
            glVertex2f(cLft + 0.0f, cTop + 0.0f);
            glVertex2f(cLft + 2.0f, cTop + 0.0f);
            glVertex2f(cLft + 2.0f, cBot + 0.0f);
            glVertex2f(cLft + 0.0f, cBot + 0.0f);

            //--Right bar.
            glVertex2f(cRgt + 0.0f, cTop + 0.0f);
            glVertex2f(cRgt - 2.0f, cTop + 0.0f);
            glVertex2f(cRgt - 2.0f, cBot + 0.0f);
            glVertex2f(cRgt + 0.0f, cBot + 0.0f);

            //--Draw the scroll bar. Doesn't always render.
            if(mTotalValidEntities > 0)
            {
                //--Compute percentage.
                float cTopPercent = (float) mEntityScrollOffset                               / (float)mTotalValidEntities;
                float cBotPercent = (float)(mEntityScrollOffset+ENTITIES_DISPLAY_PER_GROUP+1) / (float)mTotalValidEntities;
                if(cBotPercent >= 1.0f) cBotPercent = 1.0f;

                //--Render outer.
                glColor3f(0.1f, 0.1f, 0.1f);
                glVertex2f(cLft + 2.0f, cTop + (cTopPercent * (cBot - cTop)));
                glVertex2f(cRgt - 2.0f, cTop + (cTopPercent * (cBot - cTop)));
                glVertex2f(cRgt - 2.0f, cTop + (cBotPercent * (cBot - cTop)));
                glVertex2f(cLft + 2.0f, cTop + (cBotPercent * (cBot - cTop)));

                //--Render inner.
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex2f(cLft + 4.0f, cTop + (cTopPercent * (cBot - cTop)) + 2.0f);
                glVertex2f(cRgt - 4.0f, cTop + (cTopPercent * (cBot - cTop)) + 2.0f);
                glVertex2f(cRgt - 4.0f, cTop + (cBotPercent * (cBot - cTop)) - 2.0f);
                glVertex2f(cLft + 4.0f, cTop + (cBotPercent * (cBot - cTop)) - 2.0f);
            }

        //--Clean.
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }

    //--Final clean up.
    glTranslatef(-EntityPane.cLft, -EntityPane.cTop, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
}
