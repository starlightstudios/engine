-- |[ =================================== ModInfo Class File ==================================== ]|
--Class file for ModInfo. Call this once at initialization.

--This stores mod information in a local lua state. It is only sent to the C++ state for creating
-- a mod load-order UI. Otherwise, mod info is lua-only.

-- |[ =========== Globals/Statics ============ ]|
--Execution constants for Mod Scripts
ModInfo = {}
ModInfo.ciReportBasicData = 0
ModInfo.ciNewGameStep = 1
ModInfo.ciLoadGameStep = 2

-- |[ ============ Class Members ============= ]|
-- |[System]|
ModInfo.__index = ModInfo
ModInfo.bIsActive    = true   --If false, the mod is known but will not run. Used for load orders.
ModInfo.sLocalName   = "Null" --Internal name of the mod
ModInfo.sDisplayName = "Null" --Display name of the mod, for mod load order
ModInfo.sPath        = "Null" --Path to the base folder of the mod

-- |[ ============ Class Methods ============= ]|
--System
function ModInfo:new(psName) end

--Properties
function ModInfo:fnSetDisplayName(psName) end
function ModInfo:fnSetPath(psPath)        end

--System
function ModInfo:fnScanModsInto(psFolderPath)         end
function ModInfo:fnTrimModsByFile(psLoadOrderFile)    end
function ModInfo:fnRunModsAtStep(piStep, pzaModArray) end

-- |[ ========== Class Constructor =========== ]|
--Must have a unique name for the map in question.
function ModInfo:new(psName)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, ModInfo)
    
    -- |[ ======== Members ========= ]|
    --Local member.
    zObject.sLocalName = psName
    
    --Display name matches the local name. This can be changed.
    zObject.sDisplayName = psName
    
    -- |[ ======= Finish Up ======== ]|
    --Return object.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "ModInfoProperties.lua")
LM_ExecuteScript(fnResolvePath() .. "ModInfoSystem.lua")