//--Base
#include "CarnUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "ControlManager.h"

///========================================== Update ==============================================
void CarnUIVendor::UpdateModeSell()
{
    ///--[ ========== Documentation =========== ]
    //--Update handler for Sell mode. This is reimplemented from the base version to use the mouse.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Fast-access pointers.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();

    ///--[ ======= Timers and Submodes ======== ]
    ///--[Timers]
    //--Run standard timers.
    StandardTimer(mSellTimer,       0, cxAdvSellTicks,     mIsSellingItem);

    //--Update blockers. If these timers are not capped, stop the update.
    if(mSellTimer    > 0 && mSellTimer    < cxAdvSellTicks)    return;
    if(mDetailsTimer > 0 && mDetailsTimer < cxAdvDetailsTicks) return;

    ///--[Mode Handlers]
    //--Confirming sale.
    if(mIsSellingItem) { UpdateSellItem(); return; }

    //--If in Details mode, pressing cancel exits details mode. All other controls are ignored.
    if(CommonDetailsUpdate()) return;

    ///--[ ========== Mouse Controls ========== ]
    ///--[Scrollbar]
    //--If the inventory size changes, recompute the scrollbar size here.
    if(mRecomputeScrollbarSize)
    {
        ScrollbarPack *rScrollbarPack = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
        if(rScrollbarPack) rScrollbarPack->mMaxSkip = rItemList->GetListSize() - cxCarnScrollPageSize;
    }

    ///--[Mouse Handlers]
    //--Update mouse info.
    GetMouseInfo();

    //--Scrollbars update before anything else.
    if(UpdateScrollbars()) return;

    ///--[Mouse Over]
    //--If the mouse moves, recheck hitboxes.
    if(mMouseMoved)
    {
        ///--[Selection Mouse Over]
        //--Check the item hitbox. If it changed, play a sound effect.
        int tOldCursor = mCursor;
        RunHitboxCheck();

        //--Iterate across the reply packs.
        int tReply = CheckRepliesWithin(cxHitbox_ItemsLo, cxHitbox_ItemsHi);
        if(tReply != -1) mCursor = tReply + mSkip;

        //--Range check, can't pick a blank item.
        if(mCursor >= rItemList->GetListSize()) mCursor = tOldCursor;

        //--If the class cursor changed, play a sound effect.
        if(tOldCursor != mCursor && mCursor != -1)
        {
            //--Description.
            AdventureItem *rItem = GetConsideredItem();
            RebuildDescriptionList(rItem);

            //--Adjust comparison item.
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);
            RecheckComparisonSlot(rItem, rActiveEntity);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Left Click]
    //--Player left clicks. Switches modes, selects items, selects comparison character.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Iterate across the reply packs.
        RunHitboxCheck();
        int tReply = CheckRepliesWithin(cxHitbox_ItemsLo, cxHitbox_Exit);
        if(tReply == -1) return;

        //--Selecting an item:
        if(tReply >= cxHitbox_ItemsLo && tReply <= cxHitbox_ItemsHi)
        {
            //--Check the item.
            AdventureItem *rSellingItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);

            //--If the item is a key-item, don't allow them to sell it.
            if(rSellingItem && !rSellingItem->IsKeyItem())
            {
                mIsSellingItem = true;
                mSellQuantity = 1;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Otherwise, play a sound effect to indicate failure.
            else if(rSellingItem && rSellingItem->IsKeyItem())
            {
                RollQuote("CantSell", 0, false);
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            return;
        }
        //--Changing shop modes:
        else if(tReply >= cxHitbox_Buy && tReply <= cxHitbox_Sell)
        {
            //--Only do something on the buy button.
            if(tReply == cxHitbox_Buy) ScrollMode(1);
            return;
        }
        //--Changing comparison character:
        else if(tReply >= cxHitbox_CharLo && tReply <= cxHitbox_CharHi)
        {
            SetComparisonCharacter(tReply - cxHitbox_CharLo);
            return;
        }
        //--Exit.
        else if(tReply == cxHitbox_Exit)
        {
            HandleExit();
            return;
        }

        //--Stop update.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Exits the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        HandleExit();
        return;
    }
}
void CarnUIVendor::UpdateSellItem()
{
    ///--[Documentation]
    //--Handles the confirmation window, allowing the user to confirm or cancel a sale. Same as
    //  the base version but updated to allow mouse usage.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetItemList();
    AdventureItem *rSoldItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);

    ///--[Mouse Handlers]
    //--Update mouse info.
    GetMouseInfo();

    ///--[Left Click / Buttons]
    //--Player left clicks. Check buttons.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Iterate across the reply packs.
        RunHitboxCheck();
        int tReply = CheckRepliesWithin(cxHitbox_Confirm, cxHitbox_Cancel);
        if(tReply == -1) return;

        //--If it's cancel, stop here.
        if(tReply == cxHitbox_Cancel)
        {
            mIsSellingItem = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--If it's confirm, purchase the item.
        if(tReply == cxHitbox_Confirm)
        {
            //--Flag.
            mIsSellingItem = false;

            //--Get the buyback list.
            StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();

            //--Buyback value.
            int tBuybackValue = rSoldItem->GetValue();

            //--Selling the entire stack:
            int tStackSize = rSoldItem->GetStackSize();
            if(mSellQuantity >= tStackSize || !rSoldItem->IsStackable())
            {
                //--Remove the item from the player's inventory.
                rInventory->LiberateItemP(rSoldItem);

                //--Move the item to a buyback list for the amount it was sold for.
                SetMemoryData(__FILE__, __LINE__);
                ShopInventoryPack *nPackage = (ShopInventoryPack *)starmemoryalloc(sizeof(ShopInventoryPack));
                nPackage->Initialize();
                nPackage->mItem = rSoldItem;
                nPackage->mPriceOverride = tBuybackValue;
                nPackage->mQuantity = mSellQuantity;
                rBuybackList->AddElementAsHead("X", nPackage, &ShopInventoryPack::DeleteThis);
            }
            //--Selling less than a full stack.
            else
            {
                //--Decrement stack size.
                rSoldItem->SetStackSize(tStackSize - mSellQuantity);

                //--Create a clone and put it on the buyback list.
                AdventureItem *nClonedItem = rSoldItem->Clone();

                //--Create a package to go around it.
                SetMemoryData(__FILE__, __LINE__);
                ShopInventoryPack *nPackage = (ShopInventoryPack *)starmemoryalloc(sizeof(ShopInventoryPack));
                nPackage->Initialize();
                nPackage->mItem = nClonedItem;
                nPackage->mPriceOverride = tBuybackValue;
                nPackage->mQuantity = mSellQuantity;
                rBuybackList->AddElementAsHead("X", nPackage, &ShopInventoryPack::DeleteThis);
            }

            //--When selling, value is NOT halved in Carnation.
            int tValue = rSoldItem->GetValue() * mSellQuantity;

            //--Increment the player's cash.
            rInventory->SetPlatina(rInventory->GetPlatina() + tValue);

            //--If there are more than AINV_MAX_BUYBACK elements on the buyback list, clean off the end.
            while(rBuybackList->GetListSize() >= AINV_MAX_BUYBACK)
            {
                rBuybackList->DeleteTail();
            }

            //--If the cursor was at the end of the list, decrement.
            if(mCursor >= rItemList->GetListSize()) mCursor --;
            if(mCursor < 0) mCursor = 0;

            //--Recompute cursor properties.
            RecomputeCursorPositions();
            mHighlightPos.Complete();
            mHighlightSize.Complete();

            //--If there is at least one entry on the list, recheck comparison character.
            AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);
            RecheckComparisonCharacter(rItem);

            //--Quote.
            RollQuote("Sell", 15, false);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
            return;
        }
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Cancels purchase.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        mIsSellingItem = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///======================================= Dummy Render ===========================================
//--The Carnation UI renders all pieces in the unaligned section. These functions disable all other rendering.
void CarnUIVendor::RenderLftSell(float pVisAlpha) { }
void CarnUIVendor::RenderTopSell(float pVisAlpha) { }
void CarnUIVendor::RenderRgtSell(float pVisAlpha) { }
void CarnUIVendor::RenderBotSell(float pVisAlpha) { }

///====================================== Main Rendering ==========================================
void CarnUIVendor::RenderUnalignedSell(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders buy mode, which is mostly the same as sell mode except which inventory is queried changes.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    float cHeightPerEntry =  23.0f;

    ///--[Common]
    //--Static pieces.
    CarnImages.rBackground_Fluffs->Draw();
    CarnImages.rBackground_Clouds->Draw();
    CarnImages.rBackground_Shelf->Draw();
    CarnImages.rBackground_Covers->Draw();
    RenderFluffs();
    CarnImages.rBackground_Desk->Draw();
    CarnImages.rFrame_BoxRgt->Draw();
    CarnImages.rFrame_BoxBot->Draw();
    CarnImages.rFrame_Hexes->Draw();

    ///--[Labels]
    //--Buy and Sell labels
    CarnImages.rFont_BuySell->DrawTextArgs(208.0f, 30.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
    CarnImages.rFont_BuySell->DrawTextArgs(400.0f, 30.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");

    //--Hexes.
    CarnImages.rFont_Hexes->DrawTextArgs(1212.0f, 26.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", AdventureInventory::Fetch()->GetPlatina());

    ///--[Sellable Items]
    //--If a scrollbar is needed, render it.
    if(rItemList->GetListSize() > cxCarnScrollPageSize)
    {
        StandardRenderScrollbar(mSkip, cxCarnScrollPageSize, rItemList->GetListSize() - cxCarnScrollPageSize, 159.0f, 192.0f, false, CarnImages.rScrollbar_Scroller, CarnImages.rScrollbar_Static);
    }

    //--Render entries.
    RenderInventoryItemListing(mSkip, cScrollPage, 1.0f);

    ///--[Cursor]
    int tPosOnScreen = mCursor - mSkip;
    if(tPosOnScreen > -1 && mCursor < rItemList->GetListSize()) CarnImages.rOverlay_Cursor->Draw(0.0f, tPosOnScreen * cHeightPerEntry);

    ///--[Highlighted Item Description]
    //--If there is a highlighted item, render its description.
    AdventureItem *rDescriptionItem = GetConsideredItem();
    if(mDescriptionList && rDescriptionItem)
    {
        //--Position.
        float cXPosition = 87.0f;
        float cYPosition = 587.0f;
        float cLineHei = CarnImages.rFont_Description->GetTextHeight();

        //--Name.
        CarnImages.rFont_Name->DrawText(cXPosition, 536.0f, 0, 1.0f, rDescriptionItem->GetName());

        //--Render.
        char *rString = (char *)mDescriptionList->PushIterator();
        while(rString)
        {
            //--Render.
            CarnImages.rFont_Description->DrawText(cXPosition, cYPosition, 0, 1.0f, rString);

            //--Next.
            cYPosition = cYPosition + cLineHei;
            rString = (char *)mDescriptionList->AutoIterate();
        }
    }

    ///--[Exit Button]
    TwoDimensionReal *rExitHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Exit");
    if(rExitHitbox)
    {
        CarnImages.rFrame_Button->Draw(rExitHitbox->mLft, rExitHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rExitHitbox->mXCenter, rExitHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Exit");
    }

    ///--[Highlighted Item Properties]
    //--If the highlighted item is a piece of equipment, shows its stats and comparison values.
    if(rDescriptionItem) RenderComparisonPane(rDescriptionItem, 1.0f);

    ///--[Overlays]
    //--Sell overlay. Does nothing if the timer is zeroed.
    RenderSellItem(pVisAlpha);

    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}

///=================================== Subroutine Rendering =======================================
void CarnUIVendor::RenderItem(AdventureItem *pItem, int pSlot, bool pIsOdd, bool pIsSellPrice, float pColorAlpha)
{
    ///--[Documentation]
    //--For buy/sell, renders the information of an item in a standard format. This only shows the icon
    //  at the moment, but the positioning is different from the base version.
    if(!pItem) return;

    //--Pointers.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();

    //--Resolve position.
    float cIconX  = 128.0f;
    float cNameX  = 154.0f;
    float cPriceR = 482.0f;
    float cStartY = 104.0f;
    float cHeight =  23.0f;
    float cRenderY = cStartY + (pSlot * cHeight);

    ///--[Render]
    //--Icon.
    StarBitmap *rImage = pItem->GetIconImage();
    if(rImage) rImage->Draw(cIconX, cRenderY+4);

    //--Left-aligned name. Needs to have modifications in its slot represented.
    char *tRenderName = pItem->GetDisplayNameOfItemWithModCount();
    if(tRenderName)
    {
        CarnImages.rFont_Description->DrawText(cNameX, cRenderY, 0, 1.0f, tRenderName);
        free(tRenderName);
    }

    //--Right-aligned price. Carnation does not halve sell values.
    int tValue = pItem->GetValue();
    if(pIsSellPrice) tValue = tValue;

    //--Enough items, move the price over.
    if(rItemList->GetListSize() > cxCarnScrollPageSize) cPriceR = 449.0f;

    CarnImages.rFont_Description->DrawTextArgs(cPriceR, cRenderY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
}
void CarnUIVendor::RenderSellItem(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--When the player chooses an item to sell, this UI appears over the rest of the UI. It shows
    //  what they are selling and for how much.
    //--Carnation UI always uses fades.
    float cColorAlpha = EasingFunction::QuadraticInOut(mSellTimer, cxAdvSellTicks) * pVisAlpha;
    if(mSellTimer < 1) return;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cColorAlpha * 0.7f));

    ///--[Item Resolve]
    //--Get the item package.
    AdventureItem *rItem = GetConsideredItem();

    ///--[Rendering]
    //--Frame.
    SetColor("Clear", cColorAlpha);
    CarnImages.rFrame_Confirm->Draw();

    //--All information specific to the item is not shown when hiding the UI.
    if(rItem && mIsSellingItem)
    {
        //--Heading.
        SetColor("Heading", cColorAlpha);
        CarnImages.rFont_Name->DrawText(VIRTUAL_CANVAS_X * 0.50f, 218.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Sale");
        SetColor("Clear", cColorAlpha);

        //--Centering computations.
        int tTotalItemValue = rItem->GetValue() * mSellQuantity;
        char *tTextBuf = InitializeString("%s x %i for %i Hexes", rItem->GetName(), mSellQuantity, tTotalItemValue);
        float cTextWid = CarnImages.rFont_Description->GetTextWidth(tTextBuf);
        float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

        //--Item name.
        CarnImages.rFont_Description->DrawTextArgs(cTextLft, 265.0f, 0, 1.0f, tTextBuf);
        free(tTextBuf);
    }

    //--Buttons.
    TwoDimensionReal *rConfirmHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Confirm");
    if(rConfirmHitbox)
    {
        CarnImages.rFrame_Button->Draw(rConfirmHitbox->mLft, rConfirmHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rConfirmHitbox->mXCenter, rConfirmHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Confirm");
    }
    TwoDimensionReal *rCancelHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("Cancel");
    if(rCancelHitbox)
    {
        CarnImages.rFrame_Button->Draw(rCancelHitbox->mLft, rCancelHitbox->mTop);
        CarnImages.rFont_Description->DrawText(rCancelHitbox->mXCenter, rCancelHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Cancel");
    }

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);
    mCancelPurchaseString-> DrawText(579.0f, 414.0f, SUGARFONT_NOCOLOR, 1.0f, CarnImages.rFont_Description);

    ///--[Finish Up]
    //--Clean.
    SetColor("Clear", cColorAlpha);
}
