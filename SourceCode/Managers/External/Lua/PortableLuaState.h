///===================================== PortableLuaState =========================================
//--A lua state you can pack up and take with you! Okay, not quite. This class represents
//  a 'savefile' made primarily in Lua. It basically saves an entire table which is meant
//  to represent the important parts of the Lua state.
//--In the source project, Doll Manor, the table gzTextVar contains almost the entirety
//  of the game's variables. Functions and some constants are built at runtime and thus
//  are not saved. Saving this table would allow the player to save all relevant variables.
//--The downside is that a lot of unnecessary variables will get saved, but it's also largely
//  idiot-proof and the programmer can make constants not part of the gzTextVar table to
//  prevent them from getting saved.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLinkedList.h"

///===================================== Local Definitions ========================================
#define PLS_NIL 0
#define PLS_NUMERIC 1
#define PLS_STRING 2
#define PLS_TABLE 3
#define PLS_BOOLEAN 4

///===================================== Local Structures =========================================
//--An object in the Portable Lua State. Can be a string, numeric, or a table.
class PLSObject
{
    //--Metadata
    public:
    int mType;
    bool mIsBaseTable;
    bool mUsesNumericIdentifer;
    float mIdentifierNumeric; //Array Index: BaseObject[index] = Value
    char *mIdentifierString; //Name, format: BaseObject.ThisObject = Value

    //--Storage
    bool mBoolean;
    float mNumeric;
    char *mString;

    //--Table Case
    StarLinkedList *mTableData;

    //--System
    PLSObject()
    {
        //--Metadata
        mType = PLS_NIL;
        mIsBaseTable = false;
        mUsesNumericIdentifer = true;
        mIdentifierNumeric = 0.0f;
        mIdentifierString = NULL;

        //--Storage
        mBoolean = false;
        mNumeric = 0.0f;
        mString = NULL;

        //--Table Case
        mTableData = NULL;
    }
    ~PLSObject()
    {
        free(mIdentifierString);
        free(mString);
        delete mTableData;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        PLSObject *rObject = (PLSObject *)pPtr;
        delete rObject;
    }

    //--Other Functions
    void SetIdentifierNumeric(float pIdentifier);
    void SetIdentifierString(const char *pName);
    void BecomeNumeric(float pValue);
    void BecomeString(const char *pValue);
    void BecomeTable();
    void BecomeBoolean(bool pBoolean);
    void Traverse(lua_State *pLuaState);
    void RenderToBuffer(const char *pLeadIn, StarAutoBuffer *pBuffer);
    void RenderToBinary(StarAutoBuffer *pBuffer);
    static PLSObject *ReadFromBinary(VirtualFile *fInfile);
};

///========================================== Classes =============================================
class PortableLuaState
{
    private:
    //--System
    bool mOperationComplete;
    char *mLocalTableName;

    //--Storage
    PLSObject *mBaseTable;

    protected:

    public:
    //--System
    PortableLuaState();
    ~PortableLuaState();

    //--Public Variables
    static int xTotalEntries;

    //--Property Queries
    //--Manipulators
    //--Core Methods
    void OperateOn(lua_State *pLuaState, const char *pTableName);
    void Reconstruct();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void WriteToFile(const char *pFileName);
    void ReadFromFile(const char *pFileName);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

