///======================================== StarUIPopup ============================================
//--A UI piece that represents several options that can appear in multiple locations on the screen,
//  usually by clicking on something. The options are dynamic and the object uses scroll rendering
//  and internal timers. It can be slotted on top of a lot of things, including non-mouse UI objects.

#pragma once

///========================================= Includes =============================================
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
class StarBitmap;
class StarFont;
class StarLinkedList;

///========================================== Classes =============================================
class StarUIPopup
{
    protected:
    ///--[Constants]
    ///--[System]
    StarLinkedList *mEntries; //char *, master

    ///--[Position]
    int mVisTimer;
    int mVisTimerMax;
    float mHBuf;
    float mVBuf;
    float mVPerEntry;
    TwoDimensionReal mDimensions;

    ///--[Display]
    int mStencilCode;
    bool mImagesReady;
    StarFont *rFont;
    StarBitmap *rNineFrame;

    protected:

    public:
    //--System
    StarUIPopup();
    virtual ~StarUIPopup();

    //--Public Variables
    //--Property Queries
    int GetTotalEntries();

    //--Manipulators
    void SetVisTimerMax(int pMax);
    void SetStencilCode(int pCode);
    void SetFontS(const char *pDLFontName);
    void SetFrameImageS(const char *pDLPath);
    void SetFontP(StarFont *pFont);
    void SetFrameImageP(StarBitmap *pImage);
    void SetPosition(float pX, float pY);
    void SetBuffers(float pHBuf, float pVBuf);
    void ClearEntries();
    void RegisterEntry(const char *pName, const char *pDisplay);

    //--Core Methods
    void FinalizeList();

    protected:
    //--Private Core Methods
    public:
    //--Update
    const char *UpdateActive();
    void UpdateInactive();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


