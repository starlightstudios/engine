///======================================== StarCamera ============================================
//--Root class of the Camera structure. Cameras are points in 2D or 3D space which have angle
//  information. They can move independently and respect boundaries.
//--The base class does nothing. Derived 2D and 3D classes handle the work. Note that the 2D
//  or 3D classes may not exist in a project if it does not have those components.
//--Cameras are typically held by the CameraManager. There can be multiple Cameras in existence
//  but only one can normally be used for rendering (unless you're in split-screen mode).

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class StarCamera
{
    private:

    protected:

    public:
    //--System
    StarCamera();
    virtual ~StarCamera();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    virtual bool Is2DCamera();
    virtual bool Is3DCamera();

    //--Property Queries
    //--Manipulators
    //--Core Methods
    virtual void Clear();
    virtual void PositionInWorld();
    virtual void UnpositionInWorld();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

