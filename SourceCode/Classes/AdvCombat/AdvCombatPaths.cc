//--Base
#include "AdvCombat.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

void AdvCombat::SetStandardRetreatScript(const char *pPath)
{
    ResetString(mStandardRetreatScript, pPath);
}
void AdvCombat::SetStandardDefeatScript(const char *pPath)
{
    ResetString(mStandardDefeatScript, pPath);
}
void AdvCombat::SetStandardVictoryScript(const char *pPath)
{
    ResetString(mStandardVictoryScript, pPath);
}
void AdvCombat::SetCombatResponseScript(const char *pPath)
{
    ResetString(mCombatResponseScript, pPath);
}
void AdvCombat::SetRetreatScript(const char *pPath)
{
    ResetString(mCurrentRetreatScript, pPath);
}
void AdvCombat::SetDefeatScript(const char *pPath)
{
    //--Set the defeat script.
    ResetString(mCurrentDefeatScript, pPath);

    //--Run the volunteer handler. This may happen multiple times during setup if doing manual
    //  script setup. The volunteer case should always reflect the last defeat script.
    if(mVolunteerScript)
    {
        if(mCurrentDefeatScript)
            LuaManager::Fetch()->ExecuteLuaFile(mVolunteerScript, 1, "S", mCurrentDefeatScript);
        else
            LuaManager::Fetch()->ExecuteLuaFile(mVolunteerScript, 1, "S", "Null");
    }
}
void AdvCombat::SetVictoryScript(const char *pPath)
{
    ResetString(mCurrentVictoryScript, pPath);
}
void AdvCombat::SetDoctorResolvePath(const char *pPath)
{
    ResetString(mDoctorResolvePath, pPath);
}
void AdvCombat::SetVolunteerScript(const char *pPath)
{
    ResetString(mVolunteerScript, pPath);
}
