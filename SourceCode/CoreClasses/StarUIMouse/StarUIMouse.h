///======================================== StarUIMouse ===========================================
//--Basic mouse handling, this can be inherited by a UI piece to gain access to mouse functionality.
//  It is meant to be used with a StarUIPiece object but can nominally be used with anything.

#pragma once

///========================================= Includes =============================================
#include "Structures.h"

///===================================== Local Structures =========================================
///--[HitboxReplyPack]
//--When RunHitboxCheck() is called, the list mHitboxReplyList is populated with these reply packs
//  that indicate which hitboxes were under the mouse.
typedef struct HitboxReplyPack
{
    //--Members.
    int mIndex;                         //Index in the mHitboxList of the reply.
    const char *rName;                  //Name of the hitbox, can be "X" or NULL.
    TwoDimensionReal *rHitbox;          //The hitbox in question.

    //--Functions.
    void Initialize()
    {
        mIndex = -1;
        rName = NULL;
        rHitbox = NULL;
    }
}HitboxReplyPack;

///--[ScrollbarPack]
//--Stores scrollbars active in this UI. Scrollbars have special click and drag functionality.
typedef struct ScrollbarPack
{
    //--Members
    int *rSkipPtr;                      //Pointer to the "skip" value which is how many objects are below rendering.
    int mPerPage;                       //How many entries can appear on a page. Defines the size of the scrollbar box.
    int mMaxSkip;                       //The maximum skip value. If dynamic, the caller will need to update this.
    int mIndexUp;                       //Index in mHitboxList of the up button. If -1, has no up button.
    int mIndexBd;                       //Index in mHitboxList of the body button. If -1, has no body button. Body is also used for drag calculations.
    int mIndexDn;                       //Index in mHitboxList of the down button. If -1, has no down button.

    //--Functions
    void Initialize()
    {
        rSkipPtr = NULL;
        mPerPage = 1;
        mMaxSkip = 0;
        mIndexUp = -1;
        mIndexBd = -1;
        mIndexDn = -1;
    }
}ScrollbarPack;

///===================================== Local Definitions ========================================
#define STARMOUSE_SCROLL_NOHIT -1
#define STARMOUSE_SCROLL_ABOVE 0
#define STARMOUSE_SCROLL_ON 1
#define STARMOUSE_SCROLL_BELOW 2

///========================================== Classes =============================================
class StarUIMouse
{
    protected:
    ///--[System]
    bool mMouseMoved;                   //Reports true if the mouse moved this tick, false if not.
    int mMouseX;                        //Stores X position of mouse on virtual canvas. Virtual mouse is supported.
    int mMouseY;                        //Stores Y position of mouse on virtual canvas. Virtual mouse is supported.
    int mMouseZ;                        //Stores position of mousewheel.
    int mMouseZPrev;                    //Previous tick's position of the mousewheel.

    ///--[Hitboxes]
    StarLinkedList *mHitboxList;        //TwoDimensionReal *, master. List of all hitboxes in the object.
    StarLinkedList *mHitboxReplyList;   //HitboxReplyPack *, master. Contains a list, in order, of all hitboxes that replied to the last mouse check.

    ///--[Scrollbars]
    int mClickStartY;                   //When a scrollbar is clicked and dragged, this is the starting Y position.
    int mClickIncY;                     //How many pixels need to be dragged before the scrollbar moves one skip in either direction.
    ScrollbarPack *rClickedScrollbar;   //If a scrollbar is being clicked and dragged, this points to it. Can be NULL. Only one can be dragged at a time.
    ScrollbarPack *rWheelScrollbar;     //If not-NULL, the mousewheel will move the scrollbar in question.
    StarLinkedList *mScrollbarList;     //ScrollbarPack *, master. List of all active scrollbars.

    protected:

    public:
    //--System
    StarUIMouse();
    virtual ~StarUIMouse();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void RegisterHitbox(const char *pName, float pLft, float pTop, float pRgt, float pBot);
    void RegisterHitboxWH(const char *pName, float pLft, float pTop, float pWid, float pHei);

    //--Core Methods
    void RunHitboxCheck();
    int CheckRepliesWithin(int pRangeLo, int pRangeHi);

    //--Scrollbar Routines
    int ComputeScrollbarHit(int pMouseX, int pMouseY, int pSkip, int pPerPage, int pMax, TwoDimensionReal pHitbox);
    bool UpdateScrollbarMouseLeft(ScrollbarPack *pPackage);

    private:
    //--Private Core Methods
    public:
    //--Update
    bool GetMouseInfo();
    virtual bool UpdateScrollbars();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


