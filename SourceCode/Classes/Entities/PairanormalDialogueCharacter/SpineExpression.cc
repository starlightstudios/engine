//--Base
#include "SpineExpression.h"

//--Classes
#include "PairanormalLevel.h"
#include "StarlightTextureLoader.h"

//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DebugManager.h"

//--Spine
#include "spine/spine.h"
#include "spine/Atlas.h"
/*
#include "Attachment.h"
#include "Atlas.h"
#include "extension.h"
#include "Skeleton.h"
#include "Slot.h"
#include "AnimationState.h"
#include "SkeletonJson.h"*/

//--Namespace
using namespace spine;

//#define EXP_DEBUG
#ifdef EXP_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
SpineExpression::SpineExpression()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_SPINEEXPRESSION;

    //--[SpineExpression]
    //--System
    mIsReady = false;

    //--Spine Structures
    mSpineAtlas = NULL;
    mSpineSkeletonData = NULL;
    mAnimationStateData = NULL;

    //--Animations
    mSkeleton = NULL;
    mAnimationState = NULL;

    //--Vertices
    mRenderHFlipped = false;
    //float mWorldVerticesPositions[MAX_VERTICES_PER_ATTACHMENT];
    //Vertex mVertices[MAX_VERTICES_PER_ATTACHMENT];
}
SpineExpression::~SpineExpression()
{
    delete mSpineAtlas;
    delete mSpineSkeletonData;
    delete mAnimationStateData;
}
void SpineExpression::Construct(const char *pBasePath, const char *pImagePath, const char *pEmotionA, const char *pEmotionB)
{
    Construct3(pBasePath, pImagePath, pEmotionA, pEmotionB, "NULL");
}
void SpineExpression::Construct3(const char *pBasePath, const char *pImagePath, const char *pEmotionA, const char *pEmotionB, const char *pEmotionC)
{
    //--Error check.
    if(!pBasePath || !pEmotionA || !pEmotionB || !pEmotionC) return;
    DebugPush(true, "Expression constructing: %s %s %s %s %s\n", pBasePath, pImagePath, pEmotionA, pEmotionB, pEmotionC);

    //--Setup.
    char tBuffer[256];

    //--String used to load the image atlas. Make sure it actually exists or fail here.
    ResetString(SpineExpression::xLoadPath, pImagePath);
    SugarBitmap *rCheckPtr = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    if(!rCheckPtr)
    {
        DebugPop("No image found.\n");
        return;
    }

    //--Debug.
    DebugPrint("Verify image: %p - %i %i\n", rCheckPtr, rCheckPtr->GetWidth(), rCheckPtr->GetHeight());

    //--Create the atlas.
    DebugPrint("Creating atlas.\n");
    sprintf(tBuffer, "%s/skeleton.atlas", pBasePath);
    mSpineAtlas = new Atlas(tBuffer, new StarlightTextureLoader());
    if(mSpineAtlas->getPages().size() == 0)
    {
        DebugPop("No atlas found %s.\n", tBuffer);
        delete mSpineAtlas;
        return;
    }

    DebugPrint("Loading skeleton data.\n");
    sprintf(tBuffer, "%s/skeleton.json", pBasePath);
    SkeletonJson tSkeletonJSON(mSpineAtlas);
    mSpineSkeletonData = tSkeletonJSON.readSkeletonDataFile(tBuffer);
    if(!mSpineSkeletonData)
    {
        DebugPop("Failed to load skeleton data");
        delete mSpineAtlas;
        return;
    }

    //--Setup mix times
    DebugPrint("Setting mix times.\n");
    mAnimationStateData = new AnimationStateData(mSpineSkeletonData);
    mAnimationStateData->setDefaultMix(0.5f);

    //--If there are three cross-mixes, handle their timers:
    if(pEmotionA && pEmotionB && pEmotionC && strcasecmp(pEmotionA, "Null") && strcasecmp(pEmotionB, "Null") && strcasecmp(pEmotionC, "Null"))
    {
        mAnimationStateData->setMix(pEmotionA, pEmotionB, 0.2f);
        mAnimationStateData->setMix(pEmotionA, pEmotionC, 0.2f);
        mAnimationStateData->setMix(pEmotionB, pEmotionA, 0.2f);
        mAnimationStateData->setMix(pEmotionB, pEmotionC, 0.2f);
        mAnimationStateData->setMix(pEmotionC, pEmotionA, 0.2f);
        mAnimationStateData->setMix(pEmotionC, pEmotionB, 0.2f);
    }

    //--Boot the skeleton.
    DebugPrint("Booting skeleton.\n");
    mSkeleton = new Skeleton(mSpineSkeletonData);
    mSkeleton->setX(0.0f);
    mSkeleton->setY(0.0f);

    //--Boot the animation. Default is 0th state.
    DebugPrint("Setting animation.\n");
    mAnimationState = new AnimationState(mAnimationStateData);
    Animation *rCheckAnimation = mSpineSkeletonData->findAnimation(pEmotionA);
    if(!rCheckAnimation)
    {
        //--List of animations.
        DebugPrint("Unable to boot to default emotion %s. Listing emotions.\n", pEmotionA);
        Vector<Animation *> rAnimations = mSpineSkeletonData->getAnimations();
        for(int i = 0; i < (int)rAnimations.size(); i ++)
        {
            Animation *rAnimation = rAnimations[i];
            const char *rAnimationName = NULL;
            if(rAnimation) rAnimationName = rAnimation->getName().buffer();
            DebugPrint(" %i: %p, %s\n", i, rAnimation, rAnimationName);
            if(i == 0) mAnimationState->setAnimation(0, rAnimationName, true);
        }
    }
    else
    {
        mAnimationState->setAnimation(0, pEmotionA, true);
        #ifdef EXP_DEBUG
        Vector<Animation *> rAnimations = mSpineSkeletonData->getAnimations();
        for(int i = 0; i < (int)rAnimations.size(); i ++)
        {
            Animation *rAnimation = rAnimations[i];
            const char *rAnimationName = NULL;
            if(rAnimation) rAnimationName = rAnimation->getName().buffer();
            DebugPrint(" %i: %p, %s\n", i, rAnimation, rAnimationName);
        }
        #endif
    }

    //--Run it once.
    mAnimationState->update(1.0f / 60.0f);
    mAnimationState->apply(*mSkeleton);
    mSkeleton->updateWorldTransform();

    //--All checks passed, this is ready to render.
    mIsReady = true;
    DebugPop("Created successfully.\n");
}

///--[Public Statics]
//--Because of the way Spine is set up, we can't use arguments to specify where a skeleton should
//  gets its image. Instead it's hard coded to use this path.
char *SpineExpression::xLoadPath = NULL;

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void SpineExpression::SetEmotion(const char *pEmotionName)
{
    //--Error check.
    if(!mIsReady) return;

    //--Check if the animation exists. If it does not, use the zeroth animation instead.
    Animation *rCheckAnimation = mSpineSkeletonData->findAnimation(pEmotionName);
    if(!rCheckAnimation)
    {
        //--Get animation list.
        Vector<Animation *> rAnimations = mSpineSkeletonData->getAnimations();
        if(rAnimations.size() < 1) return;

        //--Get zeroth animation.
        Animation *rAnimation = rAnimations[0];
        const char *rAnimationName = NULL;
        if(rAnimation) rAnimationName = rAnimation->getName().buffer();

        //--Set it as active.
        mAnimationState->setAnimation(0, rAnimationName, true);

        //--Debug.
        if(false)
        {
            fprintf(stderr, "Unable to find emotion %s. %i emotions:\n", pEmotionName, (int)rAnimations.size());
            for(int i = 0; i < (int)rAnimations.size(); i ++)
            {
                Animation *rAnimation = rAnimations[i];
                fprintf(stderr, " Animation: %p\n", rAnimation);
                if(rAnimation) fprintf(stderr, " %i: %p, %s\n", i, rAnimation, rAnimation->getName().buffer());
            }
        }
    }
    else
    {
        mAnimationState->setAnimation(0, pEmotionName, true);
    }
}
void SpineExpression::SetHFlipFlag(bool pFlag)
{
    mRenderHFlipped = pFlag;
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void SpineExpression::Update()
{
    //--Error check.
    if(!mIsReady) return;

    //--First update the animation state by the delta time
    mAnimationState->update(1.0f / 60.0f);

    //--Next, apply the state to the skeleton
    mAnimationState->apply(*mSkeleton);

    //--Calculate world transforms for rendering
    mSkeleton->updateWorldTransform();
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
//--Worker function to quickly add vertices to the buffer.
void SpineExpression::AddVertex(float x, float y, float u, float v, float r, float g, float b, float a, int sIndex)
{
    float cTexCoordMult = 1.0f;
    if(PairanormalLevel::xIsLowResMode) cTexCoordMult = 0.5f;

    Vertex *rVertex = &mVertices[sIndex];
    rVertex->x = x;
    rVertex->y = y;
    rVertex->u = u * cTexCoordMult;
    rVertex->v = v * cTexCoordMult;
    rVertex->r = r;
    rVertex->g = g;
    rVertex->b = b;
    rVertex->a = a;
    //sIndex ++;
    if(xDebug) fprintf(stderr, "Input %02i - %6.2f %6.2f - %f %f\n", sIndex, rVertex->x, rVertex->y, rVertex->u, rVertex->v);
}
bool SpineExpression::xDebug = false;
void SpineExpression::RenderAtCenter(float pX, float pY, float pScale)
{
    //--For each slot in the draw order array of the skeleton...
    if(!mIsReady || pScale == 0.0f) return;
    //fprintf(stderr, "Begin render.\n");

    //--Position.
    glTranslatef(pX, pY, 0.0f);

    //--If the scale is larger, we bottom-center the image.
    float cCalibration = 300.0f;
    if(pScale > 1.0f)
    {
        glTranslatef(0.0f, cCalibration * (pScale - 1.0f), 0.0f);
    }

    //--Scale flipping. Spine uses reversed coordinate systems.
    glScalef(pScale, pScale, 1.0f);
    glScalef(1.0f, -1.0f, 1.0f);

    //--H-flip.
    if(mRenderHFlipped) glScalef(-1.0f, 1.0f, 1.0f);

    //--For each slot...
    int tSlots = (int)mSkeleton->getSlots().size();
    for(int i = 0; i < tSlots; i ++)
    {
        //--Get the slot.
        Slot *rSlot = mSkeleton->getDrawOrder()[i];
        if(!rSlot) continue;

        //--Fetch the currently active attachment, continue with the next slot in the draw order if no
        //  attachment is active on the slot.
        Attachment *rAttachment = rSlot->getAttachment();
        if(!rAttachment) continue;

        //--Fetch the blend mode from the slot and translate it to the engine blend mode.
        BlendMode tEngineBlendMode = rSlot->getData().getBlendMode();
        if(tEngineBlendMode == BlendMode_Additive)
        {
            glBlendFunc(GL_ONE, GL_ONE);
        }
        else if(tEngineBlendMode == BlendMode_Multiply)
        {
            glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
        }
        else if(tEngineBlendMode == BlendMode_Screen)
        {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);
        }
        else
        {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        }

        //--Calculate the tinting color based on the skeleton's color and the slot's color. Each color
        //  channel is given in the range [0-1], you may have to multiply by 255 and cast to and int
        //  if your engine uses integer ranges for color channels.
        float tintR = 1.0f;//mNeutralSkeleton->r * rSlot->r;
        float tintG = 1.0f;//mNeutralSkeleton->g * rSlot->g;
        float tintB = 1.0f;//mNeutralSkeleton->b * rSlot->b;
        float tintA = 1.0f;//mNeutralSkeleton->a * rSlot->a;
        //Color skeletonColor = skeleton->getColor();
        //Color slotColor = slot->getColor();
        //Color tint(skeletonColor.r * slotColor.r, skeletonColor.g * slotColor.g, skeletonColor.b * slotColor.b, skeletonColor.a * slotColor.a);

        //--Fill the vertices array depending on the type of attachment
        int vertexIndex = 0;
        if(rAttachment->getRTTI().isExactly(RegionAttachment::rtti))
        {
            /*
            //--Cast to an spRegionAttachment so we can get the rendererObject and compute the world vertices.
            RegionAttachment *rRegionAttachment = (RegionAttachment*)rAttachment;

            // Our engine specific Texture is stored in the spAtlasRegion which was
            // assigned to the attachment on load. It represents the texture atlas
            // page that contains the image the region attachment is mapped to.
            SugarBitmap *rBitmap = (SugarBitmap *)((AtlasRegion*)rRegionAttachment->getRendererObject())->page->getRendererObject();
            rBitmap->Bind();

            // Computed the world vertices positions for the 4 vertices that make up
            // the rectangular region attachment. This assumes the world transform of the
            // bone to which the slot (and hence attachment) is attached has been calculated
            // before rendering via spSkeleton_updateWorldTransform
            rRegionAttachment->computeWorldVertices(rSlot->getBone(), mWorldVerticesPositions, 0, sizeof(Vertex));

            //--Store.
            Vector<float> tUVs = rRegionAttachment->getUVs();

            // Create 2 triangles, with 3 vertices each from the region's
            // world vertex positions and its UV coordinates (in the range [0-1]).
            AddVertex(mWorldVerticesPositions[0], mWorldVerticesPositions[1],
                tUVs[0], tUVs[1],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[2], mWorldVerticesPositions[3],
                tUVs[2], tUVs[3],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[4], mWorldVerticesPositions[5],
                tUVs[4], tUVs[5],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[4], mWorldVerticesPositions[5],
                tUVs[4], tUVs[5],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[6], mWorldVerticesPositions[7],
                tUVs[6], tUVs[7],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[0], mWorldVerticesPositions[1],
                tUVs[0], tUVs[1],
                tintR, tintG, tintB, tintA, vertexIndex);
            */
        }
        else if(rAttachment->getRTTI().isExactly(MeshAttachment::rtti))
        {
            // Check the number of vertices in the mesh attachment. If it is bigger
            // than our scratch buffer, we don't render the mesh. We do this here
            // for simplicity, in production you want to reallocate the scratch buffer
            // to fit the mesh.
            MeshAttachment *rMesh = (MeshAttachment*)rAttachment;
            if(rMesh->getWorldVerticesLength() > MAX_VERTICES_PER_ATTACHMENT) continue;

            // Our engine specific Texture is stored in the spAtlasRegion which was
            // assigned to the attachment on load. It represents the texture atlas
            // page that contains the image the mesh attachment is mapped to
            SugarBitmap *rBitmap = (SugarBitmap *)((AtlasRegion*)rMesh->getRendererObject())->page->getRendererObject();
            rBitmap->Bind();

            for(int p = 0; p < MAX_VERTICES_PER_ATTACHMENT; p ++)
            {
                mVertices[p].x = -10.0f;
                mVertices[p].y = -10.0f;
                mVertices[p].u = -10.0f;
                mVertices[p].v = -10.0f;
                mWorldVerticesPositions[p] = -10.0f;
            }

            // Computed the world vertices positions for the vertices that make up
            // the mesh attachment. This assumes the world transform of the
            // bone to which the slot (and hence attachment) is attached has been calculated
            // before rendering via spSkeleton_updateWorldTransform
            size_t tVerticesTotal = rMesh->getWorldVerticesLength() / 2;
            rMesh->computeWorldVertices(*rSlot, 0, tVerticesTotal * 2, mWorldVerticesPositions, 0, 2);
            if(xDebug) fprintf(stderr, " Vertices: %i\n", (int)tVerticesTotal);

            //--Store.
            Vector<float> tUVs = rMesh->getUVs();
            for(int p = 0; p < (int)tVerticesTotal; p ++)
            {
                AddVertex(mWorldVerticesPositions[(p*2)+0], mWorldVerticesPositions[(p*2)+1],
                   tUVs[(p*2)+0], tUVs[(p*2)+1],
                   tintR, tintG, tintB, tintA, p);
                vertexIndex ++;
            }

            //--Set.
            Vector<unsigned short> tTriangles = rMesh->getTriangles();

            //--Now actually render the thing.
            if(xDebug) fprintf(stderr, "Triangles: %i\n", (int)tTriangles.size());
            glBegin(GL_TRIANGLES);
            for(int p = 0; p < (int)tTriangles.size(); p += 3)
            {
                int A = tTriangles[p+0];
                int B = tTriangles[p+1];
                int C = tTriangles[p+2];
                if(xDebug) fprintf(stderr, " %02i: Render %02i %02i %02i\n", p, A, B, C);

                glTexCoord2f(mVertices[A].u, 1.0f - mVertices[A].v); glVertex2f(mVertices[A].x, mVertices[A].y);
                glTexCoord2f(mVertices[B].u, 1.0f - mVertices[B].v); glVertex2f(mVertices[B].x, mVertices[B].y);
                glTexCoord2f(mVertices[C].u, 1.0f - mVertices[C].v); glVertex2f(mVertices[C].x, mVertices[C].y);
            }
            glEnd();
        }
    }
    if(xDebug) xDebug = false;

/*
    //--Render slots.
    //fprintf(stderr, "Rendering slots %i.\n", mSkeleton->slotsCount);
    for(int i = 0; i < mSkeleton->slotsCount; ++i)
    {
        //--Get the slot.
        spSlot *rSlot = mSkeleton->drawOrder[i];
        if(!rSlot) continue;
        //fprintf(stderr, " %i\n", i);

        //--Fetch the currently active attachment, continue with the next slot in the draw order if no
        //  attachment is active on the slot.
        spAttachment *rAttachment = rSlot->attachment;
        //fprintf(stderr, " Attachment %p\n", rAttachment);
        if(!rAttachment) continue;

        //--Fetch the blend mode from the slot and translate it to the engine blend mode.
        //BlendMode tEngineBlendMode;
        if(rSlot->data->blendMode == SP_BLEND_MODE_ADDITIVE)
        {
            glBlendFunc(GL_ONE, GL_ONE);
        }
        else if(rSlot->data->blendMode == SP_BLEND_MODE_MULTIPLY)
        {
            glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
        }
        else if(rSlot->data->blendMode == SP_BLEND_MODE_SCREEN)
        {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);
        }
        else
        {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        }

        //--Calculate the tinting color based on the skeleton's color and the slot's color. Each color
        //  channel is given in the range [0-1], you may have to multiply by 255 and cast to and int
        //  if your engine uses integer ranges for color channels.
        float tintR = 1.0f;//mNeutralSkeleton->r * rSlot->r;
        float tintG = 1.0f;//mNeutralSkeleton->g * rSlot->g;
        float tintB = 1.0f;//mNeutralSkeleton->b * rSlot->b;
        float tintA = 1.0f;//mNeutralSkeleton->a * rSlot->a;

        //--Fill the vertices array depending on the type of attachment
        int vertexIndex = 0;
        if(rAttachment->type == SP_ATTACHMENT_REGION)
        {
            //--Cast to an spRegionAttachment so we can get the rendererObject and compute the world vertices.
            spRegionAttachment *regionAttachment = (spRegionAttachment*)rAttachment;

            // Our engine specific Texture is stored in the spAtlasRegion which was
            // assigned to the attachment on load. It represents the texture atlas
            // page that contains the image the region attachment is mapped to
            SugarBitmap *rBitmap = (SugarBitmap*)((spAtlasRegion*)regionAttachment->rendererObject)->page->rendererObject;
            rBitmap->Bind();

            // Computed the world vertices positions for the 4 vertices that make up
            // the rectangular region attachment. This assumes the world transform of the
            // bone to which the slot (and hence attachment) is attached has been calculated
            // before rendering via spSkeleton_updateWorldTransform
            spRegionAttachment_computeWorldVertices(regionAttachment, rSlot->bone, mWorldVerticesPositions, 0, 2);

            // Create 2 triangles, with 3 vertices each from the region's
            // world vertex positions and its UV coordinates (in the range [0-1]).
            AddVertex(mWorldVerticesPositions[0], mWorldVerticesPositions[1],
                regionAttachment->uvs[0], regionAttachment->uvs[1],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[2], mWorldVerticesPositions[3],
                regionAttachment->uvs[2], regionAttachment->uvs[3],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[4], mWorldVerticesPositions[5],
                regionAttachment->uvs[4], regionAttachment->uvs[5],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[4], mWorldVerticesPositions[5],
                regionAttachment->uvs[4], regionAttachment->uvs[5],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[6], mWorldVerticesPositions[7],
                regionAttachment->uvs[6], regionAttachment->uvs[7],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[0], mWorldVerticesPositions[1],
                regionAttachment->uvs[0], regionAttachment->uvs[1],
                tintR, tintG, tintB, tintA, vertexIndex);
        }
        else if (rAttachment->type == SP_ATTACHMENT_MESH)
        {
            // Cast to an spMeshAttachment so we can get the rendererObject
            // and compute the world vertices
            spMeshAttachment* mesh = (spMeshAttachment*)rAttachment;

            // Check the number of vertices in the mesh attachment. If it is bigger
            // than our scratch buffer, we don't render the mesh. We do this here
            // for simplicity, in production you want to reallocate the scratch buffer
            // to fit the mesh.
            if(mesh->super.worldVerticesLength > MAX_VERTICES_PER_ATTACHMENT) continue;

            // Our engine specific Texture is stored in the spAtlasRegion which was
            // assigned to the attachment on load. It represents the texture atlas
            // page that contains the image the mesh attachment is mapped to
            SugarBitmap *rBitmap = (SugarBitmap *)((spAtlasRegion*)mesh->rendererObject)->page->rendererObject;
            rBitmap->Bind();

            // Computed the world vertices positions for the vertices that make up
            // the mesh attachment. This assumes the world transform of the
            // bone to which the slot (and hence attachment) is attached has been calculated
            // before rendering via spSkeleton_updateWorldTransform
            spVertexAttachment_computeWorldVertices(SUPER(mesh), rSlot, 0, mesh->super.worldVerticesLength, mWorldVerticesPositions, 0, 2);

            // Mesh attachments use an array of vertices, and an array of indices to define which
            // 3 vertices make up each triangle. We loop through all triangle indices
            // and simply emit a vertex for each triangle's vertex.
            for (int i = 0; i < mesh->trianglesCount; ++i)
            {
                int index = mesh->triangles[i] << 1;
                AddVertex(mWorldVerticesPositions[index], mWorldVerticesPositions[index + 1],
                   mesh->uvs[index], mesh->uvs[index + 1],
                   tintR, tintG, tintB, tintA, vertexIndex);
            }
        }

        //--Now actually render the thing.
        glBegin(GL_TRIANGLES);
        for(int i = 0; i < vertexIndex; i += 3)
        {
            glTexCoord2f(mVertices[i+0].u, 1.0f - mVertices[i+0].v); glVertex2f(mVertices[i+0].x, mVertices[i+0].y);
            glTexCoord2f(mVertices[i+1].u, 1.0f - mVertices[i+1].v); glVertex2f(mVertices[i+1].x, mVertices[i+1].y);
            glTexCoord2f(mVertices[i+2].u, 1.0f - mVertices[i+2].v); glVertex2f(mVertices[i+2].x, mVertices[i+2].y);
        }
        glEnd();
    }
*/
    //--H-flip.
    if(mRenderHFlipped) glScalef(-1.0f, 1.0f, 1.0f);

    //--Clean.
    glScalef(1.0f, -1.0f, 1.0f);
    glScalef(1.0f / pScale, 1.0f / pScale, 1.0f);
    glTranslatef(-pX, -pY, 0.0f);
    if(pScale > 1.0f)
    {
        glTranslatef(0.0f, cCalibration * (pScale - 1.0f) * -1.0f, 0.0f);
    }
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
