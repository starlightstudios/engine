//--Base
#include "StarFileSystem.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

#if defined _PHYSFS_FILESYSTEM_
#include "physfs.h"
#endif

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///========================================== System ==============================================
StarFileSystem::StarFileSystem()
{
    //--System
    mTotalFiles = 0;
    mDirectoryInfo = NULL;
    mIsThisARecursion = false;

    //--Storage
    mBasePath = NULL;
    mLastParsedDirectory = NULL;

    //--Iteration
    mIterationPoint = 0;

    //--Public Variables
    mIgnoreFolders = false;
    mIgnoreZerothEntry = false;
    mIsRecursive = true;
}
StarFileSystem::~StarFileSystem()
{
    Clear();
}
void StarFileSystem::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((StarFileSystem *)pPtr);
}
void StarFileSystem::DeleteFileInfo(void *pPtr)
{
    if(!pPtr) return;
    FileInfo *rInfo = (FileInfo *)pPtr;
    free(rInfo->mPath);
    free(rInfo);
}

///===================================== Property Queries =========================================
int StarFileSystem::GetTotalEntries()
{
    return mTotalFiles;
}
FileInfo *StarFileSystem::GetEntry(int pSlot)
{
    if(pSlot < 0 || pSlot >= mTotalFiles) return NULL;
    return mDirectoryInfo[pSlot];
}
char *StarFileSystem::GetLastParsedDirectory()
{
    return mLastParsedDirectory;
}
char *StarFileSystem::GetIteratedPath()
{
    if(mIterationPoint < 0 || mIterationPoint >= mTotalFiles) return NULL;
    return mDirectoryInfo[mIterationPoint]->mPath;
}

///======================================== Manipulators ==========================================
void StarFileSystem::SetRecursiveFlag(bool pFlag)
{
    mIsRecursive = pFlag;
}
void StarFileSystem::ScanDirectory(const char *pPath)
{
    ///--[Documentation]
    //--Orders the given directory to be scanned into memory. Pass NULL to clear implicitly.
    Clear();
    if(!pPath) return;

    //--Store base path.
    ResetString(mBasePath, pPath);

    ///--[Scan]
    //--Order a scan to take place.
    StarLinkedList *tFileInfoList = SubScanDirectory(pPath, mIsRecursive);
    if(!tFileInfoList) return;

    ///--[Store]
    //--Copy and store.
    MergeFileInfo(tFileInfoList);
    delete tFileInfoList;

    ///--[Sort]
    //--Sort it, directories first.
    SortDirectory();

    ///--[Debug]
    //--Check the contents.
    if(false) PrintContents();
}

///======================================== Core Methods ==========================================
void StarFileSystem::Clear()
{
    //--Clears off the existing data and returns the object to a neutral state.
    for(int i = 0; i < mTotalFiles; i ++)
    {
        DeleteFileInfo(mDirectoryInfo[i]);
    }
    free(mDirectoryInfo);
    mDirectoryInfo = NULL;
    mTotalFiles = 0;

    free(mBasePath);
    mBasePath = NULL;

    free(mLastParsedDirectory);
    mLastParsedDirectory = NULL;
}
void StarFileSystem::PrintContents()
{
    DebugManager::Push(true);
    DebugManager::Print("Directory contents: %s\n", mBasePath);
    DebugManager::Print("Found %i elements.\n", mTotalFiles);
    for(int i = 0; i < mTotalFiles; i ++)
    {
        DebugManager::Print(" %p %s\n", mDirectoryInfo[i], mDirectoryInfo[i]->mPath);
    }
    DebugManager::Print("Done.\n");
    DebugManager::Pop();
}
void StarFileSystem::MergeFileInfo(StarLinkedList *pFileInfoList)
{
    ///--[Documentation]
    //--Places the data from the given pFileInfoList into the sized array. The RLL is left whole
    //  and the caller must delete it.
    //--Entries in the filesystem are pointer-copied, the RLL should not delete them!
    if(!pFileInfoList) return;

    //--Allocate
    mTotalFiles = pFileInfoList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mDirectoryInfo = (FileInfo **)starmemoryalloc(sizeof(FileInfo *) * mTotalFiles);

    //--Copy over in iteration.
    int tSlot = 0;
    FileInfo *rEntry = (FileInfo *)pFileInfoList->PushIterator();
    while(rEntry)
    {
        mDirectoryInfo[tSlot] = rEntry;
        tSlot ++;
        rEntry = (FileInfo *)pFileInfoList->AutoIterate();
    }

    //--Debug.
    if(false) PrintContents();
}
void StarFileSystem::SortDirectory()
{
    ///--[Documentation]
    //--Optional call. Normally, filesystems will sort alphabetically, or they may not sort the
    //  data at all. This call sorts such that directories are first, and otherwise the files are
    //  sorted alphabetically.
    if(mTotalFiles < 2 || !mDirectoryInfo) return;

    ///--[QSort]
    //--Just use the in-built quicksort. Let's not reinvent the wheel.
    qsort(mDirectoryInfo, mTotalFiles, sizeof(FileInfo *), &StarFileSystem::SortFunctionAlphabetical);
}
void StarFileSystem::SortDirectoryByLastAccess()
{
    //--Sorts the directory by when the entries were last accessed.
    if(mTotalFiles < 2 || !mDirectoryInfo) return;

    qsort(mDirectoryInfo, mTotalFiles, sizeof(FileInfo *), &StarFileSystem::SortFunctionLastAccess);
}
void StarFileSystem::ResetIterator()
{
    mIterationPoint = 0;
}
void StarFileSystem::Iterate()
{
    mIterationPoint ++;
}
StarLinkedList *StarFileSystem::GetFilesWithExtension(const char *pExtension)
{
    ///--[Documentation]
    //--Given a file extension, returns a heap-allocated StarLinkedList containing a reference list
    //  of all FileInfo objects that match that extension. The list can legally have zero entries.
    //--Caller must deallocate the list.
    StarLinkedList *nrList = new StarLinkedList(false);
    if(!pExtension) return nrList;

    ///--[Execution]
    //--Setup.
    int tExtensionLen = (int)strlen(pExtension);

    //--Iterate.
    fprintf(stderr, "Scanning %i files.\n", mTotalFiles);
    for(int i = 0; i < mTotalFiles; i ++)
    {
        //--Get entry.
        FileInfo *rFileInfo = GetEntry(i);
        if(!rFileInfo) continue;

        //--Check the extension. On match, add to the list.
        int tLen = (int)strlen(rFileInfo->mPath);
        fprintf(stderr, "Check %s %s\n", pExtension, rFileInfo->mPath);
        if(tLen >= tExtensionLen && !strcasecmp(&rFileInfo->mPath[tLen-tExtensionLen], pExtension))
        {
            nrList->AddElementAsTail("X", rFileInfo);
        }
    }

    //--Finish up.
    return nrList;
}

///===================================== Sorting Functions ========================================
int StarFileSystem::SortFunctionAlphabetical(const void *pInfoA, const void *pInfoB)
{
    //--Sorting function of SortDirectory() used above.
    FileInfo *rInfoA = *((FileInfo **)pInfoA);
    FileInfo *rInfoB = *((FileInfo **)pInfoB);
    if(rInfoA->mIsDirectory && !rInfoB->mIsDirectory)
    {
        return -1;
    }
    else if(!rInfoA->mIsDirectory && rInfoB->mIsDirectory)
    {
        return 1;
    }
    else
    {
        return strcmp(rInfoA->mPath, rInfoB->mPath);
    }
}
int StarFileSystem::SortFunctionLastAccess(const void *pInfoA, const void *pInfoB)
{
    //--Sorting function of SortDirectoryByLastAccess() used above.
    FileInfo *rInfoA = *((FileInfo **)pInfoA);
    FileInfo *rInfoB = *((FileInfo **)pInfoB);
    if(rInfoA->mIsDirectory && !rInfoB->mIsDirectory)
    {
        return -1;
    }
    else if(!rInfoA->mIsDirectory && rInfoB->mIsDirectory)
    {
        return 1;
    }
    else
    {
        return difftime(rInfoB->mLastAccessed, rInfoA->mLastAccessed);
    }
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
//--If no filesystem is defined, use this dummy function. It will return an empty list.
#if defined _NO_FILESYSTEM_
StarLinkedList *StarFileSystem::SubScanDirectory(const char *pPath, bool pScanDirectories)
{
    StarLinkedList *nFileInfoList = new StarLinkedList(false);
    return nFileInfoList;
}
#endif

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
bool StarFileSystem::FileExists(const char *pPattern, ...)
{
    ///--[Documentation]
    //--Returns whether or not the named file exists. The pattern is the pathname, but can use
    //  printf-style generation to expand itself if needed.
    if(!pPattern) return false;

    //--Get the variable arg list.
    va_list tArgList;
    va_start(tArgList, pPattern);

    //--Print the args into a buffer.
    char tBuffer[2048];
    vsprintf(tBuffer, pPattern, tArgList);
    va_end(tArgList);

    //--The buffer represents the pathname. Check for existence.
    FILE *fCheckFile = fopen(tBuffer, "r");
    if(!fCheckFile) return false;

    //--It exists. Clean up.
    fclose(fCheckFile);
    return true;
}
char *StarFileSystem::PareFileName(const char *pPath)
{
    //--Overload. Pares the filename and implicitly keeps the extension.
    return PareFileName(pPath, true);
}
char *StarFileSystem::PareFileName(const char *pPath, bool pKeepExtension)
{
    ///--[Documentation]
    //--Given a filename of some sort, knocks off the rest of the path (absolute or relative) and
    //  returns the filename + extension as a heap-string.  You are responsible for deallocation.
    //--If pKeepExtension is false, the extension is also knocked off.
    //--NULL can be returned on error or an invalid path of some sort.
    if(!pPath) return NULL;
    char *nString = NULL;

    //--Parse backwards to locate the / or \\, depending on OS.
    int tSlashPos = -1;
    uint32_t tLength = strlen(pPath);
    for(int i = tLength-1; i >= 0; i --)
    {
        if(pPath[i] == '/' || pPath[i] == '\\')
        {
            tSlashPos = i;
            break;
        }
    }

    //--No slash was detected.
    if(tSlashPos == -1)
    {
        ResetString(nString, pPath);
        return nString;
    }

    //--Copy and return.
    SetMemoryData(__FILE__, __LINE__);
    nString = (char *)starmemoryalloc(sizeof(char) * (tLength - tSlashPos));
    for(int i = tSlashPos+1; i < (int)tLength; i ++)
    {
        nString[i - tSlashPos - 1] = pPath[i];
        nString[i - tSlashPos - 0] = '\0';
    }

    //--If the extension is not desired, knock it off.
    if(!pKeepExtension)
    {
        for(int i = 0; i < (int)strlen(nString); i ++)
        {
            if(nString[i] == '.')
            {
                nString[i] = '\0';
                break;
            }
        }
    }

    //--<DEBUG>
    //fprintf(stderr, "Pared %s to %s with %i slots\n", pPath, nString, (tLength - tSlashPos));

    return nString;
}
StarFileSystem *StarFileSystem::Fetch()
{
    //--Unlike most fetch calls, this one returns the top of the Global's FileSystem stack. It can
    //  therefore be NULL, and must be checked!
    return (StarFileSystem *)Global::Shared()->gFileSystemStack->GetHead();
}
bool StarFileSystem::IsFileExtension(const char *pPath, const char *pExtension)
{
    //--Returns true if the file extension of pPath is the same as pExtension.
    //  Example: IsFileExtention(path, ".lua")
    if(!pPath || !pExtension) return false;

    //--Get the extension's position.
    int tPeriod = -1;
    int tLen = (int)strlen(pPath);
    for(int i = tLen - 1; i >= 0; i --)
    {
        if(pPath[i] == '.')
        {
            tPeriod = i;
            break;
        }
    }

    //--No legal extension!
    if(tPeriod == -1) return false;

    //--Do a comparison case.
    //fprintf(stderr, "File Extension: %s vs %s\n", &pPath[tPeriod], pExtension);
    return (strcasecmp(&pPath[tPeriod], pExtension) == 0);
}

///======================================== Lua Hooking ===========================================
void StarFileSystem::HookToLuaState(lua_State *pLuaState)
{
    /* FS_Exists(sPath) (1 boolean)
       Returns whether or not the given file exists. Does not need a FileSystem entry to exist. */
    lua_register(pLuaState, "FS_Exists", &Hook_FS_Exists);

    /* FS_Open(sPath)
       FS_Open(sPath, bRecurseFlag)
       Opens the specified folder on the hard drive as a Star FileSystem entry. Each call of Open()
       must be attached to a Close() call as well. The program automatically stacks the calls
       together so you can iterate into subfolders if you need to. */
    lua_register(pLuaState, "FS_Open", &Hook_FS_Open);

    /* FS_Reset()
       Resets the Star FileSystem iterator back to the 0th entry. */
    lua_register(pLuaState, "FS_Reset", &Hook_FS_Reset);

    /* FS_Iterate()
       Iterates up to the next entry on the filesystem, and returns its path. Returns "NULL" if
       there were no more paths in the directory. */
    lua_register(pLuaState, "FS_Iterate", &Hook_FS_Iterate);

    /* FS_Close()
       Closes the currently open Star FileSystem entry. */
    lua_register(pLuaState, "FS_Close", &Hook_FS_Close);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_FS_Exists(lua_State *L)
{
    //FS_Exists(sPath) (1 boolean)
    int pArgs = lua_gettop(L);
    if(pArgs < 1) return LuaArgError("FS_Exists");

    lua_pushboolean(L, StarFileSystem::FileExists(lua_tostring(L, 1)));
    return 1;
}
int Hook_FS_Open(lua_State *L)
{
    //FS_Open(sPath[])
    //FS_Open(sPath[], bRecurseFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1 && pArgs != 2) return LuaArgError("FS_Open");

    //--Create a new StarFileSystem
    StarFileSystem *nSystem = new StarFileSystem();

    //--Switch this flag if the args call for it.
    if(pArgs == 2)
    {
        nSystem->mIsRecursive = lua_toboolean(L, 2);
    }

    //--Scan it.
    nSystem->ScanDirectory(lua_tostring(L, 1));

    //--Push it as a new entry.
    Global::Shared()->gFileSystemStack->AddElementAsHead("X", nSystem, &StarFileSystem::DeleteThis);

    return 0;
}
int Hook_FS_Reset(lua_State *L)
{
    //FS_Reset()
    StarFileSystem *rSystemHead = StarFileSystem::Fetch();
    if(!rSystemHead) return fprintf(stderr, "FS_Reset:  Failed, no open StarFileSystem.\n");

    rSystemHead->ResetIterator();

    return 0;
}
int Hook_FS_Iterate(lua_State *L)
{
    //FS_Iterate()
    StarFileSystem *rSystemHead = StarFileSystem::Fetch();
    if(!rSystemHead)
    {
        fprintf(stderr, "FS_Reset:  Failed, no open StarFileSystem.\n");
        lua_pushstring(L, "NULL");
        return 1;
    }

    //--Get the path, and check it agains NULL.
    char *rPath = rSystemHead->GetIteratedPath();
    if(rPath)
    {
        lua_pushstring(L, rPath);
    }
    else
    {
        lua_pushstring(L, "NULL");
    }

    //--Iterate AFTER the path is received.
    rSystemHead->Iterate();

    return 1;
}
int Hook_FS_Close(lua_State *L)
{
    //FS_Close()
    Global::Shared()->gFileSystemStack->DeleteHead();
    return 0;
}
