#include "Structures.h"
#include "LuaManager.h"
int LuaArgError(const char *pFunctionName)
{
    if(!pFunctionName) return 0;
    if(!LuaManager::xFailSilently)
        fprintf(stderr, "%s: Failed, invalid argument list. %s.\n", pFunctionName, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
int LuaArgError(const char *pFunctionName, lua_State *L)
{
    LuaArgError(pFunctionName);
    lua_pushnil(L);
    return 1;
}
int LuaArgError(const char *pFunctionName, const char *pSwitchType, int pArgs)
{
    if(!pFunctionName || !pSwitchType) return 0;
    if(!LuaManager::xFailSilently)
        fprintf(stderr, "%s: Failed, invalid argument list. %s %i - %s.\n", pFunctionName, pSwitchType, pArgs, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
int LuaTypeError(const char *pFunctionName, lua_State *L)
{
    //--Error check:
    if(!pFunctionName) return 0;

    //--Debug print.
    if(!LuaManager::xFailSilently)
    {
        //--Get the executing line.
        lua_Debug tLuaDebug;
        lua_getstack(L, 1, &tLuaDebug);
        lua_getinfo(L, "nSl", &tLuaDebug);
        int tExecLine = tLuaDebug.currentline;

        //--Print.
        fprintf(stderr, "%s: Failed, rActiveObject was wrong type, or NULL.\nFile: %s\nLine: %i\n", pFunctionName, LuaManager::Fetch()->GetCallStack(0), tExecLine);
        fprintf(stderr, " %i Arguments\n", lua_gettop(L));
        for(int i = 0; i < lua_gettop(L); i ++)
        {
            fprintf(stderr, "  %s\n", lua_tostring(L, i+1));
        }
    }
    return 0;
}
int LuaTypeError(const char *pFunctionName, lua_State *L, int pReturns)
{
    //--Same as above, but pushes the requested number of nil's to the caller.
    LuaTypeError(pFunctionName, L);
    for(int i = 0; i < pReturns; i ++) lua_pushnil(L);
    return pReturns;
}
int LuaPropertyError(const char *pFunctionName, const char *pSwitchType, int pArgs)
{
    if(!pFunctionName || !pSwitchType) return 0;
    if(!LuaManager::xFailSilently)
        fprintf(stderr, "%s: Failed, cannot resolve %s with %i args. %s.\n", pFunctionName, pSwitchType, pArgs, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
int LuaPropertyNoMatchError(const char *pFunctionName, const char *pSwitchType)
{
    if(!pFunctionName || !pSwitchType) return 0;
    fprintf(stderr, "%s: Failed, %s does not match any property. %s.\n", pFunctionName, pSwitchType, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
