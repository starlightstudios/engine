//--Base
#include "AdventureLevel.h"

//--Classes
#include "ActorNotice.h"
#include "AdvCombat.h"
#include "AdventureLevelGenerator.h"
#include "AdventureInventory.h"
#include "AdventureDebug.h"
#include "AdventureMenu.h"
#include "AdventureLight.h"
#include "KPopDanceGame.h"
#include "FieldAbility.h"
#include "PuzzleFight.h"
#include "RayCollisionEngine.h"
#include "RunningMinigame.h"
#include "ShakeMinigame.h"
#include "TileLayer.h"
#include "TilemapActor.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLoadInterrupt.h"
#include "StarBitmap.h"
#include "StarFileSystem.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#if defined _TARGET_OS_MAC_
#include "gl_ext_defs.h"
#endif
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "HitDetection.h"
#include "Global.h"
#include "GlDfn.h"
#include "OpenGLMacros.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "SaveManager.h"
#include "StarLumpManager.h"
#include "OptionsManager.h"

///--[Debug]
//#define AL_DEBUG
#ifdef AL_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
AdventureLevel::AdventureLevel()
{
    ///--[Diagnostics]
    DebugPush(true, "AdventureLevel: Constructor begins.\n");

    ///--[Variable Initialization]
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTURELEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[TiledLevel]
    //--System
    //--Sizing
    //--Tileset Packs
    //--Raw Lists
    //--Collision Storage

    //--[AdventureLevel]
    //--System
    mBasePath = NULL;
    mIsRandomLevel = false;
    mCannotOpenMenuTimer = 1;
    mIsPlayerOnStaircase = false;
    mPlayerStartX = 0;
    mPlayerStartY = 0;
    mRunParallelDuringDialogue = false;
    mRunParallelDuringCutscene = false;
    mNoBordersThisCutscene = false;
    mEnemyAICycles = 0;

    //--Autosave
    mAutosaveTimer = 5;
    mRunAutosave = true;
    mAutosavePostTimer = 0;
    rAutosaveImage = NULL;

    //--Minigames
    mRunningMinigame = NULL;
    mKPopDanceGame = NULL;
    mPuzzleFightGame = NULL;
    mShakeMinigame = NULL;

    //--Region Notifier
    mRegionNotifierAnimTotal = 0;
    mRegionNotifierAnimState = AL_REGION_STATE_IDLE;
    mRegionNotifierAnimTimer = 1;
    mRegionNotifierAnimTimerMax = 1;
    rRegionNotifierAnim = NULL;
    rRegionNotifier = NULL;
    mRegionNotifierFinalScale = 0.30f; //Default, can be changed with scripts.

    //--Notifications
    mNotifications = new StarLinkedList(true);

    //--Ray Collision Engine
    mRayCollisionEnginesTotal = 0;
    mRayCollisionEngines = NULL;

    //--Per-map animations.
    mIsMajorAnimationMode = false;
    mMajorAnimationName[0] = '\0';
    mPerMapAnimationList = new StarLinkedList(true);

    //--Debug Handling
    mWasMenuUpLastTick = false;
    mDebugMenu = new AdventureDebug();

    //--Menu
    mIsRestSequence = false;
    mRestingTimer = 0;
    mCannotOpenMenu = false;
    mAdventureMenu = MapManager::Fetch()->GenerateNewMenu();

    //--Object Information
    mChestList = new StarLinkedList(true);
    mFakeChestList = new StarLinkedList(true);
    mDoorList = new StarLinkedList(true);
    mExitList = new StarLinkedList(true);
    mInflectionList = new StarLinkedList(true);
    mClimbableList = new StarLinkedList(true);
    mLocationList = new StarLinkedList(true);
    mPositionList = new StarLinkedList(true);
    mInvisibleZoneList = new StarLinkedList(true);

    //--Catalyst Tone
    mPlayCatalystTone = false;
    mCatalystToneCountdown = 0;

    //--Examinables
    mExaminableScript = NULL;
    mExaminablesList = new StarLinkedList(true);

    //--Camera
    mIsCameraLocked = false;
    mCameraFollowID = 0;
    mCameraDimensions.Set(0.0f, 0.0f, VIRTUAL_CANVAS_X / mCurrentScale, VIRTUAL_CANVAS_Y / mCurrentScale);

    //--Backgrounds
    rBackgroundImg = NULL;
    mBackgroundStartX = 0.0f;
    mBackgroundStartY = 0.0f;
    mBackgroundScrollX = 0.0f;
    mBackgroundScrollY = 0.0f;

    //--Foreground Overlays
    mUnderlaysTotal = 0;
    mUnderlayPacks = NULL;
    mOverlaysTotal = 0;
    mOverlayPacks = NULL;
    mSpawnAuraChance = 0;
    rSpawnAuraImage = NULL;
    mAuraParticleList = new StarLinkedList(true);

    //--Control Locking/Handling
    mControlLockTimer = 0;
    mControlEntityID = 0;
    mFollowEntityIDList = new StarLinkedList(true);

    //--Followers
    mPlayerStoppedMovingTimer = 0;
    mControlLockList = new StarLinkedList(false);
    mFollowerSpacing[0] = xPartySpacing*1;
    mFollowerSpacing[1] = xPartySpacing*2;
    mFollowerSpacing[2] = xPartySpacing*3;
    memset(mPlayerXPositions, 0, sizeof(float) * PLAYER_MOVE_STORAGE_TOTAL);
    memset(mPlayerYPositions, 0, sizeof(float) * PLAYER_MOVE_STORAGE_TOTAL);

    //--UI Flags and Timers
    mShowPlatinumCompass = false;
    mEntireUIVisTimer    = 0;
    mStaminaVisTimer     = 0;
    mStaminaRingVisTimer = 0;
    mStaminaRingVisPct   = 0.0f;
    mFullStaminaTicks    = 0;

    //--Stamina
    mBlockRunningToRegen = false;
    mPlayerStamina    = 100.0f;
    mPlayerStaminaMax = 100.0f;
    mPartyLeadStamX   = -1000.0f;
    mPartyLeadStamY   = -1000.0f;

    //--Health
    mShowHealthMeter = false;
    mHealthRemaining = 0;
    mHealthMax = 0;
    rHealthIndicator = NULL;
    rEmptyIndicator = NULL;

    //--Transition Handling
    mIsExitStaircase = false;
    mTransitionDestinationMap = NULL;
    mTransitionDestinationExit = NULL;
    mTransitionPostScript = NULL;
    mTransitionDirection = 0;
    mTransitionPercentage = 0.0f;

    //--Transition Fading
    mTransitionHoldTimer = -1;
    mIsTransitionOut = false;
    mIsTransitionIn = true;
    mTransitionTimer = 0;

    //--Script-Controlled Fading
    mUseAlternateBlend = false;
    mIsScriptFading = false;
    mScriptFadeHolds = false;
    mScriptFadeDepthFlag = SCRIPT_FADE_UNDER_CHARACTERS;
    mScriptFadeTimer = 0;
    mScriptFadeTimerMax = 1;
    mStartFadeColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mEndFadeColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

    //--Zones and Triggers
    mTriggerScript = NULL;
    mTriggerList = new StarLinkedList(true);

    //--Enemy Handling
    mHideAllViewcones = false;
    mPlayerCamoTimer = 0;
    mRetreatTimer = 0;
    mEnemySpawnList = new StarLinkedList(true);

    //--Switch Handling
    mSwitchList = new StarLinkedList(true);

    //--Save Points
    mSavePointList = new StarLinkedList(true);

    //--Reinforcement Pulsing
    mIsPulseMode = false;
    mPulseTimer = 0;

    //--Patrol Nodes
    mDisablePatrolRandomization = false;
    mPatrolNodes = new StarLinkedList(true);

    //--Camera Handling
    mIsPositiveCameraMode = false;
    mFirstLock = 0;
    mCameraZoneList = new StarLinkedList(true);

    //--Screen Shaking and Other Effects
    mScreenShakeTimer = 0;
    mShakePeriodicityRoot = 0;
    mShakePeriodicityScatter = 0;
    mShakeSoundList = new StarLinkedList(true);
    mRockFallingFrequency = 0;
    mFallingRockList = new StarLinkedList(true);
    mrFallingRockImageList = new StarLinkedList(false);

    //--Lights
    mAreLightsActive = false;
    mLightsList = new StarLinkedList(true);
    mAmbientLight = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mReuploadStandardLightData = false;
    mReuploadAllLightData = false;
    mLastUploadScaleX = 0.0f;
    mLastUploadScaleY = 0.0f;
    mLastUploadViewportW = 0.0f;
    mLastUploadViewportH = 0.0f;

    //--Underwater
    mIsUnderwaterMode = false;
    mUnderwaterFramebuffer = 0;
    mUnderwaterTexture = 0;
    mUnderwaterDepth = 0;

    //--Player Light
    mPlayerHasLight = false;
    mPlayerLightIsActive = false;
    mPlayerLightDoesNotDrain = false;
    mPlayerLightPower = 0;
    mPlayerLightPowerMax = 3600;
    mPlayerLightObject = NULL;

    //--Objectives Tracker
    mObjectivesList = new StarLinkedList(true);

    //--Dislocated Render Listing
    mDislocatedRenderList = new StarLinkedList(true);

    //--Notices System
    mNotices = new StarLinkedList(true);

    //--Field Abilities
    mCanEditFieldAbilities = true;
    mFieldAbilitiesInCancelMode = false;
    mDontCancelFieldAbilitiesForOneScene = false;
    rExecutingPackage = NULL;
    mFieldAbilityString = new StarlightString();
    mFieldAbilityStringAlt = new StarlightString();
    mExecutingFieldAbilities = new StarLinkedList(true);
    mFieldAbilityObjectList = new StarLinkedList(true);

    ///--Shooting
    mIsShootingActive = false;
    mTennisShooting = false;
    mTennisID = 0;
    mIsFiringSequence = false;
    mShootingCollisionDepth = 0;
    mShootingTimer = 0;
    mShootFiringTimer = 0;
    mExecuteExaminableName = NULL;
    rExecuteActor = NULL;
    mSpawnBulletPuff = false;
    mBulletPuffX = 0.0f;
    mBulletPuffY = 0.0f;
    mBulletPuffZ = 0.0f;
    mShootListOfExaminables = new StarLinkedList(false);
    mShootListOfTilemapActors = new StarLinkedList(false);

    //--Deep Water/Snow/Etc
    mDeepLayers = new StarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Public Variables]
    //--Used for chest opening sequences.
    mLastChestX = 0.0f;
    mLastChestY = 0.0f;

    //--Always re-flip this when loading a new level.
    xEntitiesDrainStamina = false;

    ///--[Image Resolve]
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Utility images.
    Images.Data.rViewconePixel = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/System/System/WhitePixel");
    Images.Data.rUIFont           = rDataLibrary->GetFont("Adventure Level UI");
    Images.Data.rNotificationFont = rDataLibrary->GetFont("Adventure Level Notifications");

    //--Ladder/Rope.
    Images.Data.rLadderTop  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/LadderTop");
    Images.Data.rLadderMid  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/LadderMid");
    Images.Data.rLadderBot  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/LadderBot");
    Images.Data.rRopeAnchor = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeAnchor");
    Images.Data.rRopeTop    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeTop");
    Images.Data.rRopeMid    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeMid");
    Images.Data.rRopeBot    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeBot");

    //--Stamina Bar
    Images.Data.rPlatinumCompass   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/PlatinumCompass");
    Images.Data.rStaminaBarOver    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/StaminaBarOver");
    Images.Data.rStaminaBarMiddle  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/StaminaBarMiddle");
    Images.Data.rStaminaBarUnder   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/StaminaBarUnder");
    Images.Data.rStaminaRingOver   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/StaminaRingOver");
    Images.Data.rStaminaRingUnder  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/StaminaRingUnder");
    Images.Data.rStaminaRingDanger = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/StaminaRingDanger");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    ///--[String Setup]
    mFieldAbilityString->SetString("[IMG0]Field Abilities");
    mFieldAbilityString->AllocateImages(1);
    mFieldAbilityString->SetImageP(0, -3.0f, ControlManager::Fetch()->ResolveControlImage("OpenFieldAbilityMenu"));
    mFieldAbilityString->CrossreferenceImages();

    mFieldAbilityStringAlt->SetString("[IMG0]Edit");
    mFieldAbilityStringAlt->AllocateImages(1);
    mFieldAbilityStringAlt->SetImageP(0, -3.0f, ControlManager::Fetch()->ResolveControlImage("OpenFieldAbilityMenu"));
    mFieldAbilityStringAlt->CrossreferenceImages();

    ///--[Translation]
    //--Strings
    NormalString.str.mTextReceived = InitializeString("Received ");

    //--Run translations.
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_UI);
    if(rTranslation)
    {
        for(int i = 0; i < AL_NORMALTRANS_TOTAL;  i ++) StarTranslation::StringTranslateWorker(NormalString.mem.mPtr[i], rTranslation);
    }

    ///--[White Pixel Handler]
    //--Changes images to white pixels if they did not load. This means that it's not necessary to load these sprites in
    //  order for the level to function. Used for Text Adventure cases, where the room is cross-booting to a TextLevel.
    StarBitmap *rWhitePixel = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/System/System/WhitePixel");
    if(!Images.Data.rLadderTop)        Images.Data.rLadderTop        = rWhitePixel;
    if(!Images.Data.rLadderMid)        Images.Data.rLadderMid        = rWhitePixel;
    if(!Images.Data.rLadderBot)        Images.Data.rLadderBot        = rWhitePixel;
    if(!Images.Data.rRopeAnchor)       Images.Data.rRopeAnchor       = rWhitePixel;
    if(!Images.Data.rRopeTop)          Images.Data.rRopeTop          = rWhitePixel;
    if(!Images.Data.rRopeMid)          Images.Data.rRopeMid          = rWhitePixel;
    if(!Images.Data.rRopeBot)          Images.Data.rRopeBot          = rWhitePixel;
    if(!Images.Data.rStaminaBarOver)   Images.Data.rStaminaBarOver   = rWhitePixel;
    if(!Images.Data.rStaminaBarMiddle) Images.Data.rStaminaBarMiddle = rWhitePixel;
    if(!Images.Data.rStaminaBarUnder)  Images.Data.rStaminaBarUnder  = rWhitePixel;
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    ///--[Blender Listing]
    if(!xHasBuiltListing)
    {
        xHasBuiltListing = true;
        SetMemoryData(__FILE__, __LINE__);
        xListing = (char **)starmemoryalloc(sizeof(char *) * 11);
        xListing[0] = InitializeString("GL_ZERO");
        xListing[1] = InitializeString("GL_ONE");
        xListing[2] = InitializeString("GL_SRC_COLOR");
        xListing[3] = InitializeString("GL_ONE_MINUS_SRC_COLOR");
        xListing[4] = InitializeString("GL_SRC_ALPHA");
        xListing[5] = InitializeString("GL_ONE_MINUS_SRC_ALPHA");
        xListing[6] = InitializeString("GL_DST_ALPHA");
        xListing[7] = InitializeString("GL_ONE_MINUS_DST_ALPHA");
        xListing[8] = InitializeString("GL_DST_COLOR");
        xListing[9] = InitializeString("GL_ONE_MINUS_DST_COLOR");
        xListing[10] = InitializeString("GL_SRC_ALPHA_SATURATE");

        xIntListing[0] = GL_ZERO;
        xIntListing[1] = GL_ONE;
        xIntListing[2] = GL_SRC_COLOR;
        xIntListing[3] = GL_ONE_MINUS_SRC_COLOR;
        xIntListing[4] = GL_SRC_ALPHA;
        xIntListing[5] = GL_ONE_MINUS_SRC_ALPHA;
        xIntListing[6] = GL_DST_ALPHA;
        xIntListing[7] = GL_ONE_MINUS_DST_ALPHA;
        xIntListing[8] = GL_DST_COLOR;
        xIntListing[9] = GL_ONE_MINUS_DST_COLOR;
        xIntListing[10] = GL_SRC_ALPHA_SATURATE;
    }

    ///--[Framebuffer Stuff]
    //--Builds and sets up framebuffers for underwater rendering. Does nothing if the functions didn't
    //  resolve due to an outdated graphics card.
    sglGenFramebuffers(1, &mUnderwaterFramebuffer);
    glGenTextures(1, &mUnderwaterTexture);
    glGenTextures(1, &mUnderwaterDepth);

    //--Make the texture the same size as the screen.
    glBindTexture(GL_TEXTURE_2D, mUnderwaterTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //--Make the texture the same size as the screen.
    glBindTexture(GL_TEXTURE_2D, mUnderwaterDepth);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_ALWAYS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

    //--Set the texture as the target of the framebuffer.
    sglBindFramebuffer(GL_FRAMEBUFFER, mUnderwaterFramebuffer);
    sglFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mUnderwaterTexture, 0);
    sglFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, mUnderwaterDepth, 0);

    //--Clean.
    sglBindFramebuffer(GL_FRAMEBUFFER, 0);

    ///--[Diagnostics]
    DebugPop("AdventureLevel: Construction completed.\n");
}
AdventureLevel::~AdventureLevel()
{
    ///--[Deallocation]
    //--System
    free(mBasePath);

    //--Minigames
    delete mRunningMinigame;
    delete mKPopDanceGame;
    delete mPuzzleFightGame;

    //--Region Notifier
    free(rRegionNotifierAnim);

    //--Notifications
    delete mNotifications;

    //--Ray Collision Engines
    for(int i = 0; i < mRayCollisionEnginesTotal; i ++)
    {
        delete mRayCollisionEngines[i];
    }
    free(mRayCollisionEngines);

    //--Per-map animations.
    delete mPerMapAnimationList;

    //--Debug Handling
    delete mDebugMenu;

    //--Menu
    delete mAdventureMenu;

    //--Object Information
    delete mChestList;
    delete mFakeChestList;
    delete mDoorList;
    delete mExitList;
    delete mInflectionList;
    delete mClimbableList;
    delete mLocationList;
    delete mPositionList;
    delete mInvisibleZoneList;

    //--Catalyst Tone
    //--Examinables
    free(mExaminableScript);
    delete mExaminablesList;

    //--Camera
    //--Backgrounds
    //--Foreground/Background Overlays
    free(mUnderlayPacks);
    free(mOverlayPacks);
    delete mAuraParticleList;

    //--Control Locking/Handling
    delete mFollowEntityIDList;
    delete mControlLockList;

    //--Stamina
    //--Transition Handling
    free(mTransitionDestinationMap);
    free(mTransitionDestinationExit);
    free(mTransitionPostScript);

    //--Transition Fading
    //--Script-Controlled Fading
    //--Zones and Triggers
    free(mTriggerScript);
    delete mTriggerList;

    //--Enemy Handling
    delete mEnemySpawnList;

    //--Switch Handling
    delete mSwitchList;

    //--Save Points
    delete mSavePointList;

    //--Reinforcement Pulsing
    //--Patrol Nodes
    delete mPatrolNodes;

    //--Camera Handling
    delete mCameraZoneList;

    //--Screen Shaking and Other Effects
    delete mShakeSoundList;
    delete mFallingRockList;
    delete mrFallingRockImageList;

    //--Lights
    delete mLightsList;

    //--Underwater
    //--Player Light
    delete mPlayerLightObject;

    //--Objectives Tracker
    delete mObjectivesList;

    //--Dislocated Render Listing
    delete mDislocatedRenderList;

    //--Notices System
    delete mNotices;

    //--Field Abilities
    delete mFieldAbilityString;
    delete mFieldAbilityStringAlt;
    delete mExecutingFieldAbilities;
    delete mFieldAbilityObjectList;

    //--Shooting
    free(mExecuteExaminableName);
    delete mShootListOfExaminables;
    delete mShootListOfTilemapActors;

    //--Deep Water/Snow/Etc
    delete mDeepLayers;

    ///--[OpenGL]
    sglDeleteFramebuffers(1, &mUnderwaterFramebuffer);
    glDeleteTextures(1, &mUnderwaterTexture);
    glDeleteTextures(1, &mUnderwaterDepth);

    ///--[Translation]
    for(int i = 0; i < AL_NORMALTRANS_TOTAL;  i ++) free(NormalString.mem.mPtr[i]);
}

///--[Private Statics]
//--Light boost value, used to help ambient lighting on darker monitors.
float AdventureLevel::xLightBoost = 0.0f;

///--[Public Statics]
//--Creation debug. Used to watch memory for leaks.
bool AdventureLevel::xCreationDebug = false;

//--Global location of the active Adventure Mode scenario.
char *AdventureLevel::xRootPath = NULL;

//--Global location of the script which holds all the item information in the game.
char *AdventureLevel::xItemListPath = NULL;

//--Global location of the script which remaps items to images for some UI parts.
char *AdventureLevel::xItemImageListPath = NULL;

//--Global location of the script which handles opening a Catalyst chest.
char *AdventureLevel::xCatalystHandlerPath = NULL;

//--Flag used by TriggerPacks when updating. The update stops if one reports that it handled the update.
bool AdventureLevel::xTriggerHandledUpdate = false;

//--When a switch is examined, this flag can be queried by the scripts to quickly check if that switch
//  was up or down. Switches can also be manually checked through long-form function calls.
bool AdventureLevel::xIsSwitchUp = false;

//--Switches may or may not handle the update. If a switch did handle the update, this flag should get
//  tripped by the script.
bool AdventureLevel::xExaminationDidSomething = false;

//--Last location the player rested at a fire to save their game. Save points trigger when the menu opens,
//  not when the player actually hits save.
char *AdventureLevel::xLastSavePoint = NULL;

//--The name of the currently playing music. The static function SetMusic() will handle blending.
char *AdventureLevel::xLevelMusic = NULL;

//--Enemies ignore the player when they are of the same form. This is done with the function PulseIgnore().
//  We store the last string this function was called with since enemies can respawn or the room may change
//  and the pulse needs to re-occur.
char *AdventureLevel::xLastIgnorePulse = NULL;

//--Flag used by enemies to indicate how far they are from the player. Only the closest one is stored.
float AdventureLevel::xClosestEnemy = 10000.0f;

//--Used for counting how many catalysts are in the game, total. Debug only.
int AdventureLevel::xCountCatalyst[CATALYST_TOTAL];

//--Remappings. When the program goes to open a level, the remapping list is checked. Level names are simplified
//  so that the map editor is easier to use. The remapping indicates which subdirectory the map is actually in.
//--For example, "TrapBasementA" remaps to "TrapBasement/TrapBasementA/".
//--If a remapping is not found for a given name, the base Maps/ directory is used.
int AdventureLevel::xRemappingsTotal = 0;
char **AdventureLevel::xRemappingsCheck = NULL;
char **AdventureLevel::xRemappingsResult = NULL;

//--This flag gets flipped off every tick. Any enemy that is actively seeking the player flips it back on. When
//  it's on, the player's stamina drains if they run, otherwise they can run forever. QoL feature.
bool AdventureLevel::xEntitiesDrainStamina = false;

//--This flag gets flipped off every tick. If the player is in any invisible zone that is active, enemies will
//  act like they can't see the player for that tick.
bool AdventureLevel::xIsPlayerInInvisZone = false;

//--When levels are transitioning, enemies do not update. When the level is loaded, this flag gets set to true,
//  allowing enemies to update and therefore allowing their patrol paths to be scattered.
bool AdventureLevel::xRunEnemyUpdateForPatrolScatter = false;

//--Stops autosaves from happening on the next attempt.
bool AdventureLevel::xBlockAutosaveOnce = false;

//--Icon path to the DataLibrary. Appears next to the autosave text.
char *AdventureLevel::xAutosaveIconPath = NULL;

//--Debug flag to print extra info when scanning treasures.
bool AdventureLevel::xIsScanningTreasures = false;

//--Path used to call the random level generator.
char *AdventureLevel::xRandomLevelGeneratorPath = NULL;

//--Flag used for shooting. If an examinable should stop the shot, it will flip this variable to true during its pulse.
//  If xRespondToShotImmediately is also flipped, then the cutscene will activate immediately, during the firing animation,
//  as opposed to when the gun is lowered. Normally, it activates after the user lowers their weapon a few ticks later.
//  If xStopFiringImmediately is also true, the firing sequence ends entirely. This is the case if a cutscene needs to
//  handle the firing animation on its own.
bool AdventureLevel::xRespondToShot = false;
bool AdventureLevel::xRespondToShotImmediately = false;
bool AdventureLevel::xStopFiringImmediately = false;

//--When shooting, if set to true, no bullet impact puff will appear.
bool AdventureLevel::xDontSpawnBulletImpact = false;

//--When opening a chest with random contents, a script is called. That script populates this variable with the result.
char *AdventureLevel::xRandomChestResult = NULL;

//--If set to true, disables all mugging of enemies. Used for mods and special sequences.
bool AdventureLevel::xDisableAllMugging = false;

//--Path used in Tennis Shooting mode to spawn tennis balls.
char *AdventureLevel::xSpawnTennisBallPath = NULL;

//--How many ticks the followers follow behind the lead character.
int AdventureLevel::xPartySpacing = FOLLOWER_SPACING_DEF;

//--Script called when a random chest is opened.
char *AdventureLevel::xChestScript = NULL;

//--Script called when an item node is activated.
char *AdventureLevel::xItemNodeScript = NULL;

//--If true, the player cannot open the field ability editor for any reason. Used for mods that disallow editing them.
bool AdventureLevel::xLockoutFieldAbilityEditing = false;

///--[Private Statics]
//--List of all enemies the player has killed. Gets cleared when they visit a save point and rest. Enemies
//  with their unique name on this list do not spawn.
StarLinkedList *AdventureLevel::xDestroyedEnemiesList = new StarLinkedList(false);
StarLinkedList *AdventureLevel::xMuggedEnemiesList = new StarLinkedList(false);
bool AdventureLevel::xHasBuiltListing = false;
char **AdventureLevel::xListing = NULL;
int AdventureLevel::xIntListing[11];

///===================================== Property Queries =========================================
bool AdventureLevel::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVENTURELEVEL) return true;
    if(pType == POINTER_TYPE_TILEDLEVEL) return true;
    if(pType == POINTER_TYPE_ROOTLEVEL) return true;
    return false;
}
bool AdventureLevel::IsWorldStopped()
{
    //--Returns true if something is holding up the world update. Enemies use this to see if they should
    //  wander/chase or should be stopped. It's static for simplicity, as enemies use it as a subroutine:
    //  if the AdventureLevel does not exist, the world can't be stopped. Obviously.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Check sequence.
    if(rActiveLevel->mDebugMenu->IsVisible()) return true;
    if(rActiveLevel->mIsRestSequence) return true;
    if(rActiveLevel->mAdventureMenu->NeedsToBlockControls()) return true;
    if(WorldDialogue::Fetch()->IsManagingUpdates()) return true;
    if(CutsceneManager::Fetch()->HasAnyEvents()) return true;
    return false;
}
bool AdventureLevel::IsWorldStoppedFromCutsceneOnly()
{
    //--If the world is currently stopped, this returns whether or not it's *only* cutscenes stopping it.
    //  Some enemies animate during cutscenes for dramatic effect.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Check sequence.
    if(rActiveLevel->mDebugMenu->IsVisible()) return false;
    if(rActiveLevel->mIsRestSequence) return false;
    if(rActiveLevel->mAdventureMenu->NeedsToBlockControls()) return false;
    if(WorldDialogue::Fetch()->IsManagingUpdates()) return false;
    return true;
}
bool AdventureLevel::CannotOpenMenu()
{
    return mCannotOpenMenu;
}
bool AdventureLevel::IsLevelTransitioning()
{
    return (mIsTransitionIn || mIsTransitionOut);
}
bool AdventureLevel::AreControlsLocked()
{
    return (mControlLockList->GetListSize() > 0);
}
bool AdventureLevel::IsPulsing()
{
    return mIsPulseMode;
}
bool AdventureLevel::GetClipAt(float pX, float pY, float pZ, int pLayer)
{
    //--Entity check.
    if(GetEntityClipAt(pX, pY, pZ)) return true;

    //--Layer range check.
    if(pLayer < 0 || pLayer >= mCollisionLayersTotal) return true;

    //--Compute the tile position to check.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    //--Edge check.
    if(tTileX < 0.0f || tTileY < 0.0f || tTileX >= mCollisionLayers[pLayer].mCollisionSizeX || tTileY >= mCollisionLayers[pLayer].mCollisionSizeY) return true;

    //--Collision hull.
    uint8_t tClipVal = mCollisionLayers[pLayer].mCollisionData[tTileX][tTileY];
    if(tClipVal == CLIP_NONE) return false;
    if(tClipVal == CLIP_ALL)  return true;

    //--Edge cases need a bit more precision. Run this subroutine.
    int tInTileX = (int)fmodf(pX, TileLayer::cxSizePerTile);
    int tInTileY = (int)fmodf(pY, TileLayer::cxSizePerTile);
    return IsClippedWithin(tClipVal, tInTileX, tInTileY);
}
bool AdventureLevel::GetClipAt(float pX, float pY, float pZ)
{
    return GetClipAt(pX, pY, pZ, 0);
}
bool AdventureLevel::GetWorldClipAt(float pX, float pY, float pZ, int pLayer)
{
    //--Returns collision, only taking into account collision layers and ignoring entities.
    if(pLayer < 0 || pLayer >= mCollisionLayersTotal) return true;

    //--Compute the tile position to check.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    //--Edge check.
    if(tTileX < 0.0f || tTileY < 0.0f || tTileX >= mCollisionLayers[pLayer].mCollisionSizeX || tTileY >= mCollisionLayers[pLayer].mCollisionSizeY) return true;

    //--Collision hull.
    uint8_t tClipVal = mCollisionLayers[pLayer].mCollisionData[tTileX][tTileY];
    if(tClipVal == CLIP_NONE) return false;
    if(tClipVal == CLIP_ALL)  return true;

    //--Edge cases need a bit more precision. Run this subroutine.
    int tInTileX = (int)fmodf(pX, TileLayer::cxSizePerTile);
    int tInTileY = (int)fmodf(pY, TileLayer::cxSizePerTile);
    return IsClippedWithin(tClipVal, tInTileX, tInTileY);
}
int AdventureLevel::GetCollision(int pX, int pY, int pZ)
{
    //--Returns the raw collision value, by tile and not pixel.
    if(pZ < 0 || pZ >= mCollisionLayersTotal) return true;
    if(pX < 0 || pY < 0 || pX >= mCollisionLayers[pZ].mCollisionSizeX || pY >= mCollisionLayers[pZ].mCollisionSizeY) return true;
    return mCollisionLayers[pZ].mCollisionData[pX][pY];
}
bool AdventureLevel::GetEntityClipAt(float pX, float pY, float pZ)
{
    //--Setup.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    //--Returns the entity clip information, which is the same for both collision hulls.
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--If the door is open, ignore it.
        if(rDoorPackage->mIsOpened)
        {

        }
        //--Door is closed and a wide door. Can be off by 1 X unit to the right.
        else if(rDoorPackage->mIsWide && (int)(rDoorPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            if((int)(rDoorPackage->mX / TileLayer::cxSizePerTile) == tTileX || (int)(rDoorPackage->mX / TileLayer::cxSizePerTile) + 1 == tTileX)
            {
                mDoorList->PopIterator();
                return true;
            }
        }
        //--Door is closed, narrow, must match the X/Y.
        else if((int)(rDoorPackage->mX / TileLayer::cxSizePerTile) == tTileX && (int)(rDoorPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            mDoorList->PopIterator();
            return true;
        }

        //--Next.
        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }

    //--Chest check. These are always clipped.
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        //--Match.
        if((int)(rChestPackage->mX / TileLayer::cxSizePerTile) == tTileX && (int)(rChestPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            mChestList->PopIterator();
            return true;
        }

        //--Next.
        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--Fake chest check. These are always clipped.
    rChestPackage = (DoorPackage *)mFakeChestList->PushIterator();
    while(rChestPackage)
    {
        //--Match.
        if((int)(rChestPackage->mX / TileLayer::cxSizePerTile) == tTileX && (int)(rChestPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            mFakeChestList->PopIterator();
            return true;
        }

        //--Next.
        rChestPackage = (DoorPackage *)mFakeChestList->AutoIterate();
    }

    ///--[NPCs]
    //--NPC check. All NPCs except ones controlled by the player are clipped by default, and can turn
    //  their collision checker off. They can also reply to multiple positions.
    TilemapActor *rPlayer = LocatePlayerActor();
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Check.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;

            //--On collision:
            if(rActor->IsPositionClipped(pX, pY))
            {
                //--If the player is moving, track this by rubbing the target.
                if(TilemapActor::xIsPlayerCollisionCheck && rPlayer && rActor != rPlayer)
                {
                    //--Compare the ideal facing of the player to the object. If the player is deliberately pushing
                    //  against the object, "rub" it.
                    int tBestFacing = TilemapActor::GetOrthoFacing(rPlayer->GetWorldX(), rPlayer->GetWorldY(), rActor->GetWorldX(), rActor->GetWorldY());
                    if(TilemapActor::xPlayerFacing == tBestFacing) rActor->Rub();
                }

                rEntityList->PopIterator();
                return true;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--Save points.
    SavePointPackage *rSavePackage = (SavePointPackage *)mSavePointList->PushIterator();
    while(rSavePackage)
    {
        //--Position match.
        if(rSavePackage->mX / TileLayer::cxSizePerTile == tTileX && rSavePackage->mY / TileLayer::cxSizePerTile == tTileY)
        {
            mSavePointList->PopIterator();
            return true;
        }

        //--Next.
        rSavePackage = (SavePointPackage *)mSavePointList->AutoIterate();
    }

    //--Climbables. These are only going to stop movement if they are not activated.
    ClimbablePackage *rClimbPackage = (ClimbablePackage *)mClimbableList->PushIterator();
    while(rClimbPackage)
    {
        //--Position match.
        if(!rClimbPackage->mIsActivated && rClimbPackage->mX / TileLayer::cxSizePerTile == tTileX && rClimbPackage->mY / TileLayer::cxSizePerTile == tTileY)
        {
            mClimbableList->PopIterator();
            return true;
        }

        //--Next.
        rClimbPackage = (ClimbablePackage *)mClimbableList->AutoIterate();
    }

    //--No entity hits.
    return false;
}
bool AdventureLevel::GetWanderClipAt(float pX, float pY)
{
    //--Clip routine used by wandering NPCs. They cannot wander into the player, and they can't wander over exits because they
    //  might block the player from using those exits. Enemies are not constrained in this way!

    //--Check the player actor, if it exists.
    RootEntity *rCheckActor = EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(rCheckActor && rCheckActor->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        TilemapActor *rPlayerActor = (TilemapActor *)rCheckActor;
        if(rPlayerActor->IsPositionClippedOverride(pX, pY)) return true;
    }

    //--Can't touch exits.
    ExitPackage *rExitPackage = (ExitPackage *)mExitList->PushIterator();
    while(rExitPackage)
    {
        //--Range check.
        if(IsPointWithin(pX, pY, rExitPackage->mX, rExitPackage->mY, rExitPackage->mX + rExitPackage->mW, rExitPackage->mY + rExitPackage->mH))
        {
            mExitList->PopIterator();
            return true;
        }

        rExitPackage = (ExitPackage *)mExitList->AutoIterate();
    }

    //--All checks passed, no collision.
    return false;
}
bool AdventureLevel::IsClippedWithin(const uint8_t &sClipVal, const int &sInTileX, const int &sInTileY)
{
    //--Inlined function meant to check all the myriad sub-cases of collisions. This checks instances like
    //  triangles, half-clips, curves, and the like.
    const int cHalfSize = TileLayer::cxSizePerTile * 0.5f;

    //--Top-Left edge case.
    if(sClipVal == CLIP_TL)
    {
        return (sInTileX < cHalfSize || sInTileY < cHalfSize);
    }
    //--Top edge case.
    if(sClipVal == CLIP_TOP)
    {
        return (sInTileY < cHalfSize);
    }
    //--Top-Right edge case.
    if(sClipVal == CLIP_TR)
    {
        return (sInTileX >= cHalfSize || sInTileY < cHalfSize);
    }
    //--Diagonal - Top Left - Empty. Identical to the bottom-right version!
    if(sClipVal == CLIP_DTLE || sClipVal == CLIP_DBRE)
    {
        return (sInTileX + sInTileY >= TileLayer::cxSizePerTile - 1 && sInTileX + sInTileY <= TileLayer::cxSizePerTile + 1);
    }
    //--Diagonal - Top Right - Empty. Identical to the bottom-left version!
    if(sClipVal == CLIP_DTRE || sClipVal == CLIP_DBLE)
    {
        return (abs(sInTileX - sInTileY) < 2);
    }
    //--Diagonal - Top Left - Full.
    if(sClipVal == CLIP_DTLF)
    {
        return (sInTileX + sInTileY >= TileLayer::cxSizePerTile);
    }
    //--Diagonal - Top Right - Full.
    if(sClipVal == CLIP_DTRF)
    {
        return (sInTileY >= sInTileX);
    }
    //--Left edge.
    if(sClipVal == CLIP_LFT)
    {
        return (sInTileX < cHalfSize);
    }
    //--Vertical, narrow.
    if(sClipVal == CLIP_NARROWV)
    {
        return (sInTileX < 3 || sInTileX > 13);
    }
    //--Right edge.
    if(sClipVal == CLIP_RGT)
    {
        return (sInTileX >= cHalfSize);
    }
    //--Diagonal - Bottom Left - Full.
    if(sClipVal == CLIP_DBLF)
    {
        return (sInTileY < sInTileX);
    }
    //--Diagonal - Bottom Right - Full.
    if(sClipVal == CLIP_DBRF)
    {
        return (sInTileX + sInTileY < TileLayer::cxSizePerTile);
    }
    //--Bottom left edge.
    if(sClipVal == CLIP_BL)
    {
        return (sInTileX < cHalfSize || sInTileY >= cHalfSize);
    }
    //--Bottom edge.
    if(sClipVal == CLIP_BOT)
    {
        return (sInTileY >= cHalfSize);
    }
    //--Bottom right edge.
    if(sClipVal == CLIP_BR)
    {
        return (sInTileX >= cHalfSize || sInTileY >= cHalfSize);
    }
    //--Left edge, very thing.
    if(sClipVal == CLIP_THIN_LF)
    {
        return (sInTileX < 3);
    }
    //--Right edge.
    if(sClipVal == CLIP_THIN_RT)
    {
        return (sInTileX >= TileLayer::cxSizePerTile - 3);
    }
    //--Quarter, Top Left.
    if(sClipVal == CLIP_QTL)
    {
        return (sInTileX < cHalfSize && sInTileY < cHalfSize);
    }
    //--Quarter, Top Right.
    if(sClipVal == CLIP_QTR)
    {
        return (sInTileX >= cHalfSize && sInTileY < cHalfSize);
    }
    //--Quarter, Bottom Left.
    if(sClipVal == CLIP_QBL)
    {
        return (sInTileX < cHalfSize && sInTileY >= cHalfSize);
    }
    //--Quarter, Bottom Right.
    if(sClipVal == CLIP_QBR)
    {
        return (sInTileX >= cHalfSize && sInTileY >= cHalfSize);
    }

    //--All checks failed. The default return is true.
    return true;
}
float AdventureLevel::GetPlayerStartX()
{
    return mPlayerStartX;
}
float AdventureLevel::GetPlayerStartY()
{
    return mPlayerStartY;
}
float AdventureLevel::GetCameraLft()
{
    return mCameraDimensions.mLft;
}
float AdventureLevel::GetCameraTop()
{
    return mCameraDimensions.mTop;
}
float AdventureLevel::GetDefaultCameraScale()
{
    return TL_DEFAULT_CAMERA_SCALE;
}
float AdventureLevel::GetCameraScale()
{
    return mCurrentScale;
}
bool AdventureLevel::IsCharacterInWorldParty(const char *pName)
{
    //--Error check:
    if(!pName) return false;

    //--Check the player-actor.
    RootEntity *rActor = EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(rActor && rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR) && !strcasecmp(rActor->GetName(), pName))
    {
        return true;
    }

    //--Check the followers.
    uint32_t *rEntityIDPtr = (uint32_t *)mFollowEntityIDList->PushIterator();
    while(rEntityIDPtr)
    {
        //--Get the entity and verify it exists.
        RootEntity *rFollower = EntityManager::Fetch()->GetEntityI(*rEntityIDPtr);
        if(rFollower && rFollower->IsOfType(POINTER_TYPE_TILEMAPACTOR) && !strcasecmp(rFollower->GetName(), pName))
        {
            mFollowEntityIDList->PopIterator();
            return true;
        }

        //--Next.
        rEntityIDPtr = (uint32_t *)mFollowEntityIDList->AutoIterate();
    }

    //--All checks failed, entity is not in the party.
    return false;
}
TwoDimensionReal AdventureLevel::GetPatrolNode(const char *pName)
{
    //--Dummy.
    TwoDimensionReal xDummy;
    if(!pName)
    {
        xDummy.Set(0.0f, 0.0f, 1.0f, 1.0f);
        return xDummy;
    }

    //--If the patrol node exists, return it.
    PatrolNodePack *rObject = (PatrolNodePack *)mPatrolNodes->GetElementByName(pName);
    if(rObject)
    {
        return rObject->mDimensions;
    }

    //--Not found, return the dummy.
    return xDummy;
}
int AdventureLevel::GetPatrolNodeLingerFacing(const char *pName)
{
    PatrolNodePack *rObject = (PatrolNodePack *)mPatrolNodes->GetElementByName(pName);
    if(!rObject) return 0;
    return rObject->mLingerFacing;
}
int AdventureLevel::GetPatrolNodeLingerTicks(const char *pName)
{
    PatrolNodePack *rObject = (PatrolNodePack *)mPatrolNodes->GetElementByName(pName);
    if(!rObject) return 0;
    return rObject->mLingerTicks;
}
void AdventureLevel::GetLocationByName(const char *pName, float &sX, float &sY)
{
    TwoDimensionReal *rLocation = (TwoDimensionReal *)mLocationList->GetElementByName(pName);
    if(rLocation)
    {
        sX = rLocation->mLft;
        sY = rLocation->mTop;
    }
}
bool AdventureLevel::DoesPlayerHaveLightSource()
{
    return (mPlayerHasLight && mPlayerLightIsActive);
}
float AdventureLevel::GetLightBoost()
{
    return xLightBoost;
}
bool AdventureLevel::IsStaminaBarVisible()
{
    //--Don't show the bar if the player's controls are locked.
    if(AreControlsLocked()) return false;
    if(mAdventureMenu->NeedsToBlockControls()) return false;

    //--All checks passed, bar is visible.
    return true;
}
void AdventureLevel::GetLineCollision(int pCollisionLayer, float pXA, float pYA, float pXB, float pYB, float &sReturnX, float &sReturnY)
{
    sReturnX = pXB;
    sReturnY = pYB;
    if(pCollisionLayer < 0 || pCollisionLayer >= mRayCollisionEnginesTotal) return;

    mRayCollisionEngines[pCollisionLayer]->GetShortestIntercept(pXA, pYA, pXB, pYB, sReturnX, sReturnY);
}
bool AdventureLevel::DoesDoorExist(const char *pName)
{
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->GetElementByName(pName);
    if(!rDoorPackage) return false;
    return true;
}
bool AdventureLevel::IsDoorOpen(const char *pName)
{
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->GetElementByName(pName);
    if(!rDoorPackage) return false;

    return rDoorPackage->mIsOpened;
}
bool AdventureLevel::DoesExitExist(const char *pName)
{
    return (mExitList->GetElementByName(pName) != NULL);
}
void AdventureLevel::GetExaminablePos(const char *pName, int &sLft, int &sTop, int &sRgt, int &sBot)
{
    //--Returns the position of the named examinable object, or sets everything to -1 if not found.
    //  These are returned in pixel world-units.
    sLft = -1;
    sTop = -1;
    sRgt = -1;
    sBot = -1;
    if(!pName) return;

    //--Find, set.
    ExaminePackage *rExaminePackage = (ExaminePackage *)mExaminablesList->GetElementByName(pName);
    if(!rExaminePackage) return;
    sLft = rExaminePackage->mX;
    sTop = rExaminePackage->mY;
    sRgt = rExaminePackage->mX + rExaminePackage->mW;
    sBot = rExaminePackage->mY + rExaminePackage->mH;
}
bool AdventureLevel::DoesNoticeExist(void *pPtr)
{
    return mNotices->IsElementOnList(pPtr);
}
void AdventureLevel::BuildObjectListAt(float pWorldX, float pWorldY, bool pAllowExtendedActivate)
{
    ///--[Documentation]
    //--Builds a list of all objects intersecting the given point. This is used so Field Abilities
    //  can "hit" a wide range of objects. It can optionally hit extended-activate positions.
    mFieldAbilityObjectList->ClearList();
    int tTileIntX = (int)pWorldX / TileLayer::cxSizePerTile;
    int tTileIntY = (int)pWorldY / TileLayer::cxSizePerTile;
    TwoDimensionReal tTempDim;

    //--Chest.
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rChestPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rChestPackage->mY / TileLayer::cxSizePerTile);

        //--Hit.
        if(tExamineX == tTileIntX && tExamineY == tTileIntY)
        {
            RegisterObjectToList(mChestList->GetIteratorName(), AL_SPECIAL_TYPE_CHEST);
        }

        //--Next.
        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--Fake Chest.
    DoorPackage *rFakeChestPackage = (DoorPackage *)mFakeChestList->PushIterator();
    while(rFakeChestPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rFakeChestPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rFakeChestPackage->mY / TileLayer::cxSizePerTile);

        //--Hit.
        if(tExamineX == tTileIntX && tExamineY == tTileIntY)
        {
            RegisterObjectToList(mFakeChestList->GetIteratorName(), AL_SPECIAL_TYPE_FAKECHEST);
        }

        //--Next.
        rFakeChestPackage = (DoorPackage *)mFakeChestList->AutoIterate();
    }

    //--Door.
    DoorPackage *rDoorPack = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPack)
    {
        //--Examination position.
        if(!rDoorPack->mIsWide)
        {
            tTempDim.SetWH(rDoorPack->mX, rDoorPack->mY, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        }
        else
        {
            tTempDim.SetWH(rDoorPack->mX, rDoorPack->mY, TileLayer::cxSizePerTile*2, TileLayer::cxSizePerTile);
        }

        //--Hit.
        if(IsPointWithin2DReal(pWorldX, pWorldY, tTempDim))
        {
            RegisterObjectToList(mDoorList->GetIteratorName(), AL_SPECIAL_TYPE_DOOR);
        }

        //--Next.
        rDoorPack = (DoorPackage *)mDoorList->AutoIterate();
    }

    //--Exit.
    ExitPackage *rExitPack = (ExitPackage *)mExitList->PushIterator();
    while(rExitPack)
    {
        //--Hit.
        if(IsPointWithin(pWorldX, pWorldY, rExitPack->mX, rExitPack->mY, rExitPack->mX+rExitPack->mW, rExitPack->mY+rExitPack->mH))
        {
            RegisterObjectToList(mExitList->GetIteratorName(), AL_SPECIAL_TYPE_EXIT);
        }

        //--Next.
        rExitPack = (ExitPackage *)mExitList->AutoIterate();
    }

    //--Location
    TwoDimensionReal *rLocationPack = (TwoDimensionReal *)mLocationList->PushIterator();
    while(rLocationPack)
    {
        //--Hit.
        if(IsPointWithin2DReal(pWorldX, pWorldY, *rLocationPack))
        {
            RegisterObjectToList(mLocationList->GetIteratorName(), AL_SPECIAL_TYPE_LOCATION);
        }

        //--Next.
        rLocationPack = (TwoDimensionReal *)mLocationList->AutoIterate();
    }

    //--Entities.
    EntityManager *rEntityManager = EntityManager::Fetch();
    StarLinkedList *rEntityList = rEntityManager->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Base actors and other types are ignored.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Direct entity.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            float tWorldX = rActor->GetWorldX();
            float tWorldY = rActor->GetWorldY();
            float tDistance = GetPlanarDistance(tWorldX, tWorldY, pWorldX, pWorldY);
            if(tDistance < 10.0f)
            {
                RegisterObjectToList(rCheckEntity->GetName(), AL_SPECIAL_TYPE_ACTOR);
            }
            //--Extended-activate. Optional. Only operates if the first attempt fails.
            else if(pAllowExtendedActivate)
            {
                //--If the extension is -1, this entity doesn't have the flag.
                int tExtendedDir = rActor->GetExtendedActivate();
                if(tExtendedDir != -1)
                {
                    //--Offset.
                    if(tExtendedDir      == TA_DIR_NORTH) tWorldY = tWorldY - 16.0f;
                    else if(tExtendedDir == TA_DIR_SOUTH) tWorldY = tWorldY + 16.0f;
                    else if(tExtendedDir == TA_DIR_WEST)  tWorldX = tWorldX - 16.0f;
                    else if(tExtendedDir == TA_DIR_EAST)  tWorldX = tWorldX + 16.0f;

                    //--Recompute distance.
                    float tDistance = GetPlanarDistance(tWorldX, tWorldY, pWorldX, pWorldY);
                    if(tDistance < 10.0f)
                    {
                        RegisterObjectToList(rCheckEntity->GetName(), AL_SPECIAL_TYPE_ACTOR);
                    }
                }
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--TODO
    /*
    StarLinkedList *mInflectionList; //InflectionPackage *, master
    StarLinkedList *mClimbableList; //ClimbablePackage *, master
    StarLinkedList *mInvisibleZoneList;//InvisZone *, master

//--Types for Local Packages
#define AL_SPECIAL_TYPE_INFLECTION 5
#define AL_SPECIAL_TYPE_CLIMBABLE 6
#define AL_SPECIAL_TYPE_INVISZONE 9
*/

}
void AdventureLevel::BuildObjectListInRangeOf(float pWorldX, float pWorldY, float pRadius)
{
    ///--[Documentation]
    //--Similar to the above function, creates a list of all objects within a radius of the position.
    //  This allows field abilities to "hit" objects in an area.
    mFieldAbilityObjectList->ClearList();
//    int tTileIntX = (int)pWorldX / TileLayer::cxSizePerTile;
//    int tTileIntY = (int)pWorldY / TileLayer::cxSizePerTile;
    TwoDimensionReal tTempDim;

    //--Chest.
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        //--Check distance.
        float cDistance = GetPlanarDistance(pWorldX, pWorldY, rChestPackage->mX, rChestPackage->mY);
        if(cDistance <= pRadius)
        {
            RegisterObjectToList(mChestList->GetIteratorName(), AL_SPECIAL_TYPE_CHEST);
        }

        //--Next.
        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--Fake Chest.
    DoorPackage *rFakeChestPackage = (DoorPackage *)mFakeChestList->PushIterator();
    while(rFakeChestPackage)
    {
        //--Check distance.
        float cDistance = GetPlanarDistance(pWorldX, pWorldY, rFakeChestPackage->mX, rFakeChestPackage->mY);
        if(cDistance <= pRadius)
        {
            RegisterObjectToList(mFakeChestList->GetIteratorName(), AL_SPECIAL_TYPE_FAKECHEST);
        }

        //--Next.
        rFakeChestPackage = (DoorPackage *)mFakeChestList->AutoIterate();
    }

    //--Door.
    DoorPackage *rDoorPack = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPack)
    {
        //--Examination position.
        if(!rDoorPack->mIsWide)
        {
            tTempDim.SetWH(rDoorPack->mX, rDoorPack->mY, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        }
        else
        {
            tTempDim.SetWH(rDoorPack->mX, rDoorPack->mY, TileLayer::cxSizePerTile*2, TileLayer::cxSizePerTile);
        }

        //--Use the center.
        float cDistance = GetPlanarDistance(pWorldX, pWorldY, tTempDim.mXCenter, tTempDim.mYCenter);
        if(cDistance <= pRadius)
        {
            RegisterObjectToList(mDoorList->GetIteratorName(), AL_SPECIAL_TYPE_DOOR);
        }

        //--Next.
        rDoorPack = (DoorPackage *)mDoorList->AutoIterate();
    }

    //--Exit.
    ExitPackage *rExitPack = (ExitPackage *)mExitList->PushIterator();
    while(rExitPack)
    {
        //--Check distance.
        float cDistance = GetPlanarDistance(pWorldX, pWorldY, rExitPack->mX+(rExitPack->mW*0.50f), rExitPack->mY+(rExitPack->mH*0.50f));
        if(cDistance <= pRadius)
        {
            RegisterObjectToList(mExitList->GetIteratorName(), AL_SPECIAL_TYPE_EXIT);
        }

        //--Next.
        rExitPack = (ExitPackage *)mExitList->AutoIterate();
    }

    //--Location
    TwoDimensionReal *rLocationPack = (TwoDimensionReal *)mLocationList->PushIterator();
    while(rLocationPack)
    {
        //--Check distance.
        float cDistance = GetPlanarDistance(pWorldX, pWorldY, rLocationPack->mXCenter, rLocationPack->mYCenter);
        if(cDistance <= pRadius)
        {
            RegisterObjectToList(mLocationList->GetIteratorName(), AL_SPECIAL_TYPE_LOCATION);
        }

        //--Next.
        rLocationPack = (TwoDimensionReal *)mLocationList->AutoIterate();
    }

    //--Entities.
    EntityManager *rEntityManager = EntityManager::Fetch();
    StarLinkedList *rEntityList = rEntityManager->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Base actors and other types are ignored.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Direct entity.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            float tWorldX = rActor->GetWorldX();
            float tWorldY = rActor->GetWorldY();
            float tDistance = GetPlanarDistance(tWorldX, tWorldY, pWorldX, pWorldY);
            if(tDistance < pRadius)
            {
                RegisterObjectToList(rCheckEntity->GetName(), AL_SPECIAL_TYPE_ACTOR);
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::RegisterObjectToList(const char *pName, int pType)
{
    if(!pName || !pType) return;
    FieldAbilityObjectPack *nObjectPack = (FieldAbilityObjectPack *)starmemoryalloc(sizeof(FieldAbilityObjectPack));
    nObjectPack->Initialize();
    nObjectPack->mType = pType;
    strncpy(nObjectPack->mName, pName, 127);
    mFieldAbilityObjectList->AddElement("X", nObjectPack, &FreeThis);
}
int AdventureLevel::GetNumberOfObjectsAt()
{
    return mFieldAbilityObjectList->GetListSize();
}
int AdventureLevel::GetObjectCodeAt(int pSlot)
{
    FieldAbilityObjectPack *rPackage = (FieldAbilityObjectPack *)mFieldAbilityObjectList->GetElementBySlot(pSlot);
    if(!rPackage) return AL_SPECIAL_TYPE_FAIL;
    return rPackage->mType;
}
const char *AdventureLevel::GetObjectNameAt(int pSlot)
{
    FieldAbilityObjectPack *rPackage = (FieldAbilityObjectPack *)mFieldAbilityObjectList->GetElementBySlot(pSlot);
    if(!rPackage) return NULL;
    return rPackage->mName;
}
int AdventureLevel::GetRetreatTimer()
{
    return mRetreatTimer;
}
int AdventureLevel::GetFollowersTotal()
{
    return mFollowEntityIDList->GetListSize();
}
bool AdventureLevel::EnemiesCanAcquireTargets()
{
    return (mEnemyAICycles > 15);
}
int AdventureLevel::GetPlayerCamoTimer()
{
    return mPlayerCamoTimer;
}
bool AdventureLevel::DoesChestExist(const char *pName)
{
    return (mChestList->GetElementByName(pName) != NULL);
}

///======================================== Manipulators ==========================================
void AdventureLevel::SetName(const char *pName)
{
    ///--[Documentation]
    //--Basics.
    if(!pName) return;
    ResetString(mName, pName);

    //--Random levels never do this.
    if(!strcasecmp(pName, "RandomLevel"))
    {
        mIsRandomLevel = true;
        return;
    }

    ///--[Spawn Packages]
    //--Setup.
    char tSaveBuf[128];

    //--Go through the spawn packages and update all the names to use the new room name. This should
    //  only be done once!
    SpawnPackage *rPackage = (SpawnPackage *)mEnemySpawnList->PushIterator();
    while(rPackage)
    {
        strcpy(tSaveBuf, rPackage->mUniqueSpawnName);
        sprintf(rPackage->mUniqueSpawnName, "STD|%s|%s", mName, tSaveBuf);
        rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
    }

    ///--[Catalyst Beep]
    //--Check if the player has the Platinum Compass.
    bool tHasCompass = false;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rPlatinumCompassVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/CatalystTone/iHasCatalystTone");
    if(rPlatinumCompassVar && rPlatinumCompassVar->mNumeric == 1.0f)
    {
        tHasCompass = true;
    }

    ///--[Chest Verification]
    //--Go through the chest listing. Now that we know the name of the level, we can specify whether
    //  the chests are open or not based on if a matching variable exists.
    bool tHasAnyCatalysts = false;

    //--Iterate.
    DoorPackage *rChest = (DoorPackage *)mChestList->PushIterator();
    while(rChest)
    {
        //--Check the VM. If a variable exists, this chest starts in the opened position and ignores open attempts.
        char tBuffer[128];
        sprintf(tBuffer, "Root/Variables/Chests/%s/%s", mName, mChestList->GetIteratorName());
        SysVar *rCheckPtr = (SysVar *)rDataLibrary->GetEntry(tBuffer);

        //--If the chest has been opened at least once, it will have a variable in the datalibrary. This doesn't mean
        //  it doesn't have anything in it!
        if(rCheckPtr)
        {
            //--Get the numerical value of the chest. If it's 0.0f, this is a normal chest. It is open.
            if(rCheckPtr->mNumeric == AL_CHEST_OPEN_NORMAL)
            {
                rChest->mIsOpened = true;
            }
            //--If the value is 1.0f, the chest is respawnable, but has respawned.
            else if(rCheckPtr->mNumeric == AL_CHEST_CLOSED_RESPAWN)
            {
                rChest->rRenderClosed = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestGC");
                rChest->rRenderOpen   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestGO");
            }
            //--If the value is 2.0f, then it is a respawned chest and is open.
            else if(rCheckPtr->mNumeric == AL_CHEST_OPEN_RESPAWN)
            {
                rChest->mIsOpened = true;
                rChest->rRenderClosed = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestGC");
                rChest->rRenderOpen   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestGO");
            }
            //--The special AL_CHEST_OPEN_NORMAL_PENDING value makes the chest appear brown, but next time the player
            //  rests it will change to a green chest.
            else if(rCheckPtr->mNumeric == AL_CHEST_OPEN_NORMAL_PENDING)
            {
                rChest->mIsOpened = true;
            }
        }
        //--If the chest is not opened, and it's a catalyst, flag the catalyst case if the player has the Platinum Compass.
        else if(tHasCompass && !strncasecmp(rChest->mContents, "CATALYST|", 9))
        {
            FlagCatalystTone();
        }

        //--Debug: If there is a catalyst at all, set this flag.
        if(xIsScanningTreasures && !strncasecmp(rChest->mContents, "CATALYST|", 9))
        {
            tHasAnyCatalysts = true;
        }

        //--Next.
        rChest = (DoorPackage *)mChestList->AutoIterate();
    }

    ///--[Debug]
    //--Show catalyst information in the console if scanning for treasures. Only used for debug, so we don't care if it's
    //  programmatically expensive.
    MapManager *rMapManager = MapManager::Fetch();
    if(tHasAnyCatalysts && xIsScanningTreasures)
    {
        //--Strip thw

        //--Heading.
        fprintf(stderr, "%s:\n", mName);

        //--Iterate.
        DoorPackage *rChest = (DoorPackage *)mChestList->PushIterator();
        while(rChest)
        {
            //--Catalyst, show its type.
            if(!strncasecmp(rChest->mContents, "CATALYST|", 9))
            {
                //--Catalyst information.
                fprintf(stderr, " %s %s", mChestList->GetIteratorName(), rChest->mContents);

                //--Check the map manager to see if this catalyst is registered.
                const char *rChapterName = rMapManager->IsCatalystRegistered(mName, mChestList->GetIteratorName(), rChest->mContents);
                if(!strcasecmp(rChapterName, "Null"))
                {
                    fprintf(stderr, " - warning, not a registered catalyst.\n");
                }
                else
                {
                    fprintf(stderr, " - %s.\n", rChapterName);
                }
            }


            //--Next.
            rChest = (DoorPackage *)mChestList->AutoIterate();
        }
    }
}
void AdventureLevel::RecheckCompass()
{
    ///--[Documentation]
    //--If the player is in a room and picks up a catalyst, run this routine to check if the platinum compass indicator
    //  should be removed. This is not checked when the room's name is first set, only if the player picks up a catalyst
    //  or otherwise a script calls for it.
    mShowPlatinumCompass = false;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Compass Check]
    //--Check if the player has the Platinum Compass. If not, just leave the flag false.
    SysVar *rPlatinumCompassVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/CatalystTone/iHasCatalystTone");
    if(!rPlatinumCompassVar || rPlatinumCompassVar->mNumeric == 0.0f)
    {
        return;
    }

    ///--[Chest Verification]
    //--Go through the chest listing. If any unopened chests contain a catalyst, the marker is set to true.
    DoorPackage *rChest = (DoorPackage *)mChestList->PushIterator();
    while(rChest)
    {
        //--Check the VM. If a variable exists, this chest starts in the opened position and ignores open attempts.
        char tBuffer[128];
        sprintf(tBuffer, "Root/Variables/Chests/%s/%s", mName, mChestList->GetIteratorName());
        SysVar *rCheckPtr = (SysVar *)rDataLibrary->GetEntry(tBuffer);

        //--If the chest has been opened at least once, it will have a variable in the datalibrary. This doesn't mean
        //  it doesn't have anything in it, but it will never have a catalyst.
        if(rCheckPtr)
        {
        }
        //--If the chest is not opened, and it's a catalyst, flag the catalyst case if the player has the Platinum Compass.
        //  There is no need to continue iterating, any one catalyst leaves the flag on.
        else if(!strncasecmp(rChest->mContents, "CATALYST|", 9))
        {
            mShowPlatinumCompass = true;
            mChestList->PopIterator();
            return;
        }

        //--Next.
        rChest = (DoorPackage *)mChestList->AutoIterate();
    }

    ///--[Debug]
    //--If we got this far, no unopened chests have catalysts.
}
void AdventureLevel::SetBasePath(const char *pPath)
{
    ResetString(mBasePath, pPath);
}
void AdventureLevel::SetControlEntityID(uint32_t pID)
{
    //--If the entity ID is not the current ID, locate the current entity and unflag it as
    //  a party member. This mostly affects lighting.
    EntityManager *rEntityManager = EntityManager::Fetch();
    if(pID != mControlEntityID && mControlEntityID != 0)
    {
        //--If the Actor exists, unset their flag.
        RootEntity *rEntity = rEntityManager->GetEntityI(mControlEntityID);
        if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rEntity;
            rActor->SetPartyEntityFlag(false);
        }
    }

    //--Note: Set to 0 to disable player NPC control.
    mControlEntityID = pID;

    //--Locate the actor and set them as a party member.
    RootEntity *rEntity = rEntityManager->GetEntityI(mControlEntityID);
    if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        TilemapActor *rActor = (TilemapActor *)rEntity;
        rActor->SetPartyEntityFlag(true);
    }
}
void AdventureLevel::AddFollowEntityID(uint32_t pID)
{
    //--Baseline.
    SetMemoryData(__FILE__, __LINE__);
    uint32_t *nPtr = (uint32_t *)starmemoryalloc(sizeof(uint32_t));
    *nPtr = pID;
    mFollowEntityIDList->AddElementAsTail("X", nPtr, &FreeThis);

    //--If the Actor exists, set their following flag. This is used for some rendering.
    RootEntity *rEntity = EntityManager::Fetch()->GetEntityI(pID);
    if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        //--Cast, set.
        TilemapActor *rActor = (TilemapActor *)rEntity;
        rActor->SetPartyEntityFlag(true);
    }
}
void AdventureLevel::RemoveFollowEntityName(const char *pName)
{
    //--Try to find the named NPC on the follower list and remove them from it if found.
    if(!pName) return;
    EntityManager *rEntityManager = EntityManager::Fetch();

    //--Iterate.
    uint32_t *rPtr = (uint32_t *)mFollowEntityIDList->SetToHeadAndReturn();
    while(rPtr)
    {
        //--Find the associated entity. Must be a TilemapActor.
        RootEntity *rEntity = rEntityManager->GetEntityI(*rPtr);
        if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast, check the name.
            TilemapActor *rActor = (TilemapActor *)rEntity;
            if(!strcasecmp(rActor->GetName(), pName))
            {
                rActor->SetPartyEntityFlag(false);
                mFollowEntityIDList->RemoveRandomPointerEntry();
                return;
            }
        }

        //--Next.
        rPtr = (uint32_t *)mFollowEntityIDList->IncrementAndGetRandomPointerEntry();
    }
}
void AdventureLevel::AddLockout(const char *pName)
{
    //--Check if the lockout already exists. If it does, don't re-add it.
    if(mControlLockList->GetElementByName(pName)) return;

    //--Otherwise, add it with a dummy pointer.
    static int xDummyValue = 0;
    mControlLockList->AddElement(pName, &xDummyValue);
}
void AdventureLevel::RemoveLockout(const char *pName)
{
    //--Special: If the name is "ALL" or "CLEAR" then wipe the list.
    if(pName && (!strcasecmp(pName, "ALL") || !strcasecmp(pName, "CLEAR")))
    {
        mControlLockList->ClearList();
        return;
    }

    //--Remove it from the list.
    mControlLockList->RemoveElementS(pName);
}
void AdventureLevel::SetExaminationScript(const char *pPath)
{
    ResetString(mExaminableScript, pPath);
}
void AdventureLevel::AddExaminable(int pX, int pY, const char *pFiringString)
{
    //--Overload, assumes examinable is 1x1.
    AddExaminable(pX, pY, 1, 1, 0, pFiringString);
}
void AdventureLevel::AddExaminable(int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pFiringString)
{
    //--Adds a new ExaminablePack to the list, or, if the pack already exists, modifies its position.
    //  Note that the name of the firing string is the name of the object on the list, it is not
    //  stored independently of the list.
    //--It is legal to register the same firing string twice, this means that the same "object" would
    //  have multiple positions that can be examined.
    SetMemoryData(__FILE__, __LINE__);
    ExaminePackage *nPackage = (ExaminePackage *)starmemoryalloc(sizeof(ExaminePackage));
    nPackage->Initialize();
    nPackage->mX = pX;
    nPackage->mY = pY;
    nPackage->mW = pW;
    nPackage->mH = pH;
    nPackage->mFlags = pFlags;
    mExaminablesList->AddElement(pFiringString, nPackage, &FreeThis);
}
void AdventureLevel::AddExaminableTransition(const char *pName, int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pRoomDest, const char *pPosDest, const char *pPlaySound)
{
    //--Adds a new ExaminablePack to the list, as above. This one is pre-built to be a room transition.
    //  Room transitions can still occur in the examination file if needed, this just allows automation.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    ExaminePackage *nPackage = (ExaminePackage *)starmemoryalloc(sizeof(ExaminePackage));
    nPackage->Initialize();
    nPackage->mX = pX;
    nPackage->mY = pY;
    nPackage->mW = pW;
    nPackage->mH = pH;
    nPackage->mFlags = pFlags;
    mExaminablesList->AddElement(pName, nPackage, &FreeThis);

    //--Transition flags.
    if(!pRoomDest || !pPosDest || !pPlaySound) return;
    nPackage->mIsAutoTransition = true;
    strncpy(nPackage->mAutoRoomDest,     pRoomDest,  STD_MAX_LETTERS);
    strncpy(nPackage->mAutoLocationDest, pPosDest,   STD_MAX_LETTERS);
    strncpy(nPackage->mAutoSound,        pPlaySound, STD_MAX_LETTERS);
}
void AdventureLevel::AddPatrolNode(const char *pName, int pX, int pY, int pW, int pH)
{
    //--Adds a patrol node. Used by enemies to get around.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    PatrolNodePack *nZone = (PatrolNodePack *)starmemoryalloc(sizeof(PatrolNodePack));
    nZone->Initialize();
    nZone->mDimensions.SetWH(pX, pY, pW, pH);
    mPatrolNodes->AddElement(pName, nZone, &FreeThis);
}
void AdventureLevel::AddEnemy(const char *pName, int pX, int pY, const char *pParty, const char *pAppearance, const char *pScene, int pToughness, const char *pPatrolPath, const char *pFollow)
{
    ///--[Documentation]
    //--Adds an enemy spawn package. This is normally done via the map object loader, but this function is used
    //  when enemy spawns are generated via scripts. Note that this generates the spawn package, NOT the enemy.
    //  When randomly generating levels, all spawn packages always spawn.
    if(!pName || !pParty || !pAppearance || !pScene || !pPatrolPath || !pFollow) return;

    ///--[Execution]
    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    SpawnPackage *nPackage = (SpawnPackage *)starmemoryalloc(sizeof(SpawnPackage));
    nPackage->Initialize();
    strncpy(nPackage->mRawName, pName, STD_MAX_LETTERS);

    //--Position.
    nPackage->mDimensions.SetWH(pX, pY, 16.0f, 16.0f);

    //--Properties.
    strcpy(nPackage->mPartyEnemy, pParty);
    strcpy(nPackage->mAppearancePath, pAppearance);
    strcpy(nPackage->mLoseCutscene, pScene);
    nPackage->mToughness = pToughness;
    nPackage->mDontRefaceFlag = true;
    strcpy(nPackage->mPatrolPathway, pPatrolPath);
    strcpy(nPackage->mFollowTarget, pFollow);
    sprintf(nPackage->mUniqueSpawnName, "%s", pName);
    RegisterEnemyPack(nPackage);
}
void AdventureLevel::AddChest(const char *pName, int pX, int pY, bool pIsFuture, const char *pContents)
{
    ///--[Documentation]
    //--Spawns a chest. Usually used for the random level generator. Chests spawned in this manner
    //  are never already opened and are not saved to the variable listing.
    if(!pName || !pContents) return;

    //--Create a new package.
    SetMemoryData(__FILE__, __LINE__);
    DoorPackage *nChestPack = (DoorPackage *)starmemoryalloc(sizeof(DoorPackage));
    nChestPack->Initialize();

    //--Copy position data.
    nChestPack->mX = pX;
    nChestPack->mY = pY;

    //--Set images.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    nChestPack->rRenderClosed = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestC");
    nChestPack->rRenderOpen   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestO");
    if(pIsFuture)
    {
        nChestPack->rRenderClosed = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFC");
        nChestPack->rRenderOpen   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFO");
    }

    //--Properties.
    ResetString(nChestPack->mContents, pContents);

    //--Register.
    mChestList->AddElement(pName, nChestPack, &FreeThis);
}
void AdventureLevel::SetChestContents(const char *pName, const char *pContents)
{
    ///--[Documentation]
    //--Sets contents for a chest, assuming the chest exists.
    if(!pName || !pContents) return;

    ///--[Execution]
    //--Locate.
    DoorPackage *rChestPack = (DoorPackage *)mChestList->GetElementByName(pName);
    if(!rChestPack) return;

    //--Set.
    ResetString(rChestPack->mContents, pContents);
}
void AdventureLevel::SetTransitionDestination(const char *pMapDestination, const char *pExitDestination)
{
    ResetString(mTransitionDestinationMap, pMapDestination);
    ResetString(mTransitionDestinationExit, pExitDestination);
}
void AdventureLevel::SetTransitionPostExec(const char *pScriptPath)
{
    //--Optional, sets the script that fires immediately after a transition is completed. Usually used for cutscenes.
    if(!pScriptPath || !strcasecmp(pScriptPath, "Null"))
    {
        ResetString(mTransitionPostScript, NULL);
        return;
    }
    ResetString(mTransitionPostScript, pScriptPath);
}
void AdventureLevel::SetTransitionPosition(int pDirection, float pPercentageOffset)
{
    mTransitionDirection = pDirection;
    mTransitionPercentage = pPercentageOffset;
}
void AdventureLevel::OpenDoor(const char *pName)
{
    //--Manually opens a door, usually called by scripts. Does not inherently play any sounds.
    DoorPackage *rDoor = (DoorPackage *)mDoorList->GetElementByName(pName);
    if(!rDoor) return;

    //--Flag as opened.
    rDoor->mIsOpened = true;

    //--Remove it from the RCEs.
    int tDoorNumber = mDoorList->GetSlotOfElementByPtr(rDoor);
    char tNameBuffers[4][STD_MAX_LETTERS];
    for(int i = 0; i < 4; i ++)
    {
        sprintf(tNameBuffers[i], "%i|%i", tDoorNumber, i);
        for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
        {
            if(!mRayCollisionEngines[p]) continue;
            mRayCollisionEngines[p]->UnsetVariableLine(tNameBuffers[i]);
        }
    }
}
void AdventureLevel::CloseDoor(const char *pName)
{
    //--Manually closes a door.
    DoorPackage *rDoor = (DoorPackage *)mDoorList->GetElementByName(pName);
    if(!rDoor) return;

    //--Flag as opened.
    rDoor->mIsOpened = false;

    //--Remove it from the RCEs.
    int tDoorNumber = mDoorList->GetSlotOfElementByPtr(rDoor);
    char tNameBuffers[4][STD_MAX_LETTERS];
    for(int i = 0; i < 4; i ++)
    {
        sprintf(tNameBuffers[i], "%i|%i", tDoorNumber, i);
        for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
        {
            if(!mRayCollisionEngines[p]) continue;
            mRayCollisionEngines[p]->ResetVariableLine(tNameBuffers[i]);
        }
    }
}
void AdventureLevel::SetSwitchState(const char *pName, bool pIsUp)
{
    DoorPackage *rSwitchPackage = (DoorPackage *)mSwitchList->GetElementByName(pName);
    if(rSwitchPackage)
    {
        rSwitchPackage->mIsOpened = pIsUp;
    }
}
void AdventureLevel::SetReinforcementPulseMode(bool pFlag)
{
    //--Flags.
    mIsPulseMode = pFlag;
    mPulseTimer = 0;

    //--When activating, all active field abilities run their touch-enemy code.
    if(mIsPulseMode)
    {
        //--Flag abilities to enter cancel mode.
        mFieldAbilitiesInCancelMode = true;

        //--Iterate across the active packs.
        FieldAbilityPack *rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->PushIterator();
        while(rFieldAbilityPack)
        {
            //--Mark the global package. This is the one referenced in lua scripts.
            rExecutingPackage = rFieldAbilityPack;

            //--Run the field ability touch-enemy code.
            if(rFieldAbilityPack->rParentAbility)
            {
                rFieldAbilityPack->rParentAbility->Execute(FIELDABILITY_EXEC_TOUCHENEMY);
            }

            //--Next.
            rExecutingPackage = NULL;
            rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->AutoIterate();
        }
    }
}
void AdventureLevel::SetCanEditFieldAbilities(bool pFlag)
{
    mCanEditFieldAbilities = pFlag;
}
void AdventureLevel::RunFieldAbilitiesOnActor(TilemapActor *pActor)
{
    //--Runs all active field abilities on the given actor, allowing them to modify its internal state.
    if(!pActor) return;

    //--Iterate across the active packs.
    FieldAbilityPack *rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->PushIterator();
    while(rFieldAbilityPack)
    {
        //--Mark the global package. This is the one referenced in lua scripts.
        rExecutingPackage = rFieldAbilityPack;

        //--Run the field ability touch-enemy code.
        if(rFieldAbilityPack->rParentAbility)
        {
            rFieldAbilityPack->rParentAbility->ExecuteOn(FIELDABILITY_EXEC_MODIFYACTOR, pActor);
        }

        //--Next.
        rExecutingPackage = NULL;
        rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->AutoIterate();
    }
}
void AdventureLevel::SetDontCancelFieldAbilitiesOnceFlag(bool pFlag)
{
    mDontCancelFieldAbilitiesForOneScene = pFlag;
}
void AdventureLevel::BeginRestSequence()
{
    mIsRestSequence = true;
    mRestingTimer = 0;
}
void AdventureLevel::EndRestSequenceNow()
{
    mIsRestSequence = false;
    mRestingTimer = 0;
}
void AdventureLevel::SetBackground(const char *pPath)
{
    rBackgroundImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdventureLevel::SetBackgroundPositions(float pX, float pY)
{
    mBackgroundStartX = pX;
    mBackgroundStartY = pY;
}
void AdventureLevel::SetBackgroundScrolls(float pX, float pY)
{
    mBackgroundScrollX = pX;
    mBackgroundScrollY = pY;
}
void AdventureLevel::FlagCatalystTone()
{
    mPlayCatalystTone = true;
    mShowPlatinumCompass = true;
    mCatalystToneCountdown = 15;
}
void AdventureLevel::SetLightBoost(float pValue)
{
    //--Set.
    xLightBoost = pValue;
    if(xLightBoost < 0.0f) xLightBoost = 0.0f;
    if(xLightBoost > 1.0f) xLightBoost = 1.0f;

    //--Store this in the DataLibrary.
    SysVar *rSystemVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/fAmbientLightBoost");
    if(rSystemVariable)
    {
        rSystemVariable->mNumeric = xLightBoost;
    }
}
void AdventureLevel::SetRenderingDisabled(const char *pLayerName, bool pFlag)
{
    TileLayer *rLayer = (TileLayer *)mTileLayers->GetElementByName(pLayerName);
    if(!rLayer) return;
    rLayer->SetDoNotRenderFlag(pFlag);
}
void AdventureLevel::SetAnimationDisabled(const char *pLayerName, bool pFlag)
{
    TileLayer *rLayer = (TileLayer *)mTileLayers->GetElementByName(pLayerName);
    if(!rLayer) return;
    rLayer->SetAnimationDisable(pFlag);
}
void AdventureLevel::SetCollision(int pX, int pY, int pZ, int pCollisionValue)
{
    //--Manually modifies a collision. Used for cutscenes, doesn't affect the Ray Collision Engine.
    //  pX and pY are in tiles, not pixels.
    if(pZ < 0 || pZ >= mCollisionLayersTotal) return;

    //--Edge check.
    if(pX < 0 || pY < 0 || pX >= mCollisionLayers[pZ].mCollisionSizeX || pY >= mCollisionLayers[pZ].mCollisionSizeY) return;

    //--Collision hull.
    mCollisionLayers[pZ].mCollisionData[pX][pY] = pCollisionValue;
}

///--[Objectives]
void AdventureLevel::RegisterObjective(const char *pName)
{
    ///--[Documentation]
    //--Registers a new objective with the given name. The display name is identical to the objective name
    //  but can be changed. Objectives default to incomplete when allocated. Objectives do not carry over
    //  between rooms and must be reset in the constructor.
    if(!pName) return;

    ///--[Allocate]
    SetMemoryData(__FILE__, __LINE__);
    ObjectivePack *nObjective = (ObjectivePack *)starmemoryalloc(sizeof(ObjectivePack));
    nObjective->Initialize();
    nObjective->mIsComplete = false;
    ResetString(nObjective->mDisplayName, pName);

    ///--[Register]
    mObjectivesList->AddElement(pName, nObjective, &ObjectivePack::DeleteThis);
}
void AdventureLevel::SetObjectiveDisplayName(const char *pName, const char *pDisplayName)
{
    ///--[Documentation]
    ObjectivePack *rObjectivePtr = (ObjectivePack *)mObjectivesList->GetElementByName(pName);
    if(!pName || !rObjectivePtr || !pDisplayName)
    {
        fprintf(stderr, "AdventureLevel:SetObjectiveDisplayName() - Warning, objective %s was not found, or display name %s was NULL. Failing.\n", pName, pDisplayName);
        return;
    }

    ///--[Set]
    ResetString(rObjectivePtr->mDisplayName, pDisplayName);
}
void AdventureLevel::FlagObjectiveIncomplete(const char *pName)
{
    ///--[Documentation]
    //--Marks the named objective as incomplete, if it exists. Prints an error if the objective is not found.
    ObjectivePack *rObjectivePtr = (ObjectivePack *)mObjectivesList->GetElementByName(pName);
    if(!pName || !rObjectivePtr)
    {
        fprintf(stderr, "AdventureLevel:FlagObjectiveIncomplete() - Warning, objective %s was not found. Failing.\n", pName);
        return;
    }

    ///--[Set]
    rObjectivePtr->mIsComplete = false;
}
void AdventureLevel::FlagObjectiveComplete(const char *pName)
{
    ///--[Documentation]
    //--Marks the named objective as complete, if it exists. Prints an error if the objective is not found.
    ObjectivePack *rObjectivePtr = (ObjectivePack *)mObjectivesList->GetElementByName(pName);
    if(!pName || !rObjectivePtr)
    {
        fprintf(stderr, "AdventureLevel:FlagObjectiveComplete() - Warning, objective %s was not found. Failing.\n", pName);
        return;
    }

    ///--[Set]
    rObjectivePtr->mIsComplete = true;
}
void AdventureLevel::ClearObjectives()
{
    ///--[Documentation]
    //--Clears the objective list.
    mObjectivesList->ClearList();
}

///--[Dislocations]
void AdventureLevel::RegisterDislocationPack(const char *pName, const char *pLayer, int pXStart, int pYStart, int pWid, int pHei, float pXTarget, float pYTarget)
{
    if(!pName || !pLayer || pWid < 1 || pHei < 1) return;

    SetMemoryData(__FILE__, __LINE__);
    DislocatedRenderPackage *nPackage = (DislocatedRenderPackage *)starmemoryalloc(sizeof(DislocatedRenderPackage));
    nPackage->Initialize();
    strcpy(nPackage->mLayerName, pLayer);
    nPackage->mOrigXStart = pXStart;
    nPackage->mOrigYStart = pYStart;
    nPackage->mWidth = pWid;
    nPackage->mHeight = pHei;
    nPackage->mNewXRender = pXTarget;
    nPackage->mNewYRender = pYTarget;
    mDislocatedRenderList->AddElement(pName, nPackage, &FreeThis);
}
void AdventureLevel::ModifyDislocationPack(const char *pName, float pXTarget, float pYTarget)
{
    DislocatedRenderPackage *rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->GetElementByName(pName);
    if(!rPackage) return;
    rPackage->mNewXRender = pXTarget;
    rPackage->mNewYRender = pYTarget;
}
void AdventureLevel::SetMajorAnimationMode(const char *pAnimationName)
{
    //--Activates major animation mode, which shows a single animation front-and-center above the dialogue box with
    //  the environment greyed out. Pass NULL to clear the mode.
    //--If the named animation is not found, the mode is also disabled.
    mIsMajorAnimationMode = false;
    mMajorAnimationName[0] = '\0';
    if(!pAnimationName) return;

    //--Make sure the animation exists.
    void *rCheckPtr = mPerMapAnimationList->GetElementByName(pAnimationName);
    if(!rCheckPtr) return;

    //--Set flags.
    mIsMajorAnimationMode = true;
    strncpy(mMajorAnimationName, pAnimationName, sizeof(char) * (STD_MAX_LETTERS - 1));
}
void AdventureLevel::AddAnimation(const char *pName, float pX, float pY, float pZ)
{
    //--Adds a Per-Map Animation, which use tilesets to create complicated animations that can be displayed on the screen.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    PerMapAnimation *nAnimation = (PerMapAnimation *)starmemoryalloc(sizeof(PerMapAnimation));
    nAnimation->Initialize();
    nAnimation->mRenderPosX = pX;
    nAnimation->mRenderPosY = pY;
    nAnimation->mRenderDepth = pZ;
    nAnimation->mTicksPerFrame = 1.0f;
    nAnimation->mTotalFrames = 0;
    nAnimation->mrImagePtrs = NULL;
    nAnimation->mSizePerFrameX = 1.0f;

    //--Register it.
    mPerMapAnimationList->AddElement(pName, nAnimation, &PerMapAnimation::DeleteThis);
}
void AdventureLevel::SetAnimationFromPattern(const char *pName, const char *pPattern, int pFrames)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Arg check.
    if(!pPattern || pFrames < 1) return;

    //--Allocate space.
    rAnimation->Allocate(pFrames);

    //--Locate the DL Heading which holds the entire animation. Fail if it's not found.
    char *tLocation = InitializeString("Root/Images/TempImg/%s/", pPattern);
    StarLinkedList *rHeading = DataLibrary::Fetch()->GetHeading(tLocation);
    free(tLocation);
    if(!rHeading) return;

    //--Iterate. The entries are expected to be in order.
    int i = 0;
    void *rImage = rHeading->PushIterator();
    while(i < pFrames && rImage)
    {
        //--Set.
        rAnimation->mrImagePtrs[i] = (StarBitmap *)rImage;

        //--Next.
        i ++;
        rImage = rHeading->AutoIterate();
    }
}
void AdventureLevel::SetAnimationRender(const char *pName, bool pIsRendering)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Flag.
    rAnimation->mIsRendering = pIsRendering;
}
void AdventureLevel::SetAnimationDestinationFrame(const char *pName, float pDestinationFrame)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Set.
    rAnimation->mLoopType = PMA_LOOP_NONE;
    rAnimation->mDestinationFrame = pDestinationFrame;
    rAnimation->mTickRate = 1.0f;
}
void AdventureLevel::SetAnimationDestinationFrame(const char *pName, float pDestinationFrame, float pFramerate)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Set.
    rAnimation->mLoopType = PMA_LOOP_NONE;
    rAnimation->mDestinationFrame = pDestinationFrame;
    rAnimation->mTickRate = pFramerate;
}
void AdventureLevel::SetAnimationLoop(const char *pName, float pLoopStartFrame, float pLoopEndFrame, const char *pLoopType, float pFramerateToTarget, float pTicksForWholeLoop)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation || !pLoopType || pTicksForWholeLoop < 1.0f) return;

    //--Sinusoidal looping.
    if(!strcasecmp(pLoopType, "Sin"))
    {
        rAnimation->mRebootLoopTimer = true;
        rAnimation->mLoopType = PMA_LOOP_SINUSOIDAL;
        rAnimation->mLowFrame = pLoopStartFrame;
        rAnimation->mHiFrame = pLoopEndFrame;
        rAnimation->mInternalLoopTimer = 0.0f;
        rAnimation->mTickRate = pFramerateToTarget;
        rAnimation->mTicksPerWholeLoop = pTicksForWholeLoop;
    }
    //--Linear looping.
    else if(!strcasecmp(pLoopType, "Loop"))
    {
        rAnimation->mRebootLoopTimer = true;
        rAnimation->mLoopType = PMA_LOOP_LINEAR;
        rAnimation->mLowFrame = pLoopStartFrame;
        rAnimation->mHiFrame = pLoopEndFrame;
        rAnimation->mInternalLoopTimer = 0.0f;
        rAnimation->mTickRate = pFramerateToTarget;
        rAnimation->mTicksPerWholeLoop = pTicksForWholeLoop;
    }
    //--Reverse looping.
    else if(!strcasecmp(pLoopType, "Reverse"))
    {
        rAnimation->mRebootLoopTimer = true;
        rAnimation->mLoopType = PMA_LOOP_REVERSE;
        rAnimation->mLowFrame = pLoopStartFrame;
        rAnimation->mHiFrame = pLoopEndFrame;
        rAnimation->mInternalLoopTimer = 0.0f;
        rAnimation->mTickRate = pFramerateToTarget;
        rAnimation->mTicksPerWholeLoop = pTicksForWholeLoop;
    }
    //--Error.
    else
    {

    }
}
void AdventureLevel::SetAnimationCurrentFrame(const char *pName, float pOverrideFrame)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Set.
    rAnimation->mTimer = pOverrideFrame * rAnimation->mTicksPerFrame;
}
void AdventureLevel::ActivateUnderwaterShader()
{
    mIsUnderwaterMode = true;
}
void AdventureLevel::DeactivateUnderwaterShader()
{
    mIsUnderwaterMode = false;
}
void AdventureLevel::SetParallelCutsceneDuringDialogueFlag(bool pFlag)
{
    mRunParallelDuringDialogue = pFlag;
}
void AdventureLevel::SetParallelCutsceneDuringCutsceneFlag(bool pFlag)
{
    mRunParallelDuringCutscene = pFlag;
}
void AdventureLevel::SetScreenShake(int pTicks)
{
    mScreenShakeTimer = pTicks;
}
void AdventureLevel::SetScreenShakePeriodicity(int pShakeLength, int pTickPeriod, int pScatter)
{
    //--Variables.
    mShakePeriodLength = pShakeLength;
    mShakePeriodicityRoot = pTickPeriod;
    mShakePeriodicityScatter = pScatter;

    //--If the screen shake is currently zero, does a roll immediately.
    mScreenShakeTimer = -mShakePeriodicityRoot;
}
void AdventureLevel::AddShakeSoundEffect(const char *pSoundName)
{
    char *nSoundCopy = InitializeString(pSoundName);
    mShakeSoundList->AddElement("X", nSoundCopy, &FreeThis);
}
void AdventureLevel::SetRockFallChance(int pChance)
{
    mRockFallingFrequency = pChance;
}
void AdventureLevel::AddRockFallImage(const char *pDLPath)
{
    StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mrFallingRockImageList->AddElement("X", rImage);
}
void AdventureLevel::SetHideViewcones(bool pFlag)
{
    mHideAllViewcones = pFlag;
}
void AdventureLevel::ReceiveRunningMinigame(RunningMinigame *pRunningMinigame)
{
    //--Gives the level a RunningMinigame. Takes ownership implicitly. Pass NULL to deallocate and
    //  clear an existing class.
    delete mRunningMinigame;
    mRunningMinigame = pRunningMinigame;

    //--If the minigame exists, set music.
    if(mRunningMinigame)
    {
        AudioManager::Fetch()->PlayMusic("TimeSensitive");
    }
}
void AdventureLevel::ReceiveKPopMinigame(KPopDanceGame *pMinigame)
{
    //--Gives the level a KPopDanceGame. Takes ownership. Pass NULL to deallocate and clear.
    delete mKPopDanceGame;
    mKPopDanceGame = pMinigame;

    //--If the minigame exists, stop the music.
    if(mKPopDanceGame) AudioManager::Fetch()->StopAllMusic();
}
void AdventureLevel::ReceivePuzzleFightGame(PuzzleFight *pMinigame)
{
    //--Gives the level a PuzzleFight class. Takes ownership. Pass NULL to deallocate and clear.
    delete mPuzzleFightGame;
    mPuzzleFightGame = pMinigame;

    //--Construction sequence.
    if(mPuzzleFightGame)
    {
        mPuzzleFightGame->RunLoaderScript();
        mPuzzleFightGame->Construct();
        mPuzzleFightGame->RunInitializerScript();
    }
}
void AdventureLevel::ReceiveShakeMinigame(ShakeMinigame *pMinigame)
{
    //--Gives the level a Shake Minigame. Takes ownership. Pass NULL to deallocate and clear.
    delete mShakeMinigame;
    mShakeMinigame = pMinigame;
}
void AdventureLevel::RegisterNotice(ActorNotice *pNotice)
{
    mNotices->AddElement("X", pNotice, &RootObject::DeleteThis);
}
void AdventureLevel::RunPostCombatOnEntities()
{
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootObject *rEntity = (RootObject *)rEntityList->PushIterator();
    while(rEntity)
    {
        //--Must be of the POINTER_TYPE_TILEMAPACTOR type.
        if(rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rEntity;
            rActor->HandleUIPostBattle();
        }

        //--Next.
        rEntity = (RootObject *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::SetRetreatTimer(int pTicks)
{
    mRetreatTimer = pTicks;
    if(mRetreatTimer < 1) mRetreatTimer = 0;
}
void AdventureLevel::SetRegionNotifier(const char *pDLPath)
{
    rRegionNotifier = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdventureLevel::SetRegionNotifierFinalScale(float pScale)
{
    mRegionNotifierFinalScale = pScale;
    if(mRegionNotifierFinalScale < 0.05f) mRegionNotifierFinalScale = 0.05f;
}
void AdventureLevel::AllocateRegionNotifierAnimations(int pTotal, int pTicksPerFrame)
{
    //--Clean.
    free(rRegionNotifierAnim);
    mRegionNotifierAnimTotal = 0;
    rRegionNotifierAnim = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mRegionNotifierAnimTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    rRegionNotifierAnim = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mRegionNotifierAnimTotal);
    memset(rRegionNotifierAnim, 0, sizeof(StarBitmap *) * mRegionNotifierAnimTotal);

    //--Set values.
    mRegionNotifierAnimState = AL_REGION_STATE_FADEIN;
    mRegionNotifierAnimTimer = 0;
    mRegionNotifierAnimTimerMax = mRegionNotifierAnimTotal * pTicksPerFrame;
}
void AdventureLevel::SetRegionNotifierAnimationFrame(int pFrame, const char *pDLPath)
{
    if(pFrame < 0 || pFrame >= mRegionNotifierAnimTotal) return;
    rRegionNotifierAnim[pFrame] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdventureLevel::SetCannotOpenMenu(bool pFlag)
{
    mCannotOpenMenu = pFlag;
}
void AdventureLevel::DisableBordersForOneCutscene()
{
    mNoBordersThisCutscene = true;
}
void AdventureLevel::SetSavepointDoesNotLight(const char *pName, bool pFlag)
{
    SavePointPackage *rPackage = (SavePointPackage *)mSavePointList->GetElementByName(pName);
    if(!rPackage) return;
    rPackage->mDoesNotRenderLit = pFlag;
}
void AdventureLevel::SetShowHealth(bool pFlag)
{
    mShowHealthMeter = pFlag;
}
void AdventureLevel::SetHealthValues(int pHealth, int pHealthMax)
{
    mHealthRemaining = pHealth;
    mHealthMax = pHealthMax;
}
void AdventureLevel::SetHealthIndicator(const char *pImgPath)
{
    rHealthIndicator = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);
}
void AdventureLevel::SetEmptyIndicator(const char *pImgPath)
{
    rEmptyIndicator = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);
}
void AdventureLevel::SetStaminaPosition(float pX, float pY)
{
    mPartyLeadStamX = pX;
    mPartyLeadStamY = pY;
}
void AdventureLevel::SetAuraSpawnConditions(int pChance, const char *pDLPath)
{
    mSpawnAuraChance = pChance;
    rSpawnAuraImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdventureLevel::SetPlayerCamoTimer(int pTicks)
{
    mPlayerCamoTimer = pTicks;
    if(mPlayerCamoTimer < 0) mPlayerCamoTimer = 0;
}

///======================================== Core Methods ==========================================
void AdventureLevel::RebootMenu()
{
    //--Don't call this while the menu is in use, stupid.
    delete mAdventureMenu;
    mAdventureMenu = MapManager::Fetch()->GenerateNewMenu();
}
TilemapActor *AdventureLevel::LocatePlayerActor()
{
    //--Determines the NPC that the player is controlling and returns it. Can legally return NULL if
    //  the player is not controlling an NPC or the NPC doesn't exist.
    //--If the player's controls are locked, this still returns the TilemapActor in question.
    //--If the entity ID is the wrong type, NULL is returned.
    RootEntity *rActor = EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(!rActor || !rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return NULL;

    //--Checks passed, return this as the right type.
    return (TilemapActor *)rActor;
}
TilemapActor *AdventureLevel::LocateFollowingActor(int pIndex)
{
    //--Determines the NPC that is following the player-controlled NPC and returns it. Can legally
    //  return NULL if fewer than pIndex number of entities are following the player. NPCs follow the
    //  player when in the player's party, or for cutscenes.
    uint32_t *rEntityIDPtr = (uint32_t *)mFollowEntityIDList->GetElementBySlot(pIndex);
    if(!rEntityIDPtr) return NULL;

    //--Check if the entity exists and is of the right type.
    RootEntity *rActor = EntityManager::Fetch()->GetEntityI(*rEntityIDPtr);
    if(!rActor || !rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return NULL;

    //--Checks passed, return it.
    return (TilemapActor *)rActor;
}
int AdventureLevel::GetEffectiveDepth(float pX, float pY)
{
    ///--[Documentation]
    //--Returns an integer code indicating which collision hull the entity caller should use if its center
    //  point is at X,Y. The return will INFLECTION_NO_CHANGE if no entity is doing anything.
    if(mInflectionList->GetListSize() < 1 && mClimbableList->GetListSize() < 1) return INFLECTION_NO_CHANGE;

    ///--[Inflection Zones]
    //--Scan the inflection cases.
    InflectionPackage *rPackage = (InflectionPackage *)mInflectionList->PushIterator();
    while(rPackage)
    {
        //--If the point is within the package...
        if(IsPointWithin(pX, pY, rPackage->mX, rPackage->mY, rPackage->mX + rPackage->mW, rPackage->mY + rPackage->mH))
        {
            //--In all cases pop the iterator.
            mInflectionList->PopIterator();

            //--If within the top half, use the upper inflection.
            if(pY <= rPackage->mY + (rPackage->mH * 0.5f))
            {
                return rPackage->mUpperDepth;
            }

            //--Otherwise, use the lower inflection.
            return rPackage->mLowerDepth;
        }

        //--Next.
        rPackage = (InflectionPackage *)mInflectionList->AutoIterate();
    }

    ///--[Climbable Zones]
    //--Scan the climbable cases. They are basically the same as the inflection cases. If they are not
    //  active, the player can't enter them anyway.
    ClimbablePackage *rClimbPackage = (ClimbablePackage *)mClimbableList->PushIterator();
    while(rClimbPackage)
    {
        //--If the point is within the package...
        if(IsPointWithin(pX, pY, rClimbPackage->mX, rClimbPackage->mY, rClimbPackage->mX + rClimbPackage->mW, rClimbPackage->mY + rClimbPackage->mH))
        {
            //--In all cases pop the iterator.
            mClimbableList->PopIterator();

            //--If within the top half, use the upper inflection.
            if(pY <= rClimbPackage->mY + (rClimbPackage->mH * 0.5f))
            {
                return rClimbPackage->mUpperDepth;
            }

            //--Otherwise, use the lower inflection.
            return rClimbPackage->mLowerDepth;
        }

        //--Next.
        rClimbPackage = (ClimbablePackage *)mClimbableList->AutoIterate();
    }

    ///--[Default]
    //--All checks failed. Do not change the inflection.
    return INFLECTION_NO_CHANGE;
}
bool AdventureLevel::IsInClimbableZone(float pX, float pY)
{
    //--Checks if the given position happens to be within a climbable zone. This is used to cause
    //  actors to always face up in such zones so it looks like they're climbing.
    ClimbablePackage *rClimbPackage = (ClimbablePackage *)mClimbableList->PushIterator();
    while(rClimbPackage)
    {
        //--If the point is within the package...
        if(IsPointWithin(pX, pY, rClimbPackage->mX, rClimbPackage->mY, rClimbPackage->mX + rClimbPackage->mW, rClimbPackage->mY + rClimbPackage->mH))
        {
            mClimbableList->PopIterator();
            return true;
        }

        //--Next.
        rClimbPackage = (ClimbablePackage *)mClimbableList->AutoIterate();
    }

    //--Not in a climbable zone.
    return false;
}
void AdventureLevel::PulseIgnore(const char *pString)
{
    ///--[Documentation]
    //--Orders all enemies in the level to check if they are ignoring the player using the provided string.
    //  This is triggered whenever the player enters the level or changes forms while in the level.
    ResetString(xLastIgnorePulse, pString);

    //--Iterate.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Check.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            ((TilemapActor *)rCheckEntity)->PulseIgnore(pString);
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::RepositionParty(TilemapActor *pPlayerActor, float pX, float pY)
{
    ///--[Documentation]
    //--Repositions the party to the given location. This handles moving the following actors as well.
    //  Note that the value is in world-pixels, not tiles.
    //--Note: If calling this via a script, set the facing *before* calling this function so the party
    //  members are facing the same direction as the player.
    //--If the Actor is not provided, we have to locate it. DO NOT use this during a level transition, even a "LASTSAVE"
    //  one, because the player's actor is liberated before the transition. You MUST pass in an Actor in these cases.
    int tFacing = TA_DIR_SOUTH;

    //--Actor Location.
    TilemapActor *rPlayerActor = pPlayerActor;
    if(!rPlayerActor) rPlayerActor = LocatePlayerActor();

    //--If the values are -700.0f and -700.0f, this is a "Fold" order. The position becomes wherever the
    //  party leader currently is.
    if(pX == -700.0f && pY == -700.0f && rPlayerActor)
    {
        pX = rPlayerActor->GetWorldX();
        pY = rPlayerActor->GetWorldY();
    }

    //--Set the player actor. Also get facing information.
    if(rPlayerActor)
    {
        //--Reposition.
        rPlayerActor->SetPositionByPixel(pX, pY);
        rPlayerActor->SetPartyEntityFlag(true);

        //--Get variables.
        tFacing = rPlayerActor->GetFacing();
    }

    //--Order all the followers to reposition.
    for(int i = 0; i < mFollowEntityIDList->GetListSize(); i ++)
    {
        TilemapActor *rFollower = LocateFollowingActor(i);
        if(rFollower)
        {
            rFollower->SetPositionByPixel(pX, pY);
            rFollower->SetFacing(tFacing);
        }
    }

    //--Set all of the trailing positions to this.
    for(int i = 0; i < PLAYER_MOVE_STORAGE_TOTAL; i ++)
    {
        mPlayerXPositions[i] = pX;
        mPlayerYPositions[i] = pY;
        mPlayerFacings[i] = tFacing;
        mPlayerTimers[i] = 0;
        mPlayerDepths[i] = 0;
        mPlayerRunnings[i] = false;
    }
}
bool AdventureLevel::IsLineClipped(float pX1, float pY1, float pX2, float pY2, int pDepth)
{
    ///--[Documentation and Setup]
    //--Returns true if the given line has a collision across it. This is done via a primitive ray-tracer.
    //  The tracer is relatively fast at the cost of accuracy, and very much on purpose.
    //--Lines are not allowed to cross depths. All such attempts would be considered failures.

    //--Distance check. Very short checks always are unclipped.
    const float cDistance = GetPlanarDistance(pX1, pY1, pX2, pY2);
    //fprintf(stderr, "Checking line collision %f\n", cDistance);
    if(cDistance < 2.0f) return false;

    //--Fast-access variables. These speed up execution.
    float tCurrentX = pX1;
    float tCurrentY = pY1;
    const float cDifX = (pX2 - pX1) / cDistance;
    const float cDifY = (pY2 - pY1) / cDistance;
    //fprintf(stderr, "Dif values: %f %f\n", cDifX, cDifY);

    //--Last-check values. There is no need to recheck collisions if the X/Y values didn't change
    //  in integer math since only integers can be checked. This is only used when very high precisions
    //  are being used, comment it out for low precisions.
    //int tLastCheckedX = -1000;
    //int tLastCheckedY = -1000;

    //--Starting at X1/Y1, iterate until at X2/Y2.
    for(float i = 0.0f; i < cDistance; i = i + 1.0f)
    {
        //--Is this position identical in integer to the last check? If so, skip it.
        /*if(tLastCheckedX == (int)tCurrentX && tLastCheckedY == (int)tCurrentY)
        {
            //fprintf(stderr, " Skip.\n");
            tCurrentX = tCurrentX + cDifX;
            tCurrentY = tCurrentY + cDifY;
            continue;
        }*/

        //--If not, store the position and run the check.
        //fprintf(stderr, " Checking at %i %i\n", (int)tCurrentX, (int)tCurrentY);
        if(GetClipAt(tCurrentX, tCurrentY, 0.0f, pDepth))
        {
            //fprintf(stderr, "  Hit collision.\n");
            return true;
        }

        //--If we got this far, the position was not clipped. Store and continue.
        //tLastCheckedX = (int)tCurrentX;
        //tLastCheckedY = (int)tCurrentY;
        tCurrentX = tCurrentX + cDifX;
        tCurrentY = tCurrentY + cDifY;
    }

    //--The entire trace ran through. No clips were found.
    //fprintf(stderr, "No hits.\n");
    return false;
}
void AdventureLevel::RunAutosave()
{
    ///--[Documentation and Setup]
    //--When the program transitions between rooms, the first tick during which there is nothing on
    //  the cutscene manager triggers an autosave.
    //--Autosaves are stored in a stacking set of files, AutosaveXX, with newer autosaves being lower
    //  numerically. The autosave places a set of extra variables in to specify the exact player
    //  location which are normally not valid.
    if(!mRunAutosave) return;

    //--If this flag is set, stop the autosave once.
    if(xBlockAutosaveOnce)
    {
        xBlockAutosaveOnce = false;
        mRunAutosave = false;
        return;
    }

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Error Check]
    //--If there is anything on theCutsceneManager, don't run the autosave. The CutsceneManager is
    //  supposed to run before the autosave, but something may have been enqueued.
    if(CutsceneManager::Fetch()->HasAnyEvents()) return;

    //--If this flag is 1.0f, autosaves are blocked for script reasons.
    SysVar *rBlockAutosaveVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/Autosave/iBlockAutosave");
    if(rBlockAutosaveVar && rBlockAutosaveVar->mNumeric == 1.0f)
    {
        mRunAutosave = false;
        return;
    }

    ///--[Option Check]
    //--Make sure the autosave number is at least 1. If it's zero, never autosave.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    int cMaxAutosaves = rOptionsManager->GetOptionI("Max Autosaves");

    //--Clamp.
    if(cMaxAutosaves > 100) cMaxAutosaves = 100;

    //--No autosaves are being used, stop execution.
    if(cMaxAutosaves < 1)
    {
        mRunAutosave = false;
        return;
    }

    ///--[Variables]
    //--Get the player actor. If the player is not in control of an actor, no autosave can take place.
    TilemapActor *rPlayerActor = LocatePlayerActor();

    //--This flags the savefile as being an autosave, which tells the file to look for additional
    //  position variables when being loaded.
    SysVar *rIsAutosaveVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/Autosave/iIsAutosave");

    //--These are position flags.
    SysVar *rPlayerXVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/Autosave/iPlayerX");
    SysVar *rPlayerYVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/Autosave/iPlayerY");
    SysVar *rPlayerZVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/Autosave/iPlayerZ");

    //--If any flags failed to resolve, stop here.
    if(!rPlayerActor || !rIsAutosaveVar || !rPlayerXVar || !rPlayerYVar || !rPlayerZVar) return;

    ///--[Mark as Autosave]
    //--Setting this to 1.0f will mark this save as an autosave.
    rIsAutosaveVar->mNumeric = 1.0f;

    ///--[Store Player Position]
    //--Resolve where the player actor currently is.
    rPlayerXVar->mNumeric = rPlayerActor->GetWorldX();
    rPlayerYVar->mNumeric = rPlayerActor->GetWorldY();
    rPlayerZVar->mNumeric = rPlayerActor->GetCollisionDepth();

    ///--[Run Save Routine]
    //--Order the SaveManager to run the save routine.
    SaveManager *rSaveManager = SaveManager::Fetch();
    rSaveManager->SetSavegameName("Autosave");

    //--Game Directory:
    const char *rAdventureDir = rDataLibrary->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

    //--Go to the autosave directory and rename autosaves up.
    for(int i = cMaxAutosaves - 2; i >= 0; i --)
    {
        //--Path setup.
        char *tFileBufOld = InitializeString("%s/../../Saves/Autosave/Autosave%02i.slf", rAdventureDir, i+0);
        char *tFileBufNew = InitializeString("%s/../../Saves/Autosave/Autosave%02i.slf", rAdventureDir, i+1);

        //--Delete the destination file.
        remove(tFileBufNew);

        //--Rename.
        rename(tFileBufOld, tFileBufNew);

        //--Clean.
        free(tFileBufOld);
        free(tFileBufNew);
    }

    //--Write to the file.
    char *tFileBuf = InitializeString("%s/../../Saves/Autosave/Autosave00.slf", rAdventureDir);
    rSaveManager->SaveAdventureFile(tFileBuf);

    //--Clean.
    free(tFileBuf);

    ///--[Flag]
    //--Mark the level as having completed its autosave.
    mRunAutosave = false;

    //--Unset the autosave variable.
    rIsAutosaveVar->mNumeric = 0.0f;

    //--Resolve the autosave indicator to render in the corner.
    rAutosaveImage = (StarBitmap *)rDataLibrary->GetEntry(xAutosaveIconPath);
    if(rAutosaveImage)
    {
        mAutosavePostTimer = AL_AUTOSAVE_INDICATOR_TICKS;
    }
}

///==================================== Private Core Methods ======================================
bool AdventureLevel::CheckActorAgainstExits(TilemapActor *pActor, bool pOnlyEmulate)
{
    ///--[Documentation and Setup]
    //--Subroutine called during the update cycle after the player moves. The given TilemapActor
    //  checks if it is currently wholly within any of the exits on the map. If so, begin transition
    //  to the target.
    //--For safety reasons, a successfully used exit will be removed from the list and deallocated.
    //  If there is an error, such as being unable to load its destination, this prevents the program
    //  from locking.
    //--Returns true if the actor is on an exit, false if not.
    if(!pActor) return false;
    DebugPush(false, "Checking actor against exits.\n");

    //--Flag.
    bool tIsActorOnAnyStaircase = false;

    //--Get the Actor's position.
    DebugPrint("Getting actor position.\n");
    TwoDimensionReal tActorDim;
    tActorDim.SetWH(pActor->GetWorldX(), pActor->GetWorldY(), TA_SIZE, TA_SIZE);

    //--Compare it to exits.
    DebugPrint("Checking exits.\n");
    ExitPackage *rPackage = (ExitPackage *)mExitList->SetToHeadAndReturn();
    while(rPackage)
    {
        //--Flag.
        bool tIsCollision = false;

        //--Staircases: These can have a partial collision. The player's hitbox needs to merely touch the middle.
        DebugPrint("Checking staircases and non-staircases.\n");
        if(rPackage->mIsStaircase && IsPointWithin2DReal(rPackage->mX + (rPackage->mW * 0.5f), rPackage->mY + (rPackage->mH * 0.5f), tActorDim))
        {
            tIsCollision = true;
        }
        //--Non-staircase. These need a full coverage.
        else if(!rPackage->mIsStaircase && rPackage->mX < tActorDim.mLft && rPackage->mX + rPackage->mW > tActorDim.mRgt &&
           rPackage->mY < tActorDim.mTop && rPackage->mY + rPackage->mH > tActorDim.mBot)
        {
            tIsCollision = true;
        }

        //--Hit?
        DebugPrint("Checking for collision.\n");
        if(tIsCollision)
        {
            //--When emulating, a collision just returns true.
            if(pOnlyEmulate) return true;

            //--If this is a staircase and the Actor is currently exiting a transition, then this pass is ignored.
            if(mIsPlayerOnStaircase && rPackage->mIsStaircase)
            {
                tIsActorOnAnyStaircase = true;
            }
            //--This is a transition case.
            else
            {
                //--Set strings.
                DebugPrint("Resetting strings.\n");
                ResetString(mTransitionDestinationMap,  rPackage->mMapDestination);
                ResetString(mTransitionDestinationExit, rPackage->mExitDestination);

                //--Store the direction the player was facing to exit.
                DebugPrint("And so on.\n");
                mTransitionDirection = pActor->GetFacing();

                //--Horizontal exit: Store percentage.
                mTransitionPercentage = 0.5f;
                if(rPackage->mW >= rPackage->mH && rPackage->mW > 0.0f)
                {
                    mTransitionPercentage = (tActorDim.mLft - rPackage->mX) / (float)rPackage->mW;
                }
                //--Vertical exit: Store percentage.
                else if(rPackage->mH > 0.0f)
                {
                    mTransitionPercentage = (tActorDim.mTop - rPackage->mY) / (float)rPackage->mH;
                }

                //--Flags.
                mIsTransitionOut = true;

                //--If currently in a transition, just roll the timer:
                if(mIsTransitionIn)
                {
                    mIsTransitionIn = false;
                    mTransitionTimer = TRANSITION_FADE_TICKS - mTransitionTimer;
                }
                //--Neutral case:
                else
                {
                    mTransitionTimer = 0;
                }

                //--If this is a staircase, play a sound.
                if(rPackage->mIsStaircase)
                    AudioManager::Fetch()->PlaySound("World|Stairs");

                //--Remove the exit and finish.
                //mExitList->RemoveRandomPointerEntry();
                DebugPop("Complete, match found.\n");
                return true;
            }
        }

        //--Next.
        rPackage = (ExitPackage *)mExitList->IncrementAndGetRandomPointerEntry();
    }

    //--Flag: If the player was not on a staircase, unset this flag so they can safely move off of
    //  a staircase after a transition.
    if(!tIsActorOnAnyStaircase)
    {
        mIsPlayerOnStaircase = false;
    }

    //--Player is not on any exit.
    DebugPop("Complete, no matches found.\n");
    return false;
}
void AdventureLevel::CompileAfterParse()
{
    ///--[Documentation and Setup]
    //--After the SLF file has parsed the minimum data needed to create the level, we do the optional
    //  compilation and setup. Right now, RayCollisionEngine setup takes place here.
    StarLumpManager *rSLM = StarLumpManager::Fetch();

    //--Setup.
    bool tNeedsToExport = false;

    //--After the SLF is parsed, built ray collision engines for the collision layers.
    mRayCollisionEnginesTotal = mCollisionLayersTotal;
    SetMemoryData(__FILE__, __LINE__);
    mRayCollisionEngines = (RayCollisionEngine **)starmemoryalloc(sizeof(RayCollisionEngine *) * mRayCollisionEnginesTotal);
    for(int i = 0; i < mRayCollisionEnginesTotal; i ++)
    {
        //--Name buffer.
        char tBuffer[STD_MAX_LETTERS];
        sprintf(tBuffer, "RCE%02i", i);

        //--Create the RCE.
        mRayCollisionEngines[i] = new RayCollisionEngine();
        mRayCollisionEngines[i]->SetTileSize(TileLayer::cxSizePerTile);

        //--Check if an RCE data buffer already exists. If so, just read that.
        if(rSLM->DoesLumpExist(tBuffer))
        {
            //--Get the lump.
            rSLM->StandardSeek(tBuffer, "RCEDATA000");
            VirtualFile *rVFile = rSLM->GetVirtualFile();

            //--The file is already in position, so pass it to the RCE.
            mRayCollisionEngines[i]->ReadFromFile(rVFile);
        }
        //--No RCE data exists. Compile the RCE from collision data. Then, store the lump
        //  data and write it back to the file so we don't have to compile it again.
        else
        {
            //--Inform the routine that we need to write the lumps back.
            tNeedsToExport = true;

            //--Compile.
            mRayCollisionEngines[i]->BuildWith(mCollisionLayers[i]);

            //--Create a lump.
            RootLump *nNewLump = StarLumpManager::CreateLump(tBuffer);

            //--Populate with the RCE's data. It also automatically sets the data size.
            nNewLump->mOwnsData = true;
            nNewLump->mData = mRayCollisionEngines[i]->CreateDataBuffer(nNewLump->mDataSize);
            rSLM->RegisterLump(nNewLump);
        }
    }

    //--Now spit out a copy from the SLM of the last opened file, if flagged.
    if(tNeedsToExport)
    {
        rSLM->MergeLumps();
        rSLM->Write(rSLM->GetOpenPath());
    }

    ///--[Door Building]
    //--Doors get added to the RCEs. Their lines are toggled off when the door is opened.
    int i = 0;
    char tNameBuffers[4][STD_MAX_LETTERS];
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--Name.
        sprintf(tNameBuffers[0], "%i|%i", i, 0);
        sprintf(tNameBuffers[1], "%i|%i", i, 1);
        sprintf(tNameBuffers[2], "%i|%i", i, 2);
        sprintf(tNameBuffers[3], "%i|%i", i, 3);

        //--Register to all RCEs.
        for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
        {
            //--Error check.
            if(!mRayCollisionEngines[p]) continue;

            //--Construct lines.
            float cLft = rDoorPackage->mX;
            float cTop = rDoorPackage->mY;
            float cRgt = rDoorPackage->mX + TileLayer::cxSizePerTile;
            float cBot = rDoorPackage->mY + TileLayer::cxSizePerTile;
            if(rDoorPackage->mIsWide) cRgt = cRgt + TileLayer::cxSizePerTile;
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[0], cLft, cTop, cRgt, cTop);
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[1], cLft, cBot, cRgt, cBot);
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[2], cLft, cTop, cLft, cBot);
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[3], cRgt, cTop, cRgt, cBot);
        }

        //--Next.
        i ++;
        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }
}

///=========================================== Update =============================================
void AdventureLevel::ExecuteFieldAbility(int pIndex)
{
    //--Fetch the ability.
    AdvCombat *rCombat = AdvCombat::Fetch();
    FieldAbility *rAbility = rCombat->GetFieldAbility(pIndex);
    if(!rAbility) return;

    //--Ability is on cooldown.
    if(rAbility->GetCooldown() > 0)
    {
        rAbility->Execute(FIELDABILITY_EXEC_RUNWHILECOOLING);
        return;
    }

    //--Create.
    FieldAbilityPack *nPackage = (FieldAbilityPack *)starmemoryalloc(sizeof(FieldAbilityPack));
    nPackage->Initialize();
    nPackage->rParentAbility = rAbility;

    //--Execute.
    rExecutingPackage = nPackage;
    rAbility->Execute(FIELDABILITY_EXEC_RUN);
    rAbility->ActivateCooldown();//Note: Activates cooldown AFTER ability executes, in case the ability changes cooldown during execution.

    //--Clean up.
    nPackage->mTimer = 1;
    rExecutingPackage = NULL;

    //--If the package was flagged for termination, don't save it!
    if(!nPackage->mIsExpired)
    {
        mExecutingFieldAbilities->AddElement("X", nPackage, &FreeThis);
    }
    else
    {
        free(nPackage);
    }
}
bool AdventureLevel::MovePartyMembers()
{
    ///--[Documentation]
    //--All party members move to follow the player's position. This causes the party to organically follow
    //  behind the party leader. It can also be used in a cutscene.
    //--Returns true if any party members moved, false otherwise.
    TilemapActor *rActor = LocatePlayerActor();
    if(!rActor) return false;

    //--Variables.
    float tPlayerX = rActor->GetWorldX();
    float tPlayerY = rActor->GetWorldY();

    //--Downshift.
    for(int i = PLAYER_MOVE_STORAGE_TOTAL - 1; i >= 1; i --)
    {
        mPlayerXPositions[i] = mPlayerXPositions[i-1];
        mPlayerYPositions[i] = mPlayerYPositions[i-1];
        mPlayerFacings[i]    = mPlayerFacings[i-1];
        mPlayerTimers[i]     = mPlayerTimers[i-1];
        mPlayerDepths[i]     = mPlayerDepths[i-1];
        mPlayerRunnings[i]   = mPlayerRunnings[i-1];
    }

    //--Zeroth becomes the current position.
    mPlayerXPositions[0] = tPlayerX;// + rActor->GetRemainderX();
    mPlayerYPositions[0] = tPlayerY;// + rActor->GetRemainderY();
    mPlayerFacings[0] = rActor->GetFacing();
    if(rActor->AlwaysFaceUp()) mPlayerFacings[0] = DIR_UP;
    mPlayerTimers[0] = rActor->GetMoveTimer();
    mPlayerDepths[0] = rActor->GetCollisionDepth();
    mPlayerRunnings[0] = rActor->mRanLastTick;

    //--Scan the collision depths of all actors. We use the highest one later on.
    int tHighestDepth = rActor->GetCollisionDepth();
    if(LocateFollowingActor(0) && mPlayerDepths[12] > tHighestDepth) tHighestDepth = mPlayerDepths[12];
    if(LocateFollowingActor(1) && mPlayerDepths[24] > tHighestDepth) tHighestDepth = mPlayerDepths[24];
    if(LocateFollowingActor(2) && mPlayerDepths[36] > tHighestDepth) tHighestDepth = mPlayerDepths[36];

    //--Player character's depth override.
    if(tHighestDepth != rActor->GetCollisionDepth())
    {
        float tOffsetY = mPlayerYPositions[0];
        float tZPosition = DEPTH_MIDGROUND + ((mPlayerYPositions[0] + tOffsetY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
        tZPosition = tZPosition + (DEPTH_PER_TILE * (float)tHighestDepth * 2.0f);
        rActor->SetOverrideDepth(tZPosition);
    }
    //--Normal case.
    else
    {
        rActor->SetOverrideDepth(-2.0f);
    }

    //--Order followers to move to the provided positions as necessary.
    //fprintf(stderr, "Calibrating follower offset.\n");
    bool tAnyoneMoved = false;
    int i = 0;
    TilemapActor *rFollower = LocateFollowingActor(i);
    while(rFollower)
    {
        //--Compute the spacing cap.
        int tUsePos = PLAYER_MOVE_STORAGE_TOTAL -1;
        if(i == 0) tUsePos = mFollowerSpacing[0];
        if(i == 1) tUsePos = mFollowerSpacing[1];
        if(i == 2) tUsePos = mFollowerSpacing[2];
        if(tUsePos >= PLAYER_MOVE_STORAGE_TOTAL) tUsePos = PLAYER_MOVE_STORAGE_TOTAL-1;

        //--Offset.
        float tOffsetX = mPlayerXPositions[tUsePos] - mPlayerXPositions[0];
        float tOffsetY = mPlayerYPositions[tUsePos] - mPlayerYPositions[0];
        CalibrateChaseOffsets(i, mPlayerRunnings[tUsePos], tOffsetX, tOffsetY);
        //fprintf(stderr, " %i - %5.2f %5.2f\n", i, tOffsetX, tOffsetY);

        //--Check if any movement took place.
        if(tPlayerX != mPlayerXPositions[0] + tOffsetX || tPlayerY != mPlayerYPositions[0] + tOffsetY)
        {
            tAnyoneMoved = true;
            rFollower->RandomizeIdleTimer();
        }

        //--Order.
        rFollower->SetPositionByPixel(mPlayerXPositions[0] + tOffsetX, mPlayerYPositions[0] + tOffsetY);
        rFollower->SetFacing(mPlayerFacings[tUsePos]);
        rFollower->ForceMoving(mPlayerTimers[tUsePos]);
        rFollower->SetCollisionDepth(mPlayerDepths[tUsePos]);
        rFollower->mRanLastTick = mPlayerRunnings[tUsePos];

        //--Compute the relative depth. Followers use the current depth of the leader, not their own collision depth, when computing
        //  depth. This makes them not re-order when on steps.
        float tZPosition = DEPTH_MIDGROUND + ((mPlayerYPositions[0] + tOffsetY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
        tZPosition = tZPosition + (DEPTH_PER_TILE * (float)tHighestDepth * 2.0f);
        rFollower->SetOverrideDepth(tZPosition);

        //--Next.
        i ++;
        rFollower = LocateFollowingActor(i);
    }

    //--Finish up.
    return tAnyoneMoved;
}
bool AdventureLevel::AutofoldPartyMembers()
{
    //--Orders all party members to move towards the party leader's position at walking speed. Returns
    //  true if all party members did not move (and folding is done), false if any one did move.
    TilemapActor *rActor = LocatePlayerActor();
    if(!rActor) return true;

    //--Variables.
    bool tEveryoneDoneMoving = true;
    float tPlayerX = rActor->GetWorldX();
    float tPlayerY = rActor->GetWorldY();

    //--Order followers to move to the provided positions as necessary.
    //fprintf(stderr, "Calibrating follower offset.\n");
    int i = 0;
    TilemapActor *rFollower = LocateFollowingActor(i);
    while(rFollower)
    {
        //--Order follower to move to the player's position.
        bool tAtLocation = rFollower->HandleMoveTo(tPlayerX, tPlayerY, -1.0f);
        if(!tAtLocation) tEveryoneDoneMoving = false;

        //--Next.
        i ++;
        rFollower = LocateFollowingActor(i);
    }

    //--Return whether or not everyone was done moving.
    return tEveryoneDoneMoving;
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
AdventureMenu *AdventureLevel::GetMenu()
{
    return mAdventureMenu;
}
AdventureDebug *AdventureLevel::GetDebugMenu()
{
    return mDebugMenu;
}
KPopDanceGame *AdventureLevel::GetDanceMinigame()
{
    return mKPopDanceGame;
}
PuzzleFight *AdventureLevel::GetPuzzleFight()
{
    return mPuzzleFightGame;
}
ShakeMinigame *AdventureLevel::GetShakeMinigame()
{
    return mShakeMinigame;
}
FieldAbilityPack *AdventureLevel::GetActiveFieldAbilityPack()
{
    return rExecutingPackage;
}
StarLinkedList *AdventureLevel::GetPatrolNodeList()
{
    return mPatrolNodes;
}

///===================================== Static Functions =========================================
AdventureLevel *AdventureLevel::Fetch()
{
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_ADVENTURELEVEL)) return NULL;
    return (AdventureLevel *)rCheckLevel;
}
void AdventureLevel::SetMusic(const char *pName, bool pSlowly)
{
    ///--[Documentation]
    //--Sets the music which should be playing. If the music changes then we handle fading here.
    //  Pass NULL to stop playing music.
    DebugPush(true, "Setting level music to %s\n", pName);

    ///--[Music Is Playing]
    if(xLevelMusic)
    {
        //--Debug.
        DebugPrint(" Music is playing.\n");

        ///--[Change Music Track]
        //--Music activated? Save it.
        if(pName && strcasecmp(pName, "Null"))
        {
            //--Debug.
            DebugPrint(" Playing new track.\n");

            //--If the music is the same as last time, do nothing.
            if(!strcasecmp(pName, xLevelMusic))
            {
                DebugPrint(" Identical to previous track. Do nothing.\n");
            }
            //--Layered music? Activate that, crossfade the current music.
            else if(!strncasecmp(pName, "LAYER|", 6))
            {
                //--Debug.
                DebugPrint(" Handling layered music.\n");

                //--In all cases, store the name.
                ResetString(xLevelMusic, &pName[6]);

                //--Crossfade old music if present.
                AudioManager::Fetch()->FadeMusic(-30);

                //--If layered music was already playing...
                if(xIsLayeringMusic)
                {

                }
                //--Layered music is not playing. Activate it.
                else
                {
                    ActivateMusicLayering();
                }
            }
            //--Otherwise, crossfade.
            else
            {
                //--Debug.
                DebugPrint(" Clearing music for crossfade.\n");

                //--Turn off layering if it was on.
                DeactivateMusicLayering();

                //--Fade the previous music out.
                if(!pSlowly)
                {
                    AudioManager::Fetch()->FadeMusic(-30);
                }
                //--Fade it out over 3 seconds. Used for cutscenes.
                else
                {
                    AudioManager::Fetch()->FadeMusic(-180);
                }

                //--Set the name, play the music and fade it in.
                ResetString(xLevelMusic, pName);
                AudioManager::Fetch()->PlayMusic(xLevelMusic);
            }
        }
        ///--[Null]
        //--Passed NULL while music was playing. Fade the music out.
        else
        {
            //--Debug.
            DebugPrint(" Passed null, fading music out.\n");

            //--Fade the previous music out.
            if(!pSlowly)
            {
                AudioManager::Fetch()->FadeMusic(-30);
            }
            //--Fade it out over 3 seconds. Used for cutscenes.
            else
            {
                AudioManager::Fetch()->FadeMusic(-180);
            }

            //--Flags.
            DeactivateMusicLayering();
            ResetString(xLevelMusic, NULL);
        }
    }
    ///--[No Music Is Playing]
    else
    {
        //--Debug.
        DebugPrint(" No music is playing.\n");

        ///--[Play New Music]
        //--Music activated? Save it.
        if(pName && strcasecmp(pName, "Null"))
        {
            //--Debug.
            DebugPrint(" Playing new track.\n");

            //--Layered music?
            if(!strncasecmp(pName, "LAYER|", 6))
            {
                DebugPrint(" Activating layered tracks.\n");
                ResetString(xLevelMusic, &pName[6]);
                ActivateMusicLayering();
            }
            //--Regular music.
            else
            {
                DebugPrint(" Normal music.\n");
                ResetString(xLevelMusic, pName);
                DeactivateMusicLayering();
                AudioManager::Fetch()->PlayMusic(xLevelMusic);
            }
        }
        ///--[Clear Music]
        //--Passed null, do nothing.
        else
        {
            DebugPrint(" Do nothing.\n");
            DeactivateMusicLayering();
            ResetString(xLevelMusic, NULL);
        }
    }

    ///--[Clean Up]
    DebugPop("Finished music setting.\n");
}
