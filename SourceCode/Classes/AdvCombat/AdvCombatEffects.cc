//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///===================================== Property Queries =========================================
bool AdvCombat::DoesEffectExist(uint32_t pID)
{
    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Matches ID.
        if(rEffect->GetID() == pID)
        {
            mGlobalEffectList->PopIterator();
            return true;
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }

    //--Not found.
    return false;
}

///======================================= Manipulators ===========================================
void AdvCombat::RegisterEffect(AdvCombatEffect *pEffect)
{
    //--Duplicates aren't allowed.
    if(mGlobalEffectList->IsElementOnList(pEffect)) return;

    //--Add it, flag the list to rebuild.
    mMustRebuildEffectReferences = true;
    mGlobalEffectList->AddElementAsTail("X", pEffect, &RootObject::DeleteThis);
}

///======================================= Core Methods ===========================================
void AdvCombat::RebuildEffectReferences()
{
    ///--[Documentation and Setup]
    //--There is a global list of effects, and a global list of entities. Entities show which effects
    //  are impacting them on their UI, just above their HP. Because this is purely a visual thing,
    //  entities do not contain a robust list of effects. Instead, every time the effect list is modified,
    //  we rebuild which entities are affected by what.
    //--This is controlled by the variable mMustRebuildEffectReferences.
    if(!mMustRebuildEffectReferences)
    {
        return;
    }
    mMustRebuildEffectReferences = false;

    ///--[Clear]
    //--Iterate across all entities. Clear the effect render list for all of them.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->GetEffectRenderList()->ClearList();
        rEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }
    rEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->GetEffectRenderList()->ClearList();
        rEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    ///--[Pulse Effects]
    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Run across all targets in the effect.
        StarLinkedList *rTargetList = rEffect->GetTargetList();
        AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)rTargetList->PushIterator();
        while(rPackage)
        {
            //--Get the entity. If it's found, add the effect to the rendering list. Effects can never
            //  contain duplicate entries, so the entity always has unique effect entries as well.
            AdvCombatEntity *rEntity = GetEntityByID(rPackage->mTargetID);
            if(rEntity) rEntity->GetEffectRenderList()->AddElement("X", rEffect);

            //--Next.
            rPackage = (AdvCombatEffectTargetPack *)rTargetList->AutoIterate();
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }
}
StarLinkedList *AdvCombat::GetEffectsReferencing(AdvCombatEntity *pEntity)
{
    //--Returns a StarLinkedList containing all effects currently listing the given entity as a target. The list
    //  must be deallocated by the caller. The list can legally be empty.
    StarLinkedList *nEffectList = new StarLinkedList(false);
    if(!pEntity) return nEffectList;

    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Run across all targets in the effect.
        StarLinkedList *rTargetList = rEffect->GetTargetList();
        AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)rTargetList->PushIterator();
        while(rPackage)
        {
            //--Get the entity pointer.
            AdvCombatEntity *rEntity = GetEntityByID(rPackage->mTargetID);

            //--Match, add to list.
            if(rEntity == pEntity)
            {
                nEffectList->AddElement("X", rEffect);
            }

            //--Next.
            rPackage = (AdvCombatEffectTargetPack *)rTargetList->AutoIterate();
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }

    //--Return the list.
    return nEffectList;
}
void AdvCombat::StoreEffectsReferencing(uint32_t pID)
{
    //--Clear.
    mrTempEffectList->ClearList();
    if(pID == 0) return;

    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Run across all targets in the effect.
        StarLinkedList *rTargetList = rEffect->GetTargetList();
        AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)rTargetList->PushIterator();
        while(rPackage)
        {
            //--Match, add to list.
            if(rPackage->mTargetID == pID) mrTempEffectList->AddElement("X", rEffect);

            //--Next.
            rPackage = (AdvCombatEffectTargetPack *)rTargetList->AutoIterate();
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }
}
void AdvCombat::PushEffectByID(uint32_t pID)
{
    //--Push.
    DataLibrary::Fetch()->PushActiveEntity();

    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Matches ID.
        if(rEffect->GetID() == pID)
        {
            DataLibrary::Fetch()->rActiveObject = rEffect;
            mGlobalEffectList->PopIterator();
            return;
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }
}
void AdvCombat::ClearTemporaryEffectList()
{
    mrTempEffectList->ClearList();
}
void AdvCombat::MarkEffectForRemoval(uint32_t pID)
{
    ///--[Documentation]
    //--Locate the specific effect and remove it. This takes place immediately, and forces effect
    //  references to be rebuilt. All effect builder functions must rebuild their lists once this
    //  is called. The effect itself is not deallocated until the end of the tick.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->SetToHeadAndReturn();
    while(rEffect)
    {
        //--If the script flagged destruction for the effect, put it on the effect graveyard.
        if(rEffect->GetID() == pID)
        {
            //--Flag.
            mMustRebuildEffectReferences = true;

            //--Before removal, ask the effect to remove any stat modifications it made.
            rEffect->UnApplyStatsToTargets();

            //--Remove, place on graveyard.
            mGlobalEffectList->LiberateRandomPointerEntry();
            mEffectGraveyard->AddElement("X", rEffect, &RootObject::DeleteThis);
            break;
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->IncrementAndGetRandomPointerEntry();
    }

    //--Rebuild references.
    RebuildEffectReferences();
}
void AdvCombat::PulseEffectsForRemoval()
{
    //--Run across all effects and check any that need to be removed.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->SetToHeadAndReturn();
    while(rEffect)
    {
        //--If the script flagged destruction for the effect, put it on the effect graveyard.
        if(rEffect->IsRemovedNow())
        {
            //--Flag.
            mMustRebuildEffectReferences = true;

            //--Before removal, ask the effect to remove any stat modifications it made.
            rEffect->UnApplyStatsToTargets();

            //--Remove, place on graveyard.
            mGlobalEffectList->LiberateRandomPointerEntry();
            mEffectGraveyard->AddElement("X", rEffect, &RootObject::DeleteThis);
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->IncrementAndGetRandomPointerEntry();
    }

    //--Rebuild references.
    RebuildEffectReferences();
}
void AdvCombat::SortEffects()
{
    //--Sorts the global effect list by effect priority. Most effects have a priority of zero,
    //  but HoTs and DoTs have priorities so that HoTs always run last.
    mGlobalEffectList->SortListUsing(&AdvCombat::EffectCompare);

    //--Debug.
    if(true) return;

    //--Iterate.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }
}
void AdvCombat::ClearEffects()
{
    //--Clear global lists.
    mGlobalEffectList->ClearList();
    mrTempEffectList->ClearList();

    //--Iterate across all entities. Clear the effect render list for all of them.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->GetEffectRenderList()->ClearList();
        rEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }
    rEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->GetEffectRenderList()->ClearList();
        rEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }
}

///--[Worker Function for the Above Sort]
int AdvCombat::EffectCompare(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortEffects(). Compares the effect priority number, with ties broken by age.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Get the effects.
    AdvCombatEffect *rEffectA = (AdvCombatEffect *)(*rEntryA)->rData;
    AdvCombatEffect *rEffectB = (AdvCombatEffect *)(*rEntryB)->rData;

    //--Check their priority.
    if(rEffectA->GetEffectPriority() < rEffectB->GetEffectPriority())
    {
        return -1;
    }
    else if(rEffectA->GetEffectPriority() > rEffectB->GetEffectPriority())
    {
        return 1;
    }

    //--Same priority, compare timestamp.
    if(rEffectA->GetTimeIndex() < rEffectB->GetTimeIndex())
    {
        return -1;
    }
    else if(rEffectA->GetTimeIndex() > rEffectB->GetTimeIndex())
    {
        return 1;
    }

    //--Tie for both. Return -1 anyway.
    return -1;
}

///======================================= Pointer Routing ========================================
StarLinkedList *AdvCombat::GetTemporaryEffectList()
{
    return mrTempEffectList;
}
