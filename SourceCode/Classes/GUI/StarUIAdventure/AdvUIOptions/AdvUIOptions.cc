//--Base
#include "AdvUIOptions.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"
#include "SaveManager.h"

///========================================== System ==============================================
AdvUIOptions::AdvUIOptions()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIOptions ======= ]
    ///--[System]
    //--System
    mMode = ADVMENU_OPTIONS_MODE_GAMEPLAY;
    mArrowTimer = 0;

    //--Rebinding
    mIsRebinding = false;
    mIsRebindingSecondary = false;
    mRebindTimer = 0;
    mRebindLockout = 0;

    //--Unsaved Changes Dialogue
    mIsShowingUnsavedChanges = false;
    mUnsavedChangesTimer = 0;
    mAcceptString = new StarlightString();
    mCancelString = new StarlightString();

    //--Cursor
    mCursor = 0;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Volume Constants
    for(int i = 0; i < ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY; i ++) mAbstractVolumeCurrentSlots[i] = -1;
    memset(mAbstractVolumeCurrentNames, 0, sizeof(char) * ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY * 32);
    memset(mAbstractVolumeNames, 0, sizeof(char) * AM_ABSTRACT_VOLUME_TOTAL * 32);
    strcpy(mAbstractVolumeNames[0], "Izana");
    strcpy(mAbstractVolumeNames[1], "Cyrano");
    strcpy(mAbstractVolumeNames[2], "Caelyn");
    strcpy(mAbstractVolumeNames[3], "Mei");
    strcpy(mAbstractVolumeNames[4], "Florentina");
    strcpy(mAbstractVolumeNames[5], "Christine");
    strcpy(mAbstractVolumeNames[6], "Tiffany");
    strcpy(mAbstractVolumeNames[7], "SX-399");
    strcpy(mAbstractVolumeNames[8], "JX-101");

    //--Gameplay Values
    mAutoUseDoctorBag.Initialize();
    mAutoHastenDialogue.Initialize();
    mTouristMode.Initialize();
    mCombatMemoryCursor.Initialize();
    mAmbientLightBoost.Initialize();
    mShowCensorBars.Initialize();
    mMaxAutosaves.Initialize();
    mAllowAssetStreaming.Initialize();
    mUseRAMLoading.Initialize();
    mUnloadActors.Initialize();
    mUseFadingUI.Initialize();
    mExpertCombatUI.Initialize();
    mBlocksMustActivate.Initialize();
    mShowStaminaBar.Initialize();
    mDisallowOverlays.Initialize();
    mAutorun.Initialize();
    mResolution.Initialize();

    //--Audio Values
    mMusicVol.Initialize();
    mSoundVol.Initialize();
    mVoicePlay.Initialize();
    mAbstractVol0.Initialize();
    mAbstractVol1.Initialize();
    mAbstractVol2.Initialize();
    mAbstractVol3.Initialize();
    mIzanaVoiceVol.Initialize();
    mCaelynVoiceVol.Initialize();
    mCyranoVoiceVol.Initialize();
    mSaveVolumesInSavefile.Initialize();

    //--Control Values
    //--Debug Values
    mDisableShaderOutline.Initialize();
    mDisableShaderUnderwater.Initialize();
    mDisableShaderLighting.Initialize();

    //--Comparison Auto-Handler
    mHasUnsavedChanges = false;
    mRequiresRestart = false;
    mrOptionListList = new StarLinkedList(false);

    //--Details
    mIsShowingDetails = false;
    mDetailsTimer = 0;

    //--Strings
    mShowHelpString = new StarlightString();
    mInspectString = new StarlightString();
    mPageLftString = new StarlightString();
    mPageRgtString = new StarlightString();
    mBindPrimaryString = new StarlightString();
    mBindSecondaryString = new StarlightString();
    mSetToDefaultString = new StarlightString();
    mSetAllDefaultString = new StarlightString();

    //--Constants
    cOptionTop = 187.0f;
    cOptionHei = 26.0f;
    cNameLft = 406.0f;
    cBoolRgt = 961.0f;

    ///--[Lookup References]
    mrOptionListList->AddElementAsTail("X", &mAutoUseDoctorBag);
    mrOptionListList->AddElementAsTail("X", &mAutoHastenDialogue);
    mrOptionListList->AddElementAsTail("X", &mTouristMode);
    mrOptionListList->AddElementAsTail("X", &mCombatMemoryCursor);
    mrOptionListList->AddElementAsTail("X", &mAmbientLightBoost);
    mrOptionListList->AddElementAsTail("X", &mShowCensorBars);
    mrOptionListList->AddElementAsTail("X", &mMaxAutosaves);
    mrOptionListList->AddElementAsTail("X", &mAllowAssetStreaming);
    mrOptionListList->AddElementAsTail("X", &mCombatMemoryCursor);
    mrOptionListList->AddElementAsTail("X", &mUseRAMLoading);
    mrOptionListList->AddElementAsTail("X", &mUnloadActors);
    mrOptionListList->AddElementAsTail("X", &mUseFadingUI);
    mrOptionListList->AddElementAsTail("X", &mExpertCombatUI);
    mrOptionListList->AddElementAsTail("X", &mBlocksMustActivate);
    mrOptionListList->AddElementAsTail("X", &mShowStaminaBar);
    mrOptionListList->AddElementAsTail("X", &mDisallowOverlays);
    mrOptionListList->AddElementAsTail("X", &mAutorun);
    mrOptionListList->AddElementAsTail("X", &mMusicVol);
    mrOptionListList->AddElementAsTail("X", &mSoundVol);
    mrOptionListList->AddElementAsTail("X", &mVoicePlay);
    mrOptionListList->AddElementAsTail("X", &mIzanaVoiceVol);
    mrOptionListList->AddElementAsTail("X", &mCaelynVoiceVol);
    mrOptionListList->AddElementAsTail("X", &mCyranoVoiceVol);
    mrOptionListList->AddElementAsTail("X", &mSaveVolumesInSavefile);
    mrOptionListList->AddElementAsTail("X", &mDisableShaderOutline);
    mrOptionListList->AddElementAsTail("X", &mDisableShaderUnderwater);
    mrOptionListList->AddElementAsTail("X", &mDisableShaderLighting);

    //--Note: Resolution is resolved dynamically from multiple variables.

    ///--[Options Requiring Restart]
    mAllowAssetStreaming.mRequiresRestart = true;
    mUseRAMLoading.mRequiresRestart = true;
    mUnloadActors.mRequiresRestart = true;
    mResolution.mRequiresRestart = true;

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIOptions::~AdvUIOptions()
{
    delete mAcceptString;
    delete mCancelString;
    delete mShowHelpString;
    delete mInspectString;
    delete mPageLftString;
    delete mPageRgtString;
    delete mBindPrimaryString;
    delete mBindSecondaryString;
    delete mSetToDefaultString;
    delete mSetAllDefaultString;
    delete mrOptionListList;
}
void AdvUIOptions::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Options UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",    &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIOptions::TakeForeground()
{
    ///--[Variables]
    //--Local
    mMode = ADVMENU_OPTIONS_MODE_GAMEPLAY;
    mIsShowingUnsavedChanges = false;
    mCursor = 0;
    mHasUnsavedChanges = false;

    ///--[Strings]
    ReresolveOptionStrings();

    ///--[Active Characters]
    //--Resolve the names of up to four characters in the active party, and store them for later.
    AdvCombat *rCombat = AdvCombat::Fetch();
    for(int i = 0; i < ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY; i ++)
    {
        //--Get character.
        AdvCombatEntity *rCharacter = rCombat->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--Get internal name, copy it.
        const char *rInternalName = rCharacter->GetName();
        const char *rDisplayName = rCharacter->GetDisplayName();
        strncpy(mAbstractVolumeCurrentNames[i], rDisplayName, 31);

        //--Locate the character in the lookups and save their slot.
        for(int p = 0; p < AM_ABSTRACT_VOLUME_TOTAL; p ++)
        {
            if(!strcasecmp(rInternalName, mAbstractVolumeNames[p]))
            {
                mAbstractVolumeCurrentSlots[i] = p;
                break;
            }
        }
    }

    ///--[Options Values]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    rOptionsManager->mShowExplicitErrorsForOptionQueries = true;

    //--Gameplay Values
    mAutoUseDoctorBag.   SetB(rAdventureCombat->IsAutoUseDoctorBag());
    mAutoHastenDialogue. SetB(WorldDialogue::Fetch()->IsAutoHastening());
    mTouristMode.        SetB(rAdventureCombat->IsTouristMode());
    mCombatMemoryCursor. SetB(rAdventureCombat->IsMemoryCursor());
    mAmbientLightBoost.  SetF(AdventureLevel::Fetch()->GetLightBoost());
    mShowCensorBars.     SetB(rOptionsManager->GetOptionB("Show Censor Bars"));
    mMaxAutosaves.       SetI(rOptionsManager->GetOptionI("Max Autosaves"));
    mAllowAssetStreaming.SetB(rOptionsManager->GetOptionB("Allow Asset Streaming Nextboot"));
    mUseRAMLoading.      SetB(rOptionsManager->GetOptionB("UseRAMLoading Nextboot"));
    mUnloadActors.       SetB(rOptionsManager->GetOptionB("Actors Unload After Dialogue Nextboot"));
    mUseFadingUI.        SetB(rOptionsManager->GetOptionB("UI Transitions By Fade"));
    mExpertCombatUI.     SetB(rOptionsManager->GetOptionB("Expert Combat UI"));
    mBlocksMustActivate. SetB(rOptionsManager->GetOptionB("Blocks Activate Manually"));
    mShowStaminaBar.     SetB(rOptionsManager->GetOptionB("Show Stamina Bar"));
    mDisallowOverlays.   SetB(rOptionsManager->GetOptionB("Disallow World Overlays"));
    mAutorun.            SetB(rOptionsManager->GetOptionB("Autorun"));

    //--Resolution is resolved from the DisplayManager.
    int tResolutionVal = 0;
    DisplayManager::Fetch()->GetActiveDisplayMode(tResolutionVal);
    mResolution.SetI(tResolutionVal);

    //--Censorship vars set from the datalibrary.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rIzanaVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowIzanaLewd");
    if(rIzanaVar)
    {
        mCensorIzana.mCurValue.b = false;
        if(rIzanaVar->mNumeric == 0.0f) mCensorIzana.mCurValue.b = true;
    }
    SysVar *rCaelynVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowCaelynLewd");
    if(rCaelynVar)
    {
        mCensorCaelyn.mCurValue.b = false;
        if(rCaelynVar->mNumeric == 0.0f) mCensorCaelyn.mCurValue.b = true;
    }
    SysVar *rCyranoVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowCyranoLewd");
    if(rCyranoVar)
    {
        mCensorCyrano.mCurValue.b = false;
        if(rCyranoVar->mNumeric == 0.0f) mCensorCyrano.mCurValue.b = true;
    }
    SysVar *rEnemyVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowEnemyLewd");
    if(rEnemyVar)
    {
        mCensorEnemies.mCurValue.b = false;
        if(rEnemyVar->mNumeric == 0.0f) mCensorEnemies.mCurValue.b = true;
    }

    //--Audio Values
    mMusicVol.             SetF(AudioManager::xMusicVolume);
    mSoundVol.             SetF(AudioManager::xSoundVolume);
    mIzanaVoiceVol.        SetF(AudioManager::xAbstractVolume[0]);
    mCaelynVoiceVol.       SetF(AudioManager::xAbstractVolume[1]);
    mCyranoVoiceVol.       SetF(AudioManager::xAbstractVolume[2]);
    mSaveVolumesInSavefile.SetB(rOptionsManager->GetOptionB("Save Audio Levels In Savefile"));
    mVoicePlay.            SetI(rOptionsManager->GetOptionI("Voice Line Play Chance"));

    //--Abstract Volumes
    AdvMenuOptionPack *trPackList[ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY];
    trPackList[0] = &mAbstractVol0;
    trPackList[1] = &mAbstractVol1;
    trPackList[2] = &mAbstractVol2;
    trPackList[3] = &mAbstractVol3;
    for(int i = 0; i < ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY; i ++)
    {
        if(mAbstractVolumeCurrentSlots[i] == -1) continue;
        trPackList[i]->SetF(AudioManager::xAbstractVolume[mAbstractVolumeCurrentSlots[i]]);
    }

    //--Control Values
    //--Debug Values
    mDisableShaderOutline.   SetB(rOptionsManager->GetOptionB("Disable Shader TilemapActor ColorOutline"));
    mDisableShaderUnderwater.SetB(rOptionsManager->GetOptionB("Disable Shader AdventureLevel Underwater"));
    mDisableShaderLighting.  SetB(rOptionsManager->GetOptionB("Disable Shader AdventureLevel Lighting"));

    ///--[Cursor]
    RecomputeCursorPositions();
    mHighlightPos.mTimer  = mHighlightPos.mTimerMax - 1;
    mHighlightSize.mTimer = mHighlightSize.mTimerMax - 1;
    mHighlightPos.Increment(EASING_CODE_QUADINOUT);
    mHighlightSize.Increment(EASING_CODE_QUADINOUT);

    ///--[Joystick Handling]
    //--Call this to handle the joysticks possibly being plugged in or removed.
    #if defined _ALLEGRO_PROJECT_
        al_reconfigure_joysticks();
    #endif

    //--Clean.
    rOptionsManager->mShowExplicitErrorsForOptionQueries = false;
}

///======================================= Core Methods ===========================================
void AdvUIOptions::ReresolveOptionStrings()
{
    ///--[Documentation]
    //--Rechecks all strings for control images. This UI can rebind controls directly.

    //--"Accept" string
    mAcceptString->SetString("[IMG0] Discard Changes");
    mAcceptString->AllocateImages(1);
    mAcceptString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mAcceptString->CrossreferenceImages();

    //--"Cancel" string
    mCancelString->SetString("[IMG0] Return to Options Menu");
    mCancelString->AllocateImages(1);
    mCancelString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mCancelString->CrossreferenceImages();

    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    //--"Item Details" string
    mInspectString->SetString("[IMG0] Option Details");
    mInspectString->AllocateImages(1);
    mInspectString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F2"));
    mInspectString->CrossreferenceImages();

    //--"Previous Page" string
    mPageLftString->SetString("[IMG0] Previous Page");
    mPageLftString->AllocateImages(1);
    mPageLftString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("UpLevel"));
    mPageLftString->CrossreferenceImages();

    //--"Next Page" string
    mPageRgtString->SetString("[IMG0] Next Page");
    mPageRgtString->AllocateImages(1);
    mPageRgtString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mPageRgtString->CrossreferenceImages();

    //--"Bind Primary" string
    mBindPrimaryString->SetString("[IMG0] Bind Primary");
    mBindPrimaryString->AllocateImages(1);
    mBindPrimaryString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mBindPrimaryString->CrossreferenceImages();

    //--"Bind Secondary" string
    mBindSecondaryString->SetString("[IMG0] Bind Secondary");
    mBindSecondaryString->AllocateImages(1);
    mBindSecondaryString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mBindSecondaryString->CrossreferenceImages();

    //--"Restore Defaults" string
    mSetToDefaultString->SetString("[IMG0] Restore Control Defaults");
    mSetToDefaultString->AllocateImages(1);
    mSetToDefaultString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F3"));
    mSetToDefaultString->CrossreferenceImages();

    //--"Restore All Control Defaults" string.
    mSetAllDefaultString->SetString("[IMG0] Restore All Control Defaults");
    mSetAllDefaultString->AllocateImages(1);
    mSetAllDefaultString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F5"));
    mSetAllDefaultString->CrossreferenceImages();
}
void AdvUIOptions::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Set lines.
    mHelpMenuStrings[i]->SetString("This is the options menu. Press [IMG0] or [IMG1] to change pages.");
    mHelpMenuStrings[i]->AllocateImages(2);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigControlOffset, rControlManager->ResolveControlImage("DnLevel"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigControlOffset, rControlManager->ResolveControlImage("UpLevel"));
    i ++;
    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("You can edit many options from the game. To save your options, select 'Okay' at the ");
    i ++;
    mHelpMenuStrings[i]->SetString("bottom of the menu. To cancel changes, press [IMG0] instead.");
    mHelpMenuStrings[i]->AllocateImages(1);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigControlOffset, rControlManager->ResolveControlImage("Cancel"));
    i ++;
    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("If you are unsure what an option does, highlight it and push [IMG0].");
    mHelpMenuStrings[i]->AllocateImages(1);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigControlOffset, rControlManager->ResolveControlImage("F2"));
    i ++;
    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("Press [IMG0] or [IMG1] to exit this help menu.");
    mHelpMenuStrings[i]->AllocateImages(2);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigControlOffset, rControlManager->ResolveControlImage("F1"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigControlOffset, rControlManager->ResolveControlImage("Cancel"));
    i ++;

    //--Error check.
    if(i > ADVMENU_OPTIONS_HELP_MENU_STRINGS)
    {
        fprintf(stderr, "Warning: Status Help Menu has too many strings %i vs %i\n", i, ADVMENU_OPTIONS_HELP_MENU_STRINGS);
    }

    //--Order all strings to crossreference images.
    for(int i = 0; i < ADVMENU_OPTIONS_HELP_MENU_STRINGS; i ++)
    {
        mHelpMenuStrings[i]->CrossreferenceImages();
    }
}
void AdvUIOptions::RecomputeCursorPositions()
{
    ///--[Okay/Defaults Case]
    //--Okay button.
    if((mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY && mCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_AUDIO    && mCursor == ADVMENU_OPTIONS_AUDIO_TOTAL    + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_CONTROLS && mCursor == ADVMENU_OPTIONS_CONTROLS_TOTAL + 0) ||
       (mMode == ADVMENU_OPTIONS_MODE_DEBUG    && mCursor == ADVMENU_OPTIONS_DEBUG_TOTAL    + 0))
    {
        mHighlightPos.MoveTo(449.0f, 686.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(154.0f, 51.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        return;
    }
    //--Defaults button.
    else if((mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY && mCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_AUDIO    && mCursor == ADVMENU_OPTIONS_AUDIO_TOTAL    + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_CONTROLS && mCursor == ADVMENU_OPTIONS_CONTROLS_TOTAL + 1) ||
            (mMode == ADVMENU_OPTIONS_MODE_DEBUG    && mCursor == ADVMENU_OPTIONS_DEBUG_TOTAL    + 1))
    {
        mHighlightPos.MoveTo(763.0f, 686.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(154.0f,  51.0f, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
        return;
    }

    ///--[Normal Case]
    //--Setup.
    float cLft = cNameLft - 13.0f;
    float cTop = cOptionTop + (cOptionHei * mCursor) - 6.0f;
    float cRgt = cBoolRgt + 12.0f;
    float cBot = cTop + cOptionHei + 9.0f;

    //--Set.
    mHighlightPos.MoveTo(cLft, cTop, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_OPTIONS_CURSOR_MOVE_TICKS);
}
void AdvUIOptions::RecheckChanges()
{
    ///--[Documentation]
    //--Runs across all stored variables and checks them against their original values. If different, then the unsaved
    //  changes flag is set to true.
    mHasUnsavedChanges = false;
    mRequiresRestart = false;
    AdvMenuOptionPack *rPackage = (AdvMenuOptionPack *)mrOptionListList->PushIterator();
    while(rPackage)
    {
        //--If the values are different, stop iterating and set the flag.
        if(rPackage->mCurValue.f != rPackage->mOldValue.f)
        {
            //--Mark as requiring saving.
            mHasUnsavedChanges = true;

            //--If this flag is set, restart is required.
            if(rPackage->mRequiresRestart)
            {
                mRequiresRestart = true;
            }
        }

        rPackage = (AdvMenuOptionPack *)mrOptionListList->AutoIterate();
    }

    //--Also check the resolution value which is not on the options list.
    if(mResolution.mCurValue.f != mResolution.mOldValue.f)
    {
        mHasUnsavedChanges = true;
        if(mResolution.mRequiresRestart)
        {
            mRequiresRestart = true;
        }
    }
}
void AdvUIOptions::SaveOptions()
{
    ///--[Documentation]
    //--Writes all the changed options and updates variables as needed.
    AdvCombat *rCombat = AdvCombat::Fetch();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--Set.
    WorldDialogue::Fetch()->SetAutoHastenFlag(mAutoHastenDialogue.mCurValue.b);
    rCombat->SetAutoUseDoctorBag(mAutoUseDoctorBag.mCurValue.b);
    rCombat->SetTouristMode(mTouristMode.mCurValue.b);
    rCombat->SetMemoryCursor(mCombatMemoryCursor.mCurValue.b);
    AdventureLevel::Fetch()->SetLightBoost(mAmbientLightBoost.mCurValue.f);
    rOptionsManager->SetOptionB("Show Censor Bars",                         mShowCensorBars.mCurValue.b);
    rOptionsManager->SetOptionI("Max Autosaves",                            mMaxAutosaves.mCurValue.i);
    rOptionsManager->SetOptionB("Allow Asset Streaming Nextboot",           mAllowAssetStreaming.mCurValue.b);
    rOptionsManager->SetOptionB("UseRAMLoading Nextboot",                   mUseRAMLoading.mCurValue.b);
    rOptionsManager->SetOptionB("Actors Unload After Dialogue Nextboot",    mUnloadActors.mCurValue.b);
    rOptionsManager->SetOptionB("UI Transitions By Fade",                   mUseFadingUI.mCurValue.b);
    rOptionsManager->SetOptionB("Expert Combat UI",                         mExpertCombatUI.mCurValue.b);
    rOptionsManager->SetOptionB("Blocks Activate Manually",                 mBlocksMustActivate.mCurValue.b);
    rOptionsManager->SetOptionB("Show Stamina Bar",                         mShowStaminaBar.mCurValue.b);
    rOptionsManager->SetOptionB("Save Audio Levels In Savefile",            mSaveVolumesInSavefile.mCurValue.b);
    rOptionsManager->SetOptionB("Disable Shader TilemapActor ColorOutline", mDisableShaderOutline.mCurValue.b);
    rOptionsManager->SetOptionB("Disable Shader AdventureLevel Underwater", mDisableShaderUnderwater.mCurValue.b);
    rOptionsManager->SetOptionB("Disable Shader AdventureLevel Lighting",   mDisableShaderLighting.mCurValue.b);
    rOptionsManager->SetOptionB("Disallow World Overlays",                  mDisallowOverlays.mCurValue.b);
    rOptionsManager->SetOptionB("Autorun",                                  mAutorun.mCurValue.b);
    rOptionsManager->SetOptionI("Voice Line Play Chance",                   mVoicePlay.mCurValue.i);

    //--Resolution is set by resolving values from the DisplayManager.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    int tTotalDisplayModes = rDisplayManager->GetTotalDisplayModes();
    DisplayInfo tDisplayInfo = rDisplayManager->GetDisplayMode(mResolution.mCurValue.i);
    if(mResolution.mCurValue.i >= 0 && mResolution.mCurValue.i < tTotalDisplayModes)
    {
        //fprintf(stderr, "Set resolution to slot %i, %i x %i : %i\n", mResolution.mCurValue.i, tDisplayInfo.mWidth, tDisplayInfo.mHeight, tDisplayInfo.mRefreshRate);
        rOptionsManager->SetOptionI("WinSizeX", tDisplayInfo.mWidth);
        rOptionsManager->SetOptionI("WinSizeY", tDisplayInfo.mHeight);
    }

    //--Censorship options toggle variables in the DataLibrary.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rIzanaVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowIzanaLewd");
    if(rIzanaVar)
    {
        rIzanaVar->mNumeric = 1.0f;
        if(mCensorIzana.mCurValue.b) rIzanaVar->mNumeric = 0.0f;
    }
    SysVar *rCaelynVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowCaelynLewd");
    if(rCaelynVar)
    {
        rCaelynVar->mNumeric = 1.0f;
        if(mCensorCaelyn.mCurValue.b) rCaelynVar->mNumeric = 0.0f;
    }
    SysVar *rCyranoVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowCyranoLewd");
    if(rCyranoVar)
    {
        rCyranoVar->mNumeric = 1.0f;
        if(mCensorCyrano.mCurValue.b) rCyranoVar->mNumeric = 0.0f;
    }
    SysVar *rEnemyVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Monoceros/CostumeOptions/iShowEnemyLewd");
    if(rEnemyVar)
    {
        rEnemyVar->mNumeric = 1.0f;
        if(mCensorEnemies.mCurValue.b) rEnemyVar->mNumeric = 0.0f;
    }

    //--Write the engine config to save the results.
    OptionsManager::Fetch()->WriteConfigFiles();

    //--Closing script.
    SetCloseCode("Change Options");
}
void AdvUIOptions::CancelOptions()
{
    ///--[Documentation]
    //--For options that changed dynamically in the menu, resets them back to their starting values.
    AudioManager *rAudioManager = AudioManager::Fetch();
    rAudioManager->ChangeMusicVolumeTo   (   mMusicVol.mOldValue.f);
    rAudioManager->ChangeSoundVolumeTo   (   mSoundVol.mOldValue.f);

    //--Used in Witch Hunter Izana.
    rAudioManager->ChangeAbstractVolumeTo(0, mIzanaVoiceVol.mOldValue.f);
    rAudioManager->ChangeAbstractVolumeTo(1, mCaelynVoiceVol.mOldValue.f);
    rAudioManager->ChangeAbstractVolumeTo(2, mCyranoVoiceVol.mOldValue.f);

    //--Runes of Pandemonium, uses a variable list.
    AdvMenuOptionPack *trPackList[ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY];
    trPackList[0] = &mAbstractVol0;
    trPackList[1] = &mAbstractVol0;
    trPackList[2] = &mAbstractVol0;
    trPackList[3] = &mAbstractVol0;
    for(int i = 0; i < ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY; i ++)
    {
        //--If the character slot is -1, do nothing.
        if(mAbstractVolumeCurrentSlots[i] == -1) continue;

        //--Otherwise, save the value.
        rAudioManager->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[i], trPackList[i]->mOldValue.f);
    }
}
void AdvUIOptions::SetDefaultOptions()
{
    ///--[Documentation]
    //--Sets all "Current" values to the "Default" values for all options. Doesn't affect controls,
    //  that has its own defaults set.

    //--Gameplay Values
    mAutoUseDoctorBag.mCurValue.b = true;
    mAutoHastenDialogue.mCurValue.b = false;
    mTouristMode.mCurValue.b = false;
    mCombatMemoryCursor.mCurValue.b = true;
    mAmbientLightBoost.mCurValue.f = 0.0f;
    mShowCensorBars.mCurValue.b = false;
    mMaxAutosaves.mCurValue.i = 10;
    mAllowAssetStreaming.mCurValue.b = true;
    mUseRAMLoading.mCurValue.b = true;
    mUnloadActors.mCurValue.b = false;
    mUseFadingUI.mCurValue.b = true;
    mExpertCombatUI.mCurValue.b = false;
    mBlocksMustActivate.mCurValue.b = false;
    mShowStaminaBar.mCurValue.b = false;
    mDisallowOverlays.mCurValue.b = false;
    mAutorun.mCurValue.b = false;

    //--Resolution does not get reset.

    //--Audio Values
    mMusicVol.mCurValue.f = 0.50f;
    mSoundVol.mCurValue.f = 0.75f;
    mVoicePlay.mCurValue.i = 0;
    mIzanaVoiceVol.mCurValue.f = 0.50f;
    mCaelynVoiceVol.mCurValue.f = 0.50f;
    mCyranoVoiceVol.mCurValue.f = 0.50f;
    mSaveVolumesInSavefile.mCurValue.b = false;

    //--Control Values
    //--Debug Values
    mDisableShaderOutline.mCurValue.b = false;
    mDisableShaderUnderwater.mCurValue.b = false;
    mDisableShaderLighting.mCurValue.b = false;

    //--Update audio.
    AudioManager *rAudioManager = AudioManager::Fetch();
    rAudioManager->ChangeMusicVolumeTo(mMusicVol.mCurValue.f);
    rAudioManager->ChangeSoundVolumeTo(mSoundVol.mCurValue.f);

    //--All abstract volumes go back to 0.50f.
    for(int i = 0; i < AM_ABSTRACT_VOLUME_TOTAL; i ++)
    {
        rAudioManager->ChangeAbstractVolumeTo(i, 0.50f);
    }
}
void AdvUIOptions::SetDefaultControl(int pCursor)
{
    ///--[Documentation]
    //--Given a control cursor, resets it to its default key.
    ControlManager *rControlManager = ControlManager::Fetch();
    #if defined _ALLEGRO_PROJECT_
    if(pCursor == ADVMENU_OPTIONS_CONTROLS_ACTIVATE)
    {
        rControlManager->OverbindControl("Activate",        false, ALLEGRO_KEY_Z, -1, -1);
        rControlManager->OverbindControl("Activate",        true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_CANCEL)
    {
        rControlManager->OverbindControl("Cancel",          false, ALLEGRO_KEY_X, -1, -1);
        rControlManager->OverbindControl("Cancel",          true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_RUN)
    {
        rControlManager->OverbindControl("Run",             false, ALLEGRO_KEY_LSHIFT, -1, -1);
        rControlManager->OverbindControl("Run",             true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_UP)
    {
        rControlManager->OverbindControl("Up",              false, ALLEGRO_KEY_UP, -1, -1);
        rControlManager->OverbindControl("Up",              true,  ALLEGRO_KEY_W, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_RIGHT)
    {
        rControlManager->OverbindControl("Right",           false, ALLEGRO_KEY_RIGHT, -1, -1);
        rControlManager->OverbindControl("Right",           true,  ALLEGRO_KEY_D, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_DOWN)
    {
        rControlManager->OverbindControl("Down",            false, ALLEGRO_KEY_DOWN, -1, -1);
        rControlManager->OverbindControl("Down",            true,  ALLEGRO_KEY_S, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_LEFT)
    {
        rControlManager->OverbindControl("Left",            false, ALLEGRO_KEY_LEFT, -1, -1);
        rControlManager->OverbindControl("Left",            true,  ALLEGRO_KEY_A, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERLEFT)
    {
        rControlManager->OverbindControl("UpLevel",         false, ALLEGRO_KEY_Q, -1, -1);
        rControlManager->OverbindControl("UpLevel",         true,  ALLEGRO_KEY_PGUP, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)
    {
        rControlManager->OverbindControl("DnLevel",         false, ALLEGRO_KEY_E, -1, -1);
        rControlManager->OverbindControl("DnLevel",         true,  ALLEGRO_KEY_PGDN, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_FIELDABILITIES)
    {
        rControlManager->OverbindControl("Field Abilities", false, ALLEGRO_KEY_C, -1, -1);
        rControlManager->OverbindControl("Field Abilities", true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_1)
    {
        rControlManager->OverbindControl("FieldAbility0",   false, ALLEGRO_KEY_1, -1, -1);
        rControlManager->OverbindControl("FieldAbility0",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_2)
    {
        rControlManager->OverbindControl("FieldAbility1",   false, ALLEGRO_KEY_2, -1, -1);
        rControlManager->OverbindControl("FieldAbility1",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_3)
    {
        rControlManager->OverbindControl("FieldAbility2",   false, ALLEGRO_KEY_3, -1, -1);
        rControlManager->OverbindControl("FieldAbility2",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_4)
    {
        rControlManager->OverbindControl("FieldAbility3",   false, ALLEGRO_KEY_4, -1, -1);
        rControlManager->OverbindControl("FieldAbility3",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_5)
    {
        rControlManager->OverbindControl("FieldAbility4",   false, ALLEGRO_KEY_5, -1, -1);
        rControlManager->OverbindControl("FieldAbility4",   true,  -1, -1, -1);
    }
    #endif
    #if defined _SDL_PROJECT_
    if(pCursor == ADVMENU_OPTIONS_CONTROLS_ACTIVATE)
    {
        rControlManager->OverbindControl("Activate",        false, SDL_SCANCODE_Z, -1, -1);
        rControlManager->OverbindControl("Activate",        true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_CANCEL)
    {
        rControlManager->OverbindControl("Cancel",          false, SDL_SCANCODE_X, -1, -1);
        rControlManager->OverbindControl("Cancel",          true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_RUN)
    {
        rControlManager->OverbindControl("Run",             false, SDL_SCANCODE_LSHIFT, -1, -1);
        rControlManager->OverbindControl("Run",             true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_UP)
    {
        rControlManager->OverbindControl("Up",              false, SDL_SCANCODE_UP, -1, -1);
        rControlManager->OverbindControl("Up",              true,  SDL_SCANCODE_W, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_RIGHT)
    {
        rControlManager->OverbindControl("Right",           false, SDL_SCANCODE_RIGHT, -1, -1);
        rControlManager->OverbindControl("Right",           true,  SDL_SCANCODE_D, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_DOWN)
    {
        rControlManager->OverbindControl("Down",            false, SDL_SCANCODE_DOWN, -1, -1);
        rControlManager->OverbindControl("Down",            true,  SDL_SCANCODE_S, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_LEFT)
    {
        rControlManager->OverbindControl("Left",            false, SDL_SCANCODE_LEFT, -1, -1);
        rControlManager->OverbindControl("Left",            true,  SDL_SCANCODE_A, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERLEFT)
    {
        rControlManager->OverbindControl("UpLevel",         false, SDL_SCANCODE_Q, -1, -1);
        rControlManager->OverbindControl("UpLevel",         true,  SDL_SCANCODE_PAGEUP, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)
    {
        rControlManager->OverbindControl("DnLevel",         false, SDL_SCANCODE_E, -1, -1);
        rControlManager->OverbindControl("DnLevel",         true,  SDL_SCANCODE_PAGEDOWN, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_FIELDABILITIES)
    {
        rControlManager->OverbindControl("Field Abilities", false, SDL_SCANCODE_C, -1, -1);
        rControlManager->OverbindControl("Field Abilities", true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_1)
    {
        rControlManager->OverbindControl("FieldAbility0",   false, SDL_SCANCODE_1, -1, -1);
        rControlManager->OverbindControl("FieldAbility0",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_2)
    {
        rControlManager->OverbindControl("FieldAbility1",   false, SDL_SCANCODE_2, -1, -1);
        rControlManager->OverbindControl("FieldAbility1",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_3)
    {
        rControlManager->OverbindControl("FieldAbility2",   false, SDL_SCANCODE_3, -1, -1);
        rControlManager->OverbindControl("FieldAbility2",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_4)
    {
        rControlManager->OverbindControl("FieldAbility3",   false, SDL_SCANCODE_4, -1, -1);
        rControlManager->OverbindControl("FieldAbility3",   true,  -1, -1, -1);
    }
    else if(pCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_5)
    {
        rControlManager->OverbindControl("FieldAbility4",   false, SDL_SCANCODE_5, -1, -1);
        rControlManager->OverbindControl("FieldAbility4",   true,  -1, -1, -1);
    }
    #endif

    //--Write to the save file.
    SaveManager::Fetch()->SaveControlsFile();
}
void AdvUIOptions::SetAllDefaultControls()
{
    ///--[Documentation]
    //--Resets all standard controls to their defaults.
    ControlManager *rControlManager = ControlManager::Fetch();
    #if defined _ALLEGRO_PROJECT_
    rControlManager->OverbindControl("Up",              false, ALLEGRO_KEY_UP, -1, -1);
    rControlManager->OverbindControl("Up",              true,  ALLEGRO_KEY_W, -1, -1);
    rControlManager->OverbindControl("Left",            false, ALLEGRO_KEY_LEFT, -1, -1);
    rControlManager->OverbindControl("Left",            true,  ALLEGRO_KEY_A, -1, -1);
    rControlManager->OverbindControl("Down",            false, ALLEGRO_KEY_DOWN, -1, -1);
    rControlManager->OverbindControl("Down",            true,  ALLEGRO_KEY_S, -1, -1);
    rControlManager->OverbindControl("Right",           false, ALLEGRO_KEY_RIGHT, -1, -1);
    rControlManager->OverbindControl("Right",           true,  ALLEGRO_KEY_D, -1, -1);
    rControlManager->OverbindControl("Activate",        false, ALLEGRO_KEY_Z, -1, -1);
    rControlManager->OverbindControl("Activate",        true,  -1, -1, -1);
    rControlManager->OverbindControl("Cancel",          false, ALLEGRO_KEY_X, -1, -1);
    rControlManager->OverbindControl("Cancel",          true,  -1, -1, -1);
    rControlManager->OverbindControl("Run",             false, ALLEGRO_KEY_LSHIFT, -1, -1);
    rControlManager->OverbindControl("Run",             true,  -1, -1, -1);
    rControlManager->OverbindControl("UpLevel",         false, ALLEGRO_KEY_Q, -1, -1);
    rControlManager->OverbindControl("UpLevel",         true,  ALLEGRO_KEY_PGUP, -1, -1);
    rControlManager->OverbindControl("DnLevel",         false, ALLEGRO_KEY_E, -1, -1);
    rControlManager->OverbindControl("DnLevel",         true,  ALLEGRO_KEY_PGDN, -1, -1);
    rControlManager->OverbindControl("Field Abilities", false, ALLEGRO_KEY_C, -1, -1);
    rControlManager->OverbindControl("Field Abilities", true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility0",   false, ALLEGRO_KEY_1, -1, -1);
    rControlManager->OverbindControl("FieldAbility0",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility1",   false, ALLEGRO_KEY_2, -1, -1);
    rControlManager->OverbindControl("FieldAbility1",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility2",   false, ALLEGRO_KEY_3, -1, -1);
    rControlManager->OverbindControl("FieldAbility2",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility3",   false, ALLEGRO_KEY_4, -1, -1);
    rControlManager->OverbindControl("FieldAbility3",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility4",   false, ALLEGRO_KEY_5, -1, -1);
    rControlManager->OverbindControl("FieldAbility4",   true,  -1, -1, -1);
    #endif
    #if defined _SDL_PROJECT_
    rControlManager->OverbindControl("Up",              false, SDL_SCANCODE_UP, -1, -1);
    rControlManager->OverbindControl("Up",              true,  SDL_SCANCODE_W, -1, -1);
    rControlManager->OverbindControl("Left",            false, SDL_SCANCODE_LEFT, -1, -1);
    rControlManager->OverbindControl("Left",            true,  SDL_SCANCODE_A, -1, -1);
    rControlManager->OverbindControl("Down",            false, SDL_SCANCODE_DOWN, -1, -1);
    rControlManager->OverbindControl("Down",            true,  SDL_SCANCODE_S, -1, -1);
    rControlManager->OverbindControl("Right",           false, SDL_SCANCODE_RIGHT, -1, -1);
    rControlManager->OverbindControl("Right",           true,  SDL_SCANCODE_D, -1, -1);
    rControlManager->OverbindControl("Activate",        false, SDL_SCANCODE_Z, -1, -1);
    rControlManager->OverbindControl("Activate",        true,  -1, -1, -1);
    rControlManager->OverbindControl("Cancel",          false, SDL_SCANCODE_X, -1, -1);
    rControlManager->OverbindControl("Cancel",          true,  -1, -1, -1);
    rControlManager->OverbindControl("Run",             false, SDL_SCANCODE_LSHIFT, -1, -1);
    rControlManager->OverbindControl("Run",             true,  -1, -1, -1);
    rControlManager->OverbindControl("UpLevel",         false, SDL_SCANCODE_Q, -1, -1);
    rControlManager->OverbindControl("UpLevel",         true,  SDL_SCANCODE_PAGEUP, -1, -1);
    rControlManager->OverbindControl("DnLevel",         false, SDL_SCANCODE_E, -1, -1);
    rControlManager->OverbindControl("DnLevel",         true,  SDL_SCANCODE_PAGEDOWN, -1, -1);
    rControlManager->OverbindControl("Field Abilities", false, SDL_SCANCODE_C, -1, -1);
    rControlManager->OverbindControl("Field Abilities", true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility0",   false, SDL_SCANCODE_1, -1, -1);
    rControlManager->OverbindControl("FieldAbility0",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility1",   false, SDL_SCANCODE_2, -1, -1);
    rControlManager->OverbindControl("FieldAbility1",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility2",   false, SDL_SCANCODE_3, -1, -1);
    rControlManager->OverbindControl("FieldAbility2",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility3",   false, SDL_SCANCODE_4, -1, -1);
    rControlManager->OverbindControl("FieldAbility3",   true,  -1, -1, -1);
    rControlManager->OverbindControl("FieldAbility4",   false, SDL_SCANCODE_5, -1, -1);
    rControlManager->OverbindControl("FieldAbility4",   true,  -1, -1, -1);
    #endif

    //--Write to the save file.
    SaveManager::Fetch()->SaveControlsFile();
}

///=================================== Private Core Methods =======================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
