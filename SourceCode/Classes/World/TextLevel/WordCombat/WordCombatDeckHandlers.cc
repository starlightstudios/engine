//--Base
#include "WordCombat.h"

//--Classes
#include "WordCard.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"

///===================================== Property Queries =========================================
int WordCombat::CanPlaceCard(WordCard *pCard)
{
    ///--[Documentation]
    //--Returns whether or not the given card can be clicked to be placed into the sentence. Three possible
    //  return values come back:
    //  These are: WC_DO_NOT_HIGHLIGHT, WC_GREY_OUT, and WC_HIGHLIGHT.
    //--Hopefully you can guess what they want you to do.
    if(!pCard) return WC_DO_NOT_HIGHLIGHT;

    //--Build valid destinations.
    BuildDestinationMarkers(pCard);

    //--The first valid destination found is used, if it is unoccupied.
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
    {
        //--Skip invalid destinations.
        if(!mDestinationActivities[i]) continue;

        //--If the destination is valid and unoccupied, this card can fill it. Return a highlight.
        if(!rSentenceHand[i]) return WC_HIGHLIGHT;
    }

    //--If we got this far, and it's an action card, then it's a replacement operation.
    if(pCard->GetCardType() == WORDCARD_TYPE_ACTION)
    {
        return WC_DO_NOT_HIGHLIGHT;
    }

    //--Otherwise, no valid spots. Grey it out.
    return WC_GREY_OUT;
}

int WordCombat::GetFirstUnusedSlot()
{
    //--Scans through the slots and returns the lowest one that is unoccupied.
    bool tIsOccupied[WC_MAX_CARDS_IN_HAND];
    memset(tIsOccupied, 0, sizeof(bool) * WC_MAX_CARDS_IN_HAND);

    //--Iterate across all cards in the hand, marking which slots are occupied.
    WordCard *rHandCard = (WordCard *)mrPlayerHand->PushIterator();
    while(rHandCard)
    {
        //--Mark.
        int tIndex = rHandCard->GetAssignedIndex();
        if(tIndex >= 0 && tIndex < WC_MAX_CARDS_IN_HAND) tIsOccupied[tIndex] = true;

        //--Next.
        rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
    }

    //--Now scan forwards through the slots. Return the first not-occupied slot.
    for(int i = 0; i < WC_MAX_CARDS_IN_HAND; i ++)
    {
        if(!tIsOccupied[i]) return i;
    }

    //--No empty slots!
    return -1;
}

///======================================= Manipulators ===========================================
bool WordCombat::AutoplaceCard(WordCard *pCard)
{
    //--Attempts to place the selected card in the hand. If no valid positions can be found, returns
    //  false. Otherwise, moves the card and returns true.
    //--If this card can only move to replace another, it only activates for Action cards. Elemental
    //  cards do not replace others.
    if(!pCard) return false;

    //--Build valid destinations.
    BuildDestinationMarkers(pCard);

    //--The first valid destination found is used, if it is unoccupied.
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
    {
        //--Skip invalid destinations.
        if(!mDestinationActivities[i]) continue;

        //--If the destination is valid and unoccupied, fill it!
        if(!rSentenceHand[i])
        {
            //--Liberate the card from our hand and place it in the sentence's hand.
            mrPlayerHand->RemoveElementP(pCard);
            rSentenceHand[i] = pCard;

            //--Order the card to reposition.
            pCard->MoveToSlide(mDestinationPositions[i], mDestinationButtons[i].mYCenter);

            //--At least one card had to move, so reposition all cards in the hand.
            PositionCardsInHand();
            mDisplayPower = ComputeActionPower(false);
            return true;
        }
    }

    //--If we got this far, then all slots were either invalid or occupied. If this is an action
    //  card then we can replace the existing card.
    if(pCard->GetCardType() == WORDCARD_TYPE_ACTION)
    {
        //--The existing action card should be dumped into the hand.
        if(rSentenceHand[WC_DESTINATION_ACTION])
        {
            //--If both cards are of the same effect type, this is a "free" move.
            if(pCard->GetEffectType() == rSentenceHand[WC_DESTINATION_ACTION]->GetEffectType())
            {
                mLastReplaceWasFree = true;
            }

            //--Dump.
            rSentenceHand[WC_DESTINATION_ACTION]->SetAssignedIndex(-1);
            mrPlayerHand->AddElementAsTail("X", rSentenceHand[WC_DESTINATION_ACTION]);
        }

        //--Liberate the card from our hand and place it in the sentence's hand.
        mrPlayerHand->RemoveElementP(pCard);
        rSentenceHand[WC_DESTINATION_ACTION] = pCard;

        //--Order the card to reposition.
        pCard->MoveToSlide(mDestinationPositions[WC_DESTINATION_ACTION], mDestinationButtons[WC_DESTINATION_ACTION].mYCenter);

        //--At least one card had to move, so reposition all cards in the hand.
        PositionCardsInHand();
        mDisplayPower = ComputeActionPower(false);
        return true;
    }

    //--Couldn't find a place to put the card. Return false.
    return false;
}

void WordCombat::DrawCard()
{
    //--Draws a card from the deck and adds it to the hand. This is ignored in wait mode.
    mDeckPower = mDeckPower - mDeckPowerPerCard;

    //--SFX.
    AudioManager::Fetch()->PlaySound("World|FlipSwitch");

    //--In wait mode, add time.
    if(mIsWaitMode)
    {
        //--If the player has a free draw, use that up:
        if(mFreeDrawsLeft > 0)
        {
            mFreeDrawsLeft --;
        }
        //--Otherwise, time passes.
        else
        {
            mTicksAllowed += mTicksPerCardDraw;
            CreateEnemyProgressIndicator(mTicksPerCardDraw);
        }
    }

    //--If the player's hand is currently full, pick one card at random and move it to the
    //  graveyard.
    if(mrPlayerHand->GetListSize() >= mPlayerMaxHandSize)
    {
        //--Roll a random card.
        int tHandSize = mrPlayerHand->GetListSize();
        int tRoll = rand() % tHandSize;

        //--Remove the card from the deck and move it to the graveyard.
        WordCard *rHandCard = (WordCard *)mrPlayerHand->RemoveElementI(tRoll);
        mrPlayerGraveyard->AddElementAsTail("X", rHandCard);

        //--Graveyard cards move to the bottom of the screen.
        rHandCard->MoveToSlide(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 200.0f);
        rHandCard->SetAssignedIndex(-1);
    }

    //--Roll a random card.
    int tDeckSize = mrPlayerDeck->GetListSize();
    int tRoll = rand() % tDeckSize;

    //--Remove the card from the deck and move it to the hand.
    WordCard *rDeckCard = (WordCard *)mrPlayerDeck->RemoveElementI(tRoll);
    mrPlayerHand->AddElementAsTail("X", rDeckCard);

    //--Order the card to position itself offscreen.
    rDeckCard->SetAssignedIndex(-1);
    //rDeckCard->MoveToInstantly(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 150.0f);
    PositionCardsInHand();
}
void WordCombat::BeginShuffle()
{
    //--Forces shuffling to activate. In auto-draw mode, clicking the shuffle button always triggers
    //  this. Shuffling will reset the player's hand but takes a few seconds.
    mIsShuffling = true;
    mShuffleTimer = 0;
    mShuffleTimerMax = 300;
    if(mIsWaitMode) mShuffleTimerMax = 55;
    mDisplayPower = 0;

    //--SFX.
    AudioManager::Fetch()->PlaySound("World|TakeItem");

    //--Clear all cards.
    mrPlayerGraveyard->ClearList();
    mrPlayerDeck->ClearList();
    mrPlayerHand->ClearList();

    //--All cards get sent below the screen that aren't already. They also move to the graveyard.
    WordCard *rUpdateCard = (WordCard *)mGlobalCardDeck->PushIterator();
    while(rUpdateCard)
    {
        mrPlayerGraveyard->AddElement("X", rUpdateCard);
        rUpdateCard->SetAssignedIndex(-1);
        rUpdateCard->MoveToSlide(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 200.0f);
        rUpdateCard = (WordCard *)mGlobalCardDeck->AutoIterate();
    }

    //--If the Ace card is deployed, move it offscreen.
    if(mAceTimer < WC_ACE_TICKS && mAceCard)
    {
        mAceCard->MoveToSlide(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 200.0f);
    }

    //--Cards are cleared from all locations.
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
    {
        rSentenceHand[i] = NULL;
    }
}
void WordCombat::RegisterSentencePack(const char *pString, StarBitmap *pImage, ...)
{
    //--Worker function, adds a sentence package. These display in the top left to indicate how much damage
    //  or blocking the player did.
    if(!pString) return;

    //--Basics.
    SetMemoryData(__FILE__, __LINE__);
    SentencePack *nPack = (SentencePack *)starmemoryalloc(sizeof(SentencePack));
    nPack->Init();
    nPack->rDisplayImg = pImage;
    mSentenceResultList->AddElementAsTail("X", nPack, &FreeThis);

    //--Variable argument list pattern used for the text.
    va_list tArgList;
    va_start(tArgList, pImage);
    vsprintf(nPack->mSentence, pString, tArgList);
    va_end (tArgList);
}
void WordCombat::MoveSentenceCardToHand(int pIndex)
{
    //--Given a sentence index, moves the card to the player's hand.
    if(pIndex < 0 || pIndex >= WC_DESTINATIONS_MAX) return;
    if(!rSentenceHand[pIndex]) return;

    //--If the hand already has the maximum number of cards - discard the card!
    if(mrPlayerHand->GetListSize() >= WC_MAX_CARDS_IN_HAND)
    {
        //--Neutralize.
        rSentenceHand[pIndex]->SetAssignedIndex(-1);

        //--Order it to move to the bottom of the screen.
        rSentenceHand[pIndex]->MoveToSlide(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 200.0f);

        //--Move to graveyard.
        if(rSentenceHand[pIndex] != mAceCard) mrPlayerGraveyard->AddElement("X", rSentenceHand[pIndex]);
        rSentenceHand[pIndex] = NULL;
    }
    //--Otherwise, move it to the player's hand.
    else
    {
        //--Register it to the player's hand, NULL off the sentence slot.
        rSentenceHand[pIndex]->SetAssignedIndex(-1);
        mrPlayerHand->AddElementAsTail("X", rSentenceHand[pIndex]);
        rSentenceHand[pIndex] = NULL;

        //--At least one card had to move, so reposition all cards in the hand.
        PositionCardsInHand();

        //--Recompute the power of the hand.
        mDisplayPower = ComputeActionPower(false);
    }
}

///======================================= Core Methods ===========================================
void WordCombat::ShuffleDeck()
{
    //--Selects as many cards as can be held in the hand from the deck. This is used at the start
    //  of combat. All cards from the graveyard get recycled into the hand if there are any.
    //--First, recycle all cards. Only the master copies are left.
    mrPlayerGraveyard->ClearList();
    mrPlayerDeck->ClearList();
    mrPlayerHand->ClearList();

    //--Now create references to all cards in the master list to the player's deck.
    WordCard *rCardReference = (WordCard *)mGlobalCardDeck->PushIterator();
    while(rCardReference)
    {
        rCardReference->MoveToInstantly(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 150.0f);
        mrPlayerDeck->AddElementAsTail("X", rCardReference);
        rCardReference = (WordCard *)mGlobalCardDeck->AutoIterate();
    }

    //--Now start picking cards for the hand until we run out of cards or fill the hand.
    int tCardsToAdd = mPlayerMaxHandSize - 2;
    int tAttackCardSlot = -1;
    int tJoinerCardSlot = -1;
    int i = 0;
    while(mrPlayerHand->GetListSize() < tCardsToAdd && mrPlayerDeck->GetListSize() > 0)
    {
        //--Roll a random card.
        int tDeckSize = mrPlayerDeck->GetListSize();
        int tRoll = rand() % tDeckSize;

        //--Move to that card, liberate it.
        WordCard *rDeckCard = (WordCard *)mrPlayerDeck->GetElementBySlot(tRoll);
        rDeckCard->SetAssignedIndex(-1);
        mrPlayerDeck->SetRandomPointerToThis(rDeckCard);
        mrPlayerDeck->LiberateRandomPointerEntry();

        //--If no attack cards have been added, we can add an extra card since at least one attack is in the hand.
        if(tAttackCardSlot == -1 && rDeckCard->GetCardType() == WORDCARD_TYPE_ACTION && rDeckCard->GetEffectType() == WORDCARD_EFFECT_DAMAGE)
        {
            tAttackCardSlot = i;
        }
        //--If no joiner cards have been added, we can add an extra card when at least one joiner is in the hand.
        else if(tJoinerCardSlot == -1 && (rDeckCard->GetCardType() == WORDCARD_TYPE_AND || rDeckCard->GetCardType() == WORDCARD_TYPE_OF))
        {
            tJoinerCardSlot = i;
        }

        //--Register the card to the player's hand.
        i ++;
        mrPlayerHand->AddElementAsTail("X", rDeckCard);

        //--Order the card to position itself offscreen.
        rDeckCard->MoveToInstantly(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 150.0f);
    }

    //--No attack cards? Add the first attack card found.
    if(tAttackCardSlot == -1)
    {
        WordCard *rDeckCard = (WordCard *)mrPlayerDeck->SetToHeadAndReturn();
        while(rDeckCard)
        {
            //--If the card is an action card, add it!
            if(rDeckCard->GetCardType() == WORDCARD_TYPE_ACTION && rDeckCard->GetEffectType() == WORDCARD_EFFECT_DAMAGE)
            {
                //fprintf(stderr, "Manually added an attack card.\n");
                rDeckCard->SetAssignedIndex(-1);
                rDeckCard->MoveToInstantly(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 150.0f);
                mrPlayerHand->AddElementAsTail("X", rDeckCard);
                mrPlayerDeck->LiberateRandomPointerEntry();
                break;
            }

            //--Next.
            rDeckCard = (WordCard *)mrPlayerDeck->IncrementAndGetRandomPointerEntry();
        }
    }

    //--No joiner cards? Add the first joiner card found.
    if(tJoinerCardSlot == -1)
    {
        WordCard *rDeckCard = (WordCard *)mrPlayerDeck->SetToHeadAndReturn();
        while(rDeckCard)
        {
            //--If the card is a joiner card, add it.
            if(rDeckCard->GetCardType() == WORDCARD_TYPE_AND || rDeckCard->GetCardType() == WORDCARD_TYPE_OF)
            {
                //fprintf(stderr, "Manually added a joiner card.\n");
                rDeckCard->SetAssignedIndex(-1);
                rDeckCard->MoveToInstantly(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 150.0f);
                mrPlayerHand->AddElementAsTail("X", rDeckCard);
                mrPlayerDeck->LiberateRandomPointerEntry();
                break;
            }

            //--Next.
            rDeckCard = (WordCard *)mrPlayerDeck->IncrementAndGetRandomPointerEntry();
        }
    }

    //--All cards not in the hand or graveyard are positioned near the shuffle button.
    float tXPos = 1161.0f;
    float tYPos =  735.0f;
    float tXStep = -10.0f;
    WordCard *rDeckCard = (WordCard *)mrPlayerDeck->PushIterator();
    while(rDeckCard)
    {
        rDeckCard->MoveToSlide(tXPos, tYPos);
        tXPos = tXPos + tXStep;
        rDeckCard = (WordCard *)mrPlayerDeck->AutoIterate();
    }

    //--Order all cards to move to a new position. This will cause them to seamlessly appear from
    //  the bottom of the screen. Cute!
    PositionCardsInHand();

    //--Recompute card highlights.
    RecomputeHighlights();
}
void WordCombat::PositionCardsInHand()
{
    //--Recomputes where the cards in the player's hand should be. All cards will be given orders to move
    //  to the correct position.
    //--Cards have fixed positions to make them less annoying to click on during the heat of battle.

    //--Order all the cards in the player's hand to move to their final positions in the hand.
    WordCard *rHandCard = (WordCard *)mrPlayerHand->PushIterator();
    while(rHandCard)
    {
        //--Get the assigned index. Newly added cards need to find the first open one.
        int tIndex = rHandCard->GetAssignedIndex();
        if(tIndex == -1) rHandCard->SetAssignedIndex(GetFirstUnusedSlot());

        //--Okay, re-check the assigned index. It shouldn't be -1 unless an error occurred.
        tIndex = rHandCard->GetAssignedIndex();
        if(tIndex == -1)
        {
            rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
            continue;
        }

        //--Set.
        rHandCard->MoveToSlide(mHandPositionsX[tIndex], WC_WORD_STANDARD_Y);

        //--Next.
        rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
    }
}
void WordCombat::RecomputeHighlights()
{
    //--Goes through all hand cards and figures out whether or not they should be highlighted.
    WordCard *rHandCard = (WordCard *)mrPlayerHand->PushIterator();
    while(rHandCard)
    {
        //--Check if the card can be placed.
        StarBitmap *rUseHighlight = Images.Data.rCardHighlight;
        int tHighlightFlag = CanPlaceCard(rHandCard);

        //--Card is an and/or card. It changes highlight image.
        if(rHandCard->GetCardType() == WORDCARD_TYPE_AND || rHandCard->GetCardType() == WORDCARD_TYPE_OF) rUseHighlight = Images.Data.rCardAndOfHighlight;

        //--Set.
        rHandCard->SetHighlightFlag(tHighlightFlag, rUseHighlight);

        //--Next.
        rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
    }
}
