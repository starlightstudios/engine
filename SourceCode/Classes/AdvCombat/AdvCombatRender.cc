//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatDefStruct.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureLevel.h"
#include "WorldDialogue.h"
#include "AdventureMenu.h"
#include "AdvUISkills.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

///--[Debug]
//#define COMBAT_RENDER_DEBUG
#ifdef COMBAT_RENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================= Core Call ============================================
void AdvCombat::Render(bool pBypassResolution, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the Adventure Combat interface. This uses an alpha mixer, but this is only used
    //  during the resolution sequences.
    if(!Images.mIsReady || !mIsActive || pAlpha <= 0.0f) return;
    DebugPush(true, "Combat Render: Begin.\n");

    //--If the world is still pulsing, don't render.
    DebugPrint("Checking Adventure Level.\n");
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsPulsing()) { DebugPop("Level pulsing, exited normally.\n"); return; }

    ///--[Subhandlers]
    //--Subhandlers can take over rendering. First, combat is over:
    DebugPrint("Checking resolution/introduction states.\n");
    if(mCombatResolution != ADVCOMBAT_END_NONE && !pBypassResolution) { RenderResolution();   DebugPop("Resolution running, exited normally.\n");   return; }
    if(mIsIntroduction)                                               { RenderIntroduction(); DebugPop("Introduction running, exited normally.\n"); return; }

    //--Anything below this point is "Normal" combat rendering.

    ///--[List Building]
    //--Rebuild effect references. It is possible an effect was added/removed since the last time the update ticked.
    DebugPrint("Rebuilding effect references.\n");
    RebuildEffectReferences();

    ///--[Grey Backing]
    //--Flat grey backing. Not affected by alpha mixing. Does not render during the victory pass.
    DebugPrint("Backing.\n");
    if(!pBypassResolution) StarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    ///--[Entities]
    //--Render player entities.
    DebugPrint("Rendering player entities.\n");
    AdvCombatEntity *rPartyEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyEntity)
    {
        RenderEntity(rPartyEntity, pAlpha);
        rPartyEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Render enemy entities.
    DebugPrint("Rendering enemy entities.\n");
    AdvCombatEntity *rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyEntity)
    {
        RenderEntity(rEnemyEntity, pAlpha);
        rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    ///--[UI]
    //--Options.
    bool tShowExpertBar = OptionsManager::Fetch()->GetOptionB("Expert Combat UI");

    //--Component rendering.
    DebugPrint("Rendering UI.\n");
    RenderAllyBars(pAlpha);
    RenderTurnOrder(pAlpha);
    RenderNewTurn(pAlpha);
    RenderTargetCluster();

    //--Player's interface.
    DebugPrint("Rendering player interface.\n");
    if(mPlayerInterfaceTimer > 0)
    {
        //--Compute percent.
        float cPct = EasingFunction::QuadraticInOut(mPlayerInterfaceTimer, ADVCOMBAT_MOVE_TRANSITION_TICKS);

        //--Option. Selects between fading and sliding.
        bool tUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

        //--Fading: Modify the alpha by the fade percent.
        if(tUseFade)
        {
            //--Compute fade.
            float tUseAlpha = pAlpha * cPct;

            //--Render based on multiplied alpha.
            if(tShowExpertBar)
            {
                RenderExpertBar(tUseAlpha);
            }
            else
            {
                RenderPlayerBar(tUseAlpha);
            }
        }
        //--Scrolling. Offset position.
        else
        {
            //--Compute offset.
            float cMaxOffset = 300.0f;
            float cOffset = cMaxOffset * (1.0f - cPct);

            //--Translate, render.
            glTranslatef(0.0f, cOffset, 0.0f);
            if(tShowExpertBar)
            {
                RenderExpertBar(pAlpha);
            }
            else
            {
                RenderPlayerBar(pAlpha);
            }
            glTranslatef(0.0f, -cOffset, 0.0f);
        }

    }

    //--Render animations.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    DebugPrint("Rendering combat animations.\n");
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->PushIterator();
    while(rAnimation)
    {
        rAnimation->Render();
        rAnimation = (AdvCombatAnimation *)mCombatAnimations->AutoIterate();
    }

    //--Render the enemy UIs.
    DebugPrint("Rendering enemy UIs.\n");
    rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyEntity)
    {
        RenderEntityUI(rEnemyEntity, pAlpha);
        rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    //--Text package.
    DebugPrint("Rendering text packages.\n");
    float cCombatYDrift = -30.0f;
    CombatTextPack *rTextPack = (CombatTextPack *)mTextPackages->PushIterator();
    while(rTextPack)
    {
        //--Compute Y position.
        float cPercent = EasingFunction::QuadraticOut(rTextPack->mTicks, rTextPack->mTicksMax);
        float cYUsePos = rTextPack->mY + (cCombatYDrift * cPercent);

        //--Render.
        rTextPack->mColor.SetAsMixer();
        Images.Data.rCombatTextFont->DrawText(rTextPack->mX, cYUsePos, SUGARFONT_AUTOCENTER_XY, rTextPack->mScale, rTextPack->mText);

        //--Next.
        rTextPack = (CombatTextPack *)mTextPackages->AutoIterate();
    }
    StarlightColor::ClearMixer();

    //--Prediction boxes.
    DebugPrint("Rendering prediction boxes.\n");
    RenderPredictionBoxes();

    //--Titles.
    DebugPrint("Rendering titles.\n");
    RenderTitle();

    ///--[Help Strings]
    //--UI is hidden, only thing visible is the prompt to show it.
    if(mHideCombatUI)
    {
        mShowUIString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - 22.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rHelpFont);
    }
    else
    {
        //--UI String.
        mHideUIString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - 22.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rHelpFont);

        //--Toggle Descriptions.
        mToggleDescriptionsString->DrawText(VIRTUAL_CANVAS_X - 150.0f, VIRTUAL_CANVAS_Y - 22.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rHelpFont);

        //--When using the simplified UI and not in card selection, render this.
        if(!tShowExpertBar && mPlayerMode != ADVCOMBAT_INPUT_CARDS)
            mChangeCardString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - 44.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rHelpFont);
    }

    ///--[Overlays]
    //--Dialogue, if present.
    DebugPrint("Rendering world dialogue.\n");
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible())
    {
        rWorldDialogue->Render();
    }

    //--Combat inspector.
    DebugPrint("Rendering inspector.\n");
    RenderInspector();

    //--Confirmation window overlay.
    DebugPrint("Rendering confirmation window.\n");
    RenderConfirmation();
    DebugPop("Exited normally.\n");
}

///======================================= Entity Render ==========================================
void AdvCombat::RenderEntity(AdvCombatEntity *pEntity, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Given an entity, renders it at its combat position. Special effects are handled here. This only
    //  renders the combat portrait, it does not render and UI over the entity.
    if(!pEntity) return;

    //--Make sure there's a portrait to render.
    StarBitmap *rCombatPortrait = pEntity->GetCombatPortrait();
    if(!rCombatPortrait) return;

    //--Block rendering when KO'd. This only occurs while sliding off the screen for player party members.
    if(pEntity->IsBlockRenderForKO()) return;

    ///--[Color Mixing]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Scaling]
    float cScale = 1.0f;
    float cScaleInv = 1.0f;

    //--If this is an enemy, scale down to 70%.
    if(GetPartyGroupingID(pEntity->GetID()) == AC_POSCODE_ENEMY_PARTY)
    {
        cScale = 0.90f;
        cScaleInv = 1.0f / cScale;
    }

    ///--[Position Handling]
    //--Get positions.
    float cRenderX = pEntity->GetCombatX();
    float cRenderY = pEntity->GetCombatY();

    //--Center the frame by X position.
    cRenderX = cRenderX - (rCombatPortrait->GetTrueWidth()  * 0.50f * cScale);

    //--Shake offset.
    cRenderX = cRenderX + pEntity->GetShakeX();

    //--Place at position.
    glTranslatef(cRenderX, cRenderY, 0.0f);
    glScalef(cScale, cScale, 1.0f);

    ///--[Target Handling]
    //--If targeting is currently happening, all untargeted entities are greyed out slightly.
    if(mIsSelectingTargets)
    {
        if(!IsEntityInActiveTargetCluster(pEntity))
        {
            StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f * pAlpha);
        }
    }
    //--If the entity is currently on the active target cluster, it will flash.
    else if(mIsSelectingTargets && mTargetFlashTimer < ADVCOMBAT_TARGET_FLASH_PERIOD/2 && false)
    {
        //--Make sure we're on the target cluster.
        TargetCluster *rActiveCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
        if(rActiveCluster)
        {
            //--In the cluster, set mixer to black.
            if(rActiveCluster->IsElementInCluster(pEntity))
            {
                StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f * pAlpha);
            }
        }
    }
    ///--[Black Flash]
    //--Entities flash black when they are acting.
    else if(pEntity->IsFlashingBlack())
    {
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f * pAlpha);
    }
    ///--[White Flash]
    //--Fades up to fullwhite using the stencil buffer.
    else if(pEntity->GetWhiteFlashTimer() > 0)
    {
        //--Timer.
        int tTimer = pEntity->GetWhiteFlashTimer();
        float cPercent = EasingFunction::QuadraticInOut(tTimer, ADVCE_FLASH_WHITE_TICKS);

        //--Normal image. Color mask stays on.
        DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
        glColorMask(true, true, true, true);
        rCombatPortrait->Draw();

        //--Render a white overlay.
        DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
        glDisable(GL_TEXTURE_2D);
        glColor4f(1.0f, 1.0f, 1.0f, cPercent);
        rCombatPortrait->Draw();

        //--Clean up.
        glEnable(GL_TEXTURE_2D);
        DisplayManager::DeactivateStencilling();
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-cRenderX, -cRenderY, 0.0f);
        return;
    }
    ///--[Knockout]
    //--Suspends normal rendering.
    else if(pEntity->IsKnockingOut())
    {
        //--Timer.
        int tKnockoutTimer = pEntity->GetKnockoutTimer();

        //--Fading up to white. First, render the normal image and stencil.
        if(tKnockoutTimer < ADVCE_KNOCKOUT_TOWHITE_TICKS)
        {
            //--Normal image.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            rCombatPortrait->Draw();

            //--Render a white overlay.
            float cPercent = EasingFunction::QuadraticOut(tKnockoutTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glDisable(GL_TEXTURE_2D);
            glColor4f(1.0f, 1.0f, 1.0f, cPercent);
            rCombatPortrait->Draw();

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-cRenderX, -cRenderY, 0.0f);
            return;
        }
        //--Fading down to black.
        else
        {
            //--Get percent.
            int tUseTimer = tKnockoutTimer - ADVCE_KNOCKOUT_TOWHITE_TICKS;

            //--Portrait, color mask is off.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            rCombatPortrait->Draw();

            //--Fullwhite.
            glDisable(GL_TEXTURE_2D);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            rCombatPortrait->Draw();

            //--Render a black overlay.
            float cPercent = EasingFunction::QuadraticOut(tUseTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            glDisable(GL_TEXTURE_2D);
            glColor4f(0.0f, 0.0f, 0.0f, cPercent);
            rCombatPortrait->Draw();

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-cRenderX, -cRenderY, 0.0f);
            return;
        }
    }

    ///--[Render]
    rCombatPortrait->Draw();

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    glScalef(cScaleInv, cScaleInv, 1.0f);
    glTranslatef(-cRenderX, -cRenderY, 0.0f);
}

///========================================= Entity UI ============================================
void AdvCombat::RenderEntityUI(AdvCombatEntity *pEntity, float pAlpha)
{
    ///--[Documentation]
    //--Given an entity, renders their HP bar, effects, stun status, and so on.
    if(!pEntity || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the UI.
    if(mHideCombatUI) return;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Knockout]
    //--Causes a fade out of this object. May block rendering.
    float cAlpha = 1.0f;
    if(pEntity->IsKnockingOut())
    {
        //--Render a fadeout.
        int tKnockoutTimer = pEntity->GetKnockoutTimer();
        if(tKnockoutTimer < ADVCE_KNOCKOUT_TOWHITE_TICKS)
        {
            cAlpha = 1.0f - EasingFunction::QuadraticOut(tKnockoutTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
        }
        //--Stop rendering past the towhite knockout.
        else
        {
            return;
        }
    }
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pAlpha);

    //--Get positions.
    float cRenderX = pEntity->GetCombatX();
    float cRenderY = 90.0f;

    ///--[Frame, Bars]
    //--Get relative position of the frame.
    float cFrameX = cRenderX - (Images.Data.rEnemyHPFrame->GetTrueWidth() * 0.50f);
    float cFrameY = cRenderY;

    //--Render the frame, and the HP fill.
    float cHPBarPercent = pEntity->GetDisplayHPPct();
    if(cHPBarPercent > 0.0f)
    {
        Images.Data.rEnemyHPFill->RenderPercent(cFrameX, cFrameY, 0.0f, 0.0f, cHPBarPercent, 1.0f);
    }
    Images.Data.rEnemyHPFrame->Draw(cFrameX, cFrameY);

    //--HP Text Value
    int tHPValue = pEntity->GetHealth();
    Images.Data.rEnemyHPFont->DrawTextArgs(cFrameX + 11.0f, cFrameY + 1.0f, 0, 1.0f, "%i", tHPValue);

    ///--[Name]
    //--Render the name above the bar.
    Images.Data.rEnemyHPFont->DrawText(cFrameX + 57.0f, cFrameY - 17.0f, SUGARFONT_AUTOCENTER_X, 1.0f, pEntity->GetDisplayName());

    ///--[Stun Rendering]
    //--Doesn't render if the current stun values are all zeroes.
    int tStunCur    = pEntity->GetDisplayStun();
    int tStunCap    = pEntity->GetStatistic(STATS_STUN_CAP);
    int tStunRes    = pEntity->GetStunResist();
    int tStunResCnt = pEntity->GetStunResistTimer();

    //--Must be stunnable and have stun information to show.
    if(pEntity->IsStunnable() && (tStunCur > 0 || tStunRes > 0))
    {
        //--Bar fill. Compute percentage.
        float cStunPct = 0.0f;
        if(tStunCap > 0)
        {
            cStunPct = (float)tStunCur / (float)tStunCap;
            if(cStunPct > 1.0f) cStunPct = 1.0f;
        }
        else
        {
            cStunPct = 1.0f;
        }

        //--Render.
        Images.Data.rEnemyStunBarFill->RenderPercent(cFrameX, cFrameY, 0.0f, 0.0f, cStunPct, 1.0f);

        //--Frame.
        Images.Data.rEnemyStunFrame->Draw(cFrameX, cFrameY);

        //--Stun value.
        Images.Data.rEnemyHPFont->DrawTextArgs(cFrameX + 9.0f, cFrameY + 19.0f, 0, 1.0f, "%i", tStunCur);

        //--Stun resist pips.
        for(int i = 0; i < tStunRes; i ++)
        {
            Images.Data.rEnemyStunFill->Draw(cFrameX + 82.0f + (i * 13.0f), cFrameY + 22.0f);
        }

        //--Stun resist timer.
        Images.Data.rClockIcon->Draw(cFrameX + 121.0f, cFrameY + 20.0f);
        Images.Data.rEnemyHPFont->DrawTextArgs(cFrameX + 142.0f, cFrameY + 20.0f, 0, 1.0f, "%i", tStunResCnt);
    }

    ///--[Effects]
    //--Render up to 12 effects.
    float cEffectX = cFrameX;
    float cEffectY = cFrameY + 22.0f;
    if(pEntity->IsStunnable() && (tStunCur > 0 || tStunRes > 0)) cEffectY = cEffectY + 15.0f;

    //--Constants.
    float cEffectScale = 19.0f / 50.0f;
    float cEffectScaleInv = 1.0f / cEffectScale;

    //--Counts.
    int tRendersSoFar = 0;
    StarLinkedList *rEffectList = pEntity->GetEffectRenderList();

    //--Iterate.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)rEffectList->PushIterator();
    while(rEffect)
    {
        //--Stop if we run out of renders.
        if(tRendersSoFar >= 12)
        {
            rEffectList->PopIterator();
            break;
        }

        //--If the effect is marked as hidden, do not render it.
        if(!rEffect->IsVisibleOnUI() || !rEffect->IsVisibleOnQuickUI())
        {
            rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
            continue;
        }

        //--Compute position.
        int tLineX = tRendersSoFar % 6;
        int tLineY = tRendersSoFar / 6;
        float cRenderX = cEffectX + (19.0f * tLineX);
        float cRenderY = cEffectY + (19.0f * tLineY);

        //--Position, scale.
        glTranslatef(cRenderX, cRenderY, 0.0f);
        glScalef(cEffectScale, cEffectScale, 1.0f);

        //--Get the associated icon.
        StarBitmap *rEffectBack  = rEffect->GetBackImage();
        StarBitmap *rEffectFrame = rEffect->GetFrameImage();
        StarBitmap *rEffectFront = rEffect->GetFrontImage();
        if(rEffectBack)  rEffectBack ->Draw();
        if(rEffectFrame) rEffectFrame->Draw();
        if(rEffectFront) rEffectFront->Draw();

        //--Unscale, unposition.
        glScalef(cEffectScaleInv, cEffectScaleInv, 1.0f);
        glTranslatef(-cRenderX, -cRenderY, 0.0f);

        //--Next.
        tRendersSoFar ++;
        rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
    }

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}

///========================================= Ally Bars ============================================
void AdvCombat::RenderAllyBars(float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders indicators in the top left showing the HP, MP, Effects, etc of the player's party.
    if(!Images.mIsReady || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Positions
    float cYPos = 0.0f;
    float cYStep = 38.0f;

    //--Mask Code. Increments by 1 so the portraits don't overlap.
    int tMaskCode = ACE_STENCIL_ALLY_PORTRAIT_START;

    ///--[Party Render]
    //--For each party member:
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        ///--[Bars, Fills, Pips]
        //--HP Fill.
        float tHPPct = rEntity->GetDisplayHPPct();
        Images.Data.rAllyHPFill->RenderPercent(0.0f, cYPos, 0.0f, 0.0f, tHPPct, 1.0f);

        //--MP Fill.
        float tMPPct = rEntity->GetDisplayMPPct();
        Images.Data.rAllyMPFill->RenderPercent(0.0f, cYPos, 0.0f, 0.0f, tMPPct, 1.0f);

        //--Frame.
        Images.Data.rAllyFrame->Draw(0.0f, cYPos);

        //--CP Pips.
        int tCPCount = rEntity->GetComboPoints();
        for(int i = 0; i < tCPCount; i ++)
        {
            Images.Data.rAllyCPPip->Draw(147.0f + (5.0f * i), cYPos + 29.0f);
        }

        ///--[Mask, Portrait]
        //--Render the mask.
        DisplayManager::ActivateMaskRender(tMaskCode);
        Images.Data.rAllyPortraitMask->Draw(0.0f, cYPos);

        //--Turn masking on and render the character's portrait.
        DisplayManager::ActivateStencilRender(tMaskCode);
        TwoDimensionRealPoint cRenderCoords = rEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_ALLY);
        StarBitmap *rCombatPortrait = rEntity->GetCombatPortrait();
        if(rCombatPortrait)
        {
            rCombatPortrait->Draw(cRenderCoords.mXCenter, cYPos + cRenderCoords.mYCenter);
        }

        //--Disable stencilling.
        DisplayManager::DeactivateStencilling();

        ///--[Effects]
        //--We render only 5 effects, after that, a number is shown indicating how many unrendered effects there'
        //  are. The player can look at the Combat Inspector to see the rest.
        float cEffectLft = 191.0f;
        float cEffectTop = cYPos + 12.0f;
        float cEffectWid = 19.0f;
        StarLinkedList *rEffectList = rEntity->GetEffectRenderList();

        //--Scaling.
        float cEffectScale = 19.0f / 50.0f;
        float cEffectScaleInv = 1.0f / cEffectScale;

        //--Iterate.
        int tRendersSoFar = 0;
        AdvCombatEffect *rEffect = (AdvCombatEffect *)rEffectList->PushIterator();
        while(rEffect)
        {
            //--Not visible on UI, or not associated with this entity.
            if(!rEffect->IsVisibleOnUI() || !rEffect->IsIDOnTargetList(rEntity->GetID()))
            {
                rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
                continue;
            }

            //--Max renders. Exit out.
            if(tRendersSoFar >= 5) { rEffectList->PopIterator(); break; }

            //--Compute position.
            float cLft = cEffectLft + (cEffectWid * tRendersSoFar);

            //--Get images.
            StarBitmap *rEffectBack = rEffect->GetBackImage();
            StarBitmap *rEffectFrame = rEffect->GetFrameImage();
            StarBitmap *rEffectFront = rEffect->GetFrontImage();

            //--Position, scale.
            glTranslatef(cLft, cEffectTop, 0.0f);
            glScalef(cEffectScale,cEffectScale, 1.0f);

            //--Render.
            if(rEffectBack)  rEffectBack ->Draw();
            if(rEffectFrame) rEffectFrame->Draw();
            if(rEffectFront) rEffectFront->Draw();

            //--Unscale, unposition.
            glScalef(cEffectScaleInv,cEffectScaleInv, 1.0f);
            glTranslatef(-cLft, -cEffectTop, 0.0f);

            //--Next.
            tRendersSoFar ++;
            rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
        }

        ///--[Next]
        //--Next.
        tMaskCode ++;
        cYPos = cYPos + cYStep;
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    ///--[Combat Inspector Key]
    mCombatInspectorShow->DrawText(VIRTUAL_CANVAS_X, 11.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rHelpFont);
}

///======================================== Turn Order ============================================
void AdvCombat::RenderTurnOrder(float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the turn order in the top right. Extends to the left, indicating who takes the next
    //  action by being the leftmost.
    if(!Images.mIsReady || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--List.
    StarLinkedList *tRenderList = new StarLinkedList(true);

    ///--[Next Turn]
    //--Compute the starting position based on the party size.
    float cPartyYStart = 11.0f;
    float cPartyYStep = 38.0f;

    //--Y Variables / Constants
    float cYStart = (cPartyYStart + (cPartyYStep * mrCombatParty->GetListSize()));
    float cYEnd = cYStart + 130.0f;
    float tYPos = cYStart;

    //--Compute the sub-offset.
    float cSubPct = (float)mTurnBarNewTurnTimer / (float)mTurnBarNewTurnTimerMax;

    //--Scale and Alpha Variables / Constants
    float cScaleStart = 1.25f;
    float cScaleEnd   = 0.75f;
    float cAlphaStart = 1.00f;
    float cAlphaEnd   = 1.00f;

    //--Element sizes.
    float cElemBaseWid = Images.Data.rTurnOrderFriendly->GetWidth();
    float cElemBaseHei = Images.Data.rTurnOrderFriendly->GetHeight();

    //--Rendering takes place on the far right side.
    //glTranslatef(VIRTUAL_CANVAS_X, 0.0f, 0.0f);

    //--Symbol indicating the next turn.
    Images.Data.rTurnOrderNext->Draw(11.0f, tYPos);
    tYPos = tYPos + 11.0f;

    //--Offset the Y position by this timer.
    float tTurnOrderPct = 1.0f - ((float)mTurnBarMoveTimer / (float)mTurnBarMoveTimerMax);
    float cOffsetVal = (cElemBaseHei * cScaleStart * tTurnOrderPct);
    tYPos = tYPos + cOffsetVal;

    ///--[Package Creation]
    //--In order to render these correctly, they must be done last-to-first even though the calculations
    //  are first-to-last. Therefore, we create a rendering pack and store the rendering data in there.
    AdvCombatEntity *rTurnEntity = (AdvCombatEntity *)mrTurnOrder->PushIterator();
    while(rTurnEntity)
    {
        ///--[Setup]
        //--Compute percentage.
        float cYPercent = tYPos / cYEnd;
        if(cYPercent > 1.0f) cYPercent = 1.0f;

        //--Get scale.
        float tCurScale = cScaleStart + ((cScaleEnd - cScaleStart) * cYPercent);
        float tCurAlpha = cAlphaStart + ((cAlphaEnd - cAlphaStart) * cYPercent);

        //--If targeting is taking place, and the entity in question is being targeted, they render normally.
        //  Otherwise, they are greyed out. If targeting is not happening, all entities render fullbright.
        float cMixer = 1.0f;
        if(mIsSelectingTargets)
        {
            if(!IsEntityInActiveTargetCluster(rTurnEntity))
            {
                cMixer = 0.50f;
            }
            else
            {
                tCurAlpha = 1.0f;
            }
        }

        //--Resolve which party the entity is in.
        StarBitmap *rUseBacking = Images.Data.rTurnOrderEnemy;
        if(GetPartyGroupingID(rTurnEntity->GetID()) == AC_PARTY_GROUP_PARTY) rUseBacking = Images.Data.rTurnOrderFriendly;

        ///--[Create Render Package]
        //--Store everything for later rendering.
        TurnOrderRenderPack *nPackage = (TurnOrderRenderPack *)starmemoryalloc(sizeof(TurnOrderRenderPack));
        nPackage->mXPos = cElemBaseWid * 0.0f * tCurScale;
        nPackage->mYPos = tYPos;
        nPackage->mScale = tCurScale;
        nPackage->mMixer = cMixer;
        nPackage->mAlpha = tCurAlpha;
        nPackage->rImage = rTurnEntity->GetTurnIcon();
        nPackage->rBacking = rUseBacking;
        tRenderList->AddElementAsHead("X", nPackage, &FreeThis);

        ///--[Next]
        //--Increment Y position by height.
        tYPos = tYPos + ((cElemBaseHei * tCurScale) * cSubPct);

        //--Entry cap. Stop rendering when the turn order threatens the player bar.
        if((tYPos + (cElemBaseHei * tCurScale)) > 440.0f)
        {
            mrTurnOrder->PopIterator();
            break;
        }

        //--Next.
        rTurnEntity = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
    }

    ///--[Rendering Loop]
    //--Iterate across the packages and render them back to front.
    TurnOrderRenderPack *rRenderPack = (TurnOrderRenderPack *)tRenderList->PushIterator();
    while(rRenderPack)
    {
        //--Position, scale, color.
        glTranslatef(rRenderPack->mXPos, rRenderPack->mYPos, 0.0f);
        glScalef(rRenderPack->mScale, rRenderPack->mScale, 1.0f);
        StarlightColor::SetMixer(rRenderPack->mMixer, rRenderPack->mMixer, rRenderPack->mMixer, pAlpha * rRenderPack->mAlpha);

        //--Render.
        if(rRenderPack->rBacking) rRenderPack->rBacking->Draw();
        if(rRenderPack->rImage)   rRenderPack->rImage->Draw();

        //--Unscale, unposition.
        glScalef(1.0f / rRenderPack->mScale, 1.0f / rRenderPack->mScale, 1.0f);
        glTranslatef(-rRenderPack->mXPos, -rRenderPack->mYPos, 0.0f);

        //--Next.
        rRenderPack = (TurnOrderRenderPack *)tRenderList->AutoIterate();
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    //glTranslatef(VIRTUAL_CANVAS_X * -1.0f, 0.0f, 0.0f);
    delete tRenderList;
}

///===================================== Player Interface =========================================
void AdvCombat::RenderPlayerBar(float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the player's status and either the combat cards, or the ability grid and descriptions,
    //  depending on which mode the interface is in.
    if(!Images.mIsReady || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Get the active character.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActiveEntity) return;

    //--Color Setup.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Backing]
    //Images.Data.rCharacterBacking->Draw();

    ///--[Status, Health]
    //--HP Percent.
    float tHPPercent = rActiveEntity->GetDisplayHPPct();
    if(tHPPercent > 1.0f) tHPPercent = 1.0f;
    Images.Data.rCharacterHPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tHPPercent, 1.0f);

    //--MP Percent.
    float tMPPercent = rActiveEntity->GetDisplayMPPct();
    if(tMPPercent > 1.0f) tMPPercent = 1.0f;
    Images.Data.rCharacterMPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tMPPercent, 1.0f);

    //--These always render.
    Images.Data.rCharacterBanner->Draw();
    Images.Data.rCharacterHPFrame->Draw();
    Images.Data.rCharacterMPFrame->Draw();
    Images.Data.rCharacterCPFrame->Draw();

    //--Select an icon based on what health type is dominant.
    StarBitmap *rUseIcon = Images.Data.rHealthIcon;
    if(rActiveEntity->GetAdrenaline() > 0) rUseIcon = Images.Data.rAdrenalineIcon;
    if(rActiveEntity->GetShields()    > 0) rUseIcon = Images.Data.rShieldsIcon;

    //--Render health.
    rUseIcon->Draw(54.0f, 581.0f);
    Images.Data.rPlayerHPFont->DrawTextArgs(79.0f, 576.0f, 0, 1.0f, "%i / %i", rActiveEntity->GetHealth(), rActiveEntity->GetHealthMax());

    //--Render mana.
    Images.Data.rManaIcon->Draw(54.0f, 644.0f);
    Images.Data.rPlayerHPFont->DrawTextArgs(79.0f, 639.0f, 0, 1.0f, "%i", rActiveEntity->GetMagic());

    //--Combo Points, handled using pips.
    int tComboPoints = rActiveEntity->GetComboPoints();
    if(tComboPoints > 0)
    {
        for(int i = 0; i < tComboPoints; i ++)
        {
            Images.Data.rCharacterCPBarPip->Draw(i * 13.0f, 0.0f);
        }
    }

    ///--[Name]
    //--Exactly what you think. Renders over the banner.
    const char *rDisplayName = rActiveEntity->GetDisplayName();
    Images.Data.rPlayerNameFont->DrawText(222.0f, 490.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rDisplayName);

    ///--[Submodes]
    //--All submodes always render, but may not render anything if their timers are zero.
    RenderPlayerCards(pAlpha);
    RenderPlayerJobSkills(pAlpha);
    RenderPlayerMemorizedSkills(pAlpha);
    RenderPlayerTacticsSkills(pAlpha);
}
void AdvCombat::RenderExpertBar(float pAlpha)
{
    ///--[Documentation]
    //--An alternate combat player layout. Smaller icons, but shows considerably more information on screen.
    if(pAlpha <= 0.0f) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Get the active character.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActiveEntity) return;

    //--Color Setup.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Variables]
    float tHPPercent = rActiveEntity->GetDisplayHPPct();
    float tMPPercent = rActiveEntity->GetDisplayMPPct();
    int tHlt    = rActiveEntity->GetHealth();
    int tHltMax = rActiveEntity->GetHealthMax();
    int tAdr    = rActiveEntity->GetAdrenaline();
    int tShl    = rActiveEntity->GetShields();
    int tMag    = rActiveEntity->GetMagic();

    ///--[Backing]
    //--Goes under the health/mana bars.
    Images.Data.rExpertStatisticFramesBack->Draw();

    ///--[Health]
    //--HP Percent.
    if(tHPPercent > 1.0f) tHPPercent = 1.0f;
    Images.Data.rExpertHPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tHPPercent, 1.0f);

    //--Select an icon based on what health type is dominant.
    StarBitmap *rUseIcon = Images.Data.rHealthIcon;
    if(tAdr > 0) rUseIcon = Images.Data.rAdrenalineIcon;
    if(tShl > 0) rUseIcon = Images.Data.rShieldsIcon;

    ///--[Mana]
    //--MP Percent.
    if(tMPPercent > 1.0f) tMPPercent = 1.0f;
    Images.Data.rExpertMPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tMPPercent, 1.0f);

    ///--[Frames]
    //--Render over the bars but under the text/icons.
    Images.Data.rExpertStatisticFrames->Draw();

    //--Render health.
    rUseIcon->Draw(37.0f, 480.0f);
    Images.Data.rPlayerHPFont->DrawTextArgs(63.0f, 475.0f, 0, 1.0f, "%i / %i", tHlt + tAdr + tShl, tHltMax);

    //--Render mana.
    Images.Data.rManaIcon->Draw(37.0f, 541.0f);
    Images.Data.rPlayerHPFont->DrawTextArgs(63.0f, 536.0f, 0, 1.0f, "%i", tMag);

    ///--[Combo Points]
    //--Combo Points, handled using pips. Renders over the frame.
    int tComboPoints = rActiveEntity->GetComboPoints();
    if(tComboPoints > 0)
    {
        //--Resolve the highlighted ability. If it has a CP cost, we may animate the CP icons.
        AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
        if(rAbility && rAbility->GetCPCost() > 0 && rAbility->GetCPCost() <= tComboPoints)
        {
            //--X/Y position is the basic fill's X/Y offset.
            float cCPLft = Images.Data.rExpertCPFill->GetXOffset();
            float cCPTop = Images.Data.rExpertCPFill->GetYOffset();

            //--Resolve the animation frame.
            float cTicksPerFrame = 5.0f;
            int tIndex = (int)((mPlayerCPTimer - (cTicksPerFrame * 10.0f)) / cTicksPerFrame) % ADVCOMBAT_ADVANCED_CP_BUFFER;

            //--If the animation frame is valid, use it. Otherwise use the basic fill.
            StarBitmap *rFrame = Images.Data.rExpertCPAnim[0];
            if(tIndex >= 0 && tIndex < ADVCOMBAT_ADVANCED_CP_FRAMES)
            {
                rFrame = Images.Data.rExpertCPAnim[tIndex];
            }

            //--For each CP point until we get to the ability cost, render an animation.
            for(int i = 0; i < tComboPoints; i ++)
            {
                //--Y position oscillates.
                if(i % 2 == 0)
                    rFrame->Draw(cCPLft + i * 16.0f, cCPTop);
                else
                    rFrame->Draw(cCPLft + i * 16.0f, cCPTop + 16.0f);
            }

        }
        //--Not enough CP, or the cost is zero. Render normally.
        else
        {
            for(int i = 0; i < tComboPoints; i ++)
            {
                //--Y position oscillates.
                if(i % 2 == 0)
                    Images.Data.rExpertCPFill->Draw(i * 16.0f, 0.0f);
                else
                    Images.Data.rExpertCPFill->Draw(i * 16.0f, 16.0f);
            }
        }

    }

    ///--[Skills]
    //--Get offset percentage.
    float cPercent = EasingFunction::QuadraticInOut(mExpertPageSwapTime, (float)ADVCOMBAT_PAGE_CHANGE_TICKS);

    //--Arrow oscillation timers.
    float cArrowOscillate = sinf((mExpertArrowTimer / (float)ADVCOMBAT_ARROW_OSCILLATE_TICKS) * 3.1415926f * 2.0f) * ADVCOMBAT_ARROW_OSCILLATE_DISTANCE;

    ///--[Primary Page]
    if(mExpertPageSwapTime < ADVCOMBAT_PAGE_CHANGE_TICKS)
    {
        //--Offset.
        float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;
        glTranslatef(0.0f, cBotOffset, 0.0f);

        //--Frame.
        Images.Data.rExpertSkillsFrame->Draw();

        //--Constants.
        float cStartX =  32.0f;
        float cStartY = 576.0f;
        float cSpaceX =  53.0f;
        float cSpaceY =  53.0f;

        //--Iterate. All skills render at half size, but the entire set renders at once.
        for(int x = 0; x < ACE_ABILITY_EXPERT_SIZE_X; x ++)
        {
            for(int y = 0; y < ACE_ABILITY_EXPERT_SIZE_Y; y ++)
            {
                //--Compute position.
                float cRenderX = cStartX + (cSpaceX * x);
                float cRenderY = cStartY + (cSpaceY * y);

                //--X Offsets.
                if(x >= ACE_ABILITY_MEMORIZE_GRID_X0) cRenderX = cRenderX + 18.0f;
                if(x >= ACE_ABILITY_TACTICS_GRID_X0)  cRenderX = cRenderX + 18.0f;

                //--Y Offsets.
                if(x <  ACE_ABILITY_MEMORIZE_GRID_X0 &&                                    y > 0) cRenderY = cRenderY + 8.0f;
                if(x >= ACE_ABILITY_MEMORIZE_GRID_X0 && x < ACE_ABILITY_TACTICS_GRID_X0 && y > 1) cRenderY = cRenderY + 8.0f;

                //--Get ability in slot.
                AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(x, y);
                if(!rAbility) continue;

                //--Position, scale.
                glTranslatef(cRenderX, cRenderY, 0.0f);
                //glScalef(0.50f, 0.50f, 1.0f);

                //--Render.
                rAbility->RenderAtCombat(0.0f, 0.0f, pAlpha);

                //--Skill has a cooldown, render that.
                int tCooldown = rAbility->GetCooldown();
                if(tCooldown > 0)
                {
                    //--Cooldown is one digit:
                    if(tCooldown < 10)
                    {
                        Images.Data.rClockLgIcon->Draw(4.0f, 13.0f);
                        Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(28.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                    }
                    //--Two digits.
                    else
                    {
                        Images.Data.rClockLgIcon->Draw(-5.0f, 13.0f);
                        Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(19.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                    }
                }

                //--If the ability is usable, costs CP, and we have enough CP, render an animation around it.
                if(rAbility->IsUsableNow() && rAbility->GetCPCost() > 0 && rAbility->GetCPCost() <= rActiveEntity->GetComboPoints())
                {
                    //--Resolve frame.
                    int tFrame = (int)(mPlayerCPFrameTimer / 5.0f) % ADVCOMBAT_ADVANCED_CPFRAME_TOTAL;
                    StarBitmap *rFrameImg = Images.Data.rExpertCPFrameAnim[tFrame];
                    rFrameImg->Draw();
                }

                //--Unscale, unposition.
                //glScalef(2.00f, 2.00f, 1.0f);
                glTranslatef(-cRenderX, -cRenderY, 0.0f);
            }
        }

        //--Arrows.
        Images.Data.rExpertPageArrowD->Draw(0.0f, cArrowOscillate);
        Images.Data.rExpertPageArrowU->Draw(0.0f, -cArrowOscillate);

        //--Clean.
        glTranslatef(0.0f, -cBotOffset, 0.0f);
    }

    ///--[Secondary Page]
    if(mExpertPageSwapTime > 0)
    {
        //--Offset.
        float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cPercent);
        glTranslatef(0.0f, cBotOffset, 0.0f);

        //--Frame.
        Images.Data.rExpertPageFrame->Draw();

        //--Constants.
        float cStartX =  32.0f;
        float cStartY = 576.0f;
        float cSpaceX =  53.0f;
        float cSpaceY =  53.0f;

        //--Iterate. All skills render at half size, but the entire set renders at once.
        for(int x = ACE_ABILITY_EXPERT_SIZE_X; x < ACE_ABILITY_GRID_SIZE_X; x ++)
        {
            for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
            {
                //--Compute position.
                float cRenderX = cStartX + (cSpaceX * (x - ACE_ABILITY_EXPERT_SIZE_X));
                float cRenderY = cStartY + (cSpaceY * y);

                //--Get ability in slot.
                AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(x, y);
                if(!rAbility) continue;

                //--Position, scale.
                glTranslatef(cRenderX, cRenderY, 0.0f);
                //glScalef(0.50f, 0.50f, 1.0f);

                //--Render.
                rAbility->RenderAtCombat(0.0f, 0.0f, pAlpha);

                //--Skill has a cooldown, render that.
                int tCooldown = rAbility->GetCooldown();
                if(tCooldown > 0)
                {
                    //--Cooldown is one digit:
                    if(tCooldown < 10)
                    {
                        Images.Data.rClockLgIcon->Draw(4.0f, 13.0f);
                        Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(28.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                    }
                    //--Two digits.
                    else
                    {
                        Images.Data.rClockLgIcon->Draw(-5.0f, 13.0f);
                        Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(19.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                    }
                }

                //--If the ability is usable, costs CP, and we have enough CP, render an animation around it.
                if(rAbility->IsUsableNow() && rAbility->GetCPCost() > 0 && rAbility->GetCPCost() <= rActiveEntity->GetComboPoints())
                {
                    //--Resolve frame.
                    int tFrame = (int)(mPlayerCPFrameTimer / 5.0f) % ADVCOMBAT_ADVANCED_CPFRAME_TOTAL;
                    StarBitmap *rFrameImg = Images.Data.rExpertCPFrameAnim[tFrame];
                    rFrameImg->Draw();
                }

                //--Unscale, unposition.
                //glScalef(2.00f, 2.00f, 1.0f);
                glTranslatef(-cRenderX, -cRenderY, 0.0f);
            }
        }

        //--Arrows.
        Images.Data.rExpertPageArrowD->Draw(-45.0f, cArrowOscillate);
        Images.Data.rExpertPageArrowU->Draw(-45.0f, -cArrowOscillate);

        //--Clean.
        glTranslatef(0.0f, -cBotOffset, 0.0f);
    }

    ///--[Highlight]
    RenderExpandableHighlight(mAbilityHighlightPos, mAbilityHighlightSize, 4.0f, Images.Data.rHighlight);

    ///--[Description]
    //--Offset.
    glTranslatef(69.0f, 40.0f, 0.0f);

    //--Description position. Player can hide it.
    float cPositionOffset = EasingFunction::QuadraticInOut(mPlayerDescriptionTimer, (float)ADVCOMBAT_DESCRIPTION_TICKS) * ADVCOMBAT_DESCRIPTION_OFFSET;

    //--Frame.
    Images.Data.rDescription->Draw(0, cPositionOffset);

    //--Description.
    AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
    RenderSkillDescription(rAbility);

    //--Clean.
    glTranslatef(-69.0f, -40.0f, 0.0f);
}

///=================================== Player Input Submodes ======================================
void AdvCombat::RenderPlayerCards(float pAlpha)
{
    ///--[Documentation]
    //--Renders selection cards and a highlight if in card mode.
    if(pAlpha <= 0.0f || mPlayerCardTimer < 1) return;

    ///--[Position]
    //--Compute offset.
    float cPercent = 1.0f - EasingFunction::QuadraticInOut(mPlayerCardTimer, (float)ADVCOMBAT_PLAYER_INTERFACE_TICKS);
    float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Offset.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    ///--[Rendering]
    //--Cards.
    Images.Data.rCardAttack->Draw();
    Images.Data.rCardJob->Draw();
    Images.Data.rCardMemorized->Draw();
    Images.Data.rCardTactics->Draw();

    //--Text. Uses the yellow heading.
    mHeadingStatic.SetAsMixerAlpha(pAlpha);
    Images.Data.rPlayerCardFont->DrawText( 619.0f, 680.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Attack");
    Images.Data.rPlayerCardFont->DrawText( 810.0f, 680.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Job Skills");
    Images.Data.rPlayerCardFont->DrawText(1001.0f, 680.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Memorized");
    Images.Data.rPlayerCardFont->DrawText(1001.0f, 706.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Skills");
    Images.Data.rPlayerCardFont->DrawText(1191.0f, 680.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Tactics");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Highlight.
    if(mPlayerMode == ADVCOMBAT_INPUT_CARDS) RenderExpandableHighlight(mAbilityHighlightPos, mAbilityHighlightSize, 4.0f, Images.Data.rHighlight);

    ///--[Clean]
    //--Unposition.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);

    //--Clean Mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void AdvCombat::RenderPlayerJobSkills(float pAlpha)
{
    ///--[Documentation]
    //--Renders job selection and associated description.
    if(pAlpha <= 0.0f || mPlayerJobTimer < 1) return;

    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    ///--[Position]
    //--Compute offset.
    float cPercent = 1.0f - EasingFunction::QuadraticInOut(mPlayerJobTimer, (float)ADVCOMBAT_PLAYER_INTERFACE_TICKS);
    float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Offset.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    ///--[Rendering]
    //--Description position. Player can hide it.
    float cPositionOffset = EasingFunction::QuadraticInOut(mPlayerDescriptionTimer, (float)ADVCOMBAT_DESCRIPTION_TICKS) * ADVCOMBAT_DESCRIPTION_OFFSET;

    //--Frames.
    Images.Data.rDescription->Draw(0, cPositionOffset);
    Images.Data.rJobSkillsFrame->Draw();

    //--Setup.
    float cLft = 362.0f;
    float cTop = 524.0f;
    float cSpX =  53.0f;
    float cSpY =  53.0f;

    //--Abilities.
    for(int x = 0; x < 3; x ++)
    {
        for(int y = 0; y < 3; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(x, y);
            if(!rAbility) continue;

            //--Compute position.
            float cRenderX = cLft + (cSpX * x);
            float cRenderY = cTop + (cSpY * y);
            if(y > 0) cRenderY = cRenderY + 8.0f;

            //--Render.
            rAbility->RenderAtCombat(cRenderX, cRenderY, pAlpha);

            //--Skill has a cooldown, render that.
            int tCooldown = rAbility->GetCooldown();
            if(tCooldown > 0)
            {
                //--Cooldown is one digit:
                if(tCooldown < 10)
                {
                    Images.Data.rClockLgIcon->Draw(4.0f, 13.0f);
                    Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(28.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                }
                //--Two digits.
                else
                {
                    Images.Data.rClockLgIcon->Draw(-5.0f, 13.0f);
                    Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(19.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                }
            }
        }
    }

    //--Highlight, description. Only renders if this mode is active.
    if(mPlayerMode == ADVCOMBAT_INPUT_JOB)
    {
        //--Highlight.
        RenderExpandableHighlight(mAbilityHighlightPos, mAbilityHighlightSize, 4.0f, Images.Data.rHighlight);

        //--Description.
        AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
        RenderSkillDescription(rAbility);
    }

    //--Render title.
    mHeadingStatic.SetAsMixerAlpha(pAlpha);
    Images.Data.rAbilityDescriptionFont->DrawText(522.0f, 494.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Job Skills");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Clean]
    //--Unposition.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);

    //--Clean Mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void AdvCombat::RenderPlayerMemorizedSkills(float pAlpha)
{
    ///--[Documentation]
    //--Renders job selection and associated description.
    if(pAlpha <= 0.0f || mPlayerMemorizedTimer < 1) return;

    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    ///--[Position]
    //--Compute offset.
    float cPercent = 1.0f - EasingFunction::QuadraticInOut(mPlayerMemorizedTimer, (float)ADVCOMBAT_PLAYER_INTERFACE_TICKS);
    float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Offset.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    ///--[Rendering]
    //--Description position. Player can hide it.
    float cPositionOffset = EasingFunction::QuadraticInOut(mPlayerDescriptionTimer, (float)ADVCOMBAT_DESCRIPTION_TICKS) * ADVCOMBAT_DESCRIPTION_OFFSET;

    //--Frames.
    Images.Data.rDescription->Draw(0, cPositionOffset);
    Images.Data.rMemorizedSkillsFrame->Draw();

    //--Setup.
    float cLft = 256.0f;
    float cTop = 524.0f;
    float cSpX =  53.0f;
    float cSpY =  53.0f;

    //--Abilities.
    for(int x = 0; x < 5; x ++)
    {
        for(int y = 0; y < 3; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(x + ACE_ABILITY_MEMORIZE_GRID_X0, y);
            if(!rAbility) continue;

            //--Compute position.
            float cRenderX = cLft + (cSpX * x);
            float cRenderY = cTop + (cSpY * y);
            if(y > 1) cRenderY = cRenderY + 8.0f;

            //--Render.
            rAbility->RenderAtCombat(cRenderX, cRenderY, pAlpha);

            //--Skill has a cooldown, render that.
            int tCooldown = rAbility->GetCooldown();
            if(tCooldown > 0)
            {
                //--Cooldown is one digit:
                if(tCooldown < 10)
                {
                    Images.Data.rClockLgIcon->Draw(4.0f, 13.0f);
                    Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(28.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                }
                //--Two digits.
                else
                {
                    Images.Data.rClockLgIcon->Draw(-5.0f, 13.0f);
                    Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(19.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                }
            }
        }
    }

    //--Highlight, description. Only renders if this mode is active.
    if(mPlayerMode == ADVCOMBAT_INPUT_MEMORIZED)
    {
        //--Highlight.
        RenderExpandableHighlight(mAbilityHighlightPos, mAbilityHighlightSize, 4.0f, Images.Data.rHighlight);

        //--Description.
        AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(mAbilitySelectionX + ACE_ABILITY_MEMORIZE_GRID_X0, mAbilitySelectionY);
        RenderSkillDescription(rAbility);
    }

    //--Render title.
    mHeadingStatic.SetAsMixerAlpha(pAlpha);
    Images.Data.rAbilityDescriptionFont->DrawText(522.0f, 494.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Memorized");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Clean]
    //--Unposition.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);

    //--Clean Mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void AdvCombat::RenderPlayerTacticsSkills(float pAlpha)
{
    ///--[Documentation]
    //--Renders job selection and associated description.
    if(pAlpha <= 0.0f || mPlayerTacticsTimer < 1) return;

    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    ///--[Position]
    //--Compute offset.
    float cPercent = 1.0f - EasingFunction::QuadraticInOut(mPlayerTacticsTimer, (float)ADVCOMBAT_PLAYER_INTERFACE_TICKS);
    float cBotOffset = VIRTUAL_CANVAS_Y * cPercent;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Offset.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    ///--[Rendering]
    //--Description position. Player can hide it.
    float cPositionOffset = EasingFunction::QuadraticInOut(mPlayerDescriptionTimer, (float)ADVCOMBAT_DESCRIPTION_TICKS) * ADVCOMBAT_DESCRIPTION_OFFSET;

    //--Frames.
    Images.Data.rDescription->Draw(0, cPositionOffset);
    Images.Data.rTacticsSkillsFrame->Draw();

    //--Setup.
    float cLft =  44.0f;
    float cTop = 532.0f;
    float cSpX =  53.0f;
    float cSpY =  53.0f;

    //--Abilities.
    for(int x = 0; x < 9; x ++)
    {
        for(int y = 0; y < 3; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(x + ACE_ABILITY_TACTICS_GRID_X0, y);
            if(!rAbility) continue;

            //--Compute position.
            float cRenderX = cLft + (cSpX * x);
            float cRenderY = cTop + (cSpY * y);

            //--Render.
            rAbility->RenderAtCombat(cRenderX, cRenderY, pAlpha);

            //--Skill has a cooldown, render that.
            int tCooldown = rAbility->GetCooldown();
            if(tCooldown > 0)
            {
                //--Cooldown is one digit:
                if(tCooldown < 10)
                {
                    Images.Data.rClockLgIcon->Draw(4.0f, 13.0f);
                    Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(28.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                }
                //--Two digits.
                else
                {
                    Images.Data.rClockLgIcon->Draw(-5.0f, 13.0f);
                    Images.Data.rAbilitySimpleDescriptionFont->DrawTextArgs(19.0f, 8.0f, 0, 1.0f, "%i", tCooldown);
                }
            }
        }
    }

    //--Highlight, description. Only renders if this mode is active.
    if(mPlayerMode == ADVCOMBAT_INPUT_TACTICS)
    {
        //--Highlight.
        RenderExpandableHighlight(mAbilityHighlightPos, mAbilityHighlightSize, 4.0f, Images.Data.rHighlight);

        //--Description.
        AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(mAbilitySelectionX + ACE_ABILITY_TACTICS_GRID_X0, mAbilitySelectionY);
        RenderSkillDescription(rAbility);
    }

    //--Render title.
    mHeadingStatic.SetAsMixerAlpha(pAlpha);
    Images.Data.rAbilityDescriptionFont->DrawText(522.0f, 502.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Tactics");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Clean]
    //--Unposition.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);

    //--Clean Mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void AdvCombat::RenderSkillDescription(AdvCombatAbility *pAbility)
{
    ///--[Documentation]
    //--Given an ability, renders its simplified description. Uses almost identical code to the skills menu.
    if(!pAbility) return;

    //--Don't do any rendering if not in a visible mode.
    if(mPlayerDescriptionTimer >= ADVCOMBAT_DESCRIPTION_TICKS) return;

    ///--[Resolve Offset]
    float cPositionOffset = EasingFunction::QuadraticInOut(mPlayerDescriptionTimer, (float)ADVCOMBAT_DESCRIPTION_TICKS) * ADVCOMBAT_DESCRIPTION_OFFSET;

    ///--[Common Rendering]
    //--Name.
    Images.Data.rAbilityHeaderFont->DrawText(619.0f, 462.0f + cPositionOffset, 0, 1.0f, pAbility->GetDisplayName());

    ///--[Basic Description]
    if(mShowDetailedAbilityDescriptions == ADVCOMBAT_DESCRIPTIONS_SIMPLE)
    {
        //--Setup.
        float cTxtL = 561.0f;
        float cTxtT = 516.0f + cPositionOffset;
        float cTxtH =  29.0f;

        //--Description lines.
        for(int i = 0; i < ADVMENU_SKILLS_MAX_DESCRIPTION_LINES; i ++)
        {
            //--Get line.
            StarlightString *rDescriptionLine = pAbility->GetSimplifiedDescriptionLine(i);
            if(!rDescriptionLine) break;

            //--Render.
            rDescriptionLine->DrawText(cTxtL, cTxtT + (cTxtH * i), SUGARFONT_NOCOLOR, 1.0f, Images.Data.rAbilitySimpleDescriptionFont);
        }
    }
    ///--[Advanced Descriptions]
    //--Also shows when hiding the window.
    else
    {
        //--Setup.
        float cTxtL = 561.0f;
        float cTxtT = 516.0f + cPositionOffset;
        float cTxtH =  26.0f;

        //--Description lines.
        for(int i = 0; i < ADVMENU_SKILLS_MAX_DESCRIPTION_LINES; i ++)
        {
            //--Get line.
            StarlightString *rDescriptionLine = pAbility->GetDescriptionLine(i);
            if(!rDescriptionLine) break;

            //--Render.
            rDescriptionLine->DrawText(cTxtL, cTxtT + (cTxtH * i), SUGARFONT_NOCOLOR, 1.0f, Images.Data.rAbilityDescriptionFont);
        }
    }
}

///========================================== New Turn ============================================
void AdvCombat::RenderNewTurn(float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the "TURN X" at the top of the screen. Does nothing if the time has expired.
    if(mTurnDisplayTimer >= ACE_TURN_DISPLAY_TICKS || !Images.mIsReady || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Timer setup.
    int cLetterTicks = ACE_TURN_TICKS_PER_LETTER * 5;
    int cHoldTicks = cLetterTicks + ACE_TURN_TICKS_HOLD;

    //--Populate the buffer. Note that turn 0 renders as "TURN 1". We never pass "TURN 1000", it just stays there
    //  to prevent overflow. The actual turn counter continues to rise.
    int tUseTurns = mCurrentTurn+1;
    if(tUseTurns > 1000) tUseTurns = 1000;
    char tFullBuffer[10];
    sprintf(tFullBuffer, "TURN %i", tUseTurns);

    //--Positions
    float cXStart = (VIRTUAL_CANVAS_X * 0.50f) - (Images.Data.rNewTurnFont->GetTextWidth(tFullBuffer) * 0.50f);
    float cYStart = VIRTUAL_CANVAS_Y * 0.00f;
    float cYEnd   = VIRTUAL_CANVAS_Y * 0.10f;

    //--If we're in the displaying new letters part:
    if(mTurnDisplayTimer < cLetterTicks)
    {
        //--For each letter in "TURN"
        float tXPos = cXStart;
        int tUseTicks = mTurnDisplayTimer;
        for(int i = 0; i < 4; i ++)
        {
            //--Percentage drop. The letter starts at the top of the screen and moves down.
            float cPercent = EasingFunction::QuadraticOut(tUseTicks, ACE_TURN_TICKS_PER_LETTER);
            float cYPos = cYStart + ((cYEnd - cYStart) * cPercent);

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent * pAlpha);
            float tIncrement = Images.Data.rNewTurnFont->DrawLetter(tXPos, cYPos, 0, 1.0f, tFullBuffer[i], tFullBuffer[i+1]);
            tXPos = tXPos + tIncrement;

            //--Clean.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

            //--Next. End if the letter should not display.
            tUseTicks -= ACE_TURN_TICKS_PER_LETTER;
            if(tUseTicks <= 0) break;
        }

        //--Render the turn number if there are ticks for it.
        if(tUseTicks > 0)
        {
            //--Increment by the distance of a space.
            float cPercent = EasingFunction::QuadraticOut(tUseTicks, ACE_TURN_TICKS_PER_LETTER);
            float cYPos = cYStart + ((cYEnd - cYStart) * cPercent);
            float tIncrement = Images.Data.rNewTurnFont->DrawLetter(tXPos, cYPos, 0, 1.0f, ' ', tFullBuffer[5]);
            tXPos = tXPos + tIncrement;

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent * pAlpha);
            Images.Data.rNewTurnFont->DrawText(tXPos, cYPos, 0, 1.0f, &tFullBuffer[6]);

            //--Clean.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }
    }
    //--If we're in the "Hold" part:
    else if(mTurnDisplayTimer < cHoldTicks)
    {
        Images.Data.rNewTurnFont->DrawText(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
    }
    //--If we're in the "Fade" part:
    else
    {
        float cAlpha = 1.0f - EasingFunction::QuadraticOut(mTurnDisplayTimer - cHoldTicks, ACE_TURN_TICKS_FADE);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pAlpha);
        Images.Data.rNewTurnFont->DrawText(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }
}

///====================================== Target Cluster ==========================================
void AdvCombat::RenderTargetCluster()
{
    ///--[Documentation]
    //--At the top-center of the screen, renders a box and the name of the entity being targeted.
    if(!mIsSelectingTargets || !Images.mIsReady) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Get the active target cluster.
    TargetCluster *rActiveCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
    if(!rActiveCluster) return;

    ///--[Positioning and Setup]
    //--Setup.
    float cEdge = 4.0f;
    float cTxtHei = 31.0f;

    //--Text ideal center.
    float cTextCntX = (VIRTUAL_CANVAS_X * 0.50f);
    float cTextCntY = (VIRTUAL_CANVAS_Y * 0.05f);

    //--Get the length of the name.
    int cLength = Images.Data.rTargetClusterFont->GetTextWidth(rActiveCluster->mDisplayName);

    ///--[Box]
    //--Positions.
    float cBoxLft = cTextCntX - (cLength * 0.50f) - cEdge + 0.0f;
    float cBoxTop = cTextCntY - (cTxtHei * 0.50f) - cEdge + 0.0f;
    float cBoxRgt = cTextCntX + (cLength * 0.50f) + cEdge + 0.0f;
    float cBoxBot = cTextCntY + (cTxtHei * 0.50f) + cEdge + 0.0f;
    RenderExpandableBox(cBoxLft, cBoxTop, cBoxRgt, cBoxBot, cEdge, Images.Data.rPredictionBox);

    ///--[Text]
    //--Render the text centered.
    Images.Data.rTargetClusterFont->DrawText(cTextCntX, cTextCntY - 3.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, rActiveCluster->mDisplayName);

    //--Position to render the arrows. Variable based on box width.
    float cArrowLftX = cBoxLft - Images.Data.rArrowLft->GetWidth() - 2.0f;
    float cArrowRgtX = cBoxRgt + 2.0f;
    float cArrowTopY = cBoxTop - 2.0f;

    //--If there are more than one target cluster, render arrows. The cluster wraps so always render both.
    if(mTargetClusters->GetListSize() > 1)
    {
        Images.Data.rArrowLft->Draw(cArrowLftX, cArrowTopY);
        Images.Data.rArrowRgt->Draw(cArrowRgtX, cArrowTopY);
    }
}

///========================================= Title Box ============================================
void AdvCombat::RenderTitle()
{
    ///--[Documentation and Setup]
    //--Shows a title indicating what ability is in use. Typically used by enemies to telegraph their attacks.
    if(!Images.mIsReady || !mShowTitle || !mTitleText) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    ///--[Transparency]
    //--Title fades in over 10 ticks and out over 10 ticks.
    float cAlpha = 1.0f;
    if(mTitleTicks < 10)
    {
        cAlpha = EasingFunction::QuadraticOut(mTitleTicks, 10);
    }
    else if(mTitleTicks >= mTitleTicksMax - 10)
    {
        cAlpha = 1.0f - EasingFunction::QuadraticIn(mTitleTicks - (mTitleTicksMax - 10), 10);
    }

    //--Set alpha.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Positioning and Setup]
    //--Setup.
    float cEdge = 4.0f;
    float cTxtHei = 31.0f;

    //--Text ideal center.
    float cTextCntX = (VIRTUAL_CANVAS_X * 0.50f);
    float cTextCntY = (VIRTUAL_CANVAS_Y * 0.12f);

    //--Get the length of the name.
    int cLength = Images.Data.rTargetClusterFont->GetTextWidth(mTitleText);

    ///--[Box]
    //--Positions.
    float cBoxLft = cTextCntX - (cLength * 0.50f) - cEdge + 0.0f;
    float cBoxTop = cTextCntY - (cTxtHei * 0.50f) - cEdge + 0.0f;
    float cBoxRgt = cTextCntX + (cLength * 0.50f) + cEdge + 0.0f;
    float cBoxBot = cTextCntY + (cTxtHei * 0.50f) + cEdge + 0.0f;

    //--Clamp. The left position can never be less than 115.0f.
    if(cBoxLft < 115.0f)
    {
        float cDif = 115.0f - cBoxLft;
        cBoxLft = cBoxLft + cDif;
        cBoxRgt = cBoxRgt + cDif;
    }

    //--Render.
    RenderExpandableBox(cBoxLft, cBoxTop, cBoxRgt, cBoxBot, cEdge, Images.Data.rPredictionBox);

    ///--[Text]
    //--Render the text centered.
    Images.Data.rAbilityTitleFont->DrawText(cTextCntX, cTextCntY - 3.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, mTitleText);

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///====================================== Ability Blocks ==========================================
void AdvCombat::RenderAbilityBlock(float pX, float pY, int pType, AdvCombatEntity *pEntity)
{
    ///--[Documentation]
    //--Given an entity and an ability block, renders the skills in that blocking.
    if(!pEntity) return;

    ///--[Constants]
    //--Spacing.
    float cSpX =  53.0f;
    float cSpY =  53.0f;

    ///--[Job]
    if(pType == ADVCOMBAT_ABILITY_BLOCK_JOB)
    {
        for(int x = 0; x < 3; x ++)
        {
            for(int y = 0; y < 3; y ++)
            {
                //--Skip empty slots.
                AdvCombatAbility *rAbility = pEntity->GetAbilityBySlot(x, y);
                if(!rAbility) continue;

                //--Compute position.
                float cRenderX = pX + (cSpX * x);
                float cRenderY = pY + (cSpY * y);
                if(y > 0) cRenderY = cRenderY + 8.0f;

                //--Render.
                rAbility->RenderAt(cRenderX, cRenderY);
            }
        }
    }
    ///--[Memorized]
    else if(pType == ADVCOMBAT_ABILITY_BLOCK_MEMORIZED)
    {
        for(int x = 0; x < 5; x ++)
        {
            for(int y = 0; y < 3; y ++)
            {
                //--Skip empty slots.
                AdvCombatAbility *rAbility = pEntity->GetAbilityBySlot(x + ACE_ABILITY_MEMORIZE_GRID_X0, y);
                if(!rAbility) continue;

                //--Compute position.
                float cRenderX = pX + (cSpX * x);
                float cRenderY = pY + (cSpY * y);
                if(y > 1) cRenderY = cRenderY + 8.0f;

                //--Render.
                rAbility->RenderAt(cRenderX, cRenderY);
            }
        }
    }
    ///--[Tactics]
    else if(pType == ADVCOMBAT_ABILITY_BLOCK_TACTICS)
    {
        for(int x = 0; x < 9; x ++)
        {
            for(int y = 0; y < 3; y ++)
            {
                //--Skip empty slots.
                AdvCombatAbility *rAbility = pEntity->GetAbilityBySlot(x + ACE_ABILITY_TACTICS_GRID_X0, y);
                if(!rAbility) continue;

                //--Compute position.
                float cRenderX = pX + (cSpX * x);
                float cRenderY = pY + (cSpY * y);

                //--Render.
                rAbility->RenderAt(cRenderX, cRenderY);
            }
        }
    }
}
