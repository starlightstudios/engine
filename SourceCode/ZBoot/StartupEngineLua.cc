///--[ ============ Base ============ ]
///--[ ========== Classes =========== ]
///--[AdvCombat]
#include "AbyssCombat.h"
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "CarnationCombat.h"
#include "CarnationCombatEntity.h"

///--[Debug]
#include "BuildPackage.h"

///--[Entities]
#include "Actor.h"
#include "RootEffect.h"
#include "TilemapActor.h"

///--[Events]
#include "ActorEvent.h"
#include "AudioEvent.h"
#include "CameraEvent.h"
#include "RootEvent.h"
#include "TimerEvent.h"

///--[GUI]
#include "AdventureDebug.h"
#include "AdventureMenu.h"
#include "AdvUIJournal.h"
#include "CarnUIEvent.h"
#include "CarnUIClassChange.h"
#include "CarnUIHealParty.h"
#include "CarnUIInventory.h"
#include "CarnUISkills.h"
#include "CarnUIVendor.h"
#include "CarnationMenu.h"
#include "CarnationMinimap.h"
#include "CorrupterMenu.h"
#include "DialogueActor.h"
#include "StringTyrantTitle.h"
#include "MonocerosMenu.h"
#include "WorldDialogue.h"
#include "MonoUISkills.h"

///--[Inventory]
//--AdventureInventory
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--ClassicInventory
#include "InventoryItem.h"
#include "WorldContainer.h"

///--[Lights]
///--[Loading]
#include "Chapter0LoadInterrupt.h"
#include "Chapter1LoadInterrupt.h"
#include "Chapter2LoadInterrupt.h"
#include "Chapter5LoadInterrupt.h"
#include "MonocerosLoadInterrupt.h"
#include "StringTyrantLoadInterrupt.h"

///--[Minigames]
#include "KPopDanceGame.h"
#include "PuzzleFight.h"
#include "ShakeMinigame.h"

///--[Packages]
#include "MagicPack.h"
#include "PerkPack.h"
#include "TransformPack.h"

///--[Pathing]
#include "Pathrunner.h"
#include "PathrunnerTile.h"

///--[Storage]
#include "AliasStorage.h"

///--[Title Screens]
#include "CarnationTitle.h"

///--[Translation]
#include "TranslationExecutor.h"

///--[World]
#include "AdventureLevel.h"
#include "AdventureLevelGenerator.h"
#include "BlueSphereLevel.h"
#include "CarnationLevel.h"
#include "CarnationTitle.h"
#include "FieldAbility.h"
#include "FlexButton.h"
#include "FlexMenu.h"
#include "GalleryMenu.h"
#include "MonocerosTitle.h"
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"
#include "SlittedEyeLevel.h"
#include "ScriptObjectBuilder.h"
#include "TextLevel.h"
#include "VisualLevel.h"
#include "VisualRoom.h"

///--[ ========= Executors ========== ]
///--[ ============ Gears =========== ]
///--[ ========= Interfaces ========= ]
///--[ =========== Player =========== ]
#include "PlayerPony.h"
#include "LuaContextOption.h"

///--[ ========== Samples =========== ]
#include "RootObject.h"

///======================================= Registration ===========================================
void RegisterEngineLuaFunctions(lua_State *pLuaState)
{
    ///--[Documentation]
    //--Called after the LuaManager is created, registers every function in the program to its
    //  Lua state. Lua scripts should not execute before this.

    ///--[ ============ Base ============ ]
    ///--[ ========== Classes =========== ]
    ///--[AdvCombat]
    AbyssCombat::HookToLuaState(pLuaState);
    AdvCombat::HookToLuaState(pLuaState);
    AdvCombatAbility::HookToLuaState(pLuaState);
    AdvCombatAnimation::HookToLuaState(pLuaState);
    AdvCombatEffect::HookToLuaState(pLuaState);
    AdvCombatEntity::HookToLuaState(pLuaState);
    AdvCombatJob::HookToLuaState(pLuaState);
    CarnationCombat::HookToLuaState(pLuaState);
    CarnationCombatEntity::HookToLuaState(pLuaState);

    ///--[Debug]
    BuildPackage::HookToLuaState(pLuaState);

    ///--[Entities]
    RootEntity::HookToLuaState(pLuaState);
    RootEffect::HookToLuaState(pLuaState);
    Actor::HookToLuaState(pLuaState);
    TilemapActor::HookToLuaState(pLuaState);

    ///--[Events]
    RootEvent::HookToLuaState(pLuaState);
    ActorEvent::HookToLuaState(pLuaState);
    AudioEvent::HookToLuaState(pLuaState);
    CameraEvent::HookToLuaState(pLuaState);
    TimerEvent::HookToLuaState(pLuaState);

    ///--[GUI]
    AdventureDebug::HookToLuaState(pLuaState);
    AdventureMenu::HookToLuaState(pLuaState);
    AdvUIJournal::HookToLuaState(pLuaState);
    CarnationMenu::HookToLuaState(pLuaState);
    CarnationMinimap::HookToLuaState(pLuaState);
    CarnUIEvent::HookToLuaState(pLuaState);
    CarnUIClassChange::HookToLuaState(pLuaState);
    CarnUIHealParty::HookToLuaState(pLuaState);
    CarnUIInventory::HookToLuaState(pLuaState);
    CarnUISkills::HookToLuaState(pLuaState);
    CarnUIVendor::HookToLuaState(pLuaState);
    CorrupterMenu::HookToLuaState(pLuaState);
    DialogueActor::HookToLuaState(pLuaState);
    FlexMenu::HookToLuaState(pLuaState);
    FlexButton::HookToLuaState(pLuaState);
    GalleryMenu::HookToLuaState(pLuaState);
    StringTyrantTitle::HookToLuaState(pLuaState);
    MonocerosMenu::HookToLuaState(pLuaState);
    WorldDialogue::HookToLuaState(pLuaState);
    MonoUISkills::HookToLuaState(pLuaState);

    ///--[Inventory]
    //--AdventureInventory
    AdventureInventory::HookToLuaState(pLuaState);
    AdventureItem::HookToLuaState(pLuaState);

    //--ClassicInventory
    InventoryItem::HookToLuaState(pLuaState);
    WorldContainer::HookToLuaState(pLuaState);

    ///--[Lights]
    ///--[Loading
    Chapter0LoadInterrupt::HookToLuaState(pLuaState);
    Chapter1LoadInterrupt::HookToLuaState(pLuaState);
    Chapter2LoadInterrupt::HookToLuaState(pLuaState);
    Chapter5LoadInterrupt::HookToLuaState(pLuaState);
    MonocerosLoadInterrupt::HookToLuaState(pLuaState);
    StringTyrantLoadInterrupt::HookToLuaState(pLuaState);

    ///--[Minigames]
    KPopDanceGame::HookToLuaState(pLuaState);
    PuzzleFight::HookToLuaState(pLuaState);
    ShakeMinigame::HookToLuaState(pLuaState);

    ///--[Packages]
    MagicPack::HookToLuaState(pLuaState);
    PerkPack::HookToLuaState(pLuaState);
    TransformPack::HookToLuaState(pLuaState);

    ///--[Pathing]
    Pathrunner::HookToLuaState(pLuaState);
    PathrunnerTile::HookToLuaState(pLuaState);

    ///--[Storage]
    AliasStorage::HookToLuaState(pLuaState);

    ///--[Title Screens]
    CarnationTitle::HookToLuaState(pLuaState);

    ///--[Translation]
    ///--[World]
    AdventureLevel::HookToLuaState(pLuaState);
    AdventureLevelGenerator::HookToLuaState(pLuaState);
    BlueSphereLevel::HookToLuaState(pLuaState);
    CarnationLevel::HookToLuaState(pLuaState);
    CarnationTitle::HookToLuaState(pLuaState);
    FieldAbility::HookToLuaState(pLuaState);
    MonocerosTitle::HookToLuaState(pLuaState);
    PandemoniumLevel::HookToLuaState(pLuaState);
    PandemoniumRoom::HookToLuaState(pLuaState);
    SlittedEyeLevel::HookToLuaState(pLuaState);
    ScriptObjectBuilder::HookToLuaState(pLuaState);
    TextLevel::HookToLuaState(pLuaState);
    VisualLevel::HookToLuaState(pLuaState);
    VisualRoom::HookToLuaState(pLuaState);

    ///--[ ========= Executors ========== ]
    TranslationExecutor::HookToLuaState(pLuaState);

    ///--[ ============ Gears =========== ]
    ///--[ ========= Interfaces ========= ]
    ///--[ =========== Player =========== ]
    LuaContextOption::HookToLuaState(pLuaState);
    PlayerPony::HookToLuaState(pLuaState);

    ///--[ ========== Samples =========== ]
    RootObject::HookToLuaState(pLuaState);
}
