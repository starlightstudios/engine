///========================================= TextLevel ============================================
//--Handler of Text Adventure Mode. Controls input, image layering, and parsing.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "TiledLevel.h"

///===================================== Local Definitions ========================================
//--Stored in this handy file.
#include "TextLevelStructures.h"

#define TL_STANDARD_PAD 2
#define TL_EXTRA_LINES 3
#define TL_MOST_RECENT_SAVES 20
#define TL_MUSIC_RESTART_POST_COMBAT (int)(0.05f * SECONDSTOTICKS)
#define TL_LOCALITY_SIZE 32.0f
#define TL_FADE_TICKS 180
#define TL_MAX_TEXTLOG_LINES 100

///===================================== Local Structures =========================================
///--[TextLevelTempDoor]
//--Stores doors by name until they are placed in the final structure.
typedef struct TextLevelTempDoor
{
    int mX, mY, mZ;
    char mDoorDir; //Typically N or E
    char mDoorName[STD_MAX_LETTERS];
}TextLevelTempDoor;

///--[TextLevelTempString]
//--Stores a string property until placement in the final structure.
typedef struct TextLevelTempString
{
    int mX, mY, mZ;
    char mString[STD_MAX_LETTERS];
}TextLevelTempString;

///--[TextLevelPatrolDataPack]
//--Patrol Data package, stores a patrol route and associated data until it can be sent to the Lua.
#define TLPDP_MAX_NODES 100
typedef struct TextLevelPatrolDataPack
{
    char mEnemyUniqueName[STD_MAX_LETTERS];
    char mEnemyType[STD_MAX_LETTERS];
    int mTotalNodes;
    Point3DI mNodes[TLPDP_MAX_NODES];
    void Initialize()
    {
        strcpy(mEnemyUniqueName, "Null");
        strcpy(mEnemyType, "Null");
        mTotalNodes = 0;
        for(int i = 0; i < TLPDP_MAX_NODES; i ++)
        {
            mNodes[i].mX = -10000;
            mNodes[i].mY = -10000;
            mNodes[i].mZ = -10000;
        }
    }
}TextLevelPatrolDataPack;

///--[HarshOverlayPack]
//--Pack used for harsh overlays. Emulates mist/blackness on parts of the map the player
//  cannot currently see.
#define TL_OVERLAY_TICKS 20
#define TL_OVERLAY_EXCLUSION 0
#define TL_OVERLAY_NORMAL 1
#define TL_OVERLAY_BYPASS_EXCLUSION 2
typedef struct HarshOverlayPack
{
    //--Members.
    int mTimer;
    float mScrollX;
    float mScrollY;
    float mScrollXSpeed;
    float mScrollYSpeed;
    float mScrollClampX;
    float mScrollClampY;
    StarBitmap *rImage;
    int mOverlayFlags;

    //--Functions.
    void Initialize()
    {
        mTimer = 0;
        mScrollX = 0.0f;
        mScrollY = 0.0f;
        mScrollXSpeed = 0.0f;
        mScrollYSpeed = 0.0f;
        mScrollClampX = 1.0f;
        mScrollClampY = 1.0f;
        rImage = NULL;
        mOverlayFlags = TL_OVERLAY_NORMAL;
    }
}HarshOverlayPack;

///--[TextLevelPlayerMapPack]
//--Used for rendering the player's map. Each layer can be toggled on or off.
#define TL_PLAYERMAP_VIS_TICKS 30
typedef struct TextLevelPlayerMapPack
{
    bool mIsActive;
    int mPriority;
    StarBitmap *rImage;
    void Initialize()
    {
        mIsActive = false;
        mPriority = 0;
        rImage = NULL;
    }
}TextLevelPlayerMapPack;

///========================================== Classes =============================================
class TextLevel : public TiledLevel
{
    private:
    //--System
    uint32_t mIDCounter;
    RootLevel *mSuspendedLevel;

    //--UI Flags
    bool mNoSettingsButton;
    bool mNoZoomButtons;
    bool mNoNavigationButtons;

    //--Deck Editor
    DeckEditor *mDeckEditor;

    //--Options Menu
    TextLevOptions *mOptionsScreen;

    //--Savefile Storage
    char mSavefileNames[TL_MOST_RECENT_SAVES][STD_PATH_LEN];

    //--Music
    bool mPlayTextTick;
    StarLinkedList *mMusicTracksList; //TextLevMusicPack *, master

    //--Images
    bool mIsDarkMode;
    int mImgTransitionTimer;
    char *mPlayerStoredImgPath;
    char *mStandardName;
    char *mOverrideName;
    StarBitmap *rStandardRenderImg[TL_PLAYER_LAYERS_MAX];
    StarBitmap *rOverrideRenderImg[TL_PLAYER_LAYERS_MAX];
    StarBitmap *rPreviousRenderImg[TL_PLAYER_LAYERS_MAX];

    //--Dialogue Display
    bool mUseLargeText;
    int mTextScrollOffset;
    bool mIsDraggingTextScrollbar;
    float cTxtStartX;
    float cTxtEndX;
    float cTxtWidth;
    int mAppendTimer;
    StarLinkedList *mTextLog;
    StarLinkedList *mFontLog;

    //--Map Storage
    int mActiveZLevel;
    int mActiveXOff;
    int mActiveYOff;
    StarLinkedList *mZLevels;

    //--Map Builder Properties
    int mLftMost;
    int mTopMost;
    int mRgtMost;
    int mBotMost;
    int mZLoMost;
    int mZHiMost;
    int mBuilderWorldX;
    int mBuilderWorldY;
    int mBuilderWorldZ;
    StarLinkedList *mTempDoorList; //TextLevelTempDoor *, master
    StarLinkedList *mTempAudioList; //TextLevelTempString *, master
    StarLinkedList *mTempPatrolList; //TextLevelPatrolDataPack *, master
    StarLinkedList *mTempLocationList; //TextLevelTempString *, master
    AutomapPack ****mrBuilderWorld; //AutomapPack, Reference
    char mMapPropertyBuffer[STD_MAX_LETTERS];

    //--Player Information
    int mPlayerMoveTimer;
    int mPlayerMoveTimerMax;
    int mPlayerWorldX;
    int mPlayerWorldY;
    int mPlayerWorldZ;
    char mPlayerWorldClickHandler[STD_MAX_LETTERS];
    TwoDimensionReal cDoorNorth;
    TwoDimensionReal cDoorEast;
    TwoDimensionReal cDoorSouth;
    TwoDimensionReal cDoorWest;
    TwoDimensionReal cDoorDetectNorth;
    TwoDimensionReal cDoorDetectEast;
    TwoDimensionReal cDoorDetectSouth;
    TwoDimensionReal cDoorDetectWest;

    //--Map Storage
    bool mRebuildIndicators;
    int mWorldOffsetX;
    int mWorldOffsetY;
    int mWorldOffsetZ;
    int mWorldSizeX;
    int mWorldSizeY;
    int mWorldSizeZ;
    int mCurrentRenderingZ;
    int mUnderRenderingZLo;
    int mUnderRenderingZHi;
    AutomapPack ****mrFixedWorld;
    StarLinkedList *mVisitedRoomList; //AutomapPack, Reference
    StarLinkedList *mAutomapList;     //AutomapPack
    StarLinkedList *mConnectionList;  //AutomapPack

    //--Player Map
    bool mPlayerMapActive;
    int mPlayerMapTimer;
    StarLinkedList *mPlayerMapLayers; //TextLevelPlayerMapPack *, Master

    //--Tileset for Automap
    float mScreenTranslationX;
    float mScreenTranslationY;
    bool mUseBlankAutomap;
    StarBitmap *rAutomapTileset;
    float mTilemapXSize;
    float mTilemapYSize;
    float mTilemapXSizePerTile;
    float mTilemapYSizePerTile;
    float mLineShadeX;
    float mLineShadeY;
    float mLineCornerShadeX;
    float mLineCornerShadeY;
    float mLineAlleyShadeX;
    float mLineAlleyShadeY;
    float mLineBoxShadeX;
    float mLineBoxShadeY;
    float mLineInsetShadeX;
    float mLineInsetShadeY;
    float mFullblackShadeX;
    float mFullblackShadeY;
    float mExclamationX;
    float mExclamationY;
    float mQuestionX;
    float mQuestionY;

    //--Tileset Animations
    StarLinkedList *mTilesetAnimations;

    //--Harsh Overlay
    bool mIsHarshOverlayMode;
    int mHarshOverlayTimer;
    StarLinkedList *mHarshOverlayList;

    //--Fade-in
    bool mIsFadingOut;
    bool mIsFadingIn;
    int mFadeTimer;
    char *mExecScriptOnFadeCompletion;

    //--Background
    int mOldMouseZ;
    float mBGFocusX;
    float mBGFocusY;
    float mBackgroundScale;
    int mScaleIndex;
    float mScaleListing[TL_SCALES_TOTAL];
    float mMapFadeTimer;
    float mMapFadeDelta;

    //--Text Input
    char *mLastEnteredString;
    StringEntry *mLocalStringEntry;
    int mInputLogScroll;
    StarLinkedList *mInputLog;

    //--Handling
    char *mScriptExecPath;

    //--Instructions
    bool mIsBlockPending;
    StarLinkedList *mInstructionList; //TextInstructionPack

    //--Combat Handling
    int mIsCombatMode;
    int mUseCombatType;
    bool mUseCombatActiveMode;
    char *mPlayerCombatRenderPath; //Only used for Typing Combat.
    TypingCombat *mTypingCombatHandler;
    WordCombat *mWordCombatHandler;
    char *mCombatVictoryHandler;
    char *mCombatDefeatHandler;
    int mPostCombatTimer;

    //--Persistent Player Stats
    bool mHidePlayerStats;
    int mPlayerHP;
    int mPlayerHPMax;
    int mPlayerAtk;
    int mPlayerDef;
    int mPotionCount;

    //--Additional Text
    char mStorageLines[TL_EXTRA_LINES][STD_MAX_LETTERS];

    //--Locality Window
    bool mLocalityUsesIcons;
    bool mLocalityNeedsSort;
    int mLocalityHighlight;
    int mLocalityScroll;
    TwoDimensionReal mLocalityWindow;
    StarLinkedList *mCurrentLocalityList;
    StarLinkedList *mPreviousLocalityList;
    bool mIsDraggingLocalityBar;
    float mOriginalDragPoint;

    //--Popup Command Listing
    int mPopupHighlight;
    TwoDimensionReal mPopupWindowDim;
    TwoDimensionReal mPopupSelectionDim;
    StarLinkedList *mPopupCommandList;

    //--Navigation Buttons
    uint16_t mNavButtonFlags;
    TwoDimensionReal mSettingsDim;
    TwoDimensionReal mMapAreaDim;
    TwoDimensionReal mNavButtonDim[TL_NAV_INDEX_TOTAL];

    //--Major Dialogue Handler
    bool mIsMajorDialogueMode;
    bool mIsMajorDialogueModeExit;
    bool mActivateMajorDialogueWhenQueueEmpties;
    int mMajorDialogueTimer;
    bool mMajorDialogueFadeOut;
    bool mMajorDialogueEndsGame;

    //--Rendering Constants
    float mPlayerMovePercent;
    float mPlayerTranslationX;
    float mPlayerTranslationY;
    float mSizePerRoomX;
    float mSizePerRoomY;
    float mHfX;
    float mHfY;
    float mTexXPerTile;
    float mTexYPerTile;
    float cDepthTerrain;
    float cDepthFriendly;
    float cDepthHostile;
    float cDepthPlayer;

    //--Rendering Temporary Variables
    static float cRenderX[4];
    static float cRenderY[4];
    static float cTxL;
    static float cTxT;
    static float cTxR;
    static float cTxB;
    static float cTxLExc;
    static float cTxTExc;
    static float cTxRExc;
    static float cTxBExc;
    static float cTxLQue;
    static float cTxTQue;
    static float cTxRQue;
    static float cTxBQue;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontMenuSize20;
            StarFont *rFontMenuSize30;

            //--UI Parts
            StarBitmap *rMapParts;
            StarBitmap *rScrollbar;
            StarBitmap *rPopupBorderCard;
            StarBitmap *rTextInput;
            StarBitmap *rTextInputDark;
            StarBitmap *rMask_World;
            StarBitmap *rMask_Text;
            StarBitmap *rMask_Character;
            StarBitmap *rMask_Locality;
            StarBitmap *rHPIndicatorF;
            StarBitmap *rHPIndicatorH;
            StarBitmap *rHPIndicatorE;
            StarBitmap *rOverlayMove;
            StarBitmap *rOverlayDoorH;
            StarBitmap *rOverlayDoorV;

            //--Navigation
            StarBitmap *rNavLook;
            StarBitmap *rNavNorth;
            StarBitmap *rNavUp;
            StarBitmap *rNavWest;
            StarBitmap *rNavWait;
            StarBitmap *rNavEast;
            StarBitmap *rNavThink;
            StarBitmap *rNavSouth;
            StarBitmap *rNavDown;
            StarBitmap *rNavZoomBar;
            StarBitmap *rNavZoomTick;
            StarBitmap *rNavImg[TL_NAV_INDEX_TOTAL];
        }Data;
    }Images;

    protected:

    public:
    //--System
    TextLevel();
    virtual ~TextLevel();
    void ConstructElectrosprite();
    void ConstructStringTyrant();

    //--Public Variables
    static bool xIsBuildingCommands;
    static char *xCommandHeadingString;
    static char *xCommandBaseString;

    //--Property Queries
    bool CanRunInstructions();
    bool IsCombatActive();
    bool IsLargeTextMode();
    bool IsDarkTheme();
    int GetPlayerHP();
    int GetPotions();
    int ComputeVisibleLocalityEntries();
    int GetCurrentZoomIndex();
    const char *GetSaveIndex(int pIndex);
    int GetDeckEditorCardCount(int pCardType);

    //--Manipulators
    void SetDarkMode(bool pFlag);
    void SetLargeText(bool pFlag);
    void StartFadeIn(const char *pPostExec);
    void StartFadeOut(const char *pPostExec);
    void SetPlayerWorldTile(int pX, int pY, int pZ);
    void ActivateDeckEditor();
    void ActivateOptionsMenu();
    void SetDeckEditorPostExec(const char *pPath);
    void SetDeckEditorHelpExec(const char *pPath);
    void SetDeckEditorHandCount(int pType, int pCurrent, int pMinimum, int pMaximum);
    void SetDeckEditorSizeCounts(int pMinimum, int pMaximum);
    void SetDeckEditorBonus(int pType, int pValue);
    void SetDeckEditorSentenceBonus(const char *pValue);
    void SetDeckEditorHelpHeader(const char *pString);
    void SetDeckEditorHelpLine(int pIndex, const char *pString);
    void ProvideSuspendedLevel(RootLevel *pLevel);
    void SetZoomIndex(int pIndex);
    void SetNavFlags(uint16_t pFlags);
    void RegisterAmbientMusicTrack(const char *pAudioManagerName, const char *pInternalName, float pStartingVolume);
    void ModifyAmbientMusicTrack(const char *pInternalName, float pTargetVolume);
    void SetStringHandler(const char *pPath);
    void SetDefaultImage(const char *pName, const char *pPath, int pLayer);
    void RegisterImage(const char *pName, const char *pDLPath, int pLayer);
    void UnregisterImage();
    void SetMapFocus(float pX, float pY);
    void SetLocalityIconMode(bool pFlag);
    void RegisterLocalityToBase(const char *pName, const char *pBuilderScript);
    void RegisterLocalityToBaseAsList(const char *pName, const char *pImgPathUp, const char *pImgPathDn);
    void RegisterLocalityToSub(const char *pName, const char *pSublistName, const char *pBuilderScript);
    void RegisterLocalityToSub(const char *pName, const char *pSublistName, const char *pBuilderScript, int pPriority);
    void ClearLocalityList();
    void ResumeLocalityOpenStates();
    void SetLocalityOpen(const char *pName, bool pIsOpen);
    void RegisterPopupCommand(const char *pDisplayName, const char *pExecutionString);
    void ActivateMajorDialogueMode();
    void ActivateEndingMode();
    void RegisterAnimation(const char *pName, int pTPF, int pStartX, int pStartY, int pTotalFrames, int pFramesPerRow);
    void SetMapFadeTimer(float pTimer);
    void SetMapFadeDelta(float pDelta);
    void SetStorageLine(int pLine, const char *pText);

    //--Core Methods
    void ConstructDeckEditor();
    void RunCommand(const char *pCommand);
    uint32_t GetLastID();
    uint32_t GenerateID();
    void RegisterInstruction(TextInstructionPack *pPack);
    void RegisterBlocker();
    void RegisterAppend(const char *pDialogue);
    void RegisterExecScript(const char *pPath, int pArgs, const char **pArgList);
    void RegisterPlaySFX(const char *pSFXName);
    void RegisterFadeOut(const char *pPostExec);
    void RegisterFadeIn(const char *pPostExec);
    LocalityCategory *GetNthParent(int pSlot);
    LocalityCategory *GetNthCategory(int pSlot);
    void ComputePopupWindowDimensions();
    int PopulateSavePaths(const char *pDirectory);
    void SortLocalityWindow();
    static int SortLocality(const void *pEntryA, const void *pEntryB);
    void ComputeIndicatorPositions();
    void ClampLocalityScroll();
    void HandleWorldMouse();
    void HandleTextMouse();

    ///--[Combat]
    void SetCombatType(int pType);
    void SetCombatActiveMode(bool pIsActiveMode);
    void SetHideStatsFlag(bool pFlag);
    void SetPlayerStats(int pHP, int pHPMax, int pAtk, int pDef, const char *pImgPath);
    void SetCombatScripts(const char *pVictoryScript, const char *pDefeatScript);
    void ActivateCombat();
    void FinalizeCombat();
    void ReresolveStatsAfterCombat();
    void UpdateCombat();
    void RenderCombat();

    ///--[Map Builder]
    int GetLftExtent();
    int GetTopExtent();
    int GetRgtExtent();
    int GetBotExtent();
    int GetZLoExtent();
    int GetZHiExtent();
    void AssembleBuilderWorld();
    void DeallocateBuilderWorld();
    void BuilderWorldConnectivity();
    const char *GetBuilderStringBuffer();
    AutomapPack *GetBuilderPack(int pX, int pY, int pZ);
    bool DoesBuilderRoomExist(int pX, int pY, int pZ);
    bool IsBuilderRoomUnenterable(int pX, int pY, int pZ);
    bool IsBuilderRoomNoSee(int pX, int pY, int pZ);
    void PopulateBuilderRoomName(int pX, int pY, int pZ);
    void PopulateBuilderRoomIdentity(int pX, int pY, int pZ);
    void PopulateBuilderRoomConnectivity(int pX, int pY, int pZ);
    void PopulateBuilderRoomOpenEdges(int pX, int pY, int pZ);
    void PopulateBuilderRoomDoor(int pX, int pY, int pZ, int pDoorIndex);
    void PopulateBuilderRoomAnimation(int pX, int pY, int pZ, int pAnimIndex);
    void PopulateBuilderRoomAudio(int pX, int pY, int pZ);
    void PopulateBuilderRoomStrangerGroup(int pX, int pY, int pZ);
    void PopulateBuilderPatrolIdentity(int pPatrolIndex);
    void PopulateBuilderPatrolEnemyType(int pPatrolIndex);
    void PopulateBuilderPatrolLocation(int pPatrolIndex, int pNodeIndex);
    void PopulateBuilderRoomName(int pLocationIndex);
    void PopulateBuilderLocationName(int pLocationIndex);
    void PopulateBuilderLocationRoom(int pLocationIndex);
    int GetBuilderVisOverrideAt(int pX, int pY, int pZ, int pDirection);
    int GetBuilderVisDown(int pX, int pY, int pZ);
    TextLevZLevel *GetZLevel(int pZLevel);

    ///--[Overlays]
    void ActivateOverlayMode();
    void DeactivateOverlayMode();
    void ClearOverlays();
    void RegisterOverlay(const char *pName, const char *pImage, int pFlags, float pStartX, float pStartY, float pXScroll, float pYScroll, float pXClamp, float pYClamp);
    void ModifyOverlay(const char *pName, const char *pImage);
    void UpdateOverlays();

    ///--[Parser]
    void ParseSLFFile(const char *pPath, int pZLevel);
    virtual void ParseObjectData();
    void HandleObject(ObjectInfoPack *pPack);

    ///--[Player Map]
    void RegisterPlayerMapLayer(const char *pName, int pPriority, const char *pImage);
    void SetPlayerMapLayerVisible(const char *pName, bool pIsVisible);
    void ActivatePlayerMap();
    void DeactivatePlayerMap();
    bool UpdatePlayerMap(bool pDisallowControls);
    void RenderPlayerMap();

    ///--[World Construction and Editing]
    void SetAutomapTileset(const char *pDLPath, float pXSizePerTile, float pYSizePerTile);
    void SetTilesetFadePositions(float pLineX, float pLineY, float pCornX, float pCornY, float pAlleyX, float pAlleyY, float pBoxX, float pBoxY, float pInsetX, float pInsetY, float pFullblackX, float pFullblackY);
    void SetTilesetAlarmPositions(float pExclamationX, float pExclamationY, float pQuestionX, float pQuestionY);
    void RegisterAutomapRoom(const char *pName, float pX, float pY, float pZ);
    void RegisterRoomConnection(const char *pNameA, const char *pNameB);
    void SetRoomExplored(float pX, float pY, float pZ, bool pIsExplored);
    void SetRoomVisible(float pX, float pY, float pZ, bool pIsVisible);
    void SetRoomFlags(float pX, float pY, float pZ, bool pHasHostile, bool pHasFriendly, bool pHasExaminable, bool pHasItem);
    void AddRoomTilemapLayer(float pX, float pY, float pZ, int pTileX, int pTileY, int pSlot, uint8_t pRotationFlags);
    void SetDoorState(float pX, float pY, float pZ, bool pIsSouthDoor, bool pIsOpen);
    void SetDoorIndicator(float pX, float pY, float pZ, bool pIsSouthDoor, int pTileX, int pTileY);
    void RegisterTilemapIndicator(float pX, float pY, float pZ, const char *pName, int pTileX, int pTileY, int pPriority);
    void UnregisterTilemapIndicator(float pX, float pY, float pZ, const char *pName);
    void UnregisterAllTilemapIndicators();
    void ModifyTilemapIndicator(float pX, float pY, float pZ, const char *pName, int pTileX, int pTileY, int pPriority);
    void ModifyTilemapIndicatorHostility(float pX, float pY, float pZ, const char *pName, int pHostileX, int pHostileY);
    void MoveTilemapIndicator(float pX, float pY, float pZ, const char *pName, float pNewX, float pNewY, float pNewZ);
    void ClearRoomFlags(float pX, float pY, float pZ);
    void RegisterAnimationToRoom(float pX, float pY, float pZ, const char *pAnimationName);
    void FinalizeWorld();
    void CleanUpTempStructures();
    void RecomputeFadeVisibility();
    void SetRenderingZ(int pZLevel);
    void SetUnderRenderingZ(int pZLevelLo, int pZLevelHi);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    bool HandleEvents(bool pBypass);

    //--File I/O
    void SaveFile(const char *pFileName);
    void LoadFile(const char *pFileName);

    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();
    void RenderGUI();
    void RenderTextZone();
    void RenderPortraitWindow();
    void RenderPortraitNormal();
    void RenderPortraitCrossfade();
    void RenderLocalityWindow();
    void RenderPopupWindow();

    //--Automap Rendering
    void RenderAutomap();
    void RenderBlankAutomap(TwoDimensionReal pAreaDim);
    void RenderTiledAutomap(TwoDimensionReal pAreaDim);
    void RenderAutomapTile(AutomapPack *pTile, float pOffsetX, float pOffsetY, uint16_t pRenderProperties);
    void RenderAutomapTileDoors(AutomapPack *pTile, float pOffsetX, float pOffsetY, bool pIsForceGreyed);
    void RenderFadeBorder(float pX, float pY, int pBorderCode, int pInsetCode, float pBorderAlpha, float pInsetAlpha);

    //--Pointer Routing
    RootLevel *LiberateSuspendedLevel();
    TypingCombat *GetTypingCombatHandler();
    WordCombat *GetWordCombatHandler();

    //--Static Functions
    static TextLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_TL_Create(lua_State *L);
int Hook_TL_CreateAndSuspend(lua_State *L);
int Hook_TL_Unsuspend(lua_State *L);
int Hook_TL_GetProperty(lua_State *L);
int Hook_TL_SetProperty(lua_State *L);
int Hook_TL_SetTypeCombatProperty(lua_State *L);
int Hook_TL_SetWordCombatProperty(lua_State *L);
int Hook_TL_Save(lua_State *L);
int Hook_TL_Load(lua_State *L);
int Hook_TL_Exists(lua_State *L);
int Hook_TL_GetSaveFileList(lua_State *L);
