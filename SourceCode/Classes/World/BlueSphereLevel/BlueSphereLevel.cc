//--Base
#include "BlueSphereLevel.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioPackage.h"
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

///========================================== System ==============================================
BlueSphereLevel::BlueSphereLevel()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_BLUESPHERELEVEL;

    ///--[IRenderable]
    //--System

    ///--[RootLevel]
    //--System

    ///--[BlueSphereLevel]
    //--System
    mWaitTimer = (int)(5.841f * SECONDSTOTICKS);
    mBlueSpheresRemaining = 0;
    mSuspendedLevel = NULL;

    //--Level Select
    mIsLevelSelectMode = true;
    mLastLevelSelected = 0;
    mLevelsTotal = 0;
    mLevelSelectPage = 0;
    mLevelSelectCursor = 0;
    mLevelResults = NULL;

    //--Map
    mXSize = 0;
    mYSize = 0;
    mMapData = NULL;

    //--Object Grid
    memset(mObjectGrid, 0, sizeof(int) * BSL_OBJ_X * BSL_OBJ_Y);

    //--Player Data
    mPlayerX = 0.0f;
    mPlayerY = 0.0f;
    mPlayerFacing = DIR_UP;
    mPlayerMoveProgress = 0.0f;

    //--Moving
    mTotalPlayerMoveTicks = 0;
    mColorCountOffset = 0;
    mPlayerMoveTicks = 0;
    mPlayerMoveTicksTotal = 0;
    mPlayerMoveTicksMax = 100;
    mPlayerSpeed = 6;
    mPlayerMoveProgress = 0.0f;

    //--Bumper Backwards Moving
    mBumperTurnCooldown = 0;
    mBumperBackingTicksLeft = 0;
    mBumperBackingTicksMax = BSL_BUMPER_DISTANCE;
    mBumperBackingProgress = 0;

    //--Turning
    mTurnDirection = BSL_TURN_NONE;
    mTurnTimer = 0;
    mTurnProgress = 0.0f;
    mTurnBase = 0.0f;

    //--Jumping
    mJumpDistanceLeft = 0;
    mJumpDistanceMax = BSL_JUMP_DISTANCE;
    mJumpCooldown = 0;

    //--Camera Position
    mLocalCamera = new StarCamera3D();
    mLocalCamera->SetPosition(-20.0f, 0.0f, -180.0f);
    mLocalCamera->SetRotation( 45.0f, 0.0f,   90.0f);

    //--Victory
    mIsDefeat = false;
    mVictoryMode = BSL_VICTORY_NONE;
    mVictoryTimer = 0;

    //--Images.
    memset(&Images, 0, sizeof(Images));

    ///--[Construction]
    //--Images
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Images.Data.rSystemFont = rDataLibrary->GetFont("Blue Sphere Main");
    Images.Data.rPlayerForward       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Christine_Golem/South|0");
    Images.Data.rPlayerWalkImages[0] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Christine_Golem/North|0");
    Images.Data.rPlayerWalkImages[1] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Christine_Golem/North|1");
    Images.Data.rPlayerWalkImages[2] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Christine_Golem/North|2");
    Images.Data.rPlayerWalkImages[3] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Christine_Golem/North|3");
    Images.Data.rPlayerJumpImage     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Christine_Golem/North|1");
    Images.Data.rPlayerShadowImage   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Sphere/Shadow");
    Images.Data.rBlueSphere          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Sphere/Blue");
    Images.Data.rRedSphere           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Sphere/Red");
    Images.Data.rYellowSphere        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Sphere/Yellow");
    Images.Data.rBumperSphere        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Sphere/Bumper");
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
}
BlueSphereLevel::~BlueSphereLevel()
{
    delete mSuspendedLevel;
    free(mLevelResults);
    for(int x = 0; x < mXSize; x ++) free(mMapData[x]);
    free(mMapData);
    delete mLocalCamera;
}

///===================================== Property Queries =========================================
bool BlueSphereLevel::IsReady()
{
    //--All checks passed.
    if(!Images.mIsReady) return false;
    return true;
}

///======================================= Manipulators ===========================================
void BlueSphereLevel::SizeMap(int pXSize, int pYSize)
{
    //--Deallocate.
    for(int x = 0; x < mXSize; x ++) free(mMapData[x]);
    free(mMapData);

    //--Clear.
    mXSize = 0;
    mYSize = 0;
    mMapData = NULL;

    //--Check.
    if(pXSize < 1 || pYSize < 1) return;

    //--Allocate.
    mXSize = pXSize;
    mYSize = pYSize;
    mMapData = (int **)starmemoryalloc(sizeof(int *) * mXSize);
    for(int x = 0; x < mXSize; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mMapData[x] = (int *)starmemoryalloc(sizeof(int) * mYSize);
        memset(mMapData[x], BSL_EMPTY, sizeof(int) * mYSize);
    }
}
void BlueSphereLevel::SetMapElement(int pX, int pY, int pElement)
{
    //--Range check.
    while(pX < 0) pX += mXSize;
    while(pY < 0) pY += mYSize;
    while(pX >= mXSize) pX -= mXSize;
    while(pY >= mYSize) pY -= mYSize;

    //--If currently a blue sphere, decrement the counter.
    if(mMapData[pX][pY] == BSL_BLUE_SPHERE) mBlueSpheresRemaining --;

    //--Set.
    mMapData[pX][pY] = pElement;

    //--If it's a blue sphere, increment the counter.
    if(mMapData[pX][pY] == BSL_BLUE_SPHERE) mBlueSpheresRemaining ++;
}
void BlueSphereLevel::SetPlayerPosition(float pX, float pY)
{
    mPlayerX = pX;
    mPlayerY = pY;
}
void BlueSphereLevel::SetPlayerFacing(int pDirection)
{
    mPlayerFacing = pDirection;
    if(mPlayerFacing == DIR_UP) mTurnBase = 0.0f;
    if(mPlayerFacing == DIR_LEFT) mTurnBase = -1.0f;
    if(mPlayerFacing == DIR_RIGHT) mTurnBase = 1.0f;
    if(mPlayerFacing == DIR_DOWN) mTurnBase = 2.0f;
}
void BlueSphereLevel::PlayerEnterPosition(int pX, int pY)
{
    //--Called when the player finishes a move. Changes spheres or causes a game over.
    if(pX < 0 || pX >= mXSize) return;
    if(pY < 0 || pY >= mYSize) return;

    //--If this is a red-sphere:
    if(mMapData[pX][pY] == BSL_RED_SPHERE || mMapData[pX][pY] == BSL_RED_SPHERE_WAS_BLUE)
    {
        mIsDefeat = true;
        mVictoryMode = BSL_VICTORY_BURST;
        mVictoryTimer = 0;
        AudioManager::Fetch()->PlaySound("Sphere|Win");
        AudioManager::Fetch()->FadeMusic(30);
    }
    //--Blue sphere. Change to a red sphere.
    else if(mMapData[pX][pY] == BSL_BLUE_SPHERE)
    {
        //--Change.
        mMapData[pX][pY] = BSL_RED_SPHERE_WAS_BLUE;

        //--Decrement blue sphere counter.
        mBlueSpheresRemaining --;

        //--Check for any looped spheres. If a sphere is fully encased in reds, it disappears
        //  and adds bonus points.
        RemoveLoopedSpheres(pX, pY);

        //--If the blue sphere counter hits zero, we win!
        if(mBlueSpheresRemaining < 1)
        {
            mVictoryMode = BSL_VICTORY_BURST;
            mVictoryTimer = 0;
            AudioManager::Fetch()->PlaySound("Sphere|Win");
            AudioManager::Fetch()->FadeMusic(30);
        }
        //--Otherwise, play the collection sound.
        else
        {
            AudioManager::Fetch()->PlaySound("Sphere|Collect");
        }
    }
    //--Yellow sphere. Launches the player. Similar to a jump but goes 5 squares.
    else if(mMapData[pX][pY] == BSL_LAUNCHER)
    {
        mJumpDistanceLeft = BSL_LAUNCH_DISTANCE;
        mJumpDistanceMax = BSL_LAUNCH_DISTANCE;
        AudioManager::Fetch()->PlaySound("Sphere|Jump");
    }
    //--Bumper sphere. Forces the player to walk backwards for a short distance. Jumping while this is occurring allows the player
    //  to move backwards further than normal.
    else if(mMapData[pX][pY] == BSL_BUMPER)
    {
        //--If we were already under the effects of a bumper, we don't provide the usual bumper effects.
        //  Instead we cancel them.
        if(mBumperBackingTicksLeft > 0)
        {
            mBumperBackingTicksLeft = 0;
        }
        //--Apply bumper effects.
        else
        {
            mBumperBackingTicksLeft = BSL_BUMPER_DISTANCE;
            mBumperBackingTicksMax = BSL_BUMPER_DISTANCE;
        }

        //--Standard flags.
        mBumperBackingProgress = 0;
        mBumperTurnCooldown = 5;
        AudioManager::Fetch()->PlaySound("Sphere|Bumper");
    }
}
void BlueSphereLevel::ProvideSuspendedLevel(RootLevel *pLevel)
{
    delete mSuspendedLevel;
    mSuspendedLevel = pLevel;
}

///======================================= Core Methods ===========================================
bool BlueSphereLevel::IsPointRed(int pX, int pY)
{
    ClampPosition(pX, pY);
    return (mMapData[pX][pY] == BSL_RED_SPHERE || mMapData[pX][pY] == BSL_RED_SPHERE_WAS_BLUE);
}
bool BlueSphereLevel::IsValidCenter(int pX, int pY)
{
    ClampPosition(pX, pY);
    return (mMapData[pX][pY] == BSL_BLUE_SPHERE);
}
bool BlueSphereLevel::IsValidRemove(int pX, int pY)
{
    ClampPosition(pX, pY);
    return (mMapData[pX][pY] == BSL_RED_SPHERE_WAS_BLUE || mMapData[pX][pY] == BSL_BLUE_SPHERE);
}
void BlueSphereLevel::RemoveLoopedSpheres(int pStartX, int pStartY)
{
    ///--[Documentation and Setup]
    //--Starting at the given location, checks if this sphere and nearby reds surround any blues.
    //  If any blues are surrounded, they disappear, add bonus points, and also the reds that were
    //  former blues are removed and add points.
    //--Note that edge checking is not done. The maps wrap at the edges! Make sure to consider this
    //  when making maps.

    //--List of valid squares.
    StarLinkedList *tSquareList = new StarLinkedList(true);

    ///--[Recursive Routing]
    //--Run it in each direction. It should build a list of all valid rectanges.
    TwoDimensionReal tExtentDim;
    tExtentDim.mLft = pStartX;
    tExtentDim.mTop = pStartY;
    tExtentDim.mRgt = pStartX;
    tExtentDim.mBot = pStartY;
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_UP,    BSL_TURN_RIGHT, 4, tSquareList, tExtentDim);
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_UP,    BSL_TURN_LEFT,  4, tSquareList, tExtentDim);
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_RIGHT, BSL_TURN_RIGHT, 4, tSquareList, tExtentDim);
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_RIGHT, BSL_TURN_LEFT,  4, tSquareList, tExtentDim);
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_DOWN,  BSL_TURN_RIGHT, 4, tSquareList, tExtentDim);
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_DOWN,  BSL_TURN_LEFT,  4, tSquareList, tExtentDim);
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_LEFT,  BSL_TURN_RIGHT, 4, tSquareList, tExtentDim);
    RunRedChecker(pStartX, pStartY, pStartX, pStartY, DIR_LEFT,  BSL_TURN_LEFT,  4, tSquareList, tExtentDim);

    ///--[No Squares]
    //--Save ourselves some time and stop if there were no squares at all.
    if(tSquareList->GetListSize() < 1)
    {
        delete tSquareList;
        return;
    }

    ///--[Clone Map Data]
    //--Because it's possible to have multiple squares be valid in a single pass, we copy the map data and
    //  edit that data. This way, all squares made in one pass are cleared.
    int **tMapClone = (int **)starmemoryalloc(sizeof(int *) * mXSize);
    for(int x = 0; x < mXSize; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        tMapClone[x] = (int *)starmemoryalloc(sizeof(int) * mYSize);
        memcpy(tMapClone[x], mMapData[x], sizeof(int) * mYSize);
    }

    ///--[Check All Squares]
    //--Check the squares. They must contain blue or red spheres, no empty space.
    bool tAnyValid = false;
    TwoDimensionReal *rDim = (TwoDimensionReal *)tSquareList->PushIterator();
    while(rDim)
    {
        //--Check for validity.
        bool tAllValid = true;
        for(int x = rDim->mLft + 1; x < rDim->mRgt; x ++)
        {
            for(int y = rDim->mTop + 1; y < rDim->mBot; y ++)
            {
                if(!IsValidCenter(x, y))
                {
                    tAllValid = false;
                    x = rDim->mRgt + 1;
                    break;
                }
            }
        }

        //--If valid, remove that part of the sphere. This is done on the copy.
        if(tAllValid)
        {
            tAnyValid = true;
            for(int x = rDim->mLft; x < rDim->mRgt + 1; x ++)
            {
                for(int y = rDim->mTop; y < rDim->mBot + 1; y ++)
                {
                    if(IsValidRemove(x, y))
                    {
                        //--Wrap the edges.
                        int tActualX = x;
                        int tActualY = y;
                        while(tActualX < 0) tActualX += mXSize;
                        while(tActualY < 0) tActualY += mYSize;
                        while(tActualX >= mXSize) tActualX -= mXSize;
                        while(tActualY >= mYSize) tActualY -= mYSize;

                        //--Set.
                        tMapClone[tActualX][tActualY] = BSL_EMPTY;
                    }
                }
            }
        }

        //--Next.
        rDim = (TwoDimensionReal *)tSquareList->AutoIterate();
    }

    //--SFX.
    if(tAnyValid) AudioManager::Fetch()->PlaySound("Sphere|Group");

    //--Copy the clone data back into the primary array. Clean while we're at it.
    for(int x = 0; x < mXSize; x ++)
    {
        memcpy(mMapData[x], tMapClone[x], sizeof(int) * mYSize);
        free(tMapClone[x]);
    }
    free(tMapClone);

    //--Clean the list.
    delete tSquareList;

    //--Scan back across the map data. We need to find out how many blue spheres are left.
    mBlueSpheresRemaining = 0;
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            if(mMapData[x][y] == BSL_BLUE_SPHERE) mBlueSpheresRemaining ++;
        }
    }
}

///=================================== Private Core Methods =======================================
void BlueSphereLevel::ClampPosition(int &sX, int &sY)
{
    while(sX < 0) sX += mXSize;
    while(sY < 0) sY += mYSize;
    while(sX >= mXSize) sX -= mXSize;
    while(sY >= mYSize) sY -= mYSize;
}
void BlueSphereLevel::ClampPositionF(float &sX, float &sY)
{
    while(sX < 0.0f) sX = sX + mXSize;
    while(sY < 0.0f) sY = sY + mYSize;
    while(sX >= mXSize) sX = sX - mXSize;
    while(sY >= mYSize) sY = sY - mYSize;
}
void BlueSphereLevel::RunRedChecker(int pOriginalX, int pOriginalY, int pX, int pY, int pDirection, int pTurnFlag, int pDepth, StarLinkedList *pResultList, TwoDimensionReal pExtent)
{
    //--Recursive routine which runs along lines of red spheres. Launches recursive turns at each points,
    //  trying to loop back around to touch the original point.
    int tCurrentX = pX;
    int tCurrentY = pY;

    //--Run counter.
    int tRunsLeft = mXSize;
    if(pDirection == DIR_UP || pDirection == DIR_DOWN) tRunsLeft = mYSize;

    //--Run until we hit a non-red sphere or have looped back to the original position.
    while(tRunsLeft > 0)
    {
        //--Decrement movement count.
        tRunsLeft --;

        //--Move by direction.
        if(pDirection == DIR_UP) tCurrentY --;
        else if(pDirection == DIR_RIGHT) tCurrentX ++;
        else if(pDirection == DIR_DOWN) tCurrentY ++;
        else if(pDirection == DIR_LEFT) tCurrentX --;

        //--If the requested position is not a red sphere, stop.
        if(!IsPointRed(tCurrentX, tCurrentY))
        {
            return;
        }

        //--Store the change in extent. This doesn't happen at depth zero since that's closing the circle.
        if(pDepth > 0)
        {
            if(pDirection == DIR_UP) pExtent.mTop = tCurrentY;
            else if(pDirection == DIR_RIGHT) pExtent.mRgt = tCurrentX;
            else if(pDirection == DIR_DOWN) pExtent.mBot = tCurrentY;
            else if(pDirection == DIR_LEFT) pExtent.mLft = tCurrentX;
        }

        //--If the depth is at 1, this means we cannot pass the original dimensions. Otherwise we'd make a rectangle that contains
        //  the starting point, which is illegal. We also check if we hit the original point here since that can happen at depth 1.
        if(pDepth == 1)
        {
            //--Stop checker.
            if(pDirection == DIR_UP    && tCurrentY < pOriginalY) return;
            if(pDirection == DIR_RIGHT && tCurrentX > pOriginalX) return;
            if(pDirection == DIR_DOWN  && tCurrentY > pOriginalY) return;
            if(pDirection == DIR_LEFT  && tCurrentX < pOriginalX) return;

            //--Clamp.
            int tRealX = tCurrentX;
            int tRealY = tCurrentY;
            ClampPosition(tRealX, tRealY);

            //--Found it. Copy and return.
            if(tRealX == pOriginalX && tRealY == pOriginalY)
            {
                //--The extents must be at least 2 units in both directions.
                if(pExtent.mRgt - pExtent.mLft >= 2.0f && pExtent.mBot - pExtent.mTop >= 2.0f)
                {
                    SetMemoryData(__FILE__, __LINE__);
                    TwoDimensionReal *nDimensions = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
                    memcpy(nDimensions, &pExtent, sizeof(TwoDimensionReal));
                    pResultList->AddElementAsTail("X", nDimensions, &FreeThis);
                }
                return;
            }
        }
        //--If the depth is at 0, we're looking for the original position. If we find it, we found a valid rectangle.
        else if(pDepth == 0)
        {
            //--Clamp.
            int tRealX = tCurrentX;
            int tRealY = tCurrentY;
            ClampPosition(tRealX, tRealY);

            //--Found it. Copy and return.
            if(tRealX == pOriginalX && tRealY == pOriginalY)
            {
                //--The extents must be at least 2 units in both directions.
                if(pExtent.mRgt - pExtent.mLft >= 2.0f && pExtent.mBot - pExtent.mTop >= 2.0f)
                {
                    SetMemoryData(__FILE__, __LINE__);
                    TwoDimensionReal *nDimensions = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
                    memcpy(nDimensions, &pExtent, sizeof(TwoDimensionReal));
                    pResultList->AddElementAsTail("X", nDimensions, &FreeThis);
                }
                return;
            }
        }

        //--Depth 1 stopper.
        bool tBlockSpawn = true;

        //--For depths above 1, spawn additional threads as normal.
        if(pDepth > 1)
        {
            tBlockSpawn = false;
        }
        //--If the depth is exactly 1, we can only spawn a thread if we're at the same horizontal/vertical as the original.
        else if(pDepth == 1)
        {
            //--Vertical direction.
            if(pDirection == DIR_UP || pDirection == DIR_DOWN)
            {
                tBlockSpawn = !(tCurrentY == pOriginalY);
            }
            //--Horizontal direction.
            else if(pDirection == DIR_RIGHT || pDirection == DIR_LEFT)
            {
                tBlockSpawn = !(tCurrentX == pOriginalX);
            }
        }

        //--Check the block flag.
        if(!tBlockSpawn)
        {
            //--This is a red sphere. Spawn another recursive turn thread.
            if(pTurnFlag == BSL_TURN_LEFT)
            {
                if(pDirection == DIR_UP)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_LEFT, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
                else if(pDirection == DIR_RIGHT)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_UP, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
                else if(pDirection == DIR_DOWN)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_RIGHT, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
                else if(pDirection == DIR_LEFT)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_DOWN, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
            }
            //--Right.
            else if(pTurnFlag == BSL_TURN_RIGHT)
            {
                if(pDirection == DIR_UP)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_RIGHT, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
                else if(pDirection == DIR_RIGHT)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_DOWN, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
                else if(pDirection == DIR_DOWN)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_LEFT, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
                else if(pDirection == DIR_LEFT)
                {
                    RunRedChecker(pOriginalX, pOriginalY, tCurrentX, tCurrentY, DIR_UP, pTurnFlag, pDepth - 1, pResultList, pExtent);
                }
            }
        }
    }
}

///========================================== Update ==============================================
void BlueSphereLevel::Update()
{
    ///--[Fast-Access Pointers]
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Level Select Mode]
    if(mIsLevelSelectMode)
    {
        UpdateLevelSelect();
        return;
    }

    //--Pass controls to the 3D camera. If the camera is unlocked, ignore the rest of the controls.
    mLocalCamera->Update();
    //if(rControlManager->IsDown("CameraUnlock"))
    //{
    //    return;
    //}

    ///--[Delayed Start]
    if(mWaitTimer > 0)
    {
        mWaitTimer --;
    }

    ///--[Victory Handler]
    //--Starting victory mode. All the items on the stage fly up except the player.
    if(mVictoryMode == BSL_VICTORY_BURST)
    {
        //--Timer.
        mVictoryTimer ++;

        //--Ending case.
        if(mVictoryTimer >= 30)
        {
            mVictoryMode = BSL_VICTORY_SHOWRESULTS;
            mVictoryTimer = 0;
        }
        return;
    }
    //--Show the results of the stage.
    else if(mVictoryMode == BSL_VICTORY_SHOWRESULTS)
    {
        //--Timer goes to 60.
        if(mVictoryTimer < 60)
        {
            mVictoryTimer ++;
        }

        //--Player can press a key to hasten/end this mode.
        if(rControlManager->IsAnyKeyPressed())
        {
            //--Hasten.
            if(mVictoryTimer < 60)
            {
                mVictoryTimer = 60;
            }
            //--End the mode.
            else
            {
                mVictoryMode = BSL_VICTORY_TRANSITIONOUT;
                mVictoryTimer = 0;
            }
        }
        return;
    }
    //--Fade to black.
    else if(mVictoryMode == BSL_VICTORY_TRANSITIONOUT)
    {
        //--Timer.
        mVictoryTimer ++;

        //--Activate level select again. Pass -1 to not free the old results.
        ActivateLevelSelect(-1);
        return;
    }

    ///--[Turning Handler]
    //--Turning left or right.
    if(mTurnDirection != BSL_TURN_NONE)
    {
        //--Timers.
        mTurnTimer ++;
        if(mTurnDirection == BSL_TURN_RIGHT)
        {
            mTurnProgress = EasingFunction::QuadraticOut(mTurnTimer, (float)BSL_TURN_TICKS) * -1.0f;
        }
        else
        {
            mTurnProgress = EasingFunction::QuadraticOut(mTurnTimer, (float)BSL_TURN_TICKS) * 1.0f;
        }

        //--Ending case.
        if(mTurnTimer >= BSL_TURN_TICKS)
        {
            //--Flag reset.
            mTurnTimer = 0;
            mTurnProgress = 0.0f;

            //--Facing direction up.
            if(mPlayerFacing == DIR_UP)
            {
                if(mTurnDirection == BSL_TURN_LEFT)
                {
                    mPlayerFacing = DIR_LEFT;
                    mTurnBase = mTurnBase - 1.0f;
                }
                else
                {
                    mPlayerFacing = DIR_RIGHT;
                    mTurnBase = mTurnBase + 1.0f;
                }
            }
            //--Facing right.
            else if(mPlayerFacing == DIR_RIGHT)
            {
                if(mTurnDirection == BSL_TURN_LEFT)
                {
                    mPlayerFacing = DIR_UP;
                    mTurnBase = mTurnBase - 1.0f;
                }
                else
                {
                    mPlayerFacing = DIR_DOWN;
                    mTurnBase = mTurnBase + 1.0f;
                }
            }
            //--Facing down.
            else if(mPlayerFacing == DIR_DOWN)
            {
                if(mTurnDirection == BSL_TURN_LEFT)
                {
                    mPlayerFacing = DIR_RIGHT;
                    mTurnBase = mTurnBase - 1.0f;
                }
                else
                {
                    mPlayerFacing = DIR_LEFT;
                    mTurnBase = mTurnBase + 1.0f;
                }
            }
            //--Facing left.
            else if(mPlayerFacing == DIR_LEFT)
            {
                if(mTurnDirection == BSL_TURN_LEFT)
                {
                    mPlayerFacing = DIR_DOWN;
                    mTurnBase = mTurnBase - 1.0f;
                }
                else
                {
                    mPlayerFacing = DIR_UP;
                    mTurnBase = mTurnBase + 1.0f;
                }
            }

            //--Clear.
            mTurnDirection = BSL_TURN_NONE;
        }
    }
    //--Player is not turning. Handle the movement update.
    else if(mWaitTimer < 1)
    {
        //--Player adds one to this every movement tick. It slowly increases speed.
        mTotalPlayerMoveTicks ++;
        if(mTotalPlayerMoveTicks >= 30)
        {
            //--Reset.
            mTotalPlayerMoveTicks = 0;

            //--Music changes tempo.
            AudioPackage *rMusicPack = AudioManager::Fetch()->GetMusicPack("BlueSphere");
            float tTempoIncrease = ((mPlayerSpeed / 6.0f) - 1.0f) * 50.0f;
            if(rMusicPack) rMusicPack->SetTempo(tTempoIncrease);

            //--Speed the player up.
            mPlayerSpeed = mPlayerSpeed + 0.02f;

            //--Clamp.
            if(mPlayerSpeed > 13.0f)
            {
                mPlayerSpeed = 13.0f;
            }
        }

        //--Decrement the bumper turn cooldown.
        if(mBumperTurnCooldown > 0) mBumperTurnCooldown --;

        //--Launch spheres temporarily increase the player's speed.
        float tUseSpeed = mPlayerSpeed;
        if(mJumpDistanceMax == BSL_LAUNCH_DISTANCE && mJumpDistanceLeft > 0)
        {
            tUseSpeed = 6.0f * 1.5f; //Use the base speed to prevent errors.
        }

        //--If the player is under the effects of a bumper, they move backwards.
        if(mBumperBackingTicksLeft > 0)
        {
            //--Compute the intensity of the backwards movement speed. It uses an easing function.
            if(mJumpDistanceLeft < 1) mBumperBackingProgress ++;
            float tProgress = 1.0f - (mBumperBackingProgress / BSL_BUMPER_BACKING_TIME);

            //--Modify the speed. Making it negative means its backwards movement.
            tUseSpeed = tUseSpeed * -1.0f * tProgress;
        }

        //--Movement progress.
        float tMoveTicksBeforeIncrement = mPlayerMoveTicks;
        mPlayerMoveTicksTotal ++;
        mPlayerMoveTicks = mPlayerMoveTicks + tUseSpeed;

        //--Jump handling.
        if(mJumpCooldown > 0) mJumpCooldown --;
        if(mJumpDistanceLeft > 0)
        {
            //--Compute how much the jump is reduced by. It cannot be lower than the player's normal speed.
            //  During boosts it will decrement faster.
            float tDecrementSpeed = fabsf(tUseSpeed);
            if(tDecrementSpeed < mPlayerSpeed) tDecrementSpeed = mPlayerSpeed;

            //--Decrement.
            mJumpDistanceLeft -= tDecrementSpeed;

            //--Clamp. Now we hit the ground.
            if(mJumpDistanceLeft < 1)
            {
                mJumpCooldown = 3;
                mJumpDistanceLeft = 0;
            }
        }
        //--Ground handling.
        else
        {
            //--If moving backwards, decrement the amount by the amount moved. This will be negative
            //  so we use the absolute.
            if(mBumperBackingTicksLeft > 0)
            {
                mBumperBackingTicksLeft -= fabsf(tUseSpeed);
                if(mBumperBackingTicksLeft < 0) mBumperBackingTicksLeft = 0;
            }
        }

        //--[Forwards Movement]
        if(tUseSpeed > 0.0f)
        {
            //--Player crosses the zero line while moving forwards. This allows for a turn and counts as entering the current position.
            if(tMoveTicksBeforeIncrement < 0 && mPlayerMoveTicks >= 0.0f)
            {
                //--Check left turn controls.
                if(rControlManager->IsDown("Left") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_LEFT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }
                //--Check right turn controls.
                else if(rControlManager->IsDown("Right") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_RIGHT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }

                //--Remove spheres as necessary. It's also possible to back into a sphere to claim it.
                if(mJumpDistanceLeft < 1) PlayerEnterPosition(mPlayerX, mPlayerY);
            }

            //--Player completes a full move.
            while(mPlayerMoveTicks >= 100.0f)
            {
                //--Check left turn controls.
                if(rControlManager->IsDown("Left") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_LEFT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }
                //--Check right turn controls.
                else if(rControlManager->IsDown("Right") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_RIGHT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }

                //--Reset.
                mPlayerMoveTicks = mPlayerMoveTicks - 100.0f;

                //--Facing direction up.
                if(mPlayerFacing == DIR_UP)
                {
                    mPlayerY = mPlayerY - 1.0f;
                }
                //--Facing right.
                else if(mPlayerFacing == DIR_RIGHT)
                {
                    mPlayerX = mPlayerX + 1.0f;
                }
                //--Facing down.
                else if(mPlayerFacing == DIR_DOWN)
                {
                    mPlayerY = mPlayerY + 1.0f;
                }
                //--Facing left.
                else if(mPlayerFacing == DIR_LEFT)
                {
                    mPlayerX = mPlayerX - 1.0f;
                }
                ClampPositionF(mPlayerX, mPlayerY);

                //--Remove spheres as necessary.
                if(mJumpDistanceLeft < 1) PlayerEnterPosition(mPlayerX, mPlayerY);

                //--If the player hit a bumper, cancel their turn and move them to the previous space.
                if(mBumperTurnCooldown > 0)
                {
                    //--Block turning.
                    mTurnDirection = BSL_TURN_NONE;

                    //--Set to 99% progress.
                    mPlayerMoveTicks = 99.0f;

                    //--Undo the move.
                    if(mPlayerFacing == DIR_UP)
                    {
                        mPlayerY = mPlayerY + 1.0f;
                    }
                    //--Facing right.
                    else if(mPlayerFacing == DIR_RIGHT)
                    {
                        mPlayerX = mPlayerX - 1.0f;
                    }
                    //--Facing down.
                    else if(mPlayerFacing == DIR_DOWN)
                    {
                        mPlayerY = mPlayerY - 1.0f;
                    }
                    //--Facing left.
                    else if(mPlayerFacing == DIR_LEFT)
                    {
                        mPlayerX = mPlayerX + 1.0f;
                    }
                    ClampPositionF(mPlayerX, mPlayerY);
                }
                //--Otherwise, change the grid layout.
                else
                {
                    mColorCountOffset = (mColorCountOffset + 1) % 2;
                }
            }
        }
        //--[Backwards Movement]
        else
        {
            //--Player crosses the zero line while moving backwards. This allows for a turn and counts as entering the current position.
            if(tMoveTicksBeforeIncrement > 0 && mPlayerMoveTicks <= 0.0f)
            {
                //--Check left turn controls.
                if(rControlManager->IsDown("Left") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_LEFT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }
                //--Check right turn controls.
                else if(rControlManager->IsDown("Right") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_RIGHT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }

                //--Remove spheres as necessary. It's also possible to back into a sphere to claim it.
                if(mJumpDistanceLeft < 1) PlayerEnterPosition(mPlayerX, mPlayerY);

                //--If the player hit another bumper, cancel their turn and move them to the previous space.
                if(mBumperTurnCooldown > 0)
                {
                    //--Block turning.
                    mTurnDirection = BSL_TURN_NONE;

                    //--Set to 1% progress.
                    mPlayerMoveTicks = 1.0f;
                }
            }

            //--Player completes a full move backwards. Only used when actually moving backwards, obviously.
            while(mPlayerMoveTicks <= -100.0f && mBumperBackingTicksLeft > 0)
            {
                //--Check left turn controls.
                if(rControlManager->IsDown("Left") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_LEFT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }
                //--Check right turn controls.
                else if(rControlManager->IsDown("Right") && mJumpDistanceLeft < 1)
                {
                    mTurnDirection = BSL_TURN_RIGHT;
                    mTurnProgress = 0.0f;
                    mTurnTimer = 0;
                }

                //--Reset.
                mPlayerMoveTicks = mPlayerMoveTicks + 100.0f;

                //--Facing direction up.
                if(mPlayerFacing == DIR_UP)
                {
                    mPlayerY = mPlayerY + 1.0f;
                }
                //--Facing right.
                else if(mPlayerFacing == DIR_RIGHT)
                {
                    mPlayerX = mPlayerX - 1.0f;
                }
                //--Facing down.
                else if(mPlayerFacing == DIR_DOWN)
                {
                    mPlayerY = mPlayerY - 1.0f;
                }
                //--Facing left.
                else if(mPlayerFacing == DIR_LEFT)
                {
                    mPlayerX = mPlayerX + 1.0f;
                }
                ClampPositionF(mPlayerX, mPlayerY);

                //--Remove spheres as necessary.
                if(mJumpDistanceLeft < 1) PlayerEnterPosition(mPlayerX, mPlayerY);

                //--If the player hit a bumper, cancel their turn.
                if(mBumperTurnCooldown > 0)
                {
                    mTurnDirection = BSL_TURN_NONE;
                }

                //--Change the color grid offset.
                mColorCountOffset = (mColorCountOffset + 1) % 2;
            }
        }

        //--Player is not turning. Check for jumps.
        if((rControlManager->IsFirstPress("Activate") || rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Jump")) && mJumpCooldown < 1 && mJumpDistanceLeft < 1)
        {
            mJumpDistanceLeft = BSL_JUMP_DISTANCE;
            mJumpDistanceMax = BSL_JUMP_DISTANCE;
            AudioManager::Fetch()->PlaySound("Sphere|Jump");
        }

        //--Player's movement progress is based on the timers.
        mPlayerMoveProgress = (float)mPlayerMoveTicks / (float)mPlayerMoveTicksMax;
    }

    ///--[Object Grid]
    //--I don't know either. These offsets were calibrated and I don't know the mathematical basis for them.
    int tOffsetRtX = (mXSize - 14) * -2;
    int tOffsetRtY = (mYSize - 15) * -3;
    int tOffsetUpY = (mYSize - 14) * 6;
    int tOffsetDnY = (mYSize - 14) * 1;
    int tOffsetLfY = (mYSize - 14) * -1;

    //--Recompute what's in the object grid.
    if(mPlayerFacing == DIR_UP)
    {
        for(int x = 0; x < BSL_OBJ_X; x ++)
        {
            for(int y = 0; y < BSL_OBJ_Y; y ++)
            {
                int tUseX = (x + (int)mPlayerX) - 14;
                int tUseY = (y + (int)mPlayerY) + 28 + 9 + tOffsetUpY;
                ClampPosition(tUseX, tUseY);
                mObjectGrid[x][y] = mMapData[tUseX][tUseY];
            }
        }
    }
    else if(mPlayerFacing == DIR_RIGHT)
    {
        for(int x = 0; x < BSL_OBJ_X; x ++)
        {
            for(int y = 0; y < BSL_OBJ_Y; y ++)
            {
                int tUseX = ((int)mPlayerX - x) - 17 + 2 + tOffsetRtX;
                int tUseY = ((int)mPlayerY - y) -  4 + tOffsetRtY;
                ClampPosition(tUseX, tUseY);
                mObjectGrid[x][y] = mMapData[tUseX][tUseY];
            }
        }
    }
    else if(mPlayerFacing == DIR_DOWN)
    {
        for(int x = 0; x < BSL_OBJ_X; x ++)
        {
            for(int y = 0; y < BSL_OBJ_Y; y ++)
            {
                int tUseX = ((int)mPlayerX + x) - 14;
                int tUseY = ((int)mPlayerY + y) - 21 - 1 + tOffsetDnY;
                ClampPosition(tUseX, tUseY);
                mObjectGrid[x][y] = mMapData[tUseX][tUseY];
            }
        }
    }
    else if(mPlayerFacing == DIR_LEFT)
    {
        for(int x = 0; x < BSL_OBJ_X; x ++)
        {
            for(int y = 0; y < BSL_OBJ_Y; y ++)
            {
                int tUseX = ((int)mPlayerX - x) + 13;
                int tUseY = ((int)mPlayerY - y) + 16 + 1 + tOffsetLfY;
                ClampPosition(tUseX, tUseY);
                mObjectGrid[x][y] = mMapData[tUseX][tUseY];
            }
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void BlueSphereLevel::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void BlueSphereLevel::Render()
{
    ///--[Setup and Documentation]
    //--Renders the Blue Sphere Level, which is a quasi-3D level projected based on the player's
    //  facing direction. The player is always depicted as moving forwards, and can turn at intersections.
    if(!IsReady()) return;
    MapManager::xHasRenderedMenus = true;

    ///--[Level Select Mode]
    if(mIsLevelSelectMode)
    {
        RenderLevelSelect();
        return;
    }

    ///--[Constants]
    //--Overall scale.
    float cScale = 0.05f;

    //--Spherical Radius.
    float cRadius = 150.0f;

    //--Square sizes.
    float cInc = 5.0f;

    ///--[GL Setup]
    //--Disable texturing and get ready for pseudo 3d.
    DisplayManager::CleanOrtho();
    DisplayManager::StdFrustum();

    ///--[Camera]
    //--Get values.
    float tX, tY, tZ;
    mLocalCamera->Get3DPosition(tX, tY, tZ);
    float tPitch, tYaw, tRoll;
    mLocalCamera->Get3DRotation(tPitch, tYaw, tRoll);

    //--Size and position.
    glScalef(cScale, cScale, cScale);
    glRotatef(tPitch, 1, 0, 0);
    glRotatef(tYaw, 0, 1, 0);
    glRotatef(tRoll, 0, 0, 1);
    glTranslatef(tX, tY, tZ);

    //--Rotate based on player X.
    float cWorldRotation = (mTurnBase + mTurnProgress) * 90.0f;
    glRotatef(cWorldRotation, 0.0f, 0.0f, 1.0f);

    ///--[Rendering]
    //--Begin rendering.
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);

        //--Color counter.
        int tColorCountY = 0;
        int tColorCountX = 0;

        //--Angular Offsets.
        float cLatOff = cInc;
        float cLonOff = cInc;

        //--Move Progress Offsets.
        float cProgressOffsetX = 0.0f;
        float cProgressOffsetY = 0.0f;
        if(mPlayerFacing == DIR_UP)
        {
            cProgressOffsetX = mPlayerMoveProgress * cInc * -1.0f;
        }
        else if(mPlayerFacing == DIR_RIGHT)
        {
            cProgressOffsetY = mPlayerMoveProgress * cInc;
        }
        else if(mPlayerFacing == DIR_DOWN)
        {
            cProgressOffsetX = mPlayerMoveProgress * cInc;
        }
        else if(mPlayerFacing == DIR_LEFT)
        {
            cProgressOffsetY = mPlayerMoveProgress * cInc * -1.0f;
        }

        //--Forward/Backward axis.
        tColorCountY = 0;
        for(float y = cInc * 0.5f; y < 180.0f; y = y + cInc)
        {
            //--Left/Right axis.
            tColorCountX = 0;
            for(float x = cInc * 0.5f; x < 360.0f; x = x + cInc)
            {
                //--Usage variables.
                x = x + (cProgressOffsetX);
                y = y + (cProgressOffsetY);

                //--Compute the center point of this square.
                float cCentX = cosf(x * TORADIAN) * sinf(y * TORADIAN) * cRadius;
                float cCentY =                      cosf(y * TORADIAN) * cRadius;
                float cCentZ = sinf(x * TORADIAN) * sinf(y * TORADIAN) * cRadius;

                //--Compute the halfway point between this square and the one next to it on each side.
                float cCentXUL = cosf((x-cLatOff) * TORADIAN) * sinf((y-cLonOff) * TORADIAN) * cRadius;
                float cCentYUL =                                cosf((y-cLonOff) * TORADIAN) * cRadius;
                float cCentZUL = sinf((x-cLatOff) * TORADIAN) * sinf((y-cLonOff) * TORADIAN) * cRadius;

                float cCentXUR = cosf((x+cLatOff) * TORADIAN) * sinf((y-cLonOff) * TORADIAN) * cRadius;
                float cCentYUR =                                cosf((y-cLonOff) * TORADIAN) * cRadius;
                float cCentZUR = sinf((x+cLatOff) * TORADIAN) * sinf((y-cLonOff) * TORADIAN) * cRadius;

                float cCentXDL = cosf((x-cLatOff) * TORADIAN) * sinf((y+cLonOff) * TORADIAN) * cRadius;
                float cCentYDL =                                cosf((y+cLonOff) * TORADIAN) * cRadius;
                float cCentZDL = sinf((x-cLatOff) * TORADIAN) * sinf((y+cLonOff) * TORADIAN) * cRadius;

                float cCentXDR = cosf((x+cLatOff) * TORADIAN) * sinf((y+cLonOff) * TORADIAN) * cRadius;
                float cCentYDR =                                cosf((y+cLonOff) * TORADIAN) * cRadius;
                float cCentZDR = sinf((x+cLatOff) * TORADIAN) * sinf((y+cLonOff) * TORADIAN) * cRadius;

                //--Positions.
                float cXA = (cCentX + cCentXUL) / 2.0f;
                float cXB = (cCentX + cCentXUR) / 2.0f;
                float cXC = (cCentX + cCentXDR) / 2.0f;
                float cXD = (cCentX + cCentXDL) / 2.0f;

                float cYA = (cCentY + cCentYUL) / 2.0f;
                float cYB = (cCentY + cCentYUR) / 2.0f;
                float cYC = (cCentY + cCentYDR) / 2.0f;
                float cYD = (cCentY + cCentYDL) / 2.0f;

                float cZA = (cCentZ + cCentZUL) / 2.0f;
                float cZB = (cCentZ + cCentZUR) / 2.0f;
                float cZC = (cCentZ + cCentZDR) / 2.0f;
                float cZD = (cCentZ + cCentZDL) / 2.0f;

                // Formula: [ cos(u)*sin(v)*r, cos(v)*r, sin(u)*sin(v)*r ]

                //--Color.
                if((tColorCountX + tColorCountY + mColorCountOffset) % 2 == 0)
                {
                    glColor3f(0.376f, 0.126f, 1.0f);
                }
                else
                {
                    glColor3f(0.941f, 0.565f, 0.0f);
                }

                //--Render.
                glVertex3f(cXA, cYA, cZA);
                glVertex3f(cXB, cYB, cZB);
                glVertex3f(cXC, cYC, cZC);
                glVertex3f(cXD, cYD, cZD);

                //--Clean.
                x = x - (cProgressOffsetX);
                y = y - (cProgressOffsetY);

                //--Color counter.
                tColorCountX ++;
            }

            //--Color counter.
            tColorCountY ++;
        }
    glEnd();
    glEnable(GL_TEXTURE_2D);
    //glRotatef(cWorldRotation, 0.0f, 0.0f, -1.0f);

    ///--[Entities]
    //--Compute angular offset.
    float cAngX = (0.0f * cInc);
    float cAngY = (0.0f * cInc);
    if(mPlayerFacing == DIR_UP)
    {
        cAngX =  6.0f * cInc;
        cAngY =  4.0f * cInc;
    }
    else if(mPlayerFacing == DIR_RIGHT)
    {
        cAngX = 0.0f * cInc;
        cAngY = 5.0f * cInc;
    }
    else if(mPlayerFacing == DIR_DOWN)
    {
        cAngX = -5.0f * cInc;
        cAngY = 4.0f * cInc;
    }
    else if(mPlayerFacing == DIR_LEFT)
    {
        cAngX = -10.0f * cInc;
        cAngY =  5.0f * cInc;
    }

    //--Compute height.
    float cColorMix = 1.0f;
    float cEntityHeight = cRadius + 1.0f;
    if(mVictoryMode == BSL_VICTORY_BURST)
    {
        cColorMix = 1.0f - EasingFunction::QuadraticOut(mVictoryTimer, 30.0f);
        cEntityHeight = cEntityHeight + (cInc * EasingFunction::QuadraticOut(mVictoryTimer, 30.0f) * 5.0f);
    }
    else if(mVictoryMode > BSL_VICTORY_BURST)
    {
        cColorMix = 0.0f;
        cEntityHeight = cEntityHeight + (cInc * 5.0f);
    }

    //--Iterate across the grid of objects and render them at intersections as needed.
    glColor4f(cColorMix, cColorMix, cColorMix, cColorMix);
    for(int x = 0; x < BSL_OBJ_X; x ++)
    {
        for(int y = 0; y < BSL_OBJ_Y; y ++)
        {
            //--If this is a sphere.
            if(mObjectGrid[x][BSL_OBJ_Y - y - 1] != BSL_EMPTY)
            {
                //--Move Progress Offsets.
                float cProgressOffsetX = 0.0f;
                float cProgressOffsetY = 0.0f;
                if(mPlayerFacing == DIR_UP)
                {
                    cProgressOffsetX = mPlayerMoveProgress * cInc * -1.0f;
                }
                else if(mPlayerFacing == DIR_RIGHT)
                {
                    cProgressOffsetY = mPlayerMoveProgress * cInc;
                }
                else if(mPlayerFacing == DIR_DOWN)
                {
                    cProgressOffsetX = mPlayerMoveProgress * cInc;
                }
                else if(mPlayerFacing == DIR_LEFT)
                {
                    cProgressOffsetY = mPlayerMoveProgress * cInc * -1.0f;
                }

                //--Angular modifier.
                float cUseAngX = (y * cInc) + (cProgressOffsetX);
                float cUseAngY = (x * cInc) + (cProgressOffsetY);

                //--[Shadow]
                if(false)
                {
                    //--Compute the center point of this object.
                    float cShadowHeight = cRadius + 1.0f;
                    float cCentX = cosf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cShadowHeight);
                    float cCentY =                                     cosf((cUseAngY+cAngY) * TORADIAN) * (cShadowHeight);
                    float cCentZ = sinf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cShadowHeight);

                    //--Billboard.
                    glTranslatef(cCentX, cCentY, cCentZ);
                    glRotatef(cWorldRotation, 0.0f, 0.0f, -1.0f);
                    glRotatef(tRoll, 0, 0, -1);
                    glRotatef(tYaw, 0, -1, 0);
                    glRotatef(tPitch, -1, 0, 0);

                    //--Size computation.
                    float cSizeFactor = 1.0f;
                    float cSiz = 2.0f * cSizeFactor;

                    //--Render.
                    Images.Data.rPlayerShadowImage->Bind();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0.0f, 1.0f); glVertex3f(-cSiz, -cSiz, 0.0f);
                        glTexCoord2f(1.0f, 1.0f); glVertex3f( cSiz, -cSiz, 0.0f);
                        glTexCoord2f(1.0f, 0.0f); glVertex3f( cSiz,  cSiz, 0.0f);
                        glTexCoord2f(0.0f, 0.0f); glVertex3f(-cSiz,  cSiz, 0.0f);
                    glEnd();

                    //--Un-billboard.
                    glRotatef(tPitch, 1, 0, 0);
                    glRotatef(tYaw, 0, 1, 0);
                    glRotatef(tRoll, 0, 0, 1);
                    glRotatef(cWorldRotation, 0.0f, 0.0f, 1.0f);
                    glTranslatef(-cCentX, -cCentY, -cCentZ);
                }

                //--[Sphere]
                {
                    //--Compute the center point of this object.
                    float cCentX = cosf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cEntityHeight);
                    float cCentY =                                     cosf((cUseAngY+cAngY) * TORADIAN) * (cEntityHeight);
                    float cCentZ = sinf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cEntityHeight);

                    //--Billboard.
                    glTranslatef(cCentX, cCentY, cCentZ);
                    glRotatef(cWorldRotation, 0.0f, 0.0f, -1.0f);
                    glRotatef(tRoll, 0, 0, -1);
                    glRotatef(tYaw, 0, -1, 0);
                    glRotatef(tPitch, -1, 0, 0);

                    //--Size computation.
                    float cSizeFactor = 1.0f;
                    float cSiz = 2.0f * cSizeFactor;

                    //--Render.
                    if(mObjectGrid[x][BSL_OBJ_Y - y - 1] == BSL_RED_SPHERE || mObjectGrid[x][BSL_OBJ_Y - y - 1] == BSL_RED_SPHERE_WAS_BLUE)
                    {
                        Images.Data.rRedSphere->Bind();
                    }
                    else if(mObjectGrid[x][BSL_OBJ_Y - y - 1] == BSL_BLUE_SPHERE)
                    {
                        Images.Data.rBlueSphere->Bind();
                    }
                    else if(mObjectGrid[x][BSL_OBJ_Y - y - 1] == BSL_BUMPER)
                    {
                        Images.Data.rBumperSphere->Bind();
                    }
                    else if(mObjectGrid[x][BSL_OBJ_Y - y - 1] == BSL_LAUNCHER)
                    {
                        Images.Data.rYellowSphere->Bind();
                    }

                    //--Standard render.
                    glBegin(GL_QUADS);
                        glTexCoord2f(0.0f, 1.0f); glVertex3f(-cSiz, -cSiz, 0.0f);
                        glTexCoord2f(1.0f, 1.0f); glVertex3f( cSiz, -cSiz, 0.0f);
                        glTexCoord2f(1.0f, 0.0f); glVertex3f( cSiz,  cSiz, 0.0f);
                        glTexCoord2f(0.0f, 0.0f); glVertex3f(-cSiz,  cSiz, 0.0f);
                    glEnd();

                    //--Un-billboard.
                    glRotatef(tPitch, 1, 0, 0);
                    glRotatef(tYaw, 0, 1, 0);
                    glRotatef(tRoll, 0, 0, 1);
                    glRotatef(cWorldRotation, 0.0f, 0.0f, 1.0f);
                    glTranslatef(-cCentX, -cCentY, -cCentZ);
                }
            }

        }
    }
    glColor3f(1.0f, 1.0f, 1.0f);

    ///--[Clean Up]
    //--Unotate based on player turn progress.
    glRotatef(cWorldRotation, 0.0f, 0.0f, -1.0f);

    ///--[Player Rendering]
    {
        //--Repair the angular sets.
        if(mPlayerFacing == DIR_UP)
        {
            cAngX =  5.0f * cInc;
            cAngY = 12.0f * cInc;
        }
        else if(mPlayerFacing == DIR_RIGHT)
        {
            cAngX = 8.0f * cInc;
            cAngY = 9.0f * cInc;
        }
        else if(mPlayerFacing == DIR_DOWN)
        {
            cAngX =  5.0f * cInc;
            cAngY = 12.0f * cInc;
        }
        else if(mPlayerFacing == DIR_LEFT)
        {
            cAngX = 8.0f * cInc;
            cAngY = 9.0f * cInc;
        }

        //--[GL Setup]
        //glDisable(GL_DEPTH_TEST);
        float cPlayerZBonus = -0.70f;

        //--[Position]
        //--Angular modifier.
        float cUseAngX = (13.0f * cInc) + (-0.4f * cInc);
        float cUseAngY = ( 7.0f * cInc) + (-0.4f * cInc);

        //--Left and Right are offset.
        if(mPlayerFacing == DIR_UP)
        {
        }
        else if(mPlayerFacing == DIR_RIGHT)
        {
            cUseAngX = cUseAngX - (3.0f * cInc);
            cUseAngY = cUseAngY + (3.0f * cInc);
        }
        else if(mPlayerFacing == DIR_DOWN)
        {
        }
        else if(mPlayerFacing == DIR_LEFT)
        {
            cUseAngX = cUseAngX - (3.0f * cInc);
            cUseAngY = cUseAngY + (3.0f * cInc);
        }

        //--[Shadow]
        {
            //--Compute the center point of this object.
            float cHeight = cRadius - 2.0f;
            float cCentX = cosf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cHeight);
            float cCentY =                                     cosf((cUseAngY+cAngY) * TORADIAN) * (cHeight);
            float cCentZ = sinf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cHeight);

            //--Billboard.
            glTranslatef(cCentX, cCentY, cCentZ);
            glRotatef(tRoll, 0, 0, -1);
            glRotatef(tYaw, 0, -1, 0);
            glRotatef(tPitch, -1, 0, 0);

            //--[Rendering]
            //--Setup.
            glColor3f(1.0f, 1.0f, 1.0f);

            //--Reposition.
            float cX = (Images.Data.rPlayerShadowImage->GetWidth()  * -0.5f) + 4.8f;
            float cY = (Images.Data.rPlayerShadowImage->GetHeight() * -0.5f) + 7.0f;
            float cZ = cPlayerZBonus;
            glTranslatef(cX, cY, cZ);

            //--Render.
            float cPlayerScale = 0.3f;
            glScalef(cPlayerScale, cPlayerScale, 1.0f);
            Images.Data.rPlayerShadowImage->Bind();
            Images.Data.rPlayerShadowImage->RenderAt();

            //--Clean.
            glScalef(1.0f / cPlayerScale, 1.0f / cPlayerScale, 1.0f);
            glTranslatef(-cX, -cY, -cZ);

            //--[Unposition]
            //--Un-billboard.
            glRotatef(tPitch, 1, 0, 0);
            glRotatef(tYaw, 0, 1, 0);
            glRotatef(tRoll, 0, 0, 1);
            glTranslatef(-cCentX, -cCentY, -cCentZ);
        }

        //--[Player]
        {
            //--Compute height based on jump.
            float cHeight = cRadius - 2.0f;
            if(mJumpDistanceLeft > 0)
            {
                //--Progress across the jump.
                float tJumpProgress = sinf(mJumpDistanceLeft / (float)mJumpDistanceMax * 3.1415926f);

                //--For a standard jump, the height is 5.0f units.
                if(mJumpDistanceMax == BSL_JUMP_DISTANCE)
                {
                    cHeight = cHeight + (tJumpProgress * 5.0f);
                }
                //--Launcher spheres go 15 units high.
                else
                {
                    cHeight = cHeight + (tJumpProgress * 25.0f);
                }
            }

            //--Compute the center point of this object.
            float cCentX = cosf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cHeight);
            float cCentY =                                     cosf((cUseAngY+cAngY) * TORADIAN) * (cHeight);
            float cCentZ = sinf((cUseAngX+cAngX) * TORADIAN) * sinf((cUseAngY+cAngY) * TORADIAN) * (cHeight);

            //--Billboard.
            glTranslatef(cCentX, cCentY, cCentZ);
            glRotatef(tRoll, 0, 0, -1);
            glRotatef(tYaw, 0, -1, 0);
            glRotatef(tPitch, -1, 0, 0);

            //--[Rendering]
            //--Setup.
            glColor3f(1.0f, 1.0f, 1.0f);

            //--Images.
            int cImageIndex = mPlayerMoveTicksTotal / 12 % 4;

            //--Reposition.
            float cX = (Images.Data.rPlayerWalkImages[0]->GetWidth()  * -0.5f) - 1.5f;
            float cY = (Images.Data.rPlayerWalkImages[0]->GetHeight() * -0.5f) + 1.0f;
            float cZ = cPlayerZBonus;
            if(cImageIndex == 1 || cImageIndex == 3) cY = cY + 0.5f;
            glTranslatef(cX, cY, cZ);

            //--Render.
            float cPlayerScale = 0.3f;
            glScalef(cPlayerScale, cPlayerScale, 1.0f);
            Images.Data.rPlayerWalkImages[cImageIndex]->Bind();
            Images.Data.rPlayerWalkImages[cImageIndex]->RenderAt();

            //--Clean.
            glScalef(1.0f / cPlayerScale, 1.0f / cPlayerScale, 1.0f);
            glTranslatef(-cX, -cY, -cZ);

            //--[Unposition]
            //--Un-billboard.
            glRotatef(tPitch, 1, 0, 0);
            glRotatef(tYaw, 0, 1, 0);
            glRotatef(tRoll, 0, 0, 1);
            glTranslatef(-cCentX, -cCentY, -cCentZ);
        }

        //--[GL Clean]
        //glEnable(GL_DEPTH_TEST);
    }

    //--Reset back to the zero position.
    glTranslatef(-tX, -tY, -tZ);
    glRotatef(tRoll, 0, 0, -1);
    glRotatef(tYaw, 0, -1, 0);
    glRotatef(tPitch, -1, 0, 0);
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f / cScale);

    //--Restore the GL state.
    DisplayManager::CleanFrustum();
    DisplayManager::StdOrtho();

    ///--[HUD]
    //--Show how many spheres are needed.
    if(false)
    {
        Images.Data.rSystemFont->DrawTextArgs(0.0f,  0.0f, 0, 1.0f, "Blues Left: %i", mBlueSpheresRemaining);
    }

    ///--[Debug]
    //--Render camera information.
    if(true)
    {
        Images.Data.rSystemFont->DrawTextArgs(0.0f,  0.0f, 0, 1.0f, "Pos %5.2f %5.2f %5.2f", tX, tY, tZ);
        Images.Data.rSystemFont->DrawTextArgs(0.0f, 16.0f, 0, 1.0f, "Rot %5.2f %5.2f %5.2f", tPitch, tYaw, tRoll);
        Images.Data.rSystemFont->DrawTextArgs(0.0f, 32.0f, 0, 1.0f, "Ply %5.2f %5.2f", mPlayerX, mPlayerY);
        if(mPlayerFacing == DIR_UP)
        {
            Images.Data.rSystemFont->DrawTextArgs(0.0f, 48.0f, 0, 1.0f, "Up");
        }
        else if(mPlayerFacing == DIR_RIGHT)
        {
            Images.Data.rSystemFont->DrawTextArgs(0.0f, 48.0f, 0, 1.0f, "Right");
        }
        else if(mPlayerFacing == DIR_DOWN)
        {
            Images.Data.rSystemFont->DrawTextArgs(0.0f, 48.0f, 0, 1.0f, "Down");
        }
        else if(mPlayerFacing == DIR_LEFT)
        {
            Images.Data.rSystemFont->DrawTextArgs(0.0f, 48.0f, 0, 1.0f, "Left");
        }
        Images.Data.rSystemFont->DrawTextArgs(0.0f, 64.0f, 0, 1.0f, "Speed: %f", mPlayerSpeed);
    }

    ///--[Results]
    if(mVictoryMode >= BSL_VICTORY_SHOWRESULTS)
    {
        //--Positioning.
        float cYOffset = -512.0f * (1.0f - EasingFunction::QuadraticInOut(mVictoryTimer, 60.0f));
        if(mVictoryMode == BSL_VICTORY_TRANSITIONOUT) cYOffset = 0.0f;
        glTranslatef(0.0f, cYOffset, 0.0f);

        //--Text.
        char tBuffer[64];
        if(!mIsDefeat)
        {
            strcpy(tBuffer, "Victory! Well done!");
        }
        else
        {
            strcpy(tBuffer, "Defeat...");
        }

        //--Render.
        float cScale = 5.0f;
        float tWidth = Images.Data.rSystemFont->GetTextWidth(tBuffer) * cScale;
        Images.Data.rSystemFont->DrawTextArgs((VIRTUAL_CANVAS_X - tWidth) * 0.5f, 256.0f, 0, cScale, tBuffer);

        //--Clean.
        glTranslatef(0.0f, -cYOffset, 0.0f);
    }

    ///--[Fading]
    if(mVictoryMode == BSL_VICTORY_TRANSITIONOUT)
    {
        //--Compute.
        float tAlpha = mVictoryTimer / 30.0f;

        //--Setup.
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, tAlpha);
        glDisable(GL_TEXTURE_2D);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        StarlightColor::ClearMixer();
        glEnable(GL_TEXTURE_2D);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void BlueSphereLevel::HookToLuaState(lua_State *pLuaState)
{
    /* BSL_Create()
       Creates a new Blue Sphere Level and provides it to the MapManager. */
    lua_register(pLuaState, "BSL_Create", &Hook_BSL_Create);

    /* BSL_CreateAndStore()
       Creates a new Blue Sphere Level and provides it to the MapManager. Whatever level was stored
       previously is given to the BlueSphereLevel to hold. */
    lua_register(pLuaState, "BSL_CreateAndStore", &Hook_BSL_CreateAndStore);

    /* BSL_SetProperty("Parse Data", sFilePath)
       BSL_SetProperty("Sizes", iXSize, iYSize)
       BSL_SetProperty("Element At", iX, iY, iElement)
       Sets the requested property in the active Blue Sphere Level. */
    lua_register(pLuaState, "BSL_SetProperty", &Hook_BSL_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_BSL_Create(lua_State *L)
{
    //BSL_Create()
    BlueSphereLevel *nLevel = new BlueSphereLevel();
    MapManager::Fetch()->ReceiveLevel(nLevel);

    return 0;
}
int Hook_BSL_CreateAndStore(lua_State *L)
{
    //BSL_CreateAndStore()
    BlueSphereLevel *nLevel = new BlueSphereLevel();
    nLevel->ProvideSuspendedLevel(MapManager::Fetch()->LiberateLevel());
    MapManager::Fetch()->ReceiveLevel(nLevel);
    return 0;
}
int Hook_BSL_SetProperty(lua_State *L)
{
    //BSL_SetProperty("Levels Total", iCount)
    //BSL_SetProperty("Parse Data", sFilePath)
    //BSL_SetProperty("Sizes", iXSize, iYSize)
    //BSL_SetProperty("Element At", iX, iY, iElement)
    //BSL_SetProperty("Player Start", iX, iY, iFacing)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("BSL_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Values]

    //--[Dynamic Values]
    //--Active object.
    BlueSphereLevel *rActiveLevel = (BlueSphereLevel *)MapManager::Fetch()->GetActiveLevel();
    if(!rActiveLevel || !rActiveLevel->IsOfType(POINTER_TYPE_BLUESPHERELEVEL)) return LuaTypeError("BSL_SetProperty", L);

    //--How many levels there are on the level select.
    if(!strcasecmp(rSwitchType, "Levels Total") && tArgs == 2)
    {
        rActiveLevel->ActivateLevelSelect(lua_tointeger(L, 2));
    }
    //--Parses data from the provided .slf file.
    else if(!strcasecmp(rSwitchType, "Parse Data") && tArgs == 2)
    {
        rActiveLevel->ReadFromFile(lua_tostring(L, 2));
    }
    //--Size of the level.
    else if(!strcasecmp(rSwitchType, "Sizes") && tArgs == 3)
    {
        rActiveLevel->SizeMap(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Element at the given location.
    else if(!strcasecmp(rSwitchType, "Element At") && tArgs == 4)
    {
        rActiveLevel->SetMapElement(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Player Start.
    else if(!strcasecmp(rSwitchType, "Player Start") && tArgs == 4)
    {
        rActiveLevel->SetPlayerPosition(lua_tointeger(L, 2), lua_tointeger(L, 3));
        rActiveLevel->SetPlayerFacing(lua_tointeger(L, 4));
    }
    //--Error.
    else
    {
        LuaPropertyError("BSL_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
