//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

void ControlManager::AllocateImages(int pImages)
{
    //--Deallocate.
    free(mControlImagePacks);

    //--Reset.
    mControlImagePacksTotal = 0;
    mControlImagePacks = NULL;
    if(pImages < 1) return;

    //--Allocate.
    mControlImagePacksTotal = pImages;
    SetMemoryData(__FILE__, __LINE__);
    mControlImagePacks = (ControlImagePack *)starmemoryalloc(sizeof(ControlImagePack) * mControlImagePacksTotal);
    for(int i = 0; i < mControlImagePacksTotal; i ++) mControlImagePacks[i].Initialize();
}
void ControlManager::SetErrorImage(const char *pDLPath)
{
    rErrorImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void ControlManager::SetImage(int pSlot, int pKeyCode, int pMouseCode, int pJoyCode, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mControlImagePacksTotal) return;
    mControlImagePacks[pSlot].mKeyCode = pKeyCode;
    mControlImagePacks[pSlot].mMouseCode = pMouseCode;
    mControlImagePacks[pSlot].mJoyCode = pJoyCode;
    strcpy(mControlImagePacks[pSlot].mImagePath, pDLPath);
    mControlImagePacks[pSlot].rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
StarBitmap *ControlManager::GetErrorImage()
{
    return rErrorImage;
}
StarBitmap *ControlManager::ResolveScancodeImage(int pScancode)
{
    //--Find the scancode and return the matching image, or the error image if not found.
    for(int i = 0; i < mControlImagePacksTotal; i ++)
    {
        if(mControlImagePacks[i].mKeyCode == pScancode) return mControlImagePacks[i].rImage;
    }

    //--Control didn't find a match.
    return rErrorImage;
}
StarBitmap *ControlManager::ResolveControlImage(const char *pControlName)
{
    //--Find the control that matches the name.
    ControlState *rControlState = (ControlState *)mControlList->GetElementByName(pControlName);
    if(!rControlState) return rErrorImage;

    //--Control exists, now scan the images packs for a match.
    for(int i = 0; i < mControlImagePacksTotal; i ++)
    {
        //--Pass if the control isn't identical.
        if(mControlImagePacks[i].mKeyCode   != rControlState->mWatchKeyPri)      continue;
        if(mControlImagePacks[i].mMouseCode != rControlState->mWatchMouseBtnPri) continue;
        if(mControlImagePacks[i].mJoyCode   != rControlState->mWatchJoyPri)      continue;

        //--Match!
        return mControlImagePacks[i].rImage;
    }

    //--Control didn't find a match.
    return rErrorImage;
}
const char *ControlManager::ResolveControlImagePath(const char *pControlName)
{
    ControlState *rControlState = (ControlState *)mControlList->GetElementByName(pControlName);
    if(!rControlState) return "Null";

    //--Control exists, now scan the images packs for a match.
    for(int i = 0; i < mControlImagePacksTotal; i ++)
    {
        //--Pass if the control isn't identical.
        if(mControlImagePacks[i].mKeyCode   != rControlState->mWatchKeyPri)      continue;
        if(mControlImagePacks[i].mMouseCode != rControlState->mWatchMouseBtnPri) continue;
        if(mControlImagePacks[i].mJoyCode   != rControlState->mWatchJoyPri)      continue;

        //--Match!
        return mControlImagePacks[i].mImagePath;
    }

    //--Control didn't find a match.
    return "Null";
}
