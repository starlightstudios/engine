//--Base
#include "PeakFreaksTitle.h"

//--Classes
#include "PeakFreaksLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

void PeakFreaksTitle::ActivateFinale()
{
    mIsFinaleMode = true;
}
void PeakFreaksTitle::ShowPersonalBest()
{
    mShowPersonalBest = true;
}
void PeakFreaksTitle::UpdateFinale()
{
    ///--[Documentation]
    //--Update inputs and timers for finale mode, which is normally called once the player wins
    //  or loses a match.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Cloud Spawning]
    if(mShouldSpawnClouds)
    {
        mShouldSpawnClouds = false;
        for(int i = 0; i < 6; i ++)
        {
            SpawnCloudFinale();
        }
    }

    ///--[Timers]
    //--Run to zero then play music.
    if(mMusicTimer > 0)
    {
        //--Decrement.
        mMusicTimer --;

        //--Finish.
        if(mMusicTimer < 1)
        {
            if(!PeakFreaksLevel::xPlayerVictory)
            {
                AudioManager::Fetch()->PlaySound("PF Announce TooBad");
            }
            else
            {
                AudioManager::Fetch()->PlaySound("PF Announce YouDidIt");
            }
            AudioManager::Fetch()->PlayMusic("Finale");
        }
    }

    //--Run timers.
    if(mFinaleTimer < PFTITLE_FINALE_TICKS) mFinaleTimer ++;
    if(mFinaleFadeTimer > 0) mFinaleFadeTimer --;
    mAlienTimer ++;
    if(mAlienTimer > PFTITLE_FINALE_ALIEN_TICKS) mAlienTimer = 0;
    mCreditsTimer ++;
    if(mCreditsTimer > PFTITLE_FINALE_CREDITS_TICKS) mCreditsTimer = 0;
    mCursorTimer ++;
    if(mCursorTimer > PFTITLE_CURSOR_CYCLE_TICKS) mCursorTimer = 0;

    //--No cap on this.
    mFireworksSFXTimer ++;

    //--When the fireworks timer reaches certain spots, play the fireworks SFX. This gives it the
    //  impression of "looping".
    if(mFireworksSFXTimer % 5400 == 5)
    {
        AudioManager::Fetch()->PlaySound("Fireworks");
    }

    ///--[Fireworks Handling]
    //--Update timers for all fireworks.
    PFTitle_Firework *rFirework = (PFTitle_Firework *)mFireworks->SetToHeadAndReturn();
    while(rFirework)
    {
        //--Timer.
        rFirework->mAnimTimer ++;

        //--Clamp.
        int tFrame = (int)(rFirework->mAnimTimer / PFTITLE_FIREWORK_TPF);
        if(tFrame >= PFTITLE_FIREWORKS_FRAMES) mFireworks->RemoveRandomPointerEntry();

        //--Next.
        rFirework = (PFTitle_Firework *)mFireworks->IncrementAndGetRandomPointerEntry();
    }

    //--If the fireworks timer reaches zero, spawn a new firework.
    if(mFireworksTimer > 0)
    {
        //--Timer.
        mFireworksTimer --;

        //--Spawn a new one.
        if(mFireworksTimer < 1)
        {
            //--Reset flags.
            mFireworksTimer = (rand() % (PFTITLE_FIREWORK_SPAWN_TICKS_HI - PFTITLE_FIREWORK_SPAWN_TICKS_LO)) + PFTITLE_FIREWORK_SPAWN_TICKS_LO;

            //--New firework.
            PFTitle_Firework *nFirework = (PFTitle_Firework *)starmemoryalloc(sizeof(PFTitle_Firework));
            nFirework->Initialize();

            //--Pick a scattered X location. The entire screen is fair game. -80 to center the firework frame.
            nFirework->mXPos = (rand() % (int)VIRTUAL_CANVAS_X) - 80;

            //--Y positions are mostly towards the bottom of the canvas.
            nFirework->mYPos = (rand() % 575) + 150;

            //--Random color.
            nFirework->mColorIndex = rand() % PFTITLE_FIREWORKS_COLS;

            //--Register.
            mFireworks->AddElement("X", nFirework, &FreeThis);
        }
    }

    ///--[Cloud Handling]
    //--When the timer hits its cap, reset it and spawn a cloud.
    mCloudSpawnTimer ++;
    if(mCloudSpawnTimer >= PFTITLE_CLOUD_TICKS)
    {
        //--Reset.
        mCloudSpawnTimer = 0;

        //--Spawn.
        SpawnCloudFinaleEdge();
    }

    //--Move clouds.
    PFTitle_Cloud *rCloud = (PFTitle_Cloud *)mCloudList->SetToHeadAndReturn();
    while(rCloud)
    {
        //--Move.
        rCloud->mXPos = rCloud->mXPos + rCloud->mXSpeed;

        //--Upon hitting the right side of the screen, despawn.
        if(rCloud->mXPos >= VIRTUAL_CANVAS_X)
        {
            mCloudList->RemoveRandomPointerEntry();
        }

        //--Next.
        rCloud = (PFTitle_Cloud *)mCloudList->IncrementAndGetRandomPointerEntry();
    }

    ///--[ ========== Fading to Level ========= ]
    //--Blocks out all other input.
    if(mFadeToLevelTimer > 0)
    {
        //--Run timer.
        mFadeToLevelTimer ++;

        //--Ending case.
        if(mFadeToLevelTimer >= PFTITLE_FADE_TO_LEVEL_TICKS)
        {
            //--Resolve path.
            DataLibrary *rDataLibrary = DataLibrary::Fetch();
            SysVar *rPathVariable = (SysVar *)rDataLibrary->GetEntry("Root/Paths/System/Startup/sPeakFreaksPath");
            if(!rPathVariable || !rPathVariable->mAlpha) return;

            //--Setup.
            char *tPath = NULL;

            //--To Tutorial:
            if(mFadeCase == PFTITLE_TO_TUTORIAL)
            {
                tPath = InitializeString("%s%s", rPathVariable->mAlpha, "/Maps/Tutorial/Constructor.lua");
            }
            //--Playing a level:
            else
            {
                tPath = InitializeString("%s%s", rPathVariable->mAlpha, "/Maps/SampleLevel/Constructor.lua");
            }

            //--Execute.
            LuaManager::Fetch()->ExecuteLuaFile(tPath);

            //--This object is now unstable and needs to exit.
            free(tPath);
        }
        return;
    }

    ///--[Controls]
    if(rControlManager->IsFirstPress("PF Up"))
    {
        //--Move.
        mFinaleCursor --;
        while(mFinaleCursor < 0) mFinaleCursor += PFTITLE_FINALE_TOTAL;

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Change");
    }
    else if(rControlManager->IsFirstPress("PF Down"))
    {
        //--Move.
        mFinaleCursor ++;
        while(mFinaleCursor >= PFTITLE_FINALE_TOTAL) mFinaleCursor -= PFTITLE_FINALE_TOTAL;

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Change");
    }

    //--Activate.
    if(rControlManager->IsFirstPress("PF Activate"))
    {
        //--Start the game:
        if(mFinaleCursor == PFTITLE_FINALE_REMATCH)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("PF Accept");
            AudioManager::Fetch()->StopAllSound();

            //--Fade music out.
            AudioManager::Fetch()->FadeMusic(15);

            //--Set fade handlers.
            mFadeToLevelTimer = 1;
            mFadeCase = PFTITLE_TO_LEVEL;
            return;
        }
        //--Return to main menu.
        else if(mFinaleCursor == PFTITLE_FINALE_QUIT)
        {
            mIsFinaleMode = false;
            mMusicTimer = 10;
            AudioManager::Fetch()->PlaySound("PF Accept");
            AudioManager::Fetch()->StopAllSound();
        }
    }
}
void PeakFreaksTitle::RenderFinale()
{
    ///--[Documentation]
    //--Renders finale mode.
    if(!Images.mIsReady) return;

    //--Timers.
    float mFinalePct = EasingFunction::QuadraticIn(mFinaleTimer, PFTITLE_FINALE_TICKS);
    float mAlienPct = sinf(mAlienTimer / (float)PFTITLE_FINALE_ALIEN_TICKS * 3.1415926f * 2.0f);

    ///--[Background]
    //--Doesn't move.
    Images.Finale.rBackgroundSky->Draw();

    //--Clouds. Scrolls around.
    PFTitle_Cloud *rCloud = (PFTitle_Cloud *)mCloudList->PushIterator();
    while(rCloud)
    {
        rCloud->rImage->Draw(rCloud->mXPos, rCloud->mYPos);
        rCloud = (PFTitle_Cloud *)mCloudList->AutoIterate();
    }

    //--Fireworks.
    PFTitle_Firework *rFirework = (PFTitle_Firework *)mFireworks->PushIterator();
    while(rFirework)
    {
        //--Resolve frame.
        int tFrame = (int)(rFirework->mAnimTimer / (float)PFTITLE_FIREWORK_TPF);
        if(tFrame < 0) tFrame = 0;
        if(tFrame >= PFTITLE_FIREWORKS_FRAMES) tFrame = PFTITLE_FIREWORKS_FRAMES - 1;
        SugarBitmap *rImage = Images.Finale.rFireworks[rFirework->mColorIndex][tFrame];

        //--Render.
        rImage->Draw(rFirework->mXPos, rFirework->mYPos);

        //--Next.
        rFirework = (PFTitle_Firework *)mFireworks->AutoIterate();
    }

    ///--[Portraits]
    //--Left portrait. Displays at increased size.
    if(rLftPortrait)
    {
        //--Setup.
        glTranslatef(mLftPorXOff, mLftPorYOff, 0.0f);
        glScalef(2.0f, 2.0f, 1.0f);

        //--Positions.
        float cTxL = 0.0f;
        float cTxR = 1.0f;
        if(mLftPorFlags & FLIP_HORIZONTAL)
        {
            cTxL = 1.0f - cTxL;
            cTxR = 1.0f - cTxR;
        }

        //--Render.
        rLftPortrait->Bind();
        float cWid = rLftPortrait->GetWidth();
        float cHei = rLftPortrait->GetHeight();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, 1.0f); glVertex2f(   0,    0);
            glTexCoord2f(cTxR, 1.0f); glVertex2f(cWid,    0);
            glTexCoord2f(cTxR, 0.0f); glVertex2f(cWid, cHei);
            glTexCoord2f(cTxL, 0.0f); glVertex2f(   0, cHei);
        glEnd();

        //--Clean.
        glScalef(0.5f, 0.5f, 1.0f);
        glTranslatef(-mLftPorXOff, -mLftPorYOff, 0.0f);
    }

    //--Right portrait. Normal size.
    if(rRgtPortrait)
    {
        //--Setup.
        glTranslatef(mRgtPorXOff, mRgtPorYOff, 0.0f);

        //--Positions.
        float cTxL = 0.0f;
        float cTxR = 1.0f;
        if(mRgtPorFlags & FLIP_HORIZONTAL)
        {
            cTxL = 1.0f - cTxL;
            cTxR = 1.0f - cTxR;
        }

        //--Render.
        rRgtPortrait->Bind();
        float cWid = rRgtPortrait->GetWidth();
        float cHei = rRgtPortrait->GetHeight();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, 1.0f); glVertex2f(   0,    0);
            glTexCoord2f(cTxR, 1.0f); glVertex2f(cWid,    0);
            glTexCoord2f(cTxR, 0.0f); glVertex2f(cWid, cHei);
            glTexCoord2f(cTxL, 0.0f); glVertex2f(   0, cHei);
        glEnd();

        //--Clean.
        glTranslatef(-mRgtPorXOff, -mRgtPorYOff, 0.0f);
    }

    ///--[Time Box]
    //--Slides up from the bottom.
    float cTimeYOff = (1.0f - mFinalePct) * 1000.0f;
    Images.Finale.rTimerMenu->Draw(0.0f, cTimeYOff);

    //--Construct a time string based on how many ticks elapsed during the round.
    char tBuf[32];
    int tMinutes = ( PeakFreaksLevel::xTimeElapsed) / 60 / 60;
    int tSeconds = ((PeakFreaksLevel::xTimeElapsed) / 60) % 60;
    int tMilliseconds = ((PeakFreaksLevel::xTimeElapsed % 60) / 60.0f * 1000.0f);

    //--Only goes as high as 99 minutes.
    tBuf[0] = ((tMinutes / 10) % 10) + '0';
    tBuf[1] = ((tMinutes /  1) % 10) + '0';
    tBuf[2] = ':';
    tBuf[3] = ((tSeconds / 10) % 10) + '0';
    tBuf[4] = ((tSeconds /  1) % 10) + '0';
    tBuf[5] = ':';
    tBuf[6] = ((tMilliseconds / 100) % 10) + '0';
    tBuf[7] = ((tMilliseconds /  10) % 10) + '0';
    tBuf[8] = ((tMilliseconds /   1) % 10) + '0';
    tBuf[9] = '\0';

    //--Render.
    Images.Finale.rTimeFont->DrawTextArgs(610.0f, 340.0f + cTimeYOff, 0, 1.0f, "%s", tBuf);

    //--Personal best.
    if(mShowPersonalBest) Images.Finale.rPersonalBestIco->Draw(0.0f, cTimeYOff);

    ///--[Credits Box]
    //--Backing.
    Images.Finale.rCreditsBack->Draw();

    //--Credits information.
    float cCreditsPct = EasingFunction::Linear(mCreditsTimer, PFTITLE_FINALE_CREDITS_TICKS);
    float cCreditsLft = 778.0f - (4500.0f * cCreditsPct);
    Images.Finale.rCreditsFont->DrawText(cCreditsLft, 659.0f, 0, 1.0f, "Design by Salty Justice and Terry Ross. Art by Terry Ross. Programming by Salty Justice. Music by Juhani Junkala.");

    //--Fixed.
    Images.Finale.rCreditsBar->Draw();

    //--Winner/Loser Title. Slightly scales up and down with the timer.
    SugarBitmap *rUseLabel = Images.Finale.rLabelTooBad;
    if(PeakFreaksLevel::xPlayerVictory) rUseLabel = Images.Finale.rLabelWinner;

    //--Position, based on the finale percent.
    float cLabelYOff = (1.0f - mFinalePct) * -1000.0f;
    float cLabelXPos = 405.0f + (rUseLabel->GetWidth()  * 0.5f);
    float cLabelYPos =  84.0f + (rUseLabel->GetHeight() * 0.5f) + cLabelYOff;
    float cLabelScale = 0.90f + (mAlienPct * 0.20f);
    glTranslatef(cLabelXPos, cLabelYPos, 0.0f);
    glScalef(cLabelScale, cLabelScale, 1.0f);
    rUseLabel->Draw((rUseLabel->GetWidth() * -0.5f), (rUseLabel->GetHeight() * -0.5f));
    glScalef(1.0f / cLabelScale, 1.0f / cLabelScale, 1.0f);
    glTranslatef(-cLabelXPos, -cLabelYPos, 0.0f);

    ///--[Buttons]
    RenderMenuOption(0, mFinaleCursor, Images.Finale.rText[0], Images.Finale.rSelection[0]);
    RenderMenuOption(1, mFinaleCursor, Images.Finale.rText[1], Images.Finale.rSelection[1]);

    ///--[Cursor]
    //--Resolve X position.
    float tPct = (float)(mCursorTimer / PFTITLE_CURSOR_CYCLE_TICKS) * 3.1415926f * 2.0f;
    float cCursorPosX = 720.0f + (sinf(tPct) * PFTITLE_CURSOR_CYCLE_DIST);

    //--Resolve Y position.
    float cCursorPosY = 565.0f;
    if(mFinaleCursor == 1)
    {
        cCursorPosY = 661.0f;
    }

    //--Render.
    Images.Data.rCursor->Draw(cCursorPosX, cCursorPosY);

    ///--[Alien]
    //--Only renders if the player won.
    if(PeakFreaksLevel::xPlayerVictory)
    {
        //--Resolve Y position.
        float cOverallYOff = (1.0f - mFinalePct) * -700.0f;
        float cBobbleYOff = mAlienPct * PFTITLE_FINALE_ALIEN_DISTANCE;

        //--Render.
        Images.Finale.rAlien->Draw(0.0f, cOverallYOff + cBobbleYOff);
    }

    ///--[Overlays]
    //--Fade from white overlay.
    if(mFinaleFadeTimer > 0)
    {
        float cPct = EasingFunction::Linear(mFinaleFadeTimer, PFTITLE_FINALE_FADE_TICKS);
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, cPct));
    }
    if(mFadeToLevelTimer >= 1)
    {
        float cPct = EasingFunction::QuadraticIn(mFadeToLevelTimer, PFTITLE_FADE_TO_LEVEL_TICKS);
        SugarBitmap::DrawFullBlack(cPct);
    }
}
