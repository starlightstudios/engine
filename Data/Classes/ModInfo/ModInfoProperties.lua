-- |[ =================================== ModInfo Properties =================================== ]|
--Contains getters and setters for ModInfo objects.
function ModInfo:fnSetDisplayName(psName)
    self.sDisplayName = psName
end
function ModInfo:fnSetPath(psPath)
    self.sPath = psPath
end