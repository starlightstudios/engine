///====================================== CarnationLetter =========================================
//--Represents a letter in the text box of Carnation Combat. When the letter is first rendered, it can
//  fire off applications and cause SFX, Effects, etc to occur. This allows actions to occur within
//  the text stride.
//--For sanity, only display/SFX should happen during such an event. Don't queue a new effect or ability
//  and expect the engine to handle that, do that from the caller.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[CLAppPack]
//--Packages together a string with its originator and target to make an Application that can be
//  passed to HandleApplication() in the combat class.
typedef struct CLAppPack
{
    //--Variables
    uint32_t mOriginatorID;
    uint32_t mTargetID;
    char *mString;

    //--Functions
    void Initialize()
    {
        mOriginatorID = 0;
        mTargetID = 0;
        mString = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        CLAppPack *rPtr = (CLAppPack *)pPtr;
        free(rPtr->mString);
        free(rPtr);
    }
}CLAppPack;

///===================================== Local Definitions ========================================
///====================================== CarnationLetter =========================================
class CarnationLetter
{
    private:
    ///--[System]
    bool mHasFiredApplications;
    uint32_t mLetter;

    ///--[Applications]
    StarLinkedList *mApplicationPacks; //CLAppPack *, master

    protected:

    public:
    //--System
    CarnationLetter();
    ~CarnationLetter();

    //--Public Variables
    //--Property Queries
    bool HasFiredApplications();
    uint32_t GetLetter();

    //--Manipulators
    void SetFiredApplications();
    void SetLetter(uint32_t pLetter);
    void RegisterApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    StarLinkedList *GetApplicationList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


///====================================== CarnationString =========================================
//--A string is a collection of letters, so a CarnationString is a collection of CarnationLetters.
//  That's all there is to it. Note that this structure actually has a NULL letter at the end, to
//  improve compatibility with other string functions.
typedef struct CarnationString
{
    //--Variables
    int mLength;
    CarnationLetter **mLetters;

    //--Functions
    void Initialize()
    {
        mLength = 0;
        mLetters = NULL;
    }
    void SetString(const char *pString)
    {
        //--Deallocate.
        if(mLength > 0)
        {
            for(int i = 0; i < mLength+1; i ++) delete mLetters[i];
            free(mLetters);
        }

        //--Zero.
        mLength = 0;
        mLetters = NULL;
        if(!pString) return;

        //--Initialize.
        mLength = (int)strlen(pString);
        if(mLength < 1) return;
        mLetters = (CarnationLetter **)starmemoryalloc(sizeof(CarnationLetter *) * (mLength+1));

        //--Set.
        for(int i = 0; i < mLength; i ++)
        {
            mLetters[i] = new CarnationLetter();
            mLetters[i]->SetLetter(pString[i]);
        }

        //--Last letter becomes the null-terminating letter.
        mLetters[mLength] = new CarnationLetter();
        mLetters[mLength]->SetLetter('\0');
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        CarnationString *rPtr = (CarnationString *)pPtr;
        for(int i = 0; i < rPtr->mLength+1; i ++) delete rPtr->mLetters[i];
        free(rPtr->mLetters);
        free(rPtr);
    }
}CarnationString;

