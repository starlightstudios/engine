///===================================== StarLoadInterrupt ========================================
//--This object is created right at program start and persists throughout. It is an 'interrupt' in
//  that it is called whenever a loading action occurs, and will render a loading screen automatically
//  with a background and a moving image. The object does not rely on anything else such as static
//  managers, so it is fairly independent.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class StarLoadInterrupt
{
    protected:
    //--System
    bool mIsInitted;
    bool mIsAwaitingKeypress;

    //--Loading Bar
    int mCurrent;
    int mCurrentMax;

    //--Animation Data.
    float mLastResetTime;
    int mFrame;
    int mFrameDenominator;

    //--Timing
    bool mForceNextUpdate;
    float mLastUpdateCall;

    protected:

    public:
    //--System
    StarLoadInterrupt();
    virtual ~StarLoadInterrupt();
    virtual void Construct();

    //--Public Variables
    bool mIsSuppressed;

    //--Property Queries
    int GetCallsSoFar();
    bool IsAwaitingKeypress();

    //--Manipulators
    void SetKeypressFlag(bool pFlag);
    static void WaitForKeypress();

    //--Core Methods
    void Reset(int pMaxCount);
    bool CommonLogicUpdate(bool pForceRender);
    void CommonRendererSetup();
    void CommonRendererClean();
    void Finish();
    virtual void DeallocateImages();
    static void Interrupt();

    //--Update
    //--File I/O
    //--Drawing
    virtual void Call(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    static StarLoadInterrupt *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_WaitForKeypress(lua_State *L);
int Hook_LI_Interrupt(lua_State *L);
int Hook_LI_Reset(lua_State *L);
int Hook_LI_Finish(lua_State *L);
int Hook_LI_GetLastInterruptCount(lua_State *L);
int Hook_LI_SetSuppression(lua_State *L);
