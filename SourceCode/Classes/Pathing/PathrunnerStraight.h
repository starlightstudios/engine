///=================================== Pathrunner Straight =======================================
//--Identical to the normal Pathrunner, except overrides certain behaviors so that making an NPC move
//  in straight lines is given preference over turning a lot. This necessarily slows down the pather
//  and pathing is normally intensive, so splitting it into its own class allows some speed recovery.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "Pathrunner.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class PathrunnerStraight : public Pathrunner
{
    private:
    protected:
    ///--[System]
    int mTurnPenalty;

    public:
    //--System
    PathrunnerStraight();
    virtual ~PathrunnerStraight();

    //--Property Queries
    //--Manipulators
    //--Core Methods
    static void InsertPackageOnList(PathrunnerPack *pPackage, StarLinkedList *pList, float pStartDistance);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void RunIterator(int pPulseCap, bool pStopOnReachingTarget);
    virtual StarLinkedList *ResolvePath();

    //--File I/O

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions
