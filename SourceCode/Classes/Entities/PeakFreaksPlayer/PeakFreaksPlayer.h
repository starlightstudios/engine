///====================================== PeakFreaksPlayer ========================================
//--Represents the player entity in Peak Freaks.

#pragma once

///========================================== Includes ============================================
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"
#include "Classes/Entities/RootEntity.h"

///===================================== Local Structures =========================================
///--[PeakFreaksPlayerJump]
//--Represents a jumping action. The jump is relative to the current location.
typedef struct PeakFreaksPlayerJump
{
    bool mHasCheckedClips;
    int mJumpX;
    int mJumpY;
}PeakFreaksPlayerJump;

///===================================== Local Definitions ========================================
//--Speeds
#define PLAYER_CLIMB_SPEED -0.25f
#define PLAYER_SLIP_SPEED 0.25f
#define PLAYER_SPEED_BAR_TICKS_PER_LOSS 120
#define PLAYER_DAMAGE_GRAVITY 0.500f

//--Timing
#define PLAYER_IDLE_TPF 5.0f
#define PLAYER_JUMP_WAIT_TICKS 15
#define PLAYER_GUANO_TICKS 600
#define PLAYER_CANYON_TICKS 600
#define PLAYER_SHOCK_TICKS 120
#define PLAYER_HAPPY_TICKS 120

//--Jump Durations
#define PLAYER_JUMP_DURATION0 100
#define PLAYER_JUMP_DURATION1 60
#define PLAYER_JUMP_DURATION2 43
#define PLAYER_JUMP_DURATION3 30
#define PLAYER_JUMP_DURATION4 20

//--Jump Height
#define PLAYER_JUMP_HEIGHT -0.5f

//--Respawn
#define PLAYER_RESPAWN_TICKS 60
#define PLAYER_RESPAWN_DISTANCE 200.0f

///========================================== Classes =============================================
class PeakFreaksPlayer : public RootEntity
{
    private:
    ///--[System]
    int mIdleTimer;
    int mRespawnTimer;
    int mShockTicks;
    int mHappyTicks;

    ///--[Position]
    int mTileX;
    int mTileY;
    int mLayer;
    float mPartialX;
    float mPartialY;

    ///--[Instructions]
    int mMovesRemaining;
    bool mHasPendingChange;
    float mSpeedX;
    float mSpeedY;
    float mNextSpeedX;
    float mNextSpeedY;

    //--Jumping Queue
    bool mIsJumpHelicopter;
    int mHelicopterTimer;
    bool mBypassJumpCharge;
    int mOverrideJumpSpeed;
    int mJumpLaunchTicks;
    int mJumpAirTimerCur;
    int mJumpAirTimerMax;
    PeakFreaksPlayerJump *mActiveJump;
    SugarLinkedList *mJumpingQueue; //PeakFreaksPlayerJump *, master

    //--Speed Bar
    int mSpeedBarLevel;
    int mSpeedBarTicks;

    //--Canyon Bar.
    int mCanyonTimer;

    //--Guano'd.
    int mGuanoTimer;

    ///--[Animation]
    float mMoveTimer;
    float mMoveTPF;

    ///--[Damage Sequence]
    bool mIsGettingDamaged;
    bool mFellThroughHole;
    int mDamageTimer;
    float mDamageOffX;
    float mDamageOffY;
    float mDamageSpeedX;
    float mDamageSpeedY;

    ///--[Victory Sequence]
    bool mIsWinning;
    int mWinTimer;

    ///--[Slip/Climb]
    bool mForceSlip;
    bool mForceClimb;

    ///--[AI Control]
    bool mIsAIControlled;
    bool mClearQueueWhenLanding;
    int mStallTicks;
    int mPathTicksSoFar;
    SugarLinkedList *mAIInstructions; //int *, master
    Pathrunner *mPathRunner;
    int mAIAlwaysStopTicks;
    int mAIAlwaysStopTicksLeft;
    int mAIMistakeMovesLeft;
    int mAIMistakeLo;
    int mAIMistakeHi;
    int mAIStopTicks;
    int mAIStopMovesLeft;
    int mAIStopLo;
    int mAIStopHi;
    int mAIDoubleMoveSpeedLeft;
    int mAIDoubleMoveSpeedChance;

    ///--[Images]
    struct
    {
        bool mIsReady;
        struct
        {
            //--Portraits
            SugarBitmap *rPorNeutral;
            SugarBitmap *rPorHappy;
            SugarBitmap *rPorSad;
            SugarBitmap *rPorShock;
            SugarBitmap *rTrackHead;

            //--Movement
            SugarBitmap *rIdle[4];
            SugarBitmap *rClimbRt;
            SugarBitmap *rClimbLf;
            SugarBitmap *rSlip;
            SugarBitmap *rVictory;
            SugarBitmap *rShadow;

            //--Heli-Lift
            SugarBitmap *rHeliLift[2];

            //--Directional Debug
            SugarBitmap *rArrowU;
            SugarBitmap *rArrowR;
            SugarBitmap *rArrowD;
            SugarBitmap *rArrowL;
        }Data;
    }Images;

    protected:

    public:
    //--System
    PeakFreaksPlayer();
    ~PeakFreaksPlayer();

    //--Public Variables
    //--Property Queries
    bool IsFlashingWhite();
    bool IsAirborne();
    bool IsIgnoringHazards();
    float GetWorldX();
    float GetWorldY();
    float GetDamageOffsetX();
    float GetDamageOffsetY();
    float GetDamageRotation();
    float GetOffsetX();
    float GetOffsetY();
    float GetOffsetZ();
    int GetWorldZ();
    float GetSpeedX();
    float GetSpeedY();
    int GetSpeedBarLevel();
    int GetActualSpeedBarLevel();
    static int GetDirectionFromSpeeds(float pXSpeed, float pYSpeed);
    int GetCanyonBarTimer();
    bool IsGuanod();
    int GetRespawnTimer();

    //--Manipulators
    void SetPosition(float pX, float pY);
    void SetPositionNoPartial(float pX, float pY);
    void SetLayer(int pZ);
    void SetSpeeds(float pX, float pY);
    void QueueNextSpeeds(float pX, float pY);
    void QueueJump(int pX, int pY);
    void QueueBump(int pX, int pY);
    void ResetToLastCheckpoint();
    void SetMovesRemaining(int pValue);
    void SetSpeedBar(int pLevel);
    void ActivateCanyonBar();
    void ActivateHelicopter();
    void Injure();
    void Guano();
    void Shock();
    void MakeHappy();
    void ClearJumps();

    ///--Pathing
    bool IsAIControlled();
    void SetAIControl();
    void HandleAIControlUpdate();
    void HandleAIInstructions();

    ///--Reconstitute
    void Reconstitute();

    //--Core Methods
    void SetScooterGraphics();
    void SetMimiGraphics();
    void SetAllenGraphics();
    void CheckJumpCollisions();
    void SetAIDifficulty(int pLevel);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    void BeginMove(float pXVec, float pYVec);
    void HandleSlip();
    void HandleClimb();

    //--File I/O
    //--Drawing
    SugarBitmap *ResolveFrame();
    SugarBitmap *ResolveArrow();
    SugarBitmap *GetPortrait();
    SugarBitmap *GetTrackHead();
    SugarBitmap *GetShadow();
    SugarBitmap *GetHelicopter();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
