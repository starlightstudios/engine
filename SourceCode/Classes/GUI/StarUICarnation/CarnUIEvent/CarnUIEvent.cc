//--Base
#include "CarnUIEvent.h"

//--Classes
#include "AdventureMenu.h"
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
CarnUIEvent::CarnUIEvent()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUIEVENT;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CEVT");

    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== CarnUIEvent ======= ]
    ///--[System]
    ///--[Event Data]
    mRecheckEvents = false;
    mEventName = NULL;
    mEventDescription = NULL;

    ///--[Character]
    mInstanceNumber = 0;
    mCharacterCursor = 0;
    rCharacterInternalName = NULL;
    rCharacterName = NULL;
    mCharacterHP = 0;
    mCharacterHPMax = 1;
    mCharacterSP = 0;
    mCharacterSPMax = 1;
    mCharacterInf = 0;
    mCharacterInfMax = 3;

    ///--[Revealing]
    mIsRevealed = false;
    mCanReveal = false;
    mRevealScriptPath = NULL;
    mRevealString = NULL;

    ///--[Options]
    mOptionCursor = CARNUI_EVENT_NO_OPTION;
    mCharacterList = new StarLinkedList(true);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    //--Arrow hitboxes occupying slots 0 and 1. These change the active character.
    RegisterHitboxWH("ArrowLft", 227.0f, 383.0f, 41.0f, 25.0f);
    RegisterHitboxWH("ArrowRgt", 379.0f, 383.0f, 41.0f, 25.0f);

    //--Reveal hitbox.
    RegisterHitboxWH("Reveal", 700.0f, 656.0f, 56.0f, 52.0f);

    //--Hitboxes for options.
    RegisterHitboxWH("Option0", 707.0f, 121.0f + (CARNUI_EVENT_OPTION_Y * 0.0f), 424.0f, 52.0f);
    RegisterHitboxWH("Option1", 707.0f, 121.0f + (CARNUI_EVENT_OPTION_Y * 1.0f), 424.0f, 52.0f);
    RegisterHitboxWH("Option2", 707.0f, 121.0f + (CARNUI_EVENT_OPTION_Y * 2.0f), 424.0f, 52.0f);
    RegisterHitboxWH("Option3", 707.0f, 121.0f + (CARNUI_EVENT_OPTION_Y * 3.0f), 424.0f, 52.0f);
    RegisterHitboxWH("Option4", 707.0f, 121.0f + (CARNUI_EVENT_OPTION_Y * 4.0f), 424.0f, 52.0f);
    RegisterHitboxWH("Option5", 707.0f, 121.0f + (CARNUI_EVENT_OPTION_Y * 5.0f), 424.0f, 52.0f);
}
CarnUIEvent::~CarnUIEvent()
{
    ///--[System]
    ///--[Event Data]
    free(mEventName);
    delete mEventDescription;

    ///--[Character]
    ///--[Reveal]
    free(mRevealScriptPath);
    free(mRevealString);

    ///--[Options]
    delete mCharacterList;
}
void CarnUIEvent::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation Event UI",  &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI", &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
bool CarnUIEvent::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    if(pType == POINTER_TYPE_CARNUIEVENT) return true;
    return false;
}

///======================================= Manipulators ===========================================
void CarnUIEvent::TakeForeground()
{
    ///--[Documentation]
    //--Reset variables. Note that the calling script should have done things like clearing the options.
    SetCharacterCursor(0);
    mOptionCursor = CARNUI_EVENT_NO_OPTION;

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|UIOpen");
}
void CarnUIEvent::SetInstanceNumber(int pNumber)
{
    mInstanceNumber = pNumber;
}
void CarnUIEvent::SetEventName(const char *pName)
{
    ResetString(mEventName, pName);
}
void CarnUIEvent::SetEventDescription(const char *pDescription)
{
    ///--[Documentation]
    //--If a description already exists, clear it.
    delete mEventDescription;
    mEventDescription = NULL;
    if(!pDescription) return;

    //--Runs subdivide on a string to turn it into a linked-list of strings for quick rendering.
    mEventDescription = Subdivide::SubdivideStringFlags(pDescription, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, CarnImages.rFont_Description, 440.0f);
}
void CarnUIEvent::SetIsRevealed(bool pIsRevealed)
{
    mIsRevealed = pIsRevealed;
}
void CarnUIEvent::SetCanReveal(bool pCanReveal)
{
    mCanReveal = pCanReveal;
}
void CarnUIEvent::SetRevealPath(const char *pPath)
{
    ResetString(mRevealScriptPath, pPath);
}
void CarnUIEvent::SetRevealString(const char *pPath)
{
    //--Note: Set to " " to hide the string if needed.
    ResetString(mRevealString, pPath);
}
void CarnUIEvent::SetOptionCursor(int pCursor)
{
    ///--[Documentation]
    //--Set the option cursor, and clamp it.
    mOptionCursor = pCursor;

    //--Clamp relies on the active option list, which may be NULL.
    StarLinkedList *rActiveOptionList = (StarLinkedList *)mCharacterList->GetElementByName(rCharacterInternalName);
    if(!rActiveOptionList) return;

    //--Clamp.
    if(mOptionCursor >= rActiveOptionList->GetListSize()) mOptionCursor = CARNUI_EVENT_NO_OPTION;
    if(mOptionCursor < CARNUI_EVENT_NO_OPTION) mOptionCursor = CARNUI_EVENT_NO_OPTION;
}
void CarnUIEvent::ClearOptions()
{
    mOptionCursor = CARNUI_EVENT_NO_OPTION;
    mCharacterList->ClearList();
}
void CarnUIEvent::RegisterCharacter(const char *pName)
{
    ///--[Documentation]
    //--Registers a character who can perform actions using RegisterOption() below. Each character can have different options
    //  depending on their internal state.
    if(!pName) return;

    ///--[Duplicate Check]
    void *rCheckOption = mCharacterList->GetElementByName(pName);
    if(rCheckOption) return;

    ///--[Create]
    StarLinkedList *nList = new StarLinkedList(true);
    mCharacterList->AddElementAsTail(pName, nList, &StarLinkedList::DeleteThis);
}
void CarnUIEvent::RegisterOption(const char *pCharacter, const char *pName, const char *pScript, const char *pFiringCode, const char *pDescription)
{
    ///--[Documentation]
    //--Registers an option with the given properties. Duplicates are automatically rejected.
    if(!pCharacter || !pName || !pScript || !pFiringCode || !pDescription) return;

    ///--[Character Check]
    //--Character must have a list. Bark a warning if it does not exist.
    StarLinkedList *rCharList = (StarLinkedList *)mCharacterList->GetElementByName(pCharacter);
    if(!rCharList)
    {
        fprintf(stderr, "CarnUIEvent:RegisterOption() - Warning. No character %s, option %s fails.\n", pCharacter, pName);
        return;
    }

    ///--[Duplicate Check]
    //--Options with the same name are prohibited.
    void *rCheckOption = rCharList->GetElementByName(pName);
    if(rCheckOption) return;

    ///--[Create]
    //--Create and initialize.
    CEventOptionPack *nPackage = (CEventOptionPack *)starmemoryalloc(sizeof(CEventOptionPack));
    nPackage->Initialize();
    ResetString(nPackage->mScript, pScript);
    ResetString(nPackage->mFiringCode, pFiringCode);

    //--Run subdivide to turn the description into a linked-list for fast rendering.
    nPackage->mDescription = Subdivide::SubdivideStringFlags(pDescription, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, CarnImages.rFont_Description, 440.0f);
    CarnUICommon::ReplaceCommonTagsInList(nPackage->mDescription);

    //--Register.
    rCharList->AddElementAsTail(pName, nPackage, &CEventOptionPack::DeleteThis);

    //--Set flag.
    mRecheckEvents = true;
}
void CarnUIEvent::SetOptionRevealText(const char *pCharacter, const char *pName, const char *pRevealText)
{
    ///--[Documentation]
    //--Registers an option with the given properties. Duplicates are automatically rejected.
    if(!pCharacter || !pName || !pRevealText) return;

    ///--[Locate]
    //--Get character.
    StarLinkedList *rCharList = (StarLinkedList *)mCharacterList->GetElementByName(pCharacter);
    if(!rCharList)
    {
        fprintf(stderr, "CarnUIEvent:SetOptionRevealText() - Warning. No character %s, cannot set reveal text.\n", pCharacter);
        return;
    }

    //--Get option.
    CEventOptionPack *rOptionPack = (CEventOptionPack *)rCharList->GetElementByName(pName);
    if(!rOptionPack)
    {
        fprintf(stderr, "CarnUIEvent:SetOptionRevealText() - Warning. No option %s found in character %s. Failing.\n", pName, pCharacter);
        return;
    }

    //--Clear.
    delete rOptionPack->mRevealed;

    //--Run subdivide and set it.
    rOptionPack->mRevealed = Subdivide::SubdivideStringFlags(pRevealText, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, CarnImages.rFont_Description, 440.0f);
    CarnUICommon::ReplaceCommonTagsInList(rOptionPack->mRevealed);
}
void CarnUIEvent::RecheckEvents()
{
    ///--[Documentation]
    //--Called after all events are registered and set up, removes any that have a response string
    //  of "REMOVE". This allows events to register without requiring conditionals to do so, they
    //  are purged during the next update call.
    if(!mRecheckEvents) return;

    //--Unset flag.
    mRecheckEvents = false;

    ///--[Iterate]
    StarLinkedList *rCharList = (StarLinkedList *)mCharacterList->PushIterator();
    while(rCharList)
    {
        //--For each option:
        CEventOptionPack *rOptionPack = (CEventOptionPack *)rCharList->SetToHeadAndReturn();
        while(rOptionPack)
        {
            //--If the option has "REMOVE" for its response, remove it.
            if(!strcasecmp(rOptionPack->mFiringCode, "REMOVE"))
            {
                rCharList->RemoveRandomPointerEntry();
            }

            //--Next.
            rOptionPack = (CEventOptionPack *)rCharList->IncrementAndGetRandomPointerEntry();
        }

        //--Next.
        rCharList = (StarLinkedList *)mCharacterList->AutoIterate();
    }
}

///======================================= Core Methods ===========================================
void CarnUIEvent::RefreshMenuHelp()
{
}
void CarnUIEvent::RecomputeCursorPositions()
{
}
void CarnUIEvent::SetCharacterCursor(int pCursor)
{
    ///--[Documentation]
    //--Sets the character cursor according to the active party. Queries HP/SP/etc for display.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    if(!rAdvCombat) return;

    //--If the member fails to resolve, set the cursor to 0. If there's nobody as position zero, stop.
    AdvCombatEntity *rEntity = rAdvCombat->GetActiveMemberI(pCursor);
    if(!rEntity)
    {
        //--Non-zero, set to zero.
        if(pCursor != 0)
        {
            SetCharacterCursor(0);
            return;
        }

        //--Already zero, just fail.
        return;
    }

    ///--[Set]
    mCharacterCursor = pCursor;
    rCharacterInternalName = rEntity->GetName();
    rCharacterName = rEntity->GetDisplayName();
    mCharacterHP = rEntity->GetHealth();
    mCharacterHPMax = rEntity->GetStatistic(STATS_HPMAX);
    mCharacterSP = rEntity->GetMagic();
    mCharacterSPMax = rEntity->GetStatistic(STATS_MPMAX);
    mCharacterInf = rEntity->GetInfluence();
    mCharacterInfMax = rEntity->GetInfluenceMax();

    //--Clamp. Max values must always be at least 1 to prevent div0 errors.
    if(mCharacterHPMax < 1) mCharacterHPMax = 1;
    if(mCharacterSPMax < 1) mCharacterSPMax = 1;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIEvent::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ========== Documentation =========== ]
    //--Handles updates when this is the active object.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[ ============== Timers ============== ]
    //--Immediately cap the vis timer.
    mVisibilityTimer = mVisibilityTimerMax;

    //--Remove expunged entries.
    RecheckEvents();

    ///--[ ============== Update ============== ]
    ///--[Mouse Over]
    //--If the mouse moves, recheck hitboxes.
    if(GetMouseInfo())
    {
        //--Run across the hitboxes. If the options hitboxes were hit, set the option cursor. It
        //  otherwise defaults to CARNUI_EVENT_NO_OPTION.
        RunHitboxCheck();
        SetOptionCursor(CARNUI_EVENT_NO_OPTION);

        //--Iterate across the reply packs.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--If it's in the options range:
            if(rReplyPack->mIndex >= CARNUI_EVENT_HITBOX_OPTION_START) SetOptionCursor(rReplyPack->mIndex - CARNUI_EVENT_HITBOX_OPTION_START);

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }
    }

    ///--[Left Click]
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Run across the hitboxes. Reply to the first hitbox hit.
        bool tPlaySFX = false;
        RunHitboxCheck();

        //--Iterate across the reply packs.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--Arrow left.
            if(rReplyPack->mIndex == CARNUI_EVENT_HITBOX_ARROWLFT)
            {
                tPlaySFX = true;
                mCharacterCursor = mCharacterCursor - 1;
                if(mCharacterCursor < 0) mCharacterCursor = AdvCombat::Fetch()->GetActivePartyCount() - 1;
                SetCharacterCursor(mCharacterCursor);
            }
            //--Arrow right.
            else if(rReplyPack->mIndex == CARNUI_EVENT_HITBOX_ARROWRGT)
            {
                tPlaySFX = true;
                mCharacterCursor = mCharacterCursor + 1;
                if(mCharacterCursor >= AdvCombat::Fetch()->GetActivePartyCount()) mCharacterCursor = 0;
                SetCharacterCursor(mCharacterCursor);
            }
            //--Reveal.
            else if(rReplyPack->mIndex == CARNUI_EVENT_HITBOX_REVEAL)
            {
                //--Can reveal. Don't check inventory counts of items, just call the script. It will handle the rest.
                //  This includes handling SFX.
                if(mCanReveal && mRevealScriptPath)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(mRevealScriptPath, 2, "S", "Reveal", "N", (float)mInstanceNumber);
                }
            }
            //--Options.
            else
            {
                //--Locate the active character list.
                StarLinkedList *rActiveOptionList = (StarLinkedList *)mCharacterList->GetElementByName(rCharacterInternalName);
                if(rActiveOptionList)
                {
                    //--Get the option pack by the clicked hitbox.
                    CEventOptionPack *rOptionPack = (CEventOptionPack *)rActiveOptionList->GetElementBySlot(rReplyPack->mIndex - CARNUI_EVENT_HITBOX_OPTION_START);
                    if(rOptionPack)
                    {
                        tPlaySFX = true;
                        LuaManager::Fetch()->ExecuteLuaFile(rOptionPack->mScript, 3, "S", rOptionPack->mFiringCode, "N", (float)mInstanceNumber, "S", rCharacterInternalName);
                    }
                }
            }

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }

        //--Sound, if flagged.
        if(tPlaySFX) AudioManager::Fetch()->PlaySound("Menu|Select");

        //--Stop update.
        return true;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Exits the UI. Disabled for now as player should be forced to pick an option,
    //  which can include to exit if the script allows it.
    /*if(rControlManager->IsFirstPress("MouseRgt"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }*/

    ///--[Exited Normally]
    return true;
}
void CarnUIEvent::UpdateBackground()
{
    ///--[ ============== Timers ============== ]
    //--Immediately cap the vis timer.
    mVisibilityTimer = 0;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIEvent::RenderPieces(float pAlpha)
{
    ///--[Documenation]
    //--Carnation does not use by-piece rendering. This is the main rendering call.
    if(mVisibilityTimer < 1) return;

    //--Remove expunged elements.
    RecheckEvents();

    ///--[Darkening]
    //--Base UI does not render a backing in this mode. This object handles it.
    float tBackingPct = (float)mVisibilityTimer / (float)mVisibilityTimerMax;
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * tBackingPct));

    ///--[Static Parts]
    //--Common parts that always render.
    CarnImages.rFrame_Base->Draw();
    CarnImages.rOverlay_MeterFrames->Draw();
    CarnImages.rOverlay_PortraitBack->Draw();

    ///--[Character]
    //--Retrieve the character in the active slot. If AdvCombat fails to resolve, something really
    //  bad happened so stop. The entity must be a CarnationCombatEntity to render correctly.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    if(!rAdvCombat) return;
    AdvCombatEntity *rEntity = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(rEntity && rEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
    {
        CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rEntity;
        rCarnationEntity->RenderSwitchableAt(195, 106);
    }

    //--Name, HP, etc.
    CarnImages.rFont_Name->DrawText(325.0f, 381.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rCharacterName);

    //--If there is more than one character in the active party, render selection arrows.
    if(rAdvCombat->GetActivePartyCount() > 1)
    {
        CarnImages.rOverlay_ArrowLft->Draw();
        CarnImages.rOverlay_ArrowRgt->Draw();
    }

    //--Render HP value and bars.
    float cHPPct = mCharacterHP / (float)mCharacterHPMax;
    CarnImages.rFont_Stats->DrawTextArgs(473.0f, 163.0f, 0, 1.0f, "%i / %i", mCharacterHP, mCharacterHPMax);
    CarnImages.rOverlay_HPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cHPPct, 1.0f);

    //--Render SP value and bars.
    float cSPPct = mCharacterSP / (float)mCharacterSPMax;
    CarnImages.rFont_Stats->DrawTextArgs(473.0f, 222.0f, 0, 1.0f, "%i / %i", mCharacterSP, mCharacterSPMax);
    CarnImages.rOverlay_SPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cSPPct, 1.0f);

    //--Influence values.
    CarnImages.rFont_Stats->DrawTextArgs(441.0f, 299.0f, 0, 1.0f, "Influence: %i / %i", mCharacterInf, mCharacterInfMax);

    ///--[Event Description]
    //--Event name.
    CarnImages.rFont_Name->DrawTextArgs(437.0f, 92.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mEventName);

    //--The description linked list contains strings to render.
    if(mEventDescription)
    {
        //--Setup.
        float cYHei =  30.0f;
        float tYPos = 434.0f;

        //--Render.
        char *rEventDescriptionString = (char *)mEventDescription->PushIterator();
        while(rEventDescriptionString)
        {
            //--Render.
            CarnImages.rFont_Description->DrawTextArgs(228.0f, tYPos, 0, 1.0f, rEventDescriptionString);

            //--Next.
            tYPos = tYPos + cYHei;
            rEventDescriptionString = (char *)mEventDescription->AutoIterate();
        }
    }

    ///--[Options]
    //--Locate the active character list.
    CEventOptionPack *rHighlightedOption = NULL;
    StarLinkedList *rActiveOptionList = (StarLinkedList *)mCharacterList->GetElementByName(rCharacterInternalName);
    if(rActiveOptionList)
    {
        //--For each option, render a button and text on top of the button.
        float cOptionOffsetCur  =   0.0f;
        float cOptionOffsetY    =  60.0f;
        float cOptionTxtOffsetY = 132.0f;
        CEventOptionPack *rOptionPack = (CEventOptionPack *)rActiveOptionList->PushIterator();
        while(rOptionPack)
        {
            //--Backing.
            CarnImages.rFrame_Option->Draw(0.0f, cOptionOffsetCur);

            //--Text.
            CarnImages.rFont_Button->DrawTextArgs(723.0f, cOptionOffsetCur + cOptionTxtOffsetY, 0, 1.0f, rActiveOptionList->GetIteratorName());

            //--Next.
            cOptionOffsetCur = cOptionOffsetCur + cOptionOffsetY;
            rOptionPack = (CEventOptionPack *)rActiveOptionList->AutoIterate();
        }

        //--Store the selection option.
        rHighlightedOption = (CEventOptionPack *)rActiveOptionList->GetElementBySlot(mOptionCursor);
    }

    ///--[Option Description]
    //--Get the selected description. If it does not exist, render nothing.
    if(rHighlightedOption)
    {
        //--Setup.
        float cYHei =  30.0f;
        float tYPos = 434.0f;

        //--Not revealed mode.
        if((!mIsRevealed && rHighlightedOption->mDescription) || (mIsRevealed && !rHighlightedOption->mRevealed && rHighlightedOption->mDescription))
        {
            char *rOptionDescriptionString = (char *)rHighlightedOption->mDescription->PushIterator();
            while(rOptionDescriptionString)
            {
                //--Render.
                CarnImages.rFont_Description->DrawTextArgs(704.0f, tYPos, 0, 1.0f, rOptionDescriptionString);

                //--Next.
                tYPos = tYPos + cYHei;
                rOptionDescriptionString = (char *)rHighlightedOption->mDescription->AutoIterate();
            }
        }
        //--Revealed mode. Must have a revealed variant, otherwise the code above uses the base description.
        else if(mIsRevealed && rHighlightedOption->mRevealed)
        {
            char *rOptionDescriptionString = (char *)rHighlightedOption->mRevealed->PushIterator();
            while(rOptionDescriptionString)
            {
                //--Render.
                CarnImages.rFont_Description->DrawTextArgs(704.0f, tYPos, 0, 1.0f, rOptionDescriptionString);

                //--Next.
                tYPos = tYPos + cYHei;
                rOptionDescriptionString = (char *)rHighlightedOption->mRevealed->AutoIterate();
            }
        }
    }

    ///--[Reveal Button]
    //--Appears in the bottom right if the event can have a reveal action occur. Also shows how many
    //  of the item the player has. If the player has none, the button shows x0 but can still be clicked
    //  though the script will likely do nothing.
    if(mCanReveal)
    {
        //--Button.
        CarnImages.rOverlay_RevealButton->Draw();

        //--Render how many of "Opaque Lens" are in the inventory.
        int tLensCount = AdventureInventory::Fetch()->GetCountOf("Opaque Lens");
        CarnImages.rFont_Stats->DrawTextArgs(757.0f, 680.0f, 0, 1.0f, "Lens: %i", tLensCount);
    }
    //--No button, show the reveal string. This can be set to " " to hide the string.
    else if(mRevealString)
    {
        CarnImages.rFont_Stats->DrawTextArgs(700.0f, 680.0f, 0, 1.0f, mRevealString);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void CarnUIEvent::HookToLuaState(lua_State *pLuaState)
{
    /* [System]
       CarnUIEvent_SetProperty("Open Event Mode")
       Sets the requested property in the CarnUIEvent. */
    lua_register(pLuaState, "CarnUIEvent_SetProperty", &Hook_CarnUIEvent_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnUIEvent_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    ///--[Dynamic]
    //--[System]
    //CarnUIEvent_SetProperty("Set Instance Number", iNumber)
    //CarnUIEvent_SetProperty("Set Event Name", sName)
    //CarnUIEvent_SetProperty("Set Event Description", sDescription)
    //CarnUIEvent_SetProperty("External Close")

    //--[Reveal]
    //CarnUIEvent_SetProperty("Set Is Revealed", bIsRevealed)
    //CarnUIEvent_SetProperty("Set Can Reveal", bCanReveal)
    //CarnUIEvent_SetProperty("Set Reveal Path", sPath)
    //CarnUIEvent_SetProperty("Set Reveal String", sString)

    //--[Options]
    //CarnUIEvent_SetProperty("Clear Options")
    //CarnUIEvent_SetProperty("Register Option Character", sCharacterName)
    //CarnUIEvent_SetProperty("Register Option To Character", sCharacterName, sOptionName, sScriptPath, sFiringCode, sDescription)
    //CarnUIEvent_SetProperty("Set Option Reveal Text", sCharacterName, sOptionName, sRevealText)

    ///--[ =========== Arg Resolve ============ ]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnUIEvent_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Statics.
    //if(!strcasecmp(rSwitchType, "Party Resolve Script") && tArgs == 2)
    //{
    //    ResetString(AdventureMenu::xChatResolveScript, lua_tostring(L, 2));
    //    return 0;
    //}

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("CarnUIEvent_SetProperty", L);

    //--Get the sub-object.
    CarnUIEvent *rUIObject = dynamic_cast<CarnUIEvent *>(rMenu->RetrieveUI("Event", POINTER_TYPE_CARNUIEVENT, NULL, "CarnUIEvent_SetProperty"));
    if(!rUIObject) return LuaTypeError("CarnUIEvent_SetProperty", L);

    ///--[System]
    //--Name that appears at the top of the UI.
    if(!strcasecmp(rSwitchType, "Set Instance Number") && tArgs >= 2)
    {
        rUIObject->SetInstanceNumber(lua_tointeger(L, 2));
    }
    //--Name that appears at the top of the UI.
    else if(!strcasecmp(rSwitchType, "Set Event Name") && tArgs >= 2)
    {
        rUIObject->SetEventName(lua_tostring(L, 2));
    }
    //--Fixed description of the event.
    else if(!strcasecmp(rSwitchType, "Set Event Description") && tArgs >= 2)
    {
        rUIObject->SetEventDescription(lua_tostring(L, 2));
    }
    //--Close the UI from script.
    else if(!strcasecmp(rSwitchType, "External Close") && tArgs >= 1)
    {
        rUIObject->FlagExit();
    }
    ///--[Reveal]
    //--If true, shows the results strings on the UI.
    else if(!strcasecmp(rSwitchType, "Set Is Revealed") && tArgs >= 2)
    {
        rUIObject->SetIsRevealed(lua_toboolean(L, 2));
    }
    //--If true, player can use an item to reveal event outcomes.
    else if(!strcasecmp(rSwitchType, "Set Can Reveal") && tArgs >= 2)
    {
        rUIObject->SetCanReveal(lua_toboolean(L, 2));
    }
    //--Path called to handle the player using a reveal item.
    else if(!strcasecmp(rSwitchType, "Set Reveal Path") && tArgs >= 2)
    {
        rUIObject->SetRevealPath(lua_tostring(L, 2));
    }
    //--String that appears on the UI if the reveal icon is not present.
    else if(!strcasecmp(rSwitchType, "Set Reveal String") && tArgs >= 2)
    {
        rUIObject->SetRevealString(lua_tostring(L, 2));
    }
    ///--[Options]
    //--Clears all option data, including characters.
    else if(!strcasecmp(rSwitchType, "Clear Options") && tArgs >= 1)
    {
        rUIObject->ClearOptions();
    }
    //--Registers a character who can act in the current event.
    else if(!strcasecmp(rSwitchType, "Register Option Character") && tArgs >= 2)
    {
        rUIObject->RegisterCharacter(lua_tostring(L, 2));
    }
    //--Register an option to the given character, firing the given script if selected.
    else if(!strcasecmp(rSwitchType, "Register Option To Character") && tArgs >= 6)
    {
        rUIObject->RegisterOption(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
    }
    //--Set option reveal text. If not used, the default description is used.
    else if(!strcasecmp(rSwitchType, "Set Option Reveal Text") && tArgs >= 4)
    {
        rUIObject->SetOptionRevealText(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnUIEvent_SetProperty", rSwitchType, tArgs);
    }

    //--Success.
    return 0;
}
