//--Base
#include "CarnationCombat.h"

//--Classes
#include "AdventureLevel.h"
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"
#include "CarnUICommon.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Debug]
//#define CARNATIONCOMBAT_RENDER_DEBUG
#ifdef CARNATIONCOMBAT_RENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================= Base Call ============================================
void CarnationCombat::Render(bool pBypassResolution, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the Adventure Combat interface. This uses an alpha mixer, but this is only used
    //  during the resolution sequences.
    if(!Images.mIsReady || !CarnationImg.mIsReady || !mIsActive || pAlpha <= 0.0f) return;
    DebugPush(true, "Combat Render: Begin.\n");

    //--If the world is still pulsing, don't render.
    DebugPrint("Checking Adventure Level.\n");
    //AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    //if(rActiveLevel && rActiveLevel->IsPulsing()) { DebugPop("Level pulsing, exited normally.\n"); return; }

    ///--[List Building]
    //--Rebuild effect references. It is possible an effect was added/removed since the last time the update ticked.
    DebugPrint("Rebuilding effect references.\n");
    RebuildEffectReferences();

    ///--[Subhandlers]
    //--Subhandlers can take over rendering. First, combat is over:
    DebugPrint("Checking resolution/introduction states.\n");
    if(mCombatResolution != ADVCOMBAT_END_NONE && !pBypassResolution) { RenderResolution();   DebugPop("Resolution running, exited normally.\n");   return; }
    if(mIsIntroduction)
    {
        //StarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);
        DebugPop("Introduction running, exited normally.\n");
        return;
    }

    //--Anything below this point is "Normal" combat rendering.

    ///--[Backgrounds]
    //--If present, these render under the action.
    if(mBackgroundTimer > CC_BG_FADEOUT_STOP)
    {
        //--Effective timer.
        int tEffectiveTimer = mBackgroundTimer - CC_BG_FADEOUT_STOP;

        //--Iterate across all layers.
        CarnCombatBGLayer *rBGLayer = (CarnCombatBGLayer *)mBackgroundList->PushIterator();
        while(rBGLayer)
        {
            //--Render.
            if(!rBGLayer->rImage) { rBGLayer = (CarnCombatBGLayer *)mBackgroundList->AutoIterate(); continue; }

            //--Basic rendering.
            if(rBGLayer->mLogicType == CC_BG_LOGIC_NONE)
            {
                rBGLayer->rImage->Draw();
            }
            //--Scroll in from the left.
            else if(rBGLayer->mLogicType == CC_BG_LOGIC_FROMLEFT)
            {
                float tPct = (tEffectiveTimer * rBGLayer->mLogicFactor);
                if(tPct > 1.0f) tPct = 1.0f;
                float tXPos = rBGLayer->rImage->GetTrueWidth() * (1.0f - tPct) * -1.0f;
                rBGLayer->rImage->Draw(tXPos, 0.0f);
            }
            //--Scroll in from the right.
            else if(rBGLayer->mLogicType == CC_BG_LOGIC_FROMRIGHT)
            {
                float tPct = (tEffectiveTimer * rBGLayer->mLogicFactor);
                if(tPct > 1.0f) tPct = 1.0f;
                float tXPos = rBGLayer->rImage->GetTrueWidth() * (1.0f - tPct);
                rBGLayer->rImage->Draw(tXPos, 0.0f);
            }

            //--Next.
            rBGLayer = (CarnCombatBGLayer *)mBackgroundList->AutoIterate();
        }
    }

    //--Black borders.
    DebugPrint("Backing.\n");
    //if(!pBypassResolution) StarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);
    float cBorderPct = EasingFunction::QuadraticInOut(mLetterboxTimer, CC_BG_LETTERBOX_TICKS);
    if(cBorderPct > 0.0f)
    {
        //--Setup.
        glDisable(GL_TEXTURE_2D);
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 0.80f);

        //--Top bar.
        float cLft = 0.0f;
        float cTop = 0.0f;
        float cRgt = VIRTUAL_CANVAS_X;
        float cBot = 108.0f * cBorderPct;
        glBegin(GL_QUADS);
            glVertex2f(cLft, cTop);
            glVertex2f(cRgt, cTop);
            glVertex2f(cRgt, cBot);
            glVertex2f(cLft, cBot);
        glEnd();

        //--Bottom bar.
        cTop = VIRTUAL_CANVAS_Y - (268.0f * cBorderPct);
        cBot = VIRTUAL_CANVAS_Y;
        glBegin(GL_QUADS);
            glVertex2f(cLft, cTop);
            glVertex2f(cRgt, cTop);
            glVertex2f(cRgt, cBot);
            glVertex2f(cLft, cBot);
        glEnd();

        //--Clean.
        StarlightColor::ClearMixer();
        glEnable(GL_TEXTURE_2D);
    }

    ///--[Entities]
    //--Render all entities. Player party renders above enemy party.
    RenderEnemyParty();
    RenderPlayerParty();

    //--Commands. If handling an effortless action, hide this.
    if(mPartyDecisionMode && !mIsHandlingEffortlessAction)
    {
        AdvCombatEntity *rPartyEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(mPartyDecisionIndex);
        if(rPartyEntity) RenderCommandWindow(rPartyEntity, pAlpha);
        RenderTargetWindow(pAlpha);
        RenderSkillDescriptionWindow(pAlpha);
    }

    ///--[UI]
    //RenderAllyBars(pAlpha);
    //RenderTurnOrder(pAlpha);

    //--Render animations.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    DebugPrint("Rendering combat animations.\n");
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->PushIterator();
    while(rAnimation)
    {
        rAnimation->Render();
        rAnimation = (AdvCombatAnimation *)mCombatAnimations->AutoIterate();
    }

    //--Text package.
    DebugPrint("Rendering text packages.\n");
    float cCombatYDrift = -30.0f;
    CombatTextPack *rTextPack = (CombatTextPack *)mTextPackages->PushIterator();
    while(rTextPack)
    {
        //--Compute Y position.
        float cPercent = EasingFunction::QuadraticOut(rTextPack->mTicks, rTextPack->mTicksMax);
        float cYUsePos = rTextPack->mY + (cCombatYDrift * cPercent);

        //--Render.
        rTextPack->mColor.SetAsMixer();
        Images.Data.rCombatTextFont->DrawText(rTextPack->mX, cYUsePos, SUGARFONT_AUTOCENTER_XY, rTextPack->mScale, rTextPack->mText);

        //--Next.
        rTextPack = (CombatTextPack *)mTextPackages->AutoIterate();
    }
    StarlightColor::ClearMixer();

    //--Prediction boxes.
    DebugPrint("Rendering prediction boxes.\n");
    //RenderPredictionBoxes();

    //--Titles.
    DebugPrint("Rendering titles.\n");
    //RenderTitle();

    //--Text box. Does not render when selection actions, but can be overridden by effortless actions.
    if(!mPartyDecisionMode || mIsHandlingEffortlessAction)
    {
        RenderTextWindow();
    }
    RenderNewTurn(pAlpha);
    //RenderTargetCluster();

    ///--[Overlays]
    //--Dialogue, if present.
    DebugPrint("Rendering world dialogue.\n");
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible())
    {
        rWorldDialogue->Render();
    }

    //--Combat inspector.
    DebugPrint("Rendering inspector.\n");
    RenderInspector();

    //--Confirmation window overlay.
    DebugPrint("Rendering confirmation window.\n");
    RenderConfirmation();
    DebugPop("Exited normally.\n");

    ///--[Blackout Snap]
    //--Occurs shortly after battle begins to allow the background to appear.
    if(mBackgroundTimer > CC_BG_FADEOUT_START && mBackgroundTimer < CC_BG_FADEOUT_STOP)
    {
        //float cPct = EasingFunction::QuadraticInOut(mBackgroundTimer-CC_BG_FADEOUT_START, (CC_BG_FADEOUT_STOP - CC_BG_FADEOUT_START));
        //StarBitmap::DrawFullBlack(cPct);
    }
    else if(mBackgroundTimer >= CC_BG_FADEOUT_STOP && mBackgroundTimer < CC_BG_FADEIN_STOP)
    {
        //float cPct = EasingFunction::QuadraticInOut(mBackgroundTimer-CC_BG_FADEOUT_STOP, (CC_BG_FADEIN_STOP - CC_BG_FADEOUT_STOP));
        //StarBitmap::DrawFullBlack(1.0f - cPct);
    }
}

///====================================== Party Rendering =========================================
void CarnationCombat::RenderPlayerParty()
{
    ///--[Documentation and Setup]
    //--Renders the player party members. During decision mode, the acting character moves up slightly.
    float cSpacingX = 222.0f;
    float tRenderX  = (VIRTUAL_CANVAS_X * 0.50f) - cSpacingX - (99);
    float cRenderY  = 470.0f;

    //--Fast-access pointers.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

    ///--[X Position]
    //--Reposition based on the size of the party.
    if(mrCombatParty->GetListSize() == 1)
    {
        tRenderX = tRenderX + cSpacingX;
    }
    else if(mrCombatParty->GetListSize() == 2)
    {
        tRenderX = tRenderX + (cSpacingX / 2);
    }

    ///--[Iterate]
    AdvCombatEntity *rPartyEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyEntity)
    {
        //--Compute Y position. If the character is acting right now, then force the position to max advance.
        float cPct = EasingFunction::Linear(rPartyEntity->GetCommandTimer(), CC_ACTIONSELECT_TICKS);
        if(rActingEntity == rPartyEntity && !mPartyDecisionMode && !mStatusPrintBlock) cPct = 1.0f;

        //--Resolution, force to zero.
        if(mCombatResolution != ADVCOMBAT_END_NONE) cPct = 0.0f;

        //--Compute.
        float tRenderY = cRenderY + (cPct * CC_ACTIONSELECT_DISTANCE);

        //--If the entity is not a CarnationCombatEntity, do nothing:
        if(rPartyEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
        {
            CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rPartyEntity;
            rCarnationEntity->RenderPortraitAt(tRenderX, tRenderY, false);
            rCarnationEntity->SetIdealPosition(tRenderX, tRenderY);
            rCarnationEntity->MoveToIdealPosition(0);
        }

        //--Next.
        tRenderX = tRenderX + cSpacingX;
        rPartyEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
}
void CarnationCombat::RenderEnemyParty()
{
    ///--[Documentation and Setup]
    //--Renders the enemy party members. Entities are implied to render at height 0. The exact positions
    //  of each enemy are based on the total number of enemies, to better allow centering.
    int tTotalEnemies = mrEnemyCombatParty->GetListSize();

    ///--[Positions]
    //--Variables.
    float cLftMost = 0.50f;             //The leftmost enemy position in terms of virtual canvas percentage. Minimum is 0.10f.
    float cSpacing = 0.20f;             //Distance between each entry. Optimal is 0.20f, but this shrinks if there's a lot of enemies.

    //--Calculate the leftmost position. It is the middle of the screen minus 0.10f for each enemy added past the first.
    //  It clamps at 0.10f, at which point the enemies start packing together.
    cLftMost = 0.60f - (float)(tTotalEnemies * 0.10f);
    if(cLftMost < 0.10f) cLftMost = 0.10f;

    //--Calculate the distance between each enemy. This only occurs if there are 6 or more enemies on screen at once,
    //  otherwise the program uses the optimal spacing of 0.20f. At 5 or fewer enemies there is enough space. There is
    //  0.90f - 0.10f = 0.80f space for the enemies. There is no particular minimum spacing because, let's face it, the
    //  game is going to look bad if there are a ton of enemies on screen at once no matter what happens.
    if(tTotalEnemies >= 6)
    {
        cSpacing = 0.80f / (float)(tTotalEnemies - 1);
        if(cSpacing < 0.03f) cSpacing = 0.03f;
    }

    ///--[Render Loop]
    int tCount = 0;
    AdvCombatEntity *rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyEntity)
    {
        //--Resolve position. If the enemy count is odd:
        float cRenderX = (cLftMost + (float)(tCount * cSpacing)) * VIRTUAL_CANVAS_X;

        //--Resolve the target flash timer. If not targeting, or the entity is not in the current target
        //  cluster, it defaults to 0.
        int tUseTargetTimer = 0;
        if(mIsSelectingTargets && IsEntityInActiveTargetCluster(rEnemyEntity))
        {
            tUseTargetTimer = mTargetFlashTimer;
        }

        //--If this entity is a CarnationCombatEntity (it should be), use its internal renderer.
        if(rEnemyEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
        {
            CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rEnemyEntity;
            rCarnationEntity->RenderAsEnemy(cRenderX, 0.0f, tUseTargetTimer);
        }
        //--Otherwise, use the standard renderer.
        else
        {
            StarBitmap *rImg = rEnemyEntity->GetCombatPortrait();
            if(rImg)
            {
                rImg->Draw(cRenderX - (rImg->GetTrueWidth() * 0.50f), 0.0f);
            }
        }

        //--Next.
        tCount ++;
        rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
}

///===================================== UI Part Rendering ========================================
void CarnationCombat::RenderAllyBars(float pAlpha)
{
    ///--[Documentation]
    //--Ally bars don't render in the top left, they render as the HP/MP UI for the party.
    float cSpacingX = 222.0f;
    float tRenderX  = (VIRTUAL_CANVAS_X * 0.50f) - cSpacingX - (99);
    float cRenderYHP  = 700.0f;
    float cRenderYMP  = 721.0f;

    //--Reposition based on the size of the party.
    if(mrCombatParty->GetListSize() == 1)
    {
        tRenderX = tRenderX + cSpacingX;
    }
    else if(mrCombatParty->GetListSize() == 2)
    {
        tRenderX = tRenderX + (cSpacingX / 2);
    }

    //--Iterate.
    AdvCombatEntity *rPartyEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyEntity)
    {
        //--Position.
        float cXPos = tRenderX - 3;

        //--Backings.
        CarnationImg.Image.rHPBarBack->Draw(cXPos, cRenderYHP);
        CarnationImg.Image.rMPBarBack->Draw(cXPos, cRenderYMP);

        //--Fill.
        float cHPPct = rPartyEntity->GetHealthPercent();
        float cMPPct = rPartyEntity->GetMagicPercent();
        glTranslatef(cXPos, cRenderYHP, 0.0f);
        StarBitmap::DrawHorizontalPercent(CarnationImg.Image.rHPBarFill, cHPPct);
        glTranslatef(0.0f, cRenderYMP-cRenderYHP, 0.0f);
        StarBitmap::DrawHorizontalPercent(CarnationImg.Image.rMPBarFill, cMPPct);
        glTranslatef(-cXPos, -cRenderYMP, 0.0f);

        //--Next.
        tRenderX = tRenderX + cSpacingX;
        rPartyEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
}
void CarnationCombat::RenderNewTurn(float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the "TURN X" at the top of the screen. Does nothing if the time has expired.
    //--Identical to the original, but renders in a different position so the text box doesn't overlay it.
    if(mTurnDisplayTimer >= ACE_TURN_DISPLAY_TICKS || !Images.mIsReady || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Timer setup.
    int cLetterTicks = ACE_TURN_TICKS_PER_LETTER * 5;
    int cHoldTicks = cLetterTicks + ACE_TURN_TICKS_HOLD;

    //--Populate the buffer. Note that turn 0 renders as "TURN 1". We never pass "TURN 1000", it just stays there
    //  to prevent overflow. The actual turn counter continues to rise.
    int tUseTurns = mCurrentTurn+1;
    if(tUseTurns > 1000) tUseTurns = 1000;
    char tFullBuffer[10];
    sprintf(tFullBuffer, "TURN %i", tUseTurns);

    //--Positions
    float cXStart = (VIRTUAL_CANVAS_X * 0.50f) - (Images.Data.rNewTurnFont->GetTextWidth(tFullBuffer) * 0.50f);
    float cYStart = VIRTUAL_CANVAS_Y * -0.10f;
    float cYEnd   = VIRTUAL_CANVAS_Y *  0.01f;

    //--If we're in the displaying new letters part:
    if(mTurnDisplayTimer < cLetterTicks)
    {
        //--For each letter in "TURN"
        float tXPos = cXStart;
        int tUseTicks = mTurnDisplayTimer;
        for(int i = 0; i < 4; i ++)
        {
            //--Percentage drop. The letter starts at the top of the screen and moves down.
            float cPercent = EasingFunction::QuadraticOut(tUseTicks, ACE_TURN_TICKS_PER_LETTER);
            float cYPos = cYStart + ((cYEnd - cYStart) * cPercent);

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent * pAlpha);
            float tIncrement = Images.Data.rNewTurnFont->DrawLetter(tXPos, cYPos, 0, 1.0f, tFullBuffer[i], tFullBuffer[i+1]);
            tXPos = tXPos + tIncrement;

            //--Clean.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

            //--Next. End if the letter should not display.
            tUseTicks -= ACE_TURN_TICKS_PER_LETTER;
            if(tUseTicks <= 0) break;
        }

        //--Render the turn number if there are ticks for it.
        if(tUseTicks > 0)
        {
            //--Increment by the distance of a space.
            float cPercent = EasingFunction::QuadraticOut(tUseTicks, ACE_TURN_TICKS_PER_LETTER);
            float cYPos = cYStart + ((cYEnd - cYStart) * cPercent);
            float tIncrement = Images.Data.rNewTurnFont->DrawLetter(tXPos, cYPos, 0, 1.0f, ' ', tFullBuffer[5]);
            tXPos = tXPos + tIncrement;

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent * pAlpha);
            Images.Data.rNewTurnFont->DrawText(tXPos, cYPos, 0, 1.0f, &tFullBuffer[6]);

            //--Clean.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }
    }
    //--If we're in the "Hold" part:
    else if(mTurnDisplayTimer < cHoldTicks)
    {
        Images.Data.rNewTurnFont->DrawText(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
    }
    //--If we're in the "Fade" part:
    else
    {
        float cAlpha = 1.0f - EasingFunction::QuadraticOut(mTurnDisplayTimer - cHoldTicks, ACE_TURN_TICKS_FADE);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pAlpha);
        Images.Data.rNewTurnFont->DrawText(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }
}
void CarnationCombat::RenderTextWindow()
{
    ///--[Documentation]
    //--Renders the text crawl window that describes what is happening.
    if(mTextStrings->GetListSize() < 1) return;

    //--Size of each line.
    float cEdgePad = 8.0f;
    float cLineH = CarnationImg.Font.rTextBox->GetTextHeight() + 0.0f;

    //--Size of the box itself.
    float cWid = CC_TEXTBOX_WID;
    float cHei = (cLineH * 3.0f) + (cEdgePad * 2.0f);

    //--Position.
    float cLft = (VIRTUAL_CANVAS_X * 0.50f) - (cWid * 0.50f);
    float cRgt = cLft + cWid;
    float cTop = (VIRTUAL_CANVAS_Y * 0.01f);
    float cBot = cTop + cHei;

    //--Tracking.
    float tXCur = cLft + cEdgePad + 3;
    float tYCur = cTop + cEdgePad - 3.0f;
    float tYMinimum = tYCur;
    float tXMinimum = tXCur;

    //--Move all the lines up such that only the bottom three lines are rendered.
    if(mHighestLineRendered >= 2)
    {
        tYCur = tYCur - (((mHighestLineRendered - 2) * cLineH)) + 1;
    }

    ///--[Backing]
    //--Render a backing behind the text window.
    float cCornerSize = 16.0f;
    CarnationImg.Image.rTextFrame->RenderNine(cLft, cLft + cCornerSize, cRgt - cCornerSize, cRgt, cTop, cTop + cCornerSize, cBot - cCornerSize, cBot,
                                              0.08f, 0.92f, 0.07f, 0.93f);

    ///--[Letter Rendering]
    //--Get the starting string.
    int tCurLine = 0;
    int tCurLetter = 0;
    CarnationString *rString = (CarnationString *)mTextStrings->GetElementBySlot(0);
    if(!rString) return;

    //--The number of letters to render is based on mTextTimer divided by mLettersPerTick.
    int tLettersToRender = (int)(mTextTimer / mLettersPerTick);

    //--Debug.
    if(false)
    {
        CarnationImg.Font.rTextBox->DrawTextArgs(0,  0, 0, 1.0f, "Letters: %i / %i", tLettersToRender, (int)(mTextTimerMax / mLettersPerTick));
        CarnationImg.Font.rTextBox->DrawTextArgs(0, 25, 0, 1.0f, "Lines: %i / %i", mHighestLineRendered, mTextStrings->GetListSize()-1);
    }

    //--Rendering.
    for(int i = 0; i < tLettersToRender; i ++)
    {
        //--Reset the line cursor if needed.
        if(tCurLine > mHighestLineRendered)
        {
            mHighestLineRendered = tCurLine;
            break;
        }

        //--Get the ith letter.
        uint32_t tThisLetter = rString->mLetters[tCurLetter+0]->GetLetter();
        uint32_t tNextLetter = rString->mLetters[tCurLetter+1]->GetLetter();
        tCurLetter ++;

        //--Skip rendering if we're above the minimum Y.
        if(tYCur >= tYMinimum)
        {
            //--Render it.
            float tAdvance = CarnationImg.Font.rTextBox->DrawLetter(tXCur, tYCur, 0, 1.0f, tThisLetter, tNextLetter);

            //--Move letters.
            tXCur = tXCur + tAdvance;
        }

        //--If the next letter is a null, advance the string.
        if(tNextLetter == '\0')
        {
            //--Retrieve.
            tCurLetter = 0;
            tCurLine ++;
            rString = (CarnationString *)mTextStrings->GetElementBySlot(tCurLine);

            //--Line carriage.
            tXCur = tXMinimum;
            tYCur = tYCur + cLineH;

            //--If the string doesn't exist, stop.
            if(!rString) break;
        }
    }
}
void CarnationCombat::RenderSkillDescriptionWindow(float pAlpha)
{
    ///--[Documentation]
    //--Renders the description of the highlighted skill. Can also be an item.
    if(pAlpha <= 0.0f) return;

    ///--[No Descriptions]
    //--Render this button in the bottom left when not showing descriptions.
    if(!mShowDescriptions)
    {
        CarnationImg.Image.rFrame_Button->Draw(mToggleDescriptionHitbox.mLft, mToggleDescriptionHitbox.mTop);
        CarnationImg.Font.rFontButton->DrawTextFixed(mToggleDescriptionHitbox.mXCenter, mToggleDescriptionHitbox.mYCenter, 134.0f, SUGARFONT_AUTOCENTER_XY, "Show Descriptions");
        return;
    }

    ///--[Show Descriptions]
    //--Button. Shows even if there is no description.
    CarnationImg.Image.rFrame_Button->Draw(mToggleDescriptionHitbox.mLft, mToggleDescriptionHitbox.mTop);
    CarnationImg.Font.rFontButton->DrawTextFixed(mToggleDescriptionHitbox.mXCenter, mToggleDescriptionHitbox.mYCenter, 134.0f, SUGARFONT_AUTOCENTER_XY, "Hide Descriptions");

    //--To render a description, it must exist even if enabled.
    if(!mDescription || !mDescriptionAbilityName) return;

    //--Render frame.
    CarnationImg.Image.rFrame_Description->Draw();

    //--Title.
    CarnationImg.Font.rFontDescriptionHeading->DrawText(178.0f, 531.0f, 0, 1.0f, mDescriptionAbilityName);

    //--Description.
    float cHei = CarnationImg.Font.rFontDescriptionMainline->GetTextHeight();
    CarnUICommon::RenderDescription(178.0f, 582.0f, cHei, mDescription, CarnationImg.Font.rFontDescriptionMainline);
}
void CarnationCombat::RenderConfirmation()
{
    ///--[Documentation]
    //--Renders the confirmation dialogue over the screen. Adjusted to use a white font.
    if(!Images.mIsReady || mConfirmationTimer < 1) return;

    //--Compute alpha.
    float cPct = EasingFunction::QuadraticInOut(mConfirmationTimer, ADVCOMBAT_CONFIRMATION_TICKS);

    //--Grey the screen out a bit.
    float cBacking = cPct * 0.80f;
    StarBitmap::DrawFullBlack(cBacking);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);

    //--Render the confirmation string in the middle of the screen.
    float cXPosition = VIRTUAL_CANVAS_X * 0.50f;
    float cYPosition = VIRTUAL_CANVAS_Y * 0.40f;
    mConfirmationString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, CarnationImg.Font.rFontConfirmBig);

    //--Render the accept string.
    cXPosition = VIRTUAL_CANVAS_X * 0.40f;
    cYPosition = VIRTUAL_CANVAS_Y * 0.50f;
    mAcceptString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, CarnationImg.Font.rFontConfirmSmall);

    //--Render the cancel string.
    cXPosition = VIRTUAL_CANVAS_X * 0.60f;
    cYPosition = VIRTUAL_CANVAS_Y * 0.50f;
    mCancelString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, CarnationImg.Font.rFontConfirmSmall);

    //--Clean.
    StarlightColor::ClearMixer();
}
