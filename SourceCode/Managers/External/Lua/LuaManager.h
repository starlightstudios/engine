///======================================== LuaManager ============================================
//--Stores the Lua state and provides an interface for using it, both in Lua scripts and in the
//  C++ program. Also manages the calling stack, arguments, and tarball construction.
#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[LuaPathRemapPack]
typedef struct LuaPathRemapPack
{
    char *mOriginalPath;
    char *mModifiedPath;
    void Initialize()
    {
        mOriginalPath = NULL;
        mModifiedPath = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        LuaPathRemapPack *rPack = (LuaPathRemapPack *)pPtr;
        free(rPack->mOriginalPath);
        free(rPack->mModifiedPath);
        free(rPack);
    }
}LuaPathRemapPack;

///--[Argument Pack]
//--Stores a set of arguments passed to a lua script.
typedef struct LuaArgumentPack
{
    //--Members
    int mArgumentsTotal;
    int mArgSlot;
    char **mArguments;

    //--Functions
    void Initialize()
    {
        mArgumentsTotal = 0;
        mArgSlot = 0;
        mArguments = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        LuaArgumentPack *rPack = (LuaArgumentPack *)pPtr;
        for(int i = 0; i < rPack->mArgumentsTotal; i ++) free(rPack->mArguments[i]);
        free(rPack->mArguments);
        free(rPack);
    }
}LuaArgumentPack;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class LuaManager
{
    private:
    ///--[Normal Variables]
    //--System
    static bool xIsFirstWriterCall;
    static bool xIsFirstReaderCall;
    static void *xDumpBuffer;

    //--Lua State
    lua_State *mLuaState;

    //--Passing arguments
    bool mClearArgs;
    int mTotalArgs;
    int mArgSlot;
    char **mScriptArguments;

    //--Call Stack
    StarLinkedList *mCallStack; //char *, master
    StarLinkedList *mArgsStack; //LuaArgumentPack *, master
    StarLinkedList *mScrExecStack; //bool *, master

    //--Alt-Directory Mode
    char *mCheckDirectoryPath;
    char *mAltDirectoryPath;

    //--Timer List
    StarLinkedList *mTimerList; //time_t *, master

    ///--[Translation]
    char *mStandardExtension;           //By default, is ".lua"
    char *mTranslateExtension;          //When using a translation, checks if mStandardExtension is found on the end of the script. If it is,
                                        //replaces it with mTranslateExtension and, if that file exists, executes it. If not, uses the default.
                                        //This allows translated files to execute in place of default files. This occurs for all executions,
                                        //including script mods!

    ///--[Tarballing Variables]
    //--Precompilation
    bool mUsePrecompiling;

    //--Tarball mode
    bool mIsTarballMode;
    int mTotalTarballScripts;
    LuaTarballEntry **mTarballScripts;

    ///--[Mod Remapping Variables]
    //--Primary
    bool mIsModRemappingActive;
    int mRemapPathsTotal;
    LuaPathRemapPack *mRemapPathList;

    //--Addendum
    StarLinkedList *mScriptPredendumList; //LuaPathRemapPack *, master
    StarLinkedList *mScriptAddendumList;  //LuaPathRemapPack *, master

    //--Temporary Build List
    StarLinkedList *mModRemapBuildList; //LuaPathRemapPack *, master

    public:
    //--Public Variables
    bool mWasLastActionInError;
    bool mAlwaysBarkResolveErrors;
    int mFiringCode;
    char *mFiringString;
    bool mBypassTarballOnce;
    static bool xFailSilently;
    static int xLastDumpedSize;

    //--System
    LuaManager();
    ~LuaManager();

    //--Property Queries
    lua_State *GetLuaState();
    static bool DoesScriptExist(const char *pPath);
    char *GetCallStack(int pSlot);
    float GetLuaGlobalAsFloat(const char *pName);
    char *GetLuaGlobalAsString(const char *pName);
    bool IsStoppingExecution();

    //--Manipulators
    void SetAltDirectory(const char *pSearch, const char *pReplace);
    void SetRelativePath(const char *pPath);
    void PushCallStack(const char *pPath);
    void PopCallStack();
    void AddTimer(const char *pIdentifier);
    float CheckTimer(const char *pIdentifier);
    float FinishTimer(const char *pIdentifier);
    void SetStopExecution();

    //--Core Methods
    int BinaryGetLookup(const char *pName, int pMin, int pMax);
    void CleanPath(const char *pInBuffer, char *pOutBuffer);
    bool CompileLuaFile(const char *pString);
    bool LoadLuaFile(const char *pPath);
    bool LoadLuaString(const char *pString);
    bool ExecuteLoadedFile();
    void *DumpLoadedFile();
    void ExecuteLuaFile(const char *pPath);
    void ExecuteLuaFileBypass(const char *pPath);
    void ExecuteLuaFile(const char *pPath, int pArgs, ...);
    void ExecuteLuaFileNoAddendum(const char *pPath);
    void PushExecPop(void *pEntity, const char *pPath);
    void PushExecPopBypass(void *pEntity, const char *pPath);
    void PushExecPop(void *pEntity, const char *pPath, int pArgs, ...);
    void PrintCallStack();

    ///--Addendums
    void RegisterPredendum(const char *pName, const char *pOriginalScript, const char *pAddendumScript);
    void RegisterAddendum(const char *pName, const char *pOriginalScript, const char *pAddendumScript);
    void HandlePredendumExecution(const char *pPath);
    void HandleAddendumExecution(const char *pPath);
    void ClearAddendums();

    ///--Arguments
    int GetTotalScriptArguments();
    char *GetScriptArgument(int pSlot);
    void SetArgumentListSize(int pSize);
    void AddArgument(float pNumber);
    void AddArgument(const char *pString);
    void ClearArgumentList();

    ///--Precompilation
    bool LoadPrecompiledScript(const char *pPath);

    ///--Remaps
    void ActivateRemapMode();
    void DeactivateRemapMode();
    void BeginRemapAddition();
    void AddRemap(const char *pOriginalPath, const char *pModifiedPath);
    void ConcludeRemapAddition();
    int BinaryGetRemap(const char *pName, int pMin, int pMax);
    static char *SimplifyPath(const char *pPath);

    ///--Tarball Mode
    void SetTarballMode(const char *pPathToTarball);

    ///--Translation
    void SetStandardExtension(const char *pPath);
    void SetTranslateExtension(const char *pPath);
    int LoadLuaFileFinalStep(const char *pPath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static LuaManager *Fetch();
    static int WriterFunction(lua_State *pLuaState, const void *pSource, size_t pExpectedSize, void *pDest);
    static const char *ReaderFunction(lua_State *pLuaState, void *pData, size_t *pExpectedSize);
};

//--[System Functions]
int Hook_LM_GetSystemLibrary(lua_State *L);
int Hook_LM_GetSystemOS(lua_State *L);
int Hook_LM_SetAlternateDirectory(lua_State *L);
int Hook_LM_GetRandomNumber(lua_State *L);
int Hook_LM_SetSilenceFlag(lua_State *L);
int Hook_LM_GetCallStack(lua_State *L);
int Hook_LM_BootTarballMode(lua_State *L);
int Hook_LM_StartTimer(lua_State *L);
int Hook_LM_CheckTimer(lua_State *L);
int Hook_LM_FinishTimer(lua_State *L);
int Hook_LM_IsGameRegistered(lua_State *L);
int Hook_LM_SetTranslationExtension(lua_State *L);
int Hook_LM_PrintCallStack(lua_State *L);

//--[Executors]
int Hook_LM_ExecuteScript(lua_State *L);
int Hook_LM_GetNumOfArgs(lua_State *L);
int Hook_LM_GetScriptArgument(lua_State *L);

///--[Remaps]
int Hook_LM_SimplifyPath(lua_State *L);
int Hook_LM_SetPathProperty(lua_State *L);
