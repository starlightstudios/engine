//--Base
#include "AdvUIChat.h"

//--Classes
#include "AdvUIBase.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIChat::AdvUIChat()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "ACHT");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = (VIRTUAL_CANVAS_X * 0.50f);
    mGridRenderCenterY  = (VIRTUAL_CANVAS_Y * 0.35f);
    mGridRenderSpacingX = (VIRTUAL_CANVAS_X * 0.08f);
    mGridRenderSpacingY = (VIRTUAL_CANVAS_X * 0.08f);

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ====== AdvUIChat ======= ]
    ///--[Variables]
    //--Cursor
    mDontShowHighlight = false;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Data
    mChatData = new StarLinkedList(true);

    //--Strings
    mShowHelpString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Help Strings.
    AllocateHelpStrings(cxAdvChatHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIChat::~AdvUIChat()
{
    delete mChatData;
    delete mShowHelpString;
}
void AdvUIChat::Construct()
{
    ///--[Documentation]
    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Chat UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI", &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = AdvImages.rFont_Mainline;
}

///--[Public Statics]
//--Script used to resolve who can be talked to when at a campfire.
char *AdvUIChat::xChatResolveScript = NULL;

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIChat::TakeForeground()
{
    ///--[Documentation and Setup]
    //--Called from the Campfire Menu, activates party chat mode. If no characters are available
    //  to chat with, fails.
    if(!xChatResolveScript)
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    ///--[Variable Reset]
    ///--[Grid Building]
    //--Clear the chat listing, then run the lua script which will repopulate it.
    mChatData->ClearList();
    LuaManager::Fetch()->ExecuteLuaFile(xChatResolveScript);

    //--If the list was empty for any reason, fail here.
    if(mChatData->GetListSize() < 1)
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    ///--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    AutoAllocateSquareGrid(mChatData->GetListSize());
    ProcessGrid();

    //--Iteration loop.
    int i = 0;
    ChatGridPack *rPackage = (ChatGridPack *)mChatData->PushIterator();
    while(rPackage)
    {
        //--Retrieve a grid package. If it's NULL then we ran out of space, bark an error.
        GridPackage *rGridPack = GetNextPriorityEntry();
        if(!rGridPack)
        {
            fprintf(stderr, "AdvUIChat:TakeForeground() - Warning, chat data has %i entries but the grid ran out of slots. Failing.\n", mChatData->GetListSize());
            mChatData->PopIterator();
            break;
        }

        //--Copy across.
        rGridPack->mCode = i;
        strcpy(rGridPack->mLocalName, mChatData->GetIteratorName());
        memcpy(&rGridPack->mAlignments, &rPackage->mAlignments, sizeof(AdvMenuStandardAlignments));

        //--Next.
        i ++;
        rPackage = (ChatGridPack *)mChatData->AutoIterate();
    }

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    ///--[Run Cursor]
    //--Update the cursor so it focuses on the selected entry.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}
void AdvUIChat::AddChatEntity(const char *pName, const char *pPath, float pXOff, float pYOff, float pLft, float pTop, float pRgt, float pBot, const char *pImgPath)
{
    ///--[Documentation]
    //--Adds a chat entity, which represents a party member that can be chatter with.
    if(!pName || !pPath) return;

    //--If the image doesn't exist, fail here and print a warning.
    StarBitmap *rCheckImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);
    if(!rCheckImage)
    {
        DebugManager::ForcePrint("AdvUIChat:AddChatEntity() - Error, no image %s found for entry %s.\n", pImgPath, pName);
        return;
    }

    //--Create package.
    ChatGridPack *nPackage = (ChatGridPack *)starmemoryalloc(sizeof(ChatGridPack));
    nPackage->Initialize();
    ResetString(nPackage->mCallScript, pPath);
    nPackage->mAlignments.rImage = rCheckImage;
    nPackage->mAlignments.mOffsetX = pXOff;
    nPackage->mAlignments.mOffsetY = pYOff;
    nPackage->mAlignments.mImageX  = pLft - rCheckImage->GetXOffset();
    nPackage->mAlignments.mImageY  = pTop - rCheckImage->GetYOffset();
    nPackage->mAlignments.mImageW  = pRgt - pLft;
    nPackage->mAlignments.mImageH  = pBot - pTop;

    //--Clamp.
    if(nPackage->mAlignments.mImageW < 1.0f) nPackage->mAlignments.mImageW = 1.0f;
    if(nPackage->mAlignments.mImageH < 1.0f) nPackage->mAlignments.mImageH = 1.0f;

    //--Add.
    mChatData->AddElementAsTail(pName, nPackage, &ChatGridPack::DeleteThis);
}

///======================================= Core Methods ===========================================
void AdvUIChat::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Set Lines]
    //--Common lines.
    SetHelpString(i, "On the chat menu, you can select a party member and talk to them about what's going", 2, "Activate", "Cancel");
    SetHelpString(i, "on. Their topics change as the game goes on, so catch up often.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void AdvUIChat::RecomputeCursorPositions()
{
    RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, 1.0f);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIChat::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        mVisibilityTimer ++;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--Standard Timers.
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks, mIsShowingHelp);

    //--Grid timers.
    UpdateGridTimers();

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Set this flag.
    mDontShowHighlight = false;

    ///--[ ======== Update Intercepts ========= ]
    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[ =========== Update Call ============ ]
    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return true;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get what option is under the cursor.
        int tGridOption = GetGridCodeAtCursor();

        //--Get entry.
        ChatGridPack *rSelectedPack = (ChatGridPack *)mChatData->GetElementBySlot(tGridOption);
        if(rSelectedPack && rSelectedPack->mCallScript && strcasecmp(rSelectedPack->mCallScript, "Null"))
        {
            //--Deactivate this menu, and hide everything.
            FlagExit();
            SetCodeBackward(AM_MODE_EXITNOW);

            //--Run.
            LuaManager::Fetch()->ExecuteLuaFile(rSelectedPack->mCallScript, 2, "S", "Hello", "S", "IsFireside");

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Error.
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update.
    return true;
}
void AdvUIChat::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;

    //--Run timers.
    mVisibilityTimer --;
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Unset this flag.
    mDontShowHighlight = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIChat::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Rendering of the base UI. Mostly follows the standard, however the bottom of the UI that shows
    //  the player's party will continue to render even if this object is offscreen if the Doctor UI
    //  needs it to, which is handled via a static timer.
    StarlightColor::ClearMixer();

    ///--[Timer]
    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Render Grid]
    //--A grid in the middle indicates the player's selected by both rendering a highlight box over it,
    //  and slightly increasing the icon size.
    RenderGrid(pVisAlpha);

    //--Render the cursor.
    if(!mDontShowHighlight) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Render the name of the selected entry.
    RenderGridEntryName(GetPackageAtCursor(), pVisAlpha);

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUIChat::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the header at the top of the screen.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Party Chat Menu");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, 0, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    AutoPieceClose();
}
void AdvUIChat::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Execution]
    //--Footer.
    AdvImages.rOverlay_Footer->Draw();

    ///--[Clean]
    glDisable(GL_STENCIL_TEST);
    AutoPieceClose();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
