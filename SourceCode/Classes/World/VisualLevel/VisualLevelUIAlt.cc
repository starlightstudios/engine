//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "FlexMenu.h"
#include "InventoryItem.h"

//--CoreClasses
#include "DataList.h"
#include "StarBitmap.h"
#include "StarButton.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

//--Alternative layout for the UI. Can be toggled on with a flag. The entry points for these functions are in their
//  respective non-alt functions.

void VisualLevel::RenderAltCharacterPane()
{
    //--[Documenation]
    //--Alternative character pane, renders the full character sprite
    if(!Images.mIsReady) return;
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();

    //--Scaling actions only affect the width. Heights are fixed.
    StarFont::xScaleAffectsX = true;
    StarFont::xScaleAffectsY = false;

    //--Fonts.
    StarFont *rCharactersFont = PandemoniumLevel::Images.Data.rUIFontMedium;

    //--[Initial Values]
    //--Determine the position of the right-centered character pane.
    float cWid = ConsolePane.cHei;
    float cHei = 400.0f;
    float cIns = 8.0f;
    float cLft = VIRTUAL_CANVAS_X - cWid;
    float cTop = 4.0f;
    float cRgt = cLft + cWid;
    float cBot = cTop + cHei;

    //--Recompute the borders. We need to make the overlay cover the whole screen, accounting for no letterbox in 3D mode.
    cLft = cLft + (DisplayManager::xScaledOffsetX);
    cRgt = cRgt + (DisplayManager::xScaledOffsetX);
    cTop = cTop - (DisplayManager::xScaledOffsetY);
    cBot = cBot - (DisplayManager::xScaledOffsetY);

    //--[Player Status Card]
    //--Recompute the position. This displays at the bottom of the character's portrait pane and does not have a border.
    //  This deliberately renders under the portrait.
    float cStatWid = 174.0f;
    float cStatHei =  88.0f;
    float cStatLft = cLft - cStatWid;
    float cStatTop = cTop;
    float cStatRgt = cStatLft + cStatWid;
    float cStatBot = cStatTop + cStatHei;

    //--Render the values and bars.
    glColor3f(1.0f, 1.0f, 1.0f);
    RenderBorderCardOver(cStatLft, cStatTop, cStatRgt, cStatBot, 0x01FF);
    if(rPlayerCharacter)
    {
        //--Get values.
        CombatStats tPlayerStats = rPlayerCharacter->GetCombatStatistics();

        //--Health.
        RenderBar(StarlightColor::MapRGBF(1.0f, 0.0f, 0.0f), cStatLft + 28.0f, cStatTop + 22.0f, 132.0f, 4.0f, (float)tPlayerStats.mHP / (float)tPlayerStats.mHPMax);
        rCharactersFont->DrawText(cStatLft + 10.0f, cStatTop +  8.0f, 0, "H");
        rCharactersFont->DrawTextArgs(cStatLft + 28.0f, cStatTop +  8.0f, 0, 1.0f, "%i/%i", tPlayerStats.mHP, tPlayerStats.mHPMax);
        if(rPlayerCharacter->IsStunned())
        {
            StarFont::xScaleAffectsY = false;
            rCharactersFont->DrawTextArgs(cStatLft + 80.0f, cStatTop +  8.0f, 0, 0.6f, "Stunned! (%i)", rPlayerCharacter->GetTurnsToRecovery());
        }

        //--Willpower.
        RenderBar(StarlightColor::MapRGBF(0.0f, 0.0f, 1.0f), cStatLft + 28.0f, cStatTop + 46.0f, 132.0f, 4.0f, (float)tPlayerStats.mWillPower / (float)tPlayerStats.mWillPowerMax);
        rCharactersFont->DrawText(cStatLft +  6.0f, cStatTop + 32.0f, 0, "W");
        rCharactersFont->DrawTextArgs(cStatLft + 28.0f, cStatTop + 32.0f, 0, 1.0f, "%i/%i", tPlayerStats.mWillPower, tPlayerStats.mWillPowerMax);
        if(tPlayerStats.mWillPower < 5)
        {
            StarFont::xScaleAffectsY = false;
            rCharactersFont->DrawTextArgs(cStatLft + 80.0f, cStatTop + 32.0f, 0, 0.6f, "Confounded!");
        }

        //--Stamina.
        RenderBar(StarlightColor::MapRGBF(0.0f, 1.0f, 0.0f), cStatLft + 28.0f, cStatTop + 70.0f, 132.0f, 4.0f, (float)tPlayerStats.mStamina / (float)tPlayerStats.mStaminaMax);
        rCharactersFont->DrawText(cStatLft + 11.0f, cStatTop + 56.0f, 0, "S");
        rCharactersFont->DrawTextArgs(cStatLft + 28.0f, cStatTop + 56.0f, 0, 1.0f, "%i/%i", tPlayerStats.mStamina,   tPlayerStats.mStaminaMax);
    }

    //--[Self-Menu]
    //--The Self-Menu is already expanded and placed next to the character pane. It periodically updates, specifically whenever the player gains or loses
    //  abilities (such as during a TF).
    if(!mStoredSelfMenu && mSelfMenuPopulationScript)
    {
        mStoredSelfMenu = new FlexMenu();
        LuaManager::Fetch()->PushExecPop(mStoredSelfMenu, mSelfMenuPopulationScript);
    }

    //--Render the self-menu.
    if(mStoredSelfMenu)
    {
        //--Get the button list. All except the first will render here.
        StarLinkedList *rButtonList = mStoredSelfMenu->GetButtonList();

        //--Size the border.
        float cHIndent = 8.0f;
        float cVIndent = 8.0f;
        float cSpacing = 20.0f;
        float cSelfMenuWid = cStatWid;
        mSelfMenuDimensions.SetWH(cStatRgt - cSelfMenuWid, cStatBot, cSelfMenuWid, (rButtonList->GetListSize() * cSpacing) + (cVIndent * 2.0f));
        RenderBorderCardOver(mSelfMenuDimensions.mLft, mSelfMenuDimensions.mTop, mSelfMenuDimensions.mRgt, mSelfMenuDimensions.mBot, 0x01FF);

        //--Render the buttons.
        for(int i = 0; i < rButtonList->GetListSize(); i ++)
        {
            //--Get and check.
            StarButton *rButton = (StarButton *)rButtonList->GetElementBySlot(i);
            if(!rButton) continue;

            //--Get the text width, and downscale it to make it narrower if it's too long.
            float tScale = ResolveLargestScale(rCharactersFont, mSelfMenuDimensions.GetWidth() - (cHIndent * 2.0f), rButton->GetName());

            //--Render the text.
            float cLft = mSelfMenuDimensions.mLft + cHIndent;
            float cTop = mSelfMenuDimensions.mTop + (i * cSpacing) + cVIndent;
            rCharactersFont->DrawText(cLft, cTop, 0, tScale, rButton->GetName());
        }
    }

    //--Button, renders next to the portrait card, below the stats bar.
    //Images.Data.rSelfMenuBtn->Draw(cStatRgt - Images.Data.rSelfMenuBtn->GetWidth(), cStatBot);
    //mSelfMenuBtnSize.SetWH(cStatRgt - Images.Data.rSelfMenuBtn->GetWidth(), cStatBot, Images.Data.rSelfMenuBtn->GetWidth(), Images.Data.rSelfMenuBtn->GetHeight());

    //--[Portrait Card]
    //--Card background.
    RenderBorderCardOver(cLft, cTop, cRgt, cBot, 0x003F);

    //--Store this position. It's not dynamic.
    mCharacterPortraitDim.Set(cLft + cIns, cTop + cIns, cRgt - cIns, cBot - cIns);

    //--Get the current character portrait.
    if(rPlayerCharacter && rPlayerCharacter->GetActiveImage())
    {
        //--Get and bind.
        StarBitmap *rPlayerPortrait = rPlayerCharacter->GetActiveImage();
        rPlayerPortrait->Bind();

        //--Sizes. Force them to be non-zero.
        float cImgWidth  = rPlayerPortrait->GetTrueWidth();
        float cImgHeight = rPlayerPortrait->GetTrueHeight();
        if(cImgWidth  < 1.0f) cImgWidth  = 1.0f;
        if(cImgHeight < 1.0f) cImgHeight = 1.0f;

        //--Calculate the max amount of stretch available.
        float cHSize = mCharacterPortraitDim.GetWidth();
        float cVSize = mCharacterPortraitDim.GetHeight() - 25.0f;
        float cHMult = cHSize / cImgWidth;
        float cVMult = cVSize / cImgHeight;

        //--Error check.
        if(cHMult == 0.0f || cVMult == 0.0f)
        {

        }
        //--Safe case.
        else
        {
            //--Rendering positions.
            float tCenterX = (mCharacterPortraitDim.mLft + mCharacterPortraitDim.mRgt) / 2.0f;
            float tCenterY = (mCharacterPortraitDim.mTop + mCharacterPortraitDim.mBot - 25.0f) / 2.0f;
            glTranslatef(tCenterX, tCenterY, 0.0f);
            float tRenderX = cImgWidth  / -2.0f;
            float tRenderY = cImgHeight / -2.0f;

            //--Render down slightly.
            tRenderY = tRenderY + 25.0f;

            //--If there are more (or equal) HMult, use the VMult.
            if(cHMult > cVMult)
            {
                glScalef(cVMult, cVMult, 1.0f);
                rActivePortrait->Draw(tRenderX, tRenderY);
                glScalef(1.0f / cVMult, 1.0f / cVMult, 1.0f);
            }
            //--Use the HMult.
            else
            {
                glScalef(cHMult, cHMult, 1.0f);
                rActivePortrait->Draw(tRenderX, tRenderY);
                glScalef(1.0f / cHMult, 1.0f / cHMult, 1.0f);
            }

            //--Clean.
            glTranslatef(-tCenterX, -tCenterY, 0.0f);
        }

        //--SLime transofmration timer, if it exists, is rendered in the portrait card.
        void *rSlimeTFPtr = rPlayerCharacter->GetDataList()->FetchDataEntry("iSlimeTFTurns");
        if(rSlimeTFPtr && rPlayerCharacter->GetTeam() == TEAM_PLAYER)
        {
            //--Cast.
            float *rSlimeTFTurnsPtr = (float *)rSlimeTFPtr;
            int tValue = (int)(*rSlimeTFTurnsPtr);
            if(tValue > 0)
            {
                rCharactersFont->DrawTextArgs(mCharacterPortraitDim.mLft + 4.0f, mCharacterPortraitDim.mTop + 4.0f, 0, 0.75f, "  Infected! (%i turns left)", 30 - tValue);
            }
        }
    }

    //--[Status Card]
    //--Game status display. Renders under the system menu button. Length is determined by the longest string.
    float cStatusCardScale = 0.75f;
    float cFontSpacing = 22.0f * cStatusCardScale;
    float tCursor = Images.Data.rSystemMenuBtn->GetHeight() + 6.0f;
    StarFont::xScaleAffectsX = true;
    StarFont::xScaleAffectsY = true;

    //--Render the game status. These strings are set by scripts externally.
    if(mStatusStrings)
    {
        //--Initial loop, determine the longest string.
        float tLongest = 0.0f;
        for(int i = 0; i < mStatusStringsTotal; i ++)
        {
            //--Skip blank lines.
            if(!mStatusStrings[i]) continue;

            //--Check length, store.
            float tLength = rCharactersFont->GetTextWidth(mStatusStrings[i]) * cStatusCardScale * 1.05f;
            if(tLength > tLongest) tLongest = tLength;
        }

        //--Render a card border for this.
        RenderBorderCardOver(ConsolePane.cLft + 4.0f, tCursor, ConsolePane.cLft + tLongest + 16.0f, tCursor + (cFontSpacing * mStatusStringsTotal) + 16.0f, 0x01FF);
        tCursor = tCursor + 8.0f;

        //--Loop, render.
        for(int i = 0; i < mStatusStringsTotal; i ++)
        {
            //--Skip blank lines.
            if(!mStatusStrings[i]) continue;

            //--Render.
            rCharactersFont->DrawText(ConsolePane.cLft + 12.0f, tCursor, 0, cStatusCardScale, mStatusStrings[i]);
            tCursor = tCursor + cFontSpacing;
        }
    }

    //--[Inventory Card]
    //--If the inventory needs to reconstitute, do that now. The parent class handles this.
    if(rPlayerCharacter && rPlayerCharacter->GetInventoryReconstituteFlag()) ReconstituteInventory();

    //--Reset scale values for the inventory.
    StarFont::xScaleAffectsX = true;
    StarFont::xScaleAffectsY = false;

    //--Recompute sizes. This thing resizes dynamically. It always contains at least 2 members to indicate the weapon/armor status.
    float cSpacing = 20.0f;
    int tInventoryEntries = 2 + mInventoryList->GetListSize();

    //--Clamp. A small number of items can be rendered since there's a lot less space.
    mItemsRendered = tInventoryEntries;
    cTop = cBot - cIns;
    cBot = cTop + (mItemsRendered * cSpacing) + 16.0f;
    while(cBot >= mRadarSize.mTop)
    {
        mItemsRendered --;
        cBot = cTop + (mItemsRendered * cSpacing) + 16.0f;
    }

    //--Border.
    RenderBorderCardOver(cLft, cTop, cRgt, cBot, 0x00FF);

    //--Store this position. This is dynamic, and can (and will) change through gameplay.
    float cCursor = cTop + 8.0f;
    mInventoryDim.Set(cLft + cIns, cTop + cIns, cRgt - cIns, cBot - cIns);

    //--Render the weapon/armor info.
    int cSoFar = 0;
    if(rPlayerCharacter)
    {
        //--Weapon.
        if(mItemOffset < 1)
        {
            InventoryItem *rWeapon = rPlayerCharacter->GetWeapon();
            if(rWeapon)
            {
                float tScale = ResolveLargestScale(rCharactersFont, mInventoryDim.GetWidth() - 32.0f, rWeapon->GetName());
                Images.Data.rWeaponIcon->Draw(cLft + 8, cCursor);
                rCharactersFont->DrawText(cLft + 28.0f, cCursor, 0, tScale, rWeapon->GetName());
            }
            //--No weapon, render no icon and the word "Unarmed".
            else
            {
                Images.Data.rWeaponIcon->Draw(cLft + 8, cCursor);
                rCharactersFont->DrawText(cLft + 28.0f, cCursor, 0, "No Weapon");
            }

            //--Cursor moves down.
            cSoFar ++;
            cCursor = cCursor + cSpacing;
        }

        //--Armor.
        if(mItemOffset < 2)
        {
            InventoryItem *rArmor = rPlayerCharacter->GetArmor();
            if(rArmor)
            {
                float tScale = ResolveLargestScale(rCharactersFont, mInventoryDim.GetWidth() - 32.0f, rArmor->GetName());
                Images.Data.rArmorIcon->Draw(cLft + 8, cCursor - 2.0f);
                rCharactersFont->DrawText(cLft + 28.0f, cCursor, 0, tScale, rArmor->GetName());
            }
            //--No weapon, render no icon and the word "Unarmed".
            else
            {
                Images.Data.rArmorIcon->Draw(cLft + 8, cCursor - 2.0f);
                rCharactersFont->DrawText(cLft + 28.0f, cCursor, 0, "No Armor");
            }

            //--Cursor moves down.
            cSoFar ++;
            cCursor = cCursor + cSpacing;
        }
    }

    //--Render the names of each item, and how many we're carrying.
    InventoryPack *rPack = (InventoryPack *)mInventoryList->PushIterator();
    while(rPack)
    {
        //--Clamp.
        if(cSoFar >= mItemsRendered)
        {
            mInventoryList->PopIterator();
            break;
        }

        //--Buffering.
        char tBuffer[128];
        if(rPack->mQuantity > 1)
            sprintf(tBuffer, "%s x%i", rPack->rItem->GetName(), rPack->mQuantity);
        else
            sprintf(tBuffer, "%s", rPack->rItem->GetName());

        //--Render it.
        float tScale = ResolveLargestScale(rCharactersFont, mInventoryDim.GetWidth() - 32.0f, tBuffer);
        rCharactersFont->DrawText(cLft + 28.0f, cCursor, 0, tScale, tBuffer);

        //--Next.
        cSoFar ++;
        cCursor = cCursor + cSpacing;
        rPack = (InventoryPack *)mInventoryList->AutoIterate();
    }

    //--Scroll Buttons/Bar. Only renders if there are enough entities present.
    glColor3f(1.0f, 1.0f, 1.0f);
    if(mItemsRendered < tInventoryEntries)
    {
        //--Position values.
        float cLft = mInventoryDim.mRgt - 4.0f - PandemoniumLevel::Images.Data.rScrollBtnDn->GetWidth();
        float cTop = mInventoryDim.mTop + 4.0f;
        float cRgt = mInventoryDim.mRgt - 4.0f;
        float cBot = mInventoryDim.mBot - 24.0f;

        //--Top scrolling button.
        mItemScrollUpBtn.SetWH(cLft, cTop, PandemoniumLevel::Images.Data.rScrollBtnDn->GetWidth(), PandemoniumLevel::Images.Data.rScrollBtnDn->GetHeight());
        PandemoniumLevel::Images.Data.rScrollBtnDn->Draw(cLft, cTop, SUGAR_FLIP_VERTICAL);

        //--Bottom scrolling button.
        mItemScrollDnBtn.SetWH(cLft, cBot, PandemoniumLevel::Images.Data.rScrollBtnDn->GetWidth(), PandemoniumLevel::Images.Data.rScrollBtnDn->GetHeight());
        PandemoniumLevel::Images.Data.rScrollBtnDn->Draw(cLft, cBot, 0);

        //--Move the top constant down for faster rendering.
        cTop = cTop + PandemoniumLevel::Images.Data.rScrollBtnDn->GetHeight();

        //--Draw the vertical bars.
        glColor3f(0.0f, 0.0f, 0.0f);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);

            //--Left bar.
            glVertex2f(cLft + 0.0f, cTop + 0.0f);
            glVertex2f(cLft + 2.0f, cTop + 0.0f);
            glVertex2f(cLft + 2.0f, cBot + 0.0f);
            glVertex2f(cLft + 0.0f, cBot + 0.0f);

            //--Right bar.
            glVertex2f(cRgt + 0.0f, cTop + 0.0f);
            glVertex2f(cRgt - 2.0f, cTop + 0.0f);
            glVertex2f(cRgt - 2.0f, cBot + 0.0f);
            glVertex2f(cRgt + 0.0f, cBot + 0.0f);

            //--Draw the scroll bar. Doesn't always render.
            if(mItemsRendered > 0)
            {
                //--Compute percentage.
                float cTopPercent = (float) mItemOffset                 / (float)tInventoryEntries;
                float cBotPercent = (float)(mItemOffset+mItemsRendered) / (float)tInventoryEntries;
                if(cBotPercent >= 1.0f) cBotPercent = 1.0f;

                //--Render outer.
                glColor3f(0.1f, 0.1f, 0.1f);
                glVertex2f(cLft + 2.0f, cTop + (cTopPercent * (cBot - cTop)));
                glVertex2f(cRgt - 2.0f, cTop + (cTopPercent * (cBot - cTop)));
                glVertex2f(cRgt - 2.0f, cTop + (cBotPercent * (cBot - cTop)));
                glVertex2f(cLft + 2.0f, cTop + (cBotPercent * (cBot - cTop)));

                //--Render inner.
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex2f(cLft + 4.0f, cTop + (cTopPercent * (cBot - cTop)) + 2.0f);
                glVertex2f(cRgt - 4.0f, cTop + (cTopPercent * (cBot - cTop)) + 2.0f);
                glVertex2f(cRgt - 4.0f, cTop + (cBotPercent * (cBot - cTop)) - 2.0f);
                glVertex2f(cLft + 4.0f, cTop + (cBotPercent * (cBot - cTop)) - 2.0f);
            }

        //--Clean.
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }
    //--Wipe data for the scroll buttons.
    else
    {
        mItemScrollUpBtn.SetWH(-1000, -1000, 1, 1);
        mItemScrollDnBtn.SetWH(-1000, -1000, 1, 1);
    }

    //--[Clean Up]
    //--Reset the scale flags.
    StarFont::xScaleAffectsX = true;
    StarFont::xScaleAffectsY = true;
}
#include "ContextMenu.h"
void VisualLevel::ShowContextMenuAround(int pIndex)
{
    //--Updated routine, shows the ContextMenu around an entity as indicated by its position in the unified
    //  object list. This is otherwise the same as the parent class.
    mContextMenu->ClearMenu();
    if(pIndex < 0 || pIndex >= VL_ENTITIES_PER_SCREEN) return;

    //--Actor?
    if(mUnifiedRenderListing[pIndex].rActorPtr)
    {
        ShowContextMenuAroundActor(mUnifiedRenderListing[pIndex].rActorPtr);
    }
    //--Item!?
    else if(mUnifiedRenderListing[pIndex].rInventoryPackPtr)
    {
        ShowContextMenuAroundItem(mUnifiedRenderListing[pIndex].rInventoryPackPtr->rItem);
    }
    //--Container!
    else if(mUnifiedRenderListing[pIndex].rContainerPtr)
    {
        ShowContextMenuAroundContainer(mUnifiedRenderListing[pIndex].rContainerPtr);
    }
}
