//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "EntityManager.h"

///======================================== Lua Hooking ===========================================
void TilemapActor::HookToLuaState(lua_State *pLuaState)
{
    /* TA_Create(sName)
       Creates a new TilemapActor, registers it, and pushes it on the Activity Stack. */
    lua_register(pLuaState, "TA_Create", &Hook_TA_Create);

    /* TA_CreateUsingPosition(sName, sPositionName)
       Creates a new TilemapActor, registers it, and pushes it on the Activity Stack. If the corresponding
       position exists in the active map, uses its properties to perform setup. */
    lua_register(pLuaState, "TA_CreateUsingPosition", &Hook_TA_CreateUsingPosition);

    /* //--[Static]
       TA_GetProperty("Is Mugging Check") (1 Boolean) (Static)
       TA_GetProperty("Is Non Combat Check") (1 Boolean) (Static)
       TA_GetProperty("Was Mugged") (1 Boolean) (Static)

       //--[Properties]
       TA_GetProperty("ID") (1 Integer)
       TA_GetProperty("Name") (1 String)
       TA_GetProperty("Position") (2 Floats)
       TA_GetProperty("Facing") (1 Integer)
       TA_GetProperty("Move Images Total") (1 Integer)
       TA_GetProperty("Is Touching Collision") (1 Boolean)
       TA_GetProperty("Has Special Frame", sFrameName) (1 Boolean)

       //--[Enemy Properties]
       TA_GetProperty("Is Enemy") (1 Boolean)
       TA_GetProperty("Zeroth Enemy Name") (1 String)
       TA_GetProperty("Never Drops Items On Mug") (1 Boolean)

       Returns the named property in the active TilemapActor. */
    lua_register(pLuaState, "TA_GetProperty", &Hook_TA_GetProperty);

    /* //--[Static]
       TA_SetProperty("Enemy Path", sPath) (Static)
       TA_SetProperty("Build Graphics From Name Path", sPath) (Static)
       TA_SetProperty("Standard Enemy Shadow", sDLPath) (Static)
       TA_SetProperty("Store Image Paths", bFlag) (Static)
       TA_SetProperty("Reresolve All Images") (Static)
       TA_SetProperty("Mug Platina", iPlatina) (Static)
       TA_SetProperty("Mug Experience", iExperience) (Static)
       TA_SetProperty("Mug Job Points", iJobPoints) (Static)
       TA_SetProperty("Mug Item", sItemName) (Static)
       TA_SetProperty("Enemies Always Ignore Player", bFlag) (Static)
       TA_SetProperty("Combat Initiate Range", fDistance) (Static)

       //--[System]
       TA_SetProperty("Disabled", bIsDisabled)
       TA_SetProperty("Block Render", bIsBlocked)
       TA_SetProperty("Name", sName)
       TA_SetProperty("Position", iX, iY)
       TA_SetProperty("Position By Entity", sEntityName)
       TA_SetProperty("Depth", iDepth)
       TA_SetProperty("Clipping Flag", bFlag)
       TA_SetProperty("Set Ignore World Stop", bFlag)
       TA_SetProperty("Set Ignore Special Lights", bFlag)
       TA_SetProperty("Set Ignore Clips", bFlag)
       TA_SetProperty("No Footstep Sound", bFlag)
       TA_SetProperty("Reface On Node Flag", bFlag)
       TA_SetProperty("Display String", sString, bClearAfterCombat)
       TA_SetProperty("Responds To Shot", bFlag)

       //--[Instructions]
       TA_SetProperty("Emulate Movement", iDirection, fSpeed)
       TA_SetProperty("Handle Movement", iXPixels, iYPixels)
       TA_SetProperty("Facing", iDirection)
       TA_SetProperty("Face Position", iX, iY)
       TA_SetProperty("Face Character", sNPCName)
       TA_SetProperty("Stop Moving")
       TA_SetProperty("Flashwhite")
       TA_SetProperty("Flashwhite", sReserveFrame)

       //--[Enemy Properties]
       TA_SetProperty("Defeat Scene", sDefeatScene)
       TA_SetProperty("Activate Enemy Mode", sEnemyInternalName)
       TA_SetProperty("Can Be Mugged", bFlag)
       TA_SetProperty("Set Combat Enemy", sEnemyPrototype)
       TA_SetProperty("Add Combat Enemy", sEnemyPrototype)
       TA_SetProperty("Set Item Mode", sItemString)
       TA_SetProperty("Get Stunned")
       TA_SetProperty("Begin Dying")
       TA_SetProperty("Toughness", iValue)
       TA_SetProperty("Set Follow Target", sName)

       //--[NPC Properties]
       TA_SetProperty("Activation Script", sScriptPath)
       TA_SetProperty("Extended Activation Direction", iDirectionFlag)
       TA_SetProperty("Activate Wander Mode")

       //--[Jumping]
       TA_SetProperty("Y Vertical Offset", fOffset)
       TA_SetProperty("Ignores Gravity", bFlag)
       TA_SetProperty("Z Speed", fSpeed)
       TA_SetProperty("Z Gravity", fGravity)
       TA_SetProperty("Jumping Sound", sJumpingSound)
       TA_SetProperty("Landing Sound", sLandingSound)

       //--[Rendering Properties]
       TA_SetProperty("Disable Main Render", bFlag)
       TA_SetProperty("Renders After Tiles", bFlag)
       TA_SetProperty("Flying", bFlag)
       TA_SetProperty("Auto Animates", bFlag)
       TA_SetProperty("Auto Animates Fast", bFlag)
       TA_SetProperty("Y Oscillates", bFlag)
       TA_SetProperty("Tiny", bFlag)
       TA_SetProperty("No Automatic Shadow", bFlag)
       TA_SetProperty("Rendering Depth", fDepth)
       TA_SetProperty("Rendering Depth Offset", fDepth)
       TA_SetProperty("Rendering Offsets", fOffsetX, fOffsetY)
       TA_SetProperty("Walk Ticks Per Frame", iValue)
       TA_SetProperty("Run Ticks Per Frame", iValue)
       TA_SetProperty("Move Frames Total", iTotal)
       TA_SetProperty("Move Frame", iDirection, iSlot, sPath)
       TA_SetProperty("Run Frame", iDirection, iSlot, sPath)
       TA_SetProperty("Standing Frame", iDirection, sPath)
       TA_SetProperty("Breathe Frame", iDirection, sPath)
       TA_SetProperty("Shadow", sDLPath)
       TA_SetProperty("Shake", iTicks)
       TA_SetProperty("Void Rift")
       TA_SetProperty("Special Idle")
       TA_SetProperty("Render As Single Block")
       TA_SetProperty("Whole Collision")
       TA_SetProperty("Activate By Rub")

       //--[Special Frames]
       TA_SetProperty("Add Special Frame", sRefName, sPath)
       TA_SetProperty("Set Special Frame", sRefName)
       TA_SetProperty("Wipe Special Frames")
       TA_SetProperty("Create Special Frame Anim", sName)
       TA_SetProperty("Set Special Frame Anim Length", sName, iFrames)
       TA_SetProperty("Set Special Frame Anim Element", sName, iTicks, sAnimName)
       TA_SetProperty("Set Special Frame Anim Reset Frame", sName, iSlot)
       TA_SetProperty("Activate Special Frame Anim")

       //--[Idle Animations]
       TA_SetProperty("Idle Animation Flag", bFlag)
       TA_SetProperty("Allocate Idle Frames", iFrames)
       TA_SetProperty("Set Idle Frame", iSlot, sDLPath)
       TA_SetProperty("Set Idle TPF", iTPF)
       TA_SetProperty("Set Idle Loop Frame", iLoopFrame)

       //--[Overlays]
       TA_SetProperty("Set Manual Overlay", sDLPath)
       TA_SetProperty("Set Auto Overlays", bFlag)
       TA_SetProperty("Auto Overlay TPF", fTPF)
       TA_SetProperty("Allocate Auto Overlays", iTotal)
       TA_SetProperty("Auto Overlay Frame", iSlot, sDLPath)

       Sets the named property in the active TilemapActor. */
    lua_register(pLuaState, "TA_SetProperty", &Hook_TA_SetProperty);

    /* TA_ChangeCollisionFlag(sEntityName, bFlag)
       Quickly changes the collision flag for the requested entity. Saves the scripter from having
       to push/change/pop since this is done fairly routinely. */
    lua_register(pLuaState, "TA_ChangeCollisionFlag", &Hook_TA_ChangeCollisionFlag);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_TA_Create(lua_State *L)
{
    //TA_Create(sName)
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TA_Create");

    //--Create, push, register.
    TilemapActor *nActor = new TilemapActor();
    nActor->SetName(lua_tostring(L, 1));
    EntityManager::Fetch()->RegisterPointer(lua_tostring(L, 1), nActor, &RootEntity::DeleteThis);
    DataLibrary::Fetch()->rActiveObject = nActor;
    return 0;
}
int Hook_TA_CreateUsingPosition(lua_State *L)
{
    //TA_CreateUsingPosition(sName, sPositionName)
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("TA_CreateUsingPosition");

    //--Create, push, register.
    TilemapActor *nActor = new TilemapActor();
    nActor->SetName(lua_tostring(L, 1));
    EntityManager::Fetch()->RegisterPointer(lua_tostring(L, 1), nActor, &RootEntity::DeleteThis);
    DataLibrary::Fetch()->rActiveObject = nActor;

    //--Tell the active AdventureLevel to do the default setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    rActiveLevel->SetActorToPositionPack(nActor, lua_tostring(L, 2));

    return 0;
}
int Hook_TA_GetProperty(lua_State *L)
{
    ///--[Argument List]
    //--[Static]
    //TA_GetProperty("Is Mugging Check") (1 Boolean) (Static)
    //TA_GetProperty("Is Non Combat Check") (1 Boolean) (Static)
    //TA_GetProperty("Was Mugged") (1 Boolean) (Static)

    //--[Properties]
    //TA_GetProperty("ID") (1 Integer)
    //TA_GetProperty("Name") (1 String)
    //TA_GetProperty("Position") (2 Floats)
    //TA_GetProperty("Facing") (1 Integer)
    //TA_GetProperty("Move Images Total") (1 Integer)
    //TA_GetProperty("Is Touching Collision") (1 Boolean)
    //TA_GetProperty("Is Lft Clipped") (1 Boolean)
    //TA_GetProperty("Is Top Clipped") (1 Boolean)
    //TA_GetProperty("Is Rgt Clipped") (1 Boolean)
    //TA_GetProperty("Is Bot Clipped") (1 Boolean)
    //TA_GetProperty("Has Special Frame", sFrameName) (1 Boolean)
    //TA_GetProperty("Is Showing Special Frame") (1 Boolean)
    //TA_GetProperty("Current Special Frame") (1 String)

    //--[Enemy Properties]
    //TA_GetProperty("Is Enemy") (1 Boolean)
    //TA_GetProperty("Zeroth Enemy Name") (1 String)
    //TA_GetProperty("Never Drops Items On Mug") (1 Boolean)

    //--[Debug]
    //TA_GetProperty("Print Special Frames") (No Returns)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TA_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Types]
    //--True if this is a mugging check.
    if(!strcasecmp(rSwitchType, "Is Mugging Check") && tArgs == 1)
    {
        lua_pushboolean(L, TilemapActor::xIsMugCheck);
        return 1;
    }
    //--True if this is a "non-combat" enemy pulse, such as mugging or auto-win checking.
    else if(!strcasecmp(rSwitchType, "Is Non Combat Check") && tArgs == 1)
    {
        lua_pushboolean(L, TilemapActor::xPulseIsNotCombat);
        return 1;
    }
    //--Whether or not the owning entity was mugged. Defaults to false.
    else if(!strcasecmp(rSwitchType, "Was Mugged") && tArgs == 1)
    {
        lua_pushboolean(L, TilemapActor::xActorWasMugged);
        return 1;
    }

    ///--[Dynamic Types]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    TilemapActor *rActiveActor = (TilemapActor *)rDataLibrary->rActiveObject;
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        LuaTypeError("TA_GetProperty", L);
        fprintf(stderr, " Data: ");
        for(int i = 0; i < tArgs; i ++) fprintf(stderr, "%s ", lua_tostring(L, i+1));
        fprintf(stderr, "\n");
        return 0;
    }

    ///--[Properties]
    //--ID of the TilemapActor.
    if(!strcasecmp(rSwitchType, "ID") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveActor->GetID());
        tReturns = 1;
    }
    //--Name of the actor.
    else if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        lua_pushstring(L, rActiveActor->GetName());
        tReturns = 1;
    }
    //--Position in world coordinates.
    else if(!strcasecmp(rSwitchType, "Position") && tArgs == 1)
    {
        lua_pushnumber(L, rActiveActor->GetWorldX());
        lua_pushnumber(L, rActiveActor->GetWorldY());
        tReturns = 2;
    }
    //--Facing.
    else if(!strcasecmp(rSwitchType, "Facing") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveActor->GetFacing());
        tReturns = 1;
    }
    //--How many images this entity uses in its walk cycle.
    else if(!strcasecmp(rSwitchType, "Move Images Total") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveActor->GetMovementFramesTotal());
        tReturns = 1;
    }
    //--Returns true if any collision is true on all sides.
    else if(!strcasecmp(rSwitchType, "Is Touching Collision") && tArgs == 1)
    {
        bool tIsClipped = rActiveActor->IsTopClipped();
        tIsClipped |= rActiveActor->IsBotClipped();
        tIsClipped |= rActiveActor->IsRgtClipped();
        tIsClipped |= rActiveActor->IsLftClipped();

        lua_pushboolean(L, tIsClipped);
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Is Lft Clipped") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsLftClipped());
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Is Top Clipped") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsTopClipped());
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Is Rgt Clipped") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsRgtClipped());
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Is Bot Clipped") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsBotClipped());
        tReturns = 1;
    }
    //--True if the named special frame exists for this NPC.
    else if(!strcasecmp(rSwitchType, "Has Special Frame") && tArgs >= 2)
    {
        lua_pushboolean(L, rActiveActor->HasSpecialFrame(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--True if a special frame is showing, false otherwise.
    else if(!strcasecmp(rSwitchType, "Is Showing Special Frame") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsShowingSpecialFrame());
        tReturns = 1;
    }
    //--Name of the showing special frame, or "Null" if one is not active.
    else if(!strcasecmp(rSwitchType, "Current Special Frame") && tArgs == 1)
    {
        lua_pushstring(L, rActiveActor->GetNameOfSpecialFrame());
        tReturns = 1;
    }
    ///--[Enemy Properties]
    //--True if enemy, false otherwise.
    else if(!strcasecmp(rSwitchType, "Is Enemy") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->IsEnemy("Any"));
        tReturns = 1;
    }
    //--Name of first enemy prototype, which is usually the only one.
    else if(!strcasecmp(rSwitchType, "Zeroth Enemy Name") && tArgs == 1)
    {
        lua_pushstring(L, rActiveActor->GetZerothEnemyPrototype());
        tReturns = 1;
    }
    //--If true, this entity drops nothing when mugged. XP/JP/Etc still drop.
    else if(!strcasecmp(rSwitchType, "Never Drops Items On Mug") && tArgs == 1)
    {
        lua_pushboolean(L, rActiveActor->NeverDropsItemsOnMug());
        tReturns = 1;
    }
    ///--[Debug]
    //--Prints the special frames this entity has to the console.
    else if(!strcasecmp(rSwitchType, "Print Special Frames") && tArgs == 1)
    {
        rActiveActor->PrintSpecialFrames();
        tReturns = 0;
    }
    ///--[Error]
    else
    {
        LuaPropertyError("TA_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_TA_SetProperty(lua_State *L)
{
    ///--[Argument List]
    //--[Static]
    //TA_SetProperty("Build Graphics From Name Path", sPath) (Static)
    //TA_SetProperty("Standard Enemy Shadow", sDLPath) (Static)
    //TA_SetProperty("Store Image Paths", bFlag) (Static)
    //TA_SetProperty("Reresolve All Images") (Static)
    //TA_SetProperty("Mug Platina", iPlatina) (Static)
    //TA_SetProperty("Mug Experience", iExperience) (Static)
    //TA_SetProperty("Mug Job Points", iJobPoints) (Static)
    //TA_SetProperty("Mug Item", sItemName) (Static)
    //TA_SetProperty("Enemies Always Ignore Player", bFlag) (Static)
    //TA_SetProperty("Combat Initiate Range", fDistance) (Static)

    //--[System]
    //TA_SetProperty("Disabled", bIsDisabled)
    //TA_SetProperty("Block Render", bIsBlocked)
    //TA_SetProperty("Name", sName)
    //TA_SetProperty("Position", iX, iY)
    //TA_SetProperty("Position By Entity", sEntityName)
    //TA_SetProperty("Depth", iDepth)
    //TA_SetProperty("Clipping Flag", bFlag)
    //TA_SetProperty("Set Ignore World Stop", bFlag)
    //TA_SetProperty("Set Ignore Special Lights", bFlag)
    //TA_SetProperty("Set Ignore Clips", bFlag)
    //TA_SetProperty("No Footstep Sound", bFlag)
    //TA_SetProperty("Use Eight Frames For Footsteps", bFlag)
    //TA_SetProperty("Reface On Node Flag", bFlag)
    //TA_SetProperty("Display String", sString, bClearAfterCombat)
    //TA_SetProperty("Responds To Shot", bFlag)

    //--[Instructions]
    //TA_SetProperty("Emulate Movement", iDirection, fSpeed)
    //TA_SetProperty("Handle Movement", iXPixels, iYPixels)
    //TA_SetProperty("Facing", iDirection)
    //TA_SetProperty("Face Position", iX, iY)
    //TA_SetProperty("Face Character", sNPCName)
    //TA_SetProperty("Stop Moving")
    //TA_SetProperty("Flashwhite")
    //TA_SetProperty("Flashwhite", sReserveFrame)
    //TA_SetProperty("Mug Now")

    //--[Enemy Properties]
    //TA_SetProperty("Defeat Scene", sDefeatScene)
    //TA_SetProperty("Activate Enemy Mode", sEnemyInternalName)
    //TA_SetProperty("Can Be Mugged", bFlag)
    //TA_SetProperty("Set Combat Enemy", sEnemyPrototype)
    //TA_SetProperty("Add Combat Enemy", sEnemyPrototype)
    //TA_SetProperty("Set Item Mode", sItemString)
    //TA_SetProperty("Get Stunned")
    //TA_SetProperty("Begin Dying")
    //TA_SetProperty("Toughness", iValue)
    //TA_SetProperty("Spotting Icon Offsets", pX, pY)
    //TA_SetProperty("Spotting Circle Offsets", pX, pY, pRadius, pThickness)
    //TA_SetProperty("Detection Angle", fAngle)
    //TA_SetProperty("Set Follow Target", sName)

    //--[Advanced Spotting]
    //TA_SetProperty("Eye Spotting", iEyeSprites, iBubbleSprites)
    //TA_SetProperty("Eye Sprite", iSlot, sDLPath)
    //TA_SetProperty("Bubble Sprite", iSlot, sDLPath)

    //--[NPC Properties]
    //TA_SetProperty("Activation Script", sScriptPath)
    //TA_SetProperty("Extended Activation Direction", iDirectionFlag)
    //TA_SetProperty("Activate Wander Mode")

    //--[Jumping]
    //TA_SetProperty("Y Vertical Offset", fOffset)
    //TA_SetProperty("Ignores Gravity", bFlag)
    //TA_SetProperty("Z Speed", fSpeed)
    //TA_SetProperty("Z Gravity", fGravity)
    //TA_SetProperty("Jumping Sound", sJumpingSound)
    //TA_SetProperty("Landing Sound", sLandingSound)

    //--[Rendering Properties]
    //TA_SetProperty("Disable Main Render", bFlag)
    //TA_SetProperty("Renders After Tiles", bFlag)
    //TA_SetProperty("Flying", bFlag)
    //TA_SetProperty("Auto Animates", bFlag)
    //TA_SetProperty("Auto Animates Fast", bFlag)
    //TA_SetProperty("Y Oscillates", bFlag)
    //TA_SetProperty("Tiny", bFlag)
    //TA_SetProperty("No Automatic Shadow", bFlag)
    //TA_SetProperty("Rendering Depth", fDepth)
    //TA_SetProperty("Rendering Depth Offset", fDepth)
    //TA_SetProperty("Rendering Offsets", fOffsetX, fOffsetY)
    //TA_SetProperty("Walk Ticks Per Frame", iValue)
    //TA_SetProperty("Run Ticks Per Frame", iValue)
    //TA_SetProperty("Move Frames Total", iTotal)
    //TA_SetProperty("Move Frame", iDirection, iSlot, sPath)
    //TA_SetProperty("Run Frame", iDirection, iSlot, sPath)
    //TA_SetProperty("Standing Frame", iDirection, sPath)
    //TA_SetProperty("Breathe Frame", iDirection, sPath)
    //TA_SetProperty("Shadow", sDLPath)
    //TA_SetProperty("Shake", iTicks)
    //TA_SetProperty("Void Rift")
    //TA_SetProperty("Special Idle")
    //TA_SetProperty("Render As Single Block")
    //TA_SetProperty("Whole Collision")
    //TA_SetProperty("Activate By Rub")

    //--[Special Frames]
    //TA_SetProperty("Add Special Frame", sRefName, sPath)
    //TA_SetProperty("Set Special Frame", sRefName)
    //TA_SetProperty("Wipe Special Frames")
    //TA_SetProperty("Create Special Frame Anim", sName)
    //TA_SetProperty("Set Special Frame Anim Length", sName, iFrames)
    //TA_SetProperty("Set Special Frame Anim Element", sName, iSlot, iTicks, sAnimName)
    //TA_SetProperty("Set Special Frame Anim Reset Frame", sName, iSlot)
    //TA_SetProperty("Activate Special Frame Anim", sName)

    //--[Idle Animations]
    //TA_SetProperty("Idle Animation Flag", bFlag)
    //TA_SetProperty("Allocate Idle Frames", iFrames)
    //TA_SetProperty("Set Idle Frame", iSlot, sDLPath)
    //TA_SetProperty("Set Idle TPF", iTPF)
    //TA_SetProperty("Set Idle Loop Frame", iLoopFrame)

    //--[Overlays]
    //TA_SetProperty("Set Manual Overlay", sDLPath)
    //TA_SetProperty("Set Auto Overlays", bFlag)
    //TA_SetProperty("Auto Overlay TPF", fTPF)
    //TA_SetProperty("Allocate Auto Overlays", iTotal)
    //TA_SetProperty("Auto Overlay Frame", iSlot, sDLPath)

    ///--[Argument Handling]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TA_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Types]
    //--Lua script to call when entities are building their graphics using Tiled provided names.
    if(!strcasecmp(rSwitchType, "Build Graphics From Name Path") && tArgs == 2)
    {
        ResetString(TilemapActor::xBuildGraphicsFromNamePath, lua_tostring(L, 2));
        return 0;
    }
    //--Sets what shadow appears under enemies.
    else if(!strcasecmp(rSwitchType, "Standard Enemy Shadow") && tArgs == 2)
    {
        ResetString(TilemapActor::xEnemyShadowPath, lua_tostring(L, 2));
        return 0;
    }
    //--If true, entities will store the DLPath they received when a NULL image is passed to them.
    //  Use "Reresolve All Images" once image loading completes to fix them.
    else if(!strcasecmp(rSwitchType, "Store Image Paths") && tArgs == 2)
    {
        TilemapActor::xShouldStoreImagePaths = lua_toboolean(L, 2);
        return 0;
    }
    //--If an entity received a path to an image that didn't exist because it wasn't loaded yet,
    //  this attempts to re-resolve the image(s) from the DataLibrary for ALL TilemapActors.
    else if(!strcasecmp(rSwitchType, "Reresolve All Images") && tArgs == 1)
    {
        TilemapActor::ReresolveAllImages();
        return 0;
    }
    //--Sets how much money the mugged enemy gave.
    else if(!strcasecmp(rSwitchType, "Mug Platina") && tArgs == 2)
    {
        TilemapActor::xMugPlatina = lua_tointeger(L, 2);
        return 0;
    }
    //--Sets how much experience the mugged enemy gave.
    else if(!strcasecmp(rSwitchType, "Mug Experience") && tArgs == 2)
    {
        TilemapActor::xMugExperience = lua_tointeger(L, 2);
        return 0;
    }
    //--Sets how much JP the mugged enemy gave.
    else if(!strcasecmp(rSwitchType, "Mug Job Points") && tArgs == 2)
    {
        TilemapActor::xMugJobPoints = lua_tointeger(L, 2);
        return 0;
    }
    //--Adds an item to the list of items dropped by the mugged enemy.
    else if(!strcasecmp(rSwitchType, "Mug Item") && tArgs == 2)
    {
        TilemapActor::xMugItemList->AddElement(lua_tostring(L, 2), &TilemapActor::xMugItemList);
        return 0;
    }
    //--If true, all enemies ignore the player. Used by field abilities.
    else if(!strcasecmp(rSwitchType, "Enemies Always Ignore Player") && tArgs == 2)
    {
        TilemapActor::xAlwaysIgnorePlayer = lua_toboolean(L, 2);
        return 0;
    }
    //--How many pixels (one tile = 16) enemies initiate combat at. Pass -1 to use the default.
    else if(!strcasecmp(rSwitchType, "Combat Initiate Range") && tArgs == 2)
    {
        TilemapActor::xCombatTriggerRange = lua_tonumber(L, 2);
        if(TilemapActor::xCombatTriggerRange <= 0.0f) TilemapActor::xCombatTriggerRange = TA_ENCOUNTER_DIST_DEFAULT;
        return 0;
    }

    ///--[Instantiated Types]
    //--Active object is required to exist.
    TilemapActor *rActiveActor = (TilemapActor *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        LuaTypeError("TA_SetProperty", L);
        fprintf(stderr, " Data: ");
        for(int i = 0; i < tArgs; i ++) fprintf(stderr, "%s ", lua_tostring(L, i+1));
        fprintf(stderr, "\n");
        return 0;
    }

    ///--[System]
    //--Disabled flag. Disabled entities still exist but don't update or render until re-enabled.
    if(!strcasecmp(rSwitchType, "Disabled") && tArgs == 2)
    {
        rActiveActor->SetDisable(lua_toboolean(L, 2));
    }
    //--Blocks rendering of this entity as long as the flag is active.
    else if(!strcasecmp(rSwitchType, "Block Render") && tArgs == 2)
    {
        rActiveActor->SetBlockRender(lua_toboolean(L, 2));
    }
    //--Name. Also affects the EntityManager's copy.
    else if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rActiveActor->SetName(lua_tostring(L, 2));
        EntityManager::Fetch()->RenamePointer(lua_tostring(L, 2), rActiveActor);
    }
    //--Position of Actor.
    else if(!strcasecmp(rSwitchType, "Position") && tArgs == 3)
    {
        rActiveActor->SetPosition(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Position of Actor, using another Entity as its base.
    else if(!strcasecmp(rSwitchType, "Position By Entity") && tArgs == 2)
    {
        rActiveActor->SetPositionByEntity(lua_tostring(L, 2));
    }
    //--Depth of the Actor.
    else if(!strcasecmp(rSwitchType, "Depth") && tArgs == 2)
    {
        rActiveActor->SetCollisionDepth(lua_tointeger(L, 2));
    }
    //--Collision flag.
    else if(!strcasecmp(rSwitchType, "Clipping Flag") && tArgs == 2)
    {
        rActiveActor->SetCollisionFlag(lua_toboolean(L, 2));
    }
    //--This NPC will ignore world stop due to cutscenes (and only cutscenes).
    else if(!strcasecmp(rSwitchType, "Set Ignore World Stop") && tArgs == 2)
    {
        rActiveActor->SetWorldStopIgnoreFlag(lua_toboolean(L, 2));
    }
    //--This NPC will not use the 2-tile tall lighting flag.
    else if(!strcasecmp(rSwitchType, "Set Ignore Special Lights") && tArgs == 2)
    {
        rActiveActor->SetIgnoreSpecialLights(lua_toboolean(L, 2));
    }
    //--This NPC will move through collisions if this flag is true.
    else if(!strcasecmp(rSwitchType, "Set Ignore Clips") && tArgs == 2)
    {
        rActiveActor->SetIgnoreClipsWhenMoving(lua_toboolean(L, 2));
    }
    //--NPC has no footstep sound when running.
    else if(!strcasecmp(rSwitchType, "No Footstep Sound") && tArgs == 2)
    {
        rActiveActor->SetNoFootstepSounds(lua_toboolean(L, 2));
    }
    //--If true, uses a different set of frames to play footsteps on.
    else if(!strcasecmp(rSwitchType, "Use Eight Frames For Footsteps") && tArgs == 2)
    {
        rActiveActor->SetUseEightFramesForFootsteps(lua_toboolean(L, 2));
    }
    //--If true, NPC doesn't stop and reface when touching a patrol node.
    else if(!strcasecmp(rSwitchType, "Reface On Node Flag") && tArgs == 2)
    {
        rActiveActor->SetDontRefaceFlag(lua_toboolean(L, 2));
    }
    //--Causes a string to appear over the entity's head. Optionally insta-clears after combat.
    else if(!strcasecmp(rSwitchType, "Display String") && tArgs == 3)
    {
        rActiveActor->SetUIDisplayString(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--If true, this entity will respond if a gunshot crosses its hitbox.
    else if(!strcasecmp(rSwitchType, "Responds To Shot") && tArgs == 2)
    {
        rActiveActor->SetRespondsToShot(lua_toboolean(L, 2));
    }
    ///--[Instructions]
    //--Emulates movement for one tick in the given speed set.
    else if(!strcasecmp(rSwitchType, "Emulate Movement") && tArgs == 3)
    {
        bool tEmuLft = false;
        bool tEmuTop = false;
        bool tEmuRgt = false;
        bool tEmuBot = false;
        int tFacing = lua_tointeger(L, 2);
        if(tFacing == DIR_NORTH)
        {
            tEmuTop = true;
        }
        else if(tFacing == DIR_NE)
        {
            tEmuTop = true;
            tEmuRgt = true;
        }
        else if(tFacing == DIR_EAST)
        {
            tEmuRgt = true;
        }
        else if(tFacing == DIR_SE)
        {
            tEmuRgt = true;
            tEmuBot = true;
        }
        else if(tFacing == DIR_SOUTH)
        {
            tEmuBot = true;
        }
        else if(tFacing == DIR_SW)
        {
            tEmuBot = true;
            tEmuLft = true;
        }
        else if(tFacing == DIR_WEST)
        {
            tEmuLft = true;
        }
        else if(tFacing == DIR_NW)
        {
            tEmuLft = true;
            tEmuTop = true;
        }

        rActiveActor->EmulateMovement(tEmuLft, tEmuTop, tEmuRgt, tEmuBot, lua_tonumber(L, 3), lua_tonumber(L, 3));
    }
    //--Moves the entity the given number of pixels, respecting slopes and collisions.
    else if(!strcasecmp(rSwitchType, "Handle Movement") && tArgs == 3)
    {
        rActiveActor->HandleMovement(lua_tointeger(L, 2), lua_tointeger(L, 3), true);
    }
    //--Manually set the facing direction.
    else if(!strcasecmp(rSwitchType, "Facing") && tArgs == 2)
    {
        rActiveActor->SetFacing(lua_tointeger(L, 2));
    }
    //--Set facing to a position.
    else if(!strcasecmp(rSwitchType, "Face Position") && tArgs == 3)
    {
        rActiveActor->SetFacingToPoint(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Set facing to an NPC.
    else if(!strcasecmp(rSwitchType, "Face Character") && tArgs == 2)
    {
        rActiveActor->SetFacingToNPC(lua_tostring(L, 2));
    }
    //--Stops movement animation. Useful for making cutscenes look servicable.
    else if(!strcasecmp(rSwitchType, "Stop Moving") && tArgs == 1)
    {
        rActiveActor->StopMoving();
    }
    //--Flash the character's sprite to white and then back down.
    else if(!strcasecmp(rSwitchType, "Flashwhite") && tArgs == 1)
    {
        rActiveActor->ActivateFlashwhite();
    }
    //--Flash the character's sprite to white and then back down, switching to a special frame partway through.
    else if(!strcasecmp(rSwitchType, "Flashwhite") && tArgs == 2)
    {
        rActiveActor->ActivateFlashwhite();
        rActiveActor->SetReserveFrame(lua_tostring(L, 2));
    }
    //--Immediately mugs the entity.
    else if(!strcasecmp(rSwitchType, "Mug Now") && tArgs == 1)
    {
        rActiveActor->HandleMugging();
    }
    ///--[Enemy Properties]
    //--Changes the scene that plays when this enemy is defeated.
    else if(!strcasecmp(rSwitchType, "Defeat Scene") && tArgs == 2)
    {
        rActiveActor->SetDefeatScene(lua_tostring(L, 2));
    }
    //--Flags this entity to become an enemy and trigger fights on contact.
    else if(!strcasecmp(rSwitchType, "Activate Enemy Mode") && tArgs == 2)
    {
        rActiveActor->ActivateEnemyMode(lua_tostring(L, 2));
        rActiveActor->SetLeashingPoint(rActiveActor->GetWorldX(), rActiveActor->GetWorldY());
        rActiveActor->SetMuggable(true);
    }
    //--Flags if the enemy can be mugged or not. Enemies are muggable by default.
    else if(!strcasecmp(rSwitchType, "Can Be Mugged") && tArgs == 2)
    {
        rActiveActor->SetMuggable(lua_toboolean(L, 2));
    }
    //--Clears the existing prototype list and sets the first enemy.
    else if(!strcasecmp(rSwitchType, "Set Combat Enemy") && tArgs >= 2)
    {
        rActiveActor->SetCombatEnemy(lua_tostring(L, 2));
    }
    //--Registers a combat enemy when this entity enters the battle.
    else if(!strcasecmp(rSwitchType, "Add Combat Enemy") && tArgs >= 2)
    {
        rActiveActor->AddCombatEnemy(lua_tostring(L, 2));
    }
    //--Flags the item to act as a loot pickup. It will provide the given item or items when touched. Items are separated by
    //  a bar. Ex: "Potion|Elixir"
    else if(!strcasecmp(rSwitchType, "Set Item Mode") && tArgs == 2)
    {
        rActiveActor->ActivateFallenLootMode(lua_tostring(L, 2));
    }
    //--Immediately gets stunned.
    else if(!strcasecmp(rSwitchType, "Get Stunned") && tArgs == 1)
    {
        rActiveActor->BeginStun();
    }
    //--Dies in a dramatic fashion.
    else if(!strcasecmp(rSwitchType, "Begin Dying") && tArgs == 1)
    {
        rActiveActor->BeginDying();
    }
    //--Sets the outline for this entity.
    else if(!strcasecmp(rSwitchType, "Toughness") && tArgs == 2)
    {
        rActiveActor->SetToughness(lua_tointeger(L, 2));
    }
    //--Sets the offset on the spotting icon.
    else if(!strcasecmp(rSwitchType, "Spotting Icon Offsets") && tArgs == 3)
    {
        rActiveActor->SetSpottingIconOffsets(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Sets where and how large the spotting circle for an enemy is.
    else if(!strcasecmp(rSwitchType, "Spotting Circle Offsets") && tArgs == 5)
    {
        rActiveActor->SetSpottingCircleOffsets(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Detection angle, in degrees.
    else if(!strcasecmp(rSwitchType, "Detection Angle") && tArgs == 2)
    {
        rActiveActor->SetDetectAngle(lua_tonumber(L, 2));
    }
    //--Sets which NPC the actor is following. Used in enemy mode.
    else if(!strcasecmp(rSwitchType, "Set Follow Target") && tArgs >= 2)
    {
        rActiveActor->SetFollowTarget(lua_tostring(L, 2));
    }
    ///--[Advanced Spotting]
    //--Activates "eye spotting" mode, an alternate way of displaying enemy detection. Pass both 0's to disable.
    else if(!strcasecmp(rSwitchType, "Eye Spotting") && tArgs == 3)
    {
        rActiveActor->SetEyeSpotting(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets an eye frame for eye-spotting mode.
    else if(!strcasecmp(rSwitchType, "Eye Sprite") && tArgs == 3)
    {
        rActiveActor->SetEyeSprite(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Sets an alert bubble for eye-spotting mode.
    else if(!strcasecmp(rSwitchType, "Bubble Sprite") && tArgs == 3)
    {
        rActiveActor->SetBubbleSprite(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    ///--[NPC Properties]
    //--Activation script.
    else if(!strcasecmp(rSwitchType, "Activation Script") && tArgs == 2)
    {
        rActiveActor->SetActivationScript(lua_tostring(L, 2));
    }
    //--Direction of extended activation, for counters. Cannot be a diagonal.
    else if(!strcasecmp(rSwitchType, "Extended Activation Direction") && tArgs == 2)
    {
        rActiveActor->SetExtendedActivationDirection(lua_tointeger(L, 2));
    }
    //--Causes this NPC to activate enemy mode, but to always ignore the player.
    else if(!strcasecmp(rSwitchType, "Activate Wander Mode") && tArgs == 1)
    {
        rActiveActor->ActivateWanderMode();
    }
    ///--[Jumping]
    //--Y position relative to the shadow. Only used for display.
    else if(!strcasecmp(rSwitchType, "Y Vertical Offset") && tArgs == 2)
    {
        rActiveActor->SetYVerticalOffset(lua_tonumber(L, 2));
    }
    //--If set to false, actor "falls" when jumping. Gravity is off by default.
    else if(!strcasecmp(rSwitchType, "Ignores Gravity") && tArgs == 2)
    {
        rActiveActor->SetIgnoresGravity(lua_toboolean(L, 2));
    }
    //--Sets current Z speed. Used to make an entity "jump".
    else if(!strcasecmp(rSwitchType, "Z Speed") && tArgs == 2)
    {
        rActiveActor->SetZSpeed(lua_tonumber(L, 2));
    }
    //--Gravity value when jumping.
    else if(!strcasecmp(rSwitchType, "Z Gravity") && tArgs == 2)
    {
        rActiveActor->SetGravity(lua_tonumber(L, 2));
    }
    //--Sound the character makes when jumping using an actor even. Pass "NULL" to clear.
    else if(!strcasecmp(rSwitchType, "Jumping Sound") && tArgs == 2)
    {
        rActiveActor->SetJumpingSound(lua_tostring(L, 2));
    }
    //--Sound the character makes when landing. Pass "NULL" to clear.
    else if(!strcasecmp(rSwitchType, "Landing Sound") && tArgs == 2)
    {
        rActiveActor->SetLandingSound(lua_tostring(L, 2));
    }
    ///--[Rendering Properties]
    //--If true, do not render main body. Other things, like shadow and overlay, do render.
    else if(!strcasecmp(rSwitchType, "Disable Main Render") && tArgs == 2)
    {
        rActiveActor->SetDisableMainRender(lua_toboolean(L, 2));
    }
    //--Sets whether the entity renders after tiles do, used for special transparencies.
    else if(!strcasecmp(rSwitchType, "Renders After Tiles") && tArgs == 2)
    {
        rActiveActor->SetRendersAfterTiles(lua_toboolean(L, 2));
    }
    //--Entity is flying and ignores deep water.
    else if(!strcasecmp(rSwitchType, "Flying") && tArgs == 2)
    {
        rActiveActor->SetFlyingFlag(lua_toboolean(L, 2));
    }
    //--Entity's animation timer runs even if they're not moving.
    else if(!strcasecmp(rSwitchType, "Auto Animates") && tArgs == 2)
    {
        rActiveActor->SetAutoAnimateFlag(lua_toboolean(L, 2));
    }
    //--Entity's animation timer runs every tick instead of every 3 ticks.
    else if(!strcasecmp(rSwitchType, "Auto Animates Fast") && tArgs == 2)
    {
        rActiveActor->SetAutoAnimateFlag(lua_toboolean(L, 2));
        rActiveActor->SetAutoAnimateFastFlag(lua_toboolean(L, 2));
    }
    //--Entity's Y position oscillates up and down slightly.
    else if(!strcasecmp(rSwitchType, "Y Oscillates") && tArgs == 2)
    {
        rActiveActor->SetOscillateFlag(lua_toboolean(L, 2));
    }
    //--Set the tiny flag. Used for some enemies when they double as NPCs.
    else if(!strcasecmp(rSwitchType, "Tiny") && tArgs == 2)
    {
        rActiveActor->SetTinyFlag(lua_toboolean(L, 2));
    }
    //--NPC has no automatic shadow rendering.
    else if(!strcasecmp(rSwitchType, "No Automatic Shadow") && tArgs == 2)
    {
        rActiveActor->DisableAutomaticShadow(lua_toboolean(L, 2));
    }
    //--Rendering Depth of the Actor.
    else if(!strcasecmp(rSwitchType, "Rendering Depth") && tArgs == 2)
    {
        rActiveActor->SetOverrideDepth(lua_tonumber(L, 2));
        rActiveActor->SetIgnoreDepthOverride(true);
    }
    //--Depth offset from the midground computation.
    else if(!strcasecmp(rSwitchType, "Rendering Depth Offset") && tArgs == 2)
    {
        rActiveActor->SetOverrideDepthOffset(lua_tonumber(L, 2));
    }
    //--Set rendering offsets. Defaults are -8.0, -16.0
    else if(!strcasecmp(rSwitchType, "Rendering Offsets") && tArgs == 3)
    {
        rActiveActor->SetOffsets(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--How many ticks are used in the walk cycle for each frame.
    else if(!strcasecmp(rSwitchType, "Walk Ticks Per Frame") && tArgs == 2)
    {
        rActiveActor->SetFrameSpeed(lua_tonumber(L, 2));
    }
    //--How many ticks are used in the run cycle for each frame.
    else if(!strcasecmp(rSwitchType, "Run Ticks Per Frame") && tArgs == 2)
    {
        rActiveActor->SetRunFrameSpeed(lua_tonumber(L, 2));
    }
    //--How many walk/run frames there are. Clamps between 1 and TA_MOVE_IMG_MAXIMUM.
    else if(!strcasecmp(rSwitchType, "Move Frames Total") && tArgs == 2)
    {
        rActiveActor->SetMoveFramesTotal(lua_tointeger(L, 2));
    }
    //--Movement frames.
    else if(!strcasecmp(rSwitchType, "Move Frame") && tArgs == 4)
    {
        rActiveActor->SetMoveImage(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Movement frames, running.
    else if(!strcasecmp(rSwitchType, "Run Frame") && tArgs == 4)
    {
        rActiveActor->SetRunImage(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Standing frame.
    else if(!strcasecmp(rSwitchType, "Standing Frame") && tArgs == 3)
    {
        rActiveActor->SetStandingImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Breathe frame.
    else if(!strcasecmp(rSwitchType, "Breathe Frame") && tArgs == 3)
    {
        rActiveActor->SetBreatheImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Shadow. Set to "Null" to disable shadows.
    else if(!strcasecmp(rSwitchType, "Shadow") && tArgs == 2)
    {
        rActiveActor->SetShadow(lua_tostring(L, 2));
    }
    //--Actor shakes for the given number of ticks.
    else if(!strcasecmp(rSwitchType, "Shake") && tArgs == 2)
    {
        rActiveActor->SetShakeTicks(lua_tointeger(L, 2));
    }
    //--Special animation properties used by Void Rifts.
    else if(!strcasecmp(rSwitchType, "Void Rift") && tArgs == 1)
    {
        rActiveActor->SetVoidRiftMode();
    }
    //--Special idle, used by some enemies like Scraprats.
    else if(!strcasecmp(rSwitchType, "Special Idle") && tArgs == 1)
    {
        rActiveActor->SetSpecialIdleFlag(true);
    }
    //--Special flag for blocks kicked during puzzles. These are 16 px tall instead of 24/32 like most entities.
    else if(!strcasecmp(rSwitchType, "Render As Single Block") && tArgs == 1)
    {
        rActiveActor->SetRenderAsSingleBlock();
    }
    //--Makes the entity occupy all 16px instead of a small block in the middle like NPCs.
    else if(!strcasecmp(rSwitchType, "Whole Collision") && tArgs == 1)
    {
        rActiveActor->SetUseWholeCollision();
    }
    //--Activates by the player walking into the object.
    else if(!strcasecmp(rSwitchType, "Activate By Rub") && tArgs == 1)
    {
        rActiveActor->SetActivesByRub(true);
    }
    ///--[Special Frames]
    //--Special frames. Per-character, used by cutscenes.
    else if(!strcasecmp(rSwitchType, "Add Special Frame") && tArgs == 3)
    {
        rActiveActor->AddSpecialFrame(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Activates a special frame previous created with "Add Special Frame".
    else if(!strcasecmp(rSwitchType, "Set Special Frame") && tArgs == 2)
    {
        rActiveActor->ActivateSpecialFrame(lua_tostring(L, 2));
    }
    //--Remove outstanding special frames.
    else if(!strcasecmp(rSwitchType, "Wipe Special Frames") && tArgs == 1)
    {
        rActiveActor->WipeSpecialFrames();
    }
    //--Creates a special frame animation.
    else if(!strcasecmp(rSwitchType, "Create Special Frame Anim") && tArgs == 2)
    {
        rActiveActor->CreateSpecialFrameAnim(lua_tostring(L, 2));
    }
    //--Sets how many frames total are in a special frame animation.
    else if(!strcasecmp(rSwitchType, "Set Special Frame Anim Length") && tArgs == 3)
    {
        rActiveActor->SetSpecialFrameAnimLen(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the timing and animation of a special frame animation element.
    else if(!strcasecmp(rSwitchType, "Set Special Frame Anim Element") && tArgs == 5)
    {
        rActiveActor->SetSpecialFrameAnimCase(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5));
    }
    //--When an animation loops, it loops to the specified frame (or 0 by default).
    else if(!strcasecmp(rSwitchType, "Set Special Frame Anim Reset Frame") && tArgs == 3)
    {
        rActiveActor->SetSpecialFrameAnimeResetFrame(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Activates a special frame animation. Pass "Null" to stop.
    else if(!strcasecmp(rSwitchType, "Activate Special Frame Anim") && tArgs == 2)
    {
        rActiveActor->ActivateSpecialFrameAnim(lua_tostring(L, 2));
    }
    ///--[Idle Animations]
    //--Set to true to enable idle animations, false otherwise.
    else if(!strcasecmp(rSwitchType, "Idle Animation Flag") && tArgs == 2)
    {
        rActiveActor->SetIdleModeActive(lua_toboolean(L, 2));
    }
    //--Sets how many frames long the animation is.
    else if(!strcasecmp(rSwitchType, "Allocate Idle Frames") && tArgs == 2)
    {
        rActiveActor->AllocateIdleFrames(lua_tointeger(L, 2));
    }
    //--Sets which frame displays at the given slot.
    else if(!strcasecmp(rSwitchType, "Set Idle Frame") && tArgs == 3)
    {
        rActiveActor->SetIdleFrame(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Sets how many ticks per frame for the idle animation.
    else if(!strcasecmp(rSwitchType, "Set Idle TPF") && tArgs == 2)
    {
        rActiveActor->SetIdleTPF(lua_tointeger(L, 2));
    }
    //--Sets which frame the idle animation loops on. Set to -1 to disable looping.
    else if(!strcasecmp(rSwitchType, "Set Idle Loop Frame") && tArgs == 2)
    {
        rActiveActor->SetIdleLoopFrame(lua_tointeger(L, 2));
    }
    ///--[Overlays]
    //--Specifies an overlay for the entity. Pass "Null" to disable.
    else if(!strcasecmp(rSwitchType, "Set Manual Overlay") && tArgs == 2)
    {
        rActiveActor->SetManualRenderOverlay(lua_tostring(L, 2));
    }
    //--Toggles on or off automatic overlay display.
    else if(!strcasecmp(rSwitchType, "Set Auto Overlays") && tArgs == 2)
    {
        rActiveActor->SetOverlaysActive(lua_toboolean(L, 2));
    }
    //--Sets ticks-per-frame for automated overlays.
    else if(!strcasecmp(rSwitchType, "Auto Overlay TPF") && tArgs == 2)
    {
        rActiveActor->SetOverlayTPF(lua_tonumber(L, 2));
    }
    //--Sets total automated overlay frames.
    else if(!strcasecmp(rSwitchType, "Allocate Auto Overlays") && tArgs == 2)
    {
        rActiveActor->AllocateOverlayFrames(lua_tointeger(L, 2));
    }
    //--Gets image data for a given automated overlay.
    else if(!strcasecmp(rSwitchType, "Auto Overlay Frame") && tArgs == 3)
    {
        rActiveActor->SetOverlayFrame(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("TA_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_TA_ChangeCollisionFlag(lua_State *L)
{
    //TA_ChangeCollisionFlag(sEntityName, bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("TA_ChangeCollisionFlag");

    //--Get the actor in question.
    TilemapActor *rActiveActor = (TilemapActor *)EntityManager::Fetch()->GetEntity(lua_tostring(L, 1));
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return LuaTypeError("TA_ChangeCollisionFlag", L);

    //--Set.
    rActiveActor->SetCollisionFlag(lua_toboolean(L, 2));
    return 0;
}
