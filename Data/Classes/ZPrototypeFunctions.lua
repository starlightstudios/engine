--Instructions
--Replace [ClassSample] with the name of the class
--Balance headers
--Write a file description
--Make functions and stuff
--Delete these instructions

-- |[ ================================ [ClassSample] Functions ================================= ]|
--Description here

-- |[ =============================== [ClassSample]:fnDoAThing() =============================== ]|
--Function description
function [ClassSample]:fnDoAThing(pzaListing)
    
    -- |[Argument Check]|
    if(pzaListing == nil) then return end
    
    --Do stuff
end