//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
ControlManager::ControlManager()
{
    ///--[Variables]
    //--System
    mEnabled = true;
    mRefireRate = 30; //--In milliseconds

    //--Profiles
    mIsInProfileMode = false;
    mProfileList = new StarLinkedList(true);

    //--Flags
    mIsAnyThingPressed = false;
    mIsAnyKeyPressed = false;
    mIsAnyKeyDown = false;
    mIsAnyKeyReleased = false;

    //--Storage
    mPressedKeyboardKey =  0;
    mPressedMouseButton = 0;
    mPressedJoypadButton = 0;
    memset(mDownKeys, 0, sizeof(bool) * KEYCODES_MAX);

    //--Input
    mControlList = new StarLinkedList(true);
    SetMemoryData(__FILE__, __LINE__);
    mDummyState = (ControlState *)starmemoryalloc(sizeof(ControlState));

    //--SDL Input Buffer
    #if defined _SDL_PROJECT_
    mInputBuffer = new StarLinkedList(true);
    #endif

    //--Mouse
    mMouseX = -100.0f;
    mMouseY = -100.0f;
    mMouseZ = -100.0f;

    //--SDL Joystick
    #if defined _SDL_PROJECT_
        mJoystickHandle = NULL;
    #endif

    //--Names
    mKeyboardNamesTotal = 0;
    mKeyboardNames = NULL;
    mMouseNamesTotal = 0;
    mMouseNames = NULL;
    mJoypadNamesTotal = 0;
    mJoypadNames = NULL;
    BuildKeyNameLookups();

    //--Keyboard Input Buffering
    mIsShifted = false;
    mKeyboardInputBuffer = new StarLinkedList(true);

    //--Control Images
    mControlImagePacksTotal = 0;
    rErrorImage = NULL;
    mControlImagePacks = NULL;

    //--Public Variables
    mShifted = false;
    mCaps = false;
    mCtrl = false;
    #if defined _ALLEGRO_PROJECT_
    mAllegroHasJoystickUpdate = false;
    rKeyboardStatePtr = NULL;
    rMouseStatePtr = NULL;
    #endif

    ///--[SDL Joystick Setup]
    #if defined _SDL_PROJECT_
    if(SDL_NumJoysticks() > 0)
    {
        mJoystickHandle = SDL_JoystickOpen(0);
    }
    #endif

    ///--[Report]
    #if defined _ALLEGRO_PROJECT_
    if(false)
    {
        fprintf(stderr, "Control Manager Joystick Report.\n");
        fprintf(stderr, "Number of connected joysticks: %i\n", al_get_num_joysticks());
        for(int i = 0; i < al_get_num_joysticks(); i ++)
        {
            //--Retrieve.
            ALLEGRO_JOYSTICK *rJoystick = al_get_joystick(i);
            if(!rJoystick) continue;

            //--Basics.
            fprintf(stderr, " Joystick in slot %i: %s\n", i, al_get_joystick_name(rJoystick));
            fprintf(stderr, "  Has %i buttons, %i sticks.\n", al_get_joystick_num_buttons(rJoystick), al_get_joystick_num_sticks(rJoystick));

            //--Buttons.
            fprintf(stderr, "  Button names:\n");
            for(int p = 0; p < al_get_joystick_num_buttons(rJoystick); p ++)
            {
                fprintf(stderr, "   %02i: %s\n", p, al_get_joystick_button_name(rJoystick, p));
            }
        }
    }
    #endif

    #if defined _SDL_PROJECT_
    if(false)
    {
        fprintf(stderr, "Control Manager Joystick Report.\n");
        fprintf(stderr, "Number of connected joysticks: %i\n", SDL_NumJoysticks());
        for(int i = 0; i < SDL_NumJoysticks(); i ++)
        {
            //--Retrieve.
            SDL_Joystick *tJoystick = SDL_JoystickOpen(i);
            if(!tJoystick) continue;

            //--Basics.
            fprintf(stderr, " Joystick in slot %i: %s\n", i, SDL_JoystickName(tJoystick));
            fprintf(stderr, "  Has %i buttons, %i sticks.\n", SDL_JoystickNumButtons(tJoystick), SDL_JoystickNumAxes(tJoystick));

            //--Clean.
            SDL_JoystickClose(tJoystick);
        }
    }
    #endif
}
ControlManager::~ControlManager()
{
    delete mProfileList;

    delete mControlList;
    free(mDummyState);

    for(int i = 0; i < mKeyboardNamesTotal; i ++) free(mKeyboardNames[i]);
    for(int i = 0; i < mMouseNamesTotal; i ++)    free(mMouseNames[i]);
    for(int i = 0; i < mJoypadNamesTotal; i ++)   free(mJoypadNames[i]);
    free(mKeyboardNames);
    free(mMouseNames);
    free(mJoypadNames);
    delete mKeyboardInputBuffer;
    free(mControlImagePacks);

    //--SDL Joystick
    #if defined _SDL_PROJECT_
        if(SDL_JoystickGetAttached(mJoystickHandle))
            SDL_JoystickClose(mJoystickHandle);
    #endif
}

///===================================== Property Queries =========================================
bool ControlManager::GetEnabledState()
{
    return mEnabled;
}
void ControlManager::GetMouseCoords(int &sMouseX, int &sMouseY, int &sMouseZ)
{
    sMouseX = mMouseX;
    sMouseY = mMouseY;
    sMouseZ = mMouseZ;
}
void ControlManager::GetMouseCoordsF(float &sMouseX, float &sMouseY, float &sMouseZ)
{
    sMouseX = (float)mMouseX;
    sMouseY = (float)mMouseY;
    sMouseZ = (float)mMouseZ;
}
ControlState *ControlManager::GetControlState(const char *pName)
{
    ControlState *rState = (ControlState *)mControlList->GetElementByName(pName);
    if(rState) return rState;

    memset(mDummyState, 0, sizeof(ControlState));
    return mDummyState;
}
bool ControlManager::IsAnyKeyPressed()
{
    return mIsAnyKeyPressed;
}
bool ControlManager::ControlManager::IsAnyKeyDown()
{
    for(int i = 0; i < KEYCODES_MAX; i ++)
    {
        if(mDownKeys[i]) return true;
    }

    return false;
}
bool ControlManager::IsDown(const char *pName)
{
    return GetControlState(pName)->mIsDown;
}
bool ControlManager::IsFirstPress(const char *pName)
{
    return GetControlState(pName)->mIsFirstPress;
}
bool ControlManager::IsFirstRelease(const char *pName)
{
    return GetControlState(pName)->mIsFirstRelease;
}
void ControlManager::GetKeyPressCodes(int &sKeyboard, int &sMouse, int &sJoypad)
{
    sKeyboard = mPressedKeyboardKey;
    sMouse = mPressedMouseButton;
    sJoypad = mPressedJoypadButton;
}
char ControlManager::PopKeyboardInputBuffer()
{
    //--Returns an ASCII keycode if there was one on the input buffer, or '\0' if there wasn't.
    char *rCharacter = (char *)mKeyboardInputBuffer->GetHead();
    if(!rCharacter) return '\0';

    char tReturn = *rCharacter;
    mKeyboardInputBuffer->DeleteHead();
    return tReturn;
}

///======================================== Manipulators ==========================================
void ControlManager::SetEnabledState(bool pFlag)
{
    mEnabled = pFlag;
}
void ControlManager::SetMouseCoordsByDisplay(int pMouseX, int pMouseY, int pMouseZ)
{
    //--Sets the mouse coordinates according to DISPLAY POSITIONS, that is, on the real window with
    //  hardware inputs. Do not use this for virtual canvas positions.
    float tMouseX = pMouseX;
    float tMouseY = pMouseY;

    //--If the Z mouse is -700, then it didn't change.
    if(pMouseZ == -700) pMouseZ = mMouseZ;

    //--Skip renormalizing if the mouse is locked by the display manager. The results would be nonsense.
    if(!DisplayManager::Fetch()->IsMouseLocked()) DisplayManager::NormalizeMouse(tMouseX, tMouseY);

    //--Upload.
    mMouseX = tMouseX;// * rGlobal->gScreenWidthPixels  / rGlobal->gWindowWidth;
    mMouseY = tMouseY;// * rGlobal->gScreenHeightPixels / rGlobal->gWindowHeight;
    mMouseZ = pMouseZ;
}
void ControlManager::SetMouseCoordsByCanvas(int pMouseX, int pMouseY, int pMouseZ)
{
    //--Sets the mouse coordinates according to the VIRTUAL CANVAS. If you're setting the mouse position
    //  internally, here is where you'd do it. This should not be called by mouse events.
    mMouseX = pMouseX;
    mMouseY = pMouseY;
    if(pMouseZ != -700) mMouseZ = pMouseZ;
}
void ControlManager::SetMouseZRelative(int pMouseZChange)
{
    //--Changes the mouse Z value. Used only by SDL which tracks X/Y differently than Z. Pass 0 for no change.
    mMouseZ += pMouseZChange;
}
void ControlManager::BlankControls()
{
    //--All controls are reset back to neutral.
    ControlState *rControl = (ControlState *)mControlList->PushIterator();
    while(rControl)
    {
        rControl->mIsFirstPress = false;
        rControl->mIsFirstRelease = false;
        rControl->mIsDown = false;
        rControl->mHasPendingRelease = false;
        rControl = (ControlState *)mControlList->AutoIterate();
    }
}
void ControlManager::ClearControlFlags()
{
    mIsAnyKeyPressed = false;
    mIsAnyKeyDown = false;
    mIsAnyKeyReleased = false;
    mPressedKeyboardKey = -1;
    mPressedMouseButton = -1;
    mPressedJoypadButton = -1;
}
void ControlManager::RegisterControl(const char *pName, ControlState *pState)
{
    if(!pName || !pState) return;
    mControlList->AddElement(pName, pState, &FreeThis);
}
void ControlManager::SetShiftFlag(bool pFlag)
{
    mIsShifted = pFlag;
}
void ControlManager::RegisterProfile(const char *pName)
{
    //--Pushes on a new profile. In case of failure, pushes NULL for safety.
    rLastReggedProfile = NULL;
    mIsInProfileMode = false;
    if(!pName) return;

    StarLinkedList *nList = new StarLinkedList(true);
    mProfileList->AddElement(pName, nList, &StarLinkedList::DeleteThis);
    rLastReggedProfile = nList;
    mIsInProfileMode = true;
}
void ControlManager::PopProfileStack()
{
    //--Clears the last regged profile.
    rLastReggedProfile = NULL;
    mIsInProfileMode = false;
}
void ControlManager::ActivateProfile(const char *pName)
{
    //--Activates the requested profile, triggering all its pending rebinds.
    if(!pName) return;

    //--Fetch the profile.
    StarLinkedList *rProfile = (StarLinkedList *)mProfileList->GetElementByName(pName);
    if(!rProfile) return;

    //--The profile comes in sets of three strings, each corresponding to a control and its rebind
    //  commands. Execute them as threes.
    int tListSize = rProfile->GetListSize();
    for(int i = 0; i < tListSize; i += 3)
    {
        char *rName = (char *)rProfile->GetElementBySlot(i+0);
        char *rArg0 = (char *)rProfile->GetElementBySlot(i+1);
        char *rArg1 = (char *)rProfile->GetElementBySlot(i+2);
        RebindControl(rName, rArg0, rArg1);
    }
}

///======================================== Core Methods ==========================================
StarLinkedList *ControlManager::CopyControlList()
{
    //--Creates a list of copies of the existing control states.  This list can be modified or
    //  discarded without affecting the master list.  You must deallocate it when done.
    StarLinkedList *nList = new StarLinkedList(true);

    //--Each one must be added individually, this is a copy, not a reference copy.
    ControlState *rControlState = (ControlState *)mControlList->PushIterator();
    while(rControlState)
    {
        //--Clone it
        SetMemoryData(__FILE__, __LINE__);
        ControlState *nState = (ControlState *)starmemoryalloc(sizeof(ControlState));
        memcpy(nState, rControlState, sizeof(ControlState));
        nList->AddElement(mControlList->GetIteratorName(), nState, &FreeThis);

        //--Iterate
        rControlState = (ControlState *)mControlList->AutoIterate();
    }

    //--Return new list.
    return nList;
}
bool ControlManager::IsJoystickActive(int pIndex)
{
    ///--[Documentation]
    //--Because the indexs can refer to either sticks or axes, we must resolve that part first, then
    //  return whether or not that index is currently active in the state.
    if(pIndex < 0) return false;

    ///--[Allegro Version]
    #if defined _ALLEGRO_PROJECT_

        //--Resolve which joystick and code we are operating on. The index may be over JOYSTICK_OFFSET_PER
        //  which means we're not using the 0th pad.
        int tPadIndex = pIndex / JOYSTICK_OFFSET_PER;
        int tUseIndex = pIndex % JOYSTICK_OFFSET_PER;

        //--Get the joystick structure associated with the index. If it's null, it was out of range.
        if(tPadIndex >= al_get_num_joysticks()) return false;
        ALLEGRO_JOYSTICK *rJoystick = al_get_joystick(tPadIndex);
        if(!rJoystick) return false;

        //--Store the state data with the joystick.
        ALLEGRO_JOYSTICK_STATE tStateData;
        al_get_joystick_state(rJoystick, &tStateData);

        //--Button case.
        if(tUseIndex < JOYSTICK_OFFSET_STICK)
        {
            if(tUseIndex >= al_get_joystick_num_buttons(rJoystick)) return false;
            bool tReturn = tStateData.button[tUseIndex];

            return tReturn;
        }
        //--Stick case.
        else
        {
            //--Remove the stick constant.  Makes the math easier.
            tUseIndex -= JOYSTICK_OFFSET_STICK;

            //--Figure out which stick to query.
            int tCheckStick = tUseIndex / 10;
            tUseIndex = tUseIndex % 10;
            if(tCheckStick >= al_get_joystick_num_sticks(rJoystick)) return false;

            //--Figure out which axis to query.
            int tCheckAxis = tUseIndex / 2;
            tUseIndex = tUseIndex % 2;

            //--Positive or negative?
            float tResult = tStateData.stick[tCheckStick].axis[tCheckAxis];
            if(tResult < -0.3f && tUseIndex == 0)
                return true;
            else if(tResult > 0.3f && tUseIndex == 1)
                return true;
        }

    ///--[SDL Version]
    #elif defined _SDL_PROJECT_

        //--In SDL, sticks and axes are not tracked independently of one another. Instead, all axes are
        //  stored in a single list. So, the left stick may have Axis 100 and 102, and the right stick might be 104 and 106.
        //

    #endif

    //--All passes failed, false.
    return false;
}
char *ControlManager::GetNameOfKeyIndex(int pIndex)
{
    //--Returns a heap-allocated string representing the name of the key. This is done via the index
    //  as opposed to a control's name.
    //--Do not deallocate the returned string until program halt.
    if(pIndex >= 0 && pIndex < mKeyboardNamesTotal) return mKeyboardNames[pIndex];

    //--Out of range.
    return NULL;
}
char *ControlManager::GetNameOfKeyPrimary(const char *pName)
{
    //--Returns a heap-allocated string representing the name of the key. That is, "A" or "JoyBtn10"
    //  are the strings that will be returned.  Only looks at the primary slot, as it is possible to have
    //  up to 6 bindings. Uses whichever is found to be non-null first.
    //--The string is a copy stored in the CM, do not deallocate it or mess with it.  It should
    //  only be destroyed at program halt, it's a heap string because the drivers provide the names
    //  on the fly.
    //--Typically, NULL is not stored on the heap, so assume NULL is an error, even if the scancode
    //  was technically valid.
    ControlState *rControl = (ControlState *)mControlList->GetElementByName(pName);
    if(!rControl) return NULL;

    //--Check each of the three scancodes.  If one is -1, ignore it and move on.
    if(rControl->mWatchKeyPri >= 0 && rControl->mWatchKeyPri < mKeyboardNamesTotal)
        return mKeyboardNames[rControl->mWatchKeyPri];
    if(rControl->mWatchMouseBtnPri >= 0 && rControl->mWatchMouseBtnPri < mMouseNamesTotal)
        return mMouseNames[rControl->mWatchMouseBtnPri];
    if(rControl->mWatchJoyPri >= 0 && rControl->mWatchJoyPri < mJoypadNamesTotal)
        return mJoypadNames[rControl->mWatchJoyPri];

    return NULL;
}
char *ControlManager::GetNameOfKeySecondary(const char *pName)
{
    //--As above, but queries the secondary state.
    ControlState *rControl = (ControlState *)mControlList->GetElementByName(pName);
    if(!rControl) return NULL;

    //--Check each of the three scancodes.  If one is -1, ignore it and move on.
    if(rControl->mWatchKeySec >= 0 && rControl->mWatchKeySec < mKeyboardNamesTotal)
        return mKeyboardNames[rControl->mWatchKeySec];
    if(rControl->mWatchMouseBtnSec >= 0 && rControl->mWatchMouseBtnSec < mMouseNamesTotal)
        return mMouseNames[rControl->mWatchMouseBtnSec];
    if(rControl->mWatchJoySec >= 0 && rControl->mWatchJoySec < mJoypadNamesTotal)
        return mJoypadNames[rControl->mWatchJoySec];

    return NULL;
}
void ControlManager::ClearInputBuffer()
{
    mKeyboardInputBuffer->ClearList();
}
void ControlManager::RefreshMousePosition()
{
    //--Retrieves mouse state information. Call this if the mouse is desynced.

    //--[Allegro Version]
    #if defined _ALLEGRO_PROJECT_
        ALLEGRO_MOUSE_STATE tState;
        al_get_mouse_state(&tState);

        mMouseX = al_get_mouse_state_axis(&tState, 0);
        mMouseY = al_get_mouse_state_axis(&tState, 1);
        mMouseZ = al_get_mouse_state_axis(&tState, 2);

    //--[SDL Version]
    #elif defined _SDL_PROJECT_

        SDL_GetMouseState(&mMouseX, &mMouseY);

    #endif
}
void ControlManager::NullifyStateData()
{
    //--[Allegro Version]
    #if defined _ALLEGRO_PROJECT_
        mAllegroHasJoystickUpdate = false;
        rMouseStatePtr = NULL;
        rKeyboardStatePtr = NULL;
    #endif

    //--SDL doesn't have an equivalent.
}

///=========================================== Update =============================================
bool ControlManager::IsControlDown(ControlState *pState, bool &sControlIsChangeable)
{
    ///--[Documentation]
    //--Worker function, returns whether or not the requested state is down. Since states can get
    //  input from 1) Keyboard 2) Mouse 3) Joystick 4) Internal fires, this can be complex.
    if(!pState) return false;

    //--Setup
    bool tIsDown = false;

    ///--[SDL Code]
    #if defined _SDL_PROJECT_

    //--The down code is whatever the current code is.
    tIsDown = pState->mIsDown;

    //--Scan the input packages. If any one of them is reporting a press, returns true. If reporting
    //  a release, returns false. Otherwise, returns what the state currently is.
    KeypressPack *rPackage = (KeypressPack *)mInputBuffer->PushIterator();
    while(rPackage)
    {
        //--Keyboard match:
        if(rPackage->mScancode >= 0)
        {
            if(pState->mWatchKeyPri == rPackage->mScancode)
            {
                sControlIsChangeable = true;
                tIsDown = rPackage->mIsPress;
                mInputBuffer->PopIterator();
                break;
            }
            if(pState->mWatchKeySec == rPackage->mScancode)
            {
                sControlIsChangeable = true;
                tIsDown = rPackage->mIsPress;
                mInputBuffer->PopIterator();
                break;
            }
        }
        //--Mouse match:
        else if(rPackage->mMouseButton >= 0)
        {
            if(pState->mWatchMouseBtnPri == rPackage->mMouseButton)
            {
                sControlIsChangeable = true;
                tIsDown = rPackage->mIsPress;
                mInputBuffer->PopIterator();
                break;
            }
            if(pState->mWatchMouseBtnSec == rPackage->mMouseButton)
            {
                sControlIsChangeable = true;
                tIsDown = rPackage->mIsPress;
                mInputBuffer->PopIterator();
                break;
            }
        }
        //--Joypad match:
        else if(rPackage->mJoypad >= 0)
        {
            //--If this is a button...
            if(rPackage->mJoypad >= 0)// && rPackage->mJoypad < JOYSTICK_OFFSET_STICK)
            {
                if(pState->mWatchJoyPri == rPackage->mJoypad)
                {
                    sControlIsChangeable = true;
                    tIsDown = rPackage->mIsPress;
                    mInputBuffer->PopIterator();
                    break;
                }
                if(pState->mWatchJoySec == rPackage->mJoypad)
                {
                    sControlIsChangeable = true;
                    tIsDown = rPackage->mIsPress;
                    mInputBuffer->PopIterator();
                    break;
                }
            }
            //--Axis.
            else
            {
                //--Primary key...
                if(pState->mWatchJoyPri == rPackage->mJoypad)
                {
                    //--If the value is 128, this axis has stopped.
                    if(abs(rPackage->mJoypadValue) == 128)
                    {
                        sControlIsChangeable = true;
                        tIsDown = false;
                        mInputBuffer->PopIterator();
                        break;
                    }
                    //--If the value is negative, this watch code must be negative to be a press.
                    else if(rPackage->mJoypadValue < -128 && pState->mWatchJoyPri < 0)
                    {
                        sControlIsChangeable = true;
                        tIsDown = true;
                        mInputBuffer->PopIterator();
                        break;
                    }
                    //--If the value is positive, this watch code must be positive  to be a press.
                    else if(rPackage->mJoypadValue > 128 && pState->mWatchJoyPri > 0)
                    {
                        sControlIsChangeable = true;
                        tIsDown = true;
                        mInputBuffer->PopIterator();
                        break;
                    }
                }
                //--Secondary key...
                if(pState->mWatchJoySec == rPackage->mJoypad)
                {
                    //--If the value is 128, this axis has stopped.
                    if(abs(rPackage->mJoypadValue) == 128)
                    {
                        sControlIsChangeable = true;
                        tIsDown = false;
                        mInputBuffer->PopIterator();
                        break;
                    }
                    //--If the value is negative, this watch code must be negative to be a press.
                    else if(rPackage->mJoypadValue < -128 && pState->mWatchJoySec < 0)
                    {
                        sControlIsChangeable = true;
                        tIsDown = true;
                        mInputBuffer->PopIterator();
                        break;
                    }
                    //--If the value is positive, this watch code must be positive  to be a press.
                    else if(rPackage->mJoypadValue > 128 && pState->mWatchJoySec > 0)
                    {
                        sControlIsChangeable = true;
                        tIsDown = true;
                        mInputBuffer->PopIterator();
                        break;
                    }
                }
            }
        }


        //--If the package matches the scancodes...
        /*if((pState->mWatchKeyPri == rPackage->mScancode || pState->mWatchKeySec == rPackage->mScancode) && rPackage->mScancode != -2 && )
        {

        }
           pState->mWatchMouseBtnPri == rPackage->mMouseButton || pState->mWatchMouseBtnSec == rPackage->mMouseButton ||
           pState->mWatchJoyPri == rPackage->mJoypad || pState->mWatchJoySec == rPackage->mJoypad)
        {
            //--Mark the control as changeable.
            sControlIsChangeable = true;

            //--Store the flag.
            tIsDown = rPackage->mIsPress;
            mInputBuffer->PopIterator();
            break;
        }*/

        //--Next package.
        rPackage = (KeypressPack *)mInputBuffer->AutoIterate();
    }

    //--Allegro Version
    #elif defined _ALLEGRO_PROJECT_

    //--Keyboard can modify.
    if(rKeyboardStatePtr)
    {
        if(pState->mWatchKeyPri != -1)
        {
            sControlIsChangeable = true;
            if(al_key_down(rKeyboardStatePtr, pState->mWatchKeyPri)) tIsDown = true;
        }
        if(pState->mWatchKeySec != -1)
        {
            sControlIsChangeable = true;
            if(al_key_down(rKeyboardStatePtr, pState->mWatchKeySec)) tIsDown = true;
        }
    }

    //--Mouse can modify.
    if(rMouseStatePtr)
    {
        if(pState->mWatchMouseBtnPri != -1)
        {
            sControlIsChangeable = true;
            if(al_mouse_button_down(rMouseStatePtr, pState->mWatchMouseBtnPri)) tIsDown = true;
        }
        if(pState->mWatchMouseBtnSec != -1)
        {
            sControlIsChangeable = true;
            if(al_mouse_button_down(rMouseStatePtr, pState->mWatchMouseBtnSec)) tIsDown = true;
        }
    }

    //--Joystick is not reliant on state.
    if(mAllegroHasJoystickUpdate)
    {
        if(pState->mWatchJoyPri != -1)
        {
            sControlIsChangeable = true;
            tIsDown |= IsJoystickActive(pState->mWatchJoyPri);
        }
        if(pState->mWatchJoySec != -1)
        {
            sControlIsChangeable = true;
            tIsDown |= IsJoystickActive(pState->mWatchJoySec);
        }
    }
    #endif

    return tIsDown;
}
void ControlManager::Update(bool pIsLogicTick, bool pIsPressEvent)
{
    ///--[Documentation and Setup]
    //--Update the control states.  This is only called during a logic tick in most projects.
    DebugManager::PushPrint("Debug_ControlManager", "[ControlManager Begin Update]\n");

    ///--[Non-Logic Tick]
    //--If this isn't a logic tick, that means something was pressed or released. However, the
    //  key/button pressed/release is not necessarily a control. The ControlManager still watches
    //  all input forms, though, and flags as appropriate.
    //--In the current engine, this is not used.
    if(!pIsLogicTick)
    {
        //--Pressed something
        if(pIsPressEvent)
        {
            mIsAnyKeyPressed = true;
        }
        //--Released something
        else
        {
            mIsAnyKeyReleased = true;
        }
    }
    ///--[Logic Tick]
    //--On a logic tick, reset these flags.
    else
    {
        #if defined _ALLEGRO_PROJECT_
        if(rKeyboardStatePtr)
        {
            for(int i = 0; i < KEYCODES_MAX; i ++)
            {
                mDownKeys[i] = al_key_down(rKeyboardStatePtr, i);
            }
        }
        #elif defined _SDL_PROJECT_
        int tExpectedKeys = 0;
        const Uint8 *rSDLKeyArray = SDL_GetKeyboardState(&tExpectedKeys);
        for(int i = 0; i < tExpectedKeys; i ++)
        {
            mDownKeys[i] = rSDLKeyArray[i];
        }


        #endif
    }

    ///--[All Controls Update]
    //--Order all keys to update their conditions based on the current Input state.  If the tick is
    //  a logic tick, then the FirstPress and FirstRelease flags reset, otherwise they can only be
    //  triggered.
    DebugManager::Print("Begin iteration of %i controls\n", mControlList->GetListSize());
    ControlState *rControl = (ControlState *)mControlList->PushIterator();
    while(rControl)
    {
        //--Check all possible inputs.  A code will be -1 if it's not supposed to be scanned.
        bool tControlIsChangeable = false;
        bool tIsDown = IsControlDown(rControl, tControlIsChangeable);

        //--Flip variables as the state requests
        rControl->mIsFirstPress = false;
        rControl->mIsFirstRelease = false;
        if(tControlIsChangeable)
        {
            if(tIsDown)
            {
                mIsAnyKeyDown = true;
                rControl->mHasPendingRelease = false;
                if(!rControl->mIsDown)
                {
                    rControl->mIsFirstPress = true;
                    rControl->mTicksSincePress = 0;
                }
                else
                {
                    rControl->mTicksSincePress ++;
                }
            }
            else
            {
                if(!rControl->mIsDown)
                {

                }
                else
                {
                    rControl->mIsFirstRelease = true;
                    rControl->mTicksSincePress = -1;
                    rControl->mHasPendingRelease = true;
                }
            }
            rControl->mIsDown = tIsDown;
        }

        DebugManager::Print("State of %s is %i\n", mControlList->GetIteratorName(), rControl->mIsDown);

        //--Iterate
        rControl = (ControlState *)mControlList->AutoIterate();
    }

    ///--[Allegro Only]
    //--After any tick, clear off the references to the states. That way, only if the update was
    //  called correctly will the states be references (eg:  By a gear).
    #if defined _ALLEGRO_PROJECT_
    mAllegroHasJoystickUpdate = false;
    rMouseStatePtr = NULL;
    rKeyboardStatePtr = NULL;
    #endif

    ///--[SDL Only]
    //--After any tick, wipe the input buffer.
    #if defined _SDL_PROJECT_
    mInputBuffer->ClearList();
    #endif

    ///--[Debug]
    //--<DEBUG>
    DebugManager::PopPrint("[ControlManager Complete Update]\n");
}
void ControlManager::PostUpdate()
{
    ///--[Documentation]
    //--Called at the end of a logic tick, after any other managers and classes have had a chance to
    //  look at key information.
    mIsAnyKeyPressed = false;
    mIsAnyKeyDown = false;
    mIsAnyKeyReleased = false;
    mPressedKeyboardKey = -1;
    mPressedMouseButton = -1;
    mPressedJoypadButton = -1;
}
void ControlManager::UpdatePaused(uint8_t pPauseFlags)
{
    //--By default, the ControlManager continues to function while the program is paused.
    Update(true, false);
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *ControlManager::GetControlList()
{
    return mControlList;
}

///===================================== Static Functions =========================================
ControlManager *ControlManager::Fetch()
{
    return Global::Shared()->gControlManager;
}
