///====================================== CarnStringEntry =========================================
//--Carnation version of the StringEntry class. Slight reskin, changes a lot of positions.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StringEntry.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CarnStringEntry : public StringEntry
{
    private:
    ///--[System]
    ///--[Images]
    //--Colors
    StarlightColor mCarnTitle;
    StarlightColor mCarnSubtitle;
    StarlightColor mCarnParagraph;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontHeader;
            StarFont *rFontCurEntry;
            StarFont *rFontMainline;

            //--Images
            //StarBitmap *rCursor;
            StarBitmap *rRenderBase;
            StarBitmap *rRenderPressed;
        }Data;
    }CarnImg;

    protected:

    public:
    //--System
    CarnStringEntry();
    virtual ~CarnStringEntry();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Render(float pOpacity);

    //--Pointer Routing
    //--Static Functions
    static StringEntry *GenerateStringEntry();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


