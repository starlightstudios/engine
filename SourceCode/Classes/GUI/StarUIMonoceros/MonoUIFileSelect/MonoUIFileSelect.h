///===================================== MonoUIFileSelect =========================================
//--File Selection for Monoceros. Does most of the same job but is displayed differently.

#pragma once

///========================================= Includes =============================================
#include "AdvUIFileSelect.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///======================================== Translation ===========================================
///--[MonoUIFileSelect_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIFILESELECT_STRINGS_TOTAL 19
class MonoUIFileSelect_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIFILESELECT_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextCurrentBySlot;
            char *mTextCurrentReverseSlot;
            char *mTextCurrentNewestFirst;
            char *mTextCurrentOldestFirst;
            char *mTextEnterANoteForThisSaveFile;
            char *mTextEnterName;
            char *mTextSelectASave;
            char *mTextCreateNewSavefile;
            char *mTextNoEmptySaveSlotsAvailable;
            char *mTextEmptySlot;
            char *mTextReallyDeleteThisFile;
            char *mTextControlDeleteFile;
            char *mTextControlCancel;
            char *mTextControlEditFileNote;
            char *mTextControlChangeSorting;
            char *mTextControlChangePages;
            char *mTextControlFirstLastPage;
            char *mTextControlPageLeft;
            char *mTextControlPageRight;
        }str;
    };

    //--Functions
    MonoUIFileSelect_Strings();
    virtual ~MonoUIFileSelect_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIFileSelect : public MonoUICommon, virtual public AdvUIFileSelect
{
    protected:
    ///--[Constants]
    static const int cxMonoEntriesPerPage = 3;              //How many entries from the edge of the scrolling list before scrolling.
    static const int cxMonoDeletionTicks = 10;              //Ticks for deletion menu scrolling.

    ///--[System]
    ///--[Help Strings]
    //--Help Strings
    StarlightString *mConfirmDeleteString;
    StarlightString *mCancelDeleteString;

    ///--[Images]
    struct
    {
        //--Fonts.
        StarFont *rFont_Control;

        //--Images.
        StarBitmap *rFrame_NewGame;
        StarBitmap *rFrame_NewGameSelect;
        StarBitmap *rFrame_SaveCell;
        StarBitmap *rFrame_SaveCellSelect;
        StarBitmap *rOverlay_SaveShade;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rScrollbar_Static;
    }MonoImages;
    protected:

    public:
    //--System
    MonoUIFileSelect();
    virtual ~MonoUIFileSelect();
    virtual void Construct();

    //--Public Variables
    static MonoUIFileSelect_Strings *xrStringData;

    //--Property Queries
    virtual int GetArraySlotFromLookups(int pLookup);

    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    virtual void ScanExistingFiles();
    virtual void ResolveSortString();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateDirectionalControls();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


