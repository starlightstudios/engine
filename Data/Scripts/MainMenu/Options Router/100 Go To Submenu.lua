-- |[Go To Submenu]|
--Routes the menu to one of the submenus.
local iArgs = LM_GetNumOfArgs()
if(iArgs < 1) then return end
local iOption = LM_GetScriptArgument(0, "N")

--Setup.
local sPath = "Data/Scripts/MainMenu/000 PopulateMainMenu.lua"

--Switching.
if(iOption == 0) then
	sPath = "Data/Scripts/MainMenu/Engine Options/000 Populate.lua"
elseif(iOption == 1) then
	sPath = "Data/Scripts/MainMenu/Adventure Options/000 Populate.lua"
elseif(iOption == 2) then
	sPath = "Data/Scripts/MainMenu/Classic Options/000 Populate.lua"
end

--Execute.
MapM_PushMenuStackHead()
	FlexMenu_Clear()
	LM_ExecuteScript(sPath)
DL_PopActiveObject()