//--Base
#include "TilemapActor.h"

//--Classes
#include "ActorNotice.h"
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureDebug.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "AliasStorage.h"
#include "TileLayer.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DebugManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

///--[Local Definitions]
#define TRUE_OFF_X  4.0f
#define TRUE_OFF_Y  8.0f

///========================================== System ==============================================
void TilemapActor::ActivateEnemyMode(const char *pAssociatedName)
{
    ///--[Documentation]
    //--Causes this actor to become an enemy. Most of the other functions for enemies won't work at all until this is called,
    //  since it allocates memory for enemy mode.
    if(!pAssociatedName || mIsEnemy) return;

    //--Set the name. When this enemy is killed, this name is added to the list that prevents respawning.
    ResetString(mEnemyPackName, pAssociatedName);

    //--Flags.
    mIsEnemy = true;
    mIsWanderNPC = false;
    mAIState = TA_AI_WANDER;
    mMustRollWanderPoint = true;

    //--Memory Allocation.
    mEnemyListing = new StarLinkedList(false);

    //--Use the standard shadow.
    SetShadow(xEnemyShadowPath);

    //--Build path. This goes to the default path, but can be overridden.
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    char *tPath = InitializeString("%sChapter 0/Shot Reply Dialogue.lua", rAdventurePath);

    //--Enemies by default respond to gunshots.
    SetRespondsToShot(true);
    SetActivationScript(tPath);

    //--Clean.
    free(tPath);
}

///===================================== Property Queries =========================================
const char *TilemapActor::GetUniqueEnemyName()
{
    return mEnemyPackName;
}
bool TilemapActor::IsEnemy(const char *pUniqueName)
{
    //--If the passed in name is "Any", returns true if this entity is an enemy of any type.
    if(pUniqueName && !strcasecmp(pUniqueName, "Any"))
    {
        return mIsEnemy;
    }

    //--Otherwise, checks if this is a specific enemy.
    if(!mIsEnemy || !pUniqueName) return false;
    return !(strcasecmp(pUniqueName, mEnemyPackName));
}
float TilemapActor::GetRangeToPlayer()
{
    //--Returns the centroid-centroid range from this entity to the player. Useful for chase/leash logic.
    //  Will return a really high number if the player doesn't exist for some reason.
    float tPlayerX = -10000.0f;
    float tPlayerY = -10000.0f;
    float tDummyZ = 0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tDummyZ);

    //--Return the centroid distance.
    return (GetPlanarDistance(mTrueX, mTrueY, tPlayerX, tPlayerY));
}
float TilemapActor::ComputeCurrentViewDistance()
{
    ///--[Documentation]
    //--Figures out what the current view distance should be, based on the base mViewDistance. Certain behaviors
    //  like chasing or retreating can change this.
    if(mViewDistance < 1.0f) mViewDistance = 1.0f;

    //--Retreat. If the player has retreated recently, double the view distance. This is the highest increase
    //  in view distance so we return right away.
    if(AdventureLevel::Fetch()->GetRetreatTimer() > 0) return mViewDistance * 2.0f;

    //--Chase. If the player is being chased, and the entity is in Guard Mode, double the view range.
    if(mEnemyAIType == TA_AI_USE_GUARD_AI && mAIState == TA_AI_CHASE)
    {
        return mViewDistance * 1.5f;
    }

    //--Otherwise, return the base view distance.
    return mViewDistance;
}
bool TilemapActor::IsPlayerInSightCone(float pUseViewDistance)
{
    ///--[Documentation and Setup]
    //--Returns whether or not the player is currently in the enemy's sight cone. Pass -1.0 to use the computed
    //  view distance based on the entity's AI.
    if(mIsBlind) return false;

    //--Get position.
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    ///--[Initial Checks]
    //--Fail if the player is not in the same Z.
    if(tPlayerZ != mCollisionDepth) return false;

    //--If camouflage mode is active, the player is never within sight.
    if(AdventureDebug::xCamouflageMode) return false;

    //--View distance is doubled when retreat timer is active.
    float cUseViewDistance = pUseViewDistance;
    if(cUseViewDistance == TA_USE_COMPUTED_VIEWDIST) cUseViewDistance = ComputeCurrentViewDistance();

    //--Get range to player. If it's past the detection range, fail.
    float tRangeToPlayer = GetPlanarDistance(mTrueX, mTrueY, tPlayerX, tPlayerY);
    if(tRangeToPlayer > cUseViewDistance) return false;

    ///--[Angle Determination]
    //--Get the angle to the player.
    float tPlayerAngle = atan2f(tPlayerY - mTrueY+4.0f, tPlayerX - mTrueX+4.0f) * TODEGREE;
    while(tPlayerAngle <    0.0f) tPlayerAngle = tPlayerAngle + 360.0f;
    while(tPlayerAngle >= 360.0f) tPlayerAngle = tPlayerAngle - 360.0f;

    //--Get our facing direction.
    float tFacingAngle = mCurrentViewAngle;
    while(tFacingAngle <    0.0f) tFacingAngle = tFacingAngle + 360.0f;
    while(tFacingAngle >= 360.0f) tFacingAngle = tFacingAngle - 360.0f;

    ///--[Angle Check]
    //--Now, is the player's angle within the range of the facing angle? The first case checks for the simple case, the second checks for the
    //  360 degree loop case. They are effectively the same check.
    float cHalfView = mViewAngle / 2.0f;
    if((tPlayerAngle >= tFacingAngle - cHalfView          && tPlayerAngle <= tFacingAngle + cHalfView)          ||
       (tPlayerAngle >= tFacingAngle - cHalfView - 360.0f && tPlayerAngle <= tFacingAngle + cHalfView - 360.0f) ||
       (tPlayerAngle >= tFacingAngle - cHalfView + 360.0f && tPlayerAngle <= tFacingAngle + cHalfView + 360.0f))
    {
        //--So, the player is within range and within the visible angle. Is there a collision between us and them?
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && rActiveLevel->IsLineClipped(mTrueX + 4.0f, mTrueY + 4.0f, tPlayerX, tPlayerY, mCollisionDepth))
        {
            return false;
        }

        //--Nothing between us and the player. Check passes.
        return true;
    }

    ///--[Default]
    //--All checks failed, player is not in sight range.
    return false;
}
TwoDimensionReal TilemapActor::GetFollowPosition(const char *pRegisterName)
{
    //--Given the name of a follower, returns a structure telling that follower where it should be behind us.
    //  Having more followers changes the ruleset so they space evenly.
    TwoDimensionReal tReturnStruct;
    tReturnStruct.Set(mTrueX - 1.0f, mTrueY - 1.0f, mTrueX + 1.0f, mTrueY + 1.0f);

    //--Constants.
    float cFollowDistance = 8.0f;
    float cRadsPerEntity = 60.0f * TORADIAN;

    //--Check if this name already exists on the follower list. If it does, find out what slot it's in.
    int tSlot = mFollowerNameListing->GetSlotOfElementByName(pRegisterName);

    //--If it came back as -1, add it to the list.
    if(tSlot == -1)
    {
        static int xDummyPtr;
        mFollowerNameListing->AddElement(pRegisterName, &xDummyPtr);
        tSlot = mFollowerNameListing->GetListSize() - 1;
    }

    //--Error check: The listing can't somehow have 0 elements!
    if(mFollowerNameListing->GetListSize() < 1) return tReturnStruct;

    //--Set the fan position. The positions are increments 30 degrees off the center, but alternates left and right.
    float tAngle = tSlot * cRadsPerEntity;
    tAngle = tAngle - ((mFollowerNameListing->GetListSize() - 1) * cRadsPerEntity / 2.0f);

    //--Get the base angle based on this entity's facing.
    float tBaseAngle = (((float)mTrueFacing * 45.0f) + 90.0f) * TORADIAN;

    //--Compute positions.
    float cPointX = mTrueX + (cosf(tBaseAngle + tAngle) * cFollowDistance);
    float cPointY = mTrueY + (sinf(tBaseAngle + tAngle) * cFollowDistance);
    tReturnStruct.SetWH(cPointX - 1.0f, cPointY - 1.0f, 2.0f, 2.0f);
    return tReturnStruct;
}
const char *TilemapActor::GetZerothEnemyPrototype()
{
    ///--[Documentation]
    //--Returns the zeroth enemy on the listing. If the enemy listing has not been initialized,
    //  or if there is no enemy on the listing, returns "Null".
    if(!mEnemyListing) return "Null";

    ///--[Execution]
    //--Get zero prototype.
    const char *rZeroPrototype = (const char *)mEnemyListing->GetElementBySlot(0);
    if(!rZeroPrototype)
    {
        return "Null";
    }

    //--Found. Return.
    return mEnemyListing->GetNameOfElementBySlot(0);
}

///======================================== Manipulators ==========================================
void TilemapActor::ActivateBagRefillMode()
{
    mIsBagRefill = true;
    SetMuggable(false);
}
void TilemapActor::ActivateItemNodeMode()
{
    mIsItemNode = true;
    SetMuggable(false);
}
void TilemapActor::ActivateFallenLootMode(const char *pLootString)
{
    //--Unset.
    if(!pLootString || !strcasecmp(pLootString, "Null"))
    {
        mIsItemDrop = false;
        return;
    }

    //--Enemy mode must be active.
    if(!mIsEnemy || !mEnemyListing) return;

    //--Set.
    static int xDummy = 1;
    mIsItemDrop = true;
    mEnemyListing->AddElement(pLootString, &xDummy);

    //--Other flags.
    SetMuggable(false);
}
void TilemapActor::ActivateWanderMode()
{
    ///--[Documentation]
    //--An NPC in wander mode behaves like an enemy but never chases or engages the player under any circumstances. Once active
    //  it cannot be deactivated. Enemies that are ignoring the player but are otherwise enemies are handled separately.
    //--This needs to be called last when creating an entity since it leashes to the current position.
    mIsEnemy = true;
    mIsWanderNPC = true;
    mIsIgnoringPlayer = true;
    //fprintf(stderr, "%s sets ignore player to true, ActivateWanderMode().\n", mLocalName);

    //--Initial state: Wait a random number of ticks.
    mAIState = TA_AI_WAIT;
    mWaitTimer = 0;
    mWaitTimerMax = rand() % 150 + 30;

    //--Leash to our current position.
    SetLeashingPoint(mTrueX, mTrueY);
}
void TilemapActor::ActivatePatrolMode(const char *pPatrolPath)
{
    ///--[Documentation and Setup]
    //--An NPC can be ordered to walk between a provided set of nodes. Node names are delimited by '|'. This routine stores each
    //  node in an array to be parsed as the entity moves.
    //--First, deallocate everything and set it back to zero.
    mIsPatrolling = false;
    for(int i = 0; i < mPatrolNodesTotal; i ++) free(mPatrolNodeList[i]);
    free(mPatrolNodeList);

    //--Zero, error check.
    mPatrolNodesTotal = 0;
    mPatrolNodeList = NULL;
    if(!pPatrolPath || !strcasecmp(pPatrolPath, "Null")) return;

    ///--[Special Flag: AUTO]
    //--If the entire patrol path is just "AUTO" then this entity uses the last three letters of its name to determine its behavior.
    //  This saves a bit of work for the map developer to quickly set common behaviors.
    //--Example names: EnemyAAN, EnemyABF
    //--The first letter is the sequence they will follow (using the same SEQ| logic below). The second letter is which enemy this is
    //  in sequence, and the third letter is "F" to make them follow the previous enemy in sequence, or "N" to not.
    if(!strncasecmp(pPatrolPath, "AUTO", 4))
    {
        //--Resolve the last three letters of the enemy's name.
        int tNameLen = (int)strlen(mLocalName);
        if(tNameLen < 3) return;

        //--Get out the individual characters.
        char tSequence   = mLocalName[tNameLen-3];
        char tEnemyNum   = mLocalName[tNameLen-2];
        char tFollowFlag = mLocalName[tNameLen-1];

        //--Set sequence.
        char tNewBuf[6];
        strcpy(tNewBuf, "SEQ|");
        tNewBuf[4] = tSequence;
        tNewBuf[5] = '\0';
        pPatrolPath = tNewBuf;

        //--If tFollowFlag is 'F', then this entity follows the entity one below it. If the enemy name is 'A', this doesn't
        //  happen, as there's no one to follow.
        if(tEnemyNum > 'A' && tFollowFlag == 'F')
        {
            //--Create a partial buffer of the enemy name.
            char tEnemyNameBuf[128];
            strcpy(tEnemyNameBuf, "Enemy");
            tEnemyNameBuf[5] = tSequence;
            tEnemyNameBuf[6] = tEnemyNum - 1;
            tEnemyNameBuf[7] = '\0';

            //--Locate the name of the previous enemy in the sequence.
            const char *rEnemyName = AdventureLevel::Fetch()->GetEnemyNameFromPattern(tEnemyNameBuf);

            //--If there's a match, set us to follow that enemy name.
            if(rEnemyName)
            {
                SetFollowTarget(rEnemyName);
            }
        }
    }

    ///--[Special Flag: SEQ]
    //--If this sequence exists at the start of the path, then the entity will path to all matching nodes. An example is "SEQ|A"
    //  which will cause the entity to path to every node that starts with "A". Most of the time, nodes are "A0", "A1", "B0", "B1", etc.
    if(!strncasecmp(pPatrolPath, "SEQ|", 4) && strlen(pPatrolPath) > 4)
    {
        //--Construct a new patrol path buffer.
        char tNewBuf[1024];
        tNewBuf[0] = '\0';

        //--Get the level.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(!rActiveLevel) return;

        //--Get the patrol node list.
        StarLinkedList *rPatrolNodeList = rActiveLevel->GetPatrolNodeList();
        if(!rPatrolNodeList) return;

        //--Get the buffer after the "SEQ|" part. This is the thing to match against.
        char tBuffer[128];
        strcpy(tBuffer, &pPatrolPath[4]);
        int tLen = (int)(strlen(tBuffer));

        //--Iterate across the nodes. Check for matching names.
        PatrolNodePack *rPackage = (PatrolNodePack *)rPatrolNodeList->PushIterator();
        while(rPackage)
        {
            //--Get this node's name.
            const char *rNodeName = rPatrolNodeList->GetIteratorName();

            //--Check for a match. If it matches, add it to the new patrol buffer.
            if(!strncasecmp(rNodeName, tBuffer, tLen))
            {
                strcat(tNewBuf, rNodeName);
                strcat(tNewBuf, "|");
            }

            //--Next.
            rPackage = (PatrolNodePack *)rPatrolNodeList->AutoIterate();
        }

        //--Debug.
        //fprintf(stderr, "Entity has sequence: %s\n", pPatrolPath);
        //fprintf(stderr, " New patrol buffer: %s\n", tNewBuf);

        //--Set the patrol path to the buffer, and let the normal code execute.
        pPatrolPath = tNewBuf;
    }

    ///--[Allocation]
    //--Check how many nodes there are. There is always at least 1, and a '|' denotes another one even if the name is empty.
    int tLen = (int)strlen(pPatrolPath);
    mPatrolNodesTotal = 0;
    for(int i = 0; i < tLen; i ++)
    {
        if(pPatrolPath[i] == '|') mPatrolNodesTotal ++;
    }
    if(mPatrolNodesTotal < 1) return;

    //--Allocate space for the nodes.
    SetMemoryData(__FILE__, __LINE__);
    mPatrolNodeList = (char **)starmemoryalloc(sizeof(char *) * mPatrolNodesTotal);

    ///--[Node Setting]
    //--Begin iterating across the strings, allocating space and copying the data.
    int tStartLetter = 0;
    int tCurrentNode = 0;
    for(int i = 0; i < tLen; i ++)
    {
        //--Delimiter:
        if(pPatrolPath[i] == '|')
        {
            //--Allocate space, copy string.
            int tTotalLen = i - tStartLetter;
            SetMemoryData(__FILE__, __LINE__);
            mPatrolNodeList[tCurrentNode] = (char *)starmemoryalloc(sizeof(char) * (tTotalLen + 1));
            strncpy(mPatrolNodeList[tCurrentNode], &pPatrolPath[tStartLetter], tTotalLen);
            mPatrolNodeList[tCurrentNode][tTotalLen] = '\0';

            //--Move to next string.
            tStartLetter = i + 1;
            tCurrentNode ++;
            if(tCurrentNode >= mPatrolNodesTotal) break;
        }
        //--Skip normal letter.
        else
        {

        }
    }

    //--Other flags.
    mPatrolNodeCurrent = 0;
    mIsPatrolling = true;

    //--Debug.
    //fprintf(stderr, "Patrol Node List.\n");
    //for(int i = 0; i < mPatrolNodesTotal; i ++) fprintf(stderr, "%i: %s\n", i, mPatrolNodeList[i]);
}
void TilemapActor::SetWorldStopIgnoreFlag(bool pFlag)
{
    mIgnoreWorldStop = pFlag;
}
void TilemapActor::SetCombatEnemy(const char *pPrototypeName)
{
    ///--[Documentation]
    //--Same as below, but clears the list first. This allows resetting an enemy list if a smaller one is needed.
    if(!mEnemyListing) return;

    ///--[Execution]
    //--Clear, add the enemy.
    mEnemyListing->ClearList();
    AddCombatEnemy(pPrototypeName);
}
void TilemapActor::AddCombatEnemy(const char *pPrototypeName)
{
    ///--[Documentation]
    //--Adds an enemy prototype onto the end of the existing enemy listing. When a battle is started, each
    //  prototype gets passed to combat to spawn an enemy.
    static int xDummy = 0;
    if(!pPrototypeName || !mEnemyListing) return;

    ///--[Execution]
    //--Add.
    mEnemyListing->AddElement(pPrototypeName, &xDummy);

    //--Special: If the enemy name is "NOBATTLE" then the enemy has a special flag set, and does not trigger battles.
    if(!strcasecmp(pPrototypeName, "NOBATTLE"))
    {
        mIgnoresReinforcement = true;
        mDoesNotTriggerBattles = true;
    }
}
void TilemapActor::SetDefeatScene(const char *pName)
{
    ResetString(mDefeatSceneName, pName);
}
void TilemapActor::SetVictoryScene(const char *pName)
{
    ResetString(mVictorySceneName, pName);
}
void TilemapActor::SetRetreatScene(const char *pName)
{
    ResetString(mRetreatSceneName, pName);
}
void TilemapActor::SetLeashingPoint(float pX, float pY)
{
    mLeashX = pX;
    mLeashY = pY;
}
void TilemapActor::AddCommandString(const char *pString)
{
    static int xDummyVar = 5;
    if(!pString) return;
    mCommandsNameListing->AddElement(pString, &xDummyVar);
}
void TilemapActor::AddIgnoreString(const char *pString)
{
    static int xDummyVar = 6;
    if(!pString || !strcasecmp(pString, "Null")) return;
    mIgnorePlayerCodeList->AddElement(pString, &xDummyVar);
}
void TilemapActor::SetDetectRange(int pPixels)
{
    mViewDistance = pPixels;
}
void TilemapActor::SetDetectAngle(int pDegrees)
{
    mViewAngle = pDegrees;
}
void TilemapActor::SetBlind(bool pFlag)
{
    mIsBlind = pFlag;
}
void TilemapActor::SetImmobile(bool pFlag)
{
    mIsImmobile = pFlag;
}
void TilemapActor::SetToughness(int pValue)
{
    mEnemyToughness = pValue;
}
void TilemapActor::SetFast(bool pFlag)
{
    mIsFast = pFlag;
}
void TilemapActor::SetRelentless(bool pFlag)
{
    mIsRelentless = pFlag;
}
void TilemapActor::SetNeverPauses(bool pFlag)
{
    mNeverPauses = pFlag;
}
void TilemapActor::SetFollowTarget(const char *pTarget)
{
    //--If the value is "Null" ignore it.
    if(!strcasecmp(pTarget, "Null")) return;

    ResetString(mFollowTarget, pTarget);
}
void TilemapActor::RemoveFollower(const char *pName)
{
    mFollowerNameListing->RemoveElementS(pName);
}
void TilemapActor::SetMuggable(bool pIsMuggable)
{
    mCanBeMugged = pIsMuggable;
}
void TilemapActor::SetMugLootable(bool pIsMugLootable)
{
    mHasAnythingToMug = pIsMugLootable;
}
void TilemapActor::ActivateTeamChase()
{
    //--Causes this entity to begin chasing the player. Called when another entity in the same follow group has spotted the player.
    BeginChase();
}
void TilemapActor::SetLockFacing(int pDirection)
{
    mEnemyLockFacing = pDirection;
}
void TilemapActor::SetVoidRiftMode()
{
    mIsVoidRiftMode = true;
}
void TilemapActor::SetRenderAsSingleBlock()
{
    mRenderAsSingleBlock = true;
}
void TilemapActor::SetUseWholeCollision()
{
    mWholeCollision = true;
}

///======================================== Core Methods ==========================================
void TilemapActor::PulseIgnore(const char *pPulseString)
{
    ///--[Documentation]
    //--Checks if the actor is ignoring the player in their current state. If so, they act like an NPC. They do not chase the player
    //  and may even have dialogue available.
    //--Note that wandering NPCs always ignore the player even if the pulse is run.
    mHalveLeashDistance = mIsWanderNPC;
    mIsIgnoringPlayer = mIsWanderNPC;
    //fprintf(stderr, "%s sets ignore player to %i, PulseIgnore(). Check: %s\n", mLocalName, mIsIgnoringPlayer, pPulseString);
    if(!pPulseString || pPulseString[0] == '\0') return;

    //--No need to check the ignore list if we're a wandering NPC.
    if(mIsWanderNPC) return;

    //--If the string is "ALWAYS" then always ignore the player.
    if(!strcasecmp(pPulseString, "ALWAYS"))
    {
        //fprintf(stderr, " %s sets ignore player to %i, PulseIgnore(), ALWAYS check.\n", mLocalName, mIsIgnoringPlayer);
        mIsIgnoringPlayer = true;
        return;
    }

    //--For each string, subdivided by a '|', check if we need to ignore.
    StarLinkedList *tStringList = Subdivide::SubdivideStringToList(pPulseString, "|");
    char *rString = (char *)tStringList->PushIterator();
    while(rString)
    {
        if(mIgnorePlayerCodeList->GetElementByName(rString))
        {
            //fprintf(stderr, " %s sets ignore player to %i, PulseIgnore(), is in pulse check. Matched.\n", mLocalName, mIsIgnoringPlayer);
            mIsIgnoringPlayer = true;
            tStringList->PopIterator();
            break;
        }
        rString = (char *)tStringList->AutoIterate();
    }

    //--Clean.
    delete tStringList;
}
void TilemapActor::BuildGraphicsFromName(const char *pName, bool pHasRunFrames, bool pIsEightDirection, bool pIsTwoDirectional)
{
    ///--[Documentation]
    //--Subroutine used to resolve which graphics to use for an auto-generated entity. These are usually entities specified in
    //  a Tiled file, such as enemies or NPCs not tied to a script.
    //--Most of the functionality is exposed in Lua to allow special cases.
    if(!xBuildGraphicsFromNamePath) return;

    //--Resolve arguments to pass.
    float tHasRunFrames = 0.0f;
    if(pHasRunFrames) tHasRunFrames = 1.0f;
    float tDirections = 4.0f;
    if(pIsEightDirection) tDirections = 8.0f;
    if(pIsTwoDirectional) tDirections = 2.0f;

    //--Call Lua script.
    DataLibrary::Fetch()->PushActiveEntity(this);
    LuaManager::Fetch()->ExecuteLuaFile(xBuildGraphicsFromNamePath, 3, "S", pName, "N", tHasRunFrames, "N", tDirections);
    DataLibrary::Fetch()->PopActiveEntity();
}
void TilemapActor::GetPlayerPosition(float &sX, float &sY, float &sZ)
{
    //--Returns the player's position in the given floats.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;

    //--Get the player.
    TilemapActor *rPlayer = rActiveLevel->LocatePlayerActor();
    if(!rPlayer) return;

    //--Set the values.
    sX = rPlayer->GetWorldX();
    sY = rPlayer->GetWorldY();
    sZ = rPlayer->GetCollisionDepth();
}
#define PATH_CAP 200
void TilemapActor::ComputeReinforcement(bool pIsPolling)
{
    ///--[Documentation and Setup]
    //--Called once combat starts. This entity will run through an internal set of checks to see if it
    //  can reinforce the currently battling enemy. If so, the enemy will add itself as reinforcements to
    //  the combat UI. If close enough, they reinforce on the 0th turn and stun themselves.
    //--If not an enemy or unable to path to the player, they do not reinforce. Also, there are some checks
    //  such as distraction or stunning that prevent reinforcement.
    //--If the flag pIsPolling is true, then don't apply any effects to the entity, we're just seeing if they
    //  are in range for combat, not actually in combat.
    mReinforcementTimer = 0;
    mLastComputedReinforcement = 0;
    if(!mIsEnemy || mIsDying || mSelfDestruct || mStunTimer > 0 || mIsIgnoringPlayer || mIsBagRefill || mIsItemNode || mIsItemDrop || mMugStunTimer > 0) return;

    //--Don't do anything if this enemy ignores reinforcement.
    if(mIgnoresReinforcement) return;

    //--Compute the raw distance to the player. If it's over a given threshold, fail right away.
    float tRangeToPlayer = GetRangeToPlayer();
    if(tRangeToPlayer > TileLayer::cxSizePerTile * 10.0f) return;

    //--Store the player's position.
    float tPlayerX, tPlayerY, tPlayerZ;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--If the Z position is not the same, we can never reinforce.
    if(tPlayerZ != mCollisionDepth) return;

    //--Store our original properties.
    float tOrigX = mTrueX;
    float tOrigY = mTrueY;
    int tOrigTimer = mMoveTimer;
    bool tOrigState = mIsMoving;
    float tOrigFacing = mFacing;
    float tOrigRemX = mRemainderX;
    float tOrigRemY = mRemainderY;

    //--Now run the pathing routine towards the player. We are emulating movement here, so if collisions are in
    //  the way the enemy might be unable to reach the player.
    //--Because immobility will block HandleMoveTo(), we unset the flag temporarily here.
    bool tWasImmobile = mIsImmobile;
    mIsImmobile = false;
    int tMoves = 0;
    while(tMoves < PATH_CAP)
    {
        //--Check the distance to the player. If low enough, end emulation.
        float tCurrentRangeToPlayer = GetRangeToPlayer();
        if(tCurrentRangeToPlayer < xCombatTriggerRange) break;

        //--Run emulation.
        HandleMoveTo(tPlayerX, tPlayerY, -1.0f);

        //--Next.
        tMoves ++;
    }

    //--Reset everything.
    mIsImmobile = tWasImmobile;
    mTrueX = tOrigX;
    mTrueY = tOrigY;
    mMoveTimer = tOrigTimer;
    mIsMoving = tOrigState;
    mFacing = tOrigFacing;
    mRemainderX = tOrigRemX;
    mRemainderY = tOrigRemY;

    //--If the moves value hit PATH_CAP, we couldn't find a path.
    if(tMoves >= PATH_CAP)
    {
    }
    //--If the move range was low enough, join the battle immediately.
    else if(tMoves * mMoveSpeed < TileLayer::cxSizePerTile * 3.0f)
    {
        //--Stun us, same as if we entered the battle immediately.
        if(!pIsPolling) mStunTimer = 45;

        //--Add.
        AppendEnemiesToCombat(pIsPolling);
        AdvCombat::Fetch()->RegisterWorldReference(this, 0);
    }
    //--Otherwise, become reinforcements at range.
    else
    {
        //--Compute reinforcement turns.
        float tDistance = tMoves * mMoveSpeed;
        mLastComputedReinforcement = (tDistance / (TileLayer::cxSizePerTile * 3.0f)) + 1;

        //--Append as reinforcements.
        AdvCombat::Fetch()->RegisterWorldReference(this, mLastComputedReinforcement);
        //fprintf(stderr, "Enemy %s became reinforcements: %f %i\n", mLocalName, tDistance, mLastComputedReinforcement);
    }
}
void TilemapActor::AppendEnemiesToCombat(bool pIsPolling)
{
    ///--[Documentation]
    //--Subroutine: Adds the enemies in this package to the combat UI. Can be called from the update routine, but
    //  can also be called by the CombatUI directly in case of reinforcements.
    //--If the flag pIsPolling is true, then the world is just checking who is in range, possibly for a mug check.
    //  A battle is not going to break out.
    if(!mEnemyListing) return;

    //--Setup.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AliasStorage *rEnemyPathStorage = rAdventureCombat->GetEnemyAliasStorage();

    //--Flag.
    xActorWasMugged = mHasBeenMugged;

    ///--[Debug]
    //rEnemyPathStorage->Print();

    ///--[Iterate]
    //--Iterate across each entry. Most enemies have 1, some have multiple.
    void *rDummyPtr = mEnemyListing->PushIterator();
    while(rDummyPtr)
    {
        //--Get the name associated with this. It's actually an alias on the enemy listing.
        bool tFail = true;
        const char *rPath = rEnemyPathStorage->GetString(mEnemyListing->GetIteratorName());

        //--If the path comes back NULL, report it.
        if(!rPath)
        {
            DebugManager::ForcePrint("Warning: No enemy prototype %s\n", mEnemyListing->GetIteratorName());
        }
        //--If the path is not "AUTO", call the script.
        else if(strcasecmp(rPath, "AUTO"))
        {
            tFail = false;
            LuaManager::Fetch()->ExecuteLuaFile(rPath, 1, "N", (float)mEnemyToughness);
        }
        //--If the path is "AUTO", we call a special handler which needs the name of the enemy.
        else
        {
            tFail = false;
            const char *rAutoPath = rAdventureCombat->GetEnemyAutoScript();
            LuaManager::Fetch()->ExecuteLuaFile(rAutoPath, 1, "S", mEnemyListing->GetIteratorName());
        }

        //--Get the last registered enemy. Mark it with our paragon grouping.
        if(!tFail)
        {
            AdvCombatEntity *rLastEntity = rAdventureCombat->GetLastRegisteredEnemy();
            if(rLastEntity)
            {
                rLastEntity->SetParagonGrouping(mParagonGrouping);
            }
        }

        //--Next.
        rDummyPtr = mEnemyListing->AutoIterate();
    }

    //--Clear flag.
    xActorWasMugged = false;
}
void TilemapActor::SpawnAutoWinNotices(int pXP, int pJP, int pPlatina, StarLinkedList *pItemList)
{
    //--Spawn the notices that occur when an entity is defeated by auto-win. Blanks out spots that are not used.
    float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X + 16.0f;
    float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y;
    char tBuffer[128];
    StarLinkedList *trSpawnedNotices = new StarLinkedList(false);

    //--Victory. Always appears.
    ActorNotice *nNotice = SpawnNotice("Victory!");
    trSpawnedNotices->AddElementAsHead("X", nNotice);

    //--XP.
    if(pXP > 0)
    {
        sprintf(tBuffer, "Got %i exp!", pXP);
        nNotice = SpawnNotice(tBuffer);
        trSpawnedNotices->AddElementAsHead("X", nNotice);
    }

    //--JP.
    if(pJP > 0)
    {
        sprintf(tBuffer, "Got %i JP!", pJP);
        nNotice = SpawnNotice(tBuffer);
        trSpawnedNotices->AddElementAsHead("X", nNotice);
    }

    //--Platina.
    if(pPlatina > 0)
    {
        sprintf(tBuffer, "Got %i platina!", pPlatina);
        nNotice = SpawnNotice(tBuffer);
        trSpawnedNotices->AddElementAsHead("X", nNotice);
    }

    //--Items.
    const char *rItemName = (const char *)pItemList->PushIterator();
    while(rItemName)
    {
        sprintf(tBuffer, "Got %s!", rItemName);
        nNotice = SpawnNotice(tBuffer);
        trSpawnedNotices->AddElementAsHead("X", nNotice);
        rItemName = (const char *)pItemList->AutoIterate();
    }


    //--Now rearrange the positions of the notices.
    ActorNotice *rNotice = (ActorNotice *)trSpawnedNotices->PushIterator();
    while(rNotice)
    {
        rNotice->MoveTo(tXPosition, tYPosition, TA_TRANSLATE_TICKS);
        tYPosition = tYPosition - TA_NOTICE_SPACING;
        rNotice = (ActorNotice *)trSpawnedNotices->AutoIterate();
    }

    //--Clean.
    delete trSpawnedNotices;
}
void TilemapActor::HandleMugging()
{
    ///--[Enemy Checker]
    //--It is possible to mug via scripts so we should check if this is an enemy before anything else.
    if(!mIsEnemy) return;

    ///--[Auto-Win Checker]
    //--If auto-win is enabled, mugging enemies runs a combat handler that may defeat the enemy immediately instead
    //  of simply mugging it. If this happens, the program returns out, as the combat handler does the rest.
    SysVar *rCheckVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iAllowAutoWinOnMug");
    if(rCheckVar && rCheckVar->mNumeric > 0.0f)
    {
        //fprintf(stderr, "Auto win var is %f\n", rCheckVar->mNumeric);
        if(AdvCombat::Fetch()->CheckAutoWin(this))
        {
            //fprintf(stderr, "Auto win came back true.\n");
            return;
        }
        //fprintf(stderr, "Auto win came back false.\n");
    }
    else
    {
        //fprintf(stderr, "Auto-win disallowed.\n");
    }

    ///--[Mug Success]
    //--When the mugging timer is over the limit, this is called to move resources to the player.
    if(mHasAnythingToMug)
    {
        //--If there are no enemies in this entity's storage:
        if(!mEnemyListing || mEnemyListing->GetListSize() < 1)
        {
            SpawnNotice("Nothing to steal...");
        }
        //--Otherwise, call the zeroth enemy's script.
        else
        {
            //--Setup.
            char tBuffer[256];
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            AliasStorage *rEnemyPathStorage = rAdventureCombat->GetEnemyAliasStorage();

            //--Clear static variables.
            TilemapActor::xMugPlatina = 0;
            TilemapActor::xMugExperience = 0;
            TilemapActor::xMugJobPoints = 0;
            TilemapActor::xMugItemList->ClearList();
            TilemapActor::xIsMugCheck = true;
            TilemapActor::xPulseIsNotCombat = true;

            //--Get the name associated with this. It's actually an alias on the enemy listing.
            const char *rPath = rEnemyPathStorage->GetString(mEnemyListing->GetNameOfElementBySlot(0));

            //--If the path is not "AUTO", call the script.
            if(strcasecmp(rPath, "AUTO"))
            {
                LuaManager::Fetch()->ExecuteLuaFile(rPath, 1, "N", (float)mEnemyToughness);
                //fprintf(stderr, "Enemy path is %s, alias was %s\n", rPath, mEnemyListing->GetNameOfElementBySlot(0));
            }
            //--If the path is "AUTO", we call a special handler which needs the name of the enemy.
            else
            {
                //--Get the zeroth party entry.
                const char *rZerothName = mEnemyListing->GetNameOfElementBySlot(0);

                //--Fetch the auto path and execute it.
                const char *rAutoPath = rAdventureCombat->GetEnemyAutoScript();
                LuaManager::Fetch()->ExecuteLuaFile(rAutoPath, 1, "S", rZerothName);
                //fprintf(stderr, "Enemy auto-path %s %s.\n", rAutoPath, rZerothName);
            }

            //--Unset flag.
            TilemapActor::xIsMugCheck = false;
            TilemapActor::xPulseIsNotCombat = false;

            //--If all four come back empty, print a single notice.
            if(xMugPlatina < 1 && xMugExperience < 1 && xMugJobPoints < 1 && xMugItemList->GetListSize() < 1)
            {
                SpawnNotice("Nothing to steal...");
            }
            //--Otherwise, print notices for each.
            else
            {
                //--Make a temporary list of spawned notices.
                StarLinkedList *trSpawnedNotices = new StarLinkedList(false);

                //--Notice for each item.
                void *rDummyPtr = xMugItemList->PushIterator();
                while(rDummyPtr)
                {
                    //--The name is the item.
                    const char *rName = xMugItemList->GetIteratorName();
                    sprintf(tBuffer, "Stole [%s]!", rName);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Put the item in the player's inventory.
                    LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rName);

                    //--Next.
                    rDummyPtr = xMugItemList->AutoIterate();
                }

                //--Job Points
                if(TilemapActor::xMugJobPoints > 0)
                {
                    //--Notice.
                    sprintf(tBuffer, "Got %i JP!", TilemapActor::xMugJobPoints);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Award to the player.
                    AdvCombat::Fetch()->AwardJP(TilemapActor::xMugJobPoints);
                }

                //--Cash.
                if(TilemapActor::xMugPlatina > 0)
                {
                    //--Notice.
                    sprintf(tBuffer, "Stole %i platina!", TilemapActor::xMugPlatina);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Award to the player.
                    AdventureInventory *rInventory = AdventureInventory::Fetch();
                    rInventory->SetPlatina(AdventureInventory::Fetch()->GetPlatina() + TilemapActor::xMugPlatina);
                }

                //--Experience.
                if(TilemapActor::xMugExperience > 0)
                {
                    //--Notice.
                    sprintf(tBuffer, "Got %i exp!", TilemapActor::xMugExperience);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Award to the player.
                    AdvCombat::Fetch()->AwardXP(TilemapActor::xMugExperience);
                }

                //--Now rearrange the positions of the notices.
                float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X + 16.0f;
                float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y;
                ActorNotice *rNotice = (ActorNotice *)trSpawnedNotices->PushIterator();
                while(rNotice)
                {
                    rNotice->MoveTo(tXPosition, tYPosition, TA_TRANSLATE_TICKS);
                    tYPosition = tYPosition - TA_NOTICE_SPACING;
                    rNotice = (ActorNotice *)trSpawnedNotices->AutoIterate();
                }

                //--Clean up.
                delete trSpawnedNotices;
            }
        }
    }
    else
    {
        SpawnNotice("Nothing to steal...");
    }

    //--Mark the enemy as having been looted.
    mHasAnythingToMug = false;
    mHasBeenMugged = true;
    AdventureLevel::MarkEnemyMugged(mEnemyPackName);

    //--Set the stun timer.
    mMugStunTimer = TA_MUG_STUN_TICKS;
    mMugTimer = 0;

    //--If this entity was a follower, they lose their leader.
    if(mFollowTarget)
    {
        //--If the entity exists, remove us from its follower listing.
        RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowTarget);
        if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            rActor->RemoveFollower(mLocalName);
        }

        //--Clear the flag.
        ResetString(mFollowTarget, NULL);
    }
}

///=========================================== Update =============================================
bool TilemapActor::HandleEnemyActivation(int pX, int pY)
{
    ///--[Documentation]
    //--Handles the player activating this entity when in enemy mode. Same as HandleActivation(), except done for
    //  enemies. Right now this handles mugging.
    //--Returns true if the Actor activated something in a fashion that would stop the rest of the activation
    //  update, false otherwise.

    ///--[Item Node]
    //--If this is an item node, then activating it fires a script. This script nominally awards
    //  an item and despawns the node but that's not guaranteed. This bypasses the mugging check.
    if(mIsItemNode && mItemNodeKeyword)
    {
        //--Must be on the same depth as the player.
        float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
        GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
        if(mCollisionDepth != tPlayerZ) return false;
        const float cHalfSize = TA_SIZE * 1.0f;

        //--Compute the position coordinates.
        float tLft = mTrueX - cHalfSize;
        float tTop = mTrueY - cHalfSize;
        float tRgt = mTrueX + TA_SIZE + cHalfSize;
        float tBot = mTrueY + TA_SIZE + cHalfSize;

        //--Check position.
        if(IsPointWithin(pX, pY, tLft, tTop, tRgt, tBot))
        {
            if(AdventureLevel::xItemNodeScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemNodeScript, 3, "S", mLocalName, "S", mEnemyPackName, "S", mItemNodeKeyword);
            }
            return true;
        }

        return false;
    }

    ///--[Flags]
    //--Can't be mugged, or is disabled.
    if(mIsDisabled) return false;
    if(!mCanBeMugged) return false;

    //--Global flag is set to bypass mugging.
    if(AdventureLevel::xDisableAllMugging) return false;

    //--Entity is ignoring the player.
    if(mIsIgnoringPlayer) return false;

    //--Entity is downed.
    if(mIsDying) return false;

    //--Must be on the same depth as the player.
    float tPlayerX=0.0f, tPlayerY, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
    if(mCollisionDepth != tPlayerZ) return false;

    //--Entity can be mugged, so let's see if the activation was on them.
    const float cHalfSize = TA_SIZE * 1.0f;

    ///--[Activation Check]
    //--Compute the position coordinates. Unlike normal enemies, extended activation is not allowed.
    float tLft = mTrueX - cHalfSize;
    float tTop = mTrueY - cHalfSize;
    float tRgt = mTrueX + TA_SIZE + cHalfSize;
    float tBot = mTrueY + TA_SIZE + cHalfSize;

    //--Check position.
    if(IsPointWithin(pX, pY, tLft, tTop, tRgt, tBot))
    {
        //--If the entity has spotted the player at all, or is already stunned, play a different sound.
        if(mSpottedPlayerTicks > 0 || mMugStunTimer > 0)
        {
            AudioManager::Fetch()->PlaySound("World|MugMiss");
            return true;
        }

        //--Increment.
        mMugTimer += TA_MUG_TICKS_PER_PRESS;

        //--If this put us over the cap, play a different sound.
        if(mMugTimer >= TA_MUG_TICKS_TOTAL)
        {
            AudioManager::Fetch()->PlaySound("World|MugSuccess");
        }
        //--Normal:
        else
        {
            AudioManager::Fetch()->PlaySound("World|MugHit");
        }
        return true;
    }

    //--All checks failed.
    return false;
}
