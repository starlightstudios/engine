///====================================== PeakFreaksFlyby =========================================
//--A 'flyby' entity, can be either a drone or a bird. This entity flies from left to right on the
//  screen, stopping in the middle to drop a payload. Which payload is configurable.

#pragma once

///========================================== Includes ============================================
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Subtypes
#define PFFB_SUBTYPE_GUANO 0
#define PFFB_SUBTYPE_BOMB 1
#define PFFB_SUBTYPE_SUPERBOMB 2
#define PFFB_SUBTYPE_MAX 3

//--Offsets
#define PFFB_VERTICAL_OFFSET -48.0f

//--Timing
#define PFFB_SEQ_TICKS_A 15
#define PFFB_SEQ_TICKS_B 15
#define PFFB_SEQ_TICKS_C 30
#define PFFB_SEQ_COMP_A (PFFB_SEQ_TICKS_A)
#define PFFB_SEQ_COMP_B (PFFB_SEQ_COMP_A + PFFB_SEQ_TICKS_B)
#define PFFB_SEQ_COMP_C (PFFB_SEQ_COMP_B + PFFB_SEQ_TICKS_C)
#define PFFB_MAX_TICKS (PFFB_SEQ_TICKS_A + PFFB_SEQ_TICKS_B + PFFB_SEQ_TICKS_C)

///========================================== Classes =============================================
class PeakFreaksFlyby
{
    private:
    ///--[System]
    int mSubtype;
    int mTimer;

    ///--[Position]
    float mXPosition;
    float mYPosition;
    float mTargetXPosition;
    float mEndingXPosition;

    ///--[Images]
    SugarBitmap *rImageA;
    SugarBitmap *rImageB;

    protected:

    public:
    //--System
    PeakFreaksFlyby();
    ~PeakFreaksFlyby();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool IsComplete();

    //--Manipulators
    void SetSubtype(int pSubtype);
    void SetImages(SugarBitmap *pImageA, SugarBitmap *pImageB);
    void SetTargetPosition(float pXPosition, float pYPosition);
    void SetEndingPosition(float pXPosition);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


