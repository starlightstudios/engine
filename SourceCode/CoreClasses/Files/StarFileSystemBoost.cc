//--Base
#include "StarFileSystem.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///====================================== Boost-Only Code =========================================
#if defined _BOOST_FILESYSTEM_
StarLinkedList *StarFileSystem::SubScanDirectory(const char *pPath, bool pScanDirectories)
{
    ///--[Documentation]
    //--Due to a bug in the Boost filesystem as available on MinGW, this whole thing doesn't work. It is
    //  left here for posterity. The engine never uses it.

    ///--[Error Cause]
    //--This is because the converters for pathnames are unreferenced. There is no way to get from a
    //  character path to a boost path.
    return NULL;
}
#endif
