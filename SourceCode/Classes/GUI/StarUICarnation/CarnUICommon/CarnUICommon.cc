//--Base
#include "CarnUICommon.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "CarnationMenu.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"

///========================================== System ==============================================
CarnUICommon::CarnUICommon()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Images]
    ///--[ ====== CarnUICommon ======= ]
    ///--[System]
    mIsForegroundObject = false;

    ///--[Colors]
    ///--[Mode Tabs]
    mAllowSaveMode = true;
    memset(mModeTabs, 0, sizeof(TwoDimensionReal) * CARNMENU_MODE_TOTAL);

    ///--[Images]
    memset(&CarnImagesCommon, 0, sizeof(CarnImagesCommon));

    ///--[ ================ Construction ================ ]
    //--Tab Positions
    mModeTabs[CARNMENU_MODE_STATUS].   SetWH(231.0f, 5.0f, 83.0f, 59.0f);
    mModeTabs[CARNMENU_MODE_EQUIP].    SetWH(317.0f, 5.0f, 83.0f, 59.0f);
    mModeTabs[CARNMENU_MODE_INVENTORY].SetWH(403.0f, 5.0f, 83.0f, 59.0f);
    mModeTabs[CARNMENU_MODE_SKILLS].   SetWH(489.0f, 5.0f, 83.0f, 59.0f);
    mModeTabs[CARNMENU_MODE_OPTIONS].  SetWH(576.0f, 5.0f, 83.0f, 59.0f);
    mModeTabs[CARNMENU_MODE_SAVE].     SetWH(661.0f, 5.0f, 83.0f, 59.0f);

    //--Add the CarnImages structure to the verify list.
    AppendVerifyPack("CarnImagesCommon", &CarnImagesCommon, sizeof(CarnImagesCommon));
}
CarnUICommon::~CarnUICommon()
{
}

///--[Public Statics]
//--Several UI components share a character cursor, so if you select a character in the status menu,
//  that character is selected in the equip menu. This static handles that.
int CarnUICommon::xPersistentCharacterCursor = 0;

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CarnUICommon::ZeroVisTimer()
{
    mVisibilityTimer = 0;
}
void CarnUICommon::FinishVisTimer()
{
    mVisibilityTimer = mVisibilityTimerMax;
}

///======================================= Core Methods ===========================================
int CarnUICommon::CheckTabsLeftClick(float pMouseX, float pMouseY)
{
    ///--[Documentation]
    //--Given a mouse position, checks if any of the common tabs at the top of the UI were clicked.
    //  If they were, returns the clicked index. Otherwise, returns -1 to indicate nothing was clicked.
    for(int i = 0; i < CARNMENU_TOP_RENDER; i ++)
    {
        if(mModeTabs[i].IsPointWithin(pMouseX, pMouseY))
        {
            return i;
        }
    }

    //--Nothing was clicked.
    return -1;
}
int CarnUICommon::CheckCharsLeftClick(float pMouseX, float pMouseY)
{
    ///--[Documentation]
    //--Given a mouse position, checks if the mouse hit the character tabs on the left side of the screen.
    //  If so, returns the index clicked, otherwise -1.
    float cLft =  42.0f;
    float cTop = 119.0f;
    float cRgt = cLft + 122.0f;
    float cBot = cTop + 55.0f;
    float cVSpacing = 61.0f;

    //--Variables.
    AdvCombat *rCombat = AdvCombat::Fetch();
    int tPartySize = rCombat->GetActivePartyCount();

    //--Run across party members.
    for(int i = 0; i < tPartySize; i ++)
    {
        //--Check for a hit. Change the character index if one is found.
        if(IsPointWithin(mMouseX, mMouseY, cLft, cTop, cRgt, cBot))
        {
            return i;
        }

        //--No hit. Advance the bounding box by the spacing.
        cTop = cTop + cVSpacing;
        cBot = cBot + cVSpacing;
    }

    //--Nobody hit. Return -1.
    return -1;
}
StarLinkedList *CarnUICommon::CreateDescriptionListFromString(const char *pString, StarFont *pFont, float pMaxWidth)
{
    ///--[Documentation]
    //--Given a string, many UIs need to break up the description into lines because they have different
    //  sizings for available space or different fonts. This routine runs the Subdivide() routine and
    //  returns the resulting linked list.
    //--On error, an empty list is returned. The caller is responsible for deallocation.
    if(!pString || !pFont)
    {
        return new StarLinkedList(true);
    }

    //--Call routine with common flags.
    return Subdivide::SubdivideStringFlags(pString, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, pFont, pMaxWidth);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUICommon::CommonTabClickUpdate()
{
    ///--[Documentation]
    //--Checks if the mode tabs were clicked. Returns true if they were and the update should stop,
    //  otherwise false.
    int tTabsReturnVal = CheckTabsLeftClick(mMouseX, mMouseY);
    if(tTabsReturnVal == -1) return false;

    //--Order the calling UI to change modes.
    FlagExit();
    mCodeBackward = tTabsReturnVal + CARNMENU_CHANGEMODE_OFFSET;

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

    //--Update ends here.
    return true;
}
bool CarnUICommon::CommonCharClickUpdate(int &sCharacterCursor)
{
    ///--[Documentation]
    //--Checks if the character tabs were clicked. Returns true if they were, false if not.
    int tClickedChar = CheckCharsLeftClick(mMouseX, mMouseY);
    if(tClickedChar == -1) return false;

    //--Set character cursor.
    sCharacterCursor = tClickedChar;

    //--Update the persistent cursor.
    xPersistentCharacterCursor = sCharacterCursor;

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

    //--Hit, return true to block update.
    return true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
StarBitmap *CarnUICommon::GetTabByActivity(int pActiveMode, int pFlaggedMode)
{
    ///--[Documentation]
    //--Returns the active tab is the mode is equal to the flagged mode, the inactive tab otherwise.
    if(pActiveMode == pFlaggedMode) return CarnImagesCommon.rTab_ModeActive;
    return CarnImagesCommon.rTab_ModeInactive;
}
void CarnUICommon::RenderTopTabs(int pActiveMode)
{
    ///--[Documentation]
    //--Several UIs share a set of tabs on the top of the screen that allow the user to click on them
    //  and change modes. This renders that. Whichever is the active mode renders using a different color.
    float cTabW = 86.0f;

    ///--[Tabs]
    for(int i = 0; i < CARNMENU_TOP_RENDER; i ++)
    {
        StarBitmap *rRenderImg = GetTabByActivity(pActiveMode, i);
        rRenderImg->Draw(cTabW * i, 0.0f);
    }

    ///--[Text]
    CarnImagesCommon.rFont_Tabs->DrawText(271.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Team");
    CarnImagesCommon.rFont_Tabs->DrawText(357.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equip");
    CarnImagesCommon.rFont_Tabs->DrawText(444.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Supply");
    CarnImagesCommon.rFont_Tabs->DrawText(530.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Skills");
    CarnImagesCommon.rFont_Tabs->DrawText(616.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Options");
    CarnImagesCommon.rFont_Tabs->DrawText(702.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Saving");
    //CarnImagesCommon.rFont_Tabs->DrawText(530.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Log");
    //CarnImagesCommon.rFont_Tabs->DrawText(616.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Options");
}
void CarnUICommon::RenderCharacterTabs(int pCursor)
{
    ///--[Documentation]
    //--Renders the character tabs on the left side of the UI that allow the player to switch the
    //  active character.
    AdvCombat *rCombat = AdvCombat::Fetch();
    if(!rCombat) return;

    //--Get party size, iterate.
    int tPartySize = rCombat->GetActivePartyCount();
    for(int i = 0; i < tPartySize; i ++)
    {
        //--Get party member.
        AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(i);
        if(!rAdvEntity) continue;

        //--Get name.
        const char *rMemberName = rAdvEntity->GetDisplayName();

        //--Resolve which label to use.
        StarBitmap *rUseLabel = CarnImagesCommon.rTab_CharGrey;
        if(!strcasecmp(rMemberName, "Beth")) rUseLabel = CarnImagesCommon.rTab_CharPink;
        if(!strcasecmp(rMemberName, "Lore")) rUseLabel = CarnImagesCommon.rTab_CharViolet;

        //--Render.
        float cYOffset = i * 61.0f;
        float cXOffset = 32.0f;
        if(i == pCursor) cXOffset = 0.0f;
        rUseLabel->Draw(cXOffset, cYOffset);

        //--Name.
        CarnImagesCommon.rFont_Name->DrawTextArgs(56.0f + cXOffset, 128.0f + cYOffset, 0, 1.0f, "%s", rMemberName);
    }
}

void CarnUICommon::RenderHexes()
{
    ///--[Documentation]
    //--Renders the hex counter in the top right of the screen.

    ///--[Execution]
    CarnImagesCommon.rFrame_Hexes->Draw();
    CarnImagesCommon.rFont_Hexes->DrawTextArgs(1162.0f, 11.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", AdventureInventory::Fetch()->GetPlatina());
}
void CarnUICommon::RenderDescription(float pX, float pY, float pH, StarLinkedList *pDescription, StarFont *pFont)
{
    ///--[Documentation]
    //--Many objects have descriptions of different sizes, so they are subdivided into a set of strings
    //  stored on a linked-list that can be rendered without going out of bounds. This routine uses
    //  the provided font to render a description list.
    if(!pDescription || !pFont) return;

    ///--[Execution]
    //--Setup.
    float tYPos = pY;

    //--Render.
    char *rString = (char *)pDescription->PushIterator();
    while(rString)
    {
        //--Render.
        pFont->DrawTextArgs(pX, tYPos, 0, 1.0f, rString);

        //--Next.
        tYPos = tYPos + pH;
        rString = (char *)pDescription->AutoIterate();
    }
}
void CarnUICommon::ReplaceCommonTagsInList(StarLinkedList *pList)
{
    ///--[Documentation]
    //--Given a linked list of char * pointers, scans the list and removes common tags like [PCT] and
    //  replaces them with their final versions. This should only be used at the last possible step
    //  before rendering.
    //--Note: This is considered destructive. Make sure nothing is pointing at a list entry when calling.
    if(!pList) return;

    //--Iterate.
    const char *rCheckPtr = (const char *)pList->SetToHeadAndReturn();
    while(rCheckPtr)
    {
        //--Run the subdivide routine on this using both [ and ] as delimiters. This will make every
        //  odd numbered entry a tag, and even-numbered entries the strings between them.
        StarAutoBuffer *nNewBuf = new StarAutoBuffer();
        StarLinkedList *tSplitString = Subdivide::SubdivideStringToListTags(rCheckPtr);

        //--Run across the strings. Even numbered strings are appended directly, odd numbered strings
        //  check for special tags.
        int i = 0;
        char *rSubString = (char *)tSplitString->PushIterator();
        while(rSubString)
        {
            //--Even numbered.
            if(i % 2 == 0)
            {
                nNewBuf->AppendStringWithoutNull(rSubString);
            }
            //--Odd numbered.
            else
            {
                if(!strcmp(rSubString, "[PCT]"))
                {
                    nNewBuf->AppendStringWithoutNull("%%");
                }
                else
                {
                    nNewBuf->AppendStringWithoutNull(rSubString);
                }
            }

            //--Next.
            i ++;
            rSubString = (char *)tSplitString->AutoIterate();
        }

        //--Finish the string up.
        nNewBuf->AppendNull();

        //--Get the whole entry, replace the string with the buffer's verison.
        StarLinkedListEntry *rEntry = pList->GetRandomPointerWholeEntry();
        free(rEntry->rData);
        rEntry->rData = nNewBuf->LiberateData();
        delete nNewBuf;
        delete tSplitString;

        //--Next.
        rCheckPtr = (const char *)pList->IncrementAndGetRandomPointerEntry();
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
