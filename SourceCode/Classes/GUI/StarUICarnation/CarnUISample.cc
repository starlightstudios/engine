//Replace CarnUISample with name of UI
//Delete these instructions

//--Base
#include "CarnUISample.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
CarnUISample::CarnUISample()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Help Menu]

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== CarnUISample ======= ]
    ///--[System]
    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));
}
CarnUISample::~CarnUISample()
{
}
void CarnUISample::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    //ResolveSeriesInto("Carnation Type UI",           &CarnImages, sizeof(CarnImages));
    //ResolveSeriesInto("Adventure Type UI Carnation", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",           &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void CarnUISample::TakeForeground()
{

}

///======================================= Core Methods ===========================================
void CarnUISample::RefreshMenuHelp()
{

}
void CarnUISample::RecomputeCursorPositions()
{

}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUISample::UpdateForeground(bool pCannotHandleUpdate)
{
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }
    return true;
}
void CarnUISample::UpdateBackground()
{

}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
