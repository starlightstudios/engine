//--Base
#include "AdvUIPassword.h"

//--Classes
#include "AbyssCombat.h"
#include "AdvUIBase.h"
#include "AdvUICostumes.h"
#include "AdventureDebug.h"
#include "AdventureLevel.h"
#include "AdventureMenu.h"
#include "AdvUIJournal.h"
#include "RunningMinigame.h"
#include "StringEntry.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIPassword::AdvUIPassword()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "APSW");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIPassword ======= ]
    ///--[System]
    mStringEntryForm = StringEntry::rGenerateStringEntry();

    ///--[Images]
    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    //AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIPassword::~AdvUIPassword()
{
    delete mStringEntryForm;
}
void AdvUIPassword::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    //ResolveSeriesInto("Adventure Equipment UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",      &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Variables]
//--When using passwords, indicates that the last called file handled the password somehow.
bool AdvUIPassword::xHandledPassword = false;

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIPassword::TakeForeground()
{
    ///--[Documentation]
    //--Reset the string entry handler.
    mStringEntryForm->SetCompleteFlag(false);
    mStringEntryForm->SetPromptString("Enter Password");
    mStringEntryForm->SetString(NULL);
    mStringEntryForm->SetEmptyString("Enter Password");
}

///======================================= Core Methods ===========================================
void AdvUIPassword::RefreshMenuHelp()
{
}
void AdvUIPassword::RecomputeCursorPositions()
{
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIPassword::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles input and timers for password mode. Returns true if it handled the update, false otherwise.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, true);

    ///--[String Entry Form]
    //--Run the update.
    mStringEntryForm->Update();

    //--Check for completion.
    if(mStringEntryForm->IsComplete())
    {
        //--Flag.
        FlagExit();
        xHandledPassword = false;

        //--Get the string entered. It must be at least one character long.
        const char *rEnteredString = mStringEntryForm->GetString();
        if(strlen(rEnteredString) < 1) return true;

        //--First, debug-mode is hard coded.
        if(!strcasecmp(rEnteredString, "Demantis Mode"))
        {
            AdventureDebug::xManualActivation = true;
            AudioManager::Fetch()->PlaySound("Menu|Save");
            return true;
        }
        //--Play the running minigame!
        else if(!strcasecmp(rEnteredString, "Leg It, 55!"))
        {
            //--Immediately exit all UI.
            FlagExit();
            SetCodeBackward(AM_MODE_EXITNOW);
            AudioManager::Fetch()->PlaySound("Menu|Save");

            //--Create and run the minigame.
            RunningMinigame *nMinigame = new RunningMinigame();
            nMinigame->Construct();
            nMinigame->AssemblePlayerImages();
            nMinigame->GenerateLevel();
            nMinigame->Activate();
            nMinigame->SetHoldingPattern(true);
            AdventureLevel::Fetch()->ReceiveRunningMinigame(nMinigame);
            return true;
        }
        //--Show all journal entries.
        else if(!strcasecmp(rEnteredString, "Journalissimo"))
        {
            AdvUIJournal::xShowAllEntries = true;
            AudioManager::Fetch()->PlaySound("Menu|Save");
            return true;
        }
        //--100% chance to play combat callouts.
        else if(!strcasecmp(rEnteredString, "56709"))
        {
            AbyssCombat::xAlwaysPlayCallouts = true;
            AudioManager::Fetch()->PlaySound("World|Awoo");
            return true;
        }
        //--Stupid antchievement.
        else if(!strcasecmp(rEnteredString, "Overantchiever"))
        {
            AudioManager::Fetch()->PlaySound("Menu|Save");
            AdventureMenu::xUseAntchievement = true;
            return true;
        }

        //--Next, send the instruction to the costume handler. It will handle any unlocking.
        if(AdvUICostumes::xCostumeResolveScript)
        {
            //--Execute.
            LuaManager::Fetch()->ExecuteLuaFile(AdvUICostumes::xCostumeResolveScript, 1, "S", rEnteredString);

            //--If this flag is flipped on, the file handled the password.
            if(xHandledPassword)
            {
                AudioManager::Fetch()->PlaySound("Menu|Save");
                return true;
            }
        }

        //--If we got this far, the checks failed and nothing worked.
        AudioManager::Fetch()->PlaySound("Menu|Failed");
    }
    //--Cancelled out.
    else if(mStringEntryForm->IsCancelled())
    {
        FlagExit();
    }

    //--We handled the update.
    return true;
}
void AdvUIPassword::UpdateBackground()
{
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIPassword::RenderForeground(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Most of the rendering here is handled by the string entry form. We just set its opacity.
    if(mVisibilityTimer < 1) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = EasingFunction::QuadraticInOut(mVisibilityTimer, mVisibilityTimerMax);

    //--Render password entry.
    mStringEntryForm->Render(tOpacityPct);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
