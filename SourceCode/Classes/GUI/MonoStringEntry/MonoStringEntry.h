///====================================== MonoStringEntry =========================================
//--Monoceros version of the StringEntry class. Slight reskin, changes a lot of positions.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StringEntry.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class MonoStringEntry : public StringEntry
{
    private:
    ///--[System]
    ///--[Images]
    //--Colors
    StarlightColor mMonoTitle;
    StarlightColor mMonoSubtitle;
    StarlightColor mMonoParagraph;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontHeader;
            StarFont *rFontCurEntry;
            StarFont *rFontMainline;

            //--Images
            StarBitmap *rCursor;
            StarBitmap *rRenderBase;
            StarBitmap *rRenderPressed;
        }Data;
    }MonoImg;

    protected:

    public:
    //--System
    MonoStringEntry();
    virtual ~MonoStringEntry();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Render(float pOpacity);

    //--Pointer Routing
    //--Static Functions
    static StringEntry *GenerateStringEntry();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


