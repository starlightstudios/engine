//--Base
#include "MonoUIFileSelect.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StringEntry.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "FlexMenu.h"
#include "VirtualFile.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "SaveManager.h"

///========================================== System ==============================================
MonoUIFileSelect::MonoUIFileSelect()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MFLS");

    ///--[Visiblity]
    ///--[Help Menu]

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUIFileSelect ======= ]
    ///--[System]
    ///--[Help Strings]
    //--Help Strings
    mConfirmDeleteString = new StarlightString();
    mCancelDeleteString = new StarlightString();

    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIFileSelect::~MonoUIFileSelect()
{
    delete mConfirmDeleteString;
    delete mCancelDeleteString;
}
void MonoUIFileSelect::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros File Select UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure File Select UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",                  &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIFileSelect_Strings *MonoUIFileSelect::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIFileSelect_Strings::MonoUIFileSelect_Strings()
{
    //--Local name: "MonoUIFileSelect"
    MonoUIFileSelect::xrStringData = this;

    //--Strings
    str.mTextCurrentBySlot             = InitializeString("MonoUIFileSelect_mCurrentBySlot");             //"Current: By Slot"
    str.mTextCurrentReverseSlot        = InitializeString("MonoUIFileSelect_mCurrentReverseSlot");        //"Current: Reverse Slot"
    str.mTextCurrentNewestFirst        = InitializeString("MonoUIFileSelect_mCurrentNewestFirst");        //"Current: Newest First"
    str.mTextCurrentOldestFirst        = InitializeString("MonoUIFileSelect_mCurrentOldestFirst");        //"Current: Oldest First"
    str.mTextEnterANoteForThisSaveFile = InitializeString("MonoUIFileSelect_mEnterNote");                 //"Enter a note for this save file."
    str.mTextEnterName                 = InitializeString("MonoUIFileSelect_mEnterName");                 //"Enter Name"
    str.mTextSelectASave               = InitializeString("MonoUIFileSelect_mSelectASave");               //"Select A Save"
    str.mTextCreateNewSavefile         = InitializeString("MonoUIFileSelect_mCreateNewSavefile");         //"Create New Savefile"
    str.mTextNoEmptySaveSlotsAvailable = InitializeString("MonoUIFileSelect_mNoEmptySaveSlotsAvailable"); //"No Empty Save Slots Available"
    str.mTextEmptySlot                 = InitializeString("MonoUIFileSelect_mEmptySlot");                 //"Empty Slot"
    str.mTextReallyDeleteThisFile      = InitializeString("MonoUIFileSelect_mReallyDeleteThisFile?");     //"Really Delete This File?"

    //--Help Sequence
    //--Control Sequence
    str.mTextControlDeleteFile    = InitializeString("MonoUIFileSelect_mDeleteFile");    //"[IMG0] Delete File"
    str.mTextControlCancel        = InitializeString("MonoUIFileSelect_mCancel");        //"[IMG0] Cancel"
    str.mTextControlEditFileNote  = InitializeString("MonoUIFileSelect_mEditFileNote");  //"[IMG0] Edit File Note"
    str.mTextControlChangeSorting = InitializeString("MonoUIFileSelect_mChangeSorting"); //"[IMG0] Change Sorting"
    str.mTextControlChangePages   = InitializeString("MonoUIFileSelect_mChangePages");   //"[IMG0][IMG1] Change Pages"
    str.mTextControlFirstLastPage = InitializeString("MonoUIFileSelect_mFirstLastPage"); //"[IMG0][IMG1] First/Last Page"
    str.mTextControlPageLeft      = InitializeString("MonoUIFileSelect_mPageLeft");      //"[IMG0] + [IMG1] Page Left"
    str.mTextControlPageRight     = InitializeString("MonoUIFileSelect_mPageRight");     //"[IMG0] + [IMG1] Page Right"
}
MonoUIFileSelect_Strings::~MonoUIFileSelect_Strings()
{
    for(int i = 0; i < MONOUIFILESELECT_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIFileSelect_Strings::GetSize()
{
    return MONOUIFILESELECT_STRINGS_TOTAL;
}
void MonoUIFileSelect_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIFILESELECT_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
int MonoUIFileSelect::GetArraySlotFromLookups(int pLookup)
{
    ///--[Documentation]
    //--Given a position in the visual lookups, returns the actual array position of the entry in question, or -1 on error.
    //  The files run from File00.slf to File99.slf, but the lookups change positions around to make sorting easier.
    //--The passed-in position is where we are on the grid, which goes from 0x0 to 4x4, or 0 to 24 internally.

    //--Get the grid position and add the page to it.
    int tCurrent = pLookup;
    if(tCurrent < 0 || tCurrent >= ADVUIFILE_SCAN_TOTAL) return -1;

    //--Check the index. If it's out of range, standardize it to -1.
    int tReturn = mFileListingIndex[tCurrent];
    if(tReturn < 0 || tReturn >= ADVUIFILE_SCAN_TOTAL) return -1;

    //--Now pass back the actual index.
    return tReturn;
}

///======================================= Manipulators ===========================================
void MonoUIFileSelect::TakeForeground()
{
    ///--[Documentation]
    //--Calls the base function, but also updates the confirm/cancel deletion strings.
    AdvUIFileSelect::TakeForeground();

    ///--[New Strings]
    //--Set.
    mConfirmDeleteString->AutoSetControlString(xrStringData->str.mTextControlDeleteFile, 1, "Activate");
    mCancelDeleteString-> AutoSetControlString(xrStringData->str.mTextControlCancel, 1, "Cancel");

    ///--[Old Strings]
    //--Set.
    mEditNoteString  ->AutoSetControlString(xrStringData->str.mTextControlEditFileNote, 1, "F1");
    mCycleSortString ->AutoSetControlString(xrStringData->str.mTextControlChangeSorting, 1, "F2");
    mScrollPageString->AutoSetControlString(xrStringData->str.mTextControlChangePages, 2, "UpLevel", "DnLevel");
    mDeleteFileString->AutoSetControlString(xrStringData->str.mTextControlDeleteFile, 1, "Delete");
    mFastScrollString->AutoSetControlString(xrStringData->str.mTextControlFirstLastPage, 2, "GUI|Home", "GUI|End");
    mPageLftString   ->AutoSetControlString(xrStringData->str.mTextControlPageLeft, 2, "Ctrl", "Left");
    mPageRgtString   ->AutoSetControlString(xrStringData->str.mTextControlPageRight, 2, "Ctrl", "Right");

    //--Resolve by sort type.
    ResolveSortString();
}

///======================================= Core Methods ===========================================
void MonoUIFileSelect::RefreshMenuHelp()
{

}
void MonoUIFileSelect::RecomputeCursorPositions()
{

}
void MonoUIFileSelect::ScanExistingFiles()
{
    ///--[Documentation]
    //--Scans across the file listing on the hard drive, checking off all the ones that do exist so they can be
    //  displayed in order. May be slow, this are a lot of hard-drive accesses.
    //--The only difference from the base version is that blank files are not counted.
    char *tFileBuf = NULL;

    //--Deallocate.
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        if(mLoadingPacks[i]) LoadingPack::DeleteThis(mLoadingPacks[i]);
    }

    //--Clear data.
    mEmptySaveSlotsTotal = ADVUIFILE_SCAN_TOTAL;
    mSelectFileCursor = -1;
    mSelectFileOffset = 0;
    mHighestCursor = 0;
    mEditingFileCursor = -1;
    memset(mLoadingPacks,        0, sizeof(LoadingPack *) * ADVUIFILE_SCAN_TOTAL);
    memset(mExistingFileListing, 0, sizeof(bool)          * ADVUIFILE_SCAN_TOTAL);

    ///--[Temporary Menu]
    //--Create a temporary FlexMenu. We need it to resolve images for us.
    FlexMenu *tMenu = new FlexMenu();
    tMenu->Construct();

    //--Game Directory:
    const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

    ///--[Scan Files]
    //--Begin scanning. Info is placed in the slots in order from 0 to ADVUIFILE_SCAN_TOTAL, but everything else
    //  will use mFileListingIndex[] to sort the results.
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        //--Assemble filename.
        tFileBuf = InitializeString("%s/../../Saves/File%03i.slf", rAdventureDir, i);

        //--Check for existence.
        FILE *fInfile = fopen(tFileBuf, "rb");

        //--Flag.
        mExistingFileListing[i] = (fInfile != NULL);
        if(mExistingFileListing[i])
        {
            //--Reduce the empty saves count.
            mEmptySaveSlotsTotal --;

            //--Close the file.
            fclose(fInfile);

            //--Save the filename.
            sprintf(mFileListing[mHighestCursor], "File%03i.slf", i);

            //--Store the file info.
            mLoadingPacks[mHighestCursor] = SaveManager::Fetch()->GetSaveInfo(tFileBuf);
            tMenu->CrossloadSaveImages(mLoadingPacks[mHighestCursor]);

            //--Indicate this is selectable.
            mHighestCursor ++;
        }
        //--Empty slots appear as blank.
        else
        {
        }

        //--Clean.
        free(tFileBuf);
    }

    //--Clean.
    delete tMenu;

    ///--[Sort]
    //--Actually sort things.
    if(mCurrentSortingType == AM_SORT_SLOT)
    {
        SortFilesBySlot();
    }
    else if(mCurrentSortingType == AM_SORT_SLOTREV)
    {
        SortFilesBySlotRev();
    }
    else if(mCurrentSortingType == AM_SORT_DATEREV)
    {
        SortFilesByDateRev();
    }
    else if(mCurrentSortingType == AM_SORT_DATE)
    {
        SortFilesByDate();
    }
}
void MonoUIFileSelect::ResolveSortString()
{
    ///--[Documentation]
    //--Checks the sorting type variable and changes the sorting string to reflect it.

    //--Current Sort Type
    if(mCurrentSortingType == AM_SORT_SLOT)
    {
        mCurrentSortString->SetString(xrStringData->str.mTextCurrentBySlot);
    }
    else if(mCurrentSortingType == AM_SORT_SLOTREV)
    {
        mCurrentSortString->SetString(xrStringData->str.mTextCurrentReverseSlot);
    }
    else if(mCurrentSortingType == AM_SORT_DATEREV)
    {
        mCurrentSortString->SetString(xrStringData->str.mTextCurrentNewestFirst);
    }
    else if(mCurrentSortingType == AM_SORT_DATE)
    {
        mCurrentSortString->SetString(xrStringData->str.mTextCurrentOldestFirst);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIFileSelect::UpdateDirectionalControls()
{
    ///--[Documentation]
    //--Similar to the base version, but Monoceros' save menu is not a grid, just a list, so only
    //  up and down are supported.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Single Moves]
    //--Up.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--When on "New File", do nothing.
        if(mSelectFileCursor == -1)
        {
        }
        //--Move the loading offset and clamp.
        else
        {
            //--Resolve number of moves.
            int tIterations = 1;
            if(rControlManager->IsDown("Ctrl")) tIterations = 3;

            //--Move.
            for(int i = 0; i < tIterations; i ++)
            {
                mSelectFileCursor --;
                if(mSelectFileCursor - mSelectFileOffset == 0) mSelectFileOffset --;
                if(mSelectFileOffset < 0) mSelectFileOffset = 0;
            }

            //--Range check.
            if(mSelectFileCursor < -1) mSelectFileCursor = -1;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    //--Down.
    if(rControlManager->IsFirstPress("Down"))
    {
        //--Resolve number of moves.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 3;

        //--Move.
        for(int i = 0; i < tIterations; i ++)
        {
            //--Move the cursor and clamp.
            mSelectFileCursor ++;

            //--If the loading offset pushes the cursor offscreen, cap it.
            if(mSelectFileCursor - mSelectFileOffset >= cxMonoEntriesPerPage)
            {
                mSelectFileOffset ++;
                if(mSelectFileOffset > ADVUIFILE_SCAN_TOTAL - cxMonoEntriesPerPage) mSelectFileOffset --;
                if(mSelectFileOffset < 0) mSelectFileOffset = 0;
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[No Input]
    return false;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIFileSelect::RenderPieces(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the save interface, allowing the player to make a new save, save over existing files,
    //  and so on. This UI has a flat fade-in functionality instead of sliding in from the sides.

    //--Opacity percent. Determines position and fading.
    float cColorAlpha = (float)mVisibilityTimer / (float)mVisibilityTimerMax;

    ///--[Header]
    //--Backing.
    SetColor("Clear", cColorAlpha);
    MonoImages.rOverlay_SaveShade->Draw();

    //--Colored Text.
    SetColor("Title", cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 34.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSelectASave);
    SetColor("Clear", cColorAlpha);

    ///--[New Save Button]
    //--Backing, unselected:
    if(mSelectFileCursor == -1)
    {
        MonoImages.rFrame_NewGameSelect->Draw();
    }
    //--Backing, selected.
    else
    {
        MonoImages.rFrame_NewGame->Draw();
    }

    //--Backing and Text.
    SetColor("Paragraph", cColorAlpha);
    if(mEmptySaveSlotsTotal > 0)
    {
        AdvImages.rFont_Header->DrawText(VIRTUAL_CANVAS_X * 0.50f, 149.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextCreateNewSavefile);
    }
    //--No save slots open!
    else
    {
        AdvImages.rFont_Header->DrawText(VIRTUAL_CANVAS_X * 0.50f, 149.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextNoEmptySaveSlotsAvailable);
    }

    //--Clean.
    SetColor("Clear", cColorAlpha);

    ///--[Scrollbar]
    //--Shows a scrollbar if there are more than cxMonoEntriesPerPage.
    if(mHighestCursor > cxMonoEntriesPerPage)
    {
        int tUseOffset = mSelectFileOffset;
        if(tUseOffset > mHighestCursor - cxMonoEntriesPerPage) tUseOffset = mHighestCursor - cxMonoEntriesPerPage;
        StandardRenderScrollbar(tUseOffset, cxMonoEntriesPerPage, mHighestCursor - cxMonoEntriesPerPage, 105.0f, 642.0f, false, MonoImages.rScrollbar_Scroller, MonoImages.rScrollbar_Static);
    }

    ///--[Loading Packs]
    //--New game button sizing.
    TwoDimensionReal cBoxDimensions;
    float cSizePerBox = 137.0f;
    float cBoxSpacing =  10.0f;
    float cTextSize = 1.0f;

    //--Render the save game loading packs.
    for(int i = 0; i < cxMonoEntriesPerPage; i ++)
    {
        ///--[Check Entry]
        int tFileSlot = GetArraySlotFromLookups(i + mSelectFileOffset);
        if(tFileSlot < 0 || tFileSlot >= ADVUIFILE_SCAN_TOTAL) return;

        ///--[Rendering]
        //--Compute position.
        float cTop = MonoImages.rFrame_SaveCellSelect->GetYOffset() + (cSizePerBox * i) + (cBoxSpacing * i);
        cBoxDimensions.Set(69.0f, cTop, 1249.0f, cTop + cSizePerBox);

        //--Render the border card.
        if(mSelectFileCursor - mSelectFileOffset == i)
        {
            MonoImages.rFrame_SaveCellSelect->Draw(0.0f, cTop - MonoImages.rFrame_SaveCellSelect->GetYOffset());
        }
        else
        {
            MonoImages.rFrame_SaveCell->Draw(0.0f, cTop - MonoImages.rFrame_SaveCellSelect->GetYOffset());
        }

        ///--[Empty Slot]
        //--Empty spots are rendered with "Empty Slot".
        if(!mLoadingPacks[tFileSlot])
        {
            //--Empty slot indicator.
            AdvImages.rFont_Mainline->DrawText(cBoxDimensions.mLft + 61.0f, cBoxDimensions.mTop + 6.0f, 0, cTextSize, xrStringData->str.mTextEmptySlot);

            //--File number if this slot were to be occupied.
            //rUseFont->DrawTextArgs(pLft + 5.0f, pTop + 3.0f, 0, 1.0f, "File%03i.slf", tFileSlot);
            continue;
        }

        ///--[Filled Slot]
        //--Render the name of the save file. This is not its path, and it can be renamed!
        LoadingPack *rPack = mLoadingPacks[tFileSlot];
        AdvImages.rFont_Mainline->DrawTextArgs(cBoxDimensions.mLft + 61.0f, cBoxDimensions.mTop + 6.0f, 0, cTextSize, "File %i: %s", tFileSlot, rPack->mFileName);

        //--Timestamp.
        float tTimestampLen = AdvImages.rFont_Mainline->GetTextWidth(rPack->mTimestamp) * cTextSize;
        AdvImages.rFont_Mainline->DrawText(cBoxDimensions.mRgt - 14.0f - tTimestampLen, cBoxDimensions.mTop + 8.0f, 0, cTextSize, rPack->mTimestamp);

        //--Render what map the player was on.
        AdvImages.rFont_Mainline->DrawText(cBoxDimensions.mLft + 61.0f, cBoxDimensions.mBot - 8.0f - (AdvImages.rFont_Mainline->GetTextHeight() * cTextSize), 0, cTextSize, rPack->mMapLocation);

        //--Render the characters in it.
        float cSpriteScale = 2.0f;
        float cPortraitScale = 0.355932f;
        for(int p = 0; p < 4; p ++)
        {
            //--Compute positions.
            float cLft = cBoxDimensions.mLft + 86.0f + (200.0f * p);
            float cTop = cBoxDimensions.mTop + 38.0f;

            //--Check if the savefile in question has the hard path in it:
            if(!strncasecmp(rPack->mPartyNames[p], "HardPath|", 9))
            {
                //--Offset position.
                cLft = cLft - 36.0f;
                cTop = cTop - 12.0f;

                //--Subdivide the string. The second block is the character's name. The third is the DLPath.
                StarLinkedList *tStringList = Subdivide::SubdivideStringToList(rPack->mPartyNames[p], "|");

                //--Image.
                const char *rCharName = (const char *)tStringList->GetElementBySlot(1);
                const char *rDLPathString = (const char *)tStringList->GetElementBySlot(2);
                rPack->rRenderImg[p] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(rDLPathString);

                //--Render portrait, needs scaling.
                if(rPack->rRenderImg[p])
                {
                    glTranslatef(cLft, cTop, 0.0f);
                    glScalef(cPortraitScale, cPortraitScale, 1.0f);
                    rPack->rRenderImg[p]->Draw();
                    glScalef(1.0f / cPortraitScale, 1.0f / cPortraitScale, 1.0f);
                    glTranslatef(-cLft, -cTop, 0.0f);
                }

                //--Name.
                AdvImages.rFont_Mainline->DrawText    (cLft + 116.0f, cTop + 28.0f, 0, 1.0f, rCharName);
                AdvImages.rFont_Mainline->DrawTextArgs(cLft + 116.0f, cTop + 48.0f, 0, 1.0f, "Lv. %i", rPack->mPartyLevels[p] + 1);

                //--Clean.
                delete tStringList;
            }
            //--Render sprites.
            else
            {
                //--Render. We need to scale this up.
                if(rPack->rRenderImg[p])
                {
                    glTranslatef(cLft, cTop, 0.0f);
                    glScalef(cSpriteScale, cSpriteScale, 1.0f);
                    rPack->rRenderImg[p]->Draw();
                    glScalef(1.0f / cSpriteScale, 1.0f / cSpriteScale, 1.0f);
                    glTranslatef(-cLft, -cTop, 0.0f);
                }

                //--Get the name from the buffer.
                int tCutoff = -1;
                for(int x = (int)strlen(rPack->mPartyNames[p]); x >= 0; x --)
                {
                    if(rPack->mPartyNames[p][x] == '_')
                    {
                        tCutoff = x;
                        break;
                    }
                }

                //--Error.
                if(tCutoff == -1) continue;

                //--Render.
                rPack->mPartyNames[p][tCutoff] = '\0';
                AdvImages.rFont_Mainline->DrawText    (cLft + 109.0f, cTop + 28.0f, 0, 1.0f, rPack->mPartyNames[p]);
                AdvImages.rFont_Mainline->DrawTextArgs(cLft + 109.0f, cTop + 48.0f, 0, 1.0f, "Lv. %i", rPack->mPartyLevels[p] + 1);
                rPack->mPartyNames[p][tCutoff] = '_';
            }
        }
    }
    SetColor("Clear", cColorAlpha);

    ///--[Help Strings]
    //--Left side.
    mEditNoteString->   DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mCycleSortString->  DrawText(0.0f, cxMonoFontMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mCurrentSortString->DrawText(0.0f, cxMonoFontMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--Right side.
    //mScrollPageString->DrawText(1090.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mDeleteFileString->DrawText(1090.0f, cxMonoFontMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    //mFastScrollString->DrawText(1090.0f, cxMonoFontMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--Left bottom
    //mPageLftString->DrawText(0.0f, VIRTUAL_CANVAS_Y - cxMonoFontMainlineFontH, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    //mPageRgtString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - cxMonoFontMainlineFontH, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, MonoImages.rFont_Control);

    ///--[Deletion Handler]
    //--Darkening.
    float tDeletionOpacity = EasingFunction::QuadraticInOut(mSaveDeletionOpacityTimer, cxMonoDeletionTicks);
    if(tDeletionOpacity > 0.0f) StarBitmap::DrawFullBlack(tDeletionOpacity * 0.80f);

    //--Text.
    if(mSaveDeletionIndex != -1)
    {
        //--Strings.
        SetColor("Clear", cColorAlpha);
        AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.37f +  0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextReallyDeleteThisFile);
        mConfirmDeleteString         ->DrawText(VIRTUAL_CANVAS_X * 0.30f, VIRTUAL_CANVAS_Y * 0.37f + 90.0f, 0, 1.0f, AdvImages.rFont_Mainline);
        mCancelDeleteString          ->DrawText(VIRTUAL_CANVAS_X * 0.60f, VIRTUAL_CANVAS_Y * 0.37f + 90.0f, 0, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Clean]
    SetColor("Clear", cColorAlpha);

    ///--[Entering Notes]
    //--Notes appear over everything else.
    if(mIsEnteringNotes)
    {
        mStringEntryForm->Render();
        return;
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
