//--Base
#include "AdvUIFieldAbilities.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureMenu.h"
#include "FieldAbility.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIFieldAbilities::AdvUIFieldAbilities()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AFDA");

    ///--[Visiblity]
    mVisibilityTimer = 0;
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIFieldAbilities ======= ]
    ///--[Variables]
    //--System
    mIsFirstTick = false;
    mIsSwitching = false;
    mHiCursor = 0;
    mLoCursor = 0;

    //--Field Ability List
    mrFieldAbilityList = new StarLinkedList(false);

    //--Cursor Movement
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();
    mLolightPos.Initialize();
    mLolightSize.Initialize();

    //--Help Text
    mConfirmString = new StarlightString();
    mCancelString  = new StarlightString();
    mClearString   = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIFieldAbilities::~AdvUIFieldAbilities()
{
    delete mrFieldAbilityList;
    delete mConfirmString;
    delete mCancelString;
    delete mClearString;
}
void AdvUIFieldAbilities::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Field Ability UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",          &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIFieldAbilities::TakeForeground()
{
    //--Setup.
    float cRenderWid = 150.0f;
    float cRenderLft = 245.0f;
    float cRenderTop = 185.0f;

    //--System
    mIsFirstTick = true;
    mIsSwitching = false;
    mHiCursor = 0;
    mLoCursor = 0;

    //--Cursor Movement
    mHighlightPos.MoveTo(cRenderLft, cRenderTop, 0);
    mHighlightSize.MoveTo(cRenderWid, cRenderWid, 0);

    //--Refresh field abilities.
    mrFieldAbilityList->ClearList();
    LuaManager::Fetch()->ExecuteLuaFile(AdventureMenu::xFieldAbilityResolveScript);

    //--Refresh strings.
    mConfirmString->SetString("[IMG0] Select Slot/Skill");
    mConfirmString->AllocateImages(1);
    mConfirmString->SetImageP(0, 4.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mConfirmString->CrossreferenceImages();

    mCancelString->SetString("[IMG0] Cancel/Return to Game");
    mCancelString->AllocateImages(1);
    mCancelString->SetImageP(0, 4.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mCancelString->CrossreferenceImages();

    mClearString->SetString("[IMG0] Clear Slot");
    mClearString->AllocateImages(1);
    mClearString->SetImageP(0, 4.0f, ControlManager::Fetch()->ResolveControlImage("Jump"));
    mClearString->CrossreferenceImages();
}
void AdvUIFieldAbilities::RegisterFieldAbility(const char *pDLPath)
{
    ///--[Documentation]
    //--Registers a field ability to the UI object, requires the path in question.
    FieldAbility *rFieldAbility = (FieldAbility *)DataLibrary::Fetch()->GetEntry(pDLPath);
    if(!rFieldAbility)
    {
        DebugManager::ForcePrint("AdvUIFieldAbilities:RegisterFieldAbility() - Warning, no ability at %s.\n", pDLPath);
        return;
    }

    ///--[Execution]
    //--Register it.
    mrFieldAbilityList->AddElement("X", rFieldAbility);
}

///======================================= Core Methods ===========================================
void AdvUIFieldAbilities::RefreshMenuHelp()
{

}
void AdvUIFieldAbilities::RecomputeCursorPositions()
{

}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIFieldAbilities::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles primary update responsibility.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--Cursor.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
    mLolightPos.Increment(EASING_CODE_QUADOUT);
    mLolightSize.Increment(EASING_CODE_QUADOUT);

    //--Visiblity timer.
    if(mVisibilityTimer < mVisibilityTimerMax) mVisibilityTimer ++;

    //--On the first tick, stop the update. This is because the key is still down and the key
    //  to open the UI is the same as the one to close it.
    if(mIsFirstTick)
    {
        mIsFirstTick = false;
        return true;
    }

    ///--[Controls]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Setup for cursor.
    float cRenderWid = 150.0f;
    float cRenderSpc = 175.0f;
    float cRenderLft = 245.0f;
    float cRenderTop = 185.0f;

    //--Lower Render.
    float cAbiLft = 40.0f;
    float cAbiTop = 457.0f;
    float cAbiWid = 50.0f;
    float cAbiSpc = 55.0f;

    ///--[Slot Select]
    if(!mIsSwitching)
    {
        //--Left, decrement cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mHiCursor > 0)
            {
                mHiCursor --;
                mHighlightPos.MoveTo(cRenderLft + (cRenderSpc * mHiCursor), cRenderTop, cxAdvCurTicks);
                mHighlightSize.MoveTo(cRenderWid, cRenderWid, cxAdvCurTicks);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right, increment cursor.
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mHiCursor < cxAdv_FieldAbilitySlots-1)
            {
                mHiCursor ++;
                mHighlightPos.MoveTo(cRenderLft + (cRenderSpc * mHiCursor), cRenderTop, cxAdvCurTicks);
                mHighlightSize.MoveTo(cRenderWid, cRenderWid, cxAdvCurTicks);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Accept, begin switching.
        else if(rControlManager->IsFirstPress("Activate"))
        {
            //--Flag.
            mIsSwitching = true;

            //--Specify the cursor position as where the top cursor was.
            mLolightPos.MoveTo(cRenderLft + (cRenderSpc * mHiCursor), cRenderTop, 0);
            mLolightSize.MoveTo(cRenderWid, cRenderWid, 0);

            mLolightPos.MoveTo(cAbiLft + (cAbiSpc * mLoCursor), cAbiTop, cxAdvCurTicks);
            mLolightSize.MoveTo(cAbiWid, cAbiWid, cxAdvCurTicks);

            //--Audio.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Cancel, exit menu.
        else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
        {
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Jump, clear selection.
        else if(rControlManager->IsFirstPress("Jump"))
        {
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            rAdventureCombat->SetFieldAbility(mHiCursor, NULL);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Replacement Select]
    else
    {
        //--Left, decrement cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mLoCursor > 0)
            {
                mLoCursor --;
                mLolightPos.MoveTo(cAbiLft + (cAbiSpc * mLoCursor), cAbiTop, cxAdvCurTicks);
                mLolightSize.MoveTo(cAbiWid, cAbiWid, cxAdvCurTicks);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right, increment cursor.
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mLoCursor < mrFieldAbilityList->GetListSize()-1)
            {
                mLoCursor ++;
                mLolightPos.MoveTo(cAbiLft + (cAbiSpc * mLoCursor), cAbiTop, cxAdvCurTicks);
                mLolightSize.MoveTo(cAbiWid, cAbiWid, cxAdvCurTicks);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Accept, begin switching.
        else if(rControlManager->IsFirstPress("Activate"))
        {
            //--Flag.
            mIsSwitching = false;

            //--Get the ability in the slot.
            FieldAbility *rFieldAbility = (FieldAbility *)mrFieldAbilityList->GetElementBySlot(mLoCursor);
            if(rFieldAbility)
            {
                //--Get the ability formerly in the slot.
                AdvCombat *rAdventureCombat = AdvCombat::Fetch();
                FieldAbility *rOldAbility = rAdventureCombat->GetFieldAbility(mHiCursor);

                //--Set this as the active ability.
                rAdventureCombat->SetFieldAbility(mHiCursor, rFieldAbility);

                //--If the ability is in another slot, clear it.
                for(int i = 0; i < cxAdv_FieldAbilitySlots; i ++)
                {
                    //--This is the slot we just replaced.
                    if(i == mHiCursor) continue;

                    //--Get the ability in the slot.
                    FieldAbility *rCheckAbility = rAdventureCombat->GetFieldAbility(i);

                    //--If it's the same as the one we just placed, replace it. This removes dupes and swaps slots.
                    if(rCheckAbility == rFieldAbility) rAdventureCombat->SetFieldAbility(i, rOldAbility);
                }
            }

            //--Audio.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Cancel, exit menu.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsSwitching = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    //--Finish.
    return true;
}
void AdvUIFieldAbilities::UpdateBackground()
{
    if(mVisibilityTimer > 0) mVisibilityTimer --;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIFieldAbilities::RenderPieces(float pVisAlpha)
{
    ///--[Documentation and Setup]
    if(!mImagesReady) return;

    ///--[Common]
    //--Compute visibility percentage.
    float cVisPercent = EasingFunction::QuadraticInOut(mVisibilityTimer, mVisibilityTimerMax);
    if(cVisPercent == 0.0) return;

    //--Darken the background.
    StarBitmap::DrawFullBlack(0.75f * cVisPercent);

    //--Images.
    StarBitmap *rEmptyFrame = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/AdventureUI/Abilities/OctFrameTeal");

    //--Render positions for the abilities.
    float cRenderWid = 175.0f;
    float cRenderLft = 245.0f;
    float cRenderTop = 185.0f;

    ///--[Header]
    //--Compute offsets.
    float cTopOffset = -500.0f * (1.0f - cVisPercent);
    glTranslatef(0.0f, cTopOffset, 0.0f);

    //--Backing.
    AdvImages.rHeader->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cVisPercent);
    AdvImages.rDoubleHeaderFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Field Abilities");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cVisPercent);

    //--Render the five abilities.
    AdvCombat *rCombat = AdvCombat::Fetch();
    for(int i = 0; i < cxAdv_FieldAbilitySlots; i ++)
    {
        //--Check.
        FieldAbility *rFieldAbility = rCombat->GetFieldAbility(i);
        if(rFieldAbility)
        {
            //--Get the display variables.
            StarBitmap *rBackingImg = rFieldAbility->GetBackingImage();
            StarBitmap *rFrameImg   = rFieldAbility->GetFrameImage();
            StarBitmap *rMainImg    = rFieldAbility->GetMainImage();

            //--Render.
            if(rBackingImg) rBackingImg->DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);
            if(rFrameImg)   rFrameImg->  DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);
            if(rMainImg)    rMainImg->   DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);

            //--Name.
            AdvImages.rMainFont->DrawText(cRenderLft + (cRenderWid * i) + (cRenderWid * 0.50f) - 12.0f, cRenderTop - 30.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rFieldAbility->GetDisplayName());
        }
        //--Render an empty frame if not occupied.
        else
        {
            if(rEmptyFrame) rEmptyFrame->DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);
        }
    }

    //--Help Strings.
    mConfirmString->DrawText(0.0f,  0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rMainFont);
    mCancelString-> DrawText(0.0f, 20.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rMainFont);
    mClearString->  DrawText(0.0f, 40.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rMainFont);

    //--Clean.
    glTranslatef(0.0f, -cTopOffset, 0.0f);

    ///--[Footer]
    //--Compute offsets.
    float cBotOffset = 500.0f * (1.0f - cVisPercent);
    glTranslatef(0.0f, cBotOffset, 0.0f);

    //--Backing.
    AdvImages.rFooter->Draw();

    //--Render ability list.
    int tAbiCursorX = 0;
    int tAbiCursorY = 0;
    float cAbiLft = 40.0f;
    float cAbiTop = 457.0f;
    float cAbiWid = 55.0f;
    FieldAbility *rAbility = (FieldAbility *)mrFieldAbilityList->PushIterator();
    while(rAbility)
    {
        //--Compute.
        float tX = cAbiLft + (cAbiWid * tAbiCursorX);
        float tY = cAbiTop + (cAbiWid * tAbiCursorY);

        //--Get the display variables.
        StarBitmap *rBackingImg = rAbility->GetBackingImage();
        StarBitmap *rFrameImg   = rAbility->GetFrameImage();
        StarBitmap *rMainImg    = rAbility->GetMainImage();

        //--Render.
        if(rBackingImg) rBackingImg->Draw(tX, tY);
        if(rFrameImg)   rFrameImg->  Draw(tX, tY);
        if(rMainImg)    rMainImg->   Draw(tX, tY);

        //--Next.
        tAbiCursorX ++;
        rAbility = (FieldAbility *)mrFieldAbilityList->AutoIterate();
    }

    //--Description.
    rAbility = rCombat->GetFieldAbility(mHiCursor);
    if(mIsSwitching) rAbility = (FieldAbility *)mrFieldAbilityList->GetElementBySlot(mLoCursor);
    if(rAbility)
    {
        //--Variables.
        float cTxtLft = 25.0f;
        float cTxtTop = 642.0f;
        float cTxtHei = 22.0f;

        //--Title.
        const char *rName = rAbility->GetDisplayName();
        AdvImages.rMainFont->DrawText(cTxtLft, cTxtTop, 0, 1.0f, rName);

        //--Lines.
        int tLines = rAbility->GetDisplayStringsTotal();
        for(int i = 0; i < tLines; i ++)
        {
            const char *rDisplayString = rAbility->GetDisplayString(i);
            AdvImages.rMainFont->DrawText(cTxtLft, cTxtTop + (cTxtHei * (i+1)), 0, 1.0f, rDisplayString);
        }
    }

    //--Clean.
    glTranslatef(0.0f, -cBotOffset, 0.0f);

    ///--[Highlight]
    glTranslatef(0.0f, cTopOffset, 0.0f);
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
    glTranslatef(0.0f, -cTopOffset, 0.0f);
    if(mIsSwitching)
    {
        glTranslatef(0.0f, cBotOffset, 0.0f);
        RenderExpandableHighlight(mLolightPos, mLolightSize, 4.0f, AdvImages.rOverlay_Highlight);
        glTranslatef(0.0f, -cBotOffset, 0.0f);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
