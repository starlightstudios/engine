///=============================== Peak Freaks Level Structures ===================================
//--Hazard structures used in Peak Freaks.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///--[Slider]
//--Hazard entity. Slides along a track, bumps the player if they're in its way.
typedef struct PF_Slider
{
    //--Current Position
    bool mTripleTimer;
    int mTimer;
    int mXPos;
    int mYPos;
    int mOldXPos;
    int mOldYPos;

    //--Tracks
    int mXStart;
    int mYStart;
    int mXEnd;
    int mYEnd;

    //--Bumping
    int mBumpDirX;
    int mBumpDirY;

    //--Rendering
    SugarBitmap *rRenderImg;

    //--Functions
    void Initialize();
    void Update();
    void Render();
    void RenderIceBeam(SugarBitmap *pImage);
}PF_Slider;

///--[Spinner]
//--Hazard entity. Spins around and bumps the player.
#define PF_SPINNER_SMALL_DIRS 8
#define PF_SPINNER_BIG_DIRS 4
typedef struct PF_Spinner
{
    //--Current position.
    int mTimer;
    int mXPos;
    int mYPos;

    //--Spinning.
    bool mIsBig;
    bool mIsClockwise;
    int mSpinPos;

    //--Rendering.
    SugarBitmap *rRenderImg[PF_SPINNER_SMALL_DIRS];

    //--Functions
    void Initialize();
    void Update();
    void UpdateBig();
    void Render();
    void RenderIceBeam(SugarBitmap *pImage);
}PF_Spinner;

///--[Waterfall]
//--Hazard entity. Drops water that bumps the player.
#define PF_WATERFALL_FRAMES 8
#define PF_WATERFALL_CYCLE_TICKS 120
#define PF_WATERFALL_SPREAD_TICKS 16
#define PF_WATERFALL_HOLD_TICKS 120
typedef struct PF_Waterfall
{
    //--Current position.
    int mXPos;
    int mYPos;

    //--Cycle
    int mSize;
    int mCycleTimer;
    int mSpreadTimer;
    int mSpreadCount;
    int mHoldTimer;

    //--Images
    SugarBitmap *rCycleImg[PF_WATERFALL_FRAMES];
    SugarBitmap *rWaterfallTop;
    SugarBitmap *rWaterfallMid;
    SugarBitmap *rWaterfallBot;

    //--Functions
    void Initialize();
    void Update();
    void Render();
}PF_Waterfall;

///--[Bomb Spike]
//--Hazard entity. Explodes when the player gets near, damages nearby tiles.
#define PF_BOMBSPIKE_EXPLOSION_FRAMES 3
typedef struct PF_BombSpike
{
    //--Current position.
    bool mSelfDestruct;
    bool mIsActivated;
    int mTimer;
    int mExplodeTicks;
    int mXPos;
    int mYPos;

    //--Images.
    SugarBitmap *rNeutralImg;
    SugarBitmap *rExplosions[PF_BOMBSPIKE_EXPLOSION_FRAMES];

    //--Functions
    void Initialize();
    void Update();
    void Render();
    void RenderIceBeam(SugarBitmap *pImage);
}PF_BombSpike;
