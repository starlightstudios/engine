//--Base
#include "AdvUICommon.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarPointerSeries.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
AdvUICommon::AdvUICommon()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Help Menu]
    mHelpControlOffset = cxAdvBigControlOffset;

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    mColorGrey.   SetRGBAF(0.70f, 0.70f, 0.70f, 1.0f);
    mColorHeading.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("HelpImages", &HelpImages, sizeof(HelpImages));
}
AdvUICommon::~AdvUICommon()
{
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void AdvUICommon::SetColor(const char *pColorName, float pAlpha)
{
    ///--[Documentation]
    //--Overridable function, takes in a name and sets the color mixer.
    if(!pColorName) return;

    //--Handle colors.
    if(!strcasecmp(pColorName, "Heading")) { StarlightColor::SetMixer(1.00f, 0.80f, 0.10f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Greyed"))  { StarlightColor::SetMixer(0.70f, 0.70f, 0.70f, pAlpha); return; }
    if(!strcasecmp(pColorName, "Clear"))   { StarlightColor::SetMixer(1.00f, 1.00f, 1.00f, pAlpha); return; }

    //--Error.
    fprintf(stderr, "AdvUICommon:SetColor() - Subtype %s. Failed to locate color %s.\n", mSubtype, pColorName);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUICommon::RenderComparisonCharacters(int pRearIndex, int pIndex, int pTimer, int pTimerMax, int pUIIndex)
{
    ///--[Documentation]
    //--Renders the comparison character on the left side of the UI. Also renders the swapping character
    //  if one currently exists.
    //--Passing -1 for the index will render nothing. This is not an error.
    if(pIndex < 0 && pRearIndex < 0) return;

    //--Fast-access pointers.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();

    //--Compute vis percentages.
    float cOldCharPct = EasingFunction::QuadraticInOut(pTimer, pTimerMax);
    float cCurCharPct = 1.0f - cOldCharPct;

    ///--[Rear Character]
    //--Previous comparison character. If present, renders behind the current one.
    if(pRearIndex != -1 && cOldCharPct < 1.0f)
    {
        //--Retrieve the character.
        AdvCombatEntity *rOldChar = rAdvCombat->GetActiveMemberI(pRearIndex);
        if(rOldChar)
        {
            //--Stencil and position.
            TwoDimensionRealPoint tRenderDim = rOldChar->GetUIRenderPosition(pUIIndex);

            //--Position.
            float cXPosition = tRenderDim.mXCenter + (-700.0f * cOldCharPct);
            float cYPosition = tRenderDim.mYCenter;

            //--Render.
            StarBitmap *rPortrait = rOldChar->GetCombatPortrait();
            if(rPortrait) rPortrait->Draw(cXPosition, cYPosition);
        }
    }

    ///--[Current Character]
    //--Current comparison character.
    if(pIndex != -1)
    {
        //--Get the old character. If they exist, render them.
        AdvCombatEntity *rCurChar = rAdvCombat->GetActiveMemberI(pIndex);
        if(rCurChar)
        {
            //--Stencil and position.
            TwoDimensionRealPoint tRenderDim = rCurChar->GetUIRenderPosition(pUIIndex);

            //--Position.
            float cXPosition = tRenderDim.mXCenter + (-700.0f * cCurCharPct);
            float cYPosition = tRenderDim.mYCenter;

            //--Render.
            StarBitmap *rPortrait = rCurChar->GetCombatPortrait();
            if(rPortrait) rPortrait->Draw(cXPosition, cYPosition);
        }
    }

}
void AdvUICommon::RenderGemSlotsLine(float pX, float pY, AdventureItem *pItem, StarBitmap *pGemSlotImg)
{
    ///--[Documentation]
    //--Several UI objects share the basic rendering code for showing gem slots. This function renders
    //  an item's gem slots in a single line, including the icons for the slotted gems.
    if(!pItem || !pGemSlotImg) return;

    //--Check gem slots. If there are none to render, stop.
    int tGemSlots = pItem->GetGemSlots();
    if(tGemSlots < 1) return;

    ///--[Render]
    //--Iterate.
    for(int p = 0; p < tGemSlots; p ++)
    {
        //--Get the gem.
        AdventureItem *rGem = pItem->GetGemInSlot(p);

        //--Render the gem image.
        if(rGem)
        {
            StarBitmap *rImage = rGem->GetIconImage();
            if(rImage) rImage->Draw(pX + (cxAdvGemIconW * p), pY);
        }

        //--In all cases, render the frame over the gem.
        pGemSlotImg->Draw(pX + (cxAdvGemIconW * p), pY);
    }
}
void AdvUICommon::RenderGemSlotsTwoLine(float pX, float pY, AdventureItem *pItem, StarBitmap *pGemSlotImg)
{
    ///--[Documentation]
    //--Same as above, but renders two sets of three gem slots.
    if(!pItem || !pGemSlotImg) return;

    //--Check gem slots. If there are none to render, stop.
    int tGemSlots = pItem->GetGemSlots();
    if(tGemSlots < 1) return;

    ///--[Top Row]
    for(int p = 0; p < 3; p ++)
    {
        //--Get the gem.
        AdventureItem *rGem = pItem->GetGemInSlot(p);

        //--Positions.
        float cXPos = pX + (cxAdvGemIconW * p);
        float cYPos = pY;

        //--Render the gem image.
        if(rGem)
        {
            StarBitmap *rImage = rGem->GetIconImage();
            if(rImage) rImage->Draw(cXPos, cYPos);
        }

        //--In all cases, render the frame over the gem.
        pGemSlotImg->Draw(cXPos, cYPos);
    }

    ///--[Bottom Row]
    for(int p = 3; p < tGemSlots; p ++)
    {
        //--Get the gem.
        AdventureItem *rGem = pItem->GetGemInSlot(p);

        //--Positions.
        float cXPos = pX + (cxAdvGemIconW * (p-3));
        float cYPos = pY + cxAdvGemIconW;

        //--Render the gem image.
        if(rGem)
        {
            StarBitmap *rImage = rGem->GetIconImage();
            if(rImage) rImage->Draw(cXPos, cYPos);
        }

        //--In all cases, render the frame over the gem.
        pGemSlotImg->Draw(cXPos, cYPos);
    }
}
void AdvUICommon::RenderGemSlotsSwitch(float pX, float pY, AdventureItem *pItem, StarBitmap *pGemSlotImg)
{
    ///--[Documentation]
    //--Routes between the above two calls depending on how many gem slots the item has. Note that this routine bottom-aligns the slots.
    //  This is intentional, the provided Y position should be the highest that the gems can render.
    if(!pItem || !pGemSlotImg) return;

    //--Three or fewer.
    int tGemSlots = pItem->GetGemSlots();
    if(tGemSlots <= 3)
    {
        RenderGemSlotsLine(pX, pY + cxAdvGemIconW, pItem, pGemSlotImg);
    }
    //--More than three.
    else
    {
        RenderGemSlotsTwoLine(pX, pY, pItem, pGemSlotImg);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
