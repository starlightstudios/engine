///==================================== PeakFreaksFragment ========================================
//--Fragment object used by the UI and level in Peak Freaks to add a bit of visual flair.

#pragma once

///========================================== Includes ============================================
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class PeakFreaksFragment
{
    private:
    ///--[System]
    int mLifetime;
    int mLifetimeMax;

    ///--[Position]
    float mX;
    float mY;
    float mXSpeed;
    float mYSpeed;

    ///--[Display]
    float mAngle;
    SugarBitmap *rUseBitmap;

    protected:

    public:
    //--System
    PeakFreaksFragment();
    ~PeakFreaksFragment();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool IsExpired();

    //--Manipulators
    void SetLifetime(int pTicks);
    void SetImage(SugarBitmap *pImage);
    void SetPosition(float pX, float pY);
    void SetSpeeds(float pX, float pY);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


