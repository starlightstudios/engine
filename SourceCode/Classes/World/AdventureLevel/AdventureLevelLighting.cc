//--Base
#include "AdventureLevel.h"

//--Classes
#include "RootEntity.h"
#include "AdventureLight.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "GlDfn.h"
#include "OpenGLMacros.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CameraManager.h"
#include "DisplayManager.h"
#include "DebugManager.h"
#include "EntityManager.h"

///--[Local Definitions]
#define LIGHT_POWER 20000.0f

///--[Debug]
//#define AL_LIGHTING_DEBUG
#ifdef AL_LIGHTING_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///===================================== Property Queries =========================================
bool AdventureLevel::IsLightingActive()
{
    return mAreLightsActive;
}

///======================================= Manipulators ===========================================
void AdventureLevel::ActivateLighting()
{
    mAreLightsActive = true;
    mReuploadAllLightData = true;
}
void AdventureLevel::DeactivateLighting()
{
    mAreLightsActive = false;
}
void AdventureLevel::SetAmbientLight(float pR, float pG, float pB, float pA)
{
    mAmbientLight = StarlightColor::MapRGBAF(pR, pG, pB, pA);
    mReuploadAllLightData = true;
}
void AdventureLevel::RegisterLight(AdventureLight *pLight)
{
    if(!pLight) return;
    mLightsList->AddElement(pLight->GetName(), pLight, &RootObject::DeleteThis);
    mReuploadAllLightData = true;
}
void AdventureLevel::ModifyLightColor(const char *pName, float pR, float pG, float pB, float pA)
{
    AdventureLight *rCheckLight = (AdventureLight *)mLightsList->GetElementByName(pName);
    if(rCheckLight)
    {
        rCheckLight->SetColor(pR, pB, pG, pA);
        mReuploadAllLightData = true;
    }
}
void AdventureLevel::EnableLight(const char *pName)
{
    AdventureLight *rCheckLight = (AdventureLight *)mLightsList->GetElementByName(pName);
    if(rCheckLight)
    {
        rCheckLight->Enable();
        mReuploadAllLightData = true;
    }
}
void AdventureLevel::DisableLight(const char *pName)
{
    AdventureLight *rCheckLight = (AdventureLight *)mLightsList->GetElementByName(pName);
    if(rCheckLight)
    {
        rCheckLight->Disable();
        mReuploadAllLightData = true;
    }
}
void AdventureLevel::AttachLightToEntity(const char *pLightName, const char *pEntityName)
{
    if(!pLightName || !pEntityName) return;
    AdventureLight *rLight = (AdventureLight *)mLightsList->GetElementByName(pLightName);
    RootEntity *rEntity = EntityManager::Fetch()->GetEntity(pEntityName);
    if(!rEntity || !rLight) return;
    rLight->AttachToEntity(rEntity->GetID());
    mReuploadAllLightData = true;
}

///====================================== Player Lighting =========================================
void AdventureLevel::ActivatePlayerLight()
{
    //--Flags.
    mPlayerHasLight = true;
    mPlayerLightIsActive = true;

    //--Light object creation.
    if(!mPlayerLightObject)
    {
        //--Compute current power.
        float tCurrentPower = 0.0f;
        if(mPlayerLightPowerMax > 0) tCurrentPower = (float)mPlayerLightPower / (float)mPlayerLightPowerMax;
        if(tCurrentPower < 0.15f) tCurrentPower = 0.15f;
        if(tCurrentPower > 1.00f) tCurrentPower = 1.00f;

        //--Increase light radius if the boosts are present.
        SysVar *rLightBoostA = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Global/Christine/iLightBoostA");
        SysVar *rLightBoostB = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Global/Christine/iLightBoostB");
        if(rLightBoostA && rLightBoostA->mNumeric == 1.0f) tCurrentPower = tCurrentPower + 2.50f;
        if(rLightBoostB && rLightBoostB->mNumeric == 1.0f) tCurrentPower = tCurrentPower + 2.50f;

        //--Create the light.
        mPlayerLightObject = new AdventureLight();
        mPlayerLightObject->AttachToEntity(mControlEntityID);
        mPlayerLightObject->SetRadial(LIGHT_POWER * tCurrentPower);
        mPlayerLightObject->SetColor(StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));
    }
    //--Just reattach the light if it changed owners.
    else
    {
        mPlayerLightObject->AttachToEntity(mControlEntityID);
    }

    //--Reupload lights.
    mReuploadAllLightData = true;
}
void AdventureLevel::DeactivatePlayerLight()
{
    mPlayerLightIsActive = false;
    mReuploadAllLightData = true;
}
void AdventureLevel::SetPlayerLightPower(int pTicksLeft)
{
    mPlayerLightPower = pTicksLeft;
}
void AdventureLevel::SetPlayerLightPowerMax(int pTicksLeft)
{
    mPlayerLightPowerMax = pTicksLeft;
}
void AdventureLevel::SetPlayerLightNoDrain(bool pFlag)
{
    mPlayerLightDoesNotDrain = pFlag;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void AdventureLevel::UpdateEntityLightPositions()
{
    //--Run through the lights and update their positions if they are attached to entities.
    AdventureLight *rLight = (AdventureLight *)mLightsList->PushIterator();
    while(rLight)
    {
        //--Check ID.
        uint32_t tAttachedEntityID = rLight->GetAttachedID();
        if(tAttachedEntityID)
        {
            //--Locate the entity.
            RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntityI(tAttachedEntityID);

            //--Entity must be a TileMapActor type.
            if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                TilemapActor *rActor = (TilemapActor *)rCheckEntity;
                rLight->SetPosition(rActor->GetWorldX(), rActor->GetWorldY());
            }
        }

        //--Next.
        rLight = (AdventureLight *)mLightsList->AutoIterate();
    }

    //--Same with the player light.
    if(mPlayerLightObject && mPlayerLightIsActive)
    {
        //--Decrement the power rating on the light. Each tick is 1 power unit. The light does
        //  not lose power if the world is stopped.
        if(!IsWorldStopped())
        {
            //--Increase light radius if the boosts are present.
            float tCurrentPower = 1.0f;
            SysVar *rLightBoostA = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Global/Christine/iLightBoostA");
            SysVar *rLightBoostB = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Global/Christine/iLightBoostB");
            if(rLightBoostA && rLightBoostA->mNumeric == 1.0f) tCurrentPower = tCurrentPower + 2.50f;
            if(rLightBoostB && rLightBoostB->mNumeric == 1.0f) tCurrentPower = tCurrentPower + 2.50f;

            //--Set.
            mPlayerLightObject->SetRadial(LIGHT_POWER * tCurrentPower);

            /*
            //--Decrement.
            mPlayerLightPower --;

            //--If this flag is set, the light is always set to max power.
            if(mPlayerLightDoesNotDrain)
            {
                mPlayerLightPower = mPlayerLightPowerMax;
                mPlayerLightObject->SetRadial(LIGHT_POWER * 1.0f);
            }
            //--Minimum power.
            else if(mPlayerLightPower < 1)
            {
                mPlayerLightPower = 0;
            }
            //--Set radius.
            else
            {
                //--Compute current power.
                float tCurrentPower = 0.0f;
                if(mPlayerLightPowerMax > 0) tCurrentPower = (float)mPlayerLightPower / (float)mPlayerLightPowerMax;
                if(tCurrentPower < 0.15f) tCurrentPower = 0.15f;
                if(tCurrentPower > 1.00f) tCurrentPower = 1.00f;

                //--Set.
                mPlayerLightObject->SetRadial(LIGHT_POWER * tCurrentPower);
            }*/
        }

        //--Check ID.
        uint32_t tAttachedEntityID = mPlayerLightObject->GetAttachedID();
        if(tAttachedEntityID)
        {
            //--Locate the entity.
            RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntityI(tAttachedEntityID);

            //--Entity must be a TileMapActor type.
            if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                TilemapActor *rActor = (TilemapActor *)rCheckEntity;
                mPlayerLightObject->SetPosition(rActor->GetWorldX() + 4.0f, rActor->GetWorldY() - 4.0f);
            }
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdventureLevel::RenderLights()
{
    ///--[Documentation and Setup]
    //--Uploads the lighting information to the shader. All entities will render using this light information.
    int tLightsTotal = 0;

    //--Activate the program. If it doesn't activate for any reason, stop.
    if(!DisplayManager::Fetch()->ActivateProgram("AdventureLevel Lighting")) return;

    //--Get the handle.
    GLint cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;

    //--Turn off the outliner.
    ShaderUniform1i(cShaderHandle, 0, "uShowColorOutline");

    //--Turn off fullbright mode. Entities will turn it on if they need it.
    ShaderUniform1i(cShaderHandle, 0, "uFullbrightEntity");

    //--Turn off the viewcone lighting and set the mixer to its defaults.
    ShaderUniform1i(cShaderHandle, 0, "uIsViewcone");

    //--Default. Don't use override properties. Tile layers change this as they render.
    ShaderUniform1i(cShaderHandle, 0, "uUseOverrideX");
    ShaderUniform1i(cShaderHandle, 0, "uUseOverrideY");

    //--Mixer usage is disabled by default.
    ShaderUniform1i(cShaderHandle, 0, "uUseMixer");

    //--Upload the camera's X and Y position, set by the scale.
    ShaderUniform1f(cShaderHandle, mCameraDimensions.mLft * mCurrentScale, "uCameraX");
    ShaderUniform1f(cShaderHandle, mCameraDimensions.mTop * mCurrentScale, "uCameraY");

    ///--[Light Data]
    //--Debug.
    #ifdef AL_LIGHTING_DEBUG
    bool tDebug = DebugManager::GetDebugFlag("Lights: All");
    #endif
    DebugPush(tDebug, "Uploading lights data.\n");
    DebugPrint("Total lights: %i\n", mLightsList->GetListSize());

    //--Diagnostics.
    mReuploadAllLightData = true;

    //--Iterate and upload. Lights will skip uploading if they didn't change values.
    int i = 0;
    AdventureLight *rLight = (AdventureLight *)mLightsList->PushIterator();
    while(rLight)
    {
        //--Upload.
        DebugPrint(" Uploading light data in slot %i, %p. Name: %s\n", i, rLight, mLightsList->GetIteratorName());
        rLight->HandleDataChecking(tLightsTotal, mCurrentScale, mCameraDimensions, mReuploadAllLightData);

        //--Next.
        i ++;
        rLight = (AdventureLight *)mLightsList->AutoIterate();
    }
    DebugPop("Finished uploading lights data.\n");
    DebugManager::SetDebugFlag("Lights: All", false);

    //--Toggle the flag off.
    mReuploadAllLightData = false;

    //--Upload player light if we have one. These always update since the player is usually moving
    //  or having their light power change.
    if(mPlayerLightObject && mPlayerLightIsActive) mPlayerLightObject->HandleDataChecking(tLightsTotal, mCurrentScale, mCameraDimensions, true);

    //--Upload how many lights there were. Can be zero.
    ShaderUniform1i(cShaderHandle, tLightsTotal, "uTotalLights");

    ///--[Standard Light Data]
    //--Check. If these variables got changed, we need to re-upload them.
    if(mLastUploadScaleX    != DisplayManager::xScaledOffsetX || mLastUploadScaleY    != DisplayManager::xScaledOffsetY ||
       mLastUploadViewportW != DisplayManager::xViewportW     || mLastUploadViewportH != DisplayManager::xViewportH)
    {
        mReuploadStandardLightData = true;
    }

    //--Only gets uploaded when the lights switch on or otherwise a refresh is needed.
    //  Slightly boosts rendering speed.
    if(mReuploadStandardLightData)
    {
        //--Clear.
        mReuploadStandardLightData = false;

        //--Set ambient values.
        float tUseAmbientR = mAmbientLight.r + xLightBoost;
        float tUseAmbientG = mAmbientLight.g + xLightBoost;
        float tUseAmbientB = mAmbientLight.b + xLightBoost;

        //--Ambient light values.
        ShaderUniform1f(cShaderHandle, tUseAmbientR, "uAmbientR");
        ShaderUniform1f(cShaderHandle, tUseAmbientG, "uAmbientG");
        ShaderUniform1f(cShaderHandle, tUseAmbientB, "uAmbientB");

        //--Standard sizes.
        ShaderUniform1f(cShaderHandle, VIRTUAL_CANVAS_X, "uVirtualCanvasX");
        ShaderUniform1f(cShaderHandle, VIRTUAL_CANVAS_Y, "uVirtualCanvasY");

        //--Provide Viewport values.
        ShaderUniform1f(cShaderHandle, DisplayManager::xScaledOffsetX, "uViewportOffsetX");
        ShaderUniform1f(cShaderHandle, DisplayManager::xScaledOffsetY, "uViewportOffsetY");
        ShaderUniform1f(cShaderHandle, DisplayManager::xViewportW,     "uViewportSizeX");
        ShaderUniform1f(cShaderHandle, DisplayManager::xViewportH,     "uViewportSizeY");

        //--Store these values. They can change when the player changes display modes and
        //  we will not be notified.
        mLastUploadScaleX    = DisplayManager::xScaledOffsetX;
        mLastUploadScaleY    = DisplayManager::xScaledOffsetY;
        mLastUploadViewportW = DisplayManager::xViewportW;
        mLastUploadViewportH = DisplayManager::xViewportH;

        //--Diagnostics.
        if(false)
        {
            fprintf(stderr, "Uploading standard light data.\n");
            fprintf(stderr, " Virtual canvas size: %i x %i\n", (int)VIRTUAL_CANVAS_X, (int)VIRTUAL_CANVAS_Y);
            fprintf(stderr, " Viewport size: %i x %i\n", (int)DisplayManager::xViewportW, (int)DisplayManager::xViewportH);
            fprintf(stderr, " Viewport offset: %i x %i\n", (int)DisplayManager::xScaledOffsetX, (int)DisplayManager::xScaledOffsetY);
        }
    }
}
