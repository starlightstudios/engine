//--Base
#include "CarnUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "CarnUIRegem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"

///--[Debug]
//#define CARNUIEQUIPSELECT_DEBUG
#ifdef CARNUIEQUIPSELECT_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Update ==============================================
void CarnUIEquip::UpdateSelection()
{
    ///--[Documentation]
    //--Updates selection mode. The user can switch characters, switch tabs, and select a piece of
    //  equipment to replace with another.
    ControlManager *rControlManager = ControlManager::Fetch();
    DebugPush(true, "CarnUIEquip:UpdateSelection() - Begin.\n");

    //--Make sure the active character exists.
    AdvCombatEntity *rAdvEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity)
    {
        DebugPop("Failed. No active character.\n");
        return;
    }

    ///--[Timers]
    ///--[Mouse Over]
    int tReply = CheckRepliesWithin(cxSelectBoxMin, cxSelectBoxMax);
    if(tReply != -1)
    {
        //--Set.
        int tOldCursor = mEquipmentCursor;
        mEquipmentCursor = (tReply - cxSelectBoxMin) + mReplacementSkip;

        //--SFX, description.
        if(tOldCursor != mEquipmentCursor && mEquipmentCursor != -1)
        {
            //--Locate the item in question, set description.
            AdventureItem *rReplacingItem = rAdvEntity->GetEquipmentBySlotI(mEquipmentCursor);
            if(rReplacingItem)
            {
                ResetString(mHighlightName, rReplacingItem->GetDisplayName());
                delete mDescription;
                mDescription = CreateDescriptionListFromString(rReplacingItem->GetDescription(), CarnImages.rFont_Description, 440.0f);
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Left Click / Activate]
    //--Selects the entry under the cursor.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Diagnostics.
        DebugPrint("Received left click.\n");

        //--Call.
        HandleSelectLeftClick();

        //--Diagnostics.
        DebugPop("Finished left click.\n");
        return;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Exits the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        FlagExit();
        SetCloseCode("Close Equipment");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        DebugPop("Finished right click.\n");
        return;
    }

    ///--[Finish Up]
    DebugPop("Finished with no inputs.\n");
}
void CarnUIEquip::HandleSelectLeftClick()
{
    ///--[Documentation]
    //--Does the bulk of the work of a left click. Returns out when it hits anything.
    AudioManager *rAudioManager = AudioManager::Fetch();

    //--Must be a character.
    AdvCombatEntity *rAdvEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity) return;

    //--Common functions. If a tab was clicked, switch modes.
    if(CommonTabClickUpdate())
    {
        //--If flagging exit, fire a close code.
        if(mFlagExit) SetCloseCode("Close Equipment");
        return;
    }

    //--Character cursor. If it changes, recheck proficiency data.
    int tOldCharacterCursor = mCharacterCursor;
    if(CommonCharClickUpdate(mCharacterCursor))
    {
        if(mCharacterCursor != tOldCharacterCursor) RecheckProficienciesForCharacter(mCharacterCursor);
        return;
    }

    ///--[Modify Item]
    //--Check the equipment cursor, and see if the click hit the modification hitbox.
    int tReply = CheckRepliesWithin(cxModMin, cxModMax);
    if(tReply != -1)
    {
        //--See if the hitbox click item has an item in the slot. If not, ignore it.
        int tModCursor = tReply - cxModMin;
        AdventureItem *rReplacingItem = rAdvEntity->GetEquipmentBySlotI(tModCursor);

        //--If there is an item, and it can be modded, activate modding mode.
        if(rReplacingItem && rReplacingItem->GetGemSlots() > 0)
        {
            //--Fetch the RegemUI. If it fails to resolve, just exit.
            CarnUIRegem *rRegemUI = CarnUIRegem::Fetch();
            if(!rRegemUI) { FlagExit(); return; }

            //--Order the UI to switch to the Regem UI.
            FlagExit();
            mCodeBackward = CARNMENU_MODE_REGEM + CARNMENU_CHANGEMODE_OFFSET;

            //--Tell the Regem UI to modify the selected item.
            rRegemUI->SetExitToEquipment(true);
            rRegemUI->SetConsideredItem(rReplacingItem);
            return;
        }
    }

    ///--[Select Item]
    //--Check hitbox reply packs to see if any of the equipment hitboxes were hit.
    tReply = CheckRepliesWithin(cxSelectBoxMin, cxSelectBoxMax);
    if(tReply != -1)
    {
        //--Set cursor.
        mEquipmentCursor = tReply - cxSelectBoxMin;

        //--Run subroutine to check if replacements mode can be activated. If it returns false, fail.
        if(!ActivateReplacement())
        {
            rAudioManager->PlaySound("Menu|Failed");
            return;
        }

        //--Success. Play a sound and recompute cursor position.
        RecomputeCursorPositions();
        rAudioManager->PlaySound("Menu|Select");
        mHitboxReplyList->PopIterator();
        return;
    }

    ///--[Toggle Description]
    tReply = CheckRepliesWithin(cxBtnToggleDesc, cxBtnToggleDesc);
    if(tReply != -1)
    {
        mIsDescriptionMode = !mIsDescriptionMode;
        rAudioManager->PlaySound("Menu|Select");
    }
}

///========================================== Drawing =============================================
void CarnUIEquip::RenderSelectMode()
{
    ///--[Documentation]
    //--Renders the status screen, showing which items the character has equipped and their stats.
    DebugPush(true, "CarnUIEquip:RenderSelectMode() - Begin.\n");
    if(mIsReplacing) { DebugPop("Not in selection mode. Stopping.\n"); return; }

    //--Frames.
    CarnImages.rFrame_Base->Draw();

    //--Resolve the acting entity.
    AdvCombat *rCombat = AdvCombat::Fetch();
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity) { DebugPop("No active entity found. Stopping.\n"); return; }

    ///--[Portrait]
    //if(rAdvEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
    //{
    //    CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rAdvEntity;
    //    rCarnationEntity->RenderSwitchableAt(196, 62);
    //}

    ///--[Name/Stats]
    //--Name.
    CarnImages.rFont_Name->DrawTextArgs(290.0f, 128.0f, 0, 1.0f, "%s", rAdvEntity->GetDisplayName());

    //--Level.
    CarnImages.rFont_Name->DrawTextArgs(561.0f, 125.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Lv. %i", rAdvEntity->GetLevel()+1);

    ///--[Equipment]
    //--Constants.
    float cEntryX = 285.0f;
    float cEntryY = 186.0f;
    float cEntryH =  37.0f;

    //--Weapon section. Fixed label followed by the weapon and ammo slot.
    //RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  0), "HeadingWeapon");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  0), "Weapon");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  1), "Ammo");
    //RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  3), "HeadingArmor");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  2), "Head");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  3), "Body");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  4), "Gloves");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  5), "Legs");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  6), "Boots");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  7), "Ring");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  8), "Amulet");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH *  9), "Pact");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH * 10), "Covenant");
    RenderEquipmentSlot(cEntryX, cEntryY + (cEntryH * 11), "Rune");

    //--If the selection cursor is not -1, render the modification button next to its entry.
    if(mEquipmentCursor != -1)
    {
        //--There must be an item in the slot. If there is not, the resolving function will return NULL.
        AdventureItem *rReplacingItem = rAdvEntity->GetEquipmentBySlotI(mEquipmentCursor);

        //--Get associated hitbox. Use the modification hitbox and shift over a bit. If the image failed
        //  to resolve, render nothing.
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxModMin + mEquipmentCursor);
        if(rReplacingItem && rHitbox)
        {
            CarnImages.rFrame_Modify->Draw(rHitbox->mLft, rHitbox->mTop);
        }
    }

    ///--[Proficiency Handling]
    //--Proficiency data is stored in the CarnUIVendor interface. It is only needed for display purposes,
    //  proficiency is not used for stat computations here. An icon is displayed next to a weapon if
    //  equipment has a proficiency tag.
    for(int i = 0; i < CARNUIEQP_PROFICIENCY_SLOTS; i ++)
    {
        //--Get the image in the matching slot.
        StarBitmap *rProficiencyImg = rStoredProficiencyImages[i];

        //--Get associated hitbox. Use the modification hitbox and shift over a bit. If the image failed
        //  to resolve, render nothing.
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxModMin + i);
        if(rProficiencyImg && rHitbox)
        {
            rProficiencyImg->Draw(rHitbox->mLft - 32.0f, rHitbox->mTop);
        }
    }

    ///--[Description]
    //--Render if it exists.
    if(mDescription && mIsDescriptionMode && mHighlightName)
    {
        //--Constants.
        float cTitleX = 709.0f;
        float cTitleY = 101.0f;
        float cDescX  = 709.0f;
        float cDescY  = 147.0f;
        float cDescVSpacing = 30.0f;

        //--Item name.
        CarnImages.rFont_Header->DrawText(cTitleX, cTitleY, 0, 1.0f, mHighlightName);

        //--Subroutine.
        RenderDescription(cDescX, cDescY, cDescVSpacing, mDescription, CarnImages.rFont_Description);
    }
    ///--[Statistics]
    else if(!mIsDescriptionMode)
    {
        //--Backings.
        CarnImages.rOverlay_StatBase->Draw();
        CarnImages.rOverlay_StatDetail->Draw();

        //--Fixed labels.
        AdvImages.rFont_Heading->DrawText(935.0f, 134.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Statistics");

        //--Constants.
        float cTop = 200.0f;
        float cHei = 39.0f;

        //--Variables.
        RenderStatistic(cTop + (cHei *  0.0f),    "HP",         STATS_HPMAX);
        RenderStatistic(cTop + (cHei *  1.0f),    "SP",         STATS_MPMAX);
        RenderStatistic(cTop + (cHei *  2.0f),    "Attack",     STATS_ATTACK);
        RenderStatistic(cTop + (cHei *  3.0f),    "M. Atk.",    STATS_CPMAX); //Override in Carnation
        RenderStatistic(cTop + (cHei *  4.0f)- 3, "Defense",    STATS_RESIST_PROTECTION);
        RenderStatistic(cTop + (cHei *  5.0f)- 3, "M. Def.",    STATS_ACCURACY); //Override in Carnation
        RenderStatistic(cTop + (cHei *  6.0f)- 3, "Initiative", STATS_INITIATIVE);
        RenderStatistic(cTop + (cHei *  7.0f)- 4, "Strength",   STATS_RESIST_OBSCURE); //Override in Carnation
        RenderStatistic(cTop + (cHei *  8.0f)- 5, "Intellect",  STATS_FREEACTIONGEN); //Override in Carnation
        RenderStatistic(cTop + (cHei *  9.0f)- 6, "Endurance",  STATS_FREEACTIONMAX); //Override in Carnation
        RenderStatistic(cTop + (cHei * 10.0f)- 5, "Luck",       STATS_EVADE); //Override in Carnation
    }

    ///--[Toggle Button]
    TwoDimensionReal *rToggleHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("ToggleDesc");
    if(rToggleHitbox)
    {
        CarnImages.rFrame_Button->Draw(rToggleHitbox->mLft, rToggleHitbox->mTop);
        CarnImages.rFont_Statistics->DrawText(rToggleHitbox->mXCenter, rToggleHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Toggle Stats");
    }

    ///--[Finish Up]
    DebugPop("Finished normally.\n");
}
void CarnUIEquip::RenderEquipmentSlot(float pX, float pY, const char *pSlotName)
{
    ///--[Documentation]
    //--Given a name of an equipment slot, renders that equipment slot's item, or handles rendering
    //  empty cases if the item is not found.

    ///--[Normal Cases]
    //--Locate the active character. If the entity failed to resolve, treat as if the slot is empty.
    AdvCombat *rCombat = AdvCombat::Fetch();
    AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor);
    if(!rAdvEntity)
    {
        char tBuf[64];
        sprintf(tBuf, "(%s)", pSlotName);
        RenderEquipmentEntry(pX, pY, tBuf, NULL);
        return;
    }

    //--Resolve item.
    AdventureItem *rEquipment = rAdvEntity->GetEquipmentBySlotS(pSlotName);

    //--Item exists, render it.
    if(rEquipment)
    {
        //--If the item has no gems, just show its name.
        if(rEquipment->GetSlottedGems() < 1)
        {
            RenderEquipmentEntry(pX, pY, rEquipment->GetName(), rEquipment->GetIconImage());
        }
        //--Otherwise, show a count of how many gems are in it.
        else
        {
            char tBuf[128];
            sprintf(tBuf, "%s (+%i)", rEquipment->GetName(), rEquipment->GetSlottedGems());
            RenderEquipmentEntry(pX, pY, tBuf, rEquipment->GetIconImage());
        }
    }
    //--Item does not exist, render the slot name.
    else
    {
        char tBuf[64];
        sprintf(tBuf, "(%s)", pSlotName);
        RenderEquipmentEntry(pX, pY, tBuf, NULL);
    }
}
void CarnUIEquip::RenderEquipmentEntry(float pX, float pY, const char *pString, StarBitmap *pIcon)
{
    ///--[Documentation]
    //--Renders the provided string and a detail indicator. Icon is optional.
    if(!pString) return;

    //--Constants.
    float cItemIndent  = -5.0f;
    float cOverlayOffX =-23.0f;
    float cOverlayOffY = 23.0f;

    //--Icon.
    if(pIcon) pIcon->Draw(pX+cItemIndent, pY+4.0f);

    //--Render.
    AdvImages.rFont_Statistics->DrawText(pX+cItemIndent+26.0f, pY, 0, 1.0f, pString);
    CarnImages.rOverlay_DetailItem->Draw(pX+cOverlayOffX, pY+cOverlayOffY);
}
