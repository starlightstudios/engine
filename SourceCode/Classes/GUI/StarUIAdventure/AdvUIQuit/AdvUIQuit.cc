//--Base
#include "AdvUIQuit.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "MapManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
AdvUIQuit::AdvUIQuit()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AQIT");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIQuit ======= ]
    ///--[Variables]
    //--System
    mCursor = ADVMENU_QUIT_CURSOR_TOTITLE;

    //--Quit Handling
    mIsQuitting = false;
    mQuitFadeTimer = 0;

    //--Cursor
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIQuit::~AdvUIQuit()
{
}
void AdvUIQuit::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Quit UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI", &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIQuit::TakeForeground()
{
    ///--[Variables]
    //--Local
    mCursor = ADVMENU_QUIT_CURSOR_TOTITLE;

    ///--[Construction]
    //--Set cursor to 0th position.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}

///======================================= Core Methods ===========================================
void AdvUIQuit::RefreshMenuHelp()
{

}
void AdvUIQuit::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--There are only two positions for the cursor here so this is pretty easy to manage.
    if(mCursor == ADVMENU_QUIT_CURSOR_TOTITLE)
    {
        mHighlightPos.MoveTo( 449.0f, 253.0f, ADVMENU_QUIT_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(173.0f,  51.0f, ADVMENU_QUIT_CURSOR_MOVE_TICKS);
    }
    else
    {
        mHighlightPos.MoveTo( 698.0f, 253.0f, ADVMENU_QUIT_CURSOR_MOVE_TICKS);
        mHighlightSize.MoveTo(266.0f,  51.0f, ADVMENU_QUIT_CURSOR_MOVE_TICKS);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIQuit::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles timers and player input for the quit submenu. If this submenu handled input, returns
    //  true, otherwise returns false.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Quit Fadeout]
    //--When in quit mode, fade to black, then quit or return to the title. This overrides all other
    //  actions and controls.
    if(mIsQuitting)
    {
        //--Timer.
        mQuitFadeTimer ++;

        //--Ending.
        if(mQuitFadeTimer >= ADVMENU_QUIT_FINAL_TICKS)
        {
            //--Ending: Quit to the title screen.
            if(mCursor == ADVMENU_QUIT_CURSOR_TOTITLE)
            {
                MapManager::Fetch()->mBackToTitle = true;
            }
            //--Ending: Quit the program.
            else
            {
                Global::Shared()->gQuit = true;
            }
        }
        return true;
    }

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < ADVMENU_QUIT_VIS_TICKS) mVisibilityTimer ++;

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Control Handling]
    //--This object has no sub-modes and very little control handling.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Left.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Set.
        if(mCursor != ADVMENU_QUIT_CURSOR_TOTITLE)
        {
            mCursor = ADVMENU_QUIT_CURSOR_TOTITLE;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Right.
    else if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Set.
        if(mCursor != ADVMENU_QUIT_CURSOR_EXITPROGRAM)
        {
            mCursor = ADVMENU_QUIT_CURSOR_EXITPROGRAM;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Activate. Quits the game or returns to the title.
    if(rControlManager->IsFirstPress("Activate"))
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        mIsQuitting = true;
        return true;
    }

    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}
void AdvUIQuit::UpdateBackground()
{
    if(mVisibilityTimer > 0) mVisibilityTimer --;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIQuit::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    //--Option. Selects between fading and sliding.
    bool tUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Timer]
    //--Resolve timer offset.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)ADVMENU_QUIT_VIS_TICKS);

    ///--[Setup]
    //--If the UI is using translate logic, set the alpha to 1.0f.
    if(!tUseFade) cAlpha = 1.0f;

    //--Reset mixer.
    StarlightColor::ClearMixer();

    ///============================= Left Block ===============================
    ///============================= Top Block ================================
    //--Offset.
    //HandleFadeOrTranslate(tUseFade, 0.0f, cTopOffset * 1.0f, cAlpha);
    AutoPieceOpen(DIR_UP, cAlpha);

    //--Backing.
    AdvImages.rBacking->Draw();
    AdvImages.rBackToTitleButton->Draw();
    AdvImages.rExitProgramButton->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cAlpha);
    AdvImages.rHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 176.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Quit the game?");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Back-to-Title
    AdvImages.rHeadingFont->DrawText(458.0f + 77.0f, 253.0f + 7.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "To Title");

    //--Exit Program
    AdvImages.rHeadingFont->DrawText(754.0f + 77.0f, 253.0f + 7.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Exit Program");

    //--Highlight.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Clean.
    //HandleFadeOrTranslate(tUseFade, 0.0f, cTopOffset * -1.0f, cAlpha);
    AutoPieceClose();

    ///============================ Right Block ===============================
    ///============================ Bottom Block ==============================
    ///============================ No Blocking ===============================
    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();

    ///=============================== Finale =================================
    if(mQuitFadeTimer > 0)
    {
        //--Fade percent.
        float cFadeAlpha = EasingFunction::QuadraticInOut(mQuitFadeTimer, ADVMENU_QUIT_FINAL_TICKS);
        StarBitmap::DrawFullBlack(cFadeAlpha);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
