//--Base
#include "TimerEvent.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CutsceneManager.h"
#include "DebugManager.h"

///========================================== System ==============================================
TimerEvent::TimerEvent()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_EVENT_TIMER;

    //--[RootEvent]
    //--System
    //--Script Execution
    //--Data Storage

    //--[TimerEvent]
    //--System
    //--Storage
    mTicksCanRun = 1;
    mTimer = 1;
    mTimerMax = 1;
    mGlobalTick = 0;
}
TimerEvent::~TimerEvent()
{
}

///--[Public Variables]
//--If true, the event always runs. Used for accelerating or scattering scripts.
bool TimerEvent::xAlwaysRunEvent = false;

///===================================== Property Queries =========================================
bool TimerEvent::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_EVENT_ROOT) return true;
    if(pType == POINTER_TYPE_EVENT_TIMER) return true;
    return false;
}
int TimerEvent::GetTicks()
{
    return mTimer;
}
int TimerEvent::GetTicksMax()
{
    return mTimerMax;
}

///======================================== Manipulators ==========================================
void TimerEvent::SetTicks(int pTicks)
{
    mTimer = pTicks;
}
void TimerEvent::SetTicks(int pTicks, int pTicksMax)
{
    mTimer = pTicks;
    mTimerMax = pTicksMax;
    if(mTimerMax < 1) mTimerMax = 1;
}

///======================================== Core Methods ==========================================
bool TimerEvent::IsComplete()
{
    //--Returns whether or not the timer has expired.
    return (mTimer < 1);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void TimerEvent::Update()
{
    //--Check the last global tick this event ran.
    if(Global::Shared()->gTicksElapsed == mGlobalTick && !xAlwaysRunEvent) return;

    //--Run timer, save the global tick.
    mTimer -= CutsceneManager::xTimerUpdatesThisTick;
    mGlobalTick = Global::Shared()->gTicksElapsed;
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void TimerEvent::HandleDiagnostics()
{
    //--Prints diagnostics information to the visual renderer in the DebugManager.
    DebugManager::RenderVisualTrace("TIMER EV %i %i", mTimer, mTimerMax);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void TimerEvent::HookToLuaState(lua_State *pLuaState)
{
    /* TimeEvent_SetProperty("Timer", iTimer)
       TimeEvent_SetProperty("Timer", iTimer, iTimerMax)
       Sets the given property in the active RootEvent or derived type. */
    lua_register(pLuaState, "TimeEvent_SetProperty", &Hook_TimeEvent_SetProperty);

    /* TimeEvent_GetProperty("Timer") (1 integer)
       Returns the requested property from the active RootEvent or derived type. */
    lua_register(pLuaState, "TimeEvent_GetProperty", &Hook_TimeEvent_GetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_TimeEvent_SetProperty(lua_State *L)
{
    //TimeEvent_SetProperty("Timer", iTimer)
    //TimeEvent_SetProperty("Timer", iTimer, iTimerMax)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TimeEvent_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Instantiated Cases]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    TimerEvent *rEvent = (TimerEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_TIMER)) return LuaTypeError("TimeEvent_SetProperty", L);

    //--Number of ticks to count down. Can't be used for percentages.
    if(!strcasecmp(rSwitchType, "Timer") && tArgs == 2)
    {
        rEvent->SetTicks(lua_tointeger(L, 2));
    }
    //--Ticks along with maximum ticks for percentage reasons.
    else if(!strcasecmp(rSwitchType, "Timer") && tArgs == 3)
    {
        rEvent->SetTicks(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Error.
    else
    {
        LuaPropertyError("TimeEvent_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_TimeEvent_GetProperty(lua_State *L)
{
    //TimeEvent_GetProperty("Timer") (1 integer)
    //TimeEvent_GetProperty("Timer Max") (1 integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TimeEvent_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    TimerEvent *rEvent = (TimerEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_TIMER)) return LuaTypeError("TimeEvent_GetProperty", L);

    //--Timer, obviously.
    if(!strcasecmp(rSwitchType, "Timer") && tArgs == 1)
    {
        lua_pushinteger(L, rEvent->GetTicks());
        tReturns = 1;
    }
    //--Max timer.
    else if(!strcasecmp(rSwitchType, "Timer Max") && tArgs == 1)
    {
        lua_pushinteger(L, rEvent->GetTicksMax());
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("TimeEvent_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
