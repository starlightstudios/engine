///======================================= MemoryManager ==========================================
//--Static manager which handles memory allocation and deallocation. By setting defines in this file, you can modify
//  how the program uses this manager. By default, the manager only has garbage collection abilities, which are optional.
//  You must set the MEMORY_HANDLER flag to something in order to use the rest of the manager.
//--The MEMORY_NOTHING flag will override the allocation functions be allegories of free(), starmemoryalloc(), and so on.
//--The MEMORY_OVERRIDEALLOCATE flag will use this manager in lieu of the starmemoryalloc() series of functions. This
//  allows you to override them and add debug text if you so desire.
//--The MEMORY_DEBUGMODE flag will print text when the garbage collector dumps things out, which can help to track
//  leaks.
//--To make life a little easier, Memory:: has been def'd to Memory::

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"

///===================================== Local Definitions ========================================
///--[Forward Declarations]
class StarLinkedList;
class MemoryManager;
typedef MemoryManager Memory;

///--[Typedefs]
#ifndef _TypedefFunctionPtr_
#define _TypedefFunctionPtr_
class BaseEffect;
class StarBitmap;
class FlexMenu;

//--General Purpose
typedef void(*DeletionFunctionPtr)(void *);
typedef void(*LogicFnPtr)(void *);
typedef void(*UpdateFnPtr)(void *);
typedef void(*DrawFnPtr)(void *);

//--Gears and Drives
struct MainPackage;
typedef void(*EventHandlerFnPtr)(MainPackage &);
typedef void(*LogicHandlerFnPtr)(MainPackage &);
typedef void(*RenderHandlerFnPtr)(MainPackage &);

//--Class Specific
typedef StarBitmap*(*AnimationResolveFuncPtr)(void *, int&, int&);

//--Creation Switches
typedef FlexMenu*(*TitleMenuCreateFuncPtr)(void);

#endif

///--[Other Definitions]
#define MEMORY_NOTHING 0
#define MEMORY_OVERRIDEALLOCATE 1

#define MEMORY_HANDLER MEMORY_NOTHING
//#define MEMORY_DEBUG

#define TICKS_PER_SECOND 60

///===================================== Local Structures =========================================
typedef struct GarbageEntry
{
    //--Ownership.
    void *mGarbagePtr; //Owned by this structure! Liberate from whatever else owns it.
    DeletionFunctionPtr rDeletionFunction;

    //--Flags for deletion.
    int mTicksOnGarbageList;

    //--Static Deletion Function.
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        GarbageEntry *rGarbageEntry = (GarbageEntry *)pPtr;
        rGarbageEntry->DeleteObject();
        free(rGarbageEntry);
    }

    //--Functions
    void DeleteObject()
    {
        if(!rDeletionFunction || !mGarbagePtr) return;
        rDeletionFunction(mGarbagePtr);
        mGarbagePtr = NULL;
        rDeletionFunction = NULL;
    }
}GarbageEntry;

///========================================== Classes =============================================
class MemoryManager
{
    private:
    //--System
    MemoryManager();
    ~MemoryManager();
    static void DumpAllMemory();

    //--Types of Deletion Cases
    static bool xAlwaysRequireManualPurging;
    static int xMaxEntriesBeforeAutoPurge;
    static int xMaxTicksBeforeDeletion;

    //--Garbage Handling
    static bool xIsFlaggedForFlush;
    static int xGarbageTimer;
    static int xGarbageCheckTicks;
    static StarLinkedList *xGarbageList;

    protected:

    public:

    //--Public Variables
    static uint32_t xPageSize;

    //--Property Queries
    static int GetGarbageObjectsTotal();

    //--Manipulators
    static void AddGarbage(void *pGarbagePtr, DeletionFunctionPtr pDeletionPtr);
    static void AddGarbage(void *pGarbagePtr, DeletionFunctionPtr pDeletionPtr, bool pRequiresManualDeletion);
    static void ManualFlush();

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    static void Tick();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

