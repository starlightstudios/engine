//--Base
#include "CarnLevelEvent.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
CarnLevelEvent::CarnLevelEvent()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= CarnLevelEvent ======= ]
    ///--[System]
    mTimer = CARN_NOTIFICATION_STAY_ONSCREEN_TICKS;
    mRemoveNextTick = false;

    ///--[Position]
    mDespawnAtEndOfMovement = false;
    mHEd = 28.0f;
    mVEd = 13.0f;
    mWid = 10.0f;
    mHei = 10.0f;
    mPosition.MoveTo(0.0f, 0.0f, 0);

    ///--[Text]
    mText = NULL;

    ///--[Images]
    mImagesAreReady = false;
    rFont = NULL;
    rBacking = NULL;

    ///--[ ================ Construction ================ ]
    ///--[Images]
    //--Use default font and backing. These can be changed.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rFont = rDataLibrary->GetFont("Carn World Notifications");
    rBacking = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_Notification");

    //--Get default sizes from the backing.
    if(rBacking)
    {
        mWid = rBacking->GetWidth();
        mHei = rBacking->GetHeight();
    }

    ///--[Verify]
    mImagesAreReady = (rFont && rBacking);
}
CarnLevelEvent::~CarnLevelEvent()
{
    free(mText);
}
void CarnLevelEvent::DeleteThis(void *pPtr)
{
    //--Error check.
    if(!pPtr) return;

    //--Cast, deallocate.
    CarnLevelEvent *rPtr = (CarnLevelEvent *)pPtr;
    delete rPtr;
}

///===================================== Property Queries =========================================
bool CarnLevelEvent::ShouldDespawn()
{
    return mRemoveNextTick;
}
bool CarnLevelEvent::WillDespawnWhenDoneMoving()
{
    return mDespawnAtEndOfMovement;
}
float CarnLevelEvent::GetXPosition()
{
    return mPosition.mXEnd;
}
float CarnLevelEvent::GetYPosition()
{
    return mPosition.mYEnd;
}
float CarnLevelEvent::GetWidth()
{
    return mWid;
}
float CarnLevelEvent::GetHeight()
{
    return mHei;
}
bool CarnLevelEvent::IsPointWithin(float pX, float pY)
{
    if(pX < mPosition.mXCur) return false;
    if(pY < mPosition.mYCur) return false;
    if(pX > mPosition.mXCur + mWid) return false;
    if(pY > mPosition.mYCur + mHei) return false;
    return true;
}

///======================================= Manipulators ===========================================
void CarnLevelEvent::SetText(const char *pText)
{
    ResetString(mText, pText);
}
void CarnLevelEvent::MoveTo(float pX, float pY, int pTicks)
{
    mPosition.MoveTo(pX, pY, pTicks);
}
void CarnLevelEvent::MarkDespawnAfterMoving()
{
    mDespawnAtEndOfMovement = true;
}
void CarnLevelEvent::SetFont(const char *pFontName)
{
    rFont = DataLibrary::Fetch()->GetFont(pFontName);
    mImagesAreReady = (rFont && rBacking);
}
void CarnLevelEvent::SetBacking(const char *pDLPath)
{
    rBacking = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mImagesAreReady = (rFont && rBacking);
}

///======================================= Core Methods ===========================================
void CarnLevelEvent::RecomputeSizes()
{
    ///--[Documentation]
    //--The frame for a CarnLevelEvent is meant to be adaptable and resize as necessary. To this end
    //  it is possible to recompute the size of the object. This must be done after the images are
    //  resolved because their sizes are needed.
    if(!mImagesAreReady || !mText) return;

    //--Get the width and height of the backing, these are the minimum sizes. Rendering looks really
    //  bad if you try to render a frame smaller than its smallest size.
    float tLowWid = rBacking->GetWidth();
    float tLowHei = rBacking->GetHeight();

    //--Get the length of the text.
    float cTextWid = rFont->GetTextWidth(mText);

    //--Add the borders to the text width to get the needed space.
    float tNeededSpace = cTextWid + mHEd + mHEd;
    mWid = tNeededSpace;

    //--Clamp.
    if(mWid < tLowWid) mWid = tLowWid;
    mHei = tLowHei;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void CarnLevelEvent::Update()
{
    ///--[Documentation]
    //--Updates object timers and flags for removal if needed.
    mPosition.Increment(EASING_CODE_QUADINOUT);
    if(mPosition.mTimer >= mPosition.mTimerMax && mDespawnAtEndOfMovement) mRemoveNextTick = true;

    //--Slide offscreen when the timer hits zero. This does not occur if the object is already moving.
    if(mPosition.mTimer < mPosition.mTimerMax) return;
    if(mTimer > 0 && mPosition.mTimer >= mPosition.mTimerMax)
    {
        mTimer --;
    }
    else
    {
        MoveTo(mWid * -1.0f, mPosition.mYCur, 15);
        MarkDespawnAfterMoving();
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnLevelEvent::Render()
{
    ///--[Documentation]
    //--Renders the object, assuming the images are ready.
    if(!mImagesAreReady) return;

    //--Setup.
    glTranslatef(mPosition.mXCur, mPosition.mYCur, 0.0f);

    ///--[Backing]
    //--Get sizes.
    float cPixelWid = rBacking->GetWidth();
    float cPixelHei = rBacking->GetHeight();

    //--Rendering texture positions.
    float cTxHEdge = mHEd / cPixelWid;
    float cTxVEdge = mVEd / cPixelHei;
    float cTxLEdge = cTxHEdge;
    float cTxREdge = 1.0f - cTxHEdge;
    float cTxTEdge = cTxVEdge;
    float cTxBEdge = 1.0f - cTxVEdge;

    //--Bind, render.
    rBacking->RenderNine(0.0f, mHEd, mWid-mHEd, mWid, 0.0f, mVEd, mHei-mVEd, mHei, cTxLEdge, cTxREdge, cTxTEdge, cTxBEdge);

    ///--[Text]
    if(mText) rFont->DrawText(26.0f, 16.0f, 0, 1.0f, mText);

    ///--[Clean]
    glTranslatef(mPosition.mXCur * -1.0f, mPosition.mYCur * -1.0f, 0.0f);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
