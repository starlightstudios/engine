-- |[ ==================================== Autobuilder Menu ==================================== ]|
--Scans for autobuilder profiles. If none exist, does nothing. Otherwise, switches the menu to
-- display a list of autobuilders and executes the autobuilder selected.
--There are two parts to this. If the firing code is 0, then we are building a listing of games.
-- If the firing code is nonzero, then we are building a listing of builds for the game index -1.
-- The firing code will be the game index + 1.
local iArgCount = LM_GetNumOfArgs()
if(iArgCount < 1) then return end

--Arg resolve.
local iFiringCode = LM_GetScriptArgument(0, "N")

-- |[ ======================================= Game Menu ======================================== ]|
--Shows a list of games.
if(iFiringCode == 0) then

    -- |[Check Build List Size]|
    --This call rebuilds the list and counts how many are on it. If it comes back 0, no autobuild
    -- profiles were found. Note that the first "layer" of builds is the game listing.
    local iAutobuildProfilesTotal = BP_GetProperty("Count Builds")
    if(iAutobuildProfilesTotal < 1) then return end

    -- |[Clear/Reset Menu]|
    --At this point, we're certain that the menu needs to be rebuilt.
    MapM_PushMenuStackHead()
    FlexMenu_Clear()

    --Header
    FlexMenu_SetHeaderSize(1)
    FlexMenu_SetHeaderLine(0, "Autobuild Menu")

    -- |[Build Game Listing]|
    for i = 0, iAutobuildProfilesTotal-1, 1 do
        local sGameName = BP_GetProperty("Game Name In Slot", i)
        FlexMenu_RegisterButton(sGameName)
            StarButton_SetProperty("Priority", i)
            StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/MainMenu/192 Autobuilder.lua", i+1)
        DL_PopActiveObject()
    end

    -- |[Back Button]|
    --Back to the main menu.
    FlexMenu_RegisterButton("Back")
        StarButton_SetProperty("Priority", 10000)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/MainMenu/192 Autobuilder.lua", -1)
    DL_PopActiveObject()
        
    -- |[Clean]|
    DL_PopActiveObject()
    
-- |[ ======================================= Build Menu ======================================= ]|
--Shows a list of builds for the provided game.
elseif(iFiringCode > 0 and iFiringCode < 100) then

    -- |[Check Build List]|
    --Get the game index.
    local iGameIndex = iFiringCode - 1

    --Get how many builds this game has.
    local iBuilds = BP_GetProperty("Build Count In Slot", iGameIndex)

    --If the count is zero, stop.
    if(iBuilds < 1) then return end

    -- |[Clear/Reset Menu]|
    --At this point, we're certain that the menu needs to be rebuilt.
    MapM_PushMenuStackHead()
    FlexMenu_Clear()

    -- |[Build Listing]|
    for i = 0, iBuilds-1, 1 do
        local sBuildName = BP_GetProperty("Build Name In Slot", iGameIndex, i)
        FlexMenu_RegisterButton(sBuildName)
            StarButton_SetProperty("Priority", i)
            StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/MainMenu/192 Autobuilder.lua", iFiringCode + (i+1) * 100)
        DL_PopActiveObject()
    end

    -- |[Back Button]|
    --Calls this script again, but with a 0 firing code.
    FlexMenu_RegisterButton("Back")
        StarButton_SetProperty("Priority", 10000)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/MainMenu/192 Autobuilder.lua", 0)
    DL_PopActiveObject()
        
    -- |[Clean]|
    DL_PopActiveObject()

-- |[ ===================================== Execute Build ====================================== ]|
--Shows a list of builds for the provided game.
elseif(iFiringCode > 100) then

    --The firing code, modulus 100, is the game index + 1.
    local iGameIndex = math.floor((iFiringCode % 100) - 1)
    io.write("Game index: " .. iGameIndex .. "\n")
    
    --The firing code, divided by 100, is the build index + 1.
    local iBuildIndex = math.floor(((iFiringCode - (iFiringCode % 100)) / 100) - 1)
    io.write("Build index: " .. iBuildIndex .. "\n")

    --Execute.
    BP_SetProperty("Execute Package", iGameIndex, iBuildIndex)
    
    --Play a sound effect.
    AudioManager_PlaySound("Menu|Save")
    
    --Close back to the main menu when done.
    MapM_PushMenuStackHead()
        FlexMenu_Clear()
        LM_ExecuteScript("Data/Scripts/MainMenu/000 PopulateMainMenu.lua")
    DL_PopActiveObject()

-- |[ ===================================== Close Handler ====================================== ]|
--Pass -1 to close the menu.
else
    MapM_PushMenuStackHead()
        FlexMenu_Clear()
        LM_ExecuteScript("Data/Scripts/MainMenu/000 PopulateMainMenu.lua")
    DL_PopActiveObject()
end