//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "PandemoniumRoom.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "EntityManager.h"
#include "OptionsManager.h"

//--Stores, manages, and updates the ActorIconPacks, which are used when the flag xActorIconsMove is true. This causes
//  Actor icons to move on the map, instead of instantly reappearing.

//--[Property Queries]
ActorIconPack *PandemoniumLevel::FindIconPack(uint32_t pID)
{
    //--Finds and returns the ActorIconPack in use by the provided entity ID. Returns NULL if none was found.
    ActorIconPack *rPack = (ActorIconPack *)mActorIconList->PushIterator();
    while(rPack)
    {
        //--ID Match? Return this pack.
        if(pID == rPack->mActorID)
        {
            mActorIconList->PopIterator();
            return rPack;
        }

        //--Next.
        rPack = (ActorIconPack *)mActorIconList->AutoIterate();
    }

    //--Could not find the pack.
    return NULL;
}

//--[Manipulators]
void PandemoniumLevel::AddActorToList(Actor *pActor, float pX, float pY)
{
    //--Called by PandemoniumRooms instead of rendering the Actor icons themselves. This routine will create a new package
    //  if one didn't exist for the given Actor, or update the existing pack if one does exist.
    if(!pActor) return;

    //--If the Actor already has an icon pack, update it.
    if(pActor->mIconPack)
    {
        //--Set.
        ActorIconPack *rPack = pActor->mIconPack;

        //--If not animating...
        if(rPack->mTimer >= xActorMoveTicks)
        {
            //--The passed position is the same as the end points. The Actor has not moved.
            //  This check also fails every 15 ticks just to make sure an entity doesn't get stuck.
            if(rPack->mEndX == pX && rPack->mEndY == pY && rPack->mEndZ == pActor->GetWorldZ() && Global::Shared()->gTicksElapsed % 15 != 0)
            {

            }
            //--The passed position is not the same. The Actor has moved.
            else
            {
                //--Reset flags.
                rPack->mTimer = (rand() % 3) * -1;
                rPack->mStartX = rPack->mCurX;
                rPack->mStartY = rPack->mCurY;
                rPack->mStartZ = rPack->mEndZ;
                rPack->mEndX = pX;
                rPack->mEndY = pY;
                rPack->mEndZ = pActor->GetWorldZ();

                //--Timer never lags when it's the player, since they always precipitate their own move.
                if(pActor->IsPlayerControlled()) rPack->mTimer = 1;

                //--Recompute visibility.
                rPack->mWasVisible = rPack->mIsVisible;
                rPack->mIsVisible = RecheckVisibility(rPack);
            }
        }
        //--If currently animating, ignore this.
        else
        {
            //--The passed position is the same as the end points. The Actor has not moved.
            if(rPack->mEndX == pX && rPack->mEndY == pY && rPack->mEndZ == pActor->GetWorldZ())
            {

            }
            //--The passed position is not the same. The Actor has moved before the animation completed.
            //  Attempt to salvage the situation.
            else
            {
                //--Reset flags.
                rPack->mTimer = 0;
                rPack->mStartX = rPack->mCurX;
                rPack->mStartY = rPack->mCurY;
                rPack->mStartZ = rPack->mEndZ;
                rPack->mEndX = pX;
                rPack->mEndY = pY;
                rPack->mEndZ = pActor->GetWorldZ();

                //--Recompute visibility.
                rPack->mWasVisible = rPack->mIsVisible;
                rPack->mIsVisible = RecheckVisibility(rPack);
            }
        }

        //--Done.
        return;
    }

    //--If we got this far, the actor was not on the list. Create a new entry.
    SetMemoryData(__FILE__, __LINE__);
    ActorIconPack *nPack = (ActorIconPack *)starmemoryalloc(sizeof(ActorIconPack));
    pActor->mIconPack = nPack;
    mActorIconList->AddElementAsTail("X", nPack, &DontDeleteThis);

    //--Setup.
    nPack->Initialize();
    nPack->mTimer = 0;
    nPack->mActorID = pActor->GetID();
    nPack->mActorColor.CloneFrom(pActor->GetLocalColor());
    nPack->mStartX = pX;
    nPack->mStartY = pY;
    nPack->mStartZ = pActor->GetWorldZ();
    nPack->mEndX = pX;
    nPack->mEndY = pY;
    nPack->mEndZ = nPack->mStartZ;

    //--Resolve the rendering image.
    int tRenderFlag = pActor->GetRenderFlag();
    if(tRenderFlag == RENDER_HUMAN)
    {
        nPack->rActorIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Human");
    }
    //--Monster.
    else if(tRenderFlag == RENDER_MONSTER)
    {
        nPack->rActorIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Monster");
    }

    //--Recheck visibility. A newly created Actor always starts as invisible and fades in.
    nPack->mIsVisible = RecheckVisibility(nPack);
    nPack->mWasVisible = false;

    //--Spawning entities always start invisible.
    nPack->mActorColor.a = 0.0f;
}
void PandemoniumLevel::RemoveActorPack(void *pPtr)
{
    mActorIconList->RemoveElementP(pPtr);
}

//--[Core Methods]
bool PandemoniumLevel::RecheckVisibility(ActorIconPack *pPack)
{
    //--Returns whether or not the icon in the provided pack is "visible". If obscured, on a different Z-level,
    //  destroyed, or too far away, the icon fades to fully transparent.
    if(!pPack || pPack->mActorID == 0) return false;

    //--Get the visible Z height. Entities that are not on that height are always invisible.
    int tVisibleZ = 0;
    Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerEntity) tVisibleZ = rPlayerEntity->GetWorldZ();

    //--Get the Actor associated with the package. If the Actor is not found, the ID is set to
    //  0 and the pack should be deleted once it has finished animating.
    Actor *rActor = (Actor *)EntityManager::Fetch()->GetEntityI(pPack->mActorID);
    if(!rActor || !rActor->IsOfType(POINTER_TYPE_ACTOR))
    {
        pPack->mActorID = 0;
        return false;
    }

    //--Get the room associated with the Actor. If not found, the ID is set to 0 and the
    //  package should be removed.
    PandemoniumRoom *rCurrentRoom = rActor->GetCurrentRoom();
    if(!rCurrentRoom)
    {
        pPack->mActorID = 0;
        return false;
    }

    //--If the Actor is on a different Z-level, it's invisible. Actors whose start and end Z are not the player's
    //  Z become invisible instantly!
    if(rActor->GetWorldZ() != tVisibleZ)
    {
        if(pPack->mStartZ != tVisibleZ && pPack->mEndZ != tVisibleZ) pPack->mWasVisible = false;
        return false;
    }

    //--If the Actor is obscured by an object (such as a Podling Pod) they are invisible.
    if(rActor->IsObscured()) return false;

    //--If this flag is false, objects at a distance can become invisible if the player is not nearby.
    bool tRadarFlag = OptionsManager::Fetch()->GetOptionB("Always Show Radar Indicators");
    if(!tRadarFlag)
    {
        //--Player can always see herself.
        if(rActor->IsPlayerControlled())
        {
            return true;
        }
        //--If the entity is in the same room as the player, they are visible.
        else if(rCurrentRoom->IsPlayerPresent())
        {
            return true;
        }
        //--If the entity is adjacent to a room with the player, they should render.
        else if(rCurrentRoom->IsPlayerAdjacent())
        {
            return true;
        }

        //--Radar check failed. Actor is invisible.
        return false;
    }

    //--If we got this far, all the checks passed. The object is visible.
    return true;
}

//--[Update]
void PandemoniumLevel::UpdateActorIcons()
{
    //--Run timers on all the actor icons.
    ActorIconPack *rPack = (ActorIconPack *)mActorIconList->SetToHeadAndReturn();
    while(rPack)
    {
        //--If not animating...
        if(rPack->mTimer >= xActorMoveTicks)
        {
            //--Periodically recheck visibility and existence flags.
            if(Global::Shared()->gTicksElapsed % 30 == 0)
            {
                //--Check if the Actor still exists. If not, set the ID to 0. This may happen even if the Actor
                //  in question hasn't moved, so it needs to be rechecked every tick.
                Actor *rActor = (Actor *)EntityManager::Fetch()->GetEntityI(rPack->mActorID);
                if(!rActor || !rActor->IsOfType(POINTER_TYPE_ACTOR))
                {
                    rPack->mActorID = 0;
                }

                //--If the Actor ID is 0, delete this object while it is invisible.
                rPack->mWasVisible = rPack->mIsVisible;
                if(!rPack->mActorID)
                {
                    mActorIconList->RemoveRandomPointerEntry();
                    rPack = (ActorIconPack *)mActorIconList->IncrementAndGetRandomPointerEntry();
                    continue;
                }
                //--If the pack still exists, recheck its visibility.
                else
                {
                    //--Flags.
                    rPack->mWasVisible = rPack->mIsVisible;
                    rPack->mIsVisible = RecheckVisibility(rPack);
                    if(rPack->mWasVisible != rPack->mIsVisible) rPack->mTimer = 0;

                    //--Re-resolve the rendering image. It can change when a human turns into a monster.
                    int tRenderFlag = rActor->GetRenderFlag();
                    if(tRenderFlag == RENDER_HUMAN)
                    {
                        rPack->rActorIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Human");
                    }
                    //--Monster.
                    else if(tRenderFlag == RENDER_MONSTER)
                    {
                        rPack->rActorIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Monster");
                    }
                }
            }
        }
        //--If animating, recompute alpha channels.
        else
        {
            //--Increment the timer.
            rPack->mTimer ++;

            //--If totally visible...
            if(rPack->mWasVisible && rPack->mIsVisible)
            {
                rPack->mActorColor.a = 1.0f;
            }
            //--If totally invisible...
            else if(!rPack->mWasVisible && !rPack->mIsVisible)
            {
                rPack->mActorColor.a = 0.0f;
            }
            //--If becoming visible after being out of sight...
            else if(!rPack->mWasVisible && rPack->mIsVisible)
            {
                rPack->mActorColor.a = ((float)rPack->mTimer / (float)xActorMoveTicks);
            }
            //--If becoming invisible after being visible...
            else if(rPack->mWasVisible && !rPack->mIsVisible)
            {
                rPack->mActorColor.a = 1.0f - ((float)rPack->mTimer / (float)xActorMoveTicks);
            }
        }

        //--Next pack.
        rPack = (ActorIconPack *)mActorIconList->IncrementAndGetRandomPointerEntry();
    }
}

//--[Drawing]
void PandemoniumLevel::RenderActorIcons()
{
    //--Renders the ActorIconPacks on the mActorIconList. The PandemoniumRooms do not do this if flagged, and will instead
    //  add their icons to the list.
    ActorIconPack *rPack = (ActorIconPack *)mActorIconList->PushIterator();
    while(rPack)
    {
        //--If not animating...
        if(rPack->mTimer >= xActorMoveTicks)
        {
            rPack->mCurX = rPack->mEndX;
            rPack->mCurY = rPack->mEndY;
        }
        //--If animating...
        else
        {
            float tPercent = EasingFunction::QuadraticOut(rPack->mTimer, xActorMoveTicks);
            if(tPercent < 0.0f) tPercent = 0.0f;
            rPack->mCurX = rPack->mStartX + ((rPack->mEndX - rPack->mStartX) * tPercent);
            rPack->mCurY = rPack->mStartY + ((rPack->mEndY - rPack->mStartY) * tPercent);
        }

        //--Render.
        if(rPack->rActorIcon)
        {
            float tRenderX = rPack->mCurX - (rPack->rActorIcon->GetWidth() / 2.0f);
            float tRenderY = rPack->mCurY - (rPack->rActorIcon->GetHeight() / 2.0f);
            rPack->mActorColor.SetAsMixer();
            rPack->rActorIcon->Draw(tRenderX, tRenderY);
        }

        //--Next.
        rPack = (ActorIconPack *)mActorIconList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
}
