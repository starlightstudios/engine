//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "PeakFreaksBadge.h"
#include "PeakFreaksBomb.h"
#include "PeakFreaksFlyby.h"
#include "PeakFreaksFragment.h"
#include "PeakFreaksPlayer.h"
#include "PeakFreaksPuzzlePiece.h"
#include "PeakFreaksTitle.h"
#include "TileLayer.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"
#include "SugarFont.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

///--[Local Definitions]
//#define PEAKFREAKSLEVELRENDER_DEBUG
#ifdef PEAKFREAKSLEVELRENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Drawing =============================================
void PeakFreaksLevel::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void PeakFreaksLevel::Render()
{
    ///--[Documentation]
    //--Renders the PeakFreaks level, including map, entities, and UI.
    DebugPush(true, "PeakFreaksLevel:Render Begins.\n");

    //--Offset rendering tick. Always increments, doesn't necessarily match update ticks.
    mSelectionOffsetTick ++;

    ///--[External]
    //--Flip this flag to let the engine know we handled rendering this tick.
    MapManager::xHasRenderedMenus = true;

    ///--[Error Checks]
    //--Fail if the images did not resolve.
    if(!Images.mIsReady) { DebugPop("PeakFreaksLevel:Render ends, images not ready.\n"); return; }

    //--Fail if the map scale is so off that it would cause problems or be zero.
    if(mCurrentScale < 0.01f) { DebugPop("PeakFreaksLevel:Render ends, scale invalid.\n"); return; }

    ///--[System]
    //Images.Data.rUIFont->DrawTextArgs(0.0f, 0.0f, 0, 1.0f, "%i", mWorldTimer);

    ///--[Clouds]
    //--Position at back of depth.
    glTranslatef(0.0f, 0.0f, -1.0f);

    //--Sky background.
    Images.Data.rBackgroundSky->Draw();

    //--Render independent of position.
    PFTitle_Cloud *rCloud = (PFTitle_Cloud *)mCloudList->PushIterator();
    while(rCloud)
    {
        rCloud->rImage->Draw(rCloud->mXPos, rCloud->mYPos);
        rCloud = (PFTitle_Cloud *)mCloudList->AutoIterate();
    }

    //--Clean.
    glTranslatef(0.0f, 0.0f, 1.0f);

    ///--[World Rendering]
    //--Constant Reposition. Tiles render under the UI.
    DebugPrint("Beginning world render.\n");
    float cTileBeginX = 134.0f;
    float cTileBeginY =  52.0f;
    glTranslatef(cTileBeginX, cTileBeginY, 0.0f);

    //--Scale.
    glScalef(mCurrentScale, mCurrentScale, 1.0f);

    //--Position. For now use the player's position as a camera proxy.
    float cCameraPosX = 0.0f;
    float cCameraPosY = 0.0f;
    if(mIsOpening)
    {
        cCameraPosX = mOpeningCameraX;
        cCameraPosY = mOpeningCameraY;
        glTranslatef(-cCameraPosX, -cCameraPosY, 0.0f);
    }
    else if(mPlayer)
    {
        //--Player position.
        float tPlayerWorldX = mPlayer->GetWorldX() + mPlayer->GetOffsetX();
        float tPlayerWorldY = mPlayer->GetWorldY() + mPlayer->GetOffsetY();
        float tPlayerPixelX = tPlayerWorldX * TileLayer::cxSizePerTile;
        float tPlayerPixelY = tPlayerWorldY * TileLayer::cxSizePerTile;

        //--Compute the expected size of the camera's FOV, and where the player should appear.
        //  The player is not centered due to the UI.
        float cCameraXAtOne = VIRTUAL_CANVAS_X / mCurrentScale;
        float cCameraYAtOne = VIRTUAL_CANVAS_Y / mCurrentScale;
        float cXPosTarget = 0.35f;
        float cYPosTarget = 0.40f;

        //--Camera matches the pixel position with an offset.
        float cCameraOffX = cCameraXAtOne * cXPosTarget * -1.0f;
        float cCameraOffY = cCameraYAtOne * cYPosTarget * -1.0f;
        cCameraPosX = tPlayerPixelX + cCameraOffX;
        cCameraPosY = tPlayerPixelY + cCameraOffY;
        mLastCameraX = cCameraPosX;
        mLastCameraY = cCameraPosY;
        glTranslatef(-cCameraPosX, -cCameraPosY, 0.0f);
    }
    else
    {
        cCameraPosX = -600.0f;
        cCameraPosY = -200.0f;
        glTranslatef(-cCameraPosX, -cCameraPosY, 0.0f);
        //fprintf(stderr, "%f %f\n", mPlayer->GetWorldX(), mPlayer->GetWorldY());
    }

    //--Call all of the sub-rendering functions.
    DebugPrint("Beginning sub-entity renders.\n");
    RenderTilemap();
    RenderEntities();

    //--Debug:
    if(false)
    {
        ZonePack *rDamageZone = (ZonePack *)mDamageZones->PushIterator();
        while(rDamageZone)
        {
            SugarBitmap::DrawRect(rDamageZone->mDim.mLft, rDamageZone->mDim.mTop, rDamageZone->mDim.mRgt, rDamageZone->mDim.mBot, StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f), 3.0f);
            rDamageZone = (ZonePack *)mDamageZones->AutoIterate();
        }
    }

    //--Clean.
    glTranslatef(cCameraPosX, cCameraPosY, 0.0f);
    glScalef(1.0f / mCurrentScale, 1.0f / mCurrentScale, 1.0f);
    glTranslatef(-cTileBeginX, -cTileBeginY, 0.0f);

    ///--[UI Rendering]
    DebugPrint("Beginning UI render.\n");
    RenderUI();
    RenderPause();
    RenderOpening();

    ///--[GUI Badges]
    PeakFreaksBadge *rBadge = (PeakFreaksBadge *)mBadgeList->PushIterator();
    while(rBadge)
    {
        rBadge->Render();
        rBadge = (PeakFreaksBadge *)mBadgeList->AutoIterate();
    }

    //--Fireworks.
    PFTitle_Firework *rFirework = (PFTitle_Firework *)mFireworks->PushIterator();
    while(rFirework)
    {
        //--Resolve frame.
        int tFrame = (int)(rFirework->mAnimTimer / (float)PFTITLE_FIREWORK_TPF);
        if(tFrame < 0) tFrame = 0;
        if(tFrame >= PFTITLE_FIREWORKS_FRAMES) tFrame = PFTITLE_FIREWORKS_FRAMES - 1;
        SugarBitmap *rImage = Images.Finale.rFireworks[rFirework->mColorIndex][tFrame];

        //--Render.
        rImage->Draw(rFirework->mXPos, rFirework->mYPos);

        //--Next.
        rFirework = (PFTitle_Firework *)mFireworks->AutoIterate();
    }

    ///--[Overlays]
    //--Clear indicator.
    if(mVictoryTimer < PF_VICTORY_TICKS - 5)
    {
        //--Compute percent.
        int cFadeTicks = 60;
        int tMinTicks = (PF_VICTORY_TICKS - cFadeTicks - 5);
        float cPct = 1.0f - EasingFunction::Linear(mVictoryTimer - tMinTicks, (float)cFadeTicks);
        if(cPct > 1.0f) cPct = 1.0f;

        //--Render.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);
        Images.Data.rOpeningCountdown[4]->Draw(429.0f, 299.0f);
        StarlightColor::ClearMixer();
    }

    //--Victory fade-to-white overlay.
    if(mVictoryTimer < 120)
    {
        float cPct = 1.0f - EasingFunction::Linear(mVictoryTimer, 120.0f);
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, cPct));
    }

    ///--[Debug Rendering]
    if(false)
    {
        //--Constants.
        float cTxtHei = Images.Data.rUIFont->GetTextHeight();
        float cTxtZro =   0.0f;
        float cTxtOne = 200.0f;

        //--Variables.
        float tPlayerWorldX = mPlayer->GetWorldX();
        float tPlayerWorldY = mPlayer->GetWorldY();
        float tPlayerPixelX = tPlayerWorldX * TileLayer::cxSizePerTile;
        float tPlayerPixelY = tPlayerWorldY * TileLayer::cxSizePerTile;
        float tPlayerSpeedX = mPlayer->GetSpeedX();
        float tPlayerSpeedY = mPlayer->GetSpeedY();

        //--Render.
        Images.Data.rUIFont->DrawText    (cTxtZro, cTxtHei * 0.0f, 1.0f, 0, "Player Statistics");
        Images.Data.rUIFont->DrawTextArgs(cTxtZro, cTxtHei * 1.0f, 1.0f, 0, " World: %5.2f", tPlayerWorldX);
        Images.Data.rUIFont->DrawTextArgs(cTxtOne, cTxtHei * 1.0f, 1.0f, 0, " %5.2f", tPlayerWorldY);
        Images.Data.rUIFont->DrawTextArgs(cTxtZro, cTxtHei * 2.0f, 1.0f, 0, " Pixel: %5.2f", tPlayerPixelX);
        Images.Data.rUIFont->DrawTextArgs(cTxtOne, cTxtHei * 2.0f, 1.0f, 0, " %5.2f", tPlayerPixelY);
        Images.Data.rUIFont->DrawTextArgs(cTxtZro, cTxtHei * 3.0f, 1.0f, 0, " Speeds: %5.2f", tPlayerSpeedX);
        Images.Data.rUIFont->DrawTextArgs(cTxtOne, cTxtHei * 3.0f, 1.0f, 0, " %5.2f", tPlayerSpeedY);
    }

    ///--[Debug]
    DebugPop("PeakFreaksLevel:Render ends, completed normally.\n");

    ///--[Dialogue GUI]
    //--Has a subroutine.
    RenderDialogue();

    ///--[Crossfading]
    if(mIsOpening && mOpeningTimer < PF_CROSSFADE_TICKS)
    {
        float cPct = 1.0f - EasingFunction::QuadraticOut(mOpeningTimer, PF_CROSSFADE_TICKS);
        SugarBitmap::DrawFullBlack(cPct);
    }
    if(mReturnToTitle)
    {
        float cPct = EasingFunction::QuadraticOut(mPauseToTitleTimer, PF_CROSSFADE_TICKS);
        SugarBitmap::DrawFullBlack(cPct);
    }
}

///====================================== Tile Rendering ==========================================
void PeakFreaksLevel::RenderEntities()
{
    ///--[ ====== Documentation ===== ]
    //--Renders all entities present in the level.
    if(!mPlayer) return;

    //--Checkpoints.
    ZonePack *rCheckpoint = (ZonePack *)mCheckpointZones->PushIterator();
    while(rCheckpoint)
    {
        //--Skip if not on the current layer.
        if(rCheckpoint->mLayer != mPlayer->GetWorldZ())
        {
            rCheckpoint = (ZonePack *)mCheckpointZones->AutoIterate();
            continue;
        }

        //--Resolve checkpoint image. It changes based on the size and whether active or not.
        bool tIsActive = (mActiveCheckpointName && !strcasecmp(mCheckpointZones->GetIteratorName(), mActiveCheckpointName));
        SugarBitmap *rUseImg = Images.Data.rCheckpointIH4;
        float cWid = rCheckpoint->mDim.mRgt - rCheckpoint->mDim.mLft;
        float cHei = rCheckpoint->mDim.mBot - rCheckpoint->mDim.mTop;
        if(cWid > 48.0f)
        {
            rUseImg = Images.Data.rCheckpointIH4;
            if(tIsActive) rUseImg = Images.Data.rCheckpointAH4;
        }
        else if(cWid > 32.0f)
        {
            rUseImg = Images.Data.rCheckpointIH3;
            if(tIsActive) rUseImg = Images.Data.rCheckpointAH3;
        }
        else if(cWid > 16.0f)
        {
            rUseImg = Images.Data.rCheckpointIH2;
            if(tIsActive) rUseImg = Images.Data.rCheckpointAH2;
        }
        else if(cHei > 48.0f)
        {
            rUseImg = Images.Data.rCheckpointIV4;
            if(tIsActive) rUseImg = Images.Data.rCheckpointAV4;
        }
        else if(cHei > 32.0f)
        {
            rUseImg = Images.Data.rCheckpointIV3;
            if(tIsActive) rUseImg = Images.Data.rCheckpointAV3;
        }
        else
        {
            rUseImg = Images.Data.rCheckpointIV2;
            if(tIsActive) rUseImg = Images.Data.rCheckpointAV2;
        }

        //--Render.
        rUseImg->Draw(rCheckpoint->mDim.mLft, rCheckpoint->mDim.mTop);

        //--Next.
        rCheckpoint = (ZonePack *)mCheckpointZones->AutoIterate();
    }

    ///--[ === Coins and Powerups === ]
    //--Compute the coin frame. All coins use the same frame.
    int tCoinFrame = (mCoinTimer / PFLEVEL_COIN_TPF) % PFLEVEL_COIN_FRAMES;

    //--Coins and powerups share the same listing. The subtype determines the render frame.
    ZonePack *rCoinPack = (ZonePack *)mCoinZones->PushIterator();
    while(rCoinPack)
    {
        //--Skip if not on the current layer.
        if(rCoinPack->mLayer != mPlayer->GetWorldZ())
        {
            rCoinPack = (ZonePack *)mCoinZones->AutoIterate();
            continue;
        }

        //--Resolve the image.
        SugarBitmap *rRenderImg = Images.Data.rCoin[tCoinFrame];
        if(     rCoinPack->mSubtype == PFLEVEL_COIN_SUBTYPE_GUANOBOMB)   rRenderImg = Images.Data.rPowerup_GuanoBomb;
        else if(rCoinPack->mSubtype == PFLEVEL_COIN_SUBTYPE_DRONESTRIKE) rRenderImg = Images.Data.rPowerup_Strike;
        else if(rCoinPack->mSubtype == PFLEVEL_COIN_SUBTYPE_SUPERSTRIKE) rRenderImg = Images.Data.rPowerup_SuperStrike;
        else if(rCoinPack->mSubtype == PFLEVEL_COIN_SUBTYPE_CANYONBAR)   rRenderImg = Images.Data.rPowerup_CanyonBar;
        else if(rCoinPack->mSubtype == PFLEVEL_COIN_SUBTYPE_HELILIFT)    rRenderImg = Images.Data.rPowerup_HeliLift;
        else if(rCoinPack->mSubtype == PFLEVEL_COIN_SUBTYPE_ICEBEAM)     rRenderImg = Images.Data.rPowerup_IceBeam;

        //--Render.
        if(rRenderImg) rRenderImg->Draw(rCoinPack->mDim.mLft, rCoinPack->mDim.mTop);

        //--Next.
        rCoinPack = (ZonePack *)mCoinZones->AutoIterate();
    }

    //--Treasures.
    ZonePack *rTreasurePack = (ZonePack *)mTreasureZones->PushIterator();
    while(rTreasurePack)
    {
        //--Skip if not on the current layer.
        if(rTreasurePack->mLayer != mPlayer->GetWorldZ())
        {
            rTreasurePack = (ZonePack *)mTreasureZones->AutoIterate();
            continue;
        }

        //--Render.
        Images.Data.rTreasureClosed->Draw(rTreasurePack->mDim.mLft, rTreasurePack->mDim.mTop);

        //--Next.
        rTreasurePack = (ZonePack *)mTreasureZones->AutoIterate();
    }

    ///--[Hazards]
    //--Sliders.
    PF_Slider *rSlider = (PF_Slider *)mSliderList->PushIterator();
    while(rSlider)
    {
        rSlider->Render();
        if(mIceBeamTimer > 0) rSlider->RenderIceBeam(Images.Data.rPowerupIceBeamOver);
        rSlider = (PF_Slider *)mSliderList->AutoIterate();
    }

    //--Spinners.
    PF_Spinner *rSpinner = (PF_Spinner *)mSpinnerList->PushIterator();
    while(rSpinner)
    {
        rSpinner->Render();
        if(mIceBeamTimer > 0) rSpinner->RenderIceBeam(Images.Data.rPowerupIceBeamOver);
        rSpinner = (PF_Spinner *)mSpinnerList->AutoIterate();
    }

    //--Waterfalls.
    PF_Waterfall *rWaterfall = (PF_Waterfall *)mWaterfallList->PushIterator();
    while(rWaterfall)
    {
        rWaterfall->Render();
        rWaterfall = (PF_Waterfall *)mWaterfallList->AutoIterate();
    }

    //--Bomb Spikes.
    PF_BombSpike *rBombSpike = (PF_BombSpike *)mBombSpikeList->PushIterator();
    while(rBombSpike)
    {
        rBombSpike->Render();
        if(mIceBeamTimer > 0) rBombSpike->RenderIceBeam(Images.Data.rPowerupIceBeamOver);
        rBombSpike = (PF_BombSpike *)mBombSpikeList->AutoIterate();
    }

    ///--[Dust Fragments]
    //--These render over hazards but under the players.
    PeakFreaksFragment *rUIFragment = (PeakFreaksFragment *)mWorldFragmentList->PushIterator();
    while(rUIFragment)
    {
        //--Run render.
        rUIFragment->Render();

        //--Next.
        rUIFragment = (PeakFreaksFragment *)mWorldFragmentList->AutoIterate();
    }

    ///--[Player and Rival]
    //--Player
    {
        //--Resolve the images.
        SugarBitmap *rPlayerImg = mPlayer->ResolveFrame();
        SugarBitmap *rArrowImg  = mPlayer->ResolveArrow();
        SugarBitmap *rShadowImg = mPlayer->GetShadow();

        //--Player position.
        float tPlayerWorldX  = mPlayer->GetWorldX() + mPlayer->GetOffsetX();
        float tPlayerWorldY  = mPlayer->GetWorldY() + mPlayer->GetOffsetY();
        float tPlayerOffsetZ = mPlayer->GetOffsetZ();
        float tPlayerPixelX  = tPlayerWorldX * TileLayer::cxSizePerTile;
        float tPlayerPixelY  = tPlayerWorldY * TileLayer::cxSizePerTile;

        //--If the player is respawning, this adjusts their Y position as they fly back into the game.
        int tRespawnTimer = mPlayer->GetRespawnTimer();
        if(tRespawnTimer > 0)
        {
            float tRespawnPct = tRespawnTimer / (float)PLAYER_RESPAWN_TICKS;
            tPlayerPixelY = tPlayerPixelY + (PLAYER_RESPAWN_DISTANCE * tRespawnPct);
        }

        //--Render the player's shadow under them if they are airborne.
        if(rShadowImg)
        {
            rShadowImg->Draw(tPlayerPixelX, tPlayerPixelY);
        }

        //--Get the helicopter image. Can be NULL if the player isn't using it.
        SugarBitmap *rHelicopterImg = mPlayer->GetHelicopter();

        //--Player's image. Apply the height offset.
        tPlayerPixelY = tPlayerPixelY + (tPlayerOffsetZ * TileLayer::cxSizePerTile);
        if(rPlayerImg)
        {
            //--Special: Canyon Bar can cause the player image to flash white.
            if(mPlayer->IsFlashingWhite())
            {
                //--Normal image. Color mask stays on.
                DisplayManager::ActivateMaskRender(1);
                glColorMask(true, true, true, true);
                rPlayerImg->Draw(tPlayerPixelX, tPlayerPixelY);

                //--Render a white overlay.
                DisplayManager::ActivateStencilRender(1);
                glDisable(GL_TEXTURE_2D);
                glColor4f(1.0f, 1.0f, 1.0f, 0.50f);
                rPlayerImg->Draw(tPlayerPixelX, tPlayerPixelY);

                //--Clean up.
                glEnable(GL_TEXTURE_2D);
                DisplayManager::DeactivateStencilling();
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);

                //--Helicopter.
                if(rHelicopterImg)rHelicopterImg->Draw(tPlayerPixelX-18, tPlayerPixelY-31);
            }
            //--Normal:
            else
            {
                //--Damage offsets.
                float tDamageOffX = mPlayer->GetDamageOffsetX();
                float tDamageOffY = mPlayer->GetDamageOffsetY();
                float tDamageOffR = mPlayer->GetDamageRotation();

                //--Rotation is nonzero:
                if(tDamageOffR != 0.0f)
                {
                    float tX = tPlayerPixelX + tDamageOffX + 8.0f;
                    float tY = tPlayerPixelY + tDamageOffY + 8.0f;
                    glTranslatef(tX, tY, 0.0f);
                    glRotatef(tDamageOffR, 0.0, 0.0f, 1.0f);
                    rPlayerImg->Draw(-8.0f, -8.0f);
                    glRotatef(-tDamageOffR, 0.0, 0.0f, 1.0f);
                    glTranslatef(-tX, -tY, 0.0f);
                }
                //--Normal render.
                else
                {
                    rPlayerImg->Draw(tPlayerPixelX + tDamageOffX, tPlayerPixelY + tDamageOffY);

                    //--Helicopter.
                    if(rHelicopterImg) rHelicopterImg->Draw(tPlayerPixelX-18, tPlayerPixelY-31);
                }
            }
        }
    }

    //--Rival.
    if(mRival)
    {
        //--Resolve the images.
        SugarBitmap *rRivalImg  = mRival->ResolveFrame();
        SugarBitmap *rArrowImg  = mRival->ResolveArrow();
        SugarBitmap *rShadowImg = mRival->GetShadow();

        //--Player position.
        float tRivalWorldX  = mRival->GetWorldX() + mRival->GetOffsetX();
        float tRivalWorldY  = mRival->GetWorldY() + mRival->GetOffsetY();
        float tRivalOffsetZ = mRival->GetOffsetZ();
        float tRivalPixelX  = tRivalWorldX * TileLayer::cxSizePerTile;
        float tRivalPixelY  = tRivalWorldY * TileLayer::cxSizePerTile;

        //--If the player is respawning, this adjusts their Y position as they fly back into the game.
        int tRespawnTimer = mRival->GetRespawnTimer();
        if(tRespawnTimer > 0)
        {
            float tRespawnPct = tRespawnTimer / (float)PLAYER_RESPAWN_TICKS;
            tRivalPixelY = tRivalPixelY + (PLAYER_RESPAWN_DISTANCE * tRespawnPct);
        }

        //--Render the rival's shadow under them if they are airborne.
        if(rShadowImg)
        {
            rShadowImg->Draw(tRivalPixelX, tRivalPixelY);
        }

        //--Get the helicopter image. Can be NULL if the player isn't using it.
        SugarBitmap *rHelicopterImg = mRival->GetHelicopter();

        //--Rival's image.
        tRivalPixelY = tRivalPixelY + (tRivalOffsetZ * TileLayer::cxSizePerTile);
        if(rRivalImg)
        {
            //--Special: Canyon Bar can cause the player image to flash white.
            if(mRival->IsFlashingWhite())
            {
                //--Normal image. Color mask stays on.
                DisplayManager::ActivateMaskRender(2);
                glColorMask(true, true, true, true);
                rRivalImg->Draw(tRivalPixelX, tRivalPixelY);

                //--Render a white overlay.
                DisplayManager::ActivateStencilRender(2);
                glDisable(GL_TEXTURE_2D);
                glColor4f(1.0f, 1.0f, 1.0f, 0.50f);
                rRivalImg->Draw(tRivalPixelX, tRivalPixelY);
                glEnable(GL_TEXTURE_2D);

                //--Clean up.
                DisplayManager::DeactivateStencilling();
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);

                //--Helicopter.
                if(rHelicopterImg) rHelicopterImg->Draw(tRivalPixelX-18, tRivalPixelY-31);
            }
            //--Normal:
            else
            {
                //--Damage offsets.
                float tDamageOffX = mRival->GetDamageOffsetX();
                float tDamageOffY = mRival->GetDamageOffsetY();
                float tDamageOffR = mRival->GetDamageRotation();

                //--Rotation is nonzero:
                if(tDamageOffR != 0.0f)
                {
                    float tX = tRivalPixelX + tDamageOffX + 8.0f;
                    float tY = tRivalPixelY + tDamageOffY + 8.0f;
                    glTranslatef(tX, tY, 0.0f);
                    glRotatef(tDamageOffR, 0.0, 0.0f, 1.0f);
                    rRivalImg->Draw(-8.0f, -8.0f);
                    glRotatef(tDamageOffR * -1.0f, 0.0, 0.0f, 1.0f);
                    glTranslatef(-tX, -tY, 0.0f);
                }
                //--Normal render.
                else
                {
                    rRivalImg->Draw(tRivalPixelX + tDamageOffX, tRivalPixelY + tDamageOffY);

                    //--Helicopter.
                    if(rHelicopterImg)rHelicopterImg->Draw(tRivalPixelX-18, tRivalPixelY-31);
                }
            }
        }
    }

    ///--[Airborne Entities]
    //--Flybys.
    PeakFreaksFlyby *rFlyby = (PeakFreaksFlyby *)mFlybyList->PushIterator();
    while(rFlyby)
    {
        rFlyby->Render();
        rFlyby = (PeakFreaksFlyby *)mFlybyList->AutoIterate();
    }

    //--Bombs.
    PeakFreaksBomb *rBomb = (PeakFreaksBomb *)mBombList->PushIterator();
    while(rBomb)
    {
        rBomb->Render();
        rBomb = (PeakFreaksBomb *)mBombList->AutoIterate();
    }

    //--Ice Beam Particles.
    PF_Particle *rParticle = (PF_Particle *)mIceBeamParticles->PushIterator();
    while(rParticle)
    {
        Images.Data.rPowerupIceBeamRain->Draw(rParticle->mXPos - 8.0f, rParticle->mYPos - 8.0f);

        //--Next.
        rParticle = (PF_Particle *)mIceBeamParticles->AutoIterate();
    }
}
void PeakFreaksLevel::RenderTilemap()
{
    ///--[Documentation]
    //--Renders the tiles that should be visible on screen, in order of back to front.
    DebugPush(true, "Peak Freaks Level Render Begin.\n");

    ///--[Tile Layers]
    //--Render.
    TileLayer *rLayer = (TileLayer *)mTileLayers->PushIterator();
    while(rLayer)
    {
        rLayer->RenderRange(0, 0, 1000, 1000);
        rLayer = (TileLayer *)mTileLayers->AutoIterate();
    }

    ///--[Goal Flags]
    //--First pass: Resolve the goal position and where the flags go.
    if(mResolveGoalPosition)
    {
        //--Unset.
        mResolveGoalPosition = false;

        //--Get zeroth victory zone.
        ZonePack *rZeroPack = (ZonePack *)mVictoryZones->GetElementBySlot(0);
        if(rZeroPack)
        {
            mGoalX = rZeroPack->mDim.mLft - 32.0f;
            mGoalY = rZeroPack->mDim.mTop - 38.0f;
        }
    }

    //--Render. Don't render during the tutorial.
    if(mRival)
    {
        int tFrame = (mGoalFlagTimer / PF_FLAG_TPF) % PF_FLAG_FRAMES;
        Images.Data.rGoalFlags[tFrame]->Draw(mGoalX, mGoalY);
    }

    ///--[Debug]
    DebugPop("Peak Freaks Level Render End.\n");
}

void PeakFreaksLevel::RenderUI()
{
    ///--[Documentation]
    //--Renders the UI, including menus and the puzzle interface.

    ///--[Backing]
    //--Render the fixed sections that have no special properties.
    Images.Data.rUIBackground->Draw();
    Images.Data.rUIMenuButton->Draw();

    //--Player. This uses masking. Render the mask plane which is also a blue color.
    DisplayManager::ActivateMaskRenderColors(PEAKFREAKS_PLAYER_STENCIL);
    Images.Data.rUIPlayerBacking->Draw();
    Images.Data.rUIRivalBacking->Draw();

    //--Render the player.
    DisplayManager::ActivateStencilRender(PEAKFREAKS_PLAYER_STENCIL);
    SugarBitmap *rPlayerPortrait = mPlayer->GetPortrait();
    if(rPlayerPortrait)
    {
        //--Offset due to jump.
        float cJumpOffset = mPlayer->GetOffsetZ() * 35.0f;
        glTranslatef(-96.0f, cJumpOffset, 0.0f);
        rPlayerPortrait->Draw();
        glTranslatef(96.0f, -cJumpOffset, 0.0f);
    }
    if(mPlayer->IsGuanod()) Images.Data.rUIPlayerGuanoOverlay->Draw();

    //--Render the rival.
    if(mRival)
    {
        SugarBitmap *rRivalPortrait = mRival->GetPortrait();
        if(rRivalPortrait)
        {
            float cJumpOffset = mRival->GetOffsetZ() * 35.0f;
            glTranslatef(96.0f, cJumpOffset, 0.0f);
            rRivalPortrait->Draw();
            glTranslatef(-96.0f, -cJumpOffset, 0.0f);
        }
        if(mRival->IsGuanod()) Images.Data.rUIRivalGuanoOverlay->Draw();
    }

    //--Disable stencils.
    glDisable(GL_STENCIL_TEST);

    //--Frame.
    Images.Data.rUIPlayerFrame->Draw();
    Images.Data.rUIRivalFrame->Draw();

    //--Canyon Bar Particles.
    PF_Particle *rParticle = (PF_Particle *)mCanyonBarParticles->PushIterator();
    while(rParticle)
    {
        //--Render centered.
        Images.Data.rCanyonBarFlame->Draw(rParticle->mXPos - 16.0f, rParticle->mYPos - 16.0f);

        //--Next.
        rParticle = (PF_Particle *)mCanyonBarParticles->AutoIterate();
    }

    ///--[Puzzle Interface]
    //--Constants.
    float cRenderLft = PLL_PUZZLE_X;
    float cRenderTop = PLL_PUZZLE_Y;
    float cRenderRgt = PLL_PUZZLE_X + (PLL_PUZZLE_W * PLL_PUZZLE_SIZE_X);
    float cRenderBot = PLL_PUZZLE_Y + (PLL_PUZZLE_H * PLL_PUZZLE_SIZE_Y);

    //--Stencils. Render the area mask.
    DisplayManager::ActivateMaskRender(PEAKFREAKS_PUZZLE_STENCIL);
    Images.Data.rUIPlayerPuzzleAreaMask->Draw();

    //--Switch to stencil mode, render normally.
    DisplayManager::ActivateStencilRender(PEAKFREAKS_PUZZLE_STENCIL);
    Images.Data.rUIPlayerPuzzleArea->Draw();

    //--Double-render list. Entries on this list deliberately render twice so they appear to slide
    //  over edges.
    SugarLinkedList *tDoubleRenderList = new SugarLinkedList(false);

    //--Run across pieces adjacent to the center piece and flag them if they need to render offcenter.
    SetMatchingPiecesOffsetTick();

    //--Render all the puzzle blocks.
    for(int x = 0; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        for(int y = 0; y < PLL_PUZZLE_SIZE_Y; y ++)
        {
            //--Get the type. If it's PEAKFREAK_PUZZLE_TYPE_NONE, render nothing.
            int tPuzzleType = mPuzzleGrid[x][y]->GetPuzzleType();
            if(tPuzzleType <= PEAKFREAK_PUZZLE_TYPE_NONE) continue;
            if(tPuzzleType >= PEAKFREAK_PUZZLE_TYPE_MAX) continue;

            //--Position.
            EasingPack2D tPositionPack = mPuzzleGrid[x][y]->GetPosition();

            //--If this object is over a boundary position, add it to the double-render list.
            if(tPositionPack.mXCur < cRenderLft)
            {
                tDoubleRenderList->AddElementAsHead("X", mPuzzleGrid[x][y]);
            }
            else if(tPositionPack.mXCur > cRenderRgt-PLL_PUZZLE_W)
            {
                tDoubleRenderList->AddElementAsHead("X", mPuzzleGrid[x][y]);
            }
            else if(tPositionPack.mYCur < cRenderTop)
            {
                tDoubleRenderList->AddElementAsHead("X", mPuzzleGrid[x][y]);
            }
            else if(tPositionPack.mYCur > cRenderBot-PLL_PUZZLE_H)
            {
                tDoubleRenderList->AddElementAsHead("X", mPuzzleGrid[x][y]);
            }

            //--Get the power.
            int tPower = mPuzzleGrid[x][y]->GetPower();
            if(tPower < 1) tPower = 0;
            if(tPower >PLL_POWER_MAX) tPower = PLL_POWER_MAX-1;

            //--Resolve X/Y Position. It may be offcenter due to the rendering tick.
            float tRenderX = tPositionPack.mXCur;
            float tRenderY = tPositionPack.mYCur;
            if(mPuzzleGrid[x][y]->GetOffsetTick() == mSelectionOffsetTick && mSelectionOffsetTimer < PF_SELECTION_OFFSET_TIMER)
            {
                //--Get the direction.
                float tXOff = 0.0f;
                float tYOff = 0.0f;
                if(tPuzzleType == PEAKFREAK_PUZZLE_TYPE_ARROW_DN) tYOff = PF_SELECTION_OFFSET_DISTANCE;
                if(tPuzzleType == PEAKFREAK_PUZZLE_TYPE_ARROW_UP) tYOff = -PF_SELECTION_OFFSET_DISTANCE;
                if(tPuzzleType == PEAKFREAK_PUZZLE_TYPE_ARROW_RT) tXOff = PF_SELECTION_OFFSET_DISTANCE;
                if(tPuzzleType == PEAKFREAK_PUZZLE_TYPE_ARROW_LF) tXOff = -PF_SELECTION_OFFSET_DISTANCE;

                //--Multiply by the timer.
                float cPct = mSelectionOffsetTimer / (float)PF_SELECTION_OFFSET_TIMER;
                cPct = sinf(cPct * 3.1415926f);
                tXOff = tXOff * cPct;
                tYOff = tYOff * cPct;

                //--Set.
                tRenderX = tRenderX + tXOff;
                tRenderY = tRenderY + tYOff;
            }

            //--The puzzle types align with the images in this array.
            Images.Data.mPuzzleArray[tPuzzleType]->Draw(tRenderX, tRenderY);

            //--If the puzzle block has a power bonus, render the "Crush" image over it.
            if(tPower > 0)
            {
                Images.Data.rCrushIndicator->Draw(tRenderX-4, tRenderY-4);
            }
        }
    }

    //--All blocks ordered to double-render do so here.
    PeakFreaksPuzzlePiece *rPiece = (PeakFreaksPuzzlePiece *)tDoubleRenderList->PushIterator();
    while(rPiece)
    {
        //--Position.
        EasingPack2D tPositionPack = rPiece->GetPosition();
        float tDoubleX = tPositionPack.mXCur;
        float tDoubleY = tPositionPack.mYCur;

        //--New position to double-render.
        if(tDoubleX < cRenderLft)
        {
            tDoubleX = tDoubleX + (cRenderRgt - cRenderLft);
        }
        else if(tDoubleX > cRenderRgt-PLL_PUZZLE_W)
        {
            tDoubleX = tDoubleX - (cRenderRgt - cRenderLft);
        }
        else if(tDoubleY < cRenderTop)
        {
            tDoubleY = tDoubleY + (cRenderBot - cRenderTop);
        }
        else if(tDoubleY > cRenderBot-PLL_PUZZLE_H)
        {
            tDoubleY = tDoubleY - (cRenderBot - cRenderTop);
        }

        //--Get the power.
        int tPower = rPiece->GetPower();
        if(tPower < 1) tPower = 0;
        if(tPower >PLL_POWER_MAX) tPower = PLL_POWER_MAX-1;

        //--Render.
        int tPuzzleType = rPiece->GetPuzzleType();
        Images.Data.mPuzzleArray[tPuzzleType]->Draw(tDoubleX, tDoubleY);

        //--If the puzzle block has a power bonus, render the "Crush" image over it.
        if(tPower > 0)
        {
            Images.Data.rCrushIndicator->Draw(tDoubleX-4, tDoubleY-4);
        }

        //--Next.
        rPiece = (PeakFreaksPuzzlePiece *)tDoubleRenderList->AutoIterate();
    }

    //--Disable stencils.
    DisplayManager::DeactivateStencilling();

    ///--[Basic Frame]
    //--Renders over the puzzles.
    Images.Data.rUIPlayerSpeedFrame->Draw();
    int tPlayerSpeedLevel = mPlayer->GetSpeedBarLevel();
    if(tPlayerSpeedLevel >= 1) Images.Data.rUIPlayerSpeedFill0->Draw();
    if(tPlayerSpeedLevel >= 2) Images.Data.rUIPlayerSpeedFill1->Draw();
    if(tPlayerSpeedLevel >= 3) Images.Data.rUIPlayerSpeedFill2->Draw();
    if(tPlayerSpeedLevel >= 4) Images.Data.rUIPlayerSpeedFill3->Draw();
    if(tPlayerSpeedLevel >= 5) Images.Data.rUIPlayerSpeedFill4->Draw();

    //--Cursor. Always renders in the middle of the puzzle grid.
    float tCursorX = PLL_PUZZLE_X + (PLL_PUZZLE_W * PLL_PUZZLE_MID_X) - 2.0f;
    float tCursorY = PLL_PUZZLE_Y + (PLL_PUZZLE_H * PLL_PUZZLE_MID_Y) - 2.0f;

    //--If adjacent cursors match, draw a cursor over them as well.
    int tCenterCode = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPuzzleType();

    //--Resolve current cursor image.
    int tUseTimer = mCursorTimer;
    int tCursorFrame = (mCursorTimer / PF_CURSOR_TPF) % PF_CURSOR_FRAMES;
    SugarBitmap *rCursorImg = Images.Data.rCursor[tCursorFrame];
    int cCursorOffsetX = 19;
    int cCursorOffsetY = 19;

    //--Iterate left.
    for(int x = PLL_PUZZLE_MID_X-1; x >= 0; x --)
    {
        int tUseFrame = (tUseTimer / PF_CURSOR_TPF) % PF_CURSOR_FRAMES;
        tUseTimer += 2;
        rCursorImg = Images.Data.rCursor[tUseFrame];
        if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == tCenterCode)
            rCursorImg->Draw(cRenderLft + (x * PLL_PUZZLE_W) - cCursorOffsetX - 2, tCursorY-cCursorOffsetY);
        else
            break;
    }
    tUseTimer = mCursorTimer;
    for(int x = PLL_PUZZLE_MID_X+1; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        int tUseFrame = (tUseTimer / PF_CURSOR_TPF) % PF_CURSOR_FRAMES;
        tUseTimer += 2;
        rCursorImg = Images.Data.rCursor[tUseFrame];
        if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == tCenterCode)
            rCursorImg->Draw(cRenderLft + (x * PLL_PUZZLE_W) - cCursorOffsetX - 2, tCursorY-cCursorOffsetY);
        else
            break;
    }
    tUseTimer = mCursorTimer;
    for(int y = PLL_PUZZLE_MID_Y-1; y >= 0; y --)
    {
        int tUseFrame = (tUseTimer / PF_CURSOR_TPF) % PF_CURSOR_FRAMES;
        tUseTimer += 2;
        rCursorImg = Images.Data.rCursor[tUseFrame];
        if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == tCenterCode)
            rCursorImg->Draw(tCursorX-cCursorOffsetX, cRenderTop + (y * PLL_PUZZLE_H) - cCursorOffsetY - 2);
        else
            break;
    }
    tUseTimer = mCursorTimer;
    for(int y = PLL_PUZZLE_MID_Y+1; y < PLL_PUZZLE_SIZE_Y; y ++)
    {
        int tUseFrame = (tUseTimer / PF_CURSOR_TPF) % PF_CURSOR_FRAMES;
        tUseTimer += 2;
        rCursorImg = Images.Data.rCursor[tUseFrame];
        if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == tCenterCode)
            rCursorImg->Draw(tCursorX-cCursorOffsetX, cRenderTop + (y * PLL_PUZZLE_H) - cCursorOffsetY - 2);
        else
            break;
    }

    //--Render cursor over other cursors.
    rCursorImg = Images.Data.rCursor[tCursorFrame];
    rCursorImg->Draw(tCursorX-cCursorOffsetX, tCursorY-cCursorOffsetY);
    //Images.Data.rCursorHi->Draw(tCursorX-4, tCursorY-4);

    ///--[Clean]
    delete tDoubleRenderList;

    ///--[Speed Boost Particles]
    //--Renders over the puzzle board.
    rParticle = (PF_Particle *)mSpeedBoostParticles->PushIterator();
    while(rParticle)
    {
        //--Render centered.
        if(rParticle->mTimer >= 0)
            Images.Data.rSpeedParticle->Draw(rParticle->mXPos - 1.0f, rParticle->mYPos - 1.0f);

        //--Next.
        rParticle = (PF_Particle *)mSpeedBoostParticles->AutoIterate();
    }

    ///--[Block Fragments]
    //--These render over the puzzle board to make them easier to see.
    PeakFreaksFragment *rUIFragment = (PeakFreaksFragment *)mFragmentList->PushIterator();
    while(rUIFragment)
    {
        //--Run render.
        rUIFragment->Render();

        //--Next.
        rUIFragment = (PeakFreaksFragment *)mFragmentList->AutoIterate();
    }

    ///--[Track Progress Indicators]
    //--Shows where the player and rival are relative to the checkpoints. If no rival exists, this doesn't
    //  display because we're in the tutorial level.
    if(mPlayer && mRival)
    {
        //--Render the track backing.
        Images.Data.rTrackBack->Draw();

        //--Render the track fill, based on the player's progress. Rival is not tracked.
        SugarBitmap::DrawHorizontalPercent(Images.Data.rTrackFill, mPlayerTrackProgress);

        //--Constants.
        float cHeadStart = 400.0f;
        float cHeadEnd = 1000.0f;
        float cHeadYPos = 666.0f;

        //--Render the player's head.
        SugarBitmap *rPlayerHead = mPlayer->GetTrackHead();
        if(rPlayerHead)
        {
            float cHeadPosX = cHeadStart + ((cHeadEnd - cHeadStart) * mPlayerTrackProgress);
            float cDamageOffsetY = mPlayer->GetDamageOffsetY();
            rPlayerHead->Draw(cHeadPosX, cHeadYPos + cDamageOffsetY);
        }

        //--Render the rival's head.
        SugarBitmap *rRivalHead = mRival->GetTrackHead();
        if(rRivalHead)
        {
            float cHeadPosX = cHeadStart + ((cHeadEnd - cHeadStart) * mRivalTrackProgress);
            float cDamageOffsetY = mRival->GetDamageOffsetY();
            rRivalHead->Draw(cHeadPosX, cHeadYPos + cDamageOffsetY);
        }
    }

    ///--[Objective]
    //--Renders during the tutorial. Handled by a subroutine.
    RenderObjective();
}
void PeakFreaksLevel::SetMatchingPiecesOffsetTick()
{
    ///--[Documentation]
    //--Checks all puzzle pieces on the piece grid. Any that match the central code get their local tick
    //  set to match the global tick. This way they know to render off center at certain intervals.
    mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->SetOffsetTick(mSelectionOffsetTick);
    int tCenterCode = mPuzzleGrid[PLL_PUZZLE_MID_X][PLL_PUZZLE_MID_Y]->GetPuzzleType();

    //--Iterate left.
    for(int x = PLL_PUZZLE_MID_X-1; x >= 0; x --)
    {
        if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == tCenterCode)
            mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->SetOffsetTick(mSelectionOffsetTick);
        else
            break;
    }
    for(int x = PLL_PUZZLE_MID_X+1; x < PLL_PUZZLE_SIZE_X; x ++)
    {
        if(mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->GetPuzzleType() == tCenterCode)
            mPuzzleGrid[x][PLL_PUZZLE_MID_Y]->SetOffsetTick(mSelectionOffsetTick);
        else
            break;
    }
    for(int y = PLL_PUZZLE_MID_Y-1; y >= 0; y --)
    {
        if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == tCenterCode)
            mPuzzleGrid[PLL_PUZZLE_MID_X][y]->SetOffsetTick(mSelectionOffsetTick);
        else
            break;
    }
    for(int y = PLL_PUZZLE_MID_Y+1; y < PLL_PUZZLE_SIZE_Y; y ++)
    {
        if(mPuzzleGrid[PLL_PUZZLE_MID_X][y]->GetPuzzleType() == tCenterCode)
            mPuzzleGrid[PLL_PUZZLE_MID_X][y]->SetOffsetTick(mSelectionOffsetTick);
        else
            break;
    }
}
