//--Base
#include "CarnationMinimap.h"

//--Classes
#include "CarnationLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "MapManager.h"

///========================================== System ==============================================
CarnationMinimap::CarnationMinimap()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[ ========= Derived ======== ]
    ///--[System]
    ///--[Discovery]
    mDiscoverySizeX = 0;
    mDiscoverySizeY = 0;
    mDiscoveryGrid = NULL;

    ///--[Player]
    mPlayerX = -100;
    mPlayerY = -100;

    ///--[Enemies]
    mIndicatorList = new StarLinkedList(true);

    ///--[Scaling]
    mCenterPointX.MoveTo(0.0f, 0);
    mCenterPointY.MoveTo(0.0f, 0);
    mScalePack.MoveTo(1.0f, 0);

    ///--[Images]
    //--Current Image
    rCurrentMapImg = NULL;
    rLastMapEntry = NULL;
    mMapLayerList = new StarLinkedList(true);

    //--Common Images
    memset(&Images, 0, sizeof(Images));

    ///--[ ================ Construction ================ ]
    ///--[Images]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Images.Data.rUtilityBlackout       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Maps/Utility/Blackout");
    Images.Data.rUtilityBlackoutStrict = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Maps/Utility/BlackoutStrict");
    Images.Data.rUtilityPlayer         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Maps/Utility/Player");
    Images.Data.rUtilityEnemy          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Maps/Utility/Enemy");
    Images.Data.rUtilityEvent          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Maps/Utility/Event");
    Images.Data.rUtilityGate           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Maps/Utility/Gate");
    Images.Data.rUtilityTreasure       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Maps/Utility/Chest");

    ///--[Verify]
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), false);
}
CarnationMinimap::~CarnationMinimap()
{
    delete mIndicatorList;
    for(int x = 0; x < mDiscoverySizeX; x ++) free(mDiscoveryGrid[x]);
    free(mDiscoveryGrid);
    delete mMapLayerList;
}

///================================= CMiniRoomEntry Functions =====================================
void CMiniRoomEntry::Initialize()
{
    mXOffset = 0.0f;
    mYOffset = 0.0f;
    mrImageList = new StarLinkedList(false);
}
void CMiniRoomEntry::Set(float pX, float pY, const char *pDLPath)
{
    //--Offsets.
    mXOffset = pX;
    mYOffset = pY;

    //--First image on the object.
    StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mrImageList->AddElementAsTail("X", rImage);
}
void CMiniRoomEntry::Add(const char *pDLPath)
{
    StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mrImageList->AddElementAsTail("X", rImage);
}
void CMiniRoomEntry::Render()
{
    StarBitmap *rImage = (StarBitmap *)mrImageList->PushIterator();
    while(rImage)
    {
        rImage->Draw(mXOffset, mYOffset);
        rImage = (StarBitmap *)mrImageList->AutoIterate();
    }
}
void CMiniRoomEntry::DeleteThis(void *pPtr)
{
    //--Cast and check.
    CMiniRoomEntry *rPtr = (CMiniRoomEntry *)pPtr;
    if(!rPtr) return;

    //--Deallocate.
    delete rPtr->mrImageList;
    free(rPtr);
}

///===================================== Property Queries =========================================
int CarnationMinimap::GetPlayerX()
{
    return mPlayerX;
}
int CarnationMinimap::GetPlayerY()
{
    return mPlayerY;
}

///======================================= Manipulators ===========================================
void CarnationMinimap::SetPlayerPosition(int pX, int pY)
{
    //--Position player indicator.
    mPlayerX = pX;
    mPlayerY = pY;

    //--Place center point.
    mCenterPointX.MoveTo((mPlayerX * CARNMINIMAP_DISCOVERY_BLOCK_W) + (0.5f * CARNMINIMAP_DISCOVERY_BLOCK_W), CARNMINIMAP_MAP_MOVE_TICKS);
    mCenterPointY.MoveTo((mPlayerY * CARNMINIMAP_DISCOVERY_BLOCK_H) + (0.5f * CARNMINIMAP_DISCOVERY_BLOCK_H), CARNMINIMAP_MAP_MOVE_TICKS);
}
void CarnationMinimap::SetMapImage(const char *pDLPath)
{
    ///--[Documentation and Null Case]
    //--Sets map image using the DLPath and reallocates the discovery grid to a matching size, if found.
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rCurrentMapImg = NULL;
        SetDiscoverySize(0, 0);
        mDiscoveryGrid = NULL;
        return;
    }

    ///--[Error Case]
    //--If the entry does not exist, bark an error. This is probably a mistake by the coder.
    StarBitmap *rCheckEntry = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    if(!rCheckEntry)
    {
        DebugManager::ForcePrint("CarnationMinimap::SetMapImage(): Error, no image found at %s\n", pDLPath);
        return;
    }

    //--If the map is identical to the previous, don't reset any discovery.
    if(rCurrentMapImg == rCheckEntry) return;

    ///--[Success]
    //--Image exists, resolve the needed sizes.
    rCurrentMapImg = rCheckEntry;

    //--Get requisite sizes.
    int tTrueWid = rCurrentMapImg->GetTrueWidth();
    int tTrueHei = rCurrentMapImg->GetTrueHeight();
    SetDiscoverySize(tTrueWid / CARNMINIMAP_DISCOVERY_BLOCK_W, tTrueHei / CARNMINIMAP_DISCOVERY_BLOCK_H);
}
void CarnationMinimap::SetDiscoverySize(int pXSize, int pYSize)
{
    ///--[Documentation]
    //--Allows manual specification of the size of the discovery grid.
    for(int x = 0; x < mDiscoverySizeX; x ++) free(mDiscoveryGrid[x]);
    free(mDiscoveryGrid);

    //--Zero.
    mDiscoverySizeX = 0;
    mDiscoverySizeY = 0;
    mDiscoveryGrid = NULL;
    if(pXSize < 1 || pYSize < 1) return;
    //fprintf(stderr, "Set discovery grid to size %i %i\n", pXSize, pYSize);

    //--Allocate.
    mDiscoverySizeX = pXSize;
    mDiscoverySizeY = pYSize;

    //--Allocate.
    mDiscoveryGrid = (DiscoveryEntry **)starmemoryalloc(sizeof(DiscoveryEntry *) * mDiscoverySizeX);
    for(int x = 0; x < mDiscoverySizeX; x ++)
    {
        mDiscoveryGrid[x] = (DiscoveryEntry *)starmemoryalloc(sizeof(DiscoveryEntry) * mDiscoverySizeY);
        for(int y = 0; y < mDiscoverySizeY; y ++)
        {
            mDiscoveryGrid[x][y].Initialize();
        }
    }
}
void CarnationMinimap::DiscoverPoint(int pX, int pY)
{
    ///--[Documentation]
    //--Uncovers the point specified, if it is within the grid bounds.
    if(pX < 0 || pY < 0 || pX >= mDiscoverySizeX || pY >= mDiscoverySizeY) return;
    mDiscoveryGrid[pX][pY].mIsDiscovered = true;
}
void CarnationMinimap::DiscoverArea(int pLft, int pTop, int pRgt, int pBot)
{
    ///--[Documentation]
    //--Uncovers the rectangle specified, respecting grid boundaries.
    for(int x = pLft; x <= pRgt; x ++)
    {
        for(int y = pTop; y <= pBot; y ++)
        {
            DiscoverPoint(x, y);
        }
    }
}
void CarnationMinimap::CreateMapLayer(float pXOffset, float pYOffset, const char *pDLPath)
{
    ///--[Documentation]
    //--Creates and registers a new map layer.
    if(!pDLPath) return;

    ///--[Execution]
    //--Create, register.
    CMiniRoomEntry *nEntry = (CMiniRoomEntry *)starmemoryalloc(sizeof(CMiniRoomEntry));
    nEntry->Initialize();
    nEntry->Set(pXOffset, pYOffset, pDLPath);
    rLastMapEntry = nEntry;
    mMapLayerList->AddElementAsTail("X", nEntry, &CMiniRoomEntry::DeleteThis);
}
void CarnationMinimap::AddImageToLastLayer(const char *pDLPath)
{
    ///--[Documentation]
    //--Registers a new image to the last registered map layer.
    if(!pDLPath) return;

    ///--[Execution]
    //--First, error check the last map entry. Not only can it not be NULL, but it has
    //  to be on the map layer list. Otherwise it is considered an unstable pointer.
    if(!rLastMapEntry || !mMapLayerList->IsElementOnList(rLastMapEntry)) return;

    //--Append.
    rLastMapEntry->Add(pDLPath);
}
void CarnationMinimap::RegisterIndicator(const char *pName, int pX, int pY, int pCode)
{
    ///--[Documentation]
    //--Registers an indicator at the given location. Indicators can stack, but two indicators
    //  with the same type cannot inhabit the same space. Different types are okay.

    ///--[Duplicate Check]
    //--If the named entry exists, stop.
    void *rCheckPtr = mIndicatorList->GetElementByName(pName);
    if(rCheckPtr)
    {
        fprintf(stderr, "Stopping. Indicator %s already exists.\n", pName);
        return;
    }

    ///--[Create, Register]
    //--No duplicates, create and register a new entry.
    CMiniIndicator *nIndicator = (CMiniIndicator *)starmemoryalloc(sizeof(CMiniIndicator));
    nIndicator->mX = pX;
    nIndicator->mY = pY;
    nIndicator->mCode = pCode;
    mIndicatorList->AddElementAsTail(pName, nIndicator, &FreeThis);

    //--Resolve image.
    if(pCode == CARNMINIMAP_INDICATOR_ENEMY)
    {
        nIndicator->rImage = Images.Data.rUtilityEnemy;
    }
    else if(pCode == CARNMINIMAP_INDICATOR_GATE)
    {
        nIndicator->rImage = Images.Data.rUtilityGate;
    }
    else if(pCode == CARNMINIMAP_INDICATOR_TREASURE)
    {
        nIndicator->rImage = Images.Data.rUtilityTreasure;
    }
    else if(pCode == CARNMINIMAP_INDICATOR_EVENT)
    {
        nIndicator->rImage = Images.Data.rUtilityEvent;
    }
}
void CarnationMinimap::RemoveIndicator(const char *pName)
{
    ///--[Documentation]
    //--Remove the indicator with the given name.
    mIndicatorList->RemoveElementS(pName);
}

///======================================= Core Methods ===========================================
void CarnationMinimap::ClearMapLayers()
{
    mMapLayerList->ClearList();
}
void CarnationMinimap::ClearIndicators()
{
    mIndicatorList->ClearList();
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void CarnationMinimap::Update()
{
    ///--[Timers]
    //--Run across the discovery grid and update the timers.
    for(int x = 0; x < mDiscoverySizeX; x ++)
    {
        for(int y = 0; y < mDiscoverySizeY; y ++)
        {
            if(mDiscoveryGrid[x][y].mIsDiscovered && mDiscoveryGrid[x][y].mTimer < CARNMINIMAP_DISCOVER_TICKS)
            {
                mDiscoveryGrid[x][y].mTimer ++;
            }
        }
    }

    //--Easing packages.
    mCenterPointX.Increment(EASING_CODE_QUADOUT);
    mCenterPointY.Increment(EASING_CODE_QUADOUT);
    mScalePack.Increment(EASING_CODE_QUADOUT);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnationMinimap::Render(float pAlpha)
{
    ///--[Documentation]
    //--Renders the object in its entirety, using the provided global alpha.

    ///--[Positioning]
    //--Reposition the rendering around the "center" point, which is usually the player.
    if(mScalePack.mXCur < 0.01f) return;
    float cScaleInv = 1.0f / mScalePack.mXCur;

    //--Adjust scale.
    glScalef(mScalePack.mXCur, mScalePack.mXCur, 1.0f);

    //--Center.
    glTranslatef(-mCenterPointX.mXCur, -mCenterPointY.mXCur, 0.0f);

    //--Render.
    RenderBase(pAlpha);
    RenderShadowing(pAlpha);

    ///--[Clean]
    //--Adjust scale.
    glTranslatef(mCenterPointX.mXCur, mCenterPointY.mXCur, 0.0f);
    glScalef(cScaleInv, cScaleInv, 1.0f);
}
void CarnationMinimap::RenderBase(float pAlpha)
{
    ///--[Documentation]
    //--The actual rendering component of the rendering routine. This is used for pull-out maps in the UI as opposed
    //  to being the minimap.
    if(!Images.mIsReady || pAlpha <= 0.0f) return;

    ///--[Basic]
    //--Render the basic map object.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    if(rCurrentMapImg) rCurrentMapImg->Draw();

    ///--[Rooms]
    //--Render all sub-components.
    CMiniRoomEntry *rEntry = (CMiniRoomEntry *)mMapLayerList->PushIterator();
    while(rEntry)
    {
        rEntry->Render();
        rEntry = (CMiniRoomEntry *)mMapLayerList->AutoIterate();
    }

    ///--[Indicators]
    //--Render player indicator.
    Images.Data.rUtilityPlayer->Draw(mPlayerX * CARNMINIMAP_DISCOVERY_BLOCK_W, mPlayerY * CARNMINIMAP_DISCOVERY_BLOCK_H);

    //--Render enemy indicators.
    CMiniIndicator *rIndicator = (CMiniIndicator *)mIndicatorList->PushIterator();
    while(rIndicator)
    {
        //--Render.
        if(rIndicator->rImage)
        {
            rIndicator->rImage->Draw(rIndicator->mX * CARNMINIMAP_DISCOVERY_BLOCK_W, rIndicator->mY * CARNMINIMAP_DISCOVERY_BLOCK_H);
        }

        //--Next.
        rIndicator = (CMiniIndicator *)mIndicatorList->AutoIterate();
    }

    ///--[Finish Up]
    //--Clean.
    StarlightColor::ClearMixer();
}
void CarnationMinimap::RenderShadowing(float pAlpha)
{
    ///--[Documentation]
    //--Renders the shadowing overlay which hides the map.
    if(!Images.mIsReady || pAlpha <= 0.0f) return;

    ///--[Execution]
    for(int x = 0; x < mDiscoverySizeX; x ++)
    {
        for(int y = 0; y < mDiscoverySizeY; y ++)
        {
            //--Skip if this tile is fully uncovered.
            if(mDiscoveryGrid[x][y].mTimer >= CARNMINIMAP_DISCOVER_TICKS) continue;

            //--Resolve alpha.
            float tLocalAlpha = (1.0f - EasingFunction::QuadraticOut(mDiscoveryGrid[x][y].mTimer, CARNMINIMAP_DISCOVER_TICKS)) * pAlpha;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tLocalAlpha);
            Images.Data.rUtilityBlackout->Draw(x * CARNMINIMAP_DISCOVERY_BLOCK_W, y * CARNMINIMAP_DISCOVERY_BLOCK_H);
        }
    }

    //--Clean.
    StarlightColor::ClearMixer();
}
void CarnationMinimap::RenderShadowingStrict()
{
    ///--[Documentation]
    //--Renders the shadowing overlay which hides the map, using the "Strict" version of the image.
    //  This is used for stencils and isn't supposed to render any colors.
    if(!Images.mIsReady) return;

    ///--[Execution]
    for(int x = 0; x < mDiscoverySizeX; x ++)
    {
        for(int y = 0; y < mDiscoverySizeY; y ++)
        {
            //--Skip if this tile is fully uncovered.
            if(mDiscoveryGrid[x][y].mTimer >= CARNMINIMAP_DISCOVER_TICKS) continue;

            //--Render.
            Images.Data.rUtilityBlackoutStrict->Draw(x * CARNMINIMAP_DISCOVERY_BLOCK_W, y * CARNMINIMAP_DISCOVERY_BLOCK_H);
        }
    }

    //--Outside edges.
    for(int x = -1; x < mDiscoverySizeX+1; x ++)
    {
        Images.Data.rUtilityBlackoutStrict->Draw(x * CARNMINIMAP_DISCOVERY_BLOCK_W, CARNMINIMAP_DISCOVERY_BLOCK_H * -1.0f);
        Images.Data.rUtilityBlackoutStrict->Draw(x * CARNMINIMAP_DISCOVERY_BLOCK_W, CARNMINIMAP_DISCOVERY_BLOCK_H * mDiscoverySizeY);
    }
    for(int y = 0; y < mDiscoverySizeY; y ++)
    {
        Images.Data.rUtilityBlackoutStrict->Draw(CARNMINIMAP_DISCOVERY_BLOCK_W * -1.0f,           y * CARNMINIMAP_DISCOVERY_BLOCK_H);
        Images.Data.rUtilityBlackoutStrict->Draw(CARNMINIMAP_DISCOVERY_BLOCK_W * mDiscoverySizeX, y * CARNMINIMAP_DISCOVERY_BLOCK_H);
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
CarnationMinimap *CarnationMinimap::Fetch()
{
    //--Can legally return NULL, there must be a valid CarnationLevel which has a minimap.
    RootLevel *rActiveLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rActiveLevel || !rActiveLevel->IsOfType(POINTER_TYPE_CARNATIONLEVEL)) return NULL;

    //--Return minimap from the level.
    CarnationLevel *rCarnLevel = (CarnationLevel *)rActiveLevel;
    return rCarnLevel->GetMinimap();
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
