//--Base
#include "AdvUISkills.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "StringEntry.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "Definitions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
void AdvUISkills::SetToProfilesMode()
{
    ///--[Error Check]
    //--Cannot do anything if the character doesn't resolve.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    ///--[Variables]
    mPrevMode = mMode;
    mIsProfilesMode = true;
    mProfileCursor = 0;
    mProfileSkip = 0;
    mProfileJobCursor = -1;
    mMode = ADVMENU_SKILLS_MODE_PROFILES_SELECT;

    ///--[Construction]
    //--Set cursor.
    RecomputeProfilesCursorPos();

    //--Selected character must refresh all profile pointers.
    StarLinkedList *rProfilesList = rCurrentCharacter->GetProfilesList();
    AbilityProfile *rProfile = (AbilityProfile *)rProfilesList->PushIterator();
    while(rProfile)
    {
        rProfile->ReresolveAbilityPointers(rCurrentCharacter);
        rProfile = (AbilityProfile *)rProfilesList->AutoIterate();
    }
}

///===================================== Property Queries =========================================
///======================================== Core Methods ==========================================
void AdvUISkills::RecomputeProfilesCursorPos()
{
    ///--[Documentation]
    //--Determines the position for the cursor and sets it, based on the current mode.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = 0.0f;
    float cBot = 0.0f;

    //--Resolve active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Selecting a profile.
    if(mMode == ADVMENU_SKILLS_MODE_PROFILES_SELECT)
    {
        //--Job cursor is -1, therefore, use the name as width.
        if(mProfileJobCursor == -1)
        {
            //--Constants.
            float cNameLft =  64.0f;
            float cNameTop = 148.0f;
            float cNameHei =  26.0f;

            //--Get selected profile.
            const char *rProfileName = rCurrentCharacter->GetNameOfProfileI(mProfileCursor);
            if(!rProfileName) return;

            //--Set.
            cLft = cNameLft - 3.0f;
            cTop = cNameTop + (cNameHei * mProfileCursor);
            cRgt = cNameLft + (AdvImages.rFont_Mainline->GetTextWidth(rProfileName)) + 3.0f;
            cBot = cNameTop + (cNameHei * mProfileCursor) + cNameHei - 2.0f;
        }
        //--In the job highlights.
        else
        {
            //--Position setup.
            float cIconLft = 347.0f;
            float cIconTop = 148.0f;
            float cHeight  =  26.0f;
            float cIconW   =  26.0f;
            float cIconSpc =  28.0f;

            //--Set.
            cLft = cIconLft + (cIconSpc * mProfileJobCursor) + 0.0f;
            cTop = cIconTop + (cHeight  * mProfileCursor)    + 0.0f;
            cRgt = cIconLft + (cIconSpc * mProfileJobCursor) + cIconW  + 0.0f;
            cBot = cIconTop + (cHeight  * mProfileCursor)    + cHeight + 0.0f;
        }
    }

    //--Set.
    mHighlightPos.MoveTo(cLft, cTop, ADVMENU_SKILLS_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_SKILLS_CURSOR_TICKS);
}

///=========================================== Update =============================================
bool AdvUISkills::UpdateProfilesMode(bool pDisallowControls)
{
    ///--[Documentation]
    //--A submode of the Skills UI, this allows the player to select a profile for use or editing.
    //  This update always runs, but will not handle controls if the flag pDisallowControls is true.

    ///--[Timers]
    //--Basic visiblity.
    StandardTimer(mProfilesVisibilityTimer, 0, ADVMENU_SKILLS_PROFILE_VIS_TICKS, (mIsProfilesMode));

    ///--[Error Check]
    if(pDisallowControls) return false;

    ///--[Sub Handlers]
    if(mMode == ADVMENU_SKILLS_MODE_PROFILES_SELECT)
    {
        UpdateProfilesSelectMode();
    }
    else if(mMode == ADVMENU_SKILLS_MODE_PROFILES_NEW)
    {
        UpdateProfilesNewMode();
    }
    else if(mMode == ADVMENU_SKILLS_MODE_PROFILES_RENAME)
    {
        UpdateProfilesRenameMode();
    }

    //--Handled update.
    return true;
}
void AdvUISkills::UpdateProfilesSelectMode()
{
    ///--[Documentation]
    //--Player is selecting a profile.
    bool tRecomputeCursorPosition = false;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Get their profile list.
    StarLinkedList *rProfileList = rCurrentCharacter->GetProfilesList();

    ///--[Deletion]
    //--If this is true, redirect input to deleting the selected profile.
    if(mIsProfileConfirm)
    {
        //--Activate, confirm.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Flag.
            mIsProfileConfirm = false;

            //--Get the name of the profile in question. Any jobs pointing to it move to "Default".
            const char *rProfileName = rProfileList->GetNameOfElementBySlot(mProfileCursor);
            StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();
            AdvCombatJob *rJob = (AdvCombatJob *)rJobList->PushIterator();
            while(rJob)
            {
                //--Name compare.
                const char *rAssociatedProfileName = rJob->GetAssociatedProfileName();
                if(!strcasecmp(rAssociatedProfileName, rProfileName))
                {
                    rJob->SetAssociatedProfileName("Default");
                }

                //--Next.
                rJob = (AdvCombatJob *)rJobList->AutoIterate();
            }

            //--Delete it.
            rProfileList->RemoveElementI(mProfileCursor);
            mProfileCursor --;

            //--Cursor.
            RecomputeProfilesCursorPos();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsProfileConfirm = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return;
    }

    ///--[Mode Changing]
    //--Run button, create a new profile.
    if(rControlManager->IsFirstPress("Run"))
    {
        //--If the profiles list already has 100 entries, fail.
        if(rProfileList->GetListSize() >= 100) return;

        //--Change modes.
        mMode = ADVMENU_SKILLS_MODE_PROFILES_NEW;

        //--Reset password form.
        mStringEntryForm->SetCompleteFlag(false);
        mStringEntryForm->SetPromptString("Enter Profile Name");
        mStringEntryForm->SetString(NULL);
        mStringEntryForm->SetEmptyString("Enter Profile Name");

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    //--Jump button, renames a profile.
    if(rControlManager->IsFirstPress("Jump"))
    {
        //--Fails if it's on the default profile, which is always the zero profile.
        if(mProfileCursor == 0)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Begin rename mode.
        mMode = ADVMENU_SKILLS_MODE_PROFILES_RENAME;

        //--Reset password form.
        mStringEntryForm->SetCompleteFlag(false);
        mStringEntryForm->SetPromptString("Rename Profile");
        mStringEntryForm->SetString(NULL);
        mStringEntryForm->SetEmptyString("Enter New Profile Name");

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    //--Field Abilities button, deletes a profile.
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--Fails if it's on the default profile. Can never delete that one.
        if(mProfileCursor == 0)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Activate deletion mode.
        mIsProfileConfirm = true;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Cursor Handling]
    if(AutoListUpDn(mProfileCursor, mProfileSkip, rProfileList->GetListSize(), cxProfileScrollBuf, cxProfilePageSize, true)) return;

    /*
    //--Up. Decrements job cursor. Wraps.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursor(mProfileCursor, mProfileSkip, tIterations, rProfileList->GetListSize(), ADVMENU_SKILLS_PROFILE_SCROLL_RANGE, ADVMENU_SKILLS_MAX_PROFILES_PER_PAGE, true);
    }
    //--Down. Increments cursor. Wraps.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Handler.
        tRecomputeCursorPosition = HandleCursor(mProfileCursor, mProfileSkip, tIterations, rProfileList->GetListSize(), ADVMENU_SKILLS_PROFILE_SCROLL_RANGE, ADVMENU_SKILLS_MAX_PROFILES_PER_PAGE, true);
    }*/

    //--Left. Decrements job cursor. Wraps.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Flag.
        tRecomputeCursorPosition = true;

        //--List Cap.
        StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();
        int tCursorCap = rJobList->GetListSize();

        //--Speed increase.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Iterate.
        for(int i = 0; i < tIterations * -1; i ++)
        {
            //--Set.
            mProfileJobCursor --;

            //--Wrap if needed. This only occurs on the first pass, otherwise the list clamps.
            if(mProfileJobCursor < -1 && i == 0)
            {
                mProfileJobCursor = tCursorCap - 1;
                break;
            }
            //--Just clamp on successive passes, or if wrapping is disallowed.
            else if(mProfileJobCursor < -1)
            {
                mProfileJobCursor = -1;
                break;
            }
        }
    }
    //--Right. Increments job cursor. Wraps.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Flag.
        tRecomputeCursorPosition = true;

        //--List Cap.
        StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();
        int tCursorCap = rJobList->GetListSize();

        //--Speed increase.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Iterate.
        for(int i = 0; i < tIterations; i ++)
        {
            //--Set.
            mProfileJobCursor ++;

            //--Wrap if needed. This only occurs on the first pass, otherwise the list clamps.
            if(mProfileJobCursor >= tCursorCap && i == 0)
            {
                mProfileJobCursor = -1;
                break;
            }
            //--Just clamp on successive passes, or if wrapping is disallowed.
            else if(mProfileJobCursor >= tCursorCap)
            {
                mProfileJobCursor = tCursorCap - 1;
                break;
            }
        }
    }

    //--Cursor.
    if(tRecomputeCursorPosition)
    {
        RecomputeProfilesCursorPos();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Activate, Cancel]
    //--Activate. Sets the highlighted profile as active, or changes the job's associated profile.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If this cursor is at -1, set the selected profile as active.
        if(mProfileJobCursor == -1)
        {
            rCurrentCharacter->SetActiveProfileI(mProfileCursor);
        }
        //--Not at -1. Change's job's associated profile.
        else
        {
            //--Get the name of the profile in this slot.
            const char *rProfileName = rCurrentCharacter->GetNameOfProfileI(mProfileCursor);

            //--Get the job in question.
            StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();
            AdvCombatJob *rJob = (AdvCombatJob *)rJobList->GetElementBySlot(mProfileJobCursor);
            if(rJob)
            {
                rJob->SetAssociatedProfileName(rProfileName);
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    //--Cancel, returns to skills menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mMode = mPrevMode;
        mPrevMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
        mIsProfilesMode = false;
        RecomputeCursorPositions();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdvUISkills::UpdateProfilesNewMode()
{
    ///--[Documentation]
    //--Handles update when registering a new profile.
    mStringEntryForm->Update();

    //--Check for completion.
    if(mStringEntryForm->IsComplete())
    {
        ///--[Setup]
        //--Get active character.
        AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
        if(!rCurrentCharacter) return;

        //--Get their profile list.
        StarLinkedList *rProfileList = rCurrentCharacter->GetProfilesList();

        ///--[Flags]
        //--Flag.
        mMode = ADVMENU_SKILLS_MODE_PROFILES_SELECT;

        ///--[Error Check]
        //--Get the string entered. It must be at least one character long.
        const char *rEnteredString = mStringEntryForm->GetString();
        if(strlen(rEnteredString) < 1) return;

        //--Check if the name already exists. If so, fail.
        if(rProfileList->GetElementByName(rEnteredString))
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        ///--[Create New]
        //--All checks passed, create a new package and register it to the list.
        AbilityProfile *nProfile = (AbilityProfile *)starmemoryalloc(sizeof(AbilityProfile));
        nProfile->Initialize();
        rProfileList->AddElementAsTail(rEnteredString, nProfile, &AbilityProfile::DeleteThis);

        ///--[SFX]
        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Cancelled out.
    else if(mStringEntryForm->IsCancelled())
    {
        mMode = ADVMENU_SKILLS_MODE_PROFILES_SELECT;
    }
}
void AdvUISkills::UpdateProfilesRenameMode()
{
    ///--[Documentation]
    //--Handles text input. Uses the string entry form.
    mStringEntryForm->Update();

    //--Check for completion.
    if(mStringEntryForm->IsComplete())
    {
        ///--[Setup]
        //--Get active character.
        AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
        if(!rCurrentCharacter) return;

        //--Get their profile list.
        StarLinkedList *rProfileList = rCurrentCharacter->GetProfilesList();

        ///--[Flags]
        //--Flag.
        mMode = ADVMENU_SKILLS_MODE_PROFILES_SELECT;

        ///--[Error Check]
        //--Get the string entered. It must be at least one character long.
        const char *rEnteredString = mStringEntryForm->GetString();
        if(strlen(rEnteredString) < 1) return;

        //--Check if the name already exists. If so, fail.
        if(rProfileList->GetElementByName(rEnteredString))
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        ///--[Rename]
        //--Get selected profile.
        AbilityProfile *rSelectedProfile = (AbilityProfile *)rProfileList->GetElementBySlot(mProfileCursor);
        if(!rSelectedProfile) return;

        //--Get the whole entry. Reset the name.
        rProfileList->SetRandomPointerToThis(rSelectedProfile);
        StarLinkedListEntry *rEntry = rProfileList->GetRandomPointerWholeEntry();
        ResetString(rEntry->mName, rEnteredString);

        //--Reset the cursor.
        RecomputeProfilesCursorPos();

        ///--[SFX]
        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Cancelled out.
    else if(mStringEntryForm->IsCancelled())
    {
        mMode = ADVMENU_SKILLS_MODE_PROFILES_SELECT;
    }
}

///========================================== Drawing =============================================
void AdvUISkills::RenderProfilesMode()
{
    ///--[Documentation]
    //--Renders profiles mode. This occurs over the skill menu, which may be rendering at the same time.
    if(mProfilesVisibilityTimer < 1) return;

    //--Option. Selects between fading and sliding.
    bool tUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Timer]
    //--Resolve timer offset.
    float cAlpha = EasingFunction::QuadraticInOut(mProfilesVisibilityTimer, (float)ADVMENU_SKILLS_PROFILE_VIS_TICKS);

    ///--[Setup]
    //--If the UI is using translate logic, set the alpha to 1.0f.
    if(!tUseFade) cAlpha = 1.0f;

    //--Reset mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Get active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Get their profile list.
    StarLinkedList *rProfileList = rCurrentCharacter->GetProfilesList();

    ///============================= Left Block ===============================
    ///============================= Top Block ================================
    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_UP, cAlpha);

    //--Header.
    AdvImages.rProfilesHeader->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 10.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Profiles");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Profiles Block.
    AdvImages.rProfilesFrame->Draw();

    //--Headings for profiles block.
    mColorHeading.SetAsMixerAlpha(cAlpha);
    AdvImages.rFont_Heading->DrawText( 64.0f, 108.0f, 0, 1.0f, "Name");
    AdvImages.rFont_Heading->DrawText(348.0f, 108.0f, 0, 1.0f, "Jobs Using");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Setup.
    float cY = 148.0f;
    float cH =  26.0f;

    //--Profile Listing
    int i = 0;
    AbilityProfile *rProfile = (AbilityProfile *)rProfileList->PushIterator();
    while(rProfile)
    {
        //--Call subroutine.
        RenderProfile(cY + (cH * i), rProfileList->GetIteratorName(), (i % 2 == 1), rProfile, cAlpha);

        //--Next.
        i ++;
        rProfile = (AbilityProfile *)rProfileList->AutoIterate();
    }

    //--Render details after the line color backing.
    AdvImages.rProfilesDetail->Draw();
    AdvImages.rProfilesScrollbar->Draw();

    //--Strings, top left.
    mShowHelpString->  DrawText(0.0f, cxMainlineH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollFastString->DrawText(0.0f, cxMainlineH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Highlight.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();

    ///============================ Right Block ===============================
    ///============================ Bottom Block ==============================
    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_DOWN, cAlpha);

    //--Memorized Skills frame.
    AdvImages.rProfilesMemorized->Draw();

    //--Instructions.
    AdvImages.rProfilesInstructionsFrame->Draw();

    //--Get the profile, render its ability icons.
    AbilityProfile *rHighlightedProfile = (AbilityProfile *)rProfileList->GetElementBySlot(mProfileCursor);
    RenderProfileAbilities(rHighlightedProfile);

    //--Instruction Text.
    for(int i = 0; i < ADVMENU_SKILLS_INSTRUCTION_STRINGS_TOTAL; i ++)
    {
        mInstructionString[i]->DrawText(367.0f, 548.0f + (cxMainlineH * i), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }

    //--Strings, bottom left.
    float cYPos = VIRTUAL_CANVAS_Y - cxHelpSymbolH;
    mNewProfileString   ->DrawText(0.0f, cYPos - (cxMainlineH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mRenameProfileString->DrawText(0.0f, cYPos - (cxMainlineH * 0.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Strings, bottom right.
    mDeleteProfileString->DrawText(VIRTUAL_CANVAS_X, cYPos - (cxMainlineH * 0.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();

    ///============================ No Blocking ===============================
    ///--[Name Entry]
    RenderProfileNameEntry();

    ///--[Confirm Deletion]
    float cConfirmAlpha = EasingFunction::QuadraticInOut(mProfileConfirmTimer, (float)ADVMENU_SKILLS_PROFILE_VIS_TICKS);
    if(mProfileConfirmTimer > 0)
    {
        //--Darken.
        StarBitmap::DrawFullBlack(cConfirmAlpha * 0.70f);

        //--Offset.
        if(tUseFade) cConfirmAlpha = 1.0f;
        AutoPieceOpen(DIR_UP, cAlpha);
        //HandleFadeOrTranslate(tUseFade, 0.0f, cConfirmOffset * 1.0f, cAlpha);

        //--Render backing. Borrows the buy confirm UI.
        AdvImages.rBuySkillPopup->Draw();

        //--Heading.
        mColorHeading.SetAsMixerAlpha(cConfirmAlpha);
        AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Confirm Deletion");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cConfirmAlpha);

        //--Construct buffer.
        float cTxtLft = (VIRTUAL_CANVAS_X * 0.50f);
        float cTxtTop = 223.0f;
        const char *rProfileName = rProfileList->GetNameOfElementBySlot(mProfileCursor);
        AdvImages.rFont_Mainline->DrawTextArgs(cTxtLft, cTxtTop, SUGARFONT_AUTOCENTER_X, 1.0f, "Really delete %s?", rProfileName);

        ///--[Strings]
        mConfirmDeleteString->  DrawText(376.0f, 285.0f,                          SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mCancelPurchaseString-> DrawText(987.0f, 285.0f, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

        //--Clean.
        //HandleFadeOrTranslate(tUseFade, 0.0f, cConfirmOffset * -1.0f, cAlpha);
        AutoPieceClose();
    }

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUISkills::RenderProfile(float pY, const char *pName, bool pRenderBacking, AbilityProfile *pProfile, float pAlpha)
{
    ///--[Documentation]
    //--Given a profile, renders its name, which classes are using it, and backings. Doesn't render the associated
    //  abilities yet.
    if(!pName || !pProfile) return;

    //--Get active character.
    AdvCombatEntity *rCurrentCharacter = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rCurrentCharacter) return;

    //--Active profile.
    AbilityProfile *rActiveProfile = rCurrentCharacter->GetActiveProfile();

    ///--[Basic Data]
    //--Position setup.
    float cNameX    =  64.0f;
    float cActiveX  = 339.0f;
    float cIconX    = 347.0f;
    float cHeight   =  26.0f;
    float cIconW    =  26.0f;
    float cIconSpc  =  28.0f;
    float cIconYOff =   0.0f;

    //--If this flag is set, render a backing.
    if(pRenderBacking)
    {
        StarBitmap::DrawRectFill(45.0f, pY, 1320.0f, pY + cHeight, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    //--Name.
    AdvImages.rFont_Mainline->DrawText(cNameX, pY + 0.0f, 0, 1.0f, pName);

    //--If this is the active profile, render "Active" near it in blue.
    if(rActiveProfile == pProfile)
    {
        StarlightColor::SetMixer(0.5f, 1.0f, 1.0f, pAlpha);
        AdvImages.rFont_Mainline->DrawText(cActiveX, pY + 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Active");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    ///--[Jobs]
    //--Iterate across all jobs held by the owning character. Retrieve the profile they use as a default, and if it's
    //  this one, render that job's icon.
    StarLinkedList *rJobList = rCurrentCharacter->GetJobSkillUIList();

    //--Setup.
    float tIconCurX = cIconX;
    TwoDimensionReal tFaceTableDim;

    //--Iterate.
    AdvCombatJob *rJob = (AdvCombatJob *)rJobList->PushIterator();
    while(rJob)
    {
        //--Get name.
        const char *rJobProfileName = rJob->GetAssociatedProfileName();
        if(!strcasecmp(rJobProfileName, pName))
        {
            //--Get positions and images.
            StarBitmap *rImage = rJob->GetFaceProperties(tFaceTableDim);

            //--Render.
            if(rImage)
            {
                //--Flip coordinates.
                tFaceTableDim.mLft = tFaceTableDim.mLft;
                tFaceTableDim.mTop = 1.0f - (tFaceTableDim.mTop);
                tFaceTableDim.mRgt = tFaceTableDim.mRgt;
                tFaceTableDim.mBot = 1.0f - (tFaceTableDim.mBot);

                //--Render.
                rImage->Bind();
                glBegin(GL_QUADS);
                    glTexCoord2f(tFaceTableDim.mLft, tFaceTableDim.mTop); glVertex2f(tIconCurX,          pY + cIconYOff);
                    glTexCoord2f(tFaceTableDim.mRgt, tFaceTableDim.mTop); glVertex2f(tIconCurX + cIconW, pY + cIconYOff);
                    glTexCoord2f(tFaceTableDim.mRgt, tFaceTableDim.mBot); glVertex2f(tIconCurX + cIconW, pY + cIconYOff + cIconW);
                    glTexCoord2f(tFaceTableDim.mLft, tFaceTableDim.mBot); glVertex2f(tIconCurX,          pY + cIconYOff + cIconW);
                glEnd();
            }
        }

        //--Advance X position.
        tIconCurX = tIconCurX + cIconSpc;

        //--Next.
        rJob = (AdvCombatJob *)rJobList->AutoIterate();
    }
}
void AdvUISkills::RenderProfileAbilities(AbilityProfile *pProfile)
{
    ///--[Documentation]
    //--Given an ability profile, renders the abilities in the bottom left memorization window.
    if(!pProfile) return;

    //--Constants.
    float cMemorizedSkillX =  61.0f;
    float cMemorizedSkillY = 547.0f;
    float cMemorizedSkillW =  53.0f;
    float cMemorizedSkillH =  53.0f;

    //--It is expected that the profile has already synchronized its ability pointers. Be sure to do this
    //  before rendering.
    for(int x = 0; x < ACE_ABILITY_GRID_PROFILE_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_PROFILE_SIZE_Y; y ++)
        {
            //--Ability does not exist, skip:
            if(!pProfile->rAbilityPtrs[x][y]) continue;

            //--Compute position.
            float cRenderX = cMemorizedSkillX + (cMemorizedSkillW * x);
            float cRenderY = cMemorizedSkillY + (cMemorizedSkillH * y);
            if(y > 1) cRenderY = cRenderY + 8.0f;

            //--Render.
            pProfile->rAbilityPtrs[x][y]->RenderAt(cRenderX, cRenderY);
        }
    }
}
void AdvUISkills::RenderProfileNameEntry()
{
    ///--[Documentation]
    //--Renders the string entry form over the profile UI.
    if(mStringEntryTimer < 1) return;

    //--Percentage.
    float cPercent = EasingFunction::QuadraticInOut(mStringEntryTimer, ADVMENU_SKILLS_STRING_VIS_TICKS);

    //--Offsets.
    float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cPercent * 0.7f));

    ///--[Position, Render]
    //--Position.
    glTranslatef(0.0f, cBotOffset, 0.0f);

    //--Render password entry.
    mStringEntryForm->Render(1.0f);

    //--Clean.
    glTranslatef(0.0f, -cBotOffset, 0.0f);
}
