//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "AdvUIJournalTypes.h"

//--Libraries
//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
void AdvUIJournal::SetJournalToParagonsMode()
{
    //--Clear the list.
    ClearParagons();

    //--Variables.
    mCurrentMode = ADVMENU_JOURNAL_MODE_PARAGONS;

    //--Run a script to set the paragon's properties.
    LuaManager::Fetch()->ExecuteLuaFile(xParagonResolveScript);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIJournal::CreateParagonEntry(const char *pInternalName)
{
    //--Arg check.
    if(!pInternalName) return;

    //--Duplicate check.
    if(mParagonEntries->GetElementByName(pInternalName)) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuParagonEntry *nEntry = (AdvMenuParagonEntry *)starmemoryalloc(sizeof(AdvMenuParagonEntry));
    nEntry->Initialize();
    ResetString(nEntry->mInternalName, pInternalName);
    mParagonEntries->AddElementAsTail(pInternalName, nEntry, &AdvMenuParagonEntry::DeleteThis);
}
void AdvUIJournal::SetParagonDisplayName(const char *pInternalName, const char *pDisplayName)
{
    //--Error check.
    AdvMenuParagonEntry *rEntry = (AdvMenuParagonEntry *)mParagonEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDisplayName) return;

    //--Set.
    ResetString(rEntry->mDisplayName, pDisplayName);
}
void AdvUIJournal::SetParagonIsEnabled(const char *pInternalName, bool pIsEnabled)
{
    //--Error check.
    AdvMenuParagonEntry *rEntry = (AdvMenuParagonEntry *)mParagonEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mIsEnabled = pIsEnabled;
}
void AdvUIJournal::SetParagonKOCounts(const char *pInternalName, int pKOCurrent, int pKONeeded)
{
    //--Error check.
    AdvMenuParagonEntry *rEntry = (AdvMenuParagonEntry *)mParagonEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mKOCount = pKOCurrent;
    rEntry->mKORequired = pKONeeded;
}
void AdvUIJournal::SetParagonHasDefeatedActivator(const char *pInternalName, bool pDefeatedActivator)
{
    //--Error check.
    AdvMenuParagonEntry *rEntry = (AdvMenuParagonEntry *)mParagonEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mHasDefeatedEnabledParagon = pDefeatedActivator;
}
void AdvUIJournal::SetParagonEnabledPath(const char *pInternalName, const char *pPath)
{
    //--Error check.
    AdvMenuParagonEntry *rEntry = (AdvMenuParagonEntry *)mParagonEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pPath) return;

    //--Set.
    strncpy(rEntry->mEnabledFlagPath, pPath, 255);
}
void AdvUIJournal::ClearParagons()
{
    mParagonEntries->ClearList();
}

///======================================= Core Methods ===========================================
void AdvUIJournal::ComputeParagonCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mParagonCursor - mParagonOffset;

    //--Note: Paragons mode is offset down by 1.
    tCursor ++;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mParagonEntries->GetElementBySlot(mParagonCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = AdvImages.rFont_Mainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIJournal::RenderParagonsMode(float pVisAlpha)
{
    ///--[Paragon Selection]
    //--This listing is handled differently than the others, as the entries are toggleable options.
    StarFont *rUseFont = AdvImages.rFont_Mainline;

    //--Render Constants
    float cSelectionLft = 47.0f;
    float cSelectionTop = 112.0f;
    float cSelectionHei = rUseFont->GetTextHeight();

    //--Variables
    int tOption = 0;
    float tYPos = cSelectionTop + cSelectionHei;

    ///--[Help Text]
    //--Toggle.
    mEnableParagonString->DrawText(40.0f, 50.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Stats);

    ///--[Scrollbar]
    //--If there are over ADVMENU_JOURNAL_ENTRIES_ON_LIST entries, we need a scrollbar.
    if(mParagonEntries->GetListSize() > ADVMENU_JOURNAL_ENTRIES_ON_LIST)
    {
        //--Move selection over.
        cSelectionLft = cSelectionLft + 35.0f;

        //--Render. We move over by 418 pixels to left-align the scrollbar.
        glTranslatef(-418.0f, 0.0f, 0.0f);
        RenderScrollbar(mParagonOffset, ADVMENU_JOURNAL_ENTRIES_ON_LIST, mParagonEntries->GetListSize(), 142.0f, 522.0f, true, AdvImages.rScrollbarFront, AdvImages.rScrollbarFront);
        glTranslatef(418.0f, 0.0f, 0.0f);
    }

    ///--[Headers]
    //--Note that the heading variables depend on the scrollbar. If the scrollbar exist, they shift right a bit.
    float cEnabledLft   = cSelectionLft + 353.0f;
    float cKOCountsLft  = cSelectionLft + 503.0f;
    float cActivatorLft = cSelectionLft + 673.0f;
    float cHeadingsTop  = cSelectionTop;
    rUseFont->DrawText(cEnabledLft,   cHeadingsTop, 0, 1.0f, "Is Enabled");
    rUseFont->DrawText(cKOCountsLft,  cHeadingsTop, 0, 1.0f, "KO Counts");
    rUseFont->DrawText(cActivatorLft, cHeadingsTop, 0, 1.0f, "Activator Defeated");

    ///--[Iteration]
    //--Iteration variables:
    int tRenders = 0;

    //--Iterate across entries:
    AdvMenuParagonEntry *rEntry = (AdvMenuParagonEntry *)mParagonEntries->PushIterator();
    while(rEntry)
    {
        //--If we haven't reached the start-at value, don't render anything.
        if(tOption < mParagonOffset)
        {
            tOption ++;
            rEntry = (AdvMenuParagonEntry *)mParagonEntries->AutoIterate();
            continue;
        }

        //--Backing.
        if(tOption % 2 == 0)
        {
            StarBitmap::DrawRectFill(42.0f, tYPos + 2.0f, 1323.0f, tYPos + cSelectionHei + 2.0f, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pVisAlpha));
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);
        }

        //--Render.
        tRenders ++;
        RenderMenuOption(cSelectionLft, tYPos, rUseFont, pVisAlpha, tOption, mParagonCursor, rEntry->mDisplayName);

        //--Skip if this is just dashes:
        if(!strcasecmp(rEntry->mDisplayName, "-----"))
        {
            tOption ++;
            tYPos = tYPos + cSelectionHei;
            rEntry = (AdvMenuParagonEntry *)mParagonEntries->AutoIterate();
            continue;
        }

        //--Is Enabled:
        if(rEntry->mIsEnabled && rEntry->mHasDefeatedEnabledParagon)
        {
            rUseFont->DrawText(cEnabledLft, tYPos, 0, 1.0f, "Enabled");
        }
        else if(rEntry->mHasDefeatedEnabledParagon)
        {
            rUseFont->DrawText(cEnabledLft, tYPos, 0, 1.0f, "Disabled");
        }

        //--KO Counts:
        rUseFont->DrawTextArgs(cKOCountsLft + 57.0f, tYPos, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rEntry->mKOCount);
        rUseFont->DrawTextArgs(cKOCountsLft + 60.0f, tYPos, 0, 1.0f, "/", rEntry->mKOCount);
        rUseFont->DrawTextArgs(cKOCountsLft + 71.0f, tYPos, 0, 1.0f, "%i", rEntry->mKORequired);

        //--Defeated Activator:
        if(rEntry->mHasDefeatedEnabledParagon)
            rUseFont->DrawText(cActivatorLft, tYPos, 0, 1.0f, "Yes");
        else
            rUseFont->DrawText(cActivatorLft, tYPos, 0, 1.0f, "No");

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--If we hit the end of the list, stop rendering:
        if(tRenders >= ADVMENU_JOURNAL_ENTRIES_ON_LIST)
        {
            mParagonEntries->PopIterator();
            break;
        }

        //--Next.
        tOption ++;
        tYPos = tYPos + cSelectionHei;
        rEntry = (AdvMenuParagonEntry *)mParagonEntries->AutoIterate();
    }

    ///--Help Text
    float cHelpLeft = cSelectionLft;
    float cHelpTop = 675.0f;
    rUseFont->DrawText(cHelpLeft, cHelpTop, 0, 1.0f, "Please see Combat Glossary entry 'Paragons' for more information.");
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
