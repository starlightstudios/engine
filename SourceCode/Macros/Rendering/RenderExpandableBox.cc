//--Base
//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions

//--Libraries
//--Managers

///======================================= Documentation ==========================================
//--Functionality concerning rendering variable sized boxes with frames.

///==================================== Up and Down Cursor ========================================
void RenderExpandableBox(float pLft, float pTop, float pRgt, float pBot, float pInd, StarBitmap *pImage)
{
    ///--[Documentation]
    //--Given an image of a box of fixed size where each of the edges, and the middle, can expand to
    //  and size, renders at an arbitrary size.
    if(!pImage || pInd < 1.0f) return;

    //--Size checks. The result must always be at least 3 pixels large in each section, allowing space
    //  for a top/mid/bot or lft/mid/rgt.
    //  If the required image is less than 3 pixels, don't render.
    //--This also precludes negative sizing.
    float cWid = pRgt - pLft;
    float cHei = pBot - pTop;
    if(cWid < pInd * 3.0f || cHei < pInd * 3.0f) return;

    ///--[Bind]
    //--Bind the image in question.
    pImage->Bind();

    ///--[Calculations]
    //--Create a grid that represents the rendering pieces.
    float cTxX[4];
    float cTxY[4];
    float cRnX[4];
    float cRnY[4];

    //--Image sizes.
    float cImgWid = pImage->GetWidthSafe();
    float cImgHei = pImage->GetHeightSafe();
    float cTxW = pInd / cImgWid;
    float cTxH = pInd / cImgHei;

    //--Populate the textures grid.
    cTxX[0] = 0.0f;
    cTxX[1] = cTxW;
    cTxX[2] = 1.0f - cTxW;
    cTxX[3] = 1.0f;
    cTxY[0] = 0.0f;
    cTxY[1] = cTxH;
    cTxY[2] = 1.0f - cTxH;
    cTxY[3] = 1.0f;

    //--Flip all textures.
    for(int i = 0; i < 4; i ++)
    {
        cTxY[i] = 1.0f - cTxY[i];
    }

    //--Populate the rendering coordinates grid.
    cRnX[0] = pLft;
    cRnX[1] = pLft + pInd;
    cRnX[2] = pRgt - pInd;
    cRnX[3] = pRgt;
    cRnY[0] = pTop;
    cRnY[1] = pTop + pInd;
    cRnY[2] = pBot - pInd;
    cRnY[3] = pBot;

    ///--[Render Across]
    glBegin(GL_QUADS);
    for(int x = 0; x < 3; x ++)
    {
        for(int y = 0; y < 3; y ++)
        {
            glTexCoord2f(cTxX[x+0], cTxY[y+0]); glVertex2f(cRnX[x+0], cRnY[y+0]);
            glTexCoord2f(cTxX[x+1], cTxY[y+0]); glVertex2f(cRnX[x+1], cRnY[y+0]);
            glTexCoord2f(cTxX[x+1], cTxY[y+1]); glVertex2f(cRnX[x+1], cRnY[y+1]);
            glTexCoord2f(cTxX[x+0], cTxY[y+1]); glVertex2f(cRnX[x+0], cRnY[y+1]);
        }
    }
    glEnd();
}
