-- |[ =================================== Open Options Menu ==================================== ]|
--Switches the menu to options mode.
MapM_PushMenuStackHead()
    MT_SetProperty("Show Options")
DL_PopActiveObject()
