///--[AdvCombatJob]
//--Represents a character job, which is a collection of skills, stats, and passive properties.
//  Can alternately be called a "Class", but to avoid confusion is called a Job in the code.

#pragma once

///--[Includes]
#include "Definitions.h"
#include "Structures.h"

///--[Local Structures]
///--[Local Definitions]
#define ADVCJOB_CODE_CREATE 0
#define ADVCJOB_CODE_SWITCHTO 1
#define ADVCJOB_CODE_LEVEL 2
#define ADVCJOB_CODE_MASTER 3
#define ADVCJOB_CODE_ASSEMBLE_SKILL_LIST 4
#define ADVCJOB_CODE_BEGINCOMBAT 5
#define ADVCJOB_CODE_BEGINTURN 6
#define ADVCJOB_CODE_BEGINACTION 7
#define ADVCJOB_CODE_BEGINFREEACTION 8
#define ADVCJOB_CODE_ENDACTION 9
#define ADVCJOB_CODE_ENDTURN 10
#define ADVCJOB_CODE_ENDCOMBAT 11
#define ADVCJOB_CODE_EVENTQUEUED 12
#define ADVCJOB_CODE_TOTAL 13

///--[Classes]
class AdvCombatJob : public RootObject
{
    private:
    //--System
    bool mIsMastered;
    bool mIsAlwaysMastered;
    bool mAppearsOnSkillUI;
    char *mLocalName;
    char *mDisplayName;
    char *mScriptPath;
    AdvCombatEntity *rOwner;
    bool mScriptResponses[ADVCJOB_CODE_TOTAL];

    //--Ability Listing
    char *mAssociatedProfileName;
    int mCurrentJP;
    int mJPNeededToMaster;
    StarLinkedList *mrAbilityList; //AdvCombatAbility *, ref

    //--Display.
    TwoDimensionReal mFaceTableDim;
    StarBitmap *rFaceTableImg;

    //--Tags
    StarLinkedList *mTagList; //int *, master

    protected:

    public:
    //--System
    AdvCombatJob();
    virtual ~AdvCombatJob();

    //--Public Variables
    //--Property Queries
    bool IsMastered();
    bool AppearsOnSkillUI();
    const char *GetInternalName();
    const char *GetDisplayName();
    const char *GetScriptPath();
    const char *GetAssociatedProfileName();
    int GetCurrentJP();
    int GetTotalJP();
    int GetJPToMaster();
    int GetTagCount(const char *pTag);
    bool HasInternalVersionOfAbility(AdvCombatAbility *pAbility);
    StarBitmap *GetFaceProperties(TwoDimensionReal &sDimensions);

    //--Manipulators
    void SetOwner(AdvCombatEntity *pOwner);
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetScriptPath(const char *pPath);
    void SetIsAlwaysMastered(bool pFlag);
    void SetAppearsOnSkillUI(bool pFlag);
    void SetScriptResponse(int pIndex, bool pFlag);
    void RegisterAbility(AdvCombatAbility *pAbility);
    void UnregisterAbility(AdvCombatAbility *pAbility);
    void SetCurrentJP(int pAmount);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);
    void SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath);
    void SetAssociatedProfileName(const char *pName);

    //--Core Methods
    void AssumeJob(AdvCombatEntity *pCaller);
    void CallAtCombatStart(AdvCombatEntity *pCaller);
    void CallScript(int pCode);
    void CallScriptLevelUp(int pLevel);
    void RecheckMastery();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    AdvCombatEntity *GetOwner();
    AdvCombatAbility *GetAbility(int pSlot);
    StarLinkedList *GetAbilityList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatJob_GetProperty(lua_State *L);
int Hook_AdvCombatJob_SetProperty(lua_State *L);

