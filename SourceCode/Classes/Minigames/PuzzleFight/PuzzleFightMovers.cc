//--Base
#include "PuzzleFight.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
bool PuzzleFight::MoveRowLeft(int pRow)
{
    ///--[Error Checks]
    //--First, make sure the row is in range.
    if(pRow < 0 || pRow >= mBoardH) return false;

    //--Make sure this timer is zero. The player cannot move tiles until all previous moves are done.
    if(mBoardSlideTimer > 0) return false;

    ///--[Setup]
    //--Constants.
    float cTileSize = 16.0f;
    float cBoardTileSize = 2.0f;

    ///--[Special Instances]
    //--The leftmost tile has special move code, as it has to create a "fake" version of itself first.
    int tAlt = -1;
    if(!strcasecmp(rDislocationBoard[0][pRow]->mAltDislocationName, "BoardWht"))
    {
        tAlt = 49;
    }
    else
    {
        tAlt = 50;
    }

    //--Make sure that the alt dislocation actually exists. Order it to start at the 0 slot and move left one.
    //  It will slide under the terrain.
    if(tAlt >= 0 && tAlt < mDislocationsTotal)
    {
        mDislocations[tAlt].mCurrentX     = rDislocationBoard[0][pRow]->mCurrentX;
        mDislocations[tAlt].mCurrentY     = rDislocationBoard[0][pRow]->mCurrentY;
        mDislocations[tAlt].mDestinationX = mDislocations[tAlt].mCurrentX - (mBoardTileSizeX * cTileSize);
        mDislocations[tAlt].mDestinationY = mDislocations[tAlt].mCurrentY;
    }

    ///--[Dislocations]
    //--Move the zeroth dislocation to the right side of the board.
    rDislocationBoard[0][pRow]->mCurrentX = (mBoardStartX * cTileSize) + (mBoardW * cBoardTileSize * cTileSize);

    //--Order all the dislocations to move one board tile to the left.
    for(int x = 0; x < mBoardW; x ++)
    {
        rDislocationBoard[x][pRow]->mDestinationX = rDislocationBoard[x][pRow]->mCurrentX - (cBoardTileSize * cTileSize);
    }

    //--Now change where the dislocation is registered on the board.
    DislocationPack *rLeftmostPack = rDislocationBoard[0][pRow];
    for(int x = 0; x < mBoardW-1; x ++)
    {
        rDislocationBoard[x][pRow] = rDislocationBoard[x+1][pRow];
    }
    rDislocationBoard[mBoardW-1][pRow] = rLeftmostPack;

    ///--[Board Objects]
    //--Board objects follow a similar logic to dislocations. The board object is shifted over, and if it wraps
    //  around, a dummy version of the object renders.

    //--Number of ticks to move.
    int cTicks = (int)((cTileSize * cBoardTileSize) / mBoardScrollSpeed);

    //--Shuffle.
    int tLeftmostObject = mBoardObjects[0][pRow].mObjectType;
    for(int x = 0; x < mBoardW-1; x ++)
    {
        //--Set the object type.
        mBoardObjects[x][pRow].mObjectType = mBoardObjects[x+1][pRow].mObjectType;

        //--Render movement.
        float cStartX  = (mBoardStartX * cTileSize) + ((x+1) * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + (pRow  * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + (x     * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + (pRow  * mBoardTileSizeY * cTileSize);
        mBoardObjects[x][pRow].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[x][pRow].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[x][pRow].mPosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    //--The last object on the right becomes the former leftmost object.
    mBoardObjects[mBoardW-1][pRow].mObjectType = tLeftmostObject;
    if(tLeftmostObject != BOARD_OBJECT_EMPTY)
    {
        //--"Normal" render.
        float cStartX  = (mBoardStartX * cTileSize) + ( mBoardW    * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + (pRow        * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + ((mBoardW-1) * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + (pRow        * mBoardTileSizeY * cTileSize);
        mBoardObjects[mBoardW-1][pRow].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[mBoardW-1][pRow].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[mBoardW-1][pRow].mPosition.MoveTo(cTargetX, cTargetY, cTicks);

        //--"Duplicate" render. Shifts off to the left to make it look like the object is wrapping.
        cStartX  = (mBoardStartX * cTileSize) + (   0 * mBoardTileSizeX * cTileSize);
        cStartY  = (mBoardStartY * cTileSize) + (pRow * mBoardTileSizeY * cTileSize);
        cTargetX = (mBoardStartX * cTileSize) + (  -1 * mBoardTileSizeX * cTileSize);
        cTargetY = (mBoardStartY * cTileSize) + (pRow * mBoardTileSizeY * cTileSize);
        mBoardObjects[mBoardW-1][pRow].mDupePosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[mBoardW-1][pRow].mDupePosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    ///--[Finish Up]
    //--Set this timer to lockout movement until the board finishes scrolling.
    mBoardSlideTimer = (cTileSize * cBoardTileSize) / mBoardScrollSpeed;

    //--Everything passed. Return true to indicate we moved the board.
    return true;
}
bool PuzzleFight::MoveRowRight(int pRow)
{
    ///--[Error Checks]
    //--First, make sure the row is in range.
    if(pRow < 0 || pRow >= mBoardH) return false;

    //--Make sure this timer is zero. The player cannot move tiles until all previous moves are done.
    if(mBoardSlideTimer > 0) return false;

    ///--[Setup]
    //--Constants.
    float cTileSize = 16.0f;
    float cBoardTileSize = 2.0f;
    int tRgt = mBoardW-1;

    ///--[Special Instances]
    //--The rightmost tile has special move code, as it has to create a "fake" version of itself first.
    int tAlt = -1;
    if(!strcasecmp(rDislocationBoard[tRgt][pRow]->mAltDislocationName, "BoardWht"))
    {
        tAlt = 49;
    }
    else
    {
        tAlt = 50;
    }

    //--Make sure that the alt dislocation actually exists. Order it to start at the max slot and move right one.
    //  It will slide under the terrain.
    if(tAlt >= 0 && tAlt < mDislocationsTotal)
    {
        mDislocations[tAlt].mCurrentX     = rDislocationBoard[tRgt][pRow]->mCurrentX;
        mDislocations[tAlt].mCurrentY     = rDislocationBoard[tRgt][pRow]->mCurrentY;
        mDislocations[tAlt].mDestinationX = mDislocations[tAlt].mCurrentX + (mBoardTileSizeX * cTileSize);
        mDislocations[tAlt].mDestinationY = mDislocations[tAlt].mCurrentY;
    }

    ///--[Dislocations]
    //--Move the rightmost dislocation to the left side of the board. Slide it under the left edge.
    rDislocationBoard[tRgt][pRow]->mCurrentX = (mBoardStartX * cTileSize) - (cBoardTileSize * cTileSize);

    //--Order all the dislocations to move one board tile to the right.
    for(int x = 0; x < mBoardW; x ++)
    {
        rDislocationBoard[x][pRow]->mDestinationX = rDislocationBoard[x][pRow]->mCurrentX + (cBoardTileSize * cTileSize);
    }

    //--Now change where the dislocation is registered on the board.
    DislocationPack *rRightmostPack = rDislocationBoard[tRgt][pRow];
    for(int x = mBoardW-1; x >= 1; x --)
    {
        rDislocationBoard[x][pRow] = rDislocationBoard[x-1][pRow];
    }
    rDislocationBoard[0][pRow] = rRightmostPack;

    ///--[Board Objects]
    //--Board objects follow a similar logic to dislocations. The board object is shifted over, and if it wraps
    //  around, a dummy version of the object renders.

    //--Number of ticks to move.
    int cTicks = (int)((cTileSize * cBoardTileSize) / mBoardScrollSpeed);

    //--Shuffle.
    int tRightmostObject = mBoardObjects[tRgt][pRow].mObjectType;
    for(int x = mBoardW-1; x >= 1; x --)
    {
        //--Set the object type.
        mBoardObjects[x][pRow].mObjectType = mBoardObjects[x-1][pRow].mObjectType;

        //--Render movement.
        float cStartX  = (mBoardStartX * cTileSize) + ((x-1) * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + (pRow  * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + (x     * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + (pRow  * mBoardTileSizeY * cTileSize);
        mBoardObjects[x][pRow].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[x][pRow].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[x][pRow].mPosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    //--The last object on the left becomes the former rightmost object.
    mBoardObjects[0][pRow].mObjectType = tRightmostObject;
    if(tRightmostObject != BOARD_OBJECT_EMPTY)
    {
        //--"Normal" render.
        float cStartX  = (mBoardStartX * cTileSize) + (  -1 * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + (pRow * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + (   0 * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + (pRow * mBoardTileSizeY * cTileSize);
        mBoardObjects[0][pRow].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[0][pRow].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[0][pRow].mPosition.MoveTo(cTargetX, cTargetY, cTicks);

        //--"Duplicate" render. Shifts off to the right to make it look like the object is wrapping.
        cStartX  = (mBoardStartX * cTileSize) + ( tRgt   * mBoardTileSizeX * cTileSize);
        cStartY  = (mBoardStartY * cTileSize) + ( pRow    * mBoardTileSizeY * cTileSize);
        cTargetX = (mBoardStartX * cTileSize) + ((tRgt+1) * mBoardTileSizeX * cTileSize);
        cTargetY = (mBoardStartY * cTileSize) + ( pRow    * mBoardTileSizeY * cTileSize);
        mBoardObjects[0][pRow].mDupePosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[0][pRow].mDupePosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    ///--[Finish Up]
    //--Set this timer to lockout movement until the board finishes scrolling.
    mBoardSlideTimer = (cTileSize * cBoardTileSize) / mBoardScrollSpeed;

    //--Everything passed. Return true to indicate we moved the board.
    return true;
}
bool PuzzleFight::MoveColumnUp(int pCol)
{
    ///--[Error Checks]
    //--First, make sure the column is in range.
    if(pCol < 0 || pCol >= mBoardW) return false;

    //--Make sure this timer is zero. The player cannot move tiles until all previous moves are done.
    if(mBoardSlideTimer > 0) return false;

    ///--[Setup]
    //--Constants.
    float cTileSize = 16.0f;
    float cBoardTileSize = 2.0f;

    ///--[Special Instances]
    //--The topmost tile has special move code, as it has to create a "fake" version of itself first.
    int tAlt = -1;
    if(!strcasecmp(rDislocationBoard[pCol][0]->mAltDislocationName, "BoardWht"))
    {
        tAlt = 49;
    }
    else
    {
        tAlt = 50;
    }

    //--Make sure that the alt dislocation actually exists. Order it to start at the 0 slot and move left one.
    //  It will slide under the terrain.
    if(tAlt >= 0 && tAlt < mDislocationsTotal)
    {
        mDislocations[tAlt].mCurrentX     = rDislocationBoard[pCol][0]->mCurrentX;
        mDislocations[tAlt].mCurrentY     = rDislocationBoard[pCol][0]->mCurrentY;
        mDislocations[tAlt].mDestinationX = mDislocations[tAlt].mCurrentX;
        mDislocations[tAlt].mDestinationY = mDislocations[tAlt].mCurrentY - (mBoardTileSizeX * cTileSize);
    }

    ///--[Dislocations]
    //--Move the zeroth dislocation to the bottom side of the board.
    rDislocationBoard[pCol][0]->mCurrentY = (mBoardStartY * cTileSize) + (mBoardH * cBoardTileSize * cTileSize);

    //--Order all the dislocations to move one board tile up.
    for(int y = 0; y < mBoardH; y ++)
    {
        rDislocationBoard[pCol][y]->mDestinationY = rDislocationBoard[pCol][y]->mCurrentY - (cBoardTileSize * cTileSize);
    }

    //--Now change where the dislocation is registered on the board.
    DislocationPack *rTopmostPack = rDislocationBoard[pCol][0];
    for(int y = 0; y < mBoardH-1; y ++)
    {
        rDislocationBoard[pCol][y] = rDislocationBoard[pCol][y+1];
    }
    rDislocationBoard[pCol][mBoardH-1] = rTopmostPack;

    ///--[Board Objects]
    //--Board objects follow a similar logic to dislocations. The board object is shifted over, and if it wraps
    //  around, a dummy version of the object renders.

    //--Number of ticks to move.
    int cTicks = (int)((cTileSize * cBoardTileSize) / mBoardScrollSpeed);

    //--Shuffle.
    int tTopmostObject = mBoardObjects[pCol][0].mObjectType;
    for(int y = 0; y < mBoardH-1; y ++)
    {
        //--Set the object type.
        mBoardObjects[pCol][y].mObjectType = mBoardObjects[pCol][y+1].mObjectType;

        //--Render movement.
        float cStartX  = (mBoardStartX * cTileSize) + (pCol  * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + ((y+1) * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + (pCol  * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + (y     * mBoardTileSizeY * cTileSize);
        mBoardObjects[pCol][y].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[pCol][y].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[pCol][y].mPosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    //--The last object on the bottom becomes the former topmost object.
    mBoardObjects[pCol][mBoardH-1].mObjectType = tTopmostObject;
    if(tTopmostObject != BOARD_OBJECT_EMPTY)
    {
        //--"Normal" render.
        float cStartX  = (mBoardStartX * cTileSize) + ( pCol       * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + ( mBoardH    * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + ( pCol       * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + ((mBoardH-1) * mBoardTileSizeY * cTileSize);
        mBoardObjects[pCol][mBoardH-1].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[pCol][mBoardH-1].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[pCol][mBoardH-1].mPosition.MoveTo(cTargetX, cTargetY, cTicks);

        //--"Duplicate" render. Shifts off to the top to make it look like the object is wrapping.
        cStartX  = (mBoardStartX * cTileSize) + (pCol * mBoardTileSizeX * cTileSize);
        cStartY  = (mBoardStartY * cTileSize) + (   0 * mBoardTileSizeY * cTileSize);
        cTargetX = (mBoardStartX * cTileSize) + (pCol * mBoardTileSizeX * cTileSize);
        cTargetY = (mBoardStartY * cTileSize) + (  -1 * mBoardTileSizeY * cTileSize);
        mBoardObjects[pCol][mBoardH-1].mDupePosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[pCol][mBoardH-1].mDupePosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    ///--[Finish Up]
    //--Set this timer to lockout movement until the board finishes scrolling.
    mBoardSlideTimer = (cTileSize * cBoardTileSize) / mBoardScrollSpeed;

    //--Everything passed. Return true to indicate we moved the board.
    return true;
}
bool PuzzleFight::MoveColumnDown(int pCol)
{
    ///--[Error Checks]
    //--First, make sure the column is in range.
    if(pCol < 0 || pCol >= mBoardW) return false;

    //--Make sure this timer is zero. The player cannot move tiles until all previous moves are done.
    if(mBoardSlideTimer > 0) return false;

    ///--[Setup]
    //--Constants.
    float cTileSize = 16.0f;
    float cBoardTileSize = 2.0f;
    int tBot = mBoardH-1;

    ///--[Special Instances]
    //--The bottommost tile has special move code, as it has to create a "fake" version of itself first.
    int tAlt = -1;
    if(!strcasecmp(rDislocationBoard[pCol][tBot]->mAltDislocationName, "BoardWht"))
    {
        tAlt = 49;
    }
    else
    {
        tAlt = 50;
    }

    //--Make sure that the alt dislocation actually exists. Order it to start at the max slot and move down one.
    //  It will slide under the terrain.
    if(tAlt >= 0 && tAlt < mDislocationsTotal)
    {
        mDislocations[tAlt].mCurrentX     = rDislocationBoard[pCol][tBot]->mCurrentX;
        mDislocations[tAlt].mCurrentY     = rDislocationBoard[pCol][tBot]->mCurrentY;
        mDislocations[tAlt].mDestinationX = mDislocations[tAlt].mCurrentX;
        mDislocations[tAlt].mDestinationY = mDislocations[tAlt].mCurrentY + (mBoardTileSizeX * cTileSize);
    }

    ///--[Dislocations]
    //--Move the bottommost dislocation to the top side of the board. Slide it under the top edge.
    rDislocationBoard[pCol][tBot]->mCurrentY = (mBoardStartY * cTileSize) - (cBoardTileSize * cTileSize);

    //--Order all the dislocations to move one board tile down.
    for(int y = 0; y < mBoardH; y ++)
    {
        rDislocationBoard[pCol][y]->mDestinationY = rDislocationBoard[pCol][y]->mCurrentY + (cBoardTileSize * cTileSize);
    }

    //--Now change where the dislocation is registered on the board.
    DislocationPack *rBottommostPack = rDislocationBoard[pCol][tBot];
    for(int y = mBoardH-1; y >= 1; y --)
    {
        rDislocationBoard[pCol][y] = rDislocationBoard[pCol][y-1];
    }
    rDislocationBoard[pCol][0] = rBottommostPack;

    ///--[Board Objects]
    //--Board objects follow a similar logic to dislocations. The board object is shifted over, and if it wraps
    //  around, a dummy version of the object renders.

    //--Number of ticks to move.
    int cTicks = (int)((cTileSize * cBoardTileSize) / mBoardScrollSpeed);

    //--Shuffle.
    int tBottommostObject = mBoardObjects[pCol][tBot].mObjectType;
    for(int y = mBoardH-1; y >= 1; y --)
    {
        //--Set the object type.
        mBoardObjects[pCol][y].mObjectType = mBoardObjects[pCol][y-1].mObjectType;

        //--Render movement.
        float cStartX  = (mBoardStartX * cTileSize) + (pCol  * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + ((y-1) * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + (pCol  * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + (y     * mBoardTileSizeY * cTileSize);
        mBoardObjects[pCol][y].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[pCol][y].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[pCol][y].mPosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    //--The last object on the top becomes the former bottommost object.
    mBoardObjects[pCol][0].mObjectType = tBottommostObject;
    if(tBottommostObject != BOARD_OBJECT_EMPTY)
    {
        //--"Normal" render.
        float cStartX  = (mBoardStartX * cTileSize) + (pCol * mBoardTileSizeX * cTileSize);
        float cStartY  = (mBoardStartY * cTileSize) + (  -1 * mBoardTileSizeY * cTileSize);
        float cTargetX = (mBoardStartX * cTileSize) + (pCol * mBoardTileSizeX * cTileSize);
        float cTargetY = (mBoardStartY * cTileSize) + (   0 * mBoardTileSizeY * cTileSize);
        mBoardObjects[pCol][0].mEasingType = EASING_CODE_LINEAR;
        mBoardObjects[pCol][0].mPosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[pCol][0].mPosition.MoveTo(cTargetX, cTargetY, cTicks);

        //--"Duplicate" render. Shifts off to the right to make it look like the object is wrapping.
        cStartX  = (mBoardStartX * cTileSize) + ( pCol    * mBoardTileSizeX * cTileSize);
        cStartY  = (mBoardStartY * cTileSize) + ( tBot    * mBoardTileSizeY * cTileSize);
        cTargetX = (mBoardStartX * cTileSize) + ( pCol    * mBoardTileSizeX * cTileSize);
        cTargetY = (mBoardStartY * cTileSize) + ((tBot+1) * mBoardTileSizeY * cTileSize);
        mBoardObjects[pCol][0].mDupePosition.MoveTo(cStartX, cStartY, 0);
        mBoardObjects[pCol][0].mDupePosition.MoveTo(cTargetX, cTargetY, cTicks);
    }

    ///--[Finish Up]
    //--Set this timer to lockout movement until the board finishes scrolling.
    mBoardSlideTimer = (cTileSize * cBoardTileSize) / mBoardScrollSpeed;

    //--Everything passed. Return true to indicate we moved the board.
    return true;
}
