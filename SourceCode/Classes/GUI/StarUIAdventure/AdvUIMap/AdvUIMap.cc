//--Base
#include "AdvUIMap.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== System ==============================================
AdvUIMap::AdvUIMap()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AMAP");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIMap ======= ]
    ///--[Variables]
    //--System
    mIsShowingHiddenLayers = true;

    //--Basic Map
    mIsUsingBasicMap = false;
    rBasicMap = NULL;
    rBasicOverlay = NULL;

    //--Advanced Map
    mAdvancedMapRoll = 0;
    mAdvancedPackRerollTimer = 1;
    mAdvancedMapList = new StarLinkedList(true);

    //--Player Display
    mResetFocusOnPlayer = false;
    mPlayerSpriteFlashes = false;
    mPlayerSpriteTimer = 0;
    mPlayerX = 0.0f;
    mPlayerY = 0.0f;
    mMapUsePlayerXBase = 0.0f;
    mMapUsePlayerYBase = 0.0f;
    mMapPlayerCoordScaleX = 1.0f;
    mMapPlayerCoordScaleY = 1.0f;

    //--Map Pins
    mMapPinList = new StarLinkedList(true);

    //--Common
    mMapEdgePadding = 0;
    mMapFocusX = 0.0f;
    mMapFocusY = 0.0f;

    //--Zoom Handling
    mIsZoomClose = true;
    mMapZoom.Initialize();
    mMapZoom.mXCur = 0.500f;

    //--Help Strings
    mToggleHiddenLayersString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIMap::~AdvUIMap()
{
    delete mAdvancedMapList;
    delete mMapPinList;
    delete mToggleHiddenLayersString;
}
void AdvUIMap::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Map UI",  &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI", &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIMap::TakeForeground()
{
    ///--[Variables]
    //--Error check. If no map is present for any reason, fail to set to map mode. The player may not
    //  have found the map yet, or might be in an area with no map.
    if(mIsUsingBasicMap)
    {
        if(!rBasicMap)
        {
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }
    }
    //--Advanced map. Must have at least one layer.
    else
    {
        if(mAdvancedMapList->GetListSize() < 1)
        {
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }
    }

    ///--[Construction]
    //--Reset zoom.
    mIsZoomClose = true;
    mMapZoom.MoveTo(1.5f, 1);
    mMapZoom.Complete();
    mResetFocusOnPlayer = true;

    //--Reposition the map to the player's position.
    //RepositionMap(mPlayerX - (VIRTUAL_CANVAS_X * 0.50f), mPlayerY - (VIRTUAL_CANVAS_Y * 0.50f));

    ///--[Strings]
    RefreshMenuHelp();
}
void AdvUIMap::SetMapPlayerPos(float pPlayerX, float pPlayerY)
{
    //--Sets only the player's X/Y position on the map, used for initial centering when opening.
    //  This is used if you're using an advanced map or want to otherwise override centering.
    mPlayerX = pPlayerX;
    mPlayerY = pPlayerY;
}
void AdvUIMap::SetMapInfo(const char *pDLPath, const char *pDLPathOverlay, int pPlayerX, int pPlayerY)
{
    //--Sets the map image and where the player is on that map. Pass NULL or "Null" to clear.
    mIsUsingBasicMap = false;
    mPlayerX = 0.0f;
    mPlayerY = 0.0f;
    rBasicMap = NULL;
    rBasicOverlay = NULL;
    if(!pDLPath || !strcasecmp(pDLPath, "Null")) return;

    //--Set map.
    mIsUsingBasicMap = true;
    rBasicMap = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mPlayerX = pPlayerX;
    mPlayerY = pPlayerY;

    //--Overlay. Can legally be null.
    if(pDLPathOverlay && strcasecmp(pDLPathOverlay, "Null"))
    {
        rBasicOverlay = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPathOverlay);
    }
}
void AdvUIMap::SetMapEdgePad(int pPadding)
{
    mMapEdgePadding = pPadding;
}
void AdvUIMap::ClearMapPins()
{
    mMapPinList->ClearList();
}
void AdvUIMap::RegisterMapPin(float pX, float pY, const char *pImagePath)
{
    //--Error check:
    if(!pImagePath) return;

    //--Create.
    MapPin *nPin = (MapPin *)starmemoryalloc(sizeof(MapPin));
    nPin->Initialize();
    nPin->mX = pX;
    nPin->mY = pY;
    nPin->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);

    //--Register.
    mMapPinList->AddElement("X", nPin, &FreeThis);
}
void AdvUIMap::RepositionMap(int pX, int pY)
{
    ///--[Documentation]
    //--Repositions the map and clamps the edges so we don't go over. Remember that the offsets are
    //  negatives, so negative X scrolls right.

    ///--[Map Resolve]
    //--Get the map image to use for its sizes.
    float cMapWid = 0.0f;
    float cMapHei = 0.0f;

    //--Basic map.
    if(mIsUsingBasicMap)
    {
        //--No map exists. Fail.
        if(!rBasicMap) return;

        //--Set.
        cMapWid = rBasicMap->GetTrueWidth();
        cMapHei = rBasicMap->GetTrueHeight();
    }
    //--Advanced map. Use the zeroth layer for sizing.
    else
    {
        //--Check existence.
        AdvancedMapPack *rZeroPack = (AdvancedMapPack *)mAdvancedMapList->GetElementBySlot(0);
        if(!rZeroPack || !rZeroPack->rImage) return;

        //--Set.
        cMapWid = rZeroPack->rImage->GetTrueWidth();
        cMapHei = rZeroPack->rImage->GetTrueHeight();
    }

    ///--[Clamping]
    //--Clamp the left/top dimension. The final rendering position of the map is derived from the formula:
    //  cZoom = Variable
    //  cPlayerOffX = Variable
    //  cScreenX = (VIRTUAL_CANVAS_X * 0.50f)
    //  FinalPosX = cScreenX + (-cPlayerOffX * cZoom) - (cScreenX * cZoom)
    //--The final position cannot be greater than zero, thus clamping the left/top boundary.
    float cZoom = mMapZoom.mXCur;
    float cScreenX = (VIRTUAL_CANVAS_X * 0.50f);
    float cScreenY = (VIRTUAL_CANVAS_Y * 0.50f);
    float cFinalPosX = cScreenX - (pX * cZoom) - (cScreenX * cZoom);
    float cFinalPosY = cScreenY - (pY * cZoom) - (cScreenY * cZoom);

    //--Check the right/bottom-side clamps.
    float cFinalPosR = cFinalPosX + (cMapWid * cZoom);
    float cFinalPosB = cFinalPosY + (cMapHei * cZoom);
    if(cFinalPosR < VIRTUAL_CANVAS_X)
    {
        pX = pX + ((cFinalPosR - VIRTUAL_CANVAS_X) / cZoom);
    }
    if(cFinalPosB < VIRTUAL_CANVAS_Y)
    {
        pY = pY + ((cFinalPosB - VIRTUAL_CANVAS_Y) / cZoom);
    }

    //--Check the left/top clamps in case the right/bottom pushed past zero.
    if(cFinalPosX > 0) pX = pX + (cFinalPosX / cZoom);
    if(cFinalPosY > 0) pY = pY + (cFinalPosY / cZoom);

    //--Set.
    mMapFocusX = pX;
    mMapFocusY = pY;
}

///======================================= Core Methods ===========================================
void AdvUIMap::RefreshMenuHelp()
{
    //--Toggle Hidden Layers
    mToggleHiddenLayersString->SetString("[IMG0] Toggle Layers");
    mToggleHiddenLayersString->AllocateImages(1);
    mToggleHiddenLayersString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mToggleHiddenLayersString->CrossreferenceImages();
}
void AdvUIMap::RecomputeCursorPositions()
{
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIMap::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--Vis timer.
    if(mVisibilityTimer < ADVMENU_MAP_VIS_TICKS) mVisibilityTimer ++;

    //--Reroll this each tick.
    if(mAdvancedPackRerollTimer > 0)
    {
        mAdvancedPackRerollTimer --;
    }
    else
    {
        mAdvancedMapRoll = rand() % 1000;
        mAdvancedPackRerollTimer = 1 + (rand() % 5);
    }

    //--Sprite timer.
    mPlayerSpriteTimer ++;

    //--Easing packages.
    bool tMustReposition = (mMapZoom.mTimer < mMapZoom.mTimerMax);
    mMapZoom.Increment(EASING_CODE_QUADINOUT);
    if(tMustReposition) RepositionMap(mMapFocusX, mMapFocusY);

    ///--[Control Handling]
    //--This object has no sub-modes and very little control handling.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Distance.
    float cMoveDistance = 10.0f;

    //--Up.
    if(rControlManager->IsDown("Up"))
    {
        RepositionMap(mMapFocusX, mMapFocusY - cMoveDistance);
    }
    //--Down!
    if(rControlManager->IsDown("Down"))
    {
        RepositionMap(mMapFocusX, mMapFocusY + cMoveDistance);
    }
    //--Left!
    if(rControlManager->IsDown("Left"))
    {
        RepositionMap(mMapFocusX - cMoveDistance, mMapFocusY);
    }
    //--RIGHT!
    if(rControlManager->IsDown("Right"))
    {
        RepositionMap(mMapFocusX + cMoveDistance, mMapFocusY);
    }

    //--Activate. Switches map zoom.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Zoomed in close, set to out.
        if(mIsZoomClose)
        {
            mIsZoomClose = false;
            mMapZoom.MoveTo(1.0f / 2.0f, ADVMENU_MAP_ZOOM_TICKS);
        }
        //--Zoomed out, set to in.
        else
        {
            mIsZoomClose = true;
            mMapZoom.MoveTo(1.5f, ADVMENU_MAP_ZOOM_TICKS);
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    //--Toggle. Turns certain map layers on and off.
    if(rControlManager->IsFirstPress("Run"))
    {
        mIsShowingHiddenLayers = !mIsShowingHiddenLayers;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}
void AdvUIMap::UpdateBackground()
{
    if(mVisibilityTimer > 0) mVisibilityTimer --;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIMap::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Simple switch between map rendering types. Common fade handling is also done.
    //--Fade percentage. Renders a fadeout for the first half, and a fadein for the second half.
    float cFadePct = EasingFunction::QuadraticOut(mVisibilityTimer, ADVMENU_MAP_VIS_TICKS);

    ///--[Fullblack]
    //--Don't render anything except a black fadeout if below half on the fade percent.
    if(cFadePct < 0.50f)
    {
        StarBitmap::DrawFullBlack(cFadePct * 2.0f);
        return;
    }

    //--When using advanced rendering, render a black underlay.
    StarBitmap::DrawFullBlack(1.0f);

    ///--[Subrender Switch]
    //--Basic map, no layers or special effects.
    if(mIsUsingBasicMap)
    {
        RenderBasicMapMode();
    }
    //--Render the advanced image set.
    else
    {
        RenderAdvancedMapMode();
    }

    ///--[Overlay]
    //--If there is an overlay, render it with a fixed position.
    if(rBasicOverlay) rBasicOverlay->Draw();

    ///--[Header]
    //--Render a header. Borrows the base menu one.
    AdvImages.rOverlayHeader->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixer();
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Map");
    StarlightColor::ClearMixer();

    ///--[Help Strings]
    //--Constants.
    float cHelpLft = 5.0f;
    float cHelpTop = 5.0f;

    //--Render.
    mToggleHiddenLayersString->DrawText(cHelpLft, cHelpTop, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_MapHelp);

    ///--[Fading]
    //--If we got this far, render a fullblack fade in.
    if(cFadePct < 1.0f)
    {
        float cAlpha = (cFadePct - 0.50f) * 2.0f;
        StarBitmap::DrawFullBlack(1.0f - cAlpha);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
