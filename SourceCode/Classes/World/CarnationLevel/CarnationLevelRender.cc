//--Base
#include "CarnationLevel.h"

//--Classes
#include "AdventureMenu.h"
#include "CarnationCombat.h"
#include "CarnationCombatEntity.h"
#include "CarnationMinimap.h"
#include "CarnationPopup.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "CarnationDef.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

///--[Debug]
//#define CARNATIONLEVEL_RENDER_DEBUG
#ifdef CARNATIONLEVEL_RENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Drawing =============================================
void CarnationLevel::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void CarnationLevel::Render()
{
    ///--[Documentation and Setup]
    //--Flip this flag to tell the menus not to render.
    MapManager::xHasRenderedMenus = true;

    //--Make sure images are ready to go.
    if(!Images.mIsReady) return;

    ///--[Debug]
    DebugPush(true, "Carnation Level Render begin.\n");

    ///--[Background]
    //--Render all background layers.
    CarnMapLayer *rBGLayer = (CarnMapLayer *)mBackgroundLayers->PushIterator();
    while(rBGLayer)
    {
        rBGLayer->Render(1.0f);
        rBGLayer = (CarnMapLayer *)mBackgroundLayers->AutoIterate();
    }

    //--Particles.
    AuraParticle *rParticle = (AuraParticle *)mAuraParticleList->PushIterator();
    while(rParticle)
    {
        //--Render.
        rParticle->Render();

        //--Next.
        rParticle = (AuraParticle *)mAuraParticleList->AutoIterate();
    }
    StarlightColor::ClearMixer();

    ///--[Transition Fade]
    float tTransitionAlpha = 0.0f;
    if(mIsLevelTransitioning)
    {
        //--Fadeout. Runs a timer.
        if(mTransitionStage == CL_TRANSITION_FADEOUT)
        {
            //--Compute.
            float cAlpha = EasingFunction::QuadraticOut(mTransitionTimer, CL_TRANSITION_FADEOUT_TICKS);
            tTransitionAlpha = cAlpha;

            //--Render overlay.
            StarBitmap::DrawFullBlack(cAlpha);
        }
        //--Switch. A brief black period, clears everything and then runs the new constructor.
        else if(mTransitionStage == CL_TRANSITION_LEVELSWITCH)
        {
            tTransitionAlpha = 1.0f;
            StarBitmap::DrawFullBlack(1.0f);
        }
        //--Fadein. Runs a timer.
        else if(mTransitionStage == CL_TRANSITION_FADEIN)
        {
            //--Compute.
            float cAlpha = EasingFunction::QuadraticOut(mTransitionTimer, CL_TRANSITION_FADEIN_TICKS);
            tTransitionAlpha = 1.0f - cAlpha;

            //--Render overlay.
            StarBitmap::DrawFullBlack(1.0f - cAlpha);
        }
    }

    //--Get drop opacity for combat case.
    float cCombatOpacity = EasingFunction::QuadraticInOut(mCombatTimer, CL_COMBAT_TICKS);

    ///--[Fixed UI]
    RenderFixedUI(1.0f - cCombatOpacity);

    ///--[Variable UI]
    RenderExaminables((1.0f - cCombatOpacity) * (1.0f - tTransitionAlpha));
    RenderPopups((1.0f - cCombatOpacity) * (1.0f - tTransitionAlpha));

    //--Minimap expansion. When in minimap mode, a larger version of the minimap appears.
    if(mMinimapTimer > 0)
    {
        RenderExpandedMinimap();
    }

    ///--[Event Notifications]
    RenderEvents();

    ///--[Overlays]
    //--Dialogue, if visible.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible())// &&!rAdventureCombat->IsActive())
    {
        rWorldDialogue->Render();
    }

    //--Menu, if visible.
    mMenu->Render();

    //--AdvCombat handler. It always calls the render, but may render nothing if not active.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    rAdventureCombat->Render(false, cCombatOpacity);

    ///--[Confirm Retreat]
    if(mConfirmRetreatTimer > 0)
    {
        //--Overlay.
        float cAlpha = EasingFunction::QuadraticInOut(mConfirmRetreatTimer, cxTimer_ConfirmRetreat);
        StarBitmap::DrawFullBlack(cAlpha * 0.90f);

        //--Prompt.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        Images.Data.rFont_MinimapTitle->DrawText(VIRTUAL_CANVAS_X * 0.50f, 265.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Are you sure?");
        Images.Data.rFont_MinimapHelp->DrawText(VIRTUAL_CANVAS_X * 0.50f, 300.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "You will lose items, experience, and cash");
        Images.Data.rFont_MinimapHelp->DrawText(VIRTUAL_CANVAS_X * 0.50f, 330.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "based on the difficulty level.");

        //--Buttons.
        Images.Data.rFrameMouseDecision->Draw(mButtonConfirmRetreat.mLft, mButtonConfirmRetreat.mTop);
        Images.Data.rFont_MinimapHelp->DrawText(mButtonConfirmRetreat.mXCenter, mButtonConfirmRetreat.mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Confirm");
        Images.Data.rFrameMouseDecision->Draw(mButtonCancelRetreat.mLft,  mButtonCancelRetreat.mTop);
        Images.Data.rFont_MinimapHelp->DrawText(mButtonCancelRetreat.mXCenter, mButtonCancelRetreat.mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Cancel");

        //--Clean.
        StarlightColor::ClearMixer();
    }

    ///--[Debug]
    DebugPop("Carnation Level Render completes.\n");
}
void CarnationLevel::RenderFixedUI(float pAlpha)
{
    ///--[Documentation]
    //--Renders the parts of the UI that don't move, or move much. This includes frames, dialogue, player
    //  party, and options buttons. The overlay menus are rendered elsewhere.
    if(pAlpha <= 0.0f) return;

    //--Set.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Fixed Pieces]
    //--Parts that never move.
    Images.Data.rFrameDialogue->Draw();
    Images.Data.rFrameRightBlock->Draw();
    //Images.Data.rTabBackpack->Draw();
    Images.Data.rTabJournal->Draw();

    //--Name of the room.
    if(mCurrentAreaName)
    {
        Images.Data.rFrameRoomName->Draw();
        Images.Data.rFontTitle->DrawText(VIRTUAL_CANVAS_X * 0.50f, 26.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mCurrentAreaName);
    }

    //--Tab labels.
    Images.Data.rFont_Tabs->DrawText(10.0f, 547.0f, 0, 1.0f, "Menu");

    ///--[Retreat]
    //--This tab only appears if flagged.
    if(mCanRetreat)
    {
        Images.Data.rTabOptions->Draw();
        Images.Data.rFont_Tabs->DrawText(10.0f, 671.0f, 0, 1.0f, "Escape To Hub");
    }

    ///--[Dialogue]
    mInternalDialogue->RenderOnlyText(255.0f, 556.0f, 1.0f);

    ///--[Party Handling]
    //--Setup.
    AdvCombat *rCombat = AdvCombat::Fetch();
    if(!rCombat) return;

    //--For each party member:
    int tPartySize = rCombat->GetActivePartyCount();
    for(int i = 0; i < tPartySize; i ++)
    {
        //--Get party member.
        AdvCombatEntity *rPartyMember = rCombat->GetActiveMemberI(i);
        if(!rPartyMember) continue;

        //--If the entity is not a CarnationCombatEntity, do nothing:
        if(!rPartyMember->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) continue;

        //--Cast.
        CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rPartyMember;

        //--Positioning.
        glTranslatef(0.0f, 157.0f * i, 0.0f);

        //--Render their backing.
        Images.Data.rFramePartyBack->Draw();

        //--Render the backing again, with a stencil.
        DisplayManager::ActivateMaskRender(CL_STENCIL_PARTY_START+i);
        Images.Data.rFramePartyBack->Draw();

        //--Render the party member testing by stencil.
        DisplayManager::ActivateStencilRender(CL_STENCIL_PARTY_START+i);
        glTranslatef(43.0f, 17.0f, 0.0f);
        glScalef(0.80f, 0.80f, 1.0f);
        rCarnationEntity->RenderPortraitAt(0.0f, 0.0f, true);
        glScalef(1.0f / 0.80f, 1.0f / 0.80f, 1.0f);
        glTranslatef(-43.0f, -17.0f, 0.0f);
        DisplayManager::DeactivateStencilling();

        //--Render a frame over the front.
        Images.Data.rFramePartyFront->Draw();

        //--Clean.
        glTranslatef(0.0f, (-157.0f * i), 0.0f);
    }

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Minimap]
    //--Setup stencil rendering.
    DisplayManager::ActivateMaskRender(CL_STENCIL_MINIMAP);
    glColorMask(false, false, false, false);
    Images.Data.rFrameMinimapMask->Draw();

    //--Render the minimap.
    DisplayManager::ActivateStencilRender(CL_STENCIL_MINIMAP);
    glColorMask(true, true, true, true);
    glTranslatef(1221.0f, 240.0f, 0.0f);
    mMinimap->Render(1.0f);
    glTranslatef(-1221.0f, -240.0f, 0.0f);

    //--Clean.
    DisplayManager::DeactivateStencilling();

    ///--[Alarm Rendering]
    //--Resolve which alarm frame to use.
    StarBitmap *rAlarmImg = Images.Data.rOverlayAlarm0;
    if(mAlarmVal.mXCur >= 65)
        rAlarmImg = Images.Data.rOverlayAlarm3;
    else if(mAlarmVal.mXCur >= 45)
        rAlarmImg = Images.Data.rOverlayAlarm2;
    else if(mAlarmVal.mXCur >= 25)
        rAlarmImg = Images.Data.rOverlayAlarm1;

    //--The actual rendering position is 1124x-12, but the alarm object can swing around like a charm.
    //  The center of this swinging is at 124x77 on the image itself, so we need to translate and rotate
    //  around that position to correctly render the object.
    //--If the object's rotation is zero, just render it normally.
    float cRenderX = 1124.0f;
    float cRenderY =  420.0f;
    if(mAlarmShake == 0.0f)
    {
        rAlarmImg->Draw(cRenderX, cRenderY);
    }
    //--Otherwise, do a full render with rotation.
    else
    {
        //--Constants.
        float cAroundX =  124.0f;
        float cAroundY =   77.0f;

        //--Position.
        glTranslatef(cRenderX, cRenderY, 0.0f);
        glTranslatef(cAroundX, cAroundY, 0.0f);
        glRotatef(mAlarmShake, 0.0f, 0.0f, 1.0f);
        glTranslatef(-cAroundX, -cAroundY, 0.0f);
        rAlarmImg->Draw();
        glTranslatef(cAroundX, cAroundY, 0.0f);
        glRotatef(-mAlarmShake, 0.0f, 0.0f, 1.0f);
        glTranslatef(-cAroundX, -cAroundY, 0.0f);

        //--Clean.
        glTranslatef(-cRenderX, -cRenderY, 0.0f);
    }
}
void CarnationLevel::RenderExaminables(float pAlpha)
{
    ///--[Documentation]
    //--Block render if alpha is zero.
    if(pAlpha <= 0.0f) return;

    //--Render hearts where examinables are.
    CarnExaminable *rRenderExaminable = (CarnExaminable *)mExaminablesList->PushIterator();
    while(rRenderExaminable)
    {
        //--Timer is zero, don't render:
        if(rRenderExaminable->mDiscoveryTimer < 1 && !rRenderExaminable->mAlwaysVisible)
        {
            rRenderExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
            continue;
        }

        //--Draw a heart, partially transparent based on the internal timer.
        float tLocalAlpha = EasingFunction::QuadraticInOut(rRenderExaminable->mDiscoveryTimer, CARNEXAMINE_DISCOVERY_TICKS);
        if(rRenderExaminable->mAlwaysVisible) tLocalAlpha = 1.0f;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha * tLocalAlpha);
        Images.Data.rOverlayHeart->Draw(rRenderExaminable->mHeartX, rRenderExaminable->mHeartY);

        //--If flagged, render the display name of the examinable above it.
        if(rRenderExaminable->mAlwaysShowName && rRenderExaminable->mDisplayName)
        {
            //--Backing.
            float tTextWid = Images.Data.rFontPopup->GetTextWidth(rRenderExaminable->mDisplayName);
            float tTextHei = Images.Data.rFontPopup->GetTextHeight();
            float cLft = rRenderExaminable->mHeartX + 20 - (tTextWid * 0.50f) - 4.0f;
            float cTop = rRenderExaminable->mHeartY - 24.0f;
            float cRgt = cLft + tTextWid + 8.0f;
            float cBot = cTop + tTextHei + 3.0f;
            float cPad = 3.0f;
            Images.Data.rFrameExaminableNameBack->RenderNine(cLft, cLft+cPad, cRgt-cPad, cRgt, cTop, cTop+cPad, cBot-cPad, cBot, 0.10f, 0.90f, 0.10f, 0.90f);

            //--Name.
            Images.Data.rFontPopup->DrawTextArgs(rRenderExaminable->mHeartX + 20.0f, rRenderExaminable->mHeartY - 24.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rRenderExaminable->mDisplayName);
        }

        //--Next.
        rRenderExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
}
void CarnationLevel::RenderPopups(float pAlpha)
{
    ///--[Documentation]
    //--Block render if alpha is zero.
    if(pAlpha <= 0.0f) return;

    //--Set.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Renders popup menus.
    CarnationPopup *rRenderPopup = (CarnationPopup *)mPopupList->PushIterator();
    while(rRenderPopup)
    {
        rRenderPopup->Render(Images.Data.rOverlayHeart, Images.Data.rFramePopupBacking, Images.Data.rFontPopup);
        rRenderPopup = (CarnationPopup *)mPopupList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
}

///========================================== Minimap =============================================
void CarnationLevel::RenderExpandedMinimap()
{
    ///--[Documentation]
    //--Renders the full minimap, along with extra things like onscreen instructions.

    ///--[Setup]
    //--Opacity.
    float cOpacity = EasingFunction::QuadraticInOut(mMinimapTimer, CL_MINIMAP_TICKS);

    //--Render a black underlay.
    StarBitmap::DrawFullBlack(cOpacity);

    ///--[Render]
    //--Position.
    glTranslatef(mMinimapOffsetX, mMinimapOffsetY, 0.0f);

    //--Setup stencil rendering.
    glClearStencil(0);
    glClear(GL_STENCIL_BUFFER_BIT);
    DisplayManager::ActivateMaskRender(CL_STENCIL_SHADOWING);
    mMinimap->RenderShadowingStrict();

    //--Render the minimap.
    DisplayManager::ActivateStencilRender(0);
    mMinimap->RenderBase(cOpacity);
    mMinimap->RenderShadowing(cOpacity);

    //--Clean.
    DisplayManager::DeactivateStencilling();
    glTranslatef(mMinimapOffsetX * -1.0f, mMinimapOffsetY * -1.0f, 0.0f);

    ///--[Overlays and UI]
    //--Colors.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cOpacity);

    //--Title.
    Images.Data.rFont_MinimapTitle->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 15.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Dungeon Map");

    //--Instructions.
    float cHei = Images.Data.rFont_MinimapHelp->GetTextHeight();
    float cYPos = VIRTUAL_CANVAS_Y - (cHei * 2.0f) - 10.0f;
    Images.Data.rFont_MinimapHelp->DrawTextArgs(10.0f, cYPos + (cHei * 0.0f), 0, 1.0f, "Click and drag to scroll.");
    Images.Data.rFont_MinimapHelp->DrawTextArgs(10.0f, cYPos + (cHei * 1.0f), 0, 1.0f, "Right-Click to exit.");

    //--Button.
    float cTxtX = Images.Data.rButton_CenterOnPlayer->GetXOffset() + (Images.Data.rButton_CenterOnPlayer->GetWidth() * 0.50f);
    float cTxtY = Images.Data.rButton_CenterOnPlayer->GetYOffset() + (Images.Data.rButton_CenterOnPlayer->GetHeight() * 0.50f);
    Images.Data.rButton_CenterOnPlayer->Draw();
    Images.Data.rFont_MinimapHelp->DrawTextArgs(cTxtX, cTxtY, SUGARFONT_AUTOCENTER_XY, 1.0f, "Center on Party");

    //--Clean.
    StarlightColor::ClearMixer();
}
