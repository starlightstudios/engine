//--Base
#include "CarnationDef.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

///======================================= CarnExaminable =========================================
void CarnExaminable::Initialize()
{
    //--Position
    mHeartX = 0;
    mHeartY = 0;
    mHeartR = 1;
    mHeartB = 1;

    //--Discovery
    mAlwaysVisible = false;
    mDiscoveryTimer = 0;

    //--Display
    mDisplayName = NULL;
    mAlwaysShowName = false;

    //--Execution
    mHighlightVal = 0;
    mScript = NULL;
    mArgument = NULL;
}
void CarnExaminable::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    CarnExaminable *rExaminable = (CarnExaminable *)pPtr;
    free(rExaminable->mDisplayName);
    free(rExaminable->mScript);
    free(rExaminable->mArgument);
    free(rExaminable);
}

///======================================== CarnMapLayer ==========================================
void CarnMapLayer::Initialize()
{
    mRenderPriority = 0;
    mOffsetX = 0.0f;
    mOffsetY = 0.0f;
    mIsVisible = false;
    mTimer = 1;
    mTimerMax = 1;
    rImage = NULL;
}
void CarnMapLayer::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    CarnMapLayer *rPtr = (CarnMapLayer *)pPtr;
    free(rPtr);
}
void CarnMapLayer::SetVisible(bool pIsVisible, int pMaxTicks)
{
    //--Set.
    mIsVisible = pIsVisible;
    mTimerMax = pMaxTicks;

    //--Range checking.
    if(mTimerMax < 1)
    {
        mTimer = 1;
        mTimerMax = 1;
    }
    if(mTimer < 0) mTimer = 0;
    if(mTimer > mTimerMax) mTimer = mTimerMax;
}
void CarnMapLayer::Update()
{
    if(mIsVisible)
    {
        if(mTimer < mTimerMax) mTimer ++;
    }
    else
    {
        if(mTimer > 0) mTimer --;
    }
}
void CarnMapLayer::Render(float pAlpha)
{
    //--Renders using internal opacity.
    if(mTimer <= 0 || !rImage) return;
    float tAlpha = EasingFunction::QuadraticInOut(mTimer, mTimerMax);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha * pAlpha);
    rImage->Draw(mOffsetX, mOffsetY);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
