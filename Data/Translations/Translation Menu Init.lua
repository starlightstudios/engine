-- |[ ================================= Translation Menu Init ================================== ]|
--Once the main menu is built, this populates it with translation packages based on what was found
-- during the startup scan.
--The array gzaFoundTranslations contains all located translations.

--Translation Package Format:
--sPathLetters          --Usually two letters, name of the folder that has the translation eg. "JP"
--sInternalName         --Full name of the language or translation eg. "Japanese"

-- |[Block]|
--Not all title screen objects can use this code.
if(gsMandateTitle ~= "Witch Hunter Izana") then return end

-- |[Create Entries]|
for i = 1, #gzaFoundTranslations, 1 do
    
    --Load the flag bitmap into the datalibrary.
    local sDatafilePath = "Data/Translations/" .. gzaFoundTranslations[i].sPathLetters .. "/Images.slf"
    SLF_Open(sDatafilePath)
        fnExecAutoload("AutoLoad|TranslationImages")
    SLF_Close()

    --Create an entry.
    local sFlagPath    = "Root/Images/Translations/"..gzaFoundTranslations[i].sPathLetters.."/Flag"
    local sRestartPath = "Root/Images/Translations/"..gzaFoundTranslations[i].sPathLetters.."/RestartProgram"
    MT_SetProperty("Register Translation", gzaFoundTranslations[i].sInternalName, gzaFoundTranslations[i].sExtension, gzaFoundTranslations[i].sPathLetters, sFlagPath, sRestartPath)

end

-- |[Set Internal Values]|
MT_SetProperty("Starting Language", gsLocalization)