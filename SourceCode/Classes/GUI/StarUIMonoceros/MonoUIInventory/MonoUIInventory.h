///====================================== MonoUIInventory ==========================================
//--Inventory UI object. Same as the adventure version, uses different fonts/images.

#pragma once

///========================================= Includes =============================================
#include "MonoUICommon.h"
#include "AdvUIInventory.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Help Strings
#define MONOUIINV_ITEM_MAX_NAME_LENGTH 385.0f

///======================================== Translation ===========================================
///--[MonoUIInventory_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIINVENTORY_STRINGS_TOTAL 35
class MonoUIInventory_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIINVENTORY_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextInventory;
            char *mTextHelpMenu;
            char *mTextName;
            char *mTextType;
            char *mTextValue;
            char *mTextGems;
            char *mTextCannotDeconstruct;
            char *mTextDeconNoEquip;
            char *mTextDeconNoResult;
            char *mTextDeconConfirm;
            char *mTextHelp[17];
            char *mTextControlShowHelp;
            char *mTextControlSortName;
            char *mTextControlSortValue;
            char *mTextControlSortType;
            char *mTextControlHoldScroll;
            char *mTextControlDeconstruct;
            char *mTextControlConfirm;
            char *mTextControlCancel;
        }str;
    };

    //--Functions
    MonoUIInventory_Strings();
    virtual ~MonoUIInventory_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIInventory : virtual public MonoUICommon, virtual public AdvUIInventory
{
    protected:
    ///--[Constants]
    static const int cxCursorTicks = 7;                 //Ticks to move a cursor once it repositions.
    static constexpr float cxMaxNameLen = 385.0f;           //How many pixels a name can be before it compresses to fit on the page.

    ///--[System]
    ///--[Images]
    struct MonoImages
    {
        //--Fonts.
        StarFont *rFont_Entry;

        //--Images.
        StarBitmap *rOverlay_DescriptionName;
        StarBitmap *rOverlay_ItemshadeLft;
        StarBitmap *rOverlay_ItemshadeRgt;
        StarBitmap *rOverlay_ItemshadeSelect;
        StarBitmap *rOverlay_Titleshade;
    }MonoImages;
    protected:

    public:
    //--System
    MonoUIInventory();
    virtual ~MonoUIInventory();
    virtual void Construct();

    //--Public Variables
    static MonoUIInventory_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    virtual void SetSortingString(int pFlag);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void RenderTop(float pColorAlpha);
    virtual void RenderInventoryListing(float pColorAlpha);
    void RenderInventoryEntryBack(AdventureItem *pItem, const char *pOwnerName, float pYPosition, bool pIsOdd, float pColorAlpha);
    virtual void RenderInventoryEntry(AdventureItem *pItem, const char *pOwnerName, float pYPosition, bool pIsOdd, float pColorAlpha);
    virtual void RenderInventoryDescription(AdventureItem *pItem);
    virtual void RenderDeconstruct(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


