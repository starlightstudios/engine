//--Base
#include "CarnationLevel.h"

//--Classes
#include "AdventureMenu.h"
#include "WorldDialogue.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "MapManager.h"

///======================================== Lua Hooking ===========================================
void CarnationLevel::HookToLuaState(lua_State *pLuaState)
{
    /* CarnationLevel_Create()
       Creates a new CarnationLevel and pushes it to the MapManager.*/
    lua_register(pLuaState, "CarnationLevel_Create", &Hook_CarnationLevel_Create);

    /* CarnationLevel_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the CarnationLevel class. */
    lua_register(pLuaState, "CarnationLevel_GetProperty", &Hook_CarnationLevel_GetProperty);

    /* CarnationLevel_SetProperty("Dummy")
       Sets the property in the CarnationLevel class. */
    lua_register(pLuaState, "CarnationLevel_SetProperty", &Hook_CarnationLevel_SetProperty);

    //--Switcheroo functions, allowing full WD_SetProperty() functionality within the internal
    //  dialogue box of the CarnationLevel.
    lua_register(pLuaState, "CarnDia_GetProperty", &Hook_CarnDia_GetProperty);
    lua_register(pLuaState, "CarnDia_SetProperty", &Hook_CarnDia_SetProperty);
    lua_register(pLuaState, "CarnAppend", &Hook_CarnAppend);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnationLevel_Create(lua_State *L)
{
    //CarnationLevel_Create()
    CarnationLevel *nNewLevel = new CarnationLevel();
    MapManager::Fetch()->ReceiveLevel(nNewLevel);

    //--Finish up.
    return 0;
}
int Hook_CarnationLevel_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnationLevel_GetProperty("Is Active Level Carnation") (1 Boolean) (Static)

    //--Examinables
    //CarnationLevel_GetProperty("Does Examinable Exist", sName) (1 Boolean)
    //CarnationLevel_GetProperty("Is Examinable At", iX, iY) (1 Boolean)

    //--[Parsing]
    //CarnationLevel_GetProperty("Parsed Level Name") (1 String)
    //CarnationLevel_GetProperty("Parsed Image Path") (1 String)
    //CarnationLevel_GetProperty("Parsed X Size") (1 Integer)
    //CarnationLevel_GetProperty("Parsed Y Size") (1 Integer)
    //CarnationLevel_GetProperty("Parsed Collision", iX, iY) (1 Integer)
    //CarnationLevel_GetProperty("Parsed Room Count") (1 Integer)
    //CarnationLevel_GetProperty("Parsed Room Name", iSlot) (1 String)
    //CarnationLevel_GetProperty("Parsed Room Layout", iSlot) (1 String)
    //CarnationLevel_GetProperty("Parsed Room Background", iSlot) (1 String)
    //CarnationLevel_GetProperty("Parsed Room X", iSlot) (1 Integer)
    //CarnationLevel_GetProperty("Parsed Room Y", iSlot) (1 Integer)
    //CarnationLevel_GetProperty("Parsed Room Is Safe", iSlot) (1 Boolean)
    //CarnationLevel_GetProperty("Parsed Room Danger", iSlot) (1 Integer)
    //CarnationLevel_GetProperty("Parsed Room Enemy", iSlot) (1 String)
    //CarnationLevel_GetProperty("Parsed Room Music", iSlot) (1 String)
    //CarnationLevel_GetProperty("Parsed Room Particle Path", iSlot) (1 String)
    //CarnationLevel_GetProperty("Parsed Room Particle Rate", iSlot) (1 Integer)
    //CarnationLevel_GetProperty("Parsed Connection From Room In Direction", iSlot, iDirection) (1 String)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnationLevel_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Returns true if the active level is a CarnationLevel.
    if(!strcasecmp(rSwitchType, "Is Active Level Carnation"))
    {
        CarnationLevel *rActiveLevel = CarnationLevel::Fetch();
        if(rActiveLevel && rActiveLevel->IsOfType(POINTER_TYPE_CARNATIONLEVEL))
        {
            lua_pushboolean(L, true);
        }
        else
        {
            lua_pushboolean(L, false);
        }
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    CarnationLevel *rActiveLevel = CarnationLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("CarnationLevel_GetProperty", L);

    ///--[Examinables]
    //--Returns true if an examinable exists with that name.
    if(!strcasecmp(rSwitchType, "Does Examinable Exist") && tArgs >= 2)
    {
        lua_pushboolean(L, rActiveLevel->DoesExaminableExist(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns true if an examinable exactly matches the position.
    else if(!strcasecmp(rSwitchType, "Is Examinable At") && tArgs >= 3)
    {
        lua_pushboolean(L, rActiveLevel->IsExaminableAt(lua_tonumber(L, 2), lua_tonumber(L, 3)));
        tReturns = 1;
    }
    ///--[Parsing]
    //--Named of the area, parsed from the datafile.
    else if(!strcasecmp(rSwitchType, "Parsed Level Name") && tArgs == 1)
    {
        lua_pushstring(L, rActiveLevel->GetParsedLevelName());
        tReturns = 1;
    }
    //--Image DL path.
    else if(!strcasecmp(rSwitchType, "Parsed Image Path") && tArgs == 1)
    {
        lua_pushstring(L, rActiveLevel->GetParsedImagePath());
        tReturns = 1;
    }
    //--X size of the map, in tiles.
    else if(!strcasecmp(rSwitchType, "Parsed X Size") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveLevel->GetParsedXSize());
        tReturns = 1;
    }
    //--Y size of the map.
    else if(!strcasecmp(rSwitchType, "Parsed Y Size") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveLevel->GetParsedYSize());
        tReturns = 1;
    }
    //--Collision value at given location.
    else if(!strcasecmp(rSwitchType, "Parsed Collision") && tArgs == 3)
    {
        lua_pushinteger(L, rActiveLevel->GetParsedCollision(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    //--How many rooms were parsed.
    else if(!strcasecmp(rSwitchType, "Parsed Room Count") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveLevel->GetParsedRoomCount());
        tReturns = 1;
    }
    //--Name of the room in the given slot.
    else if(!strcasecmp(rSwitchType, "Parsed Room Name") && tArgs == 2)
    {
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom)
        {
            lua_pushstring(L, rRoom->mName);
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--Layout of the room in the given slot.
    else if(!strcasecmp(rSwitchType, "Parsed Room Layout") && tArgs == 2)
    {
        const char *rPush = "Null";
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom) rPush = rRoom->mLayout;
        lua_pushstring(L, rPush);
        tReturns = 1;
    }
    //--Background of the room in the given slot.
    else if(!strcasecmp(rSwitchType, "Parsed Room Background") && tArgs == 2)
    {
        const char *rPush = "Null";
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom) rPush = rRoom->mBGPath;
        lua_pushstring(L, rPush);
        tReturns = 1;
    }
    //--X position, in tiles, of the room in the given slot.
    else if(!strcasecmp(rSwitchType, "Parsed Room X") && tArgs == 2)
    {
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom)
        {
            lua_pushinteger(L, rRoom->mX);
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--Y position, in tiles, of the room in the given slot.
    else if(!strcasecmp(rSwitchType, "Parsed Room Y") && tArgs == 2)
    {
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom)
        {
            lua_pushinteger(L, rRoom->mY);
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--True if room has no encounters, false otherwise.
    else if(!strcasecmp(rSwitchType, "Parsed Room Is Safe") && tArgs == 2)
    {
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom)
        {
            lua_pushboolean(L, rRoom->mIsSafe);
        }
        else
        {
            lua_pushboolean(L, false);
        }
        tReturns = 1;
    }
    //--Danger rating of parsed room.
    else if(!strcasecmp(rSwitchType, "Parsed Room Danger") && tArgs == 2)
    {
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom)
        {
            lua_pushinteger(L, rRoom->mDanger);
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--Which enemy lookup is used for this room.
    else if(!strcasecmp(rSwitchType, "Parsed Room Enemy") && tArgs == 2)
    {
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom)
        {
            lua_pushstring(L, rRoom->mEnemy);
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--Which enemy lookup is used for this room.
    else if(!strcasecmp(rSwitchType, "Parsed Room Music") && tArgs == 2)
    {
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom)
        {
            lua_pushstring(L, rRoom->mMusic);
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--DL Path to use for particle spawning in the parsed room.
    else if(!strcasecmp(rSwitchType, "Parsed Room Particle Path") && tArgs == 2)
    {
        const char *rPush = "Null";
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom) rPush = rRoom->mParticlePath;
        lua_pushstring(L, rPush);
        tReturns = 1;
    }
    //--Chance of spawning a particle each tick in the parsed room.
    else if(!strcasecmp(rSwitchType, "Parsed Room Particle Rate") && tArgs == 2)
    {
        int tPush = 0;
        CarnationRoom *rRoom = rActiveLevel->GetParsedRoomEntry(lua_tointeger(L, 2));
        if(rRoom) tPush = rRoom->mParticleRate;
        lua_pushinteger(L, tPush);
        tReturns = 1;
    }
    //--Returns the name of the room that this room connects to in the provided direction.
    else if(!strcasecmp(rSwitchType, "Parsed Connection From Room In Direction") && tArgs == 3)
    {
        lua_pushstring(L, rActiveLevel->GetParsedConnectionInDirectionFrom(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnationLevel_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_CarnationLevel_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnationLevel_SetProperty("Room Name", sDisplayName)
    //CarnationLevel_SetProperty("Triggers Path", sPath)
    //CarnationLevel_SetProperty("Menu Close Script", sPath)
    //CarnationLevel_SetProperty("Run Autosave")

    //--[Parsing]
    //CarnationLevel_SetProperty("Parse SLF File", sPath)
    //CarnationLevel_SetProperty("Clear SLF Data")

    //--[Examinables]
    //CarnationLevel_SetProperty("Register Examinable", sInternalName, iX, iY, sDisplayName, sScript, sArgument)
    //CarnationLevel_SetProperty("Remove Examinable", sInternalName)
    //CarnationLevel_SetProperty("Register Popup From Examinable", sInternalName, sExaminableName, sDisplayName)
    //CarnationLevel_SetProperty("Register Entry To Popup", sPopupName, sExaminableName, sDisplayName, sScript, sArgument)
    //CarnationLevel_SetProperty("Recompute Popup Positions", sPopupName)
    //CarnationLevel_SetProperty("Close All Popups")
    //CarnationLevel_SetProperty("Set Examinable Always Visible", sExaminableName, bFlag)
    //CarnationLevel_SetProperty("Set Examinable Show Name", sExaminableName, bFlag)
    //CarnationLevel_SetProperty("Set Examinable Highlight Value", sExaminableName, iValue)

    //--[Display]
    //CarnationLevel_SetProperty("Register Background Layer", sName, iRenderPriority, fOffsetX, fOffsetY, sDLPath)
    //CarnationLevel_SetProperty("Sort Background Layers")
    //CarnationLevel_SetProperty("Set BG Layer Visible", sName, bIsVisible, iTicks)
    //CarnationLevel_SetProperty("Set Aura Properties", iSpawnChance, sImagePath)

    //--[Movement]
    //CarnationLevel_SetProperty("Transition To Level", sLevelConstructorPath)

    //--[Alarm]
    //CarnationLevel_SetProperty("Set Alarm Target", iTargetValue)
    //CarnationLevel_SetProperty("Issue Alarm Kick", fDelta)
    //CarnationLevel_SetProperty("Issue Alarm Kick Auto")

    //--[Retreat]
    //CarnationLevel_SetProperty("Set Retreatable", bIsRetreatable)
    //CarnationLevel_SetProperty("Set Retreat Path", sRetreatPath)

    //--[Events]
    //CarnationLevel_SetProperty("Register Event", sText)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnationLevel_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    CarnationLevel *rActiveLevel = CarnationLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("CarnationLevel_SetProperty", L);

    ///--[System]
    //--Display name of the room, this appears on the top of the screen.
    if(!strcasecmp(rSwitchType, "Room Name") && tArgs == 2)
    {
        rActiveLevel->SetLevelName(lua_tostring(L, 2));
    }
    //--Sets firing path of the triggers script.
    else if(!strcasecmp(rSwitchType, "Triggers Path") && tArgs == 2)
    {
        rActiveLevel->SetTriggersPath(lua_tostring(L, 2));
    }
    //--Script that fires whenever the menu is closed.
    else if(!strcasecmp(rSwitchType, "Menu Close Script") && tArgs == 2)
    {
        rActiveLevel->GetMenu()->SetCloseScript(lua_tostring(L, 2));
    }
    //--Immediately run the autosave routine.
    else if(!strcasecmp(rSwitchType, "Run Autosave") && tArgs >= 1)
    {
        rActiveLevel->RunAutosave();
    }
    ///--[Parsing]
    //--Parses an SLF file to allow later querying.
    else if(!strcasecmp(rSwitchType, "Parse SLF File") && tArgs == 2)
    {
        rActiveLevel->ExecCarnationParse(lua_tostring(L, 2));
    }
    //--Clears leftover data from the parsed SLF file.
    else if(!strcasecmp(rSwitchType, "Clear SLF Data") && tArgs == 1)
    {
        rActiveLevel->DeallocateCarnationParse();
    }
    ///--[Examinables]
    //--Registers a new examinable object.
    else if(!strcasecmp(rSwitchType, "Register Examinable") && tArgs == 7)
    {
        rActiveLevel->RegisterExaminable(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5), lua_tostring(L, 6), lua_tostring(L, 7));
    }
    //--Removes the named examinable.
    else if(!strcasecmp(rSwitchType, "Remove Examinable") && tArgs == 2)
    {
        rActiveLevel->RemoveExaminable(lua_tostring(L, 2));
    }
    //--Creates a popup window using the position of the examinable provided.
    else if(!strcasecmp(rSwitchType, "Register Popup From Examinable") && tArgs == 4)
    {
        rActiveLevel->RegisterPopupFromExaminable(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Appends an entry to the named popup.
    else if(!strcasecmp(rSwitchType, "Register Entry To Popup") && tArgs == 6)
    {
        rActiveLevel->AddEntryToPopup(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
    }
    //--Once a popup is done registering entries, recompute its position and those of each button.
    else if(!strcasecmp(rSwitchType, "Recompute Popup Positions") && tArgs == 2)
    {
        rActiveLevel->RecomputePopupSizes(lua_tostring(L, 2));
    }
    //--Orders all popups to close.
    else if(!strcasecmp(rSwitchType, "Close All Popups") && tArgs == 1)
    {
        rActiveLevel->CloseAllPopups();
    }
    //--If true, examinable renders even if not discovered.
    else if(!strcasecmp(rSwitchType, "Set Examinable Always Visible") && tArgs == 3)
    {
        rActiveLevel->SetExaminableAlwaysVisible(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--If true, examinable renders its name above the heart.
    else if(!strcasecmp(rSwitchType, "Set Examinable Show Name") && tArgs == 3)
    {
        rActiveLevel->SetExaminableShowsDisplayName(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets the value the examinable changes the highlight to when moused over.
    else if(!strcasecmp(rSwitchType, "Set Examinable Highlight Value") && tArgs == 3)
    {
        rActiveLevel->SetExaminableHighlightValue(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    ///--[Display]
    //--Registers a background layer. Lower priority renders first and therefore in the background.
    else if(!strcasecmp(rSwitchType, "Register Background Layer") && tArgs == 6)
    {
        rActiveLevel->RegisterBackgroundLayer(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tostring(L, 6));
    }
    //--Once done registering background layers, sort them so they display correctly.
    else if(!strcasecmp(rSwitchType, "Sort Background Layers") && tArgs == 1)
    {
        rActiveLevel->SortBackgroundLayers();
    }
    //--Toggles if a layer is visible over time. Pass 0 ticks to make the change instant.
    else if(!strcasecmp(rSwitchType, "Set BG Layer Visible") && tArgs == 4)
    {
        rActiveLevel->SetBackgroundLayerVisible(lua_tostring(L, 2), lua_toboolean(L, 3), lua_tointeger(L, 4));
    }
    //--Sets spawning aura particle properties.
    else if(!strcasecmp(rSwitchType, "Set Aura Properties") && tArgs == 3)
    {
        rActiveLevel->SetAuraProperties(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    ///--[Movement]
    //--Begins transition proceedings to the constructor. Since maps have no built-in data, only a lua path is needed.
    else if(!strcasecmp(rSwitchType, "Transition To Level") && tArgs == 2)
    {
        rActiveLevel->BeginTransitionTo(lua_tostring(L, 2));
    }
    ///--[Alarm]
    //--Sets the target threshold for the alarm. Activation levels are 25, 45, and 65.
    else if(!strcasecmp(rSwitchType, "Set Alarm Target") && tArgs == 2)
    {
        rActiveLevel->SetAlarmTarget(lua_tointeger(L, 2), 15);
    }
    //--Pushes the alarm by the given value. This is the shake angle, it does not affect alarm activation.
    else if(!strcasecmp(rSwitchType, "Issue Alarm Kick") && tArgs == 2)
    {
        rActiveLevel->KickAlarm(lua_tonumber(L, 2));
    }
    //--Pushes the alarm with the automatic push strength used when the alarm advances one level.
    else if(!strcasecmp(rSwitchType, "Issue Alarm Kick Auto") && tArgs == 1)
    {
        rActiveLevel->KickAlarmAuto();
    }
    ///--[Retreat]
    //--If true, the retreat button will appear on the world UI.
    else if(!strcasecmp(rSwitchType, "Set Retreatable") && tArgs >= 2)
    {
        rActiveLevel->SetRetreatable(lua_toboolean(L, 2));
    }
    //--Script called when the player retreats.
    else if(!strcasecmp(rSwitchType, "Set Retreat Path") && tArgs >= 2)
    {
        rActiveLevel->SetRetreatScript(lua_tostring(L, 2));
    }
    ///--[Events]
    //--Creates a new event notification.
    else if(!strcasecmp(rSwitchType, "Register Event") && tArgs >= 2)
    {
        rActiveLevel->CreateEventNotification(lua_tostring(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnationLevel_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_CarnDia_GetProperty(lua_State *L)
{
    ///--[Documentation]
    //--Identical in every way to WD_SetProperty(), except switches the dialogue with the local CarnationLevel
    //  dialogue. This second version of the dialogue refers to the examination window in the middle of the screen.

    ///--[Level Verify]
    //--Type check.
    CarnationLevel *rActiveLevel = CarnationLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("CarnDia_GetProperty", L);

    ///--[Get Pointers]
    MapManager *rMapManager = MapManager::Fetch();
    WorldDialogue *rNormalDialogue = rMapManager->GetWorldDialogue();
    WorldDialogue *rLevelDialogue = rActiveLevel->GetInternalDialogue();

    ///--[Switcheroo]
    rMapManager->TemporarilySwitchWorldDialogue(rLevelDialogue);
    int tReturns = Hook_WD_GetProperty(L);
    rMapManager->TemporarilySwitchWorldDialogue(rNormalDialogue);

    ///--[Clean]
    return tReturns;
}
int Hook_CarnDia_SetProperty(lua_State *L)
{
    ///--[Documentation]
    //--Identical in every way to WD_SetProperty(), except switches the dialogue with the local CarnationLevel
    //  dialogue. This second version of the dialogue refers to the examination window in the middle of the screen.

    ///--[Level Verify]
    //--Type check.
    CarnationLevel *rActiveLevel = CarnationLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("CarnDia_SetProperty", L);

    ///--[Get Pointers]
    MapManager *rMapManager = MapManager::Fetch();
    WorldDialogue *rNormalDialogue = rMapManager->GetWorldDialogue();
    WorldDialogue *rLevelDialogue = rActiveLevel->GetInternalDialogue();

    ///--[Switcheroo]
    rMapManager->TemporarilySwitchWorldDialogue(rLevelDialogue);
    Hook_WD_SetProperty(L);
    rMapManager->TemporarilySwitchWorldDialogue(rNormalDialogue);

    ///--[Clean]
    return 0;
}
int Hook_CarnAppend(lua_State *L)
{
    ///--[Documentation]
    //--Same as the shortcut Append() function but routes it to the CarnationLevel's local dialogue.

    ///--[Level Verify]
    //--Type check.
    CarnationLevel *rActiveLevel = CarnationLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("CarnDia_SetProperty", L);

    ///--[Get Pointers]
    MapManager *rMapManager = MapManager::Fetch();
    WorldDialogue *rNormalDialogue = rMapManager->GetWorldDialogue();
    WorldDialogue *rLevelDialogue = rActiveLevel->GetInternalDialogue();

    ///--[Switcheroo]
    rMapManager->TemporarilySwitchWorldDialogue(rLevelDialogue);
    Hook_WD_AppendShorthand(L);
    rMapManager->TemporarilySwitchWorldDialogue(rNormalDialogue);

    ///--[Clean]
    return 0;
}
