///=================================== PeakFreaksPuzzlePiece =======================================
//--Represents a puzzle piece on the Peak Freaks game UI.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define PEAKFREAK_PUZZLE_TYPE_NONE -1
#define PEAKFREAK_PUZZLE_TYPE_ARROW_DN 0
#define PEAKFREAK_PUZZLE_TYPE_ARROW_UP 1
#define PEAKFREAK_PUZZLE_TYPE_ARROW_LF 2
#define PEAKFREAK_PUZZLE_TYPE_ARROW_RT 3
#define PEAKFREAK_PUZZLE_TYPE_MAX 4

///========================================== Classes =============================================
class PeakFreaksPuzzlePiece
{
    private:
    ///--[System]
    int mPuzzleType;
    int mPower;
    int mOffsetTick;

    ///--[Position]
    EasingPack2D mPosition;

    protected:

    public:
    //--System
    PeakFreaksPuzzlePiece();
    ~PeakFreaksPuzzlePiece();

    //--Public Variables
    //--Property Queries
    int GetPuzzleType();
    int GetPower();
    EasingPack2D GetPosition();
    int GetOffsetTick();

    //--Manipulators
    void SetPuzzleType(int pType);
    void SetPower(int pPower);
    void SetPosition(float pX, float pY);
    void MoveTo(float pX, float pY, int pTicks);
    void SetOffsetTick(int pTick);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


