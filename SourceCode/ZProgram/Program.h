//--[Program.h]
//--Header for the entry point file.

#pragma once

#include "Definitions.h"
#include "Structures.h"

int main(int pArgsTotal, char **pArgs);
void LogFPS(MainPackage &sMainPackage);
