//--Base
#include "CarnationLevel.h"

//--Classes
#include "AdventureMenu.h"
#include "CarnationPopup.h"
#include "CarnationMinimap.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "CarnationDef.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "MapManager.h"
#include "SaveManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
CarnationLevel::CarnationLevel()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--Type override.
    mType = POINTER_TYPE_CARNATIONLEVEL;

    ///--[ ======= RootLevel ======== ]
    ///--[ ===== CarnationLevel ===== ]
    ///--[System]
    mCurrentAreaName = InitializeString("Unnamed Area");
    mTriggersPath = NULL;

    ///--[Description Window]
    mInternalDialogue = new WorldDialogue();

    ///--[Examinable Objects]
    rHighlightedExaminable = NULL;
    mExaminablesList = new StarLinkedList(true);

    ///--[Input]
    //--Mouse Coordinates
    mMouseX = 0;
    mMouseY = 0;
    mMouseZ = 0;

    //--Fixed buttons.
    mCharacterA    .SetWH(13.0f,  56.0f, 219.0f, 149.0f);
    mCharacterB    .SetWH(13.0f, 213.0f, 219.0f, 149.0f);
    mCharacterC    .SetWH(13.0f, 370.0f, 219.0f, 149.0f);
    mButtonJournal .SetWH( 0.0f, 536.0f, 192.0f,  55.0f);
    mButtonBackpack.SetWH( 0.0f, 597.0f, 192.0f,  55.0f);
    mButtonOptions .SetWH( 0.0f, 658.0f, 192.0f,  55.0f);

    //--Popup Menu
    mPopupList = new StarLinkedList(true);

    ///--[Level Transition]
    mIsLevelTransitioning = true;
    mTransitionToScript = NULL;
    mTransitionTimer = 0;
    mTransitionStage = CL_TRANSITION_FADEIN;

    ///--[Alarm, Hostility, Encounters]
    //--Alarm State
    mAlarmVal.Initialize();
    mAlarmVal.MoveTo(0.0f, 0);
    mAlarmShake = 0.0f;
    mAlarmShakeDelta = 0.0f;
    mAlarmShakeCap = 50.0f;
    mAlarmShakePush = 10.0f;
    mAlarmShakeZeroTicks = 0;

    ///--[Dungeon Retreat]
    mCanRetreat = false;
    mIsConfirmRetreat = false;
    mConfirmRetreatTimer = 0;
    mRetreatScript = NULL;
    mButtonConfirmRetreat.SetWH(363.0f, 409.0f, 228.0f, 61.0f);
    mButtonCancelRetreat. SetWH(776.0f, 409.0f, 228.0f, 61.0f);

    ///--[Event Notifications]
    mEventNotificationList = new StarLinkedList(true);

    ///--[GUI]
    //--Menu.
    mMenu = MapManager::Fetch()->GenerateNewMenu();

    //--Combat Handling
    mCombatTimer = 0;

    //--Minimap
    mIsMinimapMode = false;
    mIsMinimapDrag = false;
    mMinimapDragX = 0;
    mMinimapDragY = 0;
    mMinimapTimer = 0;
    mMinimapOffsetX = 0.0f;
    mMinimapOffsetY = 0.0f;
    mMinimap = new CarnationMinimap();

    //--Auras
    mSpawnAuraChance = 0;
    rSpawnAuraImage = NULL;
    mAuraParticleList = new StarLinkedList(true);

    ///--[Queryable SLF Data]
    mQuerySLFName = NULL;
    mQuerySLFImage = NULL;
    mQuerySLFRoomInfo = NULL;
    mQuerySLFConnectionInfo = NULL;

    ///--[Images]
    //--Layers
    mBackgroundLayers = new StarLinkedList(true);

    //--Static images.
    memset(&Images, 0, sizeof(Images));

    ///--[ ================ Construction ================ ]
    ///--[Internal Dialogue]
    mInternalDialogue->Construct();
    mInternalDialogue->SetDialogueMaxWidth(845.0f);
    mInternalDialogue->ReplaceMainlineFont("Carn Dialogue");

    ///--[External Dialogue]
    WorldDialogue *rExternalDialogue = WorldDialogue::Fetch();
    rExternalDialogue->SetDecisionUsesMouse(true);
    rExternalDialogue->SetMajorSceneDarkening(0.80f);
    rExternalDialogue->ReplaceMainlineFont("Carn Dialogue");
    rExternalDialogue->ReplaceDecisionFont("Carn Dialogue Decision");
    rExternalDialogue->ReplaceNamelessBox("Root/Images/CarnationUI/World/Frame_DialogueReplace");
    rExternalDialogue->ReplaceDecisionBorderCard("Root/Images/CarnationUI/World/Frame_MouseDecision");

    ///--[Images]
    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Resolve fonts.
    Images.Data.rFontTitle         = rDataLibrary->GetFont("Carn World Title");
    Images.Data.rFontPopup         = rDataLibrary->GetFont("Carn World Popup");
    Images.Data.rFont_Tabs         = rDataLibrary->GetFont("Carn World Tabs");
    Images.Data.rFont_MinimapHelp  = rDataLibrary->GetFont("Carn Minimap Help");
    Images.Data.rFont_MinimapTitle = rDataLibrary->GetFont("Carn Minimap Title");

    //--Resolve images.
    Images.Data.rButton_CenterOnPlayer   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Button_CenterOnPlayer");
    Images.Data.rFrameDialogue           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_Dialogue");
    Images.Data.rFrameExaminableNameBack = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_ExaminableNameBack");
    Images.Data.rFrameMinimapMask        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_MinimapMask");
    Images.Data.rFrameMouseDecision      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_MouseDecision");
    Images.Data.rFramePartyBack          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_PartyBack");
    Images.Data.rFramePartyFront         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_PartyFront");
    Images.Data.rFramePopupBacking       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_PopupBacking");
    Images.Data.rFrameRightBlock         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_RightBlock");
    Images.Data.rFrameRoomName           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Frame_RoomName");
    Images.Data.rOverlayAdvance          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Overlay_Advance");
    Images.Data.rOverlayAlarm0           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Overlay_Alarm0");
    Images.Data.rOverlayAlarm1           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Overlay_Alarm1");
    Images.Data.rOverlayAlarm2           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Overlay_Alarm2");
    Images.Data.rOverlayAlarm3           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Overlay_Alarm3");
    Images.Data.rOverlayHeart            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Overlay_Heart");
    Images.Data.rTabBackpack             = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Tab_Backpack");
    Images.Data.rTabJournal              = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Tab_Journal");
    Images.Data.rTabOptions              = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/World/Tab_Options");

    ///--[Verify]
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    ///--[Debug]
    //--If the images failed to resolve, try to establish why by re-running the verifications.
    if(!Images.mIsReady)
    {
        //--Heading.
        fprintf(stderr, "=== Carnation Level Images Failed to Resolve ===\n");

        //--Re-run verifications with the debug flag set to true.
        fprintf(stderr, "== Base:\n");
        VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), true);
    }
}
CarnationLevel::~CarnationLevel()
{
    ///--[System]
    free(mCurrentAreaName);
    free(mTriggersPath);

    ///--[Description Window]
    delete mInternalDialogue;

    ///--[Examinable Objects]
    delete mExaminablesList;

    ///--[Input]
    //--Mouse Coordinates
    //--Popup Menu
    delete mPopupList;

    ///--[Level Transitioning]
    free(mTransitionToScript);

    ///--[Dungeon Retreat]
    free(mRetreatScript);

    ///--[Event Notifications]
    delete mEventNotificationList;

    ///--[GUI]
    delete mMenu;
    delete mMinimap;
    delete mAuraParticleList;

    ///--[Queryable SLF Info]
    free(mQuerySLFName);
    free(mQuerySLFImage);
    delete mQuerySLFRoomInfo;
    delete mQuerySLFConnectionInfo;

    ///--[Images]
    delete mBackgroundLayers;
}

///===================================== Property Queries =========================================
bool CarnationLevel::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_CARNATIONLEVEL) return true;
    if(pType == POINTER_TYPE_ROOTLEVEL) return true;
    return false;
}
bool CarnationLevel::AreAnyPopupsAcceptingClicks()
{
    //--Scans all popups. If any are able to accept a click, returns true. A popup in the process of
    //  hiding does not return true.
    CarnationPopup *rClickPopup = (CarnationPopup *)mPopupList->PushIterator();
    while(rClickPopup)
    {
        //--Check if the popup is able to handle a click.
        if(rClickPopup->IsShowing())
        {
            mPopupList->PopIterator();
            return true;
        }

        //--Next.
        rClickPopup = (CarnationPopup *)mPopupList->AutoIterate();
    }

    //--All checks failed.
    return false;
}
bool CarnationLevel::IsLayeringMusic()
{
    return false;
}

///======================================= Manipulators ===========================================
void CarnationLevel::SetTriggersPath(const char *pPath)
{
    //--Deallocate, reset.
    free(mTriggersPath);
    mTriggersPath = NULL;
    if(!pPath || !strcasecmp(pPath, "Null")) return;

    //--Set.
    ResetString(mTriggersPath, pPath);
}
void CarnationLevel::RegisterBackgroundLayer(const char *pName, int pRenderOrder, float pXOffset, float pYOffset, const char *pDLPath)
{
    //--Argument check.
    if(!pName || !pDLPath) return;

    //--Create and set.
    CarnMapLayer *nLayer = (CarnMapLayer *)starmemoryalloc(sizeof(CarnMapLayer));
    nLayer->Initialize();
    nLayer->SetVisible(true, 1);
    nLayer->mRenderPriority = pRenderOrder;
    nLayer->mOffsetX = pXOffset;
    nLayer->mOffsetY = pYOffset;
    nLayer->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--Register.
    mBackgroundLayers->AddElementAsTail(pName, nLayer, &CarnMapLayer::DeleteThis);
}
void CarnationLevel::SetBackgroundLayerVisible(const char *pName, bool pIsVisible, int pTicks)
{
    //--Existence check.
    CarnMapLayer *rLayer = (CarnMapLayer *)mBackgroundLayers->GetElementByName(pName);
    if(!rLayer) return;

    //--Set.
    rLayer->SetVisible(pIsVisible, pTicks);
}
void CarnationLevel::SetLevelName(const char *pName)
{
    //--Pass "Null" to clear the name.
    if(!strcasecmp(pName, "Null"))
    {
        ResetString(mCurrentAreaName, NULL);
        return;
    }

    //--Set.
    ResetString(mCurrentAreaName, pName);
}
void CarnationLevel::SetAlarmTarget(int pValue, int pTicks)
{
    mAlarmVal.MoveTo(pValue, pTicks);
}
void CarnationLevel::KickAlarm(float pPower)
{
    mAlarmShakeDelta = pPower;
}
void CarnationLevel::KickAlarmAuto()
{
    //--Shake value is negative, issue a positive kick.
    if(mAlarmShake < 0.0f)
    {
        mAlarmShakeDelta = mAlarmShakeDelta + mAlarmShakePush;
    }
    //--Positive, issue a negative kick.
    else
    {
        mAlarmShakeDelta = mAlarmShakeDelta - mAlarmShakePush;
    }
}
void CarnationLevel::SetAuraProperties(int pSpawnChance, const char *pParticleDLPath)
{
    mSpawnAuraChance = pSpawnChance;
    rSpawnAuraImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pParticleDLPath);
}
void CarnationLevel::SetRetreatable(bool pIsRetreatable)
{
    mCanRetreat = pIsRetreatable;
}
void CarnationLevel::SetRetreatScript(const char *pPath)
{
    ResetString(mRetreatScript, pPath);
}

///================================== Examinables and Popups ======================================
bool CarnationLevel::IsExaminableAt(float pX, float pY)
{
    ///--[Documentation]
    //--Returns true if an examinable matches the exact location. Used to make sure newly spawned events
    //  don't overlap existing ones.
    CarnExaminable *rExaminable = (CarnExaminable *)mExaminablesList->PushIterator();
    while(rExaminable)
    {
        //--Check:
        if(rExaminable->mHeartX == pX - 20 && rExaminable->mHeartY == pY - 18)
        {
            mExaminablesList->PopIterator();
            return true;
        }


        //--Next.
        rExaminable = (CarnExaminable *)mExaminablesList->AutoIterate();
    }

    //--No matches.
    return false;
}
bool CarnationLevel::DoesExaminableExist(const char *pInternalName)
{
    return (mExaminablesList->GetElementByName(pInternalName) != NULL);
}
void CarnationLevel::RegisterExaminable(const char *pInternalName, int pX, int pY, const char *pDisplayName, const char *pScript, const char *pArgument)
{
    //--Arg check.
    if(!pInternalName || !pDisplayName || !pScript || !pArgument) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    CarnExaminable *nExaminable = (CarnExaminable *)starmemoryalloc(sizeof(CarnExaminable));
    nExaminable->Initialize();

    //--Set values.
    nExaminable->mHeartX = pX - 20;
    nExaminable->mHeartR = pX + 20;
    nExaminable->mHeartY = pY - 18;
    nExaminable->mHeartB = pY + 18;
    ResetString(nExaminable->mDisplayName, pDisplayName);
    ResetString(nExaminable->mScript, pScript);
    ResetString(nExaminable->mArgument, pArgument);

    //--Register.
    mExaminablesList->AddElement(pInternalName, nExaminable, &CarnExaminable::DeleteThis);

    //--Check if this object is already discovered. This is done by checking for a variable in the
    //  DataLibrary, which is of the pattern: "Root/Variables/Carnation|Discoveries/Levelname/Objectname"
    //  If the variable exists, the object is discovered. Otherwise, the timer defaults to zero.
    char tBuffer[128];
    sprintf(tBuffer, "Root/Variables/Carnation|Discoveries/%s/%s", mCurrentAreaName, pInternalName);
    if(DataLibrary::Fetch()->DoesEntryExist(tBuffer))
    {
        nExaminable->mDiscoveryTimer = CARNEXAMINE_DISCOVERY_TICKS;
    }
}
void CarnationLevel::RemoveExaminable(const char *pInternalName)
{
    mExaminablesList->RemoveElementS(pInternalName);
}
void CarnationLevel::RegisterPopupFromExaminable(const char *pInternalName, const char *pExaminableName, const char *pDisplayName)
{
    //--Arg check.
    if(!pInternalName || !pExaminableName || !pDisplayName) return;

    //--Check that the examinable exists.
    CarnExaminable *rExaminable = (CarnExaminable *)mExaminablesList->GetElementByName(pExaminableName);
    if(!rExaminable)
    {
        fprintf(stderr, "CarnationLevel::RegisterPopupFromExaminable(), error. No examinable %s was found. Failing.\n", pExaminableName);
        return;
    }

    //--Create.
    CarnationPopup *nPopup = new CarnationPopup();
    nPopup->SetPosition(rExaminable->mHeartX+REGPOPUP_HEART_OFF_X, rExaminable->mHeartY);
    nPopup->SetTitle(pDisplayName);

    //--Register.
    mPopupList->AddElement(pInternalName, nPopup, &CarnationPopup::DeleteThis);
}
void CarnationLevel::AddEntryToPopup(const char *pPopupName, const char *pInternalName, const char *pDisplayName, const char *pScript, const char *pArgument)
{
    //--Appends a new examinable entry to a popup menu.
    if(!pPopupName || !pInternalName || !pDisplayName || !pScript || !pArgument) return;

    //--Locate the popup.
    CarnationPopup *rPopup = (CarnationPopup *)mPopupList->GetElementByName(pPopupName);
    if(!rPopup) return;

    //--Set.
    rPopup->RegisterExaminable(pInternalName, pDisplayName, pScript, pArgument);
}
void CarnationLevel::RecomputePopupSizes(const char *pPopupName)
{
    //--Orders the named popup menu to recompute sizes using the local fonts.
    if(!pPopupName) return;
    CarnationPopup *rPopup = (CarnationPopup *)mPopupList->GetElementByName(pPopupName);
    if(!rPopup) return;

    //--Set.
    rPopup->RecomputePositions(Images.Data.rFontPopup);
}
void CarnationLevel::CloseAllPopups()
{
    CarnationPopup *rPopup = (CarnationPopup *)mPopupList->PushIterator();
    while(rPopup)
    {
        rPopup->SetVisible(false);
        rPopup = (CarnationPopup *)mPopupList->AutoIterate();
    }
}
void CarnationLevel::SetExaminableAlwaysVisible(const char *pInternalName, bool pFlag)
{
    //--Sets the examinable in question to always be visible, regardless of being moused over.
    CarnExaminable *rExaminable = (CarnExaminable *)mExaminablesList->GetElementByName(pInternalName);
    if(!rExaminable) return;

    //--Set.
    rExaminable->mAlwaysVisible = pFlag;
}
void CarnationLevel::SetExaminableShowsDisplayName(const char *pInternalName, bool pFlag)
{
    //--Sets the examinable in question to always show its display name even when not selected.
    CarnExaminable *rExaminable = (CarnExaminable *)mExaminablesList->GetElementByName(pInternalName);
    if(!rExaminable) return;

    //--Set.
    rExaminable->mAlwaysShowName = pFlag;
}
void CarnationLevel::SetExaminableHighlightValue(const char *pInternalName, int pValue)
{
    //--Sets the examinable's alarm value when highlighted.
    CarnExaminable *rExaminable = (CarnExaminable *)mExaminablesList->GetElementByName(pInternalName);
    if(!rExaminable) return;

    //--Set.
    rExaminable->mHighlightVal = pValue;
}

///======================================= Core Methods ===========================================
void CarnationLevel::SortBackgroundLayers()
{
    mBackgroundLayers->SortListUsing(&CarnationLevel::CompareBackgroundLayer);
}
int CarnationLevel::CompareBackgroundLayer(const void *pEntryA, const void *pEntryB)
{
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;
    CarnMapLayer *rLayerA = (CarnMapLayer *)(*rEntryA)->rData;
    CarnMapLayer *rLayerB = (CarnMapLayer *)(*rEntryB)->rData;
    return rLayerA->mRenderPriority - rLayerB->mRenderPriority;
}
void CarnationLevel::BeginTransitionTo(const char *pConstructorPath)
{
    ///--[Documentation]
    //--Change to a new level.

    ///--[Mid-Transition]
    //--Level is already transitioning. Change the destination. If this function is called before the
    //  transition reaches the end of the fadeout, it will continue as if nothing happened.
    if(mIsLevelTransitioning)
    {
        if(mTransitionStage < CL_TRANSITION_LEVELSWITCH)
        {
            ResetString(mTransitionToScript, pConstructorPath);
        }
        return;
    }

    ///--[New Transition]
    //--Begin a new transition.
    mIsLevelTransitioning = true;
    ResetString(mTransitionToScript, pConstructorPath);
    mTransitionTimer = 0;
    mTransitionStage = CL_TRANSITION_FADEOUT;
}
void CarnationLevel::ClearOldLevelData()
{
    //--Clears the level data during a transition. Do NOT use this while anything is iterating
    //  or rendering unless you like causing crashes.
    SetTriggersPath(NULL);
    mExaminablesList->ClearList();
    mPopupList->ClearList();
    mBackgroundLayers->ClearList();
}
void CarnationLevel::RunAutosave()
{
    ///--[Documentation]
    //--Called by scripts in Carnation whenever the player exits a witch lair, creates a new autosave.

    ///--[Mark as Autosave]
    //--Setting this to 1.0f will mark this save as an autosave.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rIsAutosaveVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/Autosave/iIsAutosave");
    if(!rIsAutosaveVar) return;
    rIsAutosaveVar->mNumeric = 1.0f;

    ///--[Option Check]
    //--Make sure the autosave number is at least 1. If it's zero, never autosave.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    int cMaxAutosaves = rOptionsManager->GetOptionI("Max Autosaves");

    //--Clamp.
    if(cMaxAutosaves > 100) cMaxAutosaves = 100;

    //--No autosaves are being used, stop execution.
    if(cMaxAutosaves < 1) return;

    ///--[Run Save Routine]
    //--Order the SaveManager to run the save routine.
    SaveManager *rSaveManager = SaveManager::Fetch();
    rSaveManager->SetSavegameName("Autosave");

    //--Game Directory:
    const char *rAdventureDir = rDataLibrary->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

    //--Go to the autosave directory and rename autosaves up.
    for(int i = cMaxAutosaves - 2; i >= 0; i --)
    {
        //--Path setup.
        char *tFileBufOld = InitializeString("%s/../../Saves/Autosave/Autosave%02i.slf", rAdventureDir, i+0);
        char *tFileBufNew = InitializeString("%s/../../Saves/Autosave/Autosave%02i.slf", rAdventureDir, i+1);

        //--Delete the destination file.
        remove(tFileBufNew);

        //--Rename.
        rename(tFileBufOld, tFileBufNew);

        //--Clean.
        free(tFileBufOld);
        free(tFileBufNew);
    }

    //--Write to the file.
    char *tFileBuf = InitializeString("%s/../../Saves/Autosave/Autosave00.slf", rAdventureDir);
    rSaveManager->SaveAdventureFile(tFileBuf);

    //--Clean.
    free(tFileBuf);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
CarnationMinimap *CarnationLevel::GetMinimap()
{
    return mMinimap;
}
WorldDialogue *CarnationLevel::GetInternalDialogue()
{
    return mInternalDialogue;
}
AdventureMenu *CarnationLevel::GetMenu()
{
    return mMenu;
}

///===================================== Static Functions =========================================
CarnationLevel *CarnationLevel::Fetch()
{
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_CARNATIONLEVEL)) return NULL;
    return (CarnationLevel *)rCheckLevel;
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
