///================================= AdventureMenuAchievements ====================================
//--Structures used by achievements in the journal menu.
#ifndef ADVUIJOURNAL_ACHIEVE_TYPES
#define ADVUIJOURNAL_ACHIEVE_TYPES

#include "StarLinkedList.h"

///--[Constants]
#define ADVMENU_JOURNAL_ACHIEVEMENT_PER_PAGE 7
#define ADVMENU_JOURNAL_ACHIEVEMENT_SCROLL_TICKS 15

///=================================== Achievement Category =======================================
//--Stores a set of achievements. Used to sort the list. Achievements that are being unlocked will
//  always scan all subcategories, so names must be unique between categories.
typedef struct AdvMenuJournalAchievementCategory
{
    //--System
    StarLinkedList *mAchievementList; //AdvMenuJournalAchievementEntry *, master

    //--Display
    StarBitmap *rIcon;
    char *mDisplayName;

    //--Functions
    void Initialize()
    {
        mAchievementList = new StarLinkedList(true);
        rIcon = NULL;
        mDisplayName = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuJournalAchievementCategory *rPtr = (AdvMenuJournalAchievementCategory *)pPtr;
        delete rPtr->mAchievementList;
        free(rPtr->mDisplayName);
        free(rPtr);
    }
}AdvMenuJournalAchievementCategory;

///=================================== Achievement Progress =======================================
//--Represents progress towards some achievement. Is always a floating point but can be rendered
//  as integers or checkboxes if flagged.
typedef struct AdvMenuJournalAchievementProgress
{
    char *mDisplayName;
    bool mDisplayAsInteger;
    bool mDisplayAsCheckbox;
    float mValueCur;
    float mValueMax;
}AdvMenuJournalAchievementProgress;

///===================================== Achievement Entry ========================================
//--Represents an achievement in various states. Achievements are stored in a special savefile and
//  exist between game files.
typedef struct AdvMenuJournalAchievementEntry
{
    //--System
    bool mIsUnlocked;
    bool mHidesWhenNotUnlocked;
    bool mIsSpoilered;
    char *mUnlockTimestamp;

    //--Progress Cases
    int mProgressEntriesTotal; //Note: Can be zero
    AdvMenuJournalAchievementProgress *mProgressEntries;

    //--Display
    StarBitmap *rImage;
    char *mDisplayTitle;
    int mDescriptionLines;
    char **mDescription;

    //--Functions
    void Initialize()
    {
        mIsUnlocked = false;
        mHidesWhenNotUnlocked = false;
        mIsSpoilered = false;
        mUnlockTimestamp = NULL;
        mProgressEntriesTotal = 0;
        mProgressEntries = NULL;
        rImage = NULL;
        mDisplayTitle = NULL;
        mDescriptionLines = 0;
        mDescription = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuJournalAchievementEntry *rPtr = (AdvMenuJournalAchievementEntry *)pPtr;
        free(rPtr->mUnlockTimestamp);
        for(int i = 0; i < rPtr->mProgressEntriesTotal; i ++) free(rPtr->mProgressEntries[i].mDisplayName);
        free(rPtr->mProgressEntries);
        free(rPtr->mDisplayTitle);
        for(int i = 0; i < rPtr->mDescriptionLines; i ++) free(rPtr->mDescription[i]);
        free(rPtr->mDescription);
        free(rPtr);
    }
}AdvMenuJournalAchievementEntry;
#endif // ADVUIJOURNAL_ACHIEVE_TYPES
