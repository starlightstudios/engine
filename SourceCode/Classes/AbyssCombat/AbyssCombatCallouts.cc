//--Base
#include "AbyssCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "utf8.h"

//--Libraries
//--Managers

///========================================== System ==============================================
void CalloutPack::Initialize()
{
    mUserID = 0;
    mTextTimer = 0;
    mRemoveTicks = 1;
    mIsBackwards = false;
    mIsUpDown = false;
    mX = 0.0f;
    mY = 0.0f;
    mW = 1.0f;
    mH = 1.0f;
    mLinesTotal = 0;
    mLines = NULL;
}
void CalloutPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    CalloutPack *rPtr = (CalloutPack *)pPtr;
    for(int i = 0; i < rPtr->mLinesTotal; i ++) free(rPtr->mLines[i]);
    free(rPtr->mLines);
    free(rPtr);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AbyssCombat::RegisterCallout(const char *pName)
{
    ///--[Documentation]
    //--Adds a new callout and registers it on the tail of the list. Further setup can be done using
    //  the below functions, which implicitly operate on the list tail.
    if(!pName) return;

    //--Create, register.
    CalloutPack *nCalloutPack = (CalloutPack *)starmemoryalloc(sizeof(CalloutPack));
    nCalloutPack->Initialize();
    mCalloutList->AddElementAsTail(pName, nCalloutPack, &CalloutPack::DeleteThis);
}
void AbyssCombat::SetCalloutUserID(uint32_t pID)
{
    ///--[Documentation]
    //--Sets which entity the callout positions itself relative to. That the entity exists is not checked
    //  until later. It will be removed during update if the entity is not found.
    CalloutPack *rLastPack = (CalloutPack *)mCalloutList->GetTail();
    if(!rLastPack) return;

    //--Set.
    rLastPack->mUserID = pID;
}
void AbyssCombat::AllocateCalloutLines(int pLines)
{
    ///--[Documentation]
    //--Allocates space for the text lines on a callout. Pass zero to deallocate all lines.
    CalloutPack *rLastPack = (CalloutPack *)mCalloutList->GetTail();
    if(!rLastPack) return;

    //--Deallocate.
    for(int i = 0; i < rLastPack->mLinesTotal; i ++) free(rLastPack->mLines[i]);
    free(rLastPack->mLines);

    //--Zero.
    rLastPack->mLinesTotal = 0;
    rLastPack->mLines = NULL;
    if(pLines < 1) return;

    //--Allocate.
    rLastPack->mLinesTotal = pLines;
    rLastPack->mLines = (char **)starmemoryalloc(sizeof(char *) * rLastPack->mLinesTotal);
    for(int i = 0; i < rLastPack->mLinesTotal; i ++) rLastPack->mLines[i] = NULL;
}
void AbyssCombat::SetCalloutLine(int pSlot, const char *pText)
{
    ///--[Documentation]
    //--Sets text lines.
    if(!pText) return;

    //--Locate pack.
    CalloutPack *rLastPack = (CalloutPack *)mCalloutList->GetTail();
    if(!rLastPack) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rLastPack->mLinesTotal) return;

    //--Set.
    ResetString(rLastPack->mLines[pSlot], pText);
}
void AbyssCombat::FlipCalloutX()
{
    ///--[Documentation]
    //--Causes the callout to face to the right instead of the default left.
    CalloutPack *rLastPack = (CalloutPack *)mCalloutList->GetTail();
    if(!rLastPack) return;

    //--Set.
    rLastPack->mIsBackwards = true;
}
void AbyssCombat::FlipCalloutY()
{
    ///--[Documentation]
    //--Causes the callout to face to the right instead of the default left.
    CalloutPack *rLastPack = (CalloutPack *)mCalloutList->GetTail();
    if(!rLastPack) return;

    //--Set.
    rLastPack->mIsUpDown = true;
}
void AbyssCombat::PositionCallout(float pXTarget, float pYTarget)
{
    ///--[Documentation]
    //--Repositions the callout to the given position. This position is where the speech bubble is
    //  "originating" from. That is, the part the little triangle points to.
    //--You should set the lines first so it knows how wide to be.
    CalloutPack *rLastPack = (CalloutPack *)mCalloutList->GetTail();
    if(!rLastPack) return;

    //--Constants
    float cLftIndent = 11.0f;
    float cTopIndent = 11.0f;
    float cRgtIndent = 11.0f;
    //float cBotIndent = 11.0f;

    //--Text width.
    int tLettersTotal = 0;
    float tWidestLine = 0.0f;
    for(int i = 0; i < rLastPack->mLinesTotal; i ++)
    {
        if(!rLastPack->mLines[i]) continue;
        tLettersTotal += strlen(rLastPack->mLines[i]);
        float tNewWid = AbyssImg.Data.rFontCallout->GetTextWidth(rLastPack->mLines[i]);
        if(tWidestLine < tNewWid) tWidestLine = tNewWid;
    }

    //--Set.
    rLastPack->mX = pXTarget;
    rLastPack->mY = pYTarget - cTopIndent - (rLastPack->mLinesTotal * AbyssImg.Data.rFontCallout->GetTextHeight());
    rLastPack->mW = cLftIndent + tWidestLine + cRgtIndent;
    rLastPack->mH = pYTarget - rLastPack->mY;

    ///--[Timers]
    //--This also computes how long the text callout should be visible.
    rLastPack->mRemoveTicks = (tLettersTotal * ABYCOM_CALLOUT_TICKS_PER_LETTER) + (ABYCOM_CALLOUT_TICKS_TO_HOLD);
}
void AbyssCombat::SetCalloutTimer(int pTicks)
{
    ///--[Documentation]
    //--Allows a timer to be manually overridden. Set to a negative number to "hide" the callout
    //  until the number is positive.
    CalloutPack *rLastPack = (CalloutPack *)mCalloutList->GetTail();
    if(!rLastPack) return;

    //--Set.
    rLastPack->mTextTimer = pTicks;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void AbyssCombat::UpdateCallouts()
{
    ///--[Documentation]
    //--Updates the packages. Removes ones that have expired.
    CalloutPack *rCalloutPack = (CalloutPack *)mCalloutList->SetToHeadAndReturn();
    while(rCalloutPack)
    {
        //--Get the entity associated with the callout. If it doesn't exist, remove.
        AdvCombatEntity *rEntity = GetEntityByID(rCalloutPack->mUserID);
        if(!rEntity)
        {
            mCalloutList->RemoveRandomPointerEntry();
            rCalloutPack = (CalloutPack *)mCalloutList->IncrementAndGetRandomPointerEntry();
            continue;
        }

        //--Timer.
        rCalloutPack->mTextTimer ++;

        //--Removal case.
        if(rCalloutPack->mTextTimer >= rCalloutPack->mRemoveTicks) mCalloutList->RemoveRandomPointerEntry();

        //--Next.
        rCalloutPack = (CalloutPack *)mCalloutList->IncrementAndGetRandomPointerEntry();
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AbyssCombat::RenderCallouts()
{
    ///--[Documentation]
    //--Renders all callout packages.
    float cBubbleWid = AbyssImg.Data.rSpeechBubble->GetTrueWidthSafe();
    float cBubbleHei = AbyssImg.Data.rSpeechBubble->GetTrueHeightSafe();

    //--Constants
    float cLftIndent = 11.0f;
    float cTopIndent = 11.0f;
    float cRgtIndent = 11.0f;
    float cBotIndent = 11.0f;

    //--Textures.
    float cTxMLf = cLftIndent / cBubbleWid;
    float cTxMRt = (cBubbleWid - cRgtIndent) / cBubbleWid;
    float cTxMTp = cTopIndent / cBubbleHei;
    float cTxMBt = (cBubbleHei - cBotIndent) / cBubbleHei;

    //--Iterate.
    CalloutPack *rCalloutPack = (CalloutPack *)mCalloutList->PushIterator();
    while(rCalloutPack)
    {
        ///--[Skip Rendering]
        //--Callouts don't render if their timer is negative.
        if(rCalloutPack->mTextTimer < 0)
        {
            rCalloutPack = (CalloutPack *)mCalloutList->AutoIterate();
            continue;
        }

        //--Check the associated entity. If it doesn't exist, stop rendering.
        AdvCombatEntity *rEntity = GetEntityByID(rCalloutPack->mUserID);
        if(!rEntity)
        {
            rCalloutPack = (CalloutPack *)mCalloutList->AutoIterate();
            continue;
        }

        //--If the entity is dying, stop.
        if(rEntity->IsDefeated())
        {
            rCalloutPack = (CalloutPack *)mCalloutList->AutoIterate();
            continue;
        }

        //--If the entity is disabled, stop.
        if(IsPartyDisabledID(rCalloutPack->mUserID))
        {
            rCalloutPack = (CalloutPack *)mCalloutList->AutoIterate();
            continue;
        }

        //--Note: ACE_UI_INDEX_COMBAT_ALLY is re-used in Monoceros for callout positioning since
        //  ally bars don't render in this mode.
        //--If the values associated with it are ADVCE_NORENDER then don't render the callout. This
        //  happens when an entity is despawning.
        TwoDimensionRealPoint cEntityPoint = rEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_ALLY);
        if(cEntityPoint.mXCenter == ADVCE_NORENDER || cEntityPoint.mYCenter == ADVCE_NORENDER)
        {
            rCalloutPack = (CalloutPack *)mCalloutList->AutoIterate();
            continue;
        }

        ///--[Setup]
        float cLft = rEntity->mLastRenderX + cEntityPoint.mXCenter;
        float cRgt = cLft + rCalloutPack->mW;
        float cMLf = cLft + cLftIndent;
        float cMRt = cRgt - cRgtIndent;
        float cTop = rEntity->mLastRenderY + cEntityPoint.mYCenter;
        float cBot = cTop + rCalloutPack->mH;
        float cMTp = cTop + cTopIndent;
        float cMBt = cBot - cBotIndent;

        //--Clamp. Disallow the boundaries from exceeding the edges.
        if(cLft < 0.0f)
        {
            float tDif = cLft * -1.0f;
            cLft = cLft + tDif;
            cRgt = cRgt + tDif;
            cMLf = cMLf + tDif;
            cMRt = cMRt + tDif;
        }
        if(cRgt > VIRTUAL_CANVAS_X)
        {
            float tDif = (cRgt - VIRTUAL_CANVAS_X) * -1;
            cLft = cLft + tDif;
            cRgt = cRgt + tDif;
            cMLf = cMLf + tDif;
            cMRt = cMRt + tDif;
        }
        if(cTop < 0.0f)
        {
            float tDif = cTop * -1.0f;
            cTop = cTop + tDif;
            cBot = cBot + tDif;
            cMTp = cMTp + tDif;
            cMBt = cMBt + tDif;
        }
        if(cBot > VIRTUAL_CANVAS_Y)
        {
            float tDif = (cBot - VIRTUAL_CANVAS_X) * -1;
            cTop = cTop + tDif;
            cBot = cBot + tDif;
            cMTp = cMTp + tDif;
            cMBt = cMBt + tDif;
        }

        ///--[Fading]
        float tAlpha = 1.0f;
        int tFadeOutTicks = rCalloutPack->mRemoveTicks - ABYCOM_CALLOUT_TICKS_FADE_OUT;
        if(rCalloutPack->mTextTimer < ABYCOM_CALLOUT_TICKS_FADE_IN)
        {
            tAlpha = rCalloutPack->mTextTimer / (float)ABYCOM_CALLOUT_TICKS_FADE_IN;
        }
        else if(rCalloutPack->mTextTimer >= tFadeOutTicks)
        {
            tAlpha = 1.0f - ((rCalloutPack->mTextTimer - tFadeOutTicks) / (float)ABYCOM_CALLOUT_TICKS_FADE_OUT);
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);

        ///--[Speech Bubble]
        //--Box.
        AbyssImg.Data.rSpeechBubble->RenderNine(cLft, cMLf, cMRt, cRgt, cTop, cMTp, cMBt, cBot, cTxMLf, cTxMRt, cTxMTp, cTxMBt);

        //--Normal case:
        if(!rCalloutPack->mIsBackwards && !rCalloutPack->mIsUpDown)
        {
            AbyssImg.Data.rSpeechBubbleBot->Draw(cLft-10.0f, cBot-2.0f);
        }
        //--Horizontal flip:
        else if(rCalloutPack->mIsBackwards && !rCalloutPack->mIsUpDown)
        {
            AbyssImg.Data.rSpeechBubbleBot->Draw(cLft+9.0f, cBot-2.0f, FLIP_HORIZONTAL);
        }
        //--Vertical flip:
        else if(!rCalloutPack->mIsBackwards && rCalloutPack->mIsUpDown)
        {
            float cHei = cBot - cTop;
            AbyssImg.Data.rSpeechBubbleBot->Draw(cLft+9.0f, cBot - 33.0f - cHei, FLIP_VERTICAL);
        }
        //--Both flips:
        else
        {
            float cHei = cBot - cTop;
            AbyssImg.Data.rSpeechBubbleBot->Draw(cLft+9.0f, cBot - 33.0f - cHei, FLIP_BOTH);
        }

        ///--[Text]
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, tAlpha);
        float tCharTimer = 0;
        for(int i = 0; i < rCalloutPack->mLinesTotal; i ++)
        {
            //--Stop if the line is NULL.
            if(!rCalloutPack->mLines[i]) break;

            //--Y position.
            float cYPos = cTop + (i * AbyssImg.Data.rFontCallout->GetTextHeight());
            float tXPos = cLft + cLftIndent;

            //--Setup.
            int tIndex = 0;
            int tLen = utf8_strlen(rCalloutPack->mLines[i]);

            //--Iterate across the line.
            uint32_t tLetter = utf8_nextletter(rCalloutPack->mLines[i], tIndex);
            for(int p = 0; p < tLen; p ++)
            {
                //--Get the next letter for kerning purposes.
                uint32_t tNextLetter = utf8_nextletter(rCalloutPack->mLines[i], tIndex);

                //--Render.
                if(rCalloutPack->mTextTimer > tCharTimer)
                {
                    float tIncrement = AbyssImg.Data.rFontCallout->DrawLetter(tXPos, cYPos, 0, 1.0f, tLetter, tNextLetter);
                    tXPos = tXPos + tIncrement;
                    tCharTimer = tCharTimer + ABYCOM_CALLOUT_TICKS_PER_LETTER;
                }
                //--Timer has not reached this, stop rendering.
                else
                {
                    break;
                }

                //--Next letter.
                tLetter = tNextLetter;
            }
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);

        ///--[Next]
        rCalloutPack = (CalloutPack *)mCalloutList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
