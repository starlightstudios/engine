-- |[ ======================================= Boot Fonts ======================================= ]|
--Boots fonts used by Project Monoceros, replaced with translation-friendly fonts. Expects to receive
-- the base path to the Monoceros Fonts folder as its first argument.

-- |[Arguments]|
--Check.
if(fnArgCheck(1) == false) then
    io.write("Error: MonocerosFonts.lua did not receive a path as its first argument.\n")
    return
end

--Get.
local sMonocerosFontPath = LM_GetScriptArgument(0)

-- |[Setup]|
--Debug timer.
local fStartTime = 0.0
LM_StartTimer("Fonts")

--Paths.
local sFontPath = "Data/"
local sKerningPath = "Data/Scripts/Fonts/"
local sLocalPath = fnResolvePath()

--JP Paths.
local sPretendardPath = sLocalPath .. "PretendardJP-Regular.otf"
local sHasNoKerningFile = "Null"

--Special Flags
local ciFontNoFlags    = 0
local ciFontNearest    = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge       = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade   = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS   = Font_GetProperty("Constant Precache With Special S")
local ciFontSpecialExc = Font_GetProperty("Constant Precache With Special !")
local ciFontUnicode    = Font_GetProperty("Constant Precache Full Unicode")
local ciFontDummy      = Font_GetProperty("Constant Precache Dummy")
local ciFontSpecialDbg = Font_GetProperty("Constant Precache Debug")

-- |[Precompilation Handling]|
local iCompileFlag = 2
if(iCompileFlag == 1) then
    StarFont_BeginCompilation("Data/FontPrebuild/MonocerosFontPrecompileJP.slf")
elseif(iCompileFlag == 2) then
    StarFont_OpenCompiledFile("Data/FontPrebuild/MonocerosFontPrecompileJP.slf")
end

-- |[ ==================================== Font Registration =================================== ]|
-- |[Immortal]|
local sImmortalPath = sMonocerosFontPath .. "IMMORTAL.ttf"
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Immortal 120", sImmortalPath, sHasNoKerningFile,  30, ciFontNoFlags)

-- |[Garamond]|
local sGaramondPath = sMonocerosFontPath .. "EBGaramond-Regular.ttf"
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Garamond 120 DFO", sGaramondPath, sHasNoKerningFile, 30, ciFontEdge + ciFontDownfade + ciFontNearest)

-- |[Medieval]|
local sMedievalPath = sMonocerosFontPath .. "MedievalSharp-Book.ttf"
Font_Register("Medieval 15", sMedievalPath, sHasNoKerningFile, 15, ciFontNoFlags)
if(iCompileFlag == 1) then
    StarFont_FinishCompilation()
elseif(iCompileFlag == 2) then
    StarFont_OpenCompiledFile("Null")
end

-- |[Pretendard JP 60]|
local zPretendardJP60 = FontProto:fnLocatePrototype("PretendardJP 60")
zPretendardJP60:fnLoad()
    
-- |[Pretendard JP 42]|
local zPretendardJP42 = FontProto:fnLocatePrototype("PretendardJP 42")
zPretendardJP42:fnLoad()

-- |[Pretendard JP 36]|
local zPretendardJP36 = FontProto:fnLocatePrototype("PretendardJP 36")
zPretendardJP36:fnLoad()

-- |[Pretendard JP 30]|
local zPretendardJP30 = FontProto:fnLocatePrototype("PretendardJP 30")
zPretendardJP30:fnLoad()

-- |[Pretendard JP 20]|
local zPretendardJP20 = FontProto:fnLocatePrototype("PretendardJP 20")
zPretendardJP20:fnLoad()
    
-- |[ ========================================= Aliases ======================================== ]|
-- |[English Aliases]|
--Immortal 120
Font_SetProperty("Add Alias", "Immortal 120", "Abyss Combat Victory") --Only uses the ASCII characters for VICTORY, not translated

--Garamond 120 DFO
Font_SetProperty("Add Alias", "Garamond 120 DFO", "Abyss Combat Text") --Only uses numbers and a few simple characters, not translated

--Medieval 15
Font_SetProperty("Add Alias", "Medieval 15", "Abyss Combat Enemy UI") --Only numbers, not translated

-- |[Immortal]|
--Immortal 60 -Replaced
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Base Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Status Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Equipment Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu File Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Inventory Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Journal Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Option Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Skills Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Menu Vendor Double Header")
Font_SetProperty("Add Alias", "PretendardJP 60", "Monoceros Dialogue Topic Header")

-- |[Medieval Sharp]|
--Medieval 42 -Replaced
Font_SetProperty("Add Alias", "PretendardJP 42", "Monoceros Dialogue Nameplate")
Font_SetProperty("Add Alias", "PretendardJP 42", "Abyss Combat Nameplate")
Font_SetProperty("Add Alias", "PretendardJP 42", "Abyss Combat Confirm Big")

--Medieval 36 -Replaced
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Dialogue Topics")
Font_SetProperty("Add Alias", "PretendardJP 36", "Abyss Combat Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Base Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Status Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Equipment Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu File Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Inventory Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Journal Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Journal Tabs")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Journal Achieve Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Option Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Skills Header")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Vendor Header")

--Medieval 20 -Replaced
Font_SetProperty("Add Alias", "PretendardJP 20", "Abyss Combat Player UI")
Font_SetProperty("Add Alias", "PretendardJP 20", "Abyss Combat Confirm Small")

-- |[Garamond]|
--Garamond 36 -Replaced
Font_SetProperty("Add Alias", "PretendardJP 36", "Abyss Combat Ability Title")
Font_SetProperty("Add Alias", "PretendardJP 36", "Abyss Combat Command")
Font_SetProperty("Add Alias", "PretendardJP 36", "Abyss Combat Dialogue")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Base Mainline")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Menu Status Mainlg")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Dialogue Major Mainline")
Font_SetProperty("Add Alias", "PretendardJP 36", "Monoceros Dialogue Info Mainline")

--Garamond 30 -Replaced
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Base Button")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Base Health")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Base Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Equipment Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Equipment Statistic")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Inventory Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Journal Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Option Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Skills Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Status Health")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Status Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Vendor Help")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Status Mainline")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Equipment Mainline")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Journal Mainline")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Option Mainline")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Skills Mainline")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Vendor Mainline")
Font_SetProperty("Add Alias", "PretendardJP 30", "Monoceros Menu Inventory Entry")
Font_SetProperty("Add Alias", "PretendardJP 30", "Abyss Combat Subcommand")
Font_SetProperty("Add Alias", "PretendardJP 30", "Abyss Combat Loot")

--Garamond 20 -Replaced
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Standard Control")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Equipment Description")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu File Mainline")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Inventory Description")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Inventory Statistic")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Journal Description")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Journal Entries")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Journal Statistic")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Journal Achieve Main")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Skills Description")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Vendor Description")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Vendor Statistic")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Status Control")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Status Equipment")
Font_SetProperty("Add Alias", "PretendardJP 20", "Abyss Combat Callout")
Font_SetProperty("Add Alias", "PretendardJP 20", "Abyss Combat Description")
Font_SetProperty("Add Alias", "PretendardJP 20", "Abyss Combat Prediction")
Font_SetProperty("Add Alias", "PretendardJP 20", "Monoceros Menu Very Small")

-- |[ ==================================== End Compilation ===================================== ]|

local fEndTime = LM_CheckTimer("Fonts")
io.write(string.format("%6.4f: Monoceros Font Loading\n", fEndTime - fStartTime))
LM_FinishTimer("Fonts")

--Clear flags.
Font_SetProperty("Monospacing", -1.0)
