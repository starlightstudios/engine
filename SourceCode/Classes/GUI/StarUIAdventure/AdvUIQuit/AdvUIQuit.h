///========================================= AdvUIQuit ============================================
//--Asks the player if they want to quit. Simple!

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///--[Local Definitions]
//--Timers
#define ADVMENU_QUIT_VIS_TICKS 15
#define ADVMENU_QUIT_CURSOR_MOVE_TICKS 7
#define ADVMENU_QUIT_FINAL_TICKS 30

//--Cursor
#define ADVMENU_QUIT_CURSOR_TOTITLE 0
#define ADVMENU_QUIT_CURSOR_EXITPROGRAM 1

///========================================== Classes =============================================
class AdvUIQuit : virtual public AdvUICommon
{
    protected:
    ///--[Variables]
    //--System
    int mCursor;

    //--Quit Handling
    bool mIsQuitting;
    int mQuitFadeTimer;

    //--Cursor
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rHeadingFont;

        //--Images
        StarBitmap *rBacking;
        StarBitmap *rBackToTitleButton;
        StarBitmap *rExitProgramButton;
        StarBitmap *rOverlay_Highlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIQuit();
    virtual ~AdvUIQuit();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


