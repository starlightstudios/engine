///======================================== MonoUIWarp ============================================
//--Warp handler for Monoceros. The game does not do regions, instead having the whole thing take
//  place in a sinlge area. Otherwise the implementation is the same with new graphics.

#pragma once

///========================================= Includes =============================================
#include "AdvUIWarp.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///======================================== Translation ===========================================
///--[MonoUIWarp_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIWARP_STRINGS_TOTAL 3
class MonoUIWarp_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIWARP_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextHelp[3];
        }str;
    };

    //--Functions
    MonoUIWarp_Strings();
    virtual ~MonoUIWarp_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIWarp : public MonoUICommon, virtual public AdvUIWarp
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxMonoHelpStrings = 3;

    //--Sizes
    static constexpr float cxMonoMaxLocationNameWid = 250.0f;

    ///--[System]
    ///--[Images]
    struct
    {
        StarBitmap *rOverlay_WarpSelect;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIWarp();
    virtual ~MonoUIWarp();
    virtual void Construct();

    //--Public Variables
    static MonoUIWarp_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLocationMode();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


