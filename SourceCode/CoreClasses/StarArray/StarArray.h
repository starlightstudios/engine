///========================================= StarArray =============================================
//--A memory object designed to deliberately abstract access to an array, mostly to allow sizing of
//  the memory to be optimized like matching page sizes and optionally being segmented.
//--Segmenting can prevent certain memory behaviors like fragmentation when exceptionally large blocks
//  are allocated and then partially filled, making that memory become a dead zone. Using this object
//  with segmentation enabled bypasses this problem and may allow systems with smaller memory allocations
//  to still function though obviously slightly slower.

#pragma once

///========================================= Includes =============================================
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class StarArray
{
    private:
    ///--[Memory Blocks]
    bool mIsMemoryAllocated;            //True when memory is allocated. Certain properties cannot be changed if this is true.
    int mMemoryBlocksTotal;             //Total number of memory blocks, can be 0. Will be 1 if segmentation is off.
    uint8_t **mMemoryBlocks;            //Pointer to array of memory blocks.

    ///--[Segmentation]
    bool mAllowSegmentation;            //If true, allocated memory is split into blocks of mSegmentSize bytes.
    uint32_t mSegmentSize;              //Size, in bytes, of each memory segmnet. Defaults to the system's page size.

    ///--[Statistics]
    int mNominalMemorySize;             //Memory, in bytes, that the program requested be allocated.
    int mActualMemorySize;              //Memory, in bytes, that were actually allocated. Often goes up to the next nearest page.

    protected:

    public:
    //--System
    StarArray();
    ~StarArray();

    //--Public Variables
    //--Property Queries
    bool IsReady();
    bool IsSegmented();
    int GetTotalBlocks();
    uint32_t GetSegmentSize();
    int GetNominalMemorySize();
    int GetActualMemorySize();
    uint8_t GetByte(uint32_t pPosition);

    //--Manipulators
    void SetSegmentationFlag(bool pFlag);
    void OverrideSegmentSize(uint32_t pSizePerSegment);

    //--Core Methods
    void Allocate(uint32_t pBytes);
    void Reallocate(uint32_t pBytes);
    void Deallocate();
    void WriteToArray(const void *pSource, int pStartLocation, size_t pBytes);
    void ReadFromArray(void *pDestination, int pStartLocation, size_t pBytes);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void ReadFileIntoMemory(int pStartPosition, size_t pBytes, FILE *pInfile);
    void WriteFileFromMemory(int pStartPosition, size_t pBytes, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    uint8_t **GetMemoryAddress();
    uint8_t *LiberateData();

    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


