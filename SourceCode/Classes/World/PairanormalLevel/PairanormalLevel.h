///--[PairanormalLevel]
//--Level which handles the Pairanormal version of the visual novel gamestyle. Has the background, characters, text box, and UI.
//  Unlike the MOTF variant, the PairanormalLevel is designed for use with investigations, and supports scrolling.

#pragma once

///--[Includes]
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"
#include "Classes/World/RootLevel.h"

///--[Local Structures]
///--[Local Definitions]
#define PLBTN_QUIT 0
#define PLBTN_SETTINGS 1
#define PLBTN_TOTAL 2

#define FADE_TICKS_MAX 45

#define BG_SCALE 0.500000f
#define BG_OFFSET_Y 0.0f
#define BG_SCROLL_MAX 0.0f

//--Chapter End Special Phases
#define PL_CES_FADEIN 0
#define PL_CES_HOLD 1
#define PL_CES_FADEOUT 2

#define PL_CES_FADE_TICKS 120
#define PL_CES_HOLD_TICKS 240

//--Image Scroll Phases
#define PL_IS_FADEIN 0
#define PL_IS_SCROLL 1
#define PL_IS_FADEOUT 2

#define PL_IS_FADE_TICKS 120
#define PL_IS_SCROLL_TICKS 600

///--[Classes]
class PairanormalLevel : public RootLevel
{
    private:
    //--System
    int mInitialFadeTimer;
    bool mBootToMainMenu;
    float mMouseX;
    float mMouseY;

    //--Checkpoint
    char *mCurrentCheckpointName;
    char *mCurrentCheckpointScript;
    char *mCurrentCheckpointArg;

    //--Background
    int mFadeTimer;
    float mBackgroundXOffset;
    char *mLastBackgroundPath;
    StarBitmap *rPreviousBackground;
    StarBitmap *rCurrentBackground;

    //--Screen Shake
    int mScreenShakeTimer;
    float mScreenShakeCurrent;

    //--Chapter End Special
    bool mIsChapterEndSpecial;
    int mChapterEndSpecialPhase;
    int mChapterEndSpecialTimer;
    StarBitmap *rChapterEndSpecialImg;

    //--Image Scroll Special
    bool mIsImageScrollSpecial;
    int mImageScrollPhase;
    int mImageScrollTimer;
    StarBitmap *rImageSrollImg;

    //--Buttons
    TwoDimensionReal mBtnDim[PLBTN_TOTAL];

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Icons
            StarBitmap *rIconClose;
            StarBitmap *rIconSettings;
        }Data;
    }Images;

    protected:

    public:
    //--System
    PairanormalLevel();
    virtual ~PairanormalLevel();

    //--Public Variables
    static int xPlayTime;
    static char *xActiveGamePath;
    static bool xIsLowResMode;

    //--Property Queries
    const char *GetCheckpointName();
    const char *GetCheckpointScript();
    const char *GetCheckpointArg();
    float GetBackgroundOffset();
    float GetScreenShake();
    const char *GetLastBackgroundPath();

    //--Manipulators
    void FlagToMainMenu();
    void SetBackground(const char *pPath);
    void SetCheckpoint(const char *pName, const char *pScriptPath, const char *pArgument);
    void SetBackgroundOffset(float pAmount);
    void ShakeScreen();
    void ActivateChapterEnd(const char *pDLPath);
    void ActivateImageScroll(const char *pDLPath);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    void RunToCheckpointAfterLoading();
    void HandleCheckpoint(const char *pName);

    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    static PairanormalLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PairanormalLevel_IsLoading(lua_State *L);
int Hook_PairanormalLevel_SetBackground(lua_State *L);
int Hook_PairanormalLevel_SaveCheckpoint(lua_State *L);
int Hook_PairanormalLevel_FlagCheckpoint(lua_State *L);

