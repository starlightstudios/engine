--To-do
--Replace Autoload with the name of the class
--Balance the headers
--Write a description
--Delete these instructions

-- |[ ================================== Autoload Class File =================================== ]|
--Class file for Autoload. Call this once at initialization.

--Autoloader, static class that holds functions related to using the autoloader. Autoloading allows
-- the compressor program StarLumps to store load instructions inside the datafile instead of requiring
-- lua calls to load data. This allows automation of putting assets into games, and is faster than
-- running scripts to boot.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
Autoload = {}
Autoload.__index = Autoload 

-- |[ ============ Class Methods ============= ]|
--System
--Functions
function Autoload:fnExecute(psAutoloadName) end

-- |[ ========== Class Constructor =========== ]|
--Static class. Does not instantiate.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "AutoloadExecution.lua")
