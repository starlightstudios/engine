//--Base
#include "StarLumpManager.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "VirtualFile.h"
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define SLF_IMAGE_DEBUG
#ifdef SLF_IMAGE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

StarBitmapPrecache *StarLumpManager::GetImageData(const char *pLumpName)
{
    ///--[Documentation]
    //--Extracts the Image data as provided by the pLumpName. Returns NULL on failure.
    if(!mIsOpen || !pLumpName) return NULL;
    DebugPush(true, "Loading Image Data %s\n", pLumpName);

    //--Structure to hold the data.
    SetMemoryData(__FILE__, __LINE__);
    StarBitmapPrecache *nCache = (StarBitmapPrecache *)starmemoryalloc(sizeof(StarBitmapPrecache));
    memset(nCache, 0, sizeof(StarBitmapPrecache));
    StarBitmap::SetPrecacheFlags(*nCache);

    //--Locate it and seek to it.
    if(!StandardSeek(pLumpName, "IMAGE"))
    {
        //--Block errors on this call if this flag is true. Sometimes, an image not existing is
        //  itself valid, so no need to bark.
        if(!mSuppressErrorOnce) DebugManager::ForcePrint("Error in SLF file\n");
        mSuppressErrorOnce = false;
        DebugPop("");
        free(nCache);
        return NULL;
    }

    //--Get the compression type.
    mVFile->Read(&nCache->mCompressionType, sizeof(uint8_t), 1);
    DebugPrint("Compress type %i\n", nCache->mCompressionType);
    if(nCache->mCompressionType == 0)
    {
        DebugManager::ForcePrint("Compression error in %s\n", pLumpName);
        DebugPop("");
        return nCache;
    }

    //--If the compression type is cDummyCompression, return a blank precache. The caller should handle
    //  this being a blank data set. This is not an error.
    if(nCache->mCompressionType == StarBitmap::cDummyCompression)
    {
        return nCache;
    }

    //--Read the offset data.
    mVFile->Read(&nCache->mTrueWidth, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mTrueHeight, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mXOffset, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mYOffset, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mWidth, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mHeight, sizeof(int32_t), 1);
    DebugPrint("Statistics read %i %i\n", nCache->mTrueWidth, nCache->mTrueHeight);

    //--Length of the bitmap's data for upload.
    mVFile->Read(&nCache->mDataSize, sizeof(uint32_t), 1);
    DebugPrint("Raw data to read: %s %i bytes\n", pLumpName, nCache->mDataSize);

    //--Allocate an array for this job. The StarBitmap will deallocate it for us.
    SetMemoryData(__FILE__, __LINE__);
    nCache->mArray = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * nCache->mDataSize);
    if(!nCache->mArray)
    {
        fprintf(stderr, "StarLumpManager:GetImageData() - Warning. Unable to locate memory %i in %s.\n", nCache->mDataSize, pLumpName);
    }

    mVFile->Read(nCache->mArray, sizeof(uint8_t), nCache->mDataSize);
    DebugPrint("Data read successfully\n");

    //--Hitboxes
    mVFile->Read(&nCache->mTotalHitboxes, sizeof(uint8_t), 1);
    SetMemoryData(__FILE__, __LINE__);
    nCache->mHitboxes = (SgHitbox *)starmemoryalloc(sizeof(SgHitbox) * nCache->mTotalHitboxes);
    for(int i = 0; i < nCache->mTotalHitboxes; i ++)
    {
        mVFile->Read(&nCache->mHitboxes[i], sizeof(SgHitbox), 1);
        //fprintf(stderr, "Hitbox of %i %i - %i %i\n", nCache->mHitboxes[i].mLft, nCache->mHitboxes[i].mTop, nCache->mHitboxes[i].mRgt, nCache->mHitboxes[i].mBot);
    }

    //--Pass it back.
    DebugPop("Image data loaded\n");
    return nCache;
}
StarBitmap *StarLumpManager::GetImage(const char *pLumpName)
{
    ///--[Documentation and Setup]
    //--Overload, gets the ImageData and immediately uploads it to a StarBitmap.
    if(!mIsOpen || !pLumpName) return NULL;
    DebugPush(true, "Loading Image %s\n", pLumpName);

    ///--[Raw Data]
    //--Get the raw data
    StarBitmapPrecache *tDataCache = GetImageData(pLumpName);
    if(!tDataCache)
    {
        DebugManager::ForcePrint("Failed, can't find data %s\n", pLumpName);
        DebugPop("");
        return NULL;
    }

    //--If the compression type is cDummyCompression, then this is a dummy bitmap. Create it and return it
    //  but don't bother with any processing.
    if(tDataCache->mCompressionType == StarBitmap::cDummyCompression)
    {
        StarBitmap *nBitmap = new StarBitmap();
        nBitmap->SetDummyMode(pLumpName);
        return nBitmap;
    }

    ///--[Common]
    //--Create a bitmap for return.
    StarBitmap *nBitmap = new StarBitmap();

    ///--[Standard, No Atlas]
    //--Just upload the bitmap.
    bool tWasAtlas = false;
    if(StarBitmap::xDisallowAtlasing || !StarBitmap::xIsAtlasModeActive || tDataCache->mTrueWidth >= StarBitmap::xMaxTextureSize * 0.50f || tDataCache->mTrueHeight >= StarBitmap::xMaxTextureSize * 0.50f)
    {
        ///--[Upload]
        uint32_t tHandle = StarBitmap::UploadCompressed(tDataCache);
        if(!tHandle)
        {
            DebugManager::ForcePrint("Failed, error uploading the data for %s\n", pLumpName);
            DebugPop("");
            return NULL;
        }

        //--Assign the handle.
        nBitmap->AssignHandle(tHandle, true, false);
    }
    ///--[Atlas Mode]
    //--When atlasing is enabled, we try to upload this image to an atlas or create one if it doesn't
    //  already exist.
    else
    {
        //--Debug.
        tWasAtlas = true;
        //fprintf(stderr, "Beginning atlas work.\n");

        //--Check if there's no atlas currently active. If not, set this as the active atlas.
        if(!StarBitmap::xrActiveAtlas)
        {
            //--Debug.
            //fprintf(stderr, " Initializing new atlas.\n");

            //--Set.
            StarBitmap::xrActiveAtlas = nBitmap;
            nBitmap->mDependencyList = new StarLinkedList(false);

            //--Setup the length. For now we use fixed values.
            StarBitmap::xAtlasDataLen = (sizeof(uint32_t) * (int)StarBitmap::xAtlasMaxSize * (int)StarBitmap::xAtlasMaxSize);
            StarBitmap::xAtlasStride = sizeof(uint32_t) * (int)StarBitmap::xAtlasMaxSize;
            SetMemoryData(__FILE__, __LINE__);
            StarBitmap::xAtlasData = (uint8_t *)starmemoryalloc(StarBitmap::xAtlasDataLen);

            //--Reset the counting variables.
            StarBitmap::xAtlasCurrentX = 0;
            StarBitmap::xAtlasCurrentY = 0;
            StarBitmap::xAtlasCurrentTallest = 0;

            //--Debug.
            //fprintf(stderr, " Done initializing new atlas.\n");
        }

        //--Get the raw bitmap data. This will uncompress it if it was compressed.
        int32_t tWidth = tDataCache->mWidth;
        int32_t tHeight = tDataCache->mHeight;
        int32_t tDataSize = tDataCache->mDataSize;

        //--Range check. See if there's enough space left for this bitmap.
        if(StarBitmap::xAtlasCurrentX + tWidth >= (int)StarBitmap::xAtlasMaxSize)
        {
            //--Next line set.
            //fprintf(stderr, " Handling Atlas new line case.\n");
            StarBitmap::xAtlasCurrentX = 0;
            StarBitmap::xAtlasCurrentY += StarBitmap::xAtlasCurrentTallest;

            //--Check if there's enough vertical space left. If not, finish the atlas, and start
            //  a new one with this object as the atlas object.
            if(StarBitmap::xAtlasCurrentY + tHeight >= (int)StarBitmap::xAtlasMaxSize)
            {
                //--Finish the old atlas.
                //fprintf(stderr, " Handling Atlas-end case.\n");
                StarBitmap::xrActiveAtlas->FinishAtlas();

                //--Boot a new atlas.
                StarBitmap::xrActiveAtlas = nBitmap;
                nBitmap->mDependencyList = new StarLinkedList(false);

                //--Setup the length. For now we use fixed values.
                StarBitmap::xAtlasDataLen = (sizeof(uint32_t) * (int)StarBitmap::xAtlasMaxSize * (int)StarBitmap::xAtlasMaxSize);
                StarBitmap::xAtlasStride = sizeof(uint32_t) * (int)StarBitmap::xAtlasMaxSize;
                SetMemoryData(__FILE__, __LINE__);
                StarBitmap::xAtlasData = (uint8_t *)starmemoryalloc(StarBitmap::xAtlasDataLen);

                //--Reset the counting variables.
                StarBitmap::xAtlasCurrentX = 0;
                StarBitmap::xAtlasCurrentY = 0;
                StarBitmap::xAtlasCurrentTallest = 0;
                //fprintf(stderr, " Handled Atlas-end case.\n");
            }

            //--Otherwise, set the values as necessary.
            StarBitmap::xAtlasCurrentTallest = 0;
            //fprintf(stderr, " Handled new line case.\n");
        }

        //fprintf(stderr, " Getting raw data.\n");
        StarBitmap::PutDataIntoAtlas(tDataCache->mArray, tWidth, tHeight, tDataCache->mCompressionType, tDataSize);
        if(StarBitmap::xInterruptCall && !StarBitmap::xSuppressInterrupt) StarBitmap::xInterruptCall();

        //--Store our upload positions in the uv coordinates.
        StarBitmap::xLastAtlasLft = (StarBitmap::xAtlasCurrentX)           / StarBitmap::xAtlasMaxSize;
        StarBitmap::xLastAtlasTop = (StarBitmap::xAtlasCurrentY)           / StarBitmap::xAtlasMaxSize;
        StarBitmap::xLastAtlasRgt = (StarBitmap::xAtlasCurrentX + tWidth)  / StarBitmap::xAtlasMaxSize;
        StarBitmap::xLastAtlasBot = (StarBitmap::xAtlasCurrentY + tHeight) / StarBitmap::xAtlasMaxSize;
        nBitmap->SetAtlasCoordinates(StarBitmap::xLastAtlasLft, StarBitmap::xLastAtlasTop, StarBitmap::xLastAtlasRgt, StarBitmap::xLastAtlasBot);
        //fprintf(stderr, "Coords %f %f\n", StarBitmap::xLastAtlasLft, StarBitmap::xLastAtlasTop);

        //--Move the counting variables.
        StarBitmap::xAtlasCurrentX += tWidth;
        if(StarBitmap::xAtlasCurrentTallest < tHeight) StarBitmap::xAtlasCurrentTallest = tHeight;

        //--If this bitmap is not the atlas bitmap, it's a dependent bitmap.
        if(nBitmap != StarBitmap::xrActiveAtlas)
        {
            StarBitmap::xrActiveAtlas->AddToAtlasList(nBitmap);
        }
        //fprintf(stderr, "Completed.\n");
    }

    //--Common properties.
    nBitmap->SetOffset(tDataCache->mXOffset, tDataCache->mYOffset);
    nBitmap->SetTrueSizes(tDataCache->mTrueWidth, tDataCache->mTrueHeight);
    nBitmap->SetSizes(tDataCache->mWidth, tDataCache->mHeight);
    if(StarBitmap::xPadSpriteForEdging && !tWasAtlas)
    {
        nBitmap->SetOffset(tDataCache->mXOffset - 2, tDataCache->mYOffset - 2);
        nBitmap->SetSizes(tDataCache->mWidth + 4, tDataCache->mHeight + 4);
    }

    //--Hitboxes, if they exist.
    nBitmap->AllocateHitboxes(tDataCache->mTotalHitboxes);
    for(int i = 0; i < tDataCache->mTotalHitboxes; i ++)
    {
        nBitmap->SetHitbox(i, tDataCache->mHitboxes[i]);
    }

    //--Any image that was loaded out of the SLM is considered to be flipped. Note that this is not
    //  the case for Allegro-stripped bitmaps.
    nBitmap->SetInternalFlip(true);

    //--[Clean]
    DebugPop("Lump uploaded successfully\n");
    free(tDataCache->mArray);
    free(tDataCache->mHitboxes);
    free(tDataCache);
    return nBitmap;
}
StarBitmap *StarLumpManager::GetPaddedTileImage(const char *pLumpName)
{
    ///--[Documentation]
    //--Special instance of GetImage(), this version is used for tile mappings. When tiles are upscaled or downscaled,
    //  they will often bleed into one another (or transparent space) because they are in atlas format. To prevent this,
    //  this algorithm will place buffers around the edges of all tiles. The buffers are the same color as the adjacent pixel.
    //--When handling this case, a 16x16 tile becomes 18x18 (pad on all four edges). Adjust your texel coordinates accordingly.
    //--Overload, gets the ImageData and immediately uploads it to an StarBitmap.
    if(!mIsOpen || !pLumpName) return NULL;
    DebugPush(true, "Loading Image %s\n", pLumpName);

    //--Get the raw data
    StarBitmapPrecache *tDataCache = GetImageData(pLumpName);
    if(!tDataCache)
    {
        DebugManager::ForcePrint("Failed, can't find data\n");
        DebugPop("");
        return NULL;
    }

    //--If the compression type is cDummyCompression, then this is a dummy bitmap. Create it and return it
    //  but don't bother with any processing.
    if(tDataCache->mCompressionType == StarBitmap::cDummyCompression)
    {
        StarBitmap *nBitmap = new StarBitmap();
        nBitmap->SetDummyMode(pLumpName);
        return nBitmap;
    }

    ///--[Execution]
    //--Constants
    int cBytesPerPixel = sizeof(uint8_t) * 4;
    int cSizeX = xTileSizeX;
    int cSizeY = xTileSizeY;

    //--Expect number of discrete tiles.
    int tExpectedX = tDataCache->mTrueWidth / cSizeX;
    int tExpectedY = tDataCache->mTrueHeight / cSizeY;

    //--Stride values.
    int cOldStride = tDataCache->mTrueWidth * cBytesPerPixel;
    int cNewStride = (tExpectedX * (cSizeX+2)) * cBytesPerPixel;
    DebugPrint("A.\n");

    //--Figure out how many tiles we expect. Error check if there are somehow zero tiles.
    if(tExpectedX > 0 && tExpectedY > 0)
    {
        //--Allocate space for the padded array.
        SetMemoryData(__FILE__, __LINE__);
        uint8_t *nNewArray = (uint8_t *)starmemoryalloc((tExpectedX * (cSizeX+2)) * (tExpectedY * (cSizeY+2)) * cBytesPerPixel);
        if(!nNewArray)
        {
            DebugManager::ForcePrint("Failed, unable to allocate space. Lump name: %s\n", pLumpName);
            DebugPop("");
            return NULL;
        }

        //--Begin crossloading. Pad as we go.
        for(int x = 0; x < tExpectedX; x ++)
        {
            for(int y = 0; y < tExpectedY; y ++)
            {
                //--Iterate across the tile itself.
                for(int i = 0; i < cSizeX; i ++)
                {
                    for(int p = 0; p < cSizeY; p ++)
                    {
                        //--Compute the position of this pixel. Remember that the stride already includes the bytes-per-pixel value.
                        int tOrigPosition = (((y *  cSizeY)    + p + 0) * cOldStride) + (((x *  cSizeX)    + i + 0) * cBytesPerPixel);
                        int tNewPosition  = (((y * (cSizeY+2)) + p + 1) * cNewStride) + (((x * (cSizeX+2)) + i + 1) * cBytesPerPixel);

                        //--Crossload.
                        nNewArray[tNewPosition+0] = tDataCache->mArray[tOrigPosition+0];
                        nNewArray[tNewPosition+1] = tDataCache->mArray[tOrigPosition+1];
                        nNewArray[tNewPosition+2] = tDataCache->mArray[tOrigPosition+2];
                        nNewArray[tNewPosition+3] = tDataCache->mArray[tOrigPosition+3];

                        //--Special case: Left edge.
                        if(i == 0)
                        {
                            nNewArray[tNewPosition+0-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Special case: Right edge.
                        else if(i == cSizeX-1)
                        {
                            nNewArray[tNewPosition+0+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+3];
                        }

                        //--Special case: Top edge.
                        if(p == 0)
                        {
                            nNewArray[tNewPosition+0-cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        else if(p == cSizeY-1)
                        {
                            nNewArray[tNewPosition+0+cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }

                        //--Top-left edge.
                        if(i == 0 && p == 0)
                        {
                            nNewArray[tNewPosition+0-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Top-right edge.
                        else if(i == cSizeX-1 && p == 0)
                        {
                            nNewArray[tNewPosition+0+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Bottom-left edge.
                        else if(i == 0 && p == cSizeY-1)
                        {
                            nNewArray[tNewPosition+0-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Bottom-right edge.
                        else if(i == cSizeX-1 && p == cSizeY-1)
                        {
                            nNewArray[tNewPosition+0+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                    }
                }
            }
        }

        //--Replace the data in the original cache.
        free(tDataCache->mArray);
        tDataCache->mArray = nNewArray;
        tDataCache->mWidth = (tExpectedX * (cSizeX+2));
        tDataCache->mHeight = (tExpectedY * (cSizeY+2));
        tDataCache->mTrueWidth = (tExpectedX * (cSizeX+2));
        tDataCache->mTrueHeight = (tExpectedY * (cSizeY+2));
        tDataCache->mDataSize = (tExpectedX * (cSizeX+2)) * (tExpectedY * (cSizeY+2)) * cBytesPerPixel;
    }
    DebugPrint("B.\n");

    //--Upload it
    StarBitmap *nBitmap = new StarBitmap();
    uint32_t tHandle = StarBitmap::UploadCompressed(tDataCache);
    if(!tHandle)
    {
        DebugManager::ForcePrint("Failed, error uploading the data for %s\n", pLumpName);
        DebugPop("");
        return NULL;
    }
    DebugPrint("C.\n");
    nBitmap->AssignHandle(tHandle, true, false);
    nBitmap->SetOffset(tDataCache->mXOffset, tDataCache->mYOffset);
    nBitmap->SetTrueSizes(tDataCache->mTrueWidth, tDataCache->mTrueHeight);
    nBitmap->SetSizes(tDataCache->mWidth, tDataCache->mHeight);
    if(StarBitmap::xPadSpriteForEdging)
    {
        nBitmap->SetOffset(tDataCache->mXOffset - 2, tDataCache->mYOffset - 2);
        nBitmap->SetSizes(tDataCache->mWidth + 4, tDataCache->mHeight + 4);
    }

    //--Hitboxes, if they exist.
    nBitmap->AllocateHitboxes(tDataCache->mTotalHitboxes);
    for(int i = 0; i < tDataCache->mTotalHitboxes; i ++)
    {
        nBitmap->SetHitbox(i, tDataCache->mHitboxes[i]);
    }
    DebugPrint("D.\n");

    //--Any image that was loaded out of the SLM is considered to be flipped. Note that this is not
    //  the case for Allegro-stripped bitmaps.
    nBitmap->SetInternalFlip(true);
    DebugPrint("E.\n");

    //--Clean
    DebugPop("Lump uploaded successfully\n");
    free(tDataCache->mArray);
    free(tDataCache->mHitboxes);
    free(tDataCache);
    return nBitmap;
}
