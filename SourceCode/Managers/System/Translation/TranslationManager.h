///==================================== TranslationManager ========================================
//--Handles functions related to translations. This is for code that doesn't fit anywhere else. A lot
//  of GUI code is able to handle its own translation, but that should eventually migrate into this manager.

#pragma once

///========================================= Includes =============================================
class StarLinkedList;
#include "LuaManager.h"

///===================================== Local Structures =========================================
///--[TranslationModule]
//--Base class for all translation objects. Contains no data structures, just serves as an interface
//  to allocate, deallocate, and query.
class TranslationModule
{
    //--Methods
    public:
    TranslationModule()
    {
    }
    virtual ~TranslationModule()
    {
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        TranslationModule *rPtr = (TranslationModule *)pPtr;
        delete rPtr;
    }
    virtual int GetSize() = 0;
    virtual void SetString(int pSlot, const char *pString) = 0;
};

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class TranslationManager
{
    private:
    ///--[System]
    StarLinkedList *mTranslationMasterList; //TranslationModule *, master

    protected:

    public:
    //--System
    TranslationManager();
    ~TranslationManager();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void RegisterEntry(const char *pName, TranslationModule *pModule);
    void SetStringIn(const char *pEntry, int pSlot, const char *pString);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static TranslationManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_TM_SetStringIn(lua_State *L);


