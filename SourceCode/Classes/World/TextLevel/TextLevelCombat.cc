//--Base
#include "TextLevel.h"

//--Classes
#include "TypingCombat.h"
#include "WordCombat.h"

//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioPackage.h"
#include "LuaManager.h"

///======================================= Documentation ==========================================
//--The generic handler of combat types. TextLevels can use either Typing-based or Card-based. Typing
//  is used for Electrosprite Text Adventure, while Card is used for String Tyrant. This file routes
//  updates calls between the two.

///========================================== System ==============================================
void TextLevel::SetCombatType(int pType)
{
    //--Sets which type of combat will be taking place. Only call this once at game boot.
    //--If called while combat is running, nothing will happen.
    mUseCombatType = pType;
}
void TextLevel::SetCombatActiveMode(bool pIsActiveMode)
{
    //--Sets if word-combat is active or turn-based.
    mUseCombatActiveMode = pIsActiveMode;
}

///======================================= Manipulators ===========================================
void TextLevel::SetHideStatsFlag(bool pFlag)
{
    mHidePlayerStats = pFlag;
}
void TextLevel::SetPlayerStats(int pHP, int pHPMax, int pAtk, int pDef, const char *pImgPath)
{
    ///--[Documentation]
    //--These are the statistics that work between the different combat types. This function should
    //  be called after combat is activated.
    mPlayerHP = pHP;
    mPlayerHPMax = pHPMax;
    mPlayerAtk = pAtk;
    mPlayerDef = pDef;

    //--Clamp HPs.
    if(mPlayerHPMax < 1) mPlayerHPMax = 1;
    if(mPlayerHP > mPlayerHPMax) mPlayerHP = mPlayerHPMax;
    if(mPlayerHP < 0) mPlayerHP = 0;

    //--Store the image path.
    ResetString(mPlayerCombatRenderPath, pImgPath);
}
void TextLevel::SetCombatScripts(const char *pVictoryScript, const char *pDefeatScript)
{
    //--Store.
    ResetString(mCombatVictoryHandler, pVictoryScript);
    ResetString(mCombatDefeatHandler, pDefeatScript);
}

///======================================= Core Methods ===========================================
void TextLevel::ActivateCombat()
{
    ///--[Documentation]
    //--Resets the combat handler to their neutral states. From here, functions can be called to
    //  modify combat properties. If the handlers were not constructed yet, that happens now.

    //--Combat Flags.
    mIsCombatMode = mUseCombatType;

    //--Construction. Crossload information to the typing combat handler.
    if(mIsCombatMode == TL_COMBAT_TYPING)
    {
        if(!mTypingCombatHandler) mTypingCombatHandler = new TypingCombat();
        mTypingCombatHandler->Initialize();
        mTypingCombatHandler->SetPlayerStats(mPlayerHP, mPlayerHPMax, mPlayerAtk, mPlayerDef, 7, mPlayerCombatRenderPath);
    }
    //--Word Combat handler.
    else if(mIsCombatMode == TL_COMBAT_WORD)
    {
        //--Pause the music tracks.
        TextLevMusicPack *rPack = (TextLevMusicPack *)mMusicTracksList->PushIterator();
        while(rPack)
        {
            rPack->mStoredVolume = rPack->mDesiredVolume;
            rPack->mDesiredVolume = rPack->mDesiredVolume * 0.10f;
            rPack = (TextLevMusicPack *)mMusicTracksList->AutoIterate();
        }

        //--Boot.
        if(!mWordCombatHandler) mWordCombatHandler = new WordCombat();
        mWordCombatHandler->Initialize();

        //--Combat type.
        if(mUseCombatActiveMode)
            mWordCombatHandler->SetActiveCombat();
        else
            mWordCombatHandler->SetTurnCombat();
    }
    //--Another other value: Print a warning, reset to no combat.
    else
    {
        fprintf(stderr, "Warning: Unable to activate combat. Use TL_SetProperty(\"Set Combat Type\", iType) to specify one on boot.\n");
        mIsCombatMode = TL_COMBAT_NONE;
    }
}
void TextLevel::FinalizeCombat()
{
    //--Begins combat in earnest. Variables passed after this point are probably invalid, though
    //  the subclasses determine that on their own.
    if(mIsCombatMode == TL_COMBAT_TYPING && mTypingCombatHandler)
    {
        mTypingCombatHandler->BeginPlayerAttackTurn();
    }
    else if(mIsCombatMode == TL_COMBAT_WORD && mWordCombatHandler)
    {
        mWordCombatHandler->Finalize();
    }
}
void TextLevel::ReresolveStatsAfterCombat()
{
    //--Determines what the player's stats are after combat is over. This is usually just HP.
    if(mUseCombatType == TL_COMBAT_TYPING && mTypingCombatHandler)
    {
        mPlayerHP = mTypingCombatHandler->GetPlayerHP();
    }
    else if(mUseCombatType == TL_COMBAT_WORD && mWordCombatHandler)
    {
        mPlayerHP = mWordCombatHandler->GetPlayerHP();
        mPotionCount = mWordCombatHandler->GetPotions();
    }
}

///========================================== Update ==============================================
void TextLevel::UpdateCombat()
{
    //--Setup.
    int tResolutionCode = TL_COMBAT_PLAYER_WINS;

    //--Run the update and get the resolution code.
    if(mIsCombatMode == TL_COMBAT_TYPING && mTypingCombatHandler)
    {
        mTypingCombatHandler->Update();
        tResolutionCode = mTypingCombatHandler->GetCombatResolution();
    }
    //--Word combat version.
    else if(mIsCombatMode == TL_COMBAT_WORD && mWordCombatHandler)
    {
        mWordCombatHandler->Update();
        tResolutionCode = mWordCombatHandler->GetCombatResolution();
    }
    //--No combat!
    else
    {

    }

    //--Combat is continuing. Do nothing.
    if(tResolutionCode == TL_COMBAT_UNRESOLVED)
    {
    }
    //--A turn has ended in combat. Allow the world to update.
    else if(tResolutionCode == TL_COMBAT_TURN_HAS_ENDED)
    {
    }
    //--The player won!
    else if(tResolutionCode == TL_COMBAT_PLAYER_WINS)
    {
        mIsCombatMode = TL_COMBAT_NONE;
        if(mCombatVictoryHandler) LuaManager::Fetch()->ExecuteLuaFile(mCombatVictoryHandler);
    }
    //--The player was defeated...
    else
    {
        mIsCombatMode = TL_COMBAT_NONE;
        if(mCombatDefeatHandler) LuaManager::Fetch()->ExecuteLuaFile(mCombatDefeatHandler);
    }
}

///========================================== Drawing =============================================
void TextLevel::RenderCombat()
{
    if(mIsCombatMode == TL_COMBAT_TYPING && mTypingCombatHandler)
    {
        mTypingCombatHandler->Render();
    }
    else if(mIsCombatMode == TL_COMBAT_WORD && mWordCombatHandler)
    {
        mWordCombatHandler->Render();
    }
}
