//--Base
#include "PandemoniumLevel.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//--Underlays render under the map and make things look nicer. They are entirely optional.

void PandemoniumLevel::AllocateUnderlays(int pTotal)
{
    //--Deallocate existing underlays.
    mUnderlayMapsTotal = 0;
    free(mUnderlayZLevels);
    free(mUnderMapOffsetX);
    free(mUnderMapOffsetY);
    free(rUnderlayMaps);
    mUnderlayZLevels = NULL;
    mUnderMapOffsetX = NULL;
    mUnderMapOffsetY = NULL;
    rUnderlayMaps = NULL;
    if(pTotal < 1) return;

    //--Allocate and null the new space.
    mUnderlayMapsTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mUnderlayZLevels = (int *)starmemoryalloc(sizeof(int) * mUnderlayMapsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mUnderMapOffsetX = (float *)starmemoryalloc(sizeof(float) * mUnderlayMapsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mUnderMapOffsetY = (float *)starmemoryalloc(sizeof(float) * mUnderlayMapsTotal);
    SetMemoryData(__FILE__, __LINE__);
    rUnderlayMaps = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mUnderlayMapsTotal);

    //--Clear the space to legal values. Only bitmaps actually need clearing.
    memset(rUnderlayMaps, 0, sizeof(StarBitmap *) * mUnderlayMapsTotal);
}
void PandemoniumLevel::SetUnderlay(int pSlot, int pZLevel, float pOffsetX, float pOffsetY, const char *pBitmapPath)
{
    //--Sets the underlay, or does nothing if out of range. pBitmapPath can legally be NULL, which resets the underlay to NULL.
    if(pSlot < 0 || pSlot >= mUnderlayMapsTotal) return;
    mUnderlayZLevels[pSlot] = pZLevel;
    mUnderMapOffsetX[pSlot] = pOffsetX;
    mUnderMapOffsetY[pSlot] = pOffsetY;
    rUnderlayMaps[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pBitmapPath);
}
void PandemoniumLevel::RenderUnderlay(int pZLevel)
{
    //--Renders the underlay that matches the Z-level. Assumes the camera translation has already been handled. May render
    //  nothing if no Z-level matches.
    for(int i = 0; i < mUnderlayMapsTotal; i ++)
    {
        //--Skip non-matches.
        if(mUnderlayZLevels[i] != pZLevel || !rUnderlayMaps[i]) continue;

        //--Setup.
        float cScale = 0.45f;
        float cWid = rUnderlayMaps[i]->GetTrueWidth() * 0.5f;
        float cHei = rUnderlayMaps[i]->GetTrueHeight() * 0.5f;

        //--Render. Note that if multiple underlays are found, they all render together!
        glTranslatef(mUnderMapOffsetX[i], mUnderMapOffsetY[i], 0.0f);

        //--Scale down about the center point.
        glTranslatef(cWid, cHei, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        glTranslatef(-cWid, -cHei, 0.0f);

        //--Render.
        rUnderlayMaps[i]->Draw();

        //--Scale back up about the center point.
        glTranslatef(cWid, cHei, 0.0f);
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-cWid, -cHei, 0.0f);

        //--Clean translation.
        glTranslatef(-mUnderMapOffsetX[i], -mUnderMapOffsetY[i], 0.0f);
    }
}
