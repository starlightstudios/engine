//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///==================================== Structure Functions =======================================
///--[Target Cluster]
//--Defined in AdvCombatStruct.h
//--TargetCluster is a wrapper around a StarLinkedList that contains a set of AdvCombatEntities.
//  The list does not allow duplicates.
uint32_t TargetCluster::xTargetClusterIDs = 0;
void TargetCluster::Initialize()
{
    xTargetClusterIDs ++;
    mUniqueID = xTargetClusterIDs;
    mDisplayName = InitializeString("Unnamed");
    mrPredictionBoxList = new StarLinkedList(false);
    mrTargetList = new StarLinkedList(false);
}
void TargetCluster::DeleteThis(void *pPtr)
{
    //--Error check.
    if(!pPtr) return;

    //--Deallocate.
    TargetCluster *rPtr = (TargetCluster *)pPtr;
    free(rPtr->mDisplayName);
    delete rPtr->mrPredictionBoxList;
    delete rPtr->mrTargetList;
    free(rPtr);
}
void TargetCluster::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
bool TargetCluster::IsElementInCluster(void *pPtr)
{
    return mrTargetList->IsElementOnList(pPtr);
}
void TargetCluster::RegisterElement(void *pElement)
{
    if(mrTargetList->IsElementOnList(pElement)) return;
    mrTargetList->AddElement("X", pElement);
}
void TargetCluster::RemoveElementI(int pSlot)
{
    mrTargetList->RemoveElementI(pSlot);
}
void TargetCluster::RemoveElementP(void *pPtr)
{
    mrTargetList->RemoveElementP(pPtr);
}
void TargetCluster::RemoveElementID(uint32_t pUniqueID)
{
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrTargetList->SetToHeadAndReturn();
    while(rEntity)
    {
        if(rEntity->GetID() == pUniqueID)
        {
            mrTargetList->RemoveRandomPointerEntry();
        }

        rEntity = (AdvCombatEntity *)mrTargetList->IncrementAndGetRandomPointerEntry();
    }
}
TargetCluster *TargetCluster::Clone()
{
    //--Create.
    TargetCluster *nCluster = (TargetCluster *)starmemoryalloc(sizeof(TargetCluster));
    nCluster->Initialize();
    nCluster->SetDisplayName(mDisplayName);

    //--Clone the target list.
    void *rPtr = mrTargetList->PushIterator();
    while(rPtr)
    {
        nCluster->mrTargetList->AddElementAsTail(mrTargetList->GetIteratorName(), rPtr);
        rPtr = mrTargetList->AutoIterate();
    }

    //--Clone the prediction box list.
    rPtr = mrPredictionBoxList->PushIterator();
    while(rPtr)
    {
        nCluster->mrPredictionBoxList->AddElementAsTail(mrPredictionBoxList->GetIteratorName(), rPtr);
        rPtr = mrPredictionBoxList->AutoIterate();
    }

    //--Pass it back.
    return nCluster;
}

///========================================== System ==============================================
void AdvCombat::ClearTargetClusters()
{
    //--Do not remove target clusters via any means except this or Reinitialize(). Target clusters
    //  may be in use, in which case they get locked during ability execution to prevent dereference
    //  errors. Only when an ability is executing do the clusters get locked.
    if(mLockTargetClusters) return;
    mTargetClusterCounter = 0;
    mTargetClusters->ClearList();
}
void AdvCombat::MovePartyByActiveCluster()
{
    //--If any party members are in the active target cluster, they move onscreen. Those who are not
    //  move offscreen. This should only be called when the target cluster changes.
    //--First, if there's no active target cluster, move them all offscreen.
    if(!rActiveTargetCluster)
    {
        //--All non-acting party members are offscreen.
        AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rPartyMember)
        {
            //--Calc.
            float tIdealX = ComputeIdealX(true, true, 0, 1, rPartyMember->GetCombatPortrait());
            rPartyMember->SetIdealPositionX(tIdealX);
            rPartyMember->MoveToIdealPosition(cStdMoveTicks);

            //--Next.
            rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }

        //--Acting entity stays onscreen.
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
        if(rActingEntity && mrCombatParty->IsElementOnList(rActingEntity))
        {
            float tIdealX = ComputeIdealX(true, false, 0, 1, NULL);
            rActingEntity->SetIdealPositionX(tIdealX);
            rActingEntity->MoveToIdealPosition(cStdMoveTicks);
        }

        return;
    }

    //--Check the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

    //--First, count how many party members are in the target cluster. Skip the acting entity if is found.
    int tPartyCount = 0;
    void *rCheckPtr = rActiveTargetCluster->mrTargetList->PushIterator();
    while(rCheckPtr)
    {
        if(mrCombatParty->IsElementOnList(rCheckPtr) && rCheckPtr != rActingEntity)
        {
            tPartyCount ++;
        }
        rCheckPtr = rActiveTargetCluster->mrTargetList->AutoIterate();
    }

    //--If a party member is acting, they count here.
    if(mrCombatParty->IsElementOnList(rActingEntity)) tPartyCount ++;

    //--If there were no party members, order them all offscreen.
    if(tPartyCount < 1)
    {
        AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rPartyMember)
        {
            float tIdealX = ComputeIdealX(true, true, 0, 1, rPartyMember->GetCombatPortrait());
            rPartyMember->SetIdealPositionX(tIdealX);
            rPartyMember->MoveToIdealPosition(cStdMoveTicks);
            rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }
    }
    //--At least one party member in combat. Everyone but them moves offscreen, they move onscreen.
    else
    {
        //--Acting entity always gets the 0th slot.
        int i = 0;
        float tIdealX = 0.0f;
        if(mrCombatParty->IsElementOnList(rActingEntity))
        {
            tIdealX = ComputeIdealX(true, false, i, tPartyCount, NULL);
            rActingEntity->SetIdealPositionX(tIdealX);
            rActingEntity->MoveToIdealPosition(cStdMoveTicks);
            i ++;
        }

        AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rPartyMember)
        {
            //--If this is the acting entity, skip them:
            if(rPartyMember == rActingEntity)
            {
                rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
                continue;
            }

            //--Offscreen:
            if(!rActiveTargetCluster->IsElementInCluster(rPartyMember))
            {
                tIdealX = ComputeIdealX(true, true, 0, 1, rPartyMember->GetCombatPortrait());
            }
            //--Onscreen.
            else
            {
                tIdealX = ComputeIdealX(true, false, i, tPartyCount, rPartyMember->GetCombatPortrait());
                i ++;
            }

            //--Common.
            rPartyMember->SetIdealPositionX(tIdealX);
            rPartyMember->MoveToIdealPosition(cStdMoveTicks);

            //--Next.
            rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }
    }
}

///===================================== Property Queries =========================================
const char *AdvCombat::GetLastTargetCode()
{
    return mLastUsedTargetType;
}
int AdvCombat::GetTargetClusterTotal()
{
    return mTargetClusters->GetListSize();
}
const char *AdvCombat::GetNameOfTargetCluster(int pSlot)
{
    return mTargetClusters->GetNameOfElementBySlot(pSlot);
}
TargetCluster *AdvCombat::GetTargetClusterI(int pSlot)
{
    return (TargetCluster *)mTargetClusters->GetElementBySlot(pSlot);
}
TargetCluster *AdvCombat::GetTargetClusterS(const char *pClusterName)
{
    //--Can return NULL if the cluster doesn't exist.
    return (TargetCluster *)mTargetClusters->GetElementByName(pClusterName);
}
AdvCombatEntity *AdvCombat::GetEntityInCluster(const char *pClusterName, int pSlot)
{
    //--Can return NULL if the cluster doesn't exist, or if the entity doesn't exist.
    TargetCluster *rCluster = GetTargetClusterS(pClusterName);
    if(!rCluster) return NULL;
    return (AdvCombatEntity *)rCluster->mrTargetList->GetElementBySlot(pSlot);
}
TargetCluster *AdvCombat::GetActiveCluster()
{
    //--Only usable when targets have been painted. This is used when an AI is selecting a target, or
    //  when the player has selected a target. Do NOT use thisfor ability execution.
    return rActiveTargetCluster;
}

///======================================== Manipulators ==========================================
void AdvCombat::ResetTargetCode()
{
    ResetString(mLastUsedTargetType, "Custom");
}
TargetCluster *AdvCombat::CreateTargetCluster(const char *pClusterName, const char *pDisplayName)
{
    ///--[Documentation]
    //--Creates a target cluster with the given name and returns it. If the name is in error, creates
    //  a dummy cluster and barks an error.
    //--Note that the first name is the internal name, the second name is the display name the player
    //  will see. It is possible for the display name and cluster name to be identical, and it is possible
    //  for many clusters to share the same display name, but it is not possible for two clusters to
    //  use the same name. If that happens, the cluster that already has the name is returned instead.
    //--Passing NULL will return a cluster named "DUMMY CLUSTER".

    //--No cluster name or display name: Return "DUMMY CLUSTER".
    if(!pClusterName || !pDisplayName)
    {
        //--If the dummy cluster already exists, just return it.
        TargetCluster *rDummyCluster = (TargetCluster *)mTargetClusters->GetElementByName("DUMMY CLUSTER");
        if(rDummyCluster) return rDummyCluster;

        //--No dummy cluster exists yet, so create one and return that.
        SetMemoryData(__FILE__, __LINE__);
        TargetCluster *nTargetCluster = (TargetCluster *)starmemoryalloc(sizeof(TargetCluster));
        nTargetCluster->Initialize();
        nTargetCluster->SetDisplayName("DUMMY CLUSTER");
        mTargetClusters->AddElementAsTail("DUMMY CLUSTER", nTargetCluster, TargetCluster::DeleteThis);

        //--Bark a warning.
        DebugManager::ForcePrint("AdvCombat:CreateTargetCluster() - Error, cluster name was NULL.\n");
        return nTargetCluster;
    }

    //--Check if the named cluster already exists. If it does, return it.
    TargetCluster *rCheckCluster = (TargetCluster *)mTargetClusters->GetElementByName(pClusterName);
    if(rCheckCluster) return rCheckCluster;

    //--New cluster, create and return.
    mTargetClusterCounter ++;
    SetMemoryData(__FILE__, __LINE__);
    TargetCluster *nTargetCluster = (TargetCluster *)starmemoryalloc(sizeof(TargetCluster));
    nTargetCluster->Initialize();
    nTargetCluster->SetDisplayName(pDisplayName);
    mTargetClusters->AddElementAsTail(pClusterName, nTargetCluster, TargetCluster::DeleteThis);
    return nTargetCluster;
}
void AdvCombat::RemoveTargetClusterI(int pIndex)
{
    mTargetClusters->RemoveElementI(pIndex);
}
void AdvCombat::RemoveTargetClusterS(const char *pClusterName)
{
    mTargetClusters->RemoveElementS(pClusterName);
}
void AdvCombat::RegisterElementToCluster(const char *pClusterName, void *pElement)
{
    //--Worker function, adds the given element to the given cluster.
    if(!pClusterName || !pElement) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetClusterS(pClusterName);
    if(!rTargetCluster) return;

    //--Add it. Duplicates are implicitly ignored.
    rTargetCluster->RegisterElement(pElement);
}
void AdvCombat::RegisterEntityToClusterByID(const char *pClusterName, uint32_t pUniqueID)
{
    //--Searches all lists and finds the entity that has the unique ID, then adds them to the named cluster.
    if(!pClusterName || !pUniqueID) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetClusterS(pClusterName);
    if(!rTargetCluster) return;

    //--Scan the party roster.
    AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pUniqueID)
        {
            rTargetCluster->RegisterElement(rCheckEntity);
            mPartyRoster->PopIterator();
            return;
        }
        rCheckEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }

    //--Scan the enemy roster.
    rCheckEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pUniqueID)
        {
            rTargetCluster->RegisterElement(rCheckEntity);
            mEnemyRoster->PopIterator();
            return;
        }
        rCheckEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--Didn't find in either group. Report.
    DebugManager::ForcePrint("AdvCombat:RegisterEntityToClusterByID() - Error, could not find ID %i.\n", pUniqueID);
}
void AdvCombat::RemoveEntityFromClusterI(const char *pClusterName, int pSlot)
{
    //--Removes the element in the given slot from the cluster.
    if(!pClusterName) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetClusterS(pClusterName);
    if(!rTargetCluster) return;

    //--Run.
    rTargetCluster->RemoveElementI(pSlot);
}
void AdvCombat::RemoveEntityFromClusterP(const char *pClusterName, void *pPtr)
{
    //--Removes the element with the given pointer from the cluster.
    if(!pClusterName || !pPtr) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetClusterS(pClusterName);
    if(!rTargetCluster) return;

    //--Run.
    rTargetCluster->RemoveElementP(pPtr);
}

///====================================== Addition Macros =========================================
//--Macros to quickly add elements to clusters.
void AdvCombat::AddPartyTargetToClusterI(const char *pClusterName, int pSlot)
{
    //--Locates a target in the player's party by slot and adds it to the named cluster.
    RegisterElementToCluster(pClusterName, mrCombatParty->GetElementBySlot(pSlot));
}
void AdvCombat::AddPartyTargetToClusterS(const char *pClusterName, const char *pPartyMemberName)
{
    //--Locates a target in the player's party by slot and adds it to the named cluster.
    RegisterElementToCluster(pClusterName, mrCombatParty->GetElementByName(pPartyMemberName));
}
void AdvCombat::AddEnemyTargetToClusterI(const char *pClusterName, int pSlot)
{
    //--Locates a target in the enemy party by slot and adds it to the named cluster. Note that
    //  this does not check the enemy graveyard.
    RegisterElementToCluster(pClusterName, mrEnemyCombatParty->GetElementBySlot(pSlot));
}
void AdvCombat::AddEnemyTargetToClusterS(const char *pClusterName, const char *pEnemyName)
{
    //--Locates a target in the enemy party by slot and adds it to the named cluster. Note that
    //  this does not check the enemy graveyard.
    RegisterElementToCluster(pClusterName, mrEnemyCombatParty->GetElementByName(pEnemyName));
}

///===================================== Autobuild Macros =========================================
//--Given a caller, populates targets by common codes. This is usually called once and may create
//  many clusters by itself.
void AdvCombat::PopulateTargetsByCode(const char *pCodeName, void *pCaller)
{
    //--Error check:
    if(!pCodeName || !pCaller) return;

    //--Buffer.
    char tNewNameBuf[32];

    //--Storage. Store the last-used target code. Some AI scripts require this.
    ResetString(mLastUsedTargetType, pCodeName);

    //--Target the user.
    if(!strcasecmp(pCodeName, "Target Self"))
    {
        AdvCombatEntity *rCaller = (AdvCombatEntity *)pCaller;
        TargetCluster *rNewCluster = CreateTargetCluster("Self", rCaller->GetDisplayName());
        rNewCluster->RegisterElement(rCaller);
    }
    //--Target all enemies, each getting its own cluster. Does not target downed enemies. Each
    //  cluster uses the display name of the entity it contains.
    else if(!strcasecmp(pCodeName, "Target Enemies Single"))
    {
        //--Resolve which list to use. Target whichever group the caller isn't in.
        StarLinkedList *rUseList = mrEnemyCombatParty;
        if(rUseList->IsElementOnList(pCaller))
        {
            rUseList = mrCombatParty;
        }

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Skip downed entities.
            if(rEntity->IsDefeated())
            {
                rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
                continue;
            }

            //--Create a name for the cluster. This is guaranteed to be unique.
            sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

            //--Create, add.
            TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
            rNewCluster->RegisterElement(rEntity);

            //--Next.
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all enemies in a single cluster. Does not target downed enemies.
    else if(!strcasecmp(pCodeName, "Target Enemies All"))
    {
        //--Resolve which list to use. Target whichever group the caller isn't in.
        StarLinkedList *rUseList = mrEnemyCombatParty;
        if(rUseList->IsElementOnList(pCaller))
        {
            rUseList = mrCombatParty;
        }

        //--Create a cluster.
        sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
        TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, "All Enemies");

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Skip downed entities.
            if(rEntity->IsDefeated())
            {
                rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
                continue;
            }

            rNewCluster->RegisterElement(rEntity);
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all allies, each getting its own cluster. Does not target downed allies. Each
    //  cluster uses the display name of the entity it contains.
    else if(!strcasecmp(pCodeName, "Target Allies Single"))
    {
        //--Resolve which list to use. Target whichever group the caller is in.
        StarLinkedList *rUseList = mrCombatParty;
        if(!rUseList->IsElementOnList(pCaller)) rUseList = mrEnemyCombatParty;

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Skip entities who are downed.
            if(!rEntity->IsDefeated())
            {
                //--Create a name for the cluster. This is guaranteed to be unique.
                sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

                //--Create, add.
                TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
                rNewCluster->RegisterElement(rEntity);
            }

            //--Next.
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all allies, each getting its own cluster. Targets downed and not-downed allies alike.
    else if(!strcasecmp(pCodeName, "Target Allies Single Option Downed"))
    {
        //--Resolve which list to use. Target whichever group the caller is in.
        StarLinkedList *rUseList = mrCombatParty;
        if(!rUseList->IsElementOnList(pCaller)) rUseList = mrEnemyCombatParty;

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Create a name for the cluster. This is guaranteed to be unique.
            sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

            //--Create, add.
            TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
            rNewCluster->RegisterElement(rEntity);

            //--Next.
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all allies, each getting its own cluster. Does not target the caller.
    else if(!strcasecmp(pCodeName, "Target Allies Single Not Self"))
    {
        //--Resolve which list to use. Target whichever group the caller is in.
        StarLinkedList *rUseList = mrCombatParty;
        if(!rUseList->IsElementOnList(pCaller)) rUseList = mrEnemyCombatParty;

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Skip entities who are downed, or are the caller.
            if(!rEntity->IsDefeated() && pCaller != rEntity)
            {
                //--Create a name for the cluster. This is guaranteed to be unique.
                sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

                //--Create, add.
                TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
                rNewCluster->RegisterElement(rEntity);
            }

            //--Next.
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all allies that are downed, giving each a cluster.
    else if(!strcasecmp(pCodeName, "Target Allies Single Downed Only"))
    {
        //--Resolve which list to use. Target whichever group the caller is in.
        StarLinkedList *rUseList = mrCombatParty;
        if(!rUseList->IsElementOnList(pCaller)) rUseList = mrEnemyCombatParty;

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Must be downed.
            if(rEntity->IsDefeated())
            {
                //--Create a name for the cluster. This is guaranteed to be unique.
                sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

                //--Create, add.
                TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
                rNewCluster->RegisterElement(rEntity);
            }

            //--Next.
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all allies in a single cluster.
    else if(!strcasecmp(pCodeName, "Target Allies All"))
    {
        //--Resolve which list to use. Target whichever group the caller isn't in.
        StarLinkedList *rUseList = mrCombatParty;
        if(!rUseList->IsElementOnList(pCaller))
        {
            rUseList = mrEnemyCombatParty;
        }

        //--Create a cluster.
        sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
        TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, "All Allies");

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Skip entities who are downed.
            if(!rEntity->IsDefeated())
            {
                rNewCluster->RegisterElement(rEntity);
            }
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all allies in a single cluster, excluding the user.
    else if(!strcasecmp(pCodeName, "Target Allies All Notself"))
    {
        //--Resolve which list to use. Target whichever group the caller isn't in.
        StarLinkedList *rUseList = mrCombatParty;
        if(!rUseList->IsElementOnList(pCaller))
        {
            rUseList = mrEnemyCombatParty;
        }

        //--Create a cluster.
        sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
        TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, "All Allies");

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Skip entities who are downed, or are the caller.
            if(!rEntity->IsDefeated() && pCaller != rEntity)
            {
                rNewCluster->RegisterElement(rEntity);
            }
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Creates a cluster for every target on the field, friend or foe.
    else if(!strcasecmp(pCodeName, "Target All Single"))
    {
        //--Player's combat party.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rEntity)
        {
            //--Skip downed entities.
            if(rEntity->IsDefeated())
            {
                rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
                continue;
            }

            //--Create a name for the cluster. This is guaranteed to be unique.
            sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

            //--Create, add.
            TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
            rNewCluster->RegisterElement(rEntity);

            //--Next.
            rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }

        //--Enemy combat party.
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEntity)
        {
            //--Skip downed entities.
            if(rEntity->IsDefeated())
            {
                rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
                continue;
            }

            //--Create a name for the cluster. This is guaranteed to be unique.
            sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

            //--Create, add.
            TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
            rNewCluster->RegisterElement(rEntity);

            //--Next.
            rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }
    //--Creates a cluster for the player's party, and one for the enemy party.
    else if(!strcasecmp(pCodeName, "Target Parties"))
    {
        //--Resolve which list to use. Target whichever group the caller isn't in.
        StarLinkedList *rUseList = mrCombatParty;
        StarLinkedList *rOpposeList = mrEnemyCombatParty;
        if(!rUseList->IsElementOnList(pCaller))
        {
            rUseList = mrEnemyCombatParty;
            rOpposeList = mrCombatParty;
        }

        //--Create a cluster for allies.
        sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
        TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, "All Allies");

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            rNewCluster->RegisterElement(rEntity);
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }

        //--Create a cluster for enemies.
        sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
        rNewCluster = CreateTargetCluster(tNewNameBuf, "All Enemies");

        //--For each element on the list, add them as a target.
        rEntity = (AdvCombatEntity *)rOpposeList->PushIterator();
        while(rEntity)
        {
            rNewCluster->RegisterElement(rEntity);
            rEntity = (AdvCombatEntity *)rOpposeList->AutoIterate();
        }
    }
    //--Creates a single cluster containing every entity on the field.
    else if(!strcasecmp(pCodeName, "Target All"))
    {
        //--Create cluster.
        TargetCluster *rNewCluster = CreateTargetCluster("AUTOCLUS000", "Everyone");

        //--Add each party member as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rEntity)
        {
            //--Skip downed entities.
            if(rEntity->IsDefeated())
            {
                rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
                continue;
            }

            rNewCluster->RegisterElement(rEntity);
            rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }

        //--Add each enemy as a target.
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEntity)
        {
            //--Skip downed entities.
            if(rEntity->IsDefeated())
            {
                rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
                continue;
            }

            rNewCluster->RegisterElement(rEntity);
            rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }
    //--Targets entities with specific internal names. The format is "Target Single Entity By Name Option Downed NAME1|NAME2|NAME3|" with
    //  '|' being delimiters. Allows downed entities.
    else if(!strncasecmp(pCodeName, "Target Single Entity By Name Option Downed ", 43))
    {
        //--Run the subdivide routine.
        StarLinkedList *tNameList = Subdivide::SubdivideStringToList(&pCodeName[43], "|");

        //--Iterate across the list.
        char *rString = (char *)tNameList->PushIterator();
        while(rString)
        {
            //--Scan player party.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
            while(rEntity)
            {
                //--Entity's internal name must match.
                if(strcasecmp(rEntity->GetName(), rString))
                {
                    rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
                    continue;
                }

                //--Add a new cluster for the entity.
                sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
                TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
                rNewCluster->RegisterElement(rEntity);

                //--Next.
                rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
            }

            //--Scan enemy party.
            rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
            while(rEntity)
            {
                //--Entity's internal name must match.
                if(strcasecmp(rEntity->GetName(), rString))
                {
                    rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
                    continue;
                }

                //--Add a new cluster for the entity.
                sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
                TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
                rNewCluster->RegisterElement(rEntity);

                //--Next.
                rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
            }

            //--Next.
            rString = (char *)tNameList->AutoIterate();
        }

        //--Clean up.
        delete tNameList;
    }
    //--Targets entities with specific internal names. The format is "Target Single Entity By Name NAME1|NAME2|NAME3|" with
    //  '|' being delimiters.
    else if(!strncasecmp(pCodeName, "Target Single Entity By Name ", 29))
    {
        //--Run the subdivide routine.
        StarLinkedList *tNameList = Subdivide::SubdivideStringToList(&pCodeName[29], "|");

        //--Iterate across the list.
        char *rString = (char *)tNameList->PushIterator();
        while(rString)
        {
            //--Scan player party.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
            while(rEntity)
            {
                //--Entity's internal name must match.
                if(strcasecmp(rEntity->GetName(), rString))
                {
                    rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
                    continue;
                }

                //--Skip downed entities.
                if(rEntity->IsDefeated())
                {
                    rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
                    continue;
                }

                //--Add a new cluster for the entity.
                sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
                TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
                rNewCluster->RegisterElement(rEntity);

                //--Next.
                rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
            }

            //--Scan enemy party.
            rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
            while(rEntity)
            {
                //--Entity's internal name must match.
                if(strcasecmp(rEntity->GetName(), rString))
                {
                    rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
                    continue;
                }

                //--Skip downed entities.
                if(rEntity->IsDefeated())
                {
                    rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
                    continue;
                }

                //--Add a new cluster for the entity.
                sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);
                TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
                rNewCluster->RegisterElement(rEntity);

                //--Next.
                rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
            }

            //--Next.
            rString = (char *)tNameList->AutoIterate();
        }

        //--Clean up.
        delete tNameList;
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("AdvCombat:PopulateTargetsByCode() - Error, no macro %s.\n", pCodeName);
    }
}
