//--Base
#include "PeakFreaksFlyby.h"

//--Classes
#include "PeakFreaksLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitArea.h"

//--Libraries
//--Managers

///========================================== System ==============================================
PeakFreaksFlyby::PeakFreaksFlyby()
{
    ///--[System]
    mSubtype = PFFB_SUBTYPE_GUANO;
    mTimer = 0;

    ///--[Position]
    mXPosition = 0.0f;
    mYPosition = 0.0f;
    mTargetXPosition = 0.0f;

    ///--[Images]
    rImageA = NULL;
    rImageB = NULL;
}
PeakFreaksFlyby::~PeakFreaksFlyby()
{
}
void PeakFreaksFlyby::DeleteThis(void *pPtr)
{
    PeakFreaksFlyby *rPtr = (PeakFreaksFlyby *)pPtr;
    delete rPtr;
}

///===================================== Property Queries =========================================
bool PeakFreaksFlyby::IsComplete()
{
    return (mTimer >= PFFB_MAX_TICKS);
}

///======================================= Manipulators ===========================================
void PeakFreaksFlyby::SetSubtype(int pSubtype)
{
    if(pSubtype < 0 || pSubtype >= PFFB_SUBTYPE_MAX) return;
    mSubtype = pSubtype;
}
void PeakFreaksFlyby::SetImages(SugarBitmap *pImageA, SugarBitmap *pImageB)
{
    rImageA = pImageA;
    rImageB = pImageB;
}
void PeakFreaksFlyby::SetTargetPosition(float pXPosition, float pYPosition)
{
    //--Y position of this entity is PFFB_VERTICAL_OFFSET from the actual target, so the player has time to dodge.
    mTargetXPosition = pXPosition;
    mYPosition = pYPosition + PFFB_VERTICAL_OFFSET;
}
void PeakFreaksFlyby::SetEndingPosition(float pXPosition)
{
    mEndingXPosition = pXPosition;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksFlyby::Update()
{
    ///--[Timer]
    mTimer ++;

    ///--[Position Recompute]
    if(mTimer < PFFB_SEQ_COMP_A)
    {
        float cPercent = EasingFunction::QuadraticOut(mTimer, PFFB_SEQ_COMP_A);
        mXPosition = -100.0f + (mTargetXPosition + 100.0f) * cPercent;
    }
    else if(mTimer < PFFB_SEQ_COMP_B)
    {
        mXPosition = mTargetXPosition;
    }
    else if(mTimer < PFFB_SEQ_COMP_C)
    {
        float cPercent = EasingFunction::QuadraticOut(mTimer - PFFB_SEQ_COMP_B, PFFB_SEQ_COMP_C - PFFB_SEQ_COMP_B);
        mXPosition = mTargetXPosition + ((mEndingXPosition - mTargetXPosition) * cPercent);
    }

    ///--[Projectiles]
    //--On this tick, drop projectiles with the needed logic.
    if(mTimer != PFFB_SEQ_COMP_B) return;

    //--Retrieve the level.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return;

    //--Guano Bomb
    if(mSubtype == PFFB_SUBTYPE_GUANO)
    {
        //--Spawn 5 guano bombs in range 3.
        SugarLinkedList *tHitList = HitXLocationsCross(3, 5, false);
        Point2DI *rPoint = (Point2DI *)tHitList->PushIterator();
        while(rPoint)
        {
            rActiveLevel->SpawnGuanoProjectile((mXPosition / 16.0f) + rPoint->mX, ((mYPosition - PFFB_VERTICAL_OFFSET) / 16.0f) + rPoint->mY, 96.0f);
            rPoint = (Point2DI *)tHitList->AutoIterate();
        }

        //--Clean.
        delete tHitList;
    }
    //--Drone Strike
    else if(mSubtype == PFFB_SUBTYPE_BOMB)
    {
        //--Scatter the position. It can hit within a "fat cross" shape of size 2.
        int tXOffset = (rand() % 5) - 2;
        int tYOffset = (rand() % 5) - 2;

        //--Handle the corner cases.
        if(abs(tXOffset) == 2 && abs(tYOffset) == 2)
        {
            tXOffset = tXOffset / 2;
            tYOffset = tYOffset / 2;
        }

        rActiveLevel->SpawnDroneProjectile((mXPosition / 16.0f) + tXOffset, ((mYPosition - PFFB_VERTICAL_OFFSET) / 16.0f) + tYOffset, 96.0f);
    }
    //--Super Strike
    else if(mSubtype == PFFB_SUBTYPE_SUPERBOMB)
    {
        //--Spawn 3 projectibles. Only generate 2 because one always hits the 0,0 spot.
        SugarLinkedList *tHitList = HitXLocationsCross(2, 2, true);
        Point2DI *rPoint = (Point2DI *)tHitList->PushIterator();
        while(rPoint)
        {
            rActiveLevel->SpawnDroneProjectile((mXPosition / 16.0f) + rPoint->mX, ((mYPosition - PFFB_VERTICAL_OFFSET) / 16.0f) + rPoint->mY, 96.0f);
            rPoint = (Point2DI *)tHitList->AutoIterate();
        }

        //--Spawn the 0,0 hit.
        rActiveLevel->SpawnDroneProjectile((mXPosition / 16.0f) + 0, ((mYPosition - PFFB_VERTICAL_OFFSET) / 16.0f) + 0,  96.0f);
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void PeakFreaksFlyby::Render()
{
    //--Resolve frame.
    SugarBitmap *rRenderImg = rImageA;
    if(mTimer % 6 >= 3) rRenderImg = rImageB;
    if(!rRenderImg) return;

    //--Center it.
    float cXOff = rRenderImg->GetWidthSafe()  * -0.5f;
    float cYOff = rRenderImg->GetHeightSafe() * -0.5f;
    rRenderImg->Draw(mXPosition + cXOff, mYPosition + cYOff);
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
