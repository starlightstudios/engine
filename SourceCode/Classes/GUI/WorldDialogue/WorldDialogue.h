///======================================= WorldDialogue ===========================================
//--GUI piece used to represent dialogue, held as its own object in the MapManager but does not render automatically.
//  Instead, the commanding RootLevel has routines to issue its updates and rendering cycle.
//--Dialogue is under no obligation to have speaking characters and is not obligated to lock out the player's controls,
//  though it usually does. This is just the GUI part that shows the information.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
///--[DialogueCharacter]
//--Represents a single letter in the dialogue.
typedef struct
{
    //--Basics
    uint32_t mLetter; //UTF-8 compatible

    //--Flags
    bool mIsSpooky;
    bool mIsLarge;
    bool mIsSmall;

    //--Rendering
    int mTimer;
    float mAlpha;
    StarlightColor *rColor;

    //--Voice.
    const char *rCurrentVoice;

    //--Functions.
    void Initialize()
    {
        //--Basics
        mLetter = '\0';

        //--Flags
        mIsSpooky = false;
        mIsLarge = false;
        mIsSmall = false;

        //--Rendering
        mTimer = 0;
        mAlpha = 1.0f;
        rColor = NULL;

        //--Voice.
        rCurrentVoice = NULL;
    }
}DialogueCharacter;

///--[DecisionPack]
//--Represents a decision in the dialogue.
typedef struct DecisionPack
{
    //--Basics
    char *mText;

    //--Firing
    char *mContinuationScript;
    char *mFiringString;

    //--Methods
    void Initialize()
    {
        mText = NULL;
        mContinuationScript = NULL;
        mFiringString = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        DecisionPack *rPack = (DecisionPack *)pPtr;
        free(rPack->mText);
        free(rPack->mContinuationScript);
        free(rPack->mFiringString);
        free(rPack);
    }
}DecisionPack;

///--[CreditsPack]
//--Each pack represents some text to be shown in the credits. Can also have images!
typedef struct CreditsPack
{
    //--System
    int mDisplayType;
    int mTimerOffset;

    //--Fonts and Flags
    float mX;
    float mY;
    uint32_t mFlags;
    float mScale;
    StarFont *rFont;
    char mDisplayText[128];

    //--Image Handler
    StarBitmap *rDisplayImage;

    //--Fade-in/Fade-out
    int mFadeTicksIn;
    int mFadeTicksHold;
    int mFadeTicksOut;

    //--Scrolling
    float mScrollPixPerTick;

    //--Functions. Implemented in WorldDialogueCredits.cc
    void Initialize();
    void Delete();
    static void DeleteThis(void *pPtr);
    bool IsComplete(int pWorldTicks);
    void Update();
    void Render(int pWorldTicks);
}CreditsPack;

///===================================== Local Definitions ========================================
//--Visibility States
#define WD_INVISIBLE 0
#define WD_SHOWING 1
#define WD_VISIBLE 2
#define WD_HIDING 3

//--Text Speed Option Values
#define WD_TEXT_SPEED_VSLOW 0
#define WD_TEXT_SPEED_SLOW 1
#define WD_TEXT_SPEED_NORMAL 2
#define WD_TEXT_SPEED_FAST 3
#define WD_TEXT_SPEED_VFAST 4

//--Timers
#define WD_CHARACTER_CROSSFADE_TICKS 15
#define WD_SCENE_CROSSFADE_TICKS 45
#define WD_LETTER_FADE_TICKS 400
#define WD_MAJOR_SEQUENCE_FADE_TICKS 60
#define WD_HIDING_TICKS 30

//--Rendering Constants
#define WD_INDENT 8
#define WD_INDENT_MAINTEXTLFT 30.0f
#define WD_INDENT_MAINTEXTRGT 20.0f

//--Lockout Strings
#define WD_STANDARD_LOCKOUT "Dialogue"

//--Dialogue Actor Stuff
#define WD_DIALOGUE_BOT_HALF 3
#define WD_DIALOGUE_ACTOR_SLOTS 7
#define WD_ACTOR_NAME_MAX 64

//--Audio
#define WD_TEXT_TICK_INTERVAL 3.0f
#define WD_TEXT_TICK_FACTOR 0.666666f

//--Decisions
#define WD_DECISIONS_PER_PAGE 3

//--Visual Novel Constants
#define WD_VN_BG_TICKS 25
#define WD_VN_SLIDE_TICKS 25
#define WD_VN_MOVEDEST_INVALID -100000
#define WD_VN_MOVETPYE_NONE 0
#define WD_VN_MOVETPYE_TELEPORT 1
#define WD_VN_MOVETPYE_LINEAR 2
#define WD_VN_MOVETPYE_QUADIN 3
#define WD_VN_MOVETPYE_QUADOUT 4
#define WD_VN_MOVETPYE_QUADINOUT 5
#define WD_VN_MOVE_DEFAULT_TICKS 30

//--Credits
#define WD_CREDITS_TYPE_NONE -1
#define WD_CREDITS_TYPE_FADEIN 0
#define WD_CREDITS_TYPE_SCROLLUP 1

///========================================== Classes =============================================
class WorldDialogue : public RootObject
{
    private:
    protected:
    //--System
    bool mIsAnyKeyMode;
    bool mNeedsToReappend;
    bool mAutoHastenDialogue;
    bool mInstantTextMode;
    int mVisibilityState;
    int mHidingTimer;
    bool mAllKeysHasten;
    int mTagSkipLen;
    bool mUnstagedActorsCanRemap;                   //--If true, a dialogue actor is allowed to use its remaps to modify the speaker display name when
                                                    //  not in major sequence mode. If false, only global remaps can apply outside of major sequences.

    //--Constants
    float cDialogueMaxWidth;
    float cMajorSceneDarkening;
    float cMajorDialogueDarkening;

    //--Visual Novel Mode
    bool mIsVisualNovel;
    struct
    {
        //--Background Stuff
        int mBackgroundTimer;
        StarBitmap *rCurBackground;
        StarBitmap *rPrevBackground;

        //--Background Remaps
        StarLinkedList *mBackgroundRemaps;

        //--Actor Movement
        float mActorCurrents[WD_DIALOGUE_ACTOR_SLOTS];
        float mActorStarts[WD_DIALOGUE_ACTOR_SLOTS];
        float mActorTargets[WD_DIALOGUE_ACTOR_SLOTS];
        int mActorTimers[WD_DIALOGUE_ACTOR_SLOTS];
        int mActorTimersMax[WD_DIALOGUE_ACTOR_SLOTS];
        int mActorMoveFlags[WD_DIALOGUE_ACTOR_SLOTS];
        char *mActorPostExecs[WD_DIALOGUE_ACTOR_SLOTS];
    }VN;

    //--Sound
    bool mIsSilenced;
    float mTextTickTimer;

    //--Position
    float mTextScale;
    TwoDimensionReal mDimensions;
    TwoDimensionReal mUpperDimensions;

    //--Colors
    StarlightColor mWhitePack;

    //--Dialogue Storage
    bool mHasAppendedNonSpace;
    bool mDialogueUsesAutoresolveTarget;
    int mFadeTimer;
    bool mIsPrintingComplete;
    int mPrintTimeRemainder;
    char *mMajorSequenceSpeaker;
    char *mMajorSequenceSpeakerDisplay;
    StarLinkedList *mSpeakerNameRemaps;
    StarLinkedList *mDialogueLog;
    StarLinkedList *mDialogueList; //StarLinkedList *, master

    //--Decisions
    int mTicksSinceDecisionOpen;
    bool mIsDecisionMode;
    bool mDecisionUsesMouse;
    int mDecisionCursor;
    int mHighlightedDecision;
    float mDecisionScale;
    StarLinkedList *mDecisionList;
    StarLinkedList *mDecisionHitboxes; //TwoDimensionReal *, master
    TwoDimensionReal mDecisionDimensions[WD_DECISIONS_PER_PAGE];

    //--Topics
    bool mRunTopicsAfterDialogue;
    char mRunTopicsAfterDialogueName[STD_NPC_LETTERS];
    bool mIsTopicsMode;
    int mTopicTimer;
    int mTopicCursor;
    int mTopicScroll;
    int mLastTopicCursor;
    char mCurrentTopicActor[STD_NPC_LETTERS];
    DialogueTopic *mGoodbyeTopic;
    StarLinkedList *mTopicListing; //DialogueTopic *, master
    StarLinkedList *mrCurrentTopicListing;

    //--Continuation
    char *mContinuationScript;
    char *mContinuationString;

    //--Blocking
    bool mIsBlocking;
    bool mIsSoftBlocking;
    bool mIsPausing;
    int mSoftBlockTimer;
    char *mPendingDialogue;

    //--Temporary
    bool mHideDialogueBoxes;
    bool mIsSpookyText;
    bool mIsLargeText;
    bool mIsSmallText;
    int mLettersAppended;
    StarLinkedList *rActiveAppendList;

    //--Complex Actor Properties
    bool mIsMajorSequenceMode;
    bool mMajorSequenceNeedsReset;
    int mMajorSequenceTimer;
    int mMajorSequenceResetTimer;
    StarLinkedList *mActorBench; //DialogueActor *, master
    DialogueActor *mDialogueActors[WD_DIALOGUE_ACTOR_SLOTS];
    float mPortraitLookupsXDefault[WD_DIALOGUE_ACTOR_SLOTS];
    float mPortraitLookupsX[WD_DIALOGUE_ACTOR_SLOTS];
    float mPortraitLookupsXHeavyLft[WD_DIALOGUE_ACTOR_SLOTS];

    //--Actor Storage
    char mPreviousActorNames[WD_DIALOGUE_ACTOR_SLOTS][WD_ACTOR_NAME_MAX];
    char mPreviousActorEmotions[WD_DIALOGUE_ACTOR_SLOTS][WD_ACTOR_NAME_MAX];

    //--Crossfades
    DialogueActor *rPreviousActors[WD_DIALOGUE_ACTOR_SLOTS];
    StarBitmap *rPreviousImages[WD_DIALOGUE_ACTOR_SLOTS];
    int mPreviousFadeTimers[WD_DIALOGUE_ACTOR_SLOTS];
    float mPreviousXOffsets[WD_DIALOGUE_ACTOR_SLOTS];

    //--CG and TF Scene Handling
    bool mIsSceneMode;
    bool mIgnoreSceneOffsets;
    float mSceneOffsetX;
    float mSceneOffsetY;
    StarBitmap *rSceneImg;
    int mSceneTimer;
    int mSceneFadeTimeMax;
    int mSceneTicksPerFrame;
    int mSceneAnimFramesTotal;
    StarBitmap **mrSceneAnimFrames;
    bool mIgnoreOldSceneOffsets;
    float mOldSceneOffsetX;
    float mOldSceneOffsetY;
    int mSceneTransitionTimer;
    StarBitmap *rOldSceneImg;
    StarLinkedList *mCensorBarList;//TwoDimensionReal *, master
    StarLinkedList *mOldCensorBarList;//TwoDimensionReal *, master

    //--Scene Flash
    bool mIsSceneFlash;
    int mSceneFlashTimer;

    //--Voice Controller
    bool mIsFreshClear;
    char *mLeaderVoice;
    char *mLastSetSpeaker;
    const char *rCurrentVoice;
    char mNormalTextTick[32];
    StarLinkedList *mVoiceListing;

    //--String Entry
    bool mIsStringEntryMode;
    char *mStringEntryCallOnComplete;
    StringEntry *mStringEntryForm;

    //--Credits Sequence
    bool mIsCreditsSequence;
    int mCreditsTimerCur;
    int mCreditsTimerMax;
    StarLinkedList *mCreditsPacks; //CreditsPack*, master
    StarBitmap *rCreditsBackground;

    //--Asset Streaming
    StarLinkedList *mrAllShownDialoguePortraits; //StarBitmap *, reference

    //--Parser
    float mCurrentParserLen;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Borders
            StarBitmap *rBorderCard;
            StarBitmap *rNameBox;
            StarBitmap *rNamelessBox;
            StarBitmap *rNamePanel;

            //--Fonts.
            StarFont *rHeadingFont;
            StarFont *rDecisionFont;
            StarFont *rDialogueFont;
        }Data;
    }Images;


    public:
    //--System
    WorldDialogue();
    virtual ~WorldDialogue();
    virtual void Construct();

    //--Public Variables
    static int xAppendStack;
    static int xIsColon;
    static char *xTopicsDirectory;
    static float xUpperDialogueYOffset;
    static bool xUnloadDialoguePortraitsWhenClosing;
    static bool xDialogueUsesMouse;

    //--Property Queries
    bool IsVisible();
    bool IsManagingUpdates();
    bool IsMajorSequence();
    bool IsBlocking();
    bool IsStoppingEvents();
    bool HasTextFlowing();
    bool IsAutoHastening();
    int IsActorOnStage(void *pCheckPtr);

    //--Manipulators
    void Show();
    void Hide();
    void SetDialogueMaxWidth(float pLength);
    void SetMajorSceneDarkening(float pAlpha);
    void SetMajorDialogueDarkening(float pAlpha);
    void SetNormalTextTick(const char *pString);
    void SetAnyKeyMode(bool pFlag);
    void ActivateFlash(int pStartTicks);
    void SetTargetAutoresolve(bool pFlag);
    void BypassFade();
    void SetSilenceFlag(bool pFlag);
    void AddDialogueLockout();
    void ClearDialogueLockout();
    void SetSpeaker(const char *pSpeakerName);
    void SetSpeakerBySlot(int pSpeakerSlot);
    void AddSpeakerRemap(const char *pSpeakerName, const char *pRemapName);
    void SetContinuationScript(const char *pPath, const char *pString);
    void SetVoice(const char *pSpeaker);
    void SetLeaderVoice(const char *pLeaderVoice);
    void RegisterVoice(const char *pSpeaker, const char *pVoicePath);
    void SetHideFlag(bool pHideDialogueBoxes);
    void SetAutoHastenFlag(bool pAutoHastenDialogue);
    void SetInstantTextFlag(bool pFlag);
    void BypassBlock();
    void ActivateStringEntry(const char *pPostExec);
    void SetAllKeysHasten(bool pFlag);
    void ReplaceHeadingFont(const char *pDLFontName);
    void ReplaceDecisionFont(const char *pDLFontName);
    void ReplaceMainlineFont(const char *pDLFontName);
    void ReplaceNamelessBox(const char *pDLPath);
    void ReplaceDecisionBorderCard(const char *pDLPath);

    //--Core Methods
    void Clear();
    void Wipe();
    StarLinkedList *ResolveActiveDialogueList();
    virtual float ComputeLengthOf(StarLinkedList *pListOfCharacters);
    void WriteActorsToConsole();
    void WriteActorsInfoToConsole(const char *pName);
    void SetDisplaySpeakerFromName(const char *pName);

    //--Actor Stuff
    int GetSlotOfActor(const char *pName);
    void RegisterDialogueActor(DialogueActor *pActor);
    void UnregisterDialogueActor(const char *pReferenceName);
    void AddActorToSlot(const char *pReferenceName, int pSlot);
    void MoveActorToSlotI(int pStartSlot, int pDestinationSlot);
    void MoveActorToSlotS(const char *pSearchName, int pDestinationSlot);
    void RemoveActor(int pSlot);
    void ClearActors();
    void RestorePreviousActors();
    void RecomputeActorPositions();
    void SetActorEmotionI(int pSlot, const char *pEmotion);
    void SetActorEmotionS(const char *pReferenceName, const char *pEmotion);
    virtual void RenderDialogueActors(float pGlobalAlpha);

    //--Credits
    bool IsCreditsMode();
    void ActivateCreditsMode();
    void DeactivateCreditsMode();
    void SetCreditsTimeMax(int pTicks);
    void SetCreditsBackground(const char *pDLPath);
    void CreateCreditsPack();
    void UpdateCredits();
    void RenderCredits();

    //--Decisions
    bool IsDecisionMode();
    void SetDecisionMode(bool pFlag);
    void SetDecisionUsesMouse(bool pFlag);
    void AddDecision(const char *pTitle, const char *pFiringScript, const char *pFiringString);
    void ClearDecisions();
    void RefreshDecisionPositions();
    void UpdateDecisions();
    virtual void RenderDecisions();
    virtual void RenderDecisionsMouse();
    void SubrenderDecision(DecisionPack *pPack, float pXCenter, float pYCenter, bool pIsHighlighted);
    static void RenderDialogueBorderCard(StarBitmap *pBorderCard, float pLft, float pTop, float pRgt, float pBot);

    //--Major Sequence
    void SetMajorSequence(bool pFlag);
    virtual void RenderMajorSequence();

    //--Parser
    void AppendString(const char *pString, bool pAllowTranslation);
    int HandleSpecialSequences(const char *pString);
    void ResolveEmotionForForm(const char *pActorName, const char *pStartEmotion, char *sString);

    //--Saving
    void WriteToBuffer(StarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *fInfile);

    //--Scenes
    void ActivateScenesMode();
    void DeactivateScenesMode();
    void SetIgnoreSceneOffsets(bool pFlag);
    void SetSceneOffsets(float pX, float pY);
    void SetSceneFadeTimers(int pTicks);
    void ChangeSceneImage(const char *pImagePath);
    void ChangeSceneImageInstantly(const char *pImagePath);
    void ClearCensorBars();
    void RegisterCensorBar(float pLft, float pTop, float pWid, float pHei);
    void SendCensorBarsToCrossfade();
    void AllocateSceneAnimFrames(int pTotal, int pTicksPerFrame, int pStartingTick);
    void SetSceneAnimFrame(int pSlot, const char *pImagePath);
    virtual void RenderScenesMode();
    void RenderSceneImage(StarBitmap *pImage);
    void RenderCensorBars(StarLinkedList *pList);

    //--Topics
    bool IsTopicRead(const char *pName);
    void WipeTopicData();
    void SetAllTopicsToDefaults();
    void ActivateTopicsModeAfterDialogue(const char *pNPCName);
    void ActivateTopicsMode(const char *pNPCName);
    void SetGoodbyeName(const char *pName);
    void RegisterTopic(const char *pName, const char *pDisplayName, int pStartingLevel);
    void RegisterTopicFor(const char *pTopicName, const char *pNPCName, int pStartingLevel);
    void UnlockTopic(const char *pTopicName, int pMinimumLevel);
    void ClearTopicReadFlag(const char *pTopicName);
    void BuildTopicListing(const char *pNPCName);
    virtual void UpdateTopicsMode();
    virtual void RenderTopicsMode();
    void RenderTopicsMouse();

    //--Visual Novel
    void SetVisualNovelMode(bool pFlag);
    void SetVisualNovelBackground(const char *pPath);
    void RegisterBackgroundRemap(const char *pRemapName, const char *pPath);
    void UpdateVisualNovel();
    void RenderVisualNovelActors(float pGlobalAlpha);
    int HandleSpecialSequencesVN(const char *pString);
    int ComputeAdvancedFormula(const char *pString);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();
    void RunTextTimers();
    void HandleCloseOrContinue();
    void CloseImmediately();

    //--File I/O
    //--Drawing
    virtual void Render();
    virtual void RenderOnlyText(float pX, float pY, float pAlphaFactor);
    bool IsFlashSuppressingTicks();
    void RenderFlash();
    void RenderDiagnostics();

    //--Pointer Routing
    DialogueActor *GetActor(const char *pName);
    StarLinkedList *GetTopicList();

    //--Static Functions
    static WorldDialogue *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
    static void HookToLuaStateCredits(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_WD_GetProperty(lua_State *L);
int Hook_WD_SetProperty(lua_State *L);
int Hook_WD_AppendShorthand(lua_State *L);
int Hook_WD_SetCreditsProperty(lua_State *L);
