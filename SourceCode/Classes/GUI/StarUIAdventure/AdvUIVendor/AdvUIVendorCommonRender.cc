//--Base
#include "AdvUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================= Common Render ==========================================
//--Contains functions that are used to render in multiple different modes.

///======================================== Standalones ===========================================
void AdvUIVendor::RenderOwnerFace(float pXPos, float pYPos, AdvCombatEntity *pCharacter)
{
    ///--[Documentation]
    //--For items that are equipped to a character, renders their face next to the item's icon.
    if(!pCharacter) return;

    ///--[Setup]
    //--Get their face icon.
    TwoDimensionReal tFaceDim;
    StarBitmap *rFaceImg = pCharacter->GetFaceProperties(tFaceDim);
    if(!rFaceImg) return;

    //--Constants.
    float cPortraitH =  26.0f;

    //--Swap vertically to handle OpenGL.
    tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
    tFaceDim.mBot = 1.0f - (tFaceDim.mBot);

    ///--[Render]
    //--Render it.
    rFaceImg->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(pXPos,              pYPos);
        glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(pXPos + cPortraitH, pYPos);
        glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(pXPos + cPortraitH, pYPos + cPortraitH);
        glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(pXPos,              pYPos + cPortraitH);
    glEnd();
}
void AdvUIVendor::RenderModeTabs(float pColorAlpha)
{
    ///--[Documentation]
    //--At the top of the selection area is a list of Buy/Sell/Buyback etc based on what modes are
    //  available in this shop. This function renders the current tab based on what mode the UI
    //  is currently in.

    ///--[Constants]
    float cXBuy     = 319.0f;
    float cXSell    = 448.0f;
    float cXBuyback = 596.0f;
    float cXGems    = 745.0f;
    float cYCommon  =  97.0f;

    ///--[Highlighted Text]
    //--Renders first, has a different color than the others.
    if(mIsBuyMode)
        AdvImages.rFont_Heading->DrawText(cXBuy, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
    else if(mIsSellMode)
        AdvImages.rFont_Heading->DrawText(cXSell, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
    else if(mIsBuybackMode)
        AdvImages.rFont_Heading->DrawText(cXBuyback, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Buyback");
    else if(mIsGemsMode)
        AdvImages.rFont_Heading->DrawText(cXGems, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");

    ///--[Greyed Text]
    //--Renders greyed out.
    SetColor("Greyed", pColorAlpha);

    //--Render text.
    if(!mIsBuyMode)
        AdvImages.rFont_Heading->DrawText(cXBuy, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
    if(!mIsSellMode)
        AdvImages.rFont_Heading->DrawText(cXSell, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
    if(!mIsBuybackMode)
        AdvImages.rFont_Heading->DrawText(cXBuyback, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Buyback");
    if(!mIsGemsMode)
        AdvImages.rFont_Heading->DrawText(cXGems, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");

    //--Clear mixer.
    SetColor("Clear", pColorAlpha);
}
void AdvUIVendor::RenderModeTitles(float pColorAlpha)
{
    ///--[Documentation]
    //--At the top of most vendor frames is a set of titles. Name/Price/etc. This changes slightly
    //  between modes.

    ///--[Constants]
    float cXName = 269.0f;
    float cXPrice = 534.0f;
    float cXOwned = 637.0f;
    float cXSlots = 767.0f;
    float cYCommon = 142.0f;

    ///--[Render]
    //--Titles
    SetColor("Heading", pColorAlpha);
    AdvImages.rFont_Heading->DrawText(cXName,  cYCommon, 0, 1.0f, "Name");
    AdvImages.rFont_Heading->DrawText(cXPrice, cYCommon, 0, 1.0f, "Price");
    AdvImages.rFont_Heading->DrawText(cXSlots, cYCommon, 0, 1.0f, "Slots");

    //--The owned count only renders in buy/buyback mode, not sell.
    if(!mIsSellMode) AdvImages.rFont_Heading->DrawText(cXOwned, cYCommon, 0, 1.0f, "Owned");

    ///--[Clean]
    SetColor("Clear", pColorAlpha);
}
void AdvUIVendor::RenderCommonLft(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);

    ///--[Render]
    //--Comparison characters.
    RenderComparisonCharacters(mCompareCharacterOld, mCompareCharacterCur, mCompareSwapTimer, cxAdvCharSwapTicks, ACE_UI_INDEX_VENDOR);

    //--Swap arrows. Only renders if more than one comparison character exists.
    if(mShowSwapArrows)
    {
        //--Arrow offset.
        float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;

        //--Render.
        AdvImages.rOverlay_ArrowRgt->Draw(cArrowOscillate *  1.0f, 0.0f);
        AdvImages.rOverlay_ArrowLft->Draw(cArrowOscillate * -1.0f, 0.0f);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderCommonTop(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Render]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header Text.
    SetColor("Heading", cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(411.0f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mShopName);
    SetColor("Clear", cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderCommonPlatina(float pColorAlpha)
{
    ///--[Documentation]
    //--On several UI parts the platina display appears in the top right corner. This routine handles
    //  rendering that. The rest of the side's routine is usually a comparison pane which changes by mode.
    //--Platina.
    AdvImages.rFrame_Platina->Draw();

    //--Title.
    SetColor("Heading", pColorAlpha);
    AdvImages.rFont_Heading->DrawText(1060.0f, 48.0f, 0, 1.0f, "Platina: ");
    SetColor("Clear", pColorAlpha);

    //--Actual value:
    AdvImages.rFont_Heading->DrawTextArgs(1356.0f, 48.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", AdventureInventory::Fetch()->GetPlatina());
}

///======================================= Details Mode ===========================================
void AdvUIVendor::RenderItemDetails(int pTimer, int pTimerMax, AdventureItem *pItem)
{
    ///--[Documentation]
    //--Standard description handler. Descriptions appear at the bottom of the screen using a specific
    //  image and render an item's advanced description lines.
    //--It is legal for NULL to be passed as the item. This will render a blank box.
    if(pTimer < 1 || pTimerMax < 1) return;

    //--Percentage.
    float cScrollPercent = EasingFunction::QuadraticInOut(pTimer, pTimerMax);

    //--Offsets.
    float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cScrollPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cScrollPercent * 0.7f));

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    //--Frame.
    AdvImages.rFrame_Description->Draw();

    ///--[Blank Box Handler]
    //--If the item is NULL, render this and stop.
    if(!pItem)
    {
        RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, 200.0f, 76.0f, 76.0f, AdvImages.rOverlay_ExpandableHeader);
        glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
        return;
    }

    //--Expandable header and name. Note that there is an additional 48 pixels of space on the header
    //  within the dead zones.
    const char *rItemName = pItem->GetName();
    float cNameLen = AdvImages.rFont_Heading->GetTextWidth(rItemName);
    RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, cNameLen - 48.0f, 76.0f, 76.0f, AdvImages.rOverlay_ExpandableHeader);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 510.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rItemName);

    ///--[Description]
    //--Setup.
    float cXPosition =  50.0f;
    float cYPosition = 558.0f;
    float cYHeight   =  25.0f;

    //--Render advanced description lines.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = pItem->GetAdvancedDescription(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), 0, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///====================================== Index Rendering =========================================
void AdvUIVendor::RenderItemPack(ShopInventoryPack *pPack, int pSlot, bool pIsOdd, float pAlpha)
{
    ///--[Documentation]
    //--For buy/sell/buyback, renders the information of an item in a standard format. This shows its names,
    //  value, how many the player owns, and gems if any.
    if(!pPack || !pPack->mItem) return;

    //--X positions.
    float cLft    = 257.0f;
    float cRgt    = 946.0f;
    float cIconX  = 267.0f;
    float cNameX  = 289.0f;
    float cCostX  = 625.0f;
    float cOwnedX = 635.0f;
    float cGemX   = 763.0f;
    float cGemW   =  24.0f;

    //--Ownership.
    float cPortraitX = 657.0f;
    float cPortraitH =  26.0f;

    //--Compute Y position.
    float cSlotY = 187.0f;
    float cSlotH =  26.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos + 2.0f;
    float cGemY      = cYPos + 2.0f;
    float cPortraitY = cYPos + 3.0f;

    //--Backing.
    if(pIsOdd)
    {
        StarBitmap::DrawRectFill(cLft, cYPos, cRgt, cYPos + cSlotH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
    }

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Icon.
    StarBitmap *rIcon = pPack->mItem->GetIconImage();
    if(rIcon) rIcon->Draw(cIconX, cTxtY + 1.0);

    //--Name, no quantity.
    if(!pPack->mItem->IsStackable() || pPack->mQuantity < 2)
        AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pPack->mItem->GetName());
    else
        AdvImages.rFont_Mainline->DrawTextFixedArgs(cNameX, cTxtY, cxAdvMaxNameWid, 0, "%sx%i", pPack->mItem->GetName(), pPack->mQuantity);

    //--Cost. If zero, does not print.
    int tCost = pPack->mPriceOverride;
    if(tCost == -1) tCost = pPack->mItem->GetValue();
    if(tCost > 0)
    {
        AdvImages.rFont_Mainline->DrawTextArgs(cCostX, cTxtY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tCost);
    }

    //--How many the player owns. Refers to the stack size. Duplicates that are unique, like gems, don't show this.
    if(pPack->mOwnedDisplay > -1)
    {
        //--How many are owned. Must fit a specific area.
        AdvImages.rFont_Mainline->DrawTextFixedArgs(cOwnedX, cTxtY, 21.0f, 0, "%i", pPack->mOwnedDisplay);

        //--Who owns it.
        float tXPos = cPortraitX;
        for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
        {
            //--If the character doesn't own it, skip.
            if(pPack->mOwnedBy[i] == false) continue;

            //--Subroutine renders the face.
            RenderOwnerFace(tXPos, cPortraitY, AdvCombat::Fetch()->GetActiveMemberI(i));

            //--Add to the position.
            tXPos = tXPos + cPortraitH;
        }
    }

    //--Render the gem slots.
    for(int i = 0; i < pPack->mItem->GetGemSlots(); i ++)
    {
        //--If there's a gem in the slot, render it under the frame.
        AdventureItem *rGemInSlot = pPack->mItem->GetGemInSlot(i);
        if(rGemInSlot)
        {
            StarBitmap *rItemIcon = rGemInSlot->GetIconImage();
            if(rItemIcon) rItemIcon->Draw(cGemX + (cGemW * i), cGemY);
        }

        //--Frame.
        AdvImages.rOverlay_GemSlot->Draw(cGemX + (cGemW * i), cGemY);
    }
}
void AdvUIVendor::RenderItem(AdventureItem *pItem, int pSlot, bool pIsOdd, bool pIsSellPrice, float pAlpha)
{
    ///--[Documentation]
    //--Same as above, but takes the item itself instead of an item pack. This precludes rendering
    //  how many the player owns, as this is used for the selling interface and each item is sold individually.
    if(!pItem) return;

    //--X positions.
    float cLft    = 257.0f;
    float cRgt    = 946.0f;
    float cIconX  = 267.0f;
    float cNameX  = 289.0f;
    float cCostX  = 625.0f;
    float cGemX   = 763.0f;
    float cGemW   =  24.0f;

    //--Compute Y position.
    float cSlotY = 187.0f;
    float cSlotH =  26.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos + 2.0f;
    float cGemY      = cYPos + 2.0f;

    //--Backing.
    if(pIsOdd)
    {
        StarBitmap::DrawRectFill(cLft, cYPos, cRgt, cYPos + cSlotH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
    }

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Icon.
    StarBitmap *rIcon = pItem->GetIconImage();
    if(rIcon) rIcon->Draw(cIconX, cTxtY + 1.0);

    //--Name + Quantity.
    int tQuantity = pItem->GetStackSize();
    if(pItem->IsStackable() && tQuantity > 1)
    {
        AdvImages.rFont_Mainline->DrawTextFixedArgs(cNameX, cTxtY, cxAdvMaxNameWid, 0, "%sx%i", pItem->GetName(), tQuantity);
    }
    //--Don't render a quantity.
    else
    {
        AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pItem->GetName());
    }

    //--Cost. If zero, does not print.
    int tCost = pItem->GetValue();
    if(pIsSellPrice) tCost = tCost / 2;
    if(tCost > 0)
    {
        AdvImages.rFont_Mainline->DrawTextArgs(cCostX, cTxtY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tCost);
    }

    //--Render the gem slots.
    for(int i = 0; i < pItem->GetGemSlots(); i ++)
    {
        //--If there's a gem in the slot, render it under the frame.
        AdventureItem *rGemInSlot = pItem->GetGemInSlot(i);
        if(rGemInSlot)
        {
            StarBitmap *rItemIcon = rGemInSlot->GetIconImage();
            if(rItemIcon) rItemIcon->Draw(cGemX + (cGemW * i), cGemY);
        }

        //--Frame.
        AdvImages.rOverlay_GemSlot->Draw(cGemX + (cGemW * i), cGemY);
    }
}

///--[Gems]
void AdvUIVendor::RenderGem(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha)
{
    ///--[Documentation]
    //--Given a gem, renders its information. This is just an icon, owner icon if applicable, and name.
    if(!pItem) return;

    //--X positions.
    float cLft       = 257.0f;
    float cIconX     = 266.0f;
    float cPortraitX = 286.0f;
    float cNameX     = 314.0f;
    float cRgt       = 946.0f;

    //--Compute Y position.
    float cSlotY = 187.0f;
    float cSlotH =  26.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos + 2.0f;
    float cIconY     = cYPos + 2.0f;
    float cPortraitY = cYPos + 1.0f;

    //--Backing.
    if(pIsOdd)
    {
        StarBitmap::DrawRectFill(cLft, cYPos, cRgt, cYPos + cSlotH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
    }

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Icon.
    StarBitmap *rItemIcon = pItem->GetIconImage();
    if(rItemIcon)
    {
        rItemIcon->Draw(cIconX, cIconY);
    }

    //--Owner. If the string is NULL or "Null", nobody owns it.
    if(pOwner && strcasecmp(pOwner, "Null"))
    {
        RenderOwnerFace(cPortraitX, cPortraitY, AdvCombat::Fetch()->GetActiveMemberS(pOwner));
    }

    //--Name.
    AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pItem->GetName());
}
void AdvUIVendor::RenderGemMerge(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha)
{
    ///--[Documentation]
    //--Given a gem, renders its information. This version is used in the gem merge UI. It shows some additional
    //  information, like the gem's sub-components and tier.
    if(!pItem || !mMergeGem) return;

    //--X positions.
    float cLft       =  49.0f;
    float cIconX     =  58.0f;
    float cPortraitX =  78.0f;
    float cNameX     = 106.0f;
    float cRgt       = 954.0f;

    //--Compute Y position.
    float cSlotY = 187.0f;
    float cSlotH =  26.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos + 2.0f;
    float cIconY     = cYPos + 2.0f;
    float cPortraitY = cYPos + 1.0f;

    //--Colors.
    StarlightColor cGemDarken;
    cGemDarken.SetRGBAF(0.20f, 0.20f, 0.20f, pAlpha);
    StarlightColor cGemIncompatible;
    cGemIncompatible.SetRGBAF(1.0f, 0.0f, 0.0f, pAlpha);

    //--Backing.
    if(pIsOdd)
    {
        StarBitmap::DrawRectFill(cLft, cYPos, cRgt, cYPos + cSlotH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
    }

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Flag, stores whether the gems can be merged.
    bool tCanBeMerged = mMergeGem->CanBeMerged(pItem);

    //--Icon.
    StarBitmap *rItemIcon = pItem->GetIconImage();
    if(rItemIcon)
    {
        rItemIcon->Draw(cIconX, cIconY);
    }

    //--Owner. If the string is NULL or "Null", nobody owns it.
    if(pOwner && strcasecmp(pOwner, "Null"))
    {
        RenderOwnerFace(cPortraitX, cPortraitY, AdvCombat::Fetch()->GetActiveMemberS(pOwner));
    }

    //--Name.
    if(!tCanBeMerged) cGemIncompatible.SetAsMixer();
    AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pItem->GetName());
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Get color flag.
    int tTotalTier = -1;
    uint8_t tCurCol = 0x01;
    uint8_t tColorFlag = pItem->GetGemColors();

    //--Position constants.
    float cGemX = 668.0f;
    float cGemY = cTxtY + 0.0f;
    float cGemW =  41.0f;

    //--Loop.
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        //--If this color is present:
        if(tColorFlag & tCurCol)
        {
            tTotalTier ++;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }
        else
        {
            cGemDarken.SetAsMixer();
        }

        //--Position. Render at 1.5x scale.
        AdvImages.rGemIcons[i]->Draw(cGemX + (cGemW * i), cGemY);

        //--Next.
        tCurCol = tCurCol* 2;
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Gem Tier. Affects costs.
    float cTierX = 493.0f;
    if(tCanBeMerged)
    {
        AdvImages.rFont_Mainline->DrawTextArgs(cTierX, cTxtY, 0, 1.0f, "Tier %i", tTotalTier);
    }
    else
    {
        cGemIncompatible.SetAsMixer();
        AdvImages.rFont_Mainline->DrawText(cTierX, cTxtY, 0, 1.0f, "Incompatible");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    //--Platina cost to merge.
    tTotalTier = tTotalTier + mMergeGem->GetRank();
    if(tCanBeMerged)
    {
        //--Text.
        float cPlatinaR = 637.0f;
        int tMergeCost = AdventureInventory::ComputeGemUpgradePlatinaCost(tTotalTier);
        AdvImages.rFont_Mainline->DrawTextArgs(cPlatinaR, cTxtY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tMergeCost);

        //--Platina Symbol.
        StarBitmap *rPlatinaSymbol = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Platina");
        if(rPlatinaSymbol) rPlatinaSymbol->Draw(cPlatinaR + 2.0f, cTxtY + 3.0f);
    }
}

///--[Unlock Package]
void AdvUIVendor::RenderUnlockPack(ShopInventoryPack *pPack, int pSlot, bool pIsOdd, float pAlpha, float pScrollTimer)
{
    ///--[Documentation]
    //--For Unlock mode, similar to the buy layout but renders item requirements instead of price/gems/etc.
    //  Item requirements are rendered on the line below the item name and highlight if the item is
    //  in the inventory.
    if(!pPack || !pPack->mItem) return;

    ///--[Setup]
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Constants]
    //--Compute Y position.
    float cYPos = cxAdv_UnlockT + (cxAdv_UnlockH * pSlot);
    float cTxtY = cYPos + 2.0f;

    ///--[Standards]
    //--Stencil setup.
    glDisable(GL_STENCIL_TEST);

    //--Backing.
    if(pIsOdd)
    {
        StarBitmap::DrawRectFill(cxAdv_UnlBackL, cYPos, cxAdv_UnlBackR, cYPos + cxAdv_UnlockH, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
    }

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Icon.
    StarBitmap *rIcon = pPack->mItem->GetIconImage();
    if(rIcon) rIcon->Draw(cxAdv_UnlIconX, cTxtY + 1.0);

    //--Name.
    AdvImages.rFont_Mainline->DrawTextFixed(cxAdv_UnlNameX, cTxtY, cxAdvMaxNameWid, 0, pPack->mItem->GetName());

    ///--[Unlock Information]
    /*
    //--Setup.
    float tTxtPos = cNameX + cxAdvMaxNameWid + cNameSpacing;
    glEnable(GL_STENCIL_TEST);

    //--Scroll amount.
    tTxtPos = tTxtPos - (pPack->mScrollMax * pScrollTimer);

    //--Renders the unlocking data.
    bool tIsFirst = true;
    ShopUnlockPack *rPackage = (ShopUnlockPack *)pPack->mUnlockRequirements->PushIterator();
    while(rPackage)
    {
        //--Determine the color. Check the inventory to see if the player has the required amount.
        if(HasEnoughOfUnlockItem(rPackage, tIsFirst))
        {
            cActiveText.SetAsMixer();
        }
        else
        {
            cStandardText.SetAsMixer();
        }

        //--If this is not the final entry, put a comma on the string:
        if(pPack->mUnlockRequirements->GetTail() != rPackage)
        {
            //--Render the shorthand name.
            char *tBuffer = InitializeString("%s (x%i), ", rPackage->mItemShort, rPackage->mNumberNeeded);
            AdvImages.rFont_Mainline->DrawText(tTxtPos, cTxtY, 0, 1.0f, tBuffer);

            //--Advance the cursor.
            tTxtPos = tTxtPos + (AdvImages.rFont_Mainline->GetTextWidth(tBuffer));

            //--Clean.
            free(tBuffer);
        }
        //--Final entry.
        else
        {
            //--Render the shorthand name.
            char *tBuffer = InitializeString("%s (x%i)", rPackage->mItemShort, rPackage->mNumberNeeded);
            AdvImages.rFont_Mainline->DrawText(tTxtPos, cTxtY, 0, 1.0f, tBuffer);

            //--Clean.
            free(tBuffer);
        }

        //--Next.
        tIsFirst = false;
        rPackage = (ShopUnlockPack *)pPack->mUnlockRequirements->AutoIterate();
    }*/

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void AdvUIVendor::RenderUnlockMaterials(ShopInventoryPack *pItem, int pSlot, float pAlpha)
{
    ///--[Documentation]
    //--Renders the materials needed for an item to be unlocked. Typically only called on the highlighted
    //  item, shows materials the player has as green.
    if(!pItem || !pItem->mItem) return;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *trUnlockRequirements = pItem->mUnlockRequirements;

    ///--[Setup]
    //--Rendering positions.
    float tRenderX = cxAdv_IngredientL;
    float tRenderY = cxAdv_UnlockT + (cxAdv_UnlockH * pSlot) + cxAdv_IngredientT;

    //--Colors.
    StarlightColor cStandardText = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, pAlpha);
    StarlightColor cActiveText   = StarlightColor::MapRGBAF(0.0f, 1.0f, 0.0f, pAlpha);

    ///--[Rendering]
    bool tIsFirst = true;
    ShopUnlockPack *rPackage = (ShopUnlockPack *)trUnlockRequirements->PushIterator();
    while(rPackage)
    {
        //--We have enough, switch to green.
        int tInInventory = rInventory->GetCountOf(rPackage->mItemName);
        if(HasEnoughOfUnlockItem(rPackage, tIsFirst))
        {
            cActiveText.SetAsMixer();
        }
        else
        {
            cStandardText.SetAsMixer();
        }

        //--Render the name and counts.
        AdvImages.rFont_Mainline->DrawTextFixedArgs(tRenderX, tRenderY, cxAdv_IngredientW, 0, "%s (%i/%i)", rPackage->mItemDisplayName, tInInventory, rPackage->mNumberNeeded);

        //--If currently on the left position, move to the right.
        if(tRenderX == cxAdv_IngredientL)
        {
            tRenderX = cxAdv_IngredientM;
        }
        else if(tRenderX == cxAdv_IngredientM)
        {
            tRenderX = cxAdv_IngredientR;
        }
        //--If currently on the right position, move to the left, and down.
        else
        {
            tRenderX = cxAdv_IngredientL;
            tRenderY = tRenderY + cxAdv_IngredientH;
        }

        //--Next.
        tIsFirst = false;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        rPackage = (ShopUnlockPack *)trUnlockRequirements->AutoIterate();
    }
}

///--[Comparison Pane]
//--Used by Buy/Sell etc. Not used for gem merging.
void AdvUIVendor::RenderComparisonPane(AdventureItem *pComparisonItem, float pAlpha)
{
    ///--[Documentation]
    //--Renders the properties of whatever item is selected if it were to be equipped by the comparison
    //  character. If there is no comparison character, doesn't render anything except the header.
    //--If the item is a gem, doesn't render the header, letting the subroutine do that.
    if(mCompareCharacterCur == -1)
    {
        //--If the item selected is a gem, then this routine will render something.
        if(pComparisonItem && pComparisonItem->IsGem())
        {
            if(RenderComparisonPaneGem(pComparisonItem, pAlpha))
            {
                return;
            }
        }
    }

    ///--[Header]
    //--Title of Stats Area. Renders even if there is no item for properties.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(1158.0f, 137.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Properties");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Make sure the item exists. If it does not, stop here. We render the header in all cases.
    if(!pComparisonItem || mCompareCharacterCur == -1)
    {
        return;
    }

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);

    //--Setup variables.
    float cIconX         = 1020.0f;
    float cIconY         =  182.0f;
    float cIconH         =   25.0f;
    float cPropertyBaseX = cIconX         +  71.0f;
    float cPropertyBaseR = cPropertyBaseX +  62.0f;
    float cResistX       = cIconX         +  71.0f;
    float cResistR       = cResistX       +  62.0f;
    float cDamageX       = cIconX         + 200.0f;
    float cDamageR       = cDamageX       +  80.0f;
    float cPropertyBaseY = cIconY         -   2.0f;

    //--Alt-weapon. If the comparison slot is a weapon-alt, then the stats in the comparison window need to subtract
    //  the main weapon and add in the alt-weapon, as if it was the main weapon.
    EquipmentSlotPack *rPackage = rActiveEntity->GetEquipmentSlotPackageS(mCompareSlot);
    if(!rPackage)
    {
        return;
    }

    ///--[Iterate Across Properties]
    //--Render.
    for(int i = 0; i < ADVUIVENDOR_ICONS_TOTAL; i ++)
    {
        ///--[Basics]
        //--Icon.
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Statistic / Resistance.
        int tEntityValue = rActiveEntity->GetStatistic(mStatisticIndexes[i]);

        //--Damage Bonus. May not always be used.
        int tEntityBonusTags = rActiveEntity->GetTagCount(mBonusTagNames[i]);
        int tEntityMalusTags = rActiveEntity->GetTagCount(mMalusTagNames[i]);
        int tEntityDamageBonus = 100 + tEntityBonusTags - tEntityMalusTags;

        ///--[Alt Weapon Check]
        //--Check if this is an alt-weapon slot. If so, we need to modify the statistics.
        if(rPackage->mIsWeaponAlternate)
        {
            //--Get the weapon in the slot.
            int tWeaponSlot = rActiveEntity->GetSlotForWeaponDamage();
            EquipmentSlotPack *rWeaponPackage = rActiveEntity->GetEquipmentSlotPackageI(tWeaponSlot);

            //--If there's a weapon in the main slot:
            if(rWeaponPackage->mEquippedItem)
            {
                //--Statistic / Resistance
                tEntityValue = tEntityValue - rWeaponPackage->mEquippedItem->GetStatistic(mStatisticIndexes[i]);

                //--Damage Bonus
                int tItemBonusTags = rWeaponPackage->mEquippedItem->GetTagCount(mBonusTagNames[i]);
                int tItemMalusTags = rWeaponPackage->mEquippedItem->GetTagCount(mMalusTagNames[i]);
                tEntityDamageBonus = tEntityDamageBonus - tItemBonusTags + tItemMalusTags;
            }

            //--Add the value of the alternate weapon. This will give the player the stats they'd have if they swapped in battle.
            AdventureItem *rReplaceWeapon = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            if(rReplaceWeapon)
            {
                //--Statistic / Resistance
                tEntityValue = tEntityValue + rReplaceWeapon->GetStatistic(mStatisticIndexes[i]);

                //--Damage Bonus
                int tItemBonusTags = rReplaceWeapon->GetTagCount(mBonusTagNames[i]);
                int tItemMalusTags = rReplaceWeapon->GetTagCount(mMalusTagNames[i]);
                tEntityDamageBonus = tEntityDamageBonus + tItemBonusTags - tItemMalusTags;
            }
        }

        ///--[Stat Comparison - Base Property]
        //--This is a simple property with only one value to render.
        if(i < ADVUIVENDOR_RESISTANCE_BEGIN)
        {
            ///--[Normal]
            //--Setup.
            int tOldStatValue = 0;
            int tNewStatValue = 0;

            //--Stat value of the equipped item.
            AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            if(rCurrentItem) tOldStatValue = rCurrentItem->GetStatistic(mStatisticIndexes[i]);

            //--Stat value of the replacement.
            tNewStatValue = pComparisonItem->GetStatistic(mStatisticIndexes[i]);

            ///--[Render]
            //--Base value.
            AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue);

            //--If the value in this slot is different from the old value, then display that.
            if(tOldStatValue != tNewStatValue)
            {
                //--Value is lower:
                if(tNewStatValue < tOldStatValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                }
                //--Higher:
                else
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                }

                //--Render.
                AdvImages.rFont_Mainline->DrawTextArgs (cPropertyBaseX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue + (tNewStatValue - tOldStatValue));

                //--Clean.
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            }
        }
        ///--[Stat Comparison - Resistance and Damage]
        //--Resistance and Damage Bonus are side by side.
        else
        {
            ///--[Get Values]
            //--Equipped item values.
            AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            int tOldResistance = 0;
            int tOldBonusTags  = 0;
            int tOldMalusTags  = 0;
            if(rCurrentItem)
            {
                tOldResistance = rCurrentItem->GetStatistic(mStatisticIndexes[i]);
                tOldBonusTags  = rCurrentItem->GetTagCount(mBonusTagNames[i]);
                tOldMalusTags  = rCurrentItem->GetTagCount(mMalusTagNames[i]);
            }

            //--Replacement values.
            int tNewResistance = pComparisonItem->GetStatistic(mStatisticIndexes[i]);
            int tNewBonusTags  = pComparisonItem->GetTagCount(mBonusTagNames[i]);
            int tNewMalusTags  = pComparisonItem->GetTagCount(mMalusTagNames[i]);

            //--Compute Damage
            int tOldDamageValue = 100 + tOldBonusTags - tOldMalusTags;
            if(tOldDamageValue < 0) tOldDamageValue = 0;
            int tNewDamageValue = 100 + tNewBonusTags - tNewMalusTags;
            if(tNewDamageValue < 0) tNewDamageValue = 0;

            ///--[Resistance]
            //--If the resistance is Immune, render that and don't do a comparison.
            if(tEntityValue >= 1000)
            {
                AdvImages.rFont_Statistic->DrawText(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
            }
            else
            {
                //--Render the base value.
                AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue);

                //--If the value in this slot is different from the old value, then display that.
                if(tOldResistance != tNewResistance)
                {
                    //--Value is lower:
                    if(tNewResistance < tOldResistance)
                    {
                        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                    }
                    //--Higher:
                    else
                    {
                        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                    }

                    //--Render.
                    AdvImages.rFont_Mainline->DrawTextArgs (cResistX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                    AdvImages.rFont_Statistic->DrawTextArgs(cResistR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue + (tNewResistance - tOldResistance));

                    //--Clean.
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
                }
            }

            ///--[Damage Bonus]
            //--Base value.
            AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tEntityDamageBonus);

            //--If the value in this slot is different from the old value, then display that.
            if(tOldDamageValue != tNewDamageValue)
            {
                //--Value is lower:
                if(tNewDamageValue < tOldDamageValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                }
                //--Higher:
                else
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                }

                //--Render.
                AdvImages.rFont_Mainline->DrawTextArgs (cDamageX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistic->DrawTextArgs(cDamageR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tEntityDamageBonus + (tNewDamageValue - tOldDamageValue));

                //--Clean.
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            }
        }
    }

    ///--[Comparison Item]
    //--Show what item is being compared to.
    AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
    if(rCurrentItem)
    {
        //--Base.
        AdvImages.rFont_Statistic->DrawText(1009.0f, 642.0f, 0, 1.0f, "Comparing to: ");

        //--Icon.
        StarBitmap *rItemIcon = rCurrentItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(1020.0f, 665.0f);

        //--Name.
        AdvImages.rFont_Statistic->DrawText(1042.0f, 664.0f, 0, 1.0f, rCurrentItem->GetName());
    }
    else
    {
        //--Base.
        AdvImages.rFont_Statistic->DrawText(1009.0f, 642.0f, 0, 1.0f, "Comparing to: ");
        mColorGrey.SetAsMixerAlpha(pAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(1042.0f, 664.0f, 0, 1.0f, "[%s]", mCompareSlot);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }
}
bool AdvUIVendor::RenderComparisonPaneGem(AdventureItem *pGem, float pAlpha)
{
    ///--[Documentation]
    //--Renders the comparison pane item, except in gem format. Gems don't compare to anything. Does not render
    //  a header if the item does not exist.
    if(!pGem || !pGem->IsGem()) return false;

    ///--[Header]
    //--Title of Stats Area
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(1142.0f, 137.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gem Stats");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Setup]
    //--Setup variables.
    float cIconX         = 1020.0f;
    float cIconY         =  182.0f;
    float cIconH         =   25.0f;
    float cPropertyBaseX = cIconX         +  71.0f;
    float cResistX       = cIconX         +  71.0f;
    float cDamageX       = cIconX         + 200.0f;
    float cPropertyBaseY = cIconY         -   2.0f;

    ///--[Iterate Across Properties]
    //--Render.
    for(int i = 0; i < ADVUIVENDOR_ICONS_TOTAL; i ++)
    {
        ///--[Basics]
        //--Icon.
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Property. Can be either a statistic or a resistance.
        int tStatistic = pGem->GetStatistic(mStatisticIndexes[i]);

        //--Damage Bonus. May not always be used.
        int tGemBonusTags = pGem->GetTagCount(mBonusTagNames[i]);
        int tGemMalusTags = pGem->GetTagCount(mMalusTagNames[i]);
        int tGemDamageBonus = tGemBonusTags - tGemMalusTags;

        ///--[Stat Comparison - Base Property]
        //--This is a simple property with only one value to render.
        if(i < ADVUIVENDOR_RESISTANCE_BEGIN)
        {
            //--Do not display zeroes.
            //if(tStatistic == 0) continue;

            //--Stat Penalty:
            if(tStatistic < 0)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
            }
            //--Stat Bonus:
            else if(tStatistic > 0)
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
            }
            //--Zero.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0, pAlpha);
            }

            //--Render.
            AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i", tStatistic);
        }
        ///--[Stat Comparison - Resistance and Damage]
        //--Resistance and Damage Bonus are side by side.
        else
        {
            ///--[Resistance]
            //--Skip zeroes.
            //if(tStatistic != 0)
            {
                //--Stat Penalty:
                if(tStatistic < 0)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                }
                //--Stat Bonus:
                else if(tStatistic > 0)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                }
                //--Zero.
                else
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0, pAlpha);
                }

                //--Render.
                AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i", tStatistic);
            }

            ///--[Damage Bonus]
            //--Skip zeroes.
            //if(tGemDamageBonus != 0)
            {
                //--Stat Penalty:
                if(tGemDamageBonus < 0)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                }
                //--Stat Bonus:
                else if(tGemDamageBonus > 0)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                }
                //--Zero.
                else
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0, pAlpha);
                }

                //--Render.
                AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i%%", tGemDamageBonus);
            }
        }

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    //--Finish up. Returns true to indicate we handled rendering.
    return true;
}
