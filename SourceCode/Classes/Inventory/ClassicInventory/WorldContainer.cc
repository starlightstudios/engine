//--Base
#include "WorldContainer.h"

//--Classes
#include "Actor.h"
#include "ContextMenu.h"
#include "InventoryItem.h"
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"
#include "VisualLevel.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
WorldContainer::WorldContainer()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_WORLDCONTAINER;

    ///--[WorldContainer]
    //--System
    mSelfDestruct = false;
    mName = InitializeString("Unnamed Container");
    mDescription = InitializeString("Undescribed Container");

    //--Contents
    mTotalContents = 0;
    mContents = NULL;

    //--Traps
    mTrapItem = NULL;
    mExtraTrapScript = InitializeString("None");

    //--Owner
    rOwnerRoom = NULL;

    //--Visibility
    mLastSetVisible = 0;
    mIsVisibleTimer = 0;

    //--Public Variables
    mLastRenderSlot = -1;
}
WorldContainer::~WorldContainer()
{
    free(mName);
    free(mDescription);
    for(int i = 0; i < mTotalContents; i ++) delete mContents[i];
    free(mContents);
    delete mTrapItem;
    free(mExtraTrapScript);
}

///--[Private Statics]
char *WorldContainer::xContainerNameGeneratorPath = NULL;
char *WorldContainer::xNextContainerName = NULL;
char *WorldContainer::xNextContainerDescription = NULL;

///--[Public Statics]
//--Timer to prevent the playing of the container-opening sound effect too many times if multiple
//  containers get opened at once.
int WorldContainer::xOpenSoundTimer = -1;

bool WorldContainer::xTrapDespawn = false;
bool WorldContainer::xTrapSucceeded = false;

///===================================== Property Queries =========================================
const char *WorldContainer::GetName()
{
    return (const char *)mName;
}
const char *WorldContainer::GetDescription()
{
    return (const char *)mDescription;
}
const char *WorldContainer::GetExtraTrapScript()
{
    return (const char *)mExtraTrapScript;
}
bool WorldContainer::IsSelfDestructing()
{
    return mSelfDestruct;
}
int WorldContainer::GetVisibilityTimer()
{
    return mIsVisibleTimer;
}

///======================================= Manipulators ===========================================
void WorldContainer::SetDestruct(bool pFlag)
{
    mSelfDestruct = pFlag;
}
void WorldContainer::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mName, pName);
}
void WorldContainer::SetDescription(const char *pDescription)
{
    if(!pDescription) return;
    ResetString(mDescription, pDescription);
}
void WorldContainer::SetExtraTrap(const char *pTrapScript)
{
    if(!pTrapScript) return;
    ResetString(mExtraTrapScript, pTrapScript);
}
void WorldContainer::SetContentsSize(int pAmount)
{
    //--Deallocate.
    for(int i = 0; i < mTotalContents; i ++) delete mContents[i];
    free(mContents);
    mContents = NULL;
    mTotalContents = 0;
    if(pAmount < 1) return;

    //--Allocate, clear.
    mTotalContents = pAmount;
    SetMemoryData(__FILE__, __LINE__);
    mContents = (InventoryItem **)starmemoryalloc(sizeof(InventoryItem *) * mTotalContents);
    for(int i = 0; i < mTotalContents; i ++) mContents[i] = NULL;
}
void WorldContainer::SetContents(int pSlot, InventoryItem *pItem)
{
    //--Note: Implicitly takes ownership of the item. If the item is rejected, it is deleted!
    if(pSlot < 0 || pSlot >= mTotalContents) { delete pItem; return; }
    delete mContents[pSlot];
    mContents[pSlot] = pItem;
}
void WorldContainer::SetTrap(InventoryItem *pItem)
{
    //--Note: Pass NULL to untrap the item. Old trap is deleted in any case. Ownership for the
    //  passed item is taken by the container.
    delete mTrapItem;
    mTrapItem = pItem;
}
void WorldContainer::SetOwner(PandemoniumRoom *pOwner)
{
    rOwnerRoom = pOwner;
}
void WorldContainer::IncrementVisible()
{
    //--Increments the visible timer, and stores the tick it happened on.
    mLastSetVisible = Global::Shared()->gTicksElapsed;
    if(mIsVisibleTimer < VL_ENTITY_FADE_TICKS) mIsVisibleTimer ++;
}
void WorldContainer::DecrementVisible()
{
    //--Decrements the visible timer if (and only if) the mLastVisible is not equal to the current tick.
    //  This means all entities which do not increment automatically decrement.
    if(mLastSetVisible == (int)Global::Shared()->gTicksElapsed) return;

    //--All checks passed, entity is invisible. Decrement.
    if(mIsVisibleTimer > 0) mIsVisibleTimer --;
}

///======================================= Core Methods ===========================================
void WorldContainer::Open()
{
    //--Pop the container open, dumping its contents on the floor and despawning the container.
    mSelfDestruct = true;
    if(!rOwnerRoom) return;

    //--If trapped, run the trap script.
    if(mTrapItem)
    {
        //--If it has a trap script...
        const char *rTrapScript = mTrapItem->GetTrapScript();
        Actor *rLastActor = EntityManager::Fetch()->GetActingEntity();
        if(rTrapScript && rLastActor)
        {
            //--Run the script with the opener as the stack head.
            xTrapDespawn = false;
            LuaManager::Fetch()->PushExecPop(rLastActor, rTrapScript);

            //--If this flag is still true, the trap hit. Run the item on the target.
            if(xTrapSucceeded)
            {
                //--Run it.
                rLastActor->UseItemByName(mTrapItem->GetName());

                //--If the trap has auto-despawn on, it will disappear.
                if(mTrapItem->AutoDespawns() || xTrapDespawn)
                {

                }
                //--Register it to the owner.
                else
                {
                    rLastActor->RegisterItem(mTrapItem);
                }
            }
            //--Failure. Register.
            else
            {
                //--If the trap has auto-despawn on, it will disappear.
                if(mTrapItem->AutoDespawns() || xTrapDespawn)
                {
                }
                //--Register it to the room.
                else
                {
                    rOwnerRoom->RegisterItem(mTrapItem);
                }
            }
        }
        //--Error case.
        else
        {
            rOwnerRoom->RegisterItem(mTrapItem);
        }
    }

    //Note: mTrapItem may have been deallocated at this point!

    //--Register the inventory to the owner. If it doesn't exist, we stop before this point.
    for(int i = 0; i < mTotalContents; i ++)
    {
        rOwnerRoom->RegisterItem(mContents[i]);
    }

    //--If the extra-trap script exists, run that.
    if(mExtraTrapScript && strcasecmp(mExtraTrapScript, "None"))
    {
        Actor *rLastActor = EntityManager::Fetch()->GetActingEntity();
        LuaManager::Fetch()->PushExecPop(rLastActor, mExtraTrapScript);
    }

    //--Clean up.
    mTotalContents = 0;
    free(mContents);
    mContents = NULL;
    mTrapItem = NULL;
}
char *WorldContainer::AssembleOpenString(const char *pActorName, bool pIsFirstPerson)
{
    ///--[Documentation]
    //--Builds and returns a string detailing what opening the container does, from the perspective
    //  of the player (who may not be opening it).
    //--The string is heap-allocated and must be free'd when you are done with it.
    char *nString = InitializeString("An error occurred assembling this string.");
    if(!pActorName) return nString;

    //--Buffer.
    char tBuffer[STD_MAX_LETTERS];

    //--If it's first-person...
    if(pIsFirstPerson)
    {
        sprintf(tBuffer, "You open the %s, revealing ", mName);
    }
    //--Third-person.
    else
    {
        sprintf(tBuffer, "%s opens the %s, revealing ", pActorName, mName);
    }

    //--If there are no items!
    if(mTotalContents == 0)
    {
        strcat(tBuffer, "nothing at all!");
    }
    //--If there's one item...
    else if(mTotalContents == 1)
    {
        char tAppendBuffer[STD_MAX_LETTERS];
        sprintf(tAppendBuffer, "a %s.", mContents[0]->GetName());
        strcat(tBuffer, tAppendBuffer);
    }
    //--If there are multiple items...
    else
    {
        //--Up to the last item, use the comma series.
        char tAppendBuffer[STD_MAX_LETTERS];
        for(int i = 0; i < mTotalContents - 1; i ++)
        {
            sprintf(tAppendBuffer, "a %s, ", mContents[i]->GetName());
            strcat(tBuffer, tAppendBuffer);
        }

        //--For the last item, finish the series off.
        sprintf(tAppendBuffer, "and a %s.", mContents[mTotalContents-1]->GetName());
        strcat(tBuffer, tAppendBuffer);
    }

    //--Pass it back.
    ResetString(nString, tBuffer);
    return nString;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
void WorldContainer::PlayOpenSound()
{
    //--Plays the open container sound effect, or fails to play it if one is already playing.
    if((int)Global::Shared()->gTicksElapsed < xOpenSoundTimer) return;

    //--Move the timer up and play the sound.
    xOpenSoundTimer = Global::Shared()->gTicksElapsed + 30;
    AudioManager::Fetch()->PlaySound("World|OpenChest");
}
WorldContainer *WorldContainer::SpawnContainerIn(const char *pRoomName, InventoryItem *pItem)
{
    //--Static method to spawn and return a new container in the provided room. If the room or contents
    //  are invalid, a container is returned anyway, but it will be flagged for deletion.
    WorldContainer *nContainer = new WorldContainer();
    nContainer->SetContentsSize(1);
    nContainer->SetContents(0, pItem);

    //--Generate a name for the container. This is done by a lua script.
    if(xContainerNameGeneratorPath)
    {
        //--Run.
        LuaManager::Fetch()->ExecuteLuaFile(xContainerNameGeneratorPath);

        //--If the string got set, use it. Otherwise, Black Box!
        if(xNextContainerName && xNextContainerDescription)
        {
            //--Set.
            nContainer->SetName(xNextContainerName);
            nContainer->SetDescription(xNextContainerDescription);

            //--Clean.
            ResetString(xNextContainerName, NULL);
            ResetString(xNextContainerDescription, NULL);
        }
        //--Failure.
        else
        {
            nContainer->SetName("Black Box");
        }
    }
    //--Failure, it's a black box!
    else
    {
        nContainer->SetName("Black Box");
    }

    //--Get the room for registration.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel) { nContainer->SetDestruct(true); return nContainer; }
    PandemoniumRoom *rRoom = rActiveLevel->GetRoom(pRoomName);
    if(!rRoom) { nContainer->SetDestruct(true); return nContainer; }

    //--Everything worked, register and return. The caller no longer has ownership!
    rRoom->RegisterContainer(nContainer);
    return nContainer;
}
void WorldContainer::SetNameGeneratorPath(const char *pPath)
{
    ResetString(xContainerNameGeneratorPath, pPath);
}
void WorldContainer::SetNextName(const char *pName)
{
    ResetString(xNextContainerName, pName);
}
void WorldContainer::SetNextDescription(const char *pName)
{
    ResetString(xNextContainerDescription, pName);
}

///======================================== Lua Hooking ===========================================
void WorldContainer::HookToLuaState(lua_State *pLuaState)
{
    /* Container_SetNameGeneratorPath(sPath)
       Sets which path containers use to generate names. */
    lua_register(pLuaState, "Container_SetNameGeneratorPath", &Hook_Container_SetNameGeneratorPath);

    /* Container_SetNextName(sName)
       Sets the name for the currently naming container. */
    lua_register(pLuaState, "Container_SetNextName", &Hook_Container_SetNextName);

    /* Container_SetNextDescription(sName)
       Sets the description for the currently naming container. */
    lua_register(pLuaState, "Container_SetNextDescription", &Hook_Container_SetNextDescription);

    /* Container_InformTrapDespawn()
       Tells the container to despawn the item involved in the trap, regardless of whether or not
       the trap failed. */
    lua_register(pLuaState, "Container_InformTrapDespawn", &Hook_Container_InformTrapDespawn);

    /* Container_InformTrapFailed()
       Called when a trap fires and runs a script. If the trap fails, it will drop the item on the
       floor at the location. It is assumed to succeed. */
    lua_register(pLuaState, "Container_InformTrapFailed", &Hook_Container_InformTrapFailed);

    /* Container_GetProperty("Name") (1 string)
       Container_GetProperty("Extra Trap Script") (1 string)
       Returns the requested property in the active WorldContainer. */
    lua_register(pLuaState, "Container_GetProperty", &Hook_Container_GetProperty);

    /* Container_SetProperty("Flag Destruct")
       Container_SetProperty("Extra Trap Script", sPath)
       Sets the requested property in the active WorldContainer. */
    lua_register(pLuaState, "Container_SetProperty", &Hook_Container_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Container_SetNameGeneratorPath(lua_State *L)
{
    //Container_SetNameGeneratorPath(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Container_SetNameGeneratorPath");

    //--Set.
    WorldContainer::SetNameGeneratorPath(lua_tostring(L, 1));

    return 0;
}
int Hook_Container_SetNextName(lua_State *L)
{
    //Container_SetNextName(sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Container_SetNextName");

    //--Set.
    WorldContainer::SetNextName(lua_tostring(L, 1));

    return 0;
}
int Hook_Container_SetNextDescription(lua_State *L)
{
    //Container_SetNextDescription(sDescription)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Container_SetNextDescription");

    //--Set.
    WorldContainer::SetNextDescription(lua_tostring(L, 1));

    return 0;
}
int Hook_Container_InformTrapDespawn(lua_State *L)
{
    //Container_InformTrapDespawn()
    WorldContainer::xTrapDespawn = true;
    return 0;
}
int Hook_Container_InformTrapFailed(lua_State *L)
{
    //Container_InformTrapFailed()
    WorldContainer::xTrapSucceeded = false;
    return 0;
}
int Hook_Container_GetProperty(lua_State *L)
{
    //Container_GetProperty("Name") (1 string)
    //Container_GetProperty("Extra Trap Script") (1 string)
    int tArgs = lua_gettop(L);

    //--Get the class.
    WorldContainer *rContainer = (WorldContainer *)DataLibrary::Fetch()->rActiveObject;
    if(!rContainer || rContainer->GetType() != POINTER_TYPE_WORLDCONTAINER) return LuaTypeError("Container_GetProperty", L);

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Item's name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushstring(L, rContainer->GetName());
    }
    //--Extra trap, used in Corrupter Mode.
    else if(!strcasecmp(rSwitchType, "Extra Trap Script") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushstring(L, rContainer->GetExtraTrapScript());
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("Container_GetProperty", rSwitchType);
    }

    return tReturns;
}
int Hook_Container_SetProperty(lua_State *L)
{
    //Container_SetProperty("Flag Destruct")
    //Container_SetProperty("Open")
    //Container_SetProperty("Extra Trap Script", sPath)
    int tArgs = lua_gettop(L);
    const char *rSwitchType = lua_tostring(L, 1);

    //--Get the class.
    WorldContainer *rContainer = (WorldContainer *)DataLibrary::Fetch()->rActiveObject;
    if(!rContainer || rContainer->GetType() != POINTER_TYPE_WORLDCONTAINER) return LuaTypeError("Container_SetProperty", L);

    //--Flags the item for self-destruction. Only used by items that haven't been picked up.
    if(!strcasecmp(rSwitchType, "Flag Destruct") && tArgs == 1)
    {
        rContainer->SetDestruct(true);
    }
    //--Open.
    else if(!strcasecmp(rSwitchType, "Open") && tArgs == 1)
    {
        ContextMenu::OpenContainer(rContainer);
    }
    //--Extra trap, used in Corrupter Mode.
    else if(!strcasecmp(rSwitchType, "Extra Trap Script") && tArgs == 2)
    {
        rContainer->SetExtraTrap(lua_tostring(L, 2));
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("Container_SetProperty", rSwitchType);
    }

    return 0;
}
