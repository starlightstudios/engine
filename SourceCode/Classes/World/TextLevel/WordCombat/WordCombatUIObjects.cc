//--Base
#include "WordCombat.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"

///--[Note]
//--Contains both functions used by the PlayerUIObject structure, and functions related to shields/hearts in the class.

///======================================== PlayerShield ==========================================
int PlayerUIObject::ComputeValue()
{
    int tShieldValue = 0;
    if(mElementalType & TL_ELEMENT_FIRE) tShieldValue ++;
    if(mElementalType & TL_ELEMENT_WATER) tShieldValue ++;
    if(mElementalType & TL_ELEMENT_WIND) tShieldValue ++;
    if(mElementalType & TL_ELEMENT_EARTH) tShieldValue ++;
    if(mElementalType & TL_ELEMENT_LIFE) tShieldValue ++;
    if(mElementalType & TL_ELEMENT_DEATH) tShieldValue ++;
    return tShieldValue;
}
void PlayerUIObject::MoveToInstant(float pX, float pY)
{
    mXPos = pX;
    mYPos = pY;
    mMoveTicks = mMoveTicksMax;
}
void PlayerUIObject::MoveToSlide(float pX, float pY)
{
    //--Flags.
    mStartX = mXPos;
    mStartY = mYPos;
    mTargetX = pX;
    mTargetY = pY;

    //--Compute time needed.
    float cDistance = GetPlanarDistance(mStartX, mStartY, mTargetX, mTargetY);
    float cTicksNeeded = ceil(cDistance / SHIELD_MOVESPEED) + 5.0f;
    mMoveTicks = 0;
    mMoveTicksMax = (int)cTicksNeeded;
    if(mMoveTicksMax < 1) mMoveTicksMax = 1;
}
void PlayerUIObject::MoveGravity()
{
    //--Gives the shield a random X and Y impulse.
    mIsGravityMovement = true;
    mXSpeed = ((GetRandomPercent() - 0.50f) * 2.0f) * 10.0f;
    mYSpeed = ((GetRandomPercent() * 4.00f) + 3.0f) * -1.0f;
    mMoveTicks = mMoveTicksMax;
}
void PlayerUIObject::Update()
{
    //--Timer always runs.
    mElementalTimer ++;

    //--Moving? Recompute positions.
    if(mMoveTicks < mMoveTicksMax)
    {
        //--Compute and move.
        float cAngle = GetAngleBetween(mStartX, mStartY, mTargetX, mTargetY);
        float cDistance = GetPlanarDistance(mStartX, mStartY, mTargetX, mTargetY);
        float cPercent = EasingFunction::QuadraticInOut(mMoveTicks, mMoveTicksMax);
        float cNewX = mStartX + cosf(cAngle) * cDistance * cPercent;
        float cNewY = mStartY + sinf(cAngle) * cDistance * cPercent;
        mXPos = cNewX;
        mYPos = cNewY;

        //--Run timer.
        mMoveTicks ++;

        //--Finish.
        if(mMoveTicks >= mMoveTicksMax)
        {
            mXPos = mTargetX;
            mYPos = mTargetY;
        }
    }
    //--Gravity.
    else if(mIsGravityMovement)
    {
        //--Move.
        mXPos = mXPos + mXSpeed;
        mYPos = mYPos + mYSpeed;

        //--Gravity.
        float cGravity = 0.625f;
        mYSpeed = mYSpeed + cGravity;
    }
}

///========================================= Functions ============================================
void WordCombat::AttackPlayer(CombatEnemyAttackPack *pAttackingPack)
{
    //--Handles the player taking damage. Triggers screen-shake and whatnot.
    if(!pAttackingPack) return;

    //--Setup.
    int tAttackPower = pAttackingPack->mAttackPower;

    //--Check if the player has at least one "Life" shield. Life shields absorb all damage by themselves.
    PlayerUIObject *rShield = (PlayerUIObject *)mPlayerShieldList->SetToHeadAndReturn();
    while(rShield)
    {
        //--Check for the element flag.
        if(rShield->mElementalType & TL_ELEMENT_LIFE)
        {
            //--Shield moves to graveyard.
            mPlayerShieldList->LiberateRandomPointerEntry();
            mDiscardedUIObjects->AddElement("X", rShield, &FreeThis);

            //--Order it to move offscreen.
            rShield->MoveGravity();

            //--Special SFX.
            AudioManager::Fetch()->PlaySound("Combat|Impact_Debuff");

            //--Stop iterating.
            return;
        }

        rShield = (PlayerUIObject *)mPlayerShieldList->IncrementAndGetRandomPointerEntry();
    }

    //--If the attack is non-elemental, it takes off shields equal to the number of damage it does.
    //  If there are no shields left, it takes HP off.
    if(pAttackingPack->mElementFlags == TL_ELEMENT_NONE)
    {
        //--If there aren't enough shields to totally block the hit, zero off the shields.
        if(tAttackPower >= mPlayerShieldList->GetListSize())
        {
            //--Attack power decreases.
            tAttackPower -= mPlayerShieldList->GetListSize();

            //--Remove all the shields.
            mPlayerShieldList->SetRandomPointerToHead();
            PlayerUIObject *rShield = (PlayerUIObject *)mPlayerShieldList->LiberateRandomPointerEntry();
            while(rShield)
            {
                //--Move to graveyard.
                mDiscardedUIObjects->AddElement("X", rShield, &FreeThis);

                //--Order it to move offscreen.
                rShield->MoveGravity();

                //--Next.
                mPlayerShieldList->SetRandomPointerToHead();
                rShield = (PlayerUIObject *)mPlayerShieldList->LiberateRandomPointerEntry();
            }
        }
        //--Otherwise, start removing shields.
        else
        {
            //--Run the loop.
            while(tAttackPower > 0)
            {
                //--Setup.
                int tCurrentWorstShield = -1;
                int tCurrentWorstShieldScore = 1000;

                //--Iterate.
                int i = 0;
                PlayerUIObject *rShield = (PlayerUIObject *)mPlayerShieldList->PushIterator();
                while(rShield)
                {
                    //--Compute shield value.
                    int tShieldValue = rShield->ComputeValue();

                    //--If this is the worst shield thus far, mark it.
                    if(tShieldValue < tCurrentWorstShieldScore)
                    {
                        tCurrentWorstShield = i;
                        tCurrentWorstShieldScore = tShieldValue;
                    }

                    //--Next.
                    i ++;
                    rShield = (PlayerUIObject *)mPlayerShieldList->AutoIterate();
                }

                //--The worst shield has been found. Send it to the graveyard.
                PlayerUIObject *rPtr = (PlayerUIObject *)mPlayerShieldList->GetElementBySlot(tCurrentWorstShield);
                mPlayerShieldList->SetRandomPointerToThis(rPtr);
                mPlayerShieldList->LiberateRandomPointerEntry();
                mDiscardedUIObjects->AddElement("X", rPtr, &FreeThis);

                //--Discarded shield is ordered to move offscreen.
                rPtr->MoveGravity();

                //--Decrement attack power.
                tAttackPower --;
            }
        }
    }

    //--Any attack power remaining hits the hearts.
    bool tIsHalfHeart = (mPlayerHP % 2 == 1);
    mPlayerHP -= tAttackPower;
    mDamageScreenScatterTimer = WC_SCATTER_LENGTH;
    mDamageScreenScatterX = (GetRandomPercent() - 0.50f) * WC_SCATTER_SEVERITY;
    mDamageScreenScatterY = (GetRandomPercent() - 0.50f) * WC_SCATTER_SEVERITY;

    //--SFX. When hitting HP:
    if(tAttackPower > 0)
    {
        AudioManager::Fetch()->PlaySound("Combat|Impact_Strike_Crit");
    }
    //--When hitting shields.
    else
    {
        AudioManager::Fetch()->PlaySound("Combat|Impact_Debuff");
    }

    //--Subtract hearts. For amusement, the first and last hearts are what get removed, alternating.
    bool tIsLast = true;
    for(int i = 0; i < tAttackPower; i ++)
    {
        //--If the heart is a half-heart, remove it.
        if(tIsHalfHeart)
        {
            tIsHalfHeart = false;
        }
        //--Not a half-heart, so skip removal.
        else
        {
            tIsHalfHeart = true;
            continue;
        }

        //--Setup.
        PlayerUIObject *rPtr = NULL;

        //--Last heart.
        if(tIsLast)
        {
            tIsLast = false;
            rPtr = (PlayerUIObject *)mPlayerHeartList->GetTail();
        }
        //--First heart.
        else
        {
            tIsLast = true;
            rPtr = (PlayerUIObject *)mPlayerHeartList->GetHead();
        }

        //--Set.
        if(rPtr)
        {
            mPlayerHeartList->SetRandomPointerToThis(rPtr);
            mPlayerHeartList->LiberateRandomPointerEntry();
            mDiscardedUIObjects->AddElement("X", rPtr, &FreeThis);
            rPtr->MoveGravity();
        }
    }
}
void WordCombat::GainShield(uint8_t pElementalFlags)
{
    //--Gives the player a defensive shield with the matching elements. If the player is already at the
    //  shield cap, then "better" shields stay while "worse" ones get tossed out.

    //--Create a new shield.
    SetMemoryData(__FILE__, __LINE__);
    PlayerUIObject *nShield = (PlayerUIObject *)starmemoryalloc(sizeof(PlayerUIObject));
    nShield->Initialize();
    nShield->mElementalType = pElementalFlags;
    nShield->rImage = Images.Data.rBarShield;
    if(mLastObjectWasTail)
    {
        mLastObjectWasTail = false;
        mPlayerShieldList->AddElementAsHead("X", nShield, &FreeThis);
    }
    else
    {
        mLastObjectWasTail = true;
        mPlayerShieldList->AddElementAsTail("X", nShield, &FreeThis);
    }

    //--Newly created shields spawn offscreen.
    nShield->MoveToInstant(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 200.0f);

    //--Now run this algorithm to trim the shield list. The worst shields are picked and discarded until
    //  the max number of shields remains.
    TrimShields();
    PositionShields();
}
void WordCombat::GainHearts(int pAmount, bool pHandleMoving)
{
    //--Spawns hearts as needed when the player's HP changes. Can spawn more than one at a time.
    rLastHeart = NULL;
    if(pAmount < 1) return;

    //--Loop.
    for(int i = 0; i < pAmount; i ++)
    {
        //--Create and register.
        SetMemoryData(__FILE__, __LINE__);
        PlayerUIObject *nHeart = (PlayerUIObject *)starmemoryalloc(sizeof(PlayerUIObject));
        nHeart->Initialize();
        nHeart->rImage = Images.Data.rBarHeart;
        if(mLastObjectWasTail)
        {
            mLastObjectWasTail = false;
            mPlayerHeartList->AddElementAsHead("X", nHeart, &FreeThis);
        }
        else
        {
            mLastObjectWasTail = true;
            mPlayerHeartList->AddElementAsTail("X", nHeart, &FreeThis);
        }

        //--Newly created hearts spawn offscreen.
        if(pHandleMoving) nHeart->MoveToInstant(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y + 400.0f);

        //--Store.
        rLastHeart = nHeart;
    }

    //--Reorganize the hearts as necessary.
    if(pHandleMoving) PositionHearts();
}
void WordCombat::TrimShields()
{
    //--Selects the lowest-quality shields and removes them, placing them in the shield graveyard.
    //  Remember to liberate these shields instead of removing them, otherwise they will be deleted.
    while(mPlayerShieldList->GetListSize() > WC_SHIELD_MAX)
    {
        //--Setup.
        int tCurrentWorstShield = -1;
        int tCurrentWorstShieldScore = 1000;

        //--Iterate.
        int i = 0;
        PlayerUIObject *rShield = (PlayerUIObject *)mPlayerShieldList->PushIterator();
        while(rShield)
        {
            //--Compute shield value.
            int tShieldValue = rShield->ComputeValue();

            //--If this is the worst shield thus far, mark it.
            if(tShieldValue < tCurrentWorstShieldScore)
            {
                tCurrentWorstShield = i;
                tCurrentWorstShieldScore = tShieldValue;
            }

            //--Next.
            i ++;
            rShield = (PlayerUIObject *)mPlayerShieldList->AutoIterate();
        }

        //--Once the worst shield is picked out, liberate it and send it to the graveyard.
        PlayerUIObject *rPtr = (PlayerUIObject *)mPlayerShieldList->GetElementBySlot(tCurrentWorstShield);
        mPlayerShieldList->SetRandomPointerToThis(rPtr);
        mPlayerShieldList->LiberateRandomPointerEntry();
        mDiscardedUIObjects->AddElement("X", rPtr, &FreeThis);

        //--Discarded shield is ordered to move offscreen.
        rPtr->MoveGravity();
    }
}
void WordCombat::PositionShields()
{
    //--Orders all shields to move to their ideal positions according to their lineup. Call this
    //  whenever a shield gets added or removed.
    if(!Images.mIsReady) return;

    //--Variables.
    int i = 0;
    float cOffsetPerIcon = 30.0f;
    float cShieldWid = Images.Data.rBarShield->GetWidth();
    float cShieldHei = Images.Data.rBarShield->GetHeight();
    float cStartX = (VIRTUAL_CANVAS_X - (cOffsetPerIcon * mPlayerShieldList->GetListSize()))* 0.50f;

    //--Iterate and set.
    PlayerUIObject *rShield = (PlayerUIObject *)mPlayerShieldList->PushIterator();
    while(rShield)
    {
        //--Position.
        float tXPos = cStartX + (cOffsetPerIcon * i) + (cShieldWid * 0.50f);
        float tYPos = 665.0f + (cShieldHei * 0.50f);

        //--Check distance. If it's less than the move speed, just go there instantly.
        float cDistance = GetPlanarDistance(rShield->mXPos, rShield->mYPos, tXPos, tYPos);
        if(cDistance < SHIELD_MOVESPEED)
        {
            rShield->MoveToInstant(tXPos, tYPos);
        }
        //--Slide there over a few ticks.
        else
        {
            rShield->MoveToSlide(tXPos, tYPos);
        }

        //--Next.
        i ++;
        rShield = (PlayerUIObject *)mPlayerShieldList->AutoIterate();
    }
}
void WordCombat::PositionHearts()
{
    //--Follows a similar logic to the shield positioner, with a different spacing.
    if(!Images.mIsReady) return;

    //--Variables.
    int i = 0;
    float cOffsetPerIcon = 40.0f;
    float cHeartWid = Images.Data.rBarHeart->GetWidth();
    float cHeartHei = Images.Data.rBarHeart->GetHeight();
    float cStartX = (VIRTUAL_CANVAS_X - (cOffsetPerIcon * mPlayerHeartList->GetListSize()))* 0.50f;

    //--Iterate and set.
    PlayerUIObject *rHeart = (PlayerUIObject *)mPlayerHeartList->PushIterator();
    while(rHeart)
    {
        //--Position.
        float tXPos = cStartX + (cOffsetPerIcon * i) + (cHeartWid * 0.50f);
        float tYPos = 710.0f + (cHeartHei * 0.50f);

        //--Check distance. If it's less than the move speed, just go there instantly.
        float cDistance = GetPlanarDistance(rHeart->mXPos, rHeart->mYPos, tXPos, tYPos);
        if(cDistance < SHIELD_MOVESPEED)
        {
            rHeart->MoveToInstant(tXPos, tYPos);
        }
        //--Slide there over a few ticks.
        else
        {
            rHeart->MoveToSlide(tXPos, tYPos);
        }

        //--Next.
        i ++;
        rHeart = (PlayerUIObject *)mPlayerHeartList->AutoIterate();
    }
}
void WordCombat::UpdateUIObjects()
{
    //--Shields move their elemental indicators and positions.
    PlayerUIObject *rShield = (PlayerUIObject *)mPlayerShieldList->PushIterator();
    while(rShield)
    {
        rShield->Update();
        rShield = (PlayerUIObject *)mPlayerShieldList->AutoIterate();
    }

    //--Hearts update. Usually just moving to position.
    PlayerUIObject *rHeart = (PlayerUIObject *)mPlayerHeartList->PushIterator();
    while(rHeart)
    {
        rHeart->Update();
        rHeart = (PlayerUIObject *)mPlayerHeartList->AutoIterate();
    }

    //--Discarded shields. They finish their last move and then self-terminate.
    PlayerUIObject *rDiscardedObject = (PlayerUIObject *)mDiscardedUIObjects->SetToHeadAndReturn();
    while(rDiscardedObject)
    {
        //--Update.
        rDiscardedObject->Update();

        //--Gravity case. Shield is offscreen.
        if(rDiscardedObject->mIsGravityMovement)
        {
            //--Ending case:
            if(rDiscardedObject->mYPos >= VIRTUAL_CANVAS_Y + 200.0f)
            {
                mDiscardedUIObjects->RemoveRandomPointerEntry();
            }
        }
        //--If the shield stops moving and is not under gravity, remove it.
        else if(rDiscardedObject->mMoveTicks >= rDiscardedObject->mMoveTicksMax)
        {
            mDiscardedUIObjects->RemoveRandomPointerEntry();
        }

        //--Next.
        rDiscardedObject = (PlayerUIObject *)mDiscardedUIObjects->IncrementAndGetRandomPointerEntry();
    }
}
void WordCombat::RenderUIObjects(float pAlpha)
{
    //--Renders the shields, both discarded and normal. Also renders the elemental indicators.
    float cXHf = Images.Data.rBarShield->GetWidth() * 0.50f;
    float cYHf = Images.Data.rBarShield->GetHeight() * 0.50f;

    //--Render the UI Object graveyard. These are shields/hearts that are falling offscreen to be removed.
    PlayerUIObject *rUIObject = (PlayerUIObject *)mDiscardedUIObjects->PushIterator();
    while(rUIObject)
    {
        //--Position.
        if(!rUIObject->rImage)
        {
            rUIObject = (PlayerUIObject *)mDiscardedUIObjects->AutoIterate();
            continue;
        }

        //--Sizes.
        float tXPos = rUIObject->mXPos - (rUIObject->rImage->GetWidth()  * 0.50f);
        float tYPos = rUIObject->mYPos - (rUIObject->rImage->GetHeight() * 0.50f);

        //--Render.
        rUIObject->rImage->Draw(tXPos, tYPos);

        //--Next.
        rUIObject = (PlayerUIObject *)mDiscardedUIObjects->AutoIterate();
    }

    //--Render the player's shield indicators.
    PlayerUIObject *rShield = (PlayerUIObject *)mPlayerShieldList->PushIterator();
    while(rShield)
    {
        //--Position.
        float tXPos = rShield->mXPos - cXHf;
        float tYPos = rShield->mYPos - cYHf;

        //--Render.
        Images.Data.rBarShield->Draw(tXPos, tYPos);

        //--Next.
        rShield = (PlayerUIObject *)mPlayerShieldList->AutoIterate();
    }

    //--Now render the orbiting elemental indicators. These render above the shields.
    rShield = (PlayerUIObject *)mPlayerShieldList->PushIterator();
    while(rShield)
    {
        //--Position.
        float tXPos = rShield->mXPos - cXHf;
        float tYPos = rShield->mYPos - cYHf;

        //--Properties.
        float cOrbitRadius = 20.0f;
        float cHfW = (Images.Data.rBarShield->GetWidth()  - Images.Data.rBarShieldOrbit->GetWidth())  * 0.50f;
        float cHfH = (Images.Data.rBarShield->GetHeight() - Images.Data.rBarShieldOrbit->GetHeight()) * 0.50f;

        //--Render elemental properties. These go over the shield.
        int c = 1;
        float cAngleOff = (15.0f * TORADIAN) + (2.0f * TORADIAN * rShield->mElementalTimer);
        for(int p = 0x01; p < 0x20; p = p * 2)
        {
            if(rShield->mElementalType & p)
            {
                //--Switch color.
                mElementalColors[c].SetAsMixer();

                //--Position.
                float tOrbitXPos = (tXPos + cHfW) + (cosf(cAngleOff) * cOrbitRadius);
                float tOrbitYPos = (tYPos + cHfH) + (sinf(cAngleOff) * cOrbitRadius);

                //--Render.
                Images.Data.rBarShieldOrbit->Draw(tOrbitXPos, tOrbitYPos);

                //--Move the angle up.
                cAngleOff = cAngleOff + (15.0f * TORADIAN);
            }

            //--Next.
            c ++;
        }

        //--Next.
        rShield = (PlayerUIObject *)mPlayerShieldList->AutoIterate();
    }

    //--Reset colors.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Render hearts. Discarded hearts are already handled.
    void *rTail = mPlayerHeartList->GetTail();
    PlayerUIObject *rHeart = (PlayerUIObject *)mPlayerHeartList->PushIterator();
    while(rHeart)
    {
        //--Position.
        float tXPos = rHeart->mXPos - cXHf;
        float tYPos = rHeart->mYPos - cYHf;

        //--Render normal.
        if(rHeart != rTail || mPlayerHP % 2 == 0)
        {
            Images.Data.rBarHeart->Draw(tXPos, tYPos);
        }
        //--Render split in half if the player has half of a heart.
        else
        {
            float cWid = Images.Data.rBarHeart->GetTrueWidth() * 0.50f;
            float cHei = Images.Data.rBarHeart->GetTrueHeight();
            Images.Data.rBarHeart->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(tXPos,        tYPos);
                glTexCoord2f(0.5f, 1.0f); glVertex2f(tXPos + cWid, tYPos);
                glTexCoord2f(0.5f, 0.0f); glVertex2f(tXPos + cWid, tYPos + cHei);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(tXPos,        tYPos + cHei);
            glEnd();
        }

        //--Next.
        rHeart = (PlayerUIObject *)mPlayerHeartList->AutoIterate();
    }
}
