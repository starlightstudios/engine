//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvUIChat.h"
#include "AdvUICostumes.h"
#include "AdvUIFieldAbilities.h"
#include "AdvUIForms.h"
#include "AdvUIVendor.h"
#include "AdvUIMap.h"
#include "AdvUIPassword.h"
#include "AdvUIRelive.h"
#include "AdvUIWarp.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

///======================================== Lua Hooking ===========================================
void AdventureMenu::HookToLuaState(lua_State *pLuaState)
{
    /* AM_GetProperty("Dummy") (Static) (1 Boolean)
       Returns the requested property in the AdventureMenu. */
    lua_register(pLuaState, "AM_GetProperty", &Hook_AM_GetProperty);

    /* [Static]
       AM_SetProperty("Party Resolve Script", sPath) (Static)
       AM_SetProperty("Rest Resolve Script", sPath) (Static)
       AM_SetProperty("Bestiary Resolve Script", sPath) (Static)
       AM_SetProperty("Profile Resolve Script", sPath) (Static)
       AM_SetProperty("Quest Resolve Script", sPath) (Static)
       AM_SetProperty("Warp Resolve Script", sPath) (Static)
       AM_SetProperty("Warp Execute Script", sPath) (Static)
       AM_SetProperty("Relive Resolve Script", sPath) (Static)
       AM_SetProperty("Costume Resolve Script", sPath) (Static)
       AM_SetProperty("Field Ability Resolve Script", sPath) (Static)
       AM_SetProperty("Total Catalysts", iNumber) (Static)
       AM_SetProperty("Text Image Remaps Total", iNumber) (Static)
       AM_SetProperty("Text Image Remaps", iSlot, sBaseName, sDLPath) (Static)
       AM_SetProperty("Flag Handled Password") (Static)
       AM_SetProperty("Form Resolve Script", sScriptPath) (Static)
       AM_SetProperty("Form Party Resolve Script", sScriptPath) (Static)
       AM_SetProperty("Set Relive Icon", sDLPath) (Static)
       AM_SetProperty("Set Form Icon", sDLPath) (Static)

       [Dynamic]
       AM_SetProperty("Activate Nowhere Mode")
       AM_SetProperty("Chat Member", sName, sPath, sImagePath)
       AM_SetProperty("Map Edge Pad", iPadding)
       AM_SetProperty("Clear Advanced Map")
       AM_SetProperty("Advanced Map", sLayerPath)
       AM_SetProperty("Advanced Map Fixed", sLayerPath)
       AM_SetProperty("Advanced Map", sLayerPath, iLayerChanceLo, iLayerChanceHi)
       AM_SetProperty("Advanced Map Player", sIndicatorPath, fXPos, fYPos)
       AM_SetProperty("Advanced Map Properties", fRemapX, fRemapY, fScaleX, fScaleY)
       AM_SetProperty("Execute Rest")

       [Relive]
       AM_SetProperty("Register Relive Script", sDisplayName, sScriptPath, sImagePath)
       AM_SetProperty("Set Relive Alignments", sDisplayName, fXOff, fYOff, fLft, fTop, fWid, fHei)

       [Costume]
       AM_SetProperty("Register Costume Heading", sInternalName, sCharacterName, sDisplayName, sFormName, sImageDLPath)
       AM_SetProperty("Register Costume Entry", sHeadingName, sCostumeName, sImageDLPath, sScriptPath)

       [Forms]
       AM_SetProperty("Register Form Member", sMemberName, sDisplayName, sImgPath)
       AM_SetProperty("Set Form Member Alignments", sMemberName, fXOff, fYOff, fLft, fTop, fWid, fHei)
       AM_SetProperty("Register Form Class", sFormName, sDisplayName, sFormPath, sJobScriptPath, sImgPath)
       AM_SetProperty("Set Form Class Alignments", sMemberName, fXOff, fYOff, fLft, fTop, fWid, fHei)

       [Warp]
       AM_SetProperty("Register Warp Region", sRegionName, sImagePath)
       AM_SetProperty("Set Warp Region Alignments", sRegionName, fXOff, fYOff, fLft, fTop, fWid, fHei)
       AM_SetProperty("Register Warp Region Advanced Pack", sRegionName, sImagePath, iLoRoll, iHiRoll)
       AM_SetProperty("Register Warp Location", sRegionName, sDisplayName, sMapName, sImgPath, fImgX, fImgY)

       [Field Abilities]
       AM_SetProperty("Register Field Ability", sDLPath)
       Sets the requested property in the AdventureMenu. */
    lua_register(pLuaState, "AM_SetProperty", &Hook_AM_SetProperty);

    /* AM_SetMapInfo(sMapPath, sMapPathOverlay, iPlayerX, iPlayerY)
       Sets the current map image and where the player is on the map. Pass "Null" to clear the map. */
    lua_register(pLuaState, "AM_SetMapInfo", &Hook_AM_SetMapInfo);

    /* AM_SetShopProperty("Show", sShopName, sBootPath)
       AM_SetShopProperty("Add Item", sName, iPriceOverride, iQuantity)
       AM_SetShopProperty("Register Upgrade", sName, iPlatinaCost, iPowderCost, iFlakeCost, iShardCost, iPieceCost, iChunkCost, iOreCost)
       Sets properties related to shops. */
    lua_register(pLuaState, "AM_SetShopProperty", &Hook_AM_SetShopProperty);

    ///--[Sub Handlers]
    //HookToLuaStateJournal(pLuaState);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AM_GetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    //AM_GetProperty("Unlock Replace Info") (Static) (1 String)

    ///--[Dynamic]
    ///--[ ========= Argument Resolve ========= ]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Static: Unlock replacement info. Can be NULL in which case "Null" is passed to the caller.
    if(!strcasecmp(rSwitchType, "Unlock Replace Info") && tArgs == 1)
    {
        //--Handle null case.
        if(!AdvUIVendor::xUnlockReplaceInfo)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, AdvUIVendor::xUnlockReplaceInfo);
        }

        //--Common.
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("AM_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AM_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    //AM_SetProperty("Party Resolve Script", sPath) (Static)
    //AM_SetProperty("Rest Resolve Script", sPath) (Static)
    //AM_SetProperty("Bestiary Resolve Script", sPath) (Static)
    //AM_SetProperty("Profile Resolve Script", sPath) (Static)
    //AM_SetProperty("Quest Resolve Script", sPath) (Static)
    //AM_SetProperty("Location Resolve Script", sPath) (Static)
    //AM_SetProperty("Combat Resolve Script", sPath) (Static)
    //AM_SetProperty("Warp Resolve Script", sPath) (Static)
    //AM_SetProperty("Warp Execute Script", sPath) (Static)
    //AM_SetProperty("Relive Resolve Script", sPath) (Static)
    //AM_SetProperty("Costume Resolve Script", sPath) (Static)
    //AM_SetProperty("Field Ability Resolve Script", sPath) (Static)
    //AM_SetProperty("Total Catalysts", iNumber) (Static)
    //AM_SetProperty("Text Image Remaps Total", iNumber) (Static)
    //AM_SetProperty("Text Image Remaps", iSlot, sBaseName, sDLPath) (Static)
    //AM_SetProperty("Flag Handled Password") (Static)
    //AM_SetProperty("Form Resolve Script", sScriptPath) (Static)
    //AM_SetProperty("Form Party Resolve Script", sScriptPath) (Static)
    //AM_SetProperty("Set Relive Icon", sDLPath) (Static)
    //AM_SetProperty("Set Costume Icon", sDLPath) (Static)
    //AM_SetProperty("Set Form Icon", sDLPath) (Static)
    //AM_SetProperty("Set Deconstruct Script", sPath) (Static)

    ///--[Dynamic]
    //--[System]
    //AM_SetProperty("Close Menu Now")
    //AM_SetProperty("Activate Nowhere Mode")
    //AM_SetProperty("Chat Member", sName, sPath, sImagePath)
    //AM_SetProperty("Execute Rest")
    //AM_SetProperty("Append Deconstruct Item", sItemName)
    //AM_SetProperty("Re-Call Base Initialize")

    //--[Map]
    //AM_SetProperty("Player Map Position", fX, fY)
    //AM_SetProperty("Player Sprite Flashes", bFlag)
    //AM_SetProperty("Map Edge Pad", iPadding)
    //AM_SetProperty("Clear Map Pins")
    //AM_SetProperty("Register Map Pin", fX, fY, sImagePath)

    //--[Advanced Map]
    //AM_SetProperty("Clear Advanced Map")
    //AM_SetProperty("Create Advanced Map Layer", sLayerName, sLayerPath)
    //AM_SetProperty("Layer Is Toggleable", sLayerName, bIsToggleable)
    //AM_SetProperty("Layer Render Chance", sLayerName, iRenderLo, iRenderHi)
    //AM_SetProperty("Layer Is Fixed", sLayerName, bIsFixed)
    //AM_SetProperty("Layer Renders Player", sLayerName, sImgPath, bFlashes, fXOffset, fYOffset)
    //AM_SetProperty("Layer Reads Stencil", sLayerName, iStencilValue)
    //AM_SetProperty("Layer Writes Stencil", sLayerName, iStencilValue)

    //--[Relive]
    //AM_SetProperty("Register Relive Script", sDisplayName, sScriptPath, sImagePath)
    //AM_SetProperty("Set Relive Alignments", sDisplayName, fXOff, fYOff, fLft, fTop, fWid, fHei)

    //--[Costume]
    //AM_SetProperty("Register Costume Heading", sInternalName, sCharacterName, sDisplayName, sFormName, sImageDLPath)
    //AM_SetProperty("Register Costume Entry", sHeadingName, sCostumeName, sImageDLPath, sScriptPath)

    //--[Forms]
    //AM_SetProperty("Register Form Member", sMemberName, sDisplayName, sImgPath)
    //AM_SetProperty("Set Form Member Alignments", sMemberName, fXOff, fYOff, fLft, fTop, fWid, fHei)
    //AM_SetProperty("Register Form Class", sFormName, sDisplayName, sFormPath, sJobScriptPath, sImgPath)
    //AM_SetProperty("Set Form Class Alignments", sMemberName, fXOff, fYOff, fLft, fTop, fWid, fHei)

    //--[Warp]
    //AM_SetProperty("Register Warp Region", sRegionName, sIconPath, sMapPath, sMapOverlayPath)
    //AM_SetProperty("Set Warp Region Alignments", sRegionName, fXOff, fYOff, fLft, fTop, fWid, fHei)
    //AM_SetProperty("Set Warp Region Advanced Clamps", sRegionName, fClampLft, fClampTop, fClampRgt, fClampBot)
    //AM_SetProperty("Register Warp Location", sRegionName, sDisplayName, sActualName, fImgX, fImgY)
    //AM_SetProperty("Register Warp Region Advanced Pack", sRegionName, sLayerName, sImagePath)
    //AM_SetProperty("Warp Region Advanced Render Chance", sRegionName, sLayerName, iRenderLo, iRenderHi)
    //AM_SetProperty("Warp Region Advanced Render Reads Stencil", sRegionName, sLayerName, iStencilValue)
    //AM_SetProperty("Warp Region Advanced Render Writes Stencil", sRegionName, sLayerName, iStencilValue)
    //AM_SetProperty("Warp Region Advanced Render Can Toggle", sRegionName, sLayerName, bCanToggle)

    //--[Field Abilities]
    //AM_SetProperty("Register Field Ability", sDLPath)

    //--[Trainer]
    //AM_SetProperty("Open Trainer")

    ///--[ ========= Argument Resolve ========= ]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Statics.
    if(!strcasecmp(rSwitchType, "Party Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIChat::xChatResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired when the player rests.
    else if(!strcasecmp(rSwitchType, "Rest Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xRestResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine where the player can warp to.
    else if(!strcasecmp(rSwitchType, "Warp Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIWarp::xWarpResolveScript, lua_tostring(L, 2));
        ResetString(AdventureMenu::xWarpResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to warp the player somewhere.
    else if(!strcasecmp(rSwitchType, "Warp Execute Script") && tArgs == 2)
    {
        ResetString(AdvUIWarp::xWarpExecuteScript, lua_tostring(L, 2));
        ResetString(AdventureMenu::xWarpExecuteScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine reliving of scenarios.
    else if(!strcasecmp(rSwitchType, "Relive Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIRelive::xReliveResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine which costumes are available.
    else if(!strcasecmp(rSwitchType, "Costume Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUICostumes::xCostumeResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to resolve which field abilities are available.
    else if(!strcasecmp(rSwitchType, "Field Ability Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xFieldAbilityResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Total number of Catalysts available in this chapter.
    else if(!strcasecmp(rSwitchType, "Total Catalysts") && tArgs == 2)
    {
        AdventureMenu::xCatalystTotal = lua_tointeger(L, 2);
        return 0;
    }
    //--How many text-image remaps there are. Allocates space.
    else if(!strcasecmp(rSwitchType, "Text Image Remaps Total") && tArgs == 2)
    {
        //--Get how many are expected.
        int tExpected = lua_tointeger(L, 2);

        //--Deallocate existing amount.
        for(int i = 0; i < AdventureMenu::xTextImgRemapsTotal; i ++)
        {
            free(AdventureMenu::xTextImgPath[i]);
            free(AdventureMenu::xTextImgRemap[i]);
        }
        AdventureMenu::xTextImgRemapsTotal = 0;
        free(AdventureMenu::xTextImgPath);
        free(AdventureMenu::xTextImgRemap);

        //--Allocate space and clear.
        AdventureMenu::xTextImgRemapsTotal = tExpected;
        SetMemoryData(__FILE__, __LINE__);
        AdventureMenu::xTextImgPath  = (char **)starmemoryalloc(sizeof(char *) * tExpected);
        SetMemoryData(__FILE__, __LINE__);
        AdventureMenu::xTextImgRemap = (char **)starmemoryalloc(sizeof(char *) * tExpected);
        for(int i = 0; i < AdventureMenu::xTextImgRemapsTotal; i ++)
        {
            AdventureMenu::xTextImgPath[i]  = NULL;
            AdventureMenu::xTextImgRemap[i] = NULL;
        }
        //fprintf(stderr, "Set image remaps total to %i\n", tExpected);
        return 0;
    }
    //--Sets a text remap.
    else if(!strcasecmp(rSwitchType, "Text Image Remaps") && tArgs == 4)
    {
        //--Check the slot.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AdventureMenu::xTextImgRemapsTotal) return 0;

        //--Set.
        ResetString(AdventureMenu::xTextImgRemap[tSlot], lua_tostring(L, 3));
        ResetString(AdventureMenu::xTextImgPath[tSlot],  lua_tostring(L, 4));
        //fprintf(stderr, "Remapped %i to %s - %s\n", tSlot, AdventureMenu::xTextImgRemap[tSlot], AdventureMenu::xTextImgPath[tSlot]);
        //void *rCheckPtr = DataLibrary::Fetch()->GetEntry(AdventureMenu::xTextImgPath[tSlot]);
        //fprintf(stderr, " Check ptr %p\n", rCheckPtr);
        return 0;
    }
    //--Indicates a subscript handled a password.
    else if(!strcasecmp(rSwitchType, "Flag Handled Password") && tArgs == 1)
    {
        AdvUIPassword::xHandledPassword = true;
        return 0;
    }
    //--Which script to use when resolving forms for each party member.
    else if(!strcasecmp(rSwitchType, "Form Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIForms::xFormResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Which script to use to figure out which party members are in the party and can transform.
    if(!strcasecmp(rSwitchType, "Form Party Resolve Script") && tArgs == 2)
    {
        ResetString(AdvUIForms::xFormPartyResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Changes which icon appears in the costume slot.
    else if(!strcasecmp(rSwitchType, "Set Costume Icon") && tArgs == 2)
    {
        ResetString(AdventureMenu::xCostumeIconPath, lua_tostring(L, 2));
        return 0;
    }
    //--Changes which icon appears in the transformation slot.
    else if(!strcasecmp(rSwitchType, "Set Form Icon") && tArgs == 2)
    {
        ResetString(AdventureMenu::xTransformIconPath, lua_tostring(L, 2));
        return 0;
    }
    //--Changes which icon is used for the Relive image.
    else if(!strcasecmp(rSwitchType, "Set Relive Icon") && tArgs == 2)
    {
        ResetString(AdventureMenu::xReliveIconPath, lua_tostring(L, 2));
        return 0;
    }
    //--Changes the script called to deconstruct items.
    else if(!strcasecmp(rSwitchType, "Set Deconstruct Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xDeconstructScript, lua_tostring(L, 2));
        return 0;
    }

    ///--[Sub-Handlers]
    //--Ask the AdvUIWarp handler if it caught the input. This only occurs after static calls.
    bool tWarpCaughtInput = false;
    int tResult = Hook_AdvUIWarp_SetProperty(tWarpCaughtInput, L);
    if(tWarpCaughtInput) return tResult;

    //--Ask the AdvUIMap handler if it caught the input.
    bool tMapCaughtInput = false;
    tResult = Hook_AdvUIMap_SetProperty(tMapCaughtInput, L);
    if(tMapCaughtInput) return tResult;

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetProperty", L);

    ///--[Dynamics]
    //--Close the menu immediately.
    if(!strcasecmp(rSwitchType, "Close Menu Now") && tArgs == 1)
    {
        rMenu->Hide();
        rMenu->ZeroVisibility();
    }
    //--Adds a character to the chat listing.
    else if(!strcasecmp(rSwitchType, "Chat Member") && tArgs == 10)
    {
        //--Chat Submenu.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Chat", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIChat *rChatUI = dynamic_cast<AdvUIChat *>(rStarUIPiece);
        rChatUI->AddChatEntity(lua_tostring(L, 2), lua_tostring(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8), lua_tonumber(L, 9), lua_tostring(L, 10));
    }
    //--Adds a deconstruction item to the list.
    else if(!strcasecmp(rSwitchType, "Append Deconstruct Item") && tArgs == 2)
    {
        rMenu->AppendDeconstructItem(lua_tostring(L, 2));
    }
    //--The "Switch Party" button does not appear unless there are enough active members to justify it. However,
    //  when a new member is added, the menu does not re-check this until it is reopened. Call this after enabling
    //  switching to make the menu work correctly. It won't open the menu, just reorganize it.
    else if(!strcasecmp(rSwitchType, "Re-Call Base Initialize") && tArgs == 1)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Base", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIBase *rBaseUI = dynamic_cast<AdvUIBase *>(rStarUIPiece);
        rBaseUI->ResetBuiltGridFlag();
    }
    //--Executes a rest action, doesn't handle the fading in and out part.
    else if(!strcasecmp(rSwitchType, "Execute Rest") && tArgs == 1)
    {
        rMenu->ExecuteRest();
    }
    ///--[Relive]
    //--Adds a script we can relive.
    else if(!strcasecmp(rSwitchType, "Register Relive Script") && tArgs == 4)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Relive", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIRelive *rReliveUI = dynamic_cast<AdvUIRelive *>(rStarUIPiece);
        rReliveUI->RegisterReliveScript(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Sets icon alignments for the relive script.
    else if(!strcasecmp(rSwitchType, "Set Relive Alignments") && tArgs == 8)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Relive", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIRelive *rReliveUI = dynamic_cast<AdvUIRelive *>(rStarUIPiece);
        rReliveUI->SetReliveAlignments(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
    }
    ///--[Costumes]
    //--When building the costume menu, registers a new "Heading", which is a playable form (like Christine Golem or Mei Alraune)
    else if(!strcasecmp(rSwitchType, "Register Costume Heading") && tArgs == 6)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Costumes", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUICostumes *rCostumeUI = dynamic_cast<AdvUICostumes *>(rStarUIPiece);
        rCostumeUI->AddCostumeHeading(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
        //rMenu->AddCostumeHeading(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
    }
    //--Sets image alignment data for a costume heading.
    else if(!strcasecmp(rSwitchType, "Set Heading Alignments") && tArgs == 8)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Costumes", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUICostumes *rCostumeUI = dynamic_cast<AdvUICostumes *>(rStarUIPiece);
        rCostumeUI->SetCostumeHeadingImageData(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
        //rMenu->SetCostumeHeadingImageData(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
    }
    //--When building the costume menu, registers a costume entry to an existing heading. These are costumes like Christine's Gala dress.
    else if(!strcasecmp(rSwitchType, "Register Costume Entry") && tArgs == 5)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Costumes", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUICostumes *rCostumeUI = dynamic_cast<AdvUICostumes *>(rStarUIPiece);
        rCostumeUI->AddCostumeEntry(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5));
        //rMenu->AddCostumeEntry(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5));
    }
    //--Sets image alignment data for a costume entry.
    else if(!strcasecmp(rSwitchType, "Set Entry Alignments") && tArgs == 9)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Costumes", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUICostumes *rCostumeUI = dynamic_cast<AdvUICostumes *>(rStarUIPiece);
        rCostumeUI->SetCostumeEntryImageData(lua_tostring(L, 2), lua_tostring(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8), lua_tonumber(L, 9));
        //rMenu->SetCostumeEntryImageData(lua_tostring(L, 2), lua_tostring(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8), lua_tonumber(L, 9));
    }
    ///--[Forms]
    //--When opening the Forms UI, registers a party member.
    else if(!strcasecmp(rSwitchType, "Register Form Member") && tArgs == 4)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Forms", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIForms *rFormsUI = dynamic_cast<AdvUIForms *>(rStarUIPiece);
        rFormsUI->RegisterFormPartyMember(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
        //rMenu->RegisterFormPartyMember(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Alignments for party members in Forms UI.
    else if(!strcasecmp(rSwitchType, "Set Form Member Alignments") && tArgs == 8)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Forms", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIForms *rFormsUI = dynamic_cast<AdvUIForms *>(rStarUIPiece);
        rFormsUI->SetFormPartyMemberAlignments(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
        //rMenu->SetFormPartyMemberAlignments(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
    }
    //--When selecting a party member in the Forms UI, registers a tranformation.
    else if(!strcasecmp(rSwitchType, "Register Form Class") && tArgs == 6)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Forms", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIForms *rFormsUI = dynamic_cast<AdvUIForms *>(rStarUIPiece);
        rFormsUI->RegisterFormTransformation(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
        //rMenu->RegisterFormTransformation(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
    }
    //--Alignments for transformations in Forms UI.
    else if(!strcasecmp(rSwitchType, "Set Form Class Alignments") && tArgs == 8)
    {
        //--Locate the UI piece.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("Forms", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return 0;

        //--Dynamic cast.
        AdvUIForms *rFormsUI = dynamic_cast<AdvUIForms *>(rStarUIPiece);
        rFormsUI->SetFormPartyTransformationAlignments(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
        //rMenu->SetFormPartyTransformationAlignments(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
    }
    ///--[Field Abilities]
    //--Registers a field ability when rebuilding the ability list.
    else if(!strcasecmp(rSwitchType, "Register Field Ability") && tArgs == 2)
    {
        //--Get the internal UI.
        StarUIPiece *rStarUIPiece = rMenu->RetrieveUI("FieldAbilities", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
        if(!rStarUIPiece) return LuaTypeError("AM_SetProperty", L);

        //--Cast, call.
        AdvUIFieldAbilities *rFieldAbilityUI = dynamic_cast<AdvUIFieldAbilities *>(rStarUIPiece);
        rFieldAbilityUI->RegisterFieldAbility(lua_tostring(L, 2));
    }
    ///--[Trainer]
    //--Opens the menu to Trainer mode.
    else if(!strcasecmp(rSwitchType, "Open Trainer") && tArgs == 1)
    {
        rMenu->OpenTrainer();
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("AM_SetProperty", rSwitchType, tArgs);
    }
    return 0;
}
int Hook_AM_SetMapInfo(lua_State *L)
{
    ///--[Arguments]
    //AM_SetMapInfo(sMapPath, sMapPathOverlay, iPlayerX, iPlayerY)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("AM_SetMapInfo");

    ///--[Resolve Object]
    //--Dynamic. Get the AdventureMenu which holds the AdvUIMap object.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetMapInfo", L);

    //--Get the internal map UI.
    StarUIPiece *rUIPiece = rMenu->RetrieveUI("Map", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetMapInfo");
    if(!rUIPiece) return LuaTypeError("AM_SetMapInfo", L);

    //--Cast.
    AdvUIMap *rMapUI = dynamic_cast<AdvUIMap *>(rUIPiece);

    ///--[Execution]
    //--Set.
    rMapUI->SetMapInfo(lua_tostring(L, 1), lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    return 0;
}
int Hook_AM_SetShopProperty(lua_State *L)
{
    ///--[Argument Listing]
    //AM_SetShopProperty("Show", sShopName, sBootPath, sTeardownPath)
    //AM_SetShopProperty("Add Item", sName, iPriceOverride, iQuantity)
    //AM_SetShopProperty("Set Item Display Name", sItemName, sDisplayName)
    //AM_SetShopProperty("Remove Item", sName)
    //AM_SetShopProperty("Register Upgrade", sName, iPlatinaCost, iPowderCost, iFlakeCost, iShardCost, iPieceCost, iChunkCost, iOreCost)
    //AM_SetShopProperty("Allow Gemcutting")
    //AM_SetShopProperty("Set Buy Response Script", sPath)
    //AM_SetShopProperty("Disallow Equip Response")
    //AM_SetShopProperty("Set Unlock Mode")
    //AM_SetShopProperty("Set Unlock Can Equip")
    //AM_SetShopProperty("Set Unlock Variable", sItemName, sVariablePath)
    //AM_SetShopProperty("Set Unlock Removal", sItemName, bRemoveOnUnlock)
    //AM_SetShopProperty("Set Unlock Execute Script", sItemName, sExecutionPath)
    //AM_SetShopProperty("Add Unlock Requirement", sItemName, sUnlockName, sUnlockShort, iQuantity)
    //AM_SetShopProperty("Add Unlock Material", sItemName, sShorthandName)
    //AM_SetShopProperty("Add Category", sCategoryName, sImgPath)
    //AM_SetShopProperty("Add Item To Category", sItemName, sCategoryName)
    //AM_SetShopProperty("Set Use Materials Pages", bUsePages)
    //AM_SetShopProperty("Clear Materials Pages")
    //AM_SetShopProperty("Add Materials Page", sPageName)
    //AM_SetShopProperty("Add Material To Page", sPageName, sItemName, sShorthand)
    //AM_SetShopProperty("Recompute Cursor")
    //AM_SetShopProperty("Set Buy Only")

    ///--[Argument Checking]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetShopProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Object.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetShopProperty", L);

    AdvUIVendor *rVendorUI = rMenu->GetVendor();
    if(!rVendorUI) return LuaTypeError("AM_SetShopProperty", L);

    ///--[General]
    //--Activates the shop menu and calls the provided subroutine.
    if(!strcasecmp(rSwitchType, "Show") && tArgs == 4)
    {
        rMenu->InitializeVendorMode(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Adds a new item to the shop. Pass -1 for price to use default value. Pass -1 for quantity to have unlimited.
    else if(!strcasecmp(rSwitchType, "Add Item") && tArgs == 4)
    {
        rVendorUI->RegisterItem(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets display name override for an item.
    else if(!strcasecmp(rSwitchType, "Set Item Display Name") && tArgs == 3)
    {
        rVendorUI->SetItemDisplayName(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Removes an item from the shop.
    else if(!strcasecmp(rSwitchType, "Remove Item") && tArgs == 2)
    {
        rVendorUI->UnregisterItem(lua_tostring(L, 2));
    }
    //--Allows the shop to cut gems.
    else if(!strcasecmp(rSwitchType, "Allow Gemcutting") && tArgs == 1)
    {
        rVendorUI->SetGemcutterFlag(true);
    }
    //--Sets a script that fires whenever the player buys an item.
    else if(!strcasecmp(rSwitchType, "Set Buy Response Script") && tArgs == 2)
    {
        rVendorUI->SetBuyResponseScript(lua_tostring(L, 2));
    }
    //--If a buy response is in place, call this function to prevent the player from equipping the item
    //  they just bought if the script is going to remove it.
    else if(!strcasecmp(rSwitchType, "Disallow Equip Response") && tArgs == 1)
    {
        AdventureMenu::xDisallowEquippingFromResponse = true;
        AdvUIVendor::xDisallowEquipFromResponse = true;
    }
    //--Sets to unlock mode. Disables normal shop functionality.
    else if(!strcasecmp(rSwitchType, "Set Unlock Mode") && tArgs == 1)
    {
        rVendorUI->ActivateModeUnlock();
    }
    //--Call after creating an item to let the interface know it can ask the player to equip it.
    else if(!strcasecmp(rSwitchType, "Set Unlock Can Equip") && tArgs == 1)
    {
        rVendorUI->SetUnlockCanEquip(true);
    }
    //--Sets the associated variable for the given item.
    else if(!strcasecmp(rSwitchType, "Set Unlock Variable") && tArgs == 3)
    {
        rVendorUI->SetUnlockVariable(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--If true, the item is removed from the vendor list when unlocked.
    else if(!strcasecmp(rSwitchType, "Set Unlock Removal") && tArgs == 3)
    {
        rVendorUI->SetUnlockRemove(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets a script path to execute when an item is unlocked.
    else if(!strcasecmp(rSwitchType, "Set Unlock Execute Script") && tArgs == 3)
    {
        rVendorUI->SetUnlockExecute(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Adds a new unlock requirement for the named item.
    else if(!strcasecmp(rSwitchType, "Add Unlock Requirement") && tArgs == 5)
    {
        rVendorUI->AddItemUnlockRequirement(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tointeger(L, 5));
    }
    //--Adds a material to the list. Not required for unlocking to actually work.
    else if(!strcasecmp(rSwitchType, "Add Unlock Material") && tArgs == 3)
    {
        rVendorUI->AddUnlockMaterial(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Registers a new category for the shop.
    else if(!strcasecmp(rSwitchType, "Add Category") && tArgs == 3)
    {
        rVendorUI->RegisterCategory(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Places an item on the named category list.
    else if(!strcasecmp(rSwitchType, "Add Item To Category") && tArgs == 3)
    {
        rVendorUI->RegisterItemToCategory(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Toggles using pages in the materials display.
    else if(!strcasecmp(rSwitchType, "Set Use Materials Pages") && tArgs == 2)
    {
        rVendorUI->SetUnlockMaterialUsePages(lua_toboolean(L, 2));
    }
    //--Clears materials page data.
    else if(!strcasecmp(rSwitchType, "Clear Materials Pages") && tArgs == 1)
    {
        rVendorUI->ClearMaterialsPages();
    }
    //--Adds a new page of materials.
    else if(!strcasecmp(rSwitchType, "Add Materials Page") && tArgs == 2)
    {
        rVendorUI->AddMaterialsPage(lua_tostring(L, 2));
    }
    //--Adds a material to the named page.
    else if(!strcasecmp(rSwitchType, "Add Material To Page") && tArgs == 4)
    {
        rVendorUI->AddUnlockMaterialToPage(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Order the cursor to recompute.
    else if(!strcasecmp(rSwitchType, "Recompute Cursor") && tArgs == 1)
    {
        rVendorUI->RecomputeCursorPositions();
    }
    //--Cannot sell/buyback while this flag is active.
    else if(!strcasecmp(rSwitchType, "Set Buy Only") && tArgs == 1)
    {
        rVendorUI->SetBuyOnly(true);
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AM_SetShopProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
