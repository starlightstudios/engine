//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

int AdvCombat::GetNextEnemyID()
{
    //--Returns a unique ID. Used for enemies to give them all unique names. The ID counter is reset
    //  each time the combat handler is reinitialized. Enemies are programmatically registered with
    //  the unique name "Autogen|XX" where XX is the ID. Use leading zeroes to pad.
    mUniqueEnemyIDCounter ++;
    return mUniqueEnemyIDCounter - 1;
}
const char *AdvCombat::GetParagonScript()
{
    return mParagonScript;
}
const char *AdvCombat::GetEnemyAutoScript()
{
    return mEnemyAutoScript;
}
int AdvCombat::GetLastParagonValue()
{
    return mLastParagonValue;
}
AdvCombatEntity *AdvCombat::GetLastRegisteredEnemy()
{
    return rLastRegisteredEnemy;
}
void AdvCombat::RegisterEnemy(const char *pUniqueName, AdvCombatEntity *pEntity, int pReinforcementTurns)
{
    ///--[Documentation]
    //--Registers the given enemy to combat. If the reinforcement timer is zero, they are registered
    //  to the enemy combat listing immediately. Otherwise, they are stuffed into a reinforcement pack.
    //  The first turn for reinforcements is turn 2, as a turn-1 reinforcement would be registered immediately.
    rLastRegisteredEnemy = pEntity;
    if(!pUniqueName || !pEntity) return;

    //--In all cases, register the enemy to the roster.
    mEnemyRoster->AddElement(pUniqueName, pEntity, &RootObject::DeleteThis);

    //--Reinforcement counter is 0 or lower, so they are registered to combat immediately:
    if(pReinforcementTurns < 1)
    {
        pEntity->SetReinforcementTurns(0);
        mrEnemyCombatParty->AddElementAsTail(pUniqueName, pEntity);
    }
    //--Otherwise, they go in the reinforcement bin.
    else
    {
        pEntity->SetReinforcementTurns(pReinforcementTurns);
        mrEnemyReinforcements->AddElementAsTail(pUniqueName, pEntity);
    }
}
void AdvCombat::SetParagonScript(const char *pScript)
{
    ResetString(mParagonScript, pScript);
}
void AdvCombat::SetEnemyAutoScript(const char *pScript)
{
    ResetString(mEnemyAutoScript, pScript);
}
void AdvCombat::SetParagonFlag(int pFlag)
{
    mLastParagonValue = pFlag;
}
