//--Base
#include "Chapter2LoadInterrupt.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
Chapter2LoadInterrupt::Chapter2LoadInterrupt()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======== StarLoadInterrupt ========= ]
    ///--[ ======= Chapter2LoadInterrupt ====== ]
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    mIsReady = false;
}
Chapter2LoadInterrupt::~Chapter2LoadInterrupt()
{
}
void Chapter2LoadInterrupt::Construct()
{
    ///--[Documentation]
    //--Loads and verifies images associated with this object. Because this object is meant to appear during
    //  the loading of other objects, it handles its images internally, not using a conventional DataLibrary.

    ///--[Load Frames]
    //--Loading animation is a filmstrip series placed in a linked list.
    StarLumpManager *tSLM = new StarLumpManager();
    tSLM->Open("Data/Loading.slf");
    if(!tSLM->IsFileOpen()) return;

    //--Suppress the load interrupt.
    bool tOldFlag = StarBitmap::xSuppressInterrupt;
    StarBitmap::xSuppressInterrupt = true;

    //--Load.

    //--Verify.
    mIsReady = false;

    ///--[Finish Up]
    //--Set to the String Tyrant variant. If anything went wrong, the default one will be used.
    StarBitmap::xInterruptCall = &StarLoadInterrupt::Interrupt;

    //--Set this flag.
    mIsAwaitingKeypress = false;

    //--Clean up.
    delete tSLM;
    StarBitmap::xSuppressInterrupt = tOldFlag;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void Chapter2LoadInterrupt::DeallocateImages()
{
    ///--[Documentation]
    //--Deallocates and NULLs all images associated with this object.
    mIsReady = false;
    StarLoadInterrupt::DeallocateImages();

    //--Derived call.
    //--NULL.
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void Chapter2LoadInterrupt::Call(bool pForceRender)
{
    ///--[Documentation and Setup]
    //--Chapter 2 Load Interrupt renderer. Doesn't do anything yet.
    if(!CommonLogicUpdate(pForceRender) || !mIsReady) return;

    //--Call the renderer setup. The screen is now ready for drawing.
    //CommonRendererSetup();

    ///--[Finish Up]
    //CommonRendererClean();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void Chapter2LoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_BootChapter2()
       Switches to String Tyrant mode. Pass "NULL" to switch to normal mode. */
    lua_register(pLuaState, "LI_BootChapter2", &Hook_LI_BootChapter2);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LI_BootChapter2(lua_State *L)
{
    //LI_BootChapter2()

    //--Create a new LoadInterrupt.
    Chapter2LoadInterrupt *nInterrupt = new Chapter2LoadInterrupt();
    nInterrupt->Construct();

    //--Dealloacte the old one.
    GLOBAL *rGlobal = Global::Shared();
    delete rGlobal->gLoadInterrupt;

    //--Replace.
    rGlobal->gLoadInterrupt = nInterrupt;
    return 0;
}
