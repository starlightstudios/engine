//--Base
#include "AdvUICostumes.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUICostumes::AdvUICostumes()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "ACOS");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = (VIRTUAL_CANVAS_X * 0.50f);
    mGridRenderCenterY  = (VIRTUAL_CANVAS_Y * 0.35f) + 50.0f;
    mGridRenderSpacingX = (VIRTUAL_CANVAS_X * 0.10f);
    mGridRenderSpacingY = (VIRTUAL_CANVAS_X * 0.10f);

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ===== AdvUICostumes ====== ]
    ///--[Variables]
    //--System
    mIsEntryMode = false;
    mEntryTimer = 0;

    //--Cursor
    mDontShowHighlight = false;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Data
    mHeadingData = new StarLinkedList(true);
    rEntryData = new StarLinkedList(false);

    //--Strings
    mShowHelpString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Additional grids.
    CreateStoredGrid("Entries");

    //--Help Strings.
    AllocateHelpStrings(cxAdvCostumeHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUICostumes::~AdvUICostumes()
{
    delete mHeadingData;
    delete rEntryData;
    delete mShowHelpString;
}
void AdvUICostumes::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Costumes UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",     &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = AdvImages.rFont_Mainline;
}

///--[Public Statics]
//--Script that fires when the costumes menu initializes, building a list of available costumes.
char *AdvUICostumes::xCostumeResolveScript = NULL;

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUICostumes::TakeForeground()
{
    ///--[Documentation and Setup]
    //--If there's no costume menu handler to call, stop here.
    if(!xCostumeResolveScript)
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--If the base grid wasn't active, activate it now. Then, clear and reset the entries grid.
    ActivateStoredGrid("Base");
    ClearStoredGrids();
    CreateStoredGrid("Entries");

    //--The costumes menu needs to be rebuilt each time it is activated, since the party may have
    //  changed since last it was opened.
    mHeadingData->ClearList();
    LuaManager::Fetch()->ExecuteLuaFile(xCostumeResolveScript);

    //--If no costumes got built somehow, stop here.
    if(mHeadingData->GetListSize() < 1)
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Play SFX to indicate the menu is opened.
    AudioManager::Fetch()->PlaySound("Menu|Select");

    //--Set flags.
    mIsEntryMode = false;

    ///--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    AutoAllocateSquareGrid(mHeadingData->GetListSize());
    ProcessGrid();

    //--Iteration loop.
    int i = 0;
    CostumeHeadingPack *rPackage = (CostumeHeadingPack *)mHeadingData->PushIterator();
    while(rPackage)
    {
        //--Retrieve a grid package. If it's NULL then we ran out of space, bark an error.
        GridPackage *rGridPack = GetNextPriorityEntry();
        if(!rGridPack)
        {
            fprintf(stderr, "AdvUICostumes:TakeForeground() - Warning, heading data has %i entries but the grid ran out of slots. Failing.\n", mHeadingData->GetListSize());
            mHeadingData->PopIterator();
            break;
        }

        //--Copy across.
        rGridPack->mCode = i;
        strcpy(rGridPack->mLocalName, rPackage->mDisplayName);
        memcpy(&rGridPack->mAlignments, &rPackage->mAlignments, sizeof(AdvMenuStandardAlignments));

        //--Next.
        i ++;
        rPackage = (CostumeHeadingPack *)mHeadingData->AutoIterate();
    }

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    ///--[Run Cursor]
    //--Update the cursor so it focuses on the selected entry.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}
void AdvUICostumes::AddCostumeHeading(const char *pInternalName, const char *pCharName, const char *pDisplayName, const char *pFormName, const char *pImagePath)
{
    ///--[Documentation]
    //--Adds a new heading to the costume menu. Headings are things like "Mei Alraune" and "Christine Golem", describing
    //  a character and an associated form or class. This can include characters who do not join the party.
    //--The heading automatically creates a new linked-list and associates itself with that list. This new list
    //  stores all the available costumes in that heading.
    if(!pInternalName || !pCharName || !pFormName || !pImagePath) return;

    //--Create the new package.
    SetMemoryData(__FILE__, __LINE__);
    CostumeHeadingPack *nPack = (CostumeHeadingPack *)starmemoryalloc(sizeof(CostumeHeadingPack));
    nPack->Initialize();
    nPack->mAlignments.rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    ResetString(nPack->mName, pCharName);
    ResetString(nPack->mDisplayName, pDisplayName);
    ResetString(nPack->mFormName, pFormName);

    //--Register to the main list.
    mHeadingData->AddElementAsTail(pInternalName, nPack, &CostumeHeadingPack::DeleteThis);
}
void AdvUICostumes::SetCostumeHeadingImageData(const char *pInternalName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei)
{
    ///--[Documentation]
    //--Sets the graphics alignment data for a given entry.

    //--Make sure the heading exists.
    CostumeHeadingPack *rHeading = (CostumeHeadingPack *)mHeadingData->GetElementByName(pInternalName);
    if(!rHeading || !rHeading->mAlignments.rImage) return;

    //--If the image has not resolved yet, it needs to now.
    rHeading->mAlignments.rImage->LoadDataFromSLF();

    //--Set.
    rHeading->mAlignments.mOffsetX = pXOff;
    rHeading->mAlignments.mOffsetY = pYOff;
    rHeading->mAlignments.mImageX  = pLft - rHeading->mAlignments.rImage->GetXOffset();
    rHeading->mAlignments.mImageY  = pTop - rHeading->mAlignments.rImage->GetYOffset();
    rHeading->mAlignments.mImageW  = pWid;
    rHeading->mAlignments.mImageH  = pHei;

    //--Clamp.
    if(rHeading->mAlignments.mImageW < 1.0f) rHeading->mAlignments.mImageW = 1.0f;
    if(rHeading->mAlignments.mImageH < 1.0f) rHeading->mAlignments.mImageH = 1.0f;
}
void AdvUICostumes::AddCostumeEntry(const char *pHeadingName, const char *pCostumeName, const char *pImagePath, const char *pScriptPath)
{
    ///--[Documentation]
    //--Adds a new costume to the named heading. This contains an image to act as a preview, as well as a script to run
    //  if the player selects that costume.
    if(!pHeadingName || !pCostumeName || !pImagePath || !pScriptPath) return;

    //--Make sure the heading exists.
    CostumeHeadingPack *rHeading = (CostumeHeadingPack *)mHeadingData->GetElementByName(pHeadingName);
    if(!rHeading) return;

    //--Create the new package, register it.
    SetMemoryData(__FILE__, __LINE__);
    CostumeEntryPack *nPack = (CostumeEntryPack *)starmemoryalloc(sizeof(CostumeEntryPack));
    nPack->Initialize();
    ResetString(nPack->mExecPath, pScriptPath);
    nPack->mAlignments.rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    rHeading->mSublist->AddElement(pCostumeName, nPack, &CostumeEntryPack::DeleteThis);
}
void AdvUICostumes::SetCostumeEntryImageData(const char *pHeadingName, const char *pInternalName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei)
{
    ///--[Documentation]
    //--Given a heading and an entry name, sets graphics alignment data.

    //--Get the heading.
    CostumeHeadingPack *rHeading = (CostumeHeadingPack *)mHeadingData->GetElementByName(pHeadingName);
    if(!rHeading) return;

    //--Get the entry.
    CostumeEntryPack *rEntry = (CostumeEntryPack *)rHeading->mSublist->GetElementByName(pInternalName);
    if(!rEntry || !rEntry->mAlignments.rImage) return;

    //--If the image has not resolved data yet, do that now.
    rEntry->mAlignments.rImage->LoadDataFromSLF();

    //--Set.
    rEntry->mAlignments.mOffsetX = pXOff;
    rEntry->mAlignments.mOffsetY = pYOff;
    rEntry->mAlignments.mImageX  = pLft - rEntry->mAlignments.rImage->GetXOffset();
    rEntry->mAlignments.mImageY  = pTop - rEntry->mAlignments.rImage->GetYOffset();
    rEntry->mAlignments.mImageW  = pWid;
    rEntry->mAlignments.mImageH  = pHei;

    //--Clamp.
    if(rEntry->mAlignments.mImageW < 1.0f) rEntry->mAlignments.mImageW = 1.0f;
    if(rEntry->mAlignments.mImageH < 1.0f) rEntry->mAlignments.mImageH = 1.0f;
}

///======================================= Core Methods ===========================================
void AdvUICostumes::RefreshMenuHelp()
{

}
void AdvUICostumes::RecomputeCursorPositions()
{
    RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, 1.0f);
}
void AdvUICostumes::ActivateEntriesMode()
{
    ///--[Documentation]
    //--This object contains two grids. When selecting headings, the Entry grid is not in use. When
    //  a heading is selected, we move to Entries mode. This moves the existing grid into the Entry
    //  grid slots, and repopulates the current grid with the entry data.
    if(mIsEntryMode)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    ///--[Entry]
    //--Store the active entry.
    int tSlotOfSelection = GetGridCodeAtCursor();
    CostumeHeadingPack *rSelectedHeading = (CostumeHeadingPack *)mHeadingData->GetElementBySlot(tSlotOfSelection);
    if(!rSelectedHeading || !rSelectedHeading->mSublist || rSelectedHeading->mSublist->GetListSize() < 1)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Set flag.
    mIsEntryMode = true;

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

    ///--[Allocate, Set]
    //--Switch to the Entries grid. This will archive the Base grid for later.
    ActivateStoredGrid("Entries");

    //--Allocate space for the new data.
    rEntryData = rSelectedHeading->mSublist;
    AutoAllocateSquareGrid(rEntryData->GetListSize());
    ProcessGrid();

    //--Iteration loop.
    int i = 0;
    CostumeEntryPack *rPackage = (CostumeEntryPack *)rEntryData->PushIterator();
    while(rPackage)
    {
        //--Retrieve a grid package. If it's NULL then we ran out of space, bark an error.
        GridPackage *rGridPack = GetNextPriorityEntry();
        if(!rGridPack)
        {
            fprintf(stderr, "AdvUICostumes:ActivateEntriesMode() - Warning, heading data has %i entries but the grid ran out of slots. Failing.\n", rEntryData->GetListSize());
            rEntryData->PopIterator();
            break;
        }

        //--Copy across.
        rGridPack->mCode = i;
        strcpy(rGridPack->mLocalName, rEntryData->GetIteratorName());
        memcpy(&rGridPack->mAlignments, &rPackage->mAlignments, sizeof(AdvMenuStandardAlignments));

        //--Next.
        i ++;
        rPackage = (CostumeEntryPack *)rEntryData->AutoIterate();
    }

    ///--[Cursor]
    RecomputeCursorPositions();
}
void AdvUICostumes::DeactivateEntriesMode()
{
    ///--[Documentation]
    //--Called when the player cancels out of Entries mode.
    if(!mIsEntryMode) return;

    ///--[Execution]
    //--Flag.
    mIsEntryMode = false;
    rEntryData = NULL;

    //--Switch back to Base.
    ActivateStoredGrid("Base");

    ///--[Cursor]
    RecomputeCursorPositions();
}
void AdvUICostumes::ResetCostumeImages()
{
    ///--[Documentation]
    //--Run across all headings and entires. Order the bitmaps in them to load their data. This is
    //  because activating a costume handler unloads data and we can't have that.
    CostumeHeadingPack *rHeading = (CostumeHeadingPack *)mHeadingData->PushIterator();
    while(rHeading)
    {
        //--Refresh.
        if(rHeading->mAlignments.rImage) rHeading->mAlignments.rImage->LoadDataFromSLF();

        //--Iterate.
        CostumeEntryPack *rEntry = (CostumeEntryPack *)rHeading->mSublist->PushIterator();
        while(rEntry)
        {
            if(rEntry->mAlignments.rImage) rEntry->mAlignments.rImage->LoadDataFromSLF();
            rEntry = (CostumeEntryPack *)rHeading->mSublist->AutoIterate();
        }

        //--Next.
        rHeading = (CostumeHeadingPack *)mHeadingData->AutoIterate();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUICostumes::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        mVisibilityTimer ++;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--Standard Timers.
    StandardTimer(mEntryTimer,          0, cxAdvVisTicks, mIsEntryMode);
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks, mIsShowingHelp);

    //--Grid timers.
    UpdateGridTimers();

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Set this flag.
    mDontShowHighlight = false;

    ///--[ ======== Update Intercepts ========= ]
    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[ =========== Update Call ============ ]
    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return true;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If not in Entries mode:
        if(!mIsEntryMode)
        {
            ActivateEntriesMode();
        }
        //--In Entries mode:
        else
        {
            //--Get package.
            int tSelectedPack = GetGridCodeAtCursor();
            CostumeEntryPack *rPackage = (CostumeEntryPack *)rEntryData->GetElementBySlot(tSelectedPack);
            if(!rPackage) return false;

            //--Build path.
            char tPath[1024];
            const char *rGamePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
            snprintf(tPath, 1024, "%s/%s", rGamePath, rPackage->mExecPath);

            //--Run it.
            LuaManager::Fetch()->ExecuteLuaFile(tPath);

            //--Refresh images which may unload.
            ResetCostumeImages();

            //--SFX.
            AudioManager::Fetch()->PlaySound("World|TakeItem");
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        //--If not in Entries mode:
        if(!mIsEntryMode)
        {
            FlagExit();
        }
        //--In Entries mode:
        else
        {
            DeactivateEntriesMode();
        }
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update.
    return true;
}
void AdvUICostumes::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;

    //--Run timers.
    mVisibilityTimer --;
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Unset this flag.
    mDontShowHighlight = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUICostumes::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Rendering of the base UI. Mostly follows the standard, however the bottom of the UI that shows
    //  the player's party will continue to render even if this object is offscreen if the Doctor UI
    //  needs it to, which is handled via a static timer.
    StarlightColor::ClearMixer();

    ///--[Timer]
    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Render Grid]
    //--When in the Base mode, the Entries grid needs to render if its timer is nonzero.
    if(!mIsEntryMode)
    {
        //--Compute alphas.
        float cBaseAlpha  = (1.0f - EasingFunction::QuadraticInOut(mEntryTimer, cxAdvVisTicks)) * pVisAlpha;
        float cEntryAlpha = EasingFunction::QuadraticInOut(mEntryTimer, cxAdvVisTicks) * pVisAlpha;

        //--Timer must be over zero or there's nothing to render.
        if(mEntryTimer > 0)
        {
            ActivateStoredGrid("Entries");
            RenderGrid(cEntryAlpha);
            ActivateStoredGrid("Base");
        }

        //--Render the grid normally.
        RenderGrid(cBaseAlpha);
        if(!mDontShowHighlight) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
        RenderGridEntryName(GetPackageAtCursor(), cBaseAlpha);
    }
    //--When in Entries mode, render the Base grid if the timer isn't capped.
    else
    {
        //--Compute alphas.
        float cBaseAlpha  = (1.0f - EasingFunction::QuadraticInOut(mEntryTimer, cxAdvVisTicks)) * pVisAlpha;
        float cEntryAlpha = EasingFunction::QuadraticInOut(mEntryTimer, cxAdvVisTicks) * pVisAlpha;

        //--Render the grid normally.
        RenderGrid(cEntryAlpha);
        if(!mDontShowHighlight) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
        RenderGridEntryName(GetPackageAtCursor(), cEntryAlpha);

        //--Timer must not be capped.
        if(mEntryTimer < cxAdvVisTicks)
        {
            ActivateStoredGrid("Base");
            RenderGrid(cBaseAlpha);
            ActivateStoredGrid("Entries");
        }
    }

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUICostumes::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the header at the top of the screen.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Costume Menu");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, 0, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    AutoPieceClose();
}
void AdvUICostumes::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Execution]
    //--Footer.
    AdvImages.rOverlay_Footer->Draw();

    ///--[Clean]
    glDisable(GL_STENCIL_TEST);
    AutoPieceClose();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
