//--Base
#include "StarLumpManager.h"

//--Classes
//--CoreClasses
#include "StarArray.h"
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define SLF_DEBUG
#ifdef SLF_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
StarLumpManager::StarLumpManager()
{
    ///--[StarLumpManager]
    //--System
    mIsOpen = false;
    mOpenPath = NULL;
    mFileVersion = 0;

    //--File
    mVFile = NULL;

    //--Chunks
    mTotalLumps = 0;
    mLumpList = NULL;

    //--New Lumps
    mNewLumpList = new StarLinkedList(true);

    //--Path Lookups
    rCurrentPathLookup = NULL;
    mPathLookupList = new StarLinkedList(true);

    //--Delayed Bitmap Loading
    mrDelayedBitmapLoadList = new StarLinkedList(false);

    //--Autoload Lump Data
    mAutoLoadLumpData = new StarLinkedList(true);

    //--File Path Data
    mFileIdentities = new StarLinkedList(true);
}
StarLumpManager::~StarLumpManager()
{
    Clear();
    delete mNewLumpList;
    delete mPathLookupList;
    delete mrDelayedBitmapLoadList;
    delete mAutoLoadLumpData;
    delete mFileIdentities;
}
void StarLumpManager::DeleteRootLump(void *pPtr)
{
    RootLump *rPtr = (RootLump *)pPtr;
    free(rPtr->mLookup.mName);
    if(rPtr->mOwnsData) free(rPtr->mData);
    free(rPtr);
}

///--[Public Statics]
//--If true, case-insensitive handlers are used to check for lump names.
bool StarLumpManager::xIsCaseInsensitive = false;

//--Used to toggle on and off debug prints. The #define above must also be set or this does nothing.
bool StarLumpManager::xDebugFlag = false;

//--Sizes for tiles. Default is 16. Used when extracting tile information from SLF level files.
int StarLumpManager::xTileSizeX = DEFAULT_TILE_SIZE;
int StarLumpManager::xTileSizeY = DEFAULT_TILE_SIZE;

///===================================== Property Queries =========================================
bool StarLumpManager::IsFileOpen()
{
    return mIsOpen;
}
char *StarLumpManager::GetOpenPath()
{
    return mOpenPath;
}
int StarLumpManager::GetTotalLumps()
{
    return mTotalLumps;
}
bool StarLumpManager::DoesLumpExist(const char *pName)
{
    if(!pName) return false;
    int tArrayPos = BinaryGetLookup(pName, 0, mTotalLumps-1);
    if(tArrayPos == -1) return false;
    return true;
}
char *StarLumpManager::GetHeaderOf(const char *pName)
{
    //--Note: Can return NULL if there's no lump by that name. The name will otherwise be a heap
    //  copy that must be deleted when you're done with it.
    int tArrayPos = BinaryGetLookup(pName, 0, mTotalLumps-1);
    if(tArrayPos == -1) return NULL;

    //--Store the old position.
    uint32_t tOldPos = mVFile->GetCurPosition();

    //--Seek to this position.
    mVFile->Seek(mLumpList[tArrayPos].mLookup.mPosition);

    //--Read out the Lump header.
    SetMemoryData(__FILE__, __LINE__);
    char *nHeaderString = (char *)starmemoryalloc(sizeof(char) * 11);
    mVFile->Read(nHeaderString, sizeof(char), 10);
    nHeaderString[10] = '\0';

    //--Return the VFile to the original position.
    mVFile->Seek(tOldPos);

    //--Pass back the copied name.
    return nHeaderString;
}
bool StarLumpManager::DoesPathExist(void *pPtr)
{
    return mPathLookupList->IsElementOnList(pPtr);
}
PathLookup *StarLumpManager::GetCurrentPathLookup()
{
    return rCurrentPathLookup;
}
int StarLumpManager::GetSizeOfLoadQueue()
{
    return mrDelayedBitmapLoadList->GetListSize();
}
bool StarLumpManager::IsQueuedForLoading(void *pPtr)
{
    return mrDelayedBitmapLoadList->IsElementOnList(pPtr);
}
int StarLumpManager::GetAutoLoadCount()
{
    return mAutoLoadLumpData->GetListSize();
}
const char *StarLumpManager::GetAutoLoadName(int pSlot)
{
    return mAutoLoadLumpData->GetNameOfElementBySlot(pSlot);
}
const char *StarLumpManager::GetAutoLoadPath(int pSlot)
{
    return (const char *)mAutoLoadLumpData->GetElementBySlot(pSlot);
}
const char *StarLumpManager::GetFilePath(const char *pFileName)
{
    return (const char *)mFileIdentities->GetElementByName(pFileName);
}

///======================================== Manipulators ==========================================
void StarLumpManager::AddBitmapToLoadList(StarBitmap *pImage)
{
    //--If the image is already on the list, forget it.
    if(!pImage || mrDelayedBitmapLoadList->IsElementOnList(pImage)) return;

    //--Add.
    mrDelayedBitmapLoadList->AddElementAsTail("X", pImage);
}
void StarLumpManager::RemoveBitmapFromLoadList(void *pPtr)
{
    //--Remove by pointer.
    mrDelayedBitmapLoadList->RemoveElementP(pPtr);
}
void StarLumpManager::RegisterFileIdentity(const char *pFileName, const char *pFilePath)
{
    ///--[Documentation]
    //--Registers a file name/path pairing. These can be used by the autoloader to load from multiple
    //  files in one sequence, by storing the file identity in the autoloader but leaving the specific
    //  file path to the program.
    if(!pFileName || !pFilePath) return;

    //--If the file identity already exists, overwrite.
    char *rExistingIdentity = (char *)mFileIdentities->GetElementByName(pFileName);
    if(rExistingIdentity)
    {
        mFileIdentities->RemoveElementS(pFileName);
    }

    //--Otherwise, create the identity.
    char *nFilePath = InitializeString(pFilePath);
    mFileIdentities->AddElement(pFileName, nFilePath, &FreeThis);
}
void StarLumpManager::ReplaceLumpWithDummyImage(const char *pLumpName)
{
    ///--[Documentation]
    //--Given a name, and assuming a file is open, locates the given lump and replaces its data with
    //  a dummy image lump. Dummy images contain a minimum of data to reduce file sizes for images
    //  that are unused due to modding or override games.
    if(!pLumpName) return;

    ///--[Execution]
    //--First, locate the lump.
    int tArrayPos = BinaryGetLookup(pLumpName, 0, mTotalLumps-1);
    if(tArrayPos == -1)
    {
        fprintf(stderr, "StarLumpManager:ReplaceLumpWithDummyImage() - Warning. Lump %s was not found.\n", pLumpName);
        return;
    }

    //--Clear and reallocate.
    if(mLumpList[tArrayPos].mOwnsData)
    {
        //--Simple array:
        if(!mLumpList[tArrayPos].mUsesStarArray)
        {
            //--Deallocate.
            free(mLumpList[tArrayPos].mData);
            mLumpList[tArrayPos].mData = NULL;
        }
        //--Array object type.
        else
        {
            mLumpList[tArrayPos].mUsesStarArray = false;
            delete mLumpList[tArrayPos].rDataObject;
            mLumpList[tArrayPos].rDataObject = NULL;
        }
    }
    //--Clear.
    else
    {
        mLumpList[tArrayPos].mUsesStarArray = false;
        mLumpList[tArrayPos].mData = NULL;
        mLumpList[tArrayPos].rDataObject = NULL;
    }

    //--Create new data. Give it the dummy compression type.
    mLumpList[tArrayPos].mOwnsData = true;
    mLumpList[tArrayPos].mDataSize = 11;
    mLumpList[tArrayPos].mData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * 11);
    mLumpList[tArrayPos].mData[ 0] = 'I';
    mLumpList[tArrayPos].mData[ 1] = 'M';
    mLumpList[tArrayPos].mData[ 2] = 'A';
    mLumpList[tArrayPos].mData[ 3] = 'G';
    mLumpList[tArrayPos].mData[ 4] = 'E';
    mLumpList[tArrayPos].mData[ 5] = '0';
    mLumpList[tArrayPos].mData[ 6] = '0';
    mLumpList[tArrayPos].mData[ 7] = '0';
    mLumpList[tArrayPos].mData[ 8] = '0';
    mLumpList[tArrayPos].mData[ 9] = '0';
    mLumpList[tArrayPos].mData[10] = StarBitmap::cDummyCompression;
}

///======================================== Core Methods ==========================================
void StarLumpManager::PopulateAutoLoad(const char *pLumpName)
{
    ///--[Documentation]
    //--Given the name of an autoload lump, finds it and then retrieves all the data. The information
    //  can then be queried from mAutoLoadLumpData, which can be used to load the given data, print it,
    //  issue delayed loading instructions, etc.
    //--If the lump isn't found, the auto-load data gets cleared anyway.
    if(!mIsOpen || !pLumpName) return;

    ///--[Setup]
    //--Clear existing auto-load data if any is present.
    mAutoLoadLumpData->ClearList();

    //--Locate it and seek to it.
    if(!StandardSeek(pLumpName, "AUTOLOAD"))
    {
        if(!mSuppressErrorOnce) DebugManager::ForcePrint("Error in SLF file. No auto-load lump named %s found.\n", pLumpName);
        mSuppressErrorOnce = false;
        return;
    }

    ///--[Header]
    //--Read the lump's size. Only used for diagnostics.
    uint32_t tLumpSize = 0;
    mVFile->Read(&tLumpSize, sizeof(uint32_t), 1);

    //--Read how many entries are in the lump.
    int32_t tEntries = 0;
    mVFile->Read(&tEntries, sizeof(int32_t), 1);

    ///--[Entries]
    //--Store this flag. This always uses 2 bytes for string length.
    bool tOldFlag = mVFile->GetOneByteForLengthsFlag();
    mVFile->SetUseOneByteForStringLengths(false);

    //--Begin reading entries.
    for(int i = 0; i < tEntries; i ++)
    {
        //--Read names.
        char *nBitmapName = mVFile->ReadLenString();
        char *nDLPath = mVFile->ReadLenString();

        //--Store them.
        mAutoLoadLumpData->AddElementAsTail(nBitmapName, nDLPath, &FreeThis);

        //--Free the bitmap name, it got copied.
        free(nBitmapName);
    }

    //--Unset flag.
    mVFile->SetUseOneByteForStringLengths(tOldFlag);
}
bool StarLumpManager::LoadNextDelayedBitmap()
{
    //--Loads the next bitmap in the list of delayed bitmaps. Returns true if anything was loaded,
    //  false if the list was empty.
    StarBitmap *rZeroBitmap = (StarBitmap *)mrDelayedBitmapLoadList->GetHead();
    if(!rZeroBitmap) return false;

    //--Bitmap exists, order it to load its memory and remove it.
    rZeroBitmap->LoadDataFromSLF();
    mrDelayedBitmapLoadList->DeleteHead();

    //--Return true to indicate we did something.
    return true;
}
int StarLumpManager::BinaryGetLookup(const char *pName, int pMin, int pMax)
{
    ///--[Documentation]
    //--Does a binary search on the mLumpList array.  This is a recursive call, and therefore does
    //  less NULL checking than would be necessary.

    //--Array is empty
    if(pMax < pMin)
    {
        return -1;
    }

    //--Find the halfway point, cut the set in half
    int tMid = (pMin + pMax) / 2;

    //--Check the given position.
    const char *rCheckName = mLumpList[tMid].mLookup.mName;

    //--Case sensitive:
    int tRet = 0;
    if(!xIsCaseInsensitive)
    {
        tRet = strcmp(pName, rCheckName);
    }
    else
    {
        tRet = strcasecmp(pName, rCheckName);
    }

    //--After Midpoint
    if(tRet > 0)
    {
        return BinaryGetLookup(pName, tMid+1, pMax);
    }
    //--Before midpoint
    else if(tRet < 0)
    {
        return BinaryGetLookup(pName, pMin, tMid-1);
    }
    //--Match
    else
    {
        return tMid;
    }
    return -1;
}
bool StarLumpManager::StandardSeek(const char *pLumpName, const char *pHeaderType)
{
    ///--[Documentation]
    //--Locates the named Lump, and checks if it is of the named Type. If all went well, returns
    //  true, false on error. The VFile is left at the end of the header.
    if(!mIsOpen || !pLumpName || !pHeaderType) return false;
    DebugManager::PushPrint(false, "Seeking to %s\n", pLumpName);

    //--Locate the Lump. We can use a binary search since the headers are sorted.
    int tArrayPos = BinaryGetLookup(pLumpName, 0, mTotalLumps-1);
    if(tArrayPos == -1)
    {
        DebugManager::PopPrint("Lump wasn't found\n");
        return false;
    }

    //--Seek to the location in the file.
    DebugManager::Print("Slot %i\n", tArrayPos);
    DebugManager::Print("Position %i\n", mLumpList[tArrayPos].mLookup.mPosition);
    mVFile->Seek(mLumpList[tArrayPos].mLookup.mPosition);

    //--Read out the Lump header.
    char tHeader[11];
    mVFile->Read(tHeader, sizeof(char), 10);
    tHeader[10] = '\0';
    DebugManager::Print("Check %s vs %s\n", tHeader, pHeaderType);
    if(strncmp(tHeader, pHeaderType, strlen(pHeaderType)))
    {
        DebugManager::PopPrint("Lump was of the wrong type\n");
        return false;
    }
    DebugManager::PopPrint("Finished, Header was %s\n", tHeader);
    return true;
}
bool StarLumpManager::SlotwiseSeek(int pSlot)
{
    ///--[Documentation]
    //--Seeks to a specific slot, instead of doing a binary search. The type is not checked, but
    //  the header is still seeked past. Returns true on success, false on error.
    if(pSlot < 0 || pSlot >= (int)mTotalLumps) return false;

    //--Seek.
    mVFile->Seek(mLumpList[pSlot].mLookup.mPosition);

    //--Read out the Lump header.
    char tHeader[11];
    mVFile->Read(tHeader, sizeof(char), 10);
    tHeader[10] = '\0';

    //--Done.
    return true;
}
void StarLumpManager::Clear()
{
    ///--[Delete]
    //--Clears all existing data and resets back to factory-zero.
    DebugPush(xDebugFlag, "Clearing all data from SLM.\n");
    free(mOpenPath);
    delete mVFile;

    //--Lumps
    DebugPrint("Clearing lump info.\n");
    for(uint32_t i = 0; i < mTotalLumps; i ++)
    {
        free(mLumpList[i].mLookup.mName);
        if(mLumpList[i].mOwnsData)
        {
            free(mLumpList[i].mData);
            delete mLumpList[i].rDataObject;
        }
    }
    free(mLumpList);

    //--New Lumps
    DebugPrint("Clearing new lump list.\n");
    mNewLumpList->ClearList();

    ///--[Initialize]
    //--System
    DebugPrint("Re-nulling system variables.\n");
    mIsOpen = false;
    mOpenPath = NULL;
    mFileVersion = 0;

    //--File
    mVFile = NULL;

    //--Lumps
    mTotalLumps = 0;
    mLumpList = NULL;
    DebugPop("Finished clearing SLM data.\n");
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void StarLumpManager::Open(const char *pPath)
{
    //--Overload for the Open() function below. This assumes the standard header is in use.
    Open(pPath, STANDARD_HEADER);
}
void StarLumpManager::Open(const char *pPath, const char *pHeader)
{
    ///--[Documentation and Setup]
    //--Opens a file, parses out all its data and stores it in the list and header.
    DebugPush(xDebugFlag, "==> StarLumpManager: Opening file.\n");
    if(!pPath)
    {
        mSuppressErrorOnce = false;
        DebugManager::ForcePrint("No path provided, exiting.\n");
        DebugPop("");
        return;
    }
    DebugPrint("File was %s\n", pPath);

    //--Was the path identical to the last opened SLF? Then it's already open!
    if(mOpenPath && !strcmp(pPath, mOpenPath))
    {
        mSuppressErrorOnce = false;
        DebugPop("File was already open, exiting.\n");
        return;
    }
    DebugPrint("Closing previous file.\n");
    if(mOpenPath)
    {
        DebugPrint(" Previous file was %s\n", mOpenPath);
    }
    Close();
    DebugPrint("Finished closing previous file.\n");

    //--Virtual file, should specify the loading flag as virtual or real depending on how much of
    //  the file you intend to read.
    DebugPrint("Opening virtual file.\n");
    mIsOpen = true;
    mVFile = new VirtualFile(pPath, false);
    if(!mVFile->IsReady())
    {
        if(!mSuppressErrorOnce) DebugManager::ForcePrint("StarLumpManager:  Error, could not find %s\n", pPath);
        mSuppressErrorOnce = false;
        Close();
        DebugPop("Could not find file.\n");
        return;
    }

    //--Store the path
    DebugPrint("Storing path.\n");
    ResetString(mOpenPath, pPath);

    //--Check the path lookups. If this path is not in the lookups, add it. Otherwise, set the current lookup.
    rCurrentPathLookup = NULL;
    PathLookup *rPathLookup = (PathLookup *)mPathLookupList->PushIterator();
    while(rPathLookup)
    {
        //--Is this a match? If so, set this as the current path.
        if(!strcasecmp(rPathLookup->mPath, mOpenPath))
        {
            //fprintf(stderr, "Using existing lookup %p\n", rPathLookup);
            rCurrentPathLookup = rPathLookup;
            mPathLookupList->PopIterator();
            break;
        }

        rPathLookup = (PathLookup *)mPathLookupList->AutoIterate();
    }

    //--If no path lookup was set, create a new one.
    if(!rCurrentPathLookup)
    {
        PathLookup *nLookup = (PathLookup *)starmemoryalloc(sizeof(PathLookup));
        nLookup->Initialize();
        nLookup->mPath = InitializeString(mOpenPath);
        mPathLookupList->AddElementAsTail("X", nLookup, PathLookup::DeleteThis);
        rCurrentPathLookup = nLookup;
        //fprintf(stderr, "Created new path lookup %p %s\n", nLookup, mOpenPath);
    }

    //--Header. Do not read more characters than the expected maximum!
    DebugPrint("Checking header.\n");
    int tCharsToRead = (int)strlen(pHeader);
    if(tCharsToRead >= HEADER_MAX_LEN) tCharsToRead = HEADER_MAX_LEN - 1;

    //--Parse the header into the buffer.
    DebugPrint("Reading header %i.\n", tCharsToRead);
    mVFile->mDebugFlag = xDebugFlag;
    mVFile->Read(mHeader, sizeof(char), tCharsToRead);
    DebugPrint("Finished reading header.\n", tCharsToRead);
    mVFile->mDebugFlag = false;
    mHeader[tCharsToRead] = '\0';

    //--Compare the received header against the expected one. If they don't match, this file is not
    //  the right type.
    DebugPrint("Comparing header.\n");
    if(strcmp(mHeader, pHeader))
    {
        if(!mSuppressErrorOnce) DebugManager::ForcePrint("Error, file header %s does not match expected %s\n", mHeader, pHeader);
        mSuppressErrorOnce = false;
        Close();
        DebugPop("File had unexpected header.\n");
        return;
    }

    //--Total number of Lumps
    DebugPrint("Booting lumps.\n");
    mVFile->Read(&mTotalLumps, sizeof(uint32_t), 1);
    SetMemoryData(__FILE__, __LINE__);
    mLumpList = (RootLump *)starmemoryalloc(sizeof(RootLump) * mTotalLumps);
    for(int i = 0; i < (int)mTotalLumps; i ++)
    {
        mLumpList[i].Init();
    }

    //--Read each of the lookups. Set the data pointers for the lumps to these lookups. This is faster
    //  when in memory mode, but not mandated.
    DebugPrint("Reading lumps.\n");
    if(mVFile->IsInMemoryMode())
    {
        //--Get the master data pointer.
        //uint8_t *rMasterDataPtr = mVFile->GetData();
        StarArray *rMasterDataPtr = mVFile->GetData();

        //--For each lump, copy it from the file.
        uint8_t tLen = 0;
        for(uint32_t i = 0; i < mTotalLumps; i ++)
        {
            //--These lumps do not own their own data, the virtual file does.
            mLumpList[i].mOwnsData = false;

            //--Read the length of the name.
            mVFile->Read(&tLen, sizeof(uint8_t), 1);

            //--Clear the existing name. It defaults to "Unnamed Lump" and needs to be reallocated.
            free(mLumpList[i].mLookup.mName);
            mLumpList[i].mLookup.mName = NULL;

            //--Read the name, store it, and add a null.
            SetMemoryData(__FILE__, __LINE__);
            mLumpList[i].mLookup.mName = (char *)starmemoryalloc(sizeof(char) * (tLen+1));
            mVFile->Read(mLumpList[i].mLookup.mName, sizeof(char), tLen);
            mLumpList[i].mLookup.mName[tLen] = '\0';

            //--Get the position and set the data pointer.
            mVFile->Read(&mLumpList[i].mLookup.mPosition, sizeof(uint64_t), 1);
            //mLumpList[i].mData = &rMasterDataPtr[mLumpList[i].mLookup.mPosition];

            //--Store the data object reference in the lump. It has a lifetime equal to the file it came from.
            mLumpList[i].mData = NULL;
            mLumpList[i].mUsesStarArray = true;
            mLumpList[i].rDataObject = rMasterDataPtr;
            mLumpList[i].mStarArrayOffset = mLumpList[i].mLookup.mPosition;
        }
    }
    //--When in stream mode, the lump takes ownership but does not read its data yet, because we don't
    //  know how large the lump is.
    else
    {
        //--Setup.
        uint8_t tLen = 0;

        //--Read.
        for(uint32_t i = 0; i < mTotalLumps; i ++)
        {
            //--The lumps own their own data.
            mLumpList[i].mOwnsData = true;

            //--Read the length of the name.
            mVFile->Read(&tLen, sizeof(uint8_t), 1);

            //--Read the name, store it, and add a null.
            SetMemoryData(__FILE__, __LINE__);
            mLumpList[i].mLookup.mName = (char *)starmemoryalloc(sizeof(char) * (tLen+1));
            mVFile->Read(mLumpList[i].mLookup.mName, sizeof(char), tLen);
            mLumpList[i].mLookup.mName[tLen] = '\0';

            //--Get the position.
            mVFile->Read(&mLumpList[i].mLookup.mPosition, sizeof(uint64_t), 1);

            //--Mark the lump as having no data - yet.
            mLumpList[i].mData = NULL;
        }
    }

    //--Determine how many bytes each lump should be.
    DebugPrint("Indexing lumps.\n");
    for(uint32_t i = 0; i < mTotalLumps-1; i ++)
    {
        //--The size of a lump is the next lump's position minus this one. Lumps are always space-filling.
        mLumpList[i].mDataSize = mLumpList[i+1].mLookup.mPosition -  mLumpList[i+0].mLookup.mPosition;
        //fprintf(stderr, "Lump %i %i\n", i, mLumpList[i].mDataSize);

        //--In stream mode, we need to get the lump's data here.
        if(!mVFile->IsInMemoryMode())
        {
            //--Seek to the lump's memory position.
            mVFile->Seek(mLumpList[i].mLookup.mPosition);

            //--Read it.
            mVFile->Read(mLumpList[i].mData, mLumpList[i].mDataSize, 1);
        }
    }

    //--Last lump uses the end-of-file for its size.
    DebugPrint("Setting EOF.\n");
    if(mTotalLumps > 0)
    {
        mLumpList[mTotalLumps-1].mDataSize = mVFile->GetLength() -  mLumpList[mTotalLumps-1].mLookup.mPosition - 1;
        //fprintf(stderr, "Lump %i %i\n", mTotalLumps-1, mLumpList[mTotalLumps-1].mDataSize);
    }

    //--Clear flags.
    mSuppressErrorOnce = false;
    DebugPop("Finished opening file.\n");
}
void StarLumpManager::Close()
{
    //--Closes and clears everything, resetting to a neutral state. Basically identical to close.
    //  Note that this can therefore be used after Write() to clear the data.
    Clear();
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
VirtualFile *StarLumpManager::GetVirtualFile()
{
    return mVFile;
}
RootLump *StarLumpManager::GetLumpList()
{
    return mLumpList;
}

///===================================== Static Functions =========================================
#include "Global.h"
StarLumpManager *StarLumpManager::Fetch()
{
    return Global::Shared()->gStarLumpManager;
}
RootLump *StarLumpManager::CreateLump(const char *pName, const char *pHeader, uint32_t pExpectedSize)
{
    //--Creates and returns a RootLump with the given name and expected size. Headers are usually
    //  exactly 10 bytes with no closing null.
    if(!pName || !pHeader || pExpectedSize < 10 || pExpectedSize < strlen(pHeader)) return NULL;

    //--Subroutine.
    RootLump *nLump = StarLumpManager::CreateLump(pName);

    //--Allocate. The lump now owns the given data, though the space is empty.
    nLump->mOwnsData = true;
    nLump->mDataSize = pExpectedSize;
    SetMemoryData(__FILE__, __LINE__);
    nLump->mData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * nLump->mDataSize);
    memcpy(nLump->mData, pHeader, (int)strlen(pHeader) * sizeof(char));

    //--Done.
    return nLump;
}
RootLump *StarLumpManager::CreateLump(const char *pName)
{
    //--Creates and returns a RootLump with nullified data. You must provide data before sending it
    //  off for use. Good if you don't know how big the lump is going to be.
    //--The flag mOwnsData is used to determine if this lump should deallocate its data block when
    //  it gets deallocated. If it's referencing something else, leave it false.
    if(!pName) return NULL;

    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    RootLump *nLump = (RootLump *)starmemoryalloc(sizeof(RootLump));
    nLump->Init();
    ResetString(nLump->mLookup.mName, pName);

    //--Done.
    return nLump;
}

///======================================== Lua Hooking ===========================================
void StarLumpManager::HookToLuaState(lua_State *pLuaState)
{
    /* SLF_SetDebugFlag(bFlag)
       Sets the debug flag on or off. Debug shows prints when opening files. */
    lua_register(pLuaState, "SLF_SetDebugFlag", &Hook_SLF_SetDebugFlag);

    /* SLF_Open(sPath[])
       Opens the specified StarLumpFile.  All read operations will fail if a file is not open.
       Opening multiple files concurrently is not supported, and opening a new file will close the
       other one, if there was one.*/
    lua_register(pLuaState, "SLF_Open", &Hook_SLF_Open);

    /* SLF_Close()
       Closes and deallocated the opened SLF file.  Does nothing if one wasn't open. */
    lua_register(pLuaState, "SLF_Close", &Hook_SLF_Close);

    /* SLF_IsFileOpen()
       Returns true if a file is currently open in the SLF, false if not. */
    lua_register(pLuaState, "SLF_IsFileOpen", &Hook_SLF_IsFileOpen);

    /* SLF_GetOpenPath()
       Returns the path of the currently open file.  Returns "NULL" on failure. */
    lua_register(pLuaState, "SLF_GetOpenPath", &Hook_SLF_GetOpenPath);

    /* SLF_PopulateAutoLoad(sLumpName)
       Populates the global array with auto-load information. */
    lua_register(pLuaState, "SLF_PopulateAutoLoad", &Hook_SLF_PopulateAutoLoad);

    /* SLF_GetAutoLoadCount() (1 Integer)
       Returns how many auto-load entries there were on the last populated lump. */
    lua_register(pLuaState, "SLF_GetAutoLoadCount", &Hook_SLF_GetAutoLoadCount);

    /* SLF_GetAutoLoadEntry(iSlot) (2 Strings)
       Returns the name/path of the auto-load entry in the given slot. */
    lua_register(pLuaState, "SLF_GetAutoLoadEntry", &Hook_SLF_GetAutoLoadEntry);

    /* SLF_RegisterFileIdentity(sIdentityName, sFilePath)
       Registers a file path to the SLM. Used so the autoloader can switch files during loading. */
    lua_register(pLuaState, "SLF_RegisterFileIdentity", &Hook_SLF_RegisterFileIdentity);

    /* SLF_GetFilePath(sIdentityName)
       Returns a file path previously registered with SLF_RegisterFileIdentity(). */
    lua_register(pLuaState, "SLF_GetFilePath", &Hook_SLF_GetFilePath);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_SLF_SetDebugFlag(lua_State *L)
{
    //SLF_SetDebugFlag(bFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SLF_SetDebugFlag");
    StarLumpManager::xDebugFlag = lua_toboolean(L, 1);
    return 0;
}
int Hook_SLF_Open(lua_State *L)
{
    //SLF_Open(sPath[])
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SLF_Open");

    StarLumpManager::Fetch()->Open(lua_tostring(L, 1));

    return 0;
}
int Hook_SLF_Close(lua_State *L)
{
    //SLF_Close()

    StarLumpManager::Fetch()->Close();

    return 0;
}
int Hook_SLF_IsFileOpen(lua_State *L)
{
    //SLF_IsFileOpen()
    lua_pushboolean(L, StarLumpManager::Fetch()->IsFileOpen());
    return 1;
}
int Hook_SLF_GetOpenPath(lua_State *L)
{
    //SLF_GetOpenPath()
    char *rPath = StarLumpManager::Fetch()->GetOpenPath();
    if(!rPath)
    {
        lua_pushstring(L, "NULL");
    }
    else
    {
        lua_pushstring(L, rPath);
    }
    return 1;
}
int Hook_SLF_PopulateAutoLoad(lua_State *L)
{
    //SLF_PopulateAutoLoad(sLumpName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SLF_PopulateAutoLoad");

    StarLumpManager::Fetch()->PopulateAutoLoad(lua_tostring(L, 1));
    return 0;
}
int Hook_SLF_GetAutoLoadCount(lua_State *L)
{
    //SLF_GetAutoLoadCount() (1 Integer)
    lua_pushinteger(L, StarLumpManager::Fetch()->GetAutoLoadCount());
    return 1;
}
int Hook_SLF_GetAutoLoadEntry(lua_State *L)
{
    //SLF_GetAutoLoadEntry(iSlot) (2 Strings)
    int pArgs = lua_gettop(L);
    if(pArgs < 1)
    {
        LuaArgError("SLF_GetAutoLoadEntry");
        lua_pushstring(L, "Null");
        lua_pushstring(L, "Null");
        return 2;
    }

    //--Retrieve.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    const char *rNamePtr = rSLM->GetAutoLoadName(lua_tointeger(L, 1));
    const char *rPathPtr = rSLM->GetAutoLoadPath(lua_tointeger(L, 1));

    //--Error case.
    if(!rNamePtr || !rPathPtr)
    {
        lua_pushstring(L, "Null");
        lua_pushstring(L, "Null");
        return 2;
    }

    //--Success.
    lua_pushstring(L, rNamePtr);
    lua_pushstring(L, rPathPtr);
    return 2;
}
int Hook_SLF_RegisterFileIdentity(lua_State *L)
{
    //SLF_RegisterFileIdentity(sIdentityName, sFilePath)
    int pArgs = lua_gettop(L);
    if(pArgs < 2) { LuaArgError("SLF_RegisterFileIdentity"); return 0; }

    //--Set.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    rSLM->RegisterFileIdentity(lua_tostring(L, 1), lua_tostring(L, 2));
    return 0;
}
int Hook_SLF_GetFilePath(lua_State *L)
{
    //SLF_GetFilePath(sIdentityName)
    int pArgs = lua_gettop(L);
    if(pArgs < 1) { LuaArgError("SLF_GetFilePath"); lua_pushstring(L, "Null"); return 1; }

    //--Get.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    const char *rFilePath = rSLM->GetFilePath(lua_tostring(L, 1));

    //--No path:
    if(!rFilePath)
    {
        lua_pushstring(L, "Null");
    }
    //--Valid.
    else
    {
         lua_pushstring(L, rFilePath);
    }

    return 1;
}
