//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdventureLight.h"
#include "AdventureMenu.h"
#include "TileLayer.h"
#include "TilemapActor.h"
#include "RayCollisionEngine.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"

bool AdventureLevel::ActivateAt(int pX, int pY)
{
    ///--[Documentation and Setup]
    //--Performs an activation, which consists of things like searching, opening doors, talking to NPCs,
    //  and sometimes teleporting or moving around. This is usually called by an Actor externally.
    //--It is legal to call an activation at negative coordinates (in theory). Activations do nothing
    //  at all if they fail.
    //--Returns true if something was activated, false otherwise.
    bool tDidAnything = false;

    //--Some objects are per-tile. Store that.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    ///--[Doors]
    //--Check doors.
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rDoorPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rDoorPackage->mY / TileLayer::cxSizePerTile);

        //--If this door is acting as a transition exit...
        if(rDoorPackage->mLevelTransitOnOpen && rDoorPackage->mLevelTransitExit && tTileY == tExamineY && ((tTileX == tExamineX) || (rDoorPackage->mIsWide && tTileX - 1 == tExamineX)))
        {
            //--If the player character is looking straight west or east, do nothing.
            TilemapActor *rPlayerActor = LocatePlayerActor();
            if(rPlayerActor && (rPlayerActor->GetFacing() == TA_DIR_EAST || rPlayerActor->GetFacing() == TA_DIR_WEST))
            {
            }
            //--Execute transition.
            else
            {
                tDidAnything = true;
                SetTransitionDestination(rDoorPackage->mLevelTransitOnOpen, rDoorPackage->mLevelTransitExit);

                //--Space Doors use a different sound.
                if(rDoorPackage->mUseSpaceSound)
                {
                    AudioManager::Fetch()->PlaySound("World|AutoDoorOpen");
                }
                //--Normal case.
                else
                {
                    AudioManager::Fetch()->PlaySound("World|FlipSwitch");
                }
            }
        }
        //--Door must not already be open, and be at the activation position.
        else if(!rDoorPackage->mIsOpened && tTileY == tExamineY && ((tTileX == tExamineX) || (tTileX - 1 == tExamineX && rDoorPackage->mIsWide)))
        {
            //--If the door is script locked, it will not open here.
            if(!rDoorPackage->mIsScriptLocked)
            {
                //--Flags.
                tDidAnything = true;
                rDoorPackage->mIsOpened = true;

                //--Remove it from the RCEs.
                int tDoorSlot = mDoorList->GetSlotOfElementByPtr(rDoorPackage);
                char tNameBuffer[STD_MAX_LETTERS];
                for(int i = 0; i < 4; i ++)
                {
                    sprintf(tNameBuffer, "%i|%i", tDoorSlot, i);
                    for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
                    {
                        if(!mRayCollisionEngines[p]) continue;
                        mRayCollisionEngines[p]->UnsetVariableLine(tNameBuffer);
                    }
                }

                //--Space Doors use a different sound.
                if(rDoorPackage->mUseSpaceSound)
                {
                    AudioManager::Fetch()->PlaySound("World|AutoDoorOpen");
                }
                //--Normal case.
                else
                {
                    AudioManager::Fetch()->PlaySound("World|FlipSwitch");
                }

            }

            //--If the door has an examination case, it gets called here. It may or may not have opened.
            //  The examinable script has to be set!
            if(rDoorPackage->mExamineScript)
            {
                tDidAnything = true;
                LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", rDoorPackage->mExamineScript);
            }
        }

        //--Next.
        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }

    ///--[Chests]
    //--Variable for tracking green chest openings.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rGreenChestVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Chests/Random/iTotalGreensOpened");

    //--Check chests.
    bool tOpenedAChest = false;
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rChestPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rChestPackage->mY / TileLayer::cxSizePerTile);

        //--Chest must not already be open, and be at the activation position.
        if(!rChestPackage->mIsOpened && tTileY == tExamineY && tTileX == tExamineX)
        {
            //--Setup.
            SysVar *rVariable = NULL;

            //--If the chest is script locked, it will not open here.
            if(!rChestPackage->mIsScriptLocked)
            {
                //--Flags.
                tDidAnything = true;
                tOpenedAChest= true;
                rChestPackage->mIsOpened = true;

                //--Create a variable in the DataLibrary. The path should already exist. This does not happen
                //  if this is a randomly-generated level.
                if(strcasecmp(mName, "RandomLevel"))
                {
                    //--Setup.
                    char tBuffer[128];
                    sprintf(tBuffer, "Root/Variables/Chests/%s/%s", mName, mChestList->GetIteratorName());

                    //--It is possible the variable already exists. If it does not, create and set it.
                    rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry(tBuffer);
                    if(!rVariable)
                    {
                        //--Set.
                        SetMemoryData(__FILE__, __LINE__);
                        rVariable = (SysVar *)starmemoryalloc(sizeof(SysVar));
                        rVariable->mIsSaved = true;
                        rVariable->mNumeric = AL_CHEST_OPEN_NORMAL;
                        SetMemoryData(__FILE__, __LINE__);
                        rVariable->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen("NULL") + 1));
                        strcpy(rVariable->mAlpha, "NULL");

                        //--Store.
                        DataLibrary::Fetch()->RegisterPointer(tBuffer, rVariable, &DataLibrary::DeleteSysVar);
                    }

                    //--Now that we have the variable, we may need to do special things if the chest is respawnable.
                    //  Non-respawning chests never do anything interesting and sit at AL_CHEST_OPEN_NORMAL forever.
                    if(rVariable && rChestPackage->mCanRespawn)
                    {
                        //--This is the first time the chest was picked up, and the variable is at AL_CHEST_OPEN_NORMAL.
                        //  Switch it to AL_CHEST_OPEN_NORMAL_PENDING so it appears normal until the player rests.
                        if(rVariable->mNumeric == AL_CHEST_OPEN_NORMAL)
                        {
                            rVariable->mNumeric = AL_CHEST_OPEN_NORMAL_PENDING;
                        }
                        //--If the variable's value was AL_CHEST_CLOSED_RESPAWN, it is a green chest that already respawned
                        //  at least once. Flag it. Add one to the global green chest count.
                        else if(rVariable->mNumeric == AL_CHEST_CLOSED_RESPAWN)
                        {
                            rVariable->mNumeric = AL_CHEST_OPEN_RESPAWN;
                            if(rGreenChestVar) rGreenChestVar->mNumeric = rGreenChestVar->mNumeric + 1.0f;
                        }
                    }
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("World|FlipSwitch");
            }

            //--Contents. If this is set, the chest contains something and it will print a message.
            //  If the examine script is set, no message is printed.
            if(rChestPackage->mContents)
            {
                //--Special: Catalysts.
                if(!strncasecmp(rChestPackage->mContents, "CATALYST|", 9))
                {
                    //--Store chest location.
                    mLastChestX = rChestPackage->mX;
                    mLastChestY = rChestPackage->mY;

                    //--Run the Catalyst handler. This bypasses the item handler!
                    LuaManager::Fetch()->ExecuteLuaFile(xCatalystHandlerPath, 1, "S", &rChestPackage->mContents[9]);
                }
                //--Non-catalysts:
                else
                {
                    ///--[Resolve Item]
                    //--Setup.
                    const char *rUseContents = rChestPackage->mContents;

                    //--If the sysvar does not exist, then use whatever the contents are.
                    if(!rVariable)
                    {
                    }
                    //--If the sysvar exists and is AL_CHEST_OPEN_NORMAL or AL_CHEST_OPEN_NORMAL_PENDING, this is the base
                    //  contents of the chest. No scripts called.
                    else if(rVariable->mNumeric == AL_CHEST_OPEN_NORMAL || rVariable->mNumeric == AL_CHEST_OPEN_NORMAL_PENDING)
                    {
                    }
                    //--If the sysvar is either of the random values, we need to do extra work. A call script is needed, or else
                    //  the script just gives the same item repeatedly.
                    else if(rChestPackage->mRandomScript)
                    {
                        //--Reset the random result variable.
                        ResetString(xRandomChestResult, "Null");

                        //--Assemble the path to the script. It is relative to the root directory.
                        char tBuffer[512];
                        const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
                        sprintf(tBuffer, "%s/%s", rAdventurePath, rChestPackage->mRandomScript);
                        LuaManager::Fetch()->ExecuteLuaFile(tBuffer, 2, "S", mName, "S", mChestList->GetIteratorName());

                        //--Set to the script result.
                        rUseContents = xRandomChestResult;
                    }
                    //--If there's no random script provided, but there is a keyword, use that instead and call a standard script.
                    else if(rChestPackage->mKeyword && xChestScript)
                    {
                        ResetString(xRandomChestResult, "Null");
                        LuaManager::Fetch()->ExecuteLuaFile(xChestScript, 1, "S", rChestPackage->mKeyword);
                        rUseContents = xRandomChestResult;
                    }

                    //--Add the item to the inventory. This is done by calling the inventory script.
                    if(xItemListPath)
                    {
                        LuaManager::Fetch()->ExecuteLuaFile(xItemListPath, 1, "S", rUseContents);
                    }

                    //--If there is no examinable script, start basic dialogue and print what we found.
                    if(!rChestPackage->mExamineScript)
                    {
                        //--Notifications system:
                        if(OptionsManager::Fetch()->GetOptionB("Use Chest Notifications"))
                        {
                            HandleNotificationForItem(false, rUseContents, NormalString.str.mTextReceived);
                        }
                        //--Dialogue system:
                        else
                        {
                            //--Build the string all at once.
                            char tBuffer[STD_MAX_LETTERS];
                            strcpy(tBuffer, NormalString.str.mTextReceived);
                            strcat(tBuffer, rUseContents);
                            strcat(tBuffer, ".");

                            //--Dialogue.
                            WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
                            rWorldDialogue->Show();
                            rWorldDialogue->SetMajorSequence(false);
                            rWorldDialogue->AppendString(tBuffer, false);
                            rWorldDialogue->SetSilenceFlag(true);
                        }
                    }
                }
            }

            //--If the chest has an examination case, it gets called here. It may or may not have opened.
            //  The examinable script has to be set!
            if(rChestPackage->mExamineScript)
            {
                tDidAnything = true;
                LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", rChestPackage->mExamineScript);
            }
        }

        //--Next.
        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--If any chest opened, call this script. This will allow the scripts to respond, which is usually
    //  an updating of map pins if a catalyst was picked up.
    if(tOpenedAChest && xChestScript)
    {
        LuaManager::Fetch()->ExecuteLuaFile(xChestScript, 1, "S", "OPENED CHEST");
    }

    ///--[NPCs/Mugging]
    //--Check NPCs. If they have activation scripts, run that here.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Check.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            if(rActor->HandleActivation((int)pX, (int)pY))
            {
                tDidAnything = true;
                rEntityList->PopIterator();
                break;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--If anything has occured so far, stop here.
    if(tDidAnything) return tDidAnything;

    ///--[Examinable]
    //--Check examinable points. This cannot fire if the examination script was not set.
    if(mExaminableScript)
    {
        //--Iterate.
        ExaminePackage *rExaminePackage = (ExaminePackage *)mExaminablesList->PushIterator();
        while(rExaminePackage)
        {
            //--Flag.
            bool tShouldExecute = false;

            //--The activation position mut be within the examinable's hitbox. An examinable cannot be
            //  sized less than 1.
            if(pX >= rExaminePackage->mX && pY >= rExaminePackage->mY && pX <= rExaminePackage->mX + rExaminePackage->mW - 1 && pY <= rExaminePackage->mY + rExaminePackage->mH - 1)
            {
                //--If this flag is set, we can't examine the object from North of it.
                if(rExaminePackage->mFlags & EXAMINE_NOT_FROM_NORTH)
                {
                    //--Get the Actor's position.
                    TilemapActor *rPlayerActor = LocatePlayerActor();
                    if(rPlayerActor && rPlayerActor->GetWorldY() >= rExaminePackage->mY + rExaminePackage->mH)
                    {
                        tShouldExecute = true;
                    }
                }
                //--Normal case.
                else
                {
                    tShouldExecute = true;
                }
            }

            //--If this flag is set to true, execute the examination sequence.
            if(tShouldExecute)
            {
                //--Flag.
                tDidAnything = true;

                //--Normal case: Execute the exectuable.
                if(!rExaminePackage->mIsAutoTransition)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", mExaminablesList->GetIteratorName());
                }
                //--Automated transition.
                else
                {
                    //--Sound.
                    if(strcasecmp(rExaminePackage->mAutoSound, "Null"))
                    {
                        AudioManager::Fetch()->PlaySound(rExaminePackage->mAutoSound);
                    }

                    //--Set up a transition.
                    SetTransitionDestination(rExaminePackage->mAutoRoomDest, "PlayerStart");
                    SetTransitionPostExec(rExaminePackage->mAutoLocationDest);
                }
            }

            //--Next.
            rExaminePackage = (ExaminePackage *)mExaminablesList->AutoIterate();
        }
    }

    //--If anything has occured so far, stop here.
    if(tDidAnything) return tDidAnything;

    ///--[Switches]
    //--Switches. These run the examinable script but also pass in the activation state of the switch.
    //  Switches can always and only be examined from the south!
    DoorPackage *rSwitchPack = (DoorPackage *)mSwitchList->PushIterator();
    while(rSwitchPack)
    {
        //--Examination position.
        int tExamineX = (int)(rSwitchPack->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rSwitchPack->mY / TileLayer::cxSizePerTile);

        //--Locate the actor for position check.
        int tActorPosY = -10000;
        TilemapActor *rPlayerActor = LocatePlayerActor();
        if(rPlayerActor) tActorPosY = rPlayerActor->GetWorldY();

        //--Must be at the activation position, and south of it.
        if(tTileX == tExamineX && tTileY == tExamineY && tActorPosY >= rSwitchPack->mY + TileLayer::cxSizePerTile)
        {
            //--Set the activity state of the switch. It can query this at any time.
            xIsSwitchUp = rSwitchPack->mIsOpened;

            //--Run the examination script.
            xExaminationDidSomething = false;
            LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", mSwitchList->GetIteratorName());

            //--If this flag got tripped, the switch handled the update.
            if(xExaminationDidSomething) tDidAnything = true;
        }

        //--Next.
        rSwitchPack = (DoorPackage *)mSwitchList->AutoIterate();
    }

    ///--[Save Points]
    //--Save points.
    SavePointPackage *rSavePack = (SavePointPackage *)mSavePointList->PushIterator();
    while(rSavePack)
    {
        //--Examination position.
        int tExamineX = (int)(rSavePack->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rSavePack->mY / TileLayer::cxSizePerTile);

        //--Must be at the activation position.
        if(tTileX == tExamineX && tTileY == tExamineY)
        {
            //--If the lighting shader is on, register a light at this point.
            if(IsLightingActive() && !rSavePack->mIsLit)
            {
                AdventureLight *nLight = new AdventureLight();
                nLight->SetName("SaveLight");
                nLight->SetPosition(rSavePack->mX + 8.0f, rSavePack->mY + 8.0f);
                nLight->SetRadial(32000.0f);
                RegisterLight(nLight);
            }

            //--Light it if it wasn't already.
            rSavePack->mIsLit = true;

            //--Store the current room as the last save point.
            ResetString(xLastSavePoint, mName);

            //--Also set it in the datalibrary.
            SysVar *rLastSavePoint = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Global/Autosave/sLastSavePoint");
            if(rLastSavePoint)
            {
                ResetString(rLastSavePoint->mAlpha, xLastSavePoint);
            }

            //--Activate the submenu in save mode.
            mAdventureMenu->OpenCampfire();
            mAdventureMenu->SetBenchMode(rSavePack->mIsBench);
            tDidAnything = true;
            mSavePointList->PopIterator();
            break;
        }

        //--Next.
        rSavePack = (SavePointPackage *)mSavePointList->AutoIterate();
    }

    ///--[No Hits]
    //--All fails, debug.
    if(!tDidAnything && false)
    {
        //--Show the WorldDialogue GUI and add some text.
        WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
        rWorldDialogue->Clear();
        rWorldDialogue->Show();
        rWorldDialogue->AddDialogueLockout();
        rWorldDialogue->AppendString("No problem here.", false);
        return true;
    }

    //--All checks failed.
    return tDidAnything;
}
