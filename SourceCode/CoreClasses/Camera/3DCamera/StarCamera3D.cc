//--Base
#include "StarCamera3D.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers


#define CAMERA_DEFAULT_X -469.000f
#define CAMERA_DEFAULT_Y -3232.000f
#define CAMERA_DEFAULT_Z -122.000f

#define CAMERA_DEFAULT_PIT 71.000f
#define CAMERA_DEFAULT_YAW 0.000f
#define CAMERA_DEFAULT_ROL -271.000f

/*
#define CAMERA_DEFAULT_X -198.000f
#define CAMERA_DEFAULT_Y 1948.000f
#define CAMERA_DEFAULT_Z -138.000f

#define CAMERA_DEFAULT_PIT 78.000f
#define CAMERA_DEFAULT_YAW 0.000f
#define CAMERA_DEFAULT_ROL -181.000f*/

///========================================== System ==============================================
StarCamera3D::StarCamera3D()
{
    ///--[StarCamera]
    //--System

    ///--[StarCamera2D]
    //--System
    //--Map Related States
    //--Zoom factor.
    //--Spatial positioning
    //--Non-polygonal collision zones.
    //--Polygonal collision zones.
    //--Pushers.
    //--Locking and Clamping

    ///--[StarCamera3D]
    //--System
    //--Movement
    mMoveSpeed = 5.0f;
    mMoveSpeedMin = 2.0f;
    mMoveSpeedMax = 10.0f;

    //--Position
    mMoveStore = 0.0f;
    memset(&mCoordinates, 0, sizeof(ThreeDimensionReal));
    mCoordinates.mXCenter = CAMERA_DEFAULT_X;
    mCoordinates.mYCenter = CAMERA_DEFAULT_Y;
    mCoordinates.mZCenter = CAMERA_DEFAULT_Z;

    //--Rotations
    mXRotation = CAMERA_DEFAULT_PIT;
    mYRotation = CAMERA_DEFAULT_YAW;
    mZRotation = CAMERA_DEFAULT_ROL;

    //--Flags
    mMouseSensitivity = 1.0f;
    mOldMouseX = -100.0f;
    mOldMouseY = 0.0f;
    mOldMouseZ = 0.0f;
    mNoClip = true;
}
StarCamera3D::~StarCamera3D()
{

}

///===================================== Property Queries =========================================
bool StarCamera3D::Is2DCamera()
{
    return false;
}
bool StarCamera3D::Is3DCamera()
{
    return true;
}
void StarCamera3D::Get3DPosition(float &sX, float &sY, float &sZ)
{
    //--Default returns all zeroes.
    sX = mCoordinates.mXCenter;
    sY = mCoordinates.mYCenter;
    sZ = mCoordinates.mZCenter;
}
void StarCamera3D::Get3DRotation(float &sX, float &sY, float &sZ)
{
    //--Default returns all zeroes.
    sX = mXRotation;
    sY = mYRotation;
    sZ = mZRotation;
}

///======================================= Manipulators ===========================================
void StarCamera3D::SetPosition(float pX, float pY, float pZ)
{
    mCoordinates.mXCenter = pX;
    mCoordinates.mYCenter = pY;
    mCoordinates.mZCenter = pZ;
}
void StarCamera3D::SetRotation(float pX, float pY, float pZ)
{
    //--Set.
    mXRotation = pX;
    mYRotation = pY;
    mZRotation = pZ;

    //--Clamping.
    while(mZRotation <   0.0f) mZRotation = mZRotation + 360.0f;
    while(mZRotation > 360.0f) mZRotation = mZRotation - 360.0f;
}

///======================================= Core Methods ===========================================
void StarCamera3D::PositionInWorld()
{
    //--Rotate the world.
    glRotatef(mXRotation, 1, 0, 0);
    glRotatef(mYRotation, 0, 1, 0);
    glRotatef(mZRotation, 0, 0, 1);

    //--Position the camera.
    glTranslatef(mCoordinates.mXCenter, mCoordinates.mYCenter, mCoordinates.mZCenter);
}
void StarCamera3D::UnpositionInWorld()
{
    //--Undo the above in the opposite order.
    glTranslatef(-mCoordinates.mXCenter, -mCoordinates.mYCenter, -mCoordinates.mZCenter);
    glRotatef(-mZRotation, 0, 0, 1);
    glRotatef(-mYRotation, 0, 1, 0);
    glRotatef(-mXRotation, 1, 0, 0);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
