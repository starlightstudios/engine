//--Base
#include "PairanormalDialogue.h"

//--Classes
#include "PairanormalDialogueCharacter.h"
#include "PairanormalLevel.h"
#include "PairanormalMenu.h"
#include "RootEvent.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "OptionsManager.h"

///--[Local Definitions]
#define TICK_INTERVAL 3.0f

#define IMAGE_FADE_TICKS 120
#define TEXTBOX_FADE_TICKS 15

#define DEPTH_IMAGES -0.900000f

///========================================== System ==============================================
PairanormalDialogue::PairanormalDialogue()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PAIRANORMALDIALOGUE;

    //--[PairanormalDialogue]
    //--System
    mIsLoad = false;
    mCharacterFirstShowThisTick = false;
    mIsHardBlocking = false;
    mCharactersRecomputePositions = false;

    //--Tag Remaps
    mTagRemapList = new SugarLinkedList(true);

    //--Speaker
    mIsSpeakerPlayer = false;
    mIsSpeakerThought = false;
    mIsSpeakerInfo = false;
    mIsSpeakerCredits = false;
    mSpeakerChangedThisTick = false;
    mCharactersChangedVisibilityThisTick = false;
    mCharacterSpeaker = InitializeString("Nobody");
    mSpeakerName = InitializeString("Nobody");
    mFocusName = InitializeString("Nobody");
    mCharacterZoom = 1.0f;

    //--Text Ticking
    mTextTickTimer = 0.0f;
    mTextTickInterval = TICK_INTERVAL;
    strcpy(mVoiceTick, "Voice|Thought");

    //--Display
    cFontScale = 1.00f;
    mIsUsingSwaggerNextPass = true;
    mDialogueBoxDim.SetWH(56.0f, 572.0f, 916.0f, 162.0f);

    //--Glitch
    mGlitchTimer = 1;
    mGlitchRunTimer = 1;
    mGlitchFlags = 0x00;
    mGlitchVHSStart = 0.0f;
    mGlitchVHSRange = 0.0f;
    mCreepySlot = 0;

    //--Text
    mAnythingToAppend = false;
    mWaitingOnKeypress = false;
    mMainString = NULL;
    mCurrentAppendLetter = 0;
    mCurrentAppendX = 0;
    mCurrentAppendY = 0;
    mCurrentTextFlags = 0x00;
    memset(mSubstrings, 0, sizeof(char) * MAX_LINES * MAX_LETTERS);
    memset(mSubstringsDisplay, 0, sizeof(bool) * MAX_LINES * MAX_LETTERS);
    memset(mSubstringFlags, 0, sizeof(uint8_t) * MAX_LINES * MAX_LETTERS);
    mChangeTextBoxesTimer = 0;
    rCurrentTextBox = NULL;
    rPreviousTextBox = NULL;

    //--Flags
    mReappend = false;

    //--Image Display Mode
    mIsImageDisplayMode = false;
    mImageFadeTimer = 0;
    rActiveImage = NULL;
    rFadingImage = NULL;

    //--Decisions
    mShowDecisions = false;
    mDecisionsNeedReposition = false;
    mHighlightedDecision = -1;
    mDecisionTwoLists = false;
    mDecisionList = new SugarLinkedList(true);

    //--Timings
    mAppendSpeed = 0.5f;
    mSpeedFactor = 1.0f;
    mAppendNumerator = 0.0f;
    mAppendDenominator = 1.0f;

    //--Characters
    memset(rActiveCharacters, 0, sizeof(void *) * MAX_ACTIVE_CHARACTERS);
    mCharactersList = new SugarLinkedList(true);
    mCharNameRemapList = new SugarLinkedList(true);

    //--Major Scenes
    rMajorSceneImg = NULL;

    //--Investigation Mode
    mIsInvestigationMode = false;
    mInvestigationTimeLeft = -1;
    mInvestigationTimeMax = -1;
    mGoHomeScript = InitializeString("No String");
    mGoHomeArg = InitializeString("No String");
    mBGScrollTimer = 0;
    mInvestigationPointsLeft = 0;
    mInvestigationBottomScrollTimer = 0;
    mInventoryOffset = 0;
    rShowDiscoveredObject = NULL;
    mInvestigationObjectList = new SugarLinkedList(true);
    mDiscoveredObjectList = new SugarLinkedList(true);
    mRenderableObjectList = new SugarLinkedList(true);
    memset(mInvestigationButtons, 0, sizeof(TwoDimensionReal) * DIABTN_INV_TOTAL);

    //--Name-Entry Mode
    mIsNameEntryMode = false;
    mIsShifted = false;
    mIsCapsLocked = false;
    mLetterCursor = 3;
    mNameEntryTimer = 0;
    mNameEntryString[0] = '\0';
    strcpy(mCurrentPlayerName, "Jay");

    //--Character Talking Mode
    mIsPresentationMode = false;
    mPresentationOffset = 0;
    rSlidingCharacter = NULL;
    mCharacterTalkSwapTimer = 0;
    mCharacterFocusTimer = 0;
    mSelectedCharacter = -1;
    mSpeakingToCharacter = -1;
    mCharacterTalkList = new SugarLinkedList(false);
    mPresentNameBox.Set(0.0f, 0.0f, 1.0f, 1.0f);
    mPresentInfoBox.Set(0.0f, 0.0f, 1.0f, 1.0f);

    //--Images
    memset(&Images, 0, sizeof(Images));
}
PairanormalDialogue::~PairanormalDialogue()
{
    delete mTagRemapList;
    free(mCharacterSpeaker);
    free(mSpeakerName);
    free(mFocusName);
    free(mMainString);
    delete mDecisionList;
    delete mCharactersList;
    delete mCharNameRemapList;
    free(mGoHomeScript);
    free(mGoHomeArg);
    delete mInvestigationObjectList;
    delete mDiscoveredObjectList;
    delete mRenderableObjectList;
}
void PairanormalDialogue::Construct()
{
    ///--[Setup]
    //--Load image data.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->GetSection("Root/Pairanormal/")) return;

    ///--[Common]
    //--Fonts
    Images.Data.rDialogueFont = rDataLibrary->GetFont("Pairanormal Dialogue Normal");
    Images.Data.rHeadingFont  = rDataLibrary->GetFont("Pairanormal Dialogue Heading");

    ///--[Dialogue Parts]
    //--Overlays
    Images.Data.rBackgroundBarsBot  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/BorderBarsBot");
    Images.Data.rBackgroundBarsTop  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/BorderBarsTop");
    Images.Data.rBackgroundBarUnder = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/Underbar");
    Images.Data.rVHSOverlay         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Overlays/VHS");
    Images.Data.rCreepyOverlay[0]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Overlays/Creepy0");
    Images.Data.rCreepyOverlay[1]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Overlays/Creepy1");
    Images.Data.rCreepyOverlay[2]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Overlays/Creepy2");
    Images.Data.rCreepyOverlay[3]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Overlays/Creepy3");

    //--Common UI Pieces
    Images.Data.rBtnExitGame = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Common/Close");
    Images.Data.rBtnSettings = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Common/Settings");

    //--Dialogue Parts
    Images.Data.rContinueIcon = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/ContinueIcon");
    Images.Data.rDialogueBoxBlack = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/DialogueBlack");
    Images.Data.rDialogueBoxWhite = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/DialogueWhite");
    Images.Data.rNameBarLft = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/NameBarLft");
    Images.Data.rNameBarRgt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/NameBarRgt");

    //--Decision Parts
    Images.Data.rDecisionBoxBlack   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/DecisionBlack");
    Images.Data.rDecisionBoxWhite   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/DecisionWhite");
    Images.Data.rDecisionBoxBlackHf = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/DecisionBlackHf");
    Images.Data.rDecisionBoxWhiteHf = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/DecisionWhiteHf");

    //--Info Box
    Images.Data.rInfoBoxBot = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/InfoBoxBot");
    Images.Data.rInfoBoxTop = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Dialogue/InfoBoxTop");

    ///--[Investigation UI]
    //--UI Parts
    Images.Investigation.rInvArrows = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/Arrows");
    Images.Investigation.rCloseupBacking = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/CloseupBacking");
    Images.Investigation.rHand = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/Hand");
    Images.Investigation.rHouse = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/House");
    Images.Investigation.rIconBacking = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/IconBacking");
    Images.Investigation.rItemNameBacking = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/ItemNameBacking");
    Images.Investigation.rMagnifyingLens = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/MagnifyingLens");
    Images.Investigation.rMoreCharsLft = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/MoreDiaLft");
    Images.Investigation.rMoreCharsRgt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/MoreDiaRgt");
    Images.Investigation.rTimerBarBlack = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/TimerBarBlack");
    Images.Investigation.rTimerBarWhite = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/TimerBarWhite");

    //--Presentation UI.
    Images.Investigation.rButtonBlack = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/ButtonBlack");
    Images.Investigation.rButtonWhite = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/ButtonWhite");
    Images.Investigation.rShowMeBox = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/GotSomeToShowMeBox");
    Images.Investigation.rShowMeNameBox = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Investigation/GotSomeToShowMeNameBox");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *)) && VerifyStructure(&Images.Investigation, sizeof(Images.Investigation), sizeof(void *));

    //--Sub-calls.
    ConstructInvestigation();

    ///--[Speaker Box]
    rPreviousTextBox = Images.Data.rDialogueBoxBlack;
    rCurrentTextBox = Images.Data.rDialogueBoxBlack;
}

///--[Public Statics]
//--Allows voice ticks to play. Some players toggle this off.
bool PairanormalDialogue::xAllowVoiceTicks = true;

//--Shows white boxes around what you can examine. Players can toggle this on in the options menu if they get stuck.
bool PairanormalDialogue::xShowInvestigationHints = false;

//--Investigation path variables.
char *PairanormalDialogue::xInvestigationStdImg = NULL;
char *PairanormalDialogue::xInvestigationStdPath = NULL;

//--Static copy for instantiation. Gets booted when first fetched.
PairanormalDialogue *PairanormalDialogue::xStaticCopy = NULL;

///===================================== Property Queries =========================================
bool PairanormalDialogue::IsStoppingEvents()
{
    //fprintf(stderr, "Query - ");
    //--Never stops events if not constructed yet.
    if(!Images.mIsReady)
    {
        //fprintf(stderr, "Not constructed.\n");
        return false;
    }

    //--Cannot stop events when in investigation mode.
    if(mIsInvestigationMode)
    {
        //fprintf(stderr, "In investigation.\n");
        return false;
    }

    //--Not investigating, which means a scene is running.
    if(mAnythingToAppend)
    {
        //fprintf(stderr, "Has text appending.\n");
        return true;
    }

    if(mWaitingOnKeypress)
    {
        //fprintf(stderr, "Waiting on keypress.\n");
        return true;
    }
    //fprintf(stderr, "Not blocking.\n");
    return false;
}
float PairanormalDialogue::GetAppendSpeed()
{
    return mAppendSpeed;
}
bool PairanormalDialogue::IsUsingSwagger()
{
    return mIsUsingSwaggerNextPass;
}
const char *PairanormalDialogue::GetPlayerName()
{
    return mCurrentPlayerName;
}
char *PairanormalDialogue::ComposeCharacterList()
{
    //--Note: Caller is responsible for deallocation, string is heap-allocated. If no characters
    //  are on the field, "Nobody" is passed back.
    int tList[] = {MIDDLE_CHARACTER, MIDDLE_CHARACTER+1, MIDDLE_CHARACTER-1, MIDDLE_CHARACTER+2, MIDDLE_CHARACTER-2, MIDDLE_CHARACTER+3, MIDDLE_CHARACTER-3};

    //--Iterate.
    bool tFoundAnyCharacters = false;
    SugarAutoBuffer *tBuffer = new SugarAutoBuffer();
    for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
    {
        //--Ignore empty slots.
        if(!rActiveCharacters[tList[i]]) continue;

        //--If this character is legal, at least one character was fount.
        tFoundAnyCharacters = true;

        //--Append this character's name to the buffer.
        tBuffer->AppendStringWithoutNull(rActiveCharacters[tList[i]]->GetInternalName());
        tBuffer->AppendStringWithoutNull("|");
        tBuffer->AppendStringWithoutNull(rActiveCharacters[tList[i]]->GetEmotionName());
        tBuffer->AppendStringWithoutNull("|");
    }

    //--If no characters were found, return a simple heap-allocated string.
    if(!tFoundAnyCharacters)
    {
        delete tBuffer;
        char *nString = InitializeString("Nobody");
        return nString;
    }
    //--Otherwise, finish up the buffer and return its contents.
    else
    {
        tBuffer->AppendNull();
        char *nString = (char *)tBuffer->LiberateData();
        delete tBuffer;
        return nString;
    }
}
bool PairanormalDialogue::IsCharacterActive(const char *pName)
{
    //--Returns true if the named character is on the active lineup. Aliases are NOT checked.
    if(!pName) return false;

    //--Character's reference pointer.
    PairanormalDialogueCharacter *rCheckCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pName);
    if(rCheckCharacter)
    {
        for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
        {
            if(rActiveCharacters[i] == rCheckCharacter)
            {
                return true;
            }
        }
    }

    //--Not found.
    return false;
}

///======================================== Manipulators ==========================================
void PairanormalDialogue::SetSpeaker(const char *pName)
{
    //--Error check. Name can never be NULL.
    if(!pName) return;

    //--If the old speaker is the same as the new one, ignore it.
    if(!strcasecmp(pName, mSpeakerName)) return;

    //--Reset flags.
    mIsSpeakerPlayer = false;
    mIsSpeakerInfo = false;
    mIsSpeakerCredits = false;
    mIsSpeakerThought = false;

    //--Store the speaker's name.
    ResetString(mSpeakerName, pName);

    //--New speaker. Store it, set flags.
    if(strcasecmp(mCharacterSpeaker, mSpeakerName))
    {
        mSpeakerChangedThisTick = true;
        mCharactersRecomputePositions = true;
    }

    //--Speaker can be the player if specified. Manually modifies voice to be the player's.
    if(!strcasecmp(mSpeakerName, "Player"))
    {
        mIsSpeakerPlayer = true;
        sprintf(mVoiceTick, "Voice|Player");
    }
    //--If the name "Info" is used, show an info box instead.
    else if(!strcasecmp(mSpeakerName, "Info"))
    {
        mIsSpeakerInfo = true;
    }
    //--If the name "Credits" is used, show the credits box in the middle of the screen.
    else if(!strcasecmp(mSpeakerName, "Credits"))
    {
        mIsSpeakerCredits = true;
    }
    //--If the name "Thought" is used, show a different box and no name tag.
    else if(!strcasecmp(mSpeakerName, "Thought"))
    {
        mIsSpeakerThought = true;
    }
    //--Anyone who isn't a special speaker becomes the last character speaker.
    else
    {
        //--In all cases, this name gets set as the speaker, even if it does not reference a character.
        ResetString(mCharacterSpeaker, mSpeakerName);
        FocusCharacter(mSpeakerName);
        SetVoice(mSpeakerName);
    }

    //--If the special cases got flagged, the speaker does not qualify as changing.
    if(mIsSpeakerInfo || mIsSpeakerThought || mIsSpeakerPlayer)
    {
        mCharactersRecomputePositions = false;
    }

    //--Info box. Precludes the display of the dialogue boxes.
    rPreviousTextBox = rCurrentTextBox;
    if(mIsSpeakerInfo)
    {
        rCurrentTextBox = Images.Data.rInfoBoxBot;
    }
    //--Credits have no backing.
    else if(mIsSpeakerCredits)
    {
        rCurrentTextBox = NULL;
    }
    //--White border.
    else if(!mIsSpeakerThought)
    {
        rCurrentTextBox = Images.Data.rDialogueBoxWhite;
    }
    //--Thoughts use a different box.
    else
    {
        rCurrentTextBox = Images.Data.rDialogueBoxBlack;
    }

    //--If the text box changes, flag this.
    if(rCurrentTextBox != rPreviousTextBox) mChangeTextBoxesTimer = 0;
}
void PairanormalDialogue::SetSpeakerName(const char *pName)
{
    //--Sets only the name, independent of everything else.
    ResetString(mSpeakerName, pName);
}
void PairanormalDialogue::HighlightCharacter(const char *pName, bool pIsHighlighted)
{
    //--Manually toggles highlighting on whichever character is named. Doesn't change who is focused or speaking.
    //  Multiple characters can be highlighted at once.
    if(!pName) return;

    //--Locate, set.
    PairanormalDialogueCharacter *rActor = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pName);
    if(rActor) rActor->SetOverrideFocus(pIsHighlighted);
}
void PairanormalDialogue::FocusCharacter(const char *pName)
{
    //--Changes the character under focus, but not the speaker, to the given name. Should be called
    //  after the speaker has resolved.
    if(!pName) return;
    mCharactersRecomputePositions = true;

    //--All characters set their override flags to false.
    PairanormalDialogueCharacter *rOverrideChar = (PairanormalDialogueCharacter *)mCharactersList->PushIterator();
    while(rOverrideChar)
    {
        rOverrideChar->SetOverrideFocus(false);
        rOverrideChar = (PairanormalDialogueCharacter *)mCharactersList->AutoIterate();
    }

    //--First, check if the character actually exists. If it is on the stage, then set it as the focus.
    PairanormalDialogueCharacter *rCheckCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pName);
    if(rCheckCharacter)
    {
        ResetString(mFocusName, pName);
        return;
    }

    //--If the focus wasn't set, check to see if there is an alias case.
    const char *rCheckName = (const char *)mCharNameRemapList->GetElementByName(pName);
    if(rCheckName)
    {
        PairanormalDialogueCharacter *rCheckCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(rCheckName);
        if(rCheckCharacter)
        {
            ResetString(mFocusName, rCheckName);
        }
    }
}
void PairanormalDialogue::SetVoice(const char *pVoiceName)
{
    //--Sets the speaker. Allows aliases.
    if(!pVoiceName) return;

    //--First, check if the character actually exists. Doesn't need to be on stage to be the focus.
    PairanormalDialogueCharacter *rCheckCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pVoiceName);
    if(rCheckCharacter)
    {
        sprintf(mVoiceTick, "Voice|%s", pVoiceName);
        return;
    }

    //--If the focus wasn't set, check to see if there is an alias case.
    const char *rCheckName = (const char *)mCharNameRemapList->GetElementByName(pVoiceName);
    if(rCheckName)
    {
        PairanormalDialogueCharacter *rCheckCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(rCheckName);
        if(rCheckCharacter)
        {
            sprintf(mVoiceTick, "Voice|%s", rCheckName);
        }
        else
        {
        }
    }
    else
    {
    }
}
void PairanormalDialogue::AddCharacterAlias(const char *pBaseName, const char *pAlias)
{
    //--Adds a name alias, causing the base name to replace the alias for purposes of highlighting
    //  and voice ticks.
    if(!pBaseName || !pAlias) return;

    //--Check if the alias already exists.
    if(mCharNameRemapList->GetElementByName(pAlias) != NULL) return;

    //--Create a new string.
    char *nString = InitializeString(pBaseName);
    mCharNameRemapList->AddElement(pAlias, nString, &FreeThis);
}
void PairanormalDialogue::AppendDialogue(const char *pDialogue)
{
    ///--[Documentation and Setup]
    //--Error check.
    if(!pDialogue) return;

    ///--[Playername and Variables]
    //--PairanormalDialogue replacement. Variable tags need to have the line replaced entirely before parsing
    //  begins. First, we replace playername cases.
    SugarAutoBuffer *tAutoBuffer = new SugarAutoBuffer();
    int tLen = (int)strlen(pDialogue);
    for(int i = 0; i < tLen; i ++)
    {
        //--[PLAYERNAME] tag.
        if(!strncasecmp(&pDialogue[i], "[PLAYERNAME]", 12))
        {
            //--Append the player's name.
            tAutoBuffer->AppendStringWithoutNull(mCurrentPlayerName);

            //--Skip ahead.
            i += 11;
        }
        //--All other cases.
        else
        {
            tAutoBuffer->AppendCharacter(pDialogue[i]);
        }
    }

    //--Add a NULL onto the end.
    tAutoBuffer->AppendCharacter('\0');

    //--Extract the memory as a string. Use that as the dialogue.
    uint8_t *nExtractedString = tAutoBuffer->LiberateData();
    delete tAutoBuffer;

    //--Reset flags.
    mAnythingToAppend = true;
    mWaitingOnKeypress = true;
    ResetString(mMainString, (const char *)nExtractedString);
    free(nExtractedString);
    nExtractedString = NULL;

    ///--[Speaker Handler]
    bool tLastLetterWasBrace = false;
    tAutoBuffer = new SugarAutoBuffer();
    tLen = (int)strlen(mMainString);
    for(int i = 0; i < tLen; i ++)
    {
        //--Colons. A single colon sets the speaker as the word right behind it, using spaces or ] as a delimiter.
        //  To render an actual colon, put two down.
        //--Example: "Mari: Hello, everyone! Here's a list:: One, two, three."
        if(mMainString[i] == ':')
        {
            //--Double-colon cases. If the next letter is a colon, skip one letter and replace it with a colon.
            if(i < tLen - 1 && mMainString[i+1] == ':')
            {
                tAutoBuffer->AppendStringWithoutNull(":");
                i ++;
            }
            //--Next letter is not a colon, so go backwards to extract the name of the speaker.
            else
            {
                //--Scan backwards.
                int tNameStart = 0;
                for(int p = i - 1; p >= 0; p --)
                {
                    //--Ending delimiter.
                    if(mMainString[p] == ' ' || mMainString[p] == ']')
                    {
                        tNameStart = p+1;
                        break;
                    }
                }

                //--Copy the name to a buffer.
                char *tNameBuffer = (char *)starmemoryalloc(sizeof(char) * tLen);
                strncpy(tNameBuffer, &mMainString[tNameStart], i - tNameStart);
                tNameBuffer[i - tNameStart] = '\0';

                //--Set it.
                SetSpeaker(tNameBuffer);

                //--Now we need to remove letters from the auto buffer to remove the speaker designation.
                tAutoBuffer->RemoveBytes(i - tNameStart);

                //--Skip another letter if that letter is a space.
                if(i < tLen - 1 && mMainString[i+1] == ' ')
                {
                    i ++;
                }

                //--Clean.
                free(tNameBuffer);
            }
        }
        //--All other cases.
        else
        {
            //--Append.
            tAutoBuffer->AppendCharacter(mMainString[i]);

            //--Flag.
            tLastLetterWasBrace = mMainString[i] == ']';
        }
    }

    //--Last-letter check. If the last letter is a ']', we need to append an extra space. This is because the substring parser will screw up
    //  and miss the last tag since it moves to the next string instead of handling the tag.
    if(tLastLetterWasBrace) tAutoBuffer->AppendCharacter(' ');

    //--Add a NULL onto the end.
    tAutoBuffer->AppendCharacter('\0');

    //--Extract the memory as a string. Use that as the dialogue.
    nExtractedString = tAutoBuffer->LiberateData();
    delete tAutoBuffer;

    //--Reset flags.
    mAnythingToAppend = true;
    mWaitingOnKeypress = true;
    ResetString(mMainString, (const char *)nExtractedString);
    free(nExtractedString);
    nExtractedString = NULL;

    ///--[Reset Flags]
    mCurrentAppendLetter = 0;
    mCurrentAppendX = 0;
    mCurrentAppendY = 0;

    //--Clear all display cases.
    mCurrentTextFlags = 0;
    memset(mSubstrings, 0, sizeof(char) * MAX_LINES * MAX_LETTERS);
    memset(mSubstringsDisplay, 0, sizeof(bool) * MAX_LINES * MAX_LETTERS);
    memset(mSubstringFlags, 0, sizeof(uint8_t) * MAX_LINES * MAX_LETTERS);

    //--If we're switching fonts, now is the time to do it.
    if(mIsUsingSwaggerNextPass)
    {
        Images.Data.rDialogueFont = DataLibrary::Fetch()->GetFont("Pairanormal Dialogue Normal");
    }
    else
    {
        Images.Data.rDialogueFont = DataLibrary::Fetch()->GetFont("Pairanormal Dialogue Special");
    }

    ///--[Apply Text]
    //--Setup.
    int tStringLen = (int)strlen(mMainString);
    float cUseWid = 1200.0f;
    if(mIsSpeakerInfo) cUseWid = 475.0f;

    //--Loop until the whole description has been parsed out.
    int tCurrentLine = 0;
    int tLettersParsedTotal = 0;
    while(tLettersParsedTotal < tStringLen)
    {
        //--Run the subdivide.
        int tCharsParsed = 0;
        char *tLine = Subdivide::SubdivideStringBraces(tCharsParsed, &mMainString[tLettersParsedTotal], MAX_LETTERS, cUseWid, Images.Data.rDialogueFont, cFontScale);

        //--Store.
        strcpy(mSubstrings[tCurrentLine], tLine);
        free(tLine);

        //--Move to the next line.
        tLettersParsedTotal += tCharsParsed;
        tCurrentLine ++;

        //--Error.
        if(tCurrentLine > MAX_LINES) break;
    }

    //--Clean.
}
void PairanormalDialogue::AppendDialogueOnlyChars(const char *pDialogue)
{
    //--Called when running to a checkpoint, this only scans for character tags and shows/hides as needed.
    if(!pDialogue) return;

    //--Iterate.
    int tLen = (int)strlen(pDialogue);
    int tLetter = 0;
    for(tLetter = 0; tLetter < tLen; tLetter ++)
    {
        if(pDialogue[tLetter] == '[')
        {
            //--Setup.
            int tBraceStack = 0;

            //--Begin seeking forwards until we find the matching end brace.
            int tTentativeLetter = tLetter + 1;
            while(tTentativeLetter < tLen)
            {
                //--Get the new letter.
                char tNewLetter = pDialogue[tTentativeLetter];

                //--Increment brace stack.
                if(tNewLetter == '[')
                {
                    tBraceStack ++;
                }
                //--Decrement brace stack.
                else if(tNewLetter == ']')
                {
                    tBraceStack --;
                }

                //--If the brace stack hits -1, end this parse.
                if(tBraceStack < 0)
                {
                    break;
                }

                //--Next.
                tTentativeLetter ++;
            }

            //--Take the string which was parsed out. Put it in a buffer and send it to the handler.
            char tStringBuf[MAX_LETTERS];
            strncpy(tStringBuf, &pDialogue[tLetter], tTentativeLetter - tLetter + 1);
            tStringBuf[tLetter - tLetter + 1] = '\0';

            //--Move up.
            tLetter = tTentativeLetter + 1;
            //mCurrentAppendX --;

            //--Handle special cases.
            //fprintf(stderr, "Handled quick append: %s\n", tStringBuf);
            HandleTagQuick(tStringBuf, true);
        }
    }
}
void PairanormalDialogue::RegisterCharacter(const char *pName, PairanormalDialogueCharacter *pCharacter)
{
    //--Error check.
    if(!pName || !pCharacter) return;

    //--Check for a duplicate.
    if(mCharactersList->GetElementByName(pName) != NULL || mCharactersList->IsElementOnList(pCharacter)) return;

    //--Add it.
    pCharacter->SetInternalName(pName);
    mCharactersList->AddElement(pName, pCharacter, &RootObject::DeleteThis);
}
void PairanormalDialogue::ShowCharacter(const char *pName)
{
    //--Overload, uses whatever emotion was set previously.
    ShowCharacter(pName, "NoChange");
}
void PairanormalDialogue::ShowCharacter(const char *pName, const char *pEmotion)
{
    //--Shows the character and positions them in the lineup, assuming there is space.
    PairanormalDialogueCharacter *rCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pName);
    if(!rCharacter || !pEmotion)
    {
        return;
    }

    //--Set emotion.
    //fprintf(stderr, "Showing character with emotion %s\n", pEmotion);
    rCharacter->SetEmotion(pEmotion);

    //--Check if the character is already visible. Do nothing if they are.
    for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
    {
        if(rActiveCharacters[i] == rCharacter) return;
    }

    //--Flags.
    mCharactersChangedVisibilityThisTick = true;
    rCharacter->mIsNextTransitionFade = true;
    rCharacter->SetOverrideFocus(false);

    //--Presumably, the characters will now need to recompute their positions.
    mCharactersRecomputePositions = true;

    //--Check the middle slot. If it's empty, place it there. This character also instantly becomes the
    //  character speaker when this happens.
    if(rActiveCharacters[MIDDLE_CHARACTER] == NULL)
    {
        mCharacterFirstShowThisTick = true;
        ResetString(mCharacterSpeaker, pName);
        rActiveCharacters[MIDDLE_CHARACTER] = rCharacter;
        FocusCharacter(mCharacterSpeaker);
        return;
    }

    //--Start attempting to place the character to the right, then left, of the middle.
    //  There are currently two valid slots on either side, so we hard-code it.
    if(rActiveCharacters[MIDDLE_CHARACTER+1] == NULL) { rActiveCharacters[MIDDLE_CHARACTER+1] = rCharacter; return; }
    if(rActiveCharacters[MIDDLE_CHARACTER-1] == NULL) { rActiveCharacters[MIDDLE_CHARACTER-1] = rCharacter; return; }
    if(rActiveCharacters[MIDDLE_CHARACTER+2] == NULL) { rActiveCharacters[MIDDLE_CHARACTER+2] = rCharacter; return; }
    if(rActiveCharacters[MIDDLE_CHARACTER-2] == NULL) { rActiveCharacters[MIDDLE_CHARACTER-2] = rCharacter; return; }
    if(rActiveCharacters[MIDDLE_CHARACTER+3] == NULL) { rActiveCharacters[MIDDLE_CHARACTER+3] = rCharacter; return; }
    if(rActiveCharacters[MIDDLE_CHARACTER-3] == NULL) { rActiveCharacters[MIDDLE_CHARACTER-3] = rCharacter; return; }

    //--If we got this far, there was no space for another character. Oops!
}
void PairanormalDialogue::ShowCharacterBySet(const char *pSet)
{
    //--Receives a set of characters in the format "CharacterName|Emotion|CharacterName|Emotion" and displays the characters
    //  with the matching emotion. If the string "Nobody" is received, does nothing.
    if(!pSet || !strcasecmp(pSet, "Nobody")) return;

    //--Setup.
    int tLetterInBuf = 0;
    char tCharNameBuf[128];
    char tEmotionNameBuf[128];
    int tMode = 0;

    //--Iterate until we find a '|'.
    int tCharacter = 0;
    int tLen = (int)strlen(pSet);
    while(tCharacter < tLen)
    {
        //--Bars change the mode.
        if(pSet[tCharacter] == '|')
        {
            //--Character name mode, move to emotion name.
            if(tMode == 0)
            {
                tMode = 1;
                tLetterInBuf = 0;
                tEmotionNameBuf[0] = '\0';
            }
            //--Emotion name mode, set and move back to character name.
            else
            {
                //--Show.
                ShowCharacter(tCharNameBuf, tEmotionNameBuf);

                //--Reset everything.
                tMode = 0;
                tLetterInBuf = 0;
                tCharNameBuf[0] = '\0';
                tEmotionNameBuf[0] = '\0';
            }
        }
        //--Copy.
        else
        {
            //--Character name mode.
            if(tMode == 0)
            {
                tCharNameBuf[tLetterInBuf+0] = pSet[tCharacter];
                tCharNameBuf[tLetterInBuf+1] = '\0';
            }
            //--Emotion name mode.
            else
            {
                tEmotionNameBuf[tLetterInBuf+0] = pSet[tCharacter];
                tEmotionNameBuf[tLetterInBuf+1] = '\0';
            }

            //--Move the letter buffer up one.
            tLetterInBuf ++;
        }

        //--Next letter.
        tCharacter ++;
    }

    //--All shown characters update 30 times so they're in position. This is only used when loading the game.
    mIsLoad = true;
}
void PairanormalDialogue::HideCharacter(const char *pName)
{
    //--Error check. Make sure the character exists.
    PairanormalDialogueCharacter *rCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pName);
    if(!rCharacter) return;

    //--Flags.
    mCharactersChangedVisibilityThisTick = true;

    //--Characters will usually need to recompute their positions.
    mCharactersRecomputePositions = true;

    //--If this character was the middle character, the characters to the right take precedence.
    if(rActiveCharacters[MIDDLE_CHARACTER] == rCharacter)
    {
        rActiveCharacters[MIDDLE_CHARACTER+0] = rActiveCharacters[MIDDLE_CHARACTER+1];
        rActiveCharacters[MIDDLE_CHARACTER+1] = rActiveCharacters[MIDDLE_CHARACTER+2];
        rActiveCharacters[MIDDLE_CHARACTER+2] = NULL;
    }
    //--If this character was on the right side, shift the right characters down.
    else if(rActiveCharacters[MIDDLE_CHARACTER+1] == rCharacter)
    {
        rActiveCharacters[MIDDLE_CHARACTER+1] = rActiveCharacters[MIDDLE_CHARACTER+2];
        rActiveCharacters[MIDDLE_CHARACTER+2] = NULL;
    }
    //--Character was on the left side. Shift up.
    else if(rActiveCharacters[MIDDLE_CHARACTER-1] == rCharacter)
    {
        rActiveCharacters[MIDDLE_CHARACTER-1] = rActiveCharacters[MIDDLE_CHARACTER-2];
        rActiveCharacters[MIDDLE_CHARACTER-2] = NULL;
    }
    //--Far-right character. No sliding.
    else if(rActiveCharacters[MIDDLE_CHARACTER+2] == rCharacter)
    {
        rActiveCharacters[MIDDLE_CHARACTER+2] = NULL;
    }
    //--Far left. No shifting.
    else if(rActiveCharacters[MIDDLE_CHARACTER-2] == rCharacter)
    {
        rActiveCharacters[MIDDLE_CHARACTER-2] = NULL;
    }

    //--Character is ordered to fade out.
    rCharacter->SetRenderPositionByFade(rCharacter->GetRenderX(), rCharacter->GetRenderY(), -15.0f);
}
void PairanormalDialogue::SetCharacterEmotion(const char *pName, const char *pEmotion)
{
    //--Error check. Make sure the character exists.
    PairanormalDialogueCharacter *rCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pName);
    if(!rCharacter)
    {
        const char *rAliasCheck = (const char *)mCharNameRemapList->GetElementByName(pName);
        if(rAliasCheck)
        {
            rCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(rAliasCheck);
            if(!rCharacter) return;
        }
        else
        {
            return;
        }
    }

    //--"N" becomes "Neutral" as a shorthand.
    //fprintf(stderr, "Setting character %s %p emotion %s\n", pName, rCharacter, pEmotion);
    if(!strcasecmp(pEmotion, "N"))
    {
        rCharacter->SetEmotion("Neutral");
    }
    //--All other cases.
    else
    {
        rCharacter->SetEmotion(pEmotion);
    }
}
void PairanormalDialogue::HideAllCharacters()
{
    //--All characters are hidden.
    for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
    {
        //--Order the character to fade out.
        if(rActiveCharacters[i])
        {
            rActiveCharacters[i]->SetRenderPositionByFade(rActiveCharacters[i]->GetRenderX(), rActiveCharacters[i]->GetRenderY(), -15.0f);
        }

        //--Null off the character.
        rActiveCharacters[i] = NULL;
    }

    //--Flags.
    mCharactersChangedVisibilityThisTick = true;
}
void PairanormalDialogue::HideAllCharactersFast()
{
    //--All characters are hidden.
    for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
    {
        //--Order the character to fade out.
        if(rActiveCharacters[i])
        {
            rActiveCharacters[i]->SetRenderPositionByFade(rActiveCharacters[i]->GetRenderX(), rActiveCharacters[i]->GetRenderY(), 0.0f);
        }

        //--Null off the character.
        rActiveCharacters[i] = NULL;
    }

    //--Flags.
    mCharactersChangedVisibilityThisTick = true;
}
void PairanormalDialogue::ShowMajorScene(const char *pDLPath)
{
    rMajorSceneImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void PairanormalDialogue::HideMajorScene()
{
    rMajorSceneImg = NULL;
}
void PairanormalDialogue::SetAppendSpeed(float pSpeed)
{
    mAppendSpeed = pSpeed;
}
void PairanormalDialogue::SetUseSwagger(bool pFlag)
{
    mIsUsingSwaggerNextPass = pFlag;
}
void PairanormalDialogue::SetPlayerName(const char *pName)
{
    strncpy(mCurrentPlayerName, pName, DIABTN_NAME_LETTERS);
    mLetterCursor = (int)strlen(mCurrentPlayerName);
}

///======================================== Core Methods ==========================================
void PairanormalDialogue::ResolvePositionForActor(PairanormalDialogueCharacter *pActorPtr, bool pIsSpeakerChange)
{
    //--Figures out what position the given actor should be in, based on who is speaking and where the actor
    //  is in the lineup of actors. If the actor is not in the active list, does nothing.
    if(!pActorPtr) return;

    //--If nobody is speaking or the speaker is "Me", "Thought", "All", or "Self", the actor does not
    //  need to move. This only happens when the speaker changes and no characters were shown or hidden.
    if(pIsSpeakerChange)
    {
        if(!mFocusName) return;
        if(!strcasecmp(mFocusName, "Me")) return;
        if(!strcasecmp(mFocusName, "All")) return;
        if(!strcasecmp(mFocusName, "Thought")) return;
        if(!strcasecmp(mFocusName, "Self")) return;
        if(!strcasecmp(mFocusName, "Info")) return;
    }

    //--Setup.
    int tSlotCharacterIsIn = 0;
    int tSlotSpeakerIsIn = MIDDLE_CHARACTER;
    int tTotalCharacters = 0;

    //--Attempt to locate the speaking actor. If no actor is found, we don't need to move. The actor
    //  must be visible.
    for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
    {
        //--Skip empty slots.
        if(!rActiveCharacters[i]) continue;

        //--Add a character to the speaker count.
        tTotalCharacters ++;

        //--Character is this actor.
        if(rActiveCharacters[i] == pActorPtr)
        {
            tSlotCharacterIsIn = i;
        }

        //--Character is the current speaker. This is not exclusive with this being the character in question.
        if(!strcasecmp(mFocusName, rActiveCharacters[i]->GetInternalName()))
        {
            tSlotSpeakerIsIn = i;
        }
    }

    //--If there are no characters at all, do nothing. Make sure to show the character before calling this.
    if(tTotalCharacters < 1) return;

    //--Standard Y position.
    float cMidXPos = VIRTUAL_CANVAS_X * 0.5f;
    float cStdYPos = mDialogueBoxDim.mTop;
    int cUseTicks = STD_SLIDE_TICKS;
    //fprintf(stderr, "Current speaker name is %s.\n", mFocusName);

    //--Now that we have determined where the character is and where the speaker is, we need to determine
    //  their relative positions. First, if the character IS the speaker, they are centered.
    if(tSlotCharacterIsIn == tSlotSpeakerIsIn)
    {
        //--Normal case:
        pActorPtr->SetFocus(true);
        if(!pActorPtr->mIsNextTransitionFade && !pActorPtr->mIsNextTransitionSlideFromSide)
        {
            pActorPtr->SetRenderPositionBySlide(cMidXPos, cStdYPos, cUseTicks);
        }
        //--Fade-in case:
        else if(pActorPtr->mIsNextTransitionFade)
        {
            pActorPtr->SetRenderPositionByFade(cMidXPos, cStdYPos, cUseTicks);
        }
        //--Slide from the side. Always favours the right side.
        else
        {
            pActorPtr->SetRenderPosition(VIRTUAL_CANVAS_X * 1.25f, cStdYPos);
            pActorPtr->SetRenderPositionBySlide(cMidXPos, cStdYPos, cUseTicks);
        }

        //fprintf(stderr, " Actor %s %i %i computes center position %f %f\n", pActorPtr->GetInternalName(), tSlotCharacterIsIn, tSlotSpeakerIsIn, cMidXPos, cStdYPos);
    }
    //--If this character is in another slot, they are to the right or left of the speaker.
    else
    {
        //--Drop focus.
        pActorPtr->SetFocus(false);

        //--Position.
        int tSlotDif = tSlotCharacterIsIn - tSlotSpeakerIsIn;
        float cDif = 0.0f;

        //--One different. Keeps a decent spacing.
        if(abs(tSlotDif) == 1)
        {
            cDif = (tSlotCharacterIsIn - tSlotSpeakerIsIn) * VIRTUAL_CANVAS_X * 0.20f;
        }
        //--Two different. Slightly closer in than the one-different case.
        else
        {
            cDif = (tSlotCharacterIsIn - tSlotSpeakerIsIn) * VIRTUAL_CANVAS_X * 0.17f;
        }

        //--Normal case:
        if(!pActorPtr->mIsNextTransitionFade && !pActorPtr->mIsNextTransitionSlideFromSide)
        {
            pActorPtr->SetRenderPositionBySlide(cMidXPos + cDif, cStdYPos, cUseTicks);
        }
        //--Fade-in case:
        else if(pActorPtr->mIsNextTransitionFade)
        {
            pActorPtr->SetRenderPositionByFade(cMidXPos + cDif, cStdYPos, cUseTicks);
        }
        //--Slide from the side. Direction is based on which is closer.
        else
        {
            if(cDif > 0.0f)
            {
                pActorPtr->SetRenderPosition(VIRTUAL_CANVAS_X * 1.25f, cStdYPos);
            }
            else
            {
                pActorPtr->SetRenderPosition(VIRTUAL_CANVAS_X * -0.25f, cStdYPos);
            }
            pActorPtr->SetRenderPositionBySlide(cMidXPos + cDif, cStdYPos, cUseTicks);
        }
        //fprintf(stderr, " Actor %s %i %i computes non-center position %f %f\n", pActorPtr->GetInternalName(), tSlotCharacterIsIn, tSlotSpeakerIsIn, cMidXPos + cDif, cStdYPos);
    }

    //--Unset flags.
    pActorPtr->mIsNextTransitionFade = false;
    pActorPtr->mIsNextTransitionSlideFromSide = false;
}
void PairanormalDialogue::ActivateDecisions()
{
    mShowDecisions = true;
    mDecisionsNeedReposition = true;
    mHighlightedDecision = -1;
    mDecisionTwoLists = false;
    mDecisionList->ClearList();
}
void PairanormalDialogue::AppendDecision(const char *pDisplayText, const char *pDecisionScript, const char *pScriptArg)
{
    //--Adds a new decision package to the list.
    if(!pDecisionScript || !pDisplayText || !pScriptArg) return;
    PairanormalDecisionPack *nPackage = (PairanormalDecisionPack *)malloc(sizeof(PairanormalDecisionPack));
    strcpy(nPackage->mScriptPath, pDecisionScript);
    strcpy(nPackage->mDisplayText, pDisplayText);
    strcpy(nPackage->mScriptArg, pScriptArg);
    mDecisionList->AddElementAsTail("X", nPackage, &FreeThis);

    //--Flag for reposition.
    mDecisionsNeedReposition = true;
}
void PairanormalDialogue::RepositionDecisions()
{
    //--Called when decisions are added. Repositions their hitboxes. Note that the boxes are 686x75, don't
    //  use the image's dimensions since it includes the edge shading.
    float cBoxWid = 686.0f;
    float cBoxHei = 75.0f;
    float cUseLft = (VIRTUAL_CANVAS_X - cBoxWid) * 0.50f;
    float cInitialY = 174.0f;
    float cSpacingY = 80.0f;

    //--Unset this flag.
    mDecisionsNeedReposition = false;

    //--Normal list.
    if(mDecisionList->GetListSize() == 1)
    {
        cInitialY = 306.0f;
        cSpacingY = 100.0f;
    }
    else if(mDecisionList->GetListSize() == 2)
    {
        cInitialY = 254.0f;
        cSpacingY = 120.0f;
    }
    else if(mDecisionList->GetListSize() == 3)
    {
        cInitialY = 204.0f;
        cSpacingY = 100.0f;
    }
    else if(mDecisionList->GetListSize() == 4)
    {
        cInitialY = 174.0f;
        cSpacingY = 80.0f;
    }
    else if(mDecisionList->GetListSize() >= 4 && mDecisionList->GetListSize() < 8)
    {
        cInitialY = 154.0f;
        cSpacingY = 70.0f;
    }
    //--8 or more: Two rows.
    else
    {
        //--Flag.
        mDecisionTwoLists = true;

        //--Left Column Variables.
        float cXLft = 215.0f;
        cInitialY = 154.0f;
        cSpacingY = 70.0f;

        //--Left column.
        int i = 0;
        float cPos = cInitialY;
        PairanormalDecisionPack *rPack = (PairanormalDecisionPack *)mDecisionList->PushIterator();
        while(rPack)
        {
            //--Set.
            rPack->mPosition.SetWH(cXLft, cPos, cBoxWid/2, cBoxHei);

            //--Next.
            i ++;
            cPos = cPos + cSpacingY;
            rPack = (PairanormalDecisionPack *)mDecisionList->AutoIterate();
            if(i >= mDecisionList->GetListSize() / 2) break;
        }

        //--Right column.
        cXLft = 790.0f;
        cInitialY = 154.0f;
        cSpacingY = 70.0f;
        cPos = cInitialY;
        while(rPack)
        {
            //--Set.
            rPack->mPosition.SetWH(cXLft, cPos, cBoxWid/2, cBoxHei);

            //--Next.
            cPos = cPos + cSpacingY;
            rPack = (PairanormalDecisionPack *)mDecisionList->AutoIterate();
        }

        //--Exclusive with normal rows.
        return;
    }

    //--Iterate and position.
    float cPos = cInitialY;
    PairanormalDecisionPack *rPack = (PairanormalDecisionPack *)mDecisionList->PushIterator();
    while(rPack)
    {
        //--Set.
        rPack->mPosition.SetWH(cUseLft, cPos, cBoxWid, cBoxHei);

        //--Next.
        cPos = cPos + cSpacingY;
        rPack = (PairanormalDecisionPack *)mDecisionList->AutoIterate();
    }
}
void PairanormalDialogue::FactoryZero()
{
    ///--[Documentation and Setup]
    //--Clears this object back to factory-zero settings and re-resolves all pointers.

    ///--[Deallocation]
    //--Deallocate these objects.
    /*delete mTagRemapList;
    free(mCharacterSpeaker);
    free(mSpeakerName);
    free(mMainString);
    delete mDecisionList;
    delete mCharactersList;
    free(mGoHomeScript);
    free(mGoHomeArg);
    delete mInvestigationObjectList;
    delete mDiscoveredObjectList;
    delete mRenderableObjectList;

    //--Reset these objects to NULL.
    mTagRemapList = NULL;
    mCharacterSpeaker = NULL;
    mSpeakerName = NULL;
    mMainString = NULL;
    mDecisionList = NULL;
    mCharactersList = NULL;
    mGoHomeScript = NULL;
    mGoHomeArg = NULL;
    mInvestigationObjectList = NULL;
    mDiscoveredObjectList = NULL;
    mRenderableObjectList = NULL;*/

    ///--[Initialization]
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PAIRANORMALDIALOGUE;

    //--[PairanormalDialogue]
    //--System
    mIsHardBlocking = false;
    mCharactersRecomputePositions = false;

    //--Tag Remaps
    //mTagRemapList = new SugarLinkedList(true);

    //--Speaker
    mIsSpeakerPlayer = false;
    mIsSpeakerThought = false;
    mIsSpeakerInfo = false;
    mIsSpeakerCredits = false;
    mSpeakerChangedThisTick = false;
    mCharactersChangedVisibilityThisTick = false;
    //ResetString(mCharacterSpeaker, "Nobody");
    //ResetString(mSpeakerName, "Nobody");

    //--Text Ticking
    mTextTickTimer = 0.0f;
    mTextTickInterval = TICK_INTERVAL;
    strcpy(mVoiceTick, "Voice|Thought");

    //--Display
    cFontScale = 1.00f;
    mIsUsingSwaggerNextPass = true;
    mDialogueBoxDim.SetWH(56.0f, 572.0f, 916.0f, 162.0f);

    //--Glitch
    mGlitchTimer = 1;
    mGlitchRunTimer = 1;
    mGlitchFlags = 0x00;
    mGlitchVHSStart = 0.0f;
    mGlitchVHSRange = 0.0f;
    mCreepySlot = 0;

    //--Text
    mAnythingToAppend = false;
    mWaitingOnKeypress = false;
    //ResetString(mMainString, NULL);
    mCurrentAppendLetter = 0;
    mCurrentAppendX = 0;
    mCurrentAppendY = 0;
    mCurrentTextFlags = 0x00;
    memset(mSubstrings, 0, sizeof(char) * MAX_LINES * MAX_LETTERS);
    memset(mSubstringsDisplay, 0, sizeof(bool) * MAX_LINES * MAX_LETTERS);
    memset(mSubstringFlags, 0, sizeof(uint8_t) * MAX_LINES * MAX_LETTERS);

    //--Flags
    mReappend = false;

    //--Image Display Mode
    mIsImageDisplayMode = false;
    rActiveImage = NULL;

    //--Decisions
    mShowDecisions = false;
    mDecisionsNeedReposition = false;
    mHighlightedDecision = -1;
    //mDecisionList = new SugarLinkedList(true);

    //--Timings
    mAppendSpeed = 1.0f;
    mSpeedFactor = 1.0f;
    mAppendNumerator = 0.0f;
    mAppendDenominator = 1.0f;

    //--Characters
    memset(rActiveCharacters, 0, sizeof(void *) * MAX_ACTIVE_CHARACTERS);
    //mCharactersList = new SugarLinkedList(true);

    //--Major Scenes
    rMajorSceneImg = NULL;

    //--Investigation Mode
    mIsInvestigationMode = false;
    mInvestigationTimeLeft = -1;
    mInvestigationTimeMax = -1;
    //ResetString(mGoHomeScript, "No String");
    //ResetString(mGoHomeArg, "No String");
    mInvestigationPointsLeft = 0;
    mInvestigationBottomScrollTimer = 0;
    mInventoryOffset = 0;
    rShowDiscoveredObject = NULL;
    //mInvestigationObjectList = new SugarLinkedList(true);
    //mDiscoveredObjectList = new SugarLinkedList(true);
    //mRenderableObjectList = new SugarLinkedList(true);
    memset(mInvestigationButtons, 0, sizeof(TwoDimensionReal) * DIABTN_INV_TOTAL);

    //--Name-Entry Mode
    mIsNameEntryMode = false;
    mIsShifted = false;
    mIsCapsLocked = false;
    mLetterCursor = 3;
    mNameEntryTimer = 0;
    strcpy(mCurrentPlayerName, "Jay");

    //--Character Talking Mode
    mIsPresentationMode = false;
    mPresentationOffset = 0;
    mSelectedCharacter = -1;
    mSpeakingToCharacter = -1;
    //mCharacterTalkList = new SugarLinkedList(false);
    mPresentNameBox.Set(0.0f, 0.0f, 1.0f, 1.0f);
    mPresentInfoBox.Set(0.0f, 0.0f, 1.0f, 1.0f);

    ///--[Construction]
    //--Images don't need to get reset.
    ConstructInvestigation();
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void PairanormalDialogue::Update()
{
    ///--[Documentation and Setup]
    //--Handles appending dialogue to the box, or decisions. This can be throttled by the controlling
    //  entity. No auto-update takes place.

    ///--[Text Box Fading]
    if(mChangeTextBoxesTimer < TEXTBOX_FADE_TICKS) mChangeTextBoxesTimer ++;

    ///--[Image Fading]
    if(mImageFadeTimer < IMAGE_FADE_TICKS) mImageFadeTimer ++;

    ///--[Update Visible Characters]
    for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
    {
        if(rActiveCharacters[i]) rActiveCharacters[i]->Update(false);
    }

    //--All characters who did not render here will render if they are fading out.
    PairanormalDialogueCharacter *rCharacter = (PairanormalDialogueCharacter *)mCharactersList->PushIterator();
    while(rCharacter)
    {
        if(rCharacter->IsFadingOut()) rCharacter->Update(false);
        rCharacter = (PairanormalDialogueCharacter *)mCharactersList->AutoIterate();
    }

    ///--[Glitch Timer]
    //--Increments to the cap.
    if(mGlitchTimer < mGlitchRunTimer)
    {
        mGlitchTimer ++;
    }

    ///--[Investigation Mode]
    //--When in investigation, just send the update to that subroutine.
    if(mIsInvestigationMode)
    {
        UpdateInvestigationMode();
        return;
    }

    ///--[Name Entry]
    //--Updates name entry and stops this update.
    if(mIsNameEntryMode)
    {
        UpdateNameEntry();
        return;
    }
    else if(PairanormalMenu::xImmediateRename)
    {
        ActivateNameEntry();
        PairanormalMenu::xImmediateRename = false;
    }

    ///--[Appendation Handling]
    //--As long as there is anything to append, controls are suspended.
    if(mAnythingToAppend && !mIsHardBlocking)
    {
        ///--[Timer]
        if(mGlitchTimer >= mGlitchRunTimer)
        {
            if(PairanormalMenu::xSpeedupFlag)
            {
                mAppendNumerator = mAppendNumerator + ((mAppendSpeed * mSpeedFactor) * 10.0f);
            }
            else
            {
                mAppendNumerator = mAppendNumerator + (mAppendSpeed * mSpeedFactor);
            }
        }
        //--Runs at a slower speed while a glitch is running.
        else
        {
            mAppendNumerator = mAppendNumerator + (mAppendSpeed * 0.15f * mSpeedFactor);
        }

        ///--[Showing Text]
        //--As long as the numerator is greater than the denominator, append.
        while(mAppendNumerator >= mAppendDenominator)
        {
            //--Subtract.
            mAppendNumerator = mAppendNumerator - mAppendDenominator;

            //--Take the next letter and unhide it.
            int tStringLen = (int)strlen(mMainString);
            if(mCurrentAppendLetter < tStringLen)
            {
                //--Check the letter in question. If it starts with a brace, we need to handle a brace stack.
                char tLetter = mMainString[mCurrentAppendLetter];
                if(tLetter == '[')
                {
                    //--Setup.
                    int tBraceStack = 0;

                    //--Begin seeking forwards until we find the matching end brace.
                    int tTentativeLetter = mCurrentAppendLetter + 1;
                    while(tTentativeLetter < tStringLen)
                    {
                        //--Get the new letter.
                        char tNewLetter = mMainString[tTentativeLetter];

                        //--Increment brace stack.
                        if(tNewLetter == '[')
                        {
                            tBraceStack ++;
                        }
                        //--Decrement brace stack.
                        else if(tNewLetter == ']')
                        {
                            tBraceStack --;
                        }

                        //--If the brace stack hits -1, end this parse.
                        if(tBraceStack < 0)
                        {
                            break;
                        }

                        //--Next.
                        tTentativeLetter ++;
                    }

                    //--Take the string which was parsed out. Put it in a buffer and send it to the handler.
                    char tStringBuf[MAX_LETTERS];
                    strncpy(tStringBuf, &mMainString[mCurrentAppendLetter], tTentativeLetter - mCurrentAppendLetter + 1);
                    tStringBuf[tTentativeLetter - mCurrentAppendLetter + 1] = '\0';

                    //--Move up.
                    mCurrentAppendLetter = tTentativeLetter + 1;
                    //mCurrentAppendX --;

                    //--Handle special cases.
                    mReappend = false;
                    HandleTag(tStringBuf, true);

                    //--If the reappend flag is set, run another character.
                    if(mReappend) mAppendNumerator = mAppendNumerator + mAppendDenominator;
                    mReappend = false;
                    continue;
                }

                //--Increment.
                mCurrentAppendLetter ++;
                char cLetter = mSubstrings[mCurrentAppendY][mCurrentAppendX];
                mSubstringsDisplay[mCurrentAppendY][mCurrentAppendX] = true;

                //--Tick timer.
                mTextTickTimer = mTextTickTimer + 1.0f;
                if(mTextTickTimer >= mTextTickInterval)
                {
                    //--Reset.
                    mTextTickTimer = fmodf(mTextTickTimer, mTextTickInterval);

                    //--Don't play text ticks if on a punctuation.
                    if(cLetter != '.')
                    {
                        //--If Thought is active:
                        if(mIsSpeakerThought || mIsSpeakerInfo)
                        {
                            if(xAllowVoiceTicks) AudioManager::Fetch()->PlaySound("Voice|Thought");
                        }
                        //--Otherwise, use the last speaking character.
                        else
                        {
                            if(xAllowVoiceTicks) AudioManager::Fetch()->PlaySound(mVoiceTick);
                        }
                    }
                }

                //--Flag application.
                if(mCurrentTextFlags & TEXTFLAG_APPLY_GLITCH)
                {
                    //--Roll for what glitch to apply.
                    int tRoll = rand() % 100;

                    //--Mirror along the X axis.
                    if(tRoll < 25)
                    {
                        mSubstringFlags[mCurrentAppendY][mCurrentAppendX] = TEXTFLAG_MIRRORX;
                    }
                    //--Mirror along the Y axis.
                    else if(tRoll < 50)
                    {
                        mSubstringFlags[mCurrentAppendY][mCurrentAppendX] = TEXTFLAG_MIRRORY;
                    }
                    //--Double-render X flip.
                    else if(tRoll < 65)
                    {
                        mSubstringFlags[mCurrentAppendY][mCurrentAppendX] = TEXTFLAG_DOUBLERENDERX;
                    }
                    //--Double-render Y flip.
                    else if(tRoll < 80)
                    {
                        mSubstringFlags[mCurrentAppendY][mCurrentAppendX] = TEXTFLAG_DOUBLERENDERY;
                    }
                    //--All other cases, no glitch.
                    else
                    {
                        mSubstringFlags[mCurrentAppendY][mCurrentAppendX] = TEXTFLAG_NOTHING;
                    }
                }
                //--Italics. Exclusive with the glitch flag!
                else if(mCurrentTextFlags & TEXTFLAG_APPLY_ITALICS)
                {
                    mSubstringFlags[mCurrentAppendY][mCurrentAppendX] = TEXTFLAG_ITALICS;
                }

                //--Next letter.
                mCurrentAppendX ++;

                //--If this was the last letter on the line, move up.
                if(mCurrentAppendX >= MAX_LETTERS || mCurrentAppendX >= (int)strlen(mSubstrings[mCurrentAppendY]))
                {
                    //--Reset.
                    mCurrentAppendLetter ++;
                    mCurrentAppendY ++;
                    mCurrentAppendX = 0;

                    //--Ending case.
                    if(mCurrentAppendY >= MAX_LINES)
                    {
                        mAnythingToAppend = false;
                        break;
                    }
                }
            }
            //--String ended.
            else
            {
                mAnythingToAppend = false;
                break;
            }
        }
    }
    //--Reset tick interval.
    else
    {
        mTextTickTimer = 0.0f;
    }

    ///--[Input]
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Hard Block]
    //--Hard Block.
    if(mIsHardBlocking)
    {
        //--If any key was pressed, and it was NOT the recompile kerning key...
        if((rControlManager->IsAnyKeyPressed() || rControlManager->IsDown("Cancel")) && !rControlManager->IsDown("RebuildKerning"))
        {
            mIsHardBlocking = false;
        }
    }
    ///--[Wait on Keypress]
    //--If waiting on a keypress...
    else if(!mAnythingToAppend && mWaitingOnKeypress && !mShowDecisions)
    {
        //--If any key was pressed, and it was NOT the recompile kerning key...
        if((rControlManager->IsAnyKeyPressed() || rControlManager->IsDown("Cancel")) && !rControlManager->IsDown("RebuildKerning"))
        {
            mWaitingOnKeypress = false;
        }

        //--If SPEEDTEXT is in play, auto-advance.
        if(mSpeedFactor > 1.50f)
        {
            mWaitingOnKeypress = false;
        }
    }
    ///--[Decision Handler]
    //--If showing decisions.
    else if(!mAnythingToAppend && mShowDecisions)
    {
        //--If decisions need to reposition, handle it.
        if(mDecisionsNeedReposition) RepositionDecisions();

        ///--[Mouse Position]
        //--Get mouse location.
        float tMouseX, tMouseY, tMouseZ;
        rControlManager->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);

        ///--[Decision Highlighting]
        //--Scan the decisions to see which one the mouse is over.
        mHighlightedDecision = -1;

        //--Scan the decisions to see if it's over one of them.
        int i = 0;
        PairanormalDecisionPack *rPack = (PairanormalDecisionPack *)mDecisionList->PushIterator();
        while(rPack)
        {
            //--Check for a hit.
            if(rPack->mPosition.IsPointWithin(tMouseX, tMouseY))
            {
                mHighlightedDecision = i;
                mDecisionList->PopIterator();
                break;
            }

            //--Next.
            i ++;
            rPack = (PairanormalDecisionPack *)mDecisionList->AutoIterate();
        }

        ///--[Mouse Clicking]
        //--Player clicks the mouse at a location on screen.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Setup.
            PairanormalDecisionPack *rClickedPack = (PairanormalDecisionPack *)mDecisionList->GetElementBySlot(mHighlightedDecision);

            //--If this is not NULL, it was clicked. Execute it.
            if(rClickedPack)
            {
                //--Store the script and argument. We need to save it for later.
                char *tExecScriptPath = InitializeString(rClickedPack->mScriptPath);
                char *tExecScriptArg = InitializeString(rClickedPack->mScriptArg);

                //--Clear decision mode. This clears the clicked pack, but we stored the paths and can safely use them.
                mShowDecisions = false;
                mWaitingOnKeypress = false;
                mDecisionList->ClearList();

                //--Run the script.
                LuaManager::Fetch()->ExecuteLuaFile(tExecScriptPath, 1, "S", tExecScriptArg);

                //--Clean.
                free(tExecScriptPath);
                free(tExecScriptArg);
            }
        }
    }

    ///--[Position Update]
    //--If this flag is set, the characters all need to recompute their positions.
    if(mCharactersRecomputePositions || mCharacterFirstShowThisTick)
    {
        //--Check if we need to recompute positions based on just speaker changes.
        bool tOnlySpeakerChanged = mSpeakerChangedThisTick;
        if(mCharactersChangedVisibilityThisTick) tOnlySpeakerChanged = false;
        if(mCharacterFirstShowThisTick) tOnlySpeakerChanged = false;

        //--Only active slots need to recompute.
        for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
        {
            if(rActiveCharacters[i]) ResolvePositionForActor(rActiveCharacters[i], tOnlySpeakerChanged);
        }

        //--Unset the flags.
        mCharactersRecomputePositions = false;
        mCharacterFirstShowThisTick = false;
    }

    ///--[Loading]
    //--Max all positions.
    if(mIsLoad)
    {
        //--Flag.
        mIsLoad = false;

        //--Set.
        for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
        {
            if(rActiveCharacters[i]) rActiveCharacters[i]->MaxOutPosition();
        }
    }

    //--Reset these flags.
    mSpeakerChangedThisTick = false;
    mCharactersChangedVisibilityThisTick = false;
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void PairanormalDialogue::Render()
{
    ///--[Documentation and Setup]
    //--Renders the dialogue box, as you might expect. Letters append onto the end as the box updates.
    if(!Images.mIsReady) return;

    ///--[Border Bars]
    //--These make things look a little nicer.
    float cOffset = PairanormalLevel::Fetch()->GetScreenShake() * -1.0f;
    glDepthMask(false);
    Images.Data.rBackgroundBarsBot->Draw(cOffset, 0.0f);
    Images.Data.rBackgroundBarsTop->Draw(cOffset, 0.0f);
    glDepthMask(true);

    ///--[Debug Overlay]
    //--Show hit detection zones for examinable objects.
    if(xShowInvestigationHints)
    {
        //--Setup.
        float cXOffset = PairanormalLevel::Fetch()->GetBackgroundOffset() * BG_SCALE;
        float cYOffset = BG_OFFSET_Y * BG_SCALE;
        glDisable(GL_TEXTURE_2D);
        glLineWidth(2.0f);
        glTranslatef(cXOffset, cYOffset, 0.0f);

        //--Iterate.
        InvestigationPack *rPack = (InvestigationPack *)mInvestigationObjectList->PushIterator();
        while(rPack)
        {
            //--Render.
            glBegin(GL_LINE_LOOP);
                glVertex2f(rPack->mExaminePosition.mLft, rPack->mExaminePosition.mTop);
                glVertex2f(rPack->mExaminePosition.mRgt, rPack->mExaminePosition.mTop);
                glVertex2f(rPack->mExaminePosition.mRgt, rPack->mExaminePosition.mBot);
                glVertex2f(rPack->mExaminePosition.mLft, rPack->mExaminePosition.mBot);
            glEnd();

            rPack = (InvestigationPack *)mInvestigationObjectList->AutoIterate();
        }

        //--Clean.
        glTranslatef(-cXOffset, -cYOffset, 0.0f);
        glLineWidth(1.0f);
        glEnable(GL_TEXTURE_2D);
    }

    ///--[Image Mode]
    //--Fading image.
    if(rFadingImage && mImageFadeTimer < IMAGE_FADE_TICKS)
    {
        //--Centering.
        float cScale = 0.60f;
        float cImgWid = rFadingImage->GetWidth() * cScale;
        float cImgHei = rFadingImage->GetHeight() * cScale;
        float cXPos = (VIRTUAL_CANVAS_X - cImgWid) * 0.50f;
        float cYPos = ((VIRTUAL_CANVAS_Y * 0.80) - cImgHei) * 0.50f;

        //--Alpha.
        float cAlpha = 1.0f - (mImageFadeTimer / (float)IMAGE_FADE_TICKS);

        //--Render.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        glTranslatef(cXPos, cYPos, DEPTH_IMAGES);
        glScalef(cScale, cScale, 1.0f);
        rFadingImage->Draw();
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-cXPos, -cYPos, -DEPTH_IMAGES);
        StarlightColor::ClearMixer();
    }

    //--Renders under the rest of the dialogue.
    if(mIsImageDisplayMode && rActiveImage)
    {
        //--Centering.
        float cScale = 0.60f;
        float cImgWid = rActiveImage->GetWidth() * cScale;
        float cImgHei = rActiveImage->GetHeight() * cScale;
        float cXPos = (VIRTUAL_CANVAS_X - cImgWid) * 0.50f;
        float cYPos = ((VIRTUAL_CANVAS_Y * 0.80) - cImgHei) * 0.50f;

        //--Alpha.
        float cAlpha = (mImageFadeTimer / (float)IMAGE_FADE_TICKS);

        //--Render.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        glTranslatef(cXPos, cYPos, DEPTH_IMAGES);
        glScalef(cScale, cScale, 1.0f);
        rActiveImage->Draw();
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-cXPos, -cYPos, -DEPTH_IMAGES);
        StarlightColor::ClearMixer();
    }

    ///--[Discovered Object]
    //--If there is one, display it under the UI.
    if(rShowDiscoveredObject && rShowDiscoveredObject->rIconImg)
    {
        //--Backing.
        Images.Investigation.rCloseupBacking->Draw();

        //--Position.
        float cLft = 450.0f;
        float cTop = 139.0f;
        float cRgt = cLft + 482.0f;
        float cBot = cTop + 402.0f;
        float cInd = 25.0f;

        //--Bind it and render it in the object zone with an indent.
        rShowDiscoveredObject->rIconImg->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(rShowDiscoveredObject->mIconU1, rShowDiscoveredObject->mIconV1); glVertex2f(cLft + cInd, cTop + cInd);
            glTexCoord2f(rShowDiscoveredObject->mIconU2, rShowDiscoveredObject->mIconV1); glVertex2f(cRgt - cInd, cTop + cInd);
            glTexCoord2f(rShowDiscoveredObject->mIconU2, rShowDiscoveredObject->mIconV2); glVertex2f(cRgt - cInd, cBot - cInd);
            glTexCoord2f(rShowDiscoveredObject->mIconU1, rShowDiscoveredObject->mIconV2); glVertex2f(cLft + cInd, cBot - cInd);
        glEnd();
    }

    ///--[Main PairanormalDialogue]
    //--Doesn't render during investigation mode.
    if(!mIsInvestigationMode)
    {
        ///--[Characters]
        //--Activate the shader if in glitch mode.
        if(mGlitchTimer < mGlitchRunTimer && (mGlitchFlags & DIAGLITCH_COLINV)) DisplayManager::Fetch()->ActivateProgram("PairanormalDialogue ColorInvert");

        //--Render characters using automatic centering.
        for(int i = 0; i < MAX_ACTIVE_CHARACTERS; i ++)
        {
            if(rActiveCharacters[i]) rActiveCharacters[i]->Render(mCharacterZoom);
        }

        //--All characters who did not render here will render if they are fading out.
        PairanormalDialogueCharacter *rCharacter = (PairanormalDialogueCharacter *)mCharactersList->PushIterator();
        while(rCharacter)
        {
            if(rCharacter->IsFadingOut()) rCharacter->Render(1.0f);
            rCharacter = (PairanormalDialogueCharacter *)mCharactersList->AutoIterate();
        }

        //--Clean up.
        DisplayManager::Fetch()->ActivateProgram(NULL);

        //--Backing.
        Images.Data.rBackgroundBarUnder->Draw();

        ///--[Major Scene Image]
        //--If it exists, render it. Major scene objects are downscaled.
        if(rMajorSceneImg)
        {
            float cScale = 0.5f;
            float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rMajorSceneImg->GetTrueWidth() * 0.5f * cScale);
            float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rMajorSceneImg->GetTrueHeight() * 0.5f * cScale);
            rMajorSceneImg->DrawScaled(tXPos, tYPos, 0, cScale);
        }

        ///--[Backing]
        //--Previous text box, if it exists, renders.
        float cCrossfade = EasingFunction::QuadraticInOut(mChangeTextBoxesTimer, TEXTBOX_FADE_TICKS);
        if(rPreviousTextBox && mChangeTextBoxesTimer < TEXTBOX_FADE_TICKS)
        {
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f - cCrossfade);
            rPreviousTextBox->Draw();
        }

        //--The text box, if it exists, renders.
        if(rCurrentTextBox)
        {
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cCrossfade);
            rCurrentTextBox->Draw();
        }

        ///--[Text]
        //--Setup.
        char tBuffer[2];
        tBuffer[1] = '\0';
        StarlightColor::ClearMixer();

        //--Resolve positions. PairanormalDialogue switches places if rendering on the info box.
        float cUseLft = mDialogueBoxDim.mLft + 20.0f;
        float cUseTop = mDialogueBoxDim.mTop + 22.0f;
        if(mIsSpeakerInfo)
        {
            cUseLft = 436.0f;
            cUseTop = 310.0f;
        }
        //--Credits use a spot closer to the middle of the screen.
        else if(mIsSpeakerCredits)
        {
            cUseTop = 300.0f;
        }

        //--If using a white box, switch to using black dialogue.
        if(rCurrentTextBox == Images.Data.rDialogueBoxWhite)
        {
            StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        }

        //--Render text. This is done per-letter.
        for(int y = 0; y < MAX_LINES; y ++)
        {
            //--Running position.
            float tRunningX = cUseLft;

            //--Iterate.
            for(int x = 0; x < MAX_LETTERS; x ++)
            {
                //--If the letter is not set to display, stop here.
                if(!mSubstringsDisplay[y][x]) break;

                //--Display the letter.
                tBuffer[0] = mSubstrings[y][x];

                //--Normal case:
                if(mSubstringFlags[y][x] == TEXTFLAG_NOTHING)
                {
                    Images.Data.rDialogueFont->DrawText(tRunningX, cUseTop + (y * 35.0f), 0, cFontScale, tBuffer);
                }
                //--Italics.
                else if(mSubstringFlags[y][x] == TEXTFLAG_ITALICS)
                {
                    Images.Data.rDialogueFont->DrawText(tRunningX, cUseTop + (y * 35.0f), SUGARFONT_ITALIC, cFontScale, tBuffer);
                }
                //--Mirror along the X axis.
                else if(mSubstringFlags[y][x] == TEXTFLAG_MIRRORX)
                {
                    Images.Data.rDialogueFont->DrawText(tRunningX, cUseTop + (y * 35.0f), SUGARFONT_MIRRORX, cFontScale, tBuffer);
                }
                //--Mirror along the Y axis.
                else if(mSubstringFlags[y][x] == TEXTFLAG_MIRRORY)
                {
                    Images.Data.rDialogueFont->DrawText(tRunningX, cUseTop + (y * 35.0f), SUGARFONT_MIRRORY, cFontScale, tBuffer);
                }
                //--Double-render X axis.
                else if(mSubstringFlags[y][x] == TEXTFLAG_DOUBLERENDERX)
                {
                    Images.Data.rDialogueFont->DrawText(tRunningX, cUseTop + (y * 35.0f), SUGARFONT_DOUBLERENDERX, cFontScale, tBuffer);
                }
                //--Double-render Y axis.
                else if(mSubstringFlags[y][x] == TEXTFLAG_DOUBLERENDERY)
                {
                    Images.Data.rDialogueFont->DrawText(tRunningX, cUseTop + (y * 35.0f), SUGARFONT_DOUBLERENDERY, cFontScale, tBuffer);
                }

                //--Check the next letter. If there isn't one, it defaults to 0.
                char cNextLetter = 0;
                if(mSubstringsDisplay[y][x+1]) cNextLetter = mSubstrings[y][x+1];

                //--Move the cursor.
                tRunningX = tRunningX + (Images.Data.rDialogueFont->GetKerningBetween(mSubstrings[y][x], cNextLetter) * cFontScale);
            }
        }
        StarlightColor::ClearMixer();

        ///--[Advance Arrow]
        //--When not printing dialogue and waiting for a keypress, show this arrow.
        if(!mAnythingToAppend && mWaitingOnKeypress && !mShowDecisions)
        {
            Images.Data.rContinueIcon->Draw();
        }
        //--Hard break.
        else if(mIsHardBlocking)
        {
            Images.Data.rContinueIcon->Draw();
        }

        ///--[Name Box]
        //--Info boxes render in a very different place and use the name "Info".
        if(mIsSpeakerInfo)
        {
            Images.Data.rInfoBoxTop->Draw();
            Images.Data.rHeadingFont->DrawText(551.0f, 260.0f, 0, 1.0f, "Info");
        }
        //--Thought boxes never render a name card.
        else if(mIsSpeakerThought || mIsSpeakerCredits)
        {
        }
        //--For the player, render on the right and always show "You". Thoughts do not render a name bar.
        else if(mIsSpeakerPlayer)
        {
            Images.Data.rNameBarRgt->Draw();
            Images.Data.rHeadingFont->DrawText(967.0f, 520.0f, 0, 1.0f, "You");
        }
        //--General case, renders on the left.
        else
        {
            Images.Data.rNameBarLft->Draw();
            Images.Data.rHeadingFont->DrawText(100.0f, 520.0f, 0, 1.0f, mSpeakerName);
        }

        ///--[Decisions]
        //--Constants.
        float cDecBoxOffBlkLft = -15.0f;
        float cDecBoxOffBlkTop =  -5.0f;
        float cDecBoxOffWhtLft = -50.0f;
        float cDecBoxOffWhtTop =  -5.0f;
        float cDecTxtOffTop = 0.0f;
        float cDecTxtScale = 1.0f;

        //--Images.
        SugarBitmap *rUseBoxBlock = Images.Data.rDecisionBoxBlack;
        SugarBitmap *rUseBoxWhite = Images.Data.rDecisionBoxWhite;
        float cBlkOffset = cDecBoxOffBlkLft;
        float cWhtOffset = cDecBoxOffWhtLft;
        if(mDecisionTwoLists)
        {
            rUseBoxBlock = Images.Data.rDecisionBoxBlackHf;
            rUseBoxWhite = Images.Data.rDecisionBoxWhiteHf;
            cBlkOffset = cBlkOffset / 2.0f;
            cWhtOffset = cWhtOffset / 2.0f;
        }

        //--If decisions are active and nothing is scrolling, show the decisions.
        if(!mAnythingToAppend && mShowDecisions)
        {
            //--If decisions need to reposition, handle it.
            if(mDecisionsNeedReposition) RepositionDecisions();

            //--Iterate.
            int i = 0;
            PairanormalDecisionPack *rPack = (PairanormalDecisionPack *)mDecisionList->PushIterator();
            while(rPack)
            {
                //--Text positions.
                float cTextWid = Images.Data.rHeadingFont->GetTextWidth(rPack->mDisplayText) * cDecTxtScale;
                float cTextLft = rPack->mPosition.mXCenter - (cTextWid * 0.50f);
                float cTextScale = 1.0f;
                if(cTextWid >= 660.0f)
                {
                    cTextScale = 660.0f / cTextWid;
                    cTextLft = rPack->mPosition.mXCenter - (660.0f * 0.50f);
                }

                //--Render a black box at the position when not highlighted.
                if(mHighlightedDecision != i)
                {
                    rUseBoxBlock->Draw(rPack->mPosition.mLft + cBlkOffset, rPack->mPosition.mTop + cDecBoxOffBlkTop);
                    Images.Data.rHeadingFont->DrawText(cTextLft, rPack->mPosition.mTop + cDecTxtOffTop, 0, cDecTxtScale * cTextScale, rPack->mDisplayText);
                }
                //--White box. The offsets center the white box differently.
                else
                {
                    rUseBoxWhite->Draw(rPack->mPosition.mLft + cWhtOffset, rPack->mPosition.mTop + cDecBoxOffWhtTop);
                    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
                    Images.Data.rHeadingFont->DrawText(cTextLft, rPack->mPosition.mTop + cDecTxtOffTop, 0, cDecTxtScale * cTextScale, rPack->mDisplayText);
                    StarlightColor::ClearMixer();
                }

                //--Next.
                i ++;
                rPack = (PairanormalDecisionPack *)mDecisionList->AutoIterate();
            }
        }
    }

    ///--[Investigation]
    //--Render the investigation UI over everything else.
    if(mIsInvestigationMode) RenderInvestigationMode();

    //--Time Bar. This renders independent of the investigation. Set the max time to -1 to hide it.
    if(mInvestigationTimeMax > 0)
    {
        //--Base.
        Images.Investigation.rTimerBarBlack->Draw();

        //--White overlay.
        if(mInvestigationTimeLeft > 0)
        {
            float tPer = 1.0f - ((float)mInvestigationTimeLeft / (float)mInvestigationTimeMax);
            Images.Investigation.rTimerBarWhite->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(tPer, 0.0f); glVertex2f(1105.0f + (220.0f * tPer), 15.0f);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(1105.0f + (220.0f * 1.0f), 15.0f);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(1105.0f + (220.0f * 1.0f), 20.0f);
                glTexCoord2f(tPer, 1.0f); glVertex2f(1105.0f + (220.0f * tPer), 20.0f);
            glEnd();
        }
    }

    ///--[Name Entry]
    //--Render this over everything else.
    if(mIsNameEntryMode) RenderNameEntry();

    ///--[Clean]
    //--Clear the mixer.
    StarlightColor::ClearMixer();

    ///--[Glitch Overlay]
    //--Render the glitch overlay.
    if(mGlitchTimer < mGlitchRunTimer && (mGlitchFlags & DIAGLITCH_LINES) && OptionsManager::Fetch()->GetOptionB("Pairanormal Flashing Images"))
    {
        Images.Data.rVHSOverlay->Draw();
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
PairanormalDialogue *PairanormalDialogue::Fetch()
{
    //--If the static copy has not been instantiated yet, do that here.
    if(!xStaticCopy)
    {
        xStaticCopy = new PairanormalDialogue();
        xStaticCopy->Construct();
    }

    return xStaticCopy;
}

///======================================== Lua Hooking ===========================================
void PairanormalDialogue::HookToLuaState(lua_State *pLuaState)
{
    /* Dia_CreateCharacter(sName)
       Creates and pushes a new PairanormalDialogueCharacter which can be shown in the dialogue. */
    lua_register(pLuaState, "Dia_CreateCharacter", &Hook_Dia_CreateCharacter);

    /* Dia_SetProperty("Set Dialogue", sString)
       Dia_SetProperty("Activate Decisions")
       Dia_SetProperty("Append Decision", sDisplayText, sScriptPath, sArgument)
       Dia_SetProperty("Start Investigation")
       Dia_SetProperty("Stop Investigation")
       Dia_SetProperty("Clear Investigation")
       Dia_SetProperty("Set Go Home Strings", sScriptPath, sScriptArg)
       Dia_SetProperty("Clear Discovered Object")
       Dia_SetProperty("Add Investigation Object", sObjectName, fLft, fTop, fRgt, fBot, sIconPath, sImgPath, sScriptPath, sScriptArg)
       Dia_SetProperty("Add Discovered Object", sObjectName, sIconPath, sImgPath, sScriptPath, sScriptArg)
       Dia_SetProperty("Add Talkable Character", sCharacterName, sScriptPath)
       Dia_SetProperty("Show Scene Image", sDLPath)
       Dia_SetProperty("Hide Scene Image")
       Dia_SetProperty("Get Investigation Time") (1 Integer)
       Dia_SetProperty("Investigation Time Max", iAmount)
       Dia_SetProperty("Investigation Time Left", iAmount)
       Sets the requested property in the global PairanormalDialogue. */
    lua_register(pLuaState, "Dia_SetProperty", &Hook_Dia_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Dia_CreateCharacter(lua_State *L)
{
    //Dia_CreateCharacter(sName)
    int tArgs = lua_gettop(L);
    DataLibrary::Fetch()->PushActiveEntity();
    if(tArgs < 1) return LuaArgError("Dia_CreateCharacter");

    //--Create.
    PairanormalDialogueCharacter *nCharacter = new PairanormalDialogueCharacter();
    PairanormalDialogue::Fetch()->RegisterCharacter(lua_tostring(L, 1), nCharacter);
    DataLibrary::Fetch()->rActiveObject = nCharacter;
    return 0;
}
int Hook_Dia_SetProperty(lua_State *L)
{
    ///--[Argument List]
    //--[Static]
    //Dia_SetProperty("Set Investigation Image", sString) (Static)
    //Dia_SetProperty("Set Investigation Script", sString) (Static)

    //--[Remaps]
    //Dia_SetProperty("Add Tag Remap", sOriginal, sRemapTo)

    //--[Dialogue]
    //Dia_SetProperty("Set Dialogue", sString)
    //Dia_SetProperty("Add Character Alias", sBaseName, sAlias)

    //--[Decisions]
    //Dia_SetProperty("Activate Decisions")
    //Dia_SetProperty("Append Decision", sDisplayText, sScriptPath, sArgument)

    //--[Name Entry]
    //Dia_SetProperty("Player Name", sString)
    //Dia_SetProperty("Name Entry String", sString)

    //--[Investigation]
    //Dia_SetProperty("Start Investigation")
    //Dia_SetProperty("Stop Investigation")
    //Dia_SetProperty("Clear Investigation")
    //Dia_SetProperty("Set Go Home Strings", sScriptPath, sScriptArg)
    //Dia_SetProperty("Clear Discovered Object")
    //Dia_SetProperty("Parse SLF File", sPath)
    //Dia_SetProperty("Add Investigation Object", sObjectName, fLft, fTop, fRgt, fBot)
    //Dia_SetProperty("Add Investigation Object", sObjectName, fLft, fTop, fRgt, fBot, sIconPath, sScriptPath, sScriptArg)
    //Dia_SetProperty("Add Investigation Object", sObjectName, fLft, fTop, fRgt, fBot, sIconPath, sScriptPath, sScriptArg, fX1, fY1, fX2, fY2)
    //Dia_SetProperty("Add Discovered Object", sObjectName, sIconPath, sScriptPath, sScriptArg)
    //Dia_SetProperty("Add Discovered Object", sObjectName, sIconPath, sScriptPath, sScriptArg, fX1, fY1, fX2, fY2)
    //Dia_SetProperty("Rem Discovered Object", sObjectName)
    //Dia_SetProperty("Add Talkable Character", sCharacterName, sScriptPath)
    //Dia_SetProperty("Get Investigation Time") (1 Integer)
    //Dia_SetProperty("Investigation Time Max", iAmount)
    //Dia_SetProperty("Investigation Time Left", iAmount)
    //Dia_SetProperty("Clear Investigation Objects")

    //--[Scenes]
    //Dia_SetProperty("Show Scene Image", sDLPath)
    //Dia_SetProperty("Hide Scene Image")

    ///--[Arg Check]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Dia_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Statics]
    if(!strcasecmp(rSwitchType, "Set Investigation Image") && tArgs == 2)
    {
        ResetString(PairanormalDialogue::xInvestigationStdImg, lua_tostring(L, 2));
        return 0;
    }
    else if(!strcasecmp(rSwitchType, "Set Investigation Script") && tArgs == 2)
    {
        ResetString(PairanormalDialogue::xInvestigationStdPath, lua_tostring(L, 2));
        return 0;
    }

    ///--[Dynamic]
    //--Active object.
    PairanormalDialogue *rPairanormalDialogue = PairanormalDialogue::Fetch();

    ///--[Bypass on Checkpoint]
    //--Does nothing if currently running to a checkpoint.
    if(RootEvent::xCurrentCheckpointState == true)
    {
        //--Investigation timer.
        if(!strcasecmp(rSwitchType, "Get Investigation Time") && tArgs == 1)
        {
            //fprintf(stderr, "Can't get time, running to checkpoint.\n");
            lua_pushinteger(L, rPairanormalDialogue->GetInvestigationTime());
            return 1;
        }
        //--Dialogue. Only shows/hides characters.
        else if(!strcasecmp(rSwitchType, "Set Dialogue") && tArgs == 2)
        {
            //fprintf(stderr, "Can't get time, running to checkpoint.\n");
            rPairanormalDialogue->AppendDialogueOnlyChars(lua_tostring(L, 2));
            return 0;
        }

        return 0;
    }

    ///--[Remaps]
    //--Adds a tag remap.
    if(!strcasecmp(rSwitchType, "Add Tag Remap") && tArgs == 3)
    {
        rPairanormalDialogue->RegisterTagRemap(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Dialogue]
    //--Sets the dialogue. Implicitly clears previous dialogue.
    else if(!strcasecmp(rSwitchType, "Set Dialogue") && tArgs == 2)
    {
        rPairanormalDialogue->AppendDialogue(lua_tostring(L, 2));
    }
    //--Remaps a name when selecting focus/voice ticks.
    else if(!strcasecmp(rSwitchType, "Add Character Alias") && tArgs == 3)
    {
        rPairanormalDialogue->AddCharacterAlias(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Decisions]
    //--Activates decision mode once dialogue stops scrolling.
    else if(!strcasecmp(rSwitchType, "Activate Decisions") && tArgs == 1)
    {
        rPairanormalDialogue->ActivateDecisions();
    }
    //--Adds a decision.
    else if(!strcasecmp(rSwitchType, "Append Decision") && tArgs == 4)
    {
        rPairanormalDialogue->AppendDecision(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    ///--[Name Entry]
    //--Player's name as seen in the name input screen.
    else if(!strcasecmp(rSwitchType, "Player Name") && tArgs == 2)
    {
        rPairanormalDialogue->SetPlayerName(lua_tostring(L, 2));
    }
    //--String to show during name entry.
    else if(!strcasecmp(rSwitchType, "Name Entry String") && tArgs == 2)
    {
        rPairanormalDialogue->SetNameEntryString(lua_tostring(L, 2));
    }
    ///--[Investigation]
    //--Starts investigating.
    else if(!strcasecmp(rSwitchType, "Start Investigation") && tArgs == 1)
    {
        rPairanormalDialogue->ActivateInvestigationMode();
    }
    //--Stops or Suspends investigating.
    else if(!strcasecmp(rSwitchType, "Stop Investigation") && tArgs == 1)
    {
        rPairanormalDialogue->DeactivateInvestigationMode();
    }
    //--Clears investigation data. Use before activating investigation the first time.
    else if(!strcasecmp(rSwitchType, "Clear Investigation") && tArgs == 1)
    {
        rPairanormalDialogue->ClearInvestigationData();
    }
    //--Clears investigation characters. Only use when the investigation is totally over!
    else if(!strcasecmp(rSwitchType, "Clear Investigation Characters") && tArgs == 1)
    {
        rPairanormalDialogue->ClearInvestigationCharacterData();
    }
    //--Sets what happens when the player clicks the Go Home button.
    else if(!strcasecmp(rSwitchType, "Set Go Home Strings") && tArgs == 3)
    {
        rPairanormalDialogue->SetGoHomeStrings(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets what happens when the player clicks the Go Home button.
    else if(!strcasecmp(rSwitchType, "Clear Discovered Object") && tArgs == 1)
    {
        rPairanormalDialogue->ClearDiscoveredObjectPtr();
    }
    //--Uses the provided SLF file to get examinable object positions.
    else if(!strcasecmp(rSwitchType, "Parse SLF File") && tArgs == 2)
    {
        rPairanormalDialogue->ParseSLFFile(lua_tostring(L, 2));
    }
    //--Adds an object to investigate, re-using arguments and using statics when possible.
    else if(!strcasecmp(rSwitchType, "Add Investigation Object") && tArgs == 6)
    {
        TwoDimensionReal tDim;
        tDim.Set(lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
        rPairanormalDialogue->AddInvestigationObject(lua_tostring(L, 2), tDim, PairanormalDialogue::xInvestigationStdImg, PairanormalDialogue::xInvestigationStdPath, lua_tostring(L, 2));
    }
    //--Adds an object to investigate.
    else if(!strcasecmp(rSwitchType, "Add Investigation Object") && tArgs == 9)
    {
        TwoDimensionReal tDim;
        tDim.Set(lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
        rPairanormalDialogue->AddInvestigationObject(lua_tostring(L, 2), tDim, lua_tostring(L, 7), lua_tostring(L, 8), lua_tostring(L, 9));
    }
    //--Adds an object to investigate, with specified UV coordinates.
    else if(!strcasecmp(rSwitchType, "Add Investigation Object") && tArgs == 13)
    {
        TwoDimensionReal tDim;
        tDim.Set(lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
        rPairanormalDialogue->AddInvestigationObject(lua_tostring(L, 2), tDim, lua_tostring(L, 7), lua_tostring(L, 8), lua_tostring(L, 9), lua_tonumber(L, 10), lua_tonumber(L, 11), lua_tonumber(L, 12), lua_tonumber(L, 13));
    }
    //--Adds an object to discovered list.
    else if(!strcasecmp(rSwitchType, "Add Discovered Object") && tArgs == 5)
    {
        rPairanormalDialogue->AddDiscoveredObject(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5));
    }
    //--Adds an object to discovered list, with specified UV coordinates.
    else if(!strcasecmp(rSwitchType, "Add Discovered Object") && tArgs == 9)
    {
        rPairanormalDialogue->AddDiscoveredObject(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8), lua_tonumber(L, 9));
    }
    //--Removes an object from the discovered list.
    else if(!strcasecmp(rSwitchType, "Rem Discovered Object") && tArgs == 2)
    {
        rPairanormalDialogue->RemoveDiscoveredObject(lua_tostring(L, 2));
    }
    //--Adds an object to discovered list.
    else if(!strcasecmp(rSwitchType, "Add Talkable Character") && tArgs == 3)
    {
        rPairanormalDialogue->RegisterTalkableCharacter(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Returns how many units are left of investigation time.
    else if(!strcasecmp(rSwitchType, "Get Investigation Time") && tArgs == 1)
    {
        lua_pushinteger(L, rPairanormalDialogue->GetInvestigationTime());
        return 1;
    }
    //--Sets max time. Set to -1 for it to be unlimited.
    else if(!strcasecmp(rSwitchType, "Investigation Time Max") && tArgs == 2)
    {
        rPairanormalDialogue->SetInvestigationTimeMax(lua_tointeger(L, 2));
    }
    //--Sets investigation time left.
    else if(!strcasecmp(rSwitchType, "Investigation Time Left") && tArgs == 2)
    {
        rPairanormalDialogue->SetInvestigationTime(lua_tointeger(L, 2));
    }
    //--Clears investigation objects between SLF files. Used when switching rooms.
    else if(!strcasecmp(rSwitchType, "Clear Investigation Objects") && tArgs == 1)
    {
        rPairanormalDialogue->ClearInvestigationObjects();
    }
    ///--[Scenes]
    //--Shows a scene image.
    else if(!strcasecmp(rSwitchType, "Show Scene Image") && tArgs == 2)
    {
        rPairanormalDialogue->ShowMajorScene(lua_tostring(L, 2));
    }
    //--Hides scene images.
    else if(!strcasecmp(rSwitchType, "Hide Scene Image") && tArgs == 1)
    {
        rPairanormalDialogue->HideMajorScene();
    }
    //--Error.
    else
    {
        LuaPropertyError("Dia_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
