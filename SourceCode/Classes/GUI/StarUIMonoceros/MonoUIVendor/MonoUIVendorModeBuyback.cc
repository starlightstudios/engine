//--Base
#include "MonoUIVendor.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

///===================================== Segment Rendering ========================================
void MonoUIVendor::RenderLftBuyback(float pVisAlpha)
{
    AdvUIVendor::RenderLftBuyback(pVisAlpha);
}
void MonoUIVendor::RenderTopBuyback(float pVisAlpha)
{
    AdvUIVendor::RenderTopBuyback(pVisAlpha);
}
void MonoUIVendor::RenderRgtBuyback(float pVisAlpha)
{
    AdvUIVendor::RenderRgtBuyback(pVisAlpha);
}
void MonoUIVendor::RenderBotBuyback(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Fast-access pointers.
    StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();

    ///--[Render]
    //--Tabs, titles.
    RenderModeTabs(cColorAlpha);
    RenderModeTitles(cColorAlpha);

    //--Backings. Rendered apart from the entries, allowing the cursor to be over the backing and under
    //  the text entries.
    float cEntrySpcH = 56.0f;
    int tBackingsToRender = rBuybackList->GetListSize();
    if(tBackingsToRender > cxMonoScrollPageSize) tBackingsToRender = cxMonoScrollPageSize;
    for(int i = 0; i < tBackingsToRender; i ++)
    {
        MonoImages.rOverlay_Option->Draw(0.0f, i * cEntrySpcH);
    }

    //--Cursor.
    MonoImages.rOverlay_Selected->Draw(0.0f, mHighlightPos.mYCur - MonoImages.rOverlay_Selected->GetYOffset());

    //--Render entries.
    RenderBuybackItemListing(mSkip, cScrollPage, cColorAlpha);

    //--Note: Scrollbars are not nominally possible since the buyback list has a hard size cap.

    //--If there are no entries on the item list, render this instead.
    if(rBuybackList->GetListSize() < 1)
    {
        AdvImages.rFont_Mainline->DrawText(345.0f, 242.0f, 0, 1.0f, xrStringData->str.mTextNothingBuyback);
    }

    //--Help Strings.
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--This string only renders if character swapping is possible.
    if(mShowSwapArrows)
        mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--This string only renders if multiple compare slots are present.
    if(mCanSwapCompareSlot)
        mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, MonoImages.rFont_Control);

}
void MonoUIVendor::RenderUnalignedBuyback(float pVisAlpha)
{
    AdvUIVendor::RenderUnalignedBuyback(pVisAlpha);
}
