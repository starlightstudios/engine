///======================================= ShakeMinigame ==========================================
//--Shaking minigame, used when an entity grabs the player and begins inflicting damage. The player
//  needs to mash keys to get them off.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class ShakeMinigame : public RootObject
{
    private:
    ///--[System]
    bool mIsPendingRemoval;
    TilemapActor *rGrabbingActor;

    ///--[Keypresses]
    int mPlayerKeypresses;
    int mPlayerKeypressesNeeded;

    ///--[Animation]
    int mPlayerAnimTimer;
    int mPlayerAnimDenom;

    protected:

    public:
    //--System
    ShakeMinigame();
    virtual ~ShakeMinigame();

    //--Public Variables
    //--Property Queries
    bool IsComplete();

    //--Manipulators
    void SetGrabbingActorS(const char *pName);
    void SetGrabbingActorP(TilemapActor *pActor);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();
    void HandlePlayerControls();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    static ShakeMinigame *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_ShakeMinigame_Create(lua_State *L);
int Hook_ShakeMinigame_GetProperty(lua_State *L);
int Hook_ShakeMinigame_SetProperty(lua_State *L);
