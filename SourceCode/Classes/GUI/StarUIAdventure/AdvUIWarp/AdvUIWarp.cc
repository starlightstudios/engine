//--Base
#include "AdvUIWarp.h"
#include "AdvUIWarpStructures.h"

//--Classes
#include "AdvUIBase.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIWarp::AdvUIWarp()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_ADVUIWARP;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AWRP");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = (VIRTUAL_CANVAS_X * 0.50f);
    mGridRenderCenterY  = (VIRTUAL_CANVAS_Y * 0.30f) + 68.0f;
    mGridRenderSpacingX = (VIRTUAL_CANVAS_X * 0.10f);
    mGridRenderSpacingY = (VIRTUAL_CANVAS_X * 0.10f);

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ======= AdvUIWarp ======== ]
    ///--[System]
    mLocationVisTimer = 0;
    mAdvancedPackRoll = 0;
    mAdvancedPackRerollTimer = 0;

    ///--[Display]
    mWarpZoomFactor = 1.0f / 2.0f;

    ///--[Region Grid]
    mRegionData = new StarLinkedList(true);

    ///--[Cursor]
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    ///--[Location Selection]
    mIsSelectingLocation = false;
    mLocationOffsetTimer = 0;
    mLocationCursor = 0;
    mLocationPos.Initialize();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Strings
    AllocateHelpStrings(cxAdvHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIWarp::~AdvUIWarp()
{
    delete mRegionData;
}
void AdvUIWarp::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Warp UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI", &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = AdvImages.rFont_Mainline;
}

///--[Public Statics]
//--Script that fires when the menu initializes. Allows the player to warp.
char *AdvUIWarp::xWarpResolveScript = NULL;

//--Script that fires when the player elects to warp.
char *AdvUIWarp::xWarpExecuteScript = NULL;

///===================================== Property Queries =========================================
bool AdvUIWarp::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    if(pType == POINTER_TYPE_ADVUIWARP) return true;
    return false;
}

///======================================= Manipulators ===========================================
void AdvUIWarp::TakeForeground()
{
    ///--[Documentation and Setup]
    //--Activates warp mode. Clears internal lists and calls the warp handler to build a list of
    //  warpable regions. The caller should check if this function was immediately followed by a FlagExit()
    //  call, in which case there was an error.
    ClearGrid();

    //--Reset the backwards code. It may have been previously set to AM_CAMPFIRE_MODE_WARP_EXIT_NOW which
    //  orders the calling UI to close when this object exits.
    SetCodeBackward(AM_CAMPFIRE_MODE_WARP);

    ///--[Run Lua]
    //--If there is no warp resolve script, stop.
    if(!xWarpResolveScript) { FlagExit(); return; }

    //--Execute the warp list builder. This will populate the list with all of the regions and locations
    //  the player can warp to. The name provided to the script is the room we are currently in.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) { FlagExit(); return; }

    //--Exec.
    LuaManager::Fetch()->ExecuteLuaFile(xWarpResolveScript, 1, "S", rActiveLevel->GetName());

    //--If no warp locations resolved for any reason, fail.
    if(mRegionData->GetListSize() < 1) { FlagExit(); return; }

    ///--[Trim]
    //--Go through the region list. Any regions with zero locations are trimmed.
    WarpRegionPack *rDataPack = (WarpRegionPack *)mRegionData->SetToHeadAndReturn();
    while(rDataPack)
    {
        //--If the locations list has no entries, remove.
        if(rDataPack->mLocations->GetListSize() < 1)
        {
            mRegionData->RemoveRandomPointerEntry();
        }

        //--Next.
        rDataPack = (WarpRegionPack *)mRegionData->IncrementAndGetRandomPointerEntry();
    }

    //--If there are no regions after trimming, fail.
    if(mRegionData->GetListSize() < 1) { FlagExit(); return; }

    ///--[Success]
    //--All checks passed, it is possible to warp. Play SFX to indicate the menu is opened.
    AudioManager::Fetch()->PlaySound("Menu|Select");

    ///--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    AutoAllocateSquareGrid(mRegionData->GetListSize());

    //--Process the grid to set the icons to their positions.
    ProcessGrid();

    //--Populate region data into cells in the grid.
    int i = 0;
    WarpRegionPack *rWarpPack = (WarpRegionPack *)mRegionData->PushIterator();
    while(rWarpPack)
    {
        //--Retrieve a grid package. If it's NULL then we ran out of space, bark an error.
        GridPackage *rGridPack = GetNextPriorityEntry();
        if(!rGridPack)
        {
            fprintf(stderr, "AdvUIWarp:TakeForeground() - Warning, region data has %i entries but the grid ran out of slots. Failing.\n", mRegionData->GetListSize());
            mRegionData->PopIterator();
            break;
        }

        //--Copy across.
        rGridPack->mCode = i;
        strcpy(rGridPack->mLocalName, mRegionData->GetIteratorName());
        memcpy(&rGridPack->mAlignments, &rWarpPack->mAlignments, sizeof(AdvMenuStandardAlignments));

        //--Next.
        i ++;
        rWarpPack = (WarpRegionPack *)mRegionData->AutoIterate();
    }

    ///--[Run Cursor]
    //--Update the cursor so it focuses on the selected entry.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}
void AdvUIWarp::RegisterWarpRegion(const char *pRegionName, const char *pIconPath, const char *pMapImagePath, const char *pMapOverlayPath)
{
    ///--[Documentation]
    //--Creates a new region the player can select to warp to.
    if(!pRegionName || !pIconPath || !pMapImagePath || !pMapOverlayPath) return;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Allocate Pack]
    //--Allocate.
    WarpRegionPack *nRegionPack = (WarpRegionPack *)starmemoryalloc(sizeof(WarpRegionPack));
    nRegionPack->Initialize();
    nRegionPack->mAlignments.rImage = (StarBitmap *)rDataLibrary->GetEntry(pIconPath);
    nRegionPack->rMapImage          = NULL;
    nRegionPack->rMapOverlayImage   = (StarBitmap *)rDataLibrary->GetEntry(pMapOverlayPath);

    ///--[Normal Maps]
    //--If the map image path is a normal path, use that.
    if(strcasecmp(pMapImagePath, "ADVANCED"))
    {
        nRegionPack->rMapImage = (StarBitmap *)rDataLibrary->GetEntry(pMapImagePath);
    }
    //--If the path is "Advanced", then additional setup in another function is required. Set flags.
    else
    {
        nRegionPack->mIsUsingAdvancedMaps = true;
    }

    //--Register.
    mRegionData->AddElementAsTail(pRegionName, nRegionPack, &WarpRegionPack::DeleteThis);
}
void AdvUIWarp::SetWarpRegionAlignments(const char *pRegionName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei)
{
    ///--[Documentation]
    //--Modifies the alignments for the region icon.
    if(!pRegionName) return;

    //--Get the region in question.
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;

    //--If the image has not resolved yet, it needs to now.
    if(!rRegionPack->mAlignments.rImage) return;
    rRegionPack->mAlignments.rImage->LoadDataFromSLF();

    //--Set.
    rRegionPack->mAlignments.mOffsetX = pXOff;
    rRegionPack->mAlignments.mOffsetY = pYOff;
    rRegionPack->mAlignments.mImageX  = pLft - rRegionPack->mAlignments.rImage->GetXOffset();
    rRegionPack->mAlignments.mImageY  = pTop - rRegionPack->mAlignments.rImage->GetYOffset();
    rRegionPack->mAlignments.mImageW  = pWid;
    rRegionPack->mAlignments.mImageH  = pHei;

    //--Clamp.
    if(rRegionPack->mAlignments.mImageW < 1.0f) rRegionPack->mAlignments.mImageW = 1.0f;
    if(rRegionPack->mAlignments.mImageH < 1.0f) rRegionPack->mAlignments.mImageH = 1.0f;
}
void AdvUIWarp::SetWarpRegionAdvancedClamps(const char *pRegionName, float pClampLft, float pClampTop, float pClampRgt, float pClampBot)
{
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;
    rRegionPack->mAdvancedClampLft = pClampLft;
    rRegionPack->mAdvancedClampTop = pClampTop;
    rRegionPack->mAdvancedClampRgt = pClampRgt;
    rRegionPack->mAdvancedClampBot = pClampBot;
}
void AdvUIWarp::RegisterWarpLocation(const char *pRegionName, const char *pDisplayName, const char *pActualName, float pImgX, float pImgY)
{
    ///--[Documentation]
    //--Given a region, registers a location the player can choose to warp to.
    if(!pRegionName || !pDisplayName || !pActualName) return;

    //--Locate the region package.
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;

    //--Allocate.
    WarpLocationPack *nLocationPack = (WarpLocationPack *)starmemoryalloc(sizeof(WarpLocationPack));
    nLocationPack->Initialize();
    ResetString(nLocationPack->mRoom, pActualName);
    nLocationPack->mCoordX = pImgX;
    nLocationPack->mCoordY = pImgY;

    //--Register.
    rRegionPack->mLocations->AddElementAsTail(pDisplayName, nLocationPack, &WarpLocationPack::DeleteThis);
}
void AdvUIWarp::RegisterWarpRegionAdvancedPack(const char *pRegionName, const char *pLayerName, const char *pImagePath)
{
    ///--[Documentation]
    //--Creates a new AdvancedMapPack *, shared with the map UI. This one is just for the warp regions.
    if(!pRegionName || !pLayerName || !pImagePath) return;

    //--Get the region in question.
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;

    //--Allocate.
    AdvancedMapPack *nPack = (AdvancedMapPack *)starmemoryalloc(sizeof(AdvancedMapPack));
    nPack->Initialize();
    nPack->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);

    //--Register.
    rRegionPack->mAdvancedMapPacks->AddElementAsTail(pLayerName, nPack, &FreeThis);
}
void AdvUIWarp::SetWarpRegionPackRenderChance(const char *pRegionName, const char *pLayerName, int pLowChance, int pHighChance)
{
    //--Locate region:
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;

    //--Locate layer:
    AdvancedMapPack *rAdvancedMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->GetElementByName(pLayerName);
    if(!rAdvancedMapPack) return;

    //--Set:
    rAdvancedMapPack->mRenderChanceLo = pLowChance;
    rAdvancedMapPack->mRenderChanceHi = pHighChance;
}
void AdvUIWarp::SetWarpRegionPackReadsStencil(const char *pRegionName, const char *pLayerName, int pStencilValue)
{
    //--Locate region:
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;

    //--Locate layer:
    AdvancedMapPack *rAdvancedMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->GetElementByName(pLayerName);
    if(!rAdvancedMapPack) return;

    //--Set:
    rAdvancedMapPack->mReadsStencil = true;
    rAdvancedMapPack->mReadStencil = pStencilValue;
}
void AdvUIWarp::SetWarpRegionPackWritesStencil(const char *pRegionName, const char *pLayerName, int pStencilValue)
{
    //--Locate region:
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;

    //--Locate layer:
    AdvancedMapPack *rAdvancedMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->GetElementByName(pLayerName);
    if(!rAdvancedMapPack) return;

    //--Set:
    rAdvancedMapPack->mWritesStencil = true;
    rAdvancedMapPack->mWriteStencil = pStencilValue;
}
void AdvUIWarp::SetWarpRegionPackCanToggle(const char *pRegionName, const char *pLayerName, bool pCanToggle)
{
    //--Locate region:
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementByName(pRegionName);
    if(!rRegionPack) return;

    //--Locate layer:
    AdvancedMapPack *rAdvancedMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->GetElementByName(pLayerName);
    if(!rAdvancedMapPack) return;

    //--Set.
    rAdvancedMapPack->mCanBeToggled = pCanToggle;
}

///======================================= Core Methods ===========================================
void AdvUIWarp::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Set Strings]
    SetHelpString(i, "This menu allows you to select a location to warp to. You will teleport");
    SetHelpString(i, "there instantly. All campfires are warp locations.");
    SetHelpString(i, "");
    SetHelpString(i, "For story reasons it may not always be possible to warp.");
    SetHelpString(i, "");
    SetHelpString(i, "");
    SetHelpString(i, "You cannot warp to or from a resting bench.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference images.
    for(int i = 0; i < cxAdvHelpStrings; i ++)
    {
        mHelpMenuStrings[i]->CrossreferenceImages();
    }
}
void AdvUIWarp::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Recomputes cursor position whenever it changes. In grid mode this is just which element is highlighted,
    //  in location selection mode the cursor expands to match the length of the name.

    ///--[Grid]
    if(!mIsSelectingLocation)
    {
        //--Compute visibility.
        float tVisPercent = EasingFunction::QuadraticInOut(mVisibilityTimer, mVisibilityTimerMax);

        //--Pass to subroutine.
        RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, tVisPercent);
        return;
    }

    ///--[Location Select]
    //--Get effective position.
    int tCursorPos = mLocationCursor - mLocationSkip;

    //--Locate the region package.
    int tSelectedRegion = GetGridCodeAtCursor();
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementBySlot(tSelectedRegion);
    if(!rRegionPack) return;

    //--Get the currently selected location package.
    WarpLocationPack *rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->GetElementBySlot(mLocationCursor);
    if(!rLocationPack) return;

    //--Compute.
    float cBuffer = 2.0f;
    float cLft = 31.0f - cBuffer;
    float cTop = 16.0f + (tCursorPos * 21.0f) - cBuffer;
    float cRgt = cLft + (AdvImages.rFont_Mainline->GetTextWidth(rRegionPack->mLocations->GetNameOfElementBySlot(mLocationCursor))) + (cBuffer * 2.0f) + 2.0f;
    float cBot = cTop + (20.0f) + (cBuffer * 2.0f);

    //--If the width of the name is longer than the cap, set it to the cap.
    if(cRgt - cLft > cxAdvMaxLocationNameWid) cRgt = cLft + cxAdvMaxLocationNameWid;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        cxAdvCurTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxAdvCurTicks);
}
void AdvUIWarp::ClearGrid()
{
    mIsSelectingLocation = false;
    mRegionData->ClearList();
}
void AdvUIWarp::FocusOnWarpLocation()
{
    ///--[Documentation]
    //--Sets the offsets to focus on the location currently under the cursor. Automatically clamps scrolling.
    if(!mIsSelectingLocation) return;
    mWarpZoomFactor = 1.0f / 1.5f;

    //--Locate the region package.
    int tSelectedRegion = GetGridCodeAtCursor();
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementBySlot(tSelectedRegion);
    if(!rRegionPack) return;

    //--Get the currently selected location package.
    WarpLocationPack *rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->GetElementBySlot(mLocationCursor);
    if(!rLocationPack) return;

    //--Get the initial X/Y positions.
    float tFinalX = (rLocationPack->mCoordX * -1.0f * mWarpZoomFactor) + (VIRTUAL_CANVAS_X * 0.50f);
    float tFinalY = (rLocationPack->mCoordY * -1.0f * mWarpZoomFactor) + (VIRTUAL_CANVAS_Y * 0.50f);

    //--Sizes.
    float cLftClamp = 0.0f;
    float cTopClamp = 0.0f;
    float cRgtClamp = 0.0f;
    float cBotClamp = 0.0f;

    //--Normal usage:
    if(!rRegionPack->mIsUsingAdvancedMaps && rRegionPack->rMapImage)
    {
        cRgtClamp = (rRegionPack->rMapImage->GetTrueWidth()  * mWarpZoomFactor * -1.0f) + (VIRTUAL_CANVAS_X);
        cBotClamp = (rRegionPack->rMapImage->GetTrueHeight() * mWarpZoomFactor * -1.0f) + (VIRTUAL_CANVAS_Y);
    }
    //--Advanced map pack. Use the last package.
    else if(rRegionPack->mIsUsingAdvancedMaps)
    {
        cLftClamp = rRegionPack->mAdvancedClampLft;
        cTopClamp = rRegionPack->mAdvancedClampTop;
        cRgtClamp = rRegionPack->mAdvancedClampRgt;
        cBotClamp = rRegionPack->mAdvancedClampBot;
    }

    //--Clamp edges.
    if(cRgtClamp > 0.0f) cRgtClamp = 0.0f;
    if(cBotClamp > 0.0f) cBotClamp = 0.0f;

    //--Clamp.
    if(tFinalX > cLftClamp) tFinalX = 0.0f;
    if(tFinalY > cTopClamp) tFinalY = 0.0f;
    if(tFinalX < cRgtClamp) tFinalX = cRgtClamp;
    if(tFinalY < cBotClamp) tFinalY = cBotClamp;

    //--Set.
    mLocationPos.MoveTo(tFinalX, tFinalY, cxAdvWarpScrollTicks);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIWarp::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Handles controls and timers for the warp menu. If this is not the object handling controls,
    //  stops before then.
    //--Returns true if it handled controls, false if not.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--Reroll this value periodically. It causes brief changes in display, used for causing the glitch
    //  functionality in chapter 5's map mode.
    if(mAdvancedPackRerollTimer > 0)
    {
        mAdvancedPackRerollTimer --;
    }
    else
    {
        mAdvancedPackRoll = rand() % 1000;
        mAdvancedPackRerollTimer = 1 + (rand() % 5);
    }

    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer,     0, mVisibilityTimerMax,     true);
    StandardTimer(mLocationVisTimer,    0, mVisibilityTimerMax * 2, mIsSelectingLocation);
    StandardTimer(mHelpVisibilityTimer, 0, cHlpTicks,               mIsShowingHelp);

    //--Grid timers.
    UpdateGridTimers();

    //--Easing packages.
    mHighlightPos. Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
    mLocationPos.  Increment(EASING_CODE_QUADOUT);

    //--If this object is the focus object and is currently fading in, recompute the cursor
    //  position each tick. This makes it more seamless as the cursor will match the object scaling.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--If this timer is not capped, block the update.
    if(mLocationVisTimer > 0 && mLocationVisTimer < mVisibilityTimerMax*2) return true;

    ///--[Mode Handlers]
    //--If a mode handler returns true, block the update.
    if(CommonHelpUpdate()) return true;

    ///--[Control Subdivision]
    //--Selecting region.
    if(!mIsSelectingLocation)
    {
        UpdateRegionMode();
    }
    //--Selection location within a region.
    else
    {
        UpdateLocationMode();
    }

    //--Handled update.
    return true;
}
void AdvUIWarp::UpdateBackground()
{
    ///--[Timers]
    StandardTimer(mVisibilityTimer,  0, mVisibilityTimerMax, false);
    StandardTimer(mLocationVisTimer, 0, mVisibilityTimerMax * 2, false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIWarp::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Warp mode renders in addition to the base menu's rendering. This means region mode renders at
    //  100% opacity except for the selection grid. Locations mode renders according to a timer and
    //  overlays entirely over regions mode.

    ///--[Common Block]
    //--Unless location mode is at 100% opacity, render the standard menu backings. This occurs at
    //  full opacity regardless of timers.
    if(mLocationVisTimer < mVisibilityTimerMax*2)
    {
        //--Heading.
        AdvImages.rOverlay_Header->Draw();

        //--Header text is yellow.
        SetColor("Heading", 1.0f);
        AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select Region");
        SetColor("Clear", 1.0f);
    }

    ///--[Mode Block]
    //--Regions mode. Does not render if location is fully opaque.
    if(mLocationVisTimer < mVisibilityTimerMax*2) RenderRegionMode();

    //--Location mode. Stops internally if at 0 visibility.
    RenderLocationMode();

    //--Help.
    RenderHelp(pVisAlpha);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
