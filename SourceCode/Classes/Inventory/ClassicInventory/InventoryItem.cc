//--Base
#include "InventoryItem.h"

//--Classes
#include "InventoryPrototypes.h"
#include "PandemoniumLevel.h"
#include "WorldContainer.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
InventoryItem::InventoryItem()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_INVENTORYITEM;

    //--[InventoryItem]
    //--System
    mAutoDespawn = false;
    mLocalName = InitializeString("Unnamed Item");
    mDescription = InitializeString("This item has no description.");
    mItemTier = -1;

    //--Durability
    mDurability = -1;
    mDurabilityMax = 1;
    mBreakDurability = -2;

    //--Storage
    mUseScript = NULL;
    mItemType = ITEM_TYPE_NONE;
    rUser = NULL;
    mTargetList = new StarLinkedList(false);

    //--Trap Properties
    mTrapScript = NULL;

    //--Banes
    mBaneList = new StarLinkedList(true);

    //--Combat Statistics
    mCombatStats.Clear();

    //--Player Commands
    mCanUseOffGround = false;
    mCommandList = new StarLinkedList(true);

    //--Public Variables
    mIsConsumed = false;
    mIsSelfDestructing = false;
}
InventoryItem::~InventoryItem()
{
    free(mLocalName);
    free(mDescription);
    free(mUseScript);
    delete mTargetList;
    free(mTrapScript);
    delete mCommandList;
    delete mBaneList;
}
InventoryItem *InventoryItem::Clone()
{
    //--Returns a copy of the original class.
    InventoryItem *nClone = new InventoryItem();

    //--Basic Variables
    nClone->SetName(mLocalName);
    nClone->SetAutoDespawn(mAutoDespawn);
    nClone->SetItemTier(mItemTier);
    nClone->SetDescription(mDescription);
    nClone->SetUsePath(mUseScript);
    nClone->SetItemType(mItemType);
    nClone->SetTrapPath(mTrapScript);
    nClone->SetGroundUseFlag(mCanUseOffGround);

    //--Durability.
    nClone->SetDurability(mDurability);
    nClone->SetDurabilityMax(mDurabilityMax);
    nClone->SetDurabilityBreak(mBreakDurability);

    //--Combat Stats
    nClone->SetAttackPower(mCombatStats.mAttackPower);
    nClone->SetDefensePower(mCombatStats.mDefensePower);
    nClone->SetAccuracyPower(mCombatStats.mAccuracy);

    //--Copy the command list.
    ItemCommand *rCommand = (ItemCommand *)mCommandList->PushIterator();
    while(rCommand)
    {
        //--Clone it.
        ItemCommand *nCommandClone = rCommand->Clone();

        //--Append it.
        nClone->RegisterCommand(nCommandClone);

        //--Next.
        rCommand = (ItemCommand *)mCommandList->AutoIterate();
    }

    //--Copy the Bane list.
    float *rBaneValue = (float *)mBaneList->PushIterator();
    while(rBaneValue)
    {
        nClone->AddBane(mBaneList->GetIteratorName(), *rBaneValue);
        rBaneValue = (float *)mBaneList->AutoIterate();
    }

    return nClone;
}

///--[Public Statics]
//--Static timer that prevents too many pickup sounds from occurring at once.
int InventoryItem::xSoundTimer = -1;

///===================================== Property Queries =========================================
char *InventoryItem::GetName()
{
    return mLocalName;
}
char *InventoryItem::GetDescription()
{
    return mDescription;
}
bool InventoryItem::CanUseOffGround()
{
    return mCanUseOffGround;
}
bool InventoryItem::AutoDespawns()
{
    return mAutoDespawn;
}
const char *InventoryItem::GetTrapScript()
{
    return (const char *)mTrapScript;
}
int InventoryItem::GetItemType()
{
    return mItemType;
}
int InventoryItem::GetItemTier()
{
    return mItemTier;
}
const char *InventoryItem::GetUseString()
{
    if(mItemType == ITEM_TYPE_ARMOR || mItemType == ITEM_TYPE_WEAPON) return "Equip";
    if(mItemType == ITEM_TYPE_CONSUMABLE) return "Use";
    return "Error";
}
int InventoryItem::GetDurability()
{
    return mDurability;
}
int InventoryItem::GetDurabilityMax()
{
    return mDurabilityMax;
}
int InventoryItem::GetDurabilityBreak()
{
    return mBreakDurability;
}
int InventoryItem::GetAttackPower()
{
    return mCombatStats.mAttackPower;
}
int InventoryItem::GetDefensePower()
{
    return mCombatStats.mDefensePower;
}
int InventoryItem::GetAccuracyBonus()
{
    return mCombatStats.mAccuracy;
}
float InventoryItem::GetBaneFactor(const char *pName)
{
    //--Note: Returns 1.0f if the bane is not found.
    float *rBane = (float *)mBaneList->GetElementByName(pName);
    if(rBane)
    {
        return (*rBane);
    }
    return 1.0f;
}

///======================================== Manipulators ==========================================
void InventoryItem::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void InventoryItem::SetAutoDespawn(bool pFlag)
{
    mAutoDespawn = pFlag;
}
void InventoryItem::SetDurability(int pValue)
{
    mDurability = pValue;
}
void InventoryItem::SetDurabilityMax(int pValue)
{
    mDurabilityMax = pValue;
}
void InventoryItem::SetDurabilityBreak(int pValue)
{
    mBreakDurability = pValue;
}
void InventoryItem::SetItemTier(int pTier)
{
    mItemTier = pTier;
}
void InventoryItem::SetItemType(int pType)
{
    if(pType < ITEM_TYPE_NONE || pType >= ITEM_TYPE_TOTAL) return;
    mItemType = pType;
}
void InventoryItem::SetUsePath(const char *pPath)
{
    ResetString(mUseScript, pPath);
}
void InventoryItem::SetTrapPath(const char *pPath)
{
    ResetString(mTrapScript, pPath);
}
void InventoryItem::SetTypeByString(const char *pString)
{
    if(!pString) return;
    if(!strcasecmp(pString, "Consumable"))
    {
        SetItemType(ITEM_TYPE_CONSUMABLE);
    }
    else if(!strcasecmp(pString, "Weapon"))
    {
        SetItemType(ITEM_TYPE_WEAPON);
    }
    else if(!strcasecmp(pString, "Armor"))
    {
        SetItemType(ITEM_TYPE_ARMOR);
    }
}
void InventoryItem::SetDescription(const char *pString)
{
    if(!pString) return;
    ResetString(mDescription, pString);
}
void InventoryItem::SetGroundUseFlag(bool pFlag)
{
    mCanUseOffGround = pFlag;
}
void InventoryItem::RegisterCommand(ItemCommand *pCommand)
{
    if(!pCommand) return;
    mCommandList->AddElement("X", pCommand, &ItemCommand::DeleteThis);
}
void InventoryItem::RegisterCommand(const char *pString, const char *pLuaScript)
{
    //--Creates and adds an ItemCommand.
    if(!pString || !pLuaScript) return;
    SetMemoryData(__FILE__, __LINE__);
    ItemCommand *nCommand = (ItemCommand *)starmemoryalloc(sizeof(ItemCommand));
    nCommand->InitializeToScript(pString, pLuaScript);
}
void InventoryItem::SetAttackPower(int pPower)
{
    mCombatStats.mAttackPower = pPower;
}
void InventoryItem::SetDefensePower(int pPower)
{
    mCombatStats.mDefensePower = pPower;
}
void InventoryItem::SetAccuracyPower(int pPower)
{
    mCombatStats.mAccuracy = pPower;
}
void InventoryItem::FlagConsumed()
{
    mIsConsumed = true;
}
void InventoryItem::AddBane(const char *pName, float pMultiplier)
{
    //--Add or replace a bane.
    if(!pName) return;

    //--Base exists: Replace it.
    float *rBaneVal = (float *)mBaneList->GetElementByName(pName);
    if(rBaneVal)
    {
        *rBaneVal = pMultiplier;
    }
    else
    {
        SetMemoryData(__FILE__, __LINE__);
        float *nBaneVal = (float *)starmemoryalloc(sizeof(float));
        *nBaneVal = pMultiplier;
        mBaneList->AddElement(pName, nBaneVal, &FreeThis);
    }
}

///======================================== Core Methods ==========================================
void InventoryItem::UseItemOn(Actor *pUser, Actor *pTarget)
{
    //--Uses the item on the given target. Note that the target cannot legally be null for some items,
    //  such as a potion. In those cases, the item will not be consumed. Note also that equipment
    //  items should not call this, they should be equipped directly.
    //--The item in question will be retrievable at the top of the activity stack. The Item can push
    //  the target using InventoryItem_PushCurrentTarget(), but only in its calling script and any
    //  derived scripts.
    if(!mUseScript) return;

    //--Get the user, set them as needed.
    rUser = pUser;

    //--First, clear the target list off.
    mTargetList->ClearList();

    //--Put the target on as the 0th member, if it's not NULL. If it's NULL, the list won't register
    //  it anyway, so no harm done.
    mTargetList->AddElementAsHead("X", pTarget);

    //--Run the script in question, with this object as the active one.
    LuaManager::Fetch()->PushExecPop(this, mUseScript);

    //--Clean up.
    rUser = NULL;
    mTargetList->ClearList();
}
StarBitmap *InventoryItem::Get3DImageFor()
{
    //--Returns the image associated with the item of this type. Can be a potion beaker, weapons,
    //  or many other things.
    if(mItemType == ITEM_TYPE_WEAPON) return (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/Weapon");

    //--Default.
    return (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/Item");
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *InventoryItem::GetCommandList()
{
    return mCommandList;
}
StarLinkedList *InventoryItem::GetBaneList()
{
    return mBaneList;
}
Actor *InventoryItem::GetUser()
{
    return rUser;
}
void *InventoryItem::GetTarget(int pSlot)
{
    return mTargetList->GetElementBySlot(pSlot);
}

///===================================== Static Functions =========================================
void InventoryItem::PlayPickupSound(InventoryItem *pItem)
{
    //--Plays the sound associated with picking up the given item. If the item is not provided, a
    //  generic sound plays.
    //--No sound plays if the static timer is set. This prevents too much noise occurring if many
    //  items somehow get picked up at once.
    if((int)Global::Shared()->gTicksElapsed < xSoundTimer) return;

    //--Move the timer up and play the sound.
    xSoundTimer = Global::Shared()->gTicksElapsed + 30;

    //--No item: Play generic sound.
    if(!pItem)
    {
        AudioManager::Fetch()->PlaySound("World|TakeItem");
    }
    //--Item is an item.
    else if(pItem->GetItemType() == ITEM_TYPE_CONSUMABLE)
    {
        AudioManager::Fetch()->PlaySound("World|TakeItem");
    }
    //--Item is a weapon.
    else if(pItem->GetItemType() == ITEM_TYPE_WEAPON)
    {
        AudioManager::Fetch()->PlaySound("World|TakeWeapon");
    }
    //--Item is a piece of armor.
    else if(pItem->GetItemType() == ITEM_TYPE_ARMOR)
    {
        AudioManager::Fetch()->PlaySound("World|TakeArmor");
    }
    //--Unspecified type, play generic sound.
    else
    {
        AudioManager::Fetch()->PlaySound("World|TakeItem");
    }
}

///======================================== Lua Hooking ===========================================
void InventoryItem::HookToLuaState(lua_State *pLuaState)
{
    /* InventoryItem_CreatePrototype(sName)
       Creates and pushes an InventoryItem to the static Prototypes list. Clones of these can quickly
       be deployed after creation. Use InventoryItem_SetProperty() to modify the new item, and
       DL_PopActiveObject() once done to clean up. */
    lua_register(pLuaState, "InventoryItem_CreatePrototype", &Hook_InventoryItem_CreatePrototype);

    /* InventoryItem_GetProperty("Self Destruct") (1 boolean)
       InventoryItem_GetProperty("Name") (1 string)
       InventoryItem_GetProperty("Tier") (1 integer)
       InventoryItem_GetProperty("Durability") (1 integer)
       InventoryItem_GetProperty("DurabilityMax") (1 integer)
       InventoryItem_GetProperty("DurabilityBreak") (1 integer)
       Returns the given property of the active Inventory Item. */
    lua_register(pLuaState, "InventoryItem_GetProperty", &Hook_InventoryItem_GetProperty);

    /* InventoryItem_SetProperty("Flag Destruct")
       InventoryItem_SetProperty("Type", sType) --Consumable, Weapon, Armor
       InventoryItem_SetProperty("Tier", iTier)
       InventoryItem_SetProperty("Trap Path", sPath)
       InventoryItem_SetProperty("Description", sDescription)
       InventoryItem_SetProperty("Add Command", sCommandName, sCommandScript)
       InventoryItem_SetProperty("Execute Path", sPath)
       InventoryItem_SetProperty("Atk Bonus", iAmount)
       InventoryItem_SetProperty("Def Bonus", iAmount)
       InventoryItem_SetProperty("Acc Bonus", iAmount)
       InventoryItem_SetProperty("Bane", sBaneName, fFactor)
       InventoryItem_SetProperty("Durability", iValue)
       InventoryItem_SetProperty("DurabilityMax", iValue)
       InventoryItem_SetProperty("DurabilityBreak", iValue)
       Sets the property of the currently active Inventory Item.*/
    lua_register(pLuaState, "InventoryItem_SetProperty", &Hook_InventoryItem_SetProperty);

    /* InventoryItem_CreateFromPrototype(sName, sLocation)
       InventoryItem_CreateFromPrototype(sName, sLocation, bHasContainer, sTrapItemName)
       Spawns an InventoryItem from the prototype list and places it in the given location. The location
       is expected to be a room name, but can also be CHARINV|sName or PLAYERINV to quickly drop
       it directly in a character's inventory, or the player's inventory.
       Using the first overload spawns the item directly, the second one can add a container and optionally
       trap the container. Provide the sTrapName to trap it, or "NULL" to not trap it. */
    lua_register(pLuaState, "InventoryItem_CreateFromPrototype", &Hook_InventoryItem_CreateFromPrototype);

    /* InventoryItem_PushPrototype(sName)
       Pushes a raw prototype from the storage list. Unlike CreateFromPrototype(), this pushes
       the original 'master' copy. It should be considered read-only. Edits to the prototype
       do NOT affect derived copies unless created after the original was edited. */
    lua_register(pLuaState, "InventoryItem_PushPrototype", &Hook_InventoryItem_PushPrototype);

    /* InventoryItem_PushCurrentUser()
       Pushes the user of the item. This may not be the same as the target, if the user was using
       it to help someone else. */
    lua_register(pLuaState, "InventoryItem_PushCurrentUser", &Hook_InventoryItem_PushCurrentUser);

    /* InventoryItem_PushCurrentTarget(iSlot)
       Pushes the target of the item currently in use. This should only be called during the item's
       on-use script. The target can legally be NULL, but targets will always be in order 0, 1, 2, etc
       until a NULL is found. If iSlot is not passed, target 0 is pushed.
       Remember to pop the target when you're done with it. */
    lua_register(pLuaState, "InventoryItem_PushCurrentTarget", &Hook_InventoryItem_PushCurrentTarget);

    /* InventoryItem_Consume()
       Marks the item as consumed. The Actor will purge it as soon as possible. */
    lua_register(pLuaState, "InventoryItem_Consume", &Hook_InventoryItem_Consume);

}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_InventoryItem_CreatePrototype(lua_State *L)
{
    //InventoryItem_CreatePrototype(sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("InventoryItem_CreatePrototype");

    //--Create.
    InventoryItem *nItem = new InventoryItem();
    nItem->SetName(lua_tostring(L, 1));

    //--Register.
    InventoryPrototypes::Upload(lua_tostring(L, 1), nItem);

    //--Push as active object.
    DataLibrary::Fetch()->PushActiveEntity(nItem);

    return 0;
}
int Hook_InventoryItem_GetProperty(lua_State *L)
{
    //InventoryItem_GetProperty("Self Destruct") (1 boolean)
    //InventoryItem_GetProperty("Name") (1 string)
    //InventoryItem_GetProperty("Tier") (1 integer)
    //InventoryItem_GetProperty("Durability") (1 integer)
    //InventoryItem_GetProperty("DurabilityMax") (1 integer)
    //InventoryItem_GetProperty("DurabilityBreak") (1 integer)
    int tArgs = lua_gettop(L);

    //--Get the class.
    InventoryItem *rItem = (InventoryItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || rItem->GetType() != POINTER_TYPE_INVENTORYITEM) return LuaTypeError("InventoryItem_GetProperty", L);

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--If the item is flagged for self-destruct.
    if(!strcasecmp(rSwitchType, "Self Destruct") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushboolean(L, rItem->mIsSelfDestructing);
    }
    //--Item's name.
    else if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushstring(L, rItem->GetName());
    }
    //--Tier. Used for AI evaluations.
    else if(!strcasecmp(rSwitchType, "Tier") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushinteger(L, rItem->GetItemTier());
    }
    //--Durability. Used in Corrupter Mode.
    else if(!strcasecmp(rSwitchType, "Durability") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushinteger(L, rItem->GetDurability());
    }
    //--DurabilityMax. Used in Corrupter Mode.
    else if(!strcasecmp(rSwitchType, "DurabilityMax") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushinteger(L, rItem->GetDurabilityMax());
    }
    //--DurabilityBreak. Used in Corrupter Mode.
    else if(!strcasecmp(rSwitchType, "DurabilityBreak") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushinteger(L, rItem->GetDurabilityBreak());
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("InventoryItem_GetProperty", rSwitchType);
    }

    return tReturns;
}
int Hook_InventoryItem_SetProperty(lua_State *L)
{
    //InventoryItem_SetProperty("Flag Destruct")
    //InventoryItem_SetProperty("Auto Despawn")
    //InventoryItem_SetProperty("Type", sType) --Consumable, Weapon, Armor
    //InventoryItem_SetProperty("Tier", iTier)
    //InventoryItem_SetProperty("Trap Path", sPath)
    //InventoryItem_SetProperty("Description", sDescription)
    //InventoryItem_SetProperty("Can Use Off Ground", bFlag)
    //InventoryItem_SetProperty("Execute Path", sPath)
    //InventoryItem_SetProperty("Add Command", sCommandName, sCommandScript)
    //InventoryItem_SetProperty("Atk Bonus", iAmount)
    //InventoryItem_SetProperty("Def Bonus", iAmount)
    //InventoryItem_SetProperty("Acc Bonus", iAmount)
    //InventoryItem_SetProperty("Bane", sBaneName, fFactor)
    //InventoryItem_SetProperty("Durability", iValue)
    //InventoryItem_SetProperty("DurabilityMax", iValue)
    //InventoryItem_SetProperty("DurabilityBreak", iValue)
    int tArgs = lua_gettop(L);
    const char *rSwitchType = lua_tostring(L, 1);

    //--Get the class.
    InventoryItem *rItem = (InventoryItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || rItem->GetType() != POINTER_TYPE_INVENTORYITEM) return LuaTypeError("InventoryItem_SetProperty", L);

    //--Flags the item for self-destruction. Only used by items that haven't been picked up.
    if(!strcasecmp(rSwitchType, "Flag Destruct") && tArgs == 1)
    {
        rItem->mIsSelfDestructing = true;
    }
    //--Flags the item to auto-destruct asap. Used for items that exist only as traps.
    else if(!strcasecmp(rSwitchType, "Auto Despawn") && tArgs == 1)
    {
        rItem->SetAutoDespawn(true);
    }
    //--Sets the item's type. Type is a string, but should be "Consumable", "Weapon", or "Armor".
    else if(!strcasecmp(rSwitchType, "Type") && tArgs == 2)
    {
        rItem->SetTypeByString(lua_tostring(L, 2));
    }
    //--Tier. Used for AI evaluations.
    else if(!strcasecmp(rSwitchType, "Tier") && tArgs == 2)
    {
        rItem->SetItemTier(lua_tointeger(L, 2));
    }
    //--Trap Path. If this is the trap item in a container, this script fires.
    else if(!strcasecmp(rSwitchType, "Trap Path") && tArgs == 2)
    {
        rItem->SetTrapPath(lua_tostring(L, 2));
    }
    //--Description
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rItem->SetDescription(lua_tostring(L, 2));
    }
    //--Whether or not the "use" option appears when the item is on the ground.
    else if(!strcasecmp(rSwitchType, "Can Use Off Ground") && tArgs == 2)
    {
        rItem->SetGroundUseFlag(lua_toboolean(L, 2));
    }
    //--Which script to execute when the item is used.
    else if(!strcasecmp(rSwitchType, "Execute Path") && tArgs == 2)
    {
        rItem->SetUsePath(lua_tostring(L, 2));
    }
    //--Adds a command for this item when it's in your inventory. This is an 'always on' command.
    else if(!strcasecmp(rSwitchType, "Add Command") && tArgs == 2)
    {
        rItem->SetDescription(lua_tostring(L, 2));
    }
    //--Attack power bonus. Applies a flat increase to damage, if the attack hits.
    else if(!strcasecmp(rSwitchType, "Atk Bonus") && tArgs == 2)
    {
        rItem->SetAttackPower(lua_tointeger(L, 2));
    }
    //--Defense power bonus. Increases the required to-hit roll of the enemy.
    else if(!strcasecmp(rSwitchType, "Def Bonus") && tArgs == 2)
    {
        rItem->SetDefensePower(lua_tointeger(L, 2));
    }
    //--Accuracy bonus. Increases your to-hit roll when attacking.
    else if(!strcasecmp(rSwitchType, "Acc Bonus") && tArgs == 2)
    {
        rItem->SetAccuracyPower(lua_tointeger(L, 2));
    }
    //--Bane: Does extra damage to a target of a specific name.
    else if(!strcasecmp(rSwitchType, "Bane") && tArgs == 3)
    {
        rItem->AddBane(lua_tostring(L, 2), lua_tonumber(L, 3));
    }
    //--Durability value of the weapon.
    else if(!strcasecmp(rSwitchType, "Durability") && tArgs == 2)
    {
        rItem->SetDurability(lua_tointeger(L, 2));
    }
    //--Max durability value of the weapon.
    else if(!strcasecmp(rSwitchType, "DurabilityMax") && tArgs == 2)
    {
        rItem->SetDurabilityMax(lua_tointeger(L, 2));
    }
    //--Break point of the weapon.
    else if(!strcasecmp(rSwitchType, "DurabilityBreak") && tArgs == 2)
    {
        rItem->SetDurabilityBreak(lua_tointeger(L, 2));
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("InventoryItem_SetProperty", rSwitchType);
    }

    return 0;
}
int Hook_InventoryItem_CreateFromPrototype(lua_State *L)
{
    //InventoryItem_CreateFromPrototype(sName, sLocation)
    //InventoryItem_CreateFromPrototype(sName, sLocation, bHasContainer, sTrapItemName)
    int tArgs = lua_gettop(L);
    if(tArgs != 2 && tArgs != 4) return LuaArgError("InventoryItem_CreateFromPrototype");

    //--Get the clone from the storage list. If it's NULL, bark an error.
    InventoryItem *nItem = InventoryPrototypes::Fetch(lua_tostring(L, 1));
    if(!nItem)
    {
        fprintf(stderr, "InventoryItem_CreateFromPrototype: Error, no item %s\n", lua_tostring(L, 1));
        return 0;
    }

    //--Find the room in question and register it. The active PandemoniumLevel takes over.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(rActiveLevel)
    {
        //--Basic: Just spawn the item directly.
        if(tArgs == 2)
        {
            rActiveLevel->SpawnItemIn(nItem, lua_tostring(L, 2));
        }
        //--Advanced: Spawn the item in a randomly generated container.
        else if(tArgs == 4)
        {
            //--Flag.
            if(lua_toboolean(L, 3))
            {
                const char *rTrapItemName = lua_tostring(L, 4);
                WorldContainer *tCheckContainer = WorldContainer::SpawnContainerIn(lua_tostring(L, 2), nItem);
                if(tCheckContainer->IsSelfDestructing())
                {
                    delete tCheckContainer;
                }
                //--Trap it.
                else if(strcasecmp(rTrapItemName, "Null"))
                {
                    InventoryItem *nItem = InventoryPrototypes::Fetch(rTrapItemName);
                    if(nItem) tCheckContainer->SetTrap(nItem);
                }
            }
            //--Normal.
            else
            {
                rActiveLevel->SpawnItemIn(nItem, lua_tostring(L, 2));
            }
        }
    }
    //--Error: No level. Clean up.
    else
    {
        fprintf(stderr, "InventoryItem_CreateFromPrototype: Error, no active level.\n");
        delete nItem;
        return 0;
    }

    return 0;
}
int Hook_InventoryItem_PushPrototype(lua_State *L)
{
    //InventoryItem_PushPrototype(sName)
    DataLibrary::Fetch()->PushActiveEntity();
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("InventoryItem_PushPrototype");

    //--Get the prototype in question.
    InventoryItem *rItem = InventoryPrototypes::FetchPrototype(lua_tostring(L, 1));
    DataLibrary::Fetch()->rActiveObject = rItem;
    return 0;
}
int Hook_InventoryItem_PushCurrentUser(lua_State *L)
{
    //InventoryItem_PushCurrentUser()

    //--Make sure an InventoryItem was the top of the activity stack.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    InventoryItem *rActiveItem = (InventoryItem *)rDataLibrary->rActiveObject;
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_INVENTORYITEM))
    {
        rDataLibrary->PushActiveEntity();
        return LuaTypeError("InventoryItem_PushCurrentUser", L);
    }

    //--Get and push.
    rDataLibrary->PushActiveEntity(rActiveItem->GetUser());
    return 0;
}
int Hook_InventoryItem_PushCurrentTarget(lua_State *L)
{
    //InventoryItem_PushCurrentTarget(iSlot)
    int tArgs = lua_gettop(L);

    //--Make sure an InventoryItem was the top of the activity stack.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    InventoryItem *rActiveItem = (InventoryItem *)rDataLibrary->rActiveObject;
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_INVENTORYITEM))
    {
        rDataLibrary->PushActiveEntity();
        return LuaTypeError("InventoryItem_PushCurrentTarget", L);
    }

    //--Push a NULL target.
    rDataLibrary->PushActiveEntity();

    //--Resolve the slot. If no arg was passed, use slot 0. A negative slot fails.
    int tSlot = 0;
    if(tArgs > 0) tSlot = lua_tointeger(L, 1);
    if(tSlot < 0) tSlot = 0;

    //--Get the pointer. It can legally be NULL!
    void *rTarget = rActiveItem->GetTarget(tSlot);
    rDataLibrary->rActiveObject = rTarget;
    return 0;
}
int Hook_InventoryItem_Consume(lua_State *L)
{
    //InventoryItem_Consume()

    //--Make sure an InventoryItem was the top of the activity stack.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    InventoryItem *rActiveItem = (InventoryItem *)rDataLibrary->rActiveObject;
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_INVENTORYITEM)) return LuaTypeError("InventoryItem_PushCurrentTarget", L);

    //--Flag.
    rActiveItem->FlagConsumed();

    return 0;
}
