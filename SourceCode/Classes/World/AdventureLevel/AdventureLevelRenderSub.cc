//--Base
#include "AdventureLevel.h"

//--Classes
#include "ActorNotice.h"
#include "AdvCombat.h"
#include "FieldAbility.h"
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"
#include "OpenGLMacros.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "OptionsManager.h"

///--[Local Definitions]
//#define ALSR_DEBUG
#ifdef ALSR_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================= Entities =============================================
void AdventureLevel::RenderEntities(bool pIsBeforeTiles)
{
    ///--[Documentation]
    //--Renders the entities in the world.
    DebugPrint("Render entities.\n");
    TilemapActor::xAllowRender = true;

    //--Flag: Set whether or not entities that render before or after tiles should render. After-tile rendering
    //  is used for special effects or entities that have special transparencies.
    TilemapActor::xIsRenderingBeforeTiles = pIsBeforeTiles;

    //--Fast-access pointers.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

    //--Iterate.
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Cast and render.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            rCheckEntity->Render();
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
    glBindTexture(GL_TEXTURE_2D, 0);
}

///========================================== Tilemap =============================================
void AdventureLevel::RenderTilemap(float pLft, float pTop, float pRgt, float pBot)
{
    //--Render all the layers. It's that easy!
    DebugPrint("Render tile layers.\n");

    //--If lighting is active, don't use the override values by default. Some tiles may use them.
    if(mAreLightsActive)
    {
        uint32_t cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 0, "uUseOverrideX");
        ShaderUniform1i(cShaderHandle, 0, "uUseOverrideY");
    }

    //--Render.
    TileLayer *rLayer = (TileLayer *)mTileLayers->PushIterator();
    while(rLayer)
    {
        rLayer->RenderRange(pLft, pTop, pRgt, pBot);
        rLayer = (TileLayer *)mTileLayers->AutoIterate();
    }

    //--Tile dislocations.
    DislocatedRenderPackage *rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->PushIterator();
    while(rPackage)
    {
        //--Locate the tile layer in question.
        TileLayer *rCheckLayer = (TileLayer *)mTileLayers->GetElementByName(rPackage->mLayerName);
        if(!rCheckLayer)
        {
            rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->AutoIterate();
            continue;
        }

        //--Layer exists, render dislocated.
        rCheckLayer->RenderDislocated(rPackage->mOrigXStart, rPackage->mOrigYStart, rPackage->mOrigXStart + rPackage->mWidth, rPackage->mOrigYStart + rPackage->mHeight, rPackage->mNewXRender, rPackage->mNewYRender);

        //--Next.
        rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->AutoIterate();
    }
}

///=========================================== Doors ==============================================
void AdventureLevel::RenderDoors()
{
    //--Render doors. All doors are midground objects.
    DebugPrint("Render doors.\n");
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--[Narrow Doors]
        if(!rDoorPackage->mIsWide)
        {
            //--Closed door.
            if(rDoorPackage->rRenderClosed && !rDoorPackage->mIsOpened)
            {
                //--Move up one Y position for space EW doors.
                float tRenderY = rDoorPackage->mY - 16.0f;
                if(rDoorPackage->mIsSpaceEW)
                {
                    tRenderY = tRenderY - 16.0f;
                }

                //--Compute the position.
                float tZPosition = DEPTH_MIDGROUND + (rDoorPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE) + 0.000050f;
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(rDoorPackage->mX, tRenderY, tZPosition);

                //--Rendering.
                rDoorPackage->rRenderClosed->Bind();
                glBegin(GL_QUADS);

                    //--Top half.
                    glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                    glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                    glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                    glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                    //--Bottom half.
                    glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

                //--Finish up.
                glEnd();
                glTranslatef(rDoorPackage->mX * -1.0f, tRenderY * -1.0f, -tZPosition);
            }
            //--Open door.
            else if(rDoorPackage->rRenderOpen && rDoorPackage->mIsOpened)
            {
                //--Compute the position.
                float tZPosition = DEPTH_MIDGROUND + (rDoorPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE) + 0.000050f;
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(rDoorPackage->mX, rDoorPackage->mY - 16.0f, tZPosition);

                //--Rendering.
                rDoorPackage->rRenderOpen->Bind();
                glBegin(GL_QUADS);

                    //--Top half.
                    glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                    glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                    glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                    glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                    //--Bottom half.
                    glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

                //--Finish up.
                glEnd();
                glTranslatef(rDoorPackage->mX * -1.0f, (rDoorPackage->mY - 16.0f) * -1.0f, -tZPosition);
            }
        }
        //--[Wide Door]
        else
        {
            //--Closed door.
            if(rDoorPackage->rRenderClosed && !rDoorPackage->mIsOpened)
            {
                float tZPosition = DEPTH_MIDGROUND + ((rDoorPackage->mY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(0.0f, 0.0f, tZPosition);
                rDoorPackage->rRenderClosed->Draw(rDoorPackage->mX, rDoorPackage->mY - 16.0f);
                glTranslatef(0.0f, 0.0f, -tZPosition);
            }
            //--Open door.
            else if(rDoorPackage->rRenderOpen && rDoorPackage->mIsOpened)
            {
                //--Compute the position.
                float tZPosition = DEPTH_MIDGROUND + (rDoorPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE) + 0.000050f;
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(rDoorPackage->mX, rDoorPackage->mY - 16.0f, tZPosition);

                //--Rendering.
                rDoorPackage->rRenderOpen->Bind();
                glBegin(GL_QUADS);

                    //--Top half.
                    glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                    glTexCoord2f(1.0f, 1.0f); glVertex2f(32.0f,  0.0f);
                    glTexCoord2f(1.0f, 0.5f); glVertex2f(32.0f, 16.0f);
                    glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                    //--Bottom half.
                    glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.5f); glVertex3f(32.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.0f); glVertex3f(32.0f, 32.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

                //--Finish up.
                glEnd();
                glTranslatef(rDoorPackage->mX * -1.0f, (rDoorPackage->mY - 16.0f) * -1.0f, -tZPosition);
            }
        }

        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }
}

///========================================== Objects =============================================
void AdventureLevel::RenderClimbables()
{
    //--Typically climbables are just tiles. Deprecated.
}
void AdventureLevel::RenderChests()
{
    //--Render chests. Like doors, they are midground objects.
    DebugPrint("Render chests.\n");
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        if(rChestPackage->rRenderClosed && !rChestPackage->mIsOpened)
        {
            //--Compute position.
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rChestPackage->mZ * 2.0f);

            //--Translate, render, clean.
            glTranslatef(0.0f, 0.0f, tZPosition);
            rChestPackage->rRenderClosed->Draw(rChestPackage->mX, rChestPackage->mY - 16.0f);
            glTranslatef(0.0f, 0.0f, -tZPosition);
        }
        else if(rChestPackage->rRenderOpen && rChestPackage->mIsOpened)
        {
            //--Compute the position.
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rChestPackage->mZ * 2.0f);

            //--Translate.
            glTranslatef(rChestPackage->mX, rChestPackage->mY - 16.0f, tZPosition);

            //--Rendering.
            rChestPackage->rRenderOpen->Bind();
            glBegin(GL_QUADS);

                //--Top half.
                glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                //--Bottom half.
                glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);
                glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(16.0f, 32.0f);
                glTexCoord2f(0.0f, 0.0f); glVertex2f( 0.0f, 32.0f);

            //--Finish up.
            glEnd();

            //--Clean.
            glTranslatef(rChestPackage->mX * -1.0f, (rChestPackage->mY - 16.0f) * -1.0f, -tZPosition);
        }

        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--Render fake chests. Same property as real chests but they don't open.
    DebugPrint("Render fake chests.\n");
    rChestPackage = (DoorPackage *)mFakeChestList->PushIterator();
    while(rChestPackage)
    {
        if(rChestPackage->rRenderClosed && !rChestPackage->mIsOpened)
        {
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            glTranslatef(0.0f, 0.0f, tZPosition);
            rChestPackage->rRenderClosed->Draw(rChestPackage->mX, rChestPackage->mY - 16.0f);
            glTranslatef(0.0f, 0.0f, -tZPosition);
        }
        else if(rChestPackage->rRenderOpen && rChestPackage->mIsOpened)
        {
            //--Compute the position.
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            glTranslatef(rChestPackage->mX, rChestPackage->mY - 16.0f, tZPosition);

            //--Rendering.
            rChestPackage->rRenderOpen->Bind();
            glBegin(GL_QUADS);

                //--Top half.
                glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                //--Bottom half.
                glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -DEPTH_PER_TILE);
                glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, -DEPTH_PER_TILE);
                glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

            //--Finish up.
            glEnd();
            glTranslatef(rChestPackage->mX * -1.0f, (rChestPackage->mY - 16.0f) * -1.0f, -tZPosition);
        }

        rChestPackage = (DoorPackage *)mFakeChestList->AutoIterate();
    }
}
void AdventureLevel::RenderExits()
{
    //--Render exits. Only staircases typically render.
    DebugPrint("Render exits.\n");
    ExitPackage *rPackage = (ExitPackage *)mExitList->PushIterator();
    while(rPackage)
    {
        if(rPackage->rRenderImg)
        {
            glTranslatef(0.0f, 0.0f, -0.900000f);
            rPackage->rRenderImg->Draw(rPackage->mX, rPackage->mY);
            glTranslatef(0.0f, 0.0f,  0.900000f);
        }
        rPackage = (ExitPackage *)mExitList->AutoIterate();
    }
}
void AdventureLevel::RenderSwitches()
{
    //--Render switches.
    DebugPrint("Render switches.\n");
    DoorPackage *rSwitchPackage = (DoorPackage *)mSwitchList->PushIterator();
    while(rSwitchPackage)
    {
        //--Setup.
        StarBitmap *rRenderImg = NULL;

        //--If open:
        if(rSwitchPackage->mIsOpened)
        {
            rRenderImg = rSwitchPackage->rRenderOpen;
        }
        //--If closed:
        else
        {
            rRenderImg = rSwitchPackage->rRenderClosed;
        }

        //--Render it.
        if(rRenderImg)
        {
            //--Positions.
            float tXPos = rSwitchPackage->mX;
            float tYPos = rSwitchPackage->mY - 16.0f;
            float tZPosHi = -0.400000f;
            float tZPosLo = DEPTH_MIDGROUND + (rSwitchPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);

            //--Overrides.
            if(rSwitchPackage->mRenderHi != -2.0f)
            {
                tZPosHi = rSwitchPackage->mRenderHi + ((rSwitchPackage->mY-16.0f) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            }
            if(rSwitchPackage->mRenderLo != -2.0f)
            {
                tZPosLo = rSwitchPackage->mRenderLo + (rSwitchPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            }

            //--Rendering.
            rRenderImg->Bind();
            glTranslatef(tXPos, tYPos, 0.0f);
            glBegin(GL_QUADS);

                //--Top half.
                glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.0f,  0.0f, tZPosHi);
                glTexCoord2f(1.0f, 1.0f); glVertex3f(16.0f,  0.0f, tZPosHi);
                glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, tZPosHi);
                glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, tZPosHi);

                //--Bottom half.
                glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, tZPosLo);
                glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, tZPosLo);
                glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, tZPosLo);
                glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, tZPosLo);

            //--Finish up.
            glEnd();
            glTranslatef(-tXPos, -tYPos, 0.0f);
        }

        //--Next.
        rSwitchPackage = (DoorPackage *)mSwitchList->AutoIterate();
    }
}
void AdventureLevel::RenderSavePoints()
{
    //--Save points.
    DebugPrint("Render save points.\n");
    SavePointPackage *rSavePackage = (SavePointPackage *)mSavePointList->PushIterator();
    while(rSavePackage)
    {
        //--Setup.
        StarBitmap *rImage = NULL;
        float tZPos = DEPTH_MIDGROUND + (rSavePackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);

        //--Modify the X position if it's a bench.
        float tXPos = rSavePackage->mX;
        if(rSavePackage->mIsBench) tXPos = tXPos - (TileLayer::cxSizePerTile * 0.50f);

        //--Inactive.
        if(!rSavePackage->mIsLit || rSavePackage->mDoesNotRenderLit)
        {
            rImage = rSavePackage->rUnlitImg;
        }
        //--Active.
        else
        {
            rImage = rSavePackage->rLitImgA;
            if(Global::Shared()->gTicksElapsed % 30 < 15) rImage = rSavePackage->rLitImgB;
        }

        //--Render.
        glTranslatef(0.0f, 0.0f, tZPos);
        if(rImage) rImage->Draw(tXPos, rSavePackage->mY);
        glTranslatef(0.0f, 0.0f, -tZPos);

        //--Next.
        rSavePackage = (SavePointPackage *)mSavePointList->AutoIterate();
    }
}

///========================================= Viewcones ============================================
void AdventureLevel::RenderViewcones()
{
    //--If this flag is set, don't render any viewcones. It may be used during cutscenes.
    if(mHideAllViewcones) return;

    //--If lighting is active, set up the shader for the viewcones. Set to the default mixer.
    GLint cShaderHandle = 0;
    if(mAreLightsActive)
    {
        //--Shader.
        cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 1, "uIsViewcone");
    }

    //--Texture binding.
    Images.Data.rViewconePixel->Bind();

    //--Modified camera coordinates.

    //--Render the entity viewcones.
    DebugPrint("Render entity viewcones.\n");
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Cast and render.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Get the actor's position. They must have their sight range be within the visible screen area.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            float cActorX = rActor->GetWorldX();
            float cActorY = rActor->GetWorldY();
            float cViewDist = rActor->GetViewDistance();
            TwoDimensionReal cModdedCamera;
            cModdedCamera.Set(mCameraDimensions.mLft - cViewDist, mCameraDimensions.mTop - cViewDist, mCameraDimensions.mRgt + cViewDist, mCameraDimensions.mBot + cViewDist);

            //--Actor is within the camera, render. We allow padding equal to the view distance to avoid the case where
            //  the actor is just offscreen and their viewcone gets hidden.
            if(IsPointWithin2DReal(cActorX, cActorY, cModdedCamera))
            {
                rActor->RenderVisibilityCone(mAreLightsActive, cShaderHandle);
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--Clean up.
    if(mAreLightsActive)
    {
        cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 0, "uIsViewcone");
    }
}
void AdventureLevel::RenderPerMapAnimations()
{
    //--Renders any one-off animations that are on the map currently. Individual animations can be flagged to not render.
    //  Rendering is blocked during a major animation if that animation happens to be the major one. The others will
    //  render as normal.
    /*
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->PushIterator();
    while(rAnimation)
    {
        //--If the animation is not rendering, ignore it. Also ignore it if the information is illegal.
        if(!rAnimation->mIsRendering || !rAnimation->mrImagePtrs || rAnimation->mTotalFrames < 1)
        {
            rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
            continue;
        }

        //--If this animation happens to be the major animation, don't render it.
        if(mIsMajorAnimationMode && !strcasecmp(mMajorAnimationName, mPerMapAnimationList->GetIteratorName()))
        {
            rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
            continue;
        }

        //--Get information.
        float cTotalSizeX = (float)rAnimation->rImagePtr->GetWidth();

        //--Determine which frame we're playing.
        float tFrame = rAnimation->mTimer / rAnimation->mTicksPerFrame;
        if(tFrame >= rAnimation->mTotalFrames) tFrame = rAnimation->mTotalFrames - 1;
        if(tFrame < 1) tFrame = 0;

        //--Convert to an integer so frames don't scroll.
        tFrame = (int)tFrame;

        //--Compute the coordinates for this frame.
        float cLft = rAnimation->mRenderPosX;
        float cTop = rAnimation->mRenderPosY;
        float cRgt = cLft + rAnimation->mSizePerFrameX;
        float cBot = cTop + rAnimation->rImagePtr->GetHeight();
        float cRenderLft = (rAnimation->mSizePerFrameX *  tFrame)   / cTotalSizeX;
        float cRenderRgt = cRenderLft + (rAnimation->mSizePerFrameX / cTotalSizeX);
        float cZPos = rAnimation->mRenderDepth;

        //--Render it.
        rAnimation->rImagePtr->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cRenderLft, 0.0f); glVertex3f(cLft, cTop, cZPos);
            glTexCoord2f(cRenderRgt, 0.0f); glVertex3f(cRgt, cTop, cZPos);
            glTexCoord2f(cRenderRgt, 1.0f); glVertex3f(cRgt, cBot, cZPos);
            glTexCoord2f(cRenderLft, 1.0f); glVertex3f(cLft, cBot, cZPos);
        glEnd();

        //--Next.
        rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
    }*/
}

///========================================= Entity UI ============================================
void AdventureLevel::RenderEntityUILayer()
{
    ///--[Documentation]
    //--Entities have a "UI Layer" that renders over the entities themselves.
    DebugPrint("Render entities.\n");

    //--Fast-access pointers.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

    ///--[Enemy UI]
    //--Iterate.
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Cast and render.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            rActor->RenderUI();
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    ///--[Floating Notices]
    //--Render all notices.
    ActorNotice *rNotice = (ActorNotice *)mNotices->PushIterator();
    while(rNotice)
    {
        rNotice->Render();
        rNotice = (ActorNotice *)mNotices->AutoIterate();
    }

    ///--[Player Stamina Bar]
    //--Renders over the player to indicate stamina usage.
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    TilemapActor *rPlayerActor = LocatePlayerActor();
    if(rPlayerActor && OptionsManager::Fetch()->GetOptionB("Show Stamina Bar") == false && !rCutsceneManager->HasAnyEvents())
    {
        //--Compute stamina.
        float cStmPercent = (float)mPlayerStamina   / (float)mPlayerStaminaMax;

        //--Very low on stamina:
        if(cStmPercent < 0.50f)
        {
            rPlayerActor->RenderStaminaRing(mCameraDimensions.mLft, mCameraDimensions.mTop, mCurrentScale, cStmPercent, mStaminaRingVisPct, Images.Data.rStaminaRingUnder, Images.Data.rStaminaRingDanger);
        }
        //--Normal:
        else
        {
            rPlayerActor->RenderStaminaRing(mCameraDimensions.mLft, mCameraDimensions.mTop, mCurrentScale, cStmPercent, mStaminaRingVisPct, Images.Data.rStaminaRingUnder, Images.Data.rStaminaRingOver);
        }
    }
}

///========================================= World UI =============================================
void AdventureLevel::RenderWorldUI()
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Renders the world UI, which includes the stamina bars and field abilities display.
    if(mStaminaVisTimer < 1) return;

    //--Never renders in a cutscene.
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rCutsceneManager->HasAnyEvents()) return;

    //--Compute visibility rates for the various pieces.
    float cUIAlpha = EasingFunction::Linear(mEntireUIVisTimer, AL_UITIME_FADE_TICKS);
    float cStamPercent = (float)mStaminaVisTimer / (float)STAMINA_VIS_TICKS;
    float tLockPercent = 1.0f - (mControlLockTimer / (float)AL_CONTROL_LOCK_TICKS);

    ///--[ =========== Stamina Bar ============ ]
    //--The stamina bar appears in the top left of the screen. It has a fixed position. It can be toggled off
    //  in the options menu.
    bool tShowStaminaBar = OptionsManager::Fetch()->GetOptionB("Show Stamina Bar");
    if(tShowStaminaBar == true && cStamPercent > 0.0f)
    {
        //--Positions.
        float cLft = 4.0f;
        float cTop = 4.0f;
        float cRgt = cLft + (Images.Data.rStaminaBarMiddle->GetWidth() * 1.0f);
        float cBot = cTop + (Images.Data.rStaminaBarMiddle->GetHeight());

        //--Visibility.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cStamPercent);

        //--Computations.
        float cStmPercent = (float)mPlayerStamina   / (float)mPlayerStaminaMax;
        float cMid = cLft + (Images.Data.rStaminaBarMiddle->GetWidth() * cStmPercent);
        float cTxR = cStmPercent;

        //--Render the overlay.
        Images.Data.rStaminaBarOver->Draw();

        //--Render the left half of the middle bar.
        if(cStmPercent > 0.0f)
        {
            Images.Data.rStaminaBarMiddle->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTop);
                glTexCoord2f(cTxR, 0.0f); glVertex2f(cMid, cTop);
                glTexCoord2f(cTxR, 1.0f); glVertex2f(cMid, cBot);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(cLft, cBot);
            glEnd();
        }

        //--Render the right half of the underlay.
        if(cStmPercent < 1.0f)
        {
            Images.Data.rStaminaBarUnder->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(cTxR, 0.0f); glVertex2f(cMid, cTop);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTop);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(cRgt, cBot);
                glTexCoord2f(cTxR, 1.0f); glVertex2f(cMid, cBot);
            glEnd();
        }
    }

    ///--[ ========= Platinum Compass ========= ]
    //--An indicator that appears next to the stamina bar (or in the corner if no stamina bar is present). If it's
    //  on the screen, a catalyst is in a chest somewhere in the room.
    if(mShowPlatinumCompass)
    {
        //--Variables.
        float cLft = 4.0f;
        float cTop = 5.0f;

        //--If the stamina bar is rendering, shift the compass below it.
        if(tShowStaminaBar == true && cStamPercent > 0.0f) cTop = cTop + 24.0f;

        //--Render.
        Images.Data.rPlatinumCompass->Draw(cLft, cTop);

    }

    ///--[ ========= Field Abilities ========== ]
    //--Field abilities render on the left side of the screen in a vertical stack.
    if(cUIAlpha > 0.0f)
    {
        //--Constants.
        float cFieldLft = 4.0f;
        float cFieldHei = 50.0f;
        float cFieldSpc = 10.0f;
        float cFieldTop = (VIRTUAL_CANVAS_Y * 0.50f) - (((5.0f * cFieldHei) + (4.0f * cFieldSpc)) * 0.50f);
        float tLowestY = cFieldHei;

        //--Variables.
        AdvCombat *rAdventureCombat = AdvCombat::Fetch();
        bool tAtLeastOneFieldAbility = false;

        //--For each field ability:
        for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
        {
            //--Position.
            float cTop = cFieldTop + ((cFieldHei + cFieldSpc) * i);

            //--Get the field ability.
            FieldAbility *rFieldAbility = rAdventureCombat->GetFieldAbility(i);
            if(rFieldAbility)
            {
                tAtLeastOneFieldAbility = true;
                rFieldAbility->Render(cFieldLft, cTop, i, ADVLEV_FIELD_STENCIL_START + i, cUIAlpha);
                tLowestY = cTop + cFieldHei;
            }
        }

        //--Reset mixer.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha);

        //--Indicator for pressing buttons to edit field abilities.
        if(mCanEditFieldAbilities && !xLockoutFieldAbilityEditing)
        {
            //--No field abilities present.
            if(!tAtLeastOneFieldAbility)
            {
                float cUseX = 5.0f;
                float cUseY = 5.0f;
                if(tShowStaminaBar) cUseY = cUseY + 30.0f;
                mFieldAbilityString->DrawText(cUseX, cUseY, SUGARFONT_NOCOLOR, 1.0f, Images.Data.rUIFont);
            }
            //--At least one field ability present.
            else
            {
                float cUseX = 5.0f;
                float cUseY = tLowestY;
                mFieldAbilityStringAlt->DrawText(cUseX, cUseY, SUGARFONT_NOCOLOR, 1.0f, Images.Data.rUIFont);
            }
        }
    }

    /*
    //--Horizontal layout. Deprecated, may be needed later.
    if(false)
    {
        //--Constants.
        float cFieldLft = 4.0f;
        float cFieldTop = 4.0f;
        float cFieldSpc = 60.0f;

        //--Move the indicator over if the stamina bar is displaying.
        if(tShowStaminaBar) cFieldLft = cRgt + 5.0f;

        //--For each field ability:
        for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
        {
            //--Position.
            float cLft = cFieldLft + (cFieldSpc * i);

            //--Get the field ability.
            FieldAbility *rFieldAbility = rAdventureCombat->GetFieldAbility(i);
            if(rFieldAbility)
            {
                tAtLeastOneFieldAbility = true;
                rFieldAbility->Render(cLft, cFieldTop, i, ADVLEV_FIELD_STENCIL_START + i, cVisPercent * cUIAlpha);
            }
        }

        //--Reset mixer.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cVisPercent * cUIAlpha);

        //--Indicator for pressing buttons to edit field abilities.
        if(mCanEditFieldAbilities && !xLockoutFieldAbilityEditing)
        {
            float cUseX = 5.0f;
            float cUseY = 3.0f;
            if(tShowStaminaBar) cUseX = 100.0f;
            if(tAtLeastOneFieldAbility) cUseY = 53.0f;
            mFieldAbilityString->DrawText(cUseX, cUseY, SUGARFONT_NOCOLOR, 1.0f, Images.Data.rUIFont);
        }
    }*/

    ///--[ ========= Health Indicator ========= ]
    //--Renders some indicators to show health. Used in some mods.
    if(mShowHealthMeter && rHealthIndicator)
    {
        //--Spacing. Health indicator always exists, empties can be left NULL.
        float tXPos = 0.0f;
        float cYPos = 30.0f;
        float cXSpacing = rHealthIndicator->GetWidth() + 3.0f;

        //--Reset mixer.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha);

        //--Rendering health indicators.
        for(int i = 0; i < mHealthRemaining; i ++)
        {
            rHealthIndicator->Draw(tXPos, cYPos);
            tXPos = tXPos + cXSpacing;
        }

        //--If the empties exist, render those as well.
        if(rEmptyIndicator)
        {
            for(int i = mHealthRemaining; i < mHealthMax; i ++)
            {
                rEmptyIndicator->Draw(tXPos, cYPos);
                tXPos = tXPos + cXSpacing;
            }
        }
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tLockPercent);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    ///--[ ======== Autosave Indicator ======== ]
    if(rAutosaveImage && mAutosavePostTimer > 0)
    {
        //--Calculate alpha.
        float cAlpha = EasingFunction::Linear(mAutosavePostTimer, AL_AUTOSAVE_INDICATOR_TICKS);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Render.
        Images.Data.rUIFont->DrawText(VIRTUAL_CANVAS_X, 0.0f, SUGARFONT_RIGHTALIGN_X, "Autosaved...");
        rAutosaveImage->Draw(1241.0f, -1.0f);

        //--Clean.
        StarlightColor::ClearMixer();
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///===================================== Region Indicator =========================================
void AdventureLevel::RenderRegionOverlay()
{
    ///--[Region Notifier]
    //--Appears in the middle or top-right corner to indicate the area the player is currently in.
    //  Can animate if set correctly.
    float cXOff = 33.0f;
    float cYOff = -28.0f;

    //--Global UI visiblity.
    float cUIAlpha = EasingFunction::Linear(mEntireUIVisTimer, AL_UITIME_FADE_TICKS);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha);

    //--Render nothing if fading.
    if(mIsTransitionIn || mIsTransitionOut || mTransitionHoldTimer > -1)
    {
        //--If already in the idle state, that can be rendered.
        if(mRegionNotifierAnimState == AL_REGION_STATE_IDLE && rRegionNotifier)
        {
            //--Positions.
            float cLftX = VIRTUAL_CANVAS_X - (rRegionNotifier->GetTrueWidth() * mRegionNotifierFinalScale) + cXOff;
            float cTopY = cYOff;

            //--Render.
            glTranslatef(cLftX, cTopY, 0.0f);
            glScalef(mRegionNotifierFinalScale, mRegionNotifierFinalScale, 1.0f);
            rRegionNotifier->Draw();
            glScalef(1.0f / mRegionNotifierFinalScale, 1.0f / mRegionNotifierFinalScale, 1.0f);
            glTranslatef(-cLftX, -cTopY, 0.0f);
        }
    }
    //--Fade in animation.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_FADEIN)
    {
        //--Do nothing if no frames were allocated.
        if(mRegionNotifierAnimTotal < 1)
        {
        }
        //--Render.
        else
        {
            //--Resolve percentage completion.
            float cFadePercent = mRegionNotifierAnimTimer / (float)AL_REGION_FADE_TICKS;

            //--Clamp frame.
            int tUseFrame = 0;

            //--Render the frame.
            if(rRegionNotifierAnim[tUseFrame])
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cFadePercent * cUIAlpha);
                rRegionNotifierAnim[tUseFrame]->Draw();
                StarlightColor::ClearMixer();
            }
        }
    }
    //--Initial animation.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_ANIMATE)
    {
        //--Do nothing if no frames were allocated.
        if(mRegionNotifierAnimTotal < 1)
        {
        }
        //--Render.
        else
        {
            //--Resolve percentage completion.
            float cPercent = mRegionNotifierAnimTimer / (float)mRegionNotifierAnimTimerMax;

            //--Clamp frame.
            int tUseFrame = (int)(mRegionNotifierAnimTotal * cPercent);
            if(tUseFrame < 0) tUseFrame = 0;
            if(tUseFrame >= mRegionNotifierAnimTotal) tUseFrame = mRegionNotifierAnimTotal-1;

            //--Render the frame.
            if(rRegionNotifierAnim[tUseFrame])
            {
                rRegionNotifierAnim[tUseFrame]->Draw();
            }
        }
    }
    //--Moving to the top right.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_SHRINK)
    {
        //--Take the last frame.
        int tUseFrame = mRegionNotifierAnimTotal-1;
        if(tUseFrame >= 0 && tUseFrame < mRegionNotifierAnimTotal && rRegionNotifierAnim[tUseFrame])
        {
            //--Left position.
            float cLftX = VIRTUAL_CANVAS_X - (rRegionNotifierAnim[tUseFrame]->GetTrueWidth() * mRegionNotifierFinalScale) + cXOff;

            //--Compute the percentage of shrink.
            float cScalePercent  = EasingFunction::QuadraticInOut(mRegionNotifierAnimTimer, AL_REGION_SHRINK_TICKS);
            float cLocalePercent = EasingFunction::QuadraticIn(mRegionNotifierAnimTimer, AL_REGION_SHRINK_TICKS);
            float cScale = 1.0f + ((mRegionNotifierFinalScale - 1.0f) * cScalePercent);
            float cXPos  = 0.0f + ((      cLftX - 0.0f) * cLocalePercent);
            float cYPos  = cYOff * cScalePercent;

            //--Render.
            if(cScale > 0.0f)
            {
                glTranslatef(cXPos, cYPos, 0.0f);
                glScalef(cScale, cScale, 1.0f);
                rRegionNotifierAnim[tUseFrame]->Draw();
                glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
                glTranslatef(-cXPos, -cYPos, 0.0f);
            }
        }
    }
    //--Holding position in the top right.
    else if(mRegionNotifierAnimState == AL_REGION_STATE_IDLE)
    {
        if(rRegionNotifier)
        {
            //--Left position.
            float cLftX = VIRTUAL_CANVAS_X - (rRegionNotifier->GetTrueWidth() * mRegionNotifierFinalScale) + cXOff;
            float cTopY = cYOff;

            //--Scrolls offscreen when a cutscene is playing.
            if(mControlLockTimer > 0)
            {
                //--Calculations.
                float tPercent = EasingFunction::QuadraticInOut(mControlLockTimer,(float)AL_CONTROL_LOCK_TICKS);
                cTopY = cTopY - (150.0f * tPercent);
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, (1.0f - tPercent) * cUIAlpha);
            }

            //--Render.
            glTranslatef(cLftX, cTopY, 0.0f);
            glScalef(mRegionNotifierFinalScale, mRegionNotifierFinalScale, 1.0f);
            rRegionNotifier->Draw();
            glScalef(1.0f / mRegionNotifierFinalScale, 1.0f / mRegionNotifierFinalScale, 1.0f);
            glTranslatef(-cLftX, -cTopY, 0.0f);
            StarlightColor::ClearMixer();
        }
    }

    //--Clean.
    StarlightColor::ClearMixer();
}
