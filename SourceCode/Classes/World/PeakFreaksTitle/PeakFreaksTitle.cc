//--Base
#include "PeakFreaksTitle.h"

//--Classes
#include "PeakFreaksLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///========================================== System ==============================================
PeakFreaksTitle::PeakFreaksTitle()
{
    ///--[Variable Initialization]
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PEAKFREAKSTITLE;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[TiledLevel]
    //--System
    //--Sizing
    //--Tileset Packs
    //--Raw Lists
    //--Collision Storage

    ///--[PeakFreaksTitle]
    //--System
    mOpenTimer = 0;
    mLogoTimer = 0;
    mFlagTimer = 0;
    mSpinnerTimer = 0;
    mMusicVolumeNormal = AudioManager::xMusicVolume;

    //--Music
    mMusicTimer = PFTITLE_MUSIC_TICKS;

    //--Seed Mandate
    mSeedProgress = 0;
    memset(mPressedNumbers, 0, sizeof(int) * 4);

    //--Selection
    mCurSelection = PFTITLE_OPTION_START;
    mCursorTimer = 0;
    memset(mSelectionTimers, 0, sizeof(int) * PFTITLE_OPTION_TOTAL);
    mSelectionTimers[PFTITLE_OPTION_START] = PFTITLE_SELECTION_MAX_TICKS;

    //--Difficulty Select
    mIsDifficultyMode = false;
    mDifficultySelection = 0;
    memset(mDifficultySelectionTimers, 0, sizeof(int) * PTTITLE_DIFFICULTY_MAX);
    mDifficultyTimer = 0;
    mPlayerSlideTimer = 0;
    memset(mVersusSlideTimers, 0, sizeof(int) * PTTITLE_DIFFICULTY_MAX);

    //--High Scores Mode
    mIsScoresMode = false;
    mScoresTimer = 0;
    mScoresScooterTimer = 0;
    mScoresMimiTimer = 0;
    mScoresAllenTimer = 0;
    memset(mScoreTableEasy,   0, sizeof(int) * PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY);
    memset(mScoreTableMedium, 0, sizeof(int) * PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY);
    memset(mScoreTableHard,   0, sizeof(int) * PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY);
    mScoreTimeBuffer[0] = '\0';

    //--Fade To Level
    mFadeCase = PFTITLE_TO_TUTORIAL;
    mFadeToLevelTimer = 0;

    //--Clouds
    mShouldSpawnClouds = true;
    mCloudSpawnTimer = 0;
    mCloudList = new SugarLinkedList(true);

    //--Finale
    mIsFinaleMode = false;
    mFinaleTimer = 0;
    mFinaleFadeTimer = PFTITLE_FINALE_FADE_TICKS;
    mFireworksSFXTimer = 0;
    mCreditsTimer = 0;
    mAlienTimer = 0;
    mShowPersonalBest = false;
    mFinaleCursor = 0;
    mLftPorXOff = 0.0f;
    mLftPorYOff = 0.0f;
    mLftPorFlags = 0;
    mRgtPorXOff = 0.0f;
    mRgtPorYOff = 0.0f;
    mRgtPorFlags = 0;
    rLftPortrait = NULL;
    rRgtPortrait = NULL;
    mFireworksTimer = 1;
    mFireworks = new SugarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));
}
PeakFreaksTitle::~PeakFreaksTitle()
{
    delete mCloudList;
    delete mFireworks;
}
void PeakFreaksTitle::Construct()
{
    ///--[Images]
    //--Setup.
    char tBuffer[256];
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Main Menu]
    //--Resolve.
    Images.Data.rBackgroundSky    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Background_All");
    Images.Data.rBackgroundMtn    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Background_Mountain");
    Images.Data.rClouds[0]        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Cloud0");
    Images.Data.rClouds[1]        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Cloud1");
    Images.Data.rClouds[2]        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Cloud2");
    Images.Data.rCursor           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Cursor");
    Images.Data.rNotifierKeyboard = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Notifier_Keyboard");
    Images.Data.rNotifierGamepad  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Notifier_Gamepad");
    Images.Data.rTextScores       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Text_Scores");
    Images.Data.rTextStart        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Text_Start");
    Images.Data.rTextTutorial     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Text_Tutorial");
    Images.Data.rSelectedScores   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Sel_Scores");
    Images.Data.rSelectedStart    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Sel_Start");
    Images.Data.rSelectedTutorial = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/Sel_Tutorial");

    //--Difficulty Select
    Images.Data.rDiffQuotes[0]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Quote_Scooter");
    Images.Data.rDiffQuotes[1]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Quote_Mimi");
    Images.Data.rDiffQuotes[2]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Quote_Allen");
    Images.Data.rDiffTitles[0]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Title_Scooter");
    Images.Data.rDiffTitles[1]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Title_Mimi");
    Images.Data.rDiffTitles[2]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Title_Allen");
    Images.Data.rDiffPortraits[0]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scooter/Neutral");
    Images.Data.rDiffPortraits[1]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Mimi/Neutral");
    Images.Data.rDiffPortraits[2]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Allen/Neutral");
    Images.Data.rDiffSelected[0]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Selected_Simple");
    Images.Data.rDiffSelected[1]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Selected_Challenging");
    Images.Data.rDiffSelected[2]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Selected_Impossible");
    Images.Data.rDiffText[0]        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Text_Simple");
    Images.Data.rDiffText[1]        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Text_Challenging");
    Images.Data.rDiffText[2]        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Text_Impossible");
    Images.Data.rDiffVersus         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Difficulty/Title_VS");
    Images.Data.rDiffPlayerPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Kidd/Neutral");

    //--High Scores
    Images.Data.rScoresFont          = rDataLibrary->GetFont("PeakFreaks Scores UI");
    Images.Data.rScoresBack          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scores/BackButton");
    Images.Data.rScoresBoard         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scores/Board");
    Images.Data.rScoresEmpty         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scores/Empty");
    Images.Data.rScoresLabel         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scores/Label");
    Images.Data.rScoresTrophyAllen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scores/TrophyAllen");
    Images.Data.rScoresTrophyMimi    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scores/TrophyMimi");
    Images.Data.rScoresTrophyScooter = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scores/TrophyScooter");

    //--Logo frames:
    for(int i = 0; i < PFTITLE_LOGO_FRAMES; i ++)
    {
        sprintf(tBuffer, "Root/Images/UI/Title/Logo%02i", i);
        Images.Data.rLogoFrames[i] = (SugarBitmap *)rDataLibrary->GetEntry(tBuffer);
    }

    //--Flag frames:
    for(int i = 0; i < PFTITLE_FLAG_FRAMES; i ++)
    {
        sprintf(tBuffer, "Root/Images/UI/Title/Flag%02i", i);
        Images.Data.rFlagFrames[i] = (SugarBitmap *)rDataLibrary->GetEntry(tBuffer);
    }

    //--Spinner frames:
    for(int i = 0; i < PFTITLE_SPINNER_FRAMES; i ++)
    {
        sprintf(tBuffer, "Root/Images/UI/Title/Spinner%01i", i);
        Images.Data.rSpinnerFrames[i] = (SugarBitmap *)rDataLibrary->GetEntry(tBuffer);
    }

    ///--[Finale]
    //--Fonts
    Images.Finale.rTimeFont    = rDataLibrary->GetFont("PeakFreaks Finale Time");
    Images.Finale.rCreditsFont = rDataLibrary->GetFont("PeakFreaks Finale Credits");

    //--Images
    Images.Finale.rAlien           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Alien");
    Images.Finale.rBackgroundSky   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Background");
    Images.Finale.rClouds[0]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Cloud0");
    Images.Finale.rClouds[1]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Cloud1");
    Images.Finale.rClouds[2]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Cloud2");
    Images.Finale.rCreditsBack     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/CreditsBack");
    Images.Finale.rCreditsBar      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/CreditsBar");
    Images.Finale.rLabelTooBad     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Label_TooBad");
    Images.Finale.rLabelWinner     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Label_Winner");
    Images.Finale.rPersonalBestIco = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/PB");
    Images.Finale.rSelection[0]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Select_Rematch");
    Images.Finale.rSelection[1]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Select_Quit");
    Images.Finale.rText[0]         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Text_Rematch");
    Images.Finale.rText[1]         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/Text_Quit");
    Images.Finale.rTimerMenu       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Finale/TimerMenu");

    //--Fireworks
    char tBuf[128];
    const char *cFireworkCols[] = {"Blue", "Green", "Orange", "Pink"};
    for(int i = 0; i < PFTITLE_FIREWORKS_COLS; i ++)
    {
        for(int p = 0; p < PFTITLE_FIREWORKS_FRAMES; p ++)
        {
            sprintf(tBuf, "Root/Images/UI/Fireworks/%s|%i", cFireworkCols[i], p);
            Images.Finale.rFireworks[i][p] = (SugarBitmap *)rDataLibrary->GetEntry(tBuf);
        }
    }

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *)) && VerifyStructure(&Images.Finale, sizeof(Images.Finale), sizeof(void *));

    ///--[Finale Portraits]
    //--If booting in Finale mode, resolve the left and right portraits.
    if(mIsFinaleMode)
    {
        //--Player Won:
        if(PeakFreaksLevel::xPlayerVictory)
        {
            //--Player is constant.
            mLftPorXOff =  93.0f;
            mLftPorYOff = -15.0f;
            mLftPorFlags = 0;
            rLftPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Kidd/Happy");

            //--Resolve opponent based on the difficulty.
            if(PeakFreaksLevel::xDifficulty == PTTITLE_DIFFICULTY_SIMPLE)
            {
                mRgtPorXOff  = 1029.0f;
                mRgtPorYOff  =   54.0f;
                mRgtPorFlags = 0;
                rRgtPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scooter/Sad");
            }
            else if(PeakFreaksLevel::xDifficulty == PTTITLE_DIFFICULTY_CHALLENGING)
            {
                mRgtPorXOff  = 1033.0f;
                mRgtPorYOff  =   31.0f;
                mRgtPorFlags = 0;
                rRgtPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Mimi/Sad");
            }
            else
            {
                mRgtPorXOff  = 1000.0f;
                mRgtPorYOff  =   27.0f;
                mRgtPorFlags = 0;
                rRgtPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Allen/Sad");
            }
        }
        //--Player Lost:
        else
        {
            //--Player is constant on the right.
            mRgtPorXOff =  941.0f;
            mRgtPorYOff =   51.0f;
            mRgtPorFlags = FLIP_HORIZONTAL;
            rRgtPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Kidd/Sad");

            //--Resolve opponent based on the difficulty.
            if(PeakFreaksLevel::xDifficulty == PTTITLE_DIFFICULTY_SIMPLE)
            {
                mLftPorXOff = -115.0f;
                mLftPorYOff =  -42.0f;
                mLftPorFlags = FLIP_HORIZONTAL;
                rLftPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Scooter/Happy");
            }
            else if(PeakFreaksLevel::xDifficulty == PTTITLE_DIFFICULTY_CHALLENGING)
            {
                mLftPorXOff =   4.0f;
                mLftPorYOff = -66.0f;
                mLftPorFlags = FLIP_HORIZONTAL;
                rLftPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Mimi/Happy");
            }
            else
            {
                mLftPorXOff =  -40.0f;
                mLftPorYOff = -174.0f;
                mLftPorFlags = FLIP_HORIZONTAL;
                rLftPortrait = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Allen/Happy");
            }
        }
    }

    ///--[Load High Scores]
    LoadHighScores();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void PeakFreaksTitle::SpawnCloud()
{
    ///--[Documentation]
    //--Spawns a cloud in a random spot on the map. This is used when the object initializes to give
    //  the impression that clouds were already flying in the background.
    PFTitle_Cloud *nCloud = (PFTitle_Cloud *)starmemoryalloc(sizeof(PFTitle_Cloud));
    nCloud->Initialize();

    //--Roll a random Y position.
    nCloud->mYPos = (rand() % (PFTITLE_CLOUD_Y_HI - PFTITLE_CLOUD_Y_LO)) + PFTITLE_CLOUD_Y_LO;

    //--Select a cloud image.
    int tCloudSlot = rand() % PFTITLE_CLOUD_FRAMES;
    nCloud->rImage = Images.Data.rClouds[tCloudSlot];

    //--Randomize the X position.
    nCloud->mXPos = rand() % (int)VIRTUAL_CANVAS_X;

    //--Roll a speed.
    nCloud->mXSpeed = ((rand() % (PFTITLE_CLOUD_SPEED_HI - PFTITLE_CLOUD_SPEED_LO)) + PFTITLE_CLOUD_SPEED_LO) / 10.0f;

    //--Register.
    mCloudList->AddElement("X", nCloud, &FreeThis);
}
void PeakFreaksTitle::SpawnCloudEdge()
{
    ///--[Documentation]
    //--Spawns a cloud off the left edge of the map. This allows it to scroll naturally onto the field.
    PFTitle_Cloud *nCloud = (PFTitle_Cloud *)starmemoryalloc(sizeof(PFTitle_Cloud));
    nCloud->Initialize();

    //--Roll a random Y position.
    nCloud->mYPos = (rand() % (PFTITLE_CLOUD_Y_HI - PFTITLE_CLOUD_Y_LO)) + PFTITLE_CLOUD_Y_LO;

    //--Select a cloud image.
    int tCloudSlot = rand() % PFTITLE_CLOUD_FRAMES;
    nCloud->rImage = Images.Data.rClouds[tCloudSlot];

    //--Resolve the X position. The cloud needs to be just off frame to the left.
    nCloud->mXPos = nCloud->rImage->GetWidth() * -1;

    //--Roll a speed.
    nCloud->mXSpeed = ((rand() % (PFTITLE_CLOUD_SPEED_HI - PFTITLE_CLOUD_SPEED_LO)) + PFTITLE_CLOUD_SPEED_LO) / 10.0f;

    //--Register.
    mCloudList->AddElement("X", nCloud, &FreeThis);
}
void PeakFreaksTitle::SpawnCloudFinale()
{
    //--Identical to the above, but uses the finale cloud images.
    PFTitle_Cloud *nCloud = (PFTitle_Cloud *)starmemoryalloc(sizeof(PFTitle_Cloud));
    nCloud->Initialize();

    //--Roll a random Y position.
    nCloud->mYPos = (rand() % (PFTITLE_CLOUD_Y_HI - PFTITLE_CLOUD_Y_LO)) + PFTITLE_CLOUD_Y_LO;

    //--Select a cloud image.
    int tCloudSlot = rand() % PFTITLE_CLOUD_FRAMES;
    nCloud->rImage = Images.Finale.rClouds[tCloudSlot];

    //--Randomize the X position.
    nCloud->mXPos = rand() % (int)VIRTUAL_CANVAS_X;

    //--Roll a speed.
    nCloud->mXSpeed = ((rand() % (PFTITLE_CLOUD_SPEED_HI - PFTITLE_CLOUD_SPEED_LO)) + PFTITLE_CLOUD_SPEED_LO) / 10.0f;

    //--Register.
    mCloudList->AddElement("X", nCloud, &FreeThis);
}
void PeakFreaksTitle::SpawnCloudFinaleEdge()
{
    //--Identical to the above, but uses the finale cloud images.
    PFTitle_Cloud *nCloud = (PFTitle_Cloud *)starmemoryalloc(sizeof(PFTitle_Cloud));
    nCloud->Initialize();

    //--Roll a random Y position.
    nCloud->mYPos = (rand() % (PFTITLE_CLOUD_Y_HI - PFTITLE_CLOUD_Y_LO)) + PFTITLE_CLOUD_Y_LO;

    //--Select a cloud image.
    int tCloudSlot = rand() % PFTITLE_CLOUD_FRAMES;
    nCloud->rImage = Images.Finale.rClouds[tCloudSlot];

    //--Resolve the X position. The cloud needs to be just off frame to the left.
    nCloud->mXPos = nCloud->rImage->GetWidth() * -1;

    //--Roll a speed.
    nCloud->mXSpeed = ((rand() % (PFTITLE_CLOUD_SPEED_HI - PFTITLE_CLOUD_SPEED_LO)) + PFTITLE_CLOUD_SPEED_LO) / 10.0f;

    //--Register.
    mCloudList->AddElement("X", nCloud, &FreeThis);
}
void PeakFreaksTitle::ResolveScoreIntoBuffer(int pTicks)
{
    //--Given a number of ticks, resolves this into a string of format MM:SS:LL and puts this into
    //  mScoreTimeBuffer for rendering. Negative values are treated as 0.
    if(pTicks < 0) pTicks = 0;

    //--Resolve minutes. Cap is 99.
    int tMinutes = ((pTicks / 60) / 60);
    if(tMinutes > 99) tMinutes = 99;

    //--Resolve seconds. Cap is 59.
    int tSeconds = ((pTicks / 60) % 60);

    //--Resolve milliseconds. Only the first two digits are rendered.
    float tMilliseconds = (pTicks % 60) / 60.0f;
    int tMillisecondsInt = (int)(tMilliseconds * 100.0f);

    //--Print to buffer.
    sprintf(mScoreTimeBuffer, "%02i:%02i:%02i", tMinutes, tSeconds, tMillisecondsInt);
}
void PeakFreaksTitle::LoadHighScores()
{
    //--Loads high score data into the local variables.
    PFTitle_Highscore *tHighscoreData = ReadHighscores();
    if(!tHighscoreData) return;

    //--Cross copy.
    for(int i = 0; i < 3; i ++)
    {
        mScoreTableEasy[i] = tHighscoreData->mEasyTimes[i];
        mScoreTableMedium[i] = tHighscoreData->mMediumTimes[i];
        mScoreTableHard[i] = tHighscoreData->mHardTimes[i];
    }
    mScoresDefeatedScooter = tHighscoreData->mWonEasy;
    mScoresDefeatedMimi = tHighscoreData->mWonMedium;
    mScoresDefeatedAllen = tHighscoreData->mWonHard;

    //--Clean.
    free(tHighscoreData);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksTitle::Update()
{
    ///--[ ============== Finale ============== ]
    //--In finale mode, call the finale update and stop.
    if(mIsFinaleMode) { UpdateFinale(); return ;}

    ///--[ ========= Debug Key Update ========= ]
    //--Allows the player to type "SEEDXXXX" where XXXX is a four-digit seed. This mandates the seed
    //  used to generate the next played level.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(true)
    {
        //--Get the pressed keycode.
        int tKeyboard, tMouse, tJoy;
        rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoy);
        const char *rPressedKeyName = rControlManager->GetNameOfKeyIndex(tKeyboard);

        //--Make sure a key was pressed.
        if(rPressedKeyName)
        {
            //--Downshift it if it's a letter. Numbers and other characters do not need modification.
            char tCharacter = rPressedKeyName[0];
            if(tCharacter >= 'A' && tCharacter <= 'Z')
            {
                tCharacter = tCharacter + 'a' - 'A';
            }

            //--Now check if it matches a key we need.
            if((tCharacter == 's' && mSeedProgress == 0) ||
               (tCharacter == 'e' && mSeedProgress == 1) ||
               (tCharacter == 'e' && mSeedProgress == 2) ||
               (tCharacter == 'd' && mSeedProgress == 3))
            {
                //--Increment.
                mSeedProgress ++;
            }
            //--If we're in seed progress 4-7, store only numbers.
            else if(mSeedProgress >= 4 && mSeedProgress <= 7)
            {
                //--Make sure it's a number:
                if(tCharacter >= '0' && tCharacter <= '9')
                {
                    mPressedNumbers[mSeedProgress-4] = tCharacter - '0';
                    mSeedProgress ++;
                }

                //--Ending case:
                if(mSeedProgress == 8)
                {
                    mSeedProgress = 0;
                    PeakFreaksLevel::xMandatedSeed = (mPressedNumbers[0] * 1000) + (mPressedNumbers[1] * 100) + (mPressedNumbers[2] * 10) + (mPressedNumbers[3]);
                    AudioManager::Fetch()->PlaySound("Blink");
                    fprintf(stderr, "Mandated seed: %i\n", PeakFreaksLevel::xMandatedSeed);
                }
            }
        }
    }

    ///--[ =========== Timers Update ========== ]
    //--Basic fade timer.
    if(mOpenTimer < PFTITLE_FADE_TO_LEVEL_TICKS)
    {
        mOpenTimer ++;
    }

    //--Clouds.
    if(mShouldSpawnClouds)
    {
        mShouldSpawnClouds = false;
        for(int i = 0; i < 6; i ++)
        {
            SpawnCloud();
        }
    }

    ///--[Music]
    //--Run to zero then play music.
    if(mMusicTimer > 0)
    {
        //--Decrement.
        mMusicTimer --;

        //--Finish.
        if(mMusicTimer < 1)
        {
            AudioManager::Fetch()->PlayMusic("MainMenu");
            AudioManager::Fetch()->StopAllSound();
        }
    }

    ///--[Fixed Timers]
    mLogoTimer ++;
    mFlagTimer ++;
    mCursorTimer ++;
    mSpinnerTimer ++;
    if(mCursorTimer > PFTITLE_CURSOR_CYCLE_TICKS) mCursorTimer = 0;

    //--High score timers.
    mScoresScooterTimer = (mScoresScooterTimer + 1) % PFTITLE_HIGHSCORES_TIME_SCOOTER;
    mScoresMimiTimer    = (mScoresMimiTimer    + 1) % PFTITLE_HIGHSCORES_TIME_MIMI;
    mScoresAllenTimer   = (mScoresAllenTimer   + 1) % PFTITLE_HIGHSCORES_TIME_ALLEN;

    ///--[Selection Handling]
    //--Whichever menu option is selected increases to its timer cap, all others decrease.
    for(int i = 0; i < PFTITLE_OPTION_TOTAL; i ++)
    {
        if(mCurSelection == i)
        {
            mSelectionTimers[i] ++;
            if(mSelectionTimers[i] > PFTITLE_SELECTION_MAX_TICKS) mSelectionTimers[i] = PFTITLE_SELECTION_MAX_TICKS;
        }
        else
        {
            mSelectionTimers[i] --;
            if(mSelectionTimers[i] < 0) mSelectionTimers[i] = 0;
        }
    }

    //--Difficulty selection timers.
    for(int i = 0; i < PTTITLE_DIFFICULTY_MAX; i ++)
    {
        if(mDifficultySelection == i)
        {
            mDifficultySelectionTimers[i] ++;
            if(mDifficultySelectionTimers[i] > PFTITLE_SELECTION_MAX_TICKS) mDifficultySelectionTimers[i] = PFTITLE_SELECTION_MAX_TICKS;
        }
        else
        {
            mDifficultySelectionTimers[i] --;
            if(mDifficultySelectionTimers[i] < 0) mDifficultySelectionTimers[i] = 0;
        }
    }

    ///--[Difficulty Mode]
    //--If the player is not in difficulty-selection mode, decrement the timers to zero.
    if(!mIsDifficultyMode)
    {
        //--Decrement.
        if(mDifficultyTimer > 0) mDifficultyTimer --;
        if(mPlayerSlideTimer > 0) mPlayerSlideTimer --;

        //--Decrement all rival timers.
        for(int i = 0; i < PTTITLE_DIFFICULTY_MAX; i ++)
        {
            if(mVersusSlideTimers[i] > 0) mVersusSlideTimers[i] --;
        }
    }
    //--If the player is in difficulty-selection mode, increment the timers to their cap.
    else
    {
        //--Difficulty timer runs to cap.
        if(mDifficultyTimer < PFTITLE_DIFFSWITCH_TICKS) mDifficultyTimer ++;

        //--Player timer runs up with a delay.
        if(mDifficultyTimer > (int)(PFTITLE_DIFFSWITCH_TICKS * 0.25f))
        {
            if(mPlayerSlideTimer < PFTITLE_PLAYERSLIDE_TICKS) mPlayerSlideTimer ++;
        }
        else
        {
            if(mPlayerSlideTimer > 0) mPlayerSlideTimer --;
        }

        //--Rival timers run up with a delay, if selected. Otherwise, decrement.
        for(int i = 0; i < PTTITLE_DIFFICULTY_MAX; i ++)
        {
            //--Selected, and over the timer cap.
            if(mDifficultySelection == i && mDifficultyTimer > (int)(PFTITLE_DIFFSWITCH_TICKS * 0.35f))
            {
                if(mVersusSlideTimers[i] < PFTITLE_RIVALSLIDE_TICKS)
                {
                    mVersusSlideTimers[i] ++;
                    if(mVersusSlideTimers[i] % 3 == 0) AudioManager::Fetch()->PlaySound("PF Voice Tick");
                }
            }
            //--Decrement.
            else
            {
                if(mVersusSlideTimers[i] > 0) mVersusSlideTimers[i] --;
            }
        }
    }

    //--If the player is in high scores mode:
    if(mIsScoresMode)
    {
        if(mScoresTimer < PFTITLE_HIGHSCORES_TICKS_MAX) mScoresTimer ++;
    }
    //--Player is not in high scores mode.
    else
    {
        if(mScoresTimer > 0) mScoresTimer --;
    }

    ///--[Cloud Handling]
    //--When the timer hits its cap, reset it and spawn a cloud.
    mCloudSpawnTimer ++;
    if(mCloudSpawnTimer >= PFTITLE_CLOUD_TICKS)
    {
        //--Reset.
        mCloudSpawnTimer = 0;

        //--Spawn.
        SpawnCloudEdge();
    }

    //--Move clouds.
    PFTitle_Cloud *rCloud = (PFTitle_Cloud *)mCloudList->SetToHeadAndReturn();
    while(rCloud)
    {
        //--Move.
        rCloud->mXPos = rCloud->mXPos + rCloud->mXSpeed;

        //--Upon hitting the right side of the screen, despawn.
        if(rCloud->mXPos >= VIRTUAL_CANVAS_X)
        {
            mCloudList->RemoveRandomPointerEntry();
        }

        //--Next.
        rCloud = (PFTitle_Cloud *)mCloudList->IncrementAndGetRandomPointerEntry();
    }

    ///--[ ========== Fading to Level ========= ]
    //--Blocks out all other input.
    if(mFadeToLevelTimer > 0)
    {
        //--Run timer.
        mFadeToLevelTimer ++;

        //--Ending case.
        if(mFadeToLevelTimer >= PFTITLE_FADE_TO_LEVEL_TICKS)
        {
            //--Resolve path.
            DataLibrary *rDataLibrary = DataLibrary::Fetch();
            SysVar *rPathVariable = (SysVar *)rDataLibrary->GetEntry("Root/Paths/System/Startup/sPeakFreaksPath");
            if(!rPathVariable || !rPathVariable->mAlpha) return;

            //--Setup.
            char *tPath = NULL;

            //--To Tutorial:
            if(mFadeCase == PFTITLE_TO_TUTORIAL)
            {
                tPath = InitializeString("%s%s", rPathVariable->mAlpha, "/Maps/Tutorial/Constructor.lua");
            }
            //--Playing a level:
            else
            {
                tPath = InitializeString("%s%s", rPathVariable->mAlpha, "/Maps/SampleLevel/Constructor.lua");
            }

            //--Execute.
            LuaManager::Fetch()->ExecuteLuaFile(tPath);

            //--This object is now unstable and needs to exit.
            free(tPath);
        }
        return;
    }

    ///--[ ======== Alternate Controls ======== ]
    if(mIsDifficultyMode) { UpdateDifficulty(); return; }
    if(mIsScoresMode)     { UpdateScores();     return; }

    ///--[ ========== Player Controls ========= ]
    ///--[Controls]
    if(rControlManager->IsFirstPress("PF Up"))
    {
        //--Move.
        mCurSelection --;
        while(mCurSelection < 0) mCurSelection += PFTITLE_OPTION_TOTAL;

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Change");
    }
    else if(rControlManager->IsFirstPress("PF Down"))
    {
        //--Move.
        mCurSelection ++;
        while(mCurSelection >= PFTITLE_OPTION_TOTAL) mCurSelection -= PFTITLE_OPTION_TOTAL;

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Change");
    }

    //--Activate.
    if(rControlManager->IsFirstPress("PF Activate"))
    {
        //--Start the game:
        if(mCurSelection == PFTITLE_OPTION_START)
        {
            mIsDifficultyMode = true;
            AudioManager::Fetch()->PlaySound("PF Accept");
        }
        //--Start the tutorial:
        else if(mCurSelection == PFTITLE_OPTION_TUTORIAL)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("PF Accept");

            //--Begin fading.
            mFadeCase = PFTITLE_TO_TUTORIAL;
            mFadeToLevelTimer = 1;
            return;
        }
        //--High scores:
        else if(mCurSelection == PFTITLE_OPTION_SCORES)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("PF Accept");

            //--Flags.
            mIsScoresMode = true;

            //--Adjust music volume.
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVolumeNormal * 0.50f);

            //--Scatter timers.
            mScoresScooterTimer = rand() % PFTITLE_HIGHSCORES_TIME_SCOOTER;
            mScoresMimiTimer    = rand() % PFTITLE_HIGHSCORES_TIME_MIMI;
            mScoresAllenTimer   = rand() % PFTITLE_HIGHSCORES_TIME_ALLEN;
        }
    }
}
void PeakFreaksTitle::UpdateDifficulty()
{
    ///--[Documentation]
    //--Updates player controls (not timers) for difficulty-select mode. Timers are handled in the
    //  base update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Controls]
    if(rControlManager->IsFirstPress("PF Up"))
    {
        //--Move.
        mDifficultySelection --;
        while(mDifficultySelection < 0) mDifficultySelection += PTTITLE_DIFFICULTY_MAX;

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Change");
    }
    else if(rControlManager->IsFirstPress("PF Down"))
    {
        //--Move.
        mDifficultySelection ++;
        while(mDifficultySelection >= PTTITLE_DIFFICULTY_MAX) mDifficultySelection -= PTTITLE_DIFFICULTY_MAX;

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Change");
    }

    //--Activate.
    if(rControlManager->IsFirstPress("PF Activate"))
    {
        //--Common:
        mFadeCase = PFTITLE_TO_LEVEL;
        mFadeToLevelTimer = 1;

        //--Fade music out.
        AudioManager::Fetch()->FadeMusic(15);

        //--Start the game:
        if(mDifficultySelection == PTTITLE_DIFFICULTY_SIMPLE)
        {
            PeakFreaksLevel::xDifficulty = PTTITLE_DIFFICULTY_SIMPLE;
        }
        //--Medium difficulty:
        else if(mDifficultySelection == PTTITLE_DIFFICULTY_CHALLENGING)
        {
            PeakFreaksLevel::xDifficulty = PTTITLE_DIFFICULTY_CHALLENGING;
        }
        //--MAXIMUM DANGER
        else if(mDifficultySelection == PTTITLE_DIFFICULTY_IMPOSSIBLE)
        {
            PeakFreaksLevel::xDifficulty = PTTITLE_DIFFICULTY_IMPOSSIBLE;
        }
        return;
    }

    //--Cancel. Exits difficulty select.
    if(rControlManager->IsFirstPress("PF Cancel"))
    {
        mIsDifficultyMode = false;
        AudioManager::Fetch()->PlaySound("PF Cancel");
    }
}
void PeakFreaksTitle::UpdateScores()
{
    ///--[Documentation]
    //--Updates player controls (not timers) for high scores mode.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Cancel. Exits scores mode.
    if(rControlManager->IsFirstPress("PF Cancel"))
    {
        mIsScoresMode = false;

        //--Adjust music volume.
        AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVolumeNormal * 1.00f);

        //--SFX.
        AudioManager::Fetch()->PlaySound("PF Cancel");
    }
}

///========================================= File I/O =============================================
PFTitle_Highscore *PeakFreaksTitle::ReadHighscores()
{
    ///--[Documentation]
    //--Reads a highscores structure from the hard drive and returns it. If the file doesn't exist then
    //  the structure is returned but with default values.
    PFTitle_Highscore *nStructure = (PFTitle_Highscore *)starmemoryalloc(sizeof(PFTitle_Highscore));
    nStructure->Initialize();

    //--Get and check the file.
    FILE *fInfile = fopen("Highscore.slf", "rb");
    if(!fInfile) return nStructure;

    //--Read data.
    fread(nStructure, sizeof(PFTitle_Highscore), 1, fInfile);

    //--Finish up.
    fclose(fInfile);
    return nStructure;
}
void PeakFreaksTitle::WriteHighscores(PFTitle_Highscore *pScoreStruct)
{
    ///--[Documentation]
    //--Given a highscore structure, writes it to the hard drive.
    if(!pScoreStruct) return;

    //--Get and check the file.
    FILE *fOutfile = fopen("Highscore.slf", "wb");
    if(!fOutfile) return;

    //--Write.
    fwrite(pScoreStruct, sizeof(PFTitle_Highscore), 1, fOutfile);

    //--Finish up.
    fclose(fOutfile);
}

///========================================== Drawing =============================================
void PeakFreaksTitle::RenderMenuOption(int pSlot, int pCursor, SugarBitmap *pImage, SugarBitmap *pSelImg)
{
    ///--[Documentation]
    //--Renders the menu option in the specified slot. Menu options expand and contract when selected,
    //  so the algorithm is quite complex.
    if(pSlot < 0 || pSlot >= PFTITLE_OPTION_TOTAL) return;
    if(!pImage || !pSelImg) return;

    //--Resolve image.
    SugarBitmap *rUseImg = pImage;
    if(pCursor == pSlot) rUseImg = pSelImg;

    //--Render.
    rUseImg->Draw();
}
void PeakFreaksTitle::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void PeakFreaksTitle::Render()
{
    ///--[External]
    //--Flip this flag to let the engine know we handled rendering this tick.
    MapManager::xHasRenderedMenus = true;

    ///--[Error Checks]
    //--Fail if the images did not resolve.
    if(!Images.mIsReady) return;

    ///--[ ============== Finale ============== ]
    //--In finale mode, call the finale update and stop.
    if(mIsFinaleMode) { RenderFinale(); return ;}

    //--Compute offset for background.
    float cBGOffset = EasingFunction::QuadraticInOut(mDifficultyTimer, PFTITLE_DIFFSWITCH_TICKS) * 232.0f;

    ///--[Static Pieces]
    //--Sky starts with an offset of -232.0f to allow some "look up" room.
    glTranslatef(0.0f, cBGOffset, 0.0f);
    Images.Data.rBackgroundSky->Draw(0.0f, -232.0f);

    ///--[Clouds]
    PFTitle_Cloud *rCloud = (PFTitle_Cloud *)mCloudList->PushIterator();
    while(rCloud)
    {
        rCloud->rImage->Draw(rCloud->mXPos, rCloud->mYPos);
        rCloud = (PFTitle_Cloud *)mCloudList->AutoIterate();
    }

    ///--[Foreground Pieces]
    //--Mountain follows BG.
    Images.Data.rBackgroundMtn->Draw();
    glTranslatef(0.0f, -cBGOffset, 0.0f);

    //--Fixed position.
    Images.Data.rNotifierKeyboard->Draw();

    ///--[Main Menu]
    if(mDifficultyTimer < (PFTITLE_DIFFSWITCH_TICKS * 0.25f) && mScoresTimer < PFTITLE_HIGHSCORES_TICKS_FADEOUT)
    {
        ///--[Menu]
        RenderMenuOption(PFTITLE_OPTION_START,    mCurSelection, Images.Data.rTextStart,    Images.Data.rSelectedStart);
        RenderMenuOption(PFTITLE_OPTION_TUTORIAL, mCurSelection, Images.Data.rTextTutorial, Images.Data.rSelectedTutorial);
        RenderMenuOption(PFTITLE_OPTION_SCORES,   mCurSelection, Images.Data.rTextScores,   Images.Data.rSelectedScores);

        ///--[Cursor]
        //--Resolve X position.
        float tPct = (float)(mCursorTimer / PFTITLE_CURSOR_CYCLE_TICKS) * 3.1415926f * 2.0f;
        float cCursorPosX = 13.0f + (sinf(tPct) * PFTITLE_CURSOR_CYCLE_DIST);

        //--Resolve Y position.
        float cCursorPosY = 451.0f;
        if(mCurSelection == PFTITLE_OPTION_TUTORIAL) cCursorPosY = 521.0f;
        if(mCurSelection == PFTITLE_OPTION_SCORES)   cCursorPosY = 593.0f;

        //--Render.
        Images.Data.rCursor->Draw(cCursorPosX, cCursorPosY);
    }
    ///--[Difficulty Menu]
    else if(mDifficultyTimer >= (PFTITLE_DIFFSWITCH_TICKS * 0.75f))
    {
        ///--[Menu]
        RenderMenuOption(PTTITLE_DIFFICULTY_SIMPLE,      mDifficultySelection, Images.Data.rDiffText[0], Images.Data.rDiffSelected[0]);
        RenderMenuOption(PTTITLE_DIFFICULTY_CHALLENGING, mDifficultySelection, Images.Data.rDiffText[1], Images.Data.rDiffSelected[1]);
        RenderMenuOption(PTTITLE_DIFFICULTY_IMPOSSIBLE,  mDifficultySelection, Images.Data.rDiffText[2], Images.Data.rDiffSelected[2]);

        ///--[Cursor]
        //--Resolve X position.
        float tPct = (float)(mCursorTimer / PFTITLE_CURSOR_CYCLE_TICKS) * 3.1415926f * 2.0f;
        float cCursorPosX = 13.0f + (sinf(tPct) * PFTITLE_CURSOR_CYCLE_DIST);

        //--Resolve Y position.
        float cCursorPosY = 451.0f;
        if(mDifficultySelection == PTTITLE_DIFFICULTY_CHALLENGING) cCursorPosY = 521.0f;
        if(mDifficultySelection == PTTITLE_DIFFICULTY_IMPOSSIBLE)  cCursorPosY = 593.0f;

        //--Render.
        Images.Data.rCursor->Draw(cCursorPosX, cCursorPosY);
    }
    ///--[High Scores Menu]
    //--Note: Blocks rendering of all other features.
    else if(mScoresTimer >= PFTITLE_HIGHSCORES_TICKS_FADEOUT)
    {
        //--Fade-in timer.
        float cFadeInPct = 1.0f - EasingFunction::QuadraticOut(mScoresTimer-PFTITLE_HIGHSCORES_TICKS_FADEOUT, PFTITLE_HIGHSCORES_TICKS_MAX-PFTITLE_HIGHSCORES_TICKS_FADEOUT);

        //--Compute vertical offsets.
        float cCharacterOffY = -768.0f * cFadeInPct;
        float cUIOffY = 768.0f * cFadeInPct;

        //--Background.
        Images.Finale.rBackgroundSky->Draw();

        //--Clouds. Scrolls around.
        PFTitle_Cloud *rCloud = (PFTitle_Cloud *)mCloudList->PushIterator();
        while(rCloud)
        {
            rCloud->rImage->Draw(rCloud->mXPos, rCloud->mYPos);
            rCloud = (PFTitle_Cloud *)mCloudList->AutoIterate();
        }

        //--Fixed UI pieces.
        Images.Data.rScoresBack->Draw(0.0f, cCharacterOffY);

        //--Characters are offset from top down.
        glTranslatef(0.0f, cCharacterOffY, 0.0f);

        //--Floating characters.
        float cScooterPct = EasingFunction::Linear(mScoresScooterTimer, PFTITLE_HIGHSCORES_TIME_SCOOTER);
        float cScooterY = -62.0f + sinf(cScooterPct * 3.1415926f * 2.0f)  * 10.0f;
        Images.Data.rDiffPortraits[0]->Draw(165.0f, cScooterY, FLIP_HORIZONTAL);
        float cMimiPct = EasingFunction::Linear(mScoresMimiTimer, PFTITLE_HIGHSCORES_TIME_MIMI);
        float cMimiY = -8.0f + sinf(cMimiPct * 3.1415926f * 2.0f) * 10.0f;
        Images.Data.rDiffPortraits[1]->Draw(433.0f, cMimiY, FLIP_HORIZONTAL);
        float cAllenPct = EasingFunction::Linear(mScoresAllenTimer, PFTITLE_HIGHSCORES_TIME_ALLEN);
        float cAllenY = -99.0f + sinf(cAllenPct * 3.1415926f * 2.0f) * 10.0f;
        Images.Data.rDiffPortraits[2]->Draw(729.0f, cAllenY, FLIP_HORIZONTAL);
        glTranslatef(0.0f, cCharacterOffY * -1.0f, 0.0f);

        //--Scoreboard backings.
        glTranslatef(0.0f, cUIOffY, 0.0f);
        Images.Data.rScoresBoard->Draw(270.0f, 294.0f);
        Images.Data.rScoresBoard->Draw(589.0f, 294.0f);
        Images.Data.rScoresBoard->Draw(910.0f, 294.0f);

        //--Easy scoreboard values.
        float cXPos = 308.0f;
        float cYPos = 328.0f;
        float cYSpc =  65.0f;
        for(int i = 0; i < PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY; i ++)
        {
            //--If the score value is 0, print the "Empty" indicator.
            if(mScoreTableEasy[i] == 0)
            {
                Images.Data.rScoresEmpty->Draw(cXPos, cYPos + (cYSpc * i));
            }
            //--Otherwise, turn the tick value into a seconds indicator.
            else
            {
                ResolveScoreIntoBuffer(mScoreTableEasy[i]);
                Images.Data.rScoresFont->DrawText(cXPos, cYPos + (cYSpc * i), 0, 1.0f, mScoreTimeBuffer);
            }
        }

        //--Medium scoreboard values.
        cXPos = 630.0f;
        for(int i = 0; i < PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY; i ++)
        {
            //--If the score value is 0, print the "Empty" indicator.
            if(mScoreTableMedium[i] == 0)
            {
                Images.Data.rScoresEmpty->Draw(cXPos, cYPos + (cYSpc * i));
            }
            //--Otherwise, turn the tick value into a seconds indicator.
            else
            {
                ResolveScoreIntoBuffer(mScoreTableMedium[i]);
                Images.Data.rScoresFont->DrawText(cXPos, cYPos + (cYSpc * i), 0, 1.0f, mScoreTimeBuffer);
            }
        }

        //--Medium scoreboard values.
        cXPos = 953.0f;
        for(int i = 0; i < PFTITLE_HIGHSCORES_SCORES_PER_DIFFICULTY; i ++)
        {
            //--If the score value is 0, print the "Empty" indicator.
            if(mScoreTableHard[i] == 0)
            {
                Images.Data.rScoresEmpty->Draw(cXPos, cYPos + (cYSpc * i));
            }
            //--Otherwise, turn the tick value into a seconds indicator.
            else
            {
                ResolveScoreIntoBuffer(mScoreTableHard[i]);
                Images.Data.rScoresFont->DrawText(cXPos, cYPos + (cYSpc * i), 0, 1.0f, mScoreTimeBuffer);
            }
        }

        //--Trophies.
        if(mScoresDefeatedScooter)
        {
            Images.Data.rScoresTrophyScooter->Draw();
        }
        if(mScoresDefeatedMimi)
        {
            Images.Data.rScoresTrophyMimi->Draw();
        }
        if(mScoresDefeatedAllen)
        {
            Images.Data.rScoresTrophyAllen->Draw();
        }

        glTranslatef(0.0f, cUIOffY * -1.0f, 0.0f);

        //--Fading in:
        if(mScoresTimer < PFTITLE_HIGHSCORES_TICKS_MAX)
        {
            SugarBitmap::DrawFullBlack(cFadeInPct);
        }
        return;
    }

    ///--[Logo]
    int tLogoFrame = (mLogoTimer / PFTITLE_LOGO_TPF) % PFTITLE_LOGO_FRAMES;
    Images.Data.rLogoFrames[tLogoFrame]->Draw(30, 72);

    ///--[Flags]
    //--Follows BG offset.
    glTranslatef(0.0f, cBGOffset, 0.0f);
    int tFlagFrame = (mFlagTimer / PFTITLE_FLAG_TPF) % PFTITLE_FLAG_FRAMES;
    Images.Data.rFlagFrames[tFlagFrame]->Draw(837, 0);
    glTranslatef(0.0f, -cBGOffset, 0.0f);

    ///--[Spinner]
    //--Resolve frame.
    int cSpinTimer = mSpinnerTimer % PFTITLE_SPINNER_TPF;
    int cSpinnerFrame = (mSpinnerTimer / PFTITLE_SPINNER_TPF) % PFTITLE_SPINNER_FRAMES;

    //--Position.
    float cXPos = 0.0f;
    if(cSpinTimer >= PFTITLE_SPINNER_SHAKE)
    {
        cXPos = (((cSpinTimer - PFTITLE_SPINNER_SHAKE) % 3) - 1) * 3;
    }

    //--Render.
    Images.Data.rSpinnerFrames[cSpinnerFrame]->Draw(cXPos, cBGOffset);

    ///--[Player]
    //--Scrolls down from the top. Only appears during difficulty selection.
    //if(mPlayerSlideTimer > 0)
    {
        //--Resolve flag.
        int tFlag = FLIP_HORIZONTAL;

        //--Resolve position.
        float cPct = EasingFunction::QuadraticInOut(mPlayerSlideTimer, PFTITLE_PLAYERSLIDE_TICKS);
        cPct = 1.0f;
        float tPlayerXPos = -490.0f;
        float tPlayerYPos =  -60.0f + (-1000.0f * (1.0f - cPct));
        if(mIsDifficultyMode)
        {
            tPlayerXPos = 590.0f;
            tFlag = 0;
        }
        Images.Data.rDiffPlayerPortrait->Draw(tPlayerXPos, tPlayerYPos, tFlag);
    }

    ///--[Versus Rival]
    //--Rival Portrait only appears during difficulty selection. Each rival has their own timer.
    for(int i = 0; i < PTTITLE_DIFFICULTY_MAX; i ++)
    {
        //--If the timer is zero, render nothing.
        if(mVersusSlideTimers[i] < 1) continue;

        //--Constants.
        float cOffset = 300.0f;

        //--Resolve percentage to render.
        float cPct = EasingFunction::QuadraticInOut(mVersusSlideTimers[i], PFTITLE_RIVALSLIDE_TICKS);

        //--Everything uses the percent as an alpha.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);

        //--Resolve position.
        float tRivalXPos = -28.0f + (cOffset * (1.0f - cPct));
        float tRivalYPos = -82.0f;
        Images.Data.rDiffPortraits[i]->Draw(tRivalXPos, tRivalYPos);

        //--Also render their title.
        float tTitleXPos = (cOffset * (1.0f - cPct));
        Images.Data.rDiffTitles[i]->Draw(tTitleXPos, 0.0f);

        //--Render their callout. This uses very different fade/scale mechanics.
        float cQuoteX = 813.0f + tTitleXPos;
        float cQuoteY = 444.0f;
        glTranslatef(cQuoteX, cQuoteY, 0.0f);

        //--Compute scale. Bottom clamp is 0.01f.
        float cScale = cPct;
        if(cScale < 0.01f) cScale = 0.01f;

        //--Move to center.
        float cHfWid = Images.Data.rDiffQuotes[i]->GetTrueWidth()  * 0.50f;
        float cHfHei = Images.Data.rDiffQuotes[i]->GetTrueHeight() * 0.50f;
        glTranslatef(cHfWid, cHfHei, 0.0f);
        glScalef(cScale, cScale, 1.0f);

        //--Render centered.
        Images.Data.rDiffQuotes[i]->Draw(-cHfWid, -cHfHei);

        //--Clean.
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-cHfWid, -cHfHei, 0.0f);
        glTranslatef(-cQuoteX, -cQuoteY, 0.0f);
        StarlightColor::ClearMixer();

        //--Render the back button.
        float cBackOffset = -768.0f * (1.0f - cPct);
        Images.Data.rScoresBack->Draw(0.0f, cBackOffset);
    }

    //--Versus indicator. Uses the same percent logic as the player, but renders over the rival.
    if(mPlayerSlideTimer > 0)
    {
        float cPct = EasingFunction::QuadraticInOut(mPlayerSlideTimer, PFTITLE_PLAYERSLIDE_TICKS);
        float tVersusYPos = (-1000.0f * (1.0f - cPct));
        Images.Data.rDiffVersus->Draw(0.0f, tVersusYPos);
    }

    ///--[Overlay]
    if(mFadeToLevelTimer >= 1)
    {
        float cPct = EasingFunction::QuadraticIn(mFadeToLevelTimer, PFTITLE_FADE_TO_LEVEL_TICKS);
        SugarBitmap::DrawFullBlack(cPct);
    }
    if(mOpenTimer < PFTITLE_FADE_TO_LEVEL_TICKS)
    {
        float cPct = 1.0f - EasingFunction::QuadraticIn(mOpenTimer, PFTITLE_FADE_TO_LEVEL_TICKS);
        SugarBitmap::DrawFullBlack(cPct);
    }

    //--High scores switch.
    if(mScoresTimer > 0 && mScoresTimer < PFTITLE_HIGHSCORES_TICKS_FADEOUT)
    {
        //--Compute percent.
        float cPct = EasingFunction::QuadraticIn(mScoresTimer, PFTITLE_HIGHSCORES_TICKS_FADEOUT);
        SugarBitmap::DrawFullBlack(cPct);
    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void PeakFreaksTitle::HookToLuaState(lua_State *pLuaState)
{
    /* PeakTitle_Create()
       Creates a new PeakFreaksTitle and gives it to the MapManager. */
    lua_register(pLuaState, "PeakTitle_Create", &Hook_PeakTitle_Create);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_PeakTitle_Create(lua_State *L)
{
    //PeakTitle_Create()
    PeakFreaksTitle *nLevel = new PeakFreaksTitle();
    nLevel->Construct();
    MapManager::Fetch()->ReceiveLevel(nLevel);
    return 0;
}
