-- |[ ===================================== Damage Ability ===================================== ]|
--An extremely basic damage ability, nothing special at all. On purpose!
local iArgsTotal = LM_GetNumOfArgs()
if(iArgsTotal < 1) then return end

--Argument resolve.
local ciSwitchCode = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Construction ====================================== ]|
if(ciSwitchCode == ObjectPrototype.ciScriptCreate) then
    
    -- |[ =================== Setup ==================== ]|
    --Create.
    SOB_CreateEntry("Damage Ability")
    
    --System.
    SOP_SetProperty("Execution Path", LM_GetCallStack(0))
    
    --Description.
    SOP_SetProperty("Set Description Auto", 
        "An ability that deals basic damage and has no special bells or whistles.")

    -- |[ =================== Fields =================== ]|
    -- |[System]|
    SOP_SetProperty("Add String Field",      "Character Name", "Mei")
    SOP_SetProperty("Set Field Description", "Character Name", "Name of the character in question. Eg: Mei, Christine.")

    SOP_SetProperty("Add String Field",      "Job Name", "Fencer")
    SOP_SetProperty("Set Field Description", "Job Name", "Name of the job in question. Eg: Fencer, Lancer.")

    SOP_SetProperty("Add String Field",      "Skill Name", "Attack")
    SOP_SetProperty("Set Field Description", "Skill Name", "Name of the skill in question. Eg: Attack, Rend, Blind.")

    SOP_SetProperty("Add String Field",      "Icon Name", "Null")
    SOP_SetProperty("Set Field Description", "Icon Name", "The path of the icon used to represent the skill.[BR]Leave as Null to attempt to auto-generate the path based on charname/class/skillname.")

    -- |[Usability]|
    SOP_SetProperty("Add Integer Field",     "JP Cost", 100)
    SOP_SetProperty("Set Field Description", "JP Cost", "How much JP the skill costs to purchase, can be zero.[BR]Common JP Costs are 50 (Cheap), 100 (Normal), 150 (Advanced), Passive (300), Unique (500)")
    
    SOP_SetProperty("Add Boolean Field",     "Is Free Action", 0)
    SOP_SetProperty("Set Field Description", "Is Free Action", "A free action can be used without ending the user's turn, if a free action charge is available.[BR]Set to 1 for free actions, all else are 0.")

    SOP_SetProperty("Add Integer Field",     "MP Cost", 0)
    SOP_SetProperty("Set Field Description", "MP Cost", "How much MP the skill costs to use in battle. Can be zero.")

    SOP_SetProperty("Add Integer Field",     "CP Cost", 0)
    SOP_SetProperty("Set Field Description", "CP Cost", "How much CP the skill costs to use in battle. Can be zero.")

    SOP_SetProperty("Add Integer Field",     "Cooldown", 0)
    SOP_SetProperty("Set Field Description", "Cooldown", "Number of turn ends that must elapse between uses of the skill. 1 means free actions cannot be chained, every-other-turn is cooldown = 2. Can be zero.")

    SOP_SetProperty("Add String Field",      "Targeting", "Target Enemies Single")
    SOP_SetProperty("Set Field Description", "Targeting", "Which method the skill uses to target enemies or friendlies. Full list is in AdvCombatTargeting.cc. Common entries:[BR]"..
        "Target Self, Target Enemies Single, Target Enemies All, Target Allies Single, Target Allies All, Target All Single, Target Parties, Target All")

    SOP_SetProperty("Add Integer Field",     "CP Generate", 0)
    SOP_SetProperty("Set Field Description", "CP Generate", "How much CP the skill generates when used. Typically 1 for turn-ending actions, 0 for free actions, and 0 for CP-using skills.")

    -- |[Damage Properties]|
    SOP_SetProperty("Add Integer Field",              "Damage Type", -1)
    SOP_SetProperty("Allocate Enumerations In Field", "Damage Type", 13)
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  0, -1, "Use Weapon")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  1,  0, "Slash")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  2,  1, "Strike")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  3,  2, "Pierce")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  4,  3, "Flame")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  5,  4, "Freeze")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  6,  5, "Shock")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  7,  6, "Crusade")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  8,  7, "Obscure")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type",  9,  8, "Bleed")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type", 10,  9, "Poison")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type", 11, 10, "Corrode")
    SOP_SetProperty("Set Enumerations In Field",      "Damage Type", 12, 11, "Terrify")
    SOP_SetProperty("Set Enumerations Locking Flag",  "Damage Type", true)
    SOP_SetProperty("Set Field Description",          "Damage Type", "The damage type that this attack uses for computations. -1 is 'Use Equipped Weapon'.[BR]"..
        "Slash=0, Strike, Pierce, Flame=3, Freeze, Shock, Crusade=6, Obscure, Bleed, Poison=9, Corrode, Terrify=11")

    SOP_SetProperty("Add String Field",      "Animation Name", "Null")
    SOP_SetProperty("Set Field Description", "Animation Name", "Name of the animation that plays when the skill is used. Leave as Null to use the damage type animation.[BR]"..
        "Bleed, Blind, Buff, Claw Slash, Corrode, Debuff, Flames, Freeze, Gun Shot, Haste, Healing, Laser Shot, Light, Pierce, PierceF, Poison, ShadowA, ShadowB, ShadowC, Shock, Slow, "..
        "Strike, Sword Slash, Terrify, Whip, Slash Lava")

    SOP_SetProperty("Add Integer Field",     "Miss Rate", 5)
    SOP_SetProperty("Set Field Description", "Miss Rate", "Base chance for the attack to miss. Typically 5, high-hit-rate skills may have negative values.")

    SOP_SetProperty("Add Float Field",       "Damage Factor", 1.0)
    SOP_SetProperty("Set Field Description", "Damage Factor", "Factor multiplied by user's attack power to determine damage. 1.0x is default attack power.")

    
-- |[ ======================================= Execution ======================================== ]|
elseif(ciSwitchCode == ObjectPrototype.ciScriptExecute) then

    -- |[ =================== Setup ==================== ]|
    -- |[Diagnostics]|
    local iTotalFields = SOP_GetProperty("Total Fields")

    -- |[Fields]|
    --System
    local sCharName     = SOP_GetProperty("Get String Field",  "Character Name")
    local sJobName      = SOP_GetProperty("Get String Field",  "Job Name")
    local sSkillName    = SOP_GetProperty("Get String Field",  "Skill Name")
    local sIconName     = SOP_GetProperty("Get String Field",  "Icon Name")
    
    --Usability
    local iJPCost       = SOP_GetProperty("Get Integer Field", "JP Cost")
    local iIsFreeAction = SOP_GetProperty("Get Integer Field", "Is Free Action")
    local iMPCost       = SOP_GetProperty("Get Integer Field", "MP Cost")
    local iCPCost       = SOP_GetProperty("Get Integer Field", "CP Cost")
    local iCooldown     = SOP_GetProperty("Get Integer Field", "Cooldown")
    local sTargetFlag   = SOP_GetProperty("Get String Field",  "Targeting")
    local iCPGenerate   = SOP_GetProperty("Get Integer Field", "CP Generate")
    
    --Damage
    local iDamageType   = SOP_GetProperty("Get Integer Field", "Damage Type")
    local sAnimName     = SOP_GetProperty("Get String Field",  "Animation Name")
    local iMissRate     = SOP_GetProperty("Get Integer Field", "Miss Rate")
    local fDamageFactor = SOP_GetProperty("Get Float Field",   "Damage Factor")
    
    -- |[No-Space Versions]|
    --Ability names can't have spaces in the variable names. A utility function can strip those out.
    local sCharNameNospace  = ObjectPrototype:fnNospace(sCharName)
    local sJobNameNospace   = ObjectPrototype:fnNospace(sJobName)
    local sSkillNameNospace = ObjectPrototype:fnNospace(sSkillName)
    
    --Generate the lua skillname.
    local sLuaSkillName = "gzPrototypes.Combat." .. sCharNameNospace .. "." .. sJobNameNospace .. "." .. sSkillNameNospace

    -- |[Formatting and Derived Values]|
    --Adjust the floating point entries to use the normal 2 digits of precision, or more if specified.
    fDamageFactor = ObjectPrototype:fnGetFloatWithPrecision(fDamageFactor)

    --Resolve JP cost if it can be switched to an enumeration.
    local sJPBuf = ObjectPrototype:fnGetJPEnumeration(iJPCost)
    
    --Resolve free action enumeration.
    local sFreeActionBuf = "gbIsNotFreeAction"
    if(iIsFreeAction == 1.0) then sFreeActionBuf = "gbIsFreeAction" end
    
    --Resolve icon name if it was Null.
    if(sIconName == "Null") then
        sIconName = sCharName .. "|" .. sJobName .. "|" .. sSkillName
    end
    
    --MP/CP/etc enumerations
    local sMPEnum = "giNoMPCost"
    if(iMPCost > 0) then sMPEnum = iMPCost end
    local sCPEnum = "giNoCPCost"
    if(iCPCost > 0) then sCPEnum = iCPCost end
    local sCooldownEnum = "giNoCooldown"
    if(iCooldown > 0) then sCooldownEnum = iCooldown end
    local sCPGenEnum = "giNoCPGeneration"
    if(iCPGenerate > 0) then sCPGenEnum = iCPGenerate end
    
    --Damage enumeration.
    local sDamageType = ObjectPrototype:fnGetDamageEnumeration(iDamageType)
    
    --Animation type. If it's "Null" then change it based on the damage type.
    if(sAnimName == "Null") then
        sAnimName = ObjectPrototype:fnGetDamageAnimFromType(iDamageType)
    end

    -- |[ ================= Open File ================== ]|
    --Generate a filename.
    local sFilename = ObjectPrototype:fnGeneratePathname("DamAbi " .. sSkillNameNospace .. " ")
    if(sFilename == "Null") then
        io.write("Warning. Attempted to generate a unique filename but failed.\nPrototype: " .. fnResolvePath() .. "\n")
        return
    end
    
    --Open/create it.
    local fOutfile = io.open(sFilename, "w")

    -- |[ ================== Writing =================== ]|
    -- |[Header]|
    --Generate a header and a comment.
    fOutfile:write(ObjectPrototype:fnCreateBalancedHeader(sSkillName, 100) .. "\n")
    fOutfile:write([[-- |[Description]|]] .. "\n")
    fOutfile:write([[--AUTOGENERATED TO-DO (Put a single-line description here when done)]] .. "\n")
    fOutfile:write([[--*Make this character's job script call this file where needed]] .. "\n")
    fOutfile:write([[--*Give the skill a description]] .. "\n")
    fOutfile:write([[--*Set voice data or comment it out]] .. "\n")
    fOutfile:write([[--*Check animation name, particularly Claw Slash vs Sword Slash]] .. "\n")
    fOutfile:write([[--*Remove this TO-DO when done!]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[-- |[Arguments]|]] .. "\n")
    fOutfile:write([[local iArgumentsTotal = LM_GetNumOfArgs()]] .. "\n")
    fOutfile:write([[if(iArgumentsTotal < 1) then return end]] .. "\n")
    fOutfile:write([[local iSwitchType = LM_GetScriptArgument(0, "N")]] .. "\n")
    fOutfile:write([[]] .. "\n")

    -- |[Prototype Generation]|
    fOutfile:write([[-- |[ ==================================== Ability Prototype =================================== ]|]] .. "\n")
    fOutfile:write([[--Create a prefabrication on first execution to speed up subsequent executions.]] .. "\n")
    fOutfile:write([[if(]] .. sLuaSkillName .. [[ == nil) then]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[    -- |[ ========= Basic Properties ========= ]|]] .. "\n")
    fOutfile:write([[    -- |[System]|]] .. "\n")
    fOutfile:write([[    local zAbiStruct = AbiPrototype:new()]] .. "\n")
    fOutfile:write([[  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)]] .. "\n")
    fOutfile:write([[    zAbiStruct:fnSetSystem("]] .. sJobName .. [[", "]] .. sSkillName .. [[", "$SkillName", ]] .. sJPBuf .. [[, ]] .. sFreeActionBuf .. [[, "Direct", "Active", "]] .. sIconName .. [[", "Direct")]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[    -- |[Descriptions]|]] .. "\n")
    fOutfile:write([[    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\n\n[Costs]"]] .. "\n")
    fOutfile:write([[    zAbiStruct.sSimpleDescMarkdown  = "AUTOGENERATED DESCRIPTION, REPLACE!"]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[    -- |[Usability Variables]|]] .. "\n")
    fOutfile:write([[  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)]] .. "\n")
    fOutfile:write([[    zAbiStruct:fnSetUsability(]] .. sMPEnum .. [[, ]] .. sCPEnum .. [[, ]] .. sCooldownEnum .. [[, "]] .. sTargetFlag .. [[", ]] .. sCPGenEnum .. [[)]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[    -- |[ ========= Execution Package ======== ]|]] .. "\n")
    fOutfile:write([[    -- |[Execution Ability Package]|]] .. "\n")
    fOutfile:write([[    zAbiStruct:fnCreateAbiPack("]] .. sAnimName .. [[")]] .. "\n")
    fOutfile:write([[  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)]] .. "\n")
    fOutfile:write([[    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(]] .. sDamageType .. [[, ]] .. iMissRate .. [[, ]] .. fDamageFactor .. [[)]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[    -- |[ ============= Finish Up ============ ]|]] .. "\n")
    fOutfile:write([[    -- |[Voice]|]] .. "\n")
    fOutfile:write([[    zAbiStruct.zExecutionAbiPackage.sVoiceData = "]] .. sCharName .. [[|Offense|General:Swing"]] .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[    -- |[Finalize]|]] .. "\n")
    fOutfile:write([[    zAbiStruct:fnFinalize()]] .. "\n")
    fOutfile:write([[    ]] .. sLuaSkillName .. [[ = zAbiStruct]] .. "\n")
    fOutfile:write([[end]] .. "\n")
    fOutfile:write([[]] .. "\n")

    -- |[Execution Block]|
    --Standard execution, no special stuff.
    fOutfile:write([[-- |[ ======================================== Execution ======================================= ]|]] .. "\n")
    fOutfile:write([[--Activate prototype.]] .. "\n")
    fOutfile:write([[gzRefAbility = ]] .. sLuaSkillName .. "\n")
    fOutfile:write([[]] .. "\n")
    fOutfile:write([[--Call the standardized handler.]] .. "\n")
    fOutfile:write([[LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)]] .. "\n")

    -- |[ ================= Finish Up ================== ]|
    --Close the file.
    fOutfile:close()
    
    --Report success to the console.
    io.write("Successfully wrote file: " .. sFilename .. "\n")
end