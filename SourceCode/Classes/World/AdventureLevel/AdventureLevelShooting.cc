//--Base
#include "AdventureLevel.h"

//--Classes
#include "TilemapActor.h"
#include "TileLayer.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

///=========================================== System =============================================
void AdventureLevel::ActivateShooting()
{
    mIsShootingActive = true;
    mTennisShooting = false;
    xRespondToShot = false;
    xRespondToShotImmediately = false;
    xStopFiringImmediately = false;
    ResetString(mExecuteExaminableName, NULL);
    rExecuteActor = NULL;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdventureLevel::MarkShootingAsTennis(uint32_t pShooterID)
{
    if(pShooterID == 0) return;
    mTennisShooting = true;
    mTennisID = pShooterID;
}
void AdventureLevel::CancelShooting()
{
    mIsShootingActive = false;
}
void AdventureLevel::ClearTennisShooting()
{
    ///--[Documentation]
    //--In tennis shooting mode, the entire party is affected. Remove special frames and overlays
    //  from all party members.

    ///--[Party Leader]
    //--Remove special frames from all party members.
    TilemapActor *rPlayerEntity = LocatePlayerActor();
    if(rPlayerEntity)
    {
        rPlayerEntity->ActivateSpecialFrame(NULL);
        rPlayerEntity->SetManualRenderOverlay(NULL);
        rPlayerEntity->SetBlockRender(false);
    }

    ///--[Followers]
    //--Iterate.
    uint32_t *rEntityIDPtr = (uint32_t *)mFollowEntityIDList->PushIterator();
    while(rEntityIDPtr)
    {
        //--Get the entity and verify it exists.
        RootEntity *rFollower = EntityManager::Fetch()->GetEntityI(*rEntityIDPtr);
        if(rFollower && rFollower->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rFollower;
            rActor->ActivateSpecialFrame(NULL);
            rActor->SetManualRenderOverlay(NULL);
            rActor->SetBlockRender(false);
        }

        //--Next.
        rEntityIDPtr = (uint32_t *)mFollowEntityIDList->AutoIterate();
    }
}
void AdventureLevel::SetShootingDepth(int pDepth)
{
    mShootingCollisionDepth = pDepth;
}

///======================================= Core Methods ===========================================
void AdventureLevel::FireShot()
{
    mIsFiringSequence = true;
    mShootFiringTimer = 0;
}
void AdventureLevel::ShootAlongVector()
{
    ///--[Documentation]
    //--Once the player fires a shot, this algorithm creates a set of spots it checks for objects
    //  that can be hit. The shot is "hitscan", flying its full distance instantly.
    //--The shot uses a special collision layer to fly, or if not provided, the zeroth layer.
    //  The shot is "wide" in that it can activate objects a few pixels to the side. It can also hit
    //  objects to the side, including collisions.
    //--Iteration stops when the shot hits something or flies a certain distance. Examinable objects
    //  and entities can have a flag allowing a response to the shot. This always triggers before
    //  the collision check.
    TilemapActor *rPlayerEntity = LocatePlayerActor();
    if(!rPlayerEntity) return;

    //--Reset flags.
    mSpawnBulletPuff = false;

    //--Get the starting position.
    float cStartX = rPlayerEntity->GetWorldX();
    float cStartY = rPlayerEntity->GetWorldY();

    //--Bullet puff matches the player's Z.
    mBulletPuffZ = rPlayerEntity->GetRenderDepth();

    //--Get the facing and create a set of speeds and offset checks.
    float cSpeedX = 0.0f;
    float cSpeedY = 0.0f;
    float cXOffsets[AL_SHOOT_OFFSETS];
    float cYOffsets[AL_SHOOT_OFFSETS];
    int tFacing = rPlayerEntity->GetFacing();

    //--Resolve from direction.
    if(tFacing == TA_DIR_NORTH)
    {
        cSpeedX = 0.0f;
        cSpeedY = -2.0f;
        cXOffsets[0] = -1.0f;
        cXOffsets[1] = -2.0f;
        cXOffsets[2] =  1.0f;
        cXOffsets[3] =  2.0f;
        cYOffsets[0] =  0.0f;
        cYOffsets[1] =  0.0f;
        cYOffsets[2] =  0.0f;
        cYOffsets[3] =  0.0f;
    }
    else if(tFacing == TA_DIR_NE)
    {
        cSpeedX = 1.0f;
        cSpeedY = -1.0f;
        cXOffsets[0] = -1.0f;
        cXOffsets[1] = -2.0f;
        cXOffsets[2] =  1.0f;
        cXOffsets[3] =  2.0f;
        cYOffsets[0] = -1.0f;
        cYOffsets[1] = -2.0f;
        cYOffsets[2] =  1.0f;
        cYOffsets[3] =  2.0f;
    }
    else if(tFacing == TA_DIR_EAST)
    {
        cSpeedX = 2.0f;
        cSpeedY = 0.0f;
        cXOffsets[0] =  0.0f;
        cXOffsets[1] =  0.0f;
        cXOffsets[2] =  0.0f;
        cXOffsets[3] =  0.0f;
        cYOffsets[0] = -1.0f;
        cYOffsets[1] = -2.0f;
        cYOffsets[2] =  1.0f;
        cYOffsets[3] =  2.0f;
    }
    else if(tFacing == TA_DIR_SE)
    {
        cSpeedX = 1.0f;
        cSpeedY = 1.0f;
        cXOffsets[0] =  1.0f;
        cXOffsets[1] =  2.0f;
        cXOffsets[2] = -1.0f;
        cXOffsets[3] = -2.0f;
        cYOffsets[0] = -1.0f;
        cYOffsets[1] = -2.0f;
        cYOffsets[2] =  1.0f;
        cYOffsets[3] =  2.0f;
    }
    else if(tFacing == TA_DIR_SOUTH)
    {
        cSpeedX = 0.0f;
        cSpeedY = 2.0f;
        cXOffsets[0] = -1.0f;
        cXOffsets[1] = -2.0f;
        cXOffsets[2] =  1.0f;
        cXOffsets[3] =  2.0f;
        cYOffsets[0] =  0.0f;
        cYOffsets[1] =  0.0f;
        cYOffsets[2] =  0.0f;
        cYOffsets[3] =  0.0f;
    }
    else if(tFacing == TA_DIR_SW)
    {
        cSpeedX = -1.0f;
        cSpeedY = 1.0f;
        cXOffsets[0] = -1.0f;
        cXOffsets[1] = -2.0f;
        cXOffsets[2] =  1.0f;
        cXOffsets[3] =  2.0f;
        cYOffsets[0] = -1.0f;
        cYOffsets[1] = -2.0f;
        cYOffsets[2] =  1.0f;
        cYOffsets[3] =  2.0f;
    }
    else if(tFacing == TA_DIR_WEST)
    {
        cSpeedX = -2.0f;
        cSpeedY = 0.0f;
        cXOffsets[0] =  0.0f;
        cXOffsets[1] =  0.0f;
        cXOffsets[2] =  0.0f;
        cXOffsets[3] =  0.0f;
        cYOffsets[0] = -1.0f;
        cYOffsets[1] = -2.0f;
        cYOffsets[2] =  1.0f;
        cYOffsets[3] =  2.0f;
    }
    else //TA_DIR_NW
    {
        cSpeedX = -1.0f;
        cSpeedY = -1.0f;
        cXOffsets[0] = -1.0f;
        cXOffsets[1] = -2.0f;
        cXOffsets[2] =  1.0f;
        cXOffsets[3] =  2.0f;
        cYOffsets[0] =  1.0f;
        cYOffsets[1] =  2.0f;
        cYOffsets[2] = -1.0f;
        cYOffsets[3] = -2.0f;
    }

    ///--[List Creation]
    //--To save time, create a list of all examinables that can respond to hits.
    StarLinkedList *tExaminableList = new StarLinkedList(false);
    ExaminePackage *rExaminePackage = (ExaminePackage *)mExaminablesList->PushIterator();
    while(rExaminePackage)
    {
        if(rExaminePackage->mFlags & EXAMINE_RESPOND_GUNSHOT)
        {
            tExaminableList->AddElementAsTail(mExaminablesList->GetIteratorName(), rExaminePackage);
        }
        rExaminePackage = (ExaminePackage *)mExaminablesList->AutoIterate();
    }

    //--List of all TilemapActors who can respond to hits.
    StarLinkedList *tActorList = new StarLinkedList(false);
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootObject *rCheckPtr = (RootObject *)rEntityList->PushIterator();
    while(rCheckPtr)
    {
        //--Check type and cast.
        if(rCheckPtr->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckPtr;
            if(rActor->RespondsToShot())
            {
                tActorList->AddElement("X", rActor);
            }
        }

        //--Next.
        rCheckPtr = (RootObject *)rEntityList->AutoIterate();
    }

    ///--[Iteration]
    //--Variables.
    float tCurX = cStartX;
    float tCurY = cStartY;

    //--Reset static flags.
    xRespondToShot = false;
    xRespondToShotImmediately = false;
    xStopFiringImmediately = false;
    xDontSpawnBulletImpact = false;
    ResetString(mExecuteExaminableName, NULL);
    rExecuteActor = NULL;

    //--Now we run until we've moved to the distance cap or hit something.
    int tDistanceMax = 300;
    while(tDistanceMax > 0)
    {
        //--Subtract distance.
        tDistanceMax --;

        //--Advance position.
        tCurX = tCurX + cSpeedX;
        tCurY = tCurY + cSpeedY;

        //--Check for a hit. First, check the center point.
        if(CheckPointForHit(tCurX, tCurY, tExaminableList, tActorList))
        {
            mBulletPuffX = tCurX;
            mBulletPuffY = tCurY;
            mSpawnBulletPuff = true;
            break;
        }

        //--Scan the "side" positions, making the bullet a bit fatter. This helps with bad accuracy.
        for(int i = 0; i < AL_SHOOT_OFFSETS; i ++)
        {
            if(CheckPointForHit(tCurX + cXOffsets[i], tCurY + cYOffsets[i], tExaminableList, tActorList))
            {
                mBulletPuffX = tCurX;
                mBulletPuffY = tCurY;
                mSpawnBulletPuff = true;
                tDistanceMax = 0;
                break;
            }
        }
    }

    //--Clean.
    delete tExaminableList;
    delete tActorList;

    ///--[Issue Retreat Action]
    //--Whenever a shot is fired, all enemies on the map are alerted, same as if the player got into a
    //  battle and retreated.
    SetRetreatTimer(AL_RETREAT_STANDARD);

    ///--[Bullet Puff]
    //--If something got hit, this flag will be set.
    if(mSpawnBulletPuff && !xDontSpawnBulletImpact && !xStopFiringImmediately)
    {
        //--Create, register.
        TilemapActor *nActor = new TilemapActor();
        nActor->SetName("Bullet Puff");
        EntityManager::Fetch()->RegisterPointer("Bullet Puff", nActor);

        //--Compute position with offsets.
        mBulletPuffX = mBulletPuffX - 5.0f;
        mBulletPuffY = mBulletPuffY - 15.0f;

        //--Set position.
        nActor->SetPosition(mBulletPuffX / (float)TileLayer::cxSizePerTile, mBulletPuffY / (float)TileLayer::cxSizePerTile);
        nActor->SetDepth(mBulletPuffZ);
        nActor->SetOverrideDepth(mBulletPuffZ);
        nActor->SetCollisionFlag(false);
        nActor->AddSpecialFrame("Puff0", "Root/Images/Sprites/Impacts/Bullet0");
        nActor->AddSpecialFrame("Puff1", "Root/Images/Sprites/Impacts/Bullet1");
        nActor->AddSpecialFrame("Puff2", "Root/Images/Sprites/Impacts/Bullet2");
        nActor->AddSpecialFrame("Puff3", "Root/Images/Sprites/Impacts/Bullet3");
        nActor->SetToCycleSpecialFramesThenDelete(3);
    }

    ///--[Response Handling]
    //--If we have mExecuteExaminableName being not NULL, it will handle execution after firing ends. If
    //  xRespondToShotImmediately is set, then it handles things immediately.
    if(mExecuteExaminableName && xRespondToShotImmediately)
    {
        LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", mExecuteExaminableName);
        ResetString(mExecuteExaminableName, NULL);
    }

    //--If instead we have an actor, that actor handles the immediate respond.
    if(rExecuteActor && xRespondToShotImmediately)
    {
        rExecuteActor->HandleShotHit();
        rExecuteActor = NULL;
    }

    //--If this flag was set, stop the entirety of the firing sequence here. The cutscene will need
    //  to handle any special frames.
    //--The shooting sequence will run exactly one additional frame to perform cleanup.
    if(xStopFiringImmediately)
    {
        mIsShootingActive = false;
        mIsFiringSequence = false;
        mShootingTimer = 1;
        mShootFiringTimer = 0;
    }
}
bool AdventureLevel::CheckPointForHit(float pX, float pY, StarLinkedList *pExaminableList, StarLinkedList *pActorList)
{
    ///--[Documentation]
    //--Subroutine used by ShootAlongVector() to determine what, if anything, the shot hit at the given position.
    //--Examinables and entities are checked first, then collisions. If anything gets hit, the function returns true.
    //  Otherwise, it returns false.
    if(!pExaminableList || !pActorList) return false;

    ///--[Examinables]
    //--Check examinables for a hit.
    ExaminePackage *rExaminePackage = (ExaminePackage *)pExaminableList->SetToHeadAndReturn();
    while(rExaminePackage)
    {
        //--We got a hit. Run the examinable's script. It will tell us if we need to stop iteration.
        if(pX >= rExaminePackage->mX && pY >= rExaminePackage->mY && pX <= rExaminePackage->mX + rExaminePackage->mW - 1 && pY <= rExaminePackage->mY + rExaminePackage->mH - 1)
        {
            //--Create a buffer. " SHOT CHECK" is applied to the end of the object's name so it can be handled on its own.
            char *tTempString = InitializeString("%s SHOT CHECK", pExaminableList->GetRandomPointerName());
            LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", tTempString);
            free(tTempString);

            //--If this flag flips to true, we handle this hit. Store the name.
            if(xRespondToShot)
            {
                //--The handler string is different than the checker string.
                char *tHandlerString = InitializeString("%s SHOT HANDLER", pExaminableList->GetRandomPointerName());
                ResetString(mExecuteExaminableName, tHandlerString);
                free(tHandlerString);

                //--Exit the loop.
                return true;
            }
            //--Shot hit this object but it did not respond. To speed things up, remove it from the list. Any further
            //  checks that intercept the same object will not waste time querying it again.
            else
            {
                pExaminableList->RemoveRandomPointerEntry();
            }
        }

        //--Next.
        rExaminePackage = (ExaminePackage *)pExaminableList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Entities]
    //--Similar logic to above. TilemapActors can respond using their activation scripts.
    TilemapActor *rActor = (TilemapActor *)pActorList->SetToHeadAndReturn();
    while(rActor)
    {
        //--Subroutine handles the hit detection. It returns true if the shot hit.
        if(rActor->HandleShotCheck(pX, pY))
        {
            //--If this flag is true, the shot hit and this object is responding.
            if(xRespondToShot)
            {
                rExecuteActor = rActor;
                return true;
            }
            //--Shot hit this object but it did not respond. To speed things up, remove it from the list. Any further
            //  checks that intercept the same object will not waste time querying it again.
            else
            {
                pActorList->RemoveRandomPointerEntry();
            }
        }

        //--Next.
        rActor = (TilemapActor *)pActorList->IncrementAndGetRandomPointerEntry();
    }

    ///--[Collisions]
    if(GetClipAt(pX, pY, 0.0f, mShootingCollisionDepth)) return true;

    ///--[Didn't Hit]
    //--Return false to indicate we didn't hit anything.
    return false;
}
void AdventureLevel::ClearShootLists()
{
    mShootListOfExaminables->ClearList();
    mShootListOfTilemapActors->ClearList();
}
void AdventureLevel::CheckPointForHitIntoMaster(float pX, float pY)
{
    //--Checks hits using the master lists provided. If the lists are empty, populates them. The lists
    //  should be cleared each tick.
    if(mShootListOfExaminables->GetListSize() < 1)
    {
        ExaminePackage *rExaminePackage = (ExaminePackage *)mExaminablesList->PushIterator();
        while(rExaminePackage)
        {
            if(rExaminePackage->mFlags & EXAMINE_RESPOND_GUNSHOT)
            {
                mShootListOfExaminables->AddElementAsTail(mExaminablesList->GetIteratorName(), rExaminePackage);
            }
            rExaminePackage = (ExaminePackage *)mExaminablesList->AutoIterate();
        }
    }

    //--List of all TilemapActors who can respond to hits.
    if(mShootListOfTilemapActors->GetListSize() < 1)
    {
        StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
        RootObject *rCheckPtr = (RootObject *)rEntityList->PushIterator();
        while(rCheckPtr)
        {
            //--Check type and cast.
            if(rCheckPtr->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                TilemapActor *rActor = (TilemapActor *)rCheckPtr;
                if(rActor->RespondsToShot())
                {
                    mShootListOfTilemapActors->AddElement("X", rActor);
                }
            }

            //--Next.
            rCheckPtr = (RootObject *)rEntityList->AutoIterate();
        }
    }

    //--Check the point for replies.
    CheckPointForHit(pX, pY, mShootListOfExaminables, mShootListOfTilemapActors);
}

///========================================== Update ==============================================
void MapDirectionToShootingFrame(char *pBuffer, int pDirection)
{
    //--Worker function. Takes the direction and maps the special frame to the buffer for that direction.
    //  The buffer should be clear before appendation begins.
    strcat(pBuffer, "Shoot");

    //--Map direction to letters.
    if(pDirection == TA_DIR_NORTH)
    {
        strcat(pBuffer, "N");
    }
    else if(pDirection == TA_DIR_NE)
    {
        strcat(pBuffer, "NE");
    }
    else if(pDirection == TA_DIR_EAST)
    {
        strcat(pBuffer, "E");
    }
    else if(pDirection == TA_DIR_SE)
    {
        strcat(pBuffer, "SE");
    }
    else if(pDirection == TA_DIR_SOUTH)
    {
        strcat(pBuffer, "S");
    }
    else if(pDirection == TA_DIR_SW)
    {
        strcat(pBuffer, "SW");
    }
    else if(pDirection == TA_DIR_WEST)
    {
        strcat(pBuffer, "W");
    }
    else if(pDirection == TA_DIR_NW)
    {
        strcat(pBuffer, "NW");
    }
}

///======================================= Rifle Shooting =========================================
bool AdventureLevel::UpdateShootingControls()
{
    ///--[ =========== Documentation ========== ]
    //--Handles shooting controls, returning true if this routine is still managing the update. False if not.
    if(!mIsShootingActive && mShootingTimer < 1) return false;

    //--Get entity. If the entity handling the shooting does not exist, cancel shooting.
    TilemapActor *rPlayerEntity = LocatePlayerActor();
    if(!rPlayerEntity)
    {
        mIsShootingActive = false;
        mShootingTimer = 0;
        return false;
    }

    //--Switch over to Tennis mode if needed.
    if(mTennisShooting) return UpdateTennisControls();

    ///--[ ========== Firing Sequence ========= ]
    //--Supercedes controls if the player has started the firing sequence.
    if(mIsFiringSequence)
    {
        //--Timer.
        mShootFiringTimer ++;

        //--Special frame buffer.
        char tBuffer[64];
        tBuffer[0] = '\0';
        int tFacing = rPlayerEntity->GetFacing();
        MapDirectionToShootingFrame(tBuffer, tFacing);

        //--Animate the shot firing.
        if(mShootFiringTimer < AL_SHOOTING_FIRE_TICKS)
        {
            //--Zeroth-tick sound effect.
            if(mShootFiringTimer == 1)
            {
                //--Fire.
                ShootAlongVector();

                //--SFX. Doesn't play if the shot handler cancelled the shot.
                if(mIsFiringSequence) AudioManager::Fetch()->PlaySound("Firearm|RifleFire");
            }

            //--Animation.
            strcat(tBuffer, "2");
        }
        //--On this tick, run the resolver to see what we hit. This is also the recoil frame.
        //  If something got hit, it will run a cancel routine which breaks this logic set.
        //  Otherwise we handle a "miss".
        else if(mShootFiringTimer == AL_SHOOTING_FIRE_TICKS)
        {
            strcat(tBuffer, "3");
        }
        //--Run recoil frames.
        else if(mShootFiringTimer < AL_SHOOTING_FIRE_TICKS + AL_SHOOTING_RECOIL_TICKS)
        {
            //--Sound effect.
            if(mShootFiringTimer == AL_SHOOTING_FIRE_TICKS + (AL_SHOOTING_RECOIL_TICKS / 2))
            {
                AudioManager::Fetch()->PlaySound("Firearm|WorkAction");
            }

            //--Animation.
            strcat(tBuffer, "3");
        }
        //--On the final frame, cancel the firing sequence. This will handle timers as normal.
        else
        {
            mIsFiringSequence = false;
            CancelShooting();
        }

        //--Set special frame and exit.
        if(mIsFiringSequence)
            rPlayerEntity->ActivateSpecialFrame(tBuffer);
        else
            rPlayerEntity->ActivateSpecialFrame(NULL);

        return true;
    }

    ///--[ ============== Timer ============== ]
    //--This timer handles animations. Shots can also not be fired until the timer hits max.
    if(mIsShootingActive)
    {
        if(mShootingTimer < AL_SHOOTING_TICKS)
        {
            //--SFX on this tick:
            if(mShootingTimer == AL_SHOOTING_TICKS / 2)
            {
                AudioManager::Fetch()->PlaySound("Firearm|ReadyGun");
            }

            //--Timer.
            mShootingTimer ++;
        }
    }
    //--Object is not active. Decrement the timer to zero, then stop handling the update.
    else
    {
        if(mShootingTimer > 0)
        {
            //--SFX on this tick:
            if(mShootingTimer == AL_SHOOTING_TICKS -2)
            {
                AudioManager::Fetch()->PlaySound("Firearm|LowerGun");
            }

            //--Timer.
            mShootingTimer --;
        }
    }

    ///--[ ========== Special Frames ========== ]
    //--Handle special frames.
    if(rPlayerEntity)
    {
        //--If the timer is at zero, no special frame is active.
        if(mShootingTimer == 0)
        {
            rPlayerEntity->ActivateSpecialFrame("Null");
        }
        //--Normal handling.
        else
        {
            //--Get facing direction.
            int tFacing = rPlayerEntity->GetFacing();

            //--Special frame buffer.
            char tBuffer[64];
            tBuffer[0] = '\0';
            MapDirectionToShootingFrame(tBuffer, tFacing);

            //--Timing.
            if(mShootingTimer < AL_SHOOTING_TICKS)
            {
                strcat(tBuffer, "0");
            }
            else
            {
                strcat(tBuffer, "1");
            }

            //--Set.
            rPlayerEntity->ActivateSpecialFrame(tBuffer);
        }
    }

    ///--[ ========== Post-Execution ========== ]
    //--Post exec. If a shot is fired and the flags are set a specific way, then the script executes
    //  when the firing sequence ends, not when the shot hits.
    if(!mIsShootingActive && mShootingTimer == 0)
    {
        if(mExecuteExaminableName)
        {
            LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", mExecuteExaminableName);
            ResetString(mExecuteExaminableName, NULL);
        }
        else if(rExecuteActor)
        {
            rExecuteActor->HandleShotHit();
            rExecuteActor = NULL;
        }
    }

    //--If shooting is not active. Just let the timer decrement and exit.
    if(!mIsShootingActive) return false;

    ///--[ ============= Controls ============= ]
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Cancel. Stop firing.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        CancelShooting();
        return true;
    }
    //--Activate. Fire a shot.
    else if(rControlManager->IsFirstPress("Activate"))
    {
        FireShot();
        return true;
    }

    //--Directional handlers.
    bool tIsLfDown = rControlManager->IsDown("Left");
    bool tIsRtDown = rControlManager->IsDown("Right");
    bool tIsUpDown = rControlManager->IsDown("Up");
    bool tIsDnDown = rControlManager->IsDown("Down");

    //--Left handling.
    if(tIsLfDown && !tIsRtDown)
    {
        //--NW:
        if(tIsUpDown && !tIsDnDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_NW);
        }
        //--SW:
        else if(tIsDnDown && !tIsUpDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_SW);
        }
        //--W:
        else
        {
            rPlayerEntity->SetFacing(TA_DIR_WEST);
        }
    }
    //--Right handling.
    else if(tIsRtDown && !tIsLfDown)
    {
        //--NE:
        if(tIsUpDown && !tIsDnDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_NE);
        }
        //--SE:
        else if(tIsDnDown && !tIsUpDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_SE);
        }
        //--W:
        else
        {
            rPlayerEntity->SetFacing(TA_DIR_EAST);
        }
    }
    //--Neither left nor right.
    else
    {
        //--N:
        if(tIsUpDown && !tIsDnDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_NORTH);
        }
        //--S:
        else if(tIsDnDown && !tIsUpDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_SOUTH);
        }
        //--No direction key is down.
        else
        {
        }
    }

    //--Finish up.
    return true;
}

///====================================== Tennis Shooting =========================================
bool AdventureLevel::UpdateTennisControls()
{
    ///--[ ===== Documentation ===== ]
    //--In project Carnation, a different shooting handler is used that launches tennis balls. Identical
    //  to the base update, but changes some of the special frame handlers.
    TilemapActor *rPlayerEntity = LocatePlayerActor();

    ///--[ ========== Firing Sequence ========= ]
    //--Supercedes controls if the player has started the firing sequence.
    if(mIsFiringSequence)
    {
        //--Timer.
        mShootFiringTimer ++;

        //--Firing sequence.
        if(mShootFiringTimer == 1)
        {
            //--Spawn.
            if(xSpawnTennisBallPath)
            {
                //--Resolve spawn position.
                float tX = 0.0f;
                float tY = 0.0f;
                RootEntity *rTennisEntity = EntityManager::Fetch()->GetEntityI(mTennisID);
                if(rTennisEntity)
                {
                    //--Make sure it's a TilemapActor.
                    if(rTennisEntity && rTennisEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
                    {
                        TilemapActor *rTennisActor = (TilemapActor *)rTennisEntity;
                        tX = rTennisActor->GetWorldX();
                        tY = rTennisActor->GetWorldY();
                    }
                }

                //--Get firing angle.
                int tFacing = rPlayerEntity->GetFacing();

                //--Run spawner.
                LuaManager::Fetch()->ExecuteLuaFile(xSpawnTennisBallPath, 3, "N", tX, "N", tY, "N", (float)tFacing);
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Firearm|RifleFire");
        }

        //--Ending.
        if(mShootFiringTimer >= 5)
        {
            //--Flags.
            mIsFiringSequence = false;
            CancelShooting();
            ClearTennisShooting();
        }

        return true;
    }

    ///--[ ============== Timer ============== ]
    //--This timer handles animations. Shots can also not be fired until the timer hits max.
    if(mIsShootingActive)
    {
        if(mShootingTimer < AL_SHOOTING_TICKS)
        {
            //--SFX on this tick:
            if(mShootingTimer == AL_SHOOTING_TICKS / 2)
            {
                AudioManager::Fetch()->PlaySound("Firearm|ReadyGun");
            }

            //--Timer.
            mShootingTimer ++;
        }
    }
    //--Object is not active. Decrement the timer to zero, then stop handling the update.
    else
    {
        if(mShootingTimer > 0)
        {
            //--SFX on this tick:
            if(mShootingTimer == AL_SHOOTING_TICKS -2)
            {
                AudioManager::Fetch()->PlaySound("Firearm|LowerGun");
            }

            //--Timer.
            mShootingTimer --;
        }
    }

    ///--[ ========== Special Frames ========== ]
    //--Handle special frames when actively shooting.
    if(mIsShootingActive)
    {
        //--Get the leader's facing. The leader is the one who moves around.
        int tFacing = 0;
        if(rPlayerEntity) tFacing = rPlayerEntity->GetFacing();

        //--Get the entity that is doing the shooting. It can be the leader, or a follower.
        RootEntity *rTennisEntity = EntityManager::Fetch()->GetEntityI(mTennisID);
        if(rTennisEntity)
        {
            //--Make sure it's a TilemapActor.
            if(rTennisEntity && rTennisEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                //--Cast.
                TilemapActor *rTennisActor = (TilemapActor *)rTennisEntity;

                //--Map.
                char tBuf[128];
                sprintf(tBuf, "Root/Images/Sprites/Special/Beth_Human|TennisDir|%i", tFacing);

                //--Set.
                rTennisActor->SetManualRenderOverlay(tBuf);
            }
        }
    }

    ///--[ ========== Post-Execution ========== ]
    //--Post exec. If a shot is fired and the flags are set a specific way, then the script executes
    //  when the firing sequence ends, not when the shot hits.
    if(!mIsShootingActive && mShootingTimer == 0)
    {
        if(mExecuteExaminableName)
        {
            LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", mExecuteExaminableName);
            ResetString(mExecuteExaminableName, NULL);
        }
        else if(rExecuteActor)
        {
            rExecuteActor->HandleShotHit();
            rExecuteActor = NULL;
        }
    }

    //--If shooting is not active. Just let the timer decrement and exit.
    if(!mIsShootingActive) return false;

    ///--[ ============= Controls ============= ]
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Cancel. Stop firing.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        CancelShooting();
        ClearTennisShooting();
        return true;
    }
    //--Activate. Fire a shot.
    else if(rControlManager->IsFirstPress("Activate"))
    {
        FireShot();
        return true;
    }

    //--Directional handlers.
    bool tIsLfDown = rControlManager->IsDown("Left");
    bool tIsRtDown = rControlManager->IsDown("Right");
    bool tIsUpDown = rControlManager->IsDown("Up");
    bool tIsDnDown = rControlManager->IsDown("Down");

    //--Left handling.
    if(tIsLfDown && !tIsRtDown)
    {
        //--NW:
        if(tIsUpDown && !tIsDnDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_NW);
        }
        //--SW:
        else if(tIsDnDown && !tIsUpDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_SW);
        }
        //--W:
        else
        {
            rPlayerEntity->SetFacing(TA_DIR_WEST);
        }
    }
    //--Right handling.
    else if(tIsRtDown && !tIsLfDown)
    {
        //--NE:
        if(tIsUpDown && !tIsDnDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_NE);
        }
        //--SE:
        else if(tIsDnDown && !tIsUpDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_SE);
        }
        //--W:
        else
        {
            rPlayerEntity->SetFacing(TA_DIR_EAST);
        }
    }
    //--Neither left nor right.
    else
    {
        //--N:
        if(tIsUpDown && !tIsDnDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_NORTH);
        }
        //--S:
        else if(tIsDnDown && !tIsUpDown)
        {
            rPlayerEntity->SetFacing(TA_DIR_SOUTH);
        }
        //--No direction key is down.
        else
        {
        }
    }

    //--Finish up.
    return true;
}
