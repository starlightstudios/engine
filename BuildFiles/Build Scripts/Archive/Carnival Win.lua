-- |[ ================================= Project Carnival Build ================================= ]|
--Operating System: Windows
--Library Type: Allegro/SDL/SDLFMOD
local sGameName = "Carnation"
local sBuildName = "Carnival Win"

--Check existence.
if(BP_Exists(sGameName, sBuildName) == true) then return end

--Create.
BP_Create(sGameName, sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../builds/Project Carnival Win/")

-- |[ ========================================= Common ========================================= ]|
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

-- |[Basics]|
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

-- |[Executables]|
--Executable files.
BP_SetProperty("Register Command", "Copy File", "StarlightEngineAL.exe",      "PandemoniumAL.exe")
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDL.exe",     "PandemoniumSDL.exe")
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDLFMOD.exe", "PandemoniumSDLFMOD.exe")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumAL.exe",        "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumSDL.exe",       "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumSDLFMOD.exe",   "BuildFiles/IconAdv.ico")

--DLL files.
BP_SetProperty("Register Command", "Copy File", "bass.dll",    "bass.dll")
BP_SetProperty("Register Command", "Copy File", "bass_fx.dll", "bass_fx.dll")
BP_SetProperty("Register Command", "Copy File", "fmodL.dll",   "fmodL.dll")
BP_SetProperty("Register Command", "Copy File", "lua54.dll",   "lua54.dll")

-- |[Bootstraps and Counters]|
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

-- |[ =================================== Base Instructions ==================================== ]|
-- |[Text Files]|
--Relevant changelogs.
--BP_SetProperty("Register Command", "Copy File", "Changelog (Adventure).txt", "Changelog.txt")

--Credits files.
--BP_SetProperty("Register Command", "Copy File", "Credits.txt", "Credits.txt")

--Readme.
--BP_SetProperty("Register Command", "Copy File", "Readme.txt", "Readme.txt")

-- |[Configuration Files]|
--Engine and Adventure configurations.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Adventure.lua", "Config_Adventure.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",    "Config_Engine.lua")

-- |[Batch Files]|
--For people with audio problems.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/ZBlockAudio Adventure.bat", "ZBlockAudio.bat")

-- |[ =================================== Engine Subfolders ==================================== ]|
-- |[Data Directory]|
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

-- |[Saves Directory]|
--Copy the controls files.
BP_SetProperty("Register Command", "Create Directory", "Saves")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

--Copy the autosaves directory and the notifier on how to use autosaves.
BP_SetProperty("Register Command", "Create Directory", "Saves/Autosave")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/How to Recover.txt",  "Saves/Autosave/How to Recover.txt")

-- |[Shaders Directory]|
--Copy the Windows/Linux shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/WinLin", "Shaders/WinLin")

-- |[ ==================================== Game Subfolders ===================================== ]|
-- |[Adventure Directories]|
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../adventure/Games/AdventureMode", "Games/AdventureMode")

-- |[Monoceros Directories]|
BP_SetProperty("Register Command", "Copy Folder", "../projectcarnival/Chapter H", "Games/AdventureMode/Chapter H")

-- |[Overrides]|
--Taken from the special files, used to make Monoceros more standalone.
--BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/System/101 Graphics Portraits.lua")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Maps/Nowhere/Constructor.lua")
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/101 Graphics Portraits.lua", "Games/AdventureMode/System/101 Graphics Portraits.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Nowhere Constructor.lua",    "Games/AdventureMode/Maps/Nowhere/Constructor.lua")

-- |[ ====================================== File Deletion ===================================== ]|
--Delete files not used for Monoceros. This keeps the file size down.

-- |[Datafiles]|
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CassandraTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH1Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH2Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH5Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_ChristineTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_GalaDress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_MeiRune.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_MeiTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CassandraTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH1Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH2Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH5Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_ChristineTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_GalaDress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_MeiRune.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_MeiTF.slf")
    
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_56.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Aquillia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Cassandra.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Christine.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Empress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Florentina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Izuna.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_JX101.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Marriedraunes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Mei.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Mia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Miho.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Miso.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Nina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Odar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_PDU.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Polaris.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sammy.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sanya.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Septima.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sharelock.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sophie.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_SX399.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Tiffany.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Yukina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Zeke.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_56.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Aquillia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Cassandra.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Christine.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Empress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Florentina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Izuna.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_JX101.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Marriedraunes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Mei.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Mia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Miho.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Miso.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Nina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Odar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_PDU.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Polaris.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sammy.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sanya.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Septima.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sharelock.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sophie.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_SX399.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Tiffany.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Yukina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Zeke.slf")
    
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionAquaticGenetics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionArbonnePlains.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionArcaneUniversity.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsAlpha.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsBeta.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsDatacore.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsDelta.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsEpsilon.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsGamma.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsHydroponics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsRaijuRanch.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionCryogenics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionDimensionalTrap.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionEquinox.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionEvermoonForest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionLRTFacility.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionNixNedar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionNorthwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionQuantirEstate.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionQuantirHighWastes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionRegulusCitySector15.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionRegulusCitySector96.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionSerenityObservatory.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionStarfieldSwamp.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionTrafalGlacier.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionTrannadarTradingPost.slf")

-- |[Maps]|
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/AAA Debug")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/ArbonnePlains")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Beehive")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/DimensionalTrap")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Evermoon")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/NixNedar")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Quantir")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/QuantirManse")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusArcane")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusBiolabs")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusCity")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusCryo")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusEquinox")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusExterior")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusFinale")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusFlashback")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusLRT")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusManufactory")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusMines")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusSerenity")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RiverWilds")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/StarfieldSwamp")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/StForas")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Trafal")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/TrapBasement")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/TrapDungeon")

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the active object.
DL_PopActiveObject()