//--Base
#include "CarnUIClassChange.h"

//--Classes
#include "AdventureMenu.h"
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
CarnUIClassChange::CarnUIClassChange()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUICLASSCHANGE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CCLC");

    ///--[Visiblity]
    mVisibilityTimerMax = 1;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== CarnUIClassChange ======= ]
    ///--[System]
    mLockFirstSlot = false;

    ///--[Cursor]
    mIsReplaceMode = false;
    mReplaceTimer = 0;
    mCharacterCursor = 0;
    mClassCursor = -1;
    mReplaceCursor = -1;

    ///--[Scrollbar]
    mScrollSkip = 0;

    ///--[Character Info]
    mCharacterName = NULL;
    mCharacterLevel = 0;
    mCharacterHP = 1;
    mCharacterHPMax = 1;
    mCharacterSP = 1;
    mCharacterSPMax = 1;
    mCharacterClassSlots = 1;

    ///--[Class Information]
    mDescriptionClassCursor = -1;
    mDescriptionReplaceCursor = -1;
    mrCurrentClassList = new StarLinkedList(false);
    mClassInfoList = new StarLinkedList(true);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    //--Active class hitboxes.
    for(int i = cxHitbox_ActiveLo; i < cxHitbox_ActiveHi+1; i ++)
    {
        int cInd = i-cxHitbox_ActiveLo;
        char tNameBuf[64];
        sprintf(tNameBuf, "Active%02i", cInd);
        RegisterHitboxWH(tNameBuf, 237.0f, 359.0f + (26.0f * cInd), 430.0f, 26.0f);
    }

    //--Replacement class hitboxes.
    for(int i = cxHitbox_ReplaceLo; i < cxHitbox_ReplaceHi+1; i ++)
    {
        int cInd = i-cxHitbox_ReplaceLo;
        char tNameBuf[64];
        sprintf(tNameBuf, "Replace%02i", cInd);
        RegisterHitboxWH(tNameBuf, 720, 114.0f + (26.0f * cInd), 430.0f, 26.0f);
    }

    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 1118.0f,  79.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollBd", 1118.0f, 137.0f, 36.0f, 257.0f);
    RegisterHitboxWH("ScrollDn", 1118.0f, 394.0f, 36.0f,  58.0f);

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mScrollSkip;
    nScrollbarPack->mPerPage = cxCarnScrollPage;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxHitbox_ScrollUp;
    nScrollbarPack->mIndexBd = cxHitbox_ScrollBd;
    nScrollbarPack->mIndexDn = cxHitbox_ScrollDn;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;
}
CarnUIClassChange::~CarnUIClassChange()
{
    free(mCharacterName);
    delete mrCurrentClassList;
    delete mClassInfoList;
}
void CarnUIClassChange::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation Classchange UI", &CarnImages, sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI",      &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Help UI",        &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Path to the script that is called whenever this UI is opened, which should populate it with class
//  information such as names and descriptions.
char *CarnUIClassChange::xPopulateScript = NULL;

//--Path to the script that is called to determine which classes a character has equipped.
char *CarnUIClassChange::xCharacterScript = NULL;

//--Path to the script that is called whenever the player decides to change a character's classes.
char *CarnUIClassChange::xChangeScript = NULL;

///================================== ClassChangeInfo Methods =====================================
void ClassChangeInfo::Initialize()
{
    mInternalName = NULL;
    mDisplayName = NULL;
    mLevel = 0;
    mXPTotal = 0;
    mXPTotal = -1;
    mDescription = NULL;
}
void ClassChangeInfo::DeleteThis(void *pPtr)
{
    //--Check.
    if(!pPtr) return;

    //--Cast.
    ClassChangeInfo *rPtr = (ClassChangeInfo *)pPtr;

    //--Deallocate.
    free(rPtr->mInternalName);
    free(rPtr->mDisplayName);
    delete rPtr->mDescription;
    free(rPtr);
}

///===================================== Property Queries =========================================
bool CarnUIClassChange::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_CARNUICLASSCHANGE) return true;
    if(pType == POINTER_TYPE_STARUIPIECE)       return true;
    return (pType == mType);
}

///======================================= Manipulators ===========================================
void CarnUIClassChange::TakeForeground()
{
    ///--[Documentation]
    //--The player has done something to open the class change UI. Fire the population script and
    //  then the zeroth character script. If the paths aren't set, fail.
    if(!xPopulateScript || !xCharacterScript || !xChangeScript)
    {
        fprintf(stderr, "CarnUIClassChange:TakeForeground() - Warning. One of the static paths was invalid. %p %p %p\n", xPopulateScript, xCharacterScript, xChangeScript);
        FlagExit();
        return;
    }

    //--Make sure there is both a viable combat class.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    if(!rAdvCombat)
    {
        fprintf(stderr, "CarnUIClassChange:TakeForeground() - Warning. No viable combat class.\n");
        FlagExit();
        return;
    }

    //--Active party must have at least one character.
    AdvCombatEntity *rZeroCharacter = rAdvCombat->GetActiveMemberI(0);
    if(!rZeroCharacter)
    {
        fprintf(stderr, "CarnUIClassChange:TakeForeground() - Warning. No characters in active party.\n");
        FlagExit();
        return;
    }

    ///--[Clear]
    mCharacterCursor = 0;
    mClassCursor = -1;
    mReplaceCursor = -1;
    mrCurrentClassList->ClearList();
    mClassInfoList->ClearList();

    ///--[Execute]
    //--Populate with basic class info.
    LuaManager *rLuaManager = LuaManager::Fetch();
    rLuaManager->ExecuteLuaFile(xPopulateScript);

    //--Get the 0th character and run the character script with their name.
    rLuaManager->ExecuteLuaFile(xCharacterScript, 1, "S", rZeroCharacter->GetName());

    //--Adjust the scrollbar size to match the inventory size.
    ScrollbarPack *rScrollbarPack = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
    if(rScrollbarPack) rScrollbarPack->mMaxSkip = mClassInfoList->GetListSize() - cxCarnScrollPage;

    //--Refresh character properties.
    RefreshCharacterProperties();
}
void CarnUIClassChange::SetLockFirstSlot(bool pFlag)
{
    mLockFirstSlot = pFlag;
}
void CarnUIClassChange::RegisterClass(const char *pName, const char *pDisplayName)
{
    ///--[Documentation]
    //--Registers a new class with the given name. Does nothing if a duplicate is found.
    if(!pName || !pDisplayName) return;

    //--Duplicate check.
    if(mClassInfoList->GetElementByName(pName) != NULL)
    {
        fprintf(stderr, "CarnUIClassChange:RegisterClass() - Warning. Class info %s already exists, failing.\n", pName);
        return;
    }

    ///--[Execution]
    //--Create.
    ClassChangeInfo *nInfo = (ClassChangeInfo *)starmemoryalloc(sizeof(ClassChangeInfo));
    nInfo->Initialize();
    ResetString(nInfo->mInternalName, pName);
    ResetString(nInfo->mDisplayName, pDisplayName);

    //--Register.
    mClassInfoList->AddElementAsTail(pName, nInfo, &ClassChangeInfo::DeleteThis);
}
void CarnUIClassChange::SetClassDescription(const char *pName, const char *pDescription)
{
    ///--[Documentation]
    //--Sets the description for the given class, done by using Subdivide() to break the description
    //  into a string list.
    if(!pName || !pDescription) return;

    //--Existence check.
    ClassChangeInfo *rClassInfo = (ClassChangeInfo *)mClassInfoList->GetElementByName(pName);
    if(!rClassInfo)
    {
        fprintf(stderr, "CarnUIClassChange:SetClassDescription() - Warning. No class info package %s found.\n", pName);
        return;
    }

    ///--[Execution]
    //--Clear.
    delete rClassInfo->mDescription;
    rClassInfo->mDescription = NULL;

    //--Run routine.
    rClassInfo->mDescription = Subdivide::SubdivideStringFlags(pDescription, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, CarnImages.rFont_Description, 440.0f);
}
void CarnUIClassChange::SetClassLevelProperties(const char *pName, int pLevel, int pXP, int pToNext)
{
    ///--[Documentation]
    //--Sets the level properties for a given class. Needs to be done each time the character changes
    //  as different characters have different class XP values.
    if(!pName) return;

    //--Existence check.
    ClassChangeInfo *rClassInfo = (ClassChangeInfo *)mClassInfoList->GetElementByName(pName);
    if(!rClassInfo)
    {
        fprintf(stderr, "CarnUIClassChange:SetClassLevelProperties() - Warning. No class info package %s found.\n", pName);
        return;
    }

    ///--[Execution]
    //--Set.
    rClassInfo->mLevel = pLevel;
    rClassInfo->mXPTotal = pXP;
    rClassInfo->mXPToNext = pToNext;
}
void CarnUIClassChange::SetCharacterProperties(const char *pName, int pLevel, int pHP, int pHPMax, int pSP, int pSPMax)
{
    ///--[Documentation]
    //--Sets current information for the selected character.
    if(!pName) return;

    ///--[Execution]
    ResetString(mCharacterName, pName);
    mCharacterLevel = pLevel;
    mCharacterHP = pHP;
    mCharacterHPMax = pHPMax;
    mCharacterSP = pSP;
    mCharacterSPMax = pSPMax;
}
void CarnUIClassChange::ClearActiveClasses()
{
    ///--[Documentation]
    //--Clears active classes, should be called when changing characters.

    ///--[Execution]
    mrCurrentClassList->ClearList();
}
void CarnUIClassChange::RegisterActiveClass(const char *pName)
{
    ///--[Documentation]
    //--Sets the named class as the next active class.
    if(!pName) return;

    //--Existence check.
    void *rCheckPtr = mClassInfoList->GetElementByName(pName);
    if(!rCheckPtr)
    {
        fprintf(stderr, "CarnUIClassChange:RegisterActiveClass() - Warning. No class info package %s found.\n", pName);
        return;
    }

    //--Duplicate check.
    void *rDupePtr = mrCurrentClassList->GetElementByName(pName);
    if(rDupePtr)
    {
        fprintf(stderr, "CarnUIClassChange:RegisterActiveClass() - Warning. Class %s is already registered on the active classes.\n", pName);
        return;
    }

    ///--[Execution]
    mrCurrentClassList->AddElementAsTail(pName, rCheckPtr);
}
void CarnUIClassChange::SetClassSlots(int pAmount)
{
    mCharacterClassSlots = pAmount;
}

///======================================= Core Methods ===========================================
void CarnUIClassChange::RefreshMenuHelp()
{
}
void CarnUIClassChange::RecomputeCursorPositions()
{
}
void CarnUIClassChange::RefreshCharacterProperties()
{
    ///--[Documentation]
    //--If the character slot changes, refreshes HP/SP values.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AdvCombatEntity *rCharacter = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(!rCharacter)
    {
        fprintf(stderr, "CarnUIClassChange:RefreshCharacterProperties() - Warning. No character in slot %i in active party.\n", mCharacterCursor);
        FlagExit();
        return;
    }

    ///--[Execution]
    SetCharacterProperties(rCharacter->GetName(), rCharacter->GetLevel(), rCharacter->GetHealth(), rCharacter->GetHealthMax(), rCharacter->GetMagic(), rCharacter->GetStatistic(STATS_MPMAX));
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIClassChange::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ========== Documentation =========== ]
    //--Handles updates when this is the active object.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[ ============== Timers ============== ]
    //--Immediately cap the vis timer.
    mVisibilityTimer = mVisibilityTimerMax;

    ///--[ ============== Update ============== ]
    //--Switch update mode based on the replacement flag.
    if(!mIsReplaceMode)
    {
        UpdateActiveMode();
    }
    else
    {
        UpdateReplaceMode();
    }

    return true;
}
void CarnUIClassChange::UpdateActiveMode()
{
    ///--[ ========== Documentation =========== ]
    //--Handles update when the player is selecting an active class to replace. Right-click will exit the UI.
    ControlManager *rControlManager = ControlManager::Fetch();
    bool tMouseMoved = GetMouseInfo();

    //--Scrollbars update before anything else.
    if(UpdateScrollbars()) return;

    ///--[Mouse Over]
    //--If the mouse moves, recheck hitboxes.
    if(tMouseMoved)
    {
        ///--[Selection Mouse Over]
        //--Refresh hitboxes. If no class hitbox was hit, the cursor resets to -1.
        int tOldClassCursor = mClassCursor;
        mClassCursor = -1;
        RunHitboxCheck();

        //--Iterate across the reply packs.
        int tReply = CheckRepliesWithin(cxHitbox_ActiveLo, cxHitbox_ActiveHi);
        if(tReply != -1) mClassCursor = tReply;

        //--Range check, can't pick an active class that is blank.
        if(mClassCursor >= mCharacterClassSlots) mClassCursor = -1;

        //--Description cursor matches the class cursor.
        mDescriptionClassCursor = mClassCursor;

        //--If the class cursor changed, play a sound effect.
        if(tOldClassCursor != mClassCursor && mClassCursor != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        ///--[Replacement Mouse Over]
        //--This can only affect the description.
        int tOldReplaceCursor = mDescriptionReplaceCursor;
        mDescriptionReplaceCursor = -1;
        tReply = CheckRepliesWithin(cxHitbox_ReplaceLo, cxHitbox_ReplaceHi);
        if(tReply != -1)
        {
            mDescriptionReplaceCursor = tReply - cxHitbox_ReplaceLo;
            if(tOldReplaceCursor != mDescriptionReplaceCursor && mDescriptionReplaceCursor != -1)
            {
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
    }

    ///--[Left Click]
    //--Player left clicks. Switches to replacement mode if the active cursor is not -1.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Check if the player is switching characters.
        if(CommonCharClickUpdate(mCharacterCursor))
        {
            //--Rerun the character population script.
            AdvCombat *rAdvCombat = AdvCombat::Fetch();
            AdvCombatEntity *rSelectedCharacter = rAdvCombat->GetActiveMemberI(mCharacterCursor);
            LuaManager::Fetch()->ExecuteLuaFile(xCharacterScript, 1, "S", rSelectedCharacter->GetName());
            RefreshCharacterProperties();
            return;
        }

        //--Cursor must be on an active class:
        if(mClassCursor == -1) return;

        //--If the first slot is locked and this is the first slot, stop.
        if(mLockFirstSlot && mClassCursor == 0)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Switch to replacement mode.
        mIsReplaceMode = true;
        mReplaceTimer = 0;
        mReplaceCursor = -1;

        //--Stop update.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Exits the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
void CarnUIClassChange::UpdateReplaceMode()
{
    ///--[ ========== Documentation =========== ]
    //--Handles the update when the player is selecting a replacement class. Right-click returns to active mode.
    ControlManager *rControlManager = ControlManager::Fetch();
    bool tMouseMoved = GetMouseInfo();

    //--Scrollbars update before anything else.
    if(UpdateScrollbars()) return;

    ///--[ ============== Timers ============== ]
    //--Run replacement timer.
    mReplaceTimer ++;

    ///--[ ========== Input Handling ========== ]
    ///--[Mouse Over]
    //--If the mouse moves, recheck hitboxes.
    if(tMouseMoved)
    {
        ///--[Replacement]
        //--Refresh hitboxes. If no replace hitbox was hit, the cursor resets to -1.
        int tOldReplaceCursor = mReplaceCursor;
        mReplaceCursor = -1;
        RunHitboxCheck();

        //--Check replies for a replacement.
        int tReply = CheckRepliesWithin(cxHitbox_ReplaceLo, cxHitbox_ReplaceHi);
        if(tReply != -1) mReplaceCursor = tReply - cxHitbox_ReplaceLo;

        //--Range check, can't pick an active class that is blank.
        if(mReplaceCursor >= mClassInfoList->GetListSize()) mReplaceCursor = -1;

        //--Match description.
        mDescriptionReplaceCursor = mReplaceCursor;

        //--If the class cursor changed, play a sound effect.
        if(tOldReplaceCursor != mReplaceCursor)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        ///--[Selection]
        //--Can only affect the description.
        mDescriptionClassCursor = -1;
        tReply = CheckRepliesWithin(cxHitbox_ActiveLo, cxHitbox_ActiveHi);
        if(tReply != -1) mDescriptionClassCursor = tReply;
    }

    ///--[Left Click]
    //--Player left-clicks. If the replacement cursor is over a valid class, issue a replacement order to the lua scripts
    //  and exit replacement mode.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--If the replacement cursor is -1, do nothing.
        if(mReplaceCursor == -1) return;

        //--Issue replacement.
        const char *rReplaceName = mClassInfoList->GetNameOfElementBySlot(mReplaceCursor);
        LuaManager::Fetch()->ExecuteLuaFile(xChangeScript, 3, "N", (float)mCharacterCursor, "N", (float)mClassCursor, "S", rReplaceName);

        //--Exit this mode.
        mIsReplaceMode = false;
        mReplaceTimer = 0;

        //--Rerun the character script to repopulate active classes.
        AdvCombat *rAdvCombat = AdvCombat::Fetch();
        AdvCombatEntity *rSelectedCharacter = rAdvCombat->GetActiveMemberI(mCharacterCursor);
        LuaManager::Fetch()->ExecuteLuaFile(xCharacterScript, 1, "S", rSelectedCharacter->GetName());

        //--Order the calling object to handle the close code.
        SetCloseCode("Change Equipment Vendor");

        //--Update properties in case they changed.
        RefreshCharacterProperties();

        //--Stop update.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Return to active selection mode.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        mIsReplaceMode = false;
        mReplaceTimer = 0;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
void CarnUIClassChange::UpdateBackground()
{
    ///--[ ============== Timers ============== ]
    //--Immediately cap the vis timer.
    mVisibilityTimer = 0;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIClassChange::RenderPieces(float pAlpha)
{
    ///--[ ====== Documenation ====== ]
    //--Carnation does not use by-piece rendering. This is the main rendering call.
    if(mVisibilityTimer < 1) return;

    ///--[ ===== Common Render ====== ]
    ///--[Darkening]
    //--Base UI does not render a backing in this mode. This object handles it.
    float tBackingPct = (float)mVisibilityTimer / (float)mVisibilityTimerMax;
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * tBackingPct));

    ///--[Static Parts]
    //--Common parts that always render.
    RenderCharacterTabs(mCharacterCursor);
    CarnImages.rFrame_Base->Draw();
    CarnImages.rOverlay_MeterFrames->Draw();
    CarnImages.rOverlay_ClassReplaceDivider->Draw();

    ///--[ ===== Character Info ===== ]
    ///--[Retrieve]
    //--Retrieve the character in the active slot. If AdvCombat fails to resolve, something really
    //  bad happened so stop. The entity must be a CarnationCombatEntity to render correctly.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    if(!rAdvCombat) return;
    AdvCombatEntity *rEntity = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(rEntity && rEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
    {
        CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rEntity;
        rCarnationEntity->RenderSwitchableAt(195, 62);
    }

    ///--[Render]
    //--Name, HP, etc.
    CarnImages.rFont_Heading->DrawText(449.0f, 80.0f, 0, 1.0f, mCharacterName);

    //--Render HP value and bars.
    float cHPPct = 0.0f;
    if(mCharacterHPMax > 0) cHPPct = mCharacterHP / (float)mCharacterHPMax;
    CarnImages.rFont_Mainline->DrawTextArgs(473.0f, 191.0f, 0, 1.0f, "%i / %i", mCharacterHP, mCharacterHPMax);
    CarnImages.rOverlay_HPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cHPPct, 1.0f);

    //--Render SP value and bars.
    float cSPPct = 0.0f;
    if(mCharacterSPMax > 0) cSPPct = mCharacterSP / (float)mCharacterSPMax;
    CarnImages.rFont_Mainline->DrawTextArgs(473.0f, 253.0f, 0, 1.0f, "%i / %i", mCharacterSP, mCharacterSPMax);
    CarnImages.rOverlay_SPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cSPPct, 1.0f);

    ///--[ ===== Active Classes ===== ]
    //--Setup.
    float cClassCursorX = 222.0f;
    float cClassNameX   = 240.0f;
    float cClassXPX     = 660.0f;
    float cClassY       = 360.0f;
    float cClassH       =  26.0f;
    float tYCur = cClassY;

    //--Labels.
    if(!mIsReplaceMode) CarnImages.rFont_Heading->DrawText(220.0f, 330.0f, 0, 1.0f, "Select a class to change:");

    //--Render loop.
    for(int i = 0; i < mCharacterClassSlots; i ++)
    {
        //--Get class.
        ClassChangeInfo *rActiveClass = (ClassChangeInfo *)mrCurrentClassList->GetElementBySlot(i);

        //--No class. Print an empty slot.
        if(!rActiveClass)
        {
            CarnImages.rFont_Mainline->DrawTextArgs(cClassNameX, tYCur, 0, 1.0f, "(Open Slot)");
        }
        //--Populated. Print data.
        else
        {
            //--Render name and class level.
            CarnImages.rFont_Mainline->DrawTextArgs(cClassNameX, tYCur, 0, 1.0f, "%s, Level %i", rActiveClass->mDisplayName, rActiveClass->mLevel);

            //--If the to-next value is -1, level is maxed.
            if(rActiveClass->mXPToNext < 0)
            {
                CarnImages.rFont_Mainline->DrawTextArgs(cClassXPX, tYCur, SUGARFONT_RIGHTALIGN_X, 1.0f, "(Maxed!)");
            }
            //--Otherwise, show the integer values.
            else
            {
                CarnImages.rFont_Mainline->DrawTextArgs(cClassXPX, tYCur, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i / %i", rActiveClass->mXPTotal, rActiveClass->mXPToNext);
            }
        }

        //--Cursor. In replacement mode, this toggles on and off. In active mode, it does not.
        if(mClassCursor == i && mReplaceTimer % 4 < 2)
        {
            CarnImages.rOverlay_Cursor->Draw(cClassCursorX, tYCur);
        }

        //--Next.
        tYCur = tYCur + cClassH;
    }

    ///-- [ == Replacement Classes == ]
    //--Setup.
    float cReplaceCursorX =  720.0f;
    float cReplaceNameX   =  740.0f;
    float cReplaceXPX     = 1107.0f;
    float cReplaceY       =  114.0f;
    float cReplaceH       =   26.0f;
    tYCur = cReplaceY;

    //--If there is no scrollbar, move the replacement XP text to the right.
    if(mClassInfoList->GetListSize() < cxCarnScrollPage)
    {
        cReplaceXPX = 1150.0f;
    }

    //--Labels.
    if(mIsReplaceMode) CarnImages.rFont_Heading->DrawText(700.0f, 84.0f, 0, 1.0f, "Select a class to change to:");

    //--Render loop.
    int i = 0;
    int tRenders = 0;
    ClassChangeInfo *rReplaceClass = (ClassChangeInfo *)mClassInfoList->PushIterator();
    while(rReplaceClass)
    {
        //--Skip if below skip.
        if(i < mScrollSkip)
        {
            i ++;
            rReplaceClass = (ClassChangeInfo *)mClassInfoList->AutoIterate();
            continue;
        }

        //--Stop if exceeding max renders.
        if(tRenders >= cxCarnScrollPage)
        {
            mClassInfoList->PopIterator();
            break;
        }

        //--Render name and class level.
        CarnImages.rFont_Mainline->DrawTextArgs(cReplaceNameX, tYCur, 0, 1.0f, "%s, Level %i", rReplaceClass->mDisplayName, rReplaceClass->mLevel);

        //--If the to-next value is -1, level is maxed.
        if(rReplaceClass->mXPToNext < 0)
        {
            CarnImages.rFont_Mainline->DrawTextArgs(cReplaceXPX, tYCur, SUGARFONT_RIGHTALIGN_X, 1.0f, "(Maxed!)");
        }
        //--Otherwise, show the integer values.
        else
        {
            CarnImages.rFont_Mainline->DrawTextArgs(cReplaceXPX, tYCur, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i / %i", rReplaceClass->mXPTotal, rReplaceClass->mXPToNext);
        }

        //--Cursor.
        if(mReplaceCursor == i)
        {
            CarnImages.rOverlay_Cursor->Draw(cReplaceCursorX, tYCur);
        }

        //--Next.
        i ++;
        tRenders ++;
        tYCur = tYCur + cReplaceH;
        rReplaceClass = (ClassChangeInfo *)mClassInfoList->AutoIterate();
    }

    //--Scrollbar.
    if(mClassInfoList->GetListSize() >= cxCarnScrollPage)
    {
        StandardRenderScrollbar(mScrollSkip, cxCarnScrollPage, mClassInfoList->GetListSize(), 137.0f, 257.0f, false, CarnImages.rScrollbar_Scroller, CarnImages.rScrollbar_Static);
    }

    ///--[ ====== Description ======= ]
    //--Resolve the name/description of the highlighed class. It is possible for this to be NULL.
    int tDescLvl = 0;
    const char *rDescName = NULL;
    StarLinkedList *rDescList = NULL;

    //--Selection cursor:
    if(mDescriptionClassCursor != -1)
    {
        //--Check entry. It can be null.
        ClassChangeInfo *rHighlightClass = (ClassChangeInfo *)mrCurrentClassList->GetElementBySlot(mDescriptionClassCursor);
        if(rHighlightClass)
        {
            tDescLvl = rHighlightClass->mLevel;
            rDescName = rHighlightClass->mDisplayName;
            rDescList = rHighlightClass->mDescription;
        }
    }
    //--Replacement cursor.
    else if(mDescriptionReplaceCursor != -1)
    {
        //--Check entry. It can be null.
        ClassChangeInfo *rHighlightClass = (ClassChangeInfo *)mClassInfoList->GetElementBySlot(mDescriptionReplaceCursor);
        if(rHighlightClass)
        {
            tDescLvl  = rHighlightClass->mLevel;
            rDescName = rHighlightClass->mDisplayName;
            rDescList = rHighlightClass->mDescription;
        }
    }

    //--If both resolved, render.
    if(rDescName && rDescList)
    {
        //--Name/stats.
        CarnImages.rFont_Mainline->DrawTextArgs(700.0f, 491.0f, 0, 1.0f, "%s (Lv. %i)", rDescName, tDescLvl);

        //--Description.
        float cHei = CarnImages.rFont_Mainline->GetTextHeight();
        RenderDescription(700.0f, 491.0f + cHei, cHei, rDescList, CarnImages.rFont_Mainline);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void CarnUIClassChange::HookToLuaState(lua_State *pLuaState)
{
    /* [System]
       CarnUIClassChange_SetProperty("Open Event Mode")
       Sets the requested property in the CarnUIClassChange. */
    lua_register(pLuaState, "CarnUIClassChange_SetProperty", &Hook_CarnUIClassChange_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnUIClassChange_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    //CarnUIClassChange_SetProperty("Population Script", sPath)
    //CarnUIClassChange_SetProperty("Character Script", sPath)
    //CarnUIClassChange_SetProperty("Change Script", sPath)

    ///--[Dynamic]
    //--[System]
    //CarnUIClassChange_SetProperty("Lock First Class", bFlag)

    //--[Class Info]
    //CarnUIClassChange_SetProperty("Register Class", sName, sDisplayName)
    //CarnUIClassChange_SetProperty("Class Description", sName, sDescription)
    //CarnUIClassChange_SetProperty("Class Level Properties", sName, iLevel, iXP, iToNext)
    //CarnUIClassChange_SetProperty("Clear Active Classes")
    //CarnUIClassChange_SetProperty("Register Active Class", sName)
    //CarnUIClassChange_SetProperty("Set Class Slots", iCount)

    ///--[ =========== Arg Resolve ============ ]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnUIClassChange_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Statics.
    if(!strcasecmp(rSwitchType, "Population Script") && tArgs >= 2)
    {
        ResetString(CarnUIClassChange::xPopulateScript, lua_tostring(L, 2));
        return 0;
    }
    else if(!strcasecmp(rSwitchType, "Character Script") && tArgs >= 2)
    {
        ResetString(CarnUIClassChange::xCharacterScript, lua_tostring(L, 2));
        return 0;
    }
    else if(!strcasecmp(rSwitchType, "Change Script") && tArgs >= 2)
    {
        ResetString(CarnUIClassChange::xChangeScript, lua_tostring(L, 2));
        return 0;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("CarnUIClassChange_SetProperty", L);

    //--Get the sub-object.
    CarnUIClassChange *rUIObject = dynamic_cast<CarnUIClassChange *>(rMenu->RetrieveUI("ClassChange", POINTER_TYPE_CARNUICLASSCHANGE, NULL, "CarnUIClassChange_SetProperty"));
    if(!rUIObject) return LuaTypeError("CarnUIClassChange_SetProperty", L);

    ///--[System]
    //--If true, the first class slot will not be editable. Used to force characters into certain classes for story reasons.
    if(!strcasecmp(rSwitchType, "Lock First Class") && tArgs >= 2)
    {
        rUIObject->SetLockFirstSlot(lua_toboolean(L, 2));
    }
    ///--[Class Info]
    //--Registers a new class.
    else if(!strcasecmp(rSwitchType, "Register Class") && tArgs >= 3)
    {
        rUIObject->RegisterClass(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets a class description.
    else if(!strcasecmp(rSwitchType, "Class Description") && tArgs >= 3)
    {
        rUIObject->SetClassDescription(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets XP/Next/Level of a class.
    else if(!strcasecmp(rSwitchType, "Class Level Properties") && tArgs >= 5)
    {
        rUIObject->SetClassLevelProperties(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--Clears the active class list.
    else if(!strcasecmp(rSwitchType, "Clear Active Classes") && tArgs >= 1)
    {
        rUIObject->ClearActiveClasses();
    }
    //--Adds a class from the master list to the active class list.
    else if(!strcasecmp(rSwitchType, "Register Active Class") && tArgs >= 2)
    {
        rUIObject->RegisterActiveClass(lua_tostring(L, 2));
    }
    //--Sets how many classes the character can equip at once.
    else if(!strcasecmp(rSwitchType, "Set Class Slots") && tArgs >= 2)
    {
        rUIObject->SetClassSlots(lua_tointeger(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnUIClassChange_SetProperty", rSwitchType, tArgs);
    }

    //--Success.
    return 0;
}
