//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"

//--Definitions
#include "GlDfn.h"
#include "OpenGLMacros.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"

///========================================= Detection ============================================
void TilemapActor::SetEyeSpotting(int pEyeSprites, int pBubbleSprites)
{
    //--Deallocate.
    free(rSpottingFrames);
    free(rAlertingFrames);

    //--Reset.
    mEyeSpotting = false;
    mSpotFramesTotal = 0;
    mAlertFramesTotal = 0;
    rSpottingFrames = NULL;
    rAlertingFrames = NULL;

    //--Range check.
    if(pEyeSprites < 1 || pBubbleSprites < 1) return;

    //--Set.
    mEyeSpotting = true;
    mSpotFramesTotal = pEyeSprites;
    mAlertFramesTotal = pBubbleSprites;

    //--Allocate.
    rSpottingFrames = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mSpotFramesTotal);
    rAlertingFrames = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mAlertFramesTotal);

    //--Clear.
    memset(rSpottingFrames, 0, sizeof(StarBitmap *) * mSpotFramesTotal);
    memset(rAlertingFrames, 0, sizeof(StarBitmap *) * mAlertFramesTotal);
}
void TilemapActor::SetEyeSprite(int pSlot, const char *pPath)
{
    if(pSlot < 0 || pSlot >= mSpotFramesTotal) return;
    rSpottingFrames[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void TilemapActor::SetBubbleSprite(int pSlot, const char *pPath)
{
    if(pSlot < 0 || pSlot >= mAlertFramesTotal) return;
    rAlertingFrames[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void TilemapActor::RenderSpottingSprites()
{
    ///--[ ====== Documentation ===== ]
    //--Enemies that spot the player indicate this via a spotting sprite. This also handles persue cases
    //  to indicate the enemy's chase states.
    if(!mIsEnemy || mIsDying) return;
    if(mIsBagRefill) return;
    if(mIsItemNode) return;

    ///--[ ======== Spotting ======== ]
    //--Enemy is spotting the player!
    if(mAIState == TA_AI_CHASE || mSpottedPlayerTicks > (int)(mSpottedPlayerTicksMax * 0.95f))
    {
        ///--[Standard Behavior]
        if(!mEyeSpotting)
        {
            //--Basic chase:
            if(mEnemyAIType == TA_AI_USE_BASIC_AI)
            {
                if(rSpottedExclamation) rSpottedExclamation->Draw(mSpottingOffsetX, mSpottingOffsetY);
            }
            //--Guard chase:
            else
            {
                //--Chase time percent.
                float cChasePercent = GetChasePercent();

                //--Render a circle indicating how long the AI is searching.
                RenderSpottingCircle(cChasePercent);

                //--Render the exclamation if the timer is maxed out.
                if(cChasePercent >= 0.90f)
                {
                    if(rSpottedExclamation) rSpottedExclamation->Draw(mSpottingOffsetX, mSpottingOffsetY);
                }
                //--Render a question mark.
                else
                {
                    if(rSpottedQuestion) rSpottedQuestion->Draw(mSpottingOffsetX, mSpottingOffsetY);
                }
            }
        }
        ///--[Eye Indicator]
        else
        {
            //--Alert timer is nonzero:
            if(mAlertTimer > 0)
            {
                //--The bubble indicator animates independent of the chase percentage.
                int tFrame = mAlertTimer / TARUI_ALERT_TPF;
                if(tFrame >= mAlertFramesTotal) tFrame = mAlertFramesTotal-1;

                //--Render if the frame exists.
                if(rAlertingFrames[tFrame]) rAlertingFrames[tFrame]->Draw(mSpottingOffsetX, mSpottingOffsetY);
            }
            //--Use spotting frames instead.
            else
            {
                int tFrame = (int)(mSpotTimer / TARUI_SPOT_TPF) % mSpotFramesTotal;
                if(rSpottingFrames[tFrame]) rSpottingFrames[tFrame]->Draw(mSpottingOffsetX, mSpottingOffsetY);
            }
        }
    }
    ///--[ === Leashing/Searching === ]
    //--Enemy lost track of the player.
    else if(mAIState == TA_AI_LEASH || mSpottedPlayerTicks > 0)
    {
        ///--[Standard Behavior]
        if(!mEyeSpotting)
        {
            if(rSpottedQuestion) rSpottedQuestion->Draw(mSpottingOffsetX, mSpottingOffsetY);
        }
        ///--[Eye Indicator]
        else
        {
            //--Render the searching eye.
            int tFrame = (int)(mSpotTimer / TARUI_SPOT_TPF) % mSpotFramesTotal;

            //--Render if the frame exists.
            if(rSpottingFrames[tFrame]) rSpottingFrames[tFrame]->Draw(mSpottingOffsetX, mSpottingOffsetY);
        }
    }

    ///--[ ===== Time Indicator ===== ]
    //--Render a circle around the spotting icon to indicate how much time they have. Does not render in eye-spot mode.
    if(mSpottedPlayerTicks > 0 && mAIState != TA_AI_CHASE && !mEyeSpotting)
    {
        RenderSpottingCircle((float)mSpottedPlayerTicks / (float)mSpottedPlayerTicksMax);
    }
}
void TilemapActor::RenderSpottingCircle(float pPercent)
{
    ///--[Documentation and Setup]
    //--Renders a circle over the entity. The circle changes color as it renders. This is used to indicate
    //  alertness states.
    if(pPercent <= 0.0f) return;

    //--Fast-access pointers.
    int cShaderHandle = 0;
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    DisplayManager *rDisplayManager = DisplayManager::Fetch();

    //--Reposition.
    glTranslatef(mSpottingCircleX, mSpottingCircleY, 0.0f);

    ///--[Color]
    //--Color is based on percentage.
    if(pPercent < 0.50f)
    {
        glColor3f(pPercent * 2.0f, pPercent * 2.0f, 0.0f);
        if(rActiveLevel->IsLightingActive())
        {
            cShaderHandle = rDisplayManager->mLastProgramHandle;
            ShaderUniform1i(cShaderHandle, 1, "uUseMixer");
            uint32_t tColorHandle = sglGetUniformLocation(cShaderHandle, "uMixer");
            sglUniform4f(tColorHandle, pPercent * 2.0f, pPercent * 2.0f, 0.0f, 1.0f);
        }
    }
    //--Danger!
    else
    {
        glColor3f(1.0f, 1.0f - (pPercent), 0.0f);
        if(rActiveLevel->IsLightingActive())
        {
            cShaderHandle = rDisplayManager->mLastProgramHandle;
            ShaderUniform1i(cShaderHandle, 1, "uUseMixer");
            uint32_t tColorHandle = sglGetUniformLocation(cShaderHandle, "uMixer");
            sglUniform4f(tColorHandle, 1.0f, 1.0f - (pPercent), 0.0f, 1.0f);
        }
    }

    ///--[Render]
    //--Setup.
    StarBitmap *rWhitePixel = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/System/System/WhitePixel");
    if(rWhitePixel)
    {
        //--Bind.
        rWhitePixel->Bind();

        //--Render.
        glLineWidth(mSpottingCircleT);
        glBegin(GL_LINE_STRIP);
            for(int i = 0; i < (int)(361.0f * pPercent); i += 5)
            {
                float cRads = (i-90.0f) * TORADIAN;
                glVertex2f(cosf(cRads)  * mSpottingCircleR, sinf(cRads)  * mSpottingCircleR);
            }
        glEnd();

        //--Lighting clean.
        if(rActiveLevel->IsLightingActive())
        {
            ShaderUniform1i(cShaderHandle, 0, "uUseMixer");
        }
    }

    ///--[Clean]
    glLineWidth(1.0f);
    StarlightColor::ClearMixer();
    glTranslatef(-mSpottingCircleX, -mSpottingCircleY, 0.0f);
}
