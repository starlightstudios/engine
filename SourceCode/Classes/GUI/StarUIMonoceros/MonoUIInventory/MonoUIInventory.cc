//--Base
#include "MonoUIInventory.h"

//--Classes
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
MonoUIInventory::MonoUIInventory()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ===== AdvUIInventory ===== ]
    ///--[Constants]
    cItemsPerPage = 9;

    ///--[ ==== MonoUIInventory ===== ]
    ///--[System]
    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Add the AdvImages structure to the verify list.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIInventory::~MonoUIInventory()
{
}
void MonoUIInventory::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Inventory UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Inventory UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",                &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIInventory_Strings *MonoUIInventory::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIInventory_Strings::MonoUIInventory_Strings()
{
    //--Local name: "MonoUIInventory"
    MonoUIInventory::xrStringData = this;

    //--Strings
    str.mTextInventory         = InitializeString("MonoUIInventory_mInventory");                             //"Inventory"
    str.mTextHelpMenu          = InitializeString("MonoUIInventory_mHelpMenu");                              //"Help Menu"
    str.mTextName              = InitializeString("MonoUIInventory_mName");                                  //"Name"
    str.mTextType              = InitializeString("MonoUIInventory_mType");                                  //"Type"
    str.mTextValue             = InitializeString("MonoUIInventory_mValue");                                 //"Value"
    str.mTextGems              = InitializeString("MonoUIInventory_mGems");                                  //"Gems"
    str.mTextCannotDeconstruct = InitializeString("MonoUIInventory_mCannotDeconstruct");                     //"Cannot Deconstruct"
    str.mTextDeconNoEquip      = InitializeString("MonoUIInventory_mEquippedItemsCannotBeDeconstructed.");   //"Equipped items cannot be deconstructed."
    str.mTextDeconNoResult     = InitializeString("MonoUIInventory_mThisItemDoesNotBreakIntoAnythingElse."); //"This item does not break into anything else."
    str.mTextDeconConfirm      = InitializeString("MonoUIInventory_mDeconstruct[sItemName]?");               //"Deconstruct [sItemName]?"

    //--Help Sequence
    char tBuffer[100];
    for(int i = 0; i < 17; i ++)
    {
        sprintf(tBuffer, "MonoUIInventory_mHelpText%02i", i);
        str.mTextHelp[i] = InitializeString(tBuffer);
    }

    //--Control Sequence
    str.mTextControlShowHelp    = InitializeString("MonoUIInventory_mControlShowHelp");    //"[IMG0] Show Help"
    str.mTextControlSortName    = InitializeString("MonoUIInventory_mControlSortName");    //"[IMG0] Change Sort (Current: By Name)"
    str.mTextControlSortValue   = InitializeString("MonoUIInventory_mControlSortValue");   //"[IMG0] Change Sort (Current: By Value)"
    str.mTextControlSortType    = InitializeString("MonoUIInventory_mControlSortType");    //"[IMG0] Change Sort (Current: By Type)"
    str.mTextControlHoldScroll  = InitializeString("MonoUIInventory_mControlHoldScroll");  //"[IMG0] (Hold) Scroll x10"
    str.mTextControlDeconstruct = InitializeString("MonoUIInventory_mControlDeconstruct"); //"[IMG0] Deconstruct item"
    str.mTextControlConfirm     = InitializeString("MonoUIInventory_mControlConfirm");     //"[IMG0] Confirm"
    str.mTextControlCancel      = InitializeString("MonoUIInventory_mControlCancel");      //"[IMG0] Cancel"
}
MonoUIInventory_Strings::~MonoUIInventory_Strings()
{
    for(int i = 0; i < MONOUIINVENTORY_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIInventory_Strings::GetSize()
{
    return MONOUIINVENTORY_STRINGS_TOTAL;
}
void MonoUIInventory_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIINVENTORY_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUIInventory::TakeForeground()
{
    ///--[Call Base Version]
    //--Base version handles most of the same variables.
    AdvUIInventory::TakeForeground();

    ///--[Override Strings]
    //--"Show Help" string
    mShowHelpString->SetString(xrStringData->str.mTextControlShowHelp);
    mShowHelpString->CrossreferenceImages();

    //--"Change Sorting" string
    int tSortFlag = AdventureInventory::Fetch()->GetInternalSortFlag();
    if(tSortFlag == AINV_SORT_CRITERIA_NAME)
        mChangeSortString->SetString(xrStringData->str.mTextControlSortName);
    else if(tSortFlag == AINV_SORT_CRITERIA_VALUE)
        mChangeSortString->SetString(xrStringData->str.mTextControlSortValue);
    else if(tSortFlag == AINV_SORT_CRITERIA_TYPE)
        mChangeSortString->SetString(xrStringData->str.mTextControlSortType);
    mChangeSortString->CrossreferenceImages();

    //--"Hold Scroll x10" string
    mScrollFastString->SetString(xrStringData->str.mTextControlHoldScroll);
    mScrollFastString->CrossreferenceImages();

    //--"Deconstruct" string
    mDeconstructString->SetString(xrStringData->str.mTextControlDeconstruct);
    mDeconstructString->CrossreferenceImages();

    //--"Confirm Deconstruct" string
    mConfirmString->SetString(xrStringData->str.mTextControlConfirm);
    mConfirmString->CrossreferenceImages();

    //--"Cancel Deconstruct" string
    mCancelString->SetString(xrStringData->str.mTextControlCancel);
    mCancelString->CrossreferenceImages();
}

///======================================= Core Methods ===========================================
void MonoUIInventory::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Set Lines]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Common lines.
    SetHelpString(i, xrStringData->str.mTextHelp[0], 1, "F2");
    SetHelpString(i, xrStringData->str.mTextHelp[1]);
    SetHelpString(i, xrStringData->str.mTextHelp[2]);
    SetHelpString(i, xrStringData->str.mTextHelp[3]);
    SetHelpString(i, xrStringData->str.mTextHelp[4]);
    SetHelpString(i, xrStringData->str.mTextHelp[5], 2, "F1", "Cancel");
    SetHelpString(i, xrStringData->str.mTextHelp[6]);
    SetHelpString(i, xrStringData->str.mTextHelp[7]);
    SetHelpString(i, xrStringData->str.mTextHelp[8]);
    SetHelpString(i, xrStringData->str.mTextHelp[9]);
    SetHelpString(i, xrStringData->str.mTextHelp[10]);
    SetHelpString(i, xrStringData->str.mTextHelp[11]);
    SetHelpString(i, xrStringData->str.mTextHelp[12]);
    SetHelpString(i, xrStringData->str.mTextHelp[13]);
    SetHelpString(i, xrStringData->str.mTextHelp[14]);
    SetHelpString(i, xrStringData->str.mTextHelp[15]);

    //--Statistics Icon Legend.
    mHelpMenuStrings[i]->SetString(xrStringData->str.mTextHelp[16]);
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Health"));
    mHelpMenuStrings[i]->SetImageP(1, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Attack"));
    mHelpMenuStrings[i]->SetImageP(2, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Accuracy"));
    mHelpMenuStrings[i]->SetImageP(3, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Evade"));
    mHelpMenuStrings[i]->SetImageP(4, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Initiative"));
    mHelpMenuStrings[i]->SetImageP(5, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Protection"));
    i ++;

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void MonoUIInventory::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Determines where the cursor should be given the inventory cursor and scrollbar offsets.
    float cEntryY = 150.0f;
    float cEntryH =  37.0f;
    float cBuf = 5.0f;

    //--Positions.
    float cLft = 67.0f - cBuf;
    float cTop = cEntryY + (cEntryH * (mItemCursor - mItemSkip)) - cBuf + 2.0f;
    float cRgt = cLft + 100.0f + cBuf + 11.0f;
    float cBot = cTop + cEntryH + cBuf + 1.0f;

    //--Get entry.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetExtendedItemList();
    AdventureItem *rEntry = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);
    if(rEntry)
    {
        cRgt = cLft + 52.0f + AdvImages.rFont_Mainline->GetTextWidth(rEntry->GetName()) + cBuf;
    }

    //--Set.
    mHighlightPos.MoveTo(0.0f, cTop, cxCursorTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxCursorTicks);
}
void MonoUIInventory::SetSortingString(int pFlag)
{
    ///--[Documentation]
    //--The mChangeSortString changes its text based on the current sorting type.
    if(pFlag == AINV_SORT_CRITERIA_NAME)
    {
        mChangeSortString->AutoSetControlString(xrStringData->str.mTextControlSortName,  1, "F2");
    }
    else if(pFlag == AINV_SORT_CRITERIA_VALUE)
    {
        mChangeSortString->AutoSetControlString(xrStringData->str.mTextControlSortValue, 1, "F2");
    }
    else if(pFlag == AINV_SORT_CRITERIA_TYPE)
    {
        mChangeSortString->AutoSetControlString(xrStringData->str.mTextControlSortType,  1, "F2");
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
