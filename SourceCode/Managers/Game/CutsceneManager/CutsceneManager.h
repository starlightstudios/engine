///====================================== CutsceneManager =========================================
//--Used exclusively for Adventure Mode, overrides control of the program while a cutscene is playing. Entities
//  will still update as normal but the player's controls are locked out and most entities will respond to
//  instructions instead. This Manager holds the instructions, it's up to the other entities (usually AdventureLevels)
//  to act them out and determine break conditions.
//--Note that while a CutsceneManager exists explicitly and can be safely Fetch()'d, this does not preclude the use
//  of additional CutsceneManagers to handle threaded events. NPCs can be made to path around using this technique,
//  but the engine provides no inherent support for it.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLinkedList.h"
#include "Interfaces/IResettable.h"

///===================================== Local Structures =========================================
typedef struct ParallelScriptPack
{
    //--Base.
    bool mPrintExecTimeOnReset;
    int mExecTime;
    char *mRefresherScript;

    //--Scattering.
    bool mHasPendingScatter;
    int mScatterLo;
    int mScatterHi;

    //--Events.
    StarLinkedList *mEventList;

    //--Methods
    void Initialize()
    {
        //--Base.
        mPrintExecTimeOnReset = false;
        mExecTime = 0;
        mRefresherScript = NULL;

        //--Scattering.
        mHasPendingScatter = false;
        mScatterLo = 0;
        mScatterHi = 1;

        //--Events.
        mEventList = new StarLinkedList(true);
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        ParallelScriptPack *rStruct = (ParallelScriptPack *)pPtr;
        free(rStruct->mRefresherScript);
        delete rStruct->mEventList;
        free(rStruct);
    }
}ParallelScriptPack;

///===================================== Local Definitions ========================================
//--Cutscene Acceleration
#define CM_DEFAULT_CUTSCENE_CYCLES 1
#define CM_ACCELERATED_CUTSCENE_CYCLES 10

//--Diagnostics
#define CM_DIAGNOSTIC_NONE             0x00
#define CM_DIAGNOSTIC_ATLEASTFUNCTIONS 0x01 //Only used by Lua
#define CM_DIAGNOSTIC_INSTRUCTIONS     0x02 //Only used by Lua
#define CM_DIAGNOSTIC_PATHS            0x04 //Only used by Lua
#define CM_DIAGNOSTIC_VISUAL           0x08

///========================================== Classes =============================================
class CutsceneManager : public IResettable
{
    private:
    //--System
    //--Storage
    StarLinkedList *mEventList; //RootEvent *, master

    //--Bucketed Addition
    bool mIsBucketedAddition;
    bool mBypassBucket;
    StarLinkedList *mBucketedList;

    //--Drop List.
    StarLinkedList *mDropList;

    //--Parallel Simultaneous Scripts
    bool mClearAllParallelLists;
    bool mSuspendActiveScript;
    bool mRemoveActiveScript;
    ParallelScriptPack *rActivePackage;
    StarLinkedList *mParallelScriptList;       //ParallelScriptPack *, master
    StarLinkedList *mSuspendedParallelScripts; //ParallelScriptPack *, master
    StarLinkedList *mParallelClearList;        //ParallelScriptPack *, master

    //--Parallel Bucketing
    StarLinkedList *mParallelAppendList; //ParallelScriptPack *, master

    protected:

    public:
    //--System
    CutsceneManager();
    ~CutsceneManager();

    //--Public Variables
    static bool xFlag;
    static int xUpdatesPerEntityThisTick;
    static int xTimerUpdatesThisTick;
    static bool xIsScatteringScripts;
    static int xDiagnosticLevel;

    //--Property Queries
    bool HasAnyEvents();
    const char *GetActiveParallelPackName();

    //--Manipulators
    void CreateParallelScript(const char *pName, const char *pPath, bool pReportTicksOnRefresh);
    void SuspendParallelScript(const char *pName);
    void SuspendActiveScript();
    void UnsuspendParallelScript(const char *pName);
    void RemoveParallelScript(const char *pName);
    void RemoveActiveScript();
    void ClearParallelScripts();
    void ApplyScatterToAppendingPackage(const char *pName, int pMinTicks, int pMaxTicks);

    //--Core Methods
    void RegisterEvent(RootEvent *pEvent);
    RootEvent *CreateBlocker();
    void DropAllEvents();
    virtual void Reset(const char *pResetType);
    bool IsAnythingStoppingEvents(); //Implemented in local project.

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();
    void UpdateParallelEvents();
    void ScatterParallelPackage(const char *pName, int pMinTicks, int pMaxTicks);
    void UpdateParallelPackage(const char *pName, ParallelScriptPack *pPack);
    void DropListAtEndOfTick();

    //--File I/O
    //--Drawing
    void RenderDiagnostics();

    //--Pointer Routing
    //--Static Functions
    static CutsceneManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Cutscene_CreateEvent(lua_State *L);
int Hook_Cutscene_HandleParallel(lua_State *L);
int Hook_Cutscene_GetProperty(lua_State *L);
int Hook_Cutscene_SetProperty(lua_State *L);
