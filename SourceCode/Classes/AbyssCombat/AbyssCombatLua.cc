
//--Base
#include "AbyssCombat.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void AbyssCombat::HookToLuaState(lua_State *pLuaState)
{
    /* AbyCombat_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AbyCombat_GetProperty", &Hook_AbyCombat_GetProperty);

    /* AbyCombat_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AbyCombat_SetProperty", &Hook_AbyCombat_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AbyCombat_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AbyCombat_GetProperty("Always Play Callouts") (1 Boolean) (Static)

    //--[System]
    //AbyCombat_GetProperty("Is Party Slot Disabled", iSlot) (1 Boolean)
    //AbyCombat_GetProperty("Is Party ID Disabled", iUniqueID) (1 Boolean)
    //AbyCombat_GetProperty("Global Ability Exists", sAbilityName) (1 Boolean)
    //AbyCombat_GetProperty("Path Of Ability", sAbilityName) (1 String)

    //--[Commands]
    //AbyCombat_GetProperty("Command Exists", sCommandName) (1 Boolean)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AbyCombat_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Debug flag to force callouts to always play.
    if(!strcasecmp(rSwitchType, "Always Play Callouts"))
    {
        lua_pushboolean(L, AbyssCombat::xAlwaysPlayCallouts);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    if(!rAdventureCombat || !rAdventureCombat->IsOfType(POINTER_TYPE_ABYSSCOMBAT)) return LuaTypeError("AbyCombat_GetProperty", L);

    //--Cast.
    AbyssCombat *rAbyssCombat = (AbyssCombat *)rAdventureCombat;

    ///--[System]
    //--True if the given party slot is disabled, false otherwise.
    if(!strcasecmp(rSwitchType, "Is Party Slot Disabled") && tArgs == 2)
    {
        lua_pushboolean(L, rAbyssCombat->IsPartyDisabled(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--True if the ID is disabled.
    else if(!strcasecmp(rSwitchType, "Is Party ID Disabled") && tArgs == 2)
    {
        lua_pushboolean(L, rAbyssCombat->IsPartyDisabledID(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--True if the named ability exists on the global list.
    else if(!strcasecmp(rSwitchType, "Global Ability Exists") && tArgs == 2)
    {
        lua_pushboolean(L, rAbyssCombat->DoesGlobalAbilityExist(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns the hard drive path of the named ability, or "NULL" on error.
    else if(!strcasecmp(rSwitchType, "Path Of Ability") && tArgs == 2)
    {
        lua_pushstring(L, rAbyssCombat->GetPathOfGlobalAbility(lua_tostring(L, 2)));
        tReturns = 1;
    }
    ///--[Commands]
    //--Returns true if the named command exists, false otherwise.
    else if(!strcasecmp(rSwitchType, "Command Exists") && tArgs == 2)
    {
        lua_pushboolean(L, rAbyssCombat->DoesCommandExist(lua_tostring(L, 2)));
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AbyCombat_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AbyCombat_SetProperty("Set Victory Dialogue Path", sPath) (Static)
    //AbyCombat_SetProperty("Ignore Disabling For Targets", bFlag) (Static)

    //--[System]
    //AbyCombat_SetProperty("Disallow Emergency Escape", bFlag)
    //AbyCombat_SetProperty("Set Party Disabled", iSlot, bFlag)
    //AbyCombat_SetProperty("Set Party Disabled ID", iUniqueID, bFlag)
    //AbyCombat_SetProperty("Flag Member Defeated")
    //AbyCombat_SetProperty("Take Ability From Entity", sAbilityName)

    //--[Settings]
    //AbyCombat_SetProperty("Combat Text Scale", fScale)

    //--[Command List]
    //AbyCombat_SetProperty("Register Command", sInternalName)
    //AbyCombat_SetProperty("Push Command", sInternalName)
    //AbyCombat_SetProperty("Pop Command")
    //AbyCombat_SetProperty("Clear Command List")
    //AbyCombat_SetProperty("Set Command Display Name", sDisplayName)
    //AbyCombat_SetProperty("Set Command Display String", sDisplayString)
    //AbyCombat_SetProperty("Set Command Locked", bIsLocked)
    //AbyCombat_SetProperty("Set Command Glowing", bIsGlowing)
    //AbyCombat_SetProperty("Set Command Ability", sAbilityName)
    //AbyCombat_SetProperty("Set Command Quantity", iQuantity)
    //AbyCombat_SetProperty("Set Command Inventory Slot", iSlot)
    //AbyCombat_SetProperty("Add Command Icon", sPath)
    //AbyCombat_SetProperty("Add Suffix Icon", sPath)
    //AbyCombat_SetProperty("Set Command Is Charm", bIsCharm)
    //AbyCombat_SetProperty("Push Command Ability") (Pushes Activity Stack)

    //--[Callouts]
    //AbyCombat_SetProperty("Register Callout", sInternalName)
    //AbyCombat_SetProperty("Set Callout ID", iID)
    //AbyCombat_SetProperty("Allocate Callout Lines", iLinesTotal)
    //AbyCombat_SetProperty("Set Callout Line", iIndex, sString)
    //AbyCombat_SetProperty("Position Callout", iX, iY)
    //AbyCombat_SetProperty("Flip Callout X")
    //AbyCombat_SetProperty("Flip Callout Y")
    //AbyCombat_SetProperty("Set Callout Timer", iTimer)

    //--[Tutorial]
    //AbyCombat_SetProperty("Activate Tutorial", sTutorialImgPath)
    //AbyCombat_SetProperty("Register Tutorial String", fX, fY, sString)

    //--[Victory]
    //AbyCombat_SetProperty("Set Victory Speaker", sName)
    //AbyCombat_SetProperty("Set Victory Line", iLine, sString)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AbyCombat_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Path used to reset/populate victory dialogue.
    if(!strcasecmp(rSwitchType, "Set Victory Dialogue Path") && tArgs == 2)
    {
        ResetString(AbyssCombat::xVictoryDialoguePath, lua_tostring(L, 2));
        return 0;
    }
    //--If true, targeting will not ignore disabled allies.
    else if(!strcasecmp(rSwitchType, "Ignore Disabling For Targets") && tArgs == 2)
    {
        AbyssCombat::xIgnoreDisablesForTargets = lua_toboolean(L, 2);
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    if(!rAdventureCombat || !rAdventureCombat->IsOfType(POINTER_TYPE_ABYSSCOMBAT)) return LuaTypeError("AbyCombat_SetProperty", L);

    //--Cast.
    AbyssCombat *rAbyssCombat = (AbyssCombat *)rAdventureCombat;

    ///--[System]
    //--If true, the player cannot use the emergency escape button to exit a locked battle. Used for the tutorial.
    if(!strcasecmp(rSwitchType, "Disallow Emergency Escape") && tArgs == 2)
    {
        rAbyssCombat->SetDisallowEmergencyEscape(lua_toboolean(L, 2));
    }
    //--Sets if a party member is "Disabled". This makes them not appear on target lists or render in combat.
    else if(!strcasecmp(rSwitchType, "Set Party Disabled") && tArgs == 3)
    {
        rAbyssCombat->SetPartyDisabledFlag(lua_tointeger(L, 2), lua_toboolean(L, 3));
    }
    //--Specifies disabled or not by ID instead of slot.
    else if(!strcasecmp(rSwitchType, "Set Party Disabled ID") && tArgs == 3)
    {
        rAbyssCombat->SetPartyDisabledFlagByID(lua_tointeger(L, 2), lua_toboolean(L, 3));
    }
    //--Used during defeat response checking. Indicates the responding member is defeated or otherwise unable to act.
    else if(!strcasecmp(rSwitchType, "Flag Member Defeated") && tArgs == 1)
    {
        rAbyssCombat->mReplyToDefeated = true;
    }
    //--Liberates the named ability from the active AdvCombatEntity and registers it to the combat global list.
    else if(!strcasecmp(rSwitchType, "Take Ability From Entity") && tArgs == 2)
    {
        rAbyssCombat->TakeAbilityFromEntity(lua_tostring(L, 2));
    }
    ///--[Settings]
    //--Scale of in-combat text like "Guarding!" and "Poisoned!".
    else if(!strcasecmp(rSwitchType, "Combat Text Scale") && tArgs >= 2)
    {
        rAbyssCombat->SetCombatTextScale(lua_tonumber(L, 2));
    }
    ///--[Command List]
    //--Registers and pushes a new command list entry.
    else if(!strcasecmp(rSwitchType, "Register Command") && tArgs == 2)
    {
        rAbyssCombat->RegisterCommandListEntry(lua_tostring(L, 2));
    }
    //--Pushes the named command list entry from the current active entry, or the base list.
    else if(!strcasecmp(rSwitchType, "Push Command") && tArgs == 2)
    {
        rAbyssCombat->PushCommandListEntry(lua_tostring(L, 2));
    }
    //--Pops the command stack.
    else if(!strcasecmp(rSwitchType, "Pop Command") && tArgs == 1)
    {
        rAbyssCombat->PopCommandList();
    }
    //--Clears the command stack entirely.
    else if(!strcasecmp(rSwitchType, "Clear Command List") && tArgs == 1)
    {
        rAbyssCombat->ClearCommandList();
    }
    //--Sets the display name of the active command entry.
    else if(!strcasecmp(rSwitchType, "Set Command Display Name") && tArgs == 2)
    {
        rAbyssCombat->SetCommandDisplayName(lua_tostring(L, 2));
    }
    //--Sets the display string of the active command entry.
    else if(!strcasecmp(rSwitchType, "Set Command Display String") && tArgs == 2)
    {
        rAbyssCombat->SetCommandDisplayString(lua_tostring(L, 2));
    }
    //--Command is always unusable.
    else if(!strcasecmp(rSwitchType, "Set Command Locked") && tArgs == 2)
    {
        rAbyssCombat->SetCommandManualLock(lua_toboolean(L, 2));
    }
    //--The command will glow to draw attention to it.
    else if(!strcasecmp(rSwitchType, "Set Command Glowing") && tArgs == 2)
    {
        rAbyssCombat->SetCommandGlowing(lua_toboolean(L, 2));
    }
    //--Sets the associated ability of the active command entry.
    else if(!strcasecmp(rSwitchType, "Set Command Ability") && tArgs == 2)
    {
        rAbyssCombat->SetCommandAssociatedAbility(lua_tostring(L, 2));
    }
    //--Sets the quantity of the active command entry.
    else if(!strcasecmp(rSwitchType, "Set Command Quantity") && tArgs == 2)
    {
        rAbyssCombat->SetCommandQuantity(lua_tointeger(L, 2));
    }
    //--Sets the inventory slot of the entry, -1 for no slot. What the slot means varies based on the game.
    else if(!strcasecmp(rSwitchType, "Set Command Inventory Slot") && tArgs == 2)
    {
        rAbyssCombat->SetInventorySlot(lua_tointeger(L, 2));
    }
    //--Icons that appear next to the entry.
    else if(!strcasecmp(rSwitchType, "Add Command Icon") && tArgs == 2)
    {
        rAbyssCombat->AddCommandIcon(lua_tostring(L, 2));
    }
    //--Icon that appears after the entry.
    else if(!strcasecmp(rSwitchType, "Add Suffix Icon") && tArgs == 2)
    {
        rAbyssCombat->AddSuffixIcon(lua_tostring(L, 2));
    }
    //--If true, ability renders in a different color.
    else if(!strcasecmp(rSwitchType, "Set Command Is Charm") && tArgs == 2)
    {
        rAbyssCombat->SetCommandIsCharm(lua_toboolean(L, 2));
    }
    //--Pushes the ability associated with this command atop the activity stack.
    else if(!strcasecmp(rSwitchType, "Push Command Ability") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity();
        CommandListEntry *rActiveEntry = rAbyssCombat->GetActiveCommand();
        if(rActiveEntry)
        {
            DataLibrary::Fetch()->rActiveObject = rActiveEntry->rAssociatedAbility;
        }
    }
    ///--[Callouts]
    //--Creates and stores a new callout.
    else if(!strcasecmp(rSwitchType, "Register Callout") && tArgs == 2)
    {
        rAbyssCombat->RegisterCallout(lua_tostring(L, 2));
    }
    //--Sets the ID used by the callout for positioning.
    else if(!strcasecmp(rSwitchType, "Set Callout ID") && tArgs == 2)
    {
        rAbyssCombat->SetCalloutUserID(lua_tointeger(L, 2));
    }
    //--Sets how many vertical lines the callout has.
    else if(!strcasecmp(rSwitchType, "Allocate Callout Lines") && tArgs == 2)
    {
        rAbyssCombat->AllocateCalloutLines(lua_tointeger(L, 2));
    }
    //--Sets what text appears on the callout.
    else if(!strcasecmp(rSwitchType, "Set Callout Line") && tArgs == 3)
    {
        rAbyssCombat->SetCalloutLine(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Resolve the callout's position and sizes.
    else if(!strcasecmp(rSwitchType, "Position Callout") && tArgs == 3)
    {
        rAbyssCombat->PositionCallout(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Callout will appear with the speech line facing to the right.
    else if(!strcasecmp(rSwitchType, "Flip Callout X") && tArgs == 1)
    {
        rAbyssCombat->FlipCalloutX();
    }
    //--Callout will appear with the speech line up instead of down.
    else if(!strcasecmp(rSwitchType, "Flip Callout Y") && tArgs == 1)
    {
        rAbyssCombat->FlipCalloutY();
    }
    //--Sets the callout timer. Callouts are invisible if the timer is negative.
    else if(!strcasecmp(rSwitchType, "Set Callout Timer") && tArgs == 2)
    {
        rAbyssCombat->SetCalloutTimer(lua_tointeger(L, 2));
    }
    ///--[Tutorial]
    //--Causes a tutorial overlay to appear.
    else if(!strcasecmp(rSwitchType, "Activate Tutorial") && tArgs == 2)
    {
        rAbyssCombat->SetTutorialOverlay(lua_tostring(L, 2));
    }
    //--Registers a string to appear during the turorial. Allows translation.
    else if(!strcasecmp(rSwitchType, "Register Tutorial String") && tArgs == 4)
    {
        rAbyssCombat->RegisterTutorialString(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tostring(L, 4));
    }
    ///--[Victory]
    //--Uses an entity name to resolve a portrait and offsets for the victory screen.
    else if(!strcasecmp(rSwitchType, "Set Victory Speaker") && tArgs == 2)
    {
        rAbyssCombat->SetVictorySpeaker(lua_tostring(L, 2));
    }
    //--Sets the specified string on the victory screen.
    else if(!strcasecmp(rSwitchType, "Set Victory Line") && tArgs == 3)
    {
        rAbyssCombat->SetVictoryLine(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AbyCombat_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
