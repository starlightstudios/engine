--[ ================================= String Tyrant Demo Build ================================== ]
--Operating System: OSX
--Library Type: SDL
local sBuildName = "String Tyrant Demo Osx"

--Check existence.
if(BP_Exists(sBuildName) == true) then return end

--Create.
BP_Create(sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../builds/String Tyrant Demo Osx/")

--[ =========================================== Common ========================================== ]
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

--[Basics]
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

--[Executables]
--Executable files.
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDLOSX", "StringTyrantDemoSDLOSX")

--Dylib files.
BP_SetProperty("Register Command", "Copy Folder", "osxbin", "osxbin")

--[Bootstraps and Counters]
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

--[ ===================================== Base Instructions ===================================== ]
--[Text Files]
--Relevant changelogs.
BP_SetProperty("Register Command", "Copy File", "../dollmanor/Changelog (String Tyrant).txt", "Changelog.txt")

--Credits files.
BP_SetProperty("Register Command", "Copy File", "../dollmanor/Credits (String Tyrant).txt", "Credits.txt")

--Readme.
BP_SetProperty("Register Command", "Copy File", "../dollmanor/Readme.txt", "Readme.txt")

--Engine and String Tyrant configurations.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_StringTyrant.lua", "Config_StringTyrant.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",       "Config_Engine.lua")

--[Manifest Files]
--Itch.io manifest file, allows the itch.io app to be used.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/String Tyrant Demo Osx.toml", ".itch.toml")

--[ ===================================== Engine Subfolders ===================================== ]
--[Data Directory]
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

--Delete the fonts we don't need.
-- String Tyrant uses all fonts

--[Saves Directory]
--Copy the controls files.
BP_SetProperty("Register Command", "Create Directory", "Saves")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

--[Shaders Directory]
--Copy the OSX shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/OSX", "Shaders/OSX")

--[ ====================================== Game Subfolders ====================================== ]
--[Adventure Directories]
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../dollmanor/Games/DollManorDemo", "Games/DollManorDemo")


--[ ========================================= Clean Up ========================================== ]
--Pop the active object.
DL_PopActiveObject()
