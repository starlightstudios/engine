//--Base
#include "AdvUIInventory.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///====================================== Segment Drawing =========================================
void AdvUIInventory::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Most UIs do a standard setup then render left/top/right/bottom/help in order, then stop. This function
    //  does that and is called from RenderForeground() and RenderBackground(). It can be overridden if a different
    //  order is needed or there are more sub-components that need rendering. It can also be bypassed entirely
    //  by overriding RenderForeground().
    //--By default, objects slide in from the screen edges.

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    //--Deconstruction.
    RenderDeconstruct(pVisAlpha);

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);
}
void AdvUIInventory::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Only the top UI renders on this object. It slides in from the top of the screen.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Rendering]
    //--Inventory block.
    RenderInventoryListing(cColorAlpha);

    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Inventory");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Highlight.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rHighlight_Standard);

    //--Strings.
    mShowHelpString->DrawText  (0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mChangeSortString->DrawText(0.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollFastString->DrawText(0.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Right-aligned strings.
    if(AdventureMenu::xDeconstructScript) mDeconstructString->DrawText(VIRTUAL_CANVAS_X, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIInventory::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Only the bottom UI renders on this object. It slides in from the bottom of the screen.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetExtendedItemList();

    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Render]
    //--Render description frame and text.
    AdventureItem *rSelectedItem = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);
    RenderInventoryDescription(rSelectedItem);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///===================================== Component Drawing ========================================
void AdvUIInventory::RenderInventoryListing(float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the main inventory block, listing the items, their properties, who equips them, etc.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetExtendedItemList();

    ///--[Frame]
    //--The frame always renders. If there are no items to render, stop, but always render the frame.
    AdvImages.rFrame_ItemListing->Draw();

    //--If the list has zero elements, stop.
    if(rItemList->GetListSize() < 1) return;

    ///--[Headers]
    //--Static text.
    mColorHeading.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Heading->DrawText( 75.0f, 102.0f, 0, 1.0f, "Name");
    AdvImages.rFont_Heading->DrawText(558.0f, 102.0f, 0, 1.0f, "Type");
    AdvImages.rFont_Heading->DrawText(675.0f, 102.0f, 0, 1.0f, "Value");
    AdvImages.rFont_Heading->DrawText(834.0f, 102.0f, 0, 1.0f, "Gems");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    //--Icons. Render at double size.
    float cPropIconX = 980.0f;
    float cPropIconY = 101.0f;
    float cPropIconW =  42.0f;
    float cScale = 2.0f;
    float cScaleInv = 1.0f / cScale;
    glTranslatef(cPropIconX, cPropIconY, 0.0f);
    glScalef(cScale, cScale, 1.0f);
    for(int i = 0; i < ADVUIINVENTORY_HEADER_ICONS_TOTAL; i ++)
    {
        AdvImages.rPropertyIcons[i]->Draw(cPropIconW * i * cScaleInv, 0.0f);
    }

    //--Clean.
    glScalef(cScaleInv, cScaleInv, 1.0f);
    glTranslatef(cPropIconX * -1.0f, cPropIconY * -1.0f, 0.0f);

    ///--[Sorting Indicator]
    int tSortFlag = rInventory->GetInternalSortFlag();

    //--Change Sorting string.
    if(tSortFlag == AINV_SORT_CRITERIA_NAME)
        AdvImages.rOverlay_SortArrow->Draw(176.0f, 127.0f);
    else if(tSortFlag == AINV_SORT_CRITERIA_VALUE)
        AdvImages.rOverlay_SortArrow->Draw(774.0f, 127.0f);
    else if(tSortFlag == AINV_SORT_CRITERIA_TYPE)
        AdvImages.rOverlay_SortArrow->Draw(646.0f, 127.0f);

    ///--[Iterate]
    //--Setup.
    float cEntryY = 147.0f;
    float cEntryH =  26.0f;

    //--Render items until we hit the render cap.
    int tRender = 0;
    AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--Effective position.
        int tEffectiveRender = tRender - mItemSkip;

        //--Skip when scrolling.
        if(tEffectiveRender < 0)
        {
            tRender ++;
            rItem = (AdventureItem *)rItemList->AutoIterate();
            continue;
        }

        //--The name of the iterator is the owner of the item. The function needs this to render their icon.
        RenderInventoryEntry(rItem, rItemList->GetIteratorName(), cEntryY + (cEntryH * tEffectiveRender), (tRender % 2 == 0), pColorAlpha);
        tRender ++;

        //--Clamp. Only render cItemsPerPage entries.
        if(tEffectiveRender >= cItemsPerPage-1)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next entry.
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }

    ///--[Detail]
    //--Column dividers render after the entries, as the entries place black backings.
    AdvImages.rOverlay_ItemListingDetail->Draw();

    ///--[Scrollbar]
    //--If there is more than cxItemsPerPage in the inventory, a scrollbar appears on the right
    //  to show how many more entries there are.
    if(rItemList->GetListSize() > cItemsPerPage)
    {
        StandardRenderScrollbar(mItemSkip, cItemsPerPage, rItemList->GetListSize(), 127.0f, 366.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }
}
void AdvUIInventory::RenderInventoryEntry(AdventureItem *pItem, const char *pOwnerName, float pYPosition, bool pIsOdd, float pColorAlpha)
{
    ///--[Documentation]
    //--Given an inventory item and a position, renders that item's icon, user, name, etc. Does nothing
    //  if an invalid item is provided.
    if(!pItem) return;

    //--Position setup.
    float cIconX     =  65.0f;
    float cFaceX     = cIconX + 24.0f;
    float cFaceW     =  26.0f;
    float cNameX     = cFaceX + 28.0f;
    float cTypeX     = 660.0f;
    float cValueX    = 723.0f;
    float cGemX      = 815.0f;
    float cPropertyX = 998.0f;
    float cPropertyW =  42.0f;

    //--Vertical offsets.
    float cIconOffY = 2.0f;
    float cFaceOffY = 3.0f;//No more weed for that man!
    float cNameOffY = 2.0f;
    float cGemOffY  = 2.0f;

    ///--[Backing]
    //--If this is an odd-numbered entry, give it a dark backing. This makes entries easier to pick out visually.
    if(pIsOdd)
    {
        StarBitmap::DrawRectFill(35.0f, pYPosition, 1330.0f, pYPosition + 26.0f, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pColorAlpha));
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    }

    ///--[Icon]
    StarBitmap *rItemIcon = pItem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cIconX, pYPosition + cIconOffY);

    ///--[Owner]
    //--Item has no owner:
    if(!pOwnerName || !strcasecmp(pOwnerName, "Null"))
    {
    }
    //--Item has an owner:
    else
    {
        //--Variables.
        StarBitmap *rFaceImg = NULL;
        TwoDimensionReal tFaceDim;

        //--Resolve the owner's display name. It's usually the same, but not always (Ex: Chris, Christine).
        AdvCombatEntity *rOwningEntity = AdvCombat::Fetch()->GetRosterMemberS(pOwnerName);
        if(rOwningEntity)
        {
            rFaceImg = rOwningEntity->GetFaceProperties(tFaceDim);
        }

        //--Render the character's face.
        if(rFaceImg)
        {
            tFaceDim.mLft = tFaceDim.mLft;
            tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
            tFaceDim.mRgt = tFaceDim.mRgt;
            tFaceDim.mBot = 1.0f - (tFaceDim.mBot);
            rFaceImg->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(cFaceX,          pYPosition + cFaceOffY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(cFaceX + cFaceW, pYPosition + cFaceOffY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(cFaceX + cFaceW, pYPosition + cFaceOffY + cFaceW);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(cFaceX,          pYPosition + cFaceOffY + cFaceW);
            glEnd();
        }
    }

    //--Non-stacking item.
    if(pItem->GetStackSize() == 1)
    {
        AdvImages.rFont_Mainline->DrawTextFixed(cNameX, pYPosition + cNameOffY, cxMaxNameLen, 0, pItem->GetName());
    }
    //--Quantity, if applicable:
    else
    {
        char *tBuffer = InitializeString("%s x%i", pItem->GetName(), pItem->GetStackSize());
        AdvImages.rFont_Mainline->DrawTextFixed(cNameX, pYPosition + cNameOffY, cxMaxNameLen, 0, tBuffer);
        free(tBuffer);
    }

    //--Type.
    const char *rType = pItem->GetDescriptionType();
    if(rType)
        AdvImages.rFont_Statistic->DrawText(cTypeX, pYPosition + cNameOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, rType);

    //--Value. Does not render for key items as they cannot be sold.
    if(pItem->IsKeyItem() == false && pItem->GetValue() > 0)
        AdvImages.rFont_Statistic->DrawTextArgs(cValueX, pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", pItem->GetValue());

    //--Gem slots.
    RenderGemSlotsLine(cGemX, pYPosition + cGemOffY, pItem, AdvImages.rOverlay_GemSlot);

    //--Setup.
    CombatStatistics *rCombatStatistics = pItem->GetEquipStatistics();
    int tHPVal  = rCombatStatistics->GetStatByIndex(STATS_HPMAX);
    int tAtkVal = rCombatStatistics->GetStatByIndex(STATS_ATTACK);
    int tAccVal = rCombatStatistics->GetStatByIndex(STATS_ACCURACY);
    int tEvdVal = rCombatStatistics->GetStatByIndex(STATS_EVADE);
    int tIniVal = rCombatStatistics->GetStatByIndex(STATS_INITIATIVE);
    int tPrtVal = rCombatStatistics->GetStatByIndex(STATS_RESIST_PROTECTION);

    //--Properties.
    if(tHPVal  != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 0.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tHPVal);
    if(tAtkVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 1.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tAtkVal);
    if(tAccVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 2.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tAccVal);
    if(tEvdVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 3.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tEvdVal);
    if(tIniVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 4.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tIniVal);
    if(tPrtVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 5.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tPrtVal);
}
void AdvUIInventory::RenderInventoryDescription(AdventureItem *pItem)
{
    ///--[Documentation]
    //--Given an inventory item, renders its description text and the frame around it. Always renders
    //  the frame even if no item is provided.
    //--The format is identical to the Equipment UI version.
    AdvImages.rFrame_Description->Draw();

    //--If no item is provided, because the inventory has no items or there is otherwise an error,
    //  stop rendering here.
    if(!pItem) return;

    ///--[Header]
    //--Expandable header and name. Note that there is an additional 48 pixels of space on the header
    //  within the dead zones.
    const char *rItemName = pItem->GetName();
    float cNameLen = AdvImages.rFont_Heading->GetTextWidth(rItemName);
    RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, cNameLen - 48.0f, 76.0f, 76.0f, AdvImages.rOverlay_ExpandableHeader);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 510.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rItemName);

    ///--[Text Lines]
    //--Setup.
    float cXPosition =  50.0f;
    float cYPosition = 558.0f;
    float cYHeight   =  25.0f;

    //--Render advanced description lines.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = pItem->GetAdvancedDescription(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }
}
