//--Base
#include "StarCamera2D.h"

//--Classes
//--Definitions
#include "HitDetection.h"

//--Generics
//--GUI
//--Libraries
//--Managers

void StarCamera2D::SetZoneMode(bool pFlag)
{
    //--Sets the zone flag on or off.  Does NOT clear the zone data if the
    //  flag is set to false!
    mUsePositiveCamera = pFlag;
    mOldCameraFlag = pFlag;
}
void StarCamera2D::AllocZones(uint32_t pZonesTotal)
{
    //--Clears old zone data and allocates new space, clearing it.  Pass zero
    //  to deallocate all old data.
    free(mCamZoneList);
    mCamZoneList = NULL;
    mTotalCamZones = pZonesTotal;
    if(mTotalCamZones == 0) return;

    SetMemoryData(__FILE__, __LINE__);
    mCamZoneList = (CAM_ZONE *)starmemoryalloc(sizeof(CAM_ZONE) * mTotalCamZones);
    for(uint32_t i = 0; i < mTotalCamZones; i ++)
    {
        mCamZoneList[i].mIsActive = false;
        mCamZoneList[i].mLft = 0;
        mCamZoneList[i].mTop = 0;
        mCamZoneList[i].mRgt = 0;
        mCamZoneList[i].mBot = 0;
    }
}
void StarCamera2D::ReallocZones(int pZonesTotal)
{
    //--Reallocates the old zone data.  This would invalidate any pointers
    //  pointing to the list, but that rarely happens as it is.
    if(pZonesTotal == 0)
    {
        mTotalCamZones = 0;
        free(mCamZoneList);
        mCamZoneList = NULL;
        return;
    }

    mCamZoneList = (CAM_ZONE *)realloc(mCamZoneList, sizeof(CAM_ZONE) * mTotalCamZones);
    for(int i = mTotalCamZones; i < pZonesTotal; i ++)
    {
        mCamZoneList[i].mIsActive = false;
        mCamZoneList[i].mLft = 0;
        mCamZoneList[i].mTop = 0;
        mCamZoneList[i].mRgt = 0;
        mCamZoneList[i].mBot = 0;
    }

    mTotalCamZones = pZonesTotal;
}
void StarCamera2D::SetZoneActivity(uint32_t pSlot, bool pFlag)
{
    //--Enables or disables the specified camera zone.
    if(pSlot < 0 || pSlot >= mTotalCamZones || !mCamZoneList) return;

    mCamZoneList[pSlot].mIsActive = pFlag;
}
void StarCamera2D::SetZone(uint32_t pSlot, int pLft, int pTop, int pRgt, int pBot)
{
    //--Sets the specified zone.  Also implicitly activates it.
    if(pSlot < 0 || pSlot >= mTotalCamZones || !mCamZoneList) return;

    mCamZoneList[pSlot].mIsActive = true;
    mCamZoneList[pSlot].mLft = pLft;
    mCamZoneList[pSlot].mTop = pTop;
    mCamZoneList[pSlot].mRgt = pRgt;
    mCamZoneList[pSlot].mBot = pBot;
}
bool StarCamera2D::IsPositionValid(ThreeDimensionReal pPosition, int pXMove, int pYMove)
{
    //--2D checker function, returns true if the Camera can reposition itself to the specified
    //  position.  Also returns true automatically if the positive camera is not activated.
    if(!mUsePositiveCamera || !mCamZoneList) return true;

    //--Scan all the position zones.  If the new position is COMPLETELY within
    //  any one zone, then return true immediately.
    for(uint32_t i = 0; i < mTotalCamZones; i ++)
    {
        //--Inactive zones don't count.  Duh.
        if(mCamZoneList[i].mIsActive)
        {
            if(IsCollisionStrict(pPosition, mCamZoneList[i].mLft, mCamZoneList[i].mTop, mCamZoneList[i].mRgt, mCamZoneList[i].mBot))
            {
                return true;
            }
            /*if(mCamZoneList[i].mAdvisePercent < 1.0f)
            {
                float tCross = PercentOverlap(pPosition, mCamZoneList[i].mLft, mCamZoneList[i].mTop, mCamZoneList[i].mRgt, mCamZoneList[i].mBot);
                fprintf(stderr, "Cross Percent %i: %f %f\n", i, tCross, mCamZoneList[i].mAdvisePercent);
                if(tCross >= mCamZoneList[i].mAdvisePercent && tCross != 1.0f)
                {
                    if(pXMove > 0 && mCamZoneList[i].mXAdvise > 0.0f) return true;
                    if(pXMove < 0 && mCamZoneList[i].mXAdvise < 0.0f) return true;
                    if(pYMove > 0 && mCamZoneList[i].mYAdvise > 0.0f) return true;
                    if(pYMove < 0 && mCamZoneList[i].mYAdvise < 0.0f) return true;
                }
            }*/
        }
    }

    //--No zones were active and true, false.
    return false;
}
