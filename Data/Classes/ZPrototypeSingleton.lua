--To-do
--Replace [ClassSample] with the name of the class
--Balance the headers
--Write a description
--Delete these instructions

-- |[ ================================ [ClassSample] Class File ================================ ]|
--Class file for [ClassSample]. Call this once at initialization.

--Place a description here. This is a singleton version of the normal file, so it has no constructor.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
[ClassSample] = {}
[ClassSample].__index = [ClassSample] 

-- |[ ============ Class Methods ============= ]|
--System
--Functions
--function [ClassSample]:fnDoAThing(pzaListing) end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "[ClassSample]Functions.lua")
