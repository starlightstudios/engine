//--Base
#include "AdvUIFileSelect.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StringEntry.h"
#include "StarlightString.h"
#include "FlexMenu.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "SaveManager.h"

///========================================== System ==============================================
AdvUIFileSelect::AdvUIFileSelect()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AFLS");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ==== AdvUIFileSelect ===== ]
    ///--[System]
    mIsEditingNoteForNewSave = false;
    mEmptySaveSlotsTotal = ADVUIFILE_SCAN_TOTAL;

    ///--[Selection and Navigation]
    mSelectFileCursor = 0;
    mSelectFileLastColumn = 2;
    mSelectFileOffset = 0;
    mHighestCursor = 0;
    mEditingFileCursor = -1;
    mArrowBobTimer = 0;
    mArrowOpacityTimerLft = 0;
    mArrowOpacityTimerRgt = 0;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    ///--[Page Moving]
    mIsPageMoving = false;
    mIsPageMovingUp = false;
    mIsPageMovingAll = false;
    mPageMoveTimer = 0;

    ///--[File Information Storage]
    mCurrentSortingType = 0;
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        mFileListingIndex[i] = i;
    }
    memset(mFileListing,         0, sizeof(char)          * ADVUIFILE_SCAN_TOTAL * 80);
    memset(mLoadingPacks,        0, sizeof(LoadingPack *) * ADVUIFILE_SCAN_TOTAL);
    memset(mExistingFileListing, 0, sizeof(bool)          * ADVUIFILE_SCAN_TOTAL);

    ///--[Entering Notes]
    mIsEnteringNotes = false;
    mNoteSlot = 0;
    mStringEntryForm = StringEntry::rGenerateStringEntry();
    mStringEntryForm->SetHardcap(128);

    ///--[Deleting a Save]
    mSaveDeletionIndex = -1;
    mSaveDeletionIndexOld = -1;
    mSaveDeletionOpacityTimer = 0;

    ///--[Help Strings]
    mEditNoteString = new StarlightString();
    mCycleSortString = new StarlightString();
    mCurrentSortString = new StarlightString();
    mScrollPageString = new StarlightString();
    mDeleteFileString = new StarlightString();
    mFastScrollString = new StarlightString();
    mPageLftString = new StarlightString();
    mPageRgtString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIFileSelect::~AdvUIFileSelect()
{
    delete mStringEntryForm;
    delete mEditNoteString;
    delete mCycleSortString;
    delete mCurrentSortString;
    delete mScrollPageString;
    delete mDeleteFileString;
    delete mFastScrollString;
    delete mPageLftString;
    delete mPageRgtString;
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        if(!mLoadingPacks[i]) continue;
        LoadingPack::DeleteThis(mLoadingPacks[i]);
    }
}
void AdvUIFileSelect::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure File Select UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",        &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Static Variables]
//--Stores the last means of sorting files so the same type is used when opening a new menu instance.
//  This gets saved to the configuration file whenever the player changes the sorting type and closes
//  the menu.
int AdvUIFileSelect::xLastSavefileSortingType = AM_SORT_DATEREV;

///===================================== Property Queries =========================================
int AdvUIFileSelect::GetArraySlotFromLookups(int pLookup)
{
    //--Given a position in the visual lookups, returns the actual array position of the entry in question, or -1 on error.
    //  The files run from File000.slf to File999.slf, but the lookups change positions around to make sorting easier.
    //--The passed-in position is where we are on the grid, which goes from 0x0 to 4x4, or 0 to 24 internally.

    //--Get the grid position and add the page to it.
    int tCurrent = pLookup + mSelectFileOffset;
    if(tCurrent < 0 || tCurrent >= ADVUIFILE_SCAN_TOTAL) return -1;

    //--Check the index. If it's out of range, standardize it to -1.
    int tReturn = mFileListingIndex[tCurrent];
    if(tReturn < 0 || tReturn >= ADVUIFILE_SCAN_TOTAL) return -1;

    //--Now pass back the actual index.
    return tReturn;
}

///======================================= Manipulators ===========================================
void AdvUIFileSelect::TakeForeground()
{
    ///--[Variables]
    //--Flags.
    mCurrentSortingType = xLastSavefileSortingType;
    //mCurrentMode = AM_CAMPFIRE_MODE_FILESELECT;

    ///--[Construction]
    //--Scan files.
    ScanExistingFiles();

    //--Cursor.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[String Refresh]
    //--Control strings.
    mEditNoteString  ->AutoSetControlString("[IMG0] Edit File Note",        1, "F1");
    mCycleSortString ->AutoSetControlString("[IMG0] Change Sorting",        1, "F2");
    mScrollPageString->AutoSetControlString("[IMG0][IMG1] Change Pages",    2, "UpLevel", "DnLevel");
    mDeleteFileString->AutoSetControlString("[IMG0] Delete File",           1, "Delete");
    mFastScrollString->AutoSetControlString("[IMG0][IMG1] First/Last Page", 2, "GUI|Home", "GUI|End");
    mPageLftString   ->AutoSetControlString("[IMG0] + [IMG1] Page Left",    2, "Ctrl", "Left");
    mPageRgtString   ->AutoSetControlString("[IMG0] + [IMG1] Page Right",   2, "Ctrl", "Right");

    //--Sorting type, uses subroutine.
    ResolveSortString();
}
void AdvUIFileSelect::BeginPageMove(bool pIsUp, bool pIsAll)
{
    ///--[Documentation]
    //--When the page moves by scrolling at the edge of pressing special key combinations, this routine is called.
    mIsPageMoving = true;
    mIsPageMovingUp = pIsUp;
    mIsPageMovingAll = pIsAll;
    mPageMoveTimer = 0;
}

///======================================= Core Methods ===========================================
void AdvUIFileSelect::RefreshMenuHelp()
{

}
void AdvUIFileSelect::RecomputeCursorPositions()
{
    ///--[Documentation and Setup]
    //--Knowing the cursor position of the file select, computes the borders of the highlight and
    //  orders the highlight to move to it.

    //--Basic.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = 1.0f;
    float cBot = 1.0f;

    //--"New Save File" button.
    if(mSelectFileCursor == -1)
    {
        cLft = 53.0f;
        cTop = 86.0f;
        cRgt = cLft + 1260.0f;
        cBot = cTop + 81.0f;
    }
    //--Otherwise, it's on a file.
    else
    {
        //--Constants.
        float cBoxesLft =  53.0f;
        float cBoxesTop = 180.0f;
        float cBoxesWid = 236.0f;
        float cBoxesHei = 108.0f;
        float cBoxesSpX = 256.0f;
        float cBoxesSpY = 113.0f;

        //--Resolve current box.
        int tCurrentCusor = mSelectFileCursor % (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y);
        int tCurrentX = tCurrentCusor % ADVUIFILE_GRID_SIZE_X;
        int tCurrentY = tCurrentCusor / ADVUIFILE_GRID_SIZE_X;

        //--Calculate.
        cLft = cBoxesLft + (cBoxesSpX * tCurrentX);
        cTop = cBoxesTop + (cBoxesSpY * tCurrentY);
        cRgt = cLft + cBoxesWid;
        cBot = cTop + cBoxesHei;
    }

    //--Resize the cursor.
    mHighlightPos .MoveTo(cLft,        cTop,        cxAdvCurTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxAdvCurTicks);
}
void AdvUIFileSelect::ScanExistingFiles()
{
    ///--[Documentation]
    //--Scans across the file listing on the hard drive, checking off all the ones that do exist so they can be
    //  displayed in order. May be slow, this are a lot of hard-drive accesses.
    char *tFileBuf = NULL;

    //--Deallocate.
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        if(mLoadingPacks[i]) LoadingPack::DeleteThis(mLoadingPacks[i]);
    }

    //--Clear data.
    mEmptySaveSlotsTotal = ADVUIFILE_SCAN_TOTAL;
    mSelectFileCursor = -1;
    mSelectFileOffset = 0;
    mHighestCursor = 0;
    mEditingFileCursor = -1;
    memset(mLoadingPacks,        0, sizeof(LoadingPack *) * ADVUIFILE_SCAN_TOTAL);
    memset(mExistingFileListing, 0, sizeof(bool)          * ADVUIFILE_SCAN_TOTAL);

    ///--[Temporary Menu]
    //--Create a temporary FlexMenu. We need it to resolve images for us.
    FlexMenu *tMenu = new FlexMenu();
    tMenu->Construct();

    //--Game Directory:
    const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

    ///--[Scan Files]
    //--Begin scanning. Info is placed in the slots in order from 0 to ADVUIFILE_SCAN_TOTAL, but everything else
    //  will use mFileListingIndex[] to sort the results.
    for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
    {
        //--Assemble filename.
        tFileBuf = InitializeString("%s/../../Saves/File%03i.slf", rAdventureDir, i);

        //--Check for existence.
        FILE *fInfile = fopen(tFileBuf, "rb");

        //--Flag.
        mExistingFileListing[i] = (fInfile != NULL);
        if(mExistingFileListing[i])
        {
            //--Reduce the empty saves count.
            mEmptySaveSlotsTotal --;

            //--Close the file.
            fclose(fInfile);

            //--Save the filename.
            sprintf(mFileListing[mHighestCursor], "File%03i.slf", i);

            //--Store the file info.
            mLoadingPacks[mHighestCursor] = SaveManager::Fetch()->GetSaveInfo(tFileBuf);
            tMenu->CrossloadSaveImages(mLoadingPacks[mHighestCursor]);

            //--Indicate this is selectable.
            mHighestCursor ++;
        }
        //--Empty slots appear as blank.
        else
        {
            mHighestCursor ++;
        }

        //--Clean.
        free(tFileBuf);
    }

    //--Clean.
    delete tMenu;

    ///--[Sort]
    //--Actually sort things.
    if(mCurrentSortingType == AM_SORT_SLOT)
    {
        SortFilesBySlot();
    }
    else if(mCurrentSortingType == AM_SORT_SLOTREV)
    {
        SortFilesBySlotRev();
    }
    else if(mCurrentSortingType == AM_SORT_DATEREV)
    {
        SortFilesByDateRev();
    }
    else if(mCurrentSortingType == AM_SORT_DATE)
    {
        SortFilesByDate();
    }
}
void AdvUIFileSelect::ResolveSortString()
{
    ///--[Documentation]
    //--Checks the sorting type variable and changes the sorting string to reflect it.

    //--Current Sort Type
    if(mCurrentSortingType == AM_SORT_SLOT)
    {
        mCurrentSortString->SetString("Current: By Slot");
    }
    else if(mCurrentSortingType == AM_SORT_SLOTREV)
    {
        mCurrentSortString->SetString("Current: Reverse Slot");
    }
    else if(mCurrentSortingType == AM_SORT_DATEREV)
    {
        mCurrentSortString->SetString("Current: Newest First");
    }
    else if(mCurrentSortingType == AM_SORT_DATE)
    {
        mCurrentSortString->SetString("Current: Oldest First");
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIFileSelect::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Update handler, returns true if this handled the update, false otherwise.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--Does not interrupt the update cycle.
    UpdateTimers();

    ///--[Mode Handlers]
    //--If a handler returns true, stop the update.
    if(UpdatePageMove())            return true;
    if(UpdateNotes())               return true;
    if(UpdateSortControls())        return true;
    if(UpdateDeletion())            return true;
    if(UpdateDirectionalControls()) return true;
    if(UpdateSeekToEnds())          return true;
    if(UpdateActivateCancel())      return true;

    //--Handled update, return true.
    return true;
}
void AdvUIFileSelect::UpdateTimers()
{
    ///--[Documentation]
    //--Standard Timers.
    StandardTimer(mVisibilityTimer,      0, mVisibilityTimerMax, true);
    StandardLoopTimer(mArrowBobTimer,    0, cxAdvOscTicks);
    StandardTimer(mArrowOpacityTimerLft, 0, cxAdvArrowVisTicks, (mSelectFileOffset > 0));
    StandardTimer(mArrowOpacityTimerRgt, 0, cxAdvArrowVisTicks, (mSelectFileOffset < (ADVUIFILE_SCAN_TOTAL - (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y))));

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
}
bool AdvUIFileSelect::UpdatePageMove()
{
    ///--[Documentation]
    //--Segment update. Handles page scrolling and timers. Returns true if it handled the update.
    if(!mIsPageMoving) return false;

    //--Increment.
    mPageMoveTimer ++;

    //--Reached end of scroll timer.
    if(mPageMoveTimer >= cxAdvPageScrollTicks)
    {
        //--Flags.
        mIsPageMoving = false;

        //--Moving one page:
        if(!mIsPageMovingAll)
        {
            if(mIsPageMovingUp)
            {
                mSelectFileOffset += (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y);
            }
            else
            {
                mSelectFileOffset -= (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y);
            }
        }
        //--Move all pages:
        else
        {
            if(mIsPageMovingUp)
            {
                mSelectFileOffset = ADVUIFILE_SCAN_TOTAL - (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y);
            }
            else
            {
                mSelectFileOffset = 0;
            }
        }
    }

    //--Always block the update.
    return true;
}
bool AdvUIFileSelect::UpdateNotes()
{
    ///--[Documentation]
    //--Segment update. Handles editing file notes. Returns true if it handled the update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Is Entering Notes]
    //--Accept the player's keypresses to edit the save note.
    if(mIsEnteringNotes)
    {
        //--Run the update.
        mStringEntryForm->Update();

        //--Check for completion.
        if(mStringEntryForm->IsComplete())
        {
            //--Flag.
            mIsEnteringNotes = false;

            //--Get the note from the string entry form. It can legally be empty, in which case
            //  we generate a standard note.
            const char *rEnteredString = mStringEntryForm->GetString();

            //--Generate a note if the player left it empty.
            bool tDeallocateNote = false;
            char *rNewNote = SaveManager::GenerateNote(rEnteredString, tDeallocateNote);

            ///--[New Save]
            //--If this is a new saving case:
            if(mEditingFileCursor != -1)
            {
                //--Set the current savegame name.
                SaveManager::Fetch()->SetSavegameName(rNewNote);

                //--Game Directory:
                const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

                //--New savegame.
                if(mIsEditingNoteForNewSave)
                {
                    //--Set the current savegame name.
                    SaveManager::Fetch()->SetSavegameName(rNewNote);

                    //--Write to the file.
                    char *tFileBuf = InitializeString("%s/../../Saves/File%03i.slf", rAdventureDir, mEditingFileCursor);
                    SaveManager::Fetch()->SaveAdventureFile(tFileBuf);
                    free(tFileBuf);

                    //--Decrement the count of available save slots.
                    mEmptySaveSlotsTotal --;
                }
                //--Saving over an old save with a new note.
                else
                {
                    //--Write to the file. Remember the files are listed backwards.
                    char *tFileBuf = InitializeString("%s/../../Saves/File%03i.slf", rAdventureDir, mEditingFileCursor);
                    SaveManager::Fetch()->SaveAdventureFile(tFileBuf);
                    free(tFileBuf);
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Save");

                //--Exit this mode.
                FlagExit();
            }
            ///--[Note Modification]
            //--If the file edit cursor is -1, then we're just saving over the note for that pack.
            //  This code cannot create new saves, just edit existing ones.
            else
            {
                //--Get the loading pack in question. If the loading pack exists, write over it.
                LoadingPack *rPack = mLoadingPacks[mNoteSlot];
                if(!rPack) return true;

                //--Modify the string.
                ResetString(rPack->mFileName, rNewNote);

                //--Run the SaveManager's subroutine to switch the note out.
                SaveManager::ModifyFileNote(rNewNote, rPack->mFilePath);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--If the note was generated, deallocate it here.
            if(tDeallocateNote) free(rNewNote);
        }
        else if(mStringEntryForm->IsCancelled())
        {
            mIsEnteringNotes = false;
            mEditingFileCursor = -1;
        }

        //--Block the rest of the update.
        return true;
    }

    ///--[Switch to Editing Mode]
    //--Press F1 to edit notes on a file.
    if(rControlManager->IsFirstPress("F1"))
    {
        //--The given file may be different from the grid position based on page and sorting. Verify.
        int tFileSlot = GetArraySlotFromLookups(mSelectFileCursor);
        if(tFileSlot == -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Make sure there's actually a file in the given slot.
        else if(!mLoadingPacks[tFileSlot])
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Checks passed.
        else
        {
            //--Verify.
            mEditingFileCursor = -1;
            mNoteSlot = tFileSlot;
            if(mNoteSlot >= 0 && mLoadingPacks[mNoteSlot])
            {
                //--Flags.
                mIsEnteringNotes = true;

                //--Upload info.
                mStringEntryForm->SetCompleteFlag(false);
                mStringEntryForm->SetPromptString("Enter a note for this save file.");
                mStringEntryForm->SetString(mLoadingPacks[mNoteSlot]->mFileName);
                mStringEntryForm->SetEmptyString("Enter Name");
            }
        }

        //--Block the update.
        return true;
    }

    ///--[No Input]
    return false;
}
bool AdvUIFileSelect::UpdateSortControls()
{
    ///--[Documentation]
    //--Segment update. Handles F2 which changes the sort order. Returns true if it handled the update.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Keypress.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--Cycle the sort value up by 1.
        mCurrentSortingType = (mCurrentSortingType + 1) % AM_SORT_TOTAL;

        //--Actually sort things.
        if(mCurrentSortingType == AM_SORT_SLOT)
        {
            SortFilesBySlot();
        }
        else if(mCurrentSortingType == AM_SORT_SLOTREV)
        {
            SortFilesBySlotRev();
        }
        else if(mCurrentSortingType == AM_SORT_DATEREV)
        {
            SortFilesByDateRev();
        }
        else if(mCurrentSortingType == AM_SORT_DATE)
        {
            SortFilesByDate();
        }

        //--Update the sorting string to reflect the new sort type.
        ResolveSortString();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Block the rest of the update.
        return true;
    }

    ///--[No Input]
    return false;
}
bool AdvUIFileSelect::UpdateDeletion()
{
    ///--[Documentation]
    //--Segment update. Handles the confirmation window when deleting a savefile. Returns true if it handled the update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Deletion is Active]
    //--When active, blocks the update.
    if(mSaveDeletionIndex != -1)
    {
        //--Increment the timer.
        if(mSaveDeletionOpacityTimer < ADVMENU_FILESELECT_DELETION_TICKS) mSaveDeletionOpacityTimer ++;

        //--Stop updating if the timer is less than 3. This prevents accidents.
        if(mSaveDeletionOpacityTimer < 3) return true;

        //--Activate will delete the file.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Figure out which slot we're on.
            int tFileSlot = GetArraySlotFromLookups(mSaveDeletionIndex);
            if(tFileSlot == -1)
            {
                mSaveDeletionIndexOld = mSaveDeletionIndex;
                mSaveDeletionIndex = -1;
                return true;
            }

            //--Resolve the path.
            const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");
            char tFileBuf[STD_PATH_LEN];
            sprintf(tFileBuf, "%s/../../Saves/File%03i.slf", rAdventureDir, tFileSlot);
            //fprintf(stderr, "File Buf: %s\n", tFileBuf);

            //--Delete the file.
            int tDeletedFile = remove(tFileBuf);

            //--File deleted correctly:
            if(!tDeletedFile)
            {
                LoadingPack::DeleteThis(mLoadingPacks[tFileSlot]);
                mLoadingPacks[tFileSlot] = NULL;
            }
            //--Error:
            else
            {
                fprintf(stderr, "Error, unable to delete file %s. Reasons unknown.\n", tFileBuf);
            }

            //--In all cases, exit out of deletion mode.
            mSaveDeletionIndexOld = mSaveDeletionIndex;
            mSaveDeletionIndex = -1;
            AudioManager::Fetch()->PlaySound("World|TranqShot");
        }
        //--Cancel returns to normal.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mSaveDeletionIndexOld = mSaveDeletionIndex;
            mSaveDeletionIndex = -1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Block the rest of the update.
        return true;
    }
    ///--[Deletion is Inactive]
    //--Not deleting, decrement the timer.
    else
    {
        //--Timer.
        if(mSaveDeletionOpacityTimer > 0)
        {
            mSaveDeletionOpacityTimer --;
            if(mSaveDeletionOpacityTimer == 0) mSaveDeletionIndexOld = -1;
        }

        //--See if we want to activate deletion.
        if(rControlManager->IsFirstPress("Delete"))
        {
            //--Check the file index.
            int tFileSlot = GetArraySlotFromLookups(mSelectFileCursor);

            //--On the "New File" cursor, do nothing:
            if(mSelectFileCursor == -1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--If the file doesn't actually exist, you can't delete it, stupid.
            else if(tFileSlot == -1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Otherwise, set the deletion index.
            else
            {
                mSaveDeletionIndex = mSelectFileCursor;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--Block rest of the update.
            return true;
        }
    }

    ///--[No Blocking]
    return false;
}
bool AdvUIFileSelect::UpdateDirectionalControls()
{
    ///--[Documentation]
    //--Segment update. Handles the left/right/up/down keys. Returns true if it handled the update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Page Up and Down]
    //--Page up, goes left.
    if(rControlManager->IsFirstPress("UpLevel") || (rControlManager->IsDown("Ctrl") && rControlManager->IsFirstPress("Left")))
    {
        //--Page is already at 0, so do nothing.
        if(mSelectFileOffset < (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y))
        {
            return true;
        }
        //--Move a page down.
        else
        {
            BeginPageMove(false, false);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
    }
    //--Page down, goes right.
    if(rControlManager->IsFirstPress("DnLevel") || (rControlManager->IsDown("Ctrl") && rControlManager->IsFirstPress("Right")))
    {
        //--Page is already at max, so do nothing.
        if(mSelectFileOffset + (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y) >= ADVUIFILE_SCAN_TOTAL)
        {
            return true;
        }
        //--Move a page up.
        else
        {
            BeginPageMove(true, false);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
    }

    ///--[Single Moves]
    //--Up.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--When on "New File", do nothing.
        if(mSelectFileCursor == -1)
        {
        }
        //--In the first row, move to "New File".
        else if(mSelectFileCursor < ADVUIFILE_GRID_SIZE_X)
        {
            mSelectFileLastColumn = mSelectFileCursor;
            mSelectFileCursor = -1;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
        //--Otherwise, decrement by ADVUIFILE_GRID_SIZE_X.
        else
        {
            mSelectFileCursor -= ADVUIFILE_GRID_SIZE_X;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
    }
    //--Down.
    if(rControlManager->IsFirstPress("Down"))
    {
        //--When on "New File", go to the mSelectFileLastColumn'th entry.
        if(mSelectFileCursor == -1)
        {
            mSelectFileCursor = mSelectFileLastColumn;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
        //--In the first four rows, move to "New File".
        else if(mSelectFileCursor < ADVUIFILE_GRID_SIZE_X * (ADVUIFILE_GRID_SIZE_Y-1))
        {
            mSelectFileCursor += ADVUIFILE_GRID_SIZE_X;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
        //--Otherwise, do nothing.
        else
        {
        }
    }
    //--Left.
    if(rControlManager->IsFirstPress("Left"))
    {
        //--When on "New File", move a page left even if ctrl is not down.
        if(mSelectFileCursor == -1)
        {
            //--Page is already at 0, so do nothing.
            if(mSelectFileOffset < (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y))
            {

            }
            //--Move a page down.
            else
            {
                BeginPageMove(false, false);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                return true;
            }
        }
        //--In the leftmost column, move down one page. This always occurs when Ctrl is down.
        else if(mSelectFileCursor % ADVUIFILE_GRID_SIZE_X == 0)
        {
            //--Page is already at 0, so do nothing.
            if(mSelectFileOffset < (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y))
            {

            }
            //--Move a page down.
            else
            {
                //--Flags.
                BeginPageMove(false, false);
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                return true;
            }
        }
        //--Otherwise, decrement by 1.
        else
        {
            mSelectFileCursor --;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
    }
    //--Right.
    if(rControlManager->IsFirstPress("Right"))
    {
        //--When on "New File", do nothing.
        if(mSelectFileCursor == -1)
        {
            //--Page is already at max, so do nothing.
            if(mSelectFileOffset + (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y) >= ADVUIFILE_SCAN_TOTAL)
            {

            }
            //--Move a page up.
            else
            {
                BeginPageMove(true, false);
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                return true;
            }
        }
        //--In the rightmost column, move up one page. This always occurs when Ctrl is down.
        else if(mSelectFileCursor % ADVUIFILE_GRID_SIZE_X == ADVUIFILE_GRID_SIZE_X-1)
        {
            //--Page is already at max, so do nothing.
            if(mSelectFileOffset + (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y) >= ADVUIFILE_SCAN_TOTAL)
            {

            }
            //--Move a page up.
            else
            {
                BeginPageMove(true, false);
                RecomputeCursorPositions();
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                return true;
            }
        }
        //--Otherwise, increment by 1.
        else
        {
            mSelectFileCursor ++;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
    }

    ///--[No Input]
    return false;
}
bool AdvUIFileSelect::UpdateSeekToEnds()
{
    ///--[Documentation]
    //--Segment update. Handles the Home and End keys. Returns true if it handled the update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Seek to Beginning]
    //--Home, goes to zeroth page.
    if(rControlManager->IsFirstPress("GUI|Home"))
    {
        //--Page is already at 0, so do nothing.
        if(mSelectFileOffset < (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y))
        {

        }
        //--Move a page down.
        else
        {
            BeginPageMove(false, true);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
    }

    ///--[Seek to End]
    //--End, goes to last page.
    if(rControlManager->IsFirstPress("GUI|End"))
    {
        //--Page is already at max, so do nothing.
        if(mSelectFileOffset + (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y) >= ADVUIFILE_SCAN_TOTAL)
        {

        }
        //--Move a page up.
        else
        {
            BeginPageMove(true, true);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return true;
        }
    }

    ///--[No Input]
    return false;
}
bool AdvUIFileSelect::UpdateActivateCancel()
{
    ///--[Documentation]
    //--Segment update. Handles the Activate and Cancel keys. Returns true if it handled the update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Activate]
    //--Activate, brings up the note entry form. Completing the form will save out a file.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If the cursor is on -1, then create a new save file. Page doesn't matter, nor does the sorting order.
        if(mSelectFileCursor == -1)
        {
            //--Scan along the list of available spots. First open one gets saved to.
            for(int i = 0; i < ADVUIFILE_SCAN_TOTAL; i ++)
            {
                //--If this spot is occupied, ignore it.
                if(mExistingFileListing[i]) continue;

                //--Mark this for saving and activate note entry.
                mIsEditingNoteForNewSave = true;
                mIsEnteringNotes = true;
                mNoteSlot = i;
                mEditingFileCursor = i;

                //--Upload info.
                mStringEntryForm->SetCompleteFlag(false);
                mStringEntryForm->SetPromptString("Enter a note for this save file.");
                mStringEntryForm->SetString(NULL);
                mStringEntryForm->SetEmptyString("Enter Name");
                break;
            }
        }
        //--Save over an existing file, sets flags.
        else
        {
            //--Figure out which slot we're on.
            int tFileSlot = GetArraySlotFromLookups(mSelectFileCursor);
            if(tFileSlot == -1) return true;

            //--Flags.
            mIsEditingNoteForNewSave = false;
            mIsEnteringNotes = true;
            mNoteSlot = tFileSlot;
            mEditingFileCursor = tFileSlot;

            //--Upload info.
            mStringEntryForm->SetCompleteFlag(false);
            mStringEntryForm->SetPromptString("Enter a note for this save file.");
            mStringEntryForm->SetEmptyString("Enter Name");

            //--If the loading pack in question does not exist, put a default string in.
            if(!mLoadingPacks[mNoteSlot])
            {
                mStringEntryForm->SetString(NULL);
            }
            //--Otherwise, populate with the file name.
            else
            {
                mStringEntryForm->SetString(mLoadingPacks[mNoteSlot]->mFileName);
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Cancel]
    //--Back to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[No Input]
    return false;
}
void AdvUIFileSelect::UpdateBackground()
{
    ///--[Documentation]
    //--Run all timers down.
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIFileSelect::RenderPieces(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the save interface, allowing the player to make a new save, save over existing files,
    //  and so on. This UI has a flat fade-in functionality instead of sliding in from the sides.

    //--Opacity percent. Determines position and fading.
    float cColorAlpha = (float)mVisibilityTimer / (float)mVisibilityTimerMax;

    ///--[Header]
    //--Backing.
    AdvImages.rOverlay_Header->Draw();

    //--Colored Text.
    SetColor("Heading", cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Save");
    SetColor("Clear", cColorAlpha);

    ///--[New Save Button]
    //--Coloring.
    float cColor = 1.0f;
    if(mSelectFileCursor != -1) cColor = 0.5f;
    StarlightColor::SetMixer(cColor, cColor, cColor, cColorAlpha);

    //--Backing and Text.
    AdvImages.rOverlay_NewFile->Draw();
    if(mEmptySaveSlotsTotal > 0)
    {
        AdvImages.rFont_Header->DrawText(VIRTUAL_CANVAS_X * 0.50f, 105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Create New Savefile");
    }
    //--No save slots open!
    else
    {
        AdvImages.rFont_Header->DrawText(VIRTUAL_CANVAS_X * 0.50f, 105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "No Empty Save Slots Available");
    }

    //--Clean.
    SetColor("Clear", cColorAlpha);

    ///--[Body]
    //--Sizes.
    float cSpaceX = 256.0f;
    float cSpaceY = 113.0f;
    float cStartX = (VIRTUAL_CANVAS_X * 0.50f) - (236.0f * 2.5f) - (20.0f * 2.0f);
    float cStartY = (VIRTUAL_CANVAS_Y * 0.30f) - 50.0f;

    //--Variables.
    int tXPos = 0;
    int tYPos = 0;

    //--When moving pages, run a translation action.
    float cTranslateX = 0.0f;
    if(mIsPageMoving)
    {
        //--Compute the offset required.
        float cTranslatePct = EasingFunction::QuadraticInOut(mPageMoveTimer, cxAdvPageScrollTicks);
        cTranslateX = VIRTUAL_CANVAS_X * cTranslatePct * -1.0f;
        if(!mIsPageMovingUp)
        {
            cTranslateX = cTranslateX * -1.0f;
        }

        //--Run a translation action.
        glTranslatef(cTranslateX, 0.0f, 0.0f);
    }

    ///--[Normal Handling]
    //--Render all the blocks on the page.
    if(mSaveDeletionIndex == -1 && mSaveDeletionIndexOld == -1)
    {
        //--Render blocks for each save file that exists, counting up.
        for(int i = 0; i < ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y; i ++)
        {
            //--Positions.
            float cX = cStartX + (tXPos * cSpaceX);
            float cY = cStartY + (tYPos * cSpaceY);

            //--Subroutine.
            RenderFileBlock(i, cX, cY, mSelectFileCursor == i, cColorAlpha);

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= ADVUIFILE_GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= ADVUIFILE_GRID_SIZE_Y) break;
            }
        }
    }
    ///--[Deletion Moving To Center]
    //--Render the blocks in the back, fading out. The block up for deletion moves to the screen's center.
    else if(mSaveDeletionIndex != -1)
    {
        //--Modify the opacity.
        float tDeletionOpacity = EasingFunction::QuadraticInOut(mSaveDeletionOpacityTimer, ADVMENU_FILESELECT_DELETION_TICKS);
        float tModOpacity = 1.0f - (cColorAlpha * tDeletionOpacity);

        //--Render blocks for each save file that exists, counting up.
        for(int i = 0; i < ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y; i ++)
        {
            //--Normal blocks:
            if(mSaveDeletionIndex != i)
            {
                //--Positions.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);

                //--Subroutine.
                RenderFileBlock(i, cX, cY, mSelectFileCursor == i, tModOpacity);
            }
            //--Deletion block:
            else
            {
                //--Positions. This is where the block starts rendering.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);
                float cDestX = cStartX + (2.0f * cSpaceX);
                float cDestY = cStartY + (1.0f * cSpaceY);

                //--Move it towards the end position.
                float tRenderX = cX + ((cDestX - cX) * tDeletionOpacity);
                float tRenderY = cY + ((cDestY - cY) * tDeletionOpacity);

                //--Subroutine.
                RenderFileBlock(i, tRenderX, tRenderY, mSelectFileCursor == i, cColorAlpha);
            }

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= ADVUIFILE_GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= ADVUIFILE_GRID_SIZE_Y) break;
            }
        }

        //--Onscreen text.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tDeletionOpacity);
        AdvImages.rFont_Mainline->DrawText(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.55f +  0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Really Delete This File?");
        StarlightColor::ClearMixer();
    }
    ///--[Deletion Scrolls To Original Position]
    //--Save deletion index is -1, but the old index is not. The block scrolls back to its original position.
    else
    {
        //--Modify the opacity.
        float tDeletionOpacity = EasingFunction::QuadraticInOut(mSaveDeletionOpacityTimer, ADVMENU_FILESELECT_DELETION_TICKS);
        float tModOpacity = 1.0f - (cColorAlpha * tDeletionOpacity);

        //--Render blocks for each save file that exists, counting up.
        for(int i = 0; i < ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y; i ++)
        {
            //--Normal blocks:
            if(mSaveDeletionIndex != i)
            {
                //--Positions.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);

                //--Subroutine.
                RenderFileBlock(i, cX, cY, mSelectFileCursor == i, tModOpacity);
            }
            //--Deletion block:
            else
            {
                //--Positions. This is where the block starts rendering.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);
                float cDestX = cStartX + (2.0f * cSpaceX);
                float cDestY = cStartY + (1.0f * cSpaceY);

                //--Move it towards the end position.
                float tRenderX = cX + ((cDestX - cX) * tDeletionOpacity);
                float tRenderY = cY + ((cDestY - cY) * tDeletionOpacity);

                //--Subroutine.
                RenderFileBlock(i, tRenderX, tRenderY, mSelectFileCursor == i, cColorAlpha);
            }

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= ADVUIFILE_GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= ADVUIFILE_GRID_SIZE_Y) break;
            }
        }

        //--Onscreen text.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tDeletionOpacity);
        AdvImages.rFont_Mainline->DrawText(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.55f +  0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Really Delete This File?");
        StarlightColor::ClearMixer();
    }

    ///--[Page Swap Rendering]
    //--When changing pages, render an additional set of blocks and offset them.
    if(mIsPageMoving)
    {
        //--Reset these variables.
        tXPos = 0;
        tYPos = 0;
        float cXOffset = VIRTUAL_CANVAS_X;

        //--Figure out the index offset.
        int tIndexOffset = (ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y);
        if(!mIsPageMovingUp)
        {
            tIndexOffset = tIndexOffset * -1;
            cXOffset = cXOffset * -1.0f;
        }

        //--Render with an index offset.
        for(int i = 0; i < ADVUIFILE_GRID_SIZE_X * ADVUIFILE_GRID_SIZE_Y; i ++)
        {
            //--Positions.
            float cX = cStartX + (tXPos * cSpaceX);
            float cY = cStartY + (tYPos * cSpaceY);

            //--Subroutine.
            RenderFileBlock(i+tIndexOffset, cX + cXOffset, cY, mSelectFileCursor == i, cColorAlpha);

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= ADVUIFILE_GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= ADVUIFILE_GRID_SIZE_Y) break;
            }
        }

        //--Clean up.
        glTranslatef(-cTranslateX, 0.0f, 0.0f);
    }

    ///--[Arrows]
    //--Compute opacities.
    float tArrowLftOpacity = mArrowOpacityTimerLft / (float)ADVMENU_FILESELECT_ARROW_OPACITY_TICKS;
    float tArrowRgtOpacity = mArrowOpacityTimerRgt / (float)ADVMENU_FILESELECT_ARROW_OPACITY_TICKS;

    //--Compute bobbing offset.
    float cBobPct = mArrowBobTimer / (float)ADVMENU_FILESELECT_ARROW_OSCILLATE_TICKS;
    float cBobX = sinf(cBobPct * 3.1415926f * 2.0f) * ADVMENU_FILESELECT_ARROW_OSCILLATE_DISTANCE;

    //--Render left.
    if(tArrowLftOpacity > 0.0f)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha * tArrowLftOpacity);
        AdvImages.rOverlay_ArrowLft->Draw(cBobX, 0.0f);
    }
    if(tArrowRgtOpacity > 0.0f)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha * tArrowRgtOpacity);
        AdvImages.rOverlay_ArrowRgt->Draw(-cBobX, 0.0f);
    }

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    ///--[Entering Notes]
    //--Notes appear over everything else.
    if(mIsEnteringNotes)
    {
        mStringEntryForm->Render();
        return;
    }

    ///--[Help Strings]
    //--Left side.
    mEditNoteString->   DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCycleSortString->  DrawText(0.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCurrentSortString->DrawText(0.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Right side.
    mScrollPageString->DrawText(1090.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mDeleteFileString->DrawText(1090.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mFastScrollString->DrawText(1090.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Left bottom
    mPageLftString->DrawText(0.0f, VIRTUAL_CANVAS_Y - cxAdvMainlineFontH, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mPageRgtString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - cxAdvMainlineFontH, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    ///--[Highlight]
    //--Goes over everything else.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    ///--[Clean]
    StarlightColor::ClearMixer();
}
void AdvUIFileSelect::RenderFileBlock(int pIndex, float pLft, float pTop, bool pIsHighlighted, float pOpacityPct)
{
    ///--[Documentation and Setup]
    //--Given an index, renders the file information for that load information pack. Note that this is the RAW file
    //  position in the original array, NOT the lookup index. Depending on the sorting, the lookup index may not
    //  be the same as the array position.
    //--Note: This may modify the color mixer. Clear the mixer when you are done.
    int tFileSlot = GetArraySlotFromLookups(pIndex);
    if(tFileSlot < 0 || tFileSlot >= ADVUIFILE_SCAN_TOTAL) return;

    //--Fast-access pointer.
    StarFont *rUseFont = AdvImages.rFont_Small;

    //--Color.
    float cColor = 1.0f;
    if(!pIsHighlighted) cColor = 0.5f;
    StarlightColor::SetMixer(cColor, cColor, cColor, pOpacityPct);

    ///--[Base]
    //--Background.
    AdvImages.rFrame_GridBacking->Draw(pLft, pTop);

    ///--[System]
    //--Empty spots now get represented as blank cards with "Empty Slot" on them.
    if(!mLoadingPacks[tFileSlot])
    {
        //--Empty slot indicator.
        rUseFont->DrawTextArgs(pLft + 118.0f, pTop + 54.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Empty Slot");

        //--File number if this slot were to be occupied.
        rUseFont->DrawTextArgs(pLft + 5.0f, pTop + 3.0f, 0, 1.0f, "File%03i.slf", tFileSlot);
        return;
    }

    ///--[Text]
    //--File number.
    rUseFont->DrawTextArgs(pLft + 5.0f, pTop + 3.0f, 0, 1.0f, mLoadingPacks[tFileSlot]->mFilePathShort);

    //--File note. First, figure out if it needs to be a two-line now.
    char tTimestampA[32];
    char tTimestampB[32];
    float tNoteLen = rUseFont->GetTextWidth(mLoadingPacks[tFileSlot]->mFileName) * 1.0f;
    if(tNoteLen > 220.0f)
    {
        //--Setup.
        int tCharsParsed = 0;
        int tCharsParsedTotal = 0;
        float tCurY = pTop + 75.0f;
        int tStringLen = (int)strlen(mLoadingPacks[tFileSlot]->mFileName);

        //--Loop until the whole note has been parsed out.
        while(tCharsParsedTotal < tStringLen)
        {
            //--Run the subdivide.
            char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &mLoadingPacks[tFileSlot]->mFileName[tCharsParsedTotal], -1, 220.0f, rUseFont, 1.0f);
            rUseFont->DrawText(pLft + 5.0f, tCurY, 0, 1.0f, tDescriptionLine);
            tCurY = tCurY + 13.0f;

            //--Move to the next line.
            tCharsParsedTotal += tCharsParsed;

            //--Clean up.
            free(tDescriptionLine);
        }
    }
    //--One-line.
    else
    {
        rUseFont->DrawText(pLft + 5.0f, pTop + 88.0f, 0, 1.0f, mLoadingPacks[tFileSlot]->mFileName);
    }

    //--Split the timestamp into two parts.
    int tLen = (int)strlen(mLoadingPacks[tFileSlot]->mTimestamp);
    for(int p = 0; p < tLen; p ++)
    {
        if(mLoadingPacks[tFileSlot]->mTimestamp[p] == ' ')
        {
            strcpy(tTimestampB, &mLoadingPacks[tFileSlot]->mTimestamp[p+1]);
            break;
        }
        else
        {
            tTimestampA[p+0] = mLoadingPacks[tFileSlot]->mTimestamp[p];
            tTimestampA[p+1] = '\0';
        }
    }

    //--Timestamp.
    float tTimestampLen = rUseFont->GetTextWidth(tTimestampA) * 1.0f;
    rUseFont->DrawText(pLft + 233.0f - tTimestampLen, pTop + 3.0f, 0, 1.0f, tTimestampA);
    tTimestampLen = rUseFont->GetTextWidth(tTimestampB) * 1.0f;
    rUseFont->DrawText(pLft + 233.0f - tTimestampLen, pTop + 23.0f, 0, 1.0f, tTimestampB);

    ///--[Chapter Completion]
    //--Renders at half size.
    for(int p = 0; p < LOADINGPACK_MAX_CHAPTER; p ++)
    {
        //--Skip empty slots.
        if(!mLoadingPacks[tFileSlot]->mIsChapterComplete[p]) continue;

        //--Render.
        float tXPos = pLft + 233.0f - ((LOADINGPACK_MAX_CHAPTER - p) * 16.0f);
        float tYPos = pTop + 85.0f;
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(0.50f, 0.50f, 1.0f);
        AdvImages.rOverlay_ChapterComplete[p]->Draw();
        glScalef(2.00f, 2.00f, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);
    }

    ///--[Party]
    //--Render all party members. Some might not exist.
    for(int p = 0; p < LOADINGPACK_MAX_PARTY_SIZE; p ++)
    {
        //--Skip empty slots.
        if(!mLoadingPacks[tFileSlot]->rRenderImg[p]) continue;

        //--Positions.
        float cRenderX = pLft - 12.0f + (p * 32.0f);
        float cRenderY = pTop +  9.0f;

        //--Render.
        glTranslatef(cRenderX, cRenderY, 0.0f);
        glScalef(2.0f, 2.0f, 1.0f);
        mLoadingPacks[tFileSlot]->rRenderImg[p]->Draw();
        glScalef(0.5f, 0.5f, 1.0f);
        glTranslatef(-cRenderX, -cRenderY, 0.0f);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
