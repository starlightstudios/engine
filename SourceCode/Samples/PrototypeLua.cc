//--TO-DOs
//--Replace [CLASSNAME] with the name of the class.
//--Replace [CLASS_TYPE_POINTER] with the pointer type of the class, or change how it resolves
//  if the class has complex typing.
//--Add dynamic and static implementations of things
//--Delete these instructions

//--Base
#include "[CLASSNAME].h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void [CLASSNAME]::HookToLuaState(lua_State *pLuaState)
{
    /* [CLASSNAME]_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "[CLASSNAME]_GetProperty", &Hook_[CLASSNAME]_GetProperty);

    /* [CLASSNAME]_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "[CLASSNAME]_SetProperty", &Hook_[CLASSNAME]_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_[CLASSNAME]_GetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    ///--[Dynamic]
    //[CLASSNAME]_GetProperty("Dummy") (1 Integer)

    ///--[ ========= Argument Resolve ========= ]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("[CLASSNAME]_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    if(!DataLibrary::Fetch()->IsActiveValid([CLASS_TYPE_POINTER])) return LuaTypeError("[CLASSNAME]_GetProperty");
    [CLASSNAME] *rObject = ([CLASSNAME] *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("[CLASSNAME]_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_[CLASSNAME]_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    //--[System]
    //[CLASSNAME]_SetProperty("Dummy")

    ///--[ ========= Argument Resolve ========= ]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("[CLASSNAME]_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    if(!DataLibrary::Fetch()->IsActiveValid([CLASS_TYPE_POINTER])) return LuaTypeError("[CLASSNAME]_SetProperty");
    [CLASSNAME] *rAbility = ([CLASSNAME] *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("[CLASSNAME]_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
