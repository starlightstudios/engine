//--Base
#include "TilemapActor.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///======================================= Manipulators ===========================================
void TilemapActor::ActivateAdvancedPathing()
{
    //--Note: Smart and Advanced pathing can't both be on at the same time. Both use the same linked
    //  list to store instructions.
    mIsSmartPatrol = false;
    mIsAdvancedPatrol = true;
    if(!mSmartPatrolInstructions) mSmartPatrolInstructions = new StarLinkedList(true);
}
void TilemapActor::ActivateSmartPathing()
{
    mIsSmartPatrol = true;
    mIsAdvancedPatrol = false;
    if(!mSmartPatrolInstructions) mSmartPatrolInstructions = new StarLinkedList(true);
}
void TilemapActor::SetPatrolToAnyNode(bool pFlag)
{
    mPathToAnyNode = pFlag;
}

///======================================== Core Methods ==========================================
void TilemapActor::DecideNextPathDestination()
{
    ///--[Documentation]
    //--When the actor finishes moving to the node in question, decides the next node to move to.
    //  For standard pathing, this is either the next node, or the first node if we reached the last one.
    //  For smart or advanced pathing, it is possible to path to any node.
    //fprintf(stderr, "Deciding next path action.\n");
    //fprintf(stderr, " Current node: %i\n", mPatrolNodeCurrent);
    //fprintf(stderr, " Total nodes: %i\n", mPatrolNodesTotal);

    ///--[Advanced / Smart Pathing]
    if(mIsAdvancedPatrol || mIsSmartPatrol)
    {
        //--If there is exactly one node, stay at it. This is nominally an error case
        //  but that's on the map designer.
        if(mPatrolNodesTotal == 1)
        {

        }
        //--If this flag is set, we move to a random node instead of the next
        //  node in the list.
        else if(mPathToAnyNode)
        {
            //--Roll a new node. Use the node list minus one to indicate we can't use
            //  the current node.
            int tOldNode = mPatrolNodeCurrent;
            mPatrolNodeCurrent = rand() % (mPatrolNodesTotal-1);

            //--If the roll ended up on the current node, use the last node.
            if(tOldNode == mPatrolNodeCurrent) mPatrolNodeCurrent = mPatrolNodesTotal-1;
        }
        //--Normal case, move to the next node.
        else
        {
            mPatrolNodeCurrent = ((mPatrolNodeCurrent + 1) % mPatrolNodesTotal);
        }
    }
    ///--[Standard Pathing]
    //--Standard pathing can only follow nodes in order.
    else
    {
        mPatrolNodeCurrent = ((mPatrolNodeCurrent + 1) % mPatrolNodesTotal);
    }

    //--Debug.
    //fprintf(stderr, " Next node: %i\n", mPatrolNodeCurrent);

    ///--[Common Behaviors]
    //--If this flag is set, we wait at each patrol node.
    if(mUseStopOnEachNode)
    {
        //--This flag bypasses the use-stop flag. The entity patrols constantly.
        if(mNeverPauses)
        {

        }
        //--Normal case. Stop and look around.
        else
        {
            mAIState = TA_AI_WAIT;
            mWaitTimer = 0;
            mWaitTimerMax = rand() % 240 + 60;
            if(mNeverPauses)
            {
                mWaitTimerMax = 1;
            }
        }
    }
    //--Otherwise, the entity keeps going until a different timer runs out.
    else
    {
        if(!mNeverRefaceOnNode)
        {
            mAIState = TA_AI_TURNTOFACE;
            mWaitTimer = rand() % 30 + 30;
        }
    }
}
