//--Base
#include "StarlightPerlin.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
StarlightPerlin::StarlightPerlin()
{
    ///--[StarlightPerlin]
    //--System
    mIsReady = false;

    //--Storage
    mSeed = 0;
    mFrequency = 0.0f;
    mOctaves = 0;
    mLocalNoise = NULL;
}
StarlightPerlin::~StarlightPerlin()
{
    delete mLocalNoise;
}

///--[Public Statics]
StarlightPerlin *StarlightPerlin::xgStatic = NULL;

///===================================== Property Queries =========================================
float StarlightPerlin::Roll(float pX)
{
    if(!mIsReady) return 0.0f;
    return (float)mLocalNoise->noise(pX);
}
float StarlightPerlin::Roll(float pX, float pY)
{
    if(!mIsReady) return 0.0f;
    return (float)mLocalNoise->noise(pX, pY);
}
float StarlightPerlin::Roll(float pX, float pY, float pZ)
{
    if(!mIsReady) return 0.0f;
    return (float)mLocalNoise->noise(pX, pY, pZ);
}
float StarlightPerlin::RollClamped(float pX)
{
    if(!mIsReady) return 0.0f;
    return ((float)mLocalNoise->noise(pX) * 0.5f) + 0.5f;
}
float StarlightPerlin::RollClamped(float pX, float pY)
{
    if(!mIsReady) return 0.0f;
    return ((float)mLocalNoise->noise(pX, pY) * 0.5f) + 0.5f;
}
float StarlightPerlin::RollClamped(float pX, float pY, float pZ)
{
    if(!mIsReady) return 0.0f;
    return ((float)mLocalNoise->noise(pX, pY, pZ) * 0.5f) + 0.5f;
}

///======================================= Manipulators ===========================================
void StarlightPerlin::SetSeed(uint32_t pSeed)
{
    mSeed = pSeed;
}
void StarlightPerlin::SetFrequency(float pFrequency)
{
    mFrequency = pFrequency;
}
void StarlightPerlin::SetOctaves(int pOctaves)
{
    mOctaves = pOctaves;
}
void StarlightPerlin::Boot()
{
    mIsReady = true;
    delete mLocalNoise;
    mLocalNoise = new siv::PerlinNoise(mSeed);
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void StarlightPerlin::HookToLuaState(lua_State *pLuaState)
{
    /* STPerlin_Quickroll(iSeedValue, fXValue) (1 Float)
       Instantiates a perlin generator and pulses it with the given values. */
    lua_register(pLuaState, "STPerlin_Quickroll", &Hook_STPerlin_Quickroll);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_STPerlin_Quickroll(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //STPerlin_Quickroll(iSeedValue, fXValue) (1 Float)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("STPerlin_Quickroll");

    //--Instantiate.
    StarlightPerlin *tPerlinGenerator = new StarlightPerlin();
    tPerlinGenerator->SetSeed(lua_tointeger(L, 1));
    tPerlinGenerator->SetFrequency(16.0f);
    tPerlinGenerator->SetOctaves(4);
    tPerlinGenerator->Boot();
    float tRoll = tPerlinGenerator->Roll(lua_tonumber(L, 2), 0.55f, 0.55f);
    lua_pushnumber(L, tRoll);
    delete tPerlinGenerator;
    return 1;
}
