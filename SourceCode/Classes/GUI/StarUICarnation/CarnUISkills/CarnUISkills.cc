//--Base
#include "CarnUISkills.h"

//--Classes
#include "AbyssCombat.h"
#include "AdventureMenu.h"
#include "CarnationCombatEntity.h"
#include "CarnUIHealParty.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
CarnUISkills::CarnUISkills()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUISKILLS;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CSKL");

    ///--[Visiblity]
    mVisibilityTimerMax = 1;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== CarnUISkills ======= ]
    ///--[System]
    mCharacterCursor = -1;
    mSkillCursor = -1;

    ///--[Skill Listing]
    mSkillSkip = 0;
    mSkillList = new StarLinkedList(true);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    ///--[Hitboxes]
    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 1106.0f, 105.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollBd", 1106.0f, 163.0f, 36.0f, 460.0f);
    RegisterHitboxWH("ScrollDn", 1106.0f, 623.0f, 36.0f,  58.0f);

    //--Skill hitboxes.
    float cLft = 710.0f;
    float cTop = 120.0f;
    float cHei =  26.0f;
    for(int i = 0; i < cxCarnScrollPage; i ++)
    {
        char tBuf[32];
        sprintf(tBuf, "Skill%02i", i);
        RegisterHitboxWH(tBuf, cLft, cTop + (i * cHei), 450.0f, cHei);
    }

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mSkillSkip;
    nScrollbarPack->mPerPage = cxCarnScrollPage;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxHitbox_ScrollUp;
    nScrollbarPack->mIndexBd = cxHitbox_ScrollBd;
    nScrollbarPack->mIndexDn = cxHitbox_ScrollDn;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;
}
CarnUISkills::~CarnUISkills()
{
    delete mSkillList;
}
void CarnUISkills::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation Skills UI", &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI", &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
char *CarnUISkills::xCharacterBuildScript = NULL;

///===================================== Property Queries =========================================
bool CarnUISkills::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_CARNUISKILLS) return true;
    if(pType == POINTER_TYPE_STARUIPIECE)  return true;
    return (pType == mType);
}

///======================================= Manipulators ===========================================
void CarnUISkills::TakeForeground()
{
    ///--[Documentation]
    //--Called when the object is opened.
    mCharacterCursor = -1;
    ChangeCharacterCursor(xPersistentCharacterCursor);
}
void CarnUISkills::RegisterSkill(const char *pInternalName)
{
    ///--[Documentation]
    //--Registers a skill to the master list given its name. The global skill list needs to be checked
    //  to get its details.
    if(!pInternalName) return;

    //--Duplicate check.
    void *rCheckPtr = mSkillList->GetElementByName(pInternalName);
    if(rCheckPtr) return;

    //--Fast-access pointers.
    AbyssCombat *rAbyCombat = (AbyssCombat *)AdvCombat::Fetch();
    if(!rAbyCombat->IsOfType(POINTER_TYPE_ABYSSCOMBAT)) return;

    ///--[Execution]
    //--Get the path to the ability.
    const char *rCallPath = rAbyCombat->GetPathOfGlobalAbility(pInternalName);
    if(!rCallPath || !strcasecmp(rCallPath, "Null"))
    {
        fprintf(stderr, "CarnUISkills:RegisterSkill() - Warning. Ability %s was not found on the global list.\n", pInternalName);
        return;
    }

    //--Create and register entry.
    CarnSkillEntry *nEntry = (CarnSkillEntry *)starmemoryalloc(sizeof(CarnSkillEntry));
    nEntry->Initialize();
    mSkillList->AddElement(pInternalName, nEntry, &CarnSkillEntry::DeleteThis);

    //--Call path to populate.
    LuaManager::Fetch()->ExecuteLuaFile(rCallPath, 1, "N", (float)ABILITY_UI_CALL);
}
void CarnUISkills::SetSkillProperties(const char *pInternalName, const char *pDisplayName, bool pIsUsable, int pSPCost, const char *pDescription)
{
    ///--[Documentation]
    //--Sets skill properties for the named skill.
    if(!pInternalName) return;

    //--Duplicate check.
    CarnSkillEntry *rCheckPtr = (CarnSkillEntry *)mSkillList->GetElementByName(pInternalName);
    if(!rCheckPtr)
    {
        fprintf(stderr, "CarnUISkills:SetSkillProperties() - Warning. Ability %s was not found on the skill list.\n", pInternalName);
        return;
    }

    ///--[Execution]
    //--Set.
    rCheckPtr->mIsUIUsable = pIsUsable;
    rCheckPtr->mSPCost = pSPCost;
    ResetString(rCheckPtr->mDisplayName, pDisplayName);
    rCheckPtr->mDescription = CreateDescriptionListFromString(pDescription, CarnImages.rFont_Description, 440.0f);
    ReplaceCommonTagsInList(rCheckPtr->mDescription);

    //--Raw description, stores the original string for later subdivision.
    ResetString(rCheckPtr->mDescriptionRaw, pDescription);
}

///======================================= Core Methods ===========================================
void CarnUISkills::RefreshMenuHelp()
{
}
void CarnUISkills::RecomputeCursorPositions()
{
}
void CarnUISkills::ChangeCharacterCursor(int pNewCharacter)
{
    ///--[Documentation]
    //--Called when the character cursor changes, either by opening the UI or clicking the tabs
    //  on the left side to change. Clears and repopulates skills.
    if(pNewCharacter == mCharacterCursor) return;
    if(!xCharacterBuildScript) return;

    //--Fast-access pointers.
    AdvCombat *rAdvCombat;
    AdvCombatEntity *rActiveCharacter;
    if(!(rAdvCombat = AdvCombat::Fetch())) return;
    if(!(rActiveCharacter = rAdvCombat->GetActiveMemberI(pNewCharacter))) return;

    ///--[Execution]
    //--Set, clear.
    mSkillList->ClearList();
    mCharacterCursor = pNewCharacter;

    //--Get the name of the character.
    const char *rCharacterName = rActiveCharacter->GetName();

    //--Call script to repopulate.
    LuaManager::Fetch()->ExecuteLuaFile(xCharacterBuildScript, 1, "S", rCharacterName);

    //--Recompute skip handling.
    ScrollbarPack *rScrollbarPack = (ScrollbarPack *)mHitboxList->GetElementBySlot(0);
    if(rScrollbarPack)
    {
        rScrollbarPack->mMaxSkip = mSkillList->GetListSize() - cxCarnScrollPage;
        if(rScrollbarPack->mMaxSkip < 0) rScrollbarPack->mMaxSkip = 0;
    }
}
void CarnUISkills::ActivateSkill(int pSkillCursor)
{
    ///--[Documentation]
    //--Switches to the HealParty UI given the skill under the cursor.
    if(pSkillCursor < 0 || pSkillCursor >= mSkillList->GetListSize()) return;

    ///--[Execution]
    //--Fetch the HealParty UI.
    CarnUIHealParty *rHealPartyUI = CarnUIHealParty::Fetch();
    if(!rHealPartyUI) { FlagExit(); return; }

    //--Get skill properties.
    CarnSkillEntry *rSkillEntry = (CarnSkillEntry *)mSkillList->GetElementBySlot(pSkillCursor);
    const char *rSkillName = mSkillList->GetNameOfElementBySlot(pSkillCursor);
    if(!rSkillEntry || !rSkillName) return;

    //--If the entry is not usable, stop.
    if(!rSkillEntry->mIsUIUsable) return;

    //--Pass the skill properties.
    rHealPartyUI->SetSkillMode(mCharacterCursor, rSkillName, rSkillEntry->mDescriptionRaw);
    rHealPartyUI->SetExitToSkills();

    //--Order the UI to switch to the Regem UI.
    FlagExit();
    mCodeBackward = CARNMENU_MODE_HEALPARTY + CARNMENU_CHANGEMODE_OFFSET;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUISkills::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ========== Documentation =========== ]
    //--Handles updates when this is the active object.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[ ============== Timers ============== ]
    //--Immediately cap the vis timer.
    mVisibilityTimer = mVisibilityTimerMax;

    ///--[ ============== Update ============== ]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Refresh mouse data.
    GetMouseInfo();
    RunHitboxCheck();

    //--Scrollbar. Updates before clicks.
    if(UpdateScrollbars()) return true;

    ///--[Mouse Movement]
    //--Check if the highlighted skill changed. This changes the description.
    if(mMouseMoved)
    {
        //--Store old cursor.
        int tOldSkillCursor = mSkillCursor;

        //--Check if we're over a skill hitbox.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--If it's in the selection range:
            if(rReplyPack->mIndex >= cxHitbox_SkillBgn && rReplyPack->mIndex <= cxHitbox_SkillEnd)
            {
                mSkillCursor = rReplyPack->mIndex - cxHitbox_SkillBgn + mSkillSkip;
            }

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }

        //--Recheck to make sure the cursor in question is occupied.
        if(mSkillCursor >= mSkillList->GetListSize()) mSkillCursor = -1;

        //--Changed cursor, play SFX.
        if(mSkillCursor != tOldSkillCursor)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Left Click / Select]
    //--Player left-clicks.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        ///--[Changing Modes]
        if(CommonTabClickUpdate()) return true;

        ///--[Changing Characters]
        int tNewCharacterCursor = mCharacterCursor;
        if(CommonCharClickUpdate(tNewCharacterCursor))
        {
            ChangeCharacterCursor(tNewCharacterCursor);
            return true;
        }

        ///--[Skill Activated]
        //--If the skill cursor is not -1, run that.
        if(mSkillCursor != -1)
        {
            ActivateSkill(mSkillCursor);
        }

        return true;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Exits the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    return true;
}
void CarnUISkills::UpdateBackground()
{
    ///--[Timers]
    mVisibilityTimer = 0;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUISkills::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Render character portraits and a list of skills, alongside a description.

    //--Fast-access pointers.
    AdvCombat *rCombat;
    AdvCombatEntity *rAdvEntity;
    if(!(rCombat = AdvCombat::Fetch())) return;
    if(!(rAdvEntity = rCombat->GetActiveMemberI(mCharacterCursor))) return;

    ///--[Character Select]
    //--Tabs.
    RenderCharacterTabs(mCharacterCursor);

    ///--[Static Backing]
    CarnImages.rFrame_Base->Draw();

    //--Common.
    RenderTopTabs(mCodeBackward);
    RenderHexes();

    ///--[Character Information]
    //--Portrait.
    if(rAdvEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY))
    {
        CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rAdvEntity;
        rCarnationEntity->RenderSwitchableAt(196, 62);
    }
    ///--[Name/Stats]
    //--Name.
    CarnImages.rFont_Heading->DrawTextArgs(445.0f, 68.0f, 0, 1.0f, "%s", rAdvEntity->GetDisplayName());

    //--Level.
    CarnImages.rFont_Mainline->DrawTextArgs(658.0f, 83.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Lv. %i", rAdvEntity->GetLevel()+1);

    //--EXP/Needed.
    CarnImages.rFont_Mainline->DrawTextArgs(445.0f, 115.0f, 0, 1.0f, "XP: %i / %i", rAdvEntity->GetXP(), rAdvEntity->GetXP() + rAdvEntity->GetXPToNextLevel());

    ///--[HP/SP Meters]
    //--Render static components.
    CarnImages.rOverlay_MeterFrames->Draw();

    //--HP Values.
    int tHPCur = rAdvEntity->GetHealth();
    int tHPMax = rAdvEntity->GetHealthMax();
    float tHPPct = rAdvEntity->GetHealthPercent();
    CarnImages.rFont_Statistic->DrawTextArgs(480.0f, 159.0f, 0, 1.0f, "%i / %i", tHPCur, tHPMax);
    CarnImages.rOverlay_HPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tHPPct, 1.0f);

    //--SP Values.
    int tSPCur = rAdvEntity->GetMagic();
    int tSPMax = rAdvEntity->GetStatistic(STATS_MPMAX);
    float tSPPct = rAdvEntity->GetMagicPercent();
    CarnImages.rFont_Statistic->DrawTextArgs(480.0f, 220.0f, 0, 1.0f, "%i / %i", tSPCur, tSPMax);
    CarnImages.rOverlay_SPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tSPPct, 1.0f);

    ///--[Ability Listing]
    //--Title.
    CarnImages.rFont_Heading->DrawTextArgs(701.0f, 73.0f, 0, 1.0f, "Skill Listing");

    //--Iterate.
    int i = 0;
    int tRenders = 0;
    CarnSkillEntry *rEntry = (CarnSkillEntry *)mSkillList->PushIterator();
    while(rEntry)
    {
        //--Skip check.
        if(i < mSkillSkip)
        {
            i ++;
            rEntry = (CarnSkillEntry *)mSkillList->AutoIterate();
            continue;
        }

        //--Stop check.
        if(tRenders >= cxCarnScrollPage)
        {
            mSkillList->PopIterator();
            break;
        }

        //--Get hitbox.
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxHitbox_SkillBgn + tRenders);
        if(!rHitbox)
        {
            mSkillList->PopIterator();
            break;
        }

        //--Render.
        CarnImages.rFont_Mainline->DrawTextArgs(rHitbox->mLft, rHitbox->mYCenter, SUGARFONT_AUTOCENTER_Y, 1.0f, rEntry->mDisplayName);

        //--If the skill is usable, render an SP cost. This occurs even if the cost is zero.
        if(rEntry->mIsUIUsable)
        {
            CarnImages.rFont_Mainline->DrawTextArgs(rHitbox->mRgt, rHitbox->mYCenter, SUGARFONT_AUTOCENTER_Y | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i SP", rEntry->mSPCost);
        }

        //--Next.
        i ++;
        tRenders ++;
        rEntry = (CarnSkillEntry *)mSkillList->AutoIterate();
    }

    ///--[Ability Description]
    CarnSkillEntry *rHighlightedSkill = (CarnSkillEntry *)mSkillList->GetElementBySlot(mSkillCursor);
    if(rHighlightedSkill)
    {
        //--Name.
        CarnImages.rFont_Heading->DrawText(210.0f, 480.0f, 0, 1.0f, rHighlightedSkill->mDisplayName);

        //--Description.
        float cHei = CarnImages.rFont_Mainline->GetTextHeight();
        RenderDescription(210.0f, 528.0f, cHei, rHighlightedSkill->mDescription, CarnImages.rFont_Description);
    }
}

///====================================== Pointer Routing =========================================
CarnUISkills *CarnUISkills::Fetch()
{
    ///--[Documentation]
    //--Locates and returns the CarnationMenu version of this class. It is possible this doesn't exist
    //  so error-check the results.
    AdventureMenu *rAdventureMenu = AdventureMenu::Fetch();
    StarUIPiece *rStarUIPiece = rAdventureMenu->RetrieveUI("Skills", POINTER_TYPE_CARNUISKILLS, NULL, "CarnUISkills::Fetch()");
    if(!rStarUIPiece) return NULL;

    //--Dynamic cast.
    CarnUISkills *rSkillsUI = dynamic_cast<CarnUISkills *>(rStarUIPiece);
    return rSkillsUI;
}

///===================================== Static Functions =========================================
void CarnSkillEntry::Initialize()
{
    mIsUIUsable = false;
    mSPCost = 0;
    mDisplayName = NULL;
    mDescriptionRaw = NULL;
    mDescription = NULL;
}
void CarnSkillEntry::DeleteThis(void *pPtr)
{
    //--Cast and check.
    CarnSkillEntry *rPtr = (CarnSkillEntry *)pPtr;
    if(!rPtr) return;

    //--Deallocate.
    free(rPtr->mDisplayName);
    free(rPtr->mDescriptionRaw);
    delete rPtr->mDescription;
    free(rPtr);
}

///======================================== Lua Hooking ===========================================
void CarnUISkills::HookToLuaState(lua_State *pLuaState)
{
    /* [System]
       CarnUISkills_SetProperty("Use On Character Path", sPath) (Static)
       Sets the requested property in the CarnUISkills. */
    lua_register(pLuaState, "CarnUISkills_SetProperty", &Hook_CarnUISkills_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnUISkills_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    //CarnUISkills_SetProperty("Character Build Script", sPath) (Static)

    ///--[Dynamic]
    //--[Labels]
    //CarnUISkills_SetProperty("Register Skill", sInternalName)
    //CarnUISkills_SetProperty("Set Skill Properties", sInternalName, sDisplayName, bIsUsable, iSPCost, sDescription)

    ///--[ ========= Argument Resolve ========= ]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnUISkills_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Statics.
    if(!strcasecmp(rSwitchType, "Character Build Script") && tArgs >= 2)
    {
        ResetString(CarnUISkills::xCharacterBuildScript, lua_tostring(L, 2));
        return 0;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("CarnUISkills_SetProperty", L);

    //--Get the sub-object.
    CarnUISkills *rUIObject = CarnUISkills::Fetch();
    if(!rUIObject) return LuaTypeError("CarnUISkills_SetProperty", L);

    ///--[System]
    //--Registers a new skill.
    if(!strcasecmp(rSwitchType, "Register Skill") && tArgs >= 2)
    {
        rUIObject->RegisterSkill(lua_tostring(L, 2));
    }
    //--Sets properties for a named skill.
    else if(!strcasecmp(rSwitchType, "Set Skill Properties") && tArgs >= 6)
    {
        rUIObject->SetSkillProperties(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4), lua_tointeger(L, 5), lua_tostring(L, 6));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnUISkills_SetProperty", rSwitchType, tArgs);
    }

    //--Success.
    return 0;
}

