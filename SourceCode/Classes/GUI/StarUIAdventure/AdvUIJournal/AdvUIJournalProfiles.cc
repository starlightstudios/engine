//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvUIJournalTypes.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
void AdvUIJournal::SetJournalToProfilesMode()
{
    //--Clear the profiles.
    ClearProfiles();

    //--Variables.
    mCurrentMode = ADVMENU_JOURNAL_MODE_PROFILES;

    //--Run this script to set the profile's properties.
    LuaManager::Fetch()->ExecuteLuaFile(xProfileResolveScript);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIJournal::CreateProfileEntry(const char *pInternalName)
{
    //--Arg check.
    if(!pInternalName) return;

    //--Duplicate check.
    if(mProfileEntries->GetElementByName(pInternalName)) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuJournalProfileEntry *nEntry = (AdvMenuJournalProfileEntry *)starmemoryalloc(sizeof(AdvMenuJournalProfileEntry));
    nEntry->Initialize();
    ResetString(nEntry->mInternalName, pInternalName);
    mProfileEntries->AddElementAsTail(pInternalName, nEntry, &AdvMenuJournalProfileEntry::DeleteThis);
}
void AdvUIJournal::SetProfileDisplayName(const char *pInternalName, const char *pDisplayName)
{
    //--Error check.
    AdvMenuJournalProfileEntry *rEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDisplayName) return;

    //--Set.
    ResetString(rEntry->mDisplayName, pDisplayName);
}
void AdvUIJournal::SetProfilePortrait(const char *pInternalName, const char *pDLPath)
{
    //--Error check.
    AdvMenuJournalProfileEntry *rEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDLPath) return;

    //--Set.
    rEntry->rDisplayImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvUIJournal::SetProfileOffsets(const char *pInternalName, float pX, float pY)
{
    //--Error check.
    AdvMenuJournalProfileEntry *rEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mPortraitOffsetX = pX;
    rEntry->mPortraitOffsetY = pY;
}
void AdvUIJournal::AllocateProfileDescriptionLines(const char *pInternalName, int pCount)
{
    //--Error check.
    AdvMenuJournalProfileEntry *rEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->AllocateDescriptionLines(pCount);
}
void AdvUIJournal::SetProfileDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine)
{
    //--Error check.
    AdvMenuJournalProfileEntry *rEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDescriptionLine) return;

    //--Range check.
    if(pLine < 0 || pLine >= rEntry->mDescriptionLinesTotal) return;

    //--Set.
    ResetString(rEntry->mDescriptionLines[pLine], pDescriptionLine);
}
void AdvUIJournal::SetProfileDescriptionAuto(const char *pInternalName, const char *pDescriptionLine)
{
    ///--[Documentation]
    //--Given a single string, subdivides the line and automatically allocates space based on line lengths.
    if(!pInternalName || !pDescriptionLine) return;

    ///--[Locate Entry]
    AdvMenuJournalProfileEntry *rEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementByName(pInternalName);
    if(!rEntry) return;

    //--Diagnostics.
    //if(!strcasecmp(pInternalName, "Izana"))
    //{
    //    fprintf(stderr, "Length of Izana's description in bytes: %i\n", (int)strlen(pDescriptionLine));
    //    fprintf(stderr, "Length of Izana's description in letters: %i\n", (int)utf8_strlen(pDescriptionLine));
    //}

    ///--[Run Subdivide]
    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pDescriptionLine, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS, NULL, -1.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "AdvUIJournal::SetProfileDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        //--Liberate the string.
        tStringList->SetRandomPointerToHead();
        char *rString = (char *)tStringList->LiberateRandomPointerEntry();
        rEntry->mDescriptionLines[i] = rString;
    }

    //--Clean the list up.
    delete tStringList;
}
void AdvUIJournal::ClearProfiles()
{
    mProfileEntries->ClearList();
}

///======================================= Core Methods ===========================================
void AdvUIJournal::ComputeProfileCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mProfileCursor - mProfileOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mProfileEntries->GetElementBySlot(mProfileCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = AdvImages.rFont_Mainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIJournal::RenderProfilesMode(float pVisAlpha)
{
    ///--[Profile Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mProfileEntries, mProfileOffset, mProfileCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the image and information.
    AdvMenuJournalProfileEntry *rActiveEntry = (AdvMenuJournalProfileEntry *)mProfileEntries->GetElementBySlot(mProfileCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Portrait]
        //--Render the portrait behind a stencil.
        if(rActiveEntry->rDisplayImg)
        {
            //--Position.
            float cScale = 0.50f;
            float cScaleInv = 1.0f / cScale;
            float cXPos = 515.0f + rActiveEntry->mPortraitOffsetX;
            float cYPos = 113.0f + rActiveEntry->mPortraitOffsetY;

            //--Translate, scale.
            glTranslatef(cXPos, cYPos, 0.0f);
            glScalef(cScale, cScale, 1.0f);

            //--Render.
            rActiveEntry->rDisplayImg->LoadDataFromSLF();
            rActiveEntry->rDisplayImg->Draw();

            //--Clean.
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-cXPos, -cYPos, 0.0f);
        }

        ///--[Static Parts]
        //--Render all static parts over the portrait.
        AdvImages.rProfilesNameBanner->Draw();

        ///--[Name, Description]
        //--Name.
        float cNameX = 689.0f;
        float cNameY = 487.0f;
        AdvImages.rFont_Header->DrawTextArgs(cNameX, cNameY, SUGARFONT_AUTOCENTER_X, 1.0f, rActiveEntry->mDisplayName);

        //--Description.
        float cDescriptionX1 = 866.0f;
        float cDescriptionX2 = 525.0f;
        float cDescriptionY = 120.0f;
        float cDescriptionH = AdvImages.rFont_Mainline->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            if(i < 21)
                AdvImages.rFont_Mainline->DrawTextArgs(cDescriptionX1, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            else
                AdvImages.rFont_Mainline->DrawTextArgs(cDescriptionX2, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
