//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "PeakFreaksPlayer.h"
#include "PeakFreaksTitle.h"
#include "TileLayer.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"

///======================================== Definitions ===========================================
//--Counts
#define COUNTDOWN_PIECES 3

//--Timings
#define BANNER_TPF 3.0f
#define ALIEN_FLYOFF_TICKS 25
#define GO_TICKS 25

//--Start time Offsets
#define COUNTDOWN_START 45
#define COUNTDOWN_OFFSET 15
#define CNT321_OFFSET 45
#define PANNING_HOLD 10

//--Derived
#define GO_START (COUNTDOWN_START + (COUNTDOWN_PIECES * COUNTDOWN_OFFSET))
#define TRUE_START (COUNTDOWN_START + (COUNTDOWN_PIECES * CNT321_OFFSET))
#define STOP_PAN (GO_START - PANNING_HOLD)
#define STOP_TICKS (GO_START + GO_TICKS)

///======================================= Manipulators ===========================================
void PeakFreaksLevel::ClearOpening()
{
    mIsOpening = false;
    AudioManager::Fetch()->PlayMusic("Stage");
}

///========================================== Update ==============================================
void PeakFreaksLevel::UpdateOpening()
{
    ///--[Documentation and Setup]
    //--Handles opening sequence timers.
    mOpeningTimer ++;
    mOverallOpenTimer ++;

    ///--[Camera]
    //--Reset to default.
    mOpeningCameraX = 0.0f;
    mOpeningCameraY = 0.0f;
    ZonePack *rVictoryZero = (ZonePack *)mVictoryZones->GetElementBySlot(0);
    if(rVictoryZero && mPlayer)
    {
        ///--[Victory Position]
        float cVictoryX = rVictoryZero->mDim.mLft;
        float cVictoryY = rVictoryZero->mDim.mTop;
        if(mPrevOpeningX != -1.0f) cVictoryX = mPrevOpeningX;
        if(mPrevOpeningY != -1.0f) cVictoryY = mPrevOpeningY;

        ///--[Player Position]
        float cPlayerWorldX = mPlayer->GetWorldX() * TileLayer::cxSizePerTile;
        float cPlayerWorldY = mPlayer->GetWorldY() * TileLayer::cxSizePerTile;

        //--If there is an entry on the opening list, use that instead.
        Point2DF *rPoint = (Point2DF *)mOpeningPositions->GetElementBySlot(0);
        if(rPoint)
        {
            cPlayerWorldX = rPoint->mX;
            cPlayerWorldY = rPoint->mY;
        }

        ///--[Camera Offset]
        //--Compute the expected size of the camera's FOV, and where the player should appear.
        //  The player is not centered due to the UI.
        float cCameraXAtOne = VIRTUAL_CANVAS_X / mCurrentScale;
        float cCameraYAtOne = VIRTUAL_CANVAS_Y / mCurrentScale;
        float cXPosTarget = 0.35f;
        float cYPosTarget = 0.40f;

        //--Camera matches the pixel position with an offset.
        float cCameraOffX = cCameraXAtOne * cXPosTarget * -1.0f;
        float cCameraOffY = cCameraYAtOne * cYPosTarget * -1.0f;

        ///--[Position Resolve]
        //--Camera locks on goal point.
        if(mOpeningTimer < COUNTDOWN_START)
        {
            mOpeningCameraX = cVictoryX + cCameraOffX;
            mOpeningCameraY = cVictoryY + cCameraOffY;
        }
        //--Camera pans from goal to starting.
        else if(mOpeningTimer >= COUNTDOWN_START && mOpeningTimer < STOP_PAN)
        {
            //--Get percentage.
            float tPct = EasingFunction::Linear(mOpeningTimer - COUNTDOWN_START, STOP_PAN - COUNTDOWN_START);
            float cStartX = cVictoryX;
            float cStartY = cVictoryY;
            float cEndX = cPlayerWorldX;
            float cEndY = cPlayerWorldY;

            //--Scroll.
            mOpeningCameraX = (((cEndX - cStartX) * tPct) + cStartX) + cCameraOffX;
            mOpeningCameraY = (((cEndY - cStartY) * tPct) + cStartY) + cCameraOffY;
        }
        //--Camera holds on player position.
        else
        {
            //--On the 0th tick of STOP_PAN, if there is a spot on the opening list, clear it and move the timer back.
            if(mOpeningTimer == STOP_PAN && mOpeningPositions->GetListSize() > 0)
            {
                mPrevOpeningX = cPlayerWorldX;
                mPrevOpeningY = cPlayerWorldY;
                mOpeningPositions->RemoveElementI(0);
                mOpeningTimer = COUNTDOWN_START;
            }

            //--Lock.
            mOpeningCameraX = cPlayerWorldX + cCameraOffX;
            mOpeningCameraY = cPlayerWorldY + cCameraOffY;
        }
    }

    //--Ending.
    if(mOpeningTimer >= STOP_TICKS)
    {
        //--Flag.
        mIsOpening = false;

        //--Normal and Intermediate Music:
        if(xDifficulty < PTTITLE_DIFFICULTY_IMPOSSIBLE)
            AudioManager::Fetch()->PlayMusic("Stage");
        else
            AudioManager::Fetch()->PlayMusic("HardMode");
    }
}

///========================================== Drawing =============================================
void PeakFreaksLevel::RenderOpening()
{
    ///--[Documentation and Setup]
    //--Renders the opening sequence on top of the world.
    if(mOpeningTimer >= STOP_TICKS) return;

    //--Variables.
    int tAlienPhase = -1;

    ///--[Banner Sequence]
    if(mOpeningTimer < GO_START)
    {
        //--Resolve frame.
        int tBannerFrame = (int)((mOverallOpenTimer - CNT321_OFFSET + 90) / (float)BANNER_TPF);
        if(tBannerFrame >= 0)
        {
            if(tBannerFrame >= PFLEVEL_OPENING_BANNER_FRAMES) tBannerFrame = PFLEVEL_OPENING_BANNER_FRAMES - 1;
            Images.Data.rOpeningBanner[tBannerFrame]->Draw(0, 480);
        }
    }

    ///--[Countdown Sequence]
    {
        //--For each countdown piece, render it using a timing offset. Only the first three pieces do this,
        //  "GO!" is handled on its own.
        for(int i = 0; i < COUNTDOWN_PIECES; i ++)
        {
            //--Resolve the local timer for this entry.
            int tLocalTimer = mOverallOpenTimer - COUNTDOWN_START - (CNT321_OFFSET * i) + 90;
            if(tLocalTimer < 0) break;
            if(tLocalTimer >= 45) continue;

            //--SFX.
            if(tLocalTimer == 0)
            {
                //--Count down.
                if(i == 0)
                    AudioManager::Fetch()->PlaySound("PF Announce Opening3");
                else if(i == 1)
                    AudioManager::Fetch()->PlaySound("PF Announce Opening2");
                else
                    AudioManager::Fetch()->PlaySound("PF Announce Opening1");

                //--Always play the beep.
                AudioManager::Fetch()->PlaySound("PF Announce CountBeep");
            }

            //--If this part renders, set it as the alien phase.
            tAlienPhase = i;

            //--Resolve alpha.
            float cAlpha = EasingFunction::QuadraticIn(tLocalTimer, 30);
            float cSize = 1.50f - (EasingFunction::QuadraticIn(tLocalTimer, 30) * 0.50f);

            //--Central position.
            float cCenterX = VIRTUAL_CANVAS_X * 0.50f;
            float cCenterY = 530.0f;
            glTranslatef(cCenterX, cCenterY, 0.0f);

            //--Color.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

            //--Sizing.
            glScalef(cSize, cSize, 1.0f);

            //--Render.
            SugarBitmap *rBmp = Images.Data.rOpeningCountdown[i];
            rBmp->Draw(rBmp->GetTrueWidth() * -0.50f, rBmp->GetTrueHeight() * -0.50f);

            //--Clean.
            glScalef(1.0f / cSize, 1.0f / cSize, 1.0f);
            StarlightColor::ClearMixer();
            glTranslatef(-cCenterX, -cCenterY, 0.0f);
        }
    }

    ///--[GO!]
    if(mOverallOpenTimer >= GO_START)
    {
        //--If this is rendering, set it as the alien phase.
        tAlienPhase = 3;

        //--Local timer.
        int tLocalTimer = mOverallOpenTimer - GO_START;
        if(tLocalTimer == 0)
        {
            AudioManager::Fetch()->PlaySound("PF Announce LetsGo");
            AudioManager::Fetch()->PlaySound("PF Announce CountHorn");
        }

        //--Resolve alpha.
        float cAlpha = 1.0f - EasingFunction::QuadraticIn(tLocalTimer, GO_TICKS);
        float cSize = 1.0f + (EasingFunction::QuadraticIn(tLocalTimer, GO_TICKS) * 10.00f);

        //--Central position.
        float cCenterX = VIRTUAL_CANVAS_X * 0.50f;
        float cCenterY = 530.0f;
        glTranslatef(cCenterX, cCenterY, 0.0f);

        //--Color.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Sizing.
        glScalef(cSize, cSize, 1.0f);

        //--Render.
        SugarBitmap *rBmp = Images.Data.rOpeningCountdown[3];
        rBmp->Draw(rBmp->GetTrueWidth() * -0.50f, rBmp->GetTrueHeight() * -0.50f);

        //--Clean.
        glScalef(1.0f / cSize, 1.0f / cSize, 1.0f);
        StarlightColor::ClearMixer();
        glTranslatef(-cCenterX, -cCenterY, 0.0f);
    }

    ///--[That Damn Alien]
    //--The alien renders over the countdown timer and to its right. It changes properties based on
    //  the countdown render phase.
    if(tAlienPhase == 0)
    {
        Images.Data.rOpeningAlien[2]->Draw(791.0f, 333.0f);
    }
    else if(tAlienPhase == 1)
    {
        Images.Data.rOpeningAlien[1]->Draw(791.0f, 333.0f);
    }
    else if(tAlienPhase == 2)
    {
        Images.Data.rOpeningAlien[0]->Draw(791.0f, 333.0f);
    }
    //--Flying offscreen.
    else if(tAlienPhase == 3)
    {
        //--Completion rate.
        int tLocalTimer = mOpeningTimer - GO_START;
        float cPct = EasingFunction::QuadraticIn(tLocalTimer, ALIEN_FLYOFF_TICKS);

        //--Resolve rotation.
        float cAngle = 360.0f * 2.0f * cPct;

        //--Resolve position.
        float cXPos = 791.0f + (VIRTUAL_CANVAS_X * 0.50f * cPct);
        float cYPos = 333.0f;
        glTranslatef(cXPos, cYPos, 0.0f);

        //--Center, rotate.
        SugarBitmap *rBmp = Images.Data.rOpeningAlien[3];
        float cRotX = rBmp->GetTrueWidth()  * 0.50f;
        float cRotY = rBmp->GetTrueHeight() * 0.50f;
        glTranslatef(cRotX, cRotY, 0.0f);
        glRotatef(cAngle, 0.0f, 0.0f, 1.0f);
        glTranslatef(-cRotX, -cRotY, 0.0f);

        //--Render.
        rBmp->Draw();

        //--Clean.
        glTranslatef(cRotX, cRotY, 0.0f);
        glRotatef(-cAngle, 0.0f, 0.0f, 1.0f);
        glTranslatef(-cRotX, -cRotY, 0.0f);
        glTranslatef(-cXPos, -cYPos, 0.0f);
    }
}
