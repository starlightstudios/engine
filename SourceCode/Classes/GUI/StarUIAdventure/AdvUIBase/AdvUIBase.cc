//--Base
#include "AdvUIBase.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureMenu.h"
#include "AdvUIDoctor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
AdvUIBase::AdvUIBase()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "ABAS");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = (VIRTUAL_CANVAS_X * 0.50f);
    mGridRenderCenterY  = (VIRTUAL_CANVAS_Y * 0.35f);
    mGridRenderSpacingX = (VIRTUAL_CANVAS_X * 0.08f);
    mGridRenderSpacingY = (VIRTUAL_CANVAS_X * 0.08f);

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ====== AdvUIBase ======= ]
    ///--[Variables]
    //--System
    mIsFirstTick = true;
    mHasBuiltGrid = false;
    mDisplaySkillAlt = false;
    mDontShowHighlight = true;

    //--Cursor
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Strings
    mShowHelpString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Strings
    AllocateHelpStrings(cxAdvBaseHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIBase::~AdvUIBase()
{
    delete mShowHelpString;
}
void AdvUIBase::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Base UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI", &HelpImages, sizeof(HelpImages));

    //--White Pixel override. If certain assets fail to resolve, they are replaced with a valid but
    //  unused white pixel. Used for override games.
    StarBitmap *rWhitePixel = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/System/System/WhitePixel");
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        if(!AdvImages.rAdamantiteImg[i]) AdvImages.rAdamantiteImg[i] = rWhitePixel;
    }
    for(int i = 0; i < CATALYST_TOTAL; i ++)
    {
        if(!AdvImages.rCatalystImg[i]) AdvImages.rCatalystImg[i] = rWhitePixel;
    }

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = AdvImages.rMainlineFont;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIBase::SetCursorByBackwardsCode(int pMode)
{
    ///--[Documentation]
    //--Given a code of the series AM_MODE_BASE defined in AdvUIBase.h, resolves the menu position
    //  of the entry matching that code. Doctor mode, for example, is AM_MODE_DOCTOR (8) but is in
    //  position AM_MAIN_DOCTOR (2).
    //--It is not necessarily true for any derived UI that all buttons pair with a mode, or that
    //  all modes pair with a button. If something is unhandled, an error is printed.

    ///--[Normal Cases]
    //--Base mode, sets to whatever the 0th entry is.
    if(pMode == AM_MODE_BASE)
    {
        SetCursor(0);
    }
    //--Other modes:
    else if(pMode == AM_MODE_ITEMS)
    {
        SetCursor(AM_MAIN_ITEMS);
    }
    else if(pMode == AM_MODE_EQUIP)
    {
        SetCursor(AM_MAIN_EQUIPMENT);
    }
    else if(pMode == AM_MODE_STATUS)
    {
        SetCursor(AM_MAIN_STATUS);
    }
    else if(pMode == AM_MODE_SKILLS)
    {
        SetCursor(AM_MAIN_SKILLS);
    }
    else if(pMode == AM_MODE_MAP)
    {
        SetCursor(AM_MAIN_MAP);
    }
    else if(pMode == AM_MODE_OPTIONS)
    {
        SetCursor(AM_MAIN_OPTIONS);
    }
    else if(pMode == AM_MODE_QUIT)
    {
        SetCursor(AM_MAIN_QUIT);
    }
    else if(pMode == AM_MODE_DOCTOR)
    {
        SetCursor(AM_MAIN_DOCTOR);
    }
    else if(pMode == AM_MODE_JOURNAL)
    {
        SetCursor(AM_MAIN_JOURNAL);
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("AdventureMenu:SetToMainMenuByCode() - Warning. Mode %i was unhandled.\n", pMode);
    }
}
void AdvUIBase::SetCursor(int pCursor)
{
    ///--[Documentation]
    //--Sets the cursor, implicitly range-capping it based on the grid size.
    SetCursorByCode(pCursor);
}
void AdvUIBase::TakeForeground()
{
    ///--[ ======= Variables ======== ]
    //--Flip this flag on so the UI doesn't immediately close, as the key to open it from the AdventureLevel
    //  is the same as the close key.
    mIsFirstTick = true;

    ///--[ ====== Construction ====== ]
    //--On the first call, build the menu grid. Unlike a lot of other grids, this one only needs to be built
    //  once as the layout is not variable.
    if(!mHasBuiltGrid)
    {
        ///--[Basic Setup]
        //--Construct the grid and set its positions.
        mHasBuiltGrid = true;
        AllocateGrid(3, 3);
        ProcessGrid();

        ///--[Components]
        //--Populate the grid.
        QuicksetGridEntry(0, 0, AM_MAIN_JOURNAL,   "Journal",   AdvImages.rIcons[AM_MAIN_JOURNAL]);
        QuicksetGridEntry(1, 0, AM_MAIN_STATUS,    "Status",    AdvImages.rIcons[AM_MAIN_STATUS]);
        QuicksetGridEntry(2, 0, AM_MAIN_OPTIONS,   "Options",   AdvImages.rIcons[AM_MAIN_OPTIONS]);
        QuicksetGridEntry(0, 1, AM_MAIN_SKILLS,    "Skills",    AdvImages.rIcons[AM_MAIN_SKILLS]);
        QuicksetGridEntry(1, 1, AM_MAIN_EQUIPMENT, "Equipment", AdvImages.rIcons[AM_MAIN_EQUIPMENT]);
        QuicksetGridEntry(2, 1, AM_MAIN_ITEMS,     "Items",     AdvImages.rIcons[AM_MAIN_ITEMS]);
        QuicksetGridEntry(0, 2, AM_MAIN_QUIT,      "Quit",      AdvImages.rIcons[AM_MAIN_QUIT]);
        QuicksetGridEntry(1, 2, AM_MAIN_DOCTOR,    "Doctor",    AdvImages.rIcons[AM_MAIN_DOCTOR]);
        QuicksetGridEntry(2, 2, AM_MAIN_MAP,       "Map",       AdvImages.rIcons[AM_MAIN_MAP]);
    }

    //--Cursor.
    SetCursorByCode(AM_MAIN_EQUIPMENT);
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    ///--[Party]
    //--Run across the party and recompute stats. This makes sure their health values line up. Also tally
    //  JP values.
    AdvCombat *rCombat = AdvCombat::Fetch();

    //--Variables.
    mDisplaySkillAlt = false;
    int tJPTally = 0;
    int tPartySize = rCombat->GetActivePartyCount();

    //--Iterate.
    for(int i = 0; i < tPartySize; i ++)
    {
        //--Error check.
        AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
        if(!rCharacter) continue;

        //--Recheck stats.
        rCharacter->RefreshStatsForUI();

        //--Sum JP.
        tJPTally = tJPTally + rCharacter->GetTotalJP();
    }

    //--If the JP tally exceeds the previous tally by 500, switch the icons.
    SysVar *rTallyVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/CrossChapter/SkillsTracker/iJPTally");
    if(rTallyVar && tJPTally - 500 > rTallyVar->mNumeric)
    {
        mDisplaySkillAlt = true;
    }
}
void AdvUIBase::QuicksetGridEntry(int pX, int pY, int pCode, const char *pName, StarBitmap *pImagePtr)
{
    ///--[Documentation]
    //--Same as the base function, but manually forces the sizes to 96x96.

    ///--[Resolve]
    //--Get and check the package. If it doesn't exist, print a warning.
    GridPackage *rPackage = GetPackageAt(pX, pY);
    if(!rPackage)
    {
        fprintf(stderr, "AdvUIGrid:QuicksetGridEntry() - Warning. Attempted to set package %i %i to %s, but package was not found.\n", pX, pY, pName);
        return;
    }

    ///--[Set]
    //--Basic variables.
    rPackage->mCode = pCode;
    strcpy(rPackage->mLocalName, pName);
    rPackage->mAlignments.Initialize();
    rPackage->mAlignments.rImage = pImagePtr;

    //--The image sizes default to the image's width and height, assuming it exists.
    if(pImagePtr)
    {
        rPackage->mAlignments.mImageW = 96.0f;
        rPackage->mAlignments.mImageH = 96.0f;
    }
}

///======================================= Core Methods ===========================================
void AdvUIBase::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Set Lines]
    //--Common lines.
    SetHelpString(i, "This is the primary menu. Select an option with [IMG0]. Press [IMG1] to return to the game.", 2, "Activate", "Cancel");
    SetHelpString(i, "This menu allows you to see your party statistics, edit your equipment, change skills, ");
    SetHelpString(i, "heal yourself with the doctor bag, and edit game options.");
    SetHelpString(i, "");
    SetHelpString(i, "Your adamantite is visible on the right. These materials are used for upgrading gems.");
    SetHelpString(i, "Your catalysts are also on the right. These special items increase your party's stats,");
    SetHelpString(i, "and persist between chapters. Finding enough catalysts gives you a stat bonus. You");
    SetHelpString(i, "can see how many are left to find in this chapter or segment. Some chapters are split");
    SetHelpString(i, "into multiple segments. You will get a warning before changing chapter segments if ");
    SetHelpString(i, "you'd like to find the remaining catalysts.");
    SetHelpString(i, "");
    SetHelpString(i, "For more information, see the Journal's glossary section.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void AdvUIBase::RecomputeCursorPositions()
{
    RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, 1.0f);
}
void AdvUIBase::ResetBuiltGridFlag()
{
    mHasBuiltGrid = false;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIBase::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        mVisibilityTimer ++;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--Standard Timers.
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks, mIsShowingHelp);

    //--Grid timers.
    UpdateGridTimers();

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Set this flag.
    mDontShowHighlight = false;

    ///--[ ======== Update Intercepts ========= ]
    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[ =========== Update Call ============ ]
    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return true;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get what option is under the cursor.
        int tGridOption = GetGridCodeAtCursor();

        //--Mode handlers.
        if(tGridOption == AM_MAIN_ITEMS)
        {
            mCodeBackward = AM_MODE_ITEMS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_EQUIPMENT)
        {
            mCodeBackward = AM_MODE_EQUIP;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_SKILLS)
        {
            mCodeBackward = AM_MODE_SKILLS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_STATUS)
        {
            mCodeBackward = AM_MODE_STATUS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_DOCTOR)
        {
            mCodeBackward = AM_MODE_DOCTOR;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_MAP)
        {
            mCodeBackward = AM_MODE_MAP;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_OPTIONS)
        {
            mCodeBackward = AM_MODE_OPTIONS;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_JOURNAL)
        {
            mCodeBackward = AM_MODE_JOURNAL;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(tGridOption == AM_MAIN_QUIT)
        {
            mCodeBackward = AM_MODE_QUIT;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel") && !mIsFirstTick)
    {
        mCodeBackward = AM_MODE_NONE;
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update. If we passed the cancel check, unset the first-tick flag.
    mIsFirstTick = false;
    return true;
}
void AdvUIBase::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;

    //--Run timers.
    mVisibilityTimer --;
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Unset this flag.
    mDontShowHighlight = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIBase::RenderForeground(float pVisAlpha)
{
    ///--[Documentation]
    //--Same as the base version, but the visiblity timer also allows the Doctor UI's static vis timer
    //  to allow rendering.

    ///--[Visibility Checks]
    //--Images failed to resolve. Do nothing.
    if(!mImagesReady) return;

    //--Caller set the visibility at zero, do nothing.
    if(pVisAlpha <= 0.0f) return;

    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1 && AdvUIDoctor::xDoctorVisTimer < 1) return;

    //--Re-resolve if we should fade or translate. The option can theoretically change at any time.
    mUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Call Pieces Rendering]
    //--Compute how on-screen this object is by calculating the alpha from the vis timer. Multiply
    //  with the provided alpha so the host doesn't fade differently from sub-components.
    float cLocalAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)mVisibilityTimerMax) * pVisAlpha;

    //--Call subroutine. Can be overriden.
    RenderPieces(cLocalAlpha);

    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUIBase::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Rendering of the base UI. Mostly follows the standard, however the bottom of the UI that shows
    //  the player's party will continue to render even if this object is offscreen if the Doctor UI
    //  needs it to, which is handled via a static timer.
    StarlightColor::ClearMixer();

    ///--[Timer]
    //--Get the static timer and compute a special alpha value for the bottom UI.
    float cBotAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer + AdvUIDoctor::xDoctorVisTimer, (float)ADV_MENU_VIS_TICKS);

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(cBotAlpha);

    ///--[Render Grid]
    //--A grid in the middle indicates the player's selected by both rendering a highlight box over it,
    //  and slightly increasing the icon size.
    RenderGrid(pVisAlpha);

    //--Render the cursor.
    if(!mDontShowHighlight) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rHighlight);

    //--Render the name of the selected entry.
    RenderGridEntryName(GetPackageAtCursor(), pVisAlpha);

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUIBase::RenderLft(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders objects on the left side. This is the catalyst frame.
    float cColorAlpha = AutoPieceOpen(DIR_LEFT, pVisAlpha);

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Static Parts]
    //--Static parts.
    AdvImages.rCatalystFrame->Draw();
    AdvImages.rCatalystDetail->Draw();
    AdvImages.rCatalystHeader->Draw();

    ///--[Catalyst Information]
    //--Title.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rHeadingFont->DrawText(149.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Catalysts");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Icons. Render at double size.
    float cPropIconX =   45.0f;
    float cPropIconY =   77.0f;
    float cPropIconW =   46.0f * 0.50f;
    float cScale = 2.0f;
    float cScaleInv = 1.0f / cScale;
    glTranslatef(cPropIconX, cPropIconY, 0.0f);
    glScalef(cScale, cScale, 1.0f);
    for(int i = 0; i < CATALYST_TOTAL; i ++)
    {
        AdvImages.rCatalystImg[i]->Draw(cPropIconW * i, 0.0f);
    }

    //--Clean.
    glScalef(cScaleInv, cScaleInv, 1.0f);
    glTranslatef(cPropIconX * -1.0f, cPropIconY * -1.0f, 0.0f);

    //--Constants
    float cValCnt = 64.0f;
    cPropIconW = cPropIconW * 2.0f; //Doubled because no longer at double scale.

    //--Values loop.
    for(int i = 0; i < CATALYST_TOTAL; i ++)
    {
        //--How many found in this area.
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * i), 127.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%i/%i", rInventory->GetCatalystLocalCur(i), rInventory->GetCatalystLocalMax(i));

        //--Totals.
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * i), 167.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", rInventory->GetCatalystCount(i));
    }

    ///--[Catalyst Bonuses]
    //--Bonuses. Must be done individually.
    if(rInventory->ComputeCatalystBonus(CATALYST_HEALTH) > 0)
    {
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 0.0f), 207.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%+i", rInventory->ComputeCatalystBonus(CATALYST_HEALTH));
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 0.0f), 224.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "HP");
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_ATTACK) > 0)
    {
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 1.0f), 207.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%+i", rInventory->ComputeCatalystBonus(CATALYST_ATTACK));
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 1.0f), 224.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Atk");
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_INITIATIVE) > 0)
    {
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 2.0f), 207.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%+i", rInventory->ComputeCatalystBonus(CATALYST_INITIATIVE));
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 2.0f), 224.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Ini");
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_DODGE) > 0)
    {
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 3.0f), 207.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%+i", rInventory->ComputeCatalystBonus(CATALYST_DODGE));
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 3.0f), 224.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Evd");
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_ACCURACY) > 0)
    {
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 4.0f), 207.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%+i", rInventory->ComputeCatalystBonus(CATALYST_ACCURACY));
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 4.0f), 224.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Acc");
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_SKILL) > 0)
    {
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 5.0f), 207.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%+i", rInventory->ComputeCatalystBonus(CATALYST_SKILL));
        AdvImages.rStatisticFont->DrawTextArgs(cValCnt + (cPropIconW * 5.0f), 224.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Skl");
    }

    ///--[Clean]
    AutoPieceClose();
}
void AdvUIBase::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the header at the top of the screen.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    AdvImages.rHeader->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rDoubleHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Main Menu");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rMainlineFont);

    ///--[Clean]
    AutoPieceClose();
}
void AdvUIBase::RenderRgt(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders inventory information, specifically the adamantite and money counts.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Static Parts]
    AdvImages.rAdamantiteFrame->Draw();
    AdvImages.rAdamantiteDetail->Draw();
    AdvImages.rAdamantiteHeader->Draw();
    AdvImages.rPlatinaFrame->Draw();

    ///--[Adamantite Information]
    //--Title.
    mColorHeading.SetAsMixerAlpha(pVisAlpha);
    AdvImages.rHeadingFont->DrawText(1198.0f, 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Adamantite");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

    //--Icons and Names
    float cAdamantiteIconX = 1054.0f;
    float cAdamantiteTextX = 1076.0f;
    float cAdamantiteTextR = 1355.0f;
    float cAdamantiteIconY =   77.0f;
    float cAdamantiteIconH =   22.0f;
    int cFlagsL = 0;
    int cFlagsR = SUGARFONT_RIGHTALIGN_X;
    AdvImages.rAdamantiteImg[0]->Draw     (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 0.0f) + 0.0f);
    AdvImages.rStatisticFont->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 0.0f) - 2.0f, cFlagsL, 1.0f, "Powder");
    AdvImages.rStatisticFont->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 0.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_POWDER));
    AdvImages.rAdamantiteImg[1]->Draw     (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 1.0f) + 0.0f);
    AdvImages.rStatisticFont->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 1.0f) - 2.0f, cFlagsL, 1.0f, "Flakes");
    AdvImages.rStatisticFont->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 1.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_FLAKES));
    AdvImages.rAdamantiteImg[2]->Draw     (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 2.0f) + 0.0f);
    AdvImages.rStatisticFont->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 2.0f) - 2.0f, cFlagsL, 1.0f, "Shards");
    AdvImages.rStatisticFont->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 2.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_SHARD));
    AdvImages.rAdamantiteImg[3]->Draw     (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 3.0f) + 0.0f);
    AdvImages.rStatisticFont->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 3.0f) - 2.0f, cFlagsL, 1.0f, "Pieces");
    AdvImages.rStatisticFont->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 3.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_PIECE));
    AdvImages.rAdamantiteImg[4]->Draw     (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 4.0f) + 0.0f);
    AdvImages.rStatisticFont->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 4.0f) - 2.0f, cFlagsL, 1.0f, "Chunks");
    AdvImages.rStatisticFont->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 4.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_CHUNK));
    AdvImages.rAdamantiteImg[5]->Draw     (cAdamantiteIconX, cAdamantiteIconY + (cAdamantiteIconH * 5.0f) + 0.0f);
    AdvImages.rStatisticFont->DrawText    (cAdamantiteTextX, cAdamantiteIconY + (cAdamantiteIconH * 5.0f) - 2.0f, cFlagsL, 1.0f, "Ore");
    AdvImages.rStatisticFont->DrawTextArgs(cAdamantiteTextR, cAdamantiteIconY + (cAdamantiteIconH * 5.0f) - 2.0f, cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_ORE));

    ///--[Platina Frame]
    //--Title.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rHeadingFont->DrawText(1060.0f, 253.0f, 0, 1.0f, "Platina: ");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Actual value:
    AdvImages.rHeadingFont->DrawTextArgs(1356.0f, 253.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rInventory->GetPlatina());

    ///--[Clean]
    AutoPieceClose();
}
void AdvUIBase::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Execution]
    //--Overwrite the mask with the footer.
    glEnable(GL_STENCIL_TEST);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, AM_STENCIL_FOOTER, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    AdvImages.rFooter->Draw();
    glDisable(GL_STENCIL_TEST);

    //--Party rendering.
    int tPartySize = AdvCombat::Fetch()->GetActivePartyCount();
    for(int i = 0; i < tPartySize; i ++)
    {
        RenderMainCharacter(i, tPartySize);
    }

    ///--[Clean]
    glDisable(GL_STENCIL_TEST);
    AutoPieceClose();
}
void AdvUIBase::RenderMainCharacter(int pSlot, int pPartySize)
{
    ///--[Documentation]
    //--Given a character slot, renders that character and their information. If there is no character in that party slot,
    //  does nothing.
    //--This UI is designed for exactly 4 character slots. In theory the code could just keep expanding to the left. Characters
    //  Also render in the rightmost slots possible based on the party size.
    //--Note that stencilling is expected to be on for the character but not the rest of the UI.
    if(pSlot < 0 || pSlot >= 4 || pPartySize < 1) return;

    //--Check if there is a character in this slot.
    AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(pSlot);
    if(!rCharacter) return;

    ///--[Position]
    //--Translate based on the party ordering. All of the parts are designed for the leftmost slot
    //  and must be translated over to the right.
    float cSpacingX = 260.0f;
    float tOffsetX = (pSlot * cSpacingX);
    glTranslatef(tOffsetX, 0.0f, 0.0f);

    /*
    float cSpacingX = 260.0f;
    float tOffsetX = 3.0f * cSpacingX;//A zero-size party renders right aligned.
    tOffsetX = tOffsetX - ((pPartySize-1) * cSpacingX);//Shift to the left for each party member total.
    tOffsetX = tOffsetX + (pSlot * cSpacingX);//Shift to the right to reach the slot.
    glTranslatef(tOffsetX, 0.0f, 0.0f);*/

    ///--[Portrait]
    //--Renders without any masking.
    StarBitmap *rPortrait = rCharacter->GetCombatPortrait();
    if(rPortrait)
    {
        //--Check if the entity has a countermask.
        StarBitmap *rCountermask = rCharacter->GetCombatCounterMask();

        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rCharacter->GetUIRenderPosition(ACE_UI_INDEX_BASEMENU);

        //--Normal rendering.
        if(!rCountermask)
        {
            //--Don't render over the footer.
            glEnable(GL_STENCIL_TEST);
            glColorMask(true, true, true, true);
            glDepthMask(true);
            glStencilMask(0xFF);
            glStencilFunc(GL_NOTEQUAL, AM_STENCIL_FOOTER, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

            //--Render.
            rPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);

            //--Clean.
            DisplayManager::DeactivateStencilling();
        }
        //--Countermask rendering.
        else
        {
            //--Render countermask to lock off pixels. The countermask renders the same code as the footer.
            glEnable(GL_STENCIL_TEST);
            glColorMask(false, false, false, false);
            glDepthMask(false);
            glStencilMask(0xFF);
            glStencilFunc(GL_ALWAYS, AM_STENCIL_FOOTER, 0xFF);
            glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
            rCountermask->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);

            //--Render the portrait.
            glColorMask(true, true, true, true);
            glDepthMask(true);
            glStencilMask(0xFF);
            glStencilFunc(GL_NOTEQUAL, AM_STENCIL_FOOTER, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            rPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);

            //--Clean.
            DisplayManager::DeactivateStencilling();
        }
    }

    ///--[Basic Data]
    //--Statis parts.
    AdvImages.rCharacterFrame->Draw();

    //--HP bar, frame.
    float cHPBarPercent = rCharacter->GetHealthPercent();
    if(cHPBarPercent > 0.0f)
    {
        AdvImages.rCharacterHPFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cHPBarPercent, 1.0f);
    }
    AdvImages.rCharacterHPFrame->Draw();

    //--Character's level.
    AdvImages.rCharacterLevelBack->Draw();
    AdvImages.rCharacterNameBanner->Draw();
    AdvImages.rLevelFont->DrawTextArgs(87.0f, 704.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", rCharacter->GetLevel() + 1);
    AdvImages.rCharacterLevelFront->Draw();

    //--Character's name.
    AdvImages.rHeadingFont->DrawText(158.0f, 621.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rCharacter->GetDisplayName());

    //--Character's HP value.
    AdvImages.rStatisticFont->DrawTextArgs(146.0f, 696.0f, 0, 1.0f, "%i/%i", rCharacter->GetHealth(), rCharacter->GetHealthMax());

    ///--[Clean]
    glTranslatef(tOffsetX * -1.0f, 0.0f, 0.0f);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
