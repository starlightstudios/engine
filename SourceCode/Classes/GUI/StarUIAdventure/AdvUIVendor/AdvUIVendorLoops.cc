//--Base
#include "AdvUIVendor.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///========================================= Buy Mode =============================================
void AdvUIVendor::RenderShopItemListing(int pSkip, int pMaxPerPage, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the currently visible entries on the shop listing by calling RenderItemPack()
    //  in an organized fashion.
    int i = 0;
    int tRenders = 0;

    ///--[Iterate]
    ShopInventoryPack *rPackage = (ShopInventoryPack *)mShopInventory->PushIterator();
    while(rPackage)
    {
        //--Beneath the skip, don't render.
        if(i < pSkip)
        {
            i ++;
            rPackage = (ShopInventoryPack *)mShopInventory->AutoIterate();
            continue;
        }

        //--Subroutine.
        RenderItemPack(rPackage, tRenders, (i % 2 == 1), pColorAlpha);

        //--If we hit the render cap, stop.
        tRenders ++;
        if(tRenders >= pMaxPerPage)
        {
            mShopInventory->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rPackage = (ShopInventoryPack *)mShopInventory->AutoIterate();
    }
}

///========================================= Sell Mode ============================================
void AdvUIVendor::RenderInventoryItemListing(int pSkip, int pMaxPerPage, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the currently visible entries on the player's inventory listing by calling
    //  RenderItem() in an organized fashion.
    int i = 0;
    int tRenders = 0;
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();

    ///--[Iterate]
    AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--Beneath the skip, don't render.
        if(i < pSkip)
        {
            i ++;
            rItem = (AdventureItem *)rItemList->AutoIterate();
            continue;
        }

        //--Subroutine.
        RenderItem(rItem, tRenders, (i % 2 == 1), true, pColorAlpha);

        //--If we hit the render cap, stop.
        tRenders ++;
        if(tRenders >= pMaxPerPage)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }
}

///======================================= Buyback Mode ===========================================
void AdvUIVendor::RenderBuybackItemListing(int pSkip, int pMaxPerPage, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the currently visible entries on the shop's buyback listing by calling
    //  RenderItemPack() in an organized fashion.
    int i = 0;
    int tRenders = 0;
    StarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();

    ///--[Iterate]
    ShopInventoryPack *rPackage = (ShopInventoryPack *)rBuybackList->PushIterator();
    while(rPackage)
    {
        //--Beneath the skip, don't render.
        if(i < pSkip)
        {
            i ++;
            rPackage = (ShopInventoryPack *)rBuybackList->AutoIterate();
            continue;
        }

        //--Subroutine.
        RenderItemPack(rPackage, tRenders, (i % 2 == 1), pColorAlpha);

        //--If we hit the render cap, stop.
        tRenders ++;
        if(tRenders >= pMaxPerPage)
        {
            rBuybackList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rPackage = (ShopInventoryPack *)rBuybackList->AutoIterate();
    }
}

///========================================= Gems Mode ============================================
void AdvUIVendor::RenderGemsItemListing(int pSkip, int pMaxPerPage, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the currently visible entries on the shop's buyback listing by calling
    //  RenderGem() in an organized fashion.
    int i = 0;
    int tRenders = 0;
    StarLinkedList *rGemsList = AdventureInventory::Fetch()->GetGemList();

    ///--[Iterate]
    AdventureItem *rItem = (AdventureItem *)rGemsList->PushIterator();
    while(rItem)
    {
        //--Beneath the skip, don't render.
        if(i < pSkip)
        {
            i ++;
            rItem = (AdventureItem *)rGemsList->AutoIterate();
            continue;
        }

        //--Subroutine.
        RenderGem(rItem, rGemsList->GetIteratorName(), tRenders, (i % 2 == 1), pColorAlpha);

        //--If we hit the render cap, stop.
        tRenders ++;
        if(tRenders >= pMaxPerPage)
        {
            rGemsList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rItem = (AdventureItem *)rGemsList->AutoIterate();
    }
}

///====================================== Gem Merge Mode ==========================================
void AdvUIVendor::RenderGemsMergeListing(int pSkip, int pMaxPerPage, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the currently visible entries on the shop's buyback listing by calling
    //  RenderGemMerge() in an organized fashion.
    int i = 0;
    int tRenders = 0;
    StarLinkedList *rGemsList = AdventureInventory::Fetch()->GetGemList();

    ///--[Iterate]
    AdventureItem *rItem = (AdventureItem *)rGemsList->PushIterator();
    while(rItem)
    {
        //--Beneath the skip, don't render.
        if(i < pSkip)
        {
            i ++;
            rItem = (AdventureItem *)rGemsList->AutoIterate();
            continue;
        }

        //--Subroutine.
        RenderGemMerge(rItem, rGemsList->GetIteratorName(), tRenders, (i % 2 == 1), pColorAlpha);

        //--If we hit the render cap, stop.
        tRenders ++;
        if(tRenders >= pMaxPerPage)
        {
            rGemsList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rItem = (AdventureItem *)rGemsList->AutoIterate();
    }
}

///======================================== Unlock Mode ===========================================
void AdvUIVendor::RenderUnlockListing(int pSkip, int pMaxPerPage, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the currently visible entries on the shop's buyback listing by calling
    //  RenderUnlockPack() in an organized fashion.
    int i = 0;
    int tRenders = 0;
    StarLinkedList *rUseList = GetConsideredCategory();

    ///--[Iterate]
    //--Render entries.
    ShopInventoryPack *rPackage = (ShopInventoryPack *)rUseList->PushIterator();
    while(rPackage)
    {
        //--Beneath the skip, don't render.
        if(i < pSkip)
        {
            i ++;
            rPackage = (ShopInventoryPack *)rUseList->AutoIterate();
            continue;
        }

        //--If this is the highlighted entry, modify the scroll timer. It is otherwise zero.
        float tScrollPercent = 0.0f;

        //--Subroutine.
        RenderUnlockPack(rPackage, tRenders, (i % 2 == 1), pColorAlpha, tScrollPercent);
        tRenders ++;

        //--Render materials if this is the highlighted package.
        if(i == mCursor)
        {
            RenderUnlockMaterials(rPackage, tRenders, pColorAlpha);
            tRenders += 2;
        }

        //--If we hit the render cap, stop.
        if(tRenders >= pMaxPerPage)
        {
            rUseList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rPackage = (ShopInventoryPack *)rUseList->AutoIterate();
    }
}
