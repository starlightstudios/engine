//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombatJob.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
///===================================== Property Queries =========================================
int AdvCombatEntity::GetTotalProfiles()
{
    return mProfilesList->GetListSize();
}
AbilityProfile *AdvCombatEntity::GetActiveProfile()
{
    return rActiveProfile;
}
AbilityProfile *AdvCombatEntity::GetProfileS(const char *pName)
{
    return (AbilityProfile *)mProfilesList->GetElementByName(pName);
}
AbilityProfile *AdvCombatEntity::GetProfileI(int pSlot)
{
    return (AbilityProfile *)mProfilesList->GetElementBySlot(pSlot);
}
const char *AdvCombatEntity::GetNameOfProfileI(int pSlot)
{
    return mProfilesList->GetNameOfElementBySlot(pSlot);
}

///======================================= Manipulators ===========================================
void AdvCombatEntity::SetActiveProfileS(const char *pName)
{
    //--Basics.
    AbilityProfile *rCheckProfile = (AbilityProfile *)mProfilesList->GetElementByName(pName);
    if(!rCheckProfile) return;

    //--Clean the memorized listing. Reset it with the new profile.
    rActiveProfile = rCheckProfile;
    for(int x = 0; x < ACE_ABILITY_GRID_PROFILE_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_PROFILE_SIZE_Y; y ++)
        {
            SetAbilitySlot(x + ACE_ABILITY_GRID_PROFILE_OFFSET_X, y + ACE_ABILITY_GRID_PROFILE_OFFSET_Y, rActiveProfile->mAbilityNames[x][y]);
        }
    }
}
void AdvCombatEntity::SetActiveProfileI(int pSlot)
{
    //--Basics.
    AbilityProfile *rCheckProfile = (AbilityProfile *)mProfilesList->GetElementBySlot(pSlot);
    if(!rCheckProfile) return;

    //--Clean the memorized listing. Reset it with the new profile.
    rActiveProfile = rCheckProfile;
    for(int x = 0; x < ACE_ABILITY_GRID_PROFILE_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_PROFILE_SIZE_Y; y ++)
        {
            SetAbilitySlot(x + ACE_ABILITY_GRID_PROFILE_OFFSET_X, y + ACE_ABILITY_GRID_PROFILE_OFFSET_Y, rActiveProfile->mAbilityNames[x][y]);
        }
    }
}
void AdvCombatEntity::RegisterProfile(const char *pName, AbilityProfile *pProfile)
{
    //--Takes ownership of the provided profile. In case of name duplication, the original is removed
    //  and deleted, the new one taking its place.
    if(!pName || !pProfile) return;

    //--If the name is "Default" in any way, fail. The default profile cannot be overwritten.
    if(!strcasecmp(pName, "Default"))
    {
        AbilityProfile::DeleteThis(pProfile);
        return;
    }

    //--Remove existing elements.
    while(mProfilesList->RemoveElementS(pName))
    {
    }

    //--Add.
    mProfilesList->AddElement(pName, pProfile, &AbilityProfile::DeleteThis);

    //--Check if the active profile is still on the list. If not, reset it to the zeroth profile.
    if(!mProfilesList->IsElementOnList(rActiveProfile))
    {
        rActiveProfile = (AbilityProfile *)mProfilesList->GetElementBySlot(0);
    }
}
void AdvCombatEntity::SetProfileAbility(const char *pProfileName, int pX, int pY, const char *pAbilityName)
{
    //--Sets the ability name in the requested slot.
    if(!pProfileName || pX < 0 || pY < 0 || pX >= ACE_ABILITY_GRID_PROFILE_SIZE_X || pY >= ACE_ABILITY_GRID_PROFILE_SIZE_Y) return;

    //--Get the profile.
    AbilityProfile *rCheckProfile = (AbilityProfile *)mProfilesList->GetElementByName(pProfileName);
    if(!rCheckProfile) return;

    //--If the ability name was NULL or "Null", clear.
    if(!pAbilityName || !strcasecmp(pAbilityName, "Null"))
    {
        ResetString(rCheckProfile->mAbilityNames[pX][pY], NULL);
        return;
    }

    //--Otherwise, copy the name.
    ResetString(rCheckProfile->mAbilityNames[pX][pY], pAbilityName);
}

///======================================= Core Methods ===========================================
void AdvCombatEntity::ClearProfiles()
{
    //--Clear all profiles.
    mProfilesList->ClearList();

    //--Create default.
    AbilityProfile *nDefaultProfile = (AbilityProfile *)starmemoryalloc(sizeof(AbilityProfile));
    nDefaultProfile->Initialize();
    mProfilesList->AddElementAsHead("Default", nDefaultProfile, &AbilityProfile::DeleteThis);
    rActiveProfile = nDefaultProfile;
}
void AdvCombatEntity::RecheckJobsAgainstProfiles()
{
    ///--[Documentation]
    //--Checks all job associated profiles against the profile list. Any not found are set to default.
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->PushIterator();
    while(rJob)
    {
        //--Get the profile.
        const char *rAssociatedProfile = rJob->GetAssociatedProfileName();

        //--If it doesn't exist, reset.
        void *rCheckPtr = mProfilesList->GetElementByName(rAssociatedProfile);
        if(!rCheckPtr) rJob->SetAssociatedProfileName(NULL);

        //--Next.
        rJob = (AdvCombatJob *)mJobList->AutoIterate();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
StarLinkedList *AdvCombatEntity::GetProfilesList()
{
    return mProfilesList;
}

///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
