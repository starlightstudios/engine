//--[SpineExpression]
//--An emotion held by a DialogueCharacter. These are basically Spine wrappers.

#pragma once

//--[Includes]
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"
#include "spine/spine.h"

//--[Local Structures]
typedef struct Vertex
{
    //--Position.
    float x, y;

    //--Texture coordinates.
    float u, v;

    //--Color mixing.
    float r, g, b, a;
}Vertex;

//--[Local Definitions]
#define MAX_VERTICES_PER_ATTACHMENT 2048
//struct spAtlas;
//struct spSkeletonData;
//struct spAnimationStateData;
//struct spSkeleton;
//struct spAnimationState;

//--[Classes]
class SpineExpression : public RootObject
{
    private:
    //--System
    bool mIsReady;

    //--Spine Structures
    spine::Atlas *mSpineAtlas;
    spine::SkeletonData *mSpineSkeletonData;
    spine::AnimationStateData *mAnimationStateData;

    //--Animations
    spine::Skeleton *mSkeleton;
    spine::AnimationState *mAnimationState;

    //--Vertices
    bool mRenderHFlipped;
    float mWorldVerticesPositions[MAX_VERTICES_PER_ATTACHMENT];
    Vertex mVertices[MAX_VERTICES_PER_ATTACHMENT];

    protected:

    public:
    //--System
    SpineExpression();
    ~SpineExpression();
    void Construct(const char *pBasePath, const char *pImagePath, const char *pEmotionA, const char *pEmotionB);
    void Construct3(const char *pBasePath, const char *pImagePath, const char *pEmotionA, const char *pEmotionB, const char *pEmotionC);

    //--Public Variables
    static char *xLoadPath;
    static bool xDebug;

    //--Property Queries
    //--Manipulators
    void SetEmotion(const char *pEmotionName);
    void SetHFlipFlag(bool pFlag);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void AddVertex(float x, float y, float u, float v, float r, float g, float b, float a, int index);
    void RenderAtCenter(float pX, float pY, float pScale);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

