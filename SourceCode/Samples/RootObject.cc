#include "RootObject.h"
#include "Global.h"

///========================================== System ==============================================
RootObject::RootObject()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_FAIL;
    mUniqueID = Global::GetNextID();
}
RootObject::~RootObject()
{
}
void RootObject::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((RootObject *)pPtr);
}

///===================================== Property Queries =========================================
int RootObject::GetType()
{
    return mType;
}
bool RootObject::IsOfType(int pType)
{
    if(mType == POINTER_TYPE_FAIL) return false;
    return (pType == mType);
}
uint32_t RootObject::GetID()
{
    return mUniqueID;
}

///======================================== Lua Hooking ===========================================
void RootObject::HookToLuaState(lua_State *pLuaState)
{
    /* RO_GetID() (1 Integer)
       Returns the UniqueID of the object on the top of the activity stack. This ID is guaranteed
       to be unique for any reasonable program duration (it could, theoretically, overflow to 0).
       The ID will never 0, and never be negative. */
    lua_register(pLuaState, "RO_GetID", &Hook_RO_GetID);

    /* RO_GetType() (1 Integer)
       Returns the C++ type of the object on the top of the activity stack. Works for everything
       in the RootObject inheritance line, all other object types have undefined results. */
    lua_register(pLuaState, "RO_GetType", &Hook_RO_GetType);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
#include "DataLibrary.h"
int Hook_RO_GetID(lua_State *L)
{
    //RO_GetID()
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootObject *rObject = (RootObject *)rDataLibrary->rActiveObject;
    if(!rObject)
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    //--Return the type.
    lua_pushinteger(L, rObject->GetID());
    return 1;
}
int Hook_RO_GetType(lua_State *L)
{
    //RO_GetType()
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootObject *rObject = (RootObject *)rDataLibrary->rActiveObject;
    if(!rObject)
    {
        lua_pushinteger(L, POINTER_TYPE_FAIL);
        return 1;
    }

    //--Return the type.
    lua_pushinteger(L, rObject->GetType());
    return 1;
}
