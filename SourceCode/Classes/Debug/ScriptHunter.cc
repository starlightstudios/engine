//--Base
#include "ScriptHunter.h"

//--Classes
//--CoreClasses
#include "StarFileSystem.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define SH_DEBUG
#ifdef SH_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
ScriptHunter::ScriptHunter()
{
    //--[ScriptHunter]
    //--System
    //--Target Info
    mTargetDirectory = NULL;
}
ScriptHunter::~ScriptHunter()
{
    free(mTargetDirectory);
}

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void ScriptHunter::SetTargetDirectory(const char *pPath)
{
    ResetString(mTargetDirectory, pPath);
}

///======================================== Core Methods ==========================================
void ScriptHunter::Execute()
{
    ///--[Documentation and Setup]
    //--The meat of the class. Starting at the target directory, runs through all .lua files and
    //  all sub directories. Every lua file is checked in binary for possible errors. The lua files
    //  are not executed as lua files, so syntax errors or missing variables may be missed.
    if(!mTargetDirectory) return;

    ///--[Path Scan]
    //--File System, does the iterating work.
    StarFileSystem *tFileSystem = new StarFileSystem();

    //--Scan.
    tFileSystem->ScanDirectory(mTargetDirectory);
    fprintf(stderr, "Script Hunter runs on %s. %i files found.\n", mTargetDirectory, tFileSystem->GetTotalEntries());
    bool tPercentMarkers[10];
    for(int i = 0; i < 10; i ++)
    {
        tPercentMarkers[i] = false;
    }

    ///--[Iteration]
    //--Iterate across the files and call the subhandlers.
    int tTotalLuaFiles = 0;
    int tFilesTotal = tFileSystem->GetTotalEntries();
    for(int i = 0; i < tFilesTotal; i ++)
    {
        //--Progress.
        float tPercent = (float)i / (float)tFilesTotal;
        for(int p = 0; p < 10; p ++)
        {
            if(!tPercentMarkers[p])
            {
                if(tPercent >= 0.10 * (float)p)
                {
                    tPercentMarkers[p] = true;
                    //fprintf(stderr, "%i ", (p+1) * 10);
                }
                break;
            }
        }

        //--Get the info.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo)
        {
            continue;
        }

        //--We only care about .lua files, ignore .slf files in all cases.
        bool tIsLuaFile = false;
        int tLen = (int)strlen(rFileInfo->mPath);
        for(int p = tLen-1; p >= 0; p --)
        {
            //--When we reach a period, check the file type. We only care about .lua files.
            if(rFileInfo->mPath[p] == '.')
            {
                if(!strncasecmp(&rFileInfo->mPath[p+1], "lua", 3))
                {
                    tTotalLuaFiles ++;
                    tIsLuaFile = true;
                    break;
                }
            }
        }

        //--Execute.
        if(tIsLuaFile)
        {
            CheckBinaryForBugs(rFileInfo->mPath);
            //FixComments(rFileInfo->mPath);
        }
    }
    fprintf(stderr, "\n");

    ///--[Clean Up]
    fprintf(stderr, "Script hunter completes. %i lua files scanned.\n", tTotalLuaFiles);
    delete tFileSystem;
}

///==================================== Private Core Methods ======================================
bool ScriptHunter::CheckBinaryForBugs(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Given a path to a file, checks that file for bugs and spits the results both to the console
    //  and to the error log. Returns true if at least one bug was found, false if not.
    if(!pPath) return false;

    //--Variable used to track bugs.
    bool tAnyBugsFound = false;

    ///--[Filename]
    //--Seek backwards to the last slash.
    char *tFileName = StarFileSystem::PareFileName(pPath, true);
    if(!strcasecmp(tFileName, "TestFile.lua") && false)
    {
        free(tFileName);
        return false;
    }

    ///--[Basic Information]
    //--Open the file as binary.
    FILE *fInfile = fopen(pPath, "rb");

    //--Determine the file length.
    fseek(fInfile, 0, SEEK_END);
    int tFileSize = ftell(fInfile);
    fseek(fInfile, 0, SEEK_SET);

    //--Diagnostics variables.
    bool tQuotesMustBeEscaped = false;
    int tFileCursor = 0;
    int tLineCursor = 0;
    int tCharCursor = 0;
    int tLBracketTracker = 0;
    int tRBracketTracker = 0;

    //--Dialogue diagnostics.
    bool tInDialogueSet = false;
    int tDialogueProgress = 0;
    char tBuffer[11] = "fnDialogue";

    ///--[Parsing]
    //--Start reading.
    uint8_t tPreviousByte = 0;
    while(tFileCursor < tFileSize)
    {
        ///--[Read]
        //--Read the byte.
        uint8_t tByte = 0;
        fread(&tByte, sizeof(uint8_t), 1, fInfile);
        tFileCursor ++;
        tCharCursor ++;

        ///--[Standard ASCII]
        //--If the byte is standard ASCII, all is well.
        if(tByte < 128)
        {
            ///--[Dialogue Check]
            //--If the letter matches the dialogue case and not in the dialogue yet:
            if(!tInDialogueSet && tBuffer[tDialogueProgress] == tByte)
            {
                tDialogueProgress ++;
                if(tDialogueProgress >= 10) tInDialogueSet = true;
            }
            //--No match, fail.
            else
            {
                tDialogueProgress = 0;
            }

            ///--[R-Bracket Case]
            //--If the byte is an RBracket ']', track that.
            if(tByte == ']')
            {
                //--Count.
                tRBracketTracker ++;

                //--Hitting two R-brackets disables quote checking.
                if(tRBracketTracker == 2) tQuotesMustBeEscaped = false;

                //--Can't have 3 at once!
                if(tRBracketTracker >= 3)
                {
                    if(!tAnyBugsFound)
                    {
                        tAnyBugsFound = true;
                        fprintf(stderr, "%s\n", pPath);
                    }
                    fprintf(stderr, " Warning: Three R-brackets found on line %i.\n", tLineCursor+1);
                }
            }
            //--If this isn't an R bracket, reset those trackers.
            else
            {
                tRBracketTracker = 0;
            }

            ///--[L-Bracket Case]
            //--Used to track dialogue. When inside two L-brackets, quotes must be escaped.
            if(tByte == '[')
            {
                //--Count.
                tLBracketTracker ++;

                //--If we hit two of these, quotes start checking for escape sequences.
                if(tLBracketTracker == 2) tQuotesMustBeEscaped = true;
            }
            //--Reset.
            else
            {
                tLBracketTracker = 0;
            }

            ///--[Line-Ending Cases]
            //--Windows-style line-ending case.
            if(tByte == 13)
            {
                //--Reset bracket trackers.
                tRBracketTracker = 0;
                tInDialogueSet = false;
                tDialogueProgress = 0;

                //--Increment the line counter, reset the char counter.
                tLineCursor ++;
                tCharCursor = 0;

                //--Skip another byte.
                fread(&tByte, sizeof(uint8_t), 1, fInfile);
                tFileCursor ++;
            }
            //--Unix-style line-ending case.
            else if(tByte == 10)
            {
                //--Reset bracket trackers.
                tRBracketTracker = 0;
                tInDialogueSet = false;
                tDialogueProgress = 0;

                //--Increment the line counter, reset the char counter.
                tLineCursor ++;
                tCharCursor = 0;
            }

            ///--[Quote Case]
            //--Quotes must have a backslash on the previous byte.
            if(tByte == '"' && tPreviousByte != '\\' && tQuotesMustBeEscaped && tInDialogueSet)
            {
                if(!tAnyBugsFound)
                {
                    tAnyBugsFound = true;
                    fprintf(stderr, "%s\n", pPath);
                }
                fprintf(stderr, " Warning: Non-escaped quote found on line %i.\n", tLineCursor+1);
            }

            //--Store this as the previous byte.
            tPreviousByte = tByte;
            continue;
        }

        ///--[No Bugs]
        //--If no bugs were found so far, print the header.
        if(!tAnyBugsFound)
        {
            tAnyBugsFound = true;
            fprintf(stderr, "%s\n", pPath);
        }

        //--Warning line.
        fprintf(stderr, " Warning: Non-ASCII character at line %i column %i.\n", tLineCursor+1, tCharCursor+0);
        break;

        ///--[UTF-Warning]
        //--Uh-oh, the byte wasn't standard ASCII. Figure out how many bytes we need to skip for
        //  the same character, because UTF is multiple characters long.
        //--First is the four-byte case.
        if(tByte & 0xF0)
        {
            fseek(fInfile, 2, SEEK_CUR);
            tFileCursor += 3;
        }
        //--Three-byte.
        else if(tByte & 0xE0)
        {
            fseek(fInfile, 1, SEEK_CUR);
            tFileCursor += 2;
        }
        //--Two-byte.
        else
        {
            fseek(fInfile, 0, SEEK_CUR);
            tFileCursor ++;
        }

        //--Reset the previous byte to 0.
        tPreviousByte = 0;
    }

    ///--[Clean]
    //--Finish up.
    fclose(fInfile);
    free(tFileName);
    return tAnyBugsFound;
}
void ScriptHunter::FixComments(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Given a lua file, updates the comments to fit the modern standard. Standards are described
    //  below, there are several.
    if(!pPath) return;
    DebugPush(true, "Scan %s.", pPath);

    ///--[Infile Handling]
    //--Open the file as binary. Read the entire thing into a buffer.
    FILE *fInfile = fopen(pPath, "rb");
    if(!fInfile)
    {
        fprintf(stderr, "Error, file %s not found.\n", pPath);
        DebugPop("File not found.\n");
        return;
    }

    //--Determine the file length.
    fseek(fInfile, 0, SEEK_END);
    int tFileSize = ftell(fInfile);
    rewind(fInfile);

    //--Read into an array.
    char *tArray = (char *)starmemoryalloc(sizeof(char) * (tFileSize+1));
    fread(tArray, sizeof(char), tFileSize, fInfile);
    tArray[tFileSize] = '\0';

    //--Close.
    fclose(fInfile);
    DebugPrint("Scanned.\n");

    ///--[Parsing]
    //--Setup.
    int tLineCount = 0;
    bool tEndParsing = false;
    int tStringPos = 0;
    int tFilePos = 0;
    char tSubstring[1024];

    //--Output file.
    //FILE *fOutput = fopen("Debug/Test Output.lua", "w");
    FILE *fOutput = fopen(pPath, "w");
    DebugPrint("Parsing.\n");

    //--We parse this out line by line.
    //fprintf(stderr, "Beginning parse.\n");
    while(!tEndParsing)
    {
        ///--[Read the Line]
        //--Debug.
        //fprintf(stderr, " Line %i\n", tLineCount);
        //fprintf(stderr, " File position: %i\n", tFilePos);
        tLineCount ++;

        //--Clean the line.
        tStringPos = 0;
        tSubstring[0] = '\0';

        //--Read.
        while(true)
        {
            //--Copy across.
            tSubstring[tStringPos+0] = tArray[tFilePos];
            tSubstring[tStringPos+1] = '\0';
            tStringPos ++;
            tFilePos ++;

            //--Check if this is EOF.
            if(tFilePos >= tFileSize)
            {
                tEndParsing = true;
                break;
            }
            //--EOL.
            else if(tSubstring[tStringPos-1] == 10 || tSubstring[tStringPos-1] == 13)
            {
                //--Check if the next letter is also one of these. If so, increment the file count.
                //  This happens since windows systems print both 10 and 13 for EOLs.
                if(tArray[tFilePos] == 10 || tArray[tFilePos] == 13)
                {
                    tFilePos ++;
                    if(tFilePos >= tFileSize) tEndParsing = true;
                }
                break;
            }
        }

        //--If the first letter on the line is an EOL, just print an EOL.
        if(tSubstring[0] == 10 || tSubstring[0] == 13)
        {
            fprintf(fOutput, "\n");
            continue;
        }

        //--Check the last letter. If it's an EOL, remove it.
        int tLen = (int)strlen(tSubstring);
        while(tSubstring[tLen-1] == 10 || tSubstring[tLen-1] == 13)
        {
            tSubstring[tLen-1] = '\0';
            tLen --;
        }

        //--We now have the line. First, check if it's a comment. This includes indented comments.
        bool tIsCommentLine = false;
        for(int i = 0; i < tLen-1; i ++)
        {
            if(tSubstring[i] == '-' && tSubstring[i+1] == '-')
            {
                tIsCommentLine = true;
                break;
            }
        }

        //--Not a comment line? Write it to the file and continue to the next line.
        if(!tIsCommentLine)
        {
            fprintf(fOutput, "%s\n", tSubstring);
            continue;
        }

        ///--[Major Heading Pattern]
        // :--[ ========================================== Creation ========================================= ]
        // Is changed to:
        // :-- |[ ========================================= Creation ========================================= ]|
        // The line should be 102 characters long (the |'s are invisible in ZeroBrane).
        // The number of = is equal on both sides, if uneven, subtract from the left side.
        bool tIsMajorHeadingPattern = false;

        ///--[Minor Heading Pattern]
        // :    --[ ================= Dogs ================ ]
        // Is changed to:
        // :    -- |[ ================ Dogs ================ ]|
        // As above, should pad to 72, |'s are invisible.
        bool tIsMinorHeadingPattern = false;

        ///--[Subheading Pattern]
        // :    --[Dogs]
        // Is changed to:
        // :    -- |[Dogs]|
        // This one has no padding.
        bool tIsSubheadingPattern = false;

        //--Comment line, time to upgrade it. First, find the applicable pattern.
        for(int i = 0; i < tLen-1; i ++)
        {
            //--The line was already processed, so skip it:
            if(!strncmp(&tSubstring[i], "-- |[", 5))
            {
                break;
            }
            //--Heading pattern. Requires a line length over 80 and to be on the first part of the line.
            else if(!strncmp(&tSubstring[i], "--[ =", 5))
            {
                //--If this is the zeroth spot on the line, this is a major heading.
                if(i == 0 && tLen >= 80)
                {
                    tIsMajorHeadingPattern = true;
                    break;
                }

                //--Otherwise, minor heading.
                tIsMinorHeadingPattern = true;
                break;
            }
            //--Has a brace but no equals sign, subheading.
            else if(!strncmp(&tSubstring[i], "--[", 3) && strncmp(&tSubstring[i], "--[=", 4))
            {
                tIsSubheadingPattern = true;
                break;
            }
        }

        ///--Major Heading Case
        if(tIsMajorHeadingPattern)
        {
            //--Determine the string at the center of all this.
            bool tIsCopying = false;
            int c = 0;
            char tCenterString[128];
            for(int p = 0; p < tLen; p ++)
            {
                if(tSubstring[p] == '-')
                {
                }
                else if(tSubstring[p] == '[')
                {
                }
                else if(tSubstring[p] == '=')
                {
                    if(tIsCopying)
                    {
                        tCenterString[c-1] = '\0';
                        break;
                    }
                }
                else if(tSubstring[p] == ' ')
                {
                    if(tIsCopying)
                    {
                        tCenterString[c+0] = tSubstring[p];
                        tCenterString[c+1] = '\0';
                        c ++;
                    }
                }
                else
                {
                    tIsCopying = true;
                    tCenterString[c+0] = tSubstring[p];
                    tCenterString[c+1] = '\0';
                    c ++;
                }

                //--Length clamp.
                if(c > 90) break;
            }

            //--Get the length of the center string.
            int tCenterLen = (int)strlen(tCenterString);

            //--Add two padding spaces to the center length.
            tCenterLen += 2;

            //--Figure out how many equals signs we need.
            int tLftHeader = 6; // "-- |[ "
            int tRgtHeader = 3; // " ]|"
            int tEqualsLft = (100 - tCenterLen - tLftHeader - tRgtHeader) / 2;
            int tEqualsRgt = tEqualsLft;

            //--If the values go below 5, set them to 5. This is to clamp the lower end.
            if(tEqualsLft < 5) tEqualsLft = 5;
            if(tEqualsRgt < 5) tEqualsRgt = 5;

            //--If the center len is an odd number, subtract one from equals rgt.
            if(tCenterLen % 2 == 1) tEqualsRgt --;

            //--Create a new string. This string is always 102 characters long.
            char tNewString[256];
            strcpy(tNewString, "-- |[ ");

            //--Left-side equals signs.
            for(int p = 0; p < tEqualsLft; p ++)
            {
                strcat(tNewString, "=");
            }

            //--Center string.
            strcat(tNewString, " ");
            strcat(tNewString, tCenterString);
            strcat(tNewString, " ");

            //--Right-side equals signs.
            for(int p = 0; p < tEqualsRgt; p ++)
            {
                strcat(tNewString, "=");
            }

            //--Right header.
            strcat(tNewString, " ]|");

            //--Write.
            fprintf(fOutput, "%s\n", tNewString);
            continue;
        }
        ///--Minor Heading Case
        else if(tIsMinorHeadingPattern)
        {
            //--Determine the string at the center of all this.
            bool tIsCopying = false;
            bool tFoundComment = false;
            int c = 0;
            int tSpaces = 0;
            char tCenterString[128];
            for(int p = 0; p < tLen; p ++)
            {
                if(tSubstring[p] == '-')
                {
                    tFoundComment = true;
                }
                else if(tSubstring[p] == '[')
                {
                }
                else if(tSubstring[p] == '=')
                {
                    if(tIsCopying)
                    {
                        tCenterString[c-1] = '\0';
                        break;
                    }
                }
                else if(tSubstring[p] == ' ')
                {
                    if(tIsCopying)
                    {
                        tCenterString[c+0] = tSubstring[p];
                        tCenterString[c+1] = '\0';
                        c ++;
                    }
                    else if(!tFoundComment)
                    {
                        tSpaces ++;
                    }
                }
                else
                {
                    tIsCopying = true;
                    tCenterString[c+0] = tSubstring[p];
                    tCenterString[c+1] = '\0';
                    c ++;
                }

                //--Length clamp.
                if(c > 90) break;
            }

            //--Get the length of the center string.
            int tCenterLen = (int)strlen(tCenterString);

            //--Add two padding spaces to the center length.
            tCenterLen += 2;

            //--Figure out how many equals signs we need.
            int tLftHeader = 6; // "-- |[ "
            int tRgtHeader = 3; // " ]|"
            int tEqualsLft = (70 - tCenterLen - tLftHeader - tRgtHeader - tSpaces) / 2;
            int tEqualsRgt = tEqualsLft;

            //--If the values go below 5, set them to 5. This is to clamp the lower end.
            if(tEqualsLft < 5) tEqualsLft = 5;
            if(tEqualsRgt < 5) tEqualsRgt = 5;

            //--If the center len is an odd number, subtract one from equals rgt.
            if(tCenterLen % 2 == 1) tEqualsRgt --;

            //--Create a new string. This string is always 102 characters long.
            char tNewString[256];
            tNewString[0] = '\0';
            for(int p = 0; p < tSpaces; p ++)
            {
                strcat(tNewString, " ");
            }
            strcat(tNewString, "-- |[ ");

            //--Left-side equals signs.
            for(int p = 0; p < tEqualsLft; p ++)
            {
                strcat(tNewString, "=");
            }

            //--Center string.
            strcat(tNewString, " ");
            strcat(tNewString, tCenterString);
            strcat(tNewString, " ");

            //--Right-side equals signs.
            for(int p = 0; p < tEqualsRgt; p ++)
            {
                strcat(tNewString, "=");
            }

            //--Right header.
            strcat(tNewString, " ]|");

            //--Write.
            fprintf(fOutput, "%s\n", tNewString);
            continue;
        }
        ///--Subheading Case
        else if(tIsSubheadingPattern)
        {
            //--Create an alternate string with two extra slots allocated.
            char *tNewString = (char *)starmemoryalloc(sizeof(char) * (tLen + 4));

            //--Copy.
            int q = 0;
            for(int p = 0; p < tLen; p ++)
            {
                if(tSubstring[p] == '[')
                {
                    tNewString[q+0] = ' ';
                    tNewString[q+1] = '|';
                    tNewString[q+2] = '[';
                    q += 3;
                }
                else if(tSubstring[p] == ']')
                {
                    tNewString[q+0] = ']';
                    tNewString[q+1] = '|';
                    q += 2;
                }
                else
                {
                    tNewString[q] = tSubstring[p];
                    q ++;
                }
            }

            //--Cap.
            tNewString[tLen+3] = '\0';

            //--Write to file.
            fprintf(fOutput, "%s\n", tNewString);
            free(tNewString);
            continue;
        }
        ///--Normal Comment
        else
        {
            fprintf(fOutput, "%s\n", tSubstring);
            continue;
        }
    }

    ///--[Clean]
    DebugPrint("Cleaning.\n");
    fclose(fOutput);
    free(tArray);
    DebugPop("Done.\n");
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
