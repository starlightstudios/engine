//--Base
#include "TextLevel.h"

//--Classes
#include "TileLayer.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///--[Extents]
//--These indicate the size of the 3D array that constitutes the game world.
int TextLevel::GetLftExtent()
{
    return mLftMost;
}
int TextLevel::GetTopExtent()
{
    return mTopMost;
}
int TextLevel::GetRgtExtent()
{
    return mRgtMost;
}
int TextLevel::GetBotExtent()
{
    return mBotMost;
}
int TextLevel::GetZLoExtent()
{
    return mZLoMost;
}
int TextLevel::GetZHiExtent()
{
    return mZHiMost;
}

///--[World Compiler]
//--Temporarily builds a 3D world lookup. This is used for room property queries but is not the same as the finalized world. The finalized
//  world has padding and pathing properties, this one is just a lookup for crossloading.
void TextLevel::AssembleBuilderWorld()
{
    //--Deallocate in case anything was in memory.
    DeallocateBuilderWorld();

    //--Compute total sizes.
    mBuilderWorldX = mRgtMost - mLftMost + 1;
    mBuilderWorldY = mBotMost - mTopMost + 1;
    mBuilderWorldZ = mZHiMost - mZLoMost + 1;

    //--If any of the world sizes come back as zeroes, fail.
    if(mBuilderWorldX < 1 || mBuilderWorldY < 1 || mBuilderWorldZ < 1)
    {
        mBuilderWorldX = 0;
        mBuilderWorldY = 0;
        mBuilderWorldZ = 0;
        return;
    }

    //--Allocate space.
    SetMemoryData(__FILE__, __LINE__);
    mrBuilderWorld = (AutomapPack ****)starmemoryalloc(sizeof(AutomapPack ***) * mBuilderWorldX);
    for(int x = 0; x < mBuilderWorldX; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mrBuilderWorld[x] = (AutomapPack ***)starmemoryalloc(sizeof(AutomapPack **) * mBuilderWorldY);
        for(int y = 0; y < mBuilderWorldY; y ++)
        {
            SetMemoryData(__FILE__, __LINE__);
            mrBuilderWorld[x][y] = (AutomapPack **)starmemoryalloc(sizeof(AutomapPack *) * mBuilderWorldZ);
            for(int z = 0; z < mBuilderWorldZ; z ++)
            {
                mrBuilderWorld[x][y][z] = NULL;
            }
        }
    }

    //--Iterate across the automap packages and place references in the matching slot.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Resolve positions, range check.
        int tX = ((int)rCheckPack->mX) - mLftMost;
        int tY = ((int)rCheckPack->mY) - mTopMost;
        int tZ = ((int)rCheckPack->mZ) - mZLoMost;
        if(tX < 0 || tY < 0 || tZ < 0)
        {
            fprintf(stderr, "%i - %i = %i\n", ((int)rCheckPack->mX),  mLftMost, tX);
            fprintf(stderr, "%s: Out of bounds %i %i %i vs Zero.\n", rCheckPack->mInternalName, tX, tY, tZ);
            mAutomapList->PopIterator();
            break;
        }
        if(tX >= mBuilderWorldX || tY >= mBuilderWorldY || tZ >= mBuilderWorldZ)
        {
            fprintf(stderr, "%s: Out of bounds %i %i %i vs %i %i %i\n", rCheckPack->mInternalName, tX, tY, tZ, mBuilderWorldX, mBuilderWorldY, mBuilderWorldZ);
            mAutomapList->PopIterator();
            break;
        }

        //--Place.
        mrBuilderWorld[tX][tY][tZ] = rCheckPack;

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Build connectivity.
    BuilderWorldConnectivity();
}
void TextLevel::DeallocateBuilderWorld()
{
    //--Deallocate the reference array.
    for(int x = 0; x < mBuilderWorldX; x ++)
    {
        for(int y = 0; y < mBuilderWorldY; y ++)
        {
            free(mrBuilderWorld[x][y]);
        }
        free(mrBuilderWorld[x]);
    }
    free(mrBuilderWorld);
    mrBuilderWorld = NULL;

    //--Reset sizes.
    mBuilderWorldX = 0;
    mBuilderWorldY = 0;
    mBuilderWorldZ = 0;
}

///--[Connectivity]
TextLevZLevel *TextLevel::GetZLevel(int pZLevel)
{
    //--Iterate across the Z levels and return the first one that matches the given Z value.
    TextLevZLevel *rZLevel = (TextLevZLevel *)mZLevels->PushIterator();
    while(rZLevel)
    {
        if(rZLevel->mZLevel == pZLevel)
        {
            mZLevels->PopIterator();
            return rZLevel;
        }
        rZLevel = (TextLevZLevel *)mZLevels->AutoIterate();
    }

    //--No matches.
    return NULL;
}
//--[Worker Functions]
//--If the given tile matches any entry on the array, returns true. Used since many tiles can act as walls.
bool TileMatchesInArray(int pTile, int pEntries, int *pArray)
{
    if(!pArray || pEntries < 1) return false;
    for(int i = 0; i < pEntries; i ++)
    {
        if(pArray[i] == pTile) return true;
    }
    return false;
}
void TextLevel::BuilderWorldConnectivity()
{
    //--Constants.
    int cArraySize = 4;
    int cWestClosed[]  = {60, 64, 68, 72};
    int cNorthClosed[] = {61, 65, 69, 73};
    int cEastClosed[]  = {62, 66, 70, 74};
    int cSouthClosed[] = {63, 67, 71, 75};
    int cWestNarrow[]  = {80, 84, 88, 82};
    int cNorthNarrow[] = {81, 85, 89, 83};
    int cEastNarrow[]  = {82, 86, 80, 84};
    int cSouthNarrow[] = {83, 87, 81, 85};
    int cStairsDown = 49;
    int cStairsUp = 50;

    //--Offsets. cXOffCon/cYOffCon represents the left/top "padding" on the map.
    int cXOffCon = TL_STANDARD_PAD;
    int cYOffCon = TL_STANDARD_PAD;

    //--Builds connectivity lookups based on adjacency and wall data.
    for(int y = 0; y < mBuilderWorldY; y ++)
    {
        for(int x = 0; x < mBuilderWorldX; x ++)
        {
            for(int z = 0; z < mBuilderWorldZ; z ++)
            {
                //--Skip empty slots.
                if(!mrBuilderWorld[x][y][z]) continue;

                //--Fast-access pointer.
                AutomapPack *rPack = mrBuilderWorld[x][y][z];
                TextLevZLevel *rUseZLevel = GetZLevel(rPack->mZ);
                if(!rUseZLevel) continue;

                //--Default.
                mrBuilderWorld[x][y][z]->mConnectionFlags = 0x0000;
                mrBuilderWorld[x][y][z]->mOpenFlags = 0x0000;

                //--Wall Tile Layers
                TileLayer *rWallNLayer  = (TileLayer *)rUseZLevel->mTileLayers->GetElementByName("WallsN");
                TileLayer *rWallELayer  = (TileLayer *)rUseZLevel->mTileLayers->GetElementByName("WallsE");
                TileLayer *rWallSLayer  = (TileLayer *)rUseZLevel->mTileLayers->GetElementByName("WallsS");
                TileLayer *rWallWLayer  = (TileLayer *)rUseZLevel->mTileLayers->GetElementByName("WallsW");
                TileLayer *rStairsLayer = (TileLayer *)rUseZLevel->mTileLayers->GetElementByName("Floor4");

                //--Tile position.
                int tTileX = x + cXOffCon - (rUseZLevel->mTileOffX / 64);
                int tTileY = y + cYOffCon - (rUseZLevel->mTileOffY / 64);

                //--North check.
                if(y > 0 && mrBuilderWorld[x+0][y-1][z+0])
                {
                    //--Check for a tile.
                    int tTileIndex = 0;
                    if(rWallNLayer) tTileIndex = rWallNLayer->GetTileIndexAt(tTileX, tTileY);

                    //--Wall tile, no connection.
                    if(TileMatchesInArray(tTileIndex, cArraySize, cNorthClosed))
                    {
                    }
                    //--Target tile is unenterable.
                    else if(mrBuilderWorld[x+0][y-1][z+0]->mIsUnenterable)
                    {
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_N;
                    }
                    //--Narrow wall (ex: Door), connection but narrowed.
                    else if(TileMatchesInArray(tTileIndex, cArraySize, cNorthNarrow))
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_N;
                    }
                    //--No blockers, connect.
                    else
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_N;
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_N;
                    }
                }

                //--South check.
                if(y < mBuilderWorldY-1 && mrBuilderWorld[x+0][y+1][z+0])
                {
                    //--Check for a tile.
                    int tTileIndex = 0;
                    if(rWallSLayer) tTileIndex = rWallSLayer->GetTileIndexAt(tTileX, tTileY);

                    //--Wall tile, no connection.
                    if(TileMatchesInArray(tTileIndex, cArraySize, cSouthClosed))
                    {
                    }
                    //--Target tile is unenterable.
                    else if(mrBuilderWorld[x+0][y+1][z+0]->mIsUnenterable)
                    {
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_S;
                    }
                    //--Narrow wall (ex: Door), connection but narrowed.
                    else if(TileMatchesInArray(tTileIndex, cArraySize, cSouthNarrow))
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_S;
                    }
                    //--No blockers, connect.
                    else
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_S;
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_S;
                    }
                }

                //--West check.
                if(x > 0 && mrBuilderWorld[x-1][y+0][z+0])
                {
                    //--Check for a tile.
                    int tTileIndex = 0;
                    if(rWallWLayer) tTileIndex = rWallWLayer->GetTileIndexAt(tTileX, tTileY);

                    //--Wall tile, no connection.
                    if(TileMatchesInArray(tTileIndex, cArraySize, cWestClosed))
                    {
                    }
                    //--Target tile is unenterable.
                    else if(mrBuilderWorld[x-1][y+0][z+0]->mIsUnenterable)
                    {
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_W;
                    }
                    //--Narrow wall (ex: Door), connection but narrowed.
                    else if(TileMatchesInArray(tTileIndex, cArraySize, cWestNarrow))
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_W;
                    }
                    //--No blockers, connect.
                    else
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_W;
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_W;
                    }
                }

                //--East check.
                if(x < mBuilderWorldX-1 && mrBuilderWorld[x+1][y+0][z+0])
                {
                    //--Check for a tile.
                    int tTileIndex = 0;
                    if(rWallELayer) tTileIndex = rWallELayer->GetTileIndexAt(tTileX, tTileY);

                    //--Wall tile, no connection.
                    if(TileMatchesInArray(tTileIndex, cArraySize, cEastClosed))
                    {
                    }
                    //--Target tile is unenterable.
                    else if(mrBuilderWorld[x+1][y+0][z+0]->mIsUnenterable)
                    {
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_E;
                    }
                    //--Narrow wall (ex: Door), connection but narrowed.
                    else if(TileMatchesInArray(tTileIndex, cArraySize, cEastNarrow))
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_E;
                    }
                    //--No blockers, connect.
                    else
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_E;
                        mrBuilderWorld[x][y][z]->mOpenFlags |= TL_CONNECT_E;
                    }
                }

                //--Down check.
                if(z < mBuilderWorldZ-1 && mrBuilderWorld[x+0][y+0][z+1])
                {
                    //--Check for a tile.
                    int tTileIndex = 0;
                    if(rStairsLayer) tTileIndex = rStairsLayer->GetTileIndexAt(tTileX, tTileY);

                    //--A stair tile is needed for a downwards connection.
                    if(tTileIndex != cStairsDown)
                    {
                    }
                    //--We have stairs, but the tile below is unenterable (somehow).
                    else if(mrBuilderWorld[x+0][y+0][z+1]->mIsUnenterable)
                    {
                    }
                    //--We can go downstairs.
                    else
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_DN;
                    }
                }

                //--Up check.
                if(z > 0 && mrBuilderWorld[x+0][y+0][z-1])
                {
                    //--Check for a tile.
                    int tTileIndex = 0;
                    if(rStairsLayer) tTileIndex = rStairsLayer->GetTileIndexAt(tTileX, tTileY);

                    //--A stair tile is needed for a downwards connection.
                    if(tTileIndex != cStairsUp)
                    {
                    }
                    //--We have stairs, but the tile below is unenterable (somehow).
                    else if(mrBuilderWorld[x+0][y+0][z-1]->mIsUnenterable)
                    {
                    }
                    //--We can go downstairs.
                    else
                    {
                        mrBuilderWorld[x][y][z]->mConnectionFlags |= TL_CONNECT_UP;
                    }
                }

            }
        }
    }
}


///--[Room Properties]
//--These indicate things like name, connectivity, visibility, audio, etc for specific rooms. These functions fill mMapPropertyBuffer
//  with the needed string, which can then be copied and passed to lua.
const char *TextLevel::GetBuilderStringBuffer()
{
    return mMapPropertyBuffer;
}
AutomapPack *TextLevel::GetBuilderPack(int pX, int pY, int pZ)
{
    //--Worker function, returns the pack at the given position, or NULL if it doesn't exist.
    int tX = pX - mLftMost;
    int tY = pY - mTopMost;
    int tZ = pZ - mZLoMost;
    if(tX < 0 || tY < 0 || tZ < 0) return NULL;
    if(tX > mBuilderWorldX || tY > mBuilderWorldY || tZ > mBuilderWorldZ) return NULL;
    return mrBuilderWorld[tX][tY][tZ];
}
bool TextLevel::DoesBuilderRoomExist(int pX, int pY, int pZ)
{
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    return (rPackage != NULL);
}
bool TextLevel::IsBuilderRoomUnenterable(int pX, int pY, int pZ)
{
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return false;
    return rPackage->mIsUnenterable;
}
bool TextLevel::IsBuilderRoomNoSee(int pX, int pY, int pZ)
{
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return false;
    return rPackage->mNoSeePlayer;
}
void TextLevel::PopulateBuilderRoomName(int pX, int pY, int pZ)
{
    //--Standard.
    memset(mMapPropertyBuffer, 0, sizeof(char) * STD_MAX_LETTERS);
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return;

    //--Populate.
    strncpy(mMapPropertyBuffer, rPackage->mInternalName, sizeof(mMapPropertyBuffer));
}
void TextLevel::PopulateBuilderRoomIdentity(int pX, int pY, int pZ)
{
    //--Standard.
    memset(mMapPropertyBuffer, 0, sizeof(char) * STD_MAX_LETTERS);
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return;

    //--Populate.
    strncpy(mMapPropertyBuffer, rPackage->mIdentity, sizeof(mMapPropertyBuffer));
}
void TextLevel::PopulateBuilderRoomConnectivity(int pX, int pY, int pZ)
{
    //--Standard.
    memset(mMapPropertyBuffer, 0, sizeof(char) * STD_MAX_LETTERS);
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return;

    //--Build connectivity.
    if(rPackage->mConnectionFlags & TL_CONNECT_N)  strcat(mMapPropertyBuffer, "N");
    if(rPackage->mConnectionFlags & TL_CONNECT_E)  strcat(mMapPropertyBuffer, "E");
    if(rPackage->mConnectionFlags & TL_CONNECT_S)  strcat(mMapPropertyBuffer, "S");
    if(rPackage->mConnectionFlags & TL_CONNECT_W)  strcat(mMapPropertyBuffer, "W");
    if(rPackage->mConnectionFlags & TL_CONNECT_UP) strcat(mMapPropertyBuffer, "U");
    if(rPackage->mConnectionFlags & TL_CONNECT_DN) strcat(mMapPropertyBuffer, "D");
}
void TextLevel::PopulateBuilderRoomOpenEdges(int pX, int pY, int pZ)
{
    //--Standard.
    memset(mMapPropertyBuffer, 0, sizeof(char) * STD_MAX_LETTERS);
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return;

    //--Build connectivity.
    if(rPackage->mOpenFlags & TL_CONNECT_N) strcat(mMapPropertyBuffer, "N");
    if(rPackage->mOpenFlags & TL_CONNECT_E) strcat(mMapPropertyBuffer, "E");
    if(rPackage->mOpenFlags & TL_CONNECT_S) strcat(mMapPropertyBuffer, "S");
    if(rPackage->mOpenFlags & TL_CONNECT_W) strcat(mMapPropertyBuffer, "W");
}
void TextLevel::PopulateBuilderRoomDoor(int pX, int pY, int pZ, int pDoorIndex)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");

    //--Seek through all the doors and find the ones that match the room.
    TextLevelTempDoor *rDoor = (TextLevelTempDoor *)mTempDoorList->PushIterator();
    while(rDoor)
    {
        //--If the door matches the coordinates...
        if(rDoor->mX == pX && rDoor->mY == pY && rDoor->mZ == pZ)
        {
            //--Subtract the index count. If it goes to -1, use this door.
            pDoorIndex --;
            if(pDoorIndex < 0)
            {
                mMapPropertyBuffer[0] = rDoor->mDoorDir;
                mMapPropertyBuffer[1] = '|';
                mMapPropertyBuffer[2] = '\0';
                strcat(mMapPropertyBuffer, rDoor->mDoorName);
                mTempDoorList->PopIterator();
                return;
            }
        }

        //--Next.
        rDoor = (TextLevelTempDoor *)mTempDoorList->AutoIterate();
    }

    //--If we got this far, no matching door was found.
}
void TextLevel::PopulateBuilderRoomAnimation(int pX, int pY, int pZ, int pAnimIndex)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return;

    //--Get the name of the registered pack in the index.
    void *rPtr = rPackage->mrAnimationsList->PushIterator();
    while(rPtr)
    {
        //--Decrement.
        pAnimIndex --;

        //--Value goes below zero, return this entry.
        if(pAnimIndex < 0)
        {
            strcpy(mMapPropertyBuffer, rPackage->mrAnimationsList->GetIteratorName());
            rPackage->mrAnimationsList->PopIterator();
            return;
        }

        rPtr = rPackage->mrAnimationsList->AutoIterate();
    }

    //--If we got this far, no matching door was found.
}
void TextLevel::PopulateBuilderRoomAudio(int pX, int pY, int pZ)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");

    //--Seek through all the doors and find the ones that match the room.
    TextLevelTempString *rStringPack = (TextLevelTempString *)mTempAudioList->PushIterator();
    while(rStringPack)
    {
        //--If the door matches the coordinates...
        if(rStringPack->mX == pX && rStringPack->mY == pY && rStringPack->mZ == pZ)
        {
            strncpy(mMapPropertyBuffer, rStringPack->mString, sizeof(mMapPropertyBuffer));
        }

        //--Next.
        rStringPack = (TextLevelTempString *)mTempAudioList->AutoIterate();
    }

    //--If we got this far, no matching door was found.
}
void TextLevel::PopulateBuilderRoomStrangerGroup(int pX, int pY, int pZ)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return;
    strncpy(mMapPropertyBuffer, rPackage->mStrangerGroup, sizeof(mMapPropertyBuffer));
}
void TextLevel::PopulateBuilderPatrolIdentity(int pPatrolIndex)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");

    //--Get the package in question.
    TextLevelPatrolDataPack *rPackage = (TextLevelPatrolDataPack *)mTempPatrolList->GetElementBySlot(pPatrolIndex);
    if(!rPackage) return;

    //--Populate data.
    strncpy(mMapPropertyBuffer, rPackage->mEnemyUniqueName, sizeof(mMapPropertyBuffer));
}
void TextLevel::PopulateBuilderPatrolEnemyType(int pPatrolIndex)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");

    //--Get the package in question.
    TextLevelPatrolDataPack *rPackage = (TextLevelPatrolDataPack *)mTempPatrolList->GetElementBySlot(pPatrolIndex);
    if(!rPackage) return;

    //--Populate data.
    strncpy(mMapPropertyBuffer, rPackage->mEnemyType, sizeof(mMapPropertyBuffer));
}
void TextLevel::PopulateBuilderPatrolLocation(int pPatrolIndex, int pNodeIndex)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");
    //fprintf(stderr, "Check %i %i\n", pPatrolIndex, pNodeIndex);

    //--Get the package in question.
    TextLevelPatrolDataPack *rPackage = (TextLevelPatrolDataPack *)mTempPatrolList->GetElementBySlot(pPatrolIndex);
    if(!rPackage) return;
    //fprintf(stderr, " Has pack associated.\n");

    //--Get the node in question. If it's out of range, fail.
    if(pNodeIndex < 0 || pNodeIndex >= rPackage->mTotalNodes || pNodeIndex >= TLPDP_MAX_NODES) return;
    //fprintf(stderr, " Pack in range.\n");
    sprintf(mMapPropertyBuffer, "_%ix%ix%i", rPackage->mNodes[pNodeIndex].mX, rPackage->mNodes[pNodeIndex].mY, rPackage->mNodes[pNodeIndex].mZ);

    //--If any of the values are -10000, print a warning.
    if(rPackage->mNodes[pNodeIndex].mX == -10000 || rPackage->mNodes[pNodeIndex].mY == -10000 || rPackage->mNodes[pNodeIndex].mZ == -10000)
    {
        fprintf(stderr, "Warning, path %s node %i has unfilled data.\n", rPackage->mEnemyUniqueName, pNodeIndex);
    }
}
void TextLevel::PopulateBuilderLocationName(int pLocationIndex)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");

    //--Get the package in question.
    TextLevelTempString *rPackage = (TextLevelTempString *)mTempLocationList->GetElementBySlot(pLocationIndex);
    if(!rPackage) return;

    //--Get the location, and turn it into a room name.
    strncpy(mMapPropertyBuffer, rPackage->mString, sizeof(mMapPropertyBuffer));
}
void TextLevel::PopulateBuilderLocationRoom(int pLocationIndex)
{
    //--Standard.
    strcpy(mMapPropertyBuffer, "Null");

    //--Get the package in question.
    TextLevelTempString *rPackage = (TextLevelTempString *)mTempLocationList->GetElementBySlot(pLocationIndex);
    if(!rPackage) return;

    //--Get the location, and turn it into a room name.
    AutomapPack *rAutomapPackage = GetBuilderPack(rPackage->mX, rPackage->mY, rPackage->mZ);
    if(!rAutomapPackage) return;

    //--Populate.
    strncpy(mMapPropertyBuffer, rAutomapPackage->mInternalName, sizeof(mMapPropertyBuffer));
}
int TextLevel::GetBuilderVisOverrideAt(int pX, int pY, int pZ, int pDirection)
{
    //--Make sure the pack exists.
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return OVERRIDE_VISIBILITY_NONE;

    //--Code vs. Direction.
    if(pDirection == DIR_NORTH)
    {
        return rPackage->mOverrideVisN;
    }
    if(pDirection == DIR_EAST)
    {
        return rPackage->mOverrideVisE;
    }
    if(pDirection == DIR_SOUTH)
    {
        return rPackage->mOverrideVisS;
    }
    if(pDirection == DIR_WEST)
    {
        return rPackage->mOverrideVisW;
    }

    //--Error.
    return OVERRIDE_VISIBILITY_NONE;
}
int TextLevel::GetBuilderVisDown(int pX, int pY, int pZ)
{
    //--Returns how many levels below this tile the player can see if they can see the tile. This allows
    //  emulation of "balconies" and other verticality. It only goes downwards.
    AutomapPack *rPackage = GetBuilderPack(pX, pY, pZ);
    if(!rPackage) return 0;
    return rPackage->mVisibilityDown;
}
