//--Base
#include "AdvUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIVendor::AdvUIVendor()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_ADVUIVENDOR;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AVEN");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIVendor ======= ]
    ///--[Per-Class Constants]
    cScrollBuf = cxAdvScrollBuf;
    cScrollPage = cxAdvScrollPageSize;
    cYRenderConfirmCancelText = 288.0f;

    ///--[System]
    mIsBuyOnly = false;
    mArrowTimer = 0;

    ///--[Common]
    mCursor = 0;
    mSkip = 0;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    ///--[Categories]
    mCategoryCursor = 0;
    mCategoryHighlightPos.Initialize();
    mCategoryHighlightSize.Initialize();
    mCategoryList = new StarLinkedList(true);

    ///--[Shop Info]
    mShopName = NULL;
    mShopSetupPath = NULL;
    mShopTeardownPath = NULL;
    mShopInventory = new StarLinkedList(true);

    ///--[Comparison]
    mShowSwapArrows = false;
    mCompareCharacterCur = -1;
    mCompareCharacterOld = -1;
    mCompareSwapTimer = cxAdvCharSwapTicks;
    mCanSwapCompareSlot = false;
    mComparisonSlotList = new StarLinkedList(false);
    mCompareSlot = NULL;

    ///--[Materials Pane]
    mComparisonShowMaterials = false;
    mMaterialsUsePages = false;
    mMaterialsPage = 0;
    mMaterialsPages = new StarLinkedList(true);

    ///--[Details]
    mIsShowingDetails = false;
    mDetailsTimer = 0;

    ///--[Buy Mode]
    mIsBuyMode = false;
    mIsBuyingItem = false;
    mBuyTimer = 0;
    mBuyQuantity = 0;
    mAskToEquip = false;
    mAskToEquipSlot = 0;
    mAskToEquipTimer = 0;
    mBuyResponseScript = NULL;

    ///--[Sell Mode]
    mIsSellMode = false;
    mIsSellingItem = false;
    mSellTimer = 0;

    ///--[Buyback Mode]
    mIsBuybackMode = false;

    ///--[Gems Mode]
    mAllowGemcutting = false;
    mIsGemsMode = false;
    mIsMergeMode = false;
    mIsConfirmMergeMode = false;
    mIsConfirmDisassembleMode = false;
    mGemMergeTimer = 0;
    mGemDisassembleTimer = 0;
    mGemConfirmMergeTimer = 0;
    mMergeGemOwner = NULL;
    mMergeGem = NULL;

    ///--[Unlock Mode]
    mUnlockModePending = false;
    mIsUnlockMode = false;
    mUnlockScrollTimer = 0;
    mUnlockScrollTimerPause = 15;
    mUnlockScrollTimerUp = true;
    mIsUnlockingItem = false;
    mUnlockConfirmTimer = 0;
    mUnlockCanEquip = false;
    mMaterialsList = new StarLinkedList(true);

    ///--[Merge Help Menu]
    mIsShowingMergeHelp = false;
    mMergeHelpVisibilityTimer = 0;

    ///--[Strings]
    mShowHelpString = new StarlightString();
    mShowDetailsString = new StarlightString();
    mCharacterChangeString = new StarlightString();
    mScrollFastString = new StarlightString();
    mToggleCompareString = new StarlightString();
    mConfirmPurchaseString = new StarlightString();
    mCancelPurchaseString = new StarlightString();
    mAdjustQuantityString = new StarlightString();
    mTenQuantityString = new StarlightString();
    mEquipNowString = new StarlightString();
    mDontEquipString = new StarlightString();
    mDisassembleString = new StarlightString();
    mToggleCompareMaterialsString = new StarlightString();
    mToggleMaterialsPageString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Strings
    AllocateHelpStrings(cxHelpStringsTotal);
    for(int i = 0; i < cxMergeStringsTotal; i ++) mMergeHelpMenuStrings[i] = new StarlightString();

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIVendor::~AdvUIVendor()
{
    delete mCategoryList;
    for(int i = 0; i < cxMergeStringsTotal; i ++) delete mMergeHelpMenuStrings[i];
    free(mShopName);
    free(mShopSetupPath);
    free(mShopTeardownPath);
    free(mCompareSlot);
    free(mMergeGemOwner);
    free(mBuyResponseScript);
    delete mComparisonSlotList;
    delete mMaterialsList;
    delete mMaterialsPages;
    delete mShopInventory;
    delete mShowHelpString;
    delete mShowDetailsString;
    delete mCharacterChangeString;
    delete mScrollFastString;
    delete mToggleCompareString;
    delete mConfirmPurchaseString;
    delete mCancelPurchaseString;
    delete mAdjustQuantityString;
    delete mTenQuantityString;
    delete mEquipNowString;
    delete mDontEquipString;
    delete mDisassembleString;
    delete mToggleCompareMaterialsString;
    delete mToggleMaterialsPageString;
}
void AdvUIVendor::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Vendor UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Variables]
bool AdvUIVendor::xDisallowEquipFromResponse = false;

//--String that is available for query from unlock scripts. Used to transmit special information to
//  the unlock script, in format "Arg1:SubArg|Arg2:SubArg" etc.
char *AdvUIVendor::xUnlockReplaceInfo = NULL;

///===================================== Property Queries =========================================
bool AdvUIVendor::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    if(pType == POINTER_TYPE_ADVUIVENDOR) return true;
    return false;
}

///======================================= Manipulators ===========================================
void AdvUIVendor::TakeForeground()
{
    ///--[Documentation]
    //--Should be called immediately before SetShopInformation() which will handle the actual work
    //  of running item setup. This function resets variables.

    ///--[Variable Reset/Initialize]
    //--System
    mIsBuyOnly = false;
    mVisibilityTimer = 0;
    mArrowTimer = 0;

    //--Common
    mCursor = 0;
    mSkip = 0;

    //--Categories
    mCategoryCursor = 0;
    mCategoryList->ClearList();

    //--Shop Info
    ResetString(mShopName, NULL);
    ResetString(mShopSetupPath, NULL);
    ResetString(mShopTeardownPath, NULL);
    mShopInventory->ClearList();

    //--Comparison
    mShowSwapArrows = false;
    mCompareCharacterCur = -1;
    mCompareCharacterOld = -1;
    mCompareSwapTimer = cxAdvCharSwapTicks;
    mCanSwapCompareSlot = false;
    mComparisonSlotList->ClearList();
    ResetString(mCompareSlot, NULL);

    //--Materials Pane
    mComparisonShowMaterials = false;
    mMaterialsUsePages = false;
    mMaterialsPage = 0;
    mMaterialsPages->ClearList();

    //--Help Menu
    mIsShowingHelp = false;
    mHelpVisibilityTimer = 0;

    //--Merge Help Menu
    mIsShowingMergeHelp = false;
    mMergeHelpVisibilityTimer = 0;

    ///--[Mode Flags]
    //--Buy Mode
    mIsBuyMode = true;
    mIsBuyingItem = false;
    mBuyTimer = 0;
    mBuyQuantity = 0;
    mAskToEquip = false;
    mAskToEquipSlot = 0;
    mAskToEquipTimer = 0;
    ResetString(mBuyResponseScript, NULL);

    //--Sell Mode
    mIsSellMode = false;
    mIsSellingItem = false;
    mSellTimer = 0;

    //--Buyback Mode
    mIsBuybackMode = false;

    //--Gems Mode
    mAllowGemcutting = false;
    mIsGemsMode = false;
    mIsMergeMode = false;
    mIsConfirmMergeMode = false;
    mIsConfirmDisassembleMode = false;
    mGemMergeTimer = 0;
    mGemDisassembleTimer = 0;
    mGemConfirmMergeTimer = 0;
    mMergeGemOwner = NULL;
    mMergeGem = NULL;

    //--Unlock Mode
    mUnlockModePending = false;
    mIsUnlockMode = false;
    mUnlockScrollTimer = 0;
    mUnlockScrollTimerPause = 15;
    mUnlockScrollTimerUp = true;
    mIsUnlockingItem = false;
    mUnlockConfirmTimer = 0;
    mUnlockCanEquip = false;
    mMaterialsList->ClearList();

    ///--[Statistics]
    //--Build statistic lookups.
    mStatisticIndexes[ 0] = STATS_HPMAX;
    mStatisticIndexes[ 1] = STATS_ATTACK;
    mStatisticIndexes[ 2] = STATS_ACCURACY;
    mStatisticIndexes[ 3] = STATS_EVADE;
    mStatisticIndexes[ 4] = STATS_INITIATIVE;
    mStatisticIndexes[ 5] = STATS_RESIST_PROTECTION;
    mStatisticIndexes[ 6] = STATS_RESIST_SLASH;
    mStatisticIndexes[ 7] = STATS_RESIST_STRIKE;
    mStatisticIndexes[ 8] = STATS_RESIST_PIERCE;
    mStatisticIndexes[ 9] = STATS_RESIST_FLAME;
    mStatisticIndexes[10] = STATS_RESIST_FREEZE;
    mStatisticIndexes[11] = STATS_RESIST_SHOCK;
    mStatisticIndexes[12] = STATS_RESIST_CRUSADE;
    mStatisticIndexes[13] = STATS_RESIST_OBSCURE;
    mStatisticIndexes[14] = STATS_RESIST_BLEED;
    mStatisticIndexes[15] = STATS_RESIST_POISON;
    mStatisticIndexes[16] = STATS_RESIST_CORRODE;
    mStatisticIndexes[17] = STATS_RESIST_TERRIFY;

    //--Bonus Tag Names
    strcpy(mBonusTagNames[ 0], "Null");
    strcpy(mBonusTagNames[ 1], "Null");
    strcpy(mBonusTagNames[ 2], "Null");
    strcpy(mBonusTagNames[ 3], "Null");
    strcpy(mBonusTagNames[ 4], "Null");
    strcpy(mBonusTagNames[ 5], "Null");
    strcpy(mBonusTagNames[ 6], "Slash Damage Dealt +");
    strcpy(mBonusTagNames[ 7], "Strike Damage Dealt +");
    strcpy(mBonusTagNames[ 8], "Pierce Damage Dealt +");
    strcpy(mBonusTagNames[ 9], "Flame Damage Dealt +");
    strcpy(mBonusTagNames[10], "Freeze Damage Dealt +");
    strcpy(mBonusTagNames[11], "Shock Damage Dealt +");
    strcpy(mBonusTagNames[12], "Holy Damage Dealt +");
    strcpy(mBonusTagNames[13], "Shadow Damage Dealt +");
    strcpy(mBonusTagNames[14], "Bleed Damage Dealt +");
    strcpy(mBonusTagNames[15], "Poison Damage Dealt +");
    strcpy(mBonusTagNames[16], "Corrode Damage Dealt +");
    strcpy(mBonusTagNames[17], "Terrify Damage Dealt +");

    //--Malus Tag Names
    strcpy(mMalusTagNames[ 0], "Null");
    strcpy(mMalusTagNames[ 1], "Null");
    strcpy(mMalusTagNames[ 2], "Null");
    strcpy(mMalusTagNames[ 3], "Null");
    strcpy(mMalusTagNames[ 4], "Null");
    strcpy(mMalusTagNames[ 5], "Null");
    strcpy(mMalusTagNames[ 6], "Slash Damage Dealt -");
    strcpy(mMalusTagNames[ 7], "Strike Damage Dealt -");
    strcpy(mMalusTagNames[ 8], "Pierce Damage Dealt -");
    strcpy(mMalusTagNames[ 9], "Flame Damage Dealt -");
    strcpy(mMalusTagNames[10], "Freeze Damage Dealt -");
    strcpy(mMalusTagNames[11], "Shock Damage Dealt -");
    strcpy(mMalusTagNames[12], "Holy Damage Dealt -");
    strcpy(mMalusTagNames[13], "Shadow Damage Dealt -");
    strcpy(mMalusTagNames[14], "Bleed Damage Dealt -");
    strcpy(mMalusTagNames[15], "Poison Damage Dealt -");
    strcpy(mMalusTagNames[16], "Corrode Damage Dealt -");
    strcpy(mMalusTagNames[17], "Terrify Damage Dealt -");

    ///--[Strings]
    //--Control strings.
    mShowHelpString->              AutoSetControlString("[IMG0] Show Help",                             1, "F1");
    mShowDetailsString->           AutoSetControlString("[IMG0] Item Details",                          1, "F2");
    mCharacterChangeString->       AutoSetControlString("[IMG0][IMG1] Change Character",                2, "UpLevel", "DnLevel");
    mScrollFastString->            AutoSetControlString("[IMG0] (Hold) Scroll x10",                     1, "Ctrl");
    mToggleCompareString->         AutoSetControlString("[IMG0] Toggle Compare Slot",                   1, "Run");
    mConfirmPurchaseString->       AutoSetControlString("[IMG0] Confirm",                               1, "Activate");
    mCancelPurchaseString->        AutoSetControlString("[IMG0] Cancel",                                1, "Cancel");
    mAdjustQuantityString->        AutoSetControlString("[IMG0][IMG1] Adjust Quantity",                 2, "Left", "Right");
    mTenQuantityString->           AutoSetControlString("[IMG0][IMG1] Quantity x10",                    2, "DnLevel", "UpLevel");
    mDisassembleString->           AutoSetControlString("[IMG0] Disassemble Gem",                       1, "Run");
    mToggleCompareMaterialsString->AutoSetControlString("[IMG0] Toggle Materials/Statistics",           1, "Jump");
    mToggleMaterialsPageString->   AutoSetControlString("[IMG0] + [IMG1]/[IMG2] Toggle Materials Page", 3, "Ctrl", "UpLevel", "DnLevel");
}
void AdvUIVendor::SetShopInformation(const char *pShopName, const char *pPath, const char *pTeardownPath)
{
    ///--[Documentation]
    //--Calls TakeForeground() to reset variables, then runs shop scripts and sets names. The teardown path
    //  is optional.
    if(!pShopName || !pPath) return;

    ///--[Reset Variables]
    TakeForeground();

    ///--[Shop Variables]
    //--Store shop names and paths.
    ResetString(mShopName, pShopName);
    ResetString(mShopSetupPath, pPath);
    ResetString(mShopTeardownPath, pTeardownPath);

    //--Set to buy mode. Do this before executing the shop setup algorithm. The shop setup may modify the mode
    //  during script execution. Cursor is not reset until after script execution is done.
    ActivateModeBuy();

    //--Run the setup script.
    LuaManager::Fetch()->ExecuteLuaFile(mShopSetupPath);

    ///--[Post Exec]
    //--Sort the player's inventory.
    AdventureInventory::Fetch()->SortItemListByInternalCriteria();

    //--If this flag was set, override the normal shop modes and head to Unlock mode. This mode is exclusive
    //  with the others on purpose.
    if(mUnlockModePending)
    {
        ActivateModeUnlock();
        mUnlockModePending = false;
    }

    //--Reset cursor.
    ResetCursor();
    RecheckComparisonCharacter(GetConsideredItem());
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}
void AdvUIVendor::SetBuyOnly(bool pFlag)
{
    ///--[Documentation]
    //--In buy-only mode, the player cannot sell, buyback, or cut gems.
    mIsBuyOnly = pFlag;
}

///======================================= Core Methods ===========================================
void AdvUIVendor::RefreshMenuHelp()
{

}
void AdvUIVendor::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Determines where the cursor should be given the inventory cursor and scrollbar offsets. This is used
    //  for the standard parts of the UI. Gems have their own handler.
    float cNameLft = 289.0f;
    float cNameWid = 0.0f;

    //--Compute Y position.
    float cSlotY = 187.0f;
    float cSlotH =  26.0f;

    ///--[Name Length]
    //--The length of the entry depends on the mode.
    if(mIsBuyMode)
    {
        cNameWid = GetBuyCursorNameWidth();
    }
    else if(mIsSellMode)
    {
        cNameWid = GetSellCursorNameWidth();
    }
    else if(mIsBuybackMode)
    {
        cNameWid = GetBuybackCursorNameWidth();
    }
    else if(mIsGemsMode)
    {
        cNameWid = GetGemsCursorNameWidth(cNameLft);
    }
    else if(mIsUnlockMode)
    {
        cNameWid = GetUnlockCursorNameWidth();
    }

    //--Positions.
    float cLft = cNameLft - 3.0f;
    float cTop = cSlotY + (cSlotH * (mCursor - mSkip));
    float cRgt = cNameLft + cNameWid + 4.0f;
    float cBot = cTop + cSlotH + 2.0f;

    //--Set.
    mHighlightPos.MoveTo (       cLft,        cTop, cxAdvCurTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxAdvCurTicks);
}
void AdvUIVendor::RecomputeCategoryCursorPositions()
{
    ///--[Documentation]
    //--Determines where the category cursor should be. Presently only used in Unlock mode.
    float cFrameX = 251.0f;
    float cFrameY = 85.0f;
    float cEdgeW = 18.0f;
    float cEdgeH =  4.0f;
    float cIconW = 38.0f;

    //--Compute.
    float cLft = cFrameX + cEdgeW + (cIconW * mCategoryCursor);
    float cRgt = cLft + cIconW;
    float cTop = cFrameY + cEdgeH;
    float cBot = cTop + cIconW;

    //--Set.
    mCategoryHighlightPos.MoveTo (       cLft,        cTop, cxAdvCurTicks);
    mCategoryHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxAdvCurTicks);
}
void AdvUIVendor::ActivateMode(bool &sModeFlag)
{
    ///--[Documentation]
    //--To remove redundancy (such as having to modify 6 functions when a new mode flag is added), this function
    //  clears all mode flags and then sets the passed in mode flag to true, allowing only one mode to be active.
    mIsBuyMode = false;
    mIsSellMode = false;
    mIsBuybackMode = false;
    mIsGemsMode = false;
    mIsUnlockMode = false;

    //--Set provided flag to true.
    sModeFlag = true;
}
void AdvUIVendor::ResetCursor()
{
    ///--[Documentation]
    //--Resets cursor and immediately completes it. Removes redundancy when setting modes.
    mCursor = 0;
    mSkip = 0;

    //--Recompute.
    RecomputeCursorPositions();

    //--Complete.
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}
void AdvUIVendor::HandleExit()
{
    ///--[Documentation]
    //--Common handler for the player pressing cancel to exit the UI.
    FlagExit();

    //--Call teardown script if it exists.
    if(mShopTeardownPath && strcasecmp(mShopTeardownPath, "Null"))
    {
        LuaManager::Fetch()->ExecuteLuaFile(mShopTeardownPath);
        ResetString(mShopTeardownPath, NULL);
    }

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|Select");
    return;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIVendor::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Run common timers and route to a submode as necessary.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--Visibility.
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, true);
    StandardTimer(mDetailsTimer,    0, cxAdvDetTicks,       mIsShowingDetails);

    //--Comparison character timer.
    if(mCompareSwapTimer < cxAdvCharSwapTicks) mCompareSwapTimer ++;

    //--Arrow oscillation.
    mArrowTimer = (mArrowTimer + 1) % cxAdvOscTicks;

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
    mCategoryHighlightPos.Increment(EASING_CODE_QUADOUT);
    mCategoryHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Mode Calls]
    if(mIsBuyMode)
    {
        UpdateModeBuy();
    }
    else if(mIsSellMode)
    {
        UpdateModeSell();
    }
    else if(mIsBuybackMode)
    {
        UpdateModeBuyback();
    }
    else if(mIsGemsMode)
    {
        UpdateModeGems();
    }
    else if(mIsUnlockMode)
    {
        UpdateModeUnlock();
    }

    ///--[Finish Up]
    return true;
}
void AdvUIVendor::UpdateBackground()
{
    ///--[Documentation]
    //--Decrement visibility and stop.
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
    StandardTimer(mDetailsTimer,    0, cxAdvDetTicks,       false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIVendor::RenderForeground(float pVisAlpha)
{
    ///--[Documentation]
    //--Calls the base render routine, but draws a backing based on the local visibility. This is because
    //  the base menu does not render a backing for the vendor menu.

    //--Backing.
    float tBackingPct = (float)mVisibilityTimer / (float)mVisibilityTimerMax;
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * tBackingPct));

    //--Call base.
    StarUIPiece::RenderForeground(pVisAlpha);
}
void AdvUIVendor::RenderPieces(float pVisAlpha)
{
    //--Base call for all the pieces.
    StarUIPiece::RenderPieces(pVisAlpha);

    //--Unaligned pieces.
    if(mIsBuyMode)     { RenderUnalignedBuy(pVisAlpha);     return; }
    if(mIsSellMode)    { RenderUnalignedSell(pVisAlpha);    return; }
    if(mIsBuybackMode) { RenderUnalignedBuyback(pVisAlpha); return; }
    if(mIsUnlockMode)  { RenderUnalignedUnlock(pVisAlpha);  return; }
    if(mIsGemsMode)
    {
        RenderUnalignedGems(pVisAlpha);
        RenderGemsMerge(pVisAlpha);
        return;
    }
}
void AdvUIVendor::RenderLft(float pVisAlpha)
{
    ///--[Documentation]
    //--Calls all of the submode renderers.
    if(mIsBuyMode)     { RenderLftBuy(pVisAlpha);     return; }
    if(mIsSellMode)    { RenderLftSell(pVisAlpha);    return; }
    if(mIsBuybackMode) { RenderLftBuyback(pVisAlpha); return; }
    if(mIsUnlockMode)  { RenderLftUnlock(pVisAlpha);  return; }
}
void AdvUIVendor::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Calls all of the submode renderers.
    if(mIsBuyMode)     { RenderTopBuy(pVisAlpha);     return; }
    if(mIsSellMode)    { RenderTopSell(pVisAlpha);    return; }
    if(mIsBuybackMode) { RenderTopBuyback(pVisAlpha); return; }
    if(mIsGemsMode)    { RenderTopGems(pVisAlpha);    return; }
    if(mIsUnlockMode)  { RenderTopUnlock(pVisAlpha);  return; }
}
void AdvUIVendor::RenderRgt(float pVisAlpha)
{
    ///--[Documentation]
    //--Calls all of the submode renderers.
    if(mIsBuyMode)     { RenderRgtBuy(pVisAlpha);     return; }
    if(mIsSellMode)    { RenderRgtSell(pVisAlpha);    return; }
    if(mIsBuybackMode) { RenderRgtBuyback(pVisAlpha); return; }
    if(mIsGemsMode)    { RenderRgtGems(pVisAlpha);    return; }
    if(mIsUnlockMode)  { RenderRgtUnlock(pVisAlpha);  return; }
}
void AdvUIVendor::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Calls all of the submode renderers.
    if(mIsBuyMode)     { RenderBotBuy(pVisAlpha);     return; }
    if(mIsSellMode)    { RenderBotSell(pVisAlpha);    return; }
    if(mIsBuybackMode) { RenderBotBuyback(pVisAlpha); return; }
    if(mIsGemsMode)    { RenderBotGems(pVisAlpha);    return; }
    if(mIsUnlockMode)  { RenderBotUnlock(pVisAlpha);  return; }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
