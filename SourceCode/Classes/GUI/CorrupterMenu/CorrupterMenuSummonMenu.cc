//--Base
#include "CorrupterMenu.h"

//--Classes
#include "PerkPack.h"
#include "VisualLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
//====================================== Property Queries =========================================
bool CorrupterMenu::IsSummonMode()
{
    return (mCurrentMode == CM_MODE_SUMMON);
}

//========================================= Manipulators ==========================================
void CorrupterMenu::SetToSummonMode()
{
    //--Flags.
    mCurrentMode = CM_MODE_SUMMON;
    mHighlightedSummonIndex = -1;
}
void CorrupterMenu::SetSummonExecScript(const char *pPath)
{
    ResetString(mSummonExecutionScript, pPath);
}

//========================================= Core Methods ==========================================
void CorrupterMenu::ExecuteSummon(SummonPack *pPack)
{
    //--Executes the lua script which does the work and provides the prototype slot. The script will
    //  determine if the summoning failed for any reason, and will handle turn-end cases.
    if(!mSummonExecutionScript || !pPack) return;

    //--Exec.
    LuaManager::Fetch()->ExecuteLuaFile(mSummonExecutionScript, 2, "N", (float)pPack->mPrototypeSlot, "N", (float)pPack->mManaCost);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void CorrupterMenu::UpdateSummonMode()
{
    //--[Documentation]
    //--Update for Summons Mode. Highlight a monster to check its properties, click to bring up a
    //  dialog box to summon it.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--[Confirmation Dialog]
    //--Dialogue that asks the player if they want to really perform the given action. Can also be a warning
    //  of not having enough MP.
    if(mShowConfirmDialog)
    {
        //--Check left-click case.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--There is always a cancel-out button, even in not-enough-mp mode.
            if(IsPointWithin2DReal(tMouseX, tMouseY, mConfirmCancelBtn))
            {
                AudioManager::Fetch()->PlaySound("Menu|Select");
                mHighlightedSummonIndex = -1;
                mShowConfirmDialog = false;
                return;
            }

            //--The Accept button.
            if(!mIsDialogNotEnoughMP && IsPointWithin2DReal(tMouseX, tMouseY, mConfirmYesBtn))
            {
                AudioManager::Fetch()->PlaySound("Menu|Select");
                mShowConfirmDialog = false;
                SetVisibility(false);
                ExecuteSummon((SummonPack *)xSummonPackList->GetElementBySlot(mHighlightedSummonIndex));
                return;
            }
        }
    }
    //--[Regular Menu]
    //--Allow the player to preview their summon choice, and click to summon.
    else
    {
        //--Store.
        int tPrevIndex = mHighlightedSummonIndex;

        //--Figure out the active slot.
        float cStartPoint = mHeaderMinionsBtn.mBot + CM_HEADER_INDENT + (CM_Y_SPACING_PER_MONSTER * 1.5f) - 8.0f;
        mHighlightedSummonIndex = (tMouseY - cStartPoint) / CM_Y_SPACING_PER_MONSTER;
        if(tMouseY < cStartPoint) mHighlightedSummonIndex = -1;

        //--Check left-click case.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Get the highlighted pack.
            SummonPack *rSummonPack = (SummonPack *)xSummonPackList->GetElementBySlot(mHighlightedSummonIndex);
            if(!rSummonPack) return;

            //--Bring up the dialog box.
            mShowConfirmDialog = true;
            mShowConfirmDialog = (xPlayerMP >= rSummonPack->mManaCost);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Changed highlight.
        if(tPrevIndex != mHighlightedSummonIndex && mHighlightedSummonIndex >= 0 && mHighlightedSummonIndex < xSummonPackList->GetListSize())
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void CorrupterMenu::RenderSummonMode()
{
    //--[Documentation]
    //--Renders what monsters the player can summon. Clicking on a monster will bring up a confirmation
    //  dialog box, which is handled below.
    if(!Images.mIsReady) return;

    //--Setup.
    float tYCursor = mHeaderMinionsBtn.mBot + CM_HEADER_INDENT;

    //--[Heading Information]
    //--Labels above everything.
    Images.Data.rUIFont->DrawText(cXPosName, tYCursor, 0, 1.0f, "Name");

    //--HP.
    Images.Data.rUIFont->DrawText(cXPosHP+100, tYCursor, 0, 1.0f, "Cost");

    //--[Summon Information]
    //--Move the cursor down 1.5 entries.
    tYCursor = tYCursor + (CM_Y_SPACING_PER_MONSTER * 1.5f);

    //--Render the summon packs.
    int i = 0;
    SummonPack *rSummonPack = (SummonPack *)xSummonPackList->PushIterator();
    while(rSummonPack)
    {
        //--Underlay.
        /*if(mHighlightedSummonIndex == i && !mShowConfirmDialog)
        {
            glLineWidth(3.0f);
            glColor3f(0.75f, 0.75f, 0.75f);
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_LINE_LOOP);
                glVertex2f(cXPosIcon, tYCursor - 8.0f);
                glVertex2f(  cXPosWP, tYCursor - 8.0f);
                glVertex2f(  cXPosWP, tYCursor - 8.0f + CM_Y_SPACING_PER_MONSTER);
                glVertex2f(cXPosIcon, tYCursor - 8.0f + CM_Y_SPACING_PER_MONSTER);
            glEnd();
            glEnable(GL_TEXTURE_2D);
            glColor3f(1.0f, 1.0f, 1.0f);
        }*/

        //--Render the icon.
        if(rSummonPack->rIconImg)
        {
            rSummonPack->rIconImg->Draw(cXPosIcon, tYCursor - 8.0f);
        }

        //--If the MP cost is higher than the current mana, grey this out.
        if(rSummonPack->mManaCost > xPlayerMP)
        {
            mUnavailableCol.SetAsMixer();
        }
        //--Selected case.
        else if(mHighlightedSummonIndex == i)
        {
            mHighlightCol.SetAsMixer();
        }
        //--Normal case.
        else
        {
            mNormalTextCol.SetAsMixer();
        }

        //--Name of the summoning pack.
        Images.Data.rUIFont->DrawTextArgs(cXPosName, tYCursor, 0, 1.0f, rSummonPack->mSummonName);

        //--MP Cost.
        Images.Data.rUIFont->DrawTextArgs(cXPosHP+100.0f, tYCursor, 0, 1.0f, "%i", rSummonPack->mManaCost);

        //--Next.
        i ++;
        StarlightColor::ClearMixer();
        tYCursor = tYCursor + CM_Y_SPACING_PER_MONSTER;
        rSummonPack = (SummonPack *)xSummonPackList->AutoIterate();
    }

    //--Summon information for the highlighted monster. Includes stats and description.
    rSummonPack = (SummonPack *)xSummonPackList->GetElementBySlot(mHighlightedSummonIndex);
    if(!rSummonPack) return;

    //--Render the portrait.
    if(rSummonPack->rRenderImg)
    {
        float cXRender = (VIRTUAL_CANVAS_X * 0.60f) - (rSummonPack->rRenderImg->GetTrueWidth() * 0.5f);
        rSummonPack->rRenderImg->Draw(cXRender, mHeaderMinionsBtn.mBot - CM_HEADER_INDENT);
    }

    //--Army of Monsters: If this perk exists and has been unlocked, it gets added to the monsters HP/WP.
    int tArmyOfMonstersUnlocks = 0;
    PerkPack *rArmyOfMonstersPack = (PerkPack *)PerkPack::xPerkList->GetElementByName("Army of Monsters");
    if(rArmyOfMonstersPack) tArmyOfMonstersUnlocks = rArmyOfMonstersPack->GetUnlockCount();

    //--Health/Willpower Information
    tYCursor = VIRTUAL_CANVAS_Y - CM_HEADER_INDENT - CM_BTN_TEXT_SIZE - (CM_BTN_TEXT_SIZE*6.0f);
    Images.Data.rUIFont->DrawTextArgs(cXPosLocation - 80.0f, tYCursor + (CM_BTN_TEXT_SIZE * 0.0f), 0, 1.0f, "Cost: %i MP", rSummonPack->mManaCost);
    Images.Data.rUIFont->DrawTextArgs(cXPosLocation - 80.0f, tYCursor + (CM_BTN_TEXT_SIZE * 1.0f), 0, 1.0f, "HP: %i  WP: %i", rSummonPack->mHP + tArmyOfMonstersUnlocks, rSummonPack->mWP + tArmyOfMonstersUnlocks);
    for(int i = 0; i < SUMMON_PACK_DESCRIPTION_LINES; i ++)
    {
        Images.Data.rUIFont->DrawTextArgs(cXPosLocation - 80.0f, tYCursor + (CM_BTN_TEXT_SIZE * (2.0f + i)), 0, 1.0f, "%s", rSummonPack->mDescriptionLines[i]);
    }

    //--If present, render the confirmation dialog over everything else.
    if(mShowConfirmDialog) RenderSummonConfirmation();
}
void CorrupterMenu::RenderSummonConfirmation()
{
    //--Renders a card over the screen requesting confirmation.
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mConfirmBody.mLft, mConfirmBody.mTop, mConfirmBody.mRgt, mConfirmBody.mBot, 0x01FF);

    //--Render the cancel button. Always renders.
    VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mConfirmCancelBtn, CM_BTN_INDENT, "Cancel");

    //--Render the confirm button. Only renders if the player had enough MP.
    if(!mIsDialogNotEnoughMP) VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mConfirmYesBtn, CM_BTN_INDENT, "Confirm");

    //--Render text for the confirmation case.
    if(!mIsDialogNotEnoughMP)
    {
        //--Get the MP cost.
        int tRenderMP = -1;
        SummonPack *rSummonPack = (SummonPack *)xSummonPackList->GetElementBySlot(mHighlightedSummonIndex);
        if(rSummonPack) tRenderMP = rSummonPack->mManaCost;

        //--Text.
        Images.Data.rUIFont->DrawText    (mConfirmBody.mLft + CM_HEADER_INDENT, mConfirmBody.mTop + CM_HEADER_INDENT + (CM_BTN_TEXT_SIZE * 0.0f),          "Really summon this monster?");
        Images.Data.rUIFont->DrawTextArgs(mConfirmBody.mLft + CM_HEADER_INDENT, mConfirmBody.mTop + CM_HEADER_INDENT + (CM_BTN_TEXT_SIZE * 1.0f), 0, 1.0f, "Your MP: %i", xPlayerMP);
        Images.Data.rUIFont->DrawTextArgs(mConfirmBody.mLft + CM_HEADER_INDENT, mConfirmBody.mTop + CM_HEADER_INDENT + (CM_BTN_TEXT_SIZE * 2.0f), 0, 1.0f, "Cost: %i", tRenderMP);
    }
    //--Indicate the player can't summon this monster.
    else
    {
        //--Get the MP cost.
        int tRenderMP = -1;
        SummonPack *rSummonPack = (SummonPack *)xSummonPackList->GetElementBySlot(mHighlightedSummonIndex);
        if(rSummonPack) tRenderMP = rSummonPack->mManaCost;

        //--Text.
        Images.Data.rUIFont->DrawText    (mConfirmBody.mLft + CM_HEADER_INDENT, mConfirmBody.mTop + CM_HEADER_INDENT + (CM_BTN_TEXT_SIZE * 0.0f),          "Not enough MP!");
        Images.Data.rUIFont->DrawTextArgs(mConfirmBody.mLft + CM_HEADER_INDENT, mConfirmBody.mTop + CM_HEADER_INDENT + (CM_BTN_TEXT_SIZE * 1.0f), 0, 1.0f, "Your MP: %i", xPlayerMP);
        Images.Data.rUIFont->DrawTextArgs(mConfirmBody.mLft + CM_HEADER_INDENT, mConfirmBody.mTop + CM_HEADER_INDENT + (CM_BTN_TEXT_SIZE * 2.0f), 0, 1.0f, "Cost: %i", tRenderMP);
    }
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
