//--Base
#include "GalleryMenu.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

///========================================== System ==============================================
GalleryMenu::GalleryMenu()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_GALLERYMENU;

    ///--[GalleryMenu]
    //--System
    //--Sizing
    cTextSize = 22.0f;
    cButtonIndentX = 32.0f;
    cButtonIndentY = 32.0f;
    cButtonSizeX = 96.0f;
    cButtonSizeY = cTextSize + 3.0f;
    cButtonSpaceY = cButtonSizeY + 2.0f;

    //--Entries
    mSelectedEntry = -1;
    mEntryList = new StarLinkedList(true);

    //--Associated Text
    for(int x = 0; x < 10; x ++)
    {
        for(int y = 0; y < 256; y ++)
        {
            mAssociatedText[x][y] = '\0';
        }
    }

    //--Images
    memset(&Images, 0, sizeof(Images));
}
GalleryMenu::~GalleryMenu()
{
    delete mEntryList;
}
void GalleryMenu::Construct()
{
    //--Resolves the images required for rendering.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Base Images
    Images.Data.rBorderCard = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/3DUI/CardBorder");
    Images.Data.rUIFont = rDataLibrary->GetFont("Gallery Menu Main");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(StarBitmap *));
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void GalleryMenu::ClearEntries()
{
    mSelectedEntry = -1;
    mEntryList->ClearList();
}
void GalleryMenu::AddEntry(const char *pTitle, const char *pText, const char *pImagePath)
{
    if(!pTitle || !pText || !pImagePath) return;
    SetMemoryData(__FILE__, __LINE__);
    GalleryEntry *nEntry = (GalleryEntry *)starmemoryalloc(sizeof(GalleryEntry));
    nEntry->mDisplayTitle = InitializeString(pTitle);
    nEntry->mAccompanyText = InitializeString(pText);
    nEntry->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    mEntryList->AddElement(pTitle, nEntry, &GalleryEntry::DeleteThis);
}
void GalleryMenu::SetSelectedEntry(int pIndex)
{
    //--Sets all the backend for changing the entry. If it results in -1, clears everything for you. Passing the total
    //  -1 will return to the previous menu.
    if((pIndex == 0 && mEntryList->GetListSize() == 0) || (pIndex == mEntryList->GetListSize() - 1))
    {
        MapManager::Fetch()->HideGallery();
        return;
    }

    //--Nothing to be done. Don't re-parse the text.
    if(mSelectedEntry == pIndex) return;

    //--Range check.
    if(pIndex < 0 || pIndex >= mEntryList->GetListSize())
    {
        //--If mSelectedEntry is already -1, do nothing.
        if(mSelectedEntry == -1) return;

        //--Otherwise, flag...
        mSelectedEntry = -1;
        return;
    }

    //--Clear the display text.
    for(int i = 0; i < 10; i ++)
    {
        mAssociatedText[i][0] = '\0';
    }

    //--We need to populate new text. First, the flag.
    mSelectedEntry = pIndex;

    //--Now populate text. First, get the entry and check it.
    GalleryEntry *rEntry = (GalleryEntry *)mEntryList->GetElementBySlot(pIndex);
    if(!rEntry) { SetSelectedEntry(-1); return; }

    //--Base string.
    const char *rSourceString = rEntry->mAccompanyText;

    //--Constants/Setup.
    float cTextSize = 18.0f;
    float tAllowedLength = VIRTUAL_CANVAS_X * 0.65f;
    StarFont *rUseFont = Images.Data.rUIFont;
    if(!rUseFont) { SetSelectedEntry(-1); return; }

    //--Pare the string down one line at a time.
    int tRunningCursor = 0;
    int tLine = 0;
    while(true)
    {
        //--Setup.
        bool tHitStringEdge = true;
        int tCursor = 0;

        //--Move characters into a new buffer until the string length is hit.
        while(tCursor < 255 && tRunningCursor < (int)strlen(rSourceString))
        {
            //--Append the letter.
            mAssociatedText[tLine][tCursor+0] = rSourceString[tRunningCursor];
            mAssociatedText[tLine][tCursor+1] = '\0';

            //--Check the length.
            float tNewLength = rUseFont->GetTextWidth(mAssociatedText[tLine]) * cTextSize / rUseFont->GetTextHeight();

            //--If it's too long, we're done.
            if(tNewLength > tAllowedLength) { tHitStringEdge = false; break; }

            //--Otherwise, go up one.
            tCursor ++;
            tRunningCursor ++;
        }

        //--Did we hit the edge of the string? If so, we're done.
        if(tHitStringEdge) break;

        //--Now scan backwards until we hit a space. This is the cutoff point.
        int tCutoffLetter = -1;
        for(int i = (int)strlen(mAssociatedText[tLine]) - 1; i >= 0; i --)
        {
            if(mAssociatedText[tLine][i] == ' ')
            {
                mAssociatedText[tLine][i] = '\0';
                tCutoffLetter = i+1;
                break;
            }
        }

        //--If the cutoff is -1, then the line has no breaks. In that case, just go to the next line.
        if(tCutoffLetter == -1)
        {

        }
        //--If the cutoff is not -1, cut the string off there.
        else
        {
            tRunningCursor = tRunningCursor - (tCursor - tCutoffLetter);
        }

        //--Next line.
        tLine ++;
    }
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void GalleryMenu::Update()
{
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Left-click.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Get mouse position.
        float tMouseX, tMouseY, tMouseZ;
        rControlManager->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);

        //--Button changing case.
        int tSelectedEntry = -1;

        //--Left range check.
        if(tMouseX >= 8.5f + 15.0f && tMouseX <= 8.5f + 15.0f + cButtonSizeX)
        {
            //--Compute.
            tSelectedEntry = (tMouseY - (8.5f + 15.0f)) / cButtonSpaceY;
        }

        //--Update the back-end.
        SetSelectedEntry(tSelectedEntry);
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void GalleryMenu::Render()
{
    //--Resolve images the first time this objects attempts to render. If the images fail to resolve,
    //  stop rendering.
    if(!Images.mIsReady)
    {
        Construct();
        if(!Images.mIsReady) return;
    }

    //--Flag. We want to use the more transparent version, which is based on the xUseAlternateUI flag.
    bool tOldFlag = VisualLevel::xUseAlternateUI;
    VisualLevel::xUseAlternateUI = true;

    //--Border sizing.
    float cIndent = 15.0f;
    float tOtLft = cIndent;
    float tOtTop = cIndent;
    float tOtRgt = VIRTUAL_CANVAS_X - cIndent;
    float tOtBot = VIRTUAL_CANVAS_Y - cIndent;

    //--Background.
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, tOtLft, tOtTop, tOtRgt, tOtBot, 0x01FF);

    //--Button Setup.
    float cBtnIndent = 4.5f;
    float tXCursor = 8.5f + cIndent;
    float tYCursor = 8.5f + cIndent;

    //--On the left side, render the menu buttons.
    int tCount = 0;
    GalleryEntry *rEntry = (GalleryEntry *)mEntryList->PushIterator();
    while(rEntry)
    {
        //--Render a box around the button.
        float cLft = tXCursor;
        float cTop = tYCursor;
        float cRgt = cLft + cBtnIndent + Images.Data.rUIFont->GetTextWidth(rEntry->mDisplayTitle) + cBtnIndent;
        float cBot = cTop + cBtnIndent + 18.0f + cBtnIndent;
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, cLft, cTop, cRgt, cBot, 0x01EF);

        //--Render the name.
        Images.Data.rUIFont->DrawText(tXCursor+cBtnIndent, tYCursor+cBtnIndent, 0, 1.0f, rEntry->mDisplayTitle);

        //--If this is the selected entry, render the associated image.
        if(mSelectedEntry == tCount && rEntry->rImage)
        {
            //--Translate to position.
            float tImgSizeX = rEntry->rImage->GetTrueWidth();
            float tImgSizeY = rEntry->rImage->GetTrueHeight();
            float cImgLft = (VIRTUAL_CANVAS_X * 0.65f) - (tImgSizeX * 0.5f);
            float cImgTop = (VIRTUAL_CANVAS_Y * 0.44f) - (tImgSizeY * 0.5f);
            float cScale = 1.0f;

            //--Render standard.
            if(mSelectedEntry < 6)
            {
            }
            //--Render large.
            else
            {
                //--Allowed sizes.
                float cAllowedSizeX = VIRTUAL_CANVAS_X * 0.65f;
                float cAllowedSizeY = VIRTUAL_CANVAS_Y * 0.80f;

                //--Too wide, but not too tall.
                if(tImgSizeX > cAllowedSizeX && tImgSizeY <= cAllowedSizeY)
                {
                    cScale = cAllowedSizeX / tImgSizeX;
                }
                //--Too tall, but not too wide.
                else if(tImgSizeX <= cAllowedSizeX && tImgSizeY > cAllowedSizeY)
                {
                    cScale = cAllowedSizeY / tImgSizeY;
                }
                //--Both too tall and too wide.
                else
                {
                    float cScaleX = cAllowedSizeX / tImgSizeX;
                    float cScaleY = cAllowedSizeY / tImgSizeY;
                    if(cScaleX <= cScaleY)
                    {
                        cScale = cScaleX;
                    }
                    else
                    {
                        cScale = cScaleY;
                    }
                }

                //--Recompute the lft/top positions.
                cImgLft = (VIRTUAL_CANVAS_X * 0.65f) - (tImgSizeX * 0.5f * cScale);
                cImgTop = (VIRTUAL_CANVAS_Y * 0.44f) - (tImgSizeY * 0.5f * cScale);
            }

            //--Render and clean up.
            glTranslatef(cImgLft, cImgTop, 0.0f);
            if(cScale != 1.0f) glScalef(cScale, cScale, 1.0f);
            rEntry->rImage->Draw();
            if(cScale != 1.0f) glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
            glTranslatef(-cImgLft, -cImgTop, 0.0f);

            //--Also render the text at the bottom. This may cross multiple lines.
            RenderAccompanyText(rEntry->mAccompanyText, VIRTUAL_CANVAS_Y * 0.85f);
        }

        //--Next.
        tCount ++;
        tYCursor = tYCursor + cButtonSpaceY;
        rEntry = (GalleryEntry *)mEntryList->AutoIterate();
    }

    //--Clean up.
    VisualLevel::xUseAlternateUI = tOldFlag;
}
void GalleryMenu::RenderAccompanyText(const char *pText, float pYPosition)
{
    //--Given a set of text to accompany an image, render it at the bottom of the gallery. This
    //  routine will automatically split the string if the lines run too long.
    if(!Images.mIsReady) return;

    //--Constants.
    float cScale = 0.85f;
    float cTextSize = 20.0f;
    float cXPosition = VIRTUAL_CANVAS_X * 0.20f;

    //--Render.
    int i = 0;
    while(i < 10 && mAssociatedText[i][0] != '\0')
    {
        Images.Data.rUIFont->DrawText(cXPosition, pYPosition + ((cTextSize + 1.0f) * (float)i * cScale), 0, cScale, mAssociatedText[i]);
        i ++;
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
GalleryMenu *GalleryMenu::Fetch()
{
    return MapManager::Fetch()->GetGalleryMenu();
}

///======================================== Lua Hooking ===========================================
void GalleryMenu::HookToLuaState(lua_State *pLuaState)
{
    /* GM_Show()
       Shows the Gallery Menu. Does not follow the usual stack push/pop behavior for menus. */
    lua_register(pLuaState, "GM_Show", &Hook_GM_Show);

    /* GM_Hide()
       Hides the Gallery Menu. */
    lua_register(pLuaState, "GM_Hide", &Hook_GM_Hide);

    /* GM_Clear()
       Clears all the entries on the Gallery Menu. */
    lua_register(pLuaState, "GM_Clear", &Hook_GM_Clear);

    /* GM_AddEntry(sDisplayName, sText, sImgPath)
       Adds a new entry to the Gallery Menu. sDisplayName will appear on the left side and the
       provided text appears beneath the image when selected. */
    lua_register(pLuaState, "GM_AddEntry", &Hook_GM_AddEntry);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_GM_Show(lua_State *L)
{
    //GM_Show()
    MapManager::Fetch()->ShowGallery();
    return 0;
}
int Hook_GM_Hide(lua_State *L)
{
    //GM_Hide()
    MapManager::Fetch()->HideGallery();
    return 0;
}
int Hook_GM_Clear(lua_State *L)
{
    //GM_Clear()
    GalleryMenu::Fetch()->ClearEntries();
    return 0;
}
int Hook_GM_AddEntry(lua_State *L)
{
    //GM_AddEntry(sDisplayName, sText, sImgPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("GM_AddEntry");

    GalleryMenu::Fetch()->AddEntry(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));
    return 0;
}
