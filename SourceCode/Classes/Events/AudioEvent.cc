//--Base
#include "AudioEvent.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"

///========================================== System ==============================================
AudioEvent::AudioEvent()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_EVENT_AUDIO;

    ///--[RootEvent]
    //--System
    //--Script Execution
    //--Data Storage

    ///--[AudioEvent]
    //--System
    //--Storage
    mIsMusic = false;
    mSoundPath = NULL;
    mDelay = 0;
}
AudioEvent::~AudioEvent()
{
    free(mSoundPath);
}

///===================================== Property Queries =========================================
bool AudioEvent::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_EVENT_ROOT) return true;
    if(pType == POINTER_TYPE_EVENT_AUDIO) return true;
    return false;
}

///======================================= Manipulators ===========================================
void AudioEvent::SetDelay(int pTicks)
{
    mDelay = pTicks;
}
void AudioEvent::SetMusic(const char *pMusicName)
{
    mIsMusic = true;
    ResetString(mSoundPath, pMusicName);
}
void AudioEvent::SetSound(const char *pSoundName)
{
    mIsMusic = false;
    ResetString(mSoundPath, pSoundName);
}

///======================================= Core Methods ===========================================
bool AudioEvent::IsComplete()
{
    //--Returns whether or not the timer has expired.
    return (mDelay < 1);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void AudioEvent::Update()
{
    //--Counts down the delay timer. If it's zero, play the sound/music.
    mDelay --;
    if(mDelay < 1)
    {
        //--Sound effect.
        if(!mIsMusic)
        {
            AudioManager::Fetch()->PlaySound(mSoundPath);
        }
        //--Music.
        else
        {
            AudioManager::Fetch()->PlayMusic(mSoundPath);
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void AudioEvent::HookToLuaState(lua_State *pLuaState)
{
    /* AudioEvent_SetProperty("Delay", iTimer)
       Sets the given property in the active RootEvent or derived type. */
    lua_register(pLuaState, "AudioEvent_SetProperty", &Hook_AudioEvent_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AudioEvent_SetProperty(lua_State *L)
{
    //AudioEvent_SetProperty("Delay", iTimer)
    //AudioEvent_SetProperty("Sound", sSoundName)
    //AudioEvent_SetProperty("Music", sMusicName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AudioEvent_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Instantiated Cases]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    AudioEvent *rEvent = (AudioEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_AUDIO)) return LuaTypeError("AudioEvent_SetProperty", L);

    //--Number of ticks to count down. Audio plays when it reaches zero. If not set, event executes immediately.
    if(!strcasecmp(rSwitchType, "Delay") && tArgs == 2)
    {
        rEvent->SetDelay(lua_tointeger(L, 2));
    }
    //--SFX.
    else if(!strcasecmp(rSwitchType, "Sound") && tArgs == 2)
    {
        rEvent->SetSound(lua_tostring(L, 2));
    }
    //--Music.
    else if(!strcasecmp(rSwitchType, "Music") && tArgs == 2)
    {
        rEvent->SetMusic(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("AudioEvent_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
