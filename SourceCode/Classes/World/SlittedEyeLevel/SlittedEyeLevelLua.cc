//--Base
#include "SlittedEyeLevel.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "MapManager.h"

///======================================== Lua Hooking ===========================================
void SlittedEyeLevel::HookToLuaState(lua_State *pLuaState)
{
    /* SEL_Create()
       Creates a SlittedEyeLevel and sends it to the MapManager as the active level. */
    lua_register(pLuaState, "SEL_Create", &Hook_SEL_Create);

    /* See below for full argument list. */
    lua_register(pLuaState, "SEL_GetProperty", &Hook_SEL_GetProperty);

    /* See below for full argument list. */
    lua_register(pLuaState, "SEL_SetProperty", &Hook_SEL_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_SEL_Create(lua_State *L)
{
    //SEL_Create()
    SlittedEyeLevel *nNewLevel = new SlittedEyeLevel();
    MapManager::Fetch()->ReceiveLevel(nNewLevel);

    //--Finish up.
    return 0;
}
int Hook_SEL_GetProperty(lua_State *L)
{
    ///--[Argument List]
    //--[Static]
    //--[System]
    //SEL_GetProperty("Name") (1 String)

    //--[Player Statistics]
    //SEL_GetProperty("Game Time") (1 Integer)
    //SEL_GetProperty("Stamina") (1 Float)
    //SEL_GetProperty("Noise") (1 Float)

    ///--[Arguments]
    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SEL_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Values]
    //--Static Example.
    if(!strcasecmp(rSwitchType, "Static Sample") && tArgs == 1)
    {
        lua_pushinteger(L, 1);
        return 1;
    }

    ///--[Dynamic Types]
    //--Active object.
    SlittedEyeLevel *rActiveLevel = SlittedEyeLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("SEL_GetProperty", L);

    ///--[System]
    //--Name of the current level.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        lua_pushstring(L, rActiveLevel->GetName());
        tReturns = 1;
    }
    ///--[Player Statistics]
    //--Timer used to display what time it is in-game.
    else if(!strcasecmp(rSwitchType, "Game Time") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveLevel->GetPlayerTime());
        tReturns = 1;
    }
    //--Player's remaining stamina.
    else if(!strcasecmp(rSwitchType, "Stamina") && tArgs == 1)
    {
        lua_pushnumber(L, rActiveLevel->GetPlayerStamina());
        tReturns = 1;
    }
    //--Noise level. When it reaches 100, the player loses.
    else if(!strcasecmp(rSwitchType, "Noise") && tArgs == 1)
    {
        lua_pushnumber(L, rActiveLevel->GetPlayerNoise());
        tReturns = 1;
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("SEL_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_SEL_SetProperty(lua_State *L)
{
    ///--[Argument List]
    //--[Static System]
    //--[System]
    //SEL_SetProperty("Name", sName)
    //SEL_SetProperty("Update Path", sPath)
    //SEL_SetProperty("Game Over Path", sPath)
    //SEL_SetProperty("Allocate Images", iImagesTotal)
    //SEL_SetProperty("Set Image", iSlot, sDLPath)
    //SEL_SetProperty("Set Disallow Scrolling", bFlag)

    //--[Player Statistics]
    //SEL_SetProperty("Disable Hiss", bFlag)
    //SEL_SetProperty("Disable Manipulate", bFlag)
    //SEL_SetProperty("Allow Game Over", bFlag)
    //SEL_SetProperty("Game Time", iTime)
    //SEL_SetProperty("Stamina", fAmount)
    //SEL_SetProperty("Noise", fAmount)
    //SEL_SetProperty("Stamina Loss Per Hiss", fAmount)

    //--[Hiss Hitbox Handling]
    //SEL_SetProperty("Register Hiss Hitbox", sName, fLft, fTop, fRgt, fBot, sPath, fFiringCode)
    //SEL_SetProperty("Flag Hiss Hitbox For Deletion", sName)

    //--[Fading]
    //SEL_SetProperty("Begin Fade", iTicks, bPersists, fStartR, fStartG, fStartB, fStartA, fEndR, fEndG, fEndB, fEndA)
    //SEL_SetProperty("Begin Fade Using Prev", iTicks, bPersists, fEndR, fEndG, fEndB, fEndA)

    ///--[Arguments]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SEL_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Values]
    //--Dummy
    if(!strcasecmp(rSwitchType, "Handle Update Call") && tArgs == 1)
    {
        return 0;
    }

    ///--[Dynamic Values]
    //--Active object.
    SlittedEyeLevel *rActiveLevel = SlittedEyeLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("SEL_SetProperty", L);

    ///--[System]
    //--Internal name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rActiveLevel->SetName(lua_tostring(L, 2));
    }
    //--Script called to handle updating the game state.
    else if(!strcasecmp(rSwitchType, "Update Path") && tArgs == 2)
    {
        rActiveLevel->SetUpdatePath(lua_tostring(L, 2));
    }
    //--Script called when the player presses the button to continue after a game over.
    else if(!strcasecmp(rSwitchType, "Game Over Path") && tArgs == 2)
    {
        rActiveLevel->SetGameOverPath(lua_tostring(L, 2));
    }
    //--Allocates space for images in the level.
    else if(!strcasecmp(rSwitchType, "Allocate Images") && tArgs == 2)
    {
        rActiveLevel->AllocateImages(lua_tointeger(L, 2));
    }
    //--Specifies the image in the slot for the level.
    else if(!strcasecmp(rSwitchType, "Set Image") && tArgs == 3)
    {
        rActiveLevel->SetImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Locks the background at zero scroll. Used during fixed-size scenes, like Game Over.
    else if(!strcasecmp(rSwitchType, "Set Disallow Scrolling") && tArgs == 2)
    {
        rActiveLevel->SetDisallowScrolling(lua_toboolean(L, 2));
    }
    ///--[Player Statistics]
    //--Disables hissing. Used for certain sequences.
    else if(!strcasecmp(rSwitchType, "Disable Hiss") && tArgs == 2)
    {
        rActiveLevel->SetHissDisabled(lua_toboolean(L, 2));
    }
    //--Disables manipulation. Used for certain sequences.
    else if(!strcasecmp(rSwitchType, "Disable Manipulate") && tArgs == 2)
    {
        rActiveLevel->SetManipulateDisabled(lua_toboolean(L, 2));
    }
    //--Allows game over. Used for the... game over... sequence. Duh.
    else if(!strcasecmp(rSwitchType, "Allow Game Over") && tArgs == 2)
    {
        rActiveLevel->SetGameOverEnabled(lua_toboolean(L, 2));
    }
    //--Time on the display.
    else if(!strcasecmp(rSwitchType, "Game Time") && tArgs == 2)
    {
        rActiveLevel->SetGameTime(lua_tointeger(L, 2));
    }
    //--Player stamina value.
    else if(!strcasecmp(rSwitchType, "Stamina") && tArgs == 2)
    {
        rActiveLevel->SetStamina(lua_tonumber(L, 2));
    }
    //--Current noise value.
    else if(!strcasecmp(rSwitchType, "Noise") && tArgs == 2)
    {
        rActiveLevel->SetNoise(lua_tonumber(L, 2));
    }
    //--How much stamina the player loses for hissing at something.
    else if(!strcasecmp(rSwitchType, "Stamina Loss Per Hiss") && tArgs == 2)
    {
        rActiveLevel->SetStaminaLossPerHiss(lua_tonumber(L, 2));
    }
    ///--[Hiss Hitbox Handling]
    //--Creates and registers a hissing hitbox.
    else if(!strcasecmp(rSwitchType, "Register Hiss Hitbox") && tArgs == 8)
    {
        rActiveLevel->RegisterHissHitbox(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tostring(L, 7), lua_tonumber(L, 8));
    }
    //--Mark all hiss hitboxes matching the name for deletion at the end of the tick.
    else if(!strcasecmp(rSwitchType, "Flag Hiss Hitbox For Deletion") && tArgs == 2)
    {
        rActiveLevel->FlagHissHitboxForDeletion(lua_tostring(L, 2));
    }
    ///--[Fading]
    //SEL_SetProperty("Begin Fade", iTicks, bPersists, fStartR, fStartG, fStartB, fStartA, fEndR, fEndG, fEndB, fEndA)
    else if(!strcasecmp(rSwitchType, "Begin Fade") && tArgs == 11)
    {
        rActiveLevel->ActivateFade(lua_tointeger(L, 2), lua_toboolean(L, 3), StarlightColor::MapRGBAF(lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7)), StarlightColor::MapRGBAF(lua_tonumber(L, 8), lua_tonumber(L, 9), lua_tonumber(L, 10), lua_tonumber(L, 11)));
    }
    //SEL_SetProperty("Begin Fade Using Prev", iTicks, bPersists, fEndR, fEndG, fEndB, fEndA)
    else if(!strcasecmp(rSwitchType, "Begin Fade Using Prev") && tArgs == 7)
    {
        rActiveLevel->ActivateFadeUsingCurrent(lua_tointeger(L, 2), lua_toboolean(L, 3), StarlightColor::MapRGBAF(lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7)));
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("SEL_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
