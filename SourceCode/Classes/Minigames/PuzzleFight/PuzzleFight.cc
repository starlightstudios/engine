//--Base
#include "PuzzleFight.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
PuzzleFight::PuzzleFight()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_PUZZLEFIGHTGAME;

    ///--[PuzzleFight]
    //--System
    mIsTakingDown = false;
    mIsHandlingUpdates = false;
    mIsComplete = false;
    mUpdateScript = NULL;
    mGameState = MINIGAME_STATE_PLAYERSTURNINIT;
    mQuitSequence = PF_DEBUGQUIT_BACKSPACE;

    //--Title Printing
    mTitlePrintPhase = PF_TITLE_PRINT_NONE;
    mTitlePrintTimer = 0;
    mTitlePrintTimerMax = 1;
    mTitlePrintString = NULL;

    //--UI
    mUISlideTimer = 0;

    //--Dislocation Storage
    mDislocationsTotal = 0;
    mDislocations = NULL;

    //--Board
    mBoardScrollSpeed = 8.0f;
    mBoardStartX = 0;
    mBoardStartY = 0;
    mBoardW = 0;
    mBoardH = 0;
    mBoardTileSizeX = 2;
    mBoardTileSizeY = 2;
    mBoardObjects = NULL;
    rDislocationBoard = NULL;
    mNotifierList = new StarLinkedList(true);

    //--Player
    mPlayerTicksLeft = 0;
    mPlayerMovesLeft = 0;
    mPlayerEntityName = NULL;
    mPlayerHealth = MAX_HEALTH;
    mBoardSlideTimer = 0;
    mBoardActionList = new StarLinkedList(true);
    mPlayerHealthEasing.Initialize();
    mPlayerHealthEasing.MoveTo(MAX_HEALTH, 0);
    mShowConfirmDialogue = false;
    mConfirmTimer = 0;
    mConfirmAccept = new StarlightString();
    mConfirmCancel = new StarlightString();

    //--Challenger
    mChallengerHealth = MAX_HEALTH;
    mChallengerHealthEasing.Initialize();
    mChallengerHealthEasing.MoveTo(MAX_HEALTH, 0);

    //--Cursor
    mUpdateCursorImmediately = false;
    mIsCursorHorizontal = true;
    mCursorSlot = 0;
    mCursorMoveTimer = CURSOR_MOVE_TICKS;
    mCursorStart.Set(0.0f, 0.0f, 1.0f, 1.0f);
    mCursorCurrent.Set(0.0f, 0.0f, 1.0f, 1.0f);
    mCursorFinish.Set(0.0f, 0.0f, 1.0f, 1.0f);

    //--Images
    memset(&Images, 0, sizeof(Images));
}
PuzzleFight::~PuzzleFight()
{
    //--System
    free(mUpdateScript);

    //--Title Printing
    free(mTitlePrintString);

    //--UI
    //--Dislocation Storage
    for(int i = 0; i < mDislocationsTotal; i++) mDislocations[i].Deallocate();
    free(mDislocations);

    //--Board
    for(int x = 0; x < mBoardW; x ++)
    {
        free(mBoardObjects[x]);
        free(rDislocationBoard[x]);
    }
    free(mBoardObjects);
    free(rDislocationBoard);
    delete mNotifierList;

    //--Player
    delete mBoardActionList;
    delete mConfirmAccept;
    delete mConfirmCancel;

    //--Challenger
    //--Cursor
    //--Images
}
void PuzzleFight::Construct()
{
    ///--[Image Resolve]
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts
    Images.Data.rMovesFont        = rDataLibrary->GetFont("Puzzle Battle Moves");
    Images.Data.rTimeFont         = rDataLibrary->GetFont("Puzzle Battle Time");
    Images.Data.rConfirmFont      = rDataLibrary->GetFont("Puzzle Battle Confirm");
    Images.Data.rConfirmSmallFont = rDataLibrary->GetFont("Puzzle Battle Confirm Small");
    Images.Data.rCenterTitleText  = rDataLibrary->GetFont("Puzzle Battle Center Title");

    //--World Pieces
    Images.Data.rHighlight            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|Highlight");
    Images.Data.rArrowDn              = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Arrow Down");
    Images.Data.rArrowRt              = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Arrow Rgt");
    Images.Data.rArrowUp              = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Arrow Up");
    Images.Data.rArrowLf              = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Arrow Lft");
    Images.Data.rBonusBoost           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Boost");
    Images.Data.rBonusGuard           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Guard");
    Images.Data.rActionMelee          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Melee");
    Images.Data.rActionRanged         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Ranged");
    Images.Data.rPowerUpNotifier      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Power Up");
    Images.Data.rPerfectGuardNotifier = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/PuzzleBattle/Field|World Perfect Guard");

    //--UI Pieces
    Images.Data.rHealthFillLft  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Health Fill Lft");
    Images.Data.rHealthFillRgt  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Health Fill Rgt");
    Images.Data.rHealthFrameLft = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Health Frame Lft");
    Images.Data.rHealthFrameRgt = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Health Frame Rgt");
    Images.Data.rMovesBox       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Moves Box");
    Images.Data.rTimeHeader     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Time Header");
    Images.Data.rConfirmBox     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Confirm Box");
    Images.Data.rTextTop        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Text Top");
    Images.Data.rTextBot        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/PuzzleBattle/UI|Text Bottom");

    //--Set lookups for world objects.
    Images.Data.rBoardObjects[BOARD_OBJECT_ARROW_UP] = Images.Data.rArrowUp;
    Images.Data.rBoardObjects[BOARD_OBJECT_ARROW_RT] = Images.Data.rArrowRt;
    Images.Data.rBoardObjects[BOARD_OBJECT_ARROW_DN] = Images.Data.rArrowDn;
    Images.Data.rBoardObjects[BOARD_OBJECT_ARROW_LF] = Images.Data.rArrowLf;
    Images.Data.rBoardObjects[BOARD_OBJECT_BOOST]    = Images.Data.rBonusBoost;
    Images.Data.rBoardObjects[BOARD_OBJECT_GUARD]    = Images.Data.rBonusGuard;
    Images.Data.rBoardObjects[BOARD_OBJECT_MELEE]    = Images.Data.rActionMelee;
    Images.Data.rBoardObjects[BOARD_OBJECT_RANGED]   = Images.Data.rActionRanged;

    ///--[Verify]
    //--Mark the structure as incomplete if any images failed to resolve.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    ///--[String Setup]
    //--If any StarlightStrings need to be set, do it here.
    mConfirmAccept->SetString("[IMG0] Accept");
    mConfirmAccept->AllocateImages(1);
    mConfirmAccept->SetImageP(0, CM_IMG_OFFSET_Y, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mConfirmAccept->CrossreferenceImages();

    mConfirmCancel->SetString("[IMG0] Cancel");
    mConfirmCancel->AllocateImages(1);
    mConfirmCancel->SetImageP(0, CM_IMG_OFFSET_Y, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mConfirmCancel->CrossreferenceImages();
}

///====================================== DislocationPack =========================================
void DislocationPack::Initialize()
{
    mDebugNum = -1;
    mDislocationName = NULL;
    mAltDislocationName = NULL;
    mCurrentX = 0.0f;
    mCurrentY = 0.0f;
    mDestinationX = 0.0f;
    mDestinationY = 0.0f;
}
void DislocationPack::Deallocate()
{
    free(mDislocationName);
    free(mAltDislocationName);
}

///====================================== BoardObjectPack =========================================
void BoardObjectPack::Initialize()
{
    mIsDisabled = false;
    mObjectType = BOARD_OBJECT_EMPTY;
    mPosition.Initialize();
    mPosition.MoveTo(-100.0f, -100.0f, 0);
    mDupePosition.Initialize();
    mDupePosition.MoveTo(-100.0f, -100.0f, 0);
}

///====================================== BoardActionPack =========================================
void BoardActionPack::Initialize()
{
    mCode = BOARD_MOVE_ERROR;
    mCursor = -1;
}

///===================================== BoardNotifierPack ========================================
void BoardNotifierPack::Initialize()
{
    mType = PF_NOTIFIER_TYPE_NONE;
    mTimer = 1;
    mTimerMax = 1;
    mWorldX = -100.0f;
    mWorldY = -100.0f;
    rImage = NULL;
}
bool BoardNotifierPack::IsComplete()
{
    return (mTimer >= mTimerMax);
}
void BoardNotifierPack::SetToType(int pType)
{
    mType = PF_NOTIFIER_TYPE_FLOATFADE;
    if(pType == PF_NOTIFIER_TYPE_FLOATFADE)
    {
        mTimer = 0;
        mTimerMax = PF_NOTIFIER_FLOATFADE_TIME_TOTAL;
    }
}
void BoardNotifierPack::Update()
{
    mTimer ++;
}
void BoardNotifierPack::Render()
{
    //--Error check.
    if(!rImage) return;

    //--Floats for a few ticks then fades away. Appears about 1.5x tiles above its tile position.
    if(mType == PF_NOTIFIER_TYPE_FLOATFADE)
    {
        //--Setup.
        float cOffsetX = rImage->GetWidth() * -0.50f;
        float cOffsetY = -28.0f;

        //--Floating:
        if(mTimer < PF_NOTIFIER_FLOATFADE_TIME_FLOAT)
        {
            float cPercent = EasingFunction::QuadraticOut(mTimer, PF_NOTIFIER_FLOATFADE_TIME_FLOAT);
            cOffsetY = cOffsetY + (cPercent * PF_NOTIFIER_FLOATFADE_DISTANCE);
        }
        //--Fading.
        else
        {
            float cPercent = EasingFunction::QuadraticOut(mTimer-PF_NOTIFIER_FLOATFADE_TIME_FLOAT, PF_NOTIFIER_FLOATFADE_TIME_FADE);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f - cPercent);
            cOffsetY = cOffsetY + (1.0f * PF_NOTIFIER_FLOATFADE_DISTANCE);
        }

        //--Render.
        rImage->Draw(mWorldX + cOffsetX, mWorldY + cOffsetY);

        //--Clean.
        StarlightColor::ClearMixer();
    }
}

///===================================== Property Queries =========================================
bool PuzzleFight::IsHandlingUpdate()
{
    //--Returns true if this object is handling the player's controls and commanding level updates.
    //  Can return false even if the object still exists but it is taking itself down.
    return mIsHandlingUpdates;
}
bool PuzzleFight::IsComplete()
{
    //--Returns true if the object should be deleted and removed this tick.
    return mIsComplete;
}
int PuzzleFight::GetObjectAtBoardPosition(int pX, int pY)
{
    if(pX < 0 || pX >= mBoardW) return BOARD_OBJECT_EMPTY;
    if(pY < 0 || pY >= mBoardH) return BOARD_OBJECT_EMPTY;
    if(mBoardObjects[pX][pY].mIsDisabled) return BOARD_OBJECT_EMPTY;
    return mBoardObjects[pX][pY].mObjectType;
}
int PuzzleFight::GetPlayerHealth()
{
    return mPlayerHealth;
}
int PuzzleFight::GetEnemyHealth()
{
    return mChallengerHealth;
}
int PuzzleFight::GetMovesLeft()
{
    return mPlayerMovesLeft;
}
int PuzzleFight::GetTicksLeft()
{
    return mPlayerTicksLeft;
}

///======================================= Manipulators ===========================================
void PuzzleFight::BeginTakedown()
{
    mIsTakingDown = true;
}
void PuzzleFight::SetHandlingUpdates(bool pFlag)
{
    mIsHandlingUpdates = pFlag;
}
void PuzzleFight::SetComplete(bool pFlag)
{
    mIsComplete = pFlag;
}
void PuzzleFight::SetUpdateHandlerScript(const char *pPath)
{
    ResetString(mUpdateScript, pPath);
}
void PuzzleFight::SetGameStateFlag(int pFlag)
{
    mGameState = pFlag;
}
void PuzzleFight::AllocateDislocations(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mDislocationsTotal; i++)
    {
        mDislocations[i].Deallocate();
    }
    free(mDislocations);

    //--Clear.
    mDislocationsTotal = 0;
    mDislocations = NULL;

    //--Allocate.
    if(pTotal < 1) return;
    SetMemoryData(__FILE__, __LINE__);
    mDislocationsTotal = pTotal;
    mDislocations = (DislocationPack *)starmemoryalloc(sizeof(DislocationPack) * mDislocationsTotal);

    //--Initialize.
    for(int i = 0; i < mDislocationsTotal; i++)
    {
        mDislocations[i].Initialize();
        mDislocations[i].mDebugNum = i;
    }
}
void PuzzleFight::SetDislocationName(int pSlot, const char *pName)
{
    if(pSlot < 0 || pSlot >= mDislocationsTotal) return;
    ResetString(mDislocations[pSlot].mDislocationName, pName);
}
void PuzzleFight::SetDislocationAltName(int pSlot, const char *pName)
{
    if(pSlot < 0 || pSlot >= mDislocationsTotal) return;
    ResetString(mDislocations[pSlot].mAltDislocationName, pName);
}
void PuzzleFight::SetDislocationPos(int pSlot, float pX, float pY)
{
    if(pSlot < 0 || pSlot >= mDislocationsTotal) return;
    mDislocations[pSlot].mCurrentX = pX;
    mDislocations[pSlot].mCurrentY = pY;
}
void PuzzleFight::SetDislocationDest(int pSlot, float pX, float pY)
{
    if(pSlot < 0 || pSlot >= mDislocationsTotal) return;
    mDislocations[pSlot].mDestinationX = pX;
    mDislocations[pSlot].mDestinationY = pY;
}
void PuzzleFight::SetBoardTilePosition(int pX, int pY)
{
    mBoardStartX = pX;
    mBoardStartY = pY;
}
void PuzzleFight::SetBoardSize(int pW, int pH)
{
    //--Deallocate.
    for(int x = 0; x < mBoardW; x ++)
    {
        for(int y = 0; y < mBoardH; y ++)
        {
            ;
        }
        free(mBoardObjects[x]);
        free(rDislocationBoard[x]);
    }
    free(mBoardObjects);
    free(rDislocationBoard);

    //--Clear.
    mBoardW = 0;
    mBoardH = 0;
    mBoardObjects = NULL;
    rDislocationBoard = NULL;

    //--Allocate and initialize.
    if(pW < 1 || pH < 1) return;
    SetMemoryData(__FILE__, __LINE__);
    mBoardW = pW;
    mBoardH = pH;
    mBoardObjects = (BoardObjectPack **)starmemoryalloc(sizeof(BoardObjectPack *) * mBoardW);
    for(int x = 0; x < mBoardW; x ++)
    {
        mBoardObjects[x] = (BoardObjectPack *)starmemoryalloc(sizeof(BoardObjectPack) * mBoardH);
        for(int y = 0; y < mBoardH; y ++)
        {
            mBoardObjects[x][y].Initialize();
        }
    }
    rDislocationBoard = (DislocationPack ***)starmemoryalloc(sizeof(DislocationPack **) * mBoardW);
    for(int x = 0; x < mBoardW; x ++)
    {
        rDislocationBoard[x] = (DislocationPack **)starmemoryalloc(sizeof(DislocationPack *) * mBoardH);
        for(int y = 0; y < mBoardH; y ++)
        {
            rDislocationBoard[x][y] = NULL;
        }
    }
}
void PuzzleFight::RegisterDislocationPackToBoardSlot(int pPackSlot, int pBoardX, int pBoardY)
{
    //--Given a dislocation slot, registers it to that location on the board.
    if(pPackSlot < 0 || pPackSlot >= mDislocationsTotal) return;
    if(pBoardX < 0 || pBoardX >= mBoardW) return;
    if(pBoardY < 0 || pBoardY >= mBoardH) return;
    rDislocationBoard[pBoardX][pBoardY] = &mDislocations[pPackSlot];
}
void PuzzleFight::SetPlayerEntityName(const char *pName)
{
    if(!pName || !strcasecmp(pName, "Null"))
    {
        ResetString(mPlayerEntityName, NULL);
        return;
    }
    ResetString(mPlayerEntityName, pName);
}
void PuzzleFight::SetPlayerTicks(int pTicks)
{
    mPlayerTicksLeft = pTicks;
}
void PuzzleFight::SetPlayerMoves(int pMoves)
{
    mPlayerMovesLeft = pMoves;
}
void PuzzleFight::SetPlayerHealth(int pHealth)
{
    mPlayerHealth = pHealth;
    if(mPlayerHealth < 0) mPlayerHealth = 0;
    if(mPlayerHealth > MAX_HEALTH) mPlayerHealth = MAX_HEALTH;
    mPlayerHealthEasing.MoveTo(mPlayerHealth, HEALTH_SLIDE_TICKS);
}
void PuzzleFight::SetEnemyHealth(int pHealth)
{
    mChallengerHealth = pHealth;
    if(mChallengerHealth < 0) mChallengerHealth = 0;
    if(mChallengerHealth > MAX_HEALTH) mChallengerHealth = MAX_HEALTH;
    mChallengerHealthEasing.MoveTo(mChallengerHealth, HEALTH_SLIDE_TICKS);
}
void PuzzleFight::SetTitlePrint(const char *pString)
{
    //--Clear.
    if(!pString || !strcasecmp(pString, "Null"))
    {
        ResetString(mTitlePrintString, NULL);
        mTitlePrintPhase = PF_TITLE_PRINT_NONE;
        return;
    }

    //--Set.
    ResetString(mTitlePrintString, pString);
    mTitlePrintPhase = PF_TITLE_PRINT_PRINT;
    mTitlePrintTimer = 0;
    mTitlePrintTimerMax = strlen(mTitlePrintString) * TITLE_PRINT_TICKS_PER_LETTER;
}
void PuzzleFight::RegisterNotifier(int pType, float pWorldX, float pWorldY, const char *pImagePath)
{
    //--Creates and registers a notifier package. These appear on the board in world-coordinates
    //  and usually indicate a powerup of some sort.
    if(!pImagePath) return;

    //--Create.
    BoardNotifierPack *nPack = (BoardNotifierPack *)starmemoryalloc(sizeof(BoardNotifierPack));
    nPack->Initialize();
    nPack->SetToType(pType);
    nPack->mWorldX = pWorldX;
    nPack->mWorldY = pWorldY;
    nPack->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    mNotifierList->AddElementAsTail("X", nPack, &FreeThis);
}

///======================================= Core Methods ===========================================
void PuzzleFight::RunLoaderScript()
{
    //--Runs the asset loader script. Runs the Construct() function immediately after, assuming all
    //  needed graphics were loaded by the script.
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    char *tLoaderPath = InitializeString("%s/Minigames/Puzzle Fight/000 Loading.lua", rAdventurePath);
    LuaManager::Fetch()->ExecuteLuaFile(tLoaderPath);
    free(tLoaderPath);

    //--Construction handler.
    Construct();
}
void PuzzleFight::RunInitializerScript()
{
    //--Runs the initializer script to set up variables before normal execution begins.
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    char *tInitializerPath = InitializeString("%s/Minigames/Puzzle Fight/100 Initialize.lua", rAdventurePath);
    LuaManager::Fetch()->ExecuteLuaFile(tInitializerPath);
    free(tInitializerPath);

    //--Other variables.
    mUpdateCursorImmediately = true;
    mPlayerHealthEasing.MoveTo(MAX_HEALTH, 0);
    mChallengerHealthEasing.MoveTo(MAX_HEALTH, 0);
}
void PuzzleFight::SpawnBoardObject(int pType, int pBoardX, int pBoardY, float pSpawnX, float pSpawnY)
{
    //--Range check.
    if(pType < 0 || pType >= BOARD_OBJECT_TOTAL) return;
    if(pBoardX < 0 || pBoardX >= mBoardW) return;
    if(pBoardY < 0 || pBoardY >= mBoardH) return;

    //--Compute destination.
    float cTileSize = 16.0f;
    float cTargetX = (mBoardStartX * cTileSize) + (pBoardX * mBoardTileSizeX * cTileSize);
    float cTargetY = (mBoardStartY * cTileSize) + (pBoardY * mBoardTileSizeY * cTileSize);

    //--Set.
    mBoardObjects[pBoardX][pBoardY].mObjectType = pType;
    mBoardObjects[pBoardX][pBoardY].mEasingType = EASING_CODE_QUADOUT;
    mBoardObjects[pBoardX][pBoardY].mPosition.MoveTo(pSpawnX, pSpawnY, 0);
    mBoardObjects[pBoardX][pBoardY].mPosition.MoveTo(cTargetX, cTargetY, BOARD_OBJECT_INITIAL_MOVE_TICKS);
}
void PuzzleFight::DisableBoardObjectAt(int pBoardX, int pBoardY)
{
    //--Range check.
    if(pBoardX < 0 || pBoardX >= mBoardW) return;
    if(pBoardY < 0 || pBoardY >= mBoardH) return;

    //--Set.
    mBoardObjects[pBoardX][pBoardY].mIsDisabled = true;
}
void PuzzleFight::ClearBoardObjectAt(int pBoardX, int pBoardY)
{
    if(pBoardX < 0 || pBoardX >= mBoardW) return;
    if(pBoardY < 0 || pBoardY >= mBoardH) return;
    mBoardObjects[pBoardX][pBoardY].Initialize();
}
void PuzzleFight::ClearAllBoardObjects()
{
    for(int x = 0; x < mBoardW; x ++)
    {
        for(int y = 0; y < mBoardH; y ++)
        {
            mBoardObjects[x][y].Initialize();
        }
    }
}

///=================================== Private Core Methods =======================================
void PuzzleFight::CreateActionPack(int pAction, int pCursor)
{
    //--Creates and registers a BoardActionPack to the end of the action list. This stores which action
    //  to take if the player decides to cancel their previous action.
    BoardActionPack *nPack = (BoardActionPack *)starmemoryalloc(sizeof(BoardActionPack));
    mBoardActionList->AddElementAsTail("X", nPack, &FreeThis);

    //--Storage.
    nPack->Initialize();
    nPack->mCode = pAction;
    nPack->mCursor = pCursor;
}

///========================================= File I/O =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
PuzzleFight *PuzzleFight::Fetch()
{
    //--Note: Is reliant on an AdventureLevel to have ownership.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return NULL;

    //--Get from within the level.
    PuzzleFight *rPuzzleFight = rActiveLevel->GetPuzzleFight();
    return rPuzzleFight;
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
