//--Base
#include "AliasStorage.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
AliasStorage::AliasStorage()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_ALIASSTORAGE;

    ///--[AliasStorage]
    //--System
    mCheckTypes = true;

    //--Storage
    mAliasList = new StarLinkedList(true);
    mStoredStrings = new StarLinkedList(true);
}
AliasStorage::~AliasStorage()
{
    delete mAliasList;
    delete mStoredStrings;
}

///===================================== Property Queries =========================================
const char *AliasStorage::GetString(const char *pAlias)
{
    //--Returns the string at the given alias. Fails if the entry is not found, or if the entry is
    //  found but is not actually a string.
    if(!pAlias) return NULL;

    //--Find the entry.
    AliasStoragePack *rStoragePack = (AliasStoragePack *)mAliasList->GetElementByName(pAlias);
    if(!rStoragePack) return NULL;

    //--If the entry exists and we're checking types, type must match.
    if(mCheckTypes && rStoragePack->mType != POINTER_TYPE_STRING) return NULL;

    //--Found it, return it.
    return (const char *)rStoragePack->rPtr;
}

///======================================== Manipulators ==========================================
void AliasStorage::StoreString(const char *pName, const char *pString)
{
    //--Creates and stores a string which can be refereced by aliases.
    if(!pName || !pString) return;
    SetMemoryData(__FILE__, __LINE__);
    char *nString = InitializeString(pString);
    mStoredStrings->AddElement(pName, nString, &FreeThis);
}
void AliasStorage::RemoveString(const char *pName)
{
    //--Removes the named string and removes all references to it.
    if(!pName) return;
    void *rString = mStoredStrings->GetElementByName(pName);
    if(!rString) return;

    //--Check all alias packages for references.
    AliasStoragePack *rPackage = (AliasStoragePack *)mAliasList->SetToHeadAndReturn();
    while(rPackage)
    {
        if(rPackage->rPtr == rString) mAliasList->RemoveRandomPointerEntry();
        rPackage = (AliasStoragePack *)mAliasList->IncrementAndGetRandomPointerEntry();
    }

    //--Remove the element from the stored strings.
    mStoredStrings->RemoveElementP(rString);
}
void AliasStorage::RegisterAliasToString(const char *pAlias, const char *pStringName)
{
    //--Creates an alias that points to a previously stored string.
    if(!pAlias || !pStringName) return;

    //--Locate the string.
    void *rString = mStoredStrings->GetElementByName(pStringName);
    if(!rString) return;

    //--Create an alias package that points to it.
    SetMemoryData(__FILE__, __LINE__);
    AliasStoragePack *nPackage = (AliasStoragePack *)starmemoryalloc(sizeof(AliasStoragePack));
    nPackage->Initialize();
    nPackage->mType = POINTER_TYPE_STRING;
    nPackage->rDeletionFunction = NULL;
    nPackage->rPtr = rString;
    mAliasList->AddElement(pAlias, nPackage, &FreeThis);
}

///======================================== Core Methods ==========================================
void AliasStorage::Wipe()
{
    //--Wipes the class back to factory-zero, deallocating anything needed.
    mAliasList->ClearList();
    mStoredStrings->ClearList();
}
void AliasStorage::Print()
{
    //--Debug command, print the contents of this object.
    fprintf(stderr, "== Alias Storage Printing ==\n");
    fprintf(stderr, "There are %i base strings.\n", mStoredStrings->GetListSize());
    char *rString = (char *)mStoredStrings->PushIterator();
    while(rString)
    {
        fprintf(stderr, " %s / %s at %p\n", mStoredStrings->GetIteratorName(), rString, rString);
        rString = (char *)mStoredStrings->AutoIterate();
    }

    //--Aliases.
    fprintf(stderr, "Alias listing. There are %i aliases.\n", mAliasList->GetListSize());
    AliasStoragePack *rPackage = (AliasStoragePack *)mAliasList->PushIterator();
    while(rPackage)
    {
        fprintf(stderr, " %s -> %p\n", mAliasList->GetIteratorName(), rPackage->rPtr);
        rPackage = (AliasStoragePack *)mAliasList->AutoIterate();
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
AliasStorage *AliasStorage::GetChapterPathAliasStorage()
{
    //--Static, boots an alias storage used by Adventure Mode to store paths to various scripts.
    static AliasStorage *xChapterAliasStore = NULL;
    if(!xChapterAliasStore) xChapterAliasStore = new AliasStorage();
    return xChapterAliasStore;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
