//--Base
#include "WorldDialogue.h"

//--Classes
#include "AdventureDebug.h"
#include "DialogueActor.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"
#include "utf8.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

///===================================== Worker Functions =========================================
char *ResolveLastWord(StarLinkedList *pDialogueCharacterList);
int ResolveNextWord(const char *pString, char *pOutBuffer, int pStartLetter, char pDelimiter)
{
    ///--[Documentation]
    //--Returns the next word up to the delimiter. If given a string like [DOGS|ARE|GREAT],
    //  the | and ] serve to split the words off and this function helps to do that.
    //--The direct return is how many letters were parsed. The word that was parsed is placed in the pOutBuffer.
    //  It should be STD_MAX_LETTERS long or longer.
    if(!pString || !pOutBuffer) return 0;

    //--Determine the maximum parsing letters.
    int tMaxLen = (int)strlen(pString);

    //--Begin parsing.
    int tSlot = pStartLetter;
    int tCursor = 0;
    while(pString[tSlot] != pDelimiter)
    {
        //--Range checker.
        if(tSlot >= tMaxLen) return 0;

        //--Copy.
        pOutBuffer[tCursor + 0] = pString[tSlot];
        pOutBuffer[tCursor + 1] = '\0';

        //--Next letter.
        tCursor ++;
        tSlot ++;
        if(tCursor >= STD_MAX_LETTERS-1) tCursor = STD_MAX_LETTERS - 1;
    }

    //--Return how many letters got parsed.
    return tCursor;
}

///===================================== Primary Functions ========================================
void WorldDialogue::AppendString(const char *pString, bool pAllowTranslation)
{
    ///--[Documentation]
    //--Appends the requested string to the dialogue box. Handles special dialogue cases, such as tags
    //  and \n characters.
    //--pAllowTranslation should only be used when this is an originating call, such as from a Lua script.
    //  Reappends from [BLOCK] tags need not apply.
    if(!pString) return;

    //--Flags.
    bool tNeedsToDeallocateString = false;
    mNeedsToReappend = false;
    mIsPrintingComplete = false;
    mPrintTimeRemainder = 0;

    ///--[String Translation]
    //--If there is an active translation, it may replace the appended string entirely. This is done before other
    //  processing like variables. It does not occur if this is not the 0th append, re-appending after parsing will
    //  not re-search the translation.
    //--The translation is not guaranteed to exist. If it doesn't, do nothing.
    if(pAllowTranslation)
    {
        //--Resets the parser length value.
        mCurrentParserLen = 0.0f;
        //fprintf(stderr, "Starting new line appendation. Clearing parser len.\n");

        //--Query the active translation. If none is active, do nothing.
        DataLibrary *rDataLibrary = DataLibrary::Fetch();
        StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_DIALOGUE);

        //--Translation was found.
        if(rTranslation)
        {
            //--Find out where the 'Prefix' ends. The prefix determines things like the speaker's name and emotion. This must
            //  mirror the code of AdventureDebug::HandleTranslationLine() otherwise the translations won't catch.
            int tPrefixEndsAt = AdventureDebug::GetIndexOfLineStart(pString, 0);

            //--Create a prefix string.
            char tPrefixString[128];
            strncpy(tPrefixString, pString, tPrefixEndsAt);
            tPrefixString[tPrefixEndsAt] = '\0';

            //--Locate a translation.
            const char *rTranslatedString = rTranslation->GetTranslationFor(&pString[tPrefixEndsAt]);

            //--Translation found!
            if(rTranslatedString)
            {
                //--Assemble a string that will replace the original string. This string is heap-allocated
                //  while the normal string is stack allocated or held in the lua state, which is a problem.
                //  To solve it, we set a flag to clear the string once all processing is done on it.
                tNeedsToDeallocateString = true;

                //--Create the string and override pString with it.
                pString = InitializeString("%s%s", tPrefixString, rTranslatedString);

                //--Debug.
                //fprintf(stderr, "Comparing %s to translations.\n", &pString[tPrefixEndsAt]);
                //fprintf(stderr, " Match was %s\n", rTranslatedString);
                //fprintf(stderr, "%s\n", pString);
            }
        }
    }
    else
    {
        //fprintf(stderr, " Starting secondary line appendation.\n");
    }

    ///--[String Replacement]
    //--Before anything else is done, the string is parsed and copied, with certain special sequences being replaced.
    //  This allows us to change character names to variables or insert numbers.
    //--This only needs to be done once on the initial append, which is also when translation is applied, therefore
    //  this code only runs during translation.
    char *tString = NULL;
    uint32_t tLetter = 0;
    int tIndex = 0;
    if(pAllowTranslation)
    {
        int tParseLen = utf8_strlen(pString);
        //int tBasicLen = (int)strlen(pString);
        StarAutoBuffer *tAutoBuffer = new StarAutoBuffer();
        //fprintf(stderr, "Iterating %s. Length %i %i\n", pString, tParseLen, tBasicLen);

        //--Iterate.
        tLetter = utf8_nextletter(pString, tIndex);
        for(int i = 0; i < tParseLen; i ++)
        {
            //--Setup.
            int tLettersLeft = tParseLen - i;
            int tActualBytePos = u8_offset(pString, i);

            //--[NAME|Basename] will replace the Basename with a remap. This is used for characters with variable names.
            if(tLettersLeft >= 8 && !strncasecmp(&pString[tActualBytePos], "[NAME|", 6))
            {
                //--Buffers.
                int tCursor = 6;
                char tCharacterNameBuf[STD_MAX_LETTERS];

                //--Actor's Name
                int tParsed = ResolveNextWord(&pString[tActualBytePos], tCharacterNameBuf, tCursor, ']');
                tCursor = tCursor + tParsed;

                //--Search the remaps. If not found, just append the name as provided.
                char *rRemap = (char *)mSpeakerNameRemaps->GetElementByName(tCharacterNameBuf);
                if(!rRemap)
                {
                    tAutoBuffer->AppendStringWithoutNull(tCharacterNameBuf);
                }
                //--A remap was found, so append that.
                else
                {
                    tAutoBuffer->AppendStringWithoutNull(rRemap);
                }

                //--Move the cursor along.
                i += tCursor;
            }
            //--In all other cases, just append the character.
            else
            {
                char tBuffer[5];
                memset(tBuffer, 0, sizeof(char) * 5);
                u8_toutf8(tBuffer, sizeof(uint32_t), &tLetter, 1);
                tAutoBuffer->AppendStringWithoutNull(tBuffer);
                //fprintf(stderr, " Append %c %c %c %c\n", tBuffer[0], tBuffer[1], tBuffer[2], tBuffer[3]);
            }

            //--Next letter.
            tLetter = utf8_nextletter(pString, tIndex);
        }
        tAutoBuffer->AppendNull();

        //--Replace the string with the AutoBuffer's version.
        tNeedsToDeallocateString = true;
        char *tString = (char *)tAutoBuffer->LiberateData();
        delete tAutoBuffer;
        pString = tString;
        //fprintf(stderr, " Autobuffer %s\n", pString);
    }

    ///--[Blocking Case]
    //--If we're currently in blocking mode, then the string is immediately tacked onto the end of the pending
    //  string. If the pending string doesn't exist, create it.
    if(mIsBlocking)
    {
        //--Pending string doesn't exist, create it here.
        if(!mPendingDialogue)
        {
            ResetString(mPendingDialogue, pString);
        }
        //--Otherwise, tack it on.
        else
        {
            char *rOldDialogue = mPendingDialogue;
            mPendingDialogue = InitializeString("%s%s", rOldDialogue, pString);
            free(rOldDialogue);
        }

        //--Stop the append here.
        free(tString);
        if(tNeedsToDeallocateString) free((void *)pString);
        return;
    }

    ///--[Appendation Case]
    //--Increment the appendation stack.
    xAppendStack ++;

    //--Resolve which lists to use.
    StarLinkedList *rDialogueLog  = mDialogueLog;
    StarLinkedList *rDialogueList = mDialogueList;

    //--If this is the zeroth appendation action (flag will be at 1) and we're using the autotarget method,
    //  re-resolve the target here. This is done by searching for a speaker. It only happens on the zeroth pass!
    int tSkipToLetter = 0;
    if((xAppendStack == 1 || xAppendStack < 0) && mDialogueUsesAutoresolveTarget)
    {
        //--Debug.
        //fprintf(stderr, "Testing autoresolve on %s\n", pString);

        //--Setup.
        int tIndex = 0;
        int tAdvisor = 0;
        int tLength = utf8_strlen(pString);

        //--Reset speaker names but not highlighting.
        ResetString(mMajorSequenceSpeaker, NULL);

        //--Start iterating.
        utf8_nextletter(pString, tIndex);
        for(int i = 0; i < tLength; i ++)
        {
            //--Byte position.
            int tActualBytePos = u8_offset(pString, i);

            //--If we hit a [BLOCK] tag, stop.
            if(tLength >= 7 && !strncasecmp(&pString[tActualBytePos], "[BLOCK]", 7))
            {
                break;
            }
            //--If we hit a [B] tag, stop. Same as [BLOCK].
            else if(tLength >= 3 && !strncasecmp(&pString[tActualBytePos], "[B]", 3))
            {
                break;
            }
            //--If we hit a [SOFTBLOCK] tag, stop.
            else if(tLength >= 11 && !strncasecmp(&pString[tActualBytePos], "[SOFTBLOCK]", 11))
            {
                break;
            }
            //--If we hit a [P] tag, stop. Same as [SOFTBLOCK].
            else if(tLength >= 3 && !strncasecmp(&pString[tActualBytePos], "[P]", 3))
            {
                break;
            }
            //--If we spot a tag ender, set the advisor:
            else if(pString[tActualBytePos] == ']')
            {
                tAdvisor = i+1;
            }
            //--If we spot a colon that is not a double colon, that's the next speaker name.
            else if(pString[tActualBytePos] == ':' && i < tLength - 1 && pString[tActualBytePos+1] != ':' && i >= tAdvisor + 1 && !mIsSceneMode)
            {
                //--Debug.
                //fprintf(stderr, " Colon spotted at %i, advisor %i\n", i, tAdvisor);

                //--The string will ignore the first part of the sentence.
                tSkipToLetter = i+1;

                //--Name buffer.
                char tNameBuffer[STD_MAX_LETTERS];
                tNameBuffer[0] = '\0';

                //--Scan backwards to resolve the speaker.
                for(int p = tAdvisor; p < i; p ++)
                {
                    tNameBuffer[p-tAdvisor+0] = pString[p];
                    tNameBuffer[p-tAdvisor+1] = '\0';
                }

                //--Debug.
                //fprintf(stderr, " Name resolve as %s\n", tNameBuffer);
                //fprintf(stderr, " Remaining string is %s\n", &pString[tSkipToLetter]);

                //--Set highlighting. Doesn't affect the name being shown, but does affect which characters
                //  are greyed and which aren't.
                SetSpeaker(tNameBuffer);

                //--Set the display name. This function handles remaps.
                SetDisplaySpeakerFromName(tNameBuffer);

                //--Done.
                break;
            }

            //--Next letter.
            tLetter = utf8_nextletter(pString, tIndex);
        }
    }

    ///--[Logging]
    //--First, always store a copy of the string on the log. This is the raw string before parsing.
    char *nLogString = InitializeString("%s", pString);
    if(rDialogueLog) rDialogueLog->AddElementAsTail("X", nLogString, &FreeThis);

    //--Setup.
    const char *rRefString = &pString[tSkipToLetter];
    StarlightColor *rCurrentColor = &mWhitePack;

    //--Get which list to append to. If there's no list, create one.
    rActiveAppendList = (StarLinkedList *)rDialogueList->GetTail();
    if(!rActiveAppendList)
    {
        rDialogueList->AddElementAsTail("X", new StarLinkedList(true), &StarLinkedList::DeleteThis);
        rActiveAppendList = (StarLinkedList *)rDialogueList->GetTail();
    }

    ///--[Letter Appendation]
    //--Next, start adding letters. First, reset variables.
    int tNewLength = mCurrentParserLen;
    mLettersAppended = 0;
    tIndex = 0;
    int tLastLineStart = 0;
    int tLength = utf8_strlen(rRefString);
    //tBasicLen = (int)strlen(rRefString);
    //fprintf(stderr, " Running across ref string %s.\n", rRefString);
    //fprintf(stderr, " Length is %i %i.\n", tLength, tBasicLen);
    //fprintf(stderr, " Line starting length is %f\n", mCurrentParserLen);

    //--Parse.
    tLetter = utf8_nextletter(rRefString, tIndex);
    for(int i = 0; i < tLength; i ++)
    {
        //--Handle special letters. This may involve skipping letters. Special sequences should not
        //  have any UTF-8 letters in them.
        int tActualBytePos = u8_offset(rRefString, i);
        int tLettersToSkip = HandleSpecialSequences(&rRefString[tActualBytePos]);

        //--If the length got reset due to a clear, reset the local variable as well.
        if(mCurrentParserLen == 0.0) tNewLength = 0.0f;

        //--Skipping no letters.
        if(tLettersToSkip == 0)
        {
            //--Special: If this static flag is tripped, this was a '::' case. When this happens, append a colon and skip a character.
            if(xIsColon == 1)
            {
                //--Create a DialogueCharacter to wrap this letter.
                SetMemoryData(__FILE__, __LINE__);
                DialogueCharacter *nCharacter = (DialogueCharacter *)starmemoryalloc(sizeof(DialogueCharacter));
                nCharacter->Initialize();
                nCharacter->mLetter = ':';
                nCharacter->mIsSpooky = mIsSpookyText;
                nCharacter->mIsLarge = mIsLargeText;
                nCharacter->mIsSmall = mIsSmallText;
                nCharacter->rColor = rCurrentColor;
                nCharacter->mTimer = mLettersAppended * (WD_LETTER_FADE_TICKS / 2 * -1);
                nCharacter->mAlpha = 0.0f;
                nCharacter->rCurrentVoice = rCurrentVoice;
                rActiveAppendList->AddElementAsTail("X", nCharacter, &FreeThis);
                mLettersAppended ++;

                //--Skip letters.
                i += 1;
                tIndex ++;
                tLetter = utf8_nextletter(rRefString, tIndex);
                continue;
            }
            //--Special: If this flag is 2, it means a single colon is rendered and the speaker was set. No letter is skipped.
            //  The colon, most importantly, does not have a voice line.
            else if(xIsColon == 2)
            {
                //--Create a DialogueCharacter to wrap this letter.
                SetMemoryData(__FILE__, __LINE__);
                DialogueCharacter *nCharacter = (DialogueCharacter *)starmemoryalloc(sizeof(DialogueCharacter));
                nCharacter->Initialize();
                nCharacter->mLetter = ':';
                nCharacter->mIsSpooky = mIsSpookyText;
                nCharacter->mIsLarge = mIsLargeText;
                nCharacter->mIsSmall = mIsSmallText;
                nCharacter->rColor = rCurrentColor;
                nCharacter->mTimer = mLettersAppended * (WD_LETTER_FADE_TICKS / 2 * -1);
                nCharacter->mAlpha = 0.0f;
                nCharacter->rCurrentVoice = NULL;
                rActiveAppendList->AddElementAsTail("X", nCharacter, &FreeThis);
                mLettersAppended ++;

                //--Skip.
                tLetter = utf8_nextletter(rRefString, tIndex);
                continue;
            }
        }
        //--Value is positive. Skip that many letters and resume parsing.
        else if(tLettersToSkip > 0)
        {
            //--If necessary, redo the append.
            if(mNeedsToReappend)
            {
                AppendString(&rRefString[i+tLettersToSkip], false);
                break;
            }

            //--Skip letters.
            tIndex += tLettersToSkip;
            i += tLettersToSkip;
            tLetter = utf8_nextletter(rRefString, tIndex);
            continue;
        }
        //--Value is negative, cease parsing here.
        else if(tLettersToSkip < 0)
        {
            //--The append string becomes whatever was left. The length of the tag is mTagSkipLen.
            //--Place these on the reserve so the dialogue will continue when the block expires.
            //fprintf(stderr, "Stopped parsing after %i letters. Time %f\n", i, GetGameTime());
            int tBytePos = u8_offset(rRefString, i+mTagSkipLen);
            AppendString(&rRefString[tBytePos], false);
            //fprintf(stderr, "Appended remainder. Time is %f\n", GetGameTime());
            break;
        }

        //--If this letter is a space, and we have not appended a non-space yet, ignore it.
        if(!mHasAppendedNonSpace && rRefString[i] == ' ')
        {
            tLetter = utf8_nextletter(rRefString, tIndex);
            continue;
        }

        //--Create a DialogueCharacter to wrap this letter.
        SetMemoryData(__FILE__, __LINE__);
        DialogueCharacter *nCharacter = (DialogueCharacter *)starmemoryalloc(sizeof(DialogueCharacter));
        nCharacter->Initialize();
        nCharacter->mLetter = tLetter;
        nCharacter->mIsSpooky = mIsSpookyText;
        nCharacter->mIsLarge = mIsLargeText;
        nCharacter->mIsSmall = mIsSmallText;
        nCharacter->rColor = rCurrentColor;
        nCharacter->mTimer = mLettersAppended * (WD_LETTER_FADE_TICKS / 2 * -1);
        nCharacter->mAlpha = 0.0f;
        nCharacter->rCurrentVoice = rCurrentVoice;
        rActiveAppendList->AddElementAsTail("X", nCharacter, &FreeThis);
        mHasAppendedNonSpace = true;
        //fprintf(stderr, " Appended letter %3i %3i %c\n", i, tLetter, tLetter);

        //--Add to the running tally.
        mLettersAppended ++;

        //--Check if this letter pushes us over the edge.
        float tWidth = ComputeLengthOf(rActiveAppendList) * mTextScale;
        if(tWidth + mCurrentParserLen >= cDialogueMaxWidth)
        {
            //--Diagnostics.
            //fprintf(stderr, "Overran line length at %f + %f = %f\n", tWidth, mCurrentParserLen, tWidth + mCurrentParserLen);
            //fprintf(stderr, " Address of list: %p\n", rActiveAppendList);
            //fprintf(stderr, " Elements on list: %i\n", rActiveAppendList->GetListSize());
            //fprintf(stderr, "String: %s\n", rRefString);
            //fprintf(stderr, "Append list %i\n", rActiveAppendList->GetListSize());
            //DialogueCharacter *rCharacter = (DialogueCharacter *)rActiveAppendList->PushIterator();
            //while(rCharacter)
            //{
            //    fprintf(stderr, "%c", rCharacter->mLetter);
            //    rCharacter = (DialogueCharacter *)rActiveAppendList->AutoIterate();
            //}
            //fprintf(stderr, "\n");

            //--Reset the parser length.
            tNewLength = 0.0f;
            mCurrentParserLen = 0.0f;

            //--Store previous list, get a new list.
            StarLinkedList *rPreviousList = rActiveAppendList;
            rActiveAppendList = new StarLinkedList(true);
            rDialogueList->AddElementAsTail("X", rActiveAppendList, &StarLinkedList::DeleteThis);

            //--Scan backwards to a space, or the start of the string.
            int tLettersToRemove = 0;
            int tLastSpace = -1;
            for(int p = i; p >= tLastLineStart; p --)
            {
                tLettersToRemove ++;
                if(rRefString[p] == ' ')
                {
                    tLettersToRemove --;
                    tLastSpace = p;
                    break;
                }
            }

            //--If tLastSpace is -1, there were no spaces. Just hard break.
            if(tLastSpace == -1 || tLettersToRemove >= 15)
            {
                //fprintf(stderr, " Found no letters to remove, or too many to remove %i %i.\n", tLastSpace, tLettersToRemove);
            }
            //--Otherwise, we need to pull off letters.
            else
            {
                //fprintf(stderr, " Found %i letters to remove.\n", tLettersToRemove);
                for(int p = 0; p < tLettersToRemove; p ++)
                {
                    rPreviousList->SetRandomPointerToTail();
                    rActiveAppendList->AddElementAsHead("X", rPreviousList->LiberateRandomPointerEntry(), &FreeThis);
                }
            }

            //--Set this flag.
            tLastLineStart = i;
        }
        //--Store the length if we didn't run over the edge of the line.
        else
        {
            tNewLength = mCurrentParserLen + tWidth;
            //fprintf(stderr, "No overrun. %f + %f = %f\n", tWidth, mCurrentParserLen, tWidth + mCurrentParserLen);
        }

        //--Next letter.
        tLetter = utf8_nextletter(rRefString, tIndex);
    }

    //--Store the line length.
    mCurrentParserLen = tNewLength;
    //fprintf(stderr, " Line ends, parser at %f\n", mCurrentParserLen);

    ///--[Debug]
    //--Once that's done, print the results to the console.
    if(false)
    {
        //--Basics.
        fprintf(stderr, "== Printing results of append ==\n");

        //--Iterate across the lines.
        StarLinkedList *rLetterList = (StarLinkedList *)rDialogueList->PushIterator();
        while(rLetterList)
        {
            //--Iterate across the characters.
            DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
            while(rCharacterPack)
            {
                //--Spit.
                fprintf(stderr, "%c", rCharacterPack->mLetter);

                //--Next character.
                rCharacterPack = (DialogueCharacter *)rLetterList->AutoIterate();
            }

            //--Next line.
            fprintf(stderr, "\n");
            rLetterList = (StarLinkedList *)rDialogueList->AutoIterate();
        }

        //--Finish up.
        fprintf(stderr, "== End ==\n");
    }

    ///--[Finish Up]
    //--Clean up.
    rActiveAppendList = NULL;
    xAppendStack --;
    free(tString);
    if(tNeedsToDeallocateString) free((void *)pString);
}
int WorldDialogue::HandleSpecialSequences(const char *pString)
{
    ///--[Documentation]
    //--Given a string which may or may not contain special sequences, modifies the parser state in accordance
    //  with the tags.
    //--Returns 0 if the parser should continue, a positive number to indicate it should skip letters, or negative
    //  to indicate it should stop parsing.
    xIsColon = 0;
    int tLength = (int)strlen(pString);

    //--Resolve the list to append to.
    StarLinkedList *rDialogueList = ResolveActiveDialogueList();

    //--\n and the ASCII equivalent will cause a newline case. Parsing skips one letter.
    if(tLength >= 2 && pString[0] == '\\' && pString[1] == 'n')
    {
        rDialogueList->AddElementAsTail("X", new StarLinkedList(true), &StarLinkedList::DeleteThis);
        rActiveAppendList = (StarLinkedList *)rDialogueList->GetTail();
        return 1;
    }
    //--Ascii equivalent, which is 10 or 13 depending on the system.
    else if(tLength >= 1 && pString[0] == 10)
    {
        rDialogueList->AddElementAsTail("X", new StarLinkedList(true), &StarLinkedList::DeleteThis);
        rActiveAppendList = (StarLinkedList *)rDialogueList->GetTail();
        return 0;
    }
    //--[BR], same as \n but is translation-friendly.
    else if(tLength >= 4 && !strncasecmp(pString, "[BR]", 4))
    {
        rDialogueList->AddElementAsTail("X", new StarLinkedList(true), &StarLinkedList::DeleteThis);
        rActiveAppendList = (StarLinkedList *)rDialogueList->GetTail();
        return 3;
    }
    //--Colon: Specifies the speaker. You can skip this behavior using :: (which just renders one colon). Non-speakers are greyed out slightly.
    else if(pString[0] == ':' && (tLength < 2 || pString[1] != ':') && !mIsSceneMode)
    {
        //--Get the last word in question.
        char *tLastWord = ResolveLastWord(rActiveAppendList);

        //--Set that as the speaker.
        xIsColon = 2;
        SetSpeaker(tLastWord);

        //--Clean.
        free(tLastWord);
        return 0;
    }
    //--Colon-Colon: Renders a colon without setting the speaker.
    else if(tLength >= 2 && pString[0] == ':' && pString[1] == ':' && !mIsSceneMode)
    {
        xIsColon = 1;
        return 0;
    }
    //--[BLOCK] will stop execution and wait until the dialogue has finished running and the player pushed a key before continuing.
    else if((tLength >= 7 && !strncasecmp(pString, "[BLOCK]", 7)) || (tLength >= 3 && !strncasecmp(pString, "[B]", 3)))
    {
        //--Flags.
        mIsBlocking = true;
        mIsSoftBlocking = false;
        mIsPausing = false;

        //--Tag skip.
        mTagSkipLen = 7;
        if(!strncasecmp(pString, "[B]", 3)) mTagSkipLen = 3;

        //--Return a negative indicating parsing needs to stop.
        return -1;
    }
    //--[SOFTBLOCK] will stop execution and wait until the dialogue has finished running, but will auto-keypress even if the player isn't touching the key.
    else if((tLength >= 11 && !strncasecmp(pString, "[SOFTBLOCK]", 11)) || (tLength >= 3 && !strncasecmp(pString, "[P]", 3)))
    {
        //--Flags.
        mIsBlocking = true;
        mIsSoftBlocking = true;
        mIsPausing = false;
        mSoftBlockTimer = 0;

        //--If this is the [P] version, switch the flag.
        mTagSkipLen = 11;
        if(!strncasecmp(pString, "[P]", 3))
        {
            mIsPausing = true;
            mTagSkipLen = 3;
        }

        //--Return a negative indicating parsing needs to stop.
        return -1;
    }
    //--[VOICE|Actor] sets the speaking voice. Can be used to override the voice or to correct for errors due to the autodetect system.
    else if(tLength >= 7 && !strncasecmp(pString, "[VOICE|", 7))
    {
        //--Buffers.
        int tCursor = 7;
        char tVoiceBuffer[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tVoiceBuffer, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--If the voice name is the word "Leader" then use the leader's name. "Thought" does the same thing.
        if(!strcasecmp(tVoiceBuffer, "Leader") || !strcasecmp(tVoiceBuffer, "Thought"))
        {
            SetVoice(mLeaderVoice);
        }
        //--Set it directly.
        else
        {
            SetVoice(tVoiceBuffer);
        }
        return tCursor;
    }
    //--[SPOOKY] activates spooky dialogue.
    else if(tLength >= 8 && !strncasecmp(pString, "[SPOOKY]", 8))
    {
        //--Flags.
        mIsSpookyText = true;

        //--Length.
        return 8 - 1;
    }
    //--[NOSPOOKY] deactivates spooky dialogue.
    else if(tLength >= 10 && !strncasecmp(pString, "[NOSPOOKY]", 10))
    {
        //--Flags.
        mIsSpookyText = false;

        //--Length.
        return 10 - 1;
    }
    //--[LOUD] activates loud dialogue.
    else if(tLength >= 6 && !strncasecmp(pString, "[LOUD]", 6))
    {
        //--Flags.
        mIsLargeText = true;

        //--Length.
        return 6 - 1;
    }
    //--[NOLOUD] deactivates loud dialogue.
    else if(tLength >= 8 && !strncasecmp(pString, "[NOLOUD]", 8))
    {
        //--Flags.
        mIsLargeText = false;

        //--Length.
        return 8 - 1;
    }
    //--[QUIET] activates quiet dialogue.
    else if(tLength >= 7 && !strncasecmp(pString, "[QUIET]", 7))
    {
        //--Flags.
        mIsSmallText = true;

        //--Length.
        return 7 - 1;
    }
    //--[NOQUIET] deactivates quiet dialogue.
    else if(tLength >= 9 && !strncasecmp(pString, "[NOQUIET]", 9))
    {
        //--Flags.
        mIsSmallText = false;

        //--Length.
        return 9 - 1;
    }
    //--[CLEAR] will wipe all text and set everything back. Does not stop parsing.
    else if((tLength >= 7 && !strncasecmp(pString, "[CLEAR]", 7)) || (tLength >= 3 && !strncasecmp(pString, "[C]", 3)))
    {
        //--Wipe.
        Clear();
        mHasAppendedNonSpace = false;

        //--Check length.
        int tTagLen = 7;
        if(!strncasecmp(pString, "[C]", 3)) tTagLen = 3;

        //--Tag skip.
        mTagSkipLen = tTagLen;

        //--Append stack resets.
        xAppendStack = 0;

        //--Flags.
        mNeedsToReappend = true;
        mIsPrintingComplete = false;
        mPrintTimeRemainder = 0;
        mLettersAppended = 0;

        //--Add a new line.
        rDialogueList->AddElementAsTail("X", new StarLinkedList(true), &StarLinkedList::DeleteThis);
        rActiveAppendList = (StarLinkedList *)rDialogueList->GetTail();

        //--Scan forwards. If no ':' exists before the next clear tag or end of the line, the voice defaults to the normal tick.
        bool tHitColon = false;
        int tLen = (int)strlen(pString);
        for(int i = tTagLen; i < tLen; i ++)
        {
            //--Check for a clear. If we find one, we're done.
            if(!strncasecmp(&pString[i], "[CLEAR]", 7) || !strncasecmp(&pString[i], "[C]", 3))
            {
                break;
            }
            //--If we find a colon, break and be done with it.
            else if(pString[i] == ':')
            {
                tHitColon = true;
                break;
            }
        }

        //--If a colon was not hit, set the voice to the normal tick.
        if(!tHitColon) rCurrentVoice = mNormalTextTick;

        //--Skip over the tag.
        return tTagLen;
    }
    //--[E|Emotion] Sets the current actor's emotion. It can't set emotions for people who are not speaking. It's easier to use commonly.
    else if(tLength >= 5 && !strncasecmp(pString, "[E|", 3))
    {
        //--Buffers.
        int tCursor = 3;
        char tEmotionNameBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tEmotionNameBuf, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--Set it.
        SetActorEmotionS("ACTIVE", tEmotionNameBuf);
        return tCursor;
    }
    //--[EMOTION|Actor|Emotion] sets an actor's emotion. Duh. Use this instead of the [E|] tag if the actor is
    //  not the current speaker.
    else if(tLength >= 13 && !strncasecmp(pString, "[EMOTION|", 9))
    {
        //--Buffers.
        int tCursor = 9;
        char tCharacterNameBuf[STD_MAX_LETTERS];
        char tEmotionNameBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tCharacterNameBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1; //Skip the '|' as well as the parsed letters.

        //--Emotion Name
        tParsed = ResolveNextWord(pString, tEmotionNameBuf, tCursor, ']');
        tCursor = tCursor + tParsed; //Don't skip the ']' for return-length reasons.

        //--Now set the emotion.
        SetActorEmotionS(tCharacterNameBuf, tEmotionNameBuf);
        return tCursor;
    }
    //--[ADDCHAR|Actor|Slot|Emotion] adds a new character in the given slot with the given emotion. Pass -1 for the slot to use
    //  the first unoccupied slot, which is best for Visual Novel mode.
    else if(tLength >= 15 && !strncasecmp(pString, "[ADDCHAR|", 9))
    {
        //--Buffers.
        int tCursor = 9;
        char tCharacterNameBuf[STD_MAX_LETTERS];
        char tSlotNumberBuf[STD_MAX_LETTERS];
        char tEmotionNameBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tCharacterNameBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1; //Skip the '|' as well as the parsed letters.

        //--Slot Number
        tParsed = ResolveNextWord(pString, tSlotNumberBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1;

        //--Emotion Name
        tParsed = ResolveNextWord(pString, tEmotionNameBuf, tCursor, ']');
        tCursor = tCursor + tParsed; //Don't skip the ']' for return-length reasons.

        //--Change slot to an integer.
        int tDestinationSlot = atoi(tSlotNumberBuf);

        //--Check if the character is already on the field.
        int tPreviousSlot = -1;
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            if(!mDialogueActors[i]) continue;
            if(!strcasecmp(mDialogueActors[i]->GetName(), tCharacterNameBuf))
            {
                tPreviousSlot = i;
                break;
            }
        }

        //--If the character is already on the field, move them to the given slot.
        if(tPreviousSlot != -1 && tDestinationSlot != -1)
        {
            MoveActorToSlotI(tPreviousSlot, tDestinationSlot);
        }
        //--Put the character on the field.
        else if(tPreviousSlot == -1)
        {
            //--If the destination slot is -1, set the slot based on the first empty one.
            if(tDestinationSlot < 0 || tDestinationSlot >= WD_DIALOGUE_ACTOR_SLOTS)
            {
                for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
                {
                    if(mDialogueActors[i]) continue;
                    AddActorToSlot(tCharacterNameBuf, i);
                    SetActorEmotionS(tCharacterNameBuf, tEmotionNameBuf);
                    break;
                }
            }
            //--No need for special code, toss them right in.
            else
            {
                AddActorToSlot(tCharacterNameBuf, tDestinationSlot);
                SetActorEmotionS(tCharacterNameBuf, tEmotionNameBuf);
            }

            //--Re-run the speaker code. This makes it easier if a character appears during a line they are speaking.
            SetSpeaker(mLastSetSpeaker);
        }

        //--Pass back the letters parsed.
        return tCursor;
    }
    //--[REMCHAR|Actor] removes the named character from the lineup.
    else if(tLength >= 11 && !strncasecmp(pString, "[REMCHAR|", 9))
    {
        //--Buffers.
        int tCursor = 9;
        char tCharacterNameBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tCharacterNameBuf, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--Figure out which slot(s) the character is in. Remove all instances.
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            //--Skip empty slots.
            if(!mDialogueActors[i]) continue;

            //--Name match. Doesn't stop here in case of multiple copies.
            if(!strcasecmp(mDialogueActors[i]->GetName(), tCharacterNameBuf))
            {
                RemoveActor(i);
            }
        }

        //--Return the length.
        return tCursor;
    }
    //--[REMCHARBYSLOT|Actor] removes the named character from the lineup.
    else if(tLength >= 17 && !strncasecmp(pString, "[REMCHARBYSLOT|", 15))
    {
        //--Buffers.
        int tCursor = 15;
        char tCharacterSlotBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tCharacterSlotBuf, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--Translate to integer and remove.
        RemoveActor(atoi(tCharacterSlotBuf));

        //--Return the length.
        return tCursor;
    }
    //--[BACKGROUND|BackgroundDLPath] changes the background to the given image. Uses DL Paths.
    else if(tLength >= 14 && !strncasecmp(pString, "[BACKGROUND|", 12))
    {
        //--Buffers.
        int tCursor = 12;
        char tBackgroundPathBuf[STD_MAX_LETTERS];

        //--Background's Name
        int tParsed = ResolveNextWord(pString, tBackgroundPathBuf, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--Set it.
        SetVisualNovelBackground(tBackgroundPathBuf);

        //--Return the length.
        return tCursor;
    }
    //--[FOCUS|Actor] puts the highlight on the named character, even if they are not speaking.
    else if(tLength >= 9 && !strncasecmp(pString, "[FOCUS|", 7))
    {
        //--Buffers.
        int tCursor = 7;
        char tCharacterNameBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tCharacterNameBuf, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--Translate to integer and remove.
        SetSpeaker(tCharacterNameBuf);

        //--Return the length.
        return tCursor;
    }
    //--[FOCUSSLOT|Slot] puts the highlight on the marked slot.
    else if(tLength >= 13 && !strncasecmp(pString, "[FOCUSSLOT|", 11))
    {
        //--Buffers.
        int tCursor = 11;
        char tCharacterSlotBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tCharacterSlotBuf, tCursor, ']');
        tCursor = tCursor + tParsed;
        int tSlotValue = atoi(tCharacterSlotBuf);

        //--Translate to integer and remove.
        SetSpeakerBySlot(tSlotValue);

        //--Return the length.
        return tCursor;
    }
    //--[SOUND|SFXName] Plays a sound.
    else if(tLength >= 9 && !strncasecmp(pString, "[SOUND|", 7))
    {
        //--Buffers.
        int tCursor = 7;
        char tSFXNameBuf[STD_MAX_LETTERS];

        //--SFX's Name
        int tParsed = ResolveNextWord(pString, tSFXNameBuf, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--Play it.
        AudioManager::Fetch()->PlaySound(tSFXNameBuf);

        //--Return the length.
        return tCursor;
    }
    //--[MUSIC|Musicname] Changes the music.
    else if(tLength >= 9 && !strncasecmp(pString, "[MUSIC|", 7))
    {
        //--Buffers.
        int tCursor = 7;
        char tMusicName[STD_MAX_LETTERS];

        //--Music's Name
        int tParsed = ResolveNextWord(pString, tMusicName, tCursor, ']');
        tCursor = tCursor + tParsed;

        //--Play it.
        AudioManager::Fetch()->PlayMusic(tMusicName);

        //--Return the length.
        return tCursor;
    }

    //--If in Visual Novel mode, run the VN handler.
    if(mIsVisualNovel) return HandleSpecialSequencesVN(pString);

    //--No special cases.
    return 0;
}
void WorldDialogue::ResolveEmotionForForm(const char *pActorName, const char *pStartEmotion, char *sEmotionReturnString)
{
    //--Given an Actor's name and an emotion, resolves which emotion they should display. Many Actors have
    //  multiple forms, but maintain their emotion list between them.
    //--This routine uses a lua function to resolve the form.
    //--The strings should be STD_MAX_LETTERS long.
    strcpy(sEmotionReturnString, "Neutral");
    if(!pActorName || !pStartEmotion) return;

    //--Get the Lua state.
    lua_State *rLuaState = LuaManager::Fetch()->GetLuaState();

    //--Get and check the function. If it doesn't exist, return "Neutral". The function is pushed on the lua stack.
    lua_getglobal(rLuaState, "fnResolvePortraitForForm");
    if(!lua_isfunction(rLuaState, -1))
    {
        lua_pop(rLuaState, 1);
        return;
    }

    //--Push the arguments to the function. It requires the character name and the emotion, in that order.
    lua_pushstring(rLuaState, pActorName);
    lua_pushstring(rLuaState, pStartEmotion);

    //--Run the function. We passed 2 args and expect 1 back.
    if(lua_pcall(rLuaState, 2, 1, 0) != 0)
    {
        DebugManager::ForcePrint("WorldDialogue::ResolveEmotionForForm - Error in lua function: %s\n", lua_tostring(rLuaState, -1));
        lua_pop(rLuaState, 1);
        return;
    }

    //--Get the result. It should be a string.
    if(!lua_isstring(rLuaState, -1))
    {
        DebugManager::ForcePrint("WorldDialogue::ResolveEmotionForForm - Error, function did not return a string.\n");
        lua_pop(rLuaState, 1);
        return;
    }

    //--Copy the returned string into our return string.
    const char *rReturnString = lua_tostring(rLuaState, -1);
    strncpy(sEmotionReturnString, rReturnString, STD_MAX_LETTERS - 1);

    //--Clean up.
    lua_pop(rLuaState, 1);
}

//--[Worker Functions]
char *ResolveLastWord(StarLinkedList *pDialogueCharacterList)
{
    //--Given a list of DialogueCharacter objects, parses backwards and reassembles the last word that was entered.
    //  This is used for specifying which character is speaking.
    //--Cannot return NULL, but can return a 1-letter string with '\0' as its only letter.
    //--String is heap-allocated and must be deallocated by the caller when done.
    if(!pDialogueCharacterList)
    {
        SetMemoryData(__FILE__, __LINE__);
        char *nString = (char *)starmemoryalloc(sizeof(char) * 1);
        nString[0] = '\0';
        return nString;
    }

    //--Parse backwards.
    bool tNeedsToReset = true;
    DialogueCharacter *rCharacter = (DialogueCharacter *)pDialogueCharacterList->SetToTailAndReturn();
    while(rCharacter)
    {
        //--Get the letter. If it's a delimiter...
        if(rCharacter->mLetter == ' ')
        {
            tNeedsToReset = false;
            pDialogueCharacterList->IncrementRandomPointer();
            break;
        }

        //--Previous.
        rCharacter = (DialogueCharacter *)pDialogueCharacterList->DecrementAndGetRandomPointerEntry();
    }

    //--If this flag was not cleared, then there were no delimiters. Start from the beginning of the list.
    if(tNeedsToReset)
    {
        pDialogueCharacterList->SetRandomPointerToHead();
    }

    //--The random-access pointer will now be on the first letter we are interested in.
    int i = 0;
    rCharacter = (DialogueCharacter *)pDialogueCharacterList->GetRandomPointerEntry();
    SetMemoryData(__FILE__, __LINE__);
    char *nString = (char *)starmemoryalloc(sizeof(char) * (pDialogueCharacterList->GetListSize() - pDialogueCharacterList->GetSlotOfElementByPtr(rCharacter) + 1));
    while(rCharacter)
    {
        //--Set.
        nString[i] = rCharacter->mLetter;
        nString[i+1] = '\0';
        i ++;

        //--Previous.
        rCharacter = (DialogueCharacter *)pDialogueCharacterList->IncrementAndGetRandomPointerEntry();
    }

    //--Pass it back.
    return nString;
}
