//--Base
#include "MonoUIVendor.h"

//--Classes
//--CoreClasses
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
MonoUIVendor::MonoUIVendor()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_MONOUIVENDOR;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MVEN");

    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIVendor ======= ]
    ///--[Per-Class Constants]
    cScrollBuf = cxMonoScrollBuf;
    cScrollPage = cxMonoScrollPageSize;
    cYRenderConfirmCancelText = 304.0f;

    ///--[System]

    ///--[Common]
    ///--[Categories]
    ///--[Shop Info]
    ///--[Comparison]
    ///--[Materials Pane]
    ///--[Details]
    ///--[Buy Mode]
    ///--[Sell Mode]
    ///--[Buyback Mode]
    ///--[Gems Mode]
    mAllowGemcutting = true; //Gemcutting is always "allowed" but can only be switched to from Lua calls.

    ///--[Unlock Mode]
    ///--[Merge Help Menu]
    ///--[Strings]
    ///--[Images]
    ///--[ ====== MonoUIVendor ======= ]
    ///--[Variables]
    //--System
    //--Levelup Mode
    mIsLevelupMode = false;
    mLevelupCursor = 0;
    for(int i = 0; i < MONOUIVEN_LEVELUP_STATS; i ++) mLevelupStatIncreases[i] = NULL;

    //--Statistic Lookups
    mStatisticIndexes[0] = STATS_HPMAX;
    mStatisticIndexes[1] = STATS_ATTACK;
    mStatisticIndexes[2] = STATS_ACCURACY;
    mStatisticIndexes[3] = STATS_EVADE;
    mStatisticIndexes[4] = STATS_INITIATIVE;
    mStatisticIndexes[5] = STATS_RESIST_PROTECTION;
    mStatisticIndexes[6] = STATS_RESIST_SLASH;
    mStatisticIndexes[7] = STATS_RESIST_STRIKE;
    mStatisticIndexes[8] = STATS_RESIST_PIERCE;
    mStatisticIndexes[9] = STATS_RESIST_FLAME;

    //--Comparison Frame
    mShowRelativeValuesOnly = true;

    //--Help Strings
    mToggleRelativeString = new StarlightString();

    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIVendor::~MonoUIVendor()
{
    for(int i = 0; i < MONOUIVEN_LEVELUP_STATS; i ++) free(mLevelupStatIncreases[i]);
    delete mToggleRelativeString;
}
void MonoUIVendor::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Vendor UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Vendor UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",             &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIVendor_Strings *MonoUIVendor::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIVendor_Strings::MonoUIVendor_Strings()
{
    //--Local name: "MonoUIBase"
    MonoUIVendor::xrStringData = this;

    //--Strings
    str.mTextPlatina         = InitializeString("MonoUIVendor_mTextPlatina");         //"Platina: "
    str.mTextBuy             = InitializeString("MonoUIVendor_mTextBuy");             //"Buy"
    str.mTextSell            = InitializeString("MonoUIVendor_mTextSell");            //"Sell"
    str.mTextBuyback         = InitializeString("MonoUIVendor_mTextBuyback");         //"Buyback"
    str.mTextName            = InitializeString("MonoUIVendor_mTextName");            //"Name"
    str.mTextPrice           = InitializeString("MonoUIVendor_mTextPrice");           //"Price"
    str.mTextOwned           = InitializeString("MonoUIVendor_mTextOwned");           //"Owned"
    str.mTextSlots           = InitializeString("MonoUIVendor_mTextSlots");           //"Slots"
    str.mTextConfirmPurchase = InitializeString("MonoUIVendor_mTextConfirmPurchase"); //"Confirm Purchase"
    str.mTextCostsPlatina    = InitializeString("MonoUIVendor_mTextCostsPlatina");    //"Costs [iCost] Platina (You have [iCash])"
    str.mTextEquipThisItemTo = InitializeString("MonoUIVendor_mTextEquipItemTo");     //"Equip this item to %%s?"
    str.mTextConfirmSale     = InitializeString("MonoUIVendor_mTextConfirmSale");     //"Confirm Sale"
    str.mTextSaleDetails     = InitializeString("MonoUIVendor_mTextQuantityFor");     //"[sItemName] x [iQuantity] for [iSaleVal] Platina"
    str.mTextMaterials       = InitializeString("MonoUIVendor_mTextMaterials");       //"Materials"
    str.mTextNothingSell     = InitializeString("MonoUIVendor_mTextNothingToSell");   //"You have nothing to sell."
    str.mTextNothingBuyback  = InitializeString("MonoUIVendor_mTextNothingToBuy");    //"There is nothing to buy back."
    str.mTextNothingUnlock   = InitializeString("MonoUIVendor_mTextNothingToUnlock"); //"Nothing to unlock."
    str.mTextComparingTo     = InitializeString("MonoUIVendor_mTextCompareTo");       //"Comparing to: "
    str.mTextReady           = InitializeString("MonoUIVendor_mTextReady");           //"Ready"
    str.mTextIncompatible    = InitializeString("MonoUIVendor_mTextIncompatible");    //"Incompatible"
    str.mTextImmune          = InitializeString("MonoUIVendor_mTextImmune");          //"Immune"
    str.mTextTierGem         = InitializeString("MonoUIVendor_mTextTier");            //"Tier [iTier]"
    str.mTextIngredients     = InitializeString("MonoUIVendor_mTextIngredients");     //"Ingredients:"
    str.mTextBack            = InitializeString("MonoUIVendor_mTextBack");            //"Back"
    str.mTextCreateItem      = InitializeString("MonoUIVendor_mTextCreateItem");      //"Create Item"
    str.mTextLevelUp         = InitializeString("MonoUIVendor_mTextLevelUp");         //"Level Up"
    str.mTextLevelUpCost     = InitializeString("MonoUIVendor_mTextCosts");           //"Cost: %%i"
    str.mTextLevelUpMaxed    = InitializeString("MonoUIVendor_mTextMaxed");           //"Maxed!"
    str.mTextConfirmMerge    = InitializeString("MonoUIVendor_mTextConfirmMerge");    //"Confirm Merge"
    str.mTextWillMergeWith   = InitializeString("MonoUIVendor_mTextWillMerge");       //"%%s will merge with"
    str.mTextTier            = InitializeString("MonoUIVendor_mTextTier");            //"Tier"
    str.mTextColors          = InitializeString("MonoUIVendor_mTextColors");          //"Colors"
    str.mTextSelectAGem      = InitializeString("MonoUIVendor_mTextSelectGem");       //"Select a Gem"
    str.mTextYouHaveNoGems   = InitializeString("MonoUIVendor_mTextHaveNoGems");      //"You have no gems."
    str.mTextProperties      = InitializeString("MonoUIVendor_mTextProperties");      //"Properties"
    str.mTextGemStats        = InitializeString("MonoUIVendor_mTextGemStats");        //"Gem Stats"
    str.mTextAdamantite      = InitializeString("MonoUIVendor_mTextAdamantite");      //"Adamantite"
    str.mTextPowder          = InitializeString("MonoUIVendor_mTextPowder");          //"Powder"
    str.mTextFlakes          = InitializeString("MonoUIVendor_mTextFlakes");          //"Flakes"
    str.mTextShards          = InitializeString("MonoUIVendor_mTextShards");          //"Shards"
    str.mTextPieces          = InitializeString("MonoUIVendor_mTextPieces");          //"Pieces"
    str.mTextChunks          = InitializeString("MonoUIVendor_mTextChunks");          //"Chunks"
    str.mTextOre             = InitializeString("MonoUIVendor_mTextOre");             //"Ore"

    //--Control Sequence
    str.mTextControlShowHelp          = InitializeString("MonoUIVendor_mControlShowHelp");              //"[IMG0] Show Help"
    str.mTextControlItemDetails       = InitializeString("MonoUIVendor_mControlItemDetails");           //"[IMG0] Item Details"
    str.mTextControlChangeCharacter   = InitializeString("MonoUIVendor_mTextControlChangeCharacter");   //"[IMG0][IMG1] Change Character"
    str.mTextControlScroll10          = InitializeString("MonoUIVendor_mTextControlScroll10");          //"[IMG0] (Hold) Scroll x10"
    str.mTextControlToggleCompareSlot = InitializeString("MonoUIVendor_mTextControlToggleCompareSlot"); //"[IMG0] Toggle Compare Slot"
    str.mTextControlConfirm           = InitializeString("MonoUIVendor_mTextControlConfirm");           //"[IMG0] Confirm"
    str.mTextControlCancel            = InitializeString("MonoUIVendor_mTextControlCancel");            //"[IMG0] Cancel"
    str.mTextControlAdjustQuantity    = InitializeString("MonoUIVendor_mTextControlAdjustQuantity");    //"[IMG0][IMG1] Adjust Quantity"
    str.mTextControlQuantity10        = InitializeString("MonoUIVendor_mTextControlQuantity10");        //"[IMG0][IMG1] Quantity x10"
    str.mTextControlDisassembleGem    = InitializeString("MonoUIVendor_mTextControlDisassembleGem");    //"[IMG0] Disassemble Gem"
    str.mTextControlToggleStats       = InitializeString("MonoUIVendor_mTextControlToggleStats");       //"[IMG0] Toggle Materials/Statistics"
    str.mTextControlToggleMatPage     = InitializeString("MonoUIVendor_mTextControlToggleMatPage");     //"[IMG0] + [IMG1]/[IMG2] Toggle Materials Page"
    str.mTextControlToggleRelative    = InitializeString("MonoUIVendor_mTextControlToggleRelative");    //"[IMG0] Toggle Relative/Absolute Values"
}
MonoUIVendor_Strings::~MonoUIVendor_Strings()
{
    for(int i = 0; i < MONOUIVENDOR_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIVendor_Strings::GetSize()
{
    return MONOUIVENDOR_STRINGS_TOTAL;
}
void MonoUIVendor_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIVENDOR_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
bool MonoUIVendor::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE)  return true;
    if(pType == POINTER_TYPE_ADVUIVENDOR)  return true;
    if(pType == POINTER_TYPE_MONOUIVENDOR) return true;
    return false;
}

///======================================= Manipulators ===========================================
void MonoUIVendor::TakeForeground()
{
    ///--[Documentation]
    //--Same as the base call, but also resets some flags local to the MonocerosMenu.
    AdvUIVendor::TakeForeground();

    //--Variables.
    mAllowGemcutting = true; //Gemcutting is always "Allowed" but can only be switched to from Lua.
    mIsLevelupMode = false;
    mLevelupCursor = 0;

    //--Set controls.
    mToggleRelativeString-> AutoSetControlString("[IMG0] Toggle Relative/Absolute Values", 1, "OpenFieldAbilityMenu");

    //--Override strings with localization.
    mShowHelpString->SetString(xrStringData->str.mTextControlShowHelp);
    mShowHelpString->CrossreferenceImages();
    mShowDetailsString->SetString(xrStringData->str.mTextControlItemDetails);
    mShowDetailsString->CrossreferenceImages();
    mCharacterChangeString->SetString(xrStringData->str.mTextControlChangeCharacter);
    mCharacterChangeString->CrossreferenceImages();
    mScrollFastString->SetString(xrStringData->str.mTextControlScroll10);
    mScrollFastString->CrossreferenceImages();
    mToggleCompareString->SetString(xrStringData->str.mTextControlToggleCompareSlot);
    mToggleCompareString->CrossreferenceImages();
    mConfirmPurchaseString->SetString(xrStringData->str.mTextControlConfirm);
    mConfirmPurchaseString->CrossreferenceImages();
    mCancelPurchaseString->SetString(xrStringData->str.mTextControlCancel);
    mCancelPurchaseString->CrossreferenceImages();
    mAdjustQuantityString->SetString(xrStringData->str.mTextControlAdjustQuantity);
    mAdjustQuantityString->CrossreferenceImages();
    mTenQuantityString->SetString(xrStringData->str.mTextControlQuantity10);
    mTenQuantityString->CrossreferenceImages();
    mDisassembleString->SetString(xrStringData->str.mTextControlDisassembleGem);
    mDisassembleString->CrossreferenceImages();
    mToggleCompareMaterialsString->SetString(xrStringData->str.mTextControlToggleStats);
    mToggleCompareMaterialsString->CrossreferenceImages();
    mToggleMaterialsPageString->SetString(xrStringData->str.mTextControlToggleMatPage);
    mToggleMaterialsPageString->CrossreferenceImages();
    mToggleRelativeString->SetString(xrStringData->str.mTextControlToggleRelative);
    mToggleRelativeString->CrossreferenceImages();

    //--Adjust sizings for some of the strings.
    mConfirmPurchaseString->SetImageP(0, 14.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mConfirmPurchaseString->CrossreferenceImages();
    mCancelPurchaseString->SetImageP(0, 14.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mCancelPurchaseString->CrossreferenceImages();
    mAdjustQuantityString->SetImageP(0, 14.0f, ControlManager::Fetch()->ResolveControlImage("Left"));
    mAdjustQuantityString->SetImageP(1, 14.0f, ControlManager::Fetch()->ResolveControlImage("Right"));
    mAdjustQuantityString->CrossreferenceImages();
    mTenQuantityString->SetImageP(0, 14.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mTenQuantityString->SetImageP(1, 14.0f, ControlManager::Fetch()->ResolveControlImage("UpLevel"));
    mTenQuantityString->CrossreferenceImages();
}
void MonoUIVendor::SetShopInformation(const char *pShopName, const char *pPath, const char *pTeardownPath)
{
    ///--[Documentation]
    //--Calls TakeForeground() to reset variables, then runs shop scripts and sets names. The teardown path
    //  is optional.
    AdvUIVendor::SetShopInformation(pShopName, pPath, pTeardownPath);
}

///======================================= Core Methods ===========================================
void MonoUIVendor::RefreshMenuHelp()
{
}
void MonoUIVendor::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Determines where the cursor should be given the inventory cursor and scrollbar offsets. This is used
    //  for the standard parts of the UI. Gems have their own handler.
    float cNameLft = 289.0f;
    float cNameWid = 0.0f;

    //--Compute Y position.
    float cSlotY = 239.0f;
    float cSlotH =  56.0f;

    ///--[Name Length]
    //--The length of the entry depends on the mode.
    if(mIsBuyMode)
    {
        cNameWid = GetBuyCursorNameWidth();
    }
    else if(mIsSellMode)
    {
        cNameWid = GetSellCursorNameWidth();
    }
    else if(mIsBuybackMode)
    {
        cNameWid = GetBuybackCursorNameWidth();
    }
    else if(mIsGemsMode)
    {
        cNameLft = 314.0f;
        cSlotY = 243.0f;
        cSlotH =  43.0f;
        cNameWid = GetGemsCursorNameWidth(cNameLft);
    }
    else if(mIsUnlockMode)
    {
        cNameWid = GetUnlockCursorNameWidth();
    }

    //--Positions.
    float cLft = cNameLft - 3.0f;
    float cTop = cSlotY + (cSlotH * (mCursor - mSkip));
    float cRgt = cNameLft + cNameWid + 4.0f;
    float cBot = cTop + cSlotH + 2.0f;

    //--Set.
    mHighlightPos.MoveTo (       cLft,        cTop, cxAdvCurTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxAdvCurTicks);
}
void MonoUIVendor::ScrollMode(int pAmount)
{
    ///--[Documentation]
    //--Same as the basic version but gems mode is always a standalone in Monoceros.
    if(pAmount == 0) return;

    ///--[Lockout]
    //--If this flag is set, modes cannot scroll.
    if(mIsBuyOnly) return;

    ///--[Mode Switching]
    if(mIsBuyMode)
    {
        if(pAmount > 0)
            ActivateModeSell();
        else
            ActivateModeBuyback();
    }
    else if(mIsSellMode)
    {
        if(pAmount > 0)
            ActivateModeBuyback();
        else
            ActivateModeBuy();
    }
    else if(mIsBuybackMode)
    {
        if(pAmount > 0)
            ActivateModeBuy();
        else
            ActivateModeSell();
    }

    ///--[Gems Mode]
    //--In gems mode, do nothing and exit without playing a sound.
    if(mIsGemsMode) return;

    ///--[SFX]
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
}
void MonoUIVendor::ActivateMode(bool &sModeFlag)
{
    ///--[Documentation]
    //--Same as the base call but modified to add levelup mode.
    mIsBuyMode = false;
    mIsSellMode = false;
    mIsBuybackMode = false;
    mIsGemsMode = false;
    mIsUnlockMode = false;
    mIsLevelupMode = false;

    //--Set provided flag to true.
    sModeFlag = true;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIVendor::UpdateForeground(bool pCannotHandleUpdate)
{
    //--Runs the levelup update if that mode is active.
    if(mIsLevelupMode)
    {
        UpdateModeLevelup();
    }

    //--Run the base update.
    return AdvUIVendor::UpdateForeground(pCannotHandleUpdate);
}
void MonoUIVendor::UpdateBackground()
{
    return AdvUIVendor::UpdateBackground();
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIVendor::RenderPieces(float pVisAlpha)
{
    //--Base call.
    AdvUIVendor::RenderPieces(pVisAlpha);

    //--Levelup mode.
    if(mIsLevelupMode) RenderUnalignedLevelup(pVisAlpha);
}
void MonoUIVendor::RenderLft(float pVisAlpha)
{
    //--Base call.
    AdvUIVendor::RenderLft(pVisAlpha);

    //--Levelup mode.
    if(mIsLevelupMode) RenderLftLevelup(pVisAlpha);
}
void MonoUIVendor::RenderTop(float pVisAlpha)
{
    //--Base call.
    AdvUIVendor::RenderTop(pVisAlpha);

    //--Levelup mode.
    if(mIsLevelupMode) RenderTopLevelup(pVisAlpha);
}
void MonoUIVendor::RenderRgt(float pVisAlpha)
{
    //--Base call.
    AdvUIVendor::RenderRgt(pVisAlpha);

    //--Levelup mode.
    if(mIsLevelupMode) RenderRgtLevelup(pVisAlpha);
}
void MonoUIVendor::RenderBot(float pVisAlpha)
{
    //--Base call.
    AdvUIVendor::RenderBot(pVisAlpha);

    //--Levelup mode.
    if(mIsLevelupMode) RenderBotLevelup(pVisAlpha);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
