-- |[ ======================================= fnExecAll() ====================================== ]|
--Function causes all subfolders in a folder to execute a specified file.
--  Relies on the SugarFileSystem, which can be Allegro or Boost.
function fnExecAll(psCurrentPath, psFilePattern)

    -- |[Argument Check]|
    if(psCurrentPath == nil) then return end
    if(psFilePattern == nil) then return end

    -- |[Execution]|
	--Scan the directory.
	FS_Open(psCurrentPath, false)

	--For each entry, appends the psFilePattern and try to run it.
	--  Ignore non-directories. Directories end in '/'.
	local sPath = FS_Iterate()
	while(sPath ~= "NULL") do

		--Check the last byte for '/'.
		local iLength = string.len(sPath)
		if(string.byte(sPath, iLength) == string.byte("/")) then

			--Append the psFilePattern on the end and call it
			LM_ExecuteScript(sPath .. psFilePattern)

		end

        --Next.
		sPath = FS_Iterate()

	end

	--Clean.
	FS_Close()

end