//--Base
#include "AbyssCombat.h"

//--Classes
#include "AbyCombatPrediction.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
AbyssCombat::AbyssCombat()
{
    mHasInitializedAbyCombat = false;
    Initialize();
}
AbyssCombat::~AbyssCombat()
{
    Disassemble();
}

///--[Public Statics]
//--Path used to resolve what is said by who when the player wins a battle.
char *AbyssCombat::xVictoryDialoguePath = NULL;

//--Debug flag, causes callouts to always play so you can check their orientation.
bool AbyssCombat::xAlwaysPlayCallouts = false;

//--Disable override, used for abilities that un-disable party members in special circumstances.
bool AbyssCombat::xIgnoreDisablesForTargets = false;

///======================================== Structures ============================================
void TutorialStringPack::Initialize()
{
    mX = 0.0f;
    mY = 0.0f;
    mText = NULL;
}
void TutorialStringPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    TutorialStringPack *rPtr = (TutorialStringPack *)pPtr;
    free(rPtr->mText);
    free(rPtr);
}

///===================================== Property Queries =========================================
bool AbyssCombat::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVCOMBAT) return true;
    if(pType == POINTER_TYPE_ABYSSCOMBAT) return true;
    if(pType == POINTER_TYPE_CARNATIONCOMBAT) return true;
    return false;
}
bool AbyssCombat::IsPartyDisabled(int pSlot)
{
    if(pSlot < 0 || pSlot >= ABYCOM_PLAYER_PARTY_MAX_SIZE) return false;
    return mIsMemberDisabled[pSlot];
}
bool AbyssCombat::IsPartyDisabledID(uint32_t pID)
{
    ///--[Documentation]
    //--Locates the slot of the entity with the given ID and returns if it is disabled.
    //  Defaults to false in case of error.
    if(pID == 0) return false;

    //--Run across the combat party.
    int tPartySize = GetCombatPartyCount();
    for(int i = 0; i < tPartySize; i ++)
    {
        //--Get and check the member.
        AdvCombatEntity *rEntity = GetCombatMemberI(i);
        if(!rEntity || rEntity->GetID() != pID) continue;

        //--Found the member. Return if this slot is disabled.
        return IsPartyDisabled(i);
    }

    //--Not found. Default to false.
    return false;
}
bool AbyssCombat::DoesGlobalAbilityExist(const char *pName)
{
    void *rCheckAbility = mGlobalAbilities->GetElementByName(pName);
    return (rCheckAbility != NULL);
}
AdvCombatAbility *AbyssCombat::GetGlobalAbility(const char *pAbilityName)
{
    return (AdvCombatAbility *)mGlobalAbilities->GetElementByName(pAbilityName);
}
const char *AbyssCombat::GetPathOfGlobalAbility(const char *pAbilityName)
{
    AdvCombatAbility *rCheckAbility = (AdvCombatAbility *)mGlobalAbilities->GetElementByName(pAbilityName);
    if(!rCheckAbility) return "NULL";
    return rCheckAbility->GetScriptPath();
}

///======================================= Manipulators ===========================================
void AbyssCombat::SetCombatTextScale(float pScale)
{
    mCombatTextScale = pScale;
    if(mCombatTextScale < 0.01f) mCombatTextScale = 0.01f;
}
void AbyssCombat::SetDisallowEmergencyEscape(bool pFlag)
{
    mDisallowEmergencyEscape = pFlag;
}
void AbyssCombat::SetPartyDisabledFlag(int pSlot, bool pFlag)
{
    ///--[Documentation]
    //--Sets whether a party member is disabled or not. This typically happens when that party member
    //  is transformed into an enemy.
    //--If a member is disabled, then they no longer render and can no longer be targeted. This routine
    //  also removes them from the turn order list.
    if(pSlot < 0 || pSlot >= ABYCOM_PLAYER_PARTY_MAX_SIZE) return;

    //--Set.
    bool tOldFlag = mIsMemberDisabled[pSlot];
    mIsMemberDisabled[pSlot] = pFlag;

    //--If a member is newly disabled, remove them from the turn list.
    if(!tOldFlag && pFlag)
    {
        //--Get the associated pointer.
        AdvCombatEntity *rCharacterPtr = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(pSlot);
        if(!rCharacterPtr) return;

        //--Remove them from the turn order list.
        mrTurnOrder->RemoveElementP(rCharacterPtr);
    }
}
void AbyssCombat::SetPartyDisabledFlagByID(uint32_t pUniqueID, bool pFlag)
{
    ///--[Documentation]
    //--As above, but gets the slot from the unique ID provided.
    for(int i = 0; i < ABYCOM_PLAYER_PARTY_MAX_SIZE; i ++)
    {
        //--Get the associated pointer.
        AdvCombatEntity *rCharacterPtr = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(i);
        if(!rCharacterPtr) continue;

        //--Get the ID and compare it.
        uint32_t tUniqueID = rCharacterPtr->GetID();
        if(pUniqueID == tUniqueID)
        {
            SetPartyDisabledFlag(i, pFlag);
            return;
        }
    }

    //--ID not found in the player's party, do nothing.
}
void AbyssCombat::SetTutorialOverlay(const char *pDLPath)
{
    ///--[Documentation]
    //--Sets a tutorial overlay that goes above combat display. Once the timer finishes, the player
    //  needs to push any key to dismiss the tutorial. Also allows crossfading between images.
    //--Pass NULL or "Null" to clear.
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        //--Flag.
        mIsShowingTutorialOverlay = false;

        //--Switch images.
        rTutorialOverlayPrv = rTutorialOverlayCur;
        rTutorialOverlayCur = NULL;

        //--Switch strings. Previous strings are deallocated.
        delete mTutorialStringsPrv;
        mTutorialStringsPrv = mTutorialStringsCur;
        mTutorialStringsCur = NULL;
        return;
    }

    //--Set.
    mIsShowingTutorialOverlay = true;
    rTutorialOverlayPrv = rTutorialOverlayCur;
    rTutorialOverlayCur = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--String lists.
    delete mTutorialStringsPrv;
    mTutorialStringsPrv = mTutorialStringsCur;
    mTutorialStringsCur = new StarLinkedList(true);
}
void AbyssCombat::RegisterTutorialString(float pX, float pY, const char *pString)
{
    ///--[Documentation]
    //--Registers the given string to a string package for rendering. Does nothing if no tutorial is active.
    if(!pString || !mTutorialStringsCur) return;

    //--Package.
    TutorialStringPack *nPackage = (TutorialStringPack *)starmemoryalloc(sizeof(TutorialStringPack));
    nPackage->Initialize();
    nPackage->mX = pX;
    nPackage->mY = pY;
    nPackage->mText = InitializeString(pString);

    //--Attempt to translate the tutorial, if applicable.
    StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_COMBAT);
    if(rTranslation) rTranslation->Translate(nPackage->mText);

    //--Register.
    mTutorialStringsCur->AddElementAsTail("X", nPackage, &TutorialStringPack::DeleteThis);
}

///======================================= Core Methods ===========================================
StarLinkedList *AbyssCombat::ResolvePlayerCommandList()
{
    ///--[Documentation]
    //--Resolves and returns which command list the player is currently picking from. If there is nothing
    //  on the stack, then returns the base list. If the stack has an invalid entry, returns the
    //  base list again, for safety.
    if(mrPlayerCommandStack->GetListSize() < 1) return mCommandList;

    //--Check the stack:
    CommandListEntry *rStackHead = (CommandListEntry *)mrPlayerCommandStack->GetHead();
    if(!rStackHead) return mCommandList;

    //--If the list wasn't booted, fail.
    if(!rStackHead->mSublist) return mCommandList;

    //--Return the head list.
    return rStackHead->mSublist;
}
void AbyssCombat::ReresolveDescriptionAbility()
{
    ///--[Documentation]
    //--Whenever the cursor or command stack changes, re-resolve which ability is highlighted and store
    //  it for the description window. If no ability is available, set it to NULL.
    rDescriptionAbility = NULL;

    //--Get the active window.
    int tUseCursor = mPlayerCommandCursor;
    StarLinkedList *rCommandStack = ResolvePlayerCommandList();
    if(rCommandStack == mCommandList) tUseCursor = mPlayerCommandBaseCursor;

    //--Get the highlighted ability.
    CommandListEntry *rCommandEntry = (CommandListEntry *)rCommandStack->GetElementBySlot(tUseCursor);
    if(rCommandEntry)
    {
        rDescriptionAbility = rCommandEntry->rAssociatedAbility;
    }
}
float AbyssCombat::ComputeIdealX(bool pIsLeftSide, bool pIsOffscreen, int pPosition, int pTotalEntries, StarBitmap *pCombatPortrait)
{
    ///--[Documentation]
    //--Calculates the middle point where entities are standing. The player's party does not move
    //  in this game mode, and enemies are all lined up in the background at a fixed Y position.
    //--Enemies can reposition as their roster increases or decreases.
    //--Offscreen isn't a thing for enemies in this combat mode. The flag is ignored.
    if(pIsLeftSide) return 0.0f;

    //--Exactly one enemy. Put them in the middle.
    if(pTotalEntries <= 1)
    {
        return VIRTUAL_CANVAS_X * 0.50f;
    }

    //--If we got this far, there are at least two total entities. Divvy the space from the leftmost
    //  position to the right edge with padding.
    float cLftPoint = VIRTUAL_CANVAS_X * 0.15f;
    float cRgtPoint = VIRTUAL_CANVAS_X * 0.85f;
    float cSpacePerEntity = (cRgtPoint - cLftPoint) / (float)pTotalEntries;

    //--Multiply, and add a half-slot to center the entity.
    return cLftPoint + (cSpacePerEntity * (float)(pPosition + 0.50f));
}
float AbyssCombat::ComputeIdealY(AdvCombatEntity *pEntity)
{
    //--Enemies render at a fixed height, player entities ignore this.
    return ABYCOM_FIXED_ENEMY_Y;
}
void AbyssCombat::PositionEnemiesByTarget(StarLinkedList *pTargetList)
{
    //--Enemy positioning is considerably faster in this game mode. Enemies are always onscreen.
    int tTotalEnemies = mrEnemyCombatParty->GetListSize();
    int i = 0;

    //--Iterate.
    AdvCombatEntity *rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemy)
    {
        //--Compute and set.
        float cIdealX = ComputeIdealX(false, false, i, tTotalEnemies, rEnemy->GetCombatPortrait());
        float cIdealY = ComputeIdealY(rEnemy);

        //--If this is the first time the enemy has been positioned, they start offscreen.
        if(!rEnemy->HasBeenAssignedPosition())
        {
            rEnemy->MarkAssignedPosition();
            rEnemy->MoveToPosition(cIdealX, cIdealY  + ABYCOM_REPOSITION_Y_OFFSET, 0);
            rEnemy->SetFadeInTimers(0, ABYCOM_REPOSITION_TICKS);
        }

        //--Set, move to.
        rEnemy->SetIdealPosition(cIdealX, cIdealY);
        rEnemy->MoveToPosition(cIdealX, cIdealY, ABYCOM_REPOSITION_TICKS);

        //--Next.
        i ++;
        rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
}
bool AbyssCombat::IsPlayerDefeated()
{
    ///--[Documentation]
    //--Override of the basic behavior. In AbyssCombat, the player's party can be disabled or otherwise
    //  unable to act. A script call handles this. If all three respond with true, then the player's
    //  party is defeated. The usual approach of being KO'd still applies.
    if(AdvCombat::IsPlayerDefeated()) return true;

    //--Run this special script call.
    for(int i = 0; i < ABYCOM_PLAYER_PARTY_MAX_SIZE; i ++)
    {
        //--Skip empty slots.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(i);
        if(!rEntity) continue;

        //--Entity is disabled. Pass over it.
        if(mIsMemberDisabled[i]) continue;

        //--Set this flag to false and run the script with a special code.
        mReplyToDefeated = false;
        const char *rCombatScript = rEntity->GetResponsePath();
        LuaManager::Fetch()->PushExecPop(rEntity, rCombatScript, 1, "N", (float)ACE_SCRIPT_CODE_ABYDEFEAT);

        //--If the flag is set to false, stop iterating. The party is not defeated.
        if(!mReplyToDefeated) return false;
    }

    //--If we got this far, all of the player's party reported defeat.
    return true;
}
void AbyssCombat::TakeAbilityFromEntity(const char *pAbilityName)
{
    ///--[Documentation]
    //--For universal abilities, such as those used by items, they are created belonging to an entity
    //  and then liberated from that entity's job. They are then registered to the mGlobalAbilities list.
    //--This function assumes an AdvCombatEntity * is the active object.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATENTITY)) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)DataLibrary::Fetch()->rActiveObject;

    //--Get the ability list.
    StarLinkedList *rAbilityList = rEntity->GetAbilityList();

    //--Locate the named ability.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementByName(pAbilityName);
    if(!rAbility) return;

    //--Ability exists. Liberate it. Register it to our internal list.
    rAbilityList->SetRandomPointerToThis(rAbility);
    rAbilityList->LiberateRandomPointerEntry();
    mGlobalAbilities->AddElement(pAbilityName, rAbility, &AdvCombatAbility::DeleteThis);

    //--Replace the ability's owner with a NULL pointer.
    rAbility->SetOwner(NULL);

    //--Debug.
    //fprintf(stderr, "Registered global ability %s. Now %i abilities.\n", pAbilityName, mGlobalAbilities->GetListSize());
}
void AbyssCombat::CreatePredictionBox(const char *pBoxName)
{
    //--Error check.
    if(!pBoxName) return;

    //--Create.
    AbyCombatPrediction *nBox = new AbyCombatPrediction();
    nBox->SetBorderCard(AbyssImg.Data.rFramePrediction, 9.0f);
    nBox->SetFont(AbyssImg.Data.rFontPrediction);

    //--Store.
    mPredictionBoxes->AddElement(pBoxName, nBox, &RootObject::DeleteThis);

    //--If there is a target cluster active, associate us with that cluster.
    if(rPredictionCluster)
    {
        rPredictionCluster->mrPredictionBoxList->AddElementAsTail("X", nBox);
    }
}
void AbyssCombat::SetPredictionBoxStringImage(const char *pBoxName, int pStringIndex, int pSlot, float pYOffset, const char *pPath)
{
    //--Same as the base version, but adds to the Y offset for the new fonts.
    AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->GetElementByName(pBoxName);
    if(!rPredictionBox) return;
    StarlightString *rInternalString = rPredictionBox->GetString(pStringIndex);
    if(!rInternalString) return;
    rInternalString->SetImageS(pSlot, pYOffset+3.0f, pPath);
}

///========================================= Targeting ============================================
void AbyssCombat::PopulateTargetsByCode(const char *pCodeName, void *pCaller)
{
    ///--[Documentation]
    //--Runs the same as normal target population, but removes the special-KO case party members if
    //  any are on the target list.
    //--If the flag xIgnoreDisablesForTargets is set to true, then this won't happen.
    AdvCombat::PopulateTargetsByCode(pCodeName, pCaller);
    if(xIgnoreDisablesForTargets == true) return;

    //--Fast-access pointers.
    AdvCombatEntity *rPartyPtr[ABYCOM_PLAYER_PARTY_MAX_SIZE];
    rPartyPtr[0] = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(0);
    rPartyPtr[1] = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(1);
    rPartyPtr[2] = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(2);

    //--Scan across the target clusters.
    TargetCluster *rCheckCluster = (TargetCluster *)mTargetClusters->SetToHeadAndReturn();
    while(rCheckCluster)
    {
        //--Fast-access pointer.
        StarLinkedList *rTargetList = rCheckCluster->mrTargetList;

        //--Remove disabled entries.
        for(int i = 0; i < ABYCOM_PLAYER_PARTY_MAX_SIZE; i ++)
        {
            if(mIsMemberDisabled[i]) rTargetList->RemoveElementP(rPartyPtr[i]);
        }

        //--If, after removals, the cluster is empty, remove the cluster.
        if(rTargetList->GetListSize() < 1)
        {
            mTargetClusters->RemoveRandomPointerEntry();
        }

        //--Next.
        rCheckCluster = (TargetCluster *)mTargetClusters->IncrementAndGetRandomPointerEntry();
    }
}

///======================================== Turn Order ============================================
void AbyssCombat::BeginTurn()
{
    ///--[Documentation and Setup]
    //--Basically runs the standard begin-turn code, and then purges player party members who are disabled.
    if(mCurrentTurn > 0) AdvCombat::EndTurn();
    AdvCombat::BeginTurn();

    //--Purge.
    for(int i = 0; i < ABYCOM_PLAYER_PARTY_MAX_SIZE; i ++)
    {
        //--Do nothing if not disabled.
        if(!mIsMemberDisabled[i]) continue;

        //--Fetch pointer.
        AdvCombatEntity *rPlayerChar = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(i);
        if(!rPlayerChar) continue;

        //--Purge.
        mrTurnOrder->RemoveElementP(rPlayerChar);
    }
}

///=================================== Private Core Methods =======================================
///========================================= File I/O =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
AbyssCombat *AbyssCombat::Fetch()
{
    ///--[Documentation]
    //--Same basic code as the AdvCombat version but guarantees the pointer will be a valid AbyssCombat
    //  subtype, or NULL if not.
    AbyssCombat *rCombat = (AbyssCombat *)AdvCombat::Fetch();
    if(!rCombat->IsOfType(POINTER_TYPE_ABYSSCOMBAT)) return NULL;

    //--Valid.
    return rCombat;
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
