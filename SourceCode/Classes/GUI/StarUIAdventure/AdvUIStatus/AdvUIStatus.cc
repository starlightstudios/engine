//--Base
#include "AdvUIStatus.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
AdvUIStatus::AdvUIStatus()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "ASTA");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIStatus ======= ]
    ///--[System]
    //--System
    mArrowTimer = 0;

    //--Character
    mCharacterCursor = 0;
    mIsSwappingCharacter = false;
    mCharacterSwapTimer = 0;
    mPrevCharacterSlot = 0;

    //--Colors
    mColor_DarkEquipSlot.SetRGBAF(0.50f, 0.50f, 0.50f, 1.0f);
    mColor_ResistBonus.SetRGBAF(0.80f, 0.10f, 0.80f, 1.0f);
    mColor_ResistMalus.SetRGBAF(0.80f, 0.10f, 0.15f, 1.0f);

    //--Strings
    mShowHelpString = new StarlightString();
    mCharacterLftString = new StarlightString();
    mCharacterRgtString = new StarlightString();

    //--Help Menu
    AllocateHelpStrings(cxHelpStringsTotal);

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIStatus::~AdvUIStatus()
{
    delete mShowHelpString;
    delete mCharacterLftString;
    delete mCharacterRgtString;
}
void AdvUIStatus::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Status UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIStatus::TakeForeground()
{
    ///--[Documentation]
    //--Called when this object is shown.

    ///--[Variables]
    //--Reset.
    mCharacterCursor = 0;
    mIsSwappingCharacter = false;

    ///--[Strings]
    mShowHelpString->    AutoSetControlString("[IMG0] Show Help",                           1, "F1");
    mCharacterLftString->AutoSetControlString("[IMG0] or [IMG1]+[IMG2] Previous Character", 3, "UpLevel", "Ctrl", "Left");
    mCharacterRgtString->AutoSetControlString("[IMG0] or [IMG1]+[IMG2] Next Character",     3, "DnLevel", "Ctrl", "Right");

    ///--[Character Refresh]
    //--Get the character and refresh their stats.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(0);
    if(rActiveEntity) rActiveEntity->RefreshStatsForUI();
}

///======================================= Core Methods ===========================================
void AdvUIStatus::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Normal strings.
    SetHelpString(i, "This is the status menu. Press [IMG0] or [IMG1] to change characters.", 2, "DnLevel", "UpLevel");
    SetHelpString(i, "On the left side, you can see your character's experience, basic stats, and current level.");
    SetHelpString(i, "The middle shows the character's equipment and gems. The right shows resistances.");
    SetHelpString(i, "For more information on the damage types and what the statistics mean, see the ");
    SetHelpString(i, "Journal's glossary.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");
    for(int p = 0; p < 11; p ++) SetHelpString(i, "");


    //--Damage Legend.
    SetHelpString(i, "Icon Legend: Damage Types");
    mHelpMenuStrings[i]->SetString("[IMG0] Slashing [MOV200][IMG1] Striking [MOV400][IMG2] Piercing [MOV600][IMG3] Flaming [MOV800][IMG4] Freezing [MOV1000][IMG5] Shocking");
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Slashing"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Striking"));
    mHelpMenuStrings[i]->SetImageP(2, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Piercing"));
    mHelpMenuStrings[i]->SetImageP(3, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Flaming"));
    mHelpMenuStrings[i]->SetImageP(4, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Freezing"));
    mHelpMenuStrings[i]->SetImageP(5, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Shocking"));
    i ++;

    mHelpMenuStrings[i]->SetString("[IMG0] Crusading [MOV200][IMG1] Obscuring [MOV400][IMG2] Bleeding [MOV600][IMG3] Poisoning [MOV800][IMG4] Corroding [MOV1000][IMG5] Terrifying");
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Crusading"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Obscuring"));
    mHelpMenuStrings[i]->SetImageP(2, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Bleeding"));
    mHelpMenuStrings[i]->SetImageP(3, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Poisoning"));
    mHelpMenuStrings[i]->SetImageP(4, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Corroding"));
    mHelpMenuStrings[i]->SetImageP(5, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Terrifying"));
    i ++;
    SetHelpString(i, "");

    //--Statistics Legend.
    SetHelpString(i, "Icon Legend: Statistics");
    mHelpMenuStrings[i]->SetString("[IMG0] Health [MOV200][IMG1] Power [MOV400][IMG2] Accuracy [MOV600][IMG3] Evade [MOV800][IMG4] Initiative [MOV1000][IMG5] Protection");
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Health"));
    mHelpMenuStrings[i]->SetImageP(1, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Attack"));
    mHelpMenuStrings[i]->SetImageP(2, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Accuracy"));
    mHelpMenuStrings[i]->SetImageP(3, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Evade"));
    mHelpMenuStrings[i]->SetImageP(4, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Initiative"));
    mHelpMenuStrings[i]->SetImageP(5, cxAdvBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Protection"));
    i ++;

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void AdvUIStatus::ScrollCharacter(int pAmount)
{
    ///--[Documentation]
    //--Increments or decrements the character cursor by the given amount, typically in response to a press of
    //  the shoulder buttons. Handles playing sound effects as well.

    ///--[No Scroll Amount]
    //--Scroll amount was zero. Do nothing silently.
    if(!pAmount) return;

    ///--[Small Party]
    //--Party doesn't have enough members to scroll. Fail and play a sound.
    if(AdvCombat::Fetch()->GetActivePartyCount() < 2)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    ///--[Begin Scroll]
    //--Set.
    mIsSwappingCharacter = true;
    mCharacterSwapTimer = 0;
    mPrevCharacterSlot = mCharacterCursor;
    mCharacterCursor += pAmount;

    //--Wrap.
    if(mCharacterCursor < 0)
    {
        mCharacterCursor = AdvCombat::Fetch()->GetActivePartyCount() - 1;
    }
    if(mCharacterCursor >= AdvCombat::Fetch()->GetActivePartyCount())
    {
        mCharacterCursor = 0;
    }

    //--Get the new character and refresh their stats.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(rActiveEntity) rActiveEntity->RefreshStatsForUI();

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|Select");
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIStatus::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer, 0, cxAdvVisTicks, true);

    //--Arrow oscillation.
    mArrowTimer = (mArrowTimer + 1) % cxAdvOscTicks;

    //--Character swapping timer.
    if(mIsSwappingCharacter)
    {
        if(mCharacterSwapTimer < cAdvSwapTicks)
        {
            mCharacterSwapTimer ++;
        }
        else
        {
            mIsSwappingCharacter = false;
        }
    }

    //--Standard Timers.
    StandardTimer(mHelpVisibilityTimer, 0, cHlpTicks, mIsShowingHelp);

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[Control Handling]
    //--This object has no sub-modes and very little control handling.

    //--Decrement the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Left")    ))
    {
        ScrollCharacter(-1);
        return true;
    }
    if((rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel")) ||
       (rControlManager->IsDown("Ctrl")          &&  rControlManager->IsFirstPress("Right")  ))
    {
        ScrollCharacter(1);
        return true;
    }

    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}
void AdvUIStatus::UpdateBackground()
{
    ///--[Documentation]
    //--In the background, just run vis timers.
    StandardTimer(mVisibilityTimer, 0, cxAdvVisTicks, false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIStatus::RenderLft(float pVisAlpha)
{
    ///--[Documentation]
    //--Left block.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_LEFT, pVisAlpha);

    ///--[Rendering]
    //--Left-side arrow.
    float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1) AdvImages.rOverlay_ArrowLft->Draw(cArrowOscillate * -1.0f, 0.0f);

    ///--[Normal Portrait]
    //--Get selected character.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) { AutoPieceClose(); return; }

    //--Get active character portrait. If it comes back NULL, switch it to a placeholder.
    StarBitmap *rPortrait = rActiveEntity->GetCombatPortrait();
    if(!rPortrait) rPortrait = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/System/System/WhitePixel");

    //--Render the character in question.
    if(!mIsSwappingCharacter)
    {
        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS);

        //--Render.
        rPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);
        RenderBasics(rActiveEntity, cColorAlpha);
    }
    ///--[Swapping Characters]
    else
    {
        //--Percentage.
        float cScrollPercent = EasingFunction::QuadraticInOut(mCharacterSwapTimer, cAdvSwapTicks);

        //--Get the previous entity.
        AdvCombatEntity *rPreviousEntity = AdvCombat::Fetch()->GetActiveMemberI(mPrevCharacterSlot);
        if(rPreviousEntity)
        {
            //--Stencil and position.
            TwoDimensionRealPoint tRenderDim = rPreviousEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS);

            //--Get portrait.
            float cXOffset = VIRTUAL_CANVAS_X * (cScrollPercent) * -1.0f;
            StarBitmap *rPortrait = rPreviousEntity->GetCombatPortrait();
            if(rPortrait)
            {
                rPortrait->Draw(tRenderDim.mXCenter + cXOffset, tRenderDim.mYCenter);
            }

            //--Offset the basics by the offset.
            glTranslatef(cXOffset, 0.0f, 0.0f);
            RenderBasics(rPreviousEntity, cColorAlpha);
            glTranslatef(-cXOffset, 0.0f, 0.0f);
        }

        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS);

        //--Current entity renders above.
        float cXOffset = VIRTUAL_CANVAS_X * (1.0f - cScrollPercent) * -1.0f;
        rPortrait->Draw(tRenderDim.mXCenter + cXOffset, tRenderDim.mYCenter);

        //--Offset the basics by the offset.
        glTranslatef(cXOffset, 0.0f, 0.0f);
        RenderBasics(rActiveEntity, cColorAlpha);
        glTranslatef(-cXOffset, 0.0f, 0.0f);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIStatus::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Top block.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Render]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Status");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Control strings
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIStatus::RenderRgt(float pVisAlpha)
{
    ///--[Documentation]
    //--Right block.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Get active entity.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) { AutoPieceClose(); return; }

    //--Right-side arrow.
    float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1) AdvImages.rOverlay_ArrowRgt->Draw(cArrowOscillate * 1.0f, 0.0f);

    //--Resistances. Handled by subroutine.
    RenderResistances(rActiveEntity, cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIStatus::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Bottom block.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Render]
    //--Get active entity.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) { AutoPieceClose(); return; }

    //--Equipment subroutine.
    RenderEquipment(rActiveEntity, cColorAlpha);

    //--Strings.
    float cYPos = VIRTUAL_CANVAS_Y - cxAdvMainlineFontH;
    mCharacterLftString->DrawText(            0.0f, cYPos,                          SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCharacterRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///==================================== Drawing Subroutines =======================================
void AdvUIStatus::RenderBasics(AdvCombatEntity *pEntity, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders basic character statistics like Name, HP, XP, Attack, Level, etc.
    if(!pEntity) return;

    ///--[Static Parts]
    //--Backings.
    AdvImages.rFrame_Status->Draw();
    AdvImages.rOverlay_StatusDetail->Draw();
    AdvImages.rOverlay_NameBanner->Draw();
    AdvImages.rOverlay_StatusLevelBacking->Draw();

    ///--[Name, Level]
    //--Name.
    AdvImages.rFont_Heading->DrawText(200.0f, 366.0f, 0, 1.0f, pEntity->GetDisplayName());

    //--Level.
    AdvImages.rFont_Level->DrawTextArgs(143.0f, 409.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", pEntity->GetLevel() + 1);

    ///--[Bar Fill Percentages]
    //--HP bar.
    float cHPBarPercent = pEntity->GetHealthPercent();
    if(cHPBarPercent > 0.0f)
    {
        AdvImages.rOverlay_StatusHPBarFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cHPBarPercent, 1.0f);
    }

    //--XP bar.
    int tXPToNextLevelMax = pEntity->GetXPToNextLevelMax();
    int tXPToNextLevelCur = pEntity->GetXPToNextLevel();
    int tXPFromLastLevel = tXPToNextLevelMax - tXPToNextLevelCur;
    float cXPBarPercent = 0.0f;

    //--If the XP values to the next level are -1, we're at the max level. Render that.
    //  Alternately, if there was an error and the next-level value came back zero, set it to 1.0f.
    if(tXPToNextLevelMax == -1 || tXPToNextLevelMax == 0)
    {
        AdvImages.rOverlay_StatusXPBarFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f);
    }
    //--Compute normally.
    else
    {
        cXPBarPercent = tXPFromLastLevel / (float)tXPToNextLevelMax;
    }

    //--Render.
    if(cXPBarPercent > 0.0f)
    {
        AdvImages.rOverlay_StatusXPBarFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, cXPBarPercent, 1.0f);
    }

    //--Bar frames go over the fills.
    AdvImages.rOverlay_StatusHPFrame->Draw();
    AdvImages.rOverlay_StatusXPFrame->Draw();
    AdvImages.rOverlay_StatusLevelOuter->Draw();

    ///--[Text and Symbols]
    //--Constants.
    float cTxtLft = 184.0f;
    float cTxtBarRgt = 390.0f;
    float cTxtStatRgt = 340.0f;

    //--Next Level
    AdvImages.rFont_Statistic->DrawText(cTxtLft, 439.0f, 0, 1.0f, "Next");
    AdvImages.rFont_Statistic->DrawTextArgs(cTxtBarRgt, 439.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/%i", tXPFromLastLevel, tXPToNextLevelMax);

    //--Health.
    AdvImages.rFont_Statistic->DrawText(cTxtLft, 492.0f, 0, 1.0f, "HP");
    AdvImages.rFont_Statistic->DrawTextArgs(cTxtBarRgt, 492.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/%i", pEntity->GetHealth(), pEntity->GetStatistic(STATS_HPMAX));

    //--Attack Power.
    AdvImages.rFont_Statistic->DrawText(cTxtLft, 526.0f, 0, 1.0f, "Power");
    AdvImages.rFont_Statistic->DrawTextArgs(cTxtStatRgt, 526.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_ATTACK));
    AdvImages.rStatisticIcons[0]->Draw(342.0f, 530.0f);

    //--Protection
    AdvImages.rFont_Statistic->DrawText(cTxtLft, 552.0f, 0, 1.0f, "Protection");
    AdvImages.rFont_Statistic->DrawTextArgs(cTxtStatRgt, 552.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_RESIST_PROTECTION));
    AdvImages.rStatisticIcons[1]->Draw(342.0f, 556.0f);

    //--Accuracy
    AdvImages.rFont_Statistic->DrawText(cTxtLft, 578.0f, 0, 1.0f, "Accuracy");
    AdvImages.rFont_Statistic->DrawTextArgs(cTxtStatRgt, 578.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_ACCURACY));
    AdvImages.rStatisticIcons[2]->Draw(342.0f, 582.0f);

    //--Evade
    AdvImages.rFont_Statistic->DrawText(cTxtLft, 604.0f, 0, 1.0f, "Evade");
    AdvImages.rFont_Statistic->DrawTextArgs(cTxtStatRgt, 604.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_EVADE));
    AdvImages.rStatisticIcons[3]->Draw(342.0f, 608.0f);

    //--Initiative
    AdvImages.rFont_Statistic->DrawText(cTxtLft, 630.0f, 0, 1.0f, "Initiative");
    AdvImages.rFont_Statistic->DrawTextArgs(cTxtStatRgt, 630.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_INITIATIVE));
    AdvImages.rStatisticIcons[4]->Draw(342.0f, 634.0f);
}
void AdvUIStatus::RenderEquipment(AdvCombatEntity *pEntity, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders equipment slots and entries for the given character. Basically the same as the equipment UI
    //  but non-interactive.
    if(!pEntity) return;

    ///--[Static]
    //--Frame. Detail is dynamically generated.
    AdvImages.rFrame_Equipment->Draw();
    AdvImages.rOverlay_EquipmentHeader->Draw();

    //--Header text, yellow shade.
    mColorHeading.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Heading->DrawText(694.0f, 110.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equipment");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    ///--[Dynamic]
    //--Constants.
    float cSlotH = 56.0f;

    //--Iterate across the slots and call a subroutine.
    int tSlots = pEntity->GetEquipmentSlotsTotal();
    for(int i = 0; i < tSlots; i ++)
    {
        //--Entry.
        RenderEquipmentEntry(pEntity, i, pColorAlpha);

        //--If there is another slot past this one, print a divider.
        if(i < tSlots - 1)
        {
            AdvImages.rOverlay_EquipmentDetail->Draw(0.0f, cSlotH * i);
        }
    }
}
void AdvUIStatus::RenderEquipmentEntry(AdvCombatEntity *pEntity, int pSlot, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders an equipment entry for the Status UI. Unlike the Equipment UI, no handling needs to be done
    //  for switching cases since the Status UI is non-interactive.
    if(!pEntity) return;

    //--Get and check the equipment slot.
    const char *rSlotName = pEntity->GetNameOfEquipmentSlot(pSlot);
    AdventureItem *rSlotContents = pEntity->GetEquipmentBySlotI(pSlot);
    if(!rSlotName) return;

    //--Constants.
    float cSlotX = 535.0f;
    float cSlotY = 167.0f;
    float cSlotH =  56.0f;
    float cIconX = cSlotX +  4.0f;
    float cIconY = cSlotY + 22.0f;
    float cNameX = cSlotX + 25.0f;
    float cNameY = cSlotY + 22.0f;

    //--Gem positions.
    float cGemX = 782.0f;
    float cGemY = 164.0f;

    ///--[Empty Slot]
    //--Slot is empty, darken it slightly.
    if(!rSlotContents)
    {
        mColor_DarkEquipSlot.SetAsMixerAlpha(pColorAlpha);
        AdvImages.rFont_Mainline->DrawText(cSlotX, cSlotY + (cSlotH * pSlot), 0, 1.0f, rSlotName);
        AdvImages.rFont_Mainline->DrawText(cNameX, cNameY + (cSlotH * pSlot), 0, 1.0f, "(Empty)");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
        return;
    }

    ///--[Occupied Slot]
    //--Render icon/name/gems.
    const char *rItemName = rSlotContents->GetName();
    StarBitmap *rIcon = rSlotContents->GetIconImage();

    //--Render.
    AdvImages.rFont_Mainline->DrawText(cSlotX, cSlotY + (cSlotH * pSlot), 0, 1.0f, rSlotName);
    if(rItemName) AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cNameY + (cSlotH * pSlot), cxAdvItemMaxNameLen, 0, rItemName);
    if(rIcon)     rIcon->Draw(cIconX, cIconY + (cSlotH * pSlot));

    //--Gem slots.
    RenderGemSlotsSwitch(cGemX, cGemY, rSlotContents, AdvImages.rOverlay_GemSlot);

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
}
void AdvUIStatus::RenderResistances(AdvCombatEntity *pEntity, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the protection and all resistances for the active character. Also shows how much damage those
    //  resistances reduce under normal circumstances.
    if(!pEntity) return;

    ///--[Static]
    //--Frame, detail.
    AdvImages.rFrame_Resistance->Draw();
    AdvImages.rOverlay_ResistanceDetail->Draw();
    AdvImages.rOverlay_ResistanceHeader->Draw();

    //--Header text, yellow shade.
    mColorHeading.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Heading->DrawText(1037.0f, 110.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Resistances");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    ///--[Dynamic]
    //--Icon/Value setup.
    float cIconX = 913.0f;
    float cIconY = 170.0f;
    float cIconH =  25.0f;
    float cValueX = cIconX + 50.0f;
    float cValueY = cIconY -  3.0f;
    float cReductionX = 1159.0f;
    float cReductionY = cIconY - 3.0f;

    //--Icons, resist values, reduction percentage.
    for(int i = 0; i < ADVMENU_STATUS_RESIST_ICONS_TOTAL; i ++)
    {
        //--Setup.
        int tResistValue = pEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_RESIST_START + i);

        //--Render icon.
        AdvImages.rResistanceIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Value is 1000 or higher. Render "Immune".
        if(tResistValue >= 1000)
        {
            AdvImages.rFont_Statistic->DrawText(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
        }
        //--Otherwise, render the value and the percentage reduction.
        else
        {
            //--If a bonus is applied, render the statistic in purple. If a malus is applied, red.
            int tBonusEqp = pEntity->GetStatistic(ADVCE_STATS_EQUIPMENT, STATS_RESIST_START + i);
            int tBonusAbi = pEntity->GetStatistic(ADVCE_STATS_TEMPEFFECT, STATS_RESIST_START + i);
            int tTotalBonus = tBonusEqp + tBonusAbi;
            if(tTotalBonus > 0)
            {
                mColor_ResistBonus.SetAsMixerAlpha(pColorAlpha);
            }
            else if(tTotalBonus < 0)
            {
                mColor_ResistMalus.SetAsMixerAlpha(pColorAlpha);
            }

            //--Resistance amount.
            AdvImages.rFont_Statistic->DrawTextArgs(cValueX, cValueY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistValue);

            //--Computed reduction.
            float cReduction = 1.0f - AdvCombatEntity::ComputeResistance(tResistValue);
            AdvImages.rFont_Statistic->DrawTextArgs(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%0.0f%%", cReduction * 100.0f);

            //--Clean.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
