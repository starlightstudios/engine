//Replace AdvUISample with name of UI
//Balance header
//Write a description
//Delete these instructions

///======================================== AdvUISample ===========================================
//--Description

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUISample : virtual public AdvUICommon
{
    protected:
    ///--[System]
    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;

        //--Images
        StarBitmap *rFrame_Base;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUISample();
    virtual ~AdvUISample();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


