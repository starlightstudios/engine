//--Base
#include "FlexMenu.h"

//--Classes
#include "AdventureMenu.h"
#include "FlexButton.h"
#include "StringEntry.h"
#include "VisualLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarFileSystem.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"
#include "Subdivide.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///--[Local Definitions]
#define INTRO_TICKS 120

///========================================== System ==============================================
FlexMenu::FlexMenu()
{
    ///--[ ===== Variable Initialization ====== ]
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_MENU_FLEX;

    //--[StarMenu]
    //--System
    //--Buttons

    ///--[FlexMenu]
    //--System
    mCountTreasureProgress = 0;
    mClearedSelfThisTick = false;
    mHasSortedRecently = true;

    //--Splash Screen
    mAccelerateSplashState = false;
    mSplashState = FM_SPLASH_STATE_NONE;
    mSplashStateTimer = 0;
    mSplashTimer = 0;
    memset(rSplashImg, 0, sizeof(StarBitmap *) * FM_SPLASH_FRAMES_TOTAL);

    //--Selection
    mSelectedOption = -1;
    mPreviousMouseX = -1;
    mPreviousMouseY = -1;
    mPreviousMouseZ = -1;

    //--Introduction Mode
    mIsIntroMode = false;
    mIntroTimer = 0;

    //--Loading Mode
    mIsLoadingMode = false;
    mLoadingTimer = 0;
    mLoadingCursor = 0;
    mLoadingOffset = 0;
    mLoadingShowAutosaves = false;
    mBackBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mControlsBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mScrollUp.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mScrollDn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mToggleAutosave.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mLoadingPackList = new StarLinkedList(true);
    mAutosavePackList = new StarLinkedList(true);
    mIsEnteringString = false;
    mFileStringEntering = 0;
    mStringEntryForm = StringEntry::rGenerateStringEntry();
    mStringEntryForm->SetPromptString("Enter a note for this file.");
    mStringEntryForm->SetHardcap(128);

    //--Scrollbar Click
    mIsClickingScrollbar = false;
    mScrollbarClickStartY = 0.0f;

    //--Rebinding Mode
    mIsRebindingMode = false;
    mRebindingCursor = -1;
    mIsRebindSecondary = false;
    mCurrentRebindKey = -1;
    mActiveRebindPack = 0;
    mRebindPacksTotal = 0;
    mRebindPacks = NULL;
    mRebindPackCurButton.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mRebindPackLftButton.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mRebindPackRgtButton.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mSaveBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mDefaultsBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--Header
    mHasHeader = false;
    mHeaderLinesTotal = 0;
    mHeaderLines = NULL;
    mHeaderWid = 0.0f;
    mHeaderHei = 0.0f;
    cHeaderPadding = 0.0f;

    //--Descriptions
    mLinesOccupied = 0;
    mShowDescriptions = false;
    rLastHighlightedBtn = NULL;
    rDescriptionImg = NULL;
    mDescriptionDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    mDescriptionImgDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    memset(mDescriptionLines, 0, sizeof(char) * FM_DESCRIPTION_LINES * FM_DESCRIPTION_CHARS);

    //--Rendering Constants
    mCanRender = false;
    cBorder = 4.5f;
    cButtonIndentX = 11.0f;
    cButtonIndentY = 11.0f;
    cFontSize = 1.5f;
    cStepRate = 24.0f;

    //--Rendering Variables
    rOverrideFont = NULL;

    //--Location
    mCoordinates.SetWH(VIRTUAL_CANVAS_X / 2.0f, VIRTUAL_CANVAS_Y / 2.0f, 20.0f, 20.0f);

    //--Storage
    mButtonStorage = NULL;

    //--Mod Load Order
    mModLoadOrderList = new StarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));

    ///--[Resolve Images]
    Construct();

    ///--[Resolve Controls File]
    //--Resolve the control file.
    char *tControlPath = NULL;
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    #ifdef _ALLEGRO_PROJECT_
        tControlPath = InitializeString("%s/../../Saves/AdventureControlsAL.lua", rAdventurePath);
    #elif defined _SDL_PROJECT_
        tControlPath = InitializeString("%s/../../Saves/AdventureControlsSDL.lua", rAdventurePath);
    #else
        fprintf(stderr, "Error: No primary control library, cannot boot controls!");
    #endif

    //--Run the control rebinding. May not exist.
    if(tControlPath && StarFileSystem::FileExists(tControlPath) && xNeedsToLoadControls)
    {
        xNeedsToLoadControls = false;
        LuaManager::Fetch()->ExecuteLuaFile(tControlPath);
    }

    //--Clean.
    free(tControlPath);
}
FlexMenu::~FlexMenu()
{
    //--[RootObject]
    //--System

    //--[StarMenu]
    //--System
    //--Buttons

    //--[FlexMenu]
    //--System
    //--Splash Screen
    //--Selection
    //--Introduction Mode
    //--Loading Mode
    delete mLoadingPackList;
    delete mAutosavePackList;
    delete mStringEntryForm;

    //--Scrollbar Click
    //--Rebinding Mode
    for(int i = 0; i < mRebindPacksTotal; i ++) FM_RebindGroup::DeallocateThis(&mRebindPacks[i]);
    free(mRebindPacks);

    //--Header
    for(int i = 0; i < mHeaderLinesTotal; i ++) free(mHeaderLines[i].mText);
    free(mHeaderLines);

    //--Descriptions
    //--Rendering Constants
    //--Rendering Variables
    //--Location
    //--Storage
    delete mButtonStorage;

    //--Mod Load Order
    delete mModLoadOrderList;
}
void FlexMenu::Construct()
{
    ///--[Documentation]
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Splash Screen]
    //--Optional. Should only be loaded the first time this menu is used. Not part of the conventional
    //  image set, and the assets are deleted when this object is.
    if(xShowSplash)
    {
        //--Toggle the flag off.
        xShowSplash = false;

        //--Set variables so the splash screen plays.
        mSplashState = FM_SPLASH_STATE_FADEIN;

        //--Load frames.
        char tBuf[256];
        for(int i = 0; i < FM_SPLASH_FRAMES_TOTAL; i ++)
        {
            //--Resolve.
            sprintf(tBuf, "Root/Images/GUI/SplashScreen/Logo%02i", i);
            rSplashImg[i] = (StarBitmap *)rDataLibrary->GetEntry(tBuf);

            //--If the frame came back null, unset the splash screen.
            if(!rSplashImg[i]) mSplashState = FM_SPLASH_STATE_NONE;
        }
    }

    ///--[Chapter-Complete Images]
    char tBuffer[128];
    for(int i = 0; i < LOADINGPACK_MAX_CHAPTER; i ++)
    {
        sprintf(tBuffer, "Root/Images/AdventureUI/ChapterComplete/Chapter%i", i+1);
        Images.Data.rChapterComplete[i] = (StarBitmap *)rDataLibrary->GetEntry(tBuffer);
    }

    ///--[Normal Case]
    //--Used for any Pandemonium game.
    if(!xUseDummySprites)
    {
        //--Base Images
        Images.Data.rBorderCard = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/CardBorder");
        Images.Data.rUpArrow    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/UpArrow");
        Images.Data.rDnArrow    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/DnArrow");
        Images.Data.rUIFont     = rDataLibrary->GetFont("Flex Menu Main");

        //--Verify.
        Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(StarBitmap *));
        if(!Images.mIsReady) return;
    }
    //--No-Sprite case. Used for other games.
    else
    {
        //--Base Images
        Images.Data.rBorderCard = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/CardBorder");
        Images.Data.rUpArrow    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/UpArrow");
        Images.Data.rDnArrow    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/DnArrow");
        Images.Data.rUIFont     = rDataLibrary->GetFont("Flex Menu Main");

        //--Verify.
        Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(StarBitmap *));
        if(!Images.mIsReady) return;
    }

    ///--[Sizing]
    //--Description width is fixed, the height is the size-per-line.
    mDescriptionDim.SetWH(0.0f, 0.0f, 600.0f, Images.Data.rUIFont->GetTextHeight());

    //--Button sizes.
    float cStdBtnHei = (Images.Data.rUIFont->GetTextHeight() + (AM_MAINLINE_INDENT * 2.0f));
    mBackBtn.SetWH (  0.0f,   0.0f, 73.0f, cStdBtnHei);
    mScrollUp.SetWH(935.0f, 236.0f, 26.0f, cStdBtnHei);
    mScrollDn.SetWH(935.0f, 681.0f, 26.0f, cStdBtnHei);
    mControlsBtn.Set   (VIRTUAL_CANVAS_X - 190.0f, cStdBtnHei * 0.0f, VIRTUAL_CANVAS_X, cStdBtnHei * 1.0f);
    mToggleAutosave.Set(VIRTUAL_CANVAS_X - 190.0f, cStdBtnHei * 1.0f, VIRTUAL_CANVAS_X, cStdBtnHei * 2.0f);
}

///--[Public Statics]
//--Shows the splash screen on startup.
bool FlexMenu::xShowSplash = true;

//--Uses dummy sprites instead of the actual loading images. Used for games that don't use loading images.
bool FlexMenu::xUseDummySprites = false;

//--Uses the "Classic" UI, making the game look like it's running in Git. White background, black borders.
//  Boring, but functional. Can be modified through the OptionsManager.
bool FlexMenu::xUseClassicUI = false;

//--This value gets set to the global tick timer whenever a selection action occurs. Highlight SFX will not
//  play for a short time afterwards.
int FlexMenu::xNoHighlightTimer = 0;

//--Needs to load controls. Set to true at game start, set to false when the controls are loaded.
//  Should only ever execute once.
bool FlexMenu::xNeedsToLoadControls = true;

//--Marks duplicate object names when performing treasure scans.
bool FlexMenu::xObjectScanDebug = false;

//--File to output text to when performing treasure scans.
FILE *FlexMenu::xfDebugFileOut = NULL;

///===================================== Property Queries =========================================
bool FlexMenu::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_MENU_ROOT) return true;
    return (pType == mType);
}
StarFont *FlexMenu::ResolveFont()
{
    StarFont *rUseFont = rOverrideFont;
    if(!rUseFont) rUseFont = DataLibrary::Fetch()->GetFont("Flex Menu Main");
    return rUseFont;
}
bool FlexMenu::DoesButtonExist(const char *pButtonName)
{
    return (mButtonList->GetElementByName(pButtonName) != NULL);
}

///======================================== Manipulators ==========================================
void FlexMenu::Clear()
{
    //--Wipe the header.
    ResetHeader(0);

    //--Flags.
    mClearedSelfThisTick = true;
    mCanRender = false;
    mHasSortedRecently = false;

    //--Description
    mShowDescriptions = false;
    rLastHighlightedBtn = NULL;
    rDescriptionImg = NULL;
    memset(mDescriptionLines, 0, sizeof(char) * FM_DESCRIPTION_LINES * FM_DESCRIPTION_CHARS);

    //--Location
    mCoordinates.SetWH(VIRTUAL_CANVAS_X / 2.0f, VIRTUAL_CANVAS_Y / 2.0f, 20.0f, 20.0f);

    //--Storage list.
    mButtonStorage = new StarLinkedList(true);
}
void FlexMenu::SetDescriptionFlag(bool pFlag)
{
    mShowDescriptions = pFlag;
    rLastHighlightedBtn = NULL;
}
void FlexMenu::ResetHeader(int pHeaderLines)
{
    //--Wipe the header.
    mCanRender = true;
    mHasSortedRecently = false;
    mHeaderWid = 0.0f;
    mHeaderHei = 0.0f;
    cHeaderPadding = 0.0f;
    if(pHeaderLines < 0) pHeaderLines = 0;

    //--Deallocate.
    for(int i = 0; i < mHeaderLinesTotal; i ++)
        free(mHeaderLines[i].mText);
    free(mHeaderLines);
    mHeaderLinesTotal = 0;
    mHeaderLines = NULL;

    //--Flag.
    mHasHeader = (pHeaderLines > 0);
    if(pHeaderLines == 0) return;

    //--Allocate space.
    mHeaderLinesTotal = pHeaderLines;
    SetMemoryData(__FILE__, __LINE__);
    mHeaderLines = (HeaderPack *)starmemoryalloc(sizeof(HeaderPack) * mHeaderLinesTotal);

    //--Set to default.
    for(int i = 0; i < mHeaderLinesTotal; i ++)
    {
        mHeaderLines[i].mText = NULL;
        mHeaderLines[i].cFontSize = cFontSize;
    }
}
void FlexMenu::SetHeaderLine(int pIndex, const char *pString, float pFontSize)
{
    //--Changes the line to match what is expected. Also allows modification of the font size.
    //  Pass -1.0f to set the font size to the default.
    //--Font size is in pixels. The value provided is divided by 60 before being used.
    if(pIndex < 0 || pIndex >= mHeaderLinesTotal) return;
    ResetString(mHeaderLines[pIndex].mText, pString);
    if(pFontSize <= 0.0f) pFontSize = cFontSize;

    //--Get expected length.
    StarFont *rFont = ResolveFont();
    if(!rFont) return;

    //--Compute size.
    mHeaderLines[pIndex].cFontSize = pFontSize;

    //--Width. Whichever is longest.
    float tLineWid = rFont->GetTextWidth(mHeaderLines[pIndex].mText) * mHeaderLines[pIndex].cFontSize;
    if(tLineWid > mHeaderWid) mHeaderWid = tLineWid;

    //--Height. Lowest line sets it.
    float tLineHei = (float)(pIndex+1) * (mHeaderLines[pIndex].cFontSize * rFont->GetTextHeight());
    if(tLineHei > mHeaderHei) mHeaderHei = tLineHei;

    //--Padding.
    if(mHeaderHei > 0.0f) cHeaderPadding = 20.0f;
}
void FlexMenu::RegisterButton(StarButton *pButton)
{
    //--Check.
    if(!pButton) return;

    //--Buttons will register to a different list during a clear action.
    if(mClearedSelfThisTick)
    {
        mButtonStorage->AddElement(pButton->GetName(), pButton, StarButton::DeleteThis);
    }
    //--Same as the base version.
    else
    {
        StarMenu::RegisterButton(pButton);
    }

    //--Always trip this flag.
    mHasSortedRecently = false;
}
void FlexMenu::ActivateIntroMode()
{
    //--Flags.
    mIsIntroMode = true;
    mIntroTimer = -60;

    //--This global flag prevents intro mode from activating. It's just to make testing faster.
    if(Global::Shared()->gFastTitle) mIsIntroMode = false;
}

///======================================== Core Methods ==========================================
void FlexMenu::RefreshDescriptionFrom(FlexButton *pButton)
{
    //--Resets the description, or clears it if NULL is passed in. Does nothing if description mode is off.
    if(!mShowDescriptions) return;

    //--Clear.
    mLinesOccupied = 0;
    rLastHighlightedBtn = pButton;
    memset(mDescriptionLines, 0, sizeof(char) * FM_DESCRIPTION_LINES * FM_DESCRIPTION_CHARS);
    if(!pButton) return;

    //--Image.
    rDescriptionImg = pButton->GetDescriptionImage();

    //--Subdivide the description lines.
    const char *rDescription = pButton->GetDescription();
    if(!rDescription) return;

    //--Setup.
    float cLength = mDescriptionDim.GetWidth() - 64.0f;
    int tDescriptionLen = (int)strlen(rDescription);
    int tCursor = 0;
    int tRunningCursor = 0;
    int tCurrentLine = 0;

    //--Cut up.
    while(tRunningCursor < tDescriptionLen)
    {
        //--Get the string.
        char *tString = Subdivide::SubdivideString(tCursor, &rDescription[tRunningCursor], FM_DESCRIPTION_CHARS-1, cLength, Images.Data.rUIFont, 1.0f);

        //--Copy the line over.
        strcpy(mDescriptionLines[tCurrentLine], tString);

        //--Advance the cursor so the next line gets put up.
        tRunningCursor += tCursor;
        if(tCurrentLine < FM_DESCRIPTION_LINES - 1) tCurrentLine ++;
        mLinesOccupied = tCurrentLine;

        //--Clean.
        free(tString);
    }
}

///==================================== Private Core Methods ======================================
void FlexMenu::AutoSortButtons()
{
    //--Sorts the buttons and resizes the menu to accomodate them.
    if(!Images.mIsReady) return;
    mHasSortedRecently = true;

    //--First, get the buttons in order by priority. This is done automatically.
    mButtonList->SortListUsing(FlexMenu::CompareButtonPriorities);

    //--Optional image stuff.
    int tLargestLineCount = 0;
    float tTallestImage = 0.0f;

    //--Get the sizes we'll need.
    float tWidestButton = mHeaderWid;
    float tExpectedW = tWidestButton;
    float tExpectedH = mHeaderHei + cHeaderPadding + (cButtonIndentY * 2.0f) + ((float)mButtonList->GetListSize() * cStepRate) + (6.0f);
    FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--If the button has a description, activate descriptions mode.
        if(rButton->GetDescription())
        {
            //--Flag.
            mShowDescriptions = true;

            //--Count how many lines there are. Keep track of the highest count.
            RefreshDescriptionFrom(rButton);
            if(mLinesOccupied > tLargestLineCount) tLargestLineCount = mLinesOccupied;
        }

        //--If the button has an image, check its height.
        StarBitmap *rButtonImg = rButton->GetDescriptionImage();
        if(rButtonImg)
        {
            if(rButtonImg->GetTrueHeight() > tTallestImage) tTallestImage = rButtonImg->GetTrueHeight();
        }

        //--Sizing.
        rButton->SetFontSize(cFontSize);
        float tButtonW = rButton->GetTextWidth();
        if(tWidestButton < tButtonW) tWidestButton = tButtonW;

        //--Next button.
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }

    //--Clear description info.
    rLastHighlightedBtn = NULL;
    memset(mDescriptionLines, 0, sizeof(char) * FM_DESCRIPTION_LINES * FM_DESCRIPTION_CHARS);

    //--Pad out the width so we can have borders.
    tExpectedW = tWidestButton + (cButtonIndentX * 2.0f) + (10.0f);

    //--Now reposition everything.
    float tHighestBtn = 1000.0f;
    int tCount = 0;
    rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--Position.
        float tYPosition = cButtonIndentY + ((float)tCount * cStepRate) + mHeaderHei + cHeaderPadding;
        rButton->SetDimensionsWH(cButtonIndentX, tYPosition, tWidestButton, cStepRate);
        if(tYPosition < tHighestBtn) tHighestBtn = tYPosition;

        //--Next button.
        tCount ++;
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }

    //--Check: If any headers are wide enough, use them as the expected width. This is only used in the new UI format.
    if(!xUseClassicUI)
    {
        for(int i = 0; i < mHeaderLinesTotal; i ++)
        {
            float tHeaderLen = Images.Data.rUIFont->GetTextWidth(mHeaderLines[i].mText) * (mHeaderLines[i].cFontSize) * 1.07f;
            if(tHeaderLen > tExpectedW) tExpectedW = tHeaderLen;
        }
    }

    //--The menu becomes considerably wider in descriptions mode.
    if(mShowDescriptions)
    {
        //--Move the description dim over.
        mDescriptionDim.mLft = tWidestButton + 64.0f;
        mDescriptionDim.mRgt = mDescriptionDim.mLft + 500.0f;

        //--If there are description images, use their height.
        if(tExpectedH < tTallestImage) tExpectedH = tTallestImage;

        //--Set the top of the description based on how many lines it needed.
        mDescriptionDim.mTop = tHighestBtn;//tExpectedH - (mDescriptionDim.GetHeight() * tLargestLineCount);
        mDescriptionDim.mBot = mDescriptionDim.mTop + Images.Data.rUIFont->GetTextHeight();

        //--Recheck width.
        tExpectedW = tExpectedW + mDescriptionDim.GetWidth();
    }

    //--Resize the menu itself.
    mCanRender = true;
    float tX = (VIRTUAL_CANVAS_X * 0.5f) - (tExpectedW * 0.5f);
    float tY = (VIRTUAL_CANVAS_Y * 0.5f) - (tExpectedH * 0.5f);
    mCoordinates.SetWH(tX, tY, tExpectedW, tExpectedH);
}
int FlexMenu::CompareButtonPriorities(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used to sort the buttons. Buttons have a Priority value.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Get the data pointer.
    StarButton *rPackA = (StarButton *)((*rEntryA)->rData);
    StarButton *rPackB = (StarButton *)((*rEntryB)->rData);
    return rPackA->GetPriority() - rPackB->GetPriority();
}
bool FlexMenu::HandleCheatCode(const char *pPressedKey, int &sProgressVar, int pMaxLength, const char *pCheatCode)
{
    //--Worker function, handles typing cheat codes. Returns true if the code was entered this pass,
    //  false if not. The sProgressVar will be updated if a correct key is pressed.
    if(!pPressedKey || !pCheatCode) return false;

    //--Downshift it if it's a letter. Numbers and other characters do not need modification.
    char tCharacter = pPressedKey[0];
    if(tCharacter >= 'A' && tCharacter <= 'Z')
    {
        tCharacter = tCharacter + 'a' - 'A';
    }

    //--Now check if it matches a key we need.
    if(tCharacter == pCheatCode[sProgressVar])
    {
        //--Increment.
        sProgressVar ++;

        //--Ending case.
        if(sProgressVar >= pMaxLength-1)
        {
            AudioManager::Fetch()->PlaySound("World|GetCatalyst");
            return true;
        }
    }

    //--Cheat code not entered this tick.
    return false;
}
#include "AdventureLevel.h"
#include "AdventureInventory.h"
#include "StarLumpManager.h"
#include "StarFileSystem.h"
#include "VirtualFile.h"
void FlexMenu::CountTreasures()
{
    ///--[Documentation and Setup]
    //--This helpful function checks every map in the game and spits out information related to the
    //  treasure chests in them. Useful for diagnosing missing catalysts or making sure items can
    //  be found.
    //--Requires a non-trivial amount of time to run. Dumps results to a text file on the hard drive.

    //--Output file.
    FILE *fOutfile = fopen("Treasure Results.txt", "w");
    if(!fOutfile) return;

    //--Debug flag.
    AdventureLevel::xIsScanningTreasures = true;

    //--Setup.
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    memset(AdventureLevel::xCountCatalyst, 0, sizeof(int) * CATALYST_TOTAL);

    //--Create and parse.
    //const char *rDebugAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sDebugAdventurePath");
    char *tPathBuffer = InitializeString("%s/Maps/", "../adventure/Games/AdventureMode/");
    StarFileSystem *nDirectory = new StarFileSystem();
    nDirectory->mIsRecursive = true;
    nDirectory->ScanDirectory(tPathBuffer);

    //--Automatically mark duplicate objects.
    xObjectScanDebug = true;
    xfDebugFileOut = fOutfile;

    //--Printing.
    fprintf(fOutfile, "[Map Data Listing]\n");

    ///--[Map Parser]
    //--For each map, parse it. This may take a while.
    for(int i = 0; i < nDirectory->GetTotalEntries(); i ++)
    {
        ///--[Setup]
        //--Get info.
        FileInfo *rFileInfo = nDirectory->GetEntry(i);

        //--If the MapData.slf exists:
        char tBuffer[512];
        sprintf(tBuffer, "%sMapData.slf", rFileInfo->mPath);

        //--Verify the file.
        FILE *fCheckFile = fopen(tBuffer, "rb");
        if(!fCheckFile) continue;

        //--Clean.
        fclose(fCheckFile);

        ///--[Level Name]
        //--Resolve level name from its path.
        int tStartLetter = 0;
        char tCheckBuffer[128];
        for(int p = (int)strlen(rFileInfo->mPath) - 2; p >= 0; p --)
        {
            if(rFileInfo->mPath[p] == '/' || rFileInfo->mPath[p] == '\\')
            {
                tStartLetter = p+1;
                break;
            }
        }
        for(int p = tStartLetter; p < (int)strlen(rFileInfo->mPath) - 1; p ++)
        {
            tCheckBuffer[p-tStartLetter+0] = rFileInfo->mPath[p];
            tCheckBuffer[p-tStartLetter+1] = '\0';
        }

        //--Print the resolved name.
        fprintf(fOutfile, "%s\n", tCheckBuffer);

        ///--[Open and Scan]
        //--Open this as an AdventureLevel.
        AdventureLevel::xCreationDebug = true;
        AdventureLevel *tLevel = new AdventureLevel();
        bool tOldFlag = VirtualFile::xUseRAMLoading;
        VirtualFile::xUseRAMLoading = true;
        rSLM->Open(tBuffer);
        VirtualFile::xUseRAMLoading = tOldFlag;

        //--Storage.
        int tOldHealth = AdventureLevel::xCountCatalyst[CATALYST_HEALTH];
        int tOldAttack = AdventureLevel::xCountCatalyst[CATALYST_ATTACK];
        int tOldInitiative = AdventureLevel::xCountCatalyst[CATALYST_INITIATIVE];
        int tOldDodge = AdventureLevel::xCountCatalyst[CATALYST_DODGE];
        int tOldAccuracy = AdventureLevel::xCountCatalyst[CATALYST_ACCURACY];
        int tOldSkill = AdventureLevel::xCountCatalyst[CATALYST_SKILL];

        //--Instruction.
        tLevel->ParseFile(rSLM);
        tLevel->SetName(tCheckBuffer);
        AdventureLevel::xCreationDebug = false;

        //--Debug.
        if(tOldHealth < AdventureLevel::xCountCatalyst[CATALYST_HEALTH])
        {
            fprintf(fOutfile, "  Contains %i health catalysts.\n", AdventureLevel::xCountCatalyst[CATALYST_HEALTH] - tOldHealth);
        }
        if(tOldAttack < AdventureLevel::xCountCatalyst[CATALYST_ATTACK])
        {
            fprintf(fOutfile, "  Contains %i attack catalysts.\n", AdventureLevel::xCountCatalyst[CATALYST_ATTACK] - tOldAttack);
        }
        if(tOldInitiative < AdventureLevel::xCountCatalyst[CATALYST_INITIATIVE])
        {
            fprintf(fOutfile, "  Contains %i initiative catalysts.\n", AdventureLevel::xCountCatalyst[CATALYST_INITIATIVE] - tOldInitiative);
        }
        if(tOldDodge < AdventureLevel::xCountCatalyst[CATALYST_DODGE])
        {
            fprintf(fOutfile, "  Contains %i dodge catalysts.\n", AdventureLevel::xCountCatalyst[CATALYST_DODGE] - tOldDodge);
        }
        if(tOldAccuracy < AdventureLevel::xCountCatalyst[CATALYST_ACCURACY])
        {
            fprintf(fOutfile, "  Contains %i accuracy catalysts.\n", AdventureLevel::xCountCatalyst[CATALYST_ACCURACY] - tOldAccuracy);
        }
        if(tOldSkill < AdventureLevel::xCountCatalyst[CATALYST_SKILL])
        {
            fprintf(fOutfile, " Contains %i skill catalysts.\n", AdventureLevel::xCountCatalyst[CATALYST_SKILL] - tOldSkill);
        }

        //--Finish up.
        rSLM->Close();
        delete tLevel;
    }

    //--Print:
    fprintf(fOutfile, "\n[Catalyst Counts]\n");
    fprintf(fOutfile, " Health %i\n", AdventureLevel::xCountCatalyst[CATALYST_HEALTH]);
    fprintf(fOutfile, " Attack %i\n", AdventureLevel::xCountCatalyst[CATALYST_ATTACK]);
    fprintf(fOutfile, " Initiative %i\n", AdventureLevel::xCountCatalyst[CATALYST_INITIATIVE]);
    fprintf(fOutfile, " Dodge %i\n", AdventureLevel::xCountCatalyst[CATALYST_DODGE]);
    fprintf(fOutfile, " Accuracy %i\n", AdventureLevel::xCountCatalyst[CATALYST_ACCURACY]);
    fprintf(fOutfile, " Skill %i\n", AdventureLevel::xCountCatalyst[CATALYST_SKILL]);

    //--Clean.
    delete nDirectory;
    fclose(fOutfile);
    xObjectScanDebug = false;
    xfDebugFileOut = NULL;
    free(tPathBuffer);
    AdventureLevel::xIsScanningTreasures = false;
}

///=========================================== Update =============================================
void FlexMenu::Update()
{
    ///--[Routing]
    //--If in Loading mode, do that instead.
    if(mSplashState > FM_SPLASH_STATE_NONE) { UpdateSplashMode();    return; }
    if(mIsRebindingMode)                    { UpdateRebindingMode(); return; }
    if(mIsLoadingMode)                      { UpdateLoadingMode();   return; }

    ///--[Timers]
    //--Update called when this menu is atop the stack. First, sort the buttons if we need to.
    mClearedSelfThisTick = false;
    if(!mHasSortedRecently)
    {
        AutoSortButtons();
    }

    //--Intro mode: Run the timer.
    if(mIsIntroMode)
    {
        mIntroTimer ++;
        MapManager::xCandidateAlpha = mIntroTimer / (float)INTRO_TICKS;
        if(mIntroTimer >= INTRO_TICKS || ControlManager::Fetch()->IsAnyKeyPressed())
        {
            mIsIntroMode = false;
            MapManager::xCandidateAlpha = 1.0f;
        }
        return;
    }

    ///--[Cheat Code Handlers]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get codes.
    int tKeyboard, tMouse, tJoy;
    rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoy);
    const char *rPressedKeyName = rControlManager->GetNameOfKeyIndex(tKeyboard);

    //--Check progress on the cheat code for counting treasures.
    if(HandleCheatCode(rPressedKeyName, mCountTreasureProgress, COUNT_TREASURES_LEN, COUNT_TREASURES_WORD))
    {
        mCountTreasureProgress = 0;
        CountTreasures();
    }

    ///--[Mouse-Over]
    //--Get mouse position.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Reposition by local coordinates.
    tMouseX = tMouseX - (int)mCoordinates.mLft;
    tMouseY = tMouseY - (int)mCoordinates.mTop;

    //--Run an update for all buttons. Only buttons using slider mode actually bother.
    FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        rButton->Update(tMouseX, tMouseY);
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }

    //--If the mouse position changed, run an update for all buttons.
    if(tMouseX != mPreviousMouseX || tMouseY != mPreviousMouseY)
    {
        //--Button highlighting. Query each button, set highlighted for the FIRST one found. Also
        //  reset all highlighting cases.
        int i = 0;
        bool tCanHighlight = true;
        rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Reset.
            bool tPreviousHighlight = rButton->IsHighlighted();
            rButton->SetHighlight(false);

            //--If allowed, check for highlighting.
            if(tCanHighlight && rButton->IsMouseOverMe(tMouseX, tMouseY))
            {
                //--Flags.
                rButton->SetHighlight(true);
                tCanHighlight = false;
                mSelectedOption = i;

                //--Refresh the description if this button was highlighted for the first time.
                if(rLastHighlightedBtn != rButton) RefreshDescriptionFrom(rButton);

                //--SFX.
                if(!tPreviousHighlight && (int)Global::Shared()->gTicksElapsed >= xNoHighlightTimer) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--Next.
            i ++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }

        //--Store the mouse positions.
        mPreviousMouseX = tMouseX;
        mPreviousMouseY = tMouseY;
    }

    ///--[Highlight Changing]
    //--Decrement selection.
    if(rControlManager->IsFirstPress("Up") || rControlManager->IsFirstPress("PageUp"))
    {
        //--Set, clamp.
        mSelectedOption --;
        if(mSelectedOption < 0) mSelectedOption = 0;

        //--Button highlighting. All buttons are flagged as unhighlighted except the selected one.
        int i = 0;
        FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Set.
            rButton->SetHighlight(mSelectedOption == i);

            //--Next.
            i ++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Increment selection.
    if(rControlManager->IsFirstPress("Down") || rControlManager->IsFirstPress("PageDn"))
    {
        //--Set, clamp.
        mSelectedOption ++;
        if(mSelectedOption >= mButtonList->GetListSize()) mSelectedOption = mButtonList->GetListSize() - 1;
        if(mSelectedOption < 0) mSelectedOption = 0;

        //--Button highlighting. All buttons are flagged as unhighlighted except the selected one.
        int i = 0;
        FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Set.
            rButton->SetHighlight(mSelectedOption == i);

            //--Next.
            i ++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Pressing Enter will activate the selected button, if it exists.
    if(rControlManager->IsFirstPress("Enter"))
    {
        FlexButton *rSelectedButton = (FlexButton *)mButtonList->GetElementBySlot(mSelectedOption);
        if(rSelectedButton) rSelectedButton->Execute();
        mSelectedOption = -1;
    }

    ///--[Left-Click]
    //--Left-clicking case. Only the first button found that matches is executed.
    if(rControlManager->IsFirstPress("MouseLft") && !mClearedSelfThisTick)
    {
        //--Loop.
        FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            if(rButton->IsMouseOverMe(tMouseX, tMouseY))
            {
                xNoHighlightTimer = Global::Shared()->gTicksElapsed + FM_SILENCE_TICKS;
                AudioManager::Fetch()->PlaySound("Menu|Select");
                rButton->Execute();
                mButtonList->PopIterator();
                break;
            }

            //--Next.
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }
    }
    ///--[Left-Release]
    else if(rControlManager->IsFirstRelease("MouseLft"))
    {
        mIsClickingScrollbar = false;
    }

    ///--[Closing]
    //--Close.
    if(rControlManager->IsFirstPress("Escape"))
    {
        //SetClosingFlag(true);
    }

    ///--[Cleanup]
    //--If a clearing action occurred sometime during this tick, swap out the button list.
    if(mClearedSelfThisTick)
    {
        delete mButtonList;
        mButtonList = mButtonStorage;
        mButtonStorage = NULL;
    }
}
void FlexMenu::UpdateInactive()
{
    //--Update called when this menu is not atop the stack.
    FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        rButton->SetHighlight(false);
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void FlexMenu::Render()
{
    ///--[Basic]
    //--Make sure the images resolved correctly.
    if(!Images.mIsReady) return;

    //--Render the version number in the top left.
    GLOBAL *rGlobal = Global::Shared();
    if(rGlobal->gBitmapFont)
    {
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, 1.0f, rGlobal->gVersionString);
    }

    //--If in Loading/Rebinding mode, do that instead.
    if(mSplashState > FM_SPLASH_STATE_NONE) { RenderSplashMode();    return; }
    if(mIsRebindingMode)                    { RenderRebindingMode(); return; }
    if(mIsLoadingMode)                      { RenderLoadingMode();   return; }

    //--Sort if needed.
    if(!mHasSortedRecently)
    {
        AutoSortButtons();
    }

    //--Translate to menu's position.
    if(!mCanRender) { return; }
    glTranslatef(mCoordinates.mLft, mCoordinates.mTop, 0.0f);

    //--Mixer.
    float tAlpha = 1.0f;
    if(mIsIntroMode) tAlpha = mIntroTimer / (float)INTRO_TICKS;

    ///--[Classic UI]
    //--Renders without using bitmap cards. White background, black borders, TTF font.
    if(xUseClassicUI)
    {
        //--Outer square (border).
        float tOtLft = 0.0f;
        float tOtTop = 0.0f;
        float tOtRgt = mCoordinates.mRgt - mCoordinates.mLft;
        float tOtBot = mCoordinates.mBot - mCoordinates.mTop;

        //--Inner square.
        float tInLft = cBorder;
        float tInTop = cBorder;
        float tInRgt = mCoordinates.mRgt - mCoordinates.mLft - cBorder;
        float tInBot = mCoordinates.mBot - mCoordinates.mTop - cBorder;

        //--Render a background for the menu.
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glColor4f(0.7f, 0.7f, 0.7f, tAlpha);
            glVertex2f(tOtLft, tOtTop);
            glVertex2f(tOtRgt, tOtTop);
            glVertex2f(tOtRgt, tOtBot);
            glVertex2f(tOtLft, tOtBot);

            glColor4f(1.0f, 1.0f, 1.0f, tAlpha);
            glVertex2f(tInLft, tInTop);
            glVertex2f(tInRgt, tInTop);
            glVertex2f(tInRgt, tInBot);
            glVertex2f(tInLft, tInBot);
            glColor4f(1.0f, 1.0f, 1.0f, tAlpha);
        glEnd();
        glEnable(GL_TEXTURE_2D);

        //--Render the header (if applicable).
        float tRunningOffset = cButtonIndentY;
        StarFont *rUseFont = ResolveFont();
        if(mHasHeader && rUseFont && mHeaderLines)
        {
            glColor4f(0.0f, 0.0f, 0.0f, tAlpha);
            for(int i = 0; i < mHeaderLinesTotal; i ++)
            {
                //--Skip lines that are NULL.
                if(!mHeaderLines[i].mText) continue;

                //--Move the cursor down by the font size.
                tRunningOffset = tRunningOffset + (mHeaderLines[i].cFontSize * rUseFont->GetTextHeight());

                //--Render.
                rUseFont->DrawText(cButtonIndentX, tRunningOffset, 0, mHeaderLines[i].cFontSize, mHeaderLines[i].mText);
            }
            glColor4f(1.0f, 1.0f, 1.0f, tAlpha);
        }
    }
    ///--[Updated UI]
    else
    {
        //--Bitmap fonts are always white.
        glColor4f(1.0f, 1.0f, 1.0f, tAlpha);

        //--Outer square (border).
        float tOtLft = 0.0f;
        float tOtTop = 0.0f;
        float tOtRgt = mCoordinates.mRgt - mCoordinates.mLft;
        float tOtBot = mCoordinates.mBot - mCoordinates.mTop;

        //--Render the border card.
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, tOtLft, tOtTop, tOtRgt, tOtBot, 0x01FF, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.85f * tAlpha));
        glColor4f(1.0f, 1.0f, 1.0f, tAlpha);

        //--Render the header (if applicable).
        float tRunningOffset = cButtonIndentY;
        if(mHasHeader && mHeaderLines)
        {
            for(int i = 0; i < mHeaderLinesTotal; i ++)
            {
                //--Skip lines that are NULL.
                if(!mHeaderLines[i].mText) continue;

                //--Render.
                Images.Data.rUIFont->DrawText(cButtonIndentX, tRunningOffset, 0, mHeaderLines[i].cFontSize, mHeaderLines[i].mText);

                //--Move the cursor down by the font size.
                tRunningOffset = tRunningOffset + (mHeaderLines[i].cFontSize * Images.Data.rUIFont->GetTextHeight());
            }
        }
    }

    ///--[Buttons]
    //--Render the buttons, which will also have an internal check for class/updated UI.
    FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        glColor4f(1.0f, 1.0f, 1.0f, tAlpha);
        rButton->Render();
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }

    ///--[Descriptions]
    //--Setup.
    float tCursorStep = Images.Data.rUIFont->GetTextHeight();
    float tYCursor = mDescriptionDim.mTop;

    //--Description Image.
    if(rDescriptionImg) rDescriptionImg->Draw(mDescriptionImgDim.mLft, mDescriptionImgDim.mTop);

    //--Render.
    for(int i = 0; i < FM_DESCRIPTION_LINES; i ++)
    {
        if(mDescriptionLines[i][0] != '\0') Images.Data.rUIFont->DrawText(mDescriptionDim.mLft, tYCursor, 0, 1.0f, mDescriptionLines[i]);
        tYCursor = tYCursor + tCursorStep;
    }

    ///--[Finish Up]
    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
    glTranslatef(-mCoordinates.mLft, -mCoordinates.mTop, 0.0f);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

///====================================== Pointer Routing =========================================
StarLinkedList *FlexMenu::GetButtonList()
{
    return mButtonList;
}
StarButton *FlexMenu::GetButtonI(int pIndex)
{
    return (StarButton *)mButtonList->GetElementBySlot(pIndex);
}
StarButton *FlexMenu::GetButtonS(const char *pName)
{
    return (StarButton *)mButtonList->GetElementByName(pName);
}

///===================================== Static Functions =========================================
FlexMenu *FlexMenu::GenerateFlexMenu()
{
    //--Called via a function pointer when the program's main menu is being created, returns a new
    //  FlexMenu class. This can be swapped out with another function to change the menu type.
    return new FlexMenu();
}

///======================================== Lua Hooking ===========================================
void FlexMenu::HookToLuaState(lua_State *pLuaState)
{
    /* FlexMenu_Open()
       Manually opens the FlexMenu, pushing a new one atop the activity stack. This does not need
       to be called for system menus, as the C++ Code does this. This only needs to be used if
       Lua is opening a menu on its own. Remember to pop the Activity Stack when you are done. */
    lua_register(pLuaState, "FlexMenu_Open", &Hook_FlexMenu_Open);

    /* FlexMenu_FlagClose()
       Flags the FlexMenu to close itself the next chance it gets. */
    lua_register(pLuaState, "FlexMenu_FlagClose", &Hook_FlexMenu_FlagClose);

    /* FlexMenu_Clear()
       Clears the FlexMenu, including header and buttons. */
    lua_register(pLuaState, "FlexMenu_Clear", &Hook_FlexMenu_Clear);

    /* FlexMenu_RegisterButton(sButtonName)
       Creates and pushes a button of the FlexButton type with the given name. The name is what
       the button displays if it's in text mode. Be sure to pop it when you're done. */
    lua_register(pLuaState, "FlexMenu_RegisterButton", &Hook_FlexMenu_RegisterButton);

    /* FlexMenu_DoesButtonExist(sButtonName)
       Returns true if the given button exists, false if not.*/
    lua_register(pLuaState, "FlexMenu_DoesButtonExist", &Hook_FlexMenu_DoesButtonExist);

    /* FlexMenu_PushButton(sButtonName)
       Pushes the requested button atop the Activity Stack, or NULL on error. */
    lua_register(pLuaState, "FlexMenu_PushButton", &Hook_FlexMenu_PushButton);

    /* FlexMenu_SetHeaderSize(iTotalLines)
       Sets how many lines the header expects to have. Can be 0, to disable the header. Will
       reposition any buttons. Also implicitly clears the header off if any existed. */
    lua_register(pLuaState, "FlexMenu_SetHeaderSize", &Hook_FlexMenu_SetHeaderSize);

    /* FlexMenu_SetHeaderLine(iIndex, sString)
       FlexMenu_SetHeaderLine(iIndex, sString, fFontSize)
       Sets the string in the given header slot. Auto-fails for out of range. */
    lua_register(pLuaState, "FlexMenu_SetHeaderLine", &Hook_FlexMenu_SetHeaderLine);

    /* FlexMenu_ActivateIntro()
       Activates the intro, showing CGs and causing the menu to fade in. Only used at program startup. */
    lua_register(pLuaState, "FlexMenu_ActivateIntro", &Hook_FlexMenu_ActivateIntro);

    /* FlexMenu_ActivateLoading()
       Activates loading mode. Adventure Mode's path will be used to locate saves. */
    lua_register(pLuaState, "FlexMenu_ActivateLoading", &Hook_FlexMenu_ActivateLoading);

    /* FlexMenu_SetDummySpriteFlag(bUseDummySprites)
       Orders the main menu to use dummy sprites. Used for non-Pandemonium games. */
    lua_register(pLuaState, "FlexMenu_SetDummySpriteFlag", &Hook_FlexMenu_SetDummySpriteFlag);

    /* FlexMenu_GetProperty("Show Splash") (1 Boolean) (Static)
       Returns the requested variable. */
    lua_register(pLuaState, "FlexMenu_GetProperty", &Hook_FlexMenu_GetProperty);

    /* FlexMenu_SetProperty("Dummy")
       Sets the requested variable. */
    lua_register(pLuaState, "FlexMenu_SetProperty", &Hook_FlexMenu_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
#include "MapManager.h"
int Hook_FlexMenu_Open(lua_State *L)
{
    //FlexMenu_Open()
    FlexMenu *nNewMenu = new FlexMenu();
    DataLibrary::Fetch()->PushActiveEntity(nNewMenu);
    MapManager::Fetch()->PushMenuStack(nNewMenu);

    return 0;
}
int Hook_FlexMenu_FlagClose(lua_State *L)
{
    //FlexMenu_FlagClose()

    //--Can legally be a StarMenu.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_Clear", L);
    StarMenu *rMenu = (StarMenu *)rDataLibrary->rActiveObject;

    //--Set.
    rMenu->SetClosingFlag(true);
    return 0;
}
int Hook_FlexMenu_Clear(lua_State *L)
{
    //FlexMenu_Clear()

    //--Can legally be a StarMenu.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_Clear", L);
    StarMenu *rMenu = (StarMenu *)rDataLibrary->rActiveObject;

    //--Set.
    rMenu->Clear();

    return 0;
}
int Hook_FlexMenu_RegisterButton(lua_State *L)
{
    //FlexMenu_RegisterButton(sButtonName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        rDataLibrary->PushActiveEntity();
        return LuaArgError("FlexMenu_RegisterButton");
    }

    //--Activity Checking
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX))
    {
        rDataLibrary->PushActiveEntity();
        return LuaTypeError("FlexMenu_RegisterButton", L);
    }

    //--Cast.
    StarMenu *rMenu = (StarMenu *)rDataLibrary->rActiveObject;

    //--Create and push.
    FlexButton *nButton = new FlexButton();
    nButton->SetName(lua_tostring(L, 1));
    rMenu->RegisterButton(nButton);

    //--Push.
    rDataLibrary->PushActiveEntity(nButton);

    return 0;
}
int Hook_FlexMenu_DoesButtonExist(lua_State *L)
{
    //FlexMenu_DoesButtonExist(sButtonName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FlexMenu_DoesButtonExist");

    //--Activity Checking
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX))
    {
        return LuaTypeError("FlexMenu_DoesButtonExist", L);
    }

    //--Cast.
    FlexMenu *rMenu = (FlexMenu *)rDataLibrary->rActiveObject;

    //--Retrieve the button and push it.
    lua_pushboolean(L, rMenu->DoesButtonExist(lua_tostring(L, 1)));
    return 1;
}
int Hook_FlexMenu_PushButton(lua_State *L)
{
    //FlexMenu_PushButton(sButtonName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        rDataLibrary->PushActiveEntity();
        return LuaArgError("FlexMenu_PushButton");
    }

    //--Activity Checking
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX))
    {
        rDataLibrary->PushActiveEntity();
        return LuaTypeError("FlexMenu_PushButton", L);
    }

    //--Cast.
    FlexMenu *rMenu = (FlexMenu *)rDataLibrary->rActiveObject;

    //--Retrieve the button and push it.
    StarButton *rButton = rMenu->GetButtonS(lua_tostring(L, 1));
    rDataLibrary->PushActiveEntity(rButton);
    return 0;
}
int Hook_FlexMenu_SetHeaderSize(lua_State *L)
{
    //FlexMenu_SetHeaderSize(iTotalLines)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FlexMenu_SetHeaderSize");

    //--Must be a FlexMenu. StarMenus don't have headers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_SetHeaderSize", L);
    FlexMenu *rMenu = (FlexMenu *)rDataLibrary->rActiveObject;

    //--Set.
    rMenu->ResetHeader(lua_tointeger(L, 1));

    return 0;
}
int Hook_FlexMenu_SetHeaderLine(lua_State *L)
{
    //FlexMenu_SetHeaderLine(iIndex, sLine)
    //FlexMenu_SetHeaderLine(iIndex, sString, fFontSize)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("FlexMenu_SetHeaderLine");

    //--Must be a FlexMenu. StarMenus don't have headers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_SetHeaderSize", L);
    FlexMenu *rMenu = (FlexMenu *)rDataLibrary->rActiveObject;

    //--Set.
    if(tArgs == 2)
    {
        rMenu->SetHeaderLine(lua_tointeger(L, 1), lua_tostring(L, 2), -1.0f);
    }
    //--Set with a font size.
    else if(tArgs == 3)
    {
        rMenu->SetHeaderLine(lua_tointeger(L, 1), lua_tostring(L, 2), lua_tonumber(L, 3));
    }

    return 0;
}
int Hook_FlexMenu_ActivateIntro(lua_State *L)
{
    //FlexMenu_ActivateIntro()
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_ActivateIntro", L);
    FlexMenu *rMenu = (FlexMenu *)rDataLibrary->rActiveObject;
    rMenu->ActivateIntroMode();
    return 0;
}
int Hook_FlexMenu_ActivateLoading(lua_State *L)
{
    //FlexMenu_ActivateLoading()

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_ActivateLoading", L);

    //--Set.
    FlexMenu *rMenu = (FlexMenu *)rDataLibrary->rActiveObject;
    rMenu->ActivateLoadingMode();
    return 0;
}
int Hook_FlexMenu_SetDummySpriteFlag(lua_State *L)
{
    //FlexMenu_SetDummySpriteFlag(bUseDummySprites)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FlexMenu_SetDummySpriteFlag");

    //--Set.
    FlexMenu::xUseDummySprites = lua_toboolean(L, 1);
    return 0;
}

///======================================= Get Property ===========================================
int Hook_FlexMenu_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //FlexMenu_GetProperty("Show Splash") (1 Boolean) (Static)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FlexMenu_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Whether or not the splash screen should show.
    if(!strcasecmp(rSwitchType, "Show Splash"))
    {
        lua_pushboolean(L, FlexMenu::xShowSplash);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_GetProperty", L);
    //FlexMenu *rMenu = (FlexMenu *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("FlexMenu_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}

///======================================= Set Property ===========================================
int Hook_FlexMenu_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //--[Mods]
    //FlexMenu_SetProperty("Register Mod", sInternalName, sDisplayName, bIsEnabled)
    //FlexMenu_SetProperty("Set Mod Path", sInternalName, sPathname)
    //FlexMenu_SetProperty("Clear Modlist")
    //FlexMenu_SetProperty("Load Modlist", sFilepath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FlexMenu_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_MENU_FLEX)) return LuaTypeError("FlexMenu_SetProperty", L);
    FlexMenu *rMenu = (FlexMenu *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    ///--[Mods]
    //--Registers a mod to the list, implicitly as the tail.
    if(!strcasecmp(rSwitchType, "Register Mod") && tArgs >= 4)
    {
        rMenu->RegisterMod(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4));
    }
    //--Sets internal path of the mod. Optional.
    else if(!strcasecmp(rSwitchType, "Set Mod Path") && tArgs >= 3)
    {
        rMenu->SetPathOfMod(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Clears the modlist.
    else if(!strcasecmp(rSwitchType, "Clear Modlist") && tArgs >= 1)
    {
        rMenu->ClearModList();
    }
    //--Loads mods order off the hard drive.
    else if(!strcasecmp(rSwitchType, "Load Modlist") && tArgs >= 2)
    {
        rMenu->ReadLoadOrderFromFile(lua_tostring(L, 2));
    }
    ///--[Error]
    else
    {
        LuaPropertyError("FlexMenu_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}

