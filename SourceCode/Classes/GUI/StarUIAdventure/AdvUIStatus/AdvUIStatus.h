///======================================== AdvUIStatus ===========================================
//--Status screen, shows the character, stats, equipment, resistances, etc.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Icons
#define ADVMENU_STATUS_STATISTIC_ICONS_TOTAL 5
#define ADVMENU_STATUS_RESIST_ICONS_TOTAL 13

#define ADVMENU_STATUS_ITEM_MAX_NAME_LENGTH 217.0f
///========================================== Classes =============================================
class AdvUIStatus : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    //--Timers.
    static const int cAdvSwapTicks = 15;                //How many ticks it takes to change the visible character.

    //--Sizes.
    static constexpr float cxAdvItemMaxNameLen = 217.0f;    //How wide, in pixels, an equipment item name length can be before it compresses.

    //--Help.
    static const int cxHelpStringsTotal = 24;           //Number of unique StarlightStrings appearing on the help menu.

    ///--[System]
    //--System
    int mArrowTimer;

    //--Character
    int mCharacterCursor;
    bool mIsSwappingCharacter;
    int mCharacterSwapTimer;
    int mPrevCharacterSlot;

    //--Colors
    StarlightColor mColor_DarkEquipSlot;
    StarlightColor mColor_ResistBonus;
    StarlightColor mColor_ResistMalus;

    //--Strings
    StarlightString *mShowHelpString;
    StarlightString *mCharacterLftString;
    StarlightString *mCharacterRgtString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Level;
        StarFont *rFont_Mainline;
        StarFont *rFont_Statistic;

        //--Images
        StarBitmap *rFrame_Equipment;
        StarBitmap *rFrame_Status;
        StarBitmap *rFrame_Resistance;
        StarBitmap *rOverlay_ArrowLft;
        StarBitmap *rOverlay_ArrowRgt;
        StarBitmap *rOverlay_EquipmentDetail;
        StarBitmap *rOverlay_EquipmentHeader;
        StarBitmap *rOverlay_GemSlot;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_NameBanner;
        StarBitmap *rOverlay_ResistanceDetail;
        StarBitmap *rOverlay_ResistanceHeader;
        StarBitmap *rOverlay_StatusDetail;
        StarBitmap *rOverlay_StatusHPBarFill;
        StarBitmap *rOverlay_StatusHPFrame;
        StarBitmap *rOverlay_StatusLevelBacking;
        StarBitmap *rOverlay_StatusLevelOuter;
        StarBitmap *rOverlay_StatusXPBarFill;
        StarBitmap *rOverlay_StatusXPFrame;

        //--Common Parts
        StarBitmap *rStatisticIcons[ADVMENU_STATUS_STATISTIC_ICONS_TOTAL];
        StarBitmap *rResistanceIcons[ADVMENU_STATUS_RESIST_ICONS_TOTAL];
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIStatus();
    virtual ~AdvUIStatus();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    void ScrollCharacter(int pAmount);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    virtual void RenderBasics(AdvCombatEntity *pEntity, float pColorAlpha);
    virtual void RenderEquipment(AdvCombatEntity *pEntity, float pColorAlpha);
    virtual void RenderEquipmentEntry(AdvCombatEntity *pEntity, int pSlot, float pColorAlpha);
    virtual void RenderResistances(AdvCombatEntity *pEntity, float pColorAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


