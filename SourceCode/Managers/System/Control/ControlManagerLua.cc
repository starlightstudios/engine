//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

///======================================== Lua Hooking ===========================================
void ControlManager::HookToLuaState(lua_State *pLuaState)
{
    /* Profile_Begin(sName[])
       Begins a new control profile. CM_Rebind calls within the profile will be ignored and instead,
       stored for later purposes. When Profile_SetActive() is later called, the calls will be executed. */
    lua_register(pLuaState, "Profile_Begin", &Hook_Profile_Begin);

    /* Profile_End()
       Ends the current profile, popping it off the internal stack. */
    lua_register(pLuaState, "Profile_End", &Hook_Profile_End);

    /* Profile_SetActive(sName[])
       Executes a previously saved control profile. Does nothing if the profile is not found. */
    lua_register(pLuaState, "Profile_SetActive", &Hook_Profile_SetActive);

    /* CM_Rebind(sKeyName[], sPrimary[], sSecondary[])
       Rebinds the specified control to the specified targets. A list of valid keyboard targets
       and mouse targets is on the TODO list. */
    lua_register(pLuaState, "CM_Rebind", &Hook_CM_Rebind);

    /* CM_Overbind(sControlName, bIsSecondary, iKeyboard, iMouse, iJoystick)
       Rebinds the specified control by using direct scancodes. These scancodes are not portable. */
    lua_register(pLuaState, "CM_Overbind", &Hook_CM_Overbind);

    /* ControlManager_GetProperty("Input Codes By String", sString) (3 Integers) */
    lua_register(pLuaState, "ControlManager_GetProperty", &Hook_ControlManager_GetProperty);

    /* [System]
       ControlManager_SetProperty("Allocate Control Image Packs", iTotal)
       ControlManager_SetProperty("Set Control Error Image", sDLPath)
       ControlManager_SetProperty("Set Control Image Pack", iSlot, iKey, iMouse, iJoy, sDLPath) */
    lua_register(pLuaState, "ControlManager_SetProperty", &Hook_ControlManager_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Profile_Begin(lua_State *L)
{
    //Profile_Begin(sName[])
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("Profile_Begin");

    ControlManager::Fetch()->RegisterProfile(lua_tostring(L, 1));
    return 0;
}
int Hook_Profile_End(lua_State *L)
{
    //Profile_End()
    ControlManager::Fetch()->PopProfileStack();
    return 0;
}
int Hook_Profile_SetActive(lua_State *L)
{
    //Profile_SetActive(sName[])
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("Profile_SetActive");

    ControlManager::Fetch()->ActivateProfile(lua_tostring(L, 1));

    return 0;
}
int Hook_CM_Rebind(lua_State *L)
{
    //CM_Rebind(sKeyName[], sPrimary[], sSecondary[])
    int tArgs = lua_gettop(L);
    if(tArgs != 3) return LuaArgError("CM_Rebind");

    fprintf(stderr, "Rebound %s %s %s\n", lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));
    ControlManager::Fetch()->RebindControl(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));

    return 0;
}
#include "LuaManager.h"
int Hook_CM_Overbind(lua_State *L)
{
    //CM_Overbind(sControlName, bIsSecondary, iKeyboard, iMouse, iJoystick)
    int tArgs = lua_gettop(L);
    if(tArgs != 5) return LuaArgError("CM_Overbind");
    ControlManager::Fetch()->OverbindControl(lua_tostring(L, 1), lua_toboolean(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    return 0;
}
int Hook_ControlManager_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //ControlManager_GetProperty("Input Codes By String", sString) (3 Integers)
    //ControlManager_GetProperty("Control Image Path", sControlName) (1 String)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ControlManager_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[System]
    //--Keycode associated with the provided keycode. Format is "KEY_X"/"MOS_X"/"JOY_X" where X is the key in question.
    //  For a list of keycodes, see ControlManagerLookups.cc.
    if(!strcasecmp(rSwitchType, "Input Codes By String") && tArgs == 2)
    {
        int tKeyCode = -1;
        int tMouseCode = -1;
        int tJoyCode = -1;
        rControlManager->ResolveCode(lua_tostring(L, 2), tKeyCode, tMouseCode, tJoyCode);
        lua_pushinteger(L, tKeyCode);
        lua_pushinteger(L, tMouseCode);
        lua_pushinteger(L, tJoyCode);
        tReturns = 3;
    }
    //--Image used for the given control.
    else if(!strcasecmp(rSwitchType, "Control Image Path") && tArgs == 2)
    {
        lua_pushstring(L, rControlManager->ResolveControlImagePath(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("ControlManager_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_ControlManager_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //ControlManager_SetProperty("Allocate Control Image Packs", iTotal)
    //ControlManager_SetProperty("Set Control Error Image", sDLPath)
    //ControlManager_SetProperty("Set Control Image Pack", iSlot, iKey, iMouse, iJoy, sDLPath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ControlManager_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[System]
    //--Allocate space for images in the control lookups.
    if(!strcasecmp(rSwitchType, "Allocate Control Image Packs") && tArgs == 2)
    {
        rControlManager->AllocateImages(lua_tointeger(L, 2));
    }
    //--Sets the image that is displayed when no control image is found.
    else if(!strcasecmp(rSwitchType, "Set Control Error Image") && tArgs == 2)
    {
        rControlManager->SetErrorImage(lua_tostring(L, 2));
    }
    //--Sets the image matching a particular input group.
    else if(!strcasecmp(rSwitchType, "Set Control Image Pack") && tArgs == 6)
    {
        rControlManager->SetImage(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tostring(L, 6));
    }
    //--Error case.
    else
    {
        LuaPropertyError("ControlManager_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
