//--Base
#include "AdventureLevel.h"

//--Classes
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
void AdventureLevel::CreateDeepLayer(int pCollision)
{
    ///--[Documentation]
    //--Creates a deep-layer package associated with the given collision layer. If another package is
    //  already affecting that layer, does nothing.
    if(GetDeepLayerPack(pCollision)) return;

    //--Create.
    DeepLayerPack *nPack = (DeepLayerPack *)starmemoryalloc(sizeof(DeepLayerPack));
    nPack->Initialize();
    nPack->mCollisionAssociated = pCollision;
    mDeepLayers->AddElement("X", nPack, &FreeThis);
}

///===================================== Property Queries =========================================
DeepLayerPack *AdventureLevel::GetDeepLayerPack(int pCollision)
{
    ///--[Docoumentation]
    //--Returns a DeepLayerPack matching the collision, or NULL if none matches.
    DeepLayerPack *rPackage = (DeepLayerPack *)mDeepLayers->PushIterator();
    while(rPackage)
    {
        if(rPackage->mCollisionAssociated == pCollision)
        {
            mDeepLayers->PopIterator();
            return rPackage;
        }

        rPackage = (DeepLayerPack *)mDeepLayers->AutoIterate();
    }

    //--Not found.
    return NULL;
}
DeepLayerPack *AdventureLevel::GetDeepLayerPackForEntity(TilemapActor *pActor)
{
    ///--[Documentation]
    //--Returns a DeepLayerPack that is affecting the given actor, or NULL if it is not touching any
    //  collisions associated with a pack.
    if(!pActor) return NULL;

    //--Position. We check a single pixel in the center of the entity.
    int tCheckX = pActor->GetWorldX() + (TA_SIZE / 2);
    int tCheckY = pActor->GetWorldY() + (TA_SIZE / 2);

    //--Iterate.
    DeepLayerPack *rPackage = (DeepLayerPack *)mDeepLayers->PushIterator();
    while(rPackage)
    {
        //--Get the associated collision value.
        int tClipValue = GetWorldClipAt(tCheckX, tCheckY, 0, rPackage->mCollisionAssociated);

        //--Match! Return this pack.
        if(tClipValue > 0)
        {
            mDeepLayers->PopIterator();
            return rPackage;
        }

        //--Next.
        rPackage = (DeepLayerPack *)mDeepLayers->AutoIterate();
    }

    //--No matches.
    return NULL;
}

///======================================= Manipulators ===========================================
void AdventureLevel::SetDeepLayerPreventsRun(int pCollision, bool pPreventsRun)
{
    //--Get and check.
    DeepLayerPack *rPackage = GetDeepLayerPack(pCollision);
    if(!rPackage) return;

    //--Set.
    rPackage->mPreventsRun = pPreventsRun;
}
void AdventureLevel::SetDeepLayerPixelSink(int pCollision, int pPixelsSink)
{
    //--Get and check.
    DeepLayerPack *rPackage = GetDeepLayerPack(pCollision);
    if(!rPackage) return;

    //--Set.
    rPackage->mPixelSink = pPixelsSink;
}
void AdventureLevel::SetDeepLayerOverlayName(int pCollision, const char *pName)
{
    //--Get and check.
    DeepLayerPack *rPackage = GetDeepLayerPack(pCollision);
    if(!rPackage) return;

    //--User passed NULL, set to "Null".
    if(!pName)
    {
        strcpy(rPackage->mSpecialFrameOverlayName, "Null");
        return;
    }

    //--Copy. Remember this is a fixed-size array.
    strncpy(rPackage->mSpecialFrameOverlayName, pName, 31);
    rPackage->mSpecialFrameOverlayName[31] = '\0';
}
void AdventureLevel::SetDeepLayerWalkSound(int pCollision, const char *pName)
{
    //--Get and check.
    DeepLayerPack *rPackage = GetDeepLayerPack(pCollision);
    if(!rPackage) return;

    //--User passed NULL, set to "Null".
    if(!pName)
    {
        strcpy(rPackage->mWalkSound, "Null");
        return;
    }

    //--Copy. Remember this is a fixed-size array.
    strncpy(rPackage->mWalkSound, pName, 31);
    rPackage->mWalkSound[31] = '\0';
}
void AdventureLevel::SetDeepLayerRunSound(int pCollision, const char *pName)
{
    //--Get and check.
    DeepLayerPack *rPackage = GetDeepLayerPack(pCollision);
    if(!rPackage) return;

    //--User passed NULL, set to "Null".
    if(!pName)
    {
        strcpy(rPackage->mRunSound, "Null");
        return;
    }

    //--Copy. Remember this is a fixed-size array.
    strncpy(rPackage->mRunSound, pName, 31);
    rPackage->mRunSound[31] = '\0';
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
