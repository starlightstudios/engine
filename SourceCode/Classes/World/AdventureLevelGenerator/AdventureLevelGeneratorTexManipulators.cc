//--Base
#include "AdventureLevelGenerator.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

//--[Definitions]
//--Wall Standard Pattern
#define STDPATTERN_WALL_X 1.0f
#define STDPATTERN_WALL_Y 1.0f

#define STDPATTERN_WALLHI_EW_X 1.0f
#define STDPATTERN_WALLHI_EW_Y 0.0f
#define STDPATTERN_WALLHI_NS_X 0.0f
#define STDPATTERN_WALLHI_NS_Y 2.0f

#define STDPATTERN_WALLHICORNER_ULN_X 0.0f
#define STDPATTERN_WALLHICORNER_ULN_Y 0.0f
#define STDPATTERN_WALLHICORNER_ULS_X 0.0f
#define STDPATTERN_WALLHICORNER_ULS_Y 1.0f

#define STDPATTERN_WALLHICORNER_URN_X 2.0f
#define STDPATTERN_WALLHICORNER_URN_Y 0.0f
#define STDPATTERN_WALLHICORNER_URS_X 2.0f
#define STDPATTERN_WALLHICORNER_URS_Y 1.0f

#define STDPATTERN_WALLHITJUNCTIONL_LN_X 0.0f
#define STDPATTERN_WALLHITJUNCTIONL_LN_Y 3.0f
#define STDPATTERN_WALLHITJUNCTIONL_LS_X 0.0f
#define STDPATTERN_WALLHITJUNCTIONL_LS_Y 4.0f

#define STDPATTERN_WALLHITJUNCTIONL_RN_X 2.0f
#define STDPATTERN_WALLHITJUNCTIONL_RN_Y 3.0f
#define STDPATTERN_WALLHITJUNCTIONL_RS_X 2.0f
#define STDPATTERN_WALLHITJUNCTIONL_RS_Y 4.0f

#define STDPATTERN_WALLHITJUNCTIONL_MN_X 1.0f
#define STDPATTERN_WALLHITJUNCTIONL_MN_Y 3.0f
#define STDPATTERN_WALLHITJUNCTIONL_MS_X 1.0f
#define STDPATTERN_WALLHITJUNCTIONL_MS_Y 4.0f

#define STDPATTERN_WALLHICORNER_LL_X 0.0f
#define STDPATTERN_WALLHICORNER_LL_Y 5.0f
#define STDPATTERN_WALLHICORNER_LR_X 2.0f
#define STDPATTERN_WALLHICORNER_LR_Y 5.0f

//--Blackouts
#define STDPATTERN_BLACKOUT_FULL_X 0.0f
#define STDPATTERN_BLACKOUT_FULL_Y 2.0f
#define STDPATTERN_BLACKOUT_CORNERLL_X 0.0f
#define STDPATTERN_BLACKOUT_CORNERLL_Y 0.0f
#define STDPATTERN_BLACKOUT_LOWMIDDLE_X 1.0f
#define STDPATTERN_BLACKOUT_LOWMIDDLE_Y 0.0f
#define STDPATTERN_BLACKOUT_CORNERLR_X 2.0f
#define STDPATTERN_BLACKOUT_CORNERLR_Y 0.0f
#define STDPATTERN_BLACKOUT_CORNERIL_X 0.0f
#define STDPATTERN_BLACKOUT_CORNERIL_Y 1.0f
#define STDPATTERN_BLACKOUT_CORNERIR_X 2.0f
#define STDPATTERN_BLACKOUT_CORNERIR_Y 1.0f
#define STDPATTERN_BLACKOUT_LEFT_X 3.0f
#define STDPATTERN_BLACKOUT_LEFT_Y 0.0f
#define STDPATTERN_BLACKOUT_RIGHT_X 4.0f
#define STDPATTERN_BLACKOUT_RIGHT_Y 0.0f

//--[Worker Functions]
void MapUV(TwoDimensionReal &sDimensions, float pConstantX, float pConstantY, float pXStart, float pYStart, float pWidPerTile, float pHeiPerTile)
{
    //--Worker function, maps the UV coordinates based on the constants passed in.
    sDimensions.mLft = pXStart + (pConstantX * pWidPerTile) + (pWidPerTile / 32.0f);
    sDimensions.mTop = pYStart + (pConstantY * pHeiPerTile) + (pHeiPerTile / 32.0f);
    sDimensions.mRgt = sDimensions.mLft + pWidPerTile - (pWidPerTile / 32.0f * 2.0f);
    sDimensions.mBot = sDimensions.mTop + pHeiPerTile - (pHeiPerTile / 32.0f * 2.0f);
}

//--[Manipulators]
void AdventureLevelGenerator::SetTextureMap(const char *pPath)
{
    //--Load.
    rMapTexture = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    if(!rMapTexture) return;

    //--Resolve basic data.
    mTextureData.mWidPerTile = 16.0f / rMapTexture->GetWidth();
    mTextureData.mHeiPerTile = 16.0f / rMapTexture->GetHeight();
}
void AdventureLevelGenerator::SetPatternPosition(float pX, float pY)
{
    //--A texture map must exist before the pattern position can be set.
    if(!rMapTexture) return;
    mTextureData.mPatternXStart = pX / rMapTexture->GetWidth();
    mTextureData.mPatternYStart = pY / rMapTexture->GetHeight();

    //--Build texture lookups.
    MapUV(mTextureData.mErrorUV,    2,                                7,                                mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mWallUV,     STDPATTERN_WALL_X,                STDPATTERN_WALL_Y,                mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 0], STDPATTERN_WALLHI_EW_X,           STDPATTERN_WALLHI_EW_Y,           mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 1], STDPATTERN_WALLHI_NS_X,           STDPATTERN_WALLHI_NS_Y,           mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 2], STDPATTERN_WALLHICORNER_ULN_X,    STDPATTERN_WALLHICORNER_ULN_Y,    mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 3], STDPATTERN_WALLHICORNER_ULS_X,    STDPATTERN_WALLHICORNER_ULS_Y,    mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 4], STDPATTERN_WALLHICORNER_URN_X,    STDPATTERN_WALLHICORNER_URN_Y,    mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 5], STDPATTERN_WALLHICORNER_URS_X,    STDPATTERN_WALLHICORNER_URS_Y,    mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 6], STDPATTERN_WALLHITJUNCTIONL_LN_X, STDPATTERN_WALLHITJUNCTIONL_LN_Y, mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 7], STDPATTERN_WALLHITJUNCTIONL_LS_X, STDPATTERN_WALLHITJUNCTIONL_LS_Y, mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 8], STDPATTERN_WALLHITJUNCTIONL_RN_X, STDPATTERN_WALLHITJUNCTIONL_RN_Y, mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[ 9], STDPATTERN_WALLHITJUNCTIONL_RS_X, STDPATTERN_WALLHITJUNCTIONL_RS_Y, mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[10], STDPATTERN_WALLHITJUNCTIONL_MN_X, STDPATTERN_WALLHITJUNCTIONL_MN_Y, mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[11], STDPATTERN_WALLHITJUNCTIONL_MS_X, STDPATTERN_WALLHITJUNCTIONL_MS_Y, mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[12], STDPATTERN_WALLHICORNER_LL_X,     STDPATTERN_WALLHICORNER_LL_Y,     mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mUVList[13], STDPATTERN_WALLHICORNER_LR_X,     STDPATTERN_WALLHICORNER_LR_Y,     mTextureData.mPatternXStart, mTextureData.mPatternYStart, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
}
void AdventureLevelGenerator::AllocateFloors(int pTotal)
{
    mTextureData.mFloorsTotal = 0;
    free(mTextureData.mFloorUVs);
    mTextureData.mFloorUVs = NULL;
    if(pTotal < 1) return;

    mTextureData.mFloorsTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mTextureData.mFloorUVs = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal) * mTextureData.mFloorsTotal);
}
void AdventureLevelGenerator::SetFloorUV(int pSlot, float pLft, float pTop)
{
    //Left and Top are provided in texels.
    if(!rMapTexture) return;
    if(pSlot < 0 || pSlot >= mTextureData.mFloorsTotal) return;
    mTextureData.mFloorUVs[pSlot].Set(pLft, pTop, pLft+15.0f, pTop+15.0f);
    mTextureData.mFloorUVs[pSlot].mLft = mTextureData.mFloorUVs[pSlot].mLft / rMapTexture->GetWidth();
    mTextureData.mFloorUVs[pSlot].mTop = mTextureData.mFloorUVs[pSlot].mTop / rMapTexture->GetHeight();
    mTextureData.mFloorUVs[pSlot].mRgt = mTextureData.mFloorUVs[pSlot].mRgt / rMapTexture->GetWidth();
    mTextureData.mFloorUVs[pSlot].mBot = mTextureData.mFloorUVs[pSlot].mBot / rMapTexture->GetHeight();
}
void AdventureLevelGenerator::SetBlackoutPosition(float pX, float pY)
{
    //--Sets the position for the blackout lookups.
    float cBlackoutX = pX * mTextureData.mWidPerTile;
    float cBlackoutY = pY * mTextureData.mHeiPerTile;

    //--Build lookup table.
    MapUV(mTextureData.mBlackoutList[0], STDPATTERN_BLACKOUT_FULL_X,      STDPATTERN_BLACKOUT_FULL_Y,      cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mBlackoutList[1], STDPATTERN_BLACKOUT_CORNERLL_X,  STDPATTERN_BLACKOUT_CORNERLL_Y,  cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mBlackoutList[2], STDPATTERN_BLACKOUT_LOWMIDDLE_X, STDPATTERN_BLACKOUT_LOWMIDDLE_Y, cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mBlackoutList[3], STDPATTERN_BLACKOUT_CORNERLR_X,  STDPATTERN_BLACKOUT_CORNERLR_Y,  cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mBlackoutList[4], STDPATTERN_BLACKOUT_CORNERIL_X,  STDPATTERN_BLACKOUT_CORNERIL_Y,  cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mBlackoutList[5], STDPATTERN_BLACKOUT_CORNERIR_X,  STDPATTERN_BLACKOUT_CORNERIR_Y,  cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mBlackoutList[6], STDPATTERN_BLACKOUT_LEFT_X,      STDPATTERN_BLACKOUT_LEFT_Y,      cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mBlackoutList[7], STDPATTERN_BLACKOUT_RIGHT_X,     STDPATTERN_BLACKOUT_RIGHT_Y,     cBlackoutX, cBlackoutY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
}
void AdventureLevelGenerator::SetLadderPosition(float pDnX, float pDnY, float pUpX, float pUpY)
{
    mTextureData.mLadderDnUV[0].Set((pDnX+0) * mTextureData.mWidPerTile, (pDnY+0) * mTextureData.mHeiPerTile, (pDnX+1) * mTextureData.mWidPerTile, (pDnY+1) * mTextureData.mHeiPerTile);
    mTextureData.mLadderDnUV[1].Set((pDnX+0) * mTextureData.mWidPerTile, (pDnY+1) * mTextureData.mHeiPerTile, (pDnX+1) * mTextureData.mWidPerTile, (pDnY+2) * mTextureData.mHeiPerTile);
    mTextureData.mLadderDnUV[2].Set((pDnX+0) * mTextureData.mWidPerTile, (pDnY+2) * mTextureData.mHeiPerTile, (pDnX+1) * mTextureData.mWidPerTile, (pDnY+3) * mTextureData.mHeiPerTile);
    mTextureData.mLadderUpUV[0].Set((pUpX+0) * mTextureData.mWidPerTile, (pUpY+0) * mTextureData.mHeiPerTile, (pUpX+1) * mTextureData.mWidPerTile, (pUpY+1) * mTextureData.mHeiPerTile);
    mTextureData.mLadderUpUV[1].Set((pUpX+0) * mTextureData.mWidPerTile, (pUpY+1) * mTextureData.mHeiPerTile, (pUpX+1) * mTextureData.mWidPerTile, (pUpY+2) * mTextureData.mHeiPerTile);
    mTextureData.mLadderUpUV[2].Set((pUpX+0) * mTextureData.mWidPerTile, (pUpY+2) * mTextureData.mHeiPerTile, (pUpX+1) * mTextureData.mWidPerTile, (pUpY+3) * mTextureData.mHeiPerTile);
}
void AdventureLevelGenerator::SetCliffPosition(float pX, float pY)
{
    //--Constants
    float cCliffX = pX * mTextureData.mWidPerTile;
    float cCliffY = pY * mTextureData.mHeiPerTile;

    //--Build lookup table.
    MapUV(mTextureData.mCliffUV[CLIFF_TOPLFT], 0.0f, 0.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_TOPMID], 1.0f, 0.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_TOPRGT], 2.0f, 0.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_LFTMID], 0.0f, 1.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_RGTMID], 2.0f, 1.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_BOTLFT], 0.0f, 2.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_BOTMID], 1.0f, 2.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_BOTRGT], 2.0f, 2.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_CORNUL], 5.0f, 2.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_CORNUR], 3.0f, 2.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_CORNBL], 5.0f, 0.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_CORNBR], 3.0f, 0.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_TRITOP], 4.0f, 0.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_TRIRGT], 5.0f, 1.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_TRIBOT], 4.0f, 2.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_TRILFT], 3.0f, 1.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);

    //--Special.
    MapUV(mTextureData.mCliffUV[CLIFF_ULUR], 6.0f, 0.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_BLBR], 6.0f, 1.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
    MapUV(mTextureData.mCliffUV[CLIFF_ALL],  1.0f, 1.0f, cCliffX, cCliffY, mTextureData.mWidPerTile, mTextureData.mHeiPerTile);
}
void AdventureLevelGenerator::SetFloorEdgingPosition(float pX, float pY)
{
    mTextureData.mFloorEdgeUV[DIR_UP].Set       ((pX+1) * mTextureData.mWidPerTile, (pY+0) * mTextureData.mHeiPerTile, (pX+2) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile);
    mTextureData.mFloorEdgeUV[DIR_UPRIGHT].Set  ((pX+2) * mTextureData.mWidPerTile, (pY+0) * mTextureData.mHeiPerTile, (pX+3) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile);
    mTextureData.mFloorEdgeUV[DIR_RIGHT].Set    ((pX+2) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile, (pX+3) * mTextureData.mWidPerTile, (pY+2) * mTextureData.mHeiPerTile);
    mTextureData.mFloorEdgeUV[DIR_DOWNRIGHT].Set((pX+2) * mTextureData.mWidPerTile, (pY+2) * mTextureData.mHeiPerTile, (pX+3) * mTextureData.mWidPerTile, (pY+3) * mTextureData.mHeiPerTile);
    mTextureData.mFloorEdgeUV[DIR_DOWN].Set     ((pX+1) * mTextureData.mWidPerTile, (pY+2) * mTextureData.mHeiPerTile, (pX+2) * mTextureData.mWidPerTile, (pY+3) * mTextureData.mHeiPerTile);
    mTextureData.mFloorEdgeUV[DIR_DOWNLEFT].Set ((pX+0) * mTextureData.mWidPerTile, (pY+2) * mTextureData.mHeiPerTile, (pX+1) * mTextureData.mWidPerTile, (pY+3) * mTextureData.mHeiPerTile);
    mTextureData.mFloorEdgeUV[DIR_LEFT].Set     ((pX+0) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile, (pX+1) * mTextureData.mWidPerTile, (pY+2) * mTextureData.mHeiPerTile);
    mTextureData.mFloorEdgeUV[DIR_UPLEFT].Set   ((pX+0) * mTextureData.mWidPerTile, (pY+0) * mTextureData.mHeiPerTile, (pX+1) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile);
}
void AdventureLevelGenerator::SetTreasurePosition(float pX, float pY)
{
    mTextureData.mTreasureUV.Set((pX+0) * mTextureData.mWidPerTile, (pY+0) * mTextureData.mHeiPerTile, (pX+1) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile);
}
void AdventureLevelGenerator::SetEnemyUV(float pX, float pY)
{
    mTextureData.mEnemyUV[0].Set((pX+0) * mTextureData.mWidPerTile, (pY+0) * mTextureData.mHeiPerTile, (pX+1) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile);
    mTextureData.mEnemyUV[1].Set((pX+0) * mTextureData.mWidPerTile, (pY+1) * mTextureData.mHeiPerTile, (pX+1) * mTextureData.mWidPerTile, (pY+2) * mTextureData.mHeiPerTile);
    mTextureData.mEnemyUV[2].Set((pX+0) * mTextureData.mWidPerTile, (pY+2) * mTextureData.mHeiPerTile, (pX+1) * mTextureData.mWidPerTile, (pY+3) * mTextureData.mHeiPerTile);
}
