//--Base
#include "IRenderable.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
IRenderable::IRenderable()
{
}
IRenderable::~IRenderable()
{
}

///===================================== Property Queries =========================================
float IRenderable::GetDepth()
{
    return 0.0f;
}
void IRenderable::SetDepth(float pValue)
{
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void IRenderable::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void IRenderable::Render()
{
    //--Base does nothing, must be inherited.
}
int IRenderable::CompareRenderable(const void *pEntryA, const void *pEntryB)
{
    //--Sorting function to compare two instances of IRenderable classes based on their depths.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;
    IRenderable *rRenderableA = (IRenderable *)(*rEntryA)->rData;
    IRenderable *rRenderableB = (IRenderable *)(*rEntryB)->rData;

    //--Get and compare.
    float tDepthA = rRenderableA->GetDepth();
    float tDepthB = rRenderableB->GetDepth();

    //--Note: Epsilon value is good to 6 digits. It is assumed that floats are often not quite
    //  identical, and need this special comparison.
    const float cEpsilon = 0.0000001f;
    if(fabs(tDepthA - tDepthB) <= cEpsilon) return 0;

    //--Otherwise, one must be greater than the other.
    if(tDepthA > tDepthB) return 1;
    return -1;
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
