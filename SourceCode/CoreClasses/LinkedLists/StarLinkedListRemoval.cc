//--Base
#include "StarLinkedList.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

void *StarLinkedList::RemoveElementS(const char *pSearch)
{
    //--Removes and deletes (if applicable) the element by its name. The first instance matching
    //  will be deleted, and its data returned (though it may be unstable if it was deleted). If
    //  no matches are found, NULL is returned.
    //--You can be sure all matches were removed by calling this in a while loop. It will return
    //  NULL when they're all gone.
    void *rDataPtr = NULL;

    //--Special cases for empty list, head, and tail. The tail deletion is one exception, the
    //  first instance found is deleted in all other cases.
    if(mListSize == 0) return NULL;
    if(!rStringCompareFuncPtr(mListHead->mName, pSearch)) return DeleteHead();
    if(!rStringCompareFuncPtr(mListTail->mName, pSearch)) return DeleteTail();

    //--General case, the target is somewhere in this list...
    StarLinkedListEntry *rEntry = mListHead->rNext;
    for(int i = 1; i < mListSize-1; i ++)
    {
        if(!rStringCompareFuncPtr(rEntry->mName, pSearch))
        {
            StarLinkedListEntry *pEntry = rEntry->rPrev;
            StarLinkedListEntry *nEntry = rEntry->rNext;
            pEntry->rNext = nEntry;
            nEntry->rPrev = pEntry;

            rDataPtr = DeleteEntry(rEntry);
            DecrementList();
            return rDataPtr;
        }
        rEntry = rEntry->rNext;
    }

    //--The element was not found.
    return NULL;
}
void *StarLinkedList::RemoveElementI(int pSlot)
{
    //--As above, but removes an element bases on its position in the list, with 0 being the first.
    //  Can return NULL if the position was outside the range of the list.

    //--Special cases.
    if(pSlot < 0 || pSlot >= mListSize) return NULL;
    if(pSlot == 0) return DeleteHead();
    if(pSlot == mListSize-1) return DeleteTail();

    //--General case
    void *rDataPtr = NULL;
    StarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < pSlot; i ++)
    {
        rEntry = rEntry->rNext;
    }

    //--Remove the marked entry from the list.
    StarLinkedListEntry *rPrevEntry = rEntry->rPrev;
    StarLinkedListEntry *rNextEntry = rEntry->rNext;
    rPrevEntry->rNext = rNextEntry;
    rNextEntry->rPrev = rPrevEntry;
    DecrementList();

    //--Delete it and return what's left.
    rDataPtr = DeleteEntry(rEntry);
    return rDataPtr;
}
bool StarLinkedList::RemoveElementP(void *pPtr)
{
    //--Removes the element that contains the pointer provided. Does not return the pointer, since
    //  you'd have to have it in order to call this. Instead, provides a boolean indicating whether
    //  or not the pointer was found and removed.
    //--As usual, the first instance is the one removed, unless it's the tail.

    //--Special cases: RLL's cannot contain NULL entries. Also, empty list, head, and tail.
    if(!pPtr) return false;
    if(mListSize == 0) return false;
    if(mListHead->rData == pPtr) return DeleteHead();
    if(mListTail->rData == pPtr) return DeleteTail();

    StarLinkedListEntry *rEntry = mListHead->rNext;
    for(int i = 1; i < mListSize - 1; i ++)
    {
        if(rEntry->rData == pPtr)
        {
            StarLinkedListEntry *pEntry = rEntry->rPrev;
            StarLinkedListEntry *nEntry = rEntry->rNext;
            pEntry->rNext = nEntry;
            nEntry->rPrev = pEntry;

            DeleteEntry(rEntry);
            DecrementList();
            return true;
        }

        rEntry = rEntry->rNext;
    }
    return false;
}

///====================================== List Reordering =========================================
void StarLinkedList::MoveEntryTowardsHeadS(const char *pName)
{
    ///--[Documentation]
    //--Overload, handles MoveEntryTowardsHeadI but uses a name to find the index first.
    int tIndex = GetSlotOfElementByName(pName);
    if(tIndex == -1) return;

    //--Execute.
    MoveEntryTowardsHeadI(tIndex);
}
void StarLinkedList::MoveEntryTowardsHeadI(int pIndex)
{
    ///--[Documentation]
    //--Attempts to move the entry in question one slot towards the head, decrementing its position
    //  in the list order by one. If this is not possible, the routine does nothing and doesn't print
    //  any errors.
    //--This should not destabilize iterators since the pointers in question don't actually get removed,
    //  but if performed while iterating then it may cause an iterator to run on the same entry twice.

    ///--[Range Check]
    //--If the index is 0 then we don't need to do anything.
    if(pIndex <= 0) return;

    //--If the entry is past the end of the list, do nothing.
    if(pIndex >= mListSize) return;

    ///--[Fetch Entries]
    //--Visualize the entries as A, B, C, and D. C is the entry that is being moved. When the operation
    //  is done, the order is A, C, B, D. Note that A/D may not exist.
    StarLinkedListEntry *rEntryC = mListHead;
    for(int i = 0; i < pIndex; i ++)
    {
        rEntryC = rEntryC->rNext;
    }

    //--Error check.
    if(!rEntryC) return;

    //--Get entries. B must exist, but A and D may not.
    StarLinkedListEntry *rEntryB = rEntryC->rPrev;
    StarLinkedListEntry *rEntryA = rEntryB->rPrev; //Can be NULL
    StarLinkedListEntry *rEntryD = rEntryC->rNext; //Can be NULL

    ///--[Set List Order]
    //--If Entry A exists, then its next needs to point to C.
    if(rEntryA)
    {
        rEntryA->rNext = rEntryC;
    }

    //--Entry B's previous is C, next is D.
    if(rEntryB)
    {
        rEntryB->rPrev = rEntryC;
        rEntryB->rNext = rEntryD;
    }

    //--Entry C's previous is A, next is B.
    if(rEntryC)
    {
        rEntryC->rPrev = rEntryA;
        rEntryC->rNext = rEntryB;
    }

    //--If Entry D exists, then its previous is B.
    if(rEntryD)
    {
        rEntryD->rPrev = rEntryB;
    }

    ///--[Special Cases]
    //--If the index was 1, then the list head will change to Entry C.
    if(pIndex == 1) mListHead = rEntryC;

    //--If the index was the last entry, the list tail will change to Entry B.
    if(pIndex == mListSize-1) mListTail = rEntryB;
}
void StarLinkedList::MoveEntryTowardsTailS(const char *pName)
{
    ///--[Documentation]
    //--Overload, handles MoveEntryTowardsTailI but uses a name to find the index first.
    int tIndex = GetSlotOfElementByName(pName);
    if(tIndex == -1) return;

    //--Execute.
    MoveEntryTowardsTailI(tIndex);
}
void StarLinkedList::MoveEntryTowardsTailI(int pIndex)
{
    ///--[Documentation]
    //--Attempts to move the entry in question one slot towards the tail, incrementing its position
    //  in the list order by one. If this is not possible, the routine does nothing and doesn't print
    //  any errors.
    //--This should not destabilize iterators since the pointers in question don't actually get removed,
    //  but if performed while iterating then it may cause an iterator to skip an entry or run twice on one.

    ///--[Range Check]
    //--If the index is -1 or lower, ignore it.
    if(pIndex < 0) return;

    //--If the entry is at the end of the list, it cannot move further towards the tail. Stop.
    if(pIndex >= mListSize - 1) return;

    ///--[Fetch Entries]
    //--Visualize the entries as A, B, C, and D. B is the entry that is being moved. When the operation
    //  is done, the order is A, C, B, D. Note that A/D may not exist.
    StarLinkedListEntry *rEntryB = mListHead;
    for(int i = 0; i < pIndex; i ++)
    {
        rEntryB = rEntryB->rNext;
    }

    //--Error check.
    if(!rEntryB) return;

    //--Get entries. C must exist, but A and D may not.
    StarLinkedListEntry *rEntryA = rEntryB->rPrev; //Can be NULL
    StarLinkedListEntry *rEntryC = rEntryB->rNext;
    StarLinkedListEntry *rEntryD = rEntryC->rNext; //Can be NULL

    ///--[Set List Order]
    //--If Entry A exists, then its next needs to point to C.
    if(rEntryA)
    {
        rEntryA->rNext = rEntryC;
    }

    //--Entry B's previous is C, next is D.
    if(rEntryB)
    {
        rEntryB->rPrev = rEntryC;
        rEntryB->rNext = rEntryD;
    }

    //--Entry C's previous is A, next is B.
    if(rEntryC)
    {
        rEntryC->rPrev = rEntryA;
        rEntryC->rNext = rEntryB;
    }

    //--If Entry D exists, then its previous is B.
    if(rEntryD)
    {
        rEntryD->rPrev = rEntryB;
    }

    ///--[Special Cases]
    //--If the index was 0, the list head is now Entry C.
    if(pIndex == 0) mListHead = rEntryC;

    //--If the index was the second-to-last entry, the list tail is now Entry B.
    if(pIndex == mListSize - 2) mListTail = rEntryB;
}
