-- |[ ================================ Slitted Eye Windows Build =============================== ]|
--Operating System: Windows
--Library Type: Allegro/SDL/SDLFMOD
local sBuildName = "Slitted Eye Win"

--Check existence.
if(BP_Exists(sBuildName) == true) then return end

--Create.
BP_Create(sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../builds/Slitted Eye Win/")

-- |[ ========================================= Common ========================================= ]|
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

-- |[Basics]|
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

-- |[Executables]|
--Executable files.
BP_SetProperty("Register Command", "Copy File", "StarlightEngineAL.exe",      "SlittedEyeAL.exe")
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDL.exe",     "SlittedEyeSDL.exe")
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDLFMOD.exe", "SlittedEyeSDLFMOD.exe")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumAL.exe",        "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumSDL.exe",       "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "PandemoniumSDLFMOD.exe",   "BuildFiles/IconAdv.ico")

--DLL files.
BP_SetProperty("Register Command", "Copy File", "bass.dll",    "bass.dll")
BP_SetProperty("Register Command", "Copy File", "bass_fx.dll", "bass_fx.dll")
BP_SetProperty("Register Command", "Copy File", "fmodL.dll",   "fmodL.dll")
BP_SetProperty("Register Command", "Copy File", "lua54.dll",   "lua54.dll")

-- |[Bootstraps and Counters]|
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

-- |[ =================================== Base Instructions ==================================== ]|
-- |[Text Files]|
--Relevant changelogs.
--BP_SetProperty("Register Command", "Copy File", "Changelog (Adventure).txt", "Changelog.txt")

--Credits files.
--BP_SetProperty("Register Command", "Copy File", "Credits.txt", "Credits.txt")

--Readme.
--BP_SetProperty("Register Command", "Copy File", "Readme.txt", "Readme.txt")

-- |[Configuration Files]|
--Engine configurations.
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Adventure.lua", "Config_Adventure.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",    "Config_Engine.lua")

-- |[Batch Files]|
--For people with audio problems.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/ZBlockAudio Adventure.bat", "ZBlockAudio.bat")

-- |[ =================================== Engine Subfolders ==================================== ]|
-- |[Data Directory]|
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

-- |[Saves Directory]|
--Copy the controls files.
--BP_SetProperty("Register Command", "Create Directory", "Saves")
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

--Copy the autosaves directory and the notifier on how to use autosaves.
--BP_SetProperty("Register Command", "Create Directory", "Saves/Autosave")
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/How to Recover.txt",  "Saves/Autosave/How to Recover.txt")

-- |[Shaders Directory]|
--Copy the Windows/Linux shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/WinLin", "Shaders/WinLin")

-- |[ ==================================== Game Subfolders ===================================== ]|
-- |[Adventure Directories]|
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../slittedeye/Games/Slitted Eye", "Games/Slitted Eye")

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the active object.
DL_PopActiveObject()
