//--Base
#include "MonocerosLoadInterrupt.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
MonocerosLoadInterrupt::MonocerosLoadInterrupt()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======== StarLoadInterrupt ========= ]
    ///--[ ====== MonocerosLoadInterrupt ====== ]
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    mIsReady = false;
    for(int i = 0; i < LI_MONO_LOADFRAMES; i ++) mLoadingFrames[i] = NULL;
}
MonocerosLoadInterrupt::~MonocerosLoadInterrupt()
{
    for(int i = 0; i < LI_MONO_LOADFRAMES; i ++) delete mLoadingFrames[i];
}
void MonocerosLoadInterrupt::Construct()
{
    ///--[Documentation]
    //--Loads and verifies images associated with this object. Because this object is meant to appear during
    //  the loading of other objects, it handles its images internally, not using a conventional DataLibrary.

    ///--[Load Frames]
    //--Loading animation is a filmstrip series placed in a linked list.
    StarLumpManager *tSLM = new StarLumpManager();
    tSLM->Open("Data/Loading.slf");
    if(!tSLM->IsFileOpen()) return;

    //--Suppress the load interrupt.
    bool tOldFlag = StarBitmap::xSuppressInterrupt;
    StarBitmap::xSuppressInterrupt = true;

    //--Load.
    char tBuf[128];
    for(int i = 0; i < LI_MONO_LOADFRAMES; i ++)
    {
        sprintf(tBuf, "Monoceros|Frame%i", i);
        mLoadingFrames[i] = tSLM->GetImage(tBuf);
    }

    //--Verify.
    mIsReady = true;
    for(int i = 0; i < LI_MONO_LOADFRAMES; i ++)
    {
        if(!mLoadingFrames[i]) mIsReady = false;
    }

    ///--[Finish Up]
    //--Set to the String Tyrant variant. If anything went wrong, the default one will be used.
    StarBitmap::xInterruptCall = &StarLoadInterrupt::Interrupt;

    //--Set this flag.
    mIsAwaitingKeypress = false;

    //--Clean up.
    delete tSLM;
    StarBitmap::xSuppressInterrupt = tOldFlag;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void MonocerosLoadInterrupt::DeallocateImages()
{
    ///--[Documentation]
    //--Deallocates and NULLs all images associated with this object.
    mIsReady = false;
    StarLoadInterrupt::DeallocateImages();

    //--Derived call.
    for(int i = 0; i < LI_MONO_LOADFRAMES; i ++) delete mLoadingFrames[i];

    //--NULL.
    for(int i = 0; i < LI_MONO_LOADFRAMES; i ++) mLoadingFrames[i] = NULL;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonocerosLoadInterrupt::Call(bool pForceRender)
{
    ///--[Documentation and Setup]
    //--Chapter 1 Load Interrupt renderer, shows an animation of Bee Mei flying.
    if(!CommonLogicUpdate(pForceRender) || !mIsReady) return;

    //--Call the renderer setup. The screen is now ready for drawing.
    CommonRendererSetup();

    ///--[Render]
    //--Just render whichever frame is active.
    float cTPF = 5.0f;
    int tFrame = ((int)(mFrame / cTPF)) % LI_MONO_LOADFRAMES;
    mLoadingFrames[tFrame]->Draw();

    ///--[Finish Up]
    CommonRendererClean();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void MonocerosLoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_BootMonoceros()
       Switches to Monoceros mode. */
    lua_register(pLuaState, "LI_BootMonoceros", &Hook_LI_BootMonoceros);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LI_BootMonoceros(lua_State *L)
{
    //LI_BootMonoceros()

    //--Create a new LoadInterrupt.
    MonocerosLoadInterrupt *nInterrupt = new MonocerosLoadInterrupt();
    nInterrupt->Construct();

    //--Dealloacte the old one.
    GLOBAL *rGlobal = Global::Shared();
    delete rGlobal->gLoadInterrupt;

    //--Replace.
    rGlobal->gLoadInterrupt = nInterrupt;
    return 0;
}
