///====================================== RunningMinigame =========================================
//--Minigame where the protagonist and a follower must run, jump, and smash their way past various obstacles.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[RMGObstaclePack]
typedef struct RMGObstaclePack
{
    //--Position (in tile coordinates)
    int mLft;
    int mTop;
    int mRgt;
    int mBot;

    //--Obstacle Type
    int mObstacleType;

    //--Functions
    void Initialize()
    {
        mLft = 0;
        mTop = 0;
        mRgt = 0;
        mBot = 0;
        mObstacleType = -1;
    }
}RMGObstaclePack;

///--[RMGEnemy]
typedef struct RMGEnemy
{
    //--Position (in world coordinates)
    float mX;
    float mY;
    float mXSpeed;
    float mYSpeed;

    //--Display
    bool mIsInjured;
    int mImage;

    //--Functions
    void Initialize()
    {
        mX = 0.0f;
        mY = 0.0f;
        mXSpeed = 0.0f;
        mYSpeed = 0.0f;
        mIsInjured = false;
        mImage = rand() % 3;
    }
}RMGEnemy;

///===================================== Local Definitions ========================================
//--Tile Types
#define RMG_FLOOR_NORMAL 0
#define RMG_FLOOR_PIT 1
#define RMG_FLOOR_OBSTACLE 2
#define RMG_FLOOR_WARNING 3

//--Obstacle Types
#define RMG_OBSTACLE_TYPE_INVALID -1
#define RMG_OBSTACLE_TYPE_PIT 0
#define RMG_OBSTACLE_TYPE_LOWWALL 1
#define RMG_OBSTACLE_TYPE_ENEMY 2

//--World Sizes
#define RMG_TILE_SIZE 16.0f
#define RMG_ACTIVE_WORLD_LEN 32
#define RMG_ACTIVE_WORLD_WIDTH 6

//--Image Counts
#define RMG_GROUND_TILES 3
#define RMG_HIWALL_TILES 2
#define RMG_LOWALL_TILES 8
#define RMG_ENEMIES 3
#define RMG_CHECKPOINT_TILE (RMG_GROUND_TILES-1)

#define RMG_PIT_OFFSET 0
#define RMG_WARNING_OFFSET 1
#define RMG_OBSTACLE_OFFSET 2

//--Player Properties
#define RMG_PLAYER_TILE 5
#define RMG_PLAYER_RUN_TPF 6.0f

//--Player Images
#define RMG_PLAYER_RUN_TOTAL 4
#define RMG_PLAYER_RUN_W 0
#define RMG_PLAYER_RUN_NW 1
#define RMG_PLAYER_RUN_SW 2
#define RMG_PLAYER_RUN_DIR_TOTAL 3
#define RMG_PLAYER_JUMP_UP_FRAME 1
#define RMG_PLAYER_JUMP_DN_FRAME 2

//--Companion Properties
#define RMG_COMPANION_TICKS 8

//--Introduction States
#define RMG_INTRO_NONE -1
#define RMG_INTRO_PLAYER_RUNS_IN 0
#define RMG_INTRO_ASK_TO_SKIP 1
#define RMG_INTRO_LEARN_TO_JUMP 2
#define RMG_INTRO_LEARN_TO_SWING 3
#define RMG_INTRO_SHOW_HP 4
#define RMG_INTRO_F1_TO_SKIP 5
#define RMG_OUTRO_ESCAPE 6

//--Introduction Constants
#define RMG_INTRO_RUN_IN_OFFSCREEN 70
#define RMG_INTRO_RUN_IN_OFFSCREEN_DONE 120
#define RMG_INTRO_RUN_IN_END 180
#define RMG_INTRO_FADE_TICKS 45
#define RMG_INTRO_HOLD_TICKS 300

//--GUI
#define RMG_HEARTS_MAX 5

///========================================== Classes =============================================
class RunningMinigame
{
    private:
    //--System
    bool mIsActive;
    bool mHoldingPattern;
    int mOpacityTimer;
    int mFadeinTimer;
    bool mBypassTutorial;

    //--Introduction
    int mIntroductionState;
    int mIntroTimer;

    //--Player State
    int mPlayerStopTimer;
    int mPlayerInvincibleTimer;
    int mPlayerHealth;
    float mPlayerSpeed;
    int mSlashTimer;
    int mSlashCooldown;
    int mPlayerCurrentDirection[RMG_COMPANION_TICKS];
    int mRunTimer[RMG_COMPANION_TICKS];
    int mJumpTimer[RMG_COMPANION_TICKS];
    float mPlayerYPos[RMG_COMPANION_TICKS];
    float mPlayerZPos[RMG_COMPANION_TICKS];
    float mPlayerZSpeed[RMG_COMPANION_TICKS];

    //--Checkpoint
    int mCheckpoint;

    //--Enemies
    StarLinkedList *mEnemyList;

    //--Defeat
    int mDefeatFadeTimer;

    //--Hearts
    float mHeartX[RMG_HEARTS_MAX];
    float mHeartY[RMG_HEARTS_MAX];
    float mHeartR[RMG_HEARTS_MAX];
    float mHeartXSpeed[RMG_HEARTS_MAX];
    float mHeartYSpeed[RMG_HEARTS_MAX];
    float mHeartRSpeed[RMG_HEARTS_MAX];

    //--Player/Companion Images
    StarBitmap *rPlayerRunImg[RMG_PLAYER_RUN_DIR_TOTAL][RMG_PLAYER_RUN_TOTAL];
    StarBitmap *rCompanionRunImg[RMG_PLAYER_RUN_DIR_TOTAL][RMG_PLAYER_RUN_TOTAL];
    StarBitmap *rShadowImg;

    //--World Layout (Active)
    int mCurrentWorldTile;
    int mTotalWorldTiles;
    float mWorldXOffset;
    int mWorldWallHi[RMG_ACTIVE_WORLD_LEN];
    int mWorldWallLo[RMG_ACTIVE_WORLD_LEN];
    int mWorldFloor[RMG_ACTIVE_WORLD_WIDTH][RMG_ACTIVE_WORLD_LEN];
    int mWorldTile[RMG_ACTIVE_WORLD_WIDTH][RMG_ACTIVE_WORLD_LEN];

    //--World Layout (Passive)
    StarLinkedList *mObstacleList;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Base Image
            StarBitmap *rBase;

            //--UI
            StarBitmap *mHeart;
            StarBitmap *mDistanceL;
            StarBitmap *mDistanceM;
            StarBitmap *mDistanceR;
            StarBitmap *mCheckpointL;
            StarBitmap *mCheckpointM;
            StarBitmap *mCheckpointR;

            //--Ground Tiles
            StarBitmap *mGroundTile[RMG_GROUND_TILES];
            StarBitmap *mWallHi[RMG_HIWALL_TILES];
            StarBitmap *mWallLo[RMG_LOWALL_TILES];
            StarBitmap *mPitWall;
            StarBitmap *mPitMarkerTile;
            StarBitmap *mLowObstacle;
            StarBitmap *mEnemiesUp[RMG_ENEMIES];
            StarBitmap *mEnemiesDn[RMG_ENEMIES];

            //--Slashes
            StarBitmap *mSlashU;
            StarBitmap *mSlashD;

            //--Tutorial
            StarBitmap *rTutorial0;
            StarBitmap *rTutorial1;
            StarBitmap *rTutorial2;
            StarBitmap *rTutorial3;
            StarBitmap *rTutorial4;
            StarBitmap *rTutorial5;
        }Data;
    }Images;

    protected:

    public:
    //--System
    RunningMinigame();
    ~RunningMinigame();
    void Construct();
    void AssemblePlayerImages();

    //--Public Variables
    //--Property Queries
    bool IsGameFinished();
    bool IsVisible();
    bool IsFullyOpaque();

    //--Manipulators
    void Activate();
    void Deactivate();
    void SetHoldingPattern(bool pFlag);
    void TakeHit();

    //--Core Methods
    void SetPlayerImg(int pDir, int pSlot, StarBitmap *pImg);
    void SetShadowImg(StarBitmap *pImg);
    void GenerateLevel();
    StarBitmap *ResolvePlayerFrame();
    StarBitmap *ResolveCompanionFrame();
    void Reset();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

