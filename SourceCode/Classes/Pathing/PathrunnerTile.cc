//--Base
#include "PathrunnerTile.h"

//--Classes
#include "AdventureLevel.h"
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Local Definitions]
#define SP_CLIPPED -1
#define SP_UNUSED -3

///--[Debug]
//#define PATHRUNNER_SMART_PATH_DEBUG
#ifdef PATHRUNNER_SMART_PATH_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
PathrunnerTile::PathrunnerTile()
{
    ///--[ ====== Variable Initialization ===== ]
    ///--[Pathrunner]
    ///--[PathrunnerTile]
}
PathrunnerTile::~PathrunnerTile()
{

}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
bool PathrunnerTile::IsMoveShortest(int pX, int pY, int pPulseVal)
{
    ///--[Documentation]
    //--Given a position and a pulse value, returns true if that position is one pulse value lower.
    //  This allows the routine to "walk" down the pulse values to zero.
    if(pX < 0 || pY < 0 || pX >= mXSize || pY >= mYSize) return false;

    //--Pulse values match.
    if(mPathArray[pX][pY] >= 0 && mPathArray[pX][pY] < pPulseVal) return true;

    //--Failed.
    return false;
}
bool PathrunnerTile::IsMoveValid(int pX, int pY, int pPulseVal, int pDirection)
{
    ///--[Documentation]
    //--Returns true if the given move is legal, false if it hits a collision, out of bounds, or locates a shorter pulse.
    //  The X/Y values are in tiles.
    int tSpotCheckX = pX;
    int tSpotCheckY = pY;
    if(pDirection == DIR_UP)
    {
        tSpotCheckY -= 1;
    }
    else if(pDirection == DIR_RIGHT)
    {
        tSpotCheckX += 1;
    }
    else if(pDirection == DIR_DOWN)
    {
        tSpotCheckY += 1;
    }
    else if(pDirection == DIR_LEFT)
    {
        tSpotCheckX -= 1;
    }

    //--Range check.
    if(tSpotCheckX < 0 || tSpotCheckY < 0 || tSpotCheckX >= mXSize || tSpotCheckY >= mYSize) return false;

    //--If the position is clipped, can't move there.
    if(mPathArray[tSpotCheckX][tSpotCheckY] == SP_CLIPPED) return false;

    //--If the pulse value is unused, succeed.
    if(mPathArray[tSpotCheckX][tSpotCheckY] == SP_UNUSED) return true;

    //--If the pulse value is lower than the current one, fail.
    if(mPathArray[tSpotCheckX][tSpotCheckY] <= pPulseVal) return false;

    //--All checks passed.
    return true;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
StarLinkedList *PathrunnerTile::RunPathingIntoList()
{
    ///--[ ========== Documentation =========== ]
    //--Run the advanced pathing handler and put the results in the provided linked list. Start and end
    //  coordinates are in tiles, not pixels.
    //--Returns a list of (int *)'s, which represent the path instructions from start to end. If there is an
    //  error, or the path is unreachable, or the algorithm isn't done yet, returns NULL.
    if(mFailure) return NULL;

    //--If not initialized, the user needs to do that first.
    if(!mIsInitialized) return NULL;

    ///--[Path Runner]
    //--Run the pathing algorithm.
    RunIterator(mIterationsToRun, false);

    //--Debug.
    if(false)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            for(int x = 0; x < mXSize; x ++)
            {
                if(mPathArray[x][y] == SP_UNUSED)
                {
                    fprintf(stderr, "UU ");
                }
                else if(mPathArray[x][y] == SP_CLIPPED)
                {
                    fprintf(stderr, "## ");
                }
                else
                {
                    fprintf(stderr, "%02i ", mPathArray[x][y]);
                }
            }
            fprintf(stderr, "\n");
        }
    }

    //--If the algorithm didn't find a path in the given number of pulses, stop for now.
    if(!mHasAnyPath)
    {
        //--If the algorithm exhausted all pulses and didn't find a path, there is no path!
        if(mPathRunnerList->GetListSize() < 1)
        {
            mFailure = true;
            return NULL;
        }

        //--Otherwise, the function will need to be called again.
        return NULL;
    }

    ///--[ ============== Success ============= ]
    ///--[Setup]
    //--Debug.
    DebugPush(true, "Smart Pathing Has Path: Pulses run: %i\n", mPulseTotal);
    DebugPrint("Shortest distance: %f\n", mShortestDistance);
    DebugPrint("Pulses remaining: %i\n", mPathRunnerList->GetListSize());

    //--At least one solution has been found. First, error check the path array.
    if(mPathArray[mEndX][mEndY] == SP_UNUSED)
    {
        mFailure = true;
        DebugPop("Smart Pathing Completes: Unable to find a path between start and finish.\n");
        return NULL;
    }

    //--Run this algorithm to get a path.
    StarLinkedList *nPathList = ResolvePath();
    if(!nPathList)
    {
        DebugPop("Smart Pathing Completes: Couldn't build a path with ResolvePath().\n");
        return NULL;
    }

    //--Debug.
    #if defined PATHRUNNER_SMART_PATH_DEBUG
    DebugPrint("Path length after walking is %i.\n", nPathList->GetListSize());
    if(true)
    {
        int *rWalkPtr = (int *)nPathList->PushIterator();
        while(rWalkPtr)
        {
            if(*rWalkPtr == DIR_NORTH) DebugPrint("N");
            if(*rWalkPtr == DIR_SOUTH) DebugPrint("S");
            if(*rWalkPtr == DIR_WEST)  DebugPrint("W");
            if(*rWalkPtr == DIR_EAST)  DebugPrint("E");
            rWalkPtr = (int *)nPathList->AutoIterate();
        }
        DebugPrint("\n");
    }
    #endif

    //--Debug.
    if(false)
    {
        fprintf(stderr, "Printing array: %i\n", nPathList->GetListSize());
        for(int y = 0; y < mYSize; y ++)
        {
            for(int x = 0; x < mXSize; x ++)
            {
                if(mPathArray[x][y] == SP_UNUSED)
                {
                    fprintf(stderr, "UU ");
                }
                else if(mPathArray[x][y] == SP_CLIPPED)
                {
                    fprintf(stderr, "## ");
                }
                else
                {
                    fprintf(stderr, "%02i ", mPathArray[x][y]);
                }
            }
            fprintf(stderr, "\n");
        }
    }

    ///--[Finish Up]
    //--Pass back the list.
    DebugPop("Smart Pathing Completes: Finished normally.\n");
    return nPathList;
}
void PathrunnerTile::Initialize(int pStartX, int pStartY, int pEndX, int pEndY)
{
    ///--[Documentation]
    //--Sets up variables and stores everything needed for execution.
    if(mIsInitialized) return;

    //--Retrive the level that this will run in.
    rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel)
    {
        mFailure = true;
        return;
    }

    //--Store positions.
    mStartX = pStartX;
    mStartY = pStartY;
    mEndX = pEndX;
    mEndY = pEndY;

    ///--[Collision Setup]
    //--Debug.
    DebugPush(true, "Smart Pathing Initializes.\n");

    //--Get collisions.
    CollisionPack *rCollisionPack = rActiveLevel->GetCollisionPack(mCollisionDepth);
    if(!rCollisionPack)
    {
        mFailure = true;
        DebugPop("Smart Pathing Completes: Error, no collision pack on depth %i.\n", mCollisionDepth);
        return;
    }

    //--Fast-access variables.
    mXSize = rCollisionPack->mCollisionSizeX;
    mYSize = rCollisionPack->mCollisionSizeY;

    //--Size check.
    if(mXSize < 1 || mYSize < 1)
    {
        mFailure = true;
        DebugPop("Smart Pathing Completes: Error, collision pack has invalid sizes %ix%i.\n", mXSize, mYSize);
        return;
    }

    //--Create a 2-D array holding the path data. Set the values to their defaults.
    DebugPrint("Allocating.\n");
    mPathArray = (int16_t **)starmemoryalloc(sizeof(int16_t *) * mXSize);
    for(int x = 0; x < mXSize; x ++)
    {
        mPathArray[x] = (int16_t *)starmemoryalloc(sizeof(int16_t) * mYSize);
        for(int y = 0; y < mYSize; y ++)
        {
            mPathArray[x][y] = SP_UNUSED;
        }
    }

    //--Fill the array with the collision data. This speeds up pathing.
    DebugPrint("Copying.\n");
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            if(rCollisionPack->mCollisionData[x][y] > CLIP_NONE) mPathArray[x][y] = SP_CLIPPED;
        }
    }

    ///--[Path Runner Setup]
    //--Create a linked list. This holds the positions we're checking.
    mPathRunnerList = new StarLinkedList(true);

    //--The first element on the list is the starting point.
    PathrunnerPack *nFirstPack = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
    nFirstPack->mPulseVal = 0;
    nFirstPack->mDistToLine = 0;
    nFirstPack->mDistToTarget = GetPlanarDistance(mStartX, mStartY, mEndX, mEndY);
    nFirstPack->mX = pStartX;
    nFirstPack->mY = pStartY;
    nFirstPack->mPrevDir = DIR_UP;
    mPathRunnerList->AddElementAsTail("X", nFirstPack, &FreeThis);

    ///--[Distances]
    //--Store distances.
    mShortestDistance = nFirstPack->mDistToTarget;
    mOriginalDistance = mShortestDistance;

    ///--[Finish Up]
    //--Specify that initialization is done.
    mPulseTotal = 0;
    mIsInitialized = true;

    //--Debug report.
    DebugPrint("Start position:  %ix%i\n", mStartX, mStartY);
    DebugPrint("Target position: %ix%i\n", mEndX, mEndY);
    DebugPrint("Sizes: %ix%i\n", mXSize, mYSize);
    DebugPrint("Distance: %f\n", mShortestDistance);
    DebugPop("Smart Pathing Completes Initialization: Finished normally.\n");
}
void PathrunnerTile::RunIterator(int pPulseCap, bool pStopOnReachingTarget)
{
    ///--[Documentation]
    //--Runs the path iterator. The list contains all the unresolved pulses which are trying to place their
    //  values on the collision grid. While the elements are placed in such a manner that the closest ones
    //  get priority, it is very possible that the shortest possible path is not the first one arrived at.
    //--The algorithm runs until the pulse cap is reached, and then stops. Pass -1 to ignore the pulse cap
    //  and run until the algorithm is totally out of pulses.
    if(!mIsInitialized || mFailure) return;

    //--Allowable pulses. Decrements. When it reaches zero, iteration stops. If you pass a negative in, then
    //  the algorithm never reaches zero and won't stop until pulses are exhausted.
    int tAllowablePulses = pPulseCap;

    ///--[Iteration]
    PathrunnerPack *rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
    while(rIteratingPack)
    {
        ///--[Pulse Cap]
        //--If the allowable pulses value hits zero, stop iteration.
        if(tAllowablePulses == 0) break;
        tAllowablePulses --;

        ///--[Set Pulse]
        //--Remove the head pack.
        mPathRunnerList->SetRandomPointerToHead();
        mPathRunnerList->LiberateRandomPointerEntry();

        //--Check the pulse distance to make sure another pulse didn't slip in and place a shorter one here.
        if(mPathArray[rIteratingPack->mX][rIteratingPack->mY] >= 0 && mPathArray[rIteratingPack->mX][rIteratingPack->mY] < rIteratingPack->mPulseVal)
        {
            free(rIteratingPack);
            rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
            continue;
        }

        //--Mark the current tile's shortest distance to the start.
        mPulseTotal ++;
        mPathArray[rIteratingPack->mX][rIteratingPack->mY] = rIteratingPack->mPulseVal;

        ///--[Check Success]
        //--If this iterating pack hit the target, we either stop here, or keep going but mark that we found a path.
        if(rIteratingPack->mX == mEndX && rIteratingPack->mY == mEndY)
        {
            //--Indicate at least one solution has been found.
            mHasAnyPath = true;

            //--If flagged, stop iteration here.
            if(pStopOnReachingTarget)
            {
                break;
            }

            //--If we didn't stop, there is no need to spawn successors as we hit the target with this pulse.
            free(rIteratingPack);
            rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
            continue;
        }

        ///--[Spawn Successors]
        //--Check each of the cardinal directions if they can support a path move. This is done by
        //  a subroutine. If true, put the package on the end of the list.

        //--North:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_UP))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX + 0;
            nNewPackage->mY = rIteratingPack->mY - 1;
            nNewPackage->mPrevDir = DIR_UP;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget) mShortestDistance = nNewPackage->mDistToTarget;
        }

        //--South:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_DOWN))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX + 0;
            nNewPackage->mY = rIteratingPack->mY + 1;
            nNewPackage->mPrevDir = DIR_DOWN;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget) mShortestDistance = nNewPackage->mDistToTarget;
        }

        //--West:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_LEFT))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX - 1;
            nNewPackage->mY = rIteratingPack->mY + 0;
            nNewPackage->mPrevDir = DIR_LEFT;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget) mShortestDistance = nNewPackage->mDistToTarget;
        }

        //--East:
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, rIteratingPack->mPulseVal + 1, DIR_RIGHT))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = rIteratingPack->mPulseVal + 1;
            nNewPackage->mX = rIteratingPack->mX + 1;
            nNewPackage->mY = rIteratingPack->mY + 0;
            nNewPackage->mPrevDir = DIR_RIGHT;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            Pathrunner::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget) mShortestDistance = nNewPackage->mDistToTarget;
        }

        ///--[Next]
        //--Deallocate the old package.
        free(rIteratingPack);

        //--Get the new head.
        rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
    }
}
StarLinkedList *PathrunnerTile::ResolvePath()
{
    ///--[ =========== Documentation ========== ]
    //--Runs an algorithm from end to start, resolving the shortest possible path. This can be run at multiple
    //  times during iteration as the pulse information comes in.
    //--Returns a linked-list containing the movement instructions as (int *)'s. NULL on error. Caller is responsible
    //  for deallocating the list.
    if(mFailure || !mHasAnyPath) return NULL;

    //--Setup.
    StarLinkedList *nPathList = new StarLinkedList(true); //int *, master
    StarLinkedList *tTempMoveList = new StarLinkedList(true); //int *, master
    int tCurX = mEndX;
    int tCurY = mEndY;
    int tEndPathValue = mPathArray[mEndX][mEndY];

    //fprintf(stderr, "Walking from ending position %i %i to start position %i %i\n", mEndX, mEndY, mStartX, mStartY);
    //fprintf(stderr, " Expected path value: %i\n", tEndPathValue);

    ///--[ ============== Walking ============= ]
    //--Starting at the end position, "Walk" back to the start position. There should be an unbroken
    //  chain of values counting down to zero that denotes the shortest path.
    //--If multiple move cases exist, ties are broken at random. This gives some variation to the paths.
    //--If multiple move cases exist, but some are "longer" than others, the longer ones are trimmed.
    //int tCount = 0;
    while(tEndPathValue >= 1)
    {
        ///--[Move Checking]
        //--Place all adjacent moves in the tTempMoveList.
        int tLowestMove = 0x7FFF;
        tTempMoveList->ClearList();
        //fprintf(stderr, "New move. End path value: %i\n", tEndPathValue);

        //--North:
        if(IsMoveShortest(tCurX, tCurY-1, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX][tCurY-1]) tLowestMove = mPathArray[tCurX][tCurY-1];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_SOUTH;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }

        //--South:
        if(IsMoveShortest(tCurX, tCurY+1, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX][tCurY+1]) tLowestMove = mPathArray[tCurX][tCurY+1];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_NORTH;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }

        //--West:
        if(IsMoveShortest(tCurX-1, tCurY, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX-1][tCurY]) tLowestMove = mPathArray[tCurX-1][tCurY];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_EAST;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }

        //--East:
        if(IsMoveShortest(tCurX+1, tCurY, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX+1][tCurY]) tLowestMove = mPathArray[tCurX+1][tCurY];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_WEST;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }
        //fprintf(stderr, " Position: %i %i\n", tCurX, tCurY);
        //fprintf(stderr, " Lowest move: %i\n", tLowestMove);

        ///--[Trimming]
        //--Error check. No moves available results in a failure.
        if(tTempMoveList->GetListSize() == 0)
        {
            //fprintf(stderr, " No moves. Stopping.\n");
            break;
        }

        //--If there are more than one move available, trim all the ones that aren't the lowest move.
        //fprintf(stderr, " %i possible moves.\n", tTempMoveList->GetListSize());
        if(tTempMoveList->GetListSize() > 1)
        {
            //--Iterate.
            int *rMovePtr = (int *)tTempMoveList->SetToHeadAndReturn();
            while(rMovePtr)
            {
                //--Get the value at the target.
                int tTargetVal = 0;
                if(*rMovePtr == DIR_WEST)  tTargetVal = mPathArray[tCurX+1][tCurY];
                if(*rMovePtr == DIR_EAST)  tTargetVal = mPathArray[tCurX-1][tCurY];
                if(*rMovePtr == DIR_NORTH) tTargetVal = mPathArray[tCurX][tCurY+1];
                if(*rMovePtr == DIR_SOUTH) tTargetVal = mPathArray[tCurX][tCurY-1];

                //--If the target value is not the lowest value, remove this.
                if(tTargetVal > tLowestMove) tTempMoveList->RemoveRandomPointerEntry();

                //--Next.
                rMovePtr = (int *)tTempMoveList->IncrementAndGetRandomPointerEntry();
            }
        }

        //--If the trimmer somehow removed all the elements, fail.
        if(tTempMoveList->GetListSize() == 0)
        {
            break;
        }

        ///--[Path Resolve]
        //--If there's more than one move, roll which one to use at random.
        int tUseSlot = rand() % tTempMoveList->GetListSize();

        //--Liberate that element.
        int *rMovePtr = (int *)tTempMoveList->GetElementBySlot(tUseSlot);
        tTempMoveList->SetRandomPointerToThis(rMovePtr);
        tTempMoveList->LiberateRandomPointerEntry();

        //--Register it on the return list.
        nPathList->AddElementAsHead("X", rMovePtr, &FreeThis);

        //--Next.
        tEndPathValue --;

        //--Given the direction, move the cursor.
        if(     *rMovePtr == DIR_NORTH) tCurY += 1;
        else if(*rMovePtr == DIR_SOUTH) tCurY -= 1;
        else if(*rMovePtr == DIR_WEST)  tCurX += 1;
        else if(*rMovePtr == DIR_EAST)  tCurX -= 1;

        //--Debug.
        //if(     *rMovePtr == DIR_NORTH) fprintf(stderr, "--Selected move: North.\n");
        //else if(*rMovePtr == DIR_SOUTH) fprintf(stderr, "--Selected move: South.\n");
        //else if(*rMovePtr == DIR_WEST)  fprintf(stderr, "--Selected move: West.\n");
        //else if(*rMovePtr == DIR_EAST)  fprintf(stderr, "--Selected move: East.\n");


    }

    //--Clean.
    delete tTempMoveList;
    return nPathList;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void PathrunnerTile::HookToLuaState(lua_State *pLuaState)
{
    /* PathTile_RunPathing(iXStart, iYStart, iXEnd, iYEnd)
       Runs pathing between the two locations and populates the static variables with the results. */
    lua_register(pLuaState, "PathTile_RunPathing", &Hook_PathTile_RunPathing);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_PathTile_RunPathing(lua_State *L)
{
    ///--[Argument List]
    //PathTile_RunPathing(iXStart, iYStart, iXEnd, iYEnd)

    ///--[Common]
    //--Always clear this list.
    Pathrunner::xStaticPathList->ClearList();

    ///--[Arguments]
    //--Verify arguments.
    int tArgs = lua_gettop(L);
    if(tArgs < 4) return LuaArgError("PathTile_RunPathing");

    ///--[Setup]
    //--Create a pathing object and set it up.
    PathrunnerTile *tPathRunner = new PathrunnerTile();
    tPathRunner->Initialize(lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));

    //--Run into this list.
    int tAttemptsAllowed = 5;
    StarLinkedList *tResultList = tPathRunner->RunPathingIntoList();
    while(!tResultList)
    {
        //--Check for failure. If a path is impossible, stop.
        if(tPathRunner->IsFailed() || tAttemptsAllowed < 0)
        {
            break;
        }

        //--Run it again.
        tAttemptsAllowed --;
        tResultList = tPathRunner->RunPathingIntoList();
    }

    ///--[Failure Check]
    if(!tResultList)
    {
        delete tPathRunner;
        delete tResultList;
        return 0;
    }

    ///--[Success]
    //--Copy the result list into the static list. Note that if the pathrunner failed, the resulting list
    //  will have zero elements so this code will be skipped.
    tResultList->SetRandomPointerToHead();
    PathrunnerPack *rFirstPack = (PathrunnerPack *)tResultList->LiberateRandomPointerEntry();
    while(rFirstPack)
    {
        //--Register it as the tail of the static list.
        Pathrunner::xStaticPathList->AddElementAsTail("X", rFirstPack, &FreeThis);

        //--Next element.
        tResultList->SetRandomPointerToHead();
        rFirstPack = (PathrunnerPack *)tResultList->LiberateRandomPointerEntry();
    }

    ///--[Clean Up]
    delete tPathRunner;
    delete tResultList;
    return 0;
}
