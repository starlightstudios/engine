//--Base
#include "WorkshopItem.h"

//--Classes
//--CoreClasses
#include "StarFileSystem.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"
#include "SteamManager.h"

///--[Debug]
//#define WORKSHOPITEM_DEBUG
#ifdef WORKSHOPITEM_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================= No Steam =============================================
//--If Steam is not on this build, creates an empty constructor. This class should have no methods
//  and no members.
#ifndef _STEAM_API_
WorkshopItem::WorkshopItem()
{

}
WorkshopItem::~WorkshopItem()
{

}
void WorkshopItem::DeleteThis(void *pPtr)
{
    WorkshopItem *rPtr = (WorkshopItem *)pPtr;
    delete rPtr;
}
#else


///========================================== System ==============================================
WorkshopItem::WorkshopItem()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_WORKSHOPITEM;

    ///--[ ====== WorkshopItem ====== ]
    ///--[System]
    mHandle = 0;

    ///--[Upload Data]
    mUploadFolder = NULL;
    mAltFolder = NULL;
    mUploadTitle = NULL;
    mUploadDescription = NULL;
    mUploadLanguage = NULL;
    mUploadMetadata = NULL;
    mUploadChangelog = NULL;

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
WorkshopItem::~WorkshopItem()
{
    free(mUploadFolder);
    free(mAltFolder);
    free(mUploadTitle);
    free(mUploadDescription);
    free(mUploadLanguage);
    free(mUploadMetadata);
    free(mUploadChangelog);
}
void WorkshopItem::DeleteThis(void *pPtr)
{
    //--Cast and check.
    WorkshopItem *rPtr = (WorkshopItem *)pPtr;
    if(!rPtr) return;

    //--Delete.
    delete rPtr;
}

///===================================== Property Queries =========================================
bool WorkshopItem::IsUploadReady()
{
    ///--[Documentation]
    //--Check if this workshop object is ready for upload. If not, returns false. To be ready, it must
    //  have all of the text fields filled out, a folder to reference, and there must be a preview image
    //  found in the folder.
    if(!mUploadFolder) return false;
    if(!mAltFolder) return false;
    if(!mUploadTitle) return false;
    if(!mUploadDescription) return false;
    if(!mUploadLanguage) return false;
    if(!mUploadMetadata) return false;
    if(!mUploadChangelog) return false;

    //--Check if there is a preview image.
    char *tCheckPath = InitializeString("%s/PreviewImage.png", mAltFolder);
    if(!StarFileSystem::FileExists(tCheckPath))
    {
        free(tCheckPath);
        return false;
    }

    ///--[All Checks Passed]
    free(tCheckPath);
    return true;
}

///======================================= Manipulators ===========================================
void WorkshopItem::SetCreateMode(const char *pFolderPath)
{
    ///--[Documentation]
    //--Switches object into creation mode. The object will receive a callback and parse files to
    //  then create a new mod on the workshop.
    if(mHandle != 0 || !pFolderPath) return;

    ///--[Execution]
    //--Create the alternate folder path. This path needs to be adjacent to the provided folder path.
    //  For example, if the folder path is "C:/ModData/" then the alternate path is "C:/ModData_Upload/".
    //  This folder is expected to contain the workshop-only data needed by the mod.
    int tLen = (int)strlen(pFolderPath);
    char *tTempAltPath = (char *)starmemoryalloc(sizeof(char) * tLen + 8 + 1);
    strcpy(tTempAltPath, pFolderPath);

    //--Check if there are any trailing slashes. If so, remove them.
    for(int i = tLen; i >= 1; i --)
    {
        if(tTempAltPath[i] == '/' || tTempAltPath[i] == '\\')
        {
            tTempAltPath[i] = '\0';
        }
        else if(tTempAltPath[i] > 0)
        {
            break;
        }
    }
    fprintf(stderr, "Deslashes: %s\n", tTempAltPath);

    //--Append "_Upload" and use that as the path.
    strcat(tTempAltPath, "_Upload/");
    fprintf(stderr, "Alt Path: %s\n", tTempAltPath);

    //--First, check if Info.lua exists in the given folder path. If not, fail.
    char *tCheckPath = InitializeString("%s/Info.lua", tTempAltPath);
    if(!StarFileSystem::FileExists(tCheckPath))
    {
        fprintf(stderr, "WorkshopItem:SetCreateMode() - Failed. Unable to locate Info.lua in associated directory.\nChecked path: %s\n", tCheckPath);
        free(tTempAltPath);
        free(tCheckPath);
        return;
    }

    //--Store the folder path for later.
    ResetString(mUploadFolder, pFolderPath);
    ResetString(mAltFolder, tTempAltPath);

    //--Execute the lua file in question with this as the active object. The script should populate
    //  this object fully.
    LuaManager::Fetch()->PushExecPop(this, tCheckPath);

    //--Check if this object is ready for upload. If not, stop and print a warning.
    if(!IsUploadReady())
    {
        fprintf(stderr, "WorkshopItem:SetCreateMode() - Failed. Some info was not present.\n");
        fprintf(stderr, " Verify there is a preview image named PreviewImage.png\n");
        fprintf(stderr, " Pointers: %p %p %p %p %p %p %p\n", mUploadFolder, mAltFolder, mUploadTitle, mUploadDescription, mUploadLanguage, mUploadMetadata, mUploadChangelog);
        free(tTempAltPath);
        free(tCheckPath);
        return;
    }

    //--Execution.
    fprintf(stderr, "Running UGC CreateItem().\n");
    ISteamUGC *rUGC = SteamUGC();
    mHandle = SteamAPI_ISteamUGC_CreateItem(rUGC, SteamManager::xSteamAppID, k_EWorkshopFileTypeCommunity);
    //mHandle = SteamUGC()->CreateItem(SteamManager::xSteamAppID, k_EWorkshopFileTypeCommunity);
    mCreateItemResult.Set(mHandle, this, &WorkshopItem::OnItemCreated);
    fprintf(stderr, " Handle was %lu\n", mHandle);
    fprintf(stderr, " Object pointer: %p\n", this);

    //--Clean up.
    free(tCheckPath);
    free(tTempAltPath);
}
void WorkshopItem::SetUploadTitle(const char *pTitle)
{
    ResetString(mUploadTitle, pTitle);
}
void WorkshopItem::SetUploadDescription(const char *pDescription)
{
    ResetString(mUploadDescription, pDescription);
}
void WorkshopItem::SetUploadLanguage(const char *pLanguage)
{
    ResetString(mUploadLanguage, pLanguage);
}
void WorkshopItem::SetUploadMetadata(const char *pMetadata)
{
    ResetString(mUploadMetadata, pMetadata);
}
void WorkshopItem::SetUploadChangelog(const char *pChangelog)
{
    ResetString(mUploadChangelog, pChangelog);
}
void WorkshopItem::SetDeleteMode()
{
    ///--[Documentation]
    //--Switches object into deletion mode. The object will receive a callback and delete a workshop
    //  entry with the matching ID. Used for debug. End users will need to delete their mods via the
    //  steam interface.
    if(mHandle != 0) return;

    //--Execution.
    ISteamUGC *rSteamUGC = SteamUGC();
    mHandle = SteamAPI_ISteamUGC_DeleteItem(rSteamUGC, 3381304983);
    //mHandle = SteamUGC()->DeleteItem(3381304983);
    mDeleteItemResult.Set(mHandle, this, &WorkshopItem::OnItemDeleted);
}

///======================================= Core Methods ===========================================
void WorkshopItem::OnItemCreated(CreateItemResult_t *pCallback, bool pIOFailure)
{
    ///--[Documentation]
    //--Callback called by Steam whenever a new workshop object is created.
    if(pIOFailure)
    {
        fprintf(stderr, "WorkshopItem:OnItemCreated() - Warning. IO Failure.\n");
        return;
    }

    //--Callback result was not okay. Fail.
    if(pCallback->m_eResult != k_EResultOK)
    {
        fprintf(stderr, "WorkshopItem:OnItemCreated() - Warning: Callback result was not okay. Code: %i\n", pCallback->m_eResult);
        return;
    }

    ///--[Execution]
    //--Item was created.
    fprintf(stderr, "Item was created successfully.\n");
    fprintf(stderr, " Handle: %lu\n", mHandle);
    fprintf(stderr, " Publish File ID: %lu\n", pCallback->m_nPublishedFileId);

    //--It may be necessary to prompt the user to fill out the workshop legal agreement before anything can be uploaded.
    //  This will open the steam UI and prompt them.
    if(pCallback->m_bUserNeedsToAcceptWorkshopLegalAgreement)
    {
        fprintf(stderr, "User needs to do the legal agreement.\n");
        SteamAPI_ISteamFriends_ActivateGameOverlayToWebPage(SteamFriends(), "steam://url/CommunityFilePage/", k_EActivateGameOverlayToWebPageMode_Default);
        //SteamFriends()->ActivateGameOverlayToWebPage("steam://url/CommunityFilePage/");
    }

    //--Set basic information about the mod.
    fprintf(stderr, "A\n");
    ISteamUGC *rUGC = SteamUGC();
    UGCUpdateHandle_t tModHandle = SteamAPI_ISteamUGC_StartItemUpdate(rUGC, SteamManager::xSteamAppID, pCallback->m_nPublishedFileId);
    //UGCUpdateHandle_t tModHandle = SteamUGC()->StartItemUpdate(SteamManager::xSteamAppID, pCallback->m_nPublishedFileId);
    SteamAPI_ISteamUGC_SetItemTitle(rUGC, tModHandle, mUploadTitle);
    SteamAPI_ISteamUGC_SetItemDescription(rUGC, tModHandle, mUploadDescription);
    SteamAPI_ISteamUGC_SetItemUpdateLanguage(rUGC, tModHandle, mUploadLanguage);
    SteamAPI_ISteamUGC_SetItemMetadata(rUGC, tModHandle, mUploadMetadata);
    SteamAPI_ISteamUGC_SetItemVisibility(rUGC, tModHandle, k_ERemoteStoragePublishedFileVisibilityPublic);

    //UGCUpdateHandle_t tModHandle = SteamUGC()->StartItemUpdate(SteamManager::xSteamAppID, pCallback->m_nPublishedFileId);
    //SteamUGC()->SetItemTitle(tModHandle, mUploadTitle);
    //SteamUGC()->SetItemDescription(tModHandle, mUploadDescription);
    //SteamUGC()->SetItemUpdateLanguage(tModHandle, mUploadLanguage);
    //SteamUGC()->SetItemMetadata(tModHandle, mUploadMetadata);
    //SteamUGC()->SetItemVisibility(tModHandle, k_ERemoteStoragePublishedFileVisibilityPublic);
    fprintf(stderr, "B\n");

    ///--[Folder Information]
    //--The folder is where data is copied from that will be downloaded to a user's drive.
    //SteamUGC()->SetItemContent(tModHandle, mUploadFolder);
    SteamAPI_ISteamUGC_SetItemContent(rUGC, tModHandle, mUploadFolder);
    fprintf(stderr, "C\n");

    //--Determine the path to the preview. This is in the alt folder.
    char *tPreviewPath = InitializeString("%sPreviewImage.png", mAltFolder);
    SteamAPI_ISteamUGC_SetItemPreview(rUGC, tModHandle, tPreviewPath);
    //SteamUGC()->SetItemPreview(tModHandle, tPreviewPath);
    fprintf(stderr, "D\n");

    ///--[Tags]
    //--Not used in the current release.
    //SteamParamStringArray_t *pTags = new SteamParamStringArray_t();
    //pTags->m_ppStrings = new const char*[1];
    //pTags->m_ppStrings[0] = "stage";
    //pTags->m_nNumStrings = 1;
    //SteamUGC()->SetItemTags(tModHandle, pTags);
    //SteamUGC()->AddItemKeyValueTag(tModHandle, "test_key", "test_value");

    ///--[Upload]
    //--Call the upload which will run through a callback. We don't need to store the handle
    //  since the item submit callback doesn't do any processing.
    fprintf(stderr, "E\n");
    //SteamAPICall_t tSubmitItemHandle = SteamUGC()->SubmitItemUpdate(tModHandle, mUploadChangelog);
    //mSubmitItemUpdateResult.Set(tSubmitItemHandle, this, &WorkshopItem::OnItemSubmitted);
    fprintf(stderr, "F\n");
}
void WorkshopItem::OnItemDeleted(DeleteItemResult_t *pCallback, bool pIOFailure)
{
    fprintf(stderr, "Item deleted.\n");
    fprintf(stderr, " Handle: %lu\n", mHandle);
    fprintf(stderr, " Publish File ID: %lu\n", pCallback->m_nPublishedFileId);
}
void WorkshopItem::OnItemSubmitted(SubmitItemUpdateResult_t *pCallback, bool pIOFailure)
{
    ///--[Documentation]
    //--Callback from Steam upon submission of an item.
    if(pIOFailure)
    {
        fprintf(stderr, "WorkshopItem:OnItemSubmitted() - Warning. IO Failure. Stopping.\n");
        return;
    }

    //--Incorrect result.
    if(pCallback->m_eResult != k_EResultOK)
    {
        fprintf(stderr, "WorkshopItem:OnItemSubmitted() - Warning. Result was not okay. Code %i\n", pCallback->m_eResult);
        return;
    }

    ///--[Execution]
    //--All checks passed.
    fprintf(stderr, "Item has been successfully submitted.\n");
}


///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void WorkshopItem::HookToLuaState(lua_State *pLuaState)
{
    /* WorkshopItem_SetProperty("Dummy")
       Sets property inside the active WorkshopItem. */
    lua_register(pLuaState, "WorkshopItem_SetProperty", &Hook_WorkshopItem_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_WorkshopItem_GetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    ///--[Dynamic]
    //WorkshopItem_GetProperty("Dummy") (1 Integer)

    ///--[ ========= Argument Resolve ========= ]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("WorkshopItem_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_WORKSHOPITEM)) return LuaTypeError("WorkshopItem_GetProperty", L);
    WorkshopItem *rObject = (WorkshopItem *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("WI_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_WorkshopItem_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    //--[System]
    //WorkshopItem_SetProperty("Title", sTitle)
    //WorkshopItem_SetProperty("Description", sDescription)
    //WorkshopItem_SetProperty("Language", sLanguage)
    //WorkshopItem_SetProperty("Metadata", sMetadata)
    //WorkshopItem_SetProperty("Changelog", sChangelog)

    ///--[ ========= Argument Resolve ========= ]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("WorkshopItem_SetProperty");

    //--Switching.
    const char *rSWorkshopItemtchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Dummy static.
    if(!strcasecmp(rSWorkshopItemtchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_WORKSHOPITEM)) return LuaTypeError("WorkshopItem_SetProperty", L);
    WorkshopItem *rItem = (WorkshopItem *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Title.
    if(!strcasecmp(rSWorkshopItemtchType, "Title") && tArgs >= 2)
    {
        rItem->SetUploadTitle(lua_tostring(L, 2));
    }
    //--Description.
    else if(!strcasecmp(rSWorkshopItemtchType, "Description") && tArgs >= 2)
    {
        rItem->SetUploadDescription(lua_tostring(L, 2));
    }
    //--Language.
    else if(!strcasecmp(rSWorkshopItemtchType, "Language") && tArgs >= 2)
    {
        rItem->SetUploadLanguage(lua_tostring(L, 2));
    }
    //--Metadata. Optional.
    else if(!strcasecmp(rSWorkshopItemtchType, "Metadata") && tArgs >= 2)
    {
        rItem->SetUploadMetadata(lua_tostring(L, 2));
    }
    //--Changelog.
    else if(!strcasecmp(rSWorkshopItemtchType, "Changelog") && tArgs >= 2)
    {
        rItem->SetUploadChangelog(lua_tostring(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("WorkshopItem_SetProperty", rSWorkshopItemtchType, tArgs);
    }

    return 0;
}

#endif
