//Find/Replace [NEWUINAME].
//Balance the heading below
//Write a description
//Create variables/functions/hoo ha
//Do your job as a coder for once
//Delete these instructions

///======================================== [NEWUINAME] ===========================================
//--Prototype for a new UI object of StarUIPiece inheritance.

#pragma once

///========================================= Includes =============================================
#include "StarUIPiece.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================

///========================================== Classes =============================================
class [NEWUINAME] : public StarUIPiece
{
    private:
    ///--[System]
    ///--[Constants]
    static const int cVisTicks = 15;                   //Number of ticks to show/hide the object.
    static const int cHlpTicks = 15;                   //Number of ticks to show/hide the help screen.
    static const int cCurTicks = 7;                    //Number of ticks for the cursor to reposition when moved.
    static const int cHelpStringsTotal = 24;           //Number of unique StarlightStrings appearing on the help menu.

    ///--[Cursor]
    //--Indicator.
    int mCursor;

    //--Render positions.
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    ///--[Help Strings]
    StarlightString *mInstructionsStringA;

    ///--[Colors]
    StarlightColor mColorHeading;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Help;
        StarFont *rFont_Mainline;
        StarFont *rFont_Statistics;

        //--Images
        StarBitmap *rFrame_Primary;
        StarBitmap *rFrame_Help;
        StarBitmap *rHighlight_Standard;
        StarBitmap *rOverlay_BarFill;
        StarBitmap *rOverlay_BarFrame;
        StarBitmap *rOverlay_Header;
    }Images;

    protected:

    public:
    //--System
    [NEWUINAME]();
    virtual ~[NEWUINAME]();
    virtual void Construct();
    virtual void ImageDiagnostics();

    //--Public Variables
    virtual bool IsOfType(int pType);

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderHelp(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


