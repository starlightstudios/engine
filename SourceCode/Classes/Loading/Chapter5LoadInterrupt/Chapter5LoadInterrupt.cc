//--Base
#include "Chapter5LoadInterrupt.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
Chapter5LoadInterrupt::Chapter5LoadInterrupt()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======== StarLoadInterrupt ========= ]
    ///--[ ======= Chapter5LoadInterrupt ====== ]
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    mIsReady = false;
    mChristineFrames = NULL;
    mRaijuFrames = NULL;
    mBase = NULL;
    mLoading = NULL;
    for(int i = 0; i < LI_CH5_DOTS; i ++) mDots[i] = NULL;
}
Chapter5LoadInterrupt::~Chapter5LoadInterrupt()
{
    delete mChristineFrames;
    delete mRaijuFrames;
    delete mBase;
    delete mLoading;
    for(int i = 0; i < LI_CH5_DOTS; i ++) delete mDots[i];
}
void Chapter5LoadInterrupt::Construct()
{
    ///--[Documentation]
    //--Loads and verifies images associated with this object. Because this object is meant to appear during
    //  the loading of other objects, it handles its images internally, not using a conventional DataLibrary.

    ///--[Load Frames]
    //--Loading animation is a filmstrip series placed in a linked list.
    StarLumpManager *tSLM = new StarLumpManager();
    tSLM->Open("Data/Loading.slf");
    if(!tSLM->IsFileOpen()) return;

    //--Suppress the load interrupt.
    bool tOldFlag = StarBitmap::xSuppressInterrupt;
    StarBitmap::xSuppressInterrupt = true;

    //--Load.
    mChristineFrames = tSLM->GetImage("Chapter5|ChristineRun");
    mRaijuFrames     = tSLM->GetImage("Chapter5|RaijuRun");

    //--Loading Text
    mBase    = tSLM->GetImage("Chapter5|Base");
    mLoading = tSLM->GetImage("Chapter5|Loading");
    char tBuf[128];
    for(int i = 0; i < LI_CH5_DOTS; i ++)
    {
        sprintf(tBuf, "Chapter5|Dot%i", i);
        mDots[i] = tSLM->GetImage(tBuf);
    }

    //--Verify.
    mIsReady = (mChristineFrames && mRaijuFrames && mBase && mLoading);
    for(int i = 0; i < LI_CH5_DOTS; i ++)
    {
        if(!mDots[i]) mIsReady = false;
    }

    ///--[Finish Up]
    //--Set to the String Tyrant variant. If anything went wrong, the default one will be used.
    StarBitmap::xInterruptCall = &StarLoadInterrupt::Interrupt;

    //--Set this flag.
    mIsAwaitingKeypress = false;

    //--Clean up.
    delete tSLM;
    StarBitmap::xSuppressInterrupt = tOldFlag;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void Chapter5LoadInterrupt::DeallocateImages()
{
    ///--[Documentation]
    //--Deallocates and NULLs all images associated with this object.
    mIsReady = false;
    StarLoadInterrupt::DeallocateImages();

    //--Derived call.
    delete mChristineFrames;
    delete mRaijuFrames;
    delete mBase;
    delete mLoading;
    for(int i = 0; i < LI_CH5_DOTS; i ++) delete mDots[i];

    //--NULL.
    mChristineFrames = NULL;
    mRaijuFrames = NULL;
    mBase = NULL;
    mLoading = NULL;
    for(int i = 0; i < LI_CH5_DOTS; i ++) mDots[i] = NULL;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void Chapter5LoadInterrupt::Call(bool pForceRender)
{
    ///--[Documentation and Setup]
    //--Chapter 2 Load Interrupt renderer. Doesn't do anything yet.
    if(!CommonLogicUpdate(pForceRender) || !mIsReady) return;

    //--Call the renderer setup. The screen is now ready for drawing.
    CommonRendererSetup();

    ///--[Animation Constants]
    //--These are the same between all animations.
    float cRenderScale = 2.0f;
    float cWidPerFrame = 100.0f;
    float cHeiPerFrame = 100.0f;

    ///--[Christine's Animation]
    //--Compute the frame.
    int tChristineFrame = (mFrame / 1) % 40;

    //--Sizes.
    float cChristineTrueWid = mChristineFrames->GetTrueWidth();

    //--Constants.
    float cLft = ((1366.0f - 150.0f) * 0.50f) - (cWidPerFrame * 0.50f * cRenderScale);
    float cTop = 200.0f;
    float cRgt = cLft + (cWidPerFrame * cRenderScale);
    float cBot = cTop + (cHeiPerFrame * cRenderScale);
    float cTxL = (tChristineFrame + 0) * (cWidPerFrame / cChristineTrueWid);
    float cTxT = 1.0f;
    float cTxR = (tChristineFrame + 1) * (cWidPerFrame / cChristineTrueWid);
    float cTxB = 0.0f;

    //--Setup.
    mChristineFrames->Bind();

    //--Render.
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
    glEnd();

    ///--[Raiju's Animation]
    //--Compute the frame.
    int tRaijuFrame = ((mFrame+0) / mFrameDenominator) % RUN_FRAMES;

    //--Sizes.
    float cRaijuTrueWid = mRaijuFrames->GetTrueWidth();

    //--Constants.
    cLft = ((1366.0f + 150.0f) * 0.50f) - (cWidPerFrame * 0.50f * cRenderScale);
    cTop = 200.0f;
    cRgt = cLft + (cWidPerFrame * cRenderScale);
    cBot = cTop + (cHeiPerFrame * cRenderScale);
    cTxL = (tRaijuFrame + 0) * (cWidPerFrame / cRaijuTrueWid);
    cTxT = 1.0f;
    cTxR = (tRaijuFrame + 1) * (cWidPerFrame / cRaijuTrueWid);
    cTxB = 0.0f;

    //--Setup.
    mRaijuFrames->Bind();

    //--Render.
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
    glEnd();

    ///--[Loading Text]
    //--Loading text. Underlay always renders the same color.
    cLft = ((1366.0f) * 0.50f) - (mBase->GetTrueWidth() * 0.50f);
    cTop = 410.0f;
    mBase->Draw(cLft, cTop);
    mLoading->Draw(cLft, cTop);

    //--How many dots to render.
    int cDotsToRender = (mFrame / 10) % (LI_CH5_DOTS+1);
    for(int i = 0; i < cDotsToRender; i ++)
    {
        mDots[i]->Draw(cLft, cTop);
    }

    ///--[Finish Up]
    CommonRendererClean();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void Chapter5LoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_BootChapter5()
       Switches to String Tyrant mode. Pass "NULL" to switch to normal mode. */
    lua_register(pLuaState, "LI_BootChapter5", &Hook_LI_BootChapter5);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LI_BootChapter5(lua_State *L)
{
    //LI_BootChapter5()

    //--Create a new LoadInterrupt.
    Chapter5LoadInterrupt *nInterrupt = new Chapter5LoadInterrupt();
    nInterrupt->Construct();

    //--Dealloacte the old one.
    GLOBAL *rGlobal = Global::Shared();
    delete rGlobal->gLoadInterrupt;

    //--Replace.
    rGlobal->gLoadInterrupt = nInterrupt;
    return 0;
}
