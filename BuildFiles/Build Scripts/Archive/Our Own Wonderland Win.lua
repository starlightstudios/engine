-- |[ ================================ Our Own Wonderland Build ================================ ]|
--Operating System: Windows
--Library Type: Allegro/SDL/SDLFMOD
local sBuildName = "Our Own Wonderland Win"

--Check existence.
if(BP_Exists(sBuildName) == true) then return end

--Create.
BP_Create(sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../builds/Our Own Wonderland Win/")

-- |[ ========================================= Common ========================================= ]|
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

-- |[Basics]|
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

-- |[Executables]|
--Executable files.
BP_SetProperty("Register Command", "Copy File", "StarlightEngineAL.exe",      "WonderlandAL.exe")
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDL.exe",     "WonderlandSDL.exe")
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDLFMOD.exe", "WonderlandSDLFMOD.exe")
BP_SetProperty("Register Command", "Change Icon", "WonderlandAL.exe",         "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "WonderlandSDL.exe",        "BuildFiles/IconAdv.ico")
BP_SetProperty("Register Command", "Change Icon", "WonderlandSDLFMOD.exe",    "BuildFiles/IconAdv.ico")

--DLL files.
BP_SetProperty("Register Command", "Copy File", "bass.dll",    "bass.dll")
BP_SetProperty("Register Command", "Copy File", "bass_fx.dll", "bass_fx.dll")
BP_SetProperty("Register Command", "Copy File", "fmodL.dll",   "fmodL.dll")
BP_SetProperty("Register Command", "Copy File", "lua54.dll",   "lua54.dll")

-- |[Bootstraps and Counters]|
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

-- |[ =================================== Base Instructions ==================================== ]|
-- |[Text Files]|
--Relevant changelogs.
--BP_SetProperty("Register Command", "Copy File", "Changelog (Adventure).txt", "Changelog.txt")

--Credits files.
--BP_SetProperty("Register Command", "Copy File", "Credits.txt", "Credits.txt")

--Readme.
--BP_SetProperty("Register Command", "Copy File", "Readme.txt", "Readme.txt")

-- |[Configuration Files]|
--Engine and Adventure configurations.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",    "Config_Engine.lua")

-- |[Batch Files]|
--For people with audio problems.
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/ZBlockAudio Adventure.bat", "ZBlockAudio.bat")

-- |[ =================================== Engine Subfolders ==================================== ]|
-- |[Data Directory]|
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

-- |[Saves Directory]|
--Copy the controls files.
--BP_SetProperty("Register Command", "Create Directory", "Saves")
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
--BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

-- |[Shaders Directory]|
--Copy the Windows/Linux shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/WinLin", "Shaders/WinLin")

-- |[ ==================================== Game Subfolders ===================================== ]|
-- |[Game Directories]|
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../oow/Games/Wonderland",    "Games/Wonderland")

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the active object.
DL_PopActiveObject()
