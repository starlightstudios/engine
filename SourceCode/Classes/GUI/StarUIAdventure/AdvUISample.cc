//Replace AdvUISample with name of UI
//Update loading instructions with the correct type.
//Delete these instructions

//--Base
#include "AdvUISample.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
AdvUISample::AdvUISample()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUISample ======= ]
    ///--[System]
    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUISample::~AdvUISample()
{
}
void AdvUISample::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    //ResolveSeriesInto("Adventure Equipment UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",      &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUISample::TakeForeground()
{

}

///======================================= Core Methods ===========================================
void AdvUISample::RefreshMenuHelp()
{

}
void AdvUISample::RecomputeCursorPositions()
{

}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUISample::UpdateForeground(bool pCannotHandleUpdate)
{
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }
    return true;
}
void AdvUISample::UpdateBackground()
{

}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
