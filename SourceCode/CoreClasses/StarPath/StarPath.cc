//--Base
#include "StarPath.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
StarPath::StarPath()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======== StarPath ======== ]
    ///--[Variables]
    //--System
    mDirectory = NULL;
    mFilename = NULL;
    mExtension = NULL;

}
StarPath::~StarPath()
{
    free(mDirectory);
    free(mFilename);
    free(mExtension);
}

///===================================== Property Queries =========================================
const char *StarPath::GetDirectory()
{
    return mDirectory;
}
const char *StarPath::GetFilename()
{
    return mFilename;
}
const char *StarPath::GetExtension()
{
    return mExtension;
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void StarPath::ConstructFromPath(const char *pPath)
{
    ///--[Documentation]
    //--Given a string that nominally represents a file path, splits it into parts. If a part is not
    //  found it will be left as NULL.

    ///--[Clear]
    //--Deallocate.
    free(mDirectory);
    free(mFilename);
    free(mExtension);

    //--Zero.
    mDirectory = NULL;
    mFilename = NULL;
    mExtension = NULL;

    //--If no path is provided, stop.
    if(!pPath) return;

    ///--[Scanning]
    //--Scan forwards and locate the last slash and period, representing where the directory and
    //  extension stop.
    int tLastPeriod = -1;
    int tLastSlash = -1;
    int tPathLen = (int)strlen(pPath);
    for(int i = 0; i < tPathLen; i ++)
    {
        //--Slash or backslash, store it.
        if(pPath[i] == '/' || pPath[i] == '\\')
        {
            tLastSlash = i;
        }
        //--Period, store it.
        else if(pPath[i] == '.')
        {
            tLastPeriod = i;
        }
    }

    ///--[Storage]
    //--Directory. If a slash was found, everything up to and including that slash is the directory.
    int tCursor = 0;
    if(tLastSlash != -1)
    {
        mDirectory = (char *)starmemoryalloc(sizeof(char) * (tLastSlash + 2));
        strncpy(mDirectory, &pPath[tCursor], sizeof(char) * (tLastSlash + 1));
        mDirectory[tLastSlash+1] = '\0';
        tCursor = tLastSlash+1;
    }

    //--Advance the cursor so all future operations act as if there is no directory. If no directory
    //  exists then the string doesn't change.
    const char *rRemainingString = &pPath[tCursor];
    tLastPeriod = tLastPeriod - tCursor;
    tCursor = 0;

    //--If no extension was found, the entire rest of the path is the filename.
    if(tLastPeriod == -1)
    {
        mFilename = InitializeString(rRemainingString);
        return;
    }

    //--There is an extension. First, copy out the filename.
    mFilename = (char *)starmemoryalloc(sizeof(char) * (tLastPeriod+1));
    strncpy(mFilename, rRemainingString, sizeof(char) * (tLastPeriod));
    mFilename[tLastPeriod] = '\0';

    //--Copy out the extension.
    mExtension = InitializeString(&rRemainingString[tLastPeriod]);

    ///--[Diagnostics]
    if(false)
    {
        fprintf(stderr, "Report:");
        fprintf(stderr, " Path: %s\n", pPath);
        fprintf(stderr, " Directory: %s\n", mDirectory);
        fprintf(stderr, " Filename: %s\n", mFilename);
        fprintf(stderr, " Extension: %s\n", mExtension);
    }
}
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
