//--Base
#include "AdvUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///====================================== Selection Mode ==========================================
//--Handles the update when the player is selecting a piece of equipment to either check its stats
//  or to prepare to replace it. Pressing activate on a piece of equipment switches to replacement
//  mode, or if on a gem, gem replacement mode.

///======================================= Core Methods ===========================================
void AdvUIEquip::ScrollCharacter(int pAmount)
{
    ///--[Documentation]
    //--Common code fired after the player uses the shoulder buttons to change characters. The amount
    //  value should be -1 or 1, though the algorithm can handle other numbers.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AudioManager *rAudioManager = AudioManager::Fetch();

    ///--[Issue Move]
    //--Store the previous slot and run the subroutine.
    mPrevCharacterSlot = mCharacterCursor;
    bool tMoved = StandardCursorNoScroll(mCharacterCursor, pAmount, rAdvCombat->GetActivePartyCount(), true);

    //--If a move did not occur, play a fail sound.
    if(!tMoved)
    {
        rAudioManager->PlaySound("Menu|Failed");
        return;
    }

    ///--[Move Successful]
    //--A move occurred. Set the needed timers, reset cursors.
    mIsSwappingCharacter = true;
    mCharacterSwapTimer = 0;
    mGemCursor = -1;

    //--Get the character and refresh their stats.
    AdvCombatEntity *rActiveEntity = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(rActiveEntity)
    {
        //--Reset stats.
        rActiveEntity->RefreshStatsForUI();

        //--Check if the new character has fewer equipment slots than the cursor position and move if needed.
        int tSlotsTotal = rActiveEntity->GetEquipmentSlotsTotal();
        if(mEquipmentCursor >= tSlotsTotal)
        {
            mEquipmentCursor = tSlotsTotal-1;
        }
    }

    //--After checking if the cursor needs to change, recompute its position.
    RecomputeCursorPositions();
    ReresolveDetailsItem();

    //--SFX.
    rAudioManager->PlaySound("Menu|Select");
    return;
}
void AdvUIEquip::ReresolveDetailsItem()
{
    ///--[Documentation]
    //--If in details mode, this function resolves which item is under the cursor and being inspected.
    //  If the player changes the item to an invalid slot, details mode exits.
    //--If not in details mode, does nothing.
    if(!mIsShowingDetails) return;

    ///--[Equipment]
    //--Active entity must exist.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) { mIsShowingDetails = false; rDetailsItem = NULL; return; }

    //--Get the piece of equipment currently highlighted. This is not necessarily the details item. If it
    //  does not exist, exit details mode.
    AdventureItem *rSelectedItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
    if(!rSelectedItem) { mIsShowingDetails = false; rDetailsItem = NULL; return; }

    //--If the gem cursor is -1, or exceeds the gem slot count, then a piece of equipment is the details item.
    if(mGemCursor == -1) { rDetailsItem = rSelectedItem; return; }
    if(mGemCursor >= rSelectedItem->GetGemSlots()) { mGemCursor = -1; rDetailsItem = rSelectedItem; return; }

    //--A gem is highlighted. Check the gem slot.
    AdventureItem *rSelectedGem = rSelectedItem->GetGemInSlot(mGemCursor);
    if(!rSelectedGem) { mIsShowingDetails = false; rDetailsItem = NULL; return; }

    //--The gem exists. Set it as the details item.
    rDetailsItem = rSelectedGem;
}

///========================================== Update ==============================================
void AdvUIEquip::UpdateSelection()
{
    ///--[Documentation]
    //--When the player is selecting which item or gem to inspect or replace, this routine handles
    //  the input and logic. They can also switch characters here.
    //--Controls generally return out after executing, to prevent odd cases like accidentally switching
    //  items and pressing activate to activate the wrong item.

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AudioManager *rAudioManager = AudioManager::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Error Check]
    //--Retrieve the active character. The character must exist.
    AdvCombatEntity *rActiveEntity = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) return;

    ///--[Details Handler]
    //--Immediately toggles details mode on whatever is highlighted. Stops the rest of the update.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--If currently in details mode, exit details mode.
        if(mIsShowingDetails)
        {
            mIsShowingDetails = false;
            rAudioManager->PlaySound("Menu|Select");
            return;
        }

        //--Run routine to check if there is a details item. If nothing is under the cursor, this will
        //  automatically unset details mode.
        mIsShowingDetails = true;
        ReresolveDetailsItem();

        //--If details mode did not get unset by the routine, play the select sound.
        if(mIsShowingDetails)
        {
            rAudioManager->PlaySound("Menu|Select");
        }
        //--It failed, so play a failure sound.
        else
        {
            rAudioManager->PlaySound("Menu|Failed");
        }
        return;
    }

    ///--[Character Switching]
    //--Decrement the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel")) ||
       (rControlManager->IsDown("Ctrl")          && rControlManager->IsFirstPress("Left")    ))
    {
        ScrollCharacter(-1);
        return;
    }

    //--Increment the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel")) ||
       (rControlManager->IsDown("Ctrl")          &&  rControlManager->IsFirstPress("Right")  ))
    {
        ScrollCharacter(1);
        return;
    }

    ///--[Moving Up and Down]
    //--Pressing up decrements the equipment selection slot. It wraps to the last slot.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Run routine.
        if(StandardCursorNoScroll(mEquipmentCursor, -1, rActiveEntity->GetEquipmentSlotsTotal(), true))
        {
            mGemCursor = -1;
            ReresolveDetailsItem();
            RecomputeCursorPositions();
        }

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Pressing down increments the equipment selection slot. It wraps to the first slot.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Run routine.
        if(StandardCursorNoScroll(mEquipmentCursor, 1, rActiveEntity->GetEquipmentSlotsTotal(), true))
        {
            mGemCursor = -1;
            ReresolveDetailsItem();
            RecomputeCursorPositions();
        }

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Selecting Gems]
    //--Pressing Right increments the gem cursor. It wraps around to -1. Holding down Ctrl when doing this will
    //  increment and decrement the character instead and then return out, so it is not handled.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Check the item in this slot. If empty, do nothing.
        AdventureItem *rEquipment = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
        if(!rEquipment) return;

        //--Item in slot. Check if it has a gem slot.
        int tGemSlots = rEquipment->GetGemSlots();
        if(tGemSlots < 1) return;

        //--Wrap check. The gem cursor wraps to -1 (no gem selected) rather than 0.
        mGemCursor ++;
        if(mGemCursor >= tGemSlots) mGemCursor = -1;

        //--Recheck cursor.
        ReresolveDetailsItem();
        RecomputeCursorPositions();

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Pressing Left decrements the gem cursor. It wraps to the max gems on the given item.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Check the item in this slot. If empty, do nothing.
        AdventureItem *rEquipment = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
        if(!rEquipment) return;

        //--Item in slot. Check if it has a gem slot.
        int tGemSlots = rEquipment->GetGemSlots();
        if(tGemSlots < 1) return;

        //--Wrap check.
        mGemCursor --;
        if(mGemCursor < -1) mGemCursor = tGemSlots - 1;

        //--Recheck cursor.
        ReresolveDetailsItem();
        RecomputeCursorPositions();

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Activate]
    //--Begin replacing the equipment or gem in question.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If the user is in details mode, do nothing.
        if(mIsShowingDetails) return;

        //--Run subroutine to check if replacements mode can be activated. If it returns false, fail.
        if(!ActivateReplacement())
        {
            rAudioManager->PlaySound("Menu|Failed");
            return;
        }

        //--Success. Play a sound and recompute cursor position.
        RecomputeCursorPositions();
        rAudioManager->PlaySound("Menu|Select");
        return;
    }

    ///--[Cancel]
    //--Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--If in details mode, exit details mode.
        if(mIsShowingDetails)
        {
            mIsShowingDetails = false;
            rAudioManager->PlaySound("Menu|Select");
            return;
        }

        //--Otherwise, mark this object as exiting.
        FlagExit();
        //SetToMainMenu(AM_MAIN_EQUIPMENT);
        rAudioManager->PlaySound("Menu|Select");
        return;
    }
}
