//--Base
#include "StarUIPopup.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== System ==============================================
StarUIPopup::StarUIPopup()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ====== StarUIPopup ======= ]
    ///--[System]
    mEntries = new StarLinkedList(true);

    ///--[Position]
    mVisTimer = 0;
    mVisTimerMax = 1;
    mHBuf = 0.0f;
    mVBuf = 0.0f;
    mVPerEntry = 1.0f;
    mDimensions.SetWH(0.0f, 0.0f, 1.0f, 1.0f);

    ///--[Display]
    mStencilCode = 1;
    mImagesReady = false;
    rFont = NULL;
    rNineFrame = NULL;

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
StarUIPopup::~StarUIPopup()
{
    delete mEntries;
}

///===================================== Property Queries =========================================
int StarUIPopup::GetTotalEntries()
{
    return mEntries->GetListSize();
}

///======================================= Manipulators ===========================================
void StarUIPopup::SetVisTimerMax(int pMax)
{
    mVisTimerMax = pMax;
    if(mVisTimerMax < 1) mVisTimerMax = 1;
}
void StarUIPopup::SetStencilCode(int pCode)
{
    mStencilCode = pCode;
}
void StarUIPopup::SetFontS(const char *pDLFontName)
{
    rFont = DataLibrary::Fetch()->GetFont(pDLFontName);
    mImagesReady = (rFont && rNineFrame);
}
void StarUIPopup::SetFrameImageS(const char *pDLPath)
{
    rNineFrame = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mImagesReady = (rFont && rNineFrame);
}
void StarUIPopup::SetFontP(StarFont *pFont)
{
    rFont = pFont;
    mImagesReady = (rFont && rNineFrame);
}
void StarUIPopup::SetFrameImageP(StarBitmap *pImage)
{
    rNineFrame = pImage;
    mImagesReady = (rFont && rNineFrame);
}
void StarUIPopup::SetPosition(float pX, float pY)
{
    mDimensions.SetPosition(pX, pY);
}
void StarUIPopup::SetBuffers(float pHBuf, float pVBuf)
{
    mHBuf = pHBuf;
    mVBuf = pVBuf;
}
void StarUIPopup::ClearEntries()
{
    mEntries->ClearList();
}
void StarUIPopup::RegisterEntry(const char *pName, const char *pDisplay)
{
    ///--[Documentation]
    //--Registers a new entry, which is a display name string. The pName string is used for internal
    //  modification and sorting. If the string "AUTO" is provided then the entry will receive a
    //  name based on the current list size, and if all entries are "AUTO" then they will be sorted
    //  in the order they were created.
    //--Duplicates are allowed, it is up to the coder to prevent them from occurring if not desired.
    if(!pName || !pDisplay) return;

    ///--[Execution]
    //--Copy the display name.
    char *nDisplayName = InitializeString(pDisplay);

    //--Check if the name is "AUTO". If it is, generate a name.
    if(!strcmp(pName, "AUTO"))
    {
        //--Generate a name based on the length of the list.
        char tBuf[64];
        sprintf(tBuf, "%02i", mEntries->GetListSize());

        //--Register.
        mEntries->AddElement(tBuf, nDisplayName, &FreeThis);
        return;
    }

    //--Normal name case:
    mEntries->AddElement(pName, nDisplayName, &FreeThis);
}

///======================================= Core Methods ===========================================
void StarUIPopup::FinalizeList()
{
    ///--[Documentation]
    //--Once all setup is completed and all entries are registered, the list needs to be sorted and
    //  repositioned in case it went offscreen.
    if(!mImagesReady)
    {
        fprintf(stderr, "StarUIPopup::FinalizeList() - Failed. Images did not resolve. Font %p, Bitmap %p\n", rFont, rNineFrame);
        return;
    }

    //--If there are no entries, stop here.
    if(mEntries->GetListSize() < 1) return;

    ///--[List Handling]
    //--Sort the list by name.
    mEntries->SortList(false);

    //--Size per entry is based on the font.
    mVPerEntry = rFont->GetTextHeight();
    if(mVPerEntry < 1) mVPerEntry = 1;

    //--Run across the display names and get the widest one.
    float tWidestString = 0.0f;
    char *rDisplayString = (char *)mEntries->PushIterator();
    while(rDisplayString)
    {
        //--Get and compare length.
        float cStringWid = rFont->GetTextWidth(rDisplayString);
        if(cStringWid > tWidestString) tWidestString = cStringWid;

        //--Next.
        rDisplayString = (char *)mEntries->AutoIterate();
    }

    ///--[Size and Position]
    //--Size the object, including buffers.
    mDimensions.SetWH(mDimensions.mLft, mDimensions.mTop, (mHBuf * 2.0f) + tWidestString, (mVBuf * 2.0f) + (mEntries->GetListSize() * mVPerEntry));

    //--Make sure the object did not go offscreen to the right/bottom.
    if(mDimensions.mRgt >= VIRTUAL_CANVAS_X)
    {
        mDimensions.SetPosition(VIRTUAL_CANVAS_X - mDimensions.GetWidth(), mDimensions.mTop);
    }
    if(mDimensions.mBot >= VIRTUAL_CANVAS_Y)
    {
        mDimensions.SetPosition(mDimensions.mLft, VIRTUAL_CANVAS_Y - mDimensions.GetHeight());
    }

    //--Make sure the object did not go offscreen to the left/top. This is done after the right/bottom check so that if
    //  a popup is somehow bigger than the whole screen at least the top left is visible.
    if(mDimensions.mLft < 0)
    {
        mDimensions.SetPosition(0, mDimensions.mTop);
    }
    if(mDimensions.mTop < 0)
    {
        mDimensions.SetPosition(mDimensions.mLft, 0);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
const char *StarUIPopup::UpdateActive()
{
    ///--[Documentation]
    //--Update that executes when this popup is considered the active object. It is treated like a
    //  regular UI object, including running timers and is capable of returning strings to indicate
    //  how it handled the input.
    //--If the string "NOTHING" is returned, then nothing happened.
    //--If the string "CLOSE" is returned, then close the popup.
    //--If the string "CLOSE OUT" is returned, then close the popup because a click hit outside of it.
    //--If the string "NO HIT" is returned, then the user clicked the popup in its frame area.
    //--Otherwise, the popup will return the name of the clicked entry, which can be queried if more
    //  is needed about it.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--The object runs its visibility timer up to full.
    if(mVisTimer < mVisTimerMax) mVisTimer ++;

    ///--[Mouse Handling]
    //--Get the X/Y of the mouse.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    ///--[Left Click]
    //--Left click, can close the object or report a hit response.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Outside: If the click is outside the object report a "CLOSE OUT" action.
        if(!mDimensions.IsPointWithin(tMouseX, tMouseY)) return "CLOSE OUT";

        //--The click is on the object. Check if it's on the object's frame.
        float tRelX = tMouseX - mDimensions.mLft;
        float tRelY = tMouseY - mDimensions.mTop;
        if(tRelX <  mHBuf)                           return "NO HIT";
        if(tRelY <  mVBuf)                           return "NO HIT";
        if(tRelX >= mDimensions.GetWidth()  - mHBuf) return "NO HIT";
        if(tRelY >= mDimensions.GetHeight() - mVBuf) return "NO HIT";

        //--The click is on the list of strings.
        tRelY = tRelY - mVBuf;
        int tEntryClicked = tRelY / mVPerEntry;

        //--If the click somehow went outside the list range, return the close order.
        if(tEntryClicked < 0 || tEntryClicked >= mEntries->GetListSize()) return "CLOSE";

        //--Fetch and return.
        const char *rReturn = (const char *)mEntries->GetNameOfElementBySlot(tEntryClicked);
        return rReturn;
    }

    ///--[Right Click]
    //--Always closes the object.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        return "CLOSE";
    }

    ///--[No Click]
    //--Return the string "NOTHING" to indicate nothing happened.
    return "NOTHING";
}
void StarUIPopup::UpdateInactive()
{
    ///--[Documentation]
    //--Update that executes when the popup is not considered the active object. Runs its timer down
    //  to zero.
    if(mVisTimer > 0) mVisTimer --;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void StarUIPopup::Render()
{
    ///--[Documentation]
    //--Renders the popup object, including its frame and entries. Does nothing if the vis timer
    //  is zero or the images failed to resolve.
    if(mVisTimer < 1 || mImagesReady == false) return;

    ///--[Stencils]
    //--If the vis timer is not capped, activate stencilling.
    if(mVisTimer < mVisTimerMax)
    {
        //--Switch to masking rendering.
        DisplayManager::ActivateMaskRender(mStencilCode);

        //--Render a block to represent where the frame can be seen.
        float cPct = EasingFunction::QuadraticInOut(mVisTimer, mVisTimerMax);
        float tBottom = mDimensions.mTop + (mDimensions.GetHeight() * cPct);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glVertex2f(mDimensions.mLft, mDimensions.mTop);
            glVertex2f(mDimensions.mRgt, mDimensions.mTop);
            glVertex2f(mDimensions.mRgt, tBottom);
            glVertex2f(mDimensions.mLft, tBottom);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);

        //--Activate stencilling for further rendering.
        DisplayManager::ActivateStencilRender(mStencilCode);
    }

    ///--[Backing]
    //--Position setup.
    float cLft = mDimensions.mLft;
    float cRgt = mDimensions.mRgt;
    float cMLf = cLft + mHBuf;
    float cMRt = cRgt - mHBuf;
    float cTop = mDimensions.mTop;
    float cBot = mDimensions.mBot;
    float cMTp = cTop + mVBuf;
    float cMBt = cBot - mVBuf;

    //--Texture setup.
    float cTxWid = rNineFrame->GetWidth();
    float cTxHei = rNineFrame->GetHeight();
    float cTxMLf = mHBuf / cTxWid;
    float cTxMRt = (cTxWid - mHBuf) / cTxWid;
    float cTxMTp = mVBuf / cTxHei;
    float cTxMBt = (cTxHei - mVBuf) / cTxHei;
    rNineFrame->RenderNine(cLft, cMLf, cMRt, cRgt, cTop, cMTp, cMBt, cBot, cTxMLf, cTxMRt, cTxMTp, cTxMBt);

    ///--[Entries]
    //--Setup.
    float tYPos = mDimensions.mTop + mVBuf;

    //--Run across the strings and render them.
    char *rDisplayString = (char *)mEntries->PushIterator();
    while(rDisplayString)
    {
        //--Get and compare length.
        rFont->DrawText(mDimensions.mLft + mHBuf, tYPos, 0, 1.0f, rDisplayString);

        //--Next.
        tYPos = tYPos + mVPerEntry;
        rDisplayString = (char *)mEntries->AutoIterate();
    }

    ///--[Clean]
    //--Deactivate stencils.
    if(mVisTimer < mVisTimerMax)
    {
        DisplayManager::DeactivateStencilling();
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
