///==================================== AudioExec_PlaySound =======================================
//--Plays the requested sound.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "AudioManager.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AudioExec_PlaySound : public RootExecutor
{
    private:
    //--System
    char *mSoundName;

    protected:

    public:
    //--System
    AudioExec_PlaySound()
    {
        mSoundName = NULL;
    }
    virtual ~AudioExec_PlaySound()
    {
        free(mSoundName);
    }

    //--Manipulators
    void SetSoundName(const char *pName)
    {
        ResetString(mSoundName, pName);
    }

    //--Executor
    virtual void Execute()
    {
        AudioManager::Fetch()->PlaySound(mSoundName);
    }
};
