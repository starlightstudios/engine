//--Base
#include "CarnUIRegem.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"

///--[Debug]
//#define DEBUG_CARNUI_REGEM
#ifdef DEBUG_CARNUI_REGEM
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
CarnUIRegem::CarnUIRegem()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUIREGEM;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CRGM");

    ///--[Visiblity]
    mVisibilityTimerMax = 1;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== CarnUIRegem ======= ]
    ///--[System]
    rConsideredItem = NULL;
    mExitsToInventory = false;
    mExitsToEquipment = false;

    ///--[Disassembly]
    mIsDisassembleMode = false;

    ///--[Selection]
    mSelectionCursor = -1;
    mSelectionTimer = 0;

    ///--[Description]
    rLastDescriptionObject = NULL;
    mDescription = new StarLinkedList(true);

    ///--[Replacements]
    mReplacementSkip = 0;
    mrReplacementList = new StarLinkedList(false);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    ///--[Hitboxes]
    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 1108.0f, 137.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollBd", 1108.0f, 195.0f, 36.0f, 438.0f);
    RegisterHitboxWH("ScrollDn", 1108.0f, 644.0f, 36.0f,  58.0f);

    //--Selection hitboxes.
    float cTxtH = 26.0f;
    RegisterHitboxWH("Select0", 255.0f, 130.0f + (cTxtH * 0.0f), 250.0f, cTxtH);
    RegisterHitboxWH("Select1", 255.0f, 130.0f + (cTxtH * 1.0f), 250.0f, cTxtH);
    RegisterHitboxWH("Select2", 255.0f, 130.0f + (cTxtH * 2.0f), 250.0f, cTxtH);
    RegisterHitboxWH("Select3", 255.0f, 130.0f + (cTxtH * 3.0f), 250.0f, cTxtH);
    RegisterHitboxWH("Select4", 255.0f, 130.0f + (cTxtH * 4.0f), 250.0f, cTxtH);
    RegisterHitboxWH("Select5", 255.0f, 130.0f + (cTxtH * 5.0f), 250.0f, cTxtH);

    //--Replacement hitboxes.
    cTxtH = 26.0f;
    for(int i = 0; i < cxCarnScrollPage; i ++)
    {
        char tBuf[80];
        sprintf(tBuf, "Replace%02i", i);
        RegisterHitboxWH(tBuf, 700.0f, 135.0f + (cTxtH * i), 390.0f, cTxtH);
    }

    //--Buttons.
    RegisterHitboxWH("Disassemble", 540.0f, 78.0f, 128.0f, 50.0f);

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mReplacementSkip;
    nScrollbarPack->mPerPage = cxCarnScrollPage;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxHitbox_ScrollUp;
    nScrollbarPack->mIndexBd = cxHitbox_ScrollBd;
    nScrollbarPack->mIndexDn = cxHitbox_ScrollDn;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;
}
CarnUIRegem::~CarnUIRegem()
{
    delete mDescription;
    delete mrReplacementList;
}
void CarnUIRegem::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation Regem UI",  &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI", &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
bool CarnUIRegem::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_CARNUIREGEM) return true;
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    return (pType == mType);
}

///======================================= Manipulators ===========================================
void CarnUIRegem::TakeForeground()
{
    ///--[Documentation]
    //--Called when the object is first opened. Resets flags and rebuilds the replacement item list.
    mSelectionCursor = -1;
    mIsDisassembleMode = false;
    SetDescription(NULL);
    BuildReplacementList();
}
void CarnUIRegem::SetExitToInventory(bool pFlag)
{
    mExitsToInventory = pFlag;
}
void CarnUIRegem::SetExitToEquipment(bool pFlag)
{
    mExitsToEquipment = pFlag;
}
void CarnUIRegem::SetConsideredItem(AdventureItem *pItem)
{
    rConsideredItem = pItem;
}
void CarnUIRegem::HandleExit()
{
    ///--[Documentation]
    //--This object can exit to multiple different UIs depending on flags. That is handled here.

    ///--[Execution]
    //--Always flag exit.
    FlagExit();

    //--If set to go back to another UI, handle that.
    if(mExitsToInventory)
    {
        mCodeBackward = CARNMENU_MODE_INVENTORY + CARNMENU_CHANGEMODE_OFFSET;
    }
    else if(mExitsToEquipment)
    {
        mCodeBackward = CARNMENU_MODE_EQUIP + CARNMENU_CHANGEMODE_OFFSET;
    }

    //--Clear flags.
    mExitsToInventory = false;
    mExitsToEquipment = false;
}

///======================================= Core Methods ===========================================
void CarnUIRegem::RefreshMenuHelp()
{
}
void CarnUIRegem::RecomputeCursorPositions()
{
}
void CarnUIRegem::SetDescription(AdventureItem *pHighlightedItem)
{
    ///--[Documentation]
    //--Uses Subdivide() to break up an item's description for display. If the highlight item is the
    //  same as the last description item, do nothing.
    if(pHighlightedItem == rLastDescriptionObject) return;

    //--Deallocate.
    delete mDescription;

    //--Zero.
    rLastDescriptionObject = NULL;
    mDescription = NULL;

    //--Error check.
    if(!pHighlightedItem) return;

    ///--[Execution]
    //--Store.
    rLastDescriptionObject = pHighlightedItem;

    //--Set.
    const char *rDescription = pHighlightedItem->GetDescription();
    mDescription = Subdivide::SubdivideStringFlags(rDescription, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, CarnImages.rFont_Mainline, 440.0f);
}
void CarnUIRegem::BuildReplacementList()
{
    ///--[Documentation]
    //--Run across the inventory and locate every unequipped modification item. Put them all on a list.
    mrReplacementList->ClearList();

    ///--[Execution]
    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    if(!rInventory) return;

    //--Item list.
    StarLinkedList *rItemList = rInventory->GetItemList();
    if(!rItemList) return;

    //--Iterate.
    AdventureItem *rCheckItem = (AdventureItem *)rItemList->PushIterator();
    while(rCheckItem)
    {
        //--Item is a gem. Color is ignored.
        if(rCheckItem->IsGem())
        {
            mrReplacementList->AddElementAsTail("X", rCheckItem);
        }

        //--Next.
        rCheckItem = (AdventureItem *)rItemList->AutoIterate();
    }

    //--Recheck scrollbars.
    ScrollbarPack *rScrollbar = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
    if(rScrollbar)
    {
        rScrollbar->mMaxSkip = mrReplacementList->GetListSize() - cxCarnScrollPage;
        if(rScrollbar->mMaxSkip < 0) rScrollbar->mMaxSkip = 0;
    }
}
void CarnUIRegem::ExecuteReplacement(int pSelectionSlot, AdventureItem *pReplacement)
{
    ///--[Documentation]
    //--Places the given item into the selected gem slot. This will then require the replacements list
    //  to be rebuilt, and the item in question will be removed from the inventory.
    if(!rConsideredItem || pSelectionSlot < 0 || pSelectionSlot >= rConsideredItem->GetGemSlots() || !pReplacement) return;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    if(!rInventory) return;

    //--Item list.
    StarLinkedList *rItemList = rInventory->GetItemList();
    if(!rItemList) return;

    ///--[Execution]
    //--First, check the gem slot. If there is an item in the gem slot already, take it out and destroy it.
    //  Gems are lost if they are overwritten.
    AdventureItem *rItemInSlot = rConsideredItem->GetGemInSlot(pSelectionSlot);
    if(rItemInSlot)
    {
        //--Remove it from the slot. We now have ownership of the gem.
        rConsideredItem->RemoveGemFromSlot(pSelectionSlot);

        //--Delete the gem.
        delete rItemInSlot;
    }

    //--Place the replacement into the slot.
    rConsideredItem->PlaceGemInSlot(pSelectionSlot, pReplacement);

    //--Order the inventory to liberate the gem.
    rItemList->SetRandomPointerToThis(pReplacement);
    rItemList->LiberateRandomPointerEntry();

    //--Rebuild the replacements list.
    BuildReplacementList();

    //--Order the item to recompute its stats.
    rConsideredItem->ComputeStatistics();
}
void CarnUIRegem::ExecuteDisassembly(int pSelectionSlot)
{
    ///--[Documentation]
    //--Disassembles the considered item, deleting it and all its gems except the one in the provided slot.
    //  Nominally the slot should not be empty but this routine doesn't care about that.
    if(!rConsideredItem) return;

    //--Fast-access pointers.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    if(!rInventory || !rAdvCombat) return;

    ///--[Liberate]
    //--Get the gem out of the slot.
    AdventureItem *rItemInSlot = rConsideredItem->GetGemInSlot(pSelectionSlot);
    if(rItemInSlot)
    {
        //--Remove it from the slot. We now have ownership of the gem.
        rConsideredItem->RemoveGemFromSlot(pSelectionSlot);

        //--Send it to the inventory.
        rInventory->RegisterItem(rItemInSlot);
    }

    ///--[Delete]
    //--Run across party members, checking if any of them have the item equipped. If they do, unequip it.
    StarLinkedList *rActivePartyList = rAdvCombat->GetActivePartyList();
    AdvCombatEntity *rEntity = (AdvCombatEntity *)rActivePartyList->PushIterator();
    while(rEntity)
    {
        //--Iterate across equipment slots.
        int tEquipSlotsTotal = rEntity->GetEquipmentSlotsTotal();
        for(int i = 0; i < tEquipSlotsTotal; i ++)
        {
            //--Get the item.
            AdventureItem *rCompareItem = rEntity->GetEquipmentBySlotI(i);
            if(rCompareItem == rConsideredItem)
            {
                const char *pSlotName = rEntity->GetNameOfEquipmentSlot(i);
                rEntity->EquipItemToSlot(pSlotName, (AdventureItem *)AdventureInventory::xrDummyUnequipItem);
            }
        }

        //--Next.
        rEntity = (AdvCombatEntity *)rActivePartyList->AutoIterate();
    }

    //--Order the inventory to liberate the considered item. If the item was an equipped item, unequipping
    //  it should dump it back to the inventory.
    rInventory->LiberateItemP(rConsideredItem);

    //--Now the item is owned by this function. Delete it.
    delete rConsideredItem;

    ///--[Finish]
    //--Clear pointers.
    rConsideredItem = NULL;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIRegem::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updates when this is the active object.
    DebugPush(true, "CarnUIRegem:UpdateForeground() - Begin.");
    if(pCannotHandleUpdate)
    {
        UpdateBackground();
        DebugPop("Completed as background update.\n");
        return false;
    }

    //--If there is no considered item, fail.
    if(!rConsideredItem)
    {
        FlagExit();
        DebugPop("Completed, no considered item. Flagged exit.\n");
        return true;
    }

    ///--[Timers]
    //--Immediately cap the vis timer.
    mVisibilityTimer = mVisibilityTimerMax;

    //--Diagnostics.
    DebugPrint("Handled timers.\n");

    ///--[Sub-Updates]
    if(mIsDisassembleMode)
    {
        DebugPrint("Running disassembly.\n");
        UpdateDisassemblyMode();
    }
    else if(mSelectionCursor == -1)
    {
        DebugPrint("Running selection.\n");
        UpdateSelectionMode();
    }
    else
    {
        DebugPrint("Running replacement.\n");
        UpdateReplacementMode();
    }

    ///--[Finish Up]
    DebugPop("Completed normally.\n");
    return true;
}
void CarnUIRegem::UpdateSelectionMode()
{
    ///--[Documentation]
    //--In selection mode, the player can click a gem slot to select it.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Mouse Handling]
    //--Get mouse information.
    GetMouseInfo();
    RunHitboxCheck();

    //--Scrollbar. Updates before clicks.
    if(UpdateScrollbars()) return;

    //--Mouse moved, check for mouse over.
    if(mMouseMoved)
    {
        //--Setup.
        AdventureItem *rNewDescription = NULL;

        //--Get replies within the hitboxes. Selection takes priority.
        int tSelectReply = CheckRepliesWithin(cxHitbox_SelectionLo, cxHitbox_SelectionHi);
        int tReplaceReply = CheckRepliesWithin(cxHitbox_ReplaceLo, cxHitbox_ReplaceHi);
        if(tSelectReply != -1)
        {
            rNewDescription = rConsideredItem->GetGemInSlot(tSelectReply - cxHitbox_SelectionLo);
        }
        else if(tReplaceReply != -1)
        {
            rNewDescription = (AdventureItem *)mrReplacementList->GetElementBySlot(tReplaceReply - cxHitbox_ReplaceLo + mReplacementSkip);
        }

        //--If the class cursor changed, play a sound effect.
        if(rNewDescription != rLastDescriptionObject)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Set description case.
        SetDescription(rNewDescription);
    }

    ///--[Left Click Handling]
    //--Select a gem slot.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Check for a hit on selections:
        int tSelectionReply = CheckRepliesWithin(cxHitbox_SelectionLo, cxHitbox_SelectionHi);
        if(tSelectionReply != -1)
        {
            mSelectionCursor = tSelectionReply - cxHitbox_SelectionLo;
            mSelectionTimer = 0;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Check for a hit on disassembly.
        int tDisassemblyReply = CheckRepliesWithin(cxHitbox_Disassemble, cxHitbox_Disassemble);
        if(tDisassemblyReply != -1)
        {
            mIsDisassembleMode = true;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Stop update.
        return;
    }

    ///--[Right Click Handling]
    //--Right-click exits the UI.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        HandleExit();
        return;
    }
}
void CarnUIRegem::UpdateReplacementMode()
{
    ///--[Documentation]
    //--In replacement mode, the player can click a replacement gem to socket into the selected slot.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    mSelectionTimer ++;

    ///--[Mouse Handling]
    //--Get mouse information.
    GetMouseInfo();
    RunHitboxCheck();

    //--Scrollbar. Updates before clicks.
    if(UpdateScrollbars()) return;

    //--Mouse moved, check for mouse over.
    if(mMouseMoved)
    {
        //--Setup.
        AdventureItem *rNewDescription = NULL;

        //--Iterate across the reply packs.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--If it's in the selection range:
            if(rReplyPack->mIndex >= cxHitbox_SelectionLo && rReplyPack->mIndex <= cxHitbox_SelectionHi)
            {
                rNewDescription = rConsideredItem->GetGemInSlot(rReplyPack->mIndex - cxHitbox_SelectionLo);
            }
            //--If it's on the replacement list, that can set the description.
            else if(rReplyPack->mIndex >= cxHitbox_ReplaceLo && rReplyPack->mIndex <= cxHitbox_ReplaceHi)
            {
                rNewDescription = (AdventureItem *)mrReplacementList->GetElementBySlot(rReplyPack->mIndex - cxHitbox_ReplaceLo + mReplacementSkip);
            }

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }

        //--If the class cursor changed, play a sound effect.
        if(rNewDescription != rLastDescriptionObject)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Set description case.
        SetDescription(rNewDescription);
    }

    ///--[Left Click Handling]
    //--Select a gem to replace with:
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Iterate across the reply packs.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--If it's in the replacement range:
            if(rReplyPack->mIndex >= cxHitbox_ReplaceLo && rReplyPack->mIndex <= cxHitbox_ReplaceHi)
            {
                //--Get the item.
                AdventureItem *rItem = (AdventureItem *)mrReplacementList->GetElementBySlot(rReplyPack->mIndex - cxHitbox_ReplaceLo + mReplacementSkip);

                //--Execute. Note that this will rebuild the replacement list.
                ExecuteReplacement(mSelectionCursor, rItem);

                //--Unset selection mode.
                mSelectionCursor = -1;

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }

        //--Stop update.
        return;
    }

    ///--[Right Click Handling]
    //--Right-click exits selection mode.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        //--Flag.
        mSelectionCursor = -1;

        //--Stop update.
        return;
    }
}
void CarnUIRegem::UpdateDisassemblyMode()
{
    ///--[Documentation]
    //--In disassembly mode, left-clicking a gem slot keeps the gem in that slot. This exits the UI
    //  and deletes the item and all other gems, keeping the one selected.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    mSelectionTimer ++;

    ///--[Mouse Handling]
    //--Get mouse information.
    GetMouseInfo();
    RunHitboxCheck();

    //--Scrollbar. Updates before clicks.
    if(UpdateScrollbars()) return;

    //--Mouse moved, check for mouse over.
    if(mMouseMoved)
    {
        //--Setup.
        AdventureItem *rNewDescription = NULL;

        //--Iterate across the reply packs.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--If it's in the selection range:
            if(rReplyPack->mIndex >= cxHitbox_SelectionLo && rReplyPack->mIndex <= cxHitbox_SelectionHi)
            {
                rNewDescription = rConsideredItem->GetGemInSlot(rReplyPack->mIndex - cxHitbox_SelectionLo);
            }
            //--If it's on the replacement list, that can set the description.
            else if(rReplyPack->mIndex >= cxHitbox_ReplaceLo && rReplyPack->mIndex <= cxHitbox_ReplaceHi)
            {
                rNewDescription = (AdventureItem *)mrReplacementList->GetElementBySlot(rReplyPack->mIndex - cxHitbox_ReplaceLo + mReplacementSkip);
            }

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }

        //--If the class cursor changed, play a sound effect.
        if(rNewDescription != rLastDescriptionObject)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Set description case.
        SetDescription(rNewDescription);
    }

    ///--[Left Click Handling]
    //--Select a gem to keep after disassembly.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Iterate across the reply packs.
        HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
        while(rReplyPack)
        {
            //--If it's in the selection range:
            if(rReplyPack->mIndex >= cxHitbox_SelectionLo && rReplyPack->mIndex <= cxHitbox_SelectionHi)
            {
                //--Run disassembly.
                ExecuteDisassembly(rReplyPack->mIndex - cxHitbox_SelectionLo);

                //--Exit.
                HandleExit();

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }

            //--Next.
            rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
        }

        //--Stop update.
        return;
    }

    ///--[Right Click Handling]
    //--Right-click exits selection mode.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        //--Flag.
        mIsDisassembleMode = false;

        //--Stop update.
        return;
    }
}
void CarnUIRegem::UpdateBackground()
{
    mVisibilityTimer = 0;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIRegem::RenderPieces(float pAlpha)
{
    ///--[Documentation]
    //--Renders the considered item, its modifications, description, and replacements.
    DebugPush(true, "CarnUIRegem:RenderPieces() - Begin.");
    if(!rConsideredItem)
    {
        DebugPop("Completed. No considered item.\n");
        return;
    }

    ///--[Base]
    //--Diagnostics.
    DebugPrint("Rendering base components.\n");

    //--Render.
    CarnImages.rFrame_Base->Draw();

    ///--[Current Status]
    //--Diagnostics.
    DebugPrint("Rendering item current properties.\n");

    //--Render the considered item's name.
    CarnImages.rFont_Heading->DrawTextArgs(245.0f, 90.0f, 0, 1.0f, "%s", rConsideredItem->GetDisplayName());

    //--Render gem slots.
    int tGemSlots = rConsideredItem->GetGemSlots();
    for(int i = 0; i < tGemSlots; i ++)
    {
        //--Get the gem.
        AdventureItem *rGemInSlot = rConsideredItem->GetGemInSlot(i);

        //--Get positions.
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxHitbox_SelectionLo + i);

        //--Slot is empty:
        if(!rGemInSlot)
        {
            CarnImages.rFont_Heading->DrawText(rHitbox->mLft, rHitbox->mTop, 0, 1.0f, "(Empty Slot)");
        }
        else
        {
            StarBitmap *rGemImg = rGemInSlot->GetIconImage();
            if(rGemImg) rGemImg->Draw(rHitbox->mLft, rHitbox->mTop + 4);
            CarnImages.rFont_Heading->DrawText(rHitbox->mLft+26, rHitbox->mTop, 0, 1.0f, rGemInSlot->GetDisplayName());
        }

        //--If the slot is selected:
        if(i == mSelectionCursor && mSelectionTimer % 4 < 2)
        {
            CarnImages.rOverlay_Cursor->Draw(rHitbox->mLft - 15.0f, rHitbox->mTop);
        }
    }

    ///--[Object Description]
    //--Diagnostics.
    DebugPrint("Rendering description.\n");

    //--If there is a description item, render its name and description.
    if(mDescription && rLastDescriptionObject)
    {
        //--Setup.
        float cTxtLft = 220.0f;
        float cTxtTop = 535.0f;
        float cTxtHei = CarnImages.rFont_Mainline->GetTextHeight();

        //--Name.
        CarnImages.rFont_Heading->DrawText(cTxtLft, 505.0f, 0, 1.0f, rLastDescriptionObject->GetDisplayName());

        //--Description.
        float tYPos = cTxtTop;
        char *rString = (char *)mDescription->PushIterator();
        while(rString)
        {
            //--Render.
            CarnImages.rFont_Mainline->DrawText(cTxtLft, tYPos, 0, 1.0f, rString);

            //--Next.
            tYPos = tYPos + cTxtHei;
            rString = (char *)mDescription->AutoIterate();
        }
    }
    //--No description. Print disassembly instructions.
    else if(mIsDisassembleMode)
    {
        float cTxtHei = CarnImages.rFont_Mainline->GetTextHeight();
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 0.0f), 0, 1.0f, "Select a modification to keep.");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 1.0f), 0, 1.0f, "All other modifications, and the item, will be");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 2.0f), 0, 1.0f, "lost after disassembly.");
        //CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 3.0f), 0, 1.0f, "");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 4.0f), 0, 1.0f, "Right-click to cancel.");
    }
    //--No description. Print selection instructions.
    else if(mSelectionCursor == -1)
    {
        float cTxtHei = CarnImages.rFont_Mainline->GetTextHeight();
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 0.0f), 0, 1.0f, "Click on a modification slot to begin.");
        //CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 1.0f), 0, 1.0f, "");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 2.0f), 0, 1.0f, "Modifications cannot be removed from an item ");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 3.0f), 0, 1.0f, "once placed in it without destroying the item.");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 4.0f), 0, 1.0f, "If you disassemble an item, you can keep one and");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 5.0f), 0, 1.0f, "only one of the modifications in it.");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 6.0f), 0, 1.0f, "If you place a new modification on top of an old");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 7.0f), 0, 1.0f, "one, the old modification is lost!");
    }
    //--No description. Print replacement instructions.
    else
    {
        float cTxtHei = CarnImages.rFont_Mainline->GetTextHeight();
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 0.0f), 0, 1.0f, "Click the modification to place in this slot.");
        CarnImages.rFont_Mainline->DrawText(220.0f, 505.0f + (cTxtHei * 1.0f), 0, 1.0f, "Right-click to cancel.");
    }

    ///--[Replacement List]
    //--Diagnostics.
    DebugPrint("Rendering replacement list.\n");

    //--Title.
    CarnImages.rFont_Heading->DrawTextArgs(700.0f, 90.0f, 0, 1.0f, "Available modifications");

    //--List is empty.
    if(mrReplacementList->GetListSize() < 1)
    {
        CarnImages.rFont_Mainline->DrawText(700.0f, 130.0f, 0, 1.0f, "You have no modification items.");
    }

    //--Listing.
    int i = 0;
    int tRenders = 0;
    AdventureItem *rModItem = (AdventureItem *)mrReplacementList->PushIterator();
    while(rModItem)
    {
        //--Below skip.
        if(i < mReplacementSkip)
        {
            i ++;
            rModItem = (AdventureItem *)mrReplacementList->AutoIterate();
            continue;
        }

        //--Render cap.
        if(tRenders >= cxCarnScrollPage)
        {
            mrReplacementList->PopIterator();
            break;
        }

        //--Hitbox.
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxHitbox_ReplaceLo + tRenders);
        if(!rHitbox)
        {
            i ++;
            tRenders ++;
            rModItem = (AdventureItem *)mrReplacementList->AutoIterate();
            continue;
        }

        //--Icon.
        StarBitmap *rGemImg = rModItem->GetIconImage();
        if(rGemImg) rGemImg->Draw(rHitbox->mLft, rHitbox->mTop + 4);

        //--Name.
        CarnImages.rFont_Mainline->DrawText(rHitbox->mLft+26.0f, rHitbox->mTop, 0, 1.0f, rModItem->GetDisplayName());

        //--Next.
        i ++;
        tRenders ++;
        rModItem = (AdventureItem *)mrReplacementList->AutoIterate();
    }

    //--Scrollbar.
    if(mrReplacementList->GetListSize() >= cxCarnScrollPage)
    {
        StandardRenderScrollbar(mReplacementSkip, cxCarnScrollPage, mrReplacementList->GetListSize() - cxCarnScrollPage, 195.0f, 438.0f, false, CarnImages.rScrollbar_Scroller, CarnImages.rScrollbar_Static);
    }

    ///--[Disassemble Button]
    //--Only appears in selection mode.
    if(mSelectionCursor == -1 && !mIsDisassembleMode)
    {
        //--Diagnostics.
        DebugPrint("Rendering disassembly button.\n");

        //--Backing.
        CarnImages.rFrame_Button->Draw();

        //--Text.
        CarnImages.rFont_Button->DrawTextArgs(603.0f, 102.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Disassemble");
    }

    ///--[Diagnostics]
    DebugPop("Completed. Finished normally.\n");
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
CarnUIRegem *CarnUIRegem::Fetch()
{
    ///--[Documentation]
    //--Locates and returns the CarnationMenu version of this class. It is possible this doesn't exist
    //  so error-check the results.
    AdventureMenu *rAdventureMenu = AdventureMenu::Fetch();
    StarUIPiece *rStarUIPiece = rAdventureMenu->RetrieveUI("Regem", POINTER_TYPE_CARNUIREGEM, NULL, "CarnUIRegem::Fetch()");
    if(!rStarUIPiece) return NULL;

    //--Dynamic cast.
    CarnUIRegem *rRegemUI = dynamic_cast<CarnUIRegem *>(rStarUIPiece);
    return rRegemUI;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
