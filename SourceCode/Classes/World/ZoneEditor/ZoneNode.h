///========================================= ZoneNode =============================================
//--A node in the ZoneEditor. Gains its own class in order to better handle its memory cleanup and
//  initialization. This would otherwise be a simple structure.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[FacingInfo]
typedef struct FacingInfo
{
    //--Facing Direction
    float mXRotation;
    float mYRotation;
    float mZRotation;

    //--Connections
    uint32_t mNodeDestination;
}FacingInfo;

///--[AutoTransitionInfo]
typedef struct AutoTransitionInfo
{
    bool mIsTeleport;
    uint32_t mPreviousNode;
    uint32_t mNodeDestination;
}AutoTransitionInfo;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class ZoneNode
{
    private:
    //--System
    char *mLocalName;
    uint32_t mNodeUniqueID;

    //--Position
    float mX;
    float mY;
    float mZ;

    //--Color
    StarlightColor mLocalColor;

    //--Directions
    StarLinkedList *mFacingList;

    //--Automatic Transition Sets
    StarLinkedList *mImmediateTransitionList;

    protected:

    public:
    //--System
    ZoneNode();
    ~ZoneNode();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    uint32_t GetNodeID();
    const char *GetName();
    float GetX();
    float GetY();
    float GetZ();
    int GetFacingsTotal();
    FacingInfo *GetFacing(int pSlot);
    FacingInfo *GetFacingByAngle(float pAngle);
    FacingInfo *GetFacingByDestination(uint32_t pTargetNode);
    FacingInfo *GetNearestFacingByAngle(float pAngle);
    int GetSlotOfFacing(void *pPtr);
    int GetAutoTransitionsTotal();
    AutoTransitionInfo *GetAutoTransition(int pSlot);
    AutoTransitionInfo *GetAutoTransitionFrom(uint32_t pFromNode);

    //--Manipulators
    void SetUniqueID(uint32_t pID);
    void SetName(const char *pName);
    void SetPosition(float pX, float pY, float pZ);
    void AddFacing(float pXRot, float pYRot, float pZRot);
    void SetFacingDirection(int pSlot, float pXRot, float pYRot, float pZRot);
    void SetFacingConnection(int pSlot, uint32_t pID);
    void RemoveFacing(int pSlot);
    bool AddAutoTransition(uint32_t pZoneFrom, uint32_t pZoneTo);
    void RemoveAutoTransition(int pSlot);
    void RemoveIllegalAutoTransitions();
    void ToggleTeleportCase();

    //--Core Methods
    void ActivateColorMixer();
    void ActivateReverseColorMixer();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void WriteToFile(FILE *fOutfile);
    bool ReadFromFile(FILE *fInfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

