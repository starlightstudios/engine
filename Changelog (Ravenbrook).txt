//-- [ ============= Version 1.00 ============= ]
November 18th, 2024

--Released the game :3

//-- [ ============= Version 1.01 ============= ]
November 22nd, 2024

--Bugfixes
*Fixed typos.
*Options menu will only play a sound when changing options, not when highlighting them.
*Added redundancy check for scrollbars to avoid null pointers.
*Fixed a crash when having tons of modifications at the same time.
*Fixed modification UI scrollbar being invisible.
*Increased size of modifications list to fill out the UI.
*You can now Alchemize and Discard modification items.
*Fixed an extra blank line in the Regem UI.
*Confirm Sale UI now uses Hexes instead of Platina.
*Fixed some erroneous trees on the carnival minimap.
*Combat spells now show SP instead of MP.
*Cleric's Ray of Light skill now costs 10 SP instead of 70.
*Equipment UI now correctly shows Strength instead of Speed when selecting an equipment slot.
*Icebolt's description now correctly states it deals Freeze damage (not Shock).
*Compass now renders above minimap, shouldn't be cut off anymore.
*Doll bad end will now handle the hedge maze boss being defeated correctly.
*Fixed Dahlia showing up when she was the last-talked-to NPC in some of Fluffs' dialogue handlers.
*Fixed black flash when switching to certain UIs.
*Fixed some fonts being incorrect on the UI.
*Fixed non-utility tags not appearing in descriptions.
*Exiting dungeons will now clear all state flags.
*Minor effects (vined, scalded, etc) should now correctly stop animating when cleared with Purify.
*Killing a boss when affected by a status should now correctly clear the status appearance.
*Fixed killing a boss on the same turn as a character crosses the influence gap forcing them to leave during the next battle in a different dungeon.
*Fixed duplicate party members appearing if you escape after losing someone during a boss fight.
*Fixed consumable items not decreasing in value as they are used.
*Fixed Sorceress class not gaining class XP.
*Possibly fixed the inventory scroll bug, the program will now correctly recompute offsets when changing pacts.

--New Features
*You can now retreat from witch slimes during the tutorial if you've already seen the witch slime bad end.
*Added dialogue, combat dialogue, and combat pause speed options.
*Added option to disable text voice ticks when accelerating dialogue.
*Added WASD support in addition to the arrow keys for navigation.
*You can now toggle item descriptions in the equipment menu.
*Increased stats on the equipment UI will render in green, decreased stats will render red.
*Can now toggle skill descriptions on in combat in case you forgot what one of them did.

//-- [ ============= Version 1.02 ============= ]
December 10th, 2024

--Bugfixes
*Fixed modifications not being rendered as (+x) on the sell UI.
*Fixed items with modifications rendering the wrong display name on the equipment UI.
*Items that are not equipped but do have modifications in them will now correctly save and load those modifications.
*Sorceress should now correctly display its level and XP on the class change UI.
*Fixed sell dialogue showing half the price (the sale price was correctly calculated, this is display-only).
*Hopefully fixed the wrong dialogue characters appearing during influence increasing events.
*Fixed the hub minimap not re-showing areas you've been to again when returning from a dungeon.
*Fixed a broken dialogue box when using the [Deception] tag on the Duel gate event.
*SP costs will no longer render in target selection mode after selecting a skill.
*Fixed missing percentage indicators in skill descriptions.
*Enemies should not be able to respawn on top of the player or immediately adjacent to the player anymore.
*Fixed not being able to change the comparison character in Sell mode in the shop.
*Retreating should (hopefully) no longer get you endlessly stuck in battles if there is an enemy on the retreat space.

--New Features
*Added a little text string to let you know that you will be able to save when the tutorial is complete, trust me.

//-- [ ============= Version 1.03 ============= ]
DATE HERE

--Bugfixes
*Fixed some incorrect pronounds in the intro
*Fixed Offer Fealty not inflicting Influence damage.

