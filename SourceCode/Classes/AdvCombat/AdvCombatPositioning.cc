//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///======================================= Calculations ===========================================
float AdvCombat::ComputeIdealX(bool pIsLeftSide, bool pIsOffscreen, int pPosition, int pTotalEntries, StarBitmap *pCombatPortrait)
{
    ///--[Documentation]
    //--Switching function, figures out if the entity should be in the onscreen or offscreen position
    //  and returns where they should be.
    if(!pIsOffscreen)
    {
        return ComputeOnscreenX(pIsLeftSide, pPosition, pTotalEntries);
    }
    else
    {
        return ComputeOffscreenX(pIsLeftSide, pCombatPortrait);
    }
}
float AdvCombat::ComputeOnscreenX(bool pIsLeftSide, int pPosition, int pTotalEntries)
{
    ///--[Documentation]
    //--Calculates the middle point where an entity should be standing, given a side of the screen
    //  and how many entities are sharing it with them.
    //--First, the player's party. This typically contains as many as four entries (but can have more)
    //  so is given 1/3rd of screen real estate. The 0th entity is considered to be the acting entity
    //  but might also just be the first in the party order if receiving hits from an enemy.
    //--Note: Enemies are on the right side of the screen, and this function assumes they are not
    //  being targeted. Targeted enemies move to the center-right side. To compute target positions,
    //  use ComputeIdealXTarget().
    if(pIsLeftSide)
    {
        //--If there is one entity, or fewer (for range-checks):
        if(pTotalEntries <= 1)
        {
            return VIRTUAL_CANVAS_X * 0.25f;
        }

        //--If we got this far, there are at least two total entities. Divvy the space from the rightmost
        //  position to the left edge with padding.
        float cLftPoint = VIRTUAL_CANVAS_X * 0.05f;
        float cRgtPoint = VIRTUAL_CANVAS_X * 0.30f;
        float cSpacePerEntity = (cRgtPoint - cLftPoint) / (float)pTotalEntries;

        //--Multiply, and add a half-slot to center the entity.
        return cRgtPoint - (cSpacePerEntity * (float)(pPosition + 0.50f));
    }

    ///--[Enemy Party]
    //--Enemy party. First, exactly one entry and a range-check:
    if(pTotalEntries <= 1)
    {
        return VIRTUAL_CANVAS_X * 0.85f;
    }

    //--If we got this far, there are at least two total entities. Divvy the space from the leftmost
    //  position to the right edge with padding.
    float cLftPoint = VIRTUAL_CANVAS_X * 0.55f;
    float cRgtPoint = VIRTUAL_CANVAS_X * 0.95f;
    float cSpacePerEntity = (cRgtPoint - cLftPoint) / (float)pTotalEntries;

    //--Multiply, and add a half-slot to center the entity.
    return cLftPoint + (cSpacePerEntity * (float)(pPosition + 0.50f));
}
float AdvCombat::ComputeOffscreenX(bool pIsLeftSide, StarBitmap *pImage)
{
    ///--[Documentation]
    //--Computes the expected offscreen position, taking into account the width of the image.
    //  Player party is on the left.
    if(pIsLeftSide)
    {
        float cLftPoint = ADVCOMBAT_POSITION_OFFSCREEN_LFT;
        if(pImage) cLftPoint = cLftPoint - (pImage->GetTrueWidth() * 0.50f);
        return cLftPoint;
    }

    //--Right side.
    float cRgtPoint = ADVCOMBAT_POSITION_INTRO_OFFSCREEN;
    if(pImage) cRgtPoint = cRgtPoint + (pImage->GetTrueWidth() * 1.00f);
    return cRgtPoint;
}
float AdvCombat::ComputeIdealY(AdvCombatEntity *pEntity)
{
    ///--[Documentation]
    //--Determines which team the entity is on. Based on that, returns its ideal Y position.
    if(!pEntity) return 0.0f;

    //--Party ID.
    uint32_t tPartyID = GetPartyGroupingID(pEntity->GetID());

    //--Entity is in the player's party:
    if(tPartyID == AC_PARTY_GROUP_PARTY)
    {
        TwoDimensionRealPoint tPoint = pEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_BASE);
        if(pEntity->GetOverrideCombatY() != -1)
        {
            tPoint.mYCenter = pEntity->GetOverrideCombatY();
        }
        return tPoint.mYCenter;
    }

    //--Entity is in the enemy party:
    if(tPartyID == AC_PARTY_GROUP_ENEMY)
    {
        TwoDimensionRealPoint tPoint = pEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_ENEMY);
        return tPoint.mYCenter;
    }

    //--All other cases, return this "Standard" which is slightly offscreen, to help catch errors.
    return ADVCOMBAT_POSITION_STD_Y;
}

///======================================= Core Methods ===========================================
void AdvCombat::RestageCharactersWithNewID(uint32_t pID)
{
    ///--[Documentation]
    //--Sometimes during an ability, a target will be added to the cluster after the event has
    //  begun, such as when a character is guarding another character. If this happens, call this
    //  function with the ID of the character in question and they will be staged.
    CombatEventPack *rActiveEvent = (CombatEventPack *)mEventQueue->GetElementBySlot(0);
    if(!rActiveEvent) return;

    //--Get the entity by its unique ID.
    AdvCombatEntity *rEntity = GetEntityByID(pID);
    if(!rEntity) return;

    //--Check if the entity is already associated with the event. If so, do nothing.
    if(IsEntityInvolvedInEvent(rEntity, rActiveEvent)) return;

    //--Add the entity to the "Additional Entities" list for the event.
    rActiveEvent->AddAdditionalEntity(rEntity);

    //--Re-order positioning on screen.
    PositionCharactersOnScreenForEvent(rActiveEvent);
}
void AdvCombat::PositionCharactersOnScreenForEvent(CombatEventPack *pEvent)
{
    ///--[Documentation]
    //--After all events are registered, position on-screen all characters who are involved with the event.
    //  Pass NULL to move all party members offscreen.
    //--This only positions party members. Enemies are implied to always be onscreen.
    if(!pEvent)
    {
        PositionCharactersOffScreen();
        return;
    }

    //--Temporary list.
    StarLinkedList *tOnscreenPartyList = new StarLinkedList(false);

    ///--[Player's Party]
    //--Iterate across the party. Add any party members to the onscreen list.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        //--Entity is involved, add it to the onscreen list.
        if(IsEntityInvolvedInEvent(rEntity, pEvent))
        {
            tOnscreenPartyList->AddElementAsTail("X", rEntity);
        }
        //--Not involved, order them offscreen.
        else
        {
            float tX = ComputeIdealX(true, true, 0, 1, rEntity->GetCombatPortrait());
            rEntity->SetIdealPositionX(tX);
            rEntity->MoveToIdealPosition(cStdMoveTicks);
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    ///--[Position]
    //--Order all the onscreen entities to move to their positions.
    int i = 0;
    rEntity = (AdvCombatEntity *)tOnscreenPartyList->PushIterator();
    while(rEntity)
    {
        //--Compute positions.
        float tRenderX = ComputeIdealX(true, false, i, tOnscreenPartyList->GetListSize(), NULL);
        float tRenderY = ComputeIdealY(rEntity);

        //--Set this as the ideal position, order the party to move.
        rEntity->SetIdealPosition(tRenderX, tRenderY);
        rEntity->MoveToIdealPosition(cStdMoveTicks);

        //--Next.
        i ++;
        rEntity = (AdvCombatEntity *)tOnscreenPartyList->AutoIterate();
    }

    //--Clean.
    delete tOnscreenPartyList;
}
void AdvCombat::PositionListOnscreen(bool pIsLeftSide, int pTicks, StarLinkedList *pList)
{
    ///--[Documentation]
    //--Given a list containing AdvCombatEntity's, computes and positions all of them as a cohesive party group.
    if(!pList) return;

    //--Setup.
    int i = 0;
    int tListSize = pList->GetListSize();

    //--Iterate.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)pList->PushIterator();
    while(rEntity)
    {
        //--Compute positions.
        float tX = ComputeIdealX(pIsLeftSide, false, i, tListSize, NULL);
        float tY = ComputeIdealY(rEntity);

        //--Set, move to.
        rEntity->SetIdealPosition(tX, tY);
        rEntity->MoveToIdealPosition(pTicks);

        //--Next.
        i ++;
        rEntity = (AdvCombatEntity *)pList->AutoIterate();
    }
}
void AdvCombat::PositionListOffscreen(bool pIsLeftSide, int pTicks, StarLinkedList *pList)
{
    ///--[Documentation]
    //--Given a list containing AdvCombatEntity's, computes and positions all of them offscreen.
    if(!pList) return;

    //--Iterate.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)pList->PushIterator();
    while(rEntity)
    {
        //--Position.
        float tX = ComputeIdealX(pIsLeftSide, true, 0, 1, rEntity->GetCombatPortrait());
        rEntity->SetIdealPositionX(tX);
        rEntity->MoveToIdealPosition(pTicks);

        //--Next.
        rEntity = (AdvCombatEntity *)pList->AutoIterate();
    }
}

///=========================================== Macros =============================================
void AdvCombat::PositionCharactersNormally()
{
    //--First, order characters offscreen.
    PositionCharactersOffScreen();

    //--Next, see if there is an event. If so, position with that in mind.
    CombatEventPack *rEventPackage = (CombatEventPack *)mEventQueue->GetElementBySlot(0);
    if(rEventPackage)
    {
        PositionCharactersOnScreenForEvent(rEventPackage);
        return;
    }

    //--See if there is an acting party member.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity || !IsEntityInPlayerParty(rActingEntity)) return;

    //--We have an acting entity, so mark them as onscreen.
    float cIdealX = ComputeIdealX(true, false, 0, 1, NULL);
    rActingEntity->SetIdealPositionX(cIdealX);
    rActingEntity->MoveToIdealPosition(cStdMoveTicks);
}
void AdvCombat::PositionCharactersOffScreen()
{
    PositionListOffscreen(true, ADVCOMBAT_STD_MOVE_TICKS, mrCombatParty);
}
void AdvCombat::PositionEnemies()
{
    PositionEnemiesByTarget(NULL);
    //PositionListOnscreen(false, ADVCOMBAT_STD_MOVE_TICKS, mrEnemyCombatParty);
}
void AdvCombat::PositionEnemiesByTarget(StarLinkedList *pTargetList)
{
    ///--[Documentation]
    //--Given an event, positions enemies based on who is being targeted and who is not. Targeted enemies
    //  move to the center-right of the screen, untargeted ones move to the far right side.
    //--Pass NULL to call the default enemy positioning, which places them uniformly on the right side.
    bool tIsTemporaryList = false;
    if(!pTargetList)
    {
        tIsTemporaryList = true;
        pTargetList = new StarLinkedList(false);
    }

    ///--[Targets]
    //--There must be at least one target otherwise skip this section. Create a list of targets in the enemy party.
    StarLinkedList *trTargetList = new StarLinkedList(false);
    StarLinkedList *trNonTargetList = new StarLinkedList(false);
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEntity)
    {
        //--On the list, add to targets.
        if(pTargetList->IsElementOnList(rEntity))
        {
            trTargetList->AddElementAsTail("X", rEntity);
        }
        //--Not, add to not-targets.
        else
        {
            trNonTargetList->AddElementAsTail("X", rEntity);
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    //--If there was at least one target:
    if(trTargetList->GetListSize() > 0)
    {
        //--Setup.
        float tCurrentX = 780.0f;

        //--Determine approximate spacing. The width available is about 420 pixels.
        float tXSpacing = 420.0f / (float)trTargetList->GetListSize();

        //--The spacing must be at least 20 pixels. Very large parties may run over the edge.
        if(tXSpacing < 20.0f) tXSpacing = 20.0f;

        //--Position entities.
        rEntity = (AdvCombatEntity *)trTargetList->PushIterator();
        while(rEntity)
        {
            //--Entity has not positioned before. This entity was offscreen, possibly due to reinforcements.
            //  Slide them onscreen. This only fires for enemies.
            if(!rEntity->HasPositionedInBattle())
            {
                //--Setup.
                float cStartX = ComputeIdealX(false, true, 0, 1, rEntity->GetCombatPortrait());
                float cIdealY = ComputeIdealY(rEntity);

                //--Set. Enemy starts offscreen then moves to their ideal position.
                rEntity->SetIdealPosition(tCurrentX, cIdealY);
                rEntity->MoveToPosition(cStartX, cIdealY, 0);
                rEntity->MoveToIdealPosition(cIntroMoveTicks);
                rEntity->SetIgnoreMovementForBlocking(true);

                //--Flag the enemy as having positioned in battle.
                rEntity->MarkPositionedInBattle();
            }
            //--Entity has already positioned before, handle them.
            else
            {
                rEntity->SetIdealPositionX(tCurrentX);
                rEntity->MoveToIdealPosition(cMoveTargetTicks);
            }

            //--Next.
            tCurrentX = tCurrentX + tXSpacing;
            rEntity = (AdvCombatEntity *)trTargetList->AutoIterate();
        }
    }

    ///--[Non-Targets]
    //--Non-targets are positioned similarly, but get less space and are further right.
    if(trNonTargetList->GetListSize() > 0)
    {
        //--Setup.
        float tCurrentX = 1100.0f;

        //--Determine approximate spacing. The width available is about 420 pixels.
        float tXSpacing = 340.0f / (float)trNonTargetList->GetListSize();

        //--The spacing must be at least 20 pixels. Very large parties may run over the edge.
        if(tXSpacing < 20.0f) tXSpacing = 20.0f;

        //--Position entities.
        rEntity = (AdvCombatEntity *)trNonTargetList->PushIterator();
        while(rEntity)
        {
            //--Entity has not positioned before. This entity was offscreen, possibly due to reinforcements.
            //  Slide them onscreen. This only fires for enemies.
            if(!rEntity->HasPositionedInBattle())
            {
                //--Setup.
                float cStartX = ComputeIdealX(false, true, 0, 1, rEntity->GetCombatPortrait());
                float cIdealY = ComputeIdealY(rEntity);

                //--Set. Enemy starts offscreen then moves to their ideal position.
                rEntity->SetIdealPosition(tCurrentX, cIdealY);
                rEntity->MoveToPosition(cStartX, cIdealY, 0);
                rEntity->MoveToIdealPosition(cIntroMoveTicks);
                rEntity->SetIgnoreMovementForBlocking(true);

                //--Flag the enemy as having positioned in battle.
                rEntity->MarkPositionedInBattle();
            }
            //--Entity has already positioned before, handle them.
            else
            {
                rEntity->SetIdealPositionX(tCurrentX);
                rEntity->MoveToIdealPosition(cMoveTargetTicks);
            }

            //--Next.
            tCurrentX = tCurrentX + tXSpacing;
            rEntity = (AdvCombatEntity *)trNonTargetList->AutoIterate();
        }
    }

    ///--[Clean]
    delete trTargetList;
    delete trNonTargetList;

    //--If the target list itself was temporary, delete it.
    if(tIsTemporaryList)
    {
        delete pTargetList;
    }
}
