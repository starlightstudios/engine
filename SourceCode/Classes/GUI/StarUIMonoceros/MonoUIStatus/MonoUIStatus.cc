//--Base
#include "MonoUIStatus.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"

///========================================== System ==============================================
MonoUIStatus::MonoUIStatus()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MSTA");

    ///--[Visiblity]
    ///--[Help Menu]

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUIStatus ======= ]
    ///--[System]
    ///--[Help Text]
    //--Help Strings
    mActivateString = new StarlightString();

    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Strings
    AllocateHelpStrings(cxMonoHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIStatus::~MonoUIStatus()
{
    delete mActivateString;
}
void MonoUIStatus::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Status UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Status UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",             &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIStatus_Strings *MonoUIStatus::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIStatus_Strings::MonoUIStatus_Strings()
{
    //--Local name: "MonoUIBase"
    MonoUIStatus::xrStringData = this;

    //--Strings
    str.mTextStatus      = InitializeString("MonoUIStatus_mStatus");      //"Status"
    str.mTextHelpMenu    = InitializeString("MonoUIStatus_mHelp Menu");   //"Help Menu"
    str.mTextPower       = InitializeString("MonoUIStatus_mPower");       //"Power"
    str.mTextProtection  = InitializeString("MonoUIStatus_mProtection");  //"Protection"
    str.mTextAccuracy    = InitializeString("MonoUIStatus_mAccuracy");    //"Accuracy"
    str.mTextEvade       = InitializeString("MonoUIStatus_mEvade");       //"Evade"
    str.mTextInitiative  = InitializeString("MonoUIStatus_mInitiative");  //"Initiative"
    str.mTextEquipment   = InitializeString("MonoUIStatus_mEquipment");   //"Equipment"
    str.mTextEmpty       = InitializeString("MonoUIStatus_mEmpty");       //"(Empty)"
    str.mTextResistances = InitializeString("MonoUIStatus_mResistances"); //"Resistances"
    str.mTextImmune      = InitializeString("MonoUIStatus_mImmune");      //"Immune"

    //--Help Sequence
    char tBuffer[100];
    for(int i = 0; i < 16; i ++)
    {
        sprintf(tBuffer, "MonoUIStatus_mHelpText%02i", i);
        str.mTextHelp[i] = InitializeString(tBuffer);
    }

    //--Control Sequence
    str.mTextControlShowHelp         = InitializeString("MonoUIBase_mControlShowHelp");         //"[IMG0] Show Help"
    str.mTextControlPrevChar         = InitializeString("MonoUIBase_mControlPrevChar");         //"[IMG0] or [IMG1]+[IMG2] Previous Character"
    str.mTextControlNextChar         = InitializeString("MonoUIBase_mControlNextChar");         //"[IMG0] or [IMG1]+[IMG2] Next Character"
    str.mTextControlPurchaseLevelUps = InitializeString("MonoUIBase_mControlPurchaseLevelUps"); //"[IMG0] Purchase Level Ups"
}
MonoUIStatus_Strings::~MonoUIStatus_Strings()
{
    for(int i = 0; i < MONOUISTATUS_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIStatus_Strings::GetSize()
{
    return MONOUISTATUS_STRINGS_TOTAL;
}
void MonoUIStatus_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUISTATUS_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}


///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUIStatus::TakeForeground()
{
    ///--[Call Base Class]
    AdvUIStatus::TakeForeground();

    ///--[Override Strings]
    mShowHelpString->    AutoSetControlString(xrStringData->str.mTextControlShowHelp,         1, "F1");
    mCharacterLftString->AutoSetControlString(xrStringData->str.mTextControlPrevChar,         3, "UpLevel", "Ctrl", "Left");
    mCharacterRgtString->AutoSetControlString(xrStringData->str.mTextControlNextChar,         3, "DnLevel", "Ctrl", "Right");
    mActivateString->    AutoSetControlString(xrStringData->str.mTextControlPurchaseLevelUps, 1, "Activate");
}

///======================================= Core Methods ===========================================
void MonoUIStatus::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Execution]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Set lines.
    SetHelpString(i, xrStringData->str.mTextHelp[ 0], 2, "DnLevel", "UpLevel");
    SetHelpString(i, xrStringData->str.mTextHelp[ 1]);
    SetHelpString(i, xrStringData->str.mTextHelp[ 2]);
    SetHelpString(i, xrStringData->str.mTextHelp[ 3]);
    SetHelpString(i, xrStringData->str.mTextHelp[ 4]);
    SetHelpString(i, xrStringData->str.mTextHelp[ 5]);
    SetHelpString(i, xrStringData->str.mTextHelp[ 6], 2, "F1", "Cancel");
    SetHelpString(i, xrStringData->str.mTextHelp[ 7]);
    SetHelpString(i, xrStringData->str.mTextHelp[ 8]);
    SetHelpString(i, xrStringData->str.mTextHelp[ 9]);
    SetHelpString(i, xrStringData->str.mTextHelp[10]);
    SetHelpString(i, xrStringData->str.mTextHelp[11]);

    //--Damage Icon Legend.
    mHelpMenuStrings[i]->SetString(xrStringData->str.mTextHelp[12]);
    mHelpMenuStrings[i]->AllocateImages(4);
    mHelpMenuStrings[i]->SetImageP(0, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Physical"));
    mHelpMenuStrings[i]->SetImageP(1, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Bane"));
    mHelpMenuStrings[i]->SetImageP(2, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Arcane"));
    mHelpMenuStrings[i]->SetImageP(3, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Flame"));
    i ++;

    //--Statistic Icon Legend
    SetHelpString(i, xrStringData->str.mTextHelp[13]);
    SetHelpString(i, xrStringData->str.mTextHelp[14]);
    mHelpMenuStrings[i]->SetString(xrStringData->str.mTextHelp[15]);
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Health"));
    mHelpMenuStrings[i]->SetImageP(1, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Attack"));
    mHelpMenuStrings[i]->SetImageP(2, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Accuracy"));
    mHelpMenuStrings[i]->SetImageP(3, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Evade"));
    mHelpMenuStrings[i]->SetImageP(4, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Initiative"));
    mHelpMenuStrings[i]->SetImageP(5, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Protection"));
    i ++;

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIStatus::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Exactly the same as the base call, but has the ability to press the Activate key to open
    //  up the level up screen.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Activate]
    if(ControlManager::Fetch()->IsFirstPress("Activate"))
    {
        SetCodeBackward(MONOUISTS_CODE_LEVELUP);
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Call Base]
    return AdvUIStatus::UpdateForeground(pCannotHandleUpdate);
}
void MonoUIStatus::UpdateBackground()
{
    AdvUIStatus::UpdateBackground();
}

///========================================= File I/O =============================================
void MonoUIStatus::RenderLft(float pVisAlpha)
{
    AdvUIStatus::RenderLft(pVisAlpha);
}
void MonoUIStatus::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Identical to the parent version but uses a different color for the title.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Render]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextStatus);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Control strings
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIStatus::RenderRgt(float pVisAlpha)
{
    AdvUIStatus::RenderRgt(pVisAlpha);
}
void MonoUIStatus::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Identical to the base but renders an additional activation string. The user can activate the Level Up menu from here.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Render]
    //--Get active entity.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) { AutoPieceClose(); return; }

    //--Equipment subroutine.
    RenderEquipment(rActiveEntity, cColorAlpha);

    //--Strings.
    float cYPos = VIRTUAL_CANVAS_Y - cxAdvMainlineFontH;
    mCharacterLftString->DrawText(            0.0f, cYPos,                                    SUGARFONT_NOCOLOR,                          1.0f, MonoImages.rFont_Control);
    mActivateString->    DrawText(            0.0f, cYPos - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR,                          1.0f, MonoImages.rFont_Control);
    mCharacterRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos,                                    SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///==================================== Drawing Subroutines =======================================
void MonoUIStatus::RenderBasics(AdvCombatEntity *pEntity, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders basic character statistics like Name, HP, XP, Attack, Level, etc.
    if(!pEntity) return;

    ///--[Static Parts]
    //--Backings.
    AdvImages.rFrame_Status->Draw();
    AdvImages.rOverlay_StatusDetail->Draw();

    ///--[Name, Level]
    //--Name.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, pColorAlpha);
    MonoImages.rFont_Health->DrawText(244.0f, 389.0f, 0, 1.0f, pEntity->GetDisplayName());

    //--Level.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    AdvImages.rFont_Heading->DrawTextArgs(164.0f, 405.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", pEntity->GetLevel() + 1);

    //--HPMax. HP is always full on this screen due to the game design of Monoceros.
    MonoImages.rFont_Health->DrawTextArgs(244.0f, 416.0f, 0, 1.0f, "%i", pEntity->GetHealthMax());

    ///--[Text and Symbols]
    //--Constants.
    float cTxtLft = 125.0f;
    float cTxtStatRgt = 396.0f;
    float cTxtIconRgt = 400.0f;

    //--Attack Power.
    AdvImages.rFont_Mainline->DrawText    (cTxtLft, 460.0f, 0, 1.0f, xrStringData->str.mTextPower);
    AdvImages.rFont_Mainline->DrawTextArgs(cTxtStatRgt, 460.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_ATTACK));
    AdvImages.rStatisticIcons[0]->Draw(cTxtIconRgt, 473.0f);

    //--Protection
    AdvImages.rFont_Mainline->DrawText    (cTxtLft, 502.0f, 0, 1.0f, xrStringData->str.mTextProtection);
    AdvImages.rFont_Mainline->DrawTextArgs(cTxtStatRgt, 502.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_RESIST_PROTECTION));
    AdvImages.rStatisticIcons[1]->Draw(cTxtIconRgt, 515.0f);

    //--Accuracy
    AdvImages.rFont_Mainline->DrawText    (cTxtLft, 544.0f, 0, 1.0f, xrStringData->str.mTextAccuracy);
    AdvImages.rFont_Mainline->DrawTextArgs(cTxtStatRgt, 544.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_ACCURACY));
    AdvImages.rStatisticIcons[2]->Draw(cTxtIconRgt, 557.0f);

    //--Evade
    AdvImages.rFont_Mainline->DrawText    (cTxtLft, 586.0f, 0, 1.0f, xrStringData->str.mTextEvade);
    AdvImages.rFont_Mainline->DrawTextArgs(cTxtStatRgt, 586.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_EVADE));
    AdvImages.rStatisticIcons[3]->Draw(cTxtIconRgt, 593.0f);

    //--Initiative
    AdvImages.rFont_Mainline->DrawText    (cTxtLft, 628.0f, 0, 1.0f, xrStringData->str.mTextInitiative);
    AdvImages.rFont_Mainline->DrawTextArgs(cTxtStatRgt, 628.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pEntity->GetStatistic(STATS_INITIATIVE));
    AdvImages.rStatisticIcons[4]->Draw(cTxtIconRgt, 641.0f);

    ///--[Clean]
    StarlightColor::ClearMixer();
}
void MonoUIStatus::RenderEquipment(AdvCombatEntity *pEntity, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders equipment slots and entries for the given character. Basically the same as the equipment UI
    //  but non-interactive.
    if(!pEntity) return;

    ///--[Static]
    //--Frame. Detail is dynamically generated.
    AdvImages.rFrame_Equipment->Draw();

    //--Header text, yellow shade.
    mMonoSubtitleColor.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Heading->DrawText(540.0f, 139.0f, 0, 1.0f, xrStringData->str.mTextEquipment);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    ///--[Dynamic]
    //--Iterate across the slots and call a subroutine.
    int tSlots = pEntity->GetEquipmentSlotsTotal();
    for(int i = 0; i < tSlots; i ++)
    {
        RenderEquipmentEntry(pEntity, i, pColorAlpha);
    }
}
void MonoUIStatus::RenderEquipmentEntry(AdvCombatEntity *pEntity, int pSlot, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders an equipment entry for the Status UI. Unlike the Equipment UI, no handling needs to be done
    //  for switching cases since the Status UI is non-interactive.
    if(!pEntity) return;

    //--Get and check the equipment slot.
    const char *rSlotName = pEntity->GetDisplayNameOfEquipmentSlot(pSlot);
    AdventureItem *rSlotContents = pEntity->GetEquipmentBySlotI(pSlot);
    if(!rSlotName) return;

    //--Constants.
    float cSlotX = 535.0f;
    float cSlotY = 203.0f;
    float cSlotH =  50.0f;
    float cIconX = cSlotX +  4.0f;
    float cIconY = cSlotY + 22.0f;
    float cNameX = cSlotX + 25.0f;
    float cNameY = cSlotY + 22.0f;

    //--Gem positions.
    float cGemX = 784.0f;
    float cGemY = 200.0f;

    ///--[Empty Slot]
    //--Slot is empty, darken it slightly.
    if(!rSlotContents)
    {
        mColor_DarkEquipSlot.SetAsMixerAlpha(pColorAlpha);
        MonoImages.rFont_Equipment->DrawText(cSlotX, cSlotY + (cSlotH * pSlot), 0, 1.0f, rSlotName);
        MonoImages.rFont_Equipment->DrawText(cNameX, cNameY + (cSlotH * pSlot), 0, 1.0f, xrStringData->str.mTextEmpty);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
        return;
    }

    ///--[Occupied Slot]
    //--Render icon/name/gems.
    const char *rItemName = rSlotContents->GetDisplayName();
    StarBitmap *rIcon = rSlotContents->GetIconImage();

    //--Render.
    mMonoParagraphColor.SetAsMixerAlpha(pColorAlpha);
    MonoImages.rFont_Equipment->DrawText(cSlotX, cSlotY + (cSlotH * pSlot), 0, 1.0f, rSlotName);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    if(rItemName) MonoImages.rFont_Equipment->DrawTextFixed(cNameX, cNameY + (cSlotH * pSlot), cxAdvItemMaxNameLen, 0, rItemName);
    if(rIcon)     rIcon->Draw(cIconX, cIconY + (cSlotH * pSlot));

    //--Gem slots.
    RenderGemSlotsSwitch(cGemX, cGemY, rSlotContents, AdvImages.rOverlay_GemSlot);

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
}
void MonoUIStatus::RenderResistances(AdvCombatEntity *pEntity, float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the protection and all resistances for the active character. Also shows how much damage those
    //  resistances reduce under normal circumstances.
    if(!pEntity) return;

    ///--[Static]
    //--Frame, detail.
    AdvImages.rFrame_Resistance->Draw();

    //--Header text, yellow shade.
    mMonoSubtitleColor.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Heading->DrawText(964.0f, 139.0f, 0, 1.0f, xrStringData->str.mTextResistances);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    ///--[Dynamic]
    //--Icon/Value setup.
    float cIconX = 970.0f;
    float cIconY = 214.0f;
    float cIconH =  60.0f;
    float cValueX = cIconX + 66.0f;
    float cValueY = cIconY - 14.0f;
    float cReductionX = 1246.0f;
    float cReductionY = cIconY - 12.0f;

    //--Icons, resist values, reduction percentage.
    for(int i = 0; i < MONOUISS_RESIST_ICONS_TOTAL; i ++)
    {
        //--Setup.
        int tResistValue = pEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_RESIST_START + i);

        //--Render icon.
        AdvImages.rResistanceIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Value is 1000 or higher. Render "Immune".
        if(tResistValue >= 1000)
        {
            MonoImages.rFont_MainlineLg->DrawText(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, xrStringData->str.mTextImmune);
        }
        //--Otherwise, render the value and the percentage reduction.
        else
        {
            //--If a bonus is applied, render the statistic in purple. If a malus is applied, red.
            int tBonusEqp = pEntity->GetStatistic(ADVCE_STATS_EQUIPMENT, STATS_RESIST_START + i);
            int tBonusAbi = pEntity->GetStatistic(ADVCE_STATS_TEMPEFFECT, STATS_RESIST_START + i);
            int tTotalBonus = tBonusEqp + tBonusAbi;
            if(tTotalBonus > 0)
            {
                mColor_ResistBonus.SetAsMixerAlpha(pColorAlpha);
            }
            else if(tTotalBonus < 0)
            {
                mColor_ResistMalus.SetAsMixerAlpha(pColorAlpha);
            }

            //--Resistance amount.
            MonoImages.rFont_MainlineLg->DrawTextArgs(cValueX, cValueY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistValue);

            //--Computed reduction.
            float cReduction = 1.0f - AdvCombatEntity::ComputeResistance(tResistValue);
            MonoImages.rFont_MainlineLg->DrawTextArgs(cReductionX, cReductionY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%0.0f%%", cReduction * 100.0f);

            //--Clean.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
