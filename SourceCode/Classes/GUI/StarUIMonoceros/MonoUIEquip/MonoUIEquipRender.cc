//--Base
#include "MonoUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

///========================================== Routing =============================================
///======================================= Side Renders ===========================================
void MonoUIEquip::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Only the top UI renders on this object. It slides in from the top of the screen.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Rendering]
    //--Backing.
    MonoImages.rOverlay_Titleshade->Draw();

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextEquipment);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Control strings
    mShowHelpString->DrawText(0.0f, cxMonoFontMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mInspectString->DrawText (0.0f, cxMonoFontMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--This string only appears during replacement mode.
    if(mReplacementTimer > 0)
    {
        //--Completion percentage.
        float cReplacePercent = EasingFunction::QuadraticInOut(mReplacementTimer, cReplaceTicks);

        //--Render with fade.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cReplacePercent * cColorAlpha);
        mScrollFastString->DrawText(0.0f, cxMonoFontMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIEquip::RenderRgt(float pVisAlpha)
{
    ///--[Documentation]
    //--The right side of the UI shows the object's properties, replacement list, and cursor.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) return;

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Arrow]
    //--Right-side arrow.
    float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1) AdvImages.rOverlay_ArrowRgt->Draw(cArrowOscillate, 0.0f);

    ///--[Frames]
    //--Equipment list and properties.
    MonoImages.rFrame_Equipment->Draw();
    MonoImages.rFrame_Resistances->Draw();
    MonoImages.rFrame_Statistics->Draw();

    ///--[Equipment List]
    //--Header.
    mMonoSubtitleColor.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(421.0f, 137.0f, 0, 1.0f, xrStringData->str.mTextSlots);
    AdvImages.rFont_Heading->DrawText(751.0f, 137.0f, 0, 1.0f, xrStringData->str.mTextGems);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    ///--[Highlight]
    //--Indicates where the player's control cursor is.
    glTranslatef(mHighlightPos.mXCur, mHighlightPos.mYCur, 0.0f);
    glScalef(mHighlightSize.mXCur / 243.0f, mHighlightSize.mYCur / 61.0f, 1.0f);
    MonoImages.rOverlay_Selection->Draw();
    glScalef(243.0f / mHighlightSize.mXCur, 61.0f / mHighlightSize.mYCur, 1.0f);
    glTranslatef(-mHighlightPos.mXCur, -mHighlightPos.mYCur, 0.0f);

    //--Listing.
    int tSlots = rActiveEntity->GetEquipmentSlotsTotal();
    for(int i = 0; i < tSlots; i ++)
    {
        //--Below skip, don't render.
        if(i < mEqpSkip) continue;

        //--Stop rendering at cap.
        if(i - mEqpSkip >= cxMonoEqpScrollPage) break;

        //--Translate position up so the entry renders in the correct spot.
        glTranslatef(0.0f, cxMonoEqpSizePerEntry * mEqpSkip * -1.0f, 0.0f);

        //--Entry.
        RenderEquipEntry(i, cColorAlpha);

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);
        glTranslatef(0.0f, cxMonoEqpSizePerEntry * mEqpSkip * 1.0f, 0.0f);
    }

    ///--[Scrollbar]
    //--Above size entries, show a scrollbar.
    if(tSlots >= cxMonoEqpScrollPage)
    {
        StandardRenderScrollbar(mEqpSkip, cxMonoEqpScrollPage, tSlots - cxMonoEqpScrollPage, 184.0f, 496.0f, false, AdvImages.rScrollbar_Scroller, MonoImages.rScrollbar_EqpStatic);
    }

    ///--[Properties]
    //--Header.
    mMonoSubtitleColor.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(940.0f, 115.0f, 0, 1.0f, xrStringData->str.mTextStatistics);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Subroutine.
    RenderEquipProperties(cColorAlpha);

    ///--[Replacements]
    //--When selecting a replacement item, this menu appears. If the replacement timer is zero, it does nothing.
    RenderEquipReplacements();

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIEquip::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--The bottom of the UI shows control help strings.

    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Help Strings]
    //--Strings.
    float cYPos = VIRTUAL_CANVAS_Y - cxAdvMainlineFontH;
    mCharacterLftString->DrawText(            0.0f, cYPos,                          SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mCharacterRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///===================================== Equipment Details ========================================
void MonoUIEquip::RenderEquipDetails(float pColorAlpha)
{
    ///--[Documentation]
    //--Renders extended details about the item in question. This includes a description frame, expandable header,
    //  description text, and the icon.
    if(mDetailsTimer < 1 || pColorAlpha <= 0.0f) return;

    //--Percentage.
    float cDetailsPercent = EasingFunction::QuadraticInOut(mDetailsTimer, cDetailTicks);

    //--Offsets.
    float cXOffset = 0.0f;
    float cYOffset = VIRTUAL_CANVAS_Y * (1.0f - cDetailsPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cDetailsPercent * 0.7f * pColorAlpha));

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen. Does not fade even if fading is the selected option.
    glTranslatef(cXOffset * 1.0f, cYOffset * 1.0f, 0.0f);

    //--Frame.
    AdvImages.rFrame_Description->Draw();
    MonoImages.rOverlay_DescriptionName->Draw();

    ///--[No Item]
    //--Render a fixed-size header and stop if there's no item.
    if(!rDetailsItem || rDetailsItem == AdventureInventory::xrDummyUnequipItem)
    {
        glTranslatef(cXOffset * -1.0f, cYOffset * -1.0f, 0.0f);
        return;
    }

    ///--[Header and Name]
    //--Expandable header and name. Note that there is an additional 48 pixels of space on the header
    //  within the dead zones.
    mMonoParagraphColor.SetAsMixer();
    AdvImages.rFont_Heading->DrawText(196.0f, 497.0f, 0, 1.0f, rDetailsItem->GetDisplayName());

    ///--[Description]
    //--Setup.
    float cXPosition = 129.0f;
    float cYPosition = 563.0f;
    float cYHeight   =  22.0f;

    //--Render advanced description lines.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = rDetailsItem->GetAdvancedDescription(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Description);
    }

    ///--[Finish Up]
    //--Clean.
    glTranslatef(cXOffset * -1.0f, cYOffset * -1.0f, 0.0f);
}

///===================================== Equipment Entries ========================================
void MonoUIEquip::RenderEquipEntry(int pSlot, float pColorAlpha)
{
    ///--[Documentation]
    //--When rendering the equipment that the character has equipped, this function is called. These equipment
    //  entries can move around and fade during replacement mode.
    //--An entry consists of the name, icon, and gem slots.

    ///--[Constants]
    //--Colors.
    StarlightColor cDarkened = StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, pColorAlpha);

    ///--[Variables]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCharacterCursor);
    EquipmentSlotPack *rEquipSlot = rActiveEntity->GetEquipmentSlotPackageI(pSlot);

    //--Name of slot.
    const char *rSlotName = rActiveEntity->GetDisplayNameOfEquipmentSlot(pSlot);
    if(!rSlotName) return;

    //--Get contents.
    AdventureItem *rSlotContents = rActiveEntity->GetEquipmentBySlotI(pSlot);

    //--Compute positions.
    float cXOffFromAdv = -59.0f;
    float cYSpacing = cxMonoEqpSizePerEntry;
    float cNameYPosition  = cSlotNameY  + (cYSpacing * pSlot) - 15.0f;
    float cEquipYPosition = cEquipNameY + (cYSpacing * pSlot) - 3.0f;
    float cIconYPosition  = cEquipIconY + (cYSpacing * pSlot) + 8.0f;
    float cGemYPosition   = cGemIconY   + (cYSpacing * pSlot) + 9.0f;
    float cRenderAlpha    = pColorAlpha;

    ///--[Replacing]
    //--When in replacement mode, the selected entry scrolls to the top of the menu and all others disappear
    //  behind a stencil block. The zeroth entry fades out if it was not selected.
    if(mReplacementTimer > 0)
    {
        //--Effective position after scrollbar is taken into account.
        int tEffectiveSlot = pSlot - mEqpSkip;

        //--Replacement timer percentage.
        float cPercent = EasingFunction::QuadraticInOut(mReplacementTimer, cReplaceTicks);

        //--If this is the selected entry, scroll to the top.
        if(pSlot == mEquipmentCursor)
        {
            //--Compute how much to adjust the slot position.
            float cUseSlot = (float)pSlot * (1.0f - cPercent);

            //--Additional fixed offset added by the scrollbar, does not get reduced by the replacement timer.
            float cScrollOff = mEqpSkip * cYSpacing;

            //--Compute finalized Y positions.
            cNameYPosition  = cSlotNameY  + (cSlotH * cUseSlot) + cScrollOff - 15.0f;
            cEquipYPosition = cEquipNameY + (cSlotH * cUseSlot) + cScrollOff -  3.0f;
            cIconYPosition  = cEquipIconY + (cSlotH * cUseSlot) + cScrollOff +  8.0f;
            cGemYPosition   = cGemIconY   + (cSlotH * cUseSlot) + cScrollOff +  9.0f;
        }
        //--Otherwise, if we are the zeroth entry, fade out.
        else if(tEffectiveSlot == 0)
        {
            cRenderAlpha = (1.0f - cPercent) * pColorAlpha;
        }
        //--All other cases, render normally. A stencil block will cover the entry up.
        else
        {

        }
    }

    ///--[Highlight]
    //--If in replacement mode and this is the selected item, render a differently colored entry.
    if(mIsReplacing && pSlot == mEquipmentCursor)
    {
        //--Position.
        float cLft = 421.0f;
        float cTop = cNameYPosition + 8.0f;
        float cRgt = cLft + 282.0f;
        float cBot = cTop + 61.0f;
        float cWid = cRgt - cLft;
        float cHei = cBot - cTop;

        //--If this is a gem, modify position.
        if(mGemCursor > -1)
        {
            cLft = 705.0f + (24.0f * mGemCursor);
            cTop = cNameYPosition + 41.0f;
            cRgt = cLft + 30.0f;
            cBot = cTop + 30.0f;
            cWid = cRgt - cLft;
            cHei = cBot - cTop;
        }

        //--Color.
        StarlightColor::SetMixer(1.0f, 1.0f, 0.0f, pColorAlpha);

        //--Render.
        glTranslatef(cLft, cTop, 0.0f);
        glScalef(cWid / 243.0f, cHei / 61.0f, 1.0f);
        MonoImages.rOverlay_Selection->Draw();
        glScalef(243.0f / cWid, 61.0f / cHei, 1.0f);
        glTranslatef(-cLft, -cTop, 0.0f);

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    }

    ///--[Rendering]
    //--Slot is empty, darken it slightly.
    if(!rSlotContents)
    {
        cDarkened.SetAsMixerAlpha(cRenderAlpha);
        AdvImages.rFont_Mainline->DrawText(cSlotNameX + cXOffFromAdv,  cNameYPosition, 0, 1.0f, rSlotName);
        AdvImages.rFont_Mainline->DrawText(cEquipNameX + cXOffFromAdv, cEquipYPosition, 0, 1.0f, xrStringData->str.mTextEmpty);
        StarlightColor::ClearMixer();
    }
    //--Slot is occupied:
    else
    {
        //--Variables.
        const char *rItemName = rSlotContents->GetDisplayName();
        StarBitmap *rIcon = rSlotContents->GetIconImage();

        //--Render.
        mMonoParagraphColor.SetAsMixerAlpha(cRenderAlpha);
        AdvImages.rFont_Mainline->DrawText(cSlotNameX + cXOffFromAdv, cNameYPosition, 0, 1.0f, rSlotName);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cRenderAlpha);
        if(rItemName) AdvImages.rFont_Mainline->DrawTextFixed(cEquipNameX + cXOffFromAdv, cEquipYPosition, cItemNameMaxLen, 0, rItemName);
        if(rIcon)     rIcon->Draw(cEquipIconX + cXOffFromAdv, cIconYPosition);

        //--Gem slots.
        int tGemSlots = rSlotContents->GetGemSlots();
        if(rEquipSlot->mNoGems) tGemSlots = 0;
        for(int p = 0; p < tGemSlots; p ++)
        {
            //--Get the gem.
            AdventureItem *rGem = rSlotContents->GetGemInSlot(p);

            //--Render the gem image.
            if(rGem)
            {
                StarBitmap *rImage = rGem->GetIconImage();
                if(rImage) rImage->Draw(cGemIconX + (cGemIconW * p), cGemYPosition);
            }

            //--In all cases, render the frame over the gem.
            AdvImages.rOverlay_GemSlot->Draw(cGemIconX + (cGemIconW * p), cGemYPosition);
        }

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    }
}

///================================== Equipment Replacements ======================================
void MonoUIEquip::RenderEquipReplacements()
{
    ///--[Documentation]
    //--Renders the replacement submenu. This menu appears when the player selects a piece of equipment
    //  or a gem they want to change. The menu shows all the items that can replace it, including a
    //  special "unequip" item.
    //--The replacement listing appears over the existing listing using a stencil blocking.
    //--Renders replacement for the currently selected slot. This requires a stencil overlay.
    if(mReplacementTimer < 1) return;

    //--Completion percentage.
    float cReplacePercent = EasingFunction::QuadraticInOut(mReplacementTimer, cReplaceTicks);

    ///--[Divider]
    //--Move the divider. It starts at the bottom and moves to the top, "wiping" away the previous entries.
    float cDividerOffset = (1.0f - cReplacePercent) * (cReplaceDividerH);
    MonoImages.rOverlay_Replacement->Draw(0.0f, cDividerOffset);

    ///--[Stencil Overlay]
    //--Create a stencil block over the bottom part of the frame.
    glEnable(GL_STENCIL_TEST);
    glDisable(GL_TEXTURE_2D);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilFunc(GL_ALWAYS, cStencil, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    glStencilMask(0xFF);

    //--Render to the stencil buffer.
    StarBitmap::DrawRectFill(379.0f, 260.0f + cDividerOffset, 879.0f, 699.0f, StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

    //--Swap stencil mode.
    glEnable(GL_TEXTURE_2D);
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilFunc(GL_EQUAL, cStencil, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    glStencilMask(0xFF);

    //--Render the frame again.
    MonoImages.rFrame_Clean->Draw();

    ///--[Highlight]
    //--Indicates where the player's control cursor is. Render again in replacements mode otherwise
    //  the stencil will cover it.
    glTranslatef(mHighlightPos.mXCur, mHighlightPos.mYCur, 0.0f);
    glScalef(mHighlightSize.mXCur / 243.0f, mHighlightSize.mYCur / 61.0f, 1.0f);
    MonoImages.rOverlay_Selection->Draw(0.0f, 8.0f);
    glScalef(243.0f / mHighlightSize.mXCur, 61.0f / mHighlightSize.mYCur, 1.0f);
    glTranslatef(-mHighlightPos.mXCur, -mHighlightPos.mYCur, 0.0f);

    ///--[Replacement Options]
    //--Setup.
    float cYPosition = cReplaceStartY + cDividerOffset + 15.0f;

    //--Iterate.
    int tCurrent = 0;
    AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->PushIterator();
    while(rItem)
    {
        //--If the current value is below the skip, don't render.
        if(tCurrent < mReplacementSkip)
        {
            tCurrent ++;
            rItem = (AdventureItem *)mValidReplacementsList->AutoIterate();
            continue;
        }

        //--Get and render.
        const char *rItemName = rItem->GetDisplayName();
        StarBitmap *rIcon = rItem->GetIconImage();

        //--If this is the "Unequip" item, display "Empty".
        if(rItem == AdventureInventory::xrDummyUnequipItem)
        {
            AdvImages.rFont_Mainline->DrawText(cEquipNameX - 56.0f, cYPosition, 0, 1.0f, xrStringData->str.mTextEmptyUnequip);
        }
        //--Normal case:
        else
        {
            //--Basics.
            if(rItemName) AdvImages.rFont_Mainline->DrawTextFixed(cEquipNameX - 56.0f, cYPosition, cItemNameMaxLen, 0, rItemName);
            if(rIcon)     rIcon->Draw(cEquipIconX - 56.0f, cYPosition+10.0f);

            //--Gem slots.
            int tGemSlots = rItem->GetGemSlots();
            for(int p = 0; p < tGemSlots; p ++)
            {
                //--Get the gem.
                AdventureItem *rGem = rItem->GetGemInSlot(p);

                //--Render the gem image.
                if(rGem)
                {
                    StarBitmap *rImage = rGem->GetIconImage();
                    if(rImage) rImage->Draw(cGemIconX + (cGemIconW * p), cYPosition + 12.0f);
                }

                //--In all cases, render the frame over the gem.
                AdvImages.rOverlay_GemSlot->Draw(cGemIconX + (cGemIconW * p), cYPosition + 12.0f);
            }
        }

        //--Next.
        tCurrent ++;
        cYPosition = cYPosition + 30.0f;
        rItem = (AdventureItem *)mValidReplacementsList->AutoIterate();
    }

    ///--[Clean]
    //--Clean.
    glDisable(GL_STENCIL_TEST);

    ///--[Scrollbar]
    //--If the replacement list has over cReplaceEntriesPerPage entries, render a scrollbar.
    if(mValidReplacementsList->GetListSize() > cReplaceEntriesPerPage)
    {
        StandardRenderScrollbar(mReplacementSkip, cReplaceEntriesPerPage, mValidReplacementsList->GetListSize(), 286.0f, 395.0f, false, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }
}

///=================================== Equipment Properties =======================================
void MonoUIEquip::RenderEquipProperties(float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the character's properties on the right side of the equipment screen. Dynamically adjusts to show
    //  the change in statistics when the player switches weapons or armor or gems.

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCharacterCursor);

    //--Setup variables.
    float cIconX = 944.0f;
    float cIconY = 177.0f;
    float cIconH =  43.0f;
    float cPropertyBaseX = 1030.0f;
    float cPropertyBaseY = cIconY - 10.0f;

    //--Alt-weapon. If the highlighted slot is a weapon alt, the displayed statistics are what the player would
    //  have if they changed weapons in battle.
    EquipmentSlotPack *rPackage = rActiveEntity->GetEquipmentSlotPackageI(mEquipmentCursor);
    if(!rPackage) return;

    ///--[Iterate Across Properties]
    //--Render.
    for(int i = 0; i < MONOUIEQP_RESISTANCE_BEGIN; i ++)
    {
        ///--[Basics]
        //--Icon.
        MonoImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Value.
        int tValue = rActiveEntity->GetStatistic(mStatisticIndexes[i]);

        ///--[Alt Weapon Check]
        //--Check if this is an alt-weapon slot. If so, we need to modify the statistics.
        if(rPackage->mIsWeaponAlternate)
        {
            //--Subtract the value by the current weapon.
            int tWeaponSlot = rActiveEntity->GetSlotForWeaponDamage();
            EquipmentSlotPack *rWeaponPackage = rActiveEntity->GetEquipmentSlotPackageI(tWeaponSlot);

            //--If there's a weapon in the main slot:
            if(rWeaponPackage->mEquippedItem)
            {
                tValue = tValue - rWeaponPackage->mEquippedItem->GetStatistic(mStatisticIndexes[i]);
            }

            //--Add the value of the alternate weapon. This will give the player the stats they'd have if they swapped in battle.
            AdventureItem *rReplaceWeapon = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
            if(rReplaceWeapon) tValue = tValue + rReplaceWeapon->GetStatistic(mStatisticIndexes[i]);
        }

        //--If this is a resistance value and over 1000, render "Immune" instead. This also precludes stat comparisons because
        //  a base value over the immunity threshold cannot be modified.
        if(i >= MONOUIEQP_RESISTANCE_BEGIN && tValue >= 1000)
        {
            AdvImages.rFont_Statistics->DrawText(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, xrStringData->str.mTextImmune);
            continue;
        }
        //--Render normally.
        else
        {
            AdvImages.rFont_Statistics->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
        }

        ///--[Stat Comparison]
        //--If replacing, show the stat changes.
        if(mIsReplacing)
        {
            ///--[Setup]
            int tOldStatValue = 0;
            int tNewStatValue = 0;

            ///--[Normal]
            if(mGemCursor == -1)
            {
                //--Stat value of the equipped item.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem) tOldStatValue = rCurrentItem->GetStatistic(mStatisticIndexes[i]);

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewStatValue = rItem->GetStatistic(mStatisticIndexes[i]);
                    }
                }
            }
            ///--[Gem Swap]
            else
            {
                //--Stat value of the gem currently in the slot.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem)
                {
                    AdventureItem *rCurrentGem = rCurrentItem->GetGemInSlot(mGemCursor);
                    if(rCurrentGem)
                    {
                        tOldStatValue = rCurrentGem->GetStatistic(mStatisticIndexes[i]);
                    }
                }

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewStatValue = rItem->GetStatistic(mStatisticIndexes[i]);
                    }
                }
            }

            ///--[Render]
            //--If the value in this slot is different from the old value, then display that.
            if(tOldStatValue != tNewStatValue)
            {
                //--Value is lower:
                if(tNewStatValue < tOldStatValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
                }
                //--Higher:
                else
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
                }

                //--Render.
                AdvImages.rFont_Mainline->DrawTextArgs(cPropertyBaseX + 15, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistics->DrawTextArgs(1232.0f, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue + (tNewStatValue - tOldStatValue));

                //--Clean.
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
            }
        }
    }

    ///--[ ======= Resistances and Damage Bonuses ======= ]
    ///--[Setup]
    //--All statistics after MONOUIEQP_RESISTANCE_BEGIN have both a resistance value and a damage bonus
    //  value. This renders both.
    float cResistX =  944.0f;
    float cDamageX = 1237.0f;
    float cYBegin  =  515.0f;
    float cIconOffY =  10.0f;
    float cTextOffY =   0.0f;

    //--Overrides.
    cIconX = 1098.0f;

    //--Headings.
    mColorHeading.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Statistics->DrawText( 939.0f, 460.0f, 0, 1.0f, xrStringData->str.mTextResists);
    AdvImages.rFont_Statistics->DrawText(1239.0f, 460.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, xrStringData->str.mTextDamage);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    //--Render.
    for(int i = MONOUIEQP_RESISTANCE_BEGIN; i < MONOUIEQP_ICONS_TOTAL; i ++)
    {
        ///--[Setup]
        //--Positioning.
        float cYPos = cYBegin + ((i - MONOUIEQP_RESISTANCE_BEGIN) * cIconH);

        //--Icon.
        MonoImages.rPropertyIcons[i]->Draw(cIconX, cYPos + cIconOffY);

        //--Get all values.
        int tResistance = rActiveEntity->GetStatistic(mStatisticIndexes[i]);
        int tBonusTags = rActiveEntity->GetTagCount(mBonusTagNames[i]);
        int tMalusTags = rActiveEntity->GetTagCount(mMalusTagNames[i]);

        ///--[Alt Weapon Check]
        //--Check if this is an alt-weapon slot. If so, we need to modify the statistics.
        if(rPackage->mIsWeaponAlternate)
        {
            //--Subtract the value by the current weapon.
            int tWeaponSlot = rActiveEntity->GetSlotForWeaponDamage();
            EquipmentSlotPack *rWeaponPackage = rActiveEntity->GetEquipmentSlotPackageI(tWeaponSlot);

            //--If there's a weapon in the main slot:
            if(rWeaponPackage->mEquippedItem)
            {
                tResistance = tResistance - rWeaponPackage->mEquippedItem->GetStatistic(mStatisticIndexes[i]);
                tBonusTags = tBonusTags - rWeaponPackage->mEquippedItem->GetTagCount(mBonusTagNames[i]);
                tMalusTags = tMalusTags - rWeaponPackage->mEquippedItem->GetTagCount(mMalusTagNames[i]);
            }

            //--Add the value of the alternate weapon. This will give the player the stats they'd have if they swapped in battle.
            AdventureItem *rReplaceWeapon = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
            if(rReplaceWeapon)
            {
                tResistance = tResistance + rReplaceWeapon->GetStatistic(mStatisticIndexes[i]);
                tBonusTags = tBonusTags + rReplaceWeapon->GetTagCount(mBonusTagNames[i]);
                tMalusTags = tMalusTags + rReplaceWeapon->GetTagCount(mMalusTagNames[i]);
            }
        }

        ///--[Resistance]
        //--If this is a resistance value and over 1000, render "Immune" instead. This also precludes stat comparisons because
        //  a base value over the immunity threshold cannot be modified.
        if(tResistance >= 1000)
        {
            AdvImages.rFont_Statistics->DrawText(cResistX, cYPos + cTextOffY, 0, 1.0f, xrStringData->str.mTextImmune);
        }
        //--Render normally.
        else
        {
            AdvImages.rFont_Statistics->DrawTextArgs(cResistX, cYPos + cTextOffY, 0, 1.0f, "%i", tResistance);
        }

        ///--[Damage Bonus]
        //--Compute, after alt weapon check is done if needed.
        int tDamageValue = 100 + tBonusTags - tMalusTags;
        if(tDamageValue < 0) tDamageValue = 0;

        //--Render. Only renders when not replacing.
        if(!mIsReplacing)
            AdvImages.rFont_Statistics->DrawTextArgs(cDamageX, cYPos + cTextOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tDamageValue);

        ///--[Stat Comparison]
        //--If replacing, show the stat changes.
        if(mIsReplacing)
        {
            ///--[Setup]
            int tOldResistValue = 0;
            int tNewResistValue = 0;
            int tOldBonusValue = 0;
            int tNewBonusValue = 0;
            int tOldMalusValue = 0;
            int tNewMalusValue = 0;

            ///--[Normal]
            if(mGemCursor == -1)
            {
                //--Stat value of the equipped item.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem)
                {
                    tOldResistValue = rCurrentItem->GetStatistic(mStatisticIndexes[i]);
                    tOldBonusValue = rCurrentItem->GetTagCount(mBonusTagNames[i]);
                    tOldMalusValue = rCurrentItem->GetTagCount(mMalusTagNames[i]);
                }

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewResistValue = rItem->GetStatistic(mStatisticIndexes[i]);
                        tNewBonusValue = rItem->GetTagCount(mBonusTagNames[i]);
                        tNewMalusValue = rItem->GetTagCount(mMalusTagNames[i]);
                    }
                }
            }
            ///--[Gem Swap]
            else
            {
                //--Stat value of the gem currently in the slot.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem)
                {
                    AdventureItem *rCurrentGem = rCurrentItem->GetGemInSlot(mGemCursor);
                    if(rCurrentGem)
                    {
                        tOldResistValue = rCurrentGem->GetStatistic(mStatisticIndexes[i]);
                        tOldBonusValue = rCurrentGem->GetTagCount(mBonusTagNames[i]);
                        tOldMalusValue = rCurrentGem->GetTagCount(mMalusTagNames[i]);
                    }
                }

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewResistValue = rItem->GetStatistic(mStatisticIndexes[i]);
                        tNewBonusValue = rItem->GetTagCount(mBonusTagNames[i]);
                        tNewMalusValue = rItem->GetTagCount(mMalusTagNames[i]);
                    }
                }
            }

            ///--[Render]
            //--If the value in this slot is different from the old value, then display that.
            if(tOldResistValue != tNewResistValue)
            {
                //--Value is lower:
                if(tNewResistValue < tOldResistValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
                }
                //--Higher:
                else
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
                }

                //--Render.
                AdvImages.rFont_Statistics->DrawTextArgs(cResistX + 25.0f, cYPos + cTextOffY - 2.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistics->DrawTextArgs(cResistX + 50.0f, cYPos + cTextOffY,        0, 1.0f, "%i", tResistance + (tNewResistValue - tOldResistValue));

                //--Clean.
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
            }

            //--Always render the damage bonus value, even if it didn't change.
            int tOldDamageValue = tOldBonusValue - tOldMalusValue;
            int tNewDamageValue = tNewBonusValue - tNewMalusValue;

            //--Value is lower:
            if(tNewDamageValue < tOldDamageValue)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
            }
            //--Higher:
            else if(tNewDamageValue > tOldDamageValue)
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
            }
            //--Same.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
            }

            //--Render the difference if nonzero.
            if(tNewDamageValue != tOldDamageValue)
            {
                AdvImages.rFont_Statistics->DrawTextArgs(cIconX+22.0f, cYPos + cTextOffY, 0, 1.0f, "%+i", tNewDamageValue - tOldDamageValue);
            }

            //--Render, clean.
            AdvImages.rFont_Statistics->DrawTextArgs(cDamageX, cYPos + cTextOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tDamageValue + tNewDamageValue - tOldDamageValue);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
        }
    }
}
