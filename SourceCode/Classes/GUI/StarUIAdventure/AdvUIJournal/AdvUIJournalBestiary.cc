//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "AdvUIJournalTypes.h"
#include "EasingFunctions.h"
#include "Subdivide.h"
#include "utf8.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
void AdvUIJournal::SetJournalToBestiaryMode()
{
    //--Clear the bestiary.
    ClearBestiary();

    //--Variables.
    mCurrentMode = ADVMENU_JOURNAL_MODE_BESTIARY;

    //--Run this script to set the bestiary's properties.
    LuaManager::Fetch()->ExecuteLuaFile(xBestiaryResolveScript);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIJournal::CreateBestiaryEntry(const char *pInternalName)
{
    //--Arg check.
    if(!pInternalName) return;

    //--Duplicate check.
    if(mBestiaryEntries->GetElementByName(pInternalName)) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    AdvMenuJournalBestiaryEntry *nEntry = (AdvMenuJournalBestiaryEntry *)starmemoryalloc(sizeof(AdvMenuJournalBestiaryEntry));
    nEntry->Initialize();
    ResetString(nEntry->mInternalName, pInternalName);
    mBestiaryEntries->AddElementAsTail(pInternalName, nEntry, &AdvMenuJournalBestiaryEntry::DeleteThis);
}
void AdvUIJournal::SetBestiaryDisplayName(const char *pInternalName, const char *pDisplayName)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !pDisplayName || !rEntry) return;

    //--Set.
    ResetString(rEntry->mDisplayName, pDisplayName);
}
void AdvUIJournal::SetBestiaryImage(const char *pInternalName, const char *pDLPath, float pX, float pY)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !pDLPath || !rEntry) return;

    //--Retrieve the image.
    rEntry->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    rEntry->mPortraitX = pX;
    rEntry->mPortraitY = pY;
}
void AdvUIJournal::SetBestiaryParagon(const char *pInternalName, const char *pDLPath, float pX, float pY)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !pDLPath || !rEntry) return;

    //--Retrieve the image.
    rEntry->rParagon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    rEntry->mPortraitX = pX;
    rEntry->mPortraitY = pY;
}
void AdvUIJournal::SetBestiaryTurnOrderIco(const char *pInternalName, const char *pDLPath)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !pDLPath || !rEntry) return;

    //--Retrieve the image.
    rEntry->rIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvUIJournal::SetBestiaryKOCount(const char *pInternalName, int pCount)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mDefeatCount = pCount;
}
void AdvUIJournal::SetBestiaryParagonKOCount(const char *pInternalName, int pCount)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mDefeatParagonCount = pCount;
}
void AdvUIJournal::SetBestiaryKOMax(const char *pInternalName, int pCount)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mDefeatMax = pCount;
}
void AdvUIJournal::SetBestiaryCanTransformYou(const char *pInternalName, bool pFlag)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mCanTransformYou = pFlag;
}
void AdvUIJournal::SetBestiaryLevel(const char *pInternalName, int pLevel)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mLevel = pLevel;
}
void AdvUIJournal::SetBestiaryStatistics(const char *pInternalName, int pHP, int pAtk, int pIni, int pAcc, int pEvd, int pPrt)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mShowStatistics = true;
    rEntry->mHP = pHP;
    rEntry->mAtk = pAtk;
    rEntry->mIni = pIni;
    rEntry->mAcc = pAcc;
    rEntry->mEvd = pEvd;
    rEntry->mPrt = pPrt;
}
void AdvUIJournal::SetBestiaryResults(const char *pInternalName, int pExp, int pPlatina)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mExp = pExp;
    rEntry->mPlatina = pPlatina;
}
void AdvUIJournal::SetBestiaryResistances(const char *pInternalName, int pSlash, int pStrike, int pPierce, int pFlame, int pFreeze, int pShock, int pCrusade, int pObscure, int pBleed, int pPoison, int pCorrode, int pTerrify)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->mShowResistances = true;
    rEntry->mSlashRes   = pSlash;
    rEntry->mStrikeRes  = pStrike;
    rEntry->mPierceRes  = pPierce;
    rEntry->mFlameRes   = pFlame;
    rEntry->mFreezeRes  = pFreeze;
    rEntry->mShockRes   = pShock;
    rEntry->mCrusadeRes = pCrusade;
    rEntry->mObscureRes = pObscure;
    rEntry->mBleedRes   = pBleed;
    rEntry->mPoisonRes  = pPoison;
    rEntry->mCorrodeRes = pCorrode;
    rEntry->mTerrifyRes = pTerrify;
}
void AdvUIJournal::AllocateBestiaryDescriptionLines(const char *pInternalName, int pCount)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry) return;

    //--Set.
    rEntry->AllocateDescriptionLines(pCount);
}
void AdvUIJournal::SetBestiaryDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine)
{
    //--Error check.
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!pInternalName || !rEntry || !pDescriptionLine) return;

    //--Range check.
    if(pLine < 0 || pLine >= rEntry->mDescriptionLinesTotal) return;

    //--Set.
    ResetString(rEntry->mDescriptionLines[pLine], pDescriptionLine);
}
void AdvUIJournal::SetBestiaryDescriptionAuto(const char *pInternalName, const char *pDescription)
{
    ///--[Documentation]
    //--Handles automatically building bestiary descriptions. The provided description will be cut into pieces
    //  based on [BR] and \\n tags, and then allocated for display.
    if(!pInternalName || !pDescription) return;

    ///--[Locate Entry]
    AdvMenuJournalBestiaryEntry *rEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementByName(pInternalName);
    if(!rEntry) return;

    ///--[Run Subdivide]
    //--This routine will subdivide the string into a StarLinkedList, which we will use to allocate our descriptions.
    StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(pDescription, SUBDIVIDE_FLAG_COMMON_BREAKPOINTS, NULL, -1.0f);
    int tListSize = tStringList->GetListSize();
    if(tListSize < 1)
    {
        fprintf(stderr, "AdvUIJournal::SetBestiaryDescriptionAuto() - Warning, description subdivision for %s yielded no strings. Failing.\n", pInternalName);
        return;
    }

    //--Allocate.
    rEntry->AllocateDescriptionLines(tListSize);

    //--Set.
    for(int i = 0; i < tListSize; i ++)
    {
        //--Liberate the string.
        tStringList->SetRandomPointerToHead();
        char *rString = (char *)tStringList->LiberateRandomPointerEntry();
        rEntry->mDescriptionLines[i] = rString;
    }

    //--Report.
    if(false && mBestiaryEntries->GetSlotOfElementByName(pInternalName) < 5)
    {
        //--Setup.
        int cMemLen = (int)strlen(pDescription);  //Size of the string in bytes
        int cCharLen = utf8_strlen(pDescription); //Size of the string in unique characters

        //--Print.
        fprintf(stderr, "SetBestiaryDescriptionAuto: Report for %s\n", pInternalName);
        fprintf(stderr, " Description byte length: %i\n", cMemLen);
        fprintf(stderr, " Description character length: %i\n", cCharLen);
        fprintf(stderr, " Unique lines in final: %i\n", rEntry->mDescriptionLinesTotal);
    }

    //--Clean the list up.
    delete tStringList;
}
void AdvUIJournal::ClearBestiary()
{
    mBestiaryEntries->ClearList();
}

///======================================= Core Methods ===========================================
void AdvUIJournal::ComputeBestiaryCursorPosition()
{
    ///--[Setup]
    //--Cursor.
    int tCursor = 0;
    float cNameLen = 100.0f;

    ///--[Type-Specific]
    //--Compute cursor position.
    tCursor = mBestiaryCursor - mBestiaryOffset;

    //--Get entry.
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)mBestiaryEntries->GetElementBySlot(mBestiaryCursor);
    if(!rEntry) return;

    //--Length.
    cNameLen = AdvImages.rFont_Mainline->GetTextWidth(rEntry->mDisplayName);

    ///--[Final Position]
    //--Positions.
    float cLft = cxSelectionLft - 3.0f;
    float cTop = cxSelectionTop + (cxSelectionHei * tCursor) + 0.0f;
    float cRgt = cxSelectionLft + cNameLen + 3.0f;
    float cBot = cxSelectionTop + (cxSelectionHei * tCursor) + (cxSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIJournal::RenderBestiaryMode(float pVisAlpha)
{
    ///--[Enemy Selection]
    //--Handled by subroutine.
    RenderJournalEntryList(mBestiaryEntries, mBestiaryOffset, mBestiaryCursor, pVisAlpha);

    ///--[Selection Information]
    //--Render the image and information.
    AdvMenuJournalBestiaryEntry *rActiveEntry = (AdvMenuJournalBestiaryEntry *)mBestiaryEntries->GetElementBySlot(mBestiaryCursor);
    if(rActiveEntry && strcasecmp(rActiveEntry->mDisplayName, "-----"))
    {
        ///--[Portrait]
        //--Resolve image.
        StarBitmap *rRenderImg = rActiveEntry->rImage;
        if(mShowParagon) rRenderImg = rActiveEntry->rParagon;

        //--Render the portrait behind a stencil.
        if(rRenderImg && mBestiaryImageTimer < ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS)
        {
            //--Stencil setup.
            glEnable(GL_STENCIL_TEST);
            glColorMask(false, false, false, false);
            glDepthMask(false);
            glStencilFunc(GL_ALWAYS, 1, 0xFF);
            glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
            glStencilMask(0xFF);
            AdvImages.rBestiaryPortraitMask->Draw();

            //--Change stencilling to render where the stencil was.
            glColorMask(true, true, true, true);
            glDepthMask(true);
            glStencilFunc(GL_EQUAL, 1, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            glStencilMask(0xFF);

            //--Portrait can fade out.
            float cPct = EasingFunction::QuadraticInOut(mBestiaryImageTimer, ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS);
            if(cPct > 0.0f) StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f - cPct);

            //--Render portrait.
            rRenderImg->Draw(rActiveEntry->mPortraitX, rActiveEntry->mPortraitY);

            //--Disable stencils.
            glDisable(GL_STENCIL_TEST);
            StarlightColor::ClearMixer();
        }

        ///--[Static Parts]
        //--Reset alpha mixer.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pVisAlpha);

        //--Render all static parts over the portrait.
        AdvImages.rBestiaryIconsL->Draw();
        AdvImages.rBestiaryIconsR->Draw();
        AdvImages.rBestiaryNameBanner->Draw();

        ///--[Basic Stats]
        //--Setup.
        float cStatsLft = 538.0f;
        float cStatsTop = 111.0f;
        float cStatsHei =  20.0f;

        //--Level. Always visible.
        AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 0.0f), 0, 1.0f, "%i", rActiveEntry->mLevel);

        //--If the basic stats are hidden:
        if(!rActiveEntry->mShowStatistics)
        {
            for(int i = 1; i < 7; i ++) AdvImages.rFont_Stats->DrawText(cStatsLft, cStatsTop + (cStatsHei * i), 0, 1.0f, "???");
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 7.0f), 0, 1.0f, "%i", rActiveEntry->mExp);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 8.0f), 0, 1.0f, "%i", rActiveEntry->mPlatina);
        }
        //--Basic stats are known:
        else
        {
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 1.0f), 0, 1.0f, "%i", rActiveEntry->mHP);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 2.0f), 0, 1.0f, "%i", rActiveEntry->mAtk);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 3.0f), 0, 1.0f, "%i", rActiveEntry->mIni);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 4.0f), 0, 1.0f, "%i", rActiveEntry->mAcc);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 5.0f), 0, 1.0f, "%i", rActiveEntry->mEvd);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 6.0f), 0, 1.0f, "%i", rActiveEntry->mPrt);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 7.0f), 0, 1.0f, "%i", rActiveEntry->mExp);
            AdvImages.rFont_Stats->DrawTextArgs(cStatsLft, cStatsTop + (cStatsHei * 8.0f), 0, 1.0f, "%i", rActiveEntry->mPlatina);
        }

        ///--[Resistances]
        //--Setup.
        float cResistsRgt = 1300.0f;
        float cResistsTop = 111.0f;
        float cResistsHei =  20.0f;

        //--Resistances are hidden:
        if(!rActiveEntry->mShowResistances)
        {
            for(int i = 0; i < 12; i ++) AdvImages.rFont_Stats->DrawText(cResistsRgt, cResistsTop + (cResistsHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "???");
        }
        //--Resistances are known:
        else
        {
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  0.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mSlashRes,   AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  1.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mStrikeRes,  AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  2.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mPierceRes,  AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  3.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mFlameRes,   AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  4.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mFreezeRes,  AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  5.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mShockRes,   AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  6.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mCrusadeRes, AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  7.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mObscureRes, AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  8.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mBleedRes,   AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei *  9.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mPoisonRes,  AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei * 10.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mCorrodeRes, AdvImages.rFont_Stats);
            RenderResistance(cResistsRgt, cResistsTop + (cResistsHei * 11.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rActiveEntry->mTerrifyRes, AdvImages.rFont_Stats);
        }

        ///--[Defeats / Scenes]
        //--Setup. This is bottom-aligned.
        float cMiscLft =  517.0f;
        float cMiscRgt = 1319.0f;
        float cMiscBot =  506.0f;

        //--How many times the player has defeated this enemy.
        if(!mShowParagon)
        {
            AdvImages.rFont_Stats->DrawTextArgs(cMiscLft, cMiscBot + (cResistsHei * -2.0f), 0, 1.0f, "Times Defeated: %i / %i", rActiveEntry->mDefeatCount, rActiveEntry->mDefeatMax);
        }
        else
        {
            AdvImages.rFont_Stats->DrawTextArgs(cMiscLft, cMiscBot + (cResistsHei * -2.0f), 0, 1.0f, "Times Defeated: %i", rActiveEntry->mDefeatParagonCount);
        }

        //--Whether or not this enemy can TF the player:
        if(rActiveEntry->mCanTransformYou)
        {
            AdvImages.rFont_Stats->DrawText(cMiscLft, cMiscBot + (cResistsHei * -1.0f), 0, 1.0f, "Can Transform You: Yes");
        }
        else
        {
            AdvImages.rFont_Stats->DrawText(cMiscLft, cMiscBot + (cResistsHei * -1.0f), 0, 1.0f, "Can Transform You: No");
        }

        //--Toggle.
        mToggleParagonString->DrawText(cMiscRgt, cMiscBot + (cResistsHei * -1.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Stats);

        //--Paragon sticker:
        if(rActiveEntry->mDefeatParagonCount > 0)
        {
            AdvImages.rParagonSticker->Draw();
        }

        ///--[Name, Description]
        //--Name.
        float cNameX = 918.0f;
        float cNameY = 508.0f;
        AdvImages.rFont_Header->DrawTextArgs(cNameX, cNameY, SUGARFONT_AUTOCENTER_X, 1.0f, rActiveEntry->mDisplayName);

        //--Description.
        float cDescriptionX = 531.0f;
        float cDescriptionY = 552.0f;
        float cDescriptionH = AdvImages.rFont_Mainline->GetTextHeight();

        //--Iterate.
        float tCurY = cDescriptionY;
        for(int i = 0; i < rActiveEntry->mDescriptionLinesTotal; i ++)
        {
            AdvImages.rFont_Mainline->DrawTextArgs(cDescriptionX, tCurY, 0, 1.0f, rActiveEntry->mDescriptionLines[i]);
            tCurY = tCurY + cDescriptionH;
        }

        ///--[Special: Show Full Portrait]
        //--If the player presses a button, they can see the full size image of the enemy.
        if(rRenderImg && mBestiaryImageTimer > 0)
        {
            //--Percentage.
            float cPct = EasingFunction::QuadraticInOut(mBestiaryImageTimer, ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS);

            //--Darkening overlay.
            StarBitmap::DrawFullBlack((cPct * 3.0f) * (0.90f / 3.0f));

            //--Compute position.
            float cIdealX = (VIRTUAL_CANVAS_X * 0.50f) - (rRenderImg->GetTrueWidth()  * 0.50f);
            float cIdealY = (VIRTUAL_CANVAS_Y * 0.50f) - (rRenderImg->GetTrueHeight() * 0.50f);
            float cPortraitX = cIdealX  - ((cIdealX - rActiveEntry->mPortraitX) * (1.0f - cPct));
            float cPortraitY = cIdealY  - ((cIdealY - rActiveEntry->mPortraitY) * (1.0f - cPct));

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);
            rRenderImg->Draw(cPortraitX, cPortraitY);
            StarlightColor::ClearMixer();
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
