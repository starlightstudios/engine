///====================================== StarLumpManager =========================================
//--The manager responsible for controlling SLF files and extracting data from them on-demand.
//  Data is of a generic lump type, which can have subtypes to handle it (or be handled raw).

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[LumpLookup]
//Stores the file cursor for a given name.
typedef struct
{
    uint64_t mPosition;
    char *mName;
}LumpLookup;

///--[RootLump]
//--Stores a set of data, doesn't know the type or organization.
typedef struct RootLump
{
    //--Members
    bool mOwnsData;
    LumpLookup mLookup;
    uint32_t mDataSize;
    uint8_t *mData;

    //--Alternate data storage.
    bool mUsesStarArray;
    StarArray *rDataObject;
    int mStarArrayOffset;

    //--Initialization Function
    void Init()
    {
        mOwnsData = false;
        mLookup.mPosition = 0;
        mLookup.mName = InitializeString("Unnamed Lump");
        mDataSize = 0;
        mData = NULL;
        mUsesStarArray = false;
        rDataObject = NULL;
        mStarArrayOffset = 0;
    }
}RootLump;

///--[PathLookup]
//--Stores the path to a given datafile. Lumps can check the current in-use path.
typedef struct PathLookup
{
    //--Members
    char *mPath;

    //--Functions
    void Initialize()
    {
        mPath = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        PathLookup *rPtr = (PathLookup *)pPtr;
        free(rPtr->mPath);
        free(rPtr);
    }
}PathLookup;

///===================================== Local Definitions ========================================
//--Forward Declarations
struct SMLocationPack;

//--Map Types
#define MAP_INVALID -1
#define MAP_2DLINEART 0
#define MAP_2DTWODEPTH 1
#define MAP_3DVOXEL 2
#define MAP_3DVECTOR 3

#define STANDARD_HEADER "SugarLumpFile100"
#define HEADER_MAX_LEN 80

#define DEFAULT_TILE_SIZE 16

///========================================== Classes =============================================
class StarLumpManager
{
    private:
    ///--[System]
    bool mIsOpen;
    char *mOpenPath;
    int mFileVersion;
    char mHeader[HEADER_MAX_LEN];

    ///--[File]
    VirtualFile *mVFile;

    ///--[Lumps]
    uint32_t mTotalLumps;
    RootLump *mLumpList;

    ///--[New Lumps]
    StarLinkedList *mNewLumpList;

    ///--[Path Lookups]
    PathLookup *rCurrentPathLookup;
    StarLinkedList *mPathLookupList; //PathLookup *, master

    ///--[Delayed Bitmap Loading]
    StarLinkedList *mrDelayedBitmapLoadList; //StarBitmap *, reference

    ///--[Autoload Lump Handling]
    //--Autoloader Data
    StarLinkedList *mAutoLoadLumpData; //char *, master

    //--File Path Data
    StarLinkedList *mFileIdentities; //char *, master

    protected:

    public:
    //--System
    StarLumpManager();
    ~StarLumpManager();
    static void DeleteRootLump(void *pPtr);

    //--Public Variables
    bool mSuppressErrorOnce;
    static bool xIsCaseInsensitive;
    static bool xDebugFlag;
    static int xTileSizeX;
    static int xTileSizeY;

    //--Property Queries
    bool IsFileOpen();
    char *GetOpenPath();
    int GetTotalLumps();
    bool DoesLumpExist(const char *pName);
    char *GetHeaderOf(const char *pName);
    bool DoesPathExist(void *pPtr);
    PathLookup *GetCurrentPathLookup();
    int GetSizeOfLoadQueue();
    bool IsQueuedForLoading(void *pPtr);
    int GetAutoLoadCount();
    const char *GetAutoLoadName(int pSlot);
    const char *GetAutoLoadPath(int pSlot);
    const char *GetFilePath(const char *pFileName);

    //--Manipulators
    void AddBitmapToLoadList(StarBitmap *pImage);
    void RemoveBitmapFromLoadList(void *pPtr);
    void RegisterFileIdentity(const char *pFileName, const char *pFilePath);
    void ReplaceLumpWithDummyImage(const char *pLumpName);

    //--Core Methods
    void PopulateAutoLoad(const char *pLumpName);
    bool LoadNextDelayedBitmap();
    int BinaryGetLookup(const char *pName, int pMin, int pMax);
    bool StandardSeek(const char *pLumpName, const char *pHeaderType);
    bool SlotwiseSeek(int pSlot);
    void Clear();

    //--Image Extraction
    StarBitmapPrecache *GetImageData(const char *pLumpName);
    StarBitmap *GetImage(const char *pLumpName);
    StarBitmap *GetPaddedTileImage(const char *pLumpName);

    //--Lua Tarball
    LuaTarballEntry *GetTarball(int pSlot);

    //--Map Extraction
    int GetMapType(const char *pLumpName);

    //--Output
    void RegisterLump(RootLump *pLump);
    void MergeLumps();
    void Write(const char *pPath);

    //--Update
    //--File I/O
    void Open(const char *pPath);
    void Open(const char *pPath, const char *pHeader);
    void Close();

    //--Drawing
    //--Pointer Routing
    VirtualFile *GetVirtualFile();
    RootLump *GetLumpList();

    //--Static Functions
    static StarLumpManager *Fetch();
    static RootLump *CreateLump(const char *pName, const char *pHeader, uint32_t pExpectedSize);
    static RootLump *CreateLump(const char *pName);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SLF_SetDebugFlag(lua_State *L);
int Hook_SLF_Open(lua_State *L);
int Hook_SLF_Close(lua_State *L);
int Hook_SLF_IsFileOpen(lua_State *L);
int Hook_SLF_GetOpenPath(lua_State *L);
int Hook_SLF_PopulateAutoLoad(lua_State *L);
int Hook_SLF_GetAutoLoadCount(lua_State *L);
int Hook_SLF_GetAutoLoadEntry(lua_State *L);
int Hook_SLF_RegisterFileIdentity(lua_State *L);
int Hook_SLF_GetFilePath(lua_State *L);
