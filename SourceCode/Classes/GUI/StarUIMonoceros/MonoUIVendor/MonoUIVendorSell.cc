//--Base
#include "MonoUIVendor.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

///===================================== Segment Rendering ========================================
void MonoUIVendor::RenderLftSell(float pVisAlpha)
{
    AdvUIVendor::RenderLftSell(pVisAlpha);
}
void MonoUIVendor::RenderTopSell(float pVisAlpha)
{
    AdvUIVendor::RenderTopSell(pVisAlpha);
}
void MonoUIVendor::RenderRgtSell(float pVisAlpha)
{
    AdvUIVendor::RenderRgtSell(pVisAlpha);
}
void MonoUIVendor::RenderBotSell(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();

    ///--[Render]
    //--Tabs, titles.
    RenderModeTabs(cColorAlpha);
    RenderModeTitles(cColorAlpha);

    //--If there are no entries on the item list, render this instead.
    if(rItemList->GetListSize() < 1)
    {
        AdvImages.rFont_Mainline->DrawText(345.0f, 242.0f, 1.0f, 0, xrStringData->str.mTextNothingSell);
    }

    //--Backings. Rendered apart from the entries, allowing the cursor to be over the backing and under
    //  the text entries.
    float cEntrySpcH = 56.0f;
    int tBackingsToRender = rItemList->GetListSize();
    if(tBackingsToRender > cScrollPage) tBackingsToRender = cScrollPage;
    for(int i = 0; i < tBackingsToRender; i ++)
    {
        MonoImages.rOverlay_Option->Draw(0.0f, i * cEntrySpcH);
    }

    //--Cursor.
    MonoImages.rOverlay_Selected->Draw(0.0f, mHighlightPos.mYCur - MonoImages.rOverlay_Selected->GetYOffset());

    //--Render entries.
    RenderInventoryItemListing(mSkip, cScrollPage, cColorAlpha);

    //--Scrollbar.
    if(rItemList->GetListSize() > cScrollPage)
    {
        StandardRenderScrollbar(mSkip, cScrollPage, rItemList->GetListSize() - cScrollPage, 239.0f, 445.0f, false, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    //--Help Strings.
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--This string only renders if character swapping is possible.
    if(mShowSwapArrows)
        mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--This string only renders if multiple compare slots are present.
    if(mCanSwapCompareSlot)
        mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, MonoImages.rFont_Control);


    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderUnalignedSell(float pVisAlpha)
{
    AdvUIVendor::RenderUnalignedSell(pVisAlpha);
}
void MonoUIVendor::RenderSellItem(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--When the player chooses an item to sell, this UI appears over the rest of the UI. It shows
    //  what they are selling and for how much.
    //--Identical to the base version, but uses translated strings.
    if(mSellTimer < 1) return;

    ///--[Position]
    //--Percentage.
    float cSalePct = EasingFunction::QuadraticInOut(mSellTimer, cxAdvSellTicks);

    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha * cSalePct);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cSalePct));

    ///--[Item Resolve]
    //--Get the item package.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mCursor);

    ///--[Rendering]
    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--All information specific to the item is not shown when hiding the UI.
    if(rItem && mIsSellingItem)
    {
        //--Heading.
        mColorHeading.SetAsMixerAlpha(cColorAlpha);
        AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextConfirmSale);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

        //--Centering computations.
        int tTotalItemValue = rItem->GetValue() * mSellQuantity / 2;
        char *tRenderString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mTextSaleDetails, 3, "[sItemName]", rItem->GetDisplayName(), "[iQuantity]", mSellQuantity, "[iSaleVal]", tTotalItemValue);
        float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tRenderString);
        float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

        //--Item Icon
        StarBitmap *rItemIcon = rItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 205.0f);

        //--Item name.
        AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, tRenderString);
        free(tRenderString);
    }

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(579.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
