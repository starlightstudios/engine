-- |[ ====================================== io.printf() ======================================= ]|
--A function identical to the C++ printf, using the formats lua provides for output.
function io.printf(psString, ...)
    io.write(string.format(psString, table.unpack({...}) ) )
end
