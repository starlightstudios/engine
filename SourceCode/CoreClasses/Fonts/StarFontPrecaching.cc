//--Base
#include "StarFont.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarBitmap.h"

//--Definitions
#include "GlDfn.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug Flags]
//#define SUGARFONT_PRECACHE_DEBUG
#ifdef SUGARFONT_PRECACHE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///--[Notes]
//--This is designed to work only in Freetype. Bitmaps don't need it and no others are supported yet.

///--[Common]
void StarFont::ClearPrecache()
{
    if(!mPrecacheData) return;
    glDeleteTextures(1, &mPrecachedHandle);
    for(int i = 0; i < mTotalGlyphs; i ++)
    {
        glDeleteLists(mPrecacheData[i].mGLList, 1);
    }
    free(mPrecacheData);
    mPrecacheData = NULL;
}

#if defined _FONTTYPE_FREETYPE_
void StarFont::PrecacheData(uint8_t pFilterFlag)
{
    ///--[Documentation]
    //--With the font already loaded, place the data in a precached array. The images will all
    //  be amalgamated onto a single texture, and the offsets stored in data packs.
    //--We use a multipass technique to allocate space for all the letters.

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:PrecacheData() - Warning. Attempted to use dummy font %s\n", mLocalName); return; }

    ///--[Setup]
    DebugPrint("StarFont:PrecacheData() - Begin.\n");
    DebugPrint("Filter flag: %i\n", pFilterFlag);

    //--Constants.
    float cMaxSizeX = StarBitmap::xMaxTextureSize;
    float tWidestSoFar = 0.0f; //Used for italics.
    float tTallestSoFar = 0.0f;
    mTallestCharacter = 0.0f;
    float cPadding = 4.0f;
    float cBlackPadding = 0.0f;

    //--Error check. OpenGL specifies this as the minimum max texture size, anything smaller indicates an error.
    if(cMaxSizeX < 1024.0f) cMaxSizeX = 1024.0f;
    DebugPrint("Max texture size: %f\n", cMaxSizeX);

    //--Black padding.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_EDGE)
    {
        cPadding = cPadding + 2.0f;
        cBlackPadding = xOutlineSize;
    }

    //--Monospace storage.
    mMonospacing = xNextMonospace;

    //--If this flag is set, cache the font with exactly one glyph. This makes the font valid but it takes
    //  up minimal memory.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_DUMMY) mTotalGlyphs = 1;

    //--Allocate software space.
    SetMemoryData(__FILE__, __LINE__);
    DebugPrint("Allocating precache data space. Size %i\n", sizeof(StarFontPrecacheData) * mTotalGlyphs);
    mPrecacheData = (StarFontPrecacheData *)starmemoryalloc(sizeof(StarFontPrecacheData) * mTotalGlyphs);

    //--If compiling, allocate space for list metrics.
    if(StarFont::xFontSLFMode == STARFONT_SLF_PREBUILD_COMPILE)
    {
        DebugPrint("Allocating list metric data space. Size %i\n", sizeof(StarFontListMetrics) * mTotalGlyphs);
        mListMetricData = (StarFontListMetrics *)starmemoryalloc(sizeof(StarFontListMetrics) * mTotalGlyphs);
    }

    //--Make sure the GL state is correct.
    DebugPrint("Setting up GL state.\n");
    glEnable(GL_TEXTURE_2D);

    //--Running size counters.
    float tTotalPixelsX = cPadding + cBlackPadding;
    float tTotalPixelsY = cPadding + cBlackPadding;

    ///--[Iteration]
    //--Setup.
    bool tUsesMoreThanOneLine = false;
    uint32_t tGIndex = 0;
    uint32_t tTotal = 0;

    //--Run across every possible glyph, including empty slots.
    DebugPrint("Beginning precache.\n");
    DebugPush(pFilterFlag & SUGARFONT_PRECACHE_DEBUG_FLAG, "Iterating across glyphs.\n");
    int tUnicodeVal = FT_Get_First_Char(rFont, &tGIndex);
    while(tGIndex > 0)
    {
        ///--[Get the Glyph]
        //--If a font does not have a glyph in the given unicode slot, it will come back as 0.
        //DebugPrint(" Glyph %i is unicode %i\n", tTotal, tUnicodeVal);

        ///--[Error]
        //--Index out of range, somehow. Should not be logically possible, unless using dummy
        //  precaching, in which case it saves memory.
        if((int)tTotal >= mTotalGlyphs)
        {
            DebugPrint("  Hit cap. Breaking.\n");
            break;
        }

        ///--[Glyph Exists]
        //--Fetch the letter.
        FT_Load_Char(rFont, tUnicodeVal, FT_LOAD_RENDER);
        FT_GlyphSlot rGlyph = rFont->glyph;

        //--Specify the remap.
        if(tUnicodeVal >= 0)
        {
            mRemaps[tTotal].mGlyphIndex = tTotal;
            mRemaps[tTotal].mUnicodeValue = tUnicodeVal;
            //DebugPrint("  Set remap in slot %i to %i\n", tUnicodeVal, tTotal);
        }

        ///--[Initialization]
        //--Store the offset data.
        mPrecacheData[tTotal].mLftOffset = tTotalPixelsX;
        mPrecacheData[tTotal].mTopOffset = tTotalPixelsY;

        //--Zero the list.
        mPrecacheData[tTotal].mGLList = 0;
        mPrecacheData[tTotal].mGLListItalic = 0;
        mPrecacheData[tTotal].mGLListXMirror = 0;
        mPrecacheData[tTotal].mGLListYMirror = 0;

        //--Fast-access copies of metrics
        mPrecacheData[tTotal].mWidth = rGlyph->bitmap.width;
        mPrecacheData[tTotal].mHeight = rGlyph->bitmap.rows;
        mPrecacheData[tTotal].mAdvanceX = (rGlyph->advance.x >> 6) + cBlackPadding;
        mPrecacheData[tTotal].mHoriBearingX = rGlyph->metrics.horiBearingX >> 6;
        mPrecacheData[tTotal].mHoriBearingY = rGlyph->metrics.horiBearingY >> 6;
        mPrecacheData[tTotal].mFlippedTopOffset = mInternalSize - (rGlyph->metrics.horiBearingY >> 6) + (rGlyph->bitmap.rows * 1.5f);

        //--Zero the kernings.
        memset(mPrecacheData[tTotal].mKerningDistance, 0, sizeof(float) * SUGARFONT_PRECACHE_MAX);

        ///--[Kernings]
        //--Certain letters have certain kerning properties. This is the auto-generated kerning,
        //  which can be replaced with manually set kernings later.
        //--First, Space, which has a fixed kerning.
        if(tUnicodeVal == ' ')
        {
            for(int p = 0; p < SUGARFONT_PRECACHE_MAX; p ++)
            {
                mPrecacheData[tTotal].mKerningDistance[p] = mInternalSize * 0.33f;
            }
        }
        //--These punctuation letters have extra width provided.
        /*else if(tUnicodeVal == '.' || tUnicodeVal == ',' || tUnicodeVal == '\'' || tUnicodeVal == '"' || tUnicodeVal == ';' || tUnicodeVal == ':' || tUnicodeVal == '!')
        {
            for(int p = 0; p < SUGARFONT_PRECACHE_MAX; p ++)
            {
                //--Default.
                mPrecacheData[tTotal].mKerningDistance[p] = mPrecacheData[tTotal].mWidth;

                //--If the target is another punctuation, greatly reduce the bonus.
                if(p == '.' || p == ',' || p == '\'' || p == '"' || p == ';' || p == ':' || p == '!')
                {
                    mPrecacheData[tTotal].mKerningDistance[p] = mPrecacheData[tTotal].mKerningDistance[p] * 0.50f;
                }
                //--Spaces get a larger bonus after periods and commas.
                else if(p == ' ' && (tUnicodeVal == '.' || tUnicodeVal == ','))
                {
                    mPrecacheData[tTotal].mKerningDistance[p] = mPrecacheData[tTotal].mKerningDistance[p] * 1.20f;
                }
                //--All other letters.
                else
                {
                }
            }
        }*/
        else if(true)
        {
            //--Setup.
            FT_Vector tNextVector;

            //--Diagnostics.
            int tThisCharCode = FT_Get_Char_Index(rFont, tUnicodeVal);

            //--Iterate.
            for(int p = 0; p < SUGARFONT_PRECACHE_MAX; p ++)
            {
                int tNextCharCode = FT_Get_Char_Index(rFont, p);
                //int tErrorCode = FT_Get_Kerning(rFont, tTotal, FT_Get_Char_Index(rFont, p), FT_KERNING_DEFAULT, &tNextVector);
                //int tErrorCode = FT_Get_Kerning(rFont, tThisCharCode, tNextCharCode, FT_KERNING_DEFAULT, &tNextVector);
                FT_Get_Kerning(rFont, tThisCharCode, tNextCharCode, FT_KERNING_DEFAULT, &tNextVector);
                mPrecacheData[tTotal].mKerningDistance[p] = (rGlyph->advance.x >> 6) + (tNextVector.x >> 6);
            }
        }
        //--All other letters use their width plus 1 pixel.
        else
        {
            for(int p = 0; p < SUGARFONT_PRECACHE_MAX; p ++)
            {
                //--Default.
                mPrecacheData[tTotal].mKerningDistance[p] = mPrecacheData[tTotal].mWidth + 1.0f;

                //--If the target is punctuation except semicolon, actually provide a penalty to width!
                if(p == '.' || p == ',' || p == '\'' || p == '"' || p == ':' || p == '!')
                {
                    mPrecacheData[tTotal].mKerningDistance[p] = mPrecacheData[tTotal].mKerningDistance[p] * 0.90f;
                }
                //--All other letters.
                else
                {
                }
            }
        }

        ///--[Metrics]
        //--If this is the tallest character, store that for later.
        if(mTallestCharacter < mPrecacheData[tTotal].mHeight && tUnicodeVal < 128) mTallestCharacter = mPrecacheData[tTotal].mHeight;

        //--Position the cursor for this glyph. Go to the next line if it's too large.
        if(rGlyph->bitmap.rows > tTallestSoFar) tTallestSoFar = rGlyph->bitmap.rows;
        if(mPrecacheData[tTotal].mWidth > tWidestSoFar) tWidestSoFar = mPrecacheData[tTotal].mWidth;

        //--Advance the cursor.
        tTotalPixelsX = tTotalPixelsX + mPrecacheData[tTotal].mWidth + cPadding + mEndPadding + cBlackPadding;
        if(tTotalPixelsX > cMaxSizeX)
        {
            tUsesMoreThanOneLine = true;
            tTotalPixelsY = tTotalPixelsY + tTallestSoFar + cPadding + mEndPadding + cBlackPadding + 10.0f;
            tTallestSoFar = rGlyph->bitmap.rows;
            mPrecacheData[tTotal].mLftOffset = cPadding + cBlackPadding + mEndPadding;
            mPrecacheData[tTotal].mTopOffset = tTotalPixelsY;
            tTotalPixelsX = cPadding + cBlackPadding + mEndPadding;
            tTotalPixelsX = tTotalPixelsX + mPrecacheData[tTotal].mWidth + cPadding + mEndPadding + cBlackPadding;
        }

        ///--[Next]
        tUnicodeVal = FT_Get_Next_Char(rFont, tUnicodeVal, &tGIndex);
        tTotal ++;
    }

    //--Debug.
    DebugPrint("Finished assembling glyph data.\n");

    ///--[Create Texture]
    //--Add pixels for the last line.
    tTotalPixelsY = tTotalPixelsY + tTallestSoFar;

    //--Now that we know how big the texture ought to be, create an array that will hold all the data
    //  and pad the edges to cMaxSizeX size.
    if(!tUsesMoreThanOneLine)
    {
        cMaxSizeX = tTotalPixelsX;
        DebugPrint("Single line required. Using X size: %i\n", cMaxSizeX);
    }
    //fprintf(stderr, "Font %s creates precache bitmap of size %.0f %.0f\n", mLocalName, cMaxSizeX, tTotalPixelsY);
    SetMemoryData(__FILE__, __LINE__);
    DebugPrint("Texture size: %i x %i, Memory %i\n", (int)cMaxSizeX, (int)tTotalPixelsY, (int)(sizeof(uint32_t) * cMaxSizeX * tTotalPixelsY));
    uint32_t *tTextureData = (uint32_t *)starmemoryalloc(sizeof(uint32_t) * cMaxSizeX * tTotalPixelsY);
    DebugPrint("Memory address is %p\n", tTextureData);
    memset(tTextureData, 0, sizeof(uint32_t) * cMaxSizeX * tTotalPixelsY);

    //--Setup.
    tGIndex = 0;
    tTotal = 0;

    //--Run across every possible glyph, including empty slots.
    tUnicodeVal = FT_Get_First_Char(rFont, &tGIndex);
    while(tGIndex > 0)
    {
        ///--[Error]
        //--Index out of range, somehow. Should not be logically possible, unless using dummy precaching.
        if((int)tTotal >= mTotalGlyphs)
        {
            DebugPrint("  Hit cap. Breaking.\n");
            break;
        }

        ///--[Glyph Exists]
        //--Fetch the glyph information.
        FT_Load_Char(rFont, tUnicodeVal, FT_LOAD_RENDER);
        FT_GlyphSlot rGlyph = rFont->glyph;

        //--Place the bitmap data into the array. Remember that it's not a single line, but will be
        //  unpacked to a 2D texture!
        for(int y = 0; y < (int)rGlyph->bitmap.rows; y ++)
        {
            //--Compute the Y percentage.
            float cBaselineFactor = 1.0f;
            float tYPercent = (float)y / (float)rGlyph->bitmap.rows;

            //--When downfading, the baseline decreases as we get closer to the bottom of the glyph.
            if(pFilterFlag & SUGARFONT_PRECACHE_WITH_DOWNFADE)
            {
                cBaselineFactor = (xDownfadeBegin) + (tYPercent * (xDownfadeEnd - xDownfadeBegin));
            }

            //--Iterate across the row.
            for(int x = 0; x < (int)rGlyph->bitmap.width; x ++)
            {
                //--Needs a type cast.
                uint8_t *rBuffer = (uint8_t *)rGlyph->bitmap.buffer;

                //--Calculate the position for each pixel.
                int tPosition = (mPrecacheData[tTotal].mLftOffset + x) + ((mPrecacheData[tTotal].mTopOffset + y) * cMaxSizeX);

                //--Get the baseline color.
                uint8_t tBaseline = rBuffer[x + (y * rGlyph->bitmap.width)];

                //--If specified, this font has to have the part between the dot and the line in the ! specially cut off.
                if(tUnicodeVal == '!' && pFilterFlag & SUGARFONT_PRECACHE_WITH_SPECIAL_EXC)
                {
                    if(tYPercent > 0.50f && tYPercent < 0.70f)
                    {
                        tBaseline = 0;
                    }
                }

                //--When using a black outline, the color greys but the opacity stays at 100%.
                if(pFilterFlag & SUGARFONT_PRECACHE_WITH_EDGE)
                {
                    //--Eliminate pixels with a low baseline.
                    if(tBaseline < xBlackOutlineCutoff)
                    {
                        tTextureData[tPosition] = 0x00000000;
                    }
                    //--Pixel displays but it may be mostly grey.
                    else
                    {
                        //--If the baseline is below 192, it becomes 192.
                        if(tBaseline < 255) tBaseline = 255;

                        //--Apply colors.
                        uint8_t tRed = tBaseline * cBaselineFactor;
                        uint8_t tGrn = tBaseline * cBaselineFactor;
                        uint8_t tBlu = tBaseline * cBaselineFactor;
                        uint8_t tAlp = 255;
                        tTextureData[tPosition] = (tAlp << 24) | (tBlu << 16) | (tGrn << 8) | (tRed);
                    }
                }
                //--When not using a black outline, the color is white but the alpha varies.
                else
                {
                    uint8_t tRed = 255 * cBaselineFactor;
                    uint8_t tGrn = 255 * cBaselineFactor;
                    uint8_t tBlu = 255 * cBaselineFactor;
                    uint8_t tAlp = tBaseline;
                    tTextureData[tPosition] = (tAlp << 24) | (tBlu << 16) | (tGrn << 8) | (tRed);
                }
            }
        }

        ///--[Next]
        tUnicodeVal = FT_Get_Next_Char(rFont, tUnicodeVal, &tGIndex);
        tTotal ++;
    }
    DebugPrint("Finished parsing glyph data.\n");

    //--Save some data about the texture.
    mPrecachedSizeX = cMaxSizeX;
    mPrecachedSizeY = tTotalPixelsY;

    ///--[Black Edging]
    //--Puts a black outline around every letter, if flagged.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_EDGE)
    {
        DebugPrint("Applying black outline.\n");
        ApplyBlackOutline(mPrecachedSizeX, mPrecachedSizeY, tTextureData);
    }

    ///--[Texture Upload]
    //--Generate.
    DebugPrint("Generating texture.\n");
    glGenTextures(1, &mPrecachedHandle);
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--Filter using GL_NEAREST for magnification. The font will remain pixellated. Good for small fonts.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_NEAREST)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    //--Linear filtering. Good for fonts that are going to be large on screen.
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    //--Common. These properties do not change with the filter flag.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    //--Upload.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, cMaxSizeX, tTotalPixelsY, 0, GL_RGBA, GL_UNSIGNED_BYTE, tTextureData);

    //--Write a copy to the autobuffer if specified.
    if(mOutputBuffer)
    {
        DebugPrint("Writing data to autobuffer.\n");
        mOutputBuffer->AppendFloat(cMaxSizeX);
        mOutputBuffer->AppendFloat(tTotalPixelsY);
        mOutputBuffer->AppendVoidData(tTextureData, sizeof(uint32_t) * cMaxSizeX * tTotalPixelsY);
    }

    //--Clean.
    DebugPrint("Deallocating.\n");
    free(tTextureData);
    DebugPrint("Uploaded texture.\n");

    ///--[List Generation]
    BuildGlyphLists(tWidestSoFar, tTallestSoFar, pFilterFlag);
    DebugPrint("Generated lists.\n");

    //--Reset texture flags back to their default.
    StarBitmap::RestoreDefaultTextureFlags();
    DebugPop("Finished precaching.\n");
}
void StarFont::BuildGlyphLists(float pWidest, float pTallest, uint8_t pFilterFlag)
{
    ///--[Documentation]
    //--To speed up rendering of glyphs, builds glLists for the variants they need to render with. Most
    //  fonts only use the base list, but some mirrored rendering ones are available if flagged.
    //--Generates GL Lists for every single glyph.

    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:BuildGlyphLists() - Warning. Attempted to use dummy font %s\n", mLocalName); return; }

    ///--[Setup]
    uint32_t tGIndex = 0;
    uint32_t tTotal = 0;

    //--Constants.
    float cBlackPadding = 0.0f;

    //--Black padding.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_EDGE)
    {
        cBlackPadding = xOutlineSize;
    }

    ///--[Iterate]
    //--Run across the glyphs.
    int tUnicodeVal = FT_Get_First_Char(rFont, &tGIndex);
    while(tGIndex > 0)
    {
        ///--[Error]
        //--Index out of range, somehow. Should not be logically possible, unless using dummy precaching.
        if((int)tTotal >= mTotalGlyphs)
        {
            DebugPrint("  Hit cap. Breaking.\n");
            break;
        }

        ///--[Glyph Exists]
        //--Fetch the glyph information.
        FT_Load_Char(rFont, tUnicodeVal, FT_LOAD_RENDER);
        FT_GlyphSlot rGlyph = rFont->glyph;

        //--Temporary Variables
        float tLft = (rGlyph->metrics.horiBearingX >> 6)                       - cBlackPadding;
        float tTop = (mTallestCharacter - (rGlyph->metrics.horiBearingY >> 6)) - cBlackPadding;
        float tRgt = (tLft + rGlyph->bitmap.width) + (cBlackPadding * 2.0f) + mEndPadding;
        float tBot = (tTop + rGlyph->bitmap.rows)  + (cBlackPadding * 2.0f) + mEndPadding;
        if(pFilterFlag & SUGARFONT_PRECACHE_WITH_SPECIAL_S && tUnicodeVal == 'S')
        {
            tTop = tTop + 0.5f;
            tBot = tBot + 0.5f;
        }

        //--Texture coordinates. Remember that OpenGL starts them upside-down.
        float tTrueWid = mPrecacheData[tTotal].mWidth + mEndPadding;
        float tTrueHei = mPrecacheData[tTotal].mHeight + mEndPadding;
        float tTexLft = (mPrecacheData[tTotal].mLftOffset            - cBlackPadding) / mPrecachedSizeX;
        float tTexTop = (mPrecacheData[tTotal].mTopOffset            - cBlackPadding) / mPrecachedSizeY;
        float tTexRgt = (mPrecacheData[tTotal].mLftOffset + tTrueWid + cBlackPadding) / mPrecachedSizeX;
        float tTexBot = (mPrecacheData[tTotal].mTopOffset + tTrueHei + cBlackPadding) / mPrecachedSizeY;

        //--Middle-positions. Used for X/Y mirroring.
        float cMidX = (tLft + tRgt) * 0.50f;
        float cMidY = (tTop + tBot) * 0.50f;
        float cTexMidX = (tTexLft + tTexRgt) * 0.50f;
        float cTexMidY = (tTexTop + tTexBot) * 0.50f;

        //--Special: These characters are right-aligned.
        if(tUnicodeVal == 12300 || tUnicodeVal == 12303)
        {
            float tWid = tRgt - tLft;
            tLft = tLft + (mMonospacing - tWid);
            tRgt = tRgt + (mMonospacing - tWid);
        }

        //--If using precached lists, we can speed up rendering considerably here.
        mPrecacheData[tTotal].mGLList = glGenLists(1);
        glNewList(mPrecacheData[tTotal].mGLList, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
            glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft, tBot);
            glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt, tBot);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);
        glEnd();
        glEndList();

        //--Italics. Moves the top coordinates over a bit.
        if(tUnicodeVal == '*')
        {
            mPrecacheData[tTotal].mGLListItalic = glGenLists(1);
            glNewList(mPrecacheData[tTotal].mGLListItalic, GL_COMPILE);
            glBegin(GL_QUADS);
                glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
                glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft, tBot);
                glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt, tBot);
                glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);
            glEnd();
            glEndList();
        }
        else
        {
            float tQuarterWid = ((float)pWidest * 0.35f) * ((float)mPrecacheData[tTotal].mHeight / (float)pTallest);
            mPrecacheData[tTotal].mGLListItalic = glGenLists(1);
            glNewList(mPrecacheData[tTotal].mGLListItalic, GL_COMPILE);
            glBegin(GL_QUADS);
                glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft + tQuarterWid, tTop);
                glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft +        0.0f, tBot);
                glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt +        0.0f, tBot);
                glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt + tQuarterWid, tTop);
            glEnd();
            glEndList();
        }

        //--Precache the X-Mirror rendering.
        mPrecacheData[tTotal].mGLListXMirror = glGenLists(1);
        glNewList(mPrecacheData[tTotal].mGLListXMirror, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft,  tTexTop); glVertex2f(tLft,  tTop);
            glTexCoord2d(tTexLft,  tTexBot); glVertex2f(tLft,  tBot);
            glTexCoord2d(cTexMidX, tTexBot); glVertex2f(cMidX, tBot);
            glTexCoord2d(cTexMidX, tTexTop); glVertex2f(cMidX, tTop);

            glTexCoord2d(cTexMidX, tTexTop); glVertex2f(cMidX, tTop);
            glTexCoord2d(cTexMidX, tTexBot); glVertex2f(cMidX, tBot);
            glTexCoord2d(tTexLft,  tTexBot); glVertex2f(tRgt,  tBot);
            glTexCoord2d(tTexLft,  tTexTop); glVertex2f(tRgt,  tTop);
        glEnd();
        glEndList();

        //--Precache the Y-Mirror rendering.
        mPrecacheData[tTotal].mGLListYMirror = glGenLists(1);
        glNewList(mPrecacheData[tTotal].mGLListYMirror, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
            glTexCoord2d(tTexLft, cTexMidY); glVertex2f(tLft, cMidY);
            glTexCoord2d(tTexRgt, cTexMidY); glVertex2f(tRgt, cMidY);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);

            glTexCoord2d(tTexLft, cTexMidY); glVertex2f(tLft, cMidY);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tBot);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tBot);
            glTexCoord2d(tTexRgt, cTexMidY); glVertex2f(tRgt, cMidY);
        glEnd();
        glEndList();

        ///--[Store List Metrics]
        //--This is only used when compiling a font.
        if(mListMetricData)
        {
            mListMetricData[tTotal].mLft = tLft;
            mListMetricData[tTotal].mTop = tTop;
            mListMetricData[tTotal].mRgt = tRgt;
            mListMetricData[tTotal].mBot = tBot;
            mListMetricData[tTotal].mTrueWid = tTrueWid;
            mListMetricData[tTotal].mTrueHei = tTrueHei;
            mListMetricData[tTotal].mTexLft = tTexLft;
            mListMetricData[tTotal].mTexTop = tTexTop;
            mListMetricData[tTotal].mTexRgt = tTexRgt;
            mListMetricData[tTotal].mTexBot = tTexBot;
        }

        ///--[Next]
        tUnicodeVal = FT_Get_Next_Char(rFont, tUnicodeVal, &tGIndex);
        tTotal ++;
    }
}

#endif
