//--Base
#include "ScriptObjectBuilder.h"

//--Classes
#include "StarObjectPrototype.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///--[Debug]
//#define SOB_DEBUG
#ifdef SOB_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
ScriptObjectBuilder::ScriptObjectBuilder()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_SCRIPTOBJECTBUILDER;

    ///--[ ======= RootLevel ======== ]
    ///--[ == ScriptObjectBuilder === ]
    ///--[System]
    mCurrentMode = SOB_MODE_PROTOTYPE;
    mItemsPerPage = 1;
    mPrototypeList = new StarLinkedList(true);

    ///--[Prototype Mode]
    mPrototypeCursor = 0;
    mPrototypeSkip = 0;

    ///--[Fields Mode]
    mFieldCursor = 0;
    mFieldSkip = 0;
    rActivePrototype = NULL;

    ///--[Editing Mode]
    mEditCursor = 0;
    mEditingBuffer[0] = '\0';

    ///--[Render Constants]
    cListIndentLines = 2;
    cListBottomY = VIRTUAL_CANVAS_Y * 0.75f; //Point where list entries no longer render.
    cDivisorTop = VIRTUAL_CANVAS_Y * 0.77f;  //Solid white dividing line between list and explanation.
    cDivisorBot = cDivisorTop + 4.0f;
    cExplanationTop = cDivisorBot + 4.0f;    //Y position where explanation begins rendering.

    ///--[Images]
    memset(&Images, 0, sizeof(Images));

    ///--[ ================ Construction ================ ]
    ///--[Images]
    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts.
    Images.Data.rFontMainline = rDataLibrary->GetFont("Flex Menu Main");

    ///--[Verify]
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    ///--[Debug]
    //--If the images failed to resolve, try to establish why by re-running the verifications.
    if(!Images.mIsReady)
    {
        //--Heading.
        fprintf(stderr, "=== Script Object Builder Images Failed to Resolve ===\n");

        //--Re-run verifications with the debug flag set to true.
        fprintf(stderr, "== Base:\n");
        VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), true);
    }
    ///--[Constants]
    else
    {
        //--Resolve items-per-page based on text height.
        float cSizePerLine = Images.Data.rFontMainline->GetTextHeight();
        mItemsPerPage = (int)(cListBottomY / cSizePerLine) - cListIndentLines;

        //--Range check.
        if(mItemsPerPage < 1) mItemsPerPage = 1;
    }
    fprintf(stderr, "Finished construct.\n");
}
ScriptObjectBuilder::~ScriptObjectBuilder()
{
    delete mPrototypeList;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void ScriptObjectBuilder::RegisterPrototype(StarObjectPrototype *pPrototype)
{
    if(!pPrototype) return;
    mPrototypeList->AddElement(pPrototype->GetName(), pPrototype, StarObjectPrototype::DeleteThis);
}

///======================================= Core Methods ===========================================
void ScriptObjectBuilder::AppendToEditBuffer(const char *pString)
{
    ///--[Documentation]
    //--Given a string, appends it to the edit buffer at the current edit cursor. By default, this
    //  is an insertion, moving the string ahead. It also handles adding more to the string than
    //  just one letter. This is done recursively, each call only adds one letter.
    if(!pString || pString[0] == '\0') return;

    //--Do nothing if already at the edge.
    if(mEditCursor >= SOB_EDIT_BUF_SIZE - 1) return;

    ///--[Advance]
    //--Move every letter up by one, dropping the last letter.
    for(int i = SOB_EDIT_BUF_SIZE - 2; i >= mEditCursor+1; i --)
    {
        mEditingBuffer[i] = mEditingBuffer[i-1];
    }

    //--The editing buffer at the edit cursor becomes the first character of the string.
    mEditingBuffer[mEditCursor] = pString[0];

    //--Advance the cursor.
    mEditCursor ++;

    //--Call this function again with the second letter of the buffer.
    AppendToEditBuffer(&pString[1]);
}
void ScriptObjectBuilder::ConditionalAppendToEditBuffer(const char *pString, const char *pString2, bool pCondition)
{
    ///--[Documentation]
    //--Macro, uses the first or second string for appending based on if condition is true or not. Used
    //  to easily handle shift-cases. If the condition is true, uses pString2.
    if(pCondition)
    {
        AppendToEditBuffer(pString2);
    }
    else
    {
        AppendToEditBuffer(pString);
    }
}
void ScriptObjectBuilder::HandleExport()
{
    ///--[Documentation]
    //--Exports the current selected StarObjectPrototype, which consists of calling its managing lua file
    //  with the SOP as the active object and letting it do the hard work.
    if(!rActivePrototype) return;

    //--Set as active object.
    DataLibrary::Fetch()->PushActiveEntity(rActivePrototype);

    //--Call the lua script.
    const char *rExecPath = rActivePrototype->GetExecPath();
    LuaManager::Fetch()->ExecuteLuaFile(rExecPath, 1, "N", (float)SOP_EXECUTE);

    //--Clean.
    DataLibrary::Fetch()->PopActiveEntity();
}

///=================================== Private Core Methods =======================================
///========================================= File I/O =============================================
///====================================== Pointer Routing =========================================
StarFont *ScriptObjectBuilder::GetMainlineFont()
{
    return Images.Data.rFontMainline;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void ScriptObjectBuilder::HookToLuaState(lua_State *pLuaState)
{
    /* SOB_Create()
       Creates a SOB object and sets it as the active level object.*/
    lua_register(pLuaState, "SOB_Create", &Hook_SOB_Create);

    /* SOB_CreateEntry(sName)
       Creates a StarObjectPrototype and registers it to the active ScriptObjectBuilder. */
    lua_register(pLuaState, "SOB_CreateEntry", &Hook_SOB_CreateEntry);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_SOB_Create(lua_State *L)
{
    //SOB_Create()
    ScriptObjectBuilder *nObject = new ScriptObjectBuilder();
    MapManager::Fetch()->ReceiveLevel(nObject);

    return 0;
}
int Hook_SOB_CreateEntry(lua_State *L)
{
    //SOB_CreateEntry(sName)

    //--Push a NULL.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SOB_CreateEntry");

    //--Retrieve level.
    RootLevel *rLevel = MapManager::Fetch()->GetActiveLevel();
    if(rLevel->GetType() != POINTER_TYPE_SCRIPTOBJECTBUILDER) return LuaTypeError("SOB_CreateEntry", L);

    //--Cast.
    ScriptObjectBuilder *rScriptObjectBuilder = (ScriptObjectBuilder *)rLevel;

    //--Create, push.
    StarObjectPrototype *nNewPrototype = new StarObjectPrototype();
    nNewPrototype->SetInternalName(lua_tostring(L, 1));
    rDataLibrary->rActiveObject = nNewPrototype;

    //--Register.
    rScriptObjectBuilder->RegisterPrototype(nNewPrototype);
    return 0;
}
