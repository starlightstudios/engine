//--Base
#include "TilemapActor.h"

//--Classes
#include "ActorNotice.h"
#include "AdvCombat.h"
#include "AdventureDebug.h"
#include "AdventureLevel.h"
#include "Pathrunner.h"
#include "TileLayer.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"
#include "GlDfn.h"
#include "OpenGLMacros.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"

///--[Debug Definitions]
//#define ALLOW_SPEED_TUNING
#define TRUE_OFF_X  4.0f
#define TRUE_OFF_Y  8.0f

#define TA_SHOW_WARNINGS_ON_MISSING_IMAGES

///--[Other Definitions]
//--Flashwhite sequence.
#define FLASHWHITE_TICKS_UP 60
#define FLASHWHITE_TICKS_HOLD 30
#define FLASHWHITE_TICKS_DOWN 60
#define FLASHWHITE_TICKS_TOTAL (FLASHWHITE_TICKS_UP + FLASHWHITE_TICKS_HOLD + FLASHWHITE_TICKS_DOWN)

//--Oscillation constants.
#define OSCILLATION_TICKS 60
#define OSCILLATION_AMT 1.2f

///--[Debug]
//#define TILEMAPACTOR_UPDATE_DEBUG
#ifdef TILEMAPACTOR_UPDATE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
TilemapActor::TilemapActor()
{
    ///--[ ===== Variable Initialization ====== ]
    ///--[ === Standard Variables === ]
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_TILEMAPACTOR;

    ///--[RootEntity]
    //--System
    //--Nameable
    //--Renderable
    //--Data Storage
    //--Public Variables

    ///--[TilemapActor]
    //--System
    mIsDisabled = false;
    mBlockRender = false;
    mIgnoreWorldStop = false;
    mDoesntResetDepthOverride = false;
    mAutoDespawnAfterAnimation = false;
    mCollisionDepth = 0;
    mIgnoreSpecialLighting = false;
    mIgnoreCollisionsWhenMoving = false;
    mIsPartyEntity = false;
    mRendersAfterTiles = false;
    mParagonGrouping = NULL;
    mWholeCollision = false;
    mRenderAsSingleBlock = false;

    //--Instructions
    mInstructionHandlesThisTick = 0;
    mLastInstructionHandle = 0;
    rLastHandledInstruction = NULL;

    //--Position
    mIsClipped = false;
    mFacing = TA_DIR_SOUTH;
    mTileX = 0;
    mTileY = 0;
    mHasSlopeMoveLeft = false;
    mTrueX = 0;
    mTrueY = 0;
    mRemainderX = 0.0f;
    mRemainderY = 0.0f;

    //--Jump
    mJumpTimer = 0;
    mIgnoresGravity = true;
    mVerticalOffY = 0.0f;
    mZSpeed = 0.0f;
    mGravity = TA_DEFAULT_GRAVITY;
    mPlaySoundOnJumping[0] = '\0';
    mPlaySoundOnLanding[0] = '\0';

    //--Activation Handling
    mAutoActivates = false;
    mActivatesByRub = false;
    mRespondsToShot = false;
    mRubTimer = TA_RUB_TICKS;
    mAutoActivateRange = 0.0f;
    mHandlesActivation = false;
    mExtendedActivationDirection = -1;
    mActivationScript = NULL;
    mActivationDim.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--Instruction Handling
    mCurrentInstruction = TA_INS_NONE;
    mInstructionList = new StarLinkedList(true);

    //--Movement Handling
    mIsMoving = false;
    mIsRunning = false;
    mMoveTimer = 0;
    mMoveDirection = -1;
    mMoveSpeed = 1.25f;
    mRunSpeed = mMoveSpeed * 1.5f;
    mStepState = 0;
    mTicksSinceStop = 0;

    //--Movement Specials
    mIsFlying = false;
    mAutoAnimates = false;
    mAutoAnimatesFast = false;
    mYOscillates = false;
    mYOscillateTimer = 0;
    mLastMoveTick = 0;
    mNegativeMoveTimer = 0;

    //--Footstep Sounds
    mNoFootstepSounds = false;
    mWasLastFootstepLeft = true;
    mUseEightRunFrames = false;
    mPreviousFootstepRollL = -1;
    mPreviousFootstepRollR = -1;
    strcpy(mWalkFootstepSound, "Null");
    strcpy(mRunFootstepSound, "World|Footstep%c_%02i");
    memset(mWalkFootstepBuffer, 0, sizeof(char) * STD_MAX_LETTERS);
    memset(mRunFootstepBuffer,  0, sizeof(char) * STD_MAX_LETTERS);

    //--Images
    mIsVoidRiftMode = false;
    mVoidRiftTimer = 0;
    mUsesSpecialIdleImages = false;
    mIsTiny = false;
    mOffsetX = -8.0f;
    mOffsetY = -16.0f;
    mTempOffsetX = 0.0f;
    mTempOffsetY = 0.0f;
    mWalkTicksPerFrame = 10.0f;
    mRunTicksPerFrame = 8.0f;
    mOverrideDepth = -2.0f;
    mOverrideDepthOffset = 0.0f;
    mNoAutomaticShadow = false;
    mDisableMainRender = false;
    mShakeTicks = 0;
    mShakeOffX = 0;
    mShakeOffY = 0;
    mMoveImagesTotal = TA_MOVE_IMG_STANDARD;
    memset(rMoveImages, 0, sizeof(StarBitmap *) * TA_DIR_TOTAL * TA_MOVE_IMG_MAXIMUM);
    memset(rRunImages,  0, sizeof(StarBitmap *) * TA_DIR_TOTAL * TA_MOVE_IMG_MAXIMUM);
    rShadowImg = NULL;
    rManualOverlay = NULL;

    //--Optional Images
    mBreatheTimer = rand() % (int)(TA_BREATHE_MAX * 0.25f);
    memset(rStandingImages, 0, sizeof(StarBitmap *) * TA_DIR_TOTAL);
    memset(rBreatheImages,  0, sizeof(StarBitmap *) * TA_DIR_TOTAL);

    //--Image Path Storage
    mIsStoringImagePaths = false;
    mImagePathStorage = NULL;

    //--Special Images
    mAlwaysFacesUp = false;
    mIsShowingSpecialImage = false;
    mIsShowingFlashwhiteSequence = false;
    mFlashwhiteTimer = 0;
    rCurrentSpecialImage = NULL;
    mSpecialImageList = new StarLinkedList(false);
    mReserveSpecialImage = NULL;
    rActiveAnimation = NULL;
    mSpecialImageAnimationList = new StarLinkedList(true);

    //--Overlay Images
    mIsShowingOverlayImages = false;
    mOverlayImageTimer = 0;
    mOverlayImageTPF = 1.0f;
    mOverlayImagesTotal = 0;
    rOverlayImages = NULL;

    //--Idle Animations
    mHasIdleAnimation = false;
    mIdleTimerCountdown = 0;
    mIdleAnimTimer = 0;
    mIdleTPF = 1;
    mIdleLoopsToFrame = -1;
    mIdleImagesTotal = 0;
    rIdleImages = NULL;

    //--Enemy State
    mIsEnemy = false;
    mIsBagRefill = false;
    mIsItemNode = false;
    mItemNodeKeyword = NULL;
    mIsItemDrop = false;
    mHalveLeashDistance = false;
    mIsWanderNPC = false;
    mIsPredator = false;
    mEnemyToughness = 0;
    mStunTimer = 0;
    mAIState = 0;
    mEnemyLockFacing = -1;
    mLeashX = 0.0f;
    mLeashY = 0.0f;
    mEnemyPackName = NULL;
    mDefeatSceneName = NULL;
    mRetreatSceneName = NULL;
    mVictorySceneName = NULL;
    mEnemyListing = NULL;

    //--Enemy Mugging
    mCanBeMugged = false;
    mHasAnythingToMug = false;
    mHasBeenMugged = false;
    mNeverDropsItemsOnMug = false;
    mMugTimer = 0;
    mMugStunTimer = 0;
    rMugBarEmpty = NULL;
    rMugBarFull = NULL;
    memset(rMugStun, 0, sizeof(StarBitmap *) * TA_MUG_STUN_FRAMES_TOTAL);

    //--Enemy Detection
    mIsBlind = false;
    mViewDistance = 80.0f;
    mViewAngle = 120.0f;
    mCurrentViewAngle = (mFacing * 0.45f) - 90.0f;
    mDropSuspicionTicks = 0;
    mSpottedPlayerTicks = 0;
    mSpottedPlayerTicksMax = 45;
    mSpottingOffsetX = 13.0f;
    mSpottingOffsetY = -4.0f;
    mSpottingCircleX = 17.0f;
    mSpottingCircleY =  2.0f;
    mSpottingCircleR = 13.0f;
    mSpottingCircleT =  4.0f;
    rSpottedExclamation = NULL;
    rSpottedQuestion = NULL;

    ///--Alternate Spot/Alert Formulation
    mEyeSpotting = false;
    mSpotTimer = 0;
    mAlertTimer = 0;
    mSpotFramesTotal = 0;
    mAlertFramesTotal = 0;
    rSpottingFrames = NULL;
    rAlertingFrames = NULL;

    //--Special Enemies Properties
    mIgnoresReinforcement = false;
    mDoesNotTriggerBattles = false;
    mNeverPauses = false;
    mTrueFacing = DIR_UP;
    mIsImmobile = false;
    mIsFast = false;
    mIsRelentless = false;
    mNeverMugAutoWin = false;
    mFollowTarget = NULL;
    mFollowersRegistered = 0;
    mFollowerNameListing = new StarLinkedList(false);
    mCommandsNameListing = new StarLinkedList(false);

    //--Leashing
    mLeashList = new StarLinkedList(true);

    //--Immunity State
    mIsIgnoringPlayer = false;
    mIgnorePlayerCodeList = new StarLinkedList(false);

    //--Reinforcement Stuff
    mLastComputedReinforcement = 0;
    mReinforcementTimer = 0;

    //--AI Variables
    mMustRollWanderPoint = true;
    mWanderX = 0.0f;
    mWanderY = 0.0f;
    mWaitTimer = 0;
    mWaitTimerMax = 1;

    //--Dying Sequence
    mIsDying = false;
    mDeathTimer = 0;

    //--Special UI Variables
    mDisplayStringTimer = 0;
    mClearDisplayStringAfterBattle = false;
    mUIDisplayString = NULL;

    //--Special Frame Auto Cycler
    mCycleSpecialFramesThenDelete = false;
    mCycleTPF = 1;
    mCycleTimer = 0;

    //--Deep Layers
    mIsInDeepLayer = false;
    mDeepLayerCollision = -1;
    mDeepLayerPreventsRun = false;
    mDeepLayerPixelSink = 0;
    mDeepLayerOverlayName[0] = '\0';
    mDeepLayerWalkOverride[0] = '\0';
    mDeepLayerRunOverride[0] = '\0';

    ///--[AI]
    //--Chasing.
    mEnemyAIType = TA_AI_USE_GUARD_AI;
    mChaseTimeRemaining = 0;
    mLostPlayerTimer = 0;
    mExternalGuardModeCooldown = 0;
    mChaseWaitTicks = 0;
    mLastSawPlayerX = 0.0f;
    mLastSawPlayerY = 0.0f;
    mChaseSearchX = 0;
    mChaseSearchY = 0;
    mChaseWalkDist = 0;
    mHardChaseTimer = 0;
    mCommandIssueCooldown = 0;
    mChasePathrunner = NULL;
    mChaseInstructions = NULL;

    //--Leashing.
    mUseDumbLeash = false;
    mSmartLeashWalkDist = 0;
    mLeashPathrunner = NULL;
    mLeashInstructions = NULL;

    ///--[Public Variables]
    mRanLastTick = false;

    ///--[ ==== Pathing Variables === ]
    //--Common Variables
    mDropPatrolTicks = 0;
    mIsPatrolling = false;
    mUseStopOnEachNode = false;
    mNeverRefaceOnNode = false;
    mPatrolTicksLeft = 0;
    mPatrolNodesTotal = 0;
    mPatrolNodeCurrent = 0;
    mPatrolNodeList = NULL;

    //--Flags for Smart/Advanced
    mPathToAnyNode = false;

    //--Smart Patrol
    mIsSmartPatrol = false;
    mSmartPatrolDistanceLeft = 0.0f;
    mSmartPatrolInstructions = NULL;

    //--Advanced Pathing
    mIsAdvancedPatrol = false;
    mPathTicksSoFar = 0;
    mMinimumPathTicks = 15;
    mPathRunner = NULL;

    ///--[ == Death Images Checker == ]
    if(!xHasResolvedPoofImages)
    {
        //--Clear.
        memset(xrPoofImages, 0, sizeof(StarBitmap *) * TA_DEATH_FRAMES_TOTAL);

        //--Resolve.
        DataLibrary *rDataLibrary = DataLibrary::Fetch();
        xrPoofImages[0] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/0");
        xrPoofImages[1] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/1");
        xrPoofImages[2] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/2");
        xrPoofImages[3] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/3");
        xHasResolvedPoofImages = VerifyStructure(xrPoofImages, sizeof(StarBitmap *) * TA_DEATH_FRAMES_TOTAL, sizeof(void *));
    }

    ///--[ ==== Toughness Colors ==== ]
    if(!xHasBuiltToughnessLookups)
    {
        //--Flag.
        xHasBuiltToughnessLookups = true;

        //--Set.
        xToughnessLookups[0].SetRGBF(0.0f, 0.5f, 0.2f);
        xToughnessLookups[1].SetRGBF(0.6f, 0.0f, 0.0f);
        xToughnessLookups[2].SetRGBF(0.4f, 0.1f, 0.8f);
    }

    //--Resolve system images.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rSpottedExclamation = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Spotted/Exclamation");
    rSpottedQuestion    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Spotted/Question");
    rMugBarEmpty        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/BarEmpty");
    rMugBarFull         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/BarFull");
    rMugStun[0]         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/Stun0");
    rMugStun[1]         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/Stun1");
    rMugStun[2]         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/Stun2");

    ///--[ == Storing Image Paths === ]
    //--If this flag is set, this actor needs to store image paths.
    if(xShouldStoreImagePaths)
    {
        mIsStoringImagePaths = true;
        SetMemoryData(__FILE__, __LINE__);
        mImagePathStorage = (TA_Image_Resolve_Pack *)starmemoryalloc(sizeof(TA_Image_Resolve_Pack));
        mImagePathStorage->Initialize();
    }
}
TilemapActor::~TilemapActor()
{
    ///--[ === Standard Variables === ]
    free(mItemNodeKeyword);
    free(mParagonGrouping);
    delete mInstructionList;
    free(mActivationScript);
    delete mSpecialImageList;
    free(rOverlayImages);
    free(mReserveSpecialImage);
    delete mSpecialImageAnimationList;
    free(rIdleImages);
    free(mEnemyPackName);
    delete mEnemyListing;
    delete mIgnorePlayerCodeList;
    delete mLeashList;
    if(mImagePathStorage)
    {
        mImagePathStorage->Delete();
        free(mImagePathStorage);
    }
    free(mFollowTarget);
    delete mCommandsNameListing;
    delete mFollowerNameListing;
    free(mDefeatSceneName);
    free(mRetreatSceneName);
    free(mVictorySceneName);
    free(mUIDisplayString);
    free(rSpottingFrames);
    free(rAlertingFrames);

    ///--[AI]
    delete mChaseInstructions;
    delete mLeashInstructions;

    ///--[ ==== Pathing Variables === ]
    for(int i = 0; i < mPatrolNodesTotal; i ++) free(mPatrolNodeList[i]);
    free(mPatrolNodeList);
    delete mSmartPatrolInstructions;
    delete mPathRunner;
}

///--[Private Statics]
//--Images used when an enemy dies. These will clear to NULL if they haven't been resolved yet, at which
//  point the program tries to resolve them. If it fails (because they weren't loaded) this will still
//  be populated by NULL, so always check to make sure they exist!
bool TilemapActor::xHasResolvedPoofImages = false;
StarBitmap *TilemapActor::xrPoofImages[TA_DEATH_FRAMES_TOTAL];

///--[Public Statics]
//--Flag used to prevent double-render. Because the TiledLevel has depth sorting and per-depth tiles,
//  and uses scaling, entities should render there and not in the EntityManager's render cycle.
//  This flag prevents rendering during the EM's cycle.
bool TilemapActor::xAllowRender = false;

//--When true, enemies will always ignore the player. Used by field abilities.
bool TilemapActor::xAlwaysIgnorePlayer = false;

//--Flag used for special entities that render AFTER all tile rendering is completed. Used for some special effect overlays.
bool TilemapActor::xIsRenderingBeforeTiles = false;

//--Whether or not to render the viewcone. Can be toggled from the options menu.
//  The count tracks how many were rendered this tick, can be used to reduce complexity
//  to lower FPS loss.
bool TilemapActor::xRenderViewcone = true;
int TilemapActor::xViewconeCount = 0;
int TilemapActor::xViewconeCountLastTick = 0;

//--DataLibrary path for shadows when creating enemies. This is the standard shadow, individual enemies can
//  override their shadow after creation.
char *TilemapActor::xEnemyShadowPath = InitializeString("None");

//--Font used to render the reinforcement counter that appears above the enemies when pulsing reinforcements.
//  Gets set by the AdventureCombatUI when it resolves its data.
StarFont *TilemapActor::xrReinforcementFont = NULL;

//--Stores color outlines for tougher enemies.
bool TilemapActor::xHasBuiltToughnessLookups = false;
StarlightColor TilemapActor::xToughnessLookups[3];

//--Indicates the number of footstep sounds loaded into the audio manager. There must be an equal
//  number for both left and right steps. If 0 or lower, footsteps do not play.
int TilemapActor::xMaxFootstepSounds = 1;

//--When set to true, entities will store the paths of images they are sent. These can be used to
//  resolve the images later, because they haven't loaded yet.
bool TilemapActor::xShouldStoreImagePaths = false;

//--When entities are autofiring, this cooldown makes sure they don't all fire on the same tick and hang the game
//  if a whole bunch are in the same area. Typically only the first actor in range fires their script.
int TilemapActor::xAutofireCooldown = 0;

//--When an enemy gets mugged, this is set to true and the enemy script is called. The script should
//  return how much cash/xp/jp/items the enemy had.
//--There are slots for up to
bool TilemapActor::xIsMugCheck = false;
int TilemapActor::xMugPlatina = 0;
int TilemapActor::xMugExperience = 0;
int TilemapActor::xMugJobPoints = 0;
StarLinkedList *TilemapActor::xMugItemList = new StarLinkedList(false);

//--If the actor who owns an entity was mugged, during spawning this flag is set to true.
bool TilemapActor::xActorWasMugged = false;

//--Range that combat triggers. Can be modified, default is 8.0f (half a tile).
float TilemapActor::xCombatTriggerRange = TA_ENCOUNTER_DIST_DEFAULT;

//--When the player is moving, this is true. Otherwise it's false. Used for "bump" activation.
bool TilemapActor::xIsPlayerCollisionCheck = false;
int TilemapActor::xPlayerFacing = TA_DIR_SOUTH;

//--If true, CanHandleInstructions() will always return true. Used for some instances of cutscene acceleration.
bool TilemapActor::xAlwaysRunEvents = false;

//--If true, the current pulse is not a combat pulse. This can be true for mugging or auto-win checks.
bool TilemapActor::xPulseIsNotCombat = false;

//--Where on the hard drive to call, as a lua file, for the function BuildGraphicsFromName().
char *TilemapActor::xBuildGraphicsFromNamePath = NULL;

//--Counts how many times entities have run a path pulse this tick. Resets to zero at the end of a tick.
int TilemapActor::xActorPathPulsesThisTick = 0;

//--Counts how many actors have run their path pulses this tick. Resets to zero at the start of a tick.
//  Used for diagnostics.
int TilemapActor::xActorsPathingThisTick = 0;

//--Stores translation data. Should point to a data block registered to the TranslationManager.
TA_Strings *TilemapActor::xrStringData = NULL;

///==================================== Subclass Functions ========================================
TA_Strings::TA_Strings()
{
    //--Local name: "TilemapActor"
    str.mTurnCount        = InitializeString("TilemapActor_mTurnCount");                        //"[iTurns] turns!"
    str.mPreemptiveStrike = InitializeString("TilemapActor_mPreemptiveStrike");                 //"Preemptive Strike!"
    TilemapActor::xrStringData = this;
}
TA_Strings::~TA_Strings()
{
    for(int i = 0; i < TA_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int TA_Strings::GetSize()
{
    return TA_STRINGS_TOTAL;
}
void TA_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= TA_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
bool TilemapActor::CanHandleInstructions(void *pPtr)
{
    //--We can handle as many instructions per tick as CutsceneManager::xUpdatesPerEntityThisTick.
    //  Therefore, we count how many we've handled thus far this tick.
    //--If mLastInstructionHandle is below the current tick counter, then zero instructions have
    //  been handled this tick.
    if(mLastInstructionHandle < Global::Shared()->gTicksElapsed)
    {
        return true;
    }

    //--We've already handled one instruction this tick, which will have set the rLastHandledInstruction
    //  pointer. The requesting event must be the same event in order to continue executing.
    if(rLastHandledInstruction != pPtr)
    {
        return false;
    }

    //--Check if we've reached the cap. The cap can change during execution.
    if(mInstructionHandlesThisTick < CutsceneManager::xUpdatesPerEntityThisTick && !xAlwaysRunEvents)
    {
        return true;
    }

    //--Max instruction cap reached.
    return false;
}
int TilemapActor::GetX()
{
    return mTileX;
}
int TilemapActor::GetY()
{
    return mTileY;
}
float TilemapActor::GetWorldX()
{
    return mTrueX + mTempOffsetX;
}
float TilemapActor::GetWorldY()
{
    return mTrueY + mTempOffsetY;
}
float TilemapActor::GetRemainderX()
{
    return mRemainderX;
}
float TilemapActor::GetRemainderY()
{
    return mRemainderY;
}
float TilemapActor::GetRenderDepth()
{
    //--Return the override immediately.
    if(mOverrideDepth != -2.0f) return mOverrideDepth;

    //--Compute.
    float tRenderDepth = DEPTH_MIDGROUND + (mTrueY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
    tRenderDepth = tRenderDepth + (DEPTH_PER_TILE * (float)mCollisionDepth * 2.0f) + mOverrideDepthOffset;
    return tRenderDepth;
}
int TilemapActor::GetFacing()
{
    return mFacing;
}
int TilemapActor::GetMoveTimer()
{
    return mMoveTimer;
}
bool TilemapActor::IsHandlingInstruction()
{
    if(mCurrentInstruction == TA_INS_NONE) return false;
    return true;
}
float TilemapActor::GetMoveSpeed()
{
    return mMoveSpeed;
}
float TilemapActor::GetFrameSpeed()
{
    return mWalkTicksPerFrame;
}
float TilemapActor::GetRunFrameSpeed()
{
    return mRunTicksPerFrame;
}
int TilemapActor::GetCollisionDepth()
{
    return mCollisionDepth;
}
bool TilemapActor::IsPositionClipped(int pX, int pY)
{
    ///--[Documentation]
    //--Replies whether or not this entity is blocking this position. Some entities never clip the
    //  player while others reply to multiple positions.
    if(!mIsClipped) return false;

    ///--[Player Interaction]
    //--If this flag is set, and the player is the one moving, then the player may activate this entity
    //  by running into it.
    if(mActivatesByRub && xIsPlayerCollisionCheck)
    {
        //--Normal, collision is smaller than a full tile:
        if(!mWholeCollision)
        {
            if(pX >= mTrueX && pX <= mTrueX + TA_SIZE && pY >= mTrueY && pY <= mTrueY + TA_SIZE)
            {
                return true;
            }
            return false;
        }

        //--Special: Whole collision, used by pushable blocks.
        if(pX >= mTrueX-4 && pX <= mTrueX + 16-5 && pY >= mTrueY-9 && pY <= mTrueY + 16-9)
        {
            return true;
        }
        return false;
    }
    ///--[Normal Collision]
    else
    {
        //--Normal, collision is smaller than a full tile:
        if(!mWholeCollision)
        {
            return (pX >= mTrueX && pX <= mTrueX + TA_SIZE && pY >= mTrueY && pY <= mTrueY + TA_SIZE);
        }

        //--Special: Whole collision, used by pushable blocks.
        return (pX >= mTrueX-4 && pX <= mTrueX + 16-5 && pY >= mTrueY-9 && pY <= mTrueY + 16-9);
    }
}
bool TilemapActor::IsPositionClippedOverride(int pX, int pY)
{
    //--Strict interpretation of IsPositionClipped. Ignores the mIsClipped flag and just asks for a collision check.
    return (pX >= mTrueX && pX <= mTrueX + TA_SIZE && pY >= mTrueY && pY <= mTrueY + TA_SIZE);
}
float TilemapActor::GetMovementLastTick()
{
    return mAmountMovedLastTick;
}
bool TilemapActor::IsPartyEntity()
{
    return mIsPartyEntity;
}
float TilemapActor::GetViewDistance()
{
    return mViewDistance;
}
bool TilemapActor::WasMugged()
{
    return mHasBeenMugged;
}
bool TilemapActor::AlwaysFaceUp()
{
    return mAlwaysFacesUp;
}
bool TilemapActor::MugBlocksAutoWin()
{
    return mNeverMugAutoWin;
}
bool TilemapActor::ShouldRubActivate()
{
    //--If only manual activation is allowed, always fail.
    bool tBlocksActivateManually = OptionsManager::Fetch()->GetOptionB("Blocks Activate Manually");
    if(tBlocksActivateManually) return false;

    //--Otherwise, go by the local flag.
    return (mActivatesByRub && mRubTimer < 1);
}
bool TilemapActor::RespondsToShot()
{
    return mRespondsToShot;
}
bool TilemapActor::IsFlashingWhite()
{
    return mIsShowingFlashwhiteSequence;
}
bool TilemapActor::NeverDropsItemsOnMug()
{
    return mNeverDropsItemsOnMug;
}
int TilemapActor::GetMovementFramesTotal()
{
    return mMoveImagesTotal;
}
int TilemapActor::GetExtendedActivate()
{
    //--Note: Returns -1 if no extended activation.
    return mExtendedActivationDirection;
}
bool TilemapActor::HasSpecialFrame(const char *pName)
{
    return (mSpecialImageList->GetSlotOfElementByName(pName) != -1);
}
bool TilemapActor::IsShowingSpecialFrame()
{
    return mIsShowingSpecialImage;
}
const char *TilemapActor::GetNameOfSpecialFrame()
{
    //--No special frame:
    if(!mIsShowingSpecialImage || !rCurrentSpecialImage) return "Null";

    //--Search for the special image on the list.
    int tSlot = mSpecialImageList->GetSlotOfElementByPtr(rCurrentSpecialImage);
    if(tSlot < 0) return "Null";

    //--Retrive.
    return mSpecialImageList->GetNameOfElementBySlot(tSlot);
}

///======================================== Manipulators ==========================================
void TilemapActor::SetItemNodeKeyword(const char *pKeyword)
{
    ResetString(mItemNodeKeyword, pKeyword);
}
void TilemapActor::SetBlockRender(bool pFlag)
{
    mBlockRender = pFlag;
}
void TilemapActor::SetParagonGrouping(const char *pGrouping)
{
    ResetString(mParagonGrouping, pGrouping);
}
void TilemapActor::SetDisable(bool pIsDisabled)
{
    mIsDisabled = pIsDisabled;
}
void TilemapActor::SetRendersAfterTiles(bool pFlag)
{
    mRendersAfterTiles = pFlag;
}
void TilemapActor::SetIgnoreDepthOverride(bool pFlag)
{
    mDoesntResetDepthOverride = pFlag;
}
void TilemapActor::FlagHandledInstruction(void *pPtr)
{
    //--If the mLastInstructionHandle value matches the current tick, then we're handling more than one
    //  instruction. Otherwise, reset the instruction count.
    if(mLastInstructionHandle < Global::Shared()->gTicksElapsed)
    {
        mInstructionHandlesThisTick = 0;
        rLastHandledInstruction = pPtr;
        mLastInstructionHandle = Global::Shared()->gTicksElapsed;
    }
    //--Otherwise, increment the number of instructions handled.
    else
    {
        mInstructionHandlesThisTick ++;
    }
}
void TilemapActor::OverrideInstructionHandle()
{
    mInstructionHandlesThisTick = 0;
    rLastHandledInstruction = NULL;
    mLastInstructionHandle = 0;
}
void TilemapActor::SetRunning(bool pFlag)
{
    mIsRunning = pFlag;
}
void TilemapActor::SetCollisionFlag(bool pFlag)
{
    mIsClipped = pFlag;
}
void TilemapActor::SetPosition(float pX, float pY)
{
    mTileX = (int)pX;
    mTileY = (int)pY;
    mTrueX = (pX * TileLayer::cxSizePerTile) + TRUE_OFF_X;
    mTrueY = (pY * TileLayer::cxSizePerTile) + TRUE_OFF_Y;
    if(!mDoesntResetDepthOverride) mOverrideDepth = -2.0f; //In party follower cases, this gets reset shortly after.
}
void TilemapActor::SetPositionByPixel(float pX, float pY)
{
    mTileX = (int)pX / TileLayer::cxSizePerTile;
    mTileY = (int)pY / TileLayer::cxSizePerTile;
    mTrueX = pX;
    mTrueY = pY;
    if(!mDoesntResetDepthOverride) mOverrideDepth = -2.0f; //In party follower cases, this gets reset shortly after.
}
void TilemapActor::SetPositionByEntity(const char *pEntityName)
{
    //--Positions by the given entity's position as opposed to fixed coordinates.
    if(!pEntityName) return;

    //--Setup.
    float sX, sY;
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;

    //--Level provides coordinates.
    rActiveLevel->GetLocationByName(pEntityName, sX, sY);
    SetPositionByPixel(sX + (TileLayer::cxSizePerTile * 0.25f), sY - (TileLayer::cxSizePerTile * 0.50f));
}
void TilemapActor::SetRemainders(float pX, float pY)
{
    mRemainderX = pX;
    mRemainderY = pY;
}
void TilemapActor::SetOffsets(float pXOffset, float pYOffset)
{
    //--Defaults are -8.0, -16.0
    mOffsetX = pXOffset;
    mOffsetY = pYOffset;
}
void TilemapActor::SetDisableMainRender(bool pFlag)
{
    mDisableMainRender = pFlag;
}
void TilemapActor::SetManualRenderOverlay(const char *pDLPath)
{
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rManualOverlay = NULL;
        return;
    }
    rManualOverlay = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void TilemapActor::SetMoveImage(int pDirection, int pSlot, const char *pDLPath)
{
    ///--[Documentation]
    //--Set a moving image. Used when the entity is moving (obviously).
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_MAXIMUM) return;

    //--If the path is "Null" or NULL, clear the entry.
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rMoveImages[pDirection][pSlot] = NULL;
        return;
    }

    //--Set.
    rMoveImages[pDirection][pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    ///--[Path Storage]
    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rMoveImages[pDirection][pSlot] && mImagePathStorage)
    {
        strncpy(mImagePathStorage->mMoveImages[pDirection][pSlot], pDLPath, TA_DL_PATH_LEN);
    }
    ///--[Error Reporting]
    //--If the image fails to resolve, we're not storing paths, and the path was not "NULL" then bark
    //  a warning. Optional.
    #if defined TA_SHOW_WARNINGS_ON_MISSING_IMAGES
    else if(!rMoveImages[pDirection][pSlot])
    {
        fprintf(stderr, "TilemapActor:SetMoveImage: Warning, entity %s failed to set move image %i %i, %s - not found.\n", mLocalName, pDirection, pSlot, pDLPath);
    }
    #endif
}
void TilemapActor::SetRunImage(int pDirection, int pSlot, const char *pDLPath)
{
    ///--[Documentation]
    //--Set a running image. Used when the entity is running. Optional, if running images are not
    //  present then the equivalent move image is used instead.
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_MAXIMUM) return;

    //--If the path is "Null" or NULL, clear the entry.
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rRunImages[pDirection][pSlot] = NULL;
        return;
    }

    //--Set.
    rRunImages[pDirection][pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    ///--[Path Storage]
    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rRunImages[pDirection][pSlot] && mImagePathStorage)
    {
        strncpy(mImagePathStorage->mRunImages[pDirection][pSlot], pDLPath, TA_DL_PATH_LEN);
    }
    ///--[Error Reporting]
    //--If the image fails to resolve, we're not storing paths, and the path was not "NULL" then bark
    //  a warning. Optional.
    #if defined TA_SHOW_WARNINGS_ON_MISSING_IMAGES
    else if(!rRunImages[pDirection][pSlot])
    {
        fprintf(stderr, "TilemapActor:SetRunImage: Warning, entity %s failed to set run image %i %i, %s - not found.\n", mLocalName, pDirection, pSlot, pDLPath);
    }
    #endif
}
void TilemapActor::SetStandingImage(int pDirection, const char *pDLPath)
{
    ///--[Documentation]
    //--Sets an idle image. This image appears if the character is facing the given direction and
    //  not moving. If not present, the 0th move frame in that direction is used instead.
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;

    //--If the path is "Null" or NULL, clear the entry.
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rStandingImages[pDirection] = NULL;
        return;
    }

    //--Set.
    rStandingImages[pDirection] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    ///--[Path Storage]
    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rStandingImages[pDirection] && mImagePathStorage)
    {
        strncpy(mImagePathStorage->mStandingImages[pDirection], pDLPath, TA_DL_PATH_LEN);
    }
    ///--[Error Reporting]
    //--If the image fails to resolve, we're not storing paths, and the path was not "NULL" then bark
    //  a warning. Optional.
    #if defined TA_SHOW_WARNINGS_ON_MISSING_IMAGES
    else if(!rStandingImages[pDirection])
    {
        fprintf(stderr, "TilemapActor:SetIdleImage: Warning, entity %s failed to set idle image %i, %s - not found.\n", mLocalName, pDirection, pDLPath);
    }
    #endif
}
void TilemapActor::SetBreatheImage(int pDirection, const char *pDLPath)
{
    ///--[Documentation]
    //--Sets a breathing image. This appears if the character stands still for several ticks and alternates with
    //  the idle (or move0) image. Optional.
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;

    //--If the path is "Null" or NULL, clear the entry.
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rBreatheImages[pDirection] = NULL;
        return;
    }

    //--Set.
    rBreatheImages[pDirection] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    ///--[Path Storage]
    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rBreatheImages[pDirection] && mImagePathStorage)
    {
        strncpy(mImagePathStorage->mBreatheImages[pDirection], pDLPath, TA_DL_PATH_LEN);
    }
    ///--[Error Reporting]
    //--If the image fails to resolve, we're not storing paths, and the path was not "NULL" then bark
    //  a warning. Optional.
    #if defined TA_SHOW_WARNINGS_ON_MISSING_IMAGES
    else if(!rBreatheImages[pDirection])
    {
        fprintf(stderr, "TilemapActor:SetBreatheImage: Warning, entity %s failed to set breathe image %i, %s - not found.\n", mLocalName, pDirection, pDLPath);
    }
    #endif
}
void TilemapActor::SetShadow(const char *pPath)
{
    rShadowImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void TilemapActor::SetMoveSpeed(float pValue)
{
    mMoveSpeed = pValue;
    if(mMoveSpeed < 0.10f) mMoveSpeed = 0.10f;
}
void TilemapActor::SetFrameSpeed(float pValue)
{
    mWalkTicksPerFrame = pValue;
    if(mWalkTicksPerFrame < 1.0f) mWalkTicksPerFrame = 1.0f;
}
void TilemapActor::SetRunFrameSpeed(float pValue)
{
    mRunTicksPerFrame = pValue;
    if(mRunTicksPerFrame < 1.0f) mRunTicksPerFrame = 1.0f;
}
void TilemapActor::SetActivesByRub(bool pFlag)
{
    mActivatesByRub = pFlag;
}
void TilemapActor::SetActivationScript(const char *pPath)
{
    //--Note: Implicitly activates activation if the script is not null.
    if(pPath && !strcasecmp(pPath, "Null"))
    {
        ResetString(mActivationScript, NULL);
        return;
    }

    //--Normal case.
    ResetString(mActivationScript, pPath);
}
void TilemapActor::SetExtendedActivationDirection(int pValue)
{
    mExtendedActivationDirection = pValue;
}
void TilemapActor::SetFacing(int pDirection)
{
    //--Note: If out of range, facing is not affected.
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;
    mFacing = pDirection;
    mCurrentViewAngle = (mFacing * 45.0f) - 90.0f;
}
void TilemapActor::SetFacingToNPC(const char *pName)
{
    //--Looks at the NPC with the provided name. If they don't exist, does nothing.
    RootEntity *rCheckActor = EntityManager::Fetch()->GetEntity(pName);
    if(!rCheckActor || !rCheckActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return;

    //--Right type, face this entity.
    TilemapActor *rTilemapActor = (TilemapActor *)rCheckActor;
    int tTargetX = rTilemapActor->GetWorldX();
    int tTargetY = rTilemapActor->GetWorldY();
    SetFacingToPoint(tTargetX, tTargetY);
}
void TilemapActor::SetFacingToPoint(int pX, int pY)
{
    //--Faces a position. Does nothing if that position happens to be our world position.
    if(mTrueX == pX && mTrueY == pY) return;

    //--Set.
    mFacing = GetBestFacing(mTrueX, mTrueY, pX, pY);
    mCurrentViewAngle = (mFacing * 45.0f) - 90.0f;
}
void TilemapActor::ForceMoving(int pOverride)
{
    //--Overrides the move timer with the given value. Can stop movement if it's -1.
    if(pOverride == -1) { StopMoving(); return; }
    mIsMoving = true;
    mMoveTimer = pOverride;
    mLastMoveTick = 0;
}
void TilemapActor::StopMoving()
{
    if(!mAutoAnimates)
    {
        mIsMoving = false;
        mMoveTimer = -1;
    }
    mRemainderX = 0.0f;
    mRemainderY = 0.0f;
}
void TilemapActor::BeginStun()
{
    //--Immediately gets stunned, similar to being mugged, but doesn't drop anything.

    //--Set the stun timer.
    mMugStunTimer = TA_MUG_STUN_TICKS;
    mMugTimer = 0;

    //--If this entity was a follower, they lose their leader.
    if(mFollowTarget)
    {
        //--If the entity exists, remove us from its follower listing.
        RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowTarget);
        if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            rActor->RemoveFollower(mLocalName);
        }

        //--Clear the flag.
        ResetString(mFollowTarget, NULL);
    }
}
void TilemapActor::BeginDying()
{
    //--What a world, what a world!
    mIsDying = true;
    mDeathTimer = 0;
    SetRespondsToShot(false);
}
void TilemapActor::DieQuickly()
{
    //--Whataworldwhataworld!
    mIsDying = true;
    mDeathTimer = 30;
    SetRespondsToShot(false);
    ActivateSpecialFrame("Wounded");
}
void TilemapActor::ActivateFlashwhite()
{
    //--Flash the sprite to white, then drop the special frame and flash back down.
    mIsShowingFlashwhiteSequence = true;
    mFlashwhiteTimer = 0;

    //--If not using a special frame already, use whatever frame is currently animating.
    if(!rCurrentSpecialImage) rCurrentSpecialImage = ResolveFrame();

    //--This flag must be set *after* ResolveFrame() is called, otherwise it'd just return the
    //  special image (when we want the base image).
    mIsShowingSpecialImage = true;
}
void TilemapActor::SetSpecialIdleFlag(bool pFlag)
{
    mUsesSpecialIdleImages = pFlag;
}
void TilemapActor::SetTinyFlag(bool pFlag)
{
    mIsTiny = pFlag;
}
void TilemapActor::SetFlyingFlag(bool pFlag)
{
    mIsFlying = pFlag;
}
void TilemapActor::SetAutoAnimateFlag(bool pFlag)
{
    mAutoAnimates = pFlag;
    if(mAutoAnimates)
    {
        mMoveTimer = (rand() % mMoveImagesTotal) * mWalkTicksPerFrame;
    }
}
void TilemapActor::SetAutoAnimateFastFlag(bool pFlag)
{
    //--Forces the entity to update the move timer every tick. Without this flag it's once every 3 ticks.
    mAutoAnimatesFast = pFlag;
    mMoveTimer = 0;
}
void TilemapActor::SetOscillateFlag(bool pFlag)
{
    mYOscillates = pFlag;
    if(mYOscillates) mYOscillateTimer = rand() % 100;
}
void TilemapActor::SetCollisionDepth(int pValue)
{
    //--Note: Should only be used during the loading sequence, otherwise, don't set this manually!
    mCollisionDepth = pValue;
}
void TilemapActor::SetOverrideDepth(float pValue)
{
    //--Note: -2.0f is the "do nothing" value. Normal ranges are -1.0f to 0.0f.
    if(!mDoesntResetDepthOverride) mOverrideDepth = pValue;
}
void TilemapActor::SetOverrideDepthOffset(float pValue)
{
    //--Set to 0.0f to stop using this.
    mOverrideDepthOffset = pValue;
}
void TilemapActor::SetMoveTimer(int pTimer)
{
    mMoveTimer = pTimer;
}
void TilemapActor::SetAutoDespawn(bool pFlag)
{
    mAutoDespawnAfterAnimation = pFlag;
}
void TilemapActor::SetIgnoreSpecialLights(bool pFlag)
{
    mIgnoreSpecialLighting = pFlag;
}
void TilemapActor::SetIgnoreClipsWhenMoving(bool pFlag)
{
    mIgnoreCollisionsWhenMoving = pFlag;
}
void TilemapActor::SetPartyEntityFlag(bool pFlag)
{
    mIsPartyEntity = pFlag;
}
void TilemapActor::SetNoFootstepSounds(bool pFlag)
{
    mNoFootstepSounds = pFlag;
}
void TilemapActor::SetUseEightFramesForFootsteps(bool pFlag)
{
    mUseEightRunFrames = pFlag;
}
void TilemapActor::DisableAutomaticShadow(bool pFlag)
{
    mNoAutomaticShadow = pFlag;
}
void TilemapActor::SetDontRefaceFlag(bool pFlag)
{
    mNeverRefaceOnNode = pFlag;
}
void TilemapActor::SetNegativeMoveTimer(int pTicks)
{
    mNegativeMoveTimer = pTicks;
}
void TilemapActor::SetAutofire(float pRange)
{
    //--Set autofire to a range lower than 1.0f to deactivate it.
    if(pRange < 1.0f)
    {
        mAutoActivates = false;
        mAutoActivateRange = 0.0f;
    }
    else
    {
        mAutoActivates = true;
        mAutoActivateRange = pRange;
    }
}
void TilemapActor::SetUIDisplayString(const char *pString, bool pClearAfterBattle)
{
    mDisplayStringTimer = 0;
    mClearDisplayStringAfterBattle = pClearAfterBattle;
    ResetString(mUIDisplayString, pString);
}
void TilemapActor::HandleUIPostBattle()
{
    if(mClearDisplayStringAfterBattle)
    {
        ResetString(mUIDisplayString, NULL);
    }
}
void TilemapActor::SetYVerticalOffset(float pOffset)
{
    mVerticalOffY = pOffset;
}
void TilemapActor::SetIgnoresGravity(bool pFlag)
{
    mIgnoresGravity = pFlag;
}
void TilemapActor::SetZSpeed(float pSpeed)
{
    mZSpeed = pSpeed;
}
void TilemapActor::SetGravity(float pGravity)
{
    mGravity = pGravity;
}
void TilemapActor::SetJumpingSound(const char *pSoundName)
{
    if(!pSoundName || !strcasecmp(pSoundName, "Null"))
    {
        mPlaySoundOnJumping[0] = '\0';
        return;
    }
    strncpy(mPlaySoundOnJumping, pSoundName, 63);
}
void TilemapActor::SetLandingSound(const char *pSoundName)
{
    if(!pSoundName || !strcasecmp(pSoundName, "Null"))
    {
        mPlaySoundOnLanding[0] = '\0';
        return;
    }
    strncpy(mPlaySoundOnLanding, pSoundName, 63);
}
void TilemapActor::SetBlockMugAutoWin(bool pFlag)
{
    mNeverMugAutoWin = pFlag;
}
void TilemapActor::Rub()
{
    if(mRubTimer > -2) mRubTimer -= 2;
}
void TilemapActor::SetToCycleSpecialFramesThenDelete(int pTicksPerFrame)
{
    mCycleSpecialFramesThenDelete = true;
    mCycleTimer = 0;
    mCycleTPF = pTicksPerFrame;
    if(mCycleTPF < 1) mCycleTPF = 1;
}
void TilemapActor::SetRespondsToShot(bool pFlag)
{
    mRespondsToShot = pFlag;
}
void TilemapActor::SetNeverDropItemsOnMug(bool pFlag)
{
    mNeverDropsItemsOnMug = pFlag;
}
void TilemapActor::SetMoveFramesTotal(int pTotal)
{
    mMoveImagesTotal = pTotal;
    if(mMoveImagesTotal < 1) mMoveImagesTotal = 1;
    if(mMoveImagesTotal >= TA_MOVE_IMG_MAXIMUM) mMoveImagesTotal = TA_MOVE_IMG_MAXIMUM - 1;
}
void TilemapActor::SetShakeTicks(int pTicks)
{
    mShakeTicks = pTicks;
}
void TilemapActor::SetSpottingIconOffsets(float pX, float pY)
{
    mSpottingOffsetX = pX;
    mSpottingOffsetY = pY;
}
void TilemapActor::SetSpottingCircleOffsets(float pX, float pY, float pRadius, float pThickness)
{
    mSpottingCircleX = pX;
    mSpottingCircleY = pY;
    mSpottingCircleR = pRadius;
    mSpottingCircleT = pThickness;
    if(mSpottingCircleR < 1.0f) mSpottingCircleR = 1.0f;
    if(mSpottingCircleT < 1.0f) mSpottingCircleT = 1.0f;
}

///======================================== Core Methods ==========================================
bool TilemapActor::HandleActivation(int pX, int pY)
{
    //--If this Actor happens to do anything when activated, this will do that. The coordinates passed
    //  in are where the activation occurred.
    //--Returns true if the Actor activated something in a fashion that would stop the rest of the activation
    //  update, false otherwise.
    bool tBlocksActivateManually = OptionsManager::Fetch()->GetOptionB("Blocks Activate Manually");
    if(mActivatesByRub && !tBlocksActivateManually) return false;
    if(mIsEnemy && !mIsWanderNPC) return HandleEnemyActivation(pX, pY);
    if(!mActivationScript || !strcasecmp(mActivationScript, "Null")) return false;
    if(mIsDisabled) return false;

    //--Setup.
    const float cHalfSize = TA_SIZE * 0.5f;

    //--Compute the position coordinates. It can be extended with a flag.
    float tLft = mTrueX - cHalfSize;
    float tTop = mTrueY - cHalfSize;
    float tRgt = mTrueX + TA_SIZE + cHalfSize;
    float tBot = mTrueY + TA_SIZE + cHalfSize;

    //--Extended direction: These handle cases where an NPC is behind a counter and can be talked to.
    //  First, the null case.
    if(mExtendedActivationDirection == -1)
    {

    }
    //--North.
    else if(mExtendedActivationDirection == TA_DIR_NORTH)
    {
        tTop = tTop - TileLayer::cxSizePerTile;
    }
    //--East.
    else if(mExtendedActivationDirection == TA_DIR_EAST)
    {
        tRgt = tRgt + TileLayer::cxSizePerTile;
    }
    //--South.
    else if(mExtendedActivationDirection == TA_DIR_SOUTH)
    {
        tBot = tBot + TileLayer::cxSizePerTile;
    }
    //--West.
    else if(mExtendedActivationDirection == TA_DIR_WEST)
    {
        tLft = tLft - TileLayer::cxSizePerTile;
    }

    //--Check position. If necessary, run the "Hello" script.
    if(IsPointWithin(pX, pY, tLft, tTop, tRgt, tBot))
    {
        LuaManager::Fetch()->PushExecPop(this, mActivationScript, 1, "S", "Hello");
        return true;
    }

    //--All checks failed.
    return false;
}
bool TilemapActor::HandleAutoActivation(int pX, int pY)
{
    //--Handles auto-activation based on range.
    if(!mAutoActivates) return false;

    //--Check range to this actor.
    if(GetPlanarDistance(pX, pY, mTrueX, mTrueY) < mAutoActivateRange)
    {
        xAutofireCooldown = 1;
        LuaManager::Fetch()->PushExecPop(this, mActivationScript, 1, "S", "Auto");
        return true;
    }

    //--Checks failed.
    return false;
}
void TilemapActor::HandleRubActivation()
{
    if(!mActivationScript || !strcasecmp(mActivationScript, "Null")) return;
    if(mIsDisabled) return;
    mRubTimer = TA_RUB_TICKS;
    LuaManager::Fetch()->PushExecPop(this, mActivationScript, 1, "S", "Hello");
}
bool TilemapActor::HandleShotCheck(int pX, int pY)
{
    ///--[Documentation]
    //--Checks if the given position is within this object's hitbox. If it is, runs the shot check script,
    //  which is activation script with "Shot Check" as the argument.
    //--Returns true if the shot hit. AdventureLevel::xRespondToShot will be set to true if the object actually responds.
    if(mIsDisabled) return false;
    if(!mRespondsToShot) return false;
    if(!mActivationScript || !strcasecmp(mActivationScript, "Null")) return false;

    //--Collision.
    const float cHalfSize = TA_SIZE * 0.5f;
    float tLft = mTrueX - cHalfSize;
    float tTop = mTrueY - cHalfSize;
    float tRgt = mTrueX + TA_SIZE + cHalfSize;
    float tBot = mTrueY + TA_SIZE + cHalfSize;
    if(!IsPointWithin(pX, pY, tLft, tTop, tRgt, tBot)) return false;

    //--Run the script. It will set the flags, which are statics in the AdventureLevel class.
    LuaManager::Fetch()->PushExecPop(this, mActivationScript, 1, "S", "Shot Check");
    return true;
}
void TilemapActor::HandleShotHit()
{
    //--Runs the activation script if applicable.
    if(mIsDisabled) return;
    if(!mActivationScript || !strcasecmp(mActivationScript, "Null")) return;

    //--Run the script. It will set the flags, which are statics in the AdventureLevel class.
    LuaManager::Fetch()->PushExecPop(this, mActivationScript, 1, "S", "Shot Handle");
}
void TilemapActor::AddSpecialFrame(const char *pName, const char *pDLPath)
{
    //--Adds a special frame, doesn't care about duplication.
    StarBitmap *rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mSpecialImageList->AddElementAsTail(pName, rImage);

    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rImage && mImagePathStorage)
    {
        mImagePathStorage->mSpecialImageList->AddElementAsTail(pName, InitializeString(pDLPath), &FreeThis);
    }
    else if(!rImage)
    {
        //fprintf(stderr, "Unable to resolve image for special frame: %s %s\n", pName, pDLPath);
    }
}
void TilemapActor::ActivateSpecialFrame(const char *pName)
{
    //--Sets a special frame as the current one. If NULL is passed in, or "NULL", then special frames are deactivated.
    if(!pName || !strcasecmp(pName, "NULL"))
    {
        mIsShowingSpecialImage = false;
        rCurrentSpecialImage = NULL;
        return;
    }
    rCurrentSpecialImage = (StarBitmap *)mSpecialImageList->GetElementByName(pName);
    mIsShowingSpecialImage = (rCurrentSpecialImage != NULL);
}
void TilemapActor::WipeSpecialFrames()
{
    mSpecialImageList->ClearList();
    if(mIsStoringImagePaths && mImagePathStorage)
    {
        mImagePathStorage->mSpecialImageList->ClearList();
    }
}
void TilemapActor::SetReserveFrame(const char *pName)
{
    ResetString(mReserveSpecialImage, pName);
}
char *TilemapActor::GetWalkFootstepSFX(bool pOverrideNoFootstep)
{
    ///--[Documentation]
    //--Resolve which footstep sound to use when walking. Can legally return NULL.
    if(xMaxFootstepSounds < 1 || mRunTicksPerFrame == 0) return NULL;

    //--If this entity has a flag set to ignore footsteps, the override flag can bypass it.
    if(mNoFootstepSounds && !pOverrideNoFootstep) return NULL;

    //--Check which frame we're on. The footstep sound only plays on the first tick of the run
    //  frames where the foot leaves the ground.
    int tUseTimer = mMoveTimer;
    if(tUseTimer < 0) tUseTimer = 0;
    int tFrame = ((int)(tUseTimer / mWalkTicksPerFrame) % mMoveImagesTotal);
    if(tFrame == 0 || tFrame == 2) return NULL;
    if(tUseTimer % (int)mWalkTicksPerFrame != 0) return NULL;

    ///--[Resolve Sound]
    //--When walking on certain surfaces, the sound effect may change.
    char *rUseBuffer = mWalkFootstepSound;
    if(mIsInDeepLayer)
    {
        rUseBuffer = mDeepLayerWalkOverride;
    }

    //--Check if the name is invalid.
    if(!rUseBuffer || rUseBuffer[0] == '\0' || !strcasecmp(rUseBuffer, "Null")) return NULL;

    ///--[Return]
    //--Print into the footstep buffer and return it.
    sprintf(mWalkFootstepBuffer, "%s", rUseBuffer);
    return mWalkFootstepBuffer;
}
char *TilemapActor::GetRunFootstepSFX(bool pOverrideNoFootstep)
{
    ///--[Documentation]
    //--Resolve which footstep sound to use when running. Can legally return NULL.
    if(xMaxFootstepSounds < 1 || mRunTicksPerFrame == 0) return NULL;

    //--If this entity has a flag set to ignore footsteps, the override flag can bypass it.
    if(mNoFootstepSounds && !pOverrideNoFootstep) return NULL;

    //--Check which frame we're on. The footstep sound only plays on the first tick of the run
    //  frames where the foot leaves the ground.
    int tUseTimer = mMoveTimer;
    if(tUseTimer < 0) tUseTimer = 0;
    int tFrame = ((int)(tUseTimer / mRunTicksPerFrame) % mMoveImagesTotal);
    if(tUseTimer % (int)mRunTicksPerFrame != 0) return NULL;

    //--Standard four run frames:
    if(!mUseEightRunFrames)
    {
        if(tFrame == 0 || tFrame == 2) return NULL;
    }
    //--Eight run frames:
    else
    {
        if(tFrame != 2 && tFrame != 6) return NULL;
    }

    ///--[Step Rolling]
    //--Variables.
    int tRollNumber = (int)(rand() % xMaxFootstepSounds);
    char tLetter = 'L';

    //--Last step was left:
    if(mWasLastFootstepLeft)
    {
        //--Flags.
        mWasLastFootstepLeft = false;
        tLetter = 'R';

        //--If the footstep roll is the same value, modify until it isn't.
        if(mPreviousFootstepRollR == tRollNumber && xMaxFootstepSounds >= 2)
        {
            tRollNumber = ((tRollNumber + 1) % xMaxFootstepSounds);
        }

        //--Store.
        mPreviousFootstepRollR = tRollNumber;
    }
    //--Last step was right:
    else
    {
        //--Flags.
        tLetter = 'L';
        mWasLastFootstepLeft = true;

        //--If the footstep roll is the same value, modify until it isn't.
        if(mPreviousFootstepRollL == tRollNumber && xMaxFootstepSounds >= 2)
        {
            tRollNumber = ((tRollNumber + 1) % xMaxFootstepSounds);
        }

        //--Store.
        mPreviousFootstepRollL = tRollNumber;
    }

    ///--[Resolve Sound]
    //--When walking on certain surfaces, the sound effect may change.
    char *rUseBuffer = mRunFootstepSound;
    if(mIsInDeepLayer)
    {
        rUseBuffer = mDeepLayerWalkOverride;
    }

    //--Check if the name is invalid.
    if(!rUseBuffer || rUseBuffer[0] == '\0' || !strcasecmp(rUseBuffer, "Null")) return NULL;

    ///--[Return]
    //--Print into the footstep buffer and return it.
    sprintf(mRunFootstepBuffer, rUseBuffer, tLetter, tRollNumber);
    return mRunFootstepBuffer;
}
void TilemapActor::PrintSpecialFrames()
{
    //--Prints all special frames this entity has to the console. Used for debug.
    fprintf(stderr, "Printing special frames for %s\n", mLocalName);
    void *rFramePtr = mSpecialImageList->PushIterator();
    while(rFramePtr)
    {
        const char *rName = mSpecialImageList->GetIteratorName();
        fprintf(stderr, " %s %p\n", rName, rFramePtr);
        rFramePtr = mSpecialImageList->AutoIterate();
    }
}
void TilemapActor::ReresolveLocalImages()
{
    ///--[Documentation]
    //--Orders the TilemapActor to attempt to find all the images it previously failed to find.
    //  These images have their paths stored in a structure designed for this purpose.
    if(!mIsStoringImagePaths || !mImagePathStorage) return;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Standard / Optional Frames]
    //--Reresolve.
    for(int x = 0; x < TA_DIR_TOTAL; x ++)
    {
        //--Movement images.
        for(int y = 0; y < TA_MOVE_IMG_MAXIMUM; y ++)
        {
            //--Movement image.
            if(mImagePathStorage->mMoveImages[x][y][0] != '\0')
            {
                rMoveImages[x][y] = (StarBitmap *)rDataLibrary->GetEntry(mImagePathStorage->mMoveImages[x][y]);
            }

            //--Running image.
            if(mImagePathStorage->mRunImages[x][y][0] != '\0')
            {
                rRunImages[x][y]  = (StarBitmap *)rDataLibrary->GetEntry(mImagePathStorage->mRunImages[x][y]);
            }
        }

        //--Standing.
        if(mImagePathStorage->mStandingImages[x][0] != '\0')
        {
            rStandingImages[x] = (StarBitmap *)rDataLibrary->GetEntry(mImagePathStorage->mStandingImages[x]);
        }

        //--Breathing.
        if(mImagePathStorage->mBreatheImages[x][0] != '\0')
        {
            rBreatheImages[x] = (StarBitmap *)rDataLibrary->GetEntry(mImagePathStorage->mBreatheImages[x]);
        }
    }

    ///--[Special Frames]
    //--Special image paths.
    const char *rPath = (const char *)mImagePathStorage->mSpecialImageList->PushIterator();
    while(rPath)
    {
        //--Create.
        mSpecialImageList->AddElementAsTail(mImagePathStorage->mSpecialImageList->GetIteratorName(), rDataLibrary->GetEntry(rPath));

        //--Next.
        rPath = (const char *)mImagePathStorage->mSpecialImageList->AutoIterate();
    }

    ///--[Clean Up]
    mIsStoringImagePaths = false;
    mImagePathStorage->Delete();
    free(mImagePathStorage);
    mImagePathStorage = NULL;
}
ActorNotice *TilemapActor::SpawnNotice(const char *pText)
{
    //--Creates and returns an ActorNotice positioned just above the TilemapActor's head. It is
    //  returned, and also registered with the AdventureLevel.
    if(!pText) return NULL;

    //--Fetch the level.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return NULL;

    //--Position. Center the X position.
    float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X + 16.0f;
    float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y;

    //--Create.
    ActorNotice *nNotice = new ActorNotice();
    nNotice->SetTimerMax(TA_DEFAULT_NOTICE_TICKS);
    nNotice->SetText(pText);
    nNotice->MoveTo(tXPosition, tYPosition, 0);

    //--Register it to the AdventureLevel.
    rActiveLevel->RegisterNotice(nNotice);

    //--Pass it back.
    return nNotice;
}
void TilemapActor::CreateSpecialFrameAnim(const char *pName)
{
    //--Check if the anim exists. If so, do nothing.
    if(!pName || mSpecialImageAnimationList->GetElementByName(pName) != NULL) return;

    //--Create and init.
    SpecialFrameAnimPack *nPackage = (SpecialFrameAnimPack *)starmemoryalloc(sizeof(SpecialFrameAnimPack));
    nPackage->Initialize();
    mSpecialImageAnimationList->AddElement(pName, nPackage, &SpecialFrameAnimPack::DeleteThis);
}
void TilemapActor::SetSpecialFrameAnimLen(const char *pName, int pFrames)
{
    //--Locate.
    SpecialFrameAnimPack *rPackage = (SpecialFrameAnimPack *)mSpecialImageAnimationList->GetElementByName(pName);
    if(!rPackage) return;

    //--Deallocate.
    for(int i = 0; i < rPackage->mTotalFrames; i ++) free(rPackage->mFrameNames[i]);
    free(rPackage->mTimings);
    free(rPackage->mFrameNames);

    //--Zero.
    rPackage->mTotalFrames = 0;
    rPackage->mFrameNames = NULL;
    rPackage->mTimings = NULL;
    if(pFrames < 1) return;

    //--Set.
    rPackage->mTotalFrames = pFrames;
    rPackage->mTimings = (int *)starmemoryalloc(sizeof(int) * rPackage->mTotalFrames);
    rPackage->mFrameNames = (char **)starmemoryalloc(sizeof(char *) * rPackage->mTotalFrames);
    for(int i = 0; i < rPackage->mTotalFrames; i ++)
    {
        rPackage->mTimings[i] = 0;
        rPackage->mFrameNames[i] = NULL;
    }
}
void TilemapActor::SetSpecialFrameAnimCase(const char *pName, int pSlot, int mTicks, const char *pFrameName)
{
    //--Locate.
    SpecialFrameAnimPack *rPackage = (SpecialFrameAnimPack *)mSpecialImageAnimationList->GetElementByName(pName);
    if(!rPackage) return;

    //--Range check.
    if(pSlot < 0 || pSlot >= rPackage->mTotalFrames) return;

    //--Set.
    rPackage->mTimings[pSlot] = mTicks;
    ResetString(rPackage->mFrameNames[pSlot], pFrameName);
}
void TilemapActor::SetSpecialFrameAnimeResetFrame(const char *pName, int pSlot)
{
    //--Locate.
    SpecialFrameAnimPack *rPackage = (SpecialFrameAnimPack *)mSpecialImageAnimationList->GetElementByName(pName);
    if(!rPackage) return;

    //--Note: Doesn't range check. The execution code does.
    rPackage->mResetFrame = pSlot;
}
void TilemapActor::ActivateSpecialFrameAnim(const char *pName)
{
    //--Make sure the animation exists. In all cases, clear the existing animation.
    rActiveAnimation = NULL;
    SpecialFrameAnimPack *rPackage = (SpecialFrameAnimPack *)mSpecialImageAnimationList->GetElementByName(pName);
    if(!rPackage) return;

    //--If it exists, set it and zero its internal timer.
    rActiveAnimation = rPackage;
    rActiveAnimation->mTimer = 0;
}

///==================================== Private Core Methods ======================================
void TilemapActor::ResolveActivationDimensions()
{
    //--Sets the activation position based on the current facing.
    float tAngleRad = ((mFacing * 45.0f) - 90.0f) * 3.1415926f / 180.0f;

    //--Resolve the position, that is, the center of the activation square.
    float tXPosition = mTrueX + (TA_SIZE * 0.5f) + (cosf(tAngleRad) * TA_EXAMINE_DIST);
    float tYPosition = mTrueY + (TA_SIZE * 0.5f) + (sinf(tAngleRad) * TA_EXAMINE_DIST);

    //--Now position the box around this point.
    mActivationDim.SetWH(tXPosition - (TA_EXAMINE_DIM * 0.5f), tYPosition - (TA_EXAMINE_DIM * 0.5f), TA_EXAMINE_DIM, TA_EXAMINE_DIM);
}

///=========================================== Update =============================================
void TilemapActor::Update()
{
    ///--[Documentation and Setup]
    //--Update handler for tilemap actors.

    ///--[Block Checking]
    //--Disabled entities don't update.
    if(mIsDisabled) return;

    //--Debug.
    DebugPush(true, "Tilemap Actor %p %s updates.\n", this, mLocalName);

    ///--[Cycle Special Frames Then Expire]
    //--Used for actors that play a single animation and then vanish. They will play each special frame in order.
    //  Once all frames are exhausted, the entity deletes itself.
    //--These entities do literally nothing else.
    if(mCycleSpecialFramesThenDelete)
    {
        //--If the TPF is somehow zero, delete immediately.
        if(mCycleTPF < 1)
        {
            mSelfDestruct = true;
            DebugPop("Actor completes update. Cycling frames then expiring.\n");
            return;
        }

        //--Timer.
        mCycleTimer ++;

        //--Resolve frame.
        int tFrame = mCycleTimer / mCycleTPF;
        rCurrentSpecialImage = (StarBitmap *)mSpecialImageList->GetElementBySlot(tFrame);
        mIsShowingSpecialImage = true;

        //--If there is no special image, delete self.
        if(!rCurrentSpecialImage) mSelfDestruct = true;
        DebugPop("Actor completes update. Cycling frames then expiring.\n");
        return;
    }

    ///--[Special Frame Animation]
    //--Similar to above, but runs in a cycle. Plays a special frame animation by running a timer up and
    //  changing the special frame image.
    if(rActiveAnimation)
    {
        //--Debug.
        DebugPrint("Special frame animation.\n");

        //--Double-check that the animation still exists.
        if(mSpecialImageAnimationList->IsElementOnList(rActiveAnimation) && rActiveAnimation->mTotalFrames > 0)
        {
            //--Advance the timer.
            rActiveAnimation->mTimer ++;

            //--Slot is out of range, reset to  or the reset frame.
            if(rActiveAnimation->mSlot >= rActiveAnimation->mTotalFrames)
            {
                rActiveAnimation->mTimer = 0;
                rActiveAnimation->mSlot = rActiveAnimation->mResetFrame;
                if(rActiveAnimation->mSlot >= rActiveAnimation->mTotalFrames) rActiveAnimation->mSlot = 0;
                ActivateSpecialFrame(rActiveAnimation->mFrameNames[0]);
            }
            //--Check if we're past the timer for this frame. If so, advance.
            else
            {
                if(rActiveAnimation->mTimer >= rActiveAnimation->mTimings[rActiveAnimation->mSlot])
                {
                    rActiveAnimation->mTimer = 0;
                    rActiveAnimation->mSlot ++;
                    if(rActiveAnimation->mSlot >= rActiveAnimation->mTotalFrames) rActiveAnimation->mSlot = rActiveAnimation->mResetFrame;
                    if(rActiveAnimation->mSlot >= rActiveAnimation->mTotalFrames) rActiveAnimation->mSlot = 0;
                    ActivateSpecialFrame(rActiveAnimation->mFrameNames[rActiveAnimation->mSlot]);
                }
            }
        }
        //--It got deleted, so clear it.
        else
        {
            rActiveAnimation = NULL;
        }
    }

    ///--[Timers]
    //--Debug.
    DebugPrint("Running timers.\n");

    //--This timer always updates.
    if(mLastComputedReinforcement > 0 || mStunTimer == 45) mReinforcementTimer ++;

    //--This timer updates if it has a string.
    if(mDisplayStringTimer < TA_DISPLAY_STRING_TICKS && mUIDisplayString) mDisplayStringTimer ++;

    //--Rub timer increments by 1 each tick.
    if(mRubTimer < TA_RUB_TICKS) mRubTimer ++;

    //--Breathing timer.
    mBreatheTimer ++;
    if(mBreatheTimer >= TA_BREATHE_MAX) mBreatheTimer = rand() % (int)(TA_BREATHE_MAX * 0.25f);

    //--Overlay timer.
    UpdateOverlays();

    //--Shake timer. Recomputes every few ticks.
    if(mShakeTicks > 0) mShakeTicks --;
    if(mShakeTicks > 0 && mShakeTicks % 3 == 0)
    {
        float tRadians = (rand() % 360) * 3.1415926f / 180.0f;
        float cRadius = 1.2f;
        float tXDist = cosf(tRadians) * cRadius;
        float tYDist = sinf(tRadians) * cRadius;
        mShakeOffX = tXDist;
        mShakeOffY = tYDist;
    }

    ///--[Auto-Animate]
    //--If auto-animate is true, then this timer always runs if the character is not running.
    if((mAutoAnimates || mAutoAnimatesFast) && !mIsRunning)
    {
        //--Debug.
        DebugPrint("Running auto-animate.\n");

        //--Negative move timer. Entity does nothing until it zeroes.
        if(mNegativeMoveTimer > 0)
        {
            mNegativeMoveTimer --;
            DebugPop("Actor completes update. Negative move timer.\n");
            return;
        }

        //--Fast animation, 1 tick per frame.
        if(mAutoAnimatesFast)
        {
            mIsMoving = true;
            mMoveTimer ++;
            //fprintf(stderr, "%s %i - %f\n", mLocalName, mMoveTimer, mWalkTicksPerFrame);
        }
        //--Normal, slow case. Most entities use this.
        else if(Global::Shared()->gTicksElapsed % 3 == 0)
        {
            mIsMoving = true;
            mMoveTimer ++;
        }

        //--Auto-despawn check.
        if(mAutoDespawnAfterAnimation && mMoveTimer >= mWalkTicksPerFrame * mMoveImagesTotal)
        {
            mSelfDestruct = true;
        }
    }
    //--Otherwise, if we were moving in the last 3 ticks, increment.
    else if(mLastMoveTick < 3)
    {
        mLastMoveTick ++;
    }
    //--If we didn't move for 3 ticks, stop moving and reset the timers.
    else
    {
        if(!mAutoAnimates)
        {
            mIsMoving = false;
            mMoveTimer = 0;
        }
    }

    ///--[Deep Layers]
    //--Debug.
    DebugPrint("Checking deep layer packages.\n");

    //--Check level.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel)
    {
        //--Ask the level to report which, if any, deep layer this entity is in.
        DeepLayerPack *rAffectingPack = rActiveLevel->GetDeepLayerPackForEntity(this);

        //--No package, clear the variables. Alternately, flying entities ignore this.
        if(!rAffectingPack || mIsFlying)
        {
            mIsInDeepLayer = false;
        }
        //--A pack is affecting this entity, store the values.
        else
        {
            mIsInDeepLayer = true;
            mDeepLayerCollision = rAffectingPack->mCollisionAssociated;
            mDeepLayerPreventsRun = rAffectingPack->mPreventsRun;
            mDeepLayerPixelSink = rAffectingPack->mPixelSink;
            strncpy(mDeepLayerOverlayName,  rAffectingPack->mSpecialFrameOverlayName, 31);
            strncpy(mDeepLayerWalkOverride, rAffectingPack->mWalkSound, 31);
            strncpy(mDeepLayerRunOverride,  rAffectingPack->mRunSound, 31);
            mDeepLayerOverlayName[31]  = '\0';
            mDeepLayerWalkOverride[31] = '\0';
            mDeepLayerRunOverride[31]  = '\0';
        }
    }

    ///--[Flashwhite]
    //--Debug.
    DebugPrint("Checking flashwhite.\n");

    //--Flashwhite sequence.
    if(mIsShowingFlashwhiteSequence)
    {
        //--Timer.
        mFlashwhiteTimer ++;

        //--At this mark, disable the special frame. If there is a reserve frame, use that.
        if(mFlashwhiteTimer == FLASHWHITE_TICKS_UP + (FLASHWHITE_TICKS_HOLD / 2))
        {
            ActivateSpecialFrame(mReserveSpecialImage);
            ResetString(mReserveSpecialImage, NULL);
        }

        //--Ending case.
        if(mFlashwhiteTimer >= FLASHWHITE_TICKS_TOTAL)
        {
            mIsShowingFlashwhiteSequence = false;
        }
    }
    //--If oscillating, do that. This is exclusive with the flashwhite sequence.
    else if(mYOscillates)
    {
        mYOscillateTimer ++;
    }

    ///--[Gravity]
    //--Debug.
    DebugPrint("Checking gravity.\n");

    //--Gravity.
    if(!mIgnoresGravity)
    {
        //--Above the ground:
        if(mVerticalOffY < 0.0f || mZSpeed < 0.0f)
        {
            mJumpTimer ++;
            mZSpeed = mZSpeed + mGravity;
            mVerticalOffY = mVerticalOffY + mZSpeed;
            if(mVerticalOffY >= 0.0f)
            {
                mVerticalOffY = 0.0f;
                //fprintf(stderr, "Jump time: %i\n", mJumpTimer);
                if(mPlaySoundOnLanding[0] != '\0')
                {
                    AudioManager::Fetch()->PlaySound(mPlaySoundOnLanding);
                }
            }
        }
        //--On the ground.
        else
        {
            mJumpTimer = 0;
            mVerticalOffY = 0.0f;
            mZSpeed = 0.0f;
        }
    }

    ///--[Combat Lockout for AI]
    //--Debug.
    DebugPrint("Handling combat lockout.\n");

    //--Never update during combat.
    if(AdvCombat::Fetch()->IsActive())
    {
        DebugPop("Actor completes update. Finishing lockout due to AI.\n");
        return;
    }

    //--Enemy AI. Overrides all other priorities.
    if(mIsEnemy)
    {
        DebugPrint("Handling enemy logic.\n");
        UpdateEnemyMode();
        DebugPop("Actor completes update. Enemy mode.\n");
        return;
    }

    ///--[Debug]
    DebugPop("Actor completes update normally.\n");
}
bool TilemapActor::HandlePlayerControls(bool pAllowRunning)
{
    ///--[Documentation]
    //--Entities move on a per-pixel basis. This handles that. Returns true if any movement controls were
    //  pressed, false on error or no movement keys pressed.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Reset the movement amount.
    mAmountMovedLastTick = 0.0f;

    ///--[Activation Handling]
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--List of activation points. The center is always the zeroth one.
        Point3D tActivationPoints[5];
        tActivationPoints[0].Set(mActivationDim.mXCenter, mActivationDim.mYCenter, 0.0f);
        tActivationPoints[1].Set(mActivationDim.mLft,     mActivationDim.mTop,     0.0f);
        tActivationPoints[2].Set(mActivationDim.mLft,     mActivationDim.mBot,     0.0f);
        tActivationPoints[3].Set(mActivationDim.mRgt,     mActivationDim.mTop,     0.0f);
        tActivationPoints[4].Set(mActivationDim.mRgt,     mActivationDim.mBot,     0.0f);
        for(int i = 0; i < 5; i ++)
        {
            if(rActiveLevel->ActivateAt(tActivationPoints[i].mX, tActivationPoints[i].mY))
            {
                return false;
            }
        }
    }

    ///--[Run Toggling]
    //--Autorun flag.
    bool tAutorun = OptionsManager::Fetch()->GetOptionB("Autorun");

    //--Does not count as a control, sets the run state in the character.
    if(((!tAutorun && rControlManager->IsDown("Run")) || (tAutorun && !rControlManager->IsDown("Run"))) && pAllowRunning)
    {
        //--Special: If in a deep layer that precludes running, disable this.
        if(mIsInDeepLayer && mDeepLayerPreventsRun)
        {
            SetRunning(false);
        }
        //--Normal:
        else
        {
            SetRunning(true);
        }
    }
    else
    {
        SetRunning(false);
    }

    ///--[Directional Keys]
    //--Store the keys.
    bool tIsLftPressed = rControlManager->IsDown("Left");
    bool tIsRgtPressed = rControlManager->IsDown("Right");
    bool tIsTopPressed = rControlManager->IsDown("Up");
    bool tIsBotPressed = rControlManager->IsDown("Down");
    xIsPlayerCollisionCheck = true;
    EmulateMovement(tIsLftPressed, tIsTopPressed, tIsRgtPressed, tIsBotPressed);
    xIsPlayerCollisionCheck = false;

    ///--[Climbing Check]
    mAlwaysFacesUp = rActiveLevel->IsInClimbableZone(mTrueX + TA_SIZE * 0.5f, mTrueY + TA_SIZE * 0.5f);

    //--Return true if any of the movement keys were pressed. This is for following entities, it does
    //  not affect this entity.
    return (tIsLftPressed || tIsRgtPressed || tIsTopPressed || tIsBotPressed);
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarBitmap *TilemapActor::GetMoveImage(int pDirection, int pSlot)
{
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL)        return NULL;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_MAXIMUM) return NULL;
    return rMoveImages[pDirection][pSlot];
}
StarBitmap *TilemapActor::GetRunImage(int pDirection, int pSlot)
{
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL)        return NULL;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_MAXIMUM) return NULL;
    return rRunImages[pDirection][pSlot];
}
StarBitmap *TilemapActor::GetShadowImage()
{
    return rShadowImg;
}

///========================================== File I/O ============================================
///===================================== Static Functions =========================================
int TilemapActor::GetBestFacing(float pSubjectX, float pSubjectY, float pTargetX, float pTargetY)
{
    //--Static function that attempts to resolve the best "facing" angle. The two points can be
    //  anywhere in relation to one another, but will be condensed into 8 possible facings.
    //--If the two positions are identical then "North" defaults.
    if(pSubjectX == pTargetX && pSubjectY == pTargetY) return TA_DIR_NORTH;

    //--Compute angle.
    float tAngle = atan2f(pTargetY - pSubjectY, pTargetX - pSubjectX);

    //--West. Remember this is from -pi to +pi.
    if(tAngle <= 3.1415926f * -0.875f || tAngle >= 3.1415926f * 0.875f)
    {
        return TA_DIR_WEST;
    }
    //--Northwest. Still in the negative quadrant.
    else if(tAngle < 3.1415926f * -0.625f)
    {
        return TA_DIR_NW;
    }
    //--North.
    else if(tAngle < 3.1415926f * -0.375f)
    {
        return TA_DIR_NORTH;
    }
    //--Northeast.
    else if(tAngle < 3.1415926f * -0.125f)
    {
        return TA_DIR_NE;
    }
    //--Southwest. Back on the positives.
    else if(tAngle > 3.1415926f * 0.625f)
    {
        return TA_DIR_SW;
    }
    //--South.
    else if(tAngle > 3.1415926f * 0.375f)
    {
        return TA_DIR_SOUTH;
    }
    //--Southeast.
    else if(tAngle > 3.1415926f * 0.125f)
    {
        return TA_DIR_SE;
    }
    //--Close enough to zero to be east.
    else
    {
        return TA_DIR_EAST;
    }
}
int TilemapActor::GetOrthoFacing(float pSubjectX, float pSubjectY, float pTargetX, float pTargetY)
{
    //--Returns a non-diagonal facing that best describes the relationship between the two entities.
    //  This is mostly used for rub activation.
    float tXDiff = fabsf(pSubjectX - pTargetX);
    float tYDiff = fabsf(pSubjectY - pTargetY);

    //--Too different on the Y
    if(tYDiff > 12) return TA_DIR_SOUTH;

    //--Match on the X:
    if(tXDiff < 12.0f)
    {
        //--Match on X and Y:
        if(pSubjectY == pTargetY) return TA_DIR_NORTH;

        //--Y is less:
        if(pSubjectY < pTargetY) return TA_DIR_SOUTH;

        //--Y is more:
        return TA_DIR_NORTH;
    }
    //--Different X:
    else
    {
        //--X is less:
        if(pSubjectX < pTargetX) return TA_DIR_EAST;

        //--X is more:
        return TA_DIR_WEST;

    }

    //--Error case:
    return TA_DIR_NE;
}
void TilemapActor::ReresolveAllImages()
{
    //--When a TilemapActor is created and loads its images, in some circumstances the images have
    //  not been loaded yet. When flagged to, the TilemapActor can store the paths to those images.
    //  Once image loading is done, this function will scan every TilemapActor that exists and order
    //  them to re-resolve all their missing images.
    //--If the image is found the first time, the image does not re-resolve. This does nothing if
    //  the re-resolve flag is off. You should probably turn the flag off when it is completed.
    if(!xShouldStoreImagePaths) return;

    //--Run through the EntityManager where all actors are stored.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    if(!rEntityList) return;

    //--Iterate.
    RootEntity *rEntity = (RootEntity *)rEntityList->PushIterator();
    while(rEntity)
    {
        //--Entity is not a TilemapActor? Ignore it.
        if(!rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            rEntity = (RootEntity *)rEntityList->AutoIterate();
            continue;
        }

        //--Call.
        TilemapActor *rAsActor = (TilemapActor *)rEntity;
        rAsActor->ReresolveLocalImages();

        //--Next.
        rEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
