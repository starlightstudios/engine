-- |[ ==================================== fnCheckAutorun() ==================================== ]|
--Scans the autorun handler for the provided name. If found, returns the index in the array of the
-- entry that holds it. Returns -1 if not found.
function fnCheckAutorun(psName)
    
    -- |[Argument Check]|
    if(psName             == nil) then return -1 end
    if(gzaAutorunHandlers == nil) then return -1 end
    
    -- |[Scan]|
    for i = 1, #gzaAutorunHandlers, 1 do
        if(gzaAutorunHandlers[i][1] == psName) then
            return i
        end
    end
    
    -- |[Not Found]|
    return -1
end