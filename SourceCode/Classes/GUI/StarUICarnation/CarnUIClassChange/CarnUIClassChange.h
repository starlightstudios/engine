///===================================== CarnUIClassChange ========================================
//--UI used by the player to change character classes. When booted, a script should populate it with
//  class information, and then the active character specifies which classes it has available. The
//  player can then pick a class to replace and replace it from the list of replacement classes.
//--Replacing a class calls a script to handle it since most of the class activity is done in Lua.

#pragma once

///========================================= Includes =============================================
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
///--[ClassChangeInfo]
typedef struct ClassChangeInfo
{
    //--Members
    char *mInternalName;
    char *mDisplayName;
    int mLevel;
    int mXPTotal;
    int mXPToNext;
    StarLinkedList *mDescription;

    //--Methods
    void Initialize();
    static void DeleteThis(void *pPtr);
}ClassChangeInfo;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CarnUIClassChange : public CarnUICommon
{
    protected:
    ///--[Constants]
    //--Scrollbar.
    static const int cxCarnScrollBuf  =  3;
    static const int cxCarnScrollPage = 12;

    //--Hitboxes.
    static const int cxHitbox_ActiveLo  =  0;
    static const int cxHitbox_ActiveHi  =  9;
    static const int cxHitbox_ReplaceLo = 10;
    static const int cxHitbox_ReplaceHi = 21;
    static const int cxHitbox_ScrollUp  = 22;
    static const int cxHitbox_ScrollBd  = 23;
    static const int cxHitbox_ScrollDn  = 24;

    ///--[System]
    bool mLockFirstSlot;

    ///--[Cursor]
    bool mIsReplaceMode;
    int mReplaceTimer;
    int mCharacterCursor;
    int mClassCursor;
    int mReplaceCursor;

    ///--[Scrollbar]
    int mScrollSkip;

    ///--[Character Info]
    char *mCharacterName;
    int mCharacterLevel;
    int mCharacterHP;
    int mCharacterHPMax;
    int mCharacterSP;
    int mCharacterSPMax;
    int mCharacterClassSlots;

    ///--[Class Information]
    int mDescriptionClassCursor;
    int mDescriptionReplaceCursor;
    StarLinkedList *mrCurrentClassList; //ClassChangeInfo *, reference
    StarLinkedList *mClassInfoList;     //ClassChangeInfo *, master

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Description;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rOverlay_ClassReplaceDivider;
        StarBitmap *rOverlay_Cursor;
        StarBitmap *rOverlay_HPFill;
        StarBitmap *rOverlay_MeterFrames;
        StarBitmap *rOverlay_SPFill;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rScrollbar_Static;
    }CarnImages;
    protected:

    public:
    //--System
    CarnUIClassChange();
    virtual ~CarnUIClassChange();
    virtual void Construct();

    //--Public Variables
    static char *xPopulateScript;
    static char *xCharacterScript;
    static char *xChangeScript;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    void SetLockFirstSlot(bool pFlag);
    void RegisterClass(const char *pName, const char *pDisplayName);
    void SetClassDescription(const char *pName, const char *pDescription);
    void SetClassLevelProperties(const char *pName, int pLevel, int pXP, int pToNext);
    void SetCharacterProperties(const char *pName, int pLevel, int pHP, int pHPMax, int pSP, int pSPMax);
    void ClearActiveClasses();
    void RegisterActiveClass(const char *pName);
    void SetClassSlots(int pAmount);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void RefreshCharacterProperties();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    void UpdateActiveMode();
    void UpdateReplaceMode();
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnUIClassChange_SetProperty(lua_State *L);


