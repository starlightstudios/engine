///=================================== Carnation Definitions ======================================
//--Structures and definitions common to Project Carnation classes.
#pragma once
#include "StarBitmap.h"

///======================================= CarnExaminable =========================================
//--Examinable object in the world. These objects are marked with a clickable heart. Clicking the
//  heart fires the script/argument associated with the examinable. Usually this will bring up
//  a popup menu or will run the dialogue.
//--These can also be used as members of a popup menu themselves.
//--Special cases:
//  If mScript begins with "AUTOTRANSITION|path" then it will automatically transition to the given
//  destination path when clicked.
typedef struct CarnExaminable
{
    //--Position
    int mHeartX;         //Screen X position of the heart. On the popup menu, is the left of the selectable area.
    int mHeartY;         //Screen Y position of the heart. On the popup menu, is the left of the selectable area.
    int mHeartR;         //When on a popup menu, this is the right edge of the selectable area.
    int mHeartB;         //When on a popup menu, this is the bottom edge of the selectable area.

    //--Discovery
    bool mAlwaysVisible; //If true, object is always visible even if "undiscovered".
    int mDiscoveryTimer; //Object is invisible if undiscovered. Mousing near it discovers it.

    //--Display
    char *mDisplayName;  //Name to display.
    bool mAlwaysShowName;//If true, always show the name even when not selected

    //--Execution
    int mHighlightVal;   //When highlighted by the mouse, sets the alarm to this value.
    char *mScript;       //Path to the script to call if this object is clicked
    char *mArgument;     //Script argument passed when the script is fired.

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}CarnExaminable;

//--Related Definitions
#define CARNEXAMINE_DISCOVERY_TICKS 30
#define CARNEXAMINE_DISCOVERY_DISTANCE 30.0f
#define CARNEXAMINE_DISCOVERY_DISTANCE_DOUBLE 15.0f

///======================================== CarnMapLayer ==========================================
typedef struct CarnMapLayer
{
    //--Variables
    int mRenderPriority;//Influences drawing order, lower renders first.
    float mOffsetX;     //X offset.
    float mOffsetY;     //Y offset.
    bool mIsVisible;    //Flag that adjusts update timer.
    int mTimer;         //Opacity timer.
    int mTimerMax;      //Opacity max. Set externally.
    StarBitmap *rImage; //Pointer to the image in question.

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
    void SetVisible(bool pIsVisible, int pMaxTicks);
    void Update();
    void Render(float pAlpha);
}CarnMapLayer;
