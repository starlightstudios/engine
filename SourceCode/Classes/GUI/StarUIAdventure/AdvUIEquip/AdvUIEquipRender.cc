//--Base
#include "AdvUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

///========================================== Routing =============================================
void AdvUIEquip::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--In addition to rendering the sub-components and help, the Details window renders without any
    //  particular side offset.

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Equip Details]
    //--Not part of any block, renders over everything else. Does nothing if not inspecting.
    RenderEquipDetails(pVisAlpha);

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);
}

///======================================= Side Renders ===========================================
void AdvUIEquip::RenderLft(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the currently selected character on the left side. If no active character resolves,
    //  block rendering.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) return;

    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);

    ///--[Common Render]
    //--Left-side arrow.
    float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1) AdvImages.rOverlay_ArrowLft->Draw(cArrowOscillate * -1.0f, 0.0f);

    ///--[Normal Character]
    //--Render the character in question.
    if(!mIsSwappingCharacter)
    {
        //--Stencil and position.
        TwoDimensionRealPoint tRenderDim = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_EQUIPMENT);

        //--Get portrait.
        StarBitmap *rPortrait = rActiveEntity->GetCombatPortrait();
        if(rPortrait)
        {
            rPortrait->Draw(tRenderDim.mXCenter, tRenderDim.mYCenter);
        }
    }
    ///--[Swapping Characters]
    //--The currently selected character slides into the screen and the previous character slides out.
    else
    {
        //--Percentage.
        float cScrollPercent = EasingFunction::QuadraticInOut(mCharacterSwapTimer, cxAdvCharSwapTicks);

        //--Get the previous entity.
        AdvCombatEntity *rPreviousEntity = rAdventureCombat->GetActiveMemberI(mPrevCharacterSlot);
        if(rPreviousEntity)
        {
            //--Stencil and position.
            TwoDimensionRealPoint tRenderDim = rPreviousEntity->GetUIRenderPosition(ACE_UI_INDEX_EQUIPMENT);

            //--Get portrait.
            StarBitmap *rPortrait = rPreviousEntity->GetCombatPortrait();
            if(rPortrait)
            {
                float cCharXOffset = VIRTUAL_CANVAS_X * (cScrollPercent) * -1.0f;
                rPortrait->Draw(tRenderDim.mXCenter + cCharXOffset, tRenderDim.mYCenter);
            }
        }

        //--Current entity renders above.
        TwoDimensionRealPoint tRenderDim = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_EQUIPMENT);

        //--Get portrait.
        StarBitmap *rPortrait = rActiveEntity->GetCombatPortrait();
        if(rPortrait)
        {
            float cCharXOffset = VIRTUAL_CANVAS_X * (1.0f - cScrollPercent) * -1.0f;
            rPortrait->Draw(tRenderDim.mXCenter + cCharXOffset, tRenderDim.mYCenter);
        }
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIEquip::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Only the top UI renders on this object. It slides in from the top of the screen.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Rendering]
    //--Header.
    AdvImages.rFrame_Top->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Equipment");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Control strings
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mInspectString->DrawText (0.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--This string only appears during replacement mode.
    if(mReplacementTimer > 0)
    {
        //--Completion percentage.
        float cReplacePercent = EasingFunction::QuadraticInOut(mReplacementTimer, cReplaceTicks);

        //--Render with fade.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cReplacePercent * cColorAlpha);
        mScrollFastString->DrawText(0.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIEquip::RenderRgt(float pVisAlpha)
{
    ///--[Documentation]
    //--The right side of the UI shows the object's properties, replacement list, and cursor.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) return;

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Common Rendering]
    //--Right-side arrow.
    float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
    if(AdvCombat::Fetch()->GetActivePartyCount() > 1) AdvImages.rOverlay_ArrowRgt->Draw(cArrowOscillate, 0.0f);

    //--Backing frame.
    AdvImages.rFrame_Rgt->Draw();

    ///--[Equipment List]
    //--Header.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(482.0f, 114.0f, 0, 1.0f, "Slots");
    AdvImages.rFont_Heading->DrawText(767.0f, 114.0f, 0, 1.0f, "Gems");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Listing.
    int tSlots = rActiveEntity->GetEquipmentSlotsTotal();
    for(int i = 0; i < tSlots; i ++)
    {
        //--Entry.
        RenderEquipEntry(i, cColorAlpha);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

        //--If there is another slot past this one, print a divider.
        if(i < tSlots - 1)
        {
            AdvImages.rOverlay_EquipDetail->Draw(0.0f, cSlotH * i);
        }
    }

    ///--[Properties]
    //--Header.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(1110.0f, 114.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Statistics");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Subroutine.
    RenderEquipProperties(pVisAlpha);

    ///--[Replacements]
    //--When selecting a replacement item, this menu appears. If the replacement timer is zero, it does nothing.
    RenderEquipReplacements();

    ///--[Highlight]
    //--Indicates where the player's control cursor is.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rHighlight_Standard);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIEquip::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--The bottom of the UI shows control help strings.

    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Help Strings]
    //--Strings.
    float cYPos = VIRTUAL_CANVAS_Y - cxAdvMainlineFontH;
    mCharacterLftString->DrawText(            0.0f, cYPos,                          SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCharacterRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///===================================== Equipment Details ========================================
void AdvUIEquip::RenderEquipDetails(float pColorAlpha)
{
    ///--[Documentation]
    //--Renders extended details about the item in question. This includes a description frame, expandable header,
    //  description text, and the icon.
    if(mDetailsTimer < 1 || pColorAlpha <= 0.0f) return;

    //--Percentage.
    float cDetailsPercent = EasingFunction::QuadraticInOut(mDetailsTimer, cDetailTicks);

    //--Offsets.
    float cXOffset = 0.0f;
    float cYOffset = VIRTUAL_CANVAS_Y * (1.0f - cDetailsPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cDetailsPercent * 0.7f * pColorAlpha));

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen. Does not fade even if fading is the selected option.
    glTranslatef(cXOffset * 1.0f, cYOffset * 1.0f, 0.0f);

    //--Frame.
    AdvImages.rFrame_Description->Draw();

    ///--[No Item]
    //--Render a fixed-size header and stop if there's no item.
    if(!rDetailsItem || rDetailsItem == AdventureInventory::xrDummyUnequipItem)
    {
        RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, 200.0f, 76.0f, 76.0f, AdvImages.rOverlay_Header);
        glTranslatef(cXOffset * -1.0f, cYOffset * -1.0f, 0.0f);
        return;
    }

    ///--[Header and Name]
    //--Expandable header and name. Note that there is an additional 48 pixels of space on the header
    //  within the dead zones.
    const char *rItemName = rDetailsItem->GetName();
    float cNameLen = AdvImages.rFont_Heading->GetTextWidth(rItemName);
    RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, cNameLen - 48.0f, 76.0f, 76.0f, AdvImages.rOverlay_Header);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 510.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rItemName);

    ///--[Description]
    //--Setup.
    float cXPosition =  50.0f;
    float cYPosition = 558.0f;
    float cYHeight   =  25.0f;

    //--Render advanced description lines.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = rDetailsItem->GetAdvancedDescription(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), 0, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Finish Up]
    //--Clean.
    glTranslatef(cXOffset * -1.0f, cYOffset * -1.0f, 0.0f);
}

///===================================== Equipment Entries ========================================
void AdvUIEquip::RenderEquipEntry(int pSlot, float pColorAlpha)
{
    ///--[Documentation]
    //--When rendering the equipment that the character has equipped, this function is called. These equipment
    //  entries can move around and fade during replacement mode.
    //--An entry consists of the name, icon, and gem slots.

    ///--[Constants]
    //--Colors.
    StarlightColor cDarkened = StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, pColorAlpha);

    ///--[Variables]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCharacterCursor);

    //--Name of slot.
    const char *rSlotName = rActiveEntity->GetNameOfEquipmentSlot(pSlot);
    if(!rSlotName) return;

    //--Get contents.
    AdventureItem *rSlotContents = rActiveEntity->GetEquipmentBySlotI(pSlot);

    //--Compute positions.
    float cNameYPosition  = cSlotNameY  + (cSlotH * pSlot);
    float cEquipYPosition = cEquipNameY + (cSlotH * pSlot);
    float cIconYPosition  = cEquipIconY + (cSlotH * pSlot);
    float cGemYPosition   = cGemIconY   + (cSlotH * pSlot);
    float cRenderAlpha    = pColorAlpha;

    ///--[Replacing]
    //--When in replacement mode, the selected entry scrolls to the top of the menu and all others disappear
    //  behind a stencil block. The zeroth entry fades out if it was not selected.
    if(mReplacementTimer > 0)
    {
        //--Replacement timer percentage.
        float cPercent = EasingFunction::QuadraticInOut(mReplacementTimer, cReplaceTicks);

        //--If this is the selected entry, scroll to the top.
        if(pSlot == mEquipmentCursor)
        {
            float cUseSlot = (float)pSlot * (1.0f - cPercent);
            cNameYPosition  = cSlotNameY  + (cSlotH * cUseSlot);
            cEquipYPosition = cEquipNameY + (cSlotH * cUseSlot);
            cIconYPosition  = cEquipIconY + (cSlotH * cUseSlot);
            cGemYPosition   = cGemIconY   + (cSlotH * cUseSlot);
        }
        //--Otherwise, if we are the zeroth entry, fade out.
        else if(pSlot == 0)
        {
            cRenderAlpha = (1.0f - cPercent) * pColorAlpha;
        }
        //--All other cases, render normally. A stencil block will cover the entry up.
        else
        {

        }
    }

    ///--[Rendering]
    //--Slot is empty, darken it slightly.
    if(!rSlotContents)
    {
        cDarkened.SetAsMixerAlpha(cRenderAlpha);
        AdvImages.rFont_Mainline->DrawText(cSlotNameX,  cNameYPosition, 0, 1.0f, rSlotName);
        AdvImages.rFont_Mainline->DrawText(cEquipNameX, cEquipYPosition, 0, 1.0f, "(Empty)");
        StarlightColor::ClearMixer();
    }
    //--Slot is occupied:
    else
    {
        //--Colors.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cRenderAlpha);

        //--Variables.
        const char *rItemName = rSlotContents->GetName();
        StarBitmap *rIcon = rSlotContents->GetIconImage();

        //--Render.
        AdvImages.rFont_Mainline->DrawText(cSlotNameX, cNameYPosition, 0, 1.0f, rSlotName);
        if(rItemName) AdvImages.rFont_Mainline->DrawTextFixed(cEquipNameX, cEquipYPosition, cItemNameMaxLen, 0, rItemName);
        if(rIcon)     rIcon->Draw(cEquipIconX, cIconYPosition);

        //--Gem slots.
        RenderGemSlotsLine(cGemIconX, cGemYPosition, rSlotContents, AdvImages.rOverlay_GemSlot);

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    }

    ///--[Highlight]
    //--If in replacement mode and this is the selected item, render a differently colored entry.
    if(mIsReplacing && pSlot == mEquipmentCursor)
    {
        //--Position.
        float cLft = cSlotNameX - 4.0f;
        float cTop = cNameYPosition;
        float cRgt = cLft + cGemIconX - cSlotNameX;
        float cBot = cTop + cSlotH - 9.0f;

        //--If this is a gem, modify position.
        if(mGemCursor > -1)
        {
            cLft = cGemIconX + (cGemIconW * mGemCursor) - 4.0f;
            cTop = cGemIconY + (mEquipmentCursor * cSlotH) - 4.0f;
            cRgt = cLft + 30.0f;
            cBot = cTop + 30.0f;
        }

        //--Color.
        StarlightColor::SetMixer(1.0f, 1.0f, 0.0f, pColorAlpha);

        //--Render.
        RenderExpandableHighlight(cLft, cTop, cRgt, cBot, 4.0f, AdvImages.rHighlight_Standard);

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
    }
}

///================================== Equipment Replacements ======================================
void AdvUIEquip::RenderEquipReplacements()
{
    ///--[Documentation]
    //--Renders the replacement submenu. This menu appears when the player selects a piece of equipment
    //  or a gem they want to change. The menu shows all the items that can replace it, including a
    //  special "unequip" item.
    //--The replacement listing appears over the existing listing using a stencil blocking.
    //--Renders replacement for the currently selected slot. This requires a stencil overlay.
    if(mReplacementTimer < 1) return;

    //--Completion percentage.
    float cReplacePercent = EasingFunction::QuadraticInOut(mReplacementTimer, cReplaceTicks);

    ///--[Divider]
    //--Move the divider. It starts at the bottom and moves to the top, "wiping" away the previous entries.
    float cDividerOffset = (1.0f - cReplacePercent) * cReplaceDividerH;
    AdvImages.rOverlay_ReplaceDivider->Draw(0.0f, cDividerOffset);

    ///--[Stencil Overlay]
    //--Create a stencil block over the bottom part of the frame.
    glEnable(GL_STENCIL_TEST);
    glDisable(GL_TEXTURE_2D);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilFunc(GL_ALWAYS, cStencil, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    glStencilMask(0xFF);

    //--Render to the stencil buffer.
    StarBitmap::DrawRectFill(454.0f, cReplaceStartY + cDividerOffset, 924.0f, 682.0f, StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

    //--Swap stencil mode.
    glEnable(GL_TEXTURE_2D);
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilFunc(GL_EQUAL, cStencil, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    glStencilMask(0xFF);

    //--Render the frame again.
    AdvImages.rFrame_Rgt->Draw();

    ///--[Replacement Options]
    //--Setup.
    float cYPosition = cReplaceStartY + cDividerOffset;

    //--Iterate.
    int tCurrent = 0;
    AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->PushIterator();
    while(rItem)
    {
        //--If the current value is below the skip, don't render.
        if(tCurrent < mReplacementSkip)
        {
            tCurrent ++;
            rItem = (AdventureItem *)mValidReplacementsList->AutoIterate();
            continue;
        }

        //--Get and render.
        const char *rItemName = rItem->GetName();
        StarBitmap *rIcon = rItem->GetIconImage();

        //--If this is the "Unequip" item, display "Empty".
        if(rItem == AdventureInventory::xrDummyUnequipItem)
        {
            AdvImages.rFont_Mainline->DrawText(cEquipNameX, cYPosition, 0, 1.0f, "Empty (Unequip)");
        }
        //--Normal case:
        else
        {
            //--Basics.
            if(rItemName) AdvImages.rFont_Mainline->DrawTextFixed(cEquipNameX, cYPosition, cItemNameMaxLen, 0, rItemName);
            if(rIcon)     rIcon->Draw(cEquipIconX, cYPosition);

            //--Gem slots.
            RenderGemSlotsLine(cGemIconX, cYPosition, rItem, AdvImages.rOverlay_GemSlot);
        }

        //--Next.
        tCurrent ++;
        cYPosition = cYPosition + cReplaceStartH;
        rItem = (AdventureItem *)mValidReplacementsList->AutoIterate();
    }

    ///--[Clean]
    //--Clean.
    glDisable(GL_STENCIL_TEST);

    ///--[Scrollbar]
    //--If the replacement list has over cReplaceEntriesPerPage entries, render a scrollbar.
    if(mValidReplacementsList->GetListSize() > cReplaceEntriesPerPage)
    {
        StandardRenderScrollbar(mReplacementSkip, cReplaceEntriesPerPage, mValidReplacementsList->GetListSize(), 278.0f, 366.0f, true, AdvImages.rScrollbar_Static, AdvImages.rScrollbar_Scroller);
    }
}

///=================================== Equipment Properties =======================================
void AdvUIEquip::RenderEquipProperties(float pColorAlpha)
{
    ///--[Documentation]
    //--Renders the character's properties on the right side of the equipment screen. Dynamically adjusts to show
    //  the change in statistics when the player switches weapons or armor or gems.

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCharacterCursor);

    //--Setup variables.
    float cIconX = 984.0f;
    float cIconY = 159.0f;
    float cIconH =  25.0f;
    float cPropertyBaseX = 1104.0f;
    float cPropertyBaseY = cIconY -  2.0f;

    //--Alt-weapon. If the highlighted slot is a weapon alt, the displayed statistics are what the player would
    //  have if they changed weapons in battle.
    EquipmentSlotPack *rPackage = rActiveEntity->GetEquipmentSlotPackageI(mEquipmentCursor);
    if(!rPackage) return;

    ///--[Iterate Across Properties]
    //--Render.
    for(int i = 0; i < ADVUIEQUIP_RESISTANCE_BEGIN; i ++)
    {
        ///--[Basics]
        //--Icon.
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Value.
        int tValue = rActiveEntity->GetStatistic(mStatisticIndexes[i]);

        ///--[Alt Weapon Check]
        //--Check if this is an alt-weapon slot. If so, we need to modify the statistics.
        if(rPackage->mIsWeaponAlternate)
        {
            //--Subtract the value by the current weapon.
            int tWeaponSlot = rActiveEntity->GetSlotForWeaponDamage();
            EquipmentSlotPack *rWeaponPackage = rActiveEntity->GetEquipmentSlotPackageI(tWeaponSlot);

            //--If there's a weapon in the main slot:
            if(rWeaponPackage->mEquippedItem)
            {
                tValue = tValue - rWeaponPackage->mEquippedItem->GetStatistic(mStatisticIndexes[i]);
            }

            //--Add the value of the alternate weapon. This will give the player the stats they'd have if they swapped in battle.
            AdventureItem *rReplaceWeapon = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
            if(rReplaceWeapon) tValue = tValue + rReplaceWeapon->GetStatistic(mStatisticIndexes[i]);
        }

        //--If this is a resistance value and over 1000, render "Immune" instead. This also precludes stat comparisons because
        //  a base value over the immunity threshold cannot be modified.
        if(i >= ADVUIEQUIP_RESISTANCE_BEGIN && tValue >= 1000)
        {
            AdvImages.rFont_Statistics->DrawText(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
            continue;
        }
        //--Render normally.
        else
        {
            AdvImages.rFont_Statistics->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
        }

        ///--[Stat Comparison]
        //--If replacing, show the stat changes.
        if(mIsReplacing)
        {
            ///--[Setup]
            int tOldStatValue = 0;
            int tNewStatValue = 0;

            ///--[Normal]
            if(mGemCursor == -1)
            {
                //--Stat value of the equipped item.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem) tOldStatValue = rCurrentItem->GetStatistic(mStatisticIndexes[i]);

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewStatValue = rItem->GetStatistic(mStatisticIndexes[i]);
                    }
                }
            }
            ///--[Gem Swap]
            else
            {
                //--Stat value of the gem currently in the slot.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem)
                {
                    AdventureItem *rCurrentGem = rCurrentItem->GetGemInSlot(mGemCursor);
                    if(rCurrentGem)
                    {
                        tOldStatValue = rCurrentGem->GetStatistic(mStatisticIndexes[i]);
                    }
                }

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewStatValue = rItem->GetStatistic(mStatisticIndexes[i]);
                    }
                }
            }

            ///--[Render]
            //--If the value in this slot is different from the old value, then display that.
            if(tOldStatValue != tNewStatValue)
            {
                //--Value is lower:
                if(tNewStatValue < tOldStatValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
                }
                //--Higher:
                else
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
                }

                //--Render.
                AdvImages.rFont_Mainline->DrawTextArgs(cPropertyBaseX + 15, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistics->DrawTextArgs(1232.0f, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue + (tNewStatValue - tOldStatValue));

                //--Clean.
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
            }
        }
    }

    ///--[ ======= Resistances and Damage Bonuses ======= ]
    ///--[Setup]
    //--All statistics after ADVUIEQUIP_RESISTANCE_BEGIN have both a resistance value and a damage bonus
    //  value. This renders both.
    float cResistX =  984.0f;
    float cDamageX = 1234.0f;
    float cYBegin  =  337.0f;
    float cIconOffY =   2.0f;
    float cTextOffY =   0.0f;

    //--Overrides.
    cIconX = 1098.0f;

    //--Headings.
    mColorHeading.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Statistics->DrawText( 984.0f, 312.0f, 0, 1.0f, "Resists");
    AdvImages.rFont_Statistics->DrawText(1239.0f, 312.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Damage");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    //--Render.
    for(int i = ADVUIEQUIP_RESISTANCE_BEGIN; i < ADVUIEQUIP_ICONS_TOTAL; i ++)
    {
        ///--[Setup]
        //--Positioning.
        float cYPos = cYBegin + ((i - ADVUIEQUIP_RESISTANCE_BEGIN) * cIconH);

        //--Icon.
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cYPos + cIconOffY);

        //--Get all values.
        int tResistance = rActiveEntity->GetStatistic(mStatisticIndexes[i]);
        int tBonusTags = rActiveEntity->GetTagCount(mBonusTagNames[i]);
        int tMalusTags = rActiveEntity->GetTagCount(mMalusTagNames[i]);

        ///--[Alt Weapon Check]
        //--Check if this is an alt-weapon slot. If so, we need to modify the statistics.
        if(rPackage->mIsWeaponAlternate)
        {
            //--Subtract the value by the current weapon.
            int tWeaponSlot = rActiveEntity->GetSlotForWeaponDamage();
            EquipmentSlotPack *rWeaponPackage = rActiveEntity->GetEquipmentSlotPackageI(tWeaponSlot);

            //--If there's a weapon in the main slot:
            if(rWeaponPackage->mEquippedItem)
            {
                tResistance = tResistance - rWeaponPackage->mEquippedItem->GetStatistic(mStatisticIndexes[i]);
                tBonusTags = tBonusTags - rWeaponPackage->mEquippedItem->GetTagCount(mBonusTagNames[i]);
                tMalusTags = tMalusTags - rWeaponPackage->mEquippedItem->GetTagCount(mMalusTagNames[i]);
            }

            //--Add the value of the alternate weapon. This will give the player the stats they'd have if they swapped in battle.
            AdventureItem *rReplaceWeapon = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
            if(rReplaceWeapon)
            {
                tResistance = tResistance + rReplaceWeapon->GetStatistic(mStatisticIndexes[i]);
                tBonusTags = tBonusTags + rReplaceWeapon->GetTagCount(mBonusTagNames[i]);
                tMalusTags = tMalusTags + rReplaceWeapon->GetTagCount(mMalusTagNames[i]);
            }
        }

        ///--[Resistance]
        //--If this is a resistance value and over 1000, render "Immune" instead. This also precludes stat comparisons because
        //  a base value over the immunity threshold cannot be modified.
        if(tResistance >= 1000)
        {
            AdvImages.rFont_Statistics->DrawText(cResistX, cYPos + cTextOffY, 0, 1.0f, "Immune");
        }
        //--Render normally.
        else
        {
            AdvImages.rFont_Statistics->DrawTextArgs(cResistX, cYPos + cTextOffY, 0, 1.0f, "%i", tResistance);
        }

        ///--[Damage Bonus]
        //--Compute, after alt weapon check is done if needed.
        int tDamageValue = 100 + tBonusTags - tMalusTags;
        if(tDamageValue < 0) tDamageValue = 0;

        //--Render. Only renders when not replacing.
        if(!mIsReplacing)
            AdvImages.rFont_Statistics->DrawTextArgs(cDamageX, cYPos + cTextOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tDamageValue);

        ///--[Stat Comparison]
        //--If replacing, show the stat changes.
        if(mIsReplacing)
        {
            ///--[Setup]
            int tOldResistValue = 0;
            int tNewResistValue = 0;
            int tOldBonusValue = 0;
            int tNewBonusValue = 0;
            int tOldMalusValue = 0;
            int tNewMalusValue = 0;

            ///--[Normal]
            if(mGemCursor == -1)
            {
                //--Stat value of the equipped item.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem)
                {
                    tOldResistValue = rCurrentItem->GetStatistic(mStatisticIndexes[i]);
                    tOldBonusValue = rCurrentItem->GetTagCount(mBonusTagNames[i]);
                    tOldMalusValue = rCurrentItem->GetTagCount(mMalusTagNames[i]);
                }

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewResistValue = rItem->GetStatistic(mStatisticIndexes[i]);
                        tNewBonusValue = rItem->GetTagCount(mBonusTagNames[i]);
                        tNewMalusValue = rItem->GetTagCount(mMalusTagNames[i]);
                    }
                }
            }
            ///--[Gem Swap]
            else
            {
                //--Stat value of the gem currently in the slot.
                AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
                if(rCurrentItem)
                {
                    AdventureItem *rCurrentGem = rCurrentItem->GetGemInSlot(mGemCursor);
                    if(rCurrentGem)
                    {
                        tOldResistValue = rCurrentGem->GetStatistic(mStatisticIndexes[i]);
                        tOldBonusValue = rCurrentGem->GetTagCount(mBonusTagNames[i]);
                        tOldMalusValue = rCurrentGem->GetTagCount(mMalusTagNames[i]);
                    }
                }

                //--Get the replacement item.
                AdventureItem *rItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
                if(rItem)
                {
                    //--If this is the unequip item:
                    if(rItem == AdventureInventory::xrDummyUnequipItem)
                    {
                    }
                    //--Otherwise, get the value.
                    else
                    {
                        tNewResistValue = rItem->GetStatistic(mStatisticIndexes[i]);
                        tNewBonusValue = rItem->GetTagCount(mBonusTagNames[i]);
                        tNewMalusValue = rItem->GetTagCount(mMalusTagNames[i]);
                    }
                }
            }

            ///--[Render]
            //--If the value in this slot is different from the old value, then display that.
            if(tOldResistValue != tNewResistValue)
            {
                //--Value is lower:
                if(tNewResistValue < tOldResistValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
                }
                //--Higher:
                else
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
                }

                //--Render.
                AdvImages.rFont_Mainline->  DrawTextArgs(cResistX + 25.0f, cYPos + cTextOffY - 2.0f, 0, 1.0f, "->");
                AdvImages.rFont_Statistics->DrawTextArgs(cResistX + 50.0f, cYPos + cTextOffY,        0, 1.0f, "%i", tResistance + (tNewResistValue - tOldResistValue));

                //--Clean.
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
            }

            //--Always render the damage bonus value, even if it didn't change.
            int tOldDamageValue = tOldBonusValue - tOldMalusValue;
            int tNewDamageValue = tNewBonusValue - tNewMalusValue;

            //--Value is lower:
            if(tNewDamageValue < tOldDamageValue)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pColorAlpha);
            }
            //--Higher:
            else if(tNewDamageValue > tOldDamageValue)
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pColorAlpha);
            }
            //--Same.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
            }

            //--Render the difference if nonzero.
            if(tNewDamageValue != tOldDamageValue)
            {
                AdvImages.rFont_Statistics->DrawTextArgs(cIconX+22.0f, cYPos + cTextOffY, 0, 1.0f, "%+i", tNewDamageValue - tOldDamageValue);
            }

            //--Render, clean.
            AdvImages.rFont_Statistics->DrawTextArgs(cDamageX, cYPos + cTextOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tDamageValue + tNewDamageValue - tOldDamageValue);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);
        }
    }
}
