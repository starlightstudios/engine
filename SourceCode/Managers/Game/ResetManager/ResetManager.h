///======================================= ResetManager ===========================================
//--Manager responsible for resetting objects back to their basic state, such as when the program
//  returns to the title screen. All resettable objects must possess the IResettable interface.
//--The manager stores resets as human-readable strings. Most programs will want a "TitleScreen"
//  reset state, for when the player quits the game and returns to the title. Other resets may
//  include resetting the map, entities, or audio states.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "Interfaces/IResettable.h"

///===================================== Local Structures =========================================
///--[ResetList]
typedef struct ResetList
{
    int mResetListSize;
    IResettable **mResetList;
}ResetList;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class ResetManager
{
    private:
    //--System
    //--Reset List
    StarLinkedList *mResetList;

    protected:

    public:
    //--System
    ResetManager();
    ~ResetManager();
    static void DeleteResetList(void *pPtr);

    //--Public Variables
    //--Property Queries
    //--Manipulators
    ResetList *AllocateReset(const char *pName, int pSize);
    void SetResetIndex(const char *pName, int pSlot, IResettable *pMember);

    //--Core Methods
    void IssueReset(const char *pResetName);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static ResetManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Game_Reset(lua_State *L);
int Hook_Game_Quit(lua_State *L);

