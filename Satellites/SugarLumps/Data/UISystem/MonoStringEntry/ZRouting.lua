-- |[ =============================== Monoceros String Entry UI ================================ ]|
--This appears when editing the note on a save file. Used in Monoceros Title cases.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "MonoString|"
local saNames = {"Base", "Cursor", "Pressed"}
local saPaths = {"Base", "Cursor", "Pressed"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
