//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void AdventureLevel::CalibrateChaseOffsets(int pFollowIndex, bool pIsRunning, float &sOffsetX, float &sOffsetY)
{
    //--Given a set of chase offsets, "trims" them to better match player movement rates at various catalyst levels.
    //  If the trimming isn't done, following entities tend to stutter as the player moves in decimal values.
    //--Certain values are considered "hot" values and get modded. If the value isn't an exact match, it is ignored.
    int cTotalValues = 7;
    float cHotValues[]  = {10.0f, 15.0f, 21.0f, 23.0f, 32.0f, 47.0f, 68.0f};
    float cCoolValues[] = {11.0f, 16.0f, 22.0f, 22.0f, 31.0f, 48.0f, 67.0f};
    for(int i = 0; i < cTotalValues; i ++)
    {
        if(sOffsetX ==  cHotValues[i]) sOffsetX =  cCoolValues[i];
        if(sOffsetX == -cHotValues[i]) sOffsetX = -cCoolValues[i];
        if(sOffsetY ==  cHotValues[i]) sOffsetY =  cCoolValues[i];
        if(sOffsetY == -cHotValues[i]) sOffsetY = -cCoolValues[i];
    }
}
