///======================================= SysExec_Quit ===========================================
//--Quits the game when executed.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "Global.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class SysExec_Quit : public RootExecutor
{
    private:

    protected:

    public:
    //--System
    SysExec_Quit()
    {

    }
    virtual ~SysExec_Quit()
    {

    }
    virtual void Execute()
    {
        Global::Shared()->gQuit = true;
    }
};
