//--Base
#include "AudioPackageStream.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "AudioManager.h"
#include "DebugManager.h"

///========================================== System ==============================================
AudioPackageStream::AudioPackageStream()
{
    ///--[AudioPackage]
    //--System
    mAudioPackageType = AUDIOPACKAGE_TYPE_STREAM;

    ///--[AudioPackageStream]
    //--System
    mIsTempoStream = false;

    //--Stream-From-Memory
    mIsStreamFromMemory = false;
    mStreamData = NULL;

    //--Special Effects
    #if defined _BASS_AUDIO_
    mEffectPackageName = NULL;
    mEffectHandle = 0;
    #endif
}
AudioPackageStream::~AudioPackageStream()
{
    //--Common.
    Stop();

    //--Deallocate.
    free(mStreamData);
    #if defined _BASS_AUDIO_
    free(mEffectPackageName);
    if(mHandle)      BASS_StreamFree(mHandle);
    if(mTempoHandle) BASS_StreamFree(mTempoHandle);
    #endif
}
void AudioPackageStream::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((AudioPackageStream *)pPtr);
}

///===================================== Property Queries =========================================
bool AudioPackageStream::IsPlaying()
{
    ///--[Documentation and Setup]
    //--Returns whether or not the stream is playing. This is easy for a stream as there is only one instance.
    if(!mIsReady) return false;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    {
        //--Fail if the handle is not set.
        if(!mHandle) return false;

        //--Basic handle is playing.
        if(!mTempoHandle)
        {
            int tCheck = BASS_ChannelIsActive(mHandle);
            if(tCheck == BASS_ACTIVE_STOPPED || tCheck == BASS_ACTIVE_PAUSED) return false;
            return true;
        }
        //--If there is a tempo handle, check if the tempo version is playing:
        else
        {
            int tCheck = BASS_ChannelIsActive(mTempoHandle);
            if(tCheck != BASS_ACTIVE_STOPPED && tCheck != BASS_ACTIVE_PAUSED) return false;
            return true;
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        //--Channel is null, so nothing is playing.
        if(!rChannel) return false;

        //--Otherwise, query the channel.
        FMOD_BOOL tIsPlaying = false;
        FMOD_Channel_IsPlaying(rChannel, &tIsPlaying);
        return tIsPlaying;
    #endif

    ///--[Not Playing]
    return false;
}
float AudioPackageStream::GetPosition()
{
    ///--[Documentation and Setup]
    //--Returns where the stream is in its playback, in seconds. A not-playing stream returns 0, as do errors.
    if(!mIsReady) return 0.0f;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    {
        //--Fail if the handle is not set.
        if(!mHandle) return 0.0f;

        //--Basic stream.
        if(!mTempoHandle)
        {
            int32_t tBytePosition = BASS_ChannelGetPosition(mHandle, BASS_POS_BYTE);
            float tSecondsPosition = (float)BASS_ChannelBytes2Seconds(mHandle, tBytePosition);
            return tSecondsPosition;
        }
        //--Tempo stream.
        else
        {
            int32_t tBytePosition = BASS_ChannelGetPosition(mTempoHandle, BASS_POS_BYTE);
            float tSecondsPosition = (float)BASS_ChannelBytes2Seconds(mTempoHandle, tBytePosition);
            return tSecondsPosition;
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        //--Channel is null, so nothing is playing.
        if(!rChannel) return 0.0f;

        //--Otherwise, query the channel.
        unsigned int tPosition = 0;
        //uint32_t tResult = FMOD_Channel_GetPosition(rChannel, &tPosition, FMOD_TIMEUNIT_MS);
        FMOD_Channel_GetPosition(rChannel, &tPosition, FMOD_TIMEUNIT_MS);

        //--Translate to seconds.
        float tPositionAsSeconds = tPosition / 1000.0f;
        return tPositionAsSeconds;
    #endif

    ///--[Error]
    return 0.0f;
}
float AudioPackageStream::GetDuration()
{
    ///--[Documentation]
    //--Returns the length or duration of the sound, in seconds. If no sound is loaded, returns 0.0f.
    #if defined _BASS_AUDIO_
    if(!mHandle || !mIsReady || AudioManager::xBlockAllAudio) return 0.0f;

    //--Handle or TempoHandle
    uint32_t tUseHandle = mHandle;
    if(mTempoHandle) tUseHandle = mTempoHandle;

    QWORD tLengthInBytes   = BASS_ChannelGetLength(tUseHandle, BASS_POS_BYTE);
    float tLengthInSeconds = BASS_ChannelBytes2Seconds(tUseHandle, tLengthInBytes);
    return tLengthInSeconds;
    #endif
    return 0.0f;
}

///======================================= Manipulators ===========================================
void AudioPackageStream::SetMemoryStream()
{
    //--Marks this as a stream that streams from memory. Must be done before the stream is loaded.
    #if defined _BASS_AUDIO_
    if(mHandle) return;
    mIsStreamFromMemory = true;
    #endif
}
void AudioPackageStream::SetTempoStream()
{
    //--Marks this as a stream that can change tempo, but cannot have special effects. This must be done
    //  before the stream is loaded.
    #if defined _BASS_AUDIO_
    if(mHandle) return;
    mIsTempoStream = true;
    #endif
}
void AudioPackageStream::SetEffectPackage(const char *pPresetName)
{
    ///--[Documentation]
    //--Orders the stream to set the named effect package from the AudioManager's presets as active.
    //  This will take effect immediately if the stream is playing, or next time it plays if not currently playing.
    //--Pass NULL or "Null" to remove the package.
    if(!pPresetName || !strcasecmp(pPresetName, "Null"))
    {
        #if defined _BASS_AUDIO_
        free(mEffectPackageName);
        mEffectPackageName = NULL;
        #endif
        ResolveEffect();
        return;
    }

    //--Locate the package in the AudioManager to make sure it exists.
    void *rCheckPackage = AudioManager::Fetch()->GetEffectProfilePackage(pPresetName);
    if(!rCheckPackage)
    {
        DebugManager::ForcePrint("AudioPackageStream::SetEffectPackage() - Warning, no effect profile %s.\n", pPresetName);
        SetEffectPackage(NULL);
        return;
    }

    //--Store the name, and order the stream to apply the package if it's playing.
    #if defined _BASS_AUDIO_
    ResetString(mEffectPackageName, pPresetName);
    #endif
    ResolveEffect();
}
void AudioPackageStream::Load(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Loads a stream handle and checks it for errors. If required, will create a Tempo version of the stream,
    //  where the base handle is a decoder stream and the playing handle is a Tempo stream that can change
    //  the speed of playback.
    if(!pPath || AudioManager::xBlockAllAudio) return;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_

    //--Normal mode:
    if(!mIsTempoStream)
    {
        //--Stream-From-Memory. Load the file into memory.
        if(mIsStreamFromMemory)
        {
            //--Open the file and determine its size.
            FILE *fInfile = fopen(pPath, "rb");
            if(!fInfile) return;

            //--Resolve the file size.
            fseek(fInfile, 0, SEEK_END);
            int64_t tFileLength = ftell(fInfile);
            if(tFileLength < 1)
            {
                fclose(fInfile);
                return;
            }

            //--Allocate space.
            //fprintf(stderr, "File path %s\n", pPath);
            //fprintf(stderr, " Length: %I64d\n", tFileLength);
            mStreamData = starmemoryalloc(tFileLength);

            //--Reset back to the start, read the data into the array.
            fseek(fInfile, 0, SEEK_SET);
            fread(mStreamData, 1, tFileLength, fInfile);

            //--Close the file.
            fclose(fInfile);
            //fprintf(stderr, " Completed reading.\n");

            //--Open the stream pointing to the memory allocated.
            mHandle      = BASS_StreamCreateFile(true, mStreamData, 0, tFileLength, 0);
            mTempoHandle = 0;
        }
        //--Stream-From-Drive.
        else
        {
            mHandle      = BASS_StreamCreateFile(false, pPath, 0, 0, 0);
            mTempoHandle = 0;
        }

        //--Error check:
        if(mHandle)
        {
            mIsReady = true;
            DebugManager::Print("AudioManager: Loaded %s to %u.\n", pPath, mHandle);
        }

        //--Error printing.
        int tErrorCode = BASS_ErrorGetCode();
        if(tErrorCode)
        {
            fprintf(stderr, "AudioPackageStream::SetTempo() - Error, code is: %i\n", tErrorCode);
        }
    }
    //--Tempo mode:
    else
    {
        //--Load.
        mHandle      = BASS_StreamCreateFile(false, pPath, 0, 0, BASS_STREAM_DECODE);
        mTempoHandle = BASS_FX_TempoCreate(mHandle, 0);

        //--Error check:
        if(mHandle && mTempoHandle)
        {
            mIsReady = true;
            DebugManager::Print("AudioManager: Loaded tempo stream %s to %u.\n", pPath, mHandle);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Load.
        FMOD_System_CreateStream(AudioManager::xFMODSystem, pPath, FMOD_LOOP_NORMAL | FMOD_2D, 0, &mSound);

        //--If there are any subsounds, set the 0th subsound as the playing target.
        FMOD_Sound_GetNumSubSounds(mSound, &mSubSoundsTotal);
        if(mSubSoundsTotal > 0)
        {
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
            FMOD_Sound_GetSubSound(mSound, 0, &rSoundToPlay);
        }
        else
        {
            rSoundToPlay = mSound;
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
        }

        //--Ready.
        mIsReady = true;
    #endif
}
void AudioPackageStream::ResolveEffect()
{
    ///--[Documentation and Setup]
    //--Given the fact that the flag mActivateChorus is true or false, figures out if the stream
    //  is currently playing. If so, adjust the stream to use or not use the chorus effect.
    //--If the stream isn't playing, does nothing, but this routine is called by the Play() routines
    //  immediately after the stream starts.
    //--Tempo streams never have special effects.
    #if defined _BASS_AUDIO_
    if(!mHandle || mTempoHandle) return;

    ///--[Check if Playing]
    //--If the stream isn't playing, do nothing.
    if(!IsPlaying()) return;

    ///--[Chorus Effect Activate]
    //--Apply chorus handle if needed.
    if(mEffectPackageName && !mEffectHandle)
    {
        //--Get the effect package information from the AudioManager.
        EffectProfilePackage *rEffectProfile =  AudioManager::Fetch()->GetEffectProfilePackage(mEffectPackageName);

        //--Not found, issue a warning.
        if(!rEffectProfile)
        {
            //--Warning.
            DebugManager::ForcePrint("AudioPackageStream::ResolveEffect() - Warning, effect profile %s was not found.\n", mEffectPackageName);

            //--Unset.
            ResetString(mEffectPackageName, NULL);
            BASS_ChannelRemoveFX(mHandle, mEffectHandle);
            mEffectHandle = 0;
            return;
        }

        //--Effect found, apply it. The union is the size of the largest data type, so the structure should
        //  always be in the correct format for the provided type.
        mEffectHandle = BASS_ChannelSetFX(mHandle, rEffectProfile->mType, 0);
        if(!mEffectHandle)
        {
            fprintf(stderr, "Error applying FX %i to handle %i: %i\n", rEffectProfile->mType, mHandle, BASS_ErrorGetCode());
        }
        else if(!BASS_FXSetParameters(mEffectHandle, &rEffectProfile->mData))
        {
            fprintf(stderr, "Error setting effect parameters: %i\n", BASS_ErrorGetCode());
        }
    }
    ///--[Chorus Effect Deactivate]
    //--Remove chorus handle if needed.
    else if(!mEffectPackageName && mEffectHandle)
    {
        BASS_ChannelRemoveFX(mHandle, mEffectHandle);
        mEffectHandle = 0;
    }
    #endif
}
void AudioPackageStream::Play()
{
    ///--[ ========== Documentation and Setup =========== ]
    //--Begins playback of the stream, restarting playback from the beginning.
    if(!mIsReady || AudioManager::xBlockAllAudio) return;

    //--Abstract volume.
    float tAbstractVolume = ResolveAbstractVolume();

    ///--[ ================ Bass Version ================ ]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        ///--[Basic Playback]
        //--Common flags.
        mIsFading = false;

        //--Basic handle:
        if(!mTempoHandle)
        {
            BASS_ChannelPlay(mHandle, true);
            BASS_ChannelSetAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume);
        }
        //--Tempo version:
        else
        {
            BASS_ChannelPlay(mTempoHandle, true);
            BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume);
        }

        //--Run special effect checkers.
        ResolveEffect();
    }
    #endif

    ///--[ ================ FMOD Version ================ ]
    #if defined _FMOD_AUDIO_

        //--Error check.
        if(!mIsReady || !rSoundToPlay) return;
        mIsFading = false;

        //--If looping, specify that here.
        if(mStartLoopTime < mEndLoopTime)
        {
            FMOD_Sound_SetLoopPoints(rSoundToPlay, mStartLoopTime * 1000.0f, FMOD_TIMEUNIT_MS, mEndLoopTime * 1000.0f, FMOD_TIMEUNIT_MS);
            FMOD_Sound_SetMode(rSoundToPlay, FMOD_LOOP_NORMAL | FMOD_2D);
            FMOD_Sound_SetLoopCount(rSoundToPlay, -1);
        }

        //--Play the sound.
        FMOD_System_PlaySound(AudioManager::xFMODSystem, rSoundToPlay, 0, false, &rChannel);
        FMOD_Channel_SetVolume(rChannel, mLocalVolume * mGlobalVolume);
    #endif
}
void AudioPackageStream::FadeIn(int pTicks)
{
    ///--[Documentation and Setup]
    //--Causes playback of the stream to restart, and start from 0 volume fading to its normal value. Despite the fact
    //  that this is a fade-in, it does not trip the mIsFading flag, that is used to mark fading out.
    if(!mIsReady || AudioManager::xBlockAllAudio) return;
    mIsFading = false;

    //--Abstract volume factor.
    float tAbstractVolume = ResolveAbstractVolume();

    ///--[Bass Version]
    //--The sliding speed for Bass needs to be provided in seconds, and must be converted from ticks.
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Basic handle:
        if(!mTempoHandle)
        {
            BASS_ChannelSetAttribute(mHandle, BASS_ATTRIB_VOL, 0.00001f);
            BASS_ChannelSlideAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume, pTicks / 60.0f * 1000.0f);
        }
        //--Tempo version:
        else
        {
            BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_VOL, 0.0f);
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume, pTicks / 60.0f * 1000.0f);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Error check.
        if(!mIsReady || !rSoundToPlay) return;

        //--If looping, specify that here.
        if(mStartLoopTime < mEndLoopTime)
        {
            FMOD_Sound_SetLoopPoints(rSoundToPlay, mStartLoopTime * 1000.0f, FMOD_TIMEUNIT_MS, mEndLoopTime * 1000.0f, FMOD_TIMEUNIT_MS);
            FMOD_Sound_SetMode(rSoundToPlay, FMOD_LOOP_NORMAL | FMOD_2D);
            FMOD_Sound_SetLoopCount(rSoundToPlay, -1);
        }

        //--Play the sound but pause it immediately.
        FMOD_System_PlaySound(AudioManager::xFMODSystem, rSoundToPlay, 0, false, &rChannel);
        FMOD_Channel_SetPaused(rChannel, true);

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a 0 fade point here and a 1.0f fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, 0.0f);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), mLocalVolume * mGlobalVolume);

        //--Now unpause it.
        FMOD_Channel_SetPaused(rChannel, false);

        //--Check loop points.
        int tLoopCount = 0;
        uint32_t tStart, tEnd, tMode;
        FMOD_Channel_GetLoopPoints(rChannel, &tStart, FMOD_TIMEUNIT_MS, &tEnd, FMOD_TIMEUNIT_MS);
        FMOD_Channel_GetMode(rChannel, &tMode);
        FMOD_Channel_GetLoopCount(rChannel, &tLoopCount);
    #endif
}
void AudioPackageStream::FadeOut(int pTicks)
{
    ///--[Documentation and Setup]
    //--Causes the stream to fade to 0 volume, but continue playing in the background.
    if(!mIsReady || AudioManager::xBlockAllAudio) return;

    //--Common flags.
    mIsFading = true;

    ///--[Bass Version]
    //--The sliding speed for Bass needs to be provided in seconds, and must be converted from ticks.
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Basic handle:
        if(!mTempoHandle)
        {
            BASS_ChannelSlideAttribute(mHandle, BASS_ATTRIB_VOL, 0.0f, pTicks / 60.0f * 1000.0f);
        }
        //--Tempo version:
        else
        {
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, 0.0f, pTicks / 60.0f * 1000.0f);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--If there's no channel, fail.
        if(!rChannel) return;

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a volume fade point here and a 1.0f fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, mLocalVolume * mGlobalVolume);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), 0.0f);

        //--Add a delayed pause.
        FMOD_Channel_SetDelay(rChannel, 0, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), false);
    #endif
}
void AudioPackageStream::FadeOutToStop(int pTicks)
{
    ///--[Documentation and Setup]
    //--Same as FadeOut() above, except when the volume reaches zero, the stream stops playing.
    if(!mIsReady || AudioManager::xBlockAllAudio) return;

    //--Common flags.
    mIsFading = true;

    ///--[Bass Version]
    //--The sliding speed for Bass needs to be provided in seconds, and must be converted from ticks.
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Basic handle:
        if(!mTempoHandle)
        {
            BASS_ChannelSlideAttribute(mHandle, BASS_ATTRIB_VOL, -1.0f, pTicks / 60.0f * 1000.0f);
        }
        //--Tempo version:
        else
        {
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, -1.0f, pTicks / 60.0f * 1000.0f);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--If there's no channel, fail.
        if(!rChannel) return;

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a volume fade point here and a 1.0f fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, mLocalVolume * mGlobalVolume);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), 0.0f);

        //--Add a delayed stop.
        FMOD_Channel_SetDelay(rChannel, 0, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), true);
    #endif
}
void AudioPackageStream::Pause()
{
    ///--[Documentation and Setup]
    //--Pauses stream playback. Streams can be unpaused to resume playing where they left off.
    if(!mIsReady || AudioManager::xBlockAllAudio) return;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Basic handle:
        if(!mTempoHandle)
        {
            BASS_ChannelPause(mHandle);
        }
        //--Tempo version:
        else if(mTempoHandle)
        {
            BASS_ChannelPause(mTempoHandle);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_SetPaused(rChannel, true);
    #endif
}
void AudioPackageStream::Stop()
{
    ///--[Documentation and Setup]
    //--Entirely stops stream playback. Attempting to play it will start from the beginning.
    if(!mIsReady || AudioManager::xBlockAllAudio) return;

    //--Stop fading.
    mIsFading = false;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Basic handle:
        if(!mTempoHandle)
        {
            BASS_ChannelStop(mHandle);
        }
        //--Tempo version:
        else if(mTempoHandle)
        {
            BASS_ChannelStop(mTempoHandle);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_Stop(rChannel);
    #endif
}
void AudioPackageStream::SetVolume(float pVolume)
{
    ///--[Documentation and Setup]
    //--Modifies the local volume, which affects only this package. This change will take effect
    //  on a stream if it is playing.
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    mLocalVolume = pVolume;

    //--If fading out, stop here.
    if(mIsFading) return;

    //--Abstract volume factor.
    float tAbstractVolume = ResolveAbstractVolume();

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Base version:
        if(!mTempoHandle)
        {
            BASS_ChannelSetAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume);

        }
        //--Tempo version:
        else
        {
            BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_SetVolume(rChannel, mLocalVolume * mGlobalVolume);
    #endif
}
void AudioPackageStream::SetGlobalVolume(float pVolume)
{
    ///--[Documentation and Setup]
    //--Modifies the global volume, which affects all sounds/musics of a given type. This change will
    //  take effect on a stream if it is playing.

    ///--[Common]
    //--Set the volume.
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    mGlobalVolume = pVolume;

    //--If the stream is currently fading out, don't change the stream's properties. Changing the volume
    //  while a stream is fading should continue to have it fade out.
    if(mIsFading) return;

    //--Abstract volume factor.
    float tAbstractVolume = ResolveAbstractVolume();

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Basic version:
        if(!mTempoHandle)
        {
            BASS_ChannelSlideAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume, 1);
        }
        //--Tempo version:
        else
        {
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume, 1);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(rChannel)
        {
            FMOD_Channel_SetVolume(rChannel, mLocalVolume * mGlobalVolume);
        }
    #endif
}
void AudioPackageStream::SlideVolume(float pVolume, int pTicks)
{
    ///--[Documentation and Setup]
    //--Adjusts the stream's volume, from a range of 0 to 1, with 1 being maximum volume. The volume will slide to
    //  the new value over the given number of ticks, if it is playing.
    //--This deliberately bypasses the rule of SetGlobalVolume() above, and causes a change even if the stream
    //  is fading out.

    ///--[Common]
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    #if defined _FMOD_AUDIO_
    float tPreviousVolume = mLocalVolume;
    #endif
    mLocalVolume = pVolume;

    //--Abstract volume factor.
    float tAbstractVolume = ResolveAbstractVolume();

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Base Version
        if(!mTempoHandle)
        {
            BASS_ChannelSlideAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume, pTicks * TICKSTOSECONDS * 1000);
        }
        //--Tempo Version
        else
        {
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume * tAbstractVolume, pTicks * TICKSTOSECONDS * 1000);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--If there's no channel, fail.
        if(!rChannel) return;

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a volume fade point here and a new fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, tPreviousVolume * mGlobalVolume);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), mLocalVolume * mGlobalVolume);
    #endif
}
void AudioPackageStream::SeekTo(float pSeconds)
{
    ///--[Documentation and Setup]
    //--Repositions where a playing stream is to a given spot, in seconds.

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Basic handle:
        if(!mTempoHandle)
        {
            int tBytes = BASS_ChannelSeconds2Bytes(mHandle, pSeconds);
            BASS_ChannelSetPosition(mHandle, tBytes, BASS_POS_BYTE);
        }
        //--Tempo version:
        else
        {
            int tBytes = BASS_ChannelSeconds2Bytes(mTempoHandle, pSeconds);
            BASS_ChannelSetPosition(mTempoHandle, tBytes, BASS_POS_BYTE);
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_SetPosition(rChannel, pSeconds * 1000.0f, FMOD_TIMEUNIT_MS);
    #endif
}
void AudioPackageStream::SetTempo(float pTempo)
{
    ///--[Documentation and Setup]
    //--Modifies the rate at which the stream is playing. Only works if the stream was built
    //  for decoding and has a tempo handle.

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle && mTempoHandle)
    {
        BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_TEMPO, pTempo);
        int tErrorCode = BASS_ErrorGetCode();
        if(tErrorCode)
        {
            fprintf(stderr, "AudioPackageStream::SetTempo() - Error, code is: %i\n", tErrorCode);
        }
    }
    #endif
}

///======================================= Core Methods ===========================================
void AudioPackageStream::AddLoop(float pStartLoop, float pEndLoop)
{
    ///--[Documentation and Setup]
    //--Adds a loop at the given positions, in seconds. Pass in an illogical pair (start is after end)
    //  to disable looping.

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Disable looping.
        if(pStartLoop >= pEndLoop)
        {
            mStartLoopTime = 0.0f;
            mEndLoopTime = 0.0f;

            if(!mTempoHandle)
            {
                BASS_ChannelRemoveSync(mHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            }
            else
            {
                BASS_ChannelRemoveSync(mTempoHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            }
            return;
        }

        //--If a loop already exists, remove it.
        if(pStartLoop < pEndLoop && mExtraData)
        {
            if(!mTempoHandle)
            {
                BASS_ChannelRemoveSync(mHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            }
            else
            {
                BASS_ChannelRemoveSync(mTempoHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            }

            //--Clear extra data pointer.
            free(mExtraData);
            mExtraData = NULL;
        }

        //--Store read-only values.
        mStartLoopTime = pStartLoop;
        mEndLoopTime = pEndLoop;

        //--Get which handle to use.
        int tUseHandle = mHandle;
        if(mTempoHandle) tUseHandle = mTempoHandle;

        //--Data package to be passed, stores the loop start time.
        SetMemoryData(__FILE__, __LINE__);
        AudioPackageLoopData *nData = (AudioPackageLoopData *)starmemoryalloc(sizeof(AudioPackageLoopData));
        nData->mStartInBytes = BASS_ChannelSeconds2Bytes(tUseHandle, pStartLoop);
        mExtraData = nData;

        //--Loop end time.
        uint64_t tEndTime = BASS_ChannelSeconds2Bytes(tUseHandle, pEndLoop);
        BASS_ChannelSetSync(tUseHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME, tEndTime, &AudioManager::LoopCallback, mExtraData);
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Store loop times if no channel is playing.
        mStartLoopTime = pStartLoop;
        mEndLoopTime = pEndLoop;

        //--If no channel is playing, end here.
        if(!rChannel) return;

        //--If an illegal loop set is passed, disable looping.
        if(pStartLoop >= pEndLoop)
        {
            FMOD_Channel_SetLoopCount(rChannel, 0);
            return;
        }

        //--Set the loop points.
        //FMOD_Sound_SetMode(mSound, FMOD_LOOP_NORMAL);
        FMOD_Channel_SetLoopPoints(rChannel, pStartLoop * 1000.0f, FMOD_TIMEUNIT_MS, pEndLoop * 1000.0f, FMOD_TIMEUNIT_MS);
        FMOD_Channel_SetLoopCount(rChannel, -1);

    #endif
}
