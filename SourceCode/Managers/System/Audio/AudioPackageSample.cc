//--Base
#include "AudioPackageSample.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "AudioManager.h"
#include "DebugManager.h"

///========================================== System ==============================================
AudioPackageSample::AudioPackageSample()
{
    ///--[AudioPackage]
    //--System
    mAudioPackageType = AUDIOPACKAGE_TYPE_SAMPLE;

    ///--[AudioPackageSample]
}
AudioPackageSample::~AudioPackageSample()
{
    //--Common.
    Stop();

    //--Deallocate.
    #if defined _BASS_AUDIO_
    if(mHandle) BASS_SampleFree(mHandle);
    #endif
}
void AudioPackageSample::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((AudioPackageSample *)pPtr);
}

///===================================== Property Queries =========================================
bool AudioPackageSample::IsPlaying()
{
    ///--[Documentation and Setup]
    //--Returns whether or not the sample is playing. Because multiple channels can be playing the same
    //  sample, if any one is playing this returns true.
    if(!mIsReady) return false;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    {
        //--Fail if the handle is not set.
        if(!mHandle) return false;

        //--Passing NULL returns how many channels are playing. If it's not zero, the sound is playing.
        HCHANNEL tHandleArray[SAMPLE_MAX_CHANNELS];
        int tPlayingChannels = BASS_SampleGetChannels(mHandle, tHandleArray);
        if(tPlayingChannels < 1) return false;

        //--If all channels are stopped, this is stopped. Otherwise it's playing.
        //fprintf(stderr, "%i channels playing\n", tPlayingChannels);
        for(int i = 0; i < tPlayingChannels; i ++)
        {
            int tCheck = BASS_ChannelIsActive(tHandleArray[i]);
            if(tCheck != BASS_ACTIVE_STOPPED && tCheck != BASS_ACTIVE_PAUSED) return true;
        }
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_
        //--Channel is null, so nothing is playing.
        if(!rChannel) return false;

        //--Otherwise, query the channel.
        FMOD_BOOL tIsPlaying = false;
        FMOD_Channel_IsPlaying(rChannel, &tIsPlaying);
        return tIsPlaying;
    #endif

    ///--[Not Playing]
    return false;
}
float AudioPackageSample::GetPosition()
{
    ///--[Documentation and Setup]
    //--Because samples can be playing on multiple channels, their positions cannot be tracked like this.
    //  You will need to write a per-channel implementation. Returns 0.
    return 0.0f;
}
float AudioPackageSample::GetDuration()
{
    ///--[Documentation]
    //--Returns the length or duration of the sound, in seconds. If no sound is loaded, returns 0.0f.
    #if defined _BASS_AUDIO_
    if(!mHandle || !mIsReady || AudioManager::xBlockAllAudio) return 0.0f;

    //--Handle or TempoHandle
    uint32_t tUseHandle = mHandle;

    QWORD tLengthInBytes   = BASS_ChannelGetLength(tUseHandle, BASS_POS_BYTE);
    float tLengthInSeconds = BASS_ChannelBytes2Seconds(tUseHandle, tLengthInBytes);
    return tLengthInSeconds;
    #endif

    return 0.0f;
}

///======================================= Manipulators ===========================================
void AudioPackageSample::Load(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Loads sample data off the hard drive and checks it for errors.
    if(!pPath || AudioManager::xBlockAllAudio) return;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
        mHandle = BASS_SampleLoad(false, pPath, 0, 0, SAMPLE_MAX_CHANNELS, BASS_SAMPLE_OVER_POS);
        if(mHandle)
        {
            mIsReady = true;
            DebugManager::Print("AudioManager: Loaded %s to %u.\n", pPath, mHandle);
        }
        else
        {
            DebugManager::ForcePrint("AudioManager: Error loading sample %s.\n", pPath);
        }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Load.
        FMOD_System_CreateSound(AudioManager::xFMODSystem, pPath, FMOD_2D, NULL, &mSound);

        //--If there are subsounds, set the 0th subsoound as the target sound.
        FMOD_Sound_GetNumSubSounds(mSound, &mSubSoundsTotal);
        if(mSubSoundsTotal > 0)
        {
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
            FMOD_Sound_GetSubSound(mSound, 0, &rSoundToPlay);
        }
        else
        {
            rSoundToPlay = mSound;
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
        }

        //--Ready.
        mIsReady = true;

    #endif
}
void AudioPackageSample::Play()
{
    ///--[Documentation and Setup]
    //--Begans playback of the sample, creating a new channel and ordering it to play. Note that samples cannot
    //  make use of most special effects, only streams can do that.
    if(!mIsReady || AudioManager::xBlockAllAudio) return;

    ///--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        //--Flags.
        mIsFading = false;

        //--Create a new playback channel.
        uint32_t tChannel = BASS_SampleGetChannel(mHandle, false);
        if(!tChannel) return;

        //--Immediate playback.
        BASS_ChannelPlay(tChannel, true);
        BASS_ChannelSetAttribute(tChannel, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume);

        //--Diagnostics.
        //fprintf(stderr, "Volume is %f %f = %f\n", mLocalVolume, mGlobalVolume, mLocalVolume * mGlobalVolume);
    }
    #endif

    ///--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Error check.
        if(!mIsReady || !rSoundToPlay) return;
        mIsFading = false;

        //--If looping, specify that here.
        if(mStartLoopTime < mEndLoopTime)
        {
            FMOD_Sound_SetLoopPoints(rSoundToPlay, mStartLoopTime * 1000.0f, FMOD_TIMEUNIT_MS, mEndLoopTime * 1000.0f, FMOD_TIMEUNIT_MS);
            FMOD_Sound_SetMode(rSoundToPlay, FMOD_LOOP_NORMAL | FMOD_2D);
            FMOD_Sound_SetLoopCount(rSoundToPlay, -1);
        }

        //--Play the sound.
        FMOD_System_PlaySound(AudioManager::xFMODSystem, rSoundToPlay, 0, false, &rChannel);
        FMOD_Channel_SetVolume(rChannel, mLocalVolume * mGlobalVolume);
    #endif
}

void AudioPackageSample::FadeIn(int pTicks)
{
    //--Does nothing, samples just play and don't fade in.
}
void AudioPackageSample::FadeOut(int pTicks)
{
    //--Does nothing, samples can be playing over multiple channels and cannot be ordered to fade out generally.
}
void AudioPackageSample::FadeOutToStop(int pTicks)
{
    //--Does nothing, samples can be playing over multiple channels and cannot be ordered to fade out generally.
}
void AudioPackageSample::Pause()
{
    //--Samples cannot be ordered to pause.
}
void AudioPackageSample::Stop()
{
    //--Samples cannot be ordered to stop.
}
void AudioPackageSample::SetVolume(float pVolume)
{
    //--Modifies the local volume. Does not affect currently playing channels.
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    mLocalVolume = pVolume;
}
void AudioPackageSample::SetGlobalVolume(float pVolume)
{
    //--Modifies the global volume. Does not affect currently playing channels.
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    mGlobalVolume = pVolume;
}
void AudioPackageSample::SlideVolume(float pVolume, int pTicks)
{
    //--Sets the volume, but does not adjust volume for any playing channels on this sample.
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    mLocalVolume = pVolume;
}
void AudioPackageSample::SeekTo(float pSeconds)
{
    //--Cannot seek on a sample.
}
void AudioPackageSample::SetTempo(float pTempo)
{
    //--Does nothing, only works for streams.
}

///======================================= Core Methods ===========================================
void AudioPackageSample::AddLoop(float pStartLoop, float pEndLoop)
{
    //--Loops do not affect samples.
}
