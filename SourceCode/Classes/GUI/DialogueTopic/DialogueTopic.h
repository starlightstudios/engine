///======================================= DialogueTopic ==========================================
//--Represents a topic in the dialogue system. NPCs can register their names in response to topics and
//  can specify which level of the topic they are able to respond to. NPCs that cannot respond to a topic
//  in its current state will not display that topic when asked about it.
//--Topics have unique names, so registering the same topic twice will fail. Topics use their names to
//  display themselves in the dialogue.
//--There is a system Topic called "Goodbye" which is never registered on the topic lists. It just closes
//  the dialogue UI and may or may not play a dialogue sequence.
//--DialogueTopics are stored on a list in the WorldDialogue.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
typedef struct
{
    int mLastSpokeLevel;
    int mDefaultLevel;
}DTNPCRegPack;

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class DialogueTopic : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;

    //--Level
    int mDefaultLevel;
    int mCurrentLevel;

    //--NPC Registration
    StarLinkedList *mNPCListing; //DTNPCRegPack *, master

    protected:

    public:
    //--System
    DialogueTopic();
    virtual ~DialogueTopic();

    //--Public Variables
    //--Property Queries
    const char *GetInternalName();
    const char *GetDisplayName();
    int GetLevel();
    int GetLevelOfNPC(const char *pNPCName);
    bool HasDiscussedBefore(const char *pNPCName);

    //--Manipulators
    void SetName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetLevel(int pLevel);
    void SetDefaultLevel(int pLevel);
    void SetToDefaultLevel();
    void RegisterNPC(const char *pNPCName, int pLastLevel);
    void SetNPCLevel(const char *pNPCName, int pLevel);
    void MarkAsUnread();
    void RegisterOrOverrideNPC(const char *pNPCName, int pLevel);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    StarLinkedList *GetNPCList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
