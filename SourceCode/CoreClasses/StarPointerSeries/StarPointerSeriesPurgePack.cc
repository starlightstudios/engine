//--Base
#include "StarPointerSeries.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///======================================= Documentation ==========================================
//--Functions for the structure PurgeDatafile which is used by the SPS to track which entries inside
//  various datafiles need to be removed for games which don't use them.

///========================================== System ==============================================
void PurgeDatafile::Initialize()
{
    mInternalName = NULL;
    mEntryList = new StarLinkedList(true);
    mrValidList = new StarLinkedList(true);
}
void PurgeDatafile::DeleteThis(void *pPtr)
{
    //--Null check.
    PurgeDatafile *rPtr = (PurgeDatafile *)pPtr;
    if(!rPtr) return;

    //--Deallocate.
    free(rPtr->mInternalName);
    delete rPtr->mEntryList;
    delete rPtr->mrValidList;
    free(rPtr);
}

///===================================== Property Queries =========================================
bool PurgeDatafile::IsNamePathRegistered(const char *pInfileName, const char *pPath)
{
    ///--[Documentation]
    //--If the entry list already contains an infilename/path combo that matches the one provided,
    //  returns true. Otherwise, returns false.
    if(!pInfileName || !pPath) return false;

    ///--[Execution]
    //--Iterate.
    char *rCheckPath = (char *)mEntryList->PushIterator();
    while(rCheckPath)
    {
        //--Get name.
        const char *rName = mEntryList->GetIteratorName();

        //--Check if both match. If both do, don't record.
        if(!strcasecmp(rName, pInfileName) && !strcasecmp(rCheckPath, pPath))
        {
            mEntryList->PopIterator();
            return true;
        }

        //--Next.
        rCheckPath = (char *)mEntryList->AutoIterate();
    }

    //--Not found.
    return false;
}
const char *PurgeDatafile::IsPathInThisDatafile(const char *pPath)
{
    ///--[Documentation]
    //--Scans the entry list to see if the given path is in this datafile. If it is, returns the infile
    //  name of the entry. If it is not, returns NULL.
    if(!pPath) return NULL;

    ///--[Execution]
    //--Iterate.
    const char *rCheckPath = (const char *)mEntryList->PushIterator();
    while(rCheckPath)
    {
        //--Match. Return the infile name.
        if(!strcasecmp(pPath, rCheckPath))
        {
            const char *rInfileName = mEntryList->GetIteratorName();
            mEntryList->PopIterator();
            return rInfileName;
        }

        //--Next.
        rCheckPath = (const char *)mEntryList->AutoIterate();
    }

    //--Not found.
    return NULL;
}

///======================================= Manipulators ===========================================
void PurgeDatafile::SetName(const char *pName)
{
    ResetString(mInternalName, pName);
}
void PurgeDatafile::StoreInfileNamePath(const char *pInfileName, const char *pPath)
{
    ///--[Documentation]
    //--Stores the given name/path combo. A copy is made of the path for memory access safety.
    if(!pInfileName || !pPath) return;

    ///--[Execution]
    //--Copy, register.
    char *tCopyString = InitializeString(pPath);
    mEntryList->AddElementAsTail(pInfileName, tCopyString, &FreeThis);
}
void PurgeDatafile::StoreValidInfileName(const char *pName)
{
    ///--[Documentation]
    //--Places the name in the mrValidList. If the name is already on the list, does nothing.
    if(!pName) return;

    ///--[Execution]
    //--Duplicate check.
    void *rCheckPtr = mrValidList->GetElementByName(pName);
    if(rCheckPtr) return;

    //--Register, as a dummy pointer.
    mrValidList->AddElement(pName, this);
}

///======================================= List Handling ==========================================
void PurgeDatafile::ClearValidList()
{
    mrValidList->ClearList();
}

///========================================= File I/O =============================================
void PurgeDatafile::WriteToFile(FILE *fOutfile)
{
    ///--[Documentation]
    //--Given an opened file in text mode, writes the expected contents of the file in the required
    //  purge format. If the object contains no valid infile names, does nothing.
    if(!fOutfile || mrValidList->GetListSize() < 1 || !mInternalName) return;

    //--Write the header for this file. Paths are often offset relative to the final path so hand-editing is needed.
    fprintf(fOutfile, "BP_SetProperty(\"Register Command\", \"Open File For Modification\", \"%s\")\n", mInternalName);

    //--For each entry, write it as a removal case.
    const char *rDummyPtr = (const char *)mrValidList->PushIterator();
    while(rDummyPtr)
    {
        //--Write.
        fprintf(fOutfile, "BP_SetProperty(\"Register Command\", \"Dummy Image In File\", \"%s\")\n", mrValidList->GetIteratorName());

        //--Next.
        rDummyPtr = (const char *)mrValidList->AutoIterate();
    }
}
