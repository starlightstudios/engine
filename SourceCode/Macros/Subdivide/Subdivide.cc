//--Base
#include "Subdivide.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "utf8.h"
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
Subdivide::Subdivide()
{
    //--Constructor is private and the class cannot be instantiated.
}

//--Splitting
char *Subdivide::SubdivideString(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, StarFont *pFont, float pFontScale)
{
    ///--[Documentation]
    //--Given a pBaseString, splits that string based on its length. That is, cuts off letters from the end of the string
    //  if the string goes over the provided lengths, starting at the last space before the cutting word.
    //--If there is no space in the entire string, cuts at the last character before the limit.
    //--pCharacter limit sets a hard memory size on the string. Alternately, a font may be provided and the string's
    //  length will be tested using that font against pPixelLimit. If pCharacterLimit is -1, it is not tested,
    //  and if pPixelLimit is -1 or the font is invalid, that will not be tested. If neither limit is provided, the
    //  base string is returned.
    //--The return value is always a heap-allocated string of at least one character. sCharsParsed will represent how many
    //  characters were parsed, and trailing spaces will be parsed but not added to the string.
    sCharsParsed = 0;
    if(!pBaseString) return InitializeString(" ");

    //--First letter was a null, stop.
    if(pBaseString[0] == '\0') return InitializeString(" ");

    //--Check that one of the limits was provided. If not, just return a new copy of the base string.
    if(pCharacterLimit < 1 && (!pFont || pPixelLimit < 1.0f || pFontScale == 0.0f)) return InitializeString(pBaseString);

    //--Set the character parse to 0.
    bool tAtLeastOneNormalLetter = false;

    //--Setup variables.
    bool tIsCheckingFont = (pFont != NULL && pPixelLimit >= 1.0f && pFontScale > 0.0f);
    int tOriginalLen = (int)strlen(pBaseString);
    StarAutoBuffer *tCopyBuf = new StarAutoBuffer();

    //--Begin parsing.
    bool tHitEndNormally = false;
    while(true)
    {
        //--If the character is a '\n', move to the next line immediately.
        if(pBaseString[sCharsParsed] == '\n')
        {
            sCharsParsed ++;
            tHitEndNormally = true;
            break;
        }
        else if(sCharsParsed < tOriginalLen + 1 && pBaseString[sCharsParsed] == '\\' && pBaseString[sCharsParsed+1] == 'n')
        {
            sCharsParsed += 2;
            tHitEndNormally = true;
            break;
        }
        //--Normal case. Append the character.
        else
        {
            tAtLeastOneNormalLetter = true;
            tCopyBuf->AppendCharacter(pBaseString[sCharsParsed]);
        }

        //--Move the cursor up.
        sCharsParsed ++;

        //--Edge check: End of string.
        if(sCharsParsed >= tOriginalLen)
        {
            tHitEndNormally = true;
            break;
        }

        //--Edge check: Character limit.
        if(pCharacterLimit > 0 && sCharsParsed >= pCharacterLimit)
        {
            break;
        }

        //--Edge check: Font length.
        if(tIsCheckingFont)
        {
            //--Turn this into a string temporarily.
            tCopyBuf->AppendNull();
            float cLength = pFont->GetTextWidth((const char *)tCopyBuf->GetRawData()) * pFontScale;

            //--Undo the string conversion.
            tCopyBuf->Seek(-1);

            //--Check the limit.
            if(cLength >= pPixelLimit) break;
        }
    }

    //--Hit the end without at least one valid letter.
    if(!tAtLeastOneNormalLetter)
    {
        delete tCopyBuf;
        return InitializeString(" ");
    }

    //--Turn the copybuffer into a usable string, and deallocate the original buffer.
    tCopyBuf->AppendNull();
    char *nFinalString = (char *)tCopyBuf->LiberateData();
    delete tCopyBuf;

    //--If the end was not hit normally, we need to trim the final string.
    if(!tHitEndNormally)
    {
        //--Now parse backwards to the last space.
        int tCutoffLetter = -1;
        for(int i = (int)strlen(nFinalString) - 1; i >= 0; i --)
        {
            if(nFinalString[i] == ' ')
            {
                tCutoffLetter = i+1;
                break;
            }
        }

        //--If the cutoff letter is -1, then no spaces were found. Just hard-break on the letter.
        if(tCutoffLetter == -1)
        {
            nFinalString[strlen(nFinalString)] = '\0';
            sCharsParsed = strlen(nFinalString);
        }
        //--Otherwise, cutoff on the space.
        else
        {
            //--Cut the buffer.
            nFinalString[tCutoffLetter - 1] = '\0';
            sCharsParsed = tCutoffLetter;
        }
    }

    //--Pass back the new string.
    return nFinalString;
}

char *Subdivide::SubdivideStringBraces(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, StarFont *pFont, float pFontScale)
{
    //--Same as above variant, but uses a "brace stack" to handle [tags like this]. These tags don't get appended to the final version but
    //  are left in the original string, to be used for dialogue cues.
    if(!pBaseString) return InitializeString(" ");

    //--Check that one of the limits was provided. If not, just return a new copy of the base string.
    if(pCharacterLimit < 1 && (!pFont || pPixelLimit < 1.0f || pFontScale == 0.0f)) return InitializeString(pBaseString);

    //--Set the character parse to 0.
    sCharsParsed = 0;

    //--Setup variables.
    bool tIsCheckingFont = (pFont != NULL && pPixelLimit >= 1.0f && pFontScale > 0.0f);
    int tOriginalLen = (int)strlen(pBaseString);
    StarAutoBuffer *tCopyBuf = new StarAutoBuffer();

    //--Brace stack.
    int tBraceLettersSkipped = 0;
    int tBraceStack = 0;

    //--Begin parsing.
    bool tHitEndNormally = false;
    while(true)
    {
        //--If the character is a '\n', move to the next line immediately.
        if(pBaseString[sCharsParsed] == '\n')
        {
            sCharsParsed ++;
            tHitEndNormally = true;
            break;
        }
        //--Hard brace check. Once in a hard brace, ignore letters until we exit one. This is used to place tags
        //  but the tagged text doesn't go into the copied-out string.
        else if(pBaseString[sCharsParsed] == '[')
        {
            tBraceLettersSkipped ++;
            tBraceStack ++;
        }
        //--Hard brace exit.
        else if(pBaseString[sCharsParsed] == ']')
        {
            tBraceLettersSkipped ++;
            tBraceStack --;
            if(tBraceStack < 0) tBraceStack = 0;
        }
        //--Normal case. Append the character.
        else
        {
            //--If currently in a brace stack, this letter does not get appended.
            if(tBraceStack < 1)
            {
                tCopyBuf->AppendCharacter(pBaseString[sCharsParsed]);
            }
            //--Track changes.
            else
            {
                tBraceLettersSkipped ++;
            }
        }

        //--Move the cursor up.
        sCharsParsed ++;

        //--Edge check: End of string.
        if(sCharsParsed >= tOriginalLen)
        {
            tHitEndNormally = true;
            break;
        }

        //--Edge check: Character limit.
        if(pCharacterLimit > 0 && sCharsParsed >= pCharacterLimit)
        {
            break;
        }

        //--Edge check: Font length.
        if(tIsCheckingFont)
        {
            //--Turn this into a string temporarily.
            tCopyBuf->AppendNull();
            float cLength = pFont->GetTextWidth((const char *)tCopyBuf->GetRawData()) * pFontScale;

            //--Undo the string conversion.
            tCopyBuf->Seek(-1);

            //--Check the limit.
            if(cLength >= pPixelLimit) break;
        }
    }

    //--Turn the copybuffer into a usable string, and deallocate the original buffer.
    tCopyBuf->AppendNull();
    char *nFinalString = (char *)tCopyBuf->LiberateData();
    delete tCopyBuf;

    //--If the end was not hit normally, we need to trim the final string.
    if(!tHitEndNormally)
    {
        //--Now parse backwards to the last space.
        int tCutoffLetter = -1;
        for(int i = (int)strlen(nFinalString) - 1; i >= 0; i --)
        {
            if(nFinalString[i] == ' ')
            {
                tCutoffLetter = i+1;
                break;
            }
        }

        //--If the cutoff letter is -1, then no spaces were found. Just hard-break on the letter.
        if(tCutoffLetter == -1)
        {
            nFinalString[strlen(nFinalString)] = '\0';
            sCharsParsed = strlen(nFinalString) + tBraceLettersSkipped;
        }
        //--Otherwise, cutoff on the space.
        else
        {
            //--Cut the buffer.
            nFinalString[tCutoffLetter - 1] = '\0';
            sCharsParsed = tCutoffLetter + tBraceLettersSkipped;
        }
    }

    //--Pass back the new string.
    return nFinalString;
}
StarLinkedList *Subdivide::SubdivideStringToList(const char *pString, const char *pDelimiters)
{
    ///--[Documentation]
    //--Given a string, breaks it apart and populates a StarLinkedList with its sub-components. The string of delimiters provides
    //  which letters cause a string break.
    //--Always returns a list, but on error, the list may have no elements. The list must be deallocated by the caller.
    //--An example of a delimiter list is "| :" which will break up "Dogs|Cats:Turtles Horses" into four strings.
    StarLinkedList *nStringList = new StarLinkedList(true);
    if(!pString || !pDelimiters) return nStringList;

    //--Backslash '\\' is not allowed on the delimiter list.
    int cDelimiterLen = (int)strlen(pDelimiters);
    for(int i = 0; i < cDelimiterLen; i ++)
    {
        if(pDelimiters[i] == '\\') return nStringList;
    }

    //--Create a buffer that will hold strings as they are assembled. A string with no delimiters will only take that
    //  much space, though in most cases considerably less buffer size is needed.
    int cLen = (int)strlen(pString);
    char *tBuffer = (char *)starmemoryalloc(sizeof(char) * (cLen+1));
    memset(tBuffer, 0, sizeof(char) * (cLen+1));

    //--Variables.
    int l = 0;
    int c = 0;
    bool tLastWasEscape = false;

    //--Break the string into a set of substrings.
    for(int i = 0; i < cLen; i ++)
    {
        //--Escape sequences not active, normal copy.
        if(!tLastWasEscape)
        {
            //--Scan across all delimiters.
            bool tWasDelimiterFound = false;
            for(int p = 0; p < cDelimiterLen; p ++)
            {
                //--Match.
                if(pString[i] == pDelimiters[p])
                {
                    tWasDelimiterFound = true;
                    break;
                }
            }

            //--Delimiter was found. Create a new substring.
            if(tWasDelimiterFound)
            {
                //--Create, register.
                char *nString = InitializeString(tBuffer);
                nStringList->AddElementAsTail("X", nString, &FreeThis);

                //--Reset.
                c = 0;
                tBuffer[0] = '\0';
            }
            //--If a backslash is found, this becomes an escape sequence, and the backslash is not copied.
            else if(pString[i] == '\\')
            {
                tLastWasEscape = true;
            }
            //--Copy.
            else
            {
                l ++;
                tBuffer[c+0] = pString[i];
                tBuffer[c+1] = '\0';
                c ++;
            }
        }
        //--Escape sequence is active. The next letter is always copied, regardless of what it is.
        else
        {
            l ++;
            tBuffer[c+0] = pString[i];
            tBuffer[c+1] = '\0';
            c ++;
            tLastWasEscape = false;
        }
    }

    //--If there's anything left in the buffer, put it in the list.
    if(tBuffer[0] != '\0')
    {
        char *nString = InitializeString(tBuffer);
        nStringList->AddElementAsTail("X", nString, &FreeThis);
    }

    //--Clean.
    free(tBuffer);
    return nStringList;
}
StarLinkedList *Subdivide::SubdivideStringToListTags(const char *pString)
{
    ///--[Documentation]
    //--Given a string of format "Letters [tag] letters [tag]" and so on, splits the string into a linked list containing:
    //  String 0: "Letters "
    //  String 1: "[tag]" (this deliberately includes the braces)
    //  etc.
    //--This is typically used for string replacement when parsing. The returned list is heap-allocated and must be deleted
    //  by the caller. The list can legally be empty.
    StarLinkedList *nStringList = new StarLinkedList(true);
    if(!pString) return nStringList;

    ///--[Setup]
    //--Create a buffer that will hold strings as they are assembled. A string with no delimiters will only take that
    //  much space, though in most cases considerably less buffer size is needed.
    int cLen = (int)strlen(pString);
    char *tBuffer = (char *)starmemoryalloc(sizeof(char) * (cLen+1));
    memset(tBuffer, 0, sizeof(char) * (cLen+1));

    //--Variables.
    int l = 0;
    int c = 0;
    bool tIsInBrace = false;
    bool tLastWasEscape = false;

    ///--[Iteration]
    //--Break the string into a set of substrings.
    for(int i = 0; i < cLen; i ++)
    {
        //--Escape sequences not active, normal copy.
        if(!tLastWasEscape)
        {
            //--Not in a brace:
            if(tIsInBrace == false)
            {
                //--A left brace indicates we are at the end of the current string.
                if(pString[i] == '[')
                {
                    //--Flag.
                    tIsInBrace = true;

                    //--Create, register.
                    char *nString = InitializeString(tBuffer);
                    nStringList->AddElementAsTail("X", nString, &FreeThis);

                    //--Reset.
                    tBuffer[0] = '[';
                    tBuffer[1] = '\0';
                    c = 1;
                }
                //--A backslash is an escape character.
                else if(pString[i] == '\\')
                {
                    tLastWasEscape = true;
                }
                //--Copy.
                else
                {
                    l ++;
                    tBuffer[c+0] = pString[i];
                    tBuffer[c+1] = '\0';
                    c ++;
                }
            }
            //--In a brace:
            else
            {
                //--A right brace indicates we are at the end of the current string.
                if(pString[i] == ']')
                {
                    //--Flag.
                    tIsInBrace = false;

                    //--Append the right brace.
                    l ++;
                    tBuffer[c+0] = pString[i];
                    tBuffer[c+1] = '\0';
                    c ++;

                    //--Create, register.
                    char *nString = InitializeString(tBuffer);
                    nStringList->AddElementAsTail("X", nString, &FreeThis);

                    //--Reset.
                    c = 0;
                    tBuffer[0] = '\0';
                }
                //--A backslash is an escape character.
                else if(pString[i] == '\\')
                {
                    tLastWasEscape = true;
                }
                //--Copy.
                else
                {
                    l ++;
                    tBuffer[c+0] = pString[i];
                    tBuffer[c+1] = '\0';
                    c ++;
                }
            }
        }
        //--Escape sequence is active. The next letter is always copied, regardless of what it is.
        else
        {
            l ++;
            tBuffer[c+0] = pString[i];
            tBuffer[c+1] = '\0';
            c ++;
            tLastWasEscape = false;
        }
    }

    //--If there's anything left in the buffer, put it in the list.
    if(tBuffer[0] != '\0')
    {
        char *nString = InitializeString(tBuffer);
        nStringList->AddElementAsTail("X", nString, &FreeThis);
    }

    ///--[Finish]
    free(tBuffer);
    return nStringList;
}

///======================================= Item Quantity ==========================================
char *Subdivide::GetItemNameAndQuantity(const char *pString, int &sQuantity)
{
    ///--[Documentation]
    //--Given a string of format "ItemName x1111" where the quantity is after the x, returns the quantity
    //  as a number and the base item name as a heap-allocated string. Quantity can be 1 if no quantity is located.
    sQuantity = 1;

    ///--[Scan]
    //--Scan for a quantity indicator, starting from the end. If all letters from the end to the " x" are
    //  numbers it is a valid quantity.
    bool tAtLeastOneNumber = false;
    int tXLocation = -1;
    int tStringLen = (int)strlen(pString);
    for(int i = tStringLen-1; i >= 1; i --)
    {
        //--Get the letter.
        char tLetter = pString[i];

        //--If it's a number, continue.
        if(tLetter >= '0' && tLetter <= '9')
        {
            tAtLeastOneNumber = true;
        }
        //--If it's an x, check if the letter before it is a space. If so, this is the end of the numbers.
        else if(tLetter == 'x')
        {
            if(pString[i-1] == ' ')
            {
                tXLocation = i;
                break;
            }
        }
        //--If it's anything else, no quantity was detected.
        else
        {
            break;
            //--Create a duplicate of the original string.
        }
    }

    //--If the X location is still -1, no quantity was found. Return a duplicate of the original string,
    //  the quantity is implicitly 1.
    if(tXLocation == -1 || !tAtLeastOneNumber)
    {
        char *nString = InitializeString(pString);
        return nString;
    }

    ///--[Reconstruct]
    //--Reconstruct the string without the quantity in it.
    char *nItemName = (char *)starmemoryalloc(sizeof(char) * tXLocation);
    strncpy(nItemName, pString, tXLocation-1);
    nItemName[tXLocation-1] = '\0';

    //--Get the quantity out with atoi.
    sQuantity = atoi(&pString[tXLocation+1]);
    return nItemName;
}

///====================================== Tag Replacement =========================================
char *Subdivide::ReplaceTagsWithStrings(const char *pString, int pReplaceEntries, ...)
{
    ///--[Documentation]
    //--Given a string of the format "[sName] does [sThing]", where specific tags are present, replaces those tags with
    //  the provided string wherever they occur. This returns a heap-allocated string, the caller is responsible for
    //  deallocating it. If no tags are detected, returns a copy of the original string.
    //--Tags must start and end with [ and ], to bypass them simply pass in [Bypass] for the replacement and it will be ignored.
    //--Ex: ReplaceTagsWithStrings("[sName] does [sThing] [Silly]", "[sName]", "Izana", "[sThing]", "Hunting", "[Silly]", "[Bypass]")
    //  will return "Izana does Hunting [Silly]".
    //--If a tag is not found it will bark a warning and be placed into the string's final version. To prevent warnings,
    //  use the [Bypass] technique above.
    //--The first letter of the string, after the [, is the type. s for String, i for Integer, f for Float. If a type is not found,
    //  an error will be barked and execution stopped.
    if(!pString)
    {
        char *nEmptyString = (char *)starmemoryalloc(sizeof(char) * 1);
        nEmptyString[0] = '\0';
        return nEmptyString;
    }

    ///--[Replacement Listing]
    //--Setup.
    va_list tArgList;
    va_start(tArgList, pReplaceEntries);
    StarLinkedList *tReplacementList = new StarLinkedList(true);

    //--Iterate:
    for(int i = 0; i < pReplaceEntries; i ++)
    {
        //--Get the strings from the arg list.
        const char *rTagString = va_arg(tArgList, const char *);

        //--If the tag is not at least three letters long, then the [] and type cannot be resolved. Fail.
        if(strlen(rTagString) < 3)
        {
            //--Print.
            fprintf(stderr, "Subdivide:ReplaceTagsWithStrings() - Warning. Tag %s needs to be at least three characters long. Failing.\n", rTagString);

            //--Return an empty string.
            va_end(tArgList);
            char *nEmptyString = (char *)starmemoryalloc(sizeof(char) * 1);
            nEmptyString[0] = '\0';
            return nEmptyString;
        }

        //--Check the second letter for a type. Ints and floats need to be buffered.
        char tTypeLetter = rTagString[1];
        if(tTypeLetter == 'i')
        {
            int tRefValue = va_arg(tArgList, int);
            char *nString = InitializeString("%i", tRefValue);
            tReplacementList->AddElementAsTail(rTagString, nString, &FreeThis);
        }
        else if(tTypeLetter == 'f')
        {
            double tRefValue = va_arg(tArgList, double);
            char *nString = InitializeString("%f", tRefValue);
            tReplacementList->AddElementAsTail(rTagString, nString, &FreeThis);
        }
        //--Strings can be referenced directly off the stack. The list is deallocated before the stack
        //  is cleared, this will remain stable.
        else if(tTypeLetter == 's')
        {
            const char *rRefString = va_arg(tArgList, const char *);
            tReplacementList->AddElementAsTail(rTagString, (void *)rRefString, &DontDeleteThis);
        }
        //--Unhandled.
        else
        {
            //--Print.
            fprintf(stderr, "Subdivide:ReplaceTagsWithStrings() - Warning. Tag %s did not have a type. Failing.\n", rTagString);

            //--Return an empty string.
            va_end(tArgList);
            char *nEmptyString = (char *)starmemoryalloc(sizeof(char) * 1);
            nEmptyString[0] = '\0';
            return nEmptyString;
        }
    }

    //--Finish up.
    va_end(tArgList);

    ///--[String Construction]
    //--Run the subdivide routine on this using both [ and ] as delimiters. This will make every
    //  odd numbered entry a tag, and even-numbered entries the strings between them.
    StarAutoBuffer *nNewBuf = new StarAutoBuffer();
    StarLinkedList *tSplitString = Subdivide::SubdivideStringToListTags(pString);

    //--Run across the strings. Even numbered strings are appended directly, odd numbered strings
    //  check for special tags.
    int i = 0;
    char *rSubString = (char *)tSplitString->PushIterator();
    while(rSubString)
    {
        //--Even numbered.
        if(i % 2 == 0)
        {
            nNewBuf->AppendStringWithoutNull(rSubString);
        }
        //--Odd numbered.
        else
        {
            //--Attempt to locate the entry matching the tag.
            const char *rRepString = (const char *)tReplacementList->GetElementByName(rSubString);

            //--If not found, bark a warning:
            if(!rRepString)
            {
                fprintf(stderr, "Subdivide:ReplaceTagsWithStrings() - Error in string %s. No replacement for %s found.\n", pString, rSubString);
                nNewBuf->AppendStringWithoutNull(rSubString);
            }
            //--If the replacement is "[Bypass]" then put the original string in.
            else if(!strcasecmp(rRepString, "[Bypass]"))
            {
                nNewBuf->AppendStringWithoutNull(rSubString);
            }
            //--Append the replacement.
            else
            {
                nNewBuf->AppendStringWithoutNull(rRepString);
            }
        }

        //--Next.
        i ++;
        rSubString = (char *)tSplitString->AutoIterate();
    }

    //--Finish the string up.
    nNewBuf->AppendNull();

    //--Extract the buffer, this is the return string.
    char *nReturnString = (char *)nNewBuf->LiberateData();

    //--Clean up.
    delete nNewBuf;
    delete tSplitString;
    delete tReplacementList;
    return nReturnString;
}
