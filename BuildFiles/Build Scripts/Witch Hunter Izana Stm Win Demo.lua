-- |[ ========================== Witch Hunter Izana Demo Build, Steam ========================== ]|
--Operating System: Windows
--Library Type: Allegro/SDL
--Distribution: Steam
local sGameName = "Witch Hunter Izana"
local sBuildName = "Windows, Steam, Demo"

--Check existence.
if(BP_Exists(sGameName, sBuildName) == true) then return end

--Create.
BP_Create(sGameName, sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../../Uploaders/WHISteamDemo/WitchHunterIzanaDemo/")

-- |[ ========================================= Common ========================================= ]|
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

-- |[Basics]|
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

-- |[Executables]|
--Executable files.
BP_SetProperty("Register Command", "Copy File",   "Projects/Pandemonium/PandemoniumSteamAL.exe",  "WitchHunterIzanaSteamAL.exe")
BP_SetProperty("Register Command", "Copy File",   "Projects/Pandemonium/PandemoniumSteamSDL.exe", "WitchHunterIzanaSteamSDL.exe")
BP_SetProperty("Register Command", "Change Icon", "WitchHunterIzanaSteamAL.exe",                  "BuildFiles/IconWHI.ico")
BP_SetProperty("Register Command", "Change Icon", "WitchHunterIzanaSteamSDL.exe",                 "BuildFiles/IconWHI.ico")

--DLL files.
BP_SetProperty("Register Command", "Copy File", "bass.dll",    "bass.dll")
BP_SetProperty("Register Command", "Copy File", "bass_fx.dll", "bass_fx.dll")
BP_SetProperty("Register Command", "Copy File", "lua54.dll",   "lua54.dll")

-- |[Bootstraps and Counters]|
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

-- |[Steam Files]|
--The API's dll file, and the program's appid file, need to be copied over.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/steam_api.dll",       "steam_api.dll")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/whi_steam_appid.txt", "steam_appid.txt")

-- |[ =================================== Base Instructions ==================================== ]|
-- |[Text Files]|
--Relevant changelogs.
BP_SetProperty("Register Command", "Copy File", "Changelog (WHI).txt", "Changelog.txt")

--Credits files.
BP_SetProperty("Register Command", "Copy File", "Credits WHI.txt", "Credits.txt")

--Readme.
BP_SetProperty("Register Command", "Copy File", "Readme WHI.txt", "Readme.txt")

-- |[Configuration Files]|
--Engine and Adventure configurations.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Adventure.lua", "Config_Adventure.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",    "Config_Engine.lua")

-- |[ =================================== Engine Subfolders ==================================== ]|
-- |[Data Directory]|
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

-- |[Saves Directory]|
--Copy the controls files.
BP_SetProperty("Register Command", "Create Directory", "Saves")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

--Copy the autosaves directory and the notifier on how to use autosaves.
BP_SetProperty("Register Command", "Create Directory", "Saves/Autosave")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/How to Recover.txt",  "Saves/Autosave/How to Recover.txt")

-- |[Shaders Directory]|
--Copy the Windows/Linux shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/WinLin", "Shaders/WinLin")

-- |[Font Precompiles]|
--Remove these from the folder, they are meant for developer use.
BP_SetProperty("Register Command", "Delete Folder", "Data/FontPrebuild/")

-- |[Overrides]|
--Changes to make sure the game runs standalone.
BP_SetProperty("Register Command", "Delete File", "Data/Scripts/MainMenu/129 Launch Monoceros.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/129 Launch Monoceros.lua", "Data/Scripts/MainMenu/129 Launch Monoceros.lua")

-- |[Delete Script Object Builder]|
BP_SetProperty("Register Command", "Delete Folder", "Data/Object Prototypes/")

-- |[ ==================================== Game Subfolders ===================================== ]|
-- |[Adventure Directories]|
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../adventure/Games/AdventureMode", "Games/AdventureMode")

-- |[Monoceros Directories]|
BP_SetProperty("Register Command", "Copy Folder", "../projectmonoceros/Chapter C", "Games/AdventureMode/Chapter C")

-- |[Overrides]|
--Taken from the special files, used to make Monoceros more standalone.
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/System/101 Graphics Portraits.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/101 Graphics Portraits.lua", "Games/AdventureMode/System/101 Graphics Portraits.lua")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Maps/Nowhere/Constructor.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Nowhere Constructor.lua", "Games/AdventureMode/Maps/Nowhere/Constructor.lua")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/ZLaunch.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/WHI Launch File.lua", "Games/AdventureMode/ZLaunch.lua")

-- |[ ====================================== Mod Cleanup ======================================= ]|
--Delete any mods that are in the assembly folders, the game is not distributed with mods by default.
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Mods/")

--Create a blank folder and put Mod Info.txt in it. This is a blank file that indicates this is a folder users can use for mods.
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Chapter C/Mods")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Mod Info.txt", "Games/AdventureMode/Chapter C/Mods/Mod Info.txt")

--If the engine folder had the file Mod Load Order.txt in it, delete it. The player needs to manually
-- enable mods after installation which generates the load order file.
BP_SetProperty("Register Command", "Delete File", "Mod Load Order.txt")

-- |[ ====================================== File Deletion ===================================== ]|
--Delete files not used for Monoceros. This keeps the file size down.

-- |[Unused Launchers]|
--Witch Hunter Izana hotboots and doesn't need other launcher files.
BP_SetProperty("Register Command", "Delete File", "Data/Scripts/MainMenu/130 Launch Carnation.lua")
BP_SetProperty("Register Command", "Delete File", "Data/Scripts/MainMenu/131 Launch MOTF.lua")
BP_SetProperty("Register Command", "Delete File", "Data/Scripts/MainMenu/132 Launch Peak Freaks.lua")

-- |[Audio Files]|
--Clear folders.
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Audio/Music/")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Audio/PuzzleFight/")

--Music Directory.
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/Music")
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/Music/Ambient Tracks")
BP_SetProperty("Register Command", "Copy File", "../adventure/Games/AdventureMode/Audio/Music/Ambient Tracks/Cave Quiet.ogg",     "Games/AdventureMode/Audio/Music/Ambient Tracks/Cave Quiet.ogg")
BP_SetProperty("Register Command", "Copy File", "../adventure/Games/AdventureMode/Audio/Music/Ambient Tracks/Windy Mountain.ogg", "Games/AdventureMode/Audio/Music/Ambient Tracks/Windy Mountain.ogg")
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/Music/Battle Themes")
BP_SetProperty("Register Command", "Copy File", "../adventure/Games/AdventureMode/Audio/Music/Battle Themes/CombatVictory.ogg", "Games/AdventureMode/Audio/Music/Battle Themes/CombatVictory.ogg")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Audio Routing.lua", "Games/AdventureMode/Audio/Music/ZRouting.lua")

--Puzzlefight Directory.
BP_SetProperty("Register Command", "Create Directory", "Games/AdventureMode/Audio/PuzzleFight")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Puzzlefight Audio Routing.lua", "Games/AdventureMode/Audio/PuzzleFight/ZRouting.lua")

-- |[Datafiles]|
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CassandraTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH1Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH2Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_CH5Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_ChristineTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_GalaDress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_MeiRune.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScn_MeiTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CassandraTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH1Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH2Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_CH5Major.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_ChristineTF.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_GalaDress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_MeiRune.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/AdvScnLD_MeiTF.slf")
    
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_56.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Aquillia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Cassandra.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Christine.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Empress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Florentina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Izuna.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_JX101.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Marriedraunes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Mei.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Mia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Miho.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Miso.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Nina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Odar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_PDU.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Polaris.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sammy.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sanya.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Septima.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sharelock.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Sophie.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_SX399.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Tiffany.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Yukina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Portraits_Zeke.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_56.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Aquillia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Cassandra.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Christine.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Empress.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Florentina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Izuna.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_JX101.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Marriedraunes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Mei.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Mia.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Miho.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Miso.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Nina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Odar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_PDU.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Polaris.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sammy.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sanya.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Septima.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sharelock.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Sophie.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_SX399.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Tiffany.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Yukina.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_Zeke.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH0Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH0Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH1Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH1Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH2Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH2Emote.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH5Combat.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PortraitsLD_CH5Emote.slf")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/PuzzleBattle.slf")

BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Biolabs.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_CRTNoise.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_CryoLower.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_CryoMain.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Equinox.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_LRTEast.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_LRTWest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_MtSarulente.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_NixNedar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Northwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Regulus.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_System.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Trannadar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Westwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/Maps_Westwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Biolabs.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_CRTNoise.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_CryoLower.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_CryoMain.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Equinox.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_LRTEast.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_LRTWest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_MtSarulente.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_NixNedar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Northwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Regulus.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_System.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Trannadar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Westwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/MapsLD_Westwoods.slf")
    
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionAquaticGenetics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionArbonnePlains.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionArcaneUniversity.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsAlpha.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsBeta.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsDatacore.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsDelta.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsEpsilon.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsGamma.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsHydroponics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionBiolabsRaijuRanch.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionCryogenics.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionDimensionalTrap.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionEquinox.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionEvermoonForest.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionLRTFacility.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionNixNedar.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionNorthwoods.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionMtSarulente.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionQuantirEstate.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionQuantirHighWastes.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionRegulusCitySector15.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionRegulusCitySector96.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionSerenityObservatory.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionStarfieldSwamp.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionTrafalGlacier.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionTrannadarTradingPost.slf")
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Datafiles/UIRegionWestwoods.slf")

-- |[Maps]|
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/AAA Debug")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/ArbonnePlains")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Beehive")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/DimensionalTrap")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Evermoon")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/NixNedar")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Quantir")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/QuantirManse")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusArcane")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusBiolabs")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusCity")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusCryo")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusEquinox")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusExterior")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusFinale")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusFlashback")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusLRT")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusManufactory")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusMines")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RegulusSerenity")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/RiverWilds")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/StarfieldSwamp")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/StForas")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/Trafal")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/TrapBasement")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Maps/TrapDungeon")

-- |[ ==================================== Non-Demo Cleanup ==================================== ]|
--Clear all the stuff not used by the demo. First, overwrite the normal versions of certain maps with their demo counterparts.
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Debug/Sample")
BP_SetProperty("Register Command", "Copy Folder", "../projectmonoceros/Chapter C/Maps/Debug/SampleDemo", "Games/AdventureMode/Chapter C/Maps/Debug/Sample")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Debug/SampleDemo")

BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/BowmillGraveyard/GraveyardB")
BP_SetProperty("Register Command", "Copy Folder", "../projectmonoceros/Chapter C/Maps/BowmillGraveyard/GraveyardBDemo", "Games/AdventureMode/Chapter C/Maps/BowmillGraveyard/GraveyardB")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/BowmillGraveyard/GraveyardBDemo")

--Delete the other maps that aren't in the demo. Saves space and prevents people from messing with it!
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Approach")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Chapel")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Cistern")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Dollhouse")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/GoodEnd")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Monastery")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Orcville")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Palace")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Swamp")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/Villa")
BP_SetProperty("Register Command", "Delete Folder", "Games/AdventureMode/Chapter C/Maps/WestBeach")

--Override the debug warp list.
BP_SetProperty("Register Command", "Delete File", "Games/AdventureMode/Chapter C/Maps/Build Debug Warp List.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Monoceros Alt Files/Demo Debug Warp List.lua", "Games/AdventureMode/Chapter C/Maps/Build Debug Warp List.lua")

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the active object.
DL_PopActiveObject()