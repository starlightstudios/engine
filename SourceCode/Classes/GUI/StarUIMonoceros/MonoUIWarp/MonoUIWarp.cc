//--Base
#include "MonoUIWarp.h"
#include "AdvUIWarpStructures.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
MonoUIWarp::MonoUIWarp()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MWRP");

    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUIWarp ======= ]
    ///--[System]
    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Help Strings
    AllocateHelpStrings(cxMonoHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIWarp::~MonoUIWarp()
{
}
void MonoUIWarp::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Warp UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Warp UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",           &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIWarp_Strings *MonoUIWarp::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIWarp_Strings::MonoUIWarp_Strings()
{
    //--Local name: "translatable"
    MonoUIWarp::xrStringData = this;

    //--Help Sequence
    char tBuffer[100];
    for(int i = 0; i < 3; i ++)
    {
        sprintf(tBuffer, "MonoUIWarp_mHelpText%02i", i);
        str.mTextHelp[i] = InitializeString(tBuffer);
    }
}
MonoUIWarp_Strings::~MonoUIWarp_Strings()
{
    for(int i = 0; i < MONOUIWARP_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIWarp_Strings::GetSize()
{
    return MONOUIWARP_STRINGS_TOTAL;
}
void MonoUIWarp_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIWARP_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUIWarp::TakeForeground()
{
    ///--[Documentation]
    //--Same as the base version, but immediately jumps to locations mode. Monoceros only has one
    //  region so there's no need for an extra keypress.
    AdvUIWarp::TakeForeground();

    //--Immediately jump to the 0th region.
    mIsSelectingLocation = true;
    mLocationCursor = 0;
    mLocationSkip = 0;
    FocusOnWarpLocation();
    mLocationPos.Complete();

    //--Cursor.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Locate the region package.
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementBySlot(0);
    if(!rRegionPack) return;

    //--Make sure all the images in the advanced maps have loaded, if any.
    AdvancedMapPack *rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->PushIterator();
    while(rMapPack)
    {
        if(rMapPack->rImage) rMapPack->rImage->LoadDataFromSLF();
        rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->AutoIterate();
    }
}

///======================================= Core Methods ===========================================
void MonoUIWarp::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Execution]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Common lines.
    SetHelpString(i, xrStringData->str.mTextHelp[0], 2, "Activate", "Cancel");
    SetHelpString(i, xrStringData->str.mTextHelp[1]);
    SetHelpString(i, xrStringData->str.mTextHelp[2], 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void MonoUIWarp::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Recomputes cursor position whenever it changes. In grid mode this is just which element is highlighted,
    //  in location selection mode the cursor expands to match the length of the name.
    //--Same as the base call but sizes things a bit differently.

    ///--[Grid]
    if(!mIsSelectingLocation)
    {
        //--Compute visibility.
        float tVisPercent = EasingFunction::QuadraticInOut(mVisibilityTimer, mVisibilityTimerMax);

        //--Pass to subroutine.
        RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, tVisPercent);
        return;
    }

    ///--[Location Select]
    //--Get effective position.
    int tCursorPos = mLocationCursor - mLocationSkip;

    //--Locate the region package.
    int tSelectedRegion = GetGridCodeAtCursor();
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementBySlot(tSelectedRegion);
    if(!rRegionPack) return;

    //--Get the currently selected location package.
    WarpLocationPack *rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->GetElementBySlot(mLocationCursor);
    if(!rLocationPack) return;

    //--Compute.
    float cBuffer = 2.0f;
    float cLft = 31.0f - cBuffer;
    float cTop = 16.0f + (tCursorPos * 25.0f) - cBuffer;
    float cRgt = cLft + (AdvImages.rFont_Mainline->GetTextWidth(rRegionPack->mLocations->GetNameOfElementBySlot(mLocationCursor))) + (cBuffer * 2.0f) + 2.0f;
    float cBot = cTop + (20.0f) + (cBuffer * 2.0f);

    //--If the width of the name is longer than the cap, set it to the cap.
    if(cRgt - cLft > cxAdvMaxLocationNameWid) cRgt = cLft + cxAdvMaxLocationNameWid;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        cxAdvCurTicks);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, cxAdvCurTicks);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIWarp::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Same as the base call, but exiting locations mode exits the UI altogether.
    bool tResult = AdvUIWarp::UpdateForeground(pCannotHandleUpdate);

    //--If selection mode exited, order the UI to hide.
    if(!mIsSelectingLocation && !pCannotHandleUpdate)
    {
        FlagExit();
    }

    //--Finish.
    return tResult;
}
void MonoUIWarp::UpdateBackground()
{
    AdvUIWarp::UpdateBackground();
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIWarp::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Warp mode renders in addition to the base menu's rendering. This means region mode renders at
    //  100% opacity except for the selection grid. Locations mode renders according to a timer and
    //  overlays entirely over regions mode.
    RenderLocationMode();

    //--Help.
    RenderHelp(pVisAlpha);
}
void MonoUIWarp::RenderLocationMode()
{
    ///--[Documentation]
    //--Renders a map and a set of pins indicating where the player can warp to.
    if(mLocationVisTimer < 1) return;

    //--Slide percentage.
    float cFadePct = EasingFunction::QuadraticOut(mLocationVisTimer, mVisibilityTimerMax * 2.0f);

    //--Get the image or advanced set of images for rendering.
    int tSelectedRegion = GetGridCodeAtCursor();
    WarpRegionPack *rRegionPack = (WarpRegionPack *)mRegionData->GetElementBySlot(tSelectedRegion);
    if(!rRegionPack) return;

    ///--[Fullblack]
    //--Don't render anything except a black fadeout if below half on the fade percent.
    if(cFadePct < 0.50f)
    {
        StarBitmap::DrawFullBlack(cFadePct * 2.0f);
        return;
    }

    //--When using advanced rendering, render a black underlay.
    if(rRegionPack->mIsUsingAdvancedMaps) StarBitmap::DrawFullBlack(1.0f);

    ///--[Positioning]
    //--The map scrolls around when the player selects a different area. Set that here.
    float cXOffset = mLocationPos.mXCur;
    float cYOffset = mLocationPos.mYCur;
    glTranslatef(cXOffset, cYOffset, 0.0f);

    //--Scale.
    glScalef(mWarpZoomFactor, mWarpZoomFactor, 1.0f);

    ///--[Map Background]
    //--Render the background image.
    if(!rRegionPack->mIsUsingAdvancedMaps && rRegionPack->rMapImage)
    {
        rRegionPack->rMapImage->Draw();
    }
    //--Render the advanced image set.
    else if(rRegionPack->mIsUsingAdvancedMaps)
    {
        ///--[Stencil Setup]
        glClear(GL_STENCIL_BITS);
        glEnable(GL_STENCIL_TEST);
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_ALWAYS, 0, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilMask(0xFF);

        ///--[Iteration]
        //--Iterate across the advanced map packs.
        AdvancedMapPack *rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->PushIterator();
        while(rMapPack)
        {
            ///--[Validity Check]
            //--Skip if the image is not present for some reason.
            if(!rMapPack->rImage)
            {
                rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->AutoIterate();
                continue;
            }

            ///--[Stencil Handling]
            //--If the layer does not read or write stencils:
            if(!rMapPack->mReadsStencil && !rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_ALWAYS, 0, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            }
            //--Layer reads stencils.
            else if(rMapPack->mReadsStencil && !rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_EQUAL, rMapPack->mReadStencil, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            }
            //--Layer writes stencils.
            else if(!rMapPack->mReadsStencil && rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_ALWAYS, rMapPack->mWriteStencil, 0xFF);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
            }
            //--Layer does both. Use the read value. Doesn't matter much since this doesn't really write anything different.
            else if(rMapPack->mReadsStencil && rMapPack->mWritesStencil)
            {
                glStencilFunc(GL_EQUAL, rMapPack->mReadStencil, 0xFF);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
            }

            //--If the roll is within this pack's range, render.
            if(mAdvancedPackRoll >= rMapPack->mRenderChanceLo && mAdvancedPackRoll <= rMapPack->mRenderChanceHi)
            {
                rMapPack->rImage->Draw();
            }

            //--Next.
            rMapPack = (AdvancedMapPack *)rRegionPack->mAdvancedMapPacks->AutoIterate();
        }
    }

    ///--[Clean]
    //--Unset stencils.
    glDisable(GL_STENCIL_TEST);

    ///--[Map Pins]
    //--Render a pin for all warpable locations on the map.
    float cPinOffX = -9.0f;
    float cPinOffY = -30.0f;

    //--Iterate.
    int i = 0;
    WarpLocationPack *rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->PushIterator();
    while(rLocationPack)
    {
        //--If this is the active pin, render a red one:
        if(i == mLocationCursor)
        {
            //--Compute pin's render coords.
            float cRenderX = rLocationPack->mCoordX + cPinOffX;
            float cRenderY = rLocationPack->mCoordY + cPinOffY;

            //--Render.
            AdvImages.rOverlay_MapPinSelected->Draw(cRenderX, cRenderY);

            //--Modify by the scale.
            float cFinalXOffset = mLocationPos.mXEnd;
            float cFinalYOffset = mLocationPos.mYEnd;
            cRenderX = cRenderX * mWarpZoomFactor;
            cRenderY = cRenderY * mWarpZoomFactor;
            cRenderX = cRenderX + cFinalXOffset;
            cRenderY = cRenderY + cFinalYOffset;

            //--If within the given area, move the listing to the right side.
            if(cRenderX < 522.0f && cRenderY < 157.0f)
            {
                if(mLocationOffsetTimer < cxAdvWarpOffsetTicks) mLocationOffsetTimer ++;
            }
            else
            {
                if(mLocationOffsetTimer > 0) mLocationOffsetTimer --;
            }
        }
        //--Otherwise, render a blue pin.
        else
        {
            AdvImages.rOverlay_MapPinUnselected->Draw(rLocationPack->mCoordX + cPinOffX, rLocationPack->mCoordY + cPinOffY);
        }

        //--Next.
        i ++;
        rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->AutoIterate();
    }

    ///--[Unset Position]
    //--Undo scale.
    glScalef(1.0f / mWarpZoomFactor, 1.0f / mWarpZoomFactor, 1.0f);

    //--Undo area scroll.
    glTranslatef(-cXOffset, -cYOffset, 0.0f);

    ///--[Overlay]
    //--If there is an overlay, render it with a fixed position.
    if(rRegionPack->rMapOverlayImage) rRegionPack->rMapOverlayImage->Draw();

    //--No region map image, black underlay.
    if(!rRegionPack->mIsUsingAdvancedMaps && !rRegionPack->rMapImage)
    {
        StarBitmap::DrawFullBlack(1.0f);
    }
    SetColor("Clear", 1.0f);

    ///--[Listing]
    //--If the map pin is under the listing, move the listing to the right side.
    float cTranslationPct = EasingFunction::QuadraticInOut(mLocationOffsetTimer, cxAdvWarpOffsetTicks);
    float cTranslationVal = 850.0f * cTranslationPct;
    glTranslatef(cTranslationVal, 0.0f, 0.0f);

    //--Appears in the top right or top left, depending on where the map pin is. Shows the names of the warp locations.
    AdvImages.rUnderlay_MapLocationBacking->Draw();

    //--Selection.
    MonoImages.rOverlay_WarpSelect->Draw(mHighlightPos.mXCur - 20.0f, mHighlightPos.mYCur + 2.0f);

    //--Setup.
    int tRender = 0;
    float cTxtLft =  52.0f;
    float cTxtTop =   9.0f;
    float cTxtHei =  25.0f;
    SetColor("Paragraph", 1.0f);

    //--Entries.
    rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->PushIterator();
    while(rLocationPack)
    {
        //--Effective position.
        int tEffectiveRender = tRender - mLocationSkip;

        //--Skip when scrolling.
        if(tEffectiveRender < 0)
        {
            tRender ++;
            rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->AutoIterate();
            continue;
        }

        //--Render name.
        AdvImages.rFont_Mainline->DrawTextFixed(cTxtLft, cTxtTop + (cTxtHei * tEffectiveRender), cxMonoMaxLocationNameWid, 0, rRegionPack->mLocations->GetIteratorName());

        //--Cap.
        if(tEffectiveRender >= cxAdvScrollPage-1)
        {
            rRegionPack->mLocations->PopIterator();
            break;
        }

        //--Next.
        tRender ++;
        rLocationPack = (WarpLocationPack *)rRegionPack->mLocations->AutoIterate();
    }
    SetColor("Clear", 1.0f);

    ///--[Scrollbar]
    //--Nominally there are never enough warp entries in Monoceros to need a scrollbar. This may change with mods.
    /*
    if(rRegionPack->mLocations->GetListSize() > cxAdvScrollPage)
    {
        StandardRenderScrollbar(mLocationSkip, cxAdvScrollPage, rRegionPack->mLocations->GetListSize(), 25.0f, 93.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }*/

    //--Unset pin.
    glTranslatef(-cTranslationVal, 0.0f, 0.0f);

    ///--[Fading]
    //--If we got this far, render a fullblack fade in.
    if(cFadePct < 1.0f)
    {
        float cAlpha = (cFadePct - 0.50f) * 2.0f;
        StarBitmap::DrawFullBlack(1.0f - cAlpha);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
