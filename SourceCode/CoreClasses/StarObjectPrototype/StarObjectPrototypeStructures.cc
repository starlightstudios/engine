//--Base
#include "StarObjectPrototype.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///====================================== StarObjectEnum ==========================================
void StarObjectEnum::Initialize()
{
    //--System
    mEnumVal = 0;
    mString = NULL;
}
void StarObjectEnum::SetString(const char *pString)
{
    ResetString(mString, pString);
}
void StarObjectEnum::DeallocateThis(void *pPtr)
{
    //--Cast, Null check.
    StarObjectEnum *rPtr = (StarObjectEnum *)pPtr;
    if(!rPtr) return;

    //--Deallocate.
    free(rPtr->mString);
}

///====================================== StarObjectField =========================================
///--[System]
void StarObjectField::Initialize()
{
    //--System
    mName = NULL;
    mType = POINTER_TYPE_FAIL;
    mNumeric.i = 0;
    mAlpha = NULL;

    //--Description
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;

    //--Enumerations
    mLockToEnumerations = false;
    mEnumerationsTotal = 0;
    mEnumerations = NULL;
}

///--[Descriptions]
void StarObjectField::AllocateDescriptionLines(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mDescriptionLinesTotal; i ++) free(mDescriptionLines[i]);
    free(mDescriptionLines);

    //--Clear.
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mDescriptionLinesTotal = pTotal;
    mDescriptionLines = (char **)starmemoryalloc(sizeof(char *) * mDescriptionLinesTotal);
    for(int i = 0; i < mDescriptionLinesTotal; i ++) mDescriptionLines[i] = NULL;
}
void StarObjectField::SetDescriptionLine(int pSlot, const char *pDescription)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    ResetString(mDescriptionLines[pSlot], pDescription);
}

///--[Enumerations]
void StarObjectField::AllocateEnumerations(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mEnumerationsTotal; i ++) StarObjectEnum::DeallocateThis(&mEnumerations[i]);
    free(mEnumerations);

    //--Clear.
    mEnumerationsTotal = 0;
    mEnumerations = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mEnumerationsTotal = pTotal;
    mEnumerations = (StarObjectEnum *)starmemoryalloc(sizeof(StarObjectEnum) * mEnumerationsTotal);
    for(int i = 0; i < mEnumerationsTotal; i ++) mEnumerations[i].Initialize();
}
void StarObjectField::SetEnumeration(int pSlot, int pValue, const char *pDescription)
{
    if(pSlot < 0 || pSlot >= mEnumerationsTotal) return;
    mEnumerations[pSlot].mEnumVal = pValue;
    mEnumerations[pSlot].SetString(pDescription);
}
void StarObjectField::CheckAgainstEnumerations()
{
    ///--[Documentation]
    //--If this flag is set, "lock" the integer value to one of the enumerations provided.
    //  If the enumeration is not found, then use the default value which is always the zeroth
    //  enumeration on the list.
    if(!mLockToEnumerations || mEnumerationsTotal < 1) return;

    //--Store default value.
    int tDefaultEnumVal = mEnumerations[0].mEnumVal;

    //--Scan the enumerations.
    for(int i = 0; i < mEnumerationsTotal; i ++)
    {
        if(mEnumerations[i].mEnumVal == mNumeric.i)
        {
            return;
        }
    }

    //--No match was found, lock to the default.
    mNumeric.i = tDefaultEnumVal;
}

///--[Deallocation]
void StarObjectField::DeleteThis(void *pPtr)
{
    //--Cast, null check.
    StarObjectField *rPtr = (StarObjectField *)pPtr;
    if(!rPtr) return;

    //--Deallocate.
    free(rPtr->mName);
    free(rPtr->mAlpha);
    for(int i = 0; i < rPtr->mDescriptionLinesTotal; i ++) free(rPtr->mDescriptionLines[i]);
    free(rPtr->mDescriptionLines);
    for(int i = 0; i < rPtr->mEnumerationsTotal; i ++) StarObjectEnum::DeallocateThis(&rPtr->mEnumerations[i]);
    free(rPtr->mEnumerations);
    free(rPtr);
}
