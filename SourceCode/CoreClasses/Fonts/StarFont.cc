//--Base
#include "StarFont.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "UString.h"

//--Definitions
#include "DeletionFunctions.h"
#include "GlDfn.h"
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"
#include "DebugManager.h"

///--[Notes]
//--StarFonts are different from almost all other classes in that most of the code is library-specific.
//  For this reason, the StarFonts have a .cc file for each, that handles all things - constructor,
//  destructor, implementation.  The only thing not handled is the Lua calls, plus a few parts which
//  are common between all implementations.
//--Also, Bitmap-Mode is handled here to some degree, as it ignores the font types and uses an image
//  to render glyphs.

///========================================== System ==============================================
StarFont::StarFont()
{
    ///--[Documentation]
    //--Makes the StarFont a factory-zero font, will not crash or do anything when used.

    ///--[Variables]
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_SUGARFONT;

    //--[StarFont]
    //--System
    mIsReady = false;
    mOwnsFont = false;
    mTotalGlyphs = 0;
    mLocalName = InitializeString("Null");

    //--Dummy Mode
    mIsDummyMode = false;

    //--Remaps
    mTotalRemaps = 0;
    mRemaps = NULL;

    //--Font storage
    #if defined _FONTTYPE_FREETYPE_
    rFont = NULL;
    #endif

    //--Font precaching
    mEndPadding = 3.0f;
    mPrecachedHandle = 0;
    mTallestCharacter = 15.0f;
    mPrecacheData = NULL;
    mListMetricData = NULL;
    mPrecachedSizeX = 0.0f;
    mPrecachedSizeY = 0.0f;

    //--Offsets
    mInternalSize = SUGARFONT_DEFAULTSIZE;

    //--Kerning
    mNeedsKerning = true;
    mKerningScaler = 1.0f;
    mMonospacing = -1.0f;

    //--Bitmap Mode
    mOwnsBitmapFont = false;
    mIsBitmapMode = false;
    mIsBitmapFixedWidth = true;
    mIsBitmapFlipped = false;
    rAlphabetBitmap = NULL;
    mBitmapCharacters = NULL;

    //--File Output
    mOutputBuffer = NULL;
}
StarFont::~StarFont()
{
    //--Misc.
    free(mLocalName);
    free(mRemaps);
    free(mBitmapCharacters);
    delete mOutputBuffer;

    //--Delete precached data.
    if(mPrecacheData)
    {
        glDeleteTextures(1, &mPrecachedHandle);
        for(int i = 0; i < mTotalGlyphs; i ++)
        {
            if(mPrecacheData[i].mGLList)        glDeleteLists(mPrecacheData[i].mGLList, 1);
            if(mPrecacheData[i].mGLListItalic)  glDeleteLists(mPrecacheData[i].mGLListItalic, 1);
            if(mPrecacheData[i].mGLListXMirror) glDeleteLists(mPrecacheData[i].mGLListXMirror, 1);
            if(mPrecacheData[i].mGLListYMirror) glDeleteLists(mPrecacheData[i].mGLListYMirror, 1);
        }
        free(mPrecacheData);
        mPrecacheData = NULL;
    }

    //--Deallocate list metrics.
    free(mListMetricData);
}

///--[Public Statics]
//--When using SLF files for prebuilding, this flag determines what the program does. The flag can be set
//  to STARFONT_SLF_PREBUILD_NONE to completely ignore prebuilds, in which case the SLF Manager is not queried,
//  to STARFONT_SLF_PREBUILD_COMPILE in which case the font outputs its image/lookup data for later use, or
//  to STARFONT_SLF_PREBUILD_READ in which case the font checks the SLF Manager for lookup data if possible.
int StarFont::xFontSLFMode = STARFONT_SLF_PREBUILD_NONE;

//--When compiling a .slf file for font prebuilding, this is the path used. When reading fonts for prebuilding,
//  this stores the path of the activated .slf file. It can be NULL if none of the above cases are true.
char *StarFont::xFontSLFFilePath = NULL;

bool StarFont::xScaleAffectsX = true;
bool StarFont::xScaleAffectsY = true;

///===================================== Property Queries =========================================
bool StarFont::IsReady()
{
    return mIsReady;
}
bool StarFont::NeedsKerning()
{
   return mNeedsKerning;
}
const char *StarFont::GetName()
{
    return mLocalName;
}
float StarFont::GetKerningBetween(int pLetterLft, int pLetterRgt)
{
    ///--[Documentation]
    //--Returns the kerning distance between two letters as a floating point. The letters are their
    //  unicode values, not their glyph indices.
    float tDefault = mMonospacing;
    if(mMonospacing == -1.0f) tDefault = 0.0f;

    ///--[Error Check]
    //--Error checks.
    if(!mIsReady) return tDefault;
    if(mIsBitmapMode) return GetBitmapTextWidth("A");
    if(!mPrecacheData) return tDefault;

    ///--[Range Check]
    if(pLetterRgt < 0 || pLetterRgt >= mTotalRemaps) return tDefault;

    ///--[Resolve Glyphs]
    //--Compute the left slot. If it's -1, the glyph does not exist.
    int tGlyphLft = GetGlyphFromRemap(pLetterLft);
    if(tGlyphLft == -1) return tDefault;

    //--If the second letter is NULL, then return the width of the character. This is meant for cases
    //  where the user is trying to compute the length of a string.
    if(pLetterRgt == 0)
    {
        if(mMonospacing != -1.0f) return mMonospacing;
        return mPrecacheData[tGlyphLft].mAdvanceX;
    }

    //--If the letter-right is the fullwidth colon, use the basic colon's kerning index.
    if(pLetterRgt == 65306)
    {
        pLetterRgt = GetGlyphFromRemap(':');
    }

    //--Kerning is out of range:
    if(pLetterRgt < SUGARFONT_PRECACHE_OFF || pLetterRgt >= SUGARFONT_PRECACHE_HI)
    {
        //--When using monospacing, return the monospace value instead of the width.
        if(mMonospacing != -1.0f) return mMonospacing;
        return mPrecacheData[tGlyphLft].mAdvanceX;
    }

    //--Return from the lookup table.
    //if(mMonospacing != -1.0f) return mMonospacing;
    return mPrecacheData[tGlyphLft].mKerningDistance[pLetterRgt-SUGARFONT_PRECACHE_OFF];
}
int StarFont::GetGlyphFromRemap(int pUnicodeValue)
{
    ///--[Documentation]
    //--Given a unicode value, finds the glyph index. Most fonts have vast unicode areas that are
    //  not used, so we can't store glyph data for every unicode value. Instead, we use a binary
    //  search to locate the glyph index from the unicode value, which is about as fast as this
    //  operation can get.
    //--If the unicode index is out of range, or not found, returns -1.
    if(pUnicodeValue < 0) return -1;

    //--There are no remaps present, fail.
    if(mTotalRemaps < 1) return -1;

    ///--[Binary Search]
    //--Run the search to get the index of the result. If it comes back -1, it was not found.
    int tIndex = BinaryGlyphSearch(pUnicodeValue, 0, mTotalRemaps);
    if(tIndex == -1)
    {
        return -1;
    }

    //--Found.
    return mRemaps[tIndex].mGlyphIndex;
}
int StarFont::BinaryGlyphSearch(int pUnicodeValue, int pMin, int pMax)
{
    ///--[Documentation]
    //--Binary search attempting to locate a unicode value within an ordered, but sparse, array.

    //--Array is empty / error.
    if(pMax < pMin)
    {
        return -1;
    }

    //--Middle point.
    int tMid = (pMin + pMax) / 2;

    //--Exceeds final position. Error, not found.
    if(tMid >= pMax || tMid < 0) return -1;

    //--Check the given position.
    int tCheckValue = pUnicodeValue - mRemaps[tMid].mUnicodeValue;

    //--After Midpoint
    if(tCheckValue > 0)
    {
        return BinaryGlyphSearch(pUnicodeValue, tMid+1, pMax);
    }
    //--Before midpoint
    else if(tCheckValue < 0)
    {
        return BinaryGlyphSearch(pUnicodeValue, pMin, tMid);
    }

    //--Match
    return tMid;
}

///======================================== Manipulators ==========================================
void StarFont::SetInternalName(const char *pName)
{
    if(!pName) { ResetString(mLocalName, "Null"); return; }
    ResetString(mLocalName, pName);
}
void StarFont::SetAsDummy()
{
    //--Note: Once dummy mode is set, it cannot be unset!
    mIsDummyMode = true;
}
void StarFont::SetKerningScaler(float pAmount)
{
    mKerningScaler = pAmount;
}
void StarFont::SetKerning(int pLft, int pRgt, float pAmount)
{
    ///--[Documentation]
    //--Sets the kerning for a given unicode value. This must be translated into a glyph index.
    return;

    ///--[Error Checks]
    //--Error check.
    if(!mIsReady || !mPrecacheData) return;

    ///--[All Left Cases]
    //--Pass -1 for the left unicode value to make all letters have this right-kerning value.
    if(pLft == -1)
    {
        //--The right value can't also be -1.
        if(pRgt == -1) return;

        //--Resolve the unicode value to use. Remember index 0 is unicode 32.
        int tUseRgt = pRgt - SUGARFONT_PRECACHE_OFF;
        if(tUseRgt < 0 || tUseRgt >= SUGARFONT_PRECACHE_MAX) return;

        //--Run across all existing glyphs.
        for(int i = 0; i < mTotalGlyphs; i ++)
        {
            mPrecacheData[i].mKerningDistance[tUseRgt] = mPrecacheData[i].mWidth + (pAmount * mKerningScaler);
        }
    }
    ///--[All Right Cases]
    //--Pass -1 for the right unicode value to make all letters have this left-kerning value.
    else if(pRgt == -1)
    {
        //--Resolve which glyph the left unicode value refers to.
        int tGlyphLft = GetGlyphFromRemap(pLft);
        if(tGlyphLft < 0 || tGlyphLft >= mTotalGlyphs) return;

        //--Set all 96 unicode values for this glyph.
        for(int i = 0; i < SUGARFONT_PRECACHE_MAX; i ++)
        {
            mPrecacheData[tGlyphLft].mKerningDistance[i] = mPrecacheData[tGlyphLft].mWidth + (pAmount * mKerningScaler);
        }
    }
    ///--[Single Kerning]
    //--Neither left nor right are -1, this is a 1:1 kerning case.
    else
    {
        //--Resolve which glyph the left unicode value refers to.
        int tGlyphLft = GetGlyphFromRemap(pLft);
        if(tGlyphLft < 0 || tGlyphLft >= mTotalGlyphs) return;

        //--Resolve the unicode value to use. Remember index 0 is unicode 32.
        int tUseRgt = pRgt - SUGARFONT_PRECACHE_OFF;
        if(tUseRgt < 0 || tUseRgt >= SUGARFONT_PRECACHE_MAX) return;

        //--Set.
        mPrecacheData[tGlyphLft].mKerningDistance[tUseRgt] = mPrecacheData[tGlyphLft].mWidth + (pAmount * mKerningScaler);
    }
}

///======================================== Core Methods ==========================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
//--Cascading overloads. Each of these assumes neutral properties for the next overload.
void StarFont::DrawText(const char *pText)
{
    DrawText(0.0f, 0.0f, 0, 1.0f, pText);
}
void StarFont::DrawText(float pX, float pY, const char *pText)
{
    DrawText(pX, pY, 0, 1.0f, pText);
}
void StarFont::DrawText(float pX, float pY, int pFlags, const char *pText)
{
    DrawText(pX, pY, pFlags, 1.0f, pText);
}

///--[Color Overload]
//--Uses a boolean to switch colors.
void StarFont::DrawTextColor(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText)
{
    if(pUseHighlight)
    {
        pHighlightColor.SetAsMixer();
    }
    else
    {
        pBaseColor.SetAsMixer();
    }

    DrawText(pX, pY, pFlags, pSize, pText);
    StarlightColor::ClearMixer();
}

///--[Args Version]
//--These versions decode the variable argument list into a single buffer and send that for rendering.
void StarFont::DrawTextArgs(float pX, float pY, int pFlags, float pSize, const char *pText, ...)
{
    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawTextArgs() - Warning. Attempted to use dummy font %s. Printing %s\n", mLocalName, pText); return; }

    ///--[Execution]
    //--Get arg list.
    va_list tArgList;
    va_start(tArgList, pText);

    //--Create a buffer.
    char tBuffer[1024];
    int tLettersPrinted = vsprintf(tBuffer, pText, tArgList);
    va_end (tArgList);

    //--Warning: String was larger than the buffer size. Can cause errors, means the coder should
    //  increase the buffer size or go through pain-in-the-ass efforts to make variable buffering possible.
    if(tLettersPrinted >= SUGARFONT_VAARG_BUF_SIZE)
    {
        fprintf(stderr, "Warning: DrawTextArgs() Printed too many letters (%i) to buffer of size %i.\n", tLettersPrinted, SUGARFONT_VAARG_BUF_SIZE);
    }

    //--Call.
    DrawText(pX, pY, pFlags, pSize, tBuffer);
}

//--Color version.
void StarFont::DrawTextColorArgs(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText, ...)
{
    ///--[Dummy Check]
    if(mIsDummyMode) { fprintf(stderr, "StarFont:DrawTextColorArgs() - Warning. Attempted to use dummy font %s. Printing %s.\n", mLocalName, pText); return; }

    ///--[Execution]
    if(pUseHighlight)
    {
        pHighlightColor.SetAsMixer();
    }
    else
    {
        pBaseColor.SetAsMixer();
    }

    //--Get arg list.
    va_list tArgList;
    va_start(tArgList, pText);

    //--Create a buffer.
    char tBuffer[STD_MAX_LETTERS];
    int tLettersPrinted = vsprintf(tBuffer, pText, tArgList);
    va_end (tArgList);

    //--Warning: String was larger than the buffer size. Can cause errors, means the coder should
    //  increase the buffer size or go through pain-in-the-ass efforts to make variable buffering possible.
    if(tLettersPrinted >= SUGARFONT_VAARG_BUF_SIZE)
    {
        fprintf(stderr, "Warning: DrawTextColorArgs() Printed too many letters (%i) to buffer of size %i.\n", tLettersPrinted, SUGARFONT_VAARG_BUF_SIZE);
    }

    //--Call.
    DrawText(pX, pY, pFlags, pSize, tBuffer);
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
StarFont *StarFont::FetchSystemFont()
{
    //--Fetches one of the system fonts. Which one is determined by the coder, as both a TTF and
    //  Bitmap version should exist. If not found, returns a factory-zero font that does nothing
    //  but will prevent crashes.
    //--Note that both types can be used, in which case the TTF will attempt to resolve and, if that
    //  fails, the Bitmap will try to resolve.
    StarFont *rCheckFont = NULL;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--[TTF Version]
    #if defined _SYSTEMFONT_TTF_
        rCheckFont = (StarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/SystemFont");
        if(rCheckFont) return rCheckFont;
    #endif

    //--[Bitmap Version]
    #if defined _SYSTEMFONT_BITMAP_
        rCheckFont = (StarFont *)rDataLibrary->GetEntry("Root/Fonts/System/Bitmap/SystemFont");
        if(rCheckFont) return rCheckFont;
    #endif

    //--[Failure]
    //--Returns a static-init font.
    DebugManager::ForcePrint("StarFont: Error, unable to resolve a system font!\n");
    static StarFont *xStaticFont = NULL;
    if(!xStaticFont) xStaticFont = new StarFont();
    return xStaticFont;
}

///======================================== Lua Hooking ===========================================
void StarFont::HookToLuaState(lua_State *pLuaState)
{
    /* StarFont_SetBitmapCharacter(iIndex, iLft, iTop, iRgt, iBot, iForcedWid)
       Sets a character's properties in Bitmap mode. iIndex refers to the ASCII value of the character
       and must be between 0 and 255. The iLft-iBot properties refer to the pixel positions of the
       letter's box, and iForcedWid is how many characters the letter occupies in a line. A space,
       for example, is wider than it appears since it has no pixels. */
    lua_register(pLuaState, "StarFont_SetBitmapCharacter", &Hook_StarFont_SetBitmapCharacter);

    /* StarFont_SetKerning(fKerningScaler)
       StarFont_SetKerning(iLftCharacter, iRgtCharacter, fKerning)
       Sets the kerning properties for TTF fonts. The font must be the active object. */
    lua_register(pLuaState, "StarFont_SetKerning", &Hook_StarFont_SetKerning);

    //TODO
    lua_register(pLuaState, "StarFont_BeginCompilation", &Hook_StarFont_BeginCompilation);
    lua_register(pLuaState, "StarFont_FinishCompilation", &Hook_StarFont_FinishCompilation);
    lua_register(pLuaState, "StarFont_OpenCompiledFile", &Hook_StarFont_OpenCompiledFile);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_StarFont_SetBitmapCharacter(lua_State *L)
{
    //StarFont_SetBitmapCharacter(iIndex, iLft, iTop, iRgt, iBot, iForcedWid)
    int tArgs = lua_gettop(L);
    if(tArgs < 6) return LuaArgError("StarFont_SetBitmapCharacter");

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_SUGARFONT)) return LuaTypeError("StarFont_SetBitmapCharacter", L);
    StarFont *rFont = (StarFont *)rDataLibrary->rActiveObject;

    //--Set.
    rFont->SetBitmapCharacterSizes(lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6));

    return 0;
}
int Hook_StarFont_SetKerning(lua_State *L)
{
    //StarFont_SetKerning(fKerningScaler)
    //StarFont_SetKerning(iLftCharacter, iRgtCharacter, fKerning)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("StarFont_SetKerning");

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_SUGARFONT)) return LuaTypeError("StarFont_SetKerning", L);
    StarFont *rFont = (StarFont *)rDataLibrary->rActiveObject;

    //--Set the kerning scaler. Only affects calls made after the scaler is set.
    if(tArgs == 1)
    {
        rFont->SetKerningScaler(lua_tonumber(L, 1));
    }
    //--Set the kerning itself.
    else if(tArgs == 3)
    {
        rFont->SetKerning(lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tonumber(L, 3));
    }

    return 0;
}
int Hook_StarFont_BeginCompilation(lua_State *L)
{
    //StarFont_BeginCompilation(sSLFFilePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("StarFont_BeginCompilation");

    //--Call function to do the rest.
    StarFont::ActivateCompilation(lua_tostring(L, 1));
    return 0;
}
int Hook_StarFont_FinishCompilation(lua_State *L)
{
    //StarFont_FinishCompilation()
    StarFont::FinishCompilation();
    return 0;
}
int Hook_StarFont_OpenCompiledFile(lua_State *L)
{
    //StarFont_OpenCompiledFile(sFilePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("StarFont_OpenCompiledFile");

    StarFont::ActivateReadCompiledFonts(lua_tostring(L, 1));
    return 0;
}
