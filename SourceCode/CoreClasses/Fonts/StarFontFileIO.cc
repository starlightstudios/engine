//--Base
#include "StarFont.h"

//--Classes
//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarFileSystem.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///--[Debug]
//#define STARFONT_COMPILE_DEBUG
#ifdef STARFONT_COMPILE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///======================================== Lump Format ===========================================
//--Format for the FONTINFO lump.
//Header, String. Always 10 bytes, FONTINFO00.
//Name, String. Written as a length-string pair.
//Filtering flag. uint32.
//Texture X Size. uint32.
//Texture Y Size, uint32.
//Texture Data (array). Size is (sizeof(uint32_t) * TexX * TexY). Written as uint8.
//Internal Size. Float.
//Kerning Scaler. Float.
//Remaps Total, uint32
//Remaps Array. One uint32 for each remap.
//End Padding. Float.
//Tallest Character. Float.
//Glyphs Total. uint32
//Precache Data. Of format:
//    Left Offset. Float.
//    Top Offset. Float.
//    Flip-Top Offset. Float.
//    Width. Float.
//    Height. Float.
//    AdvanceX. Float.
//    HoriBearingX. Float.
//    HoriBearingY. Float.
//    Metric Lft. Float.
//    Metric Top. Float.
//    Metric Rgt. Float.
//    Metric Bot. Float.
//    Metric True Width. Float.
//    Metric True Height. Float.
//    Metric Tex Lft. Float.
//    Metric Tex Top. Float.
//    Metric Tex Rgt. Float.
//    Metric Tex Bot. Float.
//    Kerning Distances, Array of Floats. Array length is SUGARFONT_PRECACHE_MAX.


///===================================== Static Functions =========================================
void StarFont::ActivateCompilation(const char *pPath)
{
    ///--[Documentation]
    //--At program start, fonts are typically read from various files (.ttf, .otf, etc) and rasterized
    //  onto bitmaps which are uploaded to the graphics card, along with a lookup of glyphs and where
    //  they are on the bitmap.
    //--This work can be offloaded to a .slf file which stores pre-built versions of the data. This
    //  data is machine-local as the bitmaps are meant to be sized based on how big OpenGL will allow
    //  bitmaps to be on that machine, so it needs to be done on a new engine install or when the
    //  developer edits the fonts.
    //--Call this function from Lua to begin compiling font information into a .slf file for output
    //  when the matching function FinishCompilation() is called. This will overwrite whatever file
    //  is on the provided path.
    if(!pPath) return;

    //--Fail if already in compilation/read mode.
    if(StarFont::xFontSLFMode != STARFONT_SLF_PREBUILD_NONE)
    {
        DebugManager::ForcePrint("StarFont::ActivateCompilation(): Warning. Attempted to activate complation on path %s, but compilation is already active.\n", pPath);
        return;
    }

    //--Debug.
    DebugPush(true, "Starfont:ActivateCompilation() - Begins on path: %s\n", pPath);

    //--Clear.
    StarFont::xFontSLFMode = STARFONT_SLF_PREBUILD_NONE;
    ResetString(StarFont::xFontSLFFilePath, NULL);

    ///--[Verify Legality]
    //--Briefly open the path in write mode. If we can't do that, then the OS is now allowing it because
    //  the path is protected or invalid. Fail.
    FILE *fCheckfile = fopen(pPath, "wb");
    if(!fCheckfile)
    {
        DebugManager::ForcePrint("StarFont::ActivateCompilation(): Error. Unable to open path %s for writing. Failing.\n", pPath);
        DebugPop("Failing due to error.\n");
        return;
    }

    //--Clean.
    fclose(fCheckfile);
    DebugPrint("Completed creation of checkfile.\n");

    ///--[Set Globals]
    //--Setting this flag will cause fonts that load off the hard drive to store their raster data in
    //  lumps which the StarLumpManager will export later.
    StarFont::xFontSLFMode = STARFONT_SLF_PREBUILD_COMPILE;
    ResetString(StarFont::xFontSLFFilePath, pPath);
    DebugPrint("Set globals.\n");

    ///--[StarLumpManager]
    //--Order the SLM to clear itself. We don't want any previously open .slf files to leave their lumps
    //  in our new font file.
    StarLumpManager *rStarLumpManager = StarLumpManager::Fetch();
    rStarLumpManager->Clear();
    DebugPrint("Cleared StarLumpManager.\n");
}
void StarFont::FinishCompilation()
{
    ///--[Documentation]
    //--Once ActivateCompilation() above is called, all font files processed will write lumps to the StarLumpManager.
    //  When all required fonts are done writing, call this function to output the .slf file to the hard drive.
    DebugPrint("Finishing compilation.\n");

    //--Fail if not in compilation mode.
    if(StarFont::xFontSLFMode != STARFONT_SLF_PREBUILD_COMPILE)
    {
        DebugManager::ForcePrint("StarFont::FinishCompilation(): Error. Not in compilation mode.\n");
        return;
    }

    //--Fail if there was no path set.
    if(!StarFont::xFontSLFFilePath)
    {
        DebugManager::ForcePrint("StarFont::FinishCompilation(): Error. No path set.\n");
        return;
    }

    ///--[StarLumpManager]
    //--Order the SLM to write to the hard disk.
    StarLumpManager *rStarLumpManager = StarLumpManager::Fetch();
    rStarLumpManager->MergeLumps();
    DebugPrint("Merged lumps.\n");
    rStarLumpManager->Write(StarFont::xFontSLFFilePath);
    DebugPrint("Wrote to outfile.\n");
    rStarLumpManager->Clear();
    DebugPrint("Cleared StarLumpManager.\n");

    ///--[Clear]
    //--Clean up globals.
    StarFont::xFontSLFMode = STARFONT_SLF_PREBUILD_NONE;
    ResetString(StarFont::xFontSLFFilePath, NULL);
    DebugPrint("Cleaned up globals.\n");

    //--Debug.
    DebugPop("Completed.\n");
}
void StarFont::ActivateReadCompiledFonts(const char *pPath)
{
    ///--[Documentation]
    //--If a .slf file exists at the given path, orders the StarLumpManager to open it and switches
    //  static flags so newly loaded fonts will attempt to use that file instead of loading off
    //  the hard drive.
    //--Pass NULL or "Null" to end compiled mode.
    StarLumpManager *rStarLumpManager = StarLumpManager::Fetch();
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        StarFont::xFontSLFMode = STARFONT_SLF_PREBUILD_NONE;
        rStarLumpManager->Close();
        return;
    }

    ///--[Set]
    //--Check if the file exists. If not, *fail silently*. Font precompiles are not vital to operation
    //  and typically not distributed to users.
    if(StarFileSystem::FileExists(pPath) == false)
    {
        StarFont::xFontSLFMode = STARFONT_SLF_PREBUILD_NONE;
        rStarLumpManager->Close();
        return;
    }

    //--Open.
    rStarLumpManager->Open(pPath);
    if(!rStarLumpManager->IsFileOpen()) return;

    //--Set flag.
    StarFont::xFontSLFMode = STARFONT_SLF_PREBUILD_READ;
}

///========================================= File I/O =============================================
void StarFont::SendAutobufferToSLM()
{
    ///--[Documentation]
    //--Called after font construction, writes the contents of the local object's autobuffer to the
    //  StarLumpManager. If compilation is not active, does nothing.
    if(!mOutputBuffer) return;

    //--Debug.
    DebugPrint("Sending Autobuffer to SLM.\n", mTotalGlyphs);
    DebugPrint(" Total Glyphs: %i\n", mTotalGlyphs);

    ///--[Finish Kerning]
    //--Because kerning can be set externally from a lua file, it isn't until we are outputting to the
    //  SLM that we actually write kerning to the autobuffer.
    for(int i = 0; i < mTotalGlyphs; i ++)
    {
        mOutputBuffer->AppendFloat(mPrecacheData[i].mLftOffset);
        mOutputBuffer->AppendFloat(mPrecacheData[i].mTopOffset);
        mOutputBuffer->AppendFloat(mPrecacheData[i].mFlippedTopOffset);
        mOutputBuffer->AppendFloat(mPrecacheData[i].mWidth);
        mOutputBuffer->AppendFloat(mPrecacheData[i].mHeight);
        mOutputBuffer->AppendFloat(mPrecacheData[i].mAdvanceX);
        mOutputBuffer->AppendFloat(mPrecacheData[i].mHoriBearingX);
        mOutputBuffer->AppendFloat(mPrecacheData[i].mHoriBearingY);
        if(mListMetricData)
        {
            mOutputBuffer->AppendFloat(mListMetricData[i].mLft);
            mOutputBuffer->AppendFloat(mListMetricData[i].mTop);
            mOutputBuffer->AppendFloat(mListMetricData[i].mRgt);
            mOutputBuffer->AppendFloat(mListMetricData[i].mBot);
            mOutputBuffer->AppendFloat(mListMetricData[i].mTrueWid);
            mOutputBuffer->AppendFloat(mListMetricData[i].mTrueHei);
            mOutputBuffer->AppendFloat(mListMetricData[i].mTexLft);
            mOutputBuffer->AppendFloat(mListMetricData[i].mTexTop);
            mOutputBuffer->AppendFloat(mListMetricData[i].mTexRgt);
            mOutputBuffer->AppendFloat(mListMetricData[i].mTexBot);
        }
        else
        {
            for(int p = 0; p < 10; p ++) mOutputBuffer->AppendFloat(0.0f);
        }
        for(int p = 0; p < SUGARFONT_PRECACHE_MAX; p ++)
        {
            mOutputBuffer->AppendFloat(mPrecacheData[i].mKerningDistance[p]);
        }
    }

    //--Debug.
    DebugPrint(" Finished outputting glyph data.\n");

    ///--[Send Lump Out]
    //--Create lump.
    RootLump *nLump = (RootLump *)starmemoryalloc(sizeof(RootLump));
    nLump->Init();
    ResetString(nLump->mLookup.mName, mLocalName);
    nLump->mOwnsData = true;

    //--The output buffer uses a segmented internal array, and it is probably quite large as font files can get big.
    //  Therefore, specify that the lump uses a StarArray.
    nLump->mData = NULL;
    nLump->mUsesStarArray = true;
    nLump->mDataSize = mOutputBuffer->GetCursor();//Must be called before liberating the array!
    nLump->rDataObject = mOutputBuffer->LiberateArray();

    //--Debug.
    DebugPrint(" Lump Name: %s\n", nLump->mLookup.mName);
    DebugPrint(" Lump Size: %i\n", nLump->mDataSize);
    DebugPrint(" Sending to SLM.\n");

    //--Send to the SLM as a lump. The SLM takes ownership of the data.
    StarLumpManager *rStarLumpManager = StarLumpManager::Fetch();
    rStarLumpManager->RegisterLump(nLump);

    ///--[Clean]
    //--Deallocate the autobuffer.
    delete mOutputBuffer;
    mOutputBuffer = NULL;

    //--Debug.
    DebugPrint("Completed autobuffer send.\n");
}
bool StarFont::ReadFromSLM()
{
    ///--[Documentation]
    //--If all flags are set up, all the information that makes a font can be read out of an SLF file.
    //  This does that.
    //--Returns true if the font was built from the file, false if not found or other error.
    if(StarFont::xFontSLFMode != STARFONT_SLF_PREBUILD_READ) return false;

    ///--[Find Lump]
    //--Search for the lump with the matching name for this font.
    StarLumpManager *rStarLumpManager = StarLumpManager::Fetch();
    if(!rStarLumpManager->DoesLumpExist(mLocalName)) return false;
    rStarLumpManager->StandardSeek(mLocalName, "FONTINFO");
    VirtualFile *rFile = rStarLumpManager->GetVirtualFile();
    mNeedsKerning = false;

    ///--[Begin Reading]
    //--Skip the name. This file format uses 2 bytes for string lengths.
    rFile->SetUseOneByteForStringLengths(false);
    rFile->SkipLenString();

    //--Filtering flags.
    uint32_t tFilterFlags = 0;
    rFile->Read(&tFilterFlags, sizeof(uint32_t), 1);

    //--Expected X/Y sizes.
    rFile->Read(&mPrecachedSizeX, sizeof(float), 1);
    rFile->Read(&mPrecachedSizeY, sizeof(float), 1);

    ///--[Texture Upload]
    //--Generate.
    glGenTextures(1, &mPrecachedHandle);
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--Filter using GL_NEAREST for magnification. The font will remain pixellated. Good for small fonts.
    if(tFilterFlags & SUGARFONT_PRECACHE_WITH_NEAREST)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    //--Linear filtering. Good for fonts that are going to be large on screen.
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    //--Common. These properties do not change with the filter flag.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    //--Create a buffer for the texture.
    SetMemoryData(__FILE__, __LINE__);
    uint32_t *tTextureData = (uint32_t *)starmemoryalloc(sizeof(uint32_t) * mPrecachedSizeX * mPrecachedSizeY);
    memset(tTextureData, 0, sizeof(uint32_t) * mPrecachedSizeX * mPrecachedSizeY);
    rFile->Read(tTextureData, sizeof(uint32_t), mPrecachedSizeX * mPrecachedSizeY);

    //--Upload.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mPrecachedSizeX, mPrecachedSizeY, 0, GL_RGBA, GL_UNSIGNED_BYTE, tTextureData);

    ///--[Other Flags]
    //--Load.
    rFile->Read(&mInternalSize, sizeof(float), 1);
    rFile->Read(&mKerningScaler, sizeof(float), 1);

    ///--[Remaps]
    //--Read total, allocate space.
    rFile->Read(&mTotalRemaps, sizeof(uint32_t), 1);
    mRemaps = (StarFontGlyphRemap *)starmemoryalloc(sizeof(StarFontGlyphRemap) * mTotalRemaps);

    //--Read remaps.
    for(int i = 0; i < mTotalRemaps; i ++)
    {
        rFile->Read(&mRemaps[i].mGlyphIndex,   sizeof(uint32_t), 1);
        rFile->Read(&mRemaps[i].mUnicodeValue, sizeof(uint32_t), 1);
    }

    ///--[Flags]
    rFile->Read(&mEndPadding, sizeof(float), 1);
    rFile->Read(&mTallestCharacter, sizeof(float), 1);

    //--Monospacing is read from the lua file.
    mMonospacing = xNextMonospace;

    ///--[Glyphs]
    //--Read total, allocate space.
    rFile->Read(&mTotalGlyphs, sizeof(uint32_t), 1);
    SetMemoryData(__FILE__, __LINE__);
    mPrecacheData = (StarFontPrecacheData *)starmemoryalloc(sizeof(StarFontPrecacheData) * mTotalGlyphs);

    //--Read glyph precache.
    for(int i = 0; i < mTotalGlyphs; i ++)
    {
        //--Precache Data.
        rFile->Read(&mPrecacheData[i].mLftOffset, sizeof(float), 1);
        rFile->Read(&mPrecacheData[i].mTopOffset, sizeof(float), 1);
        rFile->Read(&mPrecacheData[i].mFlippedTopOffset, sizeof(float), 1);
        rFile->Read(&mPrecacheData[i].mWidth, sizeof(float), 1);
        rFile->Read(&mPrecacheData[i].mHeight, sizeof(float), 1);
        rFile->Read(&mPrecacheData[i].mAdvanceX, sizeof(float), 1);
        rFile->Read(&mPrecacheData[i].mHoriBearingX, sizeof(float), 1);
        rFile->Read(&mPrecacheData[i].mHoriBearingY, sizeof(float), 1);

        //--List Data. Not stored, used immediately.
        float tLft, tTop, tRgt, tBot, tTrueWid, tTrueHei, tTexLft, tTexTop, tTexRgt, tTexBot;
        rFile->Read(&tLft, sizeof(float), 1);
        rFile->Read(&tTop, sizeof(float), 1);
        rFile->Read(&tRgt, sizeof(float), 1);
        rFile->Read(&tBot, sizeof(float), 1);
        rFile->Read(&tTrueWid, sizeof(float), 1);
        rFile->Read(&tTrueHei, sizeof(float), 1);
        rFile->Read(&tTexLft, sizeof(float), 1);
        rFile->Read(&tTexTop, sizeof(float), 1);
        rFile->Read(&tTexRgt, sizeof(float), 1);
        rFile->Read(&tTexBot, sizeof(float), 1);

        //--Kerning Data.
        for(int p = 0; p < SUGARFONT_PRECACHE_MAX; p ++)
        {
            rFile->Read(&mPrecacheData[i].mKerningDistance[p], sizeof(float), 1);
        }

        //--Middle-positions. Used for X/Y mirroring.
        float cMidX = (tLft + tRgt) * 0.50f;
        float cMidY = (tTop + tBot) * 0.50f;
        float cTexMidX = (tTexLft + tTexRgt) * 0.50f;
        float cTexMidY = (tTexTop + tTexBot) * 0.50f;

        //--Primary Precache.
        mPrecacheData[i].mGLList = glGenLists(1);
        glNewList(mPrecacheData[i].mGLList, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
            glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft, tBot);
            glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt, tBot);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);
        glEnd();
        glEndList();

        //--Italics.
        float tQuarterWid = ((float)mPrecacheData[i].mWidth * 0.35f) * ((float)mPrecacheData[i].mHeight / (float)mTallestCharacter);
        mPrecacheData[i].mGLListItalic = glGenLists(1);
        glNewList(mPrecacheData[i].mGLListItalic, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft + tQuarterWid, tTop);
            glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft +        0.0f, tBot);
            glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt +        0.0f, tBot);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt + tQuarterWid, tTop);
        glEnd();
        glEndList();

        //--Precache the X-Mirror rendering.
        mPrecacheData[i].mGLListXMirror = glGenLists(1);
        glNewList(mPrecacheData[i].mGLListXMirror, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft,  tTexTop); glVertex2f(tLft,  tTop);
            glTexCoord2d(tTexLft,  tTexBot); glVertex2f(tLft,  tBot);
            glTexCoord2d(cTexMidX, tTexBot); glVertex2f(cMidX, tBot);
            glTexCoord2d(cTexMidX, tTexTop); glVertex2f(cMidX, tTop);

            glTexCoord2d(cTexMidX, tTexTop); glVertex2f(cMidX, tTop);
            glTexCoord2d(cTexMidX, tTexBot); glVertex2f(cMidX, tBot);
            glTexCoord2d(tTexLft,  tTexBot); glVertex2f(tRgt,  tBot);
            glTexCoord2d(tTexLft,  tTexTop); glVertex2f(tRgt,  tTop);
        glEnd();
        glEndList();

        //--Precache the Y-Mirror rendering.
        mPrecacheData[i].mGLListYMirror = glGenLists(1);
        glNewList(mPrecacheData[i].mGLListYMirror, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
            glTexCoord2d(tTexLft, cTexMidY); glVertex2f(tLft, cMidY);
            glTexCoord2d(tTexRgt, cTexMidY); glVertex2f(tRgt, cMidY);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);

            glTexCoord2d(tTexLft, cTexMidY); glVertex2f(tLft, cMidY);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tBot);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tBot);
            glTexCoord2d(tTexRgt, cTexMidY); glVertex2f(tRgt, cMidY);
        glEnd();
        glEndList();
    }

    ///--[Clean Up]
    free(tTextureData);
    return true;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
