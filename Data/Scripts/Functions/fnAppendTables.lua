-- |[ ==================================== fnAppendTables() ==================================== ]|
--Function that appends two arrays (or tables) together.
-- Table2 is appended to the end of Table1. Table2 is unchanged.
function fnAppendTables(pzaTable1, pzaTable2)
    
    -- |[Argument Check]|
    if(pzaTable1 == nil) then return end
    if(pzaTable2 == nil) then return end
    
    -- |[Append]|
    --Iterate across pzaTable2, adding all elements to pzaTable1.
    for i = 1, #pzaTable2, 1 do
		table.insert(pzaTable1, pzaTable2[i])
    end

    -- |[Finish Up]|
	return pzaTable1
end