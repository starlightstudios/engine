//--Base
#include "StarTranslation.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"

///========================================== System ==============================================
StarTranslation::StarTranslation()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ==== StarTranslation ===== ]
    mLastReturnedEntry = 0;
    mTotalEntries = 0;
    mEntries = NULL;

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
StarTranslation::~StarTranslation()
{
    for(int i = 0; i < mTotalEntries; i ++)
    {
        mEntries[i].Deallocate();
    }
    free(mEntries);
}
void StarTranslation::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    StarTranslation *rPtr = (StarTranslation *)pPtr;
    delete rPtr;
}

///===================================== Property Queries =========================================
int StarTranslation::BinaryGetLookup(const char *pString, int pMin, int pMax)
{
    ///--[Documentation]
    //--Does a recursive binary search on the entry list, looking for an entry whose base string
    //  matches the provided string.
    //--Returns -1 on failure/error.

    //--Array is empty
    if(pMax < pMin)
    {
        return -1;
    }

    //--Find the halfway point, cut the set in half
    int tMid = (pMin + pMax) / 2;

    //--Exceeds final position. Error, not found.
    if(tMid >= pMax || tMid < 0) return -1;

    //--Check the given position.
    const char *rCheckName = mEntries[tMid].mBaseString;
    int tRet = strcmp(pString, rCheckName);

    //--After Midpoint
    if(tRet > 0)
    {
        return BinaryGetLookup(pString, tMid+1, pMax);
    }
    //--Before midpoint
    else if(tRet < 0)
    {
        return BinaryGetLookup(pString, pMin, tMid);
    }
    //--Match
    else
    {
        return tMid;
    }

    //--Failure.
    return -1;
}
const char *StarTranslation::GetTranslationFor(const char *pString)
{
    ///--[Documentation]
    //--Given a string, searches this translation set for the string and returns the translation,
    //  if it exists. If not, returns NULL.
    //--This is a binary search implementation.
    mLastReturnedEntry = -1;
    if(!pString) return NULL;

    //--Get matching index. On -1, return NULL.
    int tTranslationIndex = BinaryGetLookup(pString, 0, mTotalEntries);
    if(tTranslationIndex == -1) return NULL;

    //--Return the translated string.
    mLastReturnedEntry = tTranslationIndex;
    return mEntries[tTranslationIndex].mTranslation;
}
const char *StarTranslation::GetLastPrefix()
{
    ///--[Documentation]
    //--Returns the prefix for the last string found via GetTranslationFor().
    if(mLastReturnedEntry < 0 || mLastReturnedEntry >= mTotalEntries) return NULL;
    return mEntries[mLastReturnedEntry].mPrefix;
}

///======================================= Manipulators ===========================================
void StarTranslation::Translate(char *&sString)
{
    ///--[Documentation]
    //--Given a string, attempts to translate it and replaces the string with the translated copy.
    //  If no translation is found, does nothing.
    if(!sString) return;

    //--Check if a new string is available.
    const char *rTranslation = GetTranslationFor(sString);
    if(!rTranslation)
    {
        return;
    }

    //--Reset the old string to be a copy of the new one.
    ResetString(sString, rTranslation);
}

///======================================= Core Methods ===========================================
void StarTranslation::FactoryZero()
{
    ///--[Documentation]
    //--Resets the class back to its initial state.

    //--Deallocate.
    for(int i = 0; i < mTotalEntries; i ++)
    {
        mEntries[i].Deallocate();
    }
    free(mEntries);

    //--Zero.
    mTotalEntries = 0;
    mEntries = NULL;
}
void StarTranslation::ParseFile(const char *pPath)
{
    ///--[Documentation]
    //--Given a file expected to be a csv file, opens the file and builds a translation object based
    //  on what is within. The format is that the leftmost column is the original, the second from
    //  the left is a machine translation, and the third from the left is an "updated" translation
    //  possibly done by hand. Translations are technically optional. The last column is a Prefix
    //  that gets placed before the original text in order to check if it's identical, and will also
    //  get placed before the translated text untranslated. This allows "System" properties to be
    //  placed on a line without causing issues.
    if(!pPath) return;

    //--Reset class to its initial state, just in case.
    FactoryZero();

    ///--[File Handling]
    //--Open and check.
    FILE *fInfile = fopen(pPath, "r");
    if(!fInfile) return;

    ///--[Iteration]
    //--Setup.
    char tBuffer[STARTRANS_BUFSIZE];
    StarLinkedList *tEntryList = new StarLinkedList(true);

    //--Get the first line and ignore it. It's a header.
    fgets(tBuffer, STARTRANS_BUFSIZE-1, fInfile);

    //--Main loop.
    int tCurrentLine = 0;
    while(!feof(fInfile))
    {
        //--Double check that there's a string available.
        void *rCheckPtr = fgets(tBuffer, STARTRANS_BUFSIZE-1, fInfile);
        if(!rCheckPtr) break;

        //--Warning: If we hit 10 bytes under the buffer size, WARN THE USER. We can never quite be sure just how long
        //  a string in utf-8 is going to be, particularly if some new standard comes along later. For speed we don't use
        //  dynamic buffer sizing but this may change in the future.
        int tLenOfLine = (int)strlen(tBuffer);
        if(tLenOfLine >= STARTRANS_BUFSIZE - 10)
        {
            fprintf(stderr, "Warning: Exceeded safe limits on buffer size while parsing a translation line.");
            fprintf(stderr, "This error means the buffer size needs to be larger for your translation to work. The program will now stop translating.\n");
            return;
        }

        //--If the line starts with FILEINFO then we don't care about it. This line is to help translators.
        if(!strncasecmp(tBuffer, "FILEINFO", 8))
        {
            tCurrentLine ++;
            continue;
        }

        //--Create a new entry.
        StarTranslateEntry *nEntry = (StarTranslateEntry *)starmemoryalloc(sizeof(StarTranslateEntry));
        nEntry->Initialize();

        //--Get the opening string. If it's a zero-length string, stop here.
        int tPosition = 0;
        nEntry->mBaseString = ParseLine(tBuffer, tPosition);
        if(nEntry->mBaseString[0] == '\0')
        {
            free(nEntry->mBaseString);
            free(nEntry);
            tCurrentLine ++;
            continue;
        }

        //--Put strings into the structure.
        nEntry->mTranslation = ParseLine(tBuffer, tPosition);

        //--If the next string comes back as a one-letter string, then there is no improved translation.
        //  If it doesn't come back that way, replace the machine translation.
        char *tTempString = ParseLine(tBuffer, tPosition);
        if(tTempString && tTempString[0] != '\0')
        {
            free(nEntry->mTranslation);
            nEntry->mTranslation = tTempString;
        }
        else
        {
            free(tTempString);
        }

        //--Lastly is the prefix.
        nEntry->mPrefix = ParseLine(tBuffer, tPosition);

        //--Debug report.
        //if(!strcasecmp(pPath, "Data/Translations/JP/Achievements.csv") && tCurrentLine >= 2 && tCurrentLine <= 10)
        //fprintf(stderr, "%s | %s | %s\n", nEntry->mBaseString, nEntry->mTranslation, nEntry->mPrefix);

        //--Register the entry.
        tEntryList->AddElementAsTail("X", nEntry, &StarTranslateEntry::DeleteThis);

        //--Next.
        tCurrentLine ++;
    }

    ///--[List Sorting]
    //--If the list contains no entries, stop here.
    if(tEntryList->GetListSize() < 1)
    {
        delete tEntryList;
        return;
    }

    //--Sort the list entries alphabetically by original string.
    tEntryList->SortListUsing(&StarTranslation::CompareTranslateEntries);

    //--Now create a statically sized list and copy all the linked list entries over.
    mTotalEntries = tEntryList->GetListSize();
    mEntries = (StarTranslateEntry *)starmemoryalloc(sizeof(StarTranslateEntry) * mTotalEntries);

    //--Liberate the strings.
    int i = 0;
    StarTranslateEntry *rCopyPtr = (StarTranslateEntry *)tEntryList->PushIterator();
    while(rCopyPtr)
    {
        //--Take ownership of the strings.
        mEntries[i].mBaseString = rCopyPtr->mBaseString;
        mEntries[i].mTranslation = rCopyPtr->mTranslation;
        mEntries[i].mPrefix = rCopyPtr->mPrefix;

        //--Mark them as NULL so they don't get deallocated later.
        rCopyPtr->mBaseString = NULL;
        rCopyPtr->mTranslation = NULL;
        rCopyPtr->mPrefix = NULL;

        //--Next.
        i ++;
        rCopyPtr = (StarTranslateEntry *)tEntryList->AutoIterate();
    }

    ///--[Finish Up]
    fclose(fInfile);
    delete tEntryList;

    ///--[Debug]
    /*
    for(int i = 0; i < mTotalEntries; i ++)
    {
        fprintf(stderr, "%s %s\n", mEntries[i].mBaseString, mEntries[i].mTranslation);
    }*/
}
char *StarTranslation::ParseLine(const char *pString, int &sPosition)
{
    ///--[Documentation]
    //--Given a string, parses out the next component of the string up to the next comma, and returns
    //  that as a new heap allocated string. If there is nothing left to parse, returns NULL. If there
    //  is an empty string, returns a one-byte string with a null terminator.
    //--Advances the sPosition variable ahead of the last comma read.
    if(!pString) return NULL;

    //--Make sure the position isn't past the end of the buffer space.
    if(sPosition >= STARTRANS_BUFSIZE) return NULL;

    //--Make sure the position isn't past the end of the string.
    int tMaxLen = strlen(pString);
    if(sPosition >= tMaxLen) return NULL;

    //--Variables.
    bool tIsQuotedString = false;
    int tFinalStringPosition = 0;
    const int cBufSize = STARTRANS_BUFSIZE - 1;
    char tFinalString[cBufSize];

    //--Check if this letter is a comma. If so, return a one-byte null string.
    if(pString[sPosition] == ',')
    {
        sPosition ++;
        char *nNewString = (char *)starmemoryalloc(sizeof(char) * 1);
        nNewString[0] = '\0';
        return nNewString;
    }

    //--Check if this is a quote. If so, the quote means any commas found until the ending quote don't
    //  count as terminators. Double-quotes are part of a string.
    if(pString[sPosition] == '"' && pString[sPosition + 1] != '"')
    {
        sPosition ++;
        tIsQuotedString = true;
    }

    ///--[Parsing]
    for(int i = sPosition; i < tMaxLen; i ++)
    {
        //--In quote mode, check for a single quote. This disables quote mode, and ends the string.
        if(tIsQuotedString && pString[i] == '"' && pString[i+1] != '"')
        {
            sPosition = i + 2;
            break;
        }

        //--When not in quote mode, the next comma always ends the string. In addition, newlines also end the string.
        if(!tIsQuotedString && (pString[i] == ',' || pString[i] == 10 || pString[i] == 13))
        {
            sPosition = i + 1;
            break;
        }

        //--If this is a quote, check if it's a double quote. Those are appended to the end string once.
        if(pString[i] == '"' && pString[i+1] == '"')
        {
            tFinalString[tFinalStringPosition] = '"';
            tFinalStringPosition ++;
            i ++;
            continue;
        }

        //--In all other cases, append the letter.
        tFinalString[tFinalStringPosition] = pString[i];
        tFinalStringPosition ++;
    }

    ///--[String Handling]
    //--We now have a stack buffer with the string in it, and sPosition should be set ahead of the last
    //  comma. Make a heap version of the string and return it.
    //--In case we hit the end of the string via hitting the end of the string with no newlines, append
    //  a null character.
    tFinalString[tFinalStringPosition] = '\0';
    char *nReturnString = InitializeString(tFinalString);
    return nReturnString;
}

///=================================== Private Core Methods =======================================
int StarTranslation::CompareTranslateEntries(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() to get StarTranslateEntry's into alphabetical order.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Cast to StarTranslateEntry.
    StarTranslateEntry *rTranslateA = (StarTranslateEntry *)(*rEntryA)->rData;
    StarTranslateEntry *rTranslateB = (StarTranslateEntry *)(*rEntryB)->rData;

    //--Compare.
    return strcmp(rTranslateA->mBaseString, rTranslateB->mBaseString);
}

///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
void StarTranslation::StringTranslateWorker(char *&sString, StarTranslation *pTranslation)
{
    ///--[Documentation]
    //--Given a string which is to be translated, and a StarTranslation object, this function will check
    //  if a translation exists for that string and reallocate the string in question. If no translation
    //  exists for that string, does nothing.
    if(!sString || !pTranslation) return;

    //--Check if there is a translation for this string.
    const char *rTranslatedString = pTranslation->GetTranslationFor(sString);
    if(!rTranslatedString) return;

    //--Translation exists, replace original string with a heap copy of the translated one.
    ResetString(sString, rTranslatedString);
}

///========================================= Lua Hooking ==========================================
void StarTranslation::HookToLuaState(lua_State *pLuaState)
{
    /* STrans_CreateTranslation(sLanguage, sType, sFilePath)
       Creates a translation for the given language of the given type, using the provided csv path.
       Languages are a string, with "Japanese" being supported. Type is one of "Dialogue" and "UI". */
    lua_register(pLuaState, "STrans_CreateTranslation", &Hook_STrans_CreateTranslation);

    /* STrans_ActivateTranslation(sLanguage, sType)
       Activates the translation in the given DataLibrary path. */
    lua_register(pLuaState, "STrans_ActivateTranslation", &Hook_STrans_ActivateTranslation);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_STrans_CreateTranslation(lua_State *L)
{
    //STrans_CreateTranslation(sLanguage, sType, sFilePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("STrans_CreateTranslation");

    //--Create a DataLibrary path for the language if it doesn't already exist.
    char tBuffer[256];
    sprintf(tBuffer, "%s%s/", TRANSPATH_GENERAL, lua_tostring(L, 1));
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->AddPath(tBuffer);

    //--Run the csv file to create the translation.
    StarTranslation *nTranslation = new StarTranslation();
    nTranslation->ParseFile(lua_tostring(L, 3));

    //--Save it into the data library by its type.
    sprintf(tBuffer, "%s%s/%s", TRANSPATH_GENERAL, lua_tostring(L, 1), lua_tostring(L, 2));
    rDataLibrary->RegisterPointer(tBuffer, nTranslation, &StarTranslation::DeleteThis);

    return 0;
}
int Hook_STrans_ActivateTranslation(lua_State *L)
{
    //STrans_ActivateTranslation(sLanguage, sType)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("STrans_CreateTranslation");

    //--Path buffer.
    char tInBuffer[256];
    char tOutBuffer[256];
    sprintf(tInBuffer,  "%s%s/%s", TRANSPATH_GENERAL, lua_tostring(L, 1), lua_tostring(L, 2));
    sprintf(tOutBuffer, "%s%s",    TRANSPATH_ACTIVE, lua_tostring(L, 2));

    //--Check if the given language/type translation exists.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->DoesEntryExist(tInBuffer))
    {
        DebugManager::ForcePrint("Error: STrans_ActivateTranslation(). No translation exists in %s.\n", tInBuffer);
        return 0;
    }

    //--Copy it over. This is a reference copy.
    rDataLibrary->RemovePointer(tOutBuffer);
    void *rPtr = rDataLibrary->GetEntry(tInBuffer);
    rDataLibrary->AddPath(tOutBuffer);
    rDataLibrary->RegisterPointer(tOutBuffer, rPtr, &DontDeleteThis);

    //--Finish.
    return 0;
}
