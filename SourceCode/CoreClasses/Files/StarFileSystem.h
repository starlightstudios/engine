///====================================== StarFileSystem =========================================
//--A filesystem that runs independent of OS, and also of ridiculous filesystem idiosyncracies like
//  Boost's. Neatly abstracts all that crap away from you, so you don't need to worry about it.
//--The exact internal behavior is determined by a definition, which will be either _BOOST_FILESYSTEM_
//  or _ALLEGRO_FILESYSTEM_ depending on which library you're using.
//--The StarFileSystem uses no static variables and can recursively instantiate. Alternately, you
//  can order it to recursively get directories in the iterator, and the returned pathnames will
//  have the directories appended.
//--When activated from Lua, the filesystem uses a stack (to allow recursion). The head of the stack
//  is whatever filesystem got opened most recently. When closed, the head is what is closed. The head
//  is the 0th element on the global filesystem stack. Be sure to close them in the order you open them.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[FileInfo]
//--Represents a file on the hard drive, which can also be a directory.
typedef struct FileInfo
{
    //--Members
    char *mPath;
    bool mIsDirectory;
    bool mIsFile;
    time_t mLastAccessed;

    //--Functions
    void Initialize()
    {
        mPath = NULL;
        mIsDirectory = false;
        mIsFile = false;
        mLastAccessed = 0;
    }
}FileInfo;

///========================================== Classes =============================================
class StarFileSystem
{
    private:
    //--System
    int mTotalFiles;
    FileInfo **mDirectoryInfo;
    bool mIsThisARecursion;

    //--Storage
    char *mBasePath;
    char *mLastParsedDirectory;

    //--Iteration
    int mIterationPoint;

    protected:

    public:
    //--System
    StarFileSystem();
    ~StarFileSystem();
    static void DeleteThis(void *pPtr);
    static void DeleteFileInfo(void *pPtr);

    //--Public Variables
    bool mIgnoreFolders;
    bool mIgnoreZerothEntry;
    bool mIsRecursive;

    //--Property Queries
    int GetTotalEntries();
    FileInfo *GetEntry(int pSlot);
    char *GetLastParsedDirectory();
    char *GetIteratedPath();

    //--Manipulators
    void SetRecursiveFlag(bool pFlag);
    void ScanDirectory(const char *pPath);

    //--Core Methods
    void Clear();
    void PrintContents();
    StarLinkedList *SubScanDirectory(const char *pPath, bool pScanDirectories);
    void MergeFileInfo(StarLinkedList *pFileInfoList);
    void SortDirectory();
    void SortDirectoryByLastAccess();
    static int SortFunctionAlphabetical(const void *pInfoA, const void *pInfoB);
    static int SortFunctionLastAccess(const void *pInfoA, const void *pInfoB);
    void ResetIterator();
    void Iterate();
    StarLinkedList *GetFilesWithExtension(const char *pExtension);

    //--Special Operations
    static void AutoFolderMaker();
    static void ModifySlashes(char *pOperation);
    static void CreateDirectoriesFor(const char *pPath);
    static void MakeFileBackup(const char *pPath);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static bool FileExists(const char *pPattern, ...);
    static char *PareFileName(const char *pPath);
    static char *PareFileName(const char *pPath, bool pKeepExtension);
    static StarFileSystem *Fetch();
    static bool IsFileExtension(const char *pPath, const char *pExtension);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_FS_Exists(lua_State *L);
int Hook_FS_Open(lua_State *L);
int Hook_FS_Reset(lua_State *L);
int Hook_FS_Iterate(lua_State *L);
int Hook_FS_Close(lua_State *L);
