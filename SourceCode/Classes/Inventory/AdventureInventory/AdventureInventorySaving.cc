//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"
#include "AdventureMenu.h"
#include "VirtualFile.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "LuaManager.h"

void AdventureInventory::WriteToBuffer(StarAutoBuffer *pBuffer)
{
    //--[Documentation and Setup]
    //--Writes the contents of the inventory to the provided buffer. Used by the saving process.
    if(!pBuffer) return;

    //--[System and Header Info]
    //--Write the current unique ID counter for items.
    pBuffer->AppendUInt32(AdventureItem::xItemUniqueIDCounter);

    //--Write how many platinas the player has.
    pBuffer->AppendInt32(mPlatina);

    //--Write the crafting item counts.
    pBuffer->AppendInt16(mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER]);
    pBuffer->AppendInt16(mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES]);
    pBuffer->AppendInt16(mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD]);
    pBuffer->AppendInt16(mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE]);
    pBuffer->AppendInt16(mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK]);
    pBuffer->AppendInt16(mCraftingItemCounts[CRAFT_ADAMANTITE_ORE]);

    //--[Base Inventory]
    //--Write the local item information. Items equipped on characters are handled elsewhere.
    int tTotalItemCount = mItemList->GetListSize();

    //--Iterate across the items. Items that can stack will add extra entries to the end.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        if(rItem->IsStackable())
        {
            tTotalItemCount = tTotalItemCount + (rItem->GetStackSize() - 1);
        }
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }

    //--Count gems equipped onto party members as items.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int cRosterSize = rAdventureCombat->GetRosterCount();
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Get and check party member.
        AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberI(i);
        if(!rEntity) continue;

        //--Write
        //TODO FOR BBP
    }

    //--[Socketed Gems]
    //--Gems currently socketed into equipment are also written here. Only the active party can have
    //  items socketed.
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Get and check party member.
        AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberI(i);
        if(!rEntity) continue;

        //--Write
        //TODO FOR BBP
    }

    //--[Gem Upgrades]
    //--Gem upgrades always get written first. This is because they need to be resolved before anything
    //  gets socketed.
    StarLinkedList *tUpgradeList = new StarLinkedList(true);

    //--Rebuild the list of all gems in the inventory.
    BuildGemList();

    //--[Equipment Socketing]
    //--Now that the gem upgrades have been written, write which gems are socketed into which equipment.
    //  For this, the mItemUniqueID is the weapon/armor's ID, and the mItemState is the ID of the gem
    //  socketed into it. They are written in order, 0 is not a valid Item ID or Gem ID. If items are
    //  socketed "out of order" then the gem sockets will compress. Tough beans.
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Get and check party member.
        AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberI(i);
        if(!rEntity) continue;

        //--Write
        //TODO FOR BBP
    }

    //--[Clean]
    delete tUpgradeList;
}
void AdventureInventory::ReadFromFile(VirtualFile *fInfile)
{
    //--Given a VirtualFile in position, reads inventory data out. This object should be cleared
    //  will implicitly clear itself when doing so.
    //--This needs to be called *after* the combat data since the entities will create their items
    //  as necessary, and that edits the item unique ID counter.
    mItemList->ClearList();

    //--Store the unique counter for items. We'll update this later.
    uint32_t tStoredItemIDCounter = 0;
    fInfile->Read(&tStoredItemIDCounter, sizeof(uint32_t), 1);

    //--Read platina counts.
    fInfile->Read(&mPlatina, sizeof(int32_t), 1);

    //--Read platine crafting counts.
    memset(mCraftingItemCounts, 0, sizeof(int) * CRAFT_ADAMANTITE_TOTAL);
    fInfile->Read(&mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER], sizeof(int16_t), 1);
    fInfile->Read(&mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES], sizeof(int16_t), 1);
    fInfile->Read(&mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD], sizeof(int16_t), 1);
    fInfile->Read(&mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE], sizeof(int16_t), 1);
    fInfile->Read(&mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK], sizeof(int16_t), 1);
    fInfile->Read(&mCraftingItemCounts[CRAFT_ADAMANTITE_ORE], sizeof(int16_t), 1);

    //--Get how many items to read.
    int32_t tItemCount = 0;
    fInfile->Read(&tItemCount, sizeof(int32_t), 1);

    //--Read and create the items.
    for(int i = 0; i < tItemCount; i ++)
    {
        //--Clear this flag.
        mWasAnythingCreated = false;

        //--Read the name.
        char *tName = fInfile->ReadLenString();
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", tName);

        //--If this flag was set to true, then the item was created and is on the tail of the item list.
        if(mWasAnythingCreated && mItemList->GetListSize() > 0)
        {
            //--Read the ID and charges.
            uint32_t tNewID = 0;
            int8_t tCharges = 0;
            fInfile->Read(&tNewID, sizeof(uint32_t), 1);
            fInfile->Read(&tCharges, sizeof(int8_t), 1);

            //--Get the tail item.
            AdventureItem *rNewestItem = (AdventureItem *)mItemList->GetTail();
            rNewestItem->OverrideID(tNewID);
        }
        //--Error: Skip the rest of this item's data.
        else
        {
            fInfile->SeekRelative(sizeof(uint32_t) + sizeof(int8_t));
        }

        //--Clean.
        free(tName);
    }

    //--Read and apply the upgrade information.
    int32_t tUpgradesTotal = 0;
    fInfile->Read(&tUpgradesTotal, sizeof(int32_t), 1);
    for(int i = 0; i < tUpgradesTotal; i ++)
    {
        //--Read the item ID and the upgrade ID.
        uint32_t tItemID = 0;
        uint32_t tUpgradeID = 0;
        fInfile->Read(&tItemID, sizeof(uint32_t), 1);
        fInfile->Read(&tUpgradeID, sizeof(uint32_t), 1);

        //--Locate the item in question.
        AdventureItem *rItemToUpgrade = LocateItemByItemID(tItemID);
        if(!rItemToUpgrade) continue;

        //--If this item is upgradeable, it's a gem. We need to apply its upgrade.
        //if(rItemToUpgrade->IsUpgradeable())
        {
        }
        //--This item is not upgradeable, but it's socketable. That means it needs to be socketed.
        if(rItemToUpgrade->GetGemSlots() > 0)
        {
            //--Locate the gem we need to socket the item with.
            AdventureItem *rGem = LocateItemByItemID(tUpgradeID);
            if(!rGem) continue;

            //--Now socket it in the first available slot.
            for(int p = 0; p < ADITEM_MAX_GEMS; p ++)
            {
                if(!rItemToUpgrade->GetGemInSlot(p))
                {
                    rItemToUpgrade->PlaceGemInSlot(p, rGem);
                    LiberateItemP(rGem);
                    break;
                }
            }
        }
        //--Item is neither socketable nor upgradeable. This is in error. Ignore it.
        else
        {
        }
    }

    //--Retroactively modify the static ID counter.
    AdventureItem::xItemUniqueIDCounter = tStoredItemIDCounter;

    //--Now sort the items into stacks.
    StackItems();
}
