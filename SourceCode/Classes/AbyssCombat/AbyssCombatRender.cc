//--Base
#include "AbyssCombat.h"

//--Classes
#include "AbyCombatPrediction.h"
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdventureMenu.h"
#include "AdventureLevel.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarlightString.h"
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "CutsceneManager.h"
#include "DebugManager.h"

///--[Local Definitions]
#define ADVCOMBAT_INTRO_TICKS 15

///--[Debug]
//#define ABYSSCOMBAT_RENDER_DEBUG
#ifdef ABYSSCOMBAT_RENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Drawing =============================================
void AbyssCombat::Render(bool pBypassResolution, float pAlpha)
{
    ///--[ ====== Documentation and Setup ===== ]
    //--Renders the Adventure Combat interface. This uses an alpha mixer, but this is only used
    //  during the resolution sequences.
    if(!Images.mIsReady || !AbyssImg.mIsReady || !mIsActive || pAlpha <= 0.0f)
    {
        //fprintf(stderr, "Failed render: %i %i %i %f\n", Images.mIsReady, AbyssImg.mIsReady, mIsActive, pAlpha);
        return;
    }
    DebugPush(true, "Abyss Combat Render: Begin.\n");

    //--If the world is still pulsing, don't render.
    DebugPrint("Checking Adventure Level.\n");
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsPulsing()) { DebugPop("Level pulsing, exited normally.\n"); return; }

    //--Fast-access pointers.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();

    ///--[List Building]
    //--Rebuild effect references. It is possible an effect was added/removed since the last time the update ticked.
    DebugPrint("Rebuilding effect references.\n");
    RebuildEffectReferences();

    ///--[Subhandlers]
    //--Bypass resolution for the first phase of a defeat.
    if(mCombatResolution != ADVCOMBAT_END_NONE && (mCombatResolution == ADVCOMBAT_END_DEFEAT || mCombatResolution == ADVCOMBAT_END_SURRENDER) && mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_FADE_OUT_UI)
    {
        pBypassResolution = true;
    }

    //--Subhandlers can take over rendering. First, combat is over:
    DebugPrint("Checking resolution/introduction states.\n");
    if(mCombatResolution != ADVCOMBAT_END_NONE && !pBypassResolution) { RenderResolution();   DebugPop("Resolution running, exited normally.\n");   return; }
    if(mIsIntroduction)
    {
        StarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);
        DebugPop("Introduction running, exited normally.\n");
        return;
    }

    //--Anything below this point is "Normal" combat rendering.

    ///--[Grey Backing]
    //--Flat grey backing. Not affected by alpha mixing. Does not render during the victory pass.
    DebugPrint("Backing.\n");
    if(!pBypassResolution) StarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    ///--[Transparency]
    //--Multiply by the battle start transparency.
    float cBattleTransparency = EasingFunction::QuadraticInOut(mStartBattleFadeTimer, ABYCOM_BATTLE_START_FADE_TICKS);
    pAlpha = cBattleTransparency;

    ///--[ ========= Entity Rendering ========= ]
    ///--[Enemy Party]
    //--Render all enemies on the field.
    int tEnemiesTotal = mrEnemyCombatParty->GetListSize();
    for(int i = 0; i < tEnemiesTotal; i ++)
    {
        RenderEnemy(i, pAlpha);
    }

    ///--[Player's Party]
    //--If an entity is not present, it will do nothing.
    for(int i = 0; i < 3; i ++)
    {
        //--Skip rendering members who are disabled.
        RenderPartyMember(i, pAlpha);
    }

    ///--[Player UI]
    //--Renders once all entities have rendered to prevent overlaps.
    if(!mHideCombatUI)
    {
        for(int i = 0; i < 3; i ++)
        {
            if(!mIsMemberDisabled[i]) RenderPartyMemberUI(i, pAlpha);
        }
    }

    //--Callouts. Renders over everything.
    if(!rWorldDialogue->IsVisible()) RenderCallouts();

    ///--[Enemy UI]
    //--Only renders if target mode is active and the player is targeting this enemy.
    DebugPrint("Rendering enemy UI.\n");
    if(!mHideCombatUI && mIsSelectingTargets)
    {
        //--Normal: Render all UIs at full opacity.
        if(mPredictionBoxes->GetListSize() <= 3)
        {
            AdvCombatEntity *rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
            while(rEnemyEntity)
            {
                if(IsEntityInActiveTargetCluster(rEnemyEntity))
                {
                    RenderEntityUI(rEnemyEntity, pAlpha);
                }
                rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
            }
        }
        //--Render the highlighted UI last, and decrease opacity for non-highlighted UIs.
        else
        {
            //--Setup.
            AdvCombatEntity *rHighlightedEntity = NULL;

            //--Render normally.
            AdvCombatEntity *rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
            while(rEnemyEntity)
            {
                if(IsEntityInActiveTargetCluster(rEnemyEntity))
                {
                    TargetCluster *rActiveCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);

                    if(rLastHighlightedPtr != rEnemyEntity && (rActiveCluster && rActiveCluster->mrTargetList->GetListSize() >= 4))
                    {
                        RenderEntityUI(rEnemyEntity, pAlpha * 0.33f);
                    }
                    else
                    {
                        rHighlightedEntity = rEnemyEntity;
                    }
                }
                rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
            }

            //--If a highlight is found, render it last.
            if(rHighlightedEntity)
            {
                RenderEntityUI(rHighlightedEntity, pAlpha);
            }
        }
    }

    ///--[ ============ Animations ============ ]
    //--Render animations.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    DebugPrint("Rendering combat animations.\n");
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->PushIterator();
    while(rAnimation)
    {
        rAnimation->Render();
        rAnimation = (AdvCombatAnimation *)mCombatAnimations->AutoIterate();
    }

    ///--[ =========== Text Scrolls =========== ]
    //--Text package.
    DebugPrint("Rendering text packages.\n");
    float cCombatYDrift = -30.0f;
    CombatTextPack *rTextPack = (CombatTextPack *)mTextPackages->PushIterator();
    while(rTextPack)
    {
        //--Compute Y position.
        float cPercent = EasingFunction::QuadraticOut(rTextPack->mTicks, rTextPack->mTicksMax);
        float cYUsePos = rTextPack->mY + (cCombatYDrift * cPercent);

        //--Render.
        rTextPack->mColor.SetAsMixer();
        AbyssImg.Data.rFontCombatText->DrawText(rTextPack->mX, cYUsePos, SUGARFONT_AUTOCENTER_XY, rTextPack->mScale * mCombatTextScale, rTextPack->mText);

        //--Next.
        rTextPack = (CombatTextPack *)mTextPackages->AutoIterate();
    }

    ///--[ =============== Other ============== ]
    //--Prediction boxes.
    DebugPrint("Rendering prediction boxes.\n");
    if(!mHideCombatUI) RenderPredictionBoxes();

    //--Titles. Renders 70px higher than the base game does.
    DebugPrint("Rendering titles.\n");
    if(!mHideCombatUI)
    {
        RenderTitle();
    }

    ///--[Help Strings]
    //--UI is hidden, only thing visible is the prompt to show it.
    if(mHideCombatUI)
    {
        mShowUIString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - 22.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, AbyssImg.Data.rFontControl);
    }
    else
    {
        //--UI String.
        mHideUIString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - 22.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, AbyssImg.Data.rFontControl);

        //--Toggle Descriptions.
        mToggleDescriptionsString->DrawText(VIRTUAL_CANVAS_X - 150.0f, VIRTUAL_CANVAS_Y - 22.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, AbyssImg.Data.rFontControl);
    }

    ///--[ ============= Fixed UI ============= ]
    //--Top left/right block.
    if(!mHideCombatUI)
    {
        //--Turn order.
        RenderTurnOrder(pAlpha);

        //--Prediction string.
        if(mPredictionBoxes->GetListSize() >= 4)
        {
            mCyclePredictionString->DrawText(110.0f, 2.0f, 0, 1.0f, AbyssImg.Data.rFontPlayerUI);
        }

        //--Emergency escape string.
        if(!mDisallowEmergencyEscape) mEmergencyEscapeString->DrawText(VIRTUAL_CANVAS_X - 10.0f, VIRTUAL_CANVAS_Y - 44.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, AbyssImg.Data.rFontPlayerUI);
    }

    ///--[Command UI]
    //--Only appears if the party member in question is the acting entity. Handled by a subroutine.
    if(!mHideCombatUI)
    {
        AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetHead();
        for(int i = 0; i < 3; i ++)
        {
            //--Skip disabled.
            if(mIsMemberDisabled[i]) continue;

            //--Get entity pointer.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(i);
            if(!rEntity || rEntity != rActingEntity) continue;

            //--Offset data.
            //float cXOffset = 0.0f;
            //float cYOffset = 0.0f;
            //if(i == 1) cXOffset = 480.0f;
            //if(i == 2) cXOffset = 990.0f;

            //--Render.
            //glTranslatef(cXOffset, cYOffset, 0.0f);
            RenderCommandWindow(rEntity, pAlpha);
            //glTranslatef(-cXOffset, -cYOffset, 0.0f);
        }
    }

    ///--[Overlays]
    //--Dialogue, if present.
    DebugPrint("Rendering world dialogue.\n");
    if(rWorldDialogue->IsVisible())
    {
        rWorldDialogue->Render();
    }

    //--Combat inspector.
    DebugPrint("Rendering inspector.\n");
    RenderInspector();

    //--Confirmation window overlay.
    DebugPrint("Rendering confirmation window.\n");
    RenderConfirmation();

    //--Ability descriptions.
    RenderAbilityDescriptions(pAlpha);

    ///--[Tutorials]
    //--Specially made screen overlays with text and highlights.
    if(mTutorialTimer > 0)
    {
        //--Crossfade percent.
        float cCrossfadeAlpha = EasingFunction::QuadraticInOut(mTutorialTimer, ABYCOM_TUTORIAL_FADE_TICKS);

        //--Fading in, no previous entry.
        if(!rTutorialOverlayPrv)
        {
            //--Error, nothing to render.
            if(!rTutorialOverlayCur)
            {
            }
            //--Render normally using fade alpha.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha * cCrossfadeAlpha);
                rTutorialOverlayCur->Draw();
                RenderStringPackList(mTutorialStringsCur);
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            }
        }
        //--Crossfading, has a previous entry.
        else if(rTutorialOverlayPrv)
        {
            //--No current entry, fading out.
            if(!rTutorialOverlayCur)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha * (1.0f - cCrossfadeAlpha));
                rTutorialOverlayPrv->Draw();
                RenderStringPackList(mTutorialStringsPrv);
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            }
            //--Crossfading between two entries.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha * (1.0f - cCrossfadeAlpha));
                rTutorialOverlayPrv->Draw();
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha * cCrossfadeAlpha);
                rTutorialOverlayCur->Draw();
                RenderStringPackList(mTutorialStringsCur);
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            }
        }
    }

    ///--[ ==== Combat Resolution Overfade ==== ]
    if(mCombatResolution != ADVCOMBAT_END_NONE && (mCombatResolution == ADVCOMBAT_END_DEFEAT || mCombatResolution == ADVCOMBAT_END_SURRENDER) && mDefeatCharacterPhase == ABYCOM_DEFEAT_PHASE_FADE_OUT_UI)
    {
        float tPercent = (float)mDefeatTimer / (float)ABYCOM_DEFEAT_FADEUI_TICKS;
        StarBitmap::DrawFullBlack(tPercent);
    }

    ///--[ ========= Emergency Escape ========= ]
    if(mIsEmergencyEscape)
    {
        //--Backing.
        StarBitmap::DrawFullBlack(0.75f);

        //--Position.
        float cXPosition = VIRTUAL_CANVAS_X * 0.50f;
        float cYPosition = VIRTUAL_CANVAS_Y * 0.30f;

        //--Render.
        AbyssImg.Data.rFontDescription->DrawText(cXPosition, cYPosition +   0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, AbyssString.str.mEmergencyEscapeA);
        AbyssImg.Data.rFontDescription->DrawText(cXPosition, cYPosition + 100.0f, SUGARFONT_AUTOCENTER_X, 1.0f, AbyssString.str.mEmergencyEscapeB);
    }

    ///--[ ============= Finish Up ============ ]
    DebugPop("Abyss Combat Render: Complete.\n");
}
void AbyssCombat::RenderStringPackList(StarLinkedList *pList)
{
    ///--[Documentation]
    //--Given a StarLinkedList containing TutorialStringPack's, renders them using the header font.
    if(!pList) return;
    TutorialStringPack *rStringPack = (TutorialStringPack *)pList->PushIterator();
    while(rStringPack)
    {
        AbyssImg.Data.rFontHeader->DrawText(rStringPack->mX, rStringPack->mY, SUGARFONT_NOCOLOR, 1.0f, rStringPack->mText);
        rStringPack = (TutorialStringPack *)pList->AutoIterate();
    }
}
void AbyssCombat::RenderPartyMember(int pSlot, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders a party member in the given slot.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(pSlot);
    if(!rEntity) return;

    //--Range check. Should already have failed with no resolved entity but always be safe.
    if(pSlot < 0 || pSlot >= ABYCOM_PLAYER_PARTY_MAX_SIZE) return;

    ///--[Compute Offsets]
    float cXOffset = 0.0f;
    float cYOffset = 0.0f;
    if(pSlot == 1) cXOffset = 334.0f;
    if(pSlot == 2) cXOffset = 668.0f;

    //--Y offset adjusts when the member is disabled.
    float tDisabledPct = EasingFunction::QuadraticInOut(mDisableTimers[pSlot], ABYCOM_DISABLE_FADE_TICKS);
    cYOffset = 300.0f * tDisabledPct;
    pAlpha = pAlpha * (1.0f - tDisabledPct);

    //--Shaking.
    cXOffset = cXOffset + rEntity->GetShakeX();

    ///--[Combat Portrait]
    //--Make sure there's a portrait to render.
    StarBitmap *rCombatPortrait = rEntity->GetCombatPortrait();
    if(!rCombatPortrait) return;

    //--Block rendering when KO'd. This only occurs while sliding off the screen for player party members.
    if(rEntity->IsBlockRenderForKO())
    {
        rEntity->mLastRenderX = ADVCE_NORENDER;
        rEntity->mLastRenderY = ADVCE_NORENDER;
        return;
    }

    ///--[Rendering Setup]
    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Adjust color and position if the command window is open.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(mShowCommandWindow && rActingEntity != rEntity)
    {
        //--Move downwards.
        float tPercentReveal = EasingFunction::QuadraticOut(mCommandTimer, ABYCOM_COMMAND_SCROLL_TICKS);
        //cYOffset = cYOffset + (tPercentReveal * 700.0f);

        //--Grey out with downward movement.
        float cMix = 1.0f - (0.70f * tPercentReveal);
        StarlightColor::SetMixer(cMix, cMix, cMix, pAlpha);

        //--At 100%, disable rendering.
        //if(tPercentReveal >= 1.0f) return;
    }

    //--Reposition.
    glTranslatef(cXOffset, cYOffset, 0.0f);

    //--Get the nominal render position.
    TwoDimensionRealPoint cRenderCoords = rEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_BASE);

    ///--[Rendering]
    //--Store rendering position.
    rEntity->mLastRenderX = cRenderCoords.mXCenter + cXOffset;
    rEntity->mLastRenderY = cRenderCoords.mYCenter + cYOffset;

    //--Render.
    glTranslatef(cRenderCoords.mXCenter, cRenderCoords.mYCenter, 0.0f);
    RenderAbyssEntity(rEntity, pAlpha);
    glTranslatef(-cRenderCoords.mXCenter, -cRenderCoords.mYCenter, 0.0f);

    ///--[Finish Up]
    //--Clean translation.
    glTranslatef(-cXOffset, -cYOffset, 0.0f);
}
void AbyssCombat::RenderPartyMemberUI(int pSlot, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders a party member in the given slot, with their UI above them.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->GetElementBySlot(pSlot);
    if(!rEntity) return;

    ///--[Compute Offsets]
    float cXOffset = 0.0f;
    float cYOffset = 0.0f;
    if(pSlot == 1) cXOffset = 334.0f;
    if(pSlot == 2) cXOffset = 668.0f;

    //--Y offset adjusts when the member is disabled.
    float tDisabledPct = EasingFunction::QuadraticInOut(mDisableTimers[pSlot], ABYCOM_DISABLE_FADE_TICKS);
    cYOffset = 300.0f * tDisabledPct;
    pAlpha = pAlpha * (1.0f - tDisabledPct);

    ///--[Rendering Setup]
    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Reposition.
    glTranslatef(cXOffset, cYOffset, 0.0f);

    ///--[Bars]
    //--HP Fill.
    float tHPPct = rEntity->GetDisplayHPPct();
    AbyssImg.Data.rFramePlayerHP->Draw();
    AbyssImg.Data.rFillPlayerHP->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tHPPct, 1.0f);

    //--MP Fill.
    float tMPPct = rEntity->GetDisplayMPPct();
    AbyssImg.Data.rFramePlayerMP->Draw();
    AbyssImg.Data.rFillPlayerMP->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tMPPct, 1.0f);

    //--Overlay.
    AbyssImg.Data.rOverlayHPIcon->Draw();

    ///--[Frame]
    //--Render HP/MP Values above the frame.
    float cHPX =  89.0f;
    float cHPY = 659.0f;
    float cMPX =  55.0f;
    float cMPY = 695.0f;
    int cHPToDisplay = (int)(tHPPct * rEntity->GetHealthMax());
    int cMPToDisplay = (int)(tMPPct * 100.0f);
    if(!rEntity->IsDisplayHPRunning()) cHPToDisplay = rEntity->GetHealth();
    if(!rEntity->IsDisplayMPRunning()) cMPToDisplay = rEntity->GetMagic();
    AbyssImg.Data.rFontPlayerUI->DrawTextArgs(cHPX, cHPY, 0, 1.0f, "%i/%i", cHPToDisplay, rEntity->GetHealthMax());
    AbyssImg.Data.rFontPlayerUI->DrawTextArgs(cMPX, cMPY, 0, 1.0f, "%i/%i", cMPToDisplay, 100);

    ///--[CP Pips]
    float cXPos =  0.0f;
    float cYPos =  0.0f;
    float cWid  = 12.0f;
    int tCurCP = rEntity->GetComboPoints();
    for(int i = 0; i < tCurCP; i ++)
    {
        AbyssImg.Data.rOverlayCPPip->Draw(cXPos, cYPos);
        cXPos = cXPos + cWid;
    }

    ///--[TF Indicators]
    //--These can optionally appear above the character bar to indicate their stage of a TF.
    //  The image can be NULL.
    int tTFCount = rEntity->GetTFIndicatorCount();
    StarBitmap *rTFImg = rEntity->GetTFIndicatorImg();
    if(rTFImg && tTFCount > 0)
    {
        //--Setup.
        float cIndicatorX =  67.0f;
        float cIndicatorY = 622.0f;
        float cIndicatorW =  33.0f;

        //--Render.
        if(tTFCount > 5) tTFCount = 5;
        for(int x = 0; x < tTFCount; x ++)
        {
            rTFImg->Draw(cIndicatorX + (cIndicatorW * x), cIndicatorY);
        }
    }

    ///--[Effects]
    //--We render only 5 effects, after that, a number is shown indicating how many unrendered effects there'
    //  are. In this game it is very unlikely that more than 5 effects will appear.
    float cEffectLft =  40.0f;
    float cEffectTop = 731.0f;
    float cEffectWid =  35.0f;
    StarLinkedList *rEffectList = rEntity->GetEffectRenderList();

    //--Scaling.
    float cEffectScale = 1.0f;
    float cEffectScaleInv = 1.0f / cEffectScale;

    //--Iterate.
    int tRendersSoFar = 0;
    AdvCombatEffect *rEffect = (AdvCombatEffect *)rEffectList->PushIterator();
    while(rEffect)
    {
        //--Not visible on UI, or not associated with this entity.
        if(!rEffect->IsVisibleOnUI() || !rEffect->IsIDOnTargetList(rEntity->GetID()))
        {
            rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
            continue;
        }

        //--Max renders. Exit out.
        if(tRendersSoFar >= 5) { rEffectList->PopIterator(); break; }

        //--Compute position.
        float cLft = cEffectLft + (cEffectWid * tRendersSoFar);

        //--Get images.
        StarBitmap *rEffectBack  = rEffect->GetBackImage();
        StarBitmap *rEffectFrame = rEffect->GetFrameImage();
        StarBitmap *rEffectFront = rEffect->GetFrontImage();

        //--Position, scale.
        glTranslatef(cLft, cEffectTop, 0.0f);
        glScalef(cEffectScale,cEffectScale, 1.0f);

        //--Render.
        if(rEffectBack)  rEffectBack ->Draw();
        if(rEffectFrame) rEffectFrame->Draw();
        if(rEffectFront) rEffectFront->Draw();

        //--Unscale, unposition.
        glScalef(cEffectScaleInv,cEffectScaleInv, 1.0f);
        glTranslatef(-cLft, -cEffectTop, 0.0f);

        //--Next.
        tRendersSoFar ++;
        rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
    }

    ///--[Finish Up]
    //--Clean translation.
    glTranslatef(-cXOffset, -cYOffset, 0.0f);
}
void AbyssCombat::RenderEnemy(int pSlot, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Render an enemy in the given slot. Enemies present render in a fixed layout.
    AdvCombatEntity *rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->GetElementBySlot(pSlot);
    if(!rEnemy) return;

    //--Make sure there's a portrait to render.
    StarBitmap *rCombatPortrait = rEnemy->GetCombatPortrait();
    if(!rCombatPortrait) return;

    //--Block rendering when KO'd.
    if(rEnemy->IsBlockRenderForKO())
    {
        rEnemy->mLastRenderX = ADVCE_NORENDER;
        rEnemy->mLastRenderY = ADVCE_NORENDER;
        return;
    }

    ///--[Compute Position]
    //--Enemies render at 75% size. Compute their render position.
    float cRenderScale = 0.75f;
    float cRenderScaleInv = 1.0f / cRenderScale;
    float cXPos = rEnemy->GetCombatX();
    float cYPos = rEnemy->GetCombatY();

    //--Shake offset.
    cXPos = cXPos + rEnemy->GetShakeX();

    ///--[Fade-in Alpha]
    float tFadePct = rEnemy->GetFadePercent();
    pAlpha = pAlpha * tFadePct;

    ///--[Render]
    //--Store rendering position.
    rEnemy->mLastRenderX = cXPos;
    rEnemy->mLastRenderY = cYPos;

    //--Setup.
    glTranslatef(cXPos, cYPos, 0.0f);
    glScalef(cRenderScale, cRenderScale, 1.0f);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Center the portrait.
    float cHfWid = rCombatPortrait->GetTrueWidth()  * 0.50f;
    float cHfHei = rCombatPortrait->GetTrueHeight() * 0.50f;
    glTranslatef(-cHfWid, -cHfHei, 0.0f);
    RenderAbyssEntity(rEnemy, pAlpha);
    glTranslatef(cHfWid, cHfHei, 0.0f);

    //--Exclamation point. Only renders if the enemy has a specific tag.
    if(rEnemy->GetTagCount("Exclamation") > 0)
    {
        //--Position.
        float cTrueYPos = cYPos - cHfHei - 170.0f;
        if(cTrueYPos < -75.0f) cTrueYPos = -75.0f;
        float cRenderYPos = cTrueYPos - cYPos;

        //--Render.
        AbyssImg.Data.rOverlayExclamation->Draw(0.0f, cRenderYPos);
    }

    ///--[Finish Up]
    //--Clean.
    glScalef(cRenderScaleInv, cRenderScaleInv, 1.0f);
    glTranslatef(-cXPos, -cYPos, 0.0f);
}

///========================================= Entity UI ============================================
void AbyssCombat::RenderEntityUI(AdvCombatEntity *pEntity, float pAlpha)
{
    ///--[Documentation]
    //--Given an entity, renders their HP bar, effects, stun status, and so on. Same as the base class
    //  but has a different set of images and fonts.
    if(!pEntity || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the UI.
    if(mHideCombatUI) return;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Knockout]
    //--Causes a fade out of this object. May block rendering.
    float cAlpha = 1.0f;
    if(pEntity->IsKnockingOut())
    {
        //--Render a fadeout.
        int tKnockoutTimer = pEntity->GetKnockoutTimer();
        if(tKnockoutTimer < ADVCE_KNOCKOUT_TOWHITE_TICKS)
        {
            cAlpha = 1.0f - EasingFunction::QuadraticOut(tKnockoutTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
        }
        //--Stop rendering past the to white knockout.
        else
        {
            return;
        }
    }
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pAlpha);

    //--Get positions.
    float cRenderX = pEntity->GetCombatX() - 68.0f + pEntity->GetUIRenderPosition(ACE_UI_INDEX_MONO_UI_OFFSET).mXCenter;
    float cRenderY = 300.0f;//255.0f + pEntity->GetUIRenderPosition(ACE_UI_INDEX_MONO_UI_OFFSET).mYCenter;

    ///--[Frame, Bars]
    //--Get relative position of the frame.
    float cFrameX = cRenderX - (Images.Data.rEnemyHPFrame->GetTrueWidth() * 0.50f);
    float cFrameY = cRenderY;

    //--Render the frame, and the HP fill.
    AbyssImg.Data.rFrameEnemyHP->Draw(cFrameX, cFrameY);
    double cHPBarPercent = pEntity->GetDisplayHPPct();
    if(cHPBarPercent > 0.0f)
    {
        AbyssImg.Data.rFillEnemyHP->RenderPercent(cFrameX+9.0f, cFrameY+6.0f, 0.0f, 0.0f, cHPBarPercent, 1.0f);
    }
    AbyssImg.Data.rOverlayEnemyHPIcon->Draw(cFrameX-10.0f, cFrameY+4.0f);

    //--HP Text Value
    int tHPValue = pEntity->GetHealth();
    AbyssImg.Data.rFontPlayerUI->DrawTextArgs(cFrameX + 27.0f, cFrameY + 5.0f, 0, 1.0f, "%i", tHPValue);

    ///--[Name]
    //--Render the name above the bar.
    mColorHeader.SetAsMixerAlpha(pAlpha);
    AbyssImg.Data.rFontHeader->DrawText(cFrameX + 57.0f, cFrameY - 43.0f, SUGARFONT_AUTOCENTER_X, 1.0f, pEntity->GetDisplayName());
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Stun Rendering]
    //--Doesn't render if the current stun values are all zeroes.
    int tStunCur    = pEntity->GetDisplayStun();
    int tStunCap    = pEntity->GetStatistic(STATS_STUN_CAP);
    int tStunRes    = pEntity->GetStunResist();
    //int tStunResCnt = pEntity->GetStunResistTimer();

    //--Must be stunnable and have stun information to show.
    if(pEntity->IsStunnable() && (tStunCur > 0 || tStunRes > 0))
    {
        //--Position.
        float cStunY = cFrameY + 40.0f;

        //--Bar fill. Compute percentage.
        float cStunPct = 0.0f;
        if(tStunCap > 0)
        {
            cStunPct = (float)tStunCur / (float)tStunCap;
            if(cStunPct > 1.0f) cStunPct = 1.0f;
        }
        else
        {
            cStunPct = 1.0f;
        }

        //--Render.
        AbyssImg.Data.rFillEnemyStun->RenderPercent(cFrameX, cStunY, 0.0f, 0.0f, 1.0f, 1.0f);

        //--Frame.
        AbyssImg.Data.rFrameEnemyStun->Draw(cFrameX, cStunY);

        //--Stun value.
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, pAlpha);
        AbyssImg.Data.rFontEnemyUI->DrawTextFixedArgs(cFrameX + 3.0f, cStunY - 2.0f, 71.0f, 0, "%i / %i", tStunCur, tStunCap);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        //--Stun resist pips.
        for(int i = 0; i < tStunRes; i ++)
        {
            AbyssImg.Data.rFillEnemyStar->Draw(cFrameX + 79.0f + (i * 13.0f), cStunY + 2.0f);
        }

        //--Stun resist timer.
        //Images.Data.rClockIcon->Draw(cFrameX + 121.0f, cStunY - 1.0f);
        //StarlightColor::SetMixer(0.9f, 0.8f, 0.6f, pAlpha);
        //AbyssImg.Data.rFontEnemyUI->DrawTextArgs(cFrameX + 142.0f, cStunY - 6.0f, 0, 1.0f, "%i", tStunResCnt);
        //StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    ///--[Effects]
    //--Render up to 12 effects.
    float cEffectX = cFrameX + 249.0f;
    float cEffectY = cFrameY + 0.0f;
    float cEffectB = cEffectY + 30.0f;

    //--Constants.
    float cEffectScale = 1.0f;
    float cEffectScaleInv = 1.0f / cEffectScale;

    //--Counts.
    int tRendersSoFar = 0;
    StarLinkedList *rEffectList = pEntity->GetEffectRenderList();

    //--Iterate.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)rEffectList->PushIterator();
    while(rEffect)
    {
        //--Stop if we run out of renders.
        if(tRendersSoFar >= 12)
        {
            rEffectList->PopIterator();
            break;
        }

        //--If the effect is marked as hidden, do not render it.
        if(!rEffect->IsVisibleOnUI() || !rEffect->IsVisibleOnQuickUI())
        {
            rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
            continue;
        }

        //--Compute position.
        int tLineX = tRendersSoFar / 2;
        float cRenderX = cEffectX + (35.0f * tLineX);
        float cRenderY = cEffectY;
        if(tRendersSoFar % 2 == 1) cRenderY = cEffectB;

        //--Position, scale.
        glTranslatef(cRenderX, cRenderY, 0.0f);
        glScalef(cEffectScale, cEffectScale, 1.0f);

        //--Get the associated icon.
        StarBitmap *rEffectBack  = rEffect->GetBackImage();
        StarBitmap *rEffectFrame = rEffect->GetFrameImage();
        StarBitmap *rEffectFront = rEffect->GetFrontImage();
        if(rEffectBack)  rEffectBack ->Draw();
        if(rEffectFrame) rEffectFrame->Draw();
        if(rEffectFront) rEffectFront->Draw();

        //--Unscale, unposition.
        glScalef(cEffectScaleInv, cEffectScaleInv, 1.0f);
        glTranslatef(-cRenderX, -cRenderY, 0.0f);

        //--Next.
        tRendersSoFar ++;
        rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
    }

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void AbyssCombat::RenderTurnOrder(float pAlpha)
{
    ///--[Documentation and Setup]
    //--Renders the turn order in the top left. Extends downwards, with the next-acting entity at the top.
    if(!Images.mIsReady || pAlpha <= 0.0f) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    //--Disable during victory sequence.
    if(mCombatResolution == ADVCOMBAT_END_VICTORY)  return;

    //--Mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--List.
    StarLinkedList *tRenderList = new StarLinkedList(true);

    ///--[Next Turn]
    //--Compute the starting position based on the party size.
    float cPartyYStart = 0.0f;

    //--Y Variables / Constants
    float cYStart = cPartyYStart + 0.0f;
    float cYEnd = cYStart + 130.0f;
    float tYPos = cYStart;

    //--Compute the sub-offset.
    float cSubPct = (float)mTurnBarNewTurnTimer / (float)mTurnBarNewTurnTimerMax;

    //--Scale and Alpha Variables / Constants
    float cScaleStart = 1.25f;
    float cScaleEnd   = 0.75f;
    float cAlphaStart = 1.00f;
    float cAlphaEnd   = 1.00f;

    //--Element sizes.
    float cElemBaseWid = Images.Data.rTurnOrderFriendly->GetWidth();
    float cElemBaseHei = Images.Data.rTurnOrderFriendly->GetHeight();

    //--Symbol indicating the next turn.
    //AbyssImg.Data.rNextTurnFixed->Draw();

    //--Offset the Y position by this timer.
    float tTurnOrderPct = 1.0f - ((float)mTurnBarMoveTimer / (float)mTurnBarMoveTimerMax);
    float cOffsetVal = (cElemBaseHei * cScaleStart * tTurnOrderPct);
    tYPos = tYPos + cOffsetVal;

    ///--[Package Creation]
    //--Zeroth entity uses this, all future entities use the non-cur version.
    StarBitmap *rUseBacking = AbyssImg.Data.rFrameTurnOrderCur;

    //--In order to render these correctly, they must be done last-to-first even though the calculations
    //  are first-to-last. Therefore, we create a rendering pack and store the rendering data in there.
    AdvCombatEntity *rTurnEntity = (AdvCombatEntity *)mrTurnOrder->PushIterator();
    while(rTurnEntity)
    {
        ///--[Setup]
        //--Compute percentage.
        float cYPercent = tYPos / cYEnd;
        if(cYPercent > 1.0f) cYPercent = 1.0f;

        //--Get scale.
        float tCurScale = cScaleStart + ((cScaleEnd - cScaleStart) * cYPercent);
        float tCurAlpha = cAlphaStart + ((cAlphaEnd - cAlphaStart) * cYPercent);

        //--If targeting is taking place, and the entity in question is being targeted, they render normally.
        //  Otherwise, they are greyed out. If targeting is not happening, all entities render fullbright.
        float cMixer = 1.0f;
        if(mIsSelectingTargets)
        {
            if(!IsEntityInActiveTargetCluster(rTurnEntity))
            {
                cMixer = 0.50f;
            }
            else
            {
                tCurAlpha = 1.0f;
            }
        }

        ///--[Create Render Package]
        //--Store everything for later rendering.
        TurnOrderRenderPack *nPackage = (TurnOrderRenderPack *)starmemoryalloc(sizeof(TurnOrderRenderPack));
        nPackage->mXPos = cElemBaseWid * 0.0f * tCurScale;
        nPackage->mYPos = tYPos;
        nPackage->mScale = tCurScale;
        nPackage->mMixer = cMixer;
        nPackage->mAlpha = tCurAlpha;
        nPackage->rImage = rTurnEntity->GetTurnIcon();
        nPackage->rBacking = rUseBacking;
        tRenderList->AddElementAsHead("X", nPackage, &FreeThis);

        ///--[Next]
        //--Increment Y position by height.
        tYPos = tYPos + ((cElemBaseHei * tCurScale) * cSubPct);

        //--Entry cap. Stop rendering when the turn order threatens the player bar.
        if((tYPos + (cElemBaseHei * tCurScale)) > 440.0f)
        {
            mrTurnOrder->PopIterator();
            break;
        }

        //--Next.
        rUseBacking = AbyssImg.Data.rFrameTurnOrder;
        rTurnEntity = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
    }

    ///--[Rendering Loop]
    //--Iterate across the packages and render them back to front.
    TurnOrderRenderPack *rRenderPack = (TurnOrderRenderPack *)tRenderList->PushIterator();
    while(rRenderPack)
    {
        //--Position, scale, color.
        glTranslatef(rRenderPack->mXPos, rRenderPack->mYPos, 0.0f);
        glScalef(rRenderPack->mScale, rRenderPack->mScale, 1.0f);
        StarlightColor::SetMixer(rRenderPack->mMixer, rRenderPack->mMixer, rRenderPack->mMixer, pAlpha * rRenderPack->mAlpha);

        //--Render.
        if(rRenderPack->rBacking) rRenderPack->rBacking->Draw();
        if(rRenderPack->rImage)   rRenderPack->rImage->Draw();

        //--Unscale, unposition.
        glScalef(1.0f / rRenderPack->mScale, 1.0f / rRenderPack->mScale, 1.0f);
        glTranslatef(-rRenderPack->mXPos, -rRenderPack->mYPos, 0.0f);

        //--Next.
        rRenderPack = (TurnOrderRenderPack *)tRenderList->AutoIterate();
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    delete tRenderList;
}
void AbyssCombat::RenderAbyssEntity(AdvCombatEntity *pEntity, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Performs the nitty gritty of rendering the entity, after its position has been resolved. Handles
    //  cases like flashing, whiteout, and dying.
    //--This function assumes that scaling and positioning are already handled.
    if(!pEntity) return;

    //--Make sure there's a portrait to render.
    StarBitmap *rCombatPortrait = pEntity->GetCombatPortrait();
    if(!rCombatPortrait) return;

    //--Block rendering when KO'd. This only occurs while sliding off the screen for player party members.
    if(pEntity->IsBlockRenderForKO()) return;

    //--Possible crossfading.
    int tCrossfadeTimer = pEntity->GetCrossfadeTimer();
    StarBitmap *rCrossfadePortrait = pEntity->GetCrossfadePortrait();

    ///--[Target Handling]
    //--If targeting is currently happening, all untargeted entities are greyed out slightly.
    if(mIsSelectingTargets)
    {
        if(!IsEntityInActiveTargetCluster(pEntity))
        {
            StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f * pAlpha);
        }
    }
    //--If the entity is currently on the active target cluster, it will flash.
    else if(mIsSelectingTargets && mTargetFlashTimer < ADVCOMBAT_TARGET_FLASH_PERIOD/2 && false)
    {
        //--Make sure we're on the target cluster.
        TargetCluster *rActiveCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
        if(rActiveCluster)
        {
            //--In the cluster, set mixer to black.
            if(rActiveCluster->IsElementInCluster(pEntity))
            {
                StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f * pAlpha);
            }
        }
    }
    ///--[Black Flash]
    //--Entities flash black when they are acting.
    else if(pEntity->IsFlashingBlack())
    {
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f * pAlpha);
    }
    ///--[White Flash]
    //--Fades up to fullwhite using the stencil buffer.
    else if(pEntity->GetWhiteFlashTimer() > 0)
    {
        //--Timer.
        int tTimer = pEntity->GetWhiteFlashTimer();
        float cPercent = EasingFunction::QuadraticInOut(tTimer, ADVCE_FLASH_WHITE_TICKS);

        //--Normal image. Color mask stays on.
        DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
        glColorMask(true, true, true, true);
        rCombatPortrait->Draw();

        //--Render a white overlay.
        DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
        glDisable(GL_TEXTURE_2D);
        glColor4f(1.0f, 1.0f, 1.0f, cPercent);
        rCombatPortrait->Draw();

        //--Clean up.
        glEnable(GL_TEXTURE_2D);
        DisplayManager::DeactivateStencilling();
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        return;
    }
    ///--[Knockout]
    //--Suspends normal rendering.
    else if(pEntity->IsKnockingOut())
    {
        //--Timer.
        int tKnockoutTimer = pEntity->GetKnockoutTimer();

        //--Fading up to white. First, render the normal image and stencil.
        if(tKnockoutTimer < ADVCE_KNOCKOUT_TOWHITE_TICKS)
        {
            //--Normal image.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            rCombatPortrait->Draw();

            //--Render a white overlay.
            float cPercent = EasingFunction::QuadraticOut(tKnockoutTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glDisable(GL_TEXTURE_2D);
            glColor4f(1.0f, 1.0f, 1.0f, cPercent);
            rCombatPortrait->Draw();

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            return;
        }
        //--Fading down to black.
        else
        {
            //--Disable callouts at this point.
            pEntity->mLastRenderX = ADVCE_NORENDER;
            pEntity->mLastRenderY = ADVCE_NORENDER;

            //--Get percent.
            int tUseTimer = tKnockoutTimer - ADVCE_KNOCKOUT_TOWHITE_TICKS;

            //--Portrait, color mask is off.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            rCombatPortrait->Draw();

            //--Fullwhite.
            glDisable(GL_TEXTURE_2D);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            rCombatPortrait->Draw();

            //--Render a black overlay.
            float cPercent = EasingFunction::QuadraticOut(tUseTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            glDisable(GL_TEXTURE_2D);
            glColor4f(0.0f, 0.0f, 0.0f, cPercent);
            rCombatPortrait->Draw();

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
            return;
        }
    }

    ///--[Render]
    //--Crossfading:
    if(rCrossfadePortrait && tCrossfadeTimer < ADVCE_CROSSFADE_TICKS && rCrossfadePortrait != rCombatPortrait)
    {
        //--Crossfade portrait in back.
        float tAlpha = 1.0f - EasingFunction::Linear(tCrossfadeTimer, ADVCE_CROSSFADE_TICKS);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha * pAlpha);
        rCrossfadePortrait->Draw();

        //--Normal portrait in front.
        tAlpha = 1.0f - tAlpha;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha * pAlpha);
        rCombatPortrait->Draw();
    }
    //--No crossfading.
    else
    {
        rCombatPortrait->Draw();
    }

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}

///====================================== Command Window ==========================================
///=================================== Ability Descriptions =======================================
void AbyssCombat::RenderAbilityDescriptions(float pAlpha)
{
    ///--[Documentation]
    //--At the bottom of the screen, shows a description of the highlighted ability. Does nothing
    //  if the render timer is zero.
    if(mAbilityDescriptionTimer < 1 || pAlpha <= 0.0f) return;

    //--Setup.
    float cTxtL = 100.0f;
    float cTxtT = 555.0f;
    float cTxtH =  26.0f;

    //--Resolve alpha.
    float tDescriptionAlpha = EasingFunction::QuadraticInOut(mAbilityDescriptionTimer, ABYCOM_ABILITY_DESCRIPTION_TICKS);

    ///--[Single-Line Render]
    //--If there is no ability selected, then we can still try to get a command. The command may have
    //  a string to display if it contains a menu.
    if(!rDescriptionAbility)
    {
        //--Resolve cursor.
        StarLinkedList *rPlayerStackList = ResolvePlayerCommandList();
        int tUseCursor = mPlayerCommandCursor;
        if(rPlayerStackList == mCommandList) tUseCursor = mPlayerCommandBaseCursor;

        //--Resolve the entry. If we somehow don't find one, stop.
        CommandListEntry *rSelectedEntry = (CommandListEntry *)rPlayerStackList->GetElementBySlot(tUseCursor);
        if(!rSelectedEntry) return;

        //--Make sure the associated string exists.
        if(!rSelectedEntry->mDisplayString) return;

        //--Backing
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha * tDescriptionAlpha);
        AbyssImg.Data.rFrameDescription->Draw();

        //--Render it.
        AbyssImg.Data.rFontHeader->DrawTextArgs(cTxtL, cTxtT-50.0f, 0, 1.0f, rSelectedEntry->mDisplayName);
        AbyssImg.Data.rFontDescription->DrawTextArgs(cTxtL, cTxtT, 0, 1.0f, rSelectedEntry->mDisplayString);
    }
    ///--[Ability Render]
    else
    {
        ///--[Backing]
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha * tDescriptionAlpha);
        AbyssImg.Data.rFrameDescription->Draw();

        ///--[Title]
        AbyssImg.Data.rFontHeader->DrawTextArgs(cTxtL, cTxtT-50.0f, 0, 1.0f, rDescriptionAbility->GetDisplayName());

        ///--[Text]
        //--Description lines. We use the "complex" description, the simple one is used by the skills UI.
        int tDescLinesTotal = rDescriptionAbility->GetDescriptionLinesTotal();
        for(int i = 0; i < tDescLinesTotal; i ++)
        {
            //--Get line.
            StarlightString *rDescriptionLine = rDescriptionAbility->GetDescriptionLine(i);
            if(!rDescriptionLine) break;

            //--Render.
            rDescriptionLine->DrawText(cTxtL, cTxtT + (cTxtH * i), SUGARFONT_NOCOLOR, 1.0f, AbyssImg.Data.rFontDescription);
        }
    }

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}

///========================================== Drawing =============================================
void AbyssCombat::RenderInspector()
{
    //--Does nothing.
}
void AbyssCombat::RenderInspectorAbilities()
{
    //--Does nothing.
}

///=================================== Confirmation Dialogue ======================================
void AbyssCombat::RenderConfirmation()
{
    //--Renders the confirmation dialogue over the screen.
    if(!Images.mIsReady || mConfirmationTimer < 1) return;

    //--Compute alpha.
    float cPct = EasingFunction::QuadraticInOut(mConfirmationTimer, ADVCOMBAT_CONFIRMATION_TICKS);

    //--Grey the screen out a bit.
    float cBacking = cPct * 0.80f;
    StarBitmap::DrawFullBlack(cBacking);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);

    //--Render the confirmation string in the middle of the screen.
    float cXPosition = VIRTUAL_CANVAS_X * 0.50f;
    float cYPosition = VIRTUAL_CANVAS_Y * 0.40f;
    mConfirmationString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, AbyssImg.Data.rFontConfirmBig);

    //--Render the accept string.
    cXPosition = VIRTUAL_CANVAS_X * 0.40f;
    cYPosition = VIRTUAL_CANVAS_Y * 0.50f;
    mAcceptString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, AbyssImg.Data.rFontConfirmSmall);

    //--Render the cancel string.
    cXPosition = VIRTUAL_CANVAS_X * 0.60f;
    cYPosition = VIRTUAL_CANVAS_Y * 0.50f;
    mCancelString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, AbyssImg.Data.rFontConfirmSmall);

    //--Clean.
    StarlightColor::ClearMixer();
}

///====================================== Prediction Boxes ========================================
void AbyssCombat::RenderPredictionBoxes()
{
    ///--[Documentation]
    //--Same as the base version, except if there are three or more prediction boxes, will decrease
    //  the alpha value of all prediction boxes except the cursor box. The cursor can be adjusted
    //  with the shoulder keys. This allows predictions to be visible without obscuring useful information.
    rLastHighlightedPtr = NULL;

    //--Count visible prediction boxes.
    int tVisBoxes = 0;
    AdvCombatPrediction *rCheckBox = (AdvCombatPrediction *)mPredictionBoxes->PushIterator();
    while(rCheckBox)
    {
        if(rCheckBox->IsVisible()) tVisBoxes ++;
        rCheckBox = (AdvCombatPrediction *)mPredictionBoxes->AutoIterate();
    }

    ///--[No Transparency]
    if(tVisBoxes <= 3)
    {
        AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->PushIterator();
        while(rPredictionBox)
        {
            //--Cast to AbyCombatPrediction if possible.
            if(rPredictionBox->IsOfType(POINTER_TYPE_ABYCOMBATPREDICTION))
            {
                AbyCombatPrediction *rAbyVersion = (AbyCombatPrediction *)rPredictionBox;
                rAbyVersion->SetLocalAlpha(1.0f);
            }

            //--Render.
            rPredictionBox->Render();
            rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->AutoIterate();
        }
    }
    ///--[Transparency]
    else
    {
        //--Setup.
        int i = 0;
        AbyCombatPrediction *rZeroth = NULL;

        //--Render the normal cases.
        AdvCombatPrediction *rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->PushIterator();
        while(rPredictionBox)
        {
            //--Cast to AbyCombatPrediction if possible.
            if(rPredictionBox->IsOfType(POINTER_TYPE_ABYCOMBATPREDICTION))
            {
                AbyCombatPrediction *rAbyVersion = (AbyCombatPrediction *)rPredictionBox;
                if(i == mTransparencyCursor)
                {
                    rZeroth = rAbyVersion;
                }
                else
                {
                    rAbyVersion->SetLocalAlpha(0.30f);
                    rPredictionBox->Render();
                }
            }

            //--Render.
            i ++;
            rPredictionBox = (AdvCombatPrediction *)mPredictionBoxes->AutoIterate();
        }

        //--Render the selected one last.
        if(rZeroth)
        {
            rLastHighlightedPtr = rZeroth->GetHost();
            rZeroth->SetLocalAlpha(1.0f);
            rZeroth->Render();
        }
    }

    //--Clean.
    StarlightColor::ClearMixer();
}

///======================================= Skill Titles ===========================================
void AbyssCombat::RenderTitle()
{
    ///--[Documentation]
    //--Same as the basic title render, but left-aligned.
    if(!Images.mIsReady || !mShowTitle || !mTitleText) return;

    //--Does nothing if hiding the combat UI.
    if(mHideCombatUI) return;

    ///--[Transparency]
    //--Title fades in over 10 ticks and out over 10 ticks.
    float cAlpha = 1.0f;
    if(mTitleTicks < 10)
    {
        cAlpha = EasingFunction::QuadraticOut(mTitleTicks, 10);
    }
    else if(mTitleTicks >= mTitleTicksMax - 10)
    {
        cAlpha = 1.0f - EasingFunction::QuadraticIn(mTitleTicks - (mTitleTicksMax - 10), 10);
    }

    //--Set alpha.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Positioning and Setup]
    //--Setup.
    float cEdge = 4.0f;
    float cTxtHei = 31.0f;

    //--Text ideal center.
    float cTextCntX = (VIRTUAL_CANVAS_X * 0.50f) - 441.0f;
    float cTextCntY = (VIRTUAL_CANVAS_Y * 0.12f) -  70.0f;

    //--Get the length of the name.
    int cLength = Images.Data.rTargetClusterFont->GetTextWidth(mTitleText);

    ///--[Box]
    //--Positions.
    float cBoxLft = cTextCntX - (cLength * 0.50f) - cEdge + 0.0f;
    float cBoxTop = cTextCntY - (cTxtHei * 0.50f) - cEdge + 0.0f;
    float cBoxRgt = cTextCntX + (cLength * 0.50f) + cEdge + 0.0f;
    float cBoxBot = cTextCntY + (cTxtHei * 0.50f) + cEdge + 0.0f;

    //--Clamp. The left position can never be less than 115.0f.
    if(cBoxLft < 115.0f)
    {
        float cDif = 115.0f - cBoxLft;
        cBoxLft = cBoxLft + cDif;
        cBoxRgt = cBoxRgt + cDif;
        cTextCntX = cTextCntX + cDif;
    }

    //--Render.
    RenderExpandableBox(cBoxLft, cBoxTop, cBoxRgt, cBoxBot, cEdge, Images.Data.rPredictionBox);

    ///--[Text]
    //--Render the text centered.
    Images.Data.rAbilityTitleFont->DrawText(cTextCntX, cTextCntY - 3.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, mTitleText);

    ///--[Clean]
    StarlightColor::ClearMixer();
}
