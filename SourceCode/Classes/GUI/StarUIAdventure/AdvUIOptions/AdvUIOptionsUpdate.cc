//--Base
#include "AdvUIOptions.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "SaveManager.h"

///========================================== Update ==============================================
bool AdvUIOptions::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer,     0, ADVMENU_OPTIONS_VIS_TICKS,             true);
    StandardTimer(mDetailsTimer,        0, ADVMENU_OPTIONS_DETAILS_SWITCH_TICKS,  (mIsShowingDetails));
    StandardTimer(mUnsavedChangesTimer, 0, ADVMENU_OPTIONS_UNSAVED_CHANGES_TICKS, (mIsShowingUnsavedChanges));
    StandardTimer(mRebindTimer,         0, ADVMENU_OPTIONS_REBIND_TICKS,          (mIsRebinding));
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks,           (mIsShowingHelp));

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Arrow oscillation.
    mArrowTimer = (mArrowTimer + 1) % ADVMENU_OPTIONS_ARROW_OSCILLATE_TICKS;

    ///--[Error Checking]
    //--If this menu is not the main object, stop the update here.
    if(pCannotHandleUpdate) return false;

    //--Disable control inputs during a lockout.
    if(mRebindLockout > 0) { mRebindLockout --; return true; }

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(mIsShowingHelp)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingHelp = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsShowingHelp = true;
        RefreshMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Control Handling]
    //--Setup.
    bool tRecomputeCursorPosition = false;

    ///--[Rebinding Keys]
    //--When in this mode, the next key pressed instead becomes the desired control.
    if(mIsRebinding)
    {
        //--Otherwise, wait for a keypress.
        if(rControlManager->IsAnyKeyPressed())
        {
            //--Store these scancodes. They don't get updated until the user exits this menu.
            int tKeyboard = -1;
            int tMouse = -1;
            int tJoypad = -1;
            rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoypad);

            //--Cancel rebinding mode.
            mIsRebinding = false;
            mRebindLockout = 3;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Certain keys (F1, F2, F3) cannot be overbound.
            #if defined _ALLEGRO_PROJECT_
            if(tKeyboard == ALLEGRO_KEY_F1 || tKeyboard == ALLEGRO_KEY_F2 || tKeyboard == ALLEGRO_KEY_F3)
            {
                return true;
            }
            #endif
            #if defined _SDL_PROJECT_
            if(tKeyboard == SDL_SCANCODE_F1 || tKeyboard == SDL_SCANCODE_F2 || tKeyboard == SDL_SCANCODE_F3)
            {
                return true;
            }
            #endif

            //--Resolve the control's name from its cursor value.
            if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
            {
                if(mCursor == ADVMENU_OPTIONS_CONTROLS_ACTIVATE)
                {
                    rControlManager->OverbindControl("Activate", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_CANCEL)
                {
                    rControlManager->OverbindControl("Cancel", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_RUN)
                {
                    rControlManager->OverbindControl("Run", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_UP)
                {
                    rControlManager->OverbindControl("Up", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_RIGHT)
                {
                    rControlManager->OverbindControl("Right", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_DOWN)
                {
                    rControlManager->OverbindControl("Down", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_LEFT)
                {
                    rControlManager->OverbindControl("Left", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERLEFT)
                {
                    rControlManager->OverbindControl("UpLevel", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)
                {
                    rControlManager->OverbindControl("DnLevel", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_FIELDABILITIES)
                {
                    rControlManager->OverbindControl("OpenFieldAbilityMenu", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_1)
                {
                    rControlManager->OverbindControl("FieldAbility0", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_2)
                {
                    rControlManager->OverbindControl("FieldAbility1", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_3)
                {
                    rControlManager->OverbindControl("FieldAbility2", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_4)
                {
                    rControlManager->OverbindControl("FieldAbility3", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
                else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_5)
                {
                    rControlManager->OverbindControl("FieldAbility4", mIsRebindingSecondary, tKeyboard, tMouse, tJoypad);
                }
            }

            //--Write to the save file.
            SaveManager::Fetch()->SaveControlsFile();

            //--Unset any control cases.
            rControlManager->BlankControls();
            rControlManager->ClearControlFlags();

            //--Check if any strings changed.
            ReresolveOptionStrings();
            return true;
        }
        return true;
    }

    ///--[Unsaved Changes Reminder]
    //--Intercepts player controls when exiting with unsaved changes.
    if(mIsShowingUnsavedChanges)
    {
        if(rControlManager->IsFirstPress("Activate"))
        {
            mIsShowingUnsavedChanges = false;
            CancelOptions();
            FlagExit();
            //SetToMainMenu(AM_MAIN_OPTIONS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingUnsavedChanges = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return true;
    }

    ///--[Details Handler]
    //--If currently in details mode, pressing cancel exits it. Otherwise it blocks the update.
    if(mIsShowingDetails)
    {
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingDetails = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return true;
    }

    //--Immediately switches to details mode on whatever option is highlighted. Stops the rest of the update.
    if(rControlManager->IsFirstPress("F2"))
    {
        mIsShowingDetails = true;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Shoulder Buttons]
    //--Shoulder-left, decrements the mode cursor. Wraps to maximum.
    if(rControlManager->IsFirstPress("UpLevel") || (rControlManager->IsDown("Ctrl") && rControlManager->IsFirstPress("Left")))
    {
        //--Set.
        mMode --;
        mCursor = 0;
        RecomputeCursorPositions();

        //--Wrap.
        if(mMode < 0)
        {
            mMode = ADVMENU_OPTIONS_MODE_TOTAL - 1;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }
    //--Shoulder-right, increments the mode cursor. Wraps to zero.
    if(rControlManager->IsFirstPress("DnLevel") || (rControlManager->IsDown("Ctrl") && rControlManager->IsFirstPress("Right")))
    {
        //--Set.
        mMode ++;
        mCursor = 0;
        RecomputeCursorPositions();

        //--Wrap.
        if(mMode >= ADVMENU_OPTIONS_MODE_TOTAL)
        {
            mMode = 0;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Up and Down]
    //--Decrements cursor. Wraps to end.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Resolve clamp for this mode.
        int tClamp = 0;
        if(     mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY) tClamp = ADVMENU_OPTIONS_GAMEPLAY_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)    tClamp = ADVMENU_OPTIONS_AUDIO_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS) tClamp = ADVMENU_OPTIONS_CONTROLS_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)    tClamp = ADVMENU_OPTIONS_DEBUG_TOTAL;

        //--Set.
        mCursor --;
        tRecomputeCursorPosition = true;

        //--Wrap.
        if(mCursor < 0)
        {
            mCursor = tClamp; //Sets it to the Okay button.
        }
        else if(mCursor == tClamp+0)
        {
            mCursor = tClamp-1;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Shoulder-right, increments the mode cursor. Wraps to zero.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Resolve clamp for this mode.
        int tClamp = 0;
        if(     mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY) tClamp = ADVMENU_OPTIONS_GAMEPLAY_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)    tClamp = ADVMENU_OPTIONS_AUDIO_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS) tClamp = ADVMENU_OPTIONS_CONTROLS_TOTAL;
        else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)    tClamp = ADVMENU_OPTIONS_DEBUG_TOTAL;

        //--Set.
        mCursor ++;
        tRecomputeCursorPosition = true;

        //--Wrap.
        if(mCursor >= tClamp + 1) //Offset by one for the okay button.
        {
            mCursor = 0;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Left and Right]
    //--These are used to manipulate options and pass to a subroutine.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        HandleLeftPress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        HandleRightPress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Activate, Run, and Cancel]
    //--Activate. Begins editing if on an option.
    if(rControlManager->IsFirstPress("Activate"))
    {
        HandleActivatePress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    //--Run. Only used when rebinding controls.
    if(rControlManager->IsFirstPress("Run"))
    {
        HandleRunPress(mMode, mCursor);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Has unsaved changes:
        if(mHasUnsavedChanges)
        {
            mIsShowingUnsavedChanges = true;
        }
        //--No unsaved changes, exit immediately:
        else
        {
            CancelOptions();
            FlagExit();
        }
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Restore Defaults Keys]
    //--In controls mode, and only controls mode, pressing these keys can reset controls back to their defaults.
    if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        //--Resets the selected control to its defaults.
        if(rControlManager->IsFirstPress("F3"))
        {
            SetDefaultControl(mCursor);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Resets all controls to their defaults. Useful if the player overbinds everything.
        if(rControlManager->IsFirstPress("F5"))
        {
            SetAllDefaultControls();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    ///--[Cursor Handling]
    if(tRecomputeCursorPosition)
    {
        RecomputeCursorPositions();
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}
void AdvUIOptions::UpdateBackground()
{
    ///--[Documentation]
    //--Handles background update when this is not the main object.
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
}
void AdvUIOptions::HandleActivatePress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Given a menu mode and cursor, handles activate. Booleans toggle, most other entries enter
    //  editing mode.
    if(pMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Boolean handlers.
        if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_DOCTORBAG)
        {
            mAutoUseDoctorBag.mCurValue.b = !mAutoUseDoctorBag.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            mAutoHastenDialogue.mCurValue.b = !mAutoHastenDialogue.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOURISTMODE)
        {
            mTouristMode.mCurValue.b = !mTouristMode.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            mCombatMemoryCursor.mCurValue.b = !mCombatMemoryCursor.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWCENSORBARS)
        {
            mShowCensorBars.mCurValue.b = !mShowCensorBars.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            mAllowAssetStreaming.mCurValue.b = !mAllowAssetStreaming.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            mUseRAMLoading.mCurValue.b = !mUseRAMLoading.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            mUnloadActors.mCurValue.b = !mUnloadActors.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            mUseFadingUI.mCurValue.b = !mUseFadingUI.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_EXPERTCOMBATUI)
        {
            mExpertCombatUI.mCurValue.b = !mExpertCombatUI.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_BLOCKSMUSTACTIVATE)
        {
            mBlocksMustActivate.mCurValue.b = !mBlocksMustActivate.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWSTAMINABAR)
        {
            mShowStaminaBar.mCurValue.b = !mShowStaminaBar.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_DISALLOW_OVERLAYS)
        {
            mDisallowOverlays.mCurValue.b = !mDisallowOverlays.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_AUTORUN)
        {
            mAutorun.mCurValue.b = !mAutorun.mCurValue.b;
        }
        //--Okay. Exits mode.
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Toggle boolean.
        /*if(pCursor == ADVMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            mSaveVolumesInSavefile.mCurValue.b = !mSaveVolumesInSavefile.mCurValue.b;
        }
        //--Okay. Exits mode.
        else */if(pCursor == ADVMENU_OPTIONS_AUDIO_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == ADVMENU_OPTIONS_AUDIO_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        //--Rebinding.
        if(pCursor < ADVMENU_OPTIONS_CONTROLS_TOTAL)
        {
            mIsRebinding = true;
            mIsRebindingSecondary = false;
        }
        //--Okay. Exits mode.
        if(pCursor == ADVMENU_OPTIONS_CONTROLS_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == ADVMENU_OPTIONS_CONTROLS_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        //--Toggle booleans.
        if(pCursor == ADVMENU_OPTIONS_DEBUG_DISABLE_OUTLINE)
        {
            mDisableShaderOutline.mCurValue.b = !mDisableShaderOutline.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER)
        {
            mDisableShaderUnderwater.mCurValue.b = !mDisableShaderUnderwater.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_DEBUG_DISABLE_LIGHTING)
        {
            mDisableShaderLighting.mCurValue.b = !mDisableShaderLighting.mCurValue.b;
        }
        //--Okay. Exits mode.
        if(pCursor == ADVMENU_OPTIONS_DEBUG_TOTAL + 0)
        {
            SaveOptions();
            FlagExit();
        }
        //--Defaults.
        else if(pCursor == ADVMENU_OPTIONS_DEBUG_TOTAL + 1)
        {
            SetDefaultOptions();
        }
    }

    //--Subroutine checks changes list.
    RecheckChanges();
}
void AdvUIOptions::HandleRunPress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Only actually does anything during control handling, where it will rebind the secondary input instead of the primary.
    //  It is otherwise ignored.
    if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        //--Okay/Defaults button ignored.
        if(pCursor >= ADVMENU_OPTIONS_CONTROLS_TOTAL) return;

        //--Activate secondary binding.
        mIsRebinding = true;
        mIsRebindingSecondary = true;
    }
}
void AdvUIOptions::HandleLeftPress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Given a menu mode and cursor, handles left press.
    if(pMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Boolean handlers.
        if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_DOCTORBAG)
        {
            mAutoUseDoctorBag.mCurValue.b = !mAutoUseDoctorBag.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            mAutoHastenDialogue.mCurValue.b = !mAutoHastenDialogue.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOURISTMODE)
        {
            mTouristMode.mCurValue.b = !mTouristMode.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            mCombatMemoryCursor.mCurValue.b = !mCombatMemoryCursor.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_LIGHTBOOST)
        {
            mAmbientLightBoost.mCurValue.f = mAmbientLightBoost.mCurValue.f - 0.05f;
            if(mAmbientLightBoost.mCurValue.f < 0.0f) mAmbientLightBoost.mCurValue.f = 0.0f;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWCENSORBARS)
        {
            mShowCensorBars.mCurValue.b = !mShowCensorBars.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)
        {
            mMaxAutosaves.mCurValue.i --;
            if(mMaxAutosaves.mCurValue.i < 0) mMaxAutosaves.mCurValue.i = 0;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            mAllowAssetStreaming.mCurValue.b = !mAllowAssetStreaming.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            mUseRAMLoading.mCurValue.b = !mUseRAMLoading.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            mUnloadActors.mCurValue.b = !mUnloadActors.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            mUseFadingUI.mCurValue.b = !mUseFadingUI.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_EXPERTCOMBATUI)
        {
            mExpertCombatUI.mCurValue.b = !mExpertCombatUI.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_BLOCKSMUSTACTIVATE)
        {
            mBlocksMustActivate.mCurValue.b = !mBlocksMustActivate.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWSTAMINABAR)
        {
            mShowStaminaBar.mCurValue.b = !mShowStaminaBar.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_DISALLOW_OVERLAYS)
        {
            mDisallowOverlays.mCurValue.b = !mDisallowOverlays.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_AUTORUN)
        {
            mAutorun.mCurValue.b = !mAutorun.mCurValue.b;
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 0)
        {
            mCursor = ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 1)
        {
            mCursor = ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Float handlers.
        if(pCursor == ADVMENU_OPTIONS_MUSIC_VOLUME)
        {
            mMusicVol.mCurValue.f = mMusicVol.mCurValue.f - 0.05f;
            if(mMusicVol.mCurValue.f < 0.0f) mMusicVol.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVol.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_SOUND_VOLUME)
        {
            mSoundVol.mCurValue.f = mSoundVol.mCurValue.f - 0.05f;
            if(mSoundVol.mCurValue.f < 0.0f) mSoundVol.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeSoundVolumeTo(mSoundVol.mCurValue.f);
        }
        //--Toggle boolean.
        /*else if(pCursor == ADVMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            mSaveVolumesInSavefile.mCurValue.b = !mSaveVolumesInSavefile.mCurValue.b;
        }*/
        //--Toggle integer.
        else if(pCursor == ADVMENU_OPTIONS_VOICELINE_PLAY_CHANCE)
        {
            mVoicePlay.mCurValue.i --;
            if(mVoicePlay.mCurValue.i < 0) mVoicePlay.mCurValue.i = ADVMENU_OPTION_AUDIO_MAX_PLAYCHANCE_SLOTS;//Hardcoded
        }
        //--Abstract volume. All four of these always appear, but which slot they coincide with may change.
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME0)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[0] == -1) return;

            //--Adjust.
            mAbstractVol0.mCurValue.f = mAbstractVol0.mCurValue.f - 0.05f;
            if(mAbstractVol0.mCurValue.f < 0.0f) mAbstractVol0.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[0], mAbstractVol0.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME1)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[1] == -1) return;

            //--Adjust.
            mAbstractVol1.mCurValue.f = mAbstractVol1.mCurValue.f - 0.05f;
            if(mAbstractVol1.mCurValue.f < 0.0f) mAbstractVol1.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[1], mAbstractVol1.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME2)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[2] == -1) return;

            //--Adjust.
            mAbstractVol2.mCurValue.f = mAbstractVol2.mCurValue.f - 0.05f;
            if(mAbstractVol2.mCurValue.f < 0.0f) mAbstractVol2.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[2], mAbstractVol2.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME3)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[3] == -1) return;

            //--Adjust.
            mAbstractVol3.mCurValue.f = mAbstractVol3.mCurValue.f - 0.05f;
            if(mAbstractVol3.mCurValue.f < 0.0f) mAbstractVol3.mCurValue.f = 0.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[3], mAbstractVol3.mCurValue.f);
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == ADVMENU_OPTIONS_AUDIO_TOTAL + 0)
        {
            mCursor = ADVMENU_OPTIONS_AUDIO_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == ADVMENU_OPTIONS_AUDIO_TOTAL + 1)
        {
            mCursor = ADVMENU_OPTIONS_AUDIO_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        //--Okay. Exits mode.
        if(pCursor == ADVMENU_OPTIONS_DEBUG_TOTAL + 0)
        {
            mCursor = ADVMENU_OPTIONS_DEBUG_TOTAL + 1;
            RecomputeCursorPositions();
        }
        //--Defaults.
        else if(pCursor == ADVMENU_OPTIONS_DEBUG_TOTAL + 1)
        {
            mCursor = ADVMENU_OPTIONS_DEBUG_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }

    //--Subroutine checks changes list.
    RecheckChanges();
}
void AdvUIOptions::HandleRightPress(int pMode, int pCursor)
{
    ///--[Documentation]
    //--Given a menu mode and cursor, handles right press.
    if(pMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Boolean handlers.
        if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_DOCTORBAG)
        {
            mAutoUseDoctorBag.mCurValue.b = !mAutoUseDoctorBag.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            mAutoHastenDialogue.mCurValue.b = !mAutoHastenDialogue.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOURISTMODE)
        {
            mTouristMode.mCurValue.b = !mTouristMode.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            mCombatMemoryCursor.mCurValue.b = !mCombatMemoryCursor.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_LIGHTBOOST)
        {
            mAmbientLightBoost.mCurValue.f = mAmbientLightBoost.mCurValue.f + 0.05f;
            if(mAmbientLightBoost.mCurValue.f > 1.0f) mAmbientLightBoost.mCurValue.f = 1.0f;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWCENSORBARS)
        {
            mShowCensorBars.mCurValue.b = !mShowCensorBars.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)
        {
            mMaxAutosaves.mCurValue.i ++;
            if(mMaxAutosaves.mCurValue.i > 100) mMaxAutosaves.mCurValue.i = 100;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            mAllowAssetStreaming.mCurValue.b = !mAllowAssetStreaming.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            mUseRAMLoading.mCurValue.b = !mUseRAMLoading.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            mUnloadActors.mCurValue.b = !mUnloadActors.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            mUseFadingUI.mCurValue.b = !mUseFadingUI.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_EXPERTCOMBATUI)
        {
            mExpertCombatUI.mCurValue.b = !mExpertCombatUI.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_BLOCKSMUSTACTIVATE)
        {
            mBlocksMustActivate.mCurValue.b = !mBlocksMustActivate.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWSTAMINABAR)
        {
            mShowStaminaBar.mCurValue.b = !mShowStaminaBar.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_DISALLOW_OVERLAYS)
        {
            mDisallowOverlays.mCurValue.b = !mDisallowOverlays.mCurValue.b;
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_AUTORUN)
        {
            mAutorun.mCurValue.b = !mAutorun.mCurValue.b;
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 0)
        {
            mCursor = ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 1)
        {
            mCursor = ADVMENU_OPTIONS_GAMEPLAY_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Float handlers.
        if(pCursor == ADVMENU_OPTIONS_MUSIC_VOLUME)
        {
            mMusicVol.mCurValue.f = mMusicVol.mCurValue.f + 0.05f;
            if(mMusicVol.mCurValue.f > 1.0f) mMusicVol.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVol.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_SOUND_VOLUME)
        {
            mSoundVol.mCurValue.f = mSoundVol.mCurValue.f + 0.05f;
            if(mSoundVol.mCurValue.f > 1.0f) mSoundVol.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeSoundVolumeTo(mSoundVol.mCurValue.f);
        }
        //--Toggle boolean.
        /*else if(pCursor == ADVMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            mSaveVolumesInSavefile.mCurValue.b = !mSaveVolumesInSavefile.mCurValue.b;
        }*/
        //--Toggle integer.
        else if(pCursor == ADVMENU_OPTIONS_VOICELINE_PLAY_CHANCE)
        {
            mVoicePlay.mCurValue.i ++;
            if(mVoicePlay.mCurValue.i > ADVMENU_OPTION_AUDIO_MAX_PLAYCHANCE_SLOTS) mVoicePlay.mCurValue.i = 0;//Hardcoded
        }
        //--Abstract volume. All four of these always appear, but which slot they coincide with may change.
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME0)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[0] == -1) return;

            //--Adjust.
            mAbstractVol0.mCurValue.f = mAbstractVol0.mCurValue.f + 0.05f;
            if(mAbstractVol0.mCurValue.f > 1.0f) mAbstractVol0.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[0], mAbstractVol0.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME1)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[1] == -1) return;

            //--Adjust.
            mAbstractVol1.mCurValue.f = mAbstractVol1.mCurValue.f + 0.05f;
            if(mAbstractVol1.mCurValue.f > 1.0f) mAbstractVol1.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[1], mAbstractVol1.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME2)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[2] == -1) return;

            //--Adjust.
            mAbstractVol2.mCurValue.f = mAbstractVol2.mCurValue.f + 0.05f;
            if(mAbstractVol2.mCurValue.f > 1.0f) mAbstractVol2.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[2], mAbstractVol2.mCurValue.f);
        }
        else if(pCursor == ADVMENU_OPTIONS_CHARVOLUME3)
        {
            //--No character in slot, do nothing.
            if(mAbstractVolumeCurrentSlots[3] == -1) return;

            //--Adjust.
            mAbstractVol3.mCurValue.f = mAbstractVol3.mCurValue.f + 0.05f;
            if(mAbstractVol3.mCurValue.f > 1.0f) mAbstractVol3.mCurValue.f = 1.0f;
            AudioManager::Fetch()->ChangeAbstractVolumeTo(mAbstractVolumeCurrentSlots[3], mAbstractVol3.mCurValue.f);
        }
        //--Toggle between Okay and Defaults.
        else if(pCursor == ADVMENU_OPTIONS_AUDIO_TOTAL + 0)
        {
            mCursor = ADVMENU_OPTIONS_AUDIO_TOTAL + 1;
            RecomputeCursorPositions();
        }
        else if(pCursor == ADVMENU_OPTIONS_AUDIO_TOTAL + 0)
        {
            mCursor = ADVMENU_OPTIONS_AUDIO_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
    }
    else if(pMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        //--Okay. Exits mode.
        if(pCursor == ADVMENU_OPTIONS_DEBUG_TOTAL + 0)
        {
            mCursor = ADVMENU_OPTIONS_DEBUG_TOTAL + 1;
            RecomputeCursorPositions();
        }
        //--Defaults.
        else if(pCursor == ADVMENU_OPTIONS_DEBUG_TOTAL + 1)
        {
            mCursor = ADVMENU_OPTIONS_DEBUG_TOTAL + 0;
            RecomputeCursorPositions();
        }
    }

    //--Subroutine checks changes list.
    RecheckChanges();
}
