//--Base
#include "PeakFreaksFragment.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
PeakFreaksFragment::PeakFreaksFragment()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ === PeakFreaksFragment === ]
    ///--[System]
    mLifetime = 0;
    mLifetimeMax = 30;

    ///--[Position]
    mX = 0.0f;
    mY = 0.0f;
    mXSpeed = 0.0f;
    mYSpeed = 0.0f;

    ///--[Display]
    mAngle = rand() % 360;
    rUseBitmap = NULL;
}
PeakFreaksFragment::~PeakFreaksFragment()
{
}
void PeakFreaksFragment::DeleteThis(void *pPtr)
{
    PeakFreaksFragment *rPtr = (PeakFreaksFragment *)pPtr;
    delete rPtr;
}

///===================================== Property Queries =========================================
bool PeakFreaksFragment::IsExpired()
{
    return (mLifetime >= mLifetimeMax);
}

///======================================= Manipulators ===========================================
void PeakFreaksFragment::SetLifetime(int pTicks)
{
    mLifetimeMax = pTicks;
    if(mLifetimeMax < 1) mLifetimeMax = 1;
}
void PeakFreaksFragment::SetImage(SugarBitmap *pImage)
{
    rUseBitmap = pImage;
}
void PeakFreaksFragment::SetPosition(float pX, float pY)
{
    mX = pX;
    mY = pY;
}
void PeakFreaksFragment::SetSpeeds(float pX, float pY)
{
    mXSpeed = pX;
    mYSpeed = pY;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksFragment::Update()
{
    ///--[Timer]
    //--When this hits the cap, the object should be removed.
    mLifetime ++;

    ///--[Position]
    //--Resolve speeds.
    float cPct = EasingFunction::QuadraticOut(mLifetime, mLifetimeMax);
    float cUseSpeedX = mXSpeed * (1.0f - cPct);
    float cUseSpeedY = mYSpeed * (1.0f - cPct);

    //--Update the position by the speeds.
    mX = mX + cUseSpeedX;
    mY = mY + cUseSpeedY;

    //--Angle.
    mAngle = mAngle + 5.0f;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void PeakFreaksFragment::Render()
{
    ///--[Image Check]
    //--Do nothing if the image wasn't set.
    if(!rUseBitmap) return;

    ///--[Fade Check]
    //--Object fades out in the last few ticks.
    float cAlpha = 1.0f;
    if(mLifetime >= mLifetimeMax - 5)
    {
        int tUseTimer = mLifetime - (mLifetimeMax - 5);
        cAlpha = 1.0f - EasingFunction::QuadraticOut(tUseTimer, 5);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    }

    ///--[Render At Center]
    //--Resolve center position.
    float cWid = rUseBitmap->GetWidth()  * 0.50f;
    float cHei = rUseBitmap->GetHeight() * 0.50f;
    float cCntX = mX - cWid;
    float cCntY = mY - cHei;

    //--Move to position.
    glTranslatef(cCntX, cCntY, 0.0f);

    //--Rotate.
    glRotatef(mAngle, 0.0f, 0.0f, 1.0f);

    //--Render.
    rUseBitmap->Draw(-cWid, -cHei);

    ///--[Clean]
    glRotatef(mAngle, 0.0f, 0.0f, -1.0f);
    glTranslatef(-cCntX, -cCntY, 0.0f);
    StarlightColor::ClearMixer();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
