//--Base
#include "PuzzleFight.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void PuzzleFight::HookToLuaState(lua_State *pLuaState)
{
    /* PuzzleFight_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "PuzzleFight_GetProperty", &Hook_PuzzleFight_GetProperty);

    /* PuzzleFight_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "PuzzleFight_SetProperty", &Hook_PuzzleFight_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_PuzzleFight_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //PuzzleFight_GetProperty("Object At Board Position", iX, iY) (1 Integer)

    //--[Player]
    //PuzzleFight_GetProperty("Player Health") (1 Integer)
    //PuzzleFight_GetProperty("Player Ticks Left") (1 Integer)
    //PuzzleFight_GetProperty("Player Moves Left") (1 Integer)

    //--[Enemy]
    //PuzzleFight_GetProperty("Enemy Health") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PuzzleFight_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    PuzzleFight *rPuzzleFight = PuzzleFight::Fetch();
    if(!rPuzzleFight) return LuaTypeError("PuzzleFight_GetProperty", L);

    ///--[System]
    //--Returns what object is at the given board position. -1 on out of range or empty.
    if(!strcasecmp(rSwitchType, "Object At Board Position") && tArgs == 3)
    {
        lua_pushinteger(L, rPuzzleFight->GetObjectAtBoardPosition(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }

    ///--[Player]
    //--Returns the player's current health.
    else if(!strcasecmp(rSwitchType, "Player Health") && tArgs == 1)
    {
        lua_pushinteger(L, rPuzzleFight->GetPlayerHealth());
        tReturns = 1;
    }
    //--Returns how much time, in ticks, is left. -1 means infinite time.
    else if(!strcasecmp(rSwitchType, "Player Ticks Left") && tArgs == 1)
    {
        lua_pushinteger(L, rPuzzleFight->GetTicksLeft());
        tReturns = 1;
    }
    //--Returns how many moves the player had at the end of their turn.
    else if(!strcasecmp(rSwitchType, "Player Moves Left") && tArgs == 1)
    {
        lua_pushinteger(L, rPuzzleFight->GetMovesLeft());
        tReturns = 1;
    }
    ///--[Enemy]
    //--Returns the enemy's current health.
    else if(!strcasecmp(rSwitchType, "Enemy Health") && tArgs == 1)
    {
        lua_pushinteger(L, rPuzzleFight->GetEnemyHealth());
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("PuzzleFight_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_PuzzleFight_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //PuzzleFight_SetProperty("Take Control")
    //PuzzleFight_SetProperty("Update Script", sPath)
    //PuzzleFight_SetProperty("Game State", iFlag)
    //PuzzleFight_SetProperty("Title Print", sString)
    //PuzzleFight_SetProperty("Finish")

    //--[Dislocations]
    //PuzzleFight_SetProperty("Allocate Dislocations", iTotal)
    //PuzzleFight_SetProperty("Dislocation Name", iSlot, sName)
    //PuzzleFight_SetProperty("Dislocation Alt Name", iSlot, sName)
    //PuzzleFight_SetProperty("Dislocation Pos", iSlot, fX, fY)
    //PuzzleFight_SetProperty("Dislocation Dest", iSlot, fX, fY)

    //--[Board]
    //PuzzleFight_SetProperty("Board Tile Position", iX, iY)
    //PuzzleFight_SetProperty("Board Size", iW, iH)
    //PuzzleFight_SetProperty("Register Dislocation To Board Slot", iSlot, iX, iY)
    //PuzzleFight_SetProperty("Spawn Board Object", iType, iBoardX, iBoardY, fSpawnX, fSpawnY)
    //PuzzleFight_SetProperty("Disable Board Object At", iBoardX, iBoardY)
    //PuzzleFight_SetProperty("Clear Board Object At", iBoardX, iBoardY)
    //PuzzleFight_SetProperty("Clear Board Objects")

    //--[Player]
    //PuzzleFight_SetProperty("Player Entity Name", sName)
    //PuzzleFight_SetProperty("Player Ticks Left", iTicks)
    //PuzzleFight_SetProperty("Player Moves Left", iMoves)
    //PuzzleFight_SetProperty("Player Health", iHealth)

    //--[Enemies]
    //PuzzleFight_SetProperty("Enemy Health", iHealth)

    //--[Notifiers]
    //PuzzleFight_SetProperty("Register Floatfade Notifier", fWorldX, fWorldY, sDLPath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PuzzleFight_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    PuzzleFight *rPuzzleFight = PuzzleFight::Fetch();
    if(!rPuzzleFight) return LuaTypeError("PuzzleFight_SetProperty", L);

    ///--[System]
    //--Indicates the object is ready to take control of proceedings from the AdventureLevel.
    if(!strcasecmp(rSwitchType, "Take Control") && tArgs == 1)
    {
        rPuzzleFight->SetHandlingUpdates(true);
        rPuzzleFight->SetComplete(false);
    }
    //--Which script handles update calls.
    else if(!strcasecmp(rSwitchType, "Update Script") && tArgs == 2)
    {
        rPuzzleFight->SetUpdateHandlerScript(lua_tostring(L, 2));
    }
    //--Sets which game state the minigame is in. Should be in the MINIGAME_STATE_ series.
    else if(!strcasecmp(rSwitchType, "Game State") && tArgs == 2)
    {
        rPuzzleFight->SetGameStateFlag(lua_tointeger(L, 2));
    }
    //--Prints a title on the screen to tell the player what's happening.
    else if(!strcasecmp(rSwitchType, "Title Print") && tArgs == 2)
    {
        rPuzzleFight->SetTitlePrint(lua_tostring(L, 2));
    }
    //--Ends the game.
    else if(!strcasecmp(rSwitchType, "Finish") && tArgs == 1)
    {
        rPuzzleFight->BeginTakedown();
        rPuzzleFight->SetHandlingUpdates(false);
    }
    ///--[Dislocations]
    //--Sets how many dislocations are stored by the board.
    else if(!strcasecmp(rSwitchType, "Allocate Dislocations") && tArgs == 2)
    {
        rPuzzleFight->AllocateDislocations(lua_tointeger(L, 2));
    }
    //--Sets the world-name of a dislocation.
    else if(!strcasecmp(rSwitchType, "Dislocation Name") && tArgs == 3)
    {
        rPuzzleFight->SetDislocationName(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Sets which dislocation this one should use when going off the edge of the board.
    else if(!strcasecmp(rSwitchType, "Dislocation Alt Name") && tArgs == 3)
    {
        rPuzzleFight->SetDislocationAltName(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Set current world position of the dislocation.
    else if(!strcasecmp(rSwitchType, "Dislocation Pos") && tArgs == 4)
    {
        rPuzzleFight->SetDislocationPos(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Set target world position of the dislocation. It will move towards this target each tick.
    else if(!strcasecmp(rSwitchType, "Dislocation Dest") && tArgs == 4)
    {
        rPuzzleFight->SetDislocationDest(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    ///--[Board]
    //--Sets where in the world the board starts on the tile map.
    else if(!strcasecmp(rSwitchType, "Board Tile Position") && tArgs == 3)
    {
        rPuzzleFight->SetBoardTilePosition(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets how large the board is. Default is 7x7.
    else if(!strcasecmp(rSwitchType, "Board Size") && tArgs == 3)
    {
        rPuzzleFight->SetBoardSize(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Specifies that the dislocation in the slot is at X/Y on the board.
    else if(!strcasecmp(rSwitchType, "Register Dislocation To Board Slot") && tArgs == 4)
    {
        rPuzzleFight->RegisterDislocationPackToBoardSlot(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Spawns the requested object at the given board position. Note that the spawn points are in world coordinates.
    else if(!strcasecmp(rSwitchType, "Spawn Board Object") && tArgs == 6)
    {
        rPuzzleFight->SpawnBoardObject(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Disables the given object. It will render until it is cleared, the but the subroutines will act like it is an empty square.
    else if(!strcasecmp(rSwitchType, "Disable Board Object At") && tArgs == 3)
    {
        rPuzzleFight->DisableBoardObjectAt(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Clears the requested tile of board objects.
    else if(!strcasecmp(rSwitchType, "Clear Board Object At") && tArgs == 3)
    {
        rPuzzleFight->ClearBoardObjectAt(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Removes all board objects from play.
    else if(!strcasecmp(rSwitchType, "Clear Board Objects") && tArgs == 1)
    {
        rPuzzleFight->ClearAllBoardObjects();
    }
    ///--[Player]
    //--Name of the entity the player is controlling via arrow movements.
    else if(!strcasecmp(rSwitchType, "Player Entity Name") && tArgs == 2)
    {
        rPuzzleFight->SetPlayerEntityName(lua_tostring(L, 2));
    }
    //--How many ticks the player has left. Player's action begins when it hits zero.
    else if(!strcasecmp(rSwitchType, "Player Ticks Left") && tArgs == 2)
    {
        rPuzzleFight->SetPlayerTicks(lua_tointeger(L, 2));
    }
    //--How many moves the player has left. No cap.
    else if(!strcasecmp(rSwitchType, "Player Moves Left") && tArgs == 2)
    {
        rPuzzleFight->SetPlayerMoves(lua_tointeger(L, 2));
    }
    //--Sets current health of the player. Automatically slides using an easing function.
    else if(!strcasecmp(rSwitchType, "Player Health") && tArgs == 2)
    {
        rPuzzleFight->SetPlayerHealth(lua_tointeger(L, 2));
    }
    ///--[Enemies]
    //--Sets current health of the enemy. Automatically slides using an easing function.
    else if(!strcasecmp(rSwitchType, "Enemy Health") && tArgs == 2)
    {
        rPuzzleFight->SetEnemyHealth(lua_tointeger(L, 2));
    }
    ///--[Notifiers]
    //--Registers a notifier object at the given location.
    else if(!strcasecmp(rSwitchType, "Register Floatfade Notifier") && tArgs == 4)
    {
        rPuzzleFight->RegisterNotifier(PF_NOTIFIER_TYPE_FLOATFADE, lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tostring(L, 4));
    }
    ///--[Error case]
    else
    {
        LuaPropertyError("PuzzleFight_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
