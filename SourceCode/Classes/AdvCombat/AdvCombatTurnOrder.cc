//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define ADVCTURN_DEBUG
#ifdef ADVCTURN_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
    #include "AdvCombatEntity.h"
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///===================================== Property Queries =========================================
int AdvCombat::GetTurn()
{
    return mCurrentTurn;
}
bool AdvCombat::IsEntityOnTurnList(uint32_t pUniqueID)
{
    //--Note: The acting entity is on the turn list.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrTurnOrder->PushIterator();
    while(rEntity)
    {
        if(rEntity->GetID() == pUniqueID)
        {
            mrTurnOrder->PopIterator();
            return true;
        }

        rEntity = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
    }

    //--Not found.
    return false;
}

///======================================= Manipulators ===========================================
void AdvCombat::InsertEntityInTurnOrder(uint32_t pUniqueID, int pSlot)
{
    //--Note: Can legally insert an entity already on the list into the turn order.
    AdvCombatEntity *rEntity = GetEntityByID(pUniqueID);
    if(!rEntity) return;

    //--Insert.
    mrTurnOrder->AddElementInSlot(rEntity->GetName(), rEntity, pSlot);
}
void AdvCombat::GivePlayerInitiative()
{
    mPlayerGainedInitiative = true;
}
void AdvCombat::RemovePlayerInitiative()
{
    mPlayerGainedInitiative = false;
}
void AdvCombat::SetEventTimer(int pTicks)
{
    mEventTimer = pTicks;
}
void AdvCombat::SetAsFreeAction()
{
    mWasLastActionFree = true;
}
void AdvCombat::SetAsEffortlessAction()
{
    mWasLastActionEffortless = true;
}
void AdvCombat::EnqueueEvent(CombatEventPack *pPack)
{
    //--Adds an event to the tail of the event queue.
    if(!pPack) return;
    mEventQueue->AddElementAsTail("X", pPack, &CombatEventPack::DeleteThis);
}
void AdvCombat::EnqueueApplicationPack(ApplicationPack *pPack)
{
    if(!pPack) return;
    mApplicationQueue->AddElement("X", pPack, &ApplicationPack::DeleteThis);
}

///===================================== Turn Order Roller ========================================
void AdvCombat::RollTurnOrder()
{
    ///--[Documentation and Setup]
    //--Rolls turn order. All characters roll an initiative value based on their internal value plus rnd() % 40.
    //  Once rolling is done, characters are sorted into buckets based on Always-Strikes-First/Last and Fast/Slow flags.
    //  Then, those are dumped into the turn order and we're done.
    //--Ties are broken by character ID, with party members starting at zero and counting up, then enemies.
    DebugPush(true, "AdvCombat:RollTurnOrder() - Begins.\n");
    mrTurnOrder->ClearList();

    //--Variables.
    int cTotalEntities = 0;

    ///--[Initiative Rolling]
    //--Regardless of bucket position, everyone needs to roll. Note that entities who are stunned or otherwise not acting
    //  still roll, they just pass their turns. KO'd party members do not roll, when revived they wait until the next turn.
    DebugPrint("Rolling initiatives for player party.\n");

    //--For each member, roll.
    AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyMember)
    {
        //--Ask if the member needs to roll.
        if(rPartyMember->CanActThisTurn())
        {
            int cInitiativeRoll = rPartyMember->GetStatistic(ADVCE_STATS_FINAL, STATS_INITIATIVE) + (rand() % ACE_TURN_ORDER_RANGE);
            rPartyMember->SetCurrentInitiative(cInitiativeRoll);
            rPartyMember->SetTurnID(cTotalEntities);
            cTotalEntities ++;
        }

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Same deal but for the baddies.
    DebugPrint("Rolling initiatives for enemy party.\n");
    AdvCombatEntity *rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyMember)
    {
        //--Enemies always roll since KO'd enemies leave the combat party.
        int cInitiativeRoll = rEnemyMember->GetStatistic(ADVCE_STATS_FINAL, STATS_INITIATIVE) + (rand() % ACE_TURN_ORDER_RANGE);
        rEnemyMember->SetCurrentInitiative(cInitiativeRoll);
        rEnemyMember->SetTurnID(cTotalEntities);
        cTotalEntities ++;

        //--Next.
        rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    ///--[Bucketing]
    //--We create a set of lists for each possibility. There are ACE_BUCKET_TOTAL lists and we hard clamp into them.
    //  Entities compute which one they should be in based on their turn-order flags. These are tags that are:
    //  [Always Strikes Last], [Slow], [Fast], [Always Strikes First]
    DebugPrint("Creating turn order buckets.\n");
    StarLinkedList *tTurnLists[ACE_BUCKET_TOTAL];
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++) tTurnLists[i] = new StarLinkedList(true);

    //--Run across the player's party and put them in the appropriate buckets.
    DebugPrint("Putting player party in turn buckets.\n");
    rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyMember)
    {
        //--Skip non-acting party members.
        if(!rPartyMember->CanActThisTurn())
        {
            rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
            continue;
        }

        //--Get the turn bucket in question.
        int tTurnBucket = rPartyMember->ComputeTurnBucket();
        if(tTurnBucket < ACE_BUCKET_LOWEST)  tTurnBucket = ACE_BUCKET_LOWEST;
        if(tTurnBucket > ACE_BUCKET_HIGHEST) tTurnBucket = ACE_BUCKET_HIGHEST;

        //--Create an entry.
        TurnOrderRollPack *nPackage = (TurnOrderRollPack *)starmemoryalloc(sizeof(TurnOrderRollPack));
        nPackage->rEntity = rPartyMember;
        nPackage->mInitiative = rPartyMember->GetCurrentInitiative();
        tTurnLists[tTurnBucket]->AddElement("X", nPackage, &FreeThis);

        //--If this entity is tagged for it, they can be placed in the bucket twice at an initiative penalty
        //  or bonus, depending on the tags. This always adds them to the same bucket as before.
        int tBonusTagCount = rPartyMember->GetTagCount("Act Twice At Ini +");
        int tMalusTagCount = rPartyMember->GetTagCount("Act Twice At Ini -");
        if(tBonusTagCount > 0 || tMalusTagCount > 0)
        {
            TurnOrderRollPack *nPackage = (TurnOrderRollPack *)starmemoryalloc(sizeof(TurnOrderRollPack));
            nPackage->rEntity = rPartyMember;
            nPackage->mInitiative = rPartyMember->GetCurrentInitiative() + tBonusTagCount - tMalusTagCount;
            tTurnLists[tTurnBucket]->AddElement("X", nPackage, &FreeThis);
        }

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Same for the baddies. If the player got initiative, the baddies are not added to the turn buckets
    //  if this is turn 0.
    DebugPrint("Putting enemy party in turn buckets.\n");
    if(!mPlayerGainedInitiative || mCurrentTurn != 0)
    {
        AdvCombatEntity *rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEnemyMember)
        {
            //--Compute, clamp.
            int tTurnBucket = rEnemyMember->ComputeTurnBucket();
            if(tTurnBucket < ACE_BUCKET_LOWEST)  tTurnBucket = ACE_BUCKET_LOWEST;
            if(tTurnBucket > ACE_BUCKET_HIGHEST) tTurnBucket = ACE_BUCKET_HIGHEST;

            //--Create an entry.
            TurnOrderRollPack *nPackage = (TurnOrderRollPack *)starmemoryalloc(sizeof(TurnOrderRollPack));
            nPackage->rEntity = rEnemyMember;
            nPackage->mInitiative = rEnemyMember->GetCurrentInitiative();
            tTurnLists[tTurnBucket]->AddElement("X", nPackage, &FreeThis);

            //--If this entity is tagged for it, they can be placed in the bucket twice at an initiative penalty
            //  or bonus, depending on the tags. This always adds them to the same bucket as before.
            int tBonusTagCount = rEnemyMember->GetTagCount("Act Twice At Ini +");
            int tMalusTagCount = rEnemyMember->GetTagCount("Act Twice At Ini -");
            if(tBonusTagCount > 0 || tMalusTagCount > 0)
            {
                TurnOrderRollPack *nPackage = (TurnOrderRollPack *)starmemoryalloc(sizeof(TurnOrderRollPack));
                nPackage->rEntity = rEnemyMember;
                nPackage->mInitiative = rEnemyMember->GetCurrentInitiative() + tBonusTagCount - tMalusTagCount;
                tTurnLists[tTurnBucket]->AddElement("X", nPackage, &FreeThis);
            }

            //--Next.
            rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }
    //--If the player gained initiative, then the enemies are not added here, they are added later.
    else
    {
        DebugPrint(" Ambush. Enemies not bucketed yet.\n");
    }

    ///--[Sorting]
    //--All of the buckets now sort the highest initiative to the front.
    DebugPrint("Sorting buckets.\n");
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        tTurnLists[i]->SortListUsing(&AdvCombat::SortByInitiative);
    }

    ///--[Turn Order]
    //--At last, we can now populate the turn order list.
    DebugPrint("Resolving final turn order.\n");
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        TurnOrderRollPack *rPackage = (TurnOrderRollPack *)tTurnLists[i]->PushIterator();
        while(rPackage)
        {
            //--Don't add a KO'd entry.
            if(!rPackage->rEntity->IsDefeated())
            {
                mrTurnOrder->AddElementAsTail(rPackage->rEntity->GetName(), rPackage->rEntity);
            }

            //--Next.
            rPackage = (TurnOrderRollPack *)tTurnLists[i]->AutoIterate();
        }


        /*
        AdvCombatEntity *rPtr = (AdvCombatEntity *)tTurnLists[i]->PushIterator();
        while(rPtr)
        {
            //--Entity is knocked out! Don't add them.
            if(!rPtr->IsDefeated())
            {
                mrTurnOrder->AddElementAsTail(rPtr->GetName(), rPtr);
            }
            rPtr = (AdvCombatEntity *)tTurnLists[i]->AutoIterate();
        }*/
    }

    //--If enemies got ambushed and this is the first turn, place them on the turn order list at the end.
    //  They also receive a special flag, causing them to use the ability "Ambushed" on their first turn.
    //  They also don't get the acts-twice case.
    if(mPlayerGainedInitiative && mCurrentTurn == 0)
    {
        //--Debug.
        DebugPrint("Appending ambushed enemies.\n");

        //--Append.
        AdvCombatEntity *rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEnemyMember)
        {
            rEnemyMember->SetAmbushed(true);
            mrTurnOrder->AddElementAsTail(rEnemyMember->GetName(), rEnemyMember);
            rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }

    ///--[Clean]
    //--Deallocate.
    DebugPrint("Cleaning up.\n");
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        delete tTurnLists[i];
    }

    ///--[Debug]
    if(false)
    {
        //--Header.
        fprintf(stderr, "Turn order report:\n");

        //--List elements.
        int i = 0;
        AdvCombatEntity *rCheckPtr = (AdvCombatEntity *)mrTurnOrder->PushIterator();
        while(rCheckPtr)
        {
            fprintf(stderr, " %i: %s %p - %i %i\n", i, mrTurnOrder->GetIteratorName(), rCheckPtr, rCheckPtr->ComputeTurnBucket(), rCheckPtr->GetCurrentInitiative());
            i ++;
            rCheckPtr = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
        }
    }

    ///--[Finish Up]
    DebugPop("Finished turn order.\n");
}
int AdvCombat::SortByInitiative(const void *pEntryA, const void *pEntryB)
{
    //--Compare the two entities by their initiative. If that fails, use their turn IDs.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Cast the data pointer into an TurnOrderRollPack.
    TurnOrderRollPack *rPackageA = (TurnOrderRollPack *)(*rEntryA)->rData;
    TurnOrderRollPack *rPackageB = (TurnOrderRollPack *)(*rEntryB)->rData;

    //--If the initiatives are identical, compare the turn IDs which cannot be identical.
    int cInitiativeA = rPackageA->mInitiative;
    int cInitiativeB = rPackageB->mInitiative;
    if(cInitiativeA == cInitiativeB)
    {
        return rPackageB->rEntity->GetTurnID() - rPackageA->rEntity->GetTurnID();
    }

    //--Otherwise, just return the comparison.
    return cInitiativeB - cInitiativeA;
}

///======================================= Core Methods ===========================================
void AdvCombat::RecomputeTurnBarLength()
{
    //--Whenever the turn list changes length, call this function to recompute its length and move the icons on it.
    float cTurnOrderWidPerPortrait = 48.0f;

    //--Reset timers.
    mTurnBarMoveTimer = 0;
    mTurnBarMoveTimerMax =15;

    //--Set positions.
    mTurnWidStart = mTurnWidCurrent;
    mTurnWidTarget = (cTurnOrderWidPerPortrait * (mrTurnOrder->GetListSize())) + 10.0f;
}
void AdvCombat::BeginTurnDisplay()
{
    //--Causes the turn display text to appear on screen.
    mTurnDisplayTimer = 0;
}

///========================================== Update ==============================================
void AdvCombat::BeginTurn()
{
    ///--[Documentation]
    //--Called when there are no actors able to act on the turn list. Starts a new turn.
    //  First, Increment the turn counter. Reinitialize() puts it at -1, meaning the first turn is turn 0.
    mCurrentTurn ++;

    ///--[Reinforcements]
    //--World-references.
    WorldRefPack *rPackage = (WorldRefPack *)mWorldReferences->SetToHeadAndReturn();
    while(rPackage)
    {
        //--If the package has a negative turn count, add to the battle.
        rPackage->mTurns --;
        if(rPackage->mTurns == 0)
        {
            rPackage->rActor->AppendEnemiesToCombat(false);
        }

        //--Next.
        rPackage = (WorldRefPack *)mWorldReferences->IncrementAndGetRandomPointerEntry();
    }

    //--Direct reinforcements.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrEnemyReinforcements->SetToHeadAndReturn();
    while(rEntity)
    {
        //--There is a public variable that counts the reinforcement turn for this entity.
        //  If it hits zero, add the enemy to the order of battle.
        int tReinforceTurns = rEntity->GetReinforcementTurns();
        if(tReinforceTurns < 1)
        {
            //--Note: Register before liberating as the unique name is part of the entry, not the entity.
            const char *rUniqueName = mrEnemyReinforcements->GetRandomPointerName();
            mrEnemyCombatParty->AddElementAsTail(rUniqueName, rEntity);

            //--Liberate.
            mrEnemyReinforcements->LiberateRandomPointerEntry();
        }
        else
        {
            rEntity->SetReinforcementTurns(tReinforceTurns - 1);
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrEnemyReinforcements->IncrementAndGetRandomPointerEntry();
    }

    //--If any entities reinforced, position them onscreen.
    PositionEnemies();

    //--Run the response script before turn order is rolled.
    RunGeneralResponse(ADVCOMBAT_RESPONSE_GENERAL_BEGINTURN_PREINITIATIVE);

    //--Run the turn order roller.
    RollTurnOrder();

    //--Once turn order is done, run the response script. It may "prune" the turn order here.
    RunGeneralResponse(ADVCOMBAT_RESPONSE_GENERAL_BEGINTURN_POSTINITIATIVE);

    //--Mark the new turn length.
    RecomputeTurnBarLength();
    mTurnBarMoveTimer = mTurnBarMoveTimerMax;
    mTurnBarNewTurnTimer = 0;
    mTurnBarNewTurnTimerMax = 30;

    //--Reset the turn timer.
    BeginTurnDisplay();

    //--Mark a new action as taking place.
    mIsActionConcluding = false;
    mPlayerInterfaceTimer = 0;

    //--Specify that no action is occurring. This will cause BeginAction() to start after this ends.
    mIsActionOccurring = false;

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_BEGINTURN);

    //--Call the response script again after entities have had a chance to run their scripts.
    RunGeneralResponse(ADVCOMBAT_RESPONSE_GENERAL_BEGINTURN_POSTRESPONSES);

    //--Party compression. It is possible for the turn-begin cases to remove entities from the combat party. If this
    //  happens, remove them from the turn order, and make sure slot 0 of the combat party has someone in it if
    //  someone is in the combat party.

    //--Run the statistics script.
    CallStatisticsScript(ACS_SCRIPT_CODE_TURNSTART);
}
void AdvCombat::BeginAction()
{
    ///--[Documentation and Setup]
    //--An 'Action' is the command of a single character. It is implied that whoever is 0th in the turn order
    //  list is the one performing the action.
    //--Player entities will slide in from the left side of the screen. Enemy entities stack together on the right
    //  side and never exit the playing field.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    ///--[KO'd Party Removal]
    //--Any active party members who were KO'd and have a specific flag set are removed from the party. They are
    //  not deleted yet.
    AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrCombatParty->SetToHeadAndReturn();
    while(rPartyMember)
    {
        //--KO'd and has the flag set:
        if(rPartyMember->IsKnockoutFinished() && rPartyMember->IsRemovedOnKO())
        {
            //--Remove from the combat party.
            mrCombatParty->LiberateRandomPointerEntry();
        }

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrCombatParty->IncrementAndGetRandomPointerEntry();
    }

    ///--[Flags]
    //--Entity resets their free-action counter. This occurs before abilities run their scripts.
    int tFreeActionGen = rActingEntity->GetStatistic(STATS_FREEACTIONGEN);
    rActingEntity->SetFreeActions(tFreeActionGen);
    rActingEntity->SetFreeActionsPerformed(0);

    //--Flag.
    mIsActionConcluding = false;
    mIsActionOccurring = true;
    mActionFirstTick = true;

    //--Reset this timer to zero.
    mPlayerCPTimer = 0;
    mPlayerCPFrameTimer = 0;

    ///--[Movement]
    //--Enemies always reposition.
    PositionEnemies();

    //--Check which party the entity is in. Player party, slide onscreen.
    if(IsEntityInPlayerParty(rActingEntity))
    {
        //--Order all other party members offscreen.
        PositionCharactersOffScreen();

        //--Now order the acting character back onscreen.
        float cIdealX = ComputeIdealX(true, false, 0, 1, NULL);
        rActingEntity->SetIdealPositionX(cIdealX);
        rActingEntity->MoveToIdealPosition(cStdMoveTicks);
    }
    //--Enemy party. Player party stays where it is, and goes on or offscreen based on the entity's action.
    else
    {
    }

    ///--[Responses]
    //--Call the combat script.
    RunGeneralResponse(ADVCOMBAT_RESPONSE_GENERAL_BEGINACTION);

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_BEGINACTION);

    //--Run the statistics script.
    CallStatisticsScript(ACS_SCRIPT_CODE_ACTIONSTART);
}
void AdvCombat::CompleteAction()
{
    ///--[Documentation]
    //--After the previous 'Action' is completed, call this. It advances the turn order by one. This mostly
    //  moves entities offscreen.
    mNoEventsEndsAction = false;
    mIsActionOccurring = false;
    mIsActionConcluding = false;
    mIsSelectingTargets = false;

    //--Get the zeroth entity on the turn order list. This is needed for later.
    void *rZerothTurnCharPtr = mrTurnOrder->GetElementBySlot(0);

    //--Scan the turn order list. If any entities were KO'd, they lose their turn.
    AdvCombatEntity *rKOCheck = (AdvCombatEntity *)mrTurnOrder->SetToHeadAndReturn();
    while(rKOCheck)
    {
        if(rKOCheck->IsDefeated())
        {
            rKOCheck->SetTurnKOd(mCurrentTurn);
            mrTurnOrder->RemoveRandomPointerEntry();
        }
        rKOCheck = (AdvCombatEntity *)mrTurnOrder->IncrementAndGetRandomPointerEntry();
    }

    //--Check if the entity in the 0th slot exists for crash prevention. Entities may be removed by
    //  being defeated.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity)
    {
        PositionCharactersOffScreen();
        return;
    }

    //--Free Action. Same acting entity. Their Free Action counter decreases by 1. Abilities may check
    //  the Free Action counter to see if they have any left, but the turn begins anew here.
    //--Because the acting entity is the same, they don't need to move offscreen. Allies do.
    if(mWasLastActionFree || mWasLastActionEffortless)
    {
        //--Check if the acting entity got KO'd, which may happen due to counterattacks. If this happens,
        //  the turn ends anyway.
        if(rActingEntity->IsDefeated())
        {
            mWasLastActionFree = false;
            mWasLastActionEffortless = false;
        }
        //--Entity is able to act.
        else
        {
            //--Flags.
            mIsActionConcluding = false;
            mIsActionOccurring = true;
            mActionFirstTick = true;

            //--Remove one Free Action. Increment the total actions counter. Effortless actions
            //  do not trigger this code.
            if(!mWasLastActionEffortless)
            {
                int tFreeActions = rActingEntity->GetFreeActions();
                int tFreeActionsPerformed = rActingEntity->GetFreeActionsPerformed();
                rActingEntity->SetFreeActions(tFreeActions - 1);
                rActingEntity->SetFreeActionsPerformed(tFreeActionsPerformed + 1);
            }

            //--Handle all response scripts.
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_BEGINFREEACTION);
            PositionCharactersOffScreen();

            //--Figure out the ideal X position, based on which party the entity is in. Enemy entities
            //  should not need to reposition, player entities need to go offscreen unless they are
            //  the acting entity.
            int tPartyGrouping = GetPartyGroupingID(rActingEntity->GetID());
            if(tPartyGrouping == AC_PARTY_GROUP_PARTY)
            {
                float cIdealX = ComputeIdealX(true, false, 0, 1, NULL);
                rActingEntity->SetIdealPositionX(cIdealX);
                rActingEntity->MoveToIdealPosition(cStdMoveTicks);
            }

            //--Reset flags.
            mWasLastActionFree = false;
            mWasLastActionEffortless = false;
            return;
        }
    }

    //--Order entities to move offscreen. This is bypassed by Free Actions.
    //PositionCharactersOffScreen();

    //--Check the next entity. It can be NULL if that was the last entity in the turn.
    AdvCombatEntity *rNextActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(1);

    //--Null. A new turn will need to begin.
    if(!rNextActingEntity)
    {

    }
    //--If the acting entity and the next acting entity are the same, we don't need to move them:
    else if(rActingEntity == rNextActingEntity)
    {

    }
    //--Otherwise, if the entity was in the player's party, move them offscreen. The next entity will move
    //  onscreen when their action begins, possible at the same time as the other is moving off.
    else if(IsEntityInPlayerParty(rActingEntity) && IsEntityInPlayerParty(rNextActingEntity))
    {
        //--Compute how far offscreen to go. Wider portraits go further.
        float cOffscreenX = ADVCOMBAT_POSITION_OFFSCREEN_LFT;
        StarBitmap *rCombatPortrait = rActingEntity->GetCombatPortrait();
        if(rCombatPortrait) cOffscreenX = cOffscreenX - rCombatPortrait->GetWidth();

        //--Position.
        rActingEntity->SetIdealPositionX(cOffscreenX);
        rActingEntity->MoveToIdealPosition(cStdMoveTicks);
    }
    //--Enemy entity.
    else
    {

    }

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDACTION);

    //--Once that's done, pop the turn order. Note that this happens after effects/abilities, because those need to know
    //  who the previous acting entity was.
    //--This does not happen if that entity was already removed, such as being KO'd.
    void *rNewZerothActor = mrTurnOrder->GetElementBySlot(0);
    if(rZerothTurnCharPtr == rNewZerothActor)
    {
        mrTurnOrder->RemoveElementI(0);
    }

    //--Recompute turn bar length.
    RecomputeTurnBarLength();
}
void AdvCombat::EndTurn()
{
    //--Called when all entities have finished acting and the turn order list has no elements.
    mIsTurnActive = false;

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDTURN);
}
