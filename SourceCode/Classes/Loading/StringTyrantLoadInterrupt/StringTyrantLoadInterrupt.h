///================================= StringTyrantLoadInterrupt ====================================
//--Version of the StarLoadInterrupt built for String Tyrant.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLoadInterrupt.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define MARY_FRAMES 159

///========================================== Classes =============================================
class StringTyrantLoadInterrupt : public StarLoadInterrupt
{
    private:
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    bool mIsReady;
    StarBitmap *mTextLoading;
    StarBitmap *mTextFinished;
    StarLinkedList *mStringTyrantImages;

    protected:

    public:
    //--System
    StringTyrantLoadInterrupt();
    virtual ~StringTyrantLoadInterrupt();
    virtual void Construct(const char *pDatafilePath);

    //--Public Variables
    //--Property Queries
    //--Manipulators

    //--Core Methods
    virtual void DeallocateImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Call(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_BootStringTyrant(lua_State *L);


