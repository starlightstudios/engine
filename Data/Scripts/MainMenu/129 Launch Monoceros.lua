-- |[ ================================ Launch Project Monoceros ================================ ]|
--Hotboot of Pandemonium then into project Monoceros immediately.
local zAdventureGameEntry = SysPaths:fnGetGameEntryValid("Adventure Mode")
if(zAdventureGameEntry == nil) then return end

--Variables.
local sModInternalName = "Monoceros"
local sModAutorunName  = "Monoceros"

-- |[ ==================================== Mod Registration ==================================== ]|
--Run this subroutine to scan for mods. The global gsModDirectories/gsModNames will be populated
-- with mod information.
fnScanMods("../adventure/Games/AdventureMode/")

--OSX hack since the filesystem refuses to compile. Manually specifies the directory. Not portable!
if(LM_GetSystemOS() == "OSX") then
    gsModDirectories = {}
    gsModNames = {}
    gbIsStartingChapter = false
    LM_ExecuteScript("Games/AdventureMode/Chapter C/System/000 Mod Setup.lua")
end
    
-- |[ ==================================== Mod Check / Boot ==================================== ]|
-- |[Mod Boot]|
--Find mod slot.
local iGameSlot = -1
for i = 1, #gsModNames, 1 do
    if(gsModNames[i] == sModInternalName) then
        fnMarkAutorunAsFound(sModAutorunName)
        iGameSlot = i
        break
    end
end

--Mod not found.
if(iGameSlot == -1) then 
    return
end

--Run Pandemonium.
LM_ExecuteScript(zAdventureGameEntry.sActivePath)

--Assemble the boot script name.
local sModBoothPath = gsModDirectories[iGameSlot] .. "System/000 Mod Setup.lua"

--Call the mod boot script.
gbIsStartingChapter = true
LM_ExecuteScript(sModBoothPath)
gbIsStartingChapter = false
