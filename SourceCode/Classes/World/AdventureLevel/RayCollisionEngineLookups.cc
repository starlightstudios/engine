//--Base
#include "RayCollisionEngine.h"

//--Classes
#include "TiledLevel.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///--[Collision Listing]
/*
#define CLIP_NONE 0
#define CLIP_ALL 1
#define CLIP_TL 4
#define CLIP_TOP 5
#define CLIP_TR 6
#define CLIP_DTLE 7
#define CLIP_DTRE 8
#define CLIP_DTLF 9
#define CLIP_DTRF 10
#define CLIP_LFT 14
#define CLIP_NARROWV 15
#define CLIP_RGT 16
#define CLIP_DBLE 17
#define CLIP_DBRE 18
#define CLIP_DBLF 19
#define CLIP_DBRF 20
#define CLIP_BL 24
#define CLIP_BOT 25
#define CLIP_BR 26
#define CLIP_THIN_LF 27
#define CLIP_THIN_RT 28
*/

///--[Blocking Series]
//--Queries to check if the top line of a given point needs to be created. If two collisions are touching
//  one another, neither needs to spawn a line, so these functions greatly speed up line trimming.
//--These functions also handle cases where collisions of different types might still need to spawn lines.
//--For speed, these functions don't range-check. Sanitize your inputs!
bool RayCollisionEngine::IsLftBlocked(int pX, int pY, CollisionPack pCollisionPack)
{
    //--The left is never blocked if the X position is 0.
    if(pX < 1) return false;

    //--Get the collision here and at the query position.
    int cClipComp = pCollisionPack.mCollisionData[pX-1][pY+0];

    //--The most common cases, NONE/ALL, is handled first.
    if(cClipComp == CLIP_NONE) return false;
    if(cClipComp == CLIP_ALL)  return true;

    //--These cases block spawning of a left line.
    if(cClipComp == CLIP_TR)      return true;
    if(cClipComp == CLIP_RGT)     return true;
    if(cClipComp == CLIP_BR)      return true;
    if(cClipComp == CLIP_NARROWV) return true;
    if(cClipComp == CLIP_DTLF)    return true;
    if(cClipComp == CLIP_DBLF)    return true;
    if(cClipComp == CLIP_THIN_RT) return true;

    //--All other cases do not block.
    return false;
}
bool RayCollisionEngine::IsTopBlocked(int pX, int pY, CollisionPack pCollisionPack)
{
    //--The top is never blocked if the Y position is 0.
    if(pY < 1) return false;

    //--Get the collision here and at the query position.
    int cClipComp = pCollisionPack.mCollisionData[pX+0][pY-1];

    //--The most common cases, NONE/ALL, is handled first.
    if(cClipComp == CLIP_NONE) return false;
    if(cClipComp == CLIP_ALL)  return true;

    //--These cases block spawning of a top line.
    if(cClipComp == CLIP_BL)   return true;
    if(cClipComp == CLIP_BOT)  return true;
    if(cClipComp == CLIP_BR)   return true;
    if(cClipComp == CLIP_DTLF) return true;
    if(cClipComp == CLIP_DTRF) return true;

    //--All other cases do not block.
    return false;
}
bool RayCollisionEngine::IsRgtBlocked(int pX, int pY, CollisionPack pCollisionPack)
{
    //--The right is never blocked if the X position is at the maximum.
    if(pX >= pCollisionPack.mCollisionSizeX-1) return false;

    //--Get the collision here and at the query position.
    int cClipComp = pCollisionPack.mCollisionData[pX+1][pY+0];

    //--The most common cases, NONE/ALL, is handled first.
    if(cClipComp == CLIP_NONE) return false;
    if(cClipComp == CLIP_ALL)  return true;

    //--These cases block spawning of a right line.
    if(cClipComp == CLIP_TL)      return true;
    if(cClipComp == CLIP_LFT)     return true;
    if(cClipComp == CLIP_BL)      return true;
    if(cClipComp == CLIP_NARROWV) return true;
    if(cClipComp == CLIP_DTRF)    return true;
    if(cClipComp == CLIP_DBRF)    return true;
    if(cClipComp == CLIP_THIN_LF) return true;

    //--All other cases do not block.
    return false;
}
bool RayCollisionEngine::IsBotBlocked(int pX, int pY, CollisionPack pCollisionPack)
{
    //--The top is never blocked if the Y position is at the maximum.
    if(pY >= pCollisionPack.mCollisionSizeY-1) return false;

    //--Get the collision here and at the query position.
    int cClipComp = pCollisionPack.mCollisionData[pX+0][pY+1];

    //--The most common cases, NONE/ALL, is handled first.
    if(cClipComp == CLIP_NONE) return false;
    if(cClipComp == CLIP_ALL)  return true;

    //--These cases block spawning of a top line.
    if(cClipComp == CLIP_TL)   return true;
    if(cClipComp == CLIP_TOP)  return true;
    if(cClipComp == CLIP_TR)   return true;
    if(cClipComp == CLIP_DBLF) return true;
    if(cClipComp == CLIP_DBRF) return true;

    //--All other cases do not block.
    return false;
}

///--[Creation Series]
//--Creates lines based on the collision type at the given location and registers them to the master list.
//  Once again, for speed reasons, range-checking doesn't occur.
void RayCollisionEngine::CreateLftLine(int pX, int pY, CollisionPack pCollisionPack)
{
    //--Get the collision type at this position.
    int cClipHere = pCollisionPack.mCollisionData[pX][pY];

    //--Fail if this type doesn't need a left line.
    if(cClipHere == CLIP_NONE) return;

    //--Declarations and Constants.
    const float cTileHalf = mTileSize * 0.5f;
    RCELine *nLine = NULL;

    //--These collisions all use the same left line.
    if(cClipHere == CLIP_ALL  || cClipHere == CLIP_TL || cClipHere == CLIP_LFT || cClipHere == CLIP_BL || cClipHere == CLIP_THIN_LF ||
       cClipHere == CLIP_DTRF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Narrow collision. Has two left lines.
    else if(cClipHere == CLIP_NARROWV)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Top Right L collision.
    else if(cClipHere == CLIP_TR)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Right Half collision.
    else if(cClipHere == CLIP_RGT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Top Half collision.
    else if(cClipHere == CLIP_TOP)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Bot Half collision.
    else if(cClipHere == CLIP_BOT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Bottom Right L collision.
    else if(cClipHere == CLIP_BR)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Vertical Narrow, right-side only.
    else if(cClipHere == CLIP_THIN_RT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Diagonals, top left, full and empty. note that BR and TL empty are the same collision.
    else if(cClipHere == CLIP_DTLF || cClipHere == CLIP_DTLE || cClipHere == CLIP_DBRE)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--BLF has both the vertical and diagonal line.
    else if(cClipHere == CLIP_DBRF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
}
void RayCollisionEngine::CreateTopLine(int pX, int pY, CollisionPack pCollisionPack)
{
    //--Get the collision type at this position.
    int cClipHere = pCollisionPack.mCollisionData[pX][pY];

    //--Fail if this type doesn't need a top line.
    if(cClipHere == CLIP_NONE) return;

    //--Declarations and Constants.
    const float cTileHalf = mTileSize * 0.5f;
    RCELine *nLine = NULL;

    //--These collisions all use the same top line.
    if(cClipHere == CLIP_ALL  || cClipHere == CLIP_TL || cClipHere == CLIP_TOP || cClipHere == CLIP_TR ||
       cClipHere == CLIP_DBLF || cClipHere == CLIP_DBRF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--These collision use left-half top lines.
    else if(cClipHere == CLIP_LFT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--These collision use right-half top lines.
    else if(cClipHere == CLIP_RGT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Bottom left. Two lines.
    else if(cClipHere == CLIP_BL)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Bottom right. Two lines.
    else if(cClipHere == CLIP_BR)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Narrow vertical. Two sets of lines.
    else if(cClipHere == CLIP_NARROWV)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Vertical Narrow, left-side only.
    else if(cClipHere == CLIP_THIN_LF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Vertical Narrow, right-side only.
    else if(cClipHere == CLIP_THIN_RT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Half clip, bottom.
    else if(cClipHere == CLIP_BOT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
}
void RayCollisionEngine::CreateRgtLine(int pX, int pY, CollisionPack pCollisionPack)
{
    //--Get the collision type at this position.
    int cClipHere = pCollisionPack.mCollisionData[pX][pY];

    //--Fail if this type doesn't need a right line.
    if(cClipHere == CLIP_NONE) return;

    //--Declarations and Constants.
    const float cTileHalf = mTileSize * 0.5f;
    RCELine *nLine = NULL;

    //--These collisions all use the same right line.
    if(cClipHere == CLIP_ALL  || cClipHere == CLIP_TR || cClipHere == CLIP_RGT || cClipHere == CLIP_BR || cClipHere == CLIP_THIN_RT ||
       cClipHere == CLIP_DTLF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Narrow collision. Has two right lines.
    else if(cClipHere == CLIP_NARROWV)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Top Left L collision.
    else if(cClipHere == CLIP_TL)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Left Half collision.
    else if(cClipHere == CLIP_LFT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Top Half collision.
    else if(cClipHere == CLIP_TOP)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Bot Half collision.
    else if(cClipHere == CLIP_BOT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Bottom Left L collision.
    else if(cClipHere == CLIP_BL)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Vertical Narrow, right-side only.
    else if(cClipHere == CLIP_THIN_LF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Diagonals, top right, full and empty. note that TR and BL empty are the same collision.
    else if(cClipHere == CLIP_DTRF || cClipHere == CLIP_DTRE || cClipHere == CLIP_DBLE)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--BLF has both the vertical and diagonal line.
    else if(cClipHere == CLIP_DBLF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+1) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
}
void RayCollisionEngine::CreateBotLine(int pX, int pY, CollisionPack pCollisionPack)
{
    //--Get the collision type at this position.
    int cClipHere = pCollisionPack.mCollisionData[pX][pY];

    //--Fail if this type doesn't need a bottom line.
    if(cClipHere == CLIP_NONE) return;

    //--Declarations and Constants.
    const float cTileHalf = mTileSize * 0.5f;
    RCELine *nLine = NULL;

    //--These collisions all use the same bottom line.
    if(cClipHere == CLIP_ALL  || cClipHere == CLIP_BL || cClipHere == CLIP_BOT || cClipHere == CLIP_BR ||
       cClipHere == CLIP_DTLF || cClipHere == CLIP_DTRF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--These collision use left-half bottom lines.
    else if(cClipHere == CLIP_LFT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--These collision use right-half bottom lines.
    else if(cClipHere == CLIP_RGT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Top left. Two lines.
    else if(cClipHere == CLIP_TL)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Top right. Two lines.
    else if(cClipHere == CLIP_TR)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+0) * mTileSize) + cTileHalf;
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Narrow vertical. Two sets of lines.
    else if(cClipHere == CLIP_NARROWV)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Vertical Narrow, left-side only.
    else if(cClipHere == CLIP_THIN_LF)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+0) * mTileSize) + 3.0f;
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Vertical Narrow, right-side only.
    else if(cClipHere == CLIP_THIN_RT)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize) + 13.0f;
        nLine->mYA = ((pY+1) * mTileSize);
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+1) * mTileSize);
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
    //--Half clip, top.
    else if(cClipHere == CLIP_TOP)
    {
        SetMemoryData(__FILE__, __LINE__);
        nLine = (RCELine *)starmemoryalloc(sizeof(RCELine));
        nLine->mXA = ((pX+0) * mTileSize);
        nLine->mYA = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mXB = ((pX+1) * mTileSize);
        nLine->mYB = ((pY+0) * mTileSize) + cTileHalf;
        nLine->mLastRayPulse = 0;
        nLine->RecomputeAngle();
        mMasterLineList->AddElement("X", nLine, &FreeThis);
    }
}
