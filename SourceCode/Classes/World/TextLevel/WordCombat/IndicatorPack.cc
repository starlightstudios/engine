//--Base
#include "IndicatorPack.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

///========================================== System ==============================================
void IndicatorPack::Initialize()
{
    //--Text Render
    mColor = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    rRenderFont = NULL;
    mX = 0.0f;
    mY = 0.0f;
    mFlags = 0;
    mSize = 1.0f;
    memset(mSentence, 0, sizeof(mSentence));

    //--Update
    mTimer = 0;
    mTimerMax = 1;
    mUpdateType = IP_UPDATE_NONE;
}

///===================================== Property Queries =========================================
bool IndicatorPack::IsComplete()
{
    return (mTimer >= mTimerMax);
}

///======================================= Manipulators ===========================================
void IndicatorPack::SetColor(StarlightColor pColor)
{
    mColor = pColor.Clone();
}
void IndicatorPack::SetColor(float pRed, float pGreen, float pBlue, float pAlpha)
{
    mColor = StarlightColor::MapRGBAF(pRed, pGreen, pBlue, pAlpha);
}
void IndicatorPack::SetFont(StarFont *pFont)
{
    rRenderFont = pFont;
}
void IndicatorPack::SetPosition(float pX, float pY)
{
    mX = pX;
    mY = pY;
}
void IndicatorPack::SetFlags(int pFlags)
{
    mFlags = pFlags;
}
void IndicatorPack::SetSize(float pSize)
{
    mSize = pSize;
    if(mSize <= 0.0f) mSize = 1.0f;
}
void IndicatorPack::SetString(const char *pString)
{
    strncpy(mSentence, pString, sizeof(mSentence));
}
void IndicatorPack::SetCurTimer(int pTicks)
{
    mTimer = pTicks;
    if(mTimer < 0) mTimer = 0;
}
void IndicatorPack::SetMaxTimer(int pTicks)
{
    mTimerMax = pTicks;
    if(mTimerMax < 1) mTimerMax = 1;
}
void IndicatorPack::SetUpdateNone()
{
    mUpdateType = IP_UPDATE_NONE;
}
void IndicatorPack::SetUpdateFade()
{
    mUpdateType = IP_UPDATE_FADE;
}
void IndicatorPack::SetUpdateFloat(float pYDist)
{
    mUpdateType = IP_UPDATE_FLOAT_UP;
    mYFloatUpMax = pYDist;
}
void IndicatorPack::SetUpdateFloatFadeout(float pYDist)
{
    mUpdateType = IP_UPDATE_FLOAT_UP_FADE;
    mYFloatUpMax = pYDist;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void IndicatorPack::Update()
{
    //--Run the timer.
    mTimer ++;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void IndicatorPack::Render()
{
    //--No font, don't render.
    if(!rRenderFont || mSentence[0] == '\0') return;

    //--Storage
    StarlightColor tColor = mColor.Clone();
    float tX = mX;
    float tY = mY;

    //--When using no special properties, render as normal:
    if(mUpdateType == IP_UPDATE_NONE)
    {
    }
    //--Fades out a few ticks before completion.
    else if(mUpdateType == IP_UPDATE_FADE)
    {
        if(mTimer < 15)
        {
            float tPercent = EasingFunction::QuadraticIn(mTimer, 15);
            tColor.a = (mColor.a * tPercent);
        }
        else if(mTimerMax >= 16 && mTimer >= mTimerMax - 15)
        {
            int cUseTimer = mTimer - (mTimerMax - 15);
            float tPercent = EasingFunction::QuadraticOut(cUseTimer, 15);
            tColor.a = mColor.a * (1.0f - tPercent);
        }
    }
    //--Floats up:
    else if(mUpdateType == IP_UPDATE_FLOAT_UP)
    {
        float tPercent = EasingFunction::QuadraticOut(mTimer, mTimerMax);
        tY = tY - (mYFloatUpMax * tPercent);
    }
    //--Floats up, fades out a few ticks before completion.
    else if(mUpdateType == IP_UPDATE_FLOAT_UP_FADE)
    {
        //--Move.
        float tPercent = EasingFunction::QuadraticOut(mTimer, mTimerMax);
        tY = tY - (mYFloatUpMax * tPercent);

        //--Alpha.
        if(mTimer < 15)
        {
            float tPercent = EasingFunction::QuadraticIn(mTimer, 15);
            tColor.a = (mColor.a * tPercent);
        }
        else if(mTimerMax >= 16 && mTimer >= mTimerMax - 15)
        {
            int cUseTimer = mTimer - (mTimerMax - 15);
            float tPercent = EasingFunction::QuadraticOut(cUseTimer, 15);
            tColor.a = mColor.a * (1.0f - tPercent);
        }
    }

    //--Render.
    tColor.SetAsMixer();
    rRenderFont->DrawText(tX, tY, mFlags, mSize, mSentence);
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
