//--Base
#include "AdventureLevel.h"

//--Classes
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "EntityManager.h"

///--[Core Methods]
void AdventureLevel::EmulateEnemyMovement()
{
    ///--[Documentation and Setup]
    //--When a level loads, this function is called, and all enemies on the field will run their patrol movement logic
    //  for a random number of ticks. This causes enemies to be in random, but still deterministic, positions every time.
    //--If this flag is set during the constructor, don't randomize positions.
    if(mDisablePatrolRandomization) return;

    ///--[Build List]
    //--Build a list of all TilemapActors who are enemies.
    StarLinkedList *trEnemyList = new StarLinkedList(false);
    StarLinkedList *rGlobalEntityList = EntityManager::Fetch()->GetEntityList();

    //--Iterate.
    RootEntity *rEntity = (RootEntity *)rGlobalEntityList->PushIterator();
    while(rEntity)
    {
        //--Must be a tilemap actor, in enemy mode.
        if(rEntity->GetType() == POINTER_TYPE_TILEMAPACTOR)
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rEntity;

            //--Check if it's an enemy.
            if(rActor->IsEnemy("Any"))
            {
                trEnemyList->AddElementAsTail("X", rActor);
            }
        }

        //--Next.
        rEntity = (RootEntity *)rGlobalEntityList->AutoIterate();
    }

    ///--[Run Updates]
    //--Order all entities on the list to run their update routine. Note that enemies do not run the updates individually,
    //  but rather, all entities update in sequence. This is because enemies that are following other enemies need to maintain
    //  their patrol routes.
    mEnemyAICycles = 0;
    xRunEnemyUpdateForPatrolScatter = true;

    //--Roll how many ticks to emulate.
    int tScatterRoll = (rand() % 2400) + 120;

    //--Set a maximum time of 5 seconds. If there's a ton of enemy patrols, they will stop running path AIs after 5 seconds
    //  elapses to make sure the game doesn't hang.
    Global::Shared()->gTickWarnTime = Global::Shared()->gTickStartTime + (5.0f);

    //--Run.
    for(int i = 0; i < tScatterRoll; i ++)
    {
        //--Iterate.
        TilemapActor *rEnemy = (TilemapActor *)trEnemyList->PushIterator();
        while(rEnemy)
        {
            rEnemy->Update();
            rEnemy = (TilemapActor *)trEnemyList->AutoIterate();
        }
    }

    ///--[Clean]
    //--Reset flag.
    xRunEnemyUpdateForPatrolScatter = false;

    //--Clean memory.
    delete trEnemyList;
}

///--[Rendering]
void AdventureLevel::RenderPatrolNodes()
{
    //--Setup.
    float cSize = 3.0f;
    glLineWidth(3.0f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_LINES);
    StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);

    //--Begin rendering.
    PatrolNodePack *rPatrolNode = (PatrolNodePack *)mPatrolNodes->PushIterator();
    while(rPatrolNode)
    {
        //--Top.
        glVertex2f(rPatrolNode->mDimensions.mXCenter - cSize, rPatrolNode->mDimensions.mYCenter - cSize);
        glVertex2f(rPatrolNode->mDimensions.mXCenter + cSize, rPatrolNode->mDimensions.mYCenter - cSize);

        //--Right.
        glVertex2f(rPatrolNode->mDimensions.mXCenter + cSize, rPatrolNode->mDimensions.mYCenter - cSize);
        glVertex2f(rPatrolNode->mDimensions.mXCenter + cSize, rPatrolNode->mDimensions.mYCenter + cSize);

        //--Bottom.
        glVertex2f(rPatrolNode->mDimensions.mXCenter + cSize, rPatrolNode->mDimensions.mYCenter + cSize);
        glVertex2f(rPatrolNode->mDimensions.mXCenter - cSize, rPatrolNode->mDimensions.mYCenter + cSize);

        //--Left.
        glVertex2f(rPatrolNode->mDimensions.mXCenter - cSize, rPatrolNode->mDimensions.mYCenter + cSize);
        glVertex2f(rPatrolNode->mDimensions.mXCenter - cSize, rPatrolNode->mDimensions.mYCenter - cSize);

        //--Next.
        rPatrolNode = (PatrolNodePack *)mPatrolNodes->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glLineWidth(1.0f);
}
