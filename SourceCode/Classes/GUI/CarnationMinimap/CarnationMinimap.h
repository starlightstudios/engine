///===================================== CarnationMinimap =========================================
//--Minimap feature, acts as a GUI object held by the CarnationLevel but can be expanded to show
//  a larger map.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///--[CMiniRoomEntry]
//--Part of the minimap laid over the base map. Has an offset, used for rooms that are randomly generated.
typedef struct CMiniRoomEntry
{
    //--Members
    float mXOffset;
    float mYOffset;
    StarLinkedList *mrImageList; //StarBitmap *, reference

    //--Functions
    void Initialize();
    void Set(float pX, float pY, const char *pDLPath);
    void Add(const char *pDLPath);
    void Render();
    static void DeleteThis(void *pPtr);
}CMiniRoomEntry;

///--[DiscoveryEntry]
//--Entry on the map to indicate the places the player has explored. Has timers for rendering.
typedef struct DiscoveryEntry
{
    //--Members
    bool mIsDiscovered;
    int mTimer;

    //--Functions
    void Initialize()
    {
        mIsDiscovered = false;
        mTimer = 0;
    }
}DiscoveryEntry;

///--[Indicator]
//--Appears on the map and represents something. Items, gates, special locations, etc.
typedef struct CMiniIndicator
{
    int mX;
    int mY;
    int mCode;
    StarBitmap *rImage;
    void Initialize()
    {
        mX = 0;
        mY = 0;
        mCode = 0;
        rImage = NULL;
    }
}CMiniIndicator;

///===================================== Local Definitions ========================================
///--[Timers]
#define CARNMINIMAP_DISCOVER_TICKS 30
#define CARNMINIMAP_MAP_MOVE_TICKS 15
#define CARNMINIMAP_MAP_SCALE_TICKS 15

///--[Discovery Spacing]
#define CARNMINIMAP_DISCOVERY_BLOCK_W 8.0f
#define CARNMINIMAP_DISCOVERY_BLOCK_H 8.0f

///--[Indicators]
#define CARNMINIMAP_INDICATOR_NONE 0
#define CARNMINIMAP_INDICATOR_ENEMY 1
#define CARNMINIMAP_INDICATOR_GATE 2
#define CARNMINIMAP_INDICATOR_TREASURE 3
#define CARNMINIMAP_INDICATOR_EVENT 4

///========================================== Classes =============================================
class CarnationMinimap
{
    private:
    ///--[System]

    ///--[Discovery]
    int mDiscoverySizeX;
    int mDiscoverySizeY;
    DiscoveryEntry **mDiscoveryGrid;

    ///--[Player]
    int mPlayerX;
    int mPlayerY;

    ///--[Indicators]
    StarLinkedList *mIndicatorList; //CMiniIndicator *, master

    ///--[Scaling]
    EasingPack1D mCenterPointX;
    EasingPack1D mCenterPointY;
    EasingPack1D mScalePack;

    ///--[Images]
    //--Current Image
    StarBitmap *rCurrentMapImg;
    CMiniRoomEntry *rLastMapEntry;
    StarLinkedList *mMapLayerList; //CMiniRoomEntry *, master

    //--Common Images
    struct
    {
        bool mIsReady;
        struct
        {
            StarBitmap *rUtilityBlackout;
            StarBitmap *rUtilityBlackoutStrict;
            StarBitmap *rUtilityPlayer;
            StarBitmap *rUtilityEnemy;
            StarBitmap *rUtilityEvent;
            StarBitmap *rUtilityGate;
            StarBitmap *rUtilityTreasure;
        }Data;
    }Images;

    protected:

    public:
    //--System
    CarnationMinimap();
    ~CarnationMinimap();

    //--Public Variables
    //--Property Queries
    int GetPlayerX();
    int GetPlayerY();

    //--Manipulators
    void SetPlayerPosition(int pX, int pY);
    void SetMapImage(const char *pDLPath);
    void SetDiscoverySize(int pXSize, int pYSize);
    void DiscoverPoint(int pX, int pY);
    void DiscoverArea(int pLft, int pTop, int pRgt, int pBot);
    void CreateMapLayer(float pXOffset, float pYOffset, const char *pDLPath);
    void AddImageToLastLayer(const char *pDLPath);
    void RegisterIndicator(const char *pName, int pX, int pY, int pCode);
    void RemoveIndicator(const char *pName);

    //--Core Methods
    void ClearMapLayers();
    void ClearIndicators();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render(float pAlpha);
    void RenderBase(float pAlpha);
    void RenderShadowing(float pAlpha);
    void RenderShadowingStrict();

    //--Pointer Routing
    //--Static Functions
    static CarnationMinimap *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnMinimap_SetProperty(lua_State *L);
