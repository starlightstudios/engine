///========================================== UString =============================================
//--Represents a unicode string, typically using Allegro's unicode handlers. May not be
//  compatible with all StarFont operations!
//--This is completely disabled in SDL until I figure it out.
//TODO: That.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class UString
{
    #if defined _ALLEGRO_PROJECT_

    private:
    //--System
    ALLEGRO_USTR *mUString;

    protected:

    public:
    //--System
    UString();
    ~UString();

    //--Public Variables
    //--Property Queries
    ALLEGRO_USTR *GetRaw();
    const char *GetCString();
    int GetLength();
    int GetLastSpace();

    //--Manipulators
    void Set(const char *pCString);
    void Cat(int32_t pLetter);
    void Insert(int pPos, const char *pCString);

    //--Core Methods
    void Clear();
    UString *PullOffXChars(int pCharacters);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking

    #endif
};

//--Hooking Functions

