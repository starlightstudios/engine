//--Base
#include "GameGear.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "FlexMenu.h"
#include "TilemapActor.h"
#include "VisualLevel.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DisplayManager.h"
#include "DebugManager.h"
#include "EntityManager.h"
#include "ResetManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "MemoryManager.h"
#include "SteamManager.h"
#include "StarLumpManager.h"

#if defined _STEAM_API_
    #include "steam_api.h"
#endif

//--Code common to both gears regardless of library used.
void GameGear::PostLogicHandler()
{
    ///--[Setup]
    //--Fast-access pointers.
    GLOBAL *rGlobal = Global::Shared();
    ControlManager *rControlManager = rGlobal->gControlManager;
    EntityManager *rEntityManager = rGlobal->gEntityManager;

    ///--[Control Handlers]
    //--Run end-of-tick to drop duplicate keypresses.
    rControlManager->PostUpdate();
    rEntityManager->PostUpdate();

    ///--[Audio Handlers]
    //--Audio handling. Stops any music that's playing but at zero volume.
    rGlobal->gAudioManager->Update();
    AdvCombatAbility::HandleCombatActionSounds();

    ///--[Turn Handlers]
    //--Handle turn controls. This is not the same as the usual Update cycle. Turns concern the
    //  actions of entities, Update cycles concern things like animations on a per-tick basis.
    rEntityManager->UpdateTurns();

    //--Visual Level creates rendering packs for entities who are leaving the room.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(rVisualLevel) rVisualLevel->UpdateVisiblityTimers();

    ///--[Flag Reset]
    //--Reset this cutscene flag back to 1 after every tick.
    CutsceneManager::xUpdatesPerEntityThisTick = CM_DEFAULT_CUTSCENE_CYCLES;
    CutsceneManager::xTimerUpdatesThisTick = CM_DEFAULT_CUTSCENE_CYCLES;

    //--Reset actor path pulses to zero.
    TilemapActor::xActorPathPulsesThisTick = 0;

    ///--[Mouse Locking]
    //--DisplayManager may optionally keep the mouse within the display here.
    rGlobal->gDisplayManager->UpdateMouseLock();

    ///--[Clear/Reset Controllers]
    //--To prevent instability, some objects can clear themselves after the tick is over. This way,
    //  an entity on the EM can clear the EM without deleting itself in the middle of its update.
    if(rEntityManager->mHasPendingClear)
    {
        rEntityManager->mHasPendingClear = false;
        rEntityManager->ClearAll();
    }

    ///--[Reset Handler]
    //--If the global shows a pending game reset, do so here.
    if(rGlobal->gReset)
    {
        //--Flip the flag off.
        rGlobal->gReset = false;

        //--Issue the reset.
        rGlobal->gResetManager->IssueReset("Full Game Reset");
        rGlobal->gResetManager->IssueReset("Combat");
        rGlobal->gMapManager->BeginFade();

        //--Sound settings.
        rGlobal->gAudioManager->ChangeMusicVolume(0.75f - AudioManager::xMusicVolume);
        rGlobal->gAudioManager->ChangeSoundVolume(0.75f - AudioManager::xSoundVolume);
    }

    ///--[Back-To-Title Key]
    //--If this key is presse, immediately resets the game back to title. This is a "provided as a convenience" key
    //  and bugs related to it are the player's fault.
    if(rGlobal->gControlManager->IsFirstPress("GUI|BackToTitle"))
    {
        //--Increment and reset the timer.
        rGlobal->mResetKeyPresses ++;
        rGlobal->mResetKeyCooldown = 120;

        //--If the cap is reached, reset to title.
        if(rGlobal->mResetKeyPresses >= 5) rGlobal->gMapManager->mBackToTitle = true;
    }

    //--Decrement the reset cooldown.
    if(rGlobal->mResetKeyCooldown > 0)
    {
        rGlobal->mResetKeyCooldown --;
    }
    else
    {
        rGlobal->mResetKeyPresses = 0;
    }

    ///--[Title Reset]
    //--If the MapManager has this flag set, it's quitting out of Adventure Mode and returning to the title screen.
    if(rGlobal->gMapManager->mBackToTitle)
    {
        //--Unset the flag.
        rGlobal->gMapManager->mBackToTitle = false;

        //--Issue a full reset.
        rGlobal->gResetManager->IssueReset("Full Game Reset");
        rGlobal->gResetManager->IssueReset("Combat");
        rGlobal->gMapManager->BeginFade();

        //--Also clear the DataLibrary of any pending values.
        rGlobal->gDataLibrary->Purge("Root/Variables/");

        //--Order Lua manager to clear addendums.
        LuaManager::Fetch()->ClearAddendums();

        //--Sound settings.
        //rGlobal->gAudioManager->ChangeMusicVolume(0.75f - AudioManager::xMusicVolume);
        //rGlobal->gAudioManager->ChangeSoundVolume(0.75f - AudioManager::xSoundVolume);

        //--If the menu stack has nothing on it, push something.
        /*
        if(!rGlobal->gMapManager->MenuStackHasContents())
        {
            FlexMenu *nFlexMenu = new FlexMenu();
            MapManager::Fetch()->PushMenuStack(nFlexMenu);
            LuaManager::Fetch()->PushExecPop(nFlexMenu, "Data/Scripts/MainMenu/000 PopulateMainMenu.lua");
            //MapManager::Fetch()->BeginFade();
        }*/
    }

    ///--[Shader Handler]
    //--Recompilation of shaders, if flagged.
    if(rGlobal->gControlManager->IsFirstPress("RecompileShaders"))
    {
        rGlobal->gDisplayManager->ClearAllShaderData();
        rGlobal->gDisplayManager->ShaderExec();
    }

    ///--[Kerning Handler]
    //--Recompilation of kernings, if flagged.
    if(rGlobal->gControlManager->IsFirstPress("RebuildKerning"))
    {
        DataLibrary::Fetch()->RebuildAllKerning();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Diagnostics]
    //--Prints a diagnostics report. Used for debug.
    if(rGlobal->gControlManager->IsFirstPress("Diagnostics"))
    {
        DataLibrary::PrintDiagnostics();
    }

    ///--[Visual Tracer]
    //--Prints a diagnostics report. Used for debug.
    if(rGlobal->gControlManager->IsFirstPress("ToggleVisTrace"))
    {
        DebugManager::xPrintNextVisTrace = 2;
        if(DebugManager::xVisTraceGlobalFlag > 0)
        {
            if(DebugManager::xVisTraceGlobalFlag < 0x01000000)
            {
                uint32_t tFlag = 1;
                while((DebugManager::xVisTraceGlobalFlag & tFlag) != 0)
                {
                    tFlag = tFlag * 2;
                }

                DebugManager::xVisTraceGlobalFlag = (tFlag) + (tFlag-1);
            }
            else
            {
                DebugManager::xVisTraceGlobalFlag = 0;
            }
            //fprintf(stderr, "New: %i\n", DebugManager::xVisTraceGlobalFlag);
        }
        else
        {
            DebugManager::xVisTraceGlobalFlag = 1;
            DebugManager::xPrintNextVisTrace = 2;
            //fprintf(stderr, "Set: %i\n", DebugManager::xVisTraceGlobalFlag);
        }
    }

    ///--[Cutscene Handler]
    //--Drop any and all events on the drop list.
    rGlobal->gCutsceneManager->DropListAtEndOfTick();

    ///--[Garbage Cleanup]
    //--Garbage handling.
    Memory::Tick();

    ///--[FMOD]
    //--If using FMOD, it needs to periodically do sound upkeep.
    #if defined _FMOD_AUDIO_
        FMOD_System_Update(AudioManager::xFMODSystem);
    #endif

    ///--[Steam]
    //--Steam API.
    #if defined _STEAM_API_
    if(SteamManager::Fetch()->IsSteamRunning())
    {
        SteamManager::Fetch()->PumpCallbackStack();
        SteamManager::Fetch()->RequestStats();
    }
    #endif

    ///--[Delayed Loading]
    //--If the StarLumpManager has anything in its delayed loading list, load it here until we run out of time.
    //  Ideally, the time in a tick is kept under 90% to keep the game running smoothly. The average time of
    //  the last three loading actions is taken together to guess at the time needed for the next one.
    //--There will always be at least one load action no matter what, to make sure the texture pop doesn't
    //  get jammed by a slow machine. If the list is empty, LoadNextDelayedBitmap() will return false.
    int tLoadsTotal = 0;
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    float cTimeBeforeLoad = GetGameTime();
    if(rSLM->LoadNextDelayedBitmap())
    {
        //--Setup.
        float cLoadTimes[3];
        float cSecondsPerTick = 1.0f / 60.0f;
        float tAvgLoadTime = 0.0f;
        float cHardUpperLimit = rGlobal->gTickStartTime + cSecondsPerTick;

        //--Static tracking.
        StarBitmap::xTicksWithPendingBitmaps ++;

        //--Count a load.
        tLoadsTotal ++;

        //--The load times for the first load are populated into all three slots initially, giving
        //  the average equalling the initial time.
        float tCurTime = GetGameTime();
        float cFirstLoadTime = tCurTime - cTimeBeforeLoad;
        cLoadTimes[0] = cFirstLoadTime;
        cLoadTimes[1] = cFirstLoadTime;
        cLoadTimes[2] = cFirstLoadTime;
        tAvgLoadTime = cFirstLoadTime;

        //--As long as there is time left, and something to load, keep loading. We pad by 3x the loading time
        //  just in case.
        while(tCurTime + (tAvgLoadTime * 3.0f) < cHardUpperLimit && rSLM->LoadNextDelayedBitmap())
        {
            //--A load executed. Get the current time.
            float tNewCurTime = GetGameTime();
            tLoadsTotal ++;

            //--Move the time back in the array, update the average.
            cLoadTimes[2] = cLoadTimes[1];
            cLoadTimes[1] = cLoadTimes[0];
            cLoadTimes[0] = tNewCurTime - tCurTime;
            tCurTime = tNewCurTime;
        }
    }
    else
    {
        StarBitmap::xTicksWithPendingBitmaps = 0;
    }

    ///--[Bitmap Loading Report]
    //--Print a report if 10 ticks elapse with bitmaps pending.
    if(StarBitmap::xTicksWithPendingBitmaps >= 10 && false)
    {
        DebugManager::ForcePrint("10 ticks elapsed with bitmaps pending load.\n");
        DebugManager::ForcePrint(" Total bitmaps in existence: %i\n", StarBitmap::xTotalBitmaps);
        DebugManager::ForcePrint(" Bitmaps on loading queue: %i\n", StarLumpManager::Fetch()->GetSizeOfLoadQueue());
        DebugManager::ForcePrint(" Total pixels currently uploaded: %i\n", StarBitmap::xTotalPixels);
        StarBitmap::xTicksWithPendingBitmaps = 0;
    }

    ///--[Debug]
    if(tLoadsTotal > 0 && false)
    {
        //--Get values.
        float tCurTime = GetGameTime();
        float tTickElapsedTime = tCurTime - rGlobal->gTickStartTime;
        float tSecondsPerTick = 1.0f / 60.0f;
        float tTimeConsumed = tSecondsPerTick - tTickElapsedTime;
        float tPercentConsumed = 1.0f - (tTimeConsumed / tSecondsPerTick);

        //--Print
        fprintf(stderr, "Loaded graphics during tick downtime.\n");
        fprintf(stderr, " Loaded %i images.\n", tLoadsTotal);
        fprintf(stderr, " Tick took %f seconds to elapse. (%f left) (%i%% used)\n", tTickElapsedTime, tTimeConsumed, (int)(tPercentConsumed * 100.0f));
    }

    ///--[Diagnostics]
    //--If true, report when a tick goes over the alotted time.
    if(false)
    {
        float tCurTime = GetGameTime();
        float tTickElapsedTime = tCurTime - rGlobal->gTickStartTime;
        float tSecondsPerTick = 1.0f / 60.0f;
        if(tTickElapsedTime > tSecondsPerTick)
        {
            fprintf(stderr, "Tick went over alotted time: %f to %f\n", tTickElapsedTime, tSecondsPerTick);
        }
    }
}
