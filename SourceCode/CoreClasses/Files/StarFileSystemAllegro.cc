//--Base
#include "StarFileSystem.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///===================================== Allegro-Only Code ========================================
#if defined _ALLEGRO_FILESYSTEM_
StarLinkedList *StarFileSystem::SubScanDirectory(const char *pPath, bool pScanDirectories)
{
    ///--[Documentation]
    //--Scanning subroutine. The linked-list passed back will contain FileInfo's for every single entry
    //  within the folder. If pScanDirectories is true, this will recursively be called for each
    //  sub-directory within.
    //--Check for NULL on failure.
    if(!pPath) return NULL;

    ///--[Common Code]
    //--Create the file info list. It does not delete its members.
    StarLinkedList *nFileInfoList = new StarLinkedList(false);

    //--0th element is always the "../" pathname.  It means "up one directory".
    //  If a flag is set, we don't create this.
    if(!mIgnoreZerothEntry && !mIsThisARecursion)
    {
        char tBuffer[STD_PATH_LEN];
        strcpy(tBuffer, pPath);
        strcat(tBuffer, "..");

        SetMemoryData(__FILE__, __LINE__);
        FileInfo *nEntry = (FileInfo *)starmemoryalloc(sizeof(FileInfo));
        nEntry->Initialize();
        nEntry->mIsDirectory = true;
        nEntry->mIsFile = false;
        nEntry->mPath = NULL;
        ResetString(nEntry->mPath, tBuffer);
        nFileInfoList->AddElement("X", nEntry, &DontDeleteThis);
    }

    ///--[Setup]
    //--Root Entry
    ALLEGRO_FS_ENTRY *tRootEntry = al_create_fs_entry(pPath);
    bool tDirectoryOpened = al_open_directory(tRootEntry);

    //--Store the absolute path of the directory.  Ignore this if this is a recursion.
    if(!mIsThisARecursion)
    {
        //--Temporary buffer.
        char tBuffer[512];
        strcpy(tBuffer, al_get_fs_entry_name(tRootEntry));

        //--Remove '\' and replace with '/'
        for(unsigned int i = 0; i < strlen(tBuffer); i ++)
        {
            if(tBuffer[i] == '\\') tBuffer[i] = '/';
        }

        //--This string needs to have a '/' added onto the end since it's guaranteed to be a
        //  directory.  Add that.
        strcat(tBuffer, "/");

        //--Save the final copy.
        ResetString(mLastParsedDirectory, tBuffer);
    }

    ///--[Directory Did Not Open]
    if(!tDirectoryOpened)
    {
        delete nFileInfoList;
        return NULL;
    }

    ///--[Iterate]
    //--Read all the entries, and store the filenames.
    ALLEGRO_FS_ENTRY *tEntry = al_read_directory(tRootEntry);
    while(tEntry)
    {
        ///--[Setup]
        //--Check flags.
        int32_t tFlags = al_get_fs_entry_mode(tEntry);

        ///--[Recursion]
        //--If this is a directory, and we want to recurse, do that now.
        if(tFlags & ALLEGRO_FILEMODE_ISDIR && pScanDirectories)
        {
            //--Set this flag to false, otherwise we will have duplicate ../ entries.
            mIsThisARecursion = true;

            //--Build it.
            StarLinkedList *tSubscanList = SubScanDirectory(al_get_fs_entry_name(tEntry), pScanDirectories);

            //--Copy all the entries down onto our list.
            FileInfo *rScanInfo = (FileInfo *)tSubscanList->PushIterator();
            while(rScanInfo)
            {
                //--Name
                char *tName = tSubscanList->GetIteratorName();
                nFileInfoList->AddElement(tName, rScanInfo, &DontDeleteThis);

                rScanInfo = (FileInfo *)tSubscanList->AutoIterate();
            }

            //--Clean
            delete tSubscanList;
            mIsThisARecursion = false;
        }

        ///--[Store Entry]
        //--Is this a directory, when we are ignoring directories? If so, don't create a heap
        //  copy, and don't store it.
        if(tFlags & ALLEGRO_FILEMODE_ISDIR && mIgnoreFolders)
        {

        }
        //--Normal behavior.
        else
        {
            //--Buffer.
            char tBuffer[512];
            strcpy(tBuffer, al_get_fs_entry_name(tEntry));

            //--Remove '\' and replace with '/'
            for(unsigned int i = 0; i < strlen(tBuffer); i ++)
            {
                if(tBuffer[i] == '\\') tBuffer[i] = '/';
            }

            //--Tack on a '/' on the end, to indicate this is a directory.
            if(tFlags & ALLEGRO_FILEMODE_ISDIR) strcat(tBuffer, "/");

            //--Create a heap copy.
            SetMemoryData(__FILE__, __LINE__);
            FileInfo *nEntry = (FileInfo *)starmemoryalloc(sizeof(FileInfo));
            nEntry->Initialize();
            nEntry->mIsDirectory = false;
            nEntry->mIsFile = false;
            nEntry->mPath = NULL;
            nEntry->mLastAccessed = al_get_fs_entry_atime(tEntry);
            ResetString(nEntry->mPath, tBuffer);

            //--Store it.
            nFileInfoList->AddElement("X", nEntry, &DontDeleteThis);
            if(tFlags & ALLEGRO_FILEMODE_ISFILE) nEntry->mIsFile      = true;
            if(tFlags & ALLEGRO_FILEMODE_ISDIR)  nEntry->mIsDirectory = true;
        }

        //--Destroy the old entry.
        al_destroy_fs_entry(tEntry);

        //--Iterate up.
        tEntry = al_read_directory(tRootEntry);
    }

    ///--[Finish Up]
    //--Clean.
    al_close_directory(tRootEntry);
    al_destroy_fs_entry(tRootEntry);

    //--Pass back the results.
    return nFileInfoList;
}
#endif
