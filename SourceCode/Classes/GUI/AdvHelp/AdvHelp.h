///========================================== AdvHelp =============================================
//--Help window, shows current controls. Appears in many places with different strings.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
typedef struct
{
    float mXPos;
    float mYPos;
    int mFlags;
    void Set(float pXPos, float pYPos, int pFlags)
    {
        mXPos = pXPos;
        mYPos = pYPos;
        mFlags = pFlags;
    }
}LegendStringPos;

///===================================== Local Definitions ========================================
#define ADV_HELP_STD_TICKS 15
#define ADV_HELP_STD_OFFSET -500.0f

///========================================== Classes =============================================
class AdvHelp : public RootObject
{
    private:
    //--System
    int mStringsTotal;
    StarlightString **mStrings;

    //--Legend Strings
    int mLegendStringsTotal;
    StarlightString **mLegendStrings;
    LegendStringPos *mLegendPositions;

    //--Render
    bool mIsReady;
    StarFont *rHeadingFont;
    StarFont *rMainlineFont;
    StarBitmap *rBacking;

    protected:

    public:
    //--System
    AdvHelp();
    virtual ~AdvHelp();
    void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void AllocateStrings(int pTotal);
    void AllocateLegendStrings(int pTotal);

    //--Core Methods
    void AssembleLegend();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void Render(float pYOffset);

    //--Pointer Routing
    StarlightString **GetStrings();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

