///==================================== AdvCombatPrediction =======================================
//--Prediction box that appears over a target during target selection.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Timing
#define ADVCOMBPREDICTION_VIS_TICKS 5

//--Padding
#define ADVCOMPREDICTION_PADDING_PER_LINE 1.0f
#define ADVCOMPREDICTION_PADDING_BORDER 3.0f
#define ADVCOMPREDICTION_PADDING_TEXT_V -5.0f

///========================================== Classes =============================================
class AdvCombatPrediction : public RootObject
{
    private:
    protected:
    //--System
    bool mIsShowing;
    AdvCombatEntity *rHostEntity;

    //--Display
    bool mUseLockPositions;
    int mVisTimer;
    float mXOffset;
    float mYOffset;
    float mLockX;
    float mLockY;
    int mStringsTotal;
    StarlightString **mStrings;

    //--Calculations
    float mXSize;
    float mYSize;

    //--Images
    float mBorderCardInd;
    StarFont *rTextFont;
    StarBitmap *rBorderCard;

    protected:

    public:
    //--System
    AdvCombatPrediction();
    virtual ~AdvCombatPrediction();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    bool IsShowing();
    bool IsVisible();

    //--Manipulators
    void SetShowing(bool pFlag);
    void SetLockPositionFlag(bool pFlag);
    void AllocateStrings(int pTotal);
    void SetFont(StarFont *pFont);
    void SetBorderCard(StarBitmap *pBitmap, float pIndent);
    void SetHostEntity(AdvCombatEntity *pEntity);
    void SetOffsets(float pXOffset, float pYOffset);
    void SetLockPositions(float pLockX, float pLockY);

    //--Core Methods
    void RunCalculations();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    virtual void Render();

    //--Pointer Routing
    AdvCombatEntity *GetHost();
    StarlightString *GetString(int pIndex);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

