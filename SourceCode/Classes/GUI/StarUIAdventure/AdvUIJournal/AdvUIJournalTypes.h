///================================= AdventureMenuJournalTypes ====================================
//--Contains structures used by the Journal menu. Each of the different journal entries has its own
//  type along with functions.
#include "Definitions.h"

#pragma once

///======================================== Basic Entry ===========================================
#ifndef ADVUIJOURNAL_TYPES
#define ADVUIJOURNAL_TYPES

//--Basic entry, other entries inherit from this. Contains common parts like names.
typedef struct AdvMenuJournalBasicEntry
{
    //--System
    char *mInternalName;
    char *mDisplayName;

    //--Display
    StarBitmap *rIcon;

    //--Description.
    int mDescriptionLinesTotal;
    char **mDescriptionLines;

    //--Methods
    void BaseInit()
    {
        //--System
        mInternalName = InitializeString("Internal Name");
        mDisplayName = InitializeString("Display Name");

        //--Display
        rIcon = NULL;

        //--Description
        mDescriptionLinesTotal = 0;
        mDescriptionLines = NULL;
    }
    void AllocateDescriptionLines(int pLines)
    {
        //--Deallocate.
        for(int i = 0; i < mDescriptionLinesTotal; i ++) free(mDescriptionLines[i]);
        free(mDescriptionLines);

        //--Zero.
        mDescriptionLinesTotal = 0;
        mDescriptionLines = NULL;
        if(pLines < 1) return;

        //--Allocate.
        mDescriptionLinesTotal = pLines;
        mDescriptionLines = (char **)starmemoryalloc(sizeof(char *) * mDescriptionLinesTotal);
        for(int i = 0; i < mDescriptionLinesTotal; i ++) mDescriptionLines[i] = NULL;
    }
    void BaseDealloc()
    {
        free(mInternalName);
        free(mDisplayName);
        for(int i = 0; i < mDescriptionLinesTotal; i ++)
        {
            free(mDescriptionLines[i]);
        }
        free(mDescriptionLines);
    }

}AdvMenuJournalBasicEntry;

///==================================== Derived Structures ========================================
///--[Bestiary Entry]
//--Contains a bestiary entry. Created by lua scripts.
typedef struct AdvMenuJournalBestiaryEntry : public AdvMenuJournalBasicEntry
{
    //--Display
    StarBitmap *rImage;
    StarBitmap *rParagon;
    float mPortraitX;
    float mPortraitY;

    //--Statistics
    bool mShowStatistics;
    int mLevel;
    int mHP;
    int mAtk;
    int mIni;
    int mAcc;
    int mEvd;
    int mPrt;
    int mExp;
    int mPlatina;

    //--Resistances
    bool mShowResistances;
    int mSlashRes;
    int mStrikeRes;
    int mPierceRes;
    int mFlameRes;
    int mFreezeRes;
    int mShockRes;
    int mCrusadeRes;
    int mObscureRes;
    int mBleedRes;
    int mPoisonRes;
    int mCorrodeRes;
    int mTerrifyRes;

    //--Misc. Stats
    int mDefeatCount;
    int mDefeatParagonCount;
    int mDefeatMax;
    bool mCanTransformYou;

    //--Functions
    void Initialize()
    {
        //--Base.
        BaseInit();

        //--Display
        rImage = NULL;
        rParagon = NULL;
        mPortraitX = 0.0f;
        mPortraitY = 0.0f;

        //--Statistics
        mShowStatistics = false;
        mLevel = 0;
        mHP = 0;
        mAtk = 0;
        mIni = 0;
        mAcc = 0;
        mEvd = 0;
        mPrt = 0;
        mExp = 0;
        mPlatina = 0;

        //--Resistances
        mShowResistances = false;
        mSlashRes = 0;
        mStrikeRes = 0;
        mPierceRes = 0;
        mFlameRes = 0;
        mFreezeRes = 0;
        mShockRes = 0;
        mCrusadeRes = 0;
        mObscureRes = 0;
        mBleedRes = 0;
        mPoisonRes = 0;
        mCorrodeRes = 0;
        mTerrifyRes = 0;

        //--Misc Stats
        mDefeatCount = 0;
        mDefeatParagonCount = 0;
        mDefeatMax = 10;
        mCanTransformYou = false;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuJournalBestiaryEntry *rPtr = (AdvMenuJournalBestiaryEntry *)pPtr;
        rPtr->BaseDealloc();
        free(rPtr);
    }
}AdvMenuJournalBestiaryEntry;

///--[Profile Entry]
//--Profile, stores a character image and dialogue.
typedef struct AdvMenuJournalProfileEntry : public AdvMenuJournalBasicEntry
{
    //--Display
    StarBitmap *rDisplayImg;
    float mPortraitOffsetX;
    float mPortraitOffsetY;

    //--Functions
    void Initialize()
    {
        //--Base
        BaseInit();

        //--Display
        rDisplayImg = NULL;
        mPortraitOffsetX = 0.0f;
        mPortraitOffsetY = 0.0f;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuJournalProfileEntry *rPtr = (AdvMenuJournalProfileEntry *)pPtr;
        rPtr->BaseDealloc();
        free(rPtr);
    }
}AdvMenuJournalProfileEntry;

///--[Quest Entry]
//--Quest. Has a description and an objective.
typedef struct AdvMenuJournalQuestEntry : public AdvMenuJournalBasicEntry
{
    //--System
    //--Description
    //--Objective
    char *mObjective;

    //--Functions
    void Initialize()
    {
        //--Base
        BaseInit();

        //--Objective
        mObjective = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuJournalQuestEntry *rPtr = (AdvMenuJournalQuestEntry *)pPtr;
        rPtr->BaseDealloc();
        free(rPtr->mObjective);
        free(rPtr);
    }
}AdvMenuJournalQuestEntry;

///--[Location Entry]
//--Stores information about locations the player has discovered.
typedef struct AdvMenuJournalLocationEntry : public AdvMenuJournalBasicEntry
{
    //--Display
    StarBitmap *rImage;
    float mOffsetX;
    float mOffsetY;

    //--Functions
    void Initialize()
    {
        //--Base
        BaseInit();

        //--Display
        rImage = NULL;
        mOffsetX = 0.0f;
        mOffsetY = 0.0f;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuJournalLocationEntry *rPtr = (AdvMenuJournalLocationEntry *)pPtr;
        rPtr->BaseDealloc();
        free(rPtr);
    }
}AdvMenuJournalLocationEntry;

///--[Paragon Entry]
typedef struct AdvMenuParagonEntry : public AdvMenuJournalBasicEntry
{
    //--Properties
    bool mIsEnabled;
    int mKOCount;
    int mKORequired;
    bool mHasDefeatedEnabledParagon;
    char mEnabledFlagPath[256];

    //--Functions
    void Initialize()
    {
        //--Base
        BaseInit();

        //--Properties
        mIsEnabled = true;
        mKOCount = 0;
        mKORequired = 1;
        mHasDefeatedEnabledParagon = false;
        mEnabledFlagPath[0] = '\0';
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuParagonEntry *rPtr = (AdvMenuParagonEntry *)pPtr;
        rPtr->BaseDealloc();
        free(rPtr);
    }
}AdvMenuParagonEntry;

///--[Combat Glossary Piece]
//--Represents a block, either of text or an image, on the combat glossary page.
typedef struct AdvMenuJournalGlossaryPiece
{
    //--Position
    float mX;
    float mY;

    //--Text
    int mLinesTotal;
    char **mLines;
    int *mFlags;
    StarFont **mrFonts;

    //--Image
    StarBitmap *rImage;

    //--Functions
    void Initialize()
    {
        //--Position
        mX = 0.0f;
        mY = 0.0f;

        //--Text
        mLinesTotal = 0;
        mLines = NULL;
        mFlags = NULL;
        mrFonts = NULL;

        //--Image
        rImage = NULL;
    }
    void AllocateDescriptionLines(int pLines)
    {
        //--Deallocate.
        for(int i = 0; i < mLinesTotal; i ++) free(mLines[i]);
        free(mLines);
        free(mFlags);
        free(mrFonts);

        //--Zero.
        mLinesTotal = 0;
        mLines = NULL;
        mFlags = NULL;
        mrFonts = NULL;
        if(pLines < 1) return;

        //--Allocate.
        mLinesTotal = pLines;
        mLines = (char **)starmemoryalloc(sizeof(char *) * mLinesTotal);
        mFlags = (int *)starmemoryalloc(sizeof(int) * mLinesTotal);
        mrFonts = (StarFont **)starmemoryalloc(sizeof(StarFont *) * mLinesTotal);
        for(int i = 0; i < mLinesTotal; i ++)
        {
            mLines[i] = NULL;
            mFlags[i] = 0;
            mrFonts[i] = NULL;
        }
    }
    void DeallocateThis()
    {
        for(int p = 0; p < mLinesTotal; p ++) free(mLines[p]);
        free(mLines);
        free(mFlags);
        free(mrFonts);
    }
}AdvMenuJournalGlossaryPiece;

///--[Combat Glossary Entry]
//--Stores information about locations the player has discovered.
typedef struct AdvMenuJournalCombatGlossaryEntry : public AdvMenuJournalBasicEntry
{
    //--System
    //--Pieces
    int mPiecesTotal;
    AdvMenuJournalGlossaryPiece *mPieces;

    //--Functions
    void Initialize()
    {
        //--Base
        BaseInit();

        //--System
        //--Pieces
        mPiecesTotal = 0;
        mPieces = NULL;
    }
    void AllocatePieces(int pTotal)
    {
        //--Deallocate.
        for(int i = 0; i < mPiecesTotal; i ++)
        {
            for(int p = 0; p < mPieces[i].mLinesTotal; p ++) free(mPieces[i].mLines[p]);
            free(mPieces[i].mLines);
        }
        free(mPieces);

        //--Zero.
        mPiecesTotal = 0;
        mPieces = NULL;
        if(pTotal < 1) return;

        //--Allocate.
        mPiecesTotal = pTotal;
        mPieces = (AdvMenuJournalGlossaryPiece *)starmemoryalloc(sizeof(AdvMenuJournalGlossaryPiece) * mPiecesTotal);
        for(int i = 0; i < mPiecesTotal; i ++) mPieces[i].Initialize();
    }
    static void DeleteThis(void *pPtr)
    {
        //--Error check.
        if(!pPtr) return;

        //--Cast, order base deallocation.
        AdvMenuJournalCombatGlossaryEntry *rPtr = (AdvMenuJournalCombatGlossaryEntry *)pPtr;
        rPtr->BaseDealloc();

        //--Deallocate local values.
        for(int i = 0; i < rPtr->mPiecesTotal; i ++) rPtr->mPieces[i].DeallocateThis();
        free(rPtr->mPieces);

        //--Deallocate pointer.
        free(rPtr);
    }
}AdvMenuJournalCombatGlossaryEntry;
#endif // ADVUIJOURNAL_TYPES
