//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"
#include "TileLayer.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Debug]
//#define TAPARSE_DEBUG
#ifdef TAPARSE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== Parsing =============================================
SpawnPackage *TilemapActor::ParseEnemyProperties(ObjectInfoPack *pObjectInfo)
{
    ///--[ ====== Documentation And Setup ===== ]
    //--Static function, given a set of properties derived from a Tiled map, produces and returns a SpawnPackage which can be
    //  used to create an enemy.

    ///--[ ========== Create Package ========== ]
    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    SpawnPackage *nPackage = (SpawnPackage *)starmemoryalloc(sizeof(SpawnPackage));
    nPackage->Initialize();

    ///--[ ======== Common Properties ========= ]
    //--Name.
    strncpy(nPackage->mRawName, pObjectInfo->mName, STD_MAX_LETTERS);

    //--Position. Add one tile to the Y position. Enemies come from a tileset which is 16x32 instead of 16x16.
    nPackage->mDimensions.SetWH(pObjectInfo->mX, pObjectInfo->mY, pObjectInfo->mW, pObjectInfo->mH);

    //--Store the unique ID for this enemy. This will be updated later, since the level has not received a
    //  name yet (AL_SetProperty() does this after the level is parsed).
    sprintf(nPackage->mUniqueSpawnName, "%s", pObjectInfo->mName);

    ///--[ ========= Property Parsing ========= ]
    //--Property Check: Store destination strings.
    for(int i = 0; i < pObjectInfo->mProperties->mPropertiesTotal; i ++)
    {
        ///--[Setup]
        //--Fast Access
        char *rKey = pObjectInfo->mProperties->mKeys[i];
        char *rVal = pObjectInfo->mProperties->mVals[i];
        int tKeyLen = (int)strlen(rKey);

        ///--[Combat Properties]
        //--Which enemy this represents.
        if(!strcasecmp(rKey, "Party"))
        {
            strcpy(nPackage->mPartyEnemy, rVal);
        }
        //--Which cutscene to play if the player wins.
        else if(!strcasecmp(rKey, "WinScene"))
        {
            strcpy(nPackage->mWinCutscene, rVal);
        }
        //--Which cutscene to play if the player loses.
        else if(!strcasecmp(rKey, "Scene"))
        {
            strcpy(nPackage->mLoseCutscene, rVal);
        }
        ///--[Appearance]
        //--Which sprites to use.
        else if(!strcasecmp(rKey, "Appearance"))
        {
            //--Copy.
            strcpy(nPackage->mAppearancePath, rVal);

            //--Special: Doctor bag spawns automatically gain this flag.
            if(!strcasecmp(nPackage->mAppearancePath, "DoctorBag"))
            {
                nPackage->mDontSpawnIfDead = true;
            }
        }
        //--Toughness rating. This causes the enemy to have an outline. It does not directly affect
        //  combat difficulty.
        else if(!strcasecmp(rKey, "Toughness"))
        {
            nPackage->mToughness = atoi(rVal);
        }
        ///--[Field AI Properties]
        //--Uses the enemy's name "EnemyAA", "EnemyAB" etc to resolve which nodes it patrols to and if it follows another enemy.
        else if(!strcasecmp(rKey, "AutoPatrol"))
        {
            nPackage->mUseAutoPatrol = true;
        }
        //--Range (in pixels) of the entity's viewcone.
        else if(!strcasecmp(rKey, "DetectRange"))
        {
            nPackage->mDetectRange = atoi(rVal);
        }
        //--Angle (in degrees) of the entity's viewcone.
        else if(!strcasecmp(rKey, "DetectAngle"))
        {
            nPackage->mDetectAngle = atoi(rVal);
        }
        //--Ignore, base. Uses "|" as a delimiter.
        else if(!strcasecmp(rKey, "Ignore") && tKeyLen == 6)
        {
            strcat(nPackage->mIgnoreFlag, rVal);
        }
        //--IgnoreA/IgnoreB/IgnoreC etc. Non-delimited version of the Ignore set.
        else if(!strncasecmp(rKey, "Ignore", 6))
        {
            strcat(nPackage->mIgnoreFlag, rVal);
            strcat(nPackage->mIgnoreFlag, "|");
        }
        //--Commands Entities.
        else if(!strcasecmp(rKey, "Commands"))
        {
            strcat(nPackage->mCommandsEntities, rVal);
        }
        //--Which collision depth to use.
        else if(!strcasecmp(rKey, "Depth"))
        {
            nPackage->mDepth = atoi(rVal);
        }
        //--Follow. Entity follows another entity unless it doesn't exist.
        else if(!strcasecmp(rKey, "Follow"))
        {
            strncpy(nPackage->mFollowTarget, rVal, STD_MAX_LETTERS);
        }
        //--Patrol pathway.
        else if(!strcasecmp(rKey, "PatrolPath"))
        {
            if(!strcasecmp(rVal, "Null"))
                nPackage->mPatrolPathway[0] = '\0';
            else
                strncpy(nPackage->mPatrolPathway, rVal, PATROLPATH_MAX_LETTERS);
        }
        //--Advanced Patrol. Uses map-accurate movements to get around.
        else if(!strcasecmp(rKey, "AdvancedPatrol"))
        {
            nPackage->mAdvancedPatrol = true;
        }
        //--Smart Patrol. Enemy will attempt to pathfind to get around.
        else if(!strcasecmp(rKey, "SmartPatrol"))
        {
            nPackage->mSmartPatrol = true;
        }
        //--Smart Patrol will path to any node, not just the next one.
        else if(!strcasecmp(rKey, "PatrolToAny"))
        {
            nPackage->mPatrolToAnyNode = true;
        }
        //--Locks an enemy to facing a specific direction.
        else if(!strcasecmp(rKey, "FacesOnly"))
        {
            if(!strcasecmp(rVal, "Up"))    nPackage->mLockFacing = DIR_UP;
            if(!strcasecmp(rVal, "Right")) nPackage->mLockFacing = DIR_RIGHT;
            if(!strcasecmp(rVal, "Down"))  nPackage->mLockFacing = DIR_DOWN;
            if(!strcasecmp(rVal, "Left"))  nPackage->mLockFacing = DIR_LEFT;
        }
        ///--[Flags]
        //--Blind enemies never run detect algorithms.
        else if(!strcasecmp(rKey, "Blind"))
        {
            nPackage->mIsBlind = true;
        }
        //--Immobile enemies never move.
        else if(!strcasecmp(rKey, "Immobile"))
        {
            nPackage->mIsImmobile = true;
        }
        //--Enemy chases the player at a higher speed.
        else if(!strcasecmp(rKey, "Fast"))
        {
            nPackage->mIsFast = true;
        }
        //--Enemy chases the player twice as far.
        else if(!strcasecmp(rKey, "Relentless"))
        {
            nPackage->mIsRelentless = true;
        }
        //--Never pauses on patrol route to look around.
        else if(!strcasecmp(rKey, "Never Pauses"))
        {
            nPackage->mNeverPauses = true;
        }
        //--Paragon grouping. Used to handle paragon spawning.
        else if(!strcasecmp(rKey, "ParagonGroup"))
        {
            strncpy(nPackage->mParagonGroup, rVal, PARAGON_GROUP_LETTERS);
        }
        //--Entity will not stop and reface when on a patrol node, and immediately begin moving to the next one.
        else if(!strcasecmp(rKey, "Dont Reface On Node"))
        {
            nPackage->mDontRefaceFlag = true;
        }
        //--Entity does not spawn if dead. If false, enemies can leave their corpses after defeat.
        else if(!strcasecmp(rKey, "Dont Spawn If Dead"))
        {
            nPackage->mDontSpawnIfDead = true;
        }
        //--Spawn chance, causes the entity to use pseudorandom spawning. Spawns reset at each rest.
        else if(!strcasecmp(rKey, "Spawn Chance"))
        {
            nPackage->mUseSeededSpawnChance = true;
            nPackage->mSeedSpawnChance = atoi(rVal);
        }
        //--Keyword, used if this is an item spawn node.
        else if(!strcasecmp(rKey, "Keyword"))
        {
            strncpy(nPackage->mActivateKeyword, rVal, ITEM_KEYWORD_LETTERS-1);
        }
        ///--[Mugging Flags]
        //--Block Auto Win. Enemy cannot be mugged and immediately defeated, regardless of the power
        //  differential between the two.
        else if(!strcasecmp(rKey, "Block Auto Win"))
        {
            nPackage->mBlockAutoWin = true;
        }
        //--No Mugging. Disables mugging.
        else if(!strcasecmp(rKey, "No Mugging"))
        {
            nPackage->mCannotBeMugged = true;
        }
        //--Does not drop items when mugged.
        else if(!strcasecmp(rKey, "No Items When Mugging"))
        {
            nPackage->mMuggingNeverDropsItems = true;
        }
    }

    ///--[Finish Up]
    return nPackage;
}

///=================================== Auto-Patrol Handling =======================================
void TilemapActor::HandleEnemyAutoPatrol(SpawnPackage *pPackage)
{
    ///--[Documentation and Setup]
    //--If the flag mUseAutoPatrol is true, this may override some other properties such as the patrol path.
    //  It will also require looking for other entities like patrol nodes.
    //--AutoPatrol assumes enemies use the naming pattern "EnemyAA", "EnemyAB", etc where the first A is the patrol
    //  nodes the enemy uses, and the second A is used to form clusters. The B enemy follows the A enemy, and they
    //  look for the nodes A0, A1, etc until they run out of nodes.
    //--The last two letters are the ones used regardless of how enemy name is formatted. "SampleEnemyAB" always
    //  looks to follow "SampleEnemyAA".
    if(!pPackage) return;

    ///--[Diagnostics]
    DebugPush(true, "Running auto-patrol handler on package %p\n", pPackage);
    DebugPrint("Raw name: %s\n", pPackage->mRawName);

    ///--[Setup]
    //--Make sure entities are set up and valid before attempting to do any processing. Bark warnings if anything
    //  is incorrect.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel)
    {
        fprintf(stderr, "TilemapActor::HandleEnemyAutoPatrol() - Warning, no active level. Cannot build path information. Failing.\n");
        DebugPop("Exiting.\n");
        return;
    }

    //--Get the patrol node list.
    StarLinkedList *rPatrolNodeList = rActiveLevel->GetPatrolNodeList();
    if(!rPatrolNodeList)
    {
        fprintf(stderr, "TilemapActor::HandleEnemyAutoPatrol() - Warning, no patrol node list. Cannot build path information. Failing.\n");
        DebugPop("Exiting.\n");
        return;
    }

    //--The raw name needs to be at least 3 letters long.
    int tNameLen = (int)strlen(pPackage->mRawName);
    if(tNameLen < 3)
    {
        fprintf(stderr, "TilemapActor::HandleEnemyAutoPatrol() - Warning, enemy name must be at least 3 letters long. Raw Name: %s.\n", pPackage->mRawName);
        DebugPop("Exiting.\n");
        return;
    }

    ///--[Name Resolving]
    //--Break the name into three buffers. The name of the entity, the node set, and the follow order.
    char *tEnemyNameString = InitializeString(pPackage->mRawName);
    char tEnemyPatrolLetter = pPackage->mRawName[tNameLen-2];
    char tEnemyPositionLetter = pPackage->mRawName[tNameLen-1];

    //--Knock the last two letters off the name string.
    tEnemyNameString[tNameLen-2] = '\0';

    //--Debug.
    DebugPrint("Entity base name: %s\n", tEnemyNameString);
    DebugPrint("Entity patrol letter: %c\n", tEnemyPatrolLetter);
    DebugPrint("Entity position letter: %c\n", tEnemyPositionLetter);

    //--Temp strings.
    char tPatrolLetterString[2];
    tPatrolLetterString[0] = tEnemyPatrolLetter;
    tPatrolLetterString[1] = '\0';

    ///--[Leader Handling]
    //--This enemy is the leader, so they need to set their patrol paths.
    if(tEnemyPositionLetter == 'A')
    {
        //--Follower is set to "Null".
        strcpy(pPackage->mFollowTarget, "Null");
        DebugPrint("Entity is a leader.\n");
    }
    ///--[Follower Handling]
    //--This is a follower. It needs to follow the A version of the enemy, unless it's a D or E then it will follow the B/C enemy.
    else
    {
        //--Debug.
        DebugPrint("Entity is a follower.\n");

        //--B/C followers follow the A enemy.
        if(tEnemyPositionLetter == 'B' || tEnemyPositionLetter == 'C')
        {
            strcpy(pPackage->mFollowTarget, tEnemyNameString);
            strcat(pPackage->mFollowTarget, tPatrolLetterString);
            strcat(pPackage->mFollowTarget, "A");
        }
        //--All further letters attempt to follow the enemy 2 slots below them.
        else
        {
            char tAppendString[2];
            tAppendString[0] = tEnemyPositionLetter-2;
            tAppendString[1] = '\0';;
            strcpy(pPackage->mFollowTarget, tEnemyNameString);
            strcat(pPackage->mFollowTarget, tPatrolLetterString);
            strcat(pPackage->mFollowTarget, tAppendString);
        }

        //--Debug.
        DebugPrint("Entity is following: %s\n", pPackage->mFollowTarget);
    }

    ///--[Other Flags]
    //--Always set these flags.
    pPackage->mAdvancedPatrol = true;
    pPackage->mPatrolToAnyNode = true;

    //--Patrol path is set to "SEQ|Letter".
    strcpy(pPackage->mPatrolPathway, "SEQ|");
    strcat(pPackage->mPatrolPathway, tPatrolLetterString);
    DebugPrint("Patrol path set to %s.\n", pPackage->mPatrolPathway);

    ///--[Finish Up]
    free(tEnemyNameString);
    DebugPop("Completed normally.\n");
}

///====================================== Implementation ==========================================
void TilemapActor::SetEnemyPropertiesFromPackage(SpawnPackage *pPackage, bool pIsParagon)
{
    ///--[Documentation and Setup]
    //--Called once the map decides to spawn an enemy (enemies may not spawn if they were defeated)
    //  this will set properties based on the provided package, including special properties like
    //  becoming a Doctor Bag refill.
    if(!pPackage) return;

    ///--[Auto-Patrol Handler]
    //--If applicable, call the Auto-Patrol handler. This will adjust the package's properties but does
    //  not do anything that can't be done via other package properties.
    if(pPackage->mUseAutoPatrol)
    {
        HandleEnemyAutoPatrol(pPackage);
    }

    ///--[Common Flags]
    //--All entities, even special ones, set these flags.
    SetName(pPackage->mRawName);
    ActivateEnemyMode(pPackage->mUniqueSpawnName);
    SetLeashingPoint(pPackage->mDimensions.mXCenter, (pPackage->mDimensions.mYCenter - (TileLayer::cxSizePerTile * 1.50f)));
    SetPosition(pPackage->mDimensions.mLft / TileLayer::cxSizePerTile, (pPackage->mDimensions.mTop / TileLayer::cxSizePerTile) - 1.0f);
    SetCollisionDepth(pPackage->mDepth);
    SetMuggable(false);
    SetNeverDropItemsOnMug(pPackage->mMuggingNeverDropsItems);

    ///--[Doctor Bag Pickup]
    //--Special: If the name of the appearance is "DoctorBag" then this is a DoctorBag restoring item.
    if(!strcasecmp(pPackage->mAppearancePath, "DoctorBag"))
    {
        //--Set this special flag.
        ActivateBagRefillMode();

        //--Give this entity its appearance data.
        BuildGraphicsFromName("BagRefill", false, false, false);
        return;
    }

    ///--[Item Nodes]
    //--Item nodes use similar logic to the doctor bag.
    if(!strcasecmp(pPackage->mAppearancePath, "AdamantiteNode"))
    {
        //--Set this special flag.
        ActivateItemNodeMode();

        //--Give this entity its appearance data.
        BuildGraphicsFromName("AdamantiteNode", false, false, false);
        SetItemNodeKeyword(pPackage->mActivateKeyword);
        return;
    }

    ///--[Flag Setting]
    //--String buffer, used to add paragon properties.
    char tBuffer[128];

    //--Combat Properties
    mDoesNotTriggerBattles = pPackage->mNeverTriggerBattles;
    if(pPackage->mWinCutscene[0] != '\0') SetVictoryScene(pPackage->mWinCutscene);
    SetDefeatScene(pPackage->mLoseCutscene);

    //--Give this enemy its appearance data.
    strcpy(tBuffer, pPackage->mAppearancePath);
    if(pIsParagon) strcat(tBuffer, "Paragon");
    BuildGraphicsFromName(tBuffer, false, false, false);
    SetToughness(pPackage->mToughness);

    //--Field AI Properties
    SetDetectRange(pPackage->mDetectRange);
    SetDetectAngle(pPackage->mDetectAngle);
    if(pPackage->mFollowTarget[0]  != '\0') SetFollowTarget(pPackage->mFollowTarget);
    if(pPackage->mPatrolPathway[0] != '\0') ActivatePatrolMode(pPackage->mPatrolPathway); //Note: Can override mFollowTarget
    if(pPackage->mSmartPatrol)
    {
        ActivateSmartPathing();
    }
    else if(pPackage->mAdvancedPatrol)
    {
        ActivateAdvancedPathing();
    }
    if(pPackage->mPatrolToAnyNode) SetPatrolToAnyNode(true);
    SetLockFacing(pPackage->mLockFacing);

    //--Flags
    SetBlind(pPackage->mIsBlind);
    SetImmobile(pPackage->mIsImmobile);
    SetFast(pPackage->mIsFast);
    SetRelentless(pPackage->mIsRelentless);
    SetNeverPauses(pPackage->mNeverPauses);
    SetParagonGrouping(pPackage->mParagonGroup);
    SetDontRefaceFlag(pPackage->mDontRefaceFlag);

    //--Mugging Flags
    SetBlockMugAutoWin(pPackage->mBlockAutoWin);
    SetMuggable(!pPackage->mCannotBeMugged);
    SetMugLootable(true); //Level may override this.

    //--Resolve the combat enemy and append it.
    strcpy(tBuffer, pPackage->mPartyEnemy);
    if(pIsParagon) strcat(tBuffer, " Paragon");
    AddCombatEnemy(tBuffer);

    ///--[Ignore Handling]
    //--Iterate across the mIgnoreFlag string. Delimiter is '|'. Each unique string goes on the ignore list.
    int tStartOfString = 0;
    int tLen = (int)strlen(pPackage->mIgnoreFlag);
    for(int i = 0; i < tLen; i ++)
    {
        //--If this is a '|', then add the string to the entity's list.
        if(pPackage->mIgnoreFlag[i] == '|')
        {
            pPackage->mIgnoreFlag[i] = '\0';
            AddIgnoreString(&pPackage->mIgnoreFlag[tStartOfString]);
            pPackage->mIgnoreFlag[i] = '|';
            tStartOfString = i + 1;
        }
    }

    ///--[Commander Handling]
    //--Iterate across the mCommandsEntities string. Delimiter is '|'. Each unique string goes on the ignore list.
    tLen = (int)strlen(pPackage->mCommandsEntities);
    for(int i = 0; i < tLen; i ++)
    {
        //--If this is a '|', then add the string to the entity's list.
        if(pPackage->mCommandsEntities[i] == '|')
        {
            pPackage->mCommandsEntities[i] = '\0';
            AddCommandString(&pPackage->mCommandsEntities[tStartOfString]);
            pPackage->mCommandsEntities[i] = '|';
            tStartOfString = i + 1;
        }
    }
}
