///======================================= CorrupterMenu ==========================================
//--GUI piece, used in Corrupter Mode to spawn monsters and order them around. Borrows some UI behaviors from
//  the VisualLevel, specifically the border rendering and UI font style.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
///--[MonsterPack]
//--Contains information about an Actor in the world. Mostly just a reference to the monster.
//  Considered to be volatile, refreshes when the menu is opened.
typedef struct MonsterPack
{
    //--Variables
    Actor *rMonster;
    StarBitmap *rIcon;

    //--Methods
    static MonsterPack *Create()
    {
        SetMemoryData(__FILE__, __LINE__);
        MonsterPack *nPack = (MonsterPack *)starmemoryalloc(sizeof(MonsterPack));
        nPack->rMonster = NULL;
        nPack->rIcon = NULL;
        return nPack;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        free(pPtr);
    }
}MonsterPack;

///--[SummonPack]
//--Contains information concerning summoning a monster. Includes their HP, WP, Cost, and some extra
//  display information. Also contains some description lines, which are optional.
#define SUMMON_PACK_DESCRIPTION_LINES 5
typedef struct SummonPack
{
    //--Summon Variables
    char *mSummonName;
    int mManaCost;
    int mPrototypeSlot;

    //--Monster Data
    int mHP, mWP;
    StarBitmap *rIconImg;
    StarBitmap *rRenderImg;

    //--Description Lines
    char *mDescriptionLines[SUMMON_PACK_DESCRIPTION_LINES];

    //--Static Methods
    static SummonPack *Create()
    {
        SetMemoryData(__FILE__, __LINE__);
        SummonPack *nPack = (SummonPack *)starmemoryalloc(sizeof(SummonPack));
        memset(nPack, 0, sizeof(SummonPack));
        return nPack;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        SummonPack *rPack = (SummonPack *)pPtr;
        free(rPack->mSummonName);
        for(int i = 0; i < SUMMON_PACK_DESCRIPTION_LINES; i ++) free(rPack->mDescriptionLines[i]);
        free(rPack);
    }

    //--Other Methods
    void SetLine(int pIndex, const char *pString)
    {
        if(pIndex < 0 || pIndex >= SUMMON_PACK_DESCRIPTION_LINES) return;
        ResetString(mDescriptionLines[pIndex], pString);
    }
}SummonPack;

///===================================== Local Definitions ========================================
#define CM_MODE_STATUS 0
#define CM_MODE_MINIONS 1
#define CM_MODE_SUMMON 2
#define CM_MODE_MAGIC 3
#define CM_MODE_TRANSFORM 4

#define CM_PERK_UNLOCK_LINES 12
#define CM_PERK_UNLOCK_CHARS 128

#define CM_HEADER_INDENT 12.0f
#define CM_BTN_INDENT 8.0f
#define CM_COLUMN_SPACING 8.0f
#define CM_BTN_TEXT_SIZE 20.0f
#define CM_BTN_TEXT_SIZE_SMALL 17.0f
#define CM_Y_SPACING_PER_MONSTER 32.0f

///========================================== Classes =============================================
class CorrupterMenu : public RootObject
{
    private:
    //--System
    bool mIsVisible;
    uint8_t mCurrentMode;

    //--(Status Mode)
    //--Mode
    bool mIsUnlockingPerk;
    char mPerkUnlockLines[CM_PERK_UNLOCK_LINES][CM_PERK_UNLOCK_CHARS];

    //--Position Constants
    float cPerkStartX;
    float cPerkUnlockStartX;
    float cPerkStartY;
    float cPerkBtnWid;
    float cPerkBtnHei;

    //--Button Highlighting
    int mPerkHighlightedBtn;

    //--(Minions Mode)
    //--Column Position Constants
    float cXPosIcon;
    float cXPosName;
    float cXPosHP;
    float cXPosWP;
    float cXPosLocation;
    float cXPosOrders;

    //--Standing Orders Button
    bool mIsOrdersButtonHighlighted;
    TwoDimensionReal mOrdersBtn;

    //--Storage
    int mMinionActiveSlot;
    StarLinkedList *mMonsterListing;

    //--(Summons Mode)
    //--Selection
    int mHighlightedSummonIndex;

    //--Script
    char *mSummonExecutionScript;

    //--Confirmation
    bool mShowConfirmDialog;
    bool mIsDialogNotEnoughMP;

    //--(Magic Mode)
    //--Selection
    bool mMustRefreshMagicLegality;
    int mHighlightedMagicIndex;

    //--Positioning
    float cMagicListingLft;
    float cMagicListingWid;
    float cMagicListingTop;
    float cDescriptionListingLft;
    float cDescriptionListingWid;
    float cDescriptionListingTop;

    //--Casting Button
    TwoDimensionReal mMagicCastBtn;

    //--(Transform Mode)
    //--Selection
    int mHighlightedTransformIndex;

    //--Positioning
    TwoDimensionReal mTransformListingDim;
    TwoDimensionReal mTransformPortraitDim;
    TwoDimensionReal mTransformDescriptionDim;
    TwoDimensionReal mTransformActivateBtn;

    //--(Unrolled Magic Menu)
    //--Selection
    int mHighlightedUnrolledSpell;
    int mLastUnrolledSize;

    //--(Universal)
    //--Header
    int mHighlightedHeaderBtn;
    TwoDimensionReal mHeaderStatusBtn;
    TwoDimensionReal mHeaderMinionsBtn;
    TwoDimensionReal mHeaderSummonBtn;
    TwoDimensionReal mHeaderMagicBtn;
    TwoDimensionReal mHeaderTransformBtn;
    TwoDimensionReal mHeaderCloseBtn;

    //--Confirmation Dialog
    TwoDimensionReal mConfirmBody;
    TwoDimensionReal mConfirmYesBtn;
    TwoDimensionReal mConfirmCancelBtn;

    //--Colors
    StarlightColor mNormalTextCol;
    StarlightColor mUnavailableCol;
    StarlightColor mHighlightCol;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            StarBitmap *rBorderCard;
            StarFont *rUIFont;
            StarFont *rUIFontSmall;
        }Data;
    }Images;

    protected:

    public:
    //--System
    CorrupterMenu();
    virtual ~CorrupterMenu();

    //--Public Variables
    static char *xStandingOrders;
    static StarLinkedList *xSummonPackList;
    static int xPlayerMP;

    //--Property Queries
    bool IsVisible();

    //--Manipulators
    void SetVisibility(bool pIsVisible);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Magic Menu
    bool IsMagicMode();
    void SetToMagicMode();
    void FillSpellDescription(MagicPack *pMagicPack);
    void PopulateContextAroundActor(Actor *pActor, ContextMenu *pMenu);
    void PopulateContextAroundItem(InventoryItem *pItem, ContextMenu *pMenu);
    void PopulateContextAroundContainer(WorldContainer *pContainer, ContextMenu *pMenu);
    void UpdateMagicMode();
    void RenderMagicMode();

    //--Minions Menu
    bool IsMinionsMode();
    void SetToMinionsMode();
    void RefreshMonsterListing();
    void UpdateMinionsMode();
    void RenderMinionsMode();

    //--Status Menu
    bool IsStatusMode();
    void SetToStatusMode();
    void FillStatusDescription(PerkPack *pPerkPack);
    void UpdateStatusMode();
    void RenderStatusMode();

    //--Summon Menu
    bool IsSummonMode();
    void SetToSummonMode();
    void SetSummonExecScript(const char *pPath);
    void ExecuteSummon(SummonPack *pPack);
    void UpdateSummonMode();
    void RenderSummonMode();
    void RenderSummonConfirmation();

    //--Transformation Menu
    bool IsTransformMode();
    void SetToTransformMode();
    void FillTransformDescription(TransformPack *pTransformationPack);
    void UpdateTransformMode();
    void RenderTransformMode();

    //--Unrolled Menu
    TwoDimensionReal CalculateUnrolledSize(float pLft, float pTop, float pRgt);
    bool UpdateUnrolledMenu(TwoDimensionReal pDimensions);
    void RenderUnrolledMenu(TwoDimensionReal pDimensions);

    //--Update
    void Update();
    bool UpdateHeader();

    //--File I/O
    //--Drawing
    void Render();
    void RenderHeader();

    //--Pointer Routing
    //--Static Functions
    static CorrupterMenu *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CM_RegisterSummonPack(lua_State *L);
int Hook_CM_GetProperty(lua_State *L);
int Hook_CM_SetProperty(lua_State *L);

