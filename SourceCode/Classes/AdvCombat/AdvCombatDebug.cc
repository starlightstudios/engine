//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

void AdvCombat::Debug_RestoreParty()
{
    FullRestoreParty();
}
void AdvCombat::Debug_AwardXP(int pAmount)
{
    //--Affects the active party only.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rEntity)
    {
        int tCurXP = rEntity->GetXP();
        rEntity->SetXP(tCurXP + pAmount);
        rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}
void AdvCombat::Debug_AwardJP(int pAmount)
{
    //--Affects the active party only.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rEntity)
    {
        rEntity->GainGlobalJP(pAmount);
        rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}
void AdvCombat::Debug_AwardPlatina(int pAmount)
{
    int tPlatina = AdventureInventory::Fetch()->GetPlatina();
    AdventureInventory::Fetch()->SetPlatina(tPlatina + pAmount);
}
void AdvCombat::Debug_AwardCrafting()
{
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        int tCur = rInventory->GetCraftingCount(i);
        rInventory->SetCraftingMaterial(i, tCur + 10);
    }
}
