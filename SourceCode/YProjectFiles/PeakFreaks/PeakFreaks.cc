//--Base
#include "PeakFreaks.h"
#include "StandardProgram.h"

///====================================== Main Execution ==========================================
int main(int pArgsTotal, char **pArgs)
{
    return StandardMain(pArgsTotal, pArgs);
}
