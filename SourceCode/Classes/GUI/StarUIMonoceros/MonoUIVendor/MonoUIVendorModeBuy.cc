//--Base
#include "MonoUIVendor.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "AdvUIVendorStructures.h"
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

///===================================== Segment Rendering ========================================
void MonoUIVendor::RenderLftBuy(float pVisAlpha)
{
    AdvUIVendor::RenderLftBuy(pVisAlpha);
}
void MonoUIVendor::RenderTopBuy(float pVisAlpha)
{
    AdvUIVendor::RenderTopBuy(pVisAlpha);
}
void MonoUIVendor::RenderRgtBuy(float pVisAlpha)
{
    AdvUIVendor::RenderRgtBuy(pVisAlpha);
}
void MonoUIVendor::RenderBotBuy(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Render]
    //--Tabs, titles.
    RenderModeTabs(cColorAlpha);
    RenderModeTitles(cColorAlpha);

    //--Backings. Rendered apart from the entries, allowing the cursor to be over the backing and under
    //  the text entries.
    float cEntrySpcH = 56.0f;
    int tBackingsToRender = mShopInventory->GetListSize();
    if(tBackingsToRender > cxMonoScrollPageSize) tBackingsToRender = cxMonoScrollPageSize;
    for(int i = 0; i < tBackingsToRender; i ++)
    {
        MonoImages.rOverlay_Option->Draw(0.0f, i * cEntrySpcH);
    }

    //--Cursor.
    MonoImages.rOverlay_Selected->Draw(0.0f, mHighlightPos.mYCur - MonoImages.rOverlay_Selected->GetYOffset());

    //--Render entries.
    RenderShopItemListing(mSkip, cxMonoScrollPageSize, cColorAlpha);

    //--Scrollbar.
    if(mShopInventory->GetListSize() > cxMonoScrollPageSize)
    {
        StandardRenderScrollbar(mSkip, cxMonoScrollPageSize, mShopInventory->GetListSize() - cxMonoScrollPageSize, 239.0f, 445.0f, false, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    ///--[Help Strings]
    //--Strings that always display.
    mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--This string only renders if character swapping is possible.
    if(mShowSwapArrows)
        mCharacterChangeString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    //--This string only renders if multiple compare slots are present.
    if(mCanSwapCompareSlot)
        mToggleCompareString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderUnalignedBuy(float pVisAlpha)
{
    AdvUIVendor::RenderUnalignedBuy(pVisAlpha);
}

///===================================== Common Rendering =========================================
void MonoUIVendor::RenderBuyProperties(float pVisAlpha)
{
    RenderComparisonPane(GetConsideredItem(), pVisAlpha);
}
void MonoUIVendor::RenderBuyItem(float pVisAlpha)
{
    ///--[Documentation]
    //--When the player chooses an item to purchase, this UI appears over the rest of the UI. It shows
    //  what they are purchasing, how much it costs, and allows the quantity to be adjusted.
    //--Same as the base version, but uses translatable strings.
    float cPurchasePct = EasingFunction::QuadraticInOut(mBuyTimer, cxAdvBuyTicks) * pVisAlpha;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cPurchasePct));

    ///--[Item Resolve]
    //--Get the item package.
    ShopInventoryPack *rPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mCursor);
    if(!rPack) return;

    //--Fast-access pointer.
    AdventureItem *rItem = rPack->mItem;
    if(!rItem) return;

    ///--[Rendering]
    //--Position.
    float cColorAlpha = AutoPieceOpen(DIR_UP, cPurchasePct);

    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--Heading.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextConfirmPurchase);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Centering computations.
    char *tTextBuf = InitializeString("%s x %i", rItem->GetDisplayName(), mBuyQuantity);
    float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tTextBuf);
    float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

    //--Item Icon
    StarBitmap *rItemIcon = rItem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 205.0f);

    //--Compute the item cost.
    int tItemCost = rItem->GetValue();
    if(rPack->mPriceOverride > -1) tItemCost = rPack->mPriceOverride;
    int tTotalCost = tItemCost * mBuyQuantity;

    //--Item name.
    char *tRenderString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mTextCostsPlatina, 2, "[iCost]", tTotalCost, "[iCash]", AdventureInventory::Fetch()->GetPlatina());
    AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, tTextBuf);
    AdvImages.rFont_Mainline->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 230.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tRenderString);
    free(tRenderString);

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(579.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mAdjustQuantityString-> DrawText(757.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();

    //--Clean.
    free(tTextBuf);
}
void MonoUIVendor::RenderBuyEquipItem(float pVisAlpha)
{
    ///--[Documentation]
    //--This display pops up when the player purchases an item and asks them to equip it.
    //--Identical to the base version, but uses translatable strings.
    float cPurchasePct = EasingFunction::QuadraticInOut(mAskToEquipTimer, cxAdvAskEquipTicks) * pVisAlpha;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cPurchasePct));

    ///--[Item Resolve]
    //--Get the character who should equip it.
    AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(mAskToEquipSlot);
    if(!rCharacter) return;

    //--Get their name.
    const char *rEquipName = rCharacter->GetDisplayName();
    if(!rEquipName) return;

    ///--[Rendering]
    //--Position.
    AutoPieceOpen(DIR_UP, cPurchasePct);

    //--Frame.
    AdvImages.rFrame_BuyItemPopup->Draw();

    //--Text.
    AdvImages.rFont_Heading->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 175.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextEquipThisItemTo, rEquipName);

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR,                          1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(993.0f, cYRenderConfirmCancelText, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
