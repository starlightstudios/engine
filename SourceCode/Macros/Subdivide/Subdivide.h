///========================================= Subdivide ============================================
//--Subdivision functions. Takes in a string and breaks it into smaller strings. Useful for fitting lines into a console.

#pragma once

///========================================= Includes =============================================
#include <stdint.h>
class StarFont;
class StarLinkedList;

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define SUBDIVIDE_FLAG_COMMON_BREAKPOINTS 0x01
#define SUBDIVIDE_FLAG_FONTLENGTH 0x02
#define SUBDIVIDE_FLAG_ALLOW_BACKTRACK 0x04
#define SUBDIVIDE_FLAG_DIAGNOSTICS 0x80

///========================================== Classes =============================================
class Subdivide
{
    private:
    //--System
    Subdivide();

    protected:

    public:
    ///--Splitting
    static char *SubdivideString(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, StarFont *pFont, float pFontScale);
    static char *SubdivideStringBraces(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, StarFont *pFont, float pFontScale);
    static StarLinkedList *SubdivideStringToList(const char *pString, const char *pDelimiters);
    static StarLinkedList *SubdivideStringToListTags(const char *pString);

    ///--Tags
    static char *GetItemNameAndQuantity(const char *pString, int &sQuantity);
    static char *ReplaceTagsWithStrings(const char *pString, int pReplaceEntries, ...);

    ///--Font Specialized
    static StarLinkedList *SubdivideStringFlags(const char *pString, uint32_t pFlags, StarFont *pFont, float pLength);
    static int HandleSpecialSequences(const char *pString);
};

//--Hooking Functions

