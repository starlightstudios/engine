//--Base
#include "StarUIPiece.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///======================================= Documentation ==========================================
//--Automation routines are those that call the other routines in an organized way that is common
//  to a lot of UI pieces, but may not always be used because of variations in the functionality
//  of UIs in strange ways that a programmer can't really predict.

///======================================== Positioning ===========================================
float StarUIPiece::AutoPieceOpen(int pDirection, float pVisAlpha)
{
    ///--[Documentation]
    //--Automatically combines StandardPieceOpen() and HandlePositioning() to position the object and returns the expected
    //  color alpha based on the provided direction and visibility alpha. Also stores those values so AutoPieceClose()
    //  does not require arguments.
    //--Use this if you do not need to know the X/Y offsets of the object for rendering reasons. It just saves a few calls.
    //--Returns the color alpha, which is the color mixer value based on the visibility alpha and the mode. If not using fades,
    //  the color alpha should be 1.0f.

    ///--[Execution]
    //--Take existing positions and add them to the stack.
    TranslateStackPack *nPackage = (TranslateStackPack *)starmemoryalloc(sizeof(TranslateStackPack));
    nPackage->mXOffset = mLastOffsetX;
    nPackage->mYOffset = mLastOffsetY;
    mTranslateStack->AddElementAsHead("X", nPackage, &FreeThis);

    //--Get render positions.
    float tColorAlpha;
    StandardPieceOpen(pDirection, pVisAlpha, mLastOffsetX, mLastOffsetY, tColorAlpha);

    //--Translate.
    HandlePositioning(mLastOffsetX * 1.0f, mLastOffsetY * 1.0f, pVisAlpha);

    //--Pass back the color alpha.
    return tColorAlpha;
}
void StarUIPiece::AutoPieceClose()
{
    ///--[Documentation]
    //--Called after AutoPieceOpen() to undo the translation if one occurred.

    ///--[Execution]
    //--Undo the translation. Color mixer is always set to 1.0f alpha.
    HandlePositioning(mLastOffsetX * -1.0f, mLastOffsetY * -1.0f, 1.0f);

    //--Get the head of the translation stack. Stop if it doesn't exist.
    TranslateStackPack *rStackHead = (TranslateStackPack *)mTranslateStack->GetHead();
    if(!rStackHead) return;

    //--Set the offsets to the previous stack head, then remove it.
    mLastOffsetX = rStackHead->mXOffset;
    mLastOffsetY = rStackHead->mYOffset;
    mTranslateStack->DeleteHead();
}

///========================================= Controls =============================================
bool StarUIPiece::AutoListControl(const char *pDecrement, const char *pIncrement, const char *pAccelerate, int &sCursor, int &sSkip, int pListSize, int pEdge, int pSizePerPage, bool pAllowWrap)
{
    ///--[Documentation]
    //--Pressing up and down, or left and right, to move across a list is an extremely common behavior.
    //  This function handles all the smaller calls necessary to do that, including playing SFX.
    //--If the function returns true, it means it handled the update. It is possible that no control
    //  was pressed, in which case it returns false.
    //--It is legal for any of the control names to be NULL as this means they will be ignored.
    //  If both pIncrement and pDecrement are NULL then the routine stops.
    if(!pDecrement && !pIncrement) return false;

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Decrement]
    //--The user must be pressing the decrement control, not pressing the increment control, and the
    //  decrement control must exist.
    if(pDecrement && rControlManager->IsFirstPress(pDecrement))
    {
        //--Check if a countervailing press is occurring from the increment key. If it is, stop executing
        //  immediately as neither is considered pressed.
        if(pIncrement && rControlManager->IsFirstPress(pIncrement)) return false;

        //--Resolve how many iterations to move. Holding down pAccelerate will increase this but
        //  it is legal to pass NULL to disallow acceleration.
        int tIterations = -1;
        if(pAccelerate && rControlManager->IsDown(pAccelerate)) tIterations = -10;

        //--Run routine. If the cursor moved, call the recompute function.
        if(StandardCursor(sCursor, sSkip, tIterations, pListSize, pEdge, pSizePerPage, pAllowWrap))
        {
            RecomputeCursorPositions();
        }

        //--End update, play a sound effect.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Increment]
    //--If we got this far, the player may be pressing the increment key. They are not pressing the
    //  decrement key so there is no need to check the countervailing keypress.
    if(pIncrement && rControlManager->IsFirstPress(pIncrement))
    {
        //--Resolve how many iterations to move. Holding down pAccelerate will increase this but
        //  it is legal to pass NULL to disallow acceleration.
        int tIterations = 1;
        if(pAccelerate && rControlManager->IsDown(pAccelerate)) tIterations = 10;

        //--Run routine. If the cursor moved, call the recompute function.
        if(StandardCursor(sCursor, sSkip, tIterations, pListSize, pEdge, pSizePerPage, pAllowWrap))
        {
            RecomputeCursorPositions();
        }

        //--End update, play a sound effect.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[No Keypress]
    //--Neither of the keys was pressed so don't handle the update.
    return false;
}
bool StarUIPiece::AutoListUpDn(int &sCursor, int &sSkip, int pListSize, int pEdge, int pSizePerPage, bool pAllowWrap)
{
    ///--[Documentation]
    //--Version of AutoListControl() that uses "Up", "Down", "Ctrl" in a single, shorter, line.
    return AutoListControl("Up", "Down", "Ctrl", sCursor, sSkip, pListSize, pEdge, pSizePerPage, pAllowWrap);
}
bool StarUIPiece::AutoListLfRt(int &sCursor, int &sSkip, int pListSize, int pEdge, int pSizePerPage, bool pAllowWrap)
{
    ///--[Documentation]
    //--Version of AutoListControl() that uses "Left", "Right", "Ctrl" in a single, shorter, line.
    return AutoListControl("Left", "Right", "Ctrl", sCursor, sSkip, pListSize, pEdge, pSizePerPage, pAllowWrap);
}

///==================================== Controls, No Scroll =======================================
bool StarUIPiece::AutoListControlNoScroll(const char *pDecrement, const char *pIncrement, const char *pAccelerate, int &sCursor, int pListSize, bool pAllowWrap)
{
    ///--[Documentation]
    //--Same as above, but does not bother with scrollbar variables.
    if(!pDecrement && !pIncrement) return false;

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Decrement]
    //--The user must be pressing the decrement control, not pressing the increment control, and the
    //  decrement control must exist.
    if(pDecrement && rControlManager->IsFirstPress(pDecrement))
    {
        //--Check if a countervailing press is occurring from the increment key. If it is, stop executing
        //  immediately as neither is considered pressed.
        if(pIncrement && rControlManager->IsFirstPress(pIncrement)) return false;

        //--Resolve how many iterations to move. Holding down pAccelerate will increase this but
        //  it is legal to pass NULL to disallow acceleration.
        int tIterations = -1;
        if(pAccelerate && rControlManager->IsDown(pAccelerate)) tIterations = -10;

        //--Run routine. If the cursor moved, call the recompute function.
        if(StandardCursorNoScroll(sCursor, tIterations, pListSize, pAllowWrap))
        {
            RecomputeCursorPositions();
        }

        //--End update, play a sound effect.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Increment]
    //--If we got this far, the player may be pressing the increment key. They are not pressing the
    //  decrement key so there is no need to check the countervailing keypress.
    if(pIncrement && rControlManager->IsFirstPress(pIncrement))
    {
        //--Resolve how many iterations to move. Holding down pAccelerate will increase this but
        //  it is legal to pass NULL to disallow acceleration.
        int tIterations = 1;
        if(pAccelerate && rControlManager->IsDown(pAccelerate)) tIterations = 10;

        //--Run routine. If the cursor moved, call the recompute function.
        if(StandardCursorNoScroll(sCursor, tIterations, pListSize, pAllowWrap))
        {
            RecomputeCursorPositions();
        }

        //--End update, play a sound effect.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[No Keypress]
    //--Neither of the keys was pressed so don't handle the update.
    return false;
}
bool StarUIPiece::AutoListUpDnNoScroll(int &sCursor, int pListSize, bool pAllowWrap)
{
    ///--[Documentation]
    //--Version of AutoListControl() that uses "Up", "Down", "Ctrl" in a single, shorter, line.
    return AutoListControlNoScroll("Up", "Down", "Ctrl", sCursor, pListSize, pAllowWrap);
}
bool StarUIPiece::AutoListLfRtNoScroll(int &sCursor, int pListSize, bool pAllowWrap)
{
    ///--[Documentation]
    //--Version of AutoListControl() that uses "Left", "Right", "Ctrl" in a single, shorter, line.
    return AutoListControlNoScroll("Left", "Right", "Ctrl", sCursor, pListSize, pAllowWrap);
}

///====================================== Help Automation =========================================
bool StarUIPiece::AutoHelpOpen(const char *pOpenA, const char *pOpenB, const char *pOpenC)
{
    ///--[Documentation]
    //--Checks if any of the three provided controls are pressed. If they are, shows the help menu and returns true.
    //  If no input was handled, returns false.
    //--If a string is NULL, it is not checked.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Run checks. If any of the three exist and are pressed, Open.
    if( (pOpenA && rControlManager->IsFirstPress(pOpenA)) || (pOpenB && rControlManager->IsFirstPress(pOpenB)) || (pOpenC && rControlManager->IsFirstPress(pOpenC)))
    {
        mIsShowingHelp = true;
        RefreshMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    //--No input.
    return false;
}
void StarUIPiece::AutoHelpClose(const char *pCloseA, const char *pCloseB, const char *pCloseC)
{
    ///--[Documentation]
    //--The help menu, by default, closes if any of the provided controls are pressed.
    //--If a string is NULL, it is not checked.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Run checks. If any of the three exist and are pressed, close.
    if( (pCloseA && rControlManager->IsFirstPress(pCloseA)) || (pCloseB && rControlManager->IsFirstPress(pCloseB)) || (pCloseC && rControlManager->IsFirstPress(pCloseC)))
    {
        mIsShowingHelp = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
