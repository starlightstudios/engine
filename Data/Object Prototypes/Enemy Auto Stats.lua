-- |[ ==================================== Enemy Auto Stats ==================================== ]|
--Enemy that can be appended to an existing enemy stat chart. Uses a set of grades to decide
-- the stats rather than manual stat input.
local iArgsTotal = LM_GetNumOfArgs()
if(iArgsTotal < 1) then return end

--Argument resolve.
local ciSwitchCode = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Construction ====================================== ]|
if(ciSwitchCode == ObjectPrototype.ciScriptCreate) then
    
    -- |[Setup]|
    --Create.
    SOB_CreateEntry("Enemy Auto Stats")
    
    --System.
    SOP_SetProperty("Execution Path", LM_GetCallStack(0))
    
    --Description.
    SOP_SetProperty("Set Description Auto", 
        "Enemy that will be appended to a stat chart if specified. The stats of this enemy will be generated through a set of formulae.")

    -- |[Fields]|
    --File to edit.
    SOP_SetProperty("Add Integer Field",              "File To Edit", 0)
    SOP_SetProperty("Allocate Enumerations In Field", "File To Edit", 3)
    SOP_SetProperty("Set Enumerations In Field",      "File To Edit", 0, 0, "Output to textfile")
    for i = 1, #gzaPathListing, 1 do
        SOP_SetProperty("Set Enumerations In Field", "File To Edit", i, gzaPathListing[i].iEnumIndex, gzaPathListing[i].sDisplayName)
    end
    SOP_SetProperty("Set Enumerations Locking Flag",  "File To Edit", true)
    SOP_SetProperty("Set Field Description",          "File To Edit", "Which file to attempt to output to. You must set up paths in DeveloperLocal/Object Prototype Paths.lua")
    
    --Basic system.
    SOP_SetProperty("Add String Field",      "Enemy Name", "New Enemy")
    SOP_SetProperty("Set Field Description", "Enemy Name", "Name of the enemy.")
    
    SOP_SetProperty("Add String Field",      "Display Name", "Null")
    SOP_SetProperty("Set Field Description", "Display Name", "Name of the enemy as seen in game. Leave as Null to use the base name.")
    
    SOP_SetProperty("Add Integer Field",     "Level", 1)
    SOP_SetProperty("Set Field Description", "Level", "The level of the enemy. This determines resistance rates and is the primary entry for the autogeneration of stats.[BR]"..
                                                      "Note: Levels start from 1, but level 1 is level 0 for formulas.")
    
    --Primary stats.
    ObjectPrototype:fnStatisticGenEnums("Health")
    ObjectPrototype:fnStatisticGenEnums("Attack")
    ObjectPrototype:fnStatisticGenEnums("Ini")
    ObjectPrototype:fnStatisticGenEnums("Acc")
    ObjectPrototype:fnStatisticGenEnums("Evd")
    ObjectPrototype:fnStatisticGenEnums("Stun")

    --Protection.
    SOP_SetProperty("Add Integer Field",     "Protection", 0)
    SOP_SetProperty("Set Field Description", "Protection", "'Defense' value against direct damage, uses an exponential scale. Most enemies have 0 protection.")

    --Resistances.
    local saResistList = {"Slash", "Strike", "Pierce", "Flame", "Freeze", "Shock", "Crusade", "Obscure", "Bleed", "Poison", "Corrode", "Terrify"}
    for i = 1, #saResistList, 1 do
        SOP_SetProperty("Add Integer Field",     "Resist " .. saResistList[i], 0)
        SOP_SetProperty("Set Field Description", "Resist " .. saResistList[i], "Resistance against the named damage type. Affects direct damage, dots, and chance to have an effect apply.")
    end
    
    --Rewards.
    ObjectPrototype:fnStatisticGenEnums("Platina")
    ObjectPrototype:fnStatisticGenEnums("Experience")
    
    SOP_SetProperty("Add Integer Field",     "JP", 25)
    SOP_SetProperty("Set Field Description", "JP", "Default JP award for parity of level. JP rewards decrease if the player outlevels the enemy.")
    
    SOP_SetProperty("Add String Field",      "Drop", "Null")
    SOP_SetProperty("Set Field Description", "Drop", "Name of the item this enemy can drop. Can be Null to drop nothing.")
    
    SOP_SetProperty("Add Integer Field",     "Drop Chance", 10)
    SOP_SetProperty("Set Field Description", "Drop Chance", "Chance for enemy to drop the Drop item. 100 is guaranteed.")

    --Behaviors
    SOP_SetProperty("Add String Field",      "AI String", "Null")
    SOP_SetProperty("Set Field Description", "AI String", "Name of AIString this enemy uses for its AI. Can be Null to use the stock AI.")
    
    --Display.
    SOP_SetProperty("Add String Field",      "Portrait", "Null")
    SOP_SetProperty("Set Field Description", "Portrait", "Path to the combat portrait of the enemy.")
    
    SOP_SetProperty("Add String Field",      "Turn Portrait", "Null")
    SOP_SetProperty("Set Field Description", "Turn Portrait", "Path to the turn portrait of the enemy. If Null, attempts to use the portrait path value.")

    -- |[Finish Up]|
    --Finish Up.
    DL_PopActiveObject()

-- |[ ======================================= Execution ======================================== ]|
elseif(ciSwitchCode == ObjectPrototype.ciScriptExecute) then

    -- |[ =================== Setup ==================== ]|
    -- |[Diagnostics]|
    local iTotalFields = SOP_GetProperty("Total Fields")
    
    -- |[Setup]|
    local saResistList = {"Slash", "Strike", "Pierce", "Flame", "Freeze", "Shock", "Crusade", "Obscure", "Bleed", "Poison", "Corrode", "Terrify"}

    -- |[Fields]|
    local iFileToEdit     = SOP_GetProperty("Get Integer Field", "File To Edit")
    local sEnemyName      = SOP_GetProperty("Get String Field",  "Enemy Name")
    local sDisplayName    = SOP_GetProperty("Get String Field",  "Display Name")
    local iLevel          = SOP_GetProperty("Get Integer Field", "Level")
    local iStatHealth     = SOP_GetProperty("Get Integer Field", "Health")
    local iStatAttack     = SOP_GetProperty("Get Integer Field", "Attack")
    local iStatInitiative = SOP_GetProperty("Get Integer Field", "Ini")
    local iStatAccuracy   = SOP_GetProperty("Get Integer Field", "Acc")
    local iStatEvade      = SOP_GetProperty("Get Integer Field", "Evd")
    local iStatStun       = SOP_GetProperty("Get Integer Field", "Stun")
    local iProtection     = SOP_GetProperty("Get Integer Field", "Protection")
    local iaResistances = {}
    for i = 1, #saResistList, 1 do
        iaResistances[i] = SOP_GetProperty("Get Integer Field", "Resist " .. saResistList[i])
    end
    local iRewardPlatina    = SOP_GetProperty("Get Integer Field", "Platina")
    local iRewardExperience = SOP_GetProperty("Get Integer Field", "Experience")
    local iRewardJP         = SOP_GetProperty("Get Integer Field", "JP")
    local sRewardDrop       = SOP_GetProperty("Get String Field",  "Drop")
    local iRewardDropChance = SOP_GetProperty("Get Integer Field", "Drop Chance")
    local sAIString         = SOP_GetProperty("Get String Field",  "AI String")
    local sPortrait         = SOP_GetProperty("Get String Field",  "Portrait")
    local sTurnPortrait     = SOP_GetProperty("Get String Field",  "Turn Portrait")
    
    -- |[Substitutions/ Range Checks]|
    if(sDisplayName  == "Null") then sDisplayName = sEnemyName end
    if(iLevel        < 1)       then iLevel = 1 end
    if(sTurnPortrait == "Null") then sTurnPortrait = sPortrait end
    
    -- |[ =========== Statistic Derivations ============ ]|
    --Activate chapter 1.
    StatGenerator:fnSetMode("Chapter 1")
    
    --Generate stats.
    local iFinalHP   = StatGenerator:fnGenerateStat(gciStatIndex_HPMax,      iLevel-1, iStatHealth)
    local iFinalAtk  = StatGenerator:fnGenerateStat(gciStatIndex_Attack,     iLevel-1, iStatAttack)
    local iFinalIni  = StatGenerator:fnGenerateStat(gciStatIndex_Initiative, iLevel-1, iStatInitiative)
    local iFinalAcc  = StatGenerator:fnGenerateStat(gciStatIndex_Accuracy,   iLevel-1, iStatAccuracy)
    local iFinalEvd  = StatGenerator:fnGenerateStat(gciStatIndex_Evade,      iLevel-1, iStatEvade)
    local iFinalStun = StatGenerator:fnGenerateStat(gciStatIndex_StunCap,    iLevel-1, iStatStun)
    local iFinalPlt  = StatGenerator:fnGenerateStat(gciStatIndex_Platina,    iLevel-1, iRewardPlatina)
    local iFinalExp  = StatGenerator:fnGenerateStat(gciStatIndex_Experience, iLevel-1, iRewardExperience)
    local iFinalJP   = 25
    
    -- |[ ============== Line Construction ============= ]|
    local sAdderLine = [[fnAdd("]]..sEnemyName..[[", "]]..sDisplayName..[[", "Sword", ]]..iLevel..[[, ]]..iFinalHP..[[, ]]..iFinalAtk..[[, ]]..iFinalIni..[[, ]]..iFinalAcc..[[, ]]..iFinalEvd..[[, ]]..iFinalStun..[[,]]..
                       [[]]..iProtection..[[, ]]..iaResistances[1]..[[, ]]..iaResistances[2]..[[, ]]..iaResistances[3]..[[, ]]..iaResistances[4]..[[, ]]..iaResistances[5]..[[, ]]..iaResistances[6]..
                       [[, ]]..iaResistances[7]..[[, ]]..iaResistances[8]..[[, ]]..iaResistances[9]..[[, ]]..iaResistances[10]..[[, ]]..iaResistances[11]..[[, ]]..iaResistances[12]..[[, ]]..iFinalPlt..
                       [[, ]]..iFinalExp..[[, ]]..iFinalJP..[[, "]]..sRewardDrop..[[", ]]..iRewardDropChance..[[)]]
    local sAIStringLine = [[fnApplyAIString("]]..sEnemyName..[[", "]]..sAIString..[[")]]
    local sPortraitLine = [[fnAddPortrait("Root/Images/Portraits/Combat/]]..sPortrait..[[", "Root/Images/AdventureUI/TurnPortraits/]]..sTurnPortrait..[[",  "Null")]]
    local sRemapLine = [[fnSetRemap("]]..sEnemyName..[[", "Root/Images/Portraits/Combat/]]..sPortrait..[[")]]
    sAdderLine = ObjectPrototype:fnRespaceLine(sAdderLine, {-26, -20, -9, 4, 7, 7, 4, 4, 4, 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 6, 4, 3, -23, 1})
    
    -- |[ ============= Output to Textfile ============= ]|
    --Special instance, outputs directly to a textfile and assumes the user will copy the lines into the file of their choice.
    if(iFileToEdit == 0) then

        -- |[Open File]|
        --Generate a filename.
        local sFilename = ObjectPrototype:fnGeneratePathname("Enemy Auto Stats ")
        if(sFilename == "Null") then
            io.write("Warning. Attempted to generate a unique filename but failed.\nPrototype: " .. fnResolvePath() .. "\n")
            return
        end
        
        --Open/create it.
        local fOutfile = io.open(sFilename, "w")
        
        -- |[Write]|
        fOutfile:write(sAdderLine .. "\n")
        if(sAIString ~= "Null") then
            fOutfile:write(sAIStringLine .. "\n")
        end
        fOutfile:write(sPortraitLine .. "\n")
        fOutfile:write(sRemapLine .. "\n")
        
        -- |[Finish]|
        --Close the file.
        fOutfile:close()
        
        --Report success to the console.
        io.write("Successfully wrote file: " .. sFilename .. "\n")
        
    -- |[ ========== Output to Existing File =========== ]|
    else
    
        -- |[Path Check]|
        --Get file pathname from the enumerations.
        local sPathname = "Null"
        for i = 1, #gzaPathListing, 1 do
            if(gzaPathListing[i].iEnumIndex == iFileToEdit) then
                sPathname = gzaPathListing[i].sExecPath
                break
            end
        end
        
        --Error.
        if(sPathname == "Null") then
            io.write("Error. Unable to write file, path enumeration was not found. Value: " .. iFileToEdit .. "\n")
            return
        end
        
        -- |[File Storage]|
        --This stores all of the lines that make up the file as individual strings in an array.
        local saFileStrings = {}
    
        -- |[Make Backup]|
        --Create a backup path.
        local sBackupPath = ObjectPrototype:fnGeneratePathname("Enemy Auto Stats Backup ")
        if(sBackupPath == "Null") then
            io.write("Warning. Attempted to generate a unique backup filename but failed.\nPrototype: " .. fnResolvePath() .. "\n")
            return
        end
        
        --Display.
        io.write("Pathname: " .. sPathname .. "\n")
        
        --Open the backup for write, and the file path for read.
        local fBackupFile = io.open(sBackupPath, "w")
        local fNormalFile = io.open(sPathname, "r")
        
        --Copy. Also store the file strings into the storage table.
        local i = 1
        local sFileLine = fNormalFile:read("*l")
        while(sFileLine ~= nil and i < 10000) do
            i = i + 1
            table.insert(saFileStrings, sFileLine)
            fBackupFile:write(sFileLine .. "\n")
            sFileLine = fNormalFile:read("*l")
        end
        
        --Finish backups.
        fBackupFile:close()
        fNormalFile:close()
        
        -- |[Insert fnAdd() Lines]|
        --Search for the final instance of fnAdd() in the file. Append the new entry after it.
        local iLastAddLine = ObjectPrototype:fnGetLastLineStartingWith(saFileStrings, "fnAdd(", "Add Insertion")
        table.insert(saFileStrings, iLastAddLine+1, sAdderLine)
        
        -- |[Insert fnApplyAIString() Line]|
        --Search for the final instance of fnApplyAIString() in the file, append the new entry. Note that if the AIString was "Null", do nothing.
        if(sAIString ~= "Null") then
            local iLastAIStringLine = ObjectPrototype:fnGetLastLineStartingWith(saFileStrings, "fnApplyAIString(", "AIString Insertion")
            table.insert(saFileStrings, iLastAIStringLine+1, sAIStringLine)
        end
        
        -- |[Portrait / Remap Lines]|
        --Look for fnAddPortrait() and fnSetRemap().
        local iLastPortraitLine = ObjectPrototype:fnGetLastLineStartingWith(saFileStrings, "fnAddPortrait(", "Add Portrait")
        table.insert(saFileStrings, iLastPortraitLine+1, sPortraitLine)
        local iLastRemapLine = ObjectPrototype:fnGetLastLineStartingWith(saFileStrings, "fnSetRemap(", "Add Remap")
        table.insert(saFileStrings, iLastRemapLine+1, sRemapLine)
        
        -- |[Write out]|
        --Write to backup for testing.
        fBackupFile = io.open(sBackupPath, "w")
        for i = 1, #saFileStrings, 1 do
            fBackupFile:write(saFileStrings[i] .. "\n")
        end
        fBackupFile:close()
    end
end