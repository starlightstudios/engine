-- |[ ================================== StatGenerator Common ================================== ]|
--Common to all modes. Allows mode changing and routing.

-- |[ ============================= StatGenerator:fnRegisterMode() ============================= ]|
--Registers the given mode to the global list.
function StatGenerator:fnRegisterMode(pzMode)
    
    -- |[Argument Check]|
    if(pzMode == nil) then return end

    -- |[Duplicate Check]|
    for i = 1, #self.zaValidModes, 1 do
        if(self.zaValidModes[i].sName == pzMode.sName) then
            io.write("StatGenerator:fnRegisterMode() - Warning, mode with name " .. pzMode.sName .. " already exists. Failing.\n")
            return
        end
    end
    
    -- |[Register]|
    table.insert(self.zaValidModes, pzMode)
end

-- |[ =============================== StatGenerator:fnSetMode() ================================ ]|
--Changes the internal mode or barks an error if mode not found.
function StatGenerator:fnSetMode(psMode)
    
    -- |[Argument Check]|
    if(psMode == nil) then return end
    
    -- |[Validity Check]|
    for i = 1, #self.zaValidModes, 1 do
        if(self.zaValidModes[i].sName == psMode) then
            self.zrActiveMode = self.zaValidModes[i]
            return
        end
    end
    
    -- |[Not Found]|
    io.write("StatGenerator:fnSetMode() - Warning, mode " .. psMode .. " was not found.\n")
end

-- |[ =========================== StatGenerator:fnCommonRangeCheck() =========================== ]|
--Returns common range-checks for the level and the grade so it doesn't need to be repeated. Also
-- performs a nil-check and provides stock values if one happens, while barking a warning.
function StatGenerator:fnCommonRangeCheck(piLevel, piGradeIndex)
    
    -- |[Argument Check]|
    if(piLevel == nil) then
        io.write("StatGenerator:fnCommonRangeCheck() - Error, level was nil.\n")
        return 0, 4
    end
    if(piGradeIndex == nil) then
        io.write("StatGenerator:fnCommonRangeCheck() - Error, grade index was nil.\n")
        return 0, 4
    end

    -- |[Range Checks]|
    --Level must be 0 or higher. Remember the display value of 1 is 0 in the backend.
    if(piLevel < 0) then piLevel = 0 end
    
    --The grade index must be in the enumerated set defined in ObjectPrototypeEnums.lua which
    -- ranks 0 = F and 8 = S++. Clamp to that range.
    if(piGradeIndex < 0) then piGradeIndex = 0 end
    if(piGradeIndex > 8) then piGradeIndex = 8 end
    
    -- |[Finish]|
    return piLevel, piGradeIndex
end

-- |[ ============================= StatGenerator:fnGenerateStat() ============================= ]|
--Given a stat, level, and grade, generates and returns the value for that stat using the currently
-- active internal mode. If no mode is active or on other error, returns 1.
function StatGenerator:fnGenerateStat(piStatIndex, piLevel, piGrade)
    
    -- |[Argument Check]|
    if(piStatIndex == nil) then return 1 end
    if(piLevel     == nil) then return 1 end
    if(piGrade     == nil) then return 1 end

    -- |[Activity Check]|
    --A mode must be active.
    if(self.zrActiveMode == nil) then
        io.write("StatGenerator:fnGenerateStat() - Error, no mode is currently active.\n")
        return 1
    end

    -- |[Range Check]|
    --Must be in the range of enumerations specified in the core file.
    if(piStatIndex <  gciStatIndex_Platina       or piStatIndex >  gciStatIndex_StunCap or piStatIndex == gciStatIndex_MPRegen or piStatIndex == gciStatIndex_CPMax or
       piStatIndex == gciStatIndex_FreeActionMax or piStatIndex == gciStatIndex_FreeActionGen) then
        io.write("StatGenerator:fnGenerateStat() - Error, stat index " .. piStatIndex .. " is out of range or unhandled. Failing.\n")
        return 1
    end

    -- |[Select Function for Test]|
    --Resolve the function but don't call it yet.
    local fnFunctionPointer = nil
    if(piStatIndex == gciStatIndex_HPMax) then
        fnFunctionPointer = self.zrActiveMode.fnGenerateHealth
    elseif(piStatIndex == gciStatIndex_Attack) then
        fnFunctionPointer = self.zrActiveMode.fnGenerateAttack
    elseif(piStatIndex == gciStatIndex_Initiative) then
        fnFunctionPointer = self.zrActiveMode.fnGenerateInitiative
    elseif(piStatIndex == gciStatIndex_Accuracy) then
        fnFunctionPointer = self.zrActiveMode.fnGenerateAccuracy
    elseif(piStatIndex == gciStatIndex_Evade) then
        fnFunctionPointer = self.zrActiveMode.fnGenerateEvade
    elseif(piStatIndex == gciStatIndex_StunCap) then
        fnFunctionPointer = self.zrActiveMode.fnGenerateStun
    elseif(piStatIndex == gciStatIndex_Platina) then
        fnFunctionPointer = self.zrActiveMode.fnGeneratePlatina
    elseif(piStatIndex == gciStatIndex_Experience) then
        fnFunctionPointer = self.zrActiveMode.fnGenerateExperience
    end

    --Error check.
    if(fnFunctionPointer == nil) then
        io.write("StatGenerator:fnGenerateStat() - Error, function in active mode failed to resolve. Index: " .. piStatIndex .. ". Failing.\n")
        return 1
    end

    -- |[Call It]|
    --Note: The function is an internal class function. Lua doesn't handle static pointers so we need to add a "this" pointer to the front.
    return fnFunctionPointer(StatGenerator, piLevel, piGrade)
end
