-- |[ ================================ ObjectPrototype File I/O ================================ ]|
--Contains getters and setters for ModInfo objects.

-- |[ ========================== ObjectPrototype:fnGeneratePathname() ========================== ]|
--Given a base string, generates a new path that is guaranteed to be unique. This is done by just
-- incrementing the files created counter by 1 and checking if a file already exists of that name
-- to prevent accidental overwrites.
--Returns the new path. If an error occurred, returns "Null".
function ObjectPrototype:fnGeneratePathname(psBaseName)
    
    -- |[Argument Check]|
    if(psBaseName == nil) then return "Null" end
    
    -- |[Generate]|
    --Begin iterating.
    local iAttempts = 0
    while(iAttempts < 100) do
    
        --Generate a name.
        local sNewFile = ObjectPrototype.csExportDir .. psBaseName .. ObjectPrototype.iTotalFilesCreated .. ".lua"
        ObjectPrototype.iTotalFilesCreated = ObjectPrototype.iTotalFilesCreated + 1
    
        --Check if this file already exists. If not, pass back the valid path.
        if(FS_Exists(sNewFile) == false) then
            return sNewFile
    
        --File exists, re-run the loop and try again.
        else
            iAttempts = iAttempts + 1
        end
    end
    
    -- |[Failure]|
    --If we got this far, the generator hit the softlock cap for some reason. Fail.
    return "Null"
end

-- |[ ====================== ObjectPrototype:fnGetLastLineStartingWith() ======================= ]|
--Given an array of strings, returns the last line in the array that starts with the string provided.
-- If no string is found, returns the length of the string and barks the provided warning.
function ObjectPrototype:fnGetLastLineStartingWith(psaStrings, psToFind, psError)
    
    -- |[Argument Check]|
    if(psaStrings == nil) then return 0 end
    if(psToFind   == nil) then return 0 end
    if(psError    == nil) then return 0 end
    
    -- |[Setup]|
    local iToFindLen = string.len(psToFind)
    
    -- |[Scan]|
    --Iterate across the array. Whenever a match is found, mark it.
    local iLastAddLine = -1
    for i = 1, #psaStrings, 1 do
        if(string.sub(psaStrings[i], 1, iToFindLen) == psToFind) then
            iLastAddLine = i
        end
    end
    
    --If no entry was found at all, bark the error code provided.
    if(iLastAddLine == -1) then
        iLastAddLine = #psaStrings
        io.write("Warning, output file had no instances of " .. psToFind .. ". Appending to end. Error code: " .. psError .. "\n")
    end
    
    -- |[Finish]|
    return iLastAddLine
end