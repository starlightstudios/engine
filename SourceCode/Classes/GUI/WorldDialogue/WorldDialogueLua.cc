//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueTopic.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

void WorldDialogue::HookToLuaState(lua_State *pLuaState)
{
    /* [Topics]
       WD_GetProperty("Total Topics") (1 Integer)
       WD_GetProperty("Topic Name", iSlot) (1 String)
       WD_GetProperty("Topic Level", iSlot) (1 Integer)
       WD_GetProperty("Topic NPCs Total", iSlot) (1 Integer)
       WD_GetProperty("Topic NPC Name", iSlot, iTopicSlot) (1 String)
       WD_GetProperty("Topic NPC Level", iSlot, iTopicSlot) (1 Integer)
       WD_GetProperty("Is Topic Read", sTopicName) (1 Boolean)

       [Options]
       WD_GetProperty("Is Auto Hastening") (1 Boolean)
       Returns the requested property from the WorldDialogue. */
    lua_register(pLuaState, "WD_GetProperty", &Hook_WD_GetProperty);

    /* [Static]
       WD_SetProperty("Construct") (Static)
       WD_SetProperty("Topic Directory", sPath) (Static)
       WD_SetProperty("Set Unload On Close Flag", bFlag) (Static)
       WD_SetProperty("Set Use Mouse", bFlag) (Static)

       [System]
       WD_SetProperty("Set Leader Voice", sSpeakerName)
       WD_SetProperty("Clear")
       WD_SetProperty("Wipe Topic Data")
       WD_SetProperty("Set All Topics To Default")
       WD_SetProperty("Restore Previous Actors")
       WD_SetProperty("Autoresolve Target", bFlag)
       WD_SetProperty("Debug Print Actors")
       WD_SetProperty("Debug Print Actor Info", sActorName)

       [Showing and Hiding]
       WD_SetProperty("Show")
       WD_SetProperty("FastShow")
       WD_SetProperty("Hide")

       [Credits Mode]
       WD_SetProperty("Activate Credits", sBackgroundPath)
       WD_SetProperty("Set Credits Total Ticks", iTicks)
       WD_SetProperty("Create Credits Pack")

       [Text]
       WD_SetProperty("Append", sString)
       WD_SetProperty("Continuation", sScriptPath, sFiringString)

       [Major Sequence]
       WD_SetProperty("Major Sequence", bFlag)
       WD_SetProperty("Major Sequence Fast", bFlag)
       WD_SetProperty("Visual Novel", bFlag)

       [Actors]
       WD_SetProperty("Speaker", sSpeakerName)
       WD_SetProperty("Actor In Slot", iSlot, sReferenceName)
       WD_SetProperty("Actor In Slot", iSlot, sReferenceName, sEmotion)
       WD_SetProperty("Actor Emotion", sReferenceName, sEmotion)

       [Decisions]
       WD_SetProperty("Activate Decisions")
       WD_SetProperty("Add Decision", sText, sScriptPath, sFiringString)

       [Scenes]
       WD_SetProperty("Activate Scene")
       WD_SetProperty("Activate Scene Fast")
       WD_SetProperty("Transition Scene", sImgA, sImgB)
       WD_SetProperty("Activate Flash", iStartTicks)
       WD_SetProperty("Deactivate Scene")
       WD_SetProperty("Clear Censor Bars")
       WD_SetProperty("Register Censor Bar", fLft, fTop, fWid, fHei)
       WD_SetProperty("Send Censor Bars To Crossfade")
       WD_SetProperty("Set Ignore Scene Offsets", bFlag)
       WD_SetProperty("Set Scene Offsets", fX, fY)
       WD_SetProperty("Set Scene Fade Time", iTicks)
       WD_SetProperty("Set Scene Image", sDLPath)
       WD_SetProperty("Set Scene Image Fast", sDLPath)

       [Animations]
       WD_SetProperty("Allocate Animation", iFrameCount, iTicksPerFrame, iStartTicks)
       WD_SetProperty("Set Animation Frame", iSlot, sDLPath)

       [Topics]
       WD_SetProperty("Register Topic", sTopicName, sDisplayName, iStartingLevel)
       WD_SetProperty("Register Topic For", sTopicName, sNPCName, iStartingLevel)
       WD_SetProperty("Unlock Topic", sTopicName, iMinimumLevel)
       WD_SetProperty("Clear Topic Read", sTopicName)
       WD_SetProperty("Activate Topics After Dialogue", sNPCName)
       WD_SetProperty("Activate Topics After Dialogue", sNPCName, sGoodbyeName)
       WD_SetProperty("Topic NPC Level", sTopicName, sNPCName, iLevel)

       [Options]
       WD_SetProperty("Register Voice", sVoiceName, sVoiceSound)
       WD_SetProperty("Set Dialogue Hiding", bFlag)
       WD_SetProperty("Set AutoHasten", bFlag)
       WD_SetProperty("Add Name Remap", sRemapName, sFinalName)
       WD_SetProperty("Add Background Remap", sRemapName, sPath)

       [String Entry]
       WD_SetProperty("Activate String Entry", sPostExecPath)

       Sets the requested property in the WorldDialogue. */
    lua_register(pLuaState, "WD_SetProperty", &Hook_WD_SetProperty);

    /* Append(sDialogue)
       Shorthand for WD_SetProperty("Append", sDialogue). */
    lua_register(pLuaState, "Append", &Hook_WD_AppendShorthand);

    //--Subroutine for CreditsPacks.
    WorldDialogue::HookToLuaStateCredits(pLuaState);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_WD_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Topics]
    //WD_GetProperty("Total Topics") (1 Integer)
    //WD_GetProperty("Topic Name", iSlot) (1 String)
    //WD_GetProperty("Topic Level", iSlot) (1 Integer)
    //WD_GetProperty("Topic NPCs Total", iSlot) (1 Integer)
    //WD_GetProperty("Topic NPC Name", iSlot, iTopicSlot) (1 String)
    //WD_GetProperty("Topic NPC Level", iSlot, iTopicSlot) (1 Integer)
    //WD_GetProperty("Is Topic Read", sTopicName) (1 Boolean)

    //--[Options]
    //WD_GetProperty("Is Auto Hastening") (1 Boolean)

    ///--[Setup]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("WD_GetProperty");

    //--Active object.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Topics]
    //--How many topics are currently in the world dialogue.
    if(!strcasecmp(rSwitchType, "Total Topics") && tArgs == 1)
    {
        StarLinkedList *rTopicsList = rWorldDialogue->GetTopicList();
        lua_pushinteger(L, rTopicsList->GetListSize());
        tReturns = 1;
    }
    //--Name of the topic in the given slot.
    else if(!strcasecmp(rSwitchType, "Topic Name") && tArgs == 2)
    {
        StarLinkedList *rTopicsList = rWorldDialogue->GetTopicList();
        DialogueTopic *rTopic = (DialogueTopic *)rTopicsList->GetElementBySlot(lua_tointeger(L, 2));
        if(rTopic)
        {
            lua_pushstring(L, rTopic->GetInternalName());
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--Level of the topic in the given slot.
    else if(!strcasecmp(rSwitchType, "Topic Level") && tArgs == 2)
    {
        StarLinkedList *rTopicsList = rWorldDialogue->GetTopicList();
        DialogueTopic *rTopic = (DialogueTopic *)rTopicsList->GetElementBySlot(lua_tointeger(L, 2));
        if(rTopic)
        {
            lua_pushinteger(L, rTopic->GetLevel());
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--NPCs associated with the topic in the given slot.
    else if(!strcasecmp(rSwitchType, "Topic NPCs Total") && tArgs == 2)
    {
        StarLinkedList *rTopicsList = rWorldDialogue->GetTopicList();
        DialogueTopic *rTopic = (DialogueTopic *)rTopicsList->GetElementBySlot(lua_tointeger(L, 2));
        if(rTopic)
        {
            StarLinkedList *rNPCList = rTopic->GetNPCList();
            lua_pushinteger(L, rNPCList->GetListSize());
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--Name of the NPC in the given slot of the given topic.
    else if(!strcasecmp(rSwitchType, "Topic NPC Name") && tArgs == 3)
    {
        StarLinkedList *rTopicsList = rWorldDialogue->GetTopicList();
        DialogueTopic *rTopic = (DialogueTopic *)rTopicsList->GetElementBySlot(lua_tointeger(L, 2));
        if(rTopic)
        {
            StarLinkedList *rNPCList = rTopic->GetNPCList();
            const char *rName = rNPCList->GetNameOfElementBySlot(lua_tointeger(L, 3));
            if(rName)
            {
                lua_pushstring(L, rName);
            }
            else
            {
                lua_pushstring(L, "Null");
            }
        }
        else
        {
            lua_pushstring(L, "Null");
        }
        tReturns = 1;
    }
    //--Level of the NPC with the given topic in the given slot.
    else if(!strcasecmp(rSwitchType, "Topic NPC Level") && tArgs == 3)
    {
        StarLinkedList *rTopicsList = rWorldDialogue->GetTopicList();
        DialogueTopic *rTopic = (DialogueTopic *)rTopicsList->GetElementBySlot(lua_tointeger(L, 2));
        if(rTopic)
        {
            StarLinkedList *rNPCList = rTopic->GetNPCList();
            DTNPCRegPack *rPackage = (DTNPCRegPack *)rNPCList->GetElementBySlot(lua_tointeger(L, 3));
            if(rPackage)
            {
                lua_pushinteger(L, rPackage->mLastSpokeLevel);
            }
            else
            {
                lua_pushinteger(L, 0);
            }
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--Whether or not a given topic is read. Shows pink for unread topics.
    else if(!strcasecmp(rSwitchType, "Is Topic Read") && tArgs == 2)
    {
        lua_pushboolean(L, rWorldDialogue->IsTopicRead(lua_tostring(L, 2)));
        tReturns = 1;
    }
    ///--[Options]
    //--Whether or not the auto-hasten flag is set.
    else if(!strcasecmp(rSwitchType, "Is Auto Hastening") && tArgs == 1)
    {
        lua_pushboolean(L, rWorldDialogue->IsAutoHastening());
        tReturns = 1;
    }
    ///--[Error]
    else
    {
        LuaPropertyError("WD_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_WD_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //WD_SetProperty("Construct") (Static)
    //WD_SetProperty("Topic Directory", sPath) (Static)
    //WD_SetProperty("Set Unload On Close Flag", bFlag) (Static)
    //WD_SetProperty("Set Use Mouse", bFlag) (Static)

    //--[System]
    //WD_SetProperty("Set Leader Voice", sSpeakerName)
    //WD_SetProperty("Clear")
    //WD_SetProperty("Wipe Topic Data")
    //WD_SetProperty("Set All Topics To Default")
    //WD_SetProperty("Restore Previous Actors")
    //WD_SetProperty("Autoresolve Target", bFlag)
    //WD_SetProperty("Debug Print Actors")
    //WD_SetProperty("Debug Print Actor Info", sActorName)

    //--[Showing and Hiding]
    //WD_SetProperty("Show")
    //WD_SetProperty("FastShow")
    //WD_SetProperty("Hide")

    //--[Credits Mode]
    //WD_SetProperty("Activate Credits", sBackgroundPath)
    //WD_SetProperty("Set Credits Total Ticks", iTicks)
    //WD_SetProperty("Create Credits Pack")

    //--[Text]
    //WD_SetProperty("Append", sString)
    //WD_SetProperty("Continuation", sScriptPath, sFiringString)

    //--[Major Sequence]
    //WD_SetProperty("Major Sequence", bFlag)
    //WD_SetProperty("Major Sequence Fast", bFlag)
    //WD_SetProperty("Visual Novel", bFlag)

    //--[Actors]
    //WD_SetProperty("Speaker", sSpeakerName)
    //WD_SetProperty("Actor In Slot", iSlot, sReferenceName)
    //WD_SetProperty("Actor In Slot", iSlot, sReferenceName, sEmotion)
    //WD_SetProperty("Actor Emotion", sReferenceName, sEmotion)

    //--[Decisions]
    //WD_SetProperty("Activate Decisions")
    //WD_SetProperty("Add Decision", sText, sScriptPath, sFiringString)

    //--[Scenes]
    //WD_SetProperty("Activate Scene")
    //WD_SetProperty("Activate Scene Fast")
    //WD_SetProperty("Transition Scene", sImgA, sImgB)
    //WD_SetProperty("Activate Flash", iStartTicks)
    //WD_SetProperty("Deactivate Scene")
    //WD_SetProperty("Set Ignore Scene Offsets", bFlag)
    //WD_SetProperty("Clear Censor Bars")
    //WD_SetProperty("Register Censor Bar", fLft, fTop, fWid, fHei)
    //WD_SetProperty("Send Censor Bars To Crossfade")
    //WD_SetProperty("Set Scene Offsets", fX, fY)
    //WD_SetProperty("Set Scene Fade Time", iTicks)
    //WD_SetProperty("Set Scene Image", sDLPath)
    //WD_SetProperty("Set Scene Image Fast", sDLPath)

    //--[Animations]
    //WD_SetProperty("Allocate Animation", iFrameCount, iTicksPerFrame, iStartTicks)
    //WD_SetProperty("Set Animation Frame", iSlot, sDLPath)

    //--[Topics]
    //WD_SetProperty("Register Topic", sTopicName, sDisplayName, iStartingLevel)
    //WD_SetProperty("Register Topic For", sTopicName, sNPCName, iStartingLevel)
    //WD_SetProperty("Unlock Topic", sTopicName, iMinimumLevel)
    //WD_SetProperty("Clear Topic Read", sTopicName)
    //WD_SetProperty("Activate Topics After Dialogue", sNPCName)
    //WD_SetProperty("Activate Topics After Dialogue", sNPCName, sGoodbyeName)
    //WD_SetProperty("Topic NPC Level", sTopicName, sNPCName, iLevel)

    //--[Options]
    //WD_SetProperty("Register Voice", sVoiceName, sVoiceSound)
    //WD_SetProperty("Set Dialogue Hiding", bFlag)
    //WD_SetProperty("Set AutoHasten", bFlag)
    //WD_SetProperty("Add Name Remap", sRemapName, sFinalName)
    //WD_SetProperty("Add Background Remap", sRemapName, sPath)

    //--[String Entry]
    //WD_SetProperty("Activate String Entry", sPostExecPath)

    ///--[Setup]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("WD_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Statics]
    if(!strcasecmp(rSwitchType, "Construct") && tArgs == 1)
    {
        WorldDialogue::Fetch()->Construct();
        return 0;
    }
    else if(!strcasecmp(rSwitchType, "Topic Directory") && tArgs == 2)
    {
        ResetString(WorldDialogue::xTopicsDirectory, lua_tostring(L, 2));
        return 0;
    }
    //--Static flag, see documentation in WorldDialogue.cc
    else if(!strcasecmp(rSwitchType, "Set Unload On Close Flag") && tArgs == 2)
    {
        WorldDialogue::xUnloadDialoguePortraitsWhenClosing = lua_toboolean(L, 2);
        return 0;
    }
    //--Newly created dialogues use the mouse for decisions.
    else if(!strcasecmp(rSwitchType, "Set Use Mouse") && tArgs >= 2)
    {
        WorldDialogue::xDialogueUsesMouse = lua_toboolean(L, 2);
        return 0;
    }

    ///--[Dynamic]
    //--Active object.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();

    ///--[System]
    //--Set the voice used by the [VOICE|Leader] tag. It should be the party leader. Used for examining
    //  objects and some world interaction.
    if(!strcasecmp(rSwitchType, "Set Leader Voice") && tArgs == 2)
    {
        rWorldDialogue->SetLeaderVoice(lua_tostring(L, 2));
    }
    //--Clears dialogue off the object.
    else if(!strcasecmp(rSwitchType, "Clear") && tArgs == 1)
    {
        rWorldDialogue->Clear();
    }
    //--Clear all topic data. Use during startup only.
    else if(!strcasecmp(rSwitchType, "Wipe Topic Data") && tArgs == 1)
    {
        rWorldDialogue->WipeTopicData();
    }
    else if(!strcasecmp(rSwitchType, "Set All Topics To Default") && tArgs == 1)
    {
        rWorldDialogue->SetAllTopicsToDefaults();
    }
    //--Replace all the previous actors immediately. Used when the dialogue closed and re-opened.
    else if(!strcasecmp(rSwitchType, "Restore Previous Actors") && tArgs == 1)
    {
        rWorldDialogue->RestorePreviousActors();
    }
    //--Marks the dialogue to automatically resolve which dialogue box to use during major sequences.
    else if(!strcasecmp(rSwitchType, "Autoresolve Target") && tArgs == 2)
    {
        rWorldDialogue->SetTargetAutoresolve(lua_toboolean(L, 2));
    }
    //--Debug command, writes all actors to the console.
    else if(!strcasecmp(rSwitchType, "Debug Print Actors") && tArgs == 1)
    {
        rWorldDialogue->WriteActorsToConsole();
    }
    //--Debug command, writes a specific actor's detailed info to the console.
    else if(!strcasecmp(rSwitchType, "Debug Print Actor Info") && tArgs == 2)
    {
        rWorldDialogue->WriteActorsInfoToConsole(lua_tostring(L, 2));
    }
    ///--[Showing and Hiding]
    //--Show the WorldDialogue. Also implicitly adds the dialogue lockout.
    else if(!strcasecmp(rSwitchType, "Show") && tArgs == 1)
    {
        rWorldDialogue->Show();
        rWorldDialogue->AddDialogueLockout();
    }
    //--Show the WorldDialogue, and then make it fade instantly.
    else if(!strcasecmp(rSwitchType, "FastShow") && tArgs == 1)
    {
        rWorldDialogue->Show();
        rWorldDialogue->AddDialogueLockout();
        rWorldDialogue->BypassFade();
    }
    //--Hide the WorldDialogue immediately.
    else if(!strcasecmp(rSwitchType, "Hide") && tArgs == 1)
    {
        rWorldDialogue->CloseImmediately();
    }
    ///--[Credits Mode]
    //--Activates credits mode.
    else if(!strcasecmp(rSwitchType, "Activate Credits") && tArgs == 2)
    {
        rWorldDialogue->ActivateCreditsMode();
        rWorldDialogue->SetCreditsBackground(lua_tostring(L, 2));
    }
    //--Total ticks in the credits.
    else if(!strcasecmp(rSwitchType, "Set Credits Total Ticks") && tArgs == 2)
    {
        rWorldDialogue->SetCreditsTimeMax(lua_tointeger(L, 2));
    }
    //--Creates and pushes a new CreditsPack.
    else if(!strcasecmp(rSwitchType, "Create Credits Pack") && tArgs == 1)
    {
        rWorldDialogue->CreateCreditsPack();
    }
    ///--[Text]
    //--Append text to the display.
    else if(!strcasecmp(rSwitchType, "Append") && tArgs == 2)
    {
        rWorldDialogue->AppendString(lua_tostring(L, 2), true);
    }
    //--When the dialogue ends, fire this script with this string to continue the dialogue (otherwise it closes).
    else if(!strcasecmp(rSwitchType, "Continuation") && tArgs == 3)
    {
        rWorldDialogue->SetContinuationScript(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Major Sequence]
    //--Activate or deactivate a major dialogue sequence. This modifies the UI and shows portraits.
    else if(!strcasecmp(rSwitchType, "Major Sequence") && tArgs == 2)
    {
        rWorldDialogue->SetMajorSequence(lua_toboolean(L, 2));
    }
    //--Activate or deactivate a major dialogue sequence. This modifies the UI and shows portraits.
    else if(!strcasecmp(rSwitchType, "Major Sequence Fast") && tArgs == 2)
    {
        rWorldDialogue->SetMajorSequence(lua_toboolean(L, 2));
        rWorldDialogue->BypassFade();
    }
    //--Visual Novel mode, changes rendering order and position, adds some new effects.
    else if(!strcasecmp(rSwitchType, "Visual Novel") && tArgs == 2)
    {
        rWorldDialogue->SetVisualNovelMode(lua_toboolean(L, 2));
    }
    ///--[Actors]
    //--Who is speaking during a major sequence. This causes actors to darken if they are not speaking. Pass "null" to clear darkening.
    else if(!strcasecmp(rSwitchType, "Speaker") && tArgs == 2)
    {
        rWorldDialogue->SetSpeaker(lua_tostring(L, 2));
    }
    //--Who is rendered in the given slot.
    else if(!strcasecmp(rSwitchType, "Actor In Slot") && tArgs == 3)
    {
        rWorldDialogue->AddActorToSlot(lua_tostring(L, 3), lua_tointeger(L, 2));
    }
    //--Who is rendered in the given slot, with their emotion as well.
    else if(!strcasecmp(rSwitchType, "Actor In Slot") && tArgs == 4)
    {
        rWorldDialogue->AddActorToSlot(lua_tostring(L, 3), lua_tointeger(L, 2));
        rWorldDialogue->SetActorEmotionS(lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Emotion of an Actor, by name.
    else if(!strcasecmp(rSwitchType, "Actor Emotion") && tArgs == 3)
    {
        rWorldDialogue->SetActorEmotionS(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Decisions]
    //--Begins decision mode if it wasn't already active.
    else if(!strcasecmp(rSwitchType, "Activate Decisions") && tArgs == 1)
    {
        rWorldDialogue->SetDecisionMode(true);
    }
    //--Adds a new decision in decision mode. Does nothing if decisions are not active.
    else if(!strcasecmp(rSwitchType, "Add Decision") && tArgs == 4)
    {
        rWorldDialogue->AddDecision(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    ///--[Scenes]
    //--Activates scenes mode, used for CGs and TF sequences.
    else if(!strcasecmp(rSwitchType, "Activate Scene") && tArgs == 1)
    {
        rWorldDialogue->ActivateScenesMode();
    }
    //--Activates scenes mode, used for CGs and TF sequences. Skips the fading part.
    else if(!strcasecmp(rSwitchType, "Activate Scene Fast") && tArgs == 1)
    {
        rWorldDialogue->ActivateScenesMode();
        rWorldDialogue->BypassFade();
    }
    //--Combines the "Show", "Activate Scene Fast", and two "Set Scene Image" calls to quickly transition
    //  a scene. Typically used during TF scenes with multiple entries.
    else if(!strcasecmp(rSwitchType, "Transition Scene") && tArgs == 3)
    {
        rWorldDialogue->Show();
        rWorldDialogue->AddDialogueLockout();
        rWorldDialogue->ActivateScenesMode();
        rWorldDialogue->BypassFade();
        rWorldDialogue->ChangeSceneImage(lua_tostring(L, 2));
        rWorldDialogue->ChangeSceneImage(lua_tostring(L, 3));
    }
    //--Flashes bright white. Used for rune animations.
    else if(!strcasecmp(rSwitchType, "Activate Flash") && tArgs == 2)
    {
        rWorldDialogue->ActivateFlash(lua_tointeger(L, 2));
    }
    //--Shuts off scenes mode and returns to factory-zero. Does not clear dialogue!
    else if(!strcasecmp(rSwitchType, "Deactivate Scene") && tArgs == 1)
    {
        rWorldDialogue->DeactivateScenesMode();
    }
    //--Sets whether or not the scene will use auto-centering with offsets.
    else if(!strcasecmp(rSwitchType, "Set Ignore Scene Offsets") && tArgs == 2)
    {
        rWorldDialogue->SetIgnoreSceneOffsets(lua_toboolean(L, 2));
    }
    //--Removes all censor bars, new and old.
    else if(!strcasecmp(rSwitchType, "Clear Censor Bars") && tArgs == 1)
    {
        rWorldDialogue->ClearCensorBars();
    }
    //--Register a new censor bar to the current set.
    else if(!strcasecmp(rSwitchType, "Register Censor Bar") && tArgs == 5)
    {
        rWorldDialogue->RegisterCensorBar(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Move the current set of censor bars to the old set.
    else if(!strcasecmp(rSwitchType, "Send Censor Bars To Crossfade") && tArgs == 1)
    {
        rWorldDialogue->SendCensorBarsToCrossfade();
    }
    //--Sets how far offset from dead-center the scene image renders.
    else if(!strcasecmp(rSwitchType, "Set Scene Offsets") && tArgs == 3)
    {
        rWorldDialogue->SetSceneOffsets(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Sets how many ticks the scene uses to crossfade.
    else if(!strcasecmp(rSwitchType, "Set Scene Fade Time") && tArgs == 2)
    {
        rWorldDialogue->SetSceneFadeTimers(lua_tointeger(L, 2));
    }
    //--Sets the scene image by a DL path. Does *not* reset the offsets!
    else if(!strcasecmp(rSwitchType, "Set Scene Image") && tArgs == 2)
    {
        rWorldDialogue->ChangeSceneImage(lua_tostring(L, 2));
    }
    //--Sets the scene image by a DL path, going to fullbright immediately. Does *not* reset the offsets!
    else if(!strcasecmp(rSwitchType, "Set Scene Image Fast") && tArgs == 2)
    {
        rWorldDialogue->ChangeSceneImageInstantly(lua_tostring(L, 2));
    }
    ///--[Animations]
    //--Allocates space for an animation to play. Pass 0 for the frame count to deactivate animations.
    else if(!strcasecmp(rSwitchType, "Allocate Animation") && tArgs == 4)
    {
        rWorldDialogue->AllocateSceneAnimFrames(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets a specific animation frame.
    else if(!strcasecmp(rSwitchType, "Set Animation Frame") && tArgs == 3)
    {
        rWorldDialogue->SetSceneAnimFrame(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    ///--[Topics]
    //--Creates a new topic and registers it. Does nothing if the topic already exists.
    else if(!strcasecmp(rSwitchType, "Register Topic") && tArgs == 4)
    {
        rWorldDialogue->RegisterTopic(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Indicates an NPC can respond to a given topic at a given level.
    else if(!strcasecmp(rSwitchType, "Register Topic For") && tArgs == 4)
    {
        rWorldDialogue->RegisterTopicFor(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Upgrades the level of the topic to the provided level if it is lower.
    else if(!strcasecmp(rSwitchType, "Unlock Topic") && tArgs == 3)
    {
        rWorldDialogue->UnlockTopic(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--If a topic has been read already, marks it as unread.
    else if(!strcasecmp(rSwitchType, "Clear Topic Read") && tArgs == 2)
    {
        rWorldDialogue->ClearTopicReadFlag(lua_tostring(L, 2));
    }
    //--Flags dialogue to go to topics mode once the current dialogue sequence is over. Sets the Goodbye topic to "Goodbye"
    //  if a name is not provided.
    else if(!strcasecmp(rSwitchType, "Activate Topics After Dialogue") && tArgs == 2)
    {
        rWorldDialogue->ActivateTopicsModeAfterDialogue(lua_tostring(L, 2));
    }
    //--Flags dialogue to go to topics mode once the current dialogue sequence is over, and changes the name of the Goodbye
    //  option to the provided name.
    else if(!strcasecmp(rSwitchType, "Activate Topics After Dialogue") && tArgs == 3)
    {
        rWorldDialogue->ActivateTopicsModeAfterDialogue(lua_tostring(L, 2));
        rWorldDialogue->SetGoodbyeName(lua_tostring(L, 3));
    }
    //--Level of the NPC with the given topic in the given slot.
    else if(!strcasecmp(rSwitchType, "Topic NPC Level") && tArgs == 4)
    {
        StarLinkedList *rTopicsList = rWorldDialogue->GetTopicList();
        DialogueTopic *rTopic = (DialogueTopic *)rTopicsList->GetElementByName(lua_tostring(L, 2));
        if(rTopic)
        {
            StarLinkedList *rNPCList = rTopic->GetNPCList();
            DTNPCRegPack *rPackage = (DTNPCRegPack *)rNPCList->GetElementByName(lua_tostring(L, 3));
            if(rPackage)
            {
                rPackage->mLastSpokeLevel = lua_tointeger(L, 4);
            }
        }
    }
    ///--[Options]
    //--Registers a voice for speaking characters.
    else if(!strcasecmp(rSwitchType, "Register Voice") && tArgs == 3)
    {
        rWorldDialogue->RegisterVoice(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Hides the dialogue box until Show is called again or flagged false.
    else if(!strcasecmp(rSwitchType, "Set Dialogue Hiding") && tArgs == 2)
    {
        rWorldDialogue->SetHideFlag(lua_toboolean(L, 2));
    }
    //--Automatically hastens dialogue as if the Z key were held down.
    else if(!strcasecmp(rSwitchType, "Set AutoHasten") && tArgs == 2)
    {
        rWorldDialogue->SetAutoHastenFlag(lua_toboolean(L, 2));
    }
    //--Causes a name to be swapped with another name in-stride.
    else if(!strcasecmp(rSwitchType, "Add Name Remap") && tArgs == 3)
    {
        rWorldDialogue->AddSpeakerRemap(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Allows a longer path to be abbreviated into a shorter one for background names.
    else if(!strcasecmp(rSwitchType, "Add Background Remap") && tArgs == 3)
    {
        rWorldDialogue->RegisterBackgroundRemap(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[String Entry]
    //--Begins string entry mode.
    else if(!strcasecmp(rSwitchType, "Activate String Entry") && tArgs == 2)
    {
        rWorldDialogue->ActivateStringEntry(lua_tostring(L, 2));
    }
    ///--[Error Handler]
    //--Error.
    else
    {
        LuaPropertyError("WD_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
int Hook_WD_AppendShorthand(lua_State *L)
{
    //Append(sDialogue)

    //--Setup
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Append");

    //--Execute
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    rWorldDialogue->AppendString(lua_tostring(L, 1), true);
    return 0;
}
