//--Base
#include "WordCard.h"

//--Classes
#include "WordCombat.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers

//--[Local Definitions]
#define WORDCARD_PAD_X 14.0f
#define WORDCARD_PAD_Y 14.0f

///========================================== System ==============================================
WordCard::WordCard()
{
    //--[WordCard]
    //--System
    mCardType = WORDCARD_TYPE_ACTION;
    mHighlightFlag = WC_HIGHLIGHT_IGNORE;
    rHighlightImg = NULL;
    mDisplayedWord = InitializeString("Null");
    rBackingImg = NULL;
    rRenderFont = NULL;
    rBackingL = NULL;
    rBackingM = NULL;
    rBackingR = NULL;

    //--Joker Card!
    mIsJokerCard = false;

    //--Properties
    mAndOfFlag = false;
    mElementalFlag = 0x00;
    mEffectType = WORDCARD_EFFECT_DAMAGE;
    mEffectSeverity = 1;

    //--Position
    mMoveTicks = 1;
    mMoveTicksMax = 1;
    mAssignedIndex = -1;
    mStartX = 0.0f;
    mStartY = 0.0f;
    mTargetX = 0.0f;
    mTargetY = 0.0f;
    mXPad = WORDCARD_PAD_X;
    mYPad = WORDCARD_PAD_Y;
    mDimensions.Set(0.0f, 0.0f, 1.0f, 1.0f);
}
WordCard::~WordCard()
{
    free(mDisplayedWord);
}
void WordCard::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((WordCard *)pPtr);
}

///===================================== Property Queries =========================================
bool WordCard::IsMoving()
{
    return (mMoveTicks < mMoveTicksMax);
}
int WordCard::GetHighlightFlag()
{
    return mHighlightFlag;
}
int WordCard::GetCardType()
{
    return mCardType;
}
uint8_t WordCard::GetElementalFlag()
{
    return mElementalFlag;
}
int WordCard::GetEffectType()
{
    return mEffectType;
}
int WordCard::GetEffectSeverity()
{
    return mEffectSeverity;
}
float WordCard::GetWidth()
{
    return mDimensions.GetWidth();
}
int WordCard::GetAssignedIndex()
{
    return mAssignedIndex;
}
TwoDimensionReal WordCard::GetDimensions()
{
    return mDimensions;
}
const char *WordCard::GetDisplayString()
{
    return mDisplayedWord;
}
bool WordCard::IsPointWithin(float pX, float pY)
{
    //--We use a "fat" click to make the cards easier to click in battle.
    float cLft = mDimensions.mLft - mXPad;
    float cTop = mDimensions.mTop - mYPad;
    float cRgt = mDimensions.mRgt + mXPad;
    float cBot = mDimensions.mBot + mYPad;
    return (pX >= cLft && pX <= cRgt && pY >= cTop && pY <= cBot);
}

///======================================= Manipulators ===========================================
void WordCard::SetCardType(int pFlag)
{
    mCardType = pFlag;
    if(mCardType < WORDCARD_TYPE_LOWERBND) mCardType = WORDCARD_TYPE_LOWERBND;
    if(mCardType > WORDCARD_TYPE_UPPERBND) mCardType = WORDCARD_TYPE_UPPERBND;
}
void WordCard::SetHighlightFlag(int pFlag, StarBitmap *pHighlightImg)
{
    mHighlightFlag = pFlag;
    rHighlightImg = pHighlightImg;
}
void WordCard::SetAndOfFlag()
{
    mAndOfFlag = true;
}
void WordCard::SetDisplayedWord(const char *pWord)
{
    if(!pWord) return;
    ResetString(mDisplayedWord, pWord);
}
void WordCard::SetBackingImage(StarBitmap *pBitmap)
{
    rBackingImg = pBitmap;
}
void WordCard::SetExpandableNameplates(StarBitmap *pLft, StarBitmap *pCnt, StarBitmap *pRgt)
{
    rBackingL = pLft;
    rBackingM = pCnt;
    rBackingR = pRgt;
}
void WordCard::SetFont(StarFont *pUseFont)
{
    rRenderFont = pUseFont;
}
void WordCard::SetElements(uint8_t pElementalFlag)
{
    mElementalFlag = pElementalFlag;
}
void WordCard::SetEffectType(int pFlag)
{
    mEffectType = pFlag;
    if(mEffectType < WORDCARD_TYPE_LOWERBND) mEffectType = WORDCARD_EFFECT_LOWERBND;
    if(mEffectType > WORDCARD_TYPE_UPPERBND) mEffectType = WORDCARD_EFFECT_UPPERBND;
}
void WordCard::SetEffectSeverity(int pSeverity)
{
    mEffectSeverity = pSeverity;
    if(mEffectSeverity < 1) mEffectSeverity = 1;
}
void WordCard::SetAssignedIndex(int pIndex)
{
    mAssignedIndex = pIndex;
}
void WordCard::MoveToInstantly(float pX, float pY)
{
    mDimensions.SetCenter(pX, pY);
    mMoveTicks = 1;
    mMoveTicksMax = 1;
}
void WordCard::MoveToSlide(float pX, float pY)
{
    //--Set.
    mStartX = mDimensions.mXCenter;
    mStartY = mDimensions.mYCenter;
    mTargetX = pX;
    mTargetY = pY;

    //--Compute time needed.
    float cDistance = GetPlanarDistance(mStartX, mStartY, mTargetX, mTargetY);
    float cTicksNeeded = ceil(cDistance / WORDCARD_MOVESPEED) + 5.0f;
    mMoveTicks = 0;
    mMoveTicksMax = (int)cTicksNeeded;
    if(mMoveTicksMax < 1) mMoveTicksMax = 1;
}

///======================================= Core Methods ===========================================
void WordCard::ComputeDimensions()
{
    //--Cards are always 56x85 pixels.
    float cWidth  = 56.0f;
    float cHeight = 85.0f;

    //--Now recompute these sizes.
    mDimensions.SetWH(mDimensions.mLft, mDimensions.mTop, cWidth, cHeight);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void WordCard::Update()
{
    //--Need to move to a location.
    if(mMoveTicks < mMoveTicksMax)
    {
        //--Compute and move.
        float cAngle = GetAngleBetween(mStartX, mStartY, mTargetX, mTargetY);
        float cDistance = GetPlanarDistance(mStartX, mStartY, mTargetX, mTargetY);
        float cPercent = EasingFunction::QuadraticInOut(mMoveTicks, mMoveTicksMax);
        float cNewX = mStartX + cosf(cAngle) * cDistance * cPercent;
        float cNewY = mStartY + sinf(cAngle) * cDistance * cPercent;
        mDimensions.SetCenter(cNewX, cNewY);

        //--Run timer.
        mMoveTicks ++;

        //--Finish.
        if(mMoveTicks >= mMoveTicksMax)
        {
            mDimensions.SetCenter(mTargetX, mTargetY);
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void WordCard::Render(bool pIsDarkened, bool pIsBlackout, float pAlpha)
{
    //--Error check.
    if(!rRenderFont || !mDisplayedWord) return;

    //--Variables.
    float cTexWid = rRenderFont->GetTextWidth(mDisplayedWord) * 0.50f;

    //--If darkened, mix this down a bit.
    float cMixer = 1.0f;
    if(pIsDarkened) cMixer = 0.75f;
    if(pIsBlackout) cMixer = 0.00f;
    StarlightColor::SetMixer(cMixer, cMixer, cMixer, 1.0f * pAlpha);

    //--Render the backing image.
    if(rBackingImg) rBackingImg->Draw(mDimensions.mXCenter - (rBackingImg->GetTrueWidth() * 0.50f), mDimensions.mYCenter - (rBackingImg->GetTrueHeight() * 0.50f));

    //--Backings.
    if(!mAndOfFlag && rBackingL && rBackingM && rBackingR)
    {
        //--Sizes.
        float cYOff = 42.0f;
        float tUseWid = cTexWid;
        if(tUseWid < 30) tUseWid = 30.0f;
        float tLft = mDimensions.mXCenter - tUseWid - 2.0f + 4.0f;
        float tRgt = mDimensions.mXCenter + tUseWid - 6.0f;
        float tTop = mDimensions.mYCenter         + cYOff;
        float tBot = mDimensions.mYCenter + 20.0f + cYOff;

        //--Render.
        rBackingL->Draw(mDimensions.mXCenter - tUseWid - 2.0f, mDimensions.mYCenter + cYOff);
        rBackingM->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 1.0f); glVertex2f(tLft, tTop);
            glTexCoord2f(1.0f, 1.0f); glVertex2f(tRgt, tTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(tRgt, tBot);
            glTexCoord2f(0.0f, 0.0f); glVertex2f(tLft, tBot);
        glEnd();
        rBackingR->Draw(mDimensions.mXCenter + tUseWid - 6.0f, mDimensions.mYCenter + cYOff);
    }

    //--Render the text.
    float cYOffset = 0.0f;
    if(mAndOfFlag) cYOffset = -52.0f;
    StarlightColor::SetMixer(1.0f * cMixer, 1.0f * cMixer, 1.0f * cMixer, 1.0f * pAlpha);
    rRenderFont->DrawText(mDimensions.mXCenter - cTexWid, mDimensions.mYCenter + 39.0f + cYOffset, mDisplayedWord);
    StarlightColor::ClearMixer();
}
void WordCard::RenderNoText(bool pIsDarkened, bool pIsBlackout, float pAlpha)
{
    //--Error check.
    if(!rRenderFont || !mDisplayedWord) return;

    //--Variables.
    float cTexWid = rRenderFont->GetTextWidth(mDisplayedWord) * 0.50f;

    //--If darkened, mix this down a bit.
    float cMixer = 1.0f;
    if(pIsDarkened) cMixer = 0.75f;
    if(pIsBlackout) cMixer = 0.00f;
    StarlightColor::SetMixer(cMixer, cMixer, cMixer, 1.0f * pAlpha);

    //--Render the backing image.
    if(rBackingImg) rBackingImg->Draw(mDimensions.mXCenter - (rBackingImg->GetTrueWidth() * 0.50f), mDimensions.mYCenter - (rBackingImg->GetTrueHeight() * 0.50f));

    //--Backings.
    if(!mAndOfFlag && rBackingL && rBackingM && rBackingR)
    {
        //--Sizes.
        float cYOff = 42.0f;
        float tUseWid = cTexWid;
        if(tUseWid < 30) tUseWid = 30.0f;
        float tLft = mDimensions.mXCenter - tUseWid - 2.0f + 4.0f;
        float tRgt = mDimensions.mXCenter + tUseWid - 6.0f;
        float tTop = mDimensions.mYCenter         + cYOff;
        float tBot = mDimensions.mYCenter + 20.0f + cYOff;

        //--Render.
        rBackingL->Draw(mDimensions.mXCenter - tUseWid - 2.0f, mDimensions.mYCenter + cYOff);
        rBackingM->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 1.0f); glVertex2f(tLft, tTop);
            glTexCoord2f(1.0f, 1.0f); glVertex2f(tRgt, tTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(tRgt, tBot);
            glTexCoord2f(0.0f, 0.0f); glVertex2f(tLft, tBot);
        glEnd();
        rBackingR->Draw(mDimensions.mXCenter + tUseWid - 6.0f, mDimensions.mYCenter + cYOff);
    }
}
void WordCard::RenderHighlight()
{
    //--Renders a highlight image over the card. The highlight should auto-center.
    if(!rHighlightImg || mHighlightFlag != WC_HIGHLIGHT) return;
    rHighlightImg->Draw(mDimensions.mXCenter - (rHighlightImg->GetTrueWidth() * 0.50f), mDimensions.mYCenter - (rHighlightImg->GetTrueHeight() * 0.50f));
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
