-- |[ ========================================= Loading ======================================== ]|
--Only used for loading images.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/Loading.slf")
ImageLump_SetCompression(1)

-- |[ ===================================== Splash Screen ====================================== ]|
--Setup.
local sSplashPath = sBasePath .. "Splash/"
for i = 0, 23, 1 do
    local sIntName = string.format("Splash|Logo%02i", i)
    local sImgPath = string.format(sSplashPath .. "Logo%02i.png", i)
    ImageLump_Rip(sIntName, sImgPath, 0, 0, -1, -1, 0)
end

-- |[ ======================================== Chapter 0 ======================================= ]|
--Setup.
local sChapter0Path = sBasePath .. "Chapter 0/"
ImageLump_Rip("Chapter0|Dummy",       sChapter0Path .. "Dummy.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Chapter0|Loading",     sChapter0Path .. "Loading.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Chapter0|LoadPeriod0", sChapter0Path .. "LoadPeriod0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter0|LoadPeriod1", sChapter0Path .. "LoadPeriod1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter0|LoadPeriod2", sChapter0Path .. "LoadPeriod2.png", 0, 0, -1, -1, 0)

--String Tyrant.
ImageLump_Rip("StringTyrant|Loading",  sChapter0Path .. "StringTyrantLoading.png", 0, 0, -1, -1, 0)
ImageLump_Rip("StringTyrant|Finished", sChapter0Path .. "StringTyrantFinished.png", 0, 0, -1, -1, 0)

-- |[ ======================================== Chapter 1 ======================================= ]|
--Setup.
local sChapter1Path = sBasePath .. "Chapter 1/"

--Christine and the Raiju running frames. Sprite sheets in filmstrip format.
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Chapter1|MeiBee", sChapter1Path .. "Mei Bee.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Standard UI pieces.
ImageLump_Rip("Chapter1|Backing",   sChapter1Path .. "Backing.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading00", sChapter1Path .. "Loading00.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading01", sChapter1Path .. "Loading01.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading02", sChapter1Path .. "Loading02.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading03", sChapter1Path .. "Loading03.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading04", sChapter1Path .. "Loading04.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading05", sChapter1Path .. "Loading05.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading06", sChapter1Path .. "Loading06.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading07", sChapter1Path .. "Loading07.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading08", sChapter1Path .. "Loading08.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading09", sChapter1Path .. "Loading09.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter1|Loading10", sChapter1Path .. "Loading10.png", 0, 0, -1, -1, 0)

-- |[ ======================================== Chapter 5 ======================================= ]|
--Setup.
local sChapter5Path = sBasePath .. "Chapter 5/"

--Christine and the Raiju running frames. Sprite sheets in filmstrip format.
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Chapter5|ChristineRun", sChapter5Path .. "ChristineRun.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Chapter5|RaijuRun",     sChapter5Path .. "RaijuRun.png",     0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Standard UI pieces.
ImageLump_Rip("Chapter5|Base",    sChapter5Path .. "Base.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Chapter5|Dot0",    sChapter5Path .. "Dot0.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Chapter5|Dot1",    sChapter5Path .. "Dot1.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Chapter5|Dot2",    sChapter5Path .. "Dot2.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Chapter5|Loading", sChapter5Path .. "Loading.png", 0, 0, -1, -1, 0)

-- |[ ======================================= Monoceros ======================================== ]|
--Setup.
local sMonocerosPath = sBasePath .. "Monoceros/"

--Standard UI pieces.
ImageLump_Rip("Monoceros|Frame0", sMonocerosPath .. "Load0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Monoceros|Frame1", sMonocerosPath .. "Load1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Monoceros|Frame2", sMonocerosPath .. "Load2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Monoceros|Frame3", sMonocerosPath .. "Load3.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Monoceros|Frame4", sMonocerosPath .. "Load4.png", 0, 0, -1, -1, 0)

-- |[ ======================================= Finish Up ======================================== ]|
--Finish
SLF_Close()