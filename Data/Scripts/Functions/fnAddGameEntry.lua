-- |[ ==================================== fnAddGameEntry() ==================================== ]|
--Used to add a new game entry to the list of playable games. The iNegativeLen value indicates how many
-- letters to trim off the end to get the basic path. Example:
-- "Games/AdventureMode/ZLaunch.lua"
-- string.sub("Games/AdventureMode/ZLaunch.lua", 1, -12)
-- Result: "Games/AdventureMode/"
-- Thus, the iNegativeLen value for adventure mode is -12.
function fnAddGameEntry(psName, psVarPath, psButtonText, piPriority, psFileLaunchPath, piNegativeLen)
    
    --Deprecated Warning
    if(gbPrintDeprecatedCallWarnings == true) then
        io.write("Warning: Calling fnAddGameEntry() is deprecated. Call from: " .. LM_GetCallStack(0) .. "\n")
    end
    
    --Call.
    SysPaths:fnAddGameEntry(psName, psVarPath, psButtonText, piPriority, psFileLaunchPath, piNegativeLen)
    return
    
    --[=[
    
    
    -- |[Argument Check]|
    if(psName           == nil) then return end
    if(psVarPath        == nil) then return end
    if(psButtonText     == nil) then return end
    if(piPriority       == nil) then return end
    if(psFileLaunchPath == nil) then return end
    if(piNegativeLen    == nil) then return end
    
    -- |[Duplicate Check]|
    --Scan the game entries list. If the game entry already exists, do nothing.
    for i = 1, #gsaGameEntries, 1 do
        if(gsaGameEntries[i].sName == psName) then return end
    end
    
    -- |[Create]|
    local zEntry = {}
    zEntry.sName = psName
    zEntry.sVarPath = psVarPath
    zEntry.sButtonText = psButtonText
    zEntry.iPriority = piPriority
    zEntry.sFileLaunchPath = psFileLaunchPath
    zEntry.sActivePath = "Null"
    zEntry.sSearchPaths = {}
    zEntry.iNegativeLen = piNegativeLen
    
    -- |[Register]|
    giGamesTotal = giGamesTotal + 1
    table.insert(gsaGameEntries, zEntry)
    ]=]
end