//--Base
#include "BlueSphereLevel.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarCamera3D.h"
#include "StarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "MapManager.h"

#define LEVELS_PER_PAGE 10

void BlueSphereLevel::ActivateLevelSelect(int pLevelsTotal)
{
    //--Flag reset.
    mIsLevelSelectMode = true;
    mLevelSelectPage = 0;
    mLevelSelectCursor = 0;
    AudioManager::Fetch()->PlayMusic("NULL");

    //--Pass a negative number to keep the previous results.
    if(pLevelsTotal >= 1)
    {
        //--Allocate and clear.
        mLevelsTotal = pLevelsTotal;
        SetMemoryData(__FILE__, __LINE__);
        mLevelResults = (int *)starmemoryalloc(sizeof(int) * mLevelsTotal);
        memset(mLevelResults, 0, sizeof(int) * mLevelsTotal);
    }

    //--[Clear]
    //--Reset everything back to game default.
    for(int x = 0; x < mXSize; x ++) free(mMapData[x]);
    free(mMapData);
    delete mLocalCamera;

    //--System
    mWaitTimer = (int)(5.841f * SECONDSTOTICKS);
    mBlueSpheresRemaining = 0;

    //--Map
    mXSize = 0;
    mYSize = 0;
    mMapData = NULL;

    //--Object Grid
    memset(mObjectGrid, 0, sizeof(int) * BSL_OBJ_X * BSL_OBJ_Y);

    //--Player Data
    mPlayerX = 0.0f;
    mPlayerY = 0.0f;
    mPlayerFacing = DIR_UP;
    mPlayerMoveProgress = 0.0f;

    //--Moving
    mTotalPlayerMoveTicks = 0;
    mColorCountOffset = 0;
    mPlayerMoveTicks = 0;
    mPlayerMoveTicksTotal = 0;
    mPlayerMoveTicksMax = 100;
    mPlayerSpeed = 6;
    mPlayerMoveProgress = 0.0f;

    //--Bumper Backwards Moving
    mBumperTurnCooldown = 0;
    mBumperBackingTicksLeft = 0;
    mBumperBackingTicksMax = BSL_BUMPER_DISTANCE;
    mBumperBackingProgress = 0;

    //--Turning
    mTurnDirection = BSL_TURN_NONE;
    mTurnTimer = 0;
    mTurnProgress = 0.0f;
    mTurnBase = 0.0f;

    //--Jumping
    mJumpDistanceLeft = 0;
    mJumpDistanceMax = BSL_JUMP_DISTANCE;
    mJumpCooldown = 0;

    //--Camera Position
    mLocalCamera = new StarCamera3D();
    mLocalCamera->SetPosition(-20.0f, 0.0f, -180.0f);
    mLocalCamera->SetRotation( 45.0f, 0.0f,   90.0f);

    //--Victory
    mIsDefeat = false;
    mVictoryMode = BSL_VICTORY_NONE;
    mVictoryTimer = 0;
}
void BlueSphereLevel::SetLevelState(int pSlot, int pResult)
{
    if(pSlot < 0 || pSlot >= mLevelsTotal) return;
    mLevelResults[pSlot] = pResult;
}
void BlueSphereLevel::UpdateLevelSelect()
{
    //--[Setup]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[Up and Down]
    //--Changes the cursor.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Already on the quit button:
        if(mLevelSelectCursor == LEVELS_PER_PAGE)
        {
            int tMaxEntriesOnThisPage = mLevelsTotal % LEVELS_PER_PAGE;
            mLevelSelectCursor = tMaxEntriesOnThisPage - 1;
        }
        //--Normal case:
        else
        {
            //--Decrement.
            mLevelSelectCursor --;

            //--Loop check.
            if(mLevelSelectCursor < 0) mLevelSelectCursor = LEVELS_PER_PAGE;
        }
    }
    else if(!rControlManager->IsFirstPress("Up") && rControlManager->IsFirstPress("Down"))
    {
        //--If already on the quit button:
        if(mLevelSelectCursor == LEVELS_PER_PAGE)
        {
            mLevelSelectCursor = 0;
        }
        //--Otherwise, try to increment by 1.
        else
        {
            //--Increment.
            mLevelSelectCursor ++;

            //--Check max entries on this page.
            int tMaxEntriesOnThisPage = mLevelsTotal % LEVELS_PER_PAGE;
            if(mLevelSelectCursor > tMaxEntriesOnThisPage - 1) mLevelSelectCursor = LEVELS_PER_PAGE;
        }
    }
    //--[Left and Right]
    //--Switches pages.
    else if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Decrement.
        mLevelSelectPage --;

        //--Clamp.
        if(mLevelSelectPage < 0) mLevelSelectPage = 0;
    }
    else if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Increment.
        mLevelSelectPage ++;

        //--Clamp.
        int tMaxPages = (mLevelsTotal / LEVELS_PER_PAGE);
        if(mLevelSelectPage >= tMaxPages) mLevelSelectPage = tMaxPages;
    }

    //--[Activate]
    //--Picks this level.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Quit button.
        if(mLevelSelectCursor == LEVELS_PER_PAGE)
        {
            //--If we have a stored level, send it to the MapManager.
            if(mSuspendedLevel)
            {
                //--Put in a temporary pointer so it doesn't get deallocated during the destructor.
                RootLevel *rSuspendedLevel = mSuspendedLevel;
                mSuspendedLevel = NULL;
                MapManager::Fetch()->ReceiveLevel(rSuspendedLevel);

                //--Object is not unstable. Return immediately.
                return;
            }
            //--Otherwise, this was a title call. Go back to the title.
            else
            {
                MapManager::Fetch()->mBackToTitle = true;
            }
        }
        //--Playing a level.
        else
        {
            //--Compute the level.
            mLastLevelSelected = (mLevelSelectPage * LEVELS_PER_PAGE) + mLevelSelectCursor;

            //--Build level buffer.
            const char *rBlueSpherePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sBlueSpherePath");
            char *tPathBuffer = InitializeString("%s/Level%04i.slf", rBlueSpherePath, mLastLevelSelected);

            //--Leave level select mode, parse the requested level.
            mIsLevelSelectMode = false;
            ReadFromFile(tPathBuffer);

            //--Start music.
            AudioManager::Fetch()->StopAllMusic();
            AudioManager::Fetch()->PlayMusic("BlueSphere");
        }
    }
}
void BlueSphereLevel::RenderLevelSelect()
{
    //--Render the level select menu.
    if(!Images.mIsReady) return;

    //--Constants and Setup
    float cLftInd = 35.0f;
    float cTopInd = 135.0f;
    float cScale = 3.0f;
    float cTextHei = Images.Data.rSystemFont->GetTextHeight() * cScale;
    float cTextSpc = cTextHei + 15.0f;
    int tStartPage = mLevelSelectPage * LEVELS_PER_PAGE;

    //--Header.
    Images.Data.rSystemFont->DrawTextArgs(cLftInd, 35.0f, 0, cScale, "Page %i/%i:", mLevelSelectPage+1, (mLevelsTotal / LEVELS_PER_PAGE) + 1);

    //--Iterate.
    for(int i = 0; i < LEVELS_PER_PAGE; i ++)
    {
        //--Range check.
        if(tStartPage + i >= mLevelsTotal) break;

        //--Print the name of the level:
        Images.Data.rSystemFont->DrawTextArgs(cLftInd + 100.0f, cTopInd + (i * cTextSpc), 0, cScale, "Level %i:", tStartPage + i);

        //--Status of the level: Incomplete.
        if(mLevelResults[tStartPage + i] == BSL_RESULT_INCOMPLETE)
        {
            Images.Data.rSystemFont->DrawText(cLftInd + 300.0f, cTopInd + (i * cTextSpc), 0, cScale, "Incomplete");
        }
        //--Completed.
        else if(mLevelResults[tStartPage + i] == BSL_RESULT_COMPLETE)
        {
            Images.Data.rSystemFont->DrawText(cLftInd + 300.0f, cTopInd + (i * cTextSpc), 0, cScale, "Complete");
        }
        //--Perfect!
        else if(mLevelResults[tStartPage + i] == BSL_RESULT_COMPLETE)
        {
            Images.Data.rSystemFont->DrawText(cLftInd + 300.0f, cTopInd + (i * cTextSpc), 0, cScale, "Perfect!");
        }

        //--Cursor.
        if(mLevelSelectCursor == i)
        {
            //--Positions.
            float cXPos = cLftInd;
            float cYPos = cTopInd + (i * cTextSpc) - 48.0f;

            glTranslatef(cXPos, cYPos, 0.0f);
            glScalef(3.0f, 3.0f, 1.0f);
            Images.Data.rPlayerForward->Draw(0.0f, 0.0f);
            glScalef(1.0f / 3.0f, 1.0f / 3.0f, 1.0f);
            glTranslatef(cXPos * -1.0f, cYPos * -1.0f, 0.0f);
        }
    }

    //--Exit.
    Images.Data.rSystemFont->DrawTextArgs(cLftInd + 100.0f, cTopInd + (LEVELS_PER_PAGE * cTextSpc), 0, cScale, "Quit");
    if(mLevelSelectCursor == LEVELS_PER_PAGE)
    {
        //--Positions.
        float cXPos = cLftInd;
        float cYPos = cTopInd + (LEVELS_PER_PAGE * cTextSpc) - 48.0f;

        glTranslatef(cXPos, cYPos, 0.0f);
        glScalef(3.0f, 3.0f, 1.0f);
        Images.Data.rPlayerForward->Draw(0.0f, 0.0f);
        glScalef(1.0f / 3.0f, 1.0f / 3.0f, 1.0f);
        glTranslatef(cXPos * -1.0f, cYPos * -1.0f, 0.0f);
    }
}
