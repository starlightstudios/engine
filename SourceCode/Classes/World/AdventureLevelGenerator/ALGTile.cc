//--Base
#include "ALGTile.h"

//--Classes
#include "AdventureLevelGenerator.h"
#include "TiledLevel.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
ALGTile::ALGTile()
{
    ///--[ALGTile]
    //--System
    mPulseCheck = 0;
    mPulseDistance = 0;
    mWasTunnelTile = false;

    //--Collision
    mCollisionType = CLIP_ALL;

    //--Special Texture Information
    mSpecialFloorFlag = SPECIAL_FLOOR_NONE;

    //--Texture UVs
    mUVFlags = 0;
    mTextureWid = 16.0f;
    mTextureHei = 16.0f;
    memset(mUVSlots, 0, sizeof(TwoDimensionReal) * ALGUV_TOTAL);

    //--Entity Stuff
    mIsEntrance = false;
    mIsExit = false;
    mIsTreasureCandidate = false;
    mIsTreasureFinalist = false;
    for(int i = 0; i < PATHS_MAX; i ++)
    {
        mIsEnemySpawn[i] = false;
        mPathNodeName[i][0] = '\0';
    }
}
ALGTile::~ALGTile()
{
}

///===================================== Property Queries =========================================
bool ALGTile::IsFloorTile()
{
    if(mCollisionType == CLIP_NONE) return true;
    return false;
}
bool ALGTile::IsPitTile()
{
    if(mCollisionType == CLIP_NONE) return false;
    if(mSpecialFloorFlag == SPECIAL_FLOOR_PIT) return true;
    return false;
}
bool ALGTile::IsWallTile()
{
    if(mCollisionType != CLIP_ALL) return false;
    if(mSpecialFloorFlag == SPECIAL_FLOOR_PIT) return false;
    return true;
}
bool ALGTile::IsEnemySpawn()
{
    for(int i = 0; i < PATHS_MAX; i ++)
    {
        if(mIsEnemySpawn[i]) return true;
    }
    return false;
}
bool ALGTile::IsPathNode()
{
    for(int i = 0; i < PATHS_MAX; i ++)
    {
        if(mPathNodeName[i][0] != '\0') return true;
    }
    return false;
}

///======================================== Manipulators ==========================================
void ALGTile::ClearUVs()
{
    mUVFlags = 0x0000;
}
void ALGTile::UnclipForTunnel()
{
    if(mCollisionType == CLIP_ALL) mWasTunnelTile = true;
    mCollisionType = CLIP_NONE;
}
void ALGTile::SetUV(int pSlot, float pLft, float pTop, float pRgt, float pBot)
{
    //--Error check.
    if(pSlot < 0 || pSlot >= ALGUV_TOTAL) return;

    //--Flip the rendering flag.
    uint16_t tFlag = 0x0001;
    for(int i = 0; i < pSlot; i ++) tFlag = tFlag * 2;
    mUVFlags |= tFlag;

    //--Store the UV coordinates.
    mUVSlots[pSlot].Set(pLft, pTop, pRgt, pBot);
    mUVSlots[pSlot].mTop = 1.0f - mUVSlots[pSlot].mTop;
    mUVSlots[pSlot].mBot = 1.0f - mUVSlots[pSlot].mBot;
}
void ALGTile::SetUV(int pSlot, TwoDimensionReal pDim)
{
    SetUV(pSlot, pDim.mLft, pDim.mTop, pDim.mRgt, pDim.mBot);
}

///======================================== Core Methods ==========================================
void ALGTile::CompileDataTo(int pX, int pY, int pXSize, int pYSize, int16_t **pTileArray, uint8_t **pCollisionArray)
{
    //--Constants
    float cSizePerTile = 16.0f;
    float cBitmapWid = 192.0f;
    float cBitmapHei = 192.0f;
    float cStride = cBitmapWid / cSizePerTile;

    //--Determine our position in the array.
    int tArrayPos = ((pX * pYSize) + pY);
    int tArrayPosUp = ((pX * pYSize) + pY-1);

    //--Scan across the array of render UVs and input anything we have. Zero off unused slots.
    uint16_t tFlag = 0x0001;
    for(int i = 0; i < ALGUV_TREASURE; i ++)
    {
        //--Flagged for rendering:
        if(mUVFlags & tFlag)
        {
            //--Determine tile index.
            int tXPos = (mUVSlots[i].mLft * cBitmapWid) / cSizePerTile;
            int tYPos = ((mUVSlots[i].mBot + 0.000001f) * cBitmapHei) / cSizePerTile;
            float tIndex = (tYPos * cStride) + tXPos;

            //--This tile.
            if(i < ALGUV_WALLHIA || i >= ALGUV_TREASURE)
            {
                pTileArray[i][tArrayPos] = tIndex;
            }
            //--Tile one above.
            else if(tArrayPosUp >= 0)
            {
                pTileArray[i][tArrayPosUp] = tIndex;
            }
        }
        //--Not flagged. Zero it off.
        else
        {
            //--This tile.
            if(i < ALGUV_WALLHIA || i >= ALGUV_TREASURE)
            {
                pTileArray[i][tArrayPos] = -1;
            }
            //--Tile one above.
            else if(tArrayPosUp >= 0)
            {
                pTileArray[i][tArrayPosUp] = -1;
            }
        }

        //--Set the flag for next time.
        tFlag = tFlag * 2;
    }

    //--Collision:
    if(IsFloorTile() && !IsPitTile())
    {
        pCollisionArray[pX][pY] = CLIP_NONE;
    }
    else
    {
        pCollisionArray[pX][pY] = CLIP_ALL;
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void ALGTile::RenderDebug(float pX, float pY)
{
    //--Assuming the GL State has been correctly set up, renders simple squares or triangles to
    //  represent the collision of this tile. Does nothing if the collision type is CLIP_NONE.
    if(mCollisionType == CLIP_ALL) return;

    //--Zero pulse check. Solid blue.
    if(mPulseDistance == 0)
    {
        glColor3f(1.0f, 1.0f, 1.0f);
    }
    //--Otherwise, the green then red values tick up.
    else
    {
        float tRVal, tGVal, tBVal;

        if(mPulseDistance < 20)
        {
            tRVal = 0.0f;
            tGVal = 0.0f;
            tBVal = 0.5f + (mPulseDistance / 20.0f * 0.5f);
        }
        else if(mPulseDistance < 40)
        {
            tRVal = 0.0f;
            tGVal = 0.5f + ((mPulseDistance - 20) / 20.0f * 0.5f);
            tBVal = 0.0f;
        }
        else if(mPulseDistance < 60)
        {
            tRVal = 0.5f + ((mPulseDistance - 40) / 20.0f * 0.5f);
            tGVal = 0.0f;
            tBVal = 0.0f;
        }
        else if(mPulseDistance < 80)
        {
            tRVal = 1.0f;
            tGVal = 0.0f;
            tBVal = 0.5f + ((mPulseDistance - 60) / 20.0f * 0.5f);
        }
        else if(mPulseDistance < 100)
        {
            tRVal = 1.0f;
            tGVal = 1.0f;
            tBVal = 0.5f + ((mPulseDistance - 80) / 20.0f * 0.5f);
        }
        else
        {
            tRVal = 1.0f;
            tGVal = 0.5f + ((mPulseDistance - 100) / 20.0f * 0.5f);
            tBVal = 0.0f;
        }

        //--Set.
        glColor3f(tRVal, tGVal, tBVal);
    }

    //--Render. We're in triangle modes.
    glVertex2f(pX +  0.0f, pY +  0.0f);
    glVertex2f(pX + 16.0f, pY +  0.0f);
    glVertex2f(pX +  0.0f, pY + 16.0f);

    //--Second triangle.
    glVertex2f(pX + 16.0f, pY +  0.0f);
    glVertex2f(pX + 16.0f, pY + 16.0f);
    glVertex2f(pX +  0.0f, pY + 16.0f);
}
void ALGTile::RenderTextured(float pX, float pY)
{
    //--Renders whatever textures this tile has. Rendering mode is assumed to be quads.

    //--Setup the depths.
    bool xHasSetupDepths = false;
    float cxDepths[ALGUV_TOTAL];
    if(!xHasSetupDepths)
    {
        //--Set.
        cxDepths[ALGUV_FLOORA] = ALGDEPTH_FLOOR;
        cxDepths[ALGUV_FLOORB] = ALGDEPTH_FLOOR + 0.000001f;
        cxDepths[ALGUV_FLOORC] = ALGDEPTH_FLOOR + 0.000002f;
        cxDepths[ALGUV_WALLA] = ALGDEPTH_WALL;
        cxDepths[ALGUV_WALLB] = ALGDEPTH_WALL + 0.000001f;
        cxDepths[ALGUV_WALLHIA] = ALGDEPTH_WALLHI;
        cxDepths[ALGUV_WALLHIB] = ALGDEPTH_WALLHI + 0.000001f;
        cxDepths[ALGUV_BLACKOUT] = ALGDEPTH_BLACKOUT;
        cxDepths[ALGUV_TREASURE] = ALGDEPTH_FLOOR + 0.000003f;
        cxDepths[ALGUV_ENEMYA] = ALGDEPTH_BLACKOUT + 0.000002f;
        cxDepths[ALGUV_ENEMYB] = ALGDEPTH_FLOOR + 0.000003f;
        cxDepths[ALGUV_ENEMYC] = ALGDEPTH_FLOOR + 0.000004f;

        //--Flag.
        xHasSetupDepths = true;
    }

    //--Setup enemy colors.
    static bool xHasSetupColors = false;
    static StarlightColor xColors[PATHS_MAX];
    if(!xHasSetupColors)
    {
        //--Set.
        xColors[ 0].SetRGBAI(255,   0,   0, 255);
        xColors[ 1].SetRGBAI(255, 106,   0, 255);
        xColors[ 2].SetRGBAI(255, 216,   0, 255);
        xColors[ 3].SetRGBAI(182, 255,   0, 255);
        xColors[ 4].SetRGBAI( 76, 255,   0, 255);
        xColors[ 5].SetRGBAI(  0, 255,  33, 255);
        xColors[ 6].SetRGBAI(255, 255, 128, 255);
        xColors[ 7].SetRGBAI(  0, 255, 144, 255);
        xColors[ 8].SetRGBAI(  0, 255, 255, 255);
        xColors[ 9].SetRGBAI(  0, 144, 255, 255);
        xColors[10].SetRGBAI(  0,  38, 255, 255);
        xColors[11].SetRGBAI( 72,   0, 255, 255);
        xColors[12].SetRGBAI(178,   0, 255, 255);
        xColors[13].SetRGBAI(127,   0, 110, 255);
        xColors[14].SetRGBAI(255,   0, 220, 255);
        xColors[15].SetRGBAI(255,   0, 110, 255);
        xColors[16].SetRGBAI(255, 127, 127, 255);
        xColors[17].SetRGBAI(255, 178, 127, 255);
        xColors[18].SetRGBAI(255, 233, 127, 255);
        xColors[19].SetRGBAI(218, 255, 127, 255);
        xColors[20].SetRGBAI(165, 255, 127, 255);
        xColors[21].SetRGBAI(127, 255, 255, 255);
        xColors[22].SetRGBAI(127, 201, 255, 255);
        xColors[23].SetRGBAI(127, 146, 255, 255);
        xColors[24].SetRGBAI(161, 127, 255, 255);
        xColors[25].SetRGBAI(214, 127, 255, 255);

        //--Flag.
        xHasSetupColors = true;
    }

    //--Setup
    float cYOffset = 0.0f;
    uint16_t tFlag = 0x0001;

    for(int i = 0; i < ALGUV_STANDARD; i ++)
    {
        //--When starting wall-hi, move the offset.
        if(i == ALGUV_WALLHIA)  cYOffset = -16.0f;
        if(i == ALGUV_TREASURE) cYOffset =   0.0f;

        //--Render if flagged.
        if(mUVFlags & tFlag)
        {
            glTexCoord2f(mUVSlots[i].mLft, mUVSlots[i].mTop); glVertex3f(pX +  0.0f, pY +  0.0f + cYOffset, cxDepths[i]);
            glTexCoord2f(mUVSlots[i].mRgt, mUVSlots[i].mTop); glVertex3f(pX + 16.0f, pY +  0.0f + cYOffset, cxDepths[i]);
            glTexCoord2f(mUVSlots[i].mRgt, mUVSlots[i].mBot); glVertex3f(pX + 16.0f, pY + 16.0f + cYOffset, cxDepths[i]);
            glTexCoord2f(mUVSlots[i].mLft, mUVSlots[i].mBot); glVertex3f(pX +  0.0f, pY + 16.0f + cYOffset, cxDepths[i]);
        }

        //--Set the flag for next time.
        tFlag = tFlag * 2;
    }

    //--Non-standard rendering. Changes the color palette of this tile to represent which enemy matches which path.
    if(mUVFlags & ALGTILE_HAS_ENEMYA || mUVFlags & ALGTILE_HAS_ENEMYB)
    {
        //--Enemy check. Takes priority.
        for(int i = 0; i < PATHS_MAX; i ++)
        {
            //--Enemy on this tile:
            if(!mIsEnemySpawn[i]) continue;
            if(AdventureLevelGenerator::xPathVis != -1 && i != AdventureLevelGenerator::xPathVis) continue;

            //--Render the enemy.
            xColors[i].SetAsMixer();
            glTexCoord2f(mUVSlots[ALGUV_ENEMYA].mLft, mUVSlots[ALGUV_ENEMYA].mTop); glVertex3f(pX +  0.0f, pY +  0.0f - 16.0f + i, cxDepths[ALGUV_ENEMYA]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYA].mRgt, mUVSlots[ALGUV_ENEMYA].mTop); glVertex3f(pX + 16.0f, pY +  0.0f - 16.0f + i, cxDepths[ALGUV_ENEMYA]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYA].mRgt, mUVSlots[ALGUV_ENEMYA].mBot); glVertex3f(pX + 16.0f, pY + 16.0f - 16.0f + i, cxDepths[ALGUV_ENEMYA]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYA].mLft, mUVSlots[ALGUV_ENEMYA].mBot); glVertex3f(pX +  0.0f, pY + 16.0f - 16.0f + i, cxDepths[ALGUV_ENEMYA]);

            glTexCoord2f(mUVSlots[ALGUV_ENEMYB].mLft, mUVSlots[ALGUV_ENEMYB].mTop); glVertex3f(pX +  0.0f, pY +  0.0f +  0.0f + i, cxDepths[ALGUV_ENEMYB]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYB].mRgt, mUVSlots[ALGUV_ENEMYB].mTop); glVertex3f(pX + 16.0f, pY +  0.0f +  0.0f + i, cxDepths[ALGUV_ENEMYB]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYB].mRgt, mUVSlots[ALGUV_ENEMYB].mBot); glVertex3f(pX + 16.0f, pY + 16.0f +  0.0f + i, cxDepths[ALGUV_ENEMYB]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYB].mLft, mUVSlots[ALGUV_ENEMYB].mBot); glVertex3f(pX +  0.0f, pY + 16.0f +  0.0f + i, cxDepths[ALGUV_ENEMYB]);
            StarlightColor::ClearMixer();
        }
    }
    if(mUVFlags & ALGTILE_HAS_ENEMYC)
    {
        //--Pathing check.
        for(int i = 0; i < PATHS_MAX; i ++)
        {
            //--Path on this tile:
            if(mPathNodeName[i][0] == '\0') continue;
            if(AdventureLevelGenerator::xPathVis != -1 && i != AdventureLevelGenerator::xPathVis) continue;

            //--Render the path indicator.
            xColors[i].SetAsMixer();
            glTexCoord2f(mUVSlots[ALGUV_ENEMYC].mLft, mUVSlots[ALGUV_ENEMYC].mTop); glVertex3f(pX +  0.0f, pY +  0.0f + i, cxDepths[ALGUV_ENEMYC]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYC].mRgt, mUVSlots[ALGUV_ENEMYC].mTop); glVertex3f(pX + 16.0f, pY +  0.0f + i, cxDepths[ALGUV_ENEMYC]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYC].mRgt, mUVSlots[ALGUV_ENEMYC].mBot); glVertex3f(pX + 16.0f, pY + 16.0f + i, cxDepths[ALGUV_ENEMYC]);
            glTexCoord2f(mUVSlots[ALGUV_ENEMYC].mLft, mUVSlots[ALGUV_ENEMYC].mBot); glVertex3f(pX +  0.0f, pY + 16.0f + i, cxDepths[ALGUV_ENEMYC]);
            StarlightColor::ClearMixer();
        }
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
