///================================== GUIExec_MenuPushObject ======================================
//--Locates the Object given and pushes it on top of the GUI's menu stack.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "GUI.h"
#include "GUIBaseObject.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class GUIExec_MenuPushObject : public RootExecutor
{
    private:
    char *mObjectName;

    protected:

    public:
    //--System
    GUIExec_MenuPushObject()
    {
        mObjectName = NULL;
    }
    virtual ~GUIExec_MenuPushObject()
    {
        free(mObjectName);
        mObjectName = NULL;
    }

    //--Manipulators
    void SetName(const char *pName)
    {
        ResetString(mObjectName, pName);
    }

    //--Executor
    virtual void Execute()
    {
        GUIBaseObject *rObject = GraphicalUserInterface::Fetch()->FindGUIObject(mObjectName);
        if(rObject) GraphicalUserInterface::Fetch()->PushMenuStack(rObject);
    }
};
