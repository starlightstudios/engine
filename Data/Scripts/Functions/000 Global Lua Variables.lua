-- |[ ================================== Global Lua Variables ================================== ]|
--Exactly what the name suggests, variables used by low-level lua functions. 

-- |[Game Selector Variables]|
--Variables used to track which games the engine has detected.
gsAutoExec = nil
gbClearMenu = true

--Sorting Variables
gciAdventure_Set_Start    = 100
gciClassic_Set_Start      = 110
gciStringTyrant_Set_Start = 120
gciChicmonster_Set_Start  = 130
gciTools_Set_Start        = 190
gciOtherGames_Set_Start   = 200
gciOtherGames_Counter     = gciOtherGames_Set_Start --Use for unlisted games/mods

-- |[Loading Variables]|
--Indicates whether or not a loading sequence is currently executing. Each loading sequence has a 
-- specific name and number associated with it, used to automatically generate progress bars.
gsLoadSequence = "None"

-- |[Classic Globals]|
--Difficulty. The startup scripts for each scenario use this to extrapolate.
gsDifficulty = "Normal"

-- |[Object Types]|
--These are internal C++ constants. Do not edit them.
xciType_InventoryItem = 21
xciType_WorldContainer = 22
xciType_Actor = 1002

-- |[ ==================================== Diagnostic Flags ==================================== ]|
-- |[Cutscenes]|
--Note: Use the function fnSetCutsceneDiagnostics() to set this, which will set it in both Lua and C++.
gci_Diagnostic_Cutscene_None = 0
gci_Diagnostic_Cutscene_AtLeastFunctions = 1 --Prints the name of the function. Implicitly true if any other flag is.
gci_Diagnostic_Cutscene_Arguments = 2
gci_Diagnostic_Cutscene_Paths = 4
gci_Diagnostic_Cutscene_Visual = 8
gci_Diagnostic_Cutscene_All = 255

--Flag.
gi_Diagnostic_Cutscene_Flag = gci_Diagnostic_Cutscene_None

-- |[ =================================== Colors and Display =================================== ]|
-- |[Graphics Enumerations]|
giCurrentDisplayMode = DM_GetDisplayModeInfo("Current Mode")
gbAntiAliasCharacters = true

-- |[Rendering Enumerations]|
--These are derived from the GL State.
giLinear         = DM_GetEnumeration("GL_LINEAR")
giNearest        = DM_GetEnumeration("GL_NEAREST")
giLinearLinear   = DM_GetEnumeration("GL_LINEAR_MIPMAP_LINEAR")
giLinearNearest  = DM_GetEnumeration("GL_LINEAR_MIPMAP_NEAREST")
giNearestLinear  = DM_GetEnumeration("GL_NEAREST_MIPMAP_LINEAR")
giNearestNearest = DM_GetEnumeration("GL_NEAREST_MIPMAP_NEAREST")

--Set default image flags.
ALB_SetTextureProperty("Default MinFilter", giLinearLinear)
ALB_SetTextureProperty("Default MagFilter", giLinearLinear)
ALB_SetTextureProperty("Restore Defaults")

-- |[Color Constants]|
gfColorWhite = {}
gfColorWhite.fR = 0.70
gfColorWhite.fG = 0.00
gfColorWhite.fB = 0.00
gfColorWhite.fA = 1.00

gfColorRed = {}
gfColorRed.fR = 0.70
gfColorRed.fG = 0.00
gfColorRed.fB = 0.00
gfColorRed.fA = 1.00

gfColorGreen = {}
gfColorGreen.fR = 0.00
gfColorGreen.fG = 0.00
gfColorGreen.fB = 0.40
gfColorGreen.fA = 1.00

gfColorViolet = {}
gfColorViolet.fR = 0.50
gfColorViolet.fG = 0.30
gfColorViolet.fB = 0.90
gfColorViolet.fA = 1.00

-- |[ ====================================== Autorun Cases ===================================== ]|
--Used by developers to make running the game faster. Autoruns can be passed into the engine's command line
-- as: -autorun "GAMENAME", which will cause that game's script to execute after startup.
gzaAutorunHandlers = {}
