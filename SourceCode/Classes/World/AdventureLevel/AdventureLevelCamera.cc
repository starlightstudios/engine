//--Base
#include "AdventureLevel.h"

//--Classes
#include "TilemapActor.h"
#include "TileLayer.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DisplayManager.h"
#include "EntityManager.h"

///--[Property Queries]
TwoDimensionReal AdventureLevel::GetCameraDimensions()
{
    return mCameraDimensions;
}

///--[Manipulators]
void AdventureLevel::SetCameraScale(float pScale)
{
    mCurrentScale = pScale;
    if(mCurrentScale < 0.01f) mCurrentScale = 0.01f;
}
void AdventureLevel::SetCameraLocking(bool pFlag)
{
    //--Flag.
    mIsCameraLocked = pFlag;

    //--If the camera is unlocked, it can't follow anyone but the player so reset this flag.
    if(!mIsCameraLocked) mCameraFollowID = 0;
}
void AdventureLevel::SetCameraFollowTarget(uint32_t pID)
{
    mCameraFollowID = pID;
}
void AdventureLevel::SetCameraPosition(float pLft, float pTop)
{
    //--Note: Can and will bypass clamps!
    mCameraDimensions.SetWH(pLft, pTop, VIRTUAL_CANVAS_X / mCurrentScale, VIRTUAL_CANVAS_Y / mCurrentScale);
}
void AdventureLevel::SetCameraPosition(TwoDimensionReal pPosition)
{
    //--Note: Can and will bypass clamps!
    memcpy(&mCameraDimensions, &pPosition, sizeof(TwoDimensionReal));
}

//--[Core Methods]
TwoDimensionReal AdventureLevel::GetIdealCameraPosition(float pTargetX, float pTargetY)
{
    //--Computes and returns the ideal position of the camera around the given point, with the point being
    //  in the center of view (ideally) before edge checks occur.
    //--This is both used by the AdventureLevel itself, and by external events that need to move the camera
    //  a small amount at a time.
    if(mCurrentScale == 0.0f) mCurrentScale = 1.0f;

    //--Compute the ideal top/left.
    float tIdealX = pTargetX - (VIRTUAL_CANVAS_X * 0.5f / mCurrentScale);
    float tIdealY = pTargetY - (VIRTUAL_CANVAS_Y * 0.5f / mCurrentScale);

    //--Clamps.
    float tRgtEdge = (mXSize * TileLayer::cxSizePerTile) - (VIRTUAL_CANVAS_X / mCurrentScale);
    float tBotEdge = (mYSize * TileLayer::cxSizePerTile) - (VIRTUAL_CANVAS_Y / mCurrentScale);
    if(tIdealX >= tRgtEdge) tIdealX = tRgtEdge;
    if(tIdealY >= tBotEdge) tIdealY = tBotEdge;
    if(tIdealX < 0.0f) tIdealX = 0.0f;
    if(tIdealY < 0.0f) tIdealY = 0.0f;

    //--Horizontal Special: If the level size is less than one full screen, we can clamp off the left edge.
    if(mXSize * TileLayer::cxSizePerTile * mCurrentScale < VIRTUAL_CANVAS_X)
    {
        //--Centering.
        tIdealX = ((VIRTUAL_CANVAS_X) - (mXSize * TileLayer::cxSizePerTile * mCurrentScale)) / -2.0f;

        //--Adjust to scale.
        tIdealX = tIdealX / mCurrentScale;
    }

    //--Vertical Special: If the level size is less than one full screen, we can clamp off the left edge.
    if(mYSize * TileLayer::cxSizePerTile * mCurrentScale < VIRTUAL_CANVAS_Y)
    {
        //--Centering.
        tIdealY = ((VIRTUAL_CANVAS_Y) - (mYSize * TileLayer::cxSizePerTile * mCurrentScale)) / -2.0f;

        //--Adjust to scale.
        tIdealY = tIdealY / mCurrentScale;
    }

    //--Return this as a TwoDimensionReal.
    TwoDimensionReal nDimensions;
    nDimensions.SetWH(tIdealX, tIdealY, VIRTUAL_CANVAS_X / mCurrentScale, VIRTUAL_CANVAS_Y / mCurrentScale);
    return nDimensions;
}

//--[Update]
void AdventureLevel::UpdateCameraPosition()
{
    //--Resolves the Camera's position based on the flags used by the level. First, if the Camera
    //  is not locked, focus it on the player's character.
    if(mCurrentScale == 0.0f) mCurrentScale = 1.0f;

    //--Normal case: Focus on the player's position.
    if(!mIsCameraLocked)
    {
        //--Locate the player's character.
        TilemapActor *rActor = LocatePlayerActor();
        if(!rActor || !rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return;

        //--Position.
        float tPlayerX = rActor->GetWorldX();
        float tPlayerY = rActor->GetWorldY();

        //--Compute the ideal position.
        TwoDimensionReal tIdealCamera = GetIdealCameraPosition(tPlayerX, tPlayerY);

        //--If the level is not using camera mode, just directly copy it.
        if(!mIsPositiveCameraMode || mFirstLock < 10)
        {
            if(mFirstLock < 10) mFirstLock ++;
            memcpy(&mCameraDimensions, &tIdealCamera, sizeof(TwoDimensionReal));
        }
        //--Otherwise, we need to move iteratively and not hit any of the negative camera zones.
        else
        {
            //--Iterate.
            while(tIdealCamera.mLft != mCameraDimensions.mLft || tIdealCamera.mTop != mCameraDimensions.mTop)
            {
                //--Error check variables. If this variable results in a zero, the camera is stuck where it is.
                int tMovedAtLeastOnce = 2;

                //--Camera needs to move horizontally.
                if(tIdealCamera.mLft != mCameraDimensions.mLft)
                {
                    //--Attempt to move left.
                    float tMoveAmount = tIdealCamera.mLft - mCameraDimensions.mLft;
                    if(tMoveAmount < -1.0f) tMoveAmount = -1.0f;
                    if(tMoveAmount >  1.0f) tMoveAmount =  1.0f;
                    mCameraDimensions.OffsetX(tMoveAmount);

                    //--Check if we collided with any of the camera hit zones.
                    TwoDimensionReal *rCheckZone = (TwoDimensionReal *)mCameraZoneList->PushIterator();
                    while(rCheckZone)
                    {
                        //--Upon collision, reset.
                        if(IsCollision2DReal(*rCheckZone, mCameraDimensions))
                        {
                            tMovedAtLeastOnce --;
                            mCameraDimensions.OffsetX(tMoveAmount * -1.0f);
                            mCameraZoneList->PopIterator();
                            break;
                        }

                        //--Next.
                        rCheckZone = (TwoDimensionReal *)mCameraZoneList->AutoIterate();
                    }
                }
                //--Camera needs no horizontal movement.
                else
                {
                    tMovedAtLeastOnce --;
                }

                //--Camera needs to move vertically.
                if(tIdealCamera.mTop != mCameraDimensions.mTop)
                {
                    //--Attempt to move left.
                    float tMoveAmount = tIdealCamera.mTop - mCameraDimensions.mTop;
                    if(tMoveAmount < -1.0f) tMoveAmount = -1.0f;
                    if(tMoveAmount >  1.0f) tMoveAmount =  1.0f;
                    mCameraDimensions.OffsetY(tMoveAmount);

                    //--Check if we collided with any of the camera hit zones.
                    TwoDimensionReal *rCheckZone = (TwoDimensionReal *)mCameraZoneList->PushIterator();
                    while(rCheckZone)
                    {
                        //--Upon collision, reset.
                        if(IsCollision2DReal(*rCheckZone, mCameraDimensions))
                        {
                            tMovedAtLeastOnce --;
                            mCameraDimensions.OffsetY(tMoveAmount * -1.0f);
                            mCameraZoneList->PopIterator();
                            break;
                        }

                        //--Next.
                        rCheckZone = (TwoDimensionReal *)mCameraZoneList->AutoIterate();
                    }
                }
                //--Camera needs no vertical movement.
                else
                {
                    tMovedAtLeastOnce --;
                }

                //--If both directions got stuck, stop.
                if(tMovedAtLeastOnce == 0)
                {
                    break;
                }
            }
        }
    }
    //--If the Camera happens to be locked, it can still be set to follow something other than the player. If the
    //  follow ID is 0, then the Camera remains where it is.
    else if(mCameraFollowID)
    {
        //--Locate the target.
        TilemapActor *rActor = (TilemapActor *)EntityManager::Fetch()->GetEntityI(mCameraFollowID);
        if(!rActor || !rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return;

        //--Position.
        float tFocusX = rActor->GetWorldX();
        float tFocusY = rActor->GetWorldY();

        //--Compute.
        mCameraDimensions = GetIdealCameraPosition(tFocusX, tFocusY);
    }
}
