//--Base
#include "CarnationMinimap.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void CarnationMinimap::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatAbility_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    //lua_register(pLuaState, "AdvCombatAbility_GetProperty", &Hook_AdvCombatAbility_GetProperty);

    /* AdvCombatAbility_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */

    lua_register(pLuaState, "CarnMinimap_SetProperty", &Hook_CarnMinimap_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
/*
int Hook_CarnMinimap_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnMinimap_SetProperty("Dummy") (1 Integer)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAbility_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATABILITY)) return LuaTypeError("AdvCombatAbility_GetProperty");
    AdvCombatAbility *rAbility = (AdvCombatAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}*/
int Hook_CarnMinimap_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //CarnMinimap_SetProperty("Player Position", iX, iY)
    //CarnMinimap_SetProperty("Map Image", sDLPath)
    //CarnMinimap_SetProperty("Discover Size", iX, iY)
    //CarnMinimap_SetProperty("Discover Point", iX, iY)
    //CarnMinimap_SetProperty("Discover Area", iLft, iTop, iRgt, iBot)
    //CarnMinimap_SetProperty("Register Layer", iX, iY, sDLPath)
    //CarnMinimap_SetProperty("Add To Last Layer", sDLPath)
    //CarnMinimap_SetProperty("Clear Layers")

    //--[Enemies]
    //CarnMinimap_SetProperty("Clear Indicators")
    //CarnMinimap_SetProperty("Register Indicator", sName, iX, iY, iCode)
    //CarnMinimap_SetProperty("Remove Indicator", sName)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnMinimap_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check. Must be a valid level which has a valid minimap.
    CarnationMinimap *rMinimap = CarnationMinimap::Fetch();
    if(!rMinimap) return LuaTypeError("CarnMinimap_SetProperty", L);

    ///--[System]
    //--Sets player position, in tiles.
    if(!strcasecmp(rSwitchType, "Player Position") && tArgs == 3)
    {
        rMinimap->SetPlayerPosition(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the map image.
    else if(!strcasecmp(rSwitchType, "Map Image") && tArgs == 2)
    {
        rMinimap->SetMapImage(lua_tostring(L, 2));
    }
    //--Manually specifies size of discovery grid. Note that this is handled by "Map Image" above, but random dungeons
    //  do not have base map images.
    else if(!strcasecmp(rSwitchType, "Discover Size") && tArgs == 3)
    {
        rMinimap->SetDiscoverySize(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Clears a single point on the discovery grid.
    else if(!strcasecmp(rSwitchType, "Discover Point") && tArgs == 3)
    {
        rMinimap->DiscoverPoint(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Clears a rectangular area.
    else if(!strcasecmp(rSwitchType, "Discover Area") && tArgs == 5)
    {
        rMinimap->DiscoverArea(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--Registers a minimap layer.
    else if(!strcasecmp(rSwitchType, "Register Layer") && tArgs >= 4)
    {
        rMinimap->CreateMapLayer(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tostring(L, 4));
    }
    //--Adds an image on top of the last minimap layer.
    else if(!strcasecmp(rSwitchType, "Add To Last Layer") && tArgs >= 2)
    {
        rMinimap->AddImageToLastLayer(lua_tostring(L, 2));
    }
    //--Clears map layers, does not clear the background.
    else if(!strcasecmp(rSwitchType, "Clear Layers") && tArgs >= 1)
    {
        rMinimap->ClearMapLayers();
    }
    ///--[Enemies]
    //--Clears all indicators.
    else if(!strcasecmp(rSwitchType, "Clear Indicators") && tArgs >= 1)
    {
        rMinimap->ClearIndicators();
    }
    //--Adds a new enemy indicator.
    else if(!strcasecmp(rSwitchType, "Register Indicator") && tArgs >= 5)
    {
        rMinimap->RegisterIndicator(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--Removes an enemy indicator.
    else if(!strcasecmp(rSwitchType, "Remove Indicator") && tArgs >= 2)
    {
        rMinimap->RemoveIndicator(lua_tostring(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
