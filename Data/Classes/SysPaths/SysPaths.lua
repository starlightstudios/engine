-- |[ ================================== SysPaths Class File =================================== ]|
--Class file for SysPaths. Call this once at initialization.

--SysPaths is the suite of functionality for determining which games are playable and are supported
-- by the current version of the engine. This is where the game determines which games (like WHI or RoP)
-- are playable, where their directories are (multiple are possible!) and how to populate the
-- main menu.
--This is a static class.

-- |[ ========== Game Path Listing =========== ]|
--The active path can also be found by using VM_GetVar(). A list of the paths follows. This list is
-- mirrored in DataLibrary.cc.
--"Root/Paths/System/Startup/sClassicModePath"
--"Root/Paths/System/Startup/sClassicMode3DPath"
--"Root/Paths/System/Startup/sCorrupterModePath"
--"Root/Paths/System/Startup/sAdventurePath"
--"Root/Paths/System/Startup/sDollManorPath"
--"Root/Paths/System/Startup/sElectrospriteAdventurePath"
--"Root/Paths/System/Startup/sMOTFPath"
--"Root/Paths/System/Startup/sLevelGeneratorPath"
--"Root/Paths/System/Startup/sPairanormalPath"
--"Root/Paths/System/Startup/sWonderlandPath"
--"Root/Paths/System/Startup/sSlittedEyePath"
--"Root/Paths/System/Startup/sPeakFreaksPath"

-- |[ =========== Globals/Statics ============ ]|
-- |[ =========== Game Entry Class =========== ]|
--This class stores information about a game that can be selected on the game selection menu.
--GameEntry.sName                               --Unique name, such as "Adventure Mode"
--GameEntry.sVarPath                            --DataLibrary path, must exist in the C++ listing.
--GameEntry.sButtonText                         --Text that appears on the game selection listing.
--GameEntry.iPriority                           --Relative position of the button when it appears on the game selection listing.
--GameEntry.sFileLaunchPath                     --File in the folder that is run when the game is selected. Usually "YMenuLaunch.lua".
--GameEntry.sActivePath                         --Currently active directory, typically one of the below search paths.
--GameEntry.saSearchPaths                       --List of possible places to look for this game on a normal installation.
--GameEntry.iNegativeLen                        --Length of the launcher's file name. Often -12, as "ZLaunch.lua" is 12 letters.

-- |[ ============ Class Members ============= ]|
-- |[System]|
SysPaths = {}
SysPaths.__index = SysPaths

-- |[Diagnostic Flags]|
SysPaths.bShowFailedRegistrations = false       --If true, prints a warning to indicate the given game is not compatible with the current version of the engine. Ex: Peak Freaks requires a special compilation.

-- |[Game Info Paths]|
SysPaths.saGameInfoList = {}                    --Stores a list of all paths that contain a valid XGameInfo.lua file.

-- |[Game Entries]|
SysPaths.zaGameEntries = {}                     --Stores GameEntry structures. Prototype above.

-- |[ ============ Class Methods ============= ]|
--System
--Activation
function SysPaths:fnActivateAllGames() end

--Game Entries
function SysPaths:fnGameEntryExists(psName)                                                                    end
function SysPaths:fnGetGameEntry(psName)                                                                       end
function SysPaths:fnGetGameEntryValid(psName)                                                                  end
function SysPaths:fnAddGameEntry(psName, psVarPath, psButtonText, piPriority, psFileLaunchPath, piNegativeLen) end
function SysPaths:fnAddSearchPathToEntry(psName, psSearchPath)                                                 end
function SysPaths:fnGameEntryHasValidPath(psName)                                                              end

--Scanning
function SysPaths:fnScanDirectories()                                   end
function SysPaths:fnIsValidPath(psPath, psFolderName, psaExceptionList) end
function SysPaths:fnScanGameFolders()                                   end
function SysPaths:fnScanUpFolders()                                     end
function SysPaths:fnScanOSXFolders()                                    end
function SysPaths:fnSetAllActivePaths()                                 end

--Script Hunter
function SysPaths:fnBuildScriptHunterPaths() end

-- |[ ========== Class Constructor =========== ]|
--Static singleton, is not instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "SysPathsActivation.lua")
LM_ExecuteScript(fnResolvePath() .. "SysPathsGameEntries.lua")
LM_ExecuteScript(fnResolvePath() .. "SysPathsScanning.lua")
LM_ExecuteScript(fnResolvePath() .. "SysPathsScriptHunter.lua")
