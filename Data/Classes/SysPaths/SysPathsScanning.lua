-- |[ =================================== SysPaths Scanning ==================================== ]|
--Functions related to scanning paths with the filesystem to build a list of available games.

-- |[ ============================== SysPaths:fnScanDirectories() ============================== ]|
--Scans directories, usually called at startup. This calls sub-functions.
function SysPaths:fnScanDirectories()
    
    -- |[Diagnostics]|
    local bDiagnostics = fnAutoCheckDebugFlag("SysPaths: Scanning")
    Debug_PushPrint(bDiagnostics, "SysPaths:fnScanDirectories() - Begins.\n")
    
    -- |[Execution]|
    --Reset.
    self.saGameInfoList = {}
    
    --Call all the scanner routines. This builds the list saGameInfoList.
    Debug_Print("Scanning game folders.\n")
    self:fnScanGameFolders()
    
    Debug_Print("Scanning up-level folders.\n")
    self:fnScanUpFolders()
    
    Debug_Print("Scanning OSX folders.\n")
    self:fnScanOSXFolders()
    
    --Diagnostics.
    Debug_Print("All scans complete. Total entries found: " .. #self.saGameInfoList .. "\n")
    
    --Once all game info entries are stored, execute them.
    for i = 1, #self.saGameInfoList, 1 do
        Debug_Print("Executing: " .. self.saGameInfoList[i] .. "\n")
        LM_ExecuteScript(self.saGameInfoList[i])
    end
    
    -- |[Diagnostics]|
    Debug_PopPrint("Finished.\n")
end

-- |[ ================================ SysPaths:fnIsValidPath() ================================ ]|
--Given a path and an exceptions list, returns true if this path is valid for execution, false if
-- it is not. A path can be invalid if it refers to a file and not a directory, or if it is on
-- the exceptions list and should be ignored.
--If psaExceptionList is nil, then it is assumed there are no exceptions.
function SysPaths:fnIsValidPath(psPath, psFolderName, psaExceptionList)

    -- |[Argument Check]|
    if(psPath           == nil) then return false end
    if(psFolderName     == nil) then return false end
    if(psaExceptionList == nil) then psaExceptionList = {} end
    
    -- |[Execution]|
    --Check the last byte for '/'. This indicates it's a directory. Ignore files.
    local iLength = string.len(psPath)
    if(string.byte(psPath, iLength) ~= string.byte("/")) then
        return false
    end

    --Check if the string is on the exclusion list:
    for i = 1, #psaExceptionList, 1 do
        if(string.lower(psFolderName) == psaExceptionList[i]) then
            return false
        end
    end

    -- |[All Checks Passed]|
    return true
end

-- |[ ============================== SysPaths:fnScanGameFolders() ============================== ]|
--Scans the "Games/" directory for subfolders. This is where games are in a "standard" installation,
-- which is what 99% of the playerbase will be using.
--Note: Diagnostic flag is set by caller.
function SysPaths:fnScanGameFolders()

    -- |[Exceptions List]|
    --Any directory found on this list will not be executed.
    local saExceptionsList = {}

    -- |[Iteration]|
    --Open the directory.
    FS_Open("Games/", false)
    local sPath = FS_Iterate()
        
        --Iterate across the paths.
        while(sPath ~= "NULL") do

            --Setup.
            local sFolderName = fnResolveFolderName(sPath)
            local bIsValid = self:fnIsValidPath(sPath, sFolderName, saExceptionsList)

            --If not bypassing, execute.
            if(bIsValid == true) then
                if(FS_Exists(sPath .. "XGameInfo.lua") == true) then
                    --LM_ExecuteScript(sPath .. "XGameInfo.lua")
                    table.insert(self.saGameInfoList, sPath .. "XGameInfo.lua")
                    Debug_Print(" Valid game path: " .. sPath .. "XGameInfo.lua" .. "\n")
                end
            end

            --Next.
            sPath = FS_Iterate()
        end
        
    --Clean up.
    FS_Close()

end

-- |[ =============================== SysPaths:fnScanUpFolders() =============================== ]|
--Scans the "../" directories for subfolders. This is where games are in a repository installation,
-- which is what developers usually use. This allows the games to be worked on individually without
-- having to cross-upload to the repository changes to other games.
--Note: Diagnostic flag is set by caller.
function SysPaths:fnScanUpFolders()
    
    -- |[ ===== Primary Paths ====== ]|
    -- |[Setup]|
    --Build a list of paths that are available. This is the one-folder-up set. We do not immediately
    -- attempt to execute XGameInfo.lua here, instead we store them for later as games often have
    -- sub-directories which have XGameInfo in them.
    local saOneFolderUp = {}

    --Any directory found on this list will not be executed.
    local saExceptionsList = {"build archive", "builds", "guide", "physfs", "sound effects", "stringtyrantartpack", "sugarlumps"}

    -- |[Iteration]|
    --Open the directory.
    FS_Open("../", false)
    local sPath = FS_Iterate()
        
        --Iterate across the paths.
        while(sPath ~= "NULL") do

            --Setup.
            local sFolderName = fnResolveFolderName(sPath)
            local bIsValid = self:fnIsValidPath(sPath, sFolderName, saExceptionsList)

            --If not bypassing, add it to the list.
            if(bIsValid == true) then
                table.insert(saOneFolderUp, sPath)
                Debug_Print(" Valid Candidate: " .. sPath .. "\n")
            end

            --Next.
            sPath = FS_Iterate()
        end
        
    --Clean up.
    FS_Close()

    -- |[Debug]|
    if(false) then
        io.write("One folder up: \n")
        for i = 1, #saOneFolderUp, 1 do
            io.write(" " .. saOneFolderUp[i] .. "\n")
        end
    end

    -- |[ ==== Secondary Paths ===== ]|
    --Each of the secondary folders can contain its own "Games/" directory. If it does, check for the
    -- usual XGameInfo.lua in each of those folders.
    for i = 1, #saOneFolderUp, 1 do

        --Open.
        FS_Open(saOneFolderUp[i] .. "Games/", false)
        local sPath = FS_Iterate()
        
            --Iterate across the paths.
            while(sPath ~= "NULL") do
                
                --Setup. Note there are no exceptions for subfolders.
                local sFolderName = fnResolveFolderName(sPath)
                local bIsValid = self:fnIsValidPath(sPath, sFolderName, nil)

                --If not bypassing, execute the game info script.
                if(bIsValid == true) then
                    if(FS_Exists(sPath .. "XGameInfo.lua") == true) then
                        --LM_ExecuteScript(sPath .. "XGameInfo.lua")
                        table.insert(self.saGameInfoList, sPath .. "XGameInfo.lua")
                        Debug_Print(" Valid game path: " .. sPath .. "XGameInfo.lua" .. "\n")
                    end
                end
                
                --Next.
                sPath = FS_Iterate()
            end

        --Clean up.
        FS_Close()

    end
end

-- |[ ============================== SysPaths:fnScanOSXFolders() =============================== ]|
--Adds folder paths for OSX, which has a broken filesystem at the moment. This may be removed if it
-- gets fixed. This uses hard path locations so it's not flexible, but it will work at least!
--Note: Diagnostic flag is set by caller.
function SysPaths:fnScanOSXFolders()

    -- |[OS Check]|
    --Obviously don't call this if you're not on OSX.
    if(LM_GetSystemOS() ~= "OSX") then return end

    -- |[Setup]|
    local iSlot = 0

    -- |[Path Listings for Starlight Games]|
    --Adventure Mode
    if(LM_IsGameRegistered("Pandemonium") == true) then
        SysPaths:fnAddGameEntry("Adventure Mode", "Root/Paths/System/Startup/sAdventurePath", "Play Runes of Pandemonium", 100, "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Adventure Mode", "Games/AdventureMode/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Adventure Mode", "../adventure/Games/AdventureMode/ZLaunch.lua")
    end

    --String Tyrant and Demo
    if(LM_IsGameRegistered("String Tyrant") == true) then
        
        --Main Game
        SysPaths:fnAddGameEntry("Doll Manor", "Root/Paths/System/Startup/sDollManorPath", "Play String Tyrant", 101, "YMenuLaunch.lua", -23)
        SysPaths:fnAddSearchPathToEntry("Doll Manor", "Games/DollManor/000 Launcher Script.lua")
        SysPaths:fnAddSearchPathToEntry("Doll Manor", "../dollmanor/Games/DollManor/000 Launcher Script.lua")

        --Demo
        SysPaths:fnAddGameEntry("Doll Manor Demo", "Root/Paths/System/Startup/sDollManorDemoPath", "Play String Tyrant Demo", 102, "YMenuLaunch.lua", -23)
        SysPaths:fnAddSearchPathToEntry("Doll Manor Demo", "Games/DollManorDemo/000 Launcher Script.lua")
        SysPaths:fnAddSearchPathToEntry("Doll Manor Demo", "../dollmanor/Games/DollManorDemo/000 Launcher Script.lua")
    end

    --Slitted Eye
    if(LM_IsGameRegistered("Slitted Eye") == true) then
        SysPaths:fnAddGameEntry("Slitted Eye", "Root/Paths/System/Startup/sSlittedEyePath", "Play Slitted Eye", 103,  "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Slitted Eye", "Games/Slitted Eye/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Slitted Eye", "../slittedeye/Games/Slitted Eye/ZLaunch.lua")
    end

    --Electrosprite Text Adventure: Only appears if the player unlocked it in Adventure Mode.
    local sDoesOptionExist = OM_OptionExists("Unlock Electrosprites")
    if(sDoesOptionExist == "N" and LM_IsGameRegistered("Pandemonium") == true) then

        --Get variable.
        local fUnlockElectrosprites = OM_GetOption("Unlock Electrosprites")
        
        --Unlock it.
        if(fUnlockElectrosprites == 1.0) then
            SysPaths:fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "Play Electrosprite Adventure", 104, "YMenuLaunch.lua", -23)
            SysPaths:fnAddSearchPathToEntry("Electrosprite Adventure", "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
            SysPaths:fnAddSearchPathToEntry("Electrosprite Adventure", "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
        
        --Mark it, but don't add it.
        else
            SysPaths:fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "DO NOT ADD", 104, "YMenuLaunch.lua", -23)
            SysPaths:fnAddSearchPathToEntry("Electrosprite Adventure", "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
            SysPaths:fnAddSearchPathToEntry("Electrosprite Adventure", "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
        end

    --Option did not exist. Mark it, do not add it.
    else
        SysPaths:fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "DO NOT ADD", 104, "YMenuLaunch.lua", -23)
        SysPaths:fnAddSearchPathToEntry("Electrosprite Adventure", "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
        SysPaths:fnAddSearchPathToEntry("Electrosprite Adventure", "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
    end

    -- |[Non-Bottled-Starlight Games]|
    --Monsters of the Forest
    if(LM_IsGameRegistered("Pairanormal") == true) then
        SysPaths:fnAddGameEntry("MOTF", "Root/Paths/System/Startup/sMOTFPath", "Play Monsters of the Forest", 105, "YMenuLaunch.lua", -23)
        SysPaths:fnAddSearchPathToEntry("MOTF", "Games/MonstersOfTheForest/000 Launcher Script.lua")
        SysPaths:fnAddSearchPathToEntry("MOTF", "../monstersoftheforest/Games/MonstersOfTheForest/000 Launcher Script.lua")
    end

    --Pairanormal
    if(LM_IsGameRegistered("Pairanormal") == true) then
        SysPaths:fnAddGameEntry("Pairanormal", "Root/Paths/System/Startup/sPairanormalPath", "Play Pairanormal", 106,  "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Pairanormal", "Games/Pairanormal/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Pairanormal", "../pairanormal/Games/Pairanormal/ZLaunch.lua")
    end

    --Our Own Wonderland
    if(LM_IsGameRegistered("Our Own Wonderland") == true) then
        SysPaths:fnAddGameEntry("Our Own Wonderland", "Root/Paths/System/Startup/sWonderlandPath", "Play Our Own Wonderland", 107,  "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Our Own Wonderland", "Games/Wonderland/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Our Own Wonderland", "../oow/Games/Wonderland/ZLaunch.lua")
    end

    --Peak Freaks
    if(LM_IsGameRegistered("Peak Freaks") == true) then
        SysPaths:fnAddGameEntry("Peak Freaks", "Root/Paths/System/Startup/sPeakFreaksPath", "Play Peak Freaks", 108,  "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Peak Freaks", "Games/PeakFreaks/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Peak Freaks", "../peakfreaks/Games/PeakFreaks/ZLaunch.lua")
    end

    -- |[Path Listing for Classic Mode Games]|
    if(LM_IsGameRegistered("Pandemonium Classic") == true) then
        --Classic Mode
        SysPaths:fnAddGameEntry("Classic Mode", "Root/Paths/System/Startup/sClassicModePath", "Play Classic Mode", 109, "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Classic Mode", "Games/ClassicMode/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Classic Mode", "../classic/Games/ClassicMode/ZLaunch.lua")

        --Classic Mode 3D
        SysPaths:fnAddGameEntry("Classic Mode 3D", "Root/Paths/System/Startup/sClassicMode3DPath", "Play Classic Mode 3D", 110, "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Classic Mode 3D", "Games/ClassicMode3D/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Classic Mode 3D", "../classic/Games/ClassicMode3D/ZLaunch.lua")

        --Corrupter Mode
        SysPaths:fnAddGameEntry("Corrupter Mode", "Root/Paths/System/Startup/sCorrupterModePath", "Play Corrupter Mode", 111,  "YMenuLaunch.lua", -12)
        SysPaths:fnAddSearchPathToEntry("Classic Mode 3D", "Games/CorrupterMode/ZLaunch.lua")
        SysPaths:fnAddSearchPathToEntry("Classic Mode 3D", "../classic/Games/CorrupterMode/ZLaunch.lua")
    end

    -- |[Tools]|
    --Level Generator
    if(LM_IsGameRegistered("Pandemonium") == true) then
        SysPaths:fnAddGameEntry("Level Generator", "Root/Paths/System/Startup/sLevelGeneratorPath", "Show Level Generator", 112, "YMenuLaunch.lua", -10)
        SysPaths:fnAddSearchPathToEntry("Classic Mode 3D", "Games/AdventureLevelGenerator/Grids.lua")
        SysPaths:fnAddSearchPathToEntry("Classic Mode 3D", "../adventure/Games/AdventureLevelGenerator/Grids.lua")
    end

end