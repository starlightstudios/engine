//--Base
#include "DisplayManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"
#include "StarBitmap.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "OptionsManager.h"

///--[ ====================================== SDL Version ======================================= ]
//--DisplayManager post-setup using SDL. This will create the display and store any screen info
//  it can.
#if defined _SDL_PROJECT_
void DisplayManager::ConstructorTail()
{
    ///--[Documentation]
    //--Function is called immediately after the DisplayManager has called its setup functions.
    //  It will create the display.
    DebugManager::PushPrint(true, "[DisplayManager] Creating display...\n");

    //--Setup.
    uint32_t tWindowFlags = SDL_WINDOW_OPENGL;
    GLOBAL *rGlobal = Global::Shared();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--OpenGL Library.
    SDL_GL_LoadLibrary(NULL);

    //--Version request.
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, rOptionsManager->mRequestedAccelerateVisual);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, rOptionsManager->mRequestedOpenGLMajor);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, rOptionsManager->mRequestedOpenGLMinor);

    //--Allow backwards compatibility.
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

    //--Color buffer request.
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    //--Fullscreen or Windowed Flag
    DebugManager::Print("Handling fullscreen/windowed flags.\n");
    bool tUseFullscreen = rOptionsManager->GetOptionB("Fullscreen");
    if(!tUseFullscreen)
    {
        mIsFullscreen = false;
    }
    else
    {
        tWindowFlags = tWindowFlags | SDL_WINDOW_FULLSCREEN;
        mIsFullscreen = true;
    }

    //--Sets up the Depth and Stencil Buffer stuff.  These can be blocked for debug purposes
    //  by the options.
    DebugManager::Print("Checking depth buffer flag.\n");
    bool tBlockDepthBuffer = rOptionsManager->GetOptionB("BlockDepthBuffer");
    if(!tBlockDepthBuffer)
    {
        //--This option is disabled in SDL.
    }

    DebugManager::Print("Checking stencil buffer flag.\n");
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    bool tBlockStencilBuffer = rOptionsManager->GetOptionB("BlockStencilBuffer");
    if(!tBlockStencilBuffer)
    {
        //--This option is disabled in SDL.
    }

    //--Get the location to spawn the window.  Only executes if either is non
    //  zero for compatibility reasons.
    DebugManager::Print("Checking window location flags.\n");
    int tMonitor = rOptionsManager->GetOptionI("MandateMonitor");
    int tWindowX = rOptionsManager->GetOptionI("StartWinX");
    int tWindowY = rOptionsManager->GetOptionI("StartWinY");

    //--If "MandateMonitor" is not -1, try to figure out the X/Y coordinates of the monitor the player is mandating.
    //  Spawn the window slightly offset from the top-left of that monitor.
    if(tMonitor != -1)
    {
        //--Setup.
        int tTotalMonitors = SDL_GetNumVideoDisplays();
        SDL_Rect tTempRect;

        //--Check if the monitor is in range. If out of range, bark a warning and set the monitor option to -1.
        if(tMonitor < 0 || tMonitor >= tTotalMonitors)
        {
            rOptionsManager->SetOptionI("MandateMonitor", -1);
            DebugManager::ForcePrint("Warning: User attempted to mandate monitor %i but was out of range. Monitors detected: %i\n", tMonitor, tTotalMonitors);
        }
        //--In range.
        else
        {
            //--Something went wrong and the monitor failed to resolve.
            if(SDL_GetDisplayBounds(tMonitor, &tTempRect) != 0)
            {
                rOptionsManager->SetOptionI("MandateMonitor", -1);
                DebugManager::ForcePrint("Warning. Mandated monitor %i failed to resolve. SDL Error: %s\n", tMonitor, SDL_GetError());
            }
            //--Set.
            else
            {
                tWindowX = tTempRect.x + WIN_BORDER_X;
                tWindowY = tTempRect.y + WIN_BORDER_Y;
            }
        }
    }

    //--Flag for stereo buffers. Can be switched off.
    DebugManager::Print("Checking block-double-buffer flag.\n");
    if(!rOptionsManager->GetOptionB("BlockDoubleBuffering"))
    {
        //--This option is disabled in SDL.
    }

    //--Get the size of the window (not the virtual canvas).
    rGlobal->gWindowWidth = rOptionsManager->GetOptionI("WinSizeX");
    rGlobal->gWindowHeight = rOptionsManager->GetOptionI("WinSizeY");

    //--Create the Display and test it.
    DebugManager::ForcePrint("\n");
    DebugManager::ForcePrint("If the program crashes at this point, then your display could not be created.\n");
    DebugManager::ForcePrint("This means your OGL drivers are not updated, or the program is unable to run.\n");
    mWindow = SDL_CreateWindow("Starlight Engine", tWindowX, tWindowY, rGlobal->gWindowWidth, rGlobal->gWindowHeight, tWindowFlags);
    if(!mWindow)
    {
        DebugManager::ForcePrint("Display of size %i by %i failed to create. Error: %s\n", rGlobal->gWindowWidth, rGlobal->gWindowHeight, SDL_GetError());
        DebugManager::ForcePrint("With no display, the program will now exit\n");
        rGlobal->gQuit = true;
        return;
    }

    //--Associate the GL Context with this window.
    mGLContext = SDL_GL_CreateContext(mWindow);

    //--Find the display mode that worked and set it as active in the DisplayInfo list.
    DebugManager::Print("Storing active display mode.\n");
    for(int i = 0; i < mDisplayModesTotal; i ++)
    {
        if(mDisplayModes[i].mWidth == rGlobal->gWindowWidth && mDisplayModes[i].mHeight == rGlobal->gWindowHeight)
        {
            mActiveDisplaySlot = i;
            break;
        }
    }

    //--Standard GL Settings (GL Stack pops back to these)
    DebugManager::Print("Setting OpenGL standards.\n");
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);

    //--Static setup.
    DebugManager::Print("Building enumerations list.\n");
    ConstructEnumerationList();

    //--Store GL statistics.
    DebugManager::Print("Getting GL statistics.\n");
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &StarBitmap::xMaxTextureSize);
    StarBitmap::xAtlasMaxSize = StarBitmap::xMaxTextureSize / 2;
    //StarBitmap::xMaxTextureSize = 4096;

    //--These may not be the same if fullscreen was set, so store them.
    DebugManager::Print("Refreshing screen sizes.\n");
    SDL_GetWindowSize(mWindow, &rGlobal->gWindowWidth, &rGlobal->gWindowHeight);

    //--Clean up.
    DebugManager::PopPrint("[DisplayManager] Display setup complete.\n");
}
void DisplayManager::SaveDisplayModes()
{
    ///--[Documentation]
    //--Checks the available display modes and stores their data inside a library-independent structure.
    //--Collects a list of all available display modes. We are only interested in unique modes:
    //  two modes with the same W/H but different refresh rates are ignored, we only want modes
    //  with refresh rates of 60.0 fps. Likewise, only 32bit formats are acceptable.

    ///--[Monitor Info]
    //--Total number of monitors.
    mDisplaysTotal = SDL_GetNumVideoDisplays();
    fprintf(stderr, "Total monitors detected: %i\n", mDisplaysTotal);

    //--Get info.
    for(int i = 0; i < mDisplaysTotal; i ++)
    {
        SDL_Rect tTempRect;
        if(SDL_GetDisplayBounds(i, &tTempRect) != 0)
        {
            fprintf(stderr, " Monitor %i: Error. Code %s\n", i, SDL_GetError());
        }
        else
        {
            fprintf(stderr, " Monitor %i: %i %i - %i %i\n", i, tTempRect.x, tTempRect.y, tTempRect.w, tTempRect.h);
        }
    }

    ///--[Display Query]
    //--Temporary values.
    SDL_DisplayMode tDisplayData;
    char tBuffer[128];
    int tPrevW = 0;
    int tPrevH = 0;

    //--Temporary linked list. Stores all valid modes.
    StarLinkedList *tValidModesList = new StarLinkedList(true);

    //--For each display:
    for(int i = 0; i < mDisplaysTotal; i ++)
    {
        //--Get how many modes are expected on this display.
        int tModesTotal = SDL_GetNumDisplayModes(i);

        //--For each mode in the display:
        for(int p = 0; p < tModesTotal; p ++)
        {
            //--Get mode data. SDL passes NULL back on failure.
            if(SDL_GetDisplayMode(i, p, &tDisplayData)) continue;

            //--If this is the zeroth mode in this group, or if this mode has a different size than the previous mode,
            //  add it to the global list.
            if(p == 0 || (tDisplayData.w != tPrevW || tDisplayData.h != tPrevH))
            {
                //--Create a new display entry.
                DisplayInfo *nInfo = (DisplayInfo *)starmemoryalloc(sizeof(DisplayInfo));
                nInfo->Initialize();
                nInfo->mDisplayNum  = i;
                nInfo->mWidth       = tDisplayData.w;
                nInfo->mHeight      = tDisplayData.h;
                nInfo->mRefreshRate = tDisplayData.refresh_rate;

                //--Create a string which has this as simple, human-readable data.
                sprintf(tBuffer, "Dis%i %ix%ix%i", i, nInfo->mWidth, nInfo->mHeight, nInfo->mRefreshRate);
                ResetString(nInfo->mDetails, tBuffer);

                //--Register.
                tValidModesList->AddElementAsTail("X", nInfo, &DisplayInfo::DeleteThis);
            }
            //--This mode is identical to a previous mode with a different refresh rate, which we don't care about. Ignore it.
            else
            {

            }

            //--In all cases, store the W/H.
            tPrevW = tDisplayData.w;
            tPrevH = tDisplayData.h;
        }
    }

    ///--[Display Storage]
    //--We now have a linked list holding all of the display data. Convert this list into a fixed-size array.
    mDisplayModesTotal = tValidModesList->GetListSize();
    mDisplayModes = (DisplayInfo *)starmemoryalloc(sizeof(DisplayInfo) * mDisplayModesTotal);

    //--Begin filling.
    for(int i = 0; i < mDisplayModesTotal; i ++)
    {
        //--Initialize.
        mDisplayModes[i].Initialize();

        //--Get the base entry.
        DisplayInfo *rSourceInfo = (DisplayInfo *)tValidModesList->GetElementBySlot(i);
        if(!rSourceInfo) continue;

        //--Copy.
        mDisplayModes[i].mDisplayNum = rSourceInfo->mDisplayNum;
        mDisplayModes[i].mWidth = rSourceInfo->mWidth;
        mDisplayModes[i].mHeight = rSourceInfo->mHeight;
        mDisplayModes[i].mRefreshRate = rSourceInfo->mRefreshRate;

        //--Liberate the string, no need to generate it twice.
        mDisplayModes[i].mDetails = rSourceInfo->mDetails;
        rSourceInfo->mDetails = NULL;
    }

    //--Dummy display mode. Do not deallocate until destructor!
    mDummyDisplayInfo.mDisplayNum = 0;
    mDummyDisplayInfo.mWidth = 0;
    mDummyDisplayInfo.mHeight = 0;
    mDummyDisplayInfo.mRefreshRate = 0;
    mDummyDisplayInfo.mDetails = NULL;
    ResetString(mDummyDisplayInfo.mDetails, "ERROR");

    //--Clean.
    delete tValidModesList;
}
#endif
