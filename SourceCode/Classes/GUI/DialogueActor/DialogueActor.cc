//--Base
#include "DialogueActor.h"

//--Classes
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
DialogueActor::DialogueActor()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_DIALOGUEACTOR;

    ///--[ ===== DialogueActor ====== ]
    //--System
    //--Names
    mLocalName = InitializeString("Unnamed Dialogue Actor");
    mAliasList = new StarLinkedList(true);

    //--Images
    mActiveOffsetX = 0;
    mActiveOffsetY = 0;
    rActiveImage = NULL;
    mImageList = new StarLinkedList(false);
    mImageListRev = new StarLinkedList(false);
    mImageListPaths = new StarLinkedList(true);
    mIsNotWideImageList = new StarLinkedList(false);
    mIsNotWideImageListRev = new StarLinkedList(false);
    mOffsetsList = new StarLinkedList(true);

    //--Multi-layer emotions.
    mrAdditionalLayerList = new StarLinkedList(false);

    //--Render State
    mSizeTimer = 0;
    mIsDarkened = false;
}
DialogueActor::~DialogueActor()
{
    free(mLocalName);
    delete mAliasList;
    delete mImageList;
    delete mImageListRev;
    delete mImageListPaths;
    delete mIsNotWideImageList;
    delete mIsNotWideImageListRev;
    delete mrAdditionalLayerList;
    delete mOffsetsList;
}

///===================================== Property Queries =========================================
const char *DialogueActor::GetName()
{
    return (const char *)mLocalName;
}
bool DialogueActor::IsSpeaking(const char *pSpeakerName)
{
    if(!pSpeakerName) return false;
    return (mAliasList->GetElementByName(pSpeakerName) != NULL);
}
bool DialogueActor::IsDarkened()
{
    return mIsDarkened;
}
bool DialogueActor::IsFullDark()
{
    if(mSizeTimer == 0) return true;
    return false;
}
StarBitmap *DialogueActor::GetActiveImage()
{
    return rActiveImage;
}
const char *DialogueActor::GetPathOfEmotion(const char *pEmotion)
{
    //--Returns the DLPath to the named emotion, if it exists. Returns NULL if not found.
    return (const char *)mImageListPaths->GetElementByName(pEmotion);
}
float DialogueActor::GetOffsetX(const char *pEmotion)
{
    //--If the package doesn't exist, returns the default of 0.0f
    TwoDimensionRealPoint *rPoint = (TwoDimensionRealPoint *)mOffsetsList->GetElementByName(pEmotion);
    if(!rPoint) return 0.0f;
    return rPoint->mXCenter;
}
float DialogueActor::GetActiveOffsetX()
{
    return mActiveOffsetX;
}
const char *DialogueActor::GetNameFromAlias(const char *pAlias)
{
    ///--[Documentation]
    //--Given an alias, checks if the character has an override for that name. If they do not, then
    //  returns NULL. In addition, the override "DEFAULT" also returns NULL, as it is legal to have
    //  an alias with no overrides.
    //--Overrides are typically used for translations as aliases are meant to change the character's
    //  visual name anyway.
    if(!pAlias) return NULL;

    ///--[Execution]
    //--If the alias isn't on the list, stop.
    const char *rCheckString = (const char *)mAliasList->GetElementByName(pAlias);
    if(!rCheckString) return NULL;

    //--If the alias has "DEFAULT" as its match, stop.
    if(!strcasecmp(rCheckString, "DEFAULT")) return NULL;

    //--This is an override. Return it.
    return rCheckString;
}

///======================================== Manipulators ==========================================
void DialogueActor::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void DialogueActor::AddAlias(const char *pName, const char *pDisplayName)
{
    ///--[Documentation]
    //--When speaking with an alias, a character may has a further modified display name. This is
    //  used almost exclusively for translations.
    //--The display name is independent of the alias name, and can be duplicated. The alias cannot
    //  be duplicated. If the display name "DEFAULT" is passed, then the alias name renders normally.
    if(!pName || !pDisplayName) return;

    ///--[Execution]
    //--Duplicate check.
    if(mAliasList->GetElementByName(pName) != NULL) return;

    //--Create a copy and register.
    char *nDisplayName = InitializeString(pDisplayName);
    mAliasList->AddElement(pName, nDisplayName, &FreeThis);
}
void DialogueActor::RemoveAlias(const char *pName)
{
    //--Remove every instance of the alias. Nominally it'd only appear once, but...
    while(mAliasList->RemoveElementS(pName))
    {

    }
}
void DialogueActor::AddPortrait(const char *pName, const char *pDLPath, bool pIsNotWide)
{
    ///--[Documentation and Setup]
    //--Adds a new portrait. Replaces it if that name already exists.
    if(!pName || !pDLPath) return;

    //--Debug.
    //fprintf(stderr, "Adding portrait for %s: %s, %s\n", mLocalName, pName, pDLPath);
    //fprintf(stderr, " Current list size: %i\n", mImageList->GetListSize());

    ///--[Clear]
    //--If the path is "Null" then remove the element and be done with it.
    if(!strcasecmp(pDLPath, "Null"))
    {
        mImageList->RemoveElementS(pName);
        mIsNotWideImageList->RemoveElementS(pName);
        while(mImageListPaths->RemoveElementS(pName)) {}
        return;
    }

    ///--[Replacing]
    //--Check if this portrait name already exists. If so, replace it.
    void *rCheckPortrait = mImageList->GetElementByName(pName);
    if(rCheckPortrait)
    {
        mImageList->RemoveElementS(pName);
        mIsNotWideImageList->RemoveElementS(pName);
    }
    void *rCheckPath = mImageListPaths->GetElementByName(pName);
    if(rCheckPath)
    {
        mImageListPaths->RemoveElementS(pName);
    }

    //--Static HD copies.
    static bool xTrue = true;
    static bool xFalse = false;

    //--Add a copy.
    void *rCheckPtr = DataLibrary::Fetch()->GetEntry(pDLPath);
    mImageList->AddElementAsTail(pName, rCheckPtr);
    if(pIsNotWide)
    {
        mIsNotWideImageList->AddElementAsTail(pName, &xTrue);
    }
    else
    {
        mIsNotWideImageList->AddElementAsTail(pName, &xFalse);
    }

    //--In either case, make a copy of the path and store it.
    mImageListPaths->AddElement(pName, InitializeString("%s", pDLPath), &FreeThis);

    ///--[Warning]
    //--Only enable this when debugging, otherwise it barks incorrect errors when loading the game
    //  since characters who are not in the chapter still reference, without loading, their portraits.
    bool tShowDebug = DebugManager::GetDebugFlag("Talk Sprite: Missing Image");
    if(tShowDebug && !rCheckPtr) DebugManager::ForcePrint("DialogueActor:AddPortrait - Warning, no image %s found.\n", pDLPath);
    //fprintf(stderr, " Added list size: %i\n", mImageList->GetListSize());
}
void DialogueActor::AddPortraitRev(const char *pName, const char *pDLPath, bool pIsNotWide)
{
    ///--[Documentation and Setup]
    //--Adds a new portrait for the reverse case, which appears if the character is on the right side
    //  of the dialogue. If a reverse exists, it is displayed, otherwise the normal case appears flipped.
    //--Reverse portraits do not store their paths on a list.
    if(!pName || !pDLPath) return;

    ///--[Clear]
    //--If the path is "Null" then remove the element and be done with it.
    if(!strcasecmp(pDLPath, "Null"))
    {
        mImageListRev->RemoveElementS(pName);
        mIsNotWideImageListRev->RemoveElementS(pName);
        return;
    }

    ///--[Replacing]
    //--Check if this portrait name already exists. If so, replace it.
    void *rCheckPortrait = mImageListRev->GetElementByName(pName);
    if(rCheckPortrait)
    {
        mImageListRev->RemoveElementS(pName);
        mIsNotWideImageListRev->RemoveElementS(pName);
    }

    //--Static HD copies.
    static bool xTrue = true;
    static bool xFalse = false;

    //--Add a copy.
    void *rCheckPtr = DataLibrary::Fetch()->GetEntry(pDLPath);
    mImageListRev->AddElementAsTail(pName, rCheckPtr);
    if(pIsNotWide)
    {
        mIsNotWideImageListRev->AddElementAsTail(pName, &xTrue);
    }
    else
    {
        mIsNotWideImageListRev->AddElementAsTail(pName, &xFalse);
    }

    ///--[Warning]
    //--Only enable this when debugging, otherwise it barks incorrect errors when loading the game
    //  since characters who are not in the chapter still reference, without loading, their portraits.
    //if(!rCheckPtr) DebugManager::ForcePrint("DialogueActor:AddPortraitRev - Warning, no image %s found.\n", pDLPath);
}
void DialogueActor::RemovePortrait(const char *pName)
{
    //--Remove all portraits matching the name.
    while(mImageList->RemoveElementS(pName))
    {
    }
    while(mImageListRev->RemoveElementS(pName))
    {
    }
    while(mImageListPaths->RemoveElementS(pName))
    {
    }
    while(mIsNotWideImageList->RemoveElementS(pName))
    {
    }
    while(mIsNotWideImageListRev->RemoveElementS(pName))
    {
    }
}
void DialogueActor::SetActivePortrait(const char *pName)
{
    //--Locate the requested portrait and set it as the active one. Passing NULL is legal but will
    //  clear the portrait (so don't do that).
    if(!pName)
    {
        rActiveImage = NULL;
        mActiveOffsetX = 0.0f;
        mActiveOffsetY = 0.0f;
        mrAdditionalLayerList->ClearList();
    }
    //--If the first six letters are "LAYER|" then this is a layered emotion case.
    else if(!strncasecmp(pName, "LAYER|", 6))
    {
        //--Active image is NULL.
        rActiveImage = NULL;
        mActiveOffsetX = 0.0f;
        mActiveOffsetY = 0.0f;

        //--Uses a while loop as there may be a variable number of layers. Each layer is delimited by a '|' symbol.
        char tBuffer[128];
        int tCurBufLetter = 0;
        int tCurLetter = 6;
        int cLength = (int)strlen(pName);
        while(tCurLetter < cLength)
        {
            //--Copy over.
            if(pName[tCurLetter] != '|')
            {
                tBuffer[tCurBufLetter+0] = pName[tCurLetter];
                tBuffer[tCurBufLetter+1] = '\0';
                tCurBufLetter ++;
            }
            //--Bar. Add the layer.
            else
            {
                mrAdditionalLayerList->AddElement("X", mImageList->GetElementByName(tBuffer));
                tCurBufLetter = 0;
                tBuffer[0] = '\0';
            }

            //--Next.
            tCurLetter ++;
        }
    }
    //--Otherwise, this is a single-emotion case.
    else
    {
        rActiveImage = (StarBitmap *)mImageList->GetElementByName(pName);
        mrAdditionalLayerList->ClearList();

        //--Check if an offset matches. If not, default to 0's for the offsets.
        TwoDimensionRealPoint *rOffset = (TwoDimensionRealPoint *)mOffsetsList->GetElementByName(pName);
        if(rOffset)
        {
            mActiveOffsetX = rOffset->mXCenter;
            mActiveOffsetY = rOffset->mYCenter;
        }
        else
        {
            mActiveOffsetX = 0.0f;
            mActiveOffsetY = 0.0f;
        }
    }
}
void DialogueActor::SetDarkenedState(bool pFlag)
{
    mIsDarkened = pFlag;
}
void DialogueActor::AddOffset(const char *pName, float pX, float pY)
{
    //--Offset already exists. Overwrite it.
    TwoDimensionRealPoint *rPoint = (TwoDimensionRealPoint *)mOffsetsList->GetElementByName(pName);
    if(rPoint)
    {
        rPoint->mXCenter = pX;
        rPoint->mYCenter = pY;
        return;
    }

    //--Doesn't exist, create it.
    TwoDimensionRealPoint *nPoint = (TwoDimensionRealPoint *)starmemoryalloc(sizeof(TwoDimensionRealPoint));
    nPoint->mXCenter = pX;
    nPoint->mYCenter = pY;
    mOffsetsList->AddElement(pName, nPoint, &FreeThis);
}

///======================================== Core Methods ==========================================
void DialogueActor::SetDarkeningFromName(const char *pName)
{
    //--Given a name, checks if the name is any of this DialogueActor's aliases. If it is, then
    //  this DialogueActor is speaking or otherwise should not be darkened.
    SetDarkenedState(true);
    if(!pName) return;

    //--Scan the alias list. If it exists, we're not darkened.
    void *rCheckPtr = mAliasList->GetElementByName(pName);
    SetDarkenedState(rCheckPtr == NULL);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void DialogueActor::Update()
{
    //--Just handles the size modification timers.
    if(mIsDarkened)
    {
        if(mSizeTimer > 0) mSizeTimer --;
    }
    else
    {
        if(mSizeTimer < SIZE_MOD_TICKS) mSizeTimer ++;
    }
}

///========================================== File I/O ============================================
void DialogueActor::DebugPrintData()
{
    ///--[Documentation]
    //--Prints internal information about this actor to the console to aid in diagnostics.
    fprintf(stderr, "== Printing Actor Info ==\n");
    fprintf(stderr, "Actor Name: %s\n", mLocalName);
    fprintf(stderr, "Aliases: %i\n", mAliasList->GetListSize());
    void *rPtr = mAliasList->PushIterator();
    while(rPtr)
    {
        fprintf(stderr, " %s\n", mAliasList->GetIteratorName());
        rPtr = mAliasList->AutoIterate();
    }
    fprintf(stderr, "Emotions: %i\n", mImageList->GetListSize());
    StarBitmap *rImagePtr = (StarBitmap *)mImageList->PushIterator();
    while(rImagePtr)
    {
        fprintf(stderr, " %s - %p, %i %i\n", mImageList->GetIteratorName(), rImagePtr, rImagePtr->GetTrueWidthSafe(), rImagePtr->GetTrueHeightSafe());
        rImagePtr = (StarBitmap *)mImageList->AutoIterate();
    }
    fprintf(stderr, "Paths: %i\n", mImageListPaths->GetListSize());
    char *rImagePathPtr = (char *)mImageListPaths->PushIterator();
    while(rImagePathPtr)
    {
        fprintf(stderr, " %s - %s\n", mImageListPaths->GetIteratorName(), rImagePathPtr);
        rImagePathPtr = (char *)mImageListPaths->AutoIterate();
    }
    fprintf(stderr, "== End of Actor Info ==\n");
}

///========================================== Drawing =============================================
StarBitmap *DialogueActor::RenderAt(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Overload, uses the active image. Will auto-switch to layered rendering if required. Returns the image
    //  it rendered, usually used for asset streaming.
    RenderAt(rActiveImage, pXCenter, pYBottom, pGlobalAlpha, pFlags);
    return rActiveImage;
}
StarBitmap *DialogueActor::RenderAt(StarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    ///--[Documentation and Setup]
    //--Renders whatever the current image is at the provided position. Portraits always render
    //  bottom-up and are centered. If no image is provided, attempts to use layered rendering.
    //--Returns the image it rendered, which is the one it was passed.
    if(!pUseImage)
    {
        RenderAtLayered(pXCenter, pYBottom, pGlobalAlpha, pFlags);
        return NULL;
    }

    ///--[Modify Offsets]
    float tOldOffsetX = mActiveOffsetX;
    if(pUseImage != rActiveImage)
    {
        //--If the slot is valid, temporarily set the offset to use the alternate offset.
        int tSlot = mImageList->GetSlotOfElementByPtr(pUseImage);
        if(tSlot != -1)
        {
            const char *rEmotionName = mImageList->GetNameOfElementBySlot(tSlot);
            TwoDimensionRealPoint *rPoint = (TwoDimensionRealPoint *)mOffsetsList->GetElementByName(rEmotionName);
            if(rPoint)
            {
                mActiveOffsetX = rPoint->mXCenter;
            }
            else
            {
                mActiveOffsetX = 0.0f;
            }
        }
    }

    ///--[Mixer]
    //--If this entity is darkened, modify the mixer.
    if(mIsDarkened)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pGlobalAlpha);
    }
    //--Normal color mixing.
    else
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
    }

    ///--[Setup]
    //--Constants.
    float cPortraitScale = 0.50f;
    float cWidePortraitWid = 1392.0f;
    float cNarrowPortraitWid = 694.0f;
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");
    if(tIsSmallPortraitMode) pXCenter = pXCenter - 64.0f;

    //--Resolve the size.
    float cSizeAmount = SIZE_MOD_AMOUNT;
    if(tIsSmallPortraitMode) cSizeAmount = cSizeAmount * 0.50f;

    //--Modify.
    float cSizeMod = EasingFunction::QuadraticInOut(mSizeTimer, SIZE_MOD_TICKS) * cSizeAmount;
    if(cSizeMod != 0.0f) cPortraitScale = cPortraitScale + cSizeMod;

    //--Locate the position of the active image and figure out if it is "Wide" or not.
    int tSlot = mImageList->GetSlotOfElementByPtr(pUseImage);
    bool *rIsNotWideFlag = (bool *)mIsNotWideImageList->GetElementBySlot(tSlot);

    ///--[Normal Rendering]
    //--Not wide. This is most of the player party portraits and some often-seen NPCs.
    if(rIsNotWideFlag && (*rIsNotWideFlag))
    {
        ///--[Flip Check]
        //--Flipping case: If a flipped variant exists and the flags call for it, swap it out for
        //  the flipped variant.
        if(pFlags & SUGAR_FLIP_HORIZONTAL && tSlot != -1)
        {
            //--Check for a flipped version.
            const char *rName = mImageList->GetNameOfElementBySlot(tSlot);
            StarBitmap *rImage = (StarBitmap *)mImageListRev->GetElementByName(rName);

            //--If it exists, replace it.
            if(rImage)pUseImage = rImage;
        }

        ///--[Load Instruction]
        //--All dialogue images attempt to load their image or enqueue it for asset streaming right before
        //  they render, if the flag WorldDialogue::xUnloadDialoguePortraitsWhenClosing is true. This
        //  prevents them from displaying a warning that the asset was not loaded before rendering.
        //--If the flag is false, the warning is *expected* because the code should have already loaded
        //  the asset, so we don't attempt to load before rendering.
        if(pUseImage->GetGLTextureHandle() == 0)
        {
            pUseImage->HandleNoDataAtRender(SUGARBITMAP_NODATA_DONT_CARE, false);
        }

        ///--[Rendering]
        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            //--Position.
            float tRenderX = pXCenter - (cNarrowPortraitWid * 0.50f * cPortraitScale);
            float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale);

            //--Offsets.
            if(pFlags & FLIP_HORIZONTAL)
            {
                tRenderX = tRenderX - (mActiveOffsetX * cPortraitScale);
            }
            else
            {
                tRenderX = tRenderX + (mActiveOffsetX * cPortraitScale);
            }
            tRenderY = tRenderY + (mActiveOffsetY * cPortraitScale);

            //--Render.
            pUseImage->DrawScaled(tRenderX, tRenderY, cPortraitScale, cPortraitScale, pFlags);
        }
        //--Small portrait rendering. The portraits are even smaller.
        else
        {
            //--Scale.
            float cUseScale = 1.0f + (cSizeMod * 2.0f);
            float cScaleInv = 1.0f / cUseScale;

            //--Centering.
            float cXOff = pUseImage->GetTrueWidth()  * -0.125f;
            float cYOff = pUseImage->GetTrueHeight() * -0.25f;

            //--Because the shrunken bitmap has an incorrect X offset, we need to manually change it here.
            if(pFlags & SUGAR_FLIP_HORIZONTAL) cXOff = cXOff - 524.0f;

            //--Offsets.
            if(pFlags & FLIP_HORIZONTAL)
            {
                cXOff = cXOff - (mActiveOffsetX * cPortraitScale);
            }
            else
            {
                cXOff = cXOff + (mActiveOffsetX * cPortraitScale);
            }
            cYOff = cYOff + (mActiveOffsetY * cPortraitScale);

            //--Render.
            glTranslatef(pXCenter, pYBottom + 2.0f, 0.0f);
            glScalef(cUseScale, cUseScale, 1.0f);
            glTranslatef(cXOff, cYOff, 0.0f);
                pUseImage->Draw(0, 0, pFlags);
            glTranslatef(-cXOff, -cYOff, 0.0f);
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-pXCenter, -pYBottom - 2.0f, 0.0f);
        }
    }
    ///--[Wide Rendering]
    //--A "Wide" image uses a wider offset. Enemy portraits use a 1392 standard instead of a 694 standard.
    else
    {
        ///--[Load Instruction]
        //--All dialogue images attempt to load their image or enqueue it for asset streaming right before
        //  they render, if the flag WorldDialogue::xUnloadDialoguePortraitsWhenClosing is true. This
        //  prevents them from displaying a warning that the asset was not loaded before rendering.
        //--If the flag is false, the warning is *expected* because the code should have already loaded
        //  the asset, so we don't attempt to load before rendering.
        if(pUseImage->GetGLTextureHandle() == 0)
        {
            pUseImage->HandleNoDataAtRender(SUGARBITMAP_NODATA_DONT_CARE, false);
        }

        ///--[Rendering]
        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            //--Position.
            float tRenderX = pXCenter - (cWidePortraitWid * 0.50f * cPortraitScale);
            float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale);

            //--Offsets.
            if(pFlags & FLIP_HORIZONTAL)
            {
                tRenderX = tRenderX - (mActiveOffsetX * cPortraitScale);
            }
            else
            {
                tRenderX = tRenderX + (mActiveOffsetX * cPortraitScale);
            }
            tRenderY = tRenderY + (mActiveOffsetY * cPortraitScale);

            //--Render.
            pUseImage->DrawScaled(tRenderX, tRenderY, cPortraitScale, cPortraitScale, pFlags);
        }
        //--Small portrait rendering. The portraits are even smaller.
        else
        {
            //--Scale.
            float cUseScale = 1.0f + (cSizeMod * 2.0f);
            float cScaleInv = 1.0f / cUseScale;

            //--Centering.
            float cXOff = pUseImage->GetTrueWidth()  * -0.125f;
            float cYOff = pUseImage->GetTrueHeight() * -0.25f;

            //--Because the shrunken bitmap has an incorrect X offset, we need to manually change it here.
            if(pFlags & SUGAR_FLIP_HORIZONTAL) cXOff = cXOff - 1043.0f;

            //--Offsets.
            if(pFlags & FLIP_HORIZONTAL)
            {
                cXOff = cXOff - (mActiveOffsetX * cPortraitScale);
            }
            else
            {
                cXOff = cXOff + (mActiveOffsetX * cPortraitScale);
            }
            cYOff = cYOff + (mActiveOffsetY * cPortraitScale);

            //--Render.
            glTranslatef(pXCenter, pYBottom + 2.0f, 0.0f);
            glScalef(cUseScale, cUseScale, 1.0f);
            glTranslatef(cXOff, cYOff, 0.0f);
                pUseImage->Draw(0, 0, pFlags);
            glTranslatef(-cXOff, -cYOff, 0.0f);
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-pXCenter, -pYBottom - 2.0f, 0.0f);
        }
    }

    ///--[Clean]
    //--Clean.
    mActiveOffsetX = tOldOffsetX;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
    return pUseImage;
}
void DialogueActor::RenderAtLayered(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    ///--[Documentation]
    //--Attempts to render assuming the portrait is a layered depiction. If the list contains no entries
    //  then doesn't render anything.
    //--Sizing for layered portraits is very specific and does not support low-res options for now.
    if(mrAdditionalLayerList->GetListSize() < 1) return;

    //--Constants. For now, layered emotions are always 700px wide.
    float cExpectedWid = 700.0f;

    //--If this entity is darkened, modify the mixer. All images use the same mixing.
    if(mIsDarkened)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pGlobalAlpha);
    }
    //--Normal color mixing.
    else
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
    }

    //--Modify size. All layers use the same sizing.
    float cSizeAmount = SIZE_MOD_AMOUNT;
    float cPortraitScale = 1.00f;
    float cSizeMod = EasingFunction::QuadraticInOut(mSizeTimer, SIZE_MOD_TICKS) * cSizeAmount;
    if(cSizeMod != 0.0f) cPortraitScale = cPortraitScale + cSizeMod;

    //--Begin.
    StarBitmap *rImage = (StarBitmap *)mrAdditionalLayerList->PushIterator();
    while(rImage)
    {
        //--Compute positions.
        float tRenderX = pXCenter - (cExpectedWid * 0.50f * cPortraitScale);
        float tRenderY = pYBottom - (rImage->GetTrueHeight() * cPortraitScale);

        //--Offsets.
        if(pFlags & FLIP_HORIZONTAL)
        {
            tRenderX = tRenderX - (mActiveOffsetX * cPortraitScale);
        }
        else
        {
            tRenderX = tRenderX + (mActiveOffsetX * cPortraitScale);
        }
        tRenderY = tRenderY + (mActiveOffsetY * cPortraitScale);

        //--Render.
        rImage->DrawScaled(tRenderX, tRenderY, cPortraitScale, cPortraitScale, pFlags);

        //--Next.
        rImage = (StarBitmap *)mrAdditionalLayerList->AutoIterate();
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
}
void DialogueActor::RenderAtVN(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Overload, as below but uses the active image.
    RenderAtVN(rActiveImage, pXCenter, pYBottom, pGlobalAlpha, pFlags);
}
void DialogueActor::RenderAtVN(StarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Renders whatever the current image is at the provided position. Portraits always render
    //  bottom-up and are centered.
    //--Visual Novel portraits can use different sizing properties. These never use wide/narrow distinctions.
    if(!pUseImage) return;

    //--If this entity is darkened, modify the mixer.
    if(mIsDarkened)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pGlobalAlpha);
    }
    //--Normal color mixing.
    else
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
    }

    //--Constants.
    float cPortraitScale = 1.00f;
    float cPortraitScaleInv = 1.00f;

    //--Resolve the size.
    float cSizeMod = EasingFunction::QuadraticInOut(mSizeTimer, SIZE_MOD_TICKS) * SIZE_MOD_AMOUNT;
    if(cSizeMod != 0.0f)
    {
        cPortraitScale = cPortraitScale + cSizeMod;
        cPortraitScaleInv = 1.0f / cPortraitScale;
    }

    //--Compute position.
    float tRenderX = pXCenter - (pUseImage->GetTrueWidth() * 0.50f * cPortraitScale);
    float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale);

    //--Render.
    glTranslatef(tRenderX, tRenderY, 0.0f);
    glScalef(cPortraitScale, cPortraitScale, 1.0f);
    pUseImage->Draw(0, 0, pFlags);
    glScalef(cPortraitScaleInv, cPortraitScaleInv, 1.0f);
    glTranslatef(-tRenderX, -tRenderY, 0.0f);

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void DialogueActor::HookToLuaState(lua_State *pLuaState)
{
    /* DialogueActor_Create(sName)
       Creates and pushes a new DialogueActor, also registering it to the WorldDialogue bench.
       Remember to pop it when you're done setup. */
    lua_register(pLuaState, "DialogueActor_Create", &Hook_DialogueActor_Create);

    /* DialogueActor_Exists(sName) (1 boolean)
       Returns whether or not the named dialogue actor actually exists. */
    lua_register(pLuaState, "DialogueActor_Exists", &Hook_DialogueActor_Exists);

    /* DialogueActor_Push(sName)
       Pushes the named DialogueActor onto the Activity Stack. */
    lua_register(pLuaState, "DialogueActor_Push", &Hook_DialogueActor_Push);

    /* DialogueActor_GetProperty("Portrait Path", sEmotionName) (1 String)
       Returns the requested property in the active DialogueActor. */
    lua_register(pLuaState, "DialogueActor_GetProperty", &Hook_DialogueActor_GetProperty);

    /* DialogueActor_SetProperty("Add Alias", sAlias)
       DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath)
       DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath, bIsNotWide)
       DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath)
       DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath, bIsNotWide)
       DialogueActor_SetProperty("Remove Alias", sAlias)
       Sets the requested property in the active DialogueActor. */
    lua_register(pLuaState, "DialogueActor_SetProperty", &Hook_DialogueActor_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_DialogueActor_Create(lua_State *L)
{
    //DialogueActor_Create(sName)
    int tArgs = lua_gettop(L);

    //--Push.
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    if(tArgs < 1) return LuaArgError("DialogueActor_Create");

    //--If the actor already exists, push the actor instead.
    DialogueActor *rCheckActor = WorldDialogue::Fetch()->GetActor(lua_tostring(L, 1));
    if(rCheckActor)
    {
        DataLibrary::Fetch()->rActiveObject = rCheckActor;
        return 0;
    }

    //--Create the Actor, basic setup.
    DialogueActor *nActor = new DialogueActor();
    nActor->SetName(lua_tostring(L, 1));

    //--Register it to the WorldDialogue.
    WorldDialogue::Fetch()->RegisterDialogueActor(nActor);
    DataLibrary::Fetch()->rActiveObject = nActor;
    return 0;
}
int Hook_DialogueActor_Exists(lua_State *L)
{
    //DialogueActor_Exists(sName) (1 boolean)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("DialogueActor_Exists");
        lua_pushboolean(L, false);
        return 1;
    }

    //--Check and return.
    void *rTestPtr = WorldDialogue::Fetch()->GetActor(lua_tostring(L, 1));
    lua_pushboolean(L, (rTestPtr != NULL));
    return 1;
}
#include "LuaManager.h"
int Hook_DialogueActor_Push(lua_State *L)
{
    //DialogueActor_Push(sName)
    int tArgs = lua_gettop(L);

    //--Push.
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    if(tArgs < 1) return LuaArgError("DialogueActor_Push");

    //--Create the Actor, basic setup.
    DataLibrary::Fetch()->rActiveObject = WorldDialogue::Fetch()->GetActor(lua_tostring(L, 1));
    if(!DataLibrary::Fetch()->rActiveObject)
    {
        fprintf(stderr, "%s: Failed, actor %s was not found. %s\n", "DialogueActor_Push", lua_tostring(L, 1), LuaManager::Fetch()->GetCallStack(0));
    }
    return 0;
}
int Hook_DialogueActor_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //DialogueActor_GetProperty("Portrait Path", sEmotionName) (1 String)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DialogueActor_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_DIALOGUEACTOR)) return LuaTypeError("DialogueActor_GetProperty", L);
    DialogueActor *rDialogueActor = (DialogueActor *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Returns the path to the named emotion in the datalibrary.
    if(!strcasecmp(rSwitchType, "Portrait Path") && tArgs == 2)
    {
        const char *rResult = rDialogueActor->GetPathOfEmotion(lua_tostring(L, 2));
        if(!rResult)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rResult);
        }
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("DialogueActor_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_DialogueActor_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Aliases]
    //DialogueActor_SetProperty("Add Alias", sAlias)
    //DialogueActor_SetProperty("Add Alias", sAlias, sDisplayName)
    //DialogueActor_SetProperty("Remove Alias", sAlias)

    ///--[Emotions]
    //DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath)
    //DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath, bIsNotWide)
    //DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath)
    //DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath, bIsNotWide)
    //DialogueActor_SetProperty("Add Offset", sEmotionName, fOffsetX, fOffsetY)

    ///--[ ========= Argument Listing ========= ]
    //--Verify arguments.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DialogueActor_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    DialogueActor *rDialogueActor = (DialogueActor *)rDataLibrary->rActiveObject;
    if(!rDialogueActor || !rDialogueActor->IsOfType(POINTER_TYPE_DIALOGUEACTOR)) return LuaTypeError("DialogueActor_SetProperty", L);

    ///--[Aliases]
    //--Add a new alias, using "DEFAULT" as the display name. Used for non-translated versions.
    if(!strcasecmp(rSwitchType, "Add Alias") && tArgs == 2)
    {
        rDialogueActor->AddAlias(lua_tostring(L, 2), "DEFAULT");
    }
    //--Add a new alias, with a display name. Typically used for translations.
    else if(!strcasecmp(rSwitchType, "Add Alias") && tArgs >= 2)
    {
        rDialogueActor->AddAlias(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Removes an alias.
    else if(!strcasecmp(rSwitchType, "Remove Alias") && tArgs == 2)
    {
        rDialogueActor->RemoveAlias(lua_tostring(L, 2));
    }
    ///--[Emotions]
    //--Add a new emotion.
    else if(!strcasecmp(rSwitchType, "Add Emotion") && tArgs == 3)
    {
        rDialogueActor->AddPortrait(lua_tostring(L, 2), lua_tostring(L, 3), false);
    }
    //--Add a new emotion with the HD flag set.
    else if(!strcasecmp(rSwitchType, "Add Emotion") && tArgs == 4)
    {
        rDialogueActor->AddPortrait(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4));
    }
    //--Add a new emotion for the reverse case.
    else if(!strcasecmp(rSwitchType, "Add Emotion Rev") && tArgs == 3)
    {
        rDialogueActor->AddPortraitRev(lua_tostring(L, 2), lua_tostring(L, 3), false);
    }
    //--Add a new emotion with the HD flag set for the reverse case.
    else if(!strcasecmp(rSwitchType, "Add Emotion Rev") && tArgs == 4)
    {
        rDialogueActor->AddPortraitRev(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4));
    }
    //--Specifies manual offsets for an image.
    else if(!strcasecmp(rSwitchType, "Add Offset") && tArgs == 4)
    {
        rDialogueActor->AddOffset(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("DialogueActor_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
