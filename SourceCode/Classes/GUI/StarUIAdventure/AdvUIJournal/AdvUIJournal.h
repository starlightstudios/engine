///======================================= AdvUIJournal ===========================================
//--UI object that encapsulates the journal submenu.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIJournalTypes.h"
#include "AdvUIJournalAchieveTypes.h"

#ifdef _TARGET_OS_WINDOWS_
    #ifndef _STEAM_API_
        #define constexpr const
    #endif
#endif
#ifdef _TARGET_OS_MAC_
#define constexpr const
#endif

///===================================== Local Structures =========================================
#ifndef ADVMENU_JOURNAL_DEFINITIONS
#define ADVMENU_JOURNAL_DEFINITIONS
typedef struct AdvMenuJournalSteamNameRemap
{
    char *mSteamName;
    char *mProgramName;
}AdvMenuJournalSteamNameRemap;

///===================================== Local Definitions ========================================
//--Timers
#define ADVMENU_JOURNAL_VIS_TICKS 15
#define ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS 15
#define ADVMENU_JOURNAL_CURSOR_TICKS 7

//--Sizes
#define ADVMENU_JOURNAL_ENTRIES_ON_LIST 29

//--Modes
#define ADVMENU_JOURNAL_MODE_BESTIARY 0
#define ADVMENU_JOURNAL_MODE_PROFILES 1
#define ADVMENU_JOURNAL_MODE_QUESTS 2
#define ADVMENU_JOURNAL_MODE_LOCATIONS 3
#define ADVMENU_JOURNAL_MODE_PARAGONS 4
#define ADVMENU_JOURNAL_MODE_COMBATGLOSSARY 5
#define ADVMENU_JOURNAL_MODE_TOTAL 6

#endif // ADVMENU_JOURNAL_DEFINITIONS

///========================================== Classes =============================================
class AdvUIJournal : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    //--Position.
    static constexpr float cxSelectionLft =  47.0f;
    static constexpr float cxSelectionTop = 112.0f;
    static constexpr float cxSelectionHei =  20.0f;

    //--Scrollbars.
    static const int cxScrollbarBuf = 3;
    static const int cxScrollbarListPage = 29;
    static const int cxScrollbarAchBuf = 1;
    static const int cxScrollbarAchListPage = 7;

    ///--[Variables]
    //--System
    int mCurrentMode;
    bool mIsAchievementsMode;

    //--Main Menu
    StarlightString *mMainMenuLftString;
    StarlightString *mMainMenuRgtString;

    //--Bestiary
    int mBestiaryOffset;
    int mBestiaryCursor;
    bool mShowFullBestiaryImage;
    bool mShowParagon;
    int mBestiaryImageTimer;
    StarLinkedList *mBestiaryEntries; //AdvMenuJournalBestiaryEntry *, master

    //--Profiles
    int mProfileOffset;
    int mProfileCursor;
    StarLinkedList *mProfileEntries; //AdvMenuJournalProfileEntry *, master

    //--Quests
    int mQuestOffset;
    int mQuestCursor;
    StarLinkedList *mQuestEntries; //AdvMenuJournalQuestEntry *, master

    //--Locations
    int mLocationOffset;
    int mLocationCursor;
    StarLinkedList *mLocationEntries; //AdvMenuJournalLocationEntry *, master

    //--Paragons
    int mParagonOffset;
    int mParagonCursor;
    StarLinkedList *mParagonEntries; //AdvMenuParagonEntry *, master

    //--Combat Glossary
    int mCombatOffset;
    int mCombatCursor;
    StarLinkedList *mCombatEntries; //AdvMenuJournalCombatGlossaryEntry *, master

    //--Achievements
    bool mHideSpoilers;
    EasingPack1D mAchievementScroll;
    int mAchievementCategoryOffset;
    int mAchievementCategoryCursor;
    int mAchievementEntryOffset;
    int mAchievementEntryCursor;

    //--Steam
    static int xSteamNameRemapsTotal;
    static AdvMenuJournalSteamNameRemap *xSteamNameRemaps;

    //--Common
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Help Strings
    StarlightString *mShowAchievementsString;
    StarlightString *mHideAchievementsString;
    StarlightString *mScrollAchievementString;
    StarlightString *mToggleSpoilerString;
    StarlightString *mShowFullPortraitString;
    StarlightString *mToggleParagonString;
    StarlightString *mEnableParagonString;
    StarlightString *mReturnString;
    StarlightString *mMove10String;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_BigHeader;
        StarFont *rFont_Header;
        StarFont *rFont_Mainline;
        StarFont *rFont_Stats;
        StarFont *rFont_AchievementTitle;
        StarFont *rFont_AchievementDescription;

        //--Universal
        StarBitmap *rStaticPartsAchievements;
        StarBitmap *rStaticPartsAchievementsOver;
        StarBitmap *rStaticParts;
        StarBitmap *rStaticPartsParagons;
        StarBitmap *rScrollbarBack;
        StarBitmap *rScrollbarFront;

        //--Achievements
        StarBitmap *rAchievementIconLocked;
        StarBitmap *rAchievementCheckNo;
        StarBitmap *rAchievementCheckYes;
        StarBitmap *rAchievementBlock;
        StarBitmap *rAchievementScrollbarBack;

        //--Bestiary
        StarBitmap *rBestiaryIconsL;
        StarBitmap *rBestiaryIconsR;
        StarBitmap *rBestiaryIconsRMonoceros;
        StarBitmap *rBestiaryNameBanner;
        StarBitmap *rBestiaryPortraitMask;
        StarBitmap *rParagonSticker;

        //--Profiles
        StarBitmap *rProfilesNameBanner;

        //--Misc.
        StarBitmap *rOverlay_Highlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIJournal();
    virtual ~AdvUIJournal();
    virtual void Construct();

    //--Public Variables
    static bool xShowAllEntries;
    static StarLinkedList *xAchievementCategories; //AdvMenuJournalAchievementCategory *, master
    static char *xAchievementOverPath;
    static char *xAchievementTitle;
    static char *xAchievementShowString;
    static char *xAchievementHideString;
    static char *xBestiaryResolveScript;
    static char *xProfileResolveScript;
    static char *xQuestResolveScript;
    static char *xLocationResolveScript;
    static char *xParagonResolveScript;
    static char *xCombatResolveScript;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void HandleJournalModeChange();

    ///--[Achievements]
    void SetToAchievementsMode();
    AdvMenuJournalAchievementEntry *LocateAchievement(const char *pName);
    virtual float ComputeEntryHeight(AdvMenuJournalAchievementEntry *pEntry);
    void SetAchievementOverGraphic(const char *pPath);
    void RegisterAchievementCategory(const char *pName, const char *pDisplayName, const char *pIconImg);
    void RegisterAchievement(const char *pCategory, const char *pName, const char *pDisplayName, const char *pImgPath, const char *pDescription);
    void AllocateAchievementRequirements(const char *pName, int pAmount);
    void SetAchievementRequirements(const char *pName, int pSlot, const char *pReqName, float pValue, bool pDisplayAsInt, bool pDisplayAsCheck);
    void SetAchievementProgress(const char *pName, int pSlot, float pValue);
    void SetAchievementLocked(const char *pName, bool pIsLocked);
    void SetAchievementSpoilered(const char *pName, bool pIsSpoilered);
    void UnlockAchievement(const char *pName);
    void AllocateAchievementRemaps(int pTotal);
    void SetAchievementRemap(int pSlot, const char *pProgramName, const char *pSteamName);
    void CrossReferenceAchievements();
    virtual void RecomputeJournalAchievementCursorPos();
    virtual void UpdateJournalAchievements();
    virtual void RenderJournalAchievements();
    virtual void RenderJournalAchievement(float pYPos, AdvMenuJournalAchievementEntry *pEntry, float pAlpha);
    void WriteAchievementsToFile();
    void ReadAchievementsFromFile();

    ///--[Bestiary]
    void SetJournalToBestiaryMode();
    void CreateBestiaryEntry(const char *pInternalName);
    void SetBestiaryDisplayName(const char *pInternalName, const char *pDisplayName);
    void SetBestiaryImage(const char *pInternalName, const char *pDLPath, float pX, float pY);
    void SetBestiaryParagon(const char *pInternalName, const char *pDLPath, float pX, float pY);
    void SetBestiaryTurnOrderIco(const char *pInternalName, const char *pDLPath);
    void SetBestiaryKOCount(const char *pInternalName, int pCount);
    void SetBestiaryParagonKOCount(const char *pInternalName, int pCount);
    void SetBestiaryKOMax(const char *pInternalName, int pCount);
    void SetBestiaryCanTransformYou(const char *pInternalName, bool pFlag);
    void SetBestiaryLevel(const char *pInternalName, int pLevel);
    void SetBestiaryStatistics(const char *pInternalName, int pHP, int pAtk, int pIni, int pAcc, int pEvd, int pPrt);
    void SetBestiaryResults(const char *pInternalName, int pExp, int pPlatina);
    void SetBestiaryResistances(const char *pInternalName, int pSlash, int pStrike, int pPierce, int pFlame, int pFreeze, int pShock, int pCrusade, int pObscure, int pBleed, int pPoison, int pCorrode, int pTerrify);
    void AllocateBestiaryDescriptionLines(const char *pInternalName, int pCount);
    void SetBestiaryDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine);
    virtual void SetBestiaryDescriptionAuto(const char *pInternalName, const char *pDescription);
    void ClearBestiary();
    virtual void ComputeBestiaryCursorPosition();
    virtual void RenderBestiaryMode(float pVisAlpha);

    ///--[Combat]
    virtual void SetJournalToCombatMode();
    void CreateCombatGlossaryEntry(const char *pInternalName, const char *pDisplayName);
    void AllocateCombatGlossaryPieces(const char *pInternalName, int pTotal);
    void AllocateCombatGlossaryText(const char *pInternalName, int pSlot, int pEntries, float pX, float pY);
    virtual void SetCombatGlossaryText(const char *pInternalName, int pSlot, int pLine, const char *pText, const char *pFont, int pFlags);
    virtual void SetCombatGlossaryAuto(const char *pInternalName, int pSlot, int pX, int pY, const char *pText, const char *pFont, int pFlags);
    void SetCombatGlossaryImage(const char *pInternalName, int pSlot, const char *pDLPath, float pX, float pY);
    void ClearCombatGlossary();
    virtual void ComputeCombatCursorPosition();
    virtual void RenderCombatMode(float pVisAlpha);

    ///--[Locations]
    void SetJournalToLocationsMode();
    void CreateLocationEntry(const char *pInternalName);
    void SetLocationDisplayName(const char *pInternalName, const char *pDisplayName);
    void SetLocationImage(const char *pInternalName, const char *pDLPath);
    void SetLocationImageOffsets(const char *pInternalName, float pX, float pY);
    void AllocateLocationDescriptionLines(const char *pInternalName, int pCount);
    void SetLocationDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine);
    virtual void SetLocationDescriptionAuto(const char *pInternalName, const char *pDescription);
    void ClearLocations();
    virtual void ComputeLocationCursorPosition();
    virtual void RenderLocationsMode(float pVisAlpha);

    ///--[Paragons]
    void SetJournalToParagonsMode();
    void CreateParagonEntry(const char *pInternalName);
    void SetParagonDisplayName(const char *pInternalName, const char *pDisplayName);
    void SetParagonIsEnabled(const char *pInternalName, bool pIsEnabled);
    void SetParagonKOCounts(const char *pInternalName, int pKOCurrent, int pKONeeded);
    void SetParagonHasDefeatedActivator(const char *pInternalName, bool pDefeatedActivator);
    void SetParagonEnabledPath(const char *pInternalName, const char *pPath);
    void ClearParagons();
    virtual void ComputeParagonCursorPosition();
    virtual void RenderParagonsMode(float pVisAlpha);

    ///--[Profiles]
    void SetJournalToProfilesMode();
    void CreateProfileEntry(const char *pInternalName);
    void SetProfileDisplayName(const char *pInternalName, const char *pDisplayName);
    void SetProfilePortrait(const char *pInternalName, const char *pDLPath);
    void SetProfileOffsets(const char *pInternalName, float pX, float pY);
    void AllocateProfileDescriptionLines(const char *pInternalName, int pCount);
    void SetProfileDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine);
    void SetProfileDescriptionAuto(const char *pInternalName, const char *pDescriptionLine);
    void ClearProfiles();
    virtual void ComputeProfileCursorPosition();
    virtual void RenderProfilesMode(float pVisAlpha);

    ///--[Quests]
    void SetJournalToQuestsMode();
    void CreateQuestEntry(const char *pInternalName);
    void SetQuestDisplayName(const char *pInternalName, const char *pDisplayName);
    void AllocateQuestDescriptionLines(const char *pInternalName, int pCount);
    void SetQuestDescriptionLine(const char *pInternalName, int pLine, const char *pDescriptionLine);
    void SetQuestObjective(const char *pInternalName, const char *pQuestObjective);
    virtual void SetQuestDescriptionAuto(const char *pInternalName, const char *pDescription);
    void ClearQuests();
    virtual void ComputeQuestCursorPosition();
    virtual void RenderQuestsMode(float pVisAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderMenuOption(float pX, float pY, StarFont *pFont, float pAlpha, int pSlot, int pMenuSlot, const char *pText);
    virtual void RenderJournalEntryList(StarLinkedList *pList, int pStartAt, int pHighlighted, float pAlpha);
    virtual void RenderResistance(float pX, float pY, int pFlags, float pScale, int pResist, StarFont *pFont);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AM_GetPropertyJournal(lua_State *L);
int Hook_AM_SetPropertyJournal(lua_State *L);
