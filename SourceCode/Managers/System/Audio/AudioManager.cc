//--Base
#include "AudioManager.h"

//--Classes
#include "AudioPackage.h"
#include "AudioPackageSample.h"
#include "AudioPackageStream.h"

//--CoreClasses
#include "StarLoadInterrupt.h"
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "OptionsManager.h"

///========================================== System ==============================================
AudioManager::AudioManager()
{
    ///--[Variable Initialization]
    //--[AudioManager]
    //--System
    //--Storage Lists
    mMusicList = new StarLinkedList(true);
    mSoundList = new StarLinkedList(true);
    mRandomSoundList = new StarLinkedList(true);

    //--Music controllers
    rCurrentMusic = NULL;

    //--Voice controllers
    mCurrentSpeech = NULL;

    //--Delayed Sounds
    mDelayedSoundList = new StarLinkedList(true);

    //--Effect Profiles
    mEffectProfiles = new StarLinkedList(true);

    //--FMOD only
    #if defined _FMOD_AUDIO_
    #endif

    //--Public Variables
    rLastPackage = NULL;
    for(int i = 0; i < AM_ABSTRACT_VOLUME_TOTAL; i ++)
    {
        xAbstractVolume[i] = AM_DEFAULT_ABSTRACT_VOLUME;
    }

    ///--[Construction]
    //--Boot the sound system.
    if(!xBlockAllAudio)
    {
        ///--[Setup]
        bool tBootedSuccessfully = false;

        ///--[Bass]
        #if defined _BASS_AUDIO_

            ///--[OSX]
            #ifdef _TARGET_OS_MAC_
                //--Set the device list to contain a "Default".
                BASS_SetConfig(BASS_CONFIG_DEV_DEFAULT, true);

                //--Initialize.
                tBootedSuccessfully = BASS_Init(1, 44100, 0, NULL, NULL);

            ///--[Linux]
            #elif defined _TARGET_OS_LINUX_
                tBootedSuccessfully = BASS_Init(-1, 44100, 0, NULL, NULL);

            ///--[Windows]
            #else

                //--Set the device list to contain a "Default".
                BASS_SetConfig(BASS_CONFIG_DEV_DEFAULT, true);

                //--Enumerate all sound devices.
                if(false)
                {
                    //--Setup.
                    int tDevice = 1;
                    BASS_DEVICEINFO tInfoStruct;

                    //--Iterate.
                    fprintf(stderr, "Scanning audio devices:\n");
                    while(true)
                    {
                        //--Get the info.
                        bool tDeviceExists = BASS_GetDeviceInfo(tDevice, &tInfoStruct);
                        if(!tDeviceExists) break;

                        //--Device enabled:
                        fprintf(stderr, " Device %i:\n", tDevice);
                        if(tInfoStruct.flags & BASS_DEVICE_ENABLED)
                        {
                            fprintf(stderr, "  Enabled: True\n");
                        }
                        else
                        {
                            fprintf(stderr, "  Enabled: False\n");
                        }

                        //--Device is default:
                        if(tInfoStruct.flags & BASS_DEVICE_DEFAULT)
                        {
                            fprintf(stderr, "  Default: True\n");
                        }
                        else
                        {
                            fprintf(stderr, "  Default: False\n");
                        }

                        //--Next.
                        tDevice ++;
                    }
                }

                //--Initialize.
                tBootedSuccessfully = BASS_Init(1, 44100, 0, FindWindow(NULL, "Starlight Engine"), NULL);
                BASS_FX_GetVersion();

                //--Debug report.
                //int tVersion = BASS_FX_GetVersion();
                //fprintf(stderr, "BASS FX VERSION: %i\n", tVersion);
            #endif

        ///--[FMOD]
        #elif defined _FMOD_AUDIO_

            //--Initialize FMOD stuff.
            FMOD_System_Create(&xFMODSystem);
            FMOD_System_Init(xFMODSystem, 1024, FMOD_INIT_NORMAL, xExtraDriverData);

            //--Should be a successful boot.
            tBootedSuccessfully = true;

        #endif

        ///--[Error Handling]
        //--Error output
        if(!tBootedSuccessfully)
        {
            fprintf(stderr, "AudioManager:  Error booting sound system!\n");
            xBlockAllAudio = true;
        }
    }
}
AudioManager::~AudioManager()
{
    StopAllMusic();
    StopAllSound();
    delete mMusicList;
    delete mSoundList;
    delete mRandomSoundList;
    delete mCurrentSpeech;
    delete mDelayedSoundList;
    delete mEffectProfiles;

    //--Clean up the audio from FMOD.
    #if defined _FMOD_AUDIO_
        FMOD_System_Close(xFMODSystem);
        FMOD_System_Release(xFMODSystem);
    #endif
}

///--[Public Statics]
bool AudioManager::xBlockAllAudio = false;
bool AudioManager::xBlockSound = false;
bool AudioManager::xBlockMusic = false;
float AudioManager::xMusicVolume = AM_DEFAULT_MUSIC_VOLUME;
float AudioManager::xSoundVolume = AM_DEFAULT_SOUND_VOLUME;
float AudioManager::xAbstractVolume[AM_ABSTRACT_VOLUME_TOTAL];
float AudioManager::xMasterVolume = 1.00f;

///--[FMOD Statics]
//--These only exist when using FMOD for Audio.
#if defined _FMOD_AUDIO_
    FMOD_SYSTEM *AudioManager::xFMODSystem = NULL;
    uint32_t AudioManager::xFMODVersion = 0;
    void *AudioManager::xExtraDriverData = NULL;
#endif

///===================================== Property Queries =========================================
bool AudioManager::IsMusicPlaying()
{
    return (rCurrentMusic != NULL);
}
bool AudioManager::MusicExists(const char *pName)
{
    //--Returns true if a music of the given name exists.
    return (mMusicList->GetSlotOfElementByName(pName) != -1);
}
bool AudioManager::SoundExists(const char *pName)
{
    //--Returns true if a sound of the given name exists.
    return (mSoundList->GetSlotOfElementByName(pName) != -1);
}
const char *AudioManager::GetNameOfMusicPack(void *pPtr)
{
    int tSlot = mMusicList->GetSlotOfElementByPtr(pPtr);
    if(tSlot == -1) return NULL;
    return mMusicList->GetNameOfElementBySlot(tSlot);
}
const char *AudioManager::GetNameOfPlayingMusic()
{
    if(!rCurrentMusic) return NULL;
    return GetNameOfMusicPack(rCurrentMusic);
}
float AudioManager::GetLengthOfSound(const char *pName)
{
    //--Gets the length of the requested sound, in seconds.
    AudioPackage *rPackage = (AudioPackage *)mSoundList->GetElementByName(pName);
    if(!rPackage) return 0.0f;
    return rPackage->GetDuration();
}

///======================================== Manipulators ==========================================
void AudioManager::RegisterMusic(const char *pName, AudioPackage *pMusicPack)
{
    //--Registers the AudioPackage as a piece of music.  All musics are mutually exclusive, playing
    //  any one will shut off all the others, barring the use of cross fading.
    if(!pName || !pMusicPack) return;
    mMusicList->AddElement(pName, pMusicPack, &AudioPackage::DeleteThis);
    rLastPackage = pMusicPack;
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
}
void AudioManager::RegisterSound(const char *pName, AudioPackage *pSoundPack)
{
    //--Registers the AudioPackage as a sound.  Sounds can play concurrently and do not support
    //  features like crossfading or looping.
    if(!pName || !pSoundPack) return;
    mSoundList->AddElement(pName, pSoundPack, &AudioPackage::DeleteThis);
    rLastPackage = pSoundPack;
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
}
void AudioManager::RegisterRandomSound(const char *pName, int pMaxSounds)
{
    //--Adds a RandomSound pack, which is just an integer. This can allow a sound to be picked at
    //  random instead of always playing the same one.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    int *nPtr = (int *)starmemoryalloc(sizeof(int));
    *nPtr = pMaxSounds;
    mRandomSoundList->AddElement(pName, nPtr, &FreeThis);
    if(StarBitmap::xInterruptCall) StarBitmap::xInterruptCall();
}
void AudioManager::UnregisterMusic(const char *pName)
{
    while(mMusicList->RemoveElementS(pName))
    {

    }
    rLastPackage = NULL;
}
void AudioManager::UnregisterSound(const char *pName)
{
    while(mSoundList->RemoveElementS(pName))
    {

    }
    rLastPackage = NULL;
}
void AudioManager::ClearLastPackage()
{
    rLastPackage = NULL;
}
void AudioManager::ChangeMusicVolume(float pDelta)
{
    //--Change volume.
    xMusicVolume = xMusicVolume + pDelta;

    //--Handle updating all packages. This also clamps the volume.
    ChangeMusicVolumeTo(xMusicVolume);
}
void AudioManager::ChangeSoundVolume(float pDelta)
{
    //--Change volume.
    xSoundVolume = xSoundVolume + pDelta;

    //--Handle updating all packages. This also clamps the volume.
    ChangeSoundVolumeTo(xSoundVolume);
}
void AudioManager::ChangeAbstractVolume(int pSlot, float pDelta)
{
    //--Check slot.
    if(pSlot < 0 || pSlot >= AM_ABSTRACT_VOLUME_TOTAL) return;

    //--Change.
    xAbstractVolume[pSlot] = xAbstractVolume[pSlot] + pDelta;

    //--Handle updating all packages. This also clamps the volume.
    ChangeAbstractVolumeTo(pSlot, xAbstractVolume[pSlot]);
}
void AudioManager::ChangeMusicVolumeTo(float pValue)
{
    //--Change volume.
    if(pValue > 1.0f) pValue = 1.0f;
    if(pValue < 0.0f) pValue = 0.0f;
    xMusicVolume = pValue;

    //--Inform all sounds of the change.
    AudioPackage *rAudioPackage = (AudioPackage *)mMusicList->PushIterator();
    while(rAudioPackage)
    {
        rAudioPackage->SetGlobalVolume(xMusicVolume);
        rAudioPackage = (AudioPackage *)mMusicList->AutoIterate();
    }

    //--Inform the OptionsManager.
    OptionsManager::Fetch()->SetOptionI("MusicVolume", (int)(xMusicVolume * 100.0f));
}
void AudioManager::ChangeSoundVolumeTo(float pValue)
{
    //--Change volume.
    if(pValue > 1.0f) pValue = 1.0f;
    if(pValue < 0.0f) pValue = 0.0f;
    xSoundVolume = pValue;

    //--Inform all sounds of the change.
    AudioPackage *rAudioPackage = (AudioPackage *)mSoundList->PushIterator();
    while(rAudioPackage)
    {
        //--Must be using the base sound volume, which is abstract slot -1.
        if(rAudioPackage->GetAbstractSlot() == -1)
        {
            rAudioPackage->SetGlobalVolume(xSoundVolume);
        }
        rAudioPackage = (AudioPackage *)mSoundList->AutoIterate();
    }

    //--Inform the OptionsManager.
    OptionsManager::Fetch()->SetOptionI("SoundVolume", (int)(xSoundVolume * 100.0f));
}
void AudioManager::ChangeAbstractVolumeTo(int pSlot, float pValue)
{
    //--Check slot.
    if(pSlot < 0 || pSlot >= AM_ABSTRACT_VOLUME_TOTAL) return;

    //--Change.
    if(pValue > 1.0f) pValue = 1.0f;
    if(pValue < 0.0f) pValue = 0.0f;
    xAbstractVolume[pSlot] = pValue;

    //--Inform all sounds of the change that are using the same abstract slot.
    AudioPackage *rAudioPackage = (AudioPackage *)mSoundList->PushIterator();
    while(rAudioPackage)
    {
        //--Must be using no other volume.
        if(rAudioPackage->GetAbstractSlot() == pSlot)
        {
            rAudioPackage->SetGlobalVolume(xAbstractVolume[pSlot]);
        }

        //--Next.
        rAudioPackage = (AudioPackage *)mSoundList->AutoIterate();
    }

    //--Inform the OptionsManager.
    char tBuf[128];
    sprintf(tBuf, "AbstractVolume%02i", pSlot);
    OptionsManager::Fetch()->SetOptionI(tBuf, (int)(xAbstractVolume[pSlot] * 100.0f));
}
void AudioManager::SeekMusicTo(float pSeconds)
{
    if(!rCurrentMusic) return;
    rCurrentMusic->SeekTo(pSeconds);
}
void AudioManager::AdvanceMusicToLoopPoint()
{
    //--Debugging tool. Advances the music to 2 seconds before its loop point. Useful for diagnosing
    //  looping time errors.
    //--Does nothing if no music is playing, obviously. Also does nothing if it's not looping.
    if(!rCurrentMusic || !rCurrentMusic->IsLooping()) return;

    //--Get the loop time and subtract 2 seconds. Clamp if needed.
    float tLoopPoint = rCurrentMusic->GetEndLoopTime() - 2.0f;
    if(tLoopPoint < 0.0f) tLoopPoint = 0.0f;

    //--Set.
    rCurrentMusic->SeekTo(tLoopPoint);
}
void AudioManager::ModifyLoopPointForward()
{
    //--Debugging tool. Moves the loop point on the current music forward by 0.010 seconds, and prints
    //  the loop time to the console so it can be edited in the Lua files.
    //--Does nothing if no music is playing, obviously. Also does nothing if it's not looping. Duh.
    if(!rCurrentMusic || !rCurrentMusic->IsLooping()) return;
    float tStartPoint = rCurrentMusic->GetStartLoopTime();
    float tEndPoint = rCurrentMusic->GetEndLoopTime();
    rCurrentMusic->AddLoop(tStartPoint, tEndPoint + 0.010f);
    fprintf(stderr, "Loop positions set to %f %f\n", tStartPoint, tEndPoint + 0.010f);
}
void AudioManager::ModifyLoopPointBackward()
{
    //--Debugging tool. Moves the loop point on the current music backward by 0.010 seconds, and prints
    //  the loop time to the console so it can be edited in the Lua files.
    //--Does nothing if no music is playing, obviously. Also does nothing if it's not looping. Duh.
    if(!rCurrentMusic || !rCurrentMusic->IsLooping()) return;
    float tStartPoint = rCurrentMusic->GetStartLoopTime();
    float tEndPoint = rCurrentMusic->GetEndLoopTime();
    rCurrentMusic->AddLoop(tStartPoint, tEndPoint - 0.010f);
    fprintf(stderr, "Loop positions set to %f %f\n", tStartPoint, tEndPoint - 0.010f);
}

///======================================== Core Methods ==========================================
void AudioManager::PlayMusic(const char *pName)
{
    //--Plays music.  Only one at a time.
    //fprintf(stderr, "Attempting to play music: %s\n", pName);
    if(xBlockMusic)
    {
        //fprintf(stderr, "Music is blocked. Stopping.\n");
        return;
    }

    //--Special case:  Music was NULL, so just fade the old one out.
    if(!pName || !strcmp(pName, "NULL"))
    {
        //fprintf(stderr, "Music was NULL. Stopping.\n");
        if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);
        rCurrentMusic = NULL;
        return;
    }

    //--Locate the new music.
    AudioPackage *rPack = (AudioPackage *)mMusicList->GetElementByName(pName);

    //--Was it the same as the old music?  Do nothing.
    if(rPack == rCurrentMusic)
    {
        //--Pack did not resolve:
        if(!rPack)
        {
            //fprintf(stderr, "Music pack failed to resolve, and no music track was currently playing.\n");
        }
        //--Same packs, both resolved.
        else
        {
            //fprintf(stderr, "Music was identical to current music, doing nothing. %p to %p\n", rPack, rCurrentMusic);
        }
        return;
    }

    //--Order the old music to stop.
    if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);

    //--Play the new stuff.
    if(rPack)
    {
        //fprintf(stderr, "Playing music track %s.\n", pName);
        rPack->Play();
        rPack->FadeIn(120);
        rPack->SetLoop(true);
    }
    else
    {
        //fprintf(stderr, "Music track %s not found.\n", pName);
    }

    //--Become the new current music.
    rCurrentMusic = rPack;
}
void AudioManager::PlayMusicStartingAt(const char *pName, float pStartPoint)
{
    //--Plays the music starting at the requested position. It will still crossfade if needed.
    PlayMusic(pName);

    //--If the current music exists, change its position.
    if(rCurrentMusic)
    {
        rCurrentMusic->SeekTo(pStartPoint);
    }
}
void AudioManager::PlayMusicNoFade(const char *pName)
{
    //--Identical to PlayMusic, but doesn't fade the music track in.
    if(xBlockMusic) return;

    //--Special case:  Music was NULL, so just fade the old one out.
    if(!pName || !strcmp(pName, "NULL"))
    {
        if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);
        rCurrentMusic = NULL;
        return;
    }

    //--Locate the new music.
    AudioPackage *rPack = (AudioPackage *)mMusicList->GetElementByName(pName);

    //--Was it the same as the old music?  Do nothing.
    if(rPack == rCurrentMusic) return;

    //--Order the old music to stop.
    if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);

    //--Play the new stuff.
    if(rPack)
    {
        rPack->Play();
        rPack->SetVolume(xMusicVolume + 0.40f);
        rPack->SetLoop(true);
    }

    //--Become the new current music.
    rCurrentMusic = rPack;
}
void AudioManager::FadeMusic(int pTickDuration)
{
    //--Orders the music to fade out over the requested number of ticks.  The music is still
    //  'playing' in memory until the next stop/play call, and can be faded back in.
    //--Pass a negative tick value to cause the music to cease being the active music. It will
    //  fade out using the absolute value of the ticks. Passing 0 is the same as StopAllMusic().
    if(xBlockMusic || !rCurrentMusic) return;

    //--Normal case: Music fades out but stays in memory.
    if(pTickDuration > 0)
    {
        rCurrentMusic->FadeOutToStop(pTickDuration);
    }
    //--Zero case: Music stops immediately. Remains in memory.
    else if(pTickDuration == 0)
    {
        rCurrentMusic->FadeOut(0);
    }
    //--Negative case: Fade out, ceases being in memory.
    else
    {
        rCurrentMusic->FadeOutToStop(pTickDuration * -1);
        rCurrentMusic = NULL;
    }
}
void AudioManager::StopAllMusic()
{
    //--Order all music to stop playing.
    if(xBlockMusic) return;

    AudioPackage *rPack = (AudioPackage *)mMusicList->PushIterator();
    while(rPack)
    {
        rPack->Stop();
        rPack = (AudioPackage *)mMusicList->AutoIterate();
    }
    rCurrentMusic = NULL;
}
void AudioManager::PlaySound(const char *pName)
{
    //--Unlike PlayMusic, there is no exclusivity here.
    if(xBlockSound) return;
    AudioPackage *rPack = (AudioPackage *)mSoundList->GetElementByName(pName);
    if(!rPack) return;

    //rPack->SetGlobalVolume(xSoundVolume * xMasterVolume);
    rPack->Play();
}
void AudioManager::PlaySoundByPackage(PlaySoundPackage *pPackage)
{
    //--Given a PlaySoundPackage which may contain optional special effect flags, plays the given sound.
    if(xBlockSound || !pPackage) return;

    //--Locate the sound associated with the package.
    AudioPackage *rPack = (AudioPackage *)mSoundList->GetElementByName(pPackage->mSoundName);
    if(!rPack) return;

    //--Set special flags for the package.
    if(rPack->GetAudioPackageType() == AUDIOPACKAGE_TYPE_STREAM) ((AudioPackageStream *)(rPack))->SetEffectPackage(pPackage->mEffectName);

    //--Play the sound.
    rPack->Play();
}
void AudioManager::StopSound(const char *pName)
{
    if(xBlockSound) return;
    AudioPackage *rPack = (AudioPackage *)mSoundList->GetElementByName(pName);
    if(!rPack) return;
    rPack->Stop();
}
void AudioManager::PlayRandomSound(const char *pName, int pMaxRoll)
{
    //--Plays a random sound effect of the pattern: "%s%i", pName, tRoll.
    //  The AudioManager can store certain maximums automatically. In those cases, use the overload
    //  which doesn't pass in a number.
    char tBuffer[64];
    sprintf(tBuffer, "%s%i", pName, rand() % pMaxRoll);
    PlaySound(tBuffer);
}
void AudioManager::PlayRandomSound(const char *pName)
{
    //--This version automatically resolves how many of a given sound there are in a random list.
    int *rMaxAmount = (int *)mRandomSoundList->GetElementByName(pName);
    if(!rMaxAmount) return;
    PlayRandomSound(pName, *rMaxAmount);
}
void AudioManager::StopAllSound()
{
    //--Order all sounds to stop playing.
    if(xBlockSound) return;
    AudioPackage *rPack = (AudioPackage *)mSoundList->PushIterator();
    while(rPack)
    {
        rPack->Stop();
        rPack = (AudioPackage *)mSoundList->AutoIterate();
    }
}
void AudioManager::PlaySpeech(const char *pPath)
{
    //--Unlike other audio sources, Speech only allows one instance to be in memory at a time.  If
    //  a new instance is loaded, the old one is automatically deleted.
    //--Passing NULL will deallocate the pack and not replace it.
    delete mCurrentSpeech;
    mCurrentSpeech = NULL;
    if(!pPath) return;

    mCurrentSpeech = new AudioPackageStream();
    mCurrentSpeech->Load(pPath);
    mCurrentSpeech->Play();
}
bool AudioManager::StopSpeech()
{
    ///--[Documentation]
    //--Stops the speech pack playing and nullifies it.  Does NOT flip over the dialogue hastening
    //  call, but this affects the return state.  If the speech had already stopped of its own accord
    //  then return false, if the speech was stopped by us return true.
    if(!mCurrentSpeech) return false;

    //--Sound will auto-stop on destructor call.
    bool tReturn = mCurrentSpeech->IsPlaying();
    delete mCurrentSpeech;
    mCurrentSpeech = NULL;
    return tReturn;
}
void AudioManager::PlaySoundDelay(const char *pName, int pTicks)
{
    ///--[Documentation]
    //--Enqueues a sound to play with a delay. If the ticks provided is 0 or lower, just plays the sound immediately.
    if(!pName) return;

    //--Play immediately.
    if(pTicks < 1)
    {
        PlaySound(pName);
        return;
    }

    //--Create pack.
    AudioDelayPack *nPack = (AudioDelayPack *)starmemoryalloc(sizeof(AudioDelayPack));
    nPack->mTicks = pTicks;
    strncpy(nPack->mSoundName, pName, 80);
    mDelayedSoundList->AddElement("X", nPack, &FreeThis);
}

///--[Sounds]
void AudioManager::PauseSound(const char *pName)
{
    AudioPackage *rPack = (AudioPackage *)mSoundList->GetElementByName(pName);
    if(!rPack) return;
    rPack->Pause();
}

///=========================================== Update =============================================
void AudioManager::Update()
{
    //--Stops any music package that is playing at zero volume.
    AudioPackage *rPack = (AudioPackage *)mMusicList->PushIterator();
    while(rPack)
    {
        if(rPack->IsPlaying() && rPack->GetVolume() < 0.01f && !rPack->PlaysWhenAtZeroVolume())
        {
            rPack->Stop();
        }
        rPack = (AudioPackage *)mMusicList->AutoIterate();
    }

    //--Play any SFX that are enqueued.
    AudioDelayPack *rPackage = (AudioDelayPack *)mDelayedSoundList->SetToHeadAndReturn();
    while(rPackage)
    {
        //--Decrement.
        rPackage->mTicks --;

        //--Play and remove.
        if(rPackage->mTicks < 1)
        {
            PlaySound(rPackage->mSoundName);
            mDelayedSoundList->RemoveRandomPointerEntry();
        }

        //--Next.
        rPackage = (AudioDelayPack *)mDelayedSoundList->IncrementAndGetRandomPointerEntry();
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
AudioPackage *AudioManager::GetPlayingMusic()
{
    //--Note: Can legally return NULL.
    return rCurrentMusic;
}
AudioPackage *AudioManager::GetMusicPack(const char *pName)
{
    return (AudioPackage *)mMusicList->GetElementByName(pName);
}
AudioPackage *AudioManager::GetSoundPack(const char *pName)
{
    return (AudioPackage *)mSoundList->GetElementByName(pName);
}
AudioPackage *AudioManager::GetVoicePack()
{
    return mCurrentSpeech;
}

///===================================== Static Functions =========================================
AudioManager *AudioManager::Fetch()
{
    return Global::Shared()->gAudioManager;
}

///======================================== Lua Hooking ===========================================
void AudioManager::HookToLuaState(lua_State *pLuaState)
{
    /* AudioManager_Register(sName, "AsMusic", "AsStream", sPath)
       AudioManager_Register(sName, "AsMusic", "AsSample", sPath)
       AudioManager_Register(sName, "AsSound", "AsStream", sPath)
       AudioManager_Register(sName, "AsSound", "AsSample", sPath)
       AudioManager_Register(sName, "AsRandomSound", iTotalSounds)
       Registers the audio at the given path as a member of the given list.  Music is mutually
       exclusive:  Only one music may be playing at a time.  Sound is not so picky.
       Streams are streamed from the hard drive, samples are loaded all at once.  Which to use is
       based on how much RAM you have and want to use. */
    lua_register(pLuaState, "AudioManager_Register", &Hook_AudioManager_Register);

    /* AudioManager_RegisterAdv(sName, sPath, sPropertyString)
       Registers an audio package using a segmented string with delimiter '|', used to designate
       properties for the audio package. See AudioManagerProperties.cc for a listing. */
    lua_register(pLuaState, "AudioManager_RegisterAdv", &Hook_AudioManager_RegisterAdv);

    /* AudioManager_PlayMusic(sName)
       Plays the named piece of music, if it exists.  Fails silently. */
    lua_register(pLuaState, "AudioManager_PlayMusic", &Hook_AudioManager_PlayMusic);

    /* AudioManager_PlaySound(sName)
       Plays the named sound, if it exists.  Fails silently. */
    lua_register(pLuaState, "AudioManager_PlaySound", &Hook_AudioManager_PlaySound);

    /* AudioManager_PlaySoundDelay(sName, iTicks)
       Plays the named sound, if it exists.  Fails silently. Has a delay specified by the tick number. */
    lua_register(pLuaState, "AudioManager_PlaySoundDelay", &Hook_AudioManager_PlaySoundDelay);

    /* AudioManager_PlaySpeech(sPath)
       Plays the speech loaded right off the hard drive.  If another instance of speech is currently
       playing, it is stopped automatically.  Loading a new instance of Speech will delete the old
       one, never worry about memory management again! */
    lua_register(pLuaState, "AudioManager_PlaySpeech", &Hook_AudioManager_PlaySpeech);

    /* AudioManager_StopMusic()
       Instantly stops the playing music, without fading it. */
    lua_register(pLuaState, "AudioManager_StopMusic", &Hook_AudioManager_StopMusic);

    /* AudioManager_FadeMusic(iTickDuration)
       Fades the music out. Pass a negative to cause the music to lose its 'Is the current music' status. */
    lua_register(pLuaState, "AudioManager_FadeMusic", &Hook_AudioManager_FadeMusic);

    /* AudioManager_StopSound(sName)
       Stops the named sound effect if it's playing. */
    lua_register(pLuaState, "AudioManager_StopSound", &Hook_AudioManager_StopSound);

    /* AudioManager_GetProperty("Music Volume") (1 Float)
       AudioManager_GetProperty("Sound Volume") (1 float)
       AudioManager_GetProperty("Lenth Of Sound", sSoundName) (1 Float) (in seconds)
       Gets and returns the requested property from the AudioManager. All getters are static. */
    lua_register(pLuaState, "AudioManager_GetProperty", &Hook_AudioManager_GetProperty);

    /* AudioManager_SetProperty("Music Volume", fVolume)
       AudioManager_SetProperty("Sound Volume", fVolume)
       Sets the requested property in the AudioManager. All setters are static. */
    lua_register(pLuaState, "AudioManager_SetProperty", &Hook_AudioManager_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AudioManager_Register(lua_State *L)
{
    //AudioManager_Register(sName, "AsMusic", "AsStream", sPath)
    //AudioManager_Register(sName, "AsMusic", "AsStream", sPath, fLoopStart, fLoopEnd)
    //AudioManager_Register(sName, "AsMusic", "AsSample", sPath)
    //AudioManager_Register(sName, "AsMusic", "AsSample", sPath, fLoopStart, fLoopEnd)
    //AudioManager_Register(sName, "AsSound", "AsStream", sPath)
    //AudioManager_Register(sName, "AsSound", "AsSample", sPath)
    //AudioManager_Register(sName, "AsSound", "AsStream", sPath, iAbstractSlot)
    //AudioManager_Register(sName, "AsSound", "AsSample", sPath, iAbstractSlot)
    //AudioManager_Register(sName, "AsRandomSound", iTotalSounds)
    int pArgs = lua_gettop(L);
    if(pArgs < 4) return LuaArgError("AudioManager_Register");

    //--If this is a Random Sound, just handle that here and ignore the rest.
    if(!strcmp(lua_tostring(L, 2), "AsRandomSound"))
    {
        AudioManager::Fetch()->RegisterRandomSound(lua_tostring(L, 1), lua_tointeger(L, 3));
        return 0;
    }

    //--Sound or Music?  Defaults to Sound.
    bool tIsMusic = false;
    if(!strcmp(lua_tostring(L, 2), "AsMusic"))
    {
        tIsMusic = true;
    }

    //--Sample or Stream?  Defaults to Sample.
    bool tIsStream = false;
    if(!strcmp(lua_tostring(L, 3), "AsStream"))
    {
        tIsStream = true;
    }

    //--If the package already exists, fail.
    if(tIsMusic)
    {
        if(AudioManager::Fetch()->MusicExists(lua_tostring(L, 1)))
        {
            AudioManager::Fetch()->ClearLastPackage();
            return 0;
        }
    }
    else
    {
        if(AudioManager::Fetch()->SoundExists(lua_tostring(L, 1)))
        {
            AudioManager::Fetch()->ClearLastPackage();
            return 0;
        }
    }

    //--Create
    AudioPackage *nPackage = NULL;
    if(!tIsStream)
    {
        nPackage = new AudioPackageSample();
        nPackage->Load(lua_tostring(L, 4));
    }
    else
    {
        //--If this is a music stream, it needs to be tagged as a tempo stream.
        nPackage = new AudioPackageStream();
        //if(tIsMusic) ((AudioPackageStream *)(nPackage))->SetTempoStream();
        nPackage->Load(lua_tostring(L, 4));
    }

    //--Register as a sound effect
    if(!tIsMusic)
    {
        nPackage->SetGlobalVolume(AudioManager::xSoundVolume);
        AudioManager::Fetch()->RegisterSound(lua_tostring(L, 1), nPackage);
    }
    //--Register as a music track.
    else
    {
        nPackage->SetGlobalVolume(AudioManager::xMusicVolume);
        AudioManager::Fetch()->RegisterMusic(lua_tostring(L, 1), nPackage);
    }

    //--If this is a music package, and there are 6 arguments, activate looping.
    if(tIsMusic && pArgs >= 6)
    {
        nPackage->AddLoop(lua_tonumber(L, 5), lua_tonumber(L, 6));
    }

    //--If this is a sound package and there are 5 arguments, use an "Abstract" volume. This allows
    //  different sounds to have different volumes than the usual 2.
    //--Note that abstract slot -1 is "Use the sound volume".
    if(!tIsMusic && pArgs == 5)
    {
        //--Resolve slot.
        int tSlot = lua_tointeger(L, 5);
        if(tSlot >= 0 && tSlot < AM_ABSTRACT_VOLUME_TOTAL)
        {
            nPackage->SetGlobalVolume(AudioManager::xAbstractVolume[tSlot]);
            nPackage->SetAbstractSlot(tSlot);
        }
    }

    return 0;
}
int Hook_AudioManager_RegisterAdv(lua_State *L)
{
    //AudioManager_RegisterAdv(sName, sPath, sPropertyString)
    int pArgs = lua_gettop(L);
    if(pArgs < 3) return LuaArgError("AudioManager_RegisterAdv");
    AudioManager::Fetch()->RegisterAdvanced(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));
    return 0;
}
int Hook_AudioManager_PlayMusic(lua_State *L)
{
    //AudioManager_PlayMusic(sName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_PlayMusic");

    AudioManager::Fetch()->PlayMusic(lua_tostring(L, 1));
    return 0;
}
int Hook_AudioManager_PlaySound(lua_State *L)
{
    //AudioManager_PlaySound(sName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_PlaySound");

    AudioManager::Fetch()->PlaySound(lua_tostring(L, 1));
    return 0;
}
int Hook_AudioManager_PlaySoundDelay(lua_State *L)
{
    //AudioManager_PlaySoundDelay(sName, iTicks)
    int pArgs = lua_gettop(L);
    if(pArgs != 2) return LuaArgError("AudioManager_PlaySoundDelay");

    AudioManager::Fetch()->PlaySoundDelay(lua_tostring(L, 1), lua_tointeger(L, 2));
    return 0;
}
int Hook_AudioManager_PlaySpeech(lua_State *L)
{
    //AudioManager_PlaySpeech(sPath)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_PlaySpeech");

    AudioManager::Fetch()->PlaySpeech(lua_tostring(L, 1));
    return 0;
}

///--[Stopping]
int Hook_AudioManager_StopMusic(lua_State *L)
{
    //AudioManager_StopMusic()

    AudioManager::Fetch()->StopAllMusic();
    return 0;
}
int Hook_AudioManager_FadeMusic(lua_State *L)
{
    //AudioManager_FadeMusic(iTickDuration)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_FadeMusic");

    AudioManager::Fetch()->FadeMusic(lua_tointeger(L, 1));
    return 0;
}
int Hook_AudioManager_StopSound(lua_State *L)
{
    //AudioManager_StopSound(sName)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_StopSound");

    AudioManager::Fetch()->StopSound(lua_tostring(L, 1));
    return 0;
}
int Hook_AudioManager_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AudioManager_GetProperty("Music Exists", sMusicName) (1 String)
    //AudioManager_GetProperty("Is Music Playing") (1 Boolean)
    //AudioManager_GetProperty("Name Of Music Playing") (1 String)
    //AudioManager_GetProperty("Music Volume") (1 Float) (Static)
    //AudioManager_GetProperty("Sound Volume") (1 Float) (Static)
    //AudioManager_GetProperty("Abstract Volume", iSlot) (1 Float) (Static)
    //AudioManager_GetProperty("Default Music Volume") (1 Float) (Static)
    //AudioManager_GetProperty("Default Sound Volume") (1 Float) (Static)
    //AudioManager_GetProperty("Length Of Sound", sSoundName) (1 Float) (in seconds)
    //AudioManager_GetProperty("Bass Enumeration", sEnumerationName) (1 Integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AudioManager_GetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--True if the named music track exists.
    if(!strcasecmp(rSwitchType, "Music Exists") && tArgs >= 2)
    {
        lua_pushboolean(L, AudioManager::Fetch()->MusicExists(lua_tostring(L, 2)));
        return 1;
    }
    //--True if music is currently playing, false if not.
    else if(!strcasecmp(rSwitchType, "Is Music Playing"))
    {
        lua_pushboolean(L, AudioManager::Fetch()->IsMusicPlaying());
        return 1;
    }
    //--Name of the currently playing music. Will be "Null" if no music is playing.
    else if(!strcasecmp(rSwitchType, "Name Of Music Playing"))
    {
        const char *rPlayingMusicName = AudioManager::Fetch()->GetNameOfPlayingMusic();
        if(!rPlayingMusicName)
        {
            lua_pushstring(L, "Null");
        }
        else
        {
            lua_pushstring(L, rPlayingMusicName);
        }
        return 1;
    }
    //--Music Volume.
    else if(!strcasecmp(rSwitchType, "Music Volume"))
    {
        lua_pushnumber(L, AudioManager::xMusicVolume);
        return 1;
    }
    //--Sound Volume.
    else if(!strcasecmp(rSwitchType, "Sound Volume"))
    {
        lua_pushnumber(L, AudioManager::xSoundVolume);
        return 1;
    }
    //--Abstract Volume.
    else if(!strcasecmp(rSwitchType, "Abstract Volume") && tArgs == 2)
    {
        //--Range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AM_ABSTRACT_VOLUME_TOTAL)
        {
            lua_pushnumber(L, AM_DEFAULT_ABSTRACT_VOLUME);
        }
        else
        {
            lua_pushnumber(L, AudioManager::xAbstractVolume[tSlot]);
        }
        return 1;
    }
    //--Default Music Volume.
    if(!strcasecmp(rSwitchType, "Default Music Volume"))
    {
        lua_pushnumber(L, AM_DEFAULT_MUSIC_VOLUME);
        return 1;
    }
    //--Default Sound Volume.
    else if(!strcasecmp(rSwitchType, "Default Sound Volume"))
    {
        lua_pushnumber(L, AM_DEFAULT_SOUND_VOLUME);
        return 1;
    }
    //AudioManager_GetProperty("Length Of Sound", sSoundName) (1 Float) (in seconds)
    else if(!strcasecmp(rSwitchType, "Length Of Sound") && tArgs >= 2)
    {
        lua_pushnumber(L, AudioManager::Fetch()->GetLengthOfSound(lua_tostring(L, 2)));
        return 1;
    }
    //AudioManager_GetProperty("Bass Enumeration", sEnumerationName) (1 Integer)
    else if(!strcasecmp(rSwitchType, "Bass Enumeration") && tArgs >= 2)
    {
        lua_pushnumber(L, AudioManager::Fetch()->GetBassEnumeration(lua_tostring(L, 2)));
        return 1;
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AudioManager_GetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_AudioManager_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AudioManager_SetProperty("Music Volume", fVolume)
    //AudioManager_SetProperty("Sound Volume", fVolume)
    //AudioManager_SetProperty("Abstract Volume", iSlot, fVolume)
    //AudioManager_SetProperty("Register Effect Profile", sName, iType)

    //--[Effects]
    //AudioManager_SetProperty("Set Effect Profile Property", sName, iType, sProperty, fValue)
    //AudioManager_SetProperty("Apply Effect To Sound", sSoundName, sEffectName)
    //AudioManager_SetProperty("Clear Effect Profiles")

    //--[Other]
    //AudioManager_SetProperty("Unregister Sound", sSoundName)

    ///--[Arg Resolve]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AudioManager_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    AudioManager *rAudioManager = AudioManager::Fetch();

    ///--[Static Types]
    //--Music Volume.
    if(!strcasecmp(rSwitchType, "Music Volume") && tArgs == 2)
    {
        rAudioManager->ChangeMusicVolumeTo(lua_tonumber(L, 2));
    }
    //--Sound Volume.
    else if(!strcasecmp(rSwitchType, "Sound Volume") && tArgs == 2)
    {
        rAudioManager->ChangeSoundVolumeTo(lua_tonumber(L, 2));
    }
    //--Abstract Volume.
    else if(!strcasecmp(rSwitchType, "Abstract Volume") && tArgs == 3)
    {
        rAudioManager->ChangeAbstractVolumeTo(lua_tointeger(L, 2), lua_tonumber(L, 3));
    }
    ///--[Effect Profiles]
    else if(!strcasecmp(rSwitchType, "Register Effect Profile") && tArgs >= 3)
    {
        rAudioManager->RegisterEffectProfile(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //AudioManager_SetProperty("Set Effect Profile Property", sName, iType, sProperty, fValue) (Static)
    else if(!strcasecmp(rSwitchType, "Set Effect Profile Property") && tArgs >= 5)
    {
        rAudioManager->SetEffectProfileProperty(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4), lua_tonumber(L, 5));
    }
    //AudioManager_SetProperty("Apply Effect To Sound", sSoundName, sEffectName)
    else if(!strcasecmp(rSwitchType, "Apply Effect To Sound") && tArgs >= 3)
    {
        rAudioManager->ApplyEffectProfileToSound(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    else if(!strcasecmp(rSwitchType, "Clear Effect Profiles") && tArgs >= 1)
    {
        rAudioManager->ClearEffectProfiles();
    }
    ///--[Other]
    //--Removes a sound entirely. Typically used by mods to override sound effects.
    else if(!strcasecmp(rSwitchType, "Unregister Sound") && tArgs >= 2)
    {
        rAudioManager->UnregisterSound(lua_tostring(L, 2));
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AudioManager_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
