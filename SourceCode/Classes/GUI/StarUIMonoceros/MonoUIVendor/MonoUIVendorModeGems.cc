//--Base
#include "MonoUIVendor.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== Update ==============================================
void MonoUIVendor::UpdateModeGems()
{
    ///--[Documentation]
    //--Update handler for Gems mode. Identical to the base but sizes the scroll page differently.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rGemsList = rInventory->GetGemList();

    ///--[Timers]
    //--Standard timers.
    StandardTimer(mHelpVisibilityTimer,      0, cHlpTicks,                mIsShowingHelp);
    StandardTimer(mMergeHelpVisibilityTimer, 0, cHlpTicks,                mIsShowingMergeHelp);
    StandardTimer(mGemMergeTimer,            0, cxAdvVisTicks,            mIsMergeMode);
    StandardTimer(mGemConfirmMergeTimer,     0, cxAdvAskMergeTicks,       mIsConfirmMergeMode);
    StandardTimer(mGemDisassembleTimer,      0, cxAdvAskDisassembleTicks, mIsConfirmDisassembleMode);

    //--If these timers are not zeroed or capped, block the update.
    if(mGemMergeTimer        > 0 && mGemMergeTimer        < cxAdvVisTicks)            return;
    if(mGemConfirmMergeTimer > 0 && mGemConfirmMergeTimer < cxAdvAskMergeTicks)       return;
    if(mGemDisassembleTimer  > 0 && mGemDisassembleTimer  < cxAdvAskDisassembleTicks) return;

    ///--[Gemcutting Disallowed]
    //--No gemcutting allowed at this vendor. Handle mode switches and stop the update.
    if(!mAllowGemcutting)
    {
        //--Changes modes to the left, which is Gems.
        if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
        {
            ScrollMode(-1);
            return;
        }
        //--Changes modes to the right, which is Sell.
        if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
        {
            ScrollMode(1);
            return;
        }

        //--Stop.
        return;
    }

    ///--[Mode Handlers]
    //--Merging submode.
    if(mIsMergeMode)              { UpdateModeGemsMerge(); return; }
    if(mIsConfirmDisassembleMode) { UpdateGemDisassembly(); return; }
    if(CommonDetailsUpdate()) return;
    if(CommonHelpUpdate())    return;

    ///--[Left and Right, Change Mode]
    //--Changes modes to the left, which is Gems.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && !mIsShowingDetails)
    {
        ScrollMode(-1);
        return;
    }
    //--Changes modes to the right, which is Sell.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && !mIsShowingDetails)
    {
        ScrollMode(1);
        return;
    }

    ///--[Up and Down]
    //--Increments/decrements on the gem selection list. Wrapping is supported.
    if(AutoListUpDn(mCursor, mSkip, rGemsList->GetListSize(), cScrollBuf, cxMonoScrollGemPageSize, true))
    {
        RecomputeCursorPositions();
        return;
    }

    ///--[Run]
    //--Activates disassembly.
    if(rControlManager->IsFirstPress("Run") && !mIsShowingDetails)
    {
        //--Error, no gems.
        if(rGemsList->GetListSize() < 1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Get the gem.
        AdventureItem *rSelectedGem = (AdventureItem *)rGemsList->GetElementBySlot(mCursor);
        if(!rSelectedGem) return;

        //--Get needs to have at least one subgem.
        if(rSelectedGem->GetRank() < 1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Begin disassembly.
        mIsConfirmDisassembleMode = true;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Activate]
    //--Enter merge mode.
    if(rControlManager->IsFirstPress("Activate") && !mIsShowingDetails)
    {
        //--No gems.
        if(rGemsList->GetListSize() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Switch to merge mode.
        else
        {
            //--Get the gem in question and store it in a temporary buffer.
            AdventureItem *rGem = (AdventureItem *)rGemsList->GetElementBySlot(mCursor);
            if(!rGem)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Store the name.
            const char *rName = rGemsList->GetNameOfElementBySlot(mCursor);
            ResetString(mMergeGemOwner, rName);

            //--Liberate it. This destabilizes rName.
            rGemsList->SetRandomPointerToThis(rGem);
            rGemsList->LiberateRandomPointerEntry();

            //--Store it.
            mMergeGem = rGem;

            //--Flags.
            RefreshGemMergeMenuHelp();
            mIsMergeMode = true;
            mCursor = 0;
            mSkip = 0;
            RecomputeCursorPositions();
            mHighlightPos.Complete();
            mHighlightSize.Complete();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        HandleExit();
        return;
    }
}

///===================================== Segment Rendering ========================================
void MonoUIVendor::RenderTopGems(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Render]
    //--Header.
    MonoImages.rOverlay_TitleShade->Draw();

    //--Header Text.
    SetColor("Title", cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(538.0f, 16.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mShopName);
    SetColor("Clear", cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderRgtGems(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    //--Adamantite Count
    RenderGemsMergeAdamantite(cColorAlpha);

    //--Properties
    RenderGemsComparison(cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderBotGems(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rGemsList = rInventory->GetGemList();

    ///--[Render]
    //--Inventory Listing.
    MonoImages.rFrame_GemListing->Draw();
    MonoImages.rFrame_GemInstructions->Draw();

    //--If there are no entries on the item list, render this instead.
    if(rGemsList->GetListSize() < 1)
    {
        AdvImages.rFont_Mainline->DrawTextFixed(267.0f, 289.0f, cxAdvMaxNameWid, 0, xrStringData->str.mTextYouHaveNoGems);
    }
    //--Heading.
    else
    {
        SetColor("Paragraph", cColorAlpha);
        AdvImages.rFont_Heading->DrawText(538.0f, 105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSelectAGem);
        SetColor("Clear", cColorAlpha);
    }

    //--Titles
    SetColor("Title", cColorAlpha);
    AdvImages.rFont_Heading->DrawText(206.0f, 190.0f, 0, 1.0f, xrStringData->str.mTextName);
    AdvImages.rFont_Heading->DrawText(503.0f, 190.0f, 0, 1.0f, xrStringData->str.mTextTier);
    AdvImages.rFont_Heading->DrawText(752.0f, 190.0f, 0, 1.0f, xrStringData->str.mTextColors);
    SetColor("Clear", cColorAlpha);

    //--Backings. Rendered apart from the entries, allowing the cursor to be over the backing and under
    //  the text entries.
    float cEntrySpcH = 43.0f;
    int tBackingsToRender = rGemsList->GetListSize();
    if(tBackingsToRender > cxMonoScrollGemPageSize) tBackingsToRender = cxMonoScrollGemPageSize;
    for(int i = 0; i < tBackingsToRender; i ++)
    {
        MonoImages.rOverlay_GemList->Draw(0.0f, i * cEntrySpcH);
    }

    //--Cursor.
    MonoImages.rOverlay_GemSelect->Draw(0.0f, mHighlightPos.mYCur - MonoImages.rOverlay_GemSelect->GetYOffset());

    //--Subroutine.
    RenderGemsItemListing(mSkip, cxMonoScrollGemPageSize, cColorAlpha);

    //--Scrollbar.
    if(rGemsList->GetListSize() > cxMonoScrollGemPageSize)
    {
        StandardRenderScrollbar(mSkip, cxMonoScrollGemPageSize, rGemsList->GetListSize() - cxMonoScrollGemPageSize, 219.0f, 480.0f, false, AdvImages.rScrollbar_Scroller, MonoImages.rScrollbar_GemsStatic);
    }

    //--Help Strings.
    if(!mIsMergeMode)
    {
        mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
        mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
        mShowHelpString->   DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Control);
    }

    //--Right side.
    mDisassembleString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxMonoFontMainlineFontH * 1.0f), SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, MonoImages.rFont_Control);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderUnalignedGems(float pVisAlpha)
{
    AdvUIVendor::RenderUnalignedGems(pVisAlpha);
}

///===================================== Common Rendering =========================================
void MonoUIVendor::RenderGemsComparison(float pColorAlpha)
{
    ///--[Documentation]
    //--Given a selected gem, renders its properties.
    AdventureItem *rGem = GetConsideredItem();

    ///--[Base Gem]
    //--Frame.
    SetColor("Clear", pColorAlpha);
    MonoImages.rFrame_GemProperties->Draw();

    //--Title.
    SetColor("Subtitle", pColorAlpha);
    AdvImages.rFont_Heading->DrawText(1198.0f, 384.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextProperties);
    SetColor("Clear", pColorAlpha);

    //--Constants.
    //float cLft     = 1044.0f;
    //float cRgt     = 1366.0f;
    float cPropX   = 1110.0f;
    float cResistX = 1110.0f;
    float cDamageX = 1270.0f;
    float cIconX   = 1060.0f;
    float cIconY   =  440.0f;
    float cIconH   =   30.0f;

    ///--[Common Backing]
    //--Even if there's no selected gem, render the icons.
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));
    }

    //--No gem, no render.
    if(!rGem)
    {
        return;
    }

    ///--[Gem Properties]
    //--Icons and values.
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        //--Backing.
        MonoImages.rOverlay_MaterialShade->Draw(53.0f, 237.0f + (i * cIconH));

        ///--[Simple Statistics]
        //--If this is a statistic, render a single value.
        if(i < MONOUIVEN_RESISTANCE_BEGIN)
        {
            //--Value.
            int tValue = rGem->GetStatistic(mStatisticIndexes[i]);
            AdvImages.rFont_Statistic->DrawTextArgs(cPropX, cIconY + (cIconH * i) - 4.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
        }
        ///--[Resistance and Damage]
        //--Shows two values. Resistance, and Damage Bonus.
        else
        {
            //--Get all values.
            int tResistance = rGem->GetStatistic(mStatisticIndexes[i]);
            int tBonusTags = rGem->GetTagCount(mBonusTagNames[i]);
            int tMalusTags = rGem->GetTagCount(mMalusTagNames[i]);

            //--Resistance Value
            AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cIconY + (cIconH * i) - 4.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistance);

            //--Damage Bonus
            int tDamageValue = 100 + tBonusTags - tMalusTags;
            if(tDamageValue < 0) tDamageValue = 0;

            //--Render. Only renders when not replacing.
            AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cIconY + (cIconH * i) - 4.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tDamageValue);
        }
    }
}
