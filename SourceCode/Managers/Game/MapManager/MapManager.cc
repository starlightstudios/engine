//--Base
#include "MapManager.h"

//--Classes
#include "AbyssCombat.h"
#include "AdvCombat.h"
#include "AdventureInventory.h"
#include "GalleryMenu.h"
#include "MonocerosMenu.h"
#include "RootLevel.h"
#include "PlayerPony.h"
#include "FlexMenu.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarCamera2D.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

#define FADE_TICKS 120

///========================================== System ==============================================
MapManager::MapManager()
{
    //--[MapManager]
    //--System
    //--Activity
    mActiveLevel = NULL;

    //--Menu Activity
    mHasPendingClose = false;
    mMenuStack = new StarLinkedList(true);
    mIsShowingGallery = false;
    mGalleryMenu = new GalleryMenu();

    //--Overlay
    mShowOverlay = false;
    mIsOverlayFadeIn = false;
    mOverlayFadeTimer = 0;
    mOverlayColor.SetRGBAF(1.0f, 1.0f, 1.0f, 0.5f);

    //--Class Switching
    mUseDialogueType = POINTER_TYPE_WORLDDIALOGUE;
    mUseMenuType = POINTER_TYPE_ADVENTUREMENU;
    mUseCombatType = POINTER_TYPE_ADVCOMBAT;

    //--Global GUI pieces
    mGlobalAdvCombat = NULL;
    mAdventureInventory = new AdventureInventory();
    mWorldDialogue = new WorldDialogue();

    //--Shaking
    mIsShaking = false;
    mShakeHorizontal = true;
    mShakeRadius = 0.0f;
    mShakeTicksRemaining = 0;
    mShakeTicksInitial = 1;

    //--Catalyst Storage
    mCatalystInfoList = new StarLinkedList(true);

    //--Public Variables
    mReloadAtEndOfTick = NULL;
    mBackToTitle = false;
}
MapManager::~MapManager()
{
    delete mActiveLevel;
    delete mMenuStack;
    delete mGalleryMenu;
    delete mGlobalAdvCombat;
    delete mAdventureInventory;
    delete mWorldDialogue;
    free(mReloadAtEndOfTick);
    delete mCatalystInfoList;
}

///--[Public Statics]
//--Stores if there is only one active game. This causes some title screens to quit directly.
bool MapManager::xOnlyOneGame = false;

//--Flag, prevents rendering of extras on the title screen. This includes text and character portraits.
bool MapManager::xDontRenderExtras = false;
bool MapManager::xHasRenderedMenus = false;
bool MapManager::xHasUpdatedMenus = false;
float MapManager::xCandidateAlpha = 1.0f;

///===================================== Property Queries =========================================
char *MapManager::GetMapName()
{
    if(mActiveLevel) return mActiveLevel->GetName();
    return NULL;
}
float MapManager::GetDepth()
{
    return 1.0f;
}
bool MapManager::IsMapReady()
{
    //--Returns false if no map is set/loaded.
    if(mActiveLevel) return true;
    return false;
}
float MapManager::GetScaleFactor()
{
    return 1.0f;
}
void MapManager::GetShakeFactors(float &sX, float &sY)
{
    //--Not shaking, return all zeroes.
    if(!mIsShaking)
    {
        sX = 0.0f;
        sY = 0.0f;
        return;
    }

    //--Shaking, normal case.
    if(!mShakeHorizontal)
    {
        float tAngle = (float)(rand() % 628) / 100.0f;
        sX = mShakeRadius * cos(tAngle);
        sY = mShakeRadius * sin(tAngle);
    }
    //--Shakes only horizontally.
    else
    {
        float tPercent = (float)mShakeTicksRemaining / (float)mShakeTicksInitial;
        sX = sin(3.1415926f * 20.0f * tPercent) * mShakeRadius;
        sY = 0.0f;
    }
}
bool MapManager::MenuStackHasContents()
{
    return (mMenuStack->GetListSize() > 0);
}
const char *MapManager::IsCatalystRegistered(const char *pLevelName, const char *pChestName, const char *pContents)
{
    //--After catalyst construction is done, this can be called to ask which chapter, if any, the given
    //  combination of catalyst information is registered to. Returns "Null" if the entry is not found.
    MapCatalystPack *rPackage = (MapCatalystPack *)mCatalystInfoList->PushIterator();
    while(rPackage)
    {
        //--Pack match:
        if(!strcmp(pLevelName, rPackage->mLevelName) && !strcmp(pChestName, rPackage->mChestName) && !strcmp(pContents, rPackage->mCatalystType))
        {
            mCatalystInfoList->PopIterator();
            return rPackage->mChapterAssociated;
        }

        rPackage = (MapCatalystPack *)mCatalystInfoList->AutoIterate();
    }

    //--Failed.
    return "Null";
}

///======================================== Manipulators ==========================================
void MapManager::ReceiveLevel(RootLevel *pLevel)
{
    //--Clear the active level and set a new level.
    delete mActiveLevel;
    mActiveLevel = pLevel;

    //--The DataLibrary typically waits for level transitions to purge bitmaps. Handle that here.
    //  It is possible the DataLibrary hasn't booted yet so it is NULL checked.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(rDataLibrary)
    {
        rDataLibrary->UnloadPendingBitmaps();
    }
}
void MapManager::SetScaleFactor(float pFactor)
{
}
void MapManager::SetOverlayColor(float pRed, float pGreen, float pBlue, float pAlpha)
{
    mShowOverlay = true;
    if(pAlpha <= 0.0f) mShowOverlay = false;
    mOverlayColor.SetRGBAF(pRed, pGreen, pBlue, pAlpha);
}
void MapManager::BeginFade()
{
    mShowOverlay = true;
    mIsOverlayFadeIn = true;
    mOverlayFadeTimer = 0;
}
void MapManager::BeginShake(float pRadius, int pTicks)
{
    if(pRadius < 0.0f || pTicks < 1) return;
    mIsShaking = true;
    mShakeRadius = pRadius;
    mShakeTicksRemaining = pTicks;
    mShakeTicksInitial = pTicks;
}
void MapManager::PushMenuStack(FlexMenu *pMenu)
{
    mMenuStack->AddElementAsHead("X", pMenu, RootObject::DeleteThis);
}
void MapManager::PopMenuStack()
{
    mMenuStack->DeleteHead();
}
void MapManager::ShowGallery()
{
    mIsShowingGallery = true;
}
void MapManager::HideGallery()
{
    mIsShowingGallery = false;
}
void MapManager::ChangeDialogueType(int pType)
{
    //--Note: Does not reset the dialogue type until a Reset() is called.
    mUseDialogueType = pType;
}
void MapManager::SetMenuType(int pType)
{
    //--Note: Does not reset the menu type until the next time a level is created.
    mUseMenuType = pType;
}
void MapManager::SetCombatType(int pType)
{
    //--Note: Does not reset the combat. Call a Reset() to do this once the type is set.
    mUseCombatType = pType;
}
void MapManager::TemporarilySwitchWorldDialogue(WorldDialogue *pNewDialogue)
{
    //--Temporarily places a new WorldDialogue in over the existing one. Does not delete the old version
    //  of the WorldDialogue, instead this is meant to redirect static calls such as the lua functions.
    //--The caller is expected to retain a pointer to the old dialogue and call this function again
    //  to switch the pointers back when it's done.
    mWorldDialogue = pNewDialogue;
}
void MapManager::ClearCatalystList()
{
    mCatalystInfoList->ClearList();
}
void MapManager::RegisterCatalyst(const char *pChapter, const char *pLevelName, const char *pChestName, const char *pContents)
{
    //--Registers a new catalyst entry.
    if(!pChapter || !pLevelName || !pChestName || !pContents) return;

    //--Create.
    MapCatalystPack *nPack = (MapCatalystPack *)starmemoryalloc(sizeof(MapCatalystPack));
    strncpy(nPack->mChapterAssociated, pChapter, 31);
    strncpy(nPack->mLevelName, pLevelName, 127);
    strncpy(nPack->mChestName, pChestName, 31);
    strncpy(nPack->mCatalystType, pContents, 31);

    //--Register.
    mCatalystInfoList->AddElementAsTail("X", nPack, &FreeThis);
}

///======================================== Core Methods ==========================================
void MapManager::Reset(const char *pResetType)
{
    ///--[Documentation]
    //--Resets the program using the provided code. Only called at the end of a tick.
    if(!pResetType) return;

    ///--[Full Reset]
    //--The Full Reset is only called at the end of a tick. It wipes the EM.
    if(!strcasecmp(pResetType, "Full Game Reset"))
    {
        //--Clear local stuff.
        mHasPendingClose = false;
        mMenuStack->ClearList();
        ReceiveLevel(NULL);

        //--This also stops music if it was playing.
        AudioManager::Fetch()->StopAllMusic();

        //--GUI.
        mWorldDialogue->CloseImmediately();
        mWorldDialogue->Clear();
    }
    ///--[Combat]
    //--Combat Reset. Wipes and rebuilds the Adventure Combat class.
    else if(!strcasecmp(pResetType, "Combat"))
    {
        delete mGlobalAdvCombat;
        mGlobalAdvCombat = GenerateNewCombat();
    }
    ///--[Dialogue]
    //--Dialogue. Wipes and rebuilds the WorldDialogue class.
    else if(!strcasecmp(pResetType, "Dialogue"))
    {
        delete mWorldDialogue;
        mWorldDialogue = GenerateNewDialogue();
        mWorldDialogue->Construct();
    }
    //--Unknown reset type.
    else
    {
    }
}

///=========================================== Update =============================================
void MapManager::Update()
{
    //--Shake handling
    if(mIsShaking)
    {
        mShakeTicksRemaining --;
        if(mShakeTicksRemaining < 1)
        {
            mShakeRadius = 0.0f;
            mIsShaking = false;
        }
    }

    //--Fade Handling
    if(mIsOverlayFadeIn && mShowOverlay)
    {
        mOverlayFadeTimer ++;
        if(mOverlayFadeTimer >= FADE_TICKS) mShowOverlay = false;
    }

    //--Level update.
    xHasUpdatedMenus = false;
    if(mActiveLevel)
    {
        mActiveLevel->Update();
    }

    //--If the menus were not updated in the level's cycle, update them now.
    if(!xHasUpdatedMenus)
    {
        //--Is the gallery active?
        if(mIsShowingGallery)
        {
            mGalleryMenu->Update();
        }
        //--Normal case.
        else
        {
            UpdateMenuStack(false);
        }
    }

    //--If the combat exists and has any SFX to play, do that here.
    if(mGlobalAdvCombat) mGlobalAdvCombat->PlayQueuedSounds();

    //--Swap the level out if this is present.
    if(mReloadAtEndOfTick)
    {
        LuaManager::Fetch()->ExecuteLuaFile(mReloadAtEndOfTick);
        ResetString(mReloadAtEndOfTick, NULL);
    }
}
void MapManager::UpdatePaused(uint8_t pPauseFlags)
{
    //--Update that executes when the program is paused for any reason. The default Pure Engine
    //  does nothing when paused.
}
bool MapManager::UpdateMenuStack(bool pPendingStackClose)
{
    //--Updates the menu stack, closing whatever was on it if flagged to do so. The menu can flag
    //  itself for a close, or the caller can force a close if desired.
    //--Returns true if the menu is currently active and updating should be stalled, false if not.
    if(mMenuStack->GetListSize() > 0)
    {
        //--Everything other than the last one does the not-active update.
        bool tIsStackHeadClosing = pPendingStackClose | mHasPendingClose;
        int tCount = 0;
        FlexMenu *rMenu = (FlexMenu *)mMenuStack->PushIterator();
        while(rMenu)
        {
            //--Run the inactive update.
            if(tCount < mMenuStack->GetListSize() - 1)
            {
                rMenu->UpdateInactive();
            }
            //--Run the active update. Also check for closing cases.
            else
            {
                rMenu->Update();
                tIsStackHeadClosing = tIsStackHeadClosing | rMenu->HasPendingClose();
            }

            rMenu = (FlexMenu *)mMenuStack->AutoIterate();
        }

        //--If pending a close, close the stack head (by deleting it).
        if(tIsStackHeadClosing)
        {
            PopMenuStack();
        }

        //--Skip the rest of the update.
        mHasPendingClose = false;
        return true;
    }

    //--Nothing on the menu.
    mHasPendingClose = false;
    return false;
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void MapManager::AddToRenderList(StarLinkedList *pRenderList)
{
    //--ManManager adds itself with a depth of 1.0f, which is outside the usual range of 0 to -1.
    //  The MapManager renders overlays which are considered to be above the usual rendering cycle.
    //--It also adds the active level, assuming it exists.
    xHasRenderedMenus = false;
    if(mActiveLevel) mActiveLevel->AddToRenderList(pRenderList);

    //--Add ourselves.
    pRenderList->AddElement("X", this);
}
void MapManager::Render()
{
    //--Rendering function for the MapManager's overlays. These render independent of depth.
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--In immediate-mode, we need to set a flag and render the level if it exists.
    if(!DisplayManager::Fetch()->IsUsingSortedRendering())
    {
        xHasRenderedMenus = false;
        if(mActiveLevel) mActiveLevel->Render();
    }

    //--If we have not yet rendered the menus, do so now.
    if(!xHasRenderedMenus && !mIsShowingGallery)
    {
        //--If the MapManager is rendering the menu, we should render the background now.
        StarBitmap *rBackgroundImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Background/MenuBG");
        if(rBackgroundImg) rBackgroundImg->Draw();

        //--Overlay. Uses the same alpha as the candidates below.
        StarBitmap *rBackgroundImgOver = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Background/MenuBGOver");
        if(rBackgroundImgOver && !xDontRenderExtras)
        {
            glColor4f(1.0f, 1.0f, 1.0f, xCandidateAlpha);
            rBackgroundImgOver->Draw();
            glColor3f(1.0f, 1.0f, 1.0f);
        }

        //--Render the menu.
        RenderMenuStack();
    }

    //--Render the special gallery.
    if(mIsShowingGallery)
    {
        //--Background.
        StarBitmap *rBackgroundImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Background/MenuBG");
        if(rBackgroundImg) rBackgroundImg->Draw();

        //--Gallery.
        mGalleryMenu->Render();
    }

    //--Render the overlay.
    if(mShowOverlay)
    {
        //--Setup.
        glDepthMask(false);
        glDisable(GL_TEXTURE_2D);

        //--Normal case:
        if(!mIsOverlayFadeIn)
        {
            mOverlayColor.SetAsMixer();
        }
        //--Fading case.
        else
        {
            float tAlpha = 1.0f - (mOverlayFadeTimer / (float)FADE_TICKS);
            glColor4f(0.0f, 0.0f, 0.0f, tAlpha);
        }

        //--Render.
        float tWidth = Global::Shared()->gScreenWidthPixels;
        float tHeight = Global::Shared()->gScreenHeightPixels;
        glBegin(GL_QUADS);
            glVertex2f(  0.0f,   0.0f);
            glVertex2f(tWidth,   0.0f);
            glVertex2f(tWidth, tHeight);
            glVertex2f(  0.0f, tHeight);
        glEnd();

        //--Clean.
        glColor3f(1.0f, 1.0f, 1.0f);
        glEnable(GL_TEXTURE_2D);
        glDepthMask(true);
    }
}
void MapManager::RenderMenuStack()
{
    FlexMenu *rMenu = (FlexMenu *)mMenuStack->PushIterator();
    while(rMenu)
    {
        rMenu->Render();
        rMenu = (FlexMenu *)mMenuStack->AutoIterate();
    }
}

///====================================== Pointer Routing =========================================
RootLevel *MapManager::LiberateLevel()
{
    RootLevel *mActiveLevelPtr = mActiveLevel;
    mActiveLevel = NULL;
    return mActiveLevelPtr;
}
RootLevel *MapManager::GetActiveLevel()
{
    return mActiveLevel;
}
FlexMenu *MapManager::GetMenuStackHead()
{
    return (FlexMenu *)mMenuStack->GetElementBySlot(0);
}
GalleryMenu *MapManager::GetGalleryMenu()
{
    return mGalleryMenu;
}
AdvCombat *MapManager::GetAdventureCombat()
{
    //--Boots combat if it was not created yet.
    if(!mGlobalAdvCombat)
    {
        mGlobalAdvCombat = GenerateNewCombat();
    }
    return mGlobalAdvCombat;
}
AdventureInventory *MapManager::GetAdventureInventory()
{
    return mAdventureInventory;
}
WorldDialogue *MapManager::GetWorldDialogue()
{
    return mWorldDialogue;
}
StarLinkedList *MapManager::GetCatalystListing()
{
    return mCatalystInfoList;
}

///===================================== Static Functions =========================================
MapManager *MapManager::Fetch()
{
    return Global::Shared()->gMapManager;
}
