//--Base
#include "StarAutoBuffer.h"

//--Classes
//--CoreClasses
#include "StarArray.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
StarAutoBuffer::StarAutoBuffer()
{
    ///--[StarAutoBuffer]
    //--System
    mBufferIsSegmented = false;
    mUse8BitForStringLengths = false;
    mIsDisabled = false;
    mBufferSize = 0;
    SetMemoryData(__FILE__, __LINE__);
    mBuffer = new StarArray();
    mBuffer->SetSegmentationFlag(mBufferIsSegmented);
    //mBuffer = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mBufferSize);

    //--Cursor
    mCursor = 0;
}
StarAutoBuffer::~StarAutoBuffer()
{
    delete mBuffer;
    //free(mBuffer);
}

///===================================== Property Queries =========================================
int StarAutoBuffer::GetSize()
{
    if(mIsDisabled) return 0;
    return mBufferSize;
}
int StarAutoBuffer::GetCursor()
{
    if(mIsDisabled) return 0;
    return mCursor;
}
uint8_t *StarAutoBuffer::GetRawData()
{
    ///--[Documentation]
    //--Returns a uint8_t * pointer, representing the basic data of the autobuffer. Note that this
    //  function will not work if the internal array is segmented, and a warning will be printed.
    //--This is nominally only for reading, as the write pointer is not advanced if the caller changes
    //  the array.
    //--This function can legally return NULL if the array is disabled due to previously liberating the data.
    if(mIsDisabled) return NULL;

    //--Data is segmented, a different call is required.
    if(mBuffer->IsSegmented())
    {
        fprintf(stderr, "StarAutoBuffer:GetRawData() - Cannot get raw data, internal data type is segmented. Failing.\n");
        return NULL;
    }

    //--Data is not segmented. Get the base of the arrays.
    uint8_t **rPageArray = mBuffer->GetMemoryAddress();

    //--If the array is NULL, then no data was actually allocated in the array.
    if(!rPageArray) return NULL;

    //--Return the zeroth page.
    return rPageArray[0];
}
uint8_t *StarAutoBuffer::LiberateData()
{
    ///--[Documentation]
    //--Returns a uint8_t *pointer representing the basic data of the autobuffer, and also deallocates
    //  the rest of the buffer and relinquishes ownership to the caller. The caller can then copy/store
    //  and deallocate the data. This object will then no longer function until it is reinitialized.
    //--This action will fail if the object was already liberated, or if the internal array is segmented.
    if(mIsDisabled) return NULL;

    //--Data is segmented, print an error.
    if(mBuffer->IsSegmented())
    {
        fprintf(stderr, "StarAutoBuffer:LiberateData() - Cannot liberate raw data, internal data type is segmented. Failing.\n");
        return NULL;
    }

    //--Order the internal object to drop pointers to the zeroth array and prepare for deallocation.
    uint8_t *rData = mBuffer->LiberateData();

    //--Clean pointers up, flag as disabled.
    mIsDisabled = true;
    mBufferSize = 0;
    delete mBuffer;
    mBuffer = NULL;

    //--Finish.
    return rData;
}
StarArray *StarAutoBuffer::LiberateArray()
{
    ///--[Documentation]
    //--As above, but liberates the entire StarArray object. This allows data retrieval if the
    //  data is segmented.
    if(mIsDisabled) return NULL;

    //--Store.
    StarArray *rArrayPtr = mBuffer;

    //--Clean.
    mIsDisabled = true;
    mBufferSize = 0;
    mBuffer = NULL;

    //--Finish.
    return rArrayPtr;
}

///======================================== Manipulators ==========================================
void StarAutoBuffer::SetBufferSegmentation(bool pFlag)
{
    ///--[Documentation]
    //--Modifies buffer segmentation state. Cannot occur if the buffer has already allocated memory.
    if(mIsDisabled) return;
    mBuffer->SetSegmentationFlag(pFlag);
    mBufferIsSegmented = mBuffer->IsSegmented();
}
void StarAutoBuffer::Reinitialize()
{
    ///--[Documentation]
    //--If the class has been disabled by a LiberateData() call, it can be restarted by using this
    //  function. If it hasn't, this call will do nothing.
    //--You could ostensibly create a new class to get the same effect, but it may be useful to
    //  re-use the same pointer in a while loop.
    if(!mIsDisabled) return;

    ///--[Execution]
    //--Allocate and reset.
    mIsDisabled = false;
    mBufferSize = 0;
    mBuffer = new StarArray();
    mBuffer->SetSegmentationFlag(mBufferIsSegmented);
    //mBuffer = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mBufferSize);
    mCursor = 0;
}
void StarAutoBuffer::Set8BitsForStringLengths(bool pFlag)
{
    ///--[Documentation]
    //--If this flag is true, a single byte is used for string lengths, otherwise two are. This is
    //  used for file compatibility.
    mUse8BitForStringLengths = pFlag;
}
int StarAutoBuffer::AppendVoidData(void *pPtr, size_t pSize)
{
    ///--[Documentation]
    //--Appends generic data of a generic size. This is memory-safe.
    if(mIsDisabled) return 0;

    //--Length check.
    AllocateMoreSpace(pSize);

    //--Append.
    mBuffer->WriteToArray(pPtr, mCursor, pSize);
    //memcpy(&mBuffer[mCursor], pPtr, pSize);
    mCursor += pSize;

    //--Return the size provided.
    return pSize;
}
int StarAutoBuffer::AppendNull()
{
    ///--[Documentation]
    //--Appends a single null character. Useful with AppendStringWithoutNull() to assemble a very
    //  long string.
    char tNull = 0;
    return AppendVoidData(&tNull, sizeof(char));
}
int StarAutoBuffer::AppendCharacter(char pCharacter)
{
    return AppendVoidData(&pCharacter, sizeof(char));
}
int StarAutoBuffer::AppendInt8(int8_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(int8_t));
}
int StarAutoBuffer::AppendUInt8(uint8_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint8_t));
}
int StarAutoBuffer::AppendInt16(int16_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(int16_t));
}
int StarAutoBuffer::AppendUInt16(uint16_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint16_t));
}
int StarAutoBuffer::AppendInt32(int32_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint32_t));
}
int StarAutoBuffer::AppendUInt32(uint32_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint32_t));
}
int StarAutoBuffer::AppendFloat(float pValue)
{
    return AppendVoidData(&pValue, sizeof(float));
}
int StarAutoBuffer::AppendString(const char *pString)
{
    ///--[Documentation]
    //--Simply appends the given string to the buffer, including the trailing NULL character.
    if(!pString || mIsDisabled) return 0;

    //--Length check.
    int32_t tLen = (int32_t)strlen(pString);
    int32_t tTrueLen = (sizeof(char) * (tLen+1));
    AllocateMoreSpace(tTrueLen);

    //--Append.
    for(int i = 0; i < tLen; i ++)
    {
        mBuffer->WriteToArray(&pString[i], mCursor, sizeof(char));
        //mBuffer[mCursor+0] = pString[i];
        //mBuffer[mCursor+1] = '\0';
        mCursor ++;
    }

    //--Trailing NULL.
    char tNull = '\0';
    mBuffer->WriteToArray(&tNull, mCursor, sizeof(char));
    mCursor ++;

    //--Done.
    return tTrueLen;
}
int StarAutoBuffer::AppendStringWithoutNull(const char *pString)
{
    //--Overload, assumes the below algorithm is executed allowing more than 256 characters.
    return AppendStringWithoutNull(pString, false);
}
int StarAutoBuffer::AppendStringWithoutNull(const char *pString, bool pCantExceed256)
{
    ///--[Documentation]
    //--Appends the string without the trailing NULL. If pCantExceed256 is true, then no more than
    //  256 characters will be written.
    if(!pString || mIsDisabled) return 0;

    //--Can't exceed 256?
    int32_t tLen = (int32_t)strlen(pString);
    if(mUse8BitForStringLengths)
    {
        if(pCantExceed256 && tLen >= 256) tLen = 256;
    }
    //--Use 65536 instead.
    else
    {
        if(pCantExceed256 && tLen >= 65536) tLen = 65536;
    }

    //--Allocate space as necessary.
    AllocateMoreSpace(tLen);

    //--Append.
    for(int i = 0; i < tLen; i ++)
    {
        mBuffer->WriteToArray(&pString[i], mCursor, sizeof(char));
        //mBuffer[mCursor] = pString[i];
        mCursor ++;
    }

    //--Done.
    return tLen;
}
int StarAutoBuffer::AppendStringWithLen(const char *pString)
{
    ///--[Documentation]
    //--Given a string, appends the length of that string as a single byte then the string itself,
    //  not including the trailing NULL. Returns how many letters were added.
    //--Note that this is only for *very short strings*. If more than 256 characters are in the
    //  string, it will lose accuracy since the string header is a single byte. If you need a
    //  very long string that needs a pre-sized length, do it manually.
    if(!pString || mIsDisabled) return 0;

    //--Append the length as the 0th letter. If it's more than 65535, scale it back!
    int32_t tLen = (int32_t)strlen(pString);
    if(mUse8BitForStringLengths)
    {
        if(tLen >= 0xFF) tLen = 0xFF;
        AppendInt8(tLen);
        return AppendStringWithoutNull(pString, true) + (sizeof(uint8_t) * 1);
    }

    //--65536 instead.
    if(tLen >= 0xFFFF) tLen = 0xFFFF;
    AppendInt16(tLen);
    return AppendStringWithoutNull(pString, true) + (sizeof(uint16_t) * 1);
}
int StarAutoBuffer::AppendStringWithLen(const char *pString, const char *pDefault)
{
    //--Overload. If the pString is NULL, the pDefault is used instead. If pDefault is NULL, then
    //  we use a defined internal default.
    if(pString) return AppendStringWithLen(pString);
    if(pDefault) return AppendStringWithLen(pDefault);
    return AppendStringWithLen(SAB_DEFAULT_STRING);
}
void StarAutoBuffer::RemoveBytes(int pAmount)
{
    ///--[Documentation]
    //--Seeks the cursor back and 0's out the data.
    if(pAmount < 1) return;
    if(pAmount > (int)mCursor) pAmount = mCursor;

    //--Clear.
    uint8_t tZeroBuf = 0;
    for(int i = 0; i < pAmount; i ++)
    {
        mBuffer->WriteToArray(&tZeroBuf, mCursor, sizeof(uint8_t));
        mCursor --;
    }
}
void StarAutoBuffer::Seek(int pMove)
{
    //--Note: You cannot seek past the end of the buffer nor can you seek below 0.
    mCursor += pMove;
    if(mCursor < 0) mCursor = 0;
    if(mCursor >= mBufferSize) mCursor = mBufferSize - 1;
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
void StarAutoBuffer::AllocateMoreSpace(int pAddingSize)
{
    ///--[Documentation]
    //--Called at the beginning of each append function, this checks the expected addition against
    //  the current buffer size. If more size is needed, more is added. If not, nothing happens.

    ///--[Size Check]
    //--Check if the actually required space is greater than the allocated space. If not, do nothing.
    uint32_t tNewNeededSize = mCursor + pAddingSize;
    if(tNewNeededSize < mBufferSize) return;

    ///--[Allocate Space]
    //--More space is needed! Append "pages" until the buffer size exceeds the needed size.
    while(mBufferSize <= tNewNeededSize) mBufferSize += SAB_START_SIZE;

    //--Order the array to resize.
    mBuffer->Reallocate(mBufferSize);
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
