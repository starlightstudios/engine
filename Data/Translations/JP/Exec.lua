-- |[ ======================================= Execution ======================================== ]|
--In addition to automated code, this file is executed when a translation is activated in case 
-- additional system setup is needed.

--Inform the LuaManager to look for .luajp files as possible replacements.
LM_SetTranslationExtension(".luajp")
