//--Base
#include "PairanormalDialogue.h"

//--Classes
#include "PairanormalLevel.h"
#include "PairanormalMenu.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

int ExtractString(const char *pString, char *sOutBuffer, int pMaxLetters)
{
    //--Extract a string as delimited by '|' and ']'. Stops at pMaxLetters. Returns how many letters were parsed.
    int tMaxLen = (int)strlen(pString);
    int tCursor = 0;
    int tSlot = 0;

    //--Get the emotion.
    while(pString[tSlot] != ']' && pString[tSlot] != '|')
    {
        //--Range checker.
        if(tSlot >= tMaxLen) return 0;

        //--Copy.
        sOutBuffer[tCursor + 0] = pString[tSlot];
        sOutBuffer[tCursor + 1] = '\0';

        //--Next letter.
        tCursor ++;
        tSlot ++;

        //--Clamp. We keep parsing but stop storing letters.
        if(tCursor >= pMaxLetters-1)
        {
            tCursor = pMaxLetters - 1;
            fprintf(stderr, "== WARNING == ExtractString() passed letter cap of %i\n", pMaxLetters);
        }
    }

    //--Return the letters parsed.
    return tSlot;
}
int ExtractStringBypass(const char *pString, char *sOutBuffer, int pMaxLetters)
{
    //--Identical to ExtractString() but ignores the '|' character. Often used for sound effects.
    int tMaxLen = (int)strlen(pString);
    int tCursor = 0;
    int tSlot = 0;

    //--Get the emotion.
    while(pString[tSlot] != ']')
    {
        //--Range checker.
        if(tSlot >= tMaxLen) return 0;

        //--Copy.
        sOutBuffer[tCursor + 0] = pString[tSlot];
        sOutBuffer[tCursor + 1] = '\0';

        //--Next letter.
        tCursor ++;
        tSlot ++;

        //--Clamp. We keep parsing but stop storing letters.
        if(tCursor >= pMaxLetters-1)
        {
            tCursor = pMaxLetters - 1;
            fprintf(stderr, "== WARNING == ExtractStringBypass() passed letter cap of %i\n", pMaxLetters);
        }
    }

    //--Return the letters parsed.
    return tSlot;
}
void PairanormalDialogue::RegisterTagRemap(const char *pTag, const char *pRemapTo)
{
    //--If the tag already exists, fail.
    void *rCheckPtr = mTagRemapList->GetElementByName(pTag);
    if(rCheckPtr || !pTag || !pRemapTo) return;

    //--Create a string for the remap to go to.
    char *nRemap = InitializeString(pRemapTo);
    mTagRemapList->AddElement(pTag, nRemap, &FreeThis);
}
void PairanormalDialogue::HandleTag(const char *pTag, bool pAllowRemaps)
{
    //--Given a tag in dialogue, which is of format [TAGGOESHERE], performs the action as requested by the tag.
    //  This is done in-stride, when the dialogue reaches the tag.
    if(!pTag) return;

    //--[SOFTBLOCK] will stop dialogue parsing for approximately 30 ticks, independent of text speed.
    if(!strncasecmp(pTag, "[SOFTBLOCK]", 11) || !strncasecmp(pTag, "[P]", 3))
    {
        mAppendNumerator = mAppendSpeed * -15.0f;
        if(PairanormalMenu::xSpeedupFlag) mAppendNumerator = 0.0f;
    }
    //--[BR] activates a hard block. Text stops running until the player presses a key.
    else if(!strncasecmp(pTag, "[BR]", 4))
    {
        mIsHardBlocking = true;
    }
    //--[CHAPTEREND|Imagepath] sets the background.
    else if(!strncasecmp(pTag, "[CHAPTEREND|", 12))
    {
        //--Extract the background's name.
        char tNameBuf[128];
        ExtractString(&pTag[12], tNameBuf, 128);

        //--Set.
        PairanormalLevel *rActiveLevel = PairanormalLevel::Fetch();
        rActiveLevel->ActivateChapterEnd(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[IMAGESCROLL|Imagepath] sets the background.
    else if(!strncasecmp(pTag, "[IMAGESCROLL|", 13))
    {
        //--Extract the background's name.
        char tNameBuf[128];
        ExtractString(&pTag[13], tNameBuf, 128);

        //--Set.
        PairanormalLevel *rActiveLevel = PairanormalLevel::Fetch();
        rActiveLevel->ActivateImageScroll(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[ACTIVATENAMEENTRY]. Brings up name entry mode.
    else if(!strncasecmp(pTag, "[ACTIVATENAMEENTRY]", 19))
    {
        ActivateNameEntry();
    }
    //--[GLITCH] activates a glitch.
    else if(!strncasecmp(pTag, "[GLITCH]", 8))
    {
        //--Timer setup.
        mGlitchTimer = 0;
        mGlitchRunTimer = 16;

        //--Roll a random glitch sound effect and play it.
        int tRoll = rand() % 5;
        char tBuffer[32];
        sprintf(tBuffer, "Glitch|Glitch%02i", tRoll);
        AudioManager::Fetch()->PlaySound(tBuffer);

        //--Subroutine rolls the rest of the glitch effects.
        RollGlitchEffects();
    }
    //--[SHOWCHAR|name|emotion] will show a character in the dialogue.
    else if(!strncasecmp(pTag, "[SHOWCHAR|", 10))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        int tParsed = ExtractString(&pTag[10], tNameBuf, STD_MAX_LETTERS);

        //--Extract the emotion.
        char tEmotionBuf[STD_MAX_LETTERS];
        tEmotionBuf[0] = 'N';
        tEmotionBuf[1] = '\0';
        ExtractString(&pTag[10 + tParsed + 1], tEmotionBuf, STD_MAX_LETTERS);

        //--Show the named character.
        ShowCharacter(tNameBuf, tEmotionBuf);

        //--Flag.
        mReappend = true;
    }
    //--[NAME|Name] changes the name displayed. This allows for spaces.
    else if(!strncasecmp(pTag, "[NAME|", 6))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[6], tNameBuf, STD_MAX_LETTERS);

        //--Show the named character.
        SetSpeakerName(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[FOCUS|name] changes which character is focused.
    else if(!strncasecmp(pTag, "[FOCUS|", 7))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Show the named character.
        FocusCharacter(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[FOCUSMOD|name|true] and [FOCUSMOD|name|false] changes highlighting on a character.
    else if(!strncasecmp(pTag, "[FOCUSMOD|", 10))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        int tParsed = ExtractString(&pTag[10], tNameBuf, STD_MAX_LETTERS);

        //--Extract the boolean
        char tBooleanBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[10 + tParsed + 1], tBooleanBuf, STD_MAX_LETTERS);

        //--Modify.
        HighlightCharacter(tNameBuf, !strcasecmp(tBooleanBuf, "true"));

        //--Flag.
        mReappend = true;
    }
    //--[SOUND|sfxname] will play the given sound.
    else if(!strncasecmp(pTag, "[SOUND|", 7))
    {
        //--Extract the sounds's name.
        char tSFXName[STD_MAX_LETTERS];
        ExtractStringBypass(&pTag[7], tSFXName, STD_MAX_LETTERS);

        //--Play it.
        AudioManager::Fetch()->PlaySound(tSFXName);

        //--Flag.
        mReappend = true;
    }
    //--[SFX|sfxname] will play the given sound, as above.
    else if(!strncasecmp(pTag, "[SFX|", 5))
    {
        //--Extract the sounds's name.
        char tSFXName[STD_MAX_LETTERS];
        ExtractStringBypass(&pTag[5], tSFXName, STD_MAX_LETTERS);

        //--Play it.
        AudioManager::Fetch()->PlaySound(tSFXName);

        //--Special: If the name is "World|Crash" activate the shake effect.
        if(!strcasecmp(tSFXName, "World|Crash"))
        {
            PairanormalLevel::Fetch()->ShakeScreen();
        }
        //--Gunshots do this as well.
        else if(!strcasecmp(tSFXName, "World|Gunshot"))
        {
            PairanormalLevel::Fetch()->ShakeScreen();
        }

        //--Flag.
        mReappend = true;
    }
    //--[HIDECHAR|name] will hide a character in the dialogue.
    else if(!strncasecmp(pTag, "[HIDECHAR|", 10))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[10], tNameBuf, STD_MAX_LETTERS);

        //--Hide the named character.
        HideCharacter(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[EMOTION|name|emotion] change a character's emotion in stride.
    else if(!strncasecmp(pTag, "[EMOTION|", 9))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        int tParsed = ExtractString(&pTag[9], tNameBuf, STD_MAX_LETTERS);

        //--Extract the emotion.
        char tEmotionBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[9 + tParsed + 1], tEmotionBuf, STD_MAX_LETTERS);

        //--Show the named character.
        SetCharacterEmotion(tNameBuf, tEmotionBuf);

        //--Flag.
        mReappend = true;
    }
    //--[E|emotion] is the shorthand. It uses the current speaker instead of a name.
    else if(!strncasecmp(pTag, "[E|", 3))
    {
        //--Extract the emotion.
        char tEmotionBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[3], tEmotionBuf, STD_MAX_LETTERS);

        //--Show the named character.
        SetCharacterEmotion(mSpeakerName, tEmotionBuf);

        //--Flag.
        mReappend = true;
    }
    //--[HIDEALL] hides all characters.
    else if(!strncasecmp(pTag, "[HIDEALL]", 9))
    {
        HideAllCharacters();

        //--Flag.
        mReappend = true;
    }
    //--[HIDEALLFAST] or [HIDEFAST] hides all characters instantly.
    else if(!strncasecmp(pTag, "[HIDEALLFAST]", 13) || !strncasecmp(pTag, "[HIDEFAST]", 10))
    {
        HideAllCharactersFast();

        //--Flag.
        mReappend = true;
    }
    //--[BG|Backgroundpath] sets the background.
    else if(!strncasecmp(pTag, "[BG|", 4))
    {
        //--Extract the background's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[4], tNameBuf, STD_MAX_LETTERS);

        //--Set.
        PairanormalLevel *rActiveLevel = PairanormalLevel::Fetch();
        rActiveLevel->SetBackground(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[IMAGE|Imagepath] activates image mode.
    else if(!strncasecmp(pTag, "[IMAGE|", 7))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Store previous image. Can be NULL.
        mImageFadeTimer = 0;
        rFadingImage = rActiveImage;

        //--Locate the image. Set image display mode active if it's found.
        mIsImageDisplayMode = false;
        rActiveImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(tNameBuf);
        if(rActiveImage)
        {
            mIsImageDisplayMode = true;
        }

        //--Flag.
        mReappend = true;
    }
    //--[HIDEIMAGE] deactivates image mode.
    else if(!strncasecmp(pTag, "[HIDEIMAGE]", 11))
    {
        mIsImageDisplayMode = false;

        //--Flag.
        mReappend = true;
    }
    //--[ZOOM|XXX] sets zoom. 100 = 1.0f zoom.
    else if(!strncasecmp(pTag, "[ZOOM|", 6))
    {
        //--Extract the zoom amount in characters.
        char tZoomBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[6], tZoomBuf, STD_MAX_LETTERS);

        //--Store previous image. Can be NULL.
        mCharacterZoom = atof(tZoomBuf) / 100.0f;

        //--Flag.
        mReappend = true;
    }
    //--[GLITCHTEXT] activates glitchy text when rendering fonts.
    else if(!strncasecmp(pTag, "[GLITCHTEXT]", 12))
    {
        mCurrentTextFlags |= TEXTFLAG_APPLY_GLITCH;
        mReappend = true;
    }
    //--[ENDGLITCHTEXT] deactivates glitchy text when rendering fonts.
    else if(!strncasecmp(pTag, "[ENDGLITCHTEXT]", 15))
    {
        mCurrentTextFlags = (mCurrentTextFlags | TEXTFLAG_APPLY_GLITCH) ^ TEXTFLAG_APPLY_GLITCH;
        mReappend = true;
    }
    //--[SPEEDTEXT] speeds up text ticking.
    else if(!strncasecmp(pTag, "[SPEEDTEXT]", 11))
    {
        mSpeedFactor = 2.5f;
        mReappend = true;
    }
    //--[ENDSPEEDTEXT] renormalizes text ticking.
    else if(!strncasecmp(pTag, "[ENDSPEEDTEXT]", 14))
    {
        mSpeedFactor = 1.0f;
        mReappend = true;
    }
    //--[ITALICS] activates italics for letters rendering after this tag.
    else if(!strncasecmp(pTag, "[ITALICS]", 9))
    {
        mCurrentTextFlags |= TEXTFLAG_APPLY_ITALICS;
        mReappend = true;
    }
    //--[ENDITALICS] ends italic rendering.
    else if(!strncasecmp(pTag, "[ENDITALICS]", 12))
    {
        mCurrentTextFlags = (mCurrentTextFlags | TEXTFLAG_APPLY_ITALICS) ^ TEXTFLAG_APPLY_ITALICS;
        mReappend = true;
    }
    //--[TOMAINMENU] drops everything on the next tick and boots the main menu.
    else if(!strncasecmp(pTag, "[TOMAINMENU]", 12))
    {
        PairanormalLevel::Fetch()->FlagToMainMenu();
    }
    //--[MUSIC|musicname] changes the music, obviously.
    else if(!strncasecmp(pTag, "[MUSIC|", 7))
    {
        //--Extract the music's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Play it.
        AudioManager::Fetch()->PlayMusic(tNameBuf);

        //--Update the music variable.
        SysVar *rMusicVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/LastMusic");
        if(rMusicVar)
        {
            //--Setting music to "NULL" sets it to "No Music".
            if(!strcasecmp(tNameBuf, "Null"))
            {
                ResetString(rMusicVar->mAlpha, "No Music");
            }
            //--Store it.
            else
            {
                ResetString(rMusicVar->mAlpha, tNameBuf);
            }
        }

        //--Flag.
        mReappend = true;
    }
    //--[VOICE|voicename] overrides the voice.
    else if(!strncasecmp(pTag, "[VOICE|", 7))
    {
        //--Extract the voice's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractStringBypass(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Play it.
        strcpy(mVoiceTick, tNameBuf);

        //--Reset flags.
        //mIsSpeakerPlayer = false;
        mIsSpeakerInfo = false;
        mIsSpeakerCredits = false;
        mIsSpeakerThought = false;

        //--Flag.
        mReappend = true;
    }
    //--Remaps. All default tags failed, so check for a tag remap.
    else if(pAllowRemaps)
    {
        //--Scan the remaps list for the given tag.
        const char *rCheckTag = (const char *)mTagRemapList->GetElementByName(pTag);
        if(rCheckTag)
        {
            HandleTag(rCheckTag, false);
        }
        else
        {
            fprintf(stderr, "Warning: Unhandled tag %s\n", pTag);
        }
    }
}
void PairanormalDialogue::HandleTagQuick(const char *pTag, bool pAllowRemaps)
{
    //--Specialized instance, only used when running to checkpoints.
    if(!pTag) return;

    //--[SHOWCHAR|name|emotion] will show a character in the dialogue.
    if(!strncasecmp(pTag, "[SHOWCHAR|", 10))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        int tParsed = ExtractString(&pTag[10], tNameBuf, STD_MAX_LETTERS);

        //--Extract the emotion.
        char tEmotionBuf[STD_MAX_LETTERS];
        tEmotionBuf[0] = 'N';
        tEmotionBuf[1] = '\0';
        ExtractString(&pTag[10 + tParsed + 1], tEmotionBuf, STD_MAX_LETTERS);

        //--Show the named character.
        ShowCharacter(tNameBuf, tEmotionBuf);

        //--Flag.
        mReappend = true;
    }
    //--[FOCUS|name] changes which character is focused.
    else if(!strncasecmp(pTag, "[FOCUS|", 7))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Show the named character.
        FocusCharacter(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[FOCUSMOD|name|true] and [FOCUSMOD|name|false] changes highlighting on a character.
    else if(!strncasecmp(pTag, "[FOCUSMOD|", 10))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        int tParsed = ExtractString(&pTag[10], tNameBuf, STD_MAX_LETTERS);

        //--Extract the boolean
        char tBooleanBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[10 + tParsed + 1], tBooleanBuf, STD_MAX_LETTERS);

        //--Modify.
        HighlightCharacter(tNameBuf, !strcasecmp(tBooleanBuf, "true"));

        //--Flag.
        mReappend = true;
    }
    //--[HIDECHAR|name] will hide a character in the dialogue.
    else if(!strncasecmp(pTag, "[HIDECHAR|", 10))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[10], tNameBuf, STD_MAX_LETTERS);

        //--Hide the named character.
        HideCharacter(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[EMOTION|name|emotion] change a character's emotion in stride.
    else if(!strncasecmp(pTag, "[EMOTION|", 9))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        int tParsed = ExtractString(&pTag[9], tNameBuf, STD_MAX_LETTERS);

        //--Extract the emotion.
        char tEmotionBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[9 + tParsed + 1], tEmotionBuf, STD_MAX_LETTERS);

        //--Show the named character.
        SetCharacterEmotion(tNameBuf, tEmotionBuf);

        //--Flag.
        mReappend = true;
    }
    //--[E|emotion] is the shorthand. It uses the current speaker instead of a name.
    else if(!strncasecmp(pTag, "[E|", 3))
    {
        //--Extract the emotion.
        char tEmotionBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[3], tEmotionBuf, STD_MAX_LETTERS);

        //--Show the named character.
        SetCharacterEmotion(mSpeakerName, tEmotionBuf);

        //--Flag.
        mReappend = true;
    }
    //--[HIDEALL] hides all characters.
    else if(!strncasecmp(pTag, "[HIDEALL]", 9))
    {
        HideAllCharacters();

        //--Flag.
        mReappend = true;
    }
    //--[HIDEALLFAST] or [HIDEFAST] hides all characters instantly.
    else if(!strncasecmp(pTag, "[HIDEALLFAST]", 13) || !strncasecmp(pTag, "[HIDEFAST]", 10))
    {
        HideAllCharactersFast();

        //--Flag.
        mReappend = true;
    }
    //--[BG|Backgroundpath] sets the background.
    else if(!strncasecmp(pTag, "[BG|", 4))
    {
        //--Extract the background's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[4], tNameBuf, STD_MAX_LETTERS);

        //--Set.
        PairanormalLevel *rActiveLevel = PairanormalLevel::Fetch();
        rActiveLevel->SetBackground(tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--[IMAGE|Imagepath] activates image mode.
    else if(!strncasecmp(pTag, "[IMAGE|", 7))
    {
        //--Extract the character's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Store previous image. Can be NULL.
        mImageFadeTimer = 0;
        rFadingImage = rActiveImage;

        //--Locate the image. Set image display mode active if it's found.
        mIsImageDisplayMode = false;
        rActiveImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(tNameBuf);
        if(rActiveImage)
        {
            mIsImageDisplayMode = true;
        }

        //--Flag.
        mReappend = true;
    }
    //--[HIDEIMAGE] deactivates image mode.
    else if(!strncasecmp(pTag, "[HIDEIMAGE]", 11))
    {
        mIsImageDisplayMode = false;

        //--Flag.
        mReappend = true;
    }
    //--[ZOOM|XXX] sets zoom. 100 = 1.0f zoom.
    else if(!strncasecmp(pTag, "[ZOOM|", 6))
    {
        //--Extract the zoom amount in characters.
        char tZoomBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[6], tZoomBuf, STD_MAX_LETTERS);

        //--Store previous image. Can be NULL.
        mCharacterZoom = atof(tZoomBuf) / 100.0f;

        //--Flag.
        mReappend = true;
    }
    //--[MUSIC|musicname] changes the music, obviously.
    else if(!strncasecmp(pTag, "[MUSIC|", 7))
    {
        //--Extract the music's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractString(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Play it.
        AudioManager::Fetch()->PlayMusic(tNameBuf);

        //--Update the music variable.
        SysVar *rMusicVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/LastMusic");
        if(rMusicVar)
        {
            //--Setting music to "NULL" sets it to "No Music".
            if(!strcasecmp(tNameBuf, "Null"))
            {
                ResetString(rMusicVar->mAlpha, "No Music");
            }
            //--Store it.
            else
            {
                ResetString(rMusicVar->mAlpha, tNameBuf);
            }
        }

        //--Flag.
        mReappend = true;
    }
    //--[VOICE|voicename] overrides the voice.
    else if(!strncasecmp(pTag, "[VOICE|", 7))
    {
        //--Extract the voice's name.
        char tNameBuf[STD_MAX_LETTERS];
        ExtractStringBypass(&pTag[7], tNameBuf, STD_MAX_LETTERS);

        //--Play it.
        strcpy(mVoiceTick, tNameBuf);

        //--Flag.
        mReappend = true;
    }
    //--Remaps. All default tags failed, so check for a tag remap.
    else if(pAllowRemaps)
    {
        //--Scan the remaps list for the given tag.
        const char *rCheckTag = (const char *)mTagRemapList->GetElementByName(pTag);
        if(rCheckTag)
        {
            HandleTagQuick(rCheckTag, false);
        }
        else
        {
            //fprintf(stderr, "Warning: Unhandled quick tag %s\n", pTag);
        }
    }
}
