///===================================== Standard Program =========================================
//--The "Standard" main() and related functions, used if your project doesn't need to do anything
//  special during these calls.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
///========================================= Functions ============================================
int StandardMain(int pArgsTotal, char **pArgs);
void LogFPS(MainPackage &sMainPackage);
