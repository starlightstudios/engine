///====================================== StarLinkedList ==========================================
//--General-purpose linked list, built for the Starlight engine.
//--Features:
//  Iterator handling
//  Automatic garbage handling through deletion pointers
//  Random-pointer allows for easier pruning
//  Does not rely on the STL

//--Notes:
//--The Iterator stack is not 'stable' with the Random pointer. Ideally, do not delete components
//  if there is an iterator on the stack. If you need to prune something, do it at the end of a
//  'tick' when nothing is accessing it. In addition, do not add components while the list is
//  iterating as it may or may not respect them depending on where they were added.
//--This will eventually be handled by adding a pending addition and deletion list.

#pragma once

///========================================== Includes ============================================
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

///===================================== Local Definitions ========================================
//--Deletion Function Pointer is a standardized pointer type used to delete objects or free them.
//  It should be declared elsewhere in the program surrounded by these #ifdef braces.
#ifndef _DeletionFunctionPtr_
#define _DeletionFunctionPtr_
typedef void(*DeletionFunctionPtr)(void *);
#endif

//--Sorting function that allows the user to customize sorting these linked lists. Functionality is
//  provided for sorting by name.
#ifndef _SortFunctionPtr_
#define _SortFunctionPtr_
typedef int(*SortFnPtr)(const void *, const void *);
#endif

//--Function for customizing how strings are sorted, such as by switching case-sensitivity.
typedef int(*StringCompareFuncPtr)(const char *, const char *);

///===================================== Local Structures =========================================
///--[StarLinkedListEntry]
//--An entry on the linked list, contains a name, double-link pointers, and a block of data. Also contains
//  a deletion function that will be called on the data pointer when the block is deallocated.
//--The "Liberate" functions explicitly do not call deallocation on the data block, the "Remove" functions
//  do, and are used if the data is no longer needed.
typedef struct StarLinkedListEntry
{
    char *mName;
    StarLinkedListEntry *rNext;
    StarLinkedListEntry *rPrev;
    void *rData;
    void (*rDeletionFunc)(void *);
}StarLinkedListEntry;

///--[SLLIteratorEntry]
//--The list allows a stack of iterators to move across the list without interfering with one another. This
//  is a member of that stack, storing which entry it is on and the next element in the stack to allow push/pop.
//--Because this points to an entry, the order of entries can be shuffled without breaking the iterators though
//  this may cause elements to be skipped or double-counted. Deleting an element with an iterator pointing to
//  it will most likely cause a crash.
typedef struct SLLIteratorEntry
{
    StarLinkedListEntry *rCurrentEntry;
    SLLIteratorEntry *rNextStackObject;
}SLLIteratorEntry;

///========================================== Classes =============================================
class StarLinkedList
{
    private:
    //--List Handling
    StarLinkedListEntry *mListHead;
    StarLinkedListEntry *mListTail;
    StarLinkedListEntry *mListCurrent;

    //--List Properties
    int mListSize;
    bool mHandlesDeallocation;
    StringCompareFuncPtr rStringCompareFuncPtr;

    //--Iterator Stack
    int mStackSize;
    SLLIteratorEntry *mIteratorStack;

    //--Debug
    bool mDebugFlag;

    public:
    //--System
    StarLinkedList(bool pHandleDeallocate);
    ~StarLinkedList();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    int GetListSize();
    void *GetHead();
    void *GetTail();
    void *GetEntryByRandomRoll();

    //--Manipulators
    void SetDebugFlag(bool pFlag);
    void SetDeallocation(bool pFlag);
    void SetCaseSensitivity(bool pFlag);

    ///--Sorting
    static int CompareEntries(const void *pEntryA, const void *pEntryB);
    static int CompareEntriesRev(const void *pEntryA, const void *pEntryB);
    void SortList(bool pReverseFlag);
    void SortListUsing(SortFnPtr pSortFunction);

    //--Core Methods
    void *DeleteHead();
    void *DeleteTail();
    void ClearList();
    void *DeleteEntry(StarLinkedListEntry *pEntry);
    void CloneToList(StarLinkedList *pList);

    //--Private Core Methods
    void *DeleteWholeList();
    void DecrementList();

    public:

    ///--Additions
    static StarLinkedListEntry *AllocateEntry(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElement(const char *pName, void *pElement);
    void *AddElement(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementAsHead(const char *pName, void *pElement);
    void *AddElementAsHead(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementAsTail(const char *pName, void *pElement);
    void *AddElementAsTail(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementInSlot(const char *pName, void *pElement, int pSlot);
    void *AddElementInSlot(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr, int pSlot);

    ///--Iterator Handling
    void *PushIterator();
    void *Iterate();
    void *AutoIterate();
    char *GetIteratorName();
    void PopIterator();
    void ClearStack();

    ///--Random Pointer Handling
    void SetRandomPointerToHead();
    void SetRandomPointerToTail();
    bool IncrementRandomPointer();
    bool DecrementRandomPointer();
    void *GetRandomPointerEntry();
    char *GetRandomPointerName();
    void *RemoveRandomPointerEntry();
    void *LiberateRandomPointerEntry();
    void *SetToHeadAndReturn();
    void *SetToTailAndReturn();
    void *IncrementAndGetRandomPointerEntry();
    void *DecrementAndGetRandomPointerEntry();
    bool SetRandomPointerToThis(void *pPtr);
    StarLinkedListEntry *GetRandomPointerWholeEntry();

    ///--Removal
    void *RemoveElementS(const char *pSearch);
    void *RemoveElementI(int slot);
    bool RemoveElementP(void *pPtr);
    void MoveEntryTowardsHeadS(const char *pName);
    void MoveEntryTowardsHeadI(int pIndex);
    void MoveEntryTowardsTailS(const char *pName);
    void MoveEntryTowardsTailI(int pIndex);

    ///--Searches (StarLinkedListSearches.cc)
    const char *GetNameOfElementBySlot(int pSlot);
    int GetSlotOfElementByName(const char *pName);
    int GetSlotOfElementByPtr(void *pPtr);
    void *GetElementByName(const char *pSearch);
    void *GetElementByPartialName(const char *pSearch);
    void *GetElementBySlot(int slot);
    bool IsElementOnList(void *pPtr);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

