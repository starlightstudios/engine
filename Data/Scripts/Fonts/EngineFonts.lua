-- |[ ======================================= Engine Fonts ====================================== ]|
--Boots all the fonts used by the core engine. This is called by the c++ program immediately after
-- Freetype is initialized.
if(gbBootedCoreFonts == true) then return end
gbBootedCoreFonts = true

-- |[Replacement]|
--If this is variable is not nil, call this script instead of the current one. This replacement is
-- typically used by translations that need alternate fonts for different character sets.
if(gsEngineFontPathReplace ~= nil) then
    LM_ExecuteScript(gsEngineFontPathReplace)
    return
end

-- |[Setup]|
--Debug timer.
local fStartTime = 0.0
LM_StartTimer("Fonts")

--Paths.
local sFontPath    = "Data/Scripts/Fonts/"
local sKerningPath = "Data/Scripts/Fonts/"
local sHasNoKerningFile = "Null"

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest    = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge       = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade   = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS   = Font_GetProperty("Constant Precache With Special S")
local ciFontSpecialExc = Font_GetProperty("Constant Precache With Special !")
local ciFontUnicode    = Font_GetProperty("Constant Precache Full Unicode")
local ciFontDummy      = Font_GetProperty("Constant Precache Dummy")
local ciFontSpecialDbg = Font_GetProperty("Constant Precache Debug")

-- |[Precompilation Handling]|
local iCompileFlag = 2
if(iCompileFlag == 1) then
    StarFont_BeginCompilation("Data/FontPrebuild/EngineFontPrecompileEN.slf")
elseif(iCompileFlag == 2) then
    StarFont_OpenCompiledFile("Data/FontPrebuild/EngineFontPrecompileEN.slf")
end

-- |[ =============================== Title Screen Redirections ================================ ]|
-- |[Witch Hunter Izana]|
--If the game is mandating WHI's title screen, then several fonts get switched to dummy mode. This
-- reduces overhead as the fonts will not precache or upload data. If Adventure is booted after
-- this, it will cause a lot of errors. So don't do that.
if(gsMandateTitle == "Witch Hunter Izana") then
    Font_RegisterAsDummy("Sanchez 35 DFO")
    Font_RegisterAsDummy("Sanchez 22 DFO")
end

-- |[ ==================================== Font Registration =================================== ]|
-- |[Sanchez]|
--Sanchez
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 35 DFO", sFontPath .. "SanchezRegular.otf", sHasNoKerningFile, 35, ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Sanchez 22 DFO", sFontPath .. "SanchezRegular.otf", sHasNoKerningFile, 22,  ciFontEdge + ciFontDownfade)
    
-- |[General Alias Register]|
--Sanchez 35
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "String Entry Header")

--Sanchez 22
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "String Entry Main")

-- |[Game-Specific Title Screens]|
--Playing Witch Hunter Izana: Load these fonts for the title screen.
if(gsMandateTitle == "Witch Hunter Izana") then

    --Font Register
    Font_Register("Immortal 60", sFontPath .. "IMMORTAL.ttf",           sHasNoKerningFile, 60, ciFontNoFlags)
    Font_Register("Medieval 36", sFontPath .. "MedievalSharp-Book.ttf", sHasNoKerningFile, 36, ciFontNoFlags)
    Font_Register("Garamond 30", sFontPath .. "EBGaramond-Regular.ttf", sHasNoKerningFile, 30, ciFontNoFlags)
    Font_Register("Garamond 20", sFontPath .. "EBGaramond-Regular.ttf", sHasNoKerningFile, 20, ciFontNoFlags)
    
    --Aliases
    Font_SetProperty("Add Alias", "Immortal 60", "Monoceros Title Double Header")
    Font_SetProperty("Add Alias", "Immortal 60", "Monoceros Menu Option Double Header")
    Font_SetProperty("Add Alias", "Immortal 60", "Mono String Entry Header")
    Font_SetProperty("Add Alias", "Medieval 36", "Monoceros Title Option")
    Font_SetProperty("Add Alias", "Medieval 36", "Monoceros Menu Option Header")
    Font_SetProperty("Add Alias", "Medieval 36", "Mono String Entry Current")
    Font_SetProperty("Add Alias", "Garamond 30", "Monoceros Menu Option Help")
    Font_SetProperty("Add Alias", "Garamond 30", "Mono String Entry Mainline")
    Font_SetProperty("Add Alias", "Garamond 20", "Monoceros Standard Control")
    Font_SetProperty("Add Alias", "Garamond 20", "Monoceros Title Statistic")
    Font_SetProperty("Add Alias", "Garamond 20", "Monoceros Menu Option Mainline")

end

--Playing Witches of Ravenbrook: Load these fonts for the title screen.
if(gsMandateTitle == "Witches of Ravenbrook") then
    
    --Locate the Chapter H implementation.
    local sCarnationPath = nil
    for i = 1, #gsModDirectories, 1 do
        
        --Break the string up and get the segment before the last slash.
        local iStringLen = string.len(gsModDirectories[i])
        local sLastPart = string.sub(gsModDirectories[i], iStringLen-9, iStringLen-1)
        if(sLastPart == "Chapter H") then
            sCarnationPath = gsModDirectories[i]
            break
        end
        
    end
    
    --Carnation directory found. Run its font boot.
    if(sCarnationPath ~= nil) then
        LM_ExecuteScript(sCarnationPath .. "Fonts/001 Engine Fonts.lua")
    end
end

-- |[ ===================================== System Aliases ====================================== ]|
--The fonts "System Font", "Bitmap Font", and "Bitmap Font Small" are system fonts guaranteed to exist.
-- We can register aliases for them here.

--System Font
Font_SetProperty("Add Alias", "System Font", "Virtual Console Main")

--Bitmap Font
Font_SetProperty("Add Alias", "Bitmap Font", "Adventure Debug Font")
Font_SetProperty("Add Alias", "Bitmap Font", "Corrupter Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Flex Button Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Flex Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Gallery Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Classic Level Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Classic Level Damage")
Font_SetProperty("Add Alias", "Bitmap Font", "Zone Editor Main")

--Bitmap Font Small
Font_SetProperty("Add Alias", "Bitmap Font Small", "Corrupter Menu Small")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Blue Sphere Main")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Context Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Classic Level Small")

-- |[ ==================================== End Compilation ===================================== ]|
if(iCompileFlag == 1) then
    StarFont_FinishCompilation()
elseif(iCompileFlag == 2) then
    StarFont_OpenCompiledFile("Null")
end

local fEndTime = LM_CheckTimer("Fonts")
io.write(string.format("%6.4f: Engine Font Loading\n", fEndTime - fStartTime))
LM_FinishTimer("Fonts")