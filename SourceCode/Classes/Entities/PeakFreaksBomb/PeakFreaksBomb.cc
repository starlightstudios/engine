//--Base
#include "PeakFreaksBomb.h"

//--Classes
#include "TileLayer.h"
#include "PeakFreaksLevel.h"
#include "PeakFreaksPlayer.h"

//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"

///========================================== System ==============================================
PeakFreaksBomb::PeakFreaksBomb()
{
    ///--[System]
    mSubtype = PFBOMB_SUBTYPE_GUANO;
    mTimer = 0;

    ///--[Position]
    mTileX = 0;
    mTileY = 0;
    mHeight = 0.0f;
    mYSpeed = 0.0f;
    mGravity = 0.05f;

    ///--[Images]
    rFlightImg = NULL;
    rTargetImg = NULL;
    memset(rExplosionImg, 0, sizeof(void *) * 4);
}
PeakFreaksBomb::~PeakFreaksBomb()
{

}
void PeakFreaksBomb::DeleteThis(void *pPtr)
{
    PeakFreaksBomb *rPtr = (PeakFreaksBomb *)pPtr;
    delete rPtr;
}

///===================================== Property Queries =========================================
bool PeakFreaksBomb::IsComplete()
{
    //--Guanos last 10 seconds:
    if(mSubtype == PFBOMB_SUBTYPE_GUANO)
    {
        if(mTimer >= PFGUANO_PERSIST_TICKS) return true;
    }
    //--Other bombs last 2 seconds:
    else
    {
        if(mTimer >= PFBOMB_PERSIST_TICKS) return true;
    }
    return false;
}

///======================================= Manipulators ===========================================
void PeakFreaksBomb::SetSubtype(int pSubtype)
{
    mSubtype = pSubtype;
}
void PeakFreaksBomb::SetTilePosition(int pTileX, int pTileY)
{
    mTileX = pTileX;
    mTileY = pTileY;
}
void PeakFreaksBomb::SetHeight(float pHeight)
{
    mHeight = pHeight;
}
void PeakFreaksBomb::SetFlightImg(SugarBitmap *pImg)
{
    rFlightImg = pImg;
}
void PeakFreaksBomb::SetTargetImg(SugarBitmap *pImg)
{
    rTargetImg = pImg;
}
void PeakFreaksBomb::SetExplosionImg(int pSlot, SugarBitmap *pImg)
{
    if(pSlot < 0 || pSlot >= 4) return;
    rExplosionImg[pSlot] = pImg;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PeakFreaksBomb::Update()
{
    ///--[Airborne]
    //--Bomb is still in the air. Move it downward.
    if(mHeight > 0)
    {
        //--SFX.
        if(!mHasPlayedFallSFX && mHeight < mYSpeed * 2.0f)
        {
            mHasPlayedFallSFX = true;
            AudioManager::Fetch()->PlaySound("PF Powerup GuanoDrop");
        }

        //--Move.
        mHeight = mHeight - mYSpeed;
        mYSpeed = mYSpeed + mGravity;

        //--On the tick we hit the ground:
        if(mHeight <= 0.0f)
        {
            mHeight = 0.0f;
        }
    }
    ///--[Exploded]
    //--Begin running the timer. This handles the explosion, if applicable.
    else
    {
        //--SFX.
        if(mTimer == 0)
        {
            if(mSubtype == PFBOMB_SUBTYPE_BOMB || mSubtype == PFBOMB_SUBTYPE_SUPERBOMB)
            {
                AudioManager::Fetch()->PlaySound("PF Powerup DroneStrike");
            }
            //--Guano: Ewwww.
            else
            {
                AudioManager::Fetch()->PlaySound("PF Powerup GuanoSplat");
            }
        }

        //--Timer.
        mTimer ++;

        //--Check for a player collision:
        PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
        if(!rActiveLevel) return;

        //--Get player.
        PeakFreaksPlayer *rPlayer = rActiveLevel->GetPlayer();
        if(rPlayer)
        {
            //--Check for a collision.
            int tPlayerTileX = rPlayer->GetWorldX();
            int tPlayerTileY = rPlayer->GetWorldY();
            if(tPlayerTileX == mTileX && tPlayerTileY == mTileY)
            {
                //--Bombs: Injure the player.
                if(mSubtype == PFBOMB_SUBTYPE_BOMB || mSubtype == PFBOMB_SUBTYPE_SUPERBOMB)
                {
                    rPlayer->Injure();
                }
                //--Guano: Ewwww.
                else
                {
                    rPlayer->Guano();
                }
            }
        }

        //--Get rival.
        PeakFreaksPlayer *rRival = rActiveLevel->GetRival();
        if(rRival)
        {
            //--Check for a collision.
            int tPlayerTileX = rRival->GetWorldX() / TileLayer::cxSizePerTile;
            int tPlayerTileY = rRival->GetWorldY() / TileLayer::cxSizePerTile;
            if(tPlayerTileX == mTileX && tPlayerTileY == mTileY)
            {
                //--Bombs: Injure the player.
                if(mSubtype == PFBOMB_SUBTYPE_BOMB || mSubtype == PFBOMB_SUBTYPE_SUPERBOMB)
                {
                    rRival->Injure();
                }
                //--Guano: Ewwww.
                else
                {
                    rRival->Guano();
                }
            }
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void PeakFreaksBomb::Render()
{
    ///--[Airborne]
    //--Bomb is still in the air. Place a target on the destination, bomb in the air.
    if(mHeight > 0)
    {
        //--Target.
        if(rTargetImg)
        {
            rTargetImg->Draw(mTileX * 16.0f, mTileY * 16.0f);
        }

        //--Projectile.
        if(rFlightImg)
        {
            rFlightImg->Draw(mTileX * 16.0f, mTileY * 16.0f - mHeight);
        }
    }
    ///--[Exploded]
    //--Exploding.
    else
    {
        //--Guano always shows the same frame.
        if(mSubtype == PFBOMB_SUBTYPE_GUANO)
        {
            if(rExplosionImg[0]) rExplosionImg[0]->Draw(mTileX * 16.0f, mTileY * 16.0f);
        }
        //--Bombs explode.
        else
        {
            int tFrame = mTimer / PFBOMB_TPF;
            if(tFrame >= 0 && tFrame < 4 && rExplosionImg[tFrame]) rExplosionImg[tFrame]->Draw(mTileX * 16.0f, mTileY * 16.0f);
        }

    }
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
