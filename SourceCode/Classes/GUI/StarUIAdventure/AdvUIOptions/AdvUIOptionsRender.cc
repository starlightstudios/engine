//--Base
#include "AdvUIOptions.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

///====================================== Segment Drawing =========================================
void AdvUIOptions::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Most UIs do a standard setup then render left/top/right/bottom/help in order, then stop. This function
    //  does that and is called from RenderForeground() and RenderBackground(). It can be overridden if a different
    //  order is needed or there are more sub-components that need rendering. It can also be bypassed entirely
    //  by overriding RenderForeground().
    //--By default, objects slide in from the screen edges.

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Unsaved Changes Overlay]
    if(mUnsavedChangesTimer > 0)
    {
        //--Percentage.
        float cUnsavedPercent = EasingFunction::QuadraticInOut(mUnsavedChangesTimer, ADVMENU_OPTIONS_UNSAVED_CHANGES_TICKS);

        //--Offsets.
        float cTopOffset = VIRTUAL_CANVAS_Y * (1.0f - cUnsavedPercent) * -1.0f;

        //--Darken everything behind this UI.
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cUnsavedPercent * 0.7f));

        //--Slides in from the top of the screen.
        glTranslatef(0.0f, cTopOffset * 1.0f, 0.0f);

        //--Frame.
        AdvImages.rUnsavedChangesFrame->Draw();

        //--String.
        AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 180.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "You have unsaved changes pending.");
        AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 215.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Really exit?");
        mAcceptString->DrawText(410.0f, 280.0f, 0, 1.0f, AdvImages.rFont_Mainline);
        mCancelString->DrawText(955.0f, 280.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

        //--Clean.
        glTranslatef(0.0f, cTopOffset * -1.0f, 0.0f);
    }

    ///--[Rebinding Controls Overlay]
    if(mRebindTimer > 0)
    {
        //--Percentage.
        float cRebindPercent = EasingFunction::QuadraticInOut(mRebindTimer, ADVMENU_OPTIONS_REBIND_TICKS);

        //--Offsets.
        float cTopOffset = VIRTUAL_CANVAS_Y * (1.0f - cRebindPercent) * -1.0f;

        //--Darken everything behind this UI.
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cRebindPercent * 0.7f));

        //--Slides in from the top of the screen.
        glTranslatef(0.0f, cTopOffset * 1.0f, 0.0f);

        //--Frame.
        AdvImages.rUnsavedChangesFrame->Draw();

        //--Text.
        if(!mIsRebindingSecondary)
        {
            AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 180.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Press new primary key now.");
        }
        else
        {
            AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 180.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Press new secondary key now.");
        }

        //--Clean.
        glTranslatef(0.0f, cTopOffset * -1.0f, 0.0f);
    }

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);
}
void AdvUIOptions::RenderLft(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Only the top UI renders on this object. It slides in from the top of the screen.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);

    ///--[Rendering]
    //--Left-side arrow.
    float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
    AdvImages.rArrowLft->Draw(cArrowOscillate * -1.0f, 0.0f);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIOptions::RenderTop(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Only the top UI renders on this object. It slides in from the top of the screen.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Rendering]
    //--Header.
    AdvImages.rHeader->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Settings");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Control strings
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mInspectString->DrawText (0.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--In the controls menu, render these.
    if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        mBindPrimaryString->DrawText  (0.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mBindSecondaryString->DrawText(0.0f, cxAdvMainlineFontH * 3.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mSetToDefaultString->DrawText (0.0f, cxAdvMainlineFontH * 4.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mSetAllDefaultString->DrawText(0.0f, cxAdvMainlineFontH * 5.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIOptions::RenderRgt(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Only the top UI renders on this object. It slides in from the top of the screen.
    AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Rendering]
    //--Right-side arrow.
    float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;
    AdvImages.rArrowRgt->Draw(cArrowOscillate * 1.0f, 0.0f);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIOptions::RenderBot(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Only the top UI renders on this object. It slides in from the top of the screen.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Rendering]
    //--Frame. Does not change based on option mode.
    AdvImages.rFrame->Draw();

    //--Mode-based rendering.
    if(mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
        RenderOptionsGameplay(cColorAlpha);
    else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)
        RenderOptionsAudio(cColorAlpha);
    else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
        RenderOptionsControls(cColorAlpha);
    else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)
        RenderOptionsDebug(cColorAlpha);

    //--Defaults Button
    if(mMode != ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        AdvImages.rButtonDefaults->Draw();
        AdvImages.rFont_Heading->DrawText(839.0f, 692.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Defaults");
    }

    //--Okay Button
    AdvImages.rButtonOkay->Draw();
    AdvImages.rFont_Heading->DrawText(525.0f, 692.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Okay");

    //--Highlight.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Unsaved changes text.
    if(mHasUnsavedChanges)
    {
        AdvImages.rFont_Mainline->DrawText(VIRTUAL_CANVAS_X * 0.50f, 633.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Changes Pending...");
    }

    //--Changes require restart text.
    if(mRequiresRestart)
    {
        AdvImages.rFont_Mainline->DrawText(VIRTUAL_CANVAS_X * 0.50f, 658.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Some Changes Require A Program Restart.");
    }

    //--Control Strings.
    float cYPos = VIRTUAL_CANVAS_Y - cxAdvMainlineFontH;
    mPageLftString->DrawText(            0.0f, cYPos,                          SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mPageRgtString->DrawText(VIRTUAL_CANVAS_X, cYPos, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Details. Renders over everything else in this block.
    RenderDetails();

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///================================== No-Blocking Subroutines =====================================
void AdvUIOptions::RenderOptionsGameplay(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the gameplay set.

    ///--[Header]
    //--Static.
    AdvImages.rOptionHeader->Draw();

    //--Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(412.0f, 141.0f, 0, 1.0f, "Gameplay");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Options Rendering]
    //--Boolean set.
    RenderBoolean("Auto-Use Doctor Bag",             (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_DOCTORBAG)),                 mAutoUseDoctorBag.mCurValue.b);
    RenderBoolean("Auto-Hasten Dialogue",            (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_HASTEN)),                    mAutoHastenDialogue.mCurValue.b);
    RenderBoolean("Tourist Mode",                    (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_TOURISTMODE)),               mTouristMode.mCurValue.b);
    RenderBoolean("Combat Memory Cursor",            (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)),              mCombatMemoryCursor.mCurValue.b);
    RenderFloat(  "Ambient Light Boost",             (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_LIGHTBOOST)),                mAmbientLightBoost.mCurValue.f);
    RenderBoolean("Show Censor Bars",                (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_SHOWCENSORBARS)),            mShowCensorBars.mCurValue.b);
    RenderInteger("Max Autosaves",                   (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)),              mMaxAutosaves.mCurValue.i, 100);
    RenderBoolean("Allow Asset Streaming",           (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)),       mAllowAssetStreaming.mCurValue.b);
    RenderBoolean("Use RAM Loading",                 (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_USERAMLOADING)),             mUseRAMLoading.mCurValue.b);
    RenderBoolean("Unload Actors After Dialogue",    (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)), mUnloadActors.mCurValue.b);
    RenderBoolean("Use Fading UI",                   (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_USEFADINGUI)),               mUseFadingUI.mCurValue.b);
    RenderBoolean("Expert Combat UI",                (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_EXPERTCOMBATUI)),            mExpertCombatUI.mCurValue.b);
    RenderBoolean("Puzzle Blocks Must Be Activated", (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_BLOCKSMUSTACTIVATE)),        mBlocksMustActivate.mCurValue.b);
    RenderBoolean("Show Stamina Bar",                (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_SHOWSTAMINABAR)),            mShowStaminaBar.mCurValue.b);
    RenderBoolean("Disallow Map Overlays",           (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_DISALLOW_OVERLAYS)),         mDisallowOverlays.mCurValue.b);
    RenderBoolean("Autorun",                         (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_GAMEPLAY_AUTORUN)),                   mAutorun.mCurValue.b);
}
void AdvUIOptions::RenderOptionsAudio(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the audio set.

    ///--[Header]
    //--Static.
    AdvImages.rOptionHeader->Draw();

    //--Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(412.0f, 141.0f, 0, 1.0f, "Audio");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Options Rendering]
    //--Resolve nearest-to-5 so the sound/music volumes don't look silly.
    float tMusicVolumeDiv5 = (((int)round(mMusicVol.mCurValue.f * 20.0f)) / 20.0f) + 0.005f;
    float tSoundVolumeDiv5 = (((int)round(mSoundVol.mCurValue.f * 20.0f)) / 20.0f) + 0.005f;

    //--Set.
    RenderFloat(  "Music Volume",             (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_MUSIC_VOLUME)),         tMusicVolumeDiv5);
    RenderFloat(  "Sound Volume",             (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_SOUND_VOLUME)),         tSoundVolumeDiv5);
    //RenderBoolean("Save Volumes In Savefile", (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_SAVEVALUESINSAVEFILE)), mSaveVolumesInSavefile.mCurValue.b);

    //--String values for play chance.
    if(true)
    {
        //--Variables.
        int tValue = mVoicePlay.mCurValue.i;
        float tY = (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_VOICELINE_PLAY_CHANCE));

        //--Name.
        AdvImages.rFont_Mainline->DrawText(cNameLft, tY + 0.0f, 0, 1.0f, "Voice Play Chance");

        //--String.
        const char *rString = "Test Value";
        if(tValue == 0)
        {
            rString = "Normal";
        }
        else if(tValue == 1)
        {
            rString = "2/3";
        }
        else if(tValue == 2)
        {
            rString = "Half";
        }
        else if(tValue == 3)
        {
            rString = "1/3";
        }
        else if(tValue == 4)
        {
            rString = "1/4";
        }
        else if(tValue == 5)
        {
            rString = "1/8";
        }
        else if(tValue == 6)
        {
            rString = "Never";
        }
        else if(tValue == 7)
        {
            rString = "Double";
        }

        //--Render string.
        AdvImages.rFont_Mainline->DrawText(cBoolRgt, tY + 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, rString);

        //--Divider.
        AdvImages.rFrameDetail->Draw(398.0f, tY + cOptionHei - 6.0f);
    }

    //--Lookups.
    AdvMenuOptionPack *trPackList[ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY];
    trPackList[0] = &mAbstractVol0;
    trPackList[1] = &mAbstractVol1;
    trPackList[2] = &mAbstractVol2;
    trPackList[3] = &mAbstractVol3;

    //--Render these four character strings. If a slot is empty, then the string uses a default.
    for(int i = 0; i < ADVMENU_OPTIONS_MAX_CHARS_IN_PARTY; i ++)
    {
        //--Empty slot.
        if(mAbstractVolumeCurrentNames[i][0] == '\0')
        {
            RenderFloat("Unused Voice Volume", (cOptionTop + (cOptionHei * (ADVMENU_OPTIONS_CHARVOLUME0+i))), trPackList[i]->mCurValue.f);
        }
        //--Occupied.
        else
        {
            //--Get volume to nearest 5.
            float tVolumeTo5 = (((int)round(trPackList[i]->mCurValue.f * 20.0f)) / 20.0f) + 0.005f;

            //--Name to display.
            char tBuffer[128];
            sprintf(tBuffer, "%s's Voice Volume", mAbstractVolumeCurrentNames[i]);
            RenderFloat(tBuffer, (cOptionTop + (cOptionHei * (ADVMENU_OPTIONS_CHARVOLUME0+i))), tVolumeTo5);
        }
    }
}
void AdvUIOptions::RenderOptionsControls(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the control set.

    ///--[Header]
    //--Static.
    AdvImages.rOptionHeader->Draw();

    //--Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(412.0f, 141.0f, 0, 1.0f, "Controls");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Options Rendering]
    //--List of controls.
    RenderControl("Activate/Accept",    (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_ACTIVATE)),       "Activate");
    RenderControl("Cancel/Open Menu",   (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_CANCEL)),         "Cancel");
    RenderControl("Run",                (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_RUN)),            "Run");
    RenderControl("Up",                 (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_UP)),             "Up");
    RenderControl("Right",              (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_RIGHT)),          "Right");
    RenderControl("Down",               (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_DOWN)),           "Down");
    RenderControl("Left",               (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_LEFT)),           "Left");
    RenderControl("Shoulder Left",      (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_SHOULDERLEFT)),   "UpLevel");
    RenderControl("Shoulder Right",     (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)),  "DnLevel");
    RenderControl("Field Ability Menu", (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_FIELDABILITIES)), "OpenFieldAbilityMenu");
    RenderControl("Ability 1",          (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_ABILITY_1)),      "FieldAbility0");
    RenderControl("Ability 2",          (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_ABILITY_2)),      "FieldAbility1");
    RenderControl("Ability 3",          (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_ABILITY_3)),      "FieldAbility2");
    RenderControl("Ability 4",          (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_ABILITY_4)),      "FieldAbility3");
    RenderControl("Ability 5",          (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_CONTROLS_ABILITY_5)),      "FieldAbility4");

    //--Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Mainline->DrawText(404.0f, 600.0f, 0, 1.0f, "Controls are saved immediately when rebound.");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void AdvUIOptions::RenderOptionsDebug(float pAlpha)
{
    ///--[Documentation]
    //--Renders the options block for the control set.

    ///--[Header]
    //--Static.
    AdvImages.rOptionHeader->Draw();

    //--Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Heading->DrawText(412.0f, 141.0f, 0, 1.0f, "Debug");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Options Rendering]
    //--Controls
    RenderControl("Open Debug Menu",            (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_DEBUG_OPENMENU)),          "OpenDebug");
    RenderControl("Toggle Fullscreen",          (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_DEBUG_TOGGLEFULLSCREEN)),   "ToggleFullscreen");
    RenderControl("Rebuild Kerning",            (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_DEBUG_REBUILDKERNING)),     "RebuildKerning");
    RenderControl("Rebuild Shaders",            (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_DEBUG_REBUILDSHADERS)),     "RecompileShaders");
    RenderBoolean("Disable Shader: Outline",    (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_DEBUG_DISABLE_OUTLINE)),    mDisableShaderOutline.mCurValue.b);
    RenderBoolean("Disable Shader: Underwater", (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER)), mDisableShaderUnderwater.mCurValue.b);
    RenderBoolean("Disable Shader: Lighting",   (cOptionTop + (cOptionHei * ADVMENU_OPTIONS_DEBUG_DISABLE_LIGHTING)),   mDisableShaderLighting.mCurValue.b);

    //--Text.
    mColorHeading.SetAsMixerAlpha(pAlpha);
    AdvImages.rFont_Mainline->DrawText(404.0f, 575.0f, 0, 1.0f, "Debug controls may not be rebound.");
    AdvImages.rFont_Mainline->DrawText(404.0f, 600.0f, 0, 1.0f, "They are here for reference.");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}

///===================================== Rendering Workers ========================================
void AdvUIOptions::RenderBoolean(const char *pName, float pY, bool pValue)
{
    ///--[Documentation]
    //--Given a boolean value, renders the name and True or False on the right edge. Also renders
    //  things like dividers.
    if(!pName) return;

    //--Name.
    AdvImages.rFont_Mainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Value.
    if(pValue)
        AdvImages.rFont_Mainline->DrawText(cBoolRgt, pY + 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "True");
    else
        AdvImages.rFont_Mainline->DrawText(cBoolRgt, pY + 0.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "False");

    //--Divider.
    AdvImages.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void AdvUIOptions::RenderInteger(const char *pName, float pY, int pValue, int pMaximum)
{
    ///--[Documentation]
    //--Given an integer value, renders the name and a slider on the right side.
    if(!pName || pMaximum < 1) return;

    //--Name.
    AdvImages.rFont_Mainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Slider.
    AdvImages.rSlider->Draw(805.0f, pY + 3.0f);

    //--Render the slider indicator at the given position.
    float cMin = 815.0f;
    float cMax = 939.0f;
    float cPercent = (float)pValue / (float)pMaximum;
    float cXPos = (cMin + ((cMax - cMin) * cPercent));
    AdvImages.rSliderIndicator->Draw(cXPos, pY + 3.0f);

    //--Numerical value.
    AdvImages.rFont_Mainline->DrawTextArgs(cMin - 10.0f, pY - 2.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pValue);

    //--Divider.
    AdvImages.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void AdvUIOptions::RenderFloat(const char *pName, float pY, float pValue)
{
    ///--[Documentation]
    //--Given a floating-point value, renders the name and a slider on the right side. pValue should
    //  be clamped between 0.0 and 1.0.
    if(!pName) return;

    //--Name.
    AdvImages.rFont_Mainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Slider.
    AdvImages.rSlider->Draw(805.0f, pY + 3.0f);

    //--Render the slider indicator at the given position.
    float cMin = 815.0f;
    float cMax = 939.0f;
    float cXPos = (cMin + ((cMax - cMin) * pValue));
    AdvImages.rSliderIndicator->Draw(cXPos, pY + 3.0f);

    //--Numerical value.
    AdvImages.rFont_Mainline->DrawTextArgs(cMin - 10.0f, pY - 2.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(pValue * 100.0f));

    //--Divider.
    AdvImages.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void AdvUIOptions::RenderControl(const char *pName, float pY, const char *pControlName)
{
    ///--[Documentation]
    //--Given a control, renders the name and the primary/secondary inputs as symbols.
    if(!pName || !pControlName) return;

    //--Name.
    AdvImages.rFont_Mainline->DrawText(cNameLft, pY + 0.0f, 0, 1.0f, pName);

    //--Resolve the images for the control.
    ControlManager *rControlManager = ControlManager::Fetch();
    ControlState *rControlState = rControlManager->GetControlState(pControlName);
    StarBitmap *rErrImg = rControlManager->GetErrorImage();

    ///--[Primary]
    //--Primary scancode.
    char tLetter = 'K';
    int tUseScancode = rControlState->mWatchKeyPri;
    if(tUseScancode < 0) {tLetter = 'M'; tUseScancode = rControlState->mWatchMouseBtnPri; }
    if(tUseScancode < 0) {tLetter = 'J'; tUseScancode = rControlState->mWatchJoyPri; }

    //--If the scancode is -1 or -2, the key is unbound.
    if(tUseScancode < 0)
    {
        AdvImages.rFont_Mainline->DrawTextArgs(728.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Unbound");
    }
    //--Otherwise, the key has a primary binding.
    else
    {
        //--Keyboard:
        if(tLetter == 'K')
        {
            //--Check to see if an image exists to describe this scancode.
            StarBitmap *rUseImg = rControlManager->ResolveScancodeImage(tUseScancode);

            //--If the image is not the error image, we can render it.
            if(rErrImg != rUseImg)
            {
                rUseImg->Draw(728.0f - (rUseImg->GetTrueWidth() / 2.0f), pY + 2.0f);
            }
            //--Show the scancode directly.
            else
            {
                AdvImages.rFont_Mainline->DrawTextArgs(728.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
            }
        }
        //--Mouse/Joypad:
        else
        {
            AdvImages.rFont_Mainline->DrawTextArgs(728.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
        }

    }

    ///--[Secondary]
    //--Secondary scancode.
    tLetter = 'K';
    tUseScancode = rControlState->mWatchKeySec;
    if(tUseScancode < 0) {tLetter = 'M'; tUseScancode = rControlState->mWatchMouseBtnSec; }
    if(tUseScancode < 0) {tLetter = 'J'; tUseScancode = rControlState->mWatchJoySec; }

    //--If the scancode is -1 or -2, the key is unbound.
    if(tUseScancode < 0)
    {
        AdvImages.rFont_Mainline->DrawText(920.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Unbound");
    }
    //--Otherwise, the key has a primary binding.
    else
    {
        //--Keyboard:
        if(tLetter == 'K')
        {
            //--Check to see if an image exists to describe this scancode.
            StarBitmap *rUseImg = rControlManager->ResolveScancodeImage(tUseScancode);

            //--If the image is not the error image, we can render it.
            if(rErrImg != rUseImg)
            {
                rUseImg->Draw(920.0f - (rUseImg->GetTrueWidthSafe() / 2.0f), pY + 2.0f);
            }
            //--Show the scancode directly.
            else
            {
                AdvImages.rFont_Mainline->DrawTextArgs(870.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
            }
        }
        //--Mouse/Joypad:
        else
        {
            AdvImages.rFont_Mainline->DrawTextArgs(870.0f, pY + 0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%c: %i", tLetter, tUseScancode);
        }
    }

    //--Divider.
    AdvImages.rFrameDetail->Draw(398.0f, pY + cOptionHei - 6.0f);
}
void AdvUIOptions::RenderDetails()
{
    ///--[Documentation]
    //--Renders the details window at the bottom of the screen, showing what the option highlighted does.
    //  Every option has a unique description so they are enumerated here.
    if(mDetailsTimer < 1) return;

    //--Percentage.
    float cDetailsPercent = EasingFunction::QuadraticInOut(mDetailsTimer, ADVMENU_OPTIONS_DETAILS_SWITCH_TICKS);

    //--Offsets.
    float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cDetailsPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cDetailsPercent * 0.7f));

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    //--Frame.
    AdvImages.rDescriptionFrame->Draw();

    //--Expandable header and name. Note that there is an additional 48 pixels of space on the header
    //  within the dead zones.
    char *tHeaderText = NULL;

    ///--[Description]
    //--Setup.
    float cX =  50.0f;
    float cY = 558.0f;
    float cH   =  25.0f;
    StarFont *rFont = AdvImages.rFont_Mainline;

    //--Gameplay Block:
    if(mMode == ADVMENU_OPTIONS_MODE_GAMEPLAY)
    {
        //--Auto-use Doctor Bag:
        if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_DOCTORBAG)
        {
            ResetString(tHeaderText, "Auto-Use Doctor Bag");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "The doctor bag heals your party when used from the menu, and refills after battle based on how quickly enemies");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "are defeated.");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "If this option is True, then the doctor bag will automatically be used after a battle, if it has charges.");
        }
        //--Auto-hasten Dialogue:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_HASTEN)
        {
            ResetString(tHeaderText, "Auto-Hasten Dialogue");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "During conversation, you can hold down the [Activate] key to speed up dialogue.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If this option is True, dialogue will always be sped up even if the key is not pressed.");
        }
        //--Tourist Mode:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_TOURISTMODE)
        {
            ResetString(tHeaderText, "Tourist Mode");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "If this option is True, your party deals triple damage, and never misses their attacks.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "This mode is meant for players who simply want to see the game's art and story, but not its battle system.");
        }
        //--Combat Memory Cursor:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR)
        {
            ResetString(tHeaderText, "Combat Memory Cursor");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "In battle, when your turn begins the combat cursor will default to the 'Attack' command.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If this option is True, the game will remember which command your character last used and place the cursor");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "on that command instead.");
        }
        //--Ambient Light Boost:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_LIGHTBOOST)
        {
            ResetString(tHeaderText, "Ambient Light Boost");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Some areas of the game use a dynamic lighting system. This system may be too dark to see on certain monitors.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If this is the case for you, this option allows you to modify the brightness.");
        }
        //--Show Censor Bars:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWCENSORBARS)
        {
            ResetString(tHeaderText, "Show Censor Bars");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "This option places black bars over certain lewd bits in certain scenes. It won't prevent you from getting");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "banned on a streaming platform but may at least slow down the content AIs on some platforms.");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "Bottled Starlight takes no responsibility for ensuring your streaming account is not banned, regardless");
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, "of whether you use this option or not.");
        }
        //--Autosaves:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES)
        {
            ResetString(tHeaderText, "Maximum Autosaves");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "The game autosaves when transitioning between areas. This option specifies how many backup autosaves you");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "would like to keep. They are in your Program/Saves/Autosave/ folder.");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "If the value is 0, no autosaves will be made.");
        }
        //--Asset Streaming:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING)
        {
            ResetString(tHeaderText, "Allow Asset Streaming");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "If this option is True, the game will dynamically load assets on the fly. This may cause minor stuttering");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "in some locations, but greatly decreases RAM usage.");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "If this option is False, all assets are loaded when the game loads a chapter. This will take longer and ");
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, "use more RAM, particularly video card RAM, but will not stutter.");
            rFont->DrawText(cX, cY + (cH * 4.0f), 0, 1.0f, "Set this option to True if you are using an older computer without sufficient RAM.");
            rFont->DrawText(cX, cY + (cH * 5.0f), 0, 1.0f, "You will need to restart the game for this option to take effect.");
        }
        //--RAM Loading:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_USERAMLOADING)
        {
            ResetString(tHeaderText, "Use RAM Loading");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "If this option is True, the program will load assets by loading the entire file into memory, then parsing");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "the data. This is faster than loading each piece from the hard drive, especially when using a magnetic");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "disk drive, but consumes considerably more RAM.");
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, "If this option is False, less RAM is used by the loading will take longer.");
            rFont->DrawText(cX, cY + (cH * 4.0f), 0, 1.0f, "Set this option to False if you are using an older computer without sufficient RAM.");
            rFont->DrawText(cX, cY + (cH * 5.0f), 0, 1.0f, "You will need to restart the game for this option to take effect.");
        }
        //--Unload Actors:
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE)
        {
            ResetString(tHeaderText, "Unload Actors After Dialogue");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Actors use portraits to speak during dialogue. These portraits consume video memory when not in use.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If this option is True, these portraits are unloaded when not in use, and dynamically loaded when");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "dialogue begins. You may briefly see a dialogue frame not appear when loading.");
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, "Set this option to True if you are using an older computer without sufficient RAM.");
            rFont->DrawText(cX, cY + (cH * 4.0f), 0, 1.0f, "You will need to restart the game for this option to take effect.");
        }
        //--Use Fading UI
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_USEFADINGUI)
        {
            ResetString(tHeaderText, "Use Fading UI");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "When switching UIs, the program will either fade the menus in, or slide them from offscreen.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "Use whichever you find most comfortable.");
        }
        //--Expert Combat UI
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_EXPERTCOMBATUI)
        {
            ResetString(tHeaderText, "Expert Combat UI");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "During combat, shows considerably more skill information and uses a different layout.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "For advanced players who can make use of all the extra detail at a glance.");
        }
        //--Blocks Must Be Activated
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_BLOCKSMUSTACTIVATE)
        {
            ResetString(tHeaderText, "Puzzle Blocks Require Manual Activation");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "When solving block puzzles, if this option is false, you can walk into the blocks to kick them.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "Some players may find themselves accidentally pushing the wrong blocks, though.");
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, "Set this option to true, and you will only push a block by facing it and pressing 'Activate'.");
        }
        //--Show Stamina Bar
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_SHOWSTAMINABAR)
        {
            ResetString(tHeaderText, "Show Stamina Bar");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "When you run, your stamina is reduced until you run out. This normally appears as a green ring");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "near your character's sprite. Toggle this option to true to see the stamina bar in the corner");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "of the screen instead.");
        }
        //--Disallow Map Overlays
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_DISALLOW_OVERLAYS)
        {
            ResetString(tHeaderText, "Disallow Map Overlays");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "The game periodically shows map overlays to add effects like darkness, fog, or a forest canopy.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "In rare cases for reasons not quite understood, this can cause a black screen bug that makes");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "the game unplayable. If this happens to you, toggle this option on and see if it helps!");
        }
        //--Disallow Map Overlays
        else if(mCursor == ADVMENU_OPTIONS_GAMEPLAY_AUTORUN)
        {
            ResetString(tHeaderText, "Autorun");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Run by default, instead of having to hold the run key. Hold the run key to walk instead.");
        }
    }
    //--Audio Block:
    else if(mMode == ADVMENU_OPTIONS_MODE_AUDIO)
    {
        //--Music Volume:
        if(mCursor == ADVMENU_OPTIONS_MUSIC_VOLUME)
        {
            ResetString(tHeaderText, "Music Volume");
            rFont->DrawText(cX, cY + (cY * 0.0f), 0, 1.0f, "This slider affects how loud all music tracks are.");
        }
        //--Sound Volume:
        else if(mCursor == ADVMENU_OPTIONS_SOUND_VOLUME)
        {
            ResetString(tHeaderText, "Sound Volume");
            rFont->DrawText(cX, cY + (cY * 0.0f), 0, 1.0f, "This slider affects how loud all sound effects are.");
        }
        //--Save Volumes In Savefile:
        /*else if(mCursor == ADVMENU_OPTIONS_SAVEVALUESINSAVEFILE)
        {
            ResetString(tHeaderText, "Save Volumes in Savefile");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Sound and Music volumes are written to each adventure save file.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If this option is True, the volumes will be loaded when the save file is loaded.");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "If this option is False, the volumes will stay the same when loading a save file.");
        }*/
        //--Chance to play.:
        else if(mCursor == ADVMENU_OPTIONS_VOICELINE_PLAY_CHANCE)
        {
            ResetString(tHeaderText, "Voice Line Chance To Play");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Chance to play voice lines in combat. By default, the chance for a voice line");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "to play when a character acts is 3/10, increasing by 1/10 each time a voice line");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "does not play for that character. You can reduce this chance to play with this");
            rFont->DrawText(cX, cY + (cH * 3.0f), 0, 1.0f, "option, including setting it to 0 to prevent any chance to play.");
        }
        //--Abstract volumes.
        else if(mCursor >= ADVMENU_OPTIONS_CHARVOLUME0 && mCursor <= ADVMENU_OPTIONS_CHARVOLUME3)
        {
            ResetString(tHeaderText, "Character Voice Volumes");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Each character in your party has a different voice volume so you can customize");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "them if you don't like someone's voice. If a party slot is empty, you can't");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "adjust that volume.");
        }
    }
    //--Debug Block:
    else if(mMode == ADVMENU_OPTIONS_MODE_CONTROLS)
    {
        if(mCursor == ADVMENU_OPTIONS_CONTROLS_ACTIVATE)
        {
            ResetString(tHeaderText, "Activate");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Press this button to examine things, confirm menu options, and who knows what else.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If you've gotten to this options menu, you've pressed this button before.");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "You know what it does.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_CANCEL)
        {
            ResetString(tHeaderText, "Cancel");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Exits interfaces and cancels things. Hold this down along with Activate to greatly speed up text crawling");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "during dialogue.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_RUN)
        {
            ResetString(tHeaderText, "Run");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Hold this down in the overworld to run.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_UP)
        {
            ResetString(tHeaderText, "Up");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "It's the up key. Up is an axiomatic, prime concept.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_RIGHT)
        {
            ResetString(tHeaderText, "Right");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "This is the one that doesn't make an L with your thumb.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_DOWN)
        {
            ResetString(tHeaderText, "Down");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Represents the quality of the jokes in these descriptions.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_LEFT)
        {
            ResetString(tHeaderText, "Left");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Not right. Not middle. Not forward. Left. Accept no substitutes.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERLEFT)
        {
            ResetString(tHeaderText, "Shoulder Left");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Page-up or shoulder button left. Switches pages in some UI's.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_SHOULDERRIGHT)
        {
            ResetString(tHeaderText, "Shoulder Right");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Page-down or shoulder button right. Switches pages in some UI's.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_FIELDABILITIES)
        {
            ResetString(tHeaderText, "Field Abilities Menu");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Opens the field ability UI on the overworld, allowing you to change");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "which button activates which ability.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_1)
        {
            ResetString(tHeaderText, "Field Ability 1");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Activates the field ability in the first slot.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_2)
        {
            ResetString(tHeaderText, "Field Ability 2");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Activates the field ability in the second slot.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_3)
        {
            ResetString(tHeaderText, "Field Ability 3");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Activates the field ability in the fourth slot.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "Just kidding, activates the one in the third slot.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_4)
        {
            ResetString(tHeaderText, "Field Ability 4");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Activates the field ability in the fourth slot.");
        }
        else if(mCursor == ADVMENU_OPTIONS_CONTROLS_ABILITY_5)
        {
            ResetString(tHeaderText, "Field Ability 5");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Activates the field ability in the fifth slot.");
        }
    }
    //--Controls Block:
    else if(mMode == ADVMENU_OPTIONS_MODE_DEBUG)
    {
        if(mCursor == ADVMENU_OPTIONS_DEBUG_OPENMENU)
        {
            ResetString(tHeaderText, "Open Debug Menu");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Opens the Debug Menu when on the overworld. You must have debug mode activated or it won't do anything. ");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "You can activate it with a handy password given out to patrons. Instructions are on the patreon!");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "");
        }
        else if(mCursor == ADVMENU_OPTIONS_DEBUG_TOGGLEFULLSCREEN)
        {
            ResetString(tHeaderText, "Toggle Fullscreen");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Switches between fullscreen and windowed when pressed. Bound to both F12 and F11 as OSX holds the F12 key ");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "for its stupid crap.");
            rFont->DrawText(cX, cY + (cH * 2.0f), 0, 1.0f, "I hate OSX. -Salty");
        }
        else if(mCursor == ADVMENU_OPTIONS_DEBUG_REBUILDKERNING)
        {
            ResetString(tHeaderText, "Rebuild Kerning");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Re-runs the kerning files to allow real-time editing of font kerning.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "Useful if you're making a mod. Useless to normal players.");
        }
        else if(mCursor == ADVMENU_OPTIONS_DEBUG_REBUILDSHADERS)
        {
            ResetString(tHeaderText, "Rebuild Shaders");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Re-runs the shaders and recompiles them. Good for modders.");
        }
        else if(mCursor == ADVMENU_OPTIONS_DEBUG_DISABLE_OUTLINE)
        {
            ResetString(tHeaderText, "Shader: Disable Colour Outline");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Enemies on the overworld are extra tough if they have different coloured outlines. This is done via a shader.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If you believe the shader is causing system instability, disable it here.");
        }
        else if(mCursor == ADVMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER)
        {
            ResetString(tHeaderText, "Shader: Disable Underwater");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "The Underwater shader causes the screen to distort slightly when underwater.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If you believe the shader is causing system instability, disable it here.");
        }
        else if(mCursor == ADVMENU_OPTIONS_DEBUG_DISABLE_LIGHTING)
        {
            ResetString(tHeaderText, "Shader: Disable Light");
            rFont->DrawText(cX, cY + (cH * 0.0f), 0, 1.0f, "Some areas of the game are dark. This is done via a shader.");
            rFont->DrawText(cX, cY + (cH * 1.0f), 0, 1.0f, "If you believe the shader is causing system instability, disable it here.");
        }
    }

    ///--[Header]
    //--Now that we know the name to render, do that.
    if(tHeaderText)
    {
        float cNameLen = AdvImages.rFont_Heading->GetTextWidth(tHeaderText);
        RenderExpandableHeader(VIRTUAL_CANVAS_X * 0.50f, 510.0f, cNameLen - 48.0f, 76.0f, 76.0f, AdvImages.rExpandableHeader);
        AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 510.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tHeaderText);
    }

    ///--[Finish Up]
    //--Clean.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
    free(tHeaderText);
}
