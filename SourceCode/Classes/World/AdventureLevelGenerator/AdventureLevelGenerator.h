///================================== AdventureLevelGenerator =====================================
//--A random level generator that produces AdventureLevels. Requires a tilemap to be provided along
//  with tile UV coordinates to make everything look reasonable. Creates the level in much the same
//  way as importing a MapData.slf does.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"
#include "ALGTextureData.h"
#include "StarlightPerlin.h"
#include "AdventureLevelGeneratorStructures.h"

///========================================== Classes =============================================
class AdventureLevelGenerator : public RootLevel
{
    private:
    //--System
    friend class AdventureLevel;
    friend class TiledLevel;
    int mPulse;
    int mGenerationState;
    int mLongestDistanceLastPulse;

    //--Noise Generator
    bool mIsEnteringSeed;
    char mSeedEntered[32];
    int mSeedFlashTimer;
    int32_t mSeedNumber;
    StarlightPerlin *rNoiseModule;

    //--Storage
    int mEntrancePosX;
    int mEntrancePosY;
    int mExitPosX;
    int mExitPosY;

    //--Room Listing
    int mRoomsTotal;
    ALGRoom **mRooms;

    //--Map Data
    int mXSize;
    int mYSize;
    ALGTile ***mMapData;

    //--Rendering
    int mPreviousMouseZ;
    float mRenderScale;
    float mXRenderOffset;
    float mYRenderOffset;

    //--Texturing
    StarBitmap *rMapTexture;
    ALGTextureData mTextureData;
    WallGrid mWallGridData[WALLGRIDS_TOTAL];

    protected:

    public:
    //--System
    AdventureLevelGenerator();
    virtual ~AdventureLevelGenerator();
    void Reset();
    float NormalizeRoll(StarlightPerlin *pModule, float pX, float pY, float pZ);

    //--Public Variables
    static int xPathVis;
    static int xLastSeed;

    //--Static Storage
    static bool xIsScriptDataValid;
    static ALGScriptData xScriptData;

    //--Property Queries
    //--Manipulators
    void SizeMap(int pXSize, int pYSize);

    //--Core Methods
    void GenerateSizes();
    void GenerateBaseLayout();
    void GenerateTunnels();
    void PlaceExits();
    void PlaceTreasure();
    void PlaceEnemies();
    void DigBetween(int pXA, int pYA, int pXB, int pYB);

    //--Enemies
    void RollLocationNear(int pRollSlot, int &sX, int &sY);
    void PlaceEnemyNear(int pEnemySlot, int &sX, int &sY);
    ALGEnemyPath *PlacePathsBetween(int pEnemySlot, int pStartX, int pStartY, int pEndX, int pEndY);

    //--Exporter
    AdventureLevel *ExportToAdventureLevel();

    //--Texture Manipulators
    void SetTextureMap(const char *pPath);
    void SetPatternPosition(float pX, float pY);
    void AllocateFloors(int pTotal);
    void SetFloorUV(int pSlot, float pLft, float pTop);
    void SetBlackoutPosition(float pX, float pY);
    void SetLadderPosition(float pDnX, float pDnY, float pUpX, float pUpY);
    void SetCliffPosition(float pX, float pY);
    void SetFloorEdgingPosition(float pX, float pY);
    void SetTreasurePosition(float pX, float pY);
    void SetEnemyUV(float pX, float pY);

    //--Routines
    void MarkTilesByRange(int pX, int pY, uint8_t pSpecialFlag);
    void MarkTilesByRange(int pXA, int pYA, int pXB, int pYB, uint8_t pSpecialFlag);
    void MarkTilesByRange(StarLinkedList *pList, uint8_t pSpecialFlag);
    void RangeIterator(StarLinkedList *pIteratorList, uint8_t pSpecialFlag);
    bool AreAnyTilesDisconnected();

    //--Texturing
    void SetWallGridProperties(bool pGrid[3][3], int pWallCode, int pWallHiCode, int pBlackoutCode);
    void BuildTextures();
    void EdgeFloors();

    private:
    //--Private Core Methods

    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdlevGenerator_Create(lua_State *L);
int Hook_AdlevGenerator_GetProperty(lua_State *L);
int Hook_AdlevGenerator_SetProperty(lua_State *L);

