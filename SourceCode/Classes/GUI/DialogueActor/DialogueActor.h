///======================================= DialogueActor ==========================================
//--Represents a portrait on the dialogue screen. These usually possess several emotions and render states.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define SIZE_MOD_TICKS 15.0f
#define SIZE_MOD_AMOUNT 0.05f

///========================================== Classes =============================================
class DialogueActor : public RootObject
{
    private:
    //--System
    int mTicksSinceShown;

    //--Names
    char *mLocalName;
    StarLinkedList *mAliasList; //const char *, master

    //--Images
    float mActiveOffsetX;
    float mActiveOffsetY;
    StarBitmap *rActiveImage;
    StarLinkedList *mImageList; //StarBitmap *, ref
    StarLinkedList *mImageListRev; //StarBitmap *, ref
    StarLinkedList *mImageListPaths; //char *, ref
    StarLinkedList *mIsNotWideImageList; //StarBitmap *, ref
    StarLinkedList *mIsNotWideImageListRev; //StarBitmap *, ref
    StarLinkedList *mOffsetsList; //TwoDimensionRealPoint *, master

    //--Multi-layered emotions.
    StarLinkedList *mrAdditionalLayerList; //StarBitmap *, ref

    //--Render State
    int mSizeTimer;
    bool mIsDarkened;

    protected:

    public:
    //--System
    DialogueActor();
    virtual ~DialogueActor();

    //--Public Variables
    //--Property Queries
    const char *GetName();
    bool IsSpeaking(const char *pSpeakerName);
    bool IsDarkened();
    bool IsFullDark();
    StarBitmap *GetActiveImage();
    const char *GetPathOfEmotion(const char *pEmotion);
    float GetOffsetX(const char *pEmotion);
    float GetActiveOffsetX();
    const char *GetNameFromAlias(const char *pAlias);

    //--Manipulators
    void SetName(const char *pName);
    void AddAlias(const char *pName, const char *pDisplayName);
    void RemoveAlias(const char *pName);
    void AddPortrait(const char *pName, const char *pDLPath, bool pIsNotWide);
    void AddPortraitRev(const char *pName, const char *pDLPath, bool pIsNotWide);
    void RemovePortrait(const char *pName);
    void SetActivePortrait(const char *pName);
    void SetDarkenedState(bool pFlag);
    void AddOffset(const char *pName, float pX, float pY);

    //--Core Methods
    void SetDarkeningFromName(const char *pName);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    void DebugPrintData();

    //--Drawing
    StarBitmap *RenderAt(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    StarBitmap *RenderAt(StarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    void RenderAtLayered(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    void RenderAtVN(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    void RenderAtVN(StarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_DialogueActor_Create(lua_State *L);
int Hook_DialogueActor_Exists(lua_State *L);
int Hook_DialogueActor_Push(lua_State *L);
int Hook_DialogueActor_GetProperty(lua_State *L);
int Hook_DialogueActor_SetProperty(lua_State *L);
