//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdvCombat.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "EntityManager.h"
#include "LuaManager.h"

void AdventureLevel::Debug_WipeFieldEnemies()
{
    //--Go through every TilemapActor and remove them if they had a spawn entry. If they don't, they don't
    //  get removed in this manner.
    SpawnPackage *rPackage = (SpawnPackage *)mEnemySpawnList->PushIterator();
    while(rPackage)
    {
        //--Debug.
        //fprintf(stderr, "Searching for enemy %s.\n", rPackage->mUniqueSpawnName);

        //--Add it to the dead enemy respawn list if it wasn't already dead.
        if(!IsEnemyDead(rPackage->mUniqueSpawnName))
        {
            //fprintf(stderr, " -Enemy is not dead.\n");
            //fprintf(stderr, "  Added enemy to killed list.\n");
            KillEnemy(rPackage->mUniqueSpawnName);
        }

        //--Now find the matching actor.
        StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
        RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
        while(rCheckEntity)
        {
            //--Entity must be of the right type.
            if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                //--Cast.
                TilemapActor *rActor = (TilemapActor *)rCheckEntity;
                //fprintf(stderr, " -Checking actor %s.\n", rActor->GetName());

                //--Is it the enemy in question? It's alive, then.
                if(rActor->IsEnemy(rPackage->mUniqueSpawnName))
                {
                    //fprintf(stderr, "  Found matching actor.\n");
                    rActor->mSelfDestruct = true;
                    rEntityList->PopIterator();
                    break;
                }
            }

            //--Next.
            rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
        }

        //--Next.
        rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
    }
}
void AdventureLevel::Debug_RespawnFieldEnemies()
{
    WipeDestroyedEnemies();
    SpawnEnemies();
}
void AdventureLevel::Debug_TransitionTo(const char *pMapTarget)
{
    SetTransitionDestination(pMapTarget, "PlayerStart");
}
#include "DataLibrary.h"
void AdventureLevel::Debug_FireCutscene(const char *pCutsceneTarget)
{
    //--Error check. We need the path to Adventure Mode for this to operate.
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    if(!pCutsceneTarget) return;

    //--Assemble path.
    char *tBuffer = InitializeString("%s/%s/Debug_Startup.lua", rAdventurePath, pCutsceneTarget);

    //--Execute.
    LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

    //--Clean.
    free(tBuffer);
}
