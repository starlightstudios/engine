///===================================== StringTyrantTitle ========================================
//--Title screen used by String Tyrant. If only String Tyrant is found among the playable games,
//  boots straight at program startup. Otherwise, the player needs to click the option on the boot menu.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Backgrounds
#define STT_BG_LAYERS_TOTAL 7

//--Menus
#define STT_MENU_MAIN 0
#define STT_MENU_NEWGAME 1

//--Visiblity
#define STT_VIS_TICKS 30
#define STT_MENU_SLIDE_TICKS 45
#define STT_MENU_SLIDE_DISTANCE 100
#define STT_FADE_TO_BLACK_TICKS 30

//--Game Options: Combat Type
#define STT_COMBAT_WAIT 0
#define STT_COMBAT_ACTIVE 1
#define STT_COMBAT_TOTAL 2

//--Game Options: Difficulty
#define STT_DIFFICULTY_NORMAL 0
#define STT_DIFFICULTY_HARD 1
#define STT_DIFFICULTY_EASY 2
#define STT_DIFFICULTY_TOTAL 3

///--[Menu Definitions]
//--Menu Indices
#define STT_MENU_MAIN 0
#define STT_MENU_NEWGAME 1
#define STT_MENU_LOADGAME 2
#define STT_MENU_OPTIONS 3

//--Main Menu
#define STT_MAIN_MENU_NEWGAME 0
#define STT_MAIN_MENU_LOADGAME 1
#define STT_MAIN_MENU_OPTIONS 2
#define STT_MAIN_MENU_QUIT 3
#define STT_MAIN_MENU_TOTAL 4

//--New Game Menu
#define STT_NEWGAME_MENU_TUTORIAL 0
#define STT_NEWGAME_MENU_STARTGAME 1
#define STT_NEWGAME_MENU_STARTGAMENOINTRO 2
#define STT_NEWGAME_MENU_DIFFICULTY 3
#define STT_NEWGAME_MENU_COMBATTYPE 4
#define STT_NEWGAME_MENU_BACK 5
#define STT_NEWGAME_MENU_TOTAL 6

//--Load Game Menu
#define STT_FILES_PER_PAGE 5
#define STT_LOADGAME_MENU_SHOWING 0
#define STT_LOADGAME_MENU_NEXT 1
#define STT_LOADGAME_MENU_PREV 2
#define STT_LOADGAME_MENU_BACK 3
#define STT_LOADGAME_MENU_SKIP 4
#define STT_LOADGAME_MENU_GAME0 5
#define STT_LOADGAME_MENU_GAME1 6
#define STT_LOADGAME_MENU_GAME2 7
#define STT_LOADGAME_MENU_GAME3 8
#define STT_LOADGAME_MENU_GAME4 9
#define STT_LOADGAME_MENU_TOTAL 10

//--Options Menu
#define STT_OPTIONS_MENU_MUSICVOLUME 0
#define STT_OPTIONS_MENU_SOUNDVOLUME 1
#define STT_OPTIONS_MENU_RESOLUTION 2
#define STT_OPTIONS_MENU_FULLSCREEN 3
#define STT_OPTIONS_MENU_APPLY 4
#define STT_OPTIONS_MENU_CANCEL 5
#define STT_OPTIONS_MENU_TOTAL 6

//--Function Pointers
typedef void (*STTPostFadeExec)(StringTyrantTitle *);

///========================================== Classes =============================================
class StringTyrantTitle : public RootLevel
{
    private:
    //--System
    bool mIsVisible;
    int mVisibilityTimer;
    char *mPostExecScript;

    //--Background Animation
    int mBGAnimTimer;

    //--Fading to Black
    bool mIsFadingToBlack;
    int mFadeToBlackTimer;
    STTPostFadeExec rFadePostExec;

    //--Game Options
    int mDifficulty;
    char mDifficultyStrings[STT_DIFFICULTY_TOTAL][32];
    int mCombatType;
    char mCombatStrings[STT_COMBAT_TOTAL][32];

    //--Display Mode
    char mDisplayModeBuf[STD_MAX_LETTERS];

    //--Menu Handling
    int mHighlight;
    int mActiveMenu;
    StarLinkedList *mMenuList; //STTMenuPack *, master

    //--Loading Storage
    int mActiveLoadSlot;
    int mLoadOffset;
    int mLoadNamesTotal;
    char **mLoadNames;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontMenuMain;
            StarFont *rFontMenuSmall;

            //--Background
            StarBitmap *rBackgrounds[STT_BG_LAYERS_TOTAL];
        }Data;
    }Images;

    protected:

    public:
    //--System
    StringTyrantTitle();
    virtual ~StringTyrantTitle();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    void SetPostExecScript(const char *pPath);
    void BeginFadeout(STTPostFadeExec pFadePostExec);

    //--Core Methods
    void ScanSavegames();
    void PopulateSavegames();

    //--Click Handlers
    void BuildMenuMain();
    static void HandleClick_Main(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick);
    void BuildMenuNewgame();
    static void HandleClick_Newgame(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick);
    void BuildMenuLoadGame();
    static void HandleClick_LoadGame(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick);
    void BuildMenuOptions();
    static void HandleClick_Options(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick);

    //--Executors
    static void Fadeout_Tutorial(StringTyrantTitle *pCaller);
    static void Fadeout_NewGame(StringTyrantTitle *pCaller);
    static void Fadeout_NewGameNoIntro(StringTyrantTitle *pCaller);
    static void Fadeout_LoadGame(StringTyrantTitle *pCaller);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();
    void RenderBackground(float pMixAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_STT_Create(lua_State *L);

