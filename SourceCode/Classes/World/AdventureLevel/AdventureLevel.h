///======================================= AdventureLevel ==========================================
//--Represents a level used in Adventure Mode. Uses most of the basic storage and parsing properties
//  of the TiledLevel it derives from, but with additional lists for objects like doors and exits.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "TiledLevel.h"
class RayCollisionEngine;

///===================================== Local Structures =========================================
#include "AdventureLevelStructures.h"

///===================================== Local Definitions ========================================
//--Level Transition Stuff
#define TRANSITION_HOLD 15
#define TRANSITION_HOLD_TICKS 10
#define TRANSITION_FADE_TICKS 15
#define PULSE_TICKS 90

//--Control Locking
#define AL_CONTROL_LOCK_TICKS 25
#define AL_CONTROL_LOCK_PIXELS 85

//--Resting Duration
#define RESTING_TICKS_FADEOUT 30
#define RESTING_TICKS_HOLD 30
#define RESTING_TICKS_FADEIN 30
#define RESTING_TICKS_TOTAL (RESTING_TICKS_FADEOUT + RESTING_TICKS_HOLD + RESTING_TICKS_FADEIN)

//--Script Fading Depth
#define SCRIPT_FADE_UNDER_CHARACTERS 0
#define SCRIPT_FADE_UNDER_UI 1
#define SCRIPT_FADE_OVER_UI 2
#define SCRIPT_FADE_OVER_UI_BLACKOUT_WORLD 3

//--Inflection Codes
#define INFLECTION_NO_CHANGE -1

//--Movement Storage
#define PLAYER_MOVE_STORAGE_TOTAL 81
#define FOLLOWER_SPACING_DEF 12

//--Stamina Stuff
#define STAMINA_VIS_TICKS 15
#define STAMINA_RING_VIS_TICKS 30
#define STAMINA_RING_MAX_TICKS 25
#define STAMINA_MAX 100.0f
#define STAMINA_RUN_LOWER 20.0f
#define STAMINA_RUN_ALLOW 70.0f
#define STAMINA_CONSUME_TICK 0.2f
#define STAMINA_REGEN_TICK 0.5f
#define STAMINA_REGEN_STANDING 3.5f

//--Maximum Music Layers
#define MAX_MUSIC_LAYERS 5 //Must always be at least 1

//--Field Ability Stencils
#define ADVLEV_FIELD_STENCIL_START 10

//--Types for Local Packages
#define AL_SPECIAL_TYPE_FAIL 0
#define AL_SPECIAL_TYPE_CHEST 1
#define AL_SPECIAL_TYPE_FAKECHEST 2
#define AL_SPECIAL_TYPE_DOOR 3
#define AL_SPECIAL_TYPE_EXIT 4
#define AL_SPECIAL_TYPE_INFLECTION 5
#define AL_SPECIAL_TYPE_CLIMBABLE 6
#define AL_SPECIAL_TYPE_LOCATION 7
#define AL_SPECIAL_TYPE_POSITION 8
#define AL_SPECIAL_TYPE_INVISZONE 9
#define AL_SPECIAL_TYPE_ACTOR 10

//--Retreat Handling
#define AL_RETREAT_STANDARD 1200

//--Region Notifier States
#define AL_REGION_STATE_FADEIN 0
#define AL_REGION_STATE_ANIMATE 1
#define AL_REGION_STATE_SHRINK 2
#define AL_REGION_STATE_IDLE 3

#define AL_REGION_SHRINK_TICKS 45
#define AL_REGION_FADE_TICKS 15

//--Autosave
#define AL_AUTOSAVE_INDICATOR_TICKS 120

//--Shooting
#define AL_SHOOTING_TICKS 7
#define AL_SHOOTING_FIRE_TICKS 4
#define AL_SHOOTING_RECOIL_TICKS 15
#define AL_SHOOT_OFFSETS 4

//--Chests
#define AL_CHEST_OPEN_NORMAL 0.0f
#define AL_CHEST_CLOSED_RESPAWN 1.0f
#define AL_CHEST_OPEN_RESPAWN 2.0f
#define AL_CHEST_OPEN_NORMAL_PENDING 3.0f

///--[UI Timing Constants]
#define AL_UITIME_STANDSTILL_TICKS 180
#define AL_UITIME_FADE_TICKS 30
#define AL_UITIME_FADEOUT_TICKRATE 6
#define AL_UITIME_FADEIN_TICKRATE 1

///--[Translation]
#define AL_NORMALTRANS_TOTAL 1

///========================================== Classes =============================================
class AdventureLevel : public TiledLevel
{
    private:
    //--System
    bool mIsRandomLevel;
    friend class AdventureLevelGenerator;
    int mCannotOpenMenuTimer;
    bool mIsPlayerOnStaircase;
    float mPlayerStartX;
    float mPlayerStartY;
    char *mBasePath;
    bool mRunParallelDuringDialogue;
    bool mRunParallelDuringCutscene;
    bool mNoBordersThisCutscene;
    int mEnemyAICycles;

    //--Autosave
    int mAutosaveTimer;
    bool mRunAutosave;
    int mAutosavePostTimer;
    StarBitmap *rAutosaveImage;

    //--Minigames
    RunningMinigame *mRunningMinigame;
    KPopDanceGame *mKPopDanceGame;
    PuzzleFight *mPuzzleFightGame;
    ShakeMinigame *mShakeMinigame;

    //--Region Notifier
    int mRegionNotifierAnimTotal;
    int mRegionNotifierAnimState;
    int mRegionNotifierAnimTimer;
    int mRegionNotifierAnimTimerMax;
    StarBitmap **rRegionNotifierAnim;
    StarBitmap *rRegionNotifier;
    float mRegionNotifierFinalScale;

    //--Notification Packs
    StarLinkedList *mNotifications; //NotificationPack *, master

    //--Ray Collision Engines
    int mRayCollisionEnginesTotal;
    RayCollisionEngine **mRayCollisionEngines; //Should be one for each collision layer.

    //--Per-map animations.
    bool mIsMajorAnimationMode;
    char mMajorAnimationName[STD_MAX_LETTERS];
    StarLinkedList *mPerMapAnimationList;

    //--Debug Handling
    bool mWasMenuUpLastTick;
    AdventureDebug *mDebugMenu;
    static bool xHasBuiltListing;
    static char **xListing;
    static int xIntListing[11];

    //--Menu
    bool mIsRestSequence;
    int mRestingTimer;
    bool mCannotOpenMenu;
    AdventureMenu *mAdventureMenu;

    //--Object Information
    StarLinkedList *mChestList; //DoorPackage *, master
    StarLinkedList *mFakeChestList; //DoorPackage *, master
    StarLinkedList *mDoorList; //DoorPackage *, master
    StarLinkedList *mExitList; //ExitPackage *, master
    StarLinkedList *mInflectionList; //InflectionPackage *, master
    StarLinkedList *mClimbableList; //ClimbablePackage *, master
    StarLinkedList *mLocationList; //TwoDimensionReal *, master
    StarLinkedList *mPositionList;//PositionPackage *, master
    StarLinkedList *mInvisibleZoneList;//InvisZone *, master

    //--Catalyst Tone
    bool mPlayCatalystTone;
    int mCatalystToneCountdown;

    //--Examinables
    char *mExaminableScript;
    StarLinkedList *mExaminablesList; //ExaminePackage *, master

    //--Camera
    bool mIsCameraLocked;
    uint32_t mCameraFollowID;
    TwoDimensionReal mCameraDimensions;

    //--Backgrounds
    StarBitmap *rBackgroundImg;
    float mBackgroundStartX;
    float mBackgroundStartY;
    float mBackgroundScrollX;
    float mBackgroundScrollY;

    //--Foreground/Background Overlays
    int mUnderlaysTotal;
    OverlayPackage *mUnderlayPacks;
    int mOverlaysTotal;
    OverlayPackage *mOverlayPacks;
    int mSpawnAuraChance;
    StarBitmap *rSpawnAuraImage;
    StarLinkedList *mAuraParticleList; //AuraParticle *, master

    //--Control Locking/Handling
    int mControlLockTimer;
    uint32_t mControlEntityID;
    StarLinkedList *mFollowEntityIDList; //uint32_t *, master
    StarLinkedList *mControlLockList;

    //--Followers
    int mPlayerStoppedMovingTimer;
    int mFollowerSpacing[3];
    float mPlayerXPositions[PLAYER_MOVE_STORAGE_TOTAL];
    float mPlayerYPositions[PLAYER_MOVE_STORAGE_TOTAL];
    int mPlayerFacings[PLAYER_MOVE_STORAGE_TOTAL];
    int mPlayerTimers[PLAYER_MOVE_STORAGE_TOTAL];
    int mPlayerDepths[PLAYER_MOVE_STORAGE_TOTAL];
    bool mPlayerRunnings[PLAYER_MOVE_STORAGE_TOTAL];

    //--UI Flags and Timers
    bool mShowPlatinumCompass;
    int mEntireUIVisTimer;
    int mStaminaVisTimer;
    int mStaminaRingVisTimer;
    float mStaminaRingVisPct;
    int mFullStaminaTicks;

    //--Stamina
    bool mBlockRunningToRegen;
    float mPlayerStamina;
    float mPlayerStaminaMax;
    float mPartyLeadStamX;
    float mPartyLeadStamY;

    //--Health
    bool mShowHealthMeter;
    int mHealthRemaining;
    int mHealthMax;
    StarBitmap *rHealthIndicator;
    StarBitmap *rEmptyIndicator;

    //--Transition Handling
    bool mIsExitStaircase;
    char *mTransitionDestinationMap;
    char *mTransitionDestinationExit;
    char *mTransitionPostScript;
    int mTransitionDirection;
    float mTransitionPercentage;

    //--Transition Fading
    int mTransitionHoldTimer;
    bool mIsTransitionOut;
    bool mIsTransitionIn;
    int mTransitionTimer;

    //--Script-Controlled Fading
    bool mUseAlternateBlend;
    bool mIsScriptFading;
    bool mScriptFadeHolds;
    int mScriptFadeDepthFlag;
    int mScriptFadeTimer;
    int mScriptFadeTimerMax;
    StarlightColor mStartFadeColor;
    StarlightColor mEndFadeColor;

    //--Zones and Triggers
    char *mTriggerScript;
    StarLinkedList *mTriggerList;

    //--Enemy Handling
    bool mHideAllViewcones;
    int mPlayerCamoTimer;
    int mRetreatTimer;
    StarLinkedList *mEnemySpawnList; //SpawnPackage *, master
    static StarLinkedList *xDestroyedEnemiesList;
    static StarLinkedList *xMuggedEnemiesList;

    //--Switch Handling
    StarLinkedList *mSwitchList;

    //--Save Points
    StarLinkedList *mSavePointList;

    //--Reinforcement Pulsing
    bool mIsPulseMode;
    int mPulseTimer;

    //--Patrol Nodes
    bool mDisablePatrolRandomization;
    StarLinkedList *mPatrolNodes; //PatrolNodePack *, master

    //--Camera Handling
    bool mIsPositiveCameraMode;
    int mFirstLock;
    StarLinkedList *mCameraZoneList;

    //--Screen Shaking and Other Effects
    int mScreenShakeTimer;
    int mShakePeriodLength;
    int mShakePeriodicityRoot;
    int mShakePeriodicityScatter;
    StarLinkedList *mShakeSoundList; //char *
    int mRockFallingFrequency;
    StarLinkedList *mFallingRockList; //FallingRockPack *
    StarLinkedList *mrFallingRockImageList; //StarBitmap *, ref

    //--Lights
    bool mAreLightsActive;
    StarLinkedList *mLightsList;
    StarlightColor mAmbientLight;
    static float xLightBoost;
    bool mReuploadStandardLightData;
    bool mReuploadAllLightData;
    float mLastUploadScaleX;
    float mLastUploadScaleY;
    float mLastUploadViewportW;
    float mLastUploadViewportH;

    //--Underwater
    bool mIsUnderwaterMode;
    GLuint mUnderwaterFramebuffer;
    GLuint mUnderwaterTexture;
    GLuint mUnderwaterDepth;

    //--Player Light
    bool mPlayerHasLight;
    bool mPlayerLightIsActive;
    bool mPlayerLightDoesNotDrain;
    int mPlayerLightPower;
    int mPlayerLightPowerMax;
    AdventureLight *mPlayerLightObject;

    //--Objectives Tracker
    StarLinkedList *mObjectivesList; //ObjectivePack *, master

    //--Dislocated Render Listing
    StarLinkedList *mDislocatedRenderList; //DislocatedRenderPackage *, master

    //--Notices System
    StarLinkedList *mNotices; //ActorNotice *, master

    //--Field Abilities
    bool mCanEditFieldAbilities;
    bool mFieldAbilitiesInCancelMode;
    bool mDontCancelFieldAbilitiesForOneScene;
    FieldAbilityPack *rExecutingPackage;
    StarlightString *mFieldAbilityString;
    StarlightString *mFieldAbilityStringAlt;
    StarLinkedList *mExecutingFieldAbilities; //FieldAbilityPack *, master
    StarLinkedList *mFieldAbilityObjectList; //FieldAbilityObjectPack *, master

    ///--[Translation]
    union NormalString
    {
        struct
        {
            char *mPtr[AL_NORMALTRANS_TOTAL];
        }mem;
        struct
        {
            char *mTextReceived;
        }str;
    }NormalString;

    ///--Shooting
    bool mIsShootingActive;
    bool mIsFiringSequence;
    bool mTennisShooting;
    uint32_t mTennisID;
    int mShootingCollisionDepth;
    int mShootingTimer;
    int mShootFiringTimer;
    char *mExecuteExaminableName;
    TilemapActor *rExecuteActor;
    bool mSpawnBulletPuff;
    float mBulletPuffX;
    float mBulletPuffY;
    float mBulletPuffZ;
    StarLinkedList *mShootListOfExaminables; //ExaminePackage *, reference
    StarLinkedList *mShootListOfTilemapActors; //TilemapActor *, reference

    //--Deep Water/Snow/Etc
    StarLinkedList *mDeepLayers; //DeepLayerPack *, master

    //--Public Statics
    public:
    static bool xCreationDebug;
    static char *xLayerResolverPath;
    static bool xIsLayeringMusic;
    static bool xIsCombatMaxIntensity;
    static int xCurrentlyRunningTracks;
    static float xCurrentIntensity;
    static float xLayerVolumes[MAX_MUSIC_LAYERS][101];
    static char xLayerNames[MAX_MUSIC_LAYERS][STD_MAX_LETTERS];
    static bool xForceZeroLayerVolume;
    static int xZeroOffIntensityTicks;
    static float xScriptMandatedIntensity;
    static bool xScriptMandateIntensityNow;
    static float xCombatMandatedIntensity;
    static bool xIsScanningTreasures;
    static char *xRandomLevelGeneratorPath;
    static bool xDisableAllMugging;
    static char *xSpawnTennisBallPath;
    static int xPartySpacing;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Utility
            StarBitmap *rViewconePixel;
            StarFont *rUIFont;
            StarFont *rNotificationFont;

            //--Ladder and Rope
            StarBitmap *rLadderTop;
            StarBitmap *rLadderMid;
            StarBitmap *rLadderBot;
            StarBitmap *rRopeAnchor;
            StarBitmap *rRopeTop;
            StarBitmap *rRopeMid;
            StarBitmap *rRopeBot;

            //--Stamina Bar
            StarBitmap *rPlatinumCompass;
            StarBitmap *rStaminaBarOver;
            StarBitmap *rStaminaBarMiddle;
            StarBitmap *rStaminaBarUnder;
            StarBitmap *rStaminaRingOver;
            StarBitmap *rStaminaRingUnder;
            StarBitmap *rStaminaRingDanger;
        }Data;
    }Images;

    protected:

    public:
    //--System
    AdventureLevel();
    virtual ~AdventureLevel();

    //--Public Variables
    static char *xRootPath;
    static char *xItemListPath;
    static char *xItemImageListPath;
    static char *xCatalystHandlerPath;
    static bool xTriggerHandledUpdate;
    static bool xIsSwitchUp;
    static bool xExaminationDidSomething;
    static char *xLastSavePoint;
    static char *xLevelMusic;
    static char *xLastIgnorePulse;
    static float xClosestEnemy;
    static int xRemappingsTotal;
    static char **xRemappingsCheck;
    static char **xRemappingsResult;
    static bool xEntitiesDrainStamina;
    static bool xIsPlayerInInvisZone;
    static bool xRunEnemyUpdateForPatrolScatter;
    static bool xBlockAutosaveOnce;
    static char *xAutosaveIconPath;
    static bool xRespondToShot;
    static bool xRespondToShotImmediately;
    static bool xStopFiringImmediately;
    static bool xDontSpawnBulletImpact;
    static char *xRandomChestResult;
    static char *xChestScript;
    static char *xItemNodeScript;
    static bool xLockoutFieldAbilityEditing;

    //--Public, non-static: Used for chest opening.
    float mLastChestX;
    float mLastChestY;

    //--Debug
    static int xCountCatalyst[6];

    //--Property Queries
    virtual bool IsOfType(int pType);
    static bool IsWorldStopped();
    static bool IsWorldStoppedFromCutsceneOnly();
    bool CannotOpenMenu();
    bool IsLevelTransitioning();
    bool AreControlsLocked();
    bool IsPulsing();
    bool GetWorldClipAt(float pX, float pY, float pZ, int pLayer);
    virtual bool GetClipAt(float pX, float pY, float pZ, int pLayer);
    virtual bool GetClipAt(float pX, float pY, float pZ);
    int GetCollision(int pX, int pY, int pZ);
    bool GetEntityClipAt(float pX, float pY, float pZ);
    bool GetWanderClipAt(float pX, float pY);
    static bool IsClippedWithin(const uint8_t &sClipVal, const int &sInTileX, const int &sInTileY);
    float GetPlayerStartX();
    float GetPlayerStartY();
    float GetCameraLft();
    float GetCameraTop();
    float GetDefaultCameraScale();
    float GetCameraScale();
    bool IsCharacterInWorldParty(const char *pName);
    TwoDimensionReal GetPatrolNode(const char *pName);
    int GetPatrolNodeLingerFacing(const char *pName);
    int GetPatrolNodeLingerTicks(const char *pName);
    void GetLocationByName(const char *pName, float &sX, float &sY);
    bool DoesPlayerHaveLightSource();
    static float GetLightBoost();
    bool IsStaminaBarVisible();
    void GetLineCollision(int pCollisionLayer, float pXA, float pYA, float pXB, float pYB, float &sReturnX, float &sReturnY);
    bool DoesDoorExist(const char *pName);
    bool IsDoorOpen(const char *pName);
    bool DoesExitExist(const char *pName);
    void GetExaminablePos(const char *pName, int &sLft, int &sTop, int &sRgt, int &sBot);
    bool DoesNoticeExist(void *pPtr);
    void BuildObjectListAt(float pTileX, float pTileY, bool pAllowExtendedActivate);
    void BuildObjectListInRangeOf(float pWorldX, float pWorldY, float pRadius);
    void RegisterObjectToList(const char *pName, int pType);
    int GetNumberOfObjectsAt();
    int GetObjectCodeAt(int pSlot);
    const char *GetObjectNameAt(int pSlot);
    int GetRetreatTimer();
    int GetFollowersTotal();
    virtual bool EnemiesCanAcquireTargets();
    int GetPlayerCamoTimer();
    bool DoesChestExist(const char *pName);

    //--Manipulators
    virtual void SetName(const char *pName);
    virtual void RecheckCompass();
    void SetBasePath(const char *pPath);
    void SetControlEntityID(uint32_t pID);
    void AddFollowEntityID(uint32_t pID);
    void RemoveFollowEntityName(const char *pName);
    void AddLockout(const char *pName);
    void RemoveLockout(const char *pName);
    void SetExaminationScript(const char *pPath);
    void AddExaminable(int pX, int pY, const char *pFiringString);
    void AddExaminable(int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pFiringString);
    void AddExaminableTransition(const char *pName, int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pRoomDest, const char *pPosDest, const char *pPlaySound);
    void AddPatrolNode(const char *pName, int pX, int pY, int pW, int pH);
    void AddEnemy(const char *pName, int pX, int pY, const char *pParty, const char *pAppearance, const char *pScene, int pToughness, const char *pPatrolPath, const char *pFollow);
    void AddChest(const char *pName, int pX, int pY, bool pIsFuture, const char *pContents);
    void SetChestContents(const char *pName, const char *pContents);
    void SetTransitionDestination(const char *pMapDestination, const char *pExitDestination);
    void SetTransitionPostExec(const char *pScriptPath);
    void SetTransitionPosition(int pDirection, float pPercentageOffset);
    void OpenDoor(const char *pName);
    void CloseDoor(const char *pName);
    void SetSwitchState(const char *pName, bool pIsUp);
    void SetReinforcementPulseMode(bool pFlag);
    void SetCanEditFieldAbilities(bool pFlag);
    void RunFieldAbilitiesOnActor(TilemapActor *pActor);
    void SetDontCancelFieldAbilitiesOnceFlag(bool pFlag);
    void BeginRestSequence();
    void EndRestSequenceNow();
    void SetBackground(const char *pPath);
    void SetBackgroundPositions(float pX, float pY);
    void SetBackgroundScrolls(float pX, float pY);
    void FlagCatalystTone();
    static void SetLightBoost(float pValue);
    void SetRenderingDisabled(const char *pLayerName, bool pFlag);
    void SetAnimationDisabled(const char *pLayerName, bool pFlag);
    void SetCollision(int pX, int pY, int pZ, int pCollisionValue);
    void RegisterObjective(const char *pName);
    void SetObjectiveDisplayName(const char *pName, const char *pDisplayName);
    void FlagObjectiveIncomplete(const char *pName);
    void FlagObjectiveComplete(const char *pName);
    void ClearObjectives();
    void RegisterDislocationPack(const char *pName, const char *pLayer, int pXStart, int pYStart, int pWid, int pHei, float pXTarget, float pYTarget);
    void ModifyDislocationPack(const char *pName, float pXTarget, float pYTarget);
    void SetMajorAnimationMode(const char *pAnimationName);
    void AddAnimation(const char *pName, float pX, float pY, float pZ);
    void SetAnimationFromPattern(const char *pName, const char *pPattern, int pFrames);
    void SetAnimationRender(const char *pName, bool pIsRendering);
    void SetAnimationDestinationFrame(const char *pName, float pDestinationFrame);
    void SetAnimationDestinationFrame(const char *pName, float pDestinationFrame, float pFramerate);
    void SetAnimationLoop(const char *pName, float pLoopStartFrame, float pLoopEndFrame, const char *pLoopType, float pFramerateToTarget, float pTicksForWholeLoop);
    void SetAnimationCurrentFrame(const char *pName, float pOverrideFrame);
    void ActivateUnderwaterShader();
    void DeactivateUnderwaterShader();
    void SetParallelCutsceneDuringDialogueFlag(bool pFlag);
    void SetParallelCutsceneDuringCutsceneFlag(bool pFlag);
    void SetScreenShake(int pTicks);
    void SetScreenShakePeriodicity(int pShakeLength, int pTickPeriod, int pScatter);
    void AddShakeSoundEffect(const char *pSoundName);
    void SetRockFallChance(int pChance);
    void AddRockFallImage(const char *pDLPath);
    void SetHideViewcones(bool pFlag);
    void ReceiveRunningMinigame(RunningMinigame *pRunningMinigame);
    void ReceiveKPopMinigame(KPopDanceGame *pMinigame);
    void ReceivePuzzleFightGame(PuzzleFight *pMinigame);
    void ReceiveShakeMinigame(ShakeMinigame *pMinigame);
    void RegisterNotice(ActorNotice *pNotice);
    void RunPostCombatOnEntities();
    void SetRetreatTimer(int pTicks);
    void SetRegionNotifier(const char *pDLPath);
    void SetRegionNotifierFinalScale(float pScale);
    void AllocateRegionNotifierAnimations(int pTotal, int pTicksPerFrame);
    void SetRegionNotifierAnimationFrame(int pFrame, const char *pDLPath);
    void SetCannotOpenMenu(bool pFlag);
    void DisableBordersForOneCutscene();
    void SetSavepointDoesNotLight(const char *pName, bool pFlag);
    void SetShowHealth(bool pFlag);
    void SetHealthValues(int pHealth, int pHealthMax);
    void SetHealthIndicator(const char *pImgPath);
    void SetEmptyIndicator(const char *pImgPath);
    void SetStaminaPosition(float pX, float pY);
    void SetAuraSpawnConditions(int pChance, const char *pDLPath);
    void SetPlayerCamoTimer(int pTicks);

    //--Core Methods
    void RebootMenu();
    TilemapActor *LocatePlayerActor();
    TilemapActor *LocateFollowingActor(int pIndex);
    int GetEffectiveDepth(float pX, float pY);
    bool IsInClimbableZone(float pX, float pY);
    void PulseIgnore(const char *pString);
    void RepositionParty(TilemapActor *pPlayerActor, float pX, float pY);
    bool IsLineClipped(float pX1, float pY1, float pX2, float pY2, int pDepth);
    void RunAutosave();

    ///--Activation
    bool ActivateAt(int pX, int pY);

    ///--Camera Handling
    TwoDimensionReal GetCameraDimensions();
    void SetCameraScale(float pScale);
    void SetCameraLocking(bool pFlag);
    void SetCameraFollowTarget(uint32_t pID);
    void SetCameraPosition(float pLft, float pTop);
    void SetCameraPosition(TwoDimensionReal pPosition);
    TwoDimensionReal GetIdealCameraPosition(float pTargetX, float pTargetY);
    void UpdateCameraPosition();

    ///--Chase Offsets
    static void CalibrateChaseOffsets(int pFollowIndex, bool pIsRunning, float &sOffsetX, float &sOffsetY);

    ///--Debug Handling
    void Debug_WipeFieldEnemies();
    void Debug_RespawnFieldEnemies();
    void Debug_TransitionTo(const char *pMapTarget);
    void Debug_FireCutscene(const char *pCutsceneTarget);

    ///--Deep Layers
    void CreateDeepLayer(int pCollision);
    DeepLayerPack *GetDeepLayerPack(int pCollision);
    DeepLayerPack *GetDeepLayerPackForEntity(TilemapActor *pActor);
    void SetDeepLayerPreventsRun(int pCollision, bool pPreventsRun);
    void SetDeepLayerPixelSink(int pCollision, int pPixelsSink);
    void SetDeepLayerOverlayName(int pCollision, const char *pName);
    void SetDeepLayerWalkSound(int pCollision, const char *pName);
    void SetDeepLayerRunSound(int pCollision, const char *pName);

    ///--Enemy Handling
    static bool IsEnemyDead(const char *pName);
    static bool IsEnemyMugged(const char *pName);
    const char *GetEnemyNameFromPattern(const char *pPattern);
    void RegisterEnemyPack(SpawnPackage *pPackage);
    static void KillEnemy(const char *pName);
    static void MarkEnemyMugged(const char *pName);
    void KillSpawnedEnemy(const char *pName);
    static void WipeDestroyedEnemies();
    void SpawnEnemies();
    void CheckActorAgainstInvisZones(TilemapActor *pActor);

    ///--Layering
    void AllocateForegroundPacks(int pTotal);
    void SetForegroundImage(int pSlot, const char *pPath);
    void SetForegroundOffsets(int pSlot, float pOffsetX, float pOffsetY, float pScalerX, float pScalerY);
    void SetForegroundAutoscroll(int pSlot, float pAutoscrollX, float pAutoscrollY);
    void SetForegroundAlpha(int pSlot, float pTargetAlpha, int pTicks);
    void SetForegroundScaler(int pSlot, float pScale);
    void AllocateBackgroundPacks(int pTotal);
    void SetBackgroundImage(int pSlot, const char *pPath);
    void SetBackgroundOffsets(int pSlot, float pOffsetX, float pOffsetY, float pScalerX, float pScalerY);
    void SetBackgroundAutoscroll(int pSlot, float pAutoscrollX, float pAutoscrollY);
    void SetBackgroundAlpha(int pSlot, float pTargetAlpha, int pTicks);
    void SetBackgroundScaler(int pSlot, float pScale);

    ///--Lighting
    bool IsLightingActive();
    void ActivateLighting();
    void DeactivateLighting();
    void SetAmbientLight(float pR, float pG, float pB, float pA);
    void RegisterLight(AdventureLight *pLight);
    void ModifyLightColor(const char *pName, float pR, float pG, float pB, float pA);
    void EnableLight(const char *pName);
    void DisableLight(const char *pName);
    void AttachLightToEntity(const char *pLightName, const char *pEntityName);
    void ActivatePlayerLight();
    void DeactivatePlayerLight();
    void SetPlayerLightPower(int pTicksLeft);
    void SetPlayerLightPowerMax(int pTicksLeft);
    void SetPlayerLightNoDrain(bool pFlag);
    void UpdateEntityLightPositions();
    void RenderLights();

    ///--Music Layering
    static bool IsLayeringMusic();
    static void ActivateMusicLayering();
    static void DeactivateMusicLayering();
    static void SetForceZeroVolumeForLayers(bool pFlag);
    static void SetLowIntensityTicks(int pTicks);
    static void UpdateMusicLayering();

    ///--Notifications
    NotificationPack *RegisterNotification(const char *pString);
    void HandleNotificationForItem(bool pObtainItem, const char *pItemName, const char *pPrefix);
    void ReresolveNotificationPositions();
    void UpdateNotifications();
    void RenderNotifications();

    ///--Object Parsing
    bool PositionExists(const char *pName);
    float GetPositionX(const char *pName);
    float GetPositionY(const char *pName);
    void RegisterStaircase(const char *pName, float pX, float pY, float pW, float pH, const char *pDir, const char *pExitDest, const char *pMapDest);
    virtual void ParseObjectData();
    void HandleObject(ObjectInfoPack *pPack);
    void RemoveObject(const char *pObjectType, const char *pObjectName);
    void SetActorToPositionPack(TilemapActor *pActor, const char *pPositionPack);
    void ResetActorGraphics();
    void ResetChestsDuringRest();

    ///--Patrol Nodes
    void EmulateEnemyMovement();
    void RenderPatrolNodes();

    ///--Saving
    void WriteToBuffer(StarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *fInfile);

    ///--Script Fading
    void SetAlternateBlending(bool pFlag);
    void ActivateScriptFade(int pTicks, StarlightColor pStartColor, StarlightColor pEndColor, int pDepthFlag, bool pHoldsOnComplete);
    void UpdateScriptFade();
    void RenderScriptFade(int pAtDepth);

    ///--[ ===== Shooting ===== ]
    //--System
    void ActivateShooting();

    //--Property Queries
    //--Manipulators
    void MarkShootingAsTennis(uint32_t pShooterID);
    void CancelShooting();
    void ClearTennisShooting();
    void SetShootingDepth(int pDepth);

    //--Core Methods
    void FireShot();
    bool CheckPointForHit(float pX, float pY, StarLinkedList *pExaminableList, StarLinkedList *pActorList);
    void ShootAlongVector();
    void ClearShootLists();
    void CheckPointForHitIntoMaster(float pX, float pY);

    //--Update
    bool UpdateShootingControls();
    bool UpdateTennisControls();

    ///--Transitions
    bool HandleTransition();
    void HandleTransitionToRandom();
    void RepositionActorToExit(const char *pExitName, int pDirection, float pPercentage, TilemapActor *pActor);
    static void ReallocateRemappings(int pTotal);
    static void ReallocateRemappingsRetain(int pTotal);

    ///--Triggers
    void SetTriggerScript(const char *pPath);
    bool CheckActorAgainstTriggers(TilemapActor *pActor, bool pOnlyEmulate);

    private:
    //--Private Core Methods
    bool CheckActorAgainstExits(TilemapActor *pActor, bool pOnlyEmulate);
    virtual void CompileAfterParse();

    public:
    //--Update
    virtual void Update();
    void ExecuteFieldAbility(int pIndex);
    bool MovePartyMembers();
    bool AutofoldPartyMembers();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(StarLinkedList *pRenderList);
    virtual void Render();
    void RenderEntities(bool pIsBeforeTiles);
    void RenderTilemap(float pLft, float pTop, float pRgt, float pBot);
    void RenderDoors();
    void RenderClimbables();
    void RenderChests();
    void RenderExits();
    void RenderSwitches();
    void RenderSavePoints();
    void RenderViewcones();
    void RenderPerMapAnimations();
    void RenderEntityUILayer();
    void RenderWorldUI();
    void RenderRegionOverlay();

    //--Pointer Routing
    AdventureMenu *GetMenu();
    AdventureDebug *GetDebugMenu();
    KPopDanceGame *GetDanceMinigame();
    PuzzleFight *GetPuzzleFight();
    ShakeMinigame *GetShakeMinigame();
    FieldAbilityPack *GetActiveFieldAbilityPack();
    StarLinkedList *GetPatrolNodeList();

    //--Static Functions
    static AdventureLevel *Fetch();
    static void SetMusic(const char *pName, bool pSlowly);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AL_Create(lua_State *L);
int Hook_AL_ParseSLF(lua_State *L);
int Hook_AL_GetProperty(lua_State *L);
int Hook_AL_SetProperty(lua_State *L);
int Hook_AL_BeginTransitionTo(lua_State *L);
int Hook_AL_RemoveObject(lua_State *L);
int Hook_AL_PulseIgnore(lua_State *L);
int Hook_AL_CreateObject(lua_State *L);
