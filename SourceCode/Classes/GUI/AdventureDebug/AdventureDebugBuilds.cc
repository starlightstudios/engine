//--Base
#include "AdventureDebug.h"

//--Classes
#include "BuildPackage.h"

//--CoreClasses
#include "StarFont.h"
#include "StarPointerSeries.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///======================================= Documentation ==========================================
//--Functions related to the build system which allows automatic creation of game distributions.

///======================================== File Output ===========================================
void AdventureDebug::OutputPurgeFile(const char *pFilePath)
{
    ///--[Documentation]
    //--Writes a file to the hard drive at the given path that contains a series of BP_SetProperty()
    //  commands that can be used to clear/dummy out files to reduce hard drive/RAM used by a game
    //  that doesn't actually need them.
    if(!pFilePath) return;

    ///--[Error Checks]
    //--Make sure the file can be opened.
    FILE *fOutfile = fopen(pFilePath, "w");
    if(!fOutfile)
    {
        fprintf(stderr, "AdventureDebug:OutputPurgeFile() - Warning. Unable to open file %s for writing. Stopping.\n", pFilePath);
        return;
    }

    //--Make sure the StarPointerSeries has information to actually write.
    #ifndef SPS_TRACKS_DUMMIES
    if(true)
    {
        fprintf(stderr, "AdventureDebug:OutputPurgeFile() - Warning. SPS_TRACKS_DUMMIES is not defined, no data to track. Stopping.\n");
        fclose(fOutfile);
        return;
    }

    //--Valid data, continue.
    #else

    ///--[Setup]
    //--Fast access pointers.
    StarLinkedList *rSafeRemoveList = StarPointerSeries::xSafeRemovalList;
    StarLinkedList *rPurgePackList  = StarPointerSeries::xAllExtractOrders;

    ///--[Clear]
    //--Run across all purge packs and clear their valid lists.
    PurgeDatafile *rDatafilePack = (PurgeDatafile *)rPurgePackList->PushIterator();
    while(rDatafilePack)
    {
        rDatafilePack->ClearValidList();
        rDatafilePack = (PurgeDatafile *)rPurgePackList->AutoIterate();
    }

    ///--[Build List of Purges, By Datafile]
    //--Run across all entries. The name of the entry is the path required.
    void *rDummyPtr = rSafeRemoveList->PushIterator();
    while(rDummyPtr)
    {
        //--Get the name. This is the DLPath.
        const char *rDLPath = rSafeRemoveList->GetIteratorName();

        //--Scan the stored extract orders, looking for the original image name.
        rDatafilePack = (PurgeDatafile *)rPurgePackList->PushIterator();
        while(rDatafilePack)
        {
            //--Get the infile name if it is in this pack. If it isn't, this will come back NULL.
            const char *rInfileName = rDatafilePack->IsPathInThisDatafile(rDLPath);
            if(rInfileName)
            {
                rDatafilePack->StoreValidInfileName(rInfileName);
                rPurgePackList->PopIterator();
                break;
            }

            //--Next.
            rDatafilePack = (PurgeDatafile *)rPurgePackList->AutoIterate();
        }

        //--Next.
        rDummyPtr = rSafeRemoveList->AutoIterate();
    }

    ///--[Writing]
    //--At this point, each PurgeDatafile now contains a list of object names. Begin writing to the file.
    rDatafilePack = (PurgeDatafile *)rPurgePackList->PushIterator();
    while(rDatafilePack)
    {
        //--Call.
        rDatafilePack->WriteToFile(fOutfile);

        //--Next.
        rDatafilePack = (PurgeDatafile *)rPurgePackList->AutoIterate();
    }

    ///--[Font Listing]
    //--Setup.
    StarLinkedList *rFontList = DataLibrary::Fetch()->GetMasterFontList();

    //--Headings.
    uint32_t tTotalPixels = 0;
    fprintf(fOutfile, "Font information.\n");
    fprintf(fOutfile, "Total Fonts: %i\n", rFontList->GetListSize());
    StarFont *rFont = (StarFont *)rFontList->PushIterator();
    while(rFont)
    {
        //--Compute the number of pixels in the precache.
        uint32_t tPixels = rFont->GetPrecacheSizeX() * rFont->GetPrecacheSizeY();
        tTotalPixels = tTotalPixels + tPixels;

        //--Write.
        fprintf(fOutfile, "%s - %i\n", rFont->GetName(), tPixels);

        //--Next.
        rFont = (StarFont *)rFontList->AutoIterate();
    }

    //--Footer.
    fprintf(fOutfile, "\nFont statistics:\n");
    fprintf(fOutfile, "Total pixels in fonts: %i\n", tTotalPixels);
    fprintf(fOutfile, "Expected bytes spent on fonts: %i\n", tTotalPixels * sizeof(uint32_t));
    fprintf(fOutfile, "In MB %i\n", tTotalPixels * sizeof(uint32_t) / 1024 / 1024);

    #endif

    ///--[Clean Up]
    fclose(fOutfile);
}

///========================================== Update ==============================================
void AdventureDebug::UpdateBuilds()
{
    ///--[ =============== Documentation ================ ]
    //--Used for automated build construction.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[ ================== Activate ================== ]
    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        ///--[Games List]
        //--Shows a list of games. Selecting one activates that list.
        if(mGameCursor == -1)
        {
            //--If this is the emulation option:
            if(mMenuCursor == ADM_BUILD_TOGGLE_EMULATION)
            {
                mIsEmulation = !mIsEmulation;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--If this is the emulation option:
            else if(mMenuCursor == ADM_BUILD_TOGGLE_FILE)
            {
                mBuildFile = (mBuildFile + 1) % ADM_BUILD_FILE_TOTAL;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Immediately write an output file. Does nothing if SPS_TRACKS_DUMMIES is not defined.
            else if(mMenuCursor == ADM_BUILD_WRITE_REMOVE_FILE)
            {
                //--Print a warning:
                #ifndef SPS_TRACKS_DUMMIES
                    fprintf(stderr, "Warning: SPS_TRACKS_DUMMIES is not defined, no dummy data to output.\n");

                //--Print the file.
                #else
                    OutputPurgeFile("DeveloperLocal/PurgeFile.lua");

                #endif

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Otherwise, activate a game entry.
            else
            {
                //--Make sure the game has at least one build.
                StarLinkedList *rGameList = BuildPackage::FetchBuildList();
                StarLinkedList *rBuildList = (StarLinkedList *)rGameList->GetElementBySlot(mMenuCursor - ADM_BUILD_ZERO_BUILD);
                if(rBuildList && rBuildList->GetListSize() > 0)
                {
                    mGameCursor = mMenuCursor - ADM_BUILD_ZERO_BUILD;
                    mMenuCursor = 0;
                    mMenuMax = rBuildList->GetListSize();
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                }
                else
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
            }
        }
        ///--[Entries List]
        //--Shows the builds list. Each one is a package, run when selected.
        else
        {
            //--Get the build list and package.
            StarLinkedList *rGameList = BuildPackage::FetchBuildList();
            StarLinkedList *rBuildList = (StarLinkedList *)rGameList->GetElementBySlot(mGameCursor);
            if(rBuildList)
            {
                BuildPackage *rPackage = (BuildPackage *)rBuildList->GetElementBySlot(mMenuCursor);
                if(!rPackage)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                else
                {
                    //--Execute with various file outputs.
                    if(mBuildFile == ADM_BUILD_FILE_NONE)
                        rPackage->Execute(mIsEmulation, NULL);
                    else if(mBuildFile == ADM_BUILD_FILE_STDERR)
                        rPackage->Execute(mIsEmulation, stderr);
                    else
                        rPackage->ExecuteToPath(mIsEmulation, "Build Output.txt");

                    //--SFX.
                    AudioManager::Fetch()->PlaySound("Menu|Save");
                }
            }
        }
    }
    ///--[ =================== Cancel =================== ]
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        ///--[Game Selection]
        //--Return to main menu.
        if(mGameCursor == -1)
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_BUILDS;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        ///--[Build Selection]
        //--Return to game menu.
        else
        {
            mMenuCursor = mGameCursor + 2;
            mGameCursor = -1;
            mMenuMax = BuildPackage::FetchBuildList()->GetListSize() + 2;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
}
