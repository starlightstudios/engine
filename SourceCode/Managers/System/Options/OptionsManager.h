///====================================== Options Manager =========================================
//--Holds options which can be publically accessed and modified at any
//  time. Also contains Lua functions and other routines for accessing the
//  options, and automatic security protections.

//--This manager is also controlling various system functions, such as storing
//  and handling the command line and doing some low-level requests like gear
//  queries. It's a quadruple threat!

//--PLEASE NOTE: The macros for option manipulation are mostly there for Lua
//  and are not supposed to be used by the programmer, the options for C++ use
//  are public variables, or can be found in their respective managers.

#pragma once

#include "Definitions.h"
#include "Structures.h"

///======================================== Local Macros ==========================================
//--This macro compresses getting a translation line from the OptionsManager.
#define GetUIText(X) OptionsManager::FetchTranslation(X)

///===================================== Local Definitions ========================================
#define OM_ERROR_INT -700
#define OM_ERROR_FLOAT -700.0f
#define OM_ERROR_STRING NULL
#define OM_ERROR_BOOL false

#define OM_DEBUGVARS 10

#define OM_ORBITCODE_UNKNOWN -1

#define LOWRES_SCALE 0.25f
#define LOWRES_SCALEINV 4.00f

///--[Special Macros]
//--Audio abstract volumes. This definition is duplicated in the Audio Manager.
#ifndef AM_DEFAULT_ABSTRACT_VOLUME
#define AM_DEFAULT_ABSTRACT_VOLUME 0.50f
#define AM_ABSTRACT_VOLUME_TOTAL 30
#endif

///===================================== Local Structures =========================================
///--[OptionPack]
//--Defines an option, which can be any basic type (int/float/bool/string). Options store which file
//  they should be printed to and how, allowing them to be saved to a config file easily.
typedef struct OptionPack
{
    //--Members
    bool mWasChanged;
    bool mIsDefinedExternally;
    bool mIsProgramLocal;
    bool mOwnsPointer;
    void *rPtr;
    int mTypeFlag;
    char mFilePath[STD_MAX_LETTERS];

    //--Functions
    void Initialize()
    {
        mWasChanged = false;
        mIsDefinedExternally = false;
        mIsProgramLocal = false;
        rPtr = NULL;
        mOwnsPointer = false;
        mTypeFlag = POINTER_TYPE_FAIL;
        memset(mFilePath, 0, sizeof(mFilePath));
    }
    void TakeOwnershipOfPointer()
    {
        mOwnsPointer = true;
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        OptionPack *rOptionsPack = (OptionPack *)pPtr;
        if(rOptionsPack->mOwnsPointer)
        {
            free(rOptionsPack->rPtr);
        }
        free(rOptionsPack);
    }
}OptionPack;

///========================================== Classes =============================================
class OptionsManager
{
    private:
    //--System
    bool mErrorFlag;

    //--Storage
    StarLinkedList *mOptionList; //OptionPack *, master
    StarLinkedList *mDebugFlagList; //bool *, master
    StarLinkedList *mActiveModList;//dummy pointer

    //--Debug Variables
    float mDebugVariables[OM_DEBUGVARS];

    //--Console Commands
    static StarLinkedList *xConsoleCommandList;

    //--Configuration File List
    StarLinkedList *mConfigFilePaths;//char *, master

    //--GUI Translations
    int mTranslationStringsTotal;
    char **mTranslationStrings;

    protected:

    public:
    //--System
    OptionsManager();
    ~OptionsManager();

    //--Public Variables. Configuration.
    bool mShowExplicitErrorsForOptionQueries;
    bool mIsFullscreen;
    bool mIsMemoryMode;
    int mWinMandateMonitor;
    int mWinSpawnX;
    int mWinSpawnY;
    int mWinSizeX;
    int mWinSizeY;
    int mMusicVolume;
    int mSoundVolume;
    int mAbstractVolume[AM_ABSTRACT_VOLUME_TOTAL];

    //--Hardware Configuration
    bool mForcePowerOfTwo;
    bool mBlockDepthBuffer;
    bool mBlockStencilbuffer;
    int mRequestedAccelerateVisual;
    int mRequestedOpenGLMajor;
    int mRequestedOpenGLMinor;

    //--Engine Options
    bool mNoAudioSuppression;
    bool mForceDirectAspectRatios;
    bool mBlockDoubleBuffering;
    bool mUseRAMLoading;

    //--Statics
    static bool xIsDefiningOption;
    static char *xAutorun;

    //--Property Queries
    bool GetErrorFlag();
    bool GetDebugFlag(const char *pName);
    OptionPack *GetOption(const char *pName);
    int GetOptionType(const char *pName);
    int GetOptionI(const char *pName);
    float GetOptionF(const char *pName);
    char *GetOptionS(const char *pName);
    bool GetOptionB(const char *pName);
    float GetDebugVar(int pSlot);
    int GetOrbitCode();
    const char *GetTranslatedText(int pIndex);
    bool IsModActive(const char *pName);

    //--Manipulators
    OptionPack *AddOption(const char *pName, void *pPtr, int pType, bool pOwnPointer, const char *pFileName);
    void SetOptionI(const char *pName, int pValue);
    void SetOptionF(const char *pName, float pValue);
    void SetOptionS(const char *pName, const char *pValue);
    void SetOptionB(const char *pName, bool pValue);
    void RemOption(const char *pName);
    void SetDebugVar(int pSlot, float pValue);
    void RegisterConfigPath(const char *pPath);
    void AllocateTranslationStrings(int pTotal);
    void SetTranslationString(int pIndex, const char *pString);
    void SetModActive(const char *pName);

    //--Core Methods
    void SetupOptionList();
    bool CheckPtr(OptionPack *pPack, int pExpectedType);
    void PostProcess();
    void PrintReport();
    void RunConfigFiles();
    void ClearUpdateFlags();
    void ClearModActivity();

    //--Update
    //--File I/O
    void WriteConfigFiles();
    void WriteLoadFile(const char *pPath);

    //--Drawing
    //--Pointer Routing
    StarLinkedList *GetActiveModList();

    //--Static Functions
    static OptionsManager *Fetch();
    static const char *FetchTranslation(int pIndex);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);

    //--Command Functions
    static bool xErrorFlag;
};

//--Hooking Functions
int Hook_OM_RegisterConfigPath(lua_State *L);
int Hook_OM_OptionExists(lua_State *L);
int Hook_OM_DefineOption(lua_State *L);
int Hook_OM_GetOption(lua_State *L);
int Hook_OM_SetOption(lua_State *L);
int Hook_OM_WriteConfigFiles(lua_State *L);
int Hook_OM_WriteLoadFile(lua_State *L);
int Hook_OM_SetTranslation(lua_State *L);
int Hook_OM_GetProgramTicksElapsed(lua_State *L);
int Hook_OM_GetAutorun(lua_State *L);
int Hook_OM_ClearAutorun(lua_State *L);
int Hook_OM_IsModActive(lua_State *L);
int Hook_OM_SetModActive(lua_State *L);
int Hook_OM_ClearModActivity(lua_State *L);
