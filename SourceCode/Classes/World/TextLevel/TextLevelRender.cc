//--Base
#include "TextLevel.h"

//--Classes
#include "DeckEditor.h"
#include "StringEntry.h"
#include "TextLevOptions.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

///========================================== Drawing =============================================
void TextLevel::AddToRenderList(StarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void TextLevel::Render()
{
    ///--[Setup]
    //--Flip this flag.
    MapManager::xHasRenderedMenus = true;

    //--Error check.
    if(!Images.mIsReady) return;

    ///--[Parts]
    //--GUI, which is almost everything.
    RenderGUI();

    //--Handle the combat render.
    if(mIsCombatMode) RenderCombat();

    ///--[Fade-In/Out]
    if(mIsFadingIn)
    {
        float cAlpha = 1.0f - EasingFunction::Linear(mFadeTimer, TL_FADE_TICKS);
        StarBitmap::DrawFullBlack(cAlpha);
    }
    else if(mIsFadingOut)
    {
        float cAlpha = EasingFunction::Linear(mFadeTimer, TL_FADE_TICKS);
        StarBitmap::DrawFullBlack(cAlpha);
    }
}
void TextLevel::RenderGUI()
{
    ///--[Static Parts]
    //--Backing.
    glDepthMask(false);
    glTranslatef(0.0f, 0.0f, TL_DEPTH_GUI);
    StarBitmap::DrawFullColor(StarlightColor::cxWhite);

    //--Render the UI dividers.
    if(!mIsDarkMode)
    {
        Images.Data.rTextInput->Draw();
    }
    else
    {
        Images.Data.rTextInputDark->Draw();
    }
    glDepthMask(true);

    //--Render the automap over this area of the GUI. It uses stencils to restrict itself.
    glTranslatef(0.0f, 0.0f, -TL_DEPTH_GUI);
    glTranslatef(0.0f, 0.0f, TL_DEPTH_ROOM);
    RenderAutomap();
    glTranslatef(0.0f, 0.0f, -TL_DEPTH_ROOM);
    glTranslatef(0.0f, 0.0f, TL_DEPTH_GUI);

    //--Navigation pane, goes over the automap. Renders nothing if flagged!
    if(!(mNavButtonFlags & TL_NAV_SHOWNOTHING))
    {
        //--Navigation buttons. Can be toggled off.
        if(!mNoNavigationButtons)
        {
            //--Iterate.
            uint16_t tFlag = 0x0001;
            for(int i = 0; i < TL_NAV_INDEX_NORMAL_TOTAL; i ++)
            {
                //--Fullbright.
                if(mNavButtonFlags & tFlag)
                {
                    StarlightColor::ClearMixer();
                }
                //--Darkened.
                else
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.15f);
                }

                //--Render.
                Images.Data.rNavImg[i]->Draw(mNavButtonDim[i].mLft, mNavButtonDim[i].mTop);

                //--Next.
                tFlag = tFlag * 2;
            }

            //--Clean.
            StarlightColor::ClearMixer();
        }

        //--Render the zoom handler. The bar coincides with the zoom-down button.
        if(!mNoZoomButtons)
        {
            Images.Data.rNavZoomBar->Draw(mNavButtonDim[TL_NAV_INDEX_ZOOMDN].mLft, mNavButtonDim[TL_NAV_INDEX_ZOOMDN].mTop);
            float cTickLft = 848.0f + (12.0f * (TL_SCALES_TOTAL - mScaleIndex - 1));
            float cTickTop = 377.0f;
            Images.Data.rNavZoomTick->Draw(cTickLft, cTickTop);
        }
    }

    //--Text subroutine.
    RenderTextZone();

    //--Portrait zone handler.
    RenderPortraitWindow();

    //--Locality window.
    RenderLocalityWindow();

    //--Popup window. Automatically fails if no commands are available.
    RenderPopupWindow();

    //--Render the Deck Editor. Does nothing if it's not visible.
    mDeckEditor->Render();

    //--Render the options screen. Does nothing if it's not visible.
    mOptionsScreen->Render();

    //--Render the player's map. Does nothing if it's not visible.
    RenderPlayerMap();

    //--GUI Cleanup.
    glTranslatef(0.0f, 0.0f, -TL_DEPTH_GUI);

    //--Ending sequence: Black out the screen.
    if(mIsMajorDialogueMode)
    {
        //--Compute timer.
        int tUseTimer = mMajorDialogueTimer;
        if(mMajorDialogueFadeOut) tUseTimer = TL_ENDING_TICKS;
        float cAlpha = tUseTimer / (float)TL_ENDING_TICKS;

        //--Box.
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cAlpha));
    }
    //--Exiting major dialogue. Fade back in.
    else if(mIsMajorDialogueModeExit)
    {
        //--Compute timer.
        int tUseTimer = mMajorDialogueTimer;
        if(mMajorDialogueFadeOut) tUseTimer = TL_ENDING_TICKS;
        float cAlpha = tUseTimer / (float)TL_ENDING_TICKS;

        //--Box.
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cAlpha));
    }

    //--Render WorldDialogue GUI if it's visible.
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible() && mMajorDialogueTimer > 15)
    {
        rWorldDialogue->Render();
    }

    //--Ending fade-out sequence.
    if(mIsMajorDialogueMode && mMajorDialogueFadeOut)
    {
        float cAlpha = 1.0f - (mMajorDialogueTimer / (float)TL_ENDING_TICKS);
        StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cAlpha));
    }
}
void TextLevel::RenderTextZone()
{
    ///--[Documentation and Setup]
    //--Text is rendered bottom-up. Rendering stops when we reach the top of the region.
    StarFont *rRenderFont = Images.Data.rFontMenuSize20;
    if(mUseLargeText)
    {
        rRenderFont = Images.Data.rFontMenuSize30;
    }

    //--Sizes.
    float cTxtRenderX = 15.0f;
    float cTxtRenderY = 700.0f;
    float tTxtRenderY = cTxtRenderY;

    //--Color setup.
    float cTextCol = 0.0f;
    StarlightColor cTextColor = StarlightColor::cxBlack;
    if(mIsDarkMode)
    {
        cTextCol = 1.0f;
        cTextColor = StarlightColor::cxWhite;
    }

    //--Dimensions.
    TwoDimensionReal cFrameDim;
    cFrameDim.SetWH(2.0f, 404.0f, 968.0f, 362.0f);
    float cTxtEndRenderY = cFrameDim.mTop;

    //--Stencil work.
    glEnable(GL_STENCIL_TEST);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_TEXT, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    Images.Data.rMask_Text->Draw();

    //--Now turn on stencil-controlled rendering.
    Images.Data.rMapParts->Bind();
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilMask(0xFF);
    glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_TEXT, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    //--Text renders black.
    cTextColor.SetAsMixer();

    //--Get the string.
    bool tHasRenderedYet = false;
    int i = 0;
    float tUseRenderX = cTxtRenderX;
    bool tDontRenderInputMarker = false;
    bool tLastLineWasCarriageReturn = false;
    bool tStopOnNextPass = false;
    TextPack *rPackage = (TextPack *)mTextLog->PushIterator();
    StarFont *rStoredFont = (StarFont *)mFontLog->PushIterator();
    while(rPackage)
    {
        //--If the package is not visible, don't render it.
        if(rPackage->mRenderTimer < 0)
        {
            i ++;
            tDontRenderInputMarker = true;
            rPackage = (TextPack *)mTextLog->AutoIterate();
            rStoredFont = (StarFont *)mFontLog->AutoIterate();
            continue;
        }

        //--If the package is below the render offset, don't render it.
        if(i < mTextScrollOffset)
        {
            i ++;
            rPackage = (TextPack *)mTextLog->AutoIterate();
            rStoredFont = (StarFont *)mFontLog->AutoIterate();
            continue;
        }

        //--Compute alpha.
        float tAlpha = rPackage->mRenderTimer / (float)TL_TIME_PER_LINE;
        StarlightColor::SetMixer(cTextCol, cTextCol, cTextCol, tAlpha);

        //--If the string begins with "[POS:XXX]" modify the position of the rendering cursor.
        int tStartOfString = 0;
        tUseRenderX = cTxtRenderX;
        if(!strncasecmp(rPackage->mText, "[POS:", 5))
        {
            //--Resolve position.
            char tNumber[4];
            tNumber[0] = rPackage->mText[5];
            tNumber[1] = rPackage->mText[6];
            tNumber[2] = rPackage->mText[7];
            tNumber[3] = '\0';

            //--Change to a number.
            tUseRenderX = atof(tNumber);

            //--Change the starting letter.
            tStartOfString = 9;
        }

        //--Render the string.
        if(!tHasRenderedYet)
        {
            tHasRenderedYet = true;
            tTxtRenderY = tTxtRenderY - rStoredFont->GetTextHeight();
        }
        rStoredFont->DrawText(tUseRenderX, tTxtRenderY, 0, 1.0f, &rPackage->mText[tStartOfString]);
        float cTxtStepRate = rStoredFont->GetTextHeight();

        //--Move the line down.
        if(tStartOfString == 0)
        {
            tTxtRenderY = tTxtRenderY - cTxtStepRate;
        }

        //--If the last two letters of the line were a carriage return:
        int tLen = (int)strlen(rPackage->mText);
        if(tLen == 1 && rPackage->mText[0] == ' ')
        {
            //--Multiple instances in a row.
            if(tLastLineWasCarriageReturn)
            {
                tTxtRenderY = tTxtRenderY + (cTxtStepRate * 1.20f);
            }
            tLastLineWasCarriageReturn = true;

        }
        else
        {
            tLastLineWasCarriageReturn = false;
        }

        //--Stop if we pass the top.
        if(tStopOnNextPass)
        {
            mTextLog->PopIterator();
            mFontLog->PopIterator();
            break;
        }
        if(tTxtRenderY <= cTxtEndRenderY) tStopOnNextPass = true;

        //--Next.
        i ++;
        rPackage = (TextPack *)mTextLog->AutoIterate();
        rStoredFont = (StarFont *)mFontLog->AutoIterate();
    }
    StarlightColor::ClearMixer();

    ///--[Input Marker]
    //--Either prompts the player for input or informs them they need to press a key.
    if(!tDontRenderInputMarker)
    {
        //--No block pending: Get whatever the current string is and render it. It always renders with a > in front of it.
        if(!mIsBlockPending)
        {
            cTextColor.SetAsMixer();
            const char *rEntryString = mLocalStringEntry->GetString();
            if(rEntryString) rRenderFont->DrawTextArgs(cTxtRenderX, cTxtRenderY, 0, 1.0f, "> %s", rEntryString);
        }
        //--Block is pending.
        else
        {
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            rRenderFont->DrawText(cTxtRenderX, cTxtRenderY, 0, 1.0f, "[Press any key to continue]");
            StarlightColor::ClearMixer();
        }
    }
    StarlightColor::ClearMixer();

    ///--[Scrollbar]
    //--Setup.
    glDisable(GL_STENCIL_TEST);
    float cBarHei = 300.0f;
    float cMax = (float)TL_MAX_TEXTLOG_LINES;
    float cUsed = 10.0f;
    float cLft = 948.0f;
    float cRgt = cLft + 22.0f;
    float cTop = (((float)mTextScrollOffset / cMax) * cBarHei);
    float cBot = cTop + ((cUsed / cMax) * cBarHei);

    //--Reverse the top and bottom. The scrollbar goes up.
    cTop = 728.0f - cTop;
    cBot = 728.0f - cBot;

    //--Scrollbar.
    StarlightColor::MapRGBAI(137, 110, 83, 255).SetAsMixer();
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);

    //--Clean.
    StarlightColor::ClearMixer();
}
void TextLevel::RenderPortraitWindow()
{
    ///--[Setup]
    //--Renders the top-right portrait window. This typically shows the player, or will show the last registered image pack
    //  if one exists.
    TwoDimensionReal cFrameDim;
    cFrameDim.SetWH(982.0f, 3.0f, 382.0f, 450.0f);

    //--Color setup.
    StarlightColor cTextColor = StarlightColor::cxBlack;
    if(mIsDarkMode)
    {
        cTextColor = StarlightColor::cxWhite;
    }

    //--Stencil work.
    glEnable(GL_STENCIL_TEST);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_PORTRAIT, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    Images.Data.rMask_Character->Draw();

    //--Now turn on stencil-controlled rendering.
    Images.Data.rMapParts->Bind();
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilMask(0xFF);
    glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_PORTRAIT, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    ///--[Image Render]
    //--Rendering of the image that is seen in the top right. First, figure out if there's a previous
    //  image for crossfading.
    bool tHasPreviousImage = false;
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        if(rPreviousRenderImg[i]) {tHasPreviousImage = true; break;}
    }

    //--Use the faster routine.
    if(mImgTransitionTimer < 1 || !tHasPreviousImage)
    {
        RenderPortraitNormal();
    }
    //--We need to crossfade the portraits.
    else
    {
        RenderPortraitCrossfade();
    }

    ///--[Name Render]
    //--Determine if we're using the normal or override set.
    StarBitmap **rUseArray = rStandardRenderImg;
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        if(rOverrideRenderImg[i] != NULL)
        {
            rUseArray = rOverrideRenderImg;
            break;
        }
    }

    //--Error check.
    const char *rUseName = mStandardName;
    if(rUseArray == rOverrideRenderImg) rUseName = mOverrideName;
    if(!rUseName) { glDisable(GL_STENCIL_TEST); return; }

    ///--[Stats Render]
    //--Variables.
    float tYPos = 0.0f;
    float cYIncrement = Images.Data.rFontMenuSize20->GetTextHeight();

    //--Renders the player's last-uploaded statistics. Does not occur if suppressed.
    if(!mHidePlayerStats)
    {

        //--Typing Combat.
        if(mUseCombatType == TL_COMBAT_TYPING)
        {
            cTextColor.SetAsMixer();
            Images.Data.rFontMenuSize20->DrawTextArgs(cFrameDim.mLft + 7.0f, cFrameDim.mTop + 0.0f + tYPos, 0, 1.0f, "HP: %i / %i", mPlayerHP, mPlayerHPMax);
            tYPos = tYPos + cYIncrement;
            Images.Data.rFontMenuSize20->DrawTextArgs(cFrameDim.mLft + 7.0f, cFrameDim.mTop + 0.0f + tYPos, 0, 1.0f, "Atk: %i", mPlayerAtk);
            tYPos = tYPos + cYIncrement;
            Images.Data.rFontMenuSize20->DrawTextArgs(cFrameDim.mLft + 7.0f, cFrameDim.mTop + 0.0f + tYPos, 0, 1.0f, "Def: %i", mPlayerDef);
            tYPos = tYPos + cYIncrement;
        }
        //--Word Combat. Renders the player's HP as chips.
        else if(mUseCombatType == TL_COMBAT_WORD)
        {
            //--Translate.
            float cLft = 990.0f;
            float cTop =  15.0f;
            glTranslatef(cLft, cTop, 0.0f);

            //--Halve the size.
            glScalef(0.5f, 0.5f, 1.0f);

            //--Each HP is half a heart.
            float cHeartSpc = 32.0f;
            for(int i = 0; i < mPlayerHPMax; i += 2)
            {
                //--If we're under the current HP:
                if(i < mPlayerHP)
                {
                    //--If we have an odd number of HP, draw the half-heart.
                    if(i == mPlayerHP - 1)
                    {
                        Images.Data.rHPIndicatorH->Draw((cHeartSpc * (i/2.0f)), 0.0f);
                    }
                    //--Otherwise, full heart.
                    else
                    {
                        Images.Data.rHPIndicatorF->Draw((cHeartSpc * (i/2.0f)), 0.0f);
                    }
                }
                //--Otherwise, empty.
                else
                {
                    Images.Data.rHPIndicatorE->Draw((cHeartSpc * (i/2.0f)), 0.0f);
                }
            }

            //--Clean.
            glScalef(2.0f, 2.0f, 1.0f);
            glTranslatef(-cLft, -cTop, 0.0f);
        }
    }

    ///--[Additional Lines]
    //--Lines set by scripts.
    cTextColor.SetAsMixer();
    for(int i = 0; i < TL_EXTRA_LINES; i ++)
    {
        if(mStorageLines[i][0] == '\0') continue;
        Images.Data.rFontMenuSize20->DrawTextArgs(1356.0f - 5.0f, cFrameDim.mTop + 5.0f + tYPos, SUGARFONT_RIGHTALIGN_X, 1.0f, mStorageLines[i]);
        tYPos = tYPos + cYIncrement;
    }

    StarlightColor::ClearMixer();

    ///--[Finish Up]
    //--Clean.
    glDisable(GL_STENCIL_TEST);
}
void TextLevel::RenderPortraitNormal()
{
    //--Renders the portrait as normal at full alpha. No crossfading.
    StarBitmap **rUseArray = rStandardRenderImg;
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        if(rOverrideRenderImg[i] != NULL)
        {
            rUseArray = rOverrideRenderImg;
            break;
        }
    }

    //--Sizes.
    TwoDimensionReal cFrameDim;
    cFrameDim.SetWH(982.0f, 3.0f, 382.0f, 450.0f);

    //--Compute scale.
    float cScaleX = 1.0f;
    float cScaleY = 1.0f;
    float cBiggestWid = 1.0f;
    float cBiggestHei = 1.0f;
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        //--Check if this is bigger than the existing dimensions.
        if(rUseArray[i])
        {
            if(rUseArray[i]->GetTrueWidth()  > cBiggestWid) cBiggestWid = rUseArray[i]->GetTrueWidth();
            if(rUseArray[i]->GetTrueHeight() > cBiggestHei) cBiggestHei = rUseArray[i]->GetTrueHeight();
        }
    }

    //--Image is larger than the Y dimension allows. We need to compress it.
    if(cBiggestHei > cFrameDim.GetHeight())
    {
        cScaleY = cFrameDim.GetHeight() / cBiggestHei;
    }

    //--Same in the X dimension.
    if(cBiggestWid > cFrameDim.GetWidth())
    {
        cScaleX = cFrameDim.GetWidth() / cBiggestWid;
    }

    //--The use scale is whichever scale is lowest.
    float cUseScale = cScaleX;
    if(cScaleY < cUseScale) cUseScale = cScaleY;

    //--Position.
    glTranslatef(cFrameDim.mXCenter, cFrameDim.mYCenter, 0.0f);
    glScalef(cUseScale, cUseScale, 1.0f);

    //--Render the standard image, centered in the frame.
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        //--Skip empties.
        if(!rUseArray[i]) continue;

        //--Render.
        rUseArray[i]->Draw(rUseArray[i]->GetTrueWidth() * -0.50f, rUseArray[i]->GetTrueHeight() * -0.50f);
    }

    //--Clean.
    glScalef(1.0f / cUseScale, 1.0f / cUseScale, 1.0f);
    glTranslatef(-cFrameDim.mXCenter, -cFrameDim.mYCenter, 0.0f);
}
void TextLevel::RenderPortraitCrossfade()
{
    ///--[Documentation]
    //--Render two sets of portraits, using crossfading. First, determine which source arrays to use.
    StarBitmap **rUseArray = rStandardRenderImg;
    StarBitmap **rPreArray = rPreviousRenderImg;
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        if(rOverrideRenderImg[i] != NULL)
        {
            rUseArray = rOverrideRenderImg;
            break;
        }
    }

    //--Sizes.
    TwoDimensionReal cFrameDim;
    cFrameDim.SetWH(982.0f, 3.0f, 382.0f, 450.0f);

    ///--[Alphas]
    //--Previous Image: Alpha stays full until the midway mark, then fades down.
    float cPrevAlpha = 1.0f;
    if(mImgTransitionTimer <= TL_PLAYER_TRANSITION_TICKS * 0.50f)
    {
        cPrevAlpha = mImgTransitionTimer / (TL_PLAYER_TRANSITION_TICKS * 0.50f);
    }

    //--Current Image: Alpha fades up to 1.0f and stays there.
    float cCurrAlpha = 1.0f;
    if(mImgTransitionTimer >= TL_PLAYER_TRANSITION_TICKS * 0.50f)
    {
        cCurrAlpha = 1.0f - ((mImgTransitionTimer - TL_PLAYER_TRANSITION_TICKS * 0.50f) / (TL_PLAYER_TRANSITION_TICKS * 0.50f));
    }

    ///--[Scales]
    //--Variable resolve.
    float cScalePrevX = 1.0f;
    float cScalePrevY = 1.0f;
    float cBiggestPrevWid = 1.0f;
    float cBiggestPrevHei = 1.0f;
    float cScaleX = 1.0f;
    float cScaleY = 1.0f;
    float cBiggestWid = 1.0f;
    float cBiggestHei = 1.0f;

    //--Run across both arrays, find the largest image in both arrays.
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        //--Same but for the previous array.
        if(rPreArray[i])
        {
            if(rPreArray[i]->GetTrueWidth()  > cBiggestPrevWid) cBiggestPrevWid = rPreArray[i]->GetTrueWidth();
            if(rPreArray[i]->GetTrueHeight() > cBiggestPrevHei) cBiggestPrevHei = rPreArray[i]->GetTrueHeight();
        }

        //--Check if this is bigger than the existing dimensions.
        if(rUseArray[i])
        {
            if(rUseArray[i]->GetTrueWidth()  > cBiggestWid) cBiggestWid = rUseArray[i]->GetTrueWidth();
            if(rUseArray[i]->GetTrueHeight() > cBiggestHei) cBiggestHei = rUseArray[i]->GetTrueHeight();
        }
    }

    //--Determines scales.
    if(cBiggestPrevHei > cFrameDim.GetHeight()) cScalePrevY = cFrameDim.GetHeight() / cBiggestPrevHei;
    if(cBiggestPrevWid > cFrameDim.GetWidth())  cScalePrevX = cFrameDim.GetWidth()  / cBiggestPrevWid;
    if(cBiggestHei     > cFrameDim.GetHeight()) cScaleY     = cFrameDim.GetHeight() / cBiggestHei;
    if(cBiggestWid     > cFrameDim.GetWidth())  cScaleX     = cFrameDim.GetWidth()  / cBiggestWid;

    //--The use scale is whichever scale is lowest.
    float cPrevScale = cScalePrevX;
    if(cScalePrevY < cPrevScale) cPrevScale = cScalePrevY;
    float cCurrScale = cScaleX;
    if(cScaleY < cCurrScale) cCurrScale = cScaleY;

    ///--[Iteration]
    //--Begin looping and rendering the images.
    glTranslatef(cFrameDim.mXCenter, cFrameDim.mYCenter, 0.0f);
    for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
    {
        //--Render the previous image at the needed alpha.
        if(rPreArray[i])
        {
            //--Position.
            glScalef(cPrevScale, cPrevScale, 1.0f);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPrevAlpha);

            //--Render.
            rPreArray[i]->Draw(rPreArray[i]->GetTrueWidth() * -0.50f, rPreArray[i]->GetTrueHeight() * -0.50f);

            //--Clean.
            glScalef(1.0f / cPrevScale, 1.0f / cPrevScale, 1.0f);
        }

        //--Render the current image at the needed alpha.
        if(rUseArray[i])
        {
            //--Position.
            glScalef(cCurrScale, cCurrScale, 1.0f);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cCurrAlpha);

            //--Render.
            rUseArray[i]->Draw(rUseArray[i]->GetTrueWidth() * -0.50f, rUseArray[i]->GetTrueHeight() * -0.50f);

            //--Clean.
            glScalef(1.0f / cCurrScale, 1.0f / cCurrScale, 1.0f);
        }
    }

    //--Clean.
    glTranslatef(-cFrameDim.mXCenter, -cFrameDim.mYCenter, 0.0f);
    StarlightColor::ClearMixer();
}
int TextLevel::ComputeVisibleLocalityEntries()
{
    ///--[Documentation]
    //--Computes and returns how many locality entries are currently visible. Because the menus can be opened and contain
    //  variable numbers of entries, a fixed result isn't possible, it must be computed.
    int tTotal = 0;
    LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
    while(rCategory)
    {
        //--No Icons Mode:
        if(!mLocalityUsesIcons)
        {
            //--Add one for the locality entry itself.
            if(!mLocalityUsesIcons) tTotal ++;

            //--If this is not a leaf, and is opened, it has extra entries displayed.
            if(rCategory->mEntryList && rCategory->mIsOpened)
            {
                //--Iterate across. This code remains in place because someday these may become sub-subcategories.
                //  Right now these are always leaves.
                LocalityCategory *rSubCategory = (LocalityCategory *)rCategory->mEntryList->PushIterator();
                while(rSubCategory)
                {
                    //--Add.
                    tTotal ++;

                    //--Next.
                    rSubCategory = (LocalityCategory *)rCategory->mEntryList->AutoIterate();
                }
            }
        }
        //--Icons Mode:
        else
        {
            //--If the category is not opened, skip it.
            if(!rCategory->mIsOpened)
            {
            }
            //--If the category is opened:
            else
            {
                //--Add one for the category.
                tTotal ++;

                //--Add one for each item in the sublist.
                tTotal += rCategory->mEntryList->GetListSize();
            }
        }

        //--Next.
        rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
    }

    //--Finish up.
    return tTotal;
}
void TextLevel::RenderLocalityWindow()
{
    ///--[Documentation]
    //--Shows nearby objects, entities, and anything else the scripts decide to show here. These are
    //  divided into a series of subheadings which can be opened and closed.
    if(!mCurrentLocalityList) return;

    //--Color setup.
    StarlightColor cTextColor = StarlightColor::cxBlack;
    StarlightColor cOppoColor = StarlightColor::cxWhite;
    if(mIsDarkMode)
    {
        cTextColor = StarlightColor::cxWhite;
        cOppoColor = StarlightColor::cxBlack;
    }

    //--Locality sorting, if not sorted by the update handler.
    if(mLocalityNeedsSort)
    {
        mLocalityNeedsSort = false;
        SortLocalityWindow();
    }

    //--Stencil work.
    glEnable(GL_STENCIL_TEST);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_LOCALITY, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    Images.Data.rMask_Locality->Draw();

    //--Now turn on stencil-controlled rendering.
    Images.Data.rMapParts->Bind();
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilMask(0xFF);
    glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_LOCALITY, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    ///--[List-Based]
    //--If this flag is false, render the entries as lists.
    if(!mLocalityUsesIcons)
    {
        //--Begin rendering the entries.
        int i = 0;
        cTextColor.SetAsMixer();
        float tRenderX = mLocalityWindow.mLft + 5.0f;
        float tRenderY = mLocalityWindow.mTop - mLocalityScroll;
        LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
        while(rCategory)
        {
            //--If this is the selected entry, give it a black background and white text.
            if(i == mLocalityHighlight)
            {
                StarBitmap::DrawRectFill(mLocalityWindow.mLft, tRenderY + 4.0f, mLocalityWindow.mRgt, tRenderY + Images.Data.rFontMenuSize20->GetTextHeight() + 2.0f, cTextColor);
                cOppoColor.SetAsMixer();
            }

            //--If this is a leaf, render the name without anything next to it.
            if(!rCategory->mEntryList)
            {
                Images.Data.rFontMenuSize20->DrawTextArgs(tRenderX, tRenderY, 0, 1.0f, "  [%s]", rCategory->mLocalName);
                tRenderY = tRenderY + Images.Data.rFontMenuSize20->GetTextHeight();
                if(i == mLocalityHighlight) cTextColor.SetAsMixer();
                i ++;
            }
            //--If this category is closed, render a + next to it.
            else if(!rCategory->mIsOpened)
            {
                Images.Data.rFontMenuSize20->DrawTextArgs(tRenderX, tRenderY, 0, 1.0f, "+ %s (%i)", rCategory->mLocalName, rCategory->mEntryList->GetListSize());
                tRenderY = tRenderY + Images.Data.rFontMenuSize20->GetTextHeight();
                if(i == mLocalityHighlight) cTextColor.SetAsMixer();
                i ++;
            }
            //--Opened, render a minus, and all of its sub-members.
            else
            {
                //--Category heading.
                Images.Data.rFontMenuSize20->DrawTextArgs(tRenderX, tRenderY, 0, 1.0f, "- %s", rCategory->mLocalName);
                tRenderY = tRenderY + Images.Data.rFontMenuSize20->GetTextHeight();
                if(i == mLocalityHighlight) cTextColor.SetAsMixer();
                i ++;

                //--Start rendering the sub-members. Assume they are all leaves for now.
                float tUseX = tRenderX + 25.0f;
                LocalityCategory *rSubCategory = (LocalityCategory *)rCategory->mEntryList->PushIterator();
                while(rSubCategory)
                {
                    //--If this is the selected entry, give it a black background and white text.
                    if(i == mLocalityHighlight)
                    {
                        cOppoColor.SetAsMixer();
                        StarBitmap::DrawRectFill(mLocalityWindow.mLft, tRenderY + 4.0f, mLocalityWindow.mRgt, tRenderY + Images.Data.rFontMenuSize20->GetTextHeight() + 2.0f, cTextColor);
                        cOppoColor.SetAsMixer();
                    }

                    //--Name.
                    Images.Data.rFontMenuSize20->DrawTextArgs(tUseX, tRenderY, 0, 1.0f, "[%s]", rSubCategory->mLocalName);
                    tRenderY = tRenderY + Images.Data.rFontMenuSize20->GetTextHeight();
                    if(i == mLocalityHighlight) cTextColor.SetAsMixer();
                    i ++;

                    //--If we exceed the bottom, we can stop rendering.
                    if(tRenderY > mLocalityWindow.mBot)
                    {
                        rCategory->mEntryList->PopIterator();
                        break;
                    }

                    //--Next.
                    rSubCategory = (LocalityCategory *)rCategory->mEntryList->AutoIterate();
                }
            }

            //--If we exceed the bottom, we can stop rendering.
            if(tRenderY > mLocalityWindow.mBot)
            {
                mCurrentLocalityList->PopIterator();
                break;
            }

            //--Next.
            rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
        }
    }
    ///--[Locality as Icons]
    //--Renders a set of icons on top. Each corresponds to a locality list.
    else
    {
        //--Begin rendering the entries.
        int i = -1;
        float tIconX = mLocalityWindow.mLft + 4.0f;
        float tRenderX = mLocalityWindow.mLft + 5.0f;
        float tRenderY = mLocalityWindow.mTop - mLocalityScroll + TL_LOCALITY_SIZE + 5.0f;
        LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
        while(rCategory)
        {
            //--If we're not opened, we don't need to render anything.
            if(!rCategory->mIsOpened)
            {
                rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
                continue;
            }

            //--Render the heading name.
            cTextColor.SetAsMixer();
            Images.Data.rFontMenuSize20->DrawTextArgs(tRenderX, tRenderY, 0, 1.0f, "[%s]", rCategory->mLocalName);
            tRenderY = tRenderY + Images.Data.rFontMenuSize20->GetTextHeight();
            i ++;

            //--Start rendering the sub-members. Assume they are all leaves for now.
            float tUseX = tRenderX + 25.0f;
            LocalityCategory *rSubCategory = (LocalityCategory *)rCategory->mEntryList->PushIterator();
            while(rSubCategory)
            {
                //--If this is the selected entry, give it a black background and white text.
                cTextColor.SetAsMixer();
                if(i == mLocalityHighlight)
                {
                    StarBitmap::DrawRectFill(mLocalityWindow.mLft, tRenderY + 4.0f, mLocalityWindow.mRgt, tRenderY + Images.Data.rFontMenuSize20->GetTextHeight() + 2.0f, cTextColor);
                    cOppoColor.SetAsMixer();
                }

                //--Name.
                Images.Data.rFontMenuSize20->DrawTextArgs(tUseX, tRenderY, 0, 1.0f, "[%s]", rSubCategory->mLocalName);
                tRenderY = tRenderY + Images.Data.rFontMenuSize20->GetTextHeight();
                if(i == mLocalityHighlight) cTextColor.SetAsMixer();
                i ++;

                //--If we exceed the bottom, we can stop rendering.
                if(tRenderY > mLocalityWindow.mBot)
                {
                    rCategory->mEntryList->PopIterator();
                    break;
                }

                //--Next.
                rSubCategory = (LocalityCategory *)rCategory->mEntryList->AutoIterate();
            }

            //--If we exceed the bottom, we can stop rendering.
            if(tRenderY > mLocalityWindow.mBot)
            {
                mCurrentLocalityList->PopIterator();
                break;
            }

            //--Next.
            rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
        }

        //--Render the locality icons over the text. This means rendering them second.
        rCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
        while(rCategory)
        {
            //--If the category is not the selected one, render the up button.
            StarlightColor::ClearMixer();
            if(!rCategory->mIsOpened)
            {
                if(rCategory->rImageUp) rCategory->rImageUp->Draw(tIconX, mLocalityWindow.mTop + 6.0f);
            }
            //--Render the down button if it's opened.
            else
            {
                if(rCategory->rImageDn) rCategory->rImageDn->Draw(tIconX, mLocalityWindow.mTop + 6.0f);
            }

            //--Move the icon indicator over.
            tIconX = tIconX + TL_LOCALITY_SIZE;

            //--Next.
            rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
        }

        //--Render a scrollbar on the right side, if and only if there are (TL_LOCALITY_LINES_MAX+1) entries
        //  on the locality list.
        int tUseMax = TL_LOCALITY_LINES_MAX;
        int tExpectedLocalityEntries = ComputeVisibleLocalityEntries();
        if(mLocalityUsesIcons) tUseMax -= 3;
        if(tExpectedLocalityEntries > tUseMax)
        {
            //--Backing.
            StarlightColor::ClearMixer();
            Images.Data.rScrollbar->Draw(1333.0f, 490.0f);

            //--Setup.
            float cBarStart = 513.0f;
            float cBarHei = 221.0f;
            float cTextHei = Images.Data.rFontMenuSize20->GetTextHeight();
            float cMax = (tExpectedLocalityEntries + 0.70f) * cTextHei;
            float cUsed = tUseMax * cTextHei;
            float cLft = 1334.0f;
            float cRgt = cLft + 22.0f;
            float cTop = cBarStart + ((mLocalityScroll / cMax) * cBarHei);
            float cBot = cTop + ((cUsed / cMax) * cBarHei);

            //--Scrollbar.
            StarlightColor::MapRGBAI(137, 110, 83, 255).SetAsMixer();
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
                glVertex2f(cLft, cTop);
                glVertex2f(cRgt, cTop);
                glVertex2f(cRgt, cBot);
                glVertex2f(cLft, cBot);
            glEnd();
            glEnable(GL_TEXTURE_2D);
        }
    }

    //--Clean up.
    StarlightColor::ClearMixer();
    glDisable(GL_STENCIL_TEST);
}
void TextLevel::RenderPopupWindow()
{
    ///--[Documentation]
    //--Renders the popup window, assuming it has at least one entry. Also shows highlight cases.
    if(mPopupCommandList->GetListSize() < 1) return;

    ///--[Border Card]
    //--Texture sizings.
    float cTxLL =  0.0f / 20.0f;
    float cTxLR =  4.0f / 20.0f;
    float cTxRL = 16.0f / 20.0f;
    float cTxRR = 20.0f / 20.0f;
    float cTxTT =  0.0f / 20.0f;
    float cTxTB =  4.0f / 20.0f;
    float cTxBT = 16.0f / 20.0f;
    float cTxBB = 20.0f / 20.0f;
    float cWid = 4.0f;
    float cHei = 4.0f;

    //--Rendering.
    Images.Data.rPopupBorderCard->Bind();
    glBegin(GL_QUADS);

        //--[Top Vertically]
        //--Top left.
        glTexCoord2f(cTxLL, cTxTT); glVertex2f(mPopupWindowDim.mLft + (cWid * 0.0f), mPopupWindowDim.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxLR, cTxTT); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxLL, cTxTB); glVertex2f(mPopupWindowDim.mLft + (cWid * 0.0f), mPopupWindowDim.mTop + (cHei * 1.0f));

        //--Top bar.
        glTexCoord2f(cTxLR, cTxTT); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxRL, cTxTT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));

        //--Top right.
        glTexCoord2f(cTxRL, cTxTT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxRR, cTxTT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 0.0f), mPopupWindowDim.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxRR, cTxTB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 0.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));

        //--[Middle Vertically]
        //--Middle left.
        glTexCoord2f(cTxLL, cTxTB); glVertex2f(mPopupWindowDim.mLft + (cWid * 0.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxLL, cTxBT); glVertex2f(mPopupWindowDim.mLft + (cWid * 0.0f), mPopupWindowDim.mBot - (cHei * 1.0f));

        //--Middle bar.
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));

        //--Middle right.
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxTB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 0.0f), mPopupWindowDim.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxBT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 0.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));

        //--[Bottom Vertically]
        //--Bottom left.
        glTexCoord2f(cTxLL, cTxBT); glVertex2f(mPopupWindowDim.mLft + (cWid * 0.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBB); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxLL, cTxBB); glVertex2f(mPopupWindowDim.mLft + (cWid * 0.0f), mPopupWindowDim.mBot - (cHei * 0.0f));

        //--Bottom bar.
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxLR, cTxBB); glVertex2f(mPopupWindowDim.mLft + (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 0.0f));

        //--Bottom right.
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxBT); glVertex2f(mPopupWindowDim.mRgt - (cWid * 0.0f), mPopupWindowDim.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxBB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 0.0f), mPopupWindowDim.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxRL, cTxBB); glVertex2f(mPopupWindowDim.mRgt - (cWid * 1.0f), mPopupWindowDim.mBot - (cHei * 0.0f));

    glEnd();

    ///--[Text Rendering]
    int i = 0;
    float tX = mPopupWindowDim.mLft + 6.0f;
    float tY = mPopupWindowDim.mTop + 3.0f;
    StarlightColor::cxBlack.SetAsMixer();
    PopupCommand *rCommand = (PopupCommand *)mPopupCommandList->PushIterator();
    while(rCommand)
    {
        //--If this is the selected entry, give it a black background and white text.
        if(i == mPopupHighlight)
        {
            StarlightColor::cxWhite.SetAsMixer();
            StarBitmap::DrawRectFill(mPopupWindowDim.mLft + cWid, tY + 0.0f, mPopupWindowDim.mRgt - cWid, tY + Images.Data.rFontMenuSize20->GetTextHeight() - 1.0f, StarlightColor::cxBlack);
        }

        //--Render.
        Images.Data.rFontMenuSize20->DrawText(tX, tY - 3.0f, 0, 1.0f, rCommand->mDisplayName);

        //--Next.
        i ++;
        tY = tY + Images.Data.rFontMenuSize20->GetTextHeight();
        StarlightColor::cxBlack.SetAsMixer();
        rCommand = (PopupCommand *)mPopupCommandList->AutoIterate();
    }
}
