-- |[ ================================== Engine Options Menu =================================== ]|
--Sets up the global options menu. Note that some of these changes may require a program restart to
-- take full effect.
local sBasePath = fnResolvePath()

-- |[Setup]|
--Local vars.
local iAscendingPriority = 0

--Length.
local sLongestString = "Use Small Console Font"

-- |[Game Checking]|
--Figure out which games are loaded. Some games need certain options, so the menu only has options
-- relevant to the games actually present.
local bAdventure              = fnGameExists("Adventure Mode")
local bClassic                = fnGameExists("Classic Mode")
local bStringTyrant           = fnGameExists("Doll Manor") or fnGameExists("Doll Manor Demo")
local bElectrospriteAdventure = fnGameExists("Electrosprite Adventure")
local bLevelGenerator         = fnGameExists("Level Generator")
local bPairanormal            = fnGameExists("Pairanormal")
local bOurOwnWonderland       = fnGameExists("Our Own Wonderland")

--Options list.
local bMandate     = true --Always true
local bResolution  = true --Always true
local bMipmapping  = bClassic
local bMouseDebug  = false --Deprecated
local bSmallFont   = bClassic
local bPerlinNoise = bAdventure
local bRAMLoading  = true --Always true

-- |[Header]|
FlexMenu_SetHeaderSize(2)
FlexMenu_SetHeaderLine(0, "Engine and Display Options")
FlexMenu_SetHeaderLine(1, "Some changes may require a program restart to take effect.", 0.6)

-- |[ ====================================== Menu Options ====================================== ]|
-- |[Mandate Monitor]|
--Allows user to attempt to mandate which monitor the game uses. Only really works for SDL.
if(bMandate == true) then
    FlexMenu_RegisterButton("Mandate Monitor")

        --Description.
        FlexButton_SetProperty("Description", "Mandates which monitor the game attempts to use when positioning and fullscreening.\nTries to place itself on this monitor during startup and exiting fullscreen."..
            "\n\nAllegro version is bugged and this feature does not work.")

        --Number of displays. Probably shouldn't be zero.
        local iDisplaysTotal = DM_GetDisplayModeInfo("Total Displays")

        --Option count is the no-preference option plus however many monitors are detected.
        FlexButton_SetProperty("Option Count", 1 + iDisplaysTotal)
        FlexButton_SetProperty("Option Text", 0, "No Preference")
        for i = 1, iDisplaysTotal, 1 do
            FlexButton_SetProperty("Option Text", i, "Monitor " .. i-1)
        end
        
        --Current option:
        local iMandateMonitor = OM_GetOption("MandateMonitor")
        FlexButton_SetProperty("Current Option", iMandateMonitor + 1)

        --Flags.
        StarButton_SetProperty("Priority", iAscendingPriority)
        iAscendingPriority = iAscendingPriority + 1
        FlexButton_SetProperty("Lying Length", sLongestString)

    DL_PopActiveObject()
end

-- |[Set Resolution]|
--Cycles through legal resolutions.
if(bResolution == true) then
    local iResM, iResX, iResY, iResR = DM_GetDisplayModeInfo("Data", giCurrentDisplayMode)
    FlexMenu_RegisterButton("Resolution")

        --Description.
        FlexButton_SetProperty("Description", "Changes the resolution the game renders at.\nResolutions are determined by your graphics drivers.\nRequires a program restart to take effect.")

        --Get how many modes there are.
        local iTotalDisplayModes = DM_GetDisplayModeInfo("Total Modes")
        
        --Upload the data.
        FlexButton_SetProperty("Option Count", iTotalDisplayModes)
        for i = 1, iTotalDisplayModes, 1 do
            local iResM, iResX, iResY, iResR = DM_GetDisplayModeInfo("Data", i-1)
            FlexButton_SetProperty("Option Text", i-1, "Mon" .. iResM .. ": " .. iResX .. "x" .. iResY)
        end
        
        --Set the cursor to the current display mode.
        local iCurrentMode = DM_GetDisplayModeInfo("Current Mode")
        FlexButton_SetProperty("Current Option", iCurrentMode)
        
        --Other flags.
        StarButton_SetProperty("Priority", iAscendingPriority)
        iAscendingPriority = iAscendingPriority + 1
        FlexButton_SetProperty("Lying Length", sLongestString)
    DL_PopActiveObject()
end

-- |[Toggle Mipmapping]|
--Mipmapping causes things to antialias when they are magnified or minified.
if(bMipmapping == true) then
    FlexMenu_RegisterButton("Disallow Mipmapping")

        --Description.
        FlexButton_SetProperty("Description", "Toggles antialiasing on distant or extremely close textures. Some graphics cards may crash if they don't support mipmapping, otherwise you can ignore this.")

        --Upload the data.
        FlexButton_SetProperty("Option Count", 2)
        FlexButton_SetProperty("Option Text", 0, "True")
        FlexButton_SetProperty("Option Text", 1, "False")
        
        --Set to current mode.
        local bDisallowMipmapping = OM_GetOption("DisallowMipmapping")
        if(bDisallowMipmapping) then
            FlexButton_SetProperty("Current Option", 0)
        else
            FlexButton_SetProperty("Current Option", 1)
        end
        
        --Other flags.
        StarButton_SetProperty("Priority", iAscendingPriority)
        iAscendingPriority = iAscendingPriority + 1
        FlexButton_SetProperty("Lying Length", sLongestString)
    DL_PopActiveObject()
end

-- |[Mouse Debug]|
--Shows a green cursor under the mouse. Useful for debugging resolution changes.
if(bMouseDebug == true) then
    FlexMenu_RegisterButton("Show Mouse Debug Cursor")

        --Description.
        FlexButton_SetProperty("Description", "Shows a small cursor underneath the mouse cursor, or at least, where the program thinks the mouse cursor is after scaling. Use this only if the program seems to be clicking where your cursor isn't.")

        --Upload the data.
        FlexButton_SetProperty("Option Count", 2)
        FlexButton_SetProperty("Option Text", 0, "True")
        FlexButton_SetProperty("Option Text", 1, "False")
        
        --Set to current mode.
        local bShowMouseDebug = OM_GetOption("ShowMouseDebug")
        if(bShowMouseDebug) then
            FlexButton_SetProperty("Current Option", 0)
        else
            FlexButton_SetProperty("Current Option", 1)
        end
        
        --Other flags.
        StarButton_SetProperty("Priority", iAscendingPriority)
        iAscendingPriority = iAscendingPriority + 1
        FlexButton_SetProperty("Lying Length", sLongestString)
    DL_PopActiveObject()
end

-- |[Font Options]|
if(bSmallFont == true) then
    FlexMenu_RegisterButton("Use Small Console Font")

        --Description.
        FlexButton_SetProperty("Description", "Uses a smaller font in the console, allowing it to display more information. May look bad at low resolutions.")
        
        --Uploads.
        FlexButton_SetProperty("Option Count", 2)
        FlexButton_SetProperty("Option Text", 0, "True")
        FlexButton_SetProperty("Option Text", 1, "False")
        local bUseSmallConsoleFont = OM_GetOption("UseSmallConsoleFont")
        if(bUseSmallConsoleFont) then
            FlexButton_SetProperty("Current Option", 0)
        else
            FlexButton_SetProperty("Current Option", 1)
        end
        
        --Other flags.
        StarButton_SetProperty("Priority", iAscendingPriority)
        iAscendingPriority = iAscendingPriority + 1
        FlexButton_SetProperty("Lying Length", sLongestString)
    DL_PopActiveObject()
end

-- |[Low Resolution Mode]|
FlexMenu_RegisterButton("Use Low Res (Adv)")

	--Description.
	FlexButton_SetProperty("Description", "Uses half-sized portraits and scene images in Adventure Mode. Only needed for integrated graphics cards.")
	
	--Uploads.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bUseLowResMode = OM_GetOption("LowResAdventureMode")
	if(bUseLowResMode) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	StarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

-- |[Disallow Perlin Noise]|
--[=[
if(bPerlinNoise == true) then
    FlexMenu_RegisterButton("Disallow Perlin Noise Engine")

        --Description.
        FlexButton_SetProperty("Description", "Disables Perlin Noise generation. Makes Random Level Generation unusable.")
        
        --Uploads.
        FlexButton_SetProperty("Option Count", 2)
        FlexButton_SetProperty("Option Text", 0, "True")
        FlexButton_SetProperty("Option Text", 1, "False")
        local bDisallowNoise = OM_GetOption("DisallowPerlinNoise")
        if(bDisallowNoise) then
            FlexButton_SetProperty("Current Option", 0)
        else
            FlexButton_SetProperty("Current Option", 1)
        end
        
        --Other flags.
        StarButton_SetProperty("Priority", iAscendingPriority)
        iAscendingPriority = iAscendingPriority + 1
        FlexButton_SetProperty("Lying Length", sLongestString)
    DL_PopActiveObject()
end
]=]

-- |[RAM Loading]|
if(bRAMLoading == true) then
    FlexMenu_RegisterButton("Use RAM Loading")

        --Description.
        FlexButton_SetProperty("Description", "Causes the engine to use extra RAM to load files faster. May cause instability on older machines.")
        
        --Uploads.
        FlexButton_SetProperty("Option Count", 2)
        FlexButton_SetProperty("Option Text", 0, "True")
        FlexButton_SetProperty("Option Text", 1, "False")
        local bUseRAMLoading = OM_GetOption("UseRAMLoading")
        if(bUseRAMLoading) then
            FlexButton_SetProperty("Current Option", 0)
        else
            FlexButton_SetProperty("Current Option", 1)
        end
        
        --Other flags.
        StarButton_SetProperty("Priority", iAscendingPriority)
        iAscendingPriority = iAscendingPriority + 1
        FlexButton_SetProperty("Lying Length", sLongestString)
    DL_PopActiveObject()
end

-- |[ ======================================= Save / Exit ====================================== ]|
-- |[Save]|
--Save changes and go back to the main menu.
FlexMenu_RegisterButton("Save changes")

	--Description.
	FlexButton_SetProperty("Description", "Save changes and return to the main menu.")

	StarButton_SetProperty("Priority", 10000)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Exit And Save.lua", 0)
DL_PopActiveObject()

-- |[Back]|
--Go back to the main menu.
FlexMenu_RegisterButton("Exit without saving")

	--Description.
	FlexButton_SetProperty("Description", "Return to the main menu without saving changes.")

	StarButton_SetProperty("Priority", 10001)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "101 Exit No Save.lua", 0)
DL_PopActiveObject()
