//--Base
#include "ResetManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
ResetManager::ResetManager()
{
    ///--[ResetManager]
    //--System
    //--Reset List
    mResetList = new StarLinkedList(true);
}
ResetManager::~ResetManager()
{
    delete mResetList;
}
void ResetManager::DeleteResetList(void *pPtr)
{
    ResetList *rList = (ResetList *)pPtr;
    free(rList->mResetList);
    free(rList);
}

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
ResetList *ResetManager::AllocateReset(const char *pName, int pSize)
{
    //--Allocates a new reset instruction pack. If the pack already existed, does nothing. If size
    //  is less than one, also does nothing.
    //--Returns the newly allocated list, which is also stored on the global reset list. You can then
    //  add more elements to the list manually, which is faster than using the SetResetIndex() function.
    if(!pName || pSize < 1) return NULL;

    //--See if the pack already exists under that name.
    if(mResetList->GetElementByName(pName))
    {
        fprintf(stderr, "ResetManager: Error, reset pack %s already exists.\n", pName);
        return NULL;
    }

    //--Allocate a new pack. Clear all its members to NULL for safety.
    SetMemoryData(__FILE__, __LINE__);
    ResetList *nList = (ResetList *)starmemoryalloc(sizeof(ResetList));
    nList->mResetListSize = pSize;
    SetMemoryData(__FILE__, __LINE__);
    nList->mResetList = (IResettable **)starmemoryalloc(sizeof(IResettable *) * pSize);
    for(int i = 0; i < pSize; i ++)
    {
        nList->mResetList[i] = NULL;
    }

    //--Add the pack to the list.
    mResetList->AddElement(pName, nList, &ResetManager::DeleteResetList);

    //--Pass the list back.
    return nList;
}
void ResetManager::SetResetIndex(const char *pName, int pSlot, IResettable *pMember)
{
    //--It may not be fast, but it's safe. This sets the named reset list's member to the given
    //  pointer. Use this for quick replacement. If building a new ResetList, you can try using
    //  the AllocateReset function to get back the list you just allocated.
    //--It is legal to set a slot to NULL, as that will effectively clear it.
    if(!pName || pSlot < 0) return;

    //--Locate the reset pack.
    ResetList *rList = (ResetList *)mResetList->GetElementByName(pName);
    if(!rList)
    {
        fprintf(stderr, "ResetManager: Error, no reset pack named %s in SetResetIndex().\n", pName);
        return;
    }

    //--Set the pack index if it's in bounds.
    if(pSlot >= rList->mResetListSize) return;
    rList->mResetList[pSlot] = pMember;
}

///======================================== Core Methods ==========================================
void ResetManager::IssueReset(const char *pResetName)
{
    //--Tells all objects on the matching list to reset themselves. If the list is not found, does
    //  nothing and barks a warning. If NULL is passed, does nothing.
    if(!pResetName) return;

    //--Locate the reset pack.
    ResetList *rList = (ResetList *)mResetList->GetElementByName(pResetName);
    if(!rList)
    {
        fprintf(stderr, "ResetManager: Error, no reset pack named %s\n", pResetName);
        return;
    }

    //--Issue the reset to all its members.
    for(int i = 0; i < rList->mResetListSize; i ++)
    {
        if(rList->mResetList[i]) rList->mResetList[i]->Reset(pResetName);
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
ResetManager *ResetManager::Fetch()
{
    return Global::Shared()->gResetManager;
}

///======================================== Lua Hooking ===========================================
void ResetManager::HookToLuaState(lua_State *pLuaState)
{
    /* Game_Reset()
       Orders the game to reset back to the factory-zero state. */
    lua_register(pLuaState, "Game_Reset", &Hook_Game_Reset);

    /* Game_Quit()
       Quits the game. */
    lua_register(pLuaState, "Game_Quit", &Hook_Game_Quit);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Game_Reset(lua_State *L)
{
    //Game_Reset()
    Global::Shared()->gReset = true;
    return 0;
}
int Hook_Game_Quit(lua_State *L)
{
    //Game_Quit()
    Global::Shared()->gQuit = true;
    return 0;
}
