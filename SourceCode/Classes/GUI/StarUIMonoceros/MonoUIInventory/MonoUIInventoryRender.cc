//--Base
#include "MonoUIInventory.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///====================================== Segment Drawing =========================================
void MonoUIInventory::RenderTop(float pAlpha)
{
    ///--[Documentation]
    //--Only the top UI renders on this object. It slides in from the top of the screen.

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pAlpha);

    ///--[Rendering]
    //--Inventory block.
    RenderInventoryListing(cColorAlpha);

    //--Header.
    MonoImages.rOverlay_Titleshade->Draw();

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 9.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextInventory);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Strings.
    mShowHelpString->  DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mChangeSortString->DrawText(0.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollFastString->DrawText(0.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Right-aligned strings.
    if(AdventureMenu::xDeconstructScript) mDeconstructString->DrawText(VIRTUAL_CANVAS_X, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///===================================== Component Drawing ========================================
void MonoUIInventory::RenderInventoryListing(float pColorAlpha)
{
    ///--[ ========== Documentation and Setup =========== ]
    //--Renders the main inventory block, listing the items, their properties, who equips them, etc.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetExtendedItemList();

    ///--[ ============= Heading and Frames ============= ]
    ///--[Frame]
    //--The frame always renders. If there are no items to render, stop, but always render the frame.
    AdvImages.rFrame_ItemListing->Draw();

    //--If the list has zero elements, stop.
    if(rItemList->GetListSize() < 1) return;

    ///--[Headers]
    //--Static text.
    mMonoTitleColor.SetAsMixerAlpha(pColorAlpha);
    AdvImages.rFont_Heading->DrawText( 65.0f, 95.0f, 0, 1.0f, xrStringData->str.mTextName);
    AdvImages.rFont_Heading->DrawText(662.0f, 95.0f, 0, 1.0f, xrStringData->str.mTextType);
    AdvImages.rFont_Heading->DrawText(763.0f, 95.0f, 0, 1.0f, xrStringData->str.mTextValue);
    AdvImages.rFont_Heading->DrawText(892.0f, 95.0f, 0, 1.0f, xrStringData->str.mTextGems);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pColorAlpha);

    //--Icons. Render at double size.
    float cPropIconX = 1010.0f;
    float cPropIconY =  103.0f;
    float cPropIconW =   44.0f;
    float cScale = 2.0f;
    float cScaleInv = 1.0f / cScale;
    glTranslatef(cPropIconX, cPropIconY, 0.0f);
    glScalef(cScale, cScale, 1.0f);
    for(int i = 0; i < ADVUIINVENTORY_HEADER_ICONS_TOTAL; i ++)
    {
        AdvImages.rPropertyIcons[i]->Draw(cPropIconW * i * cScaleInv, 0.0f);
    }

    //--Clean.
    glScalef(cScaleInv, cScaleInv, 1.0f);
    glTranslatef(cPropIconX * -1.0f, cPropIconY * -1.0f, 0.0f);

    ///--[Sorting Indicator]
    int tSortFlag = rInventory->GetInternalSortFlag();

    //--Change Sorting string.
    if(tSortFlag == AINV_SORT_CRITERIA_NAME)
        AdvImages.rOverlay_SortArrow->Draw(152.0f, 126.0f);
    else if(tSortFlag == AINV_SORT_CRITERIA_VALUE)
        AdvImages.rOverlay_SortArrow->Draw(847.0f, 126.0f);
    else if(tSortFlag == AINV_SORT_CRITERIA_TYPE)
        AdvImages.rOverlay_SortArrow->Draw(736.0f, 126.0f);

    ///--[ =============== Item Rendering =============== ]
    ///--[Setup]
    //--Constants.
    float cEntryY = 147.0f;
    float cEntryH =  37.0f;

    ///--[Backings]
    //--Render all item backings.
    int tRender = 0;
    AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--Effective position.
        int tEffectiveRender = tRender - mItemSkip;

        //--Skip when scrolling.
        if(tEffectiveRender < 0)
        {
            tRender ++;
            rItem = (AdventureItem *)rItemList->AutoIterate();
            continue;
        }

        //--The name of the iterator is the owner of the item. The function needs this to render their icon.
        RenderInventoryEntryBack(rItem, rItemList->GetIteratorName(), cEntryY + (cEntryH * tEffectiveRender), (tRender % 2 == 0), pColorAlpha);
        tRender ++;

        //--Clamp. Only render cxItemsPerPage entries.
        if(tEffectiveRender >= cItemsPerPage-1)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next entry.
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }

    ///--[Cursor]
    //--Render the selection cursor.
    MonoImages.rOverlay_ItemshadeSelect->Draw(mHighlightPos.mXCur, mHighlightPos.mYCur - MonoImages.rOverlay_ItemshadeLft->GetYOffset());

    ///--[Item Details]
    //--Render items until we hit the render cap.
    tRender = 0;
    rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--Effective position.
        int tEffectiveRender = tRender - mItemSkip;

        //--Skip when scrolling.
        if(tEffectiveRender < 0)
        {
            tRender ++;
            rItem = (AdventureItem *)rItemList->AutoIterate();
            continue;
        }

        //--The name of the iterator is the owner of the item. The function needs this to render their icon.
        RenderInventoryEntry(rItem, rItemList->GetIteratorName(), cEntryY + (cEntryH * tEffectiveRender), (tRender % 2 == 0), pColorAlpha);
        tRender ++;

        //--Clamp. Only render cItemsPerPage entries.
        if(tEffectiveRender >= cItemsPerPage-1)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next entry.
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }

    ///--[ ================= Scrollbar ================== ]
    //--If there is more than cxItemsPerPage in the inventory, a scrollbar appears on the right
    //  to show how many more entries there are.
    if(rItemList->GetListSize() > cItemsPerPage)
    {
        StandardRenderScrollbar(mItemSkip, cItemsPerPage, rItemList->GetListSize() - cItemsPerPage, 127.0f, 366.0f, false, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }
}
void MonoUIInventory::RenderInventoryEntryBack(AdventureItem *pItem, const char *pOwnerName, float pYPosition, bool pIsOdd, float pColorAlpha)
{
    ///--[Documentation]
    //--Non-override. Because of how the selection is rendered, all the items render their backings at the same time and then the
    //  selection cursor renders, then they render the text. Thus two functions are needed.

    //--If this entry is unselected, draw the normal left backing.
    MonoImages.rOverlay_ItemshadeLft->Draw(0.0f, pYPosition - MonoImages.rOverlay_ItemshadeLft->GetYOffset());

    //--In all cases draw the right backing.
    MonoImages.rOverlay_ItemshadeRgt->Draw(0.0f, pYPosition - MonoImages.rOverlay_ItemshadeRgt->GetYOffset());
}
void MonoUIInventory::RenderInventoryEntry(AdventureItem *pItem, const char *pOwnerName, float pYPosition, bool pIsOdd, float pColorAlpha)
{
    ///--[Documentation]
    //--Given an inventory item and a position, renders that item's icon, user, name, etc. Does nothing
    //  if an invalid item is provided.
    if(!pItem) return;

    //--Position setup.
    float cIconX     =   66.0f;
    float cFaceX     = cIconX + 24.0f;
    float cFaceW     =   26.0f;
    float cNameX     = cFaceX + 28.0f;
    float cTypeX     =  738.0f;
    float cValueX    =  778.0f;
    float cGemX      =  860.0f;
    float cGemW      =   24.0f;
    float cPropertyX = 1031.0f;
    float cPropertyW =   44.0f;

    //--Vertical offsets.
    float cIconOffY = 5.0f;
    float cFaceOffY = 3.0f;//No more weed for that man!
    float cNameOffY = -5.0f;
    float cSmlTxtOffY = 10.0f;
    float cGemOffY  = 6.0f;

    ///--[Icon]
    StarBitmap *rItemIcon = pItem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cIconX, pYPosition + cIconOffY);

    ///--[Owner]
    //--Item has no owner:
    if(!pOwnerName || !strcasecmp(pOwnerName, "Null"))
    {
    }
    //--Item has an owner:
    else
    {
        //--Variables.
        StarBitmap *rFaceImg = NULL;
        TwoDimensionReal tFaceDim;

        //--Resolve the owner's display name. It's usually the same, but not always (Ex: Chris, Christine).
        AdvCombatEntity *rOwningEntity = AdvCombat::Fetch()->GetRosterMemberS(pOwnerName);
        if(rOwningEntity)
        {
            rFaceImg = rOwningEntity->GetFaceProperties(tFaceDim);
        }

        //--Render the character's face.
        if(rFaceImg)
        {
            tFaceDim.mLft = tFaceDim.mLft;
            tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
            tFaceDim.mRgt = tFaceDim.mRgt;
            tFaceDim.mBot = 1.0f - (tFaceDim.mBot);
            rFaceImg->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(cFaceX,          pYPosition + cFaceOffY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(cFaceX + cFaceW, pYPosition + cFaceOffY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(cFaceX + cFaceW, pYPosition + cFaceOffY + cFaceW);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(cFaceX,          pYPosition + cFaceOffY + cFaceW);
            glEnd();
        }
    }

    //--Non-stacking item.
    if(pItem->GetStackSize() == 1)
    {
        MonoImages.rFont_Entry->DrawTextFixed(cNameX, pYPosition + cNameOffY, cxMaxNameLen, 0, pItem->GetDisplayName());
    }
    //--Quantity, if applicable:
    else
    {
        char *tBuffer = InitializeString("%s x%i", pItem->GetDisplayName(), pItem->GetStackSize());
        MonoImages.rFont_Entry->DrawTextFixed(cNameX, pYPosition + cNameOffY, cxMaxNameLen, 0, tBuffer);
        free(tBuffer);
    }

    //--Type.
    const char *rType = pItem->GetDescriptionType();
    if(rType)
        AdvImages.rFont_Statistic->DrawText(cTypeX, pYPosition + cNameOffY + cSmlTxtOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, rType);

    //--Value. Does not render for key items as they cannot be sold.
    if(pItem->IsKeyItem() == false && pItem->GetValue() > 0)
        AdvImages.rFont_Statistic->DrawTextArgs(cValueX, pYPosition + cNameOffY + cSmlTxtOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", pItem->GetValue());

    //--Gem slots.
    for(int i = 0; i < pItem->GetGemSlots(); i ++)
    {
        //--If there's a gem in the slot, render it under the frame.
        AdventureItem *rGemInSlot = pItem->GetGemInSlot(i);
        if(rGemInSlot)
        {
            StarBitmap *rItemIcon = rGemInSlot->GetIconImage();
            if(rItemIcon) rItemIcon->Draw(cGemX + (cGemW * i), pYPosition + cGemOffY);
        }

        //--Frame.
        AdvImages.rOverlay_GemSlot->Draw(cGemX + (cGemW * i), pYPosition + cGemOffY);
    }

    //--Setup.
    CombatStatistics *rCombatStatistics = pItem->GetEquipStatistics();
    int tHPVal  = rCombatStatistics->GetStatByIndex(STATS_HPMAX);
    int tAtkVal = rCombatStatistics->GetStatByIndex(STATS_ATTACK);
    int tAccVal = rCombatStatistics->GetStatByIndex(STATS_ACCURACY);
    int tEvdVal = rCombatStatistics->GetStatByIndex(STATS_EVADE);
    int tIniVal = rCombatStatistics->GetStatByIndex(STATS_INITIATIVE);
    int tPrtVal = rCombatStatistics->GetStatByIndex(STATS_RESIST_PROTECTION);

    //--Properties.
    if(tHPVal  != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 0.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tHPVal);
    if(tAtkVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 1.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tAtkVal);
    if(tAccVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 2.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tAccVal);
    if(tEvdVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 3.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tEvdVal);
    if(tIniVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 4.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tIniVal);
    if(tPrtVal != 0) AdvImages.rFont_Statistic->DrawTextArgs(cPropertyX + (cPropertyW * 5.0f), pYPosition + cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tPrtVal);
}

///======================================== Description ===========================================
void MonoUIInventory::RenderInventoryDescription(AdventureItem *pItem)
{
    ///--[Documentation]
    //--Given an inventory item, renders its description text and the frame around it. Always renders
    //  the frame even if no item is provided.
    //--The format is identical to the Equipment UI version.
    AdvImages.rFrame_Description->Draw();

    //--If no item is provided, because the inventory has no items or there is otherwise an error,
    //  stop rendering here.
    if(!pItem) return;

    ///--[Header]
    //--Fixed-size header.
    MonoImages.rOverlay_DescriptionName->Draw();
    mMonoParagraphColor.SetAsMixer();
    AdvImages.rFont_Heading->DrawText(195.0f, 496.0f, 0, 1.0f, pItem->GetDisplayName());

    ///--[Text Lines]
    //--Setup.
    float cXPosition = 129.0f;
    float cYPosition = 563.0f;
    float cYHeight   =  22.0f;

    //--Render advanced description lines.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = pItem->GetAdvancedDescription(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///====================================== Deconstruction ==========================================
void MonoUIInventory::RenderDeconstruct(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the deconstruction overlay. This is a window and some text showing what item is getting
    //  deconstructed and what it will break down into.
    //--Identical to the base version but uses translated strings.
    if(mDeconstructTimer < 1 || pVisAlpha <= 0.0f) return;

    ///--[Backing]
    //--Compute timer percent.
    float cPct = EasingFunction::QuadraticInOut(mDeconstructTimer, cxDeconstructTicks);

    //--Darken the background.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cPct));

    ///--[Window and Text]
    //--Position offset.
    float cYPos = (1.0f - cPct) * 700.0f;
    glTranslatef(0.0f, cYPos, 0.0f);

    //--Window.
    AdvImages.rFrame_DeconstructBox->Draw();

    //--Text if the item is equipped and threw an error.
    if(mDeconstructErrorEquipped)
    {
        AdvImages.rFont_Heading ->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 171.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextCannotDeconstruct);
        AdvImages.rFont_Mainline->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 208.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextDeconNoEquip);
    }
    //--Text if the item cannot be deconstructed (or the script otherwise didn't handle it).
    else if(mDeconstructErrorNoResults)
    {
        AdvImages.rFont_Heading ->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 171.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextCannotDeconstruct);
        AdvImages.rFont_Mainline->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 208.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextDeconNoResult);
    }
    //--Normal text.
    else
    {
        //--Get the item's name.
        //AdventureInventory *rInventory = AdventureInventory::Fetch();
        //StarLinkedList *rItemList = rInventory->GetExtendedItemList();
        //AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);

        //--Render it as the title.
        char *tRenderString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mTextDeconConfirm, 1, "[sItemName]", mStoredItemName);
        AdvImages.rFont_Heading ->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 171.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tRenderString);
        free(tRenderString);

        //--Setup.
        float cLft = 368.0f;
        //float cRgt = 656.0f;
        float cXTxt = cLft;
        float cYTxt = 208.0f;
        float cYHei = AdvImages.rFont_Mainline->GetTextHeight();

        //--List of items it will yield.
        void *rPtr = mDeconstructResults->PushIterator();
        while(rPtr)
        {
            //--Render.
            const char *rItemName = mDeconstructResults->GetIteratorName();
            AdvImages.rFont_Mainline->DrawTextArgs(cXTxt, cYTxt, 0, 1.0f, "%s", rItemName);

            //--Next.
            cYTxt = cYTxt + cYHei;
            rPtr = mDeconstructResults->AutoIterate();
        }

        //--Help strings.
        mConfirmString->DrawText(368.0f, 312.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mCancelString-> DrawText(865.0f, 312.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Clean]
    glTranslatef(0.0f, -cYPos, 0.0f);
}
