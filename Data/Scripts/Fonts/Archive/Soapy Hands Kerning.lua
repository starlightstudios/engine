-- |[ ================================ Soapy Hands Font Kerning ================================ ]|
--Sets the kerning values for the listed font.

-- |[Main Scaler]|
--This sets the standard distance between letters.
StarFont_SetKerning(1.0)

-- |[Letter Groupings]|
--The value -1 indicates "All Letters". Use this for thin/thick letters.
--[=[
StarFont_SetKerning(string.byte("i"), -1, 3.0)
StarFont_SetKerning(-1, string.byte("i"), 2.0)

--Punctuation groupings.
StarFont_SetKerning(-1, string.byte(","), 2.0)
StarFont_SetKerning(-1, string.byte("."), 2.0)
StarFont_SetKerning(-1, string.byte("'"), 2.0)
StarFont_SetKerning(-1, string.byte("!"), 2.0)
StarFont_SetKerning(-1, string.byte(":"), 2.0)
StarFont_SetKerning(string.byte("'"), -1, 2.0)

--Space Sizings.
--StarFont_SetKerning(string.byte(" "), -1, 10.0)
--StarFont_SetKerning(-1, string.byte(" "), 0.0)

-- |[Punctuation-to-Punctuation]|
--StarFont_SetKerning(string.byte("."), string.byte("."), 0.0)

-- |[Numbers-To-Anything]|
--StarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

-- |[Letters-to-Letter]|
StarFont_SetKerning(string.byte("a"), string.byte("t"), 3.0)
StarFont_SetKerning(string.byte("A"), string.byte("t"), 3.0)
StarFont_SetKerning(string.byte("c"), string.byte("t"), 2.0)
StarFont_SetKerning(string.byte("i"), string.byte("e"), 2.0)
StarFont_SetKerning(string.byte("i"), string.byte("p"), 2.0)
StarFont_SetKerning(string.byte("e"), string.byte("t"), 3.0)
StarFont_SetKerning(string.byte("f"), string.byte("e"), -1.0)
StarFont_SetKerning(string.byte("f"), string.byte("l"), -1.0)
StarFont_SetKerning(string.byte("n"), string.byte("f"), 3.0)
StarFont_SetKerning(string.byte("n"), string.byte("t"), 3.0)
StarFont_SetKerning(string.byte("o"), string.byte("f"), 3.0)
StarFont_SetKerning(string.byte("o"), string.byte("t"), 3.0)
StarFont_SetKerning(string.byte("q"), string.byte("u"), 2.0)
StarFont_SetKerning(string.byte("s"), string.byte("t"), 3.0)
StarFont_SetKerning(string.byte("t"), string.byte("a"), -1.0)
StarFont_SetKerning(string.byte("t"), string.byte("i"), -1.0)
StarFont_SetKerning(string.byte("t"), string.byte("h"), -1.0)
StarFont_SetKerning(string.byte("t"), string.byte("s"), -1.0)
StarFont_SetKerning(string.byte("t"), string.byte("t"), -1.0)
StarFont_SetKerning(string.byte("t"), string.byte("u"), -1.0)
StarFont_SetKerning(string.byte("T"), string.byte("h"), -1.0)
StarFont_SetKerning(string.byte("u"), string.byte("i"), 2.0)
StarFont_SetKerning(string.byte("w"), string.byte("i"), 2.0)

-- |[Letter-to-Punctuation]|
StarFont_SetKerning(string.byte("t"), string.byte("."), 0.0)
StarFont_SetKerning(string.byte(" "), string.byte("i"), 4.0)
]=]
