//--Base
#include "WADFile.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "DebugManager.h"

//--The Universal Doom Map Format is a standardized, text-based map format which allows extensions well beyond
//  the usual capabilities of the Doom engine. It allows things like slopes, arguments, advanced scripting,
//  and extensions by various engines. This file helps to handle this map format.

///--[Entry Point]
void WADFile::LoadAsUDMF(WAD_Directory_Entry *pEntry)
{
    //--Loads as a UDMF based on the provided entry, which should be a TEXTMAP. Also sets various
    //  flags so the program knows this is a UDMF and it cannot use the basic types anymore.
    if(!pEntry || strcasecmp(pEntry->mType, "TEXTMAP")) return;

    //--Parsing.
    ParseUDMF(pEntry->mSize, pEntry->mData);
}

///--[Main Parser]
void WADFile::ParseUDMF(int pDataSize, uint8_t *pData)
{
    //--Given a block of data of known size, parses out the information as a UDMF TEXTMAP lump.
    //  This will create and store objects as is necessary.
    //--Worker functions are defined below which simplify some of the parsing.
    bool tIsBlockComment = false;
    uint32_t tCursor = 0;

    //--Build the thing lookups if that hasn't been done already.
    if(mThingLookupsTotal < 1) BuildThingLookups();

    //--Storage.
    StarLinkedList *tThingsList = new StarLinkedList(true);
    StarLinkedList *tVertexList = new StarLinkedList(true);
    StarLinkedList *tLinedefList = new StarLinkedList(true);
    StarLinkedList *tSidedefList = new StarLinkedList(true);
    StarLinkedList *tSectorList = new StarLinkedList(true);

    //--Activity.
    WAD_WorldSprite *rActiveThing = NULL;
    UDMFVertex *rActiveVertex = NULL;
    UDMFLinedef *rActiveLinedef = NULL;
    UDMFSidedef *rActiveSidedef = NULL;
    UDMFSector *rActiveSector = NULL;

    //--Buffers.
    char tFieldBuffer[STD_MAX_LETTERS];
    char tOperatorBuffer[STD_MAX_LETTERS];
    char tValueBuffer[STD_MAX_LETTERS];

    //--Loop.
    while(tCursor < (uint32_t)pDataSize)
    {
        //--Get the line until the next endline character. This is stored in the processing buffer.
        int i = 0;
        char tBuffer[STD_MAX_LETTERS];
        while(pData[tCursor] != 10 && pData[tCursor] != 13)
        {
            //--Copy.
            tBuffer[i+0] = pData[tCursor];
            tBuffer[i+1] = '\0';
            i ++;
            tCursor ++;
        }

        //--Last character is skipped.
        tCursor ++;

        //--Remove the comments from the line. This handles block comments.
        RemoveComments(tBuffer, tIsBlockComment);

        //--If the line consists of a }, then end the current object's parsing.
        if(tBuffer[0] == '}')
        {
            rActiveThing = NULL;
            rActiveLinedef = NULL;
            rActiveSector = NULL;
            rActiveSidedef = NULL;
            rActiveVertex = NULL;
            continue;
        }

        //--Check for creation cases.
        if(!rActiveThing && !rActiveLinedef && !rActiveSector && !rActiveSidedef && !rActiveVertex)
        {
            //--If it's a new thing...
            if(!strncasecmp(tBuffer, "thing", 5))
            {
                SetMemoryData(__FILE__, __LINE__);
                rActiveThing = (WAD_WorldSprite *)starmemoryalloc(sizeof(WAD_WorldSprite));
                rActiveThing->Initialize();
                tThingsList->AddElement("X", rActiveThing, &FreeThis);
                continue;
            }
            //--If it's a new linedef...
            else if(!strncasecmp(tBuffer, "linedef", 7))
            {
                SetMemoryData(__FILE__, __LINE__);
                rActiveLinedef = (UDMFLinedef *)starmemoryalloc(sizeof(UDMFLinedef));
                rActiveLinedef->Initialize();
                tLinedefList->AddElementAsTail("X", rActiveLinedef, &FreeThis);
                continue;
            }
            //--If it's a new sidedef...
            else if(!strncasecmp(tBuffer, "sidedef", 7))
            {
                SetMemoryData(__FILE__, __LINE__);
                rActiveSidedef = (UDMFSidedef *)starmemoryalloc(sizeof(UDMFSidedef));
                rActiveSidedef->Initialize();
                tSidedefList->AddElementAsTail("X", rActiveSidedef, &FreeThis);
                continue;
            }
            //--If it's a new vertex...
            else if(!strncasecmp(tBuffer, "vertex", 6))
            {
                SetMemoryData(__FILE__, __LINE__);
                rActiveVertex = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
                rActiveVertex->Initialize();
                rActiveVertex->mX = 0.0f;
                rActiveVertex->mY = 0.0f;
                tVertexList->AddElementAsTail("X", rActiveVertex, &FreeThis);
                continue;
            }
            //--If it's a new sector...
            else if(!strncasecmp(tBuffer, "sector", 6))
            {
                SetMemoryData(__FILE__, __LINE__);
                rActiveSector = (UDMFSector *)starmemoryalloc(sizeof(UDMFSector));
                rActiveSector->Initialize();
                tSectorList->AddElementAsTail("X", rActiveSector, &FreeThis);
                continue;
            }
        }

        //--If one of the above cases didn't handle it, then assume it needs to be parsed by the active object.
        if(!SplitToThree(tBuffer, tFieldBuffer, tOperatorBuffer, tValueBuffer)) continue;

        //--Debug printing.
        //fprintf(stderr, "%s %s %s\n", tFieldBuffer, tOperatorBuffer, tValueBuffer);

        //--Vertex.
        if(rActiveThing)   { HandleThing(  tFieldBuffer, tOperatorBuffer, tValueBuffer, rActiveThing);  continue; }
        if(rActiveVertex)  { HandleVertex( tFieldBuffer, tOperatorBuffer, tValueBuffer, rActiveVertex);  continue; }
        if(rActiveLinedef) { rActiveLinedef->HandleProperty(tFieldBuffer, tOperatorBuffer, tValueBuffer); continue; }
        if(rActiveSidedef) { HandleSidedef(tFieldBuffer, tOperatorBuffer, tValueBuffer, rActiveSidedef); continue; }
        if(rActiveSector)  { HandleSector( tFieldBuffer, tOperatorBuffer, tValueBuffer, rActiveSector);  continue; }
    }

    //--We can directly copy the information into the storage lists, no conversion necessary!
    mVerticesTotal = tVertexList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mVertices = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex) * mVerticesTotal);
    for(int i = 0; i < mVerticesTotal; i ++)
    {
        void *rData = tVertexList->GetElementBySlot(i);
        if(rData) memcpy(&mVertices[i], rData, sizeof(UDMFVertex));
    }

    //--Linedef.
    mLinedefsTotal = tLinedefList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mLinedefs = (UDMFLinedef *)starmemoryalloc(sizeof(UDMFLinedef) * mLinedefsTotal);
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        void *rData = tLinedefList->GetElementBySlot(i);
        if(rData) memcpy(&mLinedefs[i], rData, sizeof(UDMFLinedef));
    }

    //--Sidedefs.
    mSidedefsTotal = tSidedefList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mSidedefs = (UDMFSidedef *)starmemoryalloc(sizeof(UDMFSidedef) * mSidedefsTotal);
    for(int i = 0; i < mSidedefsTotal; i ++)
    {
        void *rData = tSidedefList->GetElementBySlot(i);
        if(rData) memcpy(&mSidedefs[i], rData, sizeof(UDMFSidedef));
    }

    //--Sectors.
    mSectorsTotal = tSectorList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mSectors = (UDMFSector *)starmemoryalloc(sizeof(UDMFSector) * mSectorsTotal);
    for(int i = 0; i < mSectorsTotal; i ++)
    {
        void *rData = tSectorList->GetElementBySlot(i);
        if(rData) memcpy(&mSectors[i], rData, sizeof(UDMFSector));
    }

    //--Things. This is handled by the subroutine, since some things don't become sprites.
    HandleUDMFThings(tThingsList);

    //--Report the data.
    DebugManager::PushPrint(true, "UDMF Report:\n");
    DebugManager::Print(" %i world sprites.\n", mWorldSpritesTotal);
    DebugManager::Print(" %i Vertexes.\n", mVerticesTotal);
    DebugManager::Print(" %i Linedefs.\n", mLinedefsTotal);
    DebugManager::Print(" %i Sidedefs.\n", mSidedefsTotal);
    DebugManager::Print(" %i Sectors.\n",  mSectorsTotal);
    DebugManager::PopPrint("Report complete.\n");

    //--Clean up.
    delete tThingsList;
    delete tVertexList;
    delete tLinedefList;
    delete tSidedefList;
    delete tSectorList;

    //--Verify the linedefs, and crossload their properties.
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        mLinedefs[i].ResolveSpecials();
        if(!mLinedefs[i].Verify(mVerticesTotal, mSidedefsTotal, mSidedefs, mSectorsTotal))
        {
            //DebugManager::ForcePrint("WADFile:ParseUDMF - Warning, linedef %i failed verification.\n", i);
        }
    }

    //--Crossload all the sprites.
    LoadThingSprites();
}
void WADFile::RemoveComments(char *pBuffer, bool &sBlockCommentStatus)
{
    //--Removes comments from the line. Receives sBlockCommentStatus and removes the entire line if it's true,
    //  unless a */ is found, which terminates block comment mode.
    if(!pBuffer) return;

    //--Flags.
    bool tFoundDoubleSlash = false;

    //--Scan.
    int tLen = (int)strlen(pBuffer);
    for(int i = 0; i < tLen; i ++)
    {
        //--In block comment mode, we're only looking for a "*/". Otherwise, remove all letters.
        if(sBlockCommentStatus)
        {
            //--If this is a */ case...
            if(i < (tLen - 1) && pBuffer[i] == '*' && pBuffer[i+1] == '/')
            {
                sBlockCommentStatus = false;
                pBuffer[i+0] = '\0';
                pBuffer[i+1] = '\0';
                i = i + 1;
                continue;
            }

            //--If not the end comment case, remove the letter.
            pBuffer[i] = '\0';
        }
        //--Otherwise, look for '//' cases and '/*' cases.
        else
        {
            //--If we already found a '//', remove letters.
            if(tFoundDoubleSlash)
            {
                pBuffer[i] = '\0';
                continue;
            }

            //--Look for a '//' case.
            if(i < tLen - 1 && pBuffer[i] == '/' && pBuffer[i+1] == '/')
            {
                tFoundDoubleSlash = true;
                pBuffer[i+0] = '\0';
                pBuffer[i+1] = '\0';
                i = i + 1;
                continue;
            }

            //--Look to start block comments.
            if(i < tLen - 1 && pBuffer[i] == '/' && pBuffer[i+1] == '*')
            {
                sBlockCommentStatus = true;
                pBuffer[i+0] = '\0';
                pBuffer[i+1] = '\0';
                i = i + 1;
                continue;
            }
        }
    }

    //--Is there anything on the line?
    int tFirstLetter = -1;
    for(int i = 0; i < tLen; i ++)
    {
        if(pBuffer[i] != '\0' && pBuffer[i] != ' ')
        {
            tFirstLetter = i;
            break;
        }
    }

    //--No letters? Lines was 100% comment.
    if(tFirstLetter == -1) return;

    //--Remove the whitespace to make the parser work better.
    for(int i = 0; i < tLen - tFirstLetter; i ++)
    {
        pBuffer[i] = pBuffer[tFirstLetter + i];
    }
}
bool WADFile::SplitToThree(char *pBaseBuffer, char *pFieldBuffer, char *pOperatorBuffer, char *pValueBuffer)
{
    //--Given pBaseBuffer, splits it into three sub-buffers. The sub-buffers must be allocated externally, this function
    //  will modify them. The sub-buffers should be as large as the base buffer, for safety.
    //--Returns false if something broke, true if successful.
    if(!pBaseBuffer || !pFieldBuffer || !pOperatorBuffer || !pValueBuffer) return false;

    //--Clearing.
    pFieldBuffer[0] = '\0';
    pOperatorBuffer[0] = '\0';
    pValueBuffer[0] = '\0';

    //--Standard format:
    //  [Field] [Operator] [Value];
    //  Ex: X = 5.0;
    //--Whitespace is optional!
    int tMode = 0;
    int tFieldCursor = 0;
    int tOperatorCursor = 0;
    int tValueCursor = 0;

    //--Parse.
    for(int i = 0; i < (int)strlen(pBaseBuffer); i ++)
    {
        //--Field mode.
        if(tMode == 0)
        {
            //--If the value is alphanumeric, store it.
            if((pBaseBuffer[i] >= '0' && pBaseBuffer[i] <= '9') ||
               (pBaseBuffer[i] >= 'A' && pBaseBuffer[i] <= 'Z') ||
               (pBaseBuffer[i] >= 'a' && pBaseBuffer[i] <= 'z'))
            {
                pFieldBuffer[tFieldCursor+0] = pBaseBuffer[i];
                pFieldBuffer[tFieldCursor+1] = '\0';
                tFieldCursor ++;
            }
            //--If the value is whitespace, ignore it.
            else if(pBaseBuffer[i] == ' ')
            {

            }
            //--Otherwise, go to operator mode.
            else
            {
                tMode = 1;
                i --;
            }
        }
        //--Operator mode.
        else if(tMode == 1)
        {
            //--We only expect equals signs.
            if(pBaseBuffer[i] == '=')
            {
                pOperatorBuffer[tOperatorCursor+0] = pBaseBuffer[i];
                pOperatorBuffer[tOperatorCursor+1] = '\0';
                tOperatorCursor ++;
            }
            //--If the value is whitespace, ignore it.
            else if(pBaseBuffer[i] == ' ')
            {

            }
            //--Otherwise, go to value mode.
            else
            {
                tMode = 2;
                i --;
            }
        }
        //--Value mode.
        else if(tMode == 2)
        {
            //--If the value is whitespace, ignore it. Quotes are also ignored.
            if(pBaseBuffer[i] == ' ' || pBaseBuffer[i] == '"')
            {

            }
            //--If the value is a semicolon, end.
            else if(pBaseBuffer[i] == ';')
            {
                tMode = 3;
                break;
            }
            //--Otherwise, store it.
            else
            {
                pValueBuffer[tValueCursor+0] = pBaseBuffer[i];
                pValueBuffer[tValueCursor+1] = '\0';
                tValueCursor ++;
            }
        }
    }

    //--Everything worked.
    return tMode == 3;
}
void WADFile::HandleThing(char *pField, char *pOperator, char *pValue, WAD_WorldSprite *pWorldSprite)
{
    //--Handles arguments assuming they are referring to a WAD_WorldSprite object.
    if(!pField || !pOperator || !pValue || !pWorldSprite) return;
    if(pField[0] == '\0' || pOperator[0] == '\0' || pValue[0] == '\0') return;

    //--X Position. Float.
    if(!strcasecmp(pField, "x"))
    {
        pWorldSprite->mX = atof(pValue);
    }
    //--Y Position. Float.
    else if(!strcasecmp(pField, "y"))
    {
        pWorldSprite->mY = atof(pValue) * -1.0f; //Y Coordinate is flipped in Starlight Engine.
    }
    //--Z Position. Float.
    else if(!strcasecmp(pField, "height"))
    {
        pWorldSprite->mZ = atof(pValue);
    }
    //--Arg 0-4. Used for more advanced things.
    else if(!strncasecmp(pField, "arg", 3))
    {
        int tSlot = pField[3] - '0';
        if(tSlot >= 0 && tSlot <= 4) pWorldSprite->mArgs[tSlot] = atof(pValue);
    }
    //--Type. Determines sprite loading.
    else if(!strcasecmp(pField, "type"))
    {
        //--Store.
        pWorldSprite->mUniversalType = atoi(pValue);

        //--Type: Player 1 Start. This positions the 3DCamera.
        if(pWorldSprite->mUniversalType == 1)
        {
            //--Resolve the height.
            float tHeight = GetHeightAtPoint(pWorldSprite->mX, pWorldSprite->mY);

            //--Move the camera up a bit, since most people don't have their heads on the floor.
            tHeight = tHeight - 48.0f;

            //--Set the camera's XYZ position.
            StarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
            rCamera->SetPosition(pWorldSprite->mX, -pWorldSprite->mY, -PLAYER_HEIGHT);
            //fprintf(stderr, "Set position %f %f %f\n", (float)tThingListing[i].mXPosition, (float)-tThingListing[i].mYPosition, tHeight);

            //--Get the facing angle. 0 is east, 90 is north, 180 is west, and so on.
            //rCamera->SetRotation(90.0f, 0.0f, (tThingListing[i].mAngle - 90) * -1.0f);
            return;
        }

        //--Scan the lookups.
        for(int p = 0; p < mThingLookupsTotal; p ++)
        {
            //--Compare.
            if(pWorldSprite->mUniversalType == mThingLookups[p].mTypeCode)
            {
                //--Store the name for later resolving.
                memcpy(pWorldSprite->mName, mThingLookups[p].mNameCode, sizeof(char) * 8);

                //--Ignore further lookups.
                break;
            }
        }
    }
}
void WADFile::HandleVertex(char *pField, char *pOperator, char *pValue, UDMFVertex *pVertex)
{
    //--Handles arguments assuming they are referring to a UDMFVertex object.
    if(!pField || !pOperator || !pValue || !pVertex) return;
    if(pField[0] == '\0' || pOperator[0] == '\0' || pValue[0] == '\0') return;

    //--X Position. Float.
    if(!strcasecmp(pField, "x"))
    {
        pVertex->mX = atof(pValue);
    }
    //--Y Position. Float.
    else if(!strcasecmp(pField, "y"))
    {
        pVertex->mY = atof(pValue) * -1.0f; //Y Coordinate is flipped in Starlight Engine.
    }
    //--Special floor height.
    else if(!strcasecmp(pField, "zfloor"))
    {
        pVertex->mHasSpecialFloor = true;
        pVertex->mFloorZ = atof(pValue);
    }
    //--Special ceiling height.
    else if(!strcasecmp(pField, "zceiling"))
    {
        pVertex->mHasSpecialCeiling = true;
        pVertex->mCeilingZ = atof(pValue);
    }
}
void WADFile::HandleSidedef(char *pField, char *pOperator, char *pValue, UDMFSidedef *pSidedef)
{
    //--Handles operations assuming they are referring to a UDMFSidedef object.
    if(!pField || !pOperator || !pValue || !pSidedef) return;
    if(pField[0] == '\0' || pOperator[0] == '\0' || pValue[0] == '\0') return;

    //--X Offset.
    if(!strcasecmp(pField, "offsetx"))
    {
        pSidedef->mOffsetX = atoi(pValue);
    }
    //--Y Offset.
    else if(!strcasecmp(pField, "offsety"))
    {
        pSidedef->mOffsetY = atoi(pValue);
    }
    //--X Offset, only applies to the upper face.
    else if(!strcasecmp(pField, "offsetx_top"))
    {
        pSidedef->mOffsetUX = atoi(pValue);
    }
    //--Y Offset, only applies to the upper face.
    else if(!strcasecmp(pField, "offsety_top"))
    {
        pSidedef->mOffsetUY = atoi(pValue);
    }
    //--X Offset, only applies to the middle face.
    else if(!strcasecmp(pField, "offsetx_middle"))
    {
        pSidedef->mOffsetMX = atoi(pValue);
    }
    //--Y Offset, only applies to the middle face.
    else if(!strcasecmp(pField, "offsety_middle"))
    {
        pSidedef->mOffsetMY = atoi(pValue);
    }
    //--X Offset, only applies to the lower face.
    else if(!strcasecmp(pField, "offsetx_bottom"))
    {
        pSidedef->mOffsetLX = atoi(pValue);
    }
    //--Y Offset, only applies to the lower face.
    else if(!strcasecmp(pField, "offsety_bottom"))
    {
        pSidedef->mOffsetLY = atoi(pValue);
    }
    //--Name of upper texture. Max 8 letters, but is not always guaranteed to be 8.
    else if(!strcasecmp(pField, "texturetop"))
    {
        strncpy(pSidedef->mTexTop, pValue, 8);
    }
    //--Name of middle texture. Max 8 letters, but is not always guaranteed to be 8.
    else if(!strcasecmp(pField, "texturemiddle"))
    {
        strncpy(pSidedef->mTexMid, pValue, 8);
    }
    //--Name of bottom texture. Max 8 letters, but is not always guaranteed to be 8.
    else if(!strcasecmp(pField, "texturebottom"))
    {
        strncpy(pSidedef->mTexBot, pValue, 8);
    }
    //--Sector associated with this side.
    else if(!strcasecmp(pField, "sector"))
    {
        pSidedef->mSector = atoi(pValue);
    }
}
void WADFile::HandleSector(char *pField, char *pOperator, char *pValue, UDMFSector *pSector)
{
    //--Handles operations assuming they are referring to a UDMFSector object.
    if(!pField || !pOperator || !pValue || !pSector) return;
    if(pField[0] == '\0' || pOperator[0] == '\0' || pValue[0] == '\0') return;

    //--X Offset.
    if(!strcasecmp(pField, "heightfloor"))
    {
        pSector->mHeightFloor = atoi(pValue);
    }
    //--Y Offset.
    else if(!strcasecmp(pField, "heightceiling"))
    {
        pSector->mHeightCeiling = atoi(pValue);
    }
    //--Name of floor texture.
    else if(!strcasecmp(pField, "texturefloor"))
    {
        strncpy(pSector->mTexFloor, pValue, 8);
    }
    //--Name of ceiling texture.
    else if(!strcasecmp(pField, "textureceiling"))
    {
        strncpy(pSector->mTexCeiling, pValue, 8);
    }
    //--Light value.
    else if(!strcasecmp(pField, "lightlevel"))
    {
        pSector->mLightLevel = atoi(pValue);
    }
    //--Special properties.
    else if(!strcasecmp(pField, "special"))
    {
        pSector->mSpecial = atoi(pValue);
    }
    //--Tag, used for switches.
    else if(!strcasecmp(pField, "id"))
    {
        pSector->mID = atoi(pValue);
    }
    //--Texture offset X floor.
    else if(!strcasecmp(pField, "xpanningfloor"))
    {
        pSector->mXOffsetF = atoi(pValue);
    }
    //--Texture offset Y floor.
    else if(!strcasecmp(pField, "ypanningfloor"))
    {
        pSector->mYOffsetF = atoi(pValue);
    }
    //--Texture offset X ceiling.
    else if(!strcasecmp(pField, "xpanningceiling"))
    {
        pSector->mXOffsetC = atoi(pValue);
    }
    //--Texture offset Y ceiling.
    else if(!strcasecmp(pField, "ypanningceiling"))
    {
        pSector->mYOffsetC = atoi(pValue);
    }
}
