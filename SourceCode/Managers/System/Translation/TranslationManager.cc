//--Base
#include "TranslationManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers

///========================================== System ==============================================
TranslationManager::TranslationManager()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[System]
    mTranslationMasterList = new StarLinkedList(true);

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
TranslationManager::~TranslationManager()
{
    delete mTranslationMasterList;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void TranslationManager::RegisterEntry(const char *pName, TranslationModule *pModule)
{
    ///--[Documentation]
    //--Registers and takes ownership of a module.
    if(!pName || !pModule) return;

    //--Duplicate check.
    if(mTranslationMasterList->IsElementOnList(pModule)) return;

    //--Register.
    mTranslationMasterList->AddElement(pName, pModule, &TranslationModule::DeleteThis);
}
void TranslationManager::SetStringIn(const char *pEntry, int pSlot, const char *pString)
{
    ///--[Documentation]
    //--Locates a given module, and changes the string in the given slot to the given string. This
    //  does the bulk of translating strings.
    if(!pEntry || pSlot < 0 || !pString) return;

    ///--[Locate, Check]
    //--Check if the module exists.
    TranslationModule *rCheckModule = (TranslationModule *)mTranslationMasterList->GetElementByName(pEntry);
    if(!rCheckModule)
    {
        fprintf(stderr, "TranslationManager:SetStringIn() - Failed. Could not locate module %s.\n", pEntry);
        return;
    }

    //--Make sure the slot is not out of range.
    if(pSlot >= rCheckModule->GetSize())
    {
        fprintf(stderr, "TranslationManager:SetStringIn() - Failed. Module %s is of size %i, attempted to set slot %i to %s.\n", pEntry, rCheckModule->GetSize(), pSlot, pString);
        return;
    }

    //--Valid. Set.
    rCheckModule->SetString(pSlot, pString);
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
TranslationManager *TranslationManager::Fetch()
{
    return Global::Shared()->gTranslationManager;
}

///======================================== Lua Hooking ===========================================
void TranslationManager::HookToLuaState(lua_State *pLuaState)
{
    /* TM_SetStringIn(sName, iSlot, sString)
       Locates the named module and changes the string in the provided slot. */
    lua_register(pLuaState, "TM_SetStringIn", &Hook_TM_SetStringIn);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_TM_SetStringIn(lua_State *L)
{
    //TM_SetStringIn(sName, iSlot, sString)

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TM_SetStringIn");

    //--Set.
    TranslationManager::Fetch()->SetStringIn(lua_tostring(L, 1), lua_tointeger(L, 2), lua_tostring(L, 3));
    return 0;
}
