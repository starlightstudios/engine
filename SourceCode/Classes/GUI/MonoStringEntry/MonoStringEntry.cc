//--Base
#include "MonoStringEntry.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
MonoStringEntry::MonoStringEntry()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ====== StringEntry ======= ]
    ///--[ ==== MonoStringEntry ===== ]
    ///--[System]
    ///--[Images]
    mMonoTitle.    SetRGBAI(236, 211, 160, 255);
    mMonoSubtitle. SetRGBAI(181, 112,  41, 255);
    mMonoParagraph.SetRGBAI(214, 188, 219, 255);
    memset(&MonoImg, 0, sizeof(MonoImg));

    ///--[ ================ Construction ================ ]
    ///--[Setup]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Images]
    //--Fonts
    MonoImg.Data.rFontHeader   = rDataLibrary->GetFont("Mono String Entry Header");
    MonoImg.Data.rFontCurEntry = rDataLibrary->GetFont("Mono String Entry Current");
    MonoImg.Data.rFontMainline = rDataLibrary->GetFont("Mono String Entry Mainline");

    //--Images
    MonoImg.Data.rRenderBase    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonocerosUI/String/Base");
    MonoImg.Data.rCursor        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonocerosUI/String/Cursor");
    MonoImg.Data.rRenderPressed = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonocerosUI/String/Pressed");

    ///--[Verify]
    MonoImg.mIsReady = VerifyStructure(&MonoImg.Data, sizeof(MonoImg.Data), sizeof(void *), false);

    //--Error print.
    if(!MonoImg.mIsReady)
    {
        VerifyStructure(&MonoImg.Data, sizeof(MonoImg.Data), sizeof(void *), true);
    }

    ///--[ ============ Rendering Positions ============= ]
    //--Rendering position constants.
    float cRenderX = 0.0f;
    float cRenderY = 0.0f;

    //--1234567890
    float cBtnSqr = 53.0f;
    float cBtnSpc = cBtnSqr + 3.0f;
    float cXPos = 377.0f + cRenderX;
    float tYPos = 293.0f;
    for(int i = 0; i < 10; i ++)
    {
        mButtons[SE_BTN_1 + i].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[SE_BTN_1 + i], "%i", (i+1) % 10);
    }

    //--QWERTYUIOP
    cXPos = 403.0f + cRenderX;
    tYPos = 349.0f;
    int cOrder[] = {SE_BTN_Q, SE_BTN_W, SE_BTN_E, SE_BTN_R, SE_BTN_T, SE_BTN_Y, SE_BTN_U, SE_BTN_I, SE_BTN_O, SE_BTN_P};
    for(int i = 0; i < 10; i ++)
    {
        mButtons[cOrder[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder[i]], "%c", cOrder[i] + 'a');
    }

    //--ASDFGHJKL
    cXPos = 429.0f + cRenderX;
    tYPos = 405.0f;
    int cOrder2[] = {SE_BTN_A, SE_BTN_S, SE_BTN_D, SE_BTN_F, SE_BTN_G, SE_BTN_H, SE_BTN_J, SE_BTN_K, SE_BTN_L};
    for(int i = 0; i < 9; i ++)
    {
        mButtons[cOrder2[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder2[i]], "%c", cOrder2[i] + 'a');
    }

    //--ZXCVBNM
    cXPos = 455.0f + cRenderX;
    tYPos = 461.0f;
    int cOrder3[] = {SE_BTN_Z, SE_BTN_X, SE_BTN_C, SE_BTN_V, SE_BTN_B, SE_BTN_N, SE_BTN_M};
    for(int i = 0; i < 7; i ++)
    {
        mButtons[cOrder3[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder3[i]], "%c", cOrder3[i] + 'a');
    }

    //--Other keys.
    mButtons[SE_BTN_CAPSLOCK].SetWH(259.0f + cRenderX, 405.0f, 167.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_CAPSLOCK], "Caps");

    mButtons[SE_BTN_SHIFT].SetWH(285.0f + cRenderX, 461.0f, 167.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SHIFT], "Shift");

    mButtons[SE_BTN_SPACE].SetWH(481.0f + cRenderX, 517.0f, 351.0f, 54.0f);
    strcpy(mBtnStrings[SE_BTN_SPACE], "Space");

    mButtons[SE_BTN_BACKSPACE].SetWH(937.0f + cRenderX, 293.0f, 167.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_BACKSPACE], "Undo");

    //--Smaller special keys.
    mButtons[SE_BTN_COMMA].SetWH(847.0f + cRenderX, 461.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_COMMA], ",");

    mButtons[SE_BTN_PERIOD].SetWH(903.0f + cRenderX, 461.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_PERIOD], ".");

    mButtons[SE_BTN_SLASH].SetWH(959.0f + cRenderX, 461.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SLASH], "/");

    mButtons[SE_BTN_MINUS].SetWH(911.0f + cRenderX, 237.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_MINUS], "-");

    mButtons[SE_BTN_EQUALS].SetWH(967.0f + cRenderX, 237.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_EQUALS], "=");

    mButtons[SE_BTN_BACKSLASH].SetWH(1023.0f + cRenderX, 237.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_BACKSLASH], "\\");

    mButtons[SE_BTN_GRAVE].SetWH(321.0f + cRenderX, 293.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_GRAVE], "`");

    mButtons[SE_BTN_SEMICOLON].SetWH(933.0f + cRenderX, 405.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SEMICOLON], ";");

    mButtons[SE_BTN_QUOTE].SetWH(989.0f + cRenderX, 405.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_QUOTE], "'");

    mButtons[SE_BTN_LBRACE].SetWH(963.0f + cRenderX, 349.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_LBRACE], "[");

    mButtons[SE_BTN_RBRACE].SetWH(1019.0f + cRenderX, 349.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_RBRACE], "]");

    //--Finishing button.
    mButtons[SE_BTN_DONE].SetWH(997.0f + cRenderX, 571.0f, 167.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_DONE], "Done");

    //--Cancel button.
    mButtons[SE_BTN_CANCEL].SetWH(997.0f + cRenderX, 628.0f, 167.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_CANCEL], "Cancel");
}
MonoStringEntry::~MonoStringEntry()
{

}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoStringEntry::Render(float pOpacity)
{
    ///--[Documentation and Setup]
    //--Renders the StringEntry object, including highlighting the selected button. This modifies the
    //  color mixer back to default when execution completes.
    if(!Images.mIsReady || !MonoImg.mIsReady || pOpacity <= 0.0f) return;

    //--Set opacity.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);

    //--Constants.
    float cRenderX = 0.0f;
    float cRenderY = 0.0f;
    float cTextX = 436.0f;
    float cTextY = 148.0f;
    float cMaxLength = 515.0f;

    //--Base.
    MonoImg.Data.rRenderBase->Draw(cRenderX, cRenderY);

    ///--[Text Parts]
    //--Current entry is the pinkish color.
    mMonoParagraph.SetAsMixerAlpha(pOpacity);

    //--Current text, if not NULL and containing at least one letter:
    bool tVeryLongString = false;
    if(mCurrentString[0] != '\0')
    {
        //--Check the length of the string.
        float cLength = MonoImg.Data.rFontCurEntry->GetTextWidth(mCurrentString);

        //--Under the mandated length, so render the string.
        if(cLength < cMaxLength)
        {
            MonoImg.Data.rFontCurEntry->DrawText(cTextX, cTextY, 0, 1.0f, mCurrentString);
        }
        //--Render "...LastPartOfString"
        else
        {
            //--Flag.
            tVeryLongString = true;

            //--Create a clone string that contains the original. Then cut the front off until it comes in
            //  under the mandated max length.
            int i = 0;
            char *rReadPoint = &mCurrentString[0];
            int tStringLen = (int)strlen(mCurrentString);
            while(cLength >= cMaxLength)
            {
                //--Check EOF case:
                if(i >= tStringLen-1) break;

                //--Advance the read point.
                i ++;
                rReadPoint = &mCurrentString[i];

                //--Recompute length.
                cLength = MonoImg.Data.rFontCurEntry->GetTextWidth(rReadPoint);
            }

            //--Render.
            MonoImg.Data.rFontCurEntry->DrawText(cTextX-20, cTextY, 0, 1.0f, "...");
            MonoImg.Data.rFontCurEntry->DrawText(cTextX, cTextY, 0, 1.0f, rReadPoint);
        }
    }
    //--Blank string.
    else
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pOpacity);
        MonoImg.Data.rFontCurEntry->DrawText(cTextX, cTextY, 0, 1.0f, mEmptyString);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pOpacity);
    }

    //--Flashing indicator and backing.
    float cLen = MonoImg.Data.rFontCurEntry->GetTextWidth(mCurrentString) * 1.0f;
    float cIndicatorX = cTextX + cLen;
    float cIndicatorY = cTextY;
    if(tVeryLongString) cIndicatorX = 970.0f;
    MonoImg.Data.rCursor->Draw(cIndicatorX + 1.0f, cIndicatorY - 5.0f);

    //--Flashing indicator. Renders half the time.
    if(mTimer % 30 < 15)
    {
        //--Compute the indicator position.
        MonoImg.Data.rFontCurEntry->DrawText(cIndicatorX, cIndicatorY, 0, 1.0f, "_");
    }

    //--Render the prompt.
    if(mPromptString)
    {
        mMonoTitle.SetAsMixerAlpha(pOpacity);
        MonoImg.Data.rFontHeader->DrawText(683.0f, 63.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mPromptString);
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pOpacity);

    ///--[Input Clicks]
    //--Constants
    float cRenderWid = (float)MonoImg.Data.rRenderPressed->GetWidth();
    float cRenderHei = (float)MonoImg.Data.rRenderPressed->GetHeight();

    //--Show the keys pressed as slightly red.
    MonoImg.Data.rRenderPressed->Bind();
    glBegin(GL_QUADS);
    for(int i = 0; i < SE_BTN_TOTAL; i ++)
    {
        //--If the button's timer is less than the current timer, or higher than the fade value, don't render.
        if(mPressTimers[i] < mTimer) continue;

        //--Compute alpha.
        float cAlpha = (mPressTimers[i] - mTimer) / 15.0f;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pOpacity);

        //--Compute where this button is on the texture.
        float cTxL = (mButtons[i].mLft - 173.0f) / cRenderWid;
        float cTxT = (mButtons[i].mTop -  58.0f) / cRenderHei;
        float cTxR = (mButtons[i].mRgt - 173.0f) / cRenderWid;
        float cTxB = (mButtons[i].mBot -  58.0f) / cRenderHei;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex2f(mButtons[i].mLft, mButtons[i].mTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(mButtons[i].mRgt, mButtons[i].mTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(mButtons[i].mRgt, mButtons[i].mBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(mButtons[i].mLft, mButtons[i].mBot);
    }
    glEnd();
    StarlightColor::ClearMixer();

}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
StringEntry *MonoStringEntry::GenerateStringEntry()
{
    return new MonoStringEntry();
}

///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
