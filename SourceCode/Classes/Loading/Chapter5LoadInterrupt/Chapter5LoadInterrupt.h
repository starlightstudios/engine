///=================================== Chapter5LoadInterrupt ======================================
//--Version of the StarLoadInterrupt built for chapter 5.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLoadInterrupt.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define RUN_FRAMES 10
#define LI_CH5_DOTS 3

///========================================== Classes =============================================
class Chapter5LoadInterrupt : public StarLoadInterrupt
{
    private:
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    bool mIsReady;
    StarBitmap *mChristineFrames;
    StarBitmap *mRaijuFrames;
    StarBitmap *mBase;
    StarBitmap *mLoading;
    StarBitmap *mDots[LI_CH5_DOTS];

    protected:

    public:
    //--System
    Chapter5LoadInterrupt();
    virtual ~Chapter5LoadInterrupt();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators

    //--Core Methods
    virtual void DeallocateImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Call(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_BootChapter5(lua_State *L);
