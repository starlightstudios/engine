//--Base
#include "CarnationCombat.h"

//--Classes
#include "AdventureLevel.h"
#include "AudioPackage.h"
#include "CarnationLevel.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

///====================================== Initialization ==========================================
void CarnationCombat::Initialize()
{
    ///--[ ============= Root Call ============ ]
    AbyssCombat::Initialize();

    //--If already initialized, stop.
    if(mHasInitializedCarnCombat) return;
    mHasInitializedCarnCombat = true;

    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_CARNATIONCOMBAT;

    ///--[ ======== AdvCombat ======= ]
    ///--[ ==== CarnationCombat ===== ]
    ///--[System]
    mStatusInterceptFlag = false;
    mMouseX = 0;
    mMouseY = 0;
    mMouseZ = 0;

    ///--[Backgrounds]
    mLetterboxTimer = 0;
    mBackgroundTimer = 0;
    mBackgroundList = new StarLinkedList(true);

    ///--[Hitbox Storage]
    mButtonList = new StarLinkedList(true);

    ///--[Description]
    mShowDescriptions = true;
    mDescriptionAbilityName = NULL;
    mDescription = NULL;
    mToggleDescriptionHitbox.SetWH(1226.0f, 727.0f, 140.0f, 41.0f);

    ///--[Action Decision]
    mPartyDecisionMode = false;
    mIsHandlingEffortlessAction = false;
    mSelecterBuiltCommands = false;
    mPartyDecisionIndex = 0;
    mPartyDecisionTimer = 0;
    mStoredCommandInventorySlot = -1;
    rEffortlessActionCharacter = NULL;
    mPartyActions = new StarLinkedList(true);

    ///--[Textbox / Execution Sequence]
    mAnyAbilityReplied = false;
    mStatusPrintBlock = false;
    mAbilityExecPhase = 0;
    mIsRunningText = false;
    mTextTimerWaitForKeypress = false;
    mHighestLineRendered = 0;
    mTextPauseTimer = 0;
    mTextTimer = 0;
    mTextTimerMax = 0;
    mLettersPerTick = 1.00f;
    mTextStrings = new StarLinkedList(true);

    ///--[Victory/Defeat]
    mVictoryCalls = 0;
    mVictoryHandlerPath = NULL;
    mDefeatHandlerPath = NULL;
    mShowingLevelUpPack = NULL;
    mLevelUpPacks = new StarLinkedList(true);

    ///--[Specific Random Generation]
    mRandomNumberSeed = 0;

    ///--[Images]
    memset(&CarnationImg, 0, sizeof(CarnationImg));
}
void CarnationCombat::Reinitialize(bool pHandleMusic)
{
    ///--[ =========== Documentation and Setup ========== ]
    //--Re-initializes the class back to neutral variables, but leaves constants and referenced images in place. Used when the
    //  combat reboots between battles.
    AbyssCombat::Reinitialize(pHandleMusic);

    ///--[ =========== Variable Initialization ========== ]
    ///--[ ==== CarnationCombat ===== ]
    ///--[System]
    mStatusInterceptFlag = false;

    ///--[Backgrounds]
    mLetterboxTimer = 0;
    mBackgroundTimer = 0;

    ///--[Hitboxes]
    mButtonList->ClearList();

    ///--[Description]
    free(mDescriptionAbilityName);
    delete mDescription;
    mDescriptionAbilityName = NULL;
    mDescription = NULL;

    ///--[Action Decision]
    mPartyDecisionMode = false;
    mIsHandlingEffortlessAction = false;
    mSelecterBuiltCommands = false;
    mPartyDecisionIndex = 0;
    mPartyDecisionTimer = 0;
    rEffortlessActionCharacter = NULL;
    mPartyActions->ClearList();

    ///--[Textbox / Execution Sequence]
    mAnyAbilityReplied = false;
    mAbilityExecPhase = 0;
    mIsRunningText = false;
    mTextTimerWaitForKeypress = false;
    mHighestLineRendered = 0;
    mTextPauseTimer = 0;
    mTextTimer = 0;
    mTextTimerMax = 0;
    mLettersPerTick = 0.50f;
    mTextStrings->ClearList();

    ///--[Victory/Defeat]
    mVictoryCalls = 0;
    ResetString(mShowingLevelUpPack, NULL);
    mLevelUpPacks->ClearList();

    ///--[ =============== Derived Values =============== ]
    //--The letters-per-tick value is derived from the OptionsManager.
    OptionPack *rCombatTextSpeedPack = (OptionPack *)OptionsManager::Fetch()->GetOption("Combat Dialogue Speed");
    if(rCombatTextSpeedPack)
    {
        //--Value.
        int tTextSpeedValue = (*(int *)(rCombatTextSpeedPack->rPtr));

        //--Very slow.
        if(tTextSpeedValue == CC_TEXT_SPEED_VSLOW)
        {
            mLettersPerTick = 2.50f;
        }
        else if(tTextSpeedValue == CC_TEXT_SPEED_SLOW)
        {
            mLettersPerTick = 1.00f;
        }
        else if(tTextSpeedValue == CC_TEXT_SPEED_NORMAL)
        {
            mLettersPerTick = 0.66f;
        }
        else if(tTextSpeedValue == CC_TEXT_SPEED_FAST)
        {
            mLettersPerTick = 0.33f;
        }
        else if(tTextSpeedValue == CC_TEXT_SPEED_VFAST)
        {
            mLettersPerTick = 0.20f;
        }
    }
}
void CarnationCombat::HandleMusic()
{
    ///--[Documentation]
    //--Plays music at combat start. Typically only called if ordered to by the Reinitialize()
    //  function. Music can also be blocked by global flags (such as -blockmusic).
    //--The only difference from the base version is that a CarnationLevel cannot be inherited
    //  out of an AdventureLevel. This changes the layering code.
    if(!mNextCombatMusic && !mDefaultCombatMusic) return;

    ///--[Music Resolve]
    //--Setup.
    AudioManager *rAudioManager = AudioManager::Fetch();
    CarnationLevel *rCheckLevel = CarnationLevel::Fetch();

    //--Determine which track to use.
    bool tDeallocateMusic = false;
    char *rUseMusic = mDefaultCombatMusic;
    float tUseMusicStart = mCombatMusicStart;
    if(mNextCombatMusic)
    {
        rUseMusic = mNextCombatMusic;
        tUseMusicStart = mNextCombatMusicStart;
    }

    //--Stupid kazoo battle theme.
    if(rUseMusic && !strcasecmp(rUseMusic, "BattleThemeChristine"))
    {
        //--Perform a 1/1000 roll to see if it's going to be Kumquat's kazoo theme.
        int tRoll = (rand() % 1000);
        if(tRoll == 299)
        {
            tDeallocateMusic = true;
            rUseMusic = InitializeString("BattleThemeChristineKazoo");
        }
    }

    ///--[Normal Playing]
    //--Not Layering Tracks
    if(rCheckLevel && !rCheckLevel->IsLayeringMusic())
    {
        //--If the music start position is -1, don't reset the music at all. Let it play.
        if(tUseMusicStart == -1.0f)
        {
            mQuietBackgroundMusicForVictory = true;
        }
        //--Store the music position and play the new music.
        else
        {
            //--Store.
            AudioPackage *rPlayingMusic = rAudioManager->GetPlayingMusic();
            if(rPlayingMusic)
            {
                mEndCombatTracksTotal = 1;
                mEndCombatMusicResume = (float *)starmemoryalloc(sizeof(float));
                mEndCombatMusicResume[0] = rPlayingMusic->GetPosition();
            }

            //--Start playing music.
            rAudioManager->PlayMusic(rUseMusic);
            rAudioManager->SeekMusicTo(tUseMusicStart);
        }
    }
    ///--[Layered Tracks]
    //--Layered tracks. This is only the case if the combat music is not the maximum intensity track.
    //  The music positions are still stored but the values won't be used.
    else if(rCheckLevel && !AdventureLevel::xIsCombatMaxIntensity)
    {
        //--Allocate.
        mEndCombatTracksTotal = AdventureLevel::xCurrentlyRunningTracks;
        mEndCombatMusicResume = (float *)starmemoryalloc(sizeof(float) * mEndCombatTracksTotal);

        //--Iterate.
        for(int i = 0; i < AdventureLevel::xCurrentlyRunningTracks; i ++)
        {
            //--Check the package. If it exists, store its position.
            AudioPackage *rLayer = rAudioManager->GetMusicPack(AdventureLevel::xLayerNames[i]);
            if(rLayer)
            {
                mEndCombatMusicResume[i] = rLayer->GetPosition();
                rLayer->FadeOut(15);
            }
            //--Otherwise, store 0.0f.
            else
            {
                mEndCombatMusicResume[i] = 0.0f;
            }
        }

        //--Start playing music.
        rAudioManager->PlayMusic(rUseMusic);
        rAudioManager->SeekMusicTo(tUseMusicStart);
    }
    ///--[Layered Track, Max Intensity]
    //--Mandate combat intensity as maximum if combat is set that way.
    else if(rCheckLevel && AdventureLevel::xIsCombatMaxIntensity)
    {
        AdventureLevel::xCombatMandatedIntensity = 100.0f;
    }

    ///--[Finish Up]
    //--If the next-music handler was used, clear it.
    if(mNextCombatMusic)
    {
        ResetString(mNextCombatMusic, NULL);
        mNextCombatMusicStart = 0.0f;
    }

    //--Clean.
    if(tDeallocateMusic)
    {
        free(rUseMusic);
    }
}

///======================================== Construction ==========================================
void CarnationCombat::Construct()
{
    ///--[ ============= Base Call ============ ]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    AbyssCombat::Construct();

    //--Overrides.
    //const char *cInspectorResistNames[] = {"Protection", "Physical", "Bane", "Arcane", "Flame", "Freezing", "Shocking", "Crusading", "Obscuring", "Bleeding", "Poisoning", "Corroding", "Terrifying"};
    //Crossload(Images.Data.rInspectorResistIcons, sizeof(StarBitmap *) * ADVCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL, sizeof(StarBitmap *), "Root/Images/AdventureUI/DamageTypeIcons/%s", cInspectorResistNames, false);

    ///--[ =========== Construction =========== ]
    ///--[Crossload Fonts]
    CarnationImg.Font.rPlayerInterfaceFont     = rDataLibrary->GetFont("Carnation Combat Player UI");
    CarnationImg.Font.rTextBox                 = rDataLibrary->GetFont("Carnation Combat Textbox UI");
    CarnationImg.Font.rFontButton              = rDataLibrary->GetFont("Carn Combat Button");
    CarnationImg.Font.rFontConfirmBig          = rDataLibrary->GetFont("Carn Confirm Big");
    CarnationImg.Font.rFontConfirmSmall        = rDataLibrary->GetFont("Carn Confirm Small");
    CarnationImg.Font.rFontDescriptionHeading  = rDataLibrary->GetFont("Carn Combat Description Heading");
    CarnationImg.Font.rFontDescriptionMainline = rDataLibrary->GetFont("Carn Combat Description Mainline");

    ///--[Crossload Images]
    CarnationImg.Image.rFrame_Button      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/Frame_Button");
    CarnationImg.Image.rFrame_Description = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/Frame_Description");
    CarnationImg.Image.rHPBarBack         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/HP Bar Back");
    CarnationImg.Image.rHPBarFill         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/HP Bar Fill");
    CarnationImg.Image.rMPBarBack         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/MP Bar Back");
    CarnationImg.Image.rMPBarFill         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/MP Bar Fill");
    CarnationImg.Image.rPortraitFrame     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/Portrait Frame");
    CarnationImg.Image.rTextFrame         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/Text Frame");
    CarnationImg.Image.rTextFrameNoBack   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/CarnationUI/Combat/Text Frame No Back");

    ///--[Verify]
    CarnationImg.mIsReady =                          VerifyStructure(&CarnationImg.Font,  sizeof(CarnationImg.Font),  sizeof(void *), false);
    CarnationImg.mIsReady = CarnationImg.mIsReady && VerifyStructure(&CarnationImg.Image, sizeof(CarnationImg.Image), sizeof(void *), false);
}
void CarnationCombat::Disassemble()
{
    delete mBackgroundList;
    delete mButtonList;
    free(mDescriptionAbilityName);
    delete mDescription;
    delete mPartyActions;
    delete mTextStrings;
    free(mVictoryHandlerPath);
    free(mDefeatHandlerPath);
    free(mShowingLevelUpPack);
    delete mLevelUpPacks;
}
