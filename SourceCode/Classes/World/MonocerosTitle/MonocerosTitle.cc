//--Base
#include "MonocerosTitle.h"

//--Classes
#include "FlexButton.h"
#include "VisualLevel.h"
#include "MonocerosMenu.h"

//--CoreClasses
#include "StringEntry.h"
#include "MonoStringEntry.h"
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"
#include "VirtualFile.h"
#include "StarUIPiece.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "OptionsManager.h"
#include "SaveManager.h"

///--[Local Definitions]
//--Sizes
#define MONO_FILES_PER_PAGE 3

//--Timings
#define TITLE_OVER_TICKS 30
#define INTRO_TICKS ((int)(7.30f * 60.0f))

///========================================== System ==============================================
MonocerosTitle::MonocerosTitle()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_MONOCEROSTITLE;

    ///--[ ======== FlexMenu ======== ]
    //--System
    //--Splash Screen
    //--Selection
    //--Introduction Mode
    //--Loading Mode
    //--Scrollbar Click
    //--Rebinding Mode
    //--Header
    //--Descriptions
    //--Rendering Constants
    cFontSize = 1.0f;
    cStepRate = 49.0f;

    //--Rendering Variables
    //--Location
    //--Storage
    //--Images

    ///--[ ===== MonocerosTitle ===== ]
    ///--[Variables]
    //--System
    mTitleOverTimer = 0;
    mTitleShadeTimer = 0;
    mTitleOscillateTimer = 0;
    mMonoTitle.    SetRGBAI(236, 211, 160, 255);
    mMonoSubtitle. SetRGBAI(181, 112,  41, 255);
    mMonoParagraph.SetRGBAI(214, 188, 219, 255);

    //--Music
    mHasStartedMusic = false;

    //--Options Menu
    mIsShowingOptions = false;
    AdventureMenu::xSuppressLoadingErrors = true;
    DataLibrary::Fetch()->mSuppressFontErrors = true;
    mOptionsMenu = new MonocerosMenu();
    mOptionsMenu->ReverifyOptions();
    DataLibrary::Fetch()->mSuppressFontErrors = false;
    AdventureMenu::xSuppressLoadingErrors = false;

    //--Mod Load Order Menu
    mIsShowingModLoadOrder = false;
    mModChangesPending = false;
    mModOffset = 0;

    //--Other.
    mToggleAutosaveKey = new StarlightString();

    ///--[Languages]
    //--System
    mIsLanguageMenuActive = false;
    mIsLanguageSubmenuActive = false;

    //--Language Selection
    mLanguageTimer = 0;
    mStartingLanguage = 0;
    mSelectedLanguage = 0;
    mHitbox_LanguageSubmenu.SetWH(4.0f, 732.0f, 32.0f, 32.0f);

    //--Language Pack Creation/Deployment
    mLanguageHighlight = cxLanguage_Nothing;
    mLanguageUseForPacks = 0;
    mIsSelectingLanguagePack = false;
    mConfirmCreate = false;
    mConfirmDeploy = false;
    mCreateBackups = false;
    mHitbox_Create.Set(-100, -100, -99, -99);
    mHitbox_Deploy.Set(-100, -100, -99, -99);
    mHitbox_Confirm.SetWH(402.0f, 287.0f, 177.0f, 44.0f);
    mHitbox_Cancel.SetWH( 799.0f, 287.0f, 177.0f, 44.0f);
    mHitbox_Toggle.SetWH( 268.0f, 255.0f,  27.0f, 27.0f);

    //--Execution
    mIsLanguageExecuting = false;
    mExecutionTimer = 0;

    //--Storage
    mLanguageList = new StarLinkedList(true);

    ///--[Images]
    memset(&MonoImg, 0, sizeof(MonoImg));

    ///--[ ================ Construction ================ ]
    ///--[Images]
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts.
    MonoImg.Data.rFontDoubleHeader = rDataLibrary->GetFont("Monoceros Title Double Header");
    MonoImg.Data.rFontMenu         = rDataLibrary->GetFont("Monoceros Title Option");
    MonoImg.Data.rFontSaveStat     = rDataLibrary->GetFont("Monoceros Title Statistic");

    //--Images.
    MonoImg.Data.rBacking                 = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Backing");
    MonoImg.Data.rButton_LanguageShowMore = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Button_LanguageShowMore");
    MonoImg.Data.rFrameAutosavesButton    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_AutosavesButton");
    MonoImg.Data.rFrameBackButton         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_BackButton");
    MonoImg.Data.rFrame_Confirm           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_Confirm");
    MonoImg.Data.rFrameLanguage           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_Language");
    MonoImg.Data.rFrame_LanguageCheck     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_LanguageCheck");
    MonoImg.Data.rFrame_LanguageEx        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_LanguageEx");
    MonoImg.Data.rFrame_LanguageX         = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_LanguageX");
    MonoImg.Data.rFrameModLoadButtons     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_ModLoadButtons");
    MonoImg.Data.rFrameModLoadCheck       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_ModLoadCheck");
    MonoImg.Data.rFrameModLoadX           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_ModLoadX");
    MonoImg.Data.rFrameModLoadOrder       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_ModLoadOrder");
    MonoImg.Data.rFrameModUploadButton    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_ModUploadButton");
    MonoImg.Data.rFrameNewGame            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_NewGame");
    MonoImg.Data.rFrameNewGameSelect      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_NewGameSelect");
    MonoImg.Data.rFrameSaveCell           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_SaveCell");
    MonoImg.Data.rFrameSaveCellSelect     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_SaveCellSelect");
    MonoImg.Data.rFrameSaveModButton      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Frame_SaveModButton");
    MonoImg.Data.rNotification            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Notifier");
    MonoImg.Data.rOverlayFlagSelect       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Overlay_FlagSelect");
    MonoImg.Data.rOverlayFlagUK           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Overlay_FlagUK");
    MonoImg.Data.rOverlayRestart          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Overlay_Restart");
    MonoImg.Data.rOverlaySaveShade        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Overlay_SaveShade");
    MonoImg.Data.rOverlaySelection        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Overlay_Selection");
    MonoImg.Data.rOverlayShading          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Overlay_Shading");
    MonoImg.Data.rOverlayTitle            = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Overlay_Title");
    MonoImg.Data.rScrollbarBack           = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Scrollbar_Back");
    MonoImg.Data.rScrollbarFront          = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Scrollbar_Front");
    MonoImg.Data.rScrollbarModsBack       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/MonoTitle/Pieces/Scrollbar_ModsBack");

    ///--[Verify]
    MonoImg.mIsReady = VerifyStructure(&MonoImg.Data, sizeof(MonoImg.Data), sizeof(void *), false);
    if(!MonoImg.mIsReady)
    {
        fprintf(stderr, "Warning: MonocerosTitle failed to resolve assets. Printing report.\n");
        VerifyStructure(&MonoImg.Data, sizeof(MonoImg.Data), sizeof(void *), true);
    }

    ///--[Object Sizes]
    //--Modify sizes of objects.
    mBackBtn.SetWH       (   0.0f,   0.0f,  99.0f, 46.0f);
    mToggleAutosave.SetWH(1232.0f,   0.0f, 134.0f, 46.0f);
    mScrollUp.SetWH      (1313.0f,  99.0f,  38.0f, 12.0f);
    mScrollDn.SetWH      (1313.0f, 747.0f,  38.0f, 12.0f);
    mControlsBtn.Set   (-100.0f, -100.0f, -99.0f, -99.0f);

    ///--[Run Translation]
    //--Set control.
    ControlManager *rControlManager = ControlManager::Fetch();
    mToggleAutosaveKey->SetString(xrStringData->str.mTextToggleAutosaves);
    mToggleAutosaveKey->AllocateImages(1);
    mToggleAutosaveKey->SetImageP(0, 3.0f, rControlManager->ResolveControlImage("Run"));
    mToggleAutosaveKey->CrossreferenceImages();

    ///--[Default Language Pack]
    //--Create a British Flag language pack for the default language.
    LanguagePack *nPack = (LanguagePack *)starmemoryalloc(sizeof(LanguagePack));
    nPack->Initialize();
    nPack->mPosition.SetWH(499.0f, 120.0f, 120.0f, 60.0f);
    nPack->mLanguageCode = InitializeString("English");
    nPack->mFlag = MonoImg.Data.rOverlayFlagUK;
    nPack->mRestartProgram = MonoImg.Data.rOverlayRestart;
    mLanguageList->AddElementAsHead("X", nPack, &LanguagePack::DeleteThis);
}
MonocerosTitle::~MonocerosTitle()
{
    delete mOptionsMenu;
    delete mToggleAutosaveKey;
    delete mLanguageList;
}
void MonocerosTitle::Construct()
{
    FlexMenu::Construct();
}

///--[Public Variables]
//--Set to true initially, allows changing of languages. Once a game is launched, gets set to false for
//  the rest of the program's lifetime.
bool MonocerosTitle::xAllowLanguages = true;

//--Displays on the title screen to help bug reporting.
char *MonocerosTitle::xVersionString = NULL;

//--If true, the game assumes that Monoceros is in its own folder outside the engine, and adds that
//  as the search path.
bool MonocerosTitle::xIsDeveloperMode = false;

//--Static reference to translatable string lookups.
MonoTitle_Strings *MonocerosTitle::xrStringData = NULL;

///======================================== Translation ===========================================
MonoTitle_Strings::MonoTitle_Strings()
{
    //--Local name: "MonocerosTitle"
    MonocerosTitle::xrStringData = this;

    //--Strings
    str.mTextSelectASaveFile    = InitializeString("MonoTitle_mSelectSaveFile");                       //"Select a Save File"
    str.mTextNewGame            = InitializeString("MonoTitle_mNewGame");                              //"New Game"
    str.mTextRightClickAFile    = InitializeString("MonoTitle_mRightClickEditNote");                   //"Right click a file to edit its note"
    str.mTextBack               = InitializeString("MonoTitle_mBack");                                 //"Back"
    str.mTextAutoSaves          = InitializeString("MonoTitle_mAutosaves");                            //"Autosaves"
    str.mTextNormalSaves        = InitializeString("MonoTitle_mNormalSaves");                          //"Normal Saves"
    str.mTextSaveMod            = InitializeString("MonoTitle_mSave");                                 //"Save"
    str.mTextChangesPending     = InitializeString("MonoTitle_mChangesPending");                       //"Changes Pending..."
    str.mTextChangesModInstruct = InitializeString("MonoTitle_mClickSrrowsReorderActivateDeactivate"); //"Click arrows to reorder. Click X/Check to activate/deactivate."
    str.mTextToggleAutosaves    = InitializeString("MonoTitle_mToggleAutosaves");                      //"[IMG0] Toggle Autosaves"
    str.mTextDeveloperMenu      = InitializeString("MonoTitle_mDeveloperMenu");                        //"Developer Menu"
    str.mTextCreateNewPack      = InitializeString("MonoTitle_mCreateNewPack");                        //"Create New Pack"
    str.mTextLoadPack           = InitializeString("MonoTitle_mLoadPack");                             //"Load Pack"
    str.mTextWorking            = InitializeString("MonoTitle_mWorking...");                           //"Working..."
    str.mTextSelectLanguage     = InitializeString("MonoTitle_mSelectALanguage");                      //"Select a language"
    str.mTextAskCreateNewPack   = InitializeString("MonoTitle_mCreateNewPackage?");                    //"Create a new package?"
    str.mTextNewPackagePath     = InitializeString("MonoTitle_mNewPackagePath");                       //"New package: Data/Translation%s.slf"
    str.mTextBackupPack         = InitializeString("MonoTitle_mBackupExistingPack");                   //"Backup existing pack."
    str.mTextConfirm            = InitializeString("MonoTitle_mConfirm");                              //"Confirm"
    str.mTextCancel             = InitializeString("MonoTitle_mCancel");                               //"Cancel"
    str.mTextDeployPackage      = InitializeString("MonoTitle_mDeployPackage");                        //"Deploy package?"
    str.mTextWarningOverwrite   = InitializeString("MonoTitle_mWarnOverwrite");                        //"Warning: Will overwrite files of %s"
    str.mTextBackupFiles        = InitializeString("MonoTitle_mBackupFiles");                          //"Backup overwritten files"
}
MonoTitle_Strings::~MonoTitle_Strings()
{
    for(int i = 0; i < MONOTITLE_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoTitle_Strings::GetSize()
{
    return MONOTITLE_STRINGS_TOTAL;
}
void MonoTitle_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOTITLE_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
bool MonocerosTitle::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_MENU_ROOT) return true;
    if(pType == POINTER_TYPE_MENU_FLEX) return true;
    return (pType == mType);
}

///======================================= Manipulators ===========================================
void MonocerosTitle::ShowOptions()
{
    mIsShowingOptions = true;
    mOptionsMenu->Show();
    mOptionsMenu->SetToOptionsMode();
}

///======================================= Core Methods ===========================================
void MonocerosTitle::Clear()
{
    FlexMenu::Clear();
}
void MonocerosTitle::AutoSortButtons()
{
    //--Sorts the buttons and resizes the menu to accomodate them.
    if(!Images.mIsReady) return;
    mHasSortedRecently = true;

    //--First, get the buttons in order by priority. This is done automatically.
    mButtonList->SortListUsing(FlexMenu::CompareButtonPriorities);

    //--Optional image stuff.
    int tLargestLineCount = 0;
    float tTallestImage = 0.0f;

    //--Get the sizes we'll need.
    float tWidestButton = mHeaderWid;
    float tExpectedW = tWidestButton;
    float tExpectedH = mHeaderHei + cHeaderPadding + (cButtonIndentY * 2.0f) + ((float)mButtonList->GetListSize() * cStepRate) + (6.0f);
    FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--If the button has a description, activate descriptions mode.
        if(rButton->GetDescription())
        {
            //--Flag.
            mShowDescriptions = true;

            //--Count how many lines there are. Keep track of the highest count.
            RefreshDescriptionFrom(rButton);
            if(mLinesOccupied > tLargestLineCount) tLargestLineCount = mLinesOccupied;
        }

        //--If the button has an image, check its height.
        StarBitmap *rButtonImg = rButton->GetDescriptionImage();
        if(rButtonImg)
        {
            if(rButtonImg->GetTrueHeight() > tTallestImage) tTallestImage = rButtonImg->GetTrueHeight();
        }

        //--Sizing.
        rButton->SetFontSize(cFontSize);
        float tButtonW = rButton->GetTextWidth();
        if(tWidestButton < tButtonW) tWidestButton = tButtonW;

        //--Next button.
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }

    //--Clear description info.
    rLastHighlightedBtn = NULL;
    memset(mDescriptionLines, 0, sizeof(char) * FM_DESCRIPTION_LINES * FM_DESCRIPTION_CHARS);

    //--Pad out the width so we can have borders.
    tExpectedW = tWidestButton + (cButtonIndentX * 2.0f) + (10.0f);

    //--Now reposition everything.
    float tHighestBtn = 1000.0f;
    int tCount = 0;
    rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        //--Position.
        float tYPosition = cButtonIndentY + ((float)tCount * cStepRate) + mHeaderHei + cHeaderPadding + 56.0f;
        rButton->SetDimensionsWH(cButtonIndentX, tYPosition, tWidestButton, cStepRate);
        if(tYPosition < tHighestBtn) tHighestBtn = tYPosition;

        //--Next button.
        tCount ++;
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }

    //--Check: If any headers are wide enough, use them as the expected width. This is only used in the new UI format.
    if(!xUseClassicUI)
    {
        for(int i = 0; i < mHeaderLinesTotal; i ++)
        {
            float tHeaderLen = Images.Data.rUIFont->GetTextWidth(mHeaderLines[i].mText) * (mHeaderLines[i].cFontSize) * 1.07f;
            if(tHeaderLen > tExpectedW) tExpectedW = tHeaderLen;
        }
    }

    //--The menu becomes considerably wider in descriptions mode.
    if(mShowDescriptions)
    {
        //--Move the description dim over.
        mDescriptionDim.mLft = tWidestButton + 64.0f;
        mDescriptionDim.mRgt = mDescriptionDim.mLft + 500.0f;

        //--If there are description images, use their height.
        if(tExpectedH < tTallestImage) tExpectedH = tTallestImage;

        //--Set the top of the description based on how many lines it needed.
        mDescriptionDim.mTop = tHighestBtn;//tExpectedH - (mDescriptionDim.GetHeight() * tLargestLineCount);
        mDescriptionDim.mBot = mDescriptionDim.mTop + Images.Data.rUIFont->GetTextHeight();

        //--Recheck width.
        tExpectedW = tExpectedW + mDescriptionDim.GetWidth();
    }

    //--Resize the menu itself.
    mCanRender = true;
    float tX = (VIRTUAL_CANVAS_X * 0.5f) - (tExpectedW * 0.5f);
    float tY = (VIRTUAL_CANVAS_Y * 0.5f) - (tExpectedH * 0.5f) + 100.0f;
    mCoordinates.SetWH(tX, tY, tExpectedW, tExpectedH);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void MonocerosTitle::Update()
{
    ///--[Routing]
    //--If in Loading mode, do that instead.
    if(mSplashState > FM_SPLASH_STATE_NONE) { UpdateSplashMode();    return; }
    if(mIsRebindingMode)                    { UpdateRebindingMode(); return; }
    if(mIsLoadingMode)                      { UpdateLoadingMode();   return; }
    if(mIsShowingModLoadOrder)              { UpdateModMode();       return; }

    ///--[Options Menu]
    if(mIsShowingOptions)
    {
        //--Run update.
        mOptionsMenu->Update();

        //--If object is hidden, switch off options mode.
        if(mOptionsMenu->StartedHidingThisTick())
        {
            mIsShowingOptions = false;
        }
        return;
    }
    else
    {
        mOptionsMenu->UpdateBackground();
    }

    ///--[Timers]
    //--Update called when this menu is atop the stack. First, sort the buttons if we need to.
    mClearedSelfThisTick = false;
    if(!mHasSortedRecently)
    {
        AutoSortButtons();
    }

    //--Always-run.
    mTitleOscillateTimer ++;
    if(mTitleOscillateTimer >= MONOTITLE_OSCILLATE_TICKS) mTitleOscillateTimer = 0;
    mTitleShadeTimer ++;
    if(mTitleShadeTimer >= MONOTITLE_SHADE_TICKS) mTitleShadeTimer = 0;

    ///--[Submode Handlers]
    //--Intro mode: Run the timer.
    if(mIsIntroMode)
    {
        //--If the music hasn't started yet, do it now.
        if(mIntroTimer == 10 && !mHasStartedMusic)
        {
            mHasStartedMusic = true;
            AudioManager::Fetch()->PlayMusic("Witch Hunter Izana Title");
        }

        //--If the player presses a key, or we hit the end of the timer, end intro mode.
        mIntroTimer ++;
        MapManager::xCandidateAlpha = mIntroTimer / (float)INTRO_TICKS;
        if(mIntroTimer >= INTRO_TICKS || ControlManager::Fetch()->IsAnyKeyPressed())
        {
            //--If the player pushed a key then the audio may not have started so handle that.
            if(!mHasStartedMusic)
            {
                mHasStartedMusic = true;
                AudioManager::Fetch()->PlayMusic("Witch Hunter Izana Title");
            }

            //--End intro.
            mIsIntroMode = false;
            MapManager::xCandidateAlpha = 1.0f;
        }
        return;
    }
    else
    {
        if(mTitleOverTimer < TITLE_OVER_TICKS) mTitleOverTimer ++;
    }

    //--Language submenu. Handles update in subroutine. If the call returns true, stops the update.
    if(UpdateLanguageMode()) return;

    ///--[Mouse Setup]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get mouse position.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    ///--[Cheat Code Handlers]
    //--Reposition by local coordinates.
    tMouseX = tMouseX - (int)mCoordinates.mLft;
    tMouseY = tMouseY - (int)mCoordinates.mTop;

    //--Get codes.
    int tKeyboard, tMouse, tJoy;
    rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoy);
    const char *rPressedKeyName = rControlManager->GetNameOfKeyIndex(tKeyboard);

    //--Check progress on the cheat code for counting treasures.
    if(HandleCheatCode(rPressedKeyName, mCountTreasureProgress, COUNT_TREASURES_LEN, COUNT_TREASURES_WORD))
    {
        mCountTreasureProgress = 0;
        CountTreasures();
    }

    ///--[Mouse-Over]
    //--Run an update for all buttons. Only buttons using slider mode actually bother.
    FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
    while(rButton)
    {
        rButton->Update(tMouseX, tMouseY);
        rButton = (FlexButton *)mButtonList->AutoIterate();
    }

    //--If the mouse position changed, run an update for all buttons.
    if(tMouseX != mPreviousMouseX || tMouseY != mPreviousMouseY)
    {
        //--Button highlighting. Query each button, set highlighted for the FIRST one found. Also
        //  reset all highlighting cases.
        int i = 0;
        bool tCanHighlight = true;
        rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Reset.
            bool tPreviousHighlight = rButton->IsHighlighted();
            rButton->SetHighlight(false);

            //--If changing language, disallow hitting any button except quit.
            if(mStartingLanguage != mSelectedLanguage && i < MONOTITLE_BUTTONS_REMOVE_WHEN_LANGUAGE_CHANGE)
            {

            }
            //--If allowed, check for highlighting. Internal highlighting is disabled but
            //  we still want to play a sound.
            else if(tCanHighlight && rButton->IsMouseOverMe(tMouseX, tMouseY))
            {
                //--Flags.
                rButton->SetHighlight(true);
                tCanHighlight = false;
                mSelectedOption = i;

                //--Refresh the description if this button was highlighted for the first time.
                if(rLastHighlightedBtn != rButton) RefreshDescriptionFrom(rButton);

                //--SFX.
                if(!tPreviousHighlight && (int)Global::Shared()->gTicksElapsed >= xNoHighlightTimer) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--Next.
            i ++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }

        //--Store the mouse positions.
        mPreviousMouseX = tMouseX;
        mPreviousMouseY = tMouseY;
    }

    ///--[Highlight Changing]
    //--Pressing Left activates the language menu.
    if(rControlManager->IsFirstPress("Left"))
    {
        mIsLanguageMenuActive = true;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Decrement selection.
    if(rControlManager->IsFirstPress("Up") || rControlManager->IsFirstPress("PageUp"))
    {
        //--Set, clamp.
        mSelectedOption --;
        if(mSelectedOption < 0) mSelectedOption = 0;

        //--Button highlighting. All buttons are flagged as unhighlighted except the selected one.
        int i = 0;
        FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Set.
            rButton->SetHighlight(mSelectedOption == i);

            //--Next.
            i ++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Increment selection.
    if(rControlManager->IsFirstPress("Down") || rControlManager->IsFirstPress("PageDn"))
    {
        //--Set, clamp.
        mSelectedOption ++;
        if(mSelectedOption >= mButtonList->GetListSize()) mSelectedOption = mButtonList->GetListSize() - 1;
        if(mSelectedOption < 0) mSelectedOption = 0;

        //--Button highlighting. All buttons are flagged as unhighlighted except the selected one.
        int i = 0;
        FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Set.
            rButton->SetHighlight(mSelectedOption == i);

            //--Next.
            i ++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Pressing Enter will activate the selected button, if it exists.
    if(rControlManager->IsFirstPress("Enter"))
    {
        FlexButton *rSelectedButton = (FlexButton *)mButtonList->GetElementBySlot(mSelectedOption);
        if(rSelectedButton) rSelectedButton->Execute();
        mSelectedOption = -1;
    }

    ///--[Left-Click]
    //--Left-clicking case. Only the first button found that matches is executed.
    if(rControlManager->IsFirstPress("MouseLft") && !mClearedSelfThisTick)
    {
        //--Fixed-position translation button. This is not relative to the menu.
        if(IsPointWithin(tMouseX+(int)mCoordinates.mLft, tMouseY+(int)mCoordinates.mTop, 7.0f, 29.0f, 127.0f, 89.0f))
        {
            mIsLanguageMenuActive = true;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Loop.
        int i = 0;
        FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Disallow clicking buttons when changing languages, except quit.
            if(mStartingLanguage != mSelectedLanguage && i < MONOTITLE_BUTTONS_REMOVE_WHEN_LANGUAGE_CHANGE)
            {

            }
            //--Button selected.
            else if(rButton->IsMouseOverMe(tMouseX, tMouseY))
            {
                xNoHighlightTimer = Global::Shared()->gTicksElapsed + FM_SILENCE_TICKS;
                AudioManager::Fetch()->PlaySound("Menu|Select");
                rButton->Execute();
                mButtonList->PopIterator();
                break;
            }

            //--Next.
            i ++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }
    }
    ///--[Left-Release]
    else if(rControlManager->IsFirstRelease("MouseLft"))
    {
        mIsClickingScrollbar = false;
    }

    ///--[Closing]
    //--Close.
    if(rControlManager->IsFirstPress("Escape"))
    {
        //SetClosingFlag(true);
    }

    ///--[Cleanup]
    //--If a clearing action occurred sometime during this tick, swap out the button list.
    if(mClearedSelfThisTick)
    {
        delete mButtonList;
        mButtonList = mButtonStorage;
        mButtonStorage = NULL;
    }
}
void MonocerosTitle::UpdateLoadingMode()
{
    ///--[Documentation and Setup]
    //--Fast-access Pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[String Entry Form]
    //--When entering strings, pass the update down.
    if(mIsEnteringString)
    {
        //--Run the update.
        mStringEntryForm->Update();

        //--If the entry form flagged complete, extract the string and write the save.
        if(mStringEntryForm->IsComplete())
        {
            //--Flag.
            mIsEnteringString = false;

            //--Get the loading pack in question. If it doesn't exist, stop.
            LoadingPack *rPack = (LoadingPack *)mLoadingPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(!rPack) return;

            //--Get the note from the string entry form. It can legally be empty, in which case
            //  we generate a standard note.
            const char *rEnteredString = mStringEntryForm->GetString();

            //--Generate a note if the player left it empty.
            bool tDeallocateNote = false;
            char *rNewNote = SaveManager::GenerateNote(rEnteredString, tDeallocateNote);

            //--Run the SaveManager's subroutine to switch the note out.
            SaveManager::ModifyFileNote(rNewNote, rPack->mFilePath);

            //--Synchronize the note with the loading pacakge.
            ResetString(rPack->mFileName, rNewNote);

            //--If the note was generated, deallocate it here.
            if(tDeallocateNote) free(rNewNote);
        }
        else if(mStringEntryForm->IsCancelled())
        {
            mIsEnteringString = false;
        }
        return;
    }

    ///--[Hotkeys]
    //--Pressing escape deactivates loading mode.
    if(rControlManager->IsFirstPress("Escape"))
    {
        DeactivateLoadingMode();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Setup]
    //--Temporary variables.
    int tOldCursor = mLoadingCursor;

    //--Determine which loading list is in use.
    StarLinkedList *rPackList = mLoadingPackList;
    if(mLoadingShowAutosaves) rPackList = mAutosavePackList;

    ///--[Mouse Movement]
    //--Get the mouse position.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--If the mouse moved, change the highlighted object.
    if(tMouseX != mPreviousMouseX || tMouseY != mPreviousMouseY)
    {
        //--Reset the loading cursor.
        mLoadingCursor = -1;

        //--Check the New Game button.
        if(IsPointWithin(tMouseX, tMouseY, 127.0f, 127.0f, 1163.0f, 226.0f))
        {
            mLoadingCursor = FM_LOAD_NEWGAME;
        }
        //--Check the Back button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mBackBtn))
        {
            mLoadingCursor = FM_LOAD_BACK;
        }
        //--Check the Controls button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mControlsBtn))
        {
            mLoadingCursor = FM_LOAD_CONTROLS;
        }
        //--Scroll up.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mScrollUp))
        {
            mLoadingCursor = FM_LOAD_SCROLLUP;
        }
        //--Scroll down.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mScrollDn))
        {
            mLoadingCursor = FM_LOAD_SCROLLDN;
        }
        //--Toggle autosave and normal saves.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mToggleAutosave))
        {
            mLoadingCursor = FM_LOAD_TOGGLEAUTOSAVE;
        }
        //--Otherwise, check the loading and new game buttons.
        else
        {
            int cEntriesPerPage = 3;
            float cSizePerBox = 137.0f;
            float cBoxSpacing =  10.0f;
            for(int i = 0; i < cEntriesPerPage; i ++)
            {
                //--Compute position.
                float cTop = 272.0f + (cSizePerBox * i) + (cBoxSpacing * i);
                if(IsPointWithin(tMouseX, tMouseY, 68.0f, cTop, 1250.0f, cTop + cSizePerBox))
                {
                    mLoadingCursor = i + 1;
                }
            }
        }

        //--During a scrollbar click and drag, handle the movement.
        if(mIsClickingScrollbar)
        {
            //--Constants.
            TwoDimensionReal cBoxDimensions;
            int cEntriesPerPage = MONO_FILES_PER_PAGE;
            //float cSizePerBox = 137.0f;
            //float cBoxSpacing =  10.0f;
            cBoxDimensions.SetWH(1311.0f, 105.0f, 33.0f, 642.0f);

            //--Check the difference between the bar's top and the mouse current position.
            float cAmountPerMove = (1.0f / (float)(rPackList->GetListSize() - cEntriesPerPage) * cBoxDimensions.GetHeight()) * 0.8f;
            if(tMouseY - mScrollbarClickStartY < -cAmountPerMove && mLoadingOffset > 0)
            {
                mLoadingOffset --;
                mScrollbarClickStartY = mScrollbarClickStartY - cAmountPerMove;
            }
            else if(tMouseY - mScrollbarClickStartY > cAmountPerMove)
            {
                mLoadingOffset ++;
                if(mLoadingOffset > rPackList->GetListSize() - MONO_FILES_PER_PAGE)
                {
                    mLoadingOffset --;
                }
                else
                {
                    mScrollbarClickStartY = mScrollbarClickStartY + cAmountPerMove;
                }
            }
        }

        //--Save the mouse position.
        mPreviousMouseX = tMouseX;
        mPreviousMouseY = tMouseY;
    }

    //--If the previous mouse Z was -1, store it immediately.
    if(mPreviousMouseZ == -1) mPreviousMouseZ = tMouseZ;

    ///--[Mouse Scrollwheel]
    //--Interacts with the scrollbar.
    if(tMouseZ > mPreviousMouseZ)
    {
        mLoadingOffset --;
        if(mLoadingOffset < 0) mLoadingOffset = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    else if(tMouseZ < mPreviousMouseZ)
    {
        mLoadingOffset ++;
        if(mLoadingOffset > rPackList->GetListSize() - MONO_FILES_PER_PAGE)  mLoadingOffset --;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Hotkeys]
    //--Save selection up.
    if(rControlManager->IsFirstPress("Up") || rControlManager->IsFirstPress("PageUp"))
    {
        //--Offset moves first.
        if(mLoadingCursor > 1)
        {
            mLoadingCursor --;
        }
        //--Move the loading offset and clamp.
        else if(mLoadingOffset > 0)
        {
            mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Otherwise, move the cursor onto 0: New Game.
        else
        {
            mLoadingCursor = 0;
        }
    }
    //--Save selection down.
    if(rControlManager->IsFirstPress("Down") || rControlManager->IsFirstPress("PageDn"))
    {
        //--Move the cursor and clamp.
        mLoadingCursor ++;

        //--If the loading offset pushes the cursor offscreen, cap it.
        if(mLoadingCursor > MONO_FILES_PER_PAGE)
        {
            mLoadingOffset ++;
            if(mLoadingOffset > rPackList->GetListSize() - MONO_FILES_PER_PAGE) mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            mLoadingCursor = MONO_FILES_PER_PAGE;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Save selection, jump to top.
    if(rControlManager->IsFirstPress("GUI|Home"))
    {
        mLoadingCursor = 0;
        mLoadingOffset = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Save selection, jump to bottom.
    if(rControlManager->IsFirstPress("GUI|End"))
    {
        mLoadingCursor = MONO_FILES_PER_PAGE;
        mLoadingOffset = rPackList->GetListSize() - MONO_FILES_PER_PAGE;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Switch to autosaves mode.
    if(rControlManager->IsFirstPress("Run"))
    {
        mLoadingShowAutosaves = !mLoadingShowAutosaves;
        mLoadingOffset = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Store the mouse Z value.
    mPreviousMouseZ = tMouseZ;

    ///--[SFX]
    //--If the cursor changed, play a sound effect.
    if(tOldCursor != mLoadingCursor && mLoadingCursor != -1)
    {
        //--If negative, play a sound.
        if(mLoadingCursor == 0 || mLoadingCursor < -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If a package exists, play the sound. If not, don't.
        else
        {
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(rPack) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Selection via Mouse]
    //--Clicking.
    if(rControlManager->IsFirstPress("MouseLft") || rControlManager->IsFirstPress("Enter"))
    {
        //--Scroll up.
        if(mLoadingCursor == FM_LOAD_SCROLLUP)
        {
            mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Scroll down.
        else if(mLoadingCursor == FM_LOAD_SCROLLDN)
        {
            mLoadingOffset ++;
            if(mLoadingOffset > rPackList->GetListSize() - MONO_FILES_PER_PAGE) mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If the cursor was on -2, go back.
        else if(mLoadingCursor == FM_LOAD_BACK)
        {
            DeactivateLoadingMode();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If the cursor was on -3, go to the controls menu.
        else if(mLoadingCursor == FM_LOAD_CONTROLS)
        {
            //ActivateRebindingMode();
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Toggle between autosaves and normal saves.
        else if(mLoadingCursor == FM_LOAD_TOGGLEAUTOSAVE)
        {
            mLoadingShowAutosaves = !mLoadingShowAutosaves;
            mLoadingOffset = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If the cursor was on -1, check for a scrollbar click.
        else if(mLoadingCursor == -1)
        {
            //--Check for a scrollbar click.
            //float cSizePerBox = 137.0f;
            //float cBoxSpacing =  10.0f;
            int cEntriesPerPage = MONO_FILES_PER_PAGE;
            if(rPackList->GetListSize() - cEntriesPerPage > 0 && IsPointWithin(tMouseX, tMouseY, 1311.0f, 105.0f, 1311.0f + 33.0f, 105.0f + 642.0f))
            {
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                mIsClickingScrollbar = true;
                mScrollbarClickStartY = tMouseY;
            }
        }
        //--If the cursor was on 0, start a new game.
        else if(mLoadingCursor == FM_LOAD_NEWGAME)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Defaults for the SaveManager.
            SaveManager::Fetch()->SetSavegameName("New Game");

            //--Run this script, it does the rest.
            LuaManager::Fetch()->ExecuteLuaFile("Data/Scripts/MainMenu/129 Launch Monoceros.lua");
        }
        //--Otherwise, load the game under the cursor.
        else
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Make sure Adventure Mode actually has an operable path.
            const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");

            //--Get the pack.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(!rPack) { return; }

            //--Clear active mods and reset which ones are active based on the loading package.
            OptionsManager *rOptionsManager = OptionsManager::Fetch();
            rOptionsManager->ClearModActivity();
            void *rModPtr = rPack->mActiveMods->PushIterator();
            while(rModPtr)
            {
                rOptionsManager->SetModActive(rPack->mActiveMods->GetIteratorName());
                rModPtr = rPack->mActiveMods->AutoIterate();
            }

            //--Pass these for the SaveManager.
            SaveManager::Fetch()->SetSavegameName(rPack->mFileName);
            SaveManager::Fetch()->SetSavegamePath(rPack->mFilePath);

            //--Store the path.
            char *tPath = InitializeString(rPack->mFilePath);

            //--Store the character's name as a pointer.
            char *tCharName = InitializeString(rPack->mPartyNames[0]);

            //--Clear. All data is now wiped, don't use the loading pack anymore!
            DeactivateLoadingMode();

            //--Execute.
            char *tLaunchPath = InitializeString("%s/ZLaunch.lua", rAdventurePath);
            LuaManager::Fetch()->ExecuteLuaFile(tLaunchPath, 2, "S", tPath, "S", tCharName);

            //--Clean.
            free(tPath);
            free(tCharName);
            free(tLaunchPath);
        }
    }
    ///--[Right Mouse]
    //--If over a loading pack, changes the note on that pack.
    else if(rControlManager->IsFirstPress("MouseRgt"))
    {
        //--If the cursor was 0 or lower, ignore it.
        if(mLoadingCursor <= 0)
        {
        }
        //--Otherwise, modify the note. This is done by bringing up the StringEntryForm.
        else
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Set flags.
            mIsEnteringString = true;
            mFileStringEntering = mLoadingCursor + mLoadingOffset;
            mStringEntryForm->SetCompleteFlag(false);

            //--Get the string from the file in question.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(!rPack) { return; }
            mStringEntryForm->SetString(rPack->mFileName);
        }
    }

    ///--[Mouse Release]
    //--Cancel scrollbar clicks.
    if(rControlManager->IsFirstRelease("MouseLft"))
    {
        mIsClickingScrollbar = false;
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
#define INTRO_STAGE_0 ((int)(1.0f * 60.0f))
void MonocerosTitle::Render()
{
    ///--[Documentation and Setup]
    //--Make sure the images resolved correctly.
    if(!Images.mIsReady || !MonoImg.mIsReady) return;

    ///--[Version Info]
    //--Render the version number in the top left.
    GLOBAL *rGlobal = Global::Shared();
    if(rGlobal->gBitmapFont)
    {
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, 1.0f, rGlobal->gVersionString);
    }

    ///--[Mode Switching]
    //--If in Loading/Rebinding mode, do that instead.
    if(mSplashState > FM_SPLASH_STATE_NONE) { RenderSplashMode();    return; }
    if(mIsRebindingMode)                    { RenderRebindingMode(); return; }
    if(mIsLoadingMode)                      { RenderLoadingMode();   return; }
    if(mIsShowingModLoadOrder)              { RenderModMode();       return; }

    ///--[Background]
    MonoImg.Data.rBacking->Draw();
    MonoImg.Data.rNotification->Draw();

    ///--[Overlay Fade]
    //--Handles intro fade.
    if(mIsIntroMode)
    {
        //--First 2 seconds: Full blackout.
        if(mIntroTimer < INTRO_STAGE_0)
        {
            StarBitmap::DrawFullBlack(1.0f);
        }
        //--Rest of intro, dragging fade in.
        else
        {
            //--Percentage.
            float cRgt = VIRTUAL_CANVAS_X;
            float cMid = VIRTUAL_CANVAS_X * 0.50f;
            float cPct = EasingFunction::QuadraticIn(mIntroTimer - INTRO_STAGE_0, INTRO_TICKS - INTRO_STAGE_0) * 1.7f;

            //--Resolve positions for the chevron.
            float cChevTop = VIRTUAL_CANVAS_Y * (1.0f - cPct);
            float cChevBot = (VIRTUAL_CANVAS_Y * (1.0f - cPct)) + 300.0f;
            float cFadeOff = 250.0f;

            //--Rendering setup.
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
            glColor3f(0.0f, 0.0f, 0.0f);

            //--Black box above the chevron.
            if(cChevTop > 0.0f)
            {
                glVertex2f(0.0f, 0.0f);
                glVertex2f(cRgt, 0.0f);
                glVertex2f(cRgt, cChevTop);
                glVertex2f(0.0f, cChevTop);
            }

            //--Chevron.
            glVertex2f(0.0f, cChevTop);
            glVertex2f(cRgt, cChevTop);
            glVertex2f(cMid, cChevBot);
            glVertex2f(cMid, cChevBot);

            //--Fade section left.
            glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
            glVertex2f(0.0f, cChevTop);
            glVertex2f(cMid, cChevBot);
            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
            glVertex2f(cMid, cChevBot+cFadeOff);
            glVertex2f(0.0f, cChevTop+cFadeOff);

            //--Fade section right.
            glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
            glVertex2f(cMid, cChevBot);
            glVertex2f(cRgt, cChevTop);
            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
            glVertex2f(cRgt, cChevTop+cFadeOff);
            glVertex2f(cMid, cChevBot+cFadeOff);

            //--Clean.
            glEnd();
            glEnable(GL_TEXTURE_2D);
            StarlightColor::ClearMixer();
        }
    }

    ///--[Version]
    if(xVersionString && !mIsShowingOptions)
    {
        MonoImg.Data.rFontSaveStat->DrawText(7.0f, 0.0f, 0, 1.0f, xVersionString);
    }

    ///--[Language Indicator]
    //--Get the selected language pack. If it exists, render the flag in the top left under the version.
    LanguagePack *rCurrentPack = (LanguagePack *)mLanguageList->GetElementBySlot(mSelectedLanguage);
    if(rCurrentPack && !mIsShowingOptions)
    {
        //--Make sure the image exists.
        if(rCurrentPack->mFlag)
        {
            rCurrentPack->mFlag->Draw(7.0f, 29.0f);
        }
    }

    ///--[Title Overlay and Options]
    if(mTitleOverTimer > 0)
    {
        //--Fade percent.
        float cPct = EasingFunction::QuadraticIn(mTitleOverTimer, TITLE_OVER_TICKS);

        //--Overlay offset.
        float cPosPct = (float)mTitleOscillateTimer / (float)MONOTITLE_OSCILLATE_TICKS;
        float cPosOff = sinf(cPosPct * 3.1315926f * 2.0f) * MONOTITLE_OSCILLATE_DISTANCE;

        //--Render overlay.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);
        glTranslatef(0.0f, cPosOff, 0.0f);
        MonoImg.Data.rOverlayTitle->Draw();
        glTranslatef(0.0f, -cPosOff, 0.0f);

        ///--[Buttons]
        //--Render the buttons, which will also have an internal check for class/updated UI.
        glTranslatef(mCoordinates.mLft, mCoordinates.mTop, 0.0f);
        int i = 0;
        FlexButton *rButton = (FlexButton *)mButtonList->PushIterator();
        while(rButton)
        {
            //--Bypass rendering before option 4 if the translations don't match.
            if(mStartingLanguage != mSelectedLanguage && i < MONOTITLE_BUTTONS_REMOVE_WHEN_LANGUAGE_CHANGE && rCurrentPack)
            {
                i ++;
                rButton = (FlexButton *)mButtonList->AutoIterate();
                continue;
            }

            if(i == mSelectedOption)
            {
                MonoImg.Data.rOverlaySelection->Draw(-43.0f, rButton->GetTop() + 9.0f);
            }
            glColor4f(0.9f, 0.85f, 0.6f, cPct);
            rButton->Render();
            i++;
            rButton = (FlexButton *)mButtonList->AutoIterate();
        }
        glTranslatef(-mCoordinates.mLft, -mCoordinates.mTop, 0.0f);

        //--Clean.
        StarlightColor::ClearMixer();

        //--Render this overlay for whatever language is active if flagged.
        if(mStartingLanguage != mSelectedLanguage && rCurrentPack)
        {
            if(rCurrentPack->mRestartProgram) rCurrentPack->mRestartProgram->Draw();
        }

        ///--[Overlay Shade]
        float cShadePct = (float)mTitleShadeTimer / (float)MONOTITLE_SHADE_TICKS;
        float cShadeOff = fabsf(sinf(cShadePct * 3.1415926f * 2.0f)) * 0.25f;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cShadeOff);
        MonoImg.Data.rOverlayShading->Draw();
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);
    }

    ///--[Options Menu]
    mOptionsMenu->Render();

    ///--[Language Interface]
    //--Subcall, renders over everything else.
    RenderLanguageMode();
}

///=================================== Drawing: Loading Mode ======================================
void MonocerosTitle::RenderLoadingMode()
{
    ///--[Documentation]
    //--Renders Loading Mode, assuming there is at least one file to load.
    if(!Images.mIsReady || !MonoImg.mIsReady) return;

    ///--[String Entry Form]
    //--When entering strings, pass the update down.
    if(mIsEnteringString)
    {
        MonoImg.Data.rBacking->Draw();
        mStringEntryForm->Render();
        return;
    }

    //--Color setup.
    //StarlightColor cBackingColorGrey = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.85f);
    //StarlightColor cBackingColorBlue = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.4f, 0.85f);
    //StarlightColor *rUseColor = &cBackingColorGrey;

    //--New game button sizing.
    TwoDimensionReal cBoxDimensions;
    float cSizePerBox = 137.0f;
    float cBoxSpacing =  10.0f;

    //--Constants.
    int cEntriesPerPage = MONO_FILES_PER_PAGE;
    float cTextSize = 1.0f;

    ///--[Background]
    MonoImg.Data.rBacking->Draw();

    ///--[Header]
    //--Backing.
    MonoImg.Data.rOverlaySaveShade->Draw();

    //--Render some instructions.
    mMonoTitle.SetAsMixer();
    MonoImg.Data.rFontDoubleHeader->DrawText((VIRTUAL_CANVAS_X * 0.5f), 34.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSelectASaveFile);
    StarlightColor::ClearMixer();

    ///--[New Game Button]
    //--Always present, appears at the top of the screen.
    if(mLoadingCursor == 0)
    {
        MonoImg.Data.rFrameNewGameSelect->Draw();
    }
    else
    {
        MonoImg.Data.rFrameNewGame->Draw();
    }

    //--New Game text.
    mMonoParagraph.SetAsMixer();
    MonoImg.Data.rFontMenu->DrawTextArgs(553.0f, 150.0f, 0, 1.0f, xrStringData->str.mTextNewGame);
    StarlightColor::ClearMixer();

    ///--[Scrollbar]
    //--Determine which list to use.
    StarLinkedList *rPackList = mLoadingPackList;
    if(mLoadingShowAutosaves) rPackList = mAutosavePackList;

    //--Shows a scrollbar if there are more than cEntriesPerPage total.
    if(rPackList->GetListSize() > cEntriesPerPage)
    {
        StarUIPiece::StandardRenderScrollbar(mLoadingOffset, cEntriesPerPage, rPackList->GetListSize(), 105.0f, 642.0f, false, MonoImg.Data.rScrollbarFront, MonoImg.Data.rScrollbarBack);
        //RenderScrollbar(mLoadingOffset, cEntriesPerPage, rPackList->GetListSize(), 105.0f, 642.0f, true, MonoImg.Data.rScrollbarBack, MonoImg.Data.rScrollbarFront);
    }

    ///--[Loading Packs]
    //--Render the save game loading packs.
    for(int i = 0; i < cEntriesPerPage; i ++)
    {
        //--Get the loading pack in question.
        LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(i + mLoadingOffset);
        if(!rPack) continue;

        //--Compute position.
        float cTop = MonoImg.Data.rFrameSaveCellSelect->GetYOffset() + (cSizePerBox * i) + (cBoxSpacing * i);
        cBoxDimensions.Set(69.0f, cTop, 1249.0f, cTop + cSizePerBox);

        //--Render the border card.
        if(mLoadingCursor == 1 + i)
        {
            MonoImg.Data.rFrameSaveCellSelect->Draw(0.0f, cTop - MonoImg.Data.rFrameSaveCellSelect->GetYOffset());
        }
        else
        {
            MonoImg.Data.rFrameSaveCell->Draw(0.0f, cTop - MonoImg.Data.rFrameSaveCellSelect->GetYOffset());
        }

        //--Render the name of the save file. This is not its path, and it can be renamed!
        MonoImg.Data.rFontSaveStat->DrawTextArgs(cBoxDimensions.mLft + 61.0f, cBoxDimensions.mTop + 6.0f, 0, cTextSize, "File %s: %s", rPackList->GetNameOfElementBySlot(i + mLoadingOffset), rPack->mFileName);

        //--Timestamp.
        float tTimestampLen = MonoImg.Data.rFontSaveStat->GetTextWidth(rPack->mTimestamp) * cTextSize;
        MonoImg.Data.rFontSaveStat->DrawText(cBoxDimensions.mRgt - 14.0f - tTimestampLen, cBoxDimensions.mTop + 8.0f, 0, cTextSize, rPack->mTimestamp);

        //--Render what map the player was on.
        MonoImg.Data.rFontSaveStat->DrawText(cBoxDimensions.mLft + 61.0f, cBoxDimensions.mBot - 8.0f - (MonoImg.Data.rFontSaveStat->GetTextHeight() * cTextSize), 0, cTextSize, rPack->mMapLocation);

        //--Render the characters in it.
        float cSpriteScale = 2.0f;
        float cPortraitScale = 0.355932f;
        for(int p = 0; p < 4; p ++)
        {
            //--Compute positions.
            float cLft = cBoxDimensions.mLft + 86.0f + (200.0f * p);
            float cTop = cBoxDimensions.mTop + 38.0f;

            //--Check if the savefile in question has the hard path in it:
            if(!strncasecmp(rPack->mPartyNames[p], "HardPath|", 9))
            {
                //--Offset position.
                cLft = cLft - 90.0f;
                cTop = cTop - 26.0f;

                //--Subdivide the string. The second block is the character's name. The third is the DLPath.
                StarLinkedList *tStringList = Subdivide::SubdivideStringToList(rPack->mPartyNames[p], "|");

                //--Image.
                const char *rCharName = (const char *)tStringList->GetElementBySlot(1);
                const char *rDLPathString = (const char *)tStringList->GetElementBySlot(2);
                rPack->rRenderImg[p] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(rDLPathString);

                //--Render portrait, needs scaling.
                if(rPack->rRenderImg[p])
                {
                    glTranslatef(cLft, cTop, 0.0f);
                    glScalef(cPortraitScale, cPortraitScale, 1.0f);
                    rPack->rRenderImg[p]->Draw();
                    glScalef(1.0f / cPortraitScale, 1.0f / cPortraitScale, 1.0f);
                    glTranslatef(-cLft, -cTop, 0.0f);
                }

                //--Name.
                Images.Data.rUIFont->DrawText    (cLft + 116.0f, cTop + 28.0f, 0, 1.0f, rCharName);
                Images.Data.rUIFont->DrawTextArgs(cLft + 116.0f, cTop + 48.0f, 0, 1.0f, "Lv. %i", rPack->mPartyLevels[p] + 1);

                //--Clean.
                delete tStringList;
            }
            //--Render sprites.
            else
            {
                //--Render. We need to scale this up.
                if(rPack->rRenderImg[p])
                {
                    glTranslatef(cLft, cTop, 0.0f);
                    glScalef(cSpriteScale, cSpriteScale, 1.0f);
                    rPack->rRenderImg[p]->Draw();
                    glScalef(1.0f / cSpriteScale, 1.0f / cSpriteScale, 1.0f);
                    glTranslatef(-cLft, -cTop, 0.0f);
                }

                //--Get the name from the buffer.
                int tCutoff = -1;
                for(int x = (int)strlen(rPack->mPartyNames[p]); x >= 0; x --)
                {
                    if(rPack->mPartyNames[p][x] == '_')
                    {
                        tCutoff = x;
                        break;
                    }
                }

                //--Error.
                if(tCutoff == -1) continue;

                //--Render.
                rPack->mPartyNames[p][tCutoff] = '\0';
                MonoImg.Data.rFontSaveStat->DrawText    (cLft + 109.0f, cTop + 28.0f, 0, 1.0f, rPack->mPartyNames[p]);
                MonoImg.Data.rFontSaveStat->DrawTextArgs(cLft + 109.0f, cTop + 48.0f, 0, 1.0f, "Lv. %i", rPack->mPartyLevels[p] + 1);
                rPack->mPartyNames[p][tCutoff] = '_';
            }
        }
    }

    //--Instruction String.
    float cXPos = VIRTUAL_CANVAS_X * 0.20f - 197.0f;
    float cYPos = (VIRTUAL_CANVAS_Y * 0.30f) + (cSizePerBox * cEntriesPerPage) + (cBoxSpacing * cEntriesPerPage) + 50.0f;
    MonoImg.Data.rFontSaveStat->DrawText(cXPos, cYPos, 0, 1.0f, xrStringData->str.mTextRightClickAFile);

    ///--[Back Button]
    MonoImg.Data.rFrameBackButton->Draw();
    MonoImg.Data.rFontMenu->DrawText(mBackBtn.mLft+22.0f, mBackBtn.mTop-2.0f, 0, 0.7f, xrStringData->str.mTextBack);

    ///--[Controls Button]
    //rUseColor = &cBackingColorGrey;
    //if(mLoadingCursor == FM_LOAD_CONTROLS) rUseColor = &cBackingColorBlue;
    //VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mControlsBtn, 0x01FF, *rUseColor);
    //MonoImg.Data.rFontSaveStat->DrawText(mControlsBtn.mLft + 12.0f + 41.0f, mControlsBtn.mTop + 12.0f, 0, 1.0f, "Controls");

    ///--[Toggle Autosave Button]
    MonoImg.Data.rFrameAutosavesButton->Draw();
    if(!mLoadingShowAutosaves)
    {
        MonoImg.Data.rFontMenu->DrawTextFixed(mToggleAutosave.mLft+5.0f, mToggleAutosave.mTop-2.0f, 123.0f, 0, xrStringData->str.mTextAutoSaves);
    }
    else
    {
        MonoImg.Data.rFontMenu->DrawTextFixed(mToggleAutosave.mLft+5.0f, mToggleAutosave.mTop-2.0f, 123.0f, 0, xrStringData->str.mTextNormalSaves);
    }

    //--Help text.
    mToggleAutosaveKey->DrawText(mToggleAutosave.mLft - 5.0f, mToggleAutosave.mTop, SUGARFONT_RIGHTALIGN_X, 1.0f, MonoImg.Data.rFontSaveStat);
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
FlexMenu *MonocerosTitle::GenerateMonocerosTitle()
{
    return new MonocerosTitle();
}

///========================================= Lua Hooking ==========================================
void MonocerosTitle::HookToLuaState(lua_State *pLuaState)
{
    /* MT_SetProperty("Set As Title Generate") (Static)
       MT_SetProperty("Set As String Entry Generate") (Static)
       MT_SetProperty("Set Version", sVersionString) (Static)
       MT_SetProperty("Use Developer Translation") (Static)
       MT_SetProperty("Show Options")
       Sets the requested property. */
    lua_register(pLuaState, "MT_SetProperty", &Hook_MT_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_MT_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //MT_SetProperty("Set As Title Generate") (Static)
    //MT_SetProperty("Set As String Entry Generate") (Static)
    //MT_SetProperty("Set Version", sVersionString) (Static)
    //MT_SetProperty("Use Developer Translation") (Static)
    //MT_SetProperty("Show Options")

    //--[Translations]
    //MT_SetProperty("Register Translation", sLanguage, sExtension, sFolderPath, sFlagFilePath, sRestartPath)
    //MT_SetProperty("Starting Language", sLanguage)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("MT_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Changes static class generation to generate a MonocerosTitle instead of the base FlexMenu.
    if(!strcasecmp(rSwitchType, "Set As Title Generate"))
    {
        Global::Shared()->rCreateTitleMenu = &MonocerosTitle::GenerateMonocerosTitle;
        return 0;
    }
    //--Changes static class generation to generate a MonoStringEntry instead of a String Entry.
    else if(!strcasecmp(rSwitchType, "Set As String Entry Generate"))
    {
        StringEntry::rGenerateStringEntry = &MonoStringEntry::GenerateStringEntry;
        return 0;
    }
    //--Changes static class generation to generate a MonoStringEntry instead of a String Entry.
    else if(!strcasecmp(rSwitchType, "Set Version") && tArgs == 2)
    {
        ResetString(MonocerosTitle::xVersionString, lua_tostring(L, 2));
        return 0;
    }
    //--Toggles on developer mode which assumes the folder structure has Monoceros in its own directory apart from the engine.
    else if(!strcasecmp(rSwitchType, "Use Developer Translation") && tArgs >= 1)
    {
        MonocerosTitle::xIsDeveloperMode = true;
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_MONOCEROSTITLE)) return LuaTypeError("MT_SetProperty", L);
    MonocerosTitle *rTitleObject = (MonocerosTitle *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Shows the options menu.
    if(!strcasecmp(rSwitchType, "Show Options") && tArgs == 1)
    {
        rTitleObject->ShowOptions();
    }
    else if(!strcasecmp(rSwitchType, "Show Mod Load Order") && tArgs == 1)
    {
        rTitleObject->ShowModLoadOrder();
    }
    ///--[Translations]
    //--Registers a new language pack.
    else if(!strcasecmp(rSwitchType, "Register Translation") && tArgs >= 6)
    {
        rTitleObject->RegisterLanguage(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6));
    }
    //--Sets which language the GUI object booted in. Only called once.
    else if(!strcasecmp(rSwitchType, "Starting Language") && tArgs == 2)
    {
        rTitleObject->SetStartingLanguage(lua_tostring(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("MT_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
