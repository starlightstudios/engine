-- |[Save Back to Main Menu]|
--Closes the existing menu and returns to the main menu. Also saves out the config file.
MapM_PushMenuStackHead()

-- |[Functions]|
--Default boolean resolver. Handy!
local fnHandleBoolean = function(sButtonName, sOptionName)
	
    --Button does not exist, do nothing.
    if(FlexMenu_DoesButtonExist(sButtonName) == false) then return end
    
    --Resolve and update.
	FlexMenu_PushButton(sButtonName)
		local iCurrentMipmapFlag = FlexButton_GetProperty("Current Option")
		if(iCurrentMipmapFlag ~= nil and iCurrentMipmapFlag == 0) then
			OM_SetOption(sOptionName, true)
		elseif(iCurrentMipmapFlag ~= nil and iCurrentMipmapFlag == 1) then
			OM_SetOption(sOptionName, false)
		end
	DL_PopActiveObject()
	
end

-- |[Resolve]|
--Figure out which options the buttons were pointing to, and upload those values to the Options Manager.
local iResM = 0     --Unused
local iResX = 1024
local iResY = 768
local iResR = 30    --Unused
FlexMenu_PushButton("Resolution")
	local iCurrentResolution = FlexButton_GetProperty("Current Option")
	if(iCurrentResolution ~= nil) then
		iResM, iResX, iResY, iResR = DM_GetDisplayModeInfo("Data", iCurrentResolution)
	end
DL_PopActiveObject()

--Mandated monitor.
FlexMenu_PushButton("Mandate Monitor")
	local iCurrentMonitor = FlexButton_GetProperty("Current Option")
    OM_SetOption("MandateMonitor", iCurrentMonitor-1) --The 0th option is the value -1.
DL_PopActiveObject()

--Store.
OM_SetOption("WinSizeX", iResX)
OM_SetOption("WinSizeY", iResY)

--Flags.
fnHandleBoolean("Disallow Mipmapping", "DisallowMipmapping")
fnHandleBoolean("Show Mouse Debug Cursor", "ShowMouseDebug")
fnHandleBoolean("Use Small Console Font", "UseSmallConsoleFont")
fnHandleBoolean("Use Low Res for Adventure Mode", "LowResAdventureMode")
fnHandleBoolean("Disallow Perlin Noise Engine", "DisallowPerlinNoise")
fnHandleBoolean("Use RAM Loading", "UseRAMLoading")

-- |[Write]|
--Save the data to the hard drive.
OM_WriteConfigFiles()

-- |[Reset]|
--Clear the old menu, and return to the main menu.
FlexMenu_Clear()

--Run the setup script. This is elsewhere.
LM_ExecuteScript("Data/Scripts/MainMenu/000 PopulateMainMenu.lua")

--Clean Up.
DL_PopActiveObject()