//--Base
#include "StarLinkedList.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Definitions.h"

//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
StarLinkedList::StarLinkedList(bool pHandleDeallocate)
{
    //--List Handling
    mListHead = NULL;
    mListTail = NULL;
    mListCurrent = NULL;

    //--List Properties
    mListSize = 0;
    mHandlesDeallocation = pHandleDeallocate;
    rStringCompareFuncPtr = &strcmp;

    //--Iterator Stack
    mStackSize = 0;
    mIteratorStack = NULL;

    //--Debug
    mDebugFlag = false;
}
StarLinkedList::~StarLinkedList()
{
    ClearList();
}
void StarLinkedList::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((StarLinkedList *)pPtr);
}

///===================================== Property Queries =========================================
int StarLinkedList::GetListSize()
{
    return mListSize;
}
void *StarLinkedList::GetHead()
{
    if(!mListHead) return NULL;
    return mListHead->rData;
}
void *StarLinkedList::GetTail()
{
    if(!mListTail) return NULL;
    return mListTail->rData;
}
void *StarLinkedList::GetEntryByRandomRoll()
{
    //--Note: Does not return the random pointer, returns one entry selected at random. Can legally
    //  return NULL if the list is empty.
    //--All entries on the list are weighted equally. You want more advanced weighting, write it yourself.
    if(mListSize < 1) return NULL;

    //--Roll!
    int tEntry = rand() % mListSize;

    //--Get the entry in question.
    StarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < tEntry; i ++)
    {
        if(!rEntry) return NULL;
        rEntry = rEntry->rNext;
    }

    //--Return it.
    if(!rEntry) return NULL;
    return rEntry->rData;
}

///======================================== Manipulators ==========================================
void StarLinkedList::SetDebugFlag(bool pFlag)
{
    mDebugFlag = pFlag;
}
void StarLinkedList::SetDeallocation(bool pFlag)
{
    mHandlesDeallocation = pFlag;
}
void StarLinkedList::SetCaseSensitivity(bool pFlag)
{
    if(pFlag)
    {
        rStringCompareFuncPtr = &strcmp;
    }
    else
    {
        rStringCompareFuncPtr = &strcasecmp;
    }
}

///========================================== Sorting =============================================
int StarLinkedList::CompareEntries(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() below.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    return strcmp((*rEntryA)->mName, (*rEntryB)->mName);
}
int StarLinkedList::CompareEntriesRev(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() below. This one is Z to A.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    return (strcmp((*rEntryA)->mName, (*rEntryB)->mName) * -1);
}
void StarLinkedList::SortList(bool pReverseFlag)
{
    //--Sorts the list using the standard CompareEntries function, or CompareEntriesRev if the flag
    //  is set. This works regardless of the subtype since the entries are sorted, not their data.
    if(!pReverseFlag)
    {
        SortListUsing(&CompareEntries);
    }
    else
    {
        SortListUsing(&CompareEntriesRev);
    }
}
void StarLinkedList::SortListUsing(SortFnPtr pSortFunction)
{
    //--Sorts the linked list using the provided sorting function. It is up to the coder to make sure
    //  all entries on the list are compatible with the sorting function.
    //--Usage of the built-in CompareEntries function is guaranteed to work.
    if(mListSize < 2 || !pSortFunction) return;

    //--Array of entries
    SetMemoryData(__FILE__, __LINE__);
    StarLinkedListEntry **tEntriesArray = (StarLinkedListEntry **)starmemoryalloc(sizeof(StarLinkedListEntry *) * mListSize);

    //--Fill it with the list's data
    StarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        tEntriesArray[i] = rEntry;
        rEntry = rEntry->rNext;
    }

    //--Sort it.
    qsort(tEntriesArray, mListSize, sizeof(StarLinkedListEntry *), pSortFunction);

    //--Dump it back into the RLL.
    mListHead = tEntriesArray[0];
    mListTail = tEntriesArray[mListSize-1];

    rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        if(i == 0)
        {
            rEntry->rPrev = NULL;
        }
        else
        {
            rEntry->rPrev = tEntriesArray[i-1];
        }
        if(i == mListSize - 1)
        {
            rEntry->rNext = NULL;
        }
        else
        {
            rEntry->rNext = tEntriesArray[i+1];
        }
        rEntry = rEntry->rNext;
    }
    free(tEntriesArray);
}

///======================================== Core Methods ==========================================
void *StarLinkedList::DeleteHead()
{
    //--Deletes the head of the list. Auto-detects special cases where the list is one-object large.
    if(mListHead == NULL) return NULL;
    if(mListTail == mListHead) return DeleteWholeList();

    //--Get the list head and the new head. Should always exist, but we check anyway.
    StarLinkedListEntry *rOldHead = mListHead;
    StarLinkedListEntry *rNewHead = mListHead->rNext;

    //--Clear the previous pointer.
    if(rNewHead) rNewHead->rPrev = NULL;

    //--Set this as the new list head.
    mListHead = rNewHead;

    //--If the old head was the random pointer, set it to NULL. Incrementing will then move it to the new head.
    if(mListCurrent == rOldHead) mListCurrent = NULL;

    //--Shrink the list counter.
    DecrementList();

    //--Done.
    return DeleteEntry(rOldHead);
}
void *StarLinkedList::DeleteTail()
{
    //--As above, but for the tail.
    if(mListTail == NULL) return NULL;
    if(mListTail == mListHead) return DeleteWholeList();

    //--Get the list tail, and the new tail. Ostensibly it will always exist, but we check anyway.
    StarLinkedListEntry *rOldTail = mListTail;
    StarLinkedListEntry *rNewTail = rOldTail->rPrev;

    //--Clear the next pointer.
    if(rNewTail) rNewTail->rNext = NULL;

    //--Set this as the list tail.
    mListTail = rNewTail;

    //--If the removal entry happens to be the random pointer, reset it to the tail.
    if(mListCurrent == rOldTail) mListCurrent = rNewTail;

    //--Reduce the length counter.
    DecrementList();

    //--Done.
    return DeleteEntry(rOldTail);
}
void StarLinkedList::ClearList()
{
    //--Removes everything on the list, one at a time. If mHandlesDeallocation is true, then it
    //  will delete the pointer attached using the provided function pointer. If not, it is
    //  untouched, and something else should handle deletion if appropriate.
    StarLinkedListEntry *tEntry = mListHead;
    StarLinkedListEntry *nEntry;
    for(int i = 0; i < mListSize; i ++)
    {
        nEntry = tEntry->rNext;
        DeleteEntry(tEntry);
        tEntry = nEntry;
    }
    mListSize = 0;
    mListHead = NULL;
    mListTail = NULL;
}
void *StarLinkedList::DeleteEntry(StarLinkedListEntry *pEntry)
{
    //--Worker function, takes in an entry and deallocates it. The rData pointer is passed back,
    //  though the deletion function MAY have deleted it (or not, some functions may preserve data).
    //--Returns NULL on error.
    if(!pEntry) return NULL;

    //--Get Pointers
    void *rDataPtr = NULL;

    //--Linked List handles deallocation
    if(mHandlesDeallocation)
    {
        //--Entity has a function to delete it.
        if(pEntry->rDeletionFunc)
        {
            pEntry->rDeletionFunc(pEntry->rData);
        }
        //--Entity is a primitive, the deletion function was NULL. This is effectively a free() call.
        //  Generally, though, using FreeThis() is preferred if possible.
        else
        {
            free(pEntry->rData);
        }
    }
    //--List returns a pointer to allow the program to deallocate.
    else
    {
        rDataPtr = pEntry->rData;
    }

    //--The rest.
    free(pEntry->mName);
    free(pEntry);
    return rDataPtr;
}
void StarLinkedList::CloneToList(StarLinkedList *pList)
{
    //--Given another list, takes all elements on this list and creates a copy on the destination list.
    //  The ownership state does not explicitly change.
    if(!pList) return;

    //--Iterate.
    StarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        //--Create a clone. The name is cloned, the data and deletion function are references.
        pList->AddElementAsTail(rEntry->mName, rEntry->rData, rEntry->rDeletionFunc);

        //--Next.
        rEntry = rEntry->rNext;
    }
}

///==================================== Private Core Methods ======================================
void *StarLinkedList::DeleteWholeList()
{
    //--Called when the list is composed of a single member, is a special case of DeleteHead()
    //  and DeleteTail(). Is private for a reason!
    StarLinkedListEntry *tEntry = mListHead;

    //--Delete and store the data. If deallocation is on, the pointer may be unstable.
    void *rDataPtr = DeleteEntry(tEntry);

    //--Clear off the list.
    mListTail = NULL;
    mListHead = NULL;
    mListSize = 0;

    //--Return.
    return rDataPtr;
}
void StarLinkedList::DecrementList()
{
    //--Called whenever a member of the list is deleted. Is private since the list's size should
    //  not be interfered with externally, that may cause dangling pointers.
    mListSize --;

    //--Detects the case of whole-list deletion.
    if(mListSize == 0)
    {
        mListHead = NULL;
        mListTail = NULL;
    }
}
