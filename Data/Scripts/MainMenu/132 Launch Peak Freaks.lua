-- |[ ====================================== Menu Launcher ===================================== ]|
--Script called when this game is selected from the main menu.
local zPeakFreaksGameEntry = SysPaths:fnGetGameEntryValid("Peak Freaks")
if(zPeakFreaksGameEntry == nil) then return end

--Execute the file in question.
LM_ExecuteScript(zPeakFreaksGameEntry.sActivePath)
