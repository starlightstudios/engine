//--Base
#include "RootLevel.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

//--Derived types

///========================================== System ==============================================
RootLevel::RootLevel()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ROOTLEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System
    mName = NULL;
    ResetString(mName, "Unnamed Level");
}
RootLevel::~RootLevel()
{
    free(mName);
}

///===================================== Property Queries =========================================
char *RootLevel::GetName()
{
    return mName;
}
bool RootLevel::GetClipAt(float pX, float pY, float pZ)
{
    //--Base class always returns false, indicating free movement.
    return false;
}
bool RootLevel::EnemiesCanAcquireTargets()
{
    //--Used by enemy AIs to check if they can chase the player, the AI will not chase when the
    //  map is loading. This is handled by derived classes.
    return false;
}

///======================================== Manipulators ==========================================
void RootLevel::SetName(const char *pName)
{
    ResetString(mName, pName);
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void RootLevel::Update()
{
    //--Base class does nothing.
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void RootLevel::AddToRenderList(StarLinkedList *pRenderList)
{
    //--The default RootLevel doesn't actually do any rendering, and therefore does not add itself
    //  to the rendering list. If you want debug rendering, you can do it here.
}
void RootLevel::Render()
{
    //--Override meant to allow debug rendering. Fill that in here.
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
RootLevel *RootLevel::ReadFileType(const char *pPath, int pType)
{
    //--Static routing function. The passed in type is read by the path and the level is returned.
    if(!pPath || pType < POINTER_TYPE_LEVEL_BEGIN || pType > POINTER_TYPE_LEVEL_END) return NULL;

    //--RootLevel cannot actually be returned even if it's a valid type.
    if(pType == POINTER_TYPE_ROOTLEVEL)
    {
        return NULL;
    }

    //--Failed to resolve type.
    return NULL;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
