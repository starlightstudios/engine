///--[ ============ Base ============ ]
#include "PairanormalStartup.h"
#include "PairanormalDialogueCharacter.h"
#include "PairanormalDialogue.h"
#include "PairanormalMenu.h"
#include "PairanormalLevel.h"
#include "PairanormalSave.h"

///======================================= Registration ===========================================
void RegisterLocalLuaFunctions(lua_State *pLuaState)
{
    ///--[Documentation]
    //--Called after the LuaManager is created, registers every function in the program to its
    //  Lua state.  Lua scripts should not execute before this.

    ///--[ ========== Classes =========== ]
    PairanormalDialogueCharacter::HookToLuaState(pLuaState);
    PairanormalDialogue::HookToLuaState(pLuaState);
    PairanormalMenu::HookToLuaState(pLuaState);
    PairanormalLevel::HookToLuaState(pLuaState);
    PairanormalSave::HookToLuaState(pLuaState);
}
