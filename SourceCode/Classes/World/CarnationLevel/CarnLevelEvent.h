///======================================= CarnLevelEvent ==========================================
//--Represents a notifier on the events system. These can pop up when the player is injured, gets an
//  item, is influenced, or anything else. These are basically a blob of text and a background.
//  The player can click one to dismiss it or click a button to dismiss them all.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define CARN_NOTIFICATION_HEI 67.0f
#define CARN_NOTIFICATION_STAY_ONSCREEN_TICKS 300

///========================================== Classes =============================================
class CarnLevelEvent
{
    private:
    ///--[System]
    int mTimer;                             //When this counts down to 0, automatically moves offscreen and deallocates.
    bool mRemoveNextTick;                   //When set to true, object dealloactes when the caller updates.

    ///--[Position]
    bool mDespawnAtEndOfMovement;           //If true, the object flags mRemoveNextTick to true when it stops moving.
    float mHEd;                             //Size, in pixels, of the horizontal edge.
    float mVEd;                             //Size, in pixels, of the vertical edge.
    float mWid;                             //Width of the notification, including the edges.
    float mHei;                             //Height of the notification, including the edges.
    EasingPack2D mPosition;                 //Position, can handle dynamic movement.

    ///--[Text]
    char *mText;                            //Text that displays over the backing.

    ///--[Images]
    bool mImagesAreReady;
    StarFont *rFont;
    StarBitmap *rBacking;

    protected:

    public:
    //--System
    CarnLevelEvent();
    ~CarnLevelEvent();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool ShouldDespawn();
    bool WillDespawnWhenDoneMoving();
    float GetXPosition();
    float GetYPosition();
    float GetWidth();
    float GetHeight();
    bool IsPointWithin(float pX, float pY);

    //--Manipulators
    void SetText(const char *pText);
    void MoveTo(float pX, float pY, int pTicks);
    void MarkDespawnAfterMoving();
    void SetFont(const char *pFontName);
    void SetBacking(const char *pDLPath);

    //--Core Methods
    void RecomputeSizes();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


