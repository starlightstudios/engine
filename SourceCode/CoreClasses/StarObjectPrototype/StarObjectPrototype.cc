//--Base
#include "StarObjectPrototype.h"

//--Classes
#include "ScriptObjectBuilder.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

///--[Debug]
//#define SAMPLE_DEBUG
#ifdef SAMPLE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
StarObjectPrototype::StarObjectPrototype()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ == StarObjectPrototype === ]
    ///--[System]
    mInternalName = InitializeString("Unnamed Object");
    mExecScript = InitializeString("Null");
    mFieldListing = new StarLinkedList(true);

    ///--[Description]
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
}
StarObjectPrototype::~StarObjectPrototype()
{
    free(mInternalName);
    free(mExecScript);
    delete mFieldListing;
    for(int i = 0; i < mDescriptionLinesTotal; i ++) free(mDescriptionLines[i]);
    free(mDescriptionLines);
}
void StarObjectPrototype::DeleteThis(void *pPtr)
{
    //--NULL check.
    if(!pPtr) return;

    //--Cast, delete.
    StarObjectPrototype *rPtr = (StarObjectPrototype *)pPtr;
    delete rPtr;
}

///===================================== Property Queries =========================================
const char *StarObjectPrototype::GetName()
{
    return mInternalName;
}
const char *StarObjectPrototype::GetExecPath()
{
    return mExecScript;
}
int StarObjectPrototype::GetTotalFields()
{
    return mFieldListing->GetListSize();
}
StarObjectField *StarObjectPrototype::GetFieldI(int pSlot)
{
    //--Note: Can legally return NULL.
    return (StarObjectField *)mFieldListing->GetElementBySlot(pSlot);
}
StarObjectField *StarObjectPrototype::GetFieldS(const char *pName)
{
    //--Note: Can legally return NULL.
    return (StarObjectField *)mFieldListing->GetElementByName(pName);
}
int StarObjectPrototype::GetIntegerField(const char *pName)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("GetIntegerField", pName, POINTER_TYPE_INT);
    if(!rCheckPtr) return 0;

    //--Return.
    return rCheckPtr->mNumeric.i;
}
float StarObjectPrototype::GetFloatField(const char *pName)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("GetFloatField", pName, POINTER_TYPE_FLOAT);
    if(!rCheckPtr) return 0.0f;

    //--Return.
    return rCheckPtr->mNumeric.f;
}
const char *StarObjectPrototype::GetStringField(const char *pName)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("GetStringField", pName, POINTER_TYPE_STRING);
    if(!rCheckPtr) return "Null";

    //--Internal value was NULL. This shouldn't be possible, print a warning.
    if(!rCheckPtr->mAlpha)
    {
        fprintf(stderr, "StarObjectPrototype::GetStringField() - Warning, internal string field was NULL. Failing.\n");
        return "Null";
    }

    //--Return.
    return rCheckPtr->mAlpha;
}
int StarObjectPrototype::GetTotalDescriptionLines()
{
    return mDescriptionLinesTotal;
}
const char *StarObjectPrototype::GetDescriptionLine(int pSlot)
{
    //--Can legally return NULL.
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return NULL;
    return mDescriptionLines[pSlot];
}

///======================================= Manipulators ===========================================
void StarObjectPrototype::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mInternalName, pName);
}
void StarObjectPrototype::SetExecScript(const char *pPath)
{
    if(!pPath) return;
    ResetString(mExecScript, pPath);
}
void StarObjectPrototype::AllocateDescriptionLines(int pTotal)
{
    //--Clear.
    for(int i = 0; i < mDescriptionLinesTotal; i ++) free(mDescriptionLines[i]);
    free(mDescriptionLines);

    //--Reset.
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mDescriptionLinesTotal = pTotal;
    mDescriptionLines = (char **)starmemoryalloc(sizeof(char *) * mDescriptionLinesTotal);
    for(int i = 0; i < mDescriptionLinesTotal; i ++) mDescriptionLines[i] = NULL;
}
void StarObjectPrototype::SetDescriptionLine(int pLine, const char *pDescription)
{
    //--Range check.
    if(pLine < 0 || pLine >= mDescriptionLinesTotal) return;

    //--If the Description is "Null", clear the description.
    if(pDescription && !strcasecmp(pDescription, "Null"))
    {
        ResetString(mDescriptionLines[pLine], NULL);
        return;
    }

    //--Set.
    ResetString(mDescriptionLines[pLine], pDescription);
}
void StarObjectPrototype::AddIntegerField(const char *pName, int pDefaultValue)
{
    //--Error check.
    if(!pName) return;

    //--Duplicate check.
    void *rCheckPtr = mFieldListing->GetElementByName(pName);
    if(rCheckPtr) return;

    //--Create.
    StarObjectField *nField = (StarObjectField *)starmemoryalloc(sizeof(StarObjectField));
    nField->Initialize();
    ResetString(nField->mName, pName);
    nField->mType = POINTER_TYPE_INT;
    nField->mNumeric.i = pDefaultValue;

    //--Register.
    mFieldListing->AddElementAsTail(pName, nField, &StarObjectField::DeleteThis);
}
void StarObjectPrototype::AddFloatField(const char *pName, float pDefaultValue)
{
    //--Error check.
    if(!pName) return;

    //--Duplicate check.
    void *rCheckPtr = mFieldListing->GetElementByName(pName);
    if(rCheckPtr) return;

    //--Create.
    StarObjectField *nField = (StarObjectField *)starmemoryalloc(sizeof(StarObjectField));
    nField->Initialize();
    ResetString(nField->mName, pName);
    nField->mType = POINTER_TYPE_FLOAT;
    nField->mNumeric.f = pDefaultValue;

    //--Register.
    mFieldListing->AddElementAsTail(pName, nField, &StarObjectField::DeleteThis);
}
void StarObjectPrototype::AddStringField(const char *pName, const char *pDefaultValue)
{
    //--Error check.
    if(!pName || !pDefaultValue) return;

    //--Duplicate check.
    void *rCheckPtr = mFieldListing->GetElementByName(pName);
    if(rCheckPtr) return;

    //--Create.
    StarObjectField *nField = (StarObjectField *)starmemoryalloc(sizeof(StarObjectField));
    nField->Initialize();
    ResetString(nField->mName, pName);
    nField->mType = POINTER_TYPE_STRING;
    nField->mAlpha = InitializeString(pDefaultValue);

    //--Register.
    mFieldListing->AddElementAsTail(pName, nField, &StarObjectField::DeleteThis);
}
void StarObjectPrototype::SetIntegerField(const char *pName, int pValue)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("SetIntegerField", pName, POINTER_TYPE_INT);
    if(!rCheckPtr) return;

    //--Set.
    rCheckPtr->mNumeric.i = pValue;

    //--Enumerations check.
    rCheckPtr->CheckAgainstEnumerations();
}
void StarObjectPrototype::SetFloatField(const char *pName, float pValue)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("SetFloatField", pName, POINTER_TYPE_FLOAT);
    if(!rCheckPtr) return;

    //--Set.
    rCheckPtr->mNumeric.f = pValue;
}
void StarObjectPrototype::SetStringField(const char *pName, const char *pValue)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("SetStringField", pName, POINTER_TYPE_STRING);
    if(!rCheckPtr) return;

    //--Error check: Must pass a non-NULL string.
    if(!pValue) return;

    //--Set.
    ResetString(rCheckPtr->mAlpha, pValue);
}
void StarObjectPrototype::AllocateEnumerationsInField(const char *pName, int pEnumerations)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("AllocateEnumerationsInField", pName, POINTER_TYPE_INT);
    if(!rCheckPtr) return;

    //--Allocate.
    rCheckPtr->AllocateEnumerations(pEnumerations);
}
void StarObjectPrototype::SetEnumerationInField(const char *pName, int pSlot, int pValue, const char *pString)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("SetEnumerationInField", pName, POINTER_TYPE_INT);
    if(!rCheckPtr) return;

    //--Set.
    rCheckPtr->SetEnumeration(pSlot, pValue, pString);
}
void StarObjectPrototype::SetEnumerationLocking(const char *pName, bool pIsLocked)
{
    //--Subroutine check.
    StarObjectField *rCheckPtr = CheckExistence("SetEnumerationLocking", pName, POINTER_TYPE_INT);
    if(!rCheckPtr) return;

    //--Set.
    rCheckPtr->mLockToEnumerations = pIsLocked;
}

///======================================= Core Methods ===========================================
StarObjectField *StarObjectPrototype::CheckExistence(const char *pFunction, const char *pFieldName, int pFieldType)
{
    ///--[Documentation]
    //--Worker function, checks for the existence of the given field and its type. If an error occurs, barks
    //  a warning and returns NULL, using the provided function name to specify where the error occurred.
    //--Otherwise, returns the StarObjectField requested.
    if(!pFunction)
    {
        fprintf(stderr, "StarObjectPrototype::CheckExistence() - Warning, invalid function name string passed to checker function.\n");
        return NULL;
    }

    //--If the field name was Null:
    if(!pFieldName)
    {
        fprintf(stderr, "StarObjectPrototype::%s() - Error, field name was NULL. Failing.\n", pFunction);
        return NULL;
    }

    //--Existence check.
    StarObjectField *rCheckPtr = (StarObjectField *)mFieldListing->GetElementByName(pFieldName);
    if(!rCheckPtr)
    {
        fprintf(stderr, "StarObjectPrototype::%s() - Error, field %s does not exist. Failing.\n", pFunction, pFieldName);
        return NULL;
    }

    //--Type check.
    if(rCheckPtr->mType != pFieldType)
    {
        fprintf(stderr, "StarObjectPrototype::%s() - Error, field %s exists but is not expected type %i, but is of type %i. Failing.\n", pFunction, pFieldName, pFieldType, rCheckPtr->mType);
        return NULL;
    }

    //--All checks passed.
    return rCheckPtr;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarObjectField *StarObjectPrototype::GetField(const char *pName)
{
    //--Error check:
    if(!pName) return NULL;

    //--Return. Can legally return NULL.
    return (StarObjectField *)mFieldListing->GetElementByName(pName);
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void StarObjectPrototype::HookToLuaState(lua_State *pLuaState)
{
    /* Hook_SOP_GetProperty("Total Fields") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "SOP_GetProperty", &Hook_SOP_GetProperty);

    /* Hook_SOP_SetProperty("Name")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "SOP_SetProperty", &Hook_SOP_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_SOP_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //SOP_GetProperty("Total Fields") (1 Integer)

    //--[Fields]
    //SOP_GetProperty("Get Integer Field", sFieldName) (1 Integer)
    //SOP_GetProperty("Get Float Field", sFieldName) (1 Float)
    //SOP_GetProperty("Get String Field", sFieldName) (1 String)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SOP_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    StarObjectPrototype *rStarObjectPrototype = (StarObjectPrototype *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--How many fields are in the object.
    if(!strcasecmp(rSwitchType, "Total Fields") && tArgs >= 1)
    {
        lua_pushinteger(L, rStarObjectPrototype->GetTotalFields());
        tReturns = 1;
    }
    ///--[Fields]
    else if(!strcasecmp(rSwitchType, "Get Integer Field") && tArgs >= 2)
    {
        lua_pushinteger(L, rStarObjectPrototype->GetIntegerField(lua_tostring(L, 2)));
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Get Float Field") && tArgs >= 2)
    {
        lua_pushnumber(L, rStarObjectPrototype->GetFloatField(lua_tostring(L, 2)));
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Get String Field") && tArgs >= 2)
    {
        lua_pushstring(L, rStarObjectPrototype->GetStringField(lua_tostring(L, 2)));
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("SOP_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_SOP_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //SOP_SetProperty("Execution Path", sExecPath)

    //--[Description]
    //SOP_SetProperty("Allocate Description Lines", iCount)
    //SOP_SetProperty("Set Description Line", iSlot, sLine)
    //SOP_SetProperty("Set Description Auto", sString)

    //--[Fields]
    //SOP_SetProperty("Add Integer Field", sFieldName, iDefault)
    //SOP_SetProperty("Add Float Field", sFieldName, fDefault)
    //SOP_SetProperty("Add String Field", sFieldName, sDefault)
    //SOP_SetProperty("Add Boolean Field", sFieldName, iDefault)
    //SOP_SetProperty("Set Field Description", sFieldName, sDescription)

    //--[Enumerations]
    //SOP_SetProperty("Allocate Enumerations In Field", sFieldName, iEnumerations)
    //SOP_SetProperty("Set Enumerations In Field", sFieldName, iSlot, iValue, sDisplay)
    //SOP_SetProperty("Set Enumerations Locking Flag", sFieldName, bIsLocked)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SOP_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    StarObjectPrototype *rStarObjectPrototype = (StarObjectPrototype *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Execution Path
    if(!strcasecmp(rSwitchType, "Execution Path") && tArgs >= 2)
    {
        rStarObjectPrototype->SetExecScript(lua_tostring(L, 2));
    }
    ///--[Description]
    //--Allocates space for description.
    else if(!strcasecmp(rSwitchType, "Allocate Description Lines") && tArgs >= 2)
    {
        rStarObjectPrototype->AllocateDescriptionLines(lua_tointeger(L, 2));
    }
    //--Manually specify description text by line.
    else if(!strcasecmp(rSwitchType, "Set Description Line") && tArgs >= 3)
    {
        rStarObjectPrototype->SetDescriptionLine(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Automated parser allocates and sets description.
    else if(!strcasecmp(rSwitchType, "Set Description Auto") && tArgs >= 2)
    {
        //--Get font. ScriptObjectBuilder must be the active level.
        RootLevel *rLevel = MapManager::Fetch()->GetActiveLevel();
        if(rLevel->GetType() != POINTER_TYPE_SCRIPTOBJECTBUILDER)
        {
            fprintf(stderr, "SOP_SetProperty: Failed, level was wrong type, or NULL. Property: Set Description Auto\n");
            return 0;
        }

        //--Cast.
        ScriptObjectBuilder *rScriptObjectBuilder = (ScriptObjectBuilder *)rLevel;
        StarFont *rMainlineFont = rScriptObjectBuilder->GetMainlineFont();
        if(!rMainlineFont)
        {
            fprintf(stderr, "SOP_SetProperty: Failed, no mainline font resolved. Property: Set Description Auto\n");
            return 0;
        }

        //--Run subdivide.
        StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(lua_tostring(L, 2), SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, rMainlineFont, VIRTUAL_CANVAS_X * 0.90f);

        //--Allocate.
        rStarObjectPrototype->AllocateDescriptionLines(tStringList->GetListSize());

        //--Set.
        int i = 0;
        const char *rString = (const char *)tStringList->PushIterator();
        while(rString)
        {
            //--Set.
            rStarObjectPrototype->SetDescriptionLine(i, rString);

            //--Next.
            i ++;
            rString = (const char *)tStringList->AutoIterate();
        }

        //--Clean.
        delete tStringList;
    }
    ///--[Fields]
    //--Adds a new integer field.
    else if(!strcasecmp(rSwitchType, "Add Integer Field") && tArgs >= 3)
    {
        rStarObjectPrototype->AddIntegerField(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Adds a new floating-point field.
    else if(!strcasecmp(rSwitchType, "Add Float Field") && tArgs >= 3)
    {
        rStarObjectPrototype->AddFloatField(lua_tostring(L, 2), lua_tonumber(L, 3));
    }
    //--Adds a new string field.
    else if(!strcasecmp(rSwitchType, "Add String Field") && tArgs >= 3)
    {
        rStarObjectPrototype->AddStringField(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Special instance of an integer field, boolean fields always lock to 0 and 1 for false and true.
    else if(!strcasecmp(rSwitchType, "Add Boolean Field") && tArgs >= 3)
    {
        rStarObjectPrototype->AddIntegerField(lua_tostring(L, 2), lua_tointeger(L, 3));
        rStarObjectPrototype->AllocateEnumerationsInField(lua_tostring(L, 2), 2);
        rStarObjectPrototype->SetEnumerationInField(lua_tostring(L, 2), 0, 0, "False");
        rStarObjectPrototype->SetEnumerationInField(lua_tostring(L, 2), 1, 1, "True");
        rStarObjectPrototype->SetEnumerationLocking(lua_tostring(L, 2), true);
    }
    //--Sets the on-screen description for the field. Uses Subdivide.
    else if(!strcasecmp(rSwitchType, "Set Field Description") && tArgs >= 3)
    {
        //--Locate the field first.
        StarObjectField *rActiveField = rStarObjectPrototype->GetField(lua_tostring(L, 2));
        if(!rActiveField)
        {
            fprintf(stderr, "SOP_SetProperty: Failed, no field %s was found. Property: Set Field Description\n", lua_tostring(L, 2));
            return 0;
        }

        //--Get font. ScriptObjectBuilder must be the active level.
        RootLevel *rLevel = MapManager::Fetch()->GetActiveLevel();
        if(rLevel->GetType() != POINTER_TYPE_SCRIPTOBJECTBUILDER)
        {
            fprintf(stderr, "SOP_SetProperty: Failed, level was wrong type, or NULL. Property: Set Field Description\n");
            return 0;
        }

        //--Cast.
        ScriptObjectBuilder *rScriptObjectBuilder = (ScriptObjectBuilder *)rLevel;
        StarFont *rMainlineFont = rScriptObjectBuilder->GetMainlineFont();
        if(!rMainlineFont)
        {
            fprintf(stderr, "SOP_SetProperty: Failed, no mainline font resolved. Property: Set Field Description\n");
            return 0;
        }

        //--Run subdivide.
        StarLinkedList *tStringList = Subdivide::SubdivideStringFlags(lua_tostring(L, 3), SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, rMainlineFont, VIRTUAL_CANVAS_X * 0.90f);

        //--Allocate.
        rActiveField->AllocateDescriptionLines(tStringList->GetListSize());

        //--Set.
        int i = 0;
        const char *rString = (const char *)tStringList->PushIterator();
        while(rString)
        {
            //--Set.
            rActiveField->SetDescriptionLine(i, rString);

            //--Next.
            i ++;
            rString = (const char *)tStringList->AutoIterate();
        }

        //--Clean.
        delete tStringList;
    }
    ///--[Enumerations]
    //--Sets how many enumerations are in a given field. Only works on integers.
    else if(!strcasecmp(rSwitchType, "Allocate Enumerations In Field") && tArgs >= 3)
    {
        rStarObjectPrototype->AllocateEnumerationsInField(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets a given enumeration. The 0th enumeration is the default if locking is enabled.
    else if(!strcasecmp(rSwitchType, "Set Enumerations In Field") && tArgs >= 5)
    {
        rStarObjectPrototype->SetEnumerationInField(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5));
    }
    //--If true, only specifically enumerated values can be used, otherwise user input locks to the default enumeration.
    else if(!strcasecmp(rSwitchType, "Set Enumerations Locking Flag") && tArgs >= 3)
    {
        rStarObjectPrototype->SetEnumerationLocking(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("SOP_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}

