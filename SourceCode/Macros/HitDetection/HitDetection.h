///======================================= Hit Detection ===========================================
//--Generic functions for detecting collisions between geometric constructions.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--Point within Rectangle
bool IsPointWithin(float pX, float pY, float pLft, float pTop, float pRgt, float pBot);
bool IsPointWithinWH(float pX, float pY, float pLft, float pTop, float pWid, float pHei);
bool IsPointWithin2DReal(float pX, float pY, TwoDimensionReal pRect);
bool IsPointWithinExclusive(float pX, float pY, float pLft, float pTop, float pRgt, float pBot);

//--Rectangle within Rectangle
void Swap(int &s1, int &s2);
void SwapF(float &s1, float &s2);
bool IsCollision(float lft1, float top1, float rgt1, float bot1, float lft2, float top2, float rgt2, float bot2);
bool IsCollision3DReal(ThreeDimensionReal pDim1, int pLft2, int pTop2, int pRgt2, int pBot2);
bool IsCollision3DReal(ThreeDimensionReal pDim1, ThreeDimensionReal pDim2);
bool IsCollision2DReal(TwoDimensionReal pDim1, TwoDimensionReal pDim2);
bool IsCollision2DReal(TwoDimensionReal pDim1, int pLft2, int pTop2, int pRgt2, int pBot2);
bool IsCollision2D3DReal(ThreeDimensionReal pDim1, TwoDimensionReal pDim2);
bool IsCollisionExclusive(float pLft, float pTop, float pRgt, float pBot, float pLft2, float pTop2, float pRgt2, float pBot2);

//--Rectangle within Rectangle, but strict.  One must completely contain the other.
bool IsCollisionStrict(int lft1, int top1, int rgt1, int bot1, int lft2, int top2, int rgt2, int bot2);
bool IsCollisionStrict(ThreeDimensionReal pDim1, int lft2, int top2, int rgt2, int bot2);
bool IsCollisionStrict(TwoDimensionReal pDim1, int lft2, int top2, int rgt2, int bot2);
bool IsCollisionStrict(ThreeDimensionReal pDim1, ThreeDimensionReal pDim2);

//--Percentage of overlap
float PercentOverlap(int pLft1, int pTop1, int pRgt1, int pBot1, int pLft2, int pTop2, int pRgt2, int pBot2);
float PercentOverlap(ThreeDimensionReal pDim1, int pLft2, int pTop2, int pRgt2, int pBot2);

//--Point within Polygon
bool IsPolyCollisionRightHand(float pX, float pY, float *pArray);
bool IsPolyCollisionRightHand(float pX, float pY, PolygonalHitbox pHitbox);

//--Distance Calculations
float GetAngleBetween(float pX1, float pY1, float pX2, float pY2);
float GetPlanarDistance(float pX1, float pY1, float pX2, float pY2);
float GetDistanceToLine(float pLineX1, float pLineY1, float pLineX2, float pLineY2, float pPointX, float pPointY);
float Get3DDistance(float pX1, float pY1, float pZ1, float pX2, float pY2, float pZ2);
float Get3DDistance(Point3D pPointA, float pX2, float pY2, float pZ2);
float Get3DDistance(Point3D pPointA, Point3D pPointB);

//--General Purpose Polygons
//float AreaOfPoly(float *pPolygon);
float AreaOfPoly(StarLinkedList *pPolygon);
float AreaOfPolyWithout(StarLinkedList *pPolygon, int pIgnoreThisSlot);
float AreaOfTriangle(float mXA, float mYA, float mXB, float mYB, float mXC, float mYC);
bool IsCollisionPoly(float *pPolyA, float *pPolyB);
bool IsCollisionPolyStruct(PolygonalHitbox pPolyA, PolygonalHitbox pPolyB);
bool IsCollision3DRealPolyStruct(ThreeDimensionReal pDim1, PolygonalHitbox pPolyB);

//--Circular Homing
float HomeInOnAngleRad(float pStartAngle, float pTargetAngle, float pEpsilon);
float HomeInOnAngleDeg(float pStartAngle, float pTargetAngle, float pEpsilon);
