//--Base
#include "SaveManager.h"

//--Classes
//--CoreClasses
#include "StarArray.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
char *SaveManager::GenerateNote(const char *pNewNote, bool &sDeallocateNote)
{
    ///--[Documentation]
    //--If a note is passed to ModifyFileNote() below that is NULL or 0 characters long, this function
    //  will generate a new note and flag sDeallocateNote to true. If the string is valid, it passes
    //  back the string.
    //--This function allows for a standardized no-note case for all the places in the program where
    //  it is possible to edit a note.

    //--If the note was left empty or is NULL, then generate a note.
    char *rNewNote = (char *)pNewNote;
    if(!rNewNote || strlen(rNewNote) < 1)
    {
        sDeallocateNote = true;
        rNewNote = InitializeString("Empty Note");
    }

    //--Pass it back.
    return rNewNote;
}
void SaveManager::ModifyFileNote(const char *pNewNote, const char *pFilePath)
{
    ///--[Documentation and Setup]
    //--Given a file on the hard drive, modifies its "note". This is a string right after the header
    //  that the player can set to any string they want, usually to tell them what the savefile contains.
    //--The note can be modified from the file select menu both in game and not. It is therefore not
    //  necessary to actually know what's in the savefile or if it's actually valid, only to write
    //  the header and note, then whatever was previously in the file.
    if(!pFilePath) return;

    //--Make sure the note is valid, generating a standard note if not.
    bool tDeallocateNote = false;
    char *rNewNote = SaveManager::GenerateNote(pNewNote, tDeallocateNote);

    ///--[File Handling]
    //--Load the file into a VirtualFile. It *must* be in memory mode since the same file is about to
    //  be written to on the hard drive.
    VirtualFile *tVFile = new VirtualFile(pFilePath, true);
    if(!tVFile->IsReady()) return;

    //--Open the save file back up. This must be done *after* the VirtualFile creates a copy of the memory,
    //  as wb mode will wipe the file.
    FILE *fOutfile = fopen(pFilePath, "wb");
    if(!fOutfile)
    {
        delete tVFile;
        return;
    }

    ///--[Data From Source File]
    //--Get the position of the data, after the heading and the previous note.
    tVFile->Seek(strlen(LOADINFOHEADER));
    char *tPreviousNote = tVFile->ReadLenString();

    //--Store the length of the file and its cursor position. Length - Cursor = Number of bytes to write.
    int tCursor = tVFile->GetCurPosition() + 1;
    int tLength = tVFile->GetLength();

    ///--[Write To Destination]
    //--Write the header to the outfile.
    fwrite(LOADINFOHEADER, sizeof(char), strlen(LOADINFOHEADER), fOutfile);

    //--Write the new player note, with a 2-byte length before it.
    uint16_t tLen = strlen(rNewNote);
    fwrite(&tLen, sizeof(uint16_t), 1, fOutfile);
    fwrite(rNewNote, sizeof(char), tLen, fOutfile);

    //--Now write everything else from the file back into this one.
    StarArray *rDataBuffer = tVFile->GetData();
    rDataBuffer->WriteFileFromMemory(tCursor, tLength - tCursor, fOutfile);

    ///--[Clean]
    //--Always clear.
    delete tVFile;
    fclose(fOutfile);
    free(tPreviousNote);

    //--If the new note was auto-generated, free it here.
    if(tDeallocateNote) free(rNewNote);
}



///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
