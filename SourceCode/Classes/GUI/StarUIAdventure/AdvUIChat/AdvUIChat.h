///========================================= AdvUIChat ============================================
//--Chat menu. Grid UI that shows characters the player can have conversations with.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIGrid.h"

///===================================== Local Structures =========================================
#ifndef ADVUICHAT_DEFINITIONS
#define ADVUICHAT_DEFINITIONS

///--[Chat Grid Storage]
//--Stores the raw data for each chat call. Populated from a lua script.
typedef struct ChatGridPack
{
    //--Members
    char *mCallScript;
    AdvMenuStandardAlignments mAlignments;

    //--Functions
    void Initialize()
    {
        mCallScript = NULL;
        mAlignments.Initialize();
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        ChatGridPack *rPtr = (ChatGridPack *)pPtr;
        free(rPtr->mCallScript);
        free(rPtr);
    }
}ChatGridPack;
#endif

///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class AdvUIChat : virtual public AdvUICommon, virtual public AdvUIGrid
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxAdvChatHelpStrings = 4;

    ///--[Variables]
    //--Cursor
    bool mDontShowHighlight;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Data
    StarLinkedList *mChatData; //ChatGridPack *, master

    //--Strings
    StarlightString *mShowHelpString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Mainline;

        //--Common
        StarBitmap *rOverlay_Footer;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;
    }AdvImages;
    protected:

    public:
    //--System
    AdvUIChat();
    virtual ~AdvUIChat();
    virtual void Construct();

    //--Public Variables
    static char *xChatResolveScript;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    void AddChatEntity(const char *pName, const char *pPath, float pXOff, float pYOff, float pLft, float pTop, float pRgt, float pBot, const char *pImgPath);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


