-- |[ ======================================= Main Menu ======================================== ]|
--This menu executes before anything else in the game actually happens. It allows the user to select
-- which game they'd like to play, set options, or quit. It can be expanded to incorporate other
-- features, like mods or cheats.
--Resetting the game returns to this menu after cleaning up unused resources.
local sBasePath = fnResolvePath()

-- |[Diagnostics]|
local bDiagnostics = fnAutoCheckDebugFlag("Startup: Populate Main Menu")
Debug_PushPrint(bDiagnostics, "Startup: Populate Main Menu.\n")

-- |[Translation Checker]|
--This routine will resolve the active translation. It must be called before anything else in the class.
LM_ExecuteScript("Data/Translations/Translation Menu Init.lua")

-- |[Default Paths]|
--Used for the treasure counting debug options.
DL_AddPath("Root/Paths/System/Startup/")
VM_SetVar("Root/Paths/System/Startup/sDebugAdventurePath", "S", "Games/AdventureMode/")

-- |[ ================================= Title Screen Override ================================== ]|
--This one-off flag autoboots String Tyrant.
if(gsMandateTitle == "String Tyrant") then
	
	--Get entry. Execute active path immediately.
	local zStringTyrantEntry = SysPaths:fnGetGameEntryValid("Doll Manor")
	if(zStringTyrantEntry ~= nil) then
		LM_ExecuteScript(zStringTyrantEntry.sActivePath)
		return
	end
    
--Demo version.
elseif(gsMandateTitle == "String Tyrant Demo") then
	local zStringTyrantDemoEntry = SysPaths:fnGetGameEntry("Doll Manor Demo")
	if(zStringTyrantDemoEntry ~= nil) then
		LM_ExecuteScript(zStringTyrantDemoEntry.sActivePath)
		return
	end
end

-- |[ ==================================== Autorun Handlers ==================================== ]|
--If you're testing something a lot and don't want to navigate the menu every time, use these
-- autorun options.
local sAutorun = OM_GetAutorun()

-- |[Build Autorun List]|
table.insert(gzaAutorunHandlers, {"Monoceros",   fnResolvePath() .. "129 Launch Monoceros.lua", false})
table.insert(gzaAutorunHandlers, {"Carnation",   fnResolvePath() .. "130 Launch Carnation.lua", false})
table.insert(gzaAutorunHandlers, {"MOTF",        fnResolvePath() .. "131 Launch MOTF.lua", false})
table.insert(gzaAutorunHandlers, {"Peak Freaks", fnResolvePath() .. "132 Launch Peak Freaks.lua", false})

-- |[Autorun From List]|
--If the autorun argument is not "Null", see if we can autorun the provided game.
local iAutorunSlot = fnCheckAutorun(sAutorun)

--If the slot did not come back -1, then the autorun exists.
if(iAutorunSlot ~= -1) then

    --Execute the launcher. If the game/mod couldn't resolve, ignore the autorun. Otherwise, the launcher
    -- will boot us into the game immediately.
    LM_ExecuteScript(gzaAutorunHandlers[iAutorunSlot][2])
    if(gzaAutorunHandlers[iAutorunSlot][3] == true) then
        OM_ClearAutorun()
		Debug_PopPrint("Autorun exists, handling.\n")
        return
    end
end

-- |[Auto-Exec]|
--If there's anything in the auto-exec, use it here.
if(gsAutoExec ~= nil) then
	LM_ExecuteScript(gsAutoExec)
	gsAutoExec = nil
	Debug_PopPrint("Autoexec exists, handling.\n")
	return
end

-- |[ =================================== Headers and System =================================== ]|
--Intro. Only fires the first time the menu is used this sequence.
Debug_DropEvents()
if(gbHasRunIntro == false) then
    gbHasRunIntro = true
    FlexMenu_ActivateIntro()
end

-- |[Witch Hunter Izana]|
--Uses its own special menu handlers.
if(gsMandateTitle == "Witch Hunter Izana") then
    
    --Get translatable strings.
    local sTextNewGame  = Translate(gsTranslationUI, "New Game")
    local sTextContinue = Translate(gsTranslationUI, "Continue")
    local sTextOptions  = Translate(gsTranslationUI, "Options")
    local sTextMods     = Translate(gsTranslationUI, "Mod Load Order")
    local sTextQuit     = Translate(gsTranslationUI, "Quit")
    
    --New Game. Starts running the scenario.
    FlexMenu_RegisterButton(sTextNewGame)
        StarButton_SetProperty("Priority", 0)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/MainMenu/129 Launch Monoceros.lua", 0)
        FlexButton_SetProperty("Font", "Monoceros Title Option")
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Continue. Brings up the load game screen.
    FlexMenu_RegisterButton(sTextContinue)
        StarButton_SetProperty("Priority", 1)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/Open Load Menu.lua", 0)
        FlexButton_SetProperty("Font", "Monoceros Title Option")
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Options. Spawns the options menu.
    FlexMenu_RegisterButton(sTextOptions)
        StarButton_SetProperty("Priority", 2)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/Open Options Menu.lua", 0)
        FlexButton_SetProperty("Font", "Monoceros Title Option")
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Mods. Opens the mod menu.
    FlexMenu_RegisterButton(sTextMods)
        StarButton_SetProperty("Priority", 3)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/Open Mod Load Menu.lua", 0)
        FlexButton_SetProperty("Font", "Monoceros Title Option")
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Exit. Quits the game.
    FlexMenu_RegisterButton(sTextQuit)
        StarButton_SetProperty("Priority", 10000)
        StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "191 Quit.lua", 0)
        FlexButton_SetProperty("Font", "Monoceros Title Option")
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Mod population. Obtain basic data and upload it to the menu object.
    local iGameSlot = -1
    for i = 1, #gsModNames, 1 do
        if(gsModNames[i] == "Monoceros") then
            iGameSlot = i
            break
        end
    end
    if(iGameSlot ~= -1) then
        local sMonocerosModPath = gsModDirectories[iGameSlot] .. "Mods/"
        gzWhiModInfo = ModInfo:fnScanModsInto(sMonocerosModPath)
        
        --Modlist must exist.
        if(gzWhiModInfo ~= nil) then
            
            --Upload to C++ state.
            FlexMenu_SetProperty("Clear Modlist")
            for i = 1, #gzWhiModInfo, 1 do
                FlexMenu_SetProperty("Register Mod", gzWhiModInfo[i].sLocalName, gzWhiModInfo[i].sDisplayName, false)
                FlexMenu_SetProperty("Set Mod Path", gzWhiModInfo[i].sLocalName, gzWhiModInfo[i].sPath)
            end
            
            --Order the C++ to execute a list load. It will re-order the modlist based on the saved file on the hard drive.
            FlexMenu_SetProperty("Load Modlist", "Mod Load Order.txt")
        end
    end
    
	Debug_PopPrint("Using Witch Hunter Izana title loader.\n")
    return

-- |[Project Carnation]|
--Uses its own special menu handlers.
elseif(gsMandateTitle == "Witches of Ravenbrook") then
    
    --New Game. Starts running the scenario.
    FlexMenu_RegisterButton("New Game")
        StarButton_SetProperty("Priority", 0)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/MainMenu/130 Launch Carnation.lua", 0)
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Continue. Brings up the load game screen.
    FlexMenu_RegisterButton("Continue")
        StarButton_SetProperty("Priority", 1)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/Open Load Menu.lua", 0)
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Options. Spawns the options menu.
    FlexMenu_RegisterButton("Options")
        StarButton_SetProperty("Priority", 2)
        StarButton_SetProperty("NewLuaExec", 0, "Data/Scripts/Open Options Menu.lua", 0)
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
    --Exit. Quits the game.
    FlexMenu_RegisterButton("Quit")
        StarButton_SetProperty("Priority", 10000)
        StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "191 Quit.lua", 0)
        FlexButton_SetProperty("Dont Render Highlight", true)
    DL_PopActiveObject()
    
	Debug_PopPrint("Using Witches of Ravenbrook title loader.\n")
    return

end

-- |[Header]|
FlexMenu_SetHeaderSize(1)
FlexMenu_SetHeaderLine(0, "Main Menu")

-- |[ ===================================== Mode Selectors ===================================== ]|
--We use the filesystem to resolve whether or not specific files exist. These files are indicators
-- that mean the rest of the files are there, or at least are capable of doing something when
-- selected. Later we might want to use a metadata file or somesuch.
--The list of paths is built in Startup.lua.
local iPriorityCounter = 100
	
--Recheck activity, in case the player moved something and repopulated the main menu. This
-- can occur when quitting to the menu.
SysPaths:fnActivateAllGames()

--For each game entry, if the active path is not "Null", add a button for this entry.
for i = 1, #SysPaths.zaGameEntries, 1 do
	
	--Fast-access pointers.
	local zGameEntry = SysPaths.zaGameEntries[i]
	local sLaunchPath = fnResolvePathFrom(zGameEntry.sActivePath) .. zGameEntry.sFileLaunchPath
	
	--If this active path is "Null", do nothing:
	if(zGameEntry.sActivePath == "Null") then
		
	--If the active path is not "Null", but the button name is "DO NOT ADD", then it doesn't get a button.
	elseif(zGameEntry.sButtonText == "DO NOT ADD") then
	
	--All checks passed, add the button.
	else
		FlexMenu_RegisterButton(zGameEntry.sButtonText)
			StarButton_SetProperty("Priority", zGameEntry.iPriority)
			StarButton_SetProperty("NewLuaExec", 0, sLaunchPath, 0)
		DL_PopActiveObject()
		iPriorityCounter = iPriorityCounter + 1
	end
end

-- |[ ======================================== Options ========================================= ]|
--Sets options that are independent of the game that's currently running. This includes things like
-- resolution, color schemes, and control layouts.
--Always exists, never checks a path.
FlexMenu_RegisterButton("Global Options")
	StarButton_SetProperty("Priority", 7000)
    --FlexButton_SetProperty("Description", "Change program options that affect all game modes.")
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "190 GlobalOptions.lua", 0)
DL_PopActiveObject()

-- |[ ====================================== Autobuilder ======================================= ]|
--Developer-only autobuilder, allows the same functionality as from Adventure's debug menu, but
-- without loading stuff by loading Adventure mode.
if(gbShowAutobuilder == true) then
    
    --Game autobuildr.
    FlexMenu_RegisterButton("Autobuilder")
        StarButton_SetProperty("Priority", 7001)
        --FlexButton_SetProperty("Description", "Change program options that affect all game modes.")
        StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "192 Autobuilder.lua", 0)
    DL_PopActiveObject()
end

-- |[Script Object Builder]|
--Looks for a specific file. If found, the SOB can be used.
if(FS_Exists("Data/Object Prototypes/ZRouting.lua") == true) then
    FlexMenu_RegisterButton("Script Object Builder")
        StarButton_SetProperty("Priority", 7002)
        --FlexButton_SetProperty("Description", "Change program options that affect all game modes.")
        StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "193 Script Object Builder.lua", 0)
    DL_PopActiveObject()

end

-- |[ ===================================== Quit Messages ====================================== ]|
--Roll which message to show for this.
local sQuitMessage = "Quit the program."
local iRoll = LM_GetRandomNumber(0, 9)
if(iRoll == 0) then
	sQuitMessage = "Please don't leave, there's more monstergirls to toast!"
elseif(iRoll == 1) then
	sQuitMessage = "I wouldn't leave if I were you. Your desktop is much worse."
elseif(iRoll == 2) then
	sQuitMessage = "Go ahead and leave, see if I care."
elseif(iRoll == 3) then
	sQuitMessage = "Why would you quit this great game?"
elseif(iRoll == 4) then
	sQuitMessage = "Go back to your boring programs, then."
elseif(iRoll == 5) then
	sQuitMessage = "Leaving? When you come back, I'll be waiting with a bat."
elseif(iRoll == 6) then
	sQuitMessage = "Wait, don't go! We haven't run out of monstergirls yet!"
elseif(iRoll == 7) then
	sQuitMessage = "So you're saying you want to play other games? That's okay..."
elseif(iRoll == 8) then
	sQuitMessage = "I didn't give you permission to quit, get back here!"
elseif(iRoll == 9) then
	sQuitMessage = "If you're going to go look at cat photos, then I don't want you here either!"
end

--Quits the game. Always appears last.
FlexMenu_RegisterButton("Quit")
	StarButton_SetProperty("Priority", 10000)
    --FlexButton_SetProperty("Description", sQuitMessage)
	StarButton_SetProperty("NewLuaExec", 0, sBasePath .. "191 Quit.lua", 0)
DL_PopActiveObject()

--Debug.
Debug_PopPrint("Completed populating main menu.\n")
