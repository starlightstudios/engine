//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"
#include "LuaManager.h"

///=========================================== System =============================================
///--[Definitions]
#define ADLEV_NOTIF_TIME 300
#define ADLEV_NOTIF_SLIDEIN_TICKS 30
#define ADLEV_NOTIF_SLIDEOUT_TICKS 30
#define ADLEV_NOTIF_XPOS 30
#define ADLEV_NOTIF_YPOS (VIRTUAL_CANVAS_Y - 100)
#define ADLEV_NOTIF_SLIDEIN_XDIST 50.0f
#define ADLEV_NOTIF_SLIDEOUT_XDIST -550.0f

///--[Structure Functions]
void NotificationPack::Initialize()
{
    mString = new StarlightString();
    mTimer = 0;
    mTimerMax = 1;
    mYPosition.Initialize();
}
void NotificationPack::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    NotificationPack *rPtr = (NotificationPack *)pPtr;
    delete rPtr->mString;
    free(rPtr);
}

///======================================= Manipulators ===========================================
NotificationPack *AdventureLevel::RegisterNotification(const char *pString)
{
    ///--[Documentation]
    //--Receives a string and turns it into a notification. Notifications are StarlightStrings so they
    //  can show icons, but that setup must be done elsewhere.
    //--Returns the resulting package if further modification is needed. A notification is returned and
    //  registered even if there is an error in the string.

    //--New notification.
    NotificationPack *nPack = (NotificationPack *)starmemoryalloc(sizeof(NotificationPack));
    nPack->Initialize();

    //--Register.
    mNotifications->AddElementAsTail("X", nPack, &NotificationPack::DeleteThis);
    if(!pString) return nPack;

    //--Copy the string over.
    nPack->mString->SetString(pString);

    //--Use a fixed timer for the string.
    nPack->mTimerMax = ADLEV_NOTIF_TIME;

    //--Reresolve Y positions for all packages. Then, complete movement for this pack. This makes it
    //  spawn "on top" without having to slide.
    ReresolveNotificationPositions();
    nPack->mYPosition.Complete();

    //--Finish up.
    return nPack;
}
void AdventureLevel::HandleNotificationForItem(bool pObtainItem, const char *pItemName, const char *pPrefix)
{
    ///--[Documentation]
    //--Standardized notification handler for an item that the player has obtained via any sort of means.
    //  The item will be added to the player's inventory, and a notification will pop up. This can be called
    //  internally or externally via Lua.
    //--If the flag pObtainItem is false, then it is assumed the caller already handled that, but it should be
    //  done immediately before the notification is called, otherwise the icon may have changed.
    if(!pItemName) return;

    ///--[Item Creation]
    //--If flagged, create the item now and add it to the inventory.
    if(pObtainItem && xItemListPath)
    {
        LuaManager::Fetch()->ExecuteLuaFile(xItemListPath, 1, "S", pItemName);
    }

    ///--[Notification Handling]
    //--Assemble and create a notification.
    char tBuffer[STD_MAX_LETTERS];
    tBuffer[0] = '\0';
    if(pPrefix) strcpy(tBuffer, pPrefix);

    //--Split up the quantity and item name. Item names can be translated, while the quantity is tacked
    //  on at the end.
    int tQuantity = 1;
    char *tTempItemName = Subdivide::GetItemNameAndQuantity(pItemName, tQuantity);

    //--Translation of the item name, if found. Because the item might not have been registered directly, we instead
    //  need to scan the translation archive.
    char *tUseItemName = (char *)starmemoryalloc(sizeof(char) * STD_MAX_LETTERS);
    StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_ITEMS);
    if(rTranslation)
    {
        strcpy(tUseItemName, tTempItemName);
        StarTranslation::StringTranslateWorker(tUseItemName, rTranslation);
    }
    //--No translation active or found, use the item's name.
    else
    {
        strcpy(tUseItemName, tTempItemName);
    }

    //--Get the last-registered item icon. If it exists, put it in the string.
    if(AdventureInventory::xrLastRegisteredIcon)
    {
        //--Mark the item icon in the buffer.
        strcat(tBuffer, "[IMG0]");
        strcat(tBuffer, tUseItemName);
        if(tQuantity > 1)
        {
            char tNumBuf[128];
            sprintf(tNumBuf, "%i", tQuantity);
            strcat(tBuffer, " x");
            strcat(tBuffer, tNumBuf);
        }
        strcat(tBuffer, ".");

        //--Create the notification, and put the image in it.
        NotificationPack *rNotification = RegisterNotification(tBuffer);
        rNotification->mString->AllocateImages(1);
        rNotification->mString->SetImageP(0, 0.0f, AdventureInventory::xrLastRegisteredIcon);
        rNotification->mString->CrossreferenceImages();
    }
    //--Otherwise, don't put an icon in the buffer.
    else
    {
        strcat(tBuffer, tUseItemName);
        strcat(tBuffer, ".");
        RegisterNotification(tBuffer);
    }

    //--Clean.
    free(tUseItemName);
    free(tTempItemName);
}
void AdventureLevel::ReresolveNotificationPositions()
{
    ///--[Documentation]
    //--Figures out where all the packages need to be vertically and orders them to move there. Whenever
    //  the list increases or decreases in size, the packages need to move.
    float tYPosition = ADLEV_NOTIF_YPOS;
    float cHei = Images.Data.rNotificationFont->GetTextHeight();
    NotificationPack *rPack = (NotificationPack *)mNotifications->PushIterator();
    while(rPack)
    {
        //--Order a move.
        rPack->mYPosition.MoveTo(tYPosition, 15);

        //--Don't decrement the order if the notification is sliding off the screen. This causes
        //  some amount of overlap.
        if(rPack->mTimer < rPack->mTimerMax - ADLEV_NOTIF_SLIDEOUT_TICKS)
        {
            tYPosition = tYPosition - cHei;
        }

        //--Next.
        rPack = (NotificationPack *)mNotifications->AutoIterate();
    }
}

///========================================== Update ==============================================
void AdventureLevel::UpdateNotifications()
{
    ///--[Documentation]
    //--All notifications update at the same time and are registered in order of head to tail. When
    //  a notification's timer expires, it is removed.
    bool tAnyRemovals = false;
    NotificationPack *rPack = (NotificationPack *)mNotifications->SetToHeadAndReturn();
    while(rPack)
    {
        //--Run Y position easing pack.
        rPack->mYPosition.IncrementQuadOut();

        //--Run timer, remove if it expires.
        rPack->mTimer ++;
        if(rPack->mTimer >= rPack->mTimerMax)
        {
            tAnyRemovals = true;
            mNotifications->RemoveRandomPointerEntry();
        }
        //--In addition, re-run the position handler on this tick, it makes things look cooler.
        else
        {

            if(rPack->mTimer == rPack->mTimerMax - ADLEV_NOTIF_SLIDEOUT_TICKS) tAnyRemovals = true;
        }

        //--Next.
        rPack = (NotificationPack *)mNotifications->IncrementAndGetRandomPointerEntry();
    }

    //--If any entries were removed, reposition all entries.
    if(tAnyRemovals) ReresolveNotificationPositions();
}

///========================================== Drawing =============================================
void AdventureLevel::RenderNotifications()
{
    ///--[Documentation]
    //--Render notifications in the bottom left, starting as the head and moving up.
    float cXPosition = ADLEV_NOTIF_XPOS;

    //--Rendering iteration.
    NotificationPack *rPack = (NotificationPack *)mNotifications->PushIterator();
    while(rPack)
    {
        //--Resolve position and alpha. First, if this is a slide-in case:
        if(rPack->mTimer < ADLEV_NOTIF_SLIDEIN_TICKS)
        {
            //--Compute percent.
            float cPct = EasingFunction::QuadraticOut(rPack->mTimer, ADLEV_NOTIF_SLIDEIN_TICKS);

            //--Positions.
            float tUseXPos = cXPosition + ((1.0f - cPct) * ADLEV_NOTIF_SLIDEIN_XDIST);

            //--Alpha.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);

            //--Render.
            rPack->mString->DrawText(tUseXPos, rPack->mYPosition.mXCur, SUGARFONT_NOCOLOR, 1.0f, Images.Data.rNotificationFont);

            //--Clean.
            StarlightColor::ClearMixer();
        }
        //--This is a slide-out case:
        else if(rPack->mTimer >= rPack->mTimerMax - ADLEV_NOTIF_SLIDEOUT_TICKS)
        {
            //--Compute percent.
            int cBase = rPack->mTimerMax - ADLEV_NOTIF_SLIDEOUT_TICKS;
            float cPct = EasingFunction::QuadraticOut(rPack->mTimer - cBase, ADLEV_NOTIF_SLIDEOUT_TICKS);

            //--Positions.
            float tUseXPos = cXPosition + (cPct * ADLEV_NOTIF_SLIDEOUT_XDIST);

            //--Alpha.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f - cPct);

            //--Render.
            rPack->mString->DrawText(tUseXPos, rPack->mYPosition.mXCur, SUGARFONT_NOCOLOR, 1.0f, Images.Data.rNotificationFont);

            //--Clean.
            StarlightColor::ClearMixer();
        }
        //--Neutral case:
        else
        {
            rPack->mString->DrawText(cXPosition, rPack->mYPosition.mXCur, SUGARFONT_NOCOLOR, 1.0f, Images.Data.rNotificationFont);
        }

        //--Next.
        rPack = (NotificationPack *)mNotifications->AutoIterate();
    }
}
