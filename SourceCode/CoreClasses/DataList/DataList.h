///========================================= DataList =============================================
//--A class that consists of a single, StarLinkedList implementation plus some functions to
//  operate on it.  Any class which inherits this class has access to decentralized data storage
//  in a standardized fashion.
//--The mStrictFlag specifies whether or not a variable must be defined before it can be Set.
//  If it's true, the variable will not be created when the Set function is called, and an error
//  is barked instead.  If it's false, Set will create a new variable if it wasn't found.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define DATALIST_ERROR_NONE 0
#define DATALIST_ERROR_NOTFOUND 1

///========================================== Classes =============================================
class DataList
{
    private:

    protected:
    //--System
    bool mStrictFlag;
    bool mErrorFlag;

    //--Storage
    StarLinkedList *mDataList;

    public:
    //--System
    DataList();
    virtual ~DataList();

    //--Public Variables
    //--Property Queries
    virtual int GetDataErrorCode();
    virtual int GetDataListSize();
    virtual void *FetchDataEntry(const char *pName);

    //--Manipulators
    void SetStrictFlag(bool pFlag);
    virtual void ResetErrorCode();
    virtual void DefineEntry(const char *pName, void *pPtr, DeletionFunctionPtr pDeletionPtr);
    virtual void AddDataEntry(const char *pName, void *pPtr, DeletionFunctionPtr pDeletionPtr);
    virtual void AddDataEntry(const char *pName, void *pPtr);
    virtual void AddDataEntry(const char *pName, void *pPtr, bool pBlocker);
    virtual void AddDataEntryN(const char *pName, float pNumber);
    virtual void AddDataEntryS(const char *pName, const char *pString);

    //--Core Methods
    virtual void ClearDataList();

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_DataList_Define(lua_State *L);
int Hook_DataList_SetData(lua_State *L);
int Hook_DataList_GetData(lua_State *L);
