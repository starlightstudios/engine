///============================= AdventureLevelGeneratorStructures ================================
///--[Forward Declarations]
class ALGRoom;
#include <stdlib.h>
#include <stdint.h>
#include "Structures.h"

///===================================== Local Structures =========================================
///--[WallGrid]
//--Handles matching up textures to collisions.
typedef struct WallGrid
{
    //--Members.
    bool mClipMatch[3][3];
    uint16_t mFastMatch;
    bool mIsFloor;
    TwoDimensionReal *rWallUVPtr;
    TwoDimensionReal *rWallHiUVPtr;
    TwoDimensionReal *rBlackoutUVPtr;

    //--Initialize.
    void Initialize()
    {
        for(int x = 0; x < 3; x ++)
        {
            for(int y = 0; y < 3; y ++)
            {
                mClipMatch[x][y] = true;
            }
        }
        mIsFloor = false;
        rWallUVPtr = NULL;
        rWallHiUVPtr = NULL;
        rBlackoutUVPtr = NULL;
    }

    //--Fast-Matcher
    static int BuildFastMatch(bool pGrid[3][3])
    {
        uint16_t tStartVal = 0x0000;
        if(pGrid[0][0]) tStartVal |= 0x0001;
        if(pGrid[1][0]) tStartVal |= 0x0002;
        if(pGrid[2][0]) tStartVal |= 0x0004;
        if(pGrid[0][1]) tStartVal |= 0x0008;
        if(pGrid[1][1]) tStartVal |= 0x0010;
        if(pGrid[2][1]) tStartVal |= 0x0020;
        if(pGrid[0][2]) tStartVal |= 0x0040;
        if(pGrid[1][2]) tStartVal |= 0x0080;
        if(pGrid[2][2]) tStartVal |= 0x0100;
        return tStartVal;
    }
}WallGrid;

///--[IntPoint2D]
//--Tile pathing structure.
typedef struct IntPoint2D
{
    int mX;
    int mY;
    int mDistance;
}IntPoint2D;

///--[ALGEnemyPath]
typedef struct ALGEnemyPath
{
    int mEntriesTotal;
    int *mTilesX;
    int *mTilesY;

    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        ALGEnemyPath *rPtr = (ALGEnemyPath *)pPtr;
        free(rPtr->mTilesX);
        free(rPtr->mTilesY);
        free(rPtr);
    }
}ALGEnemyPath;

///--[ALGScriptData]
typedef struct ALGScriptData
{
    int mEntranceX;
    int mEntranceY;
    int mExitX;
    int mExitY;
    int mBonusObjectsTotal;
    int *mBonusObjectsX;
    int *mBonusObjectsY;
    int mEnemySpawnsTotal;
    int *mEnemySpawnsX;
    int *mEnemySpawnsY;
    int mEnemyPathsTotal;
    ALGEnemyPath *mEnemyPaths;
}ALGScriptData;

///===================================== Local Definitions ========================================
//--Zeroing Cases
#define ZERO_DESTRUCTOR 0x01
#define ZERO_CONSTRUCTOR 0x02

//--Generation States
#define GENERATE_NONE 0
#define GENERATE_SIZES 1
#define GENERATE_BASELAYOUT 2
#define GENERATE_TUNNELER 3
#define GENERATE_PLACEEXITS 4
#define GENERATE_PLACETREASURE 5
#define GENERATE_PLACEENEMIES 6
#define GENERATE_BUILDTEXTURES 7

//--Pseudorandom roll offsets.
#define X_QUERY 0.55f
#define Y_QUERY 0.55f
#define Z_QUERY_X_SIZE 100.75
#define Z_QUERY_Y_SIZE 101.75
#define Z_QUERY_ROOM_COUNT 102.75f
#define Z_QUERY_ROOM_TYPE 103.75f
#define Z_QUERY_ROOM_POSITION 104.75f
#define Z_QUERY_ROOM_RADIUS 105.75f
#define Z_QUERY_FLOOR_TILE 106.75f
#define Z_QUERY_FLOOR_PIT 107.75f
#define Z_QUERY_SPAWNEXIT 108.75f
#define Z_QUERY_ISCAVERN 109.75f
#define Z_QUERY_ENEMY 110.75f
#define Z_QUERY_ENEMY_PLACEMENT 111.75f
#define Z_QUERY_ENEMY_PATHING 112.75f
#define Z_QUERY_CROSSBAR 113.75f
#define Z_QUERY_CROSSBAR_LEN 114.75f
#define Z_QUERY_CROSSBAR_ROTATION 115.75f
#define Z_QUERY_CROSSBAR_LINK 116.75f

//--Grid Cases
#define WALLGRIDS_TOTAL 256

//--Mark Tile Flags
#define ALLOW_PITS 0x01

//--Layer Definitions
#define LAYER_FLOOR_0 0
#define LAYER_FLOOR_1 1
#define LAYER_FLOOR_2 2
#define LAYER_WALLS_0 3
#define LAYER_WALLS_1 4
#define LAYER_WALLSHI_0 5
#define LAYER_WALLSHI_1 6
#define LAYER_BLACKOUT 7
#define LAYERS_TOTAL 8
