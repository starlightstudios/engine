//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"
#include "Pathrunner.h"
#include "TileLayer.h"

//--CoreClasses
//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "EntityManager.h"

///--[Local Definitions]
#define MAXIMUM_CHASE_TICKS 720       //How long the AI will search before leashing back to start
#define EXTERNAL_GUARD_COOLDOWN 1400  //How long the AI must wait before it can be activated by a friend. Prevents "spotting chains".
#define SEARCH_LAST_SPOT_TICKS 120    //How long the AI will search the last seen spot before checking other nearby spots
#define ITERATIONS_PER_CYCLE 25       //Number of path iterations per tick.
#define CHASE_WAIT_TICKS 60           //How long the AI stops at each searched position before rolling a new one.
#define CHASE_SPEED_FACTOR 1.05f      //Speed factor when actively chasing a player
#define CHASE_FAST_SPEED_FACTOR 1.90f //Speed factor when chasing a player for fast monsters
#define SEARCH_SPEED_FACTOR 0.40f     //Speed factor when searching last seen location
#define SEARCH_DISTANCE_LO 3          //In tiles, minimum range from last seen position to search, radially
#define SEARCH_DISTANCE_HI 7          //In tiles, maximum range from last seen position to search, radially
#define SEARCH_WALK_DISTANCE 8        //In pixels, the length of each movement when searching, and therefore the fidelity of its pathing.
#define EXTERNAL_GUARD_DISTANCE 80    //In pixels, distance between entities before they will "wake up" one another in guard mode.
#define HARD_SPOTTING_TICKS 10        //How long the AI "knows" where the player is, even if they can't see them, upon breaking LOS.

///--[Debug]
//#define TA_CHASE_DEBUG
#ifdef TA_CHASE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///===================================== Worker Functions =========================================
float TilemapActor::GetChasePercent()
{
    return ((float)mChaseTimeRemaining / (float)MAXIMUM_CHASE_TICKS);
}
void TilemapActor::BeginChase()
{
    mAIState = TA_AI_CHASE;
    mChaseTimeRemaining = MAXIMUM_CHASE_TICKS;
    mLostPlayerTimer = 0;
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
    mLastSawPlayerX = tPlayerX;
    mLastSawPlayerY = tPlayerY;
}
void TilemapActor::EndChase()
{
    BeginLeashing();
}

///========================================== Basic AI ============================================
void TilemapActor::HandleChaseAI()
{
    ///--[Documentation and Setup]
    //--Handles chasing after the player. The entity will run towards the player and try to start a battle. Alternately, a different
    //  AI may take over if the AI type is TA_AI_USE_GUARD_AI.
    if(mEnemyAIType == TA_AI_USE_GUARD_AI) { HandleGuardChaseAI(); return; }

    //--Flags.
    bool tIsIgnoringPlayer = IsIgnoringPlayer();
    float tRangeToPlayer = GetRangeToPlayer();

    ///--[Leashing]
    //--Range check the player. A player that runs away will eventually trigger leashing logic.
    float tRangeToLeash = GetPlanarDistance(mTrueX, mTrueY, mLeashX, mLeashY);

    //--How far to leash is based on whether the enemy is "Relentless" or not. Relentless doubles the distance.
    float cLeashDistance = 128.0f;
    if(mIsRelentless) cLeashDistance = cLeashDistance * 2.0f;

    //--How fast we chase the player is based on the "Fast" flag. Increase speed if it's flipped.
    float cSpeedFactor = CHASE_SPEED_FACTOR;
    if(mIsFast) cSpeedFactor = CHASE_FAST_SPEED_FACTOR;

    //--If the player leaves this range, begin leashing.
    if(tRangeToPlayer >= cLeashDistance || tIsIgnoringPlayer)
    {
        EndChase();
        return;
    }
    //--Alternately, if we get far enough from the leash point, also leash back to it.
    else if(tRangeToLeash >= cLeashDistance)
    {
        EndChase();
        return;
    }

    ///--[Chasing]
    //--Chase the player down!
    float tOldTrueX = mTrueX;
    float tOldTrueY = mTrueY;

    //--Flags.
    AdventureLevel::xClosestEnemy = 0.0f;

    //--Alert timer. Only runs if advanced spotting is active.
    int cTimerCap = (int)(TARUI_ALERT_TPF * mAlertFramesTotal);
    if(mAlertTimer < cTimerCap) mAlertTimer ++;

    //--Player position.
    float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--Move towards the player's position.
    HandleMoveTo(tPlayerX, tPlayerY, mMoveSpeed * cSpeedFactor);

    //--Store the position if it's not the same as the previous position.
    if(mTrueX != tOldTrueX || mTrueY != tOldTrueY)
    {
        SetMemoryData(__FILE__, __LINE__);
        TwoDimensionReal *nNewLeashPoint = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
        nNewLeashPoint->Set(mTrueX, mTrueY, 1.0f, 1.0f);
        mLeashList->AddElementAsTail("X", nNewLeashPoint, &FreeThis);
    }

    //--Store the true facing.
    mTrueFacing = mFacing;

    ///--[Allies]
    //--Order who we're following to chase!
    if(mFollowTarget)
    {
        //--If the follow target does not exist, stop here.
        RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowTarget);
        if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            rActor->ActivateTeamChase();
        }
    }

    //--If we have anyone following us, order them to chase as well.
    void *rDummyPtr = mFollowerNameListing->PushIterator();
    while(rDummyPtr)
    {
        //--Use the name of the iterator, not the value.
        RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowerNameListing->GetIteratorName());
        if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            rActor->ActivateTeamChase();
        }

        //--Next.
        rDummyPtr = mFollowerNameListing->AutoIterate();
    }
}

///========================================== Guard AI ============================================
void TilemapActor::HandleGuardChaseAI()
{
    ///--[Documentation and Setup]
    //--This AI is a bit smarter than the basic AI, which chases directly after the player and tends
    //  to get stuck on the environment (which is on purpose). The guard AI still chases the player
    //  in a straight line if it can see them, but if it can't, will instead go to the last spot it
    //  saw the player.
    //--If it can't find the player, it will search nearby tiles. This AI runs on a timer before leashing
    //  instead of distance like the basic AI. That timer is set to full when it can see the player,
    //  and decreases when LOS is broken.
    //--This AI can theoretically be 'pulled' a long distance from where it starts. The leashing AI
    //  should be able to handle pathing back to its origin.
    DebugPush(true, "AI begins guard chase AI.\n");

    //--How fast we chase the player is based on the "Fast" flag. Increase speed if it's flipped.
    float cSpeedFactor = CHASE_SPEED_FACTOR;
    if(mIsFast) cSpeedFactor = CHASE_FAST_SPEED_FACTOR;

    ///--[Ignore Check]
    //--If the entity started ignoring the player for any reason, immediately leash. This can happen
    //  if the player enters an invis zone or a script call changes the ignore properties.
    if(IsIgnoringPlayer())
    {
        EndChase();
        DebugPop("Ignoring player. Ending chase AI.\n");
        return;
    }

    ///--[Basic Sight Check]
    //--If the enemy can currently see the player, move directly towards them. This is the same as the
    //  basic chase AI.
    //--The sight cone becomes 3x longer during chasing.
    bool cIsPlayerInSightCone = IsPlayerInSightCone(TA_USE_COMPUTED_VIEWDIST);
    if((cIsPlayerInSightCone) || mHardChaseTimer > 0)
    {
        //--Flags.
        AdventureLevel::xClosestEnemy = 0.0f;

        //--Alert timer. Only runs if advanced spotting is active.
        int cTimerCap = (int)(TARUI_ALERT_TPF * mAlertFramesTotal);
        if(mAlertTimer < cTimerCap) mAlertTimer ++;

        //--If the player is in the sight cone, set the hard chase timer up.
        if(cIsPlayerInSightCone)
        {
            mHardChaseTimer = HARD_SPOTTING_TICKS;
        }
        //--Otherwise, decrement the hard chase.
        else if(mHardChaseTimer > 0)
        {
            mHardChaseTimer --;
        }

        //--Set the chase time flag to its cap.
        mChaseTimeRemaining = MAXIMUM_CHASE_TICKS;
        mLostPlayerTimer = 0;
        mChaseWaitTicks = 1;

        //--Player position. Track where we last saw them.
        float tPlayerX=0.0f, tPlayerY=0.0f, tPlayerZ=0.0f;
        GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
        mLastSawPlayerX = tPlayerX;
        mLastSawPlayerY = tPlayerY;

        //--Move towards the player's position.
        HandleMoveTo(tPlayerX, tPlayerY, mMoveSpeed * cSpeedFactor);

        //--Cause nearby allies to switch into guard mode.
        GuardActivateNearbyEnemies();
        GuardActivateCommandEnemies();

        //--Store the true facing.
        mTrueFacing = mFacing;
        DebugPop("Player in sight. Performing basic chase.\n");
        return;
    }

    ///--[Not in LOS]
    //--Run this timer.
    mChaseTimeRemaining --;
    mLostPlayerTimer ++;
    mCommandIssueCooldown = 0;

    //--Decrement the spotting timer.
    if(mAlertTimer > 0)
    {
        mAlertTimer --;
    }

    //--End the chase.
    if(mChaseTimeRemaining < 1)
    {
        EndChase();
        DebugPop("Ending chase, time ran out.\n");
        return;
    }

    //--Cause nearby allies to switch into guard mode.
    GuardActivateNearbyEnemies();

    //--For SEARCH_LAST_SPOT_TICKS ticks, the AI will build the shortest possible path to the last spotted position
    //  and move there.
    if(mLostPlayerTimer < SEARCH_LAST_SPOT_TICKS)
    {
        //--Always reset this timer.
        mChaseWaitTicks = 1;

        //--Are we at the last position?
        float cDistance = GetPlanarDistance(mTrueX, mTrueY, mLastSawPlayerX, mLastSawPlayerY);
        if(cDistance < SEARCH_WALK_DISTANCE)
        {
            DebugPrint("At least seen position.\n");
        }
        //--Move to it.
        else
        {
            GuardPathTo(mLastSawPlayerX, mLastSawPlayerY, SEARCH_SPEED_FACTOR);
            mTrueFacing = mFacing;
            DebugPrint("Checking position %i %i\n", mLastSawPlayerX, mLastSawPlayerY);
        }
    }
    //--Pick random spots near the last spotted position and search those.
    else
    {
        //--If waiting at this position, do nothing.
        if(mChaseWaitTicks > 0)
        {
            //--Timer.
            mChaseWaitTicks --;

            //--If the timer hits zero, roll a position to move to.
            if(mChaseWaitTicks < 1)
            {
                //--Reset the position in case it fails to resolve.
                mChaseSearchX = mLastSawPlayerX;
                mChaseSearchY = mLastSawPlayerY;

                //--Run a subroutine to roll a search position.
                RollGuardSearchPosition();
            }
        }
        //--Otherwise, path to the requested position.
        else
        {
            if(GuardPathTo(mChaseSearchX, mChaseSearchY, SEARCH_SPEED_FACTOR))
            {
                mChaseWaitTicks = CHASE_WAIT_TICKS;
            }
            mTrueFacing = mFacing;
        }
    }
    DebugPop("Investigating player position. Guard AI ends.\n");
}

///================================= Guard AI Worker Functions ====================================
void TilemapActor::RollGuardSearchPosition()
{
    ///--[Documentation and Setup]
    //--Attempts to find a location the AI should search to find the player. This will be a location within 7 tiles
    //  radially, rolled at random. The AI will make several attempts, failing if the rolled position is clipped.
    //  The AI might entirely fail, in which case the AI will stand still until the next roll attempt.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();

    //--Iterate,
    int tTries = 5;
    while(tTries > 0)
    {
        //--Decrement.
        tTries --;

        //--Roll an angular offset and distance. The distance is between 3 and 7 tiles.
        int cAngle = (int)(GetRandomPercent() * 360.0f) * TORADIAN;
        int cDistance = ((int)(GetRandomPercent() * (SEARCH_DISTANCE_HI - SEARCH_DISTANCE_LO)) + SEARCH_DISTANCE_LO) * TileLayer::cxSizePerTile;

        //--Get this as a pixel position.
        int cGuardCheckX = (int)(mLastSawPlayerX + (cosf(cAngle) * cDistance));
        int cGuardCheckY = (int)(mLastSawPlayerY + (sinf(cAngle) * cDistance));

        //--Position is unclipped, so try to move to it.
        if(!rActiveLevel->GetClipAt(cGuardCheckX, cGuardCheckY, 0))
        {
            mChaseSearchX = cGuardCheckX;
            mChaseSearchY = cGuardCheckY;
            return;
        }
    }
}
void TilemapActor::GuardActivateNearbyEnemies()
{
    ///--[Documentation And Setup]
    //--Enemies who are in guard mode, either chasing or searching, will activate other entities who are
    //  also in guard mode, giving them the same search timer and location. If the target is already active,
    //  does nothing to them.
    StarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

    //--Iterate.
    RootEntity *rEntity = (RootEntity *)rEntityList->PushIterator();
    while(rEntity)
    {
        ///--[Non Applicable Checks]
        //--Skip non-TilemapActors.
        if(!rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            rEntity = (RootEntity *)rEntityList->AutoIterate();
            continue;
        }

        //--Skip this entity, can't call ourselves.
        if(rEntity == this)
        {
            rEntity = (RootEntity *)rEntityList->AutoIterate();
            continue;
        }

        ///--[Set]
        //--Cast.
        TilemapActor *rOtherEntity = (TilemapActor *)rEntity;

        //--Check if we're close.
        float cOtherX = rOtherEntity->GetWorldX();
        float cOtherY = rOtherEntity->GetWorldY();
        float cDistance = GetPlanarDistance(mTrueX, mTrueY, cOtherX, cOtherY);
        if(cDistance <= EXTERNAL_GUARD_DISTANCE)
        {
            //--Run the function on the target. It will determine internally if it should ignore the call.
            rOtherEntity->ActivateGuardMode(mLastSawPlayerX, mLastSawPlayerY, mChaseTimeRemaining, false);
        }

        ///--[Next]
        rEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
void TilemapActor::GuardActivateCommandEnemies()
{
    ///--[Documentation and Setup]
    //--Entities with commander lists will cause all the units on it to chase the target.

    //--Cooling down.
    if(mCommandIssueCooldown > 0)
    {
        mCommandIssueCooldown --;
        return;
    }

    //--Order all units commanded by this entity to activate guard mode.
    mCommandIssueCooldown = TA_COMMAND_RESET;

    //--Issue order to all commanded units.
    void *rCheckPtr = mCommandsNameListing->PushIterator();
    while(rCheckPtr)
    {
        //--Get name.
        const char *rName = mCommandsNameListing->GetIteratorName();

        //--Find the entity.
        RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(rName);
        if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rCheckActor = (TilemapActor *)rCheckEntity;
            rCheckActor->ActivateGuardMode(mLastSawPlayerX, mLastSawPlayerY, MAXIMUM_CHASE_TICKS, true);
        }

        //--Next.
        rCheckPtr = mCommandsNameListing->AutoIterate();
    }
}
void TilemapActor::ActivateGuardMode(int pLastSeenX, int pLastSeenY, int pSearchTimerCap, bool pOverrideTimer)
{
    ///--[Documentation and Setup]
    //--Called when a nearby entity is in guard mode, causes this entity to also go into guard mode. Does nothing
    //  if the entity is not an enemy who can do guard mode.
    if(!mIsEnemy) return;
    if(mEnemyAIType != TA_AI_USE_GUARD_AI) return;

    //--If the enemy is cooling down from going into guard mode, ignore this. This can be overridden by the commander.
    if(mExternalGuardModeCooldown > 0 && !pOverrideTimer) return;
    mExternalGuardModeCooldown = EXTERNAL_GUARD_COOLDOWN;

    //--Activate guard mode.
    BeginChase();
    mLastSawPlayerX = pLastSeenX;
    mLastSawPlayerY = pLastSeenY;
    mChaseTimeRemaining = pSearchTimerCap;
    if(pOverrideTimer) mHardChaseTimer = TA_COMMAND_RESET + 1;
}
bool TilemapActor::GuardPathTo(float pX, float pY, float pSpeedFactor)
{
    ///--[ ====== Documentation and Setup ===== ]
    //--Path to the given position, setting up the chase pathrunner if needed. Returns true if the point
    //  has been reached, false if not.

    ///--[ ============ Build Path ============ ]
    //--Build the instruction list if needed.
    if(!mChaseInstructions)
    {
        //--If the leash Pathrunner isn't created yet, do that. Set it to home in on the leash point.
        if(!mChasePathrunner)
        {
            //--Make the leash target a multiple of SEARCH_WALK_DISTANCE.
            int tXTarget = (int)((pX / SEARCH_WALK_DISTANCE) * SEARCH_WALK_DISTANCE);
            int tYTarget = (int)((pY / SEARCH_WALK_DISTANCE) * SEARCH_WALK_DISTANCE);

            //--Run.
            mChasePathrunner = new Pathrunner();
            mChasePathrunner->SetIterationsPerCycle(ITERATIONS_PER_CYCLE);
            mChasePathrunner->SetWalkDistance(SEARCH_WALK_DISTANCE);
            mChasePathrunner->Initialize(mTrueX, mTrueY, tXTarget, tYTarget);
        }

        //--Run the pathrunner. It can legally return NULL.
        StarLinkedList *tResultList = mChasePathrunner->RunPathingIntoList();

        //--If the path runner failed for some reason, stop here. Pass back true so the AI can at least
        //  pretend to function.
        if(mChasePathrunner->IsFailed())
        {
            delete tResultList;
            delete mChasePathrunner;
            mChasePathrunner = NULL;
            return true;
        }

        //--If the path runner didn't have a path yet, we can run it again until it does.
        if(!tResultList)
        {
            return false;
        }

        ///--[Success, Store]
        //--The path list was passed back successfully. Store it.
        delete mChaseInstructions;
        mChaseInstructions = tResultList;

        //--Clean up the path runner.
        delete mChasePathrunner;
        mChasePathrunner = NULL;

        //--Flags.
        mChaseWalkDist = SEARCH_WALK_DISTANCE;
    }

    ///--[ =========== Execute Path =========== ]
    //--The AI has a path ready. Move to it.

    ///--[Resolve Movement]
    //--Zeroth entry. If it doesn't exist, assume the move is complete.
    int *rDirection = (int *)mChaseInstructions->GetElementBySlot(0);
    if(!rDirection)
    {
        delete mChaseInstructions;
        mChaseInstructions = NULL;
        return true;
    }

    //--Setup.
    float tTargetX = mTrueX;
    float tTargetY = mTrueY;

    //--Resolve the target position.
    if(*rDirection == DIR_NORTH) tTargetY = tTargetY - mChaseWalkDist;
    if(*rDirection == DIR_SOUTH) tTargetY = tTargetY + mChaseWalkDist;
    if(*rDirection == DIR_WEST)  tTargetX = tTargetX - mChaseWalkDist;
    if(*rDirection == DIR_EAST)  tTargetX = tTargetX + mChaseWalkDist;
    mFacing = *rDirection;

    ///--[Movement Finished]
    //--Move to the target. If the move completes:
    int tStoreMove = mMoveTimer;
    if(HandleMoveTo(tTargetX, tTargetY, mMoveSpeed * pSpeedFactor))
    {
        //--Pop the movement list.
        mChaseInstructions->DeleteHead();

        //--Reset the distance variable.
        mChaseWalkDist = SEARCH_WALK_DISTANCE;

        //--If there are no instructions left, we reached the leash point.
        if(mChaseInstructions->GetListSize() < 1)
        {
            delete mChaseInstructions;
            mChaseInstructions = NULL;
            return true;
        }
    }
    ///--[Movement Incomplete]
    //--Not at the location yet.
    else
    {
        //--Flags.
        mIsMoving = true;
        if(mMoveTimer != tStoreMove) mMoveTimer = tStoreMove + 1;

        //--Get the distance between our position and the target. Store that as the distance remaining.
        mChaseWalkDist = GetPlanarDistance(mTrueX, mTrueY, tTargetX, tTargetY);
    }

    //--Not done yet.
    return false;
}
