//--Base
#include "StarFont.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
///--[Local Definitions]
//--Downfade
#define DOWNFADE_BEGIN_DEFAULT 1.50f
#define DOWNFADE_END_DEFAULT 0.50f

//--Edging
#define EDGES_MAX 3
#define EDGES_ARRAY_LEN 64 //Calibrated value for 3 edges
#define EDGE_INTERNAL_CUTOFF 32
#define EDGE_ALPHA_CUTOFF 32
#define EDGE_COLOR_ADJACENT 0xFF000000
#define EDGE_COLOR_DIAGONAL 0x40000000

///--[Private Statics]
//--Amount that the glyphs will darken as the Y value increases.
float StarFont::xDownfadeBegin = DOWNFADE_BEGIN_DEFAULT;
float StarFont::xDownfadeEnd = DOWNFADE_END_DEFAULT;

//--Minimum alpha to be considered a pixel when using black edging.
int StarFont::xBlackOutlineCutoff = EDGE_INTERNAL_CUTOFF;

//--How many pixels to outline with.
int StarFont::xOutlineSize = 1;

//--Monospacing, mandates the next created font ignores kerning. Set to -1.0f to disable.
float StarFont::xNextMonospace = -1.0f;

///======================================= Manipulators ===========================================
void StarFont::SetDownfade(float pStart, float pEnd)
{
    //--Sets how much the letters fade from top to bottom. Setting this to 0,0 means there will be
    //  no downfade, while setting it to 1,1 will mean the top-to-bottom is pure black.
    //--Pass -1.0f to reset this to the default.
    if(pStart < 0.0f || pEnd < 0.0f) { xDownfadeBegin = DOWNFADE_BEGIN_DEFAULT; xDownfadeEnd = DOWNFADE_END_DEFAULT; return; }

    //--Set and clamp.
    xDownfadeBegin = pStart;
    if(xDownfadeBegin < 0.0f) xDownfadeBegin = 0.0f;
    if(xDownfadeBegin > 1.0f) xDownfadeBegin = 1.0f;

    //--Ending value.
    xDownfadeEnd = pEnd;
    if(xDownfadeEnd < 0.0f) xDownfadeEnd = 0.0f;
    if(xDownfadeEnd > 1.0f) xDownfadeEnd = 1.0f;
}
void StarFont::SetOutlineWidth(int pPixels)
{
    //--Clamp.
    xOutlineSize = pPixels;
    if(xOutlineSize < 1) xOutlineSize = 1;
    if(xOutlineSize > EDGES_MAX) xOutlineSize = EDGES_MAX;
}
void StarFont::SetMonospacing(float pPixels)
{
    xNextMonospace = pPixels;
    if(xNextMonospace < 0.0f) xNextMonospace = -1.0f;
}

///======================================= Core Methods ===========================================
///--[Black Outline Package]
typedef struct
{
    int mXOffset;
    int mYOffset;
    uint32_t mColor;
}EdgePack;

///--[Implementation]
void StarFont::ApplyBlackOutline(int pXSize, int pYSize, uint32_t *pTextureData)
{
    //--Modifies the provided texture data such that all glyphs will have a 1-pixel wide black line around them.
    //  This makes the character pop out. The code should be set to respect the black line's texel requirements.
    //--Texture data is expected to be in RGBA8888 format.
    if(pXSize < 1 || pYSize < 1 || !pTextureData) return;

    //--Clone the texture data.
    SetMemoryData(__FILE__, __LINE__);
    uint32_t *tTextureDataClone = (uint32_t *)starmemoryalloc(sizeof(uint32_t) * pXSize * pYSize);
    memcpy(tTextureDataClone, pTextureData, sizeof(uint32_t) * pXSize * pYSize);

    //--Static List Setup
    static bool xHasBuiltLists = false;
    static int xEdgeCounts[EDGES_MAX];
    static int xEdgeStarts[EDGES_MAX];
    static EdgePack xEdgingLookups[EDGES_ARRAY_LEN];
    if(!xHasBuiltLists)
    {
        //--[Constants and Setup]
        //--Flag.
        xHasBuiltLists = true;

        //--Setup.
        int tTotalCounter = 0;

        //--[Autobuild]
        for(int i = 0; i < EDGES_MAX; i ++)
        {
            //--Variables.
            int tAdded = 0;
            xEdgeStarts[i] = tTotalCounter;

            //--Constants.
            int cDistance = i + 1;
            int cBlackDistance = i+1;
            int cGreyDistance = i+2;

            //--Iterate across the grid.
            for(int x = -cDistance; x <= cDistance; x ++)
            {
                for(int y = -cDistance; y <= cDistance; y ++)
                {
                    //--Skip the middle pixel.
                    if(x == 0 && y == 0) continue;

                    //--Low Radial distance: Black.
                    if(abs(x) + abs(y) <= cBlackDistance)
                    {
                        xEdgingLookups[tTotalCounter].mXOffset = x;
                        xEdgingLookups[tTotalCounter].mYOffset = y;
                        xEdgingLookups[tTotalCounter].mColor = EDGE_COLOR_ADJACENT;
                        tTotalCounter ++;
                        tAdded ++;
                    }
                    //--Medium Radial distance: Grey.
                    else if(abs(x) + abs(y) <= cGreyDistance)
                    {
                        xEdgingLookups[tTotalCounter].mXOffset = x;
                        xEdgingLookups[tTotalCounter].mYOffset = y;
                        xEdgingLookups[tTotalCounter].mColor = EDGE_COLOR_DIAGONAL;
                        tTotalCounter ++;
                        tAdded ++;
                    }
                }
            }

            //--Store how many edge pixels got added.
            xEdgeCounts[i] = tAdded;
        }

        //--[Report]
        //fprintf(stderr, "Total slots used %i\n", tTotalCounter);
    }


    //--Iterate across the texture data.
    for(int x = xOutlineSize; x < pXSize-xOutlineSize; x ++)
    {
        for(int y = xOutlineSize; y < pYSize-xOutlineSize; y ++)
        {
            //--Calculate the position for each pixel, get the color values out.
            int tPosition = x + (y * pXSize);
            uint8_t tAlp = (pTextureData[tPosition] & 0xFF000000) >> 24;

            //--Skip all pixels with an alpha of 64 or greater.
            if(tAlp >= EDGE_ALPHA_CUTOFF) continue;

            //--Iterate across the list for the given outline size.
            for(int i = 0; i < xEdgeCounts[xOutlineSize-1]; i ++)
            {
                //--Add the offset value for this pixel.
                int tUseLookup = xEdgeStarts[xOutlineSize-1] + i;
                int tXPos = x + xEdgingLookups[tUseLookup].mXOffset;
                int tYPos = y + xEdgingLookups[tUseLookup].mYOffset;

                //--Get the pixel in question.
                int tPixelPosition = tXPos + (tYPos * pXSize);
                uint8_t tAlpha = (pTextureData[tPixelPosition] & 0xFF000000) >> 24;

                //--If the alpha at this pixel is above the cutoff, this pixel becomes the color listed.
                if(tAlpha >= EDGE_ALPHA_CUTOFF)
                {
                    //--Set color.
                    tTextureDataClone[tPosition] = xEdgingLookups[tUseLookup].mColor;

                    //--If we set a black, there is no need to keep iterating since it can't get blacker than black.
                    if(xEdgingLookups[tUseLookup].mColor == EDGE_COLOR_ADJACENT) break;
                }
            }



            /*
            //--This pixel is transparent enough. Check the adjacent pixels. First the 1-pixel version.
            if(xOutlineSize == 1)
            {
                //--Four immediately adjacent positions.
                int tPositionL = (x-1) + ((y+0) * pXSize);
                int tPositionT = (x+0) + ((y-1) * pXSize);
                int tPositionR = (x+1) + ((y+0) * pXSize);
                int tPositionB = (x+0) + ((y+1) * pXSize);
                uint8_t tAlpL = (pTextureData[tPositionL] & 0xFF000000) >> 24;
                uint8_t tAlpT = (pTextureData[tPositionT] & 0xFF000000) >> 24;
                uint8_t tAlpR = (pTextureData[tPositionR] & 0xFF000000) >> 24;
                uint8_t tAlpB = (pTextureData[tPositionB] & 0xFF000000) >> 24;

                //--Any match: Black.
                if(tAlpL >= EDGE_ALPHA_CUTOFF || tAlpT >= EDGE_ALPHA_CUTOFF || tAlpR >= EDGE_ALPHA_CUTOFF || tAlpB >= EDGE_ALPHA_CUTOFF)
                {
                    tTextureDataClone[tPosition] = EDGE_COLOR_ADJACENT;
                    continue;
                }

                //--Corner checks for partial blacking.
                int tPositionTL = (x-1) + ((y-1) * pXSize);
                int tPositionTR = (x+1) + ((y-1) * pXSize);
                int tPositionBL = (x-1) + ((y+1) * pXSize);
                int tPositionBR = (x+1) + ((y+1) * pXSize);
                uint8_t tAlpTL = (pTextureData[tPositionTL] & 0xFF000000) >> 24;
                uint8_t tAlpTR = (pTextureData[tPositionTR] & 0xFF000000) >> 24;
                uint8_t tAlpBL = (pTextureData[tPositionBL] & 0xFF000000) >> 24;
                uint8_t tAlpBR = (pTextureData[tPositionBR] & 0xFF000000) >> 24;

                //--Any match: Black with a lower opacity.
                if(tAlpTL >= EDGE_ALPHA_CUTOFF || tAlpTR >= EDGE_ALPHA_CUTOFF || tAlpBL >= EDGE_ALPHA_CUTOFF || tAlpBR >= EDGE_ALPHA_CUTOFF)
                {
                    tTextureDataClone[tPosition] = EDGE_COLOR_DIAGONAL;
                    continue;
                }
            }
            //--Two pixel adjacency case.
            else if(xOutlineSize == 2)
            {
                //--Twelve valid positions.
                int tSetPosition = -1;
                int cPositionArray[12];
                cPositionArray[ 0] = (x-1) + ((y+0) * pXSize);
                cPositionArray[ 1] = (x-2) + ((y+0) * pXSize);
                cPositionArray[ 2] = (x+1) + ((y+0) * pXSize);
                cPositionArray[ 3] = (x+2) + ((y+0) * pXSize);
                cPositionArray[ 4] = (x+0) + ((y-1) * pXSize);
                cPositionArray[ 5] = (x+0) + ((y-2) * pXSize);
                cPositionArray[ 6] = (x+0) + ((y+1) * pXSize);
                cPositionArray[ 7] = (x+0) + ((y+2) * pXSize);
                cPositionArray[ 8] = (x-1) + ((y-1) * pXSize);
                cPositionArray[ 9] = (x-1) + ((y+1) * pXSize);
                cPositionArray[10] = (x+1) + ((y-1) * pXSize);
                cPositionArray[11] = (x+1) + ((y+1) * pXSize);
                for(int i = 0; i < 12; i ++)
                {
                    uint8_t tAlpha = (pTextureData[cPositionArray[i]] & 0xFF000000) >> 24;
                    if(tAlpha >= EDGE_ALPHA_CUTOFF) { tSetPosition = cPositionArray[i]; break; }
                }

                //--If a position is set, apply it.
                if(tSetPosition != -1)
                {
                    tTextureDataClone[tPosition] = EDGE_COLOR_ADJACENT;
                    continue;
                }

                //--Edges.
                cPositionArray[0] = (x-2) + ((y+1) * pXSize);
                cPositionArray[1] = (x-2) + ((y-1) * pXSize);
                cPositionArray[2] = (x+2) + ((y+1) * pXSize);
                cPositionArray[3] = (x+2) + ((y-1) * pXSize);
                cPositionArray[4] = (x+1) + ((y+2) * pXSize);
                cPositionArray[5] = (x-1) + ((y+2) * pXSize);
                cPositionArray[6] = (x+1) + ((y-2) * pXSize);
                cPositionArray[7] = (x-1) + ((y-2) * pXSize);
                for(int i = 0; i < 8; i ++)
                {
                    uint8_t tAlpha = (pTextureData[cPositionArray[i]] & 0xFF000000) >> 24;
                    if(tAlpha >= EDGE_ALPHA_CUTOFF) { tSetPosition = cPositionArray[i]; break; }
                }

                //--If a position is set, apply it.
                if(tSetPosition != -1)
                {
                    tTextureDataClone[tPosition] = EDGE_COLOR_DIAGONAL;
                    continue;
                }
            }
            //--Three pixel adjacency case.
            else if(xOutlineSize == 3)
            {
                //--36 valid positions.
                int tSetPosition = -1;
                int cPositionArray[36];
                cPositionArray[ 0] = (x-1) + ((y-3) * pXSize);
                cPositionArray[ 1] = (x+0) + ((y-3) * pXSize);
                cPositionArray[ 2] = (x+1) + ((y-3) * pXSize);
                cPositionArray[ 3] = (x-2) + ((y-2) * pXSize);
                cPositionArray[ 4] = (x-1) + ((y-2) * pXSize);
                cPositionArray[ 5] = (x+0) + ((y-2) * pXSize);
                cPositionArray[ 6] = (x+1) + ((y-2) * pXSize);
                cPositionArray[ 7] = (x+2) + ((y-2) * pXSize);
                cPositionArray[ 8] = (x-3) + ((y-1) * pXSize);
                cPositionArray[ 9] = (x-2) + ((y-1) * pXSize);
                cPositionArray[10] = (x-1) + ((y-1) * pXSize);
                cPositionArray[11] = (x+0) + ((y-1) * pXSize);
                cPositionArray[12] = (x+1) + ((y-1) * pXSize);
                cPositionArray[13] = (x+2) + ((y-1) * pXSize);
                cPositionArray[14] = (x+3) + ((y-1) * pXSize);
                cPositionArray[15] = (x-3) + ((y+0) * pXSize);
                cPositionArray[16] = (x-2) + ((y+0) * pXSize);
                cPositionArray[17] = (x-1) + ((y+0) * pXSize);
                cPositionArray[18] = (x+1) + ((y+0) * pXSize);
                cPositionArray[19] = (x+2) + ((y+0) * pXSize);
                cPositionArray[20] = (x+3) + ((y+0) * pXSize);
                cPositionArray[21] = (x-3) + ((y+1) * pXSize);
                cPositionArray[22] = (x-2) + ((y+1) * pXSize);
                cPositionArray[23] = (x-1) + ((y+1) * pXSize);
                cPositionArray[24] = (x+0) + ((y+1) * pXSize);
                cPositionArray[25] = (x+1) + ((y+1) * pXSize);
                cPositionArray[26] = (x+2) + ((y+1) * pXSize);
                cPositionArray[27] = (x+3) + ((y+1) * pXSize);
                cPositionArray[28] = (x-2) + ((y+2) * pXSize);
                cPositionArray[29] = (x-1) + ((y+2) * pXSize);
                cPositionArray[30] = (x+0) + ((y+2) * pXSize);
                cPositionArray[31] = (x+1) + ((y+2) * pXSize);
                cPositionArray[32] = (x+2) + ((y+2) * pXSize);
                cPositionArray[33] = (x-1) + ((y+3) * pXSize);
                cPositionArray[34] = (x+0) + ((y+3) * pXSize);
                cPositionArray[35] = (x+1) + ((y+3) * pXSize);
                for(int i = 0; i < 36; i ++)
                {
                    uint8_t tAlpha = (pTextureData[cPositionArray[i]] & 0xFF000000) >> 24;
                    if(tAlpha >= EDGE_ALPHA_CUTOFF) { tSetPosition = cPositionArray[i]; break; }
                }

                //--If a position is set, apply it.
                if(tSetPosition != -1)
                {
                    tTextureDataClone[tPosition] = EDGE_COLOR_ADJACENT;
                    continue;
                }

                //--Edges.
                cPositionArray[0] = (x-1) + ((y-4) * pXSize);
                cPositionArray[1] = (x+0) + ((y-4) * pXSize);
                cPositionArray[2] = (x+1) + ((y-4) * pXSize);
                cPositionArray[2] = (x-2) + ((y-3) * pXSize);
                cPositionArray[2] = (x+2) + ((y-3) * pXSize);
                for(int i = 0; i < 8; i ++)
                {
                    uint8_t tAlpha = (pTextureData[cPositionArray[i]] & 0xFF000000) >> 24;
                    if(tAlpha >= EDGE_ALPHA_CUTOFF) { tSetPosition = cPositionArray[i]; break; }
                }

                //--If a position is set, apply it.
                if(tSetPosition != -1)
                {
                    tTextureDataClone[tPosition] = EDGE_COLOR_DIAGONAL;
                    continue;
                }
            }*/
        }
    }

    //--Copy the clone data into the original.
    memcpy(pTextureData, tTextureDataClone, sizeof(uint32_t) * pXSize * pYSize);
    free(tTextureDataClone);
}
