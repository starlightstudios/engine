-- |[ ================================ StatGenerator Chapter 5 ================================= ]|
--Chapter 5 functions.

-- |[ ============================== StatGenerator:fnChapter5HP() ============================== ]|
--Generates an HP value given a level and grade.
function StatGenerator:fnChapter5HP(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Health consists of a flat starting bonus and then a factor. Health increases linearly by level.
    local faGradeFactors = {}
    faGradeFactors[0] = { 35, 16}      --Grade F
    faGradeFactors[1] = { 50, 19}      --Grade E
    faGradeFactors[2] = { 60, 21}      --Grade D
    faGradeFactors[3] = { 70, 25}      --Grade C
    faGradeFactors[4] = { 90, 32}      --Grade B
    faGradeFactors[5] = {110, 39}      --Grade A
    faGradeFactors[6] = {140, 47}      --Grade S
    faGradeFactors[7] = {170, 54}      --Grade S+
    faGradeFactors[8] = {190, 60}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter5Atk() ============================== ]|
--Generates an Attack value given a level and grade.
function StatGenerator:fnChapter5Atk(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Attack consists of a flat starting bonus and then a factor. Attack increases linearly by level.
    local faGradeFactors = {}
    faGradeFactors[0] = { 27,  9}      --Grade F
    faGradeFactors[1] = { 34, 11}      --Grade E
    faGradeFactors[2] = { 40, 12}      --Grade D
    faGradeFactors[3] = { 54, 14}      --Grade C
    faGradeFactors[4] = { 60, 16}      --Grade B
    faGradeFactors[5] = { 71, 19}      --Grade A
    faGradeFactors[6] = { 83, 21}      --Grade S
    faGradeFactors[7] = { 90, 25}      --Grade S+
    faGradeFactors[8] = {110, 29}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter5Ini() ============================== ]|
--Generates an Initiative value given a level and grade.
function StatGenerator:fnChapter5Ini(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Initiative scales very little with level and is primarily based on the grade.
    local faGradeFactors = {}
    faGradeFactors[0] = { 10, 1}      --Grade F
    faGradeFactors[1] = { 20, 1}      --Grade E
    faGradeFactors[2] = { 40, 2}      --Grade D
    faGradeFactors[3] = { 50, 2}      --Grade C
    faGradeFactors[4] = { 70, 2}      --Grade B
    faGradeFactors[5] = { 80, 3}      --Grade A
    faGradeFactors[6] = {100, 3}      --Grade S
    faGradeFactors[7] = {110, 4}      --Grade S+
    faGradeFactors[8] = {120, 5}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter5Acc() ============================== ]|
--Generates an Accuracy value given a level and grade.
function StatGenerator:fnChapter5Acc(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Accuracy is a relative value, and low-grades even start with negative accuracy.
    local faGradeFactors = {}
    faGradeFactors[0] = {-20,  1}      --Grade F
    faGradeFactors[1] = {-15,  2}      --Grade E
    faGradeFactors[2] = {  0,  3}      --Grade D
    faGradeFactors[3] = {  5,  4}      --Grade C
    faGradeFactors[4] = {  5,  5}      --Grade B
    faGradeFactors[5] = {  5,  6}      --Grade A
    faGradeFactors[6] = { 10,  7}      --Grade S
    faGradeFactors[7] = { 15,  8}      --Grade S+
    faGradeFactors[8] = { 20, 10}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter5Evd() ============================== ]|
--Generates an HP value given a level and grade.
function StatGenerator:fnChapter5Evd(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Evade is a relative value, and low-grades even start with negative evade. Not as severe as accuracy.
    local faGradeFactors = {}
    faGradeFactors[0] = {-10,  1}      --Grade F
    faGradeFactors[1] = { -5,  2}      --Grade E
    faGradeFactors[2] = {  5,  3}      --Grade D
    faGradeFactors[3] = {  8,  4}      --Grade C
    faGradeFactors[4] = { 10,  5}      --Grade B
    faGradeFactors[5] = { 13,  6}      --Grade A
    faGradeFactors[6] = { 19,  7}      --Grade S
    faGradeFactors[7] = { 22,  9}      --Grade S+
    faGradeFactors[8] = { 25, 12}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter5Stun() ============================== ]|
--Generates a stun value given a range. Level is typically ignored.
function StatGenerator:fnChapter5Stun(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --We typically use fixed values for stun. Stun doesn't scale with level for the player, so it
    -- doesn't scale with enemies.
    if(piGradeIndex == 0) then
        return 50
    elseif(piGradeIndex == 1) then
        return 50
    elseif(piGradeIndex == 2) then
        return 60
    elseif(piGradeIndex == 3) then
        return 80
    elseif(piGradeIndex == 4) then
        return 100
    elseif(piGradeIndex == 5) then
        return 120
    elseif(piGradeIndex == 6) then
        return 130
    elseif(piGradeIndex == 7) then
        return 130
    end
    
    --S++ is the max.
    return 150
end

-- |[ ============================== StatGenerator:fnChapter5Plt() ============================== ]|
--Generates a Platina reward value given a level and grade. Always gives 0 Plt for an F rating.
function StatGenerator:fnChapter5Plt(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Platina is generally low across the board for the chapter as there aren't that many shopping opportunities.
    local faGradeFactors = {}
    faGradeFactors[0] = {  0,  0}      --Grade F
    faGradeFactors[1] = {  3,  3}      --Grade E
    faGradeFactors[2] = {  4,  4}      --Grade D
    faGradeFactors[3] = {  5,  6}      --Grade C
    faGradeFactors[4] = {  6,  7}      --Grade B
    faGradeFactors[5] = {  8,  8}      --Grade A
    faGradeFactors[6] = {  9,  9}      --Grade S
    faGradeFactors[7] = { 10, 10}      --Grade S+
    faGradeFactors[8] = { 12, 14}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end

-- |[ ============================== StatGenerator:fnChapter5Exp() ============================== ]|
--Generates an EXP reward value given a level and grade. Always gives 0 EXP for an F rating.
function StatGenerator:fnChapter5Exp(piLevel, piGradeIndex)
    
    -- |[Automatic Range Check]|
    piLevel, piGradeIndex = self:fnCommonRangeCheck(piLevel, piGradeIndex)

    -- |[Generate]|
    --Evade is a relative value, and low-grades even start with negative evade. Not as severe as accuracy.
    local faGradeFactors = {}
    faGradeFactors[0] = {  0,  0}      --Grade F
    faGradeFactors[1] = {  3,  2}      --Grade E
    faGradeFactors[2] = {  4,  3}      --Grade D
    faGradeFactors[3] = {  5,  4}      --Grade C
    faGradeFactors[4] = {  6,  5}      --Grade B
    faGradeFactors[5] = {  8,  6}      --Grade A
    faGradeFactors[6] = {  9,  7}      --Grade S
    faGradeFactors[7] = { 10,  9}      --Grade S+
    faGradeFactors[8] = { 12, 12}      --Grade S++
    
    --Compute. Single-step, chapter 1 is not particularly complex.
    local iFinalStat = faGradeFactors[piGradeIndex][1] + (faGradeFactors[piGradeIndex][2] * piLevel)
    
    --Return
    return iFinalStat
end
