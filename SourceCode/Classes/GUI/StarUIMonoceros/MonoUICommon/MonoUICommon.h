///======================================= MonoUICommon ===========================================
//--Common UI object to all Monoceros UI sub-components.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class MonoUICommon : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    //--Timers.
    static const int cMonoVisTicks =  15;               //Standardized ticks to show/hide an object.
    static const int cMonoCurTicks =   7;               //Standardized ticks to move the cursor.
    static const int cMonoOscTicks = 120;               //Standardized tick periodicity for oscillating arrows.

    //--Sizes.
    static constexpr float cxMonoFontMainlineFontH = 22.0f; //Height, in pixels, between lines of rFont_Mainline.
    static constexpr float cxMonoBigIconOffset     = 11.0f; //Y offset for large statistic icons.
    static constexpr float cxMonoBigControlOffset  = 15.0f; //Y offset for large control icons.

    ///--[System]
    ///--[Colors]
    StarlightColor mMonoTitleColor;
    StarlightColor mMonoSubtitleColor;
    StarlightColor mMonoParagraphColor;

    protected:

    public:
    //--System
    MonoUICommon();
    virtual ~MonoUICommon();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    virtual void SetColor(const char *pColorName, float pAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


