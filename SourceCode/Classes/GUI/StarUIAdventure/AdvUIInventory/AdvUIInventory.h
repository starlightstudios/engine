///======================================= AdvUIInventory ==========================================
//--Inventory UI object. Displays items, their properties, and a description.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Timers
#define ADVUIINVENTORY_VIS_TICKS 15
#define ADVUIINVENTORY_CURSOR_MOVE_TICKS 7
#define ADVUIINVENTORY_DECONSTRUCT_TICKS 15

//--Sizes
#define ADVUIINVENTORY_ITEM_MAX_NAME_LENGTH 545.0f
#define ADVUIINVENTORY_ITEMS_PER_PAGE 14
#define ADVUIINVENTORY_ITEMS_BUFFER 3

//--Icons
#define ADVUIINVENTORY_HEADER_ICONS_TOTAL 6

///========================================== Classes =============================================
class AdvUIInventory : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    //--Timers.
    static const int cxVisTicks = 15;                   //Number of ticks to show/hide the object.
    static const int cxCursorTicks = 7;                 //Ticks to move a cursor once it repositions.
    static const int cxDeconstructTicks = 7;            //Number of ticks to show/hide deconstruction mode.

    //--Sizes.
    static const int cxScrollEdge = 3;                  //How many entries from the edge of the list before scrolling.
    static constexpr float cxMaxNameLen = 545.0f;           //How many pixels a name can be before it compresses to fit on the page.

    //--Help.
    static const int cxHelpStringsTotal = 24;           //Number of unique StarlightStrings appearing on the help menu.

    //--Local Constants
    int cItemsPerPage;                                  //How many entries can be shown on a single page before needing a scrollbar.

    ///--[System]
    //--Cursor
    int mItemCursor;
    int mItemSkip;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Deconstruction
    char *mStoredItemName;
    bool mIsDeconstructMode;
    int mDeconstructTimer;
    bool mDeconstructErrorEquipped;
    bool mDeconstructErrorNoResults;
    StarLinkedList *mDeconstructResults; //dummyptr, name of entry is the item

    ///--[Instructions]
    //--Strings
    StarlightString *mShowHelpString;
    StarlightString *mChangeSortString;
    StarlightString *mDeconstructString;
    StarlightString *mScrollFastString;
    StarlightString *mConfirmString;
    StarlightString *mCancelString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;
        StarFont *rFont_Statistic;

        //--Images
        StarBitmap *rFrame_DeconstructBox;
        StarBitmap *rFrame_Description;
        StarBitmap *rFrame_ItemListing;
        StarBitmap *rHighlight_Standard;
        StarBitmap *rOverlay_ExpandableHeader;
        StarBitmap *rOverlay_GemSlot;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_ItemListingDetail;
        StarBitmap *rOverlay_SortArrow;
        StarBitmap *rScrollbar_Static;
        StarBitmap *rScrollbar_Scroller;

        //--Common Parts
        StarBitmap *rPropertyIcons[ADVUIINVENTORY_HEADER_ICONS_TOTAL];
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIInventory();
    virtual ~AdvUIInventory();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();

    //--Deconstruction
    void UpdateDeconstruct();
    virtual void RenderDeconstruct(float pVisAlpha);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    virtual void SetSortingString(int pFlag);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    virtual void RenderInventoryListing(float pColorAlpha);
    virtual void RenderInventoryEntry(AdventureItem *pItem, const char *pOwnerName, float pYPosition, bool pIsOdd, float pColorAlpha);
    virtual void RenderInventoryDescription(AdventureItem *pItem);

    //--Pointer Routing
    StarLinkedList *GetDeconstructList();

    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


