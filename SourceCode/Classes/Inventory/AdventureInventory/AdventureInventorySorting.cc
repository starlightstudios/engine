//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

///--[Static Variables]
bool AdventureInventory::xSortListInReverse = false;

///--[Forward Declarations]
int CompareItemsByName(const void *pEntryA, const void *pEntryB);
int CompareItemsByType(const void *pEntryA, const void *pEntryB);
int CompareItemsByQuality(const void *pEntryA, const void *pEntryB);

///--[Internal Sort Functions]
int AdventureInventory::GetInternalSortFlag()
{
    return mCurrentSort;
}
void AdventureInventory::SetInternalSort(int pFlag)
{
    mCurrentSort = pFlag;
    if(mCurrentSort >= AINV_SORT_CRITERIA_MAX) mCurrentSort = 0;
    if(mCurrentSort <                       0) mCurrentSort = 0;
}
void AdventureInventory::IncrementInternalSort()
{
    mCurrentSort ++;
    if(mCurrentSort >= AINV_SORT_CRITERIA_MAX) mCurrentSort = 0;
}
void AdventureInventory::SortItemListByInternalCriteria()
{
    SortListByCriteria(mItemList, mCurrentSort, true);
}

///--[Master Function]
void AdventureInventory::SortListByCriteria(StarLinkedList *pList, int pCriteriaFlag, bool pHighestFirst)
{
    //--Given an inventory list containing AdventureItem pointers (and ONLY those!), attempts to sort
    //  the list using the provided flag. The flag is one of the AINV_SORT_CRITERIA_* pattern.
    //--When sorting lists in this manner, make sure you don't have an iterator running. That will
    //  cause undefined behavior. If you know what you're doing, you can still run this with an iterator,
    //  so the function will *not* stop you.
    //--Note: When sorting a list that isn't the mItemList, the pointers are usually referenced anyway.
    //  For example, sorting the gem list won't change anything in the item list, and when ordering
    //  equipment changes, the master item list is searched by pointer.
    if(!pList || pCriteriaFlag < 0 || pCriteriaFlag >= AINV_SORT_CRITERIA_MAX) return;

    //--Static flag to quickly switch how the sort works without requiring function duplication.
    AdventureInventory::xSortListInReverse = !pHighestFirst;

    //--Setup.
    SortFnPtr rFuncPtr = NULL;

    //--Set the sorting operation using the sorting function requested. The functions are defined and explained
    //  below here.
    if(pCriteriaFlag == AINV_SORT_CRITERIA_NAME)
    {
        rFuncPtr = &CompareItemsByName;
    }
    else if(pCriteriaFlag == AINV_SORT_CRITERIA_TYPE)
    {
        rFuncPtr = &CompareItemsByType;
    }
    else if(pCriteriaFlag == AINV_SORT_CRITERIA_VALUE)
    {
        rFuncPtr = &CompareItemsByQuality;
    }

    //--Call.
    if(rFuncPtr) pList->SortListUsing(rFuncPtr);
}

///--[Sort by Type]
int GetItemScoreByType(AdventureItem *pItem)
{
    //--Setup.
    int tItemScore = 0;
    if(!pItem) return tItemScore;

    //--Equipment gets the highest priority.
    if(pItem->IsEquipment())
    {
        if(pItem->IsEquippableIn("Weapon"))
        {
            tItemScore = 100;
        }
        else if(pItem->IsEquippableIn("Body"))
        {
            tItemScore = 99;
        }
        else if(pItem->IsEquippableIn("Accessory A"))
        {
            tItemScore = 98;
        }
        else if(pItem->IsEquippableIn("Item A"))
        {
            tItemScore = 97;
        }
    }
    //--Gems are next.
    else if(pItem->IsGem())
    {
        tItemScore = 50;
        uint8_t tGemColors = pItem->GetGemColors();
        if(tGemColors & ADITEM_GEMCOLOR_GREY)   tItemScore ++;
        if(tGemColors & ADITEM_GEMCOLOR_RED)    tItemScore ++;
        if(tGemColors & ADITEM_GEMCOLOR_BLUE)   tItemScore ++;
        if(tGemColors & ADITEM_GEMCOLOR_ORANGE) tItemScore ++;
        if(tGemColors & ADITEM_GEMCOLOR_VIOLET) tItemScore ++;
        if(tGemColors & ADITEM_GEMCOLOR_YELLOW) tItemScore ++;
    }
    //--Key items.
    else if(pItem->IsKeyItem())
    {
        tItemScore = 40;
    }
    //--All other items.
    else
    {
        tItemScore = 20;
    }

    //--Finish.
    return tItemScore;
}
int CompareItemsByType(const void *pEntryA, const void *pEntryB)
{
    //--Sorts by "Type". Weapons -> Armors -> Accessories -> Items -> Gems -> Misc.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Get the pointer out and cast to AdventureItem.
    AdventureItem *rItemA = (AdventureItem *)((*rEntryA)->rData);
    AdventureItem *rItemB = (AdventureItem *)((*rEntryB)->rData);

    //--"Score" the item.
    int tItemScoreA = GetItemScoreByType(rItemA);
    int tItemScoreB = GetItemScoreByType(rItemB);

    //--If they're identical, go by type. string.
    if(tItemScoreA == tItemScoreB)
    {
        //--Get the strings. They can legally be NULL.
        const char *rTypeA = rItemA->GetDescriptionType();
        const char *rTypeB = rItemB->GetDescriptionType();

        //--If neither is null, compare:
        if(rTypeA && rTypeB)
        {
            //--Check.
            int tResult = strcmp(rTypeA, rTypeB);
            if(tResult) return tResult;

            //--If the types are identical, check if the values are not.
            int tValueResult = (rItemB->GetValue() - rItemA->GetValue());
            if(tValueResult) return tValueResult;

            //--If the values AND type were identical, go by name.
            return CompareItemsByName(pEntryA, pEntryB);
        }

        //--If both are NULL, compare by name.
        if(!rTypeA && !rTypeB)
        {
            return CompareItemsByName(pEntryA, pEntryB);
        }

        //--If one is null and the other is not, the null always loses.
        if(rTypeA == NULL) return 1;
        return -1;
    }

    //--Otherwise, go by score.
    return (tItemScoreB - tItemScoreA);
}

///--[Sort by Name]
int CompareItemsByName(const void *pEntryA, const void *pEntryB)
{
    //--This is a good general-purpose sort.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Get the pointer out and cast to AdventureItem.
    AdventureItem *rItemA = (AdventureItem *)((*rEntryA)->rData);
    AdventureItem *rItemB = (AdventureItem *)((*rEntryB)->rData);

    //--If the two are identical, go by unique ID.
    int tScore = strcmp(rItemA->GetName(), rItemB->GetName());
    if(tScore == 0)
    {
        return (rItemB->GetID() - rItemA->GetID());
    }

    //--Go by name.
    return tScore;
}

///--[Sort by Quality]
int CompareItemsByQuality(const void *pEntryA, const void *pEntryB)
{
    //--Attempts to sort the entries by 'quality', which is a formula that just sort of figures out
    //  which is 'better' in vague game terms. Some items may have a lower quality than their immediate
    //  numbers may suggest because certain enemies are weak against certain damage types. However, as
    //  the game goes on, quality of items generally increases as their numbers do.
    //--Quality does NOT directly map onto value! Runestones and other combat items often have low values
    //  but are extremely useful.
    //--This is a good general-purpose sort.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Get the pointer out and cast to AdventureItem.
    AdventureItem *rItemA = (AdventureItem *)((*rEntryA)->rData);
    AdventureItem *rItemB = (AdventureItem *)((*rEntryB)->rData);

    //--If the qualities are identical, go by name.
    int tQualityA = rItemA->ComputeQuality();
    int tQualityB = rItemB->ComputeQuality();
    if(tQualityA == tQualityB)
    {
        return CompareItemsByName(pEntryA, pEntryB);
    }

    return ((tQualityB * 10000) - (tQualityA * 10000));
}
