//--Base
#include "TextLevel.h"

//--Classes
#include "TextLevOptions.h"
#include "TypingCombat.h"
#include "WordCombat.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "MapManager.h"

///========================================= Lua Hooking ==========================================
void TextLevel::HookToLuaState(lua_State *pLuaState)
{
    /* TL_Create()
       Creates a new TextLevel() and registers it as the active level. */
    lua_register(pLuaState, "TL_Create", &Hook_TL_Create);

    /* TL_CreateAndSuspend()
       Creates a new TextLevel() and registers it as the active level. The previous level is stored
       within and can be retrieved with TL_Unsuspend().*/
    lua_register(pLuaState, "TL_CreateAndSuspend", &Hook_TL_CreateAndSuspend);

    /* TL_Unsuspend()
       Retrieves the suspended level from the TextLevel() and makes it the active level. The TextLevel
       is deleted.*/
    lua_register(pLuaState, "TL_Unsuspend", &Hook_TL_Unsuspend);

    /* //--[Static]
       TL_GetProperty("Is Building Commands") (1 Boolean) (Static)
       TL_GetProperty("Header String") (1 String) (Static)
       TL_GetProperty("Command String") (1 String) (Static)
       TL_GetProperty("Special Ending") (1 Integer) (Static)

       --[System]
       TL_GetProperty("Last ID") (1 Integer)
       TL_GetProperty("Is Combat Active") (1 Boolean)
       TL_GetProperty("Player HP") (1 Integer)
       TL_GetProperty("Player Potions") (1 Integer)
       TL_GetProperty("Current Scale Index") (1 Integer)
       TL_GetProperty("Save Path At", iIndex) (1 String)

       --[Deck Editor]
       TL_GetProperty("Deck Card Count", iType) (1 Integer)

       --[Map]
       TL_GetProperty("Map Lft Extent") (1 Integer)
       TL_GetProperty("Map Top Extent") (1 Integer)
       TL_GetProperty("Map Rgt Extent") (1 Integer)
       TL_GetProperty("Map Bot Extent") (1 Integer)
       TL_GetProperty("Map ZLo Extent") (1 Integer)
       TL_GetProperty("Map ZHi Extent") (1 Integer)
       TL_GetProperty("Room Exists At", iX, iY, iZ) (1 Boolean)
       TL_GetProperty("Room Is Unenterable", iX, iY, iZ) (1 Boolean)
       TL_GetProperty("Room Is No See", iX, iY, iZ) (1 Boolean)
       TL_GetProperty("Room Name At", iX, iY, iZ) (1 String)
       TL_GetProperty("Room Identity At", iX, iY, iZ) (1 String)
       TL_GetProperty("Room Connectivity At", iX, iY, iZ) (1 String)
       TL_GetProperty("Room Openess At", iX, iY, iZ) (1 String)
       TL_GetProperty("Room Door At", iX, iY, iZ, iIndex) (1 String)
       TL_GetProperty("Room Animation At", iX, iY, iZ, iIndex) (1 String)
       TL_GetProperty("Room Audio At", iX, iY, iZ) (1 String)
       TL_GetProperty("Stranger Grouping At", iX, iY, iZ) (1 String)
       TL_GetProperty("Pathnode Identity", iPathIndex) (1 String)
       TL_GetProperty("Pathnode Type", iPathIndex) (1 String)
       TL_GetProperty("Pathnode Position", iPathIndex, iNodeIndex) (1 String)
       TL_GetProperty("Location Type", iLocationIndex) (1 String)
       TL_GetProperty("Location Room Name", iLocationIndex) (1 String)
       TL_GetProperty("Vis Override At", iX, iY, iZ, iDirection) (1 Integer)
       TL_GetProperty("Vis Down At", iX, iY, iZ) (1 Integer)
       Sets the requested property in the TextLevel. */
    lua_register(pLuaState, "TL_GetProperty", &Hook_TL_GetProperty);

    /* [Static]
       TL_SetProperty("Reset Script", sPath) (Static)
       TL_SetProperty("Post-Exec Script", sPath) (Static)
       TL_SetProperty("Ending Script", sPath) (Static)

       [General]
       TL_SetProperty("Parse SLF", sPath)
       TL_SetProperty("Register Music", sAudioPackageName, sMusicName, fStartingVolume)
       TL_SetProperty("Modify Music", sMusicName, fTargetVolume)
       TL_SetProperty("Current Scale Index", iIndex)
       TL_SetProperty("Automap Tilset", sDLPath)
       TL_SetProperty("Automap Fade Positions", fLineX, fLineY, fCornX, fCornY, fAlleyX, fAlleyY, fBoxX, fBoxY, fInsetX, fInsetY)
       TL_SetProperty("Automap Alarm Positions", fExclamationX, fExclamationY, fQuestionX, fQuestionY)
       TL_SetProperty("String Handler Path", sPath)
       TL_SetProperty("Player World Position", iX, iY, iZ)
       TL_SetProperty("Open Options")

       [Deck Editor]
       TL_SetProperty("Construct Deck Editor")
       TL_SetProperty("Activate Deck Editor")
       TL_SetProperty("Set Deck Editor Post Exec", sPath)
       TL_SetProperty("Set Deck Editor Help Exec", sPath)
       TL_SetProperty("Set Hand Counts", iType, iCurrent, iMinimum, iMaximum)
       TL_SetProperty("Set Deck Size Range", iMinimum, iMaximum)
       TL_SetProperty("Set Bonus", iType, iValue)
       TL_SetProperty("Set Sentence Bonus", iType, sValue)
       TL_SetProperty("Set Help Header", sString)
       TL_SetProperty("Set Help Line", iLine, sString)

       [Instructions]
       TL_SetProperty("Append", sString)
       TL_SetProperty("Default Image", sName, sDLPath, iLayer)
       TL_SetProperty("Register Image", sName, sDLPath, iLayer)
       TL_SetProperty("Unregister Image")
       TL_SetProperty("Create Blocker")
       TL_SetProperty("Exec Script", sScriptPath, iArgs, sArg1, sArg2, ...)
       TL_SetProperty("Play SFX", sSFXName)
       TL_SetProperty("Queue Fade Out", sPostExec)
       TL_SetProperty("Queue Fade In", sPostExec)

       [Portrait Window]
       TL_SetProperty("Set Storage Line", iLine, sString)

       [Commands]
       TL_SetProperty("Clear Commands")
       TL_SetProperty("Register Command", sCommandText, bActivatesImmediately)

       [Map Handling]
       TL_SetProperty("Begin Fade Out", sPostExec)
       TL_SetProperty("Begin Fade In", sPostExec)
       TL_SetProperty("Set Navigation Flags", uiFlags)
       TL_SetProperty("Register Animation", sName, iTPF, iStartX, iStartY, iTotalFrames, iFramesPerRow)
       TL_SetProperty("Register Room", sName, fX, fY, fZ)
       TL_SetProperty("Register Connection", sNameA, sNameB)
       TL_SetProperty("Set Room Explored", fX, fY, fZ, bIsExplored)
       TL_SetProperty("Set Room Visible", fX, fY, fZ, bIsVisible)
       TL_SetProperty("Set Room Flags", fX, fY, fZ, bHasHostile, bHasFriendly, bHasExaminable, bHasItem)
       TL_SetProperty("Add Room Tile Layer", fX, fY, fZ, iTileX, iTileY, iTileSlot, uiTileRotation)
       TL_SetProperty("Set Room Door State", fX, fY, fZ, bIsSouthDoor, bIsOpen)
       TL_SetProperty("Set Room Door Indicator", fX, fY, fZ, bIsSouthDoor, iTileX, iTileY)
       TL_SetProperty("Clear Room Flags", fX, fY, fZ)
       TL_SetProperty("Set Map Focus", fX, fY)
       TL_SetProperty("Register Entity Indicator", fX, fY, fZ, sName, iTileX, iTileY, iPriority)
       TL_SetProperty("Unregister Entity Indicator", fX, fY, fZ, sName)
       TL_SetProperty("Modify Entity Indicator", fX, fY, fZ, sName, iTileX, iTileY, iPriority)
       TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, sName, iHostileX, iHostileY)
       TL_SetProperty("Move Entity Indicator", fX, fY, fZ, sName, fNewX, fNewY, fNewZ)
       TL_SetProperty("Finalize World")
       TL_SetProperty("Purge Temp Data")
       TL_SetProperty("Reresolve Fades")
       TL_SetProperty("Register Animation To Room", fX, fY, fZ, sAnimationName)
       TL_SetProperty("Set Rendering Z Level", fZ)
       TL_SetProperty("Set Under Rendering Z Level", fZLo, fZHi)
       TL_SetProperty("Set Map Fade Timer", fTimer) //0.0 = no fade, 1.0 = full black.
       TL_SetProperty("Set Map Fade Delta", fDelta)

       [Overlays]
       TL_SetProperty("Activate Overlays")
       TL_SetProperty("Deactivate Overlays")
       TL_SetProperty("Clear Overlays")
       TL_SetProperty("Register Overlay", sName, sImgPath, fX, fY, fXScroll, fYScroll, fXClamp, fYClamp)
       TL_SetProperty("Modify Overlay", sName, sImgPath)

       [Player's Map]
       TL_SetProperty("Register Map Layer", sLayerName, iPriority, sImgPath)
       TL_SetProperty("Set Layer Visible", sLayerName, bIsActive)
       TL_SetProperty("Show Player Map")

       [Combat]
       TL_SetProperty("Set Combat Type", iType)
       TL_SetProperty("Set Combat Active Mode", bIsActive)
       TL_SetProperty("Activate Combat")
       TL_SetProperty("Set Hide Stats Flag", bFlag)
       TL_SetProperty("Set Player Stats", iHP, iHPMax, iAtk, iDef, sImagePath)
       TL_SetProperty("Set Combat Scripts", sVictory, sDefeat)
       TL_SetProperty("Finalize Combat")
       TL_SetProperty("Reresolve Stats After Combat")

       [Locality]
       TL_SetProperty("Set Locality Icon Flag", bFlag)
       TL_SetProperty("Register Locality Entry As List", sName)
       TL_SetProperty("Register Locality Entry As List With Image", sName, sImgPathUp, sImgPathDn)
       TL_SetProperty("Register Locality Entry", sName, sBuilderScript)
       TL_SetProperty("Register Locality Entry", sName, sSubtopic, sBuilderScript)
       TL_SetProperty("Register Locality Entry", sName, sSubtopic, sBuilderScript, iPriority)
       TL_SetProperty("Clear Locality List")
       TL_SetProperty("Resume Locality States")
       TL_SetProperty("Set Locality Open State", sLocalityTopic, bIsOpen)
       TL_SetProperty("Register Popup Command", sDisplayString, sExecutionString)
       TL_SetProperty("Begin Ending") */
    lua_register(pLuaState, "TL_SetProperty", &Hook_TL_SetProperty);

    /* TL_SetTypeCombatProperty("Add Word To Attack Pool", sWord)
       TL_SetTypeCombatProperty("Add Word To Defend Pool", sWord)
       TL_SetTypeCombatProperty("Add Enemy", iHP, iAtk, iDef, sImagePath)
       TL_SetTypeCombatProperty("Difficulty", iValue)
       Sets the requested property in the typing-combat version of combat. */
    lua_register(pLuaState, "TL_SetTypeCombatProperty", &Hook_TL_SetTypeCombatProperty);

    /* TL_SetWordCombatProperty("Player Properties", iHP, iHPMax, iPotionCount)
       TL_SetWordCombatProperty("Add Enemy", iHP, iHPMax, sImgPath)
       TL_SetWordCombatProperty("Add Attack To Enemy", iWeight, iPower, iPrepTime, uiElementalFlags)
       TL_SetWordCombatProperty("Add Weakness To Enemy", iWeakness, iAmount)
       TL_SetWordCombatProperty("Add Card To Deck", iType, iEffect, iSeverity, sWord, uiElementalFlags)
       TL_SetWordCombatProperty("Set Ace Card", iType, iEffect, iSeverity, sWord, uiElementalFlags)
       TL_SetWordCombatProperty("Add Shields", iValue)
       Sets the requested property in the sentence-combat version of combat. */
    lua_register(pLuaState, "TL_SetWordCombatProperty", &Hook_TL_SetWordCombatProperty);

    /* TL_Save(sFileName)
       Saves the game to the named file in the Saves/ direction. Overwrite is assumed to be on. */
    lua_register(pLuaState, "TL_Save", &Hook_TL_Save);

    /* TL_Load(sFileName)
       Loads the game from a save file. Overwrites the current game state if applicable. */
    lua_register(pLuaState, "TL_Load", &Hook_TL_Load);

    /* TL_Exists(sFileName) (1 Boolean)
       Returns true if the savefile exists, false if it does not. */
    lua_register(pLuaState, "TL_Exists", &Hook_TL_Exists);

    /* TL_GetSaveFileList(sPath) (1 Integer)
       Returns how many savefiles exist at that path, and populates data for TL_GetProperty("Save At", iIndex).*/
    lua_register(pLuaState, "TL_GetSaveFileList", &Hook_TL_GetSaveFileList);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_TL_Create(lua_State *L)
{
    //TL_Create()
    TextLevel *nNewLevel = new TextLevel();
    MapManager::Fetch()->ReceiveLevel(nNewLevel);

    //--Finish up.
    return 0;
}
int Hook_TL_CreateAndSuspend(lua_State *L)
{
    //TL_CreateAndSuspend()
    TextLevel *nLevel = new TextLevel();
    nLevel->ProvideSuspendedLevel(MapManager::Fetch()->LiberateLevel());
    MapManager::Fetch()->ReceiveLevel(nLevel);
    return 0;
}
int Hook_TL_Unsuspend(lua_State *L)
{
    //TL_Unsuspend()

    //--Get and check.
    TextLevel *rTextLevel = TextLevel::Fetch();
    if(!rTextLevel) return LuaTypeError("TL_Unsuspend", L);

    RootLevel *rSuspendedLevel = rTextLevel->LiberateSuspendedLevel();
    MapManager::Fetch()->ReceiveLevel(rSuspendedLevel);
    return 0;
}
int Hook_TL_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //TL_GetProperty("Is Building Commands") (1 Boolean) (Static)
    //TL_GetProperty("Header String") (1 String) (Static)
    //TL_GetProperty("Command String") (1 String) (Static)
    //TL_GetProperty("Special Ending") (1 Integer) (Static)

    //--[System]
    //TL_GetProperty("Last ID") (1 Integer)
    //TL_GetProperty("Is Combat Active") (1 Boolean)
    //TL_GetProperty("Player HP") (1 Integer)
    //TL_GetProperty("Player Potions") (1 Integer)
    //TL_GetProperty("Current Scale Index") (1 Integer)
    //TL_GetProperty("Save Path At", iIndex) (1 String)

    //--[Deck Editor]
    //TL_GetProperty("Deck Card Count", iType) (1 Integer)

    //--[Map]
    //TL_GetProperty("Map Lft Extent") (1 Integer)
    //TL_GetProperty("Map Top Extent") (1 Integer)
    //TL_GetProperty("Map Rgt Extent") (1 Integer)
    //TL_GetProperty("Map Bot Extent") (1 Integer)
    //TL_GetProperty("Map ZLo Extent") (1 Integer)
    //TL_GetProperty("Map ZHi Extent") (1 Integer)
    //TL_GetProperty("Room Exists At", iX, iY, iZ) (1 Boolean)
    //TL_GetProperty("Room Is Unenterable", iX, iY, iZ) (1 Boolean)
    //TL_GetProperty("Room Is No See", iX, iY, iZ) (1 Boolean)
    //TL_GetProperty("Room Name At", iX, iY, iZ) (1 String)
    //TL_GetProperty("Room Identity At", iX, iY, iZ) (1 String)
    //TL_GetProperty("Room Connectivity At", iX, iY, iZ) (1 String)
    //TL_GetProperty("Room Openess At", iX, iY, iZ) (1 String)
    //TL_GetProperty("Room Door At", iX, iY, iZ, iIndex) (1 String)
    //TL_GetProperty("Room Animation At", iX, iY, iZ, iIndex) (1 String)
    //TL_GetProperty("Room Audio At", iX, iY, iZ) (1 String)
    //TL_GetProperty("Stranger Grouping At", iX, iY, iZ) (1 String)
    //TL_GetProperty("Pathnode Identity", iPathIndex) (1 String)
    //TL_GetProperty("Pathnode Type", iPathIndex) (1 String)
    //TL_GetProperty("Pathnode Position", iPathIndex, iNodeIndex) (1 String)
    //TL_GetProperty("Location Type", iLocationIndex) (1 String)
    //TL_GetProperty("Location Room Name", iLocationIndex) (1 String)
    //TL_GetProperty("Vis Override At", iX, iY, iZ, iDirection) (1 Integer)
    //TL_GetProperty("Vis Down At", iX, iY, iZ) (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TL_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Values]
    //--Returns true if currently in a command building call.
    if(!strcasecmp(rSwitchType, "Is Building Commands") && tArgs == 1)
    {
        lua_pushboolean(L, TextLevel::xIsBuildingCommands);
        return 1;
    }
    //--Returns the heading of the command that was clicked when building commands.
    else if(!strcasecmp(rSwitchType, "Header String") && tArgs == 1)
    {
        lua_pushstring(L, TextLevel::xCommandHeadingString);
        return 1;
    }
    //--Returns the name of the command that was clicked when building commands.
    else if(!strcasecmp(rSwitchType, "Command String") && tArgs == 1)
    {
        lua_pushstring(L, TextLevel::xCommandBaseString);
        return 1;
    }
    //--Returns the special ending code.
    else if(!strcasecmp(rSwitchType, "Special Ending") && tArgs == 1)
    {
        lua_pushinteger(L, TextLevOptions::xEndingOverride);
        return 1;
    }

    ///--[Dynamic]
    //--Get and check.
    TextLevel *rTextLevel = TextLevel::Fetch();
    if(!rTextLevel) return LuaTypeError("TL_GetProperty", L);

    ///--[System]
    //--The ID of the last object created.
    if(!strcasecmp(rSwitchType, "Last ID") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetLastID());
        tReturns = 1;
    }
    //--Whether or not combat is currently active.
    else if(!strcasecmp(rSwitchType, "Is Combat Active") && tArgs == 1)
    {
        lua_pushboolean(L, rTextLevel->IsCombatActive());
        tReturns = 1;
    }
    //--Health of the player. Reported after combat.
    else if(!strcasecmp(rSwitchType, "Player HP") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetPlayerHP());
        tReturns = 1;
    }
    //--Potion count.
    else if(!strcasecmp(rSwitchType, "Player Potions") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetPotions());
        tReturns = 1;
    }
    //--The index of the current scale. This is not the scale, it's where the scale is relative to the other scales.
    else if(!strcasecmp(rSwitchType, "Current Scale Index") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetCurrentZoomIndex());
        tReturns = 1;
    }
    //--The name of the savefile found at the given index. Default is "Null".
    else if(!strcasecmp(rSwitchType, "Save Path At") && tArgs == 2)
    {
        lua_pushstring(L, rTextLevel->GetSaveIndex(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    ///--[Deck Editor]
    //--Leftmost map position.
    else if(!strcasecmp(rSwitchType, "Deck Card Count") && tArgs == 2)
    {
        lua_pushinteger(L, rTextLevel->GetDeckEditorCardCount(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    ///--[Map]
    //--Leftmost map position.
    else if(!strcasecmp(rSwitchType, "Map Lft Extent") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetLftExtent());
        tReturns = 1;
    }
    //--Topmost map position.
    else if(!strcasecmp(rSwitchType, "Map Top Extent") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetTopExtent());
        tReturns = 1;
    }
    //--Rightmost map position.
    else if(!strcasecmp(rSwitchType, "Map Rgt Extent") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetRgtExtent());
        tReturns = 1;
    }
    //--Bottom-most map position.
    else if(!strcasecmp(rSwitchType, "Map Bot Extent") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetBotExtent());
        tReturns = 1;
    }
    //--Lowest Z level.
    else if(!strcasecmp(rSwitchType, "Map ZLo Extent") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetZLoExtent());
        tReturns = 1;
    }
    //--Highest Z level.
    else if(!strcasecmp(rSwitchType, "Map ZHi Extent") && tArgs == 1)
    {
        lua_pushinteger(L, rTextLevel->GetZHiExtent());
        tReturns = 1;
    }
    //--Whether or not a room exists at the given position.
    else if(!strcasecmp(rSwitchType, "Room Exists At") && tArgs == 4)
    {
        lua_pushboolean(L, rTextLevel->DoesBuilderRoomExist(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4)));
        tReturns = 1;
    }
    //--Whether or not a room at the given location can be entered.
    else if(!strcasecmp(rSwitchType, "Room Is Unenterable") && tArgs == 4)
    {
        lua_pushboolean(L, rTextLevel->IsBuilderRoomUnenterable(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4)));
        tReturns = 1;
    }
    //--Whether or not a room at the given location has the "No See" flag so enemies won't spot the player.
    else if(!strcasecmp(rSwitchType, "Room Is No See") && tArgs == 4)
    {
        lua_pushboolean(L, rTextLevel->IsBuilderRoomNoSee(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4)));
        tReturns = 1;
    }
    //--Name of the room at the location.
    else if(!strcasecmp(rSwitchType, "Room Name At") && tArgs == 4)
    {
        rTextLevel->PopulateBuilderRoomName(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Identity of the room at the location.
    else if(!strcasecmp(rSwitchType, "Room Identity At") && tArgs == 4)
    {
        rTextLevel->PopulateBuilderRoomIdentity(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Connectivity of the room at the location.
    else if(!strcasecmp(rSwitchType, "Room Connectivity At") && tArgs == 4)
    {
        rTextLevel->PopulateBuilderRoomConnectivity(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Openess of the room at the location.
    else if(!strcasecmp(rSwitchType, "Room Openess At") && tArgs == 4)
    {
        rTextLevel->PopulateBuilderRoomOpenEdges(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Door at the given position/index. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Room Door At") && tArgs == 5)
    {
        rTextLevel->PopulateBuilderRoomDoor(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Animation at the given position/index. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Room Animation At") && tArgs == 5)
    {
        rTextLevel->PopulateBuilderRoomAnimation(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Audio string for the given position. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Room Audio At") && tArgs == 4)
    {
        rTextLevel->PopulateBuilderRoomAudio(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Stranger Grouping for the given position. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Stranger Grouping At") && tArgs == 4)
    {
        rTextLevel->PopulateBuilderRoomStrangerGroup(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Unique identity of the path group at the given index. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Pathnode Identity") && tArgs == 2)
    {
        rTextLevel->PopulateBuilderPatrolIdentity(lua_tointeger(L, 2));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Enemy type of the given path set. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Pathnode Type") && tArgs == 2)
    {
        rTextLevel->PopulateBuilderPatrolEnemyType(lua_tointeger(L, 2));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Node position of a given path set. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Pathnode Position") && tArgs == 3)
    {
        rTextLevel->PopulateBuilderPatrolLocation(lua_tointeger(L, 2), lua_tointeger(L, 3));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Type of the indexed location package. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Location Type") && tArgs == 2)
    {
        rTextLevel->PopulateBuilderLocationName(lua_tointeger(L, 2));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Name of the room with the indexed location package in it. Defaults to "Null".
    else if(!strcasecmp(rSwitchType, "Location Room Name") && tArgs == 2)
    {
        rTextLevel->PopulateBuilderLocationRoom(lua_tointeger(L, 2));
        lua_pushstring(L, rTextLevel->GetBuilderStringBuffer());
        tReturns = 1;
    }
    //--Returns the integer code indicating visibility overrides for the given room. These are of the
    //  OVERRIDE_VISIBILITY_NONE series.
    else if(!strcasecmp(rSwitchType, "Vis Override At") && tArgs == 5)
    {
        lua_pushinteger(L, rTextLevel->GetBuilderVisOverrideAt(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5)));
        tReturns = 1;
    }
    //--Returns how many stories below the tile are visible if the tile is visible. Used to allow vertical viewing
    //  from a higher z-level.
    else if(!strcasecmp(rSwitchType, "Vis Down At") && tArgs == 4)
    {
        lua_pushinteger(L, rTextLevel->GetBuilderVisDown(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4)));
        tReturns = 1;
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("TL_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_TL_SetProperty(lua_State *L)
{
    ///--[Argument List]
    //--[Boot]
    //TL_SetProperty("Construct Electrosprite")
    //TL_SetProperty("Construct String Tyrant")

    //--[General]
    //TL_SetProperty("Reset Script", sPath) (Static)
    //TL_SetProperty("Post-Exec Script", sPath) (Static)
    //TL_SetProperty("Ending Script", sPath) (Static)
    //TL_SetProperty("Register Music", sAudioPackageName, sMusicName, fStartingVolume)
    //TL_SetProperty("Modify Music", sMusicName, fTargetVolume)
    //TL_SetProperty("Current Scale Index", iIndex)
    //TL_SetProperty("Automap Tilset", sDLPath)
    //TL_SetProperty("Automap Fade Positions", fLineX, fLineY, fCornX, fCornY, fAlleyX, fAlleyY, fBoxX, fBoxY, fInsetX, fInsetY)
    //TL_SetProperty("Automap Alarm Positions", fExclamationX, fExclamationY, fQuestionX, fQuestionY)
    //TL_SetProperty("String Handler Path", sPath)
    //TL_SetProperty("Player World Position", iX, iY, iZ)
    //TL_SetProperty("Open Options")

    //--[Deck Editor]
    //TL_SetProperty("Construct Deck Editor")
    //TL_SetProperty("Activate Deck Editor")
    //TL_SetProperty("Set Deck Editor Post Exec", sPath)
    //TL_SetProperty("Set Deck Editor Help Exec", sPath)
    //TL_SetProperty("Set Hand Counts", iType, iCurrent, iMinimum, iMaximum)
    //TL_SetProperty("Set Deck Size Range", iMinimum, iMaximum)
    //TL_SetProperty("Set Bonus", iType, iValue)
    //TL_SetProperty("Set Sentence Bonus", iType, sValue)
    //TL_SetProperty("Set Help Header", sString)
    //TL_SetProperty("Set Help Line", iLine sString)

    //--[Map Builder]
    //TL_SetProperty("Parse SLF", sPath)
    //TL_SetProperty("Build Temporary World")

    //--[Instructions]
    //TL_SetProperty("Append", sString)
    //TL_SetProperty("Default Image", sName, sDLPath, iLayer)
    //TL_SetProperty("Register Image", sName, sDLPath, iLayer)
    //TL_SetProperty("Unregister Image")
    //TL_SetProperty("Create Blocker")
    //TL_SetProperty("Exec Script", sScriptPath, iArgs, sArg1, sArg2, ...)
    //TL_SetProperty("Play SFX", sSFXName)
    //TL_SetProperty("Queue Fade Out", sPostExec)
    //TL_SetProperty("Queue Fade In", sPostExec)

    //--[Portrait Window]
    //TL_SetProperty("Set Storage Line", iLine, sString)

    //--[Commands]
    //TL_SetProperty("Clear Commands")
    //TL_SetProperty("Register Command", sCommandText, bActivatesImmediately)

    //--[Map Handling]
    //TL_SetProperty("Begin Fade Out", sPostExec)
    //TL_SetProperty("Begin Fade In", sPostExec)
    //TL_SetProperty("Set Navigation Flags", uiFlags)
    //TL_SetProperty("Register Animation", sName, iTPF, iStartX, iStartY, iTotalFrames, iFramesPerRow)
    //TL_SetProperty("Register Room", sName, fX, fY, fZ)
    //TL_SetProperty("Register Connection", sNameA, sNameB)
    //TL_SetProperty("Set Room Explored", fX, fY, fZ, bIsExplored)
    //TL_SetProperty("Set Room Visible", fX, fY, fZ, bIsVisible)
    //TL_SetProperty("Set Room Flags", fX, fY, fZ, bHasHostile, bHasFriendly, bHasExaminable, bHasItem)
    //TL_SetProperty("Add Room Tile Layer", fX, fY, fZ, iTileX, iTileY, iTileSlot, uiTileRotation)
    //TL_SetProperty("Set Room Door State", fX, fY, fZ, bIsSouthDoor, bIsOpen)
    //TL_SetProperty("Set Room Door Indicator", fX, fY, fZ, bIsSouthDoor, iTileX, iTileY)
    //TL_SetProperty("Clear Room Flags", fX, fY, fZ)
    //TL_SetProperty("Set Map Focus", fX, fY)
    //TL_SetProperty("Register Entity Indicator", fX, fY, fZ, sName, iTileX, iTileY, iPriority)
    //TL_SetProperty("Unregister Entity Indicator", fX, fY, fZ, sName)
    //TL_SetProperty("Unregister All Entity Indicators")
    //TL_SetProperty("Modify Entity Indicator", fX, fY, fZ, sName, iTileX, iTileY, iPriority)
    //TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, sName, iHostileX, iHostileY)
    //TL_SetProperty("Move Entity Indicator", fX, fY, fZ, sName, fNewX, fNewY, fNewZ)
    //TL_SetProperty("Finalize World")
    //TL_SetProperty("Purge Temp Data")
    //TL_SetProperty("Reresolve Fades")
    //TL_SetProperty("Register Animation To Room", fX, fY, fZ, sAnimationName)
    //TL_SetProperty("Set Rendering Z Level", fZ)
    //TL_SetProperty("Set Under Rendering Z Level", fZLo, fZHi)
    //TL_SetProperty("Set Map Fade Timer", fTimer) //0.0 = no fade, 1.0 = full black.
    //TL_SetProperty("Set Map Fade Delta", fDelta)

    //--[Overlays]
    //TL_SetProperty("Activate Overlays")
    //TL_SetProperty("Deactivate Overlays")
    //TL_SetProperty("Clear Overlays")
    //TL_SetProperty("Register Overlay", sName, sImgPath, fX, fY, fXScroll, fYScroll, fXClamp, fYClamp)
    //TL_SetProperty("Modify Overlay", sName, sImgPath)

    //--[Player's Map]
    //TL_SetProperty("Register Map Layer", sLayerName, iPriority, sImgPath)
    //TL_SetProperty("Set Layer Visible", sLayerName, bIsActive)
    //TL_SetProperty("Show Player Map")

    //--[Combat]
    //TL_SetProperty("Set Combat Type", iType)
    //TL_SetProperty("Set Combat Active Mode", bIsActive)
    //TL_SetProperty("Activate Combat")
    //TL_SetProperty("Set Hide Stats Flag", bFlag)
    //TL_SetProperty("Set Player Stats", iHP, iHPMax, iAtk, iDef, sImagePath)
    //TL_SetProperty("Set Combat Scripts", sVictory, sDefeat)
    //TL_SetProperty("Finalize Combat")
    //TL_SetProperty("Reresolve Stats After Combat")

    //--[Locality]
    //TL_SetProperty("Set Locality Icon Flag", bFlag)
    //TL_SetProperty("Register Locality Entry As List", sName)
    //TL_SetProperty("Register Locality Entry As List With Image", sName, sImgPathUp, sImgPathDn)
    //TL_SetProperty("Register Locality Entry", sName, sBuilderScript)
    //TL_SetProperty("Register Locality Entry", sName, sSubtopic, sBuilderScript)
    //TL_SetProperty("Register Locality Entry", sName, sSubtopic, sBuilderScript, iPriority)
    //TL_SetProperty("Clear Locality List")
    //TL_SetProperty("Resume Locality States")
    //TL_SetProperty("Set Locality Open State", sLocalityTopic, bIsOpen)
    //TL_SetProperty("Register Popup Command", sDisplayString, sExecutionString)
    //TL_SetProperty("Begin Ending")

    ///--[Setup]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TL_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Values]
    //--Sets which reset script is called from the options menu.
    if(!strcasecmp(rSwitchType, "Reset Script") && tArgs == 2)
    {
        ResetString(TextLevOptions::xResetExec, lua_tostring(L, 2));
        return 0;
    }
    //--Sets which post-exec script is called when the options menu closes.
    else if(!strcasecmp(rSwitchType, "Post-Exec Script") && tArgs == 2)
    {
        ResetString(TextLevOptions::xPostExecPath, lua_tostring(L, 2));
        return 0;
    }
    //--Sets which reset script is called from the options menu.
    else if(!strcasecmp(rSwitchType, "Ending Script") && tArgs == 2)
    {
        ResetString(TextLevOptions::xEndingExec, lua_tostring(L, 2));
        return 0;
    }

    ///--[Dynamic Values]
    //--Active object.
    TextLevel *rActiveLevel = TextLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("TL_SetProperty", L);

    ///--[Boot]
    //--Boot graphics for Electrosprite Adventure.
    if(!strcasecmp(rSwitchType, "Construct Electrosprite") && tArgs == 1)
    {
        rActiveLevel->ConstructElectrosprite();
    }
    //--Boot graphics for String Tyrant.
    else if(!strcasecmp(rSwitchType, "Construct String Tyrant") && tArgs == 1)
    {
        rActiveLevel->ConstructStringTyrant();
    }
    ///--[General]
    //--Adds a new ambience track.
    else if(!strcasecmp(rSwitchType, "Register Music") && tArgs == 4)
    {
        rActiveLevel->RegisterAmbientMusicTrack(lua_tostring(L, 2), lua_tostring(L, 3), lua_tonumber(L, 4));
    }
    //--Change the volume on an ambience track.
    else if(!strcasecmp(rSwitchType, "Modify Music") && tArgs == 3)
    {
        rActiveLevel->ModifyAmbientMusicTrack(lua_tostring(L, 2), lua_tonumber(L, 3));
    }
    //--Sets the zoom index, which determines how the map is scaled. Doesn't directly set the zoom, just which zoom is being used.
    else if(!strcasecmp(rSwitchType, "Current Scale Index") && tArgs == 2)
    {
        rActiveLevel->SetZoomIndex(lua_tointeger(L, 2));
    }
    //--Automap tileset, activates rendering modes if non-NULL.
    else if(!strcasecmp(rSwitchType, "Automap Tilset") && tArgs == 4)
    {
        rActiveLevel->SetAutomapTileset(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Specifies where the fade overlays can be found on the automap.
    else if(!strcasecmp(rSwitchType, "Automap Fade Positions") && tArgs == 13)
    {
        rActiveLevel->SetTilesetFadePositions(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8), lua_tonumber(L, 9), lua_tonumber(L, 10), lua_tonumber(L, 11), lua_tonumber(L, 12), lua_tonumber(L, 13));
    }
    //--Specifies where the alarm indicators are for indicators.
    else if(!strcasecmp(rSwitchType, "Automap Alarm Positions") && tArgs == 5)
    {
        rActiveLevel->SetTilesetAlarmPositions(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Sets which script is called when the player enters a string.
    else if(!strcasecmp(rSwitchType, "String Handler Path") && tArgs == 2)
    {
        rActiveLevel->SetStringHandler(lua_tostring(L, 2));
    }
    //--World location of the player. Used for in-map mouse click handling.
    else if(!strcasecmp(rSwitchType, "Player World Position") && tArgs == 4)
    {
        rActiveLevel->SetPlayerWorldTile(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Opens the options menu.
    else if(!strcasecmp(rSwitchType, "Open Options") && tArgs == 1)
    {
        rActiveLevel->ActivateOptionsMenu();
    }
    ///--[Deck Editor]
    //--Orders the Deck Editor to crossload its images.
    else if(!strcasecmp(rSwitchType, "Construct Deck Editor") && tArgs == 1)
    {
        rActiveLevel->ConstructDeckEditor();
    }
    //--Shows the Deck Editor during gameplay.
    else if(!strcasecmp(rSwitchType, "Activate Deck Editor") && tArgs == 1)
    {
        rActiveLevel->ActivateDeckEditor();
    }
    //--Sets the script that executes when Accept/Defaults is selected.
    else if(!strcasecmp(rSwitchType, "Set Deck Editor Post Exec") && tArgs == 2)
    {
        rActiveLevel->SetDeckEditorPostExec(lua_tostring(L, 2));
    }
    //--Sets which lua file is called when the deck editor help buttons are clicked.
    else if(!strcasecmp(rSwitchType, "Set Deck Editor Help Exec") && tArgs == 2)
    {
        rActiveLevel->SetDeckEditorHelpExec(lua_tostring(L, 2));
    }
    //--Sets the current, minimum, and maximum for the given card type.
    else if(!strcasecmp(rSwitchType, "Set Hand Counts") && tArgs == 5)
    {
        rActiveLevel->SetDeckEditorHandCount(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--Sets how many cards are allowed in the deck.
    else if(!strcasecmp(rSwitchType, "Set Deck Size Range") && tArgs == 3)
    {
        rActiveLevel->SetDeckEditorSizeCounts(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets a stat bonus that appears on the deck editor.
    else if(!strcasecmp(rSwitchType, "Set Bonus") && tArgs == 3)
    {
        rActiveLevel->SetDeckEditorBonus(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the sentence bonus, which is a string.
    else if(!strcasecmp(rSwitchType, "Set Sentence Bonus") && tArgs == 2)
    {
        rActiveLevel->SetDeckEditorSentenceBonus(lua_tostring(L, 2));
    }
    //--Sets the string that appears on top of the help box.
    else if(!strcasecmp(rSwitchType, "Set Help Header") && tArgs == 2)
    {
        rActiveLevel->SetDeckEditorHelpHeader(lua_tostring(L, 2));
    }
    //--Sets one of the strings that appears in the help box.
    else if(!strcasecmp(rSwitchType, "Set Help Line") && tArgs == 3)
    {
        rActiveLevel->SetDeckEditorHelpLine(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    ///--[Map Builder]
    //--Parse an SLF file for world information.
    else if(!strcasecmp(rSwitchType, "Parse SLF") && tArgs == 3)
    {
        rActiveLevel->ParseSLFFile(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Build a temporary 3D lookup for room crossloading.
    else if(!strcasecmp(rSwitchType, "Build Temporary World") && tArgs == 1)
    {
        rActiveLevel->AssembleBuilderWorld();
    }
    ///--[Instructions]
    //--Appends the given text.
    else if(!strcasecmp(rSwitchType, "Append") && tArgs == 2)
    {
        rActiveLevel->RegisterAppend(lua_tostring(L, 2));
    }
    //--Default name/image pairing when no other image packs are active.
    else if(!strcasecmp(rSwitchType, "Default Image") && tArgs == 4)
    {
        rActiveLevel->SetDefaultImage(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Register an image to the display. Auto-centers and then applies offsets.
    else if(!strcasecmp(rSwitchType, "Register Image") && tArgs == 4)
    {
        rActiveLevel->RegisterImage(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Unregisters an image.
    else if(!strcasecmp(rSwitchType, "Unregister Image") && tArgs == 1)
    {
        rActiveLevel->UnregisterImage();
    }
    //--Appends a blocker.
    else if(!strcasecmp(rSwitchType, "Create Blocker") && tArgs == 1)
    {
        rActiveLevel->RegisterBlocker();
    }
    //--Executes a given script at this point in the queue.
    else if(!strcasecmp(rSwitchType, "Exec Script") && tArgs >= 3)
    {
        //--Construct the argument list. It's a list of references.
        int tExpectedArgs = lua_tointeger(L, 3);
        const char **trArgList = NULL;
        if(tExpectedArgs > 0)
        {
            const char cNullString[] = "Null";
            SetMemoryData(__FILE__, __LINE__);
            trArgList = (const char **)starmemoryalloc(sizeof(char *) * tExpectedArgs);
            for(int i = 0; i < tExpectedArgs; i ++)
            {
                if(lua_isnil(L, 4+i))
                {
                    trArgList[i] = cNullString;
                }
                else
                {
                    trArgList[i] = lua_tostring(L, 4+i);
                }

            }
        }

        //--Execute.
        rActiveLevel->RegisterExecScript(lua_tostring(L, 2), lua_tointeger(L, 3), trArgList);

        //--Clean.
        free(trArgList);
    }
    //--Begins a fade-out in queue. Optionally, executes a script. Pass "Null" to not execute a script.
    else if(!strcasecmp(rSwitchType, "Queue Fade Out") && tArgs == 2)
    {
        rActiveLevel->RegisterFadeOut(lua_tostring(L, 2));
    }
    //--Begins a fade-in in queue. Optionally, executes a script. Pass "Null" to not execute a script.
    else if(!strcasecmp(rSwitchType, "Queue Fade In") && tArgs == 2)
    {
        rActiveLevel->RegisterFadeIn(lua_tostring(L, 2));
    }
    //--Plays SFX in a queued manner.
    else if(!strcasecmp(rSwitchType, "Play SFX") && tArgs == 2)
    {
        rActiveLevel->RegisterPlaySFX(lua_tostring(L, 2));
    }
    ///--[Portrait Window]
    else if(!strcasecmp(rSwitchType, "Set Storage Line") && tArgs == 3)
    {
        rActiveLevel->SetStorageLine(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    ///--[Map Handling]
    //--Creates a new animation which can then be registered to rooms.
    else if(!strcasecmp(rSwitchType, "Register Animation") && tArgs == 7)
    {
        rActiveLevel->RegisterAnimation(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7));
    }
    //--Immediately begins a fade out, with optional post-exec.
    else if(!strcasecmp(rSwitchType, "Begin Fade Out") && tArgs == 2)
    {
        rActiveLevel->StartFadeOut(lua_tostring(L, 2));
    }
    //--Immediately begins a fade in, with optional post-exec.
    else if(!strcasecmp(rSwitchType, "Begin Fade In") && tArgs == 2)
    {
        rActiveLevel->StartFadeIn(lua_tostring(L, 2));
    }
    //--Specifies which buttons can be clicked on using the quick navigation menu.
    else if(!strcasecmp(rSwitchType, "Set Navigation Flags") && tArgs == 2)
    {
        rActiveLevel->SetNavFlags(lua_tointeger(L, 2));
    }
    //--Registers a new room to the automap.
    else if(!strcasecmp(rSwitchType, "Register Room") && tArgs == 5)
    {
        rActiveLevel->RegisterAutomapRoom(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Registers a connection line between two rooms.
    else if(!strcasecmp(rSwitchType, "Register Connection") && tArgs == 3)
    {
        rActiveLevel->RegisterRoomConnection(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets whether or not a room appears on the minimap at all.
    else if(!strcasecmp(rSwitchType, "Set Room Explored") && tArgs == 5)
    {
        rActiveLevel->SetRoomExplored(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_toboolean(L, 5));
    }
    //--Sets whether the player can see the contents of the room.
    else if(!strcasecmp(rSwitchType, "Set Room Visible") && tArgs == 5)
    {
        rActiveLevel->SetRoomVisible(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_toboolean(L, 5));
    }
    //--Sets what kinds of entities the player can see in a given room.
    else if(!strcasecmp(rSwitchType, "Set Room Flags") && tArgs == 8)
    {
        rActiveLevel->SetRoomFlags(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_toboolean(L, 5), lua_toboolean(L, 6), lua_toboolean(L, 7), lua_toboolean(L, 8));
    }
    //--Adds tilemap display to the given room.
    else if(!strcasecmp(rSwitchType, "Add Room Tile Layer") && tArgs == 8)
    {
        rActiveLevel->AddRoomTilemapLayer(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7), lua_tointeger(L, 8));
    }
    //--Changes whether or not a door is rendering as open.
    else if(!strcasecmp(rSwitchType, "Set Room Door State") && tArgs == 6)
    {
        rActiveLevel->SetDoorState(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_toboolean(L, 5), lua_toboolean(L, 6));
    }
    //--Changes whether or not a given door has a key indicator next to it.
    else if(!strcasecmp(rSwitchType, "Set Room Door Indicator") && tArgs == 7)
    {
        rActiveLevel->SetDoorIndicator(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_toboolean(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7));
    }
    //--Clears all visibility flags.
    else if(!strcasecmp(rSwitchType, "Clear Room Flags") && tArgs == 4)
    {
        rActiveLevel->ClearRoomFlags(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Sets where the camera is focusing.
    else if(!strcasecmp(rSwitchType, "Set Map Focus") && tArgs == 3)
    {
        rActiveLevel->SetMapFocus(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Registers a new entity indicator.
    else if(!strcasecmp(rSwitchType, "Register Entity Indicator") && tArgs == 8)
    {
        rActiveLevel->RegisterTilemapIndicator(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7), lua_tointeger(L, 8));
    }
    //--Removes an entity indicator and discards it.
    else if(!strcasecmp(rSwitchType, "Unregister Entity Indicator") && tArgs == 5)
    {
        rActiveLevel->UnregisterTilemapIndicator(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5));
    }
    else if(!strcasecmp(rSwitchType, "Unregister All Entity Indicators") && tArgs == 1)
    {
        rActiveLevel->UnregisterAllTilemapIndicators();
    }
    //--Changes tile/priority of an existing entity indicator.
    else if(!strcasecmp(rSwitchType, "Modify Entity Indicator") && tArgs == 8)
    {
        rActiveLevel->ModifyTilemapIndicator(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7), lua_tointeger(L, 8));
    }
    //--Changes hostility indicator of an existing entity.
    else if(!strcasecmp(rSwitchType, "Modify Entity Indicator Hosility") && tArgs == 7)
    {
        rActiveLevel->ModifyTilemapIndicatorHostility(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7));
    }
    //--Moves an entity indicator from one room to another.
    else if(!strcasecmp(rSwitchType, "Move Entity Indicator") && tArgs == 8)
    {
        rActiveLevel->MoveTilemapIndicator(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
    }
    //--Finishes world construction and builds a 3D array with all rooms in their needed positions.
    else if(!strcasecmp(rSwitchType, "Finalize World") && tArgs == 1)
    {
        rActiveLevel->FinalizeWorld();
    }
    //--When all setup is done, wipes temporary world data. This includes doors, locations, entities, etc.
    else if(!strcasecmp(rSwitchType, "Purge Temp Data") && tArgs == 1)
    {
        rActiveLevel->CleanUpTempStructures();
    }
    //--Computes which fades are present on all tiles.
    else if(!strcasecmp(rSwitchType, "Reresolve Fades") && tArgs == 1)
    {
        rActiveLevel->RecomputeFadeVisibility();
    }
    //--Orders a given room to use a given animation.
    else if(!strcasecmp(rSwitchType, "Register Animation To Room") && tArgs == 5)
    {
        rActiveLevel->RegisterAnimationToRoom(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5));
    }
    //--Sets which Z-level is currently rendering.
    else if(!strcasecmp(rSwitchType, "Set Rendering Z Level") && tArgs == 2)
    {
        rActiveLevel->SetRenderingZ(lua_tointeger(L, 2));
    }
    //--Sets which Z-level is rendering under the current one. Pass -1000 to stop under-rendering.
    else if(!strcasecmp(rSwitchType, "Set Under Rendering Z Level") && tArgs == 3)
    {
        rActiveLevel->SetUnderRenderingZ(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets the map fade timer. 0.0 = no fade, 1.0 = full black.
    else if(!strcasecmp(rSwitchType, "Set Map Fade Timer") && tArgs == 2)
    {
        rActiveLevel->SetMapFadeTimer(lua_tonumber(L, 2));
    }
    //--Sets the rate of change for the map timer. No maximum.
    else if(!strcasecmp(rSwitchType, "Set Map Fade Delta") && tArgs == 2)
    {
        rActiveLevel->SetMapFadeDelta(lua_tonumber(L, 2));
    }
    ///--[Overlays]
    //--Turns on harsh overlay effects.
    else if(!strcasecmp(rSwitchType, "Activate Overlays") && tArgs == 1)
    {
        rActiveLevel->ActivateOverlayMode();
    }
    //--Turns off harsh overlay effects.
    else if(!strcasecmp(rSwitchType, "Deactivate Overlays") && tArgs == 1)
    {
        rActiveLevel->DeactivateOverlayMode();
    }
    //--Clears the harsh overlay data.
    else if(!strcasecmp(rSwitchType, "Clear Overlays") && tArgs == 1)
    {
        rActiveLevel->ClearOverlays();
    }
    //--Registers a new harsh overlay.
    else if(!strcasecmp(rSwitchType, "Register Overlay") && tArgs == 10)
    {
        rActiveLevel->RegisterOverlay(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8), lua_tonumber(L, 9), lua_tonumber(L, 10));
    }
    //--Changes the image on a harsh overlay.
    else if(!strcasecmp(rSwitchType, "Modify Overlay") && tArgs == 3)
    {
        rActiveLevel->ModifyOverlay(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Player's Map]
    else if(!strcasecmp(rSwitchType, "Register Map Layer") && tArgs == 4)
    {
        rActiveLevel->RegisterPlayerMapLayer(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    else if(!strcasecmp(rSwitchType, "Set Layer Visible") && tArgs == 3)
    {
        rActiveLevel->SetPlayerMapLayerVisible(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    else if(!strcasecmp(rSwitchType, "Show Player Map") && tArgs == 1)
    {
        rActiveLevel->ActivatePlayerMap();
    }
    ///--[Combat Subsection]
    else if(!strcasecmp(rSwitchType, "Set Combat Type") && tArgs == 2)
    {
        rActiveLevel->SetCombatType(lua_tointeger(L, 2));
    }
    else if(!strcasecmp(rSwitchType, "Set Combat Active Mode") && tArgs == 2)
    {
        rActiveLevel->SetCombatActiveMode(lua_toboolean(L, 2));
    }
    else if(!strcasecmp(rSwitchType, "Activate Combat") && tArgs == 1)
    {
        rActiveLevel->ActivateCombat();
    }
    //--Shows or hides player stat display.
    else if(!strcasecmp(rSwitchType, "Set Hide Stats Flag") && tArgs == 2)
    {
        rActiveLevel->SetHideStatsFlag(lua_toboolean(L, 2));
    }
    //--Sets the player's combat statistics.
    else if(!strcasecmp(rSwitchType, "Set Player Stats") && tArgs == 6)
    {
        rActiveLevel->SetPlayerStats(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tostring(L, 6));
    }
    //--Sets how the game handles victory and defeat.
    else if(!strcasecmp(rSwitchType, "Set Combat Scripts") && tArgs == 3)
    {
        rActiveLevel->SetCombatScripts(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Finishes setup and starts the first combat turn.
    else if(!strcasecmp(rSwitchType, "Finalize Combat") && tArgs == 1)
    {
        rActiveLevel->FinalizeCombat();
    }
    //--Once combat is over, stores the player's HP in the world class.
    else if(!strcasecmp(rSwitchType, "Reresolve Stats After Combat") && tArgs == 1)
    {
        rActiveLevel->ReresolveStatsAfterCombat();
    }
    ///--[Locality Subsection]
    //--Switches if the Locality window uses Icon or List rendering.
    else if(!strcasecmp(rSwitchType, "Set Locality Icon Flag") && tArgs == 2)
    {
        rActiveLevel->SetLocalityIconMode(lua_toboolean(L, 2));
    }
    //--Base list. No images.
    else if(!strcasecmp(rSwitchType, "Register Locality Entry As List") && tArgs == 2)
    {
        rActiveLevel->RegisterLocalityToBaseAsList(lua_tostring(L, 2), NULL, NULL);
    }
    //--Base list, used with sorting images.
    else if(!strcasecmp(rSwitchType, "Register Locality Entry As List With Image") && tArgs == 4)
    {
        rActiveLevel->RegisterLocalityToBaseAsList(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Entry, on the base list
    else if(!strcasecmp(rSwitchType, "Register Locality Entry") && tArgs == 3)
    {
        rActiveLevel->RegisterLocalityToBase(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Entry, on a sublist
    else if(!strcasecmp(rSwitchType, "Register Locality Entry") && tArgs == 4)
    {
        rActiveLevel->RegisterLocalityToSub(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Entry, on a sublist, has a sorting priority.
    else if(!strcasecmp(rSwitchType, "Register Locality Entry") && tArgs == 5)
    {
        rActiveLevel->RegisterLocalityToSub(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tointeger(L, 5));
    }
    //--Clears the locality window off.
    else if(!strcasecmp(rSwitchType, "Clear Locality List") && tArgs == 1)
    {
        rActiveLevel->ClearLocalityList();
    }
    //--Causes the locality window, after construction, to open previously open entries of the same name.
    else if(!strcasecmp(rSwitchType, "Resume Locality States") && tArgs == 1)
    {
        rActiveLevel->ResumeLocalityOpenStates();
    }
    //--Sets the given locality category as open or closed.
    else if(!strcasecmp(rSwitchType, "Set Locality Open State") && tArgs == 3)
    {
        rActiveLevel->SetLocalityOpen(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    else if(!strcasecmp(rSwitchType, "Register Popup Command") && tArgs == 3)
    {
        rActiveLevel->RegisterPopupCommand(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    else if(!strcasecmp(rSwitchType, "Begin Major Dialogue") && tArgs == 1)
    {
        rActiveLevel->ActivateMajorDialogueMode();
    }
    else if(!strcasecmp(rSwitchType, "Begin Ending") && tArgs == 1)
    {
        rActiveLevel->ActivateEndingMode();
    }
    ///--[Other]
    //--Error.
    else
    {
        LuaPropertyError("TL_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
int Hook_TL_SetTypeCombatProperty(lua_State *L)
{
    ///--[Argument List]
    //TL_SetTypeCombatProperty("Add Word To Attack Pool", sWord)
    //TL_SetTypeCombatProperty("Add Word To Defend Pool", sWord)
    //TL_SetTypeCombatProperty("Add Enemy", iHP, iAtk, iDef, sImagePath)
    //TL_SetTypeCombatProperty("Difficulty", iValue)

    ///--[Setup]
    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TL_SetTypeCombatProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Dynamic]
    //--Get the text level.
    TextLevel *rActiveLevel = TextLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("TL_SetTypeCombatProperty", L);

    //--Get the active combat. If none is present, fail.
    TypingCombat *rCombatHandler = rActiveLevel->GetTypingCombatHandler();
    if(!rCombatHandler)
    {
        fprintf(stderr, "%s: Failed, Typing Combat was NULL. Not initialized or not the active combat type.\n", "TL_SetTypeCombatProperty");
        return 0;
    }

    ///--[Types]
    //--Adds a word the player can type when attacking.
    if(!strcasecmp(rSwitchType, "Add Word To Attack Pool") && tArgs == 2)
    {
        rCombatHandler->RegisterAttackWord(lua_tostring(L, 2));
    }
    //--Adds a word the player can type when defending.
    else if(!strcasecmp(rSwitchType, "Add Word To Defend Pool") && tArgs == 2)
    {
        rCombatHandler->RegisterDefendWord(lua_tostring(L, 2));
    }
    //--Adds an enemy to beat up.
    else if(!strcasecmp(rSwitchType, "Add Enemy") && tArgs == 5)
    {
        rCombatHandler->RegisterEnemy(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5));
    }
    //--Difficulty rating, see TL_DIFFICULTY_NORMAL.
    else if(!strcasecmp(rSwitchType, "Set Difficulty") && tArgs == 2)
    {
        rCombatHandler->SetDifficulty(lua_tointeger(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("TL_SetTypeCombatProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
int Hook_TL_SetWordCombatProperty(lua_State *L)
{
    ///--[Argument List]
    //TL_SetWordCombatProperty("Player Properties", iHP, iHPMax, iPotionCount)
    //TL_SetWordCombatProperty("Add Enemy", iHP, iHPMax, sImgPath)
    //TL_SetWordCombatProperty("Add Attack To Enemy", iWeight, iPower, iPrepTime, uiElementalFlags)
    //TL_SetWordCombatProperty("Add Weakness To Enemy", iWeakness, iAmount)
    //TL_SetWordCombatProperty("Add Card To Deck", iType, iEffect, iSeverity, sWord, uiElementalFlags)
    //TL_SetWordCombatProperty("Set Ace Card", iType, iEffect, iSeverity, sWord, uiElementalFlags)
    //TL_SetWordCombatProperty("Add Shields", iValue)

    ///--[Setup]
    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TL_SetWordCombatProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Dynamic]
    //--Get the text level.
    TextLevel *rActiveLevel = TextLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("TL_SetWordCombatProperty", L);

    //--Get the active combat. If none is present, fail.
    WordCombat *rCombatHandler = rActiveLevel->GetWordCombatHandler();
    if(!rCombatHandler)
    {
        fprintf(stderr, "%s: Failed, Sentence Combat was NULL. Not initialized or not the active combat type.\n", "TL_SetWordCombatProperty");
        return 0;
    }

    ///--[Types]
    //--Player's basic properties.
    if(!strcasecmp(rSwitchType, "Player Properties") && tArgs == 4)
    {
        rCombatHandler->SetPlayerProperties(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Adds a new enemy. Rawr!
    else if(!strcasecmp(rSwitchType, "Add Enemy") && tArgs == 4)
    {
        rCombatHandler->RegisterEnemy(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Adds an attack to the enemy.
    else if(!strcasecmp(rSwitchType, "Add Attack To Enemy") && tArgs == 5)
    {
        rCombatHandler->RegisterAttackToLastEnemy(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--Adds a weakness to the enemy.
    else if(!strcasecmp(rSwitchType, "Add Weakness To Enemy") && tArgs == 3)
    {
        rCombatHandler->RegisterWeaknessToLastEnemy(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Adds a card to the player's deck.
    else if(!strcasecmp(rSwitchType, "Add Card To Deck") && tArgs == 6)
    {
        rCombatHandler->RegisterNewCard(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5), lua_tointeger(L, 6), false);
    }
    //--Sets the Ace card.
    else if(!strcasecmp(rSwitchType, "Set Ace Card") && tArgs == 6)
    {
        rCombatHandler->RegisterNewCard(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5), lua_tointeger(L, 6), true);
    }
    //--Adds shields to the player. Used at the start of combat, provided by equipment.
    else if(!strcasecmp(rSwitchType, "Add Shields") && tArgs == 2)
    {
        for(int i = 0; i < lua_tointeger(L, 2); i ++)
        {
            rCombatHandler->GainShield(0);
        }
    }
    //--Error.
    else
    {
        LuaPropertyError("TL_SetWordCombatProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
int Hook_TL_Save(lua_State *L)
{
    //TL_Save(sSaveName)

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TL_Save");

    //--Get the text level.
    TextLevel *rActiveLevel = TextLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("TL_Save", L);

    //--Run.
    rActiveLevel->SaveFile(lua_tostring(L, 1));

    //--Finish up.
    return 0;
}
int Hook_TL_Load(lua_State *L)
{
    //TL_Load(sSaveName)

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TL_Load");

    //--Get the text level.
    TextLevel *rActiveLevel = TextLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("TL_Load", L);

    //--Run.
    rActiveLevel->LoadFile(lua_tostring(L, 1));

    //--Finish up.
    return 0;
}
int Hook_TL_Exists(lua_State *L)
{
    //TL_Exists(sSaveName)

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        lua_pushboolean(L, false);
        LuaArgError("TL_Exists");
        return 1;
    }

    FILE *fCheckfile = fopen(lua_tostring(L, 1), "r");
    lua_pushboolean(L, fCheckfile != NULL);
    fclose(fCheckfile);
    return 1;
}
int Hook_TL_GetSaveFileList(lua_State *L)
{
    //TL_GetSaveFileList(sPath)

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        lua_pushinteger(L, 0);
        LuaArgError("TL_GetSaveFileList");
        return 1;
    }

    //--Get the text level.
    TextLevel *rActiveLevel = TextLevel::Fetch();
    if(!rActiveLevel)
    {
        lua_pushinteger(L, 0);
        LuaArgError("TL_GetSaveFileList");
        return 1;
    }

    //--Populate.
    int tSavesFound = rActiveLevel->PopulateSavePaths(lua_tostring(L, 1));
    lua_pushinteger(L, tSavesFound);
    return 1;
}
