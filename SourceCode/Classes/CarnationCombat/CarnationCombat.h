///====================================== CarnationCombat ==========================================
//--Combat handler for Project Carnation and possible derived projects. Uses most of the same mechanical
//  behaviors as the adventure version, but greatly changes the UI layout and modifies the controls.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "AbyssCombat.h"
#include "CarnationLetter.h"

///===================================== Local Structures =========================================
#include "CarnationCombatStructures.h"

///===================================== Local Definitions ========================================
//--Action Selection
#define CC_ACTIONSELECT_TICKS 15
#define CC_ACTIONSELECT_DISTANCE -25.0f

//--Textbox Sizes
#define CC_TEXTBOX_WID 1000.0f
#define CC_TEXTBOX_WID_EFF (CC_TEXTBOX_WID - 25.0f)

//--Textbox Timing
#define CC_TEXT_PAUSE_WAIT 0
#define CC_TEXT_PAUSE_LONG 1
#define CC_TEXT_PAUSE_NORMAL 2
#define CC_TEXT_PAUSE_SHORT 3
#define CC_TEXT_PAUSE_NEARZERO 4

#define CC_TEXT_SPEED_VSLOW 0
#define CC_TEXT_SPEED_SLOW 1
#define CC_TEXT_SPEED_NORMAL 2
#define CC_TEXT_SPEED_FAST 3
#define CC_TEXT_SPEED_VFAST 4

//--Background Logic
#define CC_BG_LOGIC_NONE 0
#define CC_BG_LOGIC_FROMLEFT 1
#define CC_BG_LOGIC_FROMRIGHT 2

//--Background Timing
#define CC_BG_FADEOUT_START 1
#define CC_BG_FADEOUT_STOP 15
#define CC_BG_FADEIN_STOP 25
#define CC_BG_LETTERBOX_TICKS 15

///========================================== Classes =============================================
class CarnationCombat : public AbyssCombat
{
    private:
    ///--[System]
    bool mHasInitializedCarnCombat;
    bool mStatusInterceptFlag;
    int mMouseX;
    int mMouseY;
    int mMouseZ;

    ///--[Backgrounds]
    int mLetterboxTimer;
    int mBackgroundTimer;
    StarLinkedList *mBackgroundList; //CarnCombatBGLayer *, master

    ///--[Hitbox Storage]
    StarLinkedList *mButtonList; //CarnButton *, master

    ///--[Description]
    bool mShowDescriptions;
    char *mDescriptionAbilityName;
    TwoDimensionReal mToggleDescriptionHitbox;
    StarLinkedList *mDescription;

    ///--[Action Decision]
    bool mPartyDecisionMode;
    bool mIsHandlingEffortlessAction;
    bool mSelecterBuiltCommands;
    int mPartyDecisionIndex;
    int mPartyDecisionTimer;
    int mStoredCommandInventorySlot;
    void *rEffortlessActionCharacter;
    StarLinkedList *mPartyActions; //CarnationAction *, master

    ///--[Textbox / Execution Sequence]
    bool mAnyAbilityReplied;
    bool mStatusPrintBlock;
    int mAbilityExecPhase;
    bool mIsRunningText;
    bool mTextTimerWaitForKeypress;
    int mHighestLineRendered;
    int mTextPauseTimer;
    int mTextTimer;
    float mTextTimerMax;
    float mLettersPerTick;
    StarLinkedList *mTextStrings; //CarnationString *, master

    ///--[Victory/Defeat]
    int mVictoryCalls;
    char *mVictoryHandlerPath;
    char *mDefeatHandlerPath;
    char *mShowingLevelUpPack;
    StarLinkedList *mLevelUpPacks; //CarnLevelUp *, master

    ///--[Specific Random Generation]
    uint32_t mRandomNumberSeed;

    ///--[Images]
    struct
    {
        //--System
        bool mIsReady;

        //--Fonts
        struct
        {
            StarFont *rPlayerInterfaceFont;
            StarFont *rTextBox;
            StarFont *rFontButton;
            StarFont *rFontConfirmBig;
            StarFont *rFontConfirmSmall;
            StarFont *rFontDescriptionHeading;
            StarFont *rFontDescriptionMainline;
        }Font;

        //--Images
        struct
        {
            //--UI Pieces
            StarBitmap *rFrame_Button;
            StarBitmap *rFrame_Description;
            StarBitmap *rHPBarBack;
            StarBitmap *rHPBarFill;
            StarBitmap *rMPBarBack;
            StarBitmap *rMPBarFill;
            StarBitmap *rPortraitFrame;
            StarBitmap *rTextFrame;
            StarBitmap *rTextFrameNoBack;
        }Image;
    }CarnationImg;

    protected:

    public:
    //--System
    CarnationCombat();
    virtual ~CarnationCombat();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    int GetAbilityExecutionPhase();
    int GetStoredInventorySlot();
    bool IsActionEffortless();

    //--Manipulators
    void FlagAbilityReplied();
    void TripStatusResponse();
    virtual void EnqueueApplicationPack(ApplicationPack *pPack);
    void SeedRNG(uint32_t pSeed);
    float RunRNG();
    void ClearBackgrounds();
    void RegisterBackground(const char *pName, const char *pImgPath, int pLogicType, float pLogicFactor);
    void SetPrintBlock();

    //--Core Methods
    int ResolvePauseTime();
    AdvCombatEntity *ResolveDecidingEntity();
    virtual void BeginTurn();
    void ClearText();
    void AppendText(const char *pString, uint32_t pOriginator, uint32_t pTarget);
    CarnationLetter *RetrieveLetterInSlot(int pSlot, StarLinkedList *pStringList);
    void RunLetterPackages();
    void RemoveActionsConcerning(void *pEntity);
    bool CycleCommandBackwards();
    bool CycleCommandForwards();
    virtual void PlayMonsterKnockout();
    virtual bool IsPlayerDefeated();
    virtual void ResolveBattlePosition(AdvCombatEntity *pEntity, int pTypeFlag, float &sXPos, float &sYPos);
    virtual void SetPredictionBoxHostI(const char *pBoxName, uint32_t pHostID);
    virtual void WriteLoadInfo(StarAutoBuffer *pBuffer);
    virtual AdvCombatEntity *PushAbilityOwnerWhenNull();
    virtual void SetAbilityAsActiveI(int pSlot);
    virtual void SetAbilityAsActiveS(const char *pName);
    virtual void ReresolveDescriptionAbility();

    ///--[Applications]
    virtual void HandleApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString);
    void HandleCLAppPack(CLAppPack *pPackage);

    ///--[Construction]
    virtual void Initialize();
    virtual void Reinitialize(bool pHandleMusic);
    virtual void HandleMusic();
    virtual void Construct();
    virtual void Disassemble();

    ///--[Hitboxes]
    void RegisterHitbox(const char *pString, float pX, float pY, float pW, float pH);
    void SetHitboxesResolve();
    void SetHitboxesForPrimaryAction();
    void SetHitboxesForSecondaryAction();
    void SetHitboxesForTargetSelection();

    ///--[Resolution]
    void SetVictoryHandler(const char *pPath);
    void SetDefeatHandler(const char *pPath);
    void RegisterLevelUpPack(const char *pName);
    void SetLevelUpPackProperty(const char *pName, const char *pProperty, int pValue);
    virtual void UpdateResolution();
    virtual void RenderResolution();
    void RenderStat(float pX, float pY, int pBaseStat, int pIncreaseAmt, const char *pNoIncrease, const char *pIncrease);
    void RenderLevelUpBox();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    virtual void HandlePlayerControls(bool pAnyEntitiesMoving);
    void SelectTargetInSlot(int pSlot);

    //--File I/O
    //--Drawing
    virtual void Render(bool pBypassResolution, float pAlpha);
    void RenderPlayerParty();
    void RenderEnemyParty();
    virtual void RenderAllyBars(float pAlpha);
    virtual void RenderNewTurn(float pAlpha);
    void RenderCommandWindow(AdvCombatEntity *pEntity, float pAlpha);
    void RenderTargetWindow(float pAlpha);
    void RenderSkillsWindow(AdvCombatEntity *pEntity, float pAlpha);
    void RenderCommandEntry(float pX, float pY, bool pIsHighlight, StarFont *pFont, CommandListEntry *pEntry);
    void RenderTextWindow();
    void RenderSkillDescriptionWindow(float pAlpha);
    virtual void RenderConfirmation();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnCombat_GetProperty(lua_State *L);
int Hook_CarnCombat_SetProperty(lua_State *L);


