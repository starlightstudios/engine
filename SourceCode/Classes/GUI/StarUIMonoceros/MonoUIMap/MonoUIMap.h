///========================================= MonoUIMap ============================================
//--Map implementation. Basically identical to the Adventure version but changes some of the fonts.

#pragma once

///========================================= Includes =============================================
#include "AdvUIMap.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///======================================== Translation ===========================================
///--[MonoUIMap_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIMAP_STRINGS_TOTAL 1
class MonoUIMap_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIMAP_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextMap;
        }str;
    };

    //--Functions
    MonoUIMap_Strings();
    virtual ~MonoUIMap_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIMap : public MonoUICommon, virtual public AdvUIMap
{
    protected:
    ///--[System]
    ///--[Images]
    struct
    {
        StarFont *rFont_DoubleHeading;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIMap();
    virtual ~MonoUIMap();
    virtual void Construct();

    //--Public Variables
    static MonoUIMap_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


