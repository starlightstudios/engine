///====================================== PeakFreaksBadge =========================================
//--A "Badge" that appears on the UI when the player collects a powerup. This is entirely a display
//  component and doesn't impact gameplay.

#pragma once

///========================================== Includes ============================================
#include "Definitions/Definitions.h"
#include "Definitions/Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Positioning.
#define PFB_Y_MIDDLE 300.0f

//--Timing.
#define PFB_LAG_TICKS 5
#define PFB_SEQ_TICKS_A 15
#define PFB_SEQ_TICKS_B 60
#define PFB_SEQ_TICKS_C 15
#define PFB_SEQ_COMP_A (PFB_SEQ_TICKS_A)
#define PFB_SEQ_COMP_B (PFB_SEQ_COMP_A + PFB_SEQ_TICKS_B)
#define PFB_SEQ_COMP_C (PFB_SEQ_COMP_B + PFB_SEQ_TICKS_C)
#define PFB_MAX_TICKS (PFB_SEQ_TICKS_A + PFB_SEQ_TICKS_B + PFB_SEQ_TICKS_C)

///========================================== Classes =============================================
class PeakFreaksBadge
{
    private:
    ///--[Timing]
    int mTimer;

    ///--[Position]
    float mXPosition;
    float mYPosition;
    float mYPositionLag;

    ///--[Rendering]
    SugarBitmap *rMedallion;
    SugarBitmap *rBadge;
    SugarBitmap *rTitle;

    protected:

    public:
    //--System
    PeakFreaksBadge();
    ~PeakFreaksBadge();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool IsComplete();

    //--Manipulators
    void SetXPosition(float pXPosition);
    void SetMedallion(SugarBitmap *pImage);
    void SetBadge(SugarBitmap *pImage);
    void SetTitle(SugarBitmap *pImage);

    //--Core Methods
    float ComputeYPosition(float pTimer);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


