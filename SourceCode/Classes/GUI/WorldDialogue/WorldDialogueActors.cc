//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueActor.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Property Queries]
int WorldDialogue::GetSlotOfActor(const char *pName)
{
    //--Error check.
    if(!pName) return -1;

    //--Check all actors.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        //--Skip empty slots.
        if(!mDialogueActors[i]) continue;

        //--Check base name but not aliases. Actors can sometimes have duplicate aliases but not base names.
        if(!strcasecmp(mDialogueActors[i]->GetName(), pName)) return i;
    }

    //--All checks failed, given actor is not on the field.
    return -1;
}

///--[Manipulators]
void WorldDialogue::RegisterDialogueActor(DialogueActor *pActor)
{
    //--Registers a DialogueActor to the ActorBench list. The Actor will remain there until called up. The
    //  Actor will use whatever its name was when it was registered as its reference name!
    if(!pActor) return;
    mActorBench->AddElement(pActor->GetName(), pActor, &RootObject::DeleteThis);
}
void WorldDialogue::UnregisterDialogueActor(const char *pReferenceName)
{
    //--Deletes the DialogueActor in question. If any references to the DialogueActor still exist, they are wiped.
    //  Do not use this while using an Iterator or the results are undefined.
    DialogueActor *rActor = (DialogueActor *)mActorBench->GetElementByName(pReferenceName);
    if(!rActor) return;

    //--First, remove any outstanding references.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        if(mDialogueActors[i] == rActor) mDialogueActors[i] = NULL;
    }

    //--Remove the Actor from the list. We throw it on the garbage heap so iterators won't necessarily break.
    mActorBench->SetRandomPointerToThis(rActor);
    mActorBench->LiberateRandomPointerEntry();
    Memory::AddGarbage(rActor, &RootObject::DeleteThis);
}
void WorldDialogue::AddActorToSlot(const char *pReferenceName, int pSlot)
{
    //--Creates a new actor and puts it in the requested slot. If an actor with that reference name already exists,
    //  then that actor is placed into the requested slot instead.
    if(!pReferenceName || pSlot < 0 || pSlot >= WD_DIALOGUE_ACTOR_SLOTS) return;

    //--If there was an actor in the slot, store their last bitmap.
    if(mDialogueActors[pSlot])
    {
        rPreviousActors[pSlot] = mDialogueActors[pSlot];
        rPreviousImages[pSlot] = mDialogueActors[pSlot]->GetActiveImage();
        mPreviousFadeTimers[pSlot] = 0;
    }

    //--If the name is "Null" then don't print a warning or look for it, just null it off.
    if(!strcasecmp(pReferenceName, "Null"))
    {
        mDialogueActors[pSlot] = NULL;
        mPreviousActorNames[pSlot][0] = '\0';
        mPreviousActorEmotions[pSlot][0] = '\0';
        mPreviousXOffsets[pSlot] = 0.0f;
        return;
    }

    //--Place the DialogueActor in this slot.
    mDialogueActors[pSlot] = (DialogueActor *)mActorBench->GetElementByName(pReferenceName);
    strncpy(mPreviousActorNames[pSlot], pReferenceName, WD_ACTOR_NAME_MAX);
    strncpy(mPreviousActorEmotions[pSlot], "Neutral", WD_ACTOR_NAME_MAX);

    //--Warning if the DialogueActor was not on the bench.
    if(!mDialogueActors[pSlot])
    {
        DebugManager::ForcePrint("Warning: Actor %s was not found on the bench.\n", pReferenceName);
    }

    //--Recompute positions.
    RecomputeActorPositions();
}
void WorldDialogue::MoveActorToSlotI(int pStartSlot, int pDestinationSlot)
{
    //--Moves the actor in the start slot to the destination slot. The slots will swap in this case.
    //  If you want to remove the target in the destination slot, use RemoveActor() first.
    if(pStartSlot < 0 || pStartSlot >= WD_DIALOGUE_ACTOR_SLOTS || pDestinationSlot < 0 || pDestinationSlot >= WD_DIALOGUE_ACTOR_SLOTS || pStartSlot == pDestinationSlot) return;

    //--If there was an actor in the starting slot, store their last bitmap.
    if(mDialogueActors[pStartSlot])
    {
        rPreviousActors[pStartSlot] = mDialogueActors[pStartSlot];
        rPreviousImages[pStartSlot] = mDialogueActors[pStartSlot]->GetActiveImage();
        mPreviousFadeTimers[pStartSlot] = 0;
        mPreviousXOffsets[pStartSlot] = mDialogueActors[pStartSlot]->GetActiveOffsetX();
    }

    //--If there was an actor in the destination slot, store their last bitmap.
    if(mDialogueActors[pDestinationSlot])
    {
        rPreviousActors[pDestinationSlot] = mDialogueActors[pDestinationSlot];
        rPreviousImages[pDestinationSlot] = mDialogueActors[pDestinationSlot]->GetActiveImage();
        mPreviousFadeTimers[pDestinationSlot] = 0;
        mPreviousXOffsets[pDestinationSlot] = mDialogueActors[pDestinationSlot]->GetActiveOffsetX();
    }

    //--Set.
    DialogueActor *rCopyPtr = mDialogueActors[pDestinationSlot];
    mDialogueActors[pDestinationSlot] = mDialogueActors[pStartSlot];
    mDialogueActors[pStartSlot] = rCopyPtr;

    //--Swap the storage names.
    char tStorage[WD_ACTOR_NAME_MAX];
    strncpy(tStorage, mPreviousActorNames[pDestinationSlot], WD_ACTOR_NAME_MAX);
    strncpy(mPreviousActorNames[pDestinationSlot], mPreviousActorNames[pStartSlot], WD_ACTOR_NAME_MAX);
    strncpy(mPreviousActorNames[pStartSlot], tStorage, WD_ACTOR_NAME_MAX);
    strncpy(tStorage, mPreviousActorEmotions[pDestinationSlot], WD_ACTOR_NAME_MAX);
    strncpy(mPreviousActorEmotions[pDestinationSlot], mPreviousActorEmotions[pStartSlot], WD_ACTOR_NAME_MAX);
    strncpy(mPreviousActorEmotions[pStartSlot], tStorage, WD_ACTOR_NAME_MAX);

    //--Recompute positions.
    RecomputeActorPositions();
}
void WorldDialogue::MoveActorToSlotS(const char *pSearchName, int pDestinationSlot)
{
    //--As above, but will use the name of the Actor instead of a fixed slot.
    if(!pSearchName) return;

    //--Iterate.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        if(mDialogueActors[i] && !strcasecmp(mDialogueActors[i]->GetName(), pSearchName))
        {
            MoveActorToSlotI(i, pDestinationSlot);
            return;
        }
    }

    //--Recompute positions.
    RecomputeActorPositions();
}
void WorldDialogue::RemoveActor(int pSlot)
{
    //--Nulls off the Actor in the given slot. They remain on the bench.
    if(pSlot < 0 || pSlot >= WD_DIALOGUE_ACTOR_SLOTS) return;

    //--If there was an actor in the slot, store their last bitmap.
    if(mDialogueActors[pSlot])
    {
        rPreviousActors[pSlot] = mDialogueActors[pSlot];
        rPreviousImages[pSlot] = mDialogueActors[pSlot]->GetActiveImage();
        mPreviousFadeTimers[pSlot] = 0;
        mPreviousXOffsets[pSlot] = mDialogueActors[pSlot]->GetActiveOffsetX();
    }

    //--Set.
    mDialogueActors[pSlot] = NULL;
    mPreviousActorNames[pSlot][0] = '\0';
    mPreviousActorEmotions[pSlot][0] = '\0';

    //--Recompute positions.
    RecomputeActorPositions();
}
void WorldDialogue::ClearActors()
{
    //--Wipes all DialogueActors.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        //--If there was an actor in the slot, store their last bitmap.
        if(mDialogueActors[i])
        {
            rPreviousActors[i] = mDialogueActors[i];
            rPreviousImages[i] = mDialogueActors[i]->GetActiveImage();
            mPreviousFadeTimers[i] = 0;
            mPreviousXOffsets[i] = mDialogueActors[i]->GetActiveOffsetX();
        }

        //--Clear.
        mDialogueActors[i] = NULL;
        mPreviousActorNames[i][0] = '\0';
        mPreviousActorEmotions[i][0] = '\0';
    }

    //--Recompute positions.
    RecomputeActorPositions();
}
void WorldDialogue::RestorePreviousActors()
{
    //--Restores the previous actors from the last time the dialogue was visible.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        //--Null actor:
        if(mPreviousActorNames[i][0] == '\0')
        {
        }
        else
        {
            AddActorToSlot(mPreviousActorNames[i], i);
            SetActorEmotionI(i, mPreviousActorEmotions[i]);
        }
    }
}
void WorldDialogue::RecomputeActorPositions()
{
    //--The actors on the left side of the screen clump when in a sparse party.
    if(mDialogueActors[0] == NULL || mDialogueActors[1] == NULL || mDialogueActors[2] == NULL)
    {
        mPortraitLookupsX[0] = mPortraitLookupsXHeavyLft[0];
        mPortraitLookupsX[1] = mPortraitLookupsXHeavyLft[1];
        mPortraitLookupsX[2] = mPortraitLookupsXHeavyLft[2];
    }
    //--If the first 3 slots are full, use these coordinates which are more spaced out.
    else
    {
        mPortraitLookupsX[0] = mPortraitLookupsXDefault[0];
        mPortraitLookupsX[1] = mPortraitLookupsXDefault[1];
        mPortraitLookupsX[2] = mPortraitLookupsXDefault[2];
    }
}

///--[Core Methods]
void WorldDialogue::SetActorEmotionI(int pSlot, const char *pEmotion)
{
    //--Changes the emotion in the given slot.
    if(pSlot < 0 || pSlot >= WD_DIALOGUE_ACTOR_SLOTS || !mDialogueActors[pSlot]) return;

    //--Store the previous emotion.
    rPreviousActors[pSlot] = mDialogueActors[pSlot];
    rPreviousImages[pSlot] = mDialogueActors[pSlot]->GetActiveImage();
    mPreviousFadeTimers[pSlot] = 0;
    mPreviousXOffsets[pSlot] = mDialogueActors[pSlot]->GetActiveOffsetX();

    //--Set.
    mDialogueActors[pSlot]->SetActivePortrait(pEmotion);
    strncpy(mPreviousActorEmotions[pSlot], pEmotion, WD_ACTOR_NAME_MAX);

    //--If the active image is identical, then cancel the fade.
    if(mDialogueActors[pSlot]->GetActiveImage() == rPreviousImages[pSlot])
    {
        mPreviousFadeTimers[pSlot] = WD_CHARACTER_CROSSFADE_TICKS;
    }
}
void WorldDialogue::SetActorEmotionS(const char *pReferenceName, const char *pEmotion)
{
    //--Sets the emotion for the given actor by its name. The special case "ACTIVE" sets it for whoever is speaking.
    //  Note that if multiple people are speaking, their emotions all get set.
    if(!pReferenceName || !pEmotion) return;

    //--Handle the special case:
    if(!strcasecmp(pReferenceName, "ACTIVE"))
    {
        //--Iterate.
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            //--Skip empty slots.
            if(!mDialogueActors[i]) continue;

            //--If the Actor is not darkened, they are active. Set their emotion.
            if(!mDialogueActors[i]->IsDarkened())
            {
                SetActorEmotionI(i, pEmotion);
            }
        }
        return;
    }

    //--Normal case: Search the actors.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        if(mDialogueActors[i] && !strcasecmp(mDialogueActors[i]->GetName(), pReferenceName))
        {
            SetActorEmotionI(i, pEmotion);
            return;
        }
    }
}

///--[Drawing]
void WorldDialogue::RenderDialogueActors(float pGlobalAlpha)
{
    ///--[Documentation]
    //--Renders the DialogueActors, called from the primary rendering cycle. This will only be called during
    //  a major dialogue sequence.
    float cScale = 1.00f;
    float cXOffset = 0.0f;
    if(DisplayManager::xLowDefinitionFlag)
    {
        cXOffset = 64.0f;
        cScale = cScale * 2.0f;
    }

    ///--[Mixing]
    float tAlphaFactor = 1.0f;
    if(mIsMajorSequenceMode) tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor * pGlobalAlpha);

    ///--[Left Edge]
    //--The actor rendering order is not necessarily the usual left-to-right, whoever is speaking will get bumped
    //  to the front of the line. This list is used to indicate what order they render in.
    int tRenderList[WD_DIALOGUE_ACTOR_SLOTS];
    tRenderList[0] = 0;
    tRenderList[1] = 1;
    tRenderList[2] = 2;
    tRenderList[3] = 3;
    tRenderList[4] = 4;
    tRenderList[5] = 5;
    tRenderList[6] = 6;

    //--If there is an actor in the 6th slot, then the left side has four members. Use a different set of positions.
    float *rUseCoords = mPortraitLookupsX;
    if(mDialogueActors[6] != NULL)
    {
        rUseCoords = mPortraitLookupsXHeavyLft;
    }

    ///--[Back Characters]
    //--Iterate across the rendering list for back characters.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        ///--[Error Check]
        //--Locate the DialogueActor in question. If it's NULL, skip.
        if(tRenderList[i] < 0 || tRenderList[i] >= WD_DIALOGUE_ACTOR_SLOTS) continue;

        //--Fast-access pointer.
        int tSlot = tRenderList[i];
        if(mDialogueActors[tSlot] && !mDialogueActors[tSlot]->IsFullDark()) continue;

        //--Flip flag.
        uint32_t tFlipFlags = 0x00;
        if(tSlot >= WD_DIALOGUE_ACTOR_SLOTS / 2 && tSlot < 6) tFlipFlags = FLIP_HORIZONTAL;

        //--Position.
        glTranslatef(rUseCoords[tSlot], mDimensions.mTop, 0.0f);
        glScalef(cScale, cScale, 1.0f);

        ///--[No Crossfade]
        //--When not crossfading, render the character at the listed alpha.
        if(mPreviousFadeTimers[tSlot] >= WD_CHARACTER_CROSSFADE_TICKS || mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
        {
            //--Verify the dialogue actor exists.
            if(mDialogueActors[tSlot])
            {
                //--Resolve.
                DialogueActor *rActor = mDialogueActors[tSlot];

                //--Render, storing the image.
                StarBitmap *rRenderImg = rActor->RenderAt(cXOffset, 0.0f, tAlphaFactor * pGlobalAlpha, tFlipFlags);

                //--If the image needs to be placed on the un-load list, do that here.
                if(xUnloadDialoguePortraitsWhenClosing && !mrAllShownDialoguePortraits->IsElementOnList(rRenderImg))
                {
                    mrAllShownDialoguePortraits->AddElement("X", rRenderImg);
                }
            }
        }
        ///--[Crossfade]
        //--When crossfading, render the previous image and the current image at complementary alphas.
        else
        {
            //--Compute alphas. First, the "Base" alphas if the dialogue was at full opacity.
            float cFadeOutAlphaBase = (1.0f - EasingFunction::QuadraticInOut(mPreviousFadeTimers[tSlot], WD_CHARACTER_CROSSFADE_TICKS / 2));
            float cFadeInAlphaBase  = 1.6f - cFadeOutAlphaBase;

            //--Clamp the fade-in since it can be above 1.0f.
            if(cFadeInAlphaBase > 1.0f) cFadeInAlphaBase = 1.0f;

            //--Now multiply by the global alpha.
            float cFadeOutAlpha = cFadeOutAlphaBase * pGlobalAlpha * tAlphaFactor;
            float cFadeInAlpha  = cFadeInAlphaBase  * pGlobalAlpha * tAlphaFactor;

            //--Fade-out character. Doesn't always exist. Does not need to be stored in the unload list because it's already there.
            if(rPreviousActors[tSlot] && rPreviousImages[tSlot])
            {
                rPreviousActors[tSlot]->RenderAt(rPreviousImages[i], cXOffset, 0.0f, cFadeOutAlpha, tFlipFlags);
            }

            //--Fade-in character. Doesn't always exist.
            if(mDialogueActors[tSlot])
            {
                //--Verify.
                DialogueActor *rActor = mDialogueActors[tSlot];

                //--Render, storing the image.
                StarBitmap *rRenderImg = rActor->RenderAt(cXOffset, 0.0f, cFadeInAlpha, tFlipFlags);

                //--If the image needs to be placed on the un-load list, do that here. Also issue a load instruction for it.
                if(xUnloadDialoguePortraitsWhenClosing && !mrAllShownDialoguePortraits->IsElementOnList(rRenderImg))
                {
                    mrAllShownDialoguePortraits->AddElement("X", rRenderImg);
                }
            }
        }

        ///--[Clean]
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-rUseCoords[tSlot], -mDimensions.mTop, 0.0f);
    }

    ///--[Front Characters]
    //--Now do it again for the front characters.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        ///--[Error Check]
        //--Locate the DialogueActor in question. If it's NULL, skip.
        if(tRenderList[i] < 0 || tRenderList[i] >= WD_DIALOGUE_ACTOR_SLOTS) continue;

        //--Fast-access pointer.
        int tSlot = tRenderList[i];
        if(mDialogueActors[tSlot] && mDialogueActors[tSlot]->IsFullDark()) continue;

        //--Flip flag.
        uint32_t tFlipFlags = 0x00;
        if(tSlot >= WD_DIALOGUE_ACTOR_SLOTS / 2 && tSlot < 6) tFlipFlags = FLIP_HORIZONTAL;

        //--Position.
        glTranslatef(rUseCoords[tSlot], mDimensions.mTop, 0.0f);
        glScalef(cScale, cScale, 1.0f);

        ///--[No Crossfade]
        //--When not crossfading, render the character at the listed alpha.
        if(mPreviousFadeTimers[tSlot] >= WD_CHARACTER_CROSSFADE_TICKS || mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
        {
            //--Check to make sure the actor exists.
            if(mDialogueActors[tSlot])
            {
                //--Resolve.
                DialogueActor *rActor = mDialogueActors[tSlot];

                //--Render, storing the image.
                StarBitmap *rRenderImg = rActor->RenderAt(cXOffset, 0.0f, pGlobalAlpha * tAlphaFactor, tFlipFlags);

                //--If the image needs to be placed on the un-load list, do that here.
                if(xUnloadDialoguePortraitsWhenClosing && !mrAllShownDialoguePortraits->IsElementOnList(rRenderImg))
                {
                    mrAllShownDialoguePortraits->AddElement("X", rRenderImg);
                }
            }
        }
        ///--[Crossfade]
        //--When crossfading, render the previous image and the current image at complementary alphas.
        else
        {
            //--Compute alphas. First, the "Base" alphas if the dialogue was at full opacity.
            float cFadeOutAlphaBase = (1.0f - EasingFunction::QuadraticInOut(mPreviousFadeTimers[tSlot], WD_CHARACTER_CROSSFADE_TICKS / 2));
            float cFadeInAlphaBase = 1.6f - cFadeOutAlphaBase;

            //--Clamp the fade-in since it can be above 1.0f.
            if(cFadeInAlphaBase > 1.0f) cFadeInAlphaBase = 1.0f;

            //--Now multiply by the global alpha.
            float cFadeOutAlpha = cFadeOutAlphaBase * pGlobalAlpha * tAlphaFactor;
            float cFadeInAlpha  = cFadeInAlphaBase  * pGlobalAlpha * tAlphaFactor;

            //--Fade-out character. Doesn't always exist. Does not need to store the image for unload.
            if(rPreviousActors[tSlot] && rPreviousImages[tSlot])
            {
                rPreviousActors[tSlot]->RenderAt(rPreviousImages[i], cXOffset, 0.0f, cFadeOutAlpha, tFlipFlags);
            }

            //--Fade-in character. Doesn't always exist.
            if(mDialogueActors[tSlot])
            {
                //--Verify.
                DialogueActor *rActor = mDialogueActors[tSlot];

                //--Render, storing the image.
                StarBitmap *rRenderImg = rActor->RenderAt(cXOffset, 0.0f, cFadeInAlpha, tFlipFlags);

                //--If the image needs to be placed on the un-load list, do that here.
                if(xUnloadDialoguePortraitsWhenClosing && !mrAllShownDialoguePortraits->IsElementOnList(rRenderImg))
                {
                    mrAllShownDialoguePortraits->AddElement("X", rRenderImg);
                }
            }
        }

        ///--[Clean]
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-rUseCoords[tSlot], -mDimensions.mTop, 0.0f);
    }

    ///--[Clean]
    //--Reset mixer.
    StarlightColor::ClearMixer();
}
