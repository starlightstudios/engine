//--Base
#include "AudioManager.h"

//--Classes
#include "AudioPackageSample.h"
#include "AudioPackageStream.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
//--Managers
#include "DebugManager.h"

void AudioManager::RegisterAdvanced(const char *pName, const char *pPath, const char *pProperties)
{
    ///--[Documentation]
    //--Registers an audio package using a property string, which can contain an indefinite number of properties.
    //  Properties are seperated by a '|' bar. Ex: "Stream|Memory"
    //--If not enough properties are provided to resolve needed information, or a property is provided
    //  for the wrong audio type (ex: a stream property for a sample) then an error is barked
    //  and the audio package will not be registered.
    if(!pName || !pPath || !pProperties) return;

    //--Properties flags.
    AudioPackage *rGenericPackage = NULL;
    bool tIsStream = false;
    bool tIsSample = false;
    bool tIsMusic = false;
    bool tIsSound = false;
    float tGlobalVolume = 1.00f;
    float tLocalVolume = 1.00f;
    int tUseAbstractVolumeSlot = -1;
    bool tIsMemoryStream = false;
    float tLoopStart = -1.0f;
    float tLoopEnd = -1.0f;

    //--Break into a sublist.
    StarLinkedList *tStrings = Subdivide::SubdivideStringToList(pProperties, "|");

    //--Iterate across properties.
    char *rString = (char *)tStrings->PushIterator();
    while(rString)
    {
        //--Subdivide the string further, in case it contains optional arguments. There will always
        //  be at least one string.
        StarLinkedList *tSubStrings = Subdivide::SubdivideStringToList(rString, ":");

        //--Resolve basic data.
        int tArguments = tSubStrings->GetListSize();
        const char *rSwitchType = (const char *)tSubStrings->GetElementBySlot(0);

        //--Stream. Streams from the hard drive, or from memory.
        if(!strcasecmp(rSwitchType, "Stream"))
        {
            tIsStream = true;
        }
        //--Sample. Can be played multiple times, from memory. Used as SFX.
        else if(!strcasecmp(rSwitchType, "Sample"))
        {
            tIsSample = true;
        }
        //--Music. Typically only one plays at a time, though layered tracks bypass this.
        else if(!strcasecmp(rSwitchType, "Music"))
        {
            tIsMusic = true;
            tGlobalVolume = AudioManager::xMusicVolume;
        }
        //--Sound. Plays without restriction, never loops.
        else if(!strcasecmp(rSwitchType, "Sound"))
        {
            tIsSound = true;
            tGlobalVolume = AudioManager::xSoundVolume;
        }
        //--Global Volume Override.
        else if(!strcasecmp(rSwitchType, "GlobalVol") && tArguments >= 2)
        {
            const char *rArg1 = (const char *)tSubStrings->GetElementBySlot(1);
            tGlobalVolume = atof(rArg1);
        }
        //--Local Volume Override.
        else if(!strcasecmp(rSwitchType, "LocalVol") && tArguments >= 2)
        {
            const char *rArg1 = (const char *)tSubStrings->GetElementBySlot(1);
            tLocalVolume = atof(rArg1);
        }
        //--Abstract Volume Override.
        else if(!strcasecmp(rSwitchType, "AbstractVolSlot") && tArguments >= 2)
        {
            const char *rArg1 = (const char *)tSubStrings->GetElementBySlot(1);
            tUseAbstractVolumeSlot = atoi(rArg1);
        }
        //--Is a memory stream.
        else if(!strcasecmp(rSwitchType, "Memory"))
        {
            tIsMemoryStream = true;
        }
        //--Set loop points.
        else if(!strcasecmp(rSwitchType, "Loop") && tArguments >= 3)
        {
            const char *rArg1 = (const char *)tSubStrings->GetElementBySlot(1);
            const char *rArg2 = (const char *)tSubStrings->GetElementBySlot(2);
            tLoopStart = atof(rArg1);
            tLoopEnd = atof(rArg2);
        }

        //--Clean.
        delete tSubStrings;

        //--Next.
        rString = (char *)tStrings->AutoIterate();
    }

    //--Clean.
    delete tStrings;

    ///--[Error Check]
    //--Check if there was enough information to actually create a package. If not, warn the user.
    bool tIsEnoughInfo = true;

    //--Stream/Sample is missing. This part is important!
    if(!tIsStream && !tIsSample)
    {
        tIsEnoughInfo = false;
        DebugManager::ForcePrint("AudioManager::RegisterAdvanced() - Failed. Could not resolve stream or sample from provided properties.\n");
    }

    //--If both Stream and Sample are set, that's also a problem.
    if(tIsStream && tIsSample)
    {
        tIsEnoughInfo = false;
        DebugManager::ForcePrint("AudioManager::RegisterAdvanced() - Failed. Both stream and sample are set. Failing.\n");
    }

    //--Music/Sound is missing.
    if(!tIsMusic && !tIsSound)
    {
        tIsEnoughInfo = false;
        DebugManager::ForcePrint("AudioManager::RegisterAdvanced() - Failed. Could not resolve music or sound from provided properties.\n");
    }

    //--If both Music and Sound are set, that's also a problem.
    if(tIsMusic && tIsSound)
    {
        tIsEnoughInfo = false;
        DebugManager::ForcePrint("AudioManager::RegisterAdvanced() - Failed. Both music and sound are set. Failing.\n");
    }

    //--Memory Stream. Cannot apply to sample.
    if(tIsSample && tIsMemoryStream)
    {
        tIsEnoughInfo = false;
        DebugManager::ForcePrint("AudioManager::RegisterAdvanced() - Failed. Attempted to set memory mode on a sample. Samples cannot be memory streams.\n");
    }

    //--If this flag got switched to false, stop here.
    if(!tIsEnoughInfo)
    {
        DebugManager::ForcePrint(" Property String: %s\n", pProperties);
        return;
    }

    ///--[Stream Type]
    if(tIsStream)
    {
        //--Create.
        AudioPackageStream *nPackage = new AudioPackageStream();
        rGenericPackage = nPackage;

        //--Memory Stream.
        if(tIsMemoryStream) nPackage->SetMemoryStream();
    }
    ///--[Sample Type]
    else if(tIsSample)
    {
        //--Create.
        AudioPackageSample *nPackage = new AudioPackageSample();
        rGenericPackage = nPackage;
    }

    ///--[Common Properties]
    //--Abstract volume slot. Must be set before other volumes so they correctly resolve it when
    //  setting volumes.
    if(tUseAbstractVolumeSlot != -1) rGenericPackage->SetAbstractSlot(tUseAbstractVolumeSlot);

    //--Set the global/local volumes. Default is 1.00f.
    rGenericPackage->SetVolume(tLocalVolume);
    rGenericPackage->SetGlobalVolume(tGlobalVolume);

    ///--[Registery]
    //--Music package.
    if(tIsMusic)
    {
        RegisterMusic(pName, rGenericPackage);
    }
    //--Sound package.
    else
    {
        RegisterSound(pName, rGenericPackage);
    }

    //--Order the package to load.
    rGenericPackage->Load(pPath);

    ///--[Looping]
    //--If the loop points are both not negative, set them.
    if(tIsStream && tLoopStart >= 0.0f && tLoopEnd >= 0.0f)
    {
        AudioPackageStream *rStreamPackage = (AudioPackageStream *)rGenericPackage;
        rStreamPackage->AddLoop(tLoopStart, tLoopEnd);
    }
}
