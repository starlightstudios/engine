//--Base
#include "MonoUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///--[Debug]
//#define MONOUIEQUIP_DEBUG
#ifdef MONOUIEQUIP_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
MonoUIEquip::MonoUIEquip()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "MEQP");

    ///--[Visiblity]
    mVisibilityTimerMax = cMonoVisTicks;

    ///--[Images]
    ///--[ ======= AdvUIEquip ======= ]
    ///--[Constants]
    cReplaceEntriesPerPage = 14;

    ///--[Cursor]
    ///--[Help Strings]
    ///--[Colors]
    ///--[Images]
    ///--[Rendering Constants]
    cSlotNameX       = 448.0f;
    cSlotNameY       = 192.0f;
    cSlotH           =  56.0f;
    cEquipIconX      = 452.0f;
    cEquipIconY      = 214.0f;
    cEquipNameX      = 473.0f;
    cEquipNameY      = 214.0f;
    cGemIconX        = 690.0f;
    cGemIconY        = 212.0f;
    cGemIconW        =  24.0f;
    cReplaceStartY   = 241.0f;
    cReplaceStartH   =  22.0f;
    cReplaceDividerH = 438.0f;

    ///--[ ====== MonoUIEquip ======= ]
    ///--[System]
    ///--[Scrollbar]
    mEqpSkip = 0;

    ///--[Colors]
    mMonoTitleColor.    SetRGBAI(236, 211, 160, 255);
    mMonoSubtitleColor. SetRGBAI(181, 112,  41, 255);
    mMonoParagraphColor.SetRGBAI(214, 188, 219, 255);

    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    ///--[Verification Pack]
    //--Add the MonoImages structure to the verify list.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIEquip::~MonoUIEquip()
{
}
void MonoUIEquip::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Equipment UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Equipment UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Monoceros Help UI",                &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIEquip_Strings *MonoUIEquip::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIEquip_Strings::MonoUIEquip_Strings()
{
    //--Local name: "MonoUIEquip"
    MonoUIEquip::xrStringData = this;

    //--Strings
    str.mTextEquipment    = InitializeString("MonoUIEquip_mEquipment");      //"Equipment"
    str.mTextSlots        = InitializeString("MonoUIEquip_mSlots");          //"Slots"
    str.mTextGems         = InitializeString("MonoUIEquip_mGems");           //"Gems"
    str.mTextStatistics   = InitializeString("MonoUIEquip_mStatistics");     //"Statistics"
    str.mTextHelpMenu     = InitializeString("MonoUIEquip_mHelpMenu");       //"Help Menu"
    str.mTextImmune       = InitializeString("MonoUIEquip_mImmune");         //"Immune"
    str.mTextResists      = InitializeString("MonoUIEquip_mResists");        //"Resists"
    str.mTextDamage       = InitializeString("MonoUIEquip_mDamage");         //"Damage"
    str.mTextEmpty        = InitializeString("MonoUIEquip_m(Empty)");        //"(Empty)"
    str.mTextEmptyUnequip = InitializeString("MonoUIEquip_mEmpty(Unequip)"); //"Empty (Unequip)"

    //--Help Sequence
    char tBuffer[100];
    for(int i = 0; i < 17; i ++)
    {
        sprintf(tBuffer, "MonoUIEquip_mHelpText%02i", i);
        str.mTextHelp[i] = InitializeString(tBuffer);
    }

    //--Control Sequence
    str.mTextControlShowHelp    = InitializeString("MonoUIBase_mControlShowHelp");   //"[IMG0] Show Help"
    str.mTextControlItemDetails = InitializeString("MonoUIBase_mControlItemDetais"); //"[IMG0] Item Details"
    str.mTextControlHoldScroll  = InitializeString("MonoUIBase_mControlHolyScroll"); //"[IMG0] (Hold) Scroll x10"
    str.mTextControlPrevChar    = InitializeString("MonoUIBase_mControlPrevChar");   //"[IMG0] or [IMG1]+[IMG2] Previous Character"
    str.mTextControlNextChar    = InitializeString("MonoUIBase_mControlNextChar");   //"[IMG0] or [IMG1]+[IMG2] Next Character"
}
MonoUIEquip_Strings::~MonoUIEquip_Strings()
{
    for(int i = 0; i < MONOUIEQUIP_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIEquip_Strings::GetSize()
{
    return MONOUIEQUIP_STRINGS_TOTAL;
}
void MonoUIEquip_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIEQUIP_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void MonoUIEquip::TakeForeground()
{
    ///--[Call Base Version]
    //--Base version handles most of the same variables.
    AdvUIEquip::TakeForeground();

    //--Skip.
    mEqpSkip = 0;

    ///--[String Override]
    //--"Show Help" string
    mShowHelpString->SetString(xrStringData->str.mTextControlShowHelp);
    mShowHelpString->CrossreferenceImages();

    //--"Item Details" string
    mInspectString->SetString(xrStringData->str.mTextControlItemDetails);
    mInspectString->CrossreferenceImages();

    //--"Scroll Fast" string.
    mScrollFastString->SetString(xrStringData->str.mTextControlHoldScroll);
    mScrollFastString->CrossreferenceImages();

    //--"Previous Character" string
    mCharacterLftString->SetString(xrStringData->str.mTextControlPrevChar);
    mCharacterLftString->CrossreferenceImages();

    //--"Next Character" string
    mCharacterRgtString->SetString(xrStringData->str.mTextControlNextChar);
    mCharacterRgtString->CrossreferenceImages();
}

///======================================= Core Methods ===========================================
void MonoUIEquip::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Set Lines]
    //--Offset.
    mHelpControlOffset = cxMonoBigControlOffset;

    //--Common lines.
    SetHelpString(i, xrStringData->str.mTextHelp[0]);
    SetHelpString(i, xrStringData->str.mTextHelp[1]);
    SetHelpString(i, xrStringData->str.mTextHelp[2]);
    SetHelpString(i, xrStringData->str.mTextHelp[3]);
    SetHelpString(i, xrStringData->str.mTextHelp[4]);
    SetHelpString(i, xrStringData->str.mTextHelp[5]);
    SetHelpString(i, xrStringData->str.mTextHelp[6]);
    SetHelpString(i, xrStringData->str.mTextHelp[7]);
    SetHelpString(i, xrStringData->str.mTextHelp[8]);
    SetHelpString(i, xrStringData->str.mTextHelp[9], 2, "F1", "Cancel");
    SetHelpString(i, xrStringData->str.mTextHelp[10]);
    SetHelpString(i, xrStringData->str.mTextHelp[11]);
    SetHelpString(i, xrStringData->str.mTextHelp[12]);

    //--Damage Icon Legend.
    mHelpMenuStrings[i]->SetString(xrStringData->str.mTextHelp[13]);
    mHelpMenuStrings[i]->AllocateImages(4);
    mHelpMenuStrings[i]->SetImageP(0, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Physical"));
    mHelpMenuStrings[i]->SetImageP(1, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Bane"));
    mHelpMenuStrings[i]->SetImageP(2, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Arcane"));
    mHelpMenuStrings[i]->SetImageP(3, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Flame"));
    i ++;
    SetHelpString(i, xrStringData->str.mTextHelp[14]);

    //--Statistics Icon Legend.
    SetHelpString(i, xrStringData->str.mTextHelp[15]);
    mHelpMenuStrings[i]->SetString(xrStringData->str.mTextHelp[16]);
    mHelpMenuStrings[i]->AllocateImages(6);
    mHelpMenuStrings[i]->SetImageP(0, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Health"));
    mHelpMenuStrings[i]->SetImageP(1, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Attack"));
    mHelpMenuStrings[i]->SetImageP(2, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Accuracy"));
    mHelpMenuStrings[i]->SetImageP(3, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Evade"));
    mHelpMenuStrings[i]->SetImageP(4, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Initiative"));
    mHelpMenuStrings[i]->SetImageP(5, cxMonoBigIconOffset, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Protection"));
    i ++;

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void MonoUIEquip::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Depending on what mode is currently active and where the cursor is, modifies the cursor's
    //  target position and size.

    //--Selecting Equipment slot or Gem slot
    if(!mIsReplacing)
    {
        //--Effective cursor after scrollbar.
        int tEffectiveCursor = mEquipmentCursor - mEqpSkip;

        //--Not a gem slot:
        if(mGemCursor == -1)
        {
            mHighlightPos.MoveTo(421.0f, 185.0f + (tEffectiveCursor * 71.0f), cMonoCurTicks);
            mHighlightSize.MoveTo(282.0f, 61.0f, cMonoCurTicks);
        }
        //--Gem slot:
        else
        {
            mHighlightPos.MoveTo(688.0f + (24.0f * mGemCursor), 218.0f + (tEffectiveCursor * 71.0f), cMonoCurTicks);
            mHighlightSize.MoveTo(30.0f, 30.0f, cMonoCurTicks);
        }
    }
    //--Replacing.
    else
    {
        //--Modify replacement cursor by offset.
        int tUseCursor = mReplacementCursor - mReplacementSkip;
        mHighlightPos.MoveTo(416.0f, 262.0f + (30.0f * tUseCursor), cMonoCurTicks);
        mHighlightSize.MoveTo(270.0f, 25.0f, cMonoCurTicks);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void MonoUIEquip::UpdateSelection()
{
    ///--[Documentation]
    //--When the player is selecting which item or gem to inspect or replace, this routine handles
    //  the input and logic. They can also switch characters here.
    //--Controls generally return out after executing, to prevent odd cases like accidentally switching
    //  items and pressing activate to activate the wrong item.
    //--Same as the base call but handles scrollbar.

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AudioManager *rAudioManager = AudioManager::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Error Check]
    //--Retrieve the active character. The character must exist.
    AdvCombatEntity *rActiveEntity = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) return;

    ///--[Details Handler]
    //--Immediately toggles details mode on whatever is highlighted. Stops the rest of the update.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--If currently in details mode, exit details mode.
        if(mIsShowingDetails)
        {
            mIsShowingDetails = false;
            rAudioManager->PlaySound("Menu|Select");
            return;
        }

        //--Run routine to check if there is a details item. If nothing is under the cursor, this will
        //  automatically unset details mode.
        mIsShowingDetails = true;
        ReresolveDetailsItem();

        //--If details mode did not get unset by the routine, play the select sound.
        if(mIsShowingDetails)
        {
            rAudioManager->PlaySound("Menu|Select");
        }
        //--It failed, so play a failure sound.
        else
        {
            rAudioManager->PlaySound("Menu|Failed");
        }
        return;
    }

    ///--[Character Switching]
    //--Decrement the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("UpLevel") && !rControlManager->IsFirstPress("DnLevel")) ||
       (rControlManager->IsDown("Ctrl")          &&  rControlManager->IsFirstPress("Left")  ))
    {
        ScrollCharacter(-1);
        return;
    }

    //--Increment the character index by one. Wraps around to maximum.
    if((rControlManager->IsFirstPress("DnLevel") && !rControlManager->IsFirstPress("UpLevel")) ||
       (rControlManager->IsDown("Ctrl")          &&  rControlManager->IsFirstPress("Right") ))
    {
        ScrollCharacter(1);
        return;
    }

    ///--[Moving Up and Down]
    //--Pressing up decrements the equipment selection slot. It wraps to the last slot.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Run routine.
        //if(StandardCursorNoScroll(mEquipmentCursor, -1, rActiveEntity->GetEquipmentSlotsTotal(), true))
        if(StandardCursor(mEquipmentCursor, mEqpSkip, -1, rActiveEntity->GetEquipmentSlotsTotal(), cxMonoEqpScrollBuf, cxMonoEqpScrollPage, true))
        {
            mGemCursor = -1;
            ReresolveDetailsItem();
            RecomputeCursorPositions();
        }

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Pressing down increments the equipment selection slot. It wraps to the first slot.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Run routine.
        //if(StandardCursorNoScroll(mEquipmentCursor, 1, rActiveEntity->GetEquipmentSlotsTotal(), true))
        if(StandardCursor(mEquipmentCursor, mEqpSkip, 1, rActiveEntity->GetEquipmentSlotsTotal(), cxMonoEqpScrollBuf, cxMonoEqpScrollPage, true))
        {
            mGemCursor = -1;
            ReresolveDetailsItem();
            RecomputeCursorPositions();
        }

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Selecting Gems]
    //--Pressing Right increments the gem cursor. It wraps around to -1. Holding down Ctrl when doing this will
    //  increment and decrement the character instead and then return out, so it is not handled.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Check the item in this slot. If empty, do nothing.
        AdventureItem     *rEquipment = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
        EquipmentSlotPack *rEquipSlot = rActiveEntity->GetEquipmentSlotPackageI(mEquipmentCursor);
        if(!rEquipment || !rEquipSlot) return;

        //--Item in slot. Check if it has a gem slot. If it has no gem slots, or if the equipment
        //  slot itself disallows gems, ignore this.
        int tGemSlots = rEquipment->GetGemSlots();
        if(tGemSlots < 1) return;
        if(rEquipSlot->mNoGems) return;

        //--Wrap check. The gem cursor wraps to -1 (no gem selected) rather than 0.
        mGemCursor ++;
        if(mGemCursor >= tGemSlots) mGemCursor = -1;

        //--Recheck cursor.
        ReresolveDetailsItem();
        RecomputeCursorPositions();

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Pressing Left decrements the gem cursor. It wraps to the max gems on the given item.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Check the item in this slot. If empty, do nothing.
        AdventureItem     *rEquipment = rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
        EquipmentSlotPack *rEquipSlot = rActiveEntity->GetEquipmentSlotPackageI(mEquipmentCursor);
        if(!rEquipment || !rEquipSlot) return;

        //--Item in slot. Check if it has a gem slot. If it has no gem slots, or if the equipment
        //  slot itself disallows gems, ignore this.
        int tGemSlots = rEquipment->GetGemSlots();
        if(tGemSlots < 1) return;
        if(rEquipSlot->mNoGems) return;

        //--Wrap check.
        mGemCursor --;
        if(mGemCursor < -1) mGemCursor = tGemSlots - 1;

        //--Recheck cursor.
        ReresolveDetailsItem();
        RecomputeCursorPositions();

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Activate]
    //--Begin replacing the equipment or gem in question.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If the user is in details mode, do nothing.
        if(mIsShowingDetails) return;

        //--Run subroutine to check if replacements mode can be activated. If it returns false, fail.
        if(!ActivateReplacement())
        {
            rAudioManager->PlaySound("Menu|Failed");
            return;
        }

        //--Success. Play a sound and recompute cursor position.
        RecomputeCursorPositions();
        rAudioManager->PlaySound("Menu|Select");
        return;
    }

    ///--[Cancel]
    //--Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--If in details mode, exit details mode.
        if(mIsShowingDetails)
        {
            mIsShowingDetails = false;
            rAudioManager->PlaySound("Menu|Select");
            return;
        }

        //--Otherwise, mark this object as exiting.
        FlagExit();
        //SetToMainMenu(AM_MAIN_EQUIPMENT);
        rAudioManager->PlaySound("Menu|Select");
        return;
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
