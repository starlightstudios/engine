//--Base
#include "CorrupterMenu.h"

//--Classes
#include "MagicPack.h"
#include "VisualLevel.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"

TwoDimensionReal CorrupterMenu::CalculateUnrolledSize(float pLft, float pTop, float pRgt)
{
    //--Computes and returns the size needed for the unrolled magic menu.
    TwoDimensionReal nNewDim;

    //--Size constants.
    float cWid = pRgt - pLft;

    //--Get how many spells we have, and get the size from that.
    mLastUnrolledSize = CM_BTN_INDENT * 2.0f;
    MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
    while(rMagicPack)
    {
        //--If this spell is context-only, ignore it.
        if(!rMagicPack->IsCastFromCorrupterMenu())
        {
            rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
            continue;
        }

        //--Next.
        mLastUnrolledSize = mLastUnrolledSize + CM_BTN_TEXT_SIZE_SMALL;
        rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
    }

    //--All done.
    nNewDim.SetWH(pLft, pTop, cWid, mLastUnrolledSize);
    return nNewDim;
}
bool CorrupterMenu::UpdateUnrolledMenu(TwoDimensionReal pDimensions)
{
    //--Updates the menu, based on the dimensions provided by the caller. We don't know where the object
    //  is until runtime!
    //--Returns true if it handled the update, false otherwise.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Reset.
    mHighlightedUnrolledSpell = -1;

    //--Fail if the mouse is not over the window.
    if(!IsPointWithin2DReal(tMouseX, tMouseY, pDimensions)) return false;

    //--Offset.
    tMouseX = tMouseX - pDimensions.mLft - CM_BTN_INDENT;
    tMouseY = tMouseY - pDimensions.mTop - CM_BTN_INDENT;

    //--Calculate which slot is highlighted.
    mHighlightedUnrolledSpell = tMouseY / CM_BTN_TEXT_SIZE_SMALL;

    //--User left-clicked on this pane: Try to cast the spell.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Cycle to the spell in question. It may not be the same index because spells that are cast from the
        //  context menu do not appear on this submenu.
        int i = 0;
        MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
        while(rMagicPack)
        {
            //--If this spell is context-only, ignore it.
            if(!rMagicPack->IsCastFromCorrupterMenu())
            {
                rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
                continue;
            }

            //--If this is the slot in question, cast it.
            if(i == mHighlightedUnrolledSpell)
            {
                //--Check if we can actually cast it. The script is called here.
                rMagicPack->RecheckMenuCast();
                if(rMagicPack->CanCastNow())
                {
                    rMagicPack->Cast(EntityManager::Fetch()->GetLastPlayerEntity());
                    AudioManager::Fetch()->PlaySound("Menu|Select");
                }

                //--In either case, break out of the loop.
                MagicPack::xMagicList->PopIterator();
                return true;
            }

            //--Next.
            i ++;
            rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
        }
    }

    //--Did not handle the update.
    return false;
}
void CorrupterMenu::RenderUnrolledMenu(TwoDimensionReal pDimensions)
{
    //--Render the menu. This will only be called if there is something to render, the option checking occurs before this call.
    if(!Images.mIsReady || pDimensions.GetWidth() < 10.0f || pDimensions.GetHeight() < 10.0f) return;

    //--Backing.
    PandemoniumLevel::RenderBorder(pDimensions.mLft, pDimensions.mTop, pDimensions.mRgt, pDimensions.mBot, 3.0f, StarlightColor::MapRGBI(192, 192, 192), StarlightColor::MapRGBAI(0, 0, 0, 192));

    //--Setup.
    float tXCursor = pDimensions.mLft + CM_BTN_INDENT;
    float tYCursor = pDimensions.mTop + CM_BTN_INDENT;

    //--List of magic spells that can be cast from the menu. Context-only spells are ignored.
    int i = 0;
    MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
    while(rMagicPack)
    {
        //--If this spell is context-only, ignore it.
        if(!rMagicPack->IsCastFromCorrupterMenu())
        {
            rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
            continue;
        }

        //--If this spell is selected, render a highlight.
        if(mHighlightedUnrolledSpell == i)
        {
            mHighlightCol.SetAsMixer();
        }
        //--If the player cannot afford the spell, red it out.
        else if(rMagicPack->GetMPCost() > xPlayerMP)
        {
            mUnavailableCol.SetAsMixer();
        }
        //--Normal case.
        else
        {
            mNormalTextCol.SetAsMixer();
        }

        //--Resolve the widest scale for the name.
        float tScale = VisualLevel::ResolveLargestScale(Images.Data.rUIFontSmall, pDimensions.GetWidth() - (CM_BTN_INDENT * 2.0f), rMagicPack->GetName());

        //--Render the name.
        StarFont::xScaleAffectsY = false;
        Images.Data.rUIFontSmall->DrawText(tXCursor, tYCursor, 0, tScale, rMagicPack->GetName());
        StarFont::xScaleAffectsY = true;

        //--Next.
        i ++;
        tYCursor = tYCursor + CM_BTN_TEXT_SIZE_SMALL;
        rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
    }
}
