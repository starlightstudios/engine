//--Base
#include "AdvUIVendor.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///======================================== Activation ============================================
void AdvUIVendor::ActivateModeGems()
{
    ///--[Documentation]
    //--Switches to Gems mode.

    ///--[Variables]
    //--Common subroutines.
    ActivateMode(mIsGemsMode);
    ResetCursor();

    //--Make sure merge mode is turned off.
    mIsMergeMode = false;

    ///--[Other]
    //--Help.
    RefreshGemMenuHelp();

    //--Inventory resets the list of gems.
    AdventureInventory::Fetch()->BuildGemList();
}
void AdvUIVendor::RefreshGemMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    SetHelpString(i, "Gems are a bit different from the other options at a vendor. Gems can be merged ");
    SetHelpString(i, "together to gain the properties of both.");
    SetHelpString(i, "For a more complete description of gems and how they work, see the Journal's ");
    SetHelpString(i, "Combat Glossary entry on gems.");
    SetHelpString(i, "");
    SetHelpString(i, "You can disassemble a gem at a vendor to return the original gems, but any adamantite");
    SetHelpString(i, "and money spent on merging them will be lost.");
    SetHelpString(i, "Otherwise, you can merge together any gem in your inventory capable of merging. This");
    SetHelpString(i, "includes gems a character has equipped.");
    SetHelpString(i, "");
    SetHelpString(i, "To start merging, select a gem with [IMG0].", 1, "Activate");
    SetHelpString(i, "");
    SetHelpString(i, "");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}

///===================================== Property Queries =========================================
float AdvUIVendor::GetGemsCursorNameWidth(float &sLeftPos)
{
    ///--[Documentation]
    //--In gems mode, resolves the highlighted item and returns its width. Used for cursor size computations.
    //  Also adjusts the leftmost position of the name as the gemcutter UI is slightly offset from buy/sell.
    float tNameWid = 100.0f;

    ///--[Adjust Left Position]
    //--Not merge mode.
    if(!mIsMergeMode)
    {
        sLeftPos = 314.0f;
    }
    //--Merge mode.
    else
    {
        sLeftPos = 106.0f;
    }

    ///--[Common]
    //--Get name width.
    StarLinkedList *rGemList = AdventureInventory::Fetch()->GetGemList();
    AdventureItem *rItem = (AdventureItem *)rGemList->GetElementBySlot(mCursor);
    if(rItem)
    {
        tNameWid = AdvImages.rFont_Mainline->GetTextWidth(rItem->GetName());
    }

    //--Name length clamp.
    if(tNameWid > cxAdvMaxGemNameWid) tNameWid = cxAdvMaxGemNameWid;

    ///--[Finish]
    return tNameWid;
}

///========================================== Update ==============================================
void AdvUIVendor::UpdateModeGems()
{
    ///--[Documentation]
    //--Update handler for Gems mode.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rGemsList = rInventory->GetGemList();

    ///--[Timers]
    //--Standard timers.
    StandardTimer(mHelpVisibilityTimer,      0, cHlpTicks,                mIsShowingHelp);
    StandardTimer(mMergeHelpVisibilityTimer, 0, cHlpTicks,                mIsShowingMergeHelp);
    StandardTimer(mGemMergeTimer,            0, cxAdvVisTicks,            mIsMergeMode);
    StandardTimer(mGemConfirmMergeTimer,     0, cxAdvAskMergeTicks,       mIsConfirmMergeMode);
    StandardTimer(mGemDisassembleTimer,      0, cxAdvAskDisassembleTicks, mIsConfirmDisassembleMode);

    //--If these timers are not zeroed or capped, block the update.
    if(mGemMergeTimer        > 0 && mGemMergeTimer        < cxAdvVisTicks)            return;
    if(mGemConfirmMergeTimer > 0 && mGemConfirmMergeTimer < cxAdvAskMergeTicks)       return;
    if(mGemDisassembleTimer  > 0 && mGemDisassembleTimer  < cxAdvAskDisassembleTicks) return;

    ///--[Gemcutting Disallowed]
    //--No gemcutting allowed at this vendor. Handle mode switches and stop the update.
    if(!mAllowGemcutting)
    {
        //--Changes modes to the left, which is Gems.
        if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
        {
            ScrollMode(-1);
            return;
        }
        //--Changes modes to the right, which is Sell.
        if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
        {
            ScrollMode(1);
            return;
        }

        //--Stop.
        return;
    }

    ///--[Mode Handlers]
    //--Merging submode.
    if(mIsMergeMode)              { UpdateModeGemsMerge(); return; }
    if(mIsConfirmDisassembleMode) { UpdateGemDisassembly(); return; }
    if(CommonDetailsUpdate()) return;
    if(CommonHelpUpdate())    return;

    ///--[Left and Right, Change Mode]
    //--Changes modes to the left, which is Gems.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right") && !mIsShowingDetails)
    {
        ScrollMode(-1);
        return;
    }
    //--Changes modes to the right, which is Sell.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left") && !mIsShowingDetails)
    {
        ScrollMode(1);
        return;
    }

    ///--[Up and Down]
    //--Increments/decrements on the gem selection list. Wrapping is supported.
    if(AutoListUpDn(mCursor, mSkip, rGemsList->GetListSize(), cScrollBuf, cScrollPage, true))
    {
        RecomputeCursorPositions();
        return;
    }

    ///--[Run]
    //--Activates disassembly.
    if(rControlManager->IsFirstPress("Run") && !mIsShowingDetails)
    {
        //--Error, no gems.
        if(rGemsList->GetListSize() < 1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Get the gem.
        AdventureItem *rSelectedGem = (AdventureItem *)rGemsList->GetElementBySlot(mCursor);
        if(!rSelectedGem) return;

        //--Get needs to have at least one subgem.
        if(rSelectedGem->GetRank() < 1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--Begin disassembly.
        mIsConfirmDisassembleMode = true;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Activate]
    //--Enter merge mode.
    if(rControlManager->IsFirstPress("Activate") && !mIsShowingDetails)
    {
        //--No gems.
        if(rGemsList->GetListSize() < 2)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Switch to merge mode.
        else
        {
            //--Get the gem in question and store it in a temporary buffer.
            AdventureItem *rGem = (AdventureItem *)rGemsList->GetElementBySlot(mCursor);
            if(!rGem)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--Store the name.
            const char *rName = rGemsList->GetNameOfElementBySlot(mCursor);
            ResetString(mMergeGemOwner, rName);

            //--Liberate it. This destabilizes rName.
            rGemsList->SetRandomPointerToThis(rGem);
            rGemsList->LiberateRandomPointerEntry();

            //--Store it.
            mMergeGem = rGem;

            //--Flags.
            RefreshGemMergeMenuHelp();
            mIsMergeMode = true;
            mCursor = 0;
            mSkip = 0;
            RecomputeCursorPositions();
            mHighlightPos.Complete();
            mHighlightSize.Complete();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        return;
    }

    ///--[Cancel]
    //--Exit this UI. Unlike most component UIs, does not exit to the main menu, closes the entire UI.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        HandleExit();
        return;
    }
}
void AdvUIVendor::UpdateGemDisassembly()
{
    ///--[Documentation]
    //--In gem disassembly mode, the user presses Activate to confirm gem disassembly, or cancel to exit.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rGemsList = rInventory->GetGemList();

    //--Activate handler.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Flag.
        mIsConfirmDisassembleMode = false;

        //--Get and disassemble gem.
        AdventureItem *rGem = (AdventureItem *)rGemsList->GetElementBySlot(mCursor);
        if(!rGem) return;

        //--Disassemble.
        rInventory->DisassembleGem(rGem);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
        return;
    }

    //--Cancel handler.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsConfirmDisassembleMode = false;
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///===================================== Segment Rendering ========================================
void AdvUIVendor::RenderTopGems(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Render]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header Text.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(411.0f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mShopName);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderRgtGems(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_RIGHT, pVisAlpha);

    ///--[Render]
    //--Platina.
    RenderCommonPlatina(cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderBotGems(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, pVisAlpha);

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rGemsList = rInventory->GetGemList();

    ///--[Render]
    //--Inventory Listing.
    AdvImages.rFrame_Vendor->Draw();

    //--Mode tabs.
    RenderModeTabs(cColorAlpha);

    ///--[No Gemcutting]
    if(!mAllowGemcutting)
    {
        AdvImages.rFont_Heading->DrawText(269.0f, 142.0f, 0, 1.0f, "This vendor cannot cut gems.");
    }
    ///--[Allow Gemcutting]
    else
    {
        //--If there are no entries on the item list, render this instead.
        if(rGemsList->GetListSize() < 1)
        {
            AdvImages.rFont_Mainline->DrawTextFixed(267.0f, 189.0f, cxAdvMaxNameWid, 0, "You have no gems.");
        }
        //--Heading.
        else
        {
            mColorHeading.SetAsMixerAlpha(cColorAlpha);
            AdvImages.rFont_Heading->DrawText(269.0f, 142.0f, 0, 1.0f, "Select a Gem");
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);
        }

        //--Subroutine.
        RenderGemsItemListing(mSkip, cScrollPage, cColorAlpha);

        //--Scrollbar.
        if(rGemsList->GetListSize() > cScrollPage)
        {
            StandardRenderScrollbar(mSkip, cScrollPage, rGemsList->GetListSize(), 219.0f, 447.0f, true, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
        }

        //--Detail. Goes over the entries since the columns overwrite the black backing.
        AdvImages.rOverlay_DetailGems->Draw();

        //--Highlight.
        if(rGemsList->GetListSize() > 0 && !mIsMergeMode)
            RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

        //--Help Strings.
        mShowDetailsString->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mScrollFastString-> DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 2.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
        mShowHelpString->   DrawText(0.0f, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 3.0f), SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

        //--Right side.
        mDisassembleString->DrawText(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y - (cxAdvMainlineFontH * 1.0f), SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void AdvUIVendor::RenderUnalignedGems(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders components that do not associate with a side.

    ///--[Help Rendering]
    if(!mIsMergeMode) StandardHelp(pVisAlpha);

    ///--[Rendering]
    //--Details.
    AdventureItem *rDetailsItem = (AdventureItem *)AdventureInventory::Fetch()->GetGemList()->GetElementBySlot(mCursor);
    RenderItemDetails(mDetailsTimer, cxAdvDetailsTicks, rDetailsItem);

    //--Disassembly.
    RenderGemsDisassemble(pVisAlpha);

    //--Sub-mode. Merge always renders last.
    RenderGemsMerge(pVisAlpha);
}

///===================================== Common Rendering =========================================
void AdvUIVendor::RenderGemsDisassemble(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Renders extended details about the item in question. This includes a description frame, expandable header,
    //  description text, and the icon.
    if(mGemDisassembleTimer < 1) return;

    //--Resolve pointers.
    StarLinkedList *rGemsList = AdventureInventory::Fetch()->GetGemList();
    AdventureItem *rGem = (AdventureItem *)rGemsList->GetElementBySlot(mCursor);

    //--Compute local alpha.
    float cLocalAlpha = EasingFunction::QuadraticInOut(mGemDisassembleTimer, cxAdvAskDisassembleTicks) * pVisAlpha;

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.7f * cLocalAlpha));

    //--Stop rendering after darkening if the item failed to resolve.
    if(!rGem) return;

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_DOWN, cLocalAlpha);

    ///--[Frame]
    //--Frame.
    AdvImages.rFrame_GemDisassemblePopup->Draw();

    ///--[Text]
    //--Heading.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_Heading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 168.0f, SUGARFONT_AUTOCENTER_X, 1.0f, AdvString.str.mConfirmDisassembly);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Centering computations.
    char *tTextBuf = InitializeString(AdvString.str.mDisassembleContents, rGem->GetDisplayName());
    float cTextWid = AdvImages.rFont_Mainline->GetTextWidth(tTextBuf);
    float cTextLft = (VIRTUAL_CANVAS_X - cTextWid) * 0.50f;

    //--Item Icon
    StarBitmap *rItemIcon = rGem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cTextLft - 24.0f, 205.0f);

    //--Name.
    AdvImages.rFont_Mainline->DrawTextArgs(cTextLft, 205.0f, 0, 1.0f, AdvString.str.mDisassembleContents, rGem->GetDisplayName());

    //--Render the yield. If the gem contains no sub-gems:
    bool tAtLeastOneSubgem = false;
    int tSubgemCount = 0;
    float cGemLft = 374.0f;
    float cGemRgt = 688.0f;
    float cGemY   = 230.0f;
    float cGemH   =  24.0f;
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        //--Get the subgem.
        AdventureItem *rSubgem = rGem->GetGemInSlot(i);
        if(!rSubgem) continue;

        //--At least one subgem, toggle flag.
        tAtLeastOneSubgem = true;

        //--Get X position.
        float tUseX = cGemLft;
        if(tSubgemCount % 2 == 1) tUseX = cGemRgt;

        //--Get Y position.
        float tUseY = cGemY + (cGemH * (int)(tSubgemCount / 2));

        //--Icon.
        StarBitmap *rItemIcon = rSubgem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(tUseX, tUseY + 1.0f);

        //--Name.
        AdvImages.rFont_Mainline->DrawText(tUseX + 24.0f, tUseY + 0.0f, 0, 1.0f, rSubgem->GetDisplayName());

        //--Next.
        tSubgemCount ++;
    }

    //--If there was at least one subgem, also render the original gem this gem was before merging.
    if(tAtLeastOneSubgem)
    {
        //--Get X position.
        float tUseX = cGemLft;
        if(tSubgemCount % 2 == 1) tUseX = cGemRgt;

        //--Get Y position.
        float tUseY = cGemY + (cGemH * (int)(tSubgemCount / 2));

        //--Icon.
        StarBitmap *rItemIcon = rGem->GetGemBaseImage();
        if(rItemIcon) rItemIcon->Draw(tUseX, tUseY + 1.0f);

        //--Get the base name. It may need to be translated.
        const char *rBaseGemName = rGem->GetGemBaseName();
        DataLibrary *rDataLibrary = DataLibrary::Fetch();
        StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_ITEMS);
        if(rTranslation)
        {
            const char *rAlternateGemName = rTranslation->GetTranslationFor(rBaseGemName);
            if(rAlternateGemName) rBaseGemName = rAlternateGemName;
        }

        //--Name.
        AdvImages.rFont_Mainline->DrawText(tUseX + 24.0f, tUseY + 0.0f, 0, 1.0f, rBaseGemName);
    }
    //--If there were no sub-gems, render the adamantite chances.
    else
    {
        AdvImages.rFont_Mainline->DrawText(cGemLft, cGemY, 0, 1.0f, "Adamantite goes here.");
    }

    ///--[Help Strings]
    mConfirmPurchaseString->DrawText(378.0f, 311.0f, SUGARFONT_NOCOLOR                         , 1.0f, AdvImages.rFont_Mainline);
    mCancelPurchaseString-> DrawText(989.0f, 311.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
