//--Base
#include "StarButton.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
StarButton::StarButton()
{
    //--[StarButton]
    //--System
    mLocalName = InitializeString("Unnamed Button");
    mLocalPriority = 0;

    //--Location
    mCoordinates.Set(0.0f, 0.0f, 1.0f, 1.0f);

    //--Execution
    mExecutionList = new StarLinkedList(true);
}
StarButton::~StarButton()
{
    free(mLocalName);
    delete mExecutionList;
}
void StarButton::DeleteThis(void *pPtr)
{
    if(!pPtr) return;
    delete ((StarButton *)pPtr);
}

///===================================== Property Queries =========================================
char *StarButton::GetName()
{
    return mLocalName;
}
int StarButton::GetPriority()
{
    return mLocalPriority;
}
float StarButton::GetTop()
{
    return mCoordinates.mTop;
}

///======================================== Manipulators ==========================================
void StarButton::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void StarButton::SetPriority(int pPriority)
{
    mLocalPriority = pPriority;
}
void StarButton::SetPosition(float pLft, float pTop)
{
    mCoordinates.SetPosition(pLft, pTop);
}
void StarButton::SetDimensions(float pLft, float pTop, float pRgt, float pBot)
{
    mCoordinates.Set(pLft, pTop, pRgt, pBot);
}
void StarButton::SetDimensionsWH(float pLft, float pTop, float pWid, float pHei)
{
    mCoordinates.SetWH(pLft, pTop, pWid, pHei);
}
void StarButton::RegisterExecPack(VirtualButtonExecutionPack *pPack)
{
    //--Registration function. Can be used externally or internally, but assumes the pack is
    //  assembled before passage.
    if(!pPack) return;
    mExecutionList->AddElement("X", pPack, VirtualButtonExecutionPack::DeleteThis);
}
void StarButton::RegisterLuaExecPack(int pPriority, const char *pScriptPath)
{
    //--Registers a Lua execution pack with no frills, just the script. The firing code defaults to 0.0f.
    RegisterLuaExecPack(pPriority, pScriptPath, 0.0f);
}
void StarButton::RegisterLuaExecPack(int pPriority, const char *pScriptPath, float pFiringCode)
{
    //--Registers a Lua execution pack with a firing code. The script will execute with the
    //  firing code.
    if(!pScriptPath) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    VirtualButtonExecutionPack *nPack = (VirtualButtonExecutionPack *)starmemoryalloc(sizeof(VirtualButtonExecutionPack));
    nPack->Initialize();
    nPack->mPriority = pPriority;

    //--Lua Setup.
    nPack->mIsLua = true;
    ResetString(nPack->mLuaExec, pScriptPath);
    nPack->mFiringCode = pFiringCode;

    //--Register.
    RegisterExecPack(nPack);
}

///======================================== Core Methods ==========================================
void StarButton::SortPackages()
{
    //--Sorts the packages. Duh. Packages are sorted by priority, with ties being undefined.
    mExecutionList->SortListUsing(StarButton::CompareButtonPacks);
}
void StarButton::Execute()
{
    //--Executes all the packages.

    //--Sort the packages. This way, they will execute in the correct order.
    SortPackages();

    //--Iterate and execute all packages.
    VirtualButtonExecutionPack *rPack = (VirtualButtonExecutionPack *)mExecutionList->PushIterator();
    while(rPack)
    {
        ExecutePackage(rPack);
        rPack = (VirtualButtonExecutionPack *)mExecutionList->AutoIterate();
    }
}
void StarButton::ExecutePackage(VirtualButtonExecutionPack *pPack)
{
    //--Executes a provided package.
    if(!pPack) return;

    //--Lua Version.
    if(pPack->mIsLua && pPack->mLuaExec)
    {
        LuaManager::Fetch()->ExecuteLuaFile(pPack->mLuaExec, 1, "N", pPack->mFiringCode);
    }
}

///==================================== Private Core Methods ======================================
int StarButton::CompareButtonPacks(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used to sort the packages. The Entries hold VirtualButtonExecutionPacks
    //  which have a comparable priority value.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Get the data pointer.
    VirtualButtonExecutionPack *rPackA = (VirtualButtonExecutionPack *)((*rEntryA)->rData);
    VirtualButtonExecutionPack *rPackB = (VirtualButtonExecutionPack *)((*rEntryB)->rData);
    return rPackA->mPriority - rPackB->mPriority;
}

///=========================================== Update =============================================
void StarButton::Update(int pMouseX, int pMouseY)
{
    //--Handles an update. Update calls are optional, it's possible for the menu itself to handle
    //  all the updating necessary by itself.
    //--Base class does nothing.
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void StarButton::HookToLuaState(lua_State *pLuaState)
{
    /* StarButton_SetProperty("Name", sName)
       StarButton_SetProperty("Priority", iPriority)
       Sets the requested property in the Active StarButton or derived class. */
    lua_register(pLuaState, "StarButton_SetProperty", &Hook_StarButton_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_StarButton_SetProperty(lua_State *L)
{
    //StarButton_SetProperty("Name", sName)
    //StarButton_SetProperty("Priority", iPriority)
    //StarButton_SetProperty("NewLuaExec", iExecPriority, sLuaPath, iFiringCode)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("StarButton_SetProperty");

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid()) return LuaTypeError("StarButton_SetProperty", L);
    StarButton *rButton = (StarButton *)rDataLibrary->rActiveObject;

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    //--Name of the button.
    if(!strcasecmp("Name", rSwitchType) && tArgs == 2)
    {
        rButton->SetName(lua_tostring(L, 2));
    }
    //--Priority. Used for ordering buttons when using an auto-ordered menu.
    else if(!strcasecmp("Priority", rSwitchType) && tArgs == 2)
    {
        rButton->SetPriority(lua_tointeger(L, 2));
    }
    //--Creates a new lua execution.
    else if(!strcasecmp("NewLuaExec", rSwitchType) && tArgs == 4)
    {
        rButton->RegisterLuaExecPack(lua_tointeger(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Error case.
    else
    {
        return LuaPropertyError("StarButton_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
