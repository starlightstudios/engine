///====================================== AdvUIFileSelect ==========================================
//--File selection UI, allows the user to save their game, edit notes, and do other savegame related things.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "FileDef.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///--[Local Definitions]
//--Save Selection Grid
#define ADVUIFILE_SCAN_TOTAL 1000
#define ADVUIFILE_FILES_PER_PAGE 5
#define ADVUIFILE_GRID_SIZE_X 5
#define ADVUIFILE_GRID_SIZE_Y 5

//--Timers
#define ADVMENU_FILESELECT_DELETION_TICKS 10
#define ADVMENU_FILESELECT_ARROW_OPACITY_TICKS 10
#define ADVMENU_FILESELECT_ARROW_OSCILLATE_TICKS 120
#define ADVMENU_FILESELECT_ARROW_OSCILLATE_DISTANCE 5.0f

//--Sorting Types
#define AM_SORT_SLOT 0
#define AM_SORT_SLOTREV 1
#define AM_SORT_DATEREV 2
#define AM_SORT_DATE 3
#define AM_SORT_TOTAL 4

///========================================== Classes =============================================
class AdvUIFileSelect : virtual public AdvUICommon
{
    protected:
    ///--[Constants]
    static const int cxAdvArrowVisTicks = 10;               //When the arrows on the left and right side of the screen appear, how many ticks they take to fade in.
    static const int cxAdvPageScrollTicks = 10;             //When scrolling pages, how many ticks it takes the new page to scroll in.
    static const int cxAdvDeletionTicks = 10;               //Ticks for deletion menu scrolling.

    ///--[System]
    bool mIsEditingNoteForNewSave;
    int mEmptySaveSlotsTotal;

    ///--[Selection and Navigation]
    int mSelectFileCursor;
    int mSelectFileLastColumn;
    int mSelectFileOffset;
    int mHighestCursor;
    int mEditingFileCursor;
    int mArrowBobTimer;
    int mArrowOpacityTimerLft;
    int mArrowOpacityTimerRgt;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    ///--[Page Moving]
    bool mIsPageMoving;
    bool mIsPageMovingUp;
    bool mIsPageMovingAll;
    int mPageMoveTimer;

    ///--[File Information Storage]
    int mCurrentSortingType;
    int mFileListingIndex[ADVUIFILE_SCAN_TOTAL];
    char mFileListing[ADVUIFILE_SCAN_TOTAL][80]; //Safe path size
    LoadingPack *mLoadingPacks[ADVUIFILE_SCAN_TOTAL];
    bool mExistingFileListing[ADVUIFILE_SCAN_TOTAL];

    ///--[Entering Notes]
    bool mIsEnteringNotes;
    int mNoteSlot;
    StringEntry *mStringEntryForm;

    ///--[Deleting a Save]
    int mSaveDeletionIndex;
    int mSaveDeletionIndexOld;
    int mSaveDeletionOpacityTimer;

    ///--[Help Strings]
    StarlightString *mEditNoteString;
    StarlightString *mCycleSortString;
    StarlightString *mCurrentSortString;
    StarlightString *mScrollPageString;
    StarlightString *mDeleteFileString;
    StarlightString *mFastScrollString;
    StarlightString *mPageLftString;
    StarlightString *mPageRgtString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Header;
        StarFont *rFont_Mainline;
        StarFont *rFont_Small;

        //--Images
        StarBitmap *rFrame_GridBacking;
        StarBitmap *rOverlay_ArrowLft;
        StarBitmap *rOverlay_ArrowRgt;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;
        StarBitmap *rOverlay_NewFile;
        StarBitmap *rOverlay_ChapterComplete[LOADINGPACK_MAX_CHAPTER];
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIFileSelect();
    virtual ~AdvUIFileSelect();
    virtual void Construct();

    //--Public Variables
    static int xLastSavefileSortingType;

    //--Property Queries
    virtual int GetArraySlotFromLookups(int pLookup);

    //--Manipulators
    virtual void TakeForeground();
    void BeginPageMove(bool pIsUp, bool pIsAll);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    virtual void ScanExistingFiles();
    virtual void ResolveSortString();

    ///--[Sorting]
    void SortFilesBySlot();
    void SortFilesBySlotRev();
    void SortFilesByDate();
    void SortFilesByDateRev();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateTimers();
    virtual bool UpdatePageMove();
    virtual bool UpdateNotes();
    virtual bool UpdateSortControls();
    virtual bool UpdateDeletion();
    virtual bool UpdateDirectionalControls();
    virtual bool UpdateSeekToEnds();
    virtual bool UpdateActivateCancel();
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderFileBlock(int pIndex, float pLft, float pTop, bool pIsHighlighted, float pOpacityPct);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


