//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///========================================== System ==============================================
void LuaManager::RegisterPredendum(const char *pName, const char *pOriginalScript, const char *pAddendumScript)
{
    ///--[Documentation]
    //--Adds an predendum script to the global list. Whenever the original script is found to be executing,
    //  the addendum script will execute immediately before. This is used by mods to alter game
    //  behavior without affecting the base behavior.
    //--Predendum is probably not a word.
    if(!pName || !pOriginalScript || !pAddendumScript) return;

    ///--[Duplicate Check]
    //--Warn the user of duplicates. It's up to the user to check their mods for this.
    void *rCheckPackage = mScriptPredendumList->GetElementByName(pName);
    if(rCheckPackage) DebugManager::ForcePrint("LuaManager:RegisterPredendum() - Warning, addendum with unique name %s already exists. Make sure your mods are not accidentally duplicating names.\n", pName);

    //--Create a remap pack.
    LuaPathRemapPack *nPackage = (LuaPathRemapPack *)starmemoryalloc(sizeof(LuaPathRemapPack));
    nPackage->Initialize();

    //--Simplify both paths, if needed.
    const char *tSimpleOriginalPath = SimplifyPath(pOriginalScript);
    const char *tSimpleModifiedPath = SimplifyPath(pAddendumScript);
    if(!tSimpleOriginalPath) tSimpleOriginalPath = pOriginalScript;
    if(!tSimpleModifiedPath) tSimpleModifiedPath = pAddendumScript;

    //--Copy to remap pack.
    nPackage->mOriginalPath = InitializeString(tSimpleOriginalPath);
    nPackage->mModifiedPath = InitializeString(tSimpleModifiedPath);

    //--Register.
    mScriptPredendumList->AddElement(pName, nPackage, &LuaPathRemapPack::DeleteThis);

    //--If needed, deallocate:
    if(tSimpleOriginalPath != pOriginalScript) free((void *)tSimpleOriginalPath);
    if(tSimpleModifiedPath != pAddendumScript) free((void *)tSimpleModifiedPath);
}
void LuaManager::RegisterAddendum(const char *pName, const char *pOriginalScript, const char *pAddendumScript)
{
    ///--[Documentation]
    //--Adds an addendum script to the global list. Whenever the original script is found to be executing,
    //  the addendum script will execute immediately afterwards. This is used by mods to alter game
    //  behavior without affecting the base behavior.
    if(!pName || !pOriginalScript || !pAddendumScript) return;

    ///--[Duplicate Check]
    //--Warn the user of duplicates. It's up to the user to check their mods for this.
    void *rCheckPackage = mScriptAddendumList->GetElementByName(pName);
    if(rCheckPackage) DebugManager::ForcePrint("LuaManager:RegisterAddendum() - Warning, addendum with unique name %s already exists. Make sure your mods are not accidentally duplicating names.\n", pName);

    //--Create a remap pack.
    LuaPathRemapPack *nPackage = (LuaPathRemapPack *)starmemoryalloc(sizeof(LuaPathRemapPack));
    nPackage->Initialize();

    //--Simplify both paths, if needed.
    const char *tSimpleOriginalPath = SimplifyPath(pOriginalScript);
    const char *tSimpleModifiedPath = SimplifyPath(pAddendumScript);
    if(!tSimpleOriginalPath) tSimpleOriginalPath = pOriginalScript;
    if(!tSimpleModifiedPath) tSimpleModifiedPath = pAddendumScript;

    //--Copy to remap pack.
    nPackage->mOriginalPath = InitializeString(tSimpleOriginalPath);
    nPackage->mModifiedPath = InitializeString(tSimpleModifiedPath);

    //--Register.
    mScriptAddendumList->AddElement(pName, nPackage, &LuaPathRemapPack::DeleteThis);

    //--If needed, deallocate:
    if(tSimpleOriginalPath != pOriginalScript) free((void *)tSimpleOriginalPath);
    if(tSimpleModifiedPath != pAddendumScript) free((void *)tSimpleModifiedPath);
}

///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void LuaManager::HandlePredendumExecution(const char *pPath)
{
    ///--[Documentation]
    //--Before the original file has executed, executes any predendums that match the original path.
    //  There may be multiple predendums for each path.
    if(!pPath) return;

    //--Simplify the path.
    const char *tSimplePath = SimplifyPath(pPath);
    if(!tSimplePath) tSimplePath = pPath;

    //--Scan all addendums.
    LuaPathRemapPack *rPathPack = (LuaPathRemapPack *)mScriptPredendumList->PushIterator();
    while(rPathPack)
    {
        int tOrigLen = (int)strlen(rPathPack->mOriginalPath);
        int tPathLen = (int)strlen(tSimplePath);

        //--Lengths are identical:
        if(!strcmp(rPathPack->mOriginalPath, tSimplePath))
        {
            ExecuteLuaFileNoAddendum(rPathPack->mModifiedPath);
        }
        //--If the addendum path is shorter than the provided path, we check the last part of the provided path.
        else if(tPathLen >= tOrigLen)
        {
            //--Count backwards.
            int tOrigLoc = tPathLen - tOrigLen;
            if(!strcmp(rPathPack->mOriginalPath, &tSimplePath[tOrigLoc]))
            {
                ExecuteLuaFileNoAddendum(rPathPack->mModifiedPath);
            }
        }

        //--If, after execution, this flag is set, stop executing.
        if(IsStoppingExecution())
        {
            mScriptPredendumList->PopIterator();
            break;
        }

        //--Next.
        rPathPack = (LuaPathRemapPack *)mScriptPredendumList->AutoIterate();
    }

    //--If needed, deallocate:
    if(tSimplePath != pPath) free((void *)tSimplePath);
}
void LuaManager::HandleAddendumExecution(const char *pPath)
{
    ///--[Documentation]
    //--After the original file has executed, executes any addendums that match the original path.
    //  There may be multiple addendums for each path.
    if(!pPath) return;

    //--Simplify the path.
    const char *tSimplePath = SimplifyPath(pPath);
    if(!tSimplePath) tSimplePath = pPath;

    //--Scan all addendums.
    LuaPathRemapPack *rPathPack = (LuaPathRemapPack *)mScriptAddendumList->PushIterator();
    while(rPathPack)
    {
        int tOrigLen = (int)strlen(rPathPack->mOriginalPath);
        int tPathLen = (int)strlen(tSimplePath);

        //--Lengths are identical:
        if(!strcmp(rPathPack->mOriginalPath, tSimplePath))
        {
            ExecuteLuaFileNoAddendum(rPathPack->mModifiedPath);
        }
        //--If the addendum path is shorter than the provided path, we check the last part of the provided path.
        else if(tPathLen >= tOrigLen)
        {
            //--Count backwards.
            int tOrigLoc = tPathLen - tOrigLen;
            if(!strcmp(rPathPack->mOriginalPath, &tSimplePath[tOrigLoc]))
            {
                ExecuteLuaFileNoAddendum(rPathPack->mModifiedPath);
            }
        }

        //--If, after execution, this flag is set, stop executing.
        if(IsStoppingExecution())
        {
            mScriptAddendumList->PopIterator();
            break;
        }

        //--Next.
        rPathPack = (LuaPathRemapPack *)mScriptAddendumList->AutoIterate();
    }

    //--If needed, deallocate:
    if(tSimplePath != pPath) free((void *)tSimplePath);
}
void LuaManager::ClearAddendums()
{
    mScriptPredendumList->ClearList();
    mScriptAddendumList->ClearList();
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
