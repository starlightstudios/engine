//--Base
#include "Actor.h"

//--Classes
#include "ContextMenu.h"
#include "LuaContextOption.h"
#include "PlayerPony.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

void Actor::PopulateContextMenuBy(Actor *pActor, ContextMenu *pMenu)
{
    //--Populates the context menu around a given target. This usually involves allowing examination
    //  and may allow attacking if the target is hostile.
    if(!pActor || !pMenu) return;

    //--Populate with examination.
    SetMemoryData(__FILE__, __LINE__);
    ContextOption *nExamineOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
    nExamineOption->Setup("Examine", ContextMenu::ExamineTargetActor, false, pActor);
    pMenu->RegisterCommand(nExamineOption, &ContextOption::DeleteThis);

    //--If we're stunned, we can do nothing except examine.
    if(IsStunned()) return;

    //--Now request the ContextOption list from the Player. They're all valid options, but they
    //  have internal checks to see if they're valid for the given target.
    StarLinkedList *rCommandList = PlayerPony::Fetch()->GetContextCommandList();
    LuaContextOption *rOption = (LuaContextOption *)rCommandList->PushIterator();
    while(rOption)
    {
        //--Run the internal check. We need to pass the Actor and Target first.
        if(rOption->IsLegalOnTarget(this, pActor))
        {
            //--Provide a context package for this function.
            SetMemoryData(__FILE__, __LINE__);
            LCOStoragePack *nStoragePack = (LCOStoragePack *)starmemoryalloc(sizeof(LCOStoragePack));
            nStoragePack->rCallerPtr = this;
            nStoragePack->rTargetPtr = pActor;
            nStoragePack->rContextOption = rOption;

            //--Store the option.
            SetMemoryData(__FILE__, __LINE__);
            ContextOption *nLuaOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
            nLuaOption->Setup(rOption->GetName(), ContextMenu::ExecuteLuaContextOption, true, nStoragePack);
            pMenu->RegisterCommand(nLuaOption, &ContextOption::DeleteThis);
        }

        //--Next.
        rOption = (LuaContextOption *)rCommandList->AutoIterate();
    }
}
