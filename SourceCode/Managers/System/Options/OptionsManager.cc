//--Base
#include "OptionsManager.h"

//--Classes
#include "VisualLevel.h"
#include "FlexMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "StarFileSystem.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//--Needed for _mkdir in the SDL/Steam version.
#if defined _USE_USER_DIRS_
#include <direct.h>
#endif

///========================================== System ==============================================
OptionsManager::OptionsManager()
{
    ///--[Variable Setup]
    //--[OptionsManager]
    //--System
    mErrorFlag = false;

    //--Storage
    mOptionList    = new StarLinkedList(true);
    mDebugFlagList = new StarLinkedList(false);
    mActiveModList = new StarLinkedList(false);

    //--Options
    mShowExplicitErrorsForOptionQueries = false;
    mIsFullscreen = false;
    mIsMemoryMode = false;
    mWinMandateMonitor = -1;
    mWinSpawnX = 0;
    mWinSpawnY = 0;
    mWinSizeX = 800;
    mWinSizeY = 600;
    mMusicVolume = 75;
    mSoundVolume = 100;
    for(int i = 0; i < AM_ABSTRACT_VOLUME_TOTAL; i ++)
    {
        mAbstractVolume[i] = AM_DEFAULT_ABSTRACT_VOLUME;
    }

    //--Hardware Configuration
    mForcePowerOfTwo = false;
    mBlockDepthBuffer = false;
    mBlockStencilbuffer = false;
    mBlockDoubleBuffering = false;
    mUseRAMLoading = false;
    mRequestedAccelerateVisual = 1;
    mRequestedOpenGLMajor = 4;
    mRequestedOpenGLMinor = 5;

    //--Engine Options
    mNoAudioSuppression = false;
    mForceDirectAspectRatios = false;

    //--Debug Variables
    for(int i = 0; i < OM_DEBUGVARS; i ++) mDebugVariables[i] = 0.0f;
    mDebugVariables[2] = 1.25;
    mDebugVariables[3] = 90;
    mDebugVariables[4] = 25;

    //--Set the console command list to be case-insensitive.
    xConsoleCommandList->SetCaseSensitivity(false);

    //--Configuration File List
    mConfigFilePaths = new StarLinkedList(true);

    //--GUI Translations
    mTranslationStringsTotal = 0;
    mTranslationStrings = NULL;
}
OptionsManager::~OptionsManager()
{
    delete mOptionList;
    delete mDebugFlagList;
    delete mActiveModList;
    delete mConfigFilePaths;
    for(int i = 0; i < mTranslationStringsTotal; i ++) free (mTranslationStrings[i]);
    free(mTranslationStrings);
}

///--[Public Statics]
//--System
bool OptionsManager::xIsDefiningOption = false;
bool OptionsManager::xErrorFlag = false;
StarLinkedList *OptionsManager::xConsoleCommandList = new StarLinkedList(false);

//--Autorun, used by the launch scripts to automatically boot into a specific game at startup. This is
//  set by the command line, with the name of the game provided.
char *OptionsManager::xAutorun = NULL;

///===================================== Property Queries =========================================
bool OptionsManager::GetErrorFlag()
{
    bool tOrigFlag = mErrorFlag;
    mErrorFlag = false;
    return tOrigFlag;
}
bool OptionsManager::GetDebugFlag(const char *pName)
{
    //--Returns the named debug flag from the RLL.
    bool *rReturnFlag = (bool *)mDebugFlagList->GetElementByName(pName);
    if(!rReturnFlag) return false;
    return *rReturnFlag;
}
OptionPack *OptionsManager::GetOption(const char *pName)
{
    return (OptionPack *)mOptionList->GetElementByName(pName);
}
int OptionsManager::GetOptionType(const char *pName)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!rPack) return POINTER_TYPE_FAIL;
    return rPack->mTypeFlag;
}
int OptionsManager::GetOptionI(const char *pName)
{
    //--Check.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_INT))
    {
        //--If it's not an int or a float, fail.
        if(!CheckPtr(rPack, POINTER_TYPE_FLOAT))
        {
            if(mShowExplicitErrorsForOptionQueries)
                DebugManager::ForcePrint("OptionsManager::GetOptionI() - Warning, option %s does not exist %p or is not an integer or float.\n", pName, rPack);
            return OM_ERROR_INT;
        }
        //--If it's a float, return it casted.
        else
        {
            float *rPtr = (float *)rPack->rPtr;
            int tIntValue = (int)(*rPtr);
            return tIntValue;
        }
    }
    int *rPtr = (int *)rPack->rPtr;
    return *rPtr;
}
float OptionsManager::GetOptionF(const char *pName)
{
    //--Check.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_FLOAT))
    {
        //--If it's not a float or an int, fail.
        if(!CheckPtr(rPack, POINTER_TYPE_INT))
        {
            if(mShowExplicitErrorsForOptionQueries)
                DebugManager::ForcePrint("OptionsManager::GetOptionF() - Warning, option %s does not exist %p or is not a float/int.\n", pName, rPack);
            return OM_ERROR_FLOAT;
        }
        //--If it's an int, return it casted.
        else
        {
            int *rPtr = (int *)rPack->rPtr;
            float tFloatValue = (float)(*rPtr);
            return tFloatValue;
        }
    }
    float *rPtr = (float *)rPack->rPtr;
    return *rPtr;
}
char *OptionsManager::GetOptionS(const char *pName)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_STRING))
    {
        if(mShowExplicitErrorsForOptionQueries)
            DebugManager::ForcePrint("OptionsManager::GetOptionS() - Warning, option %s does not exist %p or is not a string.\n", pName, rPack);
        return OM_ERROR_STRING;
    }
    char *rPtr = (char *)rPack->rPtr;
    return rPtr;
}
bool OptionsManager::GetOptionB(const char *pName)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_BOOL))
    {
        if(mShowExplicitErrorsForOptionQueries)
            DebugManager::ForcePrint("OptionsManager::GetOptionB() - Warning, option %s does not exist %p or is not a boolean.\n", pName, rPack);
        return OM_ERROR_BOOL;
    }
    bool *rPtr = (bool *)rPack->rPtr;
    return *rPtr;
}
float OptionsManager::GetDebugVar(int pSlot)
{
    if(pSlot < 0 || pSlot >= OM_DEBUGVARS) return 0.0f;
    return mDebugVariables[pSlot];
}
int OptionsManager::GetOrbitCode()
{
    return OM_ORBITCODE_UNKNOWN;
}
const char *OptionsManager::GetTranslatedText(int pIndex)
{
    if(pIndex < 0 || pIndex >= mTranslationStringsTotal) return "Null";
    if(!mTranslationStrings[pIndex]) return "Null";
    return mTranslationStrings[pIndex];
}
bool OptionsManager::IsModActive(const char *pName)
{
    return (mActiveModList->GetElementByName(pName) != NULL);
}

///======================================== Manipulators ==========================================
OptionPack *OptionsManager::AddOption(const char *pName, void *pPtr, int pType, bool pOwnPointer, const char *pPath)
{
    //--Error check.
    if(!pName || !pPtr || pType == POINTER_TYPE_FAIL || !pPath) return NULL;

    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    OptionPack *nPack = (OptionPack *)starmemoryalloc(sizeof(OptionPack));
    nPack->Initialize();
    nPack->rPtr = pPtr;
    nPack->mTypeFlag = pType;
    nPack->mIsProgramLocal = false;
    nPack->mIsDefinedExternally = xIsDefiningOption;
    strncpy(nPack->mFilePath, pPath, sizeof(nPack->mFilePath));

    //--String type, stores a local copy of the string.
    if(pType == POINTER_TYPE_STRING)
    {
        nPack->rPtr = InitializeString("%s", pPtr);
        pOwnPointer = true;
    }

    //--If flagged to take ownership of the pointer, mark the object. This will make the
    //  pointer be deallocated when the object is deleted.
    if(pOwnPointer) nPack->TakeOwnershipOfPointer();

    //--Add to permanent list and return.
    mOptionList->AddElement(pName, nPack, &OptionPack::DeleteThis);
    return nPack;
}
void OptionsManager::SetOptionI(const char *pName, int pValue)
{
    //--Sets an option to an integer value. If it's a float, it still works.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_INT))
    {
        //--We can still set the value if it's a float.
        if(CheckPtr(rPack, POINTER_TYPE_FLOAT))
        {
            float *rPtr = (float *)rPack->rPtr;
            *rPtr = (float)pValue;
            rPack->mWasChanged = true;
        }

        //--Return here.
        return;
    }

    //--Dereference, set.
    int *rPtr = (int *)rPack->rPtr;
    *rPtr = pValue;
    rPack->mWasChanged = true;
}
void OptionsManager::SetOptionF(const char *pName, float pValue)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_FLOAT))
    {
        //--We can still set the value if it's an int.
        if(CheckPtr(rPack, POINTER_TYPE_INT))
        {
            int *rPtr = (int *)rPack->rPtr;
            *rPtr = (int)pValue;
            rPack->mWasChanged = true;
        }

        //--Return here.
        return;
    }

    //--Deteference, set.
    float *rPtr = (float *)rPack->rPtr;
    *rPtr = pValue;
    rPack->mWasChanged = true;
}
void OptionsManager::SetOptionS(const char *pName, const char *pValue)
{
    //--Retrieve the value.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_STRING)) return;

    //--Clean, reset.
    free(rPack->rPtr);
    rPack->rPtr = InitializeString(pValue);
    rPack->mWasChanged = true;
}
void OptionsManager::SetOptionB(const char *pName, bool pValue)
{
    //--Retrieve the value.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_BOOL)) return;

    //--Set.
    bool *rPtr = (bool *)rPack->rPtr;
    *rPtr = pValue;
    rPack->mWasChanged = true;
}
void OptionsManager::RemOption(const char *pName)
{
    mOptionList->RemoveElementS(pName);
}
void OptionsManager::SetDebugVar(int pSlot, float pValue)
{
    if(pSlot < 0 || pSlot >= OM_DEBUGVARS) return;
    mDebugVariables[pSlot] = pValue;
}
void OptionsManager::RegisterConfigPath(const char *pPath)
{
    if(!pPath) return;
    mConfigFilePaths->AddElement("X", InitializeString(pPath), &FreeThis);
}
void OptionsManager::AllocateTranslationStrings(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mTranslationStringsTotal; i ++) free(mTranslationStrings[i]);
    free(mTranslationStrings);

    //--Zero.
    mTranslationStringsTotal = 0;
    mTranslationStrings = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mTranslationStringsTotal = pTotal;
    mTranslationStrings = (char **)starmemoryalloc(sizeof(char *) * mTranslationStringsTotal);
    memset(mTranslationStrings, 0, sizeof(char *) * mTranslationStringsTotal);
}
void OptionsManager::SetTranslationString(int pIndex, const char *pString)
{
    if(pIndex < 0 || pIndex >= mTranslationStringsTotal) return;
    ResetString(mTranslationStrings[pIndex], pString);
}
void OptionsManager::SetModActive(const char *pName)
{
    //--Duplication check.
    if(!pName || mActiveModList->GetElementByName(pName)) return;

    //--Add a dummy pointer.
    mActiveModList->AddElement(pName, &mErrorFlag);
}

///==================================== Private Core Methods ======================================
///======================================== Core Methods ==========================================
void OptionsManager::SetupOptionList()
{
    //--Setup.
    char tBuf[128];
    //const bool cTakeOwnership = true;
    const bool cDoNotTakeOwnership = false;

    //--Configuration
    AddOption("Fullscreen",                &mIsFullscreen,              POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("MemoryMode",                &mIsMemoryMode,              POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("BlockDepthBuffer",          &mBlockDepthBuffer,          POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("BlockStencilBuffer",        &mBlockStencilbuffer,        POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("MandateMonitor",            &mWinMandateMonitor,         POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("StartWinX",                 &mWinSpawnX,                 POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("StartWinY",                 &mWinSpawnY,                 POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("WinSizeX",                  &mWinSizeX,                  POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("WinSizeY",                  &mWinSizeY,                  POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("ForcePowerOfTwo",           &mForcePowerOfTwo,           POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("MusicVolume",               &mMusicVolume,               POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("SoundVolume",               &mSoundVolume,               POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    for(int i = 0; i < AM_ABSTRACT_VOLUME_TOTAL; i ++)
    {
        sprintf(tBuf, "AbstractVolume%02i", i);
        AddOption(tBuf, &mAbstractVolume[i], POINTER_TYPE_INT, cDoNotTakeOwnership, "Config_Engine.lua");
    }

    //--Engine Options
    AddOption("AccelVisual",               &mRequestedAccelerateVisual, POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("OpenGLMajor",               &mRequestedOpenGLMajor,      POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("OpenGLMinor",               &mRequestedOpenGLMinor,      POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("NoAudioSuppression",        &mNoAudioSuppression,        POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("ForceDirectAspectRatios",   &mForceDirectAspectRatios,   POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");
    AddOption("UseRAMLoading",             &mUseRAMLoading,             POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Engine.lua");

    //--UI Options
    AddOption("UseAlternate3DUI", &VisualLevel::xUseAlternateUI, POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");
    AddOption("UseClassic2DUI",   &FlexMenu::xUseClassicUI,      POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");

    //--Pandemonium Level Render Options
    AddOption("ActorsAnimateMove",      &PandemoniumLevel::xActorIconsMove,      POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");
    AddOption("ActorsAnimateMoveTicks", &PandemoniumLevel::xActorMoveTicks,      POINTER_TYPE_INT,  cDoNotTakeOwnership, "Config_Classic.lua");
    AddOption("ShowDamageAnimations",   &PandemoniumLevel::xAnimateDamage,       POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");
    AddOption("Show2DUnderlays",        &PandemoniumLevel::xShow2DUnderlays,     POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");
    AddOption("UseSmallConsoleFont",    &PandemoniumLevel::xUseSmallConsoleFont, POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");
    AddOption("DisableDynamicFog",      &PandemoniumLevel::xDisableDynamicFog,   POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");

    //--Visual Level Render Options
    AddOption("AllowDamageOverlay", &VisualLevel::xAllowDamageOverlay, POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Classic.lua");

    //--Adventure Mode Render Options
    AddOption("LowResAdventureMode", &DisplayManager::xLowDefinitionFlag,     POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Adventure.lua");
    AddOption("DisallowPerlinNoise", &Global::Shared()->gDisallowPerlinNoise, POINTER_TYPE_BOOL, cDoNotTakeOwnership, "Config_Adventure.lua");
}
bool OptionsManager::CheckPtr(OptionPack *pPack, int pExpectedType)
{
    //--Returns true if the Pack is of the expected type
    //  Returns false if it is NULL, or is the wrong type, and sets the error
    //  flag accordingly.
    if(!pPack || pPack->mTypeFlag != pExpectedType)
    {
        mErrorFlag = true;
        return false;
    }
    return true;
}
void OptionsManager::PostProcess()
{
    ///--[Setting]
    //--Set the filtering option for StarBitmaps.
    StarBitmap::xForcePowerOfTwo = mForcePowerOfTwo;

    ///--[Nextboot Modifiers]
    //--Asset Streaming Modifier
    OptionPack *rOptionAssetStreaming         = (OptionPack *)mOptionList->GetElementByName("Allow Asset Streaming");
    OptionPack *rOptionAssetStreamingNextboot = (OptionPack *)mOptionList->GetElementByName("Allow Asset Streaming Nextboot");
    if(rOptionAssetStreaming && rOptionAssetStreamingNextboot)
    {
        bool *rValue = (bool *)(rOptionAssetStreamingNextboot->rPtr);
        SetOptionB("Allow Asset Streaming", *rValue);
    }

    //--Actors Unload Modifier
    OptionPack *rActorsUnloadAfterDialogue         = (OptionPack *)mOptionList->GetElementByName("Actors Unload After Dialogue");
    OptionPack *rActorsUnloadAfterDialogueNextboot = (OptionPack *)mOptionList->GetElementByName("Actors Unload After Dialogue Nextboot");
    if(rActorsUnloadAfterDialogue && rActorsUnloadAfterDialogueNextboot)
    {
        bool *rValue = (bool *)(rActorsUnloadAfterDialogueNextboot->rPtr);
        SetOptionB("Actors Unload After Dialogue", *rValue);
    }

    //--Use Ram Loading Modifier
    OptionPack *rUseRamLoading         = (OptionPack *)mOptionList->GetElementByName("UseRAMLoading");
    OptionPack *rUseRamLoadingNextboot = (OptionPack *)mOptionList->GetElementByName("UseRAMLoading Nextboot");
    if(rUseRamLoading && rUseRamLoadingNextboot)
    {
        bool *rValue = (bool *)(rUseRamLoadingNextboot->rPtr);
        SetOptionB("UseRAMLoading", *rValue);
    }

    ///--[Rewrite]
    //--Rewrite the config files to handle the modified flags due to nextboots.
    WriteConfigFiles();

    ///--[Clean Up]
    //--Clear any lingering flags.
    ClearUpdateFlags();
}
void OptionsManager::PrintReport()
{
    //--Prints a report of data pulled from the game's state after setup is completed. Sometimes the
    //  data set in the options will not match due to driver errors, so this report is useful for
    //  diagnostics.
    int32_t tAuxBuffers = 0;

    //--Get the values.
    glGetIntegerv(GL_AUX_BUFFERS, &tAuxBuffers);

    //--GL Version.
    int tGLMajor = 0;
    int tGLMinor = 0;
    int r=0, g=0, b=0;
    #if defined _SDL_PROJECT_
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &tGLMajor);
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &tGLMinor);

        //--Color properties.
        SDL_GL_GetAttribute(SDL_GL_RED_SIZE, &r);
        SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, &g);
        SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE, &b);
    #endif

    //--Print.
    fprintf(stderr, "\n== Program Status Report: ==\n");
    fprintf(stderr, " Expected OpenGL Version %i.%i\n", 4, 5);
    fprintf(stderr, " Received OpenGL Version %i.%i\n", tGLMajor, tGLMinor);
    fprintf(stderr, " Bit sizes: %i %i %i\n", r, g, b);
    fprintf(stderr, " Total auxillary draw buffers: %i\n", tAuxBuffers);
    fprintf(stderr, " Max texture size: %i\n", StarBitmap::xMaxTextureSize);
    fprintf(stderr, "==     End of Report      ==\n\n");
}
void OptionsManager::RunConfigFiles()
{
    ///--[Documentation]
    //--Runs all registered configuration files. This builds all variables needed by all sub-games.
    LuaManager *rLuaManager = LuaManager::Fetch();

    //--Fail-silently flag. Not all games use all config files.
    LuaManager::xFailSilently = true;

    //--Only used for user directory.
    #if defined _USE_USER_DIRS_
    const char *rUserProfile = getenv("USERPROFILE");
    #endif

    ///--[Iterate]
    //--The config file is the base path. Run across all the files and check for existence.
    char *rConfigPath = (char *)mConfigFilePaths->PushIterator();
    while(rConfigPath)
    {
        //--Primary location. Only happens on Steam. Does nothing if the user directory was not found.
        #if defined _USE_USER_DIRS_
        if(rUserProfile)
        {
            //--Create alternate directory.
            char *tAltBuf = InitializeString("%s/AppData/Local/RunesOfPandemonium/%s", rUserProfile, rConfigPath);
            //fprintf(stderr, "User Dir Path: %s\n", tAltBuf);

            //--Check if the file exists.
            if(StarFileSystem::FileExists(tAltBuf))
            {
                //fprintf(stderr, " Exists. Running.\n");
                rLuaManager->ExecuteLuaFile(tAltBuf);

                //--Next.
                free(tAltBuf);
                rConfigPath = (char *)mConfigFilePaths->AutoIterate();
                continue;
            }

            //--Does not exist.
            //fprintf(stderr, "Does not exist.\n");
            free(tAltBuf);
        }
        #endif

        //--Secondary location. Local to the engine's execution directory.
        //fprintf(stderr, "Executing local config: %s\n", rConfigPath);
        rLuaManager->ExecuteLuaFile(rConfigPath);

        //--Next.
        rConfigPath = (char *)mConfigFilePaths->AutoIterate();
    }

    //--After running the configs, all flags are cleared.
    ClearUpdateFlags();

    //--Clean up.
    LuaManager::xFailSilently = true;
}
void OptionsManager::ClearUpdateFlags()
{
    //--Sets all options pack flags back to "unmodified".
    OptionPack *rPackage = (OptionPack *)mOptionList->PushIterator();
    while(rPackage)
    {
        rPackage->mWasChanged = false;
        rPackage = (OptionPack *)mOptionList->AutoIterate();
    }
}
void OptionsManager::ClearModActivity()
{
    mActiveModList->ClearList();
}

///=========================================== Update =============================================
///========================================== File I/O ============================================
void OptionsManager::WriteConfigFiles()
{
    ///--[Documentation and Setup]
    //--Writes the config file to the given path, using all the options currently within the OM that
    //  are specified. An option flagged as "Program-Local" will not be written.
    if(mConfigFilePaths->GetListSize() < 1) return;

    //--Function buffer uses 4x the max letters for its size.
    const int cFuncBufSize = STD_MAX_LETTERS * 4;

    //--Setup.
    char tFunctionBuffer[cFuncBufSize];
    char tNameBuffer[STD_MAX_LETTERS];
    char tTypeBuffer[8];
    char tValueBuffer[STD_MAX_LETTERS];

    //--List of files that have at least one edit done to them.
    int tDummyVar = 3;
    StarLinkedList *tAtLeastOneEdit = new StarLinkedList(false);//char *, reference

    ///--[Determine Which Files to Write]
    //--Iterate across all variables. If a variable was edited, it needs to write its config file.
    OptionPack *rPackage = (OptionPack *)mOptionList->PushIterator();
    while(rPackage)
    {
        //--If edited:
        if(rPackage->mWasChanged)
        {
            //--If the tAtLeastOneEdit list already has the entry, do nothing:
            void *rCheckEntry = tAtLeastOneEdit->GetElementByName(rPackage->mFilePath);
            if(rCheckEntry)
            {
            }
            //--Entry does not exist. Create it.
            else
            {
                tAtLeastOneEdit->AddElement(rPackage->mFilePath, &tDummyVar);
            }
        }

        //--Next.
        rPackage = (OptionPack *)mOptionList->AutoIterate();
    }

    //--If there are zero files with at least one edit, we don't need to write any config files.
    if(tAtLeastOneEdit->GetListSize() < 1)
    {
        delete tAtLeastOneEdit;
        return;
    }

    ///--[Determine File Locations]
    //--Create a linked-list that is parallel with the config files. This will contain FILE pointers. The path must be on the
    //  tAtLeastOneEdit list, or it is not created.
    StarLinkedList *tFileList = new StarLinkedList(false);

    //--If using the user directory, resolve this environment variable.
    #if defined _USE_USER_DIRS_
    const char *rUserProfile = getenv("USERPROFILE");
    #endif

    //--Iterate.
    char *rConfigPath = (char *)mConfigFilePaths->PushIterator();
    while(rConfigPath)
    {
        //--Final config path.
        char *tFinalPath = InitializeString("%s", rConfigPath);

        //--Decide which path to write to. If _USE_USER_DIRS_ is defined, then the engine should prioritize placing the
        //  config files in the user's directories, and creating them if they don't exist. If the creation fails, places
        //  the config files in the engine directory.
        #if defined _USE_USER_DIRS_

            //--Get the location we expect to write to.
            char *tAltBuf = InitializeString("%s/AppData/Local/RunesOfPandemonium/%s", rUserProfile, rConfigPath);
            //fprintf(stderr, "Checking write to alternate config path: %s\n", tAltBuf);

            //--We need to check each of the folders in order to see if they exist. Right now it is assumed that the AppData and Local
            //  folders should always exist, these are basic to Windows.
            char *tTempFolder = InitializeString("%s/AppData/Local/RunesOfPandemonium", rUserProfile);
            int tErrorCode = _mkdir(tTempFolder);
            if(tErrorCode)
            {
                //--Ignore EEXIST because that just means the directory already exists.
                if(errno != EEXIST)
                    fprintf(stderr, "_mkdir(): Error, code %i. Versus EEXIST: %i and ENOENT: %i\n", errno, EEXIST, ENOENT);
                else
                    ResetString(tFinalPath, tAltBuf);
            }
            //--No error. Path exists, use it.
            else
            {
                ResetString(tFinalPath, tAltBuf);
            }

            //--Clean up.
            free(tAltBuf);
            free(tTempFolder);
        #endif

        //--Debug.
        //fprintf(stderr, "Using path: %s\n", tFinalPath);

        //--Check if this path exists on the edit list. If not, skip it.
        void *rCheckPtr = tAtLeastOneEdit->GetElementByName(rConfigPath);
        if(rCheckPtr)
        {
            //--Open and check.
            FILE *fOutfile = fopen(tFinalPath, "wb");
            if(!fOutfile)
            {
                fprintf(stderr, "Failed to open output file: %i\n", errno);
            }

            //--Place the file on the list using its short path.
            tFileList->AddElementAsTail(rConfigPath, fOutfile);
        }

        //--Next.
        free(tFinalPath);
        rConfigPath = (char *)mConfigFilePaths->AutoIterate();
    }

    ///--[Write Options]
    //--Begin writing.
    OptionPack *rOption = (OptionPack *)mOptionList->PushIterator();
    while(rOption)
    {
        //--If flagged, ignore this option.
        if(rOption->mIsProgramLocal)
        {
            rOption = (OptionPack *)mOptionList->AutoIterate();
            continue;
        }

        //--Name of the option.
        const char *rOptionName = mOptionList->GetIteratorName();
        sprintf(tNameBuffer, "\"%s\"", rOptionName);

        //--If the name begins with "ExpectedLoad_" then don't write it.
        if(!strncasecmp(rOptionName, "ExpectedLoad_", 13))
        {
            rOption = (OptionPack *)mOptionList->AutoIterate();
            continue;
        }

        //--Get the file this variable writes to. If the outfile does not exist, it may be because no variables
        //  in that file updated this pulse.
        FILE *fOutfile = (FILE *)tFileList->GetElementByName(rOption->mFilePath);
        if(!fOutfile)
        {
            //fprintf(stderr, "== Warning, no configuration file %s registered for option %s. ==\n", rOption->mFilePath, mOptionList->GetIteratorName());
            rOption = (OptionPack *)mOptionList->AutoIterate();
            continue;
        }

        //--Resolve the type.
        if(rOption->mTypeFlag == POINTER_TYPE_BOOL)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"B\"");

            //--Value goes without quotes.
            bool *rValuePtr = (bool *)rOption->rPtr;
            if(*rValuePtr)
            {
                strcpy(tValueBuffer, "true");
            }
            else
            {
                strcpy(tValueBuffer, "false");
            }
        }
        else if(rOption->mTypeFlag == POINTER_TYPE_FLOAT)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"N\"");

            //--Value goes in quotes.
            float *rValuePtr = (float *)rOption->rPtr;
            sprintf(tValueBuffer, "\"%f\"", *rValuePtr);
        }
        else if(rOption->mTypeFlag == POINTER_TYPE_INT)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"I\"");

            //--Value goes in quotes.
            int *rValuePtr = (int *)rOption->rPtr;
            sprintf(tValueBuffer, "\"%i\"", *rValuePtr);
        }
        else if(rOption->mTypeFlag == POINTER_TYPE_STRING)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"S\"");

            //--Value goes in quotes.
            char *rValuePtr = (char *)rOption->rPtr;
            sprintf(tValueBuffer, "\"%s\"", rValuePtr);
        }

        //--If the option is internal, use this format.
        if(!rOption->mIsDefinedExternally)
        {
            sprintf(tFunctionBuffer, "OM_SetOption(%s, %s)", tNameBuffer, tValueBuffer);
        }
        //--Otherwise, the option is an external one. Use this format.
        else
        {
            sprintf(tFunctionBuffer, "OM_DefineOption(%s, %s, %s, \"%s\")", tNameBuffer, tTypeBuffer, tValueBuffer, rOption->mFilePath);
        }

        //--Write the final copy.
        fprintf(fOutfile, "%s\n", tFunctionBuffer);

        //--Next.
        rOption = (OptionPack *)mOptionList->AutoIterate();
    }

    ///--[Clean]
    //--Close all the files we opened.
    FILE *fOutfilePtr = (FILE *)tFileList->PushIterator();
    while(fOutfilePtr)
    {
        fclose(fOutfilePtr);
        fOutfilePtr = (FILE *)tFileList->AutoIterate();
    }
    delete tFileList;
    delete tAtLeastOneEdit;

    //--Clear update flags. All variables are now up to date.
    ClearUpdateFlags();
}
void OptionsManager::WriteLoadFile(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Identical to writing the configuration file, but only writes variables which are used for the
    //  load interrupt counters. These change frequently as the game is updated but are not real
    //  configuration variables.
    //--In order to be written, the variable name needs to begin with "ExpectedLoad_"
    FILE *fOutfile = fopen(pPath, "w");
    if(!fOutfile) return;

    //--Function buffer uses 4x the max letters for its size.
    const int cFuncBufSize = STD_MAX_LETTERS * 4;

    //--Setup.
    char tFunctionBuffer[cFuncBufSize];
    char tNameBuffer[STD_MAX_LETTERS];
    char tTypeBuffer[8];
    char tValueBuffer[STD_MAX_LETTERS];

    ///--[Write]
    //--Begin writing.
    OptionPack *rOption = (OptionPack *)mOptionList->PushIterator();
    while(rOption)
    {
        //--If flagged, ignore this option.
        if(rOption->mIsProgramLocal)
        {

        }
        //--Write the option.
        else
        {
            //--Name of the option.
            const char *rOptionName = mOptionList->GetIteratorName();
            sprintf(tNameBuffer, "\"%s\"", rOptionName);

            //--If the name does not begin with "ExpectedLoad_" then don't write it.
            if(strncasecmp(rOptionName, "ExpectedLoad_", 13))
            {
                rOption = (OptionPack *)mOptionList->AutoIterate();
                continue;
            }

            //--Resolve the type.
            if(rOption->mTypeFlag == POINTER_TYPE_BOOL)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"B\"");

                //--Value goes without quotes.
                bool *rValuePtr = (bool *)rOption->rPtr;
                if(*rValuePtr)
                {
                    strcpy(tValueBuffer, "true");
                }
                else
                {
                    strcpy(tValueBuffer, "false");
                }
            }
            else if(rOption->mTypeFlag == POINTER_TYPE_FLOAT)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"N\"");

                //--Value goes in quotes.
                float *rValuePtr = (float *)rOption->rPtr;
                sprintf(tValueBuffer, "\"%f\"", *rValuePtr);
            }
            else if(rOption->mTypeFlag == POINTER_TYPE_INT)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"N\"");

                //--Value goes in quotes.
                int *rValuePtr = (int *)rOption->rPtr;
                sprintf(tValueBuffer, "\"%i\"", *rValuePtr);
            }
            else if(rOption->mTypeFlag == POINTER_TYPE_STRING)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"S\"");

                //--Value goes in quotes.
                char *rValuePtr = (char *)rOption->rPtr;
                sprintf(tValueBuffer, "\"%s\"", rValuePtr);
            }

            //--If the option is internal, use this format.
            if(!rOption->mIsDefinedExternally)
            {
                sprintf(tFunctionBuffer, "OM_SetOption(%s, %s)", tNameBuffer, tValueBuffer);
            }
            //--Otherwise, the option is an external one. Use this format.
            else
            {
                sprintf(tFunctionBuffer, "OM_DefineOption(%s, %s, %s, \"%s\")", tNameBuffer, tTypeBuffer, tValueBuffer, rOption->mFilePath);
            }

            //--Write the final copy.
            fprintf(fOutfile, "%s\n", tFunctionBuffer);
        }

        //--Next.
        rOption = (OptionPack *)mOptionList->AutoIterate();
    }

    ///--[Clean Up]
    //--Clean.
    fclose(fOutfile);
}

///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
StarLinkedList *OptionsManager::GetActiveModList()
{
    return mActiveModList;
}

///===================================== Static Functions =========================================
OptionsManager *OptionsManager::Fetch()
{
    return Global::Shared()->gOptionsManager;
}
const char *OptionsManager::FetchTranslation(int pIndex)
{
    //--Used to make retrieving translated text take fewer lines. A macro in the header file makes
    //  this take fewer letters as well.
    return OptionsManager::Fetch()->GetTranslatedText(pIndex);
}

///======================================== Lua Hooking ===========================================
void OptionsManager::HookToLuaState(lua_State *pLuaState)
{
    /* OM_RegisterConfigPath(sPath)
       Registers a configuration file's path. All configuration files that are detected are run to
       build variables. */
    lua_register(pLuaState, "OM_RegisterConfigPath", &Hook_OM_RegisterConfigPath);

    /* OM_OptionExists(sName) (1 String)
       Returns "N", "S", or "B" if the option exists, or "FAIL" if it does not. */
    lua_register(pLuaState, "OM_OptionExists", &Hook_OM_OptionExists);

    /* OM_DefineOption(sName, "B", bValue)
       OM_DefineOption(sName, "N", fValue)
       OM_DefineOption(sName, "S", sString)
       Defines an option of the specified type, and sets its initial value. This option can then
       be set or queried elsewhere. The Starlight engine has a list of option names and types it
       uses by default, all of which are defined in Configuration.lua at least once. */
    lua_register(pLuaState, "OM_DefineOption", &Hook_OM_DefineOption);

    lua_register(pLuaState, "OM_GetOption", Hook_OM_GetOption);
    lua_register(pLuaState, "OM_SetOption", Hook_OM_SetOption);
    lua_register(pLuaState, "OM_WriteConfigFiles", Hook_OM_WriteConfigFiles);
    lua_register(pLuaState, "OM_WriteLoadFile", Hook_OM_WriteLoadFile);

    /* OM_SetTranslation("Allocate", iTotalStrings)
       OM_SetTranslation("Set String", iSlot, sString)
       Used for building translation strings. Each utility using the program has a set of constants
       that its UI makes use of. */
    lua_register(pLuaState, "OM_SetTranslation", &Hook_OM_SetTranslation);

    /* OM_GetProgramTicksElapsed() (1 Integer)
       Returns an integer indicating how many ticks the program has been running for. */
    lua_register(pLuaState, "OM_GetProgramTicksElapsed", &Hook_OM_GetProgramTicksElapsed);

    /* OM_GetAutorun() (1 String)
       Returns a string of the Autorun argument passed to the program. Returns "Null" if there was
       no autorun provided. */
    lua_register(pLuaState, "OM_GetAutorun", &Hook_OM_GetAutorun);

    /* OM_ClearAutorun()
       Clears the Autorun argument. Useful to make sure the program doesn't immediately boot back
       into a game once it has been exited. */
    lua_register(pLuaState, "OM_ClearAutorun", &Hook_OM_ClearAutorun);

    /* OM_IsModActive(sModName) (1 Boolean)
       Returns true if the mod is active in the current game, false if not. */
    lua_register(pLuaState, "OM_IsModActive", &Hook_OM_IsModActive);

    /* OM_SetModActive(sModName)
       Marks a given mod name as active. */
    lua_register(pLuaState, "OM_SetModActive", &Hook_OM_SetModActive);

    /* OM_ClearModActivity()
       Clear all active mods. Used when quitting back to title. */
    lua_register(pLuaState, "OM_ClearModActivity", &Hook_OM_ClearModActivity);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_OM_RegisterConfigPath(lua_State *L)
{
    //OM_RegisterConfigPath(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_RegisterConfigPath");

    OptionsManager::Fetch()->RegisterConfigPath(lua_tostring(L, 1));
    return 0;
}
int Hook_OM_OptionExists(lua_State *L)
{
    //OM_OptionExists(sOptionName) (1 String)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_OptionExists");

    //--Setup.
    OptionsManager *rManager = OptionsManager::Fetch();

    //--Get the option's type, as a flag.
    int tOptionType = rManager->GetOptionType(lua_tostring(L, 1));

    //--Option does not exist:
    if(tOptionType == POINTER_TYPE_FAIL)
    {
        lua_pushstring(L, "FAIL");
    }
    //--Integer:
    else if(tOptionType == POINTER_TYPE_INT)
    {
        lua_pushstring(L, "I");
    }
    //--Float:
    else if(tOptionType == POINTER_TYPE_FLOAT)
    {
        lua_pushstring(L, "N");
    }
    //--String:
    else if(tOptionType == POINTER_TYPE_STRING)
    {
        lua_pushstring(L, "S");
    }
    //--Boolean:
    else if(tOptionType == POINTER_TYPE_BOOL)
    {
        lua_pushstring(L, "B");
    }
    //--Error:
    else
    {
        lua_pushstring(L, "FAIL");
    }
    return 1;
}
int Hook_OM_DefineOption(lua_State *L)
{
    //OM_DefineOption(sName, "B", bValue,  sConfigPath)
    //OM_DefineOption(sName, "I", iValue,  sConfigPath)
    //OM_DefineOption(sName, "N", fValue,  sConfigPath)
    //OM_DefineOption(sName, "S", sString, sConfigPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 4) return LuaArgError("OM_DefineOption");

    //--Setup
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    const char *pName = lua_tostring(L, 1);
    const char *pType = lua_tostring(L, 2);
    const char *pPath = lua_tostring(L, 4);

    //--Static flag.
    OptionsManager::xIsDefiningOption = true;

    //--Enumerations.
    const bool cTakeOwnership = true;
    const bool cDoNotTakeOwnership = false;

    //--Boolean
    if(!strcmp(pType, "B"))
    {
        SetMemoryData(__FILE__, __LINE__);
        bool *nPtr = (bool *)starmemoryalloc(sizeof(bool));
        *nPtr = lua_toboolean(L, 3);
        rOptionsManager->AddOption(pName, nPtr, POINTER_TYPE_BOOL, cTakeOwnership, pPath);
    }
    //--Integer
    else if(!strcmp(pType, "I"))
    {
        SetMemoryData(__FILE__, __LINE__);
        int *nPtr = (int *)starmemoryalloc(sizeof(int));
        *nPtr = lua_tointeger(L, 3);
        rOptionsManager->AddOption(pName, nPtr, POINTER_TYPE_INT, cTakeOwnership, pPath);
    }
    //--Floating Point.
    else if(!strcmp(pType, "N") || !strcmp(pType, "F"))
    {
        SetMemoryData(__FILE__, __LINE__);
        float *nPtr = (float *)starmemoryalloc(sizeof(float));
        *nPtr = lua_tonumber(L, 3);
        rOptionsManager->AddOption(pName, nPtr, POINTER_TYPE_FLOAT, cTakeOwnership, pPath);
    }
    //--String
    else if(!strcmp(pType, "S"))
    {
        char *nStringCopy = InitializeString("%s", lua_tostring(L, 3));
        rOptionsManager->AddOption(pName, nStringCopy, POINTER_TYPE_STRING, cDoNotTakeOwnership, pPath);
        free(nStringCopy);
    }

    //--Unset static flag.
    OptionsManager::xIsDefiningOption = false;

    return 0;
}
int Hook_OM_GetOption(lua_State *L)
{
    //OM_GetOption(sName)
    if(lua_gettop(L) != 1)
    {
        fprintf(stderr, "OM_GetOption:  Failed, invalid argument list\n");
        lua_pushnumber(L, OM_ERROR_INT);
        return 1;
    }

    //--Setup.
    OptionsManager *rManager = OptionsManager::Fetch();
    int tOptionType = rManager->GetOptionType(lua_tostring(L, 1));
    if(tOptionType == POINTER_TYPE_FAIL)
    {
        fprintf(stderr, "OM_GetOption:  Failed, option %s not found\n", lua_tostring(L, 1));
        lua_pushnumber(L, OM_ERROR_INT);
        return 1;
    }

    //--Returns.
    if(tOptionType == POINTER_TYPE_INT)
    {
        lua_pushinteger(L, rManager->GetOptionI(lua_tostring(L, 1)));
    }
    else if(tOptionType == POINTER_TYPE_FLOAT)
    {
        lua_pushnumber(L, rManager->GetOptionF(lua_tostring(L, 1)));
    }
    else if(tOptionType == POINTER_TYPE_STRING)
    {
        lua_pushstring(L, rManager->GetOptionS(lua_tostring(L, 1)));
    }
    else if(tOptionType == POINTER_TYPE_BOOL)
    {
        lua_pushboolean(L, rManager->GetOptionB(lua_tostring(L, 1)));
    }
    else
    {
        lua_pushinteger(L, 0);
    }
    return 1;
}
int Hook_OM_SetOption(lua_State *L)
{
    //--Sets the option requested. Second argument should match the type!
    //OM_SetOption(sName, SecondArg)
    if(lua_gettop(L) != 2) return fprintf(stderr, "OM_SetOption:  Failed, invalid argument list\n");

    //--Setup.
    OptionsManager *rManager = OptionsManager::Fetch();
    int tOptionType = rManager->GetOptionType(lua_tostring(L, 1));
    if(tOptionType == POINTER_TYPE_FAIL)
    {
        fprintf(stderr, "OM_SetOption:  Failed, option %s not found\n", lua_tostring(L, 1));
        lua_pushnumber(L, OM_ERROR_INT);
        return 1;
    }

    //--Setters.
    if(tOptionType == POINTER_TYPE_INT)
    {
        rManager->SetOptionI(lua_tostring(L, 1), lua_tointeger(L, 2));
    }
    else if(tOptionType == POINTER_TYPE_FLOAT)
    {
        rManager->SetOptionF(lua_tostring(L, 1), lua_tonumber(L, 2));
    }
    else if(tOptionType == POINTER_TYPE_STRING)
    {
        rManager->SetOptionS(lua_tostring(L, 1), lua_tostring(L, 2));
    }
    else if(tOptionType == POINTER_TYPE_BOOL)
    {
        rManager->SetOptionB(lua_tostring(L, 1), lua_toboolean(L, 2));
    }

    return 0;
}
int Hook_OM_WriteConfigFiles(lua_State *L)
{
    //OM_WriteConfigFiles()

    OptionsManager::Fetch()->WriteConfigFiles();
    return 0;
}
int Hook_OM_WriteLoadFile(lua_State *L)
{
    //OM_WriteLoadFile(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_WriteLoadFile");

    OptionsManager::Fetch()->WriteLoadFile(lua_tostring(L, 1));
    return 0;
}
int Hook_OM_SetTranslation(lua_State *L)
{
    ///--[Argument List]
    //OM_SetTranslation("Allocate", iTotalStrings)
    //OM_SetTranslation("Set String", iSlot, sString)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_SetTranslation");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    //if(!strcasecmp(rSwitchType, "Static Dummy"))
    //{
    //    lua_pushinteger(L, 0);
    //    return 1;
    //}

    ///--[Dynamic]
    //--Fast-access pointer.
    OptionsManager *rManager = OptionsManager::Fetch();

    ///--[System]
    //--Allocate strings.
    if(!strcasecmp(rSwitchType, "Allocate") && tArgs == 2)
    {
        rManager->AllocateTranslationStrings(lua_tointeger(L, 2));
    }
    //--Set a given string in a given slot.
    else if(!strcasecmp(rSwitchType, "Set String") && tArgs == 3)
    {
        rManager->SetTranslationString(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Error case.
    else
    {
        LuaPropertyError("OM_SetTranslation", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_OM_GetProgramTicksElapsed(lua_State *L)
{
    //OM_GetProgramTicksElapsed() (1 Integer)
    lua_pushinteger(L, Global::Shared()->gTicksElapsed);
    return 1;
}
int Hook_OM_GetAutorun(lua_State *L)
{
    //OM_GetAutorun() (1 String)
    if(!OptionsManager::xAutorun)
    {
        lua_pushstring(L, "Null");
    }
    else
    {
        lua_pushstring(L, OptionsManager::xAutorun);
    }
    return 1;
}
int Hook_OM_ClearAutorun(lua_State *L)
{
    //OM_ClearAutorun()
    ResetString(OptionsManager::xAutorun, NULL);
    return 0;
}
int Hook_OM_IsModActive(lua_State *L)
{
    //OM_IsModActive(sModName) (1 Boolean)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_IsModActive");

    //--Set.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    lua_pushboolean(L, rOptionsManager->IsModActive(lua_tostring(L, 1)));

    return 1;
}
int Hook_OM_SetModActive(lua_State *L)
{
    //OM_SetModActive(sModName)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_SetModActive");

    //--Set.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    rOptionsManager->SetModActive(lua_tostring(L, 1));

    return 0;
}
int Hook_OM_ClearModActivity(lua_State *L)
{
    //OM_ClearModActivity()
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    rOptionsManager->ClearModActivity();
    return 0;
}
