///======================================= KPopDanceGame ==========================================
//--Minigame where the player presses buttons to the beat. God damn. While largely self-sufficient,
//  the class is not built to take the place of a map class. It is meant to be held by a map class
//  and render/update over top of it.
#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Definitions ========================================
//--Arrow Indices
#define KPOP_ARROW_NON 0
#define KPOP_ARROW_TOP 1
#define KPOP_ARROW_RGT 2
#define KPOP_ARROW_BOT 3
#define KPOP_ARROW_LFT 4
#define KPOP_ARROW_ACT 5
#define KPOP_ARROW_CAN 6
#define KPOP_ARROW_TOTAL 7

//--Visibility Timing
#define KPOP_VIS_TICKS 30
#define KPOP_RESULT_TICKS 65

//--Popups
#define KPOP_ENCOURAGE_GOOD 0
#define KPOP_ENCOURAGE_GREAT 1
#define KPOP_ENCOURAGE_PERFECT 2
#define KPOP_ENCOURAGE_TOTAL 3

//--Timing
#define KPOP_TICKS_RANGE_PERFECT 3
#define KPOP_TICKS_RANGE_GREAT 6
#define KPOP_TICKS_RANGE_FINE 9
#define KPOP_TICKS_RANGE_HALFBEAT 16

//--Juice
#define KPOP_JUICE_SIZE_PERFECT 0.75f
#define KPOP_JUICE_SIZE_GREAT 0.50f
#define KPOP_JUICE_SIZE_FINE 0.25f

//--Beat Properties
#define KPOP_BEAT_FADE_TICKS 20
#define KPOP_BEAT_FADE_SIZE_BOOST 0.50f

//--Y Positions
#define KPOP_DANCER_Y 300.0f
#define KPOP_BEAT_CENTER_Y 530.0f

//--Score Spawn Positions
#define KPOP_SCORE_POS 8

///===================================== Local Structures =========================================
///--[KPopInstruction]
//--Represents an instruction for the player to fulfill.
typedef struct KPopInstruction
{
    //--Variables
    int mTimer;
    int mDirection;
    bool mDone;
    float mDoneXPos;
    int mFadeoutTimer;
    StarBitmap *rArrowImg;

    //--Methods
    void Initialize()
    {
        mTimer = 0;
        mDirection = KPOP_ARROW_NON;
        mDone = false;
        mFadeoutTimer = 0;
        rArrowImg = NULL;
    }
}KPopInstruction;

///--[KPopResultPack]
//--Represents a result, such as GOOD! or GREAT! from a keypress. If the bitmap is in
//  use that will render, otherwise, the font is used.
typedef struct KPopResultPack
{
    //--Timing
    int mTimer;

    //--Positioning
    float mX, mY;
    int mScoreSlot;

    //--Rendering
    StarFont *rFont;
    StarBitmap *rImage;
    char mBuffer[STD_MAX_LETTERS];

    //--Methods
    void Initialize()
    {
        mTimer = 0;
        mX = 0.0f;
        mY = 0.0f;
        mScoreSlot = -1;
        rFont = NULL;
        rImage = NULL;
        mBuffer[0] = '\0';
    }
}KPopResultPack;

///========================================== Classes =============================================
class KPopDanceGame : public RootObject
{
    private:
    //--System
    bool mIsActive;
    bool mStartedMusic;
    int mVisibilityTimer;

    //--Combos and Score
    int mComboCount;
    int mPerfectStreak;
    int mScore;

    //--Score Spawn Positions
    Point2DF mScoreSpawns[KPOP_SCORE_POS];
    bool mScoreOccupied[KPOP_SCORE_POS];

    //--Dance Instructions
    int mBeatTimer;
    float mBeatScrollRate;
    StarLinkedList *mInstructionList; //KPopInstruction *, master

    //--Results
    StarLinkedList *mResultList; //KPopResultPack *, master

    //--Dancers
    StarLinkedList *mDancerList; //KPopDancer *, master

    //--System Output
    bool mIsSystemOutputMode;
    FILE *fOutputFile;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rScoreFont;

            //--Arrows.
            StarBitmap *rArrows[KPOP_ARROW_TOTAL];

            //--Display
            StarBitmap *rInputBox;

            //--Popups
            StarBitmap *rPopupEncourage[KPOP_ENCOURAGE_TOTAL];
            StarBitmap *rPopupLate;
            StarBitmap *rPopupEarly;
            StarBitmap *rPopupWrong;
        }Data;
    }Images;

    protected:

    public:
    //--System
    KPopDanceGame();
    virtual ~KPopDanceGame();

    //--Public Variables
    //--Property Queries
    bool IsActive();
    bool IsVisible();

    //--Manipulators
    void SetActive(bool pFlag);
    void RegisterInstruction(int pTimer, int pDirection);
    KPopResultPack *RegisterResult(float pX, float pY, StarBitmap *pImage);
    KPopResultPack *RegisterResult(float pX, float pY, StarFont *pFont, const char *pText);
    void ActivateSystemOutput(const char *pFilePath);

    //--Core Methods
    void CreateDancer(const char *pDancerName);
    void CreateDanceMove(const char *pDancerName, const char *pMoveName);
    void AllocateDanceMoveFrames(const char *pDancerName, const char *pMoveName, int pFramesTotal);
    void SetDanceMoveFrame(const char *pDancerName, const char *pMoveName, int pFrame, int pTicks, const char *pImgPath);
    void PositionDancers();
    void GainScore(int pFlag);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    static KPopDanceGame *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_KPop_GetProperty(lua_State *L);
int Hook_KPop_SetProperty(lua_State *L);

