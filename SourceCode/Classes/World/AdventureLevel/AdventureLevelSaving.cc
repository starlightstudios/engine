//--Base
#include "AdventureLevel.h"

//--Classes
#include "TilemapActor.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"
#include "EntityManager.h"

///--[Debug]
//#define ADLEV_SAVE_DEBUG
#ifdef ADLEV_SAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

void AdventureLevel::WriteToBuffer(StarAutoBuffer *pBuffer)
{
    //--Write all the savefile information to the autobuffer provided. Called by the SaveManager during the saving sequence.
    //  The header has already been written.
    if(!pBuffer) return;

    //--Debug.
    DebugPush(true, "AdventureLevel:WriteToBuffer - Begin.\n");

    //--Write the name of this level. This is where the hard drive will look for it.
    pBuffer->AppendStringWithLen(mName);
    DebugPrint("Wrote name %s.\n", mName);

    //--The depth the player is currently at. If a save point were on a depth other than zero, this would cause the player
    //  to use the wrong collisions when loading.
    TilemapActor *rActor = (TilemapActor *)EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(rActor && rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        pBuffer->AppendInt8(rActor->GetCollisionDepth());
        DebugPrint("Wrote depth %i.\n", rActor->GetCollisionDepth());
    }
    //--Not found, write 0.
    else
    {
        pBuffer->AppendInt8(0);
        DebugPrint("Wrote depth %i, actor not found.\n", 0);
    }

    //--Write how many entities are dead. This list is static, it goes between rooms.
    pBuffer->AppendInt16(xDestroyedEnemiesList->GetListSize());
    DebugPrint("Wrote destroyed enemy count: %i\n", xDestroyedEnemiesList->GetListSize());

    //--Write the dead enemy names.
    void *rPtr = xDestroyedEnemiesList->PushIterator();
    while(rPtr)
    {
        //--Write.
        pBuffer->AppendStringWithLen(xDestroyedEnemiesList->GetIteratorName());
        DebugPrint(" Enemy: %s\n", xDestroyedEnemiesList->GetIteratorName());

        //--Next.
        rPtr = xDestroyedEnemiesList->AutoIterate();
    }

    //--Debug.
    DebugPop("AdventureLevel:WriteToBuffer - Complete.\n");
}
void AdventureLevel::ReadFromFile(VirtualFile *fInfile)
{
    //--Given a file in the correct position, reads level data out and stores it as appropriate. In this case,
    //  the level has already had its name read and has run its constructor.
    if(!fInfile) return;

    //--Read the player's depth and set that.
    int8_t tOverrideDepth = 0;
    fInfile->Read(&tOverrideDepth, sizeof(int8_t), 1);
    TilemapActor *rActor = (TilemapActor *)EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(rActor) rActor->SetCollisionDepth(tOverrideDepth);

    //--Read how many entities are dead.
    int16_t tDeadEnemiesCount = 0;
    fInfile->Read(&tDeadEnemiesCount, sizeof(int16_t), 1);

    //--Read dead enemy names, and remove them as they are read.
    for(int i = 0; i < tDeadEnemiesCount; i ++)
    {
        //--Name.
        char *tEnemyName = fInfile->ReadLenString();

        //--Remove it, by any means necessary.
        KillSpawnedEnemy(tEnemyName);

        //--Clean.
        free(tEnemyName);
    }
}
