//--Base
#include "CarnUIVendor.h"

//--Classes
#include "AdventureMenu.h"
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "CarnationCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"

///========================================== System ==============================================
CarnUIVendor::CarnUIVendor()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUIVENDOR;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Help Menu]

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIVendor ======= ]
    ///--[Per-Class Constants]
    cScrollBuf = cxCarnScrollBuf;
    cScrollPage = cxCarnScrollPageSize;

    ///--[System]
    ///--[Common]
    ///--[Categories]
    ///--[Shop Info]
    ///--[Comparison]
    ///--[Materials Pane]
    ///--[Details]
    ///--[Buy Mode]
    ///--[Sell Mode]
    ///--[Buyback Mode]
    ///--[Gems Mode]
    ///--[Unlock Mode]
    ///--[Merge Help Menu]
    ///--[Strings]
    ///--[Images]
    ///--[ ====== CarnUICommon ====== ]
    ///--[ ====== CarnUIVendor ====== ]
    ///--[System]
    mRecomputeScrollbarSize = true;

    ///--[Description]
    mDescriptionList = NULL;
    mProficiencyRenderList = new StarLinkedList(true);

    ///--[Fluffs]
    mFluffsWaitTimer = 0;
    mFluffsThinkTextTimer = cxFluffsThinkTextMax;
    mFluffsBurstTimer = 0;
    mFluffsSpeechTimeRemaining = 0;
    mFluffsFrame = cxFluffsNeutral;
    mFluffsDialogue = NULL;

    //--Random Dialogue.
    mFluffsGreetingPacks = new StarLinkedList(true);
    mFluffsPurchasePacks = new StarLinkedList(true);
    mFluffsSellPacks = new StarLinkedList(true);
    mFluffsCantAffordPacks = new StarLinkedList(true);
    mFluffsThinkingPacks = new StarLinkedList(true);
    mFluffsCantSellPacks = new StarLinkedList(true);

    ///--[Stat Indices]
    //--Stat indexes.
    mStatIndexes[0] = CARNATION_STAT_HP;
    mStatIndexes[1] = CARNATION_STAT_SP;
    mStatIndexes[2] = CARNATION_STAT_ATTACK;
    mStatIndexes[3] = CARNATION_STAT_MAGICATTACK;
    mStatIndexes[4] = CARNATION_STAT_DEFENSE;
    mStatIndexes[5] = CARNATION_STAT_MAGICDEFENSE;
    mStatIndexes[6] = CARNATION_STAT_INITIATIVE;
    mStatIndexes[7] = CARNATION_STAT_LUCK;

    //--Stat Titles.
    strcpy(mStatTitles[0], "HP");
    strcpy(mStatTitles[1], "SP");
    strcpy(mStatTitles[2], "Atk");
    strcpy(mStatTitles[3], "MAtk");
    strcpy(mStatTitles[4], "Def");
    strcpy(mStatTitles[5], "MDef");
    strcpy(mStatTitles[6], "Ini");
    strcpy(mStatTitles[7], "Lck");

    //--Proficiencies.
    mCharacterProficiencies = new StarLinkedList(true);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages",       &CarnImages,       sizeof(CarnImages));
    AppendVerifyPack("CarnImagesCommon", &CarnImagesCommon, sizeof(CarnImagesCommon));

    ///--[Hitboxes]
    //--Item hitboxes.
    float cItemLft = 128.0f;
    float cItemTop = 108.0f;
    float cItemWid = 277.0f;
    float cItemHei =  24.0f;
    for(int i = cxHitbox_ItemsLo; i <= cxHitbox_ItemsHi; i ++)
    {
        char tBuf[128];
        sprintf(tBuf, "Item%02i", i);
        RegisterHitboxWH(tBuf, cItemLft, cItemTop + (cItemHei * i), cItemWid, cItemHei-3.0f);
    }

    //--Buy and Sell buttons.
    RegisterHitboxWH("Buy",  147.0f, 20.0f, 124.0f, 41.0f);
    RegisterHitboxWH("Sell", 339.0f, 20.0f, 124.0f, 41.0f);

    //--Character select.
    RegisterHitboxWH("SelectChar0",  965.0f, 552.0f, 110.0f, 129.0f);
    RegisterHitboxWH("SelectChar1", 1083.0f, 552.0f, 110.0f, 129.0f);
    RegisterHitboxWH("SelectChar2", 1201.0f, 552.0f, 110.0f, 129.0f);

    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 456.0f, 101.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollBd", 456.0f, 159.0f, 36.0f, 192.0f);
    RegisterHitboxWH("ScrollDn", 456.0f, 351.0f, 36.0f,  58.0f);

    //--Confirm/Cancel.
    RegisterHitboxWH("Confirm", 327.0f, 366.0f, 228.0f, 61.0f);
    RegisterHitboxWH("Cancel",  815.0f, 366.0f, 228.0f, 61.0f);

    //--Exit the UI.
    RegisterHitboxWH("Exit",  4.0f, 703.0f, 228.0f, 61.0f);

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mSkip;
    nScrollbarPack->mPerPage = cxCarnScrollPageSize;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxHitbox_ScrollUp;
    nScrollbarPack->mIndexBd = cxHitbox_ScrollBd;
    nScrollbarPack->mIndexDn = cxHitbox_ScrollDn;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;

    ///--[Proficiency Positions]
    //--Constants.
    float cLftL =  935.0f;
    float cLftR = 1022.0f;
    float cRgtL = 1148.0f;
    float cRgtR = 1235.0f;
    float cYPos =  433.0f;
    float cHei  =   26.0f;

    //--Sword.
    ProficiencyRenderPack *nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cLftL, cLftR, cYPos + (cHei * 0.0f), "Sword", "Weapon Sword", 0);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);

    //--Spear.
    nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cLftL, cLftR, cYPos + (cHei * 1.0f), "Spear", "Weapon Spear", 1);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);

    //--Hammer.
    nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cLftL, cLftR, cYPos + (cHei * 2.0f), "Hammer", "Weapon Hammer", 2);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);

    //--Bow.
    nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cLftL, cLftR, cYPos + (cHei * 3.0f), "Bow", "Weapon Bow", 3);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);

    //--Staff.
    nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cRgtL, cRgtR, cYPos + (cHei * 0.0f), "Staff", "Weapon Staff", 4);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);

    //--Wand.
    nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cRgtL, cRgtR, cYPos + (cHei * 1.0f), "Wand", "Weapon Wand", 5);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);

    //--Whip.
    nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cRgtL, cRgtR, cYPos + (cHei * 2.0f), "Whip", "Weapon Whip", 6);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);

    //--Focus.
    nNewPack = (ProficiencyRenderPack *)starmemoryalloc(sizeof(ProficiencyRenderPack));
    nNewPack->Set(cRgtL, cRgtR, cYPos + (cHei * 3.0f), "Focus", "Weapon Focus", 7);
    mProficiencyRenderList->AddElement("X", nNewPack, &FreeThis);
}
CarnUIVendor::~CarnUIVendor()
{
    delete mDescriptionList;
    delete mProficiencyRenderList;
    delete mFluffsDialogue;
    delete mFluffsGreetingPacks;
    delete mFluffsPurchasePacks;
    delete mFluffsSellPacks;
    delete mFluffsCantAffordPacks;
    delete mFluffsThinkingPacks;
    delete mFluffsCantSellPacks;
    delete mCharacterProficiencies;
}
void CarnUIVendor::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Carnation Vendor UI",           &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI",           &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Vendor UI Carnation", &AdvImages,        sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",             &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
bool CarnUIVendor::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_CARNUIVENDOR) return true;
    if(pType == POINTER_TYPE_ADVUIVENDOR) return true;
    if(pType == POINTER_TYPE_STARUIPIECE)  return true;
    return (pType == mType);
}
const char *CarnUIVendor::GetProficiencyStringFromCode(int pCode)
{
    if(pCode == 1) return "Untrained";
    if(pCode == 2) return "Trained";
    if(pCode == 3) return "Skilled";
    if(pCode == 4) return "Master";
    if(pCode == 5) return "Grandmaster";
    return "Unhandled";
}

///======================================= Manipulators ===========================================
void CarnUIVendor::TakeForeground()
{
    ///--[Documentation]
    //--Handles this object becoming the main object.
    mShopInventory->ClearList();
    mCompareCharacterCur = 0;

    ///--[Execution]
    //--Adjust the scrollbar size to match the inventory size. This has to be done after registering
    //  all the items.
    mRecomputeScrollbarSize = true;

    //--Roll a random greeting.
    RollQuote("Greeting", 15, false);

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|ShopOpens");
}
void CarnUIVendor::SetFluffsMood(int pEmotionFlag, const char *pDialogue, bool pDisallowBounce)
{
    ///--[Documentation]
    //--Sets Fluffs' mood to the given flag, setting bounce and dialogue timers as needed.
    if(!pDialogue) return;

    ///--[Emotion Set]
    //--Set the frame, clamp it.
    mFluffsFrame = pEmotionFlag;
    if(mFluffsFrame <             0) mFluffsFrame = 0;
    if(mFluffsFrame > cxFluffsUpset) mFluffsFrame = cxFluffsUpset;

    ///--[Bounce]
    //--If allowed, set the bounce timer.
    if(!pDisallowBounce || true)
    {
        mFluffsBurstTimer = cxFluffsBurstMax;
    }

    ///--[Dialogue]
    //--Timers.
    mFluffsSpeechTimeRemaining = cxFluffsTextTicksMax;

    //--Place the dialogue into a linked-list format.
    delete mFluffsDialogue;
    mFluffsDialogue = CreateDescriptionListFromString(pDialogue, CarnImages.rFont_Fluffs, 330.0f);
}
void CarnUIVendor::SetFluffsMoodByString(const char *pDialogue, bool pDisallowBounce)
{
    ///--[Documentation]
    //--The first part of the string should contain a tag, such as "[Happy]" or ["Bored"] that refers
    //  to Fluffs' emotion. This function resolves that and then passes it to SetFluffsMood().
    if(!pDialogue) return;

    ///--[Execution]
    //--Scan for the [ and ].
    int tLft = -1;
    int tRgt = -1;
    int tLen = strlen(pDialogue);
    for(int i = 0; i < tLen; i ++)
    {
        if(pDialogue[i] == '[') tLft = i;
        if(pDialogue[i] == ']') tRgt = i;
    }

    //--If either did not resolve, or did so illegally, stop.
    if(tLft == -1 || tRgt == -1 || tLft >= tRgt)
    {
        fprintf(stderr, "CarnUIVendor:SetFluffsMoodByString() - Warning. String %s failed to resolve a tag.\n", pDialogue);
        return;
    }

    ///--[Decide Emotion]
    if(!strncasecmp(&pDialogue[tLft+1], "Neutral", 7))
    {
        SetFluffsMood(cxFluffsNeutral, &pDialogue[tRgt+1], pDisallowBounce);
        return;
    }
    if(!strncasecmp(&pDialogue[tLft+1], "Happy", 5))
    {
        SetFluffsMood(cxFluffsHappy, &pDialogue[tRgt+1], pDisallowBounce);
        return;
    }
    if(!strncasecmp(&pDialogue[tLft+1], "Bored", 5))
    {
        SetFluffsMood(cxFluffsBored, &pDialogue[tRgt+1], pDisallowBounce);
        return;
    }
    if(!strncasecmp(&pDialogue[tLft+1], "Upset", 5))
    {
        SetFluffsMood(cxFluffsUpset, &pDialogue[tRgt+1], pDisallowBounce);
        return;
    }

    //--Failed.
    fprintf(stderr, "CarnUIVendor:SetFluffsMoodByString() - Warning. String %s resolved a tag, but the emotion was not handled.\n", pDialogue);
}
void CarnUIVendor::ClearFluffsDialoguePacks()
{
    ///--[Documentation]
    //--Clears all packages. Usually called by Lua.

    ///--[Execution]
    mFluffsGreetingPacks->ClearList();
    mFluffsPurchasePacks->ClearList();
    mFluffsSellPacks->ClearList();
    mFluffsCantAffordPacks->ClearList();
    mFluffsThinkingPacks->ClearList();
    mFluffsCantSellPacks->ClearList();
}
void CarnUIVendor::AddDialoguePack(const char *pType, const char *pString)
{
    ///--[Documentation]
    //--Registers the given string to the associated list. Called from Lua.
    if(!pType || !pString) return;

    //--Pointers.
    StarLinkedList *rRegisterList = NULL;

    ///--[Execution]
    //--Resolve pack:
    if(!strcasecmp(pType, "Greeting"))
        rRegisterList = mFluffsGreetingPacks;
    else if(!strcasecmp(pType, "Purchase"))
        rRegisterList = mFluffsPurchasePacks;
    else if(!strcasecmp(pType, "Sell"))
        rRegisterList = mFluffsSellPacks;
    else if(!strcasecmp(pType, "CantAfford"))
        rRegisterList = mFluffsCantAffordPacks;
    else if(!strcasecmp(pType, "Thinking"))
        rRegisterList = mFluffsThinkingPacks;
    else if(!strcasecmp(pType, "CantSell"))
        rRegisterList = mFluffsCantSellPacks;

    //--If the list failed to resolve, stop.
    if(!rRegisterList)
    {
        fprintf(stderr, "CarnUIVendor::AddDialoguePack() - Warning. Cannot resolve type %s\n", pType);
        return;
    }

    //--Register.
    rRegisterList->AddElementAsTail("X", InitializeString(pString), &FreeThis);
}
void CarnUIVendor::RollQuote(const char *pType, int pDelay, bool pDisallowBounce)
{
    ///--[Documentation]
    //--Picks a random quote from the associated list and makes Fluffs say it.
    if(!pType) return;

    //--Pointers.
    StarLinkedList *rQuoteList = NULL;

    ///--[Resolve List]
    //--Resolve pack:
    if(!strcasecmp(pType, "Greeting"))
        rQuoteList = mFluffsGreetingPacks;
    else if(!strcasecmp(pType, "Purchase"))
        rQuoteList = mFluffsPurchasePacks;
    else if(!strcasecmp(pType, "Sell"))
        rQuoteList = mFluffsSellPacks;
    else if(!strcasecmp(pType, "CantAfford"))
        rQuoteList = mFluffsCantAffordPacks;
    else if(!strcasecmp(pType, "Thinking"))
        rQuoteList = mFluffsThinkingPacks;
    else if(!strcasecmp(pType, "CantSell"))
        rQuoteList = mFluffsCantSellPacks;

    //--Failed.
    if(!rQuoteList)
    {
        fprintf(stderr, "CarnUIVendor:RollQuote() - Failed. No list type %s\n", pType);
        return;
    }

    ///--[Pick Quote]
    //--Set delay.
    mFluffsWaitTimer = pDelay;

    //--Set the thinking timer.
    mFluffsThinkTextTimer = (rand() % (cxFluffsThinkTextMax - cxFluffsThinkTextMin)) + cxFluffsThinkTextMin;

    //--Error check.
    int tQuotesToPickFrom = rQuoteList->GetListSize();
    if(tQuotesToPickFrom < 1) return;

    //--Roll.
    int tRoll = rand() % tQuotesToPickFrom;

    //--Set.
    char *rString = (char *)rQuoteList->GetElementBySlot(tRoll);
    SetFluffsMoodByString(rString, false);

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|FluffsSpeaks");
}
void CarnUIVendor::ClearProficiencies()
{
    mCharacterProficiencies->ClearList();
}
void CarnUIVendor::RegisterProficiencyCharacter(const char *pName)
{
    ///--[Documentation]
    //--Registers a character proficiency pack for the named character.
    if(!pName) return;

    //--Duplicate check.
    void *rCheckPtr = mCharacterProficiencies->GetElementByName(pName);
    if(rCheckPtr) return;

    ///--[Execution]
    //--Create, register.
    ProficiencyPack *nPack = (ProficiencyPack *)starmemoryalloc(sizeof(ProficiencyPack));
    memset(nPack, 0, sizeof(ProficiencyPack));
    mCharacterProficiencies->AddElementAsTail(pName, nPack, &FreeThis);
}
void CarnUIVendor::SetProficiencyFor(const char *pName, const char *pType, int pValue)
{
    ///--[Documentation]
    //--Sets a proficiency inside the proficiency pack.
    if(!pName || !pType) return;

    //--Locate.
    ProficiencyPack *rProficiencyPack = (ProficiencyPack *)mCharacterProficiencies->GetElementByName(pName);
    if(!rProficiencyPack) return;

    ///--[Execution]
    //--Set by type.
    if(!strcasecmp(pType, "Sword"))
        rProficiencyPack->mLookups[0] = pValue;
    else if(!strcasecmp(pType, "Spear"))
        rProficiencyPack->mLookups[1] = pValue;
    else if(!strcasecmp(pType, "Hammer"))
        rProficiencyPack->mLookups[2] = pValue;
    else if(!strcasecmp(pType, "Bow"))
        rProficiencyPack->mLookups[3] = pValue;
    else if(!strcasecmp(pType, "Staff"))
        rProficiencyPack->mLookups[4] = pValue;
    else if(!strcasecmp(pType, "Wand"))
        rProficiencyPack->mLookups[5] = pValue;
    else if(!strcasecmp(pType, "Whip"))
        rProficiencyPack->mLookups[6] = pValue;
    else if(!strcasecmp(pType, "Focus"))
        rProficiencyPack->mLookups[7] = pValue;
    else
    {
        fprintf(stderr, "CarnUIVendor:SetProficiencyFor() - Warning. Unhandled proficiency %s\n", pType);
    }
}

///======================================= Core Methods ===========================================
void CarnUIVendor::RefreshMenuHelp()
{

}
void CarnUIVendor::RecomputeCursorPositions()
{

}
void CarnUIVendor::ScrollMode(int pAmount)
{
    ///--[Documentation]
    //--Only allows scrolling from buy to sell.
    if(pAmount == 0) return;

    ///--[Lockout]
    //--If this flag is set, modes cannot scroll.
    if(mIsBuyOnly) return;

    ///--[Mode Switching]
    if(mIsBuyMode)
    {
        ActivateModeSell();
    }
    else if(mIsSellMode)
    {
        ActivateModeBuy();
    }

    //--Scrollbars.
    mRecomputeScrollbarSize = true;

    //--Clear description.
    delete mDescriptionList;
    mDescriptionList = NULL;

    ///--[SFX]
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
}
int CarnUIVendor::CheckEquippableFromPurchase(AdventureItem *pItem)
{
    ///--[Documentation]
    //--After an item is purchased, check if the party can in some way equip this item. If so, it will
    //  return the active party slot of the character who can equip the item. If not, returns -1.
    //--This is a modified version, the selected character is queried if they can equip the item.
    //  In Carnation this is nominally always true (everyone can use everything) but we check anyway.
    if(!pItem) return -1;

    //--Fast-access pointers.
    AdvCombat *rCombat = AdvCombat::Fetch();
    AdvCombatEntity *rCharacter = rCombat->GetActiveMemberI(mCompareCharacterCur);
    if(!rCharacter) return -1;

    ///--[Execution]
    //--Can the character equip it?
    if(pItem->IsEquippableBy(rCharacter->GetName()))
    {
        return mCompareCharacterCur;
    }

    //--Cannot equip.
    return -1;
}
void CarnUIVendor::RebuildDescriptionList(AdventureItem *pItem)
{
    ///--[Documentation]
    //--This UI uses StarLinkedLists to handle descriptions. This should be called whenever the cursor
    //  changes and description needs rebuilding.
    delete mDescriptionList;
    mDescriptionList = NULL;
    if(!pItem) return;

    ///--[Build]
    mDescriptionList = Subdivide::SubdivideStringFlags(pItem->GetDescription(), SUBDIVIDE_FLAG_COMMON_BREAKPOINTS | SUBDIVIDE_FLAG_FONTLENGTH | SUBDIVIDE_FLAG_ALLOW_BACKTRACK, CarnImages.rFont_Description, 730.0f);
}
void CarnUIVendor::SetComparisonCharacter(int pPartySlot)
{
    ///--[Documentation]
    //--Sets the comparison character to the given slot. There is no need to perform any checks in Carnation
    //  since the UI checks on the render call and does nothing if an item is not equippable.
    mCompareCharacterCur = pPartySlot;
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    return;
}
void CarnUIVendor::RecheckComparisonCharacter(AdventureItem *pItem)
{
    //--Does nothing, as the comparison cursor does not change when the item focus changes in Carnation.
}
void CarnUIVendor::EquipPurchasedItem()
{
    ///--[Documentation]
    //--Identical to the base version, but does not allow gem transferrence.
    AdvCombatEntity *rEntity = AdvCombat::Fetch()->GetActiveMemberI(mAskToEquipSlot);
    AdventureItem *rLastReggedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
    if(!rEntity || !rLastReggedItem) return;

    ///--[Slot Selection]
    //--Dummy variable.
    int tDummy = 5;

    //--Make a list of all the slots that can accept this item. Weapon alternates are ignored.
    int tEquipSlotsTotal = rEntity->GetEquipmentSlotsTotal();
    StarLinkedList *tSlotList = new StarLinkedList(false);
    for(int i = 0; i < tEquipSlotsTotal; i ++)
    {
        //--Ignore weapon alts.
        if(rEntity->IsWeaponAlternateSlot(i)) continue;

        //--If it can be equipped in this slot, store it.
        const char *rSlotName = rEntity->GetNameOfEquipmentSlot(i);
        if(rLastReggedItem->IsEquippableIn(rSlotName))
        {
            tSlotList->AddElement(rSlotName, &tDummy);
        }
    }

    //--We now have a list of slots that can accept the item. If the list somehow came back zero-length, do nothing.
    if(tSlotList->GetListSize() < 1)
    {
        delete tSlotList;
        return;
    }

    //--If there is exactly one entry, equip to that slot. Done!
    int tUseSlot = -1;
    if(tSlotList->GetListSize() == 1)
    {
    }
    //--Otherwise, check if any of the slots in question are empty. If so, use those.
    else
    {
        for(int i = 0; i < tSlotList->GetListSize(); i ++)
        {
            //--Name.
            const char *rSlotName = tSlotList->GetNameOfElementBySlot(i);

            //--Check if the slot is empty.
            AdventureItem *rCheckItem = rEntity->GetEquipmentBySlotS(rSlotName);
            if(!rCheckItem)
            {
                tUseSlot = i;
                break;
            }
        }
    }

    ///--[Equip Action]
    //--If no slot was selected, use the slot that is being compared against.
    bool tAcceptedItem = false;
    if(tUseSlot == -1)
    {
        tAcceptedItem = rEntity->EquipItemToSlotNoGems(mCompareSlot, rLastReggedItem);
    }
    //--Otherwise, equip to the slot.
    else
    {
        const char *rSlotName = tSlotList->GetNameOfElementBySlot(tUseSlot);
        tAcceptedItem = rEntity->EquipItemToSlotNoGems(rSlotName, rLastReggedItem);
    }

    //--If flagged, the entity accepted the item.
    if(tAcceptedItem) AdventureInventory::Fetch()->LiberateItemP(rLastReggedItem);

    ///--[Inventory Handling]
    //--Items do not get their gems removed in Carnation.

    //--Recompute statistics.
    rLastReggedItem->ComputeStatistics();
    rEntity->ComputeStatistics();

    ///--[External Call]
    //--If a script is specified, call it to handle any post-menu changes.
    SetCloseCode("Change Equipment Vendor");

    ///--[Finish Up]
    //--Clean.
    delete tSlotList;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIVendor::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updating timers and handling player control inputs for this menu. The Vendor UI is
    //  a standalone UI, it closes when the player exits.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer,           0, mVisibilityTimerMax,  true);
    StandardTimer(mDetailsTimer,              0, cxAdvDetailsTicks,    mIsShowingDetails);
    if(mFluffsWaitTimer > 0)
    {
        mFluffsWaitTimer --;
    }
    else
    {
        StandardTimer(mFluffsBurstTimer,          0, cxFluffsBurstMax,     false);
        StandardTimer(mFluffsSpeechTimeRemaining, 0, cxFluffsTextTicksMax, false);
        StandardTimer(mFluffsThinkTextTimer,      0, cxFluffsThinkTextMax, false);
    }

    //--If the think text timer hits zero, spawn a think quote. This resets the timer.
    if(mFluffsThinkTextTimer < 1)
    {
        RollQuote("Thinking", 0, false);
    }

    //--If the speech timer is exactly 1, set a burst.
    if(mFluffsSpeechTimeRemaining == 1) mFluffsBurstTimer = cxFluffsBurstMax;

    //--Comparison character timer.
    if(mCompareSwapTimer < cxAdvCharSwapTicks) mCompareSwapTimer ++;

    //--Arrow oscillation.
    mArrowTimer = (mArrowTimer + 1) % cxAdvOscTicks;

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
    mCategoryHighlightPos.Increment(EASING_CODE_QUADOUT);
    mCategoryHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Submodes]
    //--Pass the update on to the submodes. Buyback and gems are not available in this game.
    if(mIsBuyMode)
    {
        UpdateModeBuy();
    }
    else if(mIsSellMode)
    {
        UpdateModeSell();
    }

    ///--[Finish]
    return true;
}
void CarnUIVendor::UpdateBackground()
{
    ///--[Documentation]
    //--Run timers and exit.
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
    StandardTimer(mDetailsTimer,    0, cxAdvDetailsTicks,   false);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIVendor::RenderForeground(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders vendor mode for the Carnation UI. This call handles the backing in addition to the normal
    //  routines as the base UI does not render a backing in front of it.

    ///--[Visibility Checks]
    //--Images failed to resolve. Do nothing.
    if(!mImagesReady) return;

    //--Caller set the visibility at zero, do nothing.
    if(pVisAlpha <= 0.0f) return;

    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    ///--[Call Pieces Rendering]
    //--Compute how on-screen this object is by calculating the alpha from the vis timer. Multiply
    //  with the provided alpha so the host doesn't fade differently from sub-components.
    float cLocalAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)mVisibilityTimerMax) * pVisAlpha;

    //--Backing.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cLocalAlpha));

    //--Call subroutine. Can be overriden.
    RenderPieces(cLocalAlpha);

    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void CarnUIVendor::RenderComparisonPane(AdventureItem *pComparisonItem, float pAlpha)
{
    ///--[Documentation]
    //--Renders the properties of whatever item is selected if it were to be equipped by the comparison
    //  character. If there is no comparison character, doesn't render anything except the header.
    //--If the item is a gem, doesn't render the header, letting the subroutine do that.

    ///--[Header]
    //--Title of Stats Area. Renders even if there is no item for properties.
    CarnImages.rFont_Name->DrawText(1137.0f, 115.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Properties");

    //--Shows the current party members.
    AdvCombat *rCombat = AdvCombat::Fetch();
    if(rCombat)
    {
        //--Get party size, iterate.
        int tPartySize = rCombat->GetActivePartyCount();
        for(int i = 0; i < tPartySize; i ++)
        {
            //--Get party member.
            AdvCombatEntity *rAdvEntity = rCombat->GetActiveMemberI(i);
            if(!rAdvEntity || !rAdvEntity->IsOfType(POINTER_TYPE_CARNATIONCOMBATENTITY)) continue;

            //--Cast.
            CarnationCombatEntity *rCarnationEntity = (CarnationCombatEntity *)rAdvEntity;

            //--If this entity is not the comparison entity, grey it out.
            if(i != mCompareCharacterCur) StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);

            //--Render a portrait box.
            float cXOffset = i * 118.0f;
            CarnImages.rOverlay_PartyPortraitBox->Draw(cXOffset, 0.0f);

            //--Render.
            float cXPos = 949.0f + cXOffset;
            float cYPos = 537.0f;
            glTranslatef(cXPos, cYPos, 0.0f);
            glScalef(0.55f, 0.55f, 1.0f);
            rCarnationEntity->RenderSwitchableAt(0, 0);
            glScalef(1.0f / 0.55f, 1.0f / 0.55f, 1.0f);
            glTranslatef(-cXPos, -cYPos, 0.0f);

            //--Clean.
            StarlightColor::ClearMixer();
        }
    }

    //--Make sure the item exists. If it does not, stop here. We render the header in all cases.
    if(!pComparisonItem || mCompareCharacterCur == -1 || !pComparisonItem->IsEquipment())
    {
        return;
    }

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);

    //--Setup variables.
    float cNameX  = 981.0f;
    float cValueL = 1175.0f;
    float cValueR = 1285.0f;
    float cYPos = 156.0f;
    float cYHei = CarnImages.rFont_Property->GetTextHeight();

    ///--[Iterate Across Properties]
    //--Render.
    for(int i = 0; i < VARNUIVEND_STAT_TITLES_TOTAL; i ++)
    {
        //--Resolve Y position.
        float cUseY = cYPos + (cYHei * i);

        ///--[Basics]
        //--Name.
        CarnImages.rFont_Property->DrawText(cNameX, cUseY, 0, 1.0f, mStatTitles[i]);

        //--Statistic / Resistance.
        int tEntityValue = rActiveEntity->GetStatistic(mStatIndexes[i]);

        ///--[Stat Comparison - Base Property]
        //--This is a simple property with only one value to render.
        int tOldStatValue = 0;
        int tNewStatValue = 0;

        //--Stat value of the equipped item.
        AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
        if(rCurrentItem)
        {
            tOldStatValue = rCurrentItem->GetStatistic(mStatIndexes[i]);
        }

        //--Stat value of the replacement.
        tNewStatValue = pComparisonItem->GetStatistic(mStatIndexes[i]);

        ///--[Render]
        //--Base value.
        CarnImages.rFont_Property->DrawTextArgs(cValueL, cUseY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue);

        //--If the value in this slot is different from the old value, then display that.
        if(tOldStatValue != tNewStatValue)
        {
            //--Value is lower:
            if(tNewStatValue < tOldStatValue)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
            }
            //--Higher:
            else
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
            }

            //--Render.
            CarnImages.rFont_Property->DrawTextArgs(cValueR - 80, cUseY, 0, 1.0f, "->");
            CarnImages.rFont_Property->DrawTextArgs(cValueR,      cUseY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue + (tNewStatValue - tOldStatValue));

            //--Clean.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }
    }

    ///--[Proficiencies]
    //--Title.
    CarnImages.rFont_Name->DrawText(1137.0f, 379.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Proficiencies");

    //--Get proficiency data.
    ProficiencyPack *rProficiencyPack = (ProficiencyPack *)mCharacterProficiencies->GetElementByName(rActiveEntity->GetName());
    if(!rProficiencyPack) return;

    //--Iterate across the proficiency render packs.
    ProficiencyRenderPack *rPackage = (ProficiencyRenderPack *)mProficiencyRenderList->PushIterator();
    while(rPackage)
    {
        //--If the tag is not present for this comparison type, grey out the text.
        int tTags = pComparisonItem->GetTagCount(rPackage->mCheckTag);
        if(tTags < 1)
        {
            StarlightColor::SetMixer(0.40f, 0.40f, 0.40f, pAlpha);
        }
        else
        {
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }

        //--Render.
        CarnImages.rFont_Property->DrawTextArgs(rPackage->mLft, rPackage->mY, 0, 1.0f, rPackage->mRenderType);
        CarnImages.rFont_Property->DrawTextArgs(rPackage->mRgt, rPackage->mY, 0, 1.0f, "%s", GetProficiencyStringFromCode(rProficiencyPack->mLookups[rPackage->mLookupSlot]));

        //--Next.
        rPackage = (ProficiencyRenderPack *)mProficiencyRenderList->AutoIterate();
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void CarnUIVendor::RenderFluffs()
{
    ///--[Documentation]
    //--Renders our favourite shopkeeper, Fluffs! This includes modifying her portrait position,
    //  dialogue, and changing emotions.

    ///--[Base]
    //--If the speech timer is zero, always render neutral.
    StarBitmap *rFluffsFrame = CarnImages.rOverlay_FluffsNeutral;

    //--Speech timer is nonzero, resolve Fluff's emotion.
    if(mFluffsSpeechTimeRemaining > 0 && mFluffsWaitTimer < 1)
    {
        if(mFluffsFrame == cxFluffsNeutral)
            rFluffsFrame = CarnImages.rOverlay_FluffsNeutral;
        else if(mFluffsFrame == cxFluffsHappy)
            rFluffsFrame = CarnImages.rOverlay_FluffsHappy;
        else if(mFluffsFrame == cxFluffsBored)
            rFluffsFrame = CarnImages.rOverlay_FluffsBored;
        else if(mFluffsFrame == cxFluffsUpset)
            rFluffsFrame = CarnImages.rOverlay_FluffsUpset;
    }

    //--If the frame failed to resolve, stop.
    if(!rFluffsFrame) return;

    //--Bounce handling. If the bounce timer is zero, draw Fluffs as normal.
    if(mFluffsBurstTimer < 1 || mFluffsWaitTimer > 0)
    {
        rFluffsFrame->Draw();
    }
    //--When bouncing, slightly distort the frame sinusoidally.
    else
    {
        //--Computations
        float cPct = (mFluffsBurstTimer / (float)cxFluffsBurstMax);
        float cSizeFactor = 0.95f + (sinf(cPct * 3.1415926f) * 0.05f);
        if(cSizeFactor < 0.01f) return;

        //--Positioning.
        float cYOffset = rFluffsFrame->GetHeight() * (1.0f - cSizeFactor);
        float cLft = rFluffsFrame->GetXOffset();
        float cTop = rFluffsFrame->GetYOffset() + cYOffset;
        glTranslatef(cLft, cTop, 0.0f);

        //--Sizing.
        float cSizeInv = 1.0f / cSizeFactor;
        glScalef(1.0f, cSizeFactor, 1.0f);

        //--Render.
        rFluffsFrame->Bind();
        rFluffsFrame->RenderAt();

        //--Clean.
        glScalef(1.0f, cSizeInv, 1.0f);
        glTranslatef(-cLft, -cTop, 0.0f);
    }

    ///--[Dialogue]
    if(mFluffsSpeechTimeRemaining > 0 && mFluffsDialogue && mFluffsWaitTimer < 1)
    {
        //--Setup.
        float cTxtX = 724.0f;
        float cTxtY =  51.0f;
        float cTxtH = CarnImages.rFont_Fluffs->GetTextHeight();

        //--Backing.
        CarnImages.rOverlay_FluffsSpeech->Draw();

        //--Render dialogue.
        char *rString = (char *)mFluffsDialogue->PushIterator();
        while(rString)
        {
            //--Render.
            CarnImages.rFont_Fluffs->DrawText(cTxtX, cTxtY, SUGARFONT_AUTOCENTER_X, 1.0f, rString);

            //--Next.
            cTxtY = cTxtY + cTxtH;
            rString = (char *)mFluffsDialogue->AutoIterate();
        }
    }
}

///====================================== Pointer Routing =========================================
StarLinkedList *CarnUIVendor::GetCharacterProficiencyList()
{
    return mCharacterProficiencies;
}
StarLinkedList *CarnUIVendor::GetProficiencyRenderList()
{
    return mProficiencyRenderList;
}

///===================================== Static Functions =========================================
CarnUIVendor *CarnUIVendor::Fetch()
{
    //--Locates and returns the CarnationMenu version of this class. It is possible this doesn't exist
    //  so error-check the results.
    AdventureMenu *rAdventureMenu = AdventureMenu::Fetch();
    StarUIPiece *rStarUIPiece = rAdventureMenu->RetrieveUI("Vendor", POINTER_TYPE_CARNUIVENDOR, NULL, "CarnUIVendor::Fetch()");
    if(!rStarUIPiece) return NULL;

    //--Dynamic cast.
    CarnUIVendor *rFetchedUI = dynamic_cast<CarnUIVendor *>(rStarUIPiece);
    return rFetchedUI;
}

///======================================== Lua Hooking ===========================================
void CarnUIVendor::HookToLuaState(lua_State *pLuaState)
{
    /* [System]
       CarnUIVendor_SetProperty("Open Event Mode")
       Sets the requested property in the CarnUIVendor. */
    lua_register(pLuaState, "CarnUIVendor_SetProperty", &Hook_CarnUIVendor_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnUIVendor_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    //CarnUIVendor_SetProperty("Population Script", sPath) (Static)

    ///--[Dynamic]
    //--[Fluffs Dialogue]
    //CarnUIVendor_SetProperty("Clear Fluffs Dialogue")
    //CarnUIVendor_SetProperty("Register Fluffs Dialogue", sType, sDialogue)

    //--[Proficiencies]
    //CarnUIVendor_SetProperty("Clear Proficiencies")
    //CarnUIVendor_SetProperty("Register Proficiency Character", sName)
    //CarnUIVendor_SetProperty("Set Proficiency Value", sName, sType, iValue)

    ///--[ =========== Arg Resolve ============ ]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnUIVendor_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    //--Statics.
    if(!strcasecmp(rSwitchType, "Population Script") && tArgs >= 2) //Sample
    {
        //ResetString(CarnUIClassChange::xPopulateScript, lua_tostring(L, 2));
        return 0;
    }

    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("CarnUIVendor_SetProperty", L);

    //--Get the sub-object.
    CarnUIVendor *rUIObject = dynamic_cast<CarnUIVendor *>(rMenu->RetrieveUI("Vendor", POINTER_TYPE_CARNUIVENDOR, NULL, "CarnUIVendor_SetProperty"));
    if(!rUIObject) return LuaTypeError("CarnUIVendor_SetProperty", L);

    ///--[Fluffs Dialogue]
    //--Clears existing dialogue packs.
    if(!strcasecmp(rSwitchType, "Clear Fluffs Dialogue") && tArgs >= 1)
    {
        rUIObject->ClearFluffsDialoguePacks();
    }
    //--Registers a new dialogue pack.
    else if(!strcasecmp(rSwitchType, "Register Fluffs Dialogue") && tArgs >= 3)
    {
        rUIObject->AddDialoguePack(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Proficiencies]
    //--Clears any lingering proficiency data.
    else if(!strcasecmp(rSwitchType, "Clear Proficiencies") && tArgs >= 1)
    {
        rUIObject->ClearProficiencies();
    }
    //--Registers a character for proficiency storage.
    else if(!strcasecmp(rSwitchType, "Register Proficiency Character") && tArgs >= 2)
    {
        rUIObject->RegisterProficiencyCharacter(lua_tostring(L, 2));
    }
    //--Sets proficiency data for a character.
    else if(!strcasecmp(rSwitchType, "Set Proficiency Value") && tArgs >= 4)
    {
        rUIObject->SetProficiencyFor(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnUIVendor_SetProperty", rSwitchType, tArgs);
    }

    //--Success.
    return 0;
}
