///===================================== Debug Definitions ========================================
//--This header contains #define values that will switch debug activities on and off inside the
//  program. These are different from the defined debug flags in the configuration, as it may
//  be desirable to hide things from people who have disassemblers and not allow them to be
//  re-enabled under any circumstances.
//--These are all POSITIVE definitions, so use #ifndef to check them.
#define ALLOW_DEBUG
#ifdef ALLOW_DEBUG

    #define DBG_ALLOW_CAMERA_ZOOMING
    #define DBG_MAP_PRECACHES_EVERYTHING
    #define DBG_MAP_CHECKS_SCALE
    #define DBG_ALLOW_CAMERA_UNLOCKING
    #define DBG_SHOW_MOUSE_WORLD_COORDS
    //#define DBG_SHOW_JOYPAD_DEPRESSION

#endif

//--Other debug flags. May not want to unset them all at once.
#define DBG_SHOW_FPS

//--Debug print options. To disable rendering, simply set the line to an out-of-range number,
//  such as a negative.
//#define DEBUG_LINE_CAMERA_XYZ 0
//#define DEBUG_LINE_CAMERA_PYR 1
#define DEBUG_LINES_TOTAL 0
