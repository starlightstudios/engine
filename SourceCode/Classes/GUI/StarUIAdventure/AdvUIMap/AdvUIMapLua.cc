//--Base
#include "AdvUIMap.h"

//--Classes
#include "AdventureMenu.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

///======================================== Lua Hooking ===========================================
//void AdvUIMap::HookToLuaState(lua_State *pLuaState)
//{
//}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AdvUIMap_SetProperty(bool &sHandled, lua_State *L)
{
    ///--[Documentation]
    //--Called from AdventureMenuLua's AM_SetProperty(). Handles routing to warp routines. If this routine
    //  caught the lua call, sHandled will be set to true.
    //--If there is an error, the base call will stop anyway. sHandled will be marked as true.
    sHandled = true;

    ///--[Argument List]
    //--[Map]
    //AM_SetProperty("Player Map Position", fX, fY)
    //AM_SetProperty("Player Sprite Flashes", bFlag)
    //AM_SetProperty("Map Edge Pad", iPadding)
    //AM_SetProperty("Clear Map Pins")
    //AM_SetProperty("Register Map Pin", fX, fY, sImagePath)

    //--[Advanced Map]
    //AM_SetProperty("Clear Advanced Map")
    //AM_SetProperty("Create Advanced Map Layer", sLayerName, sLayerPath)
    //AM_SetProperty("Layer Is Toggleable", sLayerName, bIsToggleable)
    //AM_SetProperty("Layer Render Chance", sLayerName, iRenderLo, iRenderHi)
    //AM_SetProperty("Layer Is Fixed", sLayerName, bIsFixed)
    //AM_SetProperty("Layer Renders Player", sLayerName, sImgPath, bFlashes, fXOffset, fYOffset)
    //AM_SetProperty("Layer Reads Stencil", sLayerName, iStencilValue)
    //AM_SetProperty("Layer Writes Stencil", sLayerName, iStencilValue)

    ///--[Arg Resolve]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Resolve Object]
    //--Dynamic. Get the AdventureMenu which holds the AdvUIMap object.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetProperty", L);

    //--Get the internal map UI.
    StarUIPiece *rUIPiece = rMenu->RetrieveUI("Map", POINTER_TYPE_STARUIPIECE, NULL, "AM_SetProperty");
    if(!rUIPiece) return LuaTypeError("AM_SetProperty", L);

    //--Cast.
    AdvUIMap *rMapUI = dynamic_cast<AdvUIMap *>(rUIPiece);

    ///--[Basic Map Handlers]
    //--Centering position of the player on the map, used when Advanced Maps don't use Set Map Info.
    if(!strcasecmp(rSwitchType, "Player Map Position") && tArgs == 3)
    {
        rMapUI->SetMapPlayerPos(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Map edge padding allowed when scrolling.
    else if(!strcasecmp(rSwitchType, "Map Edge Pad") && tArgs == 2)
    {
        rMapUI->SetMapEdgePad(lua_tointeger(L, 2));
    }
    //--Removes all map pins.
    else if(!strcasecmp(rSwitchType, "Clear Map Pins") && tArgs >= 1)
    {
        rMapUI->ClearMapPins();
    }
    //--Registers a new map pin at the given location with the given image.
    else if(!strcasecmp(rSwitchType, "Register Map Pin") && tArgs >= 4)
    {
        rMapUI->RegisterMapPin(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tostring(L, 4));
    }
    ///--[Advanced Map]
    //--Remapping properties for positioning the player cursor on the map.
    else if(!strcasecmp(rSwitchType, "Advanced Map Properties") && tArgs == 5)
    {
        rMapUI->SetAdvancedRemaps(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Clears all advanced map layers.
    else if(!strcasecmp(rSwitchType, "Clear Advanced Map") && tArgs == 1)
    {
        rMapUI->ClearAdvancedMapLayers();
    }
    //--Creates a new advanced map layer with the given name.
    else if(!strcasecmp(rSwitchType, "Create Advanced Map Layer") && tArgs == 3)
    {
        rMapUI->AddAdvancedLayer(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets if the layer toggles on or off when the player pushes a key.
    else if(!strcasecmp(rSwitchType, "Layer Is Toggleable") && tArgs == 3)
    {
        rMapUI->SetAdvancedLayerToggleable(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Set the odds, from 0 to 1000, of rendering this layer during any given tick.
    else if(!strcasecmp(rSwitchType, "Layer Render Chance") && tArgs == 4)
    {
        rMapUI->SetAdvancedLayerChance(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets if this layer does not pan or zoom.
    else if(!strcasecmp(rSwitchType, "Layer Is Fixed") && tArgs == 3)
    {
        rMapUI->SetAdvancedLayerIsFixed(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets if the layer shows the player. Includes optional path, flash flag, and offsets.
    else if(!strcasecmp(rSwitchType, "Layer Renders Player") && tArgs == 6)
    {
        rMapUI->SetAdvancedLayerPlayerRender(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Sets that the layer reads stencils when rendering.
    else if(!strcasecmp(rSwitchType, "Layer Reads Stencil") && tArgs == 3)
    {
        rMapUI->SetAdvancedLayerReadsStencil(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    //--Sets that the layer writes stencils when rendering.
    else if(!strcasecmp(rSwitchType, "Layer Writes Stencil") && tArgs == 3)
    {
        rMapUI->SetAdvancedLayerWritesStencil(lua_tostring(L, 2), lua_tointeger(L, 3));
    }
    ///--[Not Found]
    //--This routine did not catch the input, set the handled flag to false.
    else
    {
        sHandled = false;
    }

    ///--[Finish]
    return 0;
}
