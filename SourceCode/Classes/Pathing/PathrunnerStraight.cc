//--Base
#include "PathrunnerStraight.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers

///--[Local Definitions]
///--[Local Structures]
///--[Debug]

///========================================== System ==============================================
PathrunnerStraight::PathrunnerStraight()
{
    ///--[ ====== Variable Initialization ===== ]
    ///--[PathrunnerStraight]
    //--System
    mTurnPenalty = 99;

    ///--[Pathrunner]
    //--System
    //--Collision Storage
    //--Flags
    //--Path Runner
}
PathrunnerStraight::~PathrunnerStraight()
{
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void PathrunnerStraight::InsertPackageOnList(PathrunnerPack *pPackage, StarLinkedList *pList, float pStartDistance)
{
    ///--[Documentation]
    //--Heuristic optimizer. Places the given package on the linked list by implicitly sorting it relative to its distance to the
    //  destination (which has already been calculated). Thus, pulses that are getting closer to the target get processed first.
    //--This obviously has a problem when a cul-de-sac is hit, but most of the time this so massively speeds up operation
    //  that this heuristic is acceptable.
    //--If the distance value of the package is higher than the original starting distance, we start counting from the end of the
    //  list instead of the start. This speeds up list sorting, as it is assumed that a further away point will likely be less
    //  useful than the closer points from the word go.
    if(!pPackage || !pList) return;

    ///--[Sort from Front]
    if(pPackage->mPulseVal <= (int)pStartDistance)
    {
        //--Iterate.
        int i = 0;
        PathrunnerPack *rCheckPackage = (PathrunnerPack *)pList->PushIterator();
        while(rCheckPackage)
        {
            //--If the new package has a lower distance than the one being checked, insert there and be done with it.
            if(pPackage->mPulseVal < rCheckPackage->mPulseVal)
            {
                //--Pop iterator before placing.
                pList->PopIterator();

                //--Add it.
                pList->AddElementInSlot("X", pPackage, &FreeThis, i);
                return;
            }

            //--Next.
            i ++;
            rCheckPackage = (PathrunnerPack *)pList->AutoIterate();
        }

        //--If we got this far, every element on the list has a lower distance. Place this element on the end.
        pList->AddElementAsTail("X", pPackage, &FreeThis);
    }
    ///--[Sort from Back]
    else
    {
        //--Iterate.
        int i = pList->GetListSize()+1;
        PathrunnerPack *rCheckPackage = (PathrunnerPack *)pList->SetToTailAndReturn();
        while(rCheckPackage)
        {
            //--If the new package has a higher distance than the one being checked, insert there and be done with it.
            if(pPackage->mPulseVal >= rCheckPackage->mPulseVal)
            {
                //--Add it.
                pList->AddElementInSlot("X", pPackage, &FreeThis, i);
                return;
            }

            //--Next.
            i --;
            rCheckPackage = (PathrunnerPack *)pList->DecrementAndGetRandomPointerEntry();
        }

        //--If we got this far, every element on the list has a higher distance. Place this element on the front.
        pList->AddElementAsHead("X", pPackage, &FreeThis);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void PathrunnerStraight::RunIterator(int pPulseCap, bool pStopOnReachingTarget)
{
    ///--[Documentation]
    //--Runs the path iterator. The list contains all the unresolved pulses which are trying to place their
    //  values on the collision grid. While the elements are placed in such a manner that the closest ones
    //  get priority, it is very possible that the shortest possible path is not the first one arrived at.
    //--The algorithm runs until the pulse cap is reached, and then stops. Pass -1 to ignore the pulse cap
    //  and run until the algorithm is totally out of pulses.
    if(!mIsInitialized || mFailure) return;

    //--Allowable pulses. Decrements. When it reaches zero, iteration stops. If you pass a negative in, then
    //  the algorithm never reaches zero and won't stop until pulses are exhausted.
    int tAllowablePulses = pPulseCap;

    ///--[Iteration]
    PathrunnerPack *rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
    while(rIteratingPack)
    {
        ///--[Pulse Cap]
        //--If the allowable pulses value hits zero, stop iteration.
        if(tAllowablePulses == 0) break;
        tAllowablePulses --;

        ///--[Set Pulse]
        //--Remove the head pack.
        mPathRunnerList->SetRandomPointerToHead();
        mPathRunnerList->LiberateRandomPointerEntry();

        //--Check the pulse distance to make sure another pulse didn't slip in and place a shorter one here.
        if(mPathArray[rIteratingPack->mX][rIteratingPack->mY] < rIteratingPack->mPulseVal)
        {
            free(rIteratingPack);
            rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
            continue;
        }

        //--Mark the current tile's shortest distance to the start.
        mPulseTotal ++;
        mPathArray[rIteratingPack->mX][rIteratingPack->mY] = rIteratingPack->mPulseVal;

        ///--[Check Success]
        //--If this iterating pack hit the target, shift the target to match. This is done because the target
        //  is not necessarily grid-aligned to the start point. This grid-aligns it.
        //--If flagged, this also stops iteration because we reached the target.
        if(rIteratingPack->mDistToTarget <= mWalkDist)
        {
            //--Indicate at least one solution has been found.
            mHasAnyPath = true;

            //--Realign. Only needs to be done once.
            if(mAllowGridAligning)
            {
                mAllowGridAligning = false;
                mEndX = rIteratingPack->mX;
                mEndY = rIteratingPack->mY;
            }

            //--If flagged, stop iteration here.
            if(pStopOnReachingTarget)
            {
                break;
            }

            //--If we didn't stop, there is no need to spawn successors as we hit the target with this pulse.
            free(rIteratingPack);
            rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
            continue;
        }

        ///--[Spawn Successors]
        //--Check each of the cardinal directions if they can support a path move. This is done by
        //  a subroutine. If true, put the package on the end of the list.

        //--North:
        int tNorthPulse = rIteratingPack->mPulseVal + 1;
        if(rIteratingPack->mPulseVal > 0 && rIteratingPack->mPrevDir != DIR_UP) tNorthPulse += mTurnPenalty;
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, tNorthPulse, DIR_UP))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = tNorthPulse;
            nNewPackage->mX = rIteratingPack->mX + 0;
            nNewPackage->mY = rIteratingPack->mY - mWalkDist;
            nNewPackage->mPrevDir = DIR_UP;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            PathrunnerStraight::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        //--South:
        int tSouthPulse = rIteratingPack->mPulseVal + 1;
        if(rIteratingPack->mPulseVal > 0 && rIteratingPack->mPrevDir != DIR_DOWN) tSouthPulse += mTurnPenalty;
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, tSouthPulse, DIR_DOWN))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = tSouthPulse;
            nNewPackage->mX = rIteratingPack->mX + 0;
            nNewPackage->mY = rIteratingPack->mY + mWalkDist;
            nNewPackage->mPrevDir = DIR_DOWN;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        //--West:
        int tWestPulse = rIteratingPack->mPulseVal + 1;
        if(rIteratingPack->mPulseVal > 0 && rIteratingPack->mPrevDir != DIR_LEFT) tWestPulse += mTurnPenalty;
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, tWestPulse, DIR_LEFT))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = tWestPulse;
            nNewPackage->mX = rIteratingPack->mX - mWalkDist;
            nNewPackage->mY = rIteratingPack->mY + 0;
            nNewPackage->mPrevDir = DIR_LEFT;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            PathrunnerStraight::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        //--East:
        int tEastPulse = rIteratingPack->mPulseVal + 1;
        if(rIteratingPack->mPulseVal > 0 && rIteratingPack->mPrevDir != DIR_RIGHT) tEastPulse += mTurnPenalty;
        if(IsMoveValid(rIteratingPack->mX, rIteratingPack->mY, tEastPulse, DIR_RIGHT))
        {
            PathrunnerPack *nNewPackage = (PathrunnerPack *)starmemoryalloc(sizeof(PathrunnerPack));
            nNewPackage->mPulseVal = tEastPulse;
            nNewPackage->mX = rIteratingPack->mX + mWalkDist;
            nNewPackage->mY = rIteratingPack->mY + 0;
            nNewPackage->mPrevDir = DIR_RIGHT;
            nNewPackage->mDistToLine = GetDistanceToLine(mStartX, mStartY, mEndX, mEndY, nNewPackage->mX, nNewPackage->mY);
            nNewPackage->mDistToTarget = GetPlanarDistance(nNewPackage->mX, nNewPackage->mY, mEndX, mEndY);
            PathrunnerStraight::InsertPackageOnList(nNewPackage, mPathRunnerList, mOriginalDistance);
            if(mShortestDistance > nNewPackage->mDistToTarget)
            {
                mShortestDistance = nNewPackage->mDistToTarget;
            }
        }

        ///--[Next]
        //--Deallocate the old package.
        free(rIteratingPack);

        //--Get the new head.
        rIteratingPack = (PathrunnerPack *)mPathRunnerList->GetElementBySlot(0);
    }
}
StarLinkedList *PathrunnerStraight::ResolvePath()
{
    ///--[ =========== Documentation ========== ]
    //--Runs an algorithm from end to start, resolving the shortest possible path. This can be run at multiple
    //  times during iteration as the pulse information comes in.
    //--Returns a linked-list containing the movement instructions as int *'s. NULL on error. Caller is responsible
    //  for deallocating the list.
    if(mFailure || !mHasAnyPath) return NULL;

    //--Setup.
    StarLinkedList *nPathList = new StarLinkedList(true); //int *, master
    StarLinkedList *tTempMoveList = new StarLinkedList(true); //int *, master
    int tCurX = mEndX;
    int tCurY = mEndY;
    int tEndPathValue = mPathArray[mEndX][mEndY];

    ///--[ ============== Walking ============= ]
    //--Starting at the end position, "Walk" back to the start position. There should be an unbroken
    //  chain of values counting down to zero that denotes the shortest path.
    //--If multiple move cases exist, ties are broken at random. This gives some variation to the paths.
    //--If multiple move cases exist, but some are "longer" than others, the longer ones are trimmed.
    int tPreviousMove = -1;
    while(tEndPathValue >= 1)
    {
        ///--[Move Checking]
        //--Place all adjacent moves in the tTempMoveList.
        int tLowestMove = ADVP_UNUSED;
        tTempMoveList->ClearList();

        //--North:
        if(IsMoveShortest(tCurX, tCurY-mWalkDist, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX][tCurY-mWalkDist]) tLowestMove = mPathArray[tCurX][tCurY-mWalkDist];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_SOUTH;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }

        //--South:
        if(IsMoveShortest(tCurX, tCurY+mWalkDist, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX][tCurY+mWalkDist]) tLowestMove = mPathArray[tCurX][tCurY+mWalkDist];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_NORTH;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }

        //--West:
        if(IsMoveShortest(tCurX-mWalkDist, tCurY, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX-mWalkDist][tCurY]) tLowestMove = mPathArray[tCurX-mWalkDist][tCurY];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_EAST;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }

        //--East:
        if(IsMoveShortest(tCurX+mWalkDist, tCurY, tEndPathValue))
        {
            //--Store lowest move.
            if(tLowestMove > mPathArray[tCurX+mWalkDist][tCurY]) tLowestMove = mPathArray[tCurX+mWalkDist][tCurY];

            //--Add.
            int *nMove = (int *)starmemoryalloc(sizeof(int));
            *nMove = DIR_WEST;
            tTempMoveList->AddElement("X", nMove, &FreeThis);
        }

        ///--[Trimming]
        //--Error check. No moves available results in a failure.
        if(tTempMoveList->GetListSize() == 0)
        {
            break;
        }

        //--If there is more than one move available, and the previous move exists, if that move is available
        //  immediately pick it.
        if(tPreviousMove != -1)
        {
            //--Check if the matching move exists.
            int *rUsePtr = NULL;
            int *rMovePtr = (int *)tTempMoveList->SetToHeadAndReturn();
            while(rMovePtr)
            {
                //--Match. Use this one.
                if(*rMovePtr == tPreviousMove)
                {
                    rUsePtr = rMovePtr;
                    break;
                }

                //--Next.
                rMovePtr = (int *)tTempMoveList->IncrementAndGetRandomPointerEntry();
            }

            //--There was a usable element. Liberate it.
            if(rUsePtr)
            {
                //--Set the move direction.
                tPreviousMove = (*rUsePtr);

                //--Liberate that element.
                tTempMoveList->SetRandomPointerToThis(rUsePtr);
                tTempMoveList->LiberateRandomPointerEntry();

                //--Get the value at the target.
                //int tTargetVal = 0;
                //if(*rUsePtr == DIR_WEST)  tTargetVal = mPathArray[tCurX+mWalkDist][tCurY];
                //if(*rUsePtr == DIR_EAST)  tTargetVal = mPathArray[tCurX-mWalkDist][tCurY];
                //if(*rUsePtr == DIR_NORTH) tTargetVal = mPathArray[tCurX][tCurY+mWalkDist];
                //if(*rUsePtr == DIR_SOUTH) tTargetVal = mPathArray[tCurX][tCurY-mWalkDist];

                //--Register it on the return list.
                nPathList->AddElementAsHead("X", rUsePtr, &FreeThis);

                //--Next.
                tEndPathValue = tLowestMove;

                //--Given the direction, move the cursor.
                if(     *rUsePtr == DIR_NORTH) tCurY += mWalkDist;
                else if(*rUsePtr == DIR_SOUTH) tCurY -= mWalkDist;
                else if(*rUsePtr == DIR_WEST)  tCurX += mWalkDist;
                else if(*rUsePtr == DIR_EAST)  tCurX -= mWalkDist;
                continue;
            }
        }

        //--If there are more than one move available, trim all the ones that aren't the lowest move.
        if(tTempMoveList->GetListSize() > 1)
        {
            //--Iterate.
            int *rMovePtr = (int *)tTempMoveList->SetToHeadAndReturn();
            while(rMovePtr)
            {
                //--Get the value at the target.
                int tTargetVal = 0;
                if(*rMovePtr == DIR_WEST)  tTargetVal = mPathArray[tCurX+mWalkDist][tCurY];
                if(*rMovePtr == DIR_EAST)  tTargetVal = mPathArray[tCurX-mWalkDist][tCurY];
                if(*rMovePtr == DIR_NORTH) tTargetVal = mPathArray[tCurX][tCurY+mWalkDist];
                if(*rMovePtr == DIR_SOUTH) tTargetVal = mPathArray[tCurX][tCurY-mWalkDist];

                //--If the target value is not the lowest value, remove this.
                if(tTargetVal > tLowestMove) tTempMoveList->RemoveRandomPointerEntry();

                //--Next.
                rMovePtr = (int *)tTempMoveList->IncrementAndGetRandomPointerEntry();
            }
        }

        //--If the trimmer somehow removed all the elements, fail.
        if(tTempMoveList->GetListSize() == 0)
        {
            break;
        }

        ///--[Path Resolve]
        //--If there's more than one move, roll which one to use at random.
        int tUseSlot = rand() % tTempMoveList->GetListSize();

        //--Liberate that element.
        int *rMovePtr = (int *)tTempMoveList->GetElementBySlot(tUseSlot);
        tTempMoveList->SetRandomPointerToThis(rMovePtr);
        tTempMoveList->LiberateRandomPointerEntry();

        //--Set the move direction.
        tPreviousMove = (*rMovePtr);

        //--Register it on the return list.
        nPathList->AddElementAsHead("X", rMovePtr, &FreeThis);

        //--Next.
        tEndPathValue = tLowestMove;

        //--Given the direction, move the cursor.
        if(     *rMovePtr == DIR_NORTH) tCurY += mWalkDist;
        else if(*rMovePtr == DIR_SOUTH) tCurY -= mWalkDist;
        else if(*rMovePtr == DIR_WEST)  tCurX += mWalkDist;
        else if(*rMovePtr == DIR_EAST)  tCurX -= mWalkDist;
    }

    //--Clean.
    delete tTempMoveList;
    return nPathList;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
