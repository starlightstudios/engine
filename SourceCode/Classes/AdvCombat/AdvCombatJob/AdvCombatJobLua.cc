//--Base
#include "AdvCombatJob.h"

//--Classes
#include "AdvCombatAbility.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void AdvCombatJob::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatJob_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatJob_GetProperty", &Hook_AdvCombatJob_GetProperty);

    /* AdvCombatJob_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatJob_SetProperty", &Hook_AdvCombatJob_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AdvCombatJob_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatJob_GetProperty("Name") (1 String)
    //AdvCombatJob_GetProperty("Is Visible On Skills UI") (1 Boolean)
    //AdvCombatJob_GetProperty("Current JP") (1 Integer)
    //AdvCombatJob_GetProperty("Total Abilities") (1 Integer)
    //AdvCombatJob_GetProperty("Ability Name I", iSlot) (1 String)
    //AdvCombatJob_GetProperty("Is Ability Unlocked I", iSlot) (1 Boolean)
    //AdvCombatJob_GetProperty("Associated Profile Name") (1 String)
    //AdvCombatJob_GetProperty("Is Mastered") (1 Boolean)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatJob_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATJOB)) return LuaTypeError("AdvCombatJob_GetProperty", L);
    AdvCombatJob *rJob = (AdvCombatJob *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Job's internal name.
    if(!strcasecmp("Name", rSwitchType) && tArgs == 1)
    {
        lua_pushstring(L, rJob->GetInternalName());
        tReturns = 1;
    }
    //--True if the job appears on the skills UI, false if not.
    else if(!strcasecmp("Is Visible On Skills UI", rSwitchType) && tArgs == 1)
    {
        lua_pushboolean(L, rJob->AppearsOnSkillUI());
        tReturns = 1;
    }
    //--How much JP is available for spending.
    else if(!strcasecmp("Current JP", rSwitchType) && tArgs == 1)
    {
        lua_pushinteger(L, rJob->GetCurrentJP());
        tReturns = 1;
    }
    //--How many abilities this job has. Includes internal and external abilities.
    else if(!strcasecmp("Total Abilities", rSwitchType) && tArgs == 1)
    {
        lua_pushinteger(L, rJob->GetAbilityList()->GetListSize());
        tReturns = 1;
    }
    //--Ability name in the given slot.
    else if(!strcasecmp("Ability Name I", rSwitchType) && tArgs == 2)
    {
        AdvCombatAbility *rAbility = rJob->GetAbility(lua_tointeger(L, 2));
        if(rAbility)
            lua_pushstring(L, rAbility->GetInternalName());
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    //--Boolean indicating if the ability is unlocked.
    else if(!strcasecmp("Is Ability Unlocked I", rSwitchType) && tArgs == 2)
    {
        AdvCombatAbility *rAbility = rJob->GetAbility(lua_tointeger(L, 2));
        if(rAbility)
            lua_pushboolean(L, rAbility->IsUnlocked());
        else
            lua_pushboolean(L, false);
        tReturns = 1;
    }
    //--Associated ability profile.
    else if(!strcasecmp("Associated Profile Name", rSwitchType) && tArgs == 1)
    {
        lua_pushstring(L, rJob->GetAssociatedProfileName());
        tReturns = 1;
    }
    //--Returns true if the job is mastered, false if not.
    else if(!strcasecmp("Is Mastered", rSwitchType) && tArgs == 1)
    {
        lua_pushboolean(L, rJob->IsMastered());
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatJob_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatJob_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatJob_SetProperty("Display Name", sPath)
    //AdvCombatJob_SetProperty("Script Path", sPath)
    //AdvCombatJob_SetProperty("Script Response", iIndex, bFlag)
    //AdvCombatJob_SetProperty("Is Always Mastered", bFlag)
    //AdvCombatJob_SetProperty("Push Owner") (Pushes Active Entity)
    //AdvCombatJob_SetProperty("Fire Script", iCode)
    //AdvCombatJob_SetProperty("Appears on Skills UI", bFlag)
    //AdvCombatJob_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, sDLPath)
    //AdvCombatJob_SetProperty("Associated Profile Name", sName)

    //--[Storage]
    //AdvCombatJob_SetProperty("JP Available", iAmount)

    //--[Abilities]
    //AdvCombatJob_SetProperty("Ability Unlocked", sAbilityName, bIsUnlocked)
    //AdvCombatJob_SetProperty("Move Ability Up One Slot", sAbilityName)

    //--[Tags]
    //AdvCombatJob_SetProperty("Add Tag", sTagName, iCount)
    //AdvCombatJob_SetProperty("Remove Tag", sTagName, iCount)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatJob_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATJOB)) return LuaTypeError("AdvCombatJob_SetProperty", L);
    AdvCombatJob *rJob = (AdvCombatJob *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Sets the name that appears in the UI.
    if(!strcasecmp("Display Name", rSwitchType) && tArgs == 2)
    {
        rJob->SetDisplayName(lua_tostring(L, 2));
    }
    //--Sets the path this job calls whenever it needs a script call.
    else if(!strcasecmp("Script Path", rSwitchType) && tArgs == 2)
    {
        rJob->SetScriptPath(lua_tostring(L, 2));
    }
    //--Sets whether the job responds to a given code.
    else if(!strcasecmp("Script Response", rSwitchType) && tArgs == 3)
    {
        rJob->SetScriptResponse(lua_tointeger(L, 2), lua_toboolean(L, 3));
    }
    //--If true, the job is always mastered regardless of its abilities.
    else if(!strcasecmp("Is Always Mastered", rSwitchType) && tArgs == 2)
    {
        rJob->SetIsAlwaysMastered(lua_toboolean(L, 2));
    }
    //--Pushes owner of Job.
    else if(!strcasecmp(rSwitchType, "Push Owner") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rJob->GetOwner());
    }
    //--Fires the job's code.
    else if(!strcasecmp(rSwitchType, "Fire Script") && tArgs == 2)
    {
        rJob->CallScript(lua_tointeger(L, 2));
    }
    //--Sets if this job lists its skills on the skills UI for purchase.
    else if(!strcasecmp(rSwitchType, "Appears on Skills UI") && tArgs == 2)
    {
        rJob->SetAppearsOnSkillUI(lua_toboolean(L, 2));
    }
    //--Set dimensions of the face table used on the Profiles UI.
    else if(!strcasecmp(rSwitchType, "Face Table Data") && tArgs == 6)
    {
        rJob->SetFaceTableData(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tostring(L, 6));
    }
    //--Profile this job uses.
    else if(!strcasecmp(rSwitchType, "Associated Profile Name") && tArgs == 2)
    {
        rJob->SetAssociatedProfileName(lua_tostring(L, 2));
    }
    ///--[Storage]
    //--Amount of JP available right now.
    else if(!strcasecmp(rSwitchType, "JP Available") && tArgs == 2)
    {
        rJob->SetCurrentJP(lua_tointeger(L, 2));
    }
    ///--[Abilities]
    //--Unlocks or seals the given ability.
    else if(!strcasecmp(rSwitchType, "Ability Unlocked") && tArgs == 3)
    {
        StarLinkedList *rAbilityList = rJob->GetAbilityList();
        AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementByName(lua_tostring(L, 2));
        if(rAbility) rAbility->SetUnlocked(lua_toboolean(L, 3));
        rJob->RecheckMastery();
    }
    //--Moves the named ability up one slot in its list.
    else if(!strcasecmp(rSwitchType, "Move Ability Up One Slot") && tArgs == 2)
    {
        StarLinkedList *rAbilityList = rJob->GetAbilityList();
        rAbilityList->MoveEntryTowardsHeadS(lua_tostring(L, 2));
    }
    ///--[Tags]
    //--Adds a tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Add Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rJob->AddTag(lua_tostring(L, 2));
    }
    //--Removes a tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Remove Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rJob->RemoveTag(lua_tostring(L, 2));
    }
    ///--[Error case]
    else
    {
        LuaPropertyError("AdvCombatJob_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
