//--Base
#include "TextLevel.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

void TextLevel::ActivateOverlayMode()
{
    mIsHarshOverlayMode = true;
}
void TextLevel::DeactivateOverlayMode()
{
    mIsHarshOverlayMode = false;
}
void TextLevel::ClearOverlays()
{
    mHarshOverlayList->ClearList();
}
void TextLevel::RegisterOverlay(const char *pName, const char *pImage, int pFlags, float pStartX, float pStartY, float pXScroll, float pYScroll, float pXClamp, float pYClamp)
{
    if(!pName || !pImage || pXClamp == 0.0f || pYClamp == 0.0f) return;
    SetMemoryData(__FILE__, __LINE__);
    HarshOverlayPack *nPackage = (HarshOverlayPack *)starmemoryalloc(sizeof(HarshOverlayPack));
    nPackage->Initialize();
    mHarshOverlayList->AddElementAsTail(pName, nPackage, &FreeThis);
    nPackage->mTimer = 0;
    nPackage->mOverlayFlags = pFlags;
    nPackage->mScrollX = pStartX;
    nPackage->mScrollY = pStartY;
    nPackage->mScrollXSpeed = pXScroll;
    nPackage->mScrollYSpeed = pYScroll;
    nPackage->mScrollClampX = pXClamp;
    nPackage->mScrollClampY = pYClamp;
    nPackage->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImage);
}
void TextLevel::ModifyOverlay(const char *pName, const char *pImage)
{
    if(!pName || !pImage) return;
    HarshOverlayPack *rPackage = (HarshOverlayPack *)mHarshOverlayList->GetElementByName(pName);
    if(!rPackage) return;
    rPackage->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImage);
}
void TextLevel::UpdateOverlays()
{
    //--Timer.
    if(mIsHarshOverlayMode)
    {
        if(mHarshOverlayTimer < TL_OVERLAY_TICKS) mHarshOverlayTimer ++;
    }
    else
    {
        if(mHarshOverlayTimer > 0) mHarshOverlayTimer --;
        if(mHarshOverlayTimer == 0) return;
    }

    //--Update the overlay timers.
    HarshOverlayPack *rPackage = (HarshOverlayPack *)mHarshOverlayList->PushIterator();
    while(rPackage)
    {
        //--Increment.
        rPackage->mScrollX = rPackage->mScrollX + rPackage->mScrollXSpeed;
        rPackage->mScrollY = rPackage->mScrollY + rPackage->mScrollYSpeed;

        //--Lower clamp:
        while(rPackage->mScrollX < rPackage->mScrollClampX * -1.0f)
        {
            rPackage->mScrollX = rPackage->mScrollX + rPackage->mScrollClampX;
        }
        while(rPackage->mScrollY < rPackage->mScrollClampY * -1.0f)
        {
            rPackage->mScrollY = rPackage->mScrollY + rPackage->mScrollClampY;
        }

        //--Upper clamp:
        while(rPackage->mScrollX > rPackage->mScrollClampX)
        {
            rPackage->mScrollX = rPackage->mScrollX - rPackage->mScrollClampX;
        }
        while(rPackage->mScrollY > rPackage->mScrollClampY)
        {
            rPackage->mScrollY = rPackage->mScrollY - rPackage->mScrollClampY;
        }

        //--Next.
        rPackage = (HarshOverlayPack *)mHarshOverlayList->AutoIterate();
    }
}
