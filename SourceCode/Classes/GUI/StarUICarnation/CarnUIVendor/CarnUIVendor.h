///======================================= CarnUIVendor ===========================================
//--Vendor UI for Project Carnation. This is a shop run by Fluffs, and possibly other shopkeepers.
//  Allows the player to buy and sell items.

#pragma once

///========================================= Includes =============================================
#include "AdvUIVendor.h"
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
typedef struct ProficiencyPack
{
    //--In order: Sword, Spear, Hammer, Bow, Staff, Wand, Whip, Focus.
    int mLookups[8];
}ProficiencyPack;

typedef struct ProficiencyRenderPack
{
    //--Members.
    float mLft;
    float mRgt;
    float mY;
    char mRenderType[32];
    char mCheckTag[32];
    int mLookupSlot;

    //--Functions
    void Set(float pLft, float pRgt, float pY, const char *pRenderType, const char *pCheckTag, int pLookups)
    {
        mLft = pLft;
        mRgt = pRgt;
        mY = pY;
        strncpy(mRenderType, pRenderType, 31);
        strncpy(mCheckTag, pCheckTag, 31);
        mLookupSlot = pLookups;
    }
}ProficiencyRenderPack;

///===================================== Local Definitions ========================================
#define CARNATION_STAT_HP 0
#define CARNATION_STAT_SP 1
#define CARNATION_STAT_MAGICATTACK 3
#define CARNATION_STAT_ENDURANCE 4
#define CARNATION_STAT_INTELLECT 5
#define CARNATION_STAT_ATTACK 6
#define CARNATION_STAT_INITIATIVE 7
#define CARNATION_STAT_MAGICDEFENSE 8
#define CARNATION_STAT_LUCK 9
#define CARNATION_STAT_DEFENSE 10
#define CARNATION_STAT_SPEED 18

#define VARNUIVEND_STAT_TITLES_TOTAL 8

///========================================== Classes =============================================
class CarnUIVendor : public CarnUICommon, virtual public AdvUIVendor
{
    protected:
    ///--[Constants]
    //--Scrollbar.
    static const int cxCarnScrollBuf = 3;
    static const int cxCarnScrollPageSize = 13;

    //--Timers
    static const int cxFluffsBurstMax     =   10;
    static const int cxFluffsTextTicksMax =  600;
    static const int cxFluffsThinkTextMin = 1250;
    static const int cxFluffsThinkTextMax = 1800;

    //--Sizes
    static constexpr float cxFluffsBurstDist = 4.0f;

    //--Hitboxes.
    static const int cxHitbox_ItemsLo  =  0;
    static const int cxHitbox_ItemsHi  = 12;
    static const int cxHitbox_Buy      = 13;
    static const int cxHitbox_Sell     = 14;
    static const int cxHitbox_CharLo   = 15;
    static const int cxHitbox_CharHi   = 17;
    static const int cxHitbox_ScrollUp = 18;
    static const int cxHitbox_ScrollBd = 19;
    static const int cxHitbox_ScrollDn = 20;
    static const int cxHitbox_Confirm  = 21;
    static const int cxHitbox_Cancel   = 22;
    static const int cxHitbox_Exit     = 23;

    ///--[System]
    bool mRecomputeScrollbarSize;

    ///--[Description]
    StarLinkedList *mDescriptionList;//char *, master
    StarLinkedList *mProficiencyRenderList;//ProficiencyRenderPack *, master

    ///--[Fluffs]
    //--Constants.
    static const int cxFluffsNeutral = 0;
    static const int cxFluffsHappy = 1;
    static const int cxFluffsBored = 2;
    static const int cxFluffsUpset = 3;

    //--Variables.
    int mFluffsWaitTimer;
    int mFluffsThinkTextTimer;
    int mFluffsBurstTimer;
    int mFluffsSpeechTimeRemaining;
    int mFluffsFrame;
    StarLinkedList *mFluffsDialogue; //char *, master

    //--Random Dialogue.
    StarLinkedList *mFluffsGreetingPacks; //char *, master
    StarLinkedList *mFluffsPurchasePacks; //char *, master
    StarLinkedList *mFluffsSellPacks; //char *, master
    StarLinkedList *mFluffsCantAffordPacks; //char *, master
    StarLinkedList *mFluffsThinkingPacks; //char *, master
    StarLinkedList *mFluffsCantSellPacks; //char *, master

    ///--[Stat Indices]
    int mStatIndexes[VARNUIVEND_STAT_TITLES_TOTAL];
    char mStatTitles[VARNUIVEND_STAT_TITLES_TOTAL][25];
    StarLinkedList *mCharacterProficiencies; //ProficiencyPack *, master

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_BuySell;
        StarFont *rFont_Hexes;
        StarFont *rFont_Name;
        StarFont *rFont_Description;
        StarFont *rFont_Fluffs;
        StarFont *rFont_Property;

        //--Images
        StarBitmap *rBackground_Clouds;
        StarBitmap *rBackground_Covers;
        StarBitmap *rBackground_Desk;
        StarBitmap *rBackground_Fluffs;
        StarBitmap *rBackground_Shelf;
        StarBitmap *rFrame_BoxBot;
        StarBitmap *rFrame_BoxRgt;
        StarBitmap *rFrame_Button;
        StarBitmap *rFrame_Confirm;
        StarBitmap *rFrame_Hexes;
        StarBitmap *rOverlay_BoxSelected;
        StarBitmap *rOverlay_BoxUnselected;
        StarBitmap *rOverlay_Cursor;
        StarBitmap *rOverlay_ItemsLft;
        StarBitmap *rOverlay_ItemsRgt;
        StarBitmap *rOverlay_FluffsBored;
        StarBitmap *rOverlay_FluffsHappy;
        StarBitmap *rOverlay_FluffsNeutral;
        StarBitmap *rOverlay_FluffsSpeech;
        StarBitmap *rOverlay_FluffsUpset;
        StarBitmap *rOverlay_PartyPortraitBox;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rScrollbar_Static;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIVendor();
    virtual ~CarnUIVendor();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    const char *GetProficiencyStringFromCode(int pCode);

    //--Manipulators
    virtual void TakeForeground();
    void SetFluffsMood(int pEmotionFlag, const char *pDialogue, bool pDisallowBounce);
    void SetFluffsMoodByString(const char *pDialogue, bool pDisallowBounce);
    void ClearFluffsDialoguePacks();
    void AddDialoguePack(const char *pType, const char *pString);
    void RollQuote(const char *pType, int pDelay, bool pDisallowBounce);
    void ClearProficiencies();
    void RegisterProficiencyCharacter(const char *pName);
    void SetProficiencyFor(const char *pName, const char *pType, int pValue);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    virtual void ScrollMode(int pAmount);
    virtual int CheckEquippableFromPurchase(AdventureItem *pItem);
    void RebuildDescriptionList(AdventureItem *pItem);
    virtual void SetComparisonCharacter(int pPartySlot);
    virtual void EquipPurchasedItem();
    virtual void RecheckComparisonCharacter(AdventureItem *pItem);

    ///--[ ===== Mode Handlers ====== ]
    ///--[Mode: Buy]
    virtual void UpdateModeBuy();
    virtual void UpdateBuyItem();
    virtual void UpdateBuyEquipItem();
    virtual void RenderLftBuy(float pVisAlpha);
    virtual void RenderTopBuy(float pVisAlpha);
    virtual void RenderRgtBuy(float pVisAlpha);
    virtual void RenderBotBuy(float pVisAlpha);
    virtual void RenderUnalignedBuy(float pVisAlpha);
    virtual void RenderItemPack(ShopInventoryPack *pPack, int pSlot, bool pIsSelected, float pColorAlpha);
    virtual void RenderBuyItem(float pVisAlpha);
    virtual void RenderItem(AdventureItem *pItem, int pSlot, bool pIsOdd, bool pIsSellPrice, float pColorAlpha);
    virtual void RenderBuyEquipItem(float pVisAlpha);

    ///--[Mode: Sell]
    virtual void UpdateModeSell();
    virtual void UpdateSellItem();
    virtual void RenderLftSell(float pVisAlpha);
    virtual void RenderTopSell(float pVisAlpha);
    virtual void RenderRgtSell(float pVisAlpha);
    virtual void RenderBotSell(float pVisAlpha);
    virtual void RenderUnalignedSell(float pVisAlpha);
    virtual void RenderSellItem(float pVisAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderForeground(float pVisAlpha);
    virtual void RenderComparisonPane(AdventureItem *pComparisonItem, float pAlpha);
    void RenderFluffs();

    //--Pointer Routing
    StarLinkedList *GetCharacterProficiencyList();
    StarLinkedList *GetProficiencyRenderList();

    //--Static Functions
    static CarnUIVendor *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CarnUIVendor_SetProperty(lua_State *L);


