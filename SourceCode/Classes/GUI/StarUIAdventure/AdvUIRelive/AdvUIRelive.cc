//--Base
#include "AdvUIRelive.h"

//--Classes
#include "AdvUIBase.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

///========================================== System ==============================================
AdvUIRelive::AdvUIRelive()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "ARLV");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = (VIRTUAL_CANVAS_X * 0.50f);
    mGridRenderCenterY  = (VIRTUAL_CANVAS_Y * 0.35f) + 50.0f;
    mGridRenderSpacingX = (VIRTUAL_CANVAS_X * 0.10f);
    mGridRenderSpacingY = (VIRTUAL_CANVAS_X * 0.10f);

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ====== AdvUIRelive ======= ]
    ///--[Variables]
    //--Data.
    mReliveData = new StarLinkedList(true);

    //--Cursor
    mDontShowCursor = false;
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Strings
    mShowHelpString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Help Strings.
    AllocateHelpStrings(cxAdvReliveHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIRelive::~AdvUIRelive()
{
    delete mReliveData;
    delete mShowHelpString;
}
void AdvUIRelive::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Relive UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = AdvImages.rFont_Mainline;
}

///--[Public Variables]
//--Script that fires when the menu initializes. Allows the player to relive certain cutscenes.
char *AdvUIRelive::xReliveResolveScript = NULL;

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUIRelive::TakeForeground()
{
    ///--[Documentation]
    //--Sets to relive mode. If no relive scripts are found plays a fail tone.
    mReliveData->ClearList();
    if(!xReliveResolveScript) return;

    //--Run the relive script.
    LuaManager::Fetch()->ExecuteLuaFile(xReliveResolveScript);
    if(mReliveData->GetListSize() < 1)
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    ///--[Flags]
    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|Select");

    ///--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    AutoAllocateSquareGrid(mReliveData->GetListSize());
    ProcessGrid();

    //--Iteration loop.
    int i = 0;
    RelivePackage *rPackage = (RelivePackage *)mReliveData->PushIterator();
    while(rPackage)
    {
        //--Retrieve a grid package. If it's NULL then we ran out of space, bark an error.
        GridPackage *rGridPack = GetNextPriorityEntry();
        if(!rGridPack)
        {
            fprintf(stderr, "AdvUIRelive:TakeForeground() - Warning, heading data has %i entries but the grid ran out of slots. Failing.\n", mReliveData->GetListSize());
            mReliveData->PopIterator();
            break;
        }

        //--Copy across.
        rGridPack->mCode = i;
        strcpy(rGridPack->mLocalName, mReliveData->GetIteratorName());
        memcpy(&rGridPack->mAlignments, &rPackage->mAlignments, sizeof(AdvMenuStandardAlignments));

        //--Next.
        i ++;
        rPackage = (RelivePackage *)mReliveData->AutoIterate();
    }

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    ///--[Run Cursor]
    //--Update the cursor so it focuses on the selected entry.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}
void AdvUIRelive::RegisterReliveScript(const char *pDisplayName, const char *pScriptPath, const char *pImagePath)
{
    //--Error check.
    if(!pDisplayName || !pScriptPath) return;

    //--Allocate a new relive package.
    SetMemoryData(__FILE__, __LINE__);
    RelivePackage *nPackage = (RelivePackage *)starmemoryalloc(sizeof(RelivePackage));
    nPackage->Initialize();
    ResetString(nPackage->mScriptPath, pScriptPath);
    nPackage->mAlignments.rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);

    //--If the image exists, make sure its data is loaded.
    if(nPackage->mAlignments.rImage) nPackage->mAlignments.rImage->LoadDataFromSLF();

    //--Register.
    mReliveData->AddElement(pDisplayName, nPackage, &RelivePackage::DeleteThis);
}
void AdvUIRelive::SetReliveAlignments(const char *pDisplayName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei)
{
    //--Error check.
    if(!pDisplayName) return;

    //--Get the package.
    RelivePackage *rPackage = (RelivePackage *)mReliveData->GetElementByName(pDisplayName);
    if(!rPackage || !rPackage->mAlignments.rImage) return;

    //--Set.
    rPackage->mAlignments.mOffsetX = pXOff;
    rPackage->mAlignments.mOffsetY = pYOff;
    rPackage->mAlignments.mImageX  = pLft - rPackage->mAlignments.rImage->GetXOffset();
    rPackage->mAlignments.mImageY  = pTop - rPackage->mAlignments.rImage->GetYOffset();
    rPackage->mAlignments.mImageW  = pWid;
    rPackage->mAlignments.mImageH  = pHei;

    //--Clamp.
    if(rPackage->mAlignments.mImageW < 1.0f) rPackage->mAlignments.mImageW = 1.0f;
    if(rPackage->mAlignments.mImageH < 1.0f) rPackage->mAlignments.mImageH = 1.0f;
}

///======================================= Core Methods ===========================================
void AdvUIRelive::RefreshMenuHelp()
{
    /*
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Set lines.
    mHelpMenuStrings[i]->SetString("This menu allows you to select a scene you have viewed and view it again.");
    i ++;
    mHelpMenuStrings[i]->SetString("Nobody is going to ask why. Clearly you needed a story refresher.");
    i ++;
    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("Some scenes may also be available that you have not seen. This is");
    i ++;
    mHelpMenuStrings[i]->SetString("because those scenes are now unattainable with the current game state,");
    i ++;
    mHelpMenuStrings[i]->SetString("often because they are exclusive with another scene. They are here");
    i ++;
    mHelpMenuStrings[i]->SetString("for your convenience.");
    i ++;
    mHelpMenuStrings[i]->SetString("");
    i ++;

    mHelpMenuStrings[i]->SetString("Press [IMG0] or [IMG1] to exit this help menu.");
    mHelpMenuStrings[i]->AllocateImages(2);
    mHelpMenuStrings[i]->SetImageP(0, ADVMENU_BIG_CONTROL_OFFSET_Y, rControlManager->ResolveControlImage("F1"));
    mHelpMenuStrings[i]->SetImageP(1, ADVMENU_BIG_CONTROL_OFFSET_Y, rControlManager->ResolveControlImage("Cancel"));
    i ++;

    //--Error check.
    if(i > ADVMENU_RELIVE_HELP_MENU_STRINGS)
    {
        fprintf(stderr, "Warning: Relive Help Menu has too many strings %i vs %i\n", i, ADVMENU_RELIVE_HELP_MENU_STRINGS);
    }

    //--Order all strings to crossreference images.
    for(int i = 0; i < ADVMENU_RELIVE_HELP_MENU_STRINGS; i ++)
    {
        mHelpMenuStrings[i]->CrossreferenceImages();
    }
    */
}
void AdvUIRelive::RecomputeCursorPositions()
{
    RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, 1.0f);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIRelive::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        mVisibilityTimer ++;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--Standard Timers.
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks, mIsShowingHelp);

    //--Grid timers.
    UpdateGridTimers();

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Set this flag.
    mDontShowCursor = false;

    ///--[ ======== Update Intercepts ========= ]
    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[ =========== Update Call ============ ]
    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return true;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get the relive entry in question.
        int tCurrentCode = GetGridCodeAtCursor();
        RelivePackage *rRelivePackage = (RelivePackage *)mReliveData->GetElementBySlot(tCurrentCode);
        if(!rRelivePackage) return true;

        //--Execute the standard file to warp there.
        LuaManager::Fetch()->ExecuteLuaFile(rRelivePackage->mScriptPath, 1, "S", mReliveData->GetNameOfElementBySlot(tCurrentCode));

        //--Close this menu, exits immediately so a cutscene can fire.
        FlagExit();
        SetCodeBackward(AM_MODE_EXITNOW);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update.
    return true;
}
void AdvUIRelive::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;

    //--Run timers.
    mVisibilityTimer --;
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Unset this flag.
    mDontShowCursor = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIRelive::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Rendering of the base UI. Mostly follows the standard, however the bottom of the UI that shows
    //  the player's party will continue to render even if this object is offscreen if the Doctor UI
    //  needs it to, which is handled via a static timer.
    StarlightColor::ClearMixer();

    ///--[Timer]
    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Render Grid]
    //--Render the grid normally.
    RenderGrid(pVisAlpha);
    if(!mDontShowCursor) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);
    RenderGridEntryName(GetPackageAtCursor(), pVisAlpha);

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUIRelive::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the header at the top of the screen.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Costume Menu");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, 0, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    AutoPieceClose();
}
void AdvUIRelive::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Execution]
    //--Footer.
    AdvImages.rOverlay_Footer->Draw();

    ///--[Clean]
    glDisable(GL_STENCIL_TEST);
    AutoPieceClose();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
