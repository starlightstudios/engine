//--Base
#include "TilemapActor.h"

//--Classes
#include "TileLayer.h"

//--CoreClasses
//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"

bool TilemapActor::HandleMoveTo(int pX, int pY, float pSpeed)
{
    ///--[Documentation]
    //--Used by the MoveTo instruction, attempts to move this Actor towards the provided location. This is done
    //  using the same movement logic as player control handling. Actors do not move radially.
    //--The provided speed can be negative, in which case the actor will use its standard move speed.
    //--If the Actor gets stuck on something, they will stop moving and finish the move without reaching the target.
    //--Returns true when the TilemapActor is at the given location.
    if(pSpeed <= 0.0f) pSpeed = mMoveSpeed;
    SetRunning(false);

    //--If immoble, stop immediately.
    if(mIsImmobile) return true;

    //--Store the keys.
    bool tIsLftPressed = (pX < mTrueX);
    bool tIsRgtPressed = (pX > mTrueX);
    bool tIsTopPressed = (pY < mTrueY);
    bool tIsBotPressed = (pY > mTrueY);

    //--Auto-completion if no keys were pressed.
    if(!tIsLftPressed && !tIsRgtPressed && !tIsTopPressed && !tIsBotPressed)
    {
        return true;
    }

    //--Store our collision flag. We unclip ourselves when moving so we don't bump into... us...
    bool tStoredClipFlag = mIsClipped;
    mIsClipped = false;

    //--Case checking.
    bool tIsStopped = true;
    if(tIsLftPressed && !IsLftClipped()) tIsStopped = false;
    if(tIsTopPressed && !IsTopClipped()) tIsStopped = false;
    if(tIsRgtPressed && !IsRgtClipped()) tIsStopped = false;
    if(tIsBotPressed && !IsBotClipped()) tIsStopped = false;
    if(tIsStopped)
    {
        mIsClipped = tStoredClipFlag;
        return true;
    }

    //--Check the distance. If less than the move speed, instantly move there and end.
    float tDistance = GetPlanarDistance(mTrueX, mTrueY, pX, pY);
    if(tDistance <= pSpeed)
    {
        mTrueX = pX;
        mTrueY = pY;
        mIsClipped = tStoredClipFlag;
        return true;
    }

    //--Store the movement value.
    int tMoveTimer = mMoveTimer;

    //--Otherwise, run the movement routines.
    float tStoredSpeed = mMoveSpeed;
    mMoveSpeed = pSpeed;
    EmulateMovement(tIsLftPressed, tIsTopPressed, tIsRgtPressed, tIsBotPressed);
    mMoveSpeed = tStoredSpeed;

    //--Sets running. This only affects animation, since we will not be storing the mRanLastTick flag after EmulateMovement() is called.
    SetRunning(pSpeed >= 1.50f);

    //--Don't let the character overshoot the target.
    if((tIsLftPressed && mTrueX < pX) || (tIsRgtPressed && mTrueX > pX))
    {
        mTrueX = pX;
    }
    if((tIsTopPressed && mTrueY < pY) || (tIsBotPressed && mTrueY > pY))
    {
        mTrueY = pY;
    }

    //--If the move timer didn't increment, manually increment it. This is only used when the move speed is lower than
    //  the normal since NPCs won't animate correctly.
    if(pSpeed < mMoveSpeed && mMoveTimer <= tMoveTimer)
    {
        mIsMoving = true;
        mMoveTimer = tMoveTimer + 1;
    }

    //--Return whether or not we hit the target. This can still happen even if the distance case failed,
    //  though it's rare.
    mIsClipped = tStoredClipFlag;
    return (mTrueX == pX && mTrueY == pY);
}
bool TilemapActor::HandleMoveAmount(float &sX, float &sY)
{
    //--Used by the MoveFixed instruction, moves the Actor a fixed distance. Unlike HandleMoveTo(), this function
    //  modifies the caller values since we can only move a certain distance at a time.
    //--We emulate the keypresses here. Note that, with some amounts of movement values, the character may overshoot
    //  the target. Scripts should calibrate for this: If you need precise movements, use the MoveTo instruction.
    SetRunning(false);

    //--Store the keys.
    bool tIsLftPressed = (sX < 0.0f);
    bool tIsRgtPressed = (sX > 0.0f);
    bool tIsTopPressed = (sY < 0.0f);
    bool tIsBotPressed = (sY > 0.0f);

    //--Auto-completion if no keys were pressed.
    if(!tIsLftPressed && !tIsRgtPressed && !tIsTopPressed && !tIsBotPressed)
    {
        return true;
    }

    //--Store where we started.
    float tStartX = mTrueX;
    float tStartY = mTrueY;

    //--Store our collision flag. We shouldn't stop our own movement!
    bool tStoredClipFlag = mIsClipped;
    mIsClipped = false;

    //--Otherwise, run the movement routines.
    EmulateMovement(tIsLftPressed, tIsTopPressed, tIsRgtPressed, tIsBotPressed);

    //--Compute how much we moved.
    float tMovedX = mTrueX - tStartX;
    float tMovedY = mTrueY - tStartY;

    //--If both moved values are zero, the move got stuck.
    if(tMovedX == 0.0 && tMovedY == 0.0)
    {
        mIsClipped = tStoredClipFlag;
        return true;
    }

    //--Modify the calling variables here.
    if(sX > 0.0f)
    {
        sX = sX - tMovedX;
        if(sX < 0.0f)
        {
            sX = 0.0f;
            mRemainderX = 0.0f;
        }
    }
    else if(sX < 0.0f)
    {
        sX = sX - tMovedX;
        if(sX > 0.0f)
        {
            sX = 0.0f;
            mRemainderX = 0.0f;
        }
    }
    if(sY > 0.0f)
    {
        sY = sY - tMovedY;
        if(sY < 0.0f)
        {
            sY = 0.0f;
            mRemainderY = 0.0f;
        }
    }
    else if(sY < 0.0f)
    {
        sY = sY - tMovedY;
        if(sY > 0.0f)
        {
            sY = 0.0f;
            mRemainderY = 0.0f;
        }
    }

    //--Return false here. Movement is still taking place.
    mIsClipped = tStoredClipFlag;
    return false;
}
bool TilemapActor::HandleJumpTo(int pStartX, int pStartY, int pEndX, int pEndY, int pTimer, int pTimerMax)
{
    ///--[Documentation]
    //--Handles "Jumping" to the target point. This ignores collisions and uses the Z offset to make the movement
    //  look natural. It is actually closer to the use of an easing function.
    //--Returns true when at the destination, false if not there yet. This can also be seen from the input timer
    //  which is effectively a percentage of completion.
    if(pTimerMax < 1) return true;

    //--Setup.
    float cPercent = EasingFunction::Linear(pTimer, pTimerMax);

    ///--[Vertical Handling]
    //--Determine the Z position by a simple sin calculation.
    float cMaxHeight = -14.0f;

    //--If the start and end positions are very close to one another, the height decreases.
    float tDistance = GetPlanarDistance(pStartX, pStartY, pEndX, pEndY);
    if(tDistance < 16.0f)
    {
        float cHeightPercent = tDistance / 16.0f;
        cMaxHeight = cMaxHeight * cHeightPercent;
    }

    //--Execute.
    float cCufVertical = sinf((cPercent * 3.1415926f)) * cMaxHeight;
    mVerticalOffY = cCufVertical;
    if(mVerticalOffY > 0.0f) mVerticalOffY = 0.0f;

    ///--[Horizontal Handling]
    //--Linear movement between the two positions.
    float cPosX = pStartX + ((pEndX - pStartX) * cPercent);
    float cPosY = pStartY + ((pEndY - pStartY) * cPercent);

    //--Set positions. Update the tile versions as well.
    mTrueX = cPosX;
    mTileX = mTrueX / TileLayer::cxSizePerTile;
    mTrueY = cPosY;
    mTileY = mTrueY / TileLayer::cxSizePerTile;
    //fprintf(stderr, "Actor %s jumps to %ix%i, current %ix%i - Timer %f\n", mLocalName, pEndX, pEndY, (int)mTrueX, (int)mTrueY, cPercent);

    //--Zero the remainders.
    mRemainderX = 0.0f;
    mRemainderY = 0.0f;

    ///--[SFX]
    //--If specified, play the landing sound for this character.
    if(pTimer == 1 && mPlaySoundOnJumping[0] != '\0')
    {
        AudioManager::Fetch()->PlaySound(mPlaySoundOnJumping);
    }
    if(pTimer >= pTimerMax && mPlaySoundOnLanding[0] != '\0')
    {
        AudioManager::Fetch()->PlaySound(mPlaySoundOnLanding);
    }

    ///--[Finish Up]
    return (pTimer >= pTimerMax);
}
