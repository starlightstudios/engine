//--Base
#include "CarnationTitle.h"

//--Classes
#include "StringEntry.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarUIPiece.h"
#include "VirtualFile.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"
#include "SaveManager.h"

///======================================= Core Methods ===========================================
void CarnationTitle::LoadFile(LoadingPack *pLoadingPack)
{
    ///--[Documentation]
    //--Causes the game to load the file in the pack provided.
    if(!pLoadingPack) return;

    ///--[Execution]
    //--Make sure Adventure Mode actually has an operable path.
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");

    //--Clear active mods and reset which ones are active based on the loading package.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    rOptionsManager->ClearModActivity();
    void *rModPtr = pLoadingPack->mActiveMods->PushIterator();
    while(rModPtr)
    {
        rOptionsManager->SetModActive(pLoadingPack->mActiveMods->GetIteratorName());
        rModPtr = pLoadingPack->mActiveMods->AutoIterate();
    }

    //--Pass these for the SaveManager.
    SaveManager::Fetch()->SetSavegameName(pLoadingPack->mFileName);
    SaveManager::Fetch()->SetSavegamePath(pLoadingPack->mFilePath);

    //--Store the path.
    char *tPath = InitializeString(pLoadingPack->mFilePath);

    //--Store the character's name as a pointer.
    char *tCharName = InitializeString(pLoadingPack->mPartyNames[0]);

    //--Clear. All data is now wiped, don't use the loading pack anymore!
    DeactivateLoadingMode();

    //--Execute.
    char *tLaunchPath = InitializeString("%s/ZLaunch.lua", rAdventurePath);
    LuaManager::Fetch()->ExecuteLuaFile(tLaunchPath, 2, "S", tPath, "S", tCharName);

    //--Clean.
    free(tPath);
    free(tCharName);
    free(tLaunchPath);
}

///========================================== Update ==============================================
void CarnationTitle::UpdateLoadFile()
{
    ///--[Documentation]
    //--Shows files on the right side of the screen. Player can click one to load it.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[String Entry Form]
    //--When entering strings, pass the update down.
    if(mIsEnteringString)
    {
        //--Run the update.
        mStringEntryForm->Update();

        //--If the entry form flagged complete, extract the string and write the save.
        if(mStringEntryForm->IsComplete())
        {
            //--Flag.
            mIsEnteringString = false;

            //--Get the loading pack in question. If it doesn't exist, stop.
            LoadingPack *rPack = (LoadingPack *)mLoadingPackList->GetElementBySlot(mLoadingCursor + mLoadingOffset);
            if(!rPack) return;

            //--Get the note from the string entry form. It can legally be empty, in which case
            //  we generate a standard note.
            const char *rEnteredString = mStringEntryForm->GetString();

            //--Generate a note if the player left it empty.
            bool tDeallocateNote = false;
            char *rNewNote = SaveManager::GenerateNote(rEnteredString, tDeallocateNote);

            //--Run the SaveManager's subroutine to switch the note out.
            SaveManager::ModifyFileNote(rNewNote, rPack->mFilePath);

            //--Synchronize the note with the loading pacakge.
            ResetString(rPack->mFileName, rNewNote);

            //--If the note was generated, deallocate it here.
            if(tDeallocateNote) free(rNewNote);
        }
        else if(mStringEntryForm->IsCancelled())
        {
            mIsEnteringString = false;
        }
        return;
    }

    //--Scrollbars.
    if(UpdateScrollbars()) return;
    RunHitboxCheck();

    ///--[Highlight Check]
    //--Play a sound when the highlight changes.
    if(mMouseMoved)
    {
        //--Get via reply.
        int tOldHighlight = mHighlightedHitbox;
        mHighlightedHitbox = CheckRepliesWithin(cxHitbox_SelectFile0, cxHitbox_BackToTitle);

        //--If the highlight changes, and the new highlight is not -1, play a sound.
        if(tOldHighlight != mHighlightedHitbox && mHighlightedHitbox != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Click]
    //--Handle left clicking to activate the highlighted button.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Determine which loading list is in use.
        StarLinkedList *rPackList = mLoadingPackList;
        if(mLoadingShowAutosaves) rPackList = mAutosavePackList;

        //--Selecting a file to play.
        if(mHighlightedHitbox == cxHitbox_SelectFile0)
        {
            //--SFX. Plays before the load action.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Execute.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(0 + mLoadingOffset);
            LoadFile(rPack);
        }
        else if(mHighlightedHitbox == cxHitbox_SelectFile1)
        {
            //--SFX. Plays before the load action.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Execute.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(1 + mLoadingOffset);
            LoadFile(rPack);
        }
        else if(mHighlightedHitbox == cxHitbox_SelectFile2)
        {
            //--SFX. Plays before the load action.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Execute.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(2 + mLoadingOffset);
            LoadFile(rPack);
        }
        //--Back to main menu.
        else if(mHighlightedHitbox == cxHitbox_BackToTitle)
        {
            mHighlightedHitbox = -1;
            mMode = CT_MODE_TITLE;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Stop update.
        return;
    }

    ///--[Right Click]
    //--Handle right clicking, mostly edits notes.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        //--Determine which loading list is in use.
        StarLinkedList *rPackList = mLoadingPackList;
        if(mLoadingShowAutosaves) rPackList = mAutosavePackList;

        //--Selecting a file to play.
        if(mHighlightedHitbox == cxHitbox_SelectFile0)
        {
            //--SFX. Plays before the load action.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Execute.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(0 + mLoadingOffset);
            ActivateNoteEntry(0, rPack);
        }
        else if(mHighlightedHitbox == cxHitbox_SelectFile1)
        {
            //--SFX. Plays before the load action.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Execute.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(1 + mLoadingOffset);
            ActivateNoteEntry(1, rPack);
        }
        else if(mHighlightedHitbox == cxHitbox_SelectFile2)
        {
            //--SFX. Plays before the load action.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Execute.
            LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(2 + mLoadingOffset);
            ActivateNoteEntry(2, rPack);
        }
        //--Back to main menu.
        else if(mHighlightedHitbox == cxHitbox_BackToTitle)
        {
        }

        //--Stop update.
        return;
    }
}

///========================================== Drawing =============================================
void CarnationTitle::RenderLoadFile()
{
    ///--[Documentation]
    //--Renders a black underlay and some options.
    if(mLoadFileTimer < 1) return;

    ///--[Offset]
    float cRgtOffset = (1.0f - EasingFunction::QuadraticInOut(mLoadFileTimer, cxTimer_LoadFileMax)) * 1300.0f;
    glTranslatef(cRgtOffset, 0.0f, 0.0f);

    ///--[Render]
    //--Black underlay.
    CarnImages.rOverlay_BlackRgt->Draw();

    //--Loading mode.
    RenderLoadingMode();

    ///--[Clean]
    glTranslatef(cRgtOffset * -1.0f, 0.0f, 0.0f);
}
void CarnationTitle::RenderLoadingMode()
{
    ///--[Documentation]
    //--Override of the FlexMenu code, performs the same basic job but with new images.
    if(!Images.mIsReady) return;

    ///--[String Entry Form]
    //--When entering strings, pass the update down.
    if(mIsEnteringString)
    {
        mStringEntryForm->Render();
        return;
    }

    ///--[Header]
    //--Render some instructions.
    CarnImages.rFont_Header->DrawText((VIRTUAL_CANVAS_X * 0.5f), 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select A Save File");

    ///--[Scrollbar]
    StarLinkedList *rPackList = mLoadingPackList;
    if(mLoadingShowAutosaves) rPackList = mAutosavePackList;

    //--Show a scrollbar.
    StarUIPiece::StandardRenderScrollbar(mLoadingOffset, cxScroll_SavesPerPage, rPackList->GetListSize() - cxScroll_SavesPerPage, 188.0f, 404.0f, false, CarnImages.rScrollbar_Scroller, CarnImages.rScrollbar_Static);

    ///--[Loading Packs]
    //--Render the save game loading packs.
    for(int i = 0; i < cxScroll_SavesPerPage; i ++)
    {
        //--Get the loading pack in question.
        LoadingPack *rPack = (LoadingPack *)rPackList->GetElementBySlot(i + mLoadingOffset);
        if(!rPack) continue;

        //--Compute vertical offset.
        float cYOffset = cxPos_SaveBoxYOffset * i;
        glTranslatef(0.0f, cYOffset, 0.0f);

        RenderFileBox(rPackList->GetNameOfElementBySlot(i + mLoadingOffset), rPack);

        //--Clean.
        glTranslatef(0.0f, cYOffset * -1.0f, 0.0f);
    }

    //--Instruction String.
    CarnImages.rFont_Mainline->DrawText((VIRTUAL_CANVAS_X * 0.50f), 700.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Right-click a file to edit its note.");

    ///--[Back Button]
    TwoDimensionReal *rBackHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("BackToTitle");
    if(rBackHitbox)
    {
        CarnImages.rFrame_Button->Draw(rBackHitbox->mLft, rBackHitbox->mTop);
        CarnImages.rFont_Mainline->DrawText(rBackHitbox->mXCenter, rBackHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Back");

    }
}
void CarnationTitle::RenderFileBox(const char *pFileName, LoadingPack *pLoadingPack)
{
    ///--[Documentation]
    //--Renders file information, including the frame. This uses constants that assume the box is the topmost
    //  of the boxes. Use glTranslate() to render additional boxes.
    //--The box will always render a basic box even if the pack is empty.
    CarnImages.rFrame_SaveInfo->Draw();

    //--If there is no data here, stop.
    if(!pFileName || !pLoadingPack) return;

    ///--[Setup]
    //--Positions.
    float cFilenameX     =  347.0f;
    float cFilenameY     =  153.0f;
    float cTimestampR    = 1162.0f;
    float cTimestampY    =  153.0f;
    float cCharacterX    =  376.0f;
    float cCharacterY    =  191.0f;
    float cCharacterW    =  150.0f;
    float cCharNameOffX  =  100.0f;
    float cCharNameOffY  =   28.0f;
    float cCharLevelOffY =   48.0f;

    //--Scales.
    float cSpriteScale   = 2.0f;
    float cPortraitScale = 0.355932f;

    ///--[Information Strings]
    //--Name, timestamp.
    CarnImages.rFont_SaveMain->DrawTextArgs(cFilenameX, cFilenameY, 0, 1.0f, "File %s: %s", pFileName, pLoadingPack->mFileName);
    CarnImages.rFont_SaveMain->DrawText(cTimestampR, cTimestampY, SUGARFONT_RIGHTALIGN_X, 1.0f, pLoadingPack->mTimestamp);

    ///--[Character Data]
    //--Render character data.
    for(int p = 0; p < 4; p ++)
    {
        //--Compute positions.
        float cLft = cCharacterX + (p * cCharacterW);
        float cTop = cCharacterY;

        //--Check if the savefile in question has the hard path in it:
        if(!strncasecmp(pLoadingPack->mPartyNames[p], "HardPath|", 9))
        {
            //--Offset position.
            cLft = cLft - 36.0f;
            cTop = cTop - 12.0f;

            //--Subdivide the string. The second block is the character's name. The third is the DLPath.
            StarLinkedList *tStringList = Subdivide::SubdivideStringToList(pLoadingPack->mPartyNames[p], "|");

            //--Image.
            const char *rCharName = (const char *)tStringList->GetElementBySlot(1);
            const char *rDLPathString = (const char *)tStringList->GetElementBySlot(2);
            pLoadingPack->rRenderImg[p] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(rDLPathString);

            //--Render portrait, needs scaling.
            if(pLoadingPack->rRenderImg[p])
            {
                glTranslatef(cLft, cTop, 0.0f);
                glScalef(cPortraitScale, cPortraitScale, 1.0f);
                pLoadingPack->rRenderImg[p]->Draw();
                glScalef(1.0f / cPortraitScale, 1.0f / cPortraitScale, 1.0f);
                glTranslatef(-cLft, -cTop, 0.0f);
            }

            //--Name.
            CarnImages.rFont_Small->DrawText    (cLft + cCharNameOffX, cTop + cCharNameOffY, 0, 1.0f, rCharName);
            CarnImages.rFont_Small->DrawTextArgs(cLft + cCharNameOffX, cTop + cCharLevelOffY, 0, 1.0f, "Lv. %i", pLoadingPack->mPartyLevels[p] + 1);

            //--Clean.
            delete tStringList;
        }
        //--Render sprites.
        else
        {
            //--Render. We need to scale this up.
            if(pLoadingPack->rRenderImg[p])
            {
                glTranslatef(cLft, cTop, 0.0f);
                glScalef(cSpriteScale, cSpriteScale, 1.0f);
                pLoadingPack->rRenderImg[p]->Draw();
                glScalef(1.0f / cSpriteScale, 1.0f / cSpriteScale, 1.0f);
                glTranslatef(-cLft, -cTop, 0.0f);
            }

            //--Get the name from the buffer.
            int tCutoff = -1;
            for(int x = (int)strlen(pLoadingPack->mPartyNames[p]); x >= 0; x --)
            {
                if(pLoadingPack->mPartyNames[p][x] == '_')
                {
                    tCutoff = x;
                    break;
                }
            }

            //--Error.
            if(tCutoff == -1) continue;

            //--Special: If the class is "Teacher" and the name is "Christine", render "Chris" instead.
            if(!strncasecmp(pLoadingPack->mPartyNames[p], "Christine", 9) && !strncasecmp(&pLoadingPack->mPartyNames[p][tCutoff+1], "Teacher", 7))
            {
                Images.Data.rUIFont->DrawText    (cLft + cCharNameOffX, cTop + cCharNameOffY, 0, 1.0f, "Chris");
                Images.Data.rUIFont->DrawTextArgs(cLft + cCharNameOffX, cTop + cCharLevelOffY, 0, 1.0f, "Lv. %i", pLoadingPack->mPartyLevels[p] + 1);
            }
            //--Special: Tiffany goes by 55 for the moment.
            else if(!strncasecmp(pLoadingPack->mPartyNames[p], "Tiffany", 7))
            {
                Images.Data.rUIFont->DrawText    (cLft + cCharNameOffX, cTop + cCharNameOffY, 0, 1.0f, "55");
                Images.Data.rUIFont->DrawTextArgs(cLft + cCharNameOffX, cTop + cCharLevelOffY, 0, 1.0f, "Lv. %i", pLoadingPack->mPartyLevels[p] + 1);
            }
            //--Normal rendering.
            else
            {
                pLoadingPack->mPartyNames[p][tCutoff] = '\0';
                CarnImages.rFont_Small->DrawText    (cLft + cCharNameOffX, cTop + cCharNameOffY, 0, 1.0f, pLoadingPack->mPartyNames[p]);
                CarnImages.rFont_Small->DrawTextArgs(cLft + cCharNameOffX, cTop + cCharLevelOffY, 0, 1.0f, "Lv. %i", pLoadingPack->mPartyLevels[p] + 1);
                pLoadingPack->mPartyNames[p][tCutoff] = '_';
            }
        }
    }
}
