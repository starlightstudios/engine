//--Base
#include "TiledRawData.h"

//--Classes
#include "TiledLevel.h"

//--CoreClasses
#include "StarFileSystem.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///--[Debug]
//--Base Handlers
//#define TILEDRAWDATAFILEIO_DEBUG
#ifdef TILEDRAWDATAFILEIO_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--Flags
#define TRDFIO_READLEVEL true
#define TRDFIO_TILESETS true
#define TRDFIO_LAYERS true

///========================================== File I/O ============================================
void TiledRawData::ReadLevel(const char *pPath)
{
    ///--[Documentation and Setup]
    //--Kicks off all the file I/O. Reads the level into this class and sets everything up. Requires
    //  a functioning StarLumpManager since we're dealing with .slf files.
    if(mHasReadFileSuccessfully) return;
    StarLumpManager *rSLM = StarLumpManager::Fetch();
    if(!pPath) return;

    //--Make sure the file exists.
    DebugPush(TRDFIO_READLEVEL, "[TiledRawData] Reading level at %s\n", pPath);
    rSLM->Open(pPath);
    if(!rSLM->IsFileOpen())
    {
        DebugPop("Error: File not found.\n");
        return;
    }

    //--Set the name to the path (no extension) of the level.
    free(mName);
    mName = StarFileSystem::PareFileName(pPath, false);
    DebugPrint("Level name: %s\n", mName);

    //--Locate the info lump. It's always named 'AMapInfo' and has the header 'MAPINFO'.
    if(!rSLM->StandardSeek("AMapInfo", "MAPINFO"))
    {
        DebugPop("Error: No AMapInfo lump was located.\n");
        return;
    }

    //--Get the virtual file. This will have its file pointer moved by the SLM when we seek.
    VirtualFile *rSLFFile = rSLM->GetVirtualFile();

    //--Make sure that the MapInfoLump has 4 as its type. If not, this is an unknown map format.
    uint8_t tMapType;
    rSLFFile->Read(&tMapType, sizeof(uint8_t), 1);
    if(tMapType != 4)
    {
        DebugPop("Error: AMapInfo lump lists type as %i, not 4.\n", tMapType);
        return;
    }

    //--Get the tileset info and store it.
    mTilesetList = ReadTilesets(rSLM, rSLFFile);

    //--Begin reading the layers. There may be up to 100.
    int tLumpCount = 0;
    char tLumpNameBuffer[80];
    sprintf(tLumpNameBuffer, "Layer %02i", tLumpCount);
    while(rSLM->StandardSeek(tLumpNameBuffer, "LAYERDATA"))
    {
        //--Let the subroutine handle it.
        TiledLayerPack *nNewPack = ReadLayer(rSLFFile);

        //--Store the pack.
        mLayerList->AddElement(nNewPack->mName, nNewPack, &TiledRawData::DeleteLayerPack);

        //--Seek to the next object.
        tLumpCount ++;
        sprintf(tLumpNameBuffer, "Layer %02i", tLumpCount);
    }

    //--All checks passed, flag the read as complete.
    mHasReadFileSuccessfully = true;
    DebugPop("[TiledRawData] Completed reading file.\n");
}

///==================================== Sequenced Subroutines =====================================
StarLinkedList *TiledRawData::ReadTilesets(StarLumpManager *pSLM, VirtualFile *pVFile)
{
    ///--[Documentation and Setup]
    //--Given a virtual file that is already in position, reads all tileset data and stores it
    //  in a new linked list. The image data is local to the list, and should be dereferenced if
    //  the list is going to delete it.
    StarLinkedList *nList = new StarLinkedList(true);
    if(!pVFile) return nList;

    //--Get the total number of tilesets.
    int16_t tTilesetsTotal = 0;
    pVFile->Read(&tTilesetsTotal, sizeof(int16_t), 1);
    DebugPrint("Tilesets expected: %i\n", tTilesetsTotal);

    //--Debug.
    DebugPush(TRDFIO_TILESETS, "Reading tilesets.\n");

    //--Allocate and read.
    for(int i = 0; i < tTilesetsTotal; i ++)
    {
        //--Allocate a tileset pack.
        SetMemoryData(__FILE__, __LINE__);
        TilesetInfoPack *nPack = (TilesetInfoPack *)starmemoryalloc(sizeof(TilesetInfoPack));

        //--Initialize
        nPack->Initialize();

        //--Allocate name.
        nPack->mName = pVFile->ReadLenString();
        DebugPrint(" Name: %s\n", nPack->mName);

        //--Other data.
        nPack->mTilesToThisPoint = 0;
        nPack->mTilesTotal = 0;
        pVFile->Read(&nPack->mTilesToThisPoint, sizeof(int16_t), 1);
        pVFile->Read(&nPack->mTilesTotal, sizeof(int16_t), 1);
        DebugPrint("  Data: %i %i\n", nPack->mTilesToThisPoint, nPack->mTilesTotal);

        //--Store the pack.
        nList->AddElement("X", nPack, &TiledRawData::DeleteTilesetPack);
    }

    //--Debug.
    DebugPrint("Creating bitmaps for tilesets.\n");

    //--With the tileset data read out, create StarBitmaps and locate the tileset lumps. This must
    //  be done after all the tilesets are parsed since it moves the file pointer around.
    char tBuffer[STD_MAX_LETTERS];
    for(int i = 0; i < tTilesetsTotal; i ++)
    {

        //--Get the pack from the list.
        TilesetInfoPack *rPack = (TilesetInfoPack *)nList->GetElementBySlot(i);

        //--Name of the image's lump.
        sprintf(tBuffer, "Tileset|%s", rPack->mName);
        rPack->mTileset = pSLM->GetImage(tBuffer);
    }

    //--Debug.
    DebugPop("Finished tilesets.\n");

    //--All done.
    return nList;
}
TiledLayerPack *TiledRawData::ReadLayer(VirtualFile *pVFile)
{
    ///--[Documentation and Setup]
    //--Given a virtual file that is already in position, reads and returns a layer. The layer
    //  may be a tile, object, or image layer. May return a blank layer on error.
    TiledLayerPack *nPack = CreateLayerPack();
    if(!pVFile) return nPack;

    //--Debug.
    DebugPush(TRDFIO_LAYERS, "Reading layer information.\n");

    //--Get the layer's type. Subroutines handle each type.
    char *tType = pVFile->ReadLenString();

    //--Common data. All layers use these.
    nPack->mName = pVFile->ReadLenString();
    nPack->mProperties = ReadProperties(pVFile);

    //--Debug.
    DebugPrint(" Type: %s\n", tType);
    DebugPrint(" Name: %s\n", nPack->mName);

    //--Tile layers.
    if(!strcasecmp(tType, "Tile"))
    {
        DebugPrint(" Is Tile Layer.\n");
        ReadTileData(pVFile, nPack);
    }
    //--Object-type layer.
    else if(!strcasecmp(tType, "Object"))
    {
        DebugPrint(" Is Object Layer.\n");
        ReadObjectData(pVFile, nPack);
    }
    //--Image-type layer.
    else if(!strcasecmp(tType, "Image"))
    {
        //--Not handled yet.
        DebugPrint(" Is Image Layer.\n");
    }
    //--Other types are unhandled.
    else
    {
        DebugManager::ForcePrint("Error parsing layer lump: Unknown type %s\n", tType);
    }

    //--Clean up.
    free(tType);

    //--Debug.
    DebugPop("Finished layer.\n");

    //--All done.
    return nPack;
}
void TiledRawData::ReadTileData(VirtualFile *pVFile, TiledLayerPack *pDataPack)
{
    //--Given a virtual file that is already in position, reads out tile data into the pDataPack
    //  that has been provided. Assumes the name/properties have already been parsed and are
    //  in the pDataPack.
    if(!pVFile || !pDataPack) return;

    //--Sizing and offset information.
    pVFile->Read(&pDataPack->mX, sizeof(int16_t), 1);
    pVFile->Read(&pDataPack->mY, sizeof(int16_t), 1);
    pVFile->Read(&pDataPack->mW, sizeof(int16_t), 1);
    pVFile->Read(&pDataPack->mH, sizeof(int16_t), 1);

    //--Tile data.
    SetMemoryData(__FILE__, __LINE__);
    pDataPack->mTileData = (int16_t *)starmemoryalloc(sizeof(int16_t) * pDataPack->mW * pDataPack->mH);
    pVFile->Read(pDataPack->mTileData, sizeof(int16_t), pDataPack->mW * pDataPack->mH);

    //--Mark as a tile layer.
    pDataPack->mLayerType = TLP_TYPE_TILE;

    //--If this layer is wider/taller than the current, set this as the size.
    if(pDataPack->mW > mWid) mWid = pDataPack->mW;
    if(pDataPack->mH > mHei) mHei = pDataPack->mH;
}
void TiledRawData::ReadObjectData(VirtualFile *pVFile, TiledLayerPack *pDataPack)
{
    //--Given a virtual file that is already in position, reads out the object data into
    //  the provided pDataPack. Assumes the name/properties have already been parsed.
    if(!pVFile || !pDataPack) return;

    //--Collect all Object data. Store them on the linked-list.
    int tExpectedObjects = 0;
    pVFile->Read(&tExpectedObjects, sizeof(int16_t), 1);
    for(int i = 0; i < tExpectedObjects; i ++)
    {
        //--Create a storage pack.
        SetMemoryData(__FILE__, __LINE__);
        ObjectInfoPack *nPack = (ObjectInfoPack *)starmemoryalloc(sizeof(ObjectInfoPack));
        memset(nPack, 0, sizeof(ObjectInfoPack));
        nPack->mTileID = 0;

        //--Name/Type/Bye
        nPack->mName = pVFile->ReadLenString();
        nPack->mType = pVFile->ReadLenString();

        //--Position and tile data.
        pVFile->Read(&nPack->mX, sizeof(float), 1);
        pVFile->Read(&nPack->mY, sizeof(float), 1);
        pVFile->Read(&nPack->mW, sizeof(float), 1);
        pVFile->Read(&nPack->mH, sizeof(float), 1);
        pVFile->Read(&nPack->mTileID, sizeof(int16_t), 1);
        if(nPack->mTileID >= 100) nPack->mTileID --;

        //--Properties.
        nPack->mProperties = ReadProperties(pVFile);

        //--Add it to the list.
        pDataPack->mObjectInfo->AddElement("X", nPack, &TiledRawData::DeleteObjectPack);
    }

    //--Mark as an object layer.
    pDataPack->mLayerType = TLP_TYPE_OBJECT;
}

///===================================== Anytime Subroutines ======================================
PropertiesPack *TiledRawData::ReadProperties(VirtualFile *pVFile)
{
    //--Reads and stores a new PropertiesPack from the file, assuming it's in the right position
    //  to read them. Returns NULL on error. The file pointer is advanced by this function.
    if(!pVFile) return NULL;

    //--Property storage pack.
    SetMemoryData(__FILE__, __LINE__);
    PropertiesPack *nProperties = (PropertiesPack *)starmemoryalloc(sizeof(PropertiesPack));
    pVFile->Read(&nProperties->mPropertiesTotal, sizeof(int16_t), 1);
    SetMemoryData(__FILE__, __LINE__);
    nProperties->mKeys = (char **)starmemoryalloc(sizeof(char *) * nProperties->mPropertiesTotal);
    SetMemoryData(__FILE__, __LINE__);
    nProperties->mVals = (char **)starmemoryalloc(sizeof(char *) * nProperties->mPropertiesTotal);

    //--Read out the properties as key-value strings.
    for(int i = 0; i < nProperties->mPropertiesTotal; i ++)
    {
        //--Get the key/value pair.
        nProperties->mKeys[i] = pVFile->ReadLenString();
        nProperties->mVals[i] = pVFile->ReadLenString();
    }

    return nProperties;
}
