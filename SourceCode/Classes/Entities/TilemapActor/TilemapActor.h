///======================================= TilemapActor ///========================================
//--Entity that appears in the tilemap overworld. This has nothing to do with the Actor class.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootEntity.h"
#include "AdventureLevelStructures.h"
#include "TiledLevel.h"
#include "TranslationManager.h"

///===================================== Local Definitions ========================================
///--[Images]
//--Movement
#define TA_MOVE_IMG_STANDARD 4
#define TA_MOVE_IMG_MAXIMUM 8

//--Dying
#define TA_DEATH_TPF 4.0f
#define TA_DEATH_FRAMES_TOTAL 4

//--Notices
#define TA_DEFAULT_NOTICE_TICKS 180
#define TA_TRANSLATE_TICKS 30
#define TA_NOTICE_SPACING 10.0f

//--Display Strings
#define TA_DISPLAY_STRING_TICKS 15
#define TA_DISPLAY_STRING_OFFSET_X 0
#define TA_DISPLAY_STRING_OFFSET_Y -10

///--[Directions]
#define TA_DIR_NORTH 0
#define TA_DIR_NE 1
#define TA_DIR_EAST 2
#define TA_DIR_SE 3
#define TA_DIR_SOUTH 4
#define TA_DIR_SW 5
#define TA_DIR_WEST 6
#define TA_DIR_NW 7
#define TA_DIR_TOTAL 8

///--[Instructions]
#define TA_INS_NONE 0
#define TA_INS_MOVE_NORTH 1
#define TA_INS_MOVE_EAST 2
#define TA_INS_MOVE_SOUTH 3
#define TA_INS_MOVE_WEST 4
#define TA_INS_HIGHEST 4

///--[Sizing]
#define TA_ENCOUNTER_DIST_DEFAULT 8.0f
#define TA_SIZE 8
#define TA_EXAMINE_DIM  10.0f
#define TA_EXAMINE_DIST 6.0f
#define TA_AUTOFIRE_DIST 32.0f

///--[AI Definitions]
//--Viewcone
#ifndef TILEMAP_ACTOR_DETECTION
#define TILEMAP_ACTOR_DETECTION
#define TA_DEFAULT_DETECT_RANGE 80
#define TA_DEFAULT_DETECT_ANGLE 120
#endif

//--Instruction Codes
#define TA_AI_WANDER 0
#define TA_AI_WAIT 1
#define TA_AI_CHASE 2
#define TA_AI_LEASH 3
#define TA_AI_TURNTOFACE 4

//--Mugging
#define TA_MUG_TICKS_TOTAL 600
#define TA_MUG_TICKS_PER_PRESS 100
#define TA_MUG_TICK_LOSS_PER_TICK 3
#define TA_MUG_STUN_TICKS 300
#define TA_MUG_STUN_FRAMES_TOTAL 3

//--Commander Handling
#define TA_COMMAND_RESET 30

//--Chase Flags
#define TA_AI_USE_BASIC_AI 0
#define TA_AI_USE_GUARD_AI 1

///--[Misc]
//--Rubbing
#define TA_RUB_TICKS 32

//--Gravity
#define TA_DEFAULT_GRAVITY 0.675f

//--Breathing
#define TA_BREATHE_MAX 300
#define TA_BREATHE_PERCENT 0.90f

//--Sinking
#define TA_SINK_BOTTOM 31.0f

//--Path pulses allowed per tick
#define TA_MAX_PATH_PULSES 10

//--Use computed view distance.
#define TA_USE_COMPUTED_VIEWDIST -1.0f

///--[Advanced Spotting]
#define TARUI_SPOT_TPF 12.0f
#define TARUI_ALERT_TPF 4.0f

///===================================== Local Structures =========================================
///--[TA_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#define TA_STRINGS_TOTAL 2
class TA_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[TA_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTurnCount;
            char *mPreemptiveStrike;
        }str;
    };

    //--Functions
    TA_Strings();
    virtual ~TA_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///--[TA_Image_Resolve_Pack]
//--Contains DL Paths for all of the images the entity can load. When certain flags are set,
//  this structure keeps a copy of all image paths for the entity, and the entity can re-load
//  all images. This is used during program boot, when entities may be created before the
//  images are loaded. Most entities never instantiate this.
#include "StarLinkedList.h"
#define TA_DL_PATH_LEN 80
typedef struct TA_Image_Resolve_Pack
{
    //--Data.
    char mMoveImages[TA_DIR_TOTAL][TA_MOVE_IMG_MAXIMUM][TA_DL_PATH_LEN];
    char mRunImages[TA_DIR_TOTAL][TA_MOVE_IMG_MAXIMUM][TA_DL_PATH_LEN];
    char mStandingImages[TA_DIR_TOTAL][TA_DL_PATH_LEN];
    char mBreatheImages[TA_DIR_TOTAL][TA_DL_PATH_LEN];
    StarLinkedList *mSpecialImageList;//char *, master

    //--Functions.
    void Initialize()
    {
        memset(mMoveImages,     0, sizeof(char) * TA_DIR_TOTAL * TA_MOVE_IMG_MAXIMUM * TA_DL_PATH_LEN);
        memset(mRunImages,      0, sizeof(char) * TA_DIR_TOTAL * TA_MOVE_IMG_MAXIMUM * TA_DL_PATH_LEN);
        memset(mStandingImages, 0, sizeof(char) * TA_DIR_TOTAL *                     TA_DL_PATH_LEN);
        memset(mBreatheImages,  0, sizeof(char) * TA_DIR_TOTAL *                     TA_DL_PATH_LEN);
        mSpecialImageList = new StarLinkedList(true);
    }
    void Delete()
    {
        delete mSpecialImageList;
    }
}TA_Image_Resolve_Pack;

///--[AdvPathPack]
//--Advanced Pathing Pack. Represents a pathing movement. Not used for smart movement.
typedef struct AdvPathPack
{
    int mPulseVal;
    float mDistToTarget;
    int mX;
    int mY;
}AdvPathPack;

///--[Special Frame Anim]
//--Contains a set of special frames and timings, allowing an animation to repeat.
typedef struct SpecialFrameAnimPack
{
    //--Variables.
    int mTimer;
    int mSlot;
    int mResetFrame;
    int mTotalFrames;
    int *mTimings;
    char **mFrameNames;

    //--Functions
    void Initialize()
    {
        mTimer = 0;
        mSlot = 0;
        mResetFrame = 0;
        mTotalFrames = 0;
        mTimings = NULL;
        mFrameNames = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        //--Cast and check.
        SpecialFrameAnimPack *rPtr = (SpecialFrameAnimPack *)pPtr;
        if(!pPtr) return;

        //--Deallocate.
        for(int i = 0; i < rPtr->mTotalFrames; i ++) free(rPtr->mFrameNames[i]);
        free(rPtr->mTimings);
        free(rPtr->mFrameNames);
        free(rPtr);
    }
}SpecialFrameAnimPack;

///========================================== Classes =============================================
class TilemapActor : public RootEntity
{
    ///--[ ============= Variables ============ ]
    private:
    //--System
    bool mIsDisabled;
    bool mBlockRender;
    bool mIgnoreWorldStop;
    bool mDoesntResetDepthOverride;
    bool mAutoDespawnAfterAnimation;
    int mCollisionDepth;
    bool mIgnoreSpecialLighting;
    bool mIgnoreCollisionsWhenMoving;
    bool mIsPartyEntity;
    bool mRendersAfterTiles;
    char *mParagonGrouping;
    bool mWholeCollision;
    bool mRenderAsSingleBlock;

    //--Instructions
    int mInstructionHandlesThisTick;
    uint32_t mLastInstructionHandle;
    void *rLastHandledInstruction;

    //--Position
    bool mIsClipped;
    int mFacing;
    int mTileX;
    int mTileY;
    bool mHasSlopeMoveLeft;
    float mTrueX;
    float mTrueY;
    float mRemainderX;
    float mRemainderY;

    //--Jump
    int mJumpTimer;
    bool mIgnoresGravity;
    float mVerticalOffY;
    float mZSpeed;
    float mGravity;
    char mPlaySoundOnJumping[64];
    char mPlaySoundOnLanding[64];

    //--Activation Handling
    bool mAutoActivates;
    bool mActivatesByRub;
    bool mRespondsToShot;
    int mRubTimer;
    float mAutoActivateRange;
    bool mHandlesActivation;
    int mExtendedActivationDirection;
    char *mActivationScript;
    TwoDimensionReal mActivationDim;

    //--Instruction Handling
    int mCurrentInstruction;
    StarLinkedList *mInstructionList;

    //--Movement Handling
    bool mIsMoving;
    bool mIsRunning;
    int mMoveTimer;
    int mMoveDirection;
    float mMoveSpeed;
    float mRunSpeed;
    int mStepState;
    int mTicksSinceStop;

    //--Movement Specials
    bool mIsFlying;
    bool mAutoAnimates;
    bool mAutoAnimatesFast;
    bool mYOscillates;
    int mYOscillateTimer;
    int mLastMoveTick;
    float mAmountMovedLastTick;
    int mNegativeMoveTimer;

    //--Footstep Sounds
    bool mNoFootstepSounds;
    bool mWasLastFootstepLeft;
    bool mUseEightRunFrames;
    int mPreviousFootstepRollL;
    int mPreviousFootstepRollR;
    char mWalkFootstepSound[STD_MAX_LETTERS];
    char mRunFootstepSound[STD_MAX_LETTERS];
    char mWalkFootstepBuffer[STD_MAX_LETTERS];
    char mRunFootstepBuffer[STD_MAX_LETTERS];

    //--Images
    bool mIsVoidRiftMode;
    int mVoidRiftTimer;
    int mVoidRiftFrame;
    bool mUsesSpecialIdleImages;
    bool mIsTiny;
    float mOffsetX;
    float mOffsetY;
    float mTempOffsetX;
    float mTempOffsetY;
    float mWalkTicksPerFrame;
    float mRunTicksPerFrame;
    float mOverrideDepth;
    float mOverrideDepthOffset;
    bool mNoAutomaticShadow;
    bool mDisableMainRender;
    int mShakeTicks;
    int mShakeOffX;
    int mShakeOffY;
    int mMoveImagesTotal; //Defaults to TA_MOVE_IMG_STANDARD
    StarBitmap *rMoveImages[TA_DIR_TOTAL][TA_MOVE_IMG_MAXIMUM];
    StarBitmap *rRunImages[TA_DIR_TOTAL][TA_MOVE_IMG_MAXIMUM];
    StarBitmap *rShadowImg;
    StarBitmap *rManualOverlay;
    static bool xHasResolvedPoofImages;
    static StarBitmap *xrPoofImages[TA_DEATH_FRAMES_TOTAL];

    //--Optional Images
    int mBreatheTimer;
    StarBitmap *rStandingImages[TA_DIR_TOTAL];
    StarBitmap *rBreatheImages[TA_DIR_TOTAL];

    //--Image Path Storage
    bool mIsStoringImagePaths;
    TA_Image_Resolve_Pack *mImagePathStorage;

    //--Special Images
    bool mAlwaysFacesUp;
    bool mIsShowingSpecialImage;
    bool mIsShowingFlashwhiteSequence;
    int mFlashwhiteTimer;
    StarBitmap *rCurrentSpecialImage;
    StarLinkedList *mSpecialImageList; //StarBitmap *, ref
    char *mReserveSpecialImage;
    SpecialFrameAnimPack *rActiveAnimation;
    StarLinkedList *mSpecialImageAnimationList;//SpecialFrameAnimPack *, master

    //--Overlay Images
    bool mIsShowingOverlayImages;
    int mOverlayImageTimer;
    float mOverlayImageTPF;
    int mOverlayImagesTotal;
    StarBitmap **rOverlayImages;

    //--Idle Animations
    bool mHasIdleAnimation;
    int mIdleTimerCountdown;
    int mIdleAnimTimer;
    int mIdleTPF;
    int mIdleLoopsToFrame; //Set to -1 to disable looping.
    int mIdleImagesTotal;
    StarBitmap **rIdleImages;

    //--Enemy State
    bool mIsEnemy;
    bool mIsBagRefill;
    bool mIsItemNode;
    char *mItemNodeKeyword;
    bool mIsItemDrop;
    bool mHalveLeashDistance;
    bool mIsWanderNPC;
    bool mIsPredator;
    int mEnemyToughness;
    int mStunTimer;
    int mAIState;
    int mEnemyLockFacing;
    float mLeashX;
    float mLeashY;
    char *mEnemyPackName;
    char *mDefeatSceneName;
    char *mRetreatSceneName;
    char *mVictorySceneName;
    StarLinkedList *mEnemyListing; //char *, dummy

    //--Enemy Mugging
    bool mCanBeMugged;
    bool mHasAnythingToMug;
    bool mHasBeenMugged;
    bool mNeverDropsItemsOnMug;
    int mMugTimer;
    int mMugStunTimer;
    StarBitmap *rMugBarEmpty;
    StarBitmap *rMugBarFull;
    StarBitmap *rMugStun[TA_MUG_STUN_FRAMES_TOTAL];

    //--Enemy Detection
    bool mIsBlind;
    float mViewDistance;
    float mViewAngle;
    float mCurrentViewAngle;
    int mDropSuspicionTicks;
    int mSpottedPlayerTicks;
    int mSpottedPlayerTicksMax;
    float mSpottingOffsetX;
    float mSpottingOffsetY;
    float mSpottingCircleX;
    float mSpottingCircleY;
    float mSpottingCircleR;
    float mSpottingCircleT;
    StarBitmap *rSpottedExclamation;
    StarBitmap *rSpottedQuestion;

    ///--Alternate Spot/Alert Formulation
    bool mEyeSpotting;
    int mSpotTimer;
    int mAlertTimer;
    int mSpotFramesTotal;
    int mAlertFramesTotal;
    StarBitmap **rSpottingFrames;
    StarBitmap **rAlertingFrames;

    //--Special Enemies Properties
    bool mIgnoresReinforcement;
    bool mDoesNotTriggerBattles;
    bool mNeverPauses;
    int mTrueFacing;
    bool mIsImmobile;
    bool mIsFast;
    bool mIsRelentless;
    bool mNeverMugAutoWin;
    char *mFollowTarget;
    int mFollowersRegistered;
    StarLinkedList *mFollowerNameListing; //Dummy Integer
    StarLinkedList *mCommandsNameListing; //Dummy Integer

    //--Leashing
    StarLinkedList *mLeashList;

    //--Immunity State
    bool mIsIgnoringPlayer;
    StarLinkedList *mIgnorePlayerCodeList; //Dummy Integer

    //--Reinforcement Stuff
    int mLastComputedReinforcement;
    int mReinforcementTimer;

    //--AI Variables
    bool mMustRollWanderPoint;
    float mWanderX;
    float mWanderY;
    int mWaitTimer;
    int mWaitTimerMax;

    //--Dying Sequence
    bool mIsDying;
    int mDeathTimer;

    //--Special UI Variables
    int mDisplayStringTimer;
    bool mClearDisplayStringAfterBattle;
    char *mUIDisplayString;

    //--Special Frame Auto Cycler
    bool mCycleSpecialFramesThenDelete;
    int mCycleTPF;
    int mCycleTimer;

    //--Deep Layers
    bool mIsInDeepLayer;
    int mDeepLayerCollision;
    bool mDeepLayerPreventsRun;
    int mDeepLayerPixelSink;
    char mDeepLayerOverlayName[32];
    char mDeepLayerWalkOverride[32];
    char mDeepLayerRunOverride[32];

    ///--[AI]
    //--Chasing.
    int mEnemyAIType;
    int mChaseTimeRemaining;
    int mLostPlayerTimer;
    int mExternalGuardModeCooldown;
    int mChaseWaitTicks;
    int mLastSawPlayerX;
    int mLastSawPlayerY;
    int mChaseSearchX;
    int mChaseSearchY;
    int mChaseWalkDist;
    int mHardChaseTimer;
    int mCommandIssueCooldown;
    Pathrunner *mChasePathrunner;
    StarLinkedList *mChaseInstructions; //int *, master

    //--Leashing.
    bool mUseDumbLeash;
    int mSmartLeashWalkDist;
    Pathrunner *mLeashPathrunner;
    StarLinkedList *mLeashInstructions; //int *, master

    ///--[ ======== Pathing Variables ========= ]
    //--Common Variables
    int mDropPatrolTicks;
    bool mIsPatrolling;
    bool mUseStopOnEachNode;
    bool mNeverRefaceOnNode;
    int mPatrolTicksLeft;
    int mPatrolNodesTotal;
    int mPatrolNodeCurrent;
    char **mPatrolNodeList;

    //--Flags for Smart/Advanced
    bool mPathToAnyNode;

    //--Smart Patrol
    bool mIsSmartPatrol;
    bool mSmartPathToAnyNode;
    float mSmartPatrolDistanceLeft;
    StarLinkedList *mSmartPatrolInstructions; //int *, master

    //--Advanced Patrol
    bool mIsAdvancedPatrol;
    int mPathTicksSoFar;
    int mMinimumPathTicks;
    Pathrunner *mPathRunner;

    ///--[ ====== Functions and Statics ======= ]
    protected:
    public:
    //--System
    TilemapActor();
    virtual ~TilemapActor();

    //--Public Variables
    bool mRanLastTick;

    //--Static Public Variables
    static bool xAllowRender;
    static bool xAlwaysIgnorePlayer;
    static bool xIsRenderingBeforeTiles;
    static bool xRenderViewcone;
    static int xViewconeCount;
    static int xViewconeCountLastTick;
    static char *xEnemyShadowPath;
    static StarFont *xrReinforcementFont;
    static bool xHasBuiltToughnessLookups;
    static StarlightColor xToughnessLookups[3];
    static int xMaxFootstepSounds;
    static bool xShouldStoreImagePaths;
    static int xAutofireCooldown;
    static bool xIsMugCheck;
    static int xMugPlatina;
    static int xMugExperience;
    static int xMugJobPoints;
    static StarLinkedList *xMugItemList;
    static bool xActorWasMugged;
    static float xCombatTriggerRange;
    static bool xIsPlayerCollisionCheck;
    static int xPlayerFacing;
    static bool xAlwaysRunEvents;
    static bool xPulseIsNotCombat;
    static char *xBuildGraphicsFromNamePath;
    static int xActorPathPulsesThisTick;
    static int xActorsPathingThisTick;
    static TA_Strings *xrStringData;

    //--Property Queries
    bool CanHandleInstructions(void *pPtr);
    int GetX();
    int GetY();
    float GetWorldX();
    float GetWorldY();
    float GetRemainderX();
    float GetRemainderY();
    float GetRenderDepth();
    int GetFacing();
    int GetMoveTimer();
    bool IsHandlingInstruction();
    float GetMoveSpeed();
    float GetFrameSpeed();
    float GetRunFrameSpeed();
    int GetCollisionDepth();
    bool IsPositionClipped(int pX, int pY);
    bool IsPositionClippedOverride(int pX, int pY);
    float GetMovementLastTick();
    bool IsPartyEntity();
    float GetViewDistance();
    bool WasMugged();
    bool AlwaysFaceUp();
    bool MugBlocksAutoWin();
    bool ShouldRubActivate();
    bool RespondsToShot();
    bool IsFlashingWhite();
    bool NeverDropsItemsOnMug();
    int GetMovementFramesTotal();
    int GetExtendedActivate();
    bool HasSpecialFrame(const char *pName);
    bool IsShowingSpecialFrame();
    const char *GetNameOfSpecialFrame();

    //--Manipulators
    void SetItemNodeKeyword(const char *pKeyword);
    void SetBlockRender(bool pFlag);
    void SetParagonGrouping(const char *pGrouping);
    void SetDisable(bool pIsDisabled);
    void SetRendersAfterTiles(bool pFlag);
    void SetIgnoreDepthOverride(bool pFlag);
    void FlagHandledInstruction(void *pPtr);
    void OverrideInstructionHandle();
    void SetRunning(bool pFlag);
    void SetCollisionFlag(bool pFlag);
    void SetPosition(float pX, float pY);
    void SetPositionByPixel(float pX, float pY);
    void SetPositionByEntity(const char *pEntityName);
    void SetRemainders(float pX, float pY);
    void SetOffsets(float pXOffset, float pYOffset);
    void SetDisableMainRender(bool pFlag);
    void SetManualRenderOverlay(const char *pDLPath);
    void SetMoveImage(int pDirection, int pSlot, const char *pDLPath);
    void SetRunImage(int pDirection, int pSlot, const char *pDLPath);
    void SetStandingImage(int pDirection, const char *pDLPath);
    void SetBreatheImage(int pDirection, const char *pDLPath);
    void SetShadow(const char *pPath);
    void SetMoveSpeed(float pValue);
    void SetFrameSpeed(float pValue);
    void SetRunFrameSpeed(float pValue);
    void SetActivesByRub(bool pFlag);
    void SetActivationScript(const char *pPath);
    void SetExtendedActivationDirection(int pValue);
    void SetFacing(int pDirection);
    void SetFacingToNPC(const char *pName);
    void SetFacingToPoint(int pX, int pY);
    void ForceMoving(int pOverride);
    void StopMoving();
    void BeginStun();
    void BeginDying();
    void DieQuickly();
    void ActivateFlashwhite();
    void SetSpecialIdleFlag(bool pFlag);
    void SetTinyFlag(bool pFlag);
    void SetFlyingFlag(bool pFlag);
    void SetAutoAnimateFlag(bool pFlag);
    void SetAutoAnimateFastFlag(bool pFlag);
    void SetOscillateFlag(bool pFlag);
    void SetCollisionDepth(int pValue);
    void SetOverrideDepth(float pValue);
    void SetOverrideDepthOffset(float pValue);
    void SetMoveTimer(int pTimer);
    void SetAutoDespawn(bool pFlag);
    void SetIgnoreSpecialLights(bool pFlag);
    void SetIgnoreClipsWhenMoving(bool pFlag);
    void SetPartyEntityFlag(bool pFlag);
    void SetNoFootstepSounds(bool pFlag);
    void SetUseEightFramesForFootsteps(bool pFlag);
    void DisableAutomaticShadow(bool pFlag);
    void SetDontRefaceFlag(bool pFlag);
    void SetNegativeMoveTimer(int pTicks);
    void SetAutofire(float pRange);
    void SetUIDisplayString(const char *pString, bool pClearAfterBattle);
    void HandleUIPostBattle();
    void SetYVerticalOffset(float pOffset);
    void SetIgnoresGravity(bool pFlag);
    void SetZSpeed(float pSpeed);
    void SetGravity(float pGravity);
    void SetJumpingSound(const char *pSoundName);
    void SetLandingSound(const char *pSoundName);
    void SetBlockMugAutoWin(bool pFlag);
    void Rub();
    void SetToCycleSpecialFramesThenDelete(int pTicksPerFrame);
    void SetRespondsToShot(bool pFlag);
    void SetNeverDropItemsOnMug(bool pFlag);
    void SetMoveFramesTotal(int pTotal);
    void SetShakeTicks(int pTicks);
    void SetSpottingIconOffsets(float pX, float pY);
    void SetSpottingCircleOffsets(float pX, float pY, float pRadius, float pThickness);

    //--Core Methods
    bool HandleActivation(int pX, int pY);
    bool HandleAutoActivation(int pX, int pY);
    void HandleRubActivation();
    bool HandleShotCheck(int pX, int pY);
    void HandleShotHit();
    void AddSpecialFrame(const char *pName, const char *pDLPath);
    void ActivateSpecialFrame(const char *pName);
    void WipeSpecialFrames();
    void SetReserveFrame(const char *pName);
    char *GetWalkFootstepSFX(bool pOverrideNoFootstep);
    char *GetRunFootstepSFX(bool pOverrideNoFootstep);
    void PrintSpecialFrames();
    void ReresolveLocalImages();
    ActorNotice *SpawnNotice(const char *pText);
    void CreateSpecialFrameAnim(const char *pName);
    void SetSpecialFrameAnimLen(const char *pName, int pFrames);
    void SetSpecialFrameAnimCase(const char *pName, int pSlot, int mTicks, const char *pFrameName);
    void SetSpecialFrameAnimeResetFrame(const char *pName, int pSlot);
    void ActivateSpecialFrameAnim(const char *pName);

    ///--Collisions
    bool IsLftClipped();
    bool IsTopClipped();
    bool IsRgtClipped();
    bool IsBotClipped();
    bool IsLftClippedNarrow();
    bool IsTopClippedNarrow();
    bool IsRgtClippedNarrow();
    bool IsBotClippedNarrow();
    static bool AbsIsLftClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth);
    static bool AbsIsTopClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth);
    static bool AbsIsRgtClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth);
    static bool AbsIsBotClipped(TiledLevel *pLevel, float pLft, float pTop, int pDepth);

    ///--[ ================ Enemy Methods =============== ]
    //--System
    void ActivateEnemyMode(const char *pAssociatedName);

    //--Property Queries
    const char *GetUniqueEnemyName();
    bool IsEnemy(const char *pUniqueName);
    float GetRangeToPlayer();
    float ComputeCurrentViewDistance();
    bool IsPlayerInSightCone(float pUseViewDistance);
    TwoDimensionReal GetFollowPosition(const char *pRegisterName);
    const char *GetZerothEnemyPrototype();

    //--Manipulators
    void ActivateBagRefillMode();
    void ActivateItemNodeMode();
    void ActivateFallenLootMode(const char *pLootString);
    void ActivateWanderMode();
    void ActivatePatrolMode(const char *pPatrolPath);
    void SetWorldStopIgnoreFlag(bool pFlag);
    void SetCombatEnemy(const char *pPrototypeName);
    void AddCombatEnemy(const char *pPrototypeName);
    void SetDefeatScene(const char *pName);
    void SetVictoryScene(const char *pName);
    void SetRetreatScene(const char *pName);
    void SetLeashingPoint(float pX, float pY);
    void AddCommandString(const char *pString);
    void AddIgnoreString(const char *pString);
    void SetDetectRange(int pPixels);
    void SetDetectAngle(int pDegrees);
    void SetBlind(bool pFlag);
    void SetImmobile(bool pFlag);
    void SetToughness(int pValue);
    void SetFast(bool pFlag);
    void SetRelentless(bool pFlag);
    void SetNeverPauses(bool pFlag);
    void SetFollowTarget(const char *pTarget);
    void RemoveFollower(const char *pName);
    void SetMuggable(bool pIsMuggable);
    void SetMugLootable(bool pIsMugLootable);
    void ActivateTeamChase();
    void SetLockFacing(int pDirection);
    void SetVoidRiftMode();
    void SetRenderAsSingleBlock();
    void SetUseWholeCollision();

    //--Core Methods
    void PulseIgnore(const char *pPulseString);
    void BuildGraphicsFromName(const char *pName, bool pHasRunFrames, bool pIsEightDirection, bool pIsTwoDirectional);
    void GetPlayerPosition(float &sX, float &sY, float &sZ);
    void ComputeReinforcement(bool pIsPolling);
    void AppendEnemiesToCombat(bool pIsPolling);
    void HandleMugging();
    void SpawnAutoWinNotices(int pXP, int pJP, int pPlatina, StarLinkedList *pItemList);

    //--Update
    bool HandleEnemyActivation(int pX, int pY);
    void UpdateEnemyMode();

    ///--Common AI Routines
    float GetChaseDistance();
    void BagRefillAI();
    void ItemDropAI();
    bool IsIgnoringPlayer();
    bool ShouldRenderViewcone();
    bool RunBlockCases();
    bool CheckBattleTrigger();
    bool HandleSightCheck();

    //--Wander AI
    void HandleWanderAI();

    //--Wait AI
    void HandleWaitAI();

    //--Chase AI
    float GetChasePercent();
    void BeginChase();
    void EndChase();
    void HandleChaseAI();
    void HandleGuardChaseAI();
    void RollGuardSearchPosition();
    void GuardActivateNearbyEnemies();
    void GuardActivateCommandEnemies();
    void ActivateGuardMode(int pLastSeenX, int pLastSeenY, int pSearchTimerCap, bool pOverrideTimer);
    bool GuardPathTo(float pX, float pY, float pSpeedFactor);

    //--Leash AI
    void BeginLeashing();
    void FinishLeashing();
    void HandleLeashAI();

    //--Turn AI
    void HandleTurnAI();

    ///--Idle Animations
    void SetIdleModeActive(bool pFlag);
    void AllocateIdleFrames(int pTotal);
    void SetIdleFrame(int pSlot, const char *pDLPath);
    void SetIdleTPF(int pValue);
    void SetIdleLoopFrame(int pFrame);
    void RandomizeIdleTimer();
    void IdleUpdate();

    ///--Instructions Handling
    bool HandleInstruction(int pInstructionCode);
    void EnqueueInstruction(int pInstructionCode, bool pIsPlayerControl);
    void WipeInstructions();

    ///--Instruction
    bool HandleMoveTo(int pX, int pY, float pSpeed);
    bool HandleMoveAmount(float &sX, float &sY);
    bool HandleJumpTo(int pStartX, int pStartY, int pEndX, int pEndY, int pTimer, int pTimerMax);

    ///--Map Parsing
    static SpawnPackage *ParseEnemyProperties(ObjectInfoPack *pObjectInfo);
    void SetEnemyPropertiesFromPackage(SpawnPackage *pPackage, bool pIsParagon);
    void HandleEnemyAutoPatrol(SpawnPackage *pPackage);

    ///--Movement Handling
    bool EmulateMovement(bool pLft, bool pTop, bool pRgt, bool pBot);
    bool EmulateMovement(bool pLft, bool pTop, bool pRgt, bool pBot, float pWalkSpeed, float pRunSpeed);
    bool HandleMovement(int pX, int pY, bool pAllowDiagonals);

    ///--Overlays
    bool IsOverlayActive();
    int ComputeOverlayFrame();
    StarBitmap *GetOverlayFrameInSlot(int pSlot);
    StarBitmap *GetCurrentOverlayFrame();
    void SetOverlaysActive(bool pFlag);
    void SetOverlayTPF(float pTPF);
    void AllocateOverlayFrames(int pTotal);
    void SetOverlayFrame(int pSlot, const char *pDLPath);
    void UpdateOverlays();

    ///--[ ============= Pathing Subroutines ============ ]
    ///--Pathing
    void ActivateAdvancedPathing();
    void ActivateSmartPathing();
    void SetPatrolToAnyNode(bool pFlag);
    void DecideNextPathDestination();

    ///--Advanced Pathing
    void HandleAdvancedPathing();
    void HandleAdvancedMoving(StarLinkedList *pList, float &sDistanceVar);

    ///--Smart Pathing
    void HandleSmartPathing();
    void HandleSmartMoving(StarLinkedList *pList, float &sDistanceVar);

    ///--Standard Pathing
    void HandleStandardPathing();

    private:
    ///--Private Core Methods
    void ResolveActivationDimensions();

    public:
    ///--Update
    virtual void Update();
    bool HandlePlayerControls(bool pAllowRunning);

    ///--File I/O
    ///--Drawing
    virtual void Render();
    void RenderUI();
    void RenderStaminaRing(float pCamX, float pCamY, float pScale, float pPct, float pAlpha, StarBitmap *pBarUnder, StarBitmap *pBarOver);
    void RenderMugBar();
    void RenderVisibilityCone(bool pIsLightingActive, GLint pShaderHandle);
    StarBitmap *ResolveFrame();
    StarBitmap *GetMapImage(int pTimer);

    ///--Render UI
    void SetEyeSpotting(int pEyeSprites, int pBubbleSprites);
    void SetEyeSprite(int pSlot, const char *pPath);
    void SetBubbleSprite(int pSlot, const char *pPath);
    void RenderSpottingSprites();
    void RenderSpottingCircle(float pPercent);

    ///--Pointer Routing
    StarBitmap *GetMoveImage(int pDirection, int pSlot);
    StarBitmap *GetRunImage(int pDirection, int pSlot);
    StarBitmap *GetShadowImage();

    ///--Static Functions
    static int GetBestFacing(float pSubjectX, float pSubjectY, float pTargetX, float pTargetY);
    static int GetOrthoFacing(float pSubjectX, float pSubjectY, float pTargetX, float pTargetY);
    static void ReresolveAllImages();

    ///--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

///--Hooking Functions
int Hook_TA_Create(lua_State *L);
int Hook_TA_CreateUsingPosition(lua_State *L);
int Hook_TA_GetProperty(lua_State *L);
int Hook_TA_SetProperty(lua_State *L);
int Hook_TA_ChangeCollisionFlag(lua_State *L);
