//--Base
#include "MagicPack.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

///========================================== System ==============================================
MagicPack::MagicPack()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_MAGICPACK;

    ///--[MagicPack]
    //--System
    mLocalName = InitializeString("Unnamed Magic Spell");

    //--Storage
    mMPCost = 0;
    mScriptPath = NULL;
    mDescription = NULL;

    //--Context Menu
    mIsCastFromContextMenu = false;
    mContextTargetFlags = MAGIC_TARGET_NONE;

    //--Corrupter Menu
    mCanCastNow = false;
    mIsCastFromCorrupterMenu = false;
}
MagicPack::~MagicPack()
{
    free(mLocalName);
    free(mScriptPath);
    free(mDescription);
}

///--[Public Statics]
//--When querying targets from the context menu, this flag will be flipped to true if the object can
//  be targetted by the spell being queried.
bool MagicPack::xCanTargetThis = false;

//--Represents the object currently being targetted by the MagicPack when a context-menu click occurs.
//  Should only be non-NULL when a query is executing.
void *MagicPack::xrTargetObject = NULL;

//--A linked-list that stores all the active MagicPacks in use. Can be cleared with a Lua function.
StarLinkedList *MagicPack::xMagicList = new StarLinkedList(true);

///===================================== Property Queries =========================================
const char *MagicPack::GetName()
{
    return mLocalName;
}
const char *MagicPack::GetDescription()
{
    return mDescription;
}
int MagicPack::GetMPCost()
{
    return mMPCost;
}
bool MagicPack::IsCastFromContextMenu()
{
    return mIsCastFromContextMenu;
}
bool MagicPack::IsCastFromCorrupterMenu()
{
    return mIsCastFromCorrupterMenu;
}

///======================================= Manipulators ===========================================
void MagicPack::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void MagicPack::SetDescription(const char *pString)
{
    ResetString(mDescription, pString);
}
void MagicPack::SetScript(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void MagicPack::SetMPCost(int pCost)
{
    mMPCost = pCost;
}
void MagicPack::SetContextMenuFlags(uint8_t pCastFlags)
{
    mContextTargetFlags = pCastFlags;
    mIsCastFromContextMenu = (mContextTargetFlags != MAGIC_TARGET_NONE);
}
void MagicPack::SetCorrupterMenuFlag(bool pIsCastFromMenu)
{
    mIsCastFromCorrupterMenu = pIsCastFromMenu;
}
bool MagicPack::CanCastNow()
{
    return mCanCastNow;
}

///======================================= Core Methods ===========================================
void MagicPack::RecheckMenuCast()
{
    //--Called when the CorrupterMenu is opened, this will reverify if the spell can be cast under
    //  the player's current conditions. This is mostly handled by a script.
    mCanCastNow = false;
    if(!mIsCastFromCorrupterMenu || !mScriptPath) return;

    //--Run the script.
    xCanTargetThis = false;
    xrTargetObject = NULL;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)MAGIC_SCRIPT_MENUCANCAST);

    //--This flag can be flipped from the script to indicate the cast is legal.
    mCanCastNow = xCanTargetThis;
}
bool MagicPack::AppearsOnContextActor(Actor *pTarget)
{
    //--Returns true if the given Actor should have this spell available to target them.
    if(!pTarget || !(mContextTargetFlags & MAGIC_TARGET_ACTOR) || !mScriptPath) return false;

    //--Everything exists, store the target and run the check script.
    xCanTargetThis = false;
    xrTargetObject = pTarget;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 2, "N", (float)MAGIC_SCRIPT_CONTEXTCANTARGET, "N", (float)MAGIC_QUERY_ACTOR);
    xrTargetObject = NULL;

    //--If the script flipped this value on, the Actor can be targetted.
    return xCanTargetThis;
}
bool MagicPack::AppearsOnContextItem(InventoryItem *pTarget)
{
    //--Returns true if the given InventoryItem should have this spell available to target it.
    if(!pTarget || !(mContextTargetFlags & MAGIC_TARGET_ITEM) || !mScriptPath) return false;

    //--Everything exists, store the target and run the check script.
    xCanTargetThis = false;
    xrTargetObject = pTarget;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 2, "N", (float)MAGIC_SCRIPT_CONTEXTCANTARGET, "N", (float)MAGIC_QUERY_ITEM);
    xrTargetObject = NULL;

    //--If the script flipped this value on, the InventoryItem can be targetted.
    return xCanTargetThis;
}
bool MagicPack::AppearsOnContextContainer(WorldContainer *pTarget)
{
    //--Returns true if the given WorldContainer should have this spell available to target it.
    if(!pTarget || !(mContextTargetFlags & MAGIC_TARGET_CONTAINER) || !mScriptPath) return false;

    //--Everything exists, store the target and run the check script.
    xCanTargetThis = false;
    xrTargetObject = pTarget;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 2, "N", (float)MAGIC_SCRIPT_CONTEXTCANTARGET, "N", (float)MAGIC_QUERY_CONTAINER);
    xrTargetObject = NULL;

    //--If the script flipped this value on, the WorldContainer can be targetted.
    return xCanTargetThis;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void MagicPack::Cast(void *pTarget)
{
    //--Cast the spell! Script does most of the work. Target can legally be NULL!
    xrTargetObject = pTarget;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)MAGIC_SCRIPT_CAST);
    xrTargetObject = NULL;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
void MagicPack::ExecuteMagicCommand(void *pDataPtr)
{
    //--Expects a MagicContextPack as the execution pointer. This is used by the ContextMenu to cast
    //  spells on a given target.
    MagicContextPack *rContextPack = (MagicContextPack *)pDataPtr;
    if(!rContextPack || !rContextPack->rCallingPtr) return;

    //--Execute.
    rContextPack->rCallingPtr->Cast(rContextPack->rTargetPtr);
}

///======================================== Lua Hooking ===========================================
void MagicPack::HookToLuaState(lua_State *pLuaState)
{
    /* Magic_CreateSpell(sSpellName)
       Creates, registers, and pushes a new MagicPack. Remember to pop it when you're done. */
    lua_register(pLuaState, "Magic_CreateSpell", &Hook_Magic_CreateSpell);

    /* Magic_ClearSpells()
       Wipes all spell data. Do not call this while a spell is on the Activity Stack or the behavior
       is undefined. */
    lua_register(pLuaState, "Magic_ClearSpells", &Hook_Magic_ClearSpells);

    /* Magic_GetProperty("Name") (1 String)
       Returns the requested property from the MagicPack which is atop the Activity Stack. */
    lua_register(pLuaState, "Magic_GetProperty", &Hook_Magic_GetProperty);

    /* Magic_SetProperty("Name", sName)
       Sets the requested property in the MagicPack which is atop the Activity Stack. */
    lua_register(pLuaState, "Magic_SetProperty", &Hook_Magic_SetProperty);

    /* Magic_PushTarget()
       Pushes whatever target the magic spell is currently using. Only legal during the casting phase.
       Can legally push NULL! A spell does not need to be on the Activity Stack, this is a static call. */
    lua_register(pLuaState, "Magic_PushTarget", &Hook_Magic_PushTarget);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Magic_CreateSpell(lua_State *L)
{
    //Magic_CreateSpell(sSpellName)
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Magic_CreateSpell");

    //--Create.
    MagicPack *nPack = new MagicPack();
    nPack->SetName(lua_tostring(L, 1));

    //--Register.
    DataLibrary::Fetch()->rActiveObject = nPack;
    MagicPack::xMagicList->AddElement(lua_tostring(L, 1), nPack, &RootObject::DeleteThis);
    return 0;
}
int Hook_Magic_ClearSpells(lua_State *L)
{
    //Magic_ClearSpells()
    MagicPack::xMagicList->ClearList();
    return 0;
}
int Hook_Magic_GetProperty(lua_State *L)
{
    ///--[Argument List]
    //Magic_GetProperty("Name") (1 String)
    int tArgs = lua_gettop(L);

    ///--[Dynamic]
    //--Get the class.
    MagicPack *rMagicPack = (MagicPack *)DataLibrary::Fetch()->rActiveObject;
    if(!rMagicPack || !rMagicPack->IsOfType(POINTER_TYPE_MAGICPACK)) return LuaTypeError("Magic_GetProperty", L);

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Spell's name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushstring(L, rMagicPack->GetName());
    }
    //--Maximum number of times it can be unlocked.
    /*else if(!strcasecmp(rSwitchType, "Max Unlocks") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushinteger(L, rMagicPack->GetUnlocksMax());
    }*/
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("Magic_GetProperty", rSwitchType);
    }

    return tReturns;
}
int Hook_Magic_SetProperty(lua_State *L)
{
    ///--[Argument List]
    //Magic_SetProperty("Can Target") (Static)
    //Magic_SetProperty("Name", sName)
    //Magic_SetProperty("Description", sDescription)
    //Magic_SetProperty("Script", sScriptPath)
    //Magic_SetProperty("Cost", iCost)
    //Magic_SetProperty("Context Target Flags", uiFlags)
    //Magic_SetProperty("Cast From Menu", bIsCastFromMenu)
    int tArgs = lua_gettop(L);
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Used when targetting, switches a flag that indicates the spell can go ahead as-is.
    if(!strcasecmp(rSwitchType, "Can Target") && tArgs == 1)
    {
        MagicPack::xCanTargetThis = true;
        return 0;
    }

    ///--[Dynamic]
    //--Get the class, fail if one is not on the Activity Stack.
    MagicPack *rMagicPack = (MagicPack *)DataLibrary::Fetch()->rActiveObject;
    if(!rMagicPack || !rMagicPack->IsOfType(POINTER_TYPE_MAGICPACK)) return LuaTypeError("Magic_GetProperty", L);

    //--Change the spell's name. This does not change its list name, just its local display name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rMagicPack->SetName(lua_tostring(L, 2));
    }
    //--Description seen in the Corrupter Menu.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rMagicPack->SetDescription(lua_tostring(L, 2));
    }
    //--Which script to use when running queries.
    else if(!strcasecmp(rSwitchType, "Script") && tArgs == 2)
    {
        rMagicPack->SetScript(lua_tostring(L, 2));
    }
    //--MP cost of the spell.
    else if(!strcasecmp(rSwitchType, "Cost") && tArgs == 2)
    {
        rMagicPack->SetMPCost(lua_tointeger(L, 2));
    }
    //--Which objects can be targetted from the context menu. Should be from the MAGIC_TARGET_ACTOR series.
    else if(!strcasecmp(rSwitchType, "Context Target Flags") && tArgs == 2)
    {
        rMagicPack->SetContextMenuFlags(lua_tointeger(L, 2));
    }
    //--Whether or not the spell can be cast from the Corrupter Menu.
    else if(!strcasecmp(rSwitchType, "Cast From Menu") && tArgs == 2)
    {
        rMagicPack->SetCorrupterMenuFlag(lua_toboolean(L, 2));
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("Magic_SetProperty", rSwitchType);
    }

    return 0;
}
int Hook_Magic_PushTarget(lua_State *L)
{
    //Magic_PushTarget()
    DataLibrary::Fetch()->PushActiveEntity(MagicPack::xrTargetObject);
    return 0;
}
