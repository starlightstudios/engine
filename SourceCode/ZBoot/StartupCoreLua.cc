///=================================== Core Lua Registration =======================================
//--Registers the core classes to the lua state.

///--[ ========= CoreClasses ======== ]
#include "StarBitmap.h"
#include "StarButton.h"
#include "StarCamera2D.h"
#include "StarFont.h"
#include "DataList.h"
#include "StarLoadInterrupt.h"
#include "StarFileSystem.h"
#include "StarLinkedList.h"
#include "StarPointerSeries.h"
#include "StarObjectPrototype.h"
#include "StarlightPerlin.h"
#include "StarTranslation.h"

///--[ ======== Definitions ========= ]
#include "DebugDefinitions.h"
#include "GameGear.h"
#include "Global.h"
#include "GlDfn.h"

///--[ ========= Libraries ========== ]
#include "DataLibrary.h"

///--[ =========== Macros =========== ]
///--[ ========== Managers ========== ]
///--[External]
#include "LuaManager.h"
#include "NetworkManager.h"
#include "SaveManager.h"
#include "SteamManager.h"
#include "WorkshopItem.h"
#include "StarLumpManager.h"

///--[Game]
#include "CutsceneManager.h"
#include "EntityManager.h"
#include "MapManager.h"
#include "ResetManager.h"

///--[System]
#include "AudioPackage.h"
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"
#include "TranslationManager.h"

///======================================= Registration ===========================================
void RegisterCoreLuaFunctions(lua_State *pLuaState)
{
    ///--[Documentation]
    //--Called after the LuaManager is created, registers every function in the program to its
    //  Lua state.  Lua scripts should not execute before this.

    ///--[ ========= CoreClasses ======== ]
    StarLoadInterrupt::HookToLuaState(pLuaState);
    StarlightPerlin::HookToLuaState(pLuaState);
    StarButton::HookToLuaState(pLuaState);
    StarBitmap::HookToLuaState(pLuaState);
    StarCamera2D::HookToLuaState(pLuaState);
    StarFont::HookToLuaState(pLuaState);
    StarFileSystem::HookToLuaState(pLuaState);
    StarPointerSeries::HookToLuaState(pLuaState);
    StarObjectPrototype::HookToLuaState(pLuaState);
    StarTranslation::HookToLuaState(pLuaState);

    ///--[ ======== Definitions ========= ]
    ///--[ ========= Interfaces ========= ]
    ///--[ ========= Libraries ========== ]
    DataLibrary::HookToLuaState(pLuaState);

    ///--[ =========== Macros =========== ]
    ///--[ ========== Managers ========== ]
    AudioManager::HookToLuaState(pLuaState);
    AudioPackage::HookToLuaState(pLuaState);
    CameraManager::HookToLuaState(pLuaState);
    ControlManager::HookToLuaState(pLuaState);
    CutsceneManager::HookToLuaState(pLuaState);
    DebugManager::HookToLuaState(pLuaState);
    DisplayManager::HookToLuaState(pLuaState);
    EntityManager::HookToLuaState(pLuaState);
    MapManager::HookToLuaState(pLuaState);
    OptionsManager::HookToLuaState(pLuaState);
    ResetManager::HookToLuaState(pLuaState);
    SaveManager::HookToLuaState(pLuaState);
    SteamManager::HookToLuaState(pLuaState);
    WorkshopItem::HookToLuaState(pLuaState);
    StarLumpManager::HookToLuaState(pLuaState);
    TranslationManager::HookToLuaState(pLuaState);
}
