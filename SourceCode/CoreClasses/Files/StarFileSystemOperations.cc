//--Base
#include "StarFileSystem.h"

//--Classes
//--CoreClasses
#include "StarPath.h"

//--Definitions
//--Libraries
//--Managers

///--[Local Definitions]
#define PATHLEN 512
#define PATHLEN2 1024

void StarFileSystem::AutoFolderMaker()
{
    /*
    //--Automated folder maker, used for maps.
    char tPathBuffer[STD_PATH_LEN];
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sDebugAdventurePath");
    sprintf(tPathBuffer, "%s/Maps/RegulusBiolabs/", rAdventurePath);

    StarFileSystem *tFileSystem = new StarFileSystem();
    tFileSystem->SetRecursiveFlag(false);
    tFileSystem->ScanDirectory(tPathBuffer);

    //--Print.
    int tTotalEntries = tFileSystem->GetTotalEntries();
    for(int i = 0; i < tTotalEntries; i ++)
    {
        //--Check.
        FileInfo *rInfo = tFileSystem->GetEntry(i);
        if(!rInfo || rInfo->mIsDirectory || !rInfo->mIsFile) continue;

        //--Isolate filename.
        char *tFileName = tFileSystem->PareFileName(rInfo->mPath, false);

        //--Working directory.
        chdir(tPathBuffer);

        //--Create folder.
        char *tString = InitializeString("mkdir \"%s\"", tFileName);
        system(tString);
        free(tString);

        //--Copy.
        tString = InitializeString("copy \"%s.slf\" \"%s/MapData.slf\"", tFileName, tFileName);
        system(tString);
        free(tString);

        fprintf(stderr, "%i: %s\n", i, tString);
        fprintf(stderr, " %s\n", rInfo->mPath);
        free(tFileName);
    }

    //--Clean.
    delete tFileSystem;*/
}
void StarFileSystem::ModifySlashes(char *pOperation)
{
    ///--[Documentation]
    //--Given a string, adjusts the slashes in it for the operating system active. On Windows/DOS, this means
    //  everything is a backslash. UNIX/Bash is a forward slash.
    if(!pOperation) return;

    ///--[Execution]
    int tLen = (int)strlen(pOperation);
    for(int i = 0; i < tLen; i ++)
    {
        #if defined _TARGET_OS_WINDOWS_
            if(pOperation[i] == '/') pOperation[i] = '\\';

        #else
            if(pOperation[i] == '\\') pOperation[i] = '/';
        #endif
    }
}
void StarFileSystem::CreateDirectoriesFor(const char *pPath)
{
    ///--[Documentation]
    //--Given a file path in the format DirA/DirB/Filename.extension, creates the directory DirA and DirA/DirB/
    //  and so on across the path.
    //--This is meant to work on all operating systems.
    //--No filename actually needs to be provided, it just doesn't need to be trimmed.
    if(!pPath) return;

    ///--[Execution]
    //--Buffers.
    char tCharBuf[2];
    tCharBuf[1] = '\0';
    char tCommandBuf[PATHLEN2];
    memset(tCommandBuf, 0, sizeof(char) * PATHLEN2);
    strcpy(tCommandBuf, "mkdir \"");

    //--Variables.
    int tFirstChar = 0;
    int tLastChar = 1;

    //--Scan forwards to locate slashes.
    int tPathLen = (int)strlen(pPath);
    for(int i = 0; i < tPathLen; i ++)
    {
        //--Found a slash:
        if(pPath[i] == '\\' || pPath[i] == '/')
        {
            //--Set last char.
            tLastChar = i;

            //--Copy into the command buffer.
            for(int p = tFirstChar; p < tLastChar; p ++)
            {
                tCharBuf[0] = pPath[p];
                strcat(tCommandBuf, tCharBuf);
            }

            //--Advance the first character.
            tFirstChar = i+1;

            //--Depending on the OS, add a slash.
            #if defined _TARGET_OS_WINDOWS_
                tCharBuf[0] = '\\';
                strcat(tCommandBuf, tCharBuf);

            #else
                tCharBuf[0] = '/';
                strcat(tCommandBuf, tCharBuf);
            #endif

            //--Add a quote to the end.
            int tCommandBufSize = (int)strlen(tCommandBuf);
            tCharBuf[0] = '\"';
            strcat(tCommandBuf, tCharBuf);

            //--Command buffer now contains the next path to add.
            system(tCommandBuf);

            //--Diagnostics.
            fprintf(stderr, "%s\n", tCommandBuf);

            //--Remove that quote.
            tCommandBuf[tCommandBufSize] = '\0';
        }
    }
}
void StarFileSystem::MakeFileBackup(const char *pPath)
{
    ///--[Documentation]
    //--Given a file path, creates a backup file by taking the last six digits of the system clock and
    //  adding that to the path. This is done via system operations, so the OS does the copy action.
    if(!pPath) return;

    ///--[Error Check]
    //--If the file doesn't exist, nothing to copy.
    FILE *fCheckFile = fopen(pPath, "r");
    if(!fCheckFile)
    {
        fprintf(stderr, "StarFileSystem::MakeFileBackup() - Warning. Unable to make backup for path %s. File not found.\n", pPath);
        return;
    }
    fclose(fCheckFile);

    ///--[Path Construction]
    //--Setup.
    char tFilePath[PATHLEN];
    char tCopyPath[PATHLEN];

    //--Get time, cut off the last 6 digits.
    time_t tSeconds = time(NULL);
    int tUseTimer = tSeconds % 1000000;

    //--Create a duplicate of the path. This is needed since filesystems have preferences for forward and backslashes.
    strcpy(tFilePath, pPath);

    //--Use a StarPath to split up the string into constituents.
    StarPath *tPathParts = new StarPath();
    tPathParts->ConstructFromPath(pPath);
    const char *rDirectory = tPathParts->GetDirectory();
    const char *rFilename  = tPathParts->GetFilename();
    const char *rExtension = tPathParts->GetExtension();

    //--If no extension was found:
    if(!rExtension)
    {
        //--If no directory was found:
        if(!rDirectory)
        {
            sprintf(tCopyPath, "%s %i", rFilename, tUseTimer);
        }
        //--Has directory:
        else
        {
            sprintf(tCopyPath, "%s%s %i", rDirectory, rFilename, tUseTimer);
        }
    }
    //--Extension was found:
    else
    {
        //--If no directory was found:
        if(!rDirectory)
        {
            sprintf(tCopyPath, "%s %i%s", rFilename, tUseTimer, rExtension);
        }
        //--Has directory:
        else
        {
            sprintf(tCopyPath, "%s%s %i%s", rDirectory, rFilename, tUseTimer, rExtension);
        }
    }

    ///--[Perform Copy Action]
    //--In Windows, all slashes have to be backslashes.
    #if defined _TARGET_OS_WINDOWS_
        for(int i = 0; i < PATHLEN; i ++)
        {
            if(tFilePath[i] == '/') tFilePath[i] = '\\';
            if(tCopyPath[i] == '/') tCopyPath[i] = '\\';
        }

        //--Construct the command.
        char tBuf[PATHLEN2];
        sprintf(tBuf, "copy \"%s\" \"%s\"", tFilePath, tCopyPath);
        system(tBuf);

    //--On other operating systems, make all slashes forward slashes.
    #else
        for(int i = 0; i < PATHLEN; i ++)
        {
            if(tFilePath[i] == '\\') tFilePath[i] = '/';
            if(tCopyPath[i] == '\\') tCopyPath[i] = '/';
        }

        //--Construct the command.
        char tBuf[PATHLEN2];
        sprintf(tBuf, "cp \"%s\" \"%s\"", tFilePath, tCopyPath);
        system(tBuf);

    #endif
    ///--[Clean]
    delete tPathParts;
}
