//--[LayerLump]
//--Lump describing a layer. Typically consists of one or more tilesets plus a list of tile indexes.

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "RootLump.h"
#include <QVariantMap>

namespace Tiled {
class Layer;
class TileLayer;
class Tile;
class GidMapper;
class ObjectGroup;
class MapObject;
class ImageLayer;
}

//--[Local Structures]
typedef struct
{
    char *mName;
    char *mType;
    float mX;
    float mY;
    float mW;
    float mH;

    int mTileID;

    int mPropertiesTotal;
    char ***mProperties;

}LocalObjectData;

class LayerLump : public RootLump
{
    private:
    //--System
    //--Type
    char mLayerType[32];

    //--Common Data
    char mLayerName[80];
    int mPropertiesTotal;
    char ***mProperties;

    //--Tile Data
    int mX, mY, mW, mH;
    int16_t **mTileData;
    int16_t **mFlipData;

    //--Object Data
    int mObjectsTotal;
    LocalObjectData *mObjectData;

    //--Image Data
    char *mImageLumpName;

    public:
    //--System
    LayerLump();
    virtual ~LayerLump();

    //--Public Variables

    //--Property Queries
    char *GetName();
    char *GetImagePath();

    //--Manipulators
    //--Core Methods
    int GetUniversalID(Tiled::TileLayer *pLayer, int pX, int pY);
    int GetFlippingID(Tiled::TileLayer *pLayer, int pX, int pY);
    static char ***PopulateProperties(int &sPropertiesTotal, QVariantMap pProperties);
    void PopulateTileData(Tiled::TileLayer *pLayer);
    void PopulateObjectData(Tiled::ObjectGroup *pObjectLayer);
    void PopulateImageData(Tiled::ImageLayer *pImageLayer);

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);
    static void WriteProperties(int pPropertiesTotal, char ***pProperties, uint64_t &sCounter, FILE *pOutfile);
    void WriteTileData(uint64_t &sCounter, FILE *pOutfile);
    void WriteObjectData(uint64_t &sCounter, FILE *pOutfile);
    void WriteImageData(uint64_t &sCounter, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

