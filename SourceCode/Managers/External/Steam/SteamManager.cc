//--Base
#include "SteamManager.h"

//--Classes
#include "CSteamStats.h"
#include "WorkshopItem.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLinkedList.h"

///--[Debug]
//#define STEAM_DEBUG
#ifdef STEAM_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
SteamManager::SteamManager()
{
    ///--[Steam Manager]
    //--By default, the Steam Manager has no variables and none of its functions do anything. However,
    //  the define _STEAM_API_ causes the headers to be included. The manager also requires activation
    //  by scripts or it is disabled.
    #if defined _STEAM_API_

    //--System
    mIsSteamRunning = false;
    mTickOfLastStatRequest = -1000;
    mHasReceivedCurrentStats = false;
    mSteamStats = NULL;

    //--Steam Achievement Storage
    mAchievementsTotal = 0;
    mAchievements = NULL;

    //--Workshop Items
    mWorkshopItems = new StarLinkedList(true);
    #endif
}
SteamManager::~SteamManager()
{
    #if defined _STEAM_API_
    for(int i = 0; i < mAchievementsTotal; i ++)
    {
        mAchievements[i].FreeMemory();
    }
    free(mAchievements);
    delete mWorkshopItems;
    SteamAPI_Shutdown();
    #endif
}
#if defined _STEAM_API_
extern "C" void __cdecl SteamAPIDebugTextHook(int pSeverity, const char *pDebugText)
{
	fprintf(stderr, pDebugText);
}
#endif
void SteamManager::Boot(int pGameID)
{
    ///--[Documentation and Setup]
    //--Boot sequence. Does not occur automatically. If not booted, all of the manager's function calls
    //  never do anything.
    //--The Game ID is the internal index, not the steam ID itself.
    #if defined _STEAM_API_
    if(mIsSteamRunning) return;
    DebugPush(true, "Booting steam manager. Index: %i\n", pGameID);

    ///--[Resolve App ID]
    //--The internal code for the game references a constant code that was set when the game was first
    //  registered on Steam.
    if(pGameID == STEAM_GAME_INDEX_STRING_TYRANT)
    {
        xSteamAppID = STRING_TYRANT_APPID;
    }
    else if(pGameID == STEAM_GAME_INDEX_STRING_TYRANT_DEMO)
    {
        xSteamAppID = STRING_TYRANT_DEMO_APPID;
    }
    else if(pGameID == STEAM_GAME_INDEX_PANDEMONIUM)
    {
        xSteamAppID = PANDEMONIUM_APPID;
    }
    else if(pGameID == STEAM_GAME_INDEX_WITCHHUNTERIZANA)
    {
        xSteamAppID = WITCHHUNTERIZANA_APPID;
    }

    //--Diagnostics.
    DebugPrint("Steam App ID is %i\n", xSteamAppID);

    ///--[Boot Steam]
    //--Activate.
    SteamErrMsg errMsg = { 0 };
    if(SteamAPI_InitFlat(&errMsg) != k_ESteamAPIInitResult_OK)
    {
        mIsSteamRunning = false;
        DebugPop("Steam is not running, Steam functionality disabled.\n");
        return;
    }

    //--Success.
    mIsSteamRunning = true;
    SteamAPI_ISteamClient_SetWarningMessageHook(SteamClient(), &SteamAPIDebugTextHook);
	//SteamClient()->SetWarningMessageHook( &SteamAPIDebugTextHook );

    //--Start manual callback handling.
    #if defined STEAM_MANUAL_CALLBACKS
    SteamAPI_ManualDispatch_Init();
    #endif

    //--Request player statistics.
    RequestStats();
    DebugPop("Finished booting steam manager.\n");
    #endif
}

///--[Public Variables]
int SteamManager::xSteamAppID = PANDEMONIUM_APPID;

///===================================== Property Queries =========================================
bool SteamManager::IsSteamRunning()
{
    #if defined _STEAM_API_
    return mIsSteamRunning;
    #endif
    return false;
}

///======================================== Manipulators ==========================================
void SteamManager::RegisterWorkshopItem(WorkshopItem *pItem)
{
    #if defined _STEAM_API_
    mWorkshopItems->AddElement("X", pItem, &WorkshopItem::DeleteThis);
    #endif
}

///======================================== Core Methods ==========================================
void SteamManager::AllocateAchievements(int pTotal)
{
    #if defined _STEAM_API_
    //--Deallocate.
    DebugPush(false, "Allocating achievements.\n");
    for(int i = 0; i < mAchievementsTotal; i ++)
    {
        mAchievements[i].FreeMemory();
    }
    free(mAchievements);

    //--Reset.
    mAchievementsTotal = 0;
    mAchievements = NULL;
    if(pTotal < 1)
    {
        DebugPop("Achievements count was zero.\n");
        return;
    }

    //--Allocate.
    mAchievementsTotal = pTotal;
    mAchievements = (SteamAchievement *)starmemoryalloc(sizeof(SteamAchievement) * pTotal);
    for(int i = 0; i < mAchievementsTotal; i ++)
    {
        mAchievements[i].Initialize();
    }
    DebugPop("Finished allocating achievements.\n");
    #endif
}
void SteamManager::SetAchievement(int pIndex, const char *pName)
{
    #if defined _STEAM_API_
    DebugPush(false, "Setting achievement %i %s\n", pIndex, pName);
    if(pIndex < 0 || pIndex >= mAchievementsTotal)
    {
        DebugPop("Out of range.\n");
        return;
    }
    ResetString(mAchievements[pIndex].mName, pName);
    DebugPop("Set achievement correctly.\n");
    #endif
}
void SteamManager::UnlockAchievement(int pIndex)
{
    #if defined _STEAM_API_
    //--Range check.
    DebugPush(false, "Unlocking achievement in slot %i\n", pIndex);
    if(pIndex < 0 || pIndex >= mAchievementsTotal)
    {
        DebugPop("Out of range.\n");
        return;
    }

    //--Backend not ready.
	if(SteamUserStats() == NULL || SteamUser() == NULL)
    {
        DebugPop("Steam user stats not available.\n");
        return;
    }

    //--Achievement already unlocked.
    DebugPrint("Checking if already unlocked.\n");
    if(mAchievements[pIndex].mIsAchieved && false)
    {
        DebugPop("Already unlocked.\n");
        return;
    }

    //--Unlock it!
    DebugPrint("Unlocking.\n");
    mAchievements[pIndex].mIsAchieved = true;
    SteamAPI_ISteamUserStats_SetAchievement(SteamUserStats(), mAchievements[pIndex].mName);
    SteamAPI_ISteamUserStats_StoreStats(SteamUserStats());
    //SteamUserStats()->SetAchievement(mAchievements[pIndex].mName);
    //SteamUserStats()->StoreStats();
    DebugPop("Finished unlocking.\n");
    #endif
}
void SteamManager::UnlockAchievementS(const char *pName)
{
    //--Unlocks an achievement given its name, rather than its index.
    #if defined _STEAM_API_
    DebugPush(false, "Unlocking achievement %s\n", pName);

    //--Backend not ready.
	if(SteamUserStats() == NULL || SteamUser() == NULL)
    {
        DebugPop("Steam user stats not available.\n");
        return;
    }

    //--Make sure the achievement actually exists.
    DebugPrint("Verifying existence.\n");
    int tAchievementIndex = -1;
    for(int i = 0; i < mAchievementsTotal; i ++)
    {
        if(!strcasecmp(pName, mAchievements[i].mName))
        {
            DebugPrint("Found in slot %i\n", i);
            tAchievementIndex = i;
            break;
        }
    }

    //--Not found, stop here.
    if(tAchievementIndex == -1)
    {
        DebugPop("Achievement not found.\n");
        return;
    }

    //--Call the index routine.
    DebugPop("Calling slot unlock.\n");
    UnlockAchievement(tAchievementIndex);

    #endif
}
bool SteamManager::RequestStats()
{
    //--Requests player stats and achievements. Should be called after Steam boots or if the stats
    //  need to be refreshed, such as after unlocking something.
    //--Returns false if the request was not sent for any reason.
    #if defined _STEAM_API_
    if(mHasReceivedCurrentStats || !mIsSteamRunning)
    {
        return false;
    }

    //--If it has been less than 10 seconds since the last request was sent, don't send one.
    int tCurrentTick = (int)Global::Shared()->gTicksElapsed;
    if(mTickOfLastStatRequest + 600 >= tCurrentTick)
    {
        return false;
    }

    //--Store when the request was sent.
    DebugPush(false, "Requesting stats.\n");
    mTickOfLastStatRequest = tCurrentTick;

    //--Debug.
	DebugPrint("==> Called request stats on tick %i.\n", tCurrentTick);

	//--Is Steam loaded? If not we can't get stats.
	ISteamUser *rSteamUser = SteamUser();
	ISteamUserStats *rSteamUserStats = SteamUserStats();
	if(rSteamUserStats == NULL || rSteamUser == NULL)
	{
        DebugPop("Steam user stats not available.\n");
		return false;
	}
	//--Is the user logged on? If not we can't get stats.
	if(!SteamAPI_ISteamUser_BLoggedOn(rSteamUser))
	{
        DebugPop("Steam user is not logged on.\n");
		return false;
	}

	//--Request user stats. This will fire a callback when they arrive.
	DebugPop("Issued request.\n");
	return true;//SteamUserStats()->RequestCurrentStats();
    #endif
    return false;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
#define STEAM_MANUAL_CALLBACKS
void SteamManager::PumpCallbackStack()
{
    ///--[Documentation]
    #if defined _STEAM_API_
    //--At the end of a logic tick, checks for callbacks and runs them in the order they were received.
    if(!mIsSteamRunning) return;
	//if(SteamUserStats() == NULL || SteamUser() == NULL) return;

	///--[Basic Callbacks]
	//--In basic callback mode, just run the API's function and it does the rest.
	#ifndef STEAM_MANUAL_CALLBACKS
    SteamAPI_RunCallbacks();
	#else
	///--[Callback Stack]
    //--In manual mode, an event handler loop needs to be run to handle all the accumulated callbacks.
	HSteamPipe tSteamPipe = SteamAPI_GetHSteamPipe();

	//--Run dispatch routine.
	SteamAPI_ManualDispatch_RunFrame(tSteamPipe);

	//--Iterate across all callbacks.
	CallbackMsg_t tCallback;
	while(SteamAPI_ManualDispatch_GetNextCallback(tSteamPipe, &tCallback))
	{
        DebugPush(true, "Pumped callback stack, got something.\n");

	    //--This is a dispatching API callback that is completed.
	    if(tCallback.m_iCallback == SteamAPICallCompleted_t::k_iCallback)
        {
            //--Debug.
            DebugPrint("API callback.\n");

            //--Allocate space for the callback result.
			SteamAPICallCompleted_t *rCallCompleted = (SteamAPICallCompleted_t *)tCallback.m_pubParam;
			void *tTmpCallResult = malloc(tCallback.m_cubParam);

            //--Populate with data, find out if the API result failed.
            bool tFailed = false;
            if(SteamAPI_ManualDispatch_GetAPICallResult(tSteamPipe, rCallCompleted->m_hAsyncCall, tTmpCallResult, tCallback.m_cubParam, tCallback.m_iCallback, &tFailed))
            {
                //--Diagnostics.
                fprintf(stderr, "Received a manually dispatched API call result.\n");
                fprintf(stderr, " Size of the Call Completed object: %i vs %i\n", tCallback.m_cubParam, sizeof(SteamAPICallCompleted_t));
                fprintf(stderr, " Failed state is: %i\n", tFailed);
                fprintf(stderr, " Async Call ID: %lu\n", rCallCompleted->m_hAsyncCall);
                fprintf(stderr, " Size of call result: %i\n", tCallback.m_cubParam);
                fprintf(stderr, " Expected Callback: %i\n", tCallback.m_iCallback);
                fprintf(stderr, " Create Item Result: %i\n", CreateItemResult_t::k_iCallback);
                fprintf(stderr, " Sizes are %i  %i\n", tCallback.m_cubParam, sizeof(CreateItemResult_t));

                //--Scan workshop items looking for a match:
                WorkshopItem *rCheckItem = (WorkshopItem *)mWorkshopItems->PushIterator();
                while(rCheckItem)
                {
                    if(rCheckItem->mHandle == rCallCompleted->m_hAsyncCall)
                    {
                        fprintf(stderr, " Found a match in the async call.\n");
                        fprintf(stderr, "  Object pointer: %p\n", rCheckItem);
                        fprintf(stderr, "  Object handle: %lu\n", rCheckItem->mHandle);
                        rCheckItem->OnItemCreated((CreateItemResult_t *)tTmpCallResult, false);
                        mWorkshopItems->PopIterator();
                        break;
                    }

                    rCheckItem = (WorkshopItem *)mWorkshopItems->AutoIterate();
                }
            }
            free(tTmpCallResult);
        }
        //--Received user stats.
        else if(tCallback.m_iCallback == UserStatsReceived_t::k_iCallback)
        {
            mHasReceivedCurrentStats = true;
            DebugPrint("Received user stats on tick %i.\n", Global::Shared()->gTicksElapsed);
            DebugPrint("There are %i achievements.\n", (int)SteamUserStats()->GetNumAchievements());

            //--Iterate across our achievement list and toggle them.
            for(int i = 0; i < mAchievementsTotal; i ++)
            {
                bool tGotAchievement = SteamUserStats()->GetAchievement(mAchievements[i].mName, &mAchievements[i].mIsAchieved);
                if(tGotAchievement)
                {
                    DebugPrint("Achievement %s found. Status %i.\n", mAchievements[i].mName, mAchievements[i].mIsAchieved);
                }
                else
                {
                    DebugPrint("Achievement %s not found in backend.\n", mAchievements[i].mName);
                }
            }
        }
        //--Friends status changed.
        else if(tCallback.m_iCallback == PersonaStateChange_t::k_iCallback)
        {
            DebugPrint("Changed friends status.\n");
        }
        //--Favourites status changed.
        else if(tCallback.m_iCallback == FavoritesListChanged_t::k_iCallback)
        {
            DebugPrint("Changed favourites status.\n");
        }
        //--Other type of callback.
        else
        {
            //--Ignore these callbacks. They have to do with the focus changing.
            if(tCallback.m_iCallback == 715 || tCallback.m_iCallback == 1040015 || tCallback.m_iCallback == 1040044)
            {

            }
            //--Print.
            else
            {
                DebugPrint("Received callback of type %i\n", tCallback.m_iCallback);
            }
        }

		//--Clean up. This *must* be called before the next iteration since SteamAPI_ManualDispatch_GetNextCallback() procures memory.
		SteamAPI_ManualDispatch_FreeLastCallback(tSteamPipe);
        DebugPop("Deallocated callback.\n");
	}
    #endif
    #endif
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
SteamManager *SteamManager::Fetch()
{
    return Global::Shared()->gSteamManager;
}

///======================================== Lua Hooking ===========================================
void SteamManager::HookToLuaState(lua_State *pLuaState)
{
    /* Steam_IsSteamGame()
       Returns true if Steam is defined in this executable, false if not. Note that Steam does not
       necessarily need to be active, just defined. */
    lua_register(pLuaState, "Steam_IsSteamGame", &Hook_Steam_IsSteamGame);

    /* Steam_Activate(iGameIndex)
       Activates the steam overlay for the given game index. The indices are internal. */
    lua_register(pLuaState, "Steam_Activate", &Hook_Steam_Activate);

    /* Steam_AllocateAchievements(iTotal)
       Allocates the given number of achievements. These are client-sided. */
    lua_register(pLuaState, "Steam_AllocateAchievements", &Hook_Steam_AllocateAchievements);

    /* Steam_SetAchievement(iIndex, sName)
       Sets the name of the achievement. This is the same name as the API name on the Steampowered page. */
    lua_register(pLuaState, "Steam_SetAchievement", &Hook_Steam_SetAchievement);

    /* Steam_UnlockAchievement(iIndex)
       Marks the achievement in the index as unlocked. */
    lua_register(pLuaState, "Steam_UnlockAchievement", &Hook_Steam_UnlockAchievement);

}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_Steam_IsSteamGame(lua_State *L)
{
    //Steam_IsSteamGame() (1 Boolean)
    #if defined _STEAM_API_
    lua_pushboolean(L, true);
    return 1;

    #else
    lua_pushboolean(L, false);
    return 1;
    #endif
}
int Hook_Steam_Activate(lua_State *L)
{
    //Steam_Activate(iGameIndex)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Steam_Activate");

    SteamManager::Fetch()->Boot(lua_tointeger(L, 1));
    return 0;
}
int Hook_Steam_AllocateAchievements(lua_State *L)
{
    //Steam_AllocateAchievements(iTotal)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Steam_AllocateAchievements");

    SteamManager::Fetch()->AllocateAchievements(lua_tointeger(L, 1));
    return 0;
}
int Hook_Steam_SetAchievement(lua_State *L)
{
    //Steam_SetAchievement(iIndex, sName)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("Steam_SetAchievement");

    SteamManager::Fetch()->SetAchievement(lua_tointeger(L, 1), lua_tostring(L, 2));
    return 0;
}
int Hook_Steam_UnlockAchievement(lua_State *L)
{
    //Steam_UnlockAchievement(iIndex)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Steam_UnlockAchievement");

    SteamManager::Fetch()->UnlockAchievement(lua_tointeger(L, 1));
    return 0;
}
