-- |[ ======================================== Mod Paths ======================================= ]|
--After all other games have registered their paths, mods for Runes of Pandemonium are assembled.
-- These may be inside the Adventure directory, or may be inside adjacent directories.

-- |[Debug]|
gbModDetectDebug = false
if(gbModDetectDebug) then io.write("Scanning for mods in game directories.\n") end

-- |[Existence Check]|
--Don't bother running mod checks if Adventure Mode isn't registered.
if(DL_Exists("Root/Paths/System/Startup/sAdventurePath") == false) then
    if(gbModDetectDebug) then io.write(" Adventure Mode not found. Stopping mod scanning.\n") end
    return
end

--Get the path.
local sAdventureRoot = VM_GetVar("Root/Paths/System/Startup/sAdventurePath", "S")
if(gbModDetectDebug) then io.write(" Base adventure directory is: " .. sAdventureRoot .. "\n") end

-- |[Setup]|
--Flags.
gbIsStartingChapter = false --Informs the mod booters not to do any setup yet.
local iModsDetected = 0

--Global list of all mods.
gsModDirectories = {}
gsModNames = {}

-- |[ ====================== Manual Addition ===================== ]|
--On OSX, I couldn't get physfx to work. So known mods are manually added here.
if(LM_GetSystemOS() == "OSX") then
    
    --Debug.
    if(gbModDetectDebug) then io.write(" Running manual additions for OSX.\n") end
    
    --Run.
    if(FS_Exists(sAdventureRoot .. "Chapter M/System/000 Mod Setup.lua") == true) then
        LM_ExecuteScript(sAdventureRoot .. "Chapter M/System/000 Mod Setup.lua")
    end
    if(FS_Exists(sAdventureRoot .. "Chapter C/System/000 Mod Setup.lua") == true) then
        LM_ExecuteScript(sAdventureRoot .. "Chapter C/System/000 Mod Setup.lua")
    end
    if(FS_Exists(sAdventureRoot .. "Chapter A/System/000 Mod Setup.lua") == true) then
        LM_ExecuteScript(sAdventureRoot .. "Chapter A/System/000 Mod Setup.lua")
    end
    if(FS_Exists(sAdventureRoot .. "Chapter H/System/000 Mod Setup.lua") == true) then
        LM_ExecuteScript(sAdventureRoot .. "Chapter H/System/000 Mod Setup.lua")
    end
end

-- |[ =================== Filesystem Addition ==================== ]|
--Scan all of the folders in the game directory. Any folder that is not expected to exist is checked
-- for a specific file. If that file is found, then that directory is registered as a mod.
if(gbModDetectDebug) then io.write(" Running filesystem additions.\n") end

--List of all folders that are expected to be in the folder.
local saAcceptedList = {"Audio", "Chapter 0", "Chapter 1", "Chapter 2", "Chapter 3", "Chapter 4", "Chapter 5", "Chapter 6", "ChapterSelectDialogue", "Combat", "CostumeHandlers", "Datafiles", 
                        "Field Abilities", "Fonts", "FormHandlers", "Items", "LayeredTrackProfiles", "Load Handler", "Maps", "Minigames", "RestingDialogues", "Save Handler", "Subroutines", 
                        "System", "Topics", "ZRouting.lua", "..", "XGameInfo.lua", "YMenuLaunch.lua", "ZLaunch.lua"}
                    
--Run the loop.
FS_Open(sAdventureRoot, false)
local sFolderPath = FS_Iterate()
while(sFolderPath ~= "NULL") do
    
    --Resolve the name of the folder.
    local sFolderName = fnResolveFolderName(sFolderPath)
    
    --Debug
    if(false) then
        io.write("Folder Path: "  .. sFolderPath .. "\n")
        io.write(" Folder Name: " .. sFolderName .. "\n")
    end
    
    --Scan the folder name versus the "accepted" folder names.
    local bIsAcceptedName = false
    for i = 1, #saAcceptedList, 1 do
        if(sFolderName == saAcceptedList[i]) then
            bIsAcceptedName = true
            break
        end
    end
    
    --Name was not in the accepted list. Check if the file: modpath/System/000 Mod Setup.lua exists.
    -- If so, run it. The mod will then need to add itself to the list of mods.
    if(bIsAcceptedName == false) then
        
        --Add a detected mod directory.
        iModsDetected = iModsDetected + 1
        
        --If the file exists, execute it.
        if(FS_Exists(sFolderPath .. "System/000 Mod Setup.lua") == true) then
            LM_ExecuteScript(sFolderPath .. "System/000 Mod Setup.lua")
        end
    end
    
    --Next.
    sFolderPath = FS_Iterate()
end
FS_Close()

-- |[ ==================== Developer Addition ==================== ]|
--Run this file if it exists. It will build non-standard modpaths.
if(FS_Exists("DeveloperLocal/Modpaths.lua") == true) then
    if(gbModDetectDebug) then io.write(" Running developer additions.\n") end
    LM_ExecuteScript("DeveloperLocal/Modpaths.lua")
end

iModsDetected = #gsModDirectories

-- |[ ====================== Print All Mods ====================== ]|
if(gbModDetectDebug or true) then
    io.write("== Mod Report ==\n")
    io.write(" Detected " .. iModsDetected .. " mod directories.\n")
    io.write(" There are " .. #gsModDirectories .. " mods in the final tally.\n")
    io.write(" Listing mods: \n")
    for i = 1, #gsModDirectories, 1 do
        io.write("  " .. gsModDirectories[i] .. "\n")
    end
end

-- |[ ========================= Finish Up ======================== ]|
if(gbModDetectDebug) then io.write(" Finished scanning mods.\n") end
gbModDetectDebug = nil
