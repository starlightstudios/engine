install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @loader_path/osxbin/libSDL2-2.0.0.dylib StarlightEngineSDLOSXSteam
install_name_tool -change /usr/local/opt/lua/lib/liblua.5.2.dylib @loader_path/osxbin/liblua.5.2.4.dylib StarlightEngineSDLOSXSteam
install_name_tool -change /usr/local/opt/boost/lib/libboost_filesystem.dylib @loader_path/osxbin/libboost_filesystem.dylib StarlightEngineSDLOSXSteam
install_name_tool -change @loader_path/libbass.dylib @loader_path/osxbin/libbass.dylib StarlightEngineSDLOSXSteam
install_name_tool -change @loader_path/libbass_fx.dylib @loader_path/osxbin/libbass_fx.dylib StarlightEngineSDLOSXSteam
install_name_tool -change @loader_path/libsteam_api.dylib @loader_path/osxbin/libsteam_api.dylib StarlightEngineSDLOSXSteam