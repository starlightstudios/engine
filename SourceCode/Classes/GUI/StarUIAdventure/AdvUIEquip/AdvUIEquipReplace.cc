//--Base
#include "AdvUIEquip.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///===================================== Replacement Mode =========================================
//--Handles update when the player has selected a piece of equipment or a gem to replace. This creates
//  a list in the inventory of viable replacements, which the player then picks from or cancels.

///======================================= Core Methods ===========================================
bool AdvUIEquip::ActivateReplacement()
{
    ///--[Documentation]
    //--Activates replacements mode, usually by selecting a piece of equipment on the selection screen.
    //  This builds a list of viable replacements and sets flags.
    //--Returns true if it succeeded, false if replacements mode could not be activated, such as an
    //  item not being removable or there being no replacements.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();

    //--An active entity must exist, otherwise, stop.
    AdvCombatEntity *rActiveEntity = rAdvCombat->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) return false;

    ///--[Build Replacement List]
    //--First, run the subroutine to build a list of replacements.
    const char *rSlotName = rActiveEntity->GetNameOfEquipmentSlot(mEquipmentCursor);
    BuildSlotList(mCharacterCursor, rSlotName);

    //--If the list of replacements came back as zero, fail.
    if(mValidReplacementsList->GetListSize() < 1) return false;

    ///--[Success]
    //--Otherwise, at least one replacement exists. Switch to replacement mode.
    mIsReplacing = true;
    mReplacementCursor = 0;
    mReplacementSkip = 0;

    //--Report success to caller.
    return true;
}
void AdvUIEquip::ReresolveReplaceDetailsItem()
{
    ///--[Documentation]
    //--If in details mode, this function resolves which item is under the cursor and being inspected.
    //  If the player changes the item to an invalid slot, details mode exits.
    //--If not in details mode, does nothing.
    if(!mIsShowingDetails) return;

    ///--[Equipment]
    //--If the replacement list is NULL, stop.
    if(!mValidReplacementsList) { mIsShowingDetails = false; rDetailsItem = NULL; return; }

    //--Get the piece of equipment currently highlighted. If it does not exist, exit details mode.
    AdventureItem *rSelectedItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
    if(!rSelectedItem) { mIsShowingDetails = false; rDetailsItem = NULL; return; }

    //--If the selected item is the "Unequip" entry, it never renders details and has no gem slots.
    if(rSelectedItem == AdventureInventory::xrDummyUnequipItem) { mIsShowingDetails = false; rDetailsItem = NULL; return; }

    //--The selected item is a valid details entry.
    rDetailsItem = rSelectedItem;
}

///========================================== Update ==============================================
void AdvUIEquip::UpdateReplacement()
{
    ///--[Documentation]
    //--When the player is selecting which replacement to place in the selected slot, this routine
    //  handles input and logic.
    //--Controls generally return out after executing, to prevent odd cases like accidentally switching
    //  items and pressing activate to activate the wrong item.

    ///--[Setup]
    //--Fast-access pointers. The active entity must exist or a major error has occurred.
    AudioManager *rAudioManager = AudioManager::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterCursor);
    if(!rActiveEntity) return;

    //--The replacements list must be valid. If it isn't, immediately exit replacements mode.
    if(!mValidReplacementsList) { mIsReplacing = false; RecomputeCursorPositions(); return; }

    //--Get the item under the selection cursor. It does not need to exist unless gem selection is taking place.
    AdventureItem *rEquippedItem = (AdventureItem *)rActiveEntity->GetEquipmentBySlotI(mEquipmentCursor);
    if(mGemCursor != -1 && !rEquippedItem) { mIsReplacing = false; RecomputeCursorPositions(); return; }

    //--Get the item under the replacement cursor. This should always exist, but it may be an "Unequip" item.
    //  It can also legally be a gem if gem replacement is occurring.
    AdventureItem *rReplaceItem = (AdventureItem *)mValidReplacementsList->GetElementBySlot(mReplacementCursor);
    if(!rReplaceItem) { mIsReplacing = false; RecomputeCursorPositions(); return; }

    ///--[Details Handler]
    //--Immediately toggles details mode on whatever is highlighted. Stops the rest of the update.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--If currently in details mode, exit details mode.
        if(mIsShowingDetails)
        {
            mIsShowingDetails = false;
            rAudioManager->PlaySound("Menu|Select");
            return;
        }

        //--Run routine to check if there is a details item. If nothing is under the cursor, this will
        //  automatically unset details mode.
        mIsShowingDetails = true;
        ReresolveReplaceDetailsItem();

        //--If details mode did not get unset by the routine, play the select sound.
        if(mIsShowingDetails)
        {
            rAudioManager->PlaySound("Menu|Select");
        }
        //--It failed, so play a failure sound.
        else
        {
            rAudioManager->PlaySound("Menu|Failed");
        }
        return;
    }

    ///--[Moving Up and Down]
    //--Up decrements the replacement cursor. It wraps around to the list length. Unlike base equipment selection,
    //  list scrolling is possible if there are enough replacement entries.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Speed increase. Scroll 10 entries when Ctrl is held.
        int tIterations = -1;
        if(rControlManager->IsDown("Ctrl")) tIterations = -10;

        //--Run routine.
        if(StandardCursor(mReplacementCursor, mReplacementSkip, tIterations, mValidReplacementsList->GetListSize(), cReplaceEdgeBuf, cReplaceEntriesPerPage, true))
        {
            ReresolveReplaceDetailsItem();
            RecomputeCursorPositions();
        }

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }
    //--Down increments the replacement cursor. It wraps around to zero.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Speed increase. Scroll 10 entries when Ctrl is held.
        int tIterations = 1;
        if(rControlManager->IsDown("Ctrl")) tIterations = 10;

        //--Run routine.
        if(StandardCursor(mReplacementCursor, mReplacementSkip, tIterations, mValidReplacementsList->GetListSize(), cReplaceEdgeBuf, cReplaceEntriesPerPage, true))
        {
            ReresolveReplaceDetailsItem();
            RecomputeCursorPositions();
        }

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Activate]
    //--Swaps the slotted item with the replacement list item.
    if(rControlManager->IsFirstPress("Activate"))
    {
        ///--[Equipment]
        //--Player is replacing a piece of equipment, not a gem.
        if(mGemCursor == -1)
        {
            //--Get the name of the currently selected equipment slot. Some items can be equipped in multiple slots
            //  such as "Accessory A" and "Accessory B" so the item's type does not mandate what slot it goes in.
            const char *rSlotName = rActiveEntity->GetNameOfEquipmentSlot(mEquipmentCursor);

            //--Ask the entity to equip the item. If true is returned, the item was equipped.
            if(rActiveEntity->EquipItemToSlot(rSlotName, rReplaceItem))
            {
                //--Order the inventory to drop its pointer to the item.
                AdventureInventory::Fetch()->LiberateItemP(rReplaceItem);

                //--Check the last registered item, which will be the item unequipped. If it exists, remove any gems from it.
                //  If space is available, attempt to move those gems into the new item. If the new item does not have enough
                //  space for all of the gems, the last ones get dumped into the inventory.
                //--Note: We must store rLastReggedItem in a local variable. As soon as a gem is removed, it becomes the last
                //  registered item pointer in the inventory.
                AdventureItem *rLastReggedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
                if(rLastReggedItem)
                {
                    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
                    {
                        AdventureItem *rPrevGem = rLastReggedItem->RemoveGemFromSlot(i);
                        if(rPrevGem)
                        {
                            AdventureInventory::Fetch()->RegisterItem(rPrevGem);
                        }
                    }
                }

                //--Recompute statistics to handle the new equipment and any gem changes.
                rReplaceItem->ComputeStatistics();
                rActiveEntity->ComputeStatistics();
            }
        }
        ///--[Gem]
        //--Player is replacing a gem slotted in a piece of equipment.
        else
        {
            //--Alias to improve readability, as we know this is a gem now.
            AdventureItem *rNewGem = rReplaceItem;

            //--If this is the unequip item, handle that:
            if(rNewGem == AdventureInventory::xrDummyUnequipItem)
            {
                //--When unequipped, the pointer to the item is returned. Register it to the inventory.
                AdventureItem *rReceivedGem = rEquippedItem->RemoveGemFromSlot(mGemCursor);
                if(rReceivedGem && !rEquippedItem->mGemErrorFlag) AdventureInventory::Fetch()->RegisterItem(rReceivedGem);

                //--Order the equipment item and character to recompute bonuses.
                rEquippedItem->ComputeStatistics();
                rActiveEntity->ComputeStatistics();
            }
            //--It's a gem. Replace it.
            else
            {
                //--Swap the gems in and out.
                AdventureItem *rReceivedGem = rEquippedItem->PlaceGemInSlot(mGemCursor, rNewGem);

                //--Register/Unregister the gems as necessary.
                if(!rEquippedItem->mGemErrorFlag)
                {
                    AdventureInventory::Fetch()->RegisterItem(rReceivedGem);
                    AdventureInventory::Fetch()->LiberateItemP(rNewGem);
                }

                //--Order the equipment item and character to recompute bonuses.
                rEquippedItem->ComputeStatistics();
                rActiveEntity->ComputeStatistics();
            }
        }

        ///--[Common]
        //--SFX.
        AudioManager::Fetch()->PlaySound("World|TakeItem");

        //--Clear the equipment list.
        mValidReplacementsList->ClearList();

        //--Exit replacement mode.
        mIsReplacing = false;
        RecomputeCursorPositions();

        //--Order the calling object to handle the close code.
        SetCloseCode("Change Equipment");
    }

    ///--[Cancel]
    //--Unsets replacement move.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Exit replacements mode.
        mIsReplacing = false;

        //--Recompute cursor.
        RecomputeCursorPositions();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
