//--Base
#include "DisplayManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

//--Dummy functions used when the wrangler is unable to resolve a function. These prevent a NULL-dereference crash
//  on very old machines, but the functionality is still not there. There is a mirror function here for every
//  function which is resolved through the wrangler.
//--If flagged, the mirror functions will spit error output to the console. Otherwise they do nothing.
void DisplayManager::dsglAttachShader(GLuint, GLuint)
{

}
void DisplayManager::dsglActiveTexture(GLenum)
{

}
void DisplayManager::dsglBindBuffer(GLenum, GLuint)
{

}
void DisplayManager::dsglBindFramebuffer(GLenum, GLuint)
{

}
void DisplayManager::dsglBindVertexArray(GLuint)
{

}
void DisplayManager::dsglBlendFuncSeparate(GLenum pSrc, GLenum pDst, GLenum, GLenum)
{
    glBlendFunc(pSrc, pDst);
}
void DisplayManager::dsglBufferData(GLenum, GLsizeiptr, const GLvoid *, GLenum)
{

}
void DisplayManager::dsglBlendEquation(GLenum)
{

}
GLenum DisplayManager::dsglCheckFramebufferStatus(GLenum)
{
    return 0;
}
void DisplayManager::dsglClientActiveTexture(GLenum)
{

}
void DisplayManager::dsglCompileShader(GLuint)
{

}
void DisplayManager::dsglCompressedTexImage2D(GLenum, GLint, GLenum, GLsizei, GLsizei, GLint, GLsizei, const GLvoid *)
{

}
GLuint DisplayManager::dsglCreateProgram()
{
    return 0;
}
GLuint DisplayManager::dsglCreateShader(GLenum)
{
    return 0;
}
void DisplayManager::dsglDeleteBuffers(GLsizei, const GLuint *)
{

}
void DisplayManager::dsglDeleteProgram(GLuint)
{

}
void DisplayManager::dsglDeleteShader(GLuint)
{

}
void DisplayManager::dsglDeleteVertexArrays(GLsizei, const GLuint *)
{

}
void DisplayManager::dsglDeleteFramebuffers(GLsizei, const GLuint *)
{

}
void DisplayManager::dsglDrawBuffers(GLsizei, const GLenum *)
{

}
void DisplayManager::dsglEnableVertexAttribArray(GLuint)
{

}
void DisplayManager::dsglFramebufferTexture(GLenum, GLenum, GLuint, GLint)
{

}
void DisplayManager::dsglGenerateMipmap(GLenum)
{

}
void DisplayManager::dsglGenBuffers(GLsizei, GLuint *)
{

}
void DisplayManager::dsglGenFramebuffers(GLsizei, GLuint *)
{

}
void DisplayManager::dsglGenVertexArrays(GLsizei, GLuint *)
{

}
void DisplayManager::dsglGetShaderiv(GLuint, GLenum, GLint *)
{

}
void DisplayManager::dsglGetShaderInfoLog(GLuint, GLsizei, GLsizei *, GLchar *)
{

}
void DisplayManager::dsglGetProgramiv(GLuint, GLenum, GLint *)
{

}
void DisplayManager::dsglGetProgramInfoLog(GLuint, GLsizei, GLsizei *, GLchar *)
{

}
GLint DisplayManager::dsglGetUniformLocation(GLuint, const GLchar *)
{
    return 0;
}
void DisplayManager::dsglLinkProgram(GLuint)
{

}
void DisplayManager::dsglMultiTexCoord2f(GLenum, GLfloat, GLfloat)
{

}
void DisplayManager::dsglMultiTexCoord3f(GLenum, GLfloat, GLfloat, GLfloat)
{

}
void DisplayManager::dsglShaderSource(GLuint, GLsizei, const GLchar **, const GLint *)
{

}
void DisplayManager::dswglSwapIntervalEXT(GLint)
{

}
void DisplayManager::dsglUseProgram(GLuint)
{

}
void DisplayManager::dsglUniform4f(GLint, GLfloat, GLfloat, GLfloat, GLfloat)
{

}
void DisplayManager::dsglUniform1i(GLint, GLint)
{

}
void DisplayManager::dsglVertexAttribPointer(GLuint, GLint, GLenum, GLboolean, GLsizei, const GLvoid *)
{

}
