///====================================== AdventureLight ==========================================
//--Represents a light in AdventureMode. Lights are areas of the map that render brighter than others
//  using various blending techniques. They are stored in the AdventureLevel and can be various sizes
//  and intensities. Some lights can be attached to entities through a UniqueID.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define ADLIT_MODE_RADIAL 0
#define ADLIT_MODE_SQUARERADIAL 1

///========================================== Classes =============================================
class AdventureLight : public RootObject
{
    private:
    //--System
    bool mIsEnabled;
    char *mLocalName;
    int mMode;
    bool mIsSkipped;
    bool mNeedsToUpload;

    //--Common Properties
    float mXPosition;
    float mYPosition;
    float mIntensity;
    StarlightColor mLightColor;

    //--Square-Radial Properties
    float mWidth;
    float mHeight;

    //--Attaching
    uint32_t mAttachedID;

    protected:

    public:
    //--System
    AdventureLight();
    virtual ~AdventureLight();

    //--Public Variables
    //--Property Queries
    const char *GetName();
    uint32_t GetAttachedID();

    //--Manipulators
    void Enable();
    void Disable();
    void SetName(const char *pName);
    void SetPosition(float pX, float pY);
    void SetRadial(float pIntensity);
    void SetSquareRadial(float pWidth, float pHeight, float pIntensity);
    void SetColor(float pRed, float pBlu, float pGrn, float pAlp);
    void SetColor(StarlightColor pColor);
    void AttachToEntity(uint32_t pID);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void Render();
    void HandleDataChecking(int &sLightsIndex, float pScale, TwoDimensionReal pCameraDim, bool pAlwaysUpload);
    void UploadDataToShader(int &sLightsIndex, float pScale);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

