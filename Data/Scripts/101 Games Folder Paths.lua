-- |[ =================================== Games Folder Paths =================================== ]|
--At program boot, checks every folder in the "Engine/Games/" directory. If a matching file is found
-- in that directory, executes that file to detect it as a game.

-- |[Diagnostics]|
local bDiagnostics = fnAutoCheckDebugFlag("Startup: Populate Paths")
Debug_PushPrint(bDiagnostics, "Startup: Game Folder Paths.\n")

--Exceptions. Any directory found on this list will not be executed.
local saExceptionsList = {}

-- |[Iteration]|
--Open the directory.
FS_Open("Games/", false)
local sPath = FS_Iterate()
    
    --Iterate across the paths.
	while(sPath ~= "NULL") do

        --Setup.
        local bBypass = false
        local sFolderName = fnResolveFolderName(sPath)

        --Check if the string is on the exclusion list:
        for i = 1, #saExceptionsList, 1 do
            if(string.lower(sFolderName) == saExceptionsList[i]) then
                bBypass = true
                break
            end
        end

		--Check the last byte for '/'. This indicates it's a directory. Ignore files.
		local iLength = string.len(sPath)
		if(string.byte(sPath, iLength) ~= string.byte("/")) then
            bBypass = true
		end

        --If not bypassing, execute.
        if(bBypass == false) then
            if(FS_Exists(sPath .. "XGameInfo.lua") == true) then
				Debug_Print("Executing path for: " .. sPath .. "XGameInfo.lua" .. "\n")
                LM_ExecuteScript(sPath .. "XGameInfo.lua")
            end
        end

        --Next.
		sPath = FS_Iterate()
    end
    
--Clean up.
FS_Close()

--Diagnostics.
Debug_PopPrint("Game Folder Paths completes.\n")