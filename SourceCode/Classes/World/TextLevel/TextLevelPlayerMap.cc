//--Base
#include "TextLevel.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"

///--[Worker Function]
//--Sorts the list by priority.
int CompareMapEntriesByPriority(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() below.
    StarLinkedListEntry **rEntryA = (StarLinkedListEntry **)pEntryA;
    StarLinkedListEntry **rEntryB = (StarLinkedListEntry **)pEntryB;

    //--Cast.
    TextLevelPlayerMapPack *rPackA = (TextLevelPlayerMapPack *)(*rEntryA)->rData;
    TextLevelPlayerMapPack *rPackB = (TextLevelPlayerMapPack *)(*rEntryB)->rData;

    //--Compare.
    return (rPackA->mPriority - rPackB->mPriority);
}

///--[Normal Functions]
void TextLevel::RegisterPlayerMapLayer(const char *pName, int pPriority, const char *pImage)
{
    //--Registers a new map layer. It will be placed in the list according to the given priority.
    if(!pName || !pImage) return;

    //--Basics.
    SetMemoryData(__FILE__, __LINE__);
    TextLevelPlayerMapPack *nPackage = (TextLevelPlayerMapPack *)starmemoryalloc(sizeof(TextLevelPlayerMapPack));
    nPackage->Initialize();
    nPackage->mPriority = pPriority;
    nPackage->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImage);
    mPlayerMapLayers->AddElementAsTail(pName, nPackage, &FreeThis);

    //--Sort the list by priority.
    mPlayerMapLayers->SortListUsing(&CompareMapEntriesByPriority);
}
void TextLevel::SetPlayerMapLayerVisible(const char *pName, bool pIsVisible)
{
    if(!pName) return;
    TextLevelPlayerMapPack *rPackage = (TextLevelPlayerMapPack *)mPlayerMapLayers->GetElementByName(pName);
    if(rPackage) rPackage->mIsActive = pIsVisible;
}
void TextLevel::ActivatePlayerMap()
{
    mPlayerMapActive = true;
}
void TextLevel::DeactivatePlayerMap()
{
    mPlayerMapActive = false;
}
bool TextLevel::UpdatePlayerMap(bool pDisallowControls)
{
    ///--[Documentation and Setup]
    //--Handles timers and input when the player is looking at their map. Returns true if it is blocking
    //  input, false if it's just running timers.
    //--Note that we return the original value. This is because the flag does not change if something
    //  else handled the update and we're just running timers.

    ///--[Timer]
    //--Visible, fade in.
    if(mPlayerMapActive)
    {
        if(mPlayerMapTimer < TL_PLAYERMAP_VIS_TICKS) mPlayerMapTimer ++;
    }
    //--Invisible, fade out.
    else
    {
        if(mPlayerMapTimer > 0) mPlayerMapTimer --;
        return pDisallowControls;
    }

    ///--[Input]
    //--Press the escape key to close. That's the only control.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--If pDisallowControls is true, then something else already handled controls.
    if(pDisallowControls) return pDisallowControls;

    //--First, pressing escape. Hides the map, stops update.
    if(rControlManager->IsFirstPress("Escape"))
    {
        mPlayerMapActive = false;
        return true;
    }

    //--If we got this far, indicate we're handling input.
    return true;
}
void TextLevel::RenderPlayerMap()
{
    ///--[Documentation and Setup]
    //--Renders the player's map layers to the screen.
    if(mPlayerMapTimer < 1) return;

    ///--[Compute Alpha]
    //--The map fades in and out with a timer.
    float cAlpha = EasingFunction::QuadraticInOut(mPlayerMapTimer, TL_PLAYERMAP_VIS_TICKS);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Layer Render]
    TextLevelPlayerMapPack *rPackage = (TextLevelPlayerMapPack *)mPlayerMapLayers->PushIterator();
    while(rPackage)
    {
        //--Skip inactivate layers.
        if(!rPackage->mIsActive || !rPackage->rImage)
        {
            rPackage = (TextLevelPlayerMapPack *)mPlayerMapLayers->AutoIterate();
            continue;
        }

        //--Render.
        rPackage->rImage->Draw();

        //--Next.
        rPackage = (TextLevelPlayerMapPack *)mPlayerMapLayers->AutoIterate();
    }

    //--Clean up.
    StarlightColor::ClearMixer();
}
