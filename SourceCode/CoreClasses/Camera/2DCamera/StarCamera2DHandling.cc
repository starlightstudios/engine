//--Base
#include "StarCamera2D.h"

//--Classes
#include "PlayerPony.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

float StarCamera2D::Get2DScale()
{
    return m2DScale;
}
void StarCamera2D::SetDimensions(int pWidth, int pHeight)
{
    //--Adjusts the width and height of the screen visible to the Camera.
    mCoordinates.mRgt = mCoordinates.mLft + pWidth;
    mCoordinates.mBot = mCoordinates.mTop + pHeight;
}
void StarCamera2D::SetBoundaries(TwoDimensionReal pBoundaries)
{
    memcpy(&mBoundaries, &pBoundaries, sizeof(TwoDimensionReal));
}
void StarCamera2D::SetBoundaries(int pLft, int pTop, int pRgt, int pBot)
{
    //--Prevent the camera from scrolling past these points
    mBoundaries.mLft = (float)pLft;
    mBoundaries.mTop = (float)pTop;
    mBoundaries.mRgt = (float)pRgt;
    mBoundaries.mBot = (float)pBot;
}
void StarCamera2D::Move(int pXDist, int pYDist)
{
    //--Moves the Camera a given X and Y distance iteratively, checking each iteration to see if
    //  it has exceeded its boundaries.

    //--Temporary Variables
    int tXStore = pXDist;
    int tYStore = pYDist;
    bool mIsDone = false;
    int mTickBlocker = mMaxMovesPerTick;
    ThreeDimensionReal tNewCoords;
    memcpy(&tNewCoords, &mCoordinates, sizeof(ThreeDimensionReal));

    //--Are we inside a pusher?  If so, push the Camera's speeds.
    if(rCurrentPusher)
    {
        if(rCurrentPusher->mIdealLft != -1.0f)
        {
            pXDist = rCurrentPusher->mIdealLft - mCoordinates.mLft;
        }
        if(rCurrentPusher->mIdealTop != -1.0f)
        {
            pYDist = rCurrentPusher->mIdealTop - mCoordinates.mTop;
            if(pYDist < -3) pYDist = -3;
            if(pYDist >  3) pYDist =  3;
        }
    }

    while(!mIsDone && mTickBlocker > 0)
    {
        //--[Horizontal checker]
        //--Move Left
        if(pXDist > 0)
        {
            tNewCoords.mLft = tNewCoords.mLft + 1.0f;
            tNewCoords.mRgt = tNewCoords.mRgt + 1.0f;
            tNewCoords.mXCenter = tNewCoords.mXCenter + 1.0f;
            pXDist --;
        }
        //--Move Right
        else if(pXDist < 0)
        {
            tNewCoords.mLft = tNewCoords.mLft - 1.0f;
            tNewCoords.mRgt = tNewCoords.mRgt - 1.0f;
            tNewCoords.mXCenter = tNewCoords.mXCenter - 1.0f;
            pXDist ++;
        }

        //--Is the new position valid?  If so, copy it onto the Camera's
        //  permanant copy.  If not, discard the changes.
        if(IsPositionValid(tNewCoords, tXStore, 0) && IsPolyPositionValid(tNewCoords))
        {
            memcpy(&mCoordinates, &tNewCoords, sizeof(ThreeDimensionReal));
        }
        else
        {
            memcpy(&tNewCoords, &mCoordinates, sizeof(ThreeDimensionReal));
        }


        //--[Vertical checker]
        //--Move Up
        if(pYDist > 0)
        {
            tNewCoords.mTop     = tNewCoords.mTop + 1.0f;
            tNewCoords.mBot     = tNewCoords.mBot + 1.0f;
            tNewCoords.mYCenter = tNewCoords.mYCenter + 1.0f;
            pYDist --;
        }
        //--Move Down
        else if(pYDist < 0)
        {
            tNewCoords.mTop     = tNewCoords.mTop - 1.0f;
            tNewCoords.mBot     = tNewCoords.mBot - 1.0f;
            tNewCoords.mYCenter = tNewCoords.mYCenter - 1.0f;
            pYDist ++;
        }

        //--Is the new position valid?  If so, copy it onto the Camera's
        //  permanant copy.  If not, discard the changes.
        if(IsPositionValid(tNewCoords, 0, tYStore) && IsPolyPositionValid(tNewCoords))
        {
            memcpy(&mCoordinates, &tNewCoords, sizeof(ThreeDimensionReal));
        }
        else
        {
            memcpy(&tNewCoords, &mCoordinates, sizeof(ThreeDimensionReal));
        }

        //--If the distances have converged to zero, the Camera has reached its
        //  end point.  Otherwise the loop will make 100 passes per tick.
        if(pXDist == 0 && pYDist == 0) mIsDone = true;
        mTickBlocker --;
    }

    //--Prevent overscroll
    //LockToBoundaries();

    //--Remove the pusher
    rCurrentPusher = NULL;
}
void StarCamera2D::CenterOnPlayer()
{
    //--Asks the player where to focus. Expects the top/left to be passed back.
    float tLft = 0.0f;
    float tTop = 0.0f;
    PlayerPony::Fetch()->HandleCamera(tLft, tTop);

    //fprintf(stderr, "Target: %f %f - ", tLft, tTop);
    Move(tLft - mCoordinates.mLft, tTop - mCoordinates.mTop);
    //fprintf(stderr, "%f %f\n", mCoordinates.mLft, mCoordinates.mTop);
}
void StarCamera2D::CenterOnBox(int pLft, int pTop, int pRgt, int pBot)
{
    //--Overload of CenterOnBox which doesn't lock the scale or ignore any values.
    CenterOnBox(pLft, pTop, pRgt, pBot, false, false, false);
}
void StarCamera2D::CenterOnBox(int pLft, int pTop, int pRgt, int pBot, bool pLockScale, bool pIgnoreX, bool pIgnoreY)
{
    //--Attemps to center on the provided box boundaries.  Ideally, the box will be the same
    //  dimensions as the camera, but if not, then the scale will modify itself until it is.

    //--Adjust the scale.  Note that the scale will not instantly SNAP to a new scale, it will
    //  change in increments.  It also uses the XScale if the two would require different values.
    float tCurrentScale = MapManager::Fetch()->GetScaleFactor();
    if(!pLockScale)
    {
        float tIncrement = 0.10f;
        //float tIdealXSize = pRgt - pLft + 1.0f;
        float tIdealScale = (Global::Shared()->gWindowWidth / VIRTUAL_CANVAS_X);

        //--Calculate the change of scale in increments.
        float tNewScale = tIdealScale;
        if(tNewScale < tCurrentScale - tIncrement)
        {
            tNewScale = tNewScale + tIncrement;
        }
        else if(tNewScale > tCurrentScale + tIncrement)
        {
            tNewScale = tNewScale - tIncrement;
        }
        else
        {
            tNewScale = tIdealScale;
        }

        //--Push that change to the MapManager.
        MapManager::Fetch()->SetScaleFactor(tNewScale);
        tCurrentScale = tNewScale;
    }

    //--Original function is deprecated.
    CenterOnPosition(0, 0);
}
void StarCamera2D::CenterOnPosition(int pXTarget, int pYTarget)
{
    //--Moves the Camera such that the center of the camera is the provided target.  The camera can
    //  only move at its speed, but that's handled by the Move() function.

    //--Acquire position data.
    float tXDif, tYDif;
    ///Formula:  Ideal Position = (Target Center) - (Camera Center);

    ///Camera Move Formula: Distance to move X|Y = (Player's X|Y coordinate) -
    ///(Camera's upper left corner's X|Y point + (width|height of window/2));
    float tScale = MapManager::Fetch()->GetScaleFactor();
    SetDimensions(VIRTUAL_CANVAS_X / tScale, VIRTUAL_CANVAS_Y / tScale);

    //--Calc ideal position of the Camera.
    tXDif = ((pXTarget)) - (mCoordinates.mXCenter);
    tYDif = ((pYTarget)) - (mCoordinates.mYCenter);
    //fprintf(stderr, "Target %i %i - %f %f - %f %f\n", pXTarget, pYTarget, mCoordinates.mXCenter, mCoordinates.mYCenter, tXDif, tYDif);

    Move(tXDif, tYDif);
}
void StarCamera2D::CenterOnMouse()
{
    //--Doesn't literally center on the mouse, rather, moves the camera's center point according
    //  to the movement of the mouse cursor. This requires right-click to be held down, otherwise
    //  the values default to all zeroes.

    //--Get mouse data.
    float tMouseX, tMouseY, tMouseZ;
    ControlManager::Fetch()->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);
    ControlState *rLeftMouseState = ControlManager::Fetch()->GetControlState("MouseLft");
    ControlState *rRightMouseState = ControlManager::Fetch()->GetControlState("MouseRgt");

    //--Always record the mouse's position. This prevents sudden jerking motions when we right
    //  click to move the camera.
    float tDifX = tMouseX - mOldMouseX;
    float tDifY = tMouseY - mOldMouseY;
    mOldMouseX = tMouseX;
    mOldMouseY = tMouseY;

    //--Reset this flag every tick.
    mUsePositiveCamera = mOldCameraFlag;
    mIs2DPolygonMode = mOldPolygonFlag;

    //--If the left mouse is down, but the right is not down, center on the player.
    if(rLeftMouseState->mIsDown && !rRightMouseState->mIsDown)
    {
        CenterOnPlayer();
        return;
    }

    //--If both left and right are down, ignore boundaries and edges. Releasing left or right
    //  will re-enable this flag.
    if(rLeftMouseState->mIsDown && rRightMouseState->mIsDown)
    {
        mOldCameraFlag = mUsePositiveCamera;
        mOldPolygonFlag = mIs2DPolygonMode;
        mUsePositiveCamera = false;
        mIs2DPolygonMode = false;
    }

    //--If the mouse is not down, the camera does not move.
    if(!rRightMouseState->mIsDown) return;

    //--Allow the camera to scroll faster than normal.
    int tOldMaxMoves = mMaxMovesPerTick;
    mMaxMovesPerTick = 100000;

    //--The mouse is down, therefore the amount to move is equal to the distance the mouse moved.
    const float cSensitivity = 1.5f;
    Move(tDifX * cSensitivity * m2DScale, tDifY * cSensitivity * m2DScale);

    //--Clean up.
    mMaxMovesPerTick = tOldMaxMoves;
}
void StarCamera2D::LockToBoundaries()
{
    //--Automatically prevents the Camera from moving past its boundaries on any side.

    //--Left
    float mWidth = mCoordinates.mRgt - mCoordinates.mLft;
    float mHeight = mCoordinates.mBot - mCoordinates.mTop;
    if(mCoordinates.mLft < mBoundaries.mLft)
    {
        mCoordinates.mLft = mBoundaries.mLft;
        mCoordinates.mRgt = mCoordinates.mLft + mWidth;
    }
    //--Right
    if(mCoordinates.mRgt > mBoundaries.mRgt)
    {
        mCoordinates.mRgt = mBoundaries.mRgt;
        mCoordinates.mLft = mCoordinates.mRgt - mWidth;
    }
    //--Above
    if(mCoordinates.mTop < mBoundaries.mTop)
    {
        mCoordinates.mTop = mBoundaries.mTop;
        mCoordinates.mBot = mCoordinates.mTop + mHeight;
    }
    //--Below
    if(mCoordinates.mBot > mBoundaries.mBot)
    {
        mCoordinates.mBot = mBoundaries.mBot;
        mCoordinates.mTop = mCoordinates.mBot - mHeight;
    }

    mCoordinates.mXCenter = (mCoordinates.mLft + mCoordinates.mRgt) / 2.0f;
    mCoordinates.mYCenter = (mCoordinates.mTop + mCoordinates.mBot) / 2.0f;
}
void StarCamera2D::Translate2D()
{
    //--Calls glTranslate to move to the Camera's current position.
    glTranslatef(-mCoordinates.mLft, -mCoordinates.mTop, 0.0f);
}
void StarCamera2D::Untranslate2D()
{
    //--Calls glTranslate to move to the Camera's current position.
    glTranslatef(mCoordinates.mLft, mCoordinates.mTop, 0.0f);
}
void StarCamera2D::ForceTo(int pLft, int pTop)
{
    //--Forcibly relocates the Camera.  Use this only for debug.
    float tWidth = mCoordinates.mRgt - mCoordinates.mLft;
    float tHeight = mCoordinates.mBot - mCoordinates.mTop;
    mCoordinates.mLft = pLft;
    mCoordinates.mTop = pTop;
    mCoordinates.mRgt = pLft + tWidth;
    mCoordinates.mBot = pTop + tHeight;
    mCoordinates.mXCenter = (mCoordinates.mLft + mCoordinates.mRgt) / 2.0f;
    mCoordinates.mYCenter = (mCoordinates.mTop + mCoordinates.mBot) / 2.0f;
    fprintf(stderr, "Forced camera to %i %i\n", pLft, pTop);
}
