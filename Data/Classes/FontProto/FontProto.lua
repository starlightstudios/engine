-- |[ ================================== FontProto Class File ================================== ]|
--Class file for FontProto. Call this once at initialization.

--Stores the information necessary to create a StarFont in the C++ code.

-- |[ =========== Globals/Statics ============ ]|
-- |[Global List]|
--Global list that stores all FontProto objects.
gzaFontProtoList = {}

-- |[Constants]|
gciFontNoFlags    = 0
gciFontNearest    = Font_GetProperty("Constant Precache With Nearest")
gciFontEdge       = Font_GetProperty("Constant Precache With Edge")
gciFontDownfade   = Font_GetProperty("Constant Precache With Downfade")
gciFontSpecialS   = Font_GetProperty("Constant Precache With Special S")
gciFontSpecialExc = Font_GetProperty("Constant Precache With Special !")
gciFontUnicode    = Font_GetProperty("Constant Precache Full Unicode")
gciFontDummy      = Font_GetProperty("Constant Precache Dummy")
gciFontSpecialDbg = Font_GetProperty("Constant Precache Debug")

-- |[Special Flag Defaults]|
gcfFont_Downfade_None   =  0.0
gcfFont_Outline_Default =  1.0
gcfFont_Monospace_None  = -1.0

-- |[Compiling Font Flags]|
gcbNoFontCompiling = true
gciFont_NoCompilation = 0
gciFont_CompileNow = 1
gciFont_UseCompiled = 2

-- |[Diagnostic Variables]|
Debug_RegisterFlag("Fonts: All", false)
Debug_RegisterFlag("Fonts: Timing", true)

-- |[ ============ Class Members ============= ]|

-- |[System]|
FontProto = {}
FontProto.__index = FontProto
FontProto.bIsLoaded = false
FontProto.sLocalName = "Default"
FontProto.sFilepath = "Null"
FontProto.iTypeSize = 18
FontProto.iFlags = 0
FontProto.sPrecompilePath = "Null"

-- |[Kerning]|
FontProto.sKerningPath = "Data/Classes/FontProto/Z No Kerning.lua"

-- |[Special Flags]|
FontProto.fDownfadeStart = 0.0
FontProto.fDownfadeEnd = 0.0
FontProto.iOutlineWid = 1
FontProto.iMonospacing = -1
        
-- |[Lookups]|
--FontProto.zaLookups = {}

-- |[ ============ Class Methods ============= ]|
--System
function FontProto:new(psName, psFilepath, piTypesize, piFlags) end

--List Handling
function FontProto:fnClearGlobalList()              end
function FontProto:fnRegisterPrototype(pzPrototype) end
function FontProto:fnLocatePrototype(psName)        end

--Modifiers
function FontProto:fnSetTypesize(piSize)
    self.iTypeSize = piSize
end
function FontProto:fnSetFlags(piFlags)
    self.iFlags = piFlags
end
function FontProto:fnSetPrecompilePath(psPath)
    self.sPrecompilePath = psPath
end
function FontProto:fnSetKerningPath(psPath)
    if(psPath == nil or psPath == "Null") then
        self.sKerningPath = "Data/Classes/FontProto/Z No Kerning.lua"
    else
        self.sKerningPath = psPath
    end
end
function FontProto:fnSetDownfades(pfStart, pfEnd)
    self.fDownfadeStart = pfStart
    self.fDownfadeEnd = pfEnd
end
function FontProto:fnSetOutline(piOutlineWidth)
    self.iOutlineWid = piOutlineWidth
end
function FontProto:fnSetMonospacing(piMonospacing)
    self.iMonospacing = piMonospacing
end

function FontProto:fnSetSpecialsToDefault()
    self.fDownfadeStart = gcfFont_Downfade_None
    self.fDownfadeEnd   = gcfFont_Downfade_None
    self.iOutlineWid    = gcfFont_Outline_Default
    self.iMonospacing   = gcfFont_Monospace_None
end

function FontProto:fnLoad(pbPrecompilingDisabled)
    
    -- |[Argument Check]|
    if(pbPrecompilingDisabled == nil) then pbPrecompilingDisabled = false end
    
    -- |[Repeat Check]|
    --If the font is already loaded, don't do anything. The user may order font loading in several places.
    if(self.bIsLoaded == true) then return end
    
    -- |[Diagnostics]|
    local bDiagnostics = Debug_GetFlag("Fonts: All")
    Debug_PushPrint(bDiagnostics, "Creating font from prototype.\n")
    Debug_Print("Name: " .. self.sLocalName .. "\n")
    Debug_Print("Filepath: " .. self.sFilepath .. "\n")
    
    -- |[Error Checking]|
    --Fail if the name is still "Default".
    if(self.sLocalName == "Default") then
        Debug_ForcePrint("FontProto:fnLoad() - Failed, font name is still 'Default'.\n")
        Debug_PopPrint("")
        return
    end
    
    --Fail if the filepath is "Null".
    if(self.sFilepath == "Null") then
        Debug_ForcePrint("FontProto:fnLoad() - Failed, font path is 'Null'.\n")
        Debug_PopPrint("")
        return
    end
    
    -- |[Font Compilation]|
    --If the precompilation path is "Null", then we don't need to do anything with compiling. Otherwise, we
    -- need to check 
    Debug_Print("Checking precompilation.\n")
    local bIsCompiling = false
    local bCompileFontsIfNeeded = OM_GetOption("Compile Fonts If Needed")
    local bDisallowCompilation  = OM_GetOption("Disallow Font Compilation")
    if(self.sPrecompilePath ~= "Null" and bDisallowCompilation == false and pbPrecompilingDisabled == false) then

        --Check if the file path for the font is occupied. If it is available, use it.
        if(FS_Exists(self.sPrecompilePath) == true) then
            Font_SetProperty("Monospacing", self.iMonospacing)
            StarFont_OpenCompiledFile(self.sPrecompilePath)
            Debug_Print("Using an existing precompiled font.\n")

        --If the file doesn't exist, begin compilation.
        elseif(bCompileFontsIfNeeded == true) then
            bIsCompiling = true
            StarFont_BeginCompilation(self.sPrecompilePath)
            Debug_Print("Beginning compilation.\n")
        end
    else
        Debug_Print("Precompilation not set.\n")
    end

    -- |[Construction]|
    --Set special flags.
    Font_SetProperty("Downfade", self.fDownfadeStart, self.fDownfadeEnd)
    Font_SetProperty("Outline Width", self.iOutlineWid)
    Font_SetProperty("Monospacing", self.iMonospacing)
    
    --Register the font with the C++ state.
    Debug_Print("Loading font data.\n")
    Font_Register(self.sLocalName, self.sFilepath, self.sKerningPath, self.iTypeSize, self.iFlags)
    
    --Clear special flags back to their defaults for the next font.
    Debug_Print("Setting flags.\n")
    self:fnSetSpecialsToDefault()
    
    --Set this flag so the font doesn't load twice.
    self.bIsLoaded = true
    
    -- |[Post Compilation]|
    --If set, finish compilation here, or unset the use of compiled files for the next font.
    if(self.sPrecompilePath ~= "Null" and bDisallowCompilation == false and pbPrecompilingDisabled == false) then

        --Not compiling, unset.
        if(bIsCompiling == false) then
            StarFont_OpenCompiledFile("Null")
            Debug_Print("Resetting compiled data.\n")

        --Currently compiling, finish.
        elseif(bCompileFontsIfNeeded == true) then
            StarFont_FinishCompilation()
            Debug_Print("Finishing compilation.\n")
        end
    end
    
    -- |[Diagnostics]|
    Debug_PopPrint("Finished font handling normally.\n")
end

-- |[ ========== Class Constructor =========== ]|
--Must have a unique name for the map in question.
function FontProto:new(psName, psFilepath, piTypesize, piFlags)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    if(psName     == nil) then psName     = "Default" end
    if(psFilepath == nil) then psFilepath = "Null"    end
    if(piTypesize == nil) then piTypesize = 18        end
    if(piFlags    == nil) then piFlags    =  0        end
    
    -- |[ ======= Duplicate ======== ]|
    --Check if there is already a prototype named this. If so, print a warning and return the existing prototype.
    local zExistingProto = FontProto:fnLocatePrototype(psName)
    if(zExistingProto ~= nil) then
        io.write("Warning: Font Prototype " .. psName .. " already exists.\n")
        return zExistingProto
    end
    
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, FontProto)
    
    -- |[ ======== Members ========= ]|
    --Local members.
    zObject.sLocalName = psName
    zObject.sFilepath  = psFilepath
    zObject.iTypeSize  = piTypesize
    zObject.iFlags     = piFlags
    
    --Reset local lists so they don't use the global class listings.
    --zObject.zaWarpDestinations = {}
    
    -- |[ ======= Finish Up ======== ]|
    --Register to the global list.
    FontProto:fnRegisterPrototype(zObject)
    
    --Return object.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "FontProtoList.lua")
