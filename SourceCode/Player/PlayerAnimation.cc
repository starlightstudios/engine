//--Base
#include "PlayerPony.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DisplayManager.h"

//=========================================== Drawing =============================================
void PlayerPony::Render()
{
    //--Setup
    DisplayManager::StdModelPush();

    //--Entry point for player rendering.
    glTranslatef(0.0f, 0.0f, mDepth);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Position
    //glTranslatef(mCoordinates.mLft, mCoordinates.mTop, 0.0f);

    //--Insert player rendering here.

    //--Clean.
    DisplayManager::StdModelPop();
}
