-- |[ ================================== Sanchez 35pt Kerning ================================== ]|
--Sets the kerning values for the listed font.

-- |[Main Scaler]|
--This sets the standard distance between letters.
StarFont_SetKerning(1.0)

-- |[Letter Groupings]|
--The value -1 indicates "All Letters". Use this for thin/thick letters.
StarFont_SetKerning(string.byte("i"), -1, 3.0)
StarFont_SetKerning(-1, string.byte("i"), 3.0)

--Punctuation groupings.
StarFont_SetKerning(-1, string.byte(":"), 2.0)
StarFont_SetKerning(-1, string.byte("!"), 3.0)
StarFont_SetKerning(-1, string.byte("`"), 3.0)
StarFont_SetKerning(-1, string.byte("["), 4.0)
StarFont_SetKerning(-1, string.byte("]"), 6.0)
StarFont_SetKerning(-1, string.byte("'"), 6.0)
StarFont_SetKerning(-1, string.byte(";"), 6.0)
StarFont_SetKerning(-1, string.byte("|"), 6.0)
StarFont_SetKerning(-1, string.byte("\\"), 6.0)
StarFont_SetKerning(-1, string.byte("-"), 6.0)
StarFont_SetKerning(-1, string.byte("="), 6.0)
StarFont_SetKerning(-1, string.byte(":"), 6.0)
StarFont_SetKerning(-1, string.byte("\""), 6.0)
StarFont_SetKerning(-1, string.byte("."), 6.0)
StarFont_SetKerning(-1, string.byte("?"), 6.0)
StarFont_SetKerning(-1, string.byte("<"), 6.0)
StarFont_SetKerning(-1, string.byte(">"), 6.0)

-- |[Punctuation-to-Punctuation]|
StarFont_SetKerning(string.byte("."), string.byte("."), 3.0)

-- |[Punctuation-to-Letter]|
StarFont_SetKerning(string.byte(";"), string.byte("L"), 6.0)
StarFont_SetKerning(string.byte("-"), string.byte("C"), 6.0)
StarFont_SetKerning(string.byte(" "), string.byte("i"), 12.0)

-- |[Numbers-To-Anything]|
--StarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

-- |[Letters-to-Letter]|
StarFont_SetKerning(string.byte("b"), string.byte("a"), 3.0)
StarFont_SetKerning(string.byte("c"), string.byte("e"), 3.0)
StarFont_SetKerning(string.byte("c"), string.byte("h"), 3.0)
StarFont_SetKerning(string.byte("c"), string.byte("k"), 4.0)
StarFont_SetKerning(string.byte("C"), string.byte("h"), 2.0)
StarFont_SetKerning(string.byte("e"), string.byte("a"), 2.0)
StarFont_SetKerning(string.byte("e"), string.byte("c"), 3.0)
StarFont_SetKerning(string.byte("e"), string.byte("p"), 3.0)
StarFont_SetKerning(string.byte("e"), string.byte("r"), 2.0)
StarFont_SetKerning(string.byte("e"), string.byte("s"), 2.0)
StarFont_SetKerning(string.byte("h"), string.byte("a"), 3.0)
StarFont_SetKerning(string.byte("h"), string.byte("e"), 2.0)
StarFont_SetKerning(string.byte("l"), string.byte("y"), 4.0) 
StarFont_SetKerning(string.byte("l"), string.byte("a"), 3.0) 
StarFont_SetKerning(string.byte("L"), string.byte("o"), 4.0) 
StarFont_SetKerning(string.byte("n"), string.byte("c"), 2.0)
StarFont_SetKerning(string.byte("n"), string.byte("t"), 2.0)
StarFont_SetKerning(string.byte("n"), string.byte("n"), 2.0)
StarFont_SetKerning(string.byte("o"), string.byte("c"), 3.0)
StarFont_SetKerning(string.byte("o"), string.byte("g"), 3.0)
StarFont_SetKerning(string.byte("o"), string.byte("l"), 2.0)
StarFont_SetKerning(string.byte("o"), string.byte("n"), 2.0)
StarFont_SetKerning(string.byte("o"), string.byte("w"), 2.0)
StarFont_SetKerning(string.byte("p"), string.byte("t"), 3.0)
StarFont_SetKerning(string.byte("P"), string.byte("r"), 2.0)
StarFont_SetKerning(string.byte("r"), string.byte("e"), 3.0)
StarFont_SetKerning(string.byte("r"), string.byte("i"), 3.0)
StarFont_SetKerning(string.byte("r"), string.byte("f"), 3.0)
StarFont_SetKerning(string.byte("r"), string.byte("l"), 3.0)
StarFont_SetKerning(string.byte("s"), string.byte("o"), 2.0)
StarFont_SetKerning(string.byte("s"), string.byte("s"), 2.0)
StarFont_SetKerning(string.byte("s"), string.byte("t"), 2.0)
StarFont_SetKerning(string.byte("S"), string.byte("h"), 4.0)
StarFont_SetKerning(string.byte("S"), string.byte("e"), 3.0)
StarFont_SetKerning(string.byte("T"), string.byte("h"), 3.0)
StarFont_SetKerning(string.byte("w"), string.byte("b"), 2.0)
StarFont_SetKerning(string.byte("W"), string.byte("a"),-1.0)

-- |[Letter-to-Punctuation]|
--StarFont_SetKerning(string.byte("r"), string.byte(","), 0.0)
