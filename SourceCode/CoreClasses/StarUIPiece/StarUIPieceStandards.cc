//--Base
#include "StarUIPiece.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"

//--Definitions
//--Libraries
//--Managers
#include "DisplayManager.h"

///========================================== Timers ==============================================
void StarUIPiece::StandardTimer(int &sTimer, int pRangeLo, int pRangeHi, bool pCondition)
{
    ///--[Documentation]
    //--Standard movement of a timer between two end points based on a boolean. If true, increment.
    if(pCondition)
    {
        if(sTimer < pRangeHi) sTimer ++;
    }
    //--If false, decrement.
    else
    {
        if(sTimer > pRangeLo) sTimer --;
    }
}
void StarUIPiece::StandardLoopTimer(int &sTimer, int pRangeLo, int pRangeHi)
{
    ///--[Documentation]
    //--A timer that runs up to pRangeHi, then resets to pRangeLo and does it again.
    sTimer ++;
    if(sTimer >= pRangeHi) sTimer = pRangeLo;
}
void StarUIPiece::StandardCycleTimer(int &sTimer, int &sPauseTimer, int pRangeLo, int pRangeHi, bool &sCondition, int pPauseTicks)
{
    ///--[Documentation]
    //--A timer that cycles up and down, pausing at each end. The condition must be a single variable, not a logical statement.

    ///--[Pause]
    if(sPauseTimer > 0)
    {
        sPauseTimer --;
    }
    ///--Cycling Up]
    else if(sCondition)
    {
        sTimer ++;
        if(sTimer >= pRangeHi)
        {
            sCondition = false;
            sTimer = pRangeHi;
            sPauseTimer = pPauseTicks;
        }
    }
    ///--[Cycling Down]
    else
    {
        sTimer --;
        if(sTimer <= pRangeLo)
        {
            sCondition = true;
            sTimer = pRangeLo;
            sPauseTimer = pPauseTicks;
        }
    }
}

///========================================== Cursor ==============================================
bool StarUIPiece::StandardCursor(int &sCursor, int &sSkip, int pMoves, int pListCap, int pScrollBuffer, int pItemsPerPage, bool pWrap)
{
    ///--[Documentation]
    //--Abstraction of a simple cursor algorithm. The sCursor and sSkip values represent where we are on a list of entries,
    //  with the cursor being the selected item, and the skip being how many entries are beneath the render threshold.
    //--The number of moves is then subtracted from the cursor iteratively, and the algorithm handles scrolling, wrapping,
    //  and adjusting the skip and cursor values automatically.
    //--Returns true if the cursor changed position from where it was, false otherwise.
    if(pMoves == 0) return false;

    ///--[Range Checking]
    //--If there are one or are zero entries, nothing needs to be done.
    if(pListCap < 2) return false;

    ///--[Moving Up]
    //--Decrement the cursor, same as pressing Up in most cases, or Left in some.
    if(pMoves < 0)
    {
        //--Absolute value.
        pMoves = pMoves * -1;

        //--Iterate.
        for(int i = 0; i < pMoves; i ++)
        {
            //--Set.
            sCursor --;

            //--Wrap if needed. This only occurs on the first pass, otherwise the list clamps.
            if(sCursor < 0 && i == 0 && pWrap)
            {
                sCursor = pListCap - 1;
                sSkip = sCursor - pItemsPerPage + 1;
                if(sSkip < 0) sSkip = 0;
                break;
            }
            //--Just clamp on successive passes, or if wrapping is disallowed.
            else if(sCursor < 0)
            {
                sCursor = 0;
                break;
            }
            //--Offset scrolling.
            else
            {
                int tEffectiveCursor = sCursor - sSkip;
                if(tEffectiveCursor < pScrollBuffer)
                {
                    //--Set.
                    sSkip --;

                    //--Clamp against zero.
                    if(sSkip < 0)
                    {
                        sSkip ++;
                    }
                }
            }
        }
    }
    ///--[Moving Down]
    //--Increment the cursor, same as pressing Down or Right.
    else
    {
        //--Iterate.
        for(int i = 0; i < pMoves; i ++)
        {
            //--Set.
            sCursor ++;

            //--Wrap if needed.
            if(sCursor >= pListCap && i == 0 && pWrap)
            {
                sCursor = 0;
                sSkip = 0;
                break;
            }
            //--Clamp.
            else if(sCursor >= pListCap)
            {
                sCursor = pListCap - 1;
            }
            //--Check scrolling.
            else
            {
                int tEffectiveCursor = sCursor - sSkip;
                if(tEffectiveCursor >= (pItemsPerPage - pScrollBuffer))
                {
                    //--Increment.
                    sSkip ++;

                    //--Clamp against the end of the list.
                    if(sSkip + pItemsPerPage > pListCap)
                    {
                        sSkip --;
                    }
                }
            }
        }
    }

    //--Cursor changed position.
    return true;
}
bool StarUIPiece::StandardCursorNoScroll(int &sCursor, int pMoves, int pListCap, bool pWrap)
{
    ///--[Documentation]
    //--A version of StandardCursor() where no scrollbars or paging is done. Used if the number of elements is fixed and
    //  does not scroll, or for menu implementations like using shoulder buttons to change active characters.
    if(pMoves == 0) return false;

    ///--[Moving Up]
    //--Decrement the cursor, same as pressing Up in most cases, or Left in some.
    if(pMoves < 0)
    {
        //--Absolute value.
        pMoves = pMoves * -1;

        //--Iterate.
        for(int i = 0; i < pMoves; i ++)
        {
            //--Set.
            sCursor --;

            //--Wrap if needed. This only occurs on the first pass, otherwise the list clamps.
            if(sCursor < 0 && i == 0 && pWrap)
            {
                sCursor = pListCap - 1;
                break;
            }
            //--Just clamp on successive passes, or if wrapping is disallowed.
            else if(sCursor < 0)
            {
                sCursor = 0;
                break;
            }
        }
    }
    ///--[Moving Down]
    //--Increment the cursor, same as pressing Down or Right.
    else
    {
        //--Iterate.
        for(int i = 0; i < pMoves; i ++)
        {
            //--Set.
            sCursor ++;

            //--Wrap if needed.
            if(sCursor >= pListCap && i == 0 && pWrap)
            {
                sCursor = 0;
                break;
            }
            //--Clamp.
            else if(sCursor >= pListCap)
            {
                sCursor = pListCap - 1;
            }
        }
    }

    //--Cursor changed position.
    return true;
}

///======================================== Scrollbars ============================================
void StarUIPiece::StandardRenderScrollbar(int pSkip, int pItemsPerPage, int pMaxOffset, float pScrollTop, float pScrollHei, bool pStaticIsFront, StarBitmap *pScroller, StarBitmap *pStatic)
{
    ///--[Documentation]
    //--Renders a scrollbar using the given image with the given properties. Uses a provided front/back both to render
    //  and to resolve scrollbar sizes.
    if(!pScroller || !pStatic) return;

    ///--[Error Check]
    //--Can't have zero items per page, or zero max offset.
    if(pItemsPerPage < 1) return;
    if(pMaxOffset    < 1) return;

    ///--[Constants]
    //--Indent.
    float cInd = 6.0f;

    //--True max requires per-page added.
    int cTrueMax = pMaxOffset + pItemsPerPage;

    ///--[Static Part]
    //--Render if this is in back.
    if(!pStaticIsFront) pStatic->Draw();

    ///--[Front]
    //--Binding.
    pScroller->Bind();

    //--Determine what percentage of the inventory is presently represented.
    float cPctTop = (pSkip                ) / (float)cTrueMax;
    float cPctBot = (pSkip + pItemsPerPage) / (float)cTrueMax;

    //--Positions and Constants.
    float cLft = pStatic->GetXOffset() + (pStatic->GetWidth() * 0.50f) - (pScroller->GetWidth() * 0.50f);
    float cRgt = cLft + pScroller->GetWidth();
    float cEdg = cInd / (float)pScroller->GetHeight();

    //--Compute where the bar should be.
    float cTopTop = pScrollTop + (cPctTop * pScrollHei);
    float cTopBot = cTopTop + cInd;
    float cBotBot = pScrollTop + (cPctBot * pScrollHei);
    float cBotTop = cBotBot - cInd;

    glBegin(GL_QUADS);
        //--Render the top of the bar.
        glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
        glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
        glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
        glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

        //--Render the bottom of the bar.
        glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
        glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
        glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

        //--Render the middle of the bar.
        glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
        glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
        glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
        glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
    glEnd();

    ///--[Static Part]
    //--Render.
    if(pStaticIsFront) pStatic->Draw();
}

///======================================== Positioning ===========================================
void StarUIPiece::StandardPieceOpen(int pDirection, float pVisAlpha, float &sXOffset, float &sYOffset, float &sColorAlpha)
{
    ///--[Documentation]
    //--The most common opener for the lft/top/rgt/bot rendering groups is to resolve the alpha color,
    //  the X/Y offset, and then to position based on those. This does that except the positioning.
    //--Note: The Color Alpha is the actual alpha passed to the glColor() calls. If using a sliding
    //  UI it always resolves to 1.0f.

    ///--[Color]
    //--If not fading, color alpha is always 1.0f.
    sColorAlpha = pVisAlpha;
    if(!mUseFade) sColorAlpha = 1.0f;

    ///--[Position/Direction]
    //--Clear.
    sXOffset = 0.0f;
    sYOffset = 0.0f;

    //--Percentage onscreen is the inverse of the visalpha. At 1.0f, the object is fully visible and offsets are 0.
    float tVisPercent = 1.0f - pVisAlpha;

    //--Under normal circumstances, UIs slide in from the screen edges. The DIR_UP series of constants
    //  denotes the direction and the canvas edge is used.
    if(pDirection == DIR_UP)
    {
        sYOffset = VIRTUAL_CANVAS_Y * tVisPercent * -1.0f;
    }
    else if(pDirection == DIR_UPRIGHT)
    {
        sXOffset = VIRTUAL_CANVAS_X * tVisPercent * 1.0f;
        sYOffset = VIRTUAL_CANVAS_Y * tVisPercent * -1.0f;
    }
    else if(pDirection == DIR_RIGHT)
    {
        sXOffset = VIRTUAL_CANVAS_X * tVisPercent * 1.0f;
    }
    else if(pDirection == DIR_DOWNRIGHT)
    {
        sXOffset = VIRTUAL_CANVAS_X * tVisPercent * 1.0f;
        sYOffset = VIRTUAL_CANVAS_Y * tVisPercent * 1.0f;
    }
    else if(pDirection == DIR_DOWN)
    {
        sYOffset = VIRTUAL_CANVAS_Y * tVisPercent * 1.0f;
    }
    else if(pDirection == DIR_DOWNLEFT)
    {
        sXOffset = VIRTUAL_CANVAS_X * tVisPercent * -1.0f;
        sYOffset = VIRTUAL_CANVAS_Y * tVisPercent * 1.0f;
    }
    else if(pDirection == DIR_LEFT)
    {
        sXOffset = VIRTUAL_CANVAS_X * tVisPercent * -1.0f;
    }
    else if(pDirection == DIR_UPLEFT)
    {
        sXOffset = VIRTUAL_CANVAS_X * tVisPercent * -1.0f;
        sYOffset = VIRTUAL_CANVAS_Y * tVisPercent * -1.0f;
    }
}
