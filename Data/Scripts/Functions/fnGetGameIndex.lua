-- |[ ==================================== fnGetGameIndex() ==================================== ]|
--Returns the index of a playable game of the same name, or 0 if the game is not found.
function fnGetGameIndex(psName)
    
    --Deprecated Warning
    if(gbPrintDeprecatedCallWarnings == true) then
        io.write("Warning: Calling fnGetGameIndex() is deprecated. Call from: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Argument Check]|
    if(psName == nil) then return 0 end

    -- |[Activation Check]|
    --If the game globals were not initialized yet, stop.
    if(giGamesTotal == nil or gsaGameEntries == nil) then return 0 end

    -- |[Execution]|
    --Scan the entries for a match.
    for i = 1, giGamesTotal, 1 do
        if(gsaGameEntries[i].sName == psName) then
            return i
        end
    end
    
    --Not found.
    return 0
end