///======================================= MonocerosMenu ==========================================
//--Same as the AdventureMenu class, but changes a few population calls so that features not present
//  in Monoceros don't appear on the menus.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "AdventureMenu.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Modes
#define MONOMENU_JOURNAL_MODE_BESTIARY 0
#define MONOMENU_JOURNAL_MODE_PROFILES 1
#define MONOMENU_JOURNAL_MODE_QUESTS 2
#define MONOMENU_JOURNAL_MODE_LOCATIONS 3
#define MONOMENU_JOURNAL_MODE_COMBATGLOSSARY 4
#define MONOMENU_JOURNAL_MODE_ACHIEVEMENTS 5
#define MONOMENU_JOURNAL_MODE_TOTAL 6

//--Sizing
#define MONOMENU_BIG_CONTROL_OFFSET_Y 15.0f
#define MONOMENU_BIG_ICON_OFFSET_Y 11.0f

///========================================== Classes =============================================
class MonocerosMenu : public AdventureMenu
{
    private:
    ///--[System]
    bool mOptionsOnly;
    bool mEnableMouse;

    ///--[Strings]
    char *mTextCampfireMenu;
    char *mTextSave;
    char *mTextWarp;
    char *mTextPassword;

    //--Resolutions.
    int mResolutionsTotal;
    char **mResolutionStrings;

    ///--[Mouse Handling]
    int mPreviousMouseX, mPreviousMouseY, mPreviousMouseZ;

    ///--[Images]
    StarlightColor mMonoTitle;
    StarlightColor mMonoSubtitle;
    StarlightColor mMonoParagraph;
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontControl;
            StarFont *rFontHeading;
            StarFont *rFontMainline;

            //--Images
            StarBitmap *rScrollbarFront;
            StarBitmap *rFrameDescription;
            StarBitmap *rFrameHelp;
            StarBitmap *rOverlayDescription;
        }Standard;
    }MonoImg;

    protected:

    public:
    //--System
    MonocerosMenu();
    virtual ~MonocerosMenu();
    void ReverifyOptions();
    void SetCampfireOverrides();

    //--Public Variables
    static bool xIsMenuExecution;
    static char *xSwitchToLevelupPath;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    //--Core Methods
    void SetLevelupMode();
    void ActivateGemsMode();
    void SetBuyOnly();
    void SetToOptionsMode();

    ///--[Standard]
    virtual void RenderItemDetails(int pTimer, int pTimerMax, AdventureItem *pItem);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_MonoMenu_GetProperty(lua_State *L);
int Hook_MonoMenu_SetProperty(lua_State *L);
