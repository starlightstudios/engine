//--Base
#include "DisplayManager.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "GlDfn.h"
#define GL_MIRRORED_REPEAT                0x8370

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

void RegisterToList(StarLinkedList *pList, uint32_t pEnumeration, const char *pName)
{
    //--Worker function, registers a heap-allocated enumeration to the list.  For speed, does
    //  not bother to check the list/name for NULL safety, as this is only used in the below
    //  function and not elsewhere.
    SetMemoryData(__FILE__, __LINE__);
    uint32_t *nPtr = (uint32_t *)starmemoryalloc(sizeof(uint32_t));
    *nPtr = pEnumeration;
    pList->AddElement(pName, nPtr, &FreeThis);
}

StarLinkedList *DisplayManager::xEnumerationList = NULL;
void DisplayManager::ConstructEnumerationList()
{
    //--Called once the GL state has been created, creates a list of GL enumerations which can be
    //  queried by human-readable strings (nominally from Lua) and then allows some state-syncing
    //  between the two.
    if(xEnumerationList) return;
    xEnumerationList = new StarLinkedList(true);

    //--[Mini/Magni-fication]
    RegisterToList(xEnumerationList, GL_LINEAR,                 "GL_LINEAR");
    RegisterToList(xEnumerationList, GL_NEAREST,                "GL_NEAREST");
    RegisterToList(xEnumerationList, GL_LINEAR_MIPMAP_LINEAR,   "GL_LINEAR_MIPMAP_LINEAR");
    RegisterToList(xEnumerationList, GL_LINEAR_MIPMAP_NEAREST,  "GL_LINEAR_MIPMAP_NEAREST");
    RegisterToList(xEnumerationList, GL_NEAREST_MIPMAP_LINEAR,  "GL_NEAREST_MIPMAP_LINEAR");
    RegisterToList(xEnumerationList, GL_NEAREST_MIPMAP_NEAREST, "GL_NEAREST_MIPMAP_NEAREST");

    //--[Border-Clamping]
    RegisterToList(xEnumerationList, GL_CLAMP_TO_EDGE,        "GL_CLAMP_TO_EDGE");
    RegisterToList(xEnumerationList, GL_CLAMP_TO_BORDER,      "GL_CLAMP_TO_BORDER");
    RegisterToList(xEnumerationList, GL_REPEAT,               "GL_REPEAT");

    //--Allegro-only
    #if defined _ALLEGRO_PROJECT_
        RegisterToList(xEnumerationList, GL_MIRRORED_REPEAT,      "GL_MIRRORED_REPEAT");
    #endif

    //RegisterToList(xEnumerationList, GL_MIRROR_CLAMP_TO_EDGE, "GL_MIRROR_CLAMP_TO_EDGE");
}
uint32_t DisplayManager::GetEnumeration(const char *pName)
{
    //--Returns the enumeration, or 0 (and prints a message) if not found.
    if(!pName || !xEnumerationList) return 0;
    uint32_t *rPtr = (uint32_t *)xEnumerationList->GetElementByName(pName);

    //--Error print.
    if(!rPtr)
    {
        DebugManager::ForcePrint("Error locating enumeration %s\n", pName);
        return 0;
    }

    //--Success, return it.
    return (*rPtr);
}
