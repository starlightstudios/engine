//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdvCombat.h"
#include "AdvUIJournal.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"

//--Definitions
#include "AdvUIJournalTypes.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
AdvUIJournal::AdvUIJournal()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_ADVUIJOURNAL;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "AJOR");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUIJournal ======= ]
    ///--[System]
    //--System
    mCurrentMode = ADVMENU_JOURNAL_MODE_BESTIARY;
    mIsAchievementsMode = false;

    //--Main Menu
    mMainMenuLftString = new StarlightString();
    mMainMenuRgtString = new StarlightString();

    //--Bestiary
    mBestiaryOffset = 0;
    mBestiaryCursor = 0;
    mShowFullBestiaryImage = false;
    mShowParagon = false;
    mBestiaryImageTimer = 0;
    mBestiaryEntries = new StarLinkedList(true);

    //--Profiles
    mProfileOffset = 0;
    mProfileCursor = 0;
    mProfileEntries = new StarLinkedList(true);

    //--Quests
    mQuestOffset = 0;
    mQuestCursor = 0;
    mQuestEntries = new StarLinkedList(true);

    //--Locations
    mLocationOffset = 0;
    mLocationCursor = 0;
    mLocationEntries = new StarLinkedList(true);

    //--Paragons
    mParagonOffset = 0;
    mParagonCursor = 0;
    mParagonEntries = new StarLinkedList(true);

    //--Combat Glossary
    mCombatOffset = 0;
    mCombatCursor = 0;
    mCombatEntries = new StarLinkedList(true);

    //--Achievements
    mHideSpoilers = true;
    mAchievementScroll.Initialize();
    mAchievementCategoryOffset = 0;
    mAchievementCategoryCursor = 0;
    mAchievementEntryOffset = 0;
    mAchievementEntryCursor = 0;

    //--Common
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Help Strings
    mShowAchievementsString = new StarlightString();
    mHideAchievementsString = new StarlightString();
    mScrollAchievementString = new StarlightString();
    mToggleSpoilerString = new StarlightString();
    mShowFullPortraitString = new StarlightString();
    mToggleParagonString = new StarlightString();
    mEnableParagonString = new StarlightString();
    mReturnString = new StarlightString();
    mMove10String = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));

    //--If these variables haven't booted yet, do that now.
    if(!xAchievementOverPath)
    {
        ResetString(xAchievementOverPath,  "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch1");
        ResetString(xAchievementTitle,      "Plantchievements");
        ResetString(xAchievementShowString, "[IMG0] Show Plantchievements");
        ResetString(xAchievementHideString, "[IMG0] Hide Plantchievements");
    }
}
AdvUIJournal::~AdvUIJournal()
{
    delete mMainMenuLftString;
    delete mMainMenuRgtString;
    delete mBestiaryEntries;
    delete mProfileEntries;
    delete mQuestEntries;
    delete mLocationEntries;
    delete mParagonEntries;
    delete mCombatEntries;
    delete mShowAchievementsString;
    delete mHideAchievementsString;
    delete mScrollAchievementString;
    delete mToggleSpoilerString;
    delete mShowFullPortraitString;
    delete mToggleParagonString;
    delete mEnableParagonString;
    delete mReturnString;
    delete mMove10String;
}
void AdvUIJournal::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Adventure Journal UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",    &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Diagnostic, if set to true all journal entries render.
bool AdvUIJournal::xShowAllEntries = false;

//--Script that fires to resolve bestiary entries.
char *AdvUIJournal::xBestiaryResolveScript = NULL;

//--Script that fires to resolve profile entries.
char *AdvUIJournal::xProfileResolveScript = NULL;

//--Script that fires to resolve quest entries.
char *AdvUIJournal::xQuestResolveScript = NULL;

//--Script that fires to resolve location entries.
char *AdvUIJournal::xLocationResolveScript = NULL;

//--Script that fires to resolve paragon entries.
char *AdvUIJournal::xParagonResolveScript = NULL;

//--Script that fires to resolve combat entries.
char *AdvUIJournal::xCombatResolveScript = NULL;

char *AdvUIJournal::xAchievementOverPath = NULL;
char *AdvUIJournal::xAchievementTitle = NULL;
char *AdvUIJournal::xAchievementShowString = NULL;
char *AdvUIJournal::xAchievementHideString = NULL;

//--Achievements.
int AdvUIJournal::xSteamNameRemapsTotal = 0;
AdvMenuJournalSteamNameRemap *AdvUIJournal::xSteamNameRemaps = NULL;
StarLinkedList *AdvUIJournal::xAchievementCategories = new StarLinkedList(true);

///===================================== Property Queries =========================================
bool AdvUIJournal::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVUIJOURNAL) return true;
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    return false;
}

///======================================= Manipulators ===========================================
void AdvUIJournal::TakeForeground()
{
    //--Variables
    mIsAchievementsMode = false;

    //--Call subroutine to reset the help icons in case controls changed.
    RefreshMenuHelp();

    //--By default, we're in Bestiary mode.
    HandleJournalModeChange();
    SetJournalToBestiaryMode();
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();
}

///======================================= Core Methods ===========================================
void AdvUIJournal::RefreshMenuHelp()
{
    //--Resolve the images needed for the help menu.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Main Menu Left Indicator
    mMainMenuLftString->SetString("[IMG0] Left");
    mMainMenuLftString->AllocateImages(1);
    mMainMenuLftString->SetImageP(0, 2.0f, rControlManager->ResolveControlImage("Left"));
    mMainMenuLftString->CrossreferenceImages();
    mMainMenuLftString->SetTextColor(StarlightColor::MapRGBAI(255, 216, 0, 255));

    //--Main Menu Right Indicator
    mMainMenuRgtString->SetString("Right [IMG0]");
    mMainMenuRgtString->AllocateImages(1);
    mMainMenuRgtString->SetImageP(0, 2.0f, rControlManager->ResolveControlImage("Right"));
    mMainMenuRgtString->CrossreferenceImages();
    mMainMenuRgtString->SetTextColor(StarlightColor::MapRGBAI(255, 216, 0, 255));

    //--Show Plantchievements
    mShowAchievementsString->SetString(xAchievementShowString);
    mShowAchievementsString->AllocateImages(1);
    mShowAchievementsString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("OpenFieldAbilityMenu"));
    mShowAchievementsString->CrossreferenceImages();

    //--Hide Plantchievements
    mHideAchievementsString->SetString(xAchievementHideString);
    mHideAchievementsString->AllocateImages(1);
    mHideAchievementsString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("OpenFieldAbilityMenu"));
    mHideAchievementsString->CrossreferenceImages();

    //--Scroll Plantchievements
    mScrollAchievementString->SetString("[IMG0][IMG1] Scroll Within Category");
    mScrollAchievementString->AllocateImages(2);
    mScrollAchievementString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("UpLevel"));
    mScrollAchievementString->SetImageP(1, 5.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mScrollAchievementString->CrossreferenceImages();

    //--Toggle Plantchievement Spoiler
    mToggleSpoilerString->SetString("[IMG0] Toggle Spoilers");
    mToggleSpoilerString->AllocateImages(1);
    mToggleSpoilerString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mToggleSpoilerString->CrossreferenceImages();

    //--Zoom in/out on bestiary entry
    mShowFullPortraitString->SetString("[IMG0] Show full portrait");
    mShowFullPortraitString->AllocateImages(1);
    mShowFullPortraitString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mShowFullPortraitString->CrossreferenceImages();

    //--Toggle Paragon image
    mToggleParagonString->SetString("[IMG0] Toggle paragon");
    mToggleParagonString->AllocateImages(1);
    mToggleParagonString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mToggleParagonString->CrossreferenceImages();

    //--Enable/Disable Paragon
    mEnableParagonString->SetString("[IMG0] Enable/Disable");
    mEnableParagonString->AllocateImages(1);
    mEnableParagonString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mEnableParagonString->CrossreferenceImages();

    //--Return to main menu
    mReturnString->SetString("[IMG0] Exit");
    mReturnString->AllocateImages(1);
    mReturnString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mReturnString->CrossreferenceImages();

    //--Move 10 entries at a time
    mMove10String->SetString("[IMG0] (Hold) Move 10 Entries");
    mMove10String->AllocateImages(1);
    mMove10String->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Ctrl"));
    mMove10String->CrossreferenceImages();
}
void AdvUIJournal::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Determines the highlight position on the left side of the screen. This is done by calling a subroutine
    //  based on the active mode.

    ///--[Execution]
    if(mIsAchievementsMode)
    {
        RecomputeJournalAchievementCursorPos();
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_BESTIARY)
    {
        ComputeBestiaryCursorPosition();
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PROFILES)
    {
        ComputeProfileCursorPosition();
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_QUESTS)
    {
        ComputeQuestCursorPosition();
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_LOCATIONS)
    {
        ComputeLocationCursorPosition();
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PARAGONS)
    {
        ComputeParagonCursorPosition();
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_COMBATGLOSSARY)
    {
        ComputeCombatCursorPosition();
    }
}
void AdvUIJournal::HandleJournalModeChange()
{
    //--Called whenever the journal changes modes or exits. Used as a generic response handler
    //  to make sure all the modes have their variables handled after leaving.
    mShowFullBestiaryImage = false;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIJournal::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Handles timers, controls, and other update components for the UI object.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    StandardTimer(mVisibilityTimer,    0, mVisibilityTimerMax,                   true);
    StandardTimer(mBestiaryImageTimer, 0, ADVMENU_JOURNAL_BESTIARY_FULLIMAGE_VIS_TICKS, mShowFullBestiaryImage);

    //--Highlight.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Achievements Subhandler]
    //--Sub-handler, switches control handling to the achievements submenu.
    if(mIsAchievementsMode)
    {
        UpdateJournalAchievements();
        return true;
    }

    ///--[Toggle Achievements Mode]
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        mIsAchievementsMode = true;
        mAchievementCategoryOffset = 0;
        mAchievementCategoryCursor = 0;
        mAchievementEntryOffset = 0;
        mAchievementEntryCursor = 0;
        RecomputeJournalAchievementCursorPos();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Help Handler]
    //--Toggle description flag.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--Get, cycle.
        int tComplexDescriptionFlag = rAdventureCombat->IsShowingDetailedDescriptions();
        tComplexDescriptionFlag ++;
        if(tComplexDescriptionFlag > ADVCOMBAT_DESCRIPTIONS_HIDDEN) tComplexDescriptionFlag = ADVCOMBAT_DESCRIPTIONS_SIMPLE;

        //--Set.
        rAdventureCombat->SetDetailedDescriptionFlag(tComplexDescriptionFlag);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Main Journal Menu]
    //--Setup.
    int tStartingMode = mCurrentMode;

    //--This is always active. Pressing left or right changes journal modes.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        mCurrentMode --;
        if(mCurrentMode < 0) mCurrentMode = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    else if(!rControlManager->IsFirstPress("Left") && rControlManager->IsFirstPress("Right"))
    {
        mCurrentMode ++;
        if(mCurrentMode >= ADVMENU_JOURNAL_MODE_TOTAL) mCurrentMode = ADVMENU_JOURNAL_MODE_TOTAL - 1;
        if(mCurrentMode < 0) mCurrentMode = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--If the mode changed for any reason, run this code:
    if(tStartingMode != mCurrentMode)
    {
        //--Reset any outstanding variables.
        HandleJournalModeChange();

        //--Call the requisite mode.
        if(mCurrentMode == ADVMENU_JOURNAL_MODE_BESTIARY)
            SetJournalToBestiaryMode();
        else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PROFILES)
            SetJournalToProfilesMode();
        else if(mCurrentMode == ADVMENU_JOURNAL_MODE_QUESTS)
            SetJournalToQuestsMode();
        else if(mCurrentMode == ADVMENU_JOURNAL_MODE_LOCATIONS)
            SetJournalToLocationsMode();
        else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PARAGONS)
            SetJournalToParagonsMode();
        else if(mCurrentMode == ADVMENU_JOURNAL_MODE_COMBATGLOSSARY)
            SetJournalToCombatMode();

        //--Always recompute the cursor.
        RecomputeCursorPositions();
    }

    //--Pressing cancel returns to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--If in bestiary mode and showing the full portrait, cancels that.
        if(mCurrentMode == ADVMENU_JOURNAL_MODE_BESTIARY && mShowFullBestiaryImage)
        {
            mShowFullBestiaryImage = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return true;
        }

        //--Return to main menu.
        HandleJournalModeChange();
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    ///--[Setup]
    bool tRecompute = false;

    ///--[Bestiary Menu]
    if(mCurrentMode == ADVMENU_JOURNAL_MODE_BESTIARY)
    {
        //--Up and Down handled by standard.
        tRecompute = AutoListUpDn(mBestiaryCursor, mBestiaryOffset, mBestiaryEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);

        //--Activate, toggles on and off showing the detailed portrait.
        if(rControlManager->IsFirstPress("Activate"))
        {
            mShowFullBestiaryImage = !mShowFullBestiaryImage;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Toggle paragons.
        if(rControlManager->IsFirstPress("Run"))
        {
            mShowParagon = !mShowParagon;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Profile Menu]
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PROFILES)
    {
        tRecompute = AutoListUpDn(mProfileCursor, mProfileOffset, mProfileEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }
    ///--[Quests]
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_QUESTS)
    {
        tRecompute = AutoListUpDn(mQuestCursor, mQuestOffset, mQuestEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }
    ///--[Locations]
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_LOCATIONS)
    {
        tRecompute = AutoListUpDn(mLocationCursor, mLocationOffset, mLocationEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }
    ///--[Paragons]
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PARAGONS)
    {
        //--Up and Down handled by standard.
        tRecompute = AutoListUpDn(mParagonCursor, mParagonOffset, mParagonEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);

        //--Press Activate to enable/disable a paragon.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the entry.
            AdvMenuParagonEntry *rEntry = (AdvMenuParagonEntry *)mParagonEntries->GetElementBySlot(mParagonCursor);

            //--If the path is not "Null", resolve the variable.
            if(rEntry && strcasecmp(rEntry->mEnabledFlagPath, "Null") && rEntry->mHasDefeatedEnabledParagon)
            {
                //--Get DL Entry.
                SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry(rEntry->mEnabledFlagPath);
                if(rVariable)
                {
                    //--Swap to enabled.
                    if(rVariable->mNumeric == 0.0f)
                    {
                        rVariable->mNumeric = 1.0f;
                    }
                    //--Disabled.
                    else
                    {
                        rVariable->mNumeric = 0.0f;
                    }

                    //--Re-resolve the local flag.
                    rEntry->mIsEnabled = (rVariable->mNumeric != 0.0f);
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
    }
    ///--[Combat Glossary]
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_COMBATGLOSSARY)
    {
        tRecompute = AutoListUpDn(mCombatCursor, mCombatOffset, mCombatEntries->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    }

    ///--[Finish Up]
    //--If this flag is true, recompute the highlight and play a sound effect.
    if(tRecompute)
    {
        RecomputeCursorPositions();
    }

    //--Indicate we handled the update.
    return true;
}
void AdvUIJournal::UpdateBackground()
{
    ///--[Documentation]
    //--Called when this object is not the focus.
    StandardTimer(mVisibilityTimer, 0, mVisibilityTimerMax, false);
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIJournal::RenderPieces(float pVisAlpha)
{
    ///--[Documentation and Setup]
    //--Entry point for rendering.

    ///--[Achievements Handling]
    //--Does all of the rendering itself.
    if(mIsAchievementsMode)
    {
        RenderJournalAchievements();
        return;
    }

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, mVisibilityTimerMax);
    if(cAlpha <= 0.0f) return;

    ///--[Static Parts]
    //--Common universal parts.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Background.
    if(mCurrentMode != ADVMENU_JOURNAL_MODE_PARAGONS)
        AdvImages.rStaticParts->Draw();
    else
        AdvImages.rStaticPartsParagons->Draw();

    //--Major Header.
    mColorHeading.SetAsMixerAlpha(cAlpha);
    AdvImages.rFont_BigHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, -6.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Journal");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Main Menu]
    //--Render the menu options. These always render regardless of mode.
    float cHelpLft = 45.0f;
    float cHelpTop = 73.0f;
    float cHelpRgt = 1321.0f;

    //--Help text indicating control options.
    mMainMenuLftString->DrawText(cHelpLft, cHelpTop, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Right side. Right-aligned text.
    mMainMenuRgtString->DrawText(cHelpRgt, cHelpTop, SUGARFONT_RIGHTALIGN_X | SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Menu options. These render gold when selected.
    float cBestiaryLft   = 140.0f + 172.0f;
    float cProfilesLft   = 250.0f + 172.0f;
    float cQuestsLft     = 353.0f + 172.0f;
    float cLocationsLft  = 445.0f + 172.0f;
    float cParagonsLft   = 569.0f + 172.0f;
    float cCombatLft     = 688.0f + 172.0f;
    float cMenuOptionTop = 73.0f;
    RenderMenuOption(cBestiaryLft,  cMenuOptionTop, AdvImages.rFont_Mainline, cAlpha, 0, mCurrentMode, "Bestiary");
    RenderMenuOption(cProfilesLft,  cMenuOptionTop, AdvImages.rFont_Mainline, cAlpha, 1, mCurrentMode, "Profiles");
    RenderMenuOption(cQuestsLft,    cMenuOptionTop, AdvImages.rFont_Mainline, cAlpha, 2, mCurrentMode, "Quests");
    RenderMenuOption(cLocationsLft, cMenuOptionTop, AdvImages.rFont_Mainline, cAlpha, 3, mCurrentMode, "Locations");
    RenderMenuOption(cParagonsLft,  cMenuOptionTop, AdvImages.rFont_Mainline, cAlpha, 4, mCurrentMode, "Paragons");
    RenderMenuOption(cCombatLft,    cMenuOptionTop, AdvImages.rFont_Mainline, cAlpha, 5, mCurrentMode, "Combat Glossary");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Help options.
    mReturnString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mMove10String->DrawText(0.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Right-aligned Help.
    mShowAchievementsString->DrawText(VIRTUAL_CANVAS_X, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);

    //--Only shows up in Bestiary mode:
    if(mCurrentMode == ADVMENU_JOURNAL_MODE_BESTIARY)
    {
        mShowFullPortraitString->DrawText(0.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    }

    ///--[Submodes]
    if(mCurrentMode == ADVMENU_JOURNAL_MODE_BESTIARY)
    {
        RenderBestiaryMode(cAlpha);
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PROFILES)
    {
        RenderProfilesMode(cAlpha);
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_QUESTS)
    {
        RenderQuestsMode(cAlpha);
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_LOCATIONS)
    {
        RenderLocationsMode(cAlpha);
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_PARAGONS)
    {
        RenderParagonsMode(cAlpha);
    }
    else if(mCurrentMode == ADVMENU_JOURNAL_MODE_COMBATGLOSSARY)
    {
        RenderCombatMode(cAlpha);
    }

    ///--[Highlight]
    if(!mShowFullBestiaryImage) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    ///--[Clean Up]
    StarlightColor::ClearMixer();
}
void AdvUIJournal::RenderMenuOption(float pX, float pY, StarFont *pFont, float pAlpha, int pSlot, int pMenuSlot, const char *pText)
{
    ///--[Documentation]
    //--Used to render the options at the top of the screen. Selected option uses a highlight color.
    if(!pFont || pAlpha <= 0.0f || !pText) return;

    //--Color check.
    if(pSlot == pMenuSlot)
        glColor4f(1.0f, 0.8f, 0.1f, pAlpha);
    else
        glColor4f(1.0f, 1.0f, 1.0f, pAlpha);

    //--Render.
    pFont->DrawText(pX, pY, 0, 1.0f, pText);

    //--Clean.
    glColor4f(1.0f, 1.0f, 1.0f, pAlpha);
}
void AdvUIJournal::RenderJournalEntryList(StarLinkedList *pList, int pStartAt, int pHighlighted, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Given a list of AdvMenuJournalBasicEntry or derived, renders the selection list on the left side of the screen.
    if(!pList) return;

    //--Font
    StarFont *rUseFont = AdvImages.rFont_Mainline;

    //--Render Constants
    float cSelectionLft = 47.0f;
    float cSelectionTop = 112.0f;
    float cSelectionHei = rUseFont->GetTextHeight();

    //--Image Constants
    float cScale = 0.41f;
    float cScaleInv = 1.0f / cScale;

    //--Variables
    int tOption = 0;
    float tYPos = cSelectionTop;

    ///--[Scrollbar]
    //--If there are over ADVMENU_JOURNAL_ENTRIES_ON_LIST entries, we need a scrollbar. The scrollbar renders
    //  after the entries but must be computed here.
    bool tIsShowingScrollbar = false;
    if(pList->GetListSize() > ADVMENU_JOURNAL_ENTRIES_ON_LIST)
    {
        tIsShowingScrollbar = true;
    }

    ///--[Iteration]
    //--Iteration variables:
    int tRenders = 0;

    //--Iterate across entries:
    AdvMenuJournalBasicEntry *rEntry = (AdvMenuJournalBasicEntry *)pList->PushIterator();
    while(rEntry)
    {
        //--If we haven't reached the start-at value, don't render anything.
        if(tOption < pStartAt)
        {
            tOption ++;
            rEntry = (AdvMenuJournalBasicEntry *)pList->AutoIterate();
            continue;
        }

        //--Backing.
        if(tOption % 2 == 0)
        {
            StarBitmap::DrawRectFill(42.0f, tYPos + 2.0f, 497.0f, tYPos + cSelectionHei + 2.0f, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha));
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }

        //--Render.
        tRenders ++;
        rUseFont->DrawText(cSelectionLft, tYPos, 0, 1.0f, rEntry->mDisplayName);

        //--If there's an icon associated with this entry, render it right-aligned.
        if(rEntry->rIcon)
        {
            //--Right-align position. Moves left if there's a scrollbar.
            float cTopSpot = tYPos + (cSelectionHei + 4.0f) - (rEntry->rIcon->GetTrueHeight() * cScale);
            float cLftSpot = 480.0f - (rEntry->rIcon->GetTrueWidth() * cScale * 0.50f);
            if(tIsShowingScrollbar) cLftSpot = cLftSpot - 45.0f;

            //--Position, scale.
            glTranslatef(cLftSpot, cTopSpot, 0.0f);
            glScalef(cScale, cScale, 1.0f);

            //--Render.
            rEntry->rIcon->Draw();

            //--Clean.
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-cLftSpot, -cTopSpot, 0.0f);
        }

        //--If we hit the end of the list, stop rendering:
        if(tRenders >= ADVMENU_JOURNAL_ENTRIES_ON_LIST)
        {
            pList->PopIterator();
            break;
        }

        //--Next.
        tOption ++;
        tYPos = tYPos + cSelectionHei;
        rEntry = (AdvMenuJournalBasicEntry *)pList->AutoIterate();
    }

    ///--[Scrollbar]
    //--Render scrollbar after entries.
    if(tIsShowingScrollbar)
    {
        StandardRenderScrollbar(pStartAt, ADVMENU_JOURNAL_ENTRIES_ON_LIST, pList->GetListSize() - ADVMENU_JOURNAL_ENTRIES_ON_LIST, 142.0f, 522.0f, true, AdvImages.rScrollbarFront, AdvImages.rScrollbarBack);
    }
}
void AdvUIJournal::RenderResistance(float pX, float pY, int pFlags, float pScale, int pResist, StarFont *pFont)
{
    if(!pFont) return;
    if(pResist < 1000)
        pFont->DrawTextArgs(pX, pY, pFlags, pScale, "%i", pResist);
    else
        pFont->DrawText(pX, pY, pFlags, pScale, "Immune");
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
