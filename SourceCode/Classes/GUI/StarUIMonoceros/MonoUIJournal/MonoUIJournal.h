///======================================= MonoUIJournal ==========================================
//--UI object that encapsulates the Monoceros version of the journal UI.

#pragma once

///========================================= Includes =============================================
#include "AdvUIJournal.h"
#include "MonoUICommon.h"

#ifdef _TARGET_OS_WINDOWS_
    #ifndef _STEAM_API_
        #define constexpr const
    #endif
#endif
#ifdef _TARGET_OS_MAC_
#define constexpr const
#endif

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Modes
#define MONOMENU_JOURNAL_MODE_BESTIARY 0
#define MONOMENU_JOURNAL_MODE_PROFILES 1
#define MONOMENU_JOURNAL_MODE_QUESTS 2
#define MONOMENU_JOURNAL_MODE_LOCATIONS 3
#define MONOMENU_JOURNAL_MODE_COMBATGLOSSARY 4
#define MONOMENU_JOURNAL_MODE_ACHIEVEMENTS 5
#define MONOMENU_JOURNAL_MODE_TOTAL 6

///======================================== Translation ===========================================
///--[MonoUIJournal_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIJOURNAL_STRINGS_TOTAL 26
class MonoUIJournal_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIJOURNAL_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextImmune;
            char *mTextJournal;
            char *mTextBestiary;
            char *mTextProfiles;
            char *mTextQuests;
            char *mTextLocations;
            char *mTextCombatGlossary;
            char *mTextAchievements;
            char *mTextTimesDefeated;
            char *mTextObjective;
            char *mTextLocked;
            char *mTextHiddenUntil;
            char *mTextSpoiler;
            char *mTextSpoilerExpl;
            char *mTextUnlocked;
            char *mTextControlLeft;
            char *mTextControlRight;
            char *mTextControlShowPlantchievements;
            char *mTextControlHidePlantchievements;
            char *mTextControlScrollWithinCategory;
            char *mTextControlToggleSpoilers;
            char *mTextControlShowFullPortrait;
            char *mTextControlToggleParagon;
            char *mTextControlEnableDisable;
            char *mTextControlExit;
            char *mTextControlMode10Entries;
        }str;
    };

    //--Functions
    MonoUIJournal_Strings();
    virtual ~MonoUIJournal_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIJournal : public MonoUICommon, virtual public AdvUIJournal
{
    protected:
    ///--[Constants]
    //--Position.
    static constexpr float cxSelectionLft =  78.0f;
    static constexpr float cxSelectionTop = 168.0f;
    static constexpr float cxSelectionHei =  38.0f;

    //--Scrollbar.
    static const int cxScrollbarListPage = 14;
    static const int cxScrollbarAchPage  =  6;

    ///--[System]
    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFontDoubleHeading;
        StarFont *rFontHeading;
        StarFont *rFontTabs;
        StarFont *rFontControl;
        StarFont *rFontMainline;
        StarFont *rFontStatistics;
        StarFont *rFontHelp;
        StarFont *rFontDescription;
        StarFont *rFontEntries;
        StarFont *rFontAchieveHeader;
        StarFont *rFontAchieveMain;

        //--Images for this UI
        StarBitmap *rCatchievements;
        StarBitmap *rFrameAchievement;
        StarBitmap *rFrameLft;
        StarBitmap *rFrameRgt;
        StarBitmap *rFrameTab;
        StarBitmap *rFrameTabSelected;
        StarBitmap *rOverlayCell;
        StarBitmap *rOverlayIcons;
        StarBitmap *rOverlayInfo;
        StarBitmap *rOverlayInfoName;
        StarBitmap *rOverlayScrollbarAchievement;
        StarBitmap *rOverlayProfileName;
        StarBitmap *rOverlayQuestShade;
        StarBitmap *rOverlayScrollbarBack;
        StarBitmap *rOverlaySelect;
        StarBitmap *rOverlayShadow;
        StarBitmap *rOverlayTitleShadow;
        StarBitmap *rScrollbar_Scroller;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIJournal();
    virtual ~MonoUIJournal();
    virtual void Construct();

    //--Public Variables
    static MonoUIJournal_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    virtual void SetJournalToCombatMode();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    ///--[Achievements]
    virtual void RecomputeJournalAchievementCursorPos();
    virtual float ComputeEntryHeight(AdvMenuJournalAchievementEntry *pEntry);
    virtual void UpdateJournalAchievements();
    virtual void RenderJournalAchievements();
    virtual void RenderJournalAchievement(float pYPos, AdvMenuJournalAchievementEntry *pEntry, float pAlpha);

    ///--[Auto-Sizing]
    virtual void SetCombatGlossaryText(const char *pInternalName, int pSlot, int pLine, const char *pText, const char *pFont, int pFlags);
    virtual void SetBestiaryDescriptionAuto(const char *pInternalName, const char *pDescription);
    virtual void SetQuestDescriptionAuto(const char *pInternalName, const char *pDescription);
    virtual void SetLocationDescriptionAuto(const char *pInternalName, const char *pDescription);
    virtual void SetCombatGlossaryAuto(const char *pInternalName, int pSlot, int pX, int pY, const char *pText, const char *pFont, int pFlags);

    ///--[Cursor]
    virtual void ComputeBestiaryCursorPosition();
    virtual void ComputeCombatCursorPosition();
    virtual void ComputeLocationCursorPosition();
    virtual void ComputeParagonCursorPosition();
    virtual void ComputeProfileCursorPosition();
    virtual void ComputeQuestCursorPosition();

    ///--[Subrender]
    virtual void RenderBestiaryMode(float pVisAlpha);
    virtual void RenderCombatMode(float pVisAlpha);
    virtual void RenderLocationsMode(float pVisAlpha);
    virtual void RenderParagonsMode(float pVisAlpha);
    virtual void RenderProfilesMode(float pVisAlpha);
    virtual void RenderQuestsMode(float pVisAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderMenuOption(float pX, float pY, StarFont *pFont, float pAlpha, int pSlot, int pMenuSlot, const char *pText);
    virtual void RenderResistance(float pX, float pY, int pFlags, float pScale, int pResist, StarFont *pFont);
    virtual void RenderJournalEntryList(StarLinkedList *pList, int pStartAt, int pHighlighted, float pAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


