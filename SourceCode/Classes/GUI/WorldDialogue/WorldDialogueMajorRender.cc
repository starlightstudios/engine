//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueActor.h"
#include "StringEntry.h"
#include "VisualLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

///--[Manipulators]
void WorldDialogue::SetMajorSequence(bool pFlag)
{
    //--Basic flag.
    mIsMajorSequenceMode = pFlag;

    //--If the major sequence got shut off, stop using the upper dialogue.
    if(!mIsMajorSequenceMode)
    {
    }
    //--If the major sequence got turned on, check if we need to reset the timers.
    else
    {
        //--If the major sequence reset flag is set, and at least 15 ticks have elapsed since it was set,
        //  then we need to reset the timer.
        //if(mMajorSequenceNeedsReset && mMajorSequenceResetTimer <= (int)Global::Shared()->gTicksElapsed - 15)
        {
            mMajorSequenceTimer = 0;
        }

        //--Unflip this flag.
        mMajorSequenceNeedsReset = false;
    }
}

///--[Rendering]
void WorldDialogue::RenderMajorSequence()
{
    ///--[ ===================== Documentation ==================== ]
    //--Rendering routine during a major dialogue sequence, which usually consists of multiple characters
    //  speaking for multiple lines.
    if(!Images.mIsReady) return;

    ///--[Compute Alphas]
    float tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    if(tAlphaFactor < 0.0f) tAlphaFactor = 0.0f;
    if(tAlphaFactor > 1.0f) tAlphaFactor = 1.0f;

    ///--[Backing and Widescreen]
    //--Render a black border under everything else. This makes portraits pop out of the screen better.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.1f, 0.1f, 0.1f, cMajorDialogueDarkening * tAlphaFactor));

    ///--[Portraits]
    //--Render the portraits with the subroutine.
    if(!mIsVisualNovel)
    {
        RenderDialogueActors(tAlphaFactor);
    }
    //--Visual novel version.
    else
    {
        RenderVisualNovelActors(tAlphaFactor);
    }

    ///--[ ==================== Dialogue Render =================== ]
    //--Positioning.
    float tCursorX =  42.0f;
    float tCursorY = 600.0f;

    //--Color mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaFactor);

    ///--[Speaker's Name]
    //--If there is a speaker:
    bool tIsSpeakerCase = (mMajorSequenceSpeaker && strcasecmp(mMajorSequenceSpeaker, "Null") && strcasecmp(mMajorSequenceSpeaker, "Narrator") && strcasecmp(mMajorSequenceSpeaker, "Thought"));
    if(tIsSpeakerCase)
    {
        //--Find which side the speaker is on.
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            //--Empty slot. Skip.
            if(!mDialogueActors[i]) continue;

            //--Check aliases.
            if(mDialogueActors[i]->IsSpeaking(mMajorSequenceSpeaker))
            {
                //tIsLeftSpeaker = i < WD_DIALOGUE_ACTOR_SLOTS / 2;
                break;
            }
        }

        //--Left-sided speaker:
        if(true)
        {
            Images.Data.rNameBox->Draw();
            Images.Data.rNamePanel->Draw();
            Images.Data.rHeadingFont->DrawText(264.0f, 563.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, mMajorSequenceSpeakerDisplay);

        }
        //--Right-sided speaker:
        else
        {
            Images.Data.rNameBox->Draw(0.0f, 0.0f);
            Images.Data.rNamePanel->Draw(0.0f, 0.0f, SUGAR_FLIP_HORIZONTAL);
            Images.Data.rHeadingFont->DrawText(760.0f, 563.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, mMajorSequenceSpeakerDisplay);
        }
    }
    //--No speaker.
    else
    {
        Images.Data.rNamelessBox->Draw();
    }

    ///--[ =================== Rendering Cycle ==================== ]
    //--Move the rendering cursor.
    glTranslatef(tCursorX, tCursorY, 0.0f);

    ///--[ ======= Iterate Across Lines ======= ]
    //--Now start rendering.
    StarLinkedList *rLetterList = (StarLinkedList *)mDialogueList->PushIterator();
    while(rLetterList)
    {
        //--Offset value. Resets for each line.
        float tLineCursor = 0.0f;

        ///--[ ==== Iterate Across Letters ==== ]
        DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
        while(rCharacterPack)
        {
            ///--[Color Resolve]
            //--Spooky characters change color.
            if(rCharacterPack->mIsSpooky)
            {
                if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 2)
                {
                    StarlightColor::SetMixer(1.0f, 0.4f, 0.2f, rCharacterPack->mAlpha * tAlphaFactor);
                }
                else if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 4)
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 0.1f, rCharacterPack->mAlpha * tAlphaFactor);
                }
                else
                {
                    StarlightColor::SetMixer(1.0f, 0.2f, 0.5f, rCharacterPack->mAlpha * tAlphaFactor);
                }
            }
            //--Coloring.
            else if(rCharacterPack->rColor)
            {
                StarlightColor::SetMixer(rCharacterPack->rColor->r, rCharacterPack->rColor->g, rCharacterPack->rColor->b, rCharacterPack->mAlpha * tAlphaFactor);
            }
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, rCharacterPack->mAlpha * tAlphaFactor);
            }

            ///--[Size/Position Resolve]
            //--Store original size.
            float tOrigScale = mTextScale;

            //--Text is "Large". This makes it render at double size, and doubles the advance rate.
            if(rCharacterPack->mIsLarge)
            {
                mTextScale = mTextScale * 2.0f;
            }
            //--Text is "Small". Renders at half size, halves advance rate.
            else if(rCharacterPack->mIsSmall)
            {
                mTextScale = mTextScale * 0.5f;
            }

            ///--[Rendering]
            //--Next letter. It may be NULL. This also does the work of iterating.
            DialogueCharacter *rNextLetterPack = (DialogueCharacter *)rLetterList->AutoIterate();

            //--Render the letter.
            char tNextLetter = '\0';
            if(rNextLetterPack) tNextLetter = rNextLetterPack->mLetter;
            float tAdvance = Images.Data.rDialogueFont->DrawLetter(tLineCursor, 0.0f, 0, mTextScale, rCharacterPack->mLetter, tNextLetter);
            tAdvance = Images.Data.rDialogueFont->GetKerningBetween(rCharacterPack->mLetter, tNextLetter);

            ///--[Next]
            //--Move the cursor over.
            tLineCursor = tLineCursor + (tAdvance * mTextScale);

            //--Reset the scale.
            mTextScale = tOrigScale;

            //--Get the next letter.
            rCharacterPack = rNextLetterPack;
        }

        //--Move the cursor down.
        tCursorY = tCursorY + (Images.Data.rDialogueFont->GetTextHeight() * mTextScale);
        glTranslatef(0.0f, (Images.Data.rDialogueFont->GetTextHeight() * mTextScale), 0.0f);

        //--Next line.
        rLetterList = (StarLinkedList *)mDialogueList->AutoIterate();
    }

    ///--[Clean Up]
    //--Reset mixer and position.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
    glTranslatef(tCursorX * -1, tCursorY * -1, 0.0f);

    ///--[ =================== Other UI Pieces ==================== ]
    ///--[Decisions]
    //--Renders over everything else, but only when active.
    if(mIsDecisionMode && !mIsBlocking && mIsPrintingComplete) RenderDecisions();

    ///--[Topics]
    //--When using the mouse, topics renders over the characters.
    if(mIsTopicsMode && mDecisionUsesMouse)
    {
        RenderTopicsMode();
    }

    ///--[String Entry]
    if(mIsStringEntryMode && mStringEntryForm && mIsPrintingComplete) mStringEntryForm->Render();

    ///--[Final Clean]
    StarlightColor::ClearMixer();

    ///--[Flash]
    RenderFlash();
}
