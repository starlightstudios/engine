//--Base
#include "PeakFreaksLevel.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///========================================== System ==============================================
void PeakFreaksLevel::Pause()
{
    //--Flags.
    mIsPaused = true;
    mPauseCursor = PFLEVEL_PAUSE_RESUME;
}

///========================================== Update ==============================================
void PeakFreaksLevel::UpdatePause()
{
    ///--[Documentation and Setup]
    //--Handles updating the pause menu. If the pause menu is not actually active, this still gets
    //  called in order to run the necessary timers. Otherwise, it will handle input and can quit
    //  the game and return to the main menu.

    //--Common timers.
    mPauseTimerAlienFloat ++;
    mPauseTimerAlienAnim ++;
    if(mPauseTimerAlienFloat > PFLEVEL_PAUSE_ALIEN_FLOAT_TICKS)
    {
        mPauseTimerAlienFloat = 0;
    }
    if(mPauseTimerAlienAnim >= (int)(PFLEVEL_PAUSE_ALIEN_ANIM_TPF * PFLEVEL_PAUSE_ALIEN_FRAMES))
    {
        mPauseTimerAlienAnim = 0;
    }

    ///--[Not Active]
    //--Handles timers and then stops.
    if(!mIsPaused)
    {
        if(mPauseTimerBG   >                                    0) mPauseTimerBG   --;
        if(mPauseTimerMenu > PFLEVEL_PAUSE_SLIDE_TICKS_CURSOR_OFF) mPauseTimerMenu --;
        return;
    }

    ///--[Active]
    //--Run timers.
    if(mPauseTimerBG   < PFLEVEL_PAUSE_SLIDE_TICKS) mPauseTimerBG   ++;
    if(mPauseTimerMenu < PFLEVEL_PAUSE_SLIDE_TICKS) mPauseTimerMenu ++;
    mPauseTimerCursor ++;
    if(mPauseTimerCursor > PFLEVEL_PAUSE_CURSOR_TICKS) mPauseTimerCursor = 0;

    ///--[Input]
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("Up"))
    {
        if(mPauseCursor == 0)
        {
            mPauseCursor = PFLEVEL_PAUSE_QUIT;
        }
        else
        {
            mPauseCursor --;
        }

        AudioManager::Fetch()->PlaySound("PF Change");
    }
    else if(rControlManager->IsFirstPress("Down"))
    {
        if(mPauseCursor == PFLEVEL_PAUSE_QUIT)
        {
            mPauseCursor = PFLEVEL_PAUSE_RESUME;
        }
        else
        {
            mPauseCursor ++;
        }

        AudioManager::Fetch()->PlaySound("PF Change");
    }

    //--Activate can unpause, quit, or restart.
    if(rControlManager->IsFirstPress("PF Activate"))
    {
        if(mPauseCursor == PFLEVEL_PAUSE_RESUME)
        {
            AudioManager::Fetch()->PlaySound("PF Cancel");
            mIsPaused = false;
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVolumeNormal * 1.00f);
        }
        else if(mPauseCursor == PFLEVEL_PAUSE_RESTART)
        {
            AudioManager::Fetch()->PlaySound("PF Accept");
            mReturnToTitle = true;
            mRestartLevel = true;
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVolumeNormal * 1.00f);
        }
        else
        {
            xMandatedSeed = -1;
            AudioManager::Fetch()->PlaySound("PF Accept");
            mReturnToTitle = true;
            AudioManager::Fetch()->ChangeMusicVolumeTo(mMusicVolumeNormal * 1.00f);
        }
    }
}

///========================================== Drawing =============================================
void PeakFreaksLevel::RenderPause()
{
    ///--[Documentation and Setup]
    //--Renders the pause menu. This is always called even if not paused. It may render nothing if
    //  the timers make everything offscreen.
    if(mPauseTimerBG < 1 && mPauseTimerMenu < 1) return;

    ///--[Background]
    //--A copy of the sky background renders at half transparency over the screen.
    float cBGTransparency = EasingFunction::Linear(mPauseTimerBG, (float)PFLEVEL_PAUSE_SLIDE_TICKS) * 0.50f;
    if(cBGTransparency > 0.0f)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cBGTransparency);
        Images.Data.rBackgroundSky->Draw();
        StarlightColor::ClearMixer();
    }

    ///--[Pause Menu]
    //--Base. Moves on a different timer than the cursor and options.
    float cBasePct = 1.0f - EasingFunction::QuadraticInOut(mPauseTimerBG, (float)PFLEVEL_PAUSE_SLIDE_TICKS);
    float cBaseOffset = (VIRTUAL_CANVAS_Y * cBasePct);
    if(cBasePct < 1.0f)
    {
        Images.Data.rPauseBG->Draw(0, cBaseOffset);
    }

    ///--[Menu Parts]
    //--Use mPauseTimerMenu to give a "lag" to the position.
    float cMenuPct = 1.0f - EasingFunction::QuadraticInOut(mPauseTimerMenu, (float)PFLEVEL_PAUSE_SLIDE_TICKS);
    float cMenuOffset = (VIRTUAL_CANVAS_Y * cMenuPct);
    if(cMenuPct < 1.0f)
    {
        //--Resolve which buttons to render.
        float cCursorY = 245.0f;
        SugarBitmap *rResumeBtn  = Images.Data.rPauseTextResume;
        SugarBitmap *rRestartBtn = Images.Data.rPauseTextRestart;
        SugarBitmap *rQuitBtn    = Images.Data.rPauseTextQuit;
        if(mPauseCursor == PFLEVEL_PAUSE_RESUME)
        {
            rResumeBtn = Images.Data.rPauseSelResume;
        }
        if(mPauseCursor == PFLEVEL_PAUSE_RESTART)
        {
            cCursorY = 314.0f;
            rRestartBtn = Images.Data.rPauseSelRestart;
        }
        if(mPauseCursor == PFLEVEL_PAUSE_QUIT)
        {
            cCursorY = 391.0f;
            rQuitBtn = Images.Data.rPauseSelQuit;
        }

        //--Render.
        rResumeBtn-> Draw(0, cMenuOffset);
        rRestartBtn->Draw(0, cMenuOffset);
        rQuitBtn->   Draw(0, cMenuOffset);

        //--Cursor. First, resolve X position.
        float cCursorPct = EasingFunction::Linear(mPauseTimerCursor, (float)PFLEVEL_PAUSE_CURSOR_TICKS) * 3.1415926f * 2.0f;
        float cCursorX = 426.0f + (sinf(cCursorPct) * PFLEVEL_PAUSE_CURSOR_DIST);

        //--Resolve Y position.
        cCursorY = cCursorY + cMenuOffset;

        //--Render.
        Images.Data.rPauseCursor->Draw(cCursorX, cCursorY);
    }

    ///--[The Alien]
    //--Yes it's an alien. Shut up. Uses the same position offset as the menu.
    if(cMenuPct < 1.0f)
    {
        //--Resolve animation frame.
        int tFrame = (int)(mPauseTimerAlienAnim / PFLEVEL_PAUSE_ALIEN_ANIM_TPF);
        SugarBitmap *rAlienFrame = Images.Data.rPauseAlien[tFrame];

        //--X position is constant.
        float cXPosition = 824.0f;

        //--Resolve Y position.
        float cAlienPct = EasingFunction::Linear(mPauseTimerAlienFloat, PFLEVEL_PAUSE_ALIEN_FLOAT_TICKS) * 3.1415926f * 2.0f;
        float cYPosition = 271.0f + cMenuOffset + (sinf(cAlienPct) * PFLEVEL_PAUSE_ALIEN_FLOAT_DIST);

        //--Render.
        rAlienFrame->Draw(cXPosition, cYPosition);
    }
}
