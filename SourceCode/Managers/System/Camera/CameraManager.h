///======================================= CameraManager ==========================================
//--Contains all the Cameras currently in use by the game, and functions to manipulate them.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarCamera2D.h"
#include "StarCamera3D.h"
#include "Interfaces/IResettable.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CameraManager : public IResettable
{
    private:
    //--System

    //--Storage
    StarLinkedList *mCameraList;
    StarCamera2D *rActiveCamera2D;
    StarCamera3D *rActiveCamera3D;

    protected:

    public:
    //--System
    CameraManager();
    ~CameraManager();

    //--Public Variables
    bool mLastSwitchFailed;

    //--Property Queries
    //--Manipulators
    void CreateCamera(const char *pName);
    void RegisterCamera(const char *pName, StarCamera *pCamera);
    void SwitchCamera(const char *pName);
    void RemoveCamera(const char *pName);
    void ClearList();

    //--Core Methods
    void PreserveCameraActivity();
    virtual void Reset(const char *pResetType);

    //--Update
    void Update();
    void UpdatePaused(uint8_t pPauseFlags);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    StarCamera *GetCamera(int pSlot);
    StarCamera *GetCamera(const char *pName);

    //--Static Functions
    static CameraManager *Fetch();
    static StarCamera2D *FetchActiveCamera2D();
    static StarCamera3D *FetchActiveCamera3D();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CM_AddCamera(lua_State *L);
int Hook_CM_RemoveCamera(lua_State *L);
int Hook_CM_SetActiveCamera(lua_State *L);

