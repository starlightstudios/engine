//--Base
#include "StringTyrantLoadInterrupt.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
StringTyrantLoadInterrupt::StringTyrantLoadInterrupt()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======== StarLoadInterrupt ========= ]
    ///--[ ===== StringTyrantLoadInterrupt ==== ]
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    mIsReady = false;
    mTextLoading = NULL;
    mTextFinished = NULL;
    mStringTyrantImages = new StarLinkedList(true);
}
StringTyrantLoadInterrupt::~StringTyrantLoadInterrupt()
{
    delete mTextLoading;
    delete mTextFinished;
    delete mStringTyrantImages;
}
void StringTyrantLoadInterrupt::Construct(const char *pDatafilePath)
{
    ///--[Documentation]
    //--Loads and verifies images associated with this object. Because this object is meant to appear during
    //  the loading of other objects, it handles its images internally, not using a conventional DataLibrary.
    if(!pDatafilePath) return;

    ///--[Base Display]
    //--Base files.
    StarLumpManager *tSLM = new StarLumpManager();
    tSLM->Open("Data/Loading.slf");
    mTextLoading  = tSLM->GetImage("StringTyrant|Loading");
    mTextFinished = tSLM->GetImage("StringTyrant|Finished");
    mIsReady = (mTextLoading && mTextFinished);

    ///--[String Tyrant Pre-Rendered Loading Animation]
    //--Loading animation is a filmstrip series placed in a linked list.
    tSLM->Open(pDatafilePath);
    if(!tSLM->IsFileOpen()) return;

    //--Suppress the load interrupt.
    bool tOldFlag = StarBitmap::xSuppressInterrupt;
    StarBitmap::xSuppressInterrupt = true;

    //--Load the frames into our local list.
    char tBuffer[128];
    mStringTyrantImages = new StarLinkedList(true);
    for(int i = 0; i < MARY_FRAMES; i ++)
    {
        sprintf(tBuffer, "Img%03i", i);
        StarBitmap *nFrame = tSLM->GetImage(tBuffer);
        mStringTyrantImages->AddElementAsTail("X", nFrame, &StarBitmap::DeleteThis);
    }

    ///--[Finish Up]
    //--Set to the String Tyrant variant. If anything went wrong, the default one will be used.
    StarBitmap::xInterruptCall = &StarLoadInterrupt::Interrupt;

    //--Set this flag.
    mIsAwaitingKeypress = false;

    //--Clean up.
    delete tSLM;
    StarBitmap::xSuppressInterrupt = tOldFlag;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void StringTyrantLoadInterrupt::DeallocateImages()
{
    ///--[Documentation]
    //--Deallocates and NULLs all images associated with this object.
    mIsReady = false;
    StarLoadInterrupt::DeallocateImages();

    //--Derived call.
    delete mTextLoading;
    delete mTextFinished;
    mStringTyrantImages->ClearList();

    //--NULL.
    mTextLoading = NULL;
    mTextFinished = NULL;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void StringTyrantLoadInterrupt::Call(bool pForceRender)
{
    ///--[Documentation]
    //--In this version of the class, the call is always the same and runs the filmstrip image.
    DebugManager::PushPrint(false, "[StringTyrantLoadInterrupt] Begin\n");
    if(!mIsReady || mIsSuppressed || StarBitmap::xSuppressInterrupt)
    {
        DebugManager::PopPrint("[StringTyrantLoadInterrupt] Suppressed\n");
        return;
    }

    //--Increment the counter. This tracks how many items we've loaded and is used for the progress
    //  bar. The denominator mCurrentMax is a constant.
    mCurrent ++;

    //--Setup.
    float tCurrentTime = GetGameTime();

    //--Get how long it's been since last we rendered anything. This clamps the StarLoadInterrupt at
    //  60 fps (1.0 / 60.0 == 0.01667)
    //--pForceRender will bypass this check.
    //fprintf(stderr, "Time since last tick = %f\n", tCurrentTime - mLastUpdateCall);
    if(tCurrentTime - mLastUpdateCall < 0.01667f && !pForceRender)
    {
        DebugManager::PopPrint("[StringTyrantLoadInterrupt] Too Soon\n");
        return;
    }

    //--Store the time, increment the frame count.
    int cFrameDenom = 2;
    int cFrameMax = (((float)mCurrent / (float)mCurrentMax) * MARY_FRAMES) * cFrameDenom;
    mLastUpdateCall = tCurrentTime;
    mFrame ++;
    if(mFrame >= cFrameMax) mFrame = cFrameMax;

    //--Determine the rendering frame.
    int cRenderFrame = mFrame / cFrameDenom;
    if(cRenderFrame >= MARY_FRAMES)
    {
        cRenderFrame = MARY_FRAMES - 1;
        if(mIsAwaitingKeypress) mIsAwaitingKeypress = false;
    }

    ///--[Renderer Boot]
    //--We have determined the screen needs an update. We cannot assume the DisplayManager is
    //  available, so we need to run these GL commands the old fashioned way.
    //--Coordinates used in this section are hard-coded screen coordinates.
    CommonRendererSetup();

    ///--[Mary's Frame]
    //--Compute the frame.
    StarBitmap *rImage = (StarBitmap *)mStringTyrantImages->GetElementBySlot(cRenderFrame);

    //--Setup.
    if(rImage) rImage->Draw();

    ///--[Loading Text]
    //--Loading text. Underlay always renders the same color.
    if(mCurrent < mCurrentMax)
    {
        float cLft = ((1366.0f) * 0.50f) - (mTextLoading->GetTrueWidth() * 0.50f);
        float cTop = 360.0f;
        mTextLoading->Draw(cLft, cTop);
    }
    else
    {
        float cLft = ((1366.0f) * 0.50f) - (mTextFinished->GetTrueWidth() * 0.50f);
        float cTop = 360.0f;
        mTextFinished->Draw(cLft, cTop);
    }

    //--Finish up.
    CommonRendererClean();

    ///--[Debug]
    DebugManager::PopPrint("[StringTyrantLoadInterrupt] Finished normally\n");
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void StringTyrantLoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_BootStringTyrant(sPath)
       Switches to String Tyrant mode. Pass "NULL" to switch to normal mode. */
    lua_register(pLuaState, "LI_BootStringTyrant", &Hook_LI_BootStringTyrant);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LI_BootStringTyrant(lua_State *L)
{
    //LI_BootStringTyrant(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LI_BootStringTyrant");

    //--Create a new LoadInterrupt.
    StringTyrantLoadInterrupt *nInterrupt = new StringTyrantLoadInterrupt();
    nInterrupt->Construct(lua_tostring(L, 1));

    //--Dealloacte the old one.
    GLOBAL *rGlobal = Global::Shared();
    delete rGlobal->gLoadInterrupt;

    //--Replace.
    rGlobal->gLoadInterrupt = nInterrupt;
    return 0;
}
