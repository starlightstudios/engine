//--Base
#include "AdvUIJournal.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarAutoBuffer.h"
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "VirtualFile.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "SteamManager.h"

//--Needed for _mkdir in the SDL/Steam version.
#if defined _USE_USER_DIRS_
#include <direct.h>
#endif

///========================================== System ==============================================
void AdvUIJournal::SetToAchievementsMode()
{
    mIsAchievementsMode = true;
    mIsAchievementsMode = true;
    mAchievementCategoryOffset = 0;
    mAchievementCategoryCursor = 0;
    mAchievementEntryOffset = 0;
    mAchievementEntryCursor = 0;
    RecomputeJournalAchievementCursorPos();
}

///===================================== Property Queries =========================================
AdvMenuJournalAchievementEntry *AdvUIJournal::LocateAchievement(const char *pName)
{
    //--Scans all categories for the named achievement. Returns it if found, or NULL if not found.
    if(!pName) return NULL;
    AdvMenuJournalAchievementCategory *rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCheckCategory)
    {
        AdvMenuJournalAchievementEntry *rCheckPtr = (AdvMenuJournalAchievementEntry *)rCheckCategory->mAchievementList->GetElementByName(pName);
        if(rCheckPtr)
        {
            xAchievementCategories->PopIterator();
            return rCheckPtr;
        }
        rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    //--Not found.
    return NULL;
}

///======================================= Manipulators ===========================================
float AdvUIJournal::ComputeEntryHeight(AdvMenuJournalAchievementEntry *pEntry)
{
    //--Given an achievement, computes how much space is needed to render it vertically. It will always have a minimum
    //  height to allow the icon, but description and progress requirements can extend it downwards.
    float cBlockHei = 90.0f;
    float cDescHei  = 15.0f;

    //--Error check:
    if(!pEntry) return cBlockHei;

    //--First, the height is set as the block basic height plus progress entries *2, as each progress entry has a name and progress bar.
    float cThisEntryHei = cBlockHei + (cDescHei * pEntry->mProgressEntriesTotal * 2.0f);

    //--If the description doesn't use all 3 alloted lines, reduce the height.
    if(pEntry->mDescriptionLines < 3) cThisEntryHei = cThisEntryHei - (cDescHei * (3 - pEntry->mDescriptionLines));

    //--If a progress entry does not have a name, or is a checkbox, decrease the height.
    for(int i = 0; i < pEntry->mProgressEntriesTotal; i ++)
    {
        if(pEntry->mProgressEntries[i].mDisplayName == NULL || pEntry->mProgressEntries[i].mDisplayAsCheckbox) cThisEntryHei = cThisEntryHei - cDescHei;
    }

    //--If the height comes back below the minimum, set that.
    if(cThisEntryHei < cBlockHei) cThisEntryHei = cBlockHei;

    //--Finish up.
    return cThisEntryHei;
}
void AdvUIJournal::SetAchievementOverGraphic(const char *pPath)
{
    //--Sets the graphic that appears in the top right over the achievements.
    if(!pPath) return;

    //--Set the static path.
    ResetString(xAchievementOverPath, pPath);

    //--Re-resolve the image.
    AdvImages.rStaticPartsAchievementsOver = (StarBitmap *)DataLibrary::Fetch()->GetEntry(xAchievementOverPath);
}
void AdvUIJournal::RegisterAchievementCategory(const char *pName, const char *pDisplayName, const char *pIconImg)
{
    //--Registers a new achievement category. Fails if a category of the same name already exists.
    if(!pName || !pDisplayName) return;

    //--Duplicate check.
    if(xAchievementCategories->GetElementByName(pName) != NULL) return;

    //--Create, init, register.
    AdvMenuJournalAchievementCategory *nCategory = (AdvMenuJournalAchievementCategory *)starmemoryalloc(sizeof(AdvMenuJournalAchievementCategory));
    nCategory->Initialize();
    ResetString(nCategory->mDisplayName, pDisplayName);
    xAchievementCategories->AddElement(pName, nCategory, &AdvMenuJournalAchievementCategory::DeleteThis);

    //--Optional Icon.
    if(pIconImg)
    {
        nCategory->rIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pIconImg);
    }
}
void AdvUIJournal::RegisterAchievement(const char *pCategory, const char *pName, const char *pDisplayName, const char *pImgPath, const char *pDescription)
{
    //--Registers an achievement to a category. Fails if the achievement name is already in use, even
    //  if it's in a different category.
    if(!pCategory || !pName || !pDisplayName || !pDescription) return;

    //--Scan all the categories to see if the name is in use.
    AdvMenuJournalAchievementCategory *rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCheckCategory)
    {
        if(rCheckCategory->mAchievementList->GetElementByName(pName) != NULL)
        {
            xAchievementCategories->PopIterator();
            return;
        }
        rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    //--Make sure the category in question exists.
    AdvMenuJournalAchievementCategory *rRegisterCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->GetElementByName(pCategory);
    if(!rRegisterCategory) return;

    //--Create, init, register.
    AdvMenuJournalAchievementEntry *nAchievement = (AdvMenuJournalAchievementEntry *)starmemoryalloc(sizeof(AdvMenuJournalAchievementEntry));
    nAchievement->Initialize();
    ResetString(nAchievement->mDisplayTitle, pDisplayName);
    rRegisterCategory->mAchievementList->AddElement(pName, nAchievement, &AdvMenuJournalAchievementEntry::DeleteThis);

    //--Description handling.
    StarLinkedList *tStringList = Subdivide::SubdivideStringToList(pDescription, "\n");
    nAchievement->mDescriptionLines = tStringList->GetListSize();
    if(nAchievement->mDescriptionLines > 0)
    {
        //--Allocate space.
        nAchievement->mDescription = (char **)starmemoryalloc(sizeof(char *) * nAchievement->mDescriptionLines);

        //--Populate. Liberate each string.
        int i = 0;
        char *rString = (char *)tStringList->SetToHeadAndReturn();
        while(rString)
        {
            //--Place the description string.
            nAchievement->mDescription[i] = rString;
            tStringList->LiberateRandomPointerEntry();

            //--Next.
            i ++;
            rString = (char *)tStringList->SetToHeadAndReturn();
        }
    }

    //--Optional image.
    if(pImgPath)
    {
        nAchievement->rImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPath);
    }

    //--Clean.
    delete tStringList;

    //--Debug.
    //fprintf(stderr, "Registered achievement %s, %i\n", pName, rRegisterCategory->mAchievementList->GetListSize());
}
void AdvUIJournal::AllocateAchievementRequirements(const char *pName, int pAmount)
{
    //--Allocates space for achievement requirements.
    if(!pName) return;

    //--Get the achievement.
    AdvMenuJournalAchievementEntry *rAchievement = LocateAchievement(pName);
    if(!rAchievement) return;

    //--Clear existing if required.
    for(int i = 0; i < rAchievement->mProgressEntriesTotal; i ++) free(rAchievement->mProgressEntries[i].mDisplayName);
    free(rAchievement->mProgressEntries);

    //--Reset.
    rAchievement->mProgressEntriesTotal = 0;
    rAchievement->mProgressEntries = NULL;
    if(pAmount < 1) return;

    //--Allocate.
    rAchievement->mProgressEntriesTotal = pAmount;
    rAchievement->mProgressEntries = (AdvMenuJournalAchievementProgress *)starmemoryalloc(sizeof(AdvMenuJournalAchievementProgress) * pAmount);
    memset(rAchievement->mProgressEntries, 0, sizeof(AdvMenuJournalAchievementProgress) * pAmount);
}
void AdvUIJournal::SetAchievementRequirements(const char *pName, int pSlot, const char *pReqName, float pValue, bool pDisplayAsInt, bool pDisplayAsCheck)
{
    //--Sets how many of something an achievement must have to be marked as successful. The value cannot be zero.
    if(!pName || !pReqName || pValue <= 0.0f) return;

    //--Get the achievement.
    AdvMenuJournalAchievementEntry *rAchievement = LocateAchievement(pName);
    if(!rAchievement) return;

    //--Range check the slot.
    if(pSlot < 0 || pSlot >= rAchievement->mProgressEntriesTotal) return;

    //--Set.
    if(!strcasecmp(pReqName, "NULL"))
    {
        ResetString(rAchievement->mProgressEntries[pSlot].mDisplayName, NULL);
    }
    else
    {
        ResetString(rAchievement->mProgressEntries[pSlot].mDisplayName, pReqName);
    }
    rAchievement->mProgressEntries[pSlot].mDisplayAsInteger = pDisplayAsInt;
    rAchievement->mProgressEntries[pSlot].mDisplayAsCheckbox = pDisplayAsCheck;
    rAchievement->mProgressEntries[pSlot].mValueMax = pValue;
}
void AdvUIJournal::SetAchievementProgress(const char *pName, int pSlot, float pValue)
{
    //--Sets how many of something an achievement currently has. Note that this does not unlock the achievement
    //  even if the value passes the max. That must be done by querying the value(s) as some may be more
    //  important than others. This is done in script.
    if(!pName) return;

    //--Get the achievement.
    AdvMenuJournalAchievementEntry *rAchievement = LocateAchievement(pName);
    if(!rAchievement) return;

    //--Range check the slot.
    if(pSlot < 0 || pSlot >= rAchievement->mProgressEntriesTotal) return;

    //--Set.
    rAchievement->mProgressEntries[pSlot].mValueCur = pValue;
}
void AdvUIJournal::SetAchievementLocked(const char *pName, bool pIsLocked)
{
    //--Locked achievements do not display their details until unlocked.
    if(!pName) return;

    //--Get the achievement.
    AdvMenuJournalAchievementEntry *rAchievement = LocateAchievement(pName);
    if(!rAchievement) return;

    //--Set.
    rAchievement->mHidesWhenNotUnlocked = pIsLocked;
}
void AdvUIJournal::SetAchievementSpoilered(const char *pName, bool pIsSpoilered)
{
    //--Spoilered achievements do not display their details until unlocked, or the player presses a button.
    if(!pName) return;

    //--Get the achievement.
    AdvMenuJournalAchievementEntry *rAchievement = LocateAchievement(pName);
    if(!rAchievement) return;

    //--Set.
    rAchievement->mIsSpoilered = pIsSpoilered;
}
void AdvUIJournal::UnlockAchievement(const char *pName)
{
    ///--[Basic]
    //--Flags an achievement as unlocked and gives it a timestamp to mark the occasion.
    if(!pName) return;

    //--Get the achievement.
    AdvMenuJournalAchievementEntry *rAchievement = LocateAchievement(pName);
    if(!rAchievement) return;

    //--If the achievement is already unlocked, don't do anything.
    //if(rAchievement->mIsUnlocked) return;

    //--Mark it as unlocked.
    rAchievement->mIsUnlocked = true;

    //--Get the current timestamp, store it on the achievement.
    char tBuffer[128];
    time_t tRawTime;
    tm *tTimeInfo = NULL;
    time(&tRawTime);
    tTimeInfo = localtime(&tRawTime);
    strftime(tBuffer, sizeof(char) * 128, "%c", tTimeInfo);
    ResetString(rAchievement->mUnlockTimestamp, tBuffer);

    ///--[Steam]
    //--If Steam is active, attempt to unlock this achievement.
    #if defined _STEAM_API_
    //fprintf(stderr, "Unlocking achievement %s\n", pName);
    //fprintf(stderr, "Scanning %i remaps for steam name.\n", xSteamNameRemapsTotal);

    //--Scan the remaps.
    for(int i = 0; i < xSteamNameRemapsTotal; i ++)
    {
        //fprintf(stderr, "Comparing: %s to %s\n", pName, xSteamNameRemaps[i].mProgramName);
        if(!strcasecmp(xSteamNameRemaps[i].mProgramName, pName))
        {
            //fprintf(stderr, " Remap found in slot %i. Remap: %s\n", i, xSteamNameRemaps[i].mSteamName);
            SteamManager::Fetch()->UnlockAchievementS(xSteamNameRemaps[i].mSteamName);
            break;
        }
    }

    #endif
}
void AdvUIJournal::AllocateAchievementRemaps(int pTotal)
{
    ///--[Documentation]
    //--The program and steam names for an achievement may be different. If so, this list is checked to remap
    //  them when unlocking.
    for(int i = 0; i < xSteamNameRemapsTotal; i ++)
    {
        free(xSteamNameRemaps[i].mProgramName);
        free(xSteamNameRemaps[i].mSteamName);
    }
    free(xSteamNameRemaps);

    //--Zero.
    xSteamNameRemapsTotal = 0;
    xSteamNameRemaps = NULL;
    if(pTotal < 1) return;

    //--Allocate. Set to NULL.
    xSteamNameRemapsTotal = pTotal;
    xSteamNameRemaps = (AdvMenuJournalSteamNameRemap *)starmemoryalloc(sizeof(AdvMenuJournalSteamNameRemap) * xSteamNameRemapsTotal);
    for(int i = 0; i < xSteamNameRemapsTotal; i ++)
    {
        xSteamNameRemaps[i].mProgramName = NULL;
        xSteamNameRemaps[i].mSteamName = NULL;
    }
}
void AdvUIJournal::SetAchievementRemap(int pSlot, const char *pProgramName, const char *pSteamName)
{
    //--Sets the remap and steam names.
    if(pSlot < 0 || pSlot >= xSteamNameRemapsTotal) return;
    ResetString(xSteamNameRemaps[pSlot].mProgramName, pProgramName);
    ResetString(xSteamNameRemaps[pSlot].mSteamName, pSteamName);
}
void AdvUIJournal::CrossReferenceAchievements()
{
    ///--[Documentation and Setup]
    //--Once all achievements have been registered to the journal in Lua, allocates and registers them in Steam.
    //  If Steam is not active, does nothing.
    #if defined _STEAM_API_
    SteamManager *rSteamManager = SteamManager::Fetch();
    //fprintf(stderr, "Crossreferencing achievements.\n");

    ///--[Allocation]
    //--Count how many achievements we have across all categories.
    int tTotalAchievements = 0;
    AdvMenuJournalAchievementCategory *rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCheckCategory)
    {
        tTotalAchievements = tTotalAchievements + rCheckCategory->mAchievementList->GetListSize();
        rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    //--Allocate.
    //fprintf(stderr, " Total achievements: %i\n", tTotalAchievements);
    rSteamManager->AllocateAchievements(tTotalAchievements);

    ///--[Setting]
    //--Set all achievements with their internal name. Iterate across all achievements.
    int i = 0;
    //fprintf(stderr, " Setting achievements.\n");
    rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCheckCategory)
    {
        //--Iterate across the list.
        AdvMenuJournalAchievementEntry *rCheckPtr = (AdvMenuJournalAchievementEntry *)rCheckCategory->mAchievementList->PushIterator();
        while(rCheckPtr)
        {
            //--Fast-access pointer.
            const char *rProgramName = rCheckCategory->mAchievementList->GetIteratorName();

            //--The names may not necessarily match up between the internal name and the steam name. Check if there's a remap.
            for(int p = 0; p < xSteamNameRemapsTotal; p ++)
            {
                //--Match.
                if(!strcasecmp(xSteamNameRemaps[p].mProgramName, rProgramName))
                {
                    //--Set the basics.
                    rSteamManager->SetAchievement(i, xSteamNameRemaps[p].mSteamName);
                    //fprintf(stderr, " %i: %s - %s\n", i, rProgramName, xSteamNameRemaps[p].mSteamName);

                    //--If an achievement is currently unlocked in the code but not on Steam, unlock it.
                    if(rCheckPtr->mIsUnlocked)
                    {
                        //fprintf(stderr, "  Unlocked.\n");
                        rSteamManager->UnlockAchievement(i);
                    }
                    break;
                }
            }

            //--Next achievement.
            i ++;
            rCheckPtr = (AdvMenuJournalAchievementEntry *)rCheckCategory->mAchievementList->AutoIterate();
        }

        //--Next category.
        rCheckCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    #endif
}

///=========================================== Update =============================================
void AdvUIJournal::RecomputeJournalAchievementCursorPos()
{
    ///--[Documentation]
    //--Determines the highlight position on the left side of the screen or the selected
    //  achievement block.
    //--This should be called from RecomputeCursorPositions() in achievements mode, not directly.
    float cSelectionLft =  47.0f;
    float cSelectionTop = 112.0f;
    float cSelectionHei =  20.0f;

    ///--[Resolve]
    //--Compute cursor position.
    int tCursor = mAchievementCategoryCursor - mAchievementCategoryOffset;

    //--Get entry.
    AdvMenuJournalAchievementCategory *rEntry = (AdvMenuJournalAchievementCategory *)xAchievementCategories->GetElementBySlot(mAchievementCategoryCursor);
    if(!rEntry) return;

    //--Length.
    float cNameLen = AdvImages.rFont_Mainline->GetTextWidth(rEntry->mDisplayName) + 18.0f; // Add 18 for the icon width.

    ///--[Upload]
    //--Positions.
    float cLft = cSelectionLft - 3.0f;
    float cTop = cSelectionTop + (cSelectionHei * tCursor) + 0.0f;
    float cRgt = cSelectionLft + cNameLen + 3.0f;
    float cBot = cSelectionTop + (cSelectionHei * tCursor) + (cSelectionHei) + 4.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft,        cTop,        ADVMENU_JOURNAL_CURSOR_TICKS);
    mHighlightSize.MoveTo(cRgt - cLft, cBot - cTop, ADVMENU_JOURNAL_CURSOR_TICKS);
}
void AdvUIJournal::UpdateJournalAchievements()
{
    ///--[Documentation and Setup]
    //--Update handler. This is called after timers in the main update.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Toggle Achievements Mode]
    if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
    {
        //--If the party has zero members, then we're either in a debug situation or on the Nowhere menu.
        //  In either case, don't allow returning to the base journal.
        bool tPlayerPartyHasMembers = (AdvCombat::Fetch()->GetActivePartyCount() > 0);
        if(tPlayerPartyHasMembers)
        {
            mIsAchievementsMode = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }

        //--Recompute.
        RecomputeCursorPositions();
        return;
    }

    ///--[Timers]
    mAchievementScroll.Increment(EASING_CODE_QUADOUT);

    ///--[Arrow Keys]
    //--Setup.
    bool tRecompute = false;

    //--Up and Down handled by standard.
    tRecompute = AutoListUpDn(mAchievementCategoryCursor, mAchievementCategoryOffset, xAchievementCategories->GetListSize(), cxScrollbarBuf, cxScrollbarListPage, true);
    if(tRecompute)
    {
        mAchievementEntryCursor = 0;
        mAchievementScroll.MoveTo(0.0f, 0);
    }

    //--Uplevel and Dnlevel are handled by the standard and scroll the achievement entry offset, but are not actually
    //  the cursor and don't trigger a recompute.
    int tOldEntryCursor = mAchievementEntryCursor;
    AdvMenuJournalAchievementCategory *rActiveCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->GetElementBySlot(mAchievementCategoryCursor);
    if(rActiveCategory)
    {
        //--Get achievement list, check its size.
        StarLinkedList *rAchievementList = rActiveCategory->mAchievementList;
        int tUseSize = rAchievementList->GetListSize() - cxScrollbarAchListPage + 1;
        if(tUseSize < 1) tUseSize = 0;

        //--Handle controls. Recompute is only checked for SFX.
        AutoListControl("UpLevel", "DnLevel", "Ctrl", mAchievementEntryCursor, mAchievementEntryOffset, tUseSize, cxScrollbarAchBuf, cxScrollbarAchListPage, true);

        //--If the position changed, recompute the target offset.
        if(mAchievementEntryCursor != tOldEntryCursor)
        {
            //--Run across the entries.
            int i = 0;
            float tRunningTotal = 0.0f;
            AdvMenuJournalAchievementEntry *rEntry = (AdvMenuJournalAchievementEntry *)rAchievementList->PushIterator();
            while(rEntry)
            {
                //--If we hit the cursor, stop iterating.
                if(i >= mAchievementEntryCursor)
                {
                    rAchievementList->PopIterator();
                    break;
                }

                //--Add to the Y position.
                tRunningTotal = tRunningTotal + ComputeEntryHeight(rEntry);

                //--Next.
                i ++;
                rEntry = (AdvMenuJournalAchievementEntry *)rAchievementList->AutoIterate();
            }

            //--Set.
            mAchievementScroll.MoveTo(tRunningTotal, ADVMENU_JOURNAL_ACHIEVEMENT_SCROLL_TICKS);

            //--SFX.
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    ///--[Finish Up]
    //--If this flag is true, recompute the highlight and play a sound effect.
    if(tRecompute)
    {
        //--Recalculate.
        RecomputeCursorPositions();

        //--SFX.
        //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Toggle Spoilers]
    //--Pressing run toggles spoilers on or off.
    if(rControlManager->IsFirstPress("Run"))
    {
        mHideSpoilers = !mHideSpoilers;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    ///--[Close]
    //--Pressing cancel returns to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        HandleJournalModeChange();
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}

///========================================== Drawing =============================================
void AdvUIJournal::RenderJournalAchievements()
{
    ///--[Documentation and Setup]
    //--Achievements mode. Gets its own submenu.
    if(!mImagesReady) return;

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, ADVMENU_JOURNAL_VIS_TICKS);
    if(cAlpha <= 0.0f) return;

    ///--[Static Parts]
    //--Common universal parts.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Background.
    AdvImages.rStaticPartsAchievements->Draw();

    //--Major Header.
    mColorHeading.SetAsMixerAlpha(cAlpha);
    if(!AdventureMenu::xUseAntchievement)
        AdvImages.rFont_BigHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, -6.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xAchievementTitle);
    else
        AdvImages.rFont_BigHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, -6.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Antchievements");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Help Strings]
    //--Left-aligned Help.
    mReturnString->           DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mMove10String->           DrawText(0.0f, cxAdvMainlineFontH * 1.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mToggleSpoilerString->    DrawText(0.0f, cxAdvMainlineFontH * 2.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mScrollAchievementString->DrawText(0.0f, cxAdvMainlineFontH * 3.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    //--Right-aligned Help.
    mHideAchievementsString->DrawText(VIRTUAL_CANVAS_X, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR | SUGARFONT_RIGHTALIGN_X, 1.0f, AdvImages.rFont_Mainline);


    ///--[Scrollbar]
    //--If there are over ADVMENU_JOURNAL_ENTRIES_ON_LIST entries, we need a scrollbar.
    if(xAchievementCategories->GetListSize() > ADVMENU_JOURNAL_ENTRIES_ON_LIST)
    {
        StandardRenderScrollbar(mAchievementCategoryOffset, ADVMENU_JOURNAL_ENTRIES_ON_LIST, xAchievementCategories->GetListSize() - ADVMENU_JOURNAL_ENTRIES_ON_LIST, 142.0f, 522.0f, true, AdvImages.rScrollbarFront, AdvImages.rScrollbarBack);
    }

    ///--[Category Listing]
    //--Font
    StarFont *rUseFont = AdvImages.rFont_Mainline;

    //--Render Constants
    float cSelectionLft = 47.0f;
    float cSelectionTop = 112.0f;
    float cSelectionHei = rUseFont->GetTextHeight();

    //--Variables
    int tRenders = 0;
    int tOption = 0;
    float tYPos = cSelectionTop;

    //--Iterate.
    AdvMenuJournalAchievementCategory *rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCategory)
    {
        //--If we haven't reached the start-at value, don't render anything.
        if(tOption < mAchievementCategoryOffset)
        {
            tOption ++;
            rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
            continue;
        }

        //--Render.
        tRenders ++;
        rUseFont->DrawText(cSelectionLft + 18.0f, tYPos, 0, 1.0f, rCategory->mDisplayName);

        //--If there's an icon associated with this entry, render it right-aligned.
        if(rCategory->rIcon)
        {
            //--Compute position.
            float cTopSpot = tYPos + (cSelectionHei + 1.0f) - (rCategory->rIcon->GetTrueHeight());
            float cLftSpot = cSelectionLft;

            //--Position, scale.
            glTranslatef(cLftSpot, cTopSpot, 0.0f);

            //--Render.
            rCategory->rIcon->Draw();

            //--Clean.
            glTranslatef(-cLftSpot, -cTopSpot, 0.0f);
        }

        //--If we hit the end of the list, stop rendering:
        if(tRenders >= ADVMENU_JOURNAL_ENTRIES_ON_LIST)
        {
            xAchievementCategories->PopIterator();
            break;
        }

        //--Next.
        tOption ++;
        tYPos = tYPos + cSelectionHei;
        rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    ///--[Achievement Listing]
    //--For each achievement in the selected category, render a backing, icon, description, etc. We render one before and
    //  one after the range requirement to handle scrolling.
    AdvMenuJournalAchievementCategory *rActiveCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->GetElementBySlot(mAchievementCategoryCursor);
    if(rActiveCategory)
    {
        ///--[Stencil Block]
        //--Rendering block.
        TwoDimensionReal tRenderBlock;
        tRenderBlock.SetWH(510.0f, 108.0f, 816.0f, 620.0f);

        //--Keeps things from rendering out of range.
        glEnable(GL_STENCIL_TEST);
        glColorMask(false, false, false, false);
        glDepthMask(false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        glStencilMask(0xFF);
        StarBitmap::DrawRectFill(tRenderBlock.mLft, tRenderBlock.mTop, tRenderBlock.mRgt, tRenderBlock.mBot, StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

        //--Switch stencilling to only draw where the rectangle was.
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilMask(0xFF);

        //--Setup.
        float tYPos = mAchievementScroll.mXCur * -1.0f;

        ///--[Rendering Loop]
        //--Iterate.
        AdvMenuJournalAchievementEntry *rEntry = (AdvMenuJournalAchievementEntry *)rActiveCategory->mAchievementList->PushIterator();
        while(rEntry)
        {
            ///--[Range Checks]
            //--Compute the entry size. It's cBlockHei plus one cDescHei for each requirement.
            float cThisEntryHei = ComputeEntryHeight(rEntry);

            //--If this entry would render off the top of the screen, skip it.
            if(tYPos + cThisEntryHei < 0.0f)
            {
                tYPos = tYPos + cThisEntryHei;
                rEntry = (AdvMenuJournalAchievementEntry *)rActiveCategory->mAchievementList->AutoIterate();
                continue;
            }

            //--If this entry would render off the bottom of the screen, stop rendering.
            if(tYPos > tRenderBlock.mBot - tRenderBlock.mTop)
            {
                rActiveCategory->mAchievementList->PopIterator();
                break;
            }

            ///--[Rendering]
            RenderJournalAchievement(tYPos, rEntry, cAlpha);

            ///--[Next]
            //--Advance by the size.
            tYPos = tYPos + cThisEntryHei;
            rEntry = (AdvMenuJournalAchievementEntry *)rActiveCategory->mAchievementList->AutoIterate();
        }

        //--Clean stencilling.
        glDisable(GL_STENCIL_TEST);

        //--Scrollbar if needed.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        if(rActiveCategory->mAchievementList->GetListSize() > ADVMENU_JOURNAL_ACHIEVEMENT_PER_PAGE)
        {
            StandardRenderScrollbar(mAchievementEntryCursor, ADVMENU_JOURNAL_ACHIEVEMENT_PER_PAGE, rActiveCategory->mAchievementList->GetListSize() - ADVMENU_JOURNAL_ACHIEVEMENT_PER_PAGE, 136.0f, 520.0f, true, AdvImages.rScrollbarFront, AdvImages.rAchievementScrollbarBack);
        }
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);


    ///--[Highlight]
    if(!mShowFullBestiaryImage)RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    ///--[Overlay]
    AdvImages.rStaticPartsAchievementsOver->Draw();

    ///--[Clean Up]
    StarlightColor::ClearMixer();
}
void AdvUIJournal::RenderJournalAchievement(float pYPos, AdvMenuJournalAchievementEntry *pEntry, float pAlpha)
{
    ///--[Documentation]
    //--Given an achievement, renders it at the given Y position. Because there's a lot of possibilities with achievements,
    //  this subroutine makes the above code more legible.
    if(!pEntry) return;

    //--Setup.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Constants.
    float cBlockHei =   90.0f;
    float cIconIndX =  518.0f;
    float cIconIndY =  116.0f;
    float cNameIndX =  584.0f;
    float cNameIndY =  111.0f;
    float cDescIndX =  594.0f;
    float cDescIndY =  134.0f;
    float cDescHei  =   15.0f;
    float cUnlIndX  = 1182.0f;
    float cUnlIndY  =  111.0f;
    float cTimeIndX = 1182.0f;
    float cTimeIndY =  131.0f;
    float cProgBufX = 15.0f;
    float cProgIndX = cDescIndX + cProgBufX;

    ///--[Static Parts]
    //--Compute entry height.
    float cThisEntryHei = ComputeEntryHeight(pEntry);

    //--Backing. Always at normal color. Stretches with special code to handle progress requirements.
    if(cThisEntryHei == cBlockHei)
    {
        AdvImages.rAchievementBlock->Draw(0.0f, pYPos);
    }
    //--Needs to be stretched.
    else
    {
        //--Compute scale.
        float cScale = (cThisEntryHei - 10.0f) / 80.0f;

        //--Position.
        float cXPos = AdvImages.rAchievementBlock->GetXOffset();
        float cYPos = pYPos + AdvImages.rAchievementBlock->GetYOffset();
        glTranslatef(cXPos, cYPos, 0.0f);
        glScalef(1.0f, cScale, 1.0f);

        //--Render.
        AdvImages.rAchievementBlock->Bind();
        AdvImages.rAchievementBlock->RenderAt();

        //--Unposition.
        glScalef(1.0f, 1.0f / cScale, 1.0f);
        glTranslatef(-cXPos, -cYPos, 0.0f);
    }

    ///--[Hidden When Not Unlocked]
    //--Hidden achievements show a lock icon and a standard display. When unlocked, this stops.
    if(pEntry->mHidesWhenNotUnlocked && !pEntry->mIsUnlocked)
    {
        //--Icon.
        AdvImages.rAchievementIconLocked->Draw(cIconIndX, cIconIndY + pYPos);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        //--Display Name.
        AdvImages.rFont_AchievementTitle->DrawText(cNameIndX, cNameIndY + pYPos, 0, 1.0f, "Locked");

        //--Description.
        AdvImages.rFont_AchievementDescription->DrawText(cDescIndX, cDescIndY + pYPos, 0, 1.0f, "This achievement is hidden until it is unlocked.");
        return;
    }

    ///--[Spoilered When Not Unlocked]
    //--These achievements can be revealed by pressing a key.
    if(pEntry->mIsSpoilered && !pEntry->mIsUnlocked && mHideSpoilers)
    {
        //--Icon.
        AdvImages.rAchievementIconLocked->Draw(cIconIndX, cIconIndY + pYPos);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

        //--Display Name.
        AdvImages.rFont_AchievementTitle->DrawText(cNameIndX, cNameIndY + pYPos, 0, 1.0f, "Spoiler!");

        //--Description.
        AdvImages.rFont_AchievementDescription->DrawText(cDescIndX, cDescIndY + pYPos, 0, 1.0f, "This achievement is a possible spoiler. Push the run key to reveal it.");
        return;
    }

    //--Icon, if applicable. Renders darkened if the achievement is not unlocked.
    if(!pEntry->mIsUnlocked) StarlightColor::SetMixer(0.2f, 0.2f, 0.2f, pAlpha);
    if(pEntry->rImage) pEntry->rImage->Draw(cIconIndX, cIconIndY + pYPos);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Display Name.
    AdvImages.rFont_AchievementTitle->DrawText(cNameIndX, cNameIndY + pYPos, 0, 1.0f, pEntry->mDisplayTitle);

    //--Description.
    for(int i = 0; i < pEntry->mDescriptionLines; i ++)
    {
        AdvImages.rFont_AchievementDescription->DrawText(cDescIndX, cDescIndY + pYPos + (cDescHei * i), 0, 1.0f, pEntry->mDescription[i]);
    }

    //--If unlocked, print when:
    if(pEntry->mIsUnlocked && pEntry->mUnlockTimestamp)
    {
        AdvImages.rFont_AchievementDescription->DrawText(cUnlIndX,  cUnlIndY  + pYPos, 0, 1.0f, "Unlocked:");
        AdvImages.rFont_AchievementDescription->DrawText(cTimeIndX, cTimeIndY + pYPos, 0, 1.0f, pEntry->mUnlockTimestamp);
    }

    ///--[Requirements]
    //--Renders additional information below the description about progress towards achievement. Most achievements
    //  do not have these.
    float cProgressBarWid = 250.0f;
    float cProgressBarHei = cDescHei;
    StarlightColor cProgBarFill = StarlightColor::MapRGBAF(0.6f, 0.8f, 0.3f, pAlpha);
    StarlightColor cProgBarBack = StarlightColor::MapRGBAF(0.3f, 0.3f, 0.3f, pAlpha);
    StarlightColor cProgBarFrme = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, pAlpha);

    //--Render position.
    float tYRender = cDescIndY + pYPos + (cDescHei * pEntry->mDescriptionLines);

    //--Iterate.
    for(int i = 0; i < pEntry->mProgressEntriesTotal; i ++)
    {
        ///--[Basics]
        //--Compute Y position.
        float tProgBarX = cProgIndX;

        ///--[Checkbox Case]
        //--If this entry is a checkbox, it does not render a progress bar. Instead it renders a cute little X or empty box.
        if(pEntry->mProgressEntries[i].mDisplayAsCheckbox)
        {
            //--Checkbox is on:
            if(pEntry->mProgressEntries[i].mValueCur >= pEntry->mProgressEntries[i].mValueMax)
            {
                AdvImages.rAchievementCheckYes->Draw(tProgBarX, tYRender + 4.0f);
            }
            //--Checkbox is off:
            else
            {
                AdvImages.rAchievementCheckNo->Draw(tProgBarX, tYRender + 4.0f);
            }

            //--For checkboxes, the name is printed to the right.
            if(pEntry->mProgressEntries[i].mDisplayName)
            {
                AdvImages.rFont_AchievementDescription->DrawText(tProgBarX + 16.0f, tYRender+1.0f, 0, 1.0f, pEntry->mProgressEntries[i].mDisplayName);
            }

            //--Advance the Y cursor.
            tYRender = tYRender + cDescHei;
        }
        //--If displaying as a number:
        else
        {
            //--If this has a name, render it above to the progress bar. This also moves the rendering cursor down.
            if(pEntry->mProgressEntries[i].mDisplayName)
            {
                AdvImages.rFont_AchievementDescription->DrawText(cProgIndX, tYRender, 0, 1.0f, pEntry->mProgressEntries[i].mDisplayName);
                tYRender = tYRender + cDescHei;
            }
            else
            {
                tProgBarX = cProgIndX - cProgBufX;
            }

            //--Progress Bar. Has a frame.
            StarBitmap::DrawRectFill(tProgBarX,      tYRender+2.0f, tProgBarX + cProgressBarWid,      tYRender + cProgressBarHei,      cProgBarFrme);
            StarBitmap::DrawRectFill(tProgBarX+1.0f, tYRender+3.0f, tProgBarX + cProgressBarWid-1.0f, tYRender + cProgressBarHei-1.0f, cProgBarBack);

            //--Get the progress percentage. If zero, don't render a fill.
            float tProgressPct = pEntry->mProgressEntries[i].mValueCur / pEntry->mProgressEntries[i].mValueMax;
            if(tProgressPct > 1.0f) tProgressPct = 1.0f;
            if(tProgressPct > 0)
            {
                StarBitmap::DrawRectFill(tProgBarX+1, tYRender + 3.0f, tProgBarX + ((cProgressBarWid-2.0f) * tProgressPct), tYRender + cProgressBarHei - 1.0f, cProgBarFill);
            }

            //--Progress values. Integer version.
            if(pEntry->mProgressEntries[i].mDisplayAsInteger)
            {
                AdvImages.rFont_AchievementDescription->DrawTextArgs(tProgBarX + cProgressBarWid + 3.0f, tYRender - 1.0f, 0, 1.0f, "%i / %i", (int)pEntry->mProgressEntries[i].mValueCur, (int)pEntry->mProgressEntries[i].mValueMax);
            }
            //--Floating point version.
            else
            {
                AdvImages.rFont_AchievementDescription->DrawTextArgs(tProgBarX + cProgressBarWid + 3.0f, tYRender - 1.0f, 0, 1.0f, "%.1f / %.1f", pEntry->mProgressEntries[i].mValueCur, pEntry->mProgressEntries[i].mValueMax);
            }

            //--Advance the Y cursor.
            tYRender = tYRender + cDescHei;
        }
    }
}

///========================================= File I/O =============================================
void AdvUIJournal::WriteAchievementsToFile()
{
    ///--[Documentation]
    //--Writes achievement data to a savefile in a fixed position. All achievements currently registered in
    //  the Journal get written, even if they are not unlocked, but will be written with zeroes.
    StarAutoBuffer *tBuffer = new StarAutoBuffer();

    //--Warn when the program attempts to write achievements with zero entries.
    if(xAchievementCategories->GetListSize() < 1)
    {
        //--Debug.
        fprintf(stderr, "Warning: Tried to write achievements with %i categories.\n", xAchievementCategories->GetListSize());
        fprintf(stderr, " Printing Lua call stack.\n");

        //--Setup.
        LuaManager *rLuaManager = LuaManager::Fetch();
        int tCallStackCount = 0;
        char *rCallStackString = rLuaManager->GetCallStack(tCallStackCount);
        while(rCallStackString)
        {
            //--Print.
            fprintf(stderr, " %i: %s\n", tCallStackCount, rCallStackString);

            //--Next.
            tCallStackCount ++;
            rCallStackString = rLuaManager->GetCallStack(tCallStackCount);
        }
        return;
    }

    ///--[Achievement List]
    //--Create a list of all achievements regardless of category.
    StarLinkedList *trAllAchievementList = new StarLinkedList(false);
    AdvMenuJournalAchievementCategory *rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->PushIterator();
    while(rCategory)
    {
        //--Iterate across all entries.
        AdvMenuJournalAchievementEntry *rEntry = (AdvMenuJournalAchievementEntry *)rCategory->mAchievementList->PushIterator();
        while(rEntry)
        {
            //--Add to list.
            trAllAchievementList->AddElementAsTail(rCategory->mAchievementList->GetIteratorName(), rEntry);

            //--Next.
            rEntry = (AdvMenuJournalAchievementEntry *)rCategory->mAchievementList->AutoIterate();
        }

        //--Next.
        rCategory = (AdvMenuJournalAchievementCategory *)xAchievementCategories->AutoIterate();
    }

    ///--[Header]
    //--This is not a standard savefile or SLF.
    tBuffer->AppendStringWithoutNull("STARANTS100");

    //--Achievement count.
    tBuffer->AppendInt32(trAllAchievementList->GetListSize());

    ///--[Achievement Listing]
    //--Write the status of all achievements to the list.
    AdvMenuJournalAchievementEntry *rEntry = (AdvMenuJournalAchievementEntry *)trAllAchievementList->PushIterator();
    while(rEntry)
    {
        //--Write the internal name.
        tBuffer->AppendStringWithLen(trAllAchievementList->GetIteratorName());

        //--Write an 8-bit integer. 0 if locked, 1 if unlocked.
        if(rEntry->mIsUnlocked)
        {
            tBuffer->AppendInt8(0x01);
        }
        else
        {
            tBuffer->AppendInt8(0x00);
        }

        //--Timestamp. Should be YYYYMMDDHHMMSS. If you're playing this in the year 10,000, you're screwed.
        //  If the achievement is locked, append a dummy string.
        if(rEntry->mUnlockTimestamp)
        {
            tBuffer->AppendStringWithLen(rEntry->mUnlockTimestamp);
        }
        else
        {
            tBuffer->AppendStringWithLen("YYYYMMDDHHMMSS");
        }

        //--Progress. First, write how many we have. If the progress for an achievement changes, we will still read
        //  the values but may not use all of them, or not have values for all of them.
        tBuffer->AppendInt32(rEntry->mProgressEntriesTotal);
        for(int i = 0; i < rEntry->mProgressEntriesTotal; i ++)
        {
            tBuffer->AppendFloat(rEntry->mProgressEntries[i].mValueCur);
        }

        //--Next.
        rEntry = (AdvMenuJournalAchievementEntry *)trAllAchievementList->AutoIterate();
    }

    ///--[File Location]
    //--File location is based on where the adventure folder is.
    const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    char *tFileBuf = InitializeString("%s/../../Saves/Plantchievements.slf", rAdventureDir);

    //--Secondary location. Only used on certain builds. Saves to the user profile directory.
    //  If the directory doesn't exist, creates it if possible.
    char *tAltBuf = NULL;
    #if defined _USE_USER_DIRS_
    const char *rUserProfile = getenv("USERPROFILE");
    if(rUserProfile)
    {
        //--Create alternate directory.
        tAltBuf = InitializeString("%s/AppData/Local/RunesOfPandemonium/Plantchievements.slf", rUserProfile);
        //fprintf(stderr, "Creating Alternate path: %s\n", tAltBuf);

        //--We need to check each of the folders in order to see if they exist. Right now it is assumed that the AppData and Local
        //  folders should always exist, these are basic to Windows.
        char *tTempFolder = InitializeString("%s/AppData/Local/RunesOfPandemonium", rUserProfile);
        int tErrorCode = _mkdir(tTempFolder);
        if(tErrorCode)
        {
            //--Ignore EEXIST because that just means the directory already exists.
            if(errno != EEXIST)
                fprintf(stderr, "_mkdir(): Error, code %i. Versus EEXIST: %i and ENOENT: %i\n", errno, EEXIST, ENOENT);
        }

        //--Clean.
        free(tTempFolder);
    }
    #endif

    //--Open the file, write the buffer out.
    int tBufLen = tBuffer->GetCursor();
    uint8_t *rRawData = tBuffer->LiberateData();
    FILE *fOutfile = fopen(tFileBuf, "wb");
    if(fOutfile)
    {
        fwrite(rRawData, tBufLen, 1, fOutfile);
        fclose(fOutfile);
    }

    //--Try to open the alt directory and write the file there as well.
    if(tAltBuf)
    {
        fOutfile = fopen(tAltBuf, "wb");
        if(fOutfile)
        {
            fwrite(rRawData, tBufLen, 1, fOutfile);
            fclose(fOutfile);
        }
    }

    ///--[Clean]
    delete trAllAchievementList;
    delete tBuffer;
    free(rRawData);
    free(tFileBuf);
    free(tAltBuf);

    ///--[Debug]
    //fprintf(stderr, "Finished writing achievements.\n");
}
void AdvUIJournal::ReadAchievementsFromFile()
{
    ///--[Documentation]
    //--Reads achievement data from a savefile in a fixed position. This must be done after achievements
    //  are registered, as the savefile only loads their data, it does not create new ones. If an
    //  achievement is outdated, it is lost.
    //--Note: For Steam compatibility, some systems overwrite the achievements file or fail to store it.
    //  We thus use the user directory first if present. This is USERDIR/Local/AppData/RunesOfPandemonium/
    //  The alt directory gets priority if it exists. Non-Steam versions use a local install and ignore this.
    //  The definition of _USE_USER_DIRS_ controls this behavior.

    ///--[File Location]
    //--File location is based on where the adventure folder is.
    const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    char *tFileBuf = InitializeString("%s/../../Saves/Plantchievements.slf", rAdventureDir);
    //fprintf(stderr, "Reading achievement from %s\n", tFileBuf);

    //--Assemble the alternate directory for the achievements file.
    char *tAltBuf = NULL;
    #if defined _USE_USER_DIRS_
    const char *rUserProfile = getenv("USERPROFILE");
    if(rUserProfile)
    {
        tAltBuf = InitializeString("%s/AppData/Local/RunesOfPandemonium/Plantchievements.slf", rUserProfile);
        //fprintf(stderr, "Alternate path: %s\n", tAltBuf);
    }
    #endif

    //--Open the file as a VirtualFile.
    VirtualFile *fInfile = new VirtualFile(tAltBuf, false);
    if(!fInfile->IsReady())
    {
        //--Attempt to use the alternate path, if it exists.
        if(tFileBuf)
        {
            //--Reset.
            delete fInfile;
            fInfile = new VirtualFile(tFileBuf, false);

            //--If the file still isn't found, fail here.
            if(!fInfile->IsReady())
            {
                free(tFileBuf);
                free(tAltBuf);
                delete fInfile;
                return;
            }
        }
        //--No alternate file, fail immediately.
        else
        {
            free(tFileBuf);
            free(tAltBuf);
            delete fInfile;
            return;
        }
    }

    //--Flag. All strings are two bytes in length.
    fInfile->SetUseOneByteForStringLengths(false);

    //--Check the header.
    char tHeaderBuf[12];
    fInfile->Read(tHeaderBuf, sizeof(char) * 11, 1);
    tHeaderBuf[11] = '\0';
    if(strcasecmp(tHeaderBuf, "STARANTS100"))
    {
        DebugManager::ForcePrint("ReadAchievementsFromFile: Warning, incorrect header %s (should be 'STARANTS100')\n", tHeaderBuf);
    }

    //--Get how many achievements we expect.
    int32_t tTotalAchievements = 0;
    fInfile->Read(&tTotalAchievements, sizeof(int32_t), 1);
    //fprintf(stderr, "Achievements: %i\n", tTotalAchievements);

    ///--[Achievement Reading]
    for(int i = 0; i < tTotalAchievements; i ++)
    {
        //--Read the name.
        char *tAchievementName = fInfile->ReadLenString();
        //fprintf(stderr, " Achievement %i is %s\n", i, tAchievementName);

        //--Locate the achievement by searching all categories.
        AdvMenuJournalAchievementEntry *rAchievement = LocateAchievement(tAchievementName);
        if(rAchievement)
        {
            //--Read an 8-bit integer to get whether this is locked or unlocked.
            int8_t tIsUnlocked = 0;
            fInfile->Read(&tIsUnlocked, sizeof(int8_t), 1);
            if(tIsUnlocked != 0) rAchievement->mIsUnlocked = true;

            //--Timestamp.
            free(rAchievement->mUnlockTimestamp);
            rAchievement->mUnlockTimestamp = fInfile->ReadLenString();

            //--Progress. Read how many are expected. If the number goes out of range, read dummy values and discard them.
            int32_t tExpectedValues = 0;
            fInfile->Read(&tExpectedValues, sizeof(int32_t), 1);

            //--Read.
            for(int i = 0; i < tExpectedValues; i ++)
            {
                //--If this value is within range for the entry, use it.
                if(i < rAchievement->mProgressEntriesTotal)
                {
                    fInfile->Read(&rAchievement->mProgressEntries[i].mValueCur, sizeof(float), 1);
                }
                //--Dummy.
                else
                {
                    float tDummy = 0.0f;
                    fInfile->Read(&tDummy, sizeof(float), 1);
                }
            }
        }
        //--Not found, deprecated achievement. Read dummy values.
        else
        {
            int8_t tDummyInt;
            fInfile->Read(&tDummyInt, sizeof(int8_t), 1);
            char *tDummyString = fInfile->ReadLenString();
            free(tDummyString);

            //--Read dummy achievement progress.
            int32_t tExpectedValues = 0;
            fInfile->Read(&tExpectedValues, sizeof(int32_t), 1);
            for(int i = 0; i < tExpectedValues; i ++)
            {
                float tDummy = 0.0f;
                fInfile->Read(&tDummy, sizeof(float), 1);
            }
        }

        //--Clean.
        free(tAchievementName);
    }

    ///--[Clean]
    free(tFileBuf);
    free(tAltBuf);
    delete fInfile;

    ///--[Debug]
    //fprintf(stderr, "Finished reading achievements.\n");
}
