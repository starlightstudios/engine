//--Base
#include "TilemapActor.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
///===================================== Property Queries =========================================
bool TilemapActor::IsOverlayActive()
{
    return mIsShowingOverlayImages;
}
int TilemapActor::ComputeOverlayFrame()
{
    //--Returns zero on error.
    if(mOverlayImagesTotal < 1) return 0;

    //--Calculate the frame.
    float tCurFrame = (float)(mOverlayImageTimer / mOverlayImageTPF);

    //--Switch the frame to an integer and modulo by the frames. Frames cycle.
    int tActualFrame = ((int)tCurFrame % mOverlayImagesTotal);
    return tActualFrame;
}
StarBitmap *TilemapActor::GetOverlayFrameInSlot(int pSlot)
{
    if(pSlot < 0 || pSlot >= mOverlayImagesTotal) return NULL;
    return rOverlayImages[pSlot];
}
StarBitmap *TilemapActor::GetCurrentOverlayFrame()
{
    if(!mIsShowingOverlayImages) return NULL;
    int tFrame = ComputeOverlayFrame();
    if(tFrame < 0 || tFrame >= mOverlayImagesTotal) return NULL;
    return rOverlayImages[tFrame];
}

///======================================= Manipulators ===========================================
void TilemapActor::SetOverlaysActive(bool pFlag)
{
    mIsShowingOverlayImages = pFlag;
}
void TilemapActor::SetOverlayTPF(float pTPF)
{
    mOverlayImageTPF = pTPF;
    if(mOverlayImageTPF < 1.0f) mOverlayImageTPF = 1.0f;
}
void TilemapActor::AllocateOverlayFrames(int pTotal)
{
    //--Clear.
    free(rOverlayImages);

    //--Reset to zero.
    mOverlayImagesTotal = 0;
    rOverlayImages = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mOverlayImagesTotal = pTotal;
    rOverlayImages = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mOverlayImagesTotal);

    //--Clear.
    memset(rOverlayImages, 0, sizeof(StarBitmap *) * mOverlayImagesTotal);
}
void TilemapActor::SetOverlayFrame(int pSlot, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mOverlayImagesTotal) return;
    rOverlayImages[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void TilemapActor::UpdateOverlays()
{
    mOverlayImageTimer ++;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
