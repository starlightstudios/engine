//--Base
#include "StarCamera2D.h"

//--Classes
#include "StarBitmap.h"
#include "PlayerPony.h"

//--Definitions
#include "Global.h"
#include "HitDetection.h"

//--Generics
#include "StarLinkedList.h"

//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "OptionsManager.h"

///--[Local Definitions]
#define CAMERA_DEFAULT_X 0.0f
#define CAMERA_DEFAULT_Y 0.0f

///========================================== System ==============================================
StarCamera2D::StarCamera2D()
{
    ///--[StarCamera]
    ///--[StarCamera2D]
    //--System
    //--Zoom factor.
    m2DScale = 1.0f;
    mSmallestScale = 1.0f;
    mLargestScale = 10.0f;

    //--Zoom factor becomes clamped to 1.0f if this flag is flipped.
    //#ifndef DBG_ALLOW_CAMERA_ZOOMING
    //mSmallestScale = 1.0f;
    //mLargestScale = 1.0f;
    //#endif

    //--Spatial positioning.
    mCoordinates.mLft = CAMERA_DEFAULT_X;
    mCoordinates.mRgt = CAMERA_DEFAULT_X + (VIRTUAL_CANVAS_X);
    mCoordinates.mTop = CAMERA_DEFAULT_Y;
    mCoordinates.mBot = CAMERA_DEFAULT_Y + (VIRTUAL_CANVAS_Y);
    mCoordinates.mXCenter = (mCoordinates.mLft + mCoordinates.mRgt) / 2.0f;
    mCoordinates.mYCenter = (mCoordinates.mTop + mCoordinates.mBot) / 2.0f;
    mBoundaries.mLft = 0.0f;
    mBoundaries.mRgt = VIRTUAL_CANVAS_X;
    mBoundaries.mTop = 0.0f;
    mBoundaries.mBot = VIRTUAL_CANVAS_Y;
    mMaxMovesPerTick = 20;

    //--Non-polygonal collision zones.
    mUsePositiveCamera = false;
    mTotalCamZones = 0;
    mCamZoneList = NULL;

    //--Polygonal collision zones.
    mIs2DPolygonMode = false;
    mPolygonList = new StarLinkedList(true);

    //--Pushers.
    rCurrentPusher = NULL;
    mPusherList = new StarLinkedList(true);

    //--Locking and Clamping
    mLockCameraPipeline = false;
    mAllowMouseControl = false;
    mOldCameraFlag = false;
    mOldPolygonFlag = false;
    mOldMouseX = -1;
    mOldMouseY = -1;
}
StarCamera2D::~StarCamera2D()
{
    delete mPolygonList;
    delete mPusherList;
    free(mCamZoneList);
}

///===================================== Property Queries =========================================
bool StarCamera2D::Is2DCamera()
{
    return true;
}
ThreeDimensionReal StarCamera2D::GetDimensions()
{
    return mCoordinates;
}
TwoDimensionReal StarCamera2D::GetBoundaries()
{
    return mBoundaries;
}
bool StarCamera2D::IsObjectInFrame(ThreeDimensionReal pDimensions)
{
    return IsCollision3DReal(mCoordinates, pDimensions);
}
bool StarCamera2D::IsObjectInFrame(float pLft, float pTop, float pRgt, float pBot)
{
    return IsCollision3DReal(mCoordinates, pLft, pTop, pRgt, pBot);
}

///======================================= Manipulators ===========================================
void StarCamera2D::SetScale(float pScale)
{
    m2DScale = pScale;
}
void StarCamera2D::SetMaxMovesPerTick(int pMoves)
{
    //--Sets how many pixels the Camera may move per tick.  Pass a negative to restore defaults.
    if(pMoves < 1) pMoves = 7;
    mMaxMovesPerTick = pMoves;
}
void StarCamera2D::SetLockingState(bool pFlag)
{
    mLockCameraPipeline = pFlag;
}
void StarCamera2D::SetMouseControlFlag(bool pFlag)
{
    #ifdef DBG_ALLOW_CAMERA_UNLOCKING
    if(pFlag && !mAllowMouseControl)
    {
        mCameraStackX = mCoordinates.mLft;
        mCameraStackY = mCoordinates.mTop;
    }
    else if(!pFlag && mAllowMouseControl)
    {
        ForceTo(mCameraStackX, mCameraStackY);
    }
    mAllowMouseControl = pFlag;
    mLockCameraPipeline = pFlag;
    #endif
}

///======================================= Core Methods ===========================================
void StarCamera2D::OverrideX(float pX)
{
    mCoordinates.mLft = pX;
    mCoordinates.mRgt = pX + VIRTUAL_CANVAS_X;
    mCoordinates.mXCenter = (mCoordinates.mLft + mCoordinates.mRgt) / 2.0f;
}
void StarCamera2D::OverrideY(float pY)
{
    mCoordinates.mTop = pY;
    mCoordinates.mBot = pY + VIRTUAL_CANVAS_Y;
    mCoordinates.mYCenter = (mCoordinates.mTop + mCoordinates.mBot) / 2.0f;
}
void StarCamera2D::OverridePosition(float pX, float pY)
{
    //--Forcibly move the camera to a position, ignoring blockers and bounds.
    OverrideX(pX);
    OverrideY(pY);
}
void StarCamera2D::Clear()
{
    //--System
    //--Zooming
    m2DScale = 1.0f;
    mSmallestScale = 1.0f;
    mLargestScale = 10.0f;

    //--Zoom factor becomes clamped to 1.0f if this flag is flipped.
    #ifndef DBG_ALLOW_CAMERA_ZOOMING
    mSmallestScale = 1.0f;
    mLargestScale = 1.0f;
    #endif

    //--2D Polygon Mode
    mIs2DPolygonMode = false;
    mPolygonList->ClearList();

    //--2D Pushers
    rCurrentPusher = NULL;
    mPusherList->ClearList();

    //--2D Handling
    float tWidth = VIRTUAL_CANVAS_X;
    float tHeight = VIRTUAL_CANVAS_Y;
    mCoordinates.mLft = 0;
    mCoordinates.mRgt = 0 + tWidth;
    mCoordinates.mTop = 0;
    mCoordinates.mBot = 0 + tHeight;
    mCoordinates.mXCenter = (mCoordinates.mLft + mCoordinates.mRgt) / 2.0f;
    mCoordinates.mYCenter = (mCoordinates.mTop + mCoordinates.mBot) / 2.0f;
    mBoundaries.mLft = 0.0f;
    mBoundaries.mRgt = tWidth;
    mBoundaries.mTop = 0.0f;
    mBoundaries.mBot = tHeight;
    mMaxMovesPerTick = 7;

    //--Positive-camera zones.
    mUsePositiveCamera = false;
    mTotalCamZones = 0;
    free(mCamZoneList);
    mCamZoneList = NULL;
}
void StarCamera2D::ReloadAndReset()
{
    //--Called when the level is reloaded, this resets some flags but preserves most of the camera's
    //  knowledge of its own layout.
    m2DScale = 1.0f;
    mSmallestScale = 1.0f;
    mLargestScale = 10.0f;
    #ifndef DBG_ALLOW_CAMERA_ZOOMING
    mLargestScale = 1.0f;
    #endif
}
void StarCamera2D::PositionInWorld()
{
    //--Sets the scale of the program according to the Camera's internal scaler. Also, since it
    //  is quite likely that the scale will cause things to be offset towards the top-left, attempts
    //  to recenter the visible area.
    //--Does nothing if the scale is 1.0f, or 0.0f (to avoid DIV0 errors).
    //--For the love of Celestia, do NOT change the zoom with this active then reset it, or you will
    //  get very bizarre behavior!
    if(m2DScale == 0.0f || m2DScale == 1.0f) return;

    //--Scale
    GLOBAL *rGlobal = Global::Shared();
    glTranslatef(rGlobal->gScreenWidthPixels /  2.0f, rGlobal->gScreenHeightPixels /  2.0f, 0.0f);
    glScalef(1.0f / m2DScale, 1.0f / m2DScale, 1.0f);
    glTranslatef(rGlobal->gScreenWidthPixels / -2.0f, rGlobal->gScreenHeightPixels / -2.0f, 0.0f);
}
void StarCamera2D::UnpositionInWorld()
{
    //--Unsets the scaling from before. Once again, does not work if 0.0f or 1.0f was the scale.
    if(m2DScale == 0.0f || m2DScale == 1.0f) return;

    //--Unset the scale.
    GLOBAL *rGlobal = Global::Shared();
    glTranslatef(rGlobal->gScreenWidthPixels /  2.0f, rGlobal->gScreenHeightPixels /  2.0f, 0.0f);
    glScalef(m2DScale, m2DScale, 1.0f);
    glTranslatef(rGlobal->gScreenWidthPixels / -2.0f, rGlobal->gScreenHeightPixels / -2.0f, 0.0f);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void StarCamera2D::Update()
{
    //--Under current circumstances, camera is controlled externally.
    return;

    //--Update the zooming factor.  First pass, always set to zero change.
    int tDummyX, tDummyY, tMouseZ;
    static int xMouseZ = -1000;
    ControlManager::Fetch()->GetMouseCoords(tDummyX, tDummyY, tMouseZ);
    if(xMouseZ == -1000)
    {
        xMouseZ = tMouseZ;
    }

    //--Calc how far the Z changed, zoom accordingly. Each singular zoom value is an order of
    //  magnitude, not a linear value.
    float tChangeZoom = (float)(tMouseZ - xMouseZ) * -1.00f;
    xMouseZ = tMouseZ;

    //--Clamp the zooming factor.
    m2DScale = m2DScale + tChangeZoom;
    if(m2DScale <= mSmallestScale) m2DScale = mSmallestScale;
    if(m2DScale >= mLargestScale) m2DScale = mLargestScale;

    //--[Mouse Controls]
    //--If allowed, the mouse will override normal camera behavior.
    if(mAllowMouseControl) { CenterOnMouse(); return; }

    //--[Locking]
    //--If the camera is locked, it will not move on its own.
    if(mLockCameraPipeline) return;

    //--Standard handling. Attempts to center on the player from wherever the camera currently is.
    CenterOnPlayer();
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void StarCamera2D::Render2D()
{
    //--Renders the zones currently active, if positive camera mode is active and the program is in
    //  debug mode.
    if(!OptionsManager::Fetch()->GetOptionB("ShowHitboxes")) return;

    //--GL Setup.
    DisplayManager::StdModelPush();
    glDisable(GL_TEXTURE_2D);

    //--Render the zone where the Camera's edges are. This will only be visible if the camera is
    //  presently zoomed out, otherwise it will just edge the screen.
    glLineWidth(3.0f);
    glColor3f(1.0f, 0.5f, 0.1f);
    float tWid = mCoordinates.mRgt - mCoordinates.mLft;
    float tHei = mCoordinates.mBot - mCoordinates.mTop;
    glBegin(GL_LINE_LOOP);
        glVertex2f(0.0f,  0.0f);
        glVertex2f(tWid,  0.0f);
        glVertex2f(tWid, tHei);
        glVertex2f(0.0f, tHei);
    glEnd();

    //--Render each of the Camera's activity zones as yellow rectangles.
    glLineWidth(1.0f);
    glColor3f(1.0f, 1.0f, 0.0f);
    for(uint32_t i = 0; i < mTotalCamZones; i ++)
    {
        if(mCamZoneList[i].mIsActive)
        {
            float tWidth  = mCamZoneList[i].mRgt - mCamZoneList[i].mLft;
            float tHeight = mCamZoneList[i].mBot - mCamZoneList[i].mTop;
            glTranslatef( mCamZoneList[i].mLft,  mCamZoneList[i].mTop, 0.0f);

                glBegin(GL_LINE_LOOP);
                    glVertex2f(  0.0f,    0.0f);
                    glVertex2f(tWidth,    0.0f);
                    glVertex2f(tWidth, tHeight);
                    glVertex2f(  0.0f, tHeight);
                glEnd();

            glTranslatef(-mCamZoneList[i].mLft, -mCamZoneList[i].mTop, 0.0f);
        }
    }

    //--Polygon zones are yellow-ish rectangles as well.
    glTranslatef(-mCoordinates.mLft, -mCoordinates.mTop, 0.0f);
    glColor3f(1.0f, 0.75f, 0.0f);
    float *rArray = (float *)mPolygonList->PushIterator();
    while(rArray)
    {
        //--Render
        glBegin(GL_LINE_LOOP);
            for(int i = 0.0f; i < (int)rArray[0]; i++)
            {
                glVertex2f(rArray[(i*2)+1], rArray[(i*2)+2]);
            }
        glEnd();

        //--Iterate up
        rArray = (float *)mPolygonList->AutoIterate();
    }
    glTranslatef(mCoordinates.mLft, mCoordinates.mTop, 0.0f);

    //--Clean.
    DisplayManager::StdModelPop();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
