///======================================== AdvUIForms ============================================
//--UI Containing selectable grids that allows the player to change forms/classes.

#pragma once

///========================================= Includes =============================================
#include "AdvUICommon.h"
#include "AdvUIGrid.h"
#include "AdvCombatDefStruct.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#ifndef ADVUI_FORMS_DEFINITIONS
#define ADVUI_FORMS_DEFINITIONS

///--[Character Grid Package]
typedef struct AdvMenuFormCharacterPack
{
    //--Members
    char *mDisplayName;
    AdvMenuStandardAlignments mAlignments;

    //--Functions
    void Initialize()
    {
        mDisplayName = NULL;
        mAlignments.Initialize();
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuFormCharacterPack *rPtr = (AdvMenuFormCharacterPack *)pPtr;
        free(rPtr->mDisplayName);
        free(rPtr);
    }
}AdvMenuFormCharacterPack;

///--[Form / Class Grid Package]
typedef struct AdvMenuFormClassPack
{
    //--Members
    char *mDisplayName;
    char *mScriptPath;
    char *mJobPath;
    CombatStatistics mCombatStatistics;
    AdvMenuStandardAlignments mAlignments;

    //--Functions
    void Initialize()
    {
        mDisplayName = NULL;
        mScriptPath = NULL;
        mJobPath = NULL;
        mCombatStatistics.Initialize();
        mAlignments.Initialize();
    }
    static void DeleteThis(void *pPtr)
    {
        if(!pPtr) return;
        AdvMenuFormClassPack *rPtr = (AdvMenuFormClassPack *)pPtr;
        free(rPtr->mDisplayName);
        free(rPtr->mScriptPath);
        free(rPtr->mJobPath);
        free(rPtr);
    }
}AdvMenuFormClassPack;

#endif

///========================================== Classes =============================================
class AdvUIForms : virtual public AdvUICommon, virtual public AdvUIGrid
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxAdvFormsHelpStrings = 4;

    ///--[Variables]
    //--System
    bool mIsInFormSelect;
    int mFormSelectTimer;

    //--Cursor
    bool mDontShowHighlight;
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Data
    StarLinkedList *mCharactersData; //AdvMenuFormCharacterPack *, master
    StarLinkedList *mFormClassData;  //AdvMenuFormClassPack *, master

    //--Comparison.
    CombatStatistics mCharacterBaseStats;

    //--Strings
    StarlightString *mShowHelpString;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Mainline;

        //--Common
        StarBitmap *rOverlay_Footer;
        StarBitmap *rOverlay_Header;
        StarBitmap *rOverlay_Highlight;
    }AdvImages;

    protected:

    public:
    //--System
    AdvUIForms();
    virtual ~AdvUIForms();
    virtual void Construct();

    //--Public Variables
    static char *xFormResolveScript;
    static char *xFormPartyResolveScript;

    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();
    void RegisterFormPartyMember(const char *pCharacterName, const char *pDisplayName, const char *pImgPath);
    void SetFormPartyMemberAlignments(const char *pCharacterName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei);
    void RegisterFormTransformation(const char *pFormName, const char *pDisplayName, const char *pFormPath, const char *pJobScriptPath, const char *pImgPath);
    void SetFormPartyTransformationAlignments(const char *pFormName, float pXOff, float pYOff, float pLft, float pTop, float pWid, float pHei);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void ActivateFormsMode();
    void DeactivateFormsMode();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


