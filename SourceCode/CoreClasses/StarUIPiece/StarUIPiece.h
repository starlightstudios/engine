///======================================== StarUIPiece ============================================
//--A standardized UI piece representing a "screen" of the UI, such as the status screen or the equipment
//  screen. The basic UI piece doesn't do much, derived types handle the standardized operations.
//--UI objects are quite similar to game entities in that they typically have an update and render cycle,
//  but UI objects nominally always update in the background, allowing them to dynamically scroll
//  and fade while other UI objects take focus. This is handled by a caller UI, which is usually something
//  like the AdventureMenu or the CarnationMenu.

//--Note on pVisAlpha and pColorAlpha
//--When passing alpha values, all daughter objects and calls multiply their visiblity alpha by the
//  passed in alpha. This allows derived objects to fade in/out if their parent object is also fading.
//  However, if the option to use fades is off, objects translate in from the sides of the screen.
//  The pVisAlpha value reflects both fade percentage and translation percentage, whereas pColorAlpha
//  only represents the fade part. Typically, primary calls like RenderBot() use pVisAlpha whereas
//  a function call that does not translate, but does change the color mixer, uses pColorAlpha.

#pragma once

///========================================= Includes =============================================
#include "RootObject.h"

///===================================== Local Structures =========================================
///--[VerifyPack]
//--Stores a pointer and size of the object at the pointer. Passed to verify image functions to make
//  sure all of the objects inside the pointer resolved.
typedef struct VerifyPack
{
    void *rPtr;
    size_t mSize;
}VerifyPack;

///--[TranslateStackPack]
//--When multiple uses of AutoPieceOpen() occur, this places the translation values in a stack to
//  allow interoperability.
typedef struct TranslateStackPack
{
    float mXOffset;
    float mYOffset;
}TranslateStackPack;

///===================================== Local Definitions ========================================
#define STARUI_CLOSE_CODE_SIZE 128      //Maximum number of characters in the close code.
#define STARUI_MOUSE_NOTHING -1         //Indicates there is no currently selected object when using a mouse.

///========================================== Classes =============================================
class StarUIPiece : public RootObject
{
    protected:
    ///--[Constants]
    static const int cHlpTicks = 15;    //Number of ticks to show/hide the help screen.

    ///--[System]
    bool mIsForegroundObject;           //Used to make the object consider itself the foreground regardless of what the caller thinks.
    bool mFlagExit;                     //If true, this object was flagged to exit (player pressed cancel). Caller should handle that.
    char mSubtype[5];                   //4-digit string that indicates the type of the object for error messages.
    int mCodeBackward;                  //Integer set by the caller, returned when this object flags for exit. Caller uses it to decide what to do
                                        //when the user cancels out of a UI (such as close the UI, change modes, etc).
    char mCloseCode[STARUI_CLOSE_CODE_SIZE];//String that is passed to the Lua close handler script by the caller if this UI piece requires it.

    ///--[Diagnostics]
    StarLinkedList *mVerifyList;        //VerifyPack * listing. Created at construction, stores a list of all Image structure pointers and their sizes.
                                        //This allows automatic error printing and pointer verification.
    StarLinkedList *mDiagnostics;       //During a ResolveImages_X() call, errors are not printed. Instead, entries are added to the diagnostics list.
                                        //The list is all (char *) pointers. They should print if ImageDiagnostics() is called.

    ///--[Visiblity]
    int mVisibilityTimer;               //Counter, runs from 0 to mVisibilityTimerMax when the object shows/hides.
    int mVisibilityTimerMax;            //Max value of mVisibilityTimer for full opacity.

    ///--[Rendering]
    float mLastOffsetX;                 //When using AutoPieceOpen()/AutoPieceClose(), stores the X offset.
    float mLastOffsetY;                 //When using AutoPieceOpen()/AutoPieceClose(), stores the Y offset.
    StarLinkedList *mTranslateStack;    //TranslateStackPack *, stack of mLastOffsetX/Y values.

    ///--[Help Menu]
    bool mIsShowingHelp;                //If true, show the help menu over the UI.
    int mHelpVisibilityTimer;           //Timer for visiblity of help menu.
    int mHelpVisibilityTimerMax;        //Max value for full display of mHelpVisibilityTimer.
    StarlightColor mHelpHeaderCol;      //Text color for the header.
    float mHelpControlOffset;           //Y offset for control images so they bottom-align on the help menu. Depends on the font.
    int mHelpStringsMax;                //Size of the below list.
    StarlightString **mHelpMenuStrings; //List of help strings that can appear with control images.

    //--Help Related Images
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;  //Font, used for the heading.
        StarFont *rFont_Mainline;       //Font, used for the normal help text.

        //--Images
        StarBitmap *rFrame_Main;        //Image, backing for the help screen.
    }HelpImages;

    ///--[Images]
    bool mUseFade;                      //If true, the UI fades in or out. If false, it slides in or out from the edge of the screen.
    bool mImagesReady;                  //If true, rendering can take place. If false, images failed to resolve.
                                        //By default, the StarUIPiece has no images and derived classes must resolve them.

    protected:

    public:
    //--System
    StarUIPiece();
    virtual ~StarUIPiece();
    virtual void Construct();
    virtual bool VerifyImages();
    virtual void ImageDiagnostics();
    virtual void RunTranslation();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    bool IsFlaggingExit();
    const char *GetSubtype();
    int GetCodeBackward();
    const char *GetCloseBuffer();
    int GetVisibilityTimer();
    int GetVisibilityTimerMax();
    bool DidImagesResolve();

    //--Manipulators
    virtual void TakeForeground();
    virtual void TakeBackground();
    virtual void FlagExit();
    void UnflagExit();
    void SetCodeBackward(int pValue);
    void SetCloseCode(const char *pString);
    void AppendDiagnostic(const char *pString, ...);
    void AppendVerifyPack(const char *pName, void *pPtr, size_t pStructureSize);

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    void ResolveSeriesInto(const char *pSeriesName, void *pPtr, size_t pSize);
    void HandlePositioning(float pXOffset, float pYOffset, float pPercent);
    virtual void SetColor(const char *pColorName, float pAlpha);
    bool CommonHelpUpdate();

    ///--[Automation]
    float AutoPieceOpen(int pDirection, float pVisAlpha);
    void AutoPieceClose();
    bool AutoListControl(const char *pDecrement, const char *pIncrement, const char *pAccelerate, int &sCursor, int &sSkip, int pListSize, int pEdge, int pSizePerPage, bool pAllowWrap);
    bool AutoListUpDn(int &sCursor, int &sSkip, int pListSize, int pEdge, int pSizePerPage, bool pAllowWrap);
    bool AutoListLfRt(int &sCursor, int &sSkip, int pListSize, int pEdge, int pSizePerPage, bool pAllowWrap);
    bool AutoListControlNoScroll(const char *pDecrement, const char *pIncrement, const char *pAccelerate, int &sCursor, int pListSize, bool pAllowWrap);
    bool AutoListUpDnNoScroll(int &sCursor, int pListSize, bool pAllowWrap);
    bool AutoListLfRtNoScroll(int &sCursor, int pListSize, bool pAllowWrap);
    bool AutoHelpOpen(const char *pOpenA, const char *pOpenB, const char *pOpenC);
    void AutoHelpClose(const char *pCloseA, const char *pCloseB, const char *pCloseC);

    ///--[Standard Handlers]
    static void StandardTimer(int &sTimer, int pRangeLo, int pRangeHi, bool pCondition);
    static void StandardLoopTimer(int &sTimer, int pRangeLo, int pRangeHi);
    static void StandardCycleTimer(int &sTimer, int &sPauseTimer, int pRangeLo, int pRangeHi, bool &sCondition, int pPauseTicks);
    static bool StandardCursor(int &sCursor, int &sSkip, int pMoves, int pListCap, int pScrollBuffer, int pItemsPerPage, bool pWrap);
    static bool StandardCursorNoScroll(int &sCursor, int pMoves, int pListCap, bool pWrap);
    static void StandardRenderScrollbar(int pSkip, int pItemsPerPage, int pMaxOffset, float pScrollTop, float pScrollHei, bool pStaticIsFront, StarBitmap *pScroller, StarBitmap *pStatic);
    void StandardPieceOpen(int pDirection, float pVisAlpha, float &sXOffset, float &sYOffset, float &sColorAlpha);

    protected:
    //--Private Core Methods
    void AllocateHelpStrings(int pCount);
    void SetHelpString(int &sSlot, const char *pString);
    void SetHelpString(int &sSlot, const char *pString, int pImages, ...);

    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderForeground(float pVisAlpha);
    virtual void RenderBackground(float pVisAlpha);
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);
    virtual void RenderHelp(float pVisAlpha);
    void StandardHelp(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


