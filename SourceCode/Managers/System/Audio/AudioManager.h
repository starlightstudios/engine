///======================================= AudioManager ===========================================
//--Manages audio packages, both music and sound, as well as global controls like volume. If you
//  can hear it, this is your manager!
//--Note: The blocking flags for Music and Sound can be flipped on and off. The AM still registers
//  the samples/streams. The xBlockAllAudio flag should only be flipped by the command line.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

#if defined _FMOD_AUDIO_
    #include "fmod.hpp"
#endif

///===================================== Local Definitions ========================================
#define AM_DEFAULT_MUSIC_VOLUME 0.50f
#define AM_DEFAULT_SOUND_VOLUME 0.650001f

//--Abstract Volumes. This definition is duplicated in the OptionsManager!
#ifndef AM_DEFAULT_ABSTRACT_VOLUME
#define AM_DEFAULT_ABSTRACT_VOLUME 0.50f
#define AM_ABSTRACT_VOLUME_TOTAL 30
#endif

///===================================== Local Structures =========================================
typedef struct
{
    uint64_t mStartInBytes;
}AudioPackageLoopData;

//--Delayed sound. Allows playing SFX after a time has passed.
typedef struct
{
    int mTicks;
    char mSoundName[80];
}AudioDelayPack;

///--[PlaySoundPackage]
//--Used for enqueing sounds so they don't all play at once in some cases, stores the sound name
//  and optional flags needed for special effects.
typedef struct PlaySoundPackage
{
    //--Name of Sound
    char mSoundName[STD_MAX_LETTERS];
    char mEffectName[STD_MAX_LETTERS];

    //--Functions
    void Initialize()
    {
        mSoundName[0] = '\0';
        strcpy(mEffectName, "Null");
    }
    static void DeleteThis(void *pPtr)
    {
        //--Error check:
        if(!pPtr) return;

        //--Cast.
        PlaySoundPackage *rPtr = (PlaySoundPackage *)pPtr;

        //--Deallocate.
        free(rPtr);
    }
}PlaySoundPackage;

///--[BassFXUnion]
//--Contains copies of all the different Bass_FX effect structures, in a single union to allow
//  flexibility.
#if defined _BASS_AUDIO_
typedef union BassFXUnion
{
    BASS_BFX_ROTATE      mRotate;
    BASS_BFX_VOLUME      mVolume;
    BASS_BFX_PEAKEQ      mPeakEq;
    BASS_BFX_MIX         mMixer;
    BASS_BFX_DAMP        mDampener;
    BASS_BFX_AUTOWAH     mAutoWah;
    BASS_BFX_PHASER      mPhaser;
    BASS_BFX_CHORUS      mChorus;
    BASS_BFX_DISTORTION  mDistortion;
    BASS_BFX_COMPRESSOR2 mCompressor;
    BASS_BFX_ECHO4       mEcho;
    BASS_BFX_PITCHSHIFT  mPitchShift;
    BASS_BFX_FREEVERB    mReverb;
}BassFXUnion;
#endif
#if defined _FMOD_AUDIO_
typedef union BassFXUnion
{
    uint32_t mDummy;
}BassFXUnion;
#endif

///--[EffectProfilePackage]
//--Contains an effect, which is one of the Bass FX types, as well as the structure required to
//  set that effect. Many effects can have different variables that change how they sound, and
//  this structure encapsulates that.
typedef struct EffectProfilePackage
{
    uint32_t mType;
    BassFXUnion mData;
}EffectProfilePackage;

///========================================== Classes =============================================
class AudioManager
{
    private:
    //--System
    //--Storage Lists
    StarLinkedList *mMusicList;
    StarLinkedList *mSoundList;
    StarLinkedList *mRandomSoundList;

    //--Music controllers
    AudioPackage *rCurrentMusic;

    //--Speech controllers
    AudioPackage *mCurrentSpeech;

    //--Delayed Sounds
    StarLinkedList *mDelayedSoundList; //AudioDelayPack *, master

    //--Effect Profiles
    StarLinkedList *mEffectProfiles; //EffectProfilePackage *, master

    protected:

    public:
    //--System
    AudioManager();
    ~AudioManager();

    //--Public Variables
    AudioPackage *rLastPackage;
    static bool xBlockAllAudio;
    static bool xBlockSound;
    static bool xBlockMusic;
    static float xMusicVolume;
    static float xSoundVolume;
    static float xAbstractVolume[AM_ABSTRACT_VOLUME_TOTAL];
    static float xMasterVolume;

    //--FMOD. AudioPackages get these a lot.
    #if defined _FMOD_AUDIO_
        static FMOD_SYSTEM *xFMODSystem;
        //FMOD_SOUND *mSound, *mSoundToPlay;
        //FMOD_CHANNEL *rChannel;
        //FMOD_RESULT mFMODResult;
        static uint32_t xFMODVersion;
        static void *xExtraDriverData;
        //int mNumSubSounds;
    #endif

    //--Property Queries
    bool IsMusicPlaying();
    bool MusicExists(const char *pName);
    bool SoundExists(const char *pName);
    const char *GetNameOfMusicPack(void *pPtr);
    const char *GetNameOfPlayingMusic();
    float GetLengthOfSound(const char *pName);

    //--Manipulators
    void RegisterMusic(const char *pName, AudioPackage *pMusicPack);
    void RegisterSound(const char *pName, AudioPackage *pSoundPack);
    void RegisterRandomSound(const char *pName, int pMaxSounds);
    void UnregisterMusic(const char *pName);
    void UnregisterSound(const char *pName);
    void ClearLastPackage();
    void ChangeMusicVolume(float pDelta);
    void ChangeSoundVolume(float pDelta);
    void ChangeAbstractVolume(int pSlot, float pDelta);
    void ChangeMusicVolumeTo(float pValue);
    void ChangeSoundVolumeTo(float pValue);
    void ChangeAbstractVolumeTo(int pSlot, float pValue);
    void SeekMusicTo(float pSeconds);
    void AdvanceMusicToLoopPoint();
    void ModifyLoopPointForward();
    void ModifyLoopPointBackward();

    //--Core Methods
    void RegisterAdvanced(const char *pName, const char *pPath, const char *pProperties);
    void PlayMusic(const char *pName);
    void PlayMusicStartingAt(const char *pName, float pStartPoint);
    void PlayMusicNoFade(const char *pName);
    void FadeMusic(int pTickDuration);
    void StopAllMusic();
    void PlaySound(const char *pName);
    void PlaySoundByPackage(PlaySoundPackage *pPackage);
    void StopSound(const char *pName);
    void PlayRandomSound(const char *pName);
    void PlayRandomSound(const char *pName, int pMaxRoll);
    void StopAllSound();
    void PlaySpeech(const char *pPath);
    bool StopSpeech();
    void PlaySoundDelay(const char *pName, int pTicks);
    void PauseSound(const char *pName);

    //--Effect Profiles
    uint32_t GetBassEnumeration(const char *pEnumerationName);
    EffectProfilePackage *GetEffectProfilePackage(const char *pName);
    void RegisterEffectProfile(const char *pName, uint32_t pType);
    void SetEffectProfileProperty(const char *pName, uint32_t pType, const char *pPropertyString, float pValue);
    void ApplyEffectProfileToSound(const char *pSoundName, const char *pProfileName);
    void ClearEffectProfiles();

    //--Special Audio functions
    #if defined _BASS_AUDIO_
    static void CALLBACK LoopCallback(HSYNC pHandle, DWORD pChannel, DWORD pData, void *pUserPtr);
    #endif

    //--Update
    void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    AudioPackage *GetPlayingMusic();
    AudioPackage *GetMusicPack(const char *pName);
    AudioPackage *GetSoundPack(const char *pName);
    AudioPackage *GetVoicePack();

    //--Static Functions
    static AudioManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AudioManager_Register(lua_State *L);
int Hook_AudioManager_RegisterAdv(lua_State *L);
int Hook_AudioManager_PlayMusic(lua_State *L);
int Hook_AudioManager_PlaySound(lua_State *L);
int Hook_AudioManager_PlaySoundDelay(lua_State *L);
int Hook_AudioManager_PlaySpeech(lua_State *L);
int Hook_AudioManager_StopSound(lua_State *L);
int Hook_AudioManager_StopMusic(lua_State *L);
int Hook_AudioManager_FadeMusic(lua_State *L);
int Hook_AudioManager_GetProperty(lua_State *L);
int Hook_AudioManager_SetProperty(lua_State *L);
