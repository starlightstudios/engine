//--Base
//--Classes
#include "PairanormalDialogue.h"
#include "WorldDialogue.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "CutsceneManager.h"

///===================================== Cutscene Manager =========================================
bool CutsceneManager::IsAnythingStoppingEvents()
{
    ///--[Documentation]
    //--Used when the CutsceneManager is checking if anything is stopping events from updating. This includes
    //  things like dialogue, animations, and whatever other things this local project has. If anything
    //  is stopping events, returns true. Otherwise, false.

    ///--[Dialogue Check]
    //--Dialogue handler, often used by the AdventureLevel during cutscenes.
    WorldDialogue *rDialogue = WorldDialogue::Fetch();
    if(rDialogue->IsStoppingEvents()) return true;

    ///--[Pairanormal Check]
    //--If the Pairanormal dialogue handler is stopping events, the same thing happens.
    PairanormalDialogue *rPairanormalDialogue = PairanormalDialogue::Fetch();
    if(rPairanormalDialogue->IsStoppingEvents()) return true;

    ///--[All Checks Passed]
    return false;
}
