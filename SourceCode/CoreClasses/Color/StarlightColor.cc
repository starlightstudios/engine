//--Base
#include "StarlightColor.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///========================================== System ==============================================
///--[Public Statics]
//--Stock colors.
StarlightColor StarlightColor::cxWhite = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
StarlightColor StarlightColor::cxGrey   = StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, 1.0f);
StarlightColor StarlightColor::cxBlack  = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f);

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void StarlightColor::SetDefault()
{
    //--Sets to stock-white.
    r = 1.0f;
    g = 1.0f;
    b = 1.0f;
    a = 1.0f;
}

///--[Setters]
//--These are here to skip the middle-man static functions. This makes them slightly faster.
void StarlightColor::SetRGBI(int pRed, int pGrn, int pBlu)
{
    r = (float)pRed / 255.0f;
    g = (float)pGrn / 255.0f;
    b = (float)pBlu / 255.0f;
    a = 1.0f;
}
void StarlightColor::SetRGBAI(int pRed, int pGrn, int pBlu, int pAlp)
{
    r = (float)pRed / 255.0f;
    g = (float)pGrn / 255.0f;
    b = (float)pBlu / 255.0f;
    a = (float)pAlp / 255.0f;
}
void StarlightColor::SetRGBF(float pRed, float pGrn, float pBlu)
{
    r = pRed;
    g = pGrn;
    b = pBlu;
    a = 1.0f;
}
void StarlightColor::SetRGBAF(float pRed, float pGrn, float pBlu, float pAlp)
{
    r = pRed;
    g = pGrn;
    b = pBlu;
    a = pAlp;
}
void StarlightColor::CloneFrom(const StarlightColor pColor)
{
    memcpy(this, &pColor, sizeof(StarlightColor));
}

///======================================== Core Methods ==========================================
void StarlightColor::SetAsMixer()
{
    //--Sets this color as the opengl color mixer. Optimizer should inline this.
    glColor4f(r, g, b, a);
}
void StarlightColor::SetAsMixerAlpha(float pAlpha)
{
    //--Sets this color as the opengl color mixer, overriding the alpha.
    glColor4f(r, g, b, pAlpha);
}
StarlightColor StarlightColor::Clone()
{
    StarlightColor nColor;
    memcpy(&nColor, this, sizeof(StarlightColor));
    return nColor;
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
void StarlightColor::ConditionalMixer(bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor)
{
    if(pConditional)
    {
        pTrueColor.SetAsMixer();
    }
    else
    {
        pFalseColor.SetAsMixer();
    }
}
void StarlightColor::ClearMixer()
{
    //--Resets the color mixer to white.
    glColor3f(1.0f, 1.0f, 1.0f);
}
void StarlightColor::SetMixer(float pRed, float pGrn, float pBlu, float pAlp)
{
    glColor4f(pRed, pGrn, pBlu, pAlp);
}
StarlightColor StarlightColor::MapRGBI(uint8_t pRed, uint8_t pGrn, uint8_t pBlu)
{
    return MapRGBAI(pRed, pGrn, pBlu, 0xFF);
}
StarlightColor StarlightColor::MapRGBAI(uint8_t pRed, uint8_t pGrn, uint8_t pBlu, uint8_t pAlp)
{
    return MapRGBAF((float)pRed / 255.0f, (float)pGrn / 255.0f, (float)pBlu / 255.0f, (float)pAlp / 255.0f);
}
StarlightColor StarlightColor::MapRGBF(float pRed, float pGrn, float pBlu)
{
    return MapRGBAF(pRed, pGrn, pBlu, 1.0f);
}
StarlightColor StarlightColor::MapRGBAF(float pRed, float pGrn, float pBlu, float pAlp)
{
    StarlightColor nColor;
    nColor.r = pRed;
    nColor.g = pGrn;
    nColor.b = pBlu;
    nColor.a = pAlp;
    return nColor;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
