//--Base
#include "Chapter1LoadInterrupt.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///========================================== System ==============================================
Chapter1LoadInterrupt::Chapter1LoadInterrupt()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======== StarLoadInterrupt ========= ]
    ///--[ ======= Chapter1LoadInterrupt ====== ]
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    mIsReady = false;
    mBacking = NULL;
    mMeiFrames = NULL;
    for(int i = 0; i < LI_CH1_LOADFRAMES; i ++) mLoadingFrames[i] = NULL;
}
Chapter1LoadInterrupt::~Chapter1LoadInterrupt()
{
    delete mBacking;
    delete mMeiFrames;
    for(int i = 0; i < LI_CH1_LOADFRAMES; i ++) delete mLoadingFrames[i];
}
void Chapter1LoadInterrupt::Construct()
{
    ///--[Documentation]
    //--Loads and verifies images associated with this object. Because this object is meant to appear during
    //  the loading of other objects, it handles its images internally, not using a conventional DataLibrary.

    ///--[Load Frames]
    //--Loading animation is a filmstrip series placed in a linked list.
    StarLumpManager *tSLM = new StarLumpManager();
    tSLM->Open("Data/Loading.slf");
    if(!tSLM->IsFileOpen()) return;

    //--Suppress the load interrupt.
    bool tOldFlag = StarBitmap::xSuppressInterrupt;
    StarBitmap::xSuppressInterrupt = true;

    //--Load.
    char tBuf[128];
    mMeiFrames = tSLM->GetImage("Chapter1|MeiBee");
    mBacking = tSLM->GetImage("Chapter1|Backing");
    for(int i = 0; i < LI_CH1_LOADFRAMES; i ++)
    {
        sprintf(tBuf, "Chapter1|Loading%02i", i);
        mLoadingFrames[i] = tSLM->GetImage(tBuf);
    }

    //--Verify.
    mIsReady = (mBacking && mMeiFrames);
    for(int i = 0; i < LI_CH1_LOADFRAMES; i ++)
    {
        if(!mLoadingFrames[i]) mIsReady = false;
    }

    ///--[Finish Up]
    //--Set to the String Tyrant variant. If anything went wrong, the default one will be used.
    StarBitmap::xInterruptCall = &StarLoadInterrupt::Interrupt;

    //--Set this flag.
    mIsAwaitingKeypress = false;

    //--Clean up.
    delete tSLM;
    StarBitmap::xSuppressInterrupt = tOldFlag;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void Chapter1LoadInterrupt::DeallocateImages()
{
    ///--[Documentation]
    //--Deallocates and NULLs all images associated with this object.
    mIsReady = false;
    StarLoadInterrupt::DeallocateImages();

    //--Derived call.
    delete mBacking;
    delete mMeiFrames;
    for(int i = 0; i < LI_CH1_LOADFRAMES; i ++) delete mLoadingFrames[i];

    //--NULL.
    mBacking = NULL;
    mMeiFrames = NULL;
    for(int i = 0; i < LI_CH1_LOADFRAMES; i ++) mLoadingFrames[i] = NULL;
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
void Chapter1LoadInterrupt::Call(bool pForceRender)
{
    ///--[Documentation and Setup]
    //--Chapter 1 Load Interrupt renderer, shows an animation of Bee Mei flying.
    if(!CommonLogicUpdate(pForceRender) || !mIsReady) return;

    //--Call the renderer setup. The screen is now ready for drawing.
    CommonRendererSetup();

    ///--[Animation Constants]
    //--These are the same between all animations.
    float cRenderScale = 2.0f;
    float cWidPerFrame = 170.0f;
    float cHeiPerFrame = 100.0f;

    ///--[Mei's Animation]
    //--Compute the frame.
    int tMeiFrame = (mFrame / 1) % 34;

    //--Sizes.
    float cMeiTrueWid = mMeiFrames->GetTrueWidth();

    //--Constants. Mei is centered.
    float cLft = ((1366.0f - 0.0f) * 0.50f) - (cWidPerFrame * 0.50f * cRenderScale);
    float cTop = 200.0f;
    float cRgt = cLft + (cWidPerFrame * cRenderScale);
    float cBot = cTop + (cHeiPerFrame * cRenderScale);
    float cTxL = (tMeiFrame + 0) * (cWidPerFrame / cMeiTrueWid);
    float cTxT = 1.0f;
    float cTxR = (tMeiFrame + 1) * (cWidPerFrame / cMeiTrueWid);
    float cTxB = 0.0f;

    //--Setup.
    mMeiFrames->Bind();

    //--Render.
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
    glEnd();

    ///--[Loading Text]
    //--Render the backing.
    cLft = ((1366.0f) * 0.50f) - (mBacking->GetTrueWidth() * 0.50f);
    cTop = 410.0f;
    mBacking->Draw(cLft, cTop);

    //--Loading text. Determine which frame to render.
    int tLoadingFrame = (mFrame / 15) % LI_CH1_LOADFRAMES;
    mLoadingFrames[tLoadingFrame]->Draw(cLft, cTop);

    ///--[Finish Up]
    CommonRendererClean();
}

///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
void Chapter1LoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_BootChapter1()
       Switches to String Tyrant mode. Pass "NULL" to switch to normal mode. */
    lua_register(pLuaState, "LI_BootChapter1", &Hook_LI_BootChapter1);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_LI_BootChapter1(lua_State *L)
{
    //LI_BootChapter1()

    //--Create a new LoadInterrupt.
    Chapter1LoadInterrupt *nInterrupt = new Chapter1LoadInterrupt();
    nInterrupt->Construct();

    //--Dealloacte the old one.
    GLOBAL *rGlobal = Global::Shared();
    delete rGlobal->gLoadInterrupt;

    //--Replace.
    rGlobal->gLoadInterrupt = nInterrupt;
    return 0;
}
