//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"
#include "PathrunnerStraight.h"
#include "TileLayer.h"

//--CoreClasses
//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DebugManager.h"

///--[Local Definitions]
#define ADVP_UNUSED 0x7FFF
#define ADVP_WALK_DIST 8

///--[Local Structures]
///--[Debug]
//#define TA_ADVANCED_PATH_DEBUG
#ifdef TA_ADVANCED_PATH_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///====================================== Advanced Pathing ========================================
void TilemapActor::HandleAdvancedPathing()
{
    ///--[Documentation and Setup]
    //--Advanced pathing attempts to path through the level using the same logic of movement that the
    //  player has, including per-pixel movements. This allows the AI to path to any location, including
    //  around complex obstacles. The problem is that it is quite slow.
    //--To speed things up, it uses something like the A* heuristic, whereby the shorter the distance
    //  to the target, the higher in priority the move becomes.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;
    DebugPush(true, "TA: Advanced Pathing. Begin.\n");

    ///--[ ============ Path Exists =========== ]
    //--If we already have built the needed path, use that.
    if(mSmartPatrolInstructions->GetListSize() > 0)
    {
        HandleAdvancedMoving(mSmartPatrolInstructions, mSmartPatrolDistanceLeft);
        DebugPop("Advanced Pathing completes. Handling move.\n");
        return;
    }

    ///--[ ==== Path Needs Building, Setup ==== ]
    //--We need to build a path between where we are now, and the target. This is done via a pulse
    //  method. It requires the level to create a spreading set of movement costs from the origin
    //  point and stop when it reaches the starting point. Then it walks along the lowest movement
    //  cost path and uses that to build the path.
    if(!mPathRunner)
    {
        //--Take the center position, but modify it to be the nearest along a grid of ADVP_WALK_DIST.
        TwoDimensionReal tDimensions = rActiveLevel->GetPatrolNode(mPatrolNodeList[mPatrolNodeCurrent]);
        mSmartPatrolDistanceLeft = ADVP_WALK_DIST;
        int tXTarget = (int)(tDimensions.mXCenter / ADVP_WALK_DIST) * ADVP_WALK_DIST;
        int tYTarget = (int)(tDimensions.mYCenter / ADVP_WALK_DIST) * ADVP_WALK_DIST;

        //--Create the path runner.
        mPathRunner = new PathrunnerStraight();
        mPathRunner->Initialize(mTrueX, mTrueY, tXTarget, tYTarget);
        mPathRunner->SetIterationsPerCycle(50);

        //--Reset flags.
        mPathTicksSoFar = 0;

        //--Debug.
        DebugPrint("Built new pathrunner.\n");
        DebugPrint("Position: %f %f\n", mTrueX, mTrueY);
        DebugPrint("Target: %i %i\n", tXTarget, tYTarget);
    }

    //--If we've exceed our path pulses this tick, stop. If several entities try to run their pathing at the
    //  same time, it can cause lag, so instead this flag stops path checks after a certain number have run.
    //  Entities will just stand still and wait for pathing to free up.
    if(xActorPathPulsesThisTick > TA_MAX_PATH_PULSES)
    {
        DebugPop("Advanced Pathing completes. Exceeded path pulses this tick.\n");
        return;
    }

    //--Run. The result can legally be NULL.
    StarLinkedList *tResultList = mPathRunner->RunPathingIntoList();
    mPathTicksSoFar ++;
    xActorPathPulsesThisTick ++;

    ///--[Error Checking]
    //--If the path runner failed for some reason, stop here. Switch to dumb wander pathing.
    if(mPathRunner->IsFailed())
    {
        mIsAdvancedPatrol = false;
        delete mPathRunner;
        mPathRunner = NULL;
        DebugPop("Advanced Pathing completes. Pathrunner failed.\n");
        return;
    }

    //--If the path runner didn't have a path yet, we can run it again until it does.
    if(!tResultList)
    {
        if(mPathTicksSoFar == 300) mPathRunner->WritePathToDisk("Path.bmp");
        DebugPop("Advanced Pathing completes. Pathrunner hasn't finished yet.\n");
        return;
    }

    //--If the path runner finished sooner than the minimum ticks, and has pulses remaining, let it
    //  keep running those pulses. This can result in better paths.
    if(mPathTicksSoFar < mMinimumPathTicks && mPathRunner->HasPulsesRemaining())
    {
        delete tResultList;
        DebugPop("Advanced Pathing completes. Pathrunner finished before minimum ticks.\n");
        return;
    }

    //--Diagnostics.
    //mPathRunner->WritePathToDisk("Path.bmp");

    ///--[Success, Store]
    //--The path list was passed back successfully. Store it.
    delete mSmartPatrolInstructions;
    mSmartPatrolInstructions = tResultList;

    //--Clean up the path runner.
    delete mPathRunner;
    mPathRunner = NULL;
    DebugPrint("Patrol instruction list size is %i\n", mSmartPatrolInstructions->GetListSize());
    DebugPop("Advanced Pathing completes. Completed path.\n");
}
void TilemapActor::HandleAdvancedMoving(StarLinkedList *pList, float &sDistanceVar)
{
    ///--[Documentation]
    //--If the advanced pather currently has moves available, handles the act of moving. Similar to how the
    //  smart movement works, except it moves in increments of ADVP_WALK_DIST. This allows pathing around
    //  advanced collisions.
    if(!pList) return;

    //--Zeroth entry. If it doesn't exist, assume the move is complete.
    int *rDirection = (int *)pList->GetElementBySlot(0);
    if(!rDirection)
    {
        DecideNextPathDestination();
        return;
    }

    //--Setup.
    float tTargetX = mTrueX;
    float tTargetY = mTrueY;

    //--Resolve the target position.
    if(*rDirection == DIR_NORTH) tTargetY = tTargetY - sDistanceVar;
    if(*rDirection == DIR_SOUTH) tTargetY = tTargetY + sDistanceVar;
    if(*rDirection == DIR_WEST)  tTargetX = tTargetX - sDistanceVar;
    if(*rDirection == DIR_EAST)  tTargetX = tTargetX + sDistanceVar;

    ///--[Movement Finished]
    //--Move to the target. If the move completes:
    int tStoreMove = mMoveTimer;
    if(HandleMoveTo(tTargetX, tTargetY, mMoveSpeed * 0.40f))
    {
        //--Pop the patrol instructions.
        pList->DeleteHead();

        //--Path distance is equal to one tile.
        sDistanceVar = ADVP_WALK_DIST;

        //--Set this as our leashing point.
        SetLeashingPoint(mTrueX, mTrueY);

        //--If there are no instructions left, we reached the next node.
        if(pList->GetListSize() < 1)
        {
            DecideNextPathDestination();
        }
    }
    ///--[Movement Incomplete]
    //--Not at the location yet.
    else
    {
        //--Flags.
        mIsMoving = true;
        if(mMoveTimer != tStoreMove) mMoveTimer = tStoreMove + 1;

        //--Decrement this timer. If it reaches zero, we stop and look around a bit. This does not occur if the
        //  no-stop flag is set.
        mPatrolTicksLeft --;
        if(mPatrolTicksLeft < 1 && !mUseStopOnEachNode && !mNeverPauses && false)
        {
            mPatrolTicksLeft = (rand() % 360) + 90;
            mAIState = TA_AI_WAIT;
            mWaitTimer = 0;
            mWaitTimerMax = rand() % 60 + 60;
        }

        //--Set this as our leashing point.
        SetLeashingPoint(mTrueX, mTrueY);

        //--Get the distance between our position and the target. Store that as the distance remaining.
        sDistanceVar = GetPlanarDistance(mTrueX, mTrueY, tTargetX, tTargetY);
    }
}
