//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEffect.h"
#include "AdvCombatJob.h"
#include "AdventureItem.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

///--[Debug]
//#define ADVCE_RESPONSE_DEBUG
#ifdef ADVCE_RESPONSE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
AdvCombatEntity::AdvCombatEntity()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_ADVCOMBATENTITY;

    ///--[ ==== AdvCombatEntity ===== ]
    //--System
    mHasPositionedInBattle = false;
    mLastCalloutTimer = 0;
    mLocalName = InitializeString("Character");
    mDisplayName = InitializeString("Character");
    mClusterName = InitializeString("No Cluster");
    mResponseScriptPath = NULL;
    mAnimationScriptPath = NULL;
    memset(mScriptResponses, 0, sizeof(mScriptResponses));

    //--Carnation
    mCommandTimer = 0;

    //--Flags
    mCannotBeKnockedOut = false;
    mRemoveFromPartyOnKnockout = false;

    //--Combat Inspector
    mInspectorShowStats = -1;
    mInspectorShowResists = -1;
    mInspectorShowAbilities = 0;

    //--External
    mParagonGrouping = NULL;

    //--Persistent Statistics
    mHasPartingShot = false;
    mIsKnockedOut = false;
    mTurnKOd = 100;
    mHP = 1;
    mMP = 0;
    mCP = 0;
    mShield = 0;
    mAdrenaline = 0;
    mFreeActions = 0;
    mHPEasingPack.Initialize();
    mMPEasingPack.Initialize();
    mAdrenalineEasingPack.Initialize();
    mShieldEasingPack.Initialize();

    //--Memory Cursor
    mMemoryCardX = 0;
    mMemoryJobX = 0;
    mMemoryJobY = 0;
    mMemoryMemorizedX = 0;
    mMemoryMemorizedY = 0;
    mMemoryTacticsX = 0;
    mMemoryTacticsY = 0;

    //--Stun Handling
    mIsStunnable = true;
    mStunValue = 0;
    mStunResist = 0;
    mStunResistDecrementTurns = 0;
    mStunEasingPack.Initialize();

    //--AI
    mIsAmbushed = false;
    mAIScriptPath = NULL;

    //--Combat Position
    mHasBeenAssignedPosition = false;
    mIgnoreMovementForBlocking = false;
    mCombatIdealX = 0.0f;
    mCombatIdealY = 0.0f;
    mCombatPosition.Initialize();
    mCombatPosition.mXCur = -1000.0f;

    //--Shaking
    mShakeTimer = 0;
    mShakeX = 0.0f;

    //--Turn Order
    mTurnID = 0;
    mCurrentInitiative = 0;

    //--Statistics
    mBaseStatistics.Initialize();
    mJobStatistics.Initialize();
    mEquipmentStatistics.Initialize();
    mScriptStatistics.Initialize();
    mPermaEffectStatistics.Initialize();
    mTemporaryEffectStatistics.Initialize();
    mSummedStatistics.Initialize();

    //--Levels
    mLevel = 0;
    mTotalXP = 0;
    mLevelForMugAutoWin = 0;

    //--Jobs
    mGlobalJP = 0;
    rActiveJobAtCombatStart = NULL;
    rActiveJob = NULL;
    mJobList = new StarLinkedList(true);
    mrJobShowList = new StarLinkedList(false);

    //--Rewards
    mRewardXP = 0;
    mRewardJP = 0;
    mRewardPlatina = 0;
    mRewardDoctor = 0;
    mRewardItems = new StarLinkedList(true);

    //--Reinforcement Handling
    mReinforcementTurns = 0;

    //--Ability Listing
    mAbilityList = new StarLinkedList(true);
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            mAbilityProbabilityGrid[x][y] = 0;
            rAbilityGrid[x][y] = NULL;
            rSecondaryGrid[x][y] = NULL;
        }
    }
    mrPassiveAbilityList = new StarLinkedList(false);

    //--Ability Profiles
    rActiveProfile = NULL;
    mProfilesList = new StarLinkedList(true);
    mProfilesList->SetCaseSensitivity(false);

    //--Equipment Listing
    mSlotForWeaponDamage = 0;
    mSlotOfOriginalWeapon = 0;
    mEquipmentSlotList = new StarLinkedList(true);

    //--UI Positions
    memset(&mUIPositions, 0, sizeof(TwoDimensionRealPoint) * ACE_UI_INDEX_TOTAL);
    mFaceTableDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);

    //--Effect Handling
    mrEffectsToShowThisTick = new StarLinkedList(false);

    //--Tags
    mTagList = new StarLinkedList(true);

    //--TF Indicators. Used in AbyssCombat.
    mTFIndicatorsTotal = 0;
    rTFIndicatorImg = NULL;

    //--Images
    mIsFlashingWhite = false;
    mWhiteFlashTimer = 0;
    mIsFlashingBlack = false;
    mBlackFlashTimer = 0;
    mIsKnockingOut = false;
    mKnockoutTimer = 0;
    mBlockRenderForKO = false;
    mOverrideY = -1.0f;
    mOverrideCombatY = -1.0f;
    mFadeInTimer = 1;
    mFadeInTimerMax = 1;
    rCombatImage = NULL;
    rCombatCountermask = NULL;
    rVictoryCountermask = NULL;
    rTurnIcon = NULL;
    memset(rVendorImages, 0, sizeof(StarBitmap *) * ADVCE_VENDOR_SPRITES_TOTAL);
    rFaceTableImg = NULL;

    //--Image Crossfade
    mCrossfadeTimer = ADVCE_CROSSFADE_TICKS;
    rCrossfadeImg = NULL;

    //--Public Variables
    mLastRenderX = ADVCE_NORENDER;
    mLastRenderY = ADVCE_NORENDER;
    mLeveledUpThisBattle = false;
    mLevelLastTick = 0;

    ///--[ ================ Construction ================ ]
    //--By default, we always have at least one active profile. When loading the game,
    //  this may be overwritten and/or cleared.
    AbilityProfile *nDefaultProfile = (AbilityProfile *)starmemoryalloc(sizeof(AbilityProfile));
    nDefaultProfile->Initialize();
    mProfilesList->AddElementAsHead("Default", nDefaultProfile, &AbilityProfile::DeleteThis);
    rActiveProfile = nDefaultProfile;
}
AdvCombatEntity::~AdvCombatEntity()
{
    free(mLocalName);
    free(mDisplayName);
    free(mClusterName);
    free(mResponseScriptPath);
    free(mAnimationScriptPath);
    free(mParagonGrouping);
    free(mAIScriptPath);
    delete mJobList;
    delete mrJobShowList;
    delete mRewardItems;
    delete mAbilityList;
    delete mrPassiveAbilityList;
    delete mProfilesList;
    delete mEquipmentSlotList;
    delete mrEffectsToShowThisTick;
    delete mTagList;
}
void AdvCombatEntity::Reinitialize()
{
    //--Called when a new combat begins, clears off reference lists.
    mrEffectsToShowThisTick->ClearList();

    //--Clear the temporary effects and recompute stats.
    mTemporaryEffectStatistics.Zero();
    ComputeStatistics();
}

///--[Public Variables]
//--Experience table used to compute next level requirements.
int AdvCombatEntity::xMaxLevel = 0;
int *AdvCombatEntity::xExpTable = NULL;

///================================= AbilityProfile Functions =====================================
void AbilityProfile::ReresolveAbilityPointers(AdvCombatEntity *pEntity)
{
    //--Structure defined in AdvCombatDefStruct. When this is called, the structure will reset its
    //  local set of pointers to the ones in the given entity. Otherwise they may be NULL or may
    //  be invalid. Always call it before using them.
    memset(rAbilityPtrs,  0, sizeof(AdvCombatAbility *) * ACE_ABILITY_GRID_PROFILE_SIZE_X * ACE_ABILITY_GRID_PROFILE_SIZE_Y);
    if(!pEntity) return;

    //--Ability List.
    StarLinkedList *rAbilityList = pEntity->GetAbilityList();

    //--Run across the grid and get the ability pointers.
    for(int x = 0; x < ACE_ABILITY_GRID_PROFILE_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_PROFILE_SIZE_Y; y ++)
        {
            rAbilityPtrs[x][y] = (AdvCombatAbility *)rAbilityList->GetElementByName(mAbilityNames[x][y]);
        }
    }
}

///===================================== Property Queries =========================================
bool AdvCombatEntity::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVCOMBATENTITY) return true;
    return false;
}
bool AdvCombatEntity::HasPositionedInBattle()
{
    return mHasPositionedInBattle;
}
const char *AdvCombatEntity::GetName()
{
    return mLocalName;
}
const char *AdvCombatEntity::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatEntity::GetClusterName()
{
    return mClusterName;
}
const char *AdvCombatEntity::GetResponsePath()
{
    return mResponseScriptPath;
}
const char *AdvCombatEntity::GetAIPath()
{
    return mAIScriptPath;
}
int AdvCombatEntity::GetInspectorShowStats()
{
    return mInspectorShowStats;
}
int AdvCombatEntity::GetInspectorShowResists()
{
    return mInspectorShowResists;
}
int AdvCombatEntity::GetInspectorShowAbilities()
{
    return mInspectorShowAbilities;
}
bool AdvCombatEntity::IsAmbushed()
{
    return mIsAmbushed;
}
bool AdvCombatEntity::RespondsToCode(int pCode)
{
    if(pCode < 0 || pCode >= ACE_SCRIPT_CODE_TOTAL) return false;
    return mScriptResponses[pCode];
}
bool AdvCombatEntity::IsStunnable()
{
    return mIsStunnable;
}
bool AdvCombatEntity::IsStunnedThisTurn()
{
    //--If the entity is not stunnable period, always return false.
    if(!mIsStunnable) return false;

    //--If stunnable, and the stun value is over the threshold, we will be stunned next turn
    //  so we are considered stunned.
    int tStunNeeded = GetStatistic(STATS_STUN_CAP);
    if(mStunValue >= tStunNeeded)
    {
        return true;
    }

    //--If the stun resist value is over 1, and the counter is 0, that means we got stunned last
    //  turn and are therefore still stunned.
    if(mStunResist > 0 && mStunResistDecrementTurns == 0)
    {
        return true;
    }

    //--Not stunned.
    return false;
}
int AdvCombatEntity::GetDisplayStun()
{
    return (int)mStunEasingPack.mXCur;
}
bool AdvCombatEntity::IsDisplayHPRunning()
{
    if(mHPEasingPack.mTimer < mHPEasingPack.mTimerMax) return true;
    return false;
}
bool AdvCombatEntity::IsDisplayMPRunning()
{
    if(mMPEasingPack.mTimer < mMPEasingPack.mTimerMax) return true;
    return false;
}
float AdvCombatEntity::GetDisplayHPPct()
{
    return mHPEasingPack.mXCur;
}
float AdvCombatEntity::GetDisplayMPPct()
{
    return mMPEasingPack.mXCur;
}
float AdvCombatEntity::GetDisplayAdrenaline()
{
    return mAdrenalineEasingPack.mXCur;
}
float AdvCombatEntity::GetDisplayShield()
{
    return mShieldEasingPack.mXCur;
}
int AdvCombatEntity::GetReinforcementTurns()
{
    return mReinforcementTurns;
}
StarBitmap *AdvCombatEntity::GetFaceProperties(TwoDimensionReal &sDimensions)
{
    memcpy(&sDimensions, &mFaceTableDim, sizeof(TwoDimensionReal));
    return rFaceTableImg;
}
bool AdvCombatEntity::IsMoving()
{
    if(mIgnoreMovementForBlocking) return false;
    return (mCombatPosition.mTimer < mCombatPosition.mTimerMax);
}
float AdvCombatEntity::GetCombatX()
{
    return mCombatPosition.mXCur;
}
float AdvCombatEntity::GetCombatY()
{
    return mCombatPosition.mYCur;
}
float AdvCombatEntity::GetOverrideCombatY()
{
    return mOverrideCombatY;
}
TwoDimensionRealPoint AdvCombatEntity::GetUIRenderPosition(int pIndex)
{
    if(pIndex < 0 || pIndex >= ACE_UI_INDEX_TOTAL)
    {
        TwoDimensionRealPoint tDummy;
        tDummy.mXCenter = 0.0f;
        tDummy.mYCenter = 0.0f;
        return tDummy;
    }
    return mUIPositions[pIndex];
}
bool AdvCombatEntity::CanActThisTurn()
{
    return true;
}
bool AdvCombatEntity::IsDefeated()
{
    if(mHP < 1) return true;
    return false;
}
bool AdvCombatEntity::IsAnimating()
{
    if(mCombatPosition.mTimer < mCombatPosition.mTimerMax) return true;
    return false;
}
int AdvCombatEntity::GetCurrentInitiative()
{
    return mCurrentInitiative;
}
int AdvCombatEntity::ComputeTurnBucket()
{
    //--figure out which "Bucket" this entity falls into during turn order computations. There are 7
    //  such buckets
    int tReturnVal = ACE_BUCKET_NEUTRAL;

    //--Get relevant tag counts.
    int tASLTag  = GetTagCount("Always Strikes Last");
    int tSlowTag = GetTagCount("Slow");
    int tFastTag = GetTagCount("Fast");
    int tASFTag  = GetTagCount("Always Strikes First");

    //--If the tag count for ASF vs. ASL is zero, no effect.
    if(tASFTag == tASLTag)
    {
    }
    //--If there are more ASF than ASL tags, [Always Strikes First] applies.
    else if(tASFTag > tASLTag)
    {
        tReturnVal += ACE_BUCKET_ALWAYS_STRIKES_FIRST;
    }
    //--If there are more ASL than ASF tags, [Always Strikes Last] applies.
    else
    {
        tReturnVal += ACE_BUCKET_ALWAYS_STRIKES_LAST;
    }

    //--If the tag count for Fast vs. Slow is zero, no effect.
    if(tFastTag == tSlowTag)
    {
    }
    //--If there are more Fast than Slow tags, [Fast] applies.
    else if(tFastTag > tSlowTag)
    {
        tReturnVal += ACE_BUCKET_FAST;
    }
    //--If there are more Slow than Fast tags, [Slow] applies.
    else
    {
        tReturnVal += ACE_BUCKET_SLOW;
    }

    //--Character report.
    if(false)
    {
        fprintf(stderr, "Turn report for %s\n", mLocalName);
        fprintf(stderr, "  ASF vs  ASL: %i - %i = %i\n", tASFTag, tASLTag, tASFTag - tASLTag);
        fprintf(stderr, " Fast vs Slow: %i - %i = %i\n", tFastTag, tSlowTag, tFastTag - tSlowTag);
        fprintf(stderr, " Bucket: %i\n", tReturnVal);
    }

    //--Finish up.
    return tReturnVal;
}
int AdvCombatEntity::GetTurnID()
{
    return mTurnID;
}
int AdvCombatEntity::GetWhiteFlashTimer()
{
    return mWhiteFlashTimer;
}
bool AdvCombatEntity::IsFlashingBlack()
{
    if(!mIsFlashingBlack) return false;
    if(mBlackFlashTimer < ADVCE_FLASH_BLACK_SEGMENT * 1) return true;
    if(mBlackFlashTimer < ADVCE_FLASH_BLACK_SEGMENT * 2) return false;
    if(mBlackFlashTimer < ADVCE_FLASH_BLACK_SEGMENT * 3) return true;
    return false;
}
bool AdvCombatEntity::IsKnockingOut()
{
    return mIsKnockingOut;
}
bool AdvCombatEntity::IsKnockoutFinished()
{
    return (mKnockoutTimer >= (ADVCE_KNOCKOUT_TOWHITE_TICKS + ADVCE_KNOCKOUT_TOBLACK_TICKS));
}
bool AdvCombatEntity::IsBlockRenderForKO()
{
    return mBlockRenderForKO;
}
bool AdvCombatEntity::IsRemovedOnKO()
{
    return mRemoveFromPartyOnKnockout;
}
int AdvCombatEntity::GetKnockoutTimer()
{
    return mKnockoutTimer;
}
int AdvCombatEntity::GetGlobalJP()
{
    return mGlobalJP;
}
int AdvCombatEntity::GetTotalJP()
{
    //--Run across all jobs and tally the spent/available JP, then add the global JP.
    int tTotalJP = mGlobalJP;
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->PushIterator();
    while(rJob)
    {
        tTotalJP += rJob->GetTotalJP();
        rJob = (AdvCombatJob *)mJobList->AutoIterate();
    }
    return tTotalJP;
}
int AdvCombatEntity::GetFreeActions()
{
    return mFreeActions;
}
int AdvCombatEntity::GetFreeActionsPerformed()
{
    return mFreeActionsPerformed;
}
int AdvCombatEntity::GetRewardXP()
{
    return mRewardXP;
}
int AdvCombatEntity::GetRewardJP()
{
    return mRewardJP;
}
int AdvCombatEntity::GetRewardPlatina()
{
    return mRewardPlatina;
}
int AdvCombatEntity::GetRewardDoctor()
{
    return mRewardDoctor;
}
int AdvCombatEntity::GetTurnKOd()
{
    return mTurnKOd;
}
StarLinkedList *AdvCombatEntity::GetRewardList()
{
    return mRewardItems;
}
int AdvCombatEntity::GetEffectsWithTag(const char *pTag)
{
    ///--[Documentation]
    //--Returns how many effects are currently pointing at this entity and have the given tag at least once.
    int tTotal = 0;

    //--Iterate.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        if(rEffect->GetTagCount(pTag) > 0) tTotal ++;
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Return.
    return tTotal;
}
int AdvCombatEntity::GetEffectsWithTagPartial(const char *pTag)
{
    ///--[Documentation]
    //--Returns how many effects are currently pointing at this entity and have the given tag at least once.
    //  Partial matches are allowed.
    if(!pTag) return 0;
    int tTotal = 0;

    //--Iterate.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        if(rEffect->GetTagCountPartial(pTag) > 0) tTotal ++;
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Return.
    return tTotal;
}
int AdvCombatEntity::GetEffectIDWithTag(const char *pTag, int pSlot)
{
    ///--[Documentation]
    //--Returns the ID of the Nth effect with the given tag. Can return 0 if the Nth effect does not exist.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        if(rEffect->GetTagCount(pTag) > 0)
        {
            pSlot --;
            if(pSlot < 0)
            {
                mrEffectsToShowThisTick->PopIterator();
                return rEffect->GetID();
            }
        }
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Nth effect with tag does not exist.
    return 0;
}
int AdvCombatEntity::GetEffectIDWithTagPartial(const char *pTag, int pSlot)
{
    ///--[Documentation]
    //--Returns the ID of the Nth effect with the given tag. Can return 0 if the Nth effect does not exist.
    //  Partial tag matches are allowed.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        if(rEffect->GetTagCountPartial(pTag) > 0)
        {
            pSlot --;
            if(pSlot < 0)
            {
                mrEffectsToShowThisTick->PopIterator();
                return rEffect->GetID();
            }
        }
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Nth effect with tag does not exist.
    return 0;
}
int AdvCombatEntity::GetBaseTagCount(const char *pTag)
{
    ///--[Documentation]
    //--Returns the tag count but ignores effect/equipment/etc. Just the base ones applied directly to the entity.
    int *rTagBase = (int *)mTagList->GetElementByName(pTag);
    if(rTagBase) return (*rTagBase);
    return 0;
}
const char *AdvCombatEntity::GetFirstTagNameMatching(const char *pTag)
{
    ///--[Documentation]
    //--Scan all tags on this entity, looking for a partial match of the provided tag. Return the full name of the found tag.
    //  Return "NULL" if no tags were found.
    int tLen = (int)strlen(pTag);

    ///--[Base Tags]
    void *rCheckPtr = mTagList->PushIterator();
    while(rCheckPtr)
    {
        //--If there is a match, add the count.
        const char *rName = mTagList->GetIteratorName();
        if(!strncasecmp(rName, pTag, tLen))
        {
            mTagList->PopIterator();
            return rName;
        }

        //--Next.
        rCheckPtr = mTagList->AutoIterate();
    }

    ///--[Effects]
    //--Add effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        const char *rMatching = rEffect->GetFirstTagNameMatching(pTag);
        if(rMatching)
        {
            mTagList->PopIterator();
            return rMatching;
        }

        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--No matches. Return "NULL".
    return "NULL";
}
int AdvCombatEntity::GetTagCount(const char *pTag)
{
    ///--[Documentation]
    //--Returns the total number of the given tag affecting this entity from all sources. Tags can be applied
    //  directly to the entity, be on an effect, ability, equipment, etc.
    //--Zero is a legal value even if an invalid tag is passed in.

    //--Base value.
    int tCount = 0;
    int *rTagBase = (int *)mTagList->GetElementByName(pTag);
    if(rTagBase) tCount += *rTagBase;

    //--Add effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        tCount += rEffect->GetTagCount(pTag);
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Add equipment.
    EquipmentSlotPack *rEquipPack = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
    while(rEquipPack)
    {
        if(rEquipPack->mIsComputedForTags && rEquipPack->mEquippedItem)
        {
            tCount += rEquipPack->mEquippedItem->GetTagCount(pTag);
        }
        rEquipPack = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
    }

    //--Scan across all equipped abilities. Note that abilities that spawn effects would be double-counted
    //  so only one or the other should have the tag.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(!rAbilityGrid[x][y]) continue;
            tCount += rAbilityGrid[x][y]->GetTagCount(pTag);
        }
    }

    //--Scan across unequipped abilities that are Always Run. Note than an ability could thus double-apply
    //  itself if it is equipped, the code explicitly does not prevent this!
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mAbilityList->PushIterator();
    while(rAbility)
    {
        //--Ability must have the Runs-If-Not-Equipped flag set, otherwise the ability grid above would
        //  have handled it.
        if(rAbility->RunsIfNotEquipped())
        {
            //--If this flag is true, the ability must at least be unlocked.
            if(rAbility->RunsOnlyIfUnlocked())
            {
                if(rAbility->IsUnlocked()) tCount += rAbility->GetTagCount(pTag);
            }
            //--Flag is not true, run it.
            else
            {
                tCount += rAbility->GetTagCount(pTag);
            }
        }

        //--Next.
        rAbility = (AdvCombatAbility *)mAbilityList->AutoIterate();
    }


    //--Add job.
    if(rActiveJob)
    {
        tCount += rActiveJob->GetTagCount(pTag);
    }

    //--Pass it back.
    return tCount;
}

///============================ Property Queries - World Statistics ===============================
int AdvCombatEntity::GetXPOfTable(int pLevel)
{
    //--Returns the XP value on the static table that corresponds with the given level.
    if(xMaxLevel <= 0) return 0;
    if(pLevel < 0) return 0;
    if(pLevel >= xMaxLevel) return 0;
    return xExpTable[pLevel];
}
int AdvCombatEntity::GetLevel()
{
    return mLevel;
}
int AdvCombatEntity::GetXP()
{
    return mTotalXP;
}
int AdvCombatEntity::GetXPOfLevel()
{
    //--Returns the XP needed to reach the current level.
    return GetXPOfTable(mLevel);
}
int AdvCombatEntity::GetXPToNextLevel()
{
    //--Returns XP to next level, or -1 if at max level or on error.
    if(xMaxLevel <= 0) return -1;
    if(mLevel < 0 || mLevel+1 >= xMaxLevel) return -1;
    return xExpTable[mLevel+1] - mTotalXP;
}
int AdvCombatEntity::GetXPToNextLevelMax()
{
    //--Returns the largest possible amount of XP to the next level, as if the character had exactly
    //  as much XP needed to be at the current level. Returns -1 if at max level or on error.
    if(xMaxLevel <= 0) return -1;
    if(mLevel < 0 || mLevel+1 >= xMaxLevel) return -1;
    return xExpTable[mLevel+1] - xExpTable[mLevel+0];
}
int AdvCombatEntity::GetLevelIfXPAdded(int pXPAmount)
{
    ///--[Documentation]
    //--Computes and returns an integer representing a character's level if they gained an amount of XP provided.
    //  This value can be zero or even negative. This does not compute stat changes, handles script cases blocking XP
    //  gain, and clamps to the level cap if one is provided.

    ///--[Special Cases]
    //--Check special script variables.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rBarXPVar     = (SysVar *)rDataLibrary->GetEntry("Root/Variables/CrossChapter/EXPTracker/iBarEXPGain");
    SysVar *rLevelZeroVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/CrossChapter/EXPTracker/iAlwaysLevelZero");

    //--If level-zero is mandated, always return zero.
    if(rLevelZeroVar && rLevelZeroVar->mNumeric >= 1.0f)
    {
        return 0;
    }
    //--If barring XP is mandated, levels are maintained by XP is never gained. Always return the current level.
    if(rBarXPVar && rBarXPVar->mNumeric >= 1.0f)
    {
        return mLevel;
    }

    ///--[Normal XP Handling]
    //--Get the new XP value, clamp it to zero if needed.
    int tNewXP = mTotalXP + pXPAmount;
    if(tNewXP < 0) tNewXP = 0;

    //--Run across the EXP table and resolve the level. Note that we start counting at level 1
    //  because level 0 should always require 0 XP.
    for(int i = 1; i < xMaxLevel; i ++)
    {
        if(tNewXP < xExpTable[i]) return i-1;
    }

    //--If we passed the cap on the XP table then return the max level.
    return xMaxLevel;
}
const char *AdvCombatEntity::GetParagonGrouping()
{
    return mParagonGrouping;
}
float AdvCombatEntity::GetOverrideY()
{
    return mOverrideY;
}
int AdvCombatEntity::GetMemoryCardX()
{
    return mMemoryCardX;
}
int AdvCombatEntity::GetMemoryJobX()
{
    return mMemoryJobX;
}
int AdvCombatEntity::GetMemoryJobY()
{
    return mMemoryJobY;
}
int AdvCombatEntity::GetMemoryMemorizedX()
{
    return mMemoryMemorizedX;
}
int AdvCombatEntity::GetMemoryMemorizedY()
{
    return mMemoryMemorizedY;
}
int AdvCombatEntity::GetMemoryTacticsX()
{
    return mMemoryTacticsX;
}
int AdvCombatEntity::GetMemoryTacticsY()
{
    return mMemoryTacticsY;
}
int AdvCombatEntity::GetLevelForMugAutoWin()
{
    return mLevelForMugAutoWin;
}
int AdvCombatEntity::GetLastCalloutTime()
{
    return mLastCalloutTimer;
}
int AdvCombatEntity::GetCrossfadeTimer()
{
    return mCrossfadeTimer;
}
int AdvCombatEntity::GetTFIndicatorCount()
{
    return mTFIndicatorsTotal;
}
StarBitmap *AdvCombatEntity::GetTFIndicatorImg()
{
    return rTFIndicatorImg;
}
bool AdvCombatEntity::HasBeenAssignedPosition()
{
    return mHasBeenAssignedPosition;
}
float AdvCombatEntity::GetFadePercent()
{
    if(mFadeInTimerMax < 1) return 1.0f;
    return (float)mFadeInTimer / (float)mFadeInTimerMax;
}
float AdvCombatEntity::GetShakeX()
{
    return mShakeX;
}
int AdvCombatEntity::GetCommandTimer()
{
    return mCommandTimer;
}

///======================================== Manipulators ==========================================
void AdvCombatEntity::SetCannotBeKnockedOutFlag(bool pFlag)
{
    mCannotBeKnockedOut = pFlag;
}
void AdvCombatEntity::SetHasPartingShotFlag(bool pFlag)
{
    mHasPartingShot = pFlag;
}
void AdvCombatEntity::SetRemoveFromPartyOnKnockoutFlag(bool pFlag)
{
    mRemoveFromPartyOnKnockout = pFlag;
}
void AdvCombatEntity::MarkPositionedInBattle()
{
    mHasPositionedInBattle = true;
}
void AdvCombatEntity::SetParagonGrouping(const char *pGrouping)
{
    ResetString(mParagonGrouping, pGrouping);
}
void AdvCombatEntity::SetInternalName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void AdvCombatEntity::SetDisplayName(const char *pName)
{
    ///--[Documentation]
    //--Sets the display name for the entity, following a translation if needed.
    const char *rUseString = pName;

    //--If the string passed is "DEFUALT", use the local name.
    if(!strcasecmp(pName, "DEFAULT"))
    {
        rUseString = mLocalName;
    }

    //--Check if there's a translation available for this name.
    StarTranslation *rTranslation = (StarTranslation *)DataLibrary::Fetch()->GetEntry(TRANSPATH_COMBAT);
    if(rTranslation)
    {
        const char *rTranslatedString = rTranslation->GetTranslationFor(rUseString);
        if(rTranslatedString) rUseString = rTranslatedString;
    }

    ///--[Set]
    ResetString(mDisplayName, rUseString);
}
void AdvCombatEntity::SetClusterName(const char *pName)
{
    ResetString(mClusterName, pName);
}
void AdvCombatEntity::SetResponseScript(const char *pPath)
{
    ResetString(mResponseScriptPath, pPath);
}
void AdvCombatEntity::SetResponseScriptCode(int pCode, bool pFlag)
{
    if(pCode < 0 || pCode >= ACE_SCRIPT_CODE_TOTAL) return;
    mScriptResponses[pCode] = pFlag;
}
void AdvCombatEntity::SetAnimationScript(const char *pPath)
{
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mAnimationScriptPath, NULL);
        return;
    }
    ResetString(mAnimationScriptPath, pPath);
}
void AdvCombatEntity::SetInspectorShowStats(int pAmount)
{
    mInspectorShowStats = pAmount;
}
void AdvCombatEntity::SetInspectorShowResists(int pAmount)
{
    mInspectorShowResists = pAmount;
}
void AdvCombatEntity::SetInspectorShowAbilities(int pAmount)
{
    mInspectorShowAbilities = pAmount;
}
void AdvCombatEntity::SetAIScript(const char *pPath)
{
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mAIScriptPath, NULL);
    }
    else
    {
        ResetString(mAIScriptPath, pPath);
    }
}
void AdvCombatEntity::SetAmbushed(bool pIsAmbushed)
{
    mIsAmbushed = pIsAmbushed;
}
void AdvCombatEntity::SetReinforcementTurns(int pTurns)
{
    mReinforcementTurns = pTurns;
}
void AdvCombatEntity::RegisterJob(const char *pName, AdvCombatJob *pJob)
{
    if(!pName || !pJob) return;
    mJobList->AddElement(pName, pJob, &RootObject::DeleteThis);
}
void AdvCombatEntity::SetActiveJob(const char *pJobName)
{
    //--Set job.
    rActiveJob = (AdvCombatJob *)mJobList->GetElementByName(pJobName);
    if(rActiveJob) rActiveJob->AssumeJob(this);

    //--Load the ability profile associated with this job.
    if(rActiveJob)
    {
        //--Retrieve.
        const char *rAssociatedProfileName = rActiveJob->GetAssociatedProfileName();

        //--Set. If the profile doesn't exist, will drop to default.
        SetActiveProfileS(rAssociatedProfileName);
    }

    //--Recompute stats.
    ComputeStatistics();
}
void AdvCombatEntity::RevertToCombatStartJob()
{
    //--No change, no need to do anything.
    if(!rActiveJobAtCombatStart || rActiveJob == rActiveJobAtCombatStart) return;

    //--Set pointer.
    rActiveJob = rActiveJobAtCombatStart;

    //--If the job exists, assume it and modify the profile.
    if(rActiveJob)
    {
        //--Assume.
        rActiveJob->AssumeJob(this);

        //--Retrieve.
        const char *rAssociatedProfileName = rActiveJob->GetAssociatedProfileName();

        //--Set. If the profile doesn't exist, will drop to default.
        SetActiveProfileS(rAssociatedProfileName);
    }

    //--Recompute stats.
    ComputeStatistics();
}
void AdvCombatEntity::RebuildSkillJobList()
{
    //--Rebuilds the list of jobs that appear during skill purchasing.
    mrJobShowList->ClearList();
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->PushIterator();
    while(rJob)
    {
        if(rJob->AppearsOnSkillUI()) mrJobShowList->AddElement("X", rJob);
        rJob = (AdvCombatJob *)mJobList->AutoIterate();
    }
}
void AdvCombatEntity::SetCombatPortrait(const char *pDLPath)
{
    rCombatImage = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetCombatCountermask(const char *pDLPath)
{
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rCombatCountermask = NULL;
    }
    else
    {
        rCombatCountermask = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    }
}
void AdvCombatEntity::SetVictoryCountermask(const char *pDLPath)
{
    rVictoryCountermask = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetTurnIcon(const char *pDLPath)
{
    rTurnIcon = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetVendorImage(int pSlot, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= ADVCE_VENDOR_SPRITES_TOTAL) return;
    rVendorImages[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetUIRenderPos(int pSlot, int pX, int pY)
{
    if(pSlot < 0 || pSlot >= ACE_UI_INDEX_TOTAL) return;
    mUIPositions[pSlot].mXCenter = pX;
    mUIPositions[pSlot].mYCenter = pY;
}
void AdvCombatEntity::SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath)
{
    if(!pPath) return;
    rFaceTableImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    mFaceTableDim.Set(pLft, pTop, pRgt, pBot);

    //--If the image exists, recompute face table positions.
    if(!rFaceTableImg) return;
    float cWid = rFaceTableImg->GetWidth();
    float cHei = rFaceTableImg->GetHeight();
    mFaceTableDim.mLft = mFaceTableDim.mLft / cWid;
    mFaceTableDim.mTop = mFaceTableDim.mTop / cHei;
    mFaceTableDim.mRgt = mFaceTableDim.mRgt / cWid;
    mFaceTableDim.mBot = mFaceTableDim.mBot / cHei;
    mFaceTableDim.mXCenter = mFaceTableDim.mXCenter / cWid;
    mFaceTableDim.mYCenter = mFaceTableDim.mYCenter / cHei;
}
void AdvCombatEntity::SetIdealPosition(float pXPos, float pYPos)
{
    mCombatIdealX = pXPos;
    mCombatIdealY = pYPos;
}
void AdvCombatEntity::SetIdealPositionX(float pXPos)
{
    mCombatIdealX = pXPos;
}
void AdvCombatEntity::MoveToPosition(float pXPos, float pYPos, int pTicks)
{
    mIgnoreMovementForBlocking = false;
    if(pTicks < 1 || GetPlanarDistance(pXPos, pYPos, mCombatPosition.mXCur, mCombatPosition.mYCur) < 3.0f)
    {
        mCombatPosition.MoveTo(pXPos, pYPos, 0);
    }
    else
    {
        mCombatPosition.MoveTo(pXPos, pYPos, pTicks);
    }
}
void AdvCombatEntity::MoveToIdealPosition(int pTicks)
{
    mIgnoreMovementForBlocking = false;
    if(pTicks < 1 || GetPlanarDistance(mCombatIdealX, mCombatIdealY, mCombatPosition.mXCur, mCombatPosition.mYCur) < 3.0f)
    {
        mCombatPosition.MoveTo(mCombatIdealX, mCombatIdealY, 0);
    }
    else
    {
        mCombatPosition.MoveTo(mCombatIdealX, mCombatIdealY, pTicks);
    }
}
void AdvCombatEntity::SetIgnoreMovementForBlocking(bool pFlag)
{
    mIgnoreMovementForBlocking = pFlag;
}
void AdvCombatEntity::SetCurrentInitiative(int pValue)
{
    mCurrentInitiative = pValue;
}
void AdvCombatEntity::SetTurnID(int pValue)
{
    mTurnID = pValue;
}
void AdvCombatEntity::SetFlashingWhite(bool pFlag)
{
    mIsFlashingWhite = pFlag;
}
void AdvCombatEntity::FlashBlack()
{
    mIsFlashingBlack = true;
    mBlackFlashTimer = 0;
}
void AdvCombatEntity::BeginKnockout()
{
    mIsKnockingOut = true;
    mKnockoutTimer = 0;
}
void AdvCombatEntity::FinishKnockout()
{
    mIsKnockingOut = false;
    mIsKnockedOut = true;
}
void AdvCombatEntity::SetBlockRenderForKO(bool pFlag)
{
    mBlockRenderForKO = pFlag;
}
void AdvCombatEntity::SetGlobalJP(int pAmount)
{
    mGlobalJP = pAmount;
}
void AdvCombatEntity::SetFreeActions(int pFreeActions)
{
    mFreeActions = pFreeActions;
}
void AdvCombatEntity::SetFreeActionsPerformed(int pFreeActionsPerformed)
{
    mFreeActionsPerformed = pFreeActionsPerformed;
}
void AdvCombatEntity::ResetFreeActionsToTurnStart()
{
    //--Sets number of free actions to the combat stat pack value.
    mFreeActions = GetStatistic(STATS_FREEACTIONGEN);
}
void AdvCombatEntity::SetXP(int pXP)
{
    ///--[Documentation]
    //--Sets the XP that this entity has, and recomputes the level as needed. If a special flag is set,
    //  the XP value is always set to 0, or if a different flag is set, XP is never gained.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rBarXPVar     = (SysVar *)rDataLibrary->GetEntry("Root/Variables/CrossChapter/EXPTracker/iBarEXPGain");
    SysVar *rLevelZeroVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/CrossChapter/EXPTracker/iAlwaysLevelZero");

    //--If the variables failed to resolve, or are both zero, handle this normally.
    if(!rBarXPVar || !rLevelZeroVar || (rBarXPVar->mNumeric == 0.0f && rLevelZeroVar->mNumeric == 0.0f))
    {
        //--Set XP.
        mTotalXP = pXP;
        if(mTotalXP < 0) mTotalXP = 0;

        //--Recompute level.
        int tOldLevel = mLevel;
        for(int i = 0; i < xMaxLevel; i ++)
        {
            if(mTotalXP < xExpTable[i]) break;
            mLevel = i;
        }

        //--If the level changed, recompute stats.
        if(tOldLevel == mLevel) return;
        ComputeLevelStatistics(-1);
    }
    //--If level zero is mandated, everything gets set to 0 XP. This will de-level characters!
    else if(rLevelZeroVar && rLevelZeroVar->mNumeric >= 1.0f)
    {
        mTotalXP = 0;
        mLevel = 0;
        ComputeLevelStatistics(-1);
    }
    //--If barring XP is mandated, levels are maintained by XP is never gained.
    else if(rBarXPVar && rBarXPVar->mNumeric >= 1.0f)
    {
    }
}
void AdvCombatEntity::ComputeLevelStatistics(int pLevel)
{
    //--Sets the job statistics to the listed level. Pass -1 to use the current level.
    if(pLevel == -1) pLevel = mLevel;
    if(pLevel < 0 || pLevel >= xMaxLevel || !rActiveJob) return;

    //--Call the level-up script.
    rActiveJob->CallScriptLevelUp(mLevel);

    //--Stats are now in a structure in the combat handler. Get them.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    CombatStatistics *rStats = rAdventureCombat->GetJobLevelUpStorage();

    //--Store current HP percentage.
    float tCurHPPct = GetHealthPercent();

    //--Copy them over.
    int tOldHP  = GetStatistic(STATS_HPMAX);
    int tOldAtk = GetStatistic(STATS_ATTACK);
    int tOldAcc = GetStatistic(STATS_ACCURACY);
    int tOldEvd = GetStatistic(STATS_EVADE);
    int tOldIni = GetStatistic(STATS_INITIATIVE);
    memcpy(&mJobStatistics, rStats, sizeof(CombatStatistics));
    ComputeStatistics();

    //--Set HP by percent.
    int tNewMaxHP = GetStatistic(STATS_HPMAX);
    SetHealth(tNewMaxHP * tCurHPPct);

    //--Debug.
    if(false)
    {
        fprintf(stderr, "%s Recomputing stats for level: %i\n", mLocalName, pLevel);
        fprintf(stderr, " Max HP     %i -> %i\n", tOldHP,  GetStatistic(STATS_HPMAX));
        fprintf(stderr, " Attack     %i -> %i\n", tOldAtk, GetStatistic(STATS_ATTACK));
        fprintf(stderr, " Accuracy   %i -> %i\n", tOldAcc, GetStatistic(STATS_ACCURACY));
        fprintf(stderr, " Evade      %i -> %i\n", tOldEvd, GetStatistic(STATS_EVADE));
        fprintf(stderr, " Initiative %i -> %i\n", tOldIni, GetStatistic(STATS_INITIATIVE));
    }
}
void AdvCombatEntity::GainJP(int pJP)
{
    //--If there is an active job, goes to that job.
    if(rActiveJob)
    {
        //--Get how much JP this job needs. If the amount is less than the amount gained, the rest
        //  goes to the global pool.
        int tJPCurrent = rActiveJob->GetCurrentJP();
        int tJPNeeded = rActiveJob->GetJPToMaster();
        if(tJPCurrent + pJP > tJPNeeded)
        {
            rActiveJob->SetCurrentJP(tJPNeeded);
            GainGlobalJP(pJP - (tJPNeeded - tJPCurrent));
        }
        //--It all goes to the job.
        else
        {
            rActiveJob->SetCurrentJP(tJPCurrent + pJP);
        }
        return;
    }

    //--No active job somehow, it all goes to the global.
    GainGlobalJP(pJP);
}
void AdvCombatEntity::GainGlobalJP(int pJP)
{
    mGlobalJP += pJP;
}
void AdvCombatEntity::SetRewardXP(int pXP)
{
    mRewardXP = pXP;
    if(mRewardXP < 0) mRewardXP = 0;
}
void AdvCombatEntity::SetRewardJP(int pJP)
{
    mRewardJP = pJP;
    if(mRewardJP < 0) mRewardJP = 0;
}
void AdvCombatEntity::SetRewardPlatina(int pPlatina)
{
    mRewardPlatina = pPlatina;
    if(mRewardPlatina < 0) mRewardPlatina = 0;
}
void AdvCombatEntity::SetRewardDoctor(int pDoctor)
{
    mRewardDoctor = pDoctor;
}
void AdvCombatEntity::SetTurnKOd(int pTurn)
{
    mTurnKOd = pTurn;
    if(mTurnKOd < 0) mTurnKOd = 0;
}
void AdvCombatEntity::AddRewardItem(const char *pName)
{
    if(!pName) return;
    mRewardItems->AddElement("X", InitializeString(pName), &FreeThis);
}
void AdvCombatEntity::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdvCombatEntity::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}
void AdvCombatEntity::SetOverrideY(float pY)
{
    //--Set to -1.0f to use no special override.
    mOverrideY = pY;
}
void AdvCombatEntity::SetOverrideCombatY(float pY)
{
    //--Set to -1.0f to use no special override.
    mOverrideCombatY = pY;
}
void AdvCombatEntity::SetMemoryCard(int pX)
{
    mMemoryCardX = pX;
}
void AdvCombatEntity::SetMemoryJob(int pX, int pY)
{
    mMemoryJobX = pX;
    mMemoryJobY = pY;
}
void AdvCombatEntity::SetMemoryMemorized(int pX, int pY)
{
    mMemoryMemorizedX = pX;
    mMemoryMemorizedY = pY;
}
void AdvCombatEntity::SetMemoryTactics(int pX, int pY)
{
    mMemoryTacticsX = pX;
    mMemoryTacticsY = pY;
}
void AdvCombatEntity::ClearCombatStartJob()
{
    rActiveJobAtCombatStart = NULL;
}
void AdvCombatEntity::SetLevelForMugAutoWin(int pLevel)
{
    mLevelForMugAutoWin = pLevel;
}
void AdvCombatEntity::MarkCallout()
{
    mLastCalloutTimer = Global::Shared()->gTicksElapsed;
}
void AdvCombatEntity::MarkCrossfade()
{
    rCrossfadeImg = rCombatImage;
    mCrossfadeTimer = 0;
}
void AdvCombatEntity::SetTFIndicatorCount(int pCount)
{
    mTFIndicatorsTotal = pCount;
}
void AdvCombatEntity::SetTFIndicatorIcon(const char *pDLPath)
{
    if(!pDLPath || !strcasecmp(pDLPath, "Null"))
    {
        rTFIndicatorImg = NULL;
        return;
    }
    rTFIndicatorImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::MarkAssignedPosition()
{
    mHasBeenAssignedPosition = true;
}
void AdvCombatEntity::SetFadeInTimers(int pTimerStart, int pTimerMax)
{
    mFadeInTimer = pTimerStart;
    mFadeInTimerMax = pTimerMax;
}
void AdvCombatEntity::Shake()
{
    mShakeTimer = ADVCE_SHAKE_TICKS;
}
void AdvCombatEntity::SetCommandTimer(int pTimer)
{
    mCommandTimer = pTimer;
}

///======================================== Core Methods ==========================================
void AdvCombatEntity::ClearMemoryCursors()
{
    mMemoryCardX = 0;
    mMemoryJobX = 0;
    mMemoryJobY = 0;
    mMemoryMemorizedX = 0;
    mMemoryMemorizedY = 0;
    mMemoryTacticsX = 0;
    mMemoryTacticsY = 0;
}
void AdvCombatEntity::FullRestore()
{
    //--Restores all of an entity's HP and other stats that are restored at a rest point.
    SetHealth(mSummedStatistics.GetStatByIndex(STATS_HPMAX));
}
void AdvCombatEntity::HandleCombatStart()
{
    //--When combat begins, this is called for the player's party. Enemy units never call this.
    rActiveJobAtCombatStart = rActiveJob;

    //--Reset passive abilities. These will be re-added by the AdvCombatJob later.
    mrPassiveAbilityList->ClearList();

    //--Call combat start script.
    CallResponseScript(ACE_SCRIPT_CODE_BEGINCOMBAT);

    //--Order the active job to run its combat-begins script.
    if(rActiveJob) rActiveJob->CallScript(ADVCJOB_CODE_BEGINCOMBAT);

    //--Now iterate across all equipped abilities and call their combat start script.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y])   rAbilityGrid[x][y]->CallCode(ACA_SCRIPT_CODE_BEGINCOMBAT);
            if(rSecondaryGrid[x][y]) rSecondaryGrid[x][y]->CallCode(ACA_SCRIPT_CODE_BEGINCOMBAT);
        }
    }
}
void AdvCombatEntity::HandleCombatEnd()
{
    ///--[Documentation]
    //--Handles combat ending. This occurs before the victory screen appears (if it is going to) so
    //  things like XP/Drops can be "adjusted". You cheaters.
    CallResponseScript(ACE_SCRIPT_CODE_ENDCOMBAT);

    //--Order the active job to run its combat-ends script.
    if(rActiveJob) rActiveJob->CallScript(ADVCJOB_CODE_ENDCOMBAT);

    //--Every ability, regardless of being equipped or not, zeroes its cooldown.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mAbilityList->PushIterator();
    while(rAbility)
    {
        rAbility->SetCooldown(0);
        rAbility = (AdvCombatAbility *)mAbilityList->AutoIterate();
    }

    //--Now iterate across all equipped abilities and call their combat start script. Only equipped abilities
    //  at the end of the battle run this, but all abilities reset their cooldown.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y])
            {
                rAbilityGrid[x][y]->CallCode(ACA_SCRIPT_CODE_COMBATENDS);
            }
            if(rSecondaryGrid[x][y])
            {
                rSecondaryGrid[x][y]->CallCode(ACA_SCRIPT_CODE_COMBATENDS);
            }
        }
    }

    //--Adrenaline gets added at 50% conversion to HP.
    int tAdrenaline = mAdrenaline / 2;
    mAdrenaline = 0;
    SetHealth(mHP + tAdrenaline);
    mAdrenalineEasingPack.MoveTo(mAdrenaline, -1);

    //--Shields get zeroed.
    mShield = 0;
    mShieldEasingPack.MoveTo(mShield, -1);
}
bool AdvCombatEntity::CheckKnockout()
{
    //--Checks if the entity is knocked out now but was not previously. Returns true if they were
    //  knocked out recently, false otherwise.
    if(mIsKnockedOut) return false;

    //--Has HP, not KO'd.
    if(mHP > 0) return false;

    //--Parting shot. The entity cannot be KOd but typically their parting shot ability clears the flag.
    if(mHasPartingShot) return false;

    //--HP is zero, we got KO'd.
    mIsKnockedOut = true;
    return true;
}
const char *AdvCombatEntity::GetStringFromCode(int pCode)
{
    //--Returns a human-readable string from a script call code. Used for diagnostics.
    if(pCode == ACA_SCRIPT_CODE_CREATE)                return "Create";
    if(pCode == ACA_SCRIPT_CODE_ASSUMEJOB)             return "Assume Job";
    if(pCode == ACA_SCRIPT_CODE_BEGINCOMBAT)           return "Begin Combat";
    if(pCode == ACA_SCRIPT_CODE_BEGINTURN)             return "Begin Turn";
    if(pCode == ACA_SCRIPT_CODE_BEGINACTION)           return "Begin Action";
    if(pCode == ACA_SCRIPT_CODE_BEGINFREEACTION)       return "Begin Free Action";
    if(pCode == ACA_SCRIPT_CODE_POSTACTION)            return "End Action";
    if(pCode == ACA_SCRIPT_CODE_PAINTTARGETS)          return "Paint Targets";
    if(pCode == ACA_SCRIPT_CODE_PAINTTARGETS_RESPONSE) return "Paint Targets Response";
    if(pCode == ACA_SCRIPT_CODE_EXECUTE)               return "Execute";
    if(pCode == ACA_SCRIPT_CODE_TURNENDS)              return "End Turn";
    if(pCode == ACA_SCRIPT_CODE_COMBATENDS)            return "End Combat";
    if(pCode == ACA_SCRIPT_CODE_SPECIALCREATE)         return "Special Creation (Lua)";
    if(pCode == ACA_SCRIPT_CODE_GUI_APPLY_EFFECT)      return "GUI Apply Effect";
    if(pCode == ACA_SCRIPT_CODE_EVENT_QUEUED)          return "Event Queued";
    if(pCode == ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX)  return "Build Prediction Box";
    if(pCode == ACA_SCRIPT_CODE_QUERY_CAN_RUN)         return "Query Can Run";
    if(pCode == ACA_SCRIPT_CODE_UI_PURCHASED)          return "GUI Purchase";
    return "Invalid";
}
void AdvCombatEntity::CallResponseScript(int pCode)
{
    //--Calls response script with the matching code.
    DebugPush(true, "Calling Entity response script: Entity: %s Codes: %i - %s\n", mLocalName, pCode, GetStringFromCode(pCode));
    if(pCode < 0 || pCode >= ACE_SCRIPT_CODE_TOTAL)
    {
        DebugPop("Failed, invalid code.\n");
        return;
    }
    if(!mResponseScriptPath || !mScriptResponses[pCode])
    {
        DebugPop("Failed, no response path, or not flagged to respond.\n");
        return;
    }

    DebugPrint("Calling.\n");
    LuaManager::Fetch()->PushExecPop(this, mResponseScriptPath, 1, "N", (float)pCode);
    DebugPop("Finished.\n");
}
void AdvCombatEntity::CallAIResponseScript(int pCode)
{
    if(!mAIScriptPath) return;
    LuaManager::Fetch()->PushExecPop(this, mAIScriptPath, 1, "N", (float)pCode);
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void AdvCombatEntity::UpdateTimers()
{
    ///--[Internal Timers]
    //--Easing packages.
    mHPEasingPack.Increment(EASING_CODE_QUADINOUT);
    mMPEasingPack.Increment(EASING_CODE_QUADINOUT);
    mAdrenalineEasingPack.Increment(EASING_CODE_QUADINOUT);
    mShieldEasingPack.Increment(EASING_CODE_QUADINOUT);
    mStunEasingPack.Increment(EASING_CODE_QUADINOUT);

    //--Fading timer.
    if(mFadeInTimer < mFadeInTimerMax) mFadeInTimer ++;

    //--White flash. Is a toggle.
    if(mIsFlashingWhite)
    {
        if(mWhiteFlashTimer < ADVCE_FLASH_WHITE_TICKS) mWhiteFlashTimer ++;
    }
    else
    {
        if(mWhiteFlashTimer > 0) mWhiteFlashTimer --;
    }

    //--Black flash. Plays as a sequence.
    if(mIsFlashingBlack)
    {
        mBlackFlashTimer ++;
        if(mBlackFlashTimer >= ADVCE_FLASH_BLACK_TICKS) mIsFlashingBlack = false;
    }

    //--Knockout. Plays as a sequence.
    if(mIsKnockingOut)
    {
        mKnockoutTimer ++;
    }

    //--Crossfade.
    mCrossfadeTimer ++;

    //--Shaking.
    if(mShakeTimer > 0)
    {
        mShakeTimer --;
        if(mShakeTimer % ADVCE_SHAKE_PERIODICITY == 1)
        {
            mShakeX = (rand() % ADVCE_SHAKE_SEVERITY) - (ADVCE_SHAKE_SEVERITY / 2);
        }
    }
    else
    {
        mShakeTimer = 0;
        mShakeX = 0.0f;
    }

    ///--[External Timers]
    //--If there is an external animation handler, call it here.
    if(mAnimationScriptPath)
    {
        LuaManager::Fetch()->PushExecPop(this, mAnimationScriptPath, 1, "N", (float)ACE_AI_ANIMATION_CODE_UPDATE);
    }
}
void AdvCombatEntity::UpdatePosition()
{
    //--Move position.
    mCombatPosition.Increment(EASING_CODE_QUADOUT);
    if(mIgnoreMovementForBlocking && mCombatPosition.mTimer >= mCombatPosition.mTimerMax) mIgnoreMovementForBlocking = false;
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
StarBitmap *AdvCombatEntity::GetCombatPortrait()
{
    return rCombatImage;
}
StarBitmap *AdvCombatEntity::GetCrossfadePortrait()
{
    return rCrossfadeImg;
}
StarBitmap *AdvCombatEntity::GetCombatCounterMask()
{
    return rCombatCountermask;
}
StarBitmap *AdvCombatEntity::GetVictoryCounterMask()
{
    return rVictoryCountermask;
}
StarBitmap *AdvCombatEntity::GetTurnIcon()
{
    return rTurnIcon;
}

///====================================== Pointer Routing =========================================
StarLinkedList *AdvCombatEntity::GetAbilityList()
{
    return mAbilityList;
}
AdvCombatAbility *AdvCombatEntity::GetAbilityBySlot(int pX, int pY)
{
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return NULL;
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return NULL;
    return rAbilityGrid[pX][pY];
}
AdvCombatAbility *AdvCombatEntity::GetSecondaryBySlot(int pX, int pY)
{
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return NULL;
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return NULL;
    return rSecondaryGrid[pX][pY];
}
AdvCombatJob *AdvCombatEntity::GetActiveJob()
{
    return rActiveJob;
}
StarLinkedList *AdvCombatEntity::GetJobList()
{
    return mJobList;
}
StarLinkedList *AdvCombatEntity::GetJobSkillUIList()
{
    return mrJobShowList;
}
StarLinkedList *AdvCombatEntity::GetEffectRenderList()
{
    return mrEffectsToShowThisTick;
}
StarBitmap *AdvCombatEntity::GetVendorImage(int pIndex)
{
    if(pIndex < 0 || pIndex >= ADVCE_VENDOR_SPRITES_TOTAL) return NULL;
    return rVendorImages[pIndex];
}

///===================================== Static Functions =========================================
float AdvCombatEntity::ComputeResistance(int pResistance)
{
    //--Standard resistance formula is (1.0f - (0.95f ^ pResistance)). This allows for negatives.
    //  This formula returns the %age reductions. A value of 1 return 0.05, 2 returns 0.0975, etc.
    //--Note that individual abilities may decide to compute resistance differently!
    return pow(0.95f, (float)pResistance);
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
