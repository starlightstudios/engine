///======================================= MonoUIVendor ===========================================
//--Vendor implementation, adjusted for Monoceros UI style.

#pragma once

///========================================= Includes =============================================
#include "AdvUIVendor.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define MONOUIVEN_ICONS_TOTAL 10
#define MONOUIVEN_RESISTANCE_BEGIN 6
#define MONOUIVEN_LEVELUP_STATS 5

///======================================== Translation ===========================================
///--[MonoUIVendor_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIVENDOR_STRINGS_TOTAL 56
class MonoUIVendor_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIVENDOR_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextPlatina;
            char *mTextBuy;
            char *mTextSell;
            char *mTextBuyback;
            char *mTextName;
            char *mTextPrice;
            char *mTextOwned;
            char *mTextSlots;
            char *mTextConfirmPurchase;
            char *mTextCostsPlatina;
            char *mTextEquipThisItemTo;
            char *mTextConfirmSale;
            char *mTextSaleDetails;
            char *mTextMaterials;
            char *mTextNothingSell;
            char *mTextNothingBuyback;
            char *mTextNothingUnlock;
            char *mTextComparingTo;
            char *mTextReady;
            char *mTextIncompatible;
            char *mTextImmune;
            char *mTextTierGem;
            char *mTextIngredients;
            char *mTextBack;
            char *mTextCreateItem;
            char *mTextLevelUp;
            char *mTextLevelUpCost;
            char *mTextLevelUpMaxed;
            char *mTextConfirmMerge;
            char *mTextWillMergeWith;
            char *mTextTier;
            char *mTextColors;
            char *mTextSelectAGem;
            char *mTextYouHaveNoGems;
            char *mTextProperties;
            char *mTextGemStats;
            char *mTextAdamantite;
            char *mTextPowder;
            char *mTextFlakes;
            char *mTextShards;
            char *mTextPieces;
            char *mTextChunks;
            char *mTextOre;
            char *mTextControlShowHelp;
            char *mTextControlItemDetails;
            char *mTextControlChangeCharacter;
            char *mTextControlScroll10;
            char *mTextControlToggleCompareSlot;
            char *mTextControlConfirm;
            char *mTextControlCancel;
            char *mTextControlAdjustQuantity;
            char *mTextControlQuantity10;
            char *mTextControlDisassembleGem;
            char *mTextControlToggleStats;
            char *mTextControlToggleMatPage;
            char *mTextControlToggleRelative;
        }str;
    };

    //--Functions
    MonoUIVendor_Strings();
    virtual ~MonoUIVendor_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIVendor : public MonoUICommon, virtual public AdvUIVendor
{
    protected:
    ///--[Constants]
    //--Scrollbar Values
    static const int cxMonoScrollBuf         =  3;
    static const int cxMonoScrollPageSize    =  8;
    static const int cxMonoScrollGemPageSize = 11;
    static const int cxMono_UnlScrollPage    =  7; //Size of the Unlock UI's scroll. Smaller due to 1 slot being used for ingredients.

    //Unlock UI Rendering Constants
    static const int cxMono_UnlHdgNameX      = 330; //Heading for "Name"
    static const int cxMono_UnlHdgMaterialsX = 840; //Heading for "Materials"
    static const int cxMono_UnlHdgY          = 190; //Y position of headings
    static const int cxMono_UnlNameX         = 380; //X position of the name of the item
    static const int cxMono_UnlMaterialsX    = 840; //X position of the materials count, or "Ready"
    static const int csMono_UnlMaxNameWid    = 380; //Maximum name of an unlock item before it gets squished.
    static const int cxMono_UnlockT          = 240; //Top position of the items listing.
    static const int cxMono_UnlockH          =  56; //Height of the items listing.
    static const int cxMono_IngredientL      = 325; //Ingredients stack in groups of three to use up space better. The L/M/R values
    static const int cxMono_IngredientM      = 535; //represent the left, middle, and right positions.
    static const int cxMono_IngredientR      = 745; //
    static const int cxMono_IngredientW      = 200; //Width of an ingredient name before it gets squished.
    static const int cxMono_IngredientT      = -10; //Added to the normal slot position to make it closer to the object ir represents.
    static const int cxMono_IngredientH      =  24; //Height added per material.

    ///--[Variables]
    //--System
    //--Levelup Mode
    bool mIsLevelupMode;
    int mLevelupCursor;
    char *mLevelupStatIncreases[MONOUIVEN_LEVELUP_STATS];

    //--Statistic Lookups
    int mStatisticIndexes[MONOUIVEN_ICONS_TOTAL];

    //--Comparison Frame
    bool mShowRelativeValuesOnly; //If true, only the increase/decrease between equipment is shown, not the character's current stats.

    //--Help Strings
    StarlightString *mToggleRelativeString;

    //--Strings
    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Control;
        StarFont *rFont_Help;
        StarFont *rFont_Description;
        StarFont *rFont_InlineMaterials;

        //--Images for this UI
        StarBitmap *rOverlay_ArrowCharLft;
        StarBitmap *rOverlay_ArrowCharRgt;
        StarBitmap *rOverlay_ArrowMaterialLft;
        StarBitmap *rOverlay_ArrowMaterialRgt;
        StarBitmap *rFrame_Confirm;
        StarBitmap *rFrame_GemInstructions;
        StarBitmap *rFrame_GemListing;
        StarBitmap *rFrame_GemProperties;
        StarBitmap *rFrame_LevelupButton;
        StarBitmap *rFrame_Listing;
        StarBitmap *rFrame_Materials;
        StarBitmap *rFrame_PropUnlock;
        StarBitmap *rOverlay_DescriptionName;
        StarBitmap *rOverlay_GemList;
        StarBitmap *rOverlay_GemSelect;
        StarBitmap *rOverlay_GemSlotIcon;
        StarBitmap *rOverlay_LevelupInfo;
        StarBitmap *rOverlay_MaterialShade;
        StarBitmap *rOverlay_Option;
        StarBitmap *rOverlay_Selected;
        StarBitmap *rOverlay_TitleShade;
        StarBitmap *rOverlay_Unlock;
        StarBitmap *rScrollbar_Unlock;
        StarBitmap *rScrollbar_GemsStatic;
        StarBitmap *rTab_Buy;
        StarBitmap *rTab_BuyActive;
        StarBitmap *rTab_Buyback;
        StarBitmap *rTab_BuybackActive;
        StarBitmap *rTab_Sell;
        StarBitmap *rTab_SellActive;
        StarBitmap *rTab_Unlock;
        StarBitmap *rTab_UnlockActive;
        StarBitmap *rTab_VendorStats;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIVendor();
    virtual ~MonoUIVendor();
    virtual void Construct();

    //--Public Variables
    static MonoUIVendor_Strings *xrStringData;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    virtual void SetShopInformation(const char *pShopName, const char *pPath, const char *pTeardownPath);
    void SetBuyOnly();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();
    virtual void ScrollMode(int pAmount);
    virtual void ActivateMode(bool &sModeFlag);

    ///--[ ===== Routine Files ====== ]
    ///--[Common Render]
    virtual void RenderModeTabs(float pColorAlpha);
    virtual void RenderModeTitles(float pColorAlpha);
    virtual void RenderCommonLft(float pVisAlpha);
    virtual void RenderCommonTop(float pVisAlpha);
    virtual void RenderCommonPlatina(float pColorAlpha);
    virtual void RenderItemDetails(int pTimer, int pTimerMax, AdventureItem *pItem);
    virtual void RenderItemPack(ShopInventoryPack *pItem, int pSlot, bool pIsOdd, float pAlpha);
    virtual void RenderItem(AdventureItem *pItem, int pSlot, bool pIsOdd, bool pIsSellPrice, float pAlpha);
    virtual void RenderGem(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha);
    virtual void RenderGemMerge(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha);
    virtual void RenderUnlockPack(ShopInventoryPack *pItem, int pSlot, bool pIsOdd, float pAlpha, float pScrollTimer);
    virtual void RenderComparisonPane(AdventureItem *pComparisonItem, float pAlpha);
    virtual bool RenderComparisonPaneGem(AdventureItem *pGem, float pAlpha);

    ///--[ ===== Mode Handlers ====== ]
    ///--[Buy Mode]
    virtual void RenderLftBuy(float pVisAlpha);
    virtual void RenderTopBuy(float pVisAlpha);
    virtual void RenderRgtBuy(float pVisAlpha);
    virtual void RenderBotBuy(float pVisAlpha);
    virtual void RenderUnalignedBuy(float pVisAlpha);
    virtual void RenderBuyProperties(float pVisAlpha);
    virtual void RenderBuyItem(float pVisAlpha);
    virtual void RenderBuyEquipItem(float pVisAlpha);

    ///--[Sell Mode]
    virtual void RenderLftSell(float pVisAlpha);
    virtual void RenderTopSell(float pVisAlpha);
    virtual void RenderRgtSell(float pVisAlpha);
    virtual void RenderBotSell(float pVisAlpha);
    virtual void RenderUnalignedSell(float pVisAlpha);
    virtual void RenderSellItem(float pVisAlpha);

    ///--[Buyback Mode]
    virtual void RenderLftBuyback(float pVisAlpha);
    virtual void RenderTopBuyback(float pVisAlpha);
    virtual void RenderRgtBuyback(float pVisAlpha);
    virtual void RenderBotBuyback(float pVisAlpha);
    virtual void RenderUnalignedBuyback(float pVisAlpha);

    ///--[Gems Mode]
    virtual void UpdateModeGems();
    virtual void RenderTopGems(float pVisAlpha);
    virtual void RenderRgtGems(float pVisAlpha);
    virtual void RenderBotGems(float pVisAlpha);
    virtual void RenderUnalignedGems(float pVisAlpha);
    void RenderGemsComparison(float pColorAlpha);

    ///--[Gem Merge Mode]
    void UpdateModeGemsMerge();
    virtual void RenderTopGemsMerge(float pVisAlpha);
    virtual void RenderRgtGemsMerge(float pVisAlpha);
    virtual void RenderBotGemsMerge(float pVisAlpha);
    virtual void RenderUnalignedGemsMerge(float pVisAlpha);
    virtual void RenderGemsMergeAdamantite(float pColorAlpha);
    virtual void RenderGemsMergeComparison(float pColorAlpha);
    virtual void RenderGemsMergeItem(float pVisAlpha);

    ///--[Unlock Mode]
    virtual void UpdateModeUnlock();
    virtual void RenderLftUnlock(float pVisAlpha);
    virtual void RenderTopUnlock(float pVisAlpha);
    virtual void RenderRgtUnlock(float pVisAlpha);
    virtual void RenderBotUnlock(float pVisAlpha);
    virtual void RenderUnalignedUnlock(float pVisAlpha);
    virtual void RenderMaterials(float pAlpha);
    virtual void RenderUnlockListing(int pSkip, int pMaxPerPage, float pColorAlpha);
    virtual void RenderUnlockProperties(float pVisAlpha);
    virtual void RenderUnlockMaterials(ShopInventoryPack *pItem, int pSlot, float pAlpha);

    ///--[Levelup Mode]
    void ActivateModeLevelup();
    void ConstructLevelupStrings(int pActiveSlot);
    void UpdateModeLevelup();
    void RenderLftLevelup(float pVisAlpha);
    void RenderTopLevelup(float pVisAlpha);
    void RenderRgtLevelup(float pVisAlpha);
    void RenderBotLevelup(float pVisAlpha);
    void RenderUnalignedLevelup(float pVisAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderLft(float pVisAlpha);
    virtual void RenderTop(float pVisAlpha);
    virtual void RenderRgt(float pVisAlpha);
    virtual void RenderBot(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


