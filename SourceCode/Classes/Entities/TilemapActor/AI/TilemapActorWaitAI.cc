//--Base
#include "TilemapActor.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void TilemapActor::HandleWaitAI()
{
    ///--[Documentation and Setup]
    //--Waiting. The entity stands still, sometimes changing its facing to look around. When a timer finishes, resumes its previous
    //  behavior. Can still start chasing the player.
    bool tIsIgnoringPlayer = IsIgnoringPlayer();
    bool tIsPlayerInSight = IsPlayerInSightCone(TA_USE_COMPUTED_VIEWDIST);
    float tRangeToPlayer = GetRangeToPlayer();
    float cChaseDistance = GetChaseDistance();

    //--Range check. Chase the player if close enough.
    if(tRangeToPlayer <= cChaseDistance && !tIsIgnoringPlayer && tIsPlayerInSight)
    {
        if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
        return;
    }

    //--Monster may periodically change its facing direction. This does *not* change the true facing!
    if(mWaitTimer % 45 == 30) mFacing = mFacing + ((rand() % 3) - 1);
    if(mFacing < TA_DIR_NORTH) mFacing = mFacing + TA_DIR_TOTAL;
    if(mFacing >= TA_DIR_TOTAL) mFacing = mFacing - TA_DIR_TOTAL;

    //--Timer.
    mWaitTimer ++;

    //--Ending case.
    if(mWaitTimer >= mWaitTimerMax)
    {
        //--If we have patrol nodes, turn to face them.
        if(mIsPatrolling && mPatrolNodesTotal > 0)
        {
            mAIState = TA_AI_TURNTOFACE;
        }
        //--Otherwise immediately return to wander mode.
        else
        {
            mAIState = TA_AI_WANDER;
        }

        //--Reroll.
        mMustRollWanderPoint = true;
    }
}
