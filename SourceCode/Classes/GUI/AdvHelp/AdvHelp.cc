//--Base
#include "AdvHelp.h"

//--Classes
//--CoreClasses
#include "StarlightString.h"
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"

///========================================== System ==============================================
AdvHelp::AdvHelp()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVHELP;

    //--[AdvHelp]
    //--System
    mStringsTotal = 0;
    mStrings = NULL;

    //--Legend Strings
    mLegendStringsTotal = 0;
    mLegendStrings = NULL;
    mLegendPositions = NULL;

    //--Render
    mIsReady = false;
    rHeadingFont = NULL;
    rMainlineFont = NULL;
    rBacking = NULL;
}
AdvHelp::~AdvHelp()
{
    for(int i = 0; i < mStringsTotal;       i ++) delete mStrings[i];
    for(int i = 0; i < mLegendStringsTotal; i ++) delete mLegendStrings[i];
    free(mStrings);
    free(mLegendStrings);
    free(mLegendPositions);
}
void AdvHelp::Construct()
{
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rHeadingFont = rDataLibrary->GetFont("Adventure Help Heading");
    rMainlineFont = rDataLibrary->GetFont("Adventure Help Mainline");
    rBacking = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/HelpBacking");
    mIsReady = (rHeadingFont && rMainlineFont && rBacking);
}

///===================================== Property Queries =========================================
///======================================== Manipulators ==========================================
void AdvHelp::AllocateStrings(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mStringsTotal; i ++) delete mStrings[i];
    free(mStrings);

    //--Zero.
    mStringsTotal = 0;
    mStrings = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mStringsTotal = pTotal;
    mStrings = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mStringsTotal);
    for(int i = 0; i < mStringsTotal; i ++) mStrings[i] = new StarlightString();
}
void AdvHelp::AllocateLegendStrings(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mLegendStringsTotal; i ++) delete mLegendStrings[i];
    free(mLegendStrings);
    free(mLegendPositions);

    //--Zero.
    mLegendStringsTotal = 0;
    mLegendStrings = NULL;
    mLegendPositions = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mLegendStringsTotal = pTotal;
    mLegendStrings   = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mLegendStringsTotal);
    mLegendPositions = (LegendStringPos *) starmemoryalloc(sizeof(LegendStringPos)   * mLegendStringsTotal);
    for(int i = 0; i < mLegendStringsTotal; i ++) mLegendStrings[i] = new StarlightString();
}

///======================================== Core Methods ==========================================
void AdvHelp::AssembleLegend()
{
    ///--[Documentation]
    //--Assembles the legend, which is standardized. This shows symbols and what they mean. It does
    //  not change as the player changes controls.
    if(!mIsReady) return;
    int i = 0;
    float cStdIconOffY = 3.0f;
    AllocateLegendStrings(26);
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Damage Icons]
    //--Setup.
    float cXLft = 48.0f;
    float cXCnt = cXLft + 193.0f;
    float cXRgt = cXLft + 380.0f;
    float tYPos = 434.0f;
    float cTxtHei = rHeadingFont->GetTextHeight();

    //--Indicates damage types. First, the heading.
    mLegendStrings[i]->SetString("Damage Types");
    mLegendPositions[i].Set(310.0f, tYPos, SUGARFONT_AUTOCENTER_X);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--First line: Physical damage types.
    mLegendStrings[i]->SetString("[IMG0] Slashing");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Slashing"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Piercing");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Piercing"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Striking");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Striking"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--Second Line: Elemental damage types.
    mLegendStrings[i]->SetString("[IMG0] Flaming");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Flaming"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Freezing");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Freezing"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Shocking");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Shocking"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--Third Line: Light, Dark, Psychology
    mLegendStrings[i]->SetString("[IMG0] Crusading");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Crusading"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Obscuring");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Obscuring"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Terrifying");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Terrifying"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--Fourth Line: DoTs
    mLegendStrings[i]->SetString("[IMG0] Bleeding");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Bleeding"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Poisoning");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Poisoning"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;

    mLegendStrings[i]->SetString("[IMG0] Corroding");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Corroding"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    ///--[Statistic Icons]
    //--Setup.
    cXLft = 688.0f;
    cXCnt = cXLft + 183.0f;
    cXRgt = cXLft + 384.0f;
    tYPos = 434.0f;

    //--Indicates damage types. First, the heading.
    mLegendStrings[i]->SetString("Stat Icons");
    mLegendPositions[i].Set(964, tYPos, SUGARFONT_AUTOCENTER_X);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--First line.
    mLegendStrings[i]->SetString("[IMG0] Health");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Health"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Shields");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Shields"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Adrenaline");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Adrenaline"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--Second Line.
    mLegendStrings[i]->SetString("[IMG0] Attack");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Attack"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Protection");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Protection"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Initiative");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Initiative"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--Third Line.
    mLegendStrings[i]->SetString("[IMG0] Accuracy");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Accuracy"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Evade");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Evade"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Combo Point");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/ComboPoint"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    //--Fourth Line
    mLegendStrings[i]->SetString("[IMG0] Mana");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Mana"));
    mLegendPositions[i].Set(cXLft, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Stun");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Stun"));
    mLegendPositions[i].Set(cXCnt, tYPos, 0);
    i ++;
    mLegendStrings[i]->SetString("[IMG0] Threat");
    mLegendStrings[i]->AllocateImages(1);
    mLegendStrings[i]->SetImageP(0, cStdIconOffY, (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Threat"));
    mLegendPositions[i].Set(cXRgt, tYPos, 0);
    i ++;
    tYPos = tYPos + cTxtHei;

    ///--[Clean Up]
    //--Crossreference all strings.
    for(int p = 0; p < mLegendStringsTotal; p ++)
    {
        mLegendStrings[p]->CrossreferenceImages();
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
void AdvHelp::Render(float pYOffset)
{
    ///--[Documentation and Setup]
    //--Render the help window with a Y offset passed in by the parent class.
    if(!mIsReady) return;

    //--Position.
    glTranslatef(0.0f, pYOffset, 0.0f);

    ///--[Fixed Components]
    //--Backing.
    rBacking->Draw();

    //--Heading.
    rHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f,  81.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Controls");
    rHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 408.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Icon Legend");

    ///--[Render Lines]
    //--Position.
    float cTxtLft = 403.0f;
    float tTxtTop = 118.0f;
    float cTxtHei = rHeadingFont->GetTextHeight();

    //--Iterate.
    for(int i = 0; i < mStringsTotal; i ++)
    {
        mStrings[i]->DrawText(cTxtLft, tTxtTop, 0, 1.0f, rMainlineFont);
        tTxtTop = tTxtTop + cTxtHei;
    }

    ///--[Legend]
    //--Shows what icons mean what.
    for(int i = 0; i < mLegendStringsTotal; i ++)
    {
        mLegendStrings[i]->DrawText(mLegendPositions[i].mXPos, mLegendPositions[i].mYPos, mLegendPositions[i].mFlags, 1.0f, rMainlineFont);
    }

    ///--[Clean Up]
    glTranslatef(0.0f, -pYOffset, 0.0f);
}

///====================================== Pointer Routing =========================================
StarlightString **AdvHelp::GetStrings()
{
    return mStrings;
}

///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
