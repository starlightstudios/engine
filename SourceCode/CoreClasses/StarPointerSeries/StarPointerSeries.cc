//--Base
#include "StarPointerSeries.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "StarLumpManager.h"

///--[Debug]
//#define SPS_DEBUG
#ifdef SPS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
StarPointerSeries::StarPointerSeries()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[ ========= Derived ======== ]
    ///--[System]
    mMembers = new StarLinkedList(true);

    ///--[ ================ Construction ================ ]
    ///--[Images]
    ///--[Verify]
}
StarPointerSeries::~StarPointerSeries()
{
    delete mMembers;
}
void StarPointerSeries::DeleteThis(void *pPtr)
{
    StarPointerSeries *rPtr = (StarPointerSeries *)pPtr;
    delete rPtr;
}

///--[Public Statics]
//--See StarPointerSeries.h for explanation of what this list does.
#if defined SPS_TRACKS_DUMMIES
StarLinkedList *StarPointerSeries::xAllExtractOrders = new StarLinkedList(true);
StarLinkedList *StarPointerSeries::xSafeRemovalList  = new StarLinkedList(false);
#endif

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void StarPointerSeries::RegisterEntry(int pSubtype, const char *pPath)
{
    if(!pPath || pSubtype < SPS_SUBTYPE_NONE || pSubtype >= SPS_SUBTYPE_MAX) return;
    PointerSeriesEntry *nEntry = (PointerSeriesEntry *)starmemoryalloc(sizeof(PointerSeriesEntry));
    nEntry->Initialize();
    nEntry->mSubtype = pSubtype;
    ResetString(nEntry->mPath, pPath);
    mMembers->AddElementAsTail("X", nEntry, &PointerSeriesEntry::DeleteThis);
}

///======================================= Core Methods ===========================================
void StarPointerSeries::Crossload(void *pPtr, int pEntriesExpected)
{
    ///--[Documentation]
    //--Given a location in memory and a number of entries expected (usually derived from sizeof()), attempts
    //  to load the entries in the list, in order, onto the object. If the incorrect number of entries
    //  is expected, prints a warning and does nothing.
    DebugPush(true, "StarPointerSeries:Crossload() - Begin.\n");
    DebugPrint("Address: %p\n", pPtr);
    DebugPrint("Entries: %i\n", pEntriesExpected);

    ///--[Error Checking]
    //--Pointer validity.
    if(!pPtr)
    {
        DebugManager::ForcePrint("StarPointerSeries::Crossload() - Warning, pointer destination was NULL.\n");
        DebugPop("");
        return;
    }

    //--Check entry count.
    if(pEntriesExpected < 1 || pEntriesExpected != mMembers->GetListSize())
    {
        DebugManager::ForcePrint("StarPointerSeries::Crossload() - Warning, entry size of list is %i but expected entries is %i, failing.\n", mMembers->GetListSize(), pEntriesExpected);
        DebugPop("");
        return;
    }

    ///--[Begin Loading]
    //--Setup.
    intptr_t tMemoryAddress = (intptr_t)pPtr;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Iterate.
    PointerSeriesEntry *rEntry = (PointerSeriesEntry *)mMembers->PushIterator();
    while(rEntry)
    {
        //--Get the pointer position.
        void **rPtr = (void **)tMemoryAddress;

        //--Skip. Ignores type. Used when a structure has a non-pointer element.
        if(rEntry->mSubtype == SPS_SUBTYPE_NONE)
        {

        }
        //--Image.
        else if(rEntry->mSubtype == SPS_SUBTYPE_IMAGE)
        {
            //--Special: If the path starts with "Dummy/" then this bitmap is intended to be flagged as a dummy. Dummy
            //  bitmaps do not render and bark a warning if they do.
            //--Dummy bitmaps are used to reduce file and memory sizes when using derived games like Monoceros and Carnation,
            //  which want to maintain "completeness" in their UI objects. Once you're sure an object can be safely dummied
            //  without causing a crash you can remove it from the datafiles for the derived game.
            //--To make this easier, SPS_TRACKS_DUMMIES can be defined to print a report from the debug menu.
            if(!strncasecmp(rEntry->mPath, "Dummy/", 6))
            {
                //--Reconstruct the path with the Dummy/ part replaced with Root/.
                char *tNewPath = InitializeString("Root/%s", &rEntry->mPath[6]);
                StarBitmap *rCheckBitmap = (StarBitmap *)rDataLibrary->GetEntry(tNewPath);
                *rPtr = rCheckBitmap;

                //--If the pointer is found, mark it as a dummy.
                if(rCheckBitmap) rCheckBitmap->SetDummyMode(tNewPath);

                //--If this definition is set, track the image path.
                #if defined SPS_TRACKS_DUMMIES
                xSafeRemovalList->AddElement(tNewPath, xSafeRemovalList);
                #endif

                //--Clean.
                free(tNewPath);
            }
            //--Normal case.
            else
            {
                *rPtr = (StarBitmap *)rDataLibrary->GetEntry(rEntry->mPath);
            }

            //--Diagnostics.
            DebugPrint("Loaded image: %p %s\n", *rPtr, rEntry->mPath);
        }
        //--Font.
        else if(rEntry->mSubtype == SPS_SUBTYPE_FONT)
        {
            *rPtr = rDataLibrary->GetFont(rEntry->mPath);
            DebugPrint("Loaded font: %p %s\n", *rPtr, rEntry->mPath);
        }

        //--Next.
        tMemoryAddress += sizeof(void *);
        rEntry = (PointerSeriesEntry *)mMembers->AutoIterate();
    }

    ///--[Diagnostics]
    DebugPop("Finished normally.\n");
}
void StarPointerSeries::CrossloadDiagnostics()
{
    ///--[Documentation]
    //--Diagnostic version of Crossload() that doesn't actually do anything in terms of moving memory
    //  around. This is instead used for building purge files.

    PointerSeriesEntry *rEntry = (PointerSeriesEntry *)mMembers->PushIterator();
    while(rEntry)
    {
        //--Skip. Ignores type. Used when a structure has a non-pointer element.
        if(rEntry->mSubtype == SPS_SUBTYPE_NONE)
        {

        }
        //--Image.
        else if(rEntry->mSubtype == SPS_SUBTYPE_IMAGE)
        {
            //--If this is a Dummy/ entry, adds it to the safe removal list. Otherwise, does nothing.
            if(!strncasecmp(rEntry->mPath, "Dummy/", 6))
            {
                //--Reconstruct the path with the Dummy/ part replaced with Root/.
                char *tNewPath = InitializeString("Root/%s", &rEntry->mPath[6]);

                //--If this definition is set, track the image path.
                #if defined SPS_TRACKS_DUMMIES
                xSafeRemovalList->AddElement(tNewPath, xSafeRemovalList);
                #endif

                //--Clean.
                free(tNewPath);
            }
        }
        //--Font. Does nothing for now.
        else if(rEntry->mSubtype == SPS_SUBTYPE_FONT)
        {
        }

        //--Next.
        rEntry = (PointerSeriesEntry *)mMembers->AutoIterate();
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
///========================================= File I/O =============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
void StarPointerSeries::RegisterExtractOrder(const char *pInfileName, const char *pDLPathName)
{
    ///--[Documentation]
    //--Registers the name and path to the static list. Duplicates are stored as long as either does
    //  not match. This is used to create a set of backward lookups from DLPath to infile name to
    //  eventually the datafile, so the datafile can be cleared of unused images.
    //--This can legally be called if the define isn't set, but it won't do anything.
    if(!pInfileName || !pDLPathName) return;
    #if defined SPS_TRACKS_DUMMIES

    ///--[Error Checks]
    //--Check if there are duplicates. The entire list needs to be scanned for this.
    PurgeDatafile *rDatafileListing = (PurgeDatafile *)xAllExtractOrders->PushIterator();
    while(rDatafileListing)
    {
        //--If this is already registered, stop.
        if(rDatafileListing->IsNamePathRegistered(pInfileName, pDLPathName))
        {
            xAllExtractOrders->PopIterator();
            return;
        }

        //--Next.
        rDatafileListing = (PurgeDatafile *)xAllExtractOrders->AutoIterate();
    }

    ///--[Register]
    //--If we got this far, there is a unique path set needing to be stored. Get the datafile path from
    //  the SLM.
    char *rOpenPath = StarLumpManager::Fetch()->GetOpenPath();
    if(!rOpenPath) return;

    //--If the path already exists, add this entry to its grouping.
    PurgeDatafile *rExistingDatafile = (PurgeDatafile *)xAllExtractOrders->GetElementByName(rOpenPath);
    if(rExistingDatafile)
    {
        rExistingDatafile->StoreInfileNamePath(pInfileName, pDLPathName);
        return;
    }

    //--If the path does not exist, create and store a new entry.
    PurgeDatafile *nDatafile = (PurgeDatafile *)starmemoryalloc(sizeof(PurgeDatafile));
    nDatafile->Initialize();
    nDatafile->SetName(rOpenPath);
    nDatafile->StoreInfileNamePath(pInfileName, pDLPathName);

    //--Register the datafile pack to the global list.
    xAllExtractOrders->AddElementAsTail(rOpenPath, nDatafile, &PurgeDatafile::DeleteThis);
    #endif
}

///======================================== Lua Hooking ===========================================
void StarPointerSeries::HookToLuaState(lua_State *pLuaState)
{
     /* StarPointerSeries_SetProperty("Create New") (Static)
        StarPointerSeries_SetProperty("Finish") (Static)
       Sets the property in the StarPointerSeries class. */
    lua_register(pLuaState, "StarPointerSeries_SetProperty", &Hook_StarPointerSeries_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_StarPointerSeries_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //StarPointerSeries_SetProperty("Create New", sName) (Static)
    //StarPointerSeries_SetProperty("Finish") (Static)
    //StarPointerSeries_SetProperty("Add Image", sPath)
    //StarPointerSeries_SetProperty("Add Font", sPath)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("StarPointerSeries_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    ///--[Static]
    //--Creates a new StarPointerSeries object. If one already exists with the given name, pushes a special pointer.
    if(!strcasecmp(rSwitchType, "Create New") && tArgs >= 2)
    {
        //--Check if the named entry already exists. If it does, push the DataLibrary itself.
        void *rCheckPtr = rDataLibrary->GetStarPointerSeries(lua_tostring(L, 2));
        if(rCheckPtr)
        {
            rDataLibrary->PushActiveEntity(rDataLibrary);
            return 0;
        }

        //--Create and push a new StarPointerSeries. Also register it to the DataLibrary's internal list.
        StarPointerSeries *nSPS = new StarPointerSeries();
        rDataLibrary->RegisterStarPointerSeries(lua_tostring(L, 2), nSPS);
        rDataLibrary->PushActiveEntity(nSPS);
        return 0;
    }
    //--Pop the object off the data library.
    else if(!strcasecmp(rSwitchType, "Finish"))
    {
        rDataLibrary->PopActiveEntity();
        return 0;
    }

    ///--[Dynamic]
    //--Special: If the object in question is the DataLibrary's pointer, that means the object already existed
    //  when creating the list. This is caused by the player reloading the game when assets are already in memory.
    //  If this happens, all non-static calls fail silently.
    StarPointerSeries *rStarPointerSeries = (StarPointerSeries *)rDataLibrary->rActiveObject;
    if((void *)rStarPointerSeries == (void *)rDataLibrary) return 0;

    //--Type check.
    if(!rDataLibrary->IsActiveValid()) return LuaTypeError("StarPointerSeries_SetProperty", L);

    ///--[System]
    //--Register an image entry.
    if(!strcasecmp(rSwitchType, "Add Image") && tArgs >= 2)
    {
        rStarPointerSeries->RegisterEntry(SPS_SUBTYPE_IMAGE, lua_tostring(L, 2));
    }
    else if(!strcasecmp(rSwitchType, "Add Font") && tArgs >= 2)
    {
        rStarPointerSeries->RegisterEntry(SPS_SUBTYPE_FONT, lua_tostring(L, 2));
    }
    //--Error case.
    else
    {
        LuaPropertyError("StarPointerSeries_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
