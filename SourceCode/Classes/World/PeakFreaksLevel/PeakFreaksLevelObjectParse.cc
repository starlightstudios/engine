//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "TileLayer.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"

///--[Local Definitions]
//#define PEAKFREAKSLEVELOBJECT_DEBUG
#ifdef PEAKFREAKSLEVELOBJECT_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///==================================== Object Data Parser ========================================
void PeakFreaksLevel::ParseObjectData()
{
    ///--[Documentation]
    //--Once the data is read and stored, we can parse through it to pull out the parts we want.
    //  Note that this usually is not done right when the objects are read out, as there may be limiters
    //  set or objects might be discarded by script values.

    ///--[Debug]
    DebugPush(true, "PeakFreaksLevel:ParseObjectData Begins.\n");

    //--Iterate across them all.
    ObjectInfoPack *rObjectPack = (ObjectInfoPack *)mObjectData->PushIterator();
    while(rObjectPack)
    {
        HandleObject(rObjectPack);
        rObjectPack = (ObjectInfoPack *)mObjectData->AutoIterate();
    }

    ///--[Debug]
    DebugPop("PeakFreaksLevel:ParseObjectData ends, ended normally.\n");
}
ZonePack *PeakFreaksLevel::CreateStandardZone(ObjectInfoPack *pPack, bool pOffsetYTile)
{
    ///--[Documentation]
    //--Given an object pack, creates a "Standard" zone. This one will match the dimensions and layer.
    //  The flag pOffsetYTile specifies that this is an object with a tile in Tiled, which means it is
    //  positioned bottom-up and not top-down so its coordinates are different.
    ZonePack *nPackage = new ZonePack();
    if(!pPack) return nPackage;

    //--Position.
    if(!pOffsetYTile)
    {
        nPackage->mDim.Set(pPack->mX, pPack->mY, pPack->mX + pPack->mW, pPack->mY + pPack->mH);
    }
    //--Special Y Offset.
    else
    {
        nPackage->mDim.Set(pPack->mX, pPack->mY - 16.0f, pPack->mX + pPack->mW, pPack->mY + pPack->mH - 16.0f);
    }

    //--Standard properties.
    for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
    {
        //--Fast Access
        char *rKey = pPack->mProperties->mKeys[i];
        char *rVal = pPack->mProperties->mVals[i];

        //--Layer.
        if(!strcasecmp(rKey, "Layer"))
        {
            nPackage->mLayer = atoi(rVal);
        }
    }

    //--Finish up.
    return nPackage;
}
void PeakFreaksLevel::HandleObject(ObjectInfoPack *pPack)
{
    ///--[Documentation]
    //--Given an object pack, handles storing and/or creating the final copy in the level.

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    const char *rType = pPack->mType;

    //--Debug.
    DebugPrint("Object %p %s: Type %s\n", pPack, pPack->mName, rType);

    ///--[Chest]
    //--Stores lots of coins.
    if(!strcasecmp(rType, "Treasure"))
    {
        ZonePack *nTreasure = CreateStandardZone(pPack, true);
        mTreasureZones->AddElement("X", nTreasure, &ZonePack::DeleteThis);
    }
    ///--[Coin]
    //--Is exactly one coin.
    else if(!strcasecmp(rType, "Coin"))
    {
        ZonePack *nCoin = CreateStandardZone(pPack, true);
        mCoinZones->AddElement("X", nCoin, &ZonePack::DeleteThis);
    }
    ///--[Player Start]
    //--Sets where the player starts the level. Also acts as the zero checkpoint.
    else if(!strcasecmp(rType, "PlayerStart"))
    {
        //--Base.
        //mCheckpointX = pPack->mX;
        //mCheckpointY = pPack->mY;
        //mCheckpointZ = 0;

        //--Store the name.
        ResetString(mActiveCheckpointName, pPack->mName);

        //--Property Check:
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Layer.
            if(!strcasecmp(rKey, "Layer"))
            {
                //mCheckpointZ = atoi(rVal);
            }
        }

        //--Also store it on a list. This is used for reconstituting the level.
        ZonePack *nPlayerStart = CreateStandardZone(pPack, false);
        mPlayerStartZones->AddElement(pPack->mName, nPlayerStart, &ZonePack::DeleteThis);
    }
    ///--[Triggers]
    //--Triggers, activates scripts.
    else if(!strcasecmp(rType, "Trigger"))
    {
        ZonePack *nTrigger = CreateStandardZone(pPack, false);
        mTriggerZones->AddElement(pPack->mName, nTrigger, &ZonePack::DeleteThis);
    }
    ///--[Checkpoint]
    //--When the player touches one of these, changes where they respawn.
    else if(!strcasecmp(rType, "Checkpoint"))
    {
        ZonePack *nCheckpoint = CreateStandardZone(pPack, false);
        mCheckpointZones->AddElement(pPack->mName, nCheckpoint, &ZonePack::DeleteThis);
    }
    ///--[Goal]
    //--When the player touches one of these, they win the level!
    else if(!strcasecmp(rType, "Goal"))
    {
        ZonePack *nGoal = CreateStandardZone(pPack, false);
        mVictoryZones->AddElement("X", nGoal, &ZonePack::DeleteThis);
    }
    ///--[Damage]
    //--When the player touches one of these, they get hurt and go back to the last checkpoint.
    else if(!strcasecmp(rType, "Damage"))
    {
        ZonePack *nDamageZone = CreateStandardZone(pPack, false);
        mDamageZones->AddElement("X", nDamageZone, &ZonePack::DeleteThis);
    }
    //--Same as a damage zone, but animates differently.
    else if(!strcasecmp(rType, "Hole"))
    {
        ZonePack *nDamageZone = CreateStandardZone(pPack, false);
        mDamageZones->AddElement("X", nDamageZone, &ZonePack::DeleteThis);
    }
    ///--[Layer Drop]
    //--When the player touches one of these, they move to the destination layer. Has additional
    //  properties and does not use the standard constructor.
    else if(!strcasecmp(rType, "Layer Drop"))
    {
        //--Create.
        DropZonePack *nPackage = new DropZonePack();

        //--Position.
        nPackage->mDim.Set(pPack->mX, pPack->mY, pPack->mX + pPack->mW, pPack->mY + pPack->mH);

        //--Property Check:
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Layer.
            if(!strcasecmp(rKey, "Layer"))
            {
                nPackage->mLayer = atoi(rVal);
            }
            //--Destination.
            else if(!strcasecmp(rKey, "Destination"))
            {
                nPackage->mDestination = atoi(rVal);
            }
        }

        //--Register.
        mLayerDropZones->AddElement("X", nPackage, &DropZonePack::DeleteThis);
    }
    ///--[Slip Zone]
    //--When the player touches one of these, they slide to the bottom.
    else if(!strcasecmp(rType, "SlipZone"))
    {
        ZonePack *nPackage = CreateStandardZone(pPack, false);
        mSlipZones->AddElement("X", nPackage, &ZonePack::DeleteThis);
    }
    ///--[Climb Zone]
    //--When the player touches one of these, they slide to the bottom.
    else if(!strcasecmp(rType, "ClimbZone"))
    {
        ZonePack *nPackage = CreateStandardZone(pPack, false);
        mClimbZones->AddElement("X", nPackage, &ZonePack::DeleteThis);
    }
    ///--[Slider Hazard]
    else if(!strcasecmp(rType, "Slider"))
    {
        //--Basics.
        PF_Slider *nSlider = (PF_Slider *)starmemoryalloc(sizeof(PF_Slider));
        nSlider->Initialize();
        nSlider->mXPos = pPack->mX;
        nSlider->mYPos = pPack->mY - TileLayer::cxSizePerTile;
        nSlider->mXStart = nSlider->mXPos;
        nSlider->mYStart = nSlider->mYPos;
        nSlider->mXEnd = nSlider->mXPos;
        nSlider->mYEnd = nSlider->mYPos;
        nSlider->rRenderImg = Images.Data.rHazardSlider;

        //--Temps.
        int tSize = 1;
        bool tIsVertical = false;

        //--Property Check:
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Toggles vertical.
            if(!strcasecmp(rKey, "Vert"))
            {
                tIsVertical = true;
            }
            //--Size of movement range.
            else if(!strcasecmp(rKey, "Size"))
            {
                tSize = atoi(rVal);
            }
        }

        //--Compute destination.
        if(!tIsVertical)
        {
            nSlider->mXEnd = nSlider->mXStart + (tSize * TileLayer::cxSizePerTile);
            nSlider->mBumpDirX = 1;
        }
        else
        {
            nSlider->mYEnd = nSlider->mYStart + (tSize * TileLayer::cxSizePerTile);
            nSlider->mBumpDirY = 1;
        }

        //--Register.
        mSliderList->AddElement("X", nSlider, &FreeThis);
    }
    ///--[Spinner Hazard]
    else if(!strcasecmp(rType, "Spinner"))
    {
        //--Basics.
        PF_Spinner *nSpinner = (PF_Spinner *)starmemoryalloc(sizeof(PF_Spinner));
        nSpinner->Initialize();
        nSpinner->mXPos = pPack->mX;
        nSpinner->mYPos = pPack->mY - TileLayer::cxSizePerTile;

        //--Clockwise is rolled randomly.
        nSpinner->mIsClockwise = (rand() % 100) >= 50;
        nSpinner->mSpinPos = rand() % PF_SPINNER_SMALL_DIRS;

        //--Images.
        for(int i = 0; i < PF_SPINNER_SMALL_DIRS; i ++)
        {
            nSpinner->rRenderImg[i] = Images.Data.rHazardSpinner[i];
        }

        //--Property Check:
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Toggles vertical.
            if(!strcasecmp(rKey, "Big"))
            {
                nSpinner->mIsBig = true;
                nSpinner->mIsClockwise = true;
                nSpinner->mSpinPos = rand() % PF_SPINNER_BIG_DIRS;
                for(int i = 0; i < PF_SPINNER_BIG_DIRS; i ++)
                {
                    nSpinner->rRenderImg[i] = Images.Data.rHazardSpinnerBig[i];
                }
            }
        }

        //--Register.
        mSpinnerList->AddElement("X", nSpinner, &FreeThis);
    }
    ///--[Waterfall Hazard]
    else if(!strcasecmp(rType, "Waterfall"))
    {
        //--Basics.
        PF_Waterfall *nWaterfall = (PF_Waterfall *)starmemoryalloc(sizeof(PF_Waterfall));
        nWaterfall->Initialize();
        nWaterfall->mXPos = pPack->mX;
        nWaterfall->mYPos = pPack->mY - TileLayer::cxSizePerTile;

        //--Images.
        for(int i = 0; i < PF_WATERFALL_FRAMES; i ++)
        {
            nWaterfall->rCycleImg[i] = Images.Data.rHazardWaterfallFrames[i];
        }
        nWaterfall->rWaterfallTop = Images.Data.rHazardWaterfallTop;
        nWaterfall->rWaterfallMid = Images.Data.rHazardWaterfallMid;
        nWaterfall->rWaterfallBot = Images.Data.rHazardWaterfallBot;

        //--Property Check:
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Toggles vertical.
            if(!strcasecmp(rKey, "Size"))
            {
                nWaterfall->mSize = atoi(rVal);
            }
        }

        //--Register.
        mWaterfallList->AddElement("X", nWaterfall, &FreeThis);
    }
    ///--[BombSpike / Crystal Hazard]
    else if(!strcasecmp(rType, "Crystal"))
    {
        //--Basics.
        PF_BombSpike *nBombSpike = (PF_BombSpike *)starmemoryalloc(sizeof(PF_BombSpike));
        nBombSpike->Initialize();
        nBombSpike->mXPos = pPack->mX;
        nBombSpike->mYPos = pPack->mY - TileLayer::cxSizePerTile;

        //--Images.
        nBombSpike->rNeutralImg = Images.Data.rHazardBombSpikeNeutral;
        for(int i = 0; i < PF_BOMBSPIKE_EXPLOSION_FRAMES; i ++)
        {
            nBombSpike->rExplosions[i] = Images.Data.rHazardBombSpikeExplosion[i];
        }

        //--Register.
        mBombSpikeList->AddElement("X", nBombSpike, &FreeThis);
    }
}
