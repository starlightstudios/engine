//--Base
#include "AdvUIDoctor.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarPointerSeries.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"

///--[Debug]
//#define ADVUIDOCTOR_DEBUG
#ifdef ADVUIDOCTOR_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
AdvUIDoctor::AdvUIDoctor()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "ADOC");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Images]

    ///--[ ====== AdvUIDoctor ======= ]
    ///--[Cursor]
    //--Indicator.
    mCursor = 0;

    //--Cursor
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    ///--[Help Strings]
    mInstructionsStringA = new StarlightString();
    mInstructionsStringB = new StarlightString();
    mInstructionsStringC = new StarlightString();
    AllocateHelpStrings(cHelpStringsTotal);

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Add the AdvImages structure to the verify list.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUIDoctor::~AdvUIDoctor()
{
    delete mInstructionsStringA;
    delete mInstructionsStringB;
    delete mInstructionsStringC;
}
void AdvUIDoctor::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Run subroutines.
    ResolveSeriesInto("Adventure Doctor UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",   &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Used by the AdvUIBase menu to continue displaying the party after the doctor UI is visible.
int AdvUIDoctor::xDoctorVisTimer = 0;

///===================================== Property Queries =========================================
bool AdvUIDoctor::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    return false;
}

///======================================= Manipulators ===========================================
void AdvUIDoctor::TakeForeground()
{
    ///--[Documentation]
    //--Called when this object is shown.

    ///--[Variables]
    //--System
    mCursor = 0;

    ///--[Construction]
    //--Set cursor to 0th position, instantly complete to snap the cursor to position.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--Top instruction.
    mInstructionsStringA->SetString("[IMG0][IMG1] Select Character [IMG2] Apply Doctor Bag");
    mInstructionsStringA->AllocateImages(3);
    mInstructionsStringA->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Left"));
    mInstructionsStringA->SetImageP(1, 3.0f, ControlManager::Fetch()->ResolveControlImage("Right"));
    mInstructionsStringA->SetImageP(2, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mInstructionsStringA->CrossreferenceImages();

    //--Middle Instruction.
    mInstructionsStringB->SetString("[IMG0] Heal entire party equally");
    mInstructionsStringB->AllocateImages(3);
    mInstructionsStringB->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mInstructionsStringB->CrossreferenceImages();

    //--Bottom Instruction.
    mInstructionsStringC->SetString("1 charge = 1% HP. Potency increases healing per charge.");
    mInstructionsStringC->AllocateImages(0);
    mInstructionsStringC->CrossreferenceImages();
}

///======================================= Core Methods ===========================================
void AdvUIDoctor::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Set Lines]
    //--Set lines.
    SetHelpString(i, "The doctor bag allows you to heal your party members out of combat. You can select a");
    SetHelpString(i, "member to heal directly, or heal the whole party at once.");
    SetHelpString(i, "");
    SetHelpString(i, "The doctor bag regains charges when defeating enemies, and gains more charges if you ");
    SetHelpString(i, "defeat them in fewer turns. Each charge represents 1%% health healed, and upgrades ");
    SetHelpString(i, "can increase this and the bag's capacity.");
    SetHelpString(i, "");
    SetHelpString(i, "The doctor bag fully recharges when resting at a save point, but this respawns all");
    SetHelpString(i, "defeated enemies. There are also plus-sign shaped pickups in the world that will");
    SetHelpString(i, "refill your doctor bag when you touch them.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void AdvUIDoctor::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--Given a character under consideration, computes the highlight position. The size is always
    //  the same, while the characters are a fixed distance apart.
    float cWidPerChar = 260.0f;

    //--Positions.
    float cLft =  36.0f + (cWidPerChar * mCursor);
    float cTop = 442.0f;
    float cWid = 242.0f;
    float cHei = 317.0f;

    //--Set.
    mHighlightPos. MoveTo(cLft, cTop, cCurTicks);
    mHighlightSize.MoveTo(cWid, cHei, cCurTicks);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUIDoctor::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles updating timers and handling player control inputs for this menu. If it returns true,
    //  it handled the update and should block other objects from handling controls.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    AdvCombat *rAdvCombat = AdvCombat::Fetch();
    AudioManager *rAudioManager = AudioManager::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Standard timers.
    StandardTimer(mVisibilityTimer,     0, cVisTicks, true);
    StandardTimer(mHelpVisibilityTimer, 0, cHlpTicks, mIsShowingHelp);

    //--The static vis timer tracks the regular vis timer.
    xDoctorVisTimer = mVisibilityTimer;

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsShowingHelp = false;
            rAudioManager->PlaySound("Menu|Select");
        }
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsShowingHelp = true;
        RefreshMenuHelp();
        rAudioManager->PlaySound("Menu|Select");
        return true;
    }

    ///--[Control Handling]
    //--This object has no sub-modes and very little control handling.
    //--Left. Decrements cursor. Wraps.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Decrement.
        mCursor --;

        //--Wrap check.
        if(mCursor < 0) mCursor = rAdvCombat->GetActivePartyCount() - 1;
        if(mCursor < 0) mCursor = 0;

        //--Cursor handling.
        RecomputeCursorPositions();

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
    }
    //--Right. Increments cursor. Wraps.
    else if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Decrement.
        mCursor ++;

        //--Wrap check.
        if(mCursor >= rAdvCombat->GetActivePartyCount()) mCursor = 0;

        //--Cursor handling.
        RecomputeCursorPositions();

        //--SFX.
        rAudioManager->PlaySound("Menu|ChangeHighlight");
    }

    //--Activate. Executes healing on the given target.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Send the instruction.
        rAdvCombat->HealFromDoctorBag(mCursor);

        //--SFX.
        rAudioManager->PlaySound("Combat|DoctorBag");
        return true;
    }

    //--Run. Attempts to heal the entire party equally.
    if(rControlManager->IsFirstPress("Run"))
    {
        //--Send the instruction.
        rAdvCombat->HealFromDoctorBag(-1);

        //--SFX.
        rAudioManager->PlaySound("Combat|DoctorBag");
        return true;
    }

    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        rAudioManager->PlaySound("Menu|Select");
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}
void AdvUIDoctor::UpdateBackground()
{
    ///--[Documentation]
    //--The doctor UI runs hiding timers and continues to run the cursor if it was moving.

    ///--[Timers]
    //--Standard timers.
    StandardTimer(mVisibilityTimer, 0, cVisTicks, false);

    //--The static vis timer tracks the regular vis timer.
    xDoctorVisTimer = mVisibilityTimer;

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUIDoctor::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Top component. Does the primary rendering.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Rendering]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header Text.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Doctor Bag");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Static Parts.
    AdvImages.rFrame_Primary->Draw();
    AdvImages.rOverlay_Symbol->Draw();

    //--Percentage of doctor bag available.
    int tChargesCur = rInventory->GetDoctorBagCharges();
    int tChargesMax = rInventory->GetDoctorBagChargesMax();
    if(tChargesMax < 1) tChargesMax = 1;
    float tPercent = (float)tChargesCur / (float)tChargesMax;
    AdvImages.rOverlay_BarFill->RenderPercent(0.0f, 0.0f, 0.0f, 0.0f, tPercent, 1.0f);

    //--Bar frame.
    AdvImages.rOverlay_BarFrame->Draw();

    //--Values text.
    AdvImages.rFont_Statistics->DrawTextArgs(435.0f, 185.0f, 0, 1.0f, "Charges: %i / %i", tChargesCur, tChargesMax);

    //--Charge rate, potency.
    int tRatePct    = (int)(rInventory->GetDoctorBagChargeRate() * 100.0f);
    int tPotencyPct = (int)(rInventory->GetDoctorBagPotency() * 100.0f);
    AdvImages.rFont_Statistics->DrawText    (780.0f, 185.0f, 0, 1.0f, "Charge Rate:");
    AdvImages.rFont_Statistics->DrawTextArgs(930.0f, 185.0f, 0, 1.0f, "%i%%", tRatePct);
    AdvImages.rFont_Statistics->DrawText    (780.0f, 202.0f, 0, 1.0f, "Potency:");
    AdvImages.rFont_Statistics->DrawTextArgs(930.0f, 202.0f, 0, 1.0f, "%i%%", tPotencyPct);

    //--Instructions.
    mInstructionsStringA->DrawText(357.0f, 264.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mInstructionsStringB->DrawText(357.0f, 284.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);
    mInstructionsStringC->DrawText(357.0f, 304.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Cursor]
    //--Highlight.
    RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rHighlight_Standard);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
