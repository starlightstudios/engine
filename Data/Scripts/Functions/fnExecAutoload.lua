-- |[ ==================================== fnExecAutoload() ==================================== ]|
--Executes the autoload provided, assuming the correct SLF file is open.
function fnExecAutoload(psAutoloadName)
    
    --Deprecated. Calls class version.
    Autoload:fnExecute(psAutoloadName)
    
    --[=[
    
    -- |[ =============== Setup ============== ]|
	-- |[Argument Check]|
	if(psAutoloadName == nil) then return end
    
    -- |[Diagnostics]|
    --If the diagnostic function isn't set yet, create a dummy version. This gets overrode later.
    if(fnDiagnosticFlag == nil) then
        function fnDiagnosticFlag()
            return false
        end
    end
    
    --Get variables.
    local bReportNames         = fnDiagnosticFlag("Autoloader", "Report Names")
    local bReportInvalidGroups = fnDiagnosticFlag("Autoloader", "Report Invalid Groups")
    local bReportEntries       = fnDiagnosticFlag("Autoloader", "Report Entries")
    
    --Header text.
    local bAnyInvalidGroups = false
    if(bReportNames) then
        io.write("Handling autoload: " .. psAutoloadName .. ": ")
    end
    
    -- |[Get Variables]|
    --Run internal autoloader.
    SLF_PopulateAutoLoad(psAutoloadName)
    
    --Get how many name/path pairs to expect.
    local iAutoLoadCount = SLF_GetAutoLoadCount()
    if(bReportNames) then
        io.write(iAutoLoadCount .. " entries.\n")
    end
    
    -- |[ ============== Iterate ============= ]|
    --For each name/path pair, add the path and extract.
    for i = 0, iAutoLoadCount - 1, 1 do
        
        -- |[Get and Load]|
        --Get strings.
        local sName, sPath = SLF_GetAutoLoadEntry(i)
        if(bReportEntries) then
            io.write(" " .. sName .. ": " .. sPath .. " - ")
        end
        
        -- |[Modify Flags]|
        --Modifies filtering flags.
        if(sName == "MAGFILTER NEAREST") then
            ALB_SetTextureProperty("MagFilter", DM_GetEnumeration("GL_NEAREST"))
        elseif(sName == "MINFILTER NEAREST") then
            ALB_SetTextureProperty("MinFilter", DM_GetEnumeration("GL_NEAREST"))
        elseif(sName == "MAGFILTER LINEAR") then
            ALB_SetTextureProperty("MagFilter", DM_GetEnumeration("GL_LINEAR"))
        elseif(sName == "MINFILTER LINEAR") then
            ALB_SetTextureProperty("MinFilter", DM_GetEnumeration("GL_LINEAR"))
        elseif(sName == "WRAPS REPEAT") then
            ALB_SetTextureProperty("Wrap S", DM_GetEnumeration("GL_REPEAT"))
        elseif(sName == "WRAPT REPEAT") then
            ALB_SetTextureProperty("Wrap T", DM_GetEnumeration("GL_REPEAT"))
        elseif(sName == "RESET FLAGS") then
            ALB_SetTextureProperty("Restore Defaults")
        
        -- |[StarPointerSeries Handling]|
        --StarPointerSeries allows the C++ state to load a list of font/image entries. They are basically lists stored in the datalibrary.
        -- The lists are of paths, not pointers, so they can be created before the font/image in question has been loaded.
        elseif(sName == "SPS NEW") then
            StarPointerSeries_SetProperty("Create New", sPath)

        elseif(sName == "SPS DONE") then
            StarPointerSeries_SetProperty("Finish")

        elseif(sName == "SPS REGISTER FONT") then
            StarPointerSeries_SetProperty("Add Font", sPath)

        elseif(sName == "SPS REGISTER IMAGE") then
            StarPointerSeries_SetProperty("Add Image", sPath)

        -- |[File Switch]|
        --If the name is "FILESWITCH", change which file is open. The path will be the file identity.
        elseif(sName == "FILESWITCH") then
            local sFilePath = SLF_GetFilePath(sPath)
            
            --Error:
            if(sFilePath == "Null") then
                io.write("Warning: Autoloader did not find file identity - " .. sPath .. "\n")
            
            --Switch:
            else
                if(bReportEntries) then
                    io.write(" Executing fileswitch.\n")
                end
                SLF_Open(sFilePath)
            end
            
        
        -- |[Load File]|
        else
            DL_AddPath(sPath)
            DL_ExtractBitmap(sName, sPath)
        end
        
        -- |[Diagnostics]|
        if(bReportInvalidGroups and sName ~= "FILESWITCH") then
            if(DL_Exists(sPath) == false) then
                bAnyInvalidGroups = true
            end
        end
        if(bReportEntries and sName ~= "FILESWITCH") then
            if(DL_Exists(sPath) == true) then
                io.write("Valid.\n")
            else
                io.write("Invalid.\n")
            end
        end
    end
	
    -- |[ =========== Diagnostics ============ ]|
    --Report if at least one invalid entry was found in the group.
    if(bReportInvalidGroups) then
        if(bAnyInvalidGroups == true) then
            io.write("Invalid entry detected in autoloader group.\n")
        end
    end
    
    --Footer text.
    if(bReportNames) then
        io.write("Finished autoload.\n")
    end
    ]=]
end