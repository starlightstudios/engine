//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

///===================================== Standard Pathing =========================================
void TilemapActor::HandleStandardPathing()
{
    ///--[Documentation And Setup]
    //--Normal pathing with no special flags. The entity moves between the patrol nodes placed for them
    //  in order, and does not attempt to path around obstacles. Simple, but extremely fast to
    //  execute and only gets jammed if the map designer is a dolt.

    //--Fast-Access Pointers.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;

    //--Get the patrol node in question.
    TwoDimensionReal tDimensions = rActiveLevel->GetPatrolNode(mPatrolNodeList[mPatrolNodeCurrent]);

    ///--[Reached The Node]
    //--Move to its center point. If HandleMoveTo() returns true, the entity has arrived at the node.
    int tStoreMove = mMoveTimer;
    if(HandleMoveTo(tDimensions.mXCenter, tDimensions.mYCenter, mMoveSpeed * 0.40f))
    {
        //--Subroutine handles the next move.
        DecideNextPathDestination();

        //--Set this as our leashing point.
        SetLeashingPoint(mTrueX, mTrueY);
    }
    ///--[Not At Node]
    //--Not at the location yet.
    else
    {
        //--Flags.
        mIsMoving = true;
        if(mMoveTimer != tStoreMove) mMoveTimer = tStoreMove + 1;

        //--Decrement this timer. If it reaches zero, we stop and look around a bit. This does not occur if the
        //  no-stop flag is set.
        mPatrolTicksLeft --;
        if(mPatrolTicksLeft < 1 && !mUseStopOnEachNode && !mNeverPauses)
        {
            mPatrolTicksLeft = (rand() % 360) + 90;
            mAIState = TA_AI_WAIT;
            mWaitTimer = 0;
            mWaitTimerMax = rand() % 60 + 60;
        }

        //--Set this as our leashing point.
        SetLeashingPoint(mTrueX, mTrueY);
    }
}
