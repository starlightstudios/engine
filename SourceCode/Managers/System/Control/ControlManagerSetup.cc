//--Base
#include "ControlManager.h"

//--Classes
//--Definitions
#include "Global.h"

//--Generics
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

///--[Documentation]
//--These functions create and return a new ControlState * with a specific set of control codes already in use.
//  The caller must handle deallocation.
ControlState *ControlManager::BootControl()
{
    //--Returns a control with all its values set to neutrals.  Some of these are non-zero.
    SetMemoryData(__FILE__, __LINE__);
    ControlState *nControl = (ControlState *)starmemoryalloc(sizeof(ControlState));
    nControl->mWatchKeyPri = -1;
    nControl->mWatchKeySec = -1;
    nControl->mWatchMouseBtnPri = -1;
    nControl->mWatchMouseBtnSec = -1;
    nControl->mWatchJoyPri = -1;
    nControl->mWatchJoySec = -1;

    nControl->mIsFirstPress = false;
    nControl->mIsFirstRelease = false;
    nControl->mIsDown = false;
    nControl->mTicksSincePress = -1;

    nControl->mHasPendingRelease = false;
    return nControl;
}
ControlState *ControlManager::CreateControlWatchingKeyboard(int pPriCode, int pSecCode)
{
    //--Control watching zero, one, or two keyboard scancodes.
    ControlState *nControl = BootControl();
    nControl->mWatchKeyPri = pPriCode;
    nControl->mWatchKeySec = pSecCode;

    return nControl;
}
ControlState *ControlManager::CreateControlWatchingMouse(int pPriCode, int pSecCode)
{
    //--Control watching none, one of, or both of the mouse buttons.
    ControlState *nControl = BootControl();
    nControl->mWatchMouseBtnPri = pPriCode;
    nControl->mWatchMouseBtnSec = pSecCode;

    return nControl;
}
ControlState *ControlManager::CreateControlWatchingJoystick(int pPriCode, int pSecCode)
{
    //--Control watching none, one of, or both of the mouse buttons.
    ControlState *nControl = BootControl();
    nControl->mWatchJoyPri = pPriCode;
    nControl->mWatchJoySec = pSecCode;

    return nControl;
}
