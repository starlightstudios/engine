//--Base
#include "TextLevOptions.h"

//--Classes
#include "TextLevel.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"
#include "MapManager.h"

///========================================== System ==============================================
TextLevOptions::TextLevOptions()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_TEXTLEVOPTIONS;

    //--[TextLevOptions]
    //--System
    rCaller = NULL;
    mIsVisible = false;
    mVisibilityTimer = 0;

    //--Positions
    mTxtHeading.mXCenter = (VIRTUAL_CANVAS_X * 0.50f);
    mTxtHeading.mYCenter = 85.0f;
    mTxtConfirm.mXCenter = (VIRTUAL_CANVAS_X * 0.50f);
    mTxtConfirm.mYCenter = 230.0f;

    //--Buttons
    mBtnDim[TLO_BTN_SAVE].SetWH( 1067.0f, 639.0f, 205.0f, 73.0f);
    mBtnDim[TLO_BTN_CANCEL].SetWH(886.0f, 657.0f, 168.0f, 56.0f);
    mBtnDim[TLO_BTN_ENDINGS].SetWH(130.0f, 657.0f, 172.0f, 56.0f);
    mBtnDim[TLO_BTN_ENDINGBACK].SetWH(130.0f, 657.0f, 172.0f, 56.0f);

    //--Options
    mHighlightedOption = 0;
    for(int i = 0; i < TLO_OPTION_TOTAL; i ++) mOptionPacks[i].Initialize();

    //--Endings Mode
    mIsEndingsMode = false;
    for(int i = 0; i < TLO_ENDINGS_TOTAL; i ++) mEndingPacks[i].Initialize();

    //--String Storage
    memset(mColorThemeBuf, 0, sizeof(mDisplayModeBuf));
    memset(mTextSizeBuf, 0, sizeof(mDisplayModeBuf));
    memset(mDisplayModeBuf, 0, sizeof(mDisplayModeBuf));
    memset(mDollTypeBuf, 0, sizeof(mDisplayModeBuf));
    memset(mDollColorBuf, 0, sizeof(mDisplayModeBuf));

    //--Confirmation.
    mIsConfirmation = false;
    mConfirmationTimer = 0;
    memset(mConfirmationText, 0, sizeof(mConfirmationText));
    mConfirmationOptionHandler = -1;
    mConfirmBtnNo.SetWH( 470.0f, 354.0f, 159.0f, 56.0f);
    mConfirmBtnYes.SetWH(737.0f, 354.0f, 159.0f, 56.0f);

    //--Images
    memset(&Images, 0, sizeof(Images));
}
TextLevOptions::~TextLevOptions()
{
}
void TextLevOptions::Construct(TextLevel *pCaller)
{
    //--[Setup]
    //--Store caller.
    rCaller = pCaller;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--[Loading]
    //--Fonts
    Images.Data.rFontHeader = rDataLibrary->GetFont("Text Lev Options Header");
    Images.Data.rFontMain   = rDataLibrary->GetFont("Text Lev Options Main");

    //--Images
    Images.Data.rBacking    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/TxtOptions/Backing");
    Images.Data.rBtnBack    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/TxtOptions/BtnBack");
    Images.Data.rBtnSave    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/TxtOptions/BtnCancel");
    Images.Data.rBtnCancel  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/TxtOptions/BtnSave");
    Images.Data.rBtnEndings = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/TxtOptions/BtnEndings");
    Images.Data.rConfirm    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/TxtOptions/Confirm");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--Construct the option listings.
    ConstructOptions(pCaller);
    ConstructEndings(pCaller);
}
void TextLevOptions::ConstructOptions(TextLevel *pCaller)
{
    //--[Stop if No Caller]
    if(!pCaller) return;

    //--Fast-Access Pointers
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--Position Constants
    float cOptX = 210.0f;
    float cOptY = 148.0f;
    float cOptW = 450.0f;
    float cOptH =  30.0f;
    float cOptS =   1.0f;

    //--Flags.
    bool tShowDollOptions = false;
    int cEndingsFlags = rOptionsManager->GetOptionI("String Tyrant Endings");
    if(cEndingsFlags > 0) tShowDollOptions = true;

    ///--[Option Package Music Volume]
    //--Base value.
    int tCurrentMusicVol = (int)(AudioManager::xMusicVolume * 100.0f);

    //--Set.
    mOptionPacks[TLO_OPTION_MUSICVOL].mWheelScaler = 1.0f;
    strcpy(mOptionPacks[TLO_OPTION_MUSICVOL].mDisplay, "Music Volume");
    mOptionPacks[TLO_OPTION_MUSICVOL].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mType = POINTER_TYPE_INT;
    mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i = tCurrentMusicVol;

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mOptionalText[0], "Volume of ambient music.");
    strcpy(mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mOptionalText[1], "Left-click to increase by 5, right-click to decrease by 5.");
    strcpy(mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mOptionalText[2], "Hold Ctrl to increment/decrement by 1.");

    ///--[Option Package - Sound Volume]
    //--Base value.
    cOptY = cOptY + cOptH + cOptS;
    int tCurrentSoundVol = (int)(AudioManager::xSoundVolume * 100.0f);

    //--Set.
    mOptionPacks[TLO_OPTION_SOUNDVOL].mWheelScaler = 1.0f;
    strcpy(mOptionPacks[TLO_OPTION_SOUNDVOL].mDisplay, "Sound Volume");
    mOptionPacks[TLO_OPTION_SOUNDVOL].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mType = POINTER_TYPE_INT;
    mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i = tCurrentSoundVol;

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mOptionalText[0], "Volume of sound effects, including combat.");
    strcpy(mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mOptionalText[1], "Left-click to increase by 5, right-click to decrease by 5.");
    strcpy(mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mOptionalText[2], "Hold Ctrl to increment/decrement by 1.");

    ///--[Option Package - Color Theme]
    //--Base value.
    cOptY = cOptY + cOptH + cOptS;
    int tColorTheme = 0;
    if(pCaller->IsDarkTheme()) tColorTheme = 1;

    //--Set.
    mOptionPacks[TLO_OPTION_COLORTHEME].mWheelScaler = 1.0f;
    strcpy(mOptionPacks[TLO_OPTION_COLORTHEME].mDisplay, "Color Theme");
    mOptionPacks[TLO_OPTION_COLORTHEME].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mType = POINTER_TYPE_INT;
    mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mValue.i = tColorTheme;
    mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.rAssociatedString = mColorThemeBuf;

    //--Buffer.
    if(tColorTheme == 0)
    {
        strcpy(mColorThemeBuf, "Light");
    }
    else
    {
        strcpy(mColorThemeBuf, "Dark");
    }

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mOptionalText[0], "Light and Dark UI themes. Click to cycle.");
    strcpy(mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mOptionalText[1], "");
    strcpy(mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mOptionalText[2], "");

    ///--[Option Package - Large Text]
    //--Base value.
    cOptY = cOptY + cOptH + cOptS;
    int tLargeText = 0;
    if(pCaller->IsLargeTextMode()) tLargeText = 1;

    //--Set.
    mOptionPacks[TLO_OPTION_TEXTSIZE].mWheelScaler = 1.0f;
    strcpy(mOptionPacks[TLO_OPTION_TEXTSIZE].mDisplay, "Large Text");
    mOptionPacks[TLO_OPTION_TEXTSIZE].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mType = POINTER_TYPE_INT;
    mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mValue.i = tLargeText;
    mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.rAssociatedString = mTextSizeBuf;

    //--Buffer.
    if(tLargeText == 0)
    {
        strcpy(mTextSizeBuf, "Small");
    }
    else
    {
        strcpy(mTextSizeBuf, "Large");
    }

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mOptionalText[0], "Large text is a 30pt font, smaller is a 20pt font.");
    strcpy(mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mOptionalText[1], "Use whichever is most comfortable.");
    strcpy(mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mOptionalText[2], "");

    ///--[Option Package - Resolution]
    //--Base value.
    cOptY = cOptY + cOptH + cOptS;

    //--Get the display mode information from the display manager.
    int tActiveDisplayIndex;
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    DisplayInfo tDisplayInfo = rDisplayManager->GetActiveDisplayMode(tActiveDisplayIndex);
    sprintf(mDisplayModeBuf, "%ix%i %ihz", tDisplayInfo.mWidth, tDisplayInfo.mHeight, tDisplayInfo.mRefreshRate);

    //--Set.
    mOptionPacks[TLO_OPTION_RESOLUTION].mWheelScaler = 0.0f;
    strcpy(mOptionPacks[TLO_OPTION_RESOLUTION].mDisplay, "Resolution");
    mOptionPacks[TLO_OPTION_RESOLUTION].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mType = POINTER_TYPE_INT;
    mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mValue.i = tActiveDisplayIndex;
    mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.rAssociatedString = mDisplayModeBuf;

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mOptionalText[0], "How large the game appears on-screen. Left-click cycles up, right-click cycles down.");
    strcpy(mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mOptionalText[1], "The game will attempt to change resolutions when you click the 'Save' button.");
    strcpy(mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mOptionalText[2], "Standard resolution is 1366x768.");

    ///--[Option Package - Toggle Fullscreen]
    //--Position.
    cOptY = cOptY + cOptH + cOptS;

    //--Set.
    mOptionPacks[TLO_OPTION_FULLSCREEN].mWheelScaler = 0.0f;
    strcpy(mOptionPacks[TLO_OPTION_FULLSCREEN].mDisplay, "Toggle Fullscreen");
    mOptionPacks[TLO_OPTION_FULLSCREEN].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_FULLSCREEN].mOptionPack.mType = POINTER_TYPE_FAIL;

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_FULLSCREEN].mOptionPack.mOptionalText[0], "Click to toggle fullscreen on or off.");
    strcpy(mOptionPacks[TLO_OPTION_FULLSCREEN].mOptionPack.mOptionalText[1], "This can be done with the F11 or F12 keys at any time.");
    strcpy(mOptionPacks[TLO_OPTION_FULLSCREEN].mOptionPack.mOptionalText[2], " ");

    ///--[Option Package - Doll Type]
    //--Position
    cOptY = cOptY + cOptH + cOptS;
    float cPostFullscreenPos = cOptY;
    if(!tShowDollOptions) cOptY = -10000.0f;

    //--Set.
    int cTypePref = rOptionsManager->GetOptionI("String Tyrant Type Pref");
    mOptionPacks[TLO_OPTION_DOLLTYPE].mWheelScaler = 0.0f;
    strcpy(mOptionPacks[TLO_OPTION_DOLLTYPE].mDisplay, "Doll Type");
    mOptionPacks[TLO_OPTION_DOLLTYPE].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mType = POINTER_TYPE_INT;
    mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mValue.i = cTypePref;
    mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.rAssociatedString = mDollTypeBuf;

    //--Resolve doll type.
    if(cTypePref == 0) strcpy(mDollTypeBuf, "Random");
    else if(cTypePref == 1) strcpy(mDollTypeBuf, "Dancer");
    else if(cTypePref == 2) strcpy(mDollTypeBuf, "Geisha");
    else if(cTypePref == 3) strcpy(mDollTypeBuf, "Princess");
    else if(cTypePref == 4) strcpy(mDollTypeBuf, "Goth");
    else if(cTypePref == 5) strcpy(mDollTypeBuf, "Bride");
    else if(cTypePref == 6) strcpy(mDollTypeBuf, "Punk");

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mOptionalText[0], "Determines which doll you will transform into. Only works if");
    strcpy(mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mOptionalText[1], "set before the transformation begins. Some plot flags override this.");
    strcpy(mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mOptionalText[2], "Unlocked by getting at least one doll ending.");

    ///--[Option Package - Doll Color]
    //--Position
    cOptY = cOptY + cOptH + cOptS;

    //--Set.
    int cColorPref = rOptionsManager->GetOptionI("String Tyrant Color Pref");
    mOptionPacks[TLO_OPTION_DOLLCOLOR].mWheelScaler = 0.0f;
    strcpy(mOptionPacks[TLO_OPTION_DOLLCOLOR].mDisplay, "Doll Color");
    mOptionPacks[TLO_OPTION_DOLLCOLOR].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mType = POINTER_TYPE_INT;
    mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mValue.i = cColorPref;
    mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.rAssociatedString = mDollColorBuf;

    //--Resolve doll type.
    if(cColorPref == 0) strcpy(mDollColorBuf, "Random");
    else if(cColorPref == 1) strcpy(mDollColorBuf, "White");
    else if(cColorPref == 2) strcpy(mDollColorBuf, "Pasty");
    else if(cColorPref == 3) strcpy(mDollColorBuf, "Tanned");
    else if(cColorPref == 4) strcpy(mDollColorBuf, "Dark");

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mOptionalText[0], "Determines which color your doll skin transforms to. Only works if");
    strcpy(mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mOptionalText[1], "set before the transformation begins. Some plot flags override this.");
    strcpy(mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mOptionalText[2], "Unlocked by getting at least one doll ending.");

    ///--[Option Package - Back to Title]
    //--Position.
    cOptY = cOptY + cOptH + cOptS;

    //--Special: If the doll options are not present, move the cursor.
    if(!tShowDollOptions) cOptY = cPostFullscreenPos;

    //--Set.
    mOptionPacks[TLO_OPTION_BACKTOTITLE].mWheelScaler = 0.0f;
    strcpy(mOptionPacks[TLO_OPTION_BACKTOTITLE].mDisplay, "Back to Title Screen");
    mOptionPacks[TLO_OPTION_BACKTOTITLE].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_BACKTOTITLE].mOptionPack.mType = POINTER_TYPE_FAIL;

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_BACKTOTITLE].mOptionPack.mOptionalText[0], "Returns to the title screen. Same as the [restart] command.");
    strcpy(mOptionPacks[TLO_OPTION_BACKTOTITLE].mOptionPack.mOptionalText[1], " ");
    strcpy(mOptionPacks[TLO_OPTION_BACKTOTITLE].mOptionPack.mOptionalText[2], " ");

    ///--[Option Package - Back to Title]
    //--Position.
    cOptY = cOptY + cOptH + cOptS;

    //--Set.
    mOptionPacks[TLO_OPTION_EXITGAME].mWheelScaler = 0.0f;
    strcpy(mOptionPacks[TLO_OPTION_EXITGAME].mDisplay, "Exit Game");
    mOptionPacks[TLO_OPTION_EXITGAME].mClickDim.SetWH(cOptX, cOptY, cOptW, cOptH);
    mOptionPacks[TLO_OPTION_EXITGAME].mOptionPack.mType = POINTER_TYPE_FAIL;

    //--Text.
    strcpy(mOptionPacks[TLO_OPTION_EXITGAME].mOptionPack.mOptionalText[0], "Quits the game. Same as the [quit] command.");
    strcpy(mOptionPacks[TLO_OPTION_EXITGAME].mOptionPack.mOptionalText[1], " ");
    strcpy(mOptionPacks[TLO_OPTION_EXITGAME].mOptionPack.mOptionalText[2], " ");
}
void TextLevOptions::ConstructEndings(TextLevel *pCaller)
{
    //--[Stop if No Caller]
    if(!pCaller) return;

    //--[Variables]
    //--Fast-access.
    int i = 0;

    //--Get the options flag.
    int cEndingsFlag = OptionsManager::Fetch()->GetOptionI("String Tyrant Endings");

    //--[Position Constants]
    float cOptX = 210.0f;
    float cOptY = 148.0f;
    float cOptW = 450.0f;
    float cOptH =  30.0f;
    float cOptS =   1.0f;

    //--[Ending Package - Plaything]
    //--Set.
    i = TLO_ENDING_PLAYTHING;
    mEndingPacks[i].SetI("Ending 1: Plaything", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Plaything");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Get transformed into a doll during most parts of the game.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Face in a Crowd]
    //--Set.
    i = TLO_ENDING_FACEINACROWD;
    mEndingPacks[i].SetI("Ending 2: Face in a Crowd", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Face in a Crowd");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Attempt to confront Pygmalie before you're ready.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Gallery]
    //--Set.
    i = TLO_ENDING_GALLERY;
    mEndingPacks[i].SetI("Ending 3: Gallery", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Gallery");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Become a statue. OBEY THE LIGHT, CAPTURE THE HUMANS.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Malleable]
    //--Set.
    i = TLO_ENDING_MALLEABLE;
    mEndingPacks[i].SetI("Ending 4: Malleable", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Malleable");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Become a claygirl and hunt down your former friends.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Good Dolly]
    //--Set.
    i = TLO_ENDING_GOODDOLLY;
    mEndingPacks[i].SetI("Ending 5: Good Dolly", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Good Dolly");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "During the finale, betray your friends, then submit to Pygmalie.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Mistress of the Dolls]
    //--Set.
    i = TLO_ENDING_MISTRESSDOLLS;
    mEndingPacks[i].SetI("Ending 6: Mistress of the Dolls", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Mistress of the Dolls");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "During the finale, betray your friends, then usurp Pygmalie.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - A True Hero]
    //--Set.
    i = TLO_ENDING_TRUEHERO;
    mEndingPacks[i].SetI("Ending 7: A True Hero", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "A True Hero");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "During the finale, sacrifice yourself.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - A True Hero]
    //--Set.
    i = TLO_ENDING_JESSIE;
    mEndingPacks[i].SetI("Ending 8: Jessie the Great", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Jessie the Great");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "During the finale, sacrifice Jessie.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - World's Best Brother]
    //--Set.
    i = TLO_ENDING_LAUREN;
    mEndingPacks[i].SetI("Ending 9: World's Best Brother", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "World's Best Brother");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "During the finale, sacrifice Lauren.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Never Give Up]
    //--Set.
    i = TLO_ENDING_SARAH;
    mEndingPacks[i].SetI("Ending 10: Never Give Up", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Never Give Up");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "During the finale, sacrifice Sarah.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Reposition]
    cOptX = 610.0f;
    cOptY = 148.0f;

    //--[Ending Package - Dancing Marionette]
    //--Set.
    i = TLO_ENDING_MARIONETTE;
    mEndingPacks[i].SetI("Ending 11: Dancing Marionette", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Dancing Marionette");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Fail to escape the Grand Archives trap, and become a doll.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Identical Twins]
    //--Set.
    i = TLO_ENDING_RUBBER;
    mEndingPacks[i].SetI("Ending 12: Identical Twins", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Identical Twins");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Fail to escape the Brewey trap, and become a goopy rubber clone.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Cool It!]
    //--Set.
    i = TLO_ENDING_ICE;
    mEndingPacks[i].SetI("Ending 13: Cool It!", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Cool It!");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Fail to escape the Freezing Caverns trap, and become an ice sculpture.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - The Perfect Disguise]
    //--Set.
    i = TLO_ENDING_CLAYHOUSE;
    mEndingPacks[i].SetI("Ending 14: The Perfect Disguise", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "The Perfect Disguise");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Fail to escape the Strange House trap, and become a claygirl.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Ending Package - Work of Art]
    //--Set.
    i = TLO_ENDING_GLASS;
    mEndingPacks[i].SetI("Ending 15: Work of Art", cOptX, cOptY, cOptW, cOptH, 0);
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[0], "Work of Art");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[1], "Be defeated by the stranger and transformed into a glass sculpture.");
    strcpy(mEndingPacks[i].mOptionPack.mOptionalText[2], " ");
    cOptY = cOptY + cOptH + cOptS;

    //--[Post-Set]
    //--Now run across all the endings and change them to "Locked" if the player has not unlocked them yet.
    i = 1;
    for(int p = 0; p < TLO_ENDINGS_TOTAL; p ++)
    {
        //--Ending is available, do nothing.
        if(cEndingsFlag & i) { i = i * 2; continue;}

        //--Not available, change the package.
        strncpy(mEndingPacks[p].mDisplay, "Ending Locked", sizeof(mEndingPacks[p].mDisplay));
        mEndingPacks[p].mOptionPack.mValue.i = 1;
        strcpy(mEndingPacks[p].mOptionPack.mOptionalText[0], " ");
        strcpy(mEndingPacks[p].mOptionPack.mOptionalText[1], " ");
        strcpy(mEndingPacks[p].mOptionPack.mOptionalText[2], " ");
        i = i * 2;
    }
}

///--[Public Statics]
//--Script that gets called when the player attempts to return to the title screen.
char *TextLevOptions::xResetExec = NULL;

//--Scripts that gets called when the player closes the options menu with the Save button.
char *TextLevOptions::xPostExecPath = NULL;

//--Script that gets called when the player replays an ending, and the firing code.
char *TextLevOptions::xEndingExec = NULL;
int TextLevOptions::xEndingOverride = -1;

///===================================== Property Queries =========================================
bool TextLevOptions::IsActive()
{
    return mIsVisible;
}

///======================================== Manipulators ==========================================
void TextLevOptions::Activate()
{
    mIsVisible = true;
    mIsEndingsMode = false;
}
void TextLevOptions::Deactivate()
{
    mIsVisible = false;
}

///======================================== Core Methods ==========================================
void TextLevOptions::RefreshValues(TextLevel *pCaller)
{
    //--Updates all options with their current values.
    if(!pCaller) return;

    //--Fast-access Pointers.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--Music Volume
    int tCurrentMusicVol = (int)(AudioManager::xMusicVolume * 100.0f);
    mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i = tCurrentMusicVol;

    //--Sound Volume
    int tCurrentSoundVol = (int)(AudioManager::xSoundVolume * 100.0f);
    mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i = tCurrentSoundVol;

    //--Color Theme
    int tColorTheme = 0;
    if(pCaller->IsDarkTheme()) tColorTheme = 1;
    mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mValue.i = tColorTheme;
    if(tColorTheme == 0)
    {
        strcpy(mColorThemeBuf, "Light");
    }
    else
    {
        strcpy(mColorThemeBuf, "Dark");
    }

    //--Large Text.
    int tLargeText = 0;
    if(pCaller->IsLargeTextMode()) tLargeText = 1;
    mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mValue.i = tLargeText;
    if(tLargeText == 0)
    {
        strcpy(mTextSizeBuf, "Small");
    }
    else
    {
        strcpy(mTextSizeBuf, "Large");
    }

    //--Resolution.
    int tActiveDisplayIndex;
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    DisplayInfo tDisplayInfo = rDisplayManager->GetActiveDisplayMode(tActiveDisplayIndex);
    sprintf(mDisplayModeBuf, "%ix%i %ihz", tDisplayInfo.mWidth, tDisplayInfo.mHeight, tDisplayInfo.mRefreshRate);
    mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mValue.i = tActiveDisplayIndex;

    //--Doll Type.
    int cTypePref = rOptionsManager->GetOptionI("String Tyrant Type Pref");
    mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mValue.i = cTypePref;
    if(cTypePref == 0) strcpy(mDollTypeBuf, "Random");
    else if(cTypePref == 1) strcpy(mDollTypeBuf, "Dancer");
    else if(cTypePref == 2) strcpy(mDollTypeBuf, "Geisha");
    else if(cTypePref == 3) strcpy(mDollTypeBuf, "Princess");
    else if(cTypePref == 4) strcpy(mDollTypeBuf, "Goth");
    else if(cTypePref == 5) strcpy(mDollTypeBuf, "Bride");
    else if(cTypePref == 6) strcpy(mDollTypeBuf, "Punk");

    //--Doll Color.
    int cColorPref = rOptionsManager->GetOptionI("String Tyrant Color Pref");
    mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mValue.i = cColorPref;
    if(cColorPref == 0) strcpy(mDollColorBuf, "Random");
    else if(cColorPref == 1) strcpy(mDollColorBuf, "White");
    else if(cColorPref == 2) strcpy(mDollColorBuf, "Pasty");
    else if(cColorPref == 3) strcpy(mDollColorBuf, "Tanned");
    else if(cColorPref == 4) strcpy(mDollColorBuf, "Dark");
}
void TextLevOptions::SaveChanges()
{
    //--Saves all the modified options to their permanent versions.
    AudioManager *rAudioManager = AudioManager::Fetch();
    rAudioManager->ChangeMusicVolumeTo((float)mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i / 100.0f);
    rAudioManager->ChangeSoundVolumeTo((float)mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i / 100.0f);

    //--Check the resolution slot. Change it if the slot changed.
    int tActiveDisplayIndex;
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    rDisplayManager->GetActiveDisplayMode(tActiveDisplayIndex);
    if(tActiveDisplayIndex != mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mValue.i)
    {
        rDisplayManager->SwitchDisplayModes(mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mValue.i);
    }

    //--Update display size.
    DisplayInfo tDisplayInfo = rDisplayManager->GetActiveDisplayMode(tActiveDisplayIndex);

    //--Save the color theme:
    if(rCaller)
    {
        if(mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mValue.i == 0)
        {
            rCaller->SetDarkMode(false);
        }
        else
        {
            rCaller->SetDarkMode(true);
        }
    }

    //--Save the text size:
    if(rCaller)
    {
        if(mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mValue.i == 0)
        {
            rCaller->SetLargeText(false);
        }
        else
        {
            rCaller->SetLargeText(true);
        }
    }

    //--Set the options manager version.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    rOptionsManager->SetOptionI("WinSizeX", tDisplayInfo.mWidth);
    rOptionsManager->SetOptionI("WinSizeY", tDisplayInfo.mHeight);
    rOptionsManager->SetOptionI("MusicVolume", mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i);
    rOptionsManager->SetOptionI("SoundVolume", mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i);
    rOptionsManager->SetOptionI("String Tyrant Dark Mode",  mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mValue.i);
    rOptionsManager->SetOptionI("String Tyrant Large Text", mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mValue.i);
    rOptionsManager->SetOptionI("String Tyrant Type Pref",  mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mValue.i);
    rOptionsManager->SetOptionI("String Tyrant Color Pref", mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mValue.i);
    rOptionsManager->WriteConfigFiles();

    //--Post-exec script.
    if(xPostExecPath) LuaManager::Fetch()->ExecuteLuaFile(xPostExecPath);
}
void TextLevOptions::HandleConfirmClick()
{
    //--When the confirmation window comes up and the player clicks yes, this gets called.
    if(mConfirmationOptionHandler == TLO_OPTION_BACKTOTITLE)
    {
        if(xResetExec) LuaManager::Fetch()->ExecuteLuaFile(xResetExec);
    }
    else if(mConfirmationOptionHandler == TLO_OPTION_EXITGAME)
    {
        Global::Shared()->gQuit = true;
    }
}

///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
void TextLevOptions::Update(bool pAllowControls)
{
    ///--[Fade Handling]
    //--Fading in.
    if(mIsVisible)
    {
        if(mVisibilityTimer < TLO_VISIBLITY_TICKS) mVisibilityTimer ++;
    }
    //--Fading out.
    else
    {
        if(mVisibilityTimer > 0) mVisibilityTimer --;
    }

    //--Confirmation timer.
    if(mIsConfirmation)
    {
        if(mConfirmationTimer < TLO_CONFIRM_TICKS) mConfirmationTimer ++;
    }
    else
    {
        if(mConfirmationTimer > 0) mConfirmationTimer --;
    }

    ///--[Block Update]
    //--After all timers are done, controls are ignored when not the active object.
    if(!mIsVisible || !pAllowControls) return;

    //--Mouse coordinates. We need these to store the mousewheel position.
    int tMouseX, tMouseY, tMouseZ;
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Confirmation window:
    if(mIsConfirmation)
    {
        UpdateConfirmation();
    }
    //--Ending select:
    else if(mIsEndingsMode)
    {
        UpdateEndings();
    }
    //--Options.
    else
    {
        UpdateOptions();
    }

    ///--[Finish Up]
    //--Store the mouse Z for mousewheel movement.
    mOldMouseZ = tMouseZ;
}
void TextLevOptions::UpdateConfirmation()
{
    ///--[Documentation and Setup]
    //--Handles clicking on the confirmation window.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Mouse coordinates.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Left-clicks only.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--No button.
        if(mConfirmBtnNo.IsPointWithin(tMouseX, tMouseY))
        {
            mIsConfirmation = false;
        }
        //--Yes button, executes confirmation.
        else if(mConfirmBtnYes.IsPointWithin(tMouseX, tMouseY))
        {
            mIsConfirmation = false;
            HandleConfirmClick();
        }
    }
}
void TextLevOptions::UpdateOptions()
{
    ///--[Documentation and Setup]
    //--Handles clicking on the options.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Mouse coordinates.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Check for Ctrl-Click. This changes how some options work.
    bool tIsCtrlClick = rControlManager->IsDown("Ctrl");

    ///--[Left-Click]
    //--If left-clicking, handle button updates.
    if(rControlManager->IsFirstPress("MouseLft") || rControlManager->IsFirstPress("MouseRgt"))
    {
        //--Save Button
        if(mBtnDim[TLO_BTN_SAVE].IsPointWithin(tMouseX, tMouseY))
        {
            SaveChanges();
            Deactivate();
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }
        //--Cancel Button
        else if(mBtnDim[TLO_BTN_CANCEL].IsPointWithin(tMouseX, tMouseY))
        {
            Deactivate();
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }
        //--Endings Button
        else if(mBtnDim[TLO_BTN_ENDINGS].IsPointWithin(tMouseX, tMouseY))
        {
            mIsEndingsMode = true;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Determine how much the value changes.
        int tIncrement = 5;
        if(tIsCtrlClick) tIncrement = 1;
        if(rControlManager->IsFirstPress("MouseRgt")) tIncrement = tIncrement * -1;

        //--If we got this far, check if the click was on one of the options.
        if(mOptionPacks[TLO_OPTION_MUSICVOL].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            //--Increment.
            int tValue = mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i;
            tValue += tIncrement;

            //--Clamp.
            if(tValue > 100) tValue = 100;
            if(tValue <   0) tValue =   0;

            //--Set.
            mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i = tValue;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Sound volume.
        else if(mOptionPacks[TLO_OPTION_SOUNDVOL].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            //--Increment.
            int tValue = mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i;
            tValue += tIncrement;

            //--Clamp.
            if(tValue > 100) tValue = 100;
            if(tValue <   0) tValue =   0;

            //--Set.
            mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i = tValue;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Color theme.
        else if(mOptionPacks[TLO_OPTION_COLORTHEME].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            //--Set.
            mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mValue.i = 1 - mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mValue.i;
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Buffer.
            if(mOptionPacks[TLO_OPTION_COLORTHEME].mOptionPack.mValue.i == 0)
            {
                strcpy(mColorThemeBuf, "Light");
            }
            else
            {
                strcpy(mColorThemeBuf, "Dark");
            }
        }
        //--Text size.
        else if(mOptionPacks[TLO_OPTION_TEXTSIZE].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            //--Set.
            mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mValue.i = 1 - mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mValue.i;
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Buffer.
            if(mOptionPacks[TLO_OPTION_TEXTSIZE].mOptionPack.mValue.i == 0)
            {
                strcpy(mTextSizeBuf, "Small");
            }
            else
            {
                strcpy(mTextSizeBuf, "Large");
            }
        }
        //--Resolution.
        else if(mOptionPacks[TLO_OPTION_RESOLUTION].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            //--Increment. Only goes by 1 or -1.
            if(tIncrement > 0) tIncrement = 1;
            if(tIncrement < 0) tIncrement = -1;
            int tValue = mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mValue.i + tIncrement;

            //--Clamp.
            DisplayManager *rDisplayManager = DisplayManager::Fetch();
            if(tValue < 0) tValue = rDisplayManager->GetTotalDisplayModes() - 1;
            if(tValue >= rDisplayManager->GetTotalDisplayModes()) tValue = 0;

            //--Update the buffer.
            DisplayInfo tDisplayInfo = rDisplayManager->GetDisplayMode(tValue);
            sprintf(mDisplayModeBuf, "%ix%i %ihz", tDisplayInfo.mWidth, tDisplayInfo.mHeight, tDisplayInfo.mRefreshRate);

            //--Set.
            mOptionPacks[TLO_OPTION_RESOLUTION].mOptionPack.mValue.i = tValue;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Fullscreen toggle.
        else if(mOptionPacks[TLO_OPTION_FULLSCREEN].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            DisplayManager::Fetch()->FlipFullscreen();
            rControlManager->BlankControls();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Change doll type preference.
        else if(mOptionPacks[TLO_OPTION_DOLLTYPE].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            //--Increment. Only goes by 1 or -1.
            if(tIncrement > 0) tIncrement = 1;
            if(tIncrement < 0) tIncrement = -1;
            int tValue = mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mValue.i + tIncrement;

            //--Clamp.
            if(tValue < 0) tValue = 6;
            if(tValue > 6) tValue = 0;

            //--Set.
            mOptionPacks[TLO_OPTION_DOLLTYPE].mOptionPack.mValue.i = tValue;
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Update the strings.
            if(tValue == 0) strcpy(mDollTypeBuf, "Random");
            else if(tValue == 1) strcpy(mDollTypeBuf, "Dancer");
            else if(tValue == 2) strcpy(mDollTypeBuf, "Geisha");
            else if(tValue == 3) strcpy(mDollTypeBuf, "Princess");
            else if(tValue == 4) strcpy(mDollTypeBuf, "Goth");
            else if(tValue == 5) strcpy(mDollTypeBuf, "Bride");
            else if(tValue == 6) strcpy(mDollTypeBuf, "Punk");
        }
        //--Change doll color preference.
        else if(mOptionPacks[TLO_OPTION_DOLLCOLOR].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            //--Increment. Only goes by 1 or -1.
            if(tIncrement > 0) tIncrement = 1;
            if(tIncrement < 0) tIncrement = -1;
            int tValue = mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mValue.i + tIncrement;

            //--Clamp.
            if(tValue < 0) tValue = 4;
            if(tValue > 4) tValue = 0;

            //--Set.
            mOptionPacks[TLO_OPTION_DOLLCOLOR].mOptionPack.mValue.i = tValue;
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Update the strings.
            if(tValue == 0) strcpy(mDollColorBuf, "Random");
            else if(tValue == 1) strcpy(mDollColorBuf, "White");
            else if(tValue == 2) strcpy(mDollColorBuf, "Pasty");
            else if(tValue == 3) strcpy(mDollColorBuf, "Tanned");
            else if(tValue == 4) strcpy(mDollColorBuf, "Dark");
        }
        //--Back to title.
        else if(mOptionPacks[TLO_OPTION_BACKTOTITLE].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            mIsConfirmation = true;
            mConfirmationOptionHandler = TLO_OPTION_BACKTOTITLE;
            strcpy(mConfirmationText, "Return to title screen?");
        }
        //--Quit the game.
        else if(mOptionPacks[TLO_OPTION_EXITGAME].mClickDim.IsPointWithin(tMouseX, tMouseY))
        {
            mIsConfirmation = true;
            mConfirmationOptionHandler = TLO_OPTION_EXITGAME;
            strcpy(mConfirmationText, "Quit the game?");
        }
    }
    //--Check highlights.
    else
    {
        //--Store the previous highlight. This is used for SFX.
        int tPrevHighlight = mHighlightedOption;
        mHighlightedOption = -1;

        //--Iterate.
        for(int i = 0; i < TLO_OPTION_TOTAL; i ++)
        {
            if(mOptionPacks[i].mClickDim.IsPointWithin(tMouseX, tMouseY))
            {
                mHighlightedOption = i;
                break;
            }
        }

        //--If the highlight changes and is not -1, play SFX.
        if(mHighlightedOption != -1 && mHighlightedOption != tPrevHighlight)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Mousewheel. If there is a highlight, some options can be modified by mousewheel movement.
    if(mHighlightedOption >= 0 && mHighlightedOption < TLO_OPTION_TOTAL && (int)mOldMouseZ != tMouseZ)
    {
        //--Compute.
        int tDiff = tMouseZ - (int)mOldMouseZ;

        //--Music volume:
        if(mHighlightedOption == TLO_OPTION_MUSICVOL)
        {
            mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i += tDiff * mOptionPacks[TLO_OPTION_MUSICVOL].mWheelScaler;
            if(mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i > 100) mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i = 100;
            if(mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i <   0) mOptionPacks[TLO_OPTION_MUSICVOL].mOptionPack.mValue.i =   0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Sound volume:
        else if(mHighlightedOption == TLO_OPTION_SOUNDVOL)
        {
            mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i += tDiff * mOptionPacks[TLO_OPTION_SOUNDVOL].mWheelScaler;
            if(mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i > 100) mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i = 100;
            if(mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i <   0) mOptionPacks[TLO_OPTION_SOUNDVOL].mOptionPack.mValue.i =   0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
}
void TextLevOptions::UpdateEndings()
{
    ///--[Documentation and Setup]
    //--Handles clicking on the endings. If an ending is unlocked, clicking it will begin playing
    //  that ending. Locked endings play an error sound.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Mouse coordinates.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    ///--[Click]
    if(rControlManager->IsFirstPress("MouseLft") || rControlManager->IsFirstPress("MouseRgt"))
    {
        //--Pressing the back button returns to options mode.
        if(mBtnDim[TLO_BTN_ENDINGBACK].IsPointWithin(tMouseX, tMouseY))
        {
            mIsEndingsMode = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Iterate across endings.
        for(int i = 0; i < TLO_ENDINGS_TOTAL; i ++)
        {
            //--Click was on the button:
            if(mEndingPacks[i].mClickDim.IsPointWithin(tMouseX, tMouseY))
            {
                //--Ending is unlocked if it's a zero.
                if(mEndingPacks[i].mOptionPack.mValue.i == 0)
                {
                    Deactivate();
                    xEndingOverride = i;
                    if(xEndingExec) LuaManager::Fetch()->ExecuteLuaFile(xEndingExec);
                    AudioManager::Fetch()->PlaySound("Menu|Select");
                }
                //--Ending is locked if it's nonzero.
                else
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                return;
            }
        }
    }
    ///--[Highlights]
    else
    {
        //--Store the previous highlight. This is used for SFX.
        int tPrevHighlight = mHighlightedOption;
        mHighlightedOption = -1;

        //--Iterate.
        for(int i = 0; i < TLO_ENDINGS_TOTAL; i ++)
        {
            if(mEndingPacks[i].mClickDim.IsPointWithin(tMouseX, tMouseY))
            {
                mHighlightedOption = i;
                break;
            }
        }

        //--If the highlight changes and is not -1, play SFX.
        if(mHighlightedOption != -1 && mHighlightedOption != tPrevHighlight)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
}

///========================================== File I/O ============================================
///========================================== Drawing =============================================
void TextLevOptions::Render()
{
    ///--[Documentation and Setup]
    //--Render the options menu. Does nothing if it's not visible. Complicated!
    if(!Images.mIsReady || mVisibilityTimer < 1) return;

    //--Endings
    if(mIsEndingsMode)
    {
        RenderEndings();
    }
    //--Normal.
    else
    {
        RenderOptions();
    }
}
void TextLevOptions::RenderOptions()
{
    ///--[Documentation and Setup]
    //--Renders as if we're in the options menu.

    ///--[Visibility]
    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, TLO_VISIBLITY_TICKS);

    //--Colors.
    StarlightColor cGrey  = StarlightColor::MapRGBAF(0.80f, 0.80f, 0.80f, cAlpha);
    StarlightColor cWhite = StarlightColor::MapRGBAF(1.00f, 1.00f, 1.00f, cAlpha);

    //--Underlay.
    StarBitmap::DrawFullBlack(cAlpha * 0.75f);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Static Parts]
    //--Backing, buttons, etc.
    Images.Data.rBacking->Draw();
    Images.Data.rBtnSave->Draw();
    Images.Data.rBtnCancel->Draw();
    Images.Data.rBtnEndings->Draw();

    //--Header.
    Images.Data.rFontHeader->DrawText(mTxtHeading.mXCenter, mTxtHeading.mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Options Menu");

    ///--[Options]
    //--Render the option text.
    for(int i = 0; i < TLO_OPTION_TOTAL; i ++)
    {
        //--Resolve color.
        if(i == mHighlightedOption)
            cWhite.SetAsMixer();
        else
            cGrey.SetAsMixer();

        //--Render the left side of the entry.
        float cLft = mOptionPacks[i].mClickDim.mLft;
        float cRgt = mOptionPacks[i].mClickDim.mRgt;
        float cTop = mOptionPacks[i].mClickDim.mTop;
        Images.Data.rFontMain->DrawText(cLft, cTop, 0, 1.0f, mOptionPacks[i].mDisplay);

        //--If this is an option, render the right side. First, if a string is set, use that.
        if(mOptionPacks[i].mOptionPack.rAssociatedString)
        {
            Images.Data.rFontMain->DrawText(cRgt, cTop, SUGARFONT_RIGHTALIGN_X, 1.0f, mOptionPacks[i].mOptionPack.rAssociatedString);
        }
        //--Otherwise, render the integer value:
        else if(mOptionPacks[i].mOptionPack.mType == POINTER_TYPE_INT)
        {
            Images.Data.rFontMain->DrawTextArgs(cRgt, cTop, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", mOptionPacks[i].mOptionPack.mValue.i);
        }
    }

    //--Render text matching the highlighted option, if it exists.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    if(mHighlightedOption >= 0 && mHighlightedOption < TLO_OPTION_TOTAL)
    {
        float cLft = 210.0f;
        float cTop = 520.0f;
        float cHei = Images.Data.rFontMain->GetTextHeight();
        for(int i = 0; i < STT_OPTION_MAX_LINES; i ++)
        {
            if(mOptionPacks[mHighlightedOption].mOptionPack.mOptionalText[i][0] == '\0') continue;
            Images.Data.rFontMain->DrawText(cLft, cTop, 0, 1.0f, mOptionPacks[mHighlightedOption].mOptionPack.mOptionalText[i]);
            cTop = cTop + cHei;
        }
    }

    //--Next.
    StarlightColor::ClearMixer();

    ///--[Confirmation]
    //--Greys out everything else and shows a confirmation window.
    if(mIsConfirmation)
    {
        //--Alpha.
        float cConfirmAlpha = EasingFunction::QuadraticInOut(mConfirmationTimer, TLO_CONFIRM_TICKS);
        StarBitmap::DrawFullBlack(cConfirmAlpha * cAlpha * 0.75f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cConfirmAlpha * cAlpha);

        //--Render.
        Images.Data.rConfirm->Draw();

        //--Text.
        Images.Data.rFontHeader->DrawText(mTxtConfirm.mXCenter, mTxtConfirm.mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, mConfirmationText);

        //--Clean.
        StarlightColor::ClearMixer();
    }
}
void TextLevOptions::RenderEndings()
{
    ///--[Documentation and Setup]
    //--Renders as if we're in the endings menu.

    ///--[Visibility]
    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, TLO_VISIBLITY_TICKS);

    //--Colors.
    StarlightColor cGrey  = StarlightColor::MapRGBAF(0.80f, 0.80f, 0.80f, cAlpha);
    StarlightColor cWhite = StarlightColor::MapRGBAF(1.00f, 1.00f, 1.00f, cAlpha);

    //--Underlay.
    StarBitmap::DrawFullBlack(cAlpha * 0.75f);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Static Parts]
    //--Backing, buttons, etc.
    Images.Data.rBacking->Draw();
    Images.Data.rBtnBack->Draw();

    //--Header.
    Images.Data.rFontHeader->DrawText(mTxtHeading.mXCenter, mTxtHeading.mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Ending Select");

    ///--[Options]
    //--Render the option text.
    for(int i = 0; i < TLO_ENDINGS_TOTAL; i ++)
    {
        //--Resolve color.
        if(i == mHighlightedOption)
            cWhite.SetAsMixer();
        else
            cGrey.SetAsMixer();

        //--Render the left side of the entry.
        float cLft = mEndingPacks[i].mClickDim.mLft;
        float cTop = mEndingPacks[i].mClickDim.mTop;
        Images.Data.rFontMain->DrawText(cLft, cTop, 0, 1.0f, mEndingPacks[i].mDisplay);

        //--Endings don't render a value. They are either locked or not.
    }

    //--Render text matching the highlighted option, if it exists.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    if(mHighlightedOption >= 0 && mHighlightedOption < TLO_ENDINGS_TOTAL)
    {
        float cLft = 210.0f;
        float cTop = 520.0f;
        float cHei = Images.Data.rFontMain->GetTextHeight();
        for(int i = 0; i < STT_OPTION_MAX_LINES; i ++)
        {
            if(mEndingPacks[mHighlightedOption].mOptionPack.mOptionalText[i][0] == '\0') continue;
            Images.Data.rFontMain->DrawText(cLft, cTop, 0, 1.0f, mEndingPacks[mHighlightedOption].mOptionPack.mOptionalText[i]);
            cTop = cTop + cHei;
        }
    }

    //--Next.
    StarlightColor::ClearMixer();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
